package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.BooleanUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.lknfe.xml.nfe.Adi;
import br.com.linkcom.lknfe.xml.nfe.AutXML;
import br.com.linkcom.lknfe.xml.nfe.CIDE;
import br.com.linkcom.lknfe.xml.nfe.COFINS;
import br.com.linkcom.lknfe.xml.nfe.COFINSAliq;
import br.com.linkcom.lknfe.xml.nfe.COFINSNT;
import br.com.linkcom.lknfe.xml.nfe.COFINSOutr;
import br.com.linkcom.lknfe.xml.nfe.COFINSQtde;
import br.com.linkcom.lknfe.xml.nfe.Card;
import br.com.linkcom.lknfe.xml.nfe.Cobr;
import br.com.linkcom.lknfe.xml.nfe.Comb;
import br.com.linkcom.lknfe.xml.nfe.Compra;
import br.com.linkcom.lknfe.xml.nfe.DI;
import br.com.linkcom.lknfe.xml.nfe.Dest;
import br.com.linkcom.lknfe.xml.nfe.Det;
import br.com.linkcom.lknfe.xml.nfe.DetEvento;
import br.com.linkcom.lknfe.xml.nfe.DetExport;
import br.com.linkcom.lknfe.xml.nfe.DetPag;
import br.com.linkcom.lknfe.xml.nfe.Dup;
import br.com.linkcom.lknfe.xml.nfe.Emit;
import br.com.linkcom.lknfe.xml.nfe.EnderDest;
import br.com.linkcom.lknfe.xml.nfe.EnderEmit;
import br.com.linkcom.lknfe.xml.nfe.Entrega;
import br.com.linkcom.lknfe.xml.nfe.EnvEvento;
import br.com.linkcom.lknfe.xml.nfe.EnviNFe;
import br.com.linkcom.lknfe.xml.nfe.Evento;
import br.com.linkcom.lknfe.xml.nfe.ExportInd;
import br.com.linkcom.lknfe.xml.nfe.Exporta;
import br.com.linkcom.lknfe.xml.nfe.Fat;
import br.com.linkcom.lknfe.xml.nfe.ICMS;
import br.com.linkcom.lknfe.xml.nfe.ICMS00;
import br.com.linkcom.lknfe.xml.nfe.ICMS10;
import br.com.linkcom.lknfe.xml.nfe.ICMS20;
import br.com.linkcom.lknfe.xml.nfe.ICMS30;
import br.com.linkcom.lknfe.xml.nfe.ICMS40;
import br.com.linkcom.lknfe.xml.nfe.ICMS51;
import br.com.linkcom.lknfe.xml.nfe.ICMS60;
import br.com.linkcom.lknfe.xml.nfe.ICMS70;
import br.com.linkcom.lknfe.xml.nfe.ICMS90;
import br.com.linkcom.lknfe.xml.nfe.ICMSPart;
import br.com.linkcom.lknfe.xml.nfe.ICMSSN101;
import br.com.linkcom.lknfe.xml.nfe.ICMSSN102;
import br.com.linkcom.lknfe.xml.nfe.ICMSSN201;
import br.com.linkcom.lknfe.xml.nfe.ICMSSN202;
import br.com.linkcom.lknfe.xml.nfe.ICMSSN500;
import br.com.linkcom.lknfe.xml.nfe.ICMSSN900;
import br.com.linkcom.lknfe.xml.nfe.ICMSST;
import br.com.linkcom.lknfe.xml.nfe.ICMSTot;
import br.com.linkcom.lknfe.xml.nfe.ICMSUFDest;
import br.com.linkcom.lknfe.xml.nfe.II;
import br.com.linkcom.lknfe.xml.nfe.IPI;
import br.com.linkcom.lknfe.xml.nfe.IPINT;
import br.com.linkcom.lknfe.xml.nfe.IPITrib;
import br.com.linkcom.lknfe.xml.nfe.ISSQN;
import br.com.linkcom.lknfe.xml.nfe.ISSQNtot;
import br.com.linkcom.lknfe.xml.nfe.Ide;
import br.com.linkcom.lknfe.xml.nfe.Imposto;
import br.com.linkcom.lknfe.xml.nfe.ImpostoDevol;
import br.com.linkcom.lknfe.xml.nfe.InfAdic;
import br.com.linkcom.lknfe.xml.nfe.InfEvento;
import br.com.linkcom.lknfe.xml.nfe.InfNFe;
import br.com.linkcom.lknfe.xml.nfe.InfNFeSupl;
import br.com.linkcom.lknfe.xml.nfe.InfRespTec;
import br.com.linkcom.lknfe.xml.nfe.ListaDI;
import br.com.linkcom.lknfe.xml.nfe.ListaNFe;
import br.com.linkcom.lknfe.xml.nfe.ListaNFref;
import br.com.linkcom.lknfe.xml.nfe.ListaRastro;
import br.com.linkcom.lknfe.xml.nfe.Listaadi;
import br.com.linkcom.lknfe.xml.nfe.ListaautXML;
import br.com.linkcom.lknfe.xml.nfe.Listadet;
import br.com.linkcom.lknfe.xml.nfe.ListadetExport;
import br.com.linkcom.lknfe.xml.nfe.ListadetPag;
import br.com.linkcom.lknfe.xml.nfe.Listadup;
import br.com.linkcom.lknfe.xml.nfe.Listaevento;
import br.com.linkcom.lknfe.xml.nfe.Listavol;
import br.com.linkcom.lknfe.xml.nfe.Med;
import br.com.linkcom.lknfe.xml.nfe.NFe;
import br.com.linkcom.lknfe.xml.nfe.NFref;
import br.com.linkcom.lknfe.xml.nfe.PIS;
import br.com.linkcom.lknfe.xml.nfe.PISAliq;
import br.com.linkcom.lknfe.xml.nfe.PISNT;
import br.com.linkcom.lknfe.xml.nfe.PISOutr;
import br.com.linkcom.lknfe.xml.nfe.PISQtde;
import br.com.linkcom.lknfe.xml.nfe.Pag;
import br.com.linkcom.lknfe.xml.nfe.Prod;
import br.com.linkcom.lknfe.xml.nfe.Rastro;
import br.com.linkcom.lknfe.xml.nfe.RefECF;
import br.com.linkcom.lknfe.xml.nfe.RefNF;
import br.com.linkcom.lknfe.xml.nfe.RefNFP;
import br.com.linkcom.lknfe.xml.nfe.RetTrib;
import br.com.linkcom.lknfe.xml.nfe.Retirada;
import br.com.linkcom.lknfe.xml.nfe.Total;
import br.com.linkcom.lknfe.xml.nfe.Transp;
import br.com.linkcom.lknfe.xml.nfe.Transporta;
import br.com.linkcom.lknfe.xml.nfe.VeicTransp;
import br.com.linkcom.lknfe.xml.nfe.Vol;
import br.com.linkcom.mdfe.EnviMdfe;
import br.com.linkcom.mdfe.EventoMdfe;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Cfoptipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Configuracaotef;
import br.com.linkcom.sined.geral.bean.Configuracaotefadquirente;
import br.com.linkcom.sined.geral.bean.Contigencianfe;
import br.com.linkcom.sined.geral.bean.Declaracaoimportacao;
import br.com.linkcom.sined.geral.bean.Declaracaoimportacaoadicao;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeHistorico;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoautorizacao;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutocorrecao;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoduplicata;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitemdi;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoreferenciada;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.ArquivoMdfeSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.BandeiraCartaoNFe;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MeioPagamentoNFe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Notafiscalprodutocorrecaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.TipoIntegracaoNFe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontigencia;
import br.com.linkcom.sined.geral.bean.enumeration.Tiponotareferencia;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoMdfeService;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivonfService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.ConfiguracaotefService;
import br.com.linkcom.sined.geral.service.ContigencianfeService;
import br.com.linkcom.sined.geral.service.DeclaracaoimportacaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.ExpedicaoitemService;
import br.com.linkcom.sined.geral.service.GrupotributacaoService;
import br.com.linkcom.sined.geral.service.MdfeHistoricoService;
import br.com.linkcom.sined.geral.service.MdfeService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoautorizacaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutocorrecaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoduplicataService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoreferenciadaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.CancelamentoNfseBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ConferenciaDIFALBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.RegistrarCCeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.SelecionarConfiguracaoBean;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.EventoBean;
import br.com.linkcom.sined.util.CriptografiaUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.listener.QuartzContextListenner;
import br.com.linkcom.utils.NFCeUtil;
import br.com.linkcom.utils.StringUtils;

@Controller(path={"/faturamento/process/NotaFiscalEletronicaEstado", "/fiscal/process/NotaFiscalEletronicaEstado"}, authorizationModule=ProcessAuthorizationModule.class)
public class NotaFiscalEletronicaEstadoProcess extends MultiActionController{
	
	private NotaService notaService;
	private EmpresaService empresaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private ParametrogeralService parametrogeralService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private EnderecoService enderecoService;
	private NotafiscalprodutoduplicataService notafiscalprodutoduplicataService;
	private NotafiscalprodutoreferenciadaService notafiscalprodutoreferenciadaService;
	private ProdutoService produtoService;
	private ConfiguracaonfeService configuracaonfeService;
	private ArquivonfService arquivonfService;
	private ArquivonfnotaService arquivonfnotaService;
	private DeclaracaoimportacaoService declaracaoimportacaoService;
	private NotafiscalprodutocorrecaoService notafiscalprodutocorrecaoService;
	private GrupotributacaoService grupotributacaoService;
	private CategoriaService categoriaService;
	private NotafiscalprodutoautorizacaoService notafiscalprodutoautorizacaoService; 
	private ContigencianfeService contigencianfeService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService; 
	private ArquivoMdfeService arquivoMdfeService;
	private MdfeService mdfeService;
	private UfService ufService;
	private MunicipioService municipioService;
	private MdfeHistoricoService mdfeHistoricoService;
	private ExpedicaoitemService expedicaoItemService;
	private ConfiguracaotefService configuracaotefService;
	
	public void setConfiguracaotefService(ConfiguracaotefService configuracaotefService) {this.configuracaotefService = configuracaotefService;}
	public void setExpedicaoItemService(ExpedicaoitemService expedicaoItemService) {this.expedicaoItemService = expedicaoItemService;}
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {this.notafiscalprodutoitemService = notafiscalprodutoitemService;}
	public void setContigencianfeService(ContigencianfeService contigencianfeService) {this.contigencianfeService = contigencianfeService;}
	public void setNotafiscalprodutoautorizacaoService(NotafiscalprodutoautorizacaoService notafiscalprodutoautorizacaoService) {this.notafiscalprodutoautorizacaoService = notafiscalprodutoautorizacaoService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setNotafiscalprodutocorrecaoService(NotafiscalprodutocorrecaoService notafiscalprodutocorrecaoService) {this.notafiscalprodutocorrecaoService = notafiscalprodutocorrecaoService;}
	public void setDeclaracaoimportacaoService(DeclaracaoimportacaoService declaracaoimportacaoService) {this.declaracaoimportacaoService = declaracaoimportacaoService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setArquivonfService(ArquivonfService arquivonfService) {this.arquivonfService = arquivonfService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setNotafiscalprodutoduplicataService(NotafiscalprodutoduplicataService notafiscalprodutoduplicataService) {this.notafiscalprodutoduplicataService = notafiscalprodutoduplicataService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setNotafiscalprodutoreferenciadaService(NotafiscalprodutoreferenciadaService notafiscalprodutoreferenciadaService) {this.notafiscalprodutoreferenciadaService = notafiscalprodutoreferenciadaService;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setArquivoMdfeService(ArquivoMdfeService arquivoMdfeService) {this.arquivoMdfeService = arquivoMdfeService;}
	public void setMdfeService(MdfeService mdfeService) {this.mdfeService = mdfeService;}
	public void setUfService(UfService ufService) {this.ufService = ufService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setMdfeHistoricoService(MdfeHistoricoService mdfeHistoricoService) {this.mdfeHistoricoService = mdfeHistoricoService;}
	
	private EnviNFe createXmlEnvio(List<Notafiscalproduto> lista, WebRequestContext request, Configuracaonfe configuracaonfe, Contigencianfe contigencianfe, Boolean isNfe) throws NoSuchAlgorithmException {
		EnviNFe enviNFe = new EnviNFe();
		enviNFe.setVersao("4.00");
		enviNFe.setIndSinc(0);
		enviNFe.setIdLote(configuracaonfeService.getNextNumLoteNfe(configuracaonfe));
		
		ListaNFe listaNFe = new ListaNFe();
		enviNFe.setListaNFe(listaNFe);
		
		listaNFe.setListaNFe(this.createListaNFe(lista, request, configuracaonfe, contigencianfe, isNfe));
		
		return enviNFe;
	}
	
	private List<NFe> createListaNFe(List<Notafiscalproduto> lista, WebRequestContext request, Configuracaonfe configuracaonfe, Contigencianfe contigencianfe, Boolean isNfe) throws NoSuchAlgorithmException {
		
		List<NFe> listaNfe = new ArrayList<NFe>();
		for (Notafiscalproduto nota : lista) {
			listaNfe.add(this.makeNfeProduto(nota, request, configuracaonfe, contigencianfe, isNfe));
		}
		
		if(request.getMessages() != null && request.getMessages().length > 0){
			throw new SinedException();
		}
		
		return listaNfe;
	}
	
	private int modulo11(String chave){
		int[] pesos={4,3,2,9,8,7,6,5};  
		int somaPonderada = 0;  
		for (int i = 0; i < chave.length(); i++){
			somaPonderada += pesos[i%8]*(Integer.parseInt(chave.substring(i, i+1)));
		}  
		
		int resto = somaPonderada%11;
		
		if(resto == 0 || resto == 1) return 0;
		else return 11-resto;
	}
	
	private String addScape(String string){
		if(string == null) return null;
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			switch (string.charAt(i)) {
			case '"' :
				stringBuilder.append("&quot;");
				break;
			case '\'' :
				stringBuilder.append("&#39;");
				break;
			case '<' :
				stringBuilder.append("&lt;");
				break;
			case '>' :
				stringBuilder.append("&gt;");
				break;
			case '&' :
				stringBuilder.append("&amp;");
				break;
			default :
				stringBuilder.append(string.charAt(i));
			}
		}
		
		return Util.strings.tiraAcento(stringBuilder.toString().replace("\n", " ").replace("\r", " ")).trim();
	}
	
	private NFe makeNfeProduto(Notafiscalproduto nf, WebRequestContext request, Configuracaonfe configuracaonfe, Contigencianfe contigencianfe, Boolean isNfe) throws NoSuchAlgorithmException {
		
		boolean ambienteProducao = !configuracaonfe.getPrefixowebservice().name().endsWith("_HOM");
		boolean habilitarNfev4 = true;
		
		Notafiscalproduto nota = notafiscalprodutoService.loadForNfe(nf.getCdNota());
		
		Integer tpEmis = 1; // EMISS�O NORMAL
		if(contigencianfe != null && contigencianfe.getTipocontigencia() != null){
			if(contigencianfe.getTipocontigencia().equals(Tipocontigencia.SVAN)){
				tpEmis = 6; // EMISS�O DO AMBIENTE NACIONAL
			} else if(contigencianfe.getTipocontigencia().equals(Tipocontigencia.SVRS)){
				tpEmis = 7; // EMISS�O DO AMBIENTE DO RIO GRANDE DO SUL
			}
		}
		
		Empresa empresa = nota.getEmpresa();
		empresaService.setInformacoesPropriedadeRural(empresa);
		
		Endereco enderecoConfiguracao = configuracaonfe.getEndereco();
		Integer serienfe = nota.getSerienfe();
		Cliente cliente = nota.getCliente();
		Naturezaoperacao naturezaoperacao = nota.getNaturezaoperacao();
		Operacaonfe operacaonfe = nota.getOperacaonfe();
		String numeroNot = "N�mero da nota: ";
		if(nota.getNumero() !=null){
			numeroNot = numeroNot.concat(nota.getNumero());
		}
		Endereco enderecoCliente = null;
		if(nota.getEnderecoavulso() != null && nota.getEnderecoavulso() && nota.getEndereconota() != null && 
				nota.getEndereconota().getCdendereco() != null){
			enderecoCliente = enderecoService.loadEnderecoNota(nota.getEndereconota());
		}else if(nota.getEnderecoCliente() != null){
			enderecoCliente = enderecoService.loadEndereco(nota.getEnderecoCliente());
		}else if(isNfe){
			request.addError("Endere�o do cliente n�o cadastrado na nota. "+numeroNot);
			return null;
		}
		boolean isPessoaFisica = Tipopessoa.PESSOA_FISICA.equals(cliente.getTipopessoa());
		boolean isPessoaJuridica = Tipopessoa.PESSOA_JURIDICA.equals(cliente.getTipopessoa());

		if(isNfe && ((isPessoaFisica && cliente.getCpf() == null) ||
			(isPessoaJuridica && cliente.getCnpj() == null))){
			if(enderecoCliente != null && (enderecoCliente.getPais() == null || Pais.BRASIL.equals(enderecoCliente.getPais()))){
				request.addError("Para a nota fiscal com opera��o interna e interestadual � obrigat�rio o preenchimento do campo CNPJ / CPF do cadastro de cliente. "+numeroNot);
				return null;
			}
		}
		
		List<Categoria> listaCategoriaCliente = null;
		if(cliente != null){
			listaCategoriaCliente = categoriaService.findByPessoa(cliente);
		}
		
		List<Notafiscalprodutoitem> listaProdutos = nota.getListaItens();
		Fornecedor transportador = nota.getTransportador();
		Endereco enderTransportador = nota.getEnderecotransportador() != null ? enderecoService.loadEndereco(nota.getEnderecotransportador()) : null;
		String numeroStr = nota.getNumero();
		Integer numero = null;
		
		if(numeroStr == null || numeroStr.equals("")) {
			request.addError("N�mero n�o cadastrado. Nota sem n�mero");
			return null;
		}
		
		try{
			numero = Integer.parseInt(numeroStr);
		} catch (Exception e) {
			request.addError("N�mero do documento fiscal inv�lido. Nota " + numeroStr);
			return null;
		}
		
		if(enderecoConfiguracao == null){
			request.addError("Endere�o n�o cadastrado. Configura��o " + configuracaonfe.getDescricao() +numeroNot);
		} else {
			if(enderecoConfiguracao.getLogradouro() == null || enderecoConfiguracao.getLogradouro().equals("")) request.addError("Logradouro do endere�o n�o cadastrado. Configura��o " + configuracaonfe.getDescricao());
			if(enderecoConfiguracao.getNumero() == null || enderecoConfiguracao.getNumero().equals("")) request.addError("N�mero do endere�o n�o cadastrado. Configura��o " + configuracaonfe.getDescricao());
			if(enderecoConfiguracao.getBairro() == null || enderecoConfiguracao.getBairro().equals("")) request.addError("Bairro do endere�o n�o cadastrado. Configura��o " + configuracaonfe.getDescricao());
			if(enderecoConfiguracao.getMunicipio() == null) request.addError("Munic�pio do endere�o n�o cadastrado. Configura��o " + configuracaonfe.getDescricao());
			if(enderecoConfiguracao.getCep() == null || enderecoConfiguracao.getCep().getValue() == null || enderecoConfiguracao.getCep().getValue().equals("")) request.addError("CEP do endere�o n�o cadastrado. Configura��o " + configuracaonfe.getDescricao());
		}
		
		if(nota.getMunicipiogerador() == null) request.addError("Munic�pio gerador n�o cadastrado. Nota " + numero);
		if(nota.getNaturezaoperacao() == null || nota.getNaturezaoperacao().equals("")) request.addError("Natureza de opera��o n�o cadastrada. Nota " + numero);
		
		if(empresa == null) {
			request.addError("Empresa n�o cadastrada. Nota " + numero);
		} else {
			if(configuracaonfe.getUf() == null) request.addError("UF de emiss�o da NF-e n�o cadastrada. Empresa " + configuracaonfe.getDescricao());
			if(configuracaonfe.getModelonfe() == null || configuracaonfe.getModelonfe().equals("")) request.addError("Modelo NF-e n�o cadastrada. Configura��o " + configuracaonfe.getDescricao());
			if(serienfe == null) request.addError("S�rie NF-e n�o cadastrada. Configura��o " + configuracaonfe.getDescricao());
			if(configuracaonfe.getInscricaoestadual() == null || configuracaonfe.getInscricaoestadual().equals("")) request.addError("Inscri��o estadual n�o cadastrada. Configura��o " + configuracaonfe.getDescricao());
			if(configuracaonfe.getCrt() == null) request.addError("C�digo de regime tribut�rio n�o cadastrado. Configura��o " + configuracaonfe.getDescricao());
		}
		
		boolean exterior = false;
		if(enderecoCliente != null){
			if(enderecoCliente.getPais() != null && 
					enderecoCliente.getPais().getNome() != null &&
					!enderecoCliente.getPais().getNome().trim().toUpperCase().equals("BRASIL")){
				exterior = true;
			}
			
			if(enderecoCliente.getLogradouro() == null || enderecoCliente.getLogradouro().equals("")) request.addError("Logradouro do endere�o n�o cadastrado. Cliente " + cliente.getNome());
			if(enderecoCliente.getNumero() == null || enderecoCliente.getNumero().equals("")) request.addError("N�mero do endere�o n�o cadastrado. Cliente " + cliente.getNome());
			if(enderecoCliente.getBairro() == null || enderecoCliente.getBairro().equals("")) request.addError("Bairro do endere�o n�o cadastrado. Cliente " + cliente.getNome());
			if(!exterior && enderecoCliente.getMunicipio() == null) request.addError("Munic�pio do endere�o n�o cadastrado. Cliente " + cliente.getNome());
			if(!exterior && (enderecoCliente.getCep() == null || enderecoCliente.getCep().getValue() == null || enderecoCliente.getCep().getValue().equals(""))) request.addError("CEP do endere�o n�o cadastrado. Cliente " + cliente.getNome());
			if(exterior && isPessoaFisica && Localdestinonfe.INTERNA.equals(nota.getLocaldestinonfe()) && Util.strings.isEmpty(cliente.getPassaporte())) request.addError("Para cliente turista estrangeiro ser� necess�rio que informe um documento de identifica��o no campo passaporte. Cliente " + cliente.getNome());
		}
		
//		VALIDA��O FEITA NO VALIDATEBEAN DO CRUD DE NOTAFISCALPRODUTO
		boolean obrigacaoDocumentoReferenciado = false;
		Finalidadenfe finalidadenfe = nota.getFinalidadenfe();
		boolean devolucao = finalidadenfe.equals(Finalidadenfe.DEVOLUCAO);
		if(devolucao){
			obrigacaoDocumentoReferenciado = true;
		}
		List<String> cfopIgnoraDocReferenciado = Arrays.asList(new String[]{
			"1201", "1202", "1410", "1411", "5921", "6921"
		});

		List<Material> listaMaterialCodigoEnquadramentoIpiInvalido = new ArrayList<Material>();
		for (Notafiscalprodutoitem item : listaProdutos) {
			if(item.getTipocobrancacofins() == null) request.addError("COFINS n�o cadastrado. " + getNotaItemForErro(numero, item));
			if(item.getTipocobrancapis() == null) request.addError("PIS n�o cadastrado. " + getNotaItemForErro(numero, item));
			if(item.getTributadoicms() != null && item.getTributadoicms() && item.getTipocobrancaicms() == null) request.addError("ICMS n�o cadastrado. " + getNotaItemForErro(numero, item));
			if((item.getNcmcompleto() == null || item.getNcmcompleto().trim().equals("")) && item.getNcmcapitulo() == null) request.addError("NCM n�o cadastrado. " + getNotaItemForErro(numero, item));
			if(item.getCfop() == null || item.getCfop().getCodigo() == null) request.addError("CFOP n�o cadastrado. " + getNotaItemForErro(numero, item));
			
			if(obrigacaoDocumentoReferenciado && item.getCfop() != null && item.getCfop().getCodigo() != null && cfopIgnoraDocReferenciado.contains(item.getCfop().getCodigo())){
				obrigacaoDocumentoReferenciado = false;
			}
			
			if(isNfe && item.getMotivodesoneracaoicms() != null && 
					item.getMotivodesoneracaoicms().equals(Motivodesoneracaoicms.SUFRAMA) &&
					org.apache.commons.lang.StringUtils.isBlank(cliente.getInscricaosuframa())){
				request.addError("Para emiss�o de nota fiscal com motivo de desonera��o SUFRAMA � obrigat�rio informar o campo Inscri��o SUFRAMA no cadastro do cliente. " + getNotaItemForErro(numero, item));
			}
			
			if(item.getCfop() != null){
				if(exterior){
					if(Localdestinonfe.INTERNA.equals(nota.getLocaldestinonfe()) && !Cfopescopo.ESTADUAL.equals(item.getCfop().getCfopescopo())){
						request.addError("Para emiss�o de nota fiscal para turista estrangeiro � obrigat�rio informar CFOP de opera��o interna nos itens da nota. " + getNotaItemForErro(numero, item));
					}
					if(Localdestinonfe.EXTERIOR.equals(nota.getLocaldestinonfe()) && !Cfopescopo.INTERNACIONAL.equals(item.getCfop().getCfopescopo())){
						request.addError("Para emiss�o de nota fiscal para cliente estrangeiro � obrigat�rio informar CFOP de escopo internacional nos itens da nota. " + getNotaItemForErro(numero, item));
					}
				}
				
				if(Cfoptipo.SAIDA.equals(item.getCfop().getCfoptipo())){
					boolean drawback = item.getDrawbackexportacao() != null && !item.getDrawbackexportacao().equals("") ||
										((item.getDrawbackexportacao() == null || item.getDrawbackexportacao().equals("")) &&
											 !"3127".equals(item.getCfop().getCodigo()) &&
											 !"3211".equals(item.getCfop().getCodigo()) &&
											 !"7127".equals(item.getCfop().getCodigo()) && 
											 !"7211".equals(item.getCfop().getCodigo()));
					boolean exportacaoindireta = item.getNumregistroexportacao() != null && !item.getNumregistroexportacao().equals("") &&
													item.getChaveacessoexportacao() != null && !item.getChaveacessoexportacao().equals("") &&
													item.getQuantidadeexportacao() != null;
					if(!drawback){
						request.addError("CFOP iniciados com 7 � obrigat�rio o envio de informa��es de exporta��o (N�mero de Drawback). " + getNotaItemForErro(numero, item));
					}
					if(!drawback && !exportacaoindireta){
						request.addError("CFOP iniciados com 7 � obrigat�rio o envio de informa��es de exporta��o do item (N�mero de registro / Chave de acesso de NF-e / Qtde. exportada). " + getNotaItemForErro(numero, item));
					}
				}
				if(Cfoptipo.ENTRADA.equals(item.getCfop().getCfoptipo()) 
						&& !"3201".equals(item.getCfop().getCodigo())
						&& !"3202".equals(item.getCfop().getCodigo())
						&& !"3503".equals(item.getCfop().getCodigo())
						&& !"3553".equals(item.getCfop().getCodigo())){
					if(Cfopescopo.INTERNACIONAL.equals(item.getCfop().getCfopescopo()) &&
						(!Boolean.TRUE.equals(item.getDadosimportacao()) ||
						item.getListaNotafiscalprodutoitemdi() == null || 
						item.getListaNotafiscalprodutoitemdi().size() == 0)){
						request.addError("CFOP iniciados com 3 � obrigat�rio o envio de informa��es de importa��o. " + getNotaItemForErro(numero, item));
					}
				}
			}
			
			if(item.getTipocobrancaipi() != null){
				Integer codigoenquadramento = null;
				try {
					codigoenquadramento = org.apache.commons.lang.StringUtils.isNotBlank(item.getCodigoenquadramentoipi()) ? Integer.parseInt(item.getCodigoenquadramentoipi()) : 999; 
				} catch (Exception e) {
					request.addError("Erro ao converter c�digo de enquadramento " + item.getCodigoenquadramentoipi() + ". " + getNotaItemForErro(numero, item));
				}
				if(codigoenquadramento != null){
					if(((Tipocobrancaipi.ENTRADA_ISENTA.equals(item.getTipocobrancaipi()) || 
							Tipocobrancaipi.SAIDA_ISENTA.equals(item.getTipocobrancaipi())) &&
							(codigoenquadramento < 301 || codigoenquadramento > 399)) 
							||
							((Tipocobrancaipi.ENTRADA_IMUNE.equals(item.getTipocobrancaipi()) || 
									Tipocobrancaipi.SAIDA_IMUNE.equals(item.getTipocobrancaipi())) &&
									(codigoenquadramento < 001 || codigoenquadramento > 99))
							||
							((Tipocobrancaipi.ENTRADA_SUSPENSAO.equals(item.getTipocobrancaipi()) || 
									Tipocobrancaipi.SAIDA_SUSPENSAO.equals(item.getTipocobrancaipi())) &&
									(codigoenquadramento < 101 || codigoenquadramento > 199))
							||
							((Tipocobrancaipi.ENTRADA_TRIBUTADA_ALIQUOTA_ZERO.equals(item.getTipocobrancaipi()) || 
									Tipocobrancaipi.ENTRADA_NAO_TRIBUTADA.equals(item.getTipocobrancaipi()) ||
									Tipocobrancaipi.SAIDA_TRIBUTADA_ALIQUOTA_ZERO.equals(item.getTipocobrancaipi()) || 
									Tipocobrancaipi.SAIDA_NAO_TRIBUTADA.equals(item.getTipocobrancaipi())) &&
									!codigoenquadramento.equals(999))){
						listaMaterialCodigoEnquadramentoIpiInvalido.add(item.getMaterial());
					}
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaMaterialCodigoEnquadramentoIpiInvalido)){
			StringBuilder mensagemCodigoEnqInvalido = new StringBuilder("Verifique o C�digo de Enquadramento do IPI. O mesmo est� incompat�vel com o CST do IPI no(s) item(ns): ");
			for(Material material : listaMaterialCodigoEnquadramentoIpiInvalido){
				mensagemCodigoEnqInvalido.append(material.getIdentificacaoOuCdmaterial()).append(",");
			}
			request.addError(mensagemCodigoEnqInvalido.substring(0, mensagemCodigoEnqInvalido.length()-1));
		}
		
		List<Notafiscalprodutoreferenciada> listaReferencia = notafiscalprodutoreferenciadaService.findByNotafiscalproduto(nota);
		if(obrigacaoDocumentoReferenciado && (listaReferencia == null || listaReferencia.size() == 0)){
			request.addError("Para nota com finalidade de devolu��o de mercadoria e o CFOP seja diferente de (" + CollectionsUtil.concatenate(cfopIgnoraDocReferenciado, ", ") + ") � obrigat�rio informar os documentos referenciados.");
		}
		
		List<Notafiscalprodutoduplicata> listaDuplicata = notafiscalprodutoduplicataService.findByNotafiscalproduto(nota);
		Configuracaotef configuracaotef = configuracaotefService.loadByEmpresa(empresa);
		if(configuracaotef != null){
			if((isNfe && configuracaotef.getObrigarpagamentocartaonfe() != null && configuracaotef.getObrigarpagamentocartaonfe()) || 
					(!isNfe && configuracaotef.getObrigarpagamentocartaonfce() != null && configuracaotef.getObrigarpagamentocartaonfce())){
				for (Notafiscalprodutoduplicata notafiscalprodutoduplicata : listaDuplicata) {
					Documentotipo documentotipo = notafiscalprodutoduplicata.getDocumentotipo();
					if(documentotipo != null && 
							documentotipo.getMeiopagamentonfe() != null && (documentotipo.getMeiopagamentonfe().equals(MeioPagamentoNFe.CARTAO_CREDITO) || documentotipo.getMeiopagamentonfe().equals(MeioPagamentoNFe.CARTAO_DEBITO)) &&
							(org.apache.commons.lang.StringUtils.isBlank(notafiscalprodutoduplicata.getAdquirente()) || 
								org.apache.commons.lang.StringUtils.isBlank(notafiscalprodutoduplicata.getAutorizacao()) ||
								org.apache.commons.lang.StringUtils.isBlank(notafiscalprodutoduplicata.getBandeira()))){
						request.addError("� obrigat�rio as informa��es de TEF para os pagamentos dessa nota. Nota " + numero);
					}
				}
			}
		}
		
		if(request.getMessages() != null && request.getMessages().length > 0){
			return null;
		}
		
		NFe nfe = new NFe();
		
		InfNFe infNFe = new InfNFe(habilitarNfev4);
		nfe.setInfNFe(infNFe);
		
		Ide ide = new Ide(habilitarNfev4);
		infNFe.setIde(ide);
		
		Integer nextNumNfe = configuracaonfeService.getNextNumNfe(configuracaonfe);
		
		String chaveAcesso = 
			StringUtils.stringCheia(StringUtils.soNumero(configuracaonfe.getUf().getCdibge().toString()), "0", 2, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(new SimpleDateFormat("yyMM").format(nota.getDtEmissao())), "0", 4, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa()), "0", 14, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(configuracaonfe.getModelonfe()), "0", 2, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(serienfe.toString()), "0", 3, false) + 
			StringUtils.stringCheia(StringUtils.soNumero(numero.toString()), "0", 9, false) + 
			tpEmis +
			StringUtils.stringCheia(StringUtils.soNumero(nextNumNfe.toString()), "0", 8, false); 
		
		int dvChaveAcesso = modulo11(chaveAcesso);
		
		nf.setChaveAcesso(chaveAcesso + dvChaveAcesso);
		
		infNFe.setId("NFe" + chaveAcesso + dvChaveAcesso); 
		
		ide.setCNF(nextNumNfe); 
		ide.setCDV(dvChaveAcesso);
		
		infNFe.setVersao(habilitarNfev4 ? "4.00" : "3.10");
		
		if(habilitarNfev4 && nota.getFormapagamentonfe() != null && nota.getFormapagamentonfe().equals(Formapagamentonfe.OUTROS)){
			throw new SinedException("N�o � poss�vel utilizar essa forma de pagamento. A op��o permanece apenas para fins de compatibilidade com notas fiscais anteriores a vers�o 4.0 da NFe.");
		}
		
		ide.setCMunFG(nota.getMunicipiogerador() != null ? nota.getMunicipiogerador().getCdibge() : null);
		ide.setCUF(configuracaonfe != null && configuracaonfe.getUf() != null ? configuracaonfe.getUf().getCdibge() : null); 
		ide.setNatOp(addScape(nota.getNaturezaoperacao()!=null ? nota.getNaturezaoperacao().getDescricao() : ""));
		ide.setIndPag(nota.getFormapagamentonfe() != null ? nota.getFormapagamentonfe().getValue() : Formapagamentonfe.A_VISTA.getValue());
		ide.setMod(configuracaonfe != null ? configuracaonfe.getModelonfe() : null); 
		ide.setSerie(serienfe);
		ide.setNNF(numero);
		
		try{
			String dhEmiStr = SinedDateUtils.toString(nota.getDtEmissao(), "dd/MM/yyyy");
			if(nota.getHremissao() != null){
				dhEmiStr = dhEmiStr + " " + nota.getHremissao() + ":00"; 
			} else {
				dhEmiStr = dhEmiStr + " 00:00:00"; 
			}
			Timestamp dhEmi = SinedDateUtils.stringToTimestamp(dhEmiStr, "dd/MM/yyyy HH:mm:ss");
			ide.setDhEmi(dhEmi);
		} catch (Exception e) {
			throw new SinedException("Erro ao transformar a hora da emissao da nota.", e);
		}
		
		if(isNfe){
			ide.setDhSaiEnt(nota.getDatahorasaidaentrada());
		}
		ide.setTpNF(nota.getTipooperacaonota().getValue());
		ide.setIdDest(nota.getLocaldestinonfe() != null ? nota.getLocaldestinonfe().getCdnfe() : null);
		ide.setIndFinal(nota.getOperacaonfe() != null ? nota.getOperacaonfe().getValue() : null);
		ide.setIndPres(nota.getPresencacompradornfe() != null ? nota.getPresencacompradornfe().getCdnfe() : null);
		if(isNfe){
			ide.setTpImp(1);
		} else {
			ide.setTpImp(4);
		}
		ide.setTpEmis(tpEmis);
		ide.setTpAmb(ambienteProducao ? 1 : 2);
		ide.setFinNFe(finalidadenfe.getCdnfe());
		ide.setProcEmi(0);
		ide.setVerProc("W3erp 2.0");
		
		InfNFeSupl infNFeSupl = new InfNFeSupl();
		
		if (BooleanUtils.isFalse(isNfe) && (tpEmis != 6 && tpEmis != 7)) {
			String urlQRCode = NFCeUtil.getUrlQRCode(configuracaonfe.getUf().getSigla(), ambienteProducao);
			String urlConsulta = NFCeUtil.getUrlConsulta(configuracaonfe.getUf().getSigla(), ambienteProducao);
			String idToken = configuracaonfe.getIdToken();
			String csc = configuracaonfe.getCsc();
			
			if (org.apache.commons.lang.StringUtils.isNotEmpty(urlQRCode) && 
					org.apache.commons.lang.StringUtils.isNotEmpty(idToken) &&
					org.apache.commons.lang.StringUtils.isNotEmpty(csc)) {
				infNFeSupl.setQrCode(NFCeUtil.getCodeQRCode(nf.getChaveAcesso(), ambienteProducao ? "1" : "2", idToken, csc, urlQRCode));
			}
			if (org.apache.commons.lang.StringUtils.isNotEmpty(urlConsulta)){
				infNFeSupl.setUrlChave(urlConsulta);
			}
		}
		
		nfe.setInfNFeSupl(infNFeSupl);
		
		if(contigencianfe != null){
			ide.setDhCont(contigencianfe.getDtentrada());
			ide.setXJust(contigencianfe.getJustificativa());
		}
		
		List<Notafiscalprodutoautorizacao> listaAutorizacao = notafiscalprodutoautorizacaoService.findByNotafiscalproduto(nota);
		if(listaAutorizacao != null && listaAutorizacao.size() > 0){
			List<AutXML> listaAutXML = new ArrayList<AutXML>();
			AutXML autXML;
			for (Notafiscalprodutoautorizacao notafiscalprodutoautorizacao : listaAutorizacao) {
				if(notafiscalprodutoautorizacao.getTipopessoa() != null && (notafiscalprodutoautorizacao.getCnpj() != null || notafiscalprodutoautorizacao.getCpf() != null)){
					autXML = new AutXML();
					
					if(notafiscalprodutoautorizacao.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA) && notafiscalprodutoautorizacao.getCpf() != null){
						autXML.setCPF(notafiscalprodutoautorizacao.getCpf().getValue());
					} else if(notafiscalprodutoautorizacao.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA) && notafiscalprodutoautorizacao.getCnpj() != null){
						autXML.setCNPJ(notafiscalprodutoautorizacao.getCnpj().getValue());
					}
					
					listaAutXML.add(autXML);
				}
			}
			
			ListaautXML listaautXML2 = new ListaautXML();
			listaautXML2.setListaautXML(listaAutXML);
			infNFe.setListaautXML(listaautXML2);
		}
		
		if(listaReferencia != null && listaReferencia.size() > 0){
			
			List<NFref> listaRef = new ArrayList<NFref>();
			NFref nfref;
			RefNF refNF;
			RefNFP refNFP;
			RefECF refECF;
			Tiponotareferencia tipo;
			String mesano;
			String[] mesanoarray;
			for (Notafiscalprodutoreferenciada nfpr : listaReferencia) {
				nfref = new NFref();
				tipo = nfpr.getTiponotareferencia();
				
				if(tipo.equals(Tiponotareferencia.NF_E)){
					nfref.setRefNFe(nfpr.getChaveacesso());
				} else if(tipo.equals(Tiponotareferencia.NF)){
					refNF = new RefNF();
					nfref.setRefNF(refNF);
					
					mesano = nfpr.getMesanoemissao();
					mesanoarray = mesano.split("/");
					
					refNF.setCNPJ(nfpr.getCnpj().getValue());
					refNF.setCUF(nfpr.getUf().getCdibge());
					refNF.setMod(nfpr.getModeloNFe().getCodigoNFe());
					refNF.setNNF(Integer.parseInt(nfpr.getNumero()));
					refNF.setSerie(Integer.parseInt(nfpr.getSerie()));
					refNF.setAAMM(mesanoarray[1].substring(2, mesanoarray[1].length()) + mesanoarray[0]);
				} else if(tipo.equals(Tiponotareferencia.NF_PRODUTOR)){
					refNFP = new RefNFP();
					nfref.setRefNFP(refNFP);
					
					mesano = nfpr.getMesanoemissao();
					mesanoarray = mesano.split("/");
					
					refNFP.setIE(nfpr.getIe());
					if(nfpr.getCpf() != null) refNFP.setCPF(nfpr.getCpf().getValue());
					if(nfpr.getCnpj() != null) refNFP.setCNPJ(nfpr.getCnpj().getValue());
					refNFP.setCUF(nfpr.getUf().getCdibge());
					refNFP.setMod(nfpr.getModeloNFe().getCodigoNFe());
					refNFP.setNNF(Integer.parseInt(nfpr.getNumero()));
					refNFP.setSerie(Integer.parseInt(nfpr.getSerie()));
					refNFP.setAAMM(mesanoarray[1].substring(2, mesanoarray[1].length()) + mesanoarray[0]);
				} else if(tipo.equals(Tiponotareferencia.CT_E)){
					nfref.setRefCTe(nfpr.getChaveacesso());
				} else if(tipo.equals(Tiponotareferencia.CUPOM_FISCAL)){
					refECF = new RefECF();
					nfref.setRefECF(refECF);
					
					refECF.setMod(nfpr.getModeloNFe().getCodigoNFe());
					refECF.setNCOO(nfpr.getNumero());
					refECF.setNECF(nfpr.getSerie());
				}
				
				listaRef.add(nfref);
			}
			
			ListaNFref listaNFref = new ListaNFref();
			listaNFref.setListaNFref(listaRef);
			ide.setListaNFref(listaNFref);
		}
		
		if(empresa != null){
			Emit emit = new Emit();
			infNFe.setEmit(emit);
			
			if(Boolean.TRUE.equals(empresa.getPropriedadeRural())){
				emit.setCPF(empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa());
			}else {
				emit.setCNPJ(empresa.getCnpj().getValue());
			}
			emit.setXNome(addScape(empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa()));
			emit.setXFant(addScape(empresa.getNomeFantasiaProprietarioPrincipalOuEmpresa()));
			
			EnderEmit enderEmit = new EnderEmit();
			emit.setEnderEmit(enderEmit);
			enderEmit.setXLgr(addScape(enderecoConfiguracao.getLogradouro()));
			enderEmit.setNro(addScape(enderecoConfiguracao.getNumero()));
			enderEmit.setXCpl(addScape(enderecoConfiguracao.getComplemento()));
			enderEmit.setXBairro(addScape(enderecoConfiguracao.getBairro()));
			enderEmit.setCMun(enderecoConfiguracao.getMunicipio().getCdibge());
			enderEmit.setXMun(addScape(enderecoConfiguracao.getMunicipio().getNome()));
			enderEmit.setUF(enderecoConfiguracao.getMunicipio().getUf().getSigla().toUpperCase());
			enderEmit.setCEP(enderecoConfiguracao.getCep().getValue());
			enderEmit.setCPais("1058");
			enderEmit.setXPais("BRASIL");
			
			enderEmit.setFone(configuracaonfe.getTelefone() != null ? StringUtils.soNumero(configuracaonfe.getTelefone()) : null);
			
			emit.setIE(StringUtils.soNumero(configuracaonfe.getInscricaoestadual()));
			if(configuracaonfe.getInscricaoestadualst() != null && Boolean.TRUE.equals(nota.getRetencaoICMSSTUFdestino())) emit.setIEST(StringUtils.soNumero(configuracaonfe.getInscricaoestadualst()));
			
			boolean hasServico = false;
			for (Notafiscalprodutoitem it : listaProdutos) {
				if(it.getTributadoicms() == null || !it.getTributadoicms()) hasServico = true;
			}
			
			if(hasServico){
				if(configuracaonfe.getInscricaomunicipal() != null){
					if("ISENTA".equalsIgnoreCase(configuracaonfe.getInscricaomunicipal())){
						emit.setIM(configuracaonfe.getInscricaomunicipal());
					}else {
						emit.setIM(StringUtils.soNumero(configuracaonfe.getInscricaomunicipal()));
					}
				}
				if(nota.getCodigocnae() != null) emit.setCNAE(StringUtils.soNumero(nota.getCodigocnae().getCnae()));
			}
			
			emit.setCRT(configuracaonfe.getCrt() != null ? configuracaonfe.getCrt().getCdnfe() : null);
		}
		
		boolean hasCliente = true;
		if(isPessoaFisica && (cliente.getCpf() == null || cliente.getCpf().getValue() == null || cliente.getCpf().getValue().equals(""))){
			hasCliente = false;
		} if(isPessoaJuridica && (cliente.getCnpj() == null || cliente.getCnpj().getValue() == null || cliente.getCnpj().getValue().equals(""))){
			hasCliente = false;
		}
		
		if(isNfe || hasCliente){
			Dest dest = new Dest();
			infNFe.setDest(dest);
			
			if(isPessoaFisica){
				if(cliente.getCpf() == null || cliente.getCpf().getValue() == null || cliente.getCpf().getValue().equals("")){
					if(exterior){
						if(Util.strings.isNotEmpty(cliente.getPassaporte())){
							dest.setIdEstrangeiro(cliente.getPassaporte());
							if((cliente.getPassaporte().length() < 5 || cliente.getPassaporte().length() > 20)){
								request.addError("O Passaporte no cadastro do cliente dever� ter de 5 a 20 caracteres. Cliente " + cliente.getNome());
							}
						}
					} else {
						request.addError("CPF n�o cadastrado. Cliente " + cliente.getNome());
					}
				} else { 
					dest.setCPF(cliente.getCpf().getValue());
				}
			} else if(isPessoaJuridica){
				if(cliente.getCnpj() != null){
					dest.setCNPJ(cliente.getCnpj().getValue());
				}
			} else { 
				request.addError("Tipo de pessoa de cliente inv�lido. Nota " + numero);
			}
	
			String nomeHomologacao = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
			if(ambienteProducao){
				dest.setXNome(addScape(nota.getRazaosocialalternativaOrRazaosocial()));
			} else {
				dest.setXNome(nomeHomologacao);
			}
			dest.setEmail(addScape(cliente.getEmail()));
			dest.setiM(cliente.getInscricaomunicipal());
			if(isNfe){
				dest.setISUF(cliente.getInscricaosuframa());
			}
			
			
			String ieDest = enderecoCliente != null && enderecoCliente.getInscricaoestadual() != null && !enderecoCliente.getInscricaoestadual().trim().equals("") ? enderecoCliente.getInscricaoestadual() : 
				(cliente != null && cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : null);
			Contribuinteicmstipo contribuinteicmstipo = cliente.getContribuinteicmstipo();
			
			if(nota.getLocaldestinonfe() != null && nota.getLocaldestinonfe().equals(Localdestinonfe.EXTERIOR)){
				dest.setIndIEDest(Contribuinteicmstipo.NAO_CONTRIBUINTE.getCdnfe());
			} else if(exterior && Localdestinonfe.INTERNA.equals(nota.getLocaldestinonfe())){
				dest.setIndIEDest(Contribuinteicmstipo.NAO_CONTRIBUINTE.getCdnfe());
			} else if(contribuinteicmstipo != null){
				dest.setIndIEDest(contribuinteicmstipo.getCdnfe());
				dest.setIE(ieDest != null ? StringUtils.soNumero(ieDest) : null);
			} else {
				if(ieDest != null && !ieDest.trim().equals("") && !ieDest.trim().toUpperCase().equals("ISENTO")){
					dest.setIndIEDest(Contribuinteicmstipo.CONTRIBUINTE.getCdnfe());
					dest.setIE(ieDest != null ? StringUtils.soNumero(ieDest) : null);
				} else if(ieDest != null && !ieDest.trim().equals("") && ieDest.trim().toUpperCase().equals("ISENTO")){
					dest.setIndIEDest(Contribuinteicmstipo.ISENTO.getCdnfe());
				} else {
					dest.setIndIEDest(Contribuinteicmstipo.NAO_CONTRIBUINTE.getCdnfe());
				}
			}
			
			if (enderecoCliente != null) {
				EnderDest enderDest = new EnderDest();
				dest.setEnderDest(enderDest);
			
				enderDest.setXLgr(addScape(enderecoCliente.getLogradouro()));
				enderDest.setNro(addScape(enderecoCliente.getNumero()));
				enderDest.setXCpl(addScape(enderecoCliente.getComplemento()));
				enderDest.setXBairro(addScape(enderecoCliente.getBairro()));
		
				if(exterior){
					enderDest.setCMun("9999999");
					enderDest.setXMun("Exterior");
					enderDest.setUF("EX");
					enderDest.setCPais(enderecoCliente.getPais().getCdbacen());
					enderDest.setXPais(enderecoCliente.getPais().getNome().trim().toUpperCase());
				} else {
					enderDest.setCMun(enderecoCliente.getMunicipio() != null ? enderecoCliente.getMunicipio().getCdibge() : null);
					enderDest.setXMun(enderecoCliente.getMunicipio() != null ? addScape(enderecoCliente.getMunicipio().getNome()) : null);
					enderDest.setUF(enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null ? enderecoCliente.getMunicipio().getUf().getSigla().toUpperCase() : null);
					enderDest.setCEP(enderecoCliente.getCep() != null ? enderecoCliente.getCep().getValue() : null);
					enderDest.setCPais("1058");
					enderDest.setXPais("BRASIL");
				}
				enderDest.setFone(StringUtils.soNumero(nota.getTelefoneCliente()));
			} else {
				dest.setIsNfe(Boolean.FALSE);
			}
		}
		
		if(nota.getEnderecodiferenteretirada() != null && nota.getEnderecodiferenteretirada()){
			Retirada retirada = new Retirada();
			infNFe.setRetirada(retirada);
			
			if(nota.getTipopessoaretirada().equals(Tipopessoa.PESSOA_FISICA)){
				retirada.setCPF(nota.getCpfretirada().getValue());
			} else {
				retirada.setCNPJ(nota.getCnpjretirada().getValue());
			}
			retirada.setXLgr(addScape(nota.getLogradouroretirada()));
			retirada.setNro(addScape(nota.getNumeroretirada()));
			retirada.setXCpl(addScape(nota.getComplementoretirada()));
			retirada.setXBairro(addScape(nota.getBairroretirada()));
			retirada.setCMun(nota.getMunicipioretirada().getCdibge());
			retirada.setXMun(addScape(nota.getMunicipioretirada().getNome()));
			retirada.setUF(nota.getMunicipioretirada().getUf().getSigla());
			retirada.setcPais(nota.getPaisRetirada() != null ? nota.getPaisRetirada().getCdbacen() : null);
			retirada.setxPais(nota.getPaisRetirada() != null ? nota.getPaisRetirada().getNome() : null);
			retirada.setIE(nota.getInscricaoEstadualRetirada());
			retirada.setEmail(nota.getEmailRetirada());
			retirada.setxNome(nota.getFornecedorRetirada() != null ? nota.getFornecedorRetirada().getRazaoSocialOrNomeByTipopessoa() : null);
			retirada.setFone(nota.getTelefoneRetirada() != null ? nota.getTelefoneRetirada() : null);
		}
		
		if(nota.getEnderecodiferenteentrega() != null && nota.getEnderecodiferenteentrega()){
			Entrega entrega = new Entrega();
			infNFe.setEntrega(entrega);
			
			if(nota.getTipopessoaentrega().equals(Tipopessoa.PESSOA_FISICA)){
				entrega.setCPF(nota.getCpfentrega().getValue());
			} else {
				entrega.setCNPJ(nota.getCnpjentrega().getValue());
			}
			entrega.setXLgr(addScape(nota.getLogradouroentrega()));
			entrega.setNro(addScape(nota.getNumeroentrega()));
			entrega.setXCpl(addScape(nota.getComplementoentrega()));
			entrega.setXBairro(addScape(nota.getBairroentrega()));
			entrega.setCMun(nota.getMunicipioentrega().getCdibge());
			entrega.setXMun(addScape(nota.getMunicipioentrega().getNome()));
			entrega.setUF(nota.getMunicipioentrega().getUf().getSigla());
			entrega.setcPais(nota.getPaisEntrega() != null ? nota.getPaisEntrega().getCdbacen() : null);
			entrega.setxPais(nota.getPaisEntrega() != null ? nota.getPaisEntrega().getNome() : null);
			entrega.setIE(nota.getInscricaoEstadualEntrega());
			entrega.setEmail(nota.getEmailEntrega());
			entrega.setxNome(nota.getClienteEntrega() != null ? nota.getClienteEntrega().getRazaoOrNomeByTipopessoa() : null);
			entrega.setFone(nota.getTelefoneEntrega() != null ? nota.getTelefoneEntrega() : null);
		}
		
		
		Listadet listadet = new Listadet();
		infNFe.setListadet(listadet);
		
		List<Det> listaDet = new ArrayList<Det>();
		listadet.setListadet(listaDet);
		
		Det det;
		Prod prod;
		Imposto imposto;
		ICMS icms;
		ICMS00 icms00;
		ICMS10 icms10;
		ICMS20 icms20;
		ICMS30 icms30;
		ICMS40 icms40;
		ICMS51 icms51;
		ICMS60 icms60;
		ICMS70 icms70;
		ICMS90 icms90;
		ICMSPart icmsPart;
		ICMSST icmsST;
		ICMSSN101 icmssn101;
		ICMSSN102 icmssn102;
		ICMSSN201 icmssn201;
		ICMSSN202 icmssn202;
		ICMSSN500 icmssn500;
		ICMSSN900 icmssn900;
		IPI ipi;
		IPITrib ipiTrib;
		IPINT ipint;
		PIS pis;
		PISAliq pisAliq;
		PISNT pisnt;
		PISQtde pisqtde;
		PISOutr pisoutr;
		COFINSQtde cofinsqtde;
		COFINSOutr cofinsoutr;
		COFINS cofins;
		COFINSAliq cofinsAliq;
		COFINSNT cofinsnt;
		ISSQN issqn;
		int nItem = 1;
		Tipocobrancaicms tipocobrancaicms;
		Tipocobrancaipi tipocobrancaipi;
		Tipocobrancapis tipocobrancapis;
		Tipocobrancacofins tipocobrancacofins;
		Comb comb;
		CIDE cide;
		
		boolean haveServico = false;
		boolean exibirSTJaPaga = false;
		Money totalBCSTJaPaga = new Money();
		Money totalSTJaPaga = new Money();
		
		Money valorFCPSubtrair = new Money();
		
		for (Notafiscalprodutoitem item : listaProdutos) {
			det = new Det();
			
			det.setNItem(nItem);
			nItem++;
			
			prod = new Prod();
			det.setProd(prod);
			prod.setCProd(item.getIdentificadorForNotaSped());
			
			if(item.getGtin() != null && 
					!item.getGtin().trim().equals("") && 
					SinedUtil.validaCEAN(item.getGtin())){
				prod.setCEAN(item.getGtin()); 
				prod.setCEANTrib(item.getGtin());
			} else {
				if(habilitarNfev4){
					prod.setCEAN("SEM GTIN");
					prod.setCEANTrib("SEM GTIN");
				}
			}
			
			if(!isNfe && det.getNItem().equals(1) && !ambienteProducao){
				prod.setXProd("NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL");
			} else {
				if(item.getMaterial().getKitflexivel() != null && item.getMaterial().getKitflexivel() &&
						item.getVendamaterialmestre() != null && org.apache.commons.lang.StringUtils.isNotEmpty(item.getVendamaterialmestre().getNomealternativo())){
					prod.setXProd(addScape(item.getVendamaterialmestre().getNomealternativo()));
				}else {
					if (item.getMaterial().getNomenfOuNome() != null && item.getMaterial().getNomenfOuNome().length() > 120){
						Produto produto = produtoService.carregaProduto(item.getMaterial());
						if (produto != null && produto.getNomereduzido() != null)
							prod.setXProd(addScape(produto.getNomereduzido()));
						else
							prod.setXProd(addScape(item.getMaterial().getNomenfOuNome()));
					} else
						prod.setXProd(addScape(item.getMaterial().getNomenfOuNome()));
				}
			}
			
			if(item.getNcmcompleto() != null && !item.getNcmcompleto().equals("")){
				prod.setNCM(item.getNcmcompleto()); 
			} else {
				prod.setNCM(item.getNcmcapitulo() != null ? item.getNcmcapitulo().getCodeFormat() : null); 
			}
			
			prod.setnVE(item.getNve()); 
			prod.setEXTIPI(item.getExtipi()); 
			prod.setCFOP(item.getCfop() != null && item.getCfop().getCodigo() != null ? StringUtils.stringCheia(StringUtils.soNumero(item.getCfop().getCodigo()), "0", 4, false) : null);
			prod.setUCom(item.getUnidademedida() != null ? addScape(item.getUnidademedida().getSimbolo()) : null);
			prod.setQCom(item.getQtde());
			prod.setVUnCom(item.getValorunitario());
			prod.setVProd(item.getValorbruto().getValue().doubleValue());
			prod.setUTrib(item.getUnidademedida() != null ? addScape(item.getUnidademedida().getSimbolo()) : null);
			prod.setQTrib(item.getQtde());
			prod.setVUnTrib(item.getValorunitario());
			prod.setVFrete(item.getValorfrete().getValue().doubleValue()); 
			prod.setVDesc(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d);
			prod.setVSeg(item.getValorseguro().getValue().doubleValue()); 
			prod.setVOutro(item.getOutrasdespesas().getValue().doubleValue());
			prod.setIndTot(item.getIncluirvalorprodutos() != null && item.getIncluirvalorprodutos() ? 1 : 0);
			prod.setCEST(item.getCest());
			
			String recopi = item.getRecopi();
			if(recopi == null || recopi.trim().equals("")){
				if(nota.getRecopi() != null && 
						!nota.getRecopi().trim().equals("") &&
						item.getMaterial() != null && 
						item.getMaterial().getMaterialgrupo() != null &&
						item.getMaterial().getMaterialgrupo().getPapelimune() != null &&
						item.getMaterial().getMaterialgrupo().getPapelimune()){
					recopi = nota.getRecopi();
				}
			}
			prod.setnRECOPI(recopi);
			
			prod.setXPed(item.getNumeropedidocompra() != null && !item.getNumeropedidocompra().equals("") ? item.getNumeropedidocompra() : null);
			prod.setNItemPed(item.getItempedidocompra() != null ? item.getItempedidocompra() : null);
			prod.setCbenef(item.getCbenef() != null ? item.getCbenef() : null);
			
			Integer origem = item.getOrigemproduto() != null ? item.getOrigemproduto().getValue() : 0;
			
			if (origem == 3 || origem == 5 || origem == 8) {
				prod.setnFci(org.apache.commons.lang.StringUtils.isNotEmpty(item.getNumeroFci()) ? item.getNumeroFci() : null);
			}
			
			if(item.getDadosimportacao() != null && item.getDadosimportacao() && 
					item.getListaNotafiscalprodutoitemdi() != null && item.getListaNotafiscalprodutoitemdi().size() > 0){
				ListaDI listaDI = new ListaDI();
				prod.setListaDI(listaDI);
				
				List<DI> listDI = new ArrayList<DI>();
				
				for (Notafiscalprodutoitemdi notafiscalprodutoitemdi : item.getListaNotafiscalprodutoitemdi()) {
					if(notafiscalprodutoitemdi.getDeclaracaoimportacao() != null){
						Declaracaoimportacao declaracaoimportacao = declaracaoimportacaoService.loadForEntrada(notafiscalprodutoitemdi.getDeclaracaoimportacao());

						DI di = new DI();
						
						if(declaracaoimportacao.getNumerodidsida() != null) di.setNDI(StringUtils.soNumero(declaracaoimportacao.getNumerodidsida()));
						di.setDDi(declaracaoimportacao.getDtregistro());
						di.setCExportador(declaracaoimportacao.getCodigoexportador());
						di.setXLocDesemb(declaracaoimportacao.getLocaldesembaraco());
						di.setUFDesemb(declaracaoimportacao.getUf().getSigla().toUpperCase());
						di.setDDesemb(declaracaoimportacao.getData());
						di.setTpViaTransp(declaracaoimportacao.getViatransporte() != null ? declaracaoimportacao.getViatransporte().getCdnfe() : null);
						di.setvAFRMM(declaracaoimportacao.getValorafrmm() != null ? declaracaoimportacao.getValorafrmm().getValue().doubleValue() : null);
						di.setTpIntermedio(declaracaoimportacao.getFormaintermediacao() != null ? declaracaoimportacao.getFormaintermediacao().getCdnfe() : null);
						di.setuFTerceiro(declaracaoimportacao.getUfadquirente() != null ? declaracaoimportacao.getUfadquirente().getSigla() : null);
						di.setcNPJ(declaracaoimportacao.getCnpjadquirente() != null ? declaracaoimportacao.getCnpjadquirente().getValue() : null);
						
						if(declaracaoimportacao.getListaDeclaracaoimportacaoadicao() != null &&
								declaracaoimportacao.getListaDeclaracaoimportacaoadicao().size() > 0){
							Listaadi listaadi = new Listaadi();
							di.setListaadi(listaadi);
							
							List<Adi> listAdi = new ArrayList<Adi>();
							for (Declaracaoimportacaoadicao adicao : declaracaoimportacao.getListaDeclaracaoimportacaoadicao()) {
								Adi adi = new Adi();
								
								adi.setCFabricante(adicao.getCodigofabricante());
								adi.setNAdicao(adicao.getNumeroadicao());
								adi.setNSeqAdic(adicao.getItem());
								adi.setVDescDI(adicao.getValordesconto() != null && adicao.getValordesconto().getValue().doubleValue() > 0 ? adicao.getValordesconto().getValue().doubleValue() : null);
								adi.setnDraw(adicao.getNumerodrawback());
								
								listAdi.add(adi);
							}
							
							listaadi.setListaadi(listAdi);
						}
						
						listDI.add(di);
					}
				}
				
				listaDI.setListaDI(listDI);
			}
			
			/*if (item.getMaterial().getMaterialgrupo() != null && item.getMaterial().getMaterialgrupo().getMedicamento() != null && 
					item.getMaterial().getMaterialgrupo().getMedicamento()) {
				List<Rastro> listRastro = new ArrayList<Rastro>();
				ListaRastro listaRastro = new ListaRastro();
				Rastro rastro;
				
				if (item.getLoteestoque() != null) {
					Loteestoque lote = item.getLoteestoque();
					
					for (Lotematerial loteMaterial : lote.getListaLotematerial()) {
						if (loteMaterial.getMaterial().getCdmaterial().equals(item.getMaterial().getCdmaterial())) {
							rastro = new Rastro();
							
							rastro.setqLote(item.getQtde());
							rastro.setnLote(lote.getNumero());
							rastro.setdVal(loteMaterial.getValidade());
							rastro.setdFab(loteMaterial.getFabricacao());
							rastro.setcAgreg(loteMaterial.getCodigoAgregacao());
							
							listRastro.add(rastro);
						}
					}
				}
				
				listaRastro.setListaRastro(listRastro);
				
				prod.setListaRastro(listaRastro);
				
				Med med = new Med();
				
				if (item.getMaterial().getIsento()) {
					med.setcProdANVISA("ISENTO");
					med.setxMotivoIsencao(item.getMaterial().getCodigoIsencao() != null ? item.getMaterial().getCodigoIsencao() : "");
				} else {
					med.setcProdANVISA(item.getMaterial().getCodigoAnvisa() != null ? item.getMaterial().getCodigoAnvisa() : "");
				}
				
				med.setVPMC(item.getMaterial().getValorvendamaximo());
				
				prod.setMed(med);
			}*/
			if (item.getMaterial().getMaterialgrupo() != null && Boolean.TRUE.equals(item.getMaterial().getMaterialgrupo().getIncluirLoteNaNfe())) {
				List<Rastro> listRastro = new ArrayList<Rastro>();
				ListaRastro listaRastro = new ListaRastro();
				Rastro rastro;
				
				if (item.getLoteestoque() != null) {
					Loteestoque lote = item.getLoteestoque();
					
					for (Lotematerial loteMaterial : lote.getListaLotematerial()) {
						if (loteMaterial.getMaterial().getCdmaterial().equals(item.getMaterial().getCdmaterial())) {
							rastro = new Rastro();
							
							rastro.setqLote(item.getQtde());
							rastro.setnLote(lote.getNumero());
							rastro.setdVal(loteMaterial.getValidade());
							rastro.setdFab(loteMaterial.getFabricacao());
							rastro.setcAgreg(loteMaterial.getCodigoAgregacao());
							
							listRastro.add(rastro);
						}
					}
				}
				
				listaRastro.setListaRastro(listRastro);
				
				prod.setListaRastro(listaRastro);
				
				if(Boolean.TRUE.equals(item.getMaterial().getMaterialgrupo().getMedicamento())){
					Med med = new Med();
					
					if (item.getMaterial().getIsento()) {
						med.setcProdANVISA("ISENTO");
						med.setxMotivoIsencao(item.getMaterial().getCodigoIsencao() != null ? item.getMaterial().getCodigoIsencao() : "");
					} else {
						med.setcProdANVISA(item.getMaterial().getCodigoAnvisa() != null ? item.getMaterial().getCodigoAnvisa() : "");
					}
					
					med.setVPMC(item.getMaterial().getValorvendamaximo());
					
					prod.setMed(med);
				}
			}
				
			if(item.getDadosexportacao() != null && item.getDadosexportacao()){
				boolean drawback = item.getDrawbackexportacao() != null && !item.getDrawbackexportacao().equals("");
				boolean exportacaoindireta = item.getNumregistroexportacao() != null && !item.getNumregistroexportacao().equals("") &&
												item.getChaveacessoexportacao() != null && !item.getChaveacessoexportacao().equals("") &&
												item.getQuantidadeexportacao() != null;
				
				
				if(drawback || exportacaoindireta){
					ListadetExport listadetExport = new ListadetExport();
					prod.setListadetExport(listadetExport);
					
					List<DetExport> listDetExport = new ArrayList<DetExport>();
					
					DetExport detExport = new DetExport();
					
					if(drawback){
						detExport.setnDraw(item.getDrawbackexportacao());
					}
					
					if(exportacaoindireta){
						ExportInd exportInd = new ExportInd();
						detExport.setExportInd(exportInd);
						
						exportInd.setnRE(item.getNumregistroexportacao());
						exportInd.setchNFe(item.getChaveacessoexportacao());
						exportInd.setqExport(item.getQuantidadeexportacao());
					}
					
					listDetExport.add(detExport);
					listadetExport.setListadetExport(listDetExport);
				}
			}
			
			if(devolucao){
				ImpostoDevol impostoDevol = new ImpostoDevol();
				det.setImpostoDevol(impostoDevol);
				
				impostoDevol.setpDevol(item.getPercentualdevolvido() != null ? item.getPercentualdevolvido() : 100d);
				impostoDevol.setvIPIDevol(item.getValoripidevolvido() != null ? item.getValoripidevolvido().getValue().doubleValue() : 0d);
			}
			
			imposto = new Imposto();
			det.setImposto(imposto);
			
			if(item.getValortotaltributos() != null){
				imposto.setvTotTrib(item.getValortotaltributos().getValue().doubleValue());
			}
			
			if(!item.getTributadoicms()){ 
				haveServico = true;
				
				issqn = new ISSQN();
				imposto.setISSQN(issqn);
				
				issqn.setVBC(item.getValorbciss().getValue().doubleValue());
				issqn.setVAliq(item.getIss()); 
				issqn.setVISSQN(item.getValoriss().getValue().doubleValue()); 
				issqn.setCMunFG(item.getMunicipioiss() != null ? item.getMunicipioiss().getCdibge() : null); 
				issqn.setvDeducao(item.getValordeducaoservico() != null ? item.getValordeducaoservico().getValue().doubleValue() : null);
				issqn.setvOutro(item.getValoroutrasretencoesservico() != null ? item.getValoroutrasretencoesservico().getValue().doubleValue() : null);
				issqn.setvDescCond(item.getValordescontocondicionadoservico() != null ? item.getValordescontocondicionadoservico().getValue().doubleValue() : null);
				issqn.setvDescIncond(item.getValordescontoincondicionadoservico() != null ? item.getValordescontoincondicionadoservico().getValue().doubleValue() : null);
				issqn.setvISSRet(item.getValorissretido() != null ? item.getValorissretido().getValue().doubleValue() : null);
				issqn.setIndISS(item.getExigibilidadeiss() != null ? item.getExigibilidadeiss().getValue() : null);
				issqn.setcServico(item.getCodigoservico());
				issqn.setcMun(item.getMunicipioservico() != null ? item.getMunicipioservico().getCdibge() : null);
				issqn.setcPais(item.getPaisservico() != null ? item.getPaisservico().getCdbacen() : null);
				issqn.setnProcesso(item.getProcessoservico());
				issqn.setIndIncentivo(item.getIncentivofiscal() != null && item.getIncentivofiscal() ? 1 : 2);
				issqn.setCListServ(item.getItemlistaservico() != null ? StringUtils.stringCheia(item.getItemlistaservico().getCodigo(), "0", 5, false) : null);
			} else {
				icms = new ICMS();
				imposto.setICMS(icms);
				
				tipocobrancaicms = item.getTipocobrancaicms();
				
				if(tipocobrancaicms.equals(Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE)){
					icms00 = new ICMS00(habilitarNfev4);
					icms.setICMS00(icms00);
					
					icms00.setOrig(origem); 
					icms00.setCST(tipocobrancaicms.getCdnfe());
					icms00.setModBC(item.getModalidadebcicms().getValue()); 
					icms00.setVBC(item.getValorbcicms().getValue().doubleValue()); 
					icms00.setPICMS(item.getIcms()); 
					icms00.setVICMS(item.getValoricms().getValue().doubleValue()); 
					
					if(!Boolean.TRUE.equals(item.getDadosicmspartilha())){
						icms00.setpFCP(item.getFcp() != null && item.getFcp() > 0 ? item.getFcp() : null);
                        icms00.setvFCP(item.getValorfcp() != null && item.getValorfcp().getValue().doubleValue() > 0 ? item.getValorfcp().getValue().doubleValue() : null);                        
					}else if(item.getValorfcp() != null && item.getValorfcp().getValue().doubleValue() > 0){
						valorFCPSubtrair = valorFCPSubtrair.add(item.getValorfcp());
					}
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO)){
					icms10 = new ICMS10(habilitarNfev4);
					icms.setICMS10(icms10);
					
					icms10.setOrig(origem);
					icms10.setCST(tipocobrancaicms.getCdnfe());
					
					icms10.setModBC(item.getModalidadebcicms().getValue());
					icms10.setVBC(item.getValorbcicms().getValue().doubleValue()); 
					icms10.setPICMS(item.getIcms()); 
					icms10.setVICMS(item.getValoricms().getValue().doubleValue());
					
					icms10.setModBCST(item.getModalidadebcicmsst() != null ? item.getModalidadebcicmsst().getValue() : null); 
					icms10.setPMVAST(item.getMargemvaloradicionalicmsst() != null && item.getMargemvaloradicionalicmsst() > 0 ? item.getMargemvaloradicionalicmsst() : null);
					icms10.setPRedBCST(item.getReducaobcicmsst() != null && item.getReducaobcicmsst() > 0 ? item.getReducaobcicmsst() : null);
					icms10.setVBCST(item.getValorbcicmsst().getValue().doubleValue()); 
					icms10.setPICMSST(item.getIcmsst()); 
					icms10.setVICMSST(item.getValoricmsst().getValue().doubleValue());
					
					icms10.setvBCFCP(item.getValorbcfcp() != null && item.getValorbcfcp().getValue().doubleValue() > 0 ? item.getValorbcfcp().getValue().doubleValue() : null);
					icms10.setpFCP(item.getFcp() != null && item.getFcp() > 0 ? item.getFcp() : null);
					icms10.setvFCP(item.getValorfcp() != null && item.getValorfcp().getValue().doubleValue() > 0 ? item.getValorfcp().getValue().doubleValue() : null);
					icms10.setvBCFCPST(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icms10.setpFCPST(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icms10.setvFCPST(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC)){
					icms20 = new ICMS20(habilitarNfev4);
					icms.setICMS20(icms20);
					
					icms20.setOrig(origem); 
					icms20.setCST(tipocobrancaicms.getCdnfe());
					icms20.setModBC(item.getModalidadebcicms().getValue()); 
					icms20.setPRedBC(item.getReducaobcicms() != null && item.getReducaobcicms() > 0 ? item.getReducaobcicms() : null);
					icms20.setVBC(item.getValorbcicms().getValue().doubleValue()); 
					icms20.setPICMS(item.getIcms()); 
					icms20.setVICMS(item.getValoricms().getValue().doubleValue()); 
					
					icms20.setvBCFCP(item.getValorbcfcp() != null && item.getValorbcfcp().getValue().doubleValue() > 0 ? item.getValorbcfcp().getValue().doubleValue() : null);
					icms20.setpFCP(item.getFcp() != null && item.getFcp() > 0 ? item.getFcp() : null);
					icms20.setvFCP(item.getValorfcp() != null && item.getValorfcp().getValue().doubleValue() > 0 ? item.getValorfcp().getValue().doubleValue() : null);
					
					if(item.getValordesoneracaoicms() != null && item.getValordesoneracaoicms().getValue().doubleValue() > 0){
						icms20.setvICMSDeson(item.getValordesoneracaoicms().getValue().doubleValue());
						icms20.setMotDesICMS(item.getMotivodesoneracaoicms() != null ? item.getMotivodesoneracaoicms().getCdnfe() : null);
					}
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO)){
					icms30 = new ICMS30(habilitarNfev4);
					icms.setICMS30(icms30);
					
					icms30.setOrig(origem);
					icms30.setCST(tipocobrancaicms.getCdnfe());
					icms30.setModBCST(item.getModalidadebcicmsst() != null ? item.getModalidadebcicmsst().getValue() : null); 
					icms30.setPMVAST(item.getMargemvaloradicionalicmsst() != null && item.getMargemvaloradicionalicmsst() > 0 ? item.getMargemvaloradicionalicmsst() : null);
					icms30.setPRedBCST(item.getReducaobcicmsst() != null && item.getReducaobcicmsst() > 0 ? item.getReducaobcicmsst() : null);
					icms30.setVBCST(item.getValorbcicmsst().getValue().doubleValue()); 
					icms30.setPICMSST(item.getIcmsst()); 
					icms30.setVICMSST(item.getValoricmsst().getValue().doubleValue());
					
					icms30.setvBCFCPST(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icms30.setpFCPST(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icms30.setvFCPST(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
					
					if(item.getValordesoneracaoicms() != null && item.getValordesoneracaoicms().getValue().doubleValue() > 0){
						icms30.setvICMSDeson(item.getValordesoneracaoicms().getValue().doubleValue());
						icms30.setMotDesICMS(item.getMotivodesoneracaoicms() != null ? item.getMotivodesoneracaoicms().getCdnfe() : null);
					}
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.ISENTA) || 
						tipocobrancaicms.equals(Tipocobrancaicms.NAO_TRIBUTADA)  || 
						tipocobrancaicms.equals(Tipocobrancaicms.SUSPENSAO) ||
						tipocobrancaicms.equals(Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO)){
					icms40 = new ICMS40();
					icms.setICMS40(icms40);
					
					icms40.setOrig(origem); 
					icms40.setCST(tipocobrancaicms.getCdnfe()); 
					if(item.getValordesoneracaoicms() != null && item.getValordesoneracaoicms().getValue().doubleValue() > 0){
						icms40.setvICMSDeson(item.getValordesoneracaoicms().getValue().doubleValue());
						icms40.setMotDesICMS(item.getMotivodesoneracaoicms() != null ? item.getMotivodesoneracaoicms().getCdnfe() : null);
					}
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.DIFERIMENTO)){
					icms51 = new ICMS51(habilitarNfev4);
					icms.setICMS51(icms51);
					
					icms51.setOrig(origem); 
					icms51.setCST(tipocobrancaicms.getCdnfe());
					icms51.setModBC(item.getModalidadebcicms().getValue()); 
					icms51.setPRedBC(item.getReducaobcicms() != null && item.getReducaobcicms() > 0 ? item.getReducaobcicms() : null);
					icms51.setVBC(item.getValorbcicms().getValue().doubleValue()); 
					icms51.setPICMS(item.getIcms()); 
					icms51.setvICMSOp(item.getValoricms().getValue().doubleValue());
					icms51.setpDif(100d);
					icms51.setvICMSDif(item.getValoricms().getValue().doubleValue());
					icms51.setVICMS(0d); 	
					
					icms51.setvBCFCP(item.getValorbcfcp() != null && item.getValorbcfcp().getValue().doubleValue() > 0 ? item.getValorbcfcp().getValue().doubleValue() : null);
					icms51.setpFCP(item.getFcp() != null && item.getFcp() > 0 ? item.getFcp() : null);
					icms51.setvFCP(item.getValorfcp() != null && item.getValorfcp().getValue().doubleValue() > 0 ? item.getValorfcp().getValue().doubleValue() : null);
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO) ||
						tipocobrancaicms.equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_ICMSST_DEVENDO)){
					icms60 = new ICMS60(habilitarNfev4);
					icms.setICMS60(icms60);
					
					icms60.setOrig(origem); 
					icms60.setCST(tipocobrancaicms.getCdnfe());
					icms60.setVBCSTRet(item.getValorbcicmsst().getValue().doubleValue()); 
					icms60.setpST(item.getIcmsst() != null ? item.getIcmsst() : 0d);
					icms60.setvICMSSubstituto(item.getValorIcmsPropSub() != null ? item.getValorIcmsPropSub().getValue().doubleValue() : 0d);
					icms60.setVICMSSTRet(item.getValoricmsst().getValue().doubleValue());
					
					icms60.setpRedBCEfet(item.getRedBaseCalcEfetiva() != null ? item.getRedBaseCalcEfetiva() : null);
					icms60.setvBCEfet(item.getValorBaseCalcEfetiva() != null ? item.getValorBaseCalcEfetiva().getValue().doubleValue() : 0d);
					icms60.setpICMSEfet(item.getIcmsEfetiva() != null ? item.getIcmsEfetiva() : 0d);
					icms60.setvICMSEfet(item.getValorIcmsEfetiva() != null ? item.getValorIcmsEfetiva().getValue().doubleValue() : 0d);
					
					if(Finalidadenfe.DEVOLUCAO.equals(nota.getFinalidadenfe()) && !Operacaonfe.CONSUMIDOR_FINAL.equals(nota.getOperacaonfe()) && 
							Tipooperacaonota.ENTRADA.equals(nota.getTipooperacaonota()) &&
							configuracaonfe.getUf() != null &&
							("MG".equalsIgnoreCase(configuracaonfe.getUf().getSigla()) ||
							 "SP".equalsIgnoreCase(configuracaonfe.getUf().getSigla()))){
						icms60.setExibirTagICMSEfet(true);
					}
					
					icms60.setvBCFCPSTRet(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icms60.setpFCPSTRet(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icms60.setvFCPSTRet(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
					
					totalBCSTJaPaga = totalBCSTJaPaga.add(item.getValorbcicmsst() != null ? item.getValorbcicmsst() : new Money());
					totalSTJaPaga = totalSTJaPaga.add(item.getValoricmsst() != null ? item.getValoricmsst() : new Money());
					
//					if(item.getMaterial() != null && 
//							item.getMaterial().getMaterialgrupo() != null && 
//							item.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null){
//						Materialgrupo materialgrupo = materialgrupoService.loadForEntrada(item.getMaterial().getMaterialgrupo());
//						Materialgrupotributacao materialgrupotributacao = materialgrupo.getMaterialgrupotributacaoByEmpresa(empresa, item.getCfop());
//						
//						if(materialgrupotributacao != null 
//								&& materialgrupotributacao.getExibirmensagemstpaga() != null 
//								&& materialgrupotributacao.getExibirmensagemstpaga()){
//							if(materialgrupotributacao.getExibirmensagemstpagatotal() == null || !materialgrupotributacao.getExibirmensagemstpagatotal()){
//								StringBuilder sb = new StringBuilder();
//								if(item.getInfoadicional() != null) sb.append(item.getInfoadicional());
//								if(sb.length() > 0) sb.append("\n");
//								
//								Money valorbcicmsst = item.getValorbcicmsst() != null ? item.getValorbcicmsst() : new Money();
//								Money valoricmsst = item.getValoricmsst() != null ? item.getValoricmsst() : new Money();
//								
//								sb.append("BASE ST J� PAGA: ").append(valorbcicmsst).append(" ");
//								sb.append("ST J� PAGA: ").append(valoricmsst);
//								
//								item.setInfoadicional(sb.toString());
//							} else {
//								exibirSTJaPaga = true;
//							}
//						}
//						
//					}
					if(item.getMaterial() != null && item.getGrupotributacao() == null){
						List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
								grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
														empresa, 
														naturezaoperacao, 
														item.getMaterial().getMaterialgrupo(),
														cliente,
														true, 
														item.getMaterial(), 
														item.getNcmcompleto(),
														enderecoCliente, 
														null,
														listaCategoriaCliente,
														null,
														operacaonfe,
														nota.getModeloDocumentoFiscalEnum(),
														nota.getLocaldestinonfe(),
														nota.getDtEmissao()));
						item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
					}else {
						item.setGrupotributacao(grupotributacaoService.loadForEntrada(item.getGrupotributacao()));
					}
					Grupotributacao grupotributacao = item.getGrupotributacao();
					if(grupotributacao != null 
							&& grupotributacao.getExibirmensagemstpaga() != null 
							&& grupotributacao.getExibirmensagemstpaga()){
						if(grupotributacao.getExibirmensagemstpagatotal() == null || !grupotributacao.getExibirmensagemstpagatotal()){
							StringBuilder sb = new StringBuilder();
							if(item.getInfoadicional() != null) sb.append(item.getInfoadicional());
							if(sb.length() > 0) sb.append("\n");
							
							Money valorbcicmsst = item.getValorbcicmsst() != null ? item.getValorbcicmsst() : new Money();
							Money valoricmsst = item.getValoricmsst() != null ? item.getValoricmsst() : new Money();
							
							sb.append("BASE ST J� PAGA: ").append(valorbcicmsst).append(" ");
							sb.append("ST J� PAGA: ").append(valoricmsst);
							
							item.setInfoadicional(addScape(sb.toString()));
						} else {
							exibirSTJaPaga = true;
						}
					}
						
					
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO)){
					icms70 = new ICMS70(habilitarNfev4);
					icms.setICMS70(icms70);
					
					icms70.setOrig(origem);
					icms70.setCST(tipocobrancaicms.getCdnfe());
					icms70.setModBC(item.getModalidadebcicms().getValue());
					icms70.setPRedBC(item.getReducaobcicms()); 
					icms70.setVBC(item.getValorbcicms().getValue().doubleValue()); 
					icms70.setPICMS(item.getIcms()); 
					icms70.setVICMS(item.getValoricms().getValue().doubleValue());
					icms70.setModBCST(item.getModalidadebcicmsst() != null ? item.getModalidadebcicmsst().getValue() : null); 
					icms70.setPMVAST(item.getMargemvaloradicionalicmsst() != null && item.getMargemvaloradicionalicmsst() > 0 ? item.getMargemvaloradicionalicmsst() : null);
					icms70.setPRedBCST(item.getReducaobcicmsst() != null && item.getReducaobcicmsst() > 0 ? item.getReducaobcicmsst() : null);
					icms70.setVBCST(item.getValorbcicmsst().getValue().doubleValue()); 
					icms70.setPICMSST(item.getIcmsst()); 
					icms70.setVICMSST(item.getValoricmsst().getValue().doubleValue());
					
					icms70.setvBCFCP(item.getValorbcfcp() != null && item.getValorbcfcp().getValue().doubleValue() > 0 ? item.getValorbcfcp().getValue().doubleValue() : null);
					icms70.setpFCP(item.getFcp() != null && item.getFcp() > 0 ? item.getFcp() : null);
					icms70.setvFCP(item.getValorfcp() != null && item.getValorfcp().getValue().doubleValue() > 0 ? item.getValorfcp().getValue().doubleValue() : null);
					icms70.setvBCFCPST(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icms70.setpFCPST(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icms70.setvFCPST(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
					
					if(item.getValordesoneracaoicms() != null && item.getValordesoneracaoicms().getValue().doubleValue() > 0){
						icms70.setvICMSDeson(item.getValordesoneracaoicms().getValue().doubleValue());
						icms70.setMotDesICMS(item.getMotivodesoneracaoicms() != null ? item.getMotivodesoneracaoicms().getCdnfe() : null);
					}
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.OUTROS)){
					icms90 = new ICMS90(habilitarNfev4);
					icms.setICMS90(icms90);
					
					icms90.setOrig(origem);
					icms90.setCST(tipocobrancaicms.getCdnfe());
					icms90.setModBC(item.getModalidadebcicms() != null ? item.getModalidadebcicms().getValue() : null);
					icms90.setPRedBC(item.getReducaobcicms() != null ? item.getReducaobcicms() : null); 
					icms90.setVBC(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : null); 
					icms90.setPICMS(item.getIcms()); 
					icms90.setVICMS(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue() : null);
					icms90.setModBCST(item.getModalidadebcicmsst() != null ? item.getModalidadebcicmsst().getValue() : null); 
					icms90.setPMVAST(item.getMargemvaloradicionalicmsst() != null && item.getMargemvaloradicionalicmsst() > 0 ? item.getMargemvaloradicionalicmsst() : null);
					icms90.setPRedBCST(item.getReducaobcicmsst() != null && item.getReducaobcicmsst() > 0 ? item.getReducaobcicmsst() : null);
					icms90.setVBCST(item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : null); 
					icms90.setPICMSST(item.getIcmsst()); 
					icms90.setVICMSST(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : null);
					
					icms90.setvBCFCP(item.getValorbcfcp() != null && item.getValorbcfcp().getValue().doubleValue() > 0 ? item.getValorbcfcp().getValue().doubleValue() : null);
					icms90.setpFCP(item.getFcp() != null && item.getFcp() > 0 ? item.getFcp() : null);
					icms90.setvFCP(item.getValorfcp() != null && item.getValorfcp().getValue().doubleValue() > 0 ? item.getValorfcp().getValue().doubleValue() : null);
					icms90.setvBCFCPST(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icms90.setpFCPST(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icms90.setvFCPST(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
					
					if(item.getValordesoneracaoicms() != null && item.getValordesoneracaoicms().getValue().doubleValue() > 0){
						icms90.setvICMSDeson(item.getValordesoneracaoicms().getValue().doubleValue());
						icms90.setMotDesICMS(item.getMotivodesoneracaoicms() != null ? item.getMotivodesoneracaoicms().getCdnfe() : null);
					}
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO_PARTILHA) || tipocobrancaicms.equals(Tipocobrancaicms.OUTROS_PARTILHA)){
					icmsPart = new ICMSPart();
					icms.setICMSPart(icmsPart);
					
					icmsPart.setOrig(origem);
					icmsPart.setCST(tipocobrancaicms.getCdnfe());
					icmsPart.setModBC(item.getModalidadebcicms().getValue());
					icmsPart.setPRedBC(item.getReducaobcicms() != null && item.getReducaobcicms() > 0 ? item.getReducaobcicms() : null);  
					icmsPart.setVBC(item.getValorbcicms().getValue().doubleValue()); 
					icmsPart.setPICMS(item.getIcms()); 
					icmsPart.setVICMS(item.getValoricms().getValue().doubleValue());
					icmsPart.setModBCST(item.getModalidadebcicmsst() != null ? item.getModalidadebcicmsst().getValue() : null); 
					icmsPart.setPMVAST(item.getMargemvaloradicionalicmsst() != null && item.getMargemvaloradicionalicmsst() > 0 ? item.getMargemvaloradicionalicmsst() : null);
					icmsPart.setPRedBCST(item.getReducaobcicmsst() != null && item.getReducaobcicmsst() > 0 ? item.getReducaobcicmsst() : null);
					icmsPart.setVBCST(item.getValorbcicmsst().getValue().doubleValue()); 
					icmsPart.setPICMSST(item.getIcmsst()); 
					icmsPart.setVICMSST(item.getValoricmsst().getValue().doubleValue());
					icmsPart.setPBCOp(item.getBcoperacaopropriaicms().getValue().doubleValue());
					icmsPart.setUFST(item.getUficmsst().getSigla());
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO)){
					icmssn101 = new ICMSSN101();
					icms.setICMSSN101(icmssn101);
					
					icmssn101.setOrig(origem);
					icmssn101.setCSOSN(tipocobrancaicms.getCdnfe());
					icmssn101.setPCredSN(item.getAliquotacreditoicms());
					icmssn101.setVCredICMSSN(item.getValorcreditoicms().getValue().doubleValue());
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO) ||  
						tipocobrancaicms.equals(Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL) ||  
						tipocobrancaicms.equals(Tipocobrancaicms.IMUNE) ||  
						tipocobrancaicms.equals(Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL)){
					icmssn102 = new ICMSSN102();
					icms.setICMSSN102(icmssn102);
					
					icmssn102.setOrig(origem);
					icmssn102.setCSOSN(tipocobrancaicms.getCdnfe());
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST)){
					icmssn201 = new ICMSSN201(habilitarNfev4);
					icms.setICMSSN201(icmssn201);
					
					icmssn201.setOrig(origem);
					icmssn201.setCSOSN(tipocobrancaicms.getCdnfe());
					icmssn201.setPCredSN(item.getAliquotacreditoicms());
					icmssn201.setVCredICMSSN(item.getValorcreditoicms().getValue().doubleValue());
					icmssn201.setModBCST(item.getModalidadebcicmsst() != null ? item.getModalidadebcicmsst().getValue() : null); 
					icmssn201.setPMVAST(item.getMargemvaloradicionalicmsst() != null && item.getMargemvaloradicionalicmsst() > 0 ? item.getMargemvaloradicionalicmsst() : null);
					icmssn201.setPRedBCST(item.getReducaobcicmsst() != null && item.getReducaobcicmsst() > 0 ? item.getReducaobcicmsst() : null);
					icmssn201.setVBCST(item.getValorbcicmsst().getValue().doubleValue()); 
					icmssn201.setPICMSST(item.getIcmsst()); 
					icmssn201.setVICMSST(item.getValoricmsst().getValue().doubleValue());
					
					icmssn201.setvBCFCPST(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icmssn201.setpFCPST(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icmssn201.setvFCPST(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST) ||
						tipocobrancaicms.equals(Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST)){
					icmssn202 = new ICMSSN202(habilitarNfev4);
					icms.setICMSSN202(icmssn202);
					
					icmssn202.setOrig(origem);
					icmssn202.setCSOSN(tipocobrancaicms.getCdnfe());
					icmssn202.setModBCST(item.getModalidadebcicmsst() != null ? item.getModalidadebcicmsst().getValue() : null); 
					icmssn202.setPMVAST(item.getMargemvaloradicionalicmsst() != null && item.getMargemvaloradicionalicmsst() > 0 ? item.getMargemvaloradicionalicmsst() : null);
					icmssn202.setPRedBCST(item.getReducaobcicmsst() != null && item.getReducaobcicmsst() > 0 ? item.getReducaobcicmsst() : null);
					icmssn202.setVBCST(item.getValorbcicmsst().getValue().doubleValue()); 
					icmssn202.setPICMSST(item.getIcmsst()); 
					icmssn202.setVICMSST(item.getValoricmsst().getValue().doubleValue());
					
					icmssn202.setvBCFCPST(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icmssn202.setpFCPST(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icmssn202.setvFCPST(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE)){
					icmssn500 = new ICMSSN500(habilitarNfev4);
					icms.setICMSSN500(icmssn500);
					
					icmssn500.setOrig(origem);
					icmssn500.setCSOSN(tipocobrancaicms.getCdnfe());
					icmssn500.setVBCSTRet(item.getValorbcicmsst().getValue().doubleValue()); 
					icmssn500.setpST(item.getIcmsst() != null ? item.getIcmsst() : 0d);
					icmssn500.setvICMSSubstituto(item.getValorIcmsPropSub() != null ? item.getValorIcmsPropSub().getValue().doubleValue() : 0d);
					icmssn500.setVICMSSTRet(item.getValoricmsst().getValue().doubleValue());
					
					icmssn500.setvBCFCPSTRet(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icmssn500.setpFCPSTRet(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icmssn500.setvFCPSTRet(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
					
					icmssn500.setpRedBCEfet(item.getRedBaseCalcEfetiva() != null && item.getRedBaseCalcEfetiva() > 0 ? item.getRedBaseCalcEfetiva() : null);
					icmssn500.setvBCEfet(item.getValorBaseCalcEfetiva() != null ? item.getValorBaseCalcEfetiva().getValue().doubleValue() : 0d);
					icmssn500.setpICMSEfet(item.getIcmsEfetiva() != null ? item.getIcmsEfetiva() : 0d);
					icmssn500.setvICMSEfet(item.getValorIcmsEfetiva() != null ? item.getValorIcmsEfetiva().getValue().doubleValue() : 0d);
				}
				else if(tipocobrancaicms.equals(Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL)){
					icmssn900 = new ICMSSN900(habilitarNfev4);
					icms.setICMSSN900(icmssn900);
					
					icmssn900.setOrig(origem);
					icmssn900.setCSOSN(tipocobrancaicms.getCdnfe());
					icmssn900.setModBC(item.getModalidadebcicms() != null ? item.getModalidadebcicms().getValue() : null); 
					icmssn900.setPRedBC(item.getReducaobcicms() != null && item.getReducaobcicms() > 0 ? item.getReducaobcicms() : null);
					icmssn900.setVBC(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : 0d); 
					icmssn900.setPICMS(item.getIcms() != null ? item.getIcms() : 0d); 
					icmssn900.setVICMS(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue() : 0d); 
					icmssn900.setModBCST(item.getModalidadebcicmsst() != null ? item.getModalidadebcicmsst().getValue() : null); 
					icmssn900.setPMVAST(item.getMargemvaloradicionalicmsst() != null && item.getMargemvaloradicionalicmsst() > 0 ? item.getMargemvaloradicionalicmsst() : null);
					icmssn900.setPRedBCST(item.getReducaobcicmsst() != null && item.getReducaobcicmsst() > 0 ? item.getReducaobcicmsst() : null);
					
					if(item.getModalidadebcicmsst() != null){
						icmssn900.setVBCST(item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : 0d); 
						icmssn900.setPICMSST(item.getIcmsst() != null ? item.getIcmsst() : 0d); 
						icmssn900.setVICMSST(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0d);
					}
					
					icmssn900.setPCredSN(item.getAliquotacreditoicms());
					icmssn900.setVCredICMSSN(item.getValorcreditoicms().getValue().doubleValue());
					
					icmssn900.setvBCFCPST(item.getValorbcfcpst() != null && item.getValorbcfcpst().getValue().doubleValue() > 0 ? item.getValorbcfcpst().getValue().doubleValue() : null);
					icmssn900.setpFCPST(item.getFcpst() != null && item.getFcpst() > 0 ? item.getFcpst() : null);
					icmssn900.setvFCPST(item.getValorfcpst() != null && item.getValorfcpst().getValue().doubleValue() > 0 ? item.getValorfcpst().getValue().doubleValue() : null);
				}
				
				
				if(tipocobrancaicms.equals(Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO) || 
						(tipocobrancaicms.equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_ICMSST_DEVENDO) && habilitarNfev4)){
					icmsST = new ICMSST();
					icms.setICMSST(icmsST);
					
					icmsST.setOrig(origem);
					icmsST.setCST(tipocobrancaicms.getCdnfe());
					icmsST.setVBCSTRet(item.getValorbcicmsstremetente() != null ? item.getValorbcicmsstremetente().getValue().doubleValue() : 0d); 
					icmsST.setVICMSSTRet(item.getValoricmsremetente() != null ? item.getValoricmsremetente().getValue().doubleValue() : 0d);
					icmsST.setVBCSTDest(item.getValorbcicmsstdestinatario() != null ? item.getValorbcicmsstdestinatario().getValue().doubleValue() : 0d); 
					icmsST.setVICMSSTDest(item.getValoricmsstdestinatario() != null ? item.getValoricmsstdestinatario().getValue().doubleValue() : 0d);
				}
			}
			
			if(item.getTipocobrancaipi() != null){
				ipi = new IPI();
				imposto.setIPI(ipi);
				
				ipi.setCNPJProd(item.getCnpjprodutoripi() != null ? item.getCnpjprodutoripi().getValue() : null);
				ipi.setCSelo(addScape(item.getCodigoseloipi()));
				ipi.setQSelo(item.getQtdeseloipi()); 
				ipi.setCEnq(org.apache.commons.lang.StringUtils.isNotBlank(item.getCodigoenquadramentoipi()) ? item.getCodigoenquadramentoipi() : "999");
				
				tipocobrancaipi = item.getTipocobrancaipi();
				if(tipocobrancaipi.equals(Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO) ||
						tipocobrancaipi.equals(Tipocobrancaipi.ENTRADA_OUTRAS) ||
						tipocobrancaipi.equals(Tipocobrancaipi.SAIDA_TRIBUTABA) ||
						tipocobrancaipi.equals(Tipocobrancaipi.SAIDA_OUTRAS)){
					ipiTrib = new IPITrib();
					ipi.setIPITrib(ipiTrib);
					
					ipiTrib.setCST(tipocobrancaipi.getCdnfe());
					if(item.getTipocalculoipi().equals(Tipocalculo.PERCENTUAL)){
						ipiTrib.setVBC(item.getValorbcipi().getValue().doubleValue()); 
						ipiTrib.setPIPI(item.getIpi());
					} else {
						ipiTrib.setQUnid(item.getQtdevendidaipi());
						ipiTrib.setVUnid(item.getAliquotareaisipi().getValue().doubleValue());
					}
					ipiTrib.setVIPI(item.getValoripi().getValue().doubleValue());
				}
				else {
					ipint = new IPINT();
					ipi.setIPINT(ipint);
					
					ipint.setCST(tipocobrancaipi.getCdnfe());
				}
			}
			
			
			pis = new PIS();
			imposto.setPIS(pis);
			
			tipocobrancapis = item.getTipocobrancapis();
			if(tipocobrancapis != null){
				if( tipocobrancapis.equals(Tipocobrancapis.OPERACAO_01) || 
						tipocobrancapis.equals(Tipocobrancapis.OPERACAO_02)){
					
					pisAliq = new PISAliq();
					pis.setPISAliq(pisAliq);
					
					pisAliq.setCST(tipocobrancapis.getCdnfe());
					pisAliq.setVBC(item.getValorbcpis().getValue().doubleValue()); 
					pisAliq.setPPIS(item.getPis()); 
					pisAliq.setVPIS(item.getValorpis().getValue().doubleValue()); 
				}
				else if( tipocobrancapis.equals(Tipocobrancapis.OPERACAO_04) ||
						tipocobrancapis.equals(Tipocobrancapis.OPERACAO_05) ||
						tipocobrancapis.equals(Tipocobrancapis.OPERACAO_06) ||
						tipocobrancapis.equals(Tipocobrancapis.OPERACAO_07) ||
						tipocobrancapis.equals(Tipocobrancapis.OPERACAO_08) ||
						tipocobrancapis.equals(Tipocobrancapis.OPERACAO_09)
						){
					pisnt = new PISNT();
					pis.setPISNT(pisnt);
					
					pisnt.setCST(tipocobrancapis.getCdnfe());
				}
				else if(tipocobrancapis.equals(Tipocobrancapis.OPERACAO_03)){
					pisqtde = new PISQtde();
					pis.setPISQtde(pisqtde);
					
					pisqtde.setCST(tipocobrancapis.getCdnfe());
					pisqtde.setQBCProd(item.getQtdevendidapis());
					pisqtde.setVAliqProd(item.getAliquotareaispis().getValue().doubleValue());
					pisqtde.setVPIS(item.getValorpis().getValue().doubleValue());
				}
				else {
					pisoutr = new PISOutr();
					pis.setPISOutr(pisoutr);
					
					pisoutr.setCST(tipocobrancapis.getCdnfe());
					pisoutr.setVPIS(item.getValorpis().getValue().doubleValue());
					
					if(item.getTipocalculopis().equals(Tipocalculo.EM_VALOR)){
						pisoutr.setQBCProd(item.getQtdevendidapis());
						pisoutr.setVAliqProd(item.getAliquotareaispis().getValue().doubleValue());
					} else {
						pisoutr.setVBC(item.getValorbcpis().getValue().doubleValue()); 
						pisoutr.setPPIS(item.getPis());
					}
				}
			}
			
			cofins = new COFINS();
			imposto.setCOFINS(cofins);
			
			tipocobrancacofins = item.getTipocobrancacofins();
			if(tipocobrancacofins != null){
				if( tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_01) || 
						tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_02)){
					
					cofinsAliq = new COFINSAliq();
					cofins.setCOFINSAliq(cofinsAliq);
					
					cofinsAliq.setCST(tipocobrancacofins.getCdnfe());
					cofinsAliq.setVBC(item.getValorbccofins().getValue().doubleValue()); 
					cofinsAliq.setPCOFINS(item.getCofins()); 
					cofinsAliq.setVCOFINS(item.getValorcofins().getValue().doubleValue()); 
				}
				else if( tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_04) ||
						tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_05) ||
						tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_06) ||
						tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_07) ||
						tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_08) ||
						tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_09)
						){
					cofinsnt = new COFINSNT();
					cofins.setCOFINSNT(cofinsnt);
					
					cofinsnt.setCST(tipocobrancacofins.getCdnfe());
				}
				else if(tipocobrancacofins.equals(Tipocobrancacofins.OPERACAO_03)){
					cofinsqtde = new COFINSQtde();
					cofins.setCOFINSQtde(cofinsqtde);
					
					cofinsqtde.setCST(tipocobrancacofins.getCdnfe());
					cofinsqtde.setQBCProd(item.getQtdevendidacofins());
					cofinsqtde.setVAliqProd(item.getAliquotareaiscofins().getValue().doubleValue());
					cofinsqtde.setVCOFINS(item.getValorcofins().getValue().doubleValue());
				}
				else {
					cofinsoutr = new COFINSOutr();
					cofins.setCOFINSOutr(cofinsoutr);
					
					cofinsoutr.setCST(tipocobrancacofins.getCdnfe());
					cofinsoutr.setVCOFINS(item.getValorcofins().getValue().doubleValue());
					
					if(item.getTipocalculocofins().equals(Tipocalculo.EM_VALOR)){
						cofinsoutr.setQBCProd(item.getQtdevendidacofins());
						cofinsoutr.setVAliqProd(item.getAliquotareaiscofins().getValue().doubleValue());
					} else {
						cofinsoutr.setVBC(item.getValorbccofins().getValue().doubleValue()); 
						cofinsoutr.setPCOFINS(item.getCofins());
					}
				}
			}
			
			if(item.getDadosimportacao() != null && item.getDadosimportacao()){
				II ii = new II();
				imposto.setII(ii);
				
				ii.setVBC(item.getValorbcii() != null ? item.getValorbcii().getValue().doubleValue() : null);
				ii.setVDespAdu(item.getDespesasaduaneiras() != null ? item.getDespesasaduaneiras().getValue().doubleValue() : null);
				ii.setVII(item.getValorii() != null ? item.getValorii().getValue().doubleValue() : null);
				ii.setVIOF(item.getValoriof() != null ? item.getValoriof().getValue().doubleValue() : null);
			}
			
			if(item.getDadoscombustivel() != null && item.getDadoscombustivel()){
				comb = new Comb(habilitarNfev4);
				prod.setComb(comb);
				
				comb.setcProdANP(item.getCodigoanp() != null ? item.getCodigoanp().getCodigo() : null);
				comb.setDescANP(item.getCodigoanp() != null ? item.getCodigoanp().getProduto() : null);
				comb.setcODIF(item.getCodif() != null && !item.getCodif().equals("") ? item.getCodif() : null);
				comb.setqTemp(item.getQtdetemperatura() != null ? item.getQtdetemperatura() : null);
				comb.setpGLP(item.getPercentualglp() != null ? item.getPercentualglp() : null);
				comb.setpGNn(item.getPercentualgnn() != null ? item.getPercentualgnn() : null);
				comb.setpGNi(item.getPercentualgni() != null ? item.getPercentualgni() : null);
				comb.setvPart(item.getValorpartida() != null ? item.getValorpartida() : null);
				comb.setuFCons(item.getUfconsumo() != null ? item.getUfconsumo().getSigla() : null);
				
				if(item.getQtdebccide() != null && item.getValoraliqcide() != null && item.getValorcide() != null){
					cide = new CIDE();
					comb.setcIDE(cide);
					
					cide.setqBCProd(item.getQtdebccide());
					cide.setvAliqProd(item.getValoraliqcide());
					cide.setvCIDE(item.getValorcide());
				}
			}
			
			if(item.getDadosicmspartilha() != null && item.getDadosicmspartilha()){
				ICMSUFDest icmsufDest = new ICMSUFDest(habilitarNfev4);
				imposto.setiCMSUFDest(icmsufDest);
				
				icmsufDest.setvBCUFDest(item.getValorbcdestinatario() != null ? item.getValorbcdestinatario().getValue().doubleValue() : null);
				icmsufDest.setvBCFCPUFDest(item.getValorbcfcpdestinatario() != null ? item.getValorbcfcpdestinatario().getValue().doubleValue() : null);
				icmsufDest.setpFCPUFDest(item.getFcpdestinatario());
				icmsufDest.setpICMSUFDest(item.getIcmsdestinatario());
				icmsufDest.setpICMSInter(item.getIcmsinterestadual());
				icmsufDest.setpICMSInterPart(item.getIcmsinterestadualpartilha());
				icmsufDest.setvFCPUFDest(item.getValorfcpdestinatario() != null ? item.getValorfcpdestinatario().getValue().doubleValue() : null);
				icmsufDest.setvICMSUFDest(item.getValoricmsdestinatario() != null ? item.getValoricmsdestinatario().getValue().doubleValue() : null);
				icmsufDest.setvICMSUFRemet(item.getValoricmsremetente() != null ? item.getValoricmsremetente().getValue().doubleValue() : null);
			}
			
			det.setInfAdProd(addScape(item.getInfoadicional())); 
			
			listaDet.add(det);
		}
		
		Total total = new Total();
		infNFe.setTotal(total);
		
		ICMSTot icmsTot = new ICMSTot(habilitarNfev4);
		total.setICMSTot(icmsTot);
		
		icmsTot.setVBC(nota.getValorbcicms().getValue().doubleValue()); 
		icmsTot.setVICMS(nota.getValoricms().getValue().doubleValue()); 
		icmsTot.setvICMSDeson(nota.getValordesoneracao() != null ? nota.getValordesoneracao().getValue().doubleValue() : 0d);
		icmsTot.setvFCPUFDest(nota.getValorfcpdestinatario() != null ? nota.getValorfcpdestinatario().getValue().doubleValue() : 0d); 
		icmsTot.setvICMSUFDest(nota.getValoricmsdestinatario() != null ? nota.getValoricmsdestinatario().getValue().doubleValue() : 0d); 
		icmsTot.setvICMSUFRemet(nota.getValoricmsremetente() != null ? nota.getValoricmsremetente().getValue().doubleValue() : 0d); 
		icmsTot.setVBCST(nota.getValorbcicmsst().getValue().doubleValue());
		icmsTot.setVST(nota.getValoricmsst().getValue().doubleValue());
		icmsTot.setVProd(nota.getValorprodutos().getValue().doubleValue());
		icmsTot.setVFrete(nota.getValorfrete().getValue().doubleValue());
		icmsTot.setVPIS(nota.getValorpis().getValue().doubleValue());
		icmsTot.setVDesc(nota.getValordesconto() != null ? nota.getValordesconto().getValue().doubleValue() : 0d);
		icmsTot.setVSeg(nota.getValorseguro().getValue().doubleValue()); 
		icmsTot.setVII(nota.getValorii() != null ? nota.getValorii().getValue().doubleValue() : 0d);
		icmsTot.setVIPI(nota.getValoripi().getValue().doubleValue());
		icmsTot.setVCOFINS(nota.getValorcofins().getValue().doubleValue());
		icmsTot.setVOutro(nota.getOutrasdespesas().getValue().doubleValue()); 
		icmsTot.setVNF(nota.getValor().getValue().doubleValue());
		icmsTot.setvTotTrib(nota.getValortotaltributos() != null ? nota.getValortotaltributos().getValue().doubleValue() : null);
		
		if(nota.getValorfcp() != null && valorFCPSubtrair != null && valorFCPSubtrair.getValue().doubleValue() > 0){
			icmsTot.setvFCP(nota.getValorfcp().subtract(valorFCPSubtrair).getValue().doubleValue());
		}else {
			icmsTot.setvFCP(nota.getValorfcp() != null ? nota.getValorfcp().getValue().doubleValue() : null);
		}
		icmsTot.setvFCPST(nota.getValorfcpretidost() != null ? nota.getValorfcpretidost().getValue().doubleValue() : null);
		icmsTot.setvFCPSTRet(nota.getValorfcpretidostanteriormente() != null ? nota.getValorfcpretidostanteriormente().getValue().doubleValue() : null);
		icmsTot.setvIPIDevol(nota.getValoripidevolvido() != null ? nota.getValoripidevolvido().getValue().doubleValue() : null);
		
		if(haveServico){
			ISSQNtot issqNtot = new ISSQNtot();
			total.setISSQNtot(issqNtot);
			
			issqNtot.setVBC(nota.getValorbciss() != null && nota.getValorbciss().getValue().doubleValue() > 0 ? nota.getValorbciss().getValue().doubleValue() : null);
			issqNtot.setVISS(nota.getValoriss() != null && nota.getValoriss().getValue().doubleValue() > 0 ? nota.getValoriss().getValue().doubleValue() : null);
			issqNtot.setVCOFINS(nota.getValorcofinsservicos() != null && nota.getValorcofinsservicos().getValue().doubleValue() > 0 ? nota.getValorcofinsservicos().getValue().doubleValue() : null);
			issqNtot.setVPIS(nota.getValorpisservicos() != null && nota.getValorpisservicos().getValue().doubleValue() > 0 ? nota.getValorpisservicos().getValue().doubleValue() : null);
			issqNtot.setVServ(nota.getValorservicos() != null && nota.getValorservicos().getValue().doubleValue() > 0 ? nota.getValorservicos().getValue().doubleValue() : null);
			issqNtot.setdCompet(nota.getDtcompetenciaservico() != null ? nota.getDtcompetenciaservico() : null);
			issqNtot.setvDeducao(nota.getValordeducaoservico() != null && nota.getValordeducaoservico().getValue().doubleValue() > 0 ? nota.getValordeducaoservico().getValue().doubleValue() : null);
			issqNtot.setvOutro(nota.getValoroutrasretencoesservico() != null && nota.getValoroutrasretencoesservico().getValue().doubleValue() > 0 ? nota.getValoroutrasretencoesservico().getValue().doubleValue() : null);
			issqNtot.setvDescCond(nota.getValordescontocondicionadoservico() != null && nota.getValordescontocondicionadoservico().getValue().doubleValue() > 0 ? nota.getValordescontocondicionadoservico().getValue().doubleValue() : null);
			issqNtot.setvDescIncond(nota.getValordescontoincondicionadoservico() != null && nota.getValordescontoincondicionadoservico().getValue().doubleValue() > 0 ? nota.getValordescontoincondicionadoservico().getValue().doubleValue() : null);
			issqNtot.setvISSRet(nota.getValorissretido() != null && nota.getValorissretido().getValue().doubleValue() > 0 ? nota.getValorissretido().getValue().doubleValue() : null);
			issqNtot.setcRegTrib(nota.getRegimetributacao() != null ? nota.getRegimetributacao().getCodigonfe() : null);
			
			if(nota.getValorservicos() != null && nota.getValorservicos().getValue().doubleValue() > 0){
				icmsTot.setVProd(icmsTot.getVProd() - nota.getValorservicos().getValue().doubleValue());
			}
		}
		
		RetTrib retTrib = new RetTrib();
		total.setRetTrib(retTrib);
		
		retTrib.setVRetPIS(nota.getValorpisretido() != null && nota.getValorpisretido().getValue().doubleValue() > 0d ? nota.getValorpisretido().getValue().doubleValue() : null);
		retTrib.setVRetCOFINS(nota.getValorcofinsretido() != null && nota.getValorcofinsretido().getValue().doubleValue() > 0d  ? nota.getValorcofinsretido().getValue().doubleValue() : null); 
		retTrib.setVRetCSLL(nota.getValorcsll() != null && nota.getValorcsll().getValue().doubleValue() > 0d  ? nota.getValorcsll().getValue().doubleValue() : null); 
		retTrib.setVBCIRRF(nota.getValorbcirrf() != null && nota.getValorbcirrf().getValue().doubleValue() > 0d  ? nota.getValorbcirrf().getValue().doubleValue() : null);
		retTrib.setVIRRF(nota.getValorirrf() != null && nota.getValorirrf().getValue().doubleValue() > 0d  ? nota.getValorirrf().getValue().doubleValue() : null); 
		retTrib.setVBCRetPrev(nota.getValorbcprevsocial() != null && nota.getValorbcprevsocial().getValue().doubleValue() > 0d  ? nota.getValorbcprevsocial().getValue().doubleValue() : null); 
		retTrib.setVRetPrev(nota.getValorprevsocial() != null && nota.getValorprevsocial().getValue().doubleValue() > 0d  ? nota.getValorprevsocial().getValue().doubleValue() : null); 

		String notaempenho = nota.getNumeronotaempenho();
		String contrato = nota.getNumerocontrato();
		String pedido = nota.getNumeropedido();
		if((notaempenho != null && !notaempenho.trim().equals("")) ||
				(contrato != null && !contrato.trim().equals("")) ||
				(pedido != null && !pedido.trim().equals(""))){
			Compra compra = new Compra();
			infNFe.setCompra(compra);
			
			
			compra.setXNEmp(notaempenho);
			compra.setXPed(pedido);
			compra.setXCont(contrato);
		}
		
		String localembarque = nota.getLocalembarque();
		Uf ufembarque = nota.getUfembarque();
		if((localembarque != null && !localembarque.trim().equals("")) && ufembarque != null){
			Exporta exporta = new Exporta();
			infNFe.setExporta(exporta);
			
			exporta.setUFSaidaPais(ufembarque.getSigla());
			exporta.setXLocExporta(localembarque);
		}
		
		Transp transp = new Transp();
		infNFe.setTransp(transp);
		
		transp.setModFrete(nota.getResponsavelfrete() != null ? nota.getResponsavelfrete().getCdnfe() : ResponsavelFrete.EMITENTE.getCdnfe());
		
		if(nota.getCadastrartransportador() != null && nota.getCadastrartransportador() && nota.getTransportador() == null && nota.getEmpresa() != null){
			Empresa empresaAux = empresaService.loadWithTransportador(nota.getEmpresa());
			if(empresaAux != null){
				transportador = empresaAux.getTransportador();
				if(transportador != null){
					enderTransportador = empresaAux.getTransportador().getEndereco();
				}
			}
		}
		
		if(transportador != null && nota.getCadastrartransportador()!=null && nota.getCadastrartransportador()){
			Transporta transporta = new Transporta();
			transp.setTransporta(transporta);
			
			if(transportador.getTipopessoa().equals(Tipopessoa.PESSOA_FISICA)){
				transporta.setCPF(transportador.getCpf() != null ? transportador.getCpf().getValue() : null);
			} else if(transportador.getTipopessoa().equals(Tipopessoa.PESSOA_JURIDICA)){
				transporta.setCNPJ(transportador.getCnpj() != null ? transportador.getCnpj().getValue() : null);
			}
			
			transporta.setXNome(transportador.getRazaosocial() != null && !transportador.getRazaosocial().equals("") ?  addScape(transportador.getRazaosocial()) : addScape(transportador.getNome()));
			if(transportador.getInscricaoestadual() != null) transporta.setIE(StringUtils.soNumero(transportador.getInscricaoestadual()));
			transporta.setXEnder(enderTransportador != null ? addScape(enderTransportador.getLogradouroCompletoComBairroSemMunicipio()) : null);
			transporta.setXMun(enderTransportador != null && enderTransportador.getMunicipio() != null ? addScape(enderTransportador.getMunicipio().getNome()) : null);
			transporta.setUF(enderTransportador != null && enderTransportador.getMunicipio() != null ? enderTransportador.getMunicipio().getUf().getSigla() : null);
			
			if(nota.getTransporteveiculo() != null && 
					nota.getTransporteveiculo() && 
					nota.getPlacaveiculo() != null && 
					!nota.getPlacaveiculo().equals("") &&
					nota.getUfveiculo() != null){
				VeicTransp veicTransp = new VeicTransp();
				transp.setVeicTransp(veicTransp);
			
				veicTransp.setPlaca(nota.getPlacaveiculo().toUpperCase().replaceAll("-", ""));
				veicTransp.setUF(nota.getUfveiculo() != null ? nota.getUfveiculo().getSigla() : null);
				veicTransp.setRNTC(addScape(nota.getRntc()));
			}
			
			Listavol listavol = new Listavol();
			transp.setListavol(listavol);
			
			List<Vol> listVol = new ArrayList<Vol>();
			Vol vol = new Vol();
			
			vol.setQVol(nota.getQtdevolume());
			vol.setEsp(nota.getEspvolume());
			vol.setMarca(nota.getMarcavolume());
			vol.setNVol(nota.getNumvolume());
			vol.setPesoL(nota.getPesoliquido());
			vol.setPesoB(nota.getPesobruto());
			
			listVol.add(vol);
			listavol.setListavol(listVol);
		}
		
		if(isNfe){
			if(nota.getCadastrarcobranca() != null && nota.getCadastrarcobranca()){
				Cobr cobr = new Cobr();
				infNFe.setCobr(cobr);
				
				Fat fat = new Fat();
				cobr.setFat(fat);
				
				if(org.apache.commons.lang.StringUtils.isNotBlank(nota.getNumerofatura())) fat.setNFat(nota.getNumerofatura());
				else fat.setNFat(nota.getNumero());
				
				if(nota.getValordescontofatura() != null && nota.getValordescontofatura().getValue().doubleValue() > 0) 
					fat.setVDesc(nota.getValordescontofatura().getValue().doubleValue());
				else if(habilitarNfev4) fat.setVDesc(0d);
				
				if(nota.getValorliquidofatura() != null && nota.getValorliquidofatura().getValue().doubleValue() > 0) 
					fat.setVLiq(nota.getValorliquidofatura().getValue().doubleValue());
				else if(habilitarNfev4) fat.setVLiq(0d);
				
				if(nota.getValororiginalfatura() != null && nota.getValororiginalfatura().getValue().doubleValue() > 0) 
					fat.setVOrig(nota.getValororiginalfatura().getValue().doubleValue());
				else if(habilitarNfev4) fat.setVOrig(0d);
				
				Listadup listadup = new Listadup();
				List<Dup> listDup = new ArrayList<Dup>();
				Dup dup;
				int contador = 1;
				for (Notafiscalprodutoduplicata d : listaDuplicata) {
					dup = new Dup();
	
					if(habilitarNfev4) dup.setNDup(StringUtils.stringCheia(contador + "", "0", 3, false));
					else dup.setNDup(d.getNumero() != null ? d.getNumero().trim() : null);
					
					dup.setDVenc(d.getDtvencimento());
					if(d.getValor() != null) dup.setVDup(d.getValor().getValue().doubleValue());
					
					listDup.add(dup);
					contador++;
				}
				listadup.setListadup(listDup);
				cobr.setListadup(listadup);
			}
		}
		
		Pag pag = new Pag();
		infNFe.setPag(pag);
	
		ListadetPag listadetPag = new ListadetPag();
		List<DetPag> listDetPag = new ArrayList<DetPag>();
		if(!finalidadenfe.equals(Finalidadenfe.AJUSTE) && 
				!finalidadenfe.equals(Finalidadenfe.DEVOLUCAO) && 
				listaDuplicata != null && 
				listaDuplicata.size() > 0){
			for (Notafiscalprodutoduplicata d : listaDuplicata) {
				DetPag detPag = new DetPag();
				detPag.setIndPag(nota.getFormapagamentonfe() != null ? nota.getFormapagamentonfe().getValue() : Formapagamentonfe.A_VISTA.getValue());
				
				Documentotipo documentotipo = d.getDocumentotipo();
				if(documentotipo != null && documentotipo.getMeiopagamentonfe() != null){
					detPag.settPag(documentotipo.getMeiopagamentonfe().getValue());
					
					if(documentotipo != null && 
						documentotipo.getMeiopagamentonfe() != null && 
						(documentotipo.getMeiopagamentonfe().equals(MeioPagamentoNFe.CARTAO_CREDITO) || 
							documentotipo.getMeiopagamentonfe().equals(MeioPagamentoNFe.CARTAO_DEBITO)) &&
						(org.apache.commons.lang.StringUtils.isNotBlank(d.getAdquirente()) &&
							org.apache.commons.lang.StringUtils.isNotBlank(d.getAutorizacao()) &&
							org.apache.commons.lang.StringUtils.isNotBlank(d.getBandeira()))){
						
						Card card = new Card();
						detPag.setCard(card);
						
						BandeiraCartaoNFe bandeira = BandeiraCartaoNFe.getByNameIntegracao(d.getBandeira());
						if(bandeira == null){
							bandeira = BandeiraCartaoNFe.getByNameIntegracao(d.getBandeira().split(" ")[0]);
						}
						String cnpjAdquirente = documentotipo.getCnpjcredenciadoranfe() != null ? documentotipo.getCnpjcredenciadoranfe().getValue() : null;
						Set<Configuracaotefadquirente> listaConfiguracaotefadquirente = configuracaotef.getListaConfiguracaotefadquirente();
						for (Configuracaotefadquirente configuracaotefadquirente : listaConfiguracaotefadquirente) {
							String adquirente = configuracaotefadquirente.getAdquirente();
							if(adquirente.equalsIgnoreCase(d.getAdquirente()) && 
									configuracaotefadquirente.getFornecedor() != null &&
									configuracaotefadquirente.getFornecedor().getCnpj() != null){
								cnpjAdquirente = configuracaotefadquirente.getFornecedor().getCnpj().getValue();
							}
						}
						
						card.setTpIntegra(TipoIntegracaoNFe.EXIGIVEL.getValue());
						card.setcNPJ(cnpjAdquirente);
						card.settBand(bandeira != null ? bandeira.getValue() : null);
						card.setcAut(d.getAutorizacao());
					} else if(documentotipo.getTipointegracaonfe() != null ||
						documentotipo.getCnpjcredenciadoranfe() != null ||
						documentotipo.getBandeiracartaonfe() != null){
						Card card = new Card();
						detPag.setCard(card);
						
						card.setTpIntegra(documentotipo.getTipointegracaonfe() != null ? documentotipo.getTipointegracaonfe().getValue() : null);
						card.setcNPJ(documentotipo.getCnpjcredenciadoranfe() != null ? documentotipo.getCnpjcredenciadoranfe().getValue() : null);
						card.settBand(documentotipo.getBandeiracartaonfe() != null ? documentotipo.getBandeiracartaonfe().getValue() : null);
					}
				} else {
					detPag.settPag(MeioPagamentoNFe.OUTROS.getValue());
				}
				
				detPag.setvPag(d.getValor().getValue().doubleValue());
				listDetPag.add(detPag);
			}
		} else {
			DetPag detPag = new DetPag();
			detPag.settPag(MeioPagamentoNFe.SEM_PAGAMENTO.getValue());
			detPag.setvPag(0d);
			listDetPag.add(detPag);
		}
		listadetPag.setListadetPag(listDetPag);
		pag.setListadetPag(listadetPag);
		
		
		InfAdic infAdic = new InfAdic();
		infNFe.setInfAdic(infAdic);
		
		infAdic.setInfCpl(addScape(nota.getInfoadicionalcontrib()));
		
		String tributos = "";
		
		if (parametrogeralService.getBoolean("EXIBIR_TRIBUTOS_IBPT_DANFE") || ModeloDocumentoFiscalEnum.NFCE.equals(nota.getModeloDocumentoFiscalEnum())) {
			if (nota.getValorTotalImpostoFederal() != null && nota.getValorTotalImpostoFederal().getValue().doubleValue() > 0d ||
					nota.getValorTotalImpostoEstadual() != null && nota.getValorTotalImpostoEstadual().getValue().doubleValue() > 0d ||
					nota.getValorTotalImpostoMunicipal() != null && nota.getValorTotalImpostoMunicipal().getValue().doubleValue() > 0d) {
				tributos = "Trib. aprox. R$: " + 
					SinedUtil.descriptionDecimal(nota.getValorTotalImpostoFederal() != null ? nota.getValorTotalImpostoFederal().getValue().doubleValue() : 0d, true) + " Federal, " +
					SinedUtil.descriptionDecimal(nota.getValorTotalImpostoEstadual() != null ? nota.getValorTotalImpostoEstadual().getValue().doubleValue() : 0d, true) + " Estadual e " +
					SinedUtil.descriptionDecimal(nota.getValorTotalImpostoMunicipal() != null ? nota.getValorTotalImpostoMunicipal().getValue().doubleValue() : 0d, true) + " Municipal (Fonte: IBPT)";
			}
		}
		
		if(exibirSTJaPaga){
			StringBuilder infoComplementar = new StringBuilder(nota.getInfoadicionalfisco() != null ? addScape(nota.getInfoadicionalfisco()) : "");
			
			if(infoComplementar.length() > 0) infoComplementar.append(" ");
			infoComplementar.append("(BASE ST J� PAGA: R$ ").append(totalBCSTJaPaga).append(" ").append("ST J� PAGA: R$ ").append(totalSTJaPaga).append(")");
			infoComplementar.append("\n");
			infoComplementar.append(tributos);
			
			infAdic.setInfAdFisco(addScape(infoComplementar.toString()));
		} else {
			StringBuilder infoComplementar = new StringBuilder(nota.getInfoadicionalfisco() != null ? addScape(nota.getInfoadicionalfisco()) : "");
			infoComplementar.append(" " + tributos);
			
			infAdic.setInfAdFisco(addScape(infoComplementar.toString()));
		}
		
		if (configuracaonfe.getUf() != null && ("AM".equals(configuracaonfe.getUf().getSigla()) || "MS".equals(configuracaonfe.getUf().getSigla()) || "PE".equals(configuracaonfe.getUf().getSigla())
				|| "PR".equals(configuracaonfe.getUf().getSigla()) || "SC".equals(configuracaonfe.getUf().getSigla()) || "SP".equals(configuracaonfe.getUf().getSigla()))) {
			InfRespTec infRespTec = new InfRespTec();
			
			try {
				InputStream inputstream = QuartzContextListenner.class.getResourceAsStream("/WEB-INF/classes/responsaveltecnico.properties");
				
				Properties properties = new Properties();
				properties.load(inputstream);
				
				for (Object key : properties.keySet()) {
					String uf = (String) key;
					
					if (uf != null && configuracaonfe.getUf() != null && configuracaonfe.getUf().getSigla().equals(uf)) {
						String informacoes = properties.getProperty(uf);
						String[] dados = informacoes.split("\\|", -1);
						
						infRespTec.setCnpj(dados[0]);
						infRespTec.setxContato(dados[1]);
						infRespTec.setEmail(dados[2]);
						infRespTec.setFone(dados[3]);
						infRespTec.setIdCsrt(dados[4]);
						infRespTec.setCsrt(dados[5]);
						
						break;
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
				request.addError("Erro no servi�o para ter acesso aos dados do respons�vel fiscal.");
			}
			
			if (org.apache.commons.lang.StringUtils.isNotEmpty(infRespTec.getCsrt()) && org.apache.commons.lang.StringUtils.isNotEmpty(chaveAcesso)) {
				try {
					infRespTec.setHashCsrt(CriptografiaUtil.getHashCsrt(infRespTec.getCsrt(), chaveAcesso));
				} catch (Exception e) {
					e.printStackTrace();
					request.addError("Erro na cria��o do conte�do da tag HashCsrt.");
				}
			}
			
			if(infRespTec.getCnpj() != null && !"".equals(infRespTec.getCnpj())){
				infNFe.setInfRespTec(infRespTec);
			}
		}
		
		return nfe;
	}
	
	private String getNotaItemForErro(Integer numero, Notafiscalprodutoitem item) {
		return (numero != null ? "Nota " + numero : "") + (item.getMaterial() != null && item.getMaterial().getNome() != null ? ". Item: " + item.getMaterial().getNome() : "");
	}
	
	public ModelAndView downloadNfeZip(WebRequestContext request) throws JDOMException, IOException{
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Arquivo> lista = new ArrayList<Arquivo>();
		
		List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNotaAndCancelada(whereIn);
		HashMap<br.com.linkcom.sined.geral.bean.Nota, Arquivonfnota> mapNotaArquivoNfNota = new HashMap<br.com.linkcom.sined.geral.bean.Nota, Arquivonfnota>();
		for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
			if(arquivonfnota.getNota() == null || arquivonfnota.getNota().getCdNota() == null){
				request.addError("Nenhuma nota encontrada.");
				return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
			}
			if(validarDownloadXml(arquivonfnota)){
				if(!mapNotaArquivoNfNota.containsKey(arquivonfnota.getNota())){
					mapNotaArquivoNfNota.put(arquivonfnota.getNota(), null);
				}
			}else {
				mapNotaArquivoNfNota.put(arquivonfnota.getNota(), arquivonfnota);
			}
		}
		
		if(mapNotaArquivoNfNota.size() > 0){
			boolean erro = false;
			for(br.com.linkcom.sined.geral.bean.Nota nota : mapNotaArquivoNfNota.keySet()){
				if(mapNotaArquivoNfNota.get(nota) == null){
					request.addError("O arquivo XML da NF-e " + (org.apache.commons.lang.StringUtils.isNotBlank(nota.getNumero()) ? nota.getNumero() : nota.getCdNota()) + " n�o est� na situacao '"+Arquivonfsituacao.PROCESSADO_COM_SUCESSO.getNome() + "' ou n�o tem informa��o de chave de acesso/data do processamento/protocolo de autoriza��o.");
					erro = true;
				}
			}
			if(erro){
				return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
			}
		}
		
		for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
			lista.add(arquivonfService.getArquivoxmlEstadoByArquivonfnota(arquivonfnota));
		}
		
		List<String> listaNomes = new ArrayList<String>();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(byteArrayOutputStream);
		ZipEntry zipEntry;
		for (Arquivo arquivo : lista) {
			String name = arquivo.getName();
			String name_aux = new String(name);
			
			int i = 1;
			while(listaNomes.contains(name_aux)){
				name_aux = "(" + i + ")" + "-" + name;
				i++;
			}
			listaNomes.add(name_aux);
			
			zipEntry = new ZipEntry(name_aux);
			zipEntry.setSize(arquivo.getSize());
			zip.putNextEntry(zipEntry);
			zip.write(arquivo.getContent());
		}
		
		zip.closeEntry();
		zip.close();
		
		Resource resource = new Resource("application/zip", "nfe_" + SinedUtil.datePatternForReport() + ".zip", byteArrayOutputStream.toByteArray());
		return new ResourceModelAndView(resource);
	}
	
	private boolean validarDownloadXml(Arquivonfnota arquivonfnota) {
		return arquivonfnota.getArquivonf() ==null || arquivonfnota.getArquivonf().getArquivonfsituacao() ==null ||
				(!arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO) && 
				  (arquivonfnota.getChaveacesso() == null || 
				  arquivonfnota.getProtocolonfe() == null ||
				  arquivonfnota.getDtprocessamento() == null)
				  );
	}
	
	public ModelAndView downloadNfe(WebRequestContext request, Notafiscalproduto notafiscalproduto) {
		
		try{
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProduto(notafiscalproduto);
			if(arquivonfnota == null){
				arquivonfnota = arquivonfnotaService.findByNotaProdutoAndCancelada(notafiscalproduto);
			}	
			if(validarDownloadXml(arquivonfnota)){
				request.addError("O arquivo XML na NF-e n�o est� na situacao '"+Arquivonfsituacao.PROCESSADO_COM_SUCESSO.getNome() + "' ou n�o tem informa��o de chave de acesso/protocolo/n�mero.");
				return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
			}
			Arquivo arquivoxml = arquivonfService.getArquivoxmlEstadoByArquivonfnota(arquivonfnota);
			
			Resource resource = new Resource(arquivoxml.getContenttype(), arquivoxml.getName(), arquivoxml.getContent());
			return new ResourceModelAndView(resource);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("N�o foi poss�vel obter o arquivo XML na NF-e.");
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
		}
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView downloadXmlCartacorrecao(WebRequestContext request, Notafiscalprodutocorrecao notafiscalprodutocorrecao){
		try {
			notafiscalprodutocorrecao = notafiscalprodutocorrecaoService.loadForEntrada(notafiscalprodutocorrecao);
			
			Arquivo arquivoxml = notafiscalprodutocorrecao.getArquivoxml();
			if(arquivoxml == null){
				Arquivo arquivoenvio = arquivoService.loadWithContents(notafiscalprodutocorrecao.getArquivoenvio());
				Arquivo arquivoretorno = arquivoService.loadWithContents(notafiscalprodutocorrecao.getArquivoretorno());
				
				String arquivoenviostr1 = new String(arquivoenvio.getContent(), "UTF-8");
				String arquivoretornostr2 = new String(arquivoretorno.getContent(), "UTF-8");
				
				arquivoenviostr1 = Util.strings.tiraAcento(arquivoenviostr1);
				arquivoretornostr2 = Util.strings.tiraAcento(arquivoretornostr2);
				
				SAXBuilder sb = new SAXBuilder();  
				Document d_envio = sb.build(new ByteArrayInputStream(arquivoenviostr1.getBytes()));
				Document d_retorno = sb.build(new ByteArrayInputStream(arquivoretornostr2.getBytes()));

				Element rootElement_envio = d_envio.getRootElement();
				Element evento_envio = SinedUtil.getChildElement("evento", rootElement_envio.cloneContent());
				Document doc_envio = new Document(evento_envio);
				String evento = new XMLOutputter().outputString(doc_envio);
				
				Element rootElement_retorno = d_retorno.getRootElement();
				Element retEvento_retorno = SinedUtil.getChildElement("retEvento", rootElement_retorno.cloneContent());
				Document doc_retorno = new Document(retEvento_retorno);
				String retEvento = new XMLOutputter().outputString(doc_retorno);
				
				evento = evento.replaceAll("<\\?.+\\?>", "");
				retEvento = retEvento.replaceAll("<\\?.+\\?>", "");
				
				String s = 
					"<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
					"<procEventoNFe versao=\"1.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\">" +
						evento + 
						retEvento +
					"</procEventoNFe>";
				
				arquivoxml = new Arquivo(s.getBytes(), notafiscalprodutocorrecao.getData().getTime() + "-procEventoNfe.xml", "text/xml");
				
				notafiscalprodutocorrecao.setArquivoxml(arquivoxml);
				arquivoDAO.saveFile(notafiscalprodutocorrecao, "arquivoxml");
				notafiscalprodutocorrecaoService.updateArquivoxml(notafiscalprodutocorrecao);
			} else {
				arquivoxml = arquivoService.loadWithContents(arquivoxml);
			}
			
			Resource resource = new Resource(arquivoxml.getContenttype(), arquivoxml.getName(), arquivoxml.getContent());
			return new ResourceModelAndView(resource);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("N�o foi poss�vel obter o arquivo XML da Carta de corre��o.");
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
		}
	}
	
	/**
	 * Action ara abrir a popup para o registro de CC-e.
	 *
	 * @param request
	 * @return
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView registrarCCe(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA)){
			request.addError("A(s) nota(s) fiscal(is) tem que estar na situa��o 'NF-E EMITIDA' ou 'NF-E LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		RegistrarCCeBean bean = new RegistrarCCeBean();
		bean.setWhereIn(whereIn);
		
		return new ModelAndView("direct:/process/popup/registrarCCe", "bean", bean);
	}
	
	/**
	 * Processa a cria��o da carta de corre��o.
	 *
	 * @param request
	 * @param bean
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public void processaRegistrarCCe(WebRequestContext request, RegistrarCCeBean bean){
		if(notaService.haveNFDiferenteStatus(bean.getWhereIn(), NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA)){
			request.addError("A(s) nota(s) fiscal(is) tem que estar na situa��o 'NF-E EMITIDA' ou 'NF-E LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		try {
			long timeAtual = System.currentTimeMillis();
			
			Notafiscalproduto nf = new Notafiscalproduto(Integer.parseInt(bean.getWhereIn()));
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProduto(nf);
			
			Configuracaonfe configuracaonfe = arquivonfnota.getArquivonf().getConfiguracaonfe();
			configuracaonfe = configuracaonfeService.loadForEntrada(configuracaonfe); 
			
			Empresa empresa = empresaService.carregaEmpresa(configuracaonfe.getEmpresa());
			
			Contigencianfe contigencianfe = contigencianfeService.getContigenciaAtual(configuracaonfe.getUf());
			if(contigencianfe != null){
				request.addError("Ambiente de contig�ncia habilitado. N�o � poss�vel enviar carta de corre��o para o ambiente de contig�ncia.");
				SinedUtil.fechaPopUp(request);
				return;
			}
			
			Integer contadorevento = configuracaonfe.getContadorloteevento();
			if(contadorevento == null) contadorevento = 0;
			contadorevento++;
			configuracaonfeService.updateContadorloteevento(configuracaonfe, contadorevento);
			
			Long contadorcartacorrecao = notafiscalprodutocorrecaoService.getProximoContadorCartaCorrecao(nf.getCdNota());
			if(contadorcartacorrecao == null || contadorcartacorrecao.equals(0L)) contadorcartacorrecao = 1L;
			
//			Integer contadorcartacorrecao = configuracaonfe.getContadorcartacorrecao();
//			if(contadorcartacorrecao == null) contadorcartacorrecao = 0;
//			contadorcartacorrecao++;
//			configuracaonfeService.updateContadorcartacorrecao(configuracaonfe, contadorcartacorrecao);
			
			String chaveAcesso = arquivonfnota.getChaveacesso();
			String tpEvento = "110110";
			Integer nSeqEvento = contadorcartacorrecao.intValue();
			
			String cOrgao = configuracaonfe.getUf().getCdibge() + "";
			boolean ambienteProducao = !configuracaonfe.getPrefixowebservice().name().endsWith("_HOM");
			
			String id = "ID" + tpEvento + chaveAcesso + br.com.linkcom.neo.util.StringUtils.stringCheia(nSeqEvento.toString(), "0", 2, false);
			
			EnvEvento envEvento = new EnvEvento();
			
			envEvento.setIdLote(br.com.linkcom.neo.util.StringUtils.stringCheia(contadorevento.toString(), "0", 15, false));
			envEvento.setVersao("1.00");
			
			Listaevento listaevento = new Listaevento();
			envEvento.setListaevento(listaevento);
			
			List<Evento> listEvento = new ArrayList<Evento>();
			
			Evento evento = new Evento();
			listEvento.add(evento);
			
			listaevento.setListaevento(listEvento);
			
			evento.setVersao("1.00");
			
			InfEvento infEvento = new InfEvento();
			evento.setInfEvento(infEvento);
			
			infEvento.setId(id);
			infEvento.setcOrgao(cOrgao);
			infEvento.setTpAmb(ambienteProducao ? 1 : 2);
			infEvento.setcNPJ(empresa.getCnpj().getValue());
//			infEvento.setcPF(cPF); SOMENTE A EMPRESA QUE ENVIA A CARTA DE CORRE��O
			infEvento.setChNFe(chaveAcesso);
			infEvento.setDhEvento(new Date(timeAtual));
			infEvento.setTpEvento(tpEvento);
			infEvento.setnSeqEvento(nSeqEvento);
			infEvento.setVerEvento("1.00");
			
			DetEvento detEvento = new DetEvento(true, false, false);
			infEvento.setDetEvento(detEvento);
			
			String correcao = bean.getCorrecao();
			correcao = correcao.replaceAll("\\r?\\n", " ");
			correcao = addScape(correcao);
			correcao = correcao.trim();
			
			detEvento.setVersao("1.00");
			detEvento.setDescEvento("Carta de Correcao");
			detEvento.setxCorrecao(correcao);
			detEvento.setxCondUso("A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, " +
									"de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na " +
									"emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis " +
									"que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, " +
									"quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique " +
									"mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.");
			
			Notafiscalprodutocorrecao notafiscalprodutocorrecao = new Notafiscalprodutocorrecao();
			
			String string = new String(Util.strings.tiraAcento(envEvento.toString()).getBytes(), "UTF-8");
			Arquivo arquivo = new Arquivo(string.getBytes(), "cceenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			notafiscalprodutocorrecao.setArquivoenvio(arquivo);
			
			arquivoDAO.saveFile(notafiscalprodutocorrecao, "arquivoenvio");
			arquivoService.saveOrUpdate(arquivo);
			
			notafiscalprodutocorrecao.setData(new Timestamp(timeAtual));
			notafiscalprodutocorrecao.setNotafiscalproduto(nf);
			notafiscalprodutocorrecao.setNotafiscalprodutocorrecaosituacao(Notafiscalprodutocorrecaosituacao.GERADA);
			notafiscalprodutocorrecao.setTexto(correcao);
			notafiscalprodutocorrecao.setSequencialevento(contadorcartacorrecao.intValue());
			
			notafiscalprodutocorrecaoService.saveOrUpdate(notafiscalprodutocorrecao);
			
			request.addMessage("Carta de corre��o criada com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			request.addError("Erro na cria��o do arquivo de registro de CC-e.");
		}
		
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * Action para abrir a popup para o cancelamento de NF-e
	 *
	 * @param request
	 * @return
	 * @since 04/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cancelamentoNfe(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(notaService.haveNFWithStatus(whereIn, NotaStatus.CANCELADA, NotaStatus.NFE_CANCELANDO)){
			request.addError("N�o � permitido cancelar nota com a situa��o 'Cancelada' ou 'Cancelando NF-e'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA)){
			request.addError("Para realizar esta opera��o, � necess�rio Cancelar as contas a receber associadas a esta Nota Fiscal. Favor verifcar.");
//			request.addError("A(s) nota(s) fiscal(is) tem que estar na situa��o 'NF-E EMITIDA' ou 'NF-E LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		if(notafiscalprodutoService.haveNotaCriadasViaIntegracaoECF(whereIn)){
			request.addError("Nota importada via integra��o com ECF. Favor efetuar o cancelamento no local de origem.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Expedicaoitem> listaExpedicaoItem = expedicaoItemService.findByWhereInNota(whereIn,false);
		if(SinedUtil.isListNotEmpty(listaExpedicaoItem)){
			Expedicaoitem item = listaExpedicaoItem.get(0);
			String msg = "nota com vinculo na expedi��o. <"+item.getExpedicao().getCdexpedicao()+">";
			if(item.getNotaFiscalProdutoItem() != null && item.getNotaFiscalProdutoItem().getNotafiscalproduto() != null){
				msg = "A expedi��o " + item.getExpedicao().getCdexpedicao() +" est� vinculada a nota fiscal. Cancele a expedi��o ou remova a nota antes de cancelar a nota fiscal "+ item.getNotaFiscalProdutoItem().getNotafiscalproduto().getNumero(); 
			}
			request.addError(msg);
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		boolean origemVenda = notaService.existOrigem(whereIn);
		request.setAttribute("origemVenda", origemVenda);
		
		CancelamentoNfseBean bean = new CancelamentoNfseBean();
		bean.setWhereIn(whereIn);
		
		return new ModelAndView("direct:/process/popup/cancelamentoNfe", "bean", bean);
	}
	
	/**
	 * Processa o fechamento da popup de cancelamento de NF-e e cria��o do XML para ser enviado para a receita
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @since 04/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView processaCancelamentoNfe(WebRequestContext request, CancelamentoNfseBean bean){
		if(notaService.haveNFDiferenteStatus(bean.getWhereIn(), NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA)){
			request.addError("A(s) nota(s) fiscal(is) tem que estar na situa��o 'NF-E EMITIDA' ou 'NF-E LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		try {
			long timeAtual = System.currentTimeMillis();
			
			Notafiscalproduto nf = new Notafiscalproduto(Integer.parseInt(bean.getWhereIn()));
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProduto(nf);
			
			Configuracaonfe configuracaonfe = arquivonfnota.getArquivonf().getConfiguracaonfe();
			configuracaonfe = configuracaonfeService.loadForEntrada(configuracaonfe);
			
			Empresa empresa = empresaService.carregaEmpresaComProprietarioRural(configuracaonfe.getEmpresa());
			
			Integer contadorevento = configuracaonfe.getContadorloteevento();
			if(contadorevento == null) contadorevento = 0;
			contadorevento++;
			configuracaonfeService.updateContadorloteevento(configuracaonfe, contadorevento);
			
			String chaveAcesso = arquivonfnota.getChaveacesso();
			String tpEvento = "110111";
			Integer nSeqEvento = 1;
			
			String cOrgao = configuracaonfe.getUf().getCdibge() + "";
//				if(configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SVAN_HOM) ||
//						configuracaonfe.getPrefixowebservice().equals(Prefixowebservice.SVAN_PROD)){
//					cOrgao = "90";
//				}
			
			String versao = "1.00";
			boolean ambienteProducao = !configuracaonfe.getPrefixowebservice().name().endsWith("_HOM");
			
			String id = "ID" + tpEvento + chaveAcesso + br.com.linkcom.neo.util.StringUtils.stringCheia(nSeqEvento.toString(), "0", 2, false);
			
			EnvEvento envEvento = new EnvEvento();
			
			envEvento.setIdLote(br.com.linkcom.neo.util.StringUtils.stringCheia(contadorevento.toString(), "0", 15, false));
			envEvento.setVersao(versao);
			
			Listaevento listaevento = new Listaevento();
			envEvento.setListaevento(listaevento);
			
			List<Evento> listEvento = new ArrayList<Evento>();
			
			Evento evento = new Evento();
			listEvento.add(evento);
			
			listaevento.setListaevento(listEvento);
			
			evento.setVersao(versao);
			
			InfEvento infEvento = new InfEvento();
			evento.setInfEvento(infEvento);
			
			infEvento.setId(id);
			infEvento.setcOrgao(cOrgao);
			infEvento.setTpAmb(ambienteProducao ? 1 : 2);
			
			if (Boolean.TRUE.equals(empresa.getPropriedadeRural())) {
				infEvento.setcPF(empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa());
			} else if (empresa.getCnpj() != null && org.apache.commons.lang.StringUtils.isNotEmpty(empresa.getCnpj().getValue())) {
				infEvento.setcNPJ(empresa.getCnpj().getValue());
			} else if (empresa.getCpf() != null && org.apache.commons.lang.StringUtils.isNotEmpty(empresa.getCpf().getValue())) {
				infEvento.setcPF(empresa.getCpf().getValue());
			}
			
			infEvento.setChNFe(chaveAcesso);
			infEvento.setDhEvento(new Date(timeAtual));
			infEvento.setTpEvento(tpEvento);
			infEvento.setnSeqEvento(nSeqEvento);
			infEvento.setVerEvento(versao);
			
			DetEvento detEvento = new DetEvento(false, true, false);
			infEvento.setDetEvento(detEvento);
			
			String justificativa = bean.getMotivo();
			justificativa = justificativa.replaceAll("\\r?\\n", " ");
			justificativa = addScape(justificativa);
			justificativa = justificativa.trim();
			
			detEvento.setVersao(versao);
			detEvento.setDescEvento("Cancelamento");
			detEvento.setnProt(arquivonfnota.getProtocolonfe().trim());
			detEvento.setxJust(justificativa);
			
			String string = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + new String(Util.strings.tiraAcento(envEvento.toString()).getBytes(), "UTF-8");
			Arquivo arquivocancelamento = new Arquivo(string.getBytes(), "nfcancelamento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			arquivonfnota.setArquivoxmlcancelamento(arquivocancelamento);
			arquivonfnotaService.updateCancelamentoPorEvento(arquivonfnota, true);
			
			Contigencianfe contigencianfe = contigencianfeService.getContigenciaAtual(configuracaonfe.getUf());
			if(contigencianfe != null && configuracaonfe.getTipoconfiguracaonfe() != null){
				arquivonfnota.setTipocontigenciacancelamento(contigencianfe.getTipocontigencia());
			} else {
				arquivonfnota.setTipocontigenciacancelamento(null);
			}
			
			arquivoDAO.saveFile(arquivonfnota, "arquivoxmlcancelamento");
			arquivoService.saveOrUpdate(arquivocancelamento);
			arquivonfnotaService.updateArquivoxmlcancelamento(arquivonfnota);
			arquivonfnotaService.updateTipocontigenciacancelamento(arquivonfnota);
			
			nf.setNotaStatus(NotaStatus.NFE_CANCELANDO);
			notaService.alterarStatusAcao(nf, bean.getMotivo(), bean.getMotivoCancelamentoFaturamento());
			
			notaService.updateCancelamentoVenda(nf, bean.getCancelamentoVenda() != null && bean.getCancelamentoVenda());
			notafiscalprodutoService.atualizaExpedicao(null,null,nf,NotaStatus.NFE_CANCELANDO, false);
			request.addMessage("Cancelamento criado com sucesso.");
			
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			request.addError("Erro na cria��o do arquivo de cancelamento de NF-e.");
		}
		
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView selecionarConfiguracao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Boolean isNfe = Boolean.parseBoolean(request.getParameter("nfe"));
		
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.EMITIDA)){
			request.addError("A(s) nota(s) fiscal(is) tem que estar na situa��o 'EMITIDA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		String msgErro = notafiscalprodutoService.validaDivergenciaValoresParcela(request);
		if(org.apache.commons.lang.StringUtils.isNotEmpty(msgErro)){
			request.addError(msgErro);
			SinedUtil.fechaPopUp(request);
			return null;			
		}
		
		try {
			Empresa empresa = empresaService.getEmpresaNFeNotas(whereIn);

			boolean havePadrao;
			
			if (isNfe) {
				havePadrao = configuracaonfeService.havePadrao(null, Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, empresa);
			} else {
				havePadrao = configuracaonfeService.havePadrao(null, Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR, empresa);
			}
			
			SelecionarConfiguracaoBean bean = new SelecionarConfiguracaoBean();
			bean.setWhereIn(whereIn);
			bean.setEmpresa(empresa);
			bean.setNfe(isNfe);
			
			if(!havePadrao){
				List<Configuracaonfe> listaConfiguracao = null;
				
				if (isNfe) {
					listaConfiguracao = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, empresa);
				} else {
					listaConfiguracao = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR, empresa);
				}
				
				request.setAttribute("listaConfiguracao", listaConfiguracao);
				return new ModelAndView("direct:/process/popup/selecionarConfiguracaoEstado", "bean", bean);
			}
			
			Configuracaonfe configuracaonfePadrao = configuracaonfeService.getConfiguracaoPadrao(isNfe ? Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO : Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR, empresa);
			bean.setConfiguracaonfe(configuracaonfePadrao);
			return continueOnAction("criarNfe", bean);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	public void criarNfe(WebRequestContext request, SelecionarConfiguracaoBean bean){
		try{
			Empresa empresa = bean.getEmpresa();
			Configuracaonfe configuracaonfe = configuracaonfeService.loadForEntrada(bean.getConfiguracaonfe());
			String whereIn = bean.getWhereIn();
			Boolean isNfe = (bean.getNfe() == null ? Boolean.FALSE : bean.getNfe());
			
			Contigencianfe contigencianfe = contigencianfeService.getContigenciaAtual(configuracaonfe.getUf());
			
			Map<String, String> mapArquivos = new HashMap<String, String>();
			Map<String, List<Notafiscalproduto>> mapListaNotas = new HashMap<String, List<Notafiscalproduto>>();
			List<String> listaWhereInNotas = new ArrayList<String>();
			String[] ids = whereIn.split(",");
			
			List<Notafiscalproduto> listaNotasSemSeries = notafiscalprodutoService.findNotasSemSerie(whereIn);
			for(Notafiscalproduto nota: listaNotasSemSeries){
				if (BooleanUtils.isTrue(isNfe)) {
					if(nota.getEmpresa().getSerienfe() != null){
						notaService.updateSerieNfe(nota.getCdNota(), nota.getEmpresa().getSerienfe());
					}else{
						throw new SinedException("A empresa "+nota.getEmpresa().getNome()+" n�o possui o campo 'S�rie NF-e' preenchido.");
					}
				} else {
					if (nota.getEmpresa().getSerieNfce() != null) {
						notaService.updateSerieNfce(nota.getCdNota(), nota.getEmpresa().getSerieNfce());
					} else {
						throw new SinedException("A empresa " + nota.getEmpresa().getNome() + " n�o possui o campo 'S�rie NFC-e' preenchido.");
					}
				}
				
				if(configuracaonfe != null && configuracaonfe.getAtualizacaodataentradasaida() != null && configuracaonfe.getAtualizacaodataentradasaida() &&
						configuracaonfe.getTipoconfiguracaonfe() != null && Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR.equals(configuracaonfe.getTipoconfiguracaonfe())){
					notafiscalprodutoService.updateHoraEmissao(nota, new Hora(new Date(System.currentTimeMillis()).getTime()));
					notaService.atualizaDataEmissao(nota, new Date(System.currentTimeMillis()));
				}
			}
			
			if(configuracaonfe != null && 
					configuracaonfe.getAtualizacaodataentradasaida() != null && 
					configuracaonfe.getAtualizacaodataentradasaida()){
				notafiscalprodutoService.updateDataentradasaidaDataatual(whereIn);
			}
			
			int nota_por_lote = 5;
			try{
				nota_por_lote = Integer.parseInt(parametrogeralService.getValorPorNome(Parametrogeral.NUMERO_NOTAS_LOTE_NFE));
			} catch (Exception e) {}
			
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < ids.length; i++) {
				if(i != 0 && i % nota_por_lote == 0){
					listaWhereInNotas.add(sb.toString());
					sb = new StringBuilder();
				}
				if(sb.length() > 0) sb.append(",");
				sb.append(ids[i]);
			}
			listaWhereInNotas.add(sb.toString());
			
			for (String in : listaWhereInNotas) {
				
				String[] ids_in = in.split(",");
				List<Notafiscalproduto> listaNota = new ArrayList<Notafiscalproduto>();
				for (int i = 0; i < ids_in.length; i++) {
					listaNota.add(new Notafiscalproduto(Integer.parseInt(ids_in[i])));
				}
				
				EnviNFe enviNFe = this.createXmlEnvio(listaNota, request, configuracaonfe, contigencianfe, isNfe);
				
				String string;
				String xml = new String(Util.strings.tiraAcento(enviNFe.toString()).getBytes(), "UTF-8");
				
//				COMENTADO POIS DEMORA MUITA A VALIDA��O DO SHEMA
//					
//				SAXBuilder builder = new SAXBuilder("org.apache.xerces.parsers.SAXParser", true);
//				builder.setFeature("http://apache.org/xml/features/validation/schema", true);
//				builder.setProperty("http://apache.org/xml/properties/schema/external-schemaLocation", "http://www.portalfiscal.inf.br/nfe " + SinedUtil.getUrlWithContext() + "/schemas/enviNFe_v2.00.xsd");
//				builder.build(new StringReader(xml));
				
				string = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + xml;
				
				mapArquivos.put(in, string);
				mapListaNotas.put(in, listaNota);
			}
			
			Arquivonf arquivonf;
			Arquivonfnota arquivonfnota;
			Arquivo arquivo;
			Set<Entry<String, String>> entrySet = mapArquivos.entrySet();
			for (Entry<String, String> entry : entrySet) {
				String mimeType = "text/xml";
				String extensaoArquivo = ".xml";
				
				arquivo = new Arquivo(entry.getValue().getBytes(), (BooleanUtils.isTrue(isNfe) ? "nf_" : "nfc_") + SinedUtil.datePatternForReport() + extensaoArquivo, mimeType);
				
				arquivonf = new Arquivonf();
				arquivonf.setArquivoxml(arquivo);
				arquivonf.setConfiguracaonfe(configuracaonfe);
				arquivonf.setEmpresa(empresa);
				arquivonf.setArquivonfsituacao(Arquivonfsituacao.GERADO);
				
				if(contigencianfe != null){
					arquivonf.setTipocontigencia(contigencianfe.getTipocontigencia());
					arquivonf.setDtentradacontigencia(contigencianfe.getDtentrada());
					arquivonf.setJustificativacontigencia(contigencianfe.getJustificativa());
				}
				
				arquivoDAO.saveFile(arquivonf, "arquivoxml");
				arquivoService.saveOrUpdate(arquivo);
				arquivonfService.saveOrUpdate(arquivonf);
				
				String in = entry.getKey();
				List<Notafiscalproduto> listaNotas = mapListaNotas.get(in);
				for (Notafiscalproduto nota : listaNotas) {
					arquivonfnota = new Arquivonfnota();
					arquivonfnota.setArquivonf(arquivonf);
					arquivonfnota.setNota(nota);
					arquivonfnota.setChaveacesso(nota.getChaveAcesso());
					
					boolean ambienteProducao = !configuracaonfe.getPrefixowebservice().name().endsWith("_HOM");
					Integer tpEmis = 1; // EMISS�O NORMAL
					
					if(contigencianfe != null && contigencianfe.getTipocontigencia() != null){
						if(contigencianfe.getTipocontigencia().equals(Tipocontigencia.SVAN)){
							tpEmis = 6; // EMISS�O DO AMBIENTE NACIONAL
						} else if(contigencianfe.getTipocontigencia().equals(Tipocontigencia.SVRS)){
							tpEmis = 7; // EMISS�O DO AMBIENTE DO RIO GRANDE DO SUL
						}
					}
					
					if (BooleanUtils.isFalse(isNfe) && (tpEmis != 6 && tpEmis != 7)) {
						String urlQRCode = NFCeUtil.getUrlQRCode(configuracaonfe.getUf().getSigla(), ambienteProducao);
						String idToken = configuracaonfe.getIdToken();
						String csc = configuracaonfe.getCsc();
						
						if (org.apache.commons.lang.StringUtils.isNotEmpty(urlQRCode) && 
								org.apache.commons.lang.StringUtils.isNotEmpty(idToken) &&
								org.apache.commons.lang.StringUtils.isNotEmpty(csc)) {
							arquivonfnota.setQrCode(NFCeUtil.getCodeQRCode(nota.getChaveAcesso(), ambienteProducao ? "1" : "2", idToken, csc, urlQRCode));
						}
					}
					
					String observacao = "Arquivo de " + (BooleanUtils.isTrue(isNfe) ? "NF-e" : "NFC-e") + " criado. <a href=\"javascript:visualizarArquivonf(" + arquivonf.getCdarquivonf() + ")\">" + arquivonf.getCdarquivonf() + "</a>";
					if(contigencianfe != null){
						observacao += " (Criado para o ambiente de contig�ncia " + contigencianfe.getTipocontigencia().name() + ")";
					}
					
					nota.setNotaStatus(NotaStatus.NFE_SOLICITADA);
					notaService.alterarStatusAcao(nota, observacao, null);
					arquivonfnotaService.saveOrUpdate(arquivonfnota);
				}
				
				request.addMessage("Arquivo de nota fiscal eletr�nica criado com sucesso.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * Ajax para verificar se tem algum DIFAL a ser calculado.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 31/05/2016
	 */
	public ModelAndView ajaxVerificarDIFAL(WebRequestContext request){
		boolean haveDIFAL = false;
		
		boolean calcularDIFAL = parametrogeralService.getBoolean(Parametrogeral.CALCULAR_DIFAL_NFE);
		if(calcularDIFAL){
			try{
				String whereIn = SinedUtil.getItensSelecionados(request);
				if(whereIn == null || whereIn.trim().equals("")){
					throw new SinedException("Nenhum item selecionado.");
				}
				
				// COLOCADO PARA FAZER A VALIDA��O DE EMPRESAS DIFERENTES
				empresaService.getEmpresaNFeNotas(whereIn);
				
				List<Notafiscalproduto> listaNota = notafiscalprodutoService.findForDIFALVerificacao(whereIn);
				for (Notafiscalproduto notafiscalproduto : listaNota) {
					haveDIFAL = notafiscalprodutoService.verificaDIFAL(notafiscalproduto);
					if(haveDIFAL) break;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return new JsonModelAndView().addObject("haveDIFAL", haveDIFAL);
	}
	
	/**
	 * Abre pop-up para confer�ncia do DIFAL
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/06/2016
	 */
	public ModelAndView conferenciaDIFAL(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.trim().equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Notafiscalproduto> listaNota = notafiscalprodutoService.findForDIFALConferencia(whereIn);
		for (Notafiscalproduto notafiscalproduto : listaNota) {
			boolean haveDIFAL = notafiscalprodutoService.verificaDIFAL(notafiscalproduto);
			notafiscalproduto.setHaveDIFAL(haveDIFAL);
			
			Money valortotalfcpdestinatario = new Money();
			Money valortotalicmsdestinatario = new Money();
			Money valortotalicmsremetente = new Money();
			
			if(haveDIFAL){
				List<Notafiscalprodutoitem> listaItens = notafiscalproduto.getListaItens();
				if(listaItens != null){
					for (Notafiscalprodutoitem notafiscalprodutoitem : listaItens) {
						notafiscalprodutoitem.setEmpresa(notafiscalproduto.getEmpresa());
						notafiscalprodutoitem.setCliente(notafiscalproduto.getCliente());
						notafiscalprodutoitem.setEnderecoCliente(notafiscalproduto.getEnderecoCliente());
						notafiscalprodutoitem.setDtemissao(notafiscalproduto.getDtEmissao());
						notafiscalprodutoitem.setNaturezaoperacao(notafiscalproduto.getNaturezaoperacao());
						notafiscalprodutoitem.setOperacaonfe(notafiscalproduto.getOperacaonfe());
						notafiscalprodutoitem.setModeloDocumentoFiscalEnum(notafiscalproduto.getModeloDocumentoFiscalEnum());
						notafiscalprodutoitem.setLocaldestinonfe(notafiscalproduto.getLocaldestinonfe());
						
						notafiscalprodutoService.calculaImpostoDIFAL(notafiscalprodutoitem);
						valortotalfcpdestinatario = valortotalfcpdestinatario.add(notafiscalprodutoitem.getValorfcpdestinatario());
						valortotalicmsdestinatario = valortotalicmsdestinatario.add(notafiscalprodutoitem.getValoricmsdestinatario());
						valortotalicmsremetente = valortotalicmsremetente.add(notafiscalprodutoitem.getValoricmsremetente());
					}
				}
			}
			
			notafiscalproduto.setValorfcpdestinatario(valortotalfcpdestinatario);
			notafiscalproduto.setValoricmsdestinatario(valortotalicmsdestinatario);
			notafiscalproduto.setValoricmsremetente(valortotalicmsremetente);
		}
		
		ConferenciaDIFALBean bean = new ConferenciaDIFALBean();
		bean.setLista(listaNota);
		bean.setWhereIn(whereIn);
		
		
		Empresa empresa = empresaService.getEmpresaNFeNotas(whereIn);
		boolean havePadrao = configuracaonfeService.havePadrao(null, Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, empresa);
		request.setAttribute("havePadrao", havePadrao);
		
		if(!havePadrao){
			List<Configuracaonfe> listaConfiguracao = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, empresa);
			request.setAttribute("listaConfiguracao", listaConfiguracao);
		} else {
			Configuracaonfe configuracaonfePadrao = configuracaonfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, empresa);
			bean.setConfiguracaonfe(configuracaonfePadrao);
		}
		
		return new ModelAndView("process/conferenciaDIFAL", "bean", bean);
	}
	
	public ModelAndView saveConferenciaDIFAL(WebRequestContext request, ConferenciaDIFALBean bean){
		String whereIn = bean.getWhereIn();
		Boolean isNfe = Boolean.parseBoolean(request.getParameter("nfe"));
		
		Empresa empresa = empresaService.getEmpresaNFeNotas(whereIn);
		Configuracaonfe configuracaonfe = bean.getConfiguracaonfe();

		List<Notafiscalproduto> lista = bean.getLista();
		for (Notafiscalproduto notafiscalproduto : lista) {
			Boolean haveDIFAL = notafiscalproduto.getHaveDIFAL() != null && notafiscalproduto.getHaveDIFAL();
			if(haveDIFAL){
				List<Notafiscalprodutoitem> listaItens = notafiscalproduto.getListaItens();
				for (Notafiscalprodutoitem notafiscalprodutoitem : listaItens) {
					notafiscalprodutoitemService.updateICMSInterestadual(notafiscalprodutoitem);
				}
				
				notafiscalprodutoService.updateICMSInterestadual(notafiscalproduto);
				
                StringBuilder info = new StringBuilder();

                Double difalUfDestino = 0d;
                Double fcpUfDestino = 0d;
                Double difalUfOrigem = 0d;

                if (SinedUtil.isListNotEmpty(notafiscalproduto.getListaItens())) {
                    for (Notafiscalprodutoitem item : notafiscalproduto.getListaItens()) {
                        if (item.getValoricmsdestinatario() != null) {
                            difalUfDestino += item.getValoricmsdestinatario().getValue().doubleValue();
                        }

                        if (item.getValorfcpdestinatario() != null) {
                            fcpUfDestino += item.getValorfcpdestinatario().getValue().doubleValue();
                        }

                        if (item.getValoricmsremetente() != null) {
                            difalUfOrigem += item.getValoricmsremetente().getValue().doubleValue();
                        }
                    }
                }

                if (difalUfDestino > 0 || fcpUfDestino > 0 || fcpUfDestino > 0) {
                    String infoDifal = 
                            "Valores totais do ICMS Interestadual: DIFAL da UF destino R$ " + new Money(difalUfDestino) +
                            " + FCP R$ " + new Money(fcpUfDestino) +
                            "; DIFAL da UF Origem R$ " + new Money(difalUfOrigem) + ".";
                    info.append(infoDifal);
                }

                if (!info.toString().equals("")) {
                    Notafiscalproduto nfp = notafiscalprodutoService.carregaInfoAdicionalContrib(notafiscalproduto);

                    if(nfp != null){
                    	if(nfp.getInfoadicionalcontrib() == null){
                    		nfp.setInfoadicionalcontrib("");
                    	}
	                    if (nfp.getInfoadicionalcontrib().length() == 0) {
	                        nfp.setInfoadicionalcontrib(nfp.getInfoadicionalcontrib() + info);
	                    } else if (!br.com.linkcom.utils.StringUtils.retiraQuebras(nfp.getInfoadicionalcontrib()).contains("Valores totais do ICMS Interestadual: DIFAL da UF destino")) {
	                        nfp.setInfoadicionalcontrib(nfp.getInfoadicionalcontrib() + ". " + info);
	                    }
	
	                    try {
	                    	notafiscalprodutoService.updateInfoAdicionalContrib(nfp);
						} catch (Exception e) {
							e.printStackTrace();
						}
                    }
                }
			}
		}		
		
		SelecionarConfiguracaoBean beanSelecionar = new SelecionarConfiguracaoBean();
		beanSelecionar.setWhereIn(whereIn);
		beanSelecionar.setEmpresa(empresa);
		beanSelecionar.setConfiguracaonfe(configuracaonfe);
		beanSelecionar.setNfe(isNfe);
		continueOnAction("criarNfe", beanSelecionar);
		
		return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
	}
	
	public ModelAndView selecionarConfiguracaoMdfe(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.EMITIDO)){
			request.addError("A MDFe tem que estar na situa��o 'EMITIDA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		try {
			Empresa empresa = empresaService.getEmpresaMDFeNotas(whereIn);

			boolean havePadrao = configuracaonfeService.havePadrao(null, Tipoconfiguracaonfe.MDFE, empresa);
			
			SelecionarConfiguracaoBean bean = new SelecionarConfiguracaoBean();
			bean.setWhereIn(whereIn);
			bean.setEmpresa(empresa);
			
			if(!havePadrao){
				List<Configuracaonfe> listaConfiguracao = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.MDFE, empresa);
				request.setAttribute("listaConfiguracao", listaConfiguracao);
				
				return new ModelAndView("direct:/process/popup/selecionarConfiguracaoMdfe", "bean", bean);
			}
			
			Configuracaonfe configuracaonfePadrao = configuracaonfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.MDFE, empresa);
			bean.setConfiguracaonfe(configuracaonfePadrao);
			return continueOnAction("criarMdfe", bean);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	
	
	public void criarMdfe(WebRequestContext request, SelecionarConfiguracaoBean bean){
		try{
			Empresa empresa = bean.getEmpresa();
			Configuracaonfe configuracaonfe = configuracaonfeService.loadForEntrada(bean.getConfiguracaonfe());
			String whereIn = bean.getWhereIn();
			
			String mimeType = "text/xml";
			String extensaoArquivo = ".xml";
			
			ArquivoMdfe arquivoMdfe;
			Arquivo arquivo;
			
			for (String in : whereIn.split(",")) {
				Mdfe mdfe = mdfeService.loadForEntrada(new Mdfe(Integer.parseInt(in)));
				mdfeService.carregarListas(mdfe);
				
				EnviMdfe enviMdfe = arquivoMdfeService.createXmlMdfeEnvio(mdfe, configuracaonfe);
				
				String string;
				String xml = new String(Util.strings.tiraAcento(enviMdfe.toString()).getBytes(), "UTF-8");
				
				string = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + xml;
				
				arquivo = new Arquivo(string.getBytes(), "nf_" + SinedUtil.datePatternForReport() + extensaoArquivo, mimeType);
				
				arquivoMdfe = new ArquivoMdfe();
				arquivoMdfe.setArquivoxml(arquivo);
				arquivoMdfe.setConfiguracaonfe(configuracaonfe);
				arquivoMdfe.setEmpresa(empresa);
				arquivoMdfe.setArquivoMdfeSituacao(ArquivoMdfeSituacao.GERADO);
				arquivoMdfe.setMdfe(mdfe);
				arquivoMdfe.setChaveacesso(mdfe.getChaveAcesso());
				
				arquivoDAO.saveFile(arquivoMdfe, "arquivoxml");
				arquivoService.saveOrUpdate(arquivo);
				arquivoMdfeService.saveOrUpdate(arquivoMdfe);
					
				String observacao = "Arquivo de MDF-e criado. <a href=\"javascript:visualizarArquivoMdfe(" + arquivoMdfe.getCdArquivoMdfe() + ")\">" + arquivoMdfe.getCdArquivoMdfe() + "</a>";
				mdfe.setMdfeSituacao(MdfeSituacao.SOLICITADO);
				mdfeService.alterarStatusAcao(mdfe, observacao);
				
				if (enviMdfe != null && enviMdfe.getMdfe() != null && enviMdfe.getMdfe().getInfMdfeSupl() != null && enviMdfe.getMdfe().getInfMdfeSupl().getQrCodMdfe() != null) {
					mdfe.setQrCode(enviMdfe.getMdfe().getInfMdfeSupl().getQrCodMdfe());
					
					mdfeService.alterarQrCode(mdfe);
				}
			}
			request.addMessage("Arquivo de MDF-e criado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		
		SinedUtil.fechaPopUp(request);
	}

	public ModelAndView processaEncerramentoMdfe(WebRequestContext request, EventoBean eventoBean){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if (eventoBean.getExterior() != null && !eventoBean.getExterior()) {
			if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.AUTORIZADO)){
				request.addError("A MDFe tem que estar na situa��o 'MDFe Autorizado'.");
				SinedUtil.fechaPopUp(request);
				return null; 
			}
			
			if(eventoBean.getUf() == null || eventoBean.getMunicipio() == null){
				request.addError("� preciso informar UF e Munic�pio de encerramento.");
				SinedUtil.fechaPopUp(request);
				return null; 
			}
		}
		
		try {
			Mdfe mdfe = new Mdfe(Integer.parseInt(whereIn));
			ArquivoMdfe arquivoMdfe = arquivoMdfeService.loadForEncerramento(mdfe);
			
			if(arquivoMdfe == null){
				request.addError("Nenhum arquivo encontrado para encerramento da MDF-e.");
			}else {
			
				Configuracaonfe configuracaonfe = arquivoMdfe.getConfiguracaonfe();
				configuracaonfe = configuracaonfeService.loadForEntrada(configuracaonfe); 
				
				Empresa empresa = empresaService.carregaEmpresa(configuracaonfe.getEmpresa());
				
				Integer contadorevento = configuracaonfe.getContadorloteevento();
				if(contadorevento == null) contadorevento = 0;
				contadorevento++;
				configuracaonfeService.updateContadorloteevento(configuracaonfe, contadorevento);
				
				String chaveAcesso = arquivoMdfe.getChaveacesso();
				String tpEvento = "110112";
				Integer nSeqEvento = 1;
				
				String id = "ID" + tpEvento + chaveAcesso + br.com.linkcom.neo.util.StringUtils.stringCheia(nSeqEvento.toString(), "0", 2, false);
				
				eventoBean.setEmpresa(empresa);
				eventoBean.setArquivoMdfe(arquivoMdfe);
				eventoBean.setConfiguracaonfe(configuracaonfe);
				eventoBean.setMdfe(mdfe);
				
				if (eventoBean.getExterior() != null && !eventoBean.getExterior()) {
					eventoBean.setUf(ufService.load(eventoBean.getUf(), "uf.cduf, uf.cdibge"));
					eventoBean.setMunicipio(municipioService.load(eventoBean.getMunicipio(), "municipio.cdmunicipio, municipio.cdibge"));
				} else {
					eventoBean.setUf(Municipio.EXTERIOR_MDFE.getUf());
					eventoBean.setMunicipio(Municipio.EXTERIOR_MDFE);
				}
				
				EventoMdfe procEventoMdfe = arquivoMdfeService.createBeanEventoMdfe(eventoBean, contadorevento, id, configuracaonfe, false, true, false, null, tpEvento);
				
				String string = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + new String(Util.strings.tiraAcento(procEventoMdfe.toString()).getBytes(), "UTF-8");
				Arquivo arquivoxmlencerramento = new Arquivo(string.getBytes(), "nfencerramento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				arquivoMdfe.setArquivoxmlencerramento(arquivoxmlencerramento);
				arquivoMdfeService.updateEncerramentoPorEvento(arquivoMdfe, true);
				
				arquivoDAO.saveFile(arquivoMdfe, "arquivoxmlencerramento");
				arquivoService.saveOrUpdate(arquivoxmlencerramento);
				arquivoMdfeService.updateArquivoxmlEncerrando(arquivoMdfe);
				
				mdfeService.atualizaMdfeSituacao(mdfe, MdfeSituacao.EM_ENCERRAMENTO);
			
				Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
				MdfeHistorico mdfeHistorico = new MdfeHistorico();
				
				mdfeHistorico.setMdfe(mdfe);
				mdfeHistorico.setMdfeSituacao(MdfeSituacao.EM_ENCERRAMENTO);
				mdfeHistorico.setCdusuarioaltera(usuarioLogado != null ? usuarioLogado.getCdpessoa() : null);
				mdfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				mdfeHistorico.setObservacao("Encerramento solicitado.");
				
				mdfeHistoricoService.saveOrUpdateNoUseTransaction(mdfeHistorico);
				
				request.addMessage("Encerramento solicitado.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			request.addError("Erro na cria��o do arquivo de encerramento da MDF-e.");
		}
		
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView processaCancelamentoMdfe(WebRequestContext request, EventoBean eventoBean) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.AUTORIZADO)){
			request.addError("A MDFe tem que estar na situa��o 'MDFe Autorizado'.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		if(eventoBean.getJustificativa() == null) {
			request.addError("� preciso informar a justificativa de cancelamento.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		try {
			Mdfe mdfe = new Mdfe(Integer.parseInt(whereIn));
			ArquivoMdfe arquivoMdfe = arquivoMdfeService.loadForEncerramento(mdfe);
			
			if(arquivoMdfe == null){
				request.addError("Nenhum arquivo encontrado para cancelamento da MDF-e.");
			}else {
			
				Configuracaonfe configuracaonfe = arquivoMdfe.getConfiguracaonfe();
				configuracaonfe = configuracaonfeService.loadForEntrada(configuracaonfe); 
				
				Empresa empresa = empresaService.carregaEmpresa(configuracaonfe.getEmpresa());
				
				Integer contadorevento = configuracaonfe.getContadorloteevento();
				if(contadorevento == null) contadorevento = 0;
				contadorevento++;
				configuracaonfeService.updateContadorloteevento(configuracaonfe, contadorevento);
				
				String chaveAcesso = arquivoMdfe.getChaveacesso();
				String tpEvento = "110111";
				Integer nSeqEvento = 1;
				
				String id = "ID" + tpEvento + chaveAcesso + br.com.linkcom.neo.util.StringUtils.stringCheia(nSeqEvento.toString(), "0", 2, false);
				
				eventoBean.setEmpresa(empresa);
				eventoBean.setArquivoMdfe(arquivoMdfe);
				eventoBean.setConfiguracaonfe(configuracaonfe);
				eventoBean.setMdfe(mdfe);
				
				EventoMdfe procEventoMdfe = arquivoMdfeService.createBeanEventoMdfe(eventoBean, contadorevento, id, configuracaonfe, true, false, false, eventoBean.getJustificativa(), tpEvento);
				
				String string = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + new String(Util.strings.tiraAcento(procEventoMdfe.toString()).getBytes(), "UTF-8");
				Arquivo arquivoXmlCancelamento = new Arquivo(string.getBytes(), "nfcancelamento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				arquivoMdfe.setArquivoXmlCancelamento(arquivoXmlCancelamento);
				arquivoMdfeService.updateCancelamentoPorEvento(arquivoMdfe, true);
				
				arquivoDAO.saveFile(arquivoMdfe, "arquivoXmlCancelamento");
				arquivoService.saveOrUpdate(arquivoXmlCancelamento);
				arquivoMdfeService.updateArquivoXmlCancelando(arquivoMdfe);
				
				mdfeService.atualizaMdfeSituacao(mdfe, MdfeSituacao.EM_CANCELAMENTO);
			
				Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
				MdfeHistorico mdfeHistorico = new MdfeHistorico();
				
				mdfeHistorico.setMdfe(mdfe);
				mdfeHistorico.setMdfeSituacao(MdfeSituacao.EM_CANCELAMENTO);
				mdfeHistorico.setCdusuarioaltera(usuarioLogado != null ? usuarioLogado.getCdpessoa() : null);
				mdfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				mdfeHistorico.setObservacao("Cancelamento solicitado.");
				
				mdfeHistoricoService.saveOrUpdateNoUseTransaction(mdfeHistorico);
				
				request.addMessage("Cancelamento solicitado.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			request.addError("Erro na cria��o do arquivo de cancelamento da MDF-e.");
		}
		
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView processaInclusaoCondutorMdfe(WebRequestContext request, EventoBean eventoBean) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.AUTORIZADO)){
			request.addError("A MDFe tem que estar na situa��o 'MDFe Autorizado'.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		if(eventoBean.getNome() == null || eventoBean.getCpf() == null) {
			request.addError("� preciso informar Nome e CPF da inclus�o do condutor.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		try {
			Mdfe mdfe = new Mdfe(Integer.parseInt(whereIn));
			ArquivoMdfe arquivoMdfe = arquivoMdfeService.loadForEncerramento(mdfe);
			
			if (arquivoMdfe == null) {
				request.addError("Nenhum arquivo encontrado para inclus�o do condutor da MDF-e.");
			} else {
				Configuracaonfe configuracaonfe = arquivoMdfe.getConfiguracaonfe();
				configuracaonfe = configuracaonfeService.loadForEntrada(configuracaonfe); 
				
				Empresa empresa = empresaService.carregaEmpresa(configuracaonfe.getEmpresa());
				
				Integer contadorevento = configuracaonfe.getContadorloteevento();
				if(contadorevento == null) contadorevento = 0;
				contadorevento++;
				configuracaonfeService.updateContadorloteevento(configuracaonfe, contadorevento);
				
				String chaveAcesso = arquivoMdfe.getChaveacesso();
				String tpEvento = "110114";
				Integer nSeqEvento = 1;
				
				String id = "ID" + tpEvento + chaveAcesso + br.com.linkcom.neo.util.StringUtils.stringCheia(nSeqEvento.toString(), "0", 2, false);
				
				eventoBean.setEmpresa(empresa);
				eventoBean.setArquivoMdfe(arquivoMdfe);
				eventoBean.setConfiguracaonfe(configuracaonfe);
				eventoBean.setMdfe(mdfe);
				
				EventoMdfe procEventoMdfe = arquivoMdfeService.createBeanEventoMdfe(eventoBean, contadorevento, id, configuracaonfe, false, false, true, null, tpEvento);
				
				String string = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + new String(Util.strings.tiraAcento(procEventoMdfe.toString()).getBytes(), "UTF-8");
				Arquivo arquivoXmlInclusaoCondutor = new Arquivo(string.getBytes(), "mdfeinclusaocondutor_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				arquivoMdfe.setArquivoXmlInclusaoCondutor(arquivoXmlInclusaoCondutor);
				arquivoMdfeService.updateInclusaoCondutorPorEvento(arquivoMdfe, true);
				
				arquivoDAO.saveFile(arquivoMdfe, "arquivoXmlInclusaoCondutor");
				arquivoService.saveOrUpdate(arquivoXmlInclusaoCondutor);
				arquivoMdfeService.updateArquivoXmlInclusaoCondutor(arquivoMdfe);
				
				mdfeService.atualizaMdfeSituacao(mdfe, MdfeSituacao.EM_INCLUSAO_CONDUTOR);
			
				Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
				MdfeHistorico mdfeHistorico = new MdfeHistorico();
				
				mdfeHistorico.setMdfe(mdfe);
				mdfeHistorico.setMdfeSituacao(MdfeSituacao.EM_INCLUSAO_CONDUTOR);
				mdfeHistorico.setCdusuarioaltera(usuarioLogado != null ? usuarioLogado.getCdpessoa() : null);
				mdfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				mdfeHistorico.setObservacao("Inclus�o de Condutor solicitado.");
				
				mdfeHistoricoService.saveOrUpdateNoUseTransaction(mdfeHistorico);
				
				request.addMessage("Inclus�o de Condutor solicitado.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			request.addError("Erro na cria��o do arquivo de inclus�o de condutor da MDF-e.");
		}
		
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView selecionarEncerramentoMdfe(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String listagem = request.getParameter("listagem") != null ? request.getParameter("listagem") : "false";
		if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.AUTORIZADO)){
			request.addError("A MDFe tem que estar na situa��o 'MDFe Autorizado'.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		request.setAttribute("listagem", listagem);
		request.setAttribute("selectedItens", whereIn);
		return new ModelAndView("direct:/process/encerramentoMdfe", "bean", new EventoBean());
	}
	
	public ModelAndView selecionarCancelamentoMdfe(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String listagem = request.getParameter("listagem") != null ? request.getParameter("listagem") : "false";
		if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.AUTORIZADO)){
			request.addError("A MDFe tem que estar na situa��o 'MDFe Autorizado'.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		request.setAttribute("listagem", listagem);
		request.setAttribute("selectedItens", whereIn);
		return new ModelAndView("direct:/process/cancelamentoMdfe", "bean", new EventoBean());
	}
	
	public ModelAndView selecionarInclusaoCondutorMdfe(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String listagem = request.getParameter("listagem") != null ? request.getParameter("listagem") : "false";
		if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.AUTORIZADO)){
			request.addError("A MDFe tem que estar na situa��o 'MDFe Autorizado'.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		request.setAttribute("listagem", listagem);
		request.setAttribute("selectedItens", whereIn);
		return new ModelAndView("direct:/process/inclusaoCondutorMdfe", "bean", new EventoBean());
	}
}