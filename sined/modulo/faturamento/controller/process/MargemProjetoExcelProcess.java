package br.com.linkcom.sined.modulo.faturamento.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.VwrelatoriomargemprojetoService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.MargemProjetoFiltro;

@Controller(
		path="/faturamento/process/MargemProjeto",
		authorizationModule=ProcessAuthorizationModule.class
)
public class MargemProjetoExcelProcess extends ResourceSenderController<MargemProjetoFiltro>{

	private VwrelatoriomargemprojetoService vwrelatoriomargemprojetoService;
	
	public void setVwrelatoriomargemprojetoService(
			VwrelatoriomargemprojetoService vwrelatoriomargemprojetoService) {
		this.vwrelatoriomargemprojetoService = vwrelatoriomargemprojetoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	MargemProjetoFiltro filtro) throws Exception {
		return vwrelatoriomargemprojetoService.preparaArquivoMargemProjetoExcelFormatado(filtro);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, MargemProjetoFiltro filtro) throws Exception {
		return new ModelAndView("process/margemProjeto").addObject("filtro", filtro);
	}
	
}
