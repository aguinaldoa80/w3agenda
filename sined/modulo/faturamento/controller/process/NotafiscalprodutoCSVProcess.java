package br.com.linkcom.sined.modulo.faturamento.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotafiscalprodutoFiltro;

@Controller(
		path="/faturamento/process/NotafiscalprodutoCSV",
		authorizationModule=ProcessAuthorizationModule.class
)
public class NotafiscalprodutoCSVProcess extends ResourceSenderController<NotafiscalprodutoFiltro>{
	
	private NotafiscalprodutoService notafiscalprodutoService;
	
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	NotafiscalprodutoFiltro filtro) throws Exception {
		boolean isNfe = "true".equals(request.getParameter("isNfe"));
		if(isNfe){
			filtro.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		} else {
			filtro.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFCE);
		}
		return notafiscalprodutoService.geraArquivoCSVListagem(filtro, isNfe);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, NotafiscalprodutoFiltro filtro) throws Exception {
		boolean isNfe = "true".equals(request.getParameter("isNfe"));
		if(isNfe){
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto").addObject("filtro", filtro);
		} else {
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalconsumidor").addObject("filtro", filtro);
		}
	}
	
}
