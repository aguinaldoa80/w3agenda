package br.com.linkcom.sined.modulo.faturamento.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoFiltro;

@Controller(path="/faturamento/process/NotaFiscalServicoExcel",authorizationModule=ProcessAuthorizationModule.class)
public class NotaFiscalServicoExcelProcess extends ResourceSenderController<NotaFiscalServicoFiltro>{

	private NotaFiscalServicoService notaFiscalServicoService;
	
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	NotaFiscalServicoFiltro filtro) throws Exception {
		return notaFiscalServicoService.preparaArquivoAcompanhamentoTarefaExcel(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, NotaFiscalServicoFiltro filtro) throws Exception {
		return null;
	}
	
}