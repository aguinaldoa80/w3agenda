package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.util.List;

import org.jdom.JDOMException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.FeedApplicationImportacaoVendaFiltro;

@Controller(
		path="/faturamento/process/FeedApplicationImportacaoProcess",
		authorizationModule=ProcessAuthorizationModule.class
)
public class FeedApplicationImportacaoVendaProcess extends MultiActionController {
	
	private VendaService vendaService;
	private EmpresaService empresaService;
	
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}

	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, FeedApplicationImportacaoVendaFiltro filtro){
		request.setAttribute("listaEmpresa", empresaService.findAtivosByUsuario());
		return new ModelAndView("process/feedapplicationimportacaovenda", "filtro", filtro);
	}
	
	/**
	* M�todo que processa e cria as vendas de acordo com o arquivo xml
	*
	* @param request
	* @param filtro
	* @return
	* @throws Exception
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	@Action("processarXML")
	public ModelAndView processarArquivo(WebRequestContext request, FeedApplicationImportacaoVendaFiltro filtro) throws Exception {
		if(filtro.getArquivo() == null){
			request.addError("� preciso informar o arquivo xml.");
			return continueOnAction("carregar", filtro); 
		}
		
		if(filtro.getEmpresa() == null){
			request.addError("� preciso informar a empresa.");
			return continueOnAction("carregar", filtro); 
		}
		
		Empresa empresa = empresaService.loadForImportacaoFeedApplication(filtro.getEmpresa());
		if(empresa.getClientefeedapplication() == null || empresa.getMaterialfeedapplication() == null ||
				empresa.getPedidovendatipofeedapplication() == null || empresa.getPrazopagamentofeedapplication() == null ||
				empresa.getDocumentotipofeedapplication() == null){
			request.addError("� preciso informar todos os dados de importa��o antes de importar vendas.");
			return continueOnAction("carregar", filtro); 
		}
		
		try {
			List<Venda> listaVenda = vendaService.processarXMLFeedApplication(request, filtro.getArquivo(), empresa);
			request.setAttribute("listaVenda", listaVenda);
		} catch (JDOMException e) {
			e.printStackTrace();
			request.addError("O layout do arquivo n�o corresponde ao padr�o reconhecido pelo sistema. Favor verificar se o XML foi gerado corretamente.<br>" + e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao processar arquivo: " + e.getMessage());
		}
		
		return continueOnAction("carregar", filtro); 
	}
	
	/**
	* M�todo que abre uma popup para edi��o dos dados de importa��o de venda
	*
	* @param request
	* @param empresa
	* @return
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView configurarImportacao(WebRequestContext request, Empresa empresa){
		return new ModelAndView("direct:/process/popup/feedapplicationconfiguracaoimportacao", "bean", empresaService.loadForImportacaoFeedApplication(empresa));
	}
	
	/**
	* M�todo que salva os dados de importa��o de venda
	*
	* @param request
	* @param empresa
	* @return
	* @since 06/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView saveConfigurarImportacao(WebRequestContext request, Empresa empresa){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		String msg = "";
		try {
			empresaService.updateConfiguracaoImportacao(empresa);
		} catch (Exception e) {
			msg = "" + e.getMessage();
			
		}
		return jsonModelAndView.addObject("msg", msg);
	}
}
