package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.lang.reflect.Method;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.VwrelatoriooverheadService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.RelacaoOverheadFiltro;

@Controller(
		path="/faturamento/process/RelacaoOverhead",
		authorizationModule=ProcessAuthorizationModule.class
)
public class RelacaoOverheadExcelProcess extends ResourceSenderController<RelacaoOverheadFiltro>{

	private VwrelatoriooverheadService vwrelatoriooverheadService;
	
	public void setVwrelatoriooverheadService(
			VwrelatoriooverheadService vwrelatoriooverheadService) {
		this.vwrelatoriooverheadService = vwrelatoriooverheadService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	RelacaoOverheadFiltro filtro) throws Exception {
		return vwrelatoriooverheadService.preparaArquivoRelacaoOverheadExcelFormatado(filtro);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, RelacaoOverheadFiltro filtro) throws Exception {
		if(filtro.getEmpresa() == null && request.getSession().getAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase()) == null){
			request.getSession().setAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase(), Boolean.TRUE);
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null){
				Method methodSetEmpresa = Util.beans.getSetterMethod(filtro.getClass(), "empresa");
				if(methodSetEmpresa != null && methodSetEmpresa.getParameterTypes()[0].equals(Empresa.class)){
					methodSetEmpresa.invoke(filtro, empresa);
				}
			}
		}
		return new ModelAndView("process/relacaoOverhead").addObject("filtro", filtro);
	}
	
}
