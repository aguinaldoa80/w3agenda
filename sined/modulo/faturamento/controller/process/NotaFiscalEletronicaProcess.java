package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.lknfe.xml.nfse.abrasf.CancelarNfseEnvio;
import br.com.linkcom.lknfe.xml.nfse.assessorpublico.NfseCancelamento;
import br.com.linkcom.lknfe.xml.nfse.obaratec.CancelamentoNfseObaratec;
import br.com.linkcom.lknfe.xml.nfse.prefeituraserra.NfdCancelamento;
import br.com.linkcom.lknfe.xml.nfse.prefeiturasp.PedidoCancelamentoNFe;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEnvioNFCliente;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivonfService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioEmailResultadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.CancelamentoNfseBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.SelecionarConfiguracaoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/faturamento/process/NotaFiscalEletronica", authorizationModule=ProcessAuthorizationModule.class)
public class NotaFiscalEletronicaProcess extends MultiActionController{
	
	private NotaService notaService;
	private EmpresaService empresaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private EnderecoService enderecoService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private ArquivonfService arquivonfService;
	private ArquivonfnotaService arquivonfnotaService;
	private ConfiguracaonfeService configuracaonfeService;
	private NotaDocumentoService notaDocumentoService;
	private ParametrogeralService parametrogeralService;
	
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setArquivonfService(ArquivonfService arquivonfService) {this.arquivonfService = arquivonfService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	public ModelAndView enviarNfseCliente(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA)){
			request.addError("Para envio da NFS-e as notas tem que estar com a situa��o 'NFS-e EMITIDA' ou 'NFS-e LIQUIDADA'.");
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
		}
		
		EnvioEmailResultadoBean resultado = arquivonfService.enviarNFCliente(whereIn, TipoEnvioNFCliente.SERVICO_PDF); 
		
		if(resultado.getSucesso() > 0){
			request.addMessage(resultado.getSucesso() + " NFS-e(s) enviada(s) para o(s) cliente(s) com sucesso.");
		}
		
		if(resultado.getErro() > 0){
			request.addError(resultado.getErro() + " NFS-e(s) n�o foi(ram) enviada(s) para o(s) cliente(s).");
		}
		
		if(resultado.getListaErro() != null){
			for (String erro : resultado.getListaErro()) {
				request.addError(erro);
			}
		}
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
	}
	
	public ModelAndView cancelamentoNfse(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(notaService.haveNFWithStatus(whereIn, NotaStatus.CANCELADA, NotaStatus.NFE_CANCELANDO)){
			request.addError("N�o � permitido cancelar nota com a situa��o 'Cancelada' ou 'Cancelando NFS-e'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA)){
			request.addError("A(s) nota(s) fiscal(is) tem que estar na situa��o 'NFS-E EMITIDA' ou 'NFS-E LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		if(notaDocumentoService.validaCancelamentoNotaWebserviceFalse(request, whereIn)){
			SinedUtil.fechaPopUp(request);
			return null; 
		}
		
		CancelamentoNfseBean bean = new CancelamentoNfseBean();
		bean.setWhereIn(whereIn);
		
		return new ModelAndView("direct:/process/popup/cancelamentoNfse", "bean", bean);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView processaCancelamentoNfse(WebRequestContext request, CancelamentoNfseBean bean){
		if(notaService.haveNFDiferenteStatus(bean.getWhereIn(), NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA)){
			request.addError("A(s) nota(s) fiscal(is) tem que estar na situa��o 'NFS-E EMITIDA' ou 'NFS-E LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		try {
			List<NotaFiscalServico> listaNota = notaFiscalServicoService.findForCancelamentoNfse(bean.getWhereIn());
			List<Arquivonfnota> listaArquivonfnota;
			Arquivonfnota arquivonfnota;
			Empresa empresa;
			Configuracaonfe configuracaonfe;
			String string, xml;
			Arquivo arquivo;
			Prefixowebservice prefixowebservice;
			
			int sucesso = 0;
			for (NotaFiscalServico nota : listaNota) {
				listaArquivonfnota = nota.getListaArquivonfnota();
				if(listaArquivonfnota != null && listaArquivonfnota.size() == 1){
					arquivonfnota = listaArquivonfnota.get(0);
					empresa = nota.getEmpresa();
					configuracaonfe = arquivonfnota.getArquivonf().getConfiguracaonfe();
					
					if(empresa == null){
						request.addError("N�o foi encontrado o registro de Empresa para a nota " + nota.getNumero());
						continue;
					}
					
					empresa.setListaEndereco(SinedUtil.listToSet(enderecoService.carregarListaEndereco(empresa), br.com.linkcom.sined.geral.bean.Endereco.class));
					if(empresa.getEndereco() == null) {
						request.addError("Favor cadastrar o endere�o da empresa " + empresa.getRazaosocialOuNome());
						continue;
					}
					if(empresa.getEndereco().getMunicipio() == null) {
						request.addError("Favor cadastrar o municipio da empresa " + empresa.getRazaosocialOuNome());
						continue;
					}
					
					prefixowebservice = arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice();
					
					if(prefixowebservice.equals(Prefixowebservice.SIMOESFILHOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SIMOESFILHOISS_PROD)){
						arquivonfnota.setNota(nota);
						arquivonfService.cancelarArquivoEliss(arquivonfnota);
						sucesso++;
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.LIMEIRA_HOM) || prefixowebservice.equals(Prefixowebservice.LIMEIRA_PROD)){
						arquivonfnota.setNota(nota);
						arquivonfService.cancelarArquivoETransparencia(arquivonfnota);
						sucesso++;
						continue;
					} else if (prefixowebservice.equals(Prefixowebservice.ARANDU_HOM) ) { 
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Arandu n�o existe webservice dispon�vel para cancelamento no ambiente de homologa��o.");
						continue;
					} else if (prefixowebservice.equals(Prefixowebservice.ARANDU_PROD)) {
						arquivonfnota.setNota(nota);
						arquivonfService.cancelarNfseIssMap(arquivonfnota, empresa, bean);
						sucesso++;
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_PROD) || prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para o forneceodor GeisWeb n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.LAVRASISS_HOM) || prefixowebservice.equals(Prefixowebservice.LAVRASISS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Lavras n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.MARABA_HOM) || prefixowebservice.equals(Prefixowebservice.MARABA_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Maraba n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.SERRATALHADA_HOM) || prefixowebservice.equals(Prefixowebservice.SERRATALHADA_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Serra Talhada n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.SETELAGOAS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Sete Lagoas n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.SANTANADEPARNAIBA_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Santana de Parna�ba n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.SALVADORISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALVADORISS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Salvador n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.BRUMADINHOISS_PROD) || prefixowebservice.equals(Prefixowebservice.IGARAPEISS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Brumadinho ou Igarap� n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.PARACATUISS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Paracatu n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.CAMPINASISS_HOM) || prefixowebservice.equals(Prefixowebservice.CAMPINASISS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Campinas n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.SOROCABAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SOROCABAISS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Sorocaba n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.VINHEDOISS_PROD)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de Vinhedo n�o existe webservice dispon�vel para cancelamento.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.SPISS_HOM) || prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALTO_HOM)){
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": Para a prefeitura de S�o Paulo, Blumenau e Salto n�o existe webservice dispon�vel para cancelamento no ambiente de homologa��o.");
						continue;
					} else if(prefixowebservice.equals(Prefixowebservice.SERRAISS_HOM) || 
							prefixowebservice.equals(Prefixowebservice.SERRAISS_PROD) || 
							prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_HOM) || 
							prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.JANDIRA_HOM) || 
							prefixowebservice.equals(Prefixowebservice.JANDIRA_PROD)){
						NfdCancelamento nfdCancelamento = arquivonfService.createXmlCancelamentoNfseSmarpd(arquivonfnota.getNumeronfse(), arquivonfnota.getArquivonf().getConfiguracaonfe(), bean.getMotivo());
						xml = nfdCancelamento.toString();
					}else if(prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_PROD) || prefixowebservice.equals(Prefixowebservice.SPISS_PROD)){
						PedidoCancelamentoNFe pedidoCancelamentoNFe = arquivonfService.createXmlCancelamentoNfseSp(arquivonfnota.getNumeronfse(), empresa, prefixowebservice.getPrefixo());
						xml = pedidoCancelamentoNFe.toString();
					} else if(prefixowebservice.equals(Prefixowebservice.SALTO_PROD)){
						CancelamentoNfseObaratec pedidoCancelamentoNfeObaratec = arquivonfService.createXmlCancelamentoNfseObaratec(arquivonfnota.getNumeronfse(), empresa, arquivonfnota.getNota().getNumero(), bean.getMotivo());
						xml = pedidoCancelamentoNfeObaratec.toString();
					} else if (prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_PROD)) {
						NfseCancelamento nfseCancelamento = arquivonfService.createXmlCancelamentoNfseAssessorPublico(arquivonfnota, bean.getMotivo());
						xml = new String(Util.strings.tiraAcento(nfseCancelamento.toString()).getBytes(), "ISO-8859-1");
						xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>" + xml;
					} else if (prefixowebservice.equals(Prefixowebservice.MARABA_HOM) || prefixowebservice.equals(Prefixowebservice.MARABA_PROD)) {
						xml = arquivonfService.createXmlCancelamentoNfseGoverna(arquivonfnota, empresa, configuracaonfe);
					} else {
						// FEITO ISSO POIS NO WEBSERVICE DE RIBEIR�O PRETO TEM QUE USAR A VERS�O V2
						String sufixo_webservice = "";
						if(prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.BETIMISS_HOM) || prefixowebservice.equals(Prefixowebservice.BETIMISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_HOM) || prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.MINEIROSISS_HOM) || prefixowebservice.equals(Prefixowebservice.MINEIROSISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_HOM) || prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.REGISTROISS_HOM) || prefixowebservice.equals(Prefixowebservice.REGISTROISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.SANTAREMISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTAREMISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.FRANCAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FRANCAISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.DIADEMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.DIADEMAISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.FORTALEZA_HOM) || prefixowebservice.equals(Prefixowebservice.FORTALEZA_PROD) ||
								prefixowebservice.equals(Prefixowebservice.ITAUNAISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITAUNAISS_PROD) ||
								prefixowebservice.equals(Prefixowebservice.COTIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.COTIAISS_PROD)){
							sufixo_webservice = "_V2";
						}
						
						CancelarNfseEnvio cancelarNfseEnvio = arquivonfService.createXmlCancelamentoNfse(arquivonfnota.getNumeronfse(), arquivonfnota.getCodigoverificacao(), empresa, prefixowebservice, sufixo_webservice, arquivonfnota.getArquivonf().getConfiguracaonfe(), bean);
						xml = cancelarNfseEnvio.toString();
					}
					
					string = new String(Util.strings.tiraAcento(xml).getBytes(), "UTF-8");
					arquivo = new Arquivo(string.getBytes(), "nfcancelamento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
					
					arquivonfnota.setArquivoxmlcancelamento(arquivo);
					
					arquivoDAO.saveFile(arquivonfnota, "arquivoxmlcancelamento");
					arquivoService.saveOrUpdate(arquivo);
					arquivonfnotaService.updateArquivoxmlcancelamento(arquivonfnota);
					
					try{
						nota.setNotaStatus(NotaStatus.NFSE_CANCELANDO);
						notaService.alterarStatusAcao(nota, bean.getMotivo(), bean.getMotivoCancelamentoFaturamento());
					} catch (Exception e) {
						request.addError("N�o foi possivel cancelar a nota " + nota.getNumero() + ": " + e.getMessage());
						continue;
					}
					
					sucesso++;
					
					notaDocumentoService.adicionaMensagensCancelamentoNotaWebserviceTrue(request, nota.getCdNota() + "", true);					
				} else {
					request.addError("N�o foi encontrado o registro de Arquivonfnota para a nota " + nota.getNumero());
				}
			}
			
			if(sucesso > 0){
				request.addMessage("Pedidos de cancelamento de " + sucesso + " NFS-e criados com sucesso.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na cria��o do arquivo de cancelamento de NFS-e.");
			request.addError(e.getMessage());
		}
		
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView selecionarConfiguracao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.EMITIDA, NotaStatus.LIQUIDADA)){
			request.addError("A(s) nota(s) fiscal(is) tem que estar na situa��o 'EMITIDA' ou 'LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		String msgErro = notaFiscalServicoService.validaDivergenciaValoresParcela(request);
		if(org.apache.commons.lang.StringUtils.isNotEmpty(msgErro)){
			request.addError((String)msgErro);
			SinedUtil.fechaPopUp(request);
			return null;			
		}
//		comentado temporariamente
//		List<NotaFiscalServico> listaNfs = notaFiscalServicoService.findNotaSemContaBoleto(whereIn);
//		if(SinedUtil.isListNotEmpty(listaNfs)){
//			StringBuilder sb = new StringBuilder();
//			for(NotaFiscalServico nf : listaNfs){
//				sb.append(SinedUtil.makeLinkHistorico(nf.getCdNota().toString(), nf.getNumero() != null ? nf.getNumero() : nf.getCdNota().toString(), "visualizarNota")).append(",");
//			}
//			request.addError("Existe nota com o tipo de documento marcado como boleto sem conta banc�ria.<br>Nota(s): " + sb.substring(0, sb.length()-1));
//			SinedUtil.fechaPopUp(request);
//			return null;
//		}
		
		try {
			Empresa empresa = empresaService.getEmpresaNFeNotas(whereIn);

			boolean havePadrao = configuracaonfeService.havePadrao(null, Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO, empresa);
			
			SelecionarConfiguracaoBean bean = new SelecionarConfiguracaoBean();
			bean.setWhereIn(whereIn);
			bean.setEmpresa(empresa);
			
			if(!havePadrao){
				List<Configuracaonfe> listaConfiguracao = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO, empresa);
				request.setAttribute("listaConfiguracao", listaConfiguracao);
				
				return new ModelAndView("direct:/process/popup/selecionarConfiguracao", "bean", bean);
			}
			
			Configuracaonfe configuracaonfePadrao = configuracaonfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO, empresa);
			bean.setConfiguracaonfe(configuracaonfePadrao);
			return continueOnAction("criarNfse", bean);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	public ModelAndView verificaAtualizacaoDataEmissao(WebRequestContext request){
		boolean existeDataEmissaoAnteriorDataAtual = notaFiscalServicoService.existeDataEmissaoAnteriorDataAtual(SinedUtil.getItensSelecionados(request));
		return new JsonModelAndView()
			.addObject("existeDataEmissaoAnteriorDataAtual", existeDataEmissaoAnteriorDataAtual)
			.addObject("recalcular_imposto_criarnfse", parametrogeralService.getValorPorNome(Parametrogeral.RECALCULAR_IMPOSTO_CRIARNFSE));
	}

	public ModelAndView verificarEndereco(WebRequestContext request){
		boolean nota= true;
		String notaSemEnd = "";
		List<NotaFiscalServico> notas = notaFiscalServicoService.existeEndere�oCadastrado(SinedUtil.getItensSelecionados(request));
		
		if(notas!=null && notas.size() != 0){
			for (NotaFiscalServico notaSelecionada : notas) {
				if(notaSelecionada.getEnderecoCliente() == null || notaSelecionada.getEnderecoCliente().getCep() ==null){
					nota = false;
					if(!notaSemEnd.contains("\n"+notaSelecionada.getNumero()+"\n")){
						notaSemEnd = notaSemEnd.concat("\n"+notaSelecionada.getNumero()); 
					}
				}
			}
		}else{
			nota = false;
		}
		return new JsonModelAndView()
			.addObject("nota", nota)
			.addObject("semEndereco", notaSemEnd);
	}
	
	

	
	public ModelAndView verificaSituacaoNotas(WebRequestContext request){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean emitidaOuLiquidada = (notaService.haveNFWithStatus(whereIn, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA,
																			NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA));
		boolean canceladaOuCancelando = notaService.haveNFWithStatus(whereIn, NotaStatus.CANCELADA, NotaStatus.NFE_CANCELANDO);
		
		boolean denegada = notaService.haveNFWithStatus(whereIn, NotaStatus.NFE_DENEGADA);
		boolean parametroCancelarVendaDenegada = parametrogeralService.getBoolean(Parametrogeral.CANCELARVENDA_NFDENEGADA);
		
		return new JsonModelAndView()
			.addObject("emitidaOuLiquidadaNoEmissor", emitidaOuLiquidada)
			.addObject("denegada", denegada)
			.addObject("canceladaOuCancelando", canceladaOuCancelando)
			.addObject("parametroCancelarVendaDenegada", parametroCancelarVendaDenegada);
	}
	
	public void criarNfse(WebRequestContext request, SelecionarConfiguracaoBean bean){
		try{
			notaFiscalServicoService.recalcularImpostos(bean.getWhereIn());
			arquivonfService.createArquivonfByNota(request, bean.getWhereIn(), bean.getEmpresa(), bean.getConfiguracaonfe());
			request.addMessage("Arquivo de nota fiscal eletr�nica criado com sucesso.");
		} catch (SinedException e){
			request.addError("Erro na cria��o do arquivo de NFS-e.");
			request.addError(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na cria��o do arquivo de NFS-e.");
			request.addError(e.getMessage());
		}
		
		SinedUtil.fechaPopUp(request);
	}
	
	
	/**
	 * Action que envia o XML por e-mail para o cliente.
	 *
	 * @param request
	 * @return
	 * @since 26/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView enviarXmlParaCliente(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if (notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA)){
			request.addError("Para envio da NFS-e as notas tem que estar com a situa��o 'NFS-e EMITIDA' ou 'NFS-e LIQUIDADA'.");
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
		}
		
		EnvioEmailResultadoBean resultado = arquivonfService.enviarNFCliente(whereIn, TipoEnvioNFCliente.SERVICO_XML); 
		
		if(resultado.getSucesso() > 0){
			request.addMessage(resultado.getSucesso() + " NFS-e(s) enviada(s) para o(s) cliente(s) com sucesso.");
		}
		
		if(resultado.getErro() > 0){
			request.addError(resultado.getErro() + " NFS-e(s) n�o foi(ram) enviada(s) para o(s) cliente(s).");
		}
		
		if(resultado.getListaErro() != null){
			for (String erro : resultado.getListaErro()) {
				request.addError(erro);
			}
		}
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
	}
	
	/**
	 * Action que realiza o download do arquivo XML da NFS-e.
	 *
	 * @param request
	 * @return
	 * @since 26/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView downloadNfse(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
		}
		
		List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findForDownloadXml(whereIn);
		
		if(listaArquivonfnota != null){
			for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
				if(arquivonfnota.getArquivonf() != null && 
						arquivonfnota.getArquivonf().getConfiguracaonfe() != null && 
						arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice() != null){
					
					if(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().equals(Prefixowebservice.SPISS_HOM) ||
								arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().equals(Prefixowebservice.SPISS_PROD)){
						request.addError("N�o � poss�vel realizar o download do XML de uma NFS-e da Prefeitura de S�o Paulo.");
						return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
					}
					if(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().equals(Prefixowebservice.BLUMENAUISS_HOM) ||
								arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().equals(Prefixowebservice.BLUMENAUISS_PROD)){
						request.addError("N�o � poss�vel realizar o download do XML de uma NFS-e da Prefeitura de Blumenau.");
						return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
					}
					if(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_HOM) ||
							arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().equals(Prefixowebservice.LIMEIRA_PROD)){
						request.addError("N�o � poss�vel realizar o download do XML de uma NFS-e da Prefeitura de Limeira. Pois n�o existe troca de XML nessa integra��o. Caso precise deste XML fa�a o download direto do site da prefeitura.");
						return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
					}
				}
			}
			
			try{
				if(listaArquivonfnota.size() == 1){
					Arquivonfnota arquivonfnota = listaArquivonfnota.get(0);
					Arquivo arquivo = null;
					
					if(Prefixowebservice.CASTELO_HOM.equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice()) || 
							Prefixowebservice.CASTELO_PROD.equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice())){
						arquivo = arquivonfService.getArquivoXmlNfseCastelo(arquivonfnota);
					}else if("smarapd".equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().getPrefixo())){
						arquivo = arquivonfService.getArquivoXmlNfseSmarapd(arquivonfnota);
					} else if(Prefixowebservice.SOROCABAISS_HOM.equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice()) ||
							Prefixowebservice.SOROCABAISS_PROD.equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice())){ 
						arquivo = arquivonfService.getArquivoXmlNfseSorocaba(arquivonfnota);
					} else{
						arquivo = arquivonfService.getArquivoXmlNfse(arquivonfnota);
					}
					
					Resource resource = new Resource("text/xml", "nfse_" + arquivonfnota.getNumeronfse() + ".xml", arquivo.getContent());
					return new ResourceModelAndView(resource);
					
				} else {
					Integer contador = 1; 
					List<Arquivo> listaXML = new ArrayList<Arquivo>();
					for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
						Arquivo arquivoXML = null;
						if("smarapd".equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().getPrefixo())){
							arquivoXML = arquivonfService.getArquivoXmlNfseSmarapd(arquivonfnota);
						} else if(Prefixowebservice.SOROCABAISS_HOM.equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice()) ||
								Prefixowebservice.SOROCABAISS_PROD.equals(arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice())){ 
							arquivoXML = arquivonfService.getArquivoXmlNfseSorocaba(arquivonfnota);
						} else { 
							arquivoXML = arquivonfService.getArquivoXmlNfse(arquivonfnota);
						}
						if(arquivoXML != null){
							if(arquivoXML.getName() != null && arquivoXML.getName().contains("null")){
								arquivoXML.setName(arquivoXML.getName().replace("null", "_sem_numero_"+contador));
								contador++;
							}
							listaXML.add(arquivoXML);
						}
					}
					
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					if(listaXML != null && !listaXML.isEmpty()){				
						ZipOutputStream zip = new ZipOutputStream(byteArrayOutputStream);
						ZipEntry zipEntry;
						
						for (Arquivo arquivo : listaXML) {
							if(arquivo != null){
								zipEntry = new ZipEntry(arquivo.getName());
								zipEntry.setSize(arquivo.getSize());
								zip.putNextEntry(zipEntry);
								zip.write(arquivo.getContent());
							}
						}
						
						zip.closeEntry();
						zip.close();
					}	
					
					Resource resource = new Resource("application/zip", "nfse_" + SinedUtil.datePatternForReport() + ".zip", byteArrayOutputStream.toByteArray());
					return new ResourceModelAndView(resource);
				}
			} catch (Exception e) {
				request.addError(e);
			}
		}
		
		request.addError("Erro no download do XML da(s) Nota(s).");
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
	}
	
	/**
	 * 
	 * @param request
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView enviarNfseXmlCliente(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if (notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA)){
			request.addError("Para envio da NFS-e as notas tem que estar com a situa��o 'NFS-e EMITIDA' ou 'NFS-e LIQUIDADA'.");
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
		}
		
		EnvioEmailResultadoBean resultado = arquivonfService.enviarNFCliente(whereIn, TipoEnvioNFCliente.SERVICO_PDF, TipoEnvioNFCliente.SERVICO_XML); 
		
		if(resultado.getSucesso() > 0){
			request.addMessage(resultado.getSucesso() + " NFS-e(s) enviada(s) para o(s) cliente(s) com sucesso.");
		}
		
		if(resultado.getErro() > 0){
			request.addError(resultado.getErro() + " NFS-e(s) n�o foi(ram) enviada(s) para o(s) cliente(s).");
		}
		
		if(resultado.getListaErro() != null){
			for (String erro : resultado.getListaErro()) {
				request.addError(erro);
			}
		}
		
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
	}
	
}