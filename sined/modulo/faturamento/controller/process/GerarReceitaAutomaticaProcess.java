package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContacorrenteService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentocomissaoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.HistoricooperacaoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoduplicataService;
import br.com.linkcom.sined.geral.service.NotafiscalservicoduplicataService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.geral.service.TipotaxaService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.utils.StringUtils;

@Controller(path="/faturamento/process/GerarReceitaAutomatica", authorizationModule=ProcessAuthorizationModule.class)
public class GerarReceitaAutomaticaProcess extends MultiActionController {
	
	private NotaService notaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private DocumentoService documentoService;
	private RateioitemService rateioitemService;
	private NotaHistoricoService notaHistoricoService;
	private DocumentotipoService documentotipoService;
	private NotaDocumentoService notaDocumentoService;
	private RateioService rateioService;
	private ContareceberService contareceberService;
	private DocumentocomissaoService documentocomissaoService;
	private ContacorrenteService contacorrenteService;
	private TipotaxaService tipotaxaService;
	private ArquivonfnotaService arquivonfnotaService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ParametrogeralService parametrogeralService;
	private ContaService contaService;
	private ContratoService contratoService;
	private NotafiscalservicoduplicataService notafiscalservicoduplicataService;
	private NotafiscalprodutoduplicataService notafiscalprodutoduplicataService;
	private HistoricooperacaoService historicooperacaoService;
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}	
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {this.documentocomissaoService = documentocomissaoService;}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {this.contacorrenteService = contacorrenteService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setFechamentofinanceiroService(	FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;	}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setNotafiscalservicoduplicataService(NotafiscalservicoduplicataService notafiscalservicoduplicataService) {this.notafiscalservicoduplicataService = notafiscalservicoduplicataService;}
	public void setNotafiscalprodutoduplicataService(NotafiscalprodutoduplicataService notafiscalprodutoduplicataService) {this.notafiscalprodutoduplicataService = notafiscalprodutoduplicataService;}
	public void setHistoricooperacaoService(HistoricooperacaoService historicooperacaoService) {this.historicooperacaoService = historicooperacaoService;}
	
	/**
	 * Faz a verifica��o das notas selecionadas na tela para gerar receita automaticamente.
	 *
	 * @see br.com.linkcom.sined.geral.service.NotaService#findForReceita
	 * @see br.com.linkcom.sined.util.SinedUtil#getItensSelecionados
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView verificaNotas (WebRequestContext request){
		
		Boolean entrada = Boolean.valueOf(request.getParameter("ENTRADA") != null ? request.getParameter("ENTRADA") : "false");
		Boolean agrupamento = Boolean.valueOf(request.getParameter("agrupamento") != null ? request.getParameter("agrupamento") : "false");
		
		Object obj = null;
		if(entrada){
			obj = SinedUtil.getItensSelecionados(request);
		}else {
			obj = request.getSession().getAttribute("CODIGOS_NOTAFISCALSERVICO_FATURAMENTO");
			request.getSession().removeAttribute("CODIGOS_NOTAFISCALSERVICO_FATURAMENTO");
		}
		
		if(obj == null || obj.toString().equals("")){
			request.addError("Nenhum item selecionado");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		String whereIn = obj.toString();
		
		//Verifica��o de data limite do ultimo fechamento. 
		NotaFiscalServico aux = new NotaFiscalServico();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem notas cuja data de vencimento refere-se a um per�odo j� fechado.");
			return new ModelAndView("redirect:" + request.getParameter("CONTROLLER")); 
		}
		
		List<Nota> listaNota = notaService.findForReceita(whereIn);
		
		String idsStatus = "Para gerar receita a nota tem que estar com a situa��o 'EMITIDA' ou 'NFS-e EMITIDA'. Notas erradas: ";
		boolean erradoStatus = false;
		
		boolean existeNotaavulsa = false;
		boolean existeNotaSemContrato = false;
		StringBuilder numeroNota = new StringBuilder();
		for (Nota nota : listaNota) {
			if(nota.getListaNotaContrato() == null || nota.getListaNotaContrato().size() == 0){
				existeNotaSemContrato = true;
				if(SinedUtil.isListEmpty(nota.getListaNotavenda())){
					if(org.apache.commons.lang.StringUtils.isNotBlank(nota.getNumero())){
						numeroNota.append(nota.getNumero()).append(",");
					}
					existeNotaavulsa = true;
				}
			}
			if(!nota.getNotaStatus().equals(NotaStatus.EMITIDA) && !nota.getNotaStatus().equals(NotaStatus.NFSE_EMITIDA)){
				erradoStatus = true;
				if(nota.getNumero() != null){
					idsStatus += nota.getNumero() + ", ";
				} else {
					idsStatus += "sem n�mero, ";
				}
			}
		}
		
		if(erradoStatus){
			request.addError(idsStatus.substring(0, idsStatus.length()-2));
		}
		
		List<NotaFiscalServico> lista = notaFiscalServicoService.findNotaWithNaturezaoperacao(whereIn);
		boolean existeNotaQueNaoGeraReceber = false;
		
		String erroNotaNaoGerarReceita = "N�o � permitido gerar receita de uma nota que tenha a natureza de opera��o marcada para n�o gerar receita. Notas nessa situa��o: ";
		for(NotaFiscalServico nota: lista){
			if(nota.getNaturezaoperacao() != null && Boolean.TRUE.equals(nota.getNaturezaoperacao().getSimplesremessa())){
				existeNotaQueNaoGeraReceber = true;
				if(nota.getNumero() != null){
					erroNotaNaoGerarReceita += nota.getNumero() + ", ";
				} else {
					erroNotaNaoGerarReceita += "sem n�mero, ";
				}
			}
		}
		
		if(existeNotaQueNaoGeraReceber){
			request.addError(erroNotaNaoGerarReceita.substring(0, erroNotaNaoGerarReceita.length()-2));
		}
		
		if(erradoStatus || existeNotaQueNaoGeraReceber){
			String entradacontroller = "redirect:" + request.getParameter("CONTROLLER");
			if("true".equals(request.getParameter("ENTRADA"))){
				entradacontroller = entradacontroller + "?ACAO=consultar&cdNota=" + whereIn;
			}
			return new ModelAndView(entradacontroller);
		}
		
		
		boolean existeNotaClienteEmpresadiferente = false;
		boolean existeNotaSemDuplicata = false;
		if(agrupamento){
			Cliente cliente = null;
			Empresa empresa = null;
			Conta contaboleto = null;
			Contacarteira contacarteira = null;
			Prazopagamento prazopagamento = null;
			String chaveDuplicata = null;
			int i = 0;
			for (Nota nota : listaNota) {
				if(i == 0){
					if(cliente == null){ 
						cliente = nota.getCliente();
					}
					if(empresa == null){ 
						empresa = nota.getEmpresa();
					}
					if(contaboleto == null){ 
						contaboleto = nota.getContaboleto();
					}
					if(contacarteira == null){ 
						contacarteira = nota.getContacarteira();
					}
					if(prazopagamento == null){ 
						prazopagamento = nota.getPrazopagamentofatura();
					}
					if(chaveDuplicata == null){
						chaveDuplicata = getChaveMapaReceitaDuplicata(nota);
					}
				}else if(!(cliente.equals(nota.getCliente())) || (empresa != null && 
															empresa.getCdpessoa() != null && 
															nota.getEmpresa() != null && 
															!empresa.getCdpessoa().equals(nota.getEmpresa().getCdpessoa())) ||
															
															(contaboleto != null && 
															contaboleto.getCdconta() != null && 
															nota.getContaboleto() != null && 
															!contaboleto.getCdconta().equals(nota.getContaboleto().getCdconta())) ||
															
															(contacarteira != null && 
															contacarteira.getCdcontacarteira() != null && 
															nota.getContacarteira() != null && 
															!contacarteira.getCdcontacarteira().equals(nota.getContacarteira().getCdcontacarteira())) ||
															
															(prazopagamento != null && 
															prazopagamento.getCdprazopagamento() != null && 
															nota.getPrazopagamentofatura() != null && 
															!prazopagamento.getCdprazopagamento().equals(nota.getPrazopagamentofatura().getCdprazopagamento())) ||
															
															(!chaveDuplicata.equals(getChaveMapaReceitaDuplicata(nota)))){
					existeNotaClienteEmpresadiferente = true;
				}
				if((NotaTipo.NOTA_FISCAL_SERVICO.equals(nota.getNotaTipo()) && !notafiscalservicoduplicataService.existeDuplicata(new NotaFiscalServico(nota.getCdNota()))) ||
						(NotaTipo.NOTA_FISCAL_PRODUTO.equals(nota.getNotaTipo()) && !notafiscalprodutoduplicataService.existeDuplicata(new Notafiscalproduto(nota.getCdNota())))){
					existeNotaSemDuplicata = true;
				}
				i++;
			}
			if(existeNotaClienteEmpresadiferente && existeNotaSemDuplicata){
				request.addError("N�o � permitido faturar nota(s) de cliente(s) e empresa(s) diferentes ou nota(s) sem duplicatas.");
				return new ModelAndView("redirect:" + request.getParameter("CONTROLLER"));
			}
		}
		
		if(existeNotaavulsa && (agrupamento || existeNotaSemContrato) && ((!agrupamento || existeNotaClienteEmpresadiferente) && listaNota.size() > 1)){
			request.addError("N�o � permitido gerar receita autom�tica com nota(s) avulsa(s). " + (numeroNota.length() > 0 ? "Nota(s): " + numeroNota.substring(0, numeroNota.length()-1) : ""));
			return new ModelAndView("redirect:" + request.getParameter("CONTROLLER"));
		}		
		
		if(agrupamento || existeNotaSemContrato){
			if((!agrupamento || existeNotaClienteEmpresadiferente) && listaNota.size() > 1){
				request.setAttribute("agrupamento", agrupamento);
				return continueOnAction("gerarReceitaNotaAgrupamento", listaNota);
			}else {
				return continueOnAction("gerarReceitaNotaAvulsa", listaNota);
			}
		}else {
			return continueOnAction("gerarReceita", listaNota);
		}
	}
	
	public ModelAndView abrirConfirmacaoAgrupamentoGerarreceita(WebRequestContext request){		
		Object obj = request.getSession().getAttribute("CODIGOS_NOTAFISCALSERVICO_FATURAMENTO");
		if(obj == null || obj.toString().equals("")){
			request.addError("Nenhum item selecionado");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		String whereIn = obj.toString();	
		
		//Verifica��o de data limite do ultimo fechamento. 
		NotaFiscalServico aux = new NotaFiscalServico();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem notas cuja data de vencimento refere-se a um per�odo j� fechado.");
			return new ModelAndView("redirect:" + request.getParameter("CONTROLLER")); 
		}
		
		List<Nota> listaNota = notaService.findForReceita(whereIn);
		
		String idsStatus = "Para gerar receita a nota tem que estar com a situa��o 'EMITIDA' ou 'NFS-e EMITIDA'. Notas erradas: ";
		boolean erradoStatus = false;
		
		for (Nota nota : listaNota) {
			if(!nota.getNotaStatus().equals(NotaStatus.EMITIDA) && !nota.getNotaStatus().equals(NotaStatus.NFSE_EMITIDA)){
				erradoStatus = true;
				if(nota.getNumero() != null){
					idsStatus += nota.getNumero() + ", ";
				} else {
					idsStatus += "sem n�mero, ";
				}
			}
		}
		
		if(erradoStatus){
			request.addError(idsStatus.substring(0, idsStatus.length()-2));
		}
		
		List<NotaFiscalServico> lista = notaFiscalServicoService.findNotaWithNaturezaoperacao(whereIn);
		boolean existeNotaQueNaoGeraReceber = false;
		String erroNotaNaoGerarReceita = "N�o � permitido gerar receita de uma nota que tenha a natureza de opera��o marcada para n�o gerar receita. Notas nessa situa��o: ";
		for(NotaFiscalServico nota: lista){
			if(nota.getNaturezaoperacao() != null && Boolean.TRUE.equals(nota.getNaturezaoperacao().getSimplesremessa())){
				existeNotaQueNaoGeraReceber = true;
				if(nota.getNumero() != null){
					erroNotaNaoGerarReceita += nota.getNumero() + ", ";
				} else {
					erroNotaNaoGerarReceita += "sem n�mero, ";
				}
			}
		}
		
		if(existeNotaQueNaoGeraReceber){
			request.addError(erroNotaNaoGerarReceita.substring(0, erroNotaNaoGerarReceita.length()-2));
		}
		
		if(erradoStatus || existeNotaQueNaoGeraReceber){
			String entrada = "/faturamento/crud/NotaFiscalServico";
			if("true".equals(request.getParameter("ENTRADA"))){
				entrada = entrada + "?ACAO=consultar&cdNota=" + whereIn;
			} 
			
			SinedUtil.redirecionamento(request, entrada);
            return null;
		}
		
		if(whereIn.split(",").length > 1){ 
			return new ModelAndView("direct:/process/popup/confirmacaoAgrupamentoGerarreceitaautomatica");
		} else {				
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/process/GerarReceitaAutomatica?ACAO=verificaNotas&CONTROLLER=/faturamento/crud/NotaFiscalServico&agrupamento=false';</script>");
			return null;
		}
	}
	
	/**
	 * M�todo que gerar receita de nota avulsa
	 *
	 * @param request
	 * @param listaNota
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarReceitaNotaAvulsa(WebRequestContext request, List<Nota> listaNota){
		
		String entrada = "redirect:" + request.getParameter("CONTROLLER");
		if("true".equals(request.getParameter("ENTRADA"))){
			entrada = entrada + "?ACAO=consultar&cdNota=" + listaNota.get(0).getCdNota();
		} 
		
		Documentotipo documentotipo = documentotipoService.findNotafiscal();
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null){
			request.addError("Favor cadastrar um tipo de documento que seja nota fiscal.");
			return new ModelAndView(entrada); 
		}
		
		for (Nota nota : listaNota) {
			if(!nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
				request.addError("N�o � poss�vel gerar receita automaticamente com uma Nota Fiscal de Produto.");
				return new ModelAndView(entrada); 
			}
		}
		
		return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=criarGerarreceitaServico&selectedNotasGerarreceita=" + CollectionsUtil.listAndConcatenate(listaNota,"cdNota", ","));
	}
	
	public ModelAndView gerarReceitaNotaAgrupamento(WebRequestContext request, List<Nota> listaNota){
		boolean agrupamento = request.getAttribute("agrupamento") != null ? (Boolean) request.getAttribute("agrupamento") : Boolean.FALSE; 
		String entrada = "redirect:" + request.getParameter("CONTROLLER");
		if("true".equals(request.getParameter("ENTRADA"))){
			entrada = entrada + "?ACAO=consultar&cdNota=" + listaNota.get(0).getCdNota();
		} 
		
		Documentotipo documentotipo = documentotipoService.findNotafiscal();
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null){
			request.addError("Favor cadastrar um tipo de documento que seja nota fiscal.");
			return new ModelAndView(entrada); 
		}
		
		HashMap<String, List<Nota>> mapNota = new HashMap<String, List<Nota>>();
		for (Nota nota : listaNota) {
			if(nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
				String chave = getChaveMapaReceita(nota, agrupamento);
				List<Nota> listaNova = mapNota.get(chave);
				if(listaNova == null){
					listaNova = new ArrayList<Nota>();
				}
				listaNova.add(nota);
				mapNota.put(chave, listaNova);
			} else {
				request.addError("N�o � poss�vel gerar receita automaticamente com uma Nota Fiscal de Produto.");
				return new ModelAndView(entrada); 
			}
		}
		
		boolean sucesso = false;
		String whereInNota;
		String whereNumeroNota;
		for(String chave : mapNota.keySet()){
			whereInNota = CollectionsUtil.listAndConcatenate(mapNota.get(chave), "cdNota", ",");
			whereNumeroNota = CollectionsUtil.listAndConcatenate(mapNota.get(chave), "numero", ",");
			Documento documento = contareceberService.gerarReceitaNota(whereInNota);
			
			if(documento.getRateio() != null && SinedUtil.isListNotEmpty(documento.getRateio().getListaRateioitem())){
				boolean rateioInvalido = false;
				for(Rateioitem rateioitem : documento.getRateio().getListaRateioitem()){
					if(rateioitem.getContagerencial() == null){
						request.addError("N�o foi poss�vel gerar receita automaticamente. Material ou empresa sem conta gerencial para o rateio. " + (org.apache.commons.lang.StringUtils.isNotBlank(whereNumeroNota) ? "Nota(s): " + whereNumeroNota : ""));
						rateioInvalido = true;
						break;
					}
					if(rateioitem.getCentrocusto() == null){
						request.addError("N�o foi poss�vel gerar receita automaticamente. Material ou empresa sem centro de custo para o rateio. " + (org.apache.commons.lang.StringUtils.isNotBlank(whereNumeroNota) ? "Nota(s): " + whereNumeroNota : ""));
						rateioInvalido = true;
						break;
					}
				}
				if(rateioInvalido){
					continue;
				}
			}
			
			documento.setFromGerarreceitanota(true);
			documento.setWhereInNota(whereInNota);
			notaService.liquidar(whereInNota, documento);
			sucesso = true;
		}
		
		if(sucesso){
			request.addMessage("Receita(s) gerada(s) com sucesso.");
		}
		return new ModelAndView(entrada); 
	}
	
	private String getChaveMapaReceita(Nota nota, boolean agrupamento) {
		if(nota != null && NotaTipo.NOTA_FISCAL_SERVICO.equals(nota.getNotaTipo()) && nota.getPrazopagamentofatura() == null){
			List<Notafiscalservicoduplicata> lista = notafiscalservicoduplicataService.findByNotafiscalservico(new NotaFiscalServico(nota.getCdNota()));
			if(lista != null && lista.size() == 1){
				nota.setPrazopagamentofatura(Prazopagamento.UNICA);
			}
		}
		return  !agrupamento ? nota.getCdNota().toString() : (
				(nota.getEmpresa() != null ? nota.getEmpresa().getCdpessoa() : "") + "|" +
				(nota.getCliente() != null ? nota.getCliente().getCdpessoa() : "") + "|" +
				(nota.getContaboleto() != null ? nota.getContaboleto().getCdconta() : "") + "|" +
				(nota.getContacarteira() != null ? nota.getContacarteira().getCdcontacarteira() : "") + "|" +
				(nota.getPrazopagamentofatura() != null ? nota.getPrazopagamentofatura().getCdprazopagamento() : "")  + "|" +
				getChaveMapaReceitaDuplicata(nota))
		;
	}
	
	private String getChaveMapaReceitaDuplicata(Nota nota) {
		if(nota != null && NotaTipo.NOTA_FISCAL_SERVICO.equals(nota.getNotaTipo())){
			List<Notafiscalservicoduplicata> lista = notafiscalservicoduplicataService.findByNotafiscalservico(new NotaFiscalServico(nota.getCdNota()));
			if(SinedUtil.isListNotEmpty(lista)){
				StringBuilder chave = new StringBuilder();
				for(Notafiscalservicoduplicata duplicata : lista){
					chave.append(duplicata.getDocumentotipo() != null ? duplicata.getDocumentotipo().getCddocumentotipo() : "").append("|");
					chave.append(duplicata.getDtvencimento() != null ? duplicata.getDtvencimento().toString() : "").append("|");
				}
				return chave.toString();
			}
		}
		
		return "";
	}
	
	/**
	 * A��o para gerar a receita.
	 * 
	 * @see br.com.linkcom.sined.modulo.faturamento.controller.process.GerarReceitaAutomaticaProcess#geraReceita
	 * 
	 * @param request
	 * @param listaNota
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarReceita(WebRequestContext request, List<Nota> listaNota){
		
		String entrada = "redirect:" + request.getParameter("CONTROLLER");
		if("true".equals(request.getParameter("ENTRADA"))){
			entrada = entrada + "?ACAO=consultar&cdNota=" + listaNota.get(0).getCdNota();
		} 
		
//		String entrada = "redirect:/faturamento/crud/Nota";
//		if("true".equals(request.getParameter("ENTRADA")) && listaNota.size() == 1){
//			entrada = "redirect:/faturamento/crud/NotaFiscalServico?ACAO=consultar&cdNota="+listaNota.get(0).getCdNota();
//		} 
		
		Documentotipo documentotipo = documentotipoService.findNotafiscal();
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null){
			request.addError("Favor cadastrar um tipo de documento que seja nota fiscal.");
			return new ModelAndView(entrada); 
		}
		
		for (Nota nota : listaNota) {
			if(nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
				try {
					this.geraReceita(nota, documentotipo);
				} catch (Exception e) {
					request.addError("N�o foi poss�vel gerar receita: " + e.getMessage());
					return new ModelAndView(entrada); 
				}
			} else {
				request.addError("N�o � poss�vel gerar receita automaticamente com uma Nota Fiscal de Produto.");
				return new ModelAndView(entrada); 
			}
		}
		
		request.addMessage("Receita(s) gerada(s) com sucesso.");
		return new ModelAndView(entrada); 
	}

	/**
	 * Gera a conta a receber e atualiza a nota.
	 *
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#loadForReceita
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#findByRateio
	 * @see br.com.linkcom.sined.geral.service.TaxaService#findTaxa
	 * @see br.com.linkcom.sined.geral.service.NotaService#liquidarNota
	 *
	 * @param nota
	 * @author Rodrigo Freitas
	 * @param documentotipo 
	 */
	private void geraReceita(Nota nota, Documentotipo documentotipo) {
		
		NotaFiscalServico nfs = notaFiscalServicoService.loadForReceita(nota.getCdNota());
		Contrato contrato = nfs.getListaNotaContrato().get(0).getContrato();
		
		if(SinedUtil.isListNotEmpty(nfs.getListaDuplicata())){
			notaFiscalServicoService.setPrazogapamentoUnico(nfs);
			
			boolean considerarDiaUtil = nfs.getPrazopagamentofatura() != null && 
										nfs.getPrazopagamentofatura().getDataparceladiautil() != null &&
										nfs.getPrazopagamentofatura().getDataparceladiautil();
			Integer totalParcela = nfs.getListaDuplicata().size();
			Notafiscalservicoduplicata primeira = nfs.getListaDuplicata().remove(0);
			
			Date dtvencimento = null;
			if(considerarDiaUtil){
				dtvencimento = SinedDateUtils.getProximaDataUtil(primeira.getDtvencimento(), nfs.getEmpresa());
			}else {
				dtvencimento = primeira.getDtvencimento();
			}
			Documento doc = geraReceita(nfs, contrato, primeira.getDocumentotipo() != null ? primeira.getDocumentotipo() : documentotipo, primeira.getValor(), dtvencimento);
			doc.setDocumentoacao(Documentoacao.DEFINITIVA);
			doc.setValor(primeira.getValor());
			doc.setNumero(notaFiscalServicoService.formataNumNfse(nfs.getNumero()));
			
			String numeroNFSe = null;
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nfs);
			if(arquivonfnota != null && org.apache.commons.lang.StringUtils.isNotBlank(arquivonfnota.getNumeronfse())){
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
					numeroNFSe = notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse());
					doc.setNumero(numeroNFSe);
				}
			}
			doc.setDocumentotipo(primeira.getDocumentotipo() != null ? primeira.getDocumentotipo() : documentotipo);
			doc.setDocumentotipoTransient(doc.getDocumentotipo());
			
			List<Documento> listaDoc = new ArrayList<Documento>();
			Documento docTmp;
			int parcela = 1;
			
			historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, null, nfs, (parcela)+"/"+totalParcela);
			
			for (Notafiscalservicoduplicata dup : nfs.getListaDuplicata()) {
				doc.setPrazo(nfs.getPrazopagamentofatura());
				doc.setFinanciamento(Boolean.TRUE);
				docTmp = new Documento();
				
				docTmp.setParcela(++parcela);
				if(considerarDiaUtil){
					docTmp.setDtvencimento(SinedDateUtils.getProximaDataUtil(dup.getDtvencimento(), nfs.getEmpresa()));
				}else {
					docTmp.setDtvencimento(dup.getDtvencimento());
				}
				docTmp.setDocumentoacao(Documentoacao.DEFINITIVA);
				docTmp.setValor(dup.getValor());
				docTmp.setNumero(org.apache.commons.lang.StringUtils.isNotBlank(numeroNFSe) ? numeroNFSe :notaFiscalServicoService.formataNumNfse(nfs.getNumero()));
				docTmp.setDocumentotipo(dup.getDocumentotipo());
				docTmp.setDocumentotipoTransient(dup.getDocumentotipo());
				
				historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(doc, docTmp, nfs, (parcela+1)+"/"+totalParcela);
				
				listaDoc.add(docTmp);
			}
			
			doc.setListaDocumento(listaDoc);
			documentoService.addTaxaMovimentoTipoDocumento(doc);
			
			contareceberService.saveOrUpdate(doc);
			
			notaService.liquidarNota(nfs);
			
			if(SinedUtil.isListNotEmpty(doc.getListaDocumento())){
				for(Documento docFinanciamento : doc.getListaDocumento()){
					salvarVinculoDocumentoReceita(docFinanciamento, contrato, nfs);
				}
			}else {
				salvarVinculoDocumentoReceita(doc, contrato, nfs);
			}
			
			NotaHistorico notaHistorico = null;
			if(SinedUtil.isListNotEmpty(doc.getListaDocumento())){
				int i = 0;
				for(Documento docFinanciamento : doc.getListaDocumento()){
					if(i == 0){
						notaHistorico = new NotaHistorico(null, 
								nfs, 
								nfs.getNotaStatus(), 
								"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+docFinanciamento.getCddocumento()+")\">"+docFinanciamento.getCddocumento()+"</a>", 
								SinedUtil.getUsuarioLogado().getCdpessoa(), 
								new Timestamp(System.currentTimeMillis()));
					}else {
						notaHistorico.setObservacao(notaHistorico.getObservacao() + ", <a href=\"javascript:visualizarContaReceber("+docFinanciamento.getCddocumento()+")\">"+docFinanciamento.getCddocumento()+"</a>");
					}
					i++;
				}
			}else {
				notaHistorico = new NotaHistorico(null, 
						nfs, 
						nfs.getNotaStatus(), 
						"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+doc.getCddocumento()+")\">"+doc.getCddocumento()+"</a>", 
						SinedUtil.getUsuarioLogado().getCdpessoa(), 
						new Timestamp(System.currentTimeMillis()));
			}
			
			if(notaHistorico != null){
				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
			
			if(SinedUtil.isListNotEmpty(doc.getListaDocumento())){
				for(Documento docFinanciamento : doc.getListaDocumento()){
					verificaComissionamentoReceita(docFinanciamento, contrato, nfs);
				}
			}else {
				verificaComissionamentoReceita(doc, contrato, nfs);
			}
		}else {
			Documento documento = geraReceita(nfs, contrato, documentotipo, nfs.getValorNota(), null);
			documentoService.saveOrUpdate(documento);
			
			notaService.liquidarNota(nfs);
			
			salvarVinculoDocumentoReceita(documento, contrato, nfs);
			
			NotaHistorico notaHistorico = new NotaHistorico(null, 
					nfs, 
					nfs.getNotaStatus(), 
					"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>", 
					SinedUtil.getUsuarioLogado().getCdpessoa(), 
					new Timestamp(System.currentTimeMillis()));
			
			notaHistoricoService.saveOrUpdate(notaHistorico);
			
			verificaComissionamentoReceita(documento, contrato, nfs);
		}
	}
	
	private void salvarVinculoDocumentoReceita(Documento doc, Contrato contrato, NotaFiscalServico nfs) {
		NotaDocumento notaDocumento = new NotaDocumento();
		notaDocumento.setDocumento(doc);
		notaDocumento.setNota(nfs);
		notaDocumentoService.saveOrUpdate(notaDocumento);
	}
	
	private void verificaComissionamentoReceita(Documento doc, Contrato contrato, NotaFiscalServico nfs) {
		doc.setContrato(contrato);
		if(nfs.getListaNotaContrato() != null && nfs.getListaNotaContrato().size() > 0){
			List<Documentocomissao> listaDocumentocomissao = documentocomissaoService.findComissaocontrato(contrato, nfs);
			if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
				for(Documentocomissao documentocomissao : listaDocumentocomissao){					
					documentocomissaoService.atualizaDocumentocomissao(documentocomissao, doc);
				}
			}else
				contratoService.verificaComissionamentoDocumento(doc, false, false);
		}
	}
	
	private Documento geraReceita(NotaFiscalServico nfs, Contrato contrato, Documentotipo documentotipo, Money valorNota, Date dtvencimento) {
		notaFiscalServicoService.setPrazogapamentoUnico(nfs);
		
		Documento documento = new Documento();
		
		if(contrato.getDocumentotipo() != null){
			documento.setDocumentotipo(contrato.getDocumentotipo());
		} else {
			documento.setDocumentotipo(documentotipo);
		}
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documento.setDocumentoacao(Documentoacao.DEFINITIVA);
		documento.setTipopagamento(Tipopagamento.CLIENTE);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		
		documento.setPessoa(nfs.getCliente());
		documento.setCliente(nfs.getCliente());
		documento.setDtemissao(nfs.getDtEmissao());
		documento.setDtvencimento(dtvencimento != null ? dtvencimento : (nfs.getDtVencimento() != null ? nfs.getDtVencimento() : nfs.getDtEmissao()));
		documento.setNumero(notaFiscalServicoService.formataNumNfse(nfs.getNumero()));
		documento.setValor(valorNota);
		
		documento.setEmpresa(nfs.getEmpresa());
		documento.setConta(contrato.getConta());
		
		if(nfs.getCliente().getListaEndereco() != null){
			for (Endereco endereco : nfs.getCliente().getListaEndereco()) {
				if(endereco.getEnderecotipo() != null && endereco.getEnderecotipo().equals(Enderecotipo.COBRANCA)){
					documento.setEndereco(endereco);
					break;
				}
			}
		}
		if(documento.getEndereco() == null){
			documento.setEndereco(nfs.getEnderecoCliente());
		}
		if(documento.getEndereco() == null && nfs.getCliente().getListaEndereco() != null){
			documento.setEndereco(nfs.getCliente().getEnderecoWithPrioridade(Enderecotipo.COBRANCA, Enderecotipo.UNICO));
		}
		
		if(contrato.getConta() != null){
			Conta conta = contrato.getConta(); 
			if (conta.getBanco() != null && conta.getBanco().getNumero() != null &&
					contrato.getConta().getBanco().getNumero() == 341){ //Ita�
				if (conta.getNossonumerointervalo() != null && conta.getNossonumerointervalo() && documento.getNossonumero() == null)
					documento.setNossonumero(StringUtils.stringCheia(contareceberService.getNextNossoNumero(conta).toString(), "0", 8, false));				
			}
			documento.setVinculoProvisionado(conta);
		} else {
			documento.setVinculoProvisionado(documentotipo.getContadestino());
		}
		
		if(contrato.getConta() != null){
			Conta conta = contaService.carregaContaComMensagem(contrato.getConta(), null);
			
			//Preenche a carteira do documento com a carteira padr�o
			documento.setContacarteira(conta.getContacarteira());
			
			if(conta.getContacarteira().getMsgboleto1() != null)
				documento.setMensagem4(conta.getContacarteira().getMsgboleto1());
			
			if(conta.getContacarteira().getMsgboleto2() != null)
				documento.setMensagem5(conta.getContacarteira().getMsgboleto2());
			
			if(documento.getContacarteira() != null && documento.getContacarteira().getCdcontacarteira() == null)
				documento.setContacarteira(null);
		}
		
		if (Boolean.TRUE.equals(contrato.getReajuste())){
			documento.setMensagem6(contrato.getMensagemBoletoReajuste());
		}
		
		Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nfs);
		if(arquivonfnota != null){
			if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
				documento.setNumero(notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse()));
			}
			if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
				documento.setMensagem1("Referente a NF: " + notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse()) + "  " + (arquivonfnota.getCodigoverificacao() != null && !arquivonfnota.getCodigoverificacao().equals("") ? " C�digo de Verifica��o da NFS-e: "+arquivonfnota.getCodigoverificacao() : ""));
				if(documento.getMensagem1().length() > 80){
					documento.setMensagem1("Referente a NF: " + notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse()));
				}
			}
		} else if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MENSAGEM_NUMERO_NF)){
			documento.setMensagem1("Referente a NF: " + (nfs.getNumero() != null ? nfs.getNumero() : "<sem n�mero>"));
		}
		
		if(nfs.getListaItens().size() > 1){
			//documento.setDescricao("Documento gerado a partir da NF " + (nfs.getNumero() != null ? nfs.getNumero() : "<sem n�mero>"));	
			notaFiscalServicoService.setDescricaoObservacaoDocumentoItensNota(nfs, documento, "Presta��o de Servi�os");
		} else {
			notaFiscalServicoService.setDescricaoObservacaoDocumentoItensNota(nfs, documento, null);
		}
		
		documento.setAcaohistorico(Documentoacao.PREVISTA);
		documento.setObservacaoHistorico("Origem da nota <a href=\"javascript:visualizarNotaFiscalServico("+nfs.getCdNota()+")\">"+(nfs.getNumero() != null ? nfs.getNumero() : "sem n�mero")+"</a>.");
		Documentohistorico documentohistorico = new Documentohistorico(documento);
		
		List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
		listaHistorico.add(documentohistorico);
		
		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class, listaHistorico));
		
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		
		Taxa taxa = new Taxa();
		List<Taxaitem> listaTaxaitem = new ArrayList<Taxaitem>();
		
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		for (NotaContrato c : nfs.getListaNotaContrato()) {
			if(c.getContrato().getRateio() != null && c.getContrato().getRateio().getCdrateio() != null){
				listaContrato.add(c.getContrato());
				listaRateioitem.addAll(rateioitemService.findByRateio(c.getContrato().getRateio()));
			}
			
			contratoService.preencherTaxasContrato(listaTaxaitem, c.getContrato());
		}
		

		if(SinedUtil.isListNotEmpty(listaContrato) && SinedUtil.isListNotEmpty(listaRateioitem)){
			List<Contrato> lista = contratoService.findForCobranca(CollectionsUtil.listAndConcatenate(listaContrato, "cdcontrato", ","));
			if(SinedUtil.isListNotEmpty(lista)){
				Money valorTotalContrato = new Money();
				for(Contrato it : lista){
					valorTotalContrato = valorTotalContrato.add(it.getValorContrato(it.getDtproximovencimento())); 
				}
				rateioitemService.agruparItensRateio(listaRateioitem);
				rateioService.ajustaPercentualRateioitem(valorTotalContrato, listaRateioitem);
				rateioService.calculaValorRateioitem(documento.getValor(), listaRateioitem);
				rateioService.ajustaDiferencaRateio(documento.getValor(), listaRateioitem);
			}
		}
		
		rateioitemService.agruparItensRateio(listaRateioitem);
		
		String msg;
		String msg1 = "";
		String msg2 = "";
		for (Taxaitem taxaitem : listaTaxaitem) {
			taxaitem.setDtlimite(documento.getDtvencimento());
			
			if(taxaitem.getDias() != null && documento.getDtvencimento() != null){
				if(taxaitem.getTipocalculodias() != null && taxaitem.getTipocalculodias().equals(Tipocalculodias.ANTES_VENCIMENTO)){
					taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), -taxaitem.getDias()));
				}else {
					taxaitem.setDtlimite(SinedDateUtils.addDiasData(documento.getDtvencimento(), taxaitem.getDias()));
				}
			}
			
			msg = taxaitem.getTipotaxa().getNome() + " de " + (taxaitem.isPercentual() ? "" : "R$") + taxaitem.getValor().toString() + (taxaitem.isPercentual() ? "%" : "") + ateApos(taxaitem.getTipotaxa().getCdtipotaxa()) + SinedDateUtils.toString(taxaitem.getDtlimite()) + ". ";
			if((msg1+msg).length() <= 80){
				msg1 += msg;
			} else if((msg2+msg).length() <= 80){
				msg2 += msg;
			}
		}
		
		documento.setMensagem2(msg1);
		documento.setMensagem3(msg2);
		rateio.getListaRateioitem().addAll(listaRateioitem);
		rateioService.atualizaValorRateio(rateio, documento.getValor());
		rateioService.rateioItensPercentual(rateio, documento.getValor());
		rateioService.limpaReferenciaRateio(rateio);
		
		taxa.setListaTaxaitem(new ListSet<Taxaitem>(Taxaitem.class, listaTaxaitem));
		
		//Se a taxa de boleto em conta for diferente de zero ele cria um item em taxa para esse valor.
		if (documento.getConta() != null){
			Money taxaBoleto = contacorrenteService.getValorTaxaBoleto(documento.getConta()); 
			if (taxaBoleto != null && taxaBoleto.compareTo(new Money()) != 0){
				Tipotaxa tipotaxa = tipotaxaService.getTipoTaxa(Tipotaxa.TAXABOLETO);
				Taxaitem taxaitem = new Taxaitem();
				taxaitem.setTipotaxa(tipotaxa);
				taxaitem.setValor(taxaBoleto);
				taxaitem.setMotivo("Taxa de boleto");
				taxaitem.setDtlimite(contareceberService.getAdicionarAnoData(documento.getDtvencimento()));
				taxa.getListaTaxaitem().add(taxaitem);
//				documento.setValor(documento.getValor() != null ? documento.getValor().add(taxaBoleto) : taxaBoleto);
//				if(tipotaxa.getCentrocusto() != null && tipotaxa.getContacredito() != null){
//					Rateioitem rateioitem = new Rateioitem();
//					rateioitem.setValor(taxaBoleto);
//					rateioitem.setCentrocusto(tipotaxa.getCentrocusto());
//					rateioitem.setContagerencial(tipotaxa.getContacredito());
//					rateio.getListaRateioitem().add(rateioitem);
//				}
			}
		}
		documento.setRateio(rateio);
		documento.setTaxa(taxa);
		
		historicooperacaoService.addDescricaoDocumentoByHistoricooperacao(documento, null, nfs, "1/1");
		documentoService.addTaxaMovimentoTipoDocumento(documento);
		
		return documento;
	}
	
	private String ateApos(Integer cdtipotaxa){
		if(cdtipotaxa == 3 || cdtipotaxa == 4){
			return " at� ";
		}
		return " ap�s ";
	}
}
