package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocfop;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitementregamaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EntregamaterialService;
import br.com.linkcom.sined.geral.service.GrupotributacaoService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MemorandoexportacaoService;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaocfopService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitementregamaterialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.Aux_imposto;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path={"/faturamento/process/Notafiscalprodutoitem"}, authorizationModule=ProcessAuthorizationModule.class)
public class NotafiscalprodutoitemProcess extends MultiActionController {
	
	private CfopService cfopService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private MaterialService materialService;
	private EnderecoService enderecoService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService;
	private EmpresaService empresaService;
	private UnidademedidaService unidademedidaService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private NaturezaoperacaocfopService naturezaoperacaocfopService;
	private LoteestoqueService loteestoqueService;
	private ParametrogeralService parametrogeralService;
	private PatrimonioitemService patrimonioitemService;
	private MovpatrimonioService movpatrimonioService;
	private GrupotributacaoService grupotributacaoService;
	private ClienteService clienteService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private EntregamaterialService entregamaterialService;
	private NotafiscalprodutoitementregamaterialService notafiscalprodutoitementregamaterialService;
	private MemorandoexportacaoService memorandoexportacaoService;
	private MunicipioService municipioService;
	private LocalarmazenagemService localarmazenagemService;
	
	public void setNaturezaoperacaoService(
			NaturezaoperacaoService naturezaoperacaoService) {
		this.naturezaoperacaoService = naturezaoperacaoService;
	}
	
	public void setMovpatrimonioService(
			MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}
	public void setPatrimonioitemService(
			PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setUnidademedidaconversaoService(
			UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setUnidademedidaService(
			UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setCfopService(CfopService cfopService) {
		this.cfopService = cfopService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}	
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {
		this.notafiscalprodutoitemService = notafiscalprodutoitemService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNaturezaoperacaocfopService(NaturezaoperacaocfopService naturezaoperacaocfopService) {
		this.naturezaoperacaocfopService = naturezaoperacaocfopService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {
		this.grupotributacaoService = grupotributacaoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {
		this.entregamaterialService = entregamaterialService;
	}
	public void setNotafiscalprodutoitementregamaterialService(
			NotafiscalprodutoitementregamaterialService notafiscalprodutoitementregamaterialService) {
		this.notafiscalprodutoitementregamaterialService = notafiscalprodutoitementregamaterialService;
	}
	public void setMemorandoexportacaoService(
			MemorandoexportacaoService memorandoexportacaoService) {
		this.memorandoexportacaoService = memorandoexportacaoService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	
	/**
	 * Carrega a popup de inser��o de um produto.
	 * 
	 * @param request
	 * @return
	 * @author Marden Silva
	 */
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		request.setAttribute("consultaitemnotafiscalproduto", false);
		request.setAttribute("OBRIGAR_CEST", parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CEST));
		String cdempresa = request.getParameter("cdempresa");
		notafiscalprodutoitem.setCdNota(request.getParameter("cdNota"));
		notafiscalprodutoitem.setIncluirvalorprodutos(Boolean.TRUE);
		notafiscalprodutoitem.setIdentificadortela(request.getParameter("identificadortela"));
		
		this.setNfeParam(request, notafiscalprodutoitem);
		
		Boolean localArmazenagemObrigatorio = parametrogeralService.getBoolean(Parametrogeral.LOCAL_VENDA_OBRIGATORIO);
		Boolean exibirLocal = false;
		if(request.getParameter("exibirLocalarmazenagem") != null && "true".equalsIgnoreCase(request.getParameter("exibirLocalarmazenagem"))){
			exibirLocal = true;
			if(localArmazenagemObrigatorio && notafiscalprodutoitem.getLocalarmazenagem() == null && Util.strings.isNotEmpty(cdempresa)){
				Localarmazenagem localprincipal = localarmazenagemService.loadPrincipalByEmpresa(new Empresa(Integer.parseInt(cdempresa)));
				notafiscalprodutoitem.setLocalarmazenagem(localprincipal);
			}
		}
		request.setAttribute("LOCAL_VENDA_OBRIGATORIO", localArmazenagemObrigatorio);
		
		List<Loteestoque> listaLoteestoque = notafiscalprodutoitem.getMaterial() != null ? loteestoqueService.findLoteByMaterial(notafiscalprodutoitem.getMaterial()) : new ArrayList<Loteestoque>();
		listaLoteestoque = loteestoqueService.findComboVinculoMaterial(request, listaLoteestoque, notafiscalprodutoitem.getMaterial(), null, notafiscalprodutoitem.getLocalarmazenagem(), notafiscalprodutoitem.getUnidademedida());
		if(notafiscalprodutoitem.getLoteestoque() != null){
			notafiscalprodutoitem.setLoteestoque(loteestoqueService.carregaLoteestoque(notafiscalprodutoitem.getLoteestoque()));
			if(listaLoteestoque != null && !listaLoteestoque.contains(notafiscalprodutoitem.getLoteestoque())){
				listaLoteestoque.add(notafiscalprodutoitem.getLoteestoque());
			}
		}
		request.setAttribute("notadevolucao", notafiscalprodutoitem.getFinalidadenfe() != null && notafiscalprodutoitem.getFinalidadenfe().equals(Finalidadenfe.DEVOLUCAO));
		request.setAttribute("listaLoteestoque", listaLoteestoque);
		request.setAttribute("exibirLocal", exibirLocal);
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>(); 
		if(notafiscalprodutoitem.getMaterial() != null)
			listaUnidademedida = unidademedidaService.getUnidademedidaByMaterial(notafiscalprodutoitem.getMaterial());
		request.setAttribute("listaUnidademedida", listaUnidademedida);
		
		String notaComplementarParam = request.getParameter("notaComplementar");
		if(notaComplementarParam != null && 
				!notaComplementarParam.equals("") &&
				notaComplementarParam.toUpperCase().equals("TRUE")){
			request.setAttribute("notaComplementar", Boolean.TRUE);
		}
		
		request.setAttribute("PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO", parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO));
		
		Integer cdnaturezaoperacao = null;
		
		if (notafiscalprodutoitem.getNaturezaoperacao()!=null && notafiscalprodutoitem.getNaturezaoperacao().getCdnaturezaoperacao()!=null){
			
			cdnaturezaoperacao = notafiscalprodutoitem.getNaturezaoperacao().getCdnaturezaoperacao();
			if(cdnaturezaoperacao != null){
				Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(notafiscalprodutoitem.getNaturezaoperacao(), 
						"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.simplesremessa");
				if(naturezaoperacao != null){
					notafiscalprodutoitem.getNaturezaoperacao().setSimplesremessa(naturezaoperacao.getSimplesremessa());
				}
			}
			Empresa empresa = notafiscalprodutoitem.getEmpresa();
			Endereco enderecoempresa = null;
			Endereco enderecocliente = notafiscalprodutoitem.getEndereco();
			Uf ufEmpresa = null;
			Uf ufCliente = null;
			
			if (empresa!=null){
				enderecoempresa = enderecoService.carregaEnderecoEmpresa(empresa);
			}else {
				empresa = empresaService.loadPrincipalWithEndereco();
				if (empresa!=null && empresa.getEndereco()!=null){
					enderecoempresa = empresa.getEndereco();
				}
			}
			
			if (enderecoempresa!=null && enderecoempresa.getCdendereco()!=null){
				enderecoempresa = enderecoService.carregaEnderecoComUfPais(enderecoempresa);
				if (enderecoempresa.getMunicipio()!=null){
					ufEmpresa = enderecoempresa.getMunicipio().getUf();
				}
			}
			
			if (enderecocliente!=null){
				enderecocliente = enderecoService.carregaEnderecoComUfPais(enderecocliente);
				if (enderecocliente.getMunicipio()!=null){
					ufCliente = enderecocliente.getMunicipio().getUf();
				}
			}
			
			Cfopescopo cfopescopo = null;
			if (ufEmpresa!=null && ufCliente!=null){
				if (ufEmpresa.getCduf().equals(ufCliente.getCduf())){
					cfopescopo = Cfopescopo.ESTADUAL;
				}else {
					cfopescopo = Cfopescopo.FORA_DO_ESTADO;
				}
			}
			if(notafiscalprodutoitem.getCfop() == null){
				List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(notafiscalprodutoitem.getNaturezaoperacao(), cfopescopo);
				if (!listaNaturezaoperacaocfop.isEmpty()){
					notafiscalprodutoitem.setCfop(listaNaturezaoperacaocfop.get(0).getCfop());
				}
			}
		}
		
		request.setAttribute("cdnaturezaoperacao", cdnaturezaoperacao);
		request.setAttribute("localvendaobrigatorio", parametrogeralService.getValorPorNome(Parametrogeral.LOCAL_VENDA_OBRIGATORIO));
		
		Boolean obrigapedido = false; 
		if(notafiscalprodutoitem.getNaturezaoperacao() != null && notafiscalprodutoitem.getNaturezaoperacao().getCdnaturezaoperacao() != null){
			Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(notafiscalprodutoitem.getNaturezaoperacao());
			if(naturezaoperacao != null && naturezaoperacao.getObrigarNumeroPedido() != null){
				obrigapedido = naturezaoperacao.getObrigarNumeroPedido();
			}
		}
		request.setAttribute("obrigapedido", obrigapedido);
		
//		this.adicionaAtributoInfoCfop(request, notafiscalprodutoitem);
		return new ModelAndView("direct:process/popup/criaNotafiscalprodutoitem", "notafiscalprodutoitem", notafiscalprodutoitem);
	}
	
	/**
	 * Exclui o material da lista no escopo de sess�o.
	 * 
	 * @param request
	 * @author Marden Silva
	 */
	@SuppressWarnings("unchecked")
	@Action("excluir")
	public void excluir(WebRequestContext request){
		try {
			List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = null;
			Object object = request.getParameter("posicaoLista");
			Integer cdmaterial = null;
			if (object != null) {
				String parameter = object.toString();
				Integer index = Integer.parseInt(parameter)-1;
				Object attr = request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
				if (attr != null) {
					listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) attr;
					
					if(listaNotafiscalprodutoitem.get(index.intValue()) != null && 
							listaNotafiscalprodutoitem.get(index.intValue()).getMaterial() != null){
						cdmaterial = listaNotafiscalprodutoitem.get(index.intValue()).getMaterial().getCdmaterial();
					}
					listaNotafiscalprodutoitem.remove(index.intValue());
				}
			}
			request.getSession().setAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"), listaNotafiscalprodutoitem);
			View.getCurrent().println("var sucesso = true; var cdmaterial = '" + (cdmaterial != null ? cdmaterial : "") + "'; ");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false; var cdmaterial = ''; ");
		}
	}	
	
	/**
	 * Salva o item de intera��o no objeto interacao e logo depois joga o objeto para a sess�o.
	 * 
	 * @param request
	 * @param interacaoitem
	 * @author Marden Silva
	 */
	@SuppressWarnings("unchecked")
	@Command(validate=true)
	@Input("criar")
	@Action("salvainteracao")
	public void salvaInteracao(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){

		Object attribute = request.getSession().getAttribute("listaItensNotaFiscalProduto" + notafiscalprodutoitem.getIdentificadortela());
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = null;
		
		if(notafiscalprodutoitem.getCfop() != null){
			if(notafiscalprodutoitem.getCfop().getCodigo() == null || notafiscalprodutoitem.getCfop().getDescricaoresumida() == null){
				notafiscalprodutoitem.setCfop(cfopService.load(notafiscalprodutoitem.getCfop()));
			}
		}
		
		request.getSession().setAttribute("notafiscalprodutoitem" + notafiscalprodutoitem.getIdentificadortela(), notafiscalprodutoitem);
		
		if (attribute == null) {
			listaNotafiscalprodutoitem = new ArrayList<Notafiscalprodutoitem>();
			listaNotafiscalprodutoitem.add(notafiscalprodutoitem);	
//			this.verificarInfAproveitamentocredito(request, listaNotafiscalprodutoitem);
			this.informacoesMaterial(request, notafiscalprodutoitem);
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>" +
										"parent.montaListaItens('"+escapeSingleQuotes(notafiscalprodutoitem.getNomenfdescricaonota())+
										"','"+(notafiscalprodutoitem.getCfop() == null ? "" : notafiscalprodutoitem.getCfop().getCodigo())+
										"','"+(notafiscalprodutoitem.getValoricms() == null ? "0,00" : notafiscalprodutoitem.getValoricms())+
										"','"+(notafiscalprodutoitem.getValoriss() == null ? "0,00" : notafiscalprodutoitem.getValoriss())+
										"','"+(notafiscalprodutoitem.getValoripi() == null ? "0,00" : notafiscalprodutoitem.getValoripi())+
										"','"+(notafiscalprodutoitem.getValorpis() == null ? "0,00" : notafiscalprodutoitem.getValorpis())+
										"','"+(notafiscalprodutoitem.getValorcofins() == null ? "0,00" : notafiscalprodutoitem.getValorcofins())+
										"','"+(notafiscalprodutoitem.getValorfrete() == null ? "0,00" : notafiscalprodutoitem.getValorfrete())+
										"','"+(notafiscalprodutoitem.getValordesconto() == null ? "0,00" : notafiscalprodutoitem.getValordesconto())+
										"','"+(notafiscalprodutoitem.getValorseguro() == null ? "0,00" : notafiscalprodutoitem.getValorseguro())+
										"','"+(notafiscalprodutoitem.getQtde() == null ? "0,00" : SinedUtil.descriptionDecimal(notafiscalprodutoitem.getQtde()))+
										"','"+(notafiscalprodutoitem.getUnidademedida() == null ? "" : escapeSingleQuotes(unidademedidaService.load(notafiscalprodutoitem.getUnidademedida()).getNome()))+
										"','"+(notafiscalprodutoitem.getValorunitario() == null ? "0,00" : notafiscalprodutoitem.getValorunitario())+										
										"','"+(notafiscalprodutoitem.getValorbruto() == null ? "0,00" : notafiscalprodutoitem.getValorbruto())+
										"');" +
										" parent.$.akModalRemove();" +
										"</script>"); 
		} else {
			listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>)attribute;					
			
			if (notafiscalprodutoitem.getIndexlista() != null) {
				notafiscalprodutoitem.setInformacaoAdicionalProdutoBean(listaNotafiscalprodutoitem.get(notafiscalprodutoitem.getIndexlista()).getInformacaoAdicionalProdutoBean());
				listaNotafiscalprodutoitem.set(notafiscalprodutoitem.getIndexlista(), notafiscalprodutoitem);
//				this.verificarInfAproveitamentocredito(request, listaNotafiscalprodutoitem);
				this.informacoesMaterial(request, notafiscalprodutoitem);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"parent.editaItem('"+notafiscalprodutoitem.getIndexlista()+
											"','"+escapeSingleQuotes(notafiscalprodutoitem.getNomenfdescricaonota())+
											"','"+(notafiscalprodutoitem.getCfop() == null ? "" : notafiscalprodutoitem.getCfop().getCodigo())+
											"','"+(notafiscalprodutoitem.getValoricms() == null ? "0,00" : notafiscalprodutoitem.getValoricms())+
											"','"+(notafiscalprodutoitem.getValoriss() == null ? "0,00" : notafiscalprodutoitem.getValoriss())+
											"','"+(notafiscalprodutoitem.getValoripi() == null ? "0,00" : notafiscalprodutoitem.getValoripi())+
											"','"+(notafiscalprodutoitem.getValorpis() == null ? "0,00" : notafiscalprodutoitem.getValorpis())+
											"','"+(notafiscalprodutoitem.getValorcofins() == null ? "0,00" : notafiscalprodutoitem.getValorcofins())+
											"','"+(notafiscalprodutoitem.getValorfrete() == null ? "0,00" : notafiscalprodutoitem.getValorfrete())+
											"','"+(notafiscalprodutoitem.getValordesconto() == null ? "0,00" : notafiscalprodutoitem.getValordesconto())+
											"','"+(notafiscalprodutoitem.getValorseguro() == null ? "0,00" : notafiscalprodutoitem.getValorseguro())+
											"','"+(notafiscalprodutoitem.getQtde() == null ? "0,00" : SinedUtil.descriptionDecimal(notafiscalprodutoitem.getQtde()))+
											"','"+(notafiscalprodutoitem.getUnidademedida() == null ? "" : escapeSingleQuotes(unidademedidaService.load(notafiscalprodutoitem.getUnidademedida()).getNome()))+
											"','"+(notafiscalprodutoitem.getValorunitario() == null ? "0,00" : SinedUtil.descriptionDecimal(notafiscalprodutoitem.getValorunitario(), true))+											
											"','"+(notafiscalprodutoitem.getValorbruto() == null ? "0,00" : notafiscalprodutoitem.getValorbruto())+										
											"');" +
											" parent.$.akModalRemove();" +
											"</script>");
			} else {
				listaNotafiscalprodutoitem.add(notafiscalprodutoitem);
//				this.verificarInfAproveitamentocredito(request, listaNotafiscalprodutoitem);
				this.informacoesMaterial(request, notafiscalprodutoitem);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"parent.montaListaItens('"+escapeSingleQuotes(notafiscalprodutoitem.getNomenfdescricaonota())+
											"','"+(notafiscalprodutoitem.getCfop() == null ? "" : notafiscalprodutoitem.getCfop().getCodigo())+
											"','"+(notafiscalprodutoitem.getValoricms() == null ? "0,00" : notafiscalprodutoitem.getValoricms())+
											"','"+(notafiscalprodutoitem.getValoriss() == null ? "0,00" : notafiscalprodutoitem.getValoriss())+
											"','"+(notafiscalprodutoitem.getValoripi() == null ? "0,00" : notafiscalprodutoitem.getValoripi())+
											"','"+(notafiscalprodutoitem.getValorpis() == null ? "0,00" : notafiscalprodutoitem.getValorpis())+
											"','"+(notafiscalprodutoitem.getValorcofins() == null ? "0,00" : notafiscalprodutoitem.getValorcofins())+
											"','"+(notafiscalprodutoitem.getValorfrete() == null ? "0,00" : notafiscalprodutoitem.getValorfrete())+
											"','"+(notafiscalprodutoitem.getValordesconto() == null ? "0,00" : notafiscalprodutoitem.getValordesconto())+
											"','"+(notafiscalprodutoitem.getValorseguro() == null ? "0,00" : notafiscalprodutoitem.getValorseguro())+
											"','"+(notafiscalprodutoitem.getQtde() == null ? "0,00" : SinedUtil.descriptionDecimal(notafiscalprodutoitem.getQtde()))+
											"','"+(notafiscalprodutoitem.getUnidademedida() == null ? "" : escapeSingleQuotes(unidademedidaService.load(notafiscalprodutoitem.getUnidademedida()).getNome()))+
											"','"+(notafiscalprodutoitem.getValorunitario() == null ? "0,00" : SinedUtil.descriptionDecimal(notafiscalprodutoitem.getValorunitario(), true))+											
											"','"+(notafiscalprodutoitem.getValorbruto() == null ? "0,00" : notafiscalprodutoitem.getValorbruto())+	
											"');" +
											" parent.$.akModalRemove();" +
											"</script>");
			} 
		}	

		request.getSession().setAttribute("listaItensNotaFiscalProduto" + notafiscalprodutoitem.getIdentificadortela(), listaNotafiscalprodutoitem);
	}
	
	/**
	 * M�todo busca os dados da tributa��o ap�s o usu�rio alterar o cfop
	 *
	 * @param request
	 * @param item
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	@Command(validate=false)
	@Action("alteracfopinteracao")
	public ModelAndView alteraCfopInteracao(WebRequestContext request, Notafiscalprodutoitem item){
		Boolean criar = item.getIndexlista() == null;
		
//		String cdNota = notafiscalprodutoitem.getCdNota() != null ? notafiscalprodutoitem.getCdNota() : "";
		String cdClienteStr = request.getParameter("cdCliente");
		String cdEnderecoorigemStr = request.getParameter("cdEnderecocliente");
		String cdEmpresaStr = request.getParameter("cdEmpresa");
		String cdendereconotaStr = request.getParameter("cdendereconota");
		String cdmunicipionotaStr = request.getParameter("municipionota");
		String cdufnotaStr = request.getParameter("ufnota");
		Boolean enderecoavulso = Boolean.valueOf(request.getParameter("enderecoavulso") != null && !"".equals(request.getParameter("enderecoavulso")) ?
				request.getParameter("enderecoavulso") : "false");
		
		Cliente cliente = null;
		Empresa empresa = null;
		Endereco enderecocliente = null;
		Endereco enderecoempresa = null;
		
		if(enderecoavulso != null && enderecoavulso && cdmunicipionotaStr != null && !"".equals(cdmunicipionotaStr)){
			Endereco endereconota = new Endereco();
			endereconota.setMunicipio(new Municipio(Integer.parseInt(cdmunicipionotaStr)));
			if(cdendereconotaStr != null && !"".equals(cdendereconotaStr)){
				endereconota.setCdendereco(Integer.parseInt(cdendereconotaStr));
			}
			if(endereconota.getMunicipio() != null && cdufnotaStr != null && !"".equals(cdufnotaStr)){
				endereconota.getMunicipio().setUf(new Uf(Integer.parseInt(cdufnotaStr)));
			}
			
			enderecocliente = endereconota;
		}
		
		if(cdClienteStr != null && !"".equals(cdClienteStr)){
			cliente = clienteService.loadForTributacao(new Cliente(Integer.parseInt(cdClienteStr)));
		}	
		if(enderecocliente == null && cdEnderecoorigemStr != null && !"".equals(cdEnderecoorigemStr)){
			enderecocliente = enderecoService.carregaEnderecoComUfPais(new Endereco(Integer.parseInt(cdEnderecoorigemStr)));
		}		
		if(cdEmpresaStr != null && !"".equals(cdEmpresaStr)){
			empresa = new Empresa(Integer.parseInt(cdEmpresaStr));
			enderecoempresa = enderecoService.carregaEnderecoEmpresa(empresa);
		}else {
			empresa = empresaService.loadPrincipalWithEndereco();
			if(empresa != null && empresa.getEndereco() != null){
				enderecoempresa = empresa.getEndereco();
			}
		}
		if(enderecoempresa != null && enderecoempresa.getCdendereco() != null){
			enderecoempresa = enderecoService.carregaEnderecoComUfPais(enderecoempresa);
		}
		
		Cfop cfopitem = item.getCfop();
		if(enderecocliente != null){
			item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, empresa, cliente, enderecoempresa, enderecocliente, cfopitem);
		}

		boolean naoTemIcms = (item.getTributadoicms() != null && !item.getTributadoicms()) ||
		(item.getTipocobrancaicms() != null &&
		(
		Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
		Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
		));

		boolean naoTemIcmsSt =  (item.getTributadoicms() != null && !item.getTributadoicms()) ||
				(item.getTipocobrancaicms() != null && 
				(
				Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.DIFERIMENTO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
				));
		
		if(naoTemIcms){ 
			item.setValorbcicms(new Money());
			item.setIcms(0d);
			item.setValoricms(new Money());
		}
		
		if(naoTemIcmsSt){
			item.setValorbcicmsst(new Money());
			item.setIcmsst(0d);
			item.setValoricmsst(new Money());
		}
		
		if(cfopitem != null){
			item.setCfop(cfopService.carregarCfop(cfopitem));
		}
		
		if(item.getGrupotributacaotrans() != null){
			request.setAttribute("verificarinfocfop", "true");
		}
		
		if(criar){
			return this.criar(request, item);
		}else {
			Object attribute = request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
			List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = null;
			listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>)attribute;
			listaNotafiscalprodutoitem.set(item.getIndexlista(), item);

			request.getSession().setAttribute("listaItensNotaFiscalProduto" + item.getCdNota(), listaNotafiscalprodutoitem);
			return editar(request);
		}
	}
	
//	private void verificarInfAproveitamentocredito(WebRequestContext request, List<Notafiscalprodutoitem> listaNotafiscalprodutoitem){
//		String infadicionais = notafiscalprodutoService.getMsgitemAproveitamentocredito(listaNotafiscalprodutoitem);
//		if(infadicionais != null && !"".equals(infadicionais)){
//			request.getServletResponse().setContentType("text/html");
//			View.getCurrent().println("<script>parent.adicionaInfAproveitamentocredito(\""+ infadicionais + "\");</script>");
//		}
//	}
	
	private String escapeSingleQuotes(String opValue) {
		if(opValue==null) return null;
		return opValue.replaceAll("\\\\", "\\\\\\\\").replaceAll("\'", "\\\\'");
	}
	
	/**
	 * Edita a intera��o da lista na entrada de prospec��o; 
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#carregaIndexLista
	 * @param request
	 * @return
	 * @author Marden Silva
	 */
	@SuppressWarnings("unchecked")
	@Action("editar")
	public ModelAndView editar(WebRequestContext request){
		request.setAttribute("consultaitemnotafiscalproduto", false);
		request.setAttribute("OBRIGAR_CEST", parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CEST));
		String cdempresa = request.getParameter("cdempresa");
		Notafiscalprodutoitem notafiscalprodutoitem = null;
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			String parameter = object.toString();
			parameter = parameter.substring(parameter.indexOf("indice=")+7, parameter.length()-1);
			Integer index = Integer.parseInt(parameter)-1;
			Object attr = request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
			List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = null;
			if (attr != null) {
				listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) attr;
				notafiscalprodutoitem = listaNotafiscalprodutoitem.get(index);
				notafiscalprodutoitem.setIndexlista(index);
				notafiscalprodutoitem.setCdNota(request.getParameter("cdNota"));
				notafiscalprodutoitem.setIdentificadortela(request.getParameter("identificadortela"));
			}
		}	
		Boolean localArmazenagemObrigatorio = parametrogeralService.getBoolean(Parametrogeral.LOCAL_VENDA_OBRIGATORIO);
		Boolean exibirLocal = false;
		if(request.getParameter("exibirLocalarmazenagem") != null && "true".equalsIgnoreCase(request.getParameter("exibirLocalarmazenagem"))){
			exibirLocal = true;
			if(localArmazenagemObrigatorio && notafiscalprodutoitem.getLocalarmazenagem() == null && Util.strings.isNotEmpty(cdempresa)){
				Localarmazenagem localprincipal = localarmazenagemService.loadPrincipalByEmpresa(new Empresa(Integer.parseInt(cdempresa)));
				notafiscalprodutoitem.setLocalarmazenagem(localprincipal);
			}
		}
		request.setAttribute("exibirLocal", exibirLocal);
		request.setAttribute("LOCAL_VENDA_OBRIGATORIO", localArmazenagemObrigatorio);	
		
		request.setAttribute("PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO", parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO));
		
		this.setNfeParam(request, notafiscalprodutoitem);
		
		List<Loteestoque> listaLoteestoque = notafiscalprodutoitem.getMaterial() != null ? loteestoqueService.findLoteByMaterial(notafiscalprodutoitem.getMaterial()) : new ArrayList<Loteestoque>();
		listaLoteestoque = loteestoqueService.findComboVinculoMaterial(request, listaLoteestoque, notafiscalprodutoitem.getMaterial(), null, notafiscalprodutoitem.getLocalarmazenagem(), notafiscalprodutoitem.getUnidademedida());
		if(notafiscalprodutoitem.getLoteestoque() != null){
			notafiscalprodutoitem.setLoteestoque(loteestoqueService.carregaLoteestoque(notafiscalprodutoitem.getLoteestoque()));
			if(listaLoteestoque != null && !listaLoteestoque.contains(notafiscalprodutoitem.getLoteestoque())){
				listaLoteestoque.add(notafiscalprodutoitem.getLoteestoque());
			}
		}
		request.setAttribute("listaLoteestoque", listaLoteestoque);

		String notaComplementarParam = request.getParameter("notaComplementar");
		if(notaComplementarParam != null && 
				!notaComplementarParam.equals("") &&
				notaComplementarParam.toUpperCase().equals("TRUE")){
			request.setAttribute("notaComplementar", Boolean.TRUE);
		}
		
		Boolean notadevolucao = Boolean.TRUE;
		String finalidadenfe = request.getParameter("finalidadenfe");
		if(finalidadenfe != null){
			try {
				Finalidadenfe valorfinalidadenfe = Finalidadenfe.valueOf(finalidadenfe);
				notadevolucao = valorfinalidadenfe != null && valorfinalidadenfe.equals(Finalidadenfe.DEVOLUCAO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("notadevolucao", notadevolucao);
		
		if(request.getParameter("cdnaturezaoperacao") != null && !"".equals(request.getParameter("cdnaturezaoperacao"))){
			try {
				Integer cdnaturezaoperacao = Integer.parseInt(request.getParameter("cdnaturezaoperacao"));
				Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(new Naturezaoperacao(cdnaturezaoperacao), 
				"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.simplesremessa");
				if(naturezaoperacao != null){
					if(notafiscalprodutoitem.getNaturezaoperacao() == null){
						notafiscalprodutoitem.setNaturezaoperacao(naturezaoperacao);
					}else {
						notafiscalprodutoitem.getNaturezaoperacao().setSimplesremessa(naturezaoperacao.getSimplesremessa());
					}
				}
			} catch (Exception e) {}
		}
		
		List<Unidademedida> listaUnidademedida = unidademedidaService.getUnidademedidaByMaterial(notafiscalprodutoitem.getMaterial());
		request.setAttribute("listaUnidademedida", listaUnidademedida);
		request.setAttribute("cdnaturezaoperacao", request.getParameter("cdnaturezaoperacao"));
		request.setAttribute("localvendaobrigatorio", parametrogeralService.getValorPorNome(Parametrogeral.LOCAL_VENDA_OBRIGATORIO));
		
		if(notafiscalprodutoitem.getMaterial() != null){
			request.setAttribute("obrigarLote", materialService.obrigarLote(notafiscalprodutoitem.getMaterial()));
		}
		
//		this.adicionaAtributoInfoCfop(request, notafiscalprodutoitem);
		return new ModelAndView("direct:process/popup/criaNotafiscalprodutoitem","notafiscalprodutoitem", notafiscalprodutoitem);
	}

	private void setNfeParam(WebRequestContext request,
			Notafiscalprodutoitem notafiscalprodutoitem) {
		String nfeParam = request.getParameter("nfe");
		Boolean nfe = StringUtils.isNotBlank(nfeParam) ? Boolean.valueOf(nfeParam) : Boolean.TRUE;
		notafiscalprodutoitem.setNfe(nfe);
		request.setAttribute("nfe", nfe);
	}
	
	/**
	 * M�todo para consultar o item sem ter que editar
	 * o atributo consultaitemnotafiscalproduto � utilizado para setar os campos como disable
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	@Action("consultar")
	public ModelAndView consultar(WebRequestContext request){
		
		Notafiscalprodutoitem notafiscalprodutoitem = null;
		Object object = request.getParameter("index");
		if (object != null && !"".equals(object)) {
			String parameter = object.toString();			
			Integer index = Integer.parseInt(parameter);
			Object attr = request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
			List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = null;
			if (attr != null) {
				listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) attr;
				notafiscalprodutoitem = listaNotafiscalprodutoitem.get(index);
				notafiscalprodutoitem.setIndexlista(index);
				notafiscalprodutoitem.setCdNota(request.getParameter("cdNota"));
				notafiscalprodutoitem.setIdentificadortela(request.getParameter("identificadortela"));
			}
		}	
		
		Boolean exibirLocal = false;
		if(request.getParameter("exibirLocalarmazenagem") != null && "true".equalsIgnoreCase(request.getParameter("exibirLocalarmazenagem"))){
			exibirLocal = true;
		}
		request.setAttribute("exibirLocal", exibirLocal);
		request.setAttribute("consultaitemnotafiscalproduto", true);
		
		Boolean notadevolucao = Boolean.TRUE;
		String finalidadenfe = request.getParameter("finalidadenfe");
		if(finalidadenfe != null){
			try {
				Finalidadenfe valorfinalidadenfe = Finalidadenfe.valueOf(finalidadenfe);
				notadevolucao = valorfinalidadenfe != null && valorfinalidadenfe.equals(Finalidadenfe.DEVOLUCAO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		request.setAttribute("notadevolucao", notadevolucao);
		
		request.setAttribute("PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO", parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO));
		this.setNfeParam(request, notafiscalprodutoitem);
		return new ModelAndView("direct:process/popup/criaNotafiscalprodutoitem","notafiscalprodutoitem", notafiscalprodutoitem);
	}
	
	
	/**
	* M�todo que calcula o valor total da nota
	*
	* @param request
	* @since 17/06/2016
	* @author Luiz Fernando
	*/
	public void ajaxCalculaValorTotal(WebRequestContext request){
		notafiscalprodutoService.ajaxCalculaValorTotal(request);
	}
	
	/**
	 * M�todo ajax que busca os valores de bcicmst e valoricmsst utilizado anteriormente nas entregas
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxBuscaValoresCobradosAnteriormente(WebRequestContext request, Material material){
		JsonModelAndView json = new JsonModelAndView();	
		
		String cdentregadocumentostr = request.getParameter("cdentregadocumento");
		String cdcfop = request.getParameter("cdcfop");
		String cdempresa = request.getParameter("cdempresa");
		String valuenaturezaoperacao = request.getParameter("naturezaoperacao");
		String cdgrupotributacao = request.getParameter("cdgrupotributacao");	
		Operacaonfe operacaonfe = null; 
				
		if("CONSUMIDOR_FINAL".equals(request.getParameter("opercaonfe"))){
			operacaonfe = Operacaonfe.CONSUMIDOR_FINAL;
		}else if("NORMAL".equals(request.getParameter("opercaonfe"))){
			operacaonfe = Operacaonfe.NORMAL;
		}
		
//		Cfop cfop = null;
//		if (material.getMaterialgrupo() != null){		
//			Materialgrupo materialgrupo = materialgrupoService.loadTributacao(material.getMaterialgrupo(), empresa);
//			if(materialgrupo != null){
//				Materialgrupotributacao materialgrupotributacao = materialgrupo.getMaterialgrupotributacaoByEmpresa(empresa, cfopitem);
		Integer cdentregadocumento = null;
		if(cdentregadocumentostr != null && !"".equals(cdentregadocumentostr)){
			cdentregadocumento = Integer.parseInt(cdentregadocumentostr);
		}
		
		boolean calcularbcicmsretidoanteriormente = false;
		if(StringUtils.isNotEmpty(cdgrupotributacao)){
			Grupotributacao grupotributacao = grupotributacaoService.load(new Grupotributacao(Integer.parseInt(cdgrupotributacao)), "grupotributacao.cdgrupotributacao, grupotributacao.calcularbcicmsretidoanteriormente");
			calcularbcicmsretidoanteriormente = grupotributacao != null && grupotributacao.getCalcularbcicmsretidoanteriormente() != null && grupotributacao.getCalcularbcicmsretidoanteriormente(); 
		}
		
		Money valorbcicmsst = new Money(0.0);
		Money valoricmsst = new Money(0.0);	
		Double qtdenfp = material.getQtdenfp() != null ? material.getQtdenfp() : 1.0;
		if(material != null && material.getCdmaterial() != null && calcularbcicmsretidoanteriormente){
			material = materialService.loadForNFProduto(material);
			Grupotributacao grupotributacao = null;
			if (material.getMaterialgrupo() != null && cdcfop != null && !cdcfop.trim().equals("")){
				try{
					Empresa empresa = null;
					if(cdempresa != null && !cdempresa.trim().equals("") ){
						empresa = new Empresa(Integer.parseInt(cdempresa));
					}
					Naturezaoperacao naturezaoperacao = null;
					if(valuenaturezaoperacao != null && !"".equals(valuenaturezaoperacao)){
						naturezaoperacao = new Naturezaoperacao(Integer.parseInt(valuenaturezaoperacao));
					}
					Cfop cfop = new Cfop();
					cfop.setCdcfop(Integer.parseInt(cdcfop));
//					Materialgrupo materialgrupo = MaterialgrupoService.getInstance().loadTributacao(material.getMaterialgrupo(), empresa);
//					if(materialgrupo != null){
//						materialgrupotributacao = materialgrupo.getMaterialgrupotributacaoByEmpresa(empresa, cfop);
//					}
					Boolean considerarTributadoicms = Boolean.TRUE;
					if(material != null && material.getTributacaoestadual() != null && material.getTributacaoestadual()){
						considerarTributadoicms = Boolean.FALSE;
					}
					grupotributacao = grupotributacaoService.getGrupotributacao(Operacao.SAIDA, true, empresa, material.getMaterialgrupo(), naturezaoperacao, material.getGrupotributacao(),considerarTributadoicms, material.getNcmcompleto(), material.getMaterialtipo(), null, null, operacaonfe);
					
					if(!calcularbcicmsretidoanteriormente){
						calcularbcicmsretidoanteriormente = grupotributacao != null && grupotributacao.getCalcularbcicmsretidoanteriormente() != null && grupotributacao.getCalcularbcicmsretidoanteriormente();
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			Aux_imposto aux_imposto = notafiscalprodutoService.buscaValoresCobradosAnteriormenteIcmsst(material, qtdenfp, grupotributacao, cdentregadocumento);
			if(aux_imposto != null){
				if(aux_imposto.getValorbcicmsst() != null){
					valorbcicmsst = aux_imposto.getValorbcicmsst();
				}
				if(aux_imposto.getValoricmsst() != null){
					valoricmsst = aux_imposto.getValoricmsst();
				}
			}
		}
		
		json.addObject("valorbcicmsst", calcularbcicmsretidoanteriormente ? valorbcicmsst : new Money());
		json.addObject("valoricmsst", calcularbcicmsretidoanteriormente ? valoricmsst : new Money());
		
		return json;
	}
	
	/**
	 * Busca o peso do material para c�lculo.
	 *
	 * @param request
	 * @param notafiscalprodutoitem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/03/2013
	 */
	public ModelAndView ajaxBuscaPesosMaterial(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		JsonModelAndView json = new JsonModelAndView();
		
		Material aux_material = null;
		if(notafiscalprodutoitem.getMaterial() != null){
			aux_material = materialService.findListaMaterialunidademedida(notafiscalprodutoitem.getMaterial());
		}
		
		if(notafiscalprodutoitem.getUnidademedida() != null){
			Unidademedida aux_unidademedida = unidademedidaService.load(notafiscalprodutoitem.getUnidademedida());
			
			if(aux_unidademedida.getSimbolo() != null && aux_unidademedida.getSimbolo().trim().toUpperCase().equals("KG") && aux_material.getPeso() == null && aux_material.getPesobruto() == null){
				json.addObject("pesoLiquido", 1);
				json.addObject("pesoBruto", 1);
				return json;
			}
		}
		
		if(aux_material != null){
			Unidademedida aux_unidademedida = notafiscalprodutoitem.getUnidademedida();
			
			Double peso = aux_material.getPeso();
			Double pesobruto = aux_material.getPesobruto();
			
			if(aux_unidademedida != null && (peso != null || pesobruto != null)){
				Boolean unidadeprincipal = aux_unidademedida.equals(aux_material.getUnidademedida());
				
				if(!unidadeprincipal){
					Double fracao = notafiscalprodutoService.getFracaoConversaoUnidademedida(aux_material, aux_unidademedida);
					if(fracao != null && fracao > 0){
						if(peso != null){
							peso = peso/fracao;
						}
						if(pesobruto != null){
							pesobruto = pesobruto/fracao;
						}
					}
				}
			}
			
			if(peso != null){
				json.addObject("pesoLiquido", peso);
			} else {
				json.addObject("pesoLiquido", 0d);
			}
			
			if(pesobruto != null){
				json.addObject("pesoBruto", pesobruto);
			} else {
				json.addObject("pesoBruto", 0d);
			}
		}
		
		return json;
	}
	
	/**
	 * M�todo que adiciona informa��es da tributa��o ap�s alterar o cfop
	 *
	 * @param request
	 * @param notafiscalprodutoitem
	 * @author Luiz Fernando
	 */
	private void informacoesMaterial(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		if (notafiscalprodutoitem!=null && notafiscalprodutoitem.getMaterial() != null && 
				notafiscalprodutoitem.getMaterial().getCdmaterial() != null &&
				notafiscalprodutoitem.getGrupotributacao() != null && notafiscalprodutoitem.getGrupotributacao().getCdgrupotributacao()!=null){
			View.getCurrent().println("<script>parent.ajaxInformacoesContribuinteFiscoMaterial(" + 
					notafiscalprodutoitem.getMaterial().getCdmaterial() + ",'" +
					 notafiscalprodutoitem.getGrupotributacao().getCdgrupotributacao() + "');</script>");
		}
	}
	
	public ModelAndView converteUnidademedida(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		Double qtde = notafiscalprodutoitem.getQtde();
		Double valorunitario = notafiscalprodutoitem.getValorunitario();
		Unidademedida unidademedida = notafiscalprodutoitem.getUnidademedida();
		Unidademedida unidademedidaAntiga = notafiscalprodutoitem.getUnidademedidaAntiga();
		
		Material material = materialService.findListaMaterialunidademedida(notafiscalprodutoitem.getMaterial());
		
		Double fracao1 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedidaAntiga);
		Double fracao2 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedida);
		
		if(fracao1 != null && fracao2 != null){
			qtde = (qtde * fracao2) / fracao1;
			valorunitario = (valorunitario / fracao2) * fracao1;
		} else {
			Double fracao = null;
			
			List<Unidademedidaconversao> listaUnidademedidaconversao = unidademedidaconversaoService.conversoesByUnidademedida(unidademedida);		
			if(listaUnidademedidaconversao != null && listaUnidademedidaconversao.size() > 0){
				for (Unidademedidaconversao item : listaUnidademedidaconversao) {
					if(item.getUnidademedida() != null && 
						item.getUnidademedida().getCdunidademedida() != null && 
						item.getFracao() != null){
						
						if(item.getUnidademedidarelacionada().equals(unidademedida)){
							fracao = item.getFracaoQtdereferencia();
							break;
						}
					}
				}
			}
		
			if(fracao != null && fracao > 0){
				qtde = qtde / fracao;
				valorunitario = valorunitario * fracao;
			}
		}
		
		return new JsonModelAndView().addObject("qtde", SinedUtil.descriptionDecimal(qtde))
									 .addObject("valorunitario", SinedUtil.descriptionDecimal(valorunitario, true));
	}
	
	public ModelAndView buscarPatrimonioitemAjax(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(notafiscalprodutoitem != null && notafiscalprodutoitem.getMaterial() != null){
			List<Patrimonioitem> listaPatrimonioitem = patrimonioitemService.findByMaterial(notafiscalprodutoitem.getMaterial(), null);
			jsonModelAndView.addObject("listaPatrimonioitem", listaPatrimonioitem);
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView verificarOrigemPatrimonioitemAjax(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(notafiscalprodutoitem != null && notafiscalprodutoitem.getPatrimonioitem() != null){
			Movpatrimonio movpatrimonio = new Movpatrimonio();
			movpatrimonio.setPatrimonioitem(notafiscalprodutoitem.getPatrimonioitem());
			
			movpatrimonio = movpatrimonioService.findLastMov(movpatrimonio);
			
			if(movpatrimonio != null && 
					movpatrimonio.getLocalarmazenagem() != null && 
					movpatrimonio.getLocalarmazenagem().getCdlocalarmazenagem() != null){
				jsonModelAndView.addObject("localDescricao", movpatrimonio.getLocalarmazenagem().getNome());
				jsonModelAndView.addObject("localId", "br.com.linkcom.sined.geral.bean.Localarmazenagem[cdlocalarmazenagem=" + movpatrimonio.getLocalarmazenagem().getCdlocalarmazenagem() + "]");
			}
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * * M�todo criado para atualizar o Cfop caso 
	 * o mesmo seja alterado em notas que possua carta de 
	 * corre��o
	 * @param request
	 * @param notafiscalprodutoitem
	 */
	public void atualizaCfop(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		if(notafiscalprodutoitem.getCfop() != null){
			Integer cdnotafiscalprodutoitem = notafiscalprodutoitem.getCdnotafiscalprodutoitem();
			notafiscalprodutoitemService.atualizaCfop(cdnotafiscalprodutoitem,notafiscalprodutoitem.getCfop()); 
			SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota=" + notafiscalprodutoitem.getCdNota());
		}else{
			SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota=" + notafiscalprodutoitem.getCdNota());
		}
	}
	
	/**
	 * @param request
	 * @param notafiscalprodutoitem
	 * @return
	 * @author Andrey Leonardo
	 */
	public ModelAndView abrirPopupVincularProdutoNotaEntrada(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		notafiscalprodutoitem = notafiscalprodutoitemService.loadForPopupVincularNotaEntrada(notafiscalprodutoitem);
		return new ModelAndView("direct:process/popup/criaVinculoNotafiscalprodutoitemEntregamaterial", "notafiscalprodutoitem", notafiscalprodutoitem).addObject("cdNota", request.getParameter("cdNota"));
	}
	
	/**
	 * @param request
	 * @return
	 * @author Andrey Leonardo
	 */
	public ModelAndView buscarNotaEntrada(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		List<Entregamaterial> listaEntregamaterial = entregamaterialService.findForVincularNotaFiscalProdutoItem(notafiscalprodutoitem);
		
		for (Iterator<Entregamaterial> iterator = listaEntregamaterial.iterator(); iterator.hasNext();) {
			Entregamaterial entregamaterial = iterator.next();
			Double saldo = entregamaterial.getQtde()!=null ? entregamaterial.getQtde() : 0D;
			
			if (entregamaterial.getListaNotafiscalprodutoitementregamaterial()!=null){
				for (Notafiscalprodutoitementregamaterial notafiscalprodutoitementregamaterial: entregamaterial.getListaNotafiscalprodutoitementregamaterial()){
					if (notafiscalprodutoitementregamaterial.getSaldo()!=null){
						saldo -= notafiscalprodutoitementregamaterial.getSaldo();
					}
				}
			}
			
			entregamaterial.setSaldo(saldo);
			
			if (saldo<=0){
				iterator.remove();
			}
		}
		
		return new ModelAndView("direct:ajax/resultBuscaNfEntrada").addObject("lista", listaEntregamaterial);
	}
	
	/**
	 * @param request
	 * @return
	 * @author Andrey Leonardo
	 */
	@SuppressWarnings("unchecked")
	public ModelAndView vincularProdutoANotaFiscalEntrada(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		List<Entregamaterial> listaEntregamaterial = notafiscalprodutoitem.getListaEntregamaterial();
		
		for (Iterator<Entregamaterial> iterator = listaEntregamaterial.iterator(); iterator.hasNext();) {
			Entregamaterial entregamaterial = iterator.next();
			if (!Boolean.TRUE.equals(entregamaterial.getCheckbox())){
				iterator.remove();
			}
		}
		
		Set<Integer> listaCdmaterial = new HashSet<Integer>();
		listaCdmaterial.add(notafiscalprodutoitem.getMaterial().getCdmaterial());
		
		for (Entregamaterial entregamaterial: listaEntregamaterial){
			listaCdmaterial.add(entregamaterial.getMaterial().getCdmaterial());
		}
		
		if (listaCdmaterial.size()>1){
			return new JsonModelAndView().addObject("msg", "N�o � poss�vel associar produtos diferentes entre as notas de sa�da e de entrada.");
		}
		
		Collections.sort(listaEntregamaterial, new BeanComparator("saldo"));
		
		Double aux = notafiscalprodutoitem.getQtde();
		
		for (Entregamaterial entregamaterial: listaEntregamaterial){
			Double saldo = entregamaterial.getSaldo();
			Double saldoAux;
			boolean fimLoop = false;
			
			if (saldo<=aux){
				saldoAux = saldo;
				aux -= saldo;  
			}else {
				saldoAux = aux;
				fimLoop = true;
			}
			
			Notafiscalprodutoitementregamaterial notafiscalprodutoitemEntregamaterial = new Notafiscalprodutoitementregamaterial();
			notafiscalprodutoitemEntregamaterial.setNotafiscalprodutoitem(notafiscalprodutoitem);
			notafiscalprodutoitemEntregamaterial.setEntregamaterial(entregamaterial);
			notafiscalprodutoitemEntregamaterial.setSaldo(saldoAux);
			notafiscalprodutoitementregamaterialService.saveOrUpdate(notafiscalprodutoitemEntregamaterial);
			
			if (fimLoop){
				break;
			}
		}
		
		return new JsonModelAndView().addObject("msg", "");
	}
	
	/**
	 * @param request
	 * @param notafiscalprodutoitem
	 * @return
	 * @author Andrey Leonardo
	 */
	public ModelAndView desvincularProduto(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){		
		/*Notafiscalprodutoitementregamaterial notafiscalprodutoitementregamaterial = notafiscalprodutoitementregamaterialService.loadByNotafiscalprodutoitem(notafiscalprodutoitem);
		Notafiscalproduto notafiscalproduto = notafiscalprodutoitementregamaterial.getNotafiscalprodutoitem().getNotafiscalproduto();
		
		if(memorandoexportacaoService.isExisteMemorandoexportacao(notafiscalproduto)){
			request.addError("Um Memorando de Exporta��o j� foi emitido para esta nota de sa�da! ");
			request.addError("� necess�rio abrir o popup Emitir Memorando de Exporta��o nesta nota e realizar " +
					"a exclus�o de todos os memorandos vinculados a mesma, para que a desvincula��o dos protudos seja permitida.");
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota="+notafiscalprodutoitem.getCdNota()).addObject("request", request);
		}else{		
			notafiscalprodutoitementregamaterialService.delete(notafiscalprodutoitementregamaterial);		
			request.addMessage("Produto desvinculado com sucesso.");
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota="+notafiscalprodutoitem.getCdNota());
		}*/
		
		List<Notafiscalprodutoitementregamaterial> listaNotafiscalprodutoitementregamaterial = notafiscalprodutoitementregamaterialService.loadListaByNotafiscalprodutoitem(notafiscalprodutoitem);
		
		for (Notafiscalprodutoitementregamaterial notafiscalprodutoitementregamaterial: listaNotafiscalprodutoitementregamaterial){
			if(memorandoexportacaoService.isExisteMemorandoexportacao(notafiscalprodutoitementregamaterial.getNotafiscalprodutoitem().getNotafiscalproduto())){
				request.addError("Um Memorando de Exporta��o j� foi emitido para esta nota de sa�da.");
				request.addError("� necess�rio abrir o popup Emitir Memorando de Exporta��o nesta nota e realizar " +
								 "a exclus�o de todos os memorandos vinculados a mesma, para que a desvincula��o dos protudos seja permitida.");
				return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota=" + notafiscalprodutoitem.getCdNota());
			}
		}
		
		for (Notafiscalprodutoitementregamaterial notafiscalprodutoitementregamaterial: listaNotafiscalprodutoitementregamaterial){
			notafiscalprodutoitementregamaterialService.delete(notafiscalprodutoitementregamaterial);
		}
		
		request.addMessage("Produtos desvinculados com sucesso.");
		
		return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota=" + notafiscalprodutoitem.getCdNota());
	}
	
	public ModelAndView ajaxLoadSugestaoMunicipioincidenteiss(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		boolean issqn = !Boolean.TRUE.equals(notafiscalprodutoitem.getTributadoicms());
		ModelAndView retorno = new JsonModelAndView();
		if(notafiscalprodutoitem.getGrupotributacao() != null && issqn){
			Grupotributacao grupotributacao = grupotributacaoService.load(notafiscalprodutoitem.getGrupotributacao(), "grupotributacao.cdgrupotributacao, grupotributacao.municipioincidenteiss");
			if(grupotributacao != null && grupotributacao.getMunicipioincidenteiss() != null){
				Municipio municipioincidente = municipioService.carregaMunicipio(grupotributacao.getMunicipioincidenteiss());
				retorno
					.addObject("cdmunicipioincidenteiss", municipioincidente.getCdmunicipio())
					.addObject("nomemunicipioincidenteiss", municipioincidente.getNomecompleto());
			}
		}
		
		return retorno;
	}
}