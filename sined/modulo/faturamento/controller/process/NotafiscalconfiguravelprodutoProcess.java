package br.com.linkcom.sined.modulo.faturamento.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonfproduto;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonfprodutocampo;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.service.EmpresaconfiguracaonfprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.NotafiscalconfiguravelprodutoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.utils.StringUtils;

@Controller(path="/faturamento/process/Notafiscalconfiguravelproduto", authorizationModule=ProcessAuthorizationModule.class)
public class NotafiscalconfiguravelprodutoProcess extends MultiActionController {
	
	private EmpresaconfiguracaonfprodutoService empresaconfiguracaonfprodutoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	
	public void setEmpresaconfiguracaonfprodutoService(EmpresaconfiguracaonfprodutoService empresaconfiguracaonfprodutoService) {
		this.empresaconfiguracaonfprodutoService = empresaconfiguracaonfprodutoService;
	}

	public ModelAndView escolhaConf(WebRequestContext request, NotafiscalconfiguravelprodutoBean bean){
		return new ModelAndView("direct:/process/popup/escolhaConfProduto", "bean", bean);
	}
	
	public void confirmaEscolha(WebRequestContext request, NotafiscalconfiguravelprodutoBean bean){
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>parent.location = '../../faturamento/process/Notafiscalconfiguravelproduto?ACAO=gerarArquivo&selectedItens=" + bean.getSelectedItens() + "&empresaconfiguracaonfproduto.cdempresaconfiguracaonfproduto=" + bean.getEmpresaconfiguracaonfproduto().getCdempresaconfiguracaonfproduto()+ "';parent.$.akModalRemove(true);</script>");
	}
	
	public ModelAndView gerarArquivo(WebRequestContext request, NotafiscalconfiguravelprodutoBean bean){
		Empresaconfiguracaonfproduto conf = empresaconfiguracaonfprodutoService.loadForEntrada(bean.getEmpresaconfiguracaonfproduto());
		NotaFiltro filtro = new NotaFiltro();
		filtro.setWhereIn(bean.getSelectedItens());
		List<Notafiscalproduto> listaNota = notafiscalprodutoService.findForReport(filtro);
		StringBuilder sb = new StringBuilder();
		
		for (Notafiscalproduto nf : listaNota) {
			if(nf.getEnderecoCliente() == null) {
				request.addError("Favor cadastrar o endere�o do cliente.");
				continue;
			}
			
			sb.append(this.makeNF(nf, conf));
			sb.append("\n\n"); // QUEBRA DE P�GINA
		}
		
		if(request.getBindException().hasErrors()){
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
		}
		
		Resource resource = new Resource("application/remote_printing", "impressaonf_" + SinedUtil.datePatternForReport() + ".w3erp", sb.toString().getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}

	private String makeNF(Notafiscalproduto nf, Empresaconfiguracaonfproduto conf) {
		StringBuilder sb = new StringBuilder();
		List<Empresaconfiguracaonfprodutocampo> listaCampo;
		
		Integer totalpaginas = this.getTotalPaginas(nf, conf);
		Integer pagina = 1;
		boolean todositensescritos = false;
		do {
			nf.setPagina(pagina + "/" + totalpaginas);
			for (int i = 1; i <= conf.getLinhasnotafiscal(); i++) {
				if(i == conf.getLinhacorpoinicio()){
					listaCampo = this.getCamposCorpo(conf.getListaCampo());
					sb.append(this.makeCorpo(listaCampo, conf.getLinhacorpoinicio(), conf.getLinhacorpofim(), nf));
					i = conf.getLinhacorpofim();
				} else {
					listaCampo = this.getCamposLinha(conf.getListaCampo(), i);
					sb.append(this.makeLinha(listaCampo, nf));
					sb.append("\n");
				}
			}
			todositensescritos = this.isTodosItensEscritos(nf);
			pagina++;
		} while (!todositensescritos);

		return sb.toString();
	}
	
	/**
	 * M�todo para calcular o total de p�ginas
	 *
	 * @param nf
	 * @param conf
	 * @return
	 * @author Luiz Fernando
	 */
	private Integer getTotalPaginas(Notafiscalproduto nf, Empresaconfiguracaonfproduto conf){
		StringBuilder sb = new StringBuilder();
		List<Empresaconfiguracaonfprodutocampo> listaCampo;
		Integer totalpaginas = 0;
		boolean todositensescritos = false;
		do {
			for (int i = 1; i <= conf.getLinhasnotafiscal(); i++) {
				if(i == conf.getLinhacorpoinicio()){
					listaCampo = this.getCamposCorpo(conf.getListaCampo());
					sb.append(this.makeCorpo(listaCampo, conf.getLinhacorpoinicio(), conf.getLinhacorpofim(), nf));
					i = conf.getLinhacorpofim();
				} else {
					listaCampo = this.getCamposLinha(conf.getListaCampo(), i);
					sb.append(this.makeLinha(listaCampo, nf));
					sb.append("\n");
				}
			}
			todositensescritos = this.isTodosItensEscritos(nf);
			totalpaginas++;
		} while (!todositensescritos);
		
		for(Notafiscalprodutoitem it : nf.getListaItens())
			it.setItemescrito(null);
		
		return totalpaginas;
	}
	
	/**
	 * M�todo que verifica se todos os itens foram escritos na string
	 *
	 * @param nf
	 * @return
	 * @author Luiz Fernando
	 */
	private boolean isTodosItensEscritos(Notafiscalproduto nf){
		if(nf != null && nf.getListaItens() != null){
			for(Notafiscalprodutoitem it : nf.getListaItens()){
				if(it.getItemescrito() == null || !it.getItemescrito())
					return false;
					
			}
		}
		
		return true;
	}

	
	private String makeCorpo(List<Empresaconfiguracaonfprodutocampo> listaCampo, Integer linhacorpoinicio, Integer linhacorpofim, Notafiscalproduto nf) {
		int linhasescritas = 1;
		int totalcorpo = (linhacorpofim - linhacorpoinicio) + 1;
		
		Collections.sort(listaCampo, new Comparator<Empresaconfiguracaonfprodutocampo>(){
			public int compare(Empresaconfiguracaonfprodutocampo o1, Empresaconfiguracaonfprodutocampo o2) {
				return o1.getColuna().compareTo(o2.getColuna());
			}
		});
		
		StringBuilder sb = new StringBuilder();
		
		List<Notafiscalprodutoitem> listaItens = nf.getListaItens();
		int coluna, col;
		String str;
		char[] charArray;
		for (Notafiscalprodutoitem it : listaItens) {
			if(it.getItemescrito() != null && it.getItemescrito())
				continue;
			
			coluna = 1;

			for (Empresaconfiguracaonfprodutocampo cp : listaCampo) {
				sb.append(StringUtils.geraStringVazia(cp.getColuna() - coluna));
				str = this.getCampoCorpoStr(it, cp);
				charArray = str.toCharArray();
				col = cp.getTamanhomax();
				for (int i = 0; i < charArray.length; i++) {
					if(col == 0) {
						col = cp.getTamanhomax();
						sb.append("\n");
						linhasescritas++;
						if(linhasescritas > totalcorpo){
							return sb.toString();
						}
						
						sb.append(StringUtils.geraStringVazia(cp.getColuna() - 1));
					}
					sb.append(charArray[i]);
					col--;
				}
				
				coluna = cp.getColuna() + cp.getTamanhomax();
				sb.append(StringUtils.geraStringVazia(col));
			}
			
			if((linhasescritas + 2) > totalcorpo){
				return sb.toString();
			}
			sb.append("\n\n");
			linhasescritas+=2;
			
			it.setItemescrito(true);
		}
		
		for (int i = linhasescritas; i <= totalcorpo; i++) {
			sb.append("\n");
		}
		return sb.toString();
	}
	
	private String getCampoCorpoStr(Notafiscalprodutoitem it, Empresaconfiguracaonfprodutocampo cp) {
		StringBuilder sb = new StringBuilder();
		
		switch (cp.getCamponota()) {
			case CAMPO_LIVRE:
				sb.append(cp.getCampolivre());
				break;
			
			case QTDE_NOTAITEM:
				sb.append(it.getQtde());
				break;
			
			case DISCRIMINACAOPRODUTO_NOTAITEM:
				if(it.getMaterial().getNomenfOuNome() != null){
					sb.append(it.getMaterial().getNomenfOuNome());
				}
				if(it.getLoteestoque() != null && it.getLoteestoque().getDescriptionAutocomplete() != null){
					sb.append(" " + it.getLoteestoque().getDescriptionAutocomplete());
				}
				break;
			case VALORUNITARIO_NOTAITEM:
				if(it.getValorunitario() != null ){
					sb.append(it.getValorunitario().toString());
				}
				break;
				
			case VALORTOTALITEM_NOTAITEM:
				sb.append(it.getValorbruto().toString());
				break;
			
			case CFOP_NOTAITEM:
				if(it.getCfop() != null && it.getCfop().getCodigo() != null){
					sb.append(it.getCfop().getCodigo());
				}
				break;
			case UNIDADEMEDIDA_NOTAITEM:
				if(it.getUnidademedida() != null){
					sb.append(it.getUnidademedida());
				}
				break;
			case ALIQUOTAICMS_NOTAITEM:
				if(it.getIcms() != null ){
					sb.append(it.getIcms());
				}
				break;
			case CSTICMS_NOTAITEM:
				if(it.getTipocobrancaicms() != null){
					sb.append(it.getTipocobrancaicms().getCdnfe());
				}
				break;
			default:
				break;
		}
		
		return sb.toString();
	}

	
	private List<Empresaconfiguracaonfprodutocampo> getCamposCorpo(List<Empresaconfiguracaonfprodutocampo> listaCampo) {
		List<Empresaconfiguracaonfprodutocampo> lista = new ArrayList<Empresaconfiguracaonfprodutocampo>();
		for (Empresaconfiguracaonfprodutocampo campo : listaCampo) {
			if(campo.getCorpo() != null && campo.getCorpo()){
				lista.add(campo);
			}
		}
		return lista;
	}
	
	private String makeLinha(List<Empresaconfiguracaonfprodutocampo> listaCampo, Notafiscalproduto nf) {
		StringBuilder sb = new StringBuilder();
		String str;
		Empresaconfiguracaonfprodutocampo campo;
		Iterator<Empresaconfiguracaonfprodutocampo> iterator = listaCampo.iterator();
		boolean preencheu;
		for (int i = 1; iterator.hasNext(); i++) {
			preencheu = false;
			while(iterator.hasNext()){
				campo = iterator.next();
				
				str = "";
				if(campo.getColuna() != null && campo.getColuna().equals(i)){
					str = this.getCampoStr(campo, nf);
					
					if(campo.getTamanhomax() != null){
						if(str.length() > campo.getTamanhomax()){ 
							str = str.substring(0, campo.getTamanhomax());
						} else {
							str = br.com.linkcom.neo.util.StringUtils.stringCheia(str, " ", campo.getTamanhomax(), true);
						}
					}
					i = i + str.length() - 1;
					iterator.remove();
					sb.append(str);
					preencheu = true;
					break;
				}
			}
			if(!preencheu) sb.append(" ");
			iterator = listaCampo.iterator();
		}
		return sb.toString();
	}
	private String getCampoStr(Empresaconfiguracaonfprodutocampo campo, Notafiscalproduto nf) {
		StringBuilder sb = new StringBuilder();
		Endereco endereco;
		Municipio municipio;
		Cliente cliente;
		Date dtemissao;
		switch (campo.getCamponota()) {
			case CAMPO_LIVRE:
				sb.append(campo.getCampolivre());
				break;
			
			case VALORDESCONTO_NOTA:
				if(nf.getValordesconto() != null){
					sb.append(nf.getValordesconto().toString());
				}
				break;
	
			case NUMERO_NOTA :
				if(nf.getNumeroFormatado() != null && !nf.getNumeroFormatado().equals("")){
					sb.append(nf.getNumeroFormatado());
				}
				break;
	
			case DTEMISSAO_NOTA :
				sb.append(SinedDateUtils.toString(nf.getDtEmissao()));
				break;
	
			case END_CLIENTE :
				endereco = nf.getEnderecoCliente();
				sb.append(endereco.getLogradouro());
				break;
	
			case ENDNUM_CLIENTE :
				endereco = nf.getEnderecoCliente();
				sb.append(endereco.getLogradouro());
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					sb.append(", ");
					sb.append(endereco.getNumero());
				}
				break;
	
			case ENDNUMCOMP_CLIENTE :
				endereco = nf.getEnderecoCliente();
				sb.append(endereco.getLogradouro());
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					sb.append(", ");
					sb.append(endereco.getNumero());
				}
				if(endereco.getComplemento() != null && !endereco.getComplemento().equals("")){
					sb.append(", ");
					sb.append(endereco.getComplemento());
				}
				break;
	
			case ENDNUMCOMPBAI_CLIENTE :
				endereco = nf.getEnderecoCliente();
				sb.append(endereco.getLogradouro());
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					sb.append(", ");
					sb.append(endereco.getNumero());
				}
				if(endereco.getComplemento() != null && !endereco.getComplemento().equals("")){
					sb.append(", ");
					sb.append(endereco.getComplemento());
				}
				if(endereco.getBairro() != null && !endereco.getBairro().equals("")){
					sb.append(", ");
					sb.append(endereco.getBairro());
				}
				break;
			case NUMERO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getNumero() != null && !endereco.getNumero().equals("")){
					sb.append(endereco.getNumero());
				}
				break;
	
			case COMPLEMENTO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getComplemento() != null && !endereco.getComplemento().equals("")){
					sb.append(endereco.getComplemento());
				}
				break;
			case BAIRRO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getBairro() != null && !endereco.getBairro().equals("")){
					sb.append(endereco.getBairro());
				}
				break;
			case CEP_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getCep() != null && !endereco.getCep().toString().equals("")){
					sb.append(endereco.getCep().toString());
				}
				break;
			case MUNICIPIO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getMunicipio() != null){
					municipio = endereco.getMunicipio();
					sb.append(municipio.getNome());
				}
				break;
			case MUNICIPIOUF_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
					municipio = endereco.getMunicipio();
					sb.append(municipio.getNome());
					sb.append(" - ");
					sb.append(municipio.getUf().getSigla());
				}
				break;
			case UFDESCRICAO_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
					municipio = endereco.getMunicipio();
					sb.append(municipio.getUf().getNome());
				}
				break;
			case UFSIGLA_CLIENTE :
				endereco = nf.getEnderecoCliente();
				if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
					municipio = endereco.getMunicipio();
					sb.append(municipio.getUf().getSigla());
				}
				break;
			case CPFCNPJ_CLIENTE :
				if(nf.getCliente().getCpfOuCnpj() != null){
					cliente = nf.getCliente();
					sb.append(cliente.getCpfOuCnpj());
				}
				break;
			case INSCESTADUAL_CLIENTE:
				if(nf.getCliente() != null){
					cliente = nf.getCliente();
					if(cliente.getInscricaoestadual() != null){
						sb.append(cliente.getInscricaoestadual());
					}
				}
				break;
			case INSCMUNICIPAL_CLIENTE:
				if(nf.getCliente() != null){
					cliente = nf.getCliente();
					if(cliente.getInscricaomunicipal() != null){
						sb.append(cliente.getInscricaomunicipal());
					}
				}
				break;
			case RAZAOSOCIALNOME_CLIENTE:
				if(nf.getCliente() != null){
					cliente = nf.getCliente();
					if(cliente.getRazaosocial() != null && cliente.getRazaosocial().equals("")){
						sb.append(cliente.getRazaosocial());
					} else {
						sb.append(cliente.getNome());
					}
				}
				break;
			case RAZAOSOCIAL_CLIENTE:
				if(nf.getCliente() != null){
					cliente = nf.getCliente();
					if(cliente.getRazaosocial() != null){
						sb.append(cliente.getRazaosocial());
					}
				}
				break;
			case NOME_CLIENTE:
				if(nf.getCliente() != null){
					cliente = nf.getCliente();
					sb.append(cliente.getNome());
				}
				break;
			case DIAEMISSAO_NOTA:
				if(nf.getDtEmissao() != null){
					dtemissao = nf.getDtEmissao();
					sb.append(SinedDateUtils.getDateProperty(dtemissao, Calendar.DAY_OF_MONTH));
				}
				break;
			case MESNUMEMISSAO_NOTA:
				if(nf.getDtEmissao() != null){
					dtemissao = nf.getDtEmissao();
					sb.append((SinedDateUtils.getDateProperty(dtemissao, Calendar.MONTH) + 1));
				}
				break;
			case MESDESCRICAOEMISSAO_NOTA:
				if(nf.getDtEmissao() != null){
					dtemissao = nf.getDtEmissao();
					
					int month = SinedDateUtils.getDateProperty(dtemissao, Calendar.MONTH);
					String mesEmissao = "";
					if(month == 0) 	mesEmissao = "Janeiro";
					if(month == 1) 	mesEmissao = "Fevereiro";
					if(month == 2) 	mesEmissao = "Mar�o";
					if(month == 3) 	mesEmissao = "Abril";
					if(month == 4) 	mesEmissao = "Maio";
					if(month == 5) 	mesEmissao = "Junho";
					if(month == 6) 	mesEmissao = "Julho";
					if(month == 7) 	mesEmissao = "Agosto";
					if(month == 8) 	mesEmissao = "Setembro";
					if(month == 9) 	mesEmissao = "Outubro";
					if(month == 10) mesEmissao = "Novembro";
					if(month == 11) mesEmissao = "Dezembro";
					
					sb.append(mesEmissao);
				}
				break;
			case ANOEMISSAO_NOTA:
				if(nf.getDtEmissao()!= null){
					dtemissao = nf.getDtEmissao();
					sb.append(SinedDateUtils.getDateProperty(dtemissao, Calendar.YEAR));
				}
				break;
			case VALORPRODUTOS_NOTA:
				if(nf.getValor() != null){
					sb.append(nf.getValor());
				}
				break;
			case VALORISS_NOTA:
				if(nf.getValoriss() != null){
					sb.append(nf.getValoriss());
				}
				break;
	
			case PERCENTUALISS_NOTA:
				if(nf.getValorbciss() != null){
					sb.append(nf.getValorbciss());
				}
				break;
	
			case VALORINSS_NOTA:
				if(nf.getValorprevsocial() != null){
					sb.append(nf.getValorprevsocial());
					break;
				}
			case PERCENTUALINSS_NOTA:
				if(nf.getValorbcprevsocial() != null){
					sb.append(nf.getValorbcprevsocial());
				}
				break;
				
			case VALORCOFINS_NOTA:
				if(nf.getValorcofins() != null){
					sb.append(nf.getValorcofins());
				}
				break;
				
			case PERCENTUALCOFINS_NOTA:
				if(nf.getValorcofinsretido() != null){
					sb.append(nf.getValorcofinsretido());
				}
				break;
			case VALORCSLL_NOTA:
				if(nf.getValorcsll() != null){
					sb.append(nf.getValorcsll());
				}
				break;
//			case PERCENTUALCSLL_NOTA:
//				if(nf.getCsll() != null){
//					sb.append(nf.getCsll());
//				}
//				break;
			case VALORICMS_NOTA:
				if(nf.getValoricms() != null){
					sb.append(nf.getValoricms());
				}
				break;
			case PERCENTUALICMS_NOTA:
				if(nf.getValorbcicms() != null){
					sb.append(nf.getValorbcicms());
				}
				break;
			case VALORTOTALEXTENSO_NOTA:
				if(nf.getValorNota() != null){
					sb.append(new Extenso(nf.getValorNota()).toString());
				}
				break;
			case VOLORTOTAL_NOTA:
				if(nf.getValorNota() != null){
					sb.append(nf.getValorNota().toString());
				}
				break;
			case CDCONTRATO:
				String cdcontrato = "";
				if(nf.getListaNotaContrato() != null){
					for(int i = 0; i < nf.getListaNotaContrato().size(); i++){
						if(nf.getListaNotaContrato().get(i).getContrato().getCdcontrato() != null){
							cdcontrato += nf.getListaNotaContrato().get(i).getContrato().getCdcontrato().toString() + ", ";
						}
					}
				}
				if(!cdcontrato.equals("")){
					cdcontrato = cdcontrato.substring(0, cdcontrato.length() - 2);
				}
				sb.append(cdcontrato);
				break;
			case CDDOCUMENTO:
				if(nf.getListaNotaDocumento() != null){
					String coddocumento = CollectionsUtil.listAndConcatenate(nf.getListaNotaDocumento(), "documento.cddocumento", ",");
					sb.append(coddocumento);
				}
				break;
			case TIPO_NOTA:
				if(nf.getTipooperacaonota() != null && nf.getTipooperacaonota().getDescricao() != null){
					sb.append(nf.getTipooperacaonota().getDescricao());
				}
				break;
			case NATUREZAOPERACAO_NOTA:
				if(nf.getNaturezaoperacao() != null){
					sb.append(nf.getNaturezaoperacao());
				}
				break;
			case INSCESTADUAL_SUBTRIBUTARIO:
				if(nf.getEmpresa() != null && nf.getEmpresa().getInscricaoestadualst() != null){
					sb.append(nf.getEmpresa().getInscricaoestadualst());
				}
				break;
			case VALORFRETE_NOTA:
				if(nf.getValorfrete() != null){
					sb.append(nf.getValorfrete());
				}
				break;
			case VALORSEGURO_NOTA:
				if(nf.getValorseguro() != null){
					sb.append(nf.getValorseguro());
				}
				break;
			case VALOROUTRASDESPESAS_NOTA:
				if(nf.getOutrasdespesas() != null){
					sb.append(nf.getOutrasdespesas());
				}
				break;
			case PLACAVEICULO_NOTA:
				if(nf.getPlacaveiculo() != null){
					sb.append(nf.getPlacaveiculo());
				}
				break;
			case VENDEDOR_NOTA:
				if(nf.getListaNotavenda() != null && !nf.getListaNotavenda().isEmpty()){
					for(NotaVenda notavenda : nf.getListaNotavenda()){
						if(notavenda.getVenda() != null && notavenda.getVenda().getColaborador() != null && 
								notavenda.getVenda().getColaborador().getNome() != null){
							sb.append(notavenda.getVenda().getColaborador().getNome());
							break;
						}
					}
				}
				break;
			case CONDICAOPAGAMENTO_NOTA:
				if(nf.getFormapagamentonfe() != null){
					sb.append(nf.getFormapagamentonfe().getDescricao());
				}
				break;
			case RESPONSAVELFRETE_NOTA:
				if(nf.getResponsavelfrete() != null && nf.getResponsavelfrete().getNome() != null){
					sb.append(nf.getResponsavelfrete().getNome());
				}
				break;
			case PAGINA_NOTA:
				if(nf.getPagina() != null){
					sb.append(nf.getPagina());
				}
				break;
			default:
				break;
		}
		return sb.toString();
	}
	
	private List<Empresaconfiguracaonfprodutocampo> getCamposLinha(List<Empresaconfiguracaonfprodutocampo> listaCampo, int i) {
		List<Empresaconfiguracaonfprodutocampo> lista = new ArrayList<Empresaconfiguracaonfprodutocampo>();
		for (Empresaconfiguracaonfprodutocampo campo : listaCampo) {
			if(campo.getLinha() != null && campo.getLinha().equals(i)){
				lista.add(campo);
			}
		}
		return lista;
	}
	
}
