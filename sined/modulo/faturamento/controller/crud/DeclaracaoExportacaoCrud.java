package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacao;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacaoHistorico;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacaoNota;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoDeclaracaoExportacao;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.DeclaracaoExportacaoHistoricoService;
import br.com.linkcom.sined.geral.service.DeclaracaoExportacaoNotaService;
import br.com.linkcom.sined.geral.service.DeclaracaoExportacaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.DeclaracaoExportacaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/DeclaracaoExportacaoCrud", authorizationModule=CrudAuthorizationModule.class)
public class DeclaracaoExportacaoCrud extends CrudControllerSined<DeclaracaoExportacaoFiltro, DeclaracaoExportacao, DeclaracaoExportacao>{

	private NotafiscalprodutoService notafiscalprodutoService;
	private DeclaracaoExportacaoNotaService declaracaoExportacaoNotaService;
	private DeclaracaoExportacaoHistoricoService declaracaoExportacaoHistoricoService;
	private ArquivonfnotaService arquivoNfNotaService;
	private DeclaracaoExportacaoService declaracaoExportacaoService;
	
	public void setDeclaracaoExportacaoService(DeclaracaoExportacaoService declaracaoExportacaoService) {this.declaracaoExportacaoService = declaracaoExportacaoService;}
	public void setArquivoNfNotaService(ArquivonfnotaService arquivoNfNotaService) {this.arquivoNfNotaService = arquivoNfNotaService;}
	public void setDeclaracaoExportacaoHistoricoService(DeclaracaoExportacaoHistoricoService declaracaoExportacaoHistoricoService) {this.declaracaoExportacaoHistoricoService = declaracaoExportacaoHistoricoService;}
	public void setDeclaracaoExportacaoNotaService(DeclaracaoExportacaoNotaService declaracaoExportacaoNotaService) {this.declaracaoExportacaoNotaService = declaracaoExportacaoNotaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	
	private static final String DECLARACAO_EMPRESA = "DeclaracaoEmpresa";
	
	@Override
	protected void entrada(WebRequestContext request, DeclaracaoExportacao form)throws Exception {
		declaracaoExportacaoService.loadForEntrada(form);
		String whereInNotas = request.getParameter("listaNotas");
		if(whereInNotas != null && !whereInNotas.equals("")){
			form.setListaDeclaracaoExportacaoNota(gerarListaNotas(whereInNotas));
			if(form.getListaDeclaracaoExportacaoNota().get(0).getNotaFiscalProduto() !=null){
				form.setEmpresa(form.getListaDeclaracaoExportacaoNota().get(0).getNotaFiscalProduto().getEmpresa());
			}
		}else{
			if(form.getListaDeclaracaoExportacaoNota() !=null){
				for (DeclaracaoExportacaoNota declaracaoNota : form.getListaDeclaracaoExportacaoNota()) {
					declaracaoNota.setNotaFiscalProduto(completaDadosNota(declaracaoNota.getNotaFiscalProduto()));
				}
			}
		}
		super.entrada(request, form);
	}	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,DeclaracaoExportacaoFiltro filtro) throws CrudException {
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request,DeclaracaoExportacaoFiltro filtro) throws Exception {
		super.listagem(request, filtro);
	}
	
	public List<DeclaracaoExportacaoNota> gerarListaNotas(String whereInNotas){
		List<DeclaracaoExportacaoNota> listaDeclaracaoExportacaoNota = new ArrayList<DeclaracaoExportacaoNota>();
		List<Notafiscalproduto> listaNota = notafiscalprodutoService.findByWhereIn(whereInNotas);
		for (Notafiscalproduto nota : listaNota) {
			nota = completaDadosNota(nota);
			DeclaracaoExportacaoNota declaracaoNota= new DeclaracaoExportacaoNota();
			declaracaoNota.setNotaFiscalProduto(nota);
			listaDeclaracaoExportacaoNota.add(declaracaoNota);
		}
		return listaDeclaracaoExportacaoNota;
	}
	
	@Override
	protected void salvar(WebRequestContext request, DeclaracaoExportacao bean)	throws Exception {
		
		boolean isCriando = false;
		
		if(bean.getCdDeclaracaoExportacao() == null){
			isCriando = true;
		}
		bean.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		bean.setDtaltera(SinedDateUtils.currentTimestamp());
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			throw new SinedException("Erro ao salvar, verifique se existe nota(s) duplicadas na declara��o");
		}
		
		if(isCriando){
			salvarHistorico(AcaoDeclaracaoExportacao.CRIAR,bean);
		}else{
			salvarHistorico(AcaoDeclaracaoExportacao.ALTETAR,bean);
		}
	}
	
	/**
	 * 
	 * @exception numero da mensagem � o identico ao do caso de uso
	 */
	@Override
	protected void validateBean(DeclaracaoExportacao bean, BindException errors) {
		if(bean.getListaDeclaracaoExportacaoNota() ==null || SinedUtil.isListEmpty(bean.getListaDeclaracaoExportacaoNota())){
			errors.reject("001","Informe pelo menos uma nota fiscal");
		}else{
			String whereInNotas = "";
				for (DeclaracaoExportacaoNota declaracaoNota : bean.getListaDeclaracaoExportacaoNota()) {
					Notafiscalproduto nota = notafiscalprodutoService.findByWhereIn(declaracaoNota.getNotaFiscalProduto().getCdNota().toString()).get(0);
					if(!NotaStatus.NFE_EMITIDA.equals(nota.getNotaStatus()) && !NotaStatus.NFE_LIQUIDADA.equals(nota.getNotaStatus())){
						errors.reject("002","N�o � permitido vincular notas fiscais de situa��o diferente de NF-E EMITIDA ou NF-E LIQUIDADA.");
					}
					whereInNotas += nota.getCdNota().toString() + ",";
				}
			
			List<DeclaracaoExportacaoNota> listaNotasDeclaradas = declaracaoExportacaoNotaService.buscarNotasVinculadas(whereInNotas.substring(0,whereInNotas.length() - 1));
			boolean isNotaDeclarada = false;
			String msg = "";
			if(!SinedUtil.isListEmpty(listaNotasDeclaradas)){			
				for (DeclaracaoExportacaoNota declaracaoExportacaoNota : listaNotasDeclaradas) {
					if(!declaracaoExportacaoNota.getDeclaracaoExportacao().equals(bean)){
						msg += "Nota fiscal "+ declaracaoExportacaoNota.getNotaFiscalProduto().getNumero() + " j� est� vinculada a declara��o " + declaracaoExportacaoNota.getDeclaracaoExportacao().getNumeroDeclaracao()+"\n";
						isNotaDeclarada = true;
					}
				}
			}
			if(isNotaDeclarada){
				errors.reject("003",msg);
			}
			for (DeclaracaoExportacaoNota declaracaoNota : bean.getListaDeclaracaoExportacaoNota()) {
				Notafiscalproduto nota = notafiscalprodutoService.findByWhereIn(declaracaoNota.getNotaFiscalProduto().getCdNota().toString()).get(0);
				if(nota.getNumero()!=null){
					declaracaoNota.getNotaFiscalProduto().setNumero(nota.getNumero());
				}
				if(!nota.getEmpresa().equals(bean.getEmpresa())){
					errors.reject("004","N�o � permitido vincular a Declara��o de Exporta��o, notas fiscais de empresas diferentes.");	
				}
			}	
		}
		super.validateBean(bean, errors);
	}
	
	private void salvarHistorico(AcaoDeclaracaoExportacao acao, DeclaracaoExportacao bean){
		DeclaracaoExportacaoHistorico historico = new DeclaracaoExportacaoHistorico();
		historico.setAcao(acao.toString());
		historico.setObservacao("");
		historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		historico.setDtaltera(SinedDateUtils.currentTimestamp());
		historico.setDeclaracaoExportacao(bean);
		declaracaoExportacaoHistoricoService.saveOrUpdate(historico);
	}
	private Notafiscalproduto completaDadosNota(Notafiscalproduto nota){ 
		if(nota !=null && nota.getCdNota() !=null){
			Arquivonfnota arquivo = arquivoNfNotaService.findByNotaProduto(nota);
			if(arquivo!=null && arquivo.getArquivonf() !=null && arquivo.getArquivonf().getConfiguracaonfe()!= null){
				nota.setModeloNF(arquivo.getArquivonf().getConfiguracaonfe().getModelonfe());
			}
		}
		return nota;	
	}
	
	public ModelAndView ajaxCarregarDadosDaNota(WebRequestContext request){
		String numereoNota = (String) request.getParameter("numeroNF");
		
		Notafiscalproduto nota = new Notafiscalproduto();
		
		if(StringUtils.isNotBlank(numereoNota)){
			nota = notafiscalprodutoService.findNotaByNumeroSerie(numereoNota,null);
			nota = completaDadosNota(nota);
		}
		return new JsonModelAndView()
			.addObject("serie", nota.getSerienfe())
			.addObject("modelo", nota.getModeloNF())
			.addObject("data", nota.getDtEmissao());
	}
	
	public ModelAndView abrirPopupMaterial(WebRequestContext request){
		String numereoNota = (String) request.getParameter("numeroNota");
		if(StringUtils.isNotBlank(numereoNota)){
			Notafiscalproduto nota = notafiscalprodutoService.findNotaByNumeroSerie(numereoNota,null);
			if(nota != null){
				request.setAttribute("listaItens", nota.getListaItens());
				return new ModelAndView("direct:/crud/popup/popupMaterialNotaProduto", "bean", nota);
			}else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView setSessaoEmpresa (WebRequestContext request, Empresa empresa){
		request.getSession().setAttribute(DECLARACAO_EMPRESA, empresa.getCdpessoa());
		return new JsonModelAndView().addObject("success", true);
	}
}
