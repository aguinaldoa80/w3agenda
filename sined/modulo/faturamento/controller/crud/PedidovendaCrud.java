package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel.TipoComprovante;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresarepresentacao;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.Pedidovendasituacaoecommerce;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendatipo;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.auxiliar.PedidoVendaParametersBean;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.ContasFinanceiroSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Gradeestoquetipo;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ProducaoagendasituacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoPendencia;
import br.com.linkcom.sined.geral.bean.enumeration.TipoComprovanteEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.service.CampoextrapedidovendatipoService;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ClientevendedorService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColetaMaterialService;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.ComprovanteConfiguravelService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentocomissaovendaService;
import br.com.linkcom.sined.geral.service.DocumentocomissaovendarepresentacaoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.Ecom_PedidoVendaService;
import br.com.linkcom.sined.geral.service.EmporiumpdvService;
import br.com.linkcom.sined.geral.service.EmporiumpedidovendaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.GarantiareformaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialFaixaMarkupService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialformulavalorvendaService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.PedidovendamaterialService;
import br.com.linkcom.sined.geral.service.PedidovendamaterialmestreService;
import br.com.linkcom.sined.geral.service.PedidovendapagamentoService;
import br.com.linkcom.sined.geral.service.PedidovendapagamentorepresentacaoService;
import br.com.linkcom.sined.geral.service.PedidovendasincronizacaoService;
import br.com.linkcom.sined.geral.service.PedidovendasituacaoecommerceService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProducaoagendaService;
import br.com.linkcom.sined.geral.service.ProducaoagendahistoricoService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.RequisicaomaterialService;
import br.com.linkcom.sined.geral.service.ReservaService;
import br.com.linkcom.sined.geral.service.RestricaoService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ColetarProducaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteAnexoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteDestinatarioBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioECFBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Enviocomprovanteemail;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FornecedoragenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaMaterialItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Impostovenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.KitflexivelBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.TrocavendedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendaDevolucaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoRepresentacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.EmitirFichaColetaReport;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ComprovanteConfiguravelAlternativo;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericSearchBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaWSBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PrazopagamentoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.filter.MaterialHistoricoPrecoFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SHA512Util;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;
import br.com.linkcom.sined.util.rest.android.material.MaterialRESTModel;
import br.com.linkcom.sined.util.rest.android.material.MaterialRESTWSBean;
import br.com.linkcom.util.rest.ModelAndStatus;

@Controller(path="/faturamento/crud/Pedidovenda", authorizationModule=CrudAuthorizationModule.class)
public class PedidovendaCrud extends CrudControllerSined<PedidovendaFiltro, Pedidovenda, Pedidovenda>{

	private EmpresaService empresaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private ParametrogeralService parametrogeralService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private PedidovendaService pedidovendaService;
	private MaterialService materialService;
	private PedidovendapagamentoService pedidovendapagamentoService;
	private FornecedorService fornecedorService;
	private PrazopagamentoService prazopagamentoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ColaboradorService colaboradorService;
	private PedidovendatipoService pedidovendatipoService;
	private LocalarmazenagemService localarmazenagemService;
	private DocumentoService documentoService;
	private RestricaoService restricaoService;
	private ContaService contaService;
	private PedidovendasincronizacaoService pedidovendasincronizacaoService;
	private DocumentotipoService documentotipoService;
	private ClienteService clienteService;
	private UsuarioService usuarioService;
	private VendaService vendaService;
	private LoteestoqueService loteestoqueService;
	private SolicitacaocompraService solicitacaocompraService;
	private ProjetoService projetoService;
	private MaterialtabelaprecoService materialtabelaprecoService;
	private EmporiumpedidovendaService emporiumpedidovendaService;
	private EmporiumpdvService emporiumpdvService;
	private ProdutoService produtoService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private RegiaoService regiaoService;
	private OrdemcompraService ordemcompraService;
	private CampoextrapedidovendatipoService campoextrapedidovendatipoService;
	private PedidovendamaterialmestreService pedidovendamaterialmestreService;
	private MaterialproducaoService materialproducaoService;
	private MaterialformulavalorvendaService materialformulavalorvendaService;
	private ColetaMaterialService coletaMaterialService;
	private ColetaService coletaService;
	private ClientevendedorService clientevendedorService;
	private ValecompraService valecompraService;
	private EnvioemailService envioemailService;
	private PedidovendapagamentorepresentacaoService pedidovendapagamentorepresentacaoService;
	private PneuService pneuService;
	private RequisicaomaterialService requisicaomaterialService;
	private CategoriaService categoriaService;
	private ComprovanteConfiguravelService comprovanteConfiguravelService;
	private GarantiareformaService garantiareformaService;
	private DocumentocomissaovendaService documentocomissaovendaService;
	private DocumentocomissaovendarepresentacaoService documentocomissaovendarepresentacaoService;
	private EmitirFichaColetaReport emitirFichaColetaReport;
	private ContatoService contatoService;
	private MaterialFaixaMarkupService materialFaixaMarkupService;
	private VendamaterialService vendamaterialService;
	private ParametrogeralService parametroGeralService;
	private EnderecoService enderecoService;
	private ReservaService reservaService;
	private ContareceberService contaReceberService;
	private PedidovendasituacaoecommerceService pedidovendasituacaoecommerceService;
	private Ecom_PedidoVendaService ecom_PedidoVendaService;
	
	public void setEcom_PedidoVendaService(Ecom_PedidoVendaService ecom_PedidoVendaService) {this.ecom_PedidoVendaService = ecom_PedidoVendaService;}
	public void setPedidovendasituacaoecommerceService(PedidovendasituacaoecommerceService pedidovendasituacaoecommerceService) {this.pedidovendasituacaoecommerceService = pedidovendasituacaoecommerceService;}
	public void setEmitirFichaColetaReport(EmitirFichaColetaReport emitirFichaColetaReport) {this.emitirFichaColetaReport = emitirFichaColetaReport;}
	public void setPneuService(PneuService pneuService) {this.pneuService = pneuService;}
	public void setEnvioemailService(EnvioemailService envioemailService) {this.envioemailService = envioemailService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setClientevendedorService(ClientevendedorService clientevendedorService) {this.clientevendedorService = clientevendedorService;}
	public void setColetaService(ColetaService coletaService) {this.coletaService = coletaService;}
	public void setRegiaoService(RegiaoService regiaoService) {this.regiaoService = regiaoService;}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {this.emporiumpdvService = emporiumpdvService;}
	public void setEmporiumpedidovendaService(EmporiumpedidovendaService emporiumpedidovendaService) {this.emporiumpedidovendaService = emporiumpedidovendaService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setPedidovendapagamentoService(PedidovendapagamentoService pedidovendapagamentoService) {this.pedidovendapagamentoService = pedidovendapagamentoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {this.pedidovendahistoricoService = pedidovendahistoricoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setRestricaoService(RestricaoService restricaoService) {this.restricaoService = restricaoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setPedidovendasincronizacaoService(PedidovendasincronizacaoService pedidovendasincronizacaoService) {this.pedidovendasincronizacaoService = pedidovendasincronizacaoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {this.solicitacaocompraService = solicitacaocompraService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setMaterialtabelaprecoService(MaterialtabelaprecoService materialtabelaprecoService) {this.materialtabelaprecoService = materialtabelaprecoService;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {this.producaoagendaService = producaoagendaService;}
	public void setProducaoagendahistoricoService(ProducaoagendahistoricoService producaoagendahistoricoService) {this.producaoagendahistoricoService = producaoagendahistoricoService;}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {this.ordemcompraService = ordemcompraService;}
	public void setCampoextrapedidovendatipoService(CampoextrapedidovendatipoService campoextrapedidovendatipoService) {this.campoextrapedidovendatipoService = campoextrapedidovendatipoService;}
	public void setPedidovendamaterialmestreService(PedidovendamaterialmestreService pedidovendamaterialmestreService) {this.pedidovendamaterialmestreService = pedidovendamaterialmestreService;}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {this.materialproducaoService = materialproducaoService;}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {this.materialformulavalorvendaService = materialformulavalorvendaService;}
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {this.coletaMaterialService = coletaMaterialService;}
	public void setPedidovendapagamentorepresentacaoService(PedidovendapagamentorepresentacaoService pedidovendapagamentorepresentacaoService) {this.pedidovendapagamentorepresentacaoService = pedidovendapagamentorepresentacaoService;}
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {this.requisicaomaterialService = requisicaomaterialService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setComprovanteConfiguravelService(ComprovanteConfiguravelService comprovanteConfiguravelService) {this.comprovanteConfiguravelService = comprovanteConfiguravelService;}
	public void setGarantiareformaService(GarantiareformaService garantiareformaService) {this.garantiareformaService = garantiareformaService;}
	public void setDocumentocomissaovendaService(DocumentocomissaovendaService documentocomissaovendaService) {this.documentocomissaovendaService = documentocomissaovendaService;}
	public void setDocumentocomissaovendarepresentacaoService(DocumentocomissaovendarepresentacaoService documentocomissaovendarepresentacaoService) {this.documentocomissaovendarepresentacaoService = documentocomissaovendarepresentacaoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setMaterialFaixaMarkupService(MaterialFaixaMarkupService materialFaixaMarkupService) {this.materialFaixaMarkupService = materialFaixaMarkupService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setParametroGeralService(ParametrogeralService parametroGeralService) {this.parametroGeralService = parametroGeralService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setReservaService(ReservaService reservaService) {this.reservaService = reservaService;}
	public void setContaReceberService(ContareceberService contaReceberService) {this.contaReceberService = contaReceberService;}
	
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Pedidovenda form) throws Exception {
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		form = pedidovendaService.loadBeanForEntrada(request, form, parameters);
		request.setAttribute("LER_CODIGO_ETIQUETA_BALANCA", parametrogeralService.getBoolean(Parametrogeral.LER_CODIGO_ETIQUETA_BALANCA));
		request.setAttribute("VALIDAR_ITENSKIT_TODOSLOCAIS", parametrogeralService.getBoolean(Parametrogeral.VALIDAR_ITENSKIT_TODOSLOCAIS));
	}
	
	@Override
	protected void validateBean(Pedidovenda bean, BindException errors) {
		bean.setPneu(null);
		WebRequestContext request = NeoWeb.getRequestContext();
		
		pedidovendaService.validate(bean, errors);
		for(Object obj: errors.getAllErrors()){
			System.out.println(((ObjectError)obj).getDefaultMessage());
		}
		
		super.validateBean(bean, errors);
	}
	
	@Override
	protected Pedidovenda criar(WebRequestContext request, Pedidovenda form) throws Exception {
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		request.setAttribute("REALIZARPEDIDOVENDA", Boolean.TRUE);
		// Verifica��es E-Commerce		
		String nomeEcommerce = parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE);		
		request.setAttribute("IS_TRAYCORP", nomeEcommerce != null && nomeEcommerce.equals("TRAYCORP"));
		boolean eCommerceEnabled = parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && nomeEcommerce != null; 
		request.setAttribute("ECOMMERCE_ENABLED", eCommerceEnabled);	
		if(parameters.getCopiar() && form.getCdpedidovenda() != null){
			form = pedidovendaService.criarCopia(form);
			request.setAttribute("copiarpedidovenda", true);
			return form;
		}
		
		
		//Quando o tipo de impress�o � matricial a chamada de gera��o do relat�rio tem que ser ap�s trocar de tela na mesma janela.
		if (StringUtils.isNotBlank(parameters.getCdPedidoVendaImprimir())){
			request.setAttribute("chamada_impressao_pedidovenda", "window.location = '../../faturamento/relatorio/PedidovendaMatricial?ACAO=gerar&whereIn="+parameters.getCdPedidoVendaImprimir()+"';");
		}else{
			request.setAttribute("chamada_impressao_pedidovenda", "");
		}
		
		return super.criar(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Pedidovenda bean) throws Exception {
		pedidovendaService.salvarCrud(request, bean);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request,	Pedidovenda bean) {
		boolean coletaautomatica = false;
		boolean producaoautomatica = false;
		try {
			if(request.getAttribute("coletaautomatica") != null){
				coletaautomatica = (Boolean) request.getAttribute("coletaautomatica");
			}
			if(request.getAttribute("producaoautomatica") != null){
				producaoautomatica = (Boolean) request.getAttribute("producaoautomatica");
			}
			
		} catch (Exception e) {}
		
		if(coletaautomatica || producaoautomatica){
			String url = "/faturamento/crud/Pedidovenda?ACAO=consultar&cdpedidovenda=" + bean.getCdpedidovenda() +
							"&coletaautomatica=" + coletaautomatica + 
							"&producaoautomatica=" + producaoautomatica;
			return new ModelAndView("redirect:"+url);
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, PedidovendaFiltro filtro) throws CrudException {
		filtro.setNaoexibirresultado(null);
		Usuario usuario = SinedUtil.getUsuarioLogado();
		try {
			String hash = SHA512Util.encrypt(usuario.getLogin() + "|#$" + usuario.getPassword());
			System.out.println(hash);
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(usuario.getLogin()+"-"+ usuario.getPassword());
		Boolean reenviarPedidovenda = Boolean.valueOf(request.getParameter("reenviarPedidovenda") != null ? request.getParameter("reenviarPedidovenda") : "false"); 
		if(reenviarPedidovenda){
			String cdpedidovendastr = request.getParameter("cdpedidovenda");
			if(cdpedidovendastr != null && !"".equals(cdpedidovendastr)){
				try {
					Integer cdpedidovenda = Integer.parseInt(cdpedidovendastr);
					pedidovendasincronizacaoService.desmarcarItensPedidovenda(new Pedidovenda(cdpedidovenda));
				} catch (Exception e) {}
			}
		}
		
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		
		String cdmaterial = request.getParameter("cdmaterialreservas");
		if(cdmaterial != null && !"".equals(cdmaterial)){
			filtro = new PedidovendaFiltro();
			filtro.setNotFirstTime(true);
			filtro.setVisualizarListagemreservas(true);
			filtro.setMaterial(materialService.load(new Material(Integer.parseInt(cdmaterial)), "material.cdmaterial, material.nome"));
			String cdempresa = request.getParameter("cdempresareservas");
			if(cdempresa != null && !"".equals(cdempresa) && !"null".equals(cdempresa)){
				filtro.setEmpresa(empresaService.load(new Empresa(Integer.parseInt(cdempresa)), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia"));
			}			
		}
		
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadForFiltro(filtro.getRegiao()));
		}
		
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			Material material = materialService.load(filtro.getMaterial(), "material.cdmaterial, material.vendapromocional");
			if(material != null && material.getVendapromocional() != null && material.getVendapromocional()){
				filtro.getMaterial().setVendapromocional(material.getVendapromocional());
			}
		}
		//realizando valida��o para evitar exce��es
		if(filtro.getWhereIncdpedidovenda() != null){
			String [] listcdvenda = filtro.getWhereIncdpedidovenda().split(",");
			String whereIn = "";
			Boolean contemErro = false;
			if(listcdvenda.length > 0){
				for (String cdpedido : listcdvenda) {
					try{
						if(StringUtils.isNumeric(cdpedido.trim()) && Integer.parseInt(cdpedido.trim()) < Integer.MAX_VALUE){
							whereIn += cdpedido + ",";
						}else{
							contemErro = true;
							break;
						}
					}catch(NumberFormatException n){						
						contemErro=true;
						break;
					}
				}
			}
			if(!contemErro){
				filtro.setWhereIncdpedidovenda(whereIn.substring(0, whereIn.length() - 1));
			}else{
				filtro.setWhereIncdpedidovenda(null);
				request.addError("Favor preencher com n�mero(s), podendo conter v�rgula(s).");
			}
		}
		
		if(filtro.getListaPedidovendasituacaoecommerce() != null && filtro.getListaPedidovendasituacaoecommerce().size() > 0){
			filtro.setListaPedidovendasituacaoecommerce(pedidovendasituacaoecommerceService.loadForFiltro(CollectionsUtil.listAndConcatenate(filtro.getListaPedidovendasituacaoecommerce(), "cdpedidovendasituacaoecommerce", ",")));
		}
		
		return super.doListagem(request, filtro);
	}
	
	
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Pedidovenda form) throws CrudException {
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		if(!Boolean.TRUE.equals(request.getAttribute(CONSULTAR))){
			pedidovendaService.validateEditar(request, form, parameters);
		}
		
		List<String> errors = SinedUtil.getMessageError(request.getMessages());
		if(SinedUtil.isListNotEmpty(errors) && !Boolean.TRUE.equals(request.getAttribute(CONSULTAR))){
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=consultar&cdpedidovenda=" + form.getCdpedidovenda());
		}
		// Verifica��es E-Commerce		
		String nomeEcommerce = parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE);		
		request.setAttribute("IS_TRAYCORP", nomeEcommerce != null && nomeEcommerce.equals("TRAYCORP"));
		boolean eCommerceEnabled = parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && nomeEcommerce != null; 
		request.setAttribute("ECOMMERCE_ENABLED", eCommerceEnabled);	
		return super.doEditar(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		List<Pedidovendasituacao> listaPedidoVendasituacao = new ArrayList<Pedidovendasituacao>();
		List<ProducaoagendasituacaoEnum> listaProducaoagendasituacao = new ArrayList<ProducaoagendasituacaoEnum>();
		List<SituacaoPendencia> listaSituacaoPendencia = new ArrayList<SituacaoPendencia>();
		
		String vendasaldoproduto = parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTO);
		if(vendasaldoproduto.toUpperCase().equals("TRUE")){
			request.setAttribute("VENDASALDOPRODUTO", Boolean.TRUE);
		}else {
			request.setAttribute("VENDASALDOPRODUTO", Boolean.FALSE);
		}
		
		Boolean confirmarPercentualPedidoVenda = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.CONFIRMARPERCENTUALPEDIDOVENDA));
		request.setAttribute("CONFIRMARPERCENTUALPEDIDOVENDA", confirmarPercentualPedidoVenda);
		
		Boolean confirmarPvServicoProduto = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.CONFIRMAR_PV_SERVICO_PRODUTO));
		request.setAttribute("CONFIRMAR_PV_SERVICO_PRODUTO", confirmarPvServicoProduto);
		
		Pedidovendasituacao[] situacoes = Pedidovendasituacao.values();
		for (int i = 0; i < situacoes.length; i++) {
				listaPedidoVendasituacao.add(situacoes[i]);
		}
		request.setAttribute("listaPedidoVendasituacao", listaPedidoVendasituacao);
		
		SituacaoPendencia[] arraySituacaoPendencia = SituacaoPendencia.values();
		for (SituacaoPendencia pendencia : arraySituacaoPendencia ) {
				if(!SituacaoPendencia.PEDIDO_PREVISTO.equals(pendencia)){
					listaSituacaoPendencia.add(pendencia);
				}
		}
		request.setAttribute("listaSituacaoPendencia", listaSituacaoPendencia);
	
		listaProducaoagendasituacao.add(ProducaoagendasituacaoEnum.EM_PRODUCAO);
		listaProducaoagendasituacao.add(ProducaoagendasituacaoEnum.PRODUZIDO);
		listaProducaoagendasituacao.add(ProducaoagendasituacaoEnum.CANCELADA);
		request.setAttribute("listaProducaoagendasituacao", listaProducaoagendasituacao);
		
		request.setAttribute("listaProjetos", projetoService.findForComboByUsuariologado());
		
		Empresa empPrincipal = empresaService.loadPrincipal();
		if(empPrincipal != null){
			Boolean isWms = empresaService.isIntegracaoWms(empPrincipal);
			if (isWms != null && isWms){
				request.getSession().setAttribute("integracao", true);
			}else {
				request.getSession().setAttribute("integracao", false);
			}
		}	
		if(filtro.getPedidovendatipo() != null && filtro.getPedidovendatipo().getCdpedidovendatipo() != null){
			request.setAttribute("listaCampoExtra", campoextrapedidovendatipoService.findByTipo(filtro.getPedidovendatipo()));
		}else{
			request.setAttribute("listaCampoExtra", Collections.emptyList());
		}

		String confirmacaoDiretaPedido = parametrogeralService.getValorPorNome(Parametrogeral.CONFIRMACAO_DIRETA_PEDIDO);
		request.setAttribute("negociarPendencia", parametrogeralService.getBoolean("PERMITIR_NEGOCIAR_PENDENCIA_PEDIDO"));
		request.setAttribute("confirmacaoDiretaPedido", Boolean.valueOf(StringUtils.isNotEmpty(confirmacaoDiretaPedido) ? confirmacaoDiretaPedido : "false"));
		request.setAttribute("listaCategoriaCliente", categoriaService.findByTipo(true, false));
		request.setAttribute("listaDocumentotipo", documentotipoService.getListaDocumentoTipoUsuarioForVenda(null));
		request.setAttribute("IS_CONFIRMAR_VARIOS", parametrogeralService.getBoolean(Parametrogeral.PERMITIR_CONFIRMAR_VARIOS_PEDIDOS));
		request.setAttribute("INTEGRACAO_ECOMMERCE", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE));
		request.setAttribute("listaPedidovendasituacaoecommerce", pedidovendasituacaoecommerceService.findAll());
		request.setAttribute("isTrayCorp", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && SinedUtil.isIntegracaoTrayCorp());
		super.listagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<Pedidovenda> getLista(WebRequestContext request, PedidovendaFiltro filtro) {
		ListagemResult<Pedidovenda> listagemResult = super.getLista(request, filtro); 
		List<Pedidovenda> lista = listagemResult.list();
		
		Money aux_totalgeral = new Money(0);
		Money aux_totalgeralipi = new Money(0);
		Money aux_saldoprodutos = new Money(0);
		Double pesototal = 0.0;
		Double pesobrutototal = 0.0;
		boolean exibiripivenda = false;
		boolean exibirIcmsVenda = false;
		boolean exibirDifalVenda = false;
		if(filtro.getEmpresa() == null){
			exibiripivenda = empresaService.existeEmpresaExibirIpi();
			exibirIcmsVenda = empresaService.existeEmpresaExibirIcms();
			exibirDifalVenda = empresaService.existeEmpresaExibirDifal();
		}
		
		if(lista != null && lista.size() > 0){
			String solicitarcompravenda = parametrogeralService.buscaValorPorNome(Parametrogeral.SOLICITARCOMPRAVENDA);
			String whereInPedidovenda = CollectionsUtil.listAndConcatenate(lista, "cdpedidovenda", ",");
			
			List<Ecom_PedidoVenda> listaEcom_PedidoVenda = ecom_PedidoVendaService.findByWhereInPedidovenda(whereInPedidovenda);
			List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPedidovenda);
			
			Map<Integer, Pedidovendasituacaoecommerce> mapSituacaoEcommerce = new HashMap<Integer, Pedidovendasituacaoecommerce>();
			if(listaEcom_PedidoVenda != null && listaEcom_PedidoVenda.size() > 0){
				List<Pedidovendasituacaoecommerce> listaPedidovendasituacaoecommerce = pedidovendasituacaoecommerceService.findAll();
				for (Pedidovendasituacaoecommerce pedidovendasituacaoecommerce : listaPedidovendasituacaoecommerce) {
					if(StringUtils.isNotBlank(pedidovendasituacaoecommerce.getIdsecommerce())){
						String[] ids = pedidovendasituacaoecommerce.getIdsecommerce().split(",");
						for (String id : ids) {
							try {
								mapSituacaoEcommerce.put(Integer.parseInt(id), pedidovendasituacaoecommerce);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
			
			for (Pedidovenda pedidovenda : lista) {
				if(!exibiripivenda && pedidovenda.getEmpresa() != null && pedidovenda.getEmpresa().getExibiripivenda() != null &&
						pedidovenda.getEmpresa().getExibiripivenda()){
					exibiripivenda = true;
				}
				
				Double pesototalVenda = 0d;
				Double pesobrutoTotalVenda = 0d;
				
				pedidovenda.setExibeIconeNaoVisualizadoEcommerce(pedidovendaService.isExibeIconeNaoVisualizadoEcommerce(pedidovenda));
				
				pedidovenda.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(pedidovenda));
				pedidovenda.setListaPedidovendamaterialmestre(pedidovendamaterialmestreService.findByPedidovenda(pedidovenda));
				aux_totalgeral = aux_totalgeral.add(pedidovenda.getTotalvenda());
				aux_totalgeralipi = aux_totalgeralipi.add(pedidovenda.getTotalvendaMaisImpostos());
				aux_saldoprodutos = aux_saldoprodutos.add(pedidovenda.getSaldofinal());
				if("ORDEMCOMPRA".equalsIgnoreCase(solicitarcompravenda)) {
					if(pedidovenda.getSituacaopedidosolicitacaocompra() == null) {
						pedidovenda.setSituacaopedidosolicitacaocompra(ordemcompraService.getSituacaoOrdemcompraByPedidovenda(pedidovenda));
					}
				}else if("REQUISICAOMATERIAL".equalsIgnoreCase(solicitarcompravenda)) {
					if(pedidovenda.getSituacaopedidosolicitacaocompra() == null) {
						pedidovenda.setSituacaopedidosolicitacaocompra(requisicaomaterialService.getSituacaoRequisicaomaterialByPedidovenda(pedidovenda));
					}
				}else {
					pedidovenda.setSituacaopedidosolicitacaocompra(solicitacaocompraService.getSituacaoSolcicitacaocompraByPedidovenda(pedidovenda));
				}
				
				if(listaColeta != null && listaColeta.size() > 0){
					for (Coleta coleta : listaColeta) {
						if(coleta.getPedidovenda() != null && 
								coleta.getPedidovenda().getCdpedidovenda() != null && 
								coleta.getPedidovenda().getCdpedidovenda().equals(pedidovenda.getCdpedidovenda())){
							pedidovenda.setExisteColeta(Boolean.TRUE);
							break;
						}
					}
				}
				
				if(listaEcom_PedidoVenda != null && listaEcom_PedidoVenda.size() > 0){
					for (Ecom_PedidoVenda ecom_PedidoVenda : listaEcom_PedidoVenda) {
						if(ecom_PedidoVenda.getId_situacao() != null &&
								ecom_PedidoVenda.getNome_situacao() != null &&
								ecom_PedidoVenda.getPedidoVenda() != null && 
								ecom_PedidoVenda.getPedidoVenda().getCdpedidovenda() != null && 
								ecom_PedidoVenda.getPedidoVenda().getCdpedidovenda().equals(pedidovenda.getCdpedidovenda())){
							Integer id_situacao = ecom_PedidoVenda.getId_situacao();
							if(id_situacao != null && mapSituacaoEcommerce.containsKey(id_situacao)){
								pedidovenda.setPedidovendasituacaoecommerce(mapSituacaoEcommerce.get(id_situacao));
							}
							
							String nome_situacao = ecom_PedidoVenda.getNome_situacao();
							pedidovenda.setNomeSituacaoEcommerce(nome_situacao);
							break;
						}
					}
				}
				
				if(pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
					for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
						if(pvm.getQuantidade() != null && pvm.getMaterial() != null){
							Double fatorconversaoQtdereferencia = 1d;
							if(pvm.getMaterial().getUnidademedida() != null && 
									pvm.getUnidademedida() != null && 
									!pvm.getUnidademedida().equals(pvm.getMaterial().getUnidademedida())){
								fatorconversaoQtdereferencia = pvm.getFatorconversaoQtdereferencia();
							}
							
							if(pvm.getPeso() != null){
								pesototal += pvm.getPeso() * pvm.getQuantidade();
							}else if(pvm.getMaterial().getPeso() != null){
								Double peso = pvm.getMaterial().getPeso();
								if(fatorconversaoQtdereferencia != null && fatorconversaoQtdereferencia != 0){
									peso = peso / fatorconversaoQtdereferencia;
								}
								pesototal += peso * pvm.getQuantidade();
							}
							if(pvm.getMaterial().getPesobruto() != null){
								Double pesobruto = pvm.getMaterial().getPesobruto();
								if(fatorconversaoQtdereferencia != null && fatorconversaoQtdereferencia != 0){
									pesobruto = pvm.getMaterial().getPesobruto() / fatorconversaoQtdereferencia;
								}
								pesobrutototal += pesobruto * pvm.getQuantidade();
							}
						}
					}
					
					pesototal += SinedUtil.round(pesototalVenda, 2);
					pesobrutototal += SinedUtil.round(pesobrutoTotalVenda, 2);
				}
				pedidovenda.setProducaoagendasituacao(producaoagendaService.getProducaoagendasituacaoByPedidovenda(pedidovenda));
				
				if(parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE) &&
						pedidovenda.getLimitecreditoexcedido() != null && pedidovenda.getLimitecreditoexcedido()){
					pedidovenda.setValorSupeiorLimiteCredito(Boolean.TRUE);
				}
				
				pedidovendaService.verificaContasFinanceiroSitua�ao(pedidovenda);
			}
		}
		filtro.setTotalgeral(aux_totalgeral);
		filtro.setTotalgeralipi(aux_totalgeralipi);
		filtro.setSaldoprodutos(aux_saldoprodutos);
		filtro.setPesototal(pesototal != null && !Double.isNaN(pesototal) ? SinedUtil.round(pesototal, 2) : 0.0);
		filtro.setPesobrutototal(pesobrutototal != null && !Double.isNaN(pesobrutototal) ? SinedUtil.round(pesobrutototal, 2) : 0.0);
		
		request.setAttribute("EXIBIR_IPI_NA_VENDA", exibiripivenda);
		request.setAttribute("EXIBIR_ICMS_NA_VENDA", exibirIcmsVenda);
		request.setAttribute("EXIBIR_DIFAL_NA_VENDA", exibirDifalVenda);
		request.setAttribute("EXIBIR_CALCULO_IMPOSTOS_NA_VENDA", exibiripivenda || exibirIcmsVenda || exibirDifalVenda);
		request.setAttribute("EXIBIR_IMPOSTO_NA_VENDA", exibiripivenda || exibirIcmsVenda || exibirDifalVenda);
		return listagemResult;
	}
	
	/**
	 *	Action que faz a confirma��o do pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView confirmar(WebRequestContext request){
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		pedidovendaService.validateConfirmar(request, parameters);
		List<String> errors = SinedUtil.getMessageError(request.getMessages());
		if (SinedUtil.isListNotEmpty(errors)) {
			return sendRedirectToAction("listagem");
		}
				
		Integer percentual = parameters.getPercentualPedidoVenda();		
		if(percentual == null) percentual = 100;
		
		boolean confimacaoDireta = parametrogeralService.getBoolean(Parametrogeral.CONFIRMACAO_DIRETA_PEDIDO);
		boolean confimarVarios = parametrogeralService.getBoolean(Parametrogeral.PERMITIR_CONFIRMAR_VARIOS_PEDIDOS);
		
		if(confimarVarios || (confimacaoDireta && !parameters.getExisteReserva() && !parameters.getExisteGrade())){
			String whereInPedidoEmProcessamento = (String) request.getSession().getAttribute("confirmarVariosPedidovendaWhereInPedidoVenda");
			if(StringUtils.isNotEmpty(whereInPedidoEmProcessamento)){
				for(String idSelecionado : parameters.getSelectedItens().split(",")){
					for(String idEmProcessamento : whereInPedidoEmProcessamento.split(",")){
						if(idSelecionado.equals(idEmProcessamento)){
							request.addError("Confirma��o ainda em processamento.");
							return sendRedirectToAction("listagem");
						}
					}
				}
			}
			
			// todo: aqui confirma v�rios pedidos de venda, enviar para o ecommerce;
			pedidovendaService.confirmarVariosPedidovenda(request, parameters.getSelectedItens(), percentual);
			return null;
		}
		
		return new ModelAndView("redirect:/faturamento/process/RealizarVenda?orcamento=false&cdpedidovenda=" + parameters.getSelectedItens() + "&percentualPedidovenda=" + percentual + 
				"&somenteservico="+parameters.getSomenteServico().booleanValue() + "&&somenteproduto="+parameters.getSomenteProduto().booleanValue());
	}
	
	/**
	 * Action que faz a aprova��o do pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView aprovar(WebRequestContext request){
		String itens = request.getParameter("selectedItens");
		if(!pedidovendaService.validateAprovar(request, itens)){
			if(request.getAttribute("redirectToEntrada") != null && "true".equals(request.getAttribute("redirectToEntrada"))){
				return continueOnAction("consultar", new Pedidovenda(Integer.parseInt(itens)));
			}
			return sendRedirectToAction("listagem");
		}
		
		return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=editar&showCancelLink=false&aprovarPedidovenda=true&cdpedidovenda=" + itens);
	}
	
	/**
	 * Action que faz a solicita��o de compra do pedido de venda.
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarSolicitacaocompra(WebRequestContext request){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}

		if(pedidovendaService.havePedidovendaDiferenteSituacao(itens, Pedidovendasituacao.PREVISTA)){
			request.addError("Pedido(s) de venda com situa��o(��es) diferente de 'PREVISTA'.");
			return sendRedirectToAction("listagem");
		}
		String solicitarCompraVenda = parametrogeralService.getValorPorNome(Parametrogeral.SOLICITARCOMPRAVENDA);
		if(solicitarCompraVenda != null){
			if("SOLICITACAOCOMPRA".equalsIgnoreCase(solicitarCompraVenda)) {
				return new ModelAndView("redirect:/suprimento/crud/Solicitacaocompra?ACAO=criar&whereInCdpedidovenda=" + itens);
			} else if("ORDEMCOMPRA".equalsIgnoreCase(solicitarCompraVenda)) {
				return new ModelAndView("redirect:/suprimento/crud/Ordemcompra?ACAO=criar&whereInPedidovenda=" + itens);
			} else if("REQUISICAOMATERIAL".equalsIgnoreCase(solicitarCompraVenda)) {
				return new ModelAndView("redirect:/suprimento/crud/Requisicaomaterial?ACAO=criar&whereInCdpedidovenda=" + itens);
			}
		}
		return new ModelAndView("redirect:/suprimento/crud/Solicitacaocompra?ACAO=criar&whereInCdpedidovenda=" + itens);
	}
	
	/**
	 *	Action que faz o cancelamento do pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cancelar(WebRequestContext request, Pedidovenda pedidovenda){
		if(!pedidovendaService.validateCancelamento(request, pedidovenda)){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		pedidovendaService.cancelar(request, pedidovenda);
		request.addMessage("Pedido(s) de venda cancelado(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
		return null;
	}
	
	public ModelAndView ajaxIsCancelarEcommerce(WebRequestContext request, Pedidovenda pedidovenda){
		boolean containsVendaEcommerce = pedidovendaService.containsPedidoEcommerce(pedidovenda.getWhereInCancelamento());
		return new JsonModelAndView().addObject("containsVendaEcommerce", containsVendaEcommerce);
	} 
	
	public void ajaxMaterial(WebRequestContext request, Material material ){
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		try {
			material = materialService.getUnidademedidaValorvenda(material);
			
			if (material.getUnidademedida() != null)
				view.println("var unidademedida = '" + material.getUnidademedida().getNome() + "';");
			else
				view.println("var unidademedida = '';");
			
			if (material.getValorvenda() != null)
				view.println("var valorvenda = '" + material.getValorvenda() + "';");
			else
				view.println("var valorvenda = '';");
			
			view.println("var sucesso = true;");						
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}
	
	
	/************ M�todos do Process ***************/
	
	/** 
	 * M�todo que salva o Pedido de Venda.
	 * 
	 * @author Thiago Augusto
	 * @param request
	 * @param pedidoVenda
	 * @return
	 */
	public ModelAndView comprar(WebRequestContext request, Pedidovenda pedidoVenda){
		if(!pedidovendaService.validateComprar(request, pedidoVenda)){
			return continueOnAction("entrada", pedidoVenda);
		}
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		ModelAndView modelAndView = pedidovendaService.comprar(request, pedidoVenda, parameters);
		if(modelAndView == null && pedidovendaService.existeErro(request)){
			return continueOnAction("entrada", pedidoVenda);
		}
		return modelAndView;
	}
	
	/**
	 * M�todo que salva o PedidoHistorico
	 * 
	 * @author Thiago Augusto
	 * @param pedidoVenda
	 */
	public void salvaPedidoHistorico(Pedidovenda pedidovenda, String observacaoHistorico){
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setAcao("Criado");
		
		String obs = pedidovenda.getObservacao();
		if(StringUtils.isNotBlank(observacaoHistorico)){
			obs = observacaoHistorico + (obs != null ? obs : "");
		}
		
		if(pedidovenda.getVendaorcamento() != null){
			pedidovendahistorico.setObservacao("Cria��o a partir do or�amento: <a href=\"javascript:visualizarVendaorcamento(" + 
					pedidovenda.getVendaorcamento().getCdvendaorcamento() + 
					");\">" + 
					pedidovenda.getVendaorcamento().getCdvendaorcamento() + 
					"</a>");
			
			if(obs != null){
				pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() + " " + obs);
			}
		}else if(StringUtils.isNotBlank(pedidovenda.getWhereInOSV())){
			StringBuilder obsHistoricoVenda = new StringBuilder();
			for(String id : pedidovenda.getWhereInOSV().split(",")){
				if(!obsHistoricoVenda.toString().equals("")) obsHistoricoVenda.append(", ");
				obsHistoricoVenda.append(" <a href=\"javascript:submitFormLoginVeterinaria('/w3erpveterinaria/veterinaria/crud/Ordemservicoveterinaria?ACAO=consultar&cdordemservicoveterinaria=" + 
						id + 
						"');\">" + 
						id + 
						"</a>");
				
			}
			pedidovendahistorico.setObservacao("Cria��o a partir da(s) Ordem(ns) de Servi�o Veterin�ria: " + obsHistoricoVenda.toString());
			if(obs != null){
				pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() + ". " + obs);
			}
		}else {
			pedidovendahistorico.setObservacao(obs);
		}
		
		if(pedidovenda.getConfirmadoparceladiferenteproduto() != null && pedidovenda.getConfirmadoparceladiferenteproduto()){
			pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() != null ? pedidovendahistorico.getObservacao() +  " (Usu�rio confirmou os valores das parcelas)" : "(Usu�rio confirmou os valores das parcelas)");
		}
		
		if(pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
			StringBuilder historicoTroca = new StringBuilder();
			for(Pedidovendamaterial vm : pedidovenda.getListaPedidovendamaterial()){
				if(vm.getHistoricoTroca() != null && !"".equals(vm.getHistoricoTroca())){
					if(!"".equals(historicoTroca.toString())) historicoTroca.append("<br>");
					historicoTroca.append(vm.getHistoricoTroca());
				}
			}
			if(!"".equals(historicoTroca.toString())){
				if(pedidovendahistorico.getObservacao() != null && !"".equals(pedidovendahistorico.getObservacao())){
					pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() + " <br>Trocas de materiais efetuadas: " + historicoTroca.toString());
				}else {
					pedidovendahistorico.setObservacao("Trocas de materiais efetuadas: " + historicoTroca.toString());
				}
			}
		}
		
		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
	}
	
	private Pedidovenda carregarInfoPedidoSituacao(Pedidovenda pedidovenda){
		pedidovenda = pedidovendaService.carregarInfoPedidoSituacao(pedidovenda);
		return pedidovenda;
	}
	private Pedidovenda dadosNegociacao(WebRequestContext request,Pedidovenda pv){
		pv = pedidovendaService.loadForNegociacao(pv);
		pv.setTotalvenda(pedidovendaService.calculaValorVenda(pv));
		Money valorVendas = new Money(0.0);
		List<Venda> listaVendas = vendaService.findByPedidovenda(pv);
		for (Venda venda : listaVendas) {
			valorVendas= valorVendas.add(vendaService.calculaValorVenda(venda));
		}
		pv.setValorVendasRezalida(valorVendas);
		if(pv.getValorPendentes().compareTo(0.0) <0){
			pv.setDtGerarValeCompra(SinedDateUtils.currentDate());
			pv.setDescricaoValeCompra("REF. A NEGOCIA��O DE PEND�NCIA DO PEDIDO "+pv.getCdpedidovenda());
			pv.setOpValeCompra(Tipooperacao.TIPO_CREDITO);
			request.setAttribute("gerarVale", true);
		}
		else{
			request.setAttribute("gerarConta", true);
		}
		return pv;
	}
	
	public ModelAndView abrirNegociarPendencia(WebRequestContext request,Pedidovenda pedidovenda){
		List<Pedidovendapagamento> listaPagamento = pedidovenda.getListaPedidovendapagamento();
		Pedidovenda pv = carregarInfoPedidoSituacao(pedidovenda);
		if(pv.getSituacaoPendencia()!=null && pv.getSituacaoPendencia().equals(SituacaoPendencia.NEGOCIACAO_PENDENTE) && Pedidovendasituacao.CONFIRMADO.equals(pv.getPedidovendasituacao())){
			pv = dadosNegociacao(request, pv);
		}
		else{
			request.addError("Para negociar uma pend�ncia, o pedido deve estar Confirmado e com Negocia��o Pendente.");
			SinedUtil.fechaPopUp(request);
			return null;
		}	
		if(SinedUtil.isListNotEmpty(listaPagamento)){
			pv.setListaPedidovendapagamento(listaPagamento);
		}
		if(pv.getValorPendentes().compareTo(0.0) <0){
			pv.setDtGerarValeCompra(SinedDateUtils.currentDate());
			pv.setDescricaoValeCompra("REF. A NEGOCIA��O DE PEND�NCIA DO PEDIDO "+pv.getCdpedidovenda());
			pv.setOpValeCompra(Tipooperacao.TIPO_CREDITO);
			request.setAttribute("gerarVale", true);
		}
		else{
			request.setAttribute("gerarConta", true);
		}
		request.setAttribute("EXIBIR_IPI_NA_VENDA", false);
		return new ModelAndView("direct:/crud/popup/negociarPedidoVenda", "bean", pv);
	}
	
	public ModelAndView negociarPendencia(WebRequestContext request,Pedidovenda pedidovenda){	
		List<Pedidovendapagamento> listaPagamento = pedidovenda.getListaPedidovendapagamento();
		Indicecorrecao indice = pedidovenda.getIndicecorrecao();
		Pedidovenda pv = carregarInfoPedidoSituacao(pedidovenda);
		if(pv.getSituacaoPendencia()!=null && pv.getSituacaoPendencia().equals(SituacaoPendencia.NEGOCIACAO_PENDENTE) && Pedidovendasituacao.CONFIRMADO.equals(pv.getPedidovendasituacao())){
			pv = dadosNegociacao(request, pv);
			pv.setListaPedidovendapagamento(listaPagamento);
			pv.setIndicecorrecao(indice);
			if(SinedUtil.isListNotEmpty(listaPagamento)){
				Money valorTotal = new Money();
				for (Pedidovendapagamento pg : listaPagamento) {
					valorTotal = valorTotal.subtract(pg.getValororiginal());
				}
				Money vPendente = pv.getValorPendentes();
				if(valorTotal.compareTo(vPendente) != 0){
					request.setAttribute("erroValor", "true");
					return abrirNegociarPendencia(request,pedidovenda);
				}
			}
			if(pedidovenda.getListaPedidovendapagamento() !=null && SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendapagamento())){
				for (Pedidovendapagamento pagamento : pedidovenda.getListaPedidovendapagamento()) {
					if(pagamento.getDocumentotipo() == null){
						request.setAttribute("erroPagamento", "true");
						return abrirNegociarPendencia(request,pedidovenda);
					}else if(pedidovenda.getConta() ==null){
						pedidovenda.setDocumentotipo(documentotipoService.load(pagamento.getDocumentotipo()));
						if(Boolean.TRUE.equals(pedidovenda.getDocumentotipo().getBoleto())){
							request.setAttribute("selecionarConta", "true");
							return abrirNegociarPendencia(request,pedidovenda);
						}
					}
				}
			}else if(pv.getOpValeCompra()==null){
				request.setAttribute("selecionarPagamento", "true");
				return abrirNegociarPendencia(request,pedidovenda);
			}
			pv.setConta(pedidovenda.getConta());
			pedidovendaService.negociarPendencia(request,pv);
			pedidovendaService.updateSituacaoPendenciaNegociacao(pv, SituacaoPendencia.PENPENCIA_NEGOCIADA);
		}else{
			request.addError("Para negociar uma pend�ncia, o pedido deve estar Confirmado e com Negocia��o Pendente.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView naoNegociarPendencia(WebRequestContext request,Pedidovenda pedidovenda){
		if(!Util.objects.isPersistent(pedidovenda)){
			request.addError("Par�metro inv�lido.");
			return sendRedirectToAction("listagem");
		}
		
		Pedidovenda pv = carregarInfoPedidoSituacao(pedidovenda);
		if(pv.getSituacaoPendencia()!=null && pv.getSituacaoPendencia().equals(SituacaoPendencia.NEGOCIACAO_PENDENTE) && Pedidovendasituacao.CONFIRMADO.equals(pv.getPedidovendasituacao())){
			boolean existeDiferenca = false;
			if(parametrogeralService.getBoolean(Parametrogeral.GERAR_VENDA_AJUSTE_FATURAMENTO)){
				if(parametrogeralService.getInteger(Parametrogeral.TIPOPEDIDO_SEM_NEGOCIACAO) == null){
					request.addError("� necess�rio preencher o Tipo de Pedido no par�metro geral TIPOPEDIDO_SEM_NEGOCIACAO.");
					return sendRedirectToAction("listagem");
				}
				
				Pedidovenda beanPedidoVenda = pedidovendaService.loadForEntrada(pedidovenda);
				Money valorTotalPedido = beanPedidoVenda.getTotalvendaMaisImpostos();
				Money valorTotalVenda = vendaService.getTotalVendaByPedidoVenda(pedidovenda);
				Money diferencaTotal = valorTotalPedido.subtract(valorTotalVenda);
				if(diferencaTotal.doubleValue() > 0.10 || diferencaTotal.doubleValue() < -0.10){
					if(reservaService.existReservaPedidoVenda(pv)){
						request.addError("N�o � poss�vel gerar venda de ajuste para pedido de venda n�o negociado caso haja reserva de materiais.");
						return sendRedirectToAction("listagem");
					}
					return new ModelAndView("redirect:/faturamento/crud/Venda?ACAO=criarVendaNaoNegociada&cdpedidovendaNaoNegociada=" + pv.getCdpedidovenda());
				}else {
					request.addMessage("O pedido n�o foi negociado por n�o ter diferen�a entre o valor total do pedido e o valor total confirmado.");
					existeDiferenca = true;
				}
			}
			pedidovendaService.registrarHistoricoParaAcoesDeNegociacao(pv,"N�o negociado","O pedido n�o foi negociado.");
			pedidovendaService.updateSituacaoPendenciaNegociacao(pv, SituacaoPendencia.PEDENCIA_RESOLVIDA_SEM_NEGOCIACAO);
			
			if(!existeDiferenca){
				request.addMessage("Negocia��o desfeita com sucesso.");
			}
		}else{
			request.addError("S� � poss�vel executar a a��o N�o Negociar Pend�ncia quando o pedido estiver Confirmado e com status de Negocia��o Pendente.");
		}	
		return sendRedirectToAction("listagem");	
	}
	
	/**
	 * 
	 * Variavel sucesso valida o retorno do processo.
	 * Em caso de erro ela exibe a mensagem cadastrada no caso de uso, igual a o seu valor.
	 * 
	 * @param request
	 * @param pedidovenda
	 * 
	 */
	public ModelAndView desfazerNegociacao(WebRequestContext request,Pedidovenda pedidovenda){
		Pedidovenda pv = carregarInfoPedidoSituacao(pedidovenda);
		String retorno = "";
		if(pv.getSituacaoPendencia()!=null &&(pv.getSituacaoPendencia().equals(SituacaoPendencia.PENPENCIA_NEGOCIADA) || pv.getSituacaoPendencia().equals(SituacaoPendencia.PEDENCIA_RESOLVIDA_SEM_NEGOCIACAO))&& Pedidovendasituacao.CONFIRMADO.equals(pv.getPedidovendasituacao())){
			if(pv.getSituacaoPendencia().equals(SituacaoPendencia.PEDENCIA_RESOLVIDA_SEM_NEGOCIACAO)){
				pedidovendaService.updateSituacaoPendenciaNegociacao(pv, SituacaoPendencia.NEGOCIACAO_PENDENTE);
			}else{
				pv = dadosNegociacao(request, pv);
				retorno = pedidovendaService.desfazerNegociacao(request, pv);
				if(retorno.equals("Negocia��o desfeita com sucesso")){
					request.addMessage(retorno);
				}else{
					request.addError(retorno);
				}
			}
		}else{
			request.addError("N�o � permitido desfazer a negocia��o de pend�ncias de pedidos com situa��o diferente de confirmada ou diferente de Pend�ncia Negociada ou Pend�ncia Resolvida Sem Negocia��o.");
		}	
		return sendRedirectToAction("listagem");	
	}
	
	
	public ModelAndView finalizarPedidoParcial(WebRequestContext request,Pedidovenda pedidovenda){
		Pedidovenda pv = carregarInfoPedidoSituacao(pedidovenda);
		boolean isErro = false; 
		if(!Pedidovendasituacao.CONFIRMADO_PARCIALMENTE.equals(pv.getPedidovendasituacao())){
			request.addError("N�o � poss�vel finalizar o pedido confirmado parcial para pedido de venda com situa��o diferente de Confirmado Parcial.");
			isErro =true;
		}
		if(pv.getPedidovendatipo() != null){
			if(BaixaestoqueEnum.APOS_EXPEDICAOWMS.equals(pv.getPedidovendatipo().getBaixaestoqueEnum())){
				request.addError("N�o � poss�vel finalizar o pedido do tipo de pedido de venda definido para baixar o estoque Ap�s expedi��o no WMS.");
				isErro =true;
			}
		}	
		if(!isErro){
			pedidovendaService.finalizarPedidoParcial(pv);
			request.addMessage("Pedido finalizado com sucesso");
			if(parametrogeralService.getBoolean("PERMITIR_NEGOCIAR_PENDENCIA_PEDIDO") && GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(pv.getPedidovendatipo().getGeracaocontareceberEnum())){
				pedidovendaService.updateSituacaoPendenciaNegociacao(pv, SituacaoPendencia.NEGOCIACAO_PENDENTE);
			}
		}
		
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView pagamento(WebRequestContext request, Pedidovenda pedidovenda){	
		return pedidovendaService.pagamento(request, pedidovenda, request.getParameter("dtprazoentrega"));
	}
	
	
	
	/**
	 * M�todo ajax para buscar prazopagamento de acordo com o par�metro
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaPrazopagamento(WebRequestContext request, Pedidovenda bean){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		Boolean prazomedio = Boolean.TRUE.equals(bean.getPrazomedio());
		List<Prazopagamento> listaPrazopagamento = clienteService.findPrazoPagamento(bean.getCliente(), prazomedio, false);
		
		PrazopagamentoFiltro filtro = new PrazopagamentoFiltro();
		filtro.setOrderBy("prazopagamento.nome");
		filtro.setAsc(true);
		ordenaPrazoPagamento(filtro, listaPrazopagamento);
		
		view.println(SinedUtil.convertToJavaScript(listaPrazopagamento, "listaprazopagamento", ""));		
	}
	
	/**
	 * M�todo que busca informa��o do prazo de pagamento
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaInfoPrazopagamento(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		Boolean prazomedio = false;
		String cdprazopagamentostr = request.getParameter("prazopagamento");
		if(StringUtils.isNotBlank(cdprazopagamentostr)){
			ModelAndView modelAndView = pedidovendaService.ajaxBuscaInfoPrazopagamento(new Prazopagamento(Integer.parseInt(cdprazopagamentostr)));
			if(modelAndView != null && modelAndView.getModel() != null){
				prazomedio = (Boolean)modelAndView.getModel().get("prazoMedio");
			}
			
		}
		view.println("var prazomedio = '" + prazomedio + "';");		
	}
	
	public void buscarMaterialAJAX(WebRequestContext request, Venda venda){
		ModelAndView modelAndView = pedidovendaService.buscarMaterialAJAXJson(request, venda, false);
		View view = View.getCurrent();
		Map mapa = modelAndView.getModel();
		System.out.println(mapa.get("percentualcomissaoagencia"));
		System.out.println(mapa.get("listaMaterialUnidademedida"));
		if(mapa.containsKey("alturas") || mapa.containsKey("comprimentos") || mapa.containsKey("larguras")){
			view.println("var comprimentos = '" + mapa.get("comprimentos") + "'; ");
			view.println("var alturas = '" + mapa.get("alturas") + "'; ");
			view.println("var larguras = '" + mapa.get("larguras") + "'; ");
//			view.println("var fatorconversaocomprimento = '" + (produto.getFatorconversao() != null ? produto.getFatorconversao() : "") + "'; ");
			view.println("var fatorconversaocomprimento = ''; ");
			view.println("var margemarredondamento = '" + mapa.get("margemarredondamento") + "'; ");
		}
		view.println("var exibirmaterialcoleta = '" + mapa.get("exibirmaterialcoleta") + "';");
		view.println("var materialcoleta_id = '" + mapa.get("materialcoleta_id") + "';");
		view.println("var materialcoleta_label = \"" + mapa.get("materialcoleta_label") + "\";");
		if(mapa.containsKey("exibirMaterialcoleta")){
			view.println("var existematerialsimilarColeta = '" + mapa.get("exibirexistematerialsimilarColeta") + "';");
		}
		view.println("var existecontroleGrade = '" + mapa.get("existecontroleGrade") + "';");
		view.println("var existeitemgrade = '" + mapa.get("existeitemgrade") + "';");
		view.println("var metrocubicovalorvenda = '" + mapa.get("metrocubicovalorvenda") + "';");
		view.println("var identificacaomaterial = '" + mapa.get("identificacaomaterial") + "';");
		view.println("var multiplicador = '" + mapa.get("multiplicador") + "';");
		view.println("var showBotaoLargCompAlt = '" + mapa.get("showBotaoLargCompAlt") + "'; ");
		view.println("var existMaterialsimilar = '" + mapa.get("existMaterialsimilar") + "'; ");
		view.println("var vendapromocional = '" + mapa.get("vendapromocional")+ "';");
		view.println("var kitflexivel = '" + mapa.get("kitflexivel")+ "';");
		view.println("var valorproduto = '" + mapa.get("valorproduto") + "';");
		view.println("var valorminimo = '" + mapa.get("valorminimo") + "';");
		view.println("var valormaximo = '" + mapa.get("valormaximo") + "';");
		view.println("var percentualdescontoTabelapreco ='" + mapa.get("percentualdescontoTabelapreco") + "';");
		view.println("var tabelaprecoComPrazopagamento = '" + mapa.get("tabelaprecoComPrazopagamento") + "';");
		view.println("var naoexibirminmaxvenda = '" + mapa.get("naoexibirminmaxvenda") + "';");
		view.println("var qtddisponivel = '" + mapa.get("qtddisponivel") + "';");
		view.println("var showqtddisponivelAcimaestoque = '" + mapa.get("showqtddisponivelAcimaestoque") + "';");
		view.println("var qtddisponivelAcimaestoque = '" + mapa.get("qtddisponivelAcimaestoque") + "';");
		view.println("var msgacimaestoque = '" + mapa.get("msgacimaestoque") + "';");
		view.println("var pesquisasomentemestre = '" + mapa.get("pesquisasomentemestre") + "';");
		view.println("var quantidadeDisponivelMenosReservada = '" + mapa.get("quantidadeDisponivelMenosReservada") + "';");
				
		view.println("var validaestoque ='" + mapa.get("validaestoque") + "';");
		view.println("var materialservico ='" + mapa.get("materialservico") + "';");
		view.println("var exibiritensproducaovenda ='" + mapa.get("exibiritensproducaovenda") + "';");
		view.println("var exibiritenskitvenda ='" + mapa.get("exibiritenskitvenda") + "';");
		view.println("var producao ='" + mapa.get("producao") + "';");
		view.println("var qtdereservada ='" + mapa.get("qtdereservada") + "';");
		view.println("var qtdeunidade = '" + mapa.get("qtdeunidade") + "'; ");
		view.println("var considerarvendamultiplos = '" + mapa.get("considerarvendamultiplos") + "'; ");
		view.println("var valorcustomaterial = '" + mapa.get("valorcustomaterial")+ "';");
		view.println("var valorvendamaterial = '" + mapa.get("valorvendamaterial")+ "';");
		view.println("var pesoliquidovalorvenda = '" + mapa.get("pesoliquidovalorvenda") + "';");
		view.println("var peso = '" + mapa.get("peso") + "';");
		view.println("var pesobruto = '" + mapa.get("pesobruto") + "';");
		view.println("var tributacaoipi ='" + mapa.get("tributacaoipi") + "';");
		view.println("var materialobrigalote = '" + mapa.get("materialobrigalote") + "';");
		view.println("var dtMenorVencimentoLote = '" + mapa.get("dtMenorVencimentoLote") + "'; ");
		
		view.println("var pneumedida_id = \"" + mapa.get("pneumedida_id") + "\";");
		view.println("var pneumedida_label = \"" + mapa.get("pneumedida_labl") + "\";");
		view.println("var materialbanda_id = \"" + mapa.get("materialbanda_id") + "\";");
		view.println("var materialbanda_label = \"" + mapa.get("materialbanda_label") + "\";");
		view.println("var pneuSegmento_id = \"" + mapa.get("pneuSegmento_id") + "\";");
		view.println("var pneuSegmento_label = \"" + mapa.get("pneuSegmento_label") + "\";");
		
		view.println("var cdarquivo = '" + mapa.get("cdarquivo") + "';");
		if(mapa.containsKey("anotacoes")){
			view.println("var anotacoes = \"" + mapa.get("anotacoes") + "\"; ");
		}
		else view.println("var anotacoes = '" + "" + "';");
		
		view.println("var listaMaterialUnidademedida = " + mapa.get("listaMaterialUnidadeMedida") + ";");
		view.println("var unidademedidaMaterial = '" + mapa.get("unidadeMedidaMaterial") + "';");
		view.println("var percentualcomissaoagencia = '" + mapa.get("percentualComissaoAgencia") + "';");
		view.println("var abrirPopupPneu = '" + mapa.get("abrirPopupPneu") + "';");
		
		view.println("var obrigarLoteVenda = '" + mapa.get("obrigarLoteVenda") + "';");
		view.println("var obrigarLotePedidovenda = '" + mapa.get("obrigarLotePedidovenda") + "';");
	}

	
	public ModelAndView abrirPopUpCalcularLargCompAlt(WebRequestContext request){
		Produto produto = new Produto();
		
		String cdmaterial = request.getParameter("cdmaterial");
		
		if(cdmaterial != null && !"".equals(cdmaterial)){
			produto.setCdmaterial(Integer.parseInt(cdmaterial));
			produto = produtoService.carregaAlturaComprimentoLargura(produto);
		}		
		
		return new ModelAndView("direct:/crud/popup/popUpCalcularLargCompAlt", "bean", produto);
	}
	
	public ModelAndView abrirPopupSelecaoPneus(WebRequestContext request) throws Exception{
		String whereIn = request.getParameter("whereInPedidoVenda");
		
		if(StringUtils.isBlank(whereIn)){
			request.addError("erro ao selecionar");
			return sendRedirectToAction("listagem"); 
		}
		
		return pedidovendaService.abrirPopupSelecaoPneus(request, whereIn);
	}
	
	public ModelAndView possuiListaTemplates(WebRequestContext request) throws Exception{
		List<ReportTemplateBean> listaReport = emitirFichaColetaReport.buscarlisaTemplates();
		boolean existeTemplate = SinedUtil.isListNotEmpty(listaReport);
		boolean exibePopup = listaReport != null && listaReport.size() > 1 ? true : false;
		
		return new JsonModelAndView().addObject("exibePopup", exibePopup).addObject("existeTemplate", existeTemplate);
	}
	
	/**
	 * M�todo ajax que busca as informa��es do material promocional
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadMaterialPromocionalComMaterialrelacionado(Integer cdmaterial)
	 *
	 * @param request
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView buscarInfMaterialPromocionalAJAX(WebRequestContext request, Venda venda){	
		return vendaService.buscarInfMaterialPromocionalAJAX(request, venda);
	}
	
	public ModelAndView buscarInfMaterialProducaoAJAX(WebRequestContext request, Pedidovenda pedidovenda){	
		return vendaService.buscarInfMaterialProducaoAJAX(request, pedidovenda);
	}
	
	public ModelAndView recalcularMaterialProducaoAJAX(WebRequestContext request, Pedidovenda pedidovenda){
		Material material = materialService.loadMaterialComMaterialproducao(new Material(Integer.parseInt(pedidovenda.getCodigo())));
		pedidovenda.setMaterial(material);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
		
		pedidovendaService.trocarMaterialProducao(material, pedidovenda);
		
		if(material.getProducao()){
			Pedidovendamaterialmestre vmm = new Pedidovendamaterialmestre();
			vmm.setAltura(pedidovenda.getAlturas());
			vmm.setComprimento(pedidovenda.getComprimentos());
			vmm.setLargura(pedidovenda.getLarguras());
			vmm.setPedidovenda(pedidovenda);
			vmm.setMaterial(material);
			pedidovenda.getListaPedidovendamaterialmestre().add(vmm);
		}
		
		Double valorcusto = material.getValorcusto();
		try{
			material.setProduto_altura(pedidovenda.getAlturas());
			material.setProduto_largura(pedidovenda.getLarguras());
			material.setQuantidade(pedidovenda.getQuantidades());
			pedidovendaService.preecheValorVendaComTabelaPrecoItensProducao(material, pedidovenda.getPedidovendatipo(), pedidovenda.getCliente(), pedidovenda.getEmpresa(), pedidovenda.getPrazopagamento());
			valorcusto = materialproducaoService.getValorvendaproducao(material);			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean recapagem = SinedUtil.isRecapagem();
		
		Materialproducao materialproducaoMestre = materialproducaoService.createItemProducaoByMaterialmestre(material.getListaProducao(), material);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), true));
		
		if(recapagem){
			pedidovendaService.preecheValorVendaComTabelaPreco(material, pedidovenda.getPedidovendatipo(), pedidovenda.getCliente(), pedidovenda.getEmpresa(), material.getUnidademedida(), pedidovenda.getPrazopagamento());
			materialproducaoMestre.getMaterial().setValorvenda(material.getValorvenda());
			materialproducaoMestre.getMaterial().setValorcusto(valorcusto);
		}
		
		for (Materialproducao materialproducao : material.getListaProducao()) {
			if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && pedidovenda.getQuantidades() != null){
				materialproducao.setConsumo(materialproducao.getConsumo() * pedidovenda.getQuantidades());
				
				materialformulavalorvendaService.setValorvendaByFormula(materialproducao.getMaterial(), 
						pedidovenda.getAlturas() != null ? pedidovenda.getAlturas() : null, 
						pedidovenda.getLarguras() != null ? pedidovenda.getLarguras() : null, 
						pedidovenda.getComprimentos() != null ? pedidovenda.getComprimentos() : null,
						pedidovenda.getValorfrete() != null ? pedidovenda.getValorfrete().getValue().doubleValue() : null);
			}
		}
		if(materialproducaoMestre != null && materialproducaoMestre.getConsumo() != null && materialproducaoMestre.getConsumo() > 0 && pedidovenda.getQuantidades() != null){
			if(pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
				for(Pedidovendamaterial vendamaterial : pedidovenda.getListaPedidovendamaterial()){
					if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().equals(materialproducaoMestre.getMaterial())){
						if(vendamaterial.getDtprazoentrega() != null){
							materialproducaoMestre.setPrazoentrega(new java.util.Date(vendamaterial.getDtprazoentrega().getTime()));
						}
						materialproducaoMestre.setLoteestoque(vendamaterial.getLoteestoque());
					}
				}
			}
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", material);
		json.addObject("materialproducaoMestre", materialproducaoMestre);
		
		return json;
	}
		
	public void buscarMaterialCodigoAJAX(WebRequestContext request, Venda venda){
		if(venda != null && venda.getCodigo() != null)
			VendaService.getInstance().buscarMaterialCodigoAJAX(request, venda.getCodigo().toString());
	}
	
	public void buscarMaterialCodigoByBalancaAJAX(WebRequestContext request, Venda venda){
		if(venda != null && venda.getCodigo() != null)
			VendaService.getInstance().buscarMaterialCodigoByBalancaAJAX(request, venda.getCodigo().toString());
	}
	
	/**
	* M�todo ajax que verifica as informa��es do tipo de pedido
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#verificarPedidovendatipoAJAX(WebRequestContext request, Pedidovendatipo pedidovendatipo, Empresa empresa)
	*
	* @param request
	* @param pedidovenda
	* @since 11/12/2015
	* @author Luiz Fernando
	*/
	public void verificarPedidovendatipoAJAX(WebRequestContext request, Pedidovenda pedidovenda){
		boolean fromPedidovenda = request.getParameter("fromPedidovenda") != null ? Boolean.parseBoolean(request.getParameter("fromPedidovenda")) : false;
		boolean fromOrcamento = request.getParameter("fromOrcamento") != null ? Boolean.parseBoolean(request.getParameter("fromOrcamento")) : false;
		vendaService.verificarPedidovendatipoAJAX(request, pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa(), fromPedidovenda, fromOrcamento);
	}
	
	public void criarPagamentosPedidoVendaSemPagamento(WebRequestContext request){
		//processo utilizado somente para ajustar venda que n�o tem conta a receber
		String whereInVenda = request.getParameter("whereInVendaAjuste");
		String senha = request.getParameter("hash");
		
		String hash1 = Util.crypto.makeHashMd5("linkComAdminAjustaVenda");
		String hash2 = Util.crypto.makeHashMd5(hash1);
		
		if(hash2.equals(senha)){
		
			for(String idVenda : whereInVenda.split(",")){
				Pedidovenda form = pedidovendaService.loadForEntrada(new Pedidovenda(Integer.parseInt(idVenda)));
				
				if(form.getPedidovendatipo() != null && (Boolean.TRUE.equals(form.getPedidovendatipo().getBonificacao()) ||
						(!GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(form.getPedidovendatipo().getGeracaocontareceberEnum())))){
					continue;
				}
						
				form.setListaPedidovendapagamento(pedidovendapagamentoService.findByPedidovenda(form));
				
				if(form.getListaPedidovendapagamento() == null || form.getListaPedidovendapagamento().size() == 0) continue;
				boolean existeDoc = false;
				for(Pedidovendapagamento vendapagamento : form.getListaPedidovendapagamento()){
					if(vendapagamento.getDocumento() != null){
						existeDoc = true;
						break;
					}
				}
				if(existeDoc) continue;
				
				if (form.getColaborador()!=null && form.getColaborador().getCdpessoa()!=null){
					form.setColaborador(colaboradorService.loadWithoutPermissao(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
				}
				if (form.getLocalarmazenagem()!=null && form.getLocalarmazenagem().getCdlocalarmazenagem()!=null){
					form.setLocalarmazenagem(localarmazenagemService.load(form.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
				}
				if (form.getCliente()!=null && form.getCliente().getCdpessoa()!=null){
					form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome"));
					
					if(form.getContato() != null && form.getContato().getCdpessoa() != null){
						form.setContato(contatoService.load(form.getContato(), "contato.cdpessoa, contato.nome"));
					}
				}
				if (form.getEmpresa() !=null && form.getEmpresa().getCdpessoa()!=null){
					form.setEmpresa(empresaService.load(form.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia"));
				}
				
				form.setListaPedidovendapagamentorepresentacao(pedidovendapagamentorepresentacaoService.findPedidovendaPagamentoRepresentacaoByPedidovenda(form));
				form.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidovenda(form));
				form.setListaPedidovendahistorico(pedidovendahistoricoService.findByPedidovenda(form));
				form.setListaPedidovendamaterialmestre(pedidovendamaterialmestreService.findByPedidovenda(form));
				
				Empresa empresa = empresaService.loadComContagerenciaiscompra(form.getEmpresa());
				List<Rateioitem> listarateioitem = pedidovendaService.prepareAndSaveVendaMaterial(form, false, empresa, false, null);
				pedidovendaService.prepareAndSaveReceita(form, listarateioitem, empresa);
			}
		}
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
	}
	
	public ModelAndView recuperarVendasPorMaterial(WebRequestContext request){
		return vendaService.recuperarListaReservados(request);
	}
	
	
	
	public boolean validaTotalPedidoVenda(Pedidovenda pedidovenda, int size){	
		
		Double juros = prazopagamentoService.loadAll(pedidovenda.getPrazopagamento()).getJuros();		
		
		Money desconto = pedidovenda.getDesconto() != null ? pedidovenda.getDesconto() : new Money();
		Money frete = pedidovenda.getValorfrete() != null ? pedidovenda.getValorfrete() : new Money();
		Money valor;
		Money bean;
		Double vpValores = 0.0;
		Double vmValores = 0.0;
		Money vp_valores_money = new Money();
		
		if(juros == null || juros.equals(0d)){			  
			valor = new Money(pedidovenda.getTotalvenda()).divide(new Money(size));
			
			for(Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()){
				vp_valores_money = new Money(vp_valores_money.add(pedidovendapagamento.getValororiginal()));				
			}
			
			if((""+vp_valores_money).equals(""+(pedidovenda.getTotalvenda().add(frete).subtract(desconto)))){
				return true;
			}else return false;
		}else{
			valor = pedidovendaService.calculaJuros(new Money(pedidovenda.getTotalvenda()), size, juros);
			valor = new Money(valor.getValue().doubleValue() * size);
		}
		
		for(Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()){
			vpValores += pedidovendapagamento.getValororiginal().getValue().doubleValue();
		}
		for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
			Double descontoMat = pedidovendamaterial.getDesconto() != null ? pedidovendamaterial.getDesconto().getValue().doubleValue() : 0.0;
			vmValores += (pedidovendamaterial.getPreco() * pedidovendamaterial.getQuantidade()) - descontoMat;
		}
		bean = new Money( vmValores + (pedidovenda.getValorfrete() != null ? pedidovenda.getValorfrete().getValue().doubleValue() : 0) - desconto.getValue().doubleValue());
		
				
		if(!(bean.getValue().doubleValue() == pedidovenda.getTotalvenda().getValue().doubleValue())){
			return false;		
		}else {
			if((""+vpValores).equals(""+valor.getValue())){
				return false;
			}
		}
		
		return true;
	}
	
	public boolean validaPagamentoAntecipado(Pedidovenda pedidovenda){
		List<Pedidovendapagamento> listaPedidovendapagamento = pedidovenda.getListaPedidovendapagamento();
		
		if (listaPedidovendapagamento != null && !listaPedidovendapagamento.isEmpty()){

			List<Documento> listaAntecipacao = new ArrayList<Documento>();
			Boolean isWms = empresaService.isIntegracaoWms(pedidovenda.getEmpresa());
			
			if ((isWms != null && isWms) || parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_PEDIDO_ANTECIPACAO))
				listaAntecipacao = documentoService.findForAntecipacaoConsideraPedidoVenda(pedidovenda.getCliente(), null, pedidovenda);
			else
				listaAntecipacao = documentoService.findForAntecipacaoConsideraVenda(pedidovenda.getCliente(), null);
			
			Map<Integer, Double> mapaAntecipacao = new HashMap<Integer, Double>();
			
			for (Documento antecipacao : listaAntecipacao){
				mapaAntecipacao.put(antecipacao.getCddocumento(), antecipacao.getValor().getValue().doubleValue());
			}
			
			for (Pedidovendapagamento pedidovendapagamento : listaPedidovendapagamento){
				if (pedidovendapagamento.getDocumentoantecipacao() != null && pedidovendapagamento.getDocumentoantecipacao().getCddocumento() != null){
					Double valor = mapaAntecipacao.get(pedidovendapagamento.getDocumentoantecipacao().getCddocumento());
					if (valor != null && pedidovendapagamento.getValororiginal() != null){
						valor = valor - pedidovendapagamento.getValororiginal().getValue().doubleValue();
						if (valor < 0){
							return false;
						} else {
							mapaAntecipacao.put(pedidovendapagamento.getDocumentoantecipacao().getCddocumento(), valor);
						}
					}
				}
			}			
		}		
		
		return true;
	}
	
	/**
	 * M�todo que ajax que busca as contas relacionados a empresa
	 *
	 * @param request
	 * @param empresa
	 * @author Luiz Fernando
	 */
	public void ajaxCarregarContaboleto(WebRequestContext request, Empresa empresa){
		List<Conta> listaConta = contaService.findContasAtivasComPermissao(empresa, true);	
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaConta, "listaConta", ""));
	}
	
	/**
	 * M�todo ajax para saber se o tipo de documento � boleto
	 * busca o documentotipo e a lista de conta
	 *
	 * @param request
	 * @param documentotipo
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxTipoDocumentoBoleto(WebRequestContext request, Documentotipo documentotipo) {
		return pedidovendaService.ajaxTipoDocumentoBoleto(request, documentotipo);
	}
	
	/**
	 * M�todo ajax para saber se o tipo de documento � boleto
	 * busca o documentotipo e a lista de conta
	 *
	 * @param request
	 * @param documentotipo
	 * @return
	 * @author Luiz Fernando
	 */
	public void ajaxTipoDocumentoBoletoPagamento(WebRequestContext request) {
		vendaService.ajaxTipoDocumentoBoletoPagamento(request);
	}

	/**
	 * Verifica se a empresa da venda possui um comprovante configur�vel definido 
	 * @param request
	 * @param venda
	 * @return
	 */
	public ModelAndView possuiComprovanteConfiguravel(WebRequestContext request, Pedidovenda pedidovenda){
		boolean alternativo = Boolean.valueOf(request.getParameter("alternativo") != null ? request.getParameter("alternativo") : "false");
		
		if(!alternativo){
			String comprovanteMatricial = parametrogeralService.buscaValorPorNome(Parametrogeral.COMPROVANTE_PEDIDOVENDA_MATRICIAL);
			if ("true".equalsIgnoreCase(comprovanteMatricial)){
				return new JsonModelAndView().addObject("possui", false).addObject("matricial", true);
			}
		}
		
		Empresa empresa = pedidovenda.getEmpresa();
		
		if(empresa==null){
			pedidovenda = pedidovendaService.load(pedidovenda);
			empresa = pedidovenda.getEmpresa();
		}
		boolean possui = empresaService.possuiComprovanteConfiguravel(ComprovanteConfiguravel.TipoComprovante.PEDIDO_VENDA, empresa, alternativo);
		return new JsonModelAndView().addObject("possui", possui).addObject("matricial", false);
	}
	
	public ModelAndView possuiComprovanteConfiguravelColeta(WebRequestContext request, Pedidovenda pedidovenda){
		Empresa empresa = pedidovenda.getEmpresa();
		
		if(empresa==null){
			pedidovenda = pedidovendaService.load(pedidovenda);
			empresa = pedidovenda.getEmpresa();
		}
		boolean possui = empresaService.possuiComprovanteConfiguravel(ComprovanteConfiguravel.TipoComprovante.COLETA, empresa);
		return new JsonModelAndView().addObject("possui", possui);
	}
	
	public ModelAndView gerarComprovanteColeta(WebRequestContext request, PedidovendaFiltro filtro) {
		ComprovanteConfiguravel comprovante;
		String whereIn = filtro.getWhereIn();
		boolean variosRelatorios = StringUtils.isNotEmpty(whereIn);
		String cdpedidovendaStr = whereIn != null && whereIn.contains(",") ? whereIn.substring(0, whereIn.indexOf(",")) : whereIn;
		Integer cdpedidovenda = variosRelatorios ? Integer.parseInt( cdpedidovendaStr) : filtro.getCdpedidovenda();
		Pedidovenda pedidovenda = pedidovendaService.load(new Pedidovenda(cdpedidovenda));
	
		comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(
					ComprovanteConfiguravel.TipoComprovante.COLETA, pedidovenda.getEmpresa() );
			
		if( comprovante != null && comprovante.getLayout() != null){
			if(variosRelatorios) 
				return pedidovendaService.gerarComprovantesConfiguraveis(request, whereIn, comprovante);
			else 
				return  pedidovendaService.gerarComprovanteConfiguravel(request, cdpedidovenda, comprovante);
		}else {
			request.addError("Para emitir o comprovante de coleta, � preciso que a empresa tenha comprovante configur�vel de coleta cadastrado.");
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda");
		}
	}

	/**
	 * M�todo para abrir popup de justificativa no cancelamento
	 * 
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirJustificativaCancelamentoPedidovenda(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		/*
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(vendaService.isExisteECF(whereIn,"pedidovenda")){
			request.addError("N�o � poss�vel cancelar este registro. Pedido enviado ao ECF.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		
		StringBuilder whereInCancelar = new StringBuilder();
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findForCancelamento(whereIn);
		
		boolean registrarDevolucao = false;
		Pedidovenda pedidovendaDevolucao = null; 
		
		List<String> listaErro = new ArrayList<String>();
		
		for (Pedidovenda pv : listaPedidovenda) {
			
			if(!pv.getPedidovendasituacao().equals(Pedidovendasituacao.PREVISTA) && 
					!pv.getPedidovendasituacao().equals(Pedidovendasituacao.AGUARDANDO_APROVACAO) && 
					!pv.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
				request.addError("Pedido de venda com situa��o diferente de 'PREVISTA' ou 'AGUARDANDO APROVA��O' ou 'CONFIRMADO PARCIALMENTE.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			if(!pv.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
				whereInCancelar.append(pv.getCdpedidovenda().toString()).append(",");
			}
			
			if(pv.getEmpresa() != null && pv.getEmpresa().getIntegracaowms() != null && pv.getEmpresa().getIntegracaowms() &&
					pv.getIdentificadorcarregamento() != null){
				registrarDevolucao = true;
				pedidovendaDevolucao = pv;
			}
			
			Valecompra valecompra = valecompraService.findByPedidovenda(pv, Tipooperacao.TIPO_CREDITO);
			if(valecompra != null) {
				Money saldo = valecompraService.getSaldoByCliente(pv.getCliente());
				if(saldo.getValue().doubleValue() - valecompra.getValor().getValue().doubleValue() < 0) {
					listaErro.add("N�o ser� poss�vel cancelar o pedido "+ pv.getCdpedidovenda() +" pois o saldo do vale compra � insuficiente.");
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaErro)) {
			for(String erro : listaErro) {
				request.addError(erro);
			}
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(registrarDevolucao && listaPedidovenda.size() > 1){
			request.addError("O pedido de venda " + pedidovendaDevolucao.getCdpedidovenda() + " ir� gerar devolu��o. � preciso fazer o cancelamento individual deste pedido.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereIn);
		if(listaColeta != null && listaColeta.size() > 0){
			for (Coleta coleta : listaColeta) {
				if(coleta.getPedidovenda() != null && 
						coleta.getPedidovenda().getPedidovendasituacao() != null && 
						!coleta.getPedidovenda().getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
					List<ColetaMaterial> listaColetaMaterial = coleta.getListaColetaMaterial();
					if(listaColetaMaterial != null && listaColetaMaterial.size() > 0){
						for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
							if(coletaMaterial.getQuantidadedevolvida() != null && coletaMaterial.getQuantidadedevolvida() > 0){
								request.addError("N�o � poss�vel cancelar. J� existem coletas devolvidas.");
								SinedUtil.fechaPopUp(request);
								return null;
							}
						}
					}
				}
			}
		}
		
		
		if(whereInCancelar.length() > 0 && pedidovendaService.isProducaoEmAndamento(whereInCancelar.substring(0, whereInCancelar.length()-1))){
			request.addError("N�o � poss�vel cancelar o pedido. A produ��o est� em andamento. Necess�rio cancelar a agenda de produ��o manualmente.");
			SinedUtil.fechaPopUp(request);
			return null;
		}*/
		if(!pedidovendaService.validateAbrirJustificativaCancelamentoPedidovenda(request, whereIn)){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		request.setAttribute("descricao", "CANCELAR");
		request.setAttribute("msg", "Justificativa para o cancelamento");
		
		
		Pedidovenda pedidovenda = new Pedidovenda();
		pedidovenda.setIds(whereIn);	
		return new ModelAndView("direct:/crud/popup/pedidovendaJustificativacancelar", "pedidovenda", pedidovenda);
	}

	
	public ModelAndView abrirRegistrarDevolucao(WebRequestContext request, Pedidovenda pedidovenda){
		String cdpedidovenda = pedidovenda.getIds();
		String origemEntrada = request.getParameter("origemEntrada");
		
		if(cdpedidovenda == null || "".equals(cdpedidovenda)){
			request.addError("Nenhum iten selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(cdpedidovenda.split(",").length > 1){
			request.addError("S� � permitido gerar devolu��o de um item por vez.");
			SinedUtil.fechaPopUp(request);
			return null;
		}

		String observacaohistorico = pedidovenda.getObservacaohistorico();
		String urlRedirecionamento = "/faturamento/crud/Pedidovenda " + (origemEntrada != null && origemEntrada.equals("entrada") ? "?ACAO=consultar&cdpedidovenda=" + cdpedidovenda : "");
		pedidovenda = pedidovendaService.loadForRegistrarDevolucao(new Pedidovenda(Integer.parseInt(cdpedidovenda)));		
		
		if(pedidovenda == null){
			request.addError("N�o foi poss�vel carregar as informa��es para registrar devolu��o.");
			SinedUtil.redirecionamento(request, urlRedirecionamento);
			return null;
		} 
		
		if(!Pedidovendasituacao.PREVISTA.equals(pedidovenda.getPedidovendasituacao()) && 
				!Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(pedidovenda.getPedidovendasituacao()) &&
				!Pedidovendasituacao.CONFIRMADO_PARCIALMENTE.equals(pedidovenda.getPedidovendasituacao())){
			request.addError("S� � poss�vel registrar devolu��o de pedido de venda PREVISTO, AGUARDANDO APROVA��O ou CONFIRMADO PARCIALMENTE.");
			SinedUtil.redirecionamento(request, urlRedirecionamento);
			return null;
		}
		
		VendaDevolucaoBean bean = pedidovendaService.criaVendaDevolucaoBean(pedidovenda);
		if(bean == null || bean.getListaItens() == null || bean.getListaItens().isEmpty()){
			request.addError("N�o existe nenhum material neste pedido de venda que se possa fazer a devolu��o.");
			SinedUtil.redirecionamento(request, urlRedirecionamento);
			return null;
		}
		
		bean.setOrigemEntrada(origemEntrada);
		bean.setObservacaohistorico(observacaohistorico);
		bean.setUrl(urlRedirecionamento);
		request.setAttribute("obrigarnumero", pedidovenda.getEmpresa() != null && pedidovenda.getEmpresa().getIntegracaowms() != null && pedidovenda.getEmpresa().getIntegracaowms());
		request.setAttribute("ajustarPopup", true);
		request.setAttribute("existeDocuemnto", pedidovendapagamentoService.existeDocumento(pedidovenda));
		return new ModelAndView("direct:/crud/popup/registrarDevolucao", "bean", bean);		
	}
	
	/**
	 * M�todo que faz o processo de devolu��o do material
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView registrarDevolucao(WebRequestContext request, VendaDevolucaoBean bean){	
		return vendaService.registrarDevolucao(request, bean);
	}
	
	/**
	 * M�todo que busca a taxa de pedido de venda do cliente
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxTaxapedidovendaCliente(WebRequestContext request, Cliente cliente) {
		JsonModelAndView json = new JsonModelAndView();		
		Money taxapedidovenda = pedidovendaService.getTaxapedidovendaCliente(request, cliente);
				
		json.addObject("taxapedidovenda", taxapedidovenda != null ? SinedUtil.descriptionDecimal(taxapedidovenda.getValue().doubleValue(), true) : "");
		return json;
	}
	
	public ModelAndView abrirPopUpEnviarComprovante(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if (whereIn == null || "".equals(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		} else {
			Pedidovendasituacao [] situacoes = { Pedidovendasituacao.AGUARDANDO_APROVACAO };
			
			Boolean emitirComprovante = parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO");
			Boolean existemSelecionadosAguardandoAprovacao = pedidovendaService.havePedidovendaSituacao(whereIn, situacoes);
			
			if(!emitirComprovante && existemSelecionadosAguardandoAprovacao) {
				request.addError("O envio e emiss�o do comprovante n�o s�o permitidos na situa��o de Aguardando Aprova��o.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findForEnviocomprovanteemail(whereIn);
		
		List<EnvioComprovanteDestinatarioBean> listaDestinatario = new ArrayList<EnvioComprovanteDestinatarioBean>();
		String remetente = null;
		Boolean clientesDiferentes = false;
		Boolean empresasDiferentes = false;
		Integer cdcliente_aux = null;
		Integer cdempresa_aux = null;
		
		for(Pedidovenda pedidovenda : listaPedidovenda){
			// VERIFICA O REMETENTE DO E-MAIL
			if(remetente == null){
				remetente = pedidovenda.getColaborador().getEmail();
			} else if(!remetente.equals(pedidovenda.getColaborador().getEmail())){
				remetente = "";
			}
			
			// VERIFICA SE � CLIENTE DIFERENTE
			if(cdcliente_aux == null){
				cdcliente_aux = pedidovenda.getCliente().getCdpessoa();
			} else if(!cdcliente_aux.equals(pedidovenda.getCliente().getCdpessoa())){
				clientesDiferentes = true;
			}
			
			// VERIFICA SE � EMPRESA DIFERENTE
			if(cdempresa_aux == null){
				cdempresa_aux = pedidovenda.getEmpresa().getCdpessoa();
			} else if(!cdempresa_aux.equals(pedidovenda.getEmpresa().getCdpessoa())){
				empresasDiferentes = true;
			}
			
			// INCLUI O E-MAIL DO CLIENTE
			EnvioComprovanteDestinatarioBean envioComprovanteDestinatario = new EnvioComprovanteDestinatarioBean();
			envioComprovanteDestinatario.setId(pedidovenda.getCdpedidovenda());
			envioComprovanteDestinatario.setCdpessoa(pedidovenda.getCliente().getCdpessoa());
			envioComprovanteDestinatario.setNome(pedidovenda.getCliente().getNome());
			envioComprovanteDestinatario.setEmail(pedidovenda.getCliente().getEmail());
			listaDestinatario.add(envioComprovanteDestinatario);
			
			// INCLUI OS CONTATOS DO CLIENTE
			if(pedidovenda.getCliente().getListaContato() != null && !pedidovenda.getCliente().getListaContato().isEmpty()){
				for (PessoaContato pessoaContato : pedidovenda.getCliente().getListaContato()) {
					Contato c = pessoaContato.getContato();
					
					if (c.getEmailcontato() != null && !"".equals(c.getEmailcontato())) {
						EnvioComprovanteDestinatarioBean envioComprovanteDestinatarioContato = new EnvioComprovanteDestinatarioBean();
						envioComprovanteDestinatarioContato.setId(pedidovenda.getCdpedidovenda());
						envioComprovanteDestinatarioContato.setCdpessoa(c.getCdpessoa());
						envioComprovanteDestinatarioContato.setNome(c.getNome() + " (" + pedidovenda.getCliente().getNome() + ")");
						envioComprovanteDestinatarioContato.setEmail(c.getEmailcontato());
						listaDestinatario.add(envioComprovanteDestinatarioContato);
					}
				}
			}
		}
		
		String mensagem = "";
		if (clientesDiferentes) {
			mensagem += "Caro,\n\n";
		} else {
			mensagem += "Caro " + listaPedidovenda.get(0).getCliente().getNome() + ",\n\n";
		}
		mensagem += "Envio em anexo o comprovante para seu conhecimento.\n\n";
		if (!empresasDiferentes) {
			mensagem += listaPedidovenda.get(0).getEmpresa().getNomefantasia();
		}
		
		Enviocomprovanteemail enviocomprovanteemail = new Enviocomprovanteemail();
		enviocomprovanteemail.setRemetente(remetente);
		enviocomprovanteemail.setAssunto("Comprovante de Pedido de Venda");
		enviocomprovanteemail.setEmail(mensagem);
		enviocomprovanteemail.setListaDestinatarios(listaDestinatario);
		enviocomprovanteemail.setWhereIn(whereIn);
		enviocomprovanteemail.setEntrada(request.getParameter("fromentrada"));
		enviocomprovanteemail.setPossibilidadeAvulso(listaPedidovenda.size() == 1);
		request.setAttribute("labelId", "Pedido de Venda");
		
		return new ModelAndView("direct:/crud/popup/popUpEnviocomprovanteemail", "bean", enviocomprovanteemail);
	}
	
	/**
	 * M�todo para enviar comprovante de pedido de venda para o cliente e, caso o material
	 * possua fornecedor exclusivo, envia para o fornecedor tamb�m
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public void enviarComprovante(WebRequestContext request, Enviocomprovanteemail enviocomprovanteemail){
		String fromentrada = enviocomprovanteemail.getEntrada();
		
		if(enviocomprovanteemail != null){
			Map<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>> map = vendaService.getMapEnvioComprovante(enviocomprovanteemail);
			
			int sucesso = 0;
			int erro = 0;
			int pedidovendacancelado = 0; 
			List<EnvioComprovanteAnexoBean> listaAnexo = enviocomprovanteemail.getListaArquivos();
			Set<Entry<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>>> entrySet = map.entrySet();
			List<Pedidovenda> comprovantesEnviadosComSucesso = new ArrayList<Pedidovenda>();
			for (Entry<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>> entry : entrySet) {
				EnvioComprovanteBean comproanteBean = entry.getKey();
				List<EnvioComprovanteDestinatarioBean> listaDestinatario = entry.getValue();
				
				if(comproanteBean.getId() == null) continue;
				
				Pedidovenda pedidovenda = pedidovendaService.loadForComprovante(comproanteBean.getId());
				try{
					if(pedidovenda.getPedidovendasituacao() != null && Pedidovendasituacao.CANCELADO.equals(pedidovenda.getPedidovendasituacao())){
						pedidovendacancelado++;
					} else {
						Usuario usuario = SinedUtil.getUsuarioLogado();
						if(pedidovenda.getColaborador() != null && pedidovenda.getColaborador().getCdpessoa() != null){
							usuario = new Usuario(pedidovenda.getColaborador().getCdpessoa());
						}
						
						ComprovanteConfiguravel comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(ComprovanteConfiguravel.TipoComprovante.PEDIDO_VENDA, pedidovenda.getEmpresa());
						Resource resource = pedidovendaService.criaComprovantepedidovendaForEnviaremail(pedidovenda, comprovante);
						
						Empresa empresa = empresaService.loadComRepresentantes(pedidovenda.getEmpresa());
						List<Pedidovendamaterial> listaPedidovendamaterial = pedidovendamaterialService.loadComMaterialfornecedor(pedidovenda);
						
						if(listaDestinatario != null && listaDestinatario.size() > 0){
							for(EnvioComprovanteDestinatarioBean comprovanteDestinatarioBean : listaDestinatario){
								try {
									EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
									email
										.setFrom(enviocomprovanteemail.getRemetente())
										.setTo(comprovanteDestinatarioBean.getEmail())
										.setSubject(enviocomprovanteemail.getAssunto())
										.attachFileUsingByteArray(resource.getContents(), resource.getFileName(), resource.getContentType(), resource.getFileName())
										.addHtmlText(enviocomprovanteemail.getEmail().replace("\n", "<br>") + usuarioService.addImagemassinaturaemailUsuario(usuario, email));
									
									if(listaAnexo != null && listaAnexo.size() > 0){
										Integer id = 1;
										for(EnvioComprovanteAnexoBean anexoBean : listaAnexo){
											if(anexoBean.getArquivo() != null && anexoBean.getArquivo().getContent() != null && anexoBean.getArquivo().getContent().length > 0){
												email.attachFileUsingByteArray(anexoBean.getArquivo().getContent(), anexoBean.getArquivo().getNome(), anexoBean.getArquivo().getTipoconteudo(), id.toString());
												id++;							
											}
										}
									}
										
									envioemailService.registrarEnvio(
											enviocomprovanteemail.getRemetente(), 
											enviocomprovanteemail.getAssunto(), 
											enviocomprovanteemail.getEmail().replace("\n", "<br>"), 
											Envioemailtipo.COMPROVANTE_PEDIDO_VENDA,
											new Pessoa(comprovanteDestinatarioBean.getCdpessoa()), 
											comprovanteDestinatarioBean.getEmail(), 
											comprovanteDestinatarioBean.getNome(),
											email);
									
									email.sendMessage();
									
									sucesso++;
									
									if(!comprovantesEnviadosComSucesso.contains(pedidovenda)){
										comprovantesEnviadosComSucesso.add(pedidovenda);
									}
									
									// FEITO ISSO PARA N�O AGARRAR O MAILTRAP
									if(SinedUtil.isAmbienteDesenvolvimento()){
										Thread.sleep(5000);
									}
								} catch (Exception e) {
									e.printStackTrace();
									request.addError("Falha no envio de e-mail do pedido de venda " + pedidovenda.getCdpedidovenda() + ": " + e.getMessage());
									erro++;
								}
							}
						}
						
						if(empresa != null && empresa.getListaEmpresarepresentacao() != null && !empresa.getListaEmpresarepresentacao().isEmpty()){
							for(Empresarepresentacao empresarepresentacao : empresa.getListaEmpresarepresentacao()){
								if(empresarepresentacao.getFornecedor() != null){
									if (pedidovendamaterialService.isFornecedorPedidovendamaterial(listaPedidovendamaterial, empresarepresentacao.getFornecedor())) {
										boolean sucessofornecedor = pedidovendaService.enviaComprovanteEmailRepresentante(request, pedidovenda, empresa, empresarepresentacao.getFornecedor(), resource);
										if(sucessofornecedor){
											sucesso++;
										} else {
											erro++;
										}
									}
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					erro++;
				}
			}
			
			if (sucesso > 0) {
				pedidovendahistoricoService.makeHistoricoComprovanteEnviado(comprovantesEnviadosComSucesso);
				request.addMessage(sucesso + " comprovante(s) enviado(s) com sucesso.");
			}
			if (erro > 0) {
				request.addError(erro + " comprovante(s) n�o foi(ram) enviado(s).");
			}
			if (pedidovendacancelado > 0) {
				request.addError("N�o foi poss�vel enviar comprovante(s) de " + pedidovendacancelado + " pedido(s) de venda por estar(em) cancelado(s).");
			}
		}
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda" + (fromentrada != null && fromentrada.equals("true") ? "?ACAO=consultar&cdpedidovenda=" + enviocomprovanteemail.getWhereIn() : ""));
	}
	
	/**
	 * M�todo que busca os lotes com qtde dispon�vel para o material
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView getLoteestoque(WebRequestContext request, Material material){

		return vendaService.getLoteestoque(request, material);
	}
	
	/**
	 * M�todo que busca a qtde do lote
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView getQtdeLoteestoque(WebRequestContext request, Material material){
		String cdloteestoquestr = request.getParameter("cdloteestoque");
		String cdempresa = request.getParameter("cdempresa");
		String cdlocalarmazenagem = request.getParameter("cdlocalarmazenagem");
		Empresa empresa = null;
		Localarmazenagem localarmazenagem = null;
		try {
			if(cdempresa != null && !"".equals(cdempresa)){
				empresa = new Empresa(Integer.parseInt(cdempresa));
			}
			if(cdlocalarmazenagem != null && !"".equals(cdlocalarmazenagem)){
				localarmazenagem = new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem));
			}
		} catch (NumberFormatException e) {}
		Double qtde = 0.0;
		if(cdloteestoquestr != null && !"".equals(cdloteestoquestr) && material.getCdmaterial() != null){
			boolean cadastroErrado = false;
			boolean whereMaterialmestregrade = false;
			Material bean = materialService.loadWithGrade(material);
			if(bean != null && bean.getMaterialgrupo() != null && bean.getMaterialgrupo().getGradeestoquetipo() != null){
				if(Gradeestoquetipo.MESTRE.equals(bean.getMaterialgrupo().getGradeestoquetipo())){
					if(bean.getMaterialmestregrade() != null){
						whereMaterialmestregrade = true;
					}else {
						whereMaterialmestregrade = false;
					}
				}else if(Gradeestoquetipo.ITEMGRADE.equals(bean.getMaterialgrupo().getGradeestoquetipo())){
					if(bean.getMaterialmestregrade() != null){
						whereMaterialmestregrade = false;
					}else {
						/*verifica se o material est� sendo controlado por item de grade e n�o
						existe filhos vinculados a ele. Se retornar true, o cadastro do material est� errado ( material n�o � de grade) 
						e a quantidade retornada em estoque deve ser zero.*/
						if(materialService.isControleItemgradeSemMaterialgrademestre(material)){
							cadastroErrado = true;
						}else {
							whereMaterialmestregrade = true;
						}
					}
				}
			}
			if(!cadastroErrado){
				qtde = loteestoqueService.getQtdeLoteestoque(material, localarmazenagem, empresa, new Loteestoque(Integer.parseInt(cdloteestoquestr)), whereMaterialmestregrade);
			}
		}
		return new JsonModelAndView().addObject("qtdeloteestoque", qtde);
	}
	
	/**
	 * Action em ajax para buscar as sugest�es dos materiais inclu�dos no pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView buscarSugestoesVendaAJAX(WebRequestContext request, Venda venda){
		return pedidovendaService.buscarSugestoesVendaAJAX(request, venda, request.getParameter("codigos"));
	}
	
	public ModelAndView verificarIdentificadorPedidovenda(WebRequestContext request, Empresa empresa){
		Integer cdpessoa = Integer.parseInt(request.getParameter("cdpessoa"));
		empresa = empresaService.buscarProximoIdentificadorPedidovenda(new Empresa(cdpessoa));
		return new JsonModelAndView().addObject("identificador", empresa.getProximoidentificadorpedidovenda() != null ? empresa.getProximoidentificadorpedidovenda() : "");
	}
	
	/**
	 * M�todo chamado a partir da tela gerenciamento material.
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @throws CrudException
	 * @author Luiz Fernando
	 */
	public ModelAndView preparaFiltroFromGerenciamentoMaterial(WebRequestContext request, PedidovendaFiltro filtro) throws CrudException{
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			filtro.setMaterial(materialService.load(filtro.getMaterial(), "material.cdmaterial, material.nome"));
			filtro.setDtinicio(pedidovendaService.getMinDataPedidovendaReserva(filtro.getMaterial()));
		}
		if(filtro.getEmpresa() == null){
			filtro.setEmpresa(empresaService.loadPrincipal());
		}
//		List<Pedidovendatipo> listaPedidovendatipo = pedidovendatipoService.findReserva();
//		if(listaPedidovendatipo != null && listaPedidovendatipo.size() > 0){
//			filtro.setPedidovendatipo(listaPedidovendatipo.get(0));
//		}		
		filtro.setNaoexibirresultado(false);
		filtro.setNotFirstTime(true);
		filtro.setTipopedidoReserva(true);
		return continueOnAction("listagem", filtro);
	}
	
	/**
	 * M�todo que atualiza o saldo dos pedidos
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public ModelAndView atualizarSaldoPedidovenda(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Integer qtdeTotalAtualizada =  0;
		try {
			List<String> listaWhereIn = SinedUtil.getListaWhereIn(whereIn, 30);
			for (String in : listaWhereIn) {
				Integer qtdeAtualizada = pedidovendaService.atualizarSaldoPedidovenda(request, in);
				if(qtdeAtualizada != null && qtdeAtualizada > 0){
					qtdeTotalAtualizada += qtdeAtualizada;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		
		if(qtdeTotalAtualizada > 0){
			request.addMessage("Saldo atualizado com sucesso.");
			request.addMessage(qtdeTotalAtualizada + " pedido(s) de venda atualizado(s) com sucesso.");
		}else { 
			request.addError("N�o foi poss�vel atualizar saldo do(s) pedido(s) de venda.");
		}
			
		if("true".equals(request.getParameter("entrada"))) 
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=consultar&cdpedidovenda=" + whereIn);
		else
			return sendRedirectToAction("listagem");
	}
	
	/**
	 * M�todo ajax que verifica se o prazo de pagamento
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView verificaJurosPrazopagamento(WebRequestContext request){
		String cdprazopagamentostr = request.getParameter("cdprazopagamento");
		boolean existjuros = false;
		if(cdprazopagamentostr != null && !"".equals(cdprazopagamentostr)){
			try {
				Prazopagamento prazopagamento = prazopagamentoService.load(new Prazopagamento(Integer.parseInt(cdprazopagamentostr)), "prazopagamento.cdprazopagamento, prazopagamento.juros");
				if(prazopagamento != null && prazopagamento.getJuros() != null && prazopagamento.getJuros() > 0){
					existjuros = true;
				}
			} catch (Exception e) {}
		}
		return new JsonModelAndView().addObject("existjuros", existjuros);
	}
	
	/**
	 * M�todo ajax para buscar os produtos de acordo com a tabela de pre�o do cliente
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxBuscaMateriaisTabelaprecoByCliente(WebRequestContext request){
		return materialtabelaprecoService.buscaMateriaisTabelaprecoByCliente(request);		
	}
	
	/**
	 * Abre pop-up para o envio para o ECF
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 08/05/2013
	 */
	public ModelAndView abrirPopupEnviarECF(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		boolean erro = false;
		String[] ids = whereIn.split(",");
		for (String id : ids) {
			Pedidovenda pedidovenda = new Pedidovenda(Integer.parseInt(id));
			if(pedidovendaService.havePedidovendaDiferenteSituacao(whereIn, Pedidovendasituacao.PREVISTA)){
				request.addError("Pedido de venda " + id + " diferente de 'PREVISTO'.");
				erro = true;
			} else if(emporiumpedidovendaService.havePedidovendaEnviado(pedidovenda)){
				request.addError("Pedido de venda " + id + " j� enviado ao ECF.");
				erro = true;
			} else {
				pedidovenda = pedidovendaService.loadWithPedidovendatipo(pedidovenda);
				if(pedidovenda.getPedidovendatipo() != null && 
						pedidovenda.getPedidovendatipo().getNaopermitirenviarecf() != null && 
						pedidovenda.getPedidovendatipo().getNaopermitirenviarecf()){
					request.addError("O tipo de pedido de venda n�o permite enviar ao ECF.");
					erro = true;
				} else {
					Boolean bloquearEnvioMaterialSemIntegracao = parametrogeralService.getBoolean(Parametrogeral.EMPORIUM_BLOQUEAR_ENVIO_MATERIAL_SEM_INTEGRACAO);
					List<Pedidovendamaterial> listaPedidovendamaterial = pedidovendamaterialService.findByPedidoVenda(pedidovenda);
					if(bloquearEnvioMaterialSemIntegracao != null && bloquearEnvioMaterialSemIntegracao){
						for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
							if(pedidovendamaterial != null && pedidovendamaterial.getMaterial() != null){
								boolean vendaecf = pedidovendamaterial.getMaterial().getVendaecf() != null && pedidovendamaterial.getMaterial().getVendaecf();
								if(!vendaecf){
									request.addError("O item '" + pedidovendamaterial.getMaterial().getNome() + "' do pedido n�o est� cadastrado no ECF, verifique a marca��o 'Integra��o com ECF' no cadastro do material");
									erro = true;
								}
							}
						}
					}
					
					if (erro==false && pedidovenda.getPedidovendatipo()!=null && Boolean.TRUE.equals(pedidovenda.getPedidovendatipo().getValidarEstoquePedidoEnviarECF())){
						List<Material> listaProdutoSemSaldoDisponivel = pedidovendaService.getListaMateriaisSemSaldoDisponivel(pedidovenda, listaPedidovendamaterial);
					
						if (SinedUtil.isListNotEmpty(listaProdutoSemSaldoDisponivel)){
							request.addError("N�o h� saldo dispon�vel em estoque para o(s) produto(s): " + CollectionsUtil.listAndConcatenate(listaProdutoSemSaldoDisponivel, "nome", ", ") + ".");
							erro = true;
						}
					}
				}
			}
		}
		
		if(erro){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Emporiumpdv> listaPDV = emporiumpdvService.findAll();
		request.setAttribute("listaPDV", listaPDV);
		
		EnvioECFBean bean = new EnvioECFBean();
		bean.setWhereIn(whereIn);
		bean.setEntrada(request.getParameter("fromentrada"));
		
		return new ModelAndView("direct:/crud/popup/popUpEnviarECF", "bean", bean);
	}
	
	/**
	 * Envia o pedido de venda para o ECF.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/04/2013
	 */
	public void saveEnviarECF(WebRequestContext request, EnvioECFBean bean){
		emporiumpedidovendaService.enviarPedidovendaECF(bean);
		IntegracaoEmporiumUtil.util.fechaPopUpExecutaEmporium(request);
	}
	
	/**
	 * M�todo ajax que verifica se existem materiais em outros pedidos em reserva
	 *
	 * @param request
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxVerificaMateriaisEmReserva(WebRequestContext request, Pedidovenda pedidovenda){	
		Boolean existreserva = false;
		Boolean existePedidoSincronizacaoWms = false;
		if(StringUtils.isNotEmpty(pedidovenda.getWhereInConfirmacao())){
			String[] ids = pedidovenda.getWhereInConfirmacao().split(",");
			if(ids.length > 0){
				existePedidoSincronizacaoWms = pedidovendaService.verificaSincronizacaoComWmsForConfirmacao(pedidovenda.getWhereInConfirmacao());
				for(String id : ids){					
					if(parametrogeralService.getBoolean(Parametrogeral.EXIBIR_POPUP_RESERVA_CONFIRMACAO_PEDIDOVENDA) &&
							pedidovendaService.isMaterialReservadoOutrosPedidos(new Pedidovenda(Integer.parseInt(id)), false)){
						existreserva = true;
						break;
					}
				}
			}
		}else if(pedidovenda.getCdpedidovenda() != null){
			existePedidoSincronizacaoWms = pedidovendaService.verificaSincronizacaoComWmsForConfirmacao(pedidovenda.getCdpedidovenda().toString());
			if(parametrogeralService.getBoolean(Parametrogeral.EXIBIR_POPUP_RESERVA_CONFIRMACAO_PEDIDOVENDA)){
				existreserva = pedidovendaService.isMaterialReservadoOutrosPedidos(pedidovenda, false);
			}
		}
		return new JsonModelAndView().addObject("existreserva", existreserva).addObject("existePedidoSincronizacaoWms", existePedidoSincronizacaoWms);
	}
	
	/**
	 * M�todo que abre popup para mostrar pedidos com reservas no momento de confirmar o pedido
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirConfirmacaoPedidovendaMaterialEmReserva(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findReservasMateriais(whereIn);
		request.setAttribute("cdpedidovenda", whereIn);
		request.setAttribute("reservas", listaPedidovenda);
		
		Boolean confirmarPercentualPedidoVenda = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.CONFIRMARPERCENTUALPEDIDOVENDA));
		request.setAttribute("CONFIRMARPERCENTUALPEDIDOVENDA", confirmarPercentualPedidoVenda);
		
		String confirmarPvServicoProduto = parametrogeralService.getValorPorNome(Parametrogeral.CONFIRMAR_PV_SERVICO_PRODUTO);
		request.setAttribute("confirmarPvServicoProduto", "TRUE".equalsIgnoreCase(confirmarPvServicoProduto) && pedidovendaService.existePedidoComServicoEProduto(whereIn));
		
		return new ModelAndView("direct:/crud/popup/confirmacaoPedidovendaMaterialEmReserva");
	}
	
	/**
	 * M�todo que abre popup para mostrar pedidos com reservas no momento de confirmar o pedido
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirConfirmacaoPercentualPedidoVenda(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		Pedidovenda pedidovenda = new Pedidovenda();
		pedidovenda.setWhereInConfirmacao(whereIn);
		pedidovenda.setPercentualPedidovenda(100);
		
		boolean existeReserva = Boolean.valueOf(StringUtils.isNotEmpty(request.getParameter("existeReserva")) ? 
				request.getParameter("existeReserva") : "false");
		request.setAttribute("existeReserva", existeReserva);
		
		String confirmarPvServicoProduto = parametrogeralService.getValorPorNome(Parametrogeral.CONFIRMAR_PV_SERVICO_PRODUTO);
		request.setAttribute("confirmarPvServicoProduto", "TRUE".equalsIgnoreCase(confirmarPvServicoProduto) && pedidovendaService.existePedidoComServicoEProduto(whereIn));
		
		return new ModelAndView("direct:/crud/popup/confirmacaoPercentualPedidoVenda", "pedidovenda", pedidovenda);
	}

	public ModelAndView abrirConfirmacaoServicoprodutoPedidoVenda(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		String percentual = request.getParameter("percentualPedidovenda");
		if(percentual == null) percentual = "100";
		boolean existeReserva = Boolean.valueOf(StringUtils.isNotEmpty(request.getParameter("existeReserva")) ? request.getParameter("existeReserva") : "false");
		
		if(pedidovendaService.existePedidoComServicoEProduto(whereIn)){
			request.setAttribute("selectedItens", whereIn);
			request.setAttribute("percentualPedidovenda", percentual);
			request.setAttribute("existeReserva", existeReserva);
			return new ModelAndView("direct:/crud/popup/popupConfirmarpedidoservicoproduto");
		}else {
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda?ACAO=confirmar&selectedItens=" + whereIn);
			return null;
		}
	}
	
	/**
	 * M�todo que abre popup para leitura de c�digo de barras de cheque
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#abrirPopupCodigobarrascheque(WebRequestContext request)
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopupCodigobarrascheque(WebRequestContext request){
		return documentoService.abrirPopupCodigobarrascheque(request);
	}
	
	/**
	 * M�todo que abre popup para personaliza��o de emitente de cheque
	 *
	 * @param request
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView abrirPopupEmitenteCheque(WebRequestContext request, Vendapagamento vendapagamento){
		return vendaService.abrirPopupEmitenteCheque(request, vendapagamento);
	}
	
	public ModelAndView buscarMaterialtabelaprecoAJAX(WebRequestContext request, Venda venda){	
		return vendaService.buscarMaterialtabelaprecoAJAX(request, venda);
	}
	
	public ModelAndView comboBoxContato(WebRequestContext request, Cliente cliente) {
		return vendaService.comboBoxContato(request, cliente);
	}
	
	public ModelAndView preencherFornecedor(WebRequestContext request, Pedidovenda pedidovenda){
		
		List<Fornecedor> listaFornecedor1 = fornecedorService.findForFaturamento(pedidovenda.getEmpresa(), pedidovenda.getMaterial().getCdmaterial().toString());
		List<GenericBean> listaFornecedor = new ArrayList<GenericBean>();
		
		for (Fornecedor fornecedor: listaFornecedor1){
			listaFornecedor.add(new GenericBean(fornecedor.getCdpessoa(), fornecedor.getNome()));
		}
		
		return new JsonModelAndView().addObject("listaFornecedor", listaFornecedor);
	}
	
	public ModelAndView escolherFornecedor(WebRequestContext request, Venda venda){
		return vendaService.escolherFornecedor(request, venda);
	}
	
	/**
	 * 
	 * @param pedidovenda
	 * @param empresa
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarRepresentacao(WebRequestContext request, Pedidovenda pedidovenda){
		boolean representacao = false;
		if (pedidovenda.getEmpresa()!=null && pedidovenda.getEmpresa().getCdpessoa()!=null){
			representacao = empresaService.isRepresentacao(pedidovenda.getEmpresa());
		}
		return new JsonModelAndView().addObject("representacao", representacao);
	}
	
	/**
	 * 
	 * @param pedidovenda
	 * @param empresa
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarFornecedorProdutos(WebRequestContext request, Empresa empresa){
		return pedidovendaService.verificarFornecedorProdutos(request, empresa, request.getParameter("idsMaterial"));
	}
	
	public ModelAndView abrirGerarProducaoConferencia(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		GerarProducaoConferenciaBean bean = pedidovendaService.validaProducaoEmAndamento(request, whereIn);
		if(bean == null){
			return sendRedirectToAction("listagem");
		}
		
		request.setAttribute("pathAjax", "/faturamento/crud/Pedidovenda");
		request.setAttribute("RECAPAGEM", SinedUtil.isRecapagem());
		
		return new ModelAndView("/process/gerarProducaoConferencia", "bean", bean);
	}
	
	public ModelAndView ajaxBuscarDadosProducao(WebRequestContext request, GerarProducaoConferenciaBean bean) throws Exception{
		List<Pedidovendamaterial> listaPedidovendamaterial = pedidovendamaterialService.findByVendaProducao(bean.getWhereInPedidovenda(), bean.getWhereInPedidovendamaterial());
		List<GerarProducaoConferenciaMaterialBean> listaMaterial = new ArrayList<GerarProducaoConferenciaMaterialBean>();
		List<ColetaMaterial> listaColetaMaterial = coletaMaterialService.findByPedidovendaForProducao(bean.getWhereInPedidovenda(), bean.getWhereInPedidovendamaterial());
		Map<Integer, Double> mapaQtdeDisponivel = pedidovendamaterialService.montaMapaQuantidadeDisponivelForProducao(listaColetaMaterial);
		
		for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
			Producaoetapa producaoetapa = pedidovendamaterial.getMaterial().getProducaoetapa();
			Producaoagendatipo producaoagendatipo = null;
			if(pedidovendamaterial.getPedidovenda() != null &&
					pedidovendamaterial.getPedidovenda().getPedidovendatipo() != null && 
					pedidovendamaterial.getPedidovenda().getPedidovendatipo().getProducaoagendatipo() != null){
				producaoagendatipo = pedidovendamaterial.getPedidovenda().getPedidovendatipo().getProducaoagendatipo();
				if(producaoetapa == null && pedidovendamaterial.getPedidovenda().getPedidovendatipo().getProducaoagendatipo().getProducaoetapa() != null){
					producaoetapa = pedidovendamaterial.getPedidovenda().getPedidovendatipo().getProducaoagendatipo().getProducaoetapa();
				}
			}
			
			Double qtde = mapaQtdeDisponivel.containsKey(pedidovendamaterial.getCdpedidovendamaterial()) 
				? mapaQtdeDisponivel.get(pedidovendamaterial.getCdpedidovendamaterial()) : pedidovendamaterial.getQuantidade();
			
			listaMaterial.add(new GerarProducaoConferenciaMaterialBean(pedidovendamaterial.getMaterial(), 
				qtde != null ? qtde : 0.,
				pedidovendamaterial,
				pedidovendamaterial.getPedidovenda(),
				pedidovendamaterial.getPedidovenda().getCliente(),
				pedidovendamaterial.getLargura(),
				pedidovendamaterial.getAltura(),
				pedidovendamaterial.getComprimento(),
				pedidovendamaterial.getObservacao(),
				producaoetapa,
				producaoagendatipo,
				pedidovendamaterial.getPedidovenda().getEmpresa(),
				pedidovendamaterial.getUnidademedida(),
				pedidovendamaterial.getPneu(),
				null));
		}
		
		/*Collections.sort(listaMaterial, new Comparator<GerarProducaoConferenciaMaterialBean>(){
			public int compare(GerarProducaoConferenciaMaterialBean o1, GerarProducaoConferenciaMaterialBean o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});*/
		
		Integer qtdeentregas = bean.getQtdeentregas();
		Frequencia frequencia = bean.getFrequencia();
		Timestamp dtprimeiraentrega = bean.getDtprimeiraentrega();
		
		for (GerarProducaoConferenciaMaterialBean materialBean : listaMaterial) {
			Timestamp data_aux = new Timestamp(System.currentTimeMillis());
			if(dtprimeiraentrega != null) 
				data_aux.setTime(dtprimeiraentrega.getTime());
			
			List<GerarProducaoConferenciaMaterialItemBean> listaItem = new ArrayList<GerarProducaoConferenciaMaterialItemBean>();
			
			Double qtdedividida = materialBean.getQtde()/qtdeentregas;
			Double qtdetotal = 0d;
			for (int i = 0; i < qtdeentregas; i++) {
				qtdetotal += qtdedividida;
				if(qtdeentregas.equals(i+1) && !qtdetotal.equals(materialBean.getQtde())){
					qtdedividida += (materialBean.getQtde() - qtdetotal);
				}
				
				listaItem.add(new GerarProducaoConferenciaMaterialItemBean(qtdedividida, data_aux));
				
				if(frequencia != null){
					data_aux = SinedDateUtils.incrementTimestampFrequencia(data_aux, frequencia, 1);
				}
			}
			
			materialBean.setListaItem(listaItem);
		}

		request.setAttribute("listaMaterial", listaMaterial);
		request.setAttribute("qtdeentregas", qtdeentregas);
		
		return new ModelAndView("direct:/process/gerarProducaoConferenciaMateriais");
	}
	
	public ModelAndView salvarGerarProducao(WebRequestContext request, GerarProducaoConferenciaBean bean){
		String whereIn = bean.getWhereInPedidovenda();
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		if(pedidovendaService.havePedidovendaSituacao(whereIn, Pedidovendasituacao.CANCELADO, Pedidovendasituacao.CONFIRMADO, Pedidovendasituacao.AGUARDANDO_APROVACAO)){
			request.addError("Para gerar produ��o, o(s) pedido(s) de venda deve(m) estar com situa��o diferente de 'AGUARDANDO APROVA��O', 'CANCELADO' e 'CONFIRMADO'.");
			return sendRedirectToAction("listagem");
		}
		
		if(!pedidovendaService.haveMaterialproducao(whereIn)){
			request.addError("Para gerar produ��o, o(s) pedido(s) de venda deve(m) ter material(is) de produ��o.");
			return sendRedirectToAction("listagem");
		}
		
		List<Pedidovendamaterial> listaPedidovendamaterial = pedidovendamaterialService.findForGerarProducao(whereIn);
		if(SinedUtil.isListEmpty(listaPedidovendamaterial)){
			request.addError("Produ��o j� gerada para o(s) pedido(s) de venda selecionado(s).");
			return sendRedirectToAction("listagem");
		}
		
		try{
			List<GerarProducaoConferenciaMaterialBean> listaMaterial = bean.getListaMaterial();
			List<Producaoagenda> listaProducaoagenda = new ArrayList<Producaoagenda>();
			
			for (GerarProducaoConferenciaMaterialBean materialBean : listaMaterial) {
				Empresa empresa = materialBean.getEmpresa();
				Cliente cliente = materialBean.getCliente();
				Pedidovenda pedidovenda = materialBean.getPedidovenda();
				Material material = materialBean.getMaterial();
				Producaoetapa producaoetapa = materialBean.getProducaoetapa();
				Producaoagendatipo producaoagendatipo = materialBean.getProducaoagendatipo();
				if(producaoetapa == null) producaoetapa = materialBean.getMaterial().getProducaoetapa();
				Double largura = materialBean.getLargura();
				Double altura = materialBean.getAltura();
				Double comprimento = materialBean.getComprimento();
				Pedidovendamaterial pedidovendamaterial = materialBean.getPedidovendamaterial();
				Pneu pneu = materialBean.getPneu();
				String observacao = materialBean.getObservacao();
				List<GerarProducaoConferenciaMaterialItemBean> listaItem = materialBean.getListaItem();
				Unidademedida unidademedida = materialBean.getUnidademedidaAux();
				
				for (GerarProducaoConferenciaMaterialItemBean itemBean : listaItem) {
					Timestamp data = itemBean.getData();
					Double qtde = itemBean.getQtde();
					
					if(qtde != null && qtde > 0 && data != null){
						Producaoagendamaterial producaoagendamaterial = new Producaoagendamaterial();
						producaoagendamaterial.setMaterial(material);
						producaoagendamaterial.setQtde(qtde);
						producaoagendamaterial.setLargura(largura);
						producaoagendamaterial.setAltura(altura);
						producaoagendamaterial.setComprimento(comprimento);
						producaoagendamaterial.setProducaoetapa(producaoetapa);
						producaoagendamaterial.setObservacao(observacao);
						producaoagendamaterial.setPedidovendamaterial(pedidovendamaterial);
						producaoagendamaterial.setPneu(pneu);
						producaoagendamaterial.setUnidademedida(unidademedida);
						
						boolean achou = false;
						for (Producaoagenda producaoagenda : listaProducaoagenda) {
							Timestamp data_producaoagenda = producaoagenda.getDtentrega();
							Pedidovenda pedidovenda_producaoagenda = producaoagenda.getPedidovenda();
							
							if(data.equals(data_producaoagenda) && pedidovenda.getCdpedidovenda().equals(pedidovenda_producaoagenda.getCdpedidovenda())){
								achou = true;
								
								producaoagenda.getListaProducaoagendamaterial().add(producaoagendamaterial);
								break;
							}
						}
						
						if(!achou){
							Producaoagenda producaoagenda = new Producaoagenda();
							producaoagenda.setCliente(cliente);
							producaoagenda.setPedidovenda(pedidovenda);
							producaoagenda.setEmpresa(empresa);
							producaoagenda.setDtentrega(data);
							producaoagenda.setProducaoagendatipo(producaoagendatipo);
							
							Set<Producaoagendamaterial> listaProducaoagendamaterial = new ListSet<Producaoagendamaterial>(Producaoagendamaterial.class);
							listaProducaoagendamaterial.add(producaoagendamaterial);
							producaoagenda.setListaProducaoagendamaterial(listaProducaoagendamaterial);
							
							listaProducaoagenda.add(producaoagenda);
						}
					}
				}
			}
			
			Map<Pedidovenda, List<Producaoagenda>> mapPedidovendaHistorico = new HashMap<Pedidovenda, List<Producaoagenda>>();
			
			boolean gerouProducao = false;
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				gerouProducao = true;
				
				producaoagendaService.saveOrUpdate(producaoagenda);
				
				Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
				producaoagendahistorico.setProducaoagenda(producaoagenda);
				producaoagendahistorico.setObservacao("Criado");
				producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
				
				if(mapPedidovendaHistorico.containsKey(producaoagenda.getPedidovenda())){
					List<Producaoagenda> list = mapPedidovendaHistorico.get(producaoagenda.getPedidovenda());
					list.add(producaoagenda);
					mapPedidovendaHistorico.put(producaoagenda.getPedidovenda(), list);
				} else {
					List<Producaoagenda> list = new ArrayList<Producaoagenda>();
					list.add(producaoagenda);
					mapPedidovendaHistorico.put(producaoagenda.getPedidovenda(), list);
				}
			}
			
			Set<Entry<Pedidovenda, List<Producaoagenda>>> entrySet = mapPedidovendaHistorico.entrySet();
			
			for (Entry<Pedidovenda, List<Producaoagenda>> entry : entrySet) {
				Pedidovenda pedidovenda = entry.getKey();
				
				Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
				pedidovendahistorico.setAcao("Produ��o gerada");
				pedidovendahistorico.setPedidovenda(pedidovenda);
				pedidovendahistorico.setObservacao("Agenda(s) de produ��o(��es) geradas: " + producaoagendaService.makeLinkHistoricoProducaoagenda(entry.getValue()));
				
				pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
				
//				pedidovendaService.updateSituacao(pedidovenda.getCdpedidovenda().toString(), Pedidovendasituacao.PREVISTA);
			}
			
			if(gerouProducao){
				request.addMessage("Agenda de Produ��o gerada com sucesso.");
				if(SinedUtil.isRecapagem() && !SinedUtil.isRecapagemChaodeFabrica()){
					return new ModelAndView("redirect:/producao/crud/Producaoagenda?ACAO=produzir&selectedItens=" + CollectionsUtil.listAndConcatenate(listaProducaoagenda, "cdproducaoagenda", ","));
				}
			} else {
				request.addMessage("Nenhum registro de produ��o foi gerado.", MessageType.WARN);
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o da produ��o: " + e.getMessage());
		}
		
		return sendRedirectToAction("listagem");
	}
	
	
	
	/**
	 * M�todo que abre uma popup para escolha dos itens da grade
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public ModelAndView selecionarItensGrade(WebRequestContext request){
		return new ModelAndView("direct:/crud/popup/popUpSelecionarItensGrade", "bean", vendaService.selecionarItensGrade(request));
	}
	
	/**
	 * M�todo que abre a popup para escolha do novo respons�vel do pedido de venda
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public ModelAndView abrirPopUpTrocarVendedor(WebRequestContext request){
		String id = request.getParameter("cdpedidovenda");
		
		if(id == null || "".equals(id)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Pedidovenda pedidovenda = pedidovendaService.loadForTrocarvendedor(new Pedidovenda(Integer.parseInt(id)));
		Usuario usuario = usuarioService.load(SinedUtil.getUsuarioLogado(), "usuario.cdpessoa, usuario.restricaoclientevendedor");
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		if(usuario != null && usuario.getRestricaoclientevendedor() != null && usuario.getRestricaoclientevendedor()){
			if(pedidovenda != null && pedidovenda.getCliente() != null && pedidovenda.getCliente().getListaClientevendedor() != null &&
					!pedidovenda.getCliente().getListaClientevendedor().isEmpty()){
				for(Clientevendedor clientevendedor : pedidovenda.getCliente().getListaClientevendedor()){
					if(clientevendedor.getColaborador() != null && clientevendedor.getColaborador().getCdpessoa() != null){
						listaColaborador.add(clientevendedor.getColaborador());
					}
				}
			}
		}		
		
		request.setAttribute("exibirCombo", listaColaborador != null && !listaColaborador.isEmpty());
		request.setAttribute("listaColaboador", listaColaborador);
		request.setAttribute("pathTrocavendedor", "/faturamento/crud/Pedidovenda");
		request.setAttribute("nomeBeanTrocavendedor", "Pedido de Venda");
		
		TrocavendedorBean trocavendedorBean = new TrocavendedorBean(pedidovenda.getCdpedidovenda(), pedidovenda.getColaborador());
		return new ModelAndView("direct:/crud/popup/popUpTrocarVendedor", "bean", trocavendedorBean);
	}
	
	/**
	 * M�todo que salva a troca de vendedor do pedido de venda
	 *
	 * @see br.com.linkcom.sined.geral.service.PedidovendaService#processaTrocaVendedor(Pedidovenda pedidovendaWithColaboradorAtual)
	 *
	 * @param request
	 * @param trocavendedorBean
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void salvaTrocaVendedor(WebRequestContext request, TrocavendedorBean trocavendedorBean){
		if(trocavendedorBean.getId() == null){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		if(trocavendedorBean.getColaboradornovo() == null || trocavendedorBean.getColaboradornovo().getCdpessoa() == null){
			request.addError("Nenhum colaborador selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		Pedidovenda pedidovenda = new Pedidovenda(trocavendedorBean.getId());
		pedidovenda.setColaborador(trocavendedorBean.getColaboradornovo());
		
		pedidovendaService.processaTrocaVendedor(pedidovenda);
		
		request.addMessage("Vendedor alterado com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda?ACAO=consultar&cdpedidovenda=" + trocavendedorBean.getId());
	}
	
	/**
	 * M�todo ajax que verifica se o cliente tem d�bito(s) acima do permitido
	 *
	 * @see br.com.linkcom.sined.geral.service.ClienteService#carregarTaxapedidovendaCreditolimitecompraByCliente(Cliente cliente)
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxValordebitoContasEmAbertoByCliente(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxValordebitoContasEmAbertoByCliente(request, cliente);
	}	
	
	/**
	 * 
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, Pedidovenda bean){
		return pedidovendaService.ajaxCamposAdicionais(request, bean);
	}
	
	/**
	 * M�todo que busca uma lista de {@link Pedidovendatipo} e a renderiza em formato html para ser inserido na p�gina.
	 * @param request
	 * @return
	 */
	public ModelAndView ajaxLoadListaCampoExtra(WebRequestContext request){
		String msgError = "<null>";
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		JsonModelAndView json = new JsonModelAndView();
		
		if(cdpedidovendatipo != null && !cdpedidovendatipo.equals("")){
			Pedidovendatipo tipo = new Pedidovendatipo();
			tipo.setCdpedidovendatipo(Integer.valueOf(cdpedidovendatipo));
			List<Campoextrapedidovendatipo> lista = campoextrapedidovendatipoService.findByTipo(tipo);
		
			StringBuilder html = new StringBuilder();
			if(lista != null && !lista.isEmpty()){
				html.append("<option value='<null>'></option>");
				for(Campoextrapedidovendatipo campo : lista){
					html.append("<option value='br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo[cdcampoextrapedidovendatipo=")
						.append(campo.getCdcampoextrapedidovendatipo().toString())
						.append("]'>")
						.append(campo.getNome())
						.append("</option>");
				}
			}
			else{
				msgError = "N�o existem dados para este tipo.";
				return json.addObject("msgError", msgError);
			}
			
			return json.addObject("html", html.toString()).addObject("msgError", msgError);
		}
		else {
			msgError = "Tipo n�o definido";
			return json.addObject("msgError", msgError);
		}
	}

	public ModelAndView abrirPopUpColetarProducao(WebRequestContext request){
		String selectedItens = SinedUtil.getItensSelecionados(request);
		if(selectedItens == null || "".equals(selectedItens)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findForColetarProducao(selectedItens);
		if(listaPedidovenda == null || listaPedidovenda.isEmpty()){
			request.addError("N�o existe material de coleta no(s) pedido(s) selecionado(s).");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		List<Movimentacaoestoque> listaMovimentacaoestoqueColeta = movimentacaoestoqueService.findByPedidovenda(selectedItens);

		boolean obrigarinformarnotafiscalcoleta = false;
		for (Pedidovenda pedidovenda : listaPedidovenda) {
			if(pedidovenda.getPedidovendatipo() != null &&
				pedidovenda.getPedidovendatipo().getObrigarinformarnotafiscalcoleta() != null &&
				pedidovenda.getPedidovendatipo().getObrigarinformarnotafiscalcoleta()){
				obrigarinformarnotafiscalcoleta = true;
				break;
			}
		}
		
		listaPedidovenda = pedidovendaService.createListaPedidovendaForColetarProducao(listaPedidovenda, listaMovimentacaoestoqueColeta);
		
		if(listaPedidovenda == null || listaPedidovenda.isEmpty()){
			request.addError("A coleta j� foi realizada para o(s) pedido(s) selecionado(s).");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		Map<Pedidovenda, List<Localarmazenagem>> mapaLocal = new HashMap<Pedidovenda, List<Localarmazenagem>>();
		for(Pedidovenda pedidovenda : listaPedidovenda){
			mapaLocal.put(pedidovenda, localarmazenagemService.loadForVenda(pedidovenda.getEmpresa()));
			if(pedidovenda.getLocalarmazenagem() != null && pedidovenda.getListaPedidovendamaterial() != null && 
					!pedidovenda.getListaPedidovendamaterial().isEmpty()){
				for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial())
					pedidovendamaterial.setLocalarmazenagemcoleta(pedidovenda.getLocalarmazenagem());
			}
		}		
		
		ColetarProducaoBean coletarProducaoBean = new ColetarProducaoBean();
		coletarProducaoBean.setListaPedidovenda(listaPedidovenda);
		coletarProducaoBean.setProducaoautomatica("true".equals(request.getParameter("producaoautomatica")));
		coletarProducaoBean.setRegistrarEntradaFiscal(obrigarinformarnotafiscalcoleta);
		request.setAttribute("OBRIGARINFORMARNOTAFISCALCOLETA", obrigarinformarnotafiscalcoleta);
		return new ModelAndView("direct:/crud/popup/popUpColetaProducao", "bean", coletarProducaoBean);
	}
	
	public ModelAndView salvaColetarProducao(WebRequestContext request, ColetarProducaoBean coletarProducaoBean){
		if(coletarProducaoBean.getListaPedidovenda() == null || coletarProducaoBean.getListaPedidovenda().isEmpty()){
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		String whereInPedidovenda = CollectionsUtil.listAndConcatenate(coletarProducaoBean.getListaPedidovenda(), "cdpedidovenda", ",");
		
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findForColetarProducao(whereInPedidovenda);
		List<Movimentacaoestoque> listaMovimentacaoestoqueColeta = movimentacaoestoqueService.findByPedidovenda(whereInPedidovenda);
		listaPedidovenda = pedidovendaService.createListaPedidovendaForColetarProducao(listaPedidovenda, listaMovimentacaoestoqueColeta);
		
		if(listaPedidovenda == null || listaPedidovenda.isEmpty()){
			request.addError("A coleta j� foi realizada para o(s) pedido(s) selecionado(s).");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		Empresa empresa = null;
		if(coletarProducaoBean.getListaPedidovenda() != null && !coletarProducaoBean.getListaPedidovenda().isEmpty()){
			for(Pedidovenda pedidovenda : coletarProducaoBean.getListaPedidovenda()){
				if(pedidovenda.getEmpresa() != null){
					empresa = pedidovenda.getEmpresa();
					break;
				}
			}
		}
		
		try {
			pedidovendaService.saveColetarProducao(coletarProducaoBean, null);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("N�o foi poss�vel executar coleta para produ��o. \n" + e.getMessage());
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		if(coletarProducaoBean.getRegistrarEntradaFiscal() != null && coletarProducaoBean.getRegistrarEntradaFiscal()){
			if(coletarProducaoBean.getArquivoxmlnfe() != null || (coletarProducaoBean.getChaveacesso() != null && !"".equals(coletarProducaoBean.getChaveacesso()))){
				request.getSession().setAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL_COLETA", coletarProducaoBean.getArquivoxmlnfe());
				request.getSession().setAttribute("CHAVE_ACESSO_RECEBER_ENTRADAFISCAL_COLETA", coletarProducaoBean.getChaveacesso());
				request.getSession().setAttribute("HTML_CHAVE_ACESSO_RECEBER_ENTRADAFISCAL_COLETA", coletarProducaoBean.getHtmlChaveacesso());
				SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal?ACAO=receberEntradafiscal&receberxmlcoleta=true&fromPedidoColeta=true&whereInPedidovenda=" + whereInPedidovenda + "&producaoautomatica=" + coletarProducaoBean.getProducaoautomatica());
			}else {
				SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal?ACAO=criar&fromPedidoColeta=true&tipoOperacaoNotaFiscal=entrada&whereInPedidovenda=" + whereInPedidovenda + "&producaoautomatica=" + coletarProducaoBean.getProducaoautomatica());
			}
			return null; 
		} else if(coletarProducaoBean.getRegistrarNotaFiscal() != null && coletarProducaoBean.getRegistrarNotaFiscal()){
			SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto?ACAO=criar&fromPedidoColeta=true&whereInPedidovenda=" + whereInPedidovenda + "&producaoautomatica=" + coletarProducaoBean.getProducaoautomatica() + "&tipoOperacaoNotaFiscal=" + (coletarProducaoBean.getTipooperacaonota() != null ? coletarProducaoBean.getTipooperacaonota().name() : ""));
			return null; 
		}
		
		boolean possui = false;
		if(empresa != null){
			possui = empresaService.possuiComprovanteConfiguravel(ComprovanteConfiguravel.TipoComprovante.COLETA, empresa );
		}
		if(possui){
			String param = "";
			if(coletarProducaoBean.getProducaoautomatica() != null &&  coletarProducaoBean.getProducaoautomatica()){
				param = "?ACAO=consultar&cdpedidovenda=" + whereInPedidovenda + "&producaoautomatica=true";
			}
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<html><body>" +
					"<script>" +
					" parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/crud/Pedidovenda" + param + "';" +
					" window.open('../../faturamento/crud/Pedidovenda?ACAO=gerarComprovanteColeta&whereIn="+ whereInPedidovenda + "');" +
					"</script>" +
					"</body></html>"
					);
			if(coletarProducaoBean.getRegistrarEntradaFiscal() == null || !coletarProducaoBean.getRegistrarEntradaFiscal())
				return null;
		}
		
		request.addMessage("Coleta efetuada com sucesso.");
		
		if(coletarProducaoBean.getProducaoautomatica() != null &&  coletarProducaoBean.getProducaoautomatica()){
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda?ACAO=abrirGerarProducaoConferencia&selectedItens=" + whereInPedidovenda);
		} else {
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
		}
		return null; 
	}
	
	public ModelAndView gerarOrdemServico(WebRequestContext request){
		String whereIn = request.getParameter("cdpedidovenda");
		if(pedidovendaService.havePedidovendaSituacao(whereIn, Pedidovendasituacao.AGUARDANDO_APROVACAO,Pedidovendasituacao.CANCELADO)){
			request.addError("N�o � poss�vel gerar uma ordem de servi�o para uma venda com a situa��o 'AGUARDANDO APROVACAO' ou 'CANCELADO'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Integer cdpedidovenda = Integer.parseInt(request.getParameter("cdpedidovenda"));
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>window.open('../../servicointerno/crud/Requisicao?ACAO=" + CRIAR + "&cdpedidovenda=" + cdpedidovenda + "','page','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=no,width=1020,height=800');parent.$.akModalRemove(true);</script>");
		return null;
	}
	
	public ModelAndView ajaxBuscaObservaocaoByCliente(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaObservaocaoByCliente(request, cliente);
	}
	
	public void buscarMaterialIdentificadorTabelaprecoAJAX(WebRequestContext request, Pedidovenda pedidovenda){
		Venda venda = new Venda();
		venda.setCodigo(pedidovenda.getCodigo());
		venda.setCliente(pedidovenda.getCliente());
		venda.setPrazopagamento(pedidovenda.getPrazopagamento());
		venda.setEmpresa(pedidovenda.getEmpresa());
		venda.setDtvenda(pedidovenda.getDtpedidovenda());
		venda.setPedidovendatipo(pedidovenda.getPedidovendatipo());
		vendaService.buscarMaterialIdentificadorTabelaprecoAJAX(request, venda);
	}
	
	/**
	 * Chamada Ajax para carregamento das Formas e Prazos de Pagameneto para determinado Cliente.
	 *
	 * @param request
	 * @param cliente
	 * @author Marcos Lisboa
	 */
	public ModelAndView ajaxLoadFormaAndPrazoPagamento(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxLoadFormaAndPrazoPagamento(request, cliente);
	}
	
	/**
	 * Ajax para verificar restri��o do cliente
	 *
	 * @param request
	 * @author Rodrigo Freitas
	 * @since 04/09/2014
	 */
	public void ajaxVerificaRestricao(WebRequestContext request){
		restricaoService.verificaRestricaoCliente(request);
	}

	/**
	 * Busca o nome do vendedor principal do cliente
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/09/2014
	 */
	public ModelAndView ajaxBuscaVendedorprincipalByCliente(WebRequestContext request, Cliente cliente){
		return clientevendedorService.actionVendedorprincipalVendaPedido(cliente);
	}
	
	/**
	 * Ajax que busca o saldo do vale compra pelo cliente
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/09/2014
	 */
	public ModelAndView ajaxBuscaSaldovalecompraByCliente(WebRequestContext request, Cliente cliente){
		String param_cdpedidovenda = request.getParameter("cdpedidovenda");
		Pedidovenda pedidovenda = null;
		if(StringUtils.isNotEmpty(param_cdpedidovenda)){
			try {
				pedidovenda = new Pedidovenda(Integer.parseInt(param_cdpedidovenda));
			} catch (Exception e) {}
		}
		return valecompraService.actionSaldovalecompraVendaPedidoOrcamento(pedidovenda, cliente);
	}
	
	
	/**
	 * M�todo que verifica se o desconto concebido � maior que o desconto do prazo de pagamento.
	 *
	 * @param request
	 * @param pedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/10/2014
	 */
	public ModelAndView ajaxVerificaDesconto(WebRequestContext request, Pedidovenda pedidovenda){
		return vendaService.ajaxVerificaDescontoByPrazopagamento(pedidovenda.getPrazopagamento(), pedidovenda.getPercentualdesconto());
	}
	
	/**
	 * M�todo que armazena os dados necess�rios para executar o autocomplete de lote.
	 * 
	 * @param request
	 * @author Rafael Salvio
	 */
	public void ajaxSetDadosSessaoForLoteAutocomplete(WebRequestContext request){
		pedidovendaService.ajaxSetDadosSessaoForLoteAutocomplete(request, "PEDIDOVENDA");
	}
	
	/**
	* M�todo ajax para gerar as parcelas de representa��o
	*
	* @param request
	* @param venda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public ModelAndView pagamentoRepresentacao(WebRequestContext request, Venda venda){	
		AjaxRealizarVendaPagamentoRepresentacaoBean bean = vendaService.pagamentoRepresentacao(venda, venda.getEmpresa());
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", bean);
		return json;
	}
	
	/**
	* M�todo ajax para buscar o percentual de comiss�o
	*
	* @param request
	* @param venda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxPercentualComissaoRepresentacao(WebRequestContext request, Venda venda){
		return VendaService.getInstance().ajaxPercentualComissaoRepresentacao(venda.getEmpresa(), venda.getFornecedor());
	}
	
	/**
	* M�todo que abre a popup de kit flexivel
	*
	* @param request
	* @param bean
	* @return
	* @since 31/03/2015
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpKitFlexivel(WebRequestContext request, KitflexivelBean bean){
		return vendaService.abrirPopUpKitFlexivel(request, bean);
	}
	
	public ModelAndView abrirPopUpKitFlexivelComposicao(WebRequestContext request, KitflexivelBean bean){
		return vendaService.abrirPopUpKitFlexivelComposicao(request, bean);
	}
	
	public ModelAndView getUnidademedida(WebRequestContext request, Material bean){
		return vendaService.getUnidademedida(request, bean);
	}
	
	/**
	 * M�todo que abre a popup para consulta e edi��o da porcentagem da comiss�o das ag�ncias.
	 *
	 * @param request
	 * @return
	 * @author Jo�o Vitor
	 * @since 02/06/2015
	 */
	public ModelAndView abrirPopUpComissaoAgencia(WebRequestContext request, Vendamaterial vendamaterial){
		return vendaService.abrirPopUpComissaoAgencia(request, vendamaterial);
	}
	
	public ModelAndView changeFornecedoragencia(WebRequestContext request, FornecedoragenciaBean fornecedoragenciaBean) {
		return vendaService.changeFornecedoragencia(request, fornecedoragenciaBean);
	}
	
	/**
	* M�todo ajax que busca o local da empresa, caso a empresa possua apenas um local
	*
	* @param request
	* @return
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxBuscaLocalArmazenagemUnico(WebRequestContext request){	
		return vendaService.ajaxBuscaLocalArmazenagemUnico(request);
	}
	
	/**
	* M�todo M�todo ajax que retorna o valor aproximado de imposto da venda
	*
	* @param request
	* @param venda
	* @return
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularValorAproximadoImposto(WebRequestContext request, Pedidovenda venda){
		pedidovendaService.calcularValorAproximadoImposto(venda);
		return new JsonModelAndView().addObject("bean", venda);
	}
	
	/**
	* M�todo ajax para calcular o imposto na venda
	*
	* @see 
	*
	* @param request
	* @param impostovenda
	* @return
	* @since 11/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularImposto(WebRequestContext request, Impostovenda impostovenda){	
		return new JsonModelAndView().addObject("bean", vendaService.calcularImposto(impostovenda));
	}
	
	/**
	* M�todo ajax para calcular o peso do material
	*
	* @see br.com.linkcom.sined.modulo.faturamento.controller.crud.PedidovendaCrud#ajaxCalcularPesoMaterial(WebRequestContext request, Material material)
	*
	* @param request
	* @param material
	* @return
	* @since 26/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularPesoMaterial(WebRequestContext request, Material material){
		return vendaService.ajaxCalcularPesoMaterial(request, material);
	}
	
	/**
	 * M�todo ajax que retorna uma lista JSON com os prazos de pagamentos do cliente
	 *
	 * @param request
	 * @return
	 * @since 27/05/2016
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView getFormasPrazosPagamentoClienteJSON(WebRequestContext request){
		boolean addForma = "true".equalsIgnoreCase(request.getParameter("addForma"));
		boolean addPrazo = "true".equalsIgnoreCase(request.getParameter("addPrazo"));
		return clienteService.getFormasPrazosPagamentoClienteAlertaModelAndView(request, addForma, addPrazo);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param request
	* @param empresa
	* @return
	* @since 18/06/2016
	* @author Luiz Fernando
	*/
	public ModelAndView carregaInfEmpresa(WebRequestContext request, Empresa empresa){
		return vendaService.carregaInfEmpresa(request, empresa);
	}
	
	/**
	* M�todo ajax que verifica se existe algum item com garantia no pedido de venda origem
	*
	* @param request
	* @param empresa
	* @return
	* @since 22/06/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificarPedidovendaorigem(WebRequestContext request, Pedidovenda pedidovenda){
		boolean pedidovendaorigeminvalido = false;
		if(pedidovenda != null && Util.objects.isPersistent(pedidovenda.getPedidovendaorigem()) &&
				!pedidovendaService.existeItemComGarantia(pedidovenda.getPedidovendaorigem().getCdpedidovenda())){
			pedidovendaorigeminvalido = true;
		}
		return new JsonModelAndView().addObject("pedidovendaorigeminvalido", pedidovendaorigeminvalido);
	}
	
	/**
	* M�todo ajax para validar a garantia do pedido de venda
	*
	* @param request
	* @param pedidovenda
	* @return
	* @since 23/06/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxValidaPedidoGarantia(WebRequestContext request, Pedidovenda pedidovenda){
		return pedidovendaService.ajaxValidaPedidoGarantia(request, pedidovenda);
	}
	
	/**
	 * M�todo que abre a popup para troca de material por uma material similar
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirTrocaMaterial(WebRequestContext request, Pedidovenda pedidoVenda){
		Venda venda = new Venda();
		venda.setEmpresa(pedidoVenda.getEmpresa());
		venda.setCliente(pedidoVenda.getCliente());
		venda.setMaterialtabelapreco(null);
		venda.setProjeto(pedidoVenda.getProjeto());
		venda.setPrazopagamento(pedidoVenda.getPrazopagamento());
		venda.setPedidovendatipo(pedidoVenda.getPedidovendatipo());
		venda.setCodigo(pedidoVenda.getCodigo().toString());
		return vendaService.abrirTrocaMaterial(request, venda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#abrirPopUpInfoFinanceiraCliente(WebRequestContext request, Cliente cliente)
	*
	* @param request
	* @param cliente
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpInfoFinanceiraCliente(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInfoFinanceiraCliente(request, cliente);
	}
	
	public ModelAndView ajaxValidaLoteestoqueItensSync(WebRequestContext request){
		return vendaService.ajaxValidaLoteestoqueItensSync(request);
	}
	
	public ModelAndView converteUnidademedida(WebRequestContext request, Material bean){
		return vendaService.converteUnidademedida(request, bean);
	}
 	
	public ModelAndView ajaxBuscaInfPrazo(WebRequestContext request, Prazopagamento prazopagamento){
		return vendaService.ajaxBuscaInfPrazo(request, prazopagamento);
	}
	
	public ModelAndView ajaxAtualizarvalorvenda(WebRequestContext request, Material material){
		return vendaService.ajaxAtualizarvalorvenda(request, material);
	}
	
	public ModelAndView comboBoxTerceiro(WebRequestContext request, Venda venda) {
		return vendaService.comboBoxTerceiro(request, venda);
	}
	
	public ModelAndView buscaPrazoEntrega(WebRequestContext request, Venda venda){
		return vendaService.buscaPrazoEntrega(request, venda);
	}
	
	/**
	* M�todo que abre uma popup para escolha do comprovante alternativo
	*
	* @param request
	* @return
	* @since 31/01/2017
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpComprovanteAlternativo(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if (whereIn == null || "".equals(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		request.setAttribute("entrada", Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"));
		request.setAttribute("listaComprovante", comprovanteConfiguravelService.findComprovanteAlternativo(whereIn, TipoComprovante.PEDIDO_VENDA));
		return new ModelAndView("direct:/crud/popup/popUpComprovanteAlternativo", "bean", new ComprovanteConfiguravelAlternativo(whereIn));
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView abrirPopUpInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInformacaoClienteVenda(request, cliente);
	}
	
	public void ajaxSugerirPrazopagamentoCliente(WebRequestContext request){
		vendaService.ajaxSugerirPrazopagamentoCliente(request);
	}
	
	public ModelAndView ajaxHabilitaCamposDeACordoComGrupoMaterial(WebRequestContext request, Pedidovenda bean){
		return materialService.ajaxHabilitaCamposDeACordoComGrupoMaterial(bean.getMaterial());
	}
	
	public ModelAndView ajaxBuscaVendedorPrincipal(WebRequestContext request, Pedidovenda bean){
		return clientevendedorService.ajaxBuscaVendedorPrincipal(request, bean.getCliente());
	}
	
	public ModelAndView abrePopupAjudaVendedorprincipal(WebRequestContext request){
		return vendaService.abrePopupAjudaVendedorprincipal(request);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Pedidovenda form) throws CrudException {
		if(!vendaService.validaVendedorprincipal(form, request)){
			return continueOnAction("entrada", form);
		}
				
		return super.doSalvar(request, form);
	}
	
	public ModelAndView gerarNotaFiscalRetorno(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String urlRetorno = "/faturamento/crud/Pedidovenda" + (entrada ? "?ACAO=consultar&cdpedidovenda=" + whereIn : "");
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item encontrado.");
			return new ModelAndView("redirect:" + urlRetorno);
		}
		
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findForNotaRetorno(whereIn);
		if(SinedUtil.isListEmpty(listaPedidovenda)){
			request.addError("Nenhum item encontrado.");
			return new ModelAndView("redirect:" + urlRetorno);
		}
		
		boolean erro = false;
		StringBuilder whereInColeta = new StringBuilder();
		String whereInColetaPedido;
		for(Pedidovenda pedidovenda : listaPedidovenda){
			if(SinedUtil.isListEmpty(pedidovenda.getListaColeta())){
				erro = true;
				request.addError("Pedido de Venda " + pedidovenda.getCdpedidovenda() + " n�o possui coleta gerada.");
			}else {
				whereInColetaPedido = CollectionsUtil.listAndConcatenate(pedidovenda.getListaColeta(), "cdcoleta", ",");
				if (coletaService.isColetaPossuiNotaTodosItensSemSaldo(whereInColetaPedido, Tipooperacaonota.SAIDA)){
					erro = true;
					request.addError("Nota fiscal de retorno j� emitida para todos os materiais coletados. Pedido de venda: " + pedidovenda.getCdpedidovenda() + ".");
				}else {
					whereInColeta.append(whereInColetaPedido).append(",");
				}
			}
		}
		
		if(erro){
			return new ModelAndView("redirect:" + urlRetorno);
		}
		
		if(listaPedidovenda.size() == 1){
			SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto?ACAO=criar&notaFiscalRetornoPedido=true&tipoOperacaoNotaFiscal=saida&whereInPedidovendaRetorno=" + whereIn + "&whereInColeta=" + whereInColeta.substring(0, whereInColeta.length()-1));
			return null; 
		}else {
			try {
				pedidovendaService.gerarNotaFiscalRetorno(request, listaPedidovenda);
				request.addMessage("Nota(s) fiscal(is) de retorno gerada com sucesso.");
			} catch (Exception e) {
				request.addError(e.getMessage());
			}
			return new ModelAndView("redirect:" + urlRetorno);
		}
	}
	
	public ModelAndView msgNotaItensColeta(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		request.addError("Nota fiscal de retorno j� emitida para todos os materiais coletados.");
		return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=listagem");
	}
	
	public ModelAndView abrePopupSelecaoServicogarantido(WebRequestContext request, Pedidovenda pedidovenda){
		return pedidovendaService.abrePopupSelecaoServicogarantido(request, pedidovenda);
	}
	
	public ModelAndView ajaxExistsGarantiareforma(WebRequestContext request, Cliente cliente){
		return garantiareformaService.ajaxExistsGarantiareforma(request, cliente);
	}
	
	public ModelAndView abrePopupSelecaoGarantiareformaCliente(WebRequestContext request, Cliente cliente){
		return garantiareformaService.abrePopupSelecaoGarantiareformaCliente(request, cliente);
	}
	
	public ModelAndView recalcularComissao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String[] ids = whereIn.split(",");
		
		for (int i = 0; i < ids.length; i++) {
			if(!pedidovendaService.havePedidovendaSituacao(ids[i], Pedidovendasituacao.CANCELADO, Pedidovendasituacao.AGUARDANDO_APROVACAO)){
				Pedidovenda pedidovenda = new Pedidovenda(Integer.parseInt(ids[i]));
				
				documentocomissaovendaService.recalcularComissaopedidovenda(pedidovenda, null);
				documentocomissaovendarepresentacaoService.recalcularComissaopedidovendarepresentacao(pedidovenda, null);
			}
		}
		
		request.addMessage("Comiss�o recalculada com sucesso.");
		
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		if(entrada){
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=consultar&cdpedidovenda=" + whereIn);
		}
		
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView buscarPedidoVendaTipoAjax(WebRequestContext request, Pedidovenda pedidovenda) {
		return pedidovendatipoService.buscarPedidoVendaTipoAjax(pedidovenda.getPedidovendatipo());
	}

	
	public void ordenaPrazoPagamento(PrazopagamentoFiltro filtro,
			List<Prazopagamento> listaPrazoPagamento) {

		if ("prazopagamento.nome".equals(filtro.getOrderBy())) {
			if (filtro.isAsc()) {
				Collections.sort(listaPrazoPagamento, new Comparator<Prazopagamento>(){
					public int compare(Prazopagamento o1, Prazopagamento o2) {
						return o1.getNome().compareTo(o2.getNome());
					}
				});
			} else {
				Collections.sort(listaPrazoPagamento, new Comparator<Prazopagamento>(){
					public int compare(Prazopagamento o1, Prazopagamento o2) {
						return o2.getNome().compareTo(o1.getNome());
					}
				});
			}
		}
	}
	//faturamento/crud/Pedidovenda?ACAO=criarPagamentosVendaSemPagamento&whereInVendaAjuste=6559&hash=d7bb2f06934a9be103d0ad607d836ddc&qtdeDocumento=1
	public void criarPagamentosVendaSemPagamento(WebRequestContext request){
		//processo utilizado somente para ajustar venda que n�o tem conta a receber
		String whereInVenda = request.getParameter("whereInVendaAjuste");
		String senha = request.getParameter("hash");
		String qtdeDocumento = request.getParameter("qtdeDocumento");
		
		String hash1 = Util.crypto.makeHashMd5("linkComAdminAjustaVenda");
		String hash2 = Util.crypto.makeHashMd5(hash1);
		
		if(hash2.equals(senha)){//
		
			for(String idVenda : whereInVenda.split(",")){
				Pedidovenda form = pedidovendaService.loadForEntrada(new Pedidovenda(Integer.parseInt(idVenda)));
				
				if(form.getPedidovendatipo() == null || (!Boolean.TRUE.equals(form.getPedidovendatipo().getReserva()) ||
						GeracaocontareceberEnum.NAO_GERAR_CONTA.equals(form.getPedidovendatipo().getGeracaocontareceberEnum()))){
					continue;
				}
						
				form.setListaPedidovendapagamento(pedidovendapagamentoService.findPedidovendaPagamentoByPedidovenda(form));
				
				if(SinedUtil.isListEmpty(form.getListaPedidovendapagamento())) continue;
				Integer i = 0 ;
				for(Pedidovendapagamento vendapagamento : form.getListaPedidovendapagamento()){
					if(vendapagamento.getDocumento() != null || vendapagamento.getDocumentoantecipacao() != null){

						i++;
					}
					
				}
				if(!qtdeDocumento.equals(i.toString())){
					continue;
				}
				
				if (form.getColaborador()!=null && form.getColaborador().getCdpessoa()!=null){
					form.setColaborador(colaboradorService.loadWithoutPermissao(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
				}
				if (form.getAgencia()!=null && form.getAgencia().getCdpessoa()!=null){
					form.setAgencia(fornecedorService.load(form.getAgencia(), "fornecedor.cdpessoa, fornecedor.nome"));
				}
				if (form.getLocalarmazenagem()!=null && form.getLocalarmazenagem().getCdlocalarmazenagem()!=null){
					form.setLocalarmazenagem(localarmazenagemService.load(form.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
				}
				if (form.getCliente()!=null && form.getCliente().getCdpessoa()!=null){
					form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome"));
					
					if(form.getContato() != null && form.getContato().getCdpessoa() != null){
						form.setContato(contatoService.load(form.getContato(), "contato.cdpessoa, contato.nome"));
					}
				}
				if (form.getEmpresa() !=null && form.getEmpresa().getCdpessoa()!=null){
					form.setEmpresa(empresaService.load(form.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia"));
				}
				
				form.setListaPedidovendapagamentorepresentacao(pedidovendapagamentorepresentacaoService.findPedidovendaPagamentoRepresentacaoByPedidovenda(form));
				form.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidovenda(form));
				form.setListaPedidovendahistorico(pedidovendahistoricoService.findByPedidovenda(form));
				form.setListaPedidovendamaterialmestre(pedidovendamaterialmestreService.findByPedidovenda(form));
				
				Empresa empresa = empresaService.loadComContagerenciaiscompra(form.getEmpresa());
				List<Rateioitem> listarateioitem = pedidovendaService.prepareAndSaveVendaMaterial(form, false, empresa, false, null);
				pedidovendaService.prepareAndSaveReceita(form, listarateioitem, empresa);
			}
		}
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
	}
	
	public ModelAndView ajaxBuscaFaixaMarkup(WebRequestContext request, Pedidovenda venda){
		return materialFaixaMarkupService.findJsonFaixaMarkup(venda.getMaterial(), venda.getUnidademedida(), venda.getEmpresa());
	}
	
	public ModelAndView ajaxAbrePopupHistoricoPrecoMaterial(WebRequestContext request, MaterialHistoricoPrecoFiltro filtro){
		return materialService.ajaxAbrePopupHistoricoPrecoMaterial(request, filtro);
	}
	
	public ModelAndView ajaxMaterialProducaoOrKitOrGrade(WebRequestContext request, Material material){
		return materialService.ajaxMaterialProducaoOrKitOrGrade(request, material);
	}
	
	public void buscarSugestaoUltimoValorVendaAJAX(WebRequestContext request){
		vendamaterialService.buscarSugestaoUltimoValorVendaAJAX(request);
	}
	
	public ModelAndView preencherObservacaoPadrao(WebRequestContext request, Pedidovendatipo pedidoVendaTipo) {
		return pedidovendaService.preencherObservacaoPadrao(request, pedidoVendaTipo);
	}
	
	public ModelAndView verificarPedidoVendaTipoBloqueioQuantidade(WebRequestContext request) {
		return pedidovendatipoService.verificarPedidoVendaTipoBloqueioQuantidade(request);
	}
	
	public ModelAndView recalcularFaixaMarkup(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhuma venda foi selecionada."); 
		}

		pedidovendaService.recalcularFaixaMarkup(request, whereIn);
		request.addMessage("Rec�lculo de faixa de markup realizado com sucesso.");
		if("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=consultar&cdpedidovenda="+whereIn);
		}
		return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=listagem");
	}
	
	public ModelAndView exibirConfirmGerarValeCompra(WebRequestContext request, Pedidovendatipo pedidoVendaTipo) {
		return pedidovendaService.exibirConfirmGerarValeCompra(request, pedidoVendaTipo);
	}
	
	public ModelAndView findForAutocompletePedidovendacomodatoWSVenda(WebRequestContext request, GenericSearchBean bean){
		if(bean.getValue() == null || bean.getValue().toString() == ""){
			return new ModelAndView();
		}
		Integer cdPedidoVenda = bean.getPedidoVenda() != null? bean.getPedidoVenda().getCdpedidovenda(): null;

		List<PedidoVendaWSBean> lista = PedidovendaService.getInstance().findForAutocompletePedidovendacomodatoWSVenda(bean.getValue().toString(), cdPedidoVenda);
		return new JsonModelAndView()
				.addObject("listaPedidoVenda", lista);
	}

	public ModelAndView ajaxVerificaTemplatesComprovanteRTF(WebRequestContext request, Pedidovenda form){
		SinedUtil.validaObjectWithExecption(form);
		
		ModelAndView retorno = new JsonModelAndView();
		Pedidovenda pedidovenda = pedidovendaService.findForComprovanteRTF(form);
		pedidovendaService.verificaModeloRTF(retorno, pedidovenda.getEmpresa(), TipoComprovanteEnum.PEDIDO_DE_VENDA);
		return retorno;
	}
	
	public ModelAndView abrirSelecaoModelocomprovanteRTF(WebRequestContext request, Pedidovenda form){
		SinedUtil.validaObjectWithExecption(form);
		
		Pedidovenda pedidovenda = pedidovendaService.loadReader(form);
		pedidovendaService.setInfoSelecaoModeloComprovanteRTF(request, pedidovenda.getEmpresa(), TipoComprovanteEnum.PEDIDO_DE_VENDA);
		request.setAttribute("cdpedidovenda", pedidovenda.getCdpedidovenda());
		return new ModelAndView("direct:/crud/popup/emitirComprovanteorcamentoRTF");
	}
	
    public ModelAndView abrePopupImposto(WebRequestContext request, Pedidovendamaterial bean){
        return vendaService.abrePopupImposto(request, bean);
    }
    
    public ModelAndView abrePopupImpostoMestre(WebRequestContext request, Pedidovendamaterialmestre bean){
        return vendaService.abrePopupImposto(request, bean);
    }
	
	public ModelAndView ajaxMostrarTotalImpostos(WebRequestContext request, Pedidovenda bean){
		return vendaService.ajaxMostrarTotalImpostos(request, bean);
	}
	
	public ModelAndView ajaxEnderecoFaturamento(WebRequestContext request, Cliente cliente){
		return enderecoService.ajaxEnderecoFaturamento(request, cliente);
	}
	
	public ModelAndView ajaxBuscarLotesQuantidadeMaterial(WebRequestContext request, Pedidovenda pedidovenda){
		return vendaService.ajaxBuscarLotesQuantidadeMaterial(request, pedidovenda.getListaInclusaoLoteVendaInterface());
	}
	
	public ModelAndView ajaxBuscarQuantidadeMaterialEmLote(WebRequestContext request){
		String cdmaterialString = request.getParameter("cdmaterial");
		String cdlocalarmazenagemString = request.getParameter("cdlocalarmazenagem");
		String cdempresaString = request.getParameter("cdempresa");
		
		Double qtdeDisponivel = 0.0d;

		if(StringUtils.isNotBlank(cdmaterialString)){
			Integer cdmaterial = Integer.parseInt(cdmaterialString);
			Integer cdlocalarmazenagem = null;
			Integer cdpessoa = null;
			if(StringUtils.isNotBlank(cdlocalarmazenagemString)){
				cdlocalarmazenagem = Integer.parseInt(cdlocalarmazenagemString);				
			}
			if(StringUtils.isNotBlank(cdempresaString)){
				cdpessoa = Integer.parseInt(cdempresaString);				
			}
			
			qtdeDisponivel = loteestoqueService.buscarQtdeDisponivelMaterialLotes(cdmaterial, cdlocalarmazenagem, cdpessoa);
		}
		
		ModelAndView retorno = new JsonModelAndView();
		retorno.addObject("qtdeDisponivel", qtdeDisponivel);
		
		return retorno;
	}
	
	public ModelAndView ajaxGetPedidoVendaTipo(WebRequestContext request, Pedidovendatipo pedidovendatipo ){
		ModelAndView view = new JsonModelAndView();
		if(pedidovendatipo != null){
			view.addObject("pedidovendatipo", pedidovendatipoService.load(pedidovendatipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.vendasEcommerce"));
		}
		return view;
	}
	
	
	public ModelAndView ajaxVerificaTipoPedidoVenda(WebRequestContext request, Pedidovendatipo pedidovendatipo ){
		boolean verifica = false;
		ModelAndView view = new JsonModelAndView();
		if(pedidovendatipo != null){
			pedidovendatipo = pedidovendatipoService.load(pedidovendatipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.permitirConfirmarPedidoBaixado");
			if(Boolean.TRUE.equals(pedidovendatipo.getPermitirConfirmarPedidoBaixado())){
				verifica = true;
			}
		}
		return view.addObject("pedidoVendaTipoFlag",verifica);
	}
	
	public ModelAndView ajaxVerificaContasFinanceiroSitua�ao (WebRequestContext request, Pedidovenda pedidovenda ){
		boolean verifica = false;
		ModelAndView view = new JsonModelAndView();
		if(StringUtils.isNotEmpty(pedidovenda.getWhereInConfirmacao())){
			String[] ids = pedidovenda.getWhereInConfirmacao().split(",");
			if(ids.length > 0){
				for(String id : ids){		
					Pedidovenda pedido = pedidovendaService.loadPedidovenda(new Pedidovenda(Integer.parseInt(id)));
					pedidovendaService.verificaContasFinanceiroSitua�ao(pedido);
					if(ContasFinanceiroSituacao.CONTA_EM_ABERTO.equals(pedido.getContasFinanceiroSituacao())){
						verifica = true;
						break;
					}
				}
			}
		}
		
		return view.addObject("verificaContasFinanceiro",verifica);
	}
	
	public ModelAndView salvaPedidoEcommerceVisualizado(WebRequestContext request, Pedidovenda pedidovenda){
		if(Util.objects.isNotPersistent(pedidovenda)){
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		if(!SinedUtil.isIntegracaoTrayCorp()){
			request.addError("Essa a��o s� � permitida para pedidos originados do e-commerce Tray Corp.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		Ecom_PedidoVenda ecom_PedidoVenda = Ecom_PedidoVendaService.getInstance().loadByCdPedidoVenda(pedidovenda.getCdpedidovenda());
		
		if(ecom_PedidoVenda == null){
			request.addError("Essa a��o s� � permitida para pedidos originados do e-commerce Tray Corp.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		Pedidovenda bean = pedidovendaService.loadWithPedidovendatipo(pedidovenda);
		if(Boolean.TRUE.equals(bean.getPedidoEcommerceVisualizado())){
			request.addError("Pedido do e-commerce j� marcado como Visualizado.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
			return null;
		}
		
		boolean isAtualizaEcommerceNaAprovacaoVenda = false;
		Pedidovendatipo tipo = null;
		if (bean.getPedidovendatipo() != null && bean.getPedidovendatipo().getCdpedidovendatipo() != null){
			tipo = pedidovendatipoService.load(bean.getPedidovendatipo());
			isAtualizaEcommerceNaAprovacaoVenda = Boolean.TRUE.equals(tipo.getAtualizarPedidoEcommerceStatusAoAprovar());
		}
		
		pedidovendaService.updateCampo(pedidovenda, "pedidoEcommerceVisualizado", true);
		
		if(isAtualizaEcommerceNaAprovacaoVenda && !pedidovendahistoricoService.isExistsHistoricoAprovacao(bean)){//Deve executar a��o abaixo s� na primeira aprova��o
			pedidovendaService.insertPedidoAprovadoTabelaSincronizacaoEcommerce(bean);
		}
		
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setAcao("Altera��o");
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		pedidovendahistorico.setDtaltera(SinedDateUtils.currentTimestamp());
		pedidovendahistorico.setObservacao(Pedidovendahistorico.VISUALIZADO);
		
		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		
		request.addMessage("Pedido do e-commerce Visualizado.");
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/Pedidovenda");
		return null;
	}
	
	public ModelAndView ajaxVerificaCalcularTicketMedio(WebRequestContext request, Pedidovendatipo pedidovendatipo ){
		return vendaService.ajaxVerificaCalcularTicketMedio(request, pedidovendatipo);
	}
	
	public ModelAndView abrirMaterialDisponivelTransferencia(WebRequestContext request, Venda venda){	
		return pedidovendaService.abrirMaterialDisponivelTransferencia(request, venda);
	}
}