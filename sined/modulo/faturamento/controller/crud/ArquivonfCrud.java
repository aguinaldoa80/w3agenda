package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Arquivonfnotaerro;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivonfService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaerroService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaerrocancelamentoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ArquivonfFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.MarcarArquivonfBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.ZipManipulation;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/faturamento/crud/Arquivonf", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdarquivonf", "arquivoxml", "dtenvio", "configuracaonfe.tipoconfiguracaonfe", "configuracaonfe", "arquivonfsituacao.nome"})
public class ArquivonfCrud extends CrudControllerSined<ArquivonfFiltro, Arquivonf, Arquivonf> {
	
	private ArquivonfnotaerroService arquivonfnotaerroService;
	private ArquivonfnotaerrocancelamentoService arquivonfnotaerrocancelamentoService;
	private ArquivonfService arquivonfService;
	private ArquivonfnotaService arquivonfnotaService;
	
	public void setArquivonfService(ArquivonfService arquivonfService) {
		this.arquivonfService = arquivonfService;
	}
	public void setArquivonfnotaerrocancelamentoService(
			ArquivonfnotaerrocancelamentoService arquivonfnotaerrocancelamentoService) {
		this.arquivonfnotaerrocancelamentoService = arquivonfnotaerrocancelamentoService;
	}
	public void setArquivonfnotaerroService(
			ArquivonfnotaerroService arquivonfnotaerroService) {
		this.arquivonfnotaerroService = arquivonfnotaerroService;
	}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Arquivonf bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Arquivonf bean) throws Exception {
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
		} else super.excluir(request, bean);
	}
	
	@Override
	protected Arquivonf criar(WebRequestContext request, Arquivonf form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void listagem(WebRequestContext request, ArquivonfFiltro filtro) throws Exception {
		List<Arquivonfsituacao> listaSituacao = new ArrayList<Arquivonfsituacao>();
		for (int i = 0; i < Arquivonfsituacao.values().length; i++) {
			listaSituacao.add(Arquivonfsituacao.values()[i]);
		}
		request.setAttribute("listaSituacao", listaSituacao);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Arquivonf form) throws Exception {
		List<Arquivonfnotaerro> listaErro = arquivonfnotaerroService.findByArquivonf(form);
		if(listaErro != null && listaErro.size() > 0){
			request.setAttribute("listaErro", Boolean.TRUE);
			form.setListaArquivonfnotaerro(listaErro);
		}
		
		List<Arquivonfnota> listaArquivonfnota = form.getListaArquivonfnota();
		if(listaArquivonfnota != null){
			for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
				arquivonfnota.setListaArquivonfnotaerrocancelamento(arquivonfnotaerrocancelamentoService.findByArquivonfnota(arquivonfnota));
			}
		}
		
		request.setAttribute("acessoJuazeiro", form.getConfiguracaonfe() != null && form.getConfiguracaonfe().getPrefixowebservice() != null && 
				(Prefixowebservice.JUAZEIRO_PROD.equals(form.getConfiguracaonfe().getPrefixowebservice()) || Prefixowebservice.JUAZEIRO_HOM.equals(form.getConfiguracaonfe().getPrefixowebservice())));
	}
	
	@Override
	protected ListagemResult<Arquivonf> getLista(WebRequestContext request, ArquivonfFiltro filtro) {
		ListagemResult<Arquivonf> listagemResult = super.getLista(request, filtro);
		List<Arquivonf> lista = listagemResult.list();
		
		for (Arquivonf arquivonf : lista) {
			if(arquivonf != null && arquivonf.getArquivoxml() != null && arquivonf.getArquivoxml().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivonf.getArquivoxml().getCdarquivo());
			}
			if(arquivonf != null && arquivonf.getArquivoxmlassinado() != null && arquivonf.getArquivoxmlassinado().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivonf.getArquivoxmlassinado().getCdarquivo());
			}
			if(arquivonf != null && arquivonf.getArquivoretornoenvio() != null && arquivonf.getArquivoretornoenvio().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivonf.getArquivoretornoenvio().getCdarquivo());
			}
			if(arquivonf != null && arquivonf.getArquivoretornoconsulta() != null && arquivonf.getArquivoretornoconsulta().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivonf.getArquivoretornoconsulta().getCdarquivo());
			}
		}
		
		return listagemResult;
	}
	
	/**
	 * Action que salva o arquivo de retorno de Lavras
	 *
	 * @param request
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/12/2012
	 */
	public ModelAndView salvarArquivoRetornoEnvio(WebRequestContext request, Arquivonf arquivonf){
		if(arquivonf.getArquivoretornoenvio() != null){
			try{
				arquivonfService.processaRetornoEnvioLavrasiss(arquivonf, arquivonf.getArquivoretornoenvio().getContent());
			} catch (Exception e) {
				request.addError("Problemas no processamento do retorno do XML.");
				request.addError(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return getSalvarModelAndView(request, arquivonf);
	}
	
	/**
	 * Action que salva o arquivo de retorno do E-Transparencia
	 * 	 
	 * @param request
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/03/2018
	 */
	public ModelAndView salvarArquivoRetornoEnvioETransparencia(WebRequestContext request, Arquivonf arquivonf){
		if(arquivonf.getArquivoretornoenvio() != null){
			try{
				arquivonfService.processaRetornoEnvioETransparencia(arquivonf, arquivonf.getArquivoretornoenvio().getContent());
			} catch (Exception e) {
				request.addError("Problemas no processamento do retorno do XML.");
				request.addError(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return getSalvarModelAndView(request, arquivonf);
	}
	
	public ModelAndView salvarArquivoRetornoEnvioDataPublic(WebRequestContext request, Arquivonf arquivonf){
		if(arquivonf.getArquivoretornoenvio() != null){
			try{
				arquivonfService.processaRetornoEnvioLavrasiss(arquivonf, arquivonf.getArquivoretornoenvio().getContent());
			} catch (Exception e) {
				request.addError("Problemas no processamento do retorno do XML.");
				request.addError(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return getSalvarModelAndView(request, arquivonf);
	}
	
	public ModelAndView salvarArquivoRetornoEnvioMaraba(WebRequestContext request, Arquivonf arquivonf){
		if(arquivonf.getArquivoretornoenvio() != null){
			try{
				arquivonfService.processaRetornoEnvioMarabaIss(arquivonf, arquivonf.getArquivoretornoenvio().getContent());
			} catch (Exception e) {
				request.addError("Problemas no processamento do retorno do XML.");
				request.addError(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return getSalvarModelAndView(request, arquivonf);
	}
	
	/**
	 * Action que salva o arquivo de atualiza��o de notas com erro.
	 *
	 * @param request
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/12/2012
	 */
	public ModelAndView salvarArquivoAtualizacao(WebRequestContext request, Arquivonf arquivonf){
		
		if(arquivonf.getArquivoatualizacao() != null){
			try{
				arquivonfService.atualizaLoteProcessadoErro(request, arquivonf, arquivonf.getArquivoatualizacao().getContent());
				
				if(request.getMessages() == null || request.getMessages().length == 0){
					request.addError("N�o foi atualizada nenhuma nota.");
				}
			} catch (Exception e) {
				request.addError("Problemas na atualiza��o do lote.");
				request.addError(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return getSalvarModelAndView(request, arquivonf);
	}
	
	public ModelAndView salvarArquivoRetornoTinus(WebRequestContext request, Arquivonf arquivonf){
		if(arquivonf.getArquivoatualizacao() != null){
			try{
//				arquivonfService.atualizaLoteTinus(request, arquivonf, arquivonf.getArquivoatualizacao().getContent());
				
				if(request.getMessages() == null || request.getMessages().length == 0){
					request.addError("N�o foi atualizada nenhuma nota.");
				}
			} catch (Exception e) {
				request.addError("Problemas na atualiza��o do lote.");
				request.addError(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return getSalvarModelAndView(request, arquivonf);
	}
	
	public ModelAndView salvarArquivoAtualizacaoMi6(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonfService.atualizaLoteMi6(request, arquivonf);
			
			if(request.getMessages() == null || request.getMessages().length == 0){
				request.addError("N�o foi atualizada nenhuma nota.");
			}
		} catch (Exception e) {
			request.addError("Problemas na atualiza��o do lote.");
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		return getSalvarModelAndView(request, arquivonf);
	}
	
	public ModelAndView salvarArquivoAtualizacaoProduto(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonfService.atualizaLoteProcessadoErroProduto(request, arquivonf);
			
			if(request.getMessages() == null || request.getMessages().length == 0){
				request.addError("N�o foi atualizada nenhuma nota.");
			}
		} catch (Exception e) {
			request.addError("Problemas na atualiza��o do lote.");
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		return getSalvarModelAndView(request, arquivonf);
	}
	
	/**
	 * Action que desmarca a flag para usu�rios administradores.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivonfService#desmarcarArquivonf(MarcarArquivonfBean bean)
	 *
	 * @param request
	 * @param arquivonf
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 * @throws CrudException 
	 */
	public ModelAndView removerFlag(WebRequestContext request, Arquivonf arquivonf) throws CrudException{
		if(arquivonf == null || 
				arquivonf.getCdarquivonf() == null ||
				arquivonf.getFlag() == null || 
				arquivonf.getFlag().equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		if ("consultandolote".equals(arquivonf.getFlag())) {
			Arquivonf arquivonfFound = arquivonfService.loadForEntrada(arquivonf);
			
			if (Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO.equals(arquivonfFound.getConfiguracaonfe().getTipoconfiguracaonfe())) {
				if (arquivonfFound.getArquivoretornoenvio() != null && arquivonfFound.getEmitindo() != null && arquivonfFound.getEmitindo()) {
					if (arquivonfFound.getArquivoxmlconsultalote() == null) {
						try {
							arquivonfService.createArquivoXmlConsultaLote(arquivonfFound);
						} catch (Exception e) {
							request.addError("Erro ao tentar gerar xml de consulta.");
						}
					}
					
					arquivonfService.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
					arquivonfService.desmarcarArquivonf(arquivonf.getCdarquivonf(), arquivonf.getFlag());
				} else {
					request.addError("Fa�a o envio da NF-e primeiro.");
				}
			} else if (Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO.equals(arquivonfFound.getConfiguracaonfe().getTipoconfiguracaonfe()) && 
					(Prefixowebservice.JUAZEIRO_HOM.equals(arquivonfFound.getConfiguracaonfe().getPrefixowebservice()) || 
					Prefixowebservice.JUAZEIRO_PROD.equals(arquivonfFound.getConfiguracaonfe().getPrefixowebservice()))) {
				if (arquivonfFound.getArquivoretornoenvio() != null && arquivonfFound.getEmitindo() != null && arquivonfFound.getEmitindo()) {
					if (arquivonfFound.getArquivoxmlconsultalote() == null) {
						try {
							arquivonfService.createArquivoXmlConsultaLoteNfse(arquivonfFound);
						} catch (Exception e) {
							request.addError("Erro ao tentar gerar xml de consulta.");
						}
					}
					
					arquivonfService.updateSituacao(arquivonf, Arquivonfsituacao.ENVIADO);
					arquivonfService.desmarcarArquivonf(arquivonf.getCdarquivonf(), arquivonf.getFlag());
				} else {
					request.addError("Fa�a o envio da NFS-e primeiro.");
				}
			} else {
				request.addError("Somente � poss�vel remover a flag 'CONSULTANDO' em Nota Fiscal de Produto.");
			}
		} else if ("emitindo".equals(arquivonf.getFlag())) {
			arquivonfService.desmarcarArquivonf(arquivonf.getCdarquivonf(), arquivonf.getFlag());
		} else if ("consultandonota".equals(arquivonf.getFlag())) {
			if (arquivonf.getCdarquivonfnota() == null) {
				request.addError("Erro na passagem de par�metros.");
			} else {
				Integer cdarquivonota = Integer.parseInt(arquivonf.getCdarquivonfnota());
				Arquivonfnota arquivonfnotaFound = arquivonfnotaService.loadForConsulta(new Arquivonfnota(cdarquivonota));
				
				Arquivonf arquivonfFound = arquivonfnotaFound.getArquivonf();
				Configuracaonfe configuracaonfeFound = arquivonfFound != null ? arquivonfFound.getConfiguracaonfe() : null;
				
				if(configuracaonfeFound != null && 
						configuracaonfeFound.getTipoconfiguracaonfe() != null && 
						configuracaonfeFound.getTipoconfiguracaonfe().equals(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO)){
					boolean statusEmitida = arquivonfnotaFound.getNota() != null && arquivonfnotaFound.getNota().getNotaStatus() != null && arquivonfnotaFound.getNota().getNotaStatus().equals(NotaStatus.EMITIDA);
					boolean haveNumNfse = org.apache.commons.lang.StringUtils.isNotBlank(arquivonfnotaFound.getNumeronfse()); 
					if(statusEmitida && !haveNumNfse){
						if (arquivonfnotaFound.getArquivoxmlconsulta() == null) {
							if(configuracaonfeFound.getPrefixowebservice().equals(Prefixowebservice.BHISS_HOM) ||
									configuracaonfeFound.getPrefixowebservice().equals(Prefixowebservice.BHISS_PROD)){
								arquivonfnotaService.createArquivoXmlConsultaBhiss(arquivonfnotaFound);
							} else if(configuracaonfeFound.getPrefixowebservice().equals(Prefixowebservice.CASTELO_HOM) ||
									configuracaonfeFound.getPrefixowebservice().equals(Prefixowebservice.CASTELO_PROD)){
								arquivonfnotaService.createArquivoXmlConsultaEEl2(arquivonfnotaFound);
							}
						}
						
						arquivonfnotaService.desmarcarArquivonfnota(cdarquivonota, arquivonf.getFlag());
					} else {
						request.addError("Apenas NFS-e com status de 'EMITIDA' e n�o ter n�mero da prefeitura cadastrada aqui no W3ERP podem ser consultadas novamente.");
					}
				} else {
					List<Arquivonfnotaerro> arquivonfnotaerroList = arquivonfnotaerroService.findByArquivonfnota(arquivonfnotaFound);
					if (arquivonfnotaerroList != null && arquivonfnotaerroList.size() > 0) {
						Boolean valido = Boolean.FALSE;	
						
						for (Arquivonfnotaerro arquivonfnotaerro : arquivonfnotaerroList) {
							if ("204".equals(arquivonfnotaerro.getCodigo())) {
								valido = Boolean.TRUE;
								break;
							}
						}
						
						if (valido) {
							if (arquivonfnotaFound.getArquivoxmlconsulta() == null) {
								arquivonfnotaService.createArquivoXmlConsulta(arquivonfnotaFound);
							}
							
							arquivonfnotaService.desmarcarArquivonfnota(cdarquivonota, arquivonf.getFlag());
						} else {
							request.addError("Apenas NF-e com erro de 'Duplicidade' e com lote na situa��o de 'Processado com Erro' podem ser consultadas novamente.");
						}
					} else {
						request.addError("Apenas NF-e com erro de 'Duplicidade' e com lote na situa��o de 'Processado com Erro' podem ser consultadas novamente.");
					}
				}
			}
		}
		
		return new ModelAndView("redirect:/faturamento/crud/Arquivonf?ACAO=consultar&cdarquivonf=" + arquivonf.getCdarquivonf());
	}
	
	/**
	 * Envia lote para a Prefeitura de Paracatu
	 *
	 * @param request
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView enviarArquivoParacatu(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonfService.enviarArquivoParacatu(request, arquivonf);
			request.addMessage("Arquivo de NFS-e enviado.");		
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * Envia lote para a Prefeitura de Arandu
	 *
	 * @param request
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView enviarArquivoIssMap(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonfService.enviarArquivoIssMap(request, arquivonf);
			request.addMessage("Arquivo de NFS-e enviado.");		
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * Envia lote para o webservice de prefixo El-iss
	 *
	 * @param request
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/02/2014
	 */
	public ModelAndView enviarArquivoEliss(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonfService.enviarArquivoEliss(request, arquivonf);
		} catch (SinedException e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			request.addError("Problema no envio do arquivo NFS-e: " + e.getMessage());
			e.printStackTrace();
		}
				
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView consultarArquivoEliss(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonfService.consultarArquivoEliss(request, arquivonf, false);
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
				
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView enviarArquivoETransparencia(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonfService.enviarArquivoETransparencia(request, arquivonf);
		} catch (SinedException e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			request.addError("Problema no envio do arquivo NFS-e: " + e.getMessage());
			e.printStackTrace();
		}
				
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView consultarArquivoETransparencia(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonfService.consultarArquivoETransparencia(request, arquivonf, false);
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
				
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView processaRetornoEnvioSPNovamente(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonf = arquivonfService.loadForEntrada(arquivonf);
			
			Arquivo arquivo = arquivonf.getArquivoretornoenvio();
			arquivo = ArquivoService.getInstance().loadWithContents(arquivo);
			
			arquivonfService.processaRetornoEnvioSpiss(arquivonf, arquivo.getContent());
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
				
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView processaRetornoNovamente(WebRequestContext request, Arquivonf arquivonf){
		try {
			arquivonf = arquivonfService.loadForEntrada(arquivonf);
			
			Arquivo arquivo = arquivonf.getArquivoretornoconsulta();
			arquivo = ArquivoService.getInstance().loadWithContents(arquivo);
			
			arquivonfService.processaRetornoConsultaBhiss(arquivonf, new String(arquivo.getContent()), arquivonf.getConfiguracaonfe().getPrefixowebservice());
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
				
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView gerarArquivoLoteAtualizacao(WebRequestContext request, Arquivonf arquivonf) throws Exception {
		if(arquivonf.getArquivoatualizacaolote() != null){
			Arquivo arquivo = arquivonf.getArquivoatualizacaolote();
			if(arquivo.getName().toLowerCase().endsWith(".zip")){
				Arquivo arquivoZip = arquivo;
				ZipManipulation zip = new ZipManipulation(arquivoZip);
				zip.readEntries();
				
				
				StringBuilder sb = new StringBuilder(
						"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
						"<ConsultarLoteRpsResposta xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"http://www.abrasf.org.br/nfse.xsd\">\n" +
						"<Situacao>4</Situacao>\n" +
						"<ListaNfse>\n");
						
				for(String filename : zip.getEntriesNames()){
					sb.append(new StringUtils().tiraAcento(new String(zip.getBytesFromEntry(filename), "UTF-8"))
								.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "")
								.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>", "")
								.replace("<ConsultarLoteRpsResposta xmlns=\"http://www.abrasf.org.br/nfse.xsd\" xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\">", "")
								.replace("<Situacao>4</Situacao>", "")
								.replace("<ListaNfse>", "")
								.replace("</ListaNfse>", "")
								.replace("</ConsultarLoteRpsResposta>", "") + "\n");
				}
				
				sb.append("\n</ListaNfse>" +
						  "\n</ConsultarLoteRpsResposta>");
				
				return new ResourceModelAndView(new Resource ("text/xml", "nfretornoconsulta_" + SinedUtil.datePatternForReport() + ".xml", sb.toString().getBytes()));
			}
		}
		return new ResourceModelAndView(new Resource("text/xml", "nfretornoconsulta_" + SinedUtil.datePatternForReport() + ".xml", new String().getBytes()));
	}
}
