package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaodocumento;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaohistorico;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaomaterial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaoService;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaodocumentoService;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaohistoricoService;
import br.com.linkcom.sined.geral.service.ContratohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.geral.service.VdocumentonegociadoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.CentrocustoValorVO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnviarFaturaLocacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnviarFaturaLocacaoEmailBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FaturamentoConjuntoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratofaturalocacaoFiltro;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ContratoJson;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/Contratofaturalocacao", authorizationModule=CrudAuthorizationModule.class)
public class ContratofaturalocacaoCrud extends CrudControllerSined<ContratofaturalocacaoFiltro, Contratofaturalocacao, Contratofaturalocacao> {
	
	private ContratofaturalocacaoService contratofaturalocacaoService;
	private ContratofaturalocacaohistoricoService contratofaturalocacaohistoricoService;
	private ContratoService contratoService;
	private EnderecoService enderecoService;
	private ContratofaturalocacaodocumentoService contratofaturalocacaodocumentoService;
	private MaterialService materialService;
	private PatrimonioitemService patrimonioitemService;
	private RateioService rateioService;
	private TaxaService taxaService;
	private RomaneioService romaneioService;
	private DocumentoService documentoService;
	private ContratohistoricoService contratohistoricoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private VdocumentonegociadoService vdocumentonegociadoService;
	
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setPatrimonioitemService(
			PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setContratofaturalocacaodocumentoService(
			ContratofaturalocacaodocumentoService contratofaturalocacaodocumentoService) {
		this.contratofaturalocacaodocumentoService = contratofaturalocacaodocumentoService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setContratofaturalocacaoService(
			ContratofaturalocacaoService contratofaturalocacaoService) {
		this.contratofaturalocacaoService = contratofaturalocacaoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setContratofaturalocacaohistoricoService(
			ContratofaturalocacaohistoricoService contratofaturalocacaohistoricoService) {
		this.contratofaturalocacaohistoricoService = contratofaturalocacaohistoricoService;
	}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {
		this.contratohistoricoService = contratohistoricoService;
	}
	public void setVdocumentonegociadoService(VdocumentonegociadoService vdocumentonegociadoService) {
		this.vdocumentonegociadoService = vdocumentonegociadoService;
	}
	
	@Override
	protected ListagemResult<Contratofaturalocacao> getLista(WebRequestContext request, ContratofaturalocacaoFiltro filtro) {
		ListagemResult<Contratofaturalocacao> lista = super.getLista(request, filtro);
		List<Contratofaturalocacao> list = lista.list();
		
		filtro.setTotalfaturalocacao(new Money());
		if(list != null && list.size() > 0 && filtro.getExibirTotais() != null && filtro.getExibirTotais()){
			filtro.setTotalfaturalocacao(contratofaturalocacaoService.findForCalculoTotalGeral(filtro));
		}
		
		return lista;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Contratofaturalocacao form) throws Exception {
		if(form.getCdcontratofaturalocacao() != null){
			form.setListaContratofaturalocacaohistorico(contratofaturalocacaohistoricoService.findByContratofaturalocacao(form));
		}
	}
	
	@Override
	protected void listagem(WebRequestContext request, ContratofaturalocacaoFiltro filtro) throws Exception {
		request.setAttribute("listaSituacao", Arrays.asList(Contratofaturalocacaosituacao.values()));
	}
	
	@Override
	protected void salvar(WebRequestContext request, Contratofaturalocacao bean) throws Exception {
		boolean isCriar = bean.getCdcontratofaturalocacao() == null;
		
		Integer numeroAntigo = null;
		Integer numeroNovo = bean.getNumero();
		boolean numeroIgual = true; 
		if(!isCriar){
			Contratofaturalocacao beanAntigo = contratofaturalocacaoService.load(bean, "contratofaturalocacao.cdcontratofaturalocacao, contratofaturalocacao.numero");
			numeroAntigo = beanAntigo.getNumero();
			numeroIgual = (numeroAntigo == null && numeroNovo == null) || (numeroAntigo != null && numeroAntigo.equals(numeroNovo));
		}
		
		// GERAR O PR�XIMO N�MERO DE FATURA DE LOCA��O A PARTIR DA EMPRESA
		if(isCriar && bean.getUsarSequencial() != null && bean.getUsarSequencial()){
			contratofaturalocacaoService.gerarNumeroSequencial(bean);
		}
		
		super.salvar(request, bean);
		
		StringBuilder obs = new StringBuilder();
		if(!numeroIgual){
			obs.append("N�mero de fatura alterada de ").append(numeroAntigo != null ? numeroAntigo : "--vazio--").append(" para ").append(numeroNovo != null ? numeroNovo : "--vazio--");
			
			try{
				List<Contratofaturalocacaodocumento> listaDocumento = contratofaturalocacaodocumentoService.findForValidacao(bean.getCdcontratofaturalocacao() + "");
				for (Contratofaturalocacaodocumento contratofaturalocacaodocumento : listaDocumento) {
					if(contratofaturalocacaodocumento.getDocumento() != null &&
							contratofaturalocacaodocumento.getDocumento().getCddocumento() != null &&
							contratofaturalocacaodocumento.getDocumento().getDocumentoacao() != null &&
							!contratofaturalocacaodocumento.getDocumento().getDocumentoacao().equals(Documentoacao.BAIXADA)){
						documentoService.updateNumerosNFDocumento(contratofaturalocacaodocumento.getDocumento().getCddocumento() + "", numeroNovo + "");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("Problema na atualiza��o do n�mero da(s) conta(s) a receber: " + e.getMessage()); 
			}
		}
		
		if(isCriar && bean.getContrato() != null && bean.getContrato().getCdcontrato() != null){
			Contrato contrato = contratoService.load(bean.getContrato(), "contrato.cdcontrato, contrato.identificador");
			if(contrato != null){
				obs
				.append(" Contrato: ")
				.append("<a href=\"javascript:visualizarContrato(")
				.append(contrato.getCdcontrato())
				.append(")\">")
				.append(contrato.getIdentificadorOrCdcontrato())
				.append("</a>; ");
			}
		}
		// SALVA O HIST�RICO DE ALTERA��O
		contratofaturalocacaohistoricoService.createAndSaveHitorico(bean, obs.toString(), isCriar);
		
		if(isCriar){
			if(bean.getContrato() != null && bean.getContrato().getCdcontrato() != null){
				contratohistoricoService.criaHistoricoForContrato(bean, bean.getContrato());
			}else if(bean.getWhereInContrato() != null && !bean.getWhereInContrato().equals("")){
				List<Contrato> listaContrato = contratoService.findWithIdentificador(bean.getWhereInContrato());
				if(listaContrato != null && !listaContrato.isEmpty()){
					contratofaturalocacaoService.createHistoricoOrigemContrato(bean, listaContrato);
				}
			}
		}
	}
	
	/**
	 * Abre pop-up com a justificativa para o cancelamento.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/10/2013
	 */
	public ModelAndView abrirCancelar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean erro = false;
		List<Contratofaturalocacao> listaContratofaturalocacao = contratofaturalocacaoService.findForValidacao(whereIn);
		for (Contratofaturalocacao contratofaturalocacao : listaContratofaturalocacao) {
			if(contratofaturalocacao.getSituacao() != null && 
					!contratofaturalocacao.getSituacao().equals(Contratofaturalocacaosituacao.EMITIDA) &&
					!contratofaturalocacao.getSituacao().equals(Contratofaturalocacaosituacao.FATURADA)){
				request.addError("Para realizar o cancelamento a fatura de loca��o tem que estar nas situa��es 'EMITIDA' ou 'FATURADA'.");
				erro = true;
				break;
			}
		}
		
		if(erro){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento = contratofaturalocacaodocumentoService.findForValidacao(whereIn);
		if(SinedUtil.isListNotEmpty(listaContratofaturalocacaodocumento)){
			for(Contratofaturalocacaodocumento contratofaturalocacaodocumento : listaContratofaturalocacaodocumento){
				Documento documento = contratofaturalocacaodocumento.getDocumento();
				if(documento != null && documento.getDocumentoacao() != null){
					if(Documentoacao.BAIXADA.equals(documento.getDocumentoacao())){
						erro = true;
						break;
					} else if(Documentoacao.NEGOCIADA.equals(documento.getDocumentoacao())){
						erro = vdocumentonegociadoService.existeParcelasBaixadas(documento.getCddocumento().toString());
						break;
					}
				}
			}
		}
		if(erro){
			request.addError("Conta a receber baixada. N�o � poss�vel cancelar.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		request.setAttribute("haveDocumento", listaContratofaturalocacaodocumento != null && listaContratofaturalocacaodocumento.size() > 0);
		
		request.setAttribute("titulo", "CANCELAMENTO");
		request.setAttribute("action", "cancelar");
		
		Contratofaturalocacaohistorico bean = new Contratofaturalocacaohistorico();
		bean.setWhereIn(whereIn);
		
		return new ModelAndView("direct:/crud/popup/cancelarContratofaturalocacao", "bean", bean);
	}
	
	/**
	 * Action que realiza o cancelamento de fatura de loca��o
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2013
	 */
	public void cancelar(WebRequestContext request, Contratofaturalocacaohistorico bean){
		String whereIn = bean.getWhereIn();
		contratofaturalocacaoService.updateSituacao(whereIn, Contratofaturalocacaosituacao.CANCELADA);
		
		String[] ids = whereIn.split(",");
		for (String id : ids) {
			// SALVA O HIST�RICO DE ALTERA��O
			Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
			contratofaturalocacaohistorico.setAcao(Contratofaturalocacaoacao.CANCELADA);
			contratofaturalocacaohistorico.setContratofaturalocacao(new Contratofaturalocacao(Integer.parseInt(id)));
			contratofaturalocacaohistorico.setObservacao(bean.getObservacao());
			contratofaturalocacaohistoricoService.saveOrUpdate(contratofaturalocacaohistorico);
		}
		
		if(bean.getCancelamentoDocumento() != null && bean.getCancelamentoDocumento()){
			contratofaturalocacaoService.cancelamentoDocumentoByContratofaturalocacao(request, bean.getObservacao(), whereIn);
		}
		
		request.addMessage("Fatura(s) de loca��o cancelada(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	
	/**
	 * Abre pop-up com a justificativa para o cancelamento.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/10/2013
	 */
	public ModelAndView abrirSubstituir(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean erro = false;
		List<Contratofaturalocacao> listaContratofaturalocacao = contratofaturalocacaoService.findForValidacao(whereIn);
		
		Boolean haveMaterial = null;
		Cliente cliente = null;
		Empresa empresa = null;
		
		boolean erroEmpresa = false;
		boolean erroCliente = false;
		boolean erroMaterial = false;
		
		for (Contratofaturalocacao contratofaturalocacao : listaContratofaturalocacao) {
			if(cliente == null){
				cliente = contratofaturalocacao.getCliente();
			} else {
				Cliente clienteLocal = contratofaturalocacao.getCliente();
				if(clienteLocal != null && !clienteLocal.equals(cliente)){
					erroCliente = true;
				}
			}
			if(empresa == null){
				empresa = contratofaturalocacao.getEmpresa();
			} else {
				Empresa empresaLocal = contratofaturalocacao.getEmpresa();
				if(empresaLocal != null && !empresaLocal.equals(empresa)){
					erroEmpresa = true;
				}
			}
			if(haveMaterial == null) {
				haveMaterial = contratofaturalocacao.getListaContratofaturalocacaomaterial() != null && contratofaturalocacao.getListaContratofaturalocacaomaterial().size() > 0;
			} else {
				Boolean haveMaterialLocal = contratofaturalocacao.getListaContratofaturalocacaomaterial() != null && contratofaturalocacao.getListaContratofaturalocacaomaterial().size() > 0;
				if(!haveMaterialLocal.equals(haveMaterial)){
					erroMaterial = true;
				}
			}
			
			if(contratofaturalocacao.getSituacao() != null && 
					!contratofaturalocacao.getSituacao().equals(Contratofaturalocacaosituacao.EMITIDA) &&
					!contratofaturalocacao.getSituacao().equals(Contratofaturalocacaosituacao.FATURADA)){
				request.addError("Para realizar a substitui��o a fatura de loca��o tem que estar nas situa��es 'EMITIDA' ou 'FATURADA'.");
				erro = true;
			}
		}
		
		if(erroEmpresa){
			request.addError("S� � poss�vel substituir mais de uma fatura de loca��o da mesma empresa.");
			erro = true;
		}
		if(erroCliente){
			request.addError("S� � poss�vel substituir mais de uma fatura de loca��o do mesmo cliente.");
			erro = true;
		}
		if(erroMaterial){
			request.addError("S� � poss�vel substituir mais de uma fatura de loca��o onde todas as faturas tenham materiais ou ent�o todas as faturas n�o tenham materiais.");
			erro = true;
		}
		
		if(erro){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento = contratofaturalocacaodocumentoService.findForValidacao(whereIn);
		request.setAttribute("haveDocumento", listaContratofaturalocacaodocumento != null && listaContratofaturalocacaodocumento.size() > 0);
		
		request.setAttribute("titulo", "SUBSTITUI��O");
		request.setAttribute("action", "substituir");
		
		Contratofaturalocacaohistorico bean = new Contratofaturalocacaohistorico();
		bean.setWhereIn(whereIn);
		
		return new ModelAndView("direct:/crud/popup/cancelarContratofaturalocacao", "bean", bean);
	}
	
	/**
	 * Action que realiza a substitui��o de fatura de loca��o
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @throws CrudException 
	 * @since 10/10/2013
	 */
	public void substituir(WebRequestContext request, Contratofaturalocacaohistorico bean) throws CrudException{
		String whereIn = bean.getWhereIn();
		
		Contratofaturalocacao contratofaturalocacao = contratofaturalocacaoService.createSubstituicaoFatura(whereIn);
		contratofaturalocacaoService.updateSituacao(whereIn, Contratofaturalocacaosituacao.CANCELADA);
		
		String[] ids = whereIn.split(",");
		for (String id : ids) {
			// SALVA O HIST�RICO DE ALTERA��O
			Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
			contratofaturalocacaohistorico.setAcao(Contratofaturalocacaoacao.CANCELADA);
			contratofaturalocacaohistorico.setContratofaturalocacao(new Contratofaturalocacao(Integer.parseInt(id)));
			
			String link = contratofaturalocacaoService.createLink(contratofaturalocacao);
			
			contratofaturalocacaohistorico.setObservacao("Fatura substitu�da pela fatura " + link);
			
			if (bean.getObservacao()!=null && !bean.getObservacao().trim().equals("")){
				contratofaturalocacaohistorico.setObservacao(contratofaturalocacaohistorico.getObservacao() + ". (" + bean.getObservacao().trim() + ")");
			}
			
			contratofaturalocacaohistoricoService.saveOrUpdate(contratofaturalocacaohistorico);
		}

		if(bean.getCancelamentoDocumento() != null && bean.getCancelamentoDocumento()){
			contratofaturalocacaoService.cancelamentoDocumentoByContratofaturalocacao(request, bean.getObservacao(), whereIn);
		}
		
		request.addMessage("Fatura(s) de loca��o substitu�da(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/Contratofaturalocacao?ACAO=consultar&cdcontratofaturalocacao=" + contratofaturalocacao.getCdcontratofaturalocacao());
	}
	
	/**
	 * Realiza a valida��o da substitui��o de fatura de loca��o
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/09/2013
	 */
	public ModelAndView validacaoSubstituicaoAjax(WebRequestContext request){
		StringBuilder erro = new StringBuilder();
		boolean confirmacaoContareceber = false;
		try{
			String whereIn = SinedUtil.getItensSelecionados(request);
			if(whereIn == null || whereIn.trim().equals("")){
				throw new SinedException("Nenhum item foi selecionado.");
			}
			
			List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento = contratofaturalocacaodocumentoService.findForValidacao(whereIn);
			for (Contratofaturalocacaodocumento contratofaturalocacaodocumento : listaContratofaturalocacaodocumento) {
				if(contratofaturalocacaodocumento != null && contratofaturalocacaodocumento.getDocumento() != null){
					confirmacaoContareceber = true;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			erro.append(e.getMessage());
		}
		
		return new JsonModelAndView()
					.addObject("confirmacaoContareceber", confirmacaoContareceber)
					.addObject("erro", erro.toString());
	}
	
//	/**
//	 * Realiza a valida��o do cancelamento de fatura de loca��o
//	 *
//	 * @param request
//	 * @return
//	 * @author Rodrigo Freitas
//	 * @since 19/09/2013
//	 */
//	public ModelAndView validacaoCancelamentoAjax(WebRequestContext request){
//		StringBuilder erro = new StringBuilder();
//		boolean confirmacaoContareceber = false;
//		try{
//			String whereIn = SinedUtil.getItensSelecionados(request);
//			if(whereIn == null || whereIn.trim().equals("")){
//				throw new SinedException("Nenhum item foi selecionado.");
//			}
//			
//			List<Contratofaturalocacaodocumento> lista = contratofaturalocacaodocumentoService.findForValidacao(whereIn);
//			for (Contratofaturalocacaodocumento contratofaturalocacaodocumento : lista) {
//				if(contratofaturalocacaodocumento != null &&
//						contratofaturalocacaodocumento.getDocumento() != null){
//					confirmacaoContareceber = true;
//					
//					if(contratofaturalocacaodocumento.getDocumento().getDocumentoacao() != null &&
//							(contratofaturalocacaodocumento.getDocumento().getDocumentoacao().equals(Documentoacao.BAIXADA) ||
//									contratofaturalocacaodocumento.getDocumento().getDocumentoacao().equals(Documentoacao.BAIXADA_PARCIAL))){
//						erro.append("A conta a receber vinculada � fatura " + contratofaturalocacaodocumento.getContratofaturalocacao().getNumero() + " est� baixada. N�o � poss�vel cancelar.\n");
//					}
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			erro.append(e.getMessage());
//		}
//		
//		return new JsonModelAndView()
//						.addObject("confirmacaoContareceber", confirmacaoContareceber)
//						.addObject("erro", erro.toString());
//	}
	
	/**
	 * Carrega a lista de contrato e a lista de endere�o pelo cliente selecionado.
	 *
	 * @param request
	 * @param contratofaturalocacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/07/2013
	 */
	public ModelAndView carregaContratoEnderecoByClienteAjax(WebRequestContext request, Contratofaturalocacao contratofaturalocacao){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(contratofaturalocacao == null || 
				contratofaturalocacao.getCliente() == null ||
				contratofaturalocacao.getCliente().getCdpessoa() == null){
			return jsonModelAndView;
		}
		Endereco enderecoSelecionado = null;
		if(request.getParameter("cdenderecoSelecionado") != null){
			try {
				enderecoSelecionado = enderecoService.loadEndereco(new Endereco(Integer.parseInt(request.getParameter("cdenderecoSelecionado"))));  
			} catch (Exception e) {}
		}
		Endereco enderecofaturaSelecionado = null;
		if(request.getParameter("cdenderecofaturaSelecionado") != null){
			try {
				enderecofaturaSelecionado = enderecoService.loadEndereco(new Endereco(Integer.parseInt(request.getParameter("cdenderecofaturaSelecionado"))));  
			} catch (Exception e) {}
		}
		Cliente cliente = contratofaturalocacao.getCliente();
		
		List<Endereco> listaEndereco = enderecoService.findAtivosByCliente(cliente);
		if(enderecoSelecionado != null || enderecofaturaSelecionado != null){
			if(listaEndereco == null) listaEndereco = new ArrayList<Endereco>();
			
			if(enderecoSelecionado != null && !listaEndereco.contains(enderecoSelecionado)){
				listaEndereco.add(enderecoSelecionado);
			}
			if(enderecofaturaSelecionado != null && !listaEndereco.contains(enderecofaturaSelecionado)){
				listaEndereco.add(enderecofaturaSelecionado);
			}
		}
		jsonModelAndView.addObject("listaEndereco", listaEndereco);
		
		List<Contrato> listaContrato = contratoService.findBycliente(cliente);
		List<ContratoJson> listaContratoJson = new ArrayList<ContratoJson>();
		for (Contrato contrato : listaContrato) {
			listaContratoJson.add(new ContratoJson(contrato));
		}
		jsonModelAndView.addObject("listaContrato", listaContratoJson);
		return jsonModelAndView;
	}
	
	
	
	/**
	 * Realiza a gera��o do CSV da listagem
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/09/2013
	 */
	public ModelAndView gerarCSV(WebRequestContext request, ContratofaturalocacaoFiltro filtro){
		List<Contratofaturalocacao> lista = contratofaturalocacaoService.findForListagemCsvPdf(filtro);
		List<Rateioitem> listaRateioitem = contratofaturalocacaoService.getListaRateioitemByContratofaturalocacao(lista);
		contratofaturalocacaoService.makeCentrocustoValorVO(lista, listaRateioitem);
		
		StringBuilder rel = new StringBuilder();
		
		rel.append("N�mero;");
		rel.append("Data de Emiss�o;");
		rel.append("Data de Vencimento;");
		rel.append("Empresa;");
		rel.append("Cliente;");
		rel.append("Descri��o;");
		rel.append("�ltima observa��o;");
		rel.append("Total;");
		rel.append("Situa��o;");
		rel.append("Contrato;");
		rel.append("Conta a receber;");		
		rel.append("Centro de Custo;");
		rel.append("Valor por Centro;");
		rel.append("Material;");
		rel.append("Valor do Item;");
		rel.append("\n");
		
		Money total = new Money();
		
		for (Contratofaturalocacao c : lista) {
			total = total.add(c.getValortotal());
			String ultimoHistorico = c.getUltimoHistorico();
			ultimoHistorico = ultimoHistorico.replace("\"", "");
			ultimoHistorico = ultimoHistorico.replaceAll("\n", "\\. ");
			ultimoHistorico = ultimoHistorico.replaceAll("(\\<a href\\=javascript\\:visualizarContrato\\([0-9]+\\)\\>)", "");
			ultimoHistorico = ultimoHistorico.replaceAll("(\\<a href\\=javascript\\:visualizarFaturalocacao\\([0-9]+\\)\\>)", "");
			ultimoHistorico = ultimoHistorico.replace("</a>;", "");
			ultimoHistorico = ultimoHistorico.replace("</a>", "");
			
			rel.append(c.getNumeroFormatado());
			rel.append(";");
			
			if(c.getDtemissao() != null){
				rel.append(SinedDateUtils.toString(c.getDtemissao()));
			}
			rel.append(";");
			
			if(c.getDtvencimento() != null){
				rel.append(SinedDateUtils.toString(c.getDtvencimento()));
			}
			rel.append(";");
			
			if(c.getEmpresa() != null){
				rel.append(c.getEmpresa().getRazaosocialOuNome());
			}
			rel.append(";");
			
			if(c.getCliente() != null){
				rel.append(c.getCliente().getNome());
			}
			rel.append(";");
			
			if(c.getDescricao() != null)
				rel.append("\"" + c.getDescricao() + "\"");
			rel.append(";");
			
			if(ultimoHistorico != null)
				rel.append("\"" + ultimoHistorico + "\"");
			rel.append(";");
			
			rel.append(c.getValortotal());
			rel.append(";");
			
			rel.append(c.getSituacao());
			rel.append(";");
			
			if(c.getContrato() != null){
				if(c.getContrato().getIdentificador() != null && c.getContrato().getIdentificador() != ""){
					rel.append(c.getContrato().getIdentificador());
				} else if(c.getContrato().getCdcontrato() != null){
					rel.append(c.getContrato().getCdcontrato());
				}
			}
			rel.append(";");
			
			
			StringBuilder sb = new StringBuilder();
			if(c.getListaContratofaturalocacaodocumento() != null){
				for(Contratofaturalocacaodocumento it_doc: c.getListaContratofaturalocacaodocumento()){
					if(it_doc.getDocumento() != null){
						sb.append(it_doc.getDocumento().getCddocumento())
						.append(" - ")
						.append(br.com.linkcom.sined.util.SinedDateUtils.toString(it_doc.getDocumento().getDtvencimento()))
						.append(" ");
					}
				}
			}
			rel.append(sb.toString());
			rel.append(";");
			
			rel.append("\n");
			
			int sizeCentro = c.getListaCentrocustoValorVO() != null ? c.getListaCentrocustoValorVO().size() : 0;
			int sizeMat = c.getListaContratofaturalocacaomaterial() != null ? c.getListaContratofaturalocacaomaterial().size() : 0;
			int size = sizeCentro > sizeMat ? sizeCentro : sizeMat;
			
			for (int i = 0; i < size; i++) {
				rel.append(";;;;;;;;;;;");
				
				if(c.getListaCentrocustoValorVO() != null && c.getListaCentrocustoValorVO().size() > i){
					CentrocustoValorVO centrocustoValorVO = c.getListaCentrocustoValorVO().get(i);
					rel.append(centrocustoValorVO.getCentrocusto().getNome()).append(";");
					rel.append(centrocustoValorVO.getValor()).append(";");
				} else {
					rel.append(";;");
				}
				
				if(c.getListaContratofaturalocacaomaterial() != null && c.getListaContratofaturalocacaomaterial().size() > i){
					Contratofaturalocacaomaterial it = c.getListaContratofaturalocacaomaterial().get(i);
					rel.append(it.getMaterial().getNome());
					if(it.getPatrimonioitem() != null && it.getPatrimonioitem().getPlaqueta() != null){
						rel.append(" - ").append(it.getPatrimonioitem().getPlaqueta());
					}
					rel.append(";");
					rel.append(it.getValor()).append(";");
				} else {
					rel.append(";;");
				}
				rel.append("\n");
			}
		}
		
		rel.append(";");
		rel.append(";");
		rel.append(";");
		rel.append(";");
		rel.append(";");
		rel.append(";");
		rel.append(";");
		rel.append(total).append(";");
		rel.append(";");
		rel.append(";");
		rel.append(";");
		rel.append(";");
		rel.append(";");
		rel.append("\n");
		
		Resource resource = new Resource("text/csv","contratofaturalocacao_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	@Input("listagem")
	public ModelAndView gerarReceita(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.trim().equals("")){
			throw new SinedException("Nenhum item foi selecionado.");
		}
		
		try{
			if(contratofaturalocacaoService.existFaturaDiferenteEmitida(whereIn)){
				NeoWeb.getRequestContext().addError("Para gerar receita a fatura de loca��o tem que estar na situa��o 'EMITIDA'.");
				throw new SinedException("N�o foi poss�vel gerar receita.");
			}
			if (contratofaturalocacaoService.isFaturaAvulsaSemContrato(whereIn) || contratofaturalocacaoService.isFaturaIndenizacaoWithContrato(whereIn)){
				return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=criar&gerarreceitafaturalocacao=true&cdcontratofaturalocacao=" + whereIn);
			}else {
				contratofaturalocacaoService.gerarReceita(whereIn);
				request.addMessage("Receita(s) gerada(s) com sucesso.");
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * Busca informa��es sobre o material.
	 *
	 * @param request
	 * @param contratofaturalocacaomaterial
	 * @author Rodrigo Freitas
	 * @since 26/09/2013
	 */
	public void ajaxInfoMaterial(WebRequestContext request, Contratofaturalocacaomaterial contratofaturalocacaomaterial) {
    	Material material = contratofaturalocacaomaterial.getMaterial();
    	
    	if(material != null){
    		String response = "";
    		
    		material = materialService.loadWithContagerencialCentrocusto(material);
    		Boolean patrimonio = material.getPatrimonio();
    		
    		if(patrimonio != null && patrimonio){
    			response += "var patrimonioBool = 'true'; ";
    		} else response += "var patrimonioBool = 'false'; ";
    		
    		
    		View.getCurrent().eval(response);
    	}
    }
	
	/**
	 * A��o em ajax para buscar os itens de patrim�nio de um material.
	 *
	 * @param request
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2013
	 */
	public ModelAndView buscarPatrimonioitemAjax(WebRequestContext request, Contratofaturalocacaomaterial contratofaturalocacaomaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(contratofaturalocacaomaterial != null && contratofaturalocacaomaterial.getMaterial() != null){
			Integer cdpatrimonioitem = contratofaturalocacaomaterial.getPatrimonioitem() != null ? contratofaturalocacaomaterial.getPatrimonioitem().getCdpatrimonioitem() : null;
			List<Patrimonioitem> listaPatrimonioitem = patrimonioitemService.findByMaterial(contratofaturalocacaomaterial.getMaterial(), cdpatrimonioitem, Patrimonioitemsituacao.DISPONIVEL);
			jsonModelAndView.addObject("listaPatrimonioitem", listaPatrimonioitem);
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView criarFaturaLocacaoFechamento(WebRequestContext request){
		String fromFechamentoParam = request.getParameter("fromFechamento");
		String cdcontratoParam = request.getParameter("cdcontrato");
		String cdromaneioParam = request.getParameter("cdromaneio");
		String valorReposicaoParam = request.getParameter("valorReposicao");
		Boolean isIndenizacao = false;
		
		Money valorReposicao = new Money();
		if(valorReposicaoParam != null && !valorReposicaoParam.equals("")){
			valorReposicao = new Money(Double.parseDouble(valorReposicaoParam.replaceAll("\\.", "").replaceAll(",", ".")));
		}
		
		if(fromFechamentoParam != null && fromFechamentoParam.trim().equals("true") && 
				cdcontratoParam != null && !cdcontratoParam.trim().equals("") &&
				cdromaneioParam != null && !cdromaneioParam.trim().equals("")){
			List<FaturamentoConjuntoBean> listaFaturamentoConjuntoBean = new ArrayList<FaturamentoConjuntoBean>();
			
			List<Contrato> listaContrato = contratoService.findForCobranca(cdcontratoParam);
			if(listaContrato != null && !listaContrato.isEmpty()){
				boolean existeContratoIgnorarValorMaterial = false;
				if(listaContrato.size() > 1){
					for (Contrato contrato : listaContrato) {
						if(Boolean.TRUE.equals(contrato.getIgnoravalormaterial())){
							existeContratoIgnorarValorMaterial = true;
							break;
						}
					}
				}
				
				for(Contrato contrato : listaContrato){
					if(contrato.getRateio() != null && contrato.getRateio().getCdrateio() != null){
						contrato.setRateio(rateioService.findRateio(contrato.getRateio()));
					}
					if(contrato.getTaxa() != null && contrato.getTaxa().getCdtaxa() != null){
						contrato.setTaxa(taxaService.findTaxa(contrato.getTaxa()));
					}
					contratoService.calculaValorFaturaLocacao(contrato);
					contrato.setIgnoravalormaterial(true);
					
					Romaneio romaneio = new Romaneio(Integer.parseInt(cdromaneioParam));
					romaneio = romaneioService.loadForEntrada(romaneio);
					
					List<Movimentacaoestoque> listaMovEstoque = movimentacaoestoqueService.findByRomaneio(romaneio, Movimentacaoestoquetipo.ENTRADA);
					
					Money valor = new Money();
					if(listaMovEstoque != null && listaMovEstoque.size() > 0){
						for (Movimentacaoestoque movimentacaoestoque : listaMovEstoque) {
							if(movimentacaoestoque.getMaterial() != null){
								if(movimentacaoestoque.getValor() != null && movimentacaoestoque.getValor() > 0d){
									valor = valor.add(new Money(movimentacaoestoque.getValor()* 
											(movimentacaoestoque.getQtde() != null ? movimentacaoestoque.getQtde() : 1d)));
								} else if(movimentacaoestoque.getMaterial().getValorindenizacao() != null){
									valor = valor.add(new Money(movimentacaoestoque.getMaterial().getValorindenizacao() * 
											(movimentacaoestoque.getQtde() != null ? movimentacaoestoque.getQtde() : 1d)));
								} else if(movimentacaoestoque.getMaterial().getValorcusto() != null){
									valor = valor.add(new Money(movimentacaoestoque.getMaterial().getValorcusto() * 
											(movimentacaoestoque.getQtde() != null ? movimentacaoestoque.getQtde() : 1d)));
								}
							}
							
						}
					} else {
						List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
						for (Romaneioitem romaneioitem : listaRomaneioitem) {
							if(romaneioitem.getMaterial() != null){
								if(romaneioitem.getMaterial().getValorindenizacao() != null){
									valor = valor.add(new Money(romaneioitem.getMaterial().getValorindenizacao() * 
											(romaneioitem.getQtde() != null ? romaneioitem.getQtde() : 1d)));
								}else if(romaneioitem.getMaterial().getValorcusto() != null){
									valor = valor.add(new Money(romaneioitem.getMaterial().getValorcusto() * 
											(romaneioitem.getQtde() != null ? romaneioitem.getQtde() : 1d)));
								}
							}
						}
					}
					
					if(valor != null && valor.getValue().doubleValue() > 0 && romaneio.getListaRomaneioorigem() != null && romaneio.getListaRomaneioorigem().size() > 1){
						valor = new Money((valor.getValue().doubleValue() / romaneio.getListaRomaneioorigem().size()));
					}					
					if(valorReposicao.getValue().doubleValue() > 0.0){
						isIndenizacao = true;
						valor = valorReposicao;
					}
					Contratofaturalocacao contratofaturalocacao = contratofaturalocacaoService.criaContratofaturalocacaoByContrato(contrato, valor, existeContratoIgnorarValorMaterial);
					contratofaturalocacao.setDescricao("Indeniza��o");
					contratofaturalocacao.setSituacao(Contratofaturalocacaosituacao.EMITIDA);
					contratofaturalocacao.setIndenizacao(Boolean.TRUE);
					
					FaturamentoConjuntoBean faturamentoConjuntoBean = new FaturamentoConjuntoBean();
					faturamentoConjuntoBean.setContratofaturalocacao(contratofaturalocacao);
					faturamentoConjuntoBean.setCliente(contrato.getCliente());
					
					List<Contrato> contratos = new ArrayList<Contrato>();
					contratos.add(contrato);
					faturamentoConjuntoBean.setListaContrato(contratos);
					
					listaFaturamentoConjuntoBean.add(faturamentoConjuntoBean);
				}
				
				List<FaturamentoConjuntoBean> listaFaturamentoConjuntoAgrupado = new ArrayList<FaturamentoConjuntoBean>();
				for (FaturamentoConjuntoBean bean: listaFaturamentoConjuntoBean) {
					Contratofaturalocacao contratofaturalocacao = bean.getContratofaturalocacao();
					
					boolean achou = false;
					for (FaturamentoConjuntoBean bean2 : listaFaturamentoConjuntoAgrupado) {
						if(bean.getCliente().equals(bean2.getCliente())){
							achou = true;
							
							Contratofaturalocacao contratofaturalocacao2 = bean2.getContratofaturalocacao();
							contratofaturalocacao2.setContrato(null);
							if(StringUtils.isNotBlank(contratofaturalocacao.getDadosadicionais())){
								if(StringUtils.isNotBlank(contratofaturalocacao2.getDadosadicionais())){
									contratofaturalocacao2.setDadosadicionais(contratofaturalocacao2.getDadosadicionais() + " " + contratofaturalocacao.getDadosadicionais());
								}else {
									contratofaturalocacao2.setDadosadicionais(contratofaturalocacao.getDadosadicionais());
								}
							}
							
							if(!Boolean.TRUE.equals(isIndenizacao)){
								contratofaturalocacao2.setValortotal(contratofaturalocacao2.getValortotal().add(contratofaturalocacao.getValortotal()));								
							}else {
								contratofaturalocacao2.setValortotal(valorReposicao);
							}							
							
							List<Contrato> contratos2 = bean2.getListaContrato();
							
							contratos2.addAll(bean.getListaContrato());
							break;
						}
					}
					
					if(!achou){
						listaFaturamentoConjuntoAgrupado.add(bean);
					}
				}
				
				listaFaturamentoConjuntoBean = listaFaturamentoConjuntoAgrupado;
				
				if(listaFaturamentoConjuntoBean.size() == 1){
					FaturamentoConjuntoBean faturamentoConjuntoBean = listaFaturamentoConjuntoBean.get(0); 
					Contratofaturalocacao form = faturamentoConjuntoBean.getContratofaturalocacao();
					if(faturamentoConjuntoBean.getListaContrato() != null && !faturamentoConjuntoBean.getListaContrato().isEmpty()){
						form.setWhereInContrato(CollectionsUtil.listAndConcatenate(faturamentoConjuntoBean.getListaContrato(), "cdcontrato", ","));
					}
					try {
						setInfoForTemplate(request, form);
						setEntradaDefaultInfo(request, form);
						entrada(request, form);
					} catch (Exception e) {}
					return getEntradaModelAndView(request, form);
				}else {
					for (FaturamentoConjuntoBean bean : listaFaturamentoConjuntoBean) {
						Contratofaturalocacao contratofaturalocacao = bean.getContratofaturalocacao();
						
						List<Contrato> contratos = bean.getListaContrato();
						contratofaturalocacao.setListaContrato(contratos);
						
						if(contratos != null && contratos.size() > 1){
							contratofaturalocacao.setDescricao(contratofaturalocacaoService.getDescricaoForFaturarLocacao(contratos));
						}
						contratofaturalocacaoService.gerarNumeroSequencial(contratofaturalocacao);
						contratofaturalocacaoService.saveFromFechamento(contratofaturalocacao);
					}
				}
			}
		}
		
		request.addMessage("Fechamento de contrato realizado com sucesso.");
		request.addMessage("Fatura(s) de Loca��o(�es) criada(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/Contrato");
		return null;
	}
	
	/**
	* M�todo que retorna um arquivo txt com os inserts de origem da fatura de loca��o de acordo com o hist�rico
	*
	* @param request
	* @return
	* @since 09/07/2014
	* @author Luiz Fernando
	*/
	public ModelAndView verificarContratoFaturaSemOrigem(WebRequestContext request){
		return contratofaturalocacaoService.verificarContratoFaturaSemOrigem(request);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView popupEnviarFaturaLocacao(WebRequestContext request, EnviarFaturaLocacaoBean bean) throws Exception {
		bean.setSelectedItens(request.getParameter("selectedItens"));
		
		List<Contratofaturalocacao> listaContratofaturalocacao = contratofaturalocacaoService.findForEnviarFaturaLocacao(bean.getSelectedItens()); 
		List<EnviarFaturaLocacaoEmailBean> listaDestinatario = new ArrayList<EnviarFaturaLocacaoEmailBean>();
		Set<Integer> listaCdpessoa = new HashSet<Integer>();
		String whereInCddocumento = "";
		
		for (Contratofaturalocacao contratofaturalocacao: listaContratofaturalocacao){
			Cliente cliente = contratofaturalocacao.getCliente();
			List<PessoaContato> listaPessoaContato = cliente.getListaContato();
			listaCdpessoa.add(cliente.getCdpessoa());
			
			whereInCddocumento = contratofaturalocacao.getWhereInCddocumentoBoleto();
			
			if (cliente.getEmail()!=null && !cliente.getEmail().trim().equals("")){
				listaDestinatario.add(new EnviarFaturaLocacaoEmailBean(contratofaturalocacao, whereInCddocumento, cliente.getNome(), cliente.getEmail(), true));
			}
			
			if (listaPessoaContato != null){
				for (PessoaContato pessoaContato: listaPessoaContato) {
					Contato contato = pessoaContato.getContato();
					
					if (contato.getEmailcontato()!=null 
							&& !contato.getEmailcontato().trim().equals("")
							&& Boolean.TRUE.equals(contato.getRecebernf())){
						listaDestinatario.add(new EnviarFaturaLocacaoEmailBean(contratofaturalocacao, whereInCddocumento, contato.getNome(), contato.getEmailcontato(), Boolean.TRUE.equals(contato.getReceberboleto())));
					}
				}
			}
		}
		
		Collections.sort(listaDestinatario, new BeanComparator("nome"));
		
		bean.setListaDestinatario(listaDestinatario);
		bean.setMesmoCliente(listaCdpessoa.size()==1);
		bean.setItensSelecionados(listaContratofaturalocacao.size());
		
		return new ModelAndView("direct:/crud/popup/enviarFaturaLocacao", "bean", bean);
	}
	
	public void savePopupEnviarFaturaLocacao(WebRequestContext request, EnviarFaturaLocacaoBean bean) throws Exception {
		contratofaturalocacaoService.savePopupEnviarFaturaLocacao(request, bean);
	}
}