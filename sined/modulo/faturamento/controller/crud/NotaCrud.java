package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ExpedicaoService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotaVendaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GeraReceita;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.SituacaoNotaEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/Nota", authorizationModule=CrudAuthorizationModule.class)
public class NotaCrud extends CrudControllerSined<NotaFiltro, Nota, Nota> {
	private static final String NOTA_FISCAL_PRODUTO = "produto";
	private static final String NOTA_FISCAL_SERVICO = "servico";
	
	private NotaService notaService;
	private PrazopagamentoService prazopagamentoService;
	private NotaDocumentoService notaDocumentoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaVendaService notaVendaService;
	private VendaService vendaService;
	private ArquivonfnotaService arquivonfnotaService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private RequisicaoService requisicaoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ContratoService contratoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private ExpedicaoService expedicaoService;
	private ParametrogeralService parametrogeralService;
	private NotaHistoricoService notaHistoricoService;
	
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setNotaVendaService(NotaVendaService notaVendaService) {this.notaVendaService = notaVendaService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setFechamentofinanceiroService(	FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setExpedicaoService(ExpedicaoService expedicaoService) {this.expedicaoService = expedicaoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Nota form) throws CrudException {
		String notaTipo = (String)request.getParameter("tipoNota");
		
		if (StringUtils.equals(notaTipo, NOTA_FISCAL_PRODUTO)) {
			return new ModelAndView("forward:/faturamento/crud/Notafiscalproduto?ACAO=" + CRIAR);
		} else if (StringUtils.equals(notaTipo, NOTA_FISCAL_SERVICO)) {
			return new ModelAndView("forward:/faturamento/crud/NotaFiscalServico?ACAO=" + CRIAR);
		} else {
			throw new SinedException("Tipo de nota n�o encontrada: " + notaTipo);
		}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Nota form) throws CrudException {
		form = notaService.load(form, "nota.cdNota, nota.notaTipo");
		
		if (form instanceof Notafiscalproduto) {
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=" + EDITAR + "&cdNota=" + form.getCdNota());
		} else if (form instanceof NotaFiscalServico) {
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO=" + EDITAR + "&cdNota=" + form.getCdNota());
		} else {
			throw new SinedException("Tipo de nota n�o existente.");
		}
	}
	
	@Override
	public ModelAndView doConsultar(WebRequestContext request, Nota form) throws CrudException {
		form = notaService.load(form, "nota.cdNota, nota.notaTipo");
		
		if (NotaTipo.NOTA_FISCAL_PRODUTO.equals(form.getNotaTipo())) {
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=" + CONSULTAR + "&cdNota=" + form.getCdNota().toString());
		} else if (NotaTipo.NOTA_FISCAL_SERVICO.equals(form.getNotaTipo())) {
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO=" + CONSULTAR + "&cdNota=" + form.getCdNota().toString());
		} else {
			throw new SinedException("Tipo de nota n�o existente.");
		}
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, NotaFiltro filtro)throws CrudException {
		if(Util.booleans.isTrue(filtro.getLimparFiltro())){
			filtro = new NotaFiltro();
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request, NotaFiltro filtro) throws Exception {
		throw new SinedException("A��o n�o permitida... Favor Contactar o Administrador...");
//		request.setAttribute("listaNotaStatus", NotaStatus.getTodosEstados());
//		request.setAttribute("listaNotaTipo", NotaTipo.getTodosTipos());
	}
	
	@Override
	protected void excluir(WebRequestContext request, Nota bean) throws Exception {
		throw new SinedException("N�o � permitido excluir Notas");
	}
	
	/**
	 * Action para abrir a popup do estorno do registro do canhoto.
	 * 	 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public ModelAndView abrirPopupEstornoRegistroCanhoto(WebRequestContext request){
		NotaHistorico notaHistorico = new NotaHistorico();
		notaHistorico.setItensSelecionados(SinedUtil.getItensSelecionados(request));
		return new ModelAndView("direct:/process/popup/estornoRegistroCanhoto", "notaHistorico", notaHistorico);
	}
	
	/**
	 * Action para salvar o estorno de registro de canhoto.
	 * 	 
	 * @param request
	 * @param notaHistorico
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public void salvarPopupEstornoRegistroCanhoto(WebRequestContext request, NotaHistorico notaHistorico){
		int sucesso = 0;
		
		String whereIn = notaHistorico.getItensSelecionados();
		List<Nota> listaNota = notaService.findForEstornoCanhoto(whereIn);
		List<String> notasErro = new ArrayList<String>();
		for (Nota nota : listaNota) {
			if(nota.getRetornocanhoto() != null && nota.getRetornocanhoto()){
				notaService.updateRetornoCanhotoFalse(nota.getCdNota());
				
				NotaHistorico notaHistorico_aux = new NotaHistorico();
				notaHistorico_aux.setNota(nota);
				notaHistorico_aux.setNotaStatus(NotaStatus.ESTORNO_CANHOTO);
				notaHistorico_aux.setObservacao(notaHistorico.getObservacao());
				notaHistoricoService.saveOrUpdate(notaHistorico_aux);
				
				sucesso++;
			} else {
				notasErro.add(nota.getNumero());
			}
		}
		
		if(sucesso > 0){
			request.addMessage("Registro de canhoto estornado com sucesso.");
		}
		if(notasErro.size() > 0){
			request.addError("As notas " + CollectionsUtil.concatenate(notasErro, ", ") + " n�o tiveram o estorno realizado, pois n�o existe registro de canhoto informado.");
		}
		SinedUtil.fechaPopUp(request);
	}

	/**
	 * Action que exibe o popup para o usu�rio inserir a justificativa antes de executar uma a��o
	 * de transi��o de estado.
	 * 
	 * @param request
	 * @param acao
	 * 			a a��o a ser executada
	 * @author Hugo Ferreira
	 */
	public ModelAndView requisitarJustificativa(WebRequestContext request, NotaStatus acao) {
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		Integer codAcao = acao.getCdNotaStatus();
		
		boolean erro = false;
		boolean estornar = codAcao == 101;
		boolean cancelar = codAcao == 3;
		
		NotaStatus[] situacoes = null; 
		String stringacao = "";
		String errosituacao = null;
		
		if(estornar){
			situacoes = new NotaStatus[]{NotaStatus.CANCELADA, 
											NotaStatus.NFE_CANCELANDO, 
											NotaStatus.NFSE_CANCELANDO, 
											NotaStatus.NFSE_SOLICITADA, 
											NotaStatus.NFE_SOLICITADA,
											NotaStatus.LIQUIDADA,
											NotaStatus.NFSE_LIQUIDADA,
											NotaStatus.NFE_LIQUIDADA};
			stringacao = "estornar";
			errosituacao = "S� pode estornar nota(s) que est�o nas situa��es 'LIQUIDADA', 'CANCELADA', 'CANCELANDO' ou 'SOLICITADA'";
			
			request.setAttribute("descricao", "Estornar");
			request.setAttribute("msg", "Justificativa para o estorno");
			request.setAttribute("acaoSalvar", "estornarNota");
			
			
			List<Nota> listaNota = notaService.findNotas(itensSelecionados);
			for (Nota nota : listaNota) {
				boolean inSituacaoSolicitada = nota.getNotaStatus() != null && 
												(nota.getNotaStatus().equals(NotaStatus.NFSE_SOLICITADA) || 
												nota.getNotaStatus().equals(NotaStatus.NFE_SOLICITADA));

				boolean inSituacaoLiquidada = nota.getNotaStatus() != null && 
												(nota.getNotaStatus().equals(NotaStatus.LIQUIDADA) || 
												nota.getNotaStatus().equals(NotaStatus.NFSE_LIQUIDADA) ||
												nota.getNotaStatus().equals(NotaStatus.NFE_LIQUIDADA)
												);
				
				boolean inSituacaoDenegada = NotaStatus.NFE_DENEGADA.equals(nota.getNotaStatus());
				
				if(inSituacaoSolicitada){
					List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(nota.getCdNota().toString());
					if(listaArquivonfnota != null && listaArquivonfnota.size() > 0){
						for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
							if(arquivonfnota.getArquivonf() != null && 
									arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
									arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.GERADO) &&
									arquivonfnota.getArquivonf().getEmitindo() != null && 
									arquivonfnota.getArquivonf().getEmitindo()){
								erro = true;
								request.addError("Nota(s) j� marcada pelo emissor de Nota Fiscal Eletr�nica.");		
								break;
							}
						}
						
						if(erro) break;
					}
				}
				
				if(inSituacaoDenegada && parametrogeralService.getBoolean(Parametrogeral.CANCELARVENDA_NFDENEGADA)){
					erro = true;
					request.addError("Nota fiscal denegada n�o pode gerar receita, ser cancelada e/ou estornada.");		
					break;
				}
				
				if(inSituacaoLiquidada && notaDocumentoService.haveDocumentoNaoCanceladoByNota(nota)){
					erro = true;
					request.addError("Para realizar esta opera��o, � necess�rio Cancelar as contas a receber associadas a esta Nota Fiscal. Favor verificar.");		
					break;
				}
			}
		} 
		
		boolean origemVenda = false;
		boolean origemVendaGeracaoContaAposEmissaoNota = false;
		if(cancelar){
			situacoes = new NotaStatus[]{NotaStatus.EMITIDA, 
										NotaStatus.LIQUIDADA,
										NotaStatus.NFE_EMITIDA,
										NotaStatus.NFE_LIQUIDADA,
										NotaStatus.NFSE_EMITIDA,
										NotaStatus.NFSE_LIQUIDADA,
										NotaStatus.EM_ESPERA};
			stringacao = "cancelar";
			errosituacao = "Existem contas a receber vinculados a nota que n�o est�o canceladas. Gentileza cancelar as contas a receber primeiro.Para realizar esta opera��o, � necess�rio Cancelar as contas a receber associadas a esta Nota Fiscal. Favor verificar.";
			
			
			request.setAttribute("descricao", "Cancelar");
			request.setAttribute("msg", "Justificativa para o cancelamento");
			request.setAttribute("acaoSalvar", "cancelarNota");
			
			erro = erro || notaDocumentoService.validaCancelamentoNotaWebserviceFalse(request, itensSelecionados);
			
			origemVenda = notaService.existOrigem(itensSelecionados);
			origemVendaGeracaoContaAposEmissaoNota = notaService.existOrigemVendaGeracaoContaAposEmissaoNota(itensSelecionados, Boolean.FALSE);
		}
		
		
		boolean haveSituacaoDiferente = notaService.haveNFDiferenteStatus(itensSelecionados, situacoes);
		if(haveSituacaoDiferente){
			if(errosituacao != null) request.addError(errosituacao);
			erro = true;
		}
		
		if(erro){
			request.addError("N�o foi poss�vel " + stringacao + " nota(s).");
			SinedUtil.recarregarPaginaWithClose(request);
			return null;
		}
		
		request.setAttribute("origemVenda", origemVenda);
		request.setAttribute("origemVendaGeracaoContaAposEmissaoNota", origemVendaGeracaoContaAposEmissaoNota);
		
		NotaHistorico notaHistorico = new NotaHistorico();
		notaHistorico.setItensSelecionados(itensSelecionados);
		notaHistorico.setNotaStatus(acao);
		return new ModelAndView("direct:/crud/popup/justificativaAcao", "notaHistorico", notaHistorico).addObject("cancelar", cancelar);
	}
	
	/**
	 * Action de CANCELAR nota fiscal.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaService#cancelar(String, String)
	 * @param request
	 * @param nota
	 * @author Hugo Ferreira
	 */
	public void cancelarNota(WebRequestContext request, NotaHistorico notaHistorico) {
		int countSucesso = 0;
		try{
			countSucesso = notaService.cancelar(request, notaHistorico.getItensSelecionados(), notaHistorico.getObservacao(), notaHistorico.getMotivoCancelamentoFaturamento());
			for (Nota nota : notaService.findNotas(notaHistorico.getItensSelecionados())) {	
				if(NotaStatus.CANCELADA.equals(nota.getNotaStatus()) || NotaStatus.NFE_CANCELANDO.equals(nota.getNotaStatus())){
					if(NotaTipo.NOTA_FISCAL_PRODUTO.equals(nota.getNotaTipo())){
						notafiscalprodutoService.atualizaExpedicao(null, null,new Notafiscalproduto(nota.getCdNota()), nota.getNotaStatus());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		int total = notaHistorico.getItensSelecionados().split(",").length;
		int countSemSucesso = total - countSucesso;
		
		if (countSucesso > 0) {
			request.addMessage(countSucesso + " nota(s) foi(ram) cancelada(s) com sucesso.", MessageType.INFO);
		}
		if (countSemSucesso > 0) {
			request.addMessage(countSemSucesso + " notas(s) n�o p�de(puderam) ser cancelada(s).", MessageType.ERROR);
		}
		
		movimentacaoestoqueService.cancelarByNota(notaHistorico.getItensSelecionados());
		contratoService.estornarContratoAposCancelarNota(request, notaHistorico.getItensSelecionados());
		
		List<NotaVenda> listaNotaVenda = new ArrayList<NotaVenda>();
		List<Venda> listaVenda = new ArrayList<Venda>();
		List<Expedicao> listaExpedicao = new ArrayList<Expedicao>();
		listaNotaVenda = notaVendaService.findByNota(notaHistorico.getItensSelecionados());
		for (NotaVenda nv : listaNotaVenda) {
			if (nv.getVenda() != null && (nv.getVenda().getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(nv.getVenda().getVendasituacao()))){
				listaVenda.add(nv.getVenda());
			}
			if(nv.getExpedicao() != null && Expedicaosituacao.FATURADA.equals(nv.getExpedicao().getExpedicaosituacao()) && !listaExpedicao.contains(nv.getExpedicao())){
				listaExpedicao.add(nv.getExpedicao());
			}
		}
		
		expedicaoService.verificaSituacaoExpedicaoAposCancelamentoNota(listaExpedicao, notaHistorico.getItensSelecionados());
		
		List<NotaDocumento> listaNotaDocumento = notaDocumentoService.findForAlterarSituacaoVenda(notaHistorico.getItensSelecionados());
		List<Venda> listaVendaFaturada = new ArrayList<Venda>();
		if(listaNotaDocumento != null && !listaNotaDocumento.isEmpty()){
			for (NotaDocumento nd : listaNotaDocumento) {
				if (nd.getDocumento() != null && nd.getDocumento().getListaDocumentoOrigem() != null && 
						!nd.getDocumento().getListaDocumentoOrigem().isEmpty()){
					for(Documentoorigem documentoorigem : nd.getDocumento().getListaDocumentoOrigem()){
						if(documentoorigem.getVenda() != null){
							listaVendaFaturada.add(documentoorigem.getVenda());
						}
					}
				}
			}
		}
		
		if(Boolean.TRUE.equals(notaHistorico.getCancelamentoVenda())){
			for (Venda venda : listaVenda) {
				try{
					if(!notaVendaService.existNotaEmitida(venda, null) && (venda.getVendasituacao() == null || !Vendasituacao.CANCELADA.equals(venda.getVendasituacao()))){
						if(expedicaoService.haveExpedicaoByVendaNotInSituacao(venda, Expedicaosituacao.CANCELADA)){
							vendaService.updateSituacaoVenda(venda.getCdvenda().toString(), Vendasituacao.REALIZADA);
							Venda vendaAux = vendaService.loadForGerarReserva(venda);
							vendaService.regerarReservaNoCancelamentoNota(vendaAux, vendaAux.getPedidovendatipo(), vendaAux.getPedidovendatipo().getBaixaestoqueEnum());
							request.addMessage("A venda " + venda.getCdvenda() + " n�o foi cancelada. Essa venda possui expedi��o pendente com situa��o diferente de cancelada.", MessageType.WARN);
						}else{
							vendaService.cancelamentoVenda(venda, false, Boolean.FALSE);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					request.addMessage("Venda " + venda.getCdvenda() + " n�o foi cancelada. " + e.getMessage(), MessageType.WARN);
				}
			}
		} else {
			String whereIn = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
			if (!whereIn.equals("")){
				StringBuilder whereInUpdate = new StringBuilder();
				for(String idVenda : whereIn.split(",")){
					if(!notaVendaService.existNotaEmitida(new Venda(Integer.parseInt(idVenda)), null)){
						whereInUpdate.append(idVenda).append(",");
					}
				}
				if(whereInUpdate.length() > 0){
					vendaService.updateSituacaoVenda(whereInUpdate.substring(0, whereInUpdate.length()-1), Vendasituacao.REALIZADA);
				}
			}
			for(Venda venda: listaVenda){
				Venda bean = vendaService.loadForGerarReserva(venda);
				if(bean.getPedidovendatipo() != null){
					if(BaixaestoqueEnum.APOS_EMISSAONOTA.equals(bean.getPedidovendatipo().getBaixaestoqueEnum()) && !SituacaoNotaEnum.AUTORIZADA.equals(bean.getPedidovendatipo().getSituacaoNota())){
						vendaService.regerarReservaNoCancelamentoNota(bean, bean.getPedidovendatipo(), bean.getPedidovendatipo().getBaixaestoqueEnum());
						for(Vendamaterial vendamaterial : bean.getListavendamaterial()){
							vendaService.cancelarDevolucao(request, bean, vendamaterial);
						}
					}
				}
			}
		}
		
		if(Boolean.TRUE.equals(notaHistorico.getCancelamentoContareceberAposEmissaoNota()) && SinedUtil.isListNotEmpty(listaVenda)){
			vendaService.cancelarContareceberAposCancelarNotaComGeracaoReceitaAposEmissao(request, listaVenda, Boolean.FALSE, Boolean.FALSE);
		}
		
		if(listaVendaFaturada != null && !listaVendaFaturada.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(listaVendaFaturada, "cdvenda", ",");
			if (whereIn != null && !whereIn.equals("")){
				StringBuilder whereInUpdate = new StringBuilder();
				for(String idVenda : whereIn.split(",")){
					if(!notaVendaService.existNotaEmitida(new Venda(Integer.parseInt(idVenda)), null)){
						whereInUpdate.append(idVenda).append(",");
					}
				}
				if(whereInUpdate.length() > 0){
					vendaService.updateSituacaoVenda(whereInUpdate.substring(0, whereInUpdate.length()-1), Vendasituacao.REALIZADA);
				}
			}
		}
		
		requisicaoService.atualizaSituacaoRequisicaoCancelamentoNFS(notaHistorico.getItensSelecionados());
		SinedUtil.recarregarPaginaWithClose(request);
	}
	
	
	
	/**
	 * Action de ESTORNAR nota fiscal.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaService#estornar(String, String)
	 * @param request
	 * @param nota
	 * @author Hugo Ferreira
	 */
	public void estornarNota(WebRequestContext request, NotaHistorico notaHistorico) {
        if(notafiscalprodutoService.haveNotaCriadasViaIntegracaoECF(notaHistorico.getItensSelecionados())){
            request.addError("Nota importada via integra��o com ECF. Favor efetuar o cancelamento no local de origem.");
            SinedUtil.recarregarPaginaWithClose(request);
        }
		int countSucesso = 0;
		try{
			countSucesso = notaService.estornar(notaHistorico.getItensSelecionados(), notaHistorico.getObservacao());
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		int total = notaHistorico.getItensSelecionados().split(",").length;
		int countSemSucesso = total - countSucesso;
		
		if (countSucesso > 0) {
			request.addMessage(countSucesso + " nota(s) foi(ram) estornada(s) com sucesso.", MessageType.INFO);
		}
		if (countSemSucesso > 0) {
			request.addMessage(countSemSucesso + " nota(s) n�o p�de(puderam) ser estornada(s).", MessageType.ERROR);
		}
		
		SinedUtil.recarregarPaginaWithClose(request);
	}

	/**
	 * Redireciona para a interface de gerar receita
	 *
	 * @param request
	 * @param nota
	 * @return
	 * @throws SinedException : se o usu�rio logado n�o possuir permiss�o para gerar receita.
	 *
	 * @author Hugo Ferreira
	 */
	public ModelAndView abrirTelaGerarReceita(WebRequestContext request, Nota nota) {
		if (!SinedUtil.isUserHasAction("GERAR_RECEITA_NOTA")) {
			throw new SinedException("Infelizmente o usu�rio logado n�o tem permiss�o para gerar receita." +
					" Entre em contato com o administrador do sistema para soliciar a permiss�o.");
		}
		nota = notaService.carregaDadosNota(nota);
		if(!NotaStatus.EMITIDA.equals(nota.getNotaStatus()) && !NotaStatus.NFSE_EMITIDA.equals(nota.getNotaStatus())
				&& !NotaStatus.NFE_EMITIDA.equals(nota.getNotaStatus())){
			request.addError("Receita j� gerada para esta nota.");
			return new ModelAndView("redirect:" + request.getParameter("CONTROLLER"));
		}
		
		if(notaDocumentoService.haveDocumentoNaoCanceladoByNota(nota)){
			request.addError("Receita j� gerada para esta nota.");
//			if(notaDocumentoService.isNotaWebservice(nota) || notaService.isNotaPossuiReceita(nota)){
				boolean alterarStatus = false;
				if(NotaStatus.EMITIDA.equals(nota.getNotaStatus())){
					nota.setNotaStatus(NotaStatus.LIQUIDADA);
					alterarStatus = true;
				}else if(NotaStatus.NFSE_EMITIDA.equals(nota.getNotaStatus())){
					nota.setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
					alterarStatus = true;
				}else if(NotaStatus.NFE_EMITIDA.equals(nota.getNotaStatus())){
					nota.setNotaStatus(NotaStatus.NFE_LIQUIDADA);
					alterarStatus = true;
				}
				
				if(alterarStatus){
					notaService.alterarStatusAcao(nota, "", null);
					request.addMessage("Nota liquidada com sucesso.", MessageType.INFO);
				}
//			}
			return new ModelAndView("redirect:" + request.getParameter("CONTROLLER")); 
		}
		
		//Verifica��o de data limite do ultimo fechamento. 
		NotaFiscalServico aux = new NotaFiscalServico();
		aux.setWhereIn(nota.getCdNota().toString());
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem notas cuja data de vencimento refere-se a um per�odo j� fechado.");
			return new ModelAndView("redirect:" + request.getParameter("CONTROLLER")); 
		}
		
		if(notaDocumentoService.isNotaWebservice(nota) || notaService.isNotaPossuiReceita(nota)){
			if(NotaStatus.EMITIDA.equals(nota.getNotaStatus()))
				nota.setNotaStatus(NotaStatus.LIQUIDADA);
			else if(NotaStatus.NFSE_EMITIDA.equals(nota.getNotaStatus()))
				nota.setNotaStatus(NotaStatus.NFSE_LIQUIDADA);
			else if(NotaStatus.NFE_EMITIDA.equals(nota.getNotaStatus()))
				nota.setNotaStatus(NotaStatus.NFE_LIQUIDADA);
			
			notaService.alterarStatusAcao(nota, "", null);
			request.addError("A nota j� possui conta a receber.");
			request.addMessage("Nota liquidada com sucesso.", MessageType.INFO);
			return new ModelAndView("redirect:"+request.getParameter("CONTROLLER"));
		}
		
		Notafiscalproduto notafiscalproduto = notafiscalprodutoService.loadForReceita(new Notafiscalproduto(nota.getCdNota()));
		NotaFiscalServico notaFiscalServico = notaFiscalServicoService.loadForReceita(nota.getCdNota());
		
		if(notaFiscalServico != null && 
				notaFiscalServico.getNaturezaoperacao() != null && 
				notaFiscalServico.getNaturezaoperacao().getSimplesremessa() != null && 
				notaFiscalServico.getNaturezaoperacao().getSimplesremessa()){
			request.addError("N�o � permitido gerar receita de uma nota que tenha a natureza de opera��o marcada para n�o gerar receita.");
			return new ModelAndView("redirect:"+request.getParameter("CONTROLLER"));
		}
		
		if(notafiscalproduto != null && 
				notafiscalproduto.getNaturezaoperacao() != null && 
				notafiscalproduto.getNaturezaoperacao().getSimplesremessa() != null && 
				notafiscalproduto.getNaturezaoperacao().getSimplesremessa()){
			request.addError("N�o � permitido gerar receita de uma nota que tenha a natureza de opera��o marcada para n�o gerar receita.");
			return new ModelAndView("redirect:"+request.getParameter("CONTROLLER"));
		}
		
		GeraReceita geraReceita = notaService.criarGeraReceita(nota);
		
		request.setAttribute("listaPrazoPagamento", prazopagamentoService.findAtivos());
		request.setAttribute("CONTROLLER", request.getParameter("CONTROLLER"));
		
		if(notafiscalproduto != null){
			if(notafiscalproduto.getValordescontofatura() != null){
				geraReceita.setDescontoContratualMny(notafiscalproduto.getValordescontofatura());
			}
			notafiscalprodutoService.setPrazogapamentoUnico(notafiscalproduto);
		}else if(notaFiscalServico != null){
			if(notaFiscalServico.getValordescontofatura() != null){
				geraReceita.setDescontoContratualMny(notaFiscalServico.getValordescontofatura());
			}
			notaFiscalServicoService.setPrazogapamentoUnico(notaFiscalServico);
		}
		
		boolean existeDuplicata = (notafiscalproduto != null && SinedUtil.isListNotEmpty(notafiscalproduto.getListaDuplicata())) || 
								  (notaFiscalServico != null && SinedUtil.isListNotEmpty(notaFiscalServico.getListaDuplicata()));
		request.setAttribute("existeDuplicata", existeDuplicata);
		
		if(existeDuplicata && ((notaFiscalServico != null && notaFiscalServico.getPrazopagamentofatura() != null) ||
				(notafiscalproduto != null && notafiscalproduto.getPrazopagamentofatura() != null))){
			if(notaFiscalServico != null && notaFiscalServico.getPrazopagamentofatura() != null){
				geraReceita.setValorUmaParcela(notaFiscalServico.getValorliquidofatura());
				geraReceita.setPrazoPagamento(notaFiscalServico.getPrazopagamentofatura());
				geraReceita.setExisteDuplicata(existeDuplicata);
			}
			if(notafiscalproduto != null && notafiscalproduto.getPrazopagamentofatura() != null){
				geraReceita.setValorUmaParcela(notafiscalproduto.getValorliquidofatura());
				geraReceita.setPrazoPagamento(notafiscalproduto.getPrazopagamentofatura());
				geraReceita.setExisteDuplicata(existeDuplicata);
			}
			request.getSession().setAttribute("GERAR_RECEITA__NOTA_BEAN", geraReceita);
			return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=gerarReceita");
		}else {
			return new ModelAndView("/process/gerarReceita", "geraReceita", geraReceita);
		}
	}
	
	/**
	 * Ajax para obter o n�mero de itens de um prazo de pagamento
	 * 
	 * @see br.com.linkcom.sined.geral.service.PrazopagamentoService#countItens(Prazopagamento)
	 * @param request
	 * @param prazo
	 * @author Hugo Ferreira
	 */
	public void ajaxCountPrazoItem(WebRequestContext request, Prazopagamento prazo) {
		Long count = prazopagamentoService.countItens(prazo);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var count = " + count.toString());
	}
	
	public ModelAndView consultarByTipo(WebRequestContext request, Nota nota){
		if(nota.getCdNota() != null){
			nota = notaService.load(nota, "nota.cdNota, nota.notaTipo");
			if(nota != null){
				if(nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)){
					return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota=" + nota.getCdNota());
				} else {
					return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO=consultar&cdNota=" + nota.getCdNota());
				}
			} else return null;
		} else return null;
	}
	
	/**
	 * @param request
	 * @return
	 * @author Andrey Leonardo
	 */
	public ModelAndView verificarNotaProdutoExportacao(WebRequestContext request){
		String itensSelecionados = SinedUtil.getItensSelecionados(request);		
		boolean erro = false;		
		NotaStatus[] situacoes = null;
		String errosituacao = null;
		
		situacoes = new NotaStatus[] {NotaStatus.NFE_EMITIDA,
				NotaStatus.NFE_LIQUIDADA };
		
		errosituacao = "A(s) nota(s) fiscal(is) tem que estar na situa��o 'NF-E EMITIDA' ou 'NF-E LIQUIDADA'.";	
		
		boolean haveSituacaoDiferente = notaService.haveNFDiferenteStatus(itensSelecionados, situacoes);
		if(!haveSituacaoDiferente){
			List<Notafiscalproduto> listaNotaProduto = notafiscalprodutoService.findForMemorandoExportacao(itensSelecionados);
			if(!notafiscalprodutoService.isNotaExportacao(listaNotaProduto)){
				haveSituacaoDiferente = true;
				errosituacao = "A(s) nota(s) fiscal(is) precisa(m) ter fim de exporta��o.";
			}else{
				for(Notafiscalproduto nota : listaNotaProduto){
					if(!notafiscalprodutoService.isExisteVinculo(nota)){
						haveSituacaoDiferente = true;
						errosituacao = "A(s) nota(s) fiscal(is) precisa(m) ter produto(s) vinculado(s) a nota(s) de entrada.";
					}
				}
			}
		}
		if(haveSituacaoDiferente){
			if(errosituacao != null) 
				request.addError(errosituacao);
			erro = true;
		}
		
		if(erro){
			request.addError("N�o foi poss�vel emitir memorando de exporta��o da(s) nota(s).");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		return new ModelAndView("redirect:/faturamento/process/MemorandoExportacao?ACAO=abrirPopupGerarMemorando&itensSelecionados="+itensSelecionados);
	}
	
	public ModelAndView redirecionamentoNota(WebRequestContext request, Nota nota){
		if(nota.getCdNota() == null) throw new SinedException("Nenhum item encontrado.");
		
		Nota bean = notaService.load(nota, "nota.cdNota, nota.notaTipo");
		
		String url = "/faturamento/crud/" + 
			(bean != null && NotaTipo.NOTA_FISCAL_PRODUTO.equals(bean.getNotaTipo()) ? "Notafiscalproduto" : "NotaFiscalServico") +
			"?ACAO=consultar&cdNota="+bean.getCdNota();
		return new ModelAndView("redirect:"+url);
	}
}
