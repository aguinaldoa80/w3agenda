package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Contigencianfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Inutilizacaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.ContigencianfeService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.InutilizacaonfeService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.InutilizacaonfeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/faturamento/crud/Inutilizacaonfe", "/faturamento/crud/Inutilizacaonfce"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdinutilizacaonfe", "empresa", "configuracaonfe", "dtinutilizacao", "numinicio", "numfim", "situacao"})
public class InutilizacaonfeCrud extends CrudControllerSined<InutilizacaonfeFiltro, Inutilizacaonfe, Inutilizacaonfe> {
	
	private InutilizacaonfeService inutilizacaonfeService;
	private EmpresaService empresaService;
	private ContigencianfeService contigencianfeService;
	private ConfiguracaonfeService configuracaonfeService;
	
	public void setContigencianfeService(ContigencianfeService contigencianfeService) {
		this.contigencianfeService = contigencianfeService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setInutilizacaonfeService(InutilizacaonfeService inutilizacaonfeService) {
		this.inutilizacaonfeService = inutilizacaonfeService;
	}
	
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	
	private boolean isNfe() {
		return NeoWeb.getRequestContext().getServletRequest().getRequestURI().endsWith("/faturamento/crud/Inutilizacaonfe");
	}
	
	private ModeloDocumentoFiscalEnum getModelo() {
		return isNfe() ? ModeloDocumentoFiscalEnum.NFE : ModeloDocumentoFiscalEnum.NFCE;
	}
	
	@Override
	protected Inutilizacaonfe criar(WebRequestContext request, Inutilizacaonfe form) throws Exception {
		Inutilizacaonfe inutilizacaonfe = super.criar(request, form);
		
		inutilizacaonfe.setDtinutilizacao(SinedDateUtils.currentDate());
		inutilizacaonfe.setSituacao(Inutilizacaosituacao.GERADO);
		
		return inutilizacaonfe;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Inutilizacaonfe form) throws Exception {
		if (form.getModeloDocumentoFiscalEnum() == null) {
			form.setModeloDocumentoFiscalEnum(getModelo());
		}
		
		request.setAttribute("modelo", getModelo());
		
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("PROCESSADA_COM_SUCESSO", Inutilizacaosituacao.PROCESSADO_COM_SUCESSO.equals(form.getSituacao()));
	}
	
	@Override
	protected void salvar(WebRequestContext request, Inutilizacaonfe bean) throws Exception {
		bean.setModeloDocumentoFiscalEnum(getModelo());
		
		inutilizacaonfeService.saveOrUpdate(bean);
	}
	
	@Override
	protected void listagem(WebRequestContext request, InutilizacaonfeFiltro filtro) throws Exception {
		request.setAttribute("modelo", getModelo());
	}
	
	@Override
	protected ListagemResult<Inutilizacaonfe> getLista(WebRequestContext request, InutilizacaonfeFiltro filtro) {
		filtro.setModeloDocumentoFiscalEnum(getModelo());
		
		return super.getLista(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, InutilizacaonfeFiltro filtro) throws CrudException {
		filtro.setModeloDocumentoFiscalEnum(getModelo());
		
		return super.doListagem(request, filtro);
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Inutilizacaonfe form) throws CrudException {
		String acao = request.getParameter("ACAO");
		form = inutilizacaonfeService.load(form);
		
		if (form.getSituacao() != null && form.getSituacao().equals(Inutilizacaosituacao.PROCESSADO_COM_SUCESSO) && acao.equals("editar")) {
			if (ModeloDocumentoFiscalEnum.NFCE.equals(form.getModeloDocumentoFiscalEnum())) {
				request.addError("N�o � poss�vel editar NFC-e inutilizada processada com sucesso.");
				SinedUtil.redirecionamento(request, "/faturamento/crud/Inutilizacaonfce?ACAO=consultar&cdinutilizacaonfe=" + form.getCdinutilizacaonfe());
			} else {
				request.addError("N�o � poss�vel editar NF-e inutilizada processada com sucesso.");
				SinedUtil.redirecionamento(request, "/faturamento/crud/Inutilizacaonfe?ACAO=consultar&cdinutilizacaonfe=" + form.getCdinutilizacaonfe());
			}
			return null;
		}
		
		return super.doEditar(request, form);
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Inutilizacaonfe form) throws CrudException {
		String whereIn = request.getParameter("itenstodelete");
		
		if (form != null && form.getCdinutilizacaonfe() != null) {
			form = inutilizacaonfeService.load(form);
			if(form.getSituacao() != null && form.getSituacao().equals(Inutilizacaosituacao.PROCESSADO_COM_SUCESSO)){
				if (isNfe()) {
					request.addError("N�o � poss�vel excluir NF-e inutilizada processada com sucesso.");
					SinedUtil.redirecionamento(request, "/faturamento/crud/Inutilizacaonfe?ACAO=consultar&cdinutilizacaonfe=" + form.getCdinutilizacaonfe());
					return null;
				} else {
					request.addError("N�o � poss�vel excluir NFC-e inutilizada processada com sucesso.");
					SinedUtil.redirecionamento(request, "/faturamento/crud/Inutilizacaonfce?ACAO=consultar&cdinutilizacaonfce=" + form.getCdinutilizacaonfe());
					return null;
				}
			}
		}
		
		if (StringUtils.isNotEmpty(whereIn)) {
			List<Inutilizacaonfe> listaInutilizacao = inutilizacaonfeService.findForExcluirInutilizacao(whereIn);
			
			if(listaInutilizacao != null){
				for (Inutilizacaonfe inutilizacaonfe : listaInutilizacao) {
					if (inutilizacaonfe != null && inutilizacaonfe.getSituacao() != null && inutilizacaonfe.getSituacao().equals((Inutilizacaosituacao.PROCESSADO_COM_SUCESSO))) {
						if (isNfe()) {
							throw new SinedException("N�o � poss�vel excluir NF-e inutilizada processada com sucesso.");
						} else {
							throw new SinedException("N�o � poss�vel excluir NFC-e inutilizada processada com sucesso.");
						}
					}
				}
			}
		}
		
		return super.doExcluir(request, form);
	}
	
	public ModelAndView ajaxDadosConfiguracaonfe(WebRequestContext request, Inutilizacaonfe bean){
		ModelAndView retorno = new JsonModelAndView();
		
		if (bean.getEmpresa() != null && bean.getConfiguracaonfe() != null) {
			Configuracaonfe configNfe = configuracaonfeService.load(bean.getConfiguracaonfe());
			
			if (Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO.equals(configNfe.getTipoconfiguracaonfe())) {
				Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.serienfe");
				retorno.addObject("serienfe", empresa.getSerienfe());
			} else if (Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR.equals(configNfe.getTipoconfiguracaonfe())) {
				Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.serieNfce");
				retorno.addObject("serienfe", empresa.getSerieNfce());
			}
		}
		
		return retorno.addObject("modelo", getModelo());
	}
	
	/**
	 * Cria o XML da solicita��o de inutiliza��o de NF-e
	 *
	 * @param request
	 * @return
	 * @since 01/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView enviarSolicitacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Inutilizacaonfe> lista = inutilizacaonfeService.findForSolicitacao(whereIn);
		
		// VALIDA��O DA SITUA��O
		for (Inutilizacaonfe inutilizacaonfe : lista) {
			if(inutilizacaonfe.getSituacao() == null || !inutilizacaonfe.getSituacao().equals(Inutilizacaosituacao.GERADO)) {
				request.addError("S� pode solicitar inutiliza��es com situa��o 'GERADO'.");
				
				return sendRedirectToAction("listagem");
			}
		}
		
		StringBuilder erro = new StringBuilder();
		
		// VALIDA��O DOS DADOS
		for (Inutilizacaonfe inutilizacaonfe : lista) {
			if (inutilizacaonfe.getConfiguracaonfe().getUf() == null) {
				erro.append("Cadastrar UF de emiss�o de " + (isNfe() ? "NF-e" : "NFC-e") + " na configura��o " + inutilizacaonfe.getConfiguracaonfe().getDescricao() + "<BR>");
			} 
			
			if (inutilizacaonfe.getConfiguracaonfe().getEmpresa() == null || inutilizacaonfe.getConfiguracaonfe().getEmpresa().getSerienfe() == null) {
				erro.append("Cadastrar s�rie da " + (isNfe() ? "NF-e" : "NFC-e") + " na empresa " + inutilizacaonfe.getConfiguracaonfe().getEmpresa().getNome() + "<BR>");
			}
			
			if (inutilizacaonfe.getJustificativa() == null || inutilizacaonfe.getJustificativa().length() < 15) {
				erro.append("Cadastrar a justificativa com mais de 15 caract�res para a inutiliza��o " + inutilizacaonfe.getCdinutilizacaonfe() + "<BR>");
			}
			
			Contigencianfe contigencianfe = contigencianfeService.getContigenciaAtual(inutilizacaonfe.getConfiguracaonfe().getUf());
			
			if (contigencianfe != null) {
				erro.append("Ambiente de contig�ncia habilitado. N�o � poss�vel enviar inutiliza��o para o ambiente de contig�ncia.<BR>");
			}
		}
		
		if (erro.length() > 0) {
			request.addError(erro.toString());
			return sendRedirectToAction("listagem");
		}
		
		try {
			inutilizacaonfeService.enviarSolicitacao(lista, getModelo());
			request.addMessage("Solicita��o criada com sucesso.");
		} catch (Exception e) {
			request.addError(e.getMessage());
			request.addError("N�o foi poss�vel criar o XML de envio da inutiliza��o.");
		}
		
		return sendRedirectToAction("listagem");
	}
}
