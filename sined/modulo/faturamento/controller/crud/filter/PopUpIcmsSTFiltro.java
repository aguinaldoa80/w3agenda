package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;


public class PopUpIcmsSTFiltro {
	
	protected String index;
	
	protected String cest;
	protected Modalidadebcicmsst modalidadebcicmsst;
	protected Double aliquotaicmsst;
	protected Boolean buscarfaixaimpostoaliquotaicmsst;
	protected Money margemvaloradicionalicmsst;
	protected Boolean buscarfaixaimpostomargemvaloradicionalicmsst;
	protected Money reducaobcicmsst;
	protected Uf uficmsst;
	protected String formulabcst;
	protected String formulavalorst;
	
	public String getIndex() {
		return index;
	}
	@MaxLength(7)
	public String getCest() {
		return cest;
	}
	@DisplayName("Modal. de determ. do BC do ICMS ST")
	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}
	@DisplayName("Al�quota ICMS ST")
	public Double getAliquotaicmsst() {
		return aliquotaicmsst;
	}
	@DisplayName("Buscar da faixa de imposto")
	public Boolean getBuscarfaixaimpostoaliquotaicmsst() {
		return buscarfaixaimpostoaliquotaicmsst;
	}
	@DisplayName("% margem de valor adic. ICMS ST")
	public Money getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}
	@DisplayName("Buscar da faixa de imposto")
	public Boolean getBuscarfaixaimpostomargemvaloradicionalicmsst() {
		return buscarfaixaimpostomargemvaloradicionalicmsst;
	}
	@DisplayName("% redu��o da BC do ICMS ST")
	public Money getReducaobcicmsst() {
		return reducaobcicmsst;
	}
	@DisplayName("UF do ICMS ST devido na opera��o")
	public Uf getUficmsst() {
		return uficmsst;
	}
	@DisplayName("F�rmula para calculo da base de calculo do ST")
	public String getFormulabcst() {
		return formulabcst;
	}
	@DisplayName("F�rmula para calculo do valor do ST")
	public String getFormulavalorst() {
		return formulavalorst;
	}
	
	public void setIndex(String index) {
		this.index = index;
	}
	public void setCest(String cest) {
		this.cest = cest;
	}
	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}
	public void setAliquotaicmsst(Double aliquotaicmsst) {
		this.aliquotaicmsst = aliquotaicmsst;
	}
	public void setBuscarfaixaimpostoaliquotaicmsst(
			Boolean buscarfaixaimpostoaliquotaicmsst) {
		this.buscarfaixaimpostoaliquotaicmsst = buscarfaixaimpostoaliquotaicmsst;
	}
	public void setMargemvaloradicionalicmsst(Money margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}
	public void setBuscarfaixaimpostomargemvaloradicionalicmsst(
			Boolean buscarfaixaimpostomargemvaloradicionalicmsst) {
		this.buscarfaixaimpostomargemvaloradicionalicmsst = buscarfaixaimpostomargemvaloradicionalicmsst;
	}
	public void setReducaobcicmsst(Money reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}
	public void setUficmsst(Uf uficmsst) {
		this.uficmsst = uficmsst;
	}
	public void setFormulabcst(String formulabcst) {
		this.formulabcst = formulabcst;
	}
	public void setFormulavalorst(String formulavalorst) {
		this.formulavalorst = formulavalorst;
	}
}
