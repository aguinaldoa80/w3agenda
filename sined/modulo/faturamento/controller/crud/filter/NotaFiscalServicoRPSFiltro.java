package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class NotaFiscalServicoRPSFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
