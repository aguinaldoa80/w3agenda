package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;


import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Coleta.TipoColeta;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.enumeration.LabelColoboradorFiltro;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ColetaFiltro extends FiltroListagemSined {
	
	public static final String TIPO_NOTA_TODOS = "Todos";
	public static final String TIPO_NOTA_ENTRADA = "Entrada";
	public static final String TIPO_NOTA_SAIDA = "Sa�da";
	public static final String TIPO_NOTA_NENHUM = "Nenhum";
	
	protected Integer cdcoleta;
	protected Empresa empresa;
	protected TipoColeta tipo;
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected Colaborador colaborador;
	protected Material material;
	protected String observacao;
	protected Localarmazenagem localarmazenagem;
	protected String identificadorpedidovenda;
	protected Motivodevolucao motivodevolucao;
	protected String identificacaoexterna;
	private Pedidovendasituacao pedidovendasituacao;
	protected String tipoNota;
	
	private SituacaoVinculoExpedicao situacaoColeta;
	private Date dataColetaDe;
	private Date dataColetaAte;
	private LabelColoboradorFiltro tipoColaborador;

	
	@DisplayName("C�digo")
	public Integer getCdcoleta() {
		return cdcoleta;
	}
	public void setCdcoleta(Integer cdcoleta) {
		this.cdcoleta = cdcoleta;
	}
	@DisplayName("Situa��o da Expedi��o")
	public SituacaoVinculoExpedicao getSituacaoColeta() {
		return situacaoColeta;
	}
	public void setSituacaoColeta(SituacaoVinculoExpedicao situacaoColeta) {
		this.situacaoColeta = situacaoColeta;
	}
	public TipoColeta getTipo() {
		return tipo;
	}
	public void setTipo(TipoColeta tipo) {
		this.tipo = tipo;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@DisplayName("Local de Entrada")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	@DisplayName("Pedido venda")
	public String getIdentificadorpedidovenda() {
		return identificadorpedidovenda;
	}
	public void setIdentificadorpedidovenda(String identificadorpedidovenda) {
		this.identificadorpedidovenda = identificadorpedidovenda;
	}	
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	@DisplayName("Motivo de Devolu��o")
	public Motivodevolucao getMotivodevolucao() {
		return motivodevolucao;
	}
	@DisplayName("Tipo de Colaborador")
	public LabelColoboradorFiltro getTipoColaborador() {
		return tipoColaborador;
	}
	public void setTipoColaborador(LabelColoboradorFiltro tipoColaborador) {
		this.tipoColaborador = tipoColaborador;
	}
	public void setMotivodevolucao(Motivodevolucao motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	
	@DisplayName("Identificador externo")
	public String getIdentificacaoexterna() {
		return identificacaoexterna;
	}
	public void setIdentificacaoexterna(String identificacaoexterna) {
		this.identificacaoexterna = identificacaoexterna;
	}
	
	@DisplayName("Situa��o do Pedido")
	public Pedidovendasituacao getPedidovendasituacao() {
		return pedidovendasituacao;
	}
	
	public void setPedidovendasituacao(Pedidovendasituacao pedidovendasituacao) {
		this.pedidovendasituacao = pedidovendasituacao;
	}
	
	@DisplayName("Tipo de nota")
	public String getTipoNota() {
		return tipoNota;
	}
	
	public void setTipoNota(String tipoNota) {
		this.tipoNota = tipoNota;
	}
	public Date getDataColetaDe() {
		return dataColetaDe;
	}
	public void setDataColetaDe(Date dataColetaDe) {
		this.dataColetaDe = dataColetaDe;
	}
	public Date getDataColetaAte() {
		return dataColetaAte;
	}
	public void setDataColetaAte(Date dataColetaAte) {
		this.dataColetaAte = dataColetaAte;
	}
}