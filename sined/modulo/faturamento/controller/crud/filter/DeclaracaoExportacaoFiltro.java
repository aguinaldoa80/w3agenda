package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.enumeration.TipoConhecimentoDeEmbarque;
import br.com.linkcom.sined.geral.bean.enumeration.TipoDocumentoDeclaracao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoNaturezaDaDeclaracao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DeclaracaoExportacaoFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private TipoDocumentoDeclaracao tipoDocumentoDeclaracao;
	private String numeroDeclaracao;
	private Date dtInicioDeclaracao;
	private Date dtFimDeclaracao;
	private TipoNaturezaDaDeclaracao tipoNatureza;
	private Date dtInicioDE;
	private Date dtFimDE;
	private TipoConhecimentoDeEmbarque tipoDeEmbarque;
	private String numeroNFe;
	private Date dtInicioEmissaoNF;
	private Date dtFimEmissaoNF;
	private Material material; 
		
/////////////////////////////////////////////////////////////////////|
//INICIO DOS GET 											/////////|
/////////////////////////////////////////////////////////////////////|
	
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("TIPO DOCUMENTO DECLARA��O")
	public TipoDocumentoDeclaracao getTipoDocumentoDeclaracao() {
		return tipoDocumentoDeclaracao;
	}
	@DisplayName("N�MERO DECLARA��O")
	public String getNumeroDeclaracao() {
		return numeroDeclaracao;
	}
	public Date getDtInicioDeclaracao() {
		return dtInicioDeclaracao;
	}
	public Date getDtFimDeclaracao() {
		return dtFimDeclaracao;
	}
	@DisplayName("NATUREZA DA DECLARA��O")
	public TipoNaturezaDaDeclaracao getTipoNatureza() {
		return tipoNatureza;
	}
	public Date getDtInicioDE() {
		return dtInicioDE;
	}
	public Date getDtFimDE() {
		return dtFimDE;
	}
	public TipoConhecimentoDeEmbarque getTipoDeEmbarque() {
		return tipoDeEmbarque;
	}
	public String getNumeroNFe() {
		return numeroNFe;
	}
	public Date getDtInicioEmissaoNF() {
		return dtInicioEmissaoNF;
	}
	public Date getDtFimEmissaoNF() {
		return dtFimEmissaoNF;
	}
	public Material getMaterial() {
		return material;
	}
/////////////////////////////////////////////////////////////////////|
//INICIO DOS SET 											/////////|
/////////////////////////////////////////////////////////////////////|
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setTipoDocumentoDeclaracao(TipoDocumentoDeclaracao tipoDocumentoDeclaracao) {
		this.tipoDocumentoDeclaracao = tipoDocumentoDeclaracao;
	}
	public void setNumeroDeclaracao(String numeroDeclaracao) {
		this.numeroDeclaracao = numeroDeclaracao;
	}
	public void setDtInicioDeclaracao(Date dtInicioDeclaracao) {
		this.dtInicioDeclaracao = dtInicioDeclaracao;
	}
	public void setDtFimDeclaracao(Date dtFimDeclaracao) {
		this.dtFimDeclaracao = dtFimDeclaracao;
	}
	public void setTipoNatureza(TipoNaturezaDaDeclaracao tipoNatureza) {
		this.tipoNatureza = tipoNatureza;
	}
	public void setDtInicioDE(Date dtInicioDE) {
		this.dtInicioDE = dtInicioDE;
	}
	public void setDtFimDE(Date dtFimDE) {
		this.dtFimDE = dtFimDE;
	}
	public void setTipoDeEmbarque(TipoConhecimentoDeEmbarque tipoDeEmbarque) {
		this.tipoDeEmbarque = tipoDeEmbarque;
	}
	public void setNumeroNFe(String numeroNFe) {
		this.numeroNFe = numeroNFe;
	}
	public void setDtInicioEmissaoNF(Date dtInicioEmissaoNF) {
		this.dtInicioEmissaoNF = dtInicioEmissaoNF;
	}
	public void setDtFimEmissaoNF(Date dtFimEmissaoNF) {
		this.dtFimEmissaoNF = dtFimEmissaoNF;
	}
	
	
}

