package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fatorajuste;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContratoFiltro extends FiltroListagemSined {
	
	protected String contratos;
	protected String descricao;
	protected Boolean comnota;
	protected String identificador;
	
	protected Boolean tipopesquisacliente;
	protected Cliente clienterazaosocial;
	protected Cliente clientenome;
	
	protected Tipopessoa tipopessoa;
	protected Cnpj cnpj;
	protected Cpf cpf;
	protected Contratotipo contratotipo;
	protected Vendedortipo vendedortipo = null;
	protected Colaborador colaborador;
	protected Fornecedor fornecedor;
	protected Fatorajuste fatorajuste;
	protected Material material;
	protected Formafaturamento formafaturamento;
	protected String enderecoStr;
	protected String enderecoentregaStr;
	protected Fornecedor associacao;
	
	// INTERVALOS
	protected Date dtiniciode;
	protected Date dtinicioate;
	protected Date dtfimde;
	protected Date dtfimate;
	protected Date dtproximovencimentode;
	protected Date dtproximovencimentoate;
	protected Money valorde;
	protected Money valorate;
	protected Date dtassinaturade;
	protected Date dtassinaturaate;
	protected Date dtrenovacaode;
	protected Date dtrenovacaoate;
	protected Date dtsuspensaode;
	protected Date dtsuspensaoate;
	protected Date dtcancelamentode;
	protected Date dtcancelamentoate;
	
	// COMBOS
	protected Empresa empresa;
	protected Centrocusto centrocusto;
	protected Frequencia frequencia;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Materialgrupo materialgrupo;
	
	// CHECKLIST
	protected List<SituacaoContrato> listaSituacao;
	
	//TOTAIS
	protected Boolean exibirTotais = Boolean.FALSE;
	protected Money total;
	protected String whereIn;
	
	public ContratoFiltro() {
		List<SituacaoContrato> listaSituacao = new ArrayList<SituacaoContrato>();
		listaSituacao.add(SituacaoContrato.EM_ESPERA);
		listaSituacao.add(SituacaoContrato.NORMAL);
		listaSituacao.add(SituacaoContrato.A_CONSOLIDAR);
		listaSituacao.add(SituacaoContrato.ATENCAO);
		listaSituacao.add(SituacaoContrato.ATRASADO);
		listaSituacao.add(SituacaoContrato.SUSPENSO);
		listaSituacao.add(SituacaoContrato.ISENTO);
		
		this.listaSituacao = listaSituacao;
		this.tipopesquisacliente = Boolean.TRUE;
	}
	
	@DisplayName("Nome / Identificador")
	public Cliente getClientenome() {
		return clientenome;
	}
	
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public List<SituacaoContrato> getListaSituacao() {
		return listaSituacao;
	}
	
	public Date getDtiniciode() {
		return dtiniciode;
	}
	
	public Date getDtinicioate() {
		return dtinicioate;
	}
	
	public Date getDtfimde() {
		return dtfimde;
	}
	
	public Date getDtfimate() {
		return dtfimate;
	}
	
	@DisplayName("Total")
	public Money getTotal() {
		return total;
	}
	
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	public Date getDtproximovencimentode() {
		return dtproximovencimentode;
	}
	
	public Date getDtproximovencimentoate() {
		return dtproximovencimentoate;
	}
	
	public Money getValorde() {
		return valorde;
	}
	
	public Money getValorate() {
		return valorate;
	}
	
	@DisplayName("Periodicidade")
	public Frequencia getFrequencia() {
		return frequencia;
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	public Cpf getCpf() {
		return cpf;
	}
	
	public Cnpj getCnpj() {
		return cnpj;
	}
	
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}

	@DisplayName("Tipo")
	public Boolean getTipopesquisacliente() {
		return tipopesquisacliente;
	}
	
	@DisplayName("Raz�o social")
	public Cliente getClienterazaosocial() {
		return clienterazaosocial;
	}
	
	@DisplayName("Contratos")
	public String getContratos() {
		return contratos;
	}
	
	public Date getDtrenovacaode() {
		return dtrenovacaode;
	}
	
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Forma de faturamento")
	public Formafaturamento getFormafaturamento() {
		return formafaturamento;
	}
	
	@DisplayName("Identificadores")
	public String getIdentificador() {
		return identificador;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public void setFormafaturamento(Formafaturamento formafaturamento) {
		this.formafaturamento = formafaturamento;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setDtrenovacaode(Date dtrenovacaode) {
		this.dtrenovacaode = dtrenovacaode;
	}

	public Date getDtrenovacaoate() {
		return dtrenovacaoate;
	}
	
	@DisplayName("Associa��o")
	public Fornecedor getAssociacao() {
		return associacao;
	}
	
	public void setAssociacao(Fornecedor associacao) {
		this.associacao = associacao;
	}

	public void setDtrenovacaoate(Date dtrenovacaoate) {
		this.dtrenovacaoate = dtrenovacaoate;
	}

	public void setContratos(String contratos) {
		this.contratos = contratos;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setDtproximovencimentode(Date dtproximovencimentode) {
		this.dtproximovencimentode = dtproximovencimentode;
	}
	public void setDtproximovencimentoate(Date dtproximovencimentoate) {
		this.dtproximovencimentoate = dtproximovencimentoate;
	}
	public void setValorde(Money valorde) {
		this.valorde = valorde;
	}
	public void setValorate(Money valorate) {
		this.valorate = valorate;
	}
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	public void setDtiniciode(Date dtiniciode) {
		this.dtiniciode = dtiniciode;
	}
	public void setDtinicioate(Date dtinicioate) {
		this.dtinicioate = dtinicioate;
	}
	public void setDtfimde(Date dtfimde) {
		this.dtfimde = dtfimde;
	}
	public void setDtfimate(Date dtfimate) {
		this.dtfimate = dtfimate;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setListaSituacao(List<SituacaoContrato> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	public void setTipopesquisacliente(Boolean tipopesquisacliente) {
		this.tipopesquisacliente = tipopesquisacliente;
	}
	public void setClienterazaosocial(Cliente clienterazaosocial) {
		this.clienterazaosocial = clienterazaosocial;
	}
	public void setClientenome(Cliente clientenome) {
		this.clientenome = clientenome;
	}
	@DisplayName("Tipo de contrato")
	public Contratotipo getContratotipo() {
		return contratotipo;
	}
	
	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}
	@DisplayName("Tipo de vendedor")
	public Vendedortipo getVendedortipo() {
		return vendedortipo;
	}
	@DisplayName("Colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setVendedortipo(Vendedortipo vendedortipo) {
		this.vendedortipo = vendedortipo;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@DisplayName("Fator de ajuste")
	public Fatorajuste getFatorajuste() {
		return fatorajuste;
	}

	public void setFatorajuste(Fatorajuste fatorajuste) {
		this.fatorajuste = fatorajuste;
	}

	public Date getDtassinaturade() {
		return dtassinaturade;
	}
	public Date getDtassinaturaate() {
		return dtassinaturaate;
	}

	public void setDtassinaturade(Date dtassinaturade) {
		this.dtassinaturade = dtassinaturade;
	}
	public void setDtassinaturaate(Date dtassinaturaate) {
		this.dtassinaturaate = dtassinaturaate;
	}

	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public Date getDtsuspensaode() {
		return dtsuspensaode;
	}

	public Date getDtsuspensaoate() {
		return dtsuspensaoate;
	}

	public void setDtsuspensaode(Date dtsuspensaode) {
		this.dtsuspensaode = dtsuspensaode;
	}

	public void setDtsuspensaoate(Date dtsuspensaoate) {
		this.dtsuspensaoate = dtsuspensaoate;
	}
	
	@DisplayName("Endere�o de Faturamento")
	@MaxLength(100)
	public String getEnderecoStr() {
		return enderecoStr;
	}
	@DisplayName("Endere�o de Entrega")
	@MaxLength(100)
	public String getEnderecoentregaStr() {
		return enderecoentregaStr;
	}

	public void setEnderecoStr(String enderecoStr) {
		this.enderecoStr = enderecoStr;
	}
	public void setEnderecoentregaStr(String enderecoentregaStr) {
		this.enderecoentregaStr = enderecoentregaStr;
	}
	
	public Boolean getExibirTotais() {
		return exibirTotais;
	}
	
	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}

	@DisplayName("Grupo do Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}

	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}

	public Date getDtcancelamentode() {
		return dtcancelamentode;
	}

	public Date getDtcancelamentoate() {
		return dtcancelamentoate;
	}

	public void setDtcancelamentode(Date dtcancelamentode) {
		this.dtcancelamentode = dtcancelamentode;
	}

	public void setDtcancelamentoate(Date dtcancelamentoate) {
		this.dtcancelamentoate = dtcancelamentoate;
	}
}
