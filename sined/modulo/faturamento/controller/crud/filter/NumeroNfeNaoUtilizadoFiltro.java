package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.NumeroNfeNaoUtilizadoBean;

public class NumeroNfeNaoUtilizadoFiltro {

	protected Empresa empresa;
	protected Date dtinicioemissao;
	protected Date dtfimemissao;
	protected Configuracaonfe configuracaonfe; 
	protected String justificativa;
	protected List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado;
	protected Boolean inutilizacaoProcessadas;
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public Date getDtinicioemissao() {
		return dtinicioemissao;
	}
	public Date getDtfimemissao() {
		return dtfimemissao;
	}
	@Required
	@DisplayName("Configuração NF-e")
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}
	@Required
	@DisplayName("Justificativa")
	public String getJustificativa() {
		return justificativa;
	}
	
	@DisplayName("Exibir inutilizações processadas")
	public Boolean getInutilizacaoProcessadas() {
		return inutilizacaoProcessadas;
	}
	
	public List<NumeroNfeNaoUtilizadoBean> getListaNfeNaoUtilizado() {
		return listaNfeNaoUtilizado;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicioemissao(Date dtinicioemissao) {
		this.dtinicioemissao = dtinicioemissao;
	}
	public void setDtfimemissao(Date dtfimemissao) {
		this.dtfimemissao = dtfimemissao;
	}
	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}
	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
	public void setListaNfeNaoUtilizado(List<NumeroNfeNaoUtilizadoBean> listaNfeNaoUtilizado) {
		this.listaNfeNaoUtilizado = listaNfeNaoUtilizado;
	}
	public void setInutilizacaoProcessadas(Boolean inutilizacaoProcessadas) {
		this.inutilizacaoProcessadas = inutilizacaoProcessadas;
	}
}
