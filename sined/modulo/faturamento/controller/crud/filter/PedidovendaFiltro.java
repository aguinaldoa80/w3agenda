package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Pedidovendasituacaoecommerce;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.ContasFinanceiroSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ProducaoagendasituacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoPendencia;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PedidovendaFiltro extends FiltroListagemSined{

	private Integer cdpedidovenda;
	private Integer cdvendaorcamento;
	private String identificacaoexterna;
	private Empresa empresa;
	private Colaborador colaborador;
	private Cliente cliente;
	private String codigo;
	private String whereIn;
	private Material material;
	private Date dtinicio = SinedDateUtils.firstDateOfMonth();
	private Date dtfim;
	private List<Pedidovendasituacao> listaPedidoVendasituacao;
	private List<SituacaoPendencia> listaSituacaoPendencia;
	private List<ProducaoagendasituacaoEnum> listaProducaoagendasituacao;
	private List<Pedidovendasituacaoecommerce> listaPedidovendasituacaoecommerce;
	private Money totalgeral;
	protected Money totalgeralipi;
	private Money saldoprodutos;
	protected Prazopagamento prazopagamento;
	protected Documentotipo documentotipo;
	protected Projeto projeto;
	protected Pedidovendatipo pedidovendatipo;
	protected String enderecoStr;
	protected Regiao regiao;
	protected Date prazoentregainicio;
	protected Date prazoentregafim;
	protected String whereIncdpedidovenda;
	private Materialgrupo materialgrupo;
	protected Materialcategoria materialcategoria;
	private Categoria categoria;
	
	protected Boolean pedidoprocessocompra;
	protected Boolean visualizarListagemreservas;
	protected Boolean isRestricaoClienteVendedor;
	protected Boolean isRestricaoVendaVendedor;
	protected Double pesototal;
	protected Double pesobrutototal;
	protected Usuario usuario;
	
	protected Campoextrapedidovendatipo campoextrapedidovendatipo;
	protected String valorCampoExtra;
	protected Fornecedor fornecedorRepresentacao;
	protected String identificador;
	protected Boolean pedidoProcessoLiberacaoWMS;
	
	protected Boolean tipopedidoReserva;
	protected Boolean comprovanteEmitido;
	protected Boolean comprovanteEnviado;
	protected Boolean limitecreditoexcedido;
//	UTILIZADO PARA N�O EXIBIR NENHUM RESULTADO QUANDO O USUARIO N�O FOR COLABORADOR E TIVER RESTRI��O VENDA/VENDEDOR
	protected Boolean naoexibirresultado;
	protected TipoVendedorEnum tipoVendedor;
	protected Boolean origemOtr;
	protected ContasFinanceiroSituacao contaFinanceiroSituacao;
	
	public PedidovendaFiltro() {
		this.setListaPedidoVendasituacao(new ArrayList<Pedidovendasituacao>());
		this.getListaPedidoVendasituacao().add(Pedidovendasituacao.PREVISTA);		
		this.getListaPedidoVendasituacao().add(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE);
		this.getListaPedidoVendasituacao().add(Pedidovendasituacao.AGUARDANDO_APROVACAO);
	}
	
	@DisplayName("C�digo da Venda")
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	@MaxLength(300)
	public String getWhereIncdpedidovenda() {
		return whereIncdpedidovenda;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("C�digo")
	@MaxLength(10)
	public String getCodigo() {
		return codigo;
	}
	@DisplayName("Produto")
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Situa��o")
	public List<Pedidovendasituacao> getListaPedidoVendasituacao() {
		return listaPedidoVendasituacao;
	}
	@DisplayName("Total")
	public Money getTotalgeral() {
		return totalgeral;
	}
	@DisplayName("Ident. externa")
	@MaxLength(20)
	public String getIdentificacaoexterna() {
		return identificacaoexterna;
	}
	@DisplayName("Tipo de Pedido")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	@DisplayName("Situa��o da Produ��o")
	public List<ProducaoagendasituacaoEnum> getListaProducaoagendasituacao() {
		return listaProducaoagendasituacao;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setIdentificacaoexterna(String identificacaoexterna) {
		this.identificacaoexterna = identificacaoexterna;
	}
	public void setTotalgeral(Money totalgeral) {
		this.totalgeral = totalgeral;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	@DisplayName("Campo Adicional")
	public Campoextrapedidovendatipo getCampoextrapedidovendatipo() {
		return campoextrapedidovendatipo;
	}
	@DisplayName("Conte�do")
	public String getValorCampoExtra() {
		return valorCampoExtra;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setListaPedidoVendasituacao(List<Pedidovendasituacao> listaPedidoVendasituacao) {
		this.listaPedidoVendasituacao = listaPedidoVendasituacao;
	}
	public void setListaProducaoagendasituacao(List<ProducaoagendasituacaoEnum> listaProducaoagendasituacao) {
		this.listaProducaoagendasituacao = listaProducaoagendasituacao;
	}
	@DisplayName("Condi��o de Pagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	@DisplayName("Forma de pagamento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	
	public Boolean getComprovanteEmitido() {
		return comprovanteEmitido;
	}

	public void setComprovanteEmitido(Boolean comprovanteEmitido) {
		this.comprovanteEmitido = comprovanteEmitido;
	}

	public Boolean getComprovanteEnviado() {
		return comprovanteEnviado;
	}

	public void setComprovanteEnviado(Boolean comprovanteEnviado) {
		this.comprovanteEnviado = comprovanteEnviado;
	}

	public Boolean getNaoexibirresultado() {
		return naoexibirresultado;
	}
	public void setNaoexibirresultado(Boolean naoexibirresultado) {
		this.naoexibirresultado = naoexibirresultado;
	}

	@DisplayName("Saldo de Produtos")
	public Money getSaldoprodutos() {
		return saldoprodutos;
	}
	public void setSaldoprodutos(Money saldoprodutos) {
		this.saldoprodutos = saldoprodutos;
	}

	public Boolean getPedidoprocessocompra() {
		return pedidoprocessocompra;
	}
	public void setPedidoprocessocompra(Boolean pedidoprocessocompra) {
		this.pedidoprocessocompra = pedidoprocessocompra;
	}

	@DisplayName("C�digo do Or�amento")
	@MaxLength(value=9)
	public Integer getCdvendaorcamento() {
		return cdvendaorcamento;
	}
	public void setCdvendaorcamento(Integer cdvendaorcamento) {
		this.cdvendaorcamento = cdvendaorcamento;
	}

	public Boolean getVisualizarListagemreservas() {
		return visualizarListagemreservas;
	}
	public void setVisualizarListagemreservas(Boolean visualizarListagemreservas) {
		this.visualizarListagemreservas = visualizarListagemreservas;
	}

	public Boolean getIsRestricaoClienteVendedor() {
		return isRestricaoClienteVendedor;
	}
	public Boolean getIsRestricaoVendaVendedor() {
		return isRestricaoVendaVendedor;
	}
	public void setIsRestricaoClienteVendedor(Boolean isRestricaoClienteVendedor) {
		this.isRestricaoClienteVendedor = isRestricaoClienteVendedor;
	}
	public void setIsRestricaoVendaVendedor(Boolean isRestricaoVendaVendedor) {
		this.isRestricaoVendaVendedor = isRestricaoVendaVendedor;
	}
	
	@DisplayName("Local de Entrega")
	@MaxLength(100)
	public String getEnderecoStr() {
		return enderecoStr;
	}

	public void setEnderecoStr(String enderecoStr) {
		this.enderecoStr = enderecoStr;
	}
	
	@DisplayName("Peso L�quido Total")
	public Double getPesototal() {
		return pesototal;
	}
	@DisplayName("Peso Bruto Total")
	public Double getPesobrutototal() {
		return pesobrutototal;
	}

	public void setPesototal(Double pesototal) {
		this.pesototal = pesototal;
	}
	public void setPesobrutototal(Double pesobrutototal) {
		this.pesobrutototal = pesobrutototal;
	}
	
	@DisplayName("Regi�o")
	public Regiao getRegiao() {
		return regiao;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setCampoextrapedidovendatipo(Campoextrapedidovendatipo campoextrapedidovendatipo) {
		this.campoextrapedidovendatipo = campoextrapedidovendatipo;
	}
	public void setValorCampoExtra(String valorCampoExtra) {
		this.valorCampoExtra = valorCampoExtra;
	}
	
	public Date getPrazoentregainicio() {
		return prazoentregainicio;
	}
	public void setPrazoentregainicio(Date prazoentregainicio) {
		this.prazoentregainicio = prazoentregainicio;
	}
	
	public Date getPrazoentregafim() {
		return prazoentregafim;
	}
	public void setPrazoentregafim(Date prazoentregafim) {
		this.prazoentregafim = prazoentregafim;
	}

	@DisplayName("Fornecedor Representa��o")
	public Fornecedor getFornecedorRepresentacao() {
		return fornecedorRepresentacao;
	}
	public void setFornecedorRepresentacao(Fornecedor fornecedorRepresentacao) {
		this.fornecedorRepresentacao = fornecedorRepresentacao;
	}

	@DisplayName("Libera��o de pedidos")
	public Boolean getPedidoProcessoLiberacaoWMS() {
		return pedidoProcessoLiberacaoWMS;
	}
	public void setPedidoProcessoLiberacaoWMS(Boolean pedidoProcessoLiberacaoWMS) {
		this.pedidoProcessoLiberacaoWMS = pedidoProcessoLiberacaoWMS;
	}

	@DisplayName("Reserva")
	public Boolean getTipopedidoReserva() {
		return tipopedidoReserva;
	}

	public void setTipopedidoReserva(Boolean tipopedidoReserva) {
		this.tipopedidoReserva = tipopedidoReserva;
	}

	@DisplayName("Total  Geral com Impostos")
	public Money getTotalgeralipi() {
		return totalgeralipi;
	}

	public void setTotalgeralipi(Money totalgeralipi) {
		this.totalgeralipi = totalgeralipi;
	}

	public void setWhereIncdpedidovenda(String whereIncdpedidovenda) {
		this.whereIncdpedidovenda = whereIncdpedidovenda;
	}
	
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}

	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	@DisplayName("Categoria de material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	
	@DisplayName("Categoria de cliente")
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@DisplayName("Limite de cr�dito excedido (cliente)")
	public Boolean getLimitecreditoexcedido() {
		return limitecreditoexcedido;
	}

	public void setLimitecreditoexcedido(Boolean limitecreditoexcedido) {
		this.limitecreditoexcedido = limitecreditoexcedido;
	}
	
	@DisplayName("Tipo do vendedor")
	public TipoVendedorEnum getTipoVendedor() {
		return tipoVendedor;
	}
	
	public void setTipoVendedor(TipoVendedorEnum tipoVendedor) {
		this.tipoVendedor = tipoVendedor;
	}
	@DisplayName("PEDIDO DE VENDA COM PEND�NCIAS")
	public List<SituacaoPendencia> getListaSituacaoPendencia() {
		return listaSituacaoPendencia;
	}
	
	public void setListaSituacaoPendencia(
			List<SituacaoPendencia> listaSituacaoPendencia) {
		this.listaSituacaoPendencia = listaSituacaoPendencia;
	}
	
	public Boolean getOrigemOtr() {
		return origemOtr;
	}
	public void setOrigemOtr(Boolean origemOtr) {
		this.origemOtr = origemOtr;
	}

	@DisplayName("CONTAS / FINANCEIRO")
	public ContasFinanceiroSituacao getContaFinanceiroSituacao() {
		return contaFinanceiroSituacao;
	}

	public void setContaFinanceiroSituacao(
			ContasFinanceiroSituacao contaFinanceiroSituacao) {
		this.contaFinanceiroSituacao = contaFinanceiroSituacao;
	}

	@DisplayName("Situa��o no E-commerce")
	public List<Pedidovendasituacaoecommerce> getListaPedidovendasituacaoecommerce() {
		return listaPedidovendasituacaoecommerce;
	}

	public void setListaPedidovendasituacaoecommerce(
			List<Pedidovendasituacaoecommerce> listaPedidovendasituacaoecommerce) {
		this.listaPedidovendasituacaoecommerce = listaPedidovendasituacaoecommerce;
	}
	
	
	
}
