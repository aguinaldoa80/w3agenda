package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoNotafiscalservicoReport;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.NotafiscalservicomostrarFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class NotaFiscalServicoFiltro extends FiltroListagemSined {
	
	protected Long numNotaDe;
	protected Long numNotaAte;
	protected List<NotaStatus> listaNotaStatus;
	protected Date dtEmissaoInicio;
	protected Date dtEmissaoFim;
	protected Date dtEmissaoInicioNFSe;
	protected Date dtEmissaoFimNFSe;
	protected Date dtCancelamentoInicio;
	protected Date dtCancelamentoFim;
	protected Date dtPagamentoInicio;
	protected Date dtPagamentoFim;
	protected Projeto projeto;
	protected Cliente cliente;
	protected Colaborador colaborador;
	protected String whereIn;
	protected Empresa empresa;
	protected Boolean mostraComNumero;
	protected Boolean configuracao;
	protected Centrocusto centrocusto;
	protected Long numeroNfseDe;
	protected Long numeroNfseAte;
	protected OrdenacaoNotafiscalservicoReport ordenacaoNotafiscalservicoReport;
	protected NotafiscalservicomostrarFiltro notafiscalservicomostrarFiltro;
	
	protected Boolean incideiss;
	protected Boolean incideir;
	protected Boolean incideinss;
	protected Boolean incidepis;
	protected Boolean incidecofins;
	protected Boolean incidecsll;
	protected Boolean incideicms;

	protected TipoVendedorEnum tipoVendedor;
	protected Money total;
	protected Date dtEmissaoNfeInicio;
	protected Date dtEmissaoNfeFim;
	
	{
		this.listaNotaStatus = NotaStatus.getTodosEstados();
		this.listaNotaStatus.remove(NotaStatus.CANCELADA);
		this.listaNotaStatus.remove(NotaStatus.EM_ESPERA);
		
		Date datainicio = SinedDateUtils.firstDateOfMonth();
		datainicio = SinedDateUtils.addMesData(datainicio, -1);
		this.dtEmissaoInicio = datainicio;
	}
	
	@MaxLength(30)
	public Long getNumNotaDe() {
		return numNotaDe;
	}
	
	@MaxLength(30)
	public Long getNumNotaAte() {
		return numNotaAte;
	}
	
	@DisplayName("Situação")
	public List<NotaStatus> getListaNotaStatus() {
		return listaNotaStatus;
	}
	
	@DisplayName("")
	public Date getDtEmissaoInicio() {
		return dtEmissaoInicio;
	}

	@DisplayName("")
	public Date getDtEmissaoFim() {
		return dtEmissaoFim;
	}
	
	@DisplayName("")
	public Date getDtPagamentoInicio() {
		return dtPagamentoInicio;
	}
	
	@DisplayName("")
	public Date getDtPagamentoFim() {
		return dtPagamentoFim;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Destinatário / Sacado")
	public Cliente getCliente() {
		return cliente;
	}

	public String getWhereIn() {
		return whereIn;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Boolean getMostraComNumero() {
		return mostraComNumero;
	}
	
	@DisplayName("Campo do relatório a ser ordenado")
	public OrdenacaoNotafiscalservicoReport getOrdenacaoNotafiscalservicoReport() {
		return ordenacaoNotafiscalservicoReport;
	}
	
	public void setOrdenacaoNotafiscalservicoReport(
			OrdenacaoNotafiscalservicoReport ordenacaoNotafiscalservicoReport) {
		this.ordenacaoNotafiscalservicoReport = ordenacaoNotafiscalservicoReport;
	}

	public Date getDtEmissaoNfeInicio() {
		return dtEmissaoNfeInicio;
	}

	public Date getDtEmissaoNfeFim() {
		return dtEmissaoNfeFim;
	}

	public void setDtEmissaoNfeInicio(Date dtEmissaoNfeInicio) {
		this.dtEmissaoNfeInicio = dtEmissaoNfeInicio;
	}

	public void setDtEmissaoNfeFim(Date dtEmissaoNfeFim) {
		this.dtEmissaoNfeFim = dtEmissaoNfeFim;
	}

	public void setMostraComNumero(Boolean mostraComNumero) {
		this.mostraComNumero = mostraComNumero;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setNumNotaDe(Long numNotaDe) {
		this.numNotaDe = numNotaDe;
	}
	
	public void setNumNotaAte(Long numNotaAte) {
		this.numNotaAte = numNotaAte;
	}
	
	public void setListaNotaStatus(List<NotaStatus> listaNotaStatus) {
		this.listaNotaStatus = listaNotaStatus;
	}
	
	public void setDtEmissaoInicio(Date dtEmissaoInicio) {
		this.dtEmissaoInicio = dtEmissaoInicio;
	}
	
	public void setDtEmissaoFim(Date dtEmissaoFim) {
		this.dtEmissaoFim = dtEmissaoFim;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	@DisplayName("Total notas")
	public Money getTotal() {
		return total;
	}
	
	public void setTotal(Money total) {
		this.total = total;
	}

	public Boolean getConfiguracao() {
		return configuracao;
	}

	public void setConfiguracao(Boolean configuracao) {
		this.configuracao = configuracao;
	}
	
	public void setDtPagamentoInicio(Date dtPagamentoInicio) {
		this.dtPagamentoInicio = dtPagamentoInicio;
	}
	
	public void setDtPagamentoFim(Date dtPagamentoFim) {
		this.dtPagamentoFim = dtPagamentoFim;
	}
	
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public Long getNumeroNfseDe() {
		return numeroNfseDe;
	}

	public Long getNumeroNfseAte() {
		return numeroNfseAte;
	}

	public void setNumeroNfseDe(Long numeroNfseDe) {
		this.numeroNfseDe = numeroNfseDe;
	}

	public void setNumeroNfseAte(Long numeroNfseAte) {
		this.numeroNfseAte = numeroNfseAte;
	}
	
	@DisplayName("Incide ISS")
	public Boolean getIncideiss() {
		return incideiss;
	}
	@DisplayName("Incide IR")
	public Boolean getIncideir() {
		return incideir;
	}
	@DisplayName("Incide INSS")
	public Boolean getIncideinss() {
		return incideinss;
	}
	@DisplayName("Incide PIS")
	public Boolean getIncidepis() {
		return incidepis;
	}
	@DisplayName("Incide COFINS")
	public Boolean getIncidecofins() {
		return incidecofins;
	}
	@DisplayName("Incide CSLL")
	public Boolean getIncidecsll() {
		return incidecsll;
	}
	@DisplayName("Incide ICMS")
	public Boolean getIncideicms() {
		return incideicms;
	}

	public TipoVendedorEnum getTipoVendedor() {
		return tipoVendedor;
	}

	public void setTipoVendedor(TipoVendedorEnum tipoVendedor) {
		this.tipoVendedor = tipoVendedor;
	}

	public Date getDtCancelamentoInicio() {
		return dtCancelamentoInicio;
	}

	public Date getDtCancelamentoFim() {
		return dtCancelamentoFim;
	}

	public Date getDtEmissaoInicioNFSe() {
		return dtEmissaoInicioNFSe;
	}

	public Date getDtEmissaoFimNFSe() {
		return dtEmissaoFimNFSe;
	}

	public void setDtEmissaoInicioNFSe(Date dtEmissaoInicioNFSe) {
		this.dtEmissaoInicioNFSe = dtEmissaoInicioNFSe;
	}

	public void setDtEmissaoFimNFSe(Date dtEmissaoFimNFSe) {
		this.dtEmissaoFimNFSe = dtEmissaoFimNFSe;
	}

	public void setIncideiss(Boolean incideiss) {
		this.incideiss = incideiss;
	}
	public void setIncideir(Boolean incideir) {
		this.incideir = incideir;
	}
	public void setIncideinss(Boolean incideinss) {
		this.incideinss = incideinss;
	}
	public void setIncidepis(Boolean incidepis) {
		this.incidepis = incidepis;
	}
	public void setIncidecofins(Boolean incidecofins) {
		this.incidecofins = incidecofins;
	}
	public void setIncidecsll(Boolean incidecsll) {
		this.incidecsll = incidecsll;
	}
	public void setIncideicms(Boolean incideicms) {
		this.incideicms = incideicms;
	}

	public void setDtCancelamentoInicio(Date dtCancelamentoInicio) {
		this.dtCancelamentoInicio = dtCancelamentoInicio;
	}

	public void setDtCancelamentoFim(Date dtCancelamentoFim) {
		this.dtCancelamentoFim = dtCancelamentoFim;
	}

	public NotafiscalservicomostrarFiltro getNotafiscalservicomostrarFiltro() {
		return notafiscalservicomostrarFiltro;
	}
	
	public void setNotafiscalservicomostrarFiltro(
			NotafiscalservicomostrarFiltro notafiscalservicomostrarFiltro) {
		this.notafiscalservicomostrarFiltro = notafiscalservicomostrarFiltro;
	}
}
