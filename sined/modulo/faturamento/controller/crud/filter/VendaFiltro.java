package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Pedidovendasituacaoecommerce;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacaoentrega;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporelatoriovenda;
import br.com.linkcom.sined.geral.bean.enumeration.VendaSituacaoBoletoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Vendasituacaoentrega;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ResumoDocumentotipoVenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendaSomatorioDiaBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VendaFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private Colaborador colaborador;
	private Cliente cliente;
	private Cliente clienteindicacao;
	private String codigo;
	private String identificacaoexterna;
	private String identificador;
	private Material material;
	private Date dtinicio = SinedDateUtils.firstDateOfMonth();
	private Date dtfim;
	private Integer cdvenda;
	private Integer cdvendaorcamento;
	private Integer cdpedidovenda;
	private String whereIn;
	protected List<Vendasituacao> listaVendasituacao;
	private List<Pedidovendasituacaoecommerce> listaPedidovendasituacaoecommerce;
	private List<Expedicaosituacaoentrega> listaExpedicaosituacaoentrega;
	private List<Vendasituacaoentrega> listaVendasituacaoentrega;
	protected Money totalgeral;
	protected Money totalgeralipi;
	protected Double qtdegeral;
	protected Double pesototal;
	protected Double pesobrutototal;
	protected Double valorvalecompra;
	protected Money saldoprodutos;
	protected Money saldoprodutosbonificacao;
	protected Integer identificadorcarregamento;	
	protected Regiao regiao;
	protected Prazopagamento prazopagamento;
	protected Documentotipo documentotipo;	
	protected Projeto projeto;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Materialcategoria materialcategoria;
	protected Localarmazenagem localarmazenagem;
	protected Departamento departamento;
	protected Tiporelatoriovenda tiporelatoriovenda;
	protected String enderecoStr;
	private Categoria categoria;
	protected Pedidovendatipo pedidovendatipo;
	protected Boolean exibirTotais = false;
	protected Boolean exibirResumoFormapagamento = false;
	protected Boolean exibirResumoPedidovendatipo = false;
	protected Emporiumpdv emporiumpdv;
	protected List<VendaSomatorioDiaBean> listaSomatorioDia;
	protected String whereIncdvenda;
	protected Expedicaosituacao expedicaosituacao;
	
	protected Campoextrapedidovendatipo campoextrapedidovendatipo;
	protected String valorCampoExtra;
	protected Fornecedor fornecedorRepresentacao;
	
//	UTILIZADO PARA N�O EXIBIR NENHUM RESULTADO QUANDO O USUARIO N�O FOR COLABORADOR E TIVER RESTRI��O VENDA/VENDEDOR
	protected Boolean naoexibirresultado;
	
	protected Boolean isRestricaoClienteVendedor;
	protected Boolean isRestricaoVendaVendedor;
	protected Money valortotaldevolvido;
	
	protected List<ResumoDocumentotipoVenda> listaResumoDocumentotipoVenda;
	protected List<GenericBean> listaResumoPedidovendatipo;
	protected VendaSituacaoBoletoEnum vendaSituacaoBoleto;
	
	protected Date prazoentregainicio;
	protected Date prazoentregafim;

	protected Boolean comprovanteEmitido;
	protected Boolean comprovanteEnviado;
	protected Boolean limitecreditoexcedido;
	
	protected TipoVendedorEnum tipoVendedor;
	protected Boolean origemOtr;
	protected Boolean vendaPendenciaNaoNegociada;
	protected Boolean sinalizadoSemReserva;
	
	public VendaFiltro() {
		totalgeral = new Money();
		qtdegeral = 0d;
	}
	
	public VendaFiltro(Money totalgeral, Double qtdegeral) {
		this.totalgeral = totalgeral;
		this.qtdegeral = qtdegeral;
	}
	
	public VendaFiltro(Money totalgeral, Double qtdegeral, Money saldoprodutos) {
		this.totalgeral = totalgeral;
		this.qtdegeral = qtdegeral;
		this.saldoprodutos = saldoprodutos;
		
	}

	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Indica��o")
	public Cliente getClienteindicacao() {
		return clienteindicacao;
	}
	@DisplayName("C�digo")
	@MaxLength(10)
	public String getCodigo() {
		return codigo;
	}
	@DisplayName("Produto")
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Situa��o")
	public List<Vendasituacao> getListaVendasituacao() {
		return listaVendasituacao;
	}
	@DisplayName("Total")
	public Money getTotalgeral() {
		return totalgeral;
	}
	
	@DisplayName("Identifica��o do carregamento")
	public Integer getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}
	
	@DisplayName("Quantidade")
	public Double getQtdegeral() {
		return qtdegeral;
	}
	
	@DisplayName("Ident. externa")
	@MaxLength(20)
	public String getIdentificacaoexterna() {
		return identificacaoexterna;
	}
	@DisplayName("Identificador da venda")
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("Tipo de Venda")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}

	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public void setIdentificacaoexterna(String identificacaoexterna) {
		this.identificacaoexterna = identificacaoexterna;
	}
	
	public List<ResumoDocumentotipoVenda> getListaResumoDocumentotipoVenda() {
		return listaResumoDocumentotipoVenda;
	}
	
	public List<GenericBean> getListaResumoPedidovendatipo() {
		return listaResumoPedidovendatipo;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}

	@DisplayName("Situa��es Entrega")	
	public List<Expedicaosituacaoentrega> getListaExpedicaosituacaoentrega() {
		return listaExpedicaosituacaoentrega;
	}
	
	@DisplayName("Situa��es Entrega")	
	public List<Vendasituacaoentrega> getListaVendasituacaoentrega() {
		return listaVendasituacaoentrega;
	}

	@DisplayName("PDV")
	public Emporiumpdv getEmporiumpdv() {
		return emporiumpdv;
	}
	
	public void setEmporiumpdv(Emporiumpdv emporiumpdv) {
		this.emporiumpdv = emporiumpdv;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public void setListaResumoDocumentotipoVenda(
			List<ResumoDocumentotipoVenda> listaResumoDocumentotipoVenda) {
		this.listaResumoDocumentotipoVenda = listaResumoDocumentotipoVenda;
	}
	
	public void setListaResumoPedidovendatipo(List<GenericBean> listaResumoPedidovendatipo) {
		this.listaResumoPedidovendatipo = listaResumoPedidovendatipo;
	}
	
	public void setQtdegeral(Double qtdegeral) {
		this.qtdegeral = qtdegeral;
	}
	
	public void setIdentificadorcarregamento(Integer identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}

	public void setTotalgeral(Money totalgeral) {
		this.totalgeral = totalgeral;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setClienteindicacao(Cliente clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	@MaxLength(8)
	public Integer getCdvenda() {
		return cdvenda;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setListaVendasituacao(List<Vendasituacao> listaVendasituacao) {
		this.listaVendasituacao = listaVendasituacao;
	}
	
	@DisplayName("Regi�o")
	public Regiao getRegiao() {
		return regiao;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	@DisplayName("Condi��o de Pagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	@DisplayName("Forma de pagamento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	@DisplayName("Grupo do material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo do material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Categoria de material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	@DisplayName("Local de armazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Exibir Totais")
	public Boolean getExibirTotais() {
		return exibirTotais;
	}
	@DisplayName("Exibir Resumo de Forma de Pagamento")
	public Boolean getExibirResumoFormapagamento() {
		return exibirResumoFormapagamento;
	}
	@DisplayName("Exibir Resumo de Tipo de Pedido de Venda")
	public Boolean getExibirResumoPedidovendatipo() {
		return exibirResumoPedidovendatipo;
	}
	@DisplayName("Campo Adicional")
	public Campoextrapedidovendatipo getCampoextrapedidovendatipo() {
		return campoextrapedidovendatipo;
	}
	@DisplayName("Conte�do")
	public String getValorCampoExtra() {
		return valorCampoExtra;
	}
	public void setExibirResumoFormapagamento(Boolean exibirResumoFormapagamento) {
		this.exibirResumoFormapagamento = exibirResumoFormapagamento;
	}
	public void setExibirResumoPedidovendatipo(
			Boolean exibirResumoPedidovendatipo) {
		this.exibirResumoPedidovendatipo = exibirResumoPedidovendatipo;
	}
	public void setCampoextrapedidovendatipo(Campoextrapedidovendatipo campoextrapedidovendatipo) {
		this.campoextrapedidovendatipo = campoextrapedidovendatipo;
	}
	public void setValorCampoExtra(String valorCampoExtra) {
		this.valorCampoExtra = valorCampoExtra;
	}
	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public Boolean getNaoexibirresultado() {
		return naoexibirresultado;
	}
	public void setNaoexibirresultado(Boolean naoexibirresultado) {
		this.naoexibirresultado = naoexibirresultado;
	}

	@DisplayName("Saldo de Produtos")
	public Money getSaldoprodutos() {
		return saldoprodutos;
	}
	public void setSaldoprodutos(Money saldoprodutos) {
		this.saldoprodutos = saldoprodutos;
	}
	
	@DisplayName("C�digo do Or�amento")
	@MaxLength(value=9)
	public Integer getCdvendaorcamento() {
		return cdvendaorcamento;
	}
	@DisplayName("C�digo do Pedido de Venda")
	@MaxLength(value=9)
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}

	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setCdvendaorcamento(Integer cdvendaorcamento) {
		this.cdvendaorcamento = cdvendaorcamento;
	}

	@DisplayName("Saldo de Produtos (Bonifica��o)")
	public Money getSaldoprodutosbonificacao() {
		return saldoprodutosbonificacao;
	}
	public void setSaldoprodutosbonificacao(Money saldoprodutosbonificacao) {
		this.saldoprodutosbonificacao = saldoprodutosbonificacao;
	}

	public Boolean getIsRestricaoClienteVendedor() {
		return isRestricaoClienteVendedor;
	}
	public Boolean getIsRestricaoVendaVendedor() {
		return isRestricaoVendaVendedor;
	}
	public void setIsRestricaoClienteVendedor(Boolean isRestricaoClienteVendedor) {
		this.isRestricaoClienteVendedor = isRestricaoClienteVendedor;
	}
	public void setIsRestricaoVendaVendedor(Boolean isRestricaoVendaVendedor) {
		this.isRestricaoVendaVendedor = isRestricaoVendaVendedor;
	}
	
	@DisplayName("Tipo do Relat�rio")
	public Tiporelatoriovenda getTiporelatoriovenda() {
		return tiporelatoriovenda;
	}
	public void setTiporelatoriovenda(Tiporelatoriovenda tiporelatoriovenda) {
		this.tiporelatoriovenda = tiporelatoriovenda;
	}

	@DisplayName("Valor Total Devolvido")
	public Money getValortotaldevolvido() {
		return valortotaldevolvido;
	}
	public void setValortotaldevolvido(Money valortotaldevolvido) {
		this.valortotaldevolvido = valortotaldevolvido;
	}
	
	@DisplayName("Total Geral")
	public Money getTotalgeralWithDevolucao() {
		Money total = new Money();
		if(this.getTotalgeral() != null) total = this.getTotalgeral();
		if(this.getValortotaldevolvido() != null) total = total.subtract(this.getValortotaldevolvido());
		
		return total;
	}
	
	@DisplayName("Local de Entrega")
	@MaxLength(100)
	public String getEnderecoStr() {
		return enderecoStr;
	}

	public void setEnderecoStr(String enderecoStr) {
		this.enderecoStr = enderecoStr;
	}

	@DisplayName("Peso L�quido Total")
	public Double getPesototal() {
		return pesototal;
	}
	
	@DisplayName("Peso Bruto Total")
	public Double getPesobrutototal() {
		return pesobrutototal;
	}	
	
	public void setPesototal(Double pesototal) {
		this.pesototal = pesototal;
	}
	
	@DisplayName("Total de Vale Compra")
	public Double getValorvalecompra() {
		return valorvalecompra;
	}

	public void setValorvalecompra(Double valorvalecompra) {
		this.valorvalecompra = valorvalecompra;
	}

	public void setPesobrutototal(Double pesobrutototal) {
		this.pesobrutototal = pesobrutototal;
	}
	
	@DisplayName("Categoria de Cliente")
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	public List<VendaSomatorioDiaBean> getListaSomatorioDia() {
		return listaSomatorioDia;
	}
	
	public void setListaSomatorioDia(
			List<VendaSomatorioDiaBean> listaSomatorioDia) {
		this.listaSomatorioDia = listaSomatorioDia;
	}

	public VendaSituacaoBoletoEnum getVendaSituacaoBoleto() {
		return vendaSituacaoBoleto;
	}

	public void setVendaSituacaoBoleto(VendaSituacaoBoletoEnum vendaSituacaoBoleto) {
		this.vendaSituacaoBoleto = vendaSituacaoBoleto;
	}

	@DisplayName("Fornecedor Representa��o")
	public Fornecedor getFornecedorRepresentacao() {
		return fornecedorRepresentacao;
	}
	
	public void setFornecedorRepresentacao(Fornecedor fornecedorRepresentacao) {
		this.fornecedorRepresentacao = fornecedorRepresentacao;
	}

	@DisplayName("Total Geral com Impostos")
	public Money getTotalgeralipi() {
		return totalgeralipi;
	}

	public void setTotalgeralipi(Money totalgeralipi) {
		this.totalgeralipi = totalgeralipi;
	}

	public Date getPrazoentregainicio() {
		return prazoentregainicio;
	}

	public Date getPrazoentregafim() {
		return prazoentregafim;
	}

	public void setPrazoentregainicio(Date prazoentregainicio) {
		this.prazoentregainicio = prazoentregainicio;
	}

	public void setPrazoentregafim(Date prazoentregafim) {
		this.prazoentregafim = prazoentregafim;
	}

	@MaxLength(300)
	public String getWhereIncdvenda() {
		return whereIncdvenda;
	}

	public void setWhereIncdvenda(String whereIncdvenda) {
		this.whereIncdvenda = whereIncdvenda;
	}
	
	@DisplayName("Situa��o da Expedi��o")
	public Expedicaosituacao getExpedicaosituacao() {
		return expedicaosituacao;
	}

	public void setExpedicaosituacao(Expedicaosituacao expedicaosituacao) {
		this.expedicaosituacao = expedicaosituacao;
	}

	public Boolean getComprovanteEmitido() {
		return comprovanteEmitido;
	}

	public void setComprovanteEmitido(Boolean comprovanteEmitido) {
		this.comprovanteEmitido = comprovanteEmitido;
	}

	public Boolean getComprovanteEnviado() {
		return comprovanteEnviado;
	}

	public void setComprovanteEnviado(Boolean comprovanteEnviado) {
		this.comprovanteEnviado = comprovanteEnviado;
	}

	@DisplayName("Limite de cr�dito excedido (cliente)")
	public Boolean getLimitecreditoexcedido() {
		return limitecreditoexcedido;
	}

	public void setLimitecreditoexcedido(Boolean limitecreditoexcedido) {
		this.limitecreditoexcedido = limitecreditoexcedido;
	}
	
	@DisplayName("Tipo do vendedor")
	public TipoVendedorEnum getTipoVendedor() {
		return tipoVendedor;
	}
	
	public void setTipoVendedor(TipoVendedorEnum tipoVendedor) {
		this.tipoVendedor = tipoVendedor;
	}
	public Boolean getOrigemOtr() {
		return origemOtr;
	}
	public void setOrigemOtr(Boolean origemOtr) {
		this.origemOtr = origemOtr;
	}
	@DisplayName("Venda pend�ncia n�o negociada")
	public Boolean getVendaPendenciaNaoNegociada() {
		return vendaPendenciaNaoNegociada;
	}
	public void setVendaPendenciaNaoNegociada(Boolean vendaPendenciaNaoNegociada) {
		this.vendaPendenciaNaoNegociada = vendaPendenciaNaoNegociada;
	}
	public Boolean getSinalizadoSemReserva() {
		return sinalizadoSemReserva;
	}
	public void setSinalizadoSemReserva(Boolean sinalizadoSemReserva) {
		this.sinalizadoSemReserva = sinalizadoSemReserva;
	}
	public void setListaExpedicaosituacaoentrega(
			List<Expedicaosituacaoentrega> listaExpedicaosituacaoentrega) {
		this.listaExpedicaosituacaoentrega = listaExpedicaosituacaoentrega;
	}
	
	public void setListaVendasituacaoentrega(
			List<Vendasituacaoentrega> listaVendasituacaoentrega) {
		this.listaVendasituacaoentrega = listaVendasituacaoentrega;
	}
	
	@DisplayName("Situa��o no E-commerce")
	public List<Pedidovendasituacaoecommerce> getListaPedidovendasituacaoecommerce() {
		return listaPedidovendasituacaoecommerce;
	}

	public void setListaPedidovendasituacaoecommerce(
			List<Pedidovendasituacaoecommerce> listaPedidovendasituacaoecommerce) {
		this.listaPedidovendasituacaoecommerce = listaPedidovendasituacaoecommerce;
	}
}