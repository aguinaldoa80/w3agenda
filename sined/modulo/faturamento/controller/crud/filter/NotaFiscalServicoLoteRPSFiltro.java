package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class NotaFiscalServicoLoteRPSFiltro extends FiltroListagemSined {
	
	protected Long numNotaDe;
	protected Long numNotaAte;
	protected Date dataCriacaoDe;
	protected Date dataCriacaoAte;
	
	public Long getNumNotaDe() {
		return numNotaDe;
	}
	public Long getNumNotaAte() {
		return numNotaAte;
	}
	public Date getDataCriacaoDe() {
		return dataCriacaoDe;
	}
	public Date getDataCriacaoAte() {
		return dataCriacaoAte;
	}
	public void setDataCriacaoDe(Date dataCriacaoDe) {
		this.dataCriacaoDe = dataCriacaoDe;
	}
	public void setDataCriacaoAte(Date dataCriacaoAte) {
		this.dataCriacaoAte = dataCriacaoAte;
	}
	public void setNumNotaDe(Long numNotaDe) {
		this.numNotaDe = numNotaDe;
	}
	public void setNumNotaAte(Long numNotaAte) {
		this.numNotaAte = numNotaAte;
	}
	
}
