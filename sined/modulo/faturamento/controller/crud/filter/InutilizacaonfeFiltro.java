package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class InutilizacaonfeFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscalEnum() {
		return modeloDocumentoFiscalEnum;
	}
	
	public void setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum) {
		this.modeloDocumentoFiscalEnum = modeloDocumentoFiscalEnum;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
