package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class NotafiscalprodutoFiltro extends FiltroListagemSined {
	
	protected Long numNotaDe;
	protected Long numNotaAte;
	protected List<NotaStatus> listaNotaStatus;
	protected Date dtEmissaoInicio;
	protected Date dtEmissaoFim;
	protected Date dtPagamentoInicio;
	protected Date dtPagamentoFim;
	protected Projeto projeto;
	protected Cliente cliente;
	protected Colaborador colaborador;
	protected String whereIn;
	protected Empresa empresa;
	protected Boolean mostraComNumero;
	protected Boolean configuracao;
	protected Money valor;
	protected Naturezaoperacao naturezaoperacao;
	protected String enderecostr;
	protected Material material;
	protected Cfop cfop;
	protected Materialgrupo materialgrupo;
	protected Tipooperacaonota tipooperacaonota; 
	protected Boolean registroreceita;
	protected Boolean exibirTotais = Boolean.FALSE;

	protected TipoVendedorEnum tipoVendedor;
	protected Money totalnotas;
	protected SituacaoVinculoExpedicao situacaoExpedicao;
	protected ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum;
	protected Operacaonfe operacaonfe;
	protected Localdestinonfe localdestinonfe;
	protected Boolean gnregerada;
	protected Uf uf;
	

	{
		this.listaNotaStatus = NotaStatus.getTodosEstadosProduto();
		this.listaNotaStatus.remove(NotaStatus.CANCELADA);
		this.listaNotaStatus.remove(NotaStatus.EM_ESPERA);
		
		Date datainicio = SinedDateUtils.firstDateOfMonth();
		datainicio = SinedDateUtils.addMesData(datainicio, -1);
		this.dtEmissaoInicio = datainicio;
	}
	
	@MaxLength(30)
	public Long getNumNotaDe() {
		return numNotaDe;
	}
	@MaxLength(30)
	public Long getNumNotaAte() {
		return numNotaAte;
	}
	
	@DisplayName("Situa��o")
	public List<NotaStatus> getListaNotaStatus() {
		return listaNotaStatus;
	}
	
	@DisplayName("")
	public Date getDtEmissaoInicio() {
		return dtEmissaoInicio;
	}

	@DisplayName("")
	public Date getDtEmissaoFim() {
		return dtEmissaoFim;
	}
	
	@DisplayName("")
	public Date getDtPagamentoInicio() {
		return dtPagamentoInicio;
	}
	
	@DisplayName("")
	public Date getDtPagamentoFim() {
		return dtPagamentoFim;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Destinat�rio / Sacado")
	public Cliente getCliente() {
		return cliente;
	}

	public String getWhereIn() {
		return whereIn;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Boolean getMostraComNumero() {
		return mostraComNumero;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Reg. na receita")
	public Boolean getRegistroreceita() {
		return registroreceita;
	}
	@DisplayName("Situa��o da Expedi��o")
	public SituacaoVinculoExpedicao getSituacaoExpedicao() {
		return situacaoExpedicao;
	}
	
	@DisplayName("Consumidor final?")
	public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}
	
	@DisplayName("Local de destino")
	public Localdestinonfe getLocaldestinonfe() {
		return localdestinonfe;
	}
	
	@DisplayName("GNRE gerada?")
	public Boolean getGnregerada() {
		return gnregerada;
	}
	
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	
	public void setOperacaonfe(Operacaonfe operacaonfe) {
		this.operacaonfe = operacaonfe;
	}
	
	public void setLocaldestinonfe(Localdestinonfe localdestinonfe) {
		this.localdestinonfe = localdestinonfe;
	}
	
	public void setGnregerada(Boolean gnregerada) {
		this.gnregerada = gnregerada;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public void setSituacaoExpedicao(SituacaoVinculoExpedicao situacaoExpedicao) {
		this.situacaoExpedicao = situacaoExpedicao;
	}
	
	public void setRegistroreceita(Boolean registroreceita) {
		this.registroreceita = registroreceita;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setMostraComNumero(Boolean mostraComNumero) {
		this.mostraComNumero = mostraComNumero;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setNumNotaDe(Long numNotaDe) {
		this.numNotaDe = numNotaDe;
	}
	
	public void setNumNotaAte(Long numNotaAte) {
		this.numNotaAte = numNotaAte;
	}
	
	public void setListaNotaStatus(List<NotaStatus> listaNotaStatus) {
		this.listaNotaStatus = listaNotaStatus;
	}
	
	public void setDtEmissaoInicio(Date dtEmissaoInicio) {
		this.dtEmissaoInicio = dtEmissaoInicio;
	}
	
	public void setDtEmissaoFim(Date dtEmissaoFim) {
		this.dtEmissaoFim = dtEmissaoFim;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public Boolean getConfiguracao() {
		return configuracao;
	}

	public void setConfiguracao(Boolean configuracao) {
		this.configuracao = configuracao;
	}

	public void setDtPagamentoInicio(Date dtPagamentoInicio) {
		this.dtPagamentoInicio = dtPagamentoInicio;
	}
	
	public void setDtPagamentoFim(Date dtPagamentoFim) {
		this.dtPagamentoFim = dtPagamentoFim;
	}
	
	@DisplayName("Valor da Nota")
	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@DisplayName("Natureza de opera��o")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	
	@DisplayName("Total")
	public Money getTotalnotas() {
		return totalnotas;
	}
	public void setTotalnotas(Money totalnotas) {
		this.totalnotas = totalnotas;
	}

	@DisplayName("Endere�o")
	public String getEnderecostr() {
		return enderecostr;
	}
	public void setEnderecostr(String enderecostr) {
		this.enderecostr = enderecostr;
	}	
	
	public Boolean getExibirTotais() {
		return exibirTotais;
	}
	
	public TipoVendedorEnum getTipoVendedor() {
		return tipoVendedor;
	}

	public void setTipoVendedor(TipoVendedorEnum tipoVendedor) {
		this.tipoVendedor = tipoVendedor;
	}
	
	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}
	
	public Cfop getCfop() {
		return cfop;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	@DisplayName("Tipo de opera��o")
	public Tipooperacaonota getTipooperacaonota() {
		return tipooperacaonota;
	}
	
	public void setTipooperacaonota(Tipooperacaonota tipooperacaonota) {
		this.tipooperacaonota = tipooperacaonota;
	}
	
	@DisplayName("Modelo")
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscalEnum() {
		return modeloDocumentoFiscalEnum;
	}
	
	public void setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum) {
		this.modeloDocumentoFiscalEnum = modeloDocumentoFiscalEnum;
	}
}