package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.ModelodocumentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoNotafiscalservicoReport;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.NotafiscalservicomostrarFiltro;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class NotaFiltro extends FiltroListagemSined {
	
	protected List<NotaTipo> listaNotaTipo;
	protected String numNota;
	protected List<NotaStatus> listaNotaStatus;
	protected Date dtEmissaoInicio;
	protected Date dtEmissaoFim;
	protected Date dtPagamentoInicio;
	protected Date dtPagamentoFim;
	protected Projeto projeto;
	protected Cliente cliente;
	protected String whereIn;
	protected Empresa empresa;
	protected Boolean mostraComNumero;
	protected Centrocusto centrocusto;
	protected Long numNotaDe;
	protected Long numNotaAte;
	protected Naturezaoperacao naturezaoperacao;
	protected Money valor;
	protected Material material;
	protected Cfop cfop;
	protected OrdenacaoNotafiscalservicoReport ordenacaoNotafiscalservicoReport;
	protected NotafiscalservicomostrarFiltro notafiscalservicomostrarFiltro;
	protected Boolean registroreceita;
	protected ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum;
	
	protected Integer numeroNfseDe;
	protected Integer numeroNfseAte;	
	protected Date dtCancelamentoInicio;
	protected Date dtCancelamentoFim;
	
	// campo da NFP relevante para o relat�rio em PDF da listagem
	protected Tipooperacaonota tipooperacaonota;
	
	{
		this.listaNotaTipo = NotaTipo.getTodosTipos();
		this.listaNotaStatus = NotaStatus.getTodosEstados();
		this.listaNotaStatus.remove(NotaStatus.CANCELADA);
	}
	
	@DisplayName("Tipo de nota")
	public List<NotaTipo> getListaNotaTipo() {
		return listaNotaTipo;
	}
	
	@DisplayName("Nota Fiscal n�")
	@MaxLength(30)
	public String getNumNota() {
		return numNota;
	}
	
	@DisplayName("Situa��o")
	public List<NotaStatus> getListaNotaStatus() {
		return listaNotaStatus;
	}
	
	@DisplayName("")
	public Date getDtEmissaoInicio() {
		return dtEmissaoInicio;
	}

	@DisplayName("")
	public Date getDtEmissaoFim() {
		return dtEmissaoFim;
	}
	
	@DisplayName("")
	public Date getDtPagamentoInicio() {
		return dtPagamentoInicio;
	}
	
	@DisplayName("")
	public Date getDtPagamentoFim() {
		return dtPagamentoFim;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Destinat�rio / Sacado")
	public Cliente getCliente() {
		return cliente;
	}

	public String getWhereIn() {
		return whereIn;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Boolean getMostraComNumero() {
		return mostraComNumero;
	}
	
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	@MaxLength(30)
	public Long getNumNotaDe() {
		return numNotaDe;
	}
	
	@MaxLength(30)
	public Long getNumNotaAte() {
		return numNotaAte;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Campo do relat�rio a ser ordenado")
	public OrdenacaoNotafiscalservicoReport getOrdenacaoNotafiscalservicoReport() {
		return ordenacaoNotafiscalservicoReport;
	}
	
	@DisplayName("Reg. na receita")
	public Boolean getRegistroreceita() {
		return registroreceita;
	}
	
	public Cfop getCfop() {
		return cfop;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public void setRegistroreceita(Boolean registroreceita) {
		this.registroreceita = registroreceita;
	}
	
	public void setOrdenacaoNotafiscalservicoReport(
			OrdenacaoNotafiscalservicoReport ordenacaoNotafiscalservicoReport) {
		this.ordenacaoNotafiscalservicoReport = ordenacaoNotafiscalservicoReport;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setMostraComNumero(Boolean mostraComNumero) {
		this.mostraComNumero = mostraComNumero;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setListaNotaTipo(List<NotaTipo> listaNotaTipo) {
		this.listaNotaTipo = listaNotaTipo;
	}
	
	public void setNumNota(String numNota) {
		this.numNota = numNota;
	}
	
	public void setListaNotaStatus(List<NotaStatus> listaNotaStatus) {
		this.listaNotaStatus = listaNotaStatus;
	}
	
	public void setDtEmissaoInicio(Date dtEmissaoInicio) {
		this.dtEmissaoInicio = dtEmissaoInicio;
	}
	
	public void setDtEmissaoFim(Date dtEmissaoFim) {
		this.dtEmissaoFim = dtEmissaoFim;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	public void setDtPagamentoInicio(Date dtPagamentoInicio) {
		this.dtPagamentoInicio = dtPagamentoInicio;
	}
	
	public void setDtPagamentoFim(Date dtPagamentoFim) {
		this.dtPagamentoFim = dtPagamentoFim;
	}
	
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	public void setNumNotaDe(Long numNotaDe) {
		this.numNotaDe = numNotaDe;
	}
	
	public void setNumNotaAte(Long numNotaAte) {
		this.numNotaAte = numNotaAte;
	}

	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public Integer getNumeroNfseDe() {
		return numeroNfseDe;
	}

	public Integer getNumeroNfseAte() {
		return numeroNfseAte;
	}

	public void setNumeroNfseDe(Integer numeroNfseDe) {
		this.numeroNfseDe = numeroNfseDe;
	}

	public void setNumeroNfseAte(Integer numeroNfseAte) {
		this.numeroNfseAte = numeroNfseAte;
	}

	public Date getDtCancelamentoInicio() {
		return dtCancelamentoInicio;
	}

	public Date getDtCancelamentoFim() {
		return dtCancelamentoFim;
	}

	public void setDtCancelamentoInicio(Date dtCancelamentoInicio) {
		this.dtCancelamentoInicio = dtCancelamentoInicio;
	}

	public void setDtCancelamentoFim(Date dtCancelamentoFim) {
		this.dtCancelamentoFim = dtCancelamentoFim;
	}
	
	public NotafiscalservicomostrarFiltro getNotafiscalservicomostrarFiltro() {
		return notafiscalservicomostrarFiltro;
	}
	
	public void setNotafiscalservicomostrarFiltro(
			NotafiscalservicomostrarFiltro notafiscalservicomostrarFiltro) {
		this.notafiscalservicomostrarFiltro = notafiscalservicomostrarFiltro;
	}
	
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscalEnum() {
		return modeloDocumentoFiscalEnum;
	}
	
	public void setModeloDocumentoFiscalEnum(
			ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum) {
		this.modeloDocumentoFiscalEnum = modeloDocumentoFiscalEnum;
	}
	public Tipooperacaonota getTipooperacaonota() {
		return tipooperacaonota;
	}
	public void setTipooperacaonota(Tipooperacaonota tipooperacaonota) {
		this.tipooperacaonota = tipooperacaonota;
	}
}
