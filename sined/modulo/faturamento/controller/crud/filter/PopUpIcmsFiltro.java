package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;


public class PopUpIcmsFiltro {
	
	protected String index;
	
	protected Origemproduto origemprodutoicms;
	protected Tipotributacaoicms tipotributacaoicms;
	protected Tipocobrancaicms tipocobrancaicms; 
	
	protected Double aliquotacreditoicms;
	protected Money valorcreditoicms;
	
	protected Modalidadebcicms modalidadebcicms;
	protected Boolean calcularbcicmsretidoanteriormente;
	protected Double reducaobcicms;
	protected Money valorbcicms;
	protected Double icms;
	protected Money valoricms;
	protected Money bcoperacaopropriaicms;
	protected Motivodesoneracaoicms motivodesoneracaoicms; 
	protected Double percentualdesoneracaoicms;
	protected Boolean abaterdesoneracaoicms;
	
	protected Modalidadebcicmsst modalidadebcicmsst;
	protected Money reducaobcicmsst;
	protected Money margemvaloradicionalicmsst;
	protected Money valorbcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	protected Uf uficmsst;
	
	protected ValorFiscal valorFiscalIcms;
	protected Naturezabasecalculocredito naturezabcc;
	protected String formulabcicms;
	protected String formulabcicmsdestinatario;
	
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public Origemproduto getOrigemprodutoicms() {
		return origemprodutoicms;
	}
	public Tipotributacaoicms getTipotributacaoicms() {
		return tipotributacaoicms;
	}
	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}
	public Modalidadebcicms getModalidadebcicms() {
		return modalidadebcicms;
	}
	public Boolean getCalcularbcicmsretidoanteriormente() {
		return calcularbcicmsretidoanteriormente;
	}
	public Double getReducaobcicms() {
		return reducaobcicms;
	}
	public Money getValorbcicms() {
		return valorbcicms;
	}
	public Double getIcms() {
		return icms;
	}
	public Money getValoricms() {
		return valoricms;
	}
	public Money getBcoperacaopropriaicms() {
		return bcoperacaopropriaicms;
	}
	public Motivodesoneracaoicms getMotivodesoneracaoicms() {
		return motivodesoneracaoicms;
	}
	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}
	public Money getReducaobcicmsst() {
		return reducaobcicmsst;
	}
	public Money getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}
	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}
	public Double getIcmsst() {
		return icmsst;
	}
	public Money getValoricmsst() {
		return valoricmsst;
	}
	public Uf getUficmsst() {
		return uficmsst;
	}
	public Double getAliquotacreditoicms() {
		return aliquotacreditoicms;
	}
	public Money getValorcreditoicms() {
		return valorcreditoicms;
	}
	public Double getPercentualdesoneracaoicms() {
		return percentualdesoneracaoicms;
	}
	public Boolean getAbaterdesoneracaoicms() {
		return abaterdesoneracaoicms;
	}
	public String getFormulabcicmsdestinatario() {
		return formulabcicmsdestinatario;
	}
	public void setFormulabcicmsdestinatario(String formulabcicmsdestinatario) {
		this.formulabcicmsdestinatario = formulabcicmsdestinatario;
	}
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms) {
		this.abaterdesoneracaoicms = abaterdesoneracaoicms;
	}
	public void setPercentualdesoneracaoicms(Double percentualdesoneracaoicms) {
		this.percentualdesoneracaoicms = percentualdesoneracaoicms;
	}
	public void setAliquotacreditoicms(Double aliquotacreditoicms) {
		this.aliquotacreditoicms = aliquotacreditoicms;
	}
	public void setValorcreditoicms(Money valorcreditoicms) {
		this.valorcreditoicms = valorcreditoicms;
	}
	public void setOrigemprodutoicms(Origemproduto origemprodutoicms) {
		this.origemprodutoicms = origemprodutoicms;
	}
	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms) {
		this.tipotributacaoicms = tipotributacaoicms;
	}
	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}
	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms) {
		this.modalidadebcicms = modalidadebcicms;
	}
	public void setCalcularbcicmsretidoanteriormente(Boolean calcularbcicmsretidoanteriormente) {
		this.calcularbcicmsretidoanteriormente = calcularbcicmsretidoanteriormente;
	}
	public void setReducaobcicms(Double reducaobcicms) {
		this.reducaobcicms = reducaobcicms;
	}
	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}
	public void setIcms(Double icms) {
		this.icms = icms;
	}
	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}
	public void setBcoperacaopropriaicms(Money bcoperacaopropriaicms) {
		this.bcoperacaopropriaicms = bcoperacaopropriaicms;
	}
	public void setMotivodesoneracaoicms(Motivodesoneracaoicms motivodesoneracaoicms) {
		this.motivodesoneracaoicms = motivodesoneracaoicms;
	}
	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}
	public void setReducaobcicmsst(Money reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}
	public void setMargemvaloradicionalicmsst(Money margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}
	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}
	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}
	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}
	public void setUficmsst(Uf uficmsst) {
		this.uficmsst = uficmsst;
	}
	public ValorFiscal getValorFiscalIcms() {
		return valorFiscalIcms;
	}
	public void setValorFiscalIcms(ValorFiscal valorFiscalIcms) {
		this.valorFiscalIcms = valorFiscalIcms;
	}
	public Naturezabasecalculocredito getNaturezabcc() {
		return naturezabcc;
	}
	public void setNaturezabcc(Naturezabasecalculocredito naturezabcc) {
		this.naturezabcc = naturezabcc;
	}
	public String getFormulabcicms() {
		return formulabcicms;
	}
	public void setFormulabcicms(String formulabcicms) {
		this.formulabcicms = formulabcicms;
	}
}
