package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventosFinaisCorreios;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacaoentrega;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ExpedicaoFiltro extends FiltroListagemSined {
	
	private Integer cdexpedicao;
	private Date dtexpedicao1;
	private Date dtexpedicao2;
	private Long identificadorcarregamento;
	private Empresa empresa;
	private Fornecedor transportadora;
	private Integer codigovenda;
	private Cliente cliente;
	private Uf uf;
	private Municipio municipio;
	private Regiao regiao;
	private Tipoetiqueta tipoetiqueta;
	private List<Expedicaosituacao> listaExpedicaosituacao;
	private List<Expedicaosituacaoentrega> listaExpedicaosituacaoentrega;
	private Boolean vendaCarregada;
	private String identificadorVendaInicial;
	private String identificadorVendaFinal;
	private Date dtPrevisaoEntregaIni;
	private Date dtPrevisaoEntregaFim;
	private EventosFinaisCorreios eventosFinaisCorreios;
	private String rastreamento;
	
	// Dados Clientes
	private Colaborador colaborador;
	protected TipoVendedorEnum tipoVendedor;
	private String razaoSocial;
	protected Tipopessoa tipopessoa;
	private Cpf cpf;
	private Cnpj cnpj;
	private String vendedorPrincipal;
	private String vendedor;
	
	//Adicionar Venda
	private Venda venda;
	private List<Vendamaterial> listaVendamaterial;	private Expedicao expedicao;	
	private String selectedItensEtiqueta;
	private Notafiscalproduto nota;
	private Arquivo arquivo;
	private List<Venda> listaVendas;
	
	private List<Expedicaoitem> listaExpedicaoItem;
	
	//remover notas
	private List<Notafiscalproduto> listaNotasDaExpedicao;
	
	public ExpedicaoFiltro() {
		listaExpedicaosituacao = new ArrayList<Expedicaosituacao>();
		listaExpedicaosituacao.add(Expedicaosituacao.EM_ABERTO);
		listaExpedicaosituacao.add(Expedicaosituacao.CONFIRMADA);
		if(ParametrogeralService.getInstance().getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO)){
			listaExpedicaosituacao.add(Expedicaosituacao.EM_SEPARACAO);
			listaExpedicaosituacao.add(Expedicaosituacao.SEPARACAO_REALIZADA);
			listaExpedicaosituacao.add(Expedicaosituacao.EM_CONFERENCIA);
			listaExpedicaosituacao.add(Expedicaosituacao.CONFERENCIA_REALIZADA);
		}
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@MaxLength(10)
	@DisplayName("Carregamento")
	public Long getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}

	public Fornecedor getTransportadora() {
		return transportadora;
	}

	@MaxLength(9)
	@DisplayName("C�digo da venda")
	public Integer getCodigovenda() {
		return codigovenda;
	}

	public Cliente getCliente() {
		return cliente;
	}
	
	public Date getDtexpedicao1() {
		return dtexpedicao1;
	}

	public Date getDtexpedicao2() {
		return dtexpedicao2;
	}
	
	public Uf getUf() {
		return uf;
	}
	
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}

	@DisplayName("Regi�o")
	public Regiao getRegiao() {
		return regiao;
	}
	
	@DisplayName("ID")
	@MaxLength(9)
	public Integer getCdexpedicao() {
		return cdexpedicao;
	}
	
	@DisplayName("Tipo de etiqueta")
	public Tipoetiqueta getTipoetiqueta() {
		return tipoetiqueta;
	}

	@DisplayName("Situa��es")
	public List<Expedicaosituacao> getListaExpedicaosituacao() {
		return listaExpedicaosituacao;
	}
	
	@DisplayName("Situa��es Entrega")	
	public List<Expedicaosituacaoentrega> getListaExpedicaosituacaoentrega() {
		return listaExpedicaosituacaoentrega;
	}

	public Venda getVenda() {
		return venda;
	}
	
	@DisplayName("Expedi��o")
	public Expedicao getExpedicao() {
		return expedicao;
	}
	
	@DisplayName("Nota Fiscal")
	public Notafiscalproduto getNota() {
		return nota;
	}
	
	@DisplayName("Importar Vendas")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public String getSelectedItensEtiqueta() {
		return selectedItensEtiqueta;
	}

	public Boolean getVendaCarregada() {
		return vendaCarregada;
	}
	
	public String getIdentificadorVendaInicial() {
		return identificadorVendaInicial;
	}
	
	public String getIdentificadorVendaFinal() {
		return identificadorVendaFinal;
	}
	
	public Date getDtPrevisaoEntregaIni() {
		return dtPrevisaoEntregaIni;
	}
	
	public Date getDtPrevisaoEntregaFim() {
		return dtPrevisaoEntregaFim;
	}
	
	@DisplayName("Eventos dos correios")
	public EventosFinaisCorreios getEventosFinaisCorreios() {
		return eventosFinaisCorreios;
	}

	public List<Vendamaterial> getListaVendamaterial() {
		return listaVendamaterial;
	}
	
	public List<Venda> getListaVendas() {
		return listaVendas;
	}
	@DisplayName("Lista de Notas da Expedi��o")
	public List<Expedicaoitem> getListaExpedicaoItem() {
		return listaExpedicaoItem;
	}
	
	public List<Notafiscalproduto> getListaNotasDaExpedicao() {
		return listaNotasDaExpedicao;
	}
	
	public void setListaNotasDaExpedicao(List<Notafiscalproduto> listaNotasDaExpedicao) {
		this.listaNotasDaExpedicao = listaNotasDaExpedicao;
	}
	public void setListaExpedicaoItem(List<Expedicaoitem> listaExpedicaoItem) {
		this.listaExpedicaoItem = listaExpedicaoItem;
	}
	public void setListaVendamaterial(List<Vendamaterial> listaVendamaterial) {
		this.listaVendamaterial = listaVendamaterial;
	}

	public void setVendaCarregada(Boolean vendaCarregada) {
		this.vendaCarregada = vendaCarregada;
	}
	
	public void setIdentificadorVendaInicial(String identificadorVendaInicial) {
		this.identificadorVendaInicial = identificadorVendaInicial;
	}
	
	public void setIdentificadorVendaFinal(String identificadorVendaFinal) {
		this.identificadorVendaFinal = identificadorVendaFinal;
	}
	
	public void setDtPrevisaoEntregaIni(Date dtPrevisaoEntregaIni) {
		this.dtPrevisaoEntregaIni = dtPrevisaoEntregaIni;
	}
	
	public void setDtPrevisaoEntregaFim(Date dtPrevisaoEntregaFim) {
		this.dtPrevisaoEntregaFim = dtPrevisaoEntregaFim;
	}
	
	public void setEventosFinaisCorreios(EventosFinaisCorreios eventosFinaisCorreios) {
		this.eventosFinaisCorreios = eventosFinaisCorreios;
	}

	public void setSelectedItensEtiqueta(String selectedItensEtiqueta) {
		this.selectedItensEtiqueta = selectedItensEtiqueta;
	}
	
	public void setTipoetiqueta(Tipoetiqueta tipoetiqueta) {
		this.tipoetiqueta = tipoetiqueta;
	}
	
	public void setCdexpedicao(Integer cdexpedicao) {
		this.cdexpedicao = cdexpedicao;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

	public void setDtexpedicao1(Date dtexpedicao1) {
		this.dtexpedicao1 = dtexpedicao1;
	}

	public void setDtexpedicao2(Date dtexpedicao2) {
		this.dtexpedicao2 = dtexpedicao2;
	}

	public void setIdentificadorcarregamento(Long identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}

	public void setTransportadora(Fornecedor transportadora) {
		this.transportadora = transportadora;
	}

	public void setCodigovenda(Integer codigovenda) {
		this.codigovenda = codigovenda;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setListaExpedicaosituacao(List<Expedicaosituacao> listaExpedicaosituacao) {
		this.listaExpedicaosituacao = listaExpedicaosituacao;
	}

	public void setListaExpedicaosituacaoentrega(
			List<Expedicaosituacaoentrega> listaExpedicaosituacaoentrega) {
		this.listaExpedicaosituacaoentrega = listaExpedicaosituacaoentrega;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
	}
	
	public void setNota(Notafiscalproduto nota) {
		this.nota = nota;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setListaVendas(List<Venda> listaVendas) {
		this.listaVendas = listaVendas;
	}
	
	@DisplayName("Tipo do vendedor")
	public TipoVendedorEnum getTipoVendedor() {
		return tipoVendedor;
	}
	
	public void setTipoVendedor(TipoVendedorEnum tipoVendedor) {
		this.tipoVendedor = tipoVendedor;
	}
	
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@DisplayName("Raz�o Social")
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	@DisplayName("Tipo")
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}

	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	
	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	public String getVendedorPrincipal() {
		return vendedorPrincipal;
	}

	public void setVendedorPrincipal(String vendedorPrincipal) {
		this.vendedorPrincipal = vendedorPrincipal;
	}

	public String getVendedor() {
		return vendedor;
	}

	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}
	
	public String getRastreamento() {
		return rastreamento;
	}
	
	public void setRastreamento(String rastreamento) {
		this.rastreamento = rastreamento;
	}
	
}
