package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaosituacao;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContratofaturalocacaoFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private Integer numero1;
	private Integer numero2;
	private Cliente cliente;
	private Contrato contrato;
	private Material material;
	private List<Contratofaturalocacaosituacao> listaSituacao = new ArrayList<Contratofaturalocacaosituacao>();
	private Date periodoEmissaoInicio;
	private Date periodoEmissaoFim;
	private Date periodoVencimentoInicio;
	private Date periodoVencimentoFim;
	private Date periodoCancelamentoInicio;
	private Date periodoCancelamentoFim;
	private Tipopessoa tipoPessoa;
	protected Cnpj cnpj;
	protected Cpf cpf;
	
	protected Boolean exibirTotais = Boolean.FALSE;
	protected Money totalfaturalocacao;
	
	
	public ContratofaturalocacaoFiltro() {
		listaSituacao.add(Contratofaturalocacaosituacao.EMITIDA);
		listaSituacao.add(Contratofaturalocacaosituacao.FATURADA);
		
		this.periodoEmissaoInicio = SinedDateUtils.addMesData(SinedDateUtils.firstDateOfMonth(), -1);
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Integer getNumero1() {
		return numero1;
	}
	public Integer getNumero2() {
		return numero2;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Contrato getContrato() {
		return contrato;
	}
	public Material getMaterial() {
		return material;
	}
	public List<Contratofaturalocacaosituacao> getListaSituacao() {
		return listaSituacao;
	}
	public void setListaSituacao(
			List<Contratofaturalocacaosituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setNumero1(Integer numero1) {
		this.numero1 = numero1;
	}
	public void setNumero2(Integer numero2) {
		this.numero2 = numero2;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}

	@DisplayName("Total")
	public Money getTotalfaturalocacao() {
		return totalfaturalocacao;
	}
	public void setTotalfaturalocacao(Money totalfaturalocacao) {
		this.totalfaturalocacao = totalfaturalocacao;
	}

	public Date getPeriodoEmissaoInicio() {
		return periodoEmissaoInicio;
	}

	public Date getPeriodoEmissaoFim() {
		return periodoEmissaoFim;
	}

	public Date getPeriodoVencimentoInicio() {
		return periodoVencimentoInicio;
	}

	public Date getPeriodoVencimentoFim() {
		return periodoVencimentoFim;
	}

	public Date getPeriodoCancelamentoInicio() {
		return periodoCancelamentoInicio;
	}

	public Date getPeriodoCancelamentoFim() {
		return periodoCancelamentoFim;
	}

	public void setPeriodoEmissaoInicio(Date periodoEmissaoInicio) {
		this.periodoEmissaoInicio = periodoEmissaoInicio;
	}

	public void setPeriodoEmissaoFim(Date periodoEmissaoFim) {
		this.periodoEmissaoFim = periodoEmissaoFim;
	}

	public void setPeriodoVencimentoInicio(Date periodoVencimentoInicio) {
		this.periodoVencimentoInicio = periodoVencimentoInicio;
	}

	public void setPeriodoVencimentoFim(Date periodoVencimentoFim) {
		this.periodoVencimentoFim = periodoVencimentoFim;
	}

	public void setPeriodoCancelamentoInicio(Date periodoCancelamentoInicio) {
		this.periodoCancelamentoInicio = periodoCancelamentoInicio;
	}

	public void setPeriodoCancelamentoFim(Date periodoCancelamentoFim) {
		this.periodoCancelamentoFim = periodoCancelamentoFim;
	}

	public Boolean getExibirTotais() {
		return exibirTotais;
	}
	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}

	@DisplayName("Tipo de Pessoa")
	public Tipopessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(Tipopessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
}
