package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Vendaorcamentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoPessoaVendaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.TipoVendedorEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ResumoDocumentotipoVenda;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VendaorcamentoFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private Colaborador colaborador;
	private TipoPessoaVendaEnum tipopessoa;
	private Cliente cliente;
	private Contacrm contacrm;
	private Cliente clienteindicacao;
	private String codigo;
	private String identificador;
	private String identificacaoexterna;
	private Material material;
	private Date dtinicio;
	private Date dtfim;
	private Integer cdvendaorcamento;
	private String whereIn;
	protected List<Vendaorcamentosituacao> listaVendaorcamentosituacao;
	protected Money totalgeral;
	protected Money totalgeralipi;
	protected Double qtdegeral;
	protected Money saldoprodutos;
	protected Regiao regiao;
	protected Projeto projeto;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Materialcategoria materialcategoria;
	protected Localarmazenagem localarmazenagem;
	protected String enderecoStr;
	private Categoria categoria;
	protected Pedidovendatipo pedidovendatipo;
	protected Boolean orcamentoprocessocompra;
	protected String whereIncdvendaorcamento;
	
	protected Boolean isRestricaoClienteVendedor;
	protected Boolean isRestricaoVendaVendedor;
	
	protected Campoextrapedidovendatipo campoextrapedidovendatipo;
	protected String valorCampoExtra;
	protected Fornecedor fornecedorRepresentacao;
	
//	UTILIZADO PARA N�O EXIBIR NENHUM RESULTADO QUANDO O USUARIO N�O FOR COLABORADOR E TIVER RESTRI��O VENDA/VENDEDOR
	protected Boolean naoexibirresultado;
	
	protected List<ResumoDocumentotipoVenda> listaResumoDocumentotipoVenda;
	
	protected Boolean comprovanteEmitido;
	protected Boolean comprovanteEnviado;
	
	protected TipoVendedorEnum tipoVendedor;
	
	public VendaorcamentoFiltro() {
		totalgeral = new Money();
		qtdegeral = 0d;
	}
	
	public VendaorcamentoFiltro(Money totalgeral, Money totalipi, Double qtdegeral) {
		this.totalgeral = totalgeral;
		this.qtdegeral = qtdegeral;
		this.totalgeralipi = totalgeral != null && totalipi != null ? totalgeral.add(totalipi) : new Money();
	}
	
	public VendaorcamentoFiltro(Money totalgeral, Double qtdegeral, Money saldoprodutos) {
		this.totalgeral = totalgeral;
		this.qtdegeral = qtdegeral;
		this.saldoprodutos = saldoprodutos;
		
	}

	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Conta")
	public Contacrm getContacrm() {
		return contacrm;
	}
	@DisplayName("Indica��o")
	public Cliente getClienteindicacao() {
		return clienteindicacao;
	}
	@DisplayName("C�digo")
	@MaxLength(10)
	public String getCodigo() {
		return codigo;
	}
	@DisplayName("Produto")
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Situa��o")
	public List<Vendaorcamentosituacao> getListaVendaorcamentosituacao() {
		return listaVendaorcamentosituacao;
	}
	@DisplayName("Total")
	public Money getTotalgeral() {
		return totalgeral;
	}
	
	@DisplayName("Quantidade")
	public Double getQtdegeral() {
		return qtdegeral;
	}
	
	public List<ResumoDocumentotipoVenda> getListaResumoDocumentotipoVenda() {
		return listaResumoDocumentotipoVenda;
	}
	
	public void setListaResumoDocumentotipoVenda(
			List<ResumoDocumentotipoVenda> listaResumoDocumentotipoVenda) {
		this.listaResumoDocumentotipoVenda = listaResumoDocumentotipoVenda;
	}
	
	public void setQtdegeral(Double qtdegeral) {
		this.qtdegeral = qtdegeral;
	}

	public void setTotalgeral(Money totalgeral) {
		this.totalgeral = totalgeral;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	public void setClienteindicacao(Cliente clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	@MaxLength(8)
	public Integer getCdvendaorcamento() {
		return cdvendaorcamento;
	}
	public void setCdvendaorcamento(Integer cdvendaorcamento) {
		this.cdvendaorcamento = cdvendaorcamento;
	}
	public void setListaVendaorcamentosituacao(
			List<Vendaorcamentosituacao> listaVendaorcamentosituacao) {
		this.listaVendaorcamentosituacao = listaVendaorcamentosituacao;
	}
	
	@DisplayName("Regi�o")
	public Regiao getRegiao() {
		return regiao;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	@DisplayName("Grupo do material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo do material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Categoria de material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	@DisplayName("Local de armazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public Boolean getNaoexibirresultado() {
		return naoexibirresultado;
	}
	public void setNaoexibirresultado(Boolean naoexibirresultado) {
		this.naoexibirresultado = naoexibirresultado;
	}

	@DisplayName("Saldo de Produtos")
	public Money getSaldoprodutos() {
		return saldoprodutos;
	}
	public void setSaldoprodutos(Money saldoprodutos) {
		this.saldoprodutos = saldoprodutos;
	}
	
	public Boolean getIsRestricaoClienteVendedor() {
		return isRestricaoClienteVendedor;
	}
	public Boolean getIsRestricaoVendaVendedor() {
		return isRestricaoVendaVendedor;
	}
	@DisplayName("Campo Adicional")
	public Campoextrapedidovendatipo getCampoextrapedidovendatipo() {
		return campoextrapedidovendatipo;
	}
	@DisplayName("Conte�do")
	public String getValorCampoExtra() {
		return valorCampoExtra;
	}
	public void setIsRestricaoClienteVendedor(Boolean isRestricaoClienteVendedor) {
		this.isRestricaoClienteVendedor = isRestricaoClienteVendedor;
	}
	public void setIsRestricaoVendaVendedor(Boolean isRestricaoVendaVendedor) {
		this.isRestricaoVendaVendedor = isRestricaoVendaVendedor;
	}
	public void setCampoextrapedidovendatipo(Campoextrapedidovendatipo campoextrapedidovendatipo) {
		this.campoextrapedidovendatipo = campoextrapedidovendatipo;
	}
	public void setValorCampoExtra(String valorCampoExtra) {
		this.valorCampoExtra = valorCampoExtra;
	}

	@DisplayName("Local de Entrega")
	@MaxLength(100)
	public String getEnderecoStr() {
		return enderecoStr;
	}

	public void setEnderecoStr(String enderecoStr) {
		this.enderecoStr = enderecoStr;
	}
	
	@DisplayName("Categoria de Cliente")
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	@DisplayName("Tipo de Pedido de Venda")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}

	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}

	public Boolean getOrcamentoprocessocompra() {
		return orcamentoprocessocompra;
	}

	public void setOrcamentoprocessocompra(Boolean orcamentoprocessocompra) {
		this.orcamentoprocessocompra = orcamentoprocessocompra;
	}
	
	@DisplayName("Ident. externa")
	@MaxLength(20)
	public String getIdentificacaoexterna() {
		return identificacaoexterna;
	}
	@DisplayName("Identificador do Or�amento")
	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setIdentificacaoexterna(String identificacaoexterna) {
		this.identificacaoexterna = identificacaoexterna;
	}
	
	@DisplayName("Fornecedor Representa��o")
	public Fornecedor getFornecedorRepresentacao() {
		return fornecedorRepresentacao;
	}
	public void setFornecedorRepresentacao(Fornecedor fornecedorRepresentacao) {
		this.fornecedorRepresentacao = fornecedorRepresentacao;
	}

	@DisplayName("Total Geral com IPI")
	public Money getTotalgeralipi() {
		return totalgeralipi;
	}

	public void setTotalgeralipi(Money totalgeralipi) {
		this.totalgeralipi = totalgeralipi;
	}

	@MaxLength(300)
	public String getWhereIncdvendaorcamento() {
		return whereIncdvendaorcamento;
	}

	public void setWhereIncdvendaorcamento(String whereIncdvendaorcamento) {
		this.whereIncdvendaorcamento = whereIncdvendaorcamento;
	}

	public Boolean getComprovanteEmitido() {
		return comprovanteEmitido;
	}

	public void setComprovanteEmitido(Boolean comprovanteEmitido) {
		this.comprovanteEmitido = comprovanteEmitido;
	}

	public Boolean getComprovanteEnviado() {
		return comprovanteEnviado;
	}

	public void setComprovanteEnviado(Boolean comprovanteEnviado) {
		this.comprovanteEnviado = comprovanteEnviado;
	}

	@DisplayName("Tipo de pessoa")
	public TipoPessoaVendaEnum getTipopessoa() {
		return tipopessoa;
	}

	public void setTipopessoa(TipoPessoaVendaEnum tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	
	@DisplayName("Tipo do vendedor")
	public TipoVendedorEnum getTipoVendedor() {
		return tipoVendedor;
	}
	
	public void setTipoVendedor(TipoVendedorEnum tipoVendedor) {
		this.tipoVendedor = tipoVendedor;
	}
}