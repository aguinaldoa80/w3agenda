package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import java.util.Date;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FaturamentocontratoFiltro extends FiltroListagemSined {
	
	private Date dtinicio;
	private Date dtfim;
	private Empresa empresa;

	public FaturamentocontratoFiltro(){
		this.dtinicio = SinedDateUtils.firstDateOfMonth();
		this.dtfim = SinedDateUtils.lastDateOfMonth();	
	}
	
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
}