package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RealizarpedidovendaFiltro extends FiltroListagemSined{
	
	private Empresa empresa;
	private Colaborador colaborador;
	private Cliente cliente;
	private Endereco endereco;
	private String codigo;
	protected Pedidovendasituacao pedidovendasituacao;
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Local de entrega")
	public Endereco getEndereco() {
		return endereco;
	}
	@DisplayName("C�digo")
	@MaxLength(10)
	public String getCodigo() {
		return codigo;
	}
	@Required
	@DisplayName("Descri��o")
	public Pedidovendasituacao getpedidovendasituacao() {
		return pedidovendasituacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setPedidoVendasituacao(Pedidovendasituacao pedidovendasituacao) {
		this.pedidovendasituacao = pedidovendasituacao;
	}
	
	

}
