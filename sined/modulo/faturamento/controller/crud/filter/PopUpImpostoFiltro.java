package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;

public class PopUpImpostoFiltro {
	
	private String index;
	private String telaImposto;
	
	protected String codigoseloipi;
	protected Integer qtdeseloipi;
	protected Cnpj cnpjprodutoripi;
	
	private Tipocobrancaipi tipocobrancaipi;
	private Tipocobrancacofins tipocobrancacofins;
	private Tipocobrancapis tipocobrancapis;
	private String codigoenquadramentoipi;
	
	private Tipocalculo tipocalculo;
	
	private Money basecalculo;
	private Double aliquota;
	private Money aliquotareais;
	private Double qtdevendida;
	private Money valor;
	
	private ValorFiscal valorFiscalIpi;
	private String formulabcipi;
	
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public Tipocobrancapis getTipocobrancapis() {
		return tipocobrancapis;
	}

	public Tipocalculo getTipocalculo() {
		return tipocalculo;
	}

	public Money getBasecalculo() {
		return basecalculo;
	}

	public Double getAliquota() {
		return aliquota;
	}

	public Money getAliquotareais() {
		return aliquotareais;
	}

	public Double getQtdevendida() {
		return qtdevendida;
	}

	public Money getValor() {
		return valor;
	}

	public String getTelaImposto() {
		return telaImposto;
	}
	
	public Tipocobrancacofins getTipocobrancacofins() {
		return tipocobrancacofins;
	}
	
	@MaxLength(60)
	public String getCodigoseloipi() {
		return codigoseloipi;
	}

	public Integer getQtdeseloipi() {
		return qtdeseloipi;
	}

	public Cnpj getCnpjprodutoripi() {
		return cnpjprodutoripi;
	}
	
	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	
	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}

	public void setCodigoseloipi(String codigoseloipi) {
		this.codigoseloipi = codigoseloipi;
	}

	public void setQtdeseloipi(Integer qtdeseloipi) {
		this.qtdeseloipi = qtdeseloipi;
	}

	public void setCnpjprodutoripi(Cnpj cnpjprodutoripi) {
		this.cnpjprodutoripi = cnpjprodutoripi;
	}

	public void setTipocobrancacofins(Tipocobrancacofins tipocobrancacofins) {
		this.tipocobrancacofins = tipocobrancacofins;
	}

	public void setTelaImposto(String telaImposto) {
		this.telaImposto = telaImposto;
	}

	public void setTipocobrancapis(Tipocobrancapis tipocobrancapis) {
		this.tipocobrancapis = tipocobrancapis;
	}

	public void setTipocalculo(Tipocalculo tipocalculo) {
		this.tipocalculo = tipocalculo;
	}

	public void setBasecalculo(Money basecalculo) {
		this.basecalculo = basecalculo;
	}

	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}

	public void setAliquotareais(Money aliquotareais) {
		this.aliquotareais = aliquotareais;
	}

	public void setQtdevendida(Double qtdevendida) {
		this.qtdevendida = qtdevendida;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public ValorFiscal getValorFiscalIpi() {
		return valorFiscalIpi;
	}

	public void setValorFiscalIpi(ValorFiscal valorFiscalIpi) {
		this.valorFiscalIpi = valorFiscalIpi;
	}
	
	@DisplayName("F�rmula para c�lculo da base de c�lculo do IPI")
	public String getFormulabcipi() {
		return formulabcipi;
	}

	public void setFormulabcipi(String formulabcipi) {
		this.formulabcipi = formulabcipi;
	}

	@DisplayName("C�digo de enquadramento")
	@MaxLength(3)
	public String getCodigoenquadramentoipi() {
		return codigoenquadramentoipi;
	}

	public void setCodigoenquadramentoipi(String codigoenquadramentoipi) {
		this.codigoenquadramentoipi = codigoenquadramentoipi;
	}
}
