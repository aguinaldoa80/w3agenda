package br.com.linkcom.sined.modulo.faturamento.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContratomaterialFiltro extends FiltroListagemSined {

	private Cliente cliente;
	private Contrato contrato;
	private Boolean semProducaoagenda;
	
	public Cliente getCliente() {
		return cliente;
	}
	public Contrato getContrato() {
		return contrato;
	}
	public Boolean getSemProducaoagenda() {
		return semProducaoagenda;
	}
	public void setSemProducaoagenda(Boolean semProducaoagenda) {
		this.semProducaoagenda = semProducaoagenda;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
}
