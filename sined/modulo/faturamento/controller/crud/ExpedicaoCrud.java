package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.correios.webservice.resource.CorreiosUtils;
import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ConferenciaMaterialExpedicao;
import br.com.linkcom.sined.geral.bean.ConsultaEventoCorreios;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Embalagem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaohistorico;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Expedicaoiteminspecao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialinspecaoitem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.MaterialunidademedidaVO;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacaoentrega;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColetaMaterialService;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.ConferenciaMaterialExpedicaoService;
import br.com.linkcom.sined.geral.service.ConsultaEventoCorreiosService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.Ecom_PedidoVendaService;
import br.com.linkcom.sined.geral.service.EmbalagemService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EventosFinaisCorreiosService;
import br.com.linkcom.sined.geral.service.ExpedicaoAcompanhamentoEntregaService;
import br.com.linkcom.sined.geral.service.ExpedicaoService;
import br.com.linkcom.sined.geral.service.ExpedicaohistoricoService;
import br.com.linkcom.sined.geral.service.ExpedicaoitemService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialinspecaoitemService;
import br.com.linkcom.sined.geral.service.MaterialrelacionadoService;
import br.com.linkcom.sined.geral.service.MovimentacaoEstoqueHistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.ReservaService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.BaixarExpedicaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ClienteVO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ConferenciaMateriaisBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ConferenciaMateriaisVendaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ConferienciaPneuBeam;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ExpedicaoTrocaLoteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ExpedicaoFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.CorreiosBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/Expedicao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "")
public class ExpedicaoCrud extends CrudControllerSined<ExpedicaoFiltro, Expedicao, Expedicao> {

	private ExpedicaoService expedicaoService;
	private EnderecoService enderecoService;
	private ContatoService contatoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ExpedicaohistoricoService expedicaohistoricoService;
	private VendaService vendaService;
	private VendahistoricoService vendahistoricoService;
	private EmpresaService empresaService;
	private MaterialService materialService;
	private RegiaoService regiaoService;
	private MaterialinspecaoitemService materialinspecaoitemService;
	private ParametrogeralService parametrogeralService;
	private ExpedicaoitemService expedicaoitemService;
	private UnidademedidaService unidademedidaService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private ClienteService clienteService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private VendamaterialService vendamaterialService;
	private MaterialrelacionadoService materialrelacionadoService;
	private LocalarmazenagemService localarmazenagemService;
	private PedidovendatipoService pedidovendatipoService;
	private ReservaService reservaService;
	private ColetaMaterialService coletaMaterialService;
	private ColetaService coletaService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	private InventarioService inventarioService;
	private PedidovendaService pedidovendaService;
	private PneuService pneuService;
	private LoteestoqueService loteestoqueService;
	private ConferenciaMaterialExpedicaoService conferenciaMaterialExpedicaoService;
	private EmbalagemService embalagemService;
	private Ecom_PedidoVendaService ecom_PedidoVendaService;
	private ArquivonfnotaService arquivonfnotaService;
	private ConsultaEventoCorreiosService consultaEventoCorreiosService;
	private EventosFinaisCorreiosService eventosFinaisCorreiosService;
	
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {this.notafiscalprodutoitemService = notafiscalprodutoitemService;}
	public void setColetaService(ColetaService coletaService) {this.coletaService = coletaService;}
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {this.coletaMaterialService = coletaMaterialService;}
	public void setRegiaoService(RegiaoService regiaoService) {
		this.regiaoService = regiaoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {
		this.vendahistoricoService = vendahistoricoService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setExpedicaohistoricoService(ExpedicaohistoricoService expedicaohistoricoService) {
		this.expedicaohistoricoService = expedicaohistoricoService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setExpedicaoService(ExpedicaoService expedicaoService) {
		this.expedicaoService = expedicaoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMaterialinspecaoitemService(MaterialinspecaoitemService materialinspecaoitemService) {
		this.materialinspecaoitemService = materialinspecaoitemService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setExpedicaoitemService(ExpedicaoitemService expedicaoitemService) {
		this.expedicaoitemService = expedicaoitemService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {
		this.materialrelacionadoService = materialrelacionadoService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {
		this.pedidovendatipoService = pedidovendatipoService;
	}
	public void setReservaService(ReservaService reservaService) {
		this.reservaService = reservaService;
	}
	public void setMovimentacaoEstoqueHistoricoService(MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {
		this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	public void setConferenciaMaterialExpedicaoService(ConferenciaMaterialExpedicaoService conferenciaMaterialExpedicaoService) {
		this.conferenciaMaterialExpedicaoService = conferenciaMaterialExpedicaoService;
	}
	public void setEmbalagemService(EmbalagemService embalagemService) {
		this.embalagemService = embalagemService;
	}
	public void setEcom_PedidoVendaService(Ecom_PedidoVendaService ecom_PedidoVendaService) {
		this.ecom_PedidoVendaService = ecom_PedidoVendaService;
	}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setConsultaEventoCorreiosService(ConsultaEventoCorreiosService consultaEventoCorreiosService) {
		this.consultaEventoCorreiosService = consultaEventoCorreiosService;
	}
	public void setEventosFinaisCorreiosService(EventosFinaisCorreiosService eventosFinaisCorreiosService) {
		this.eventosFinaisCorreiosService = eventosFinaisCorreiosService;
	}
	
	@Override
	protected Expedicao criar(WebRequestContext request, Expedicao form) throws Exception {
		Expedicao expedicao = super.criar(request, form);
		
		String vendas = request.getParameter("vendas");
		if(vendas != null && !vendas.equals("")){
			String cdlocalarmazenagemstr = request.getParameter("cdlocalarmazenagem");
			Localarmazenagem localarmazenagem = null;
			if(cdlocalarmazenagemstr != null && !"".equals(cdlocalarmazenagemstr)){
				try {
					localarmazenagem = new Localarmazenagem(Integer.parseInt(cdlocalarmazenagemstr));
				} catch (Exception e) {}
			}
			expedicaoService.preencheExpedicaoByVenda(expedicao, vendas, localarmazenagem);
			
			boolean exists = vendaService.existsVendaWithTipobaixa(vendas, BaixaestoqueEnum.APOS_VENDAREALIZADA);
			request.setAttribute("EXISTS_BAIXA_APOS_VENDA", exists);
			exists = vendaService.existsVendaWithTipobaixa(vendas, BaixaestoqueEnum.APOS_EMISSAONOTA);
			request.setAttribute("EXISTS_BAIXA_APOS_EMISSAO_NF", exists);
			exists = vendaService.existsVendaWithTipobaixa(vendas, BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA);
			request.setAttribute("EXISTS_BAIXA_APOS_EXPEDICAO_CONFIRMADA", exists);
			
			form.setFromVenda(true);
		}
		
		String whereIn = request.getParameter("whereInUnificacaoExpedicao");
		if(whereIn!=null && !whereIn.equals("")){
			List<Expedicao> listaExpedicao = expedicaoService.findByUnificacao(whereIn);
			expedicaoService.preencheExpedicaoByUnificar(expedicao, listaExpedicao, whereIn);
		}
		
		boolean isExpedicaoColeta = false;
		boolean isExpedicaoNota = false;
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("COLETA")){
			isExpedicaoColeta = true;
			String whereInColetas = request.getParameter("listaColeta"); 
			if(whereInColetas!=null && !whereInColetas.equals("")){
				List<ColetaMaterial> listaColetaMaterial = coletaMaterialService.findByColeta(whereInColetas);
				List<Expedicaoitem> listaItem = retornaListaItem(form);
				if(SinedUtil.isListNotEmpty(listaColetaMaterial)){
					for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
						 Expedicaoitem item = criarItemColeta(coletaMaterial);
						if(item !=null){
							listaItem.add(item);
						}
					}
					request.setAttribute("origemColeta", true);
					request.setAttribute("exibirPneu", true);
				}
				expedicao.setListaExpedicaoitem(listaItem);
			}
		}else if (parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("NOTA")){
			isExpedicaoNota = true;
			request.setAttribute("exibirPneu", true);
			String whereInNotas = request.getParameter("listaNotas");
			if(whereInNotas!=null && !whereInNotas.equals("")){
				expedicao.setWhereInColetaOuNota(whereInNotas);
				List<Notafiscalprodutoitem> listaNotas = notafiscalprodutoitemService.findByWhereInCdnota(whereInNotas);
				List<Expedicaoitem> listaItem = retornaListaItem(form);
				for (Notafiscalprodutoitem nota : listaNotas) {
					Expedicaoitem item = criarItemNota(nota);
					if(item !=null){
						listaItem.add(item);
					}
				}
				expedicao.setListaExpedicaoitem(listaItem);
			}
		}
		
		List<Empresa> listaEmpresas = empresaService.findAtivos();
		
		if(listaEmpresas.size() == 1){
			expedicao.setEmpresa(listaEmpresas.get(0));
		}
		
		request.setAttribute("URL_RASTREAMENTO_CORREIOS", parametrogeralService.buscaValorPorNome(Parametrogeral.URL_RASTREAMENTO_CORREIOS));
		
		request.setAttribute("isExpedicaoColeta", isExpedicaoColeta);
		request.setAttribute("isExpedicaoNota", isExpedicaoNota);
		request.setAttribute("isRepagem", SinedUtil.isRecapagem());
		expedicao.setGerandoPelaVenda(form.getGerandoPelaVenda());
		return expedicao;
	}
	
	private Expedicaoitem criarItemNota(Notafiscalprodutoitem nota){
		Expedicaoitem item = new Expedicaoitem();
		nota.getNotafiscalproduto().setSituacaoExpedicao(SituacaoVinculoExpedicao.EXPEDI��O_COMPLETA);
		item.setNotaFiscalProdutoItem(nota);
		item.setDtexpedicaoitem(SinedDateUtils.currentDate());
		item.setMaterial(nota.getMaterial());
		item.setQtdeexpedicao(nota.getQtde());
		if(nota.getNotafiscalproduto() !=null){
			item.setCliente(nota.getNotafiscalproduto().getCliente());
			if(nota.getPedidovendamaterial() != null && nota.getPedidovendamaterial().getPedidovenda() != null){
				Endereco end = nota.getPedidovendamaterial().getPedidovenda().getEndereco();
				item.setEnderecocliente(end);
			}
		}
		item.setLocalarmazenagem(nota.getLocalarmazenagem());
		item.setUnidademedida(nota.getUnidademedida());
		item.setPneu(nota.getPneu());
		return item;
	}
	
	private Expedicaoitem criarItemColeta(ColetaMaterial coletaMaterial){
		Double qtdColeta = coletaMaterial.getQuantidade();
		Double qtdExpedido = 0.0;
		List<Expedicaoitem> listaItensExpedidos = expedicaoitemService.findByColetaMaterial(coletaMaterial.getCdcoletamaterial(), null, null, null);
		if(SinedUtil.isListNotEmpty(listaItensExpedidos)){
			for (Expedicaoitem itensExpedidos : listaItensExpedidos) {
				qtdExpedido += itensExpedidos.getQtdeexpedicao();
			}
		}
		if(qtdColeta - qtdExpedido > 0){
			Expedicaoitem item = new Expedicaoitem();
			item.setColetaMaterial(coletaMaterial);
			item.setPneu(coletaMaterial.getPneu());
			item.setQtdeexpedicao(qtdColeta - qtdExpedido);
			item.setCliente(coletaMaterial.getColeta().getCliente());
			item.setLocalarmazenagem(coletaMaterial.getLocalarmazenagem());
			item.setMaterial(coletaMaterial.getMaterial());
			item.setDtexpedicaoitem(SinedDateUtils.currentDate());
			if(coletaMaterial.getMaterial() !=null){
				item.setUnidademedida(coletaMaterial.getMaterial().getUnidademedida());
			}
			return item;
		}
		return null;
	}
	
	private List<Expedicaoitem> retornaListaItem(Expedicao form){
		List<Expedicaoitem> listaItem = form.getListaExpedicaoitem();
		if(SinedUtil.isListEmpty(listaItem)){
			listaItem = new ArrayList<Expedicaoitem>();
		}
		return listaItem;
	}
	
	@Override
	protected void listagem(WebRequestContext request, ExpedicaoFiltro filtro) throws Exception {
		request.setAttribute("listaEtiqueta", Tipoetiqueta.getListaTipoetiquetaExpedicao());
		
		List<Expedicaosituacao> listaExpedicaosituacao = Expedicaosituacao.getListagem();
		boolean isConferirPneu = false;
		if(parametrogeralService.getBoolean(Parametrogeral.CONFERENCIA_EXPEDICAO_RECAPAGEM) && SinedUtil.isRecapagem()){
			isConferirPneu= true;
		}
		boolean isIncluirPneu = false;
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("NOTA") && SinedUtil.isRecapagem()){
			isIncluirPneu= true;
		}
		
		request.setAttribute("RECAPAGEM", SinedUtil.isRecapagem());	
		request.setAttribute("INCLUIR_PNEU", isIncluirPneu);	
		request.setAttribute("CONFERIR_PNEU", isConferirPneu);
		request.setAttribute("listaExpedicaosituacao", listaExpedicaosituacao);
		if(SinedUtil.isIntegracaoEcompleto()){
			List<Expedicaosituacaoentrega> listaExpedicaosituacaoentrega = Expedicaosituacaoentrega.getListagem();
			request.setAttribute("listaExpedicaosituacaoentrega", listaExpedicaosituacaoentrega);			
		}
		request.setAttribute("LISTAR_RAZAOSOCIAL_CLIENTE_EXPEDICAO", parametrogeralService.getBoolean(Parametrogeral.LISTAR_RAZAOSOCIAL_CLIENTE_EXPEDICAO));
		request.setAttribute("isEcompleto", SinedUtil.isIntegracaoEcompleto());
		request.setAttribute("INTEGRACAO_ECOMMERCE", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE));
		request.setAttribute("INTEGRACAO_CORREIOS", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_CORREIOS));
		
		setAttributeExpedicao(request);
	}
	
	private void setAttributeExpedicao(WebRequestContext request) {
		request.setAttribute("imprimirSeparacao", request.getSession().getAttribute("imprimirSeparacao"));	
		request.setAttribute("whereInImprimirSeparacao", request.getSession().getAttribute("whereInImprimirSeparacao"));	
		request.getSession().removeAttribute("imprimirSeparacao");
		request.getSession().removeAttribute("whereInImprimirSeparacao");
		
		request.setAttribute("imprimirEtiquetaExpedicao", request.getSession().getAttribute("imprimirEtiquetaExpedicao"));	
		request.setAttribute("whereInImprimirEtiquetaExpedicao", request.getSession().getAttribute("whereInImprimirEtiquetaExpedicao"));	
		request.getSession().removeAttribute("imprimirEtiquetaExpedicao");
		request.getSession().removeAttribute("whereInImprimirEtiquetaExpedicao");
	}
	
	@Override
	protected ListagemResult<Expedicao> getLista(WebRequestContext request, ExpedicaoFiltro filtro) {
		ListagemResult<Expedicao> listagemResult = super.getLista(request, filtro);
		List<Expedicao> lista = listagemResult.list();
		
		if(lista != null && lista.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdexpedicao", ",");
			lista.removeAll(lista);
			lista.addAll(expedicaoService.loadForListagem(whereIn, filtro, filtro.getOrderBy(), filtro.isAsc()));
		}		
		if(SinedUtil.isListNotEmpty(lista)){
			for(Expedicao expedicao: lista){
				if(Util.objects.isPersistent(expedicao.getEventoCorreios())){
					expedicao.setEventosFinaisCorreios(eventosFinaisCorreiosService.loadByEvento(expedicao.getEventoCorreios()));
				}
			}
		}
		return listagemResult;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, ExpedicaoFiltro filtro) throws CrudException {
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadFetch(filtro.getRegiao(), "listaRegiaolocal"));
		}
		request.setAttribute("CONFERIR_EXPEDICAO", parametrogeralService.getBoolean(Parametrogeral.CONFERIR_EXPEDICAO));
		request.setAttribute("SEPARACAO_EXPEDICAO", parametrogeralService.getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO));
		boolean usaSeparacaoConferenciaMateriais = expedicaoService.isUsaSeparacaoConferenciaMateriais();
		request.setAttribute("USA_SEPARACAO_CONFERENCIA", usaSeparacaoConferenciaMateriais);
		request.setAttribute("NOME_ACAO_CONFERENCIA_MATERIAIS", usaSeparacaoConferenciaMateriais? "Iniciar confer�ncia": "Confer�ncia de materiais");
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Expedicao form) throws Exception {
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
        boolean isEditavel = true;
        if(parametrogeralService.getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO)){        	
            isEditavel = form.getExpedicaosituacao().equals(Expedicaosituacao.EM_ABERTO) || form.getExpedicaosituacao().equals(Expedicaosituacao.EM_SEPARACAO) 
            		|| form.getExpedicaosituacao().equals(Expedicaosituacao.SEPARACAO_REALIZADA) || form.getExpedicaosituacao().equals(Expedicaosituacao.EM_CONFERENCIA);
        } else{
            isEditavel = form.getExpedicaosituacao().equals(Expedicaosituacao.EM_ABERTO);
        }
        request.setAttribute("isEditavel", isEditavel);       	
        request.getSession().setAttribute("expedicao_attribute", request.getSession().getId());
		//request.setAttribute("nameAttribute", request.getSession().getId());
		boolean existsBaixaAposVenda =  false;
		if(form.getCdexpedicao() != null){
			form.setListaAcompanhamentoEntrega(ExpedicaoAcompanhamentoEntregaService.getInstance().findByExpedicao(form));
			if(Util.objects.isPersistent(form.getEventoCorreios())){
				form.setEventosFinaisCorreiosTrans(eventosFinaisCorreiosService.loadByEvento(form.getEventoCorreios()));
			}
			if(form.getListaExpedicaoitem() != null && form.getListaExpedicaoitem().size() > 0){
				List<Integer> idsVendas = new ArrayList<Integer>();
				HashMap<Material, Material> mapKit = new HashMap<Material, Material>();
				for (Expedicaoitem expedicaoitem : form.getListaExpedicaoitem()) {
					if(expedicaoitem.getVendamaterial()!=null){
						if(expedicaoitem.getVendamaterial().getMaterial() != null && 
								expedicaoitem.getVendamaterial().getMaterial().getVendapromocional() != null &&
								expedicaoitem.getVendamaterial().getMaterial().getVendapromocional()){
							if(mapKit.get(expedicaoitem.getVendamaterial().getMaterial()) == null){
								Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(expedicaoitem.getVendamaterial().getMaterial().getCdmaterial());
								material.setListaMaterialrelacionado(materialrelacionadoService.ordernarListaKit(material.getListaMaterialrelacionado()));
								try{
									material.setProduto_altura(expedicaoitem.getVendamaterial().getAltura());
									material.setProduto_largura(expedicaoitem.getVendamaterial().getLargura());
									material.setQuantidade(expedicaoitem.getVendamaterial().getQuantidade());
									materialrelacionadoService.calculaQuantidadeKit(material);			
									mapKit.put(expedicaoitem.getVendamaterial().getMaterial(), material);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							expedicaoitem.setVendamaterial(vendamaterialService.getVendamaterialForExpedicao(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial(),mapKit));
						}
					}
					
					if(expedicaoitem.getCodigovenda() != null){
						idsVendas.add(expedicaoitem.getCodigovenda());
					}
				}
				if(idsVendas != null && idsVendas.size() > 0){
					form.setFromVenda(Boolean.TRUE);
					form.setVendas(CollectionsUtil.concatenate(idsVendas, ","));
					
					boolean exists = vendaService.existsVendaWithTipobaixa(form.getVendas(), BaixaestoqueEnum.APOS_VENDAREALIZADA);
					request.setAttribute("EXISTS_BAIXA_APOS_VENDA", existsBaixaAposVenda);
					exists = vendaService.existsVendaWithTipobaixa(form.getVendas(), BaixaestoqueEnum.APOS_EMISSAONOTA);
					request.setAttribute("EXISTS_BAIXA_APOS_EMISSAO_NF", exists);
					exists = vendaService.existsVendaWithTipobaixa(form.getVendas(), BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA);
					request.setAttribute("EXISTS_BAIXA_APOS_EXPEDICAO_CONFIRMADA", exists);
				}
			}
		}
		
		boolean isConferirPneu = false;
		if(parametrogeralService.getBoolean(Parametrogeral.CONFERENCIA_EXPEDICAO_RECAPAGEM) && SinedUtil.isRecapagem()){
			isConferirPneu= true;
		}
		
		boolean isExpedicaoColeta = false;
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("COLETA")){
			isExpedicaoColeta = true;
		}
		
		if(SinedUtil.isListNotEmpty(form.getListaExpedicaoitem())){
			for (Expedicaoitem  item : form.getListaExpedicaoitem()) {
				if(item.getLoteestoque() != null && item.getLoteestoque().getValidade() == null){
					Date validadeLote = loteestoqueService.getValidadeByMaterialLote(item.getLoteEstoque(), item.getMaterial());
					item.getLoteEstoque().setValidade(validadeLote);
				}
				if(item.getColetaMaterial() != null && (item.getColetaMaterial().getCdcoletamaterial() != null || item.getColetaMaterial().getColeta() !=null)){
					request.setAttribute("origemColeta", true);
					request.setAttribute("exibirPneu", true);
					break;
				}else if(item.getNotaFiscalProdutoItem() !=null && item.getNotaFiscalProdutoItem().getCdnotafiscalprodutoitem()!=null){
					request.setAttribute("origemColeta", true);
					request.setAttribute("exibirPneu", true);
					request.setAttribute("isExpedicaoNota", true);
				}
				if(Util.objects.isPersistent(item.getVendamaterial())){
					item.setEntregaFutura(vendamaterialService.isEntregaFutura(item.getVendamaterial()));
				}
			}
		}
		
		boolean isIncluirPneu = false;
		if((parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("NOTA")) && SinedUtil.isRecapagem()){
			isIncluirPneu= true;
		}
		
		
		request.setAttribute("RECAPAGEM", SinedUtil.isRecapagem());
		request.setAttribute("INCLUIR_PNEU", isIncluirPneu);
		request.setAttribute("isExpedicaoColeta", isExpedicaoColeta);
		request.setAttribute("CONFERIR_PNEU", isConferirPneu);
		request.setAttribute("CONFERIR_EXPEDICAO", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.CONFERIR_EXPEDICAO)));
		request.setAttribute("SEPARACAO_EXPEDICAO", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.SEPARACAO_EXPEDICAO)));
		request.setAttribute("isEcompleto", SinedUtil.isIntegracaoEcompleto());
		request.setAttribute("INTEGRACAO_ECOMMERCE", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE));
		request.setAttribute("INTEGRACAO_CORREIOS", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_CORREIOS));
		
		boolean usaSeparacaoConferenciaMateriais = expedicaoService.isUsaSeparacaoConferenciaMateriais();
		request.setAttribute("USA_SEPARACAO_CONFERENCIA", usaSeparacaoConferenciaMateriais);
		request.setAttribute("NOME_ACAO_CONFERENCIA_MATERIAIS", usaSeparacaoConferenciaMateriais? "Iniciar confer�ncia": "Confer�ncia de materiais");

		boolean bool = parametrogeralService.getBoolean(Parametrogeral.SELECAO_AUTOMATICA_LOTE_FATURAMENTO);
		request.setAttribute("SELECAO_AUTOMATICA_LOTE_FATURAMENTO", bool);
		setAttributeExpedicao(request);
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Expedicao form)
			throws CrudException {	
		return super.doEntrada(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Expedicao bean) throws Exception {
		boolean isCriar = bean.getCdexpedicao() == null;
		Boolean fromVenda = bean.getFromVenda();
		String vendas = bean.getVendas();
		String whereIn = bean.getWhereIn();
		List<Venda> listavendasBeforeDelete = null;
		if(bean.getCdexpedicao() != null && !isCriar){
			listavendasBeforeDelete = vendaService.findByExpedicoesComItens(bean.getCdexpedicao());
		}
		
		for (Expedicaoitem item : bean.getListaExpedicaoitem()) {
			if(item.getPneu()!=null && item.getPneu().getCdpneu() == null){
				item.setPneu(null);
			}
			if(item.getColetaMaterial()!=null && item.getColetaMaterial().getCdcoletamaterial() == null){
				item.setColetaMaterial(null);
			}
		}
		
		if(isCriar && bean.getEmpresa() != null && Boolean.TRUE.equals(bean.getIdentificadorAutomatico())){
			Integer proximoIdentificador = empresaService.carregaProximoIdentificadorCarregamento(bean.getEmpresa());
			if(proximoIdentificador == null){
				proximoIdentificador = 1;
			}
			empresaService.updateProximoIdentificadorCarregamento(bean.getEmpresa(), proximoIdentificador+1);
			bean.setIdentificadorcarregamento(Long.parseLong(proximoIdentificador.toString()));
		}
		String obsCamposAlterados = "";
		if(!isCriar){
			Expedicao beanAntigo = expedicaoService.loadForEntrada(bean);
			obsCamposAlterados = expedicaoService.getCamposAlterados(beanAntigo, bean);			
		}
		
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("COLETA")){
			List<Expedicaoitem> listaItemCoferencia = bean.getListaExpedicaoitem();
			HashMap<Integer, Integer> itensColeta = new HashMap<Integer, Integer>();
			for (Expedicaoitem expedicaoItem : listaItemCoferencia) {
				materialService.validateObrigarLote(expedicaoItem);
				Double qtdSalvando = expedicaoItem.getQtdeexpedicao();
				Double qtdExpedido = 0.0;
				Double qtdColeta = 0.0; 
				if(expedicaoItem.getColetaMaterial()!=null && expedicaoItem.getColetaMaterial().getCdcoletamaterial()!=null){
					ColetaMaterial coletaMaterial = coletaMaterialService.findForExpedicao(expedicaoItem.getColetaMaterial().getCdcoletamaterial());
					qtdColeta += coletaMaterial.getQuantidade();
					List<Expedicaoitem> listaItensExpedidos = expedicaoitemService.findByColetaMaterial(expedicaoItem.getColetaMaterial().getCdcoletamaterial(), null, expedicaoItem.getCdexpedicaoitem(), whereIn);
					if(SinedUtil.isListNotEmpty(listaItensExpedidos)){
						for (Expedicaoitem itensExpedidos : listaItensExpedidos) {
							qtdExpedido += itensExpedidos.getQtdeexpedicao();
						}
					}
					qtdSalvando = qtdExpedido + qtdSalvando;
					if(qtdSalvando.compareTo(qtdColeta) > 0){
						throw new SinedException("N�o � permitido gerar expedi��o com quantidade superior � quantidade da nota.");
					}else if (qtdSalvando.compareTo(qtdColeta) < 0){
						expedicaoItem.getColetaMaterial().getColeta().setIsColetaIncompleta(true);
					}
					if(itensColeta.isEmpty() || !itensColeta.containsKey(coletaMaterial.getColeta().getCdcoleta())){
						List<Expedicaoitem> listaItens = expedicaoitemService.findByColeta(coletaMaterial.getColeta(), bean);
						if(SinedUtil.isListNotEmpty(listaItens)){
							for (Expedicaoitem item : listaItens) {
								if(itensColeta.isEmpty() || !itensColeta.containsKey(coletaMaterial.getColeta().getCdcoleta())){
									itensColeta.put(coletaMaterial.getColeta().getCdcoleta(), 1);
								} else {
									Integer valor = itensColeta.get(coletaMaterial.getColeta().getCdcoleta());
									valor ++;
									itensColeta.put(coletaMaterial.getColeta().getCdcoleta(), valor);
								}
							}
						}
					}
					if(itensColeta.isEmpty() || !itensColeta.containsKey(coletaMaterial.getColeta().getCdcoleta())){
						itensColeta.put(coletaMaterial.getColeta().getCdcoleta(), 1);
					}else {
						Integer valor = itensColeta.get(coletaMaterial.getColeta().getCdcoleta());
						valor ++;
						itensColeta.put(coletaMaterial.getColeta().getCdcoleta(), valor);
					}
				}
				
			}

			if(itensColeta !=null && itensColeta.size() > 0){
				for (Map.Entry<Integer, Integer> o : itensColeta.entrySet()) {
					Coleta coleta = coletaService.findById(o.getKey());
					if(coleta.getListaColetaMaterial()!=null && coleta.getListaColetaMaterial().size() > o.getValue()){
						for (Expedicaoitem expedicaoItem : listaItemCoferencia) {
							if(expedicaoItem.getColetaMaterial()!=null && expedicaoItem.getColetaMaterial().getCdcoletamaterial()!=null && expedicaoItem.getColetaMaterial().getColeta() !=null && expedicaoItem.getColetaMaterial().getColeta().getCdcoleta().equals(o.getKey())){
								expedicaoItem.getColetaMaterial().getColeta().setIsColetaIncompleta(true);
							}
						}
					}
				}
			}
		}
		
		
		
		bean.setIsCriar(isCriar);
		validaOrdementrega(bean);
		
		if(isCriar){
			expedicaoService.preparaListaInspecao(bean);
		}
		super.salvar(request, bean);
		
		if (bean.getListaExpedicaoitem() != null && bean.getListaExpedicaoitem().size() > 0) {
			for (Expedicaoitem item : bean.getListaExpedicaoitem()) {
				if (item.getPneu() != null) {
					if (item.getPneu().existeDados()) {
						pneuService.saveOrUpdate(item.getPneu());
					}
				}
			}
		}
		
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("COLETA")){
			List<Expedicaoitem> listaItens = bean.getListaExpedicaoitem();
			HashMap<Integer, Boolean> mapColetaAtualizada = new HashMap<Integer, Boolean>();
			for (Expedicaoitem itens : listaItens) {
				if(itens.getColetaMaterial()!=null && itens.getColetaMaterial().getColeta()!=null){
					Coleta coleta = itens.getColetaMaterial().getColeta();
					if(mapColetaAtualizada.isEmpty() || !mapColetaAtualizada.containsKey(coleta.getCdcoleta()) || (Boolean.TRUE.equals(coleta.getIsColetaIncompleta()) && !mapColetaAtualizada.get(coleta.getCdcoleta()))){
						String obsParaHistorico = "Entrega <a href=/w3erp/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao="+bean.getCdexpedicao()+" ' >"+bean.getCdexpedicao()+"</a> ";
						if(Boolean.TRUE.equals(coleta.getIsColetaIncompleta())){
							coletaService.updateSituacaoColeta(coleta,SituacaoVinculoExpedicao.EXPEDI��O_PARCIAL,true,obsParaHistorico,Coletaacao.ENTREGA);
							mapColetaAtualizada.put(coleta.getCdcoleta(), true);
						}else{
							coletaService.updateSituacaoColeta(coleta,SituacaoVinculoExpedicao.EXPEDI��O_COMPLETA,true,obsParaHistorico,Coletaacao.ENTREGA);
							mapColetaAtualizada.put(coleta.getCdcoleta(), false);
						}
					}
				}
			}
		}else if (parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("NOTA")) {
			List<Expedicaoitem> listaItens = bean.getListaExpedicaoitem();
			HashMap<Integer, Boolean> mapNotaAtualizada = new HashMap<Integer, Boolean>();
			for (Expedicaoitem itens : listaItens) {
				if(itens.getNotaFiscalProdutoItem()!=null && itens.getNotaFiscalProdutoItem().getNotafiscalproduto()!=null){
					Notafiscalproduto nota = itens.getNotaFiscalProdutoItem().getNotafiscalproduto();
					if(Boolean.FALSE.equals(mapNotaAtualizada.containsKey(nota.getCdNota())) && nota.getCdNota()!=null){
						String obsParaHistorico = "Entregue a expedi��o <a href=/w3erp/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao="+bean.getCdexpedicao()+" ' >"+bean.getCdexpedicao()+"</a> ";
						notafiscalprodutoService.atualizaExpedicao(obsParaHistorico, SituacaoVinculoExpedicao.EXPEDI��O_COMPLETA,nota,NotaStatus.ENTREGA);
						mapNotaAtualizada.put(nota.getCdNota(), true);
					}
				}
			}
		}
		
		Expedicaohistorico expedicaohistoricoConf = null; 
		
		String obs = "";
		List<Expedicaoitem> listaExpedicaoitem = bean.getListaExpedicaoitem();
		if(fromVenda && isCriar){
			vendaService.atualizarExpedicaoSituacao(vendas);
			if(isCriar){
				obs = expedicaoService.makeObservacaoVenda(vendas);
				vendahistoricoService.makeHistoricoExpedicao(bean, vendas);
			}
			
			if (isCriar && Boolean.TRUE.equals(bean.getConferenciamateriais())){
				expedicaohistoricoConf = new Expedicaohistorico();
				expedicaohistoricoConf.setExpedicao(bean);
				expedicaohistoricoConf.setExpedicaoacao(Expedicaoacao.CONFERENCIA_PREVIA_REALIZADA);
				expedicaohistoricoService.saveOrUpdate(expedicaohistoricoConf);
			}
			
			for(String cdVendaStr: vendas.split(",")){
				if(vendaService.isVendaEcommerce(Integer.parseInt(cdVendaStr))){
					Venda venda = new Venda();
					venda.setCdvenda(Integer.parseInt(cdVendaStr));
					expedicaoService.insertExpedicaoCriadaTabelaSincronizacaoEcommerce(venda);
				}
			}
			
		} else {
			List<Integer> idsVenda = new ArrayList<Integer>();
			for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
				Integer codigovenda = expedicaoitem.getCodigovenda();
				if(codigovenda != null){
					idsVenda.add(codigovenda);
				}
			}
			
			if(idsVenda != null && idsVenda.size() > 0){
				vendaService.atualizarExpedicaoSituacao(CollectionsUtil.concatenate(idsVenda, ","));
			}
		}
		expedicaoService.gerarReservaAoSalvar(bean, null);
		
		List<Integer> idsVendaMaterial = new ArrayList<Integer>();
		for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
			if(expedicaoitem.getVendamaterial() != null && 
					expedicaoitem.getVendamaterial().getCdvendamaterial() != null){
				idsVendaMaterial.add(expedicaoitem.getVendamaterial().getCdvendamaterial());
			}
		}
		if(idsVendaMaterial != null && idsVendaMaterial.size() > 0){
			String whereInVendaMaterial = CollectionsUtil.concatenate(idsVendaMaterial, ",");
			vendaService.atualizaTransportadoraByVendaMaterial(whereInVendaMaterial, bean.getTransportadora());
		}
		
		if(whereIn!=null && !whereIn.equals("")){
			obs = expedicaoService.makeObservacaoUnificao(whereIn);
			expedicaohistoricoService.saveHistorico(whereIn,obs,Expedicaoacao.ALTERADA);
			movimentacaoestoqueService.cancelarByExpedicao(whereIn, "unificar");
			expedicaoService.updateSituacao(whereIn, Expedicaosituacao.CANCELADA);
			expedicaohistoricoService.saveHistorico(whereIn, "", Expedicaoacao.CANCELADA);
			List<Venda> listavendas = vendaService.findByExpedicoes(whereIn);
			String whereInVenda = CollectionsUtil.listAndConcatenate(listavendas, "cdvenda", ",");
			if(whereInVenda != null && !whereInVenda.equals("")){
				vendahistoricoService.makeHistoricoExpedicaoUnificacao(bean, whereInVenda);
			}
		}
		
		Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
		expedicaohistorico.setExpedicao(bean);
		expedicaohistorico.setObservacao(obs);
		expedicaohistorico.setExpedicaoacao(isCriar ? Expedicaoacao.CRIADA : Expedicaoacao.ALTERADA);
		if(StringUtils.isNotBlank(obsCamposAlterados)){
			expedicaohistorico.setObservacao(obsCamposAlterados);
		}
		expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
		
		Object obj = request.getSession().getAttribute("conferencia_"+bean.getIdTela());
		if(obj != null && expedicaohistoricoConf != null){
			List<ConferenciaMaterialExpedicao> listaConf = (List)obj;
			
			String observa = "<a href=\"javascript:visualizaConferencia("
					+ expedicaohistoricoConf.getCdexpedicaohistorico() + ");\">Clique aqui para consultar a confer�ncia</a>.";
			expedicaohistoricoConf.setObservacao(observa);
			expedicaohistoricoService.saveOrUpdate(expedicaohistoricoConf);

			for(ConferenciaMaterialExpedicao item: listaConf){
				item.setExpedicaoHistorico(expedicaohistoricoConf);
				item.setExpedicao(bean);
				conferenciaMaterialExpedicaoService.saveOrUpdate(item);
			}

		}
		request.getSession().removeAttribute("conferencia_"+bean.getIdTela());

		if(listavendasBeforeDelete != null && !isCriar){
			List<Venda> vendasRemovidas = new ArrayList<Venda>();
			for(Venda venda: listavendasBeforeDelete){
				List<Venda> vendaComItensNaoExcluidos = vendaService.findByExpedicoesComItens(bean.getCdexpedicao(), venda);
				if(vendaComItensNaoExcluidos == null || vendaComItensNaoExcluidos.isEmpty()){
					vendasRemovidas.add(venda);
				}
			}
			if(!vendasRemovidas.isEmpty()){
				String whereInVendasRemovidas = CollectionsUtil.listAndConcatenate(vendasRemovidas, "cdvenda", ",");
				vendahistoricoService.makeHistoricoRemocaoItensExpedicao(bean, whereInVendasRemovidas);
				StringBuilder historico = new StringBuilder();
				historico.append("Venda(s) removida(s) da expedi��o: ");
				int i = 0;
				for(Venda venda: vendasRemovidas){
					historico.append("<a href='javascript:visualizarVenda(" + venda.getCdvenda() + ")'>" + venda.getIdentificadorOrCdvenda() + "</a>");
					i++;
					if(i<vendasRemovidas.size()){
						historico.append(",");
					}

					expedicaoService.ajustaReservaByCancelamento(venda, bean, null);
				}
				
				Expedicaohistorico expedicaohistoricoExclusaoVenda = new Expedicaohistorico();
				expedicaohistoricoExclusaoVenda.setExpedicao(bean);
				expedicaohistoricoExclusaoVenda.setObservacao(historico.toString());
				expedicaohistoricoExclusaoVenda.setExpedicaoacao(Expedicaoacao.VENDA_REMOVIDA);
				expedicaohistoricoService.saveOrUpdate(expedicaohistoricoExclusaoVenda);
			}
		}
		
		vendaService.verificaExpedicaoSituacao(bean, listavendasBeforeDelete);
	}
	
	private void validaOrdementrega(Expedicao bean) {
		boolean existOrdem = false;
		
		for (Expedicaoitem expedicaoitem : bean.getListaExpedicaoitem()){
			if(expedicaoitem.getOrdementrega() != null){
				existOrdem = true;
				break;
			}
		}
		if(existOrdem){
			List<Integer> ordens = new ArrayList<Integer>();
			for (Expedicaoitem expedicaoitem : bean.getListaExpedicaoitem()){
				if(expedicaoitem.getOrdementrega() != null && ordens.contains(expedicaoitem.getOrdementrega()))
					throw new SinedException("N�o � possivel salvar um produto com a mesma ordem de entrega.");
				ordens.add(expedicaoitem.getOrdementrega());
			}
		}
	}
	
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Expedicao bean) {
		if(bean.getIsCriar() != null && bean.getIsCriar() && bean.getFromVenda() != null && bean.getFromVenda()){
			return new ModelAndView("redirect:/faturamento/crud/Venda");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	public ModelAndView enviaCodigoRastreioPorEmail(WebRequestContext request){
		JsonModelAndView view = new JsonModelAndView();
		String whereIn = request.getParameter("whereIn");
		
		if(whereIn == null || whereIn == ""){
			view.addObject("success", false);
			view.addObject("message", "N�o possui expedi��o selecionada");
			return view;
		} else if(whereIn.split(",").length > 1){
			view.addObject("success", false);
			view.addObject("message", "Possui mais de uma expedi��o selecionada");
			return view;
		} 
	
		Expedicao expedicao = null;
		
		try {
			expedicao = expedicaoService.findByCdExpedicaoForEmail(Integer.parseInt(whereIn));			
		} catch (Exception e) {
			view.addObject("success", false);
			view.addObject("message", "Falha ao encontrar expedi��o");
			return view;
		}
		
		if(expedicao != null){

			if(expedicao.getExpedicaosituacao()==Expedicaosituacao.CANCELADA){
				view.addObject("success", false);
				view.addObject("message", "Para realizar essa a��o a expedi��o deve estar em uma situa��o diferente de cancelada.");
				return view;				
			}	
			if(expedicao.getTransportadora() != null && expedicao.getTransportadora().getCorreios() != null && expedicao.getTransportadora().getCorreios()
					&& parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_CORREIOS)){
				view.addObject("success", false);
				view.addObject("message", "N�o � poss�vel enviar e-mail quando a transportadora � Correios");
				return view;				
			}
			
			if(expedicao.getUrlRastreamento() == null || expedicao.getUrlRastreamento().isEmpty() 
					|| expedicao.getRastreamento() == null || expedicao.getRastreamento().isEmpty()){
				view.addObject("success", false);
				view.addObject("message", "N�o foi poss�vel enviar o email de rastreamento, pois os campos, c�digo de rastreamento e Link de rastreio n�o foram preenchidos na Expedi��o");
				return view;
			}
		}else{
			view.addObject("success", false);
			view.addObject("message", "Expedi��o n�o encontrada.");
			return view;
		}
			
		return expedicaoService.enviarCodigodeRastreioPorEmail(request, expedicao);
	}
	
	public ModelAndView finalizarEntrega(WebRequestContext request, Expedicao expedicao){
		expedicao = expedicaoService.loadForEntrada(expedicao);		
		Venda venda = null;
		if(expedicao == null){
			request.addError("Expedi��o n�o encontrada.");
			SinedUtil.fechaPopUp(request);
			return null;
		} else {
			if(expedicao.getExpedicaosituacao() == null || (!expedicao.getExpedicaosituacao().equals(Expedicaosituacao.CONFIRMADA) && 
					!expedicao.getExpedicaosituacao().equals(Expedicaosituacao.FATURADA))){
				request.addError("Situa��o do registro diferente de 'Confirmada' ou 'Faturada'.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			if(expedicao.getListaExpedicaoitem() != null && expedicao.getListaExpedicaoitem().size() > 0){
				for(Expedicaoitem item: expedicao.getListaExpedicaoitem()){
					if(item.getVendamaterial() != null && item.getVendamaterial().getVenda() != null){
						if(venda == null)
							venda =  item.getVendamaterial().getVenda();
						else
							if(!item.getVendamaterial().getVenda().getCdvenda().equals(venda.getCdvenda())){
								request.addError("Expedi��o possui vendas diferentes.");
								return null;						
							}
					}
				}
			}
		}
		
		boolean finalizadaComSucesso = expedicaoService.finalizarEntregue(expedicao, venda);
		if(!finalizadaComSucesso){
			request.addMessage("N�o foi poss�vel atualizar o e-commerce.");
		}
		
		SinedUtil.fechaPopUp(request);		
		return null;
	}
	
	/**
	 * Action em ajax que busca as informa��es do cliente;
	 *
	 * @param request
	 * @param expedicaoitem
	 * @return
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxCarregaInfoCliente(WebRequestContext request, Expedicaoitem expedicaoitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(expedicaoitem.getCliente() != null){
			GenericBean genericBean;
			
			List<Endereco> listaEndereco = enderecoService.findAtivosByCliente(expedicaoitem.getCliente());
			List<GenericBean> listaEnderecocliente = new ArrayList<GenericBean>();
			for (Endereco endereco : listaEndereco) {
				genericBean = new GenericBean("br.com.linkcom.sined.geral.bean.Endereco[cdendereco=" + endereco.getCdendereco() + "]", endereco.getDescricaoCombo());
				listaEnderecocliente.add(genericBean);
			}
			
			List<Contato> listaContato = contatoService.findByPessoa(expedicaoitem.getCliente());
			List<GenericBean> listaContatocliente = new ArrayList<GenericBean>();
			for (Contato contato : listaContato) {
				genericBean = new GenericBean("br.com.linkcom.sined.geral.bean.Contato[cdpessoa=" + contato.getCdpessoa() + "]", contato.getNome());
				listaContatocliente.add(genericBean);
			}
			
			jsonModelAndView.addObject("listaEnderecocliente", listaEnderecocliente);
			jsonModelAndView.addObject("listaContatocliente", listaContatocliente);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * Action que realiza o cancelamento da expedi��o.
	 *
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService.haveExpedicaoNotInSituacao(String whereIn, Expedicaosituacao... situacoes)
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService.updateSituacao(String whereIn, Expedicaosituacao situacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService.cancelarByExpedicao(String whereIn)
	 * 
	 * @param request
	 * @return
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cancelar(WebRequestContext request){
		String idTelaSession = request.getParameter("idTelaSession");
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.CONFIRMADA, Expedicaosituacao.EM_ABERTO, Expedicaosituacao.EM_SEPARACAO, Expedicaosituacao.EM_CONFERENCIA, Expedicaosituacao.CONFERENCIA_REALIZADA);
		
		if(notValido){
			request.addError("Para cancelar a(s) expedi��o(�es) tem que ter situa��o(�es) 'CONFIRMADA' ou 'EM ABERTO' ou 'EM SEPARA��O' ou 'EM CONFER�NCIA' ou 'CONFER�NCIA REALIZADA'.");
			return sendRedirectToAction("listagem");
		}
		boolean existsCancelada = expedicaoService.haveExpedicaoBySituacao(whereIn, Expedicaosituacao.CANCELADA);
		if(existsCancelada){
			request.addError("N�o � poss�vel cancelar expedi��o(�es) j� cancelada(s).");
			return sendRedirectToAction("listagem");
		}
		for(String cdexpedicaoStr: whereIn.split(",")){
			Expedicao expedicao = new Expedicao(Integer.parseInt(cdexpedicaoStr));
			expedicao = expedicaoService.load(expedicao);
			if(!Expedicaosituacao.CANCELADA.equals(expedicao.getExpedicaosituacao()) && !Expedicaosituacao.EM_ABERTO.equals(expedicao.getExpedicaosituacao())){
				List<Venda> listaVendas = expedicaoService.findVendasByExpedicao(expedicao);
				if(CollectionsUtil.isListNotEmpty(listaVendas)){
					for(Venda venda: listaVendas){
						boolean exists = expedicaoService.haveExpedicaoByVenda(venda, expedicao.getCdexpedicao(), Expedicaosituacao.EM_ABERTO);
						if(exists){
							request.addError("A venda "+venda.getCdvenda()+" possui outra expedi��o relacionada com situa��o 'EM ABERTO'. Para cancelar a expedi��o, as expedi��es em aberto relacionadas � venda precisam ser canceladas primeiro.");
							return sendRedirectToAction("listagem");
						}
					}
				}
			}
		}
		
		List<Vendamaterial> listaItensNaoRecriarReserva = null;
		if(idTelaSession != null && !idTelaSession.equals("<null>")){
			System.out.println("ID da sess�o: "+idTelaSession);
			Object obj = request.getSession().getAttribute(idTelaSession);
			if(obj != null && CollectionsUtil.isListNotEmpty((List)obj)){
				listaItensNaoRecriarReserva = (List)obj;
				for(Vendamaterial vm: listaItensNaoRecriarReserva){
					Vendamaterial vmAux = vendamaterialService.load(vm, "vendamaterial.cdvendamaterial, vendamaterial.venda");
					vendamaterialService.updateCampo(vm, "sinalizadoSemReserva", true);
					vendaService.updateCampo(vmAux.getVenda(), "sinalizadoSemReserva", true);
				}
			}
		}
		
		List<Expedicao> listaExpedicao = expedicaoService.findByExpedicaosituacao(whereIn, Expedicaosituacao.CONFERENCIA_REALIZADA);
		if(CollectionsUtil.isListNotEmpty(listaExpedicao)){
			for(Expedicao exp: listaExpedicao){
				List<Venda> listaVendas = expedicaoService.findVendasByExpedicao(exp);
				if(CollectionsUtil.isListNotEmpty(listaVendas) && listaVendas.size() == 1){
					vendaService.limparQuantidadeVolume(listaVendas.get(0));
					expedicaoService.limparQuantidadeVolume(exp);
				}
			}
		}
		



		movimentacaoestoqueService.cancelarByExpedicao(whereIn, "cancelamento");
		expedicaoService.updateSituacao(whereIn, Expedicaosituacao.CANCELADA);
		expedicaohistoricoService.saveHistorico(whereIn, "", Expedicaoacao.CANCELADA);		
		
		List<Venda> listaVenda = vendaService.findByExpedicoes(whereIn);
		if(SinedUtil.isListNotEmpty(listaVenda)){
			String whereInVenda = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
			vendaService.atualizarExpedicaoSituacao(whereInVenda);
			for(Venda venda: listaVenda){
				for(Expedicao expedicaoCancelar: venda.getListaExpedicaotrans()){
					expedicaoService.ajustaReservaByCancelamento(venda, expedicaoCancelar, listaItensNaoRecriarReserva);
				}
			}
		}
		
		request.addMessage("Expedi��o(��es) cancelada(s) com sucesso."); 
		
		List<Expedicaoitem> listItem = expedicaoitemService.findByWhereInExpedicao(whereIn);
		if(SinedUtil.isListNotEmpty(listItem)){
			HashMap<Integer, Integer> mapColetaAtualizada = new HashMap<Integer, Integer>();
			for (Expedicaoitem expedicaoitem : listItem) {
				if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(expedicaoitem.getDtexpedicaoitem(), expedicaoitem.getExpedicao().getEmpresa(), 
						expedicaoitem.getLocalarmazenagem(), null)) {
		        	request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
		        			"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
		            return sendRedirectToAction("listagem");
		        }
				
				if(expedicaoitem.getColetaMaterial() != null && expedicaoitem.getColetaMaterial().getColeta()!=null){
					List<Expedicaoitem> listaOutrasExpedicoes = expedicaoitemService.findByColeta(expedicaoitem.getColetaMaterial().getColeta(), expedicaoitem.getExpedicao());
					if(SinedUtil.isListNotEmpty(listaOutrasExpedicoes)){
						for (Expedicaoitem itens : listaOutrasExpedicoes) {
							if(itens.getColetaMaterial()!=null && itens.getColetaMaterial().getColeta()!=null){
								Coleta coleta = itens.getColetaMaterial().getColeta();
								if(mapColetaAtualizada.isEmpty() || !mapColetaAtualizada.containsKey(coleta.getCdcoleta())){
									String obsParaHistorico = "Cancelamento entrega <a href=/w3erp/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao="+expedicaoitem.getExpedicao().getCdexpedicao()+" ' >"+expedicaoitem.getExpedicao().getCdexpedicao()+"</a> ";
									coletaService.updateSituacaoColeta(coleta,SituacaoVinculoExpedicao.EXPEDI��O_PARCIAL,true,obsParaHistorico,Coletaacao.CANCELAMENTO_ENTREGA);
									mapColetaAtualizada.put(coleta.getCdcoleta(), coleta.getCdcoleta());
								}
							}
						}
					}else {
						Coleta coleta = expedicaoitem.getColetaMaterial().getColeta();
						if(mapColetaAtualizada.isEmpty() || !mapColetaAtualizada.containsKey(coleta.getCdcoleta())){
							String obsParaHistorico = "Cancelamento entrega <a href=/w3erp/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao="+expedicaoitem.getExpedicao().getCdexpedicao()+" ' >"+expedicaoitem.getExpedicao().getCdexpedicao()+"</a> ";
							coletaService.updateSituacaoColeta(coleta,SituacaoVinculoExpedicao.PENDENTE,true,obsParaHistorico,Coletaacao.CANCELAMENTO_ENTREGA);
							mapColetaAtualizada.put(coleta.getCdcoleta(), coleta.getCdcoleta());
						}
					}
				}else if (parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("NOTA")) {
					HashMap<Integer, Boolean> mapNotaAtualizada = new HashMap<Integer, Boolean>();
					if(expedicaoitem.getNotaFiscalProdutoItem()!=null && expedicaoitem.getNotaFiscalProdutoItem().getNotafiscalproduto()!=null){
						Notafiscalproduto nota = expedicaoitem.getNotaFiscalProdutoItem().getNotafiscalproduto();
						if(Boolean.FALSE.equals(mapNotaAtualizada.containsKey(nota.getCdNota())) && nota.getCdNota()!=null){
							String obsParaHistorico = "Entregue a expedi��o <a href=/w3erp/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao="+expedicaoitem.getExpedicao().getCdexpedicao()+" ' >"+expedicaoitem.getExpedicao().getCdexpedicao()+"</a> ";
							notafiscalprodutoService.atualizaExpedicao(obsParaHistorico, SituacaoVinculoExpedicao.PENDENTE,nota,NotaStatus.ENTREGA);
							mapNotaAtualizada.put(nota.getCdNota(), true);
						}
					}
				}
			}
		}
		if("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao=" + whereIn);
		} else return sendRedirectToAction("listagem");
	}
	
	/**
	 * Action que realiza o estorno das expedi��es
	 * 
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService.haveExpedicaoNotInSituacao(String whereIn, Expedicaosituacao... situacoes)
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService.updateSituacao(String whereIn, Expedicaosituacao situacao)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService.cancelarByExpedicao(String whereIn)
	 *
	 * @param request
	 * @return
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView estornar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.CONFIRMADA, 
				Expedicaosituacao.CONFERENCIA_REALIZADA, Expedicaosituacao.EM_SEPARACAO, Expedicaosituacao.SEPARACAO_REALIZADA, Expedicaosituacao.EM_CONFERENCIA);
		
		if(notValido){
			request.addError("Para estornar a(s) expedi��o(�es) tem que ter situa��o(�es) 'EM SEPARA��O', 'AGUARDANDO CONFER�NCIA', 'EM CONFER�NCIA' ou 'CONFIRMADA'.");
			return sendRedirectToAction("listagem");
		}
		
		List<Expedicao> listaExpedicao = expedicaoService.findForEstornar(whereIn);
		
		List<Expedicao> listaExpedicaoCR = expedicaoService.getListaExpedicaoBySituacao(listaExpedicao, Expedicaosituacao.CONFERENCIA_REALIZADA);
		if(CollectionsUtil.isListNotEmpty(listaExpedicaoCR)){
			expedicaoService.updateSituacao(CollectionsUtil.listAndConcatenate(listaExpedicaoCR, "cdexpedicao", ","), Expedicaosituacao.EM_CONFERENCIA);
			for(Expedicao exp: listaExpedicaoCR){
				Venda venda = expedicaoService.getVendaUnica(exp);
				if(venda != null){
					vendaService.limparQuantidadeVolume(venda);
				}
				expedicaoService.limparQuantidadeVolume(exp);
			}
		}
		
		List<Expedicao> listaExpedicaoC = expedicaoService.getListaExpedicaoBySituacao(listaExpedicao, Expedicaosituacao.CONFIRMADA);
		if(CollectionsUtil.isListNotEmpty(listaExpedicaoC)){
			Expedicaosituacao situacaoVoltar = expedicaoService.isUsaSeparacaoConferenciaMateriais() ? Expedicaosituacao.CONFERENCIA_REALIZADA: Expedicaosituacao.EM_ABERTO;
			if(Expedicaosituacao.EM_ABERTO.equals(situacaoVoltar)){
				movimentacaoestoqueService.cancelarByExpedicao(CollectionsUtil.listAndConcatenate(listaExpedicaoC, "cdexpedicao", ","), "estorno");
			}
			expedicaoService.updateSituacao(CollectionsUtil.listAndConcatenate(listaExpedicaoC, "cdexpedicao", ","), situacaoVoltar);
		}
		
		List<Expedicao> listaExpedicaoES = expedicaoService.getListaExpedicaoBySituacao(listaExpedicao, Expedicaosituacao.EM_SEPARACAO);
		if(CollectionsUtil.isListNotEmpty(listaExpedicaoES)){
			Expedicaosituacao situacaoVoltar = Expedicaosituacao.EM_ABERTO;
			movimentacaoestoqueService.cancelarByExpedicao(CollectionsUtil.listAndConcatenate(listaExpedicaoES, "cdexpedicao", ","), "estorno");
			expedicaoService.updateSituacao(CollectionsUtil.listAndConcatenate(listaExpedicaoES, "cdexpedicao", ","), situacaoVoltar);
		}
		
		List<Expedicao> listaExpedicaoAC = expedicaoService.getListaExpedicaoBySituacao(listaExpedicao, Expedicaosituacao.SEPARACAO_REALIZADA);
		if(CollectionsUtil.isListNotEmpty(listaExpedicaoAC)){
			Expedicaosituacao situacaoVoltar = Expedicaosituacao.EM_SEPARACAO;
			expedicaoService.updateSituacao(CollectionsUtil.listAndConcatenate(listaExpedicaoAC, "cdexpedicao", ","), situacaoVoltar);
		}
		
		List<Expedicao> listaExpedicaoEC = expedicaoService.getListaExpedicaoBySituacao(listaExpedicao, Expedicaosituacao.EM_CONFERENCIA);
		if(CollectionsUtil.isListNotEmpty(listaExpedicaoEC)){
			Expedicaosituacao situacaoVoltar = Expedicaosituacao.SEPARACAO_REALIZADA;
			expedicaoService.updateSituacao(CollectionsUtil.listAndConcatenate(listaExpedicaoEC, "cdexpedicao", ","), situacaoVoltar);
		}
		
		List<Venda> listaVenda = vendaService.findByExpedicoes(whereIn);
		if(SinedUtil.isListNotEmpty(listaVenda)){
			String whereInVenda = CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",");
			vendaService.atualizarExpedicaoSituacao(whereInVenda);
			for(Venda venda: listaVenda){
				for(Expedicao expedicaoCancelar: venda.getListaExpedicaotrans()){
					expedicaoService.ajustaReservaByEstorno(venda, expedicaoCancelar);
				}
			}
		}
		expedicaohistoricoService.saveHistorico(whereIn, "", Expedicaoacao.ESTORNADA);
		
		request.addMessage("Expedi��o(��es) estornada(s) com sucesso."); 
		if("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao=" + whereIn);
		} else return sendRedirectToAction("listagem");
	}
	
	public ModelAndView doConfirmar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(parametrogeralService.getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO)){
			boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.CONFERENCIA_REALIZADA);
			if(notValido){
				request.addError("Para confirmar a(s) expedi��o(�es) tem que ter situa��o(�es) 'Confer�ncia Realizada'.");
				return sendRedirectToAction("listagem");
			}					
		} else {
			boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_ABERTO);
			if(notValido){
				request.addError("Para confirmar a(s) expedi��o(�es) tem que ter situa��o(�es) 'Em ABERTO'.");
				return sendRedirectToAction("listagem");
			}			
		}
			
		
		
		if (("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.CONFERIR_EXPEDICAO)) || Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.CONFERENCIA_EXPEDICAO_RECAPAGEM)))
				&& expedicaoService.isNaoPossuiConferenciaMateriais(whereIn)){
			if(Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.CONFERENCIA_EXPEDICAO_RECAPAGEM))){
				request.addError("Para confirmar a(s) expedi��o(�es) tem que realizar a confer�ncia dos pneus.");
			}else{
				request.addError("Para confirmar a(s) expedi��o(�es) tem que realizar a confer�ncia dos materiais.");
			}
			return sendRedirectToAction("listagem");
		}
		
		List<Expedicao> listaExpedicao = expedicaoService.findForConfirmacao(whereIn);
		
		for (Expedicao expedicao : listaExpedicao) {
			if(SinedUtil.isIntegracaoEcompleto() && StringUtils.isBlank(expedicao.getUrlRastreamento())){
				request.addError("N�o � poss�vel confirmar a expedi��o. � preciso incluir dados de rastreamento.");
	            return sendRedirectToAction("listagem");
			}
			for (Expedicaoitem expedicaoItem : expedicao.getListaExpedicaoitem()) {
				if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(expedicaoItem.getDtexpedicaoitem(), expedicao.getEmpresa(), 
						expedicaoItem.getLocalarmazenagem(), null)) {
		        	request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
		        			"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
		            return sendRedirectToAction("listagem");
		        }
			}
		}
		
		boolean existMaterialitemByMaterialgrademestreEntrega = false;
		for (Expedicao expedicao : listaExpedicao) {
			for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
				if(expedicaoitem.getUnidademedida() != null && expedicaoitem.getMaterial() != null && 
						expedicaoitem.getMaterial().getUnidademedida() != null &&
						!expedicaoitem.getMaterial().getUnidademedida().equals(expedicaoitem.getUnidademedida())){
					Double qtde = unidademedidaService.converteQtdeUnidademedida(expedicaoitem.getMaterial().getUnidademedida(), 
										expedicaoitem.getQtdeexpedicao(),
										expedicaoitem.getUnidademedida(),
										expedicaoitem.getMaterial(),null,1.0);
					expedicaoitem.setQuantidadePrincipal(qtde);
				}
					
				if(expedicaoitem.getMaterial() != null && expedicaoitem.getMaterial().getMaterialgrupo() != null && 
						expedicaoitem.getMaterial().getMaterialmestregrade() == null &&
						expedicaoitem.getMaterial().getMaterialgrupo().getGradeestoquetipo() != null){
					boolean existMaterialitemByMaterialgrademestre = materialService.existMaterialitemByMaterialgrademestre(expedicaoitem.getMaterial());
					if(existMaterialitemByMaterialgrademestre && !materialService.isControleMaterialgrademestre(expedicaoitem.getMaterial())){
						existMaterialitemByMaterialgrademestreEntrega = true;
						expedicaoitem.setListaMaterialitemmestregrade(expedicaoService.montaMaterialitemmestregrade(materialService.findMaterialitemByMaterialmestregrade(expedicaoitem.getMaterial())));
					}
					
				}
			}	
			
			expedicao.setListaExpedicaoitem(expedicaoService.getListaCorrigidaConfirmacao(expedicao.getListaExpedicaoitem()));
		}
		
		BaixarExpedicaoBean bean = new BaixarExpedicaoBean();
		bean.setListaExpedicao(listaExpedicao);
		
		if(existMaterialitemByMaterialgrademestreEntrega){
			return new ModelAndView("crud/popup/baixarExpedicaoItemmestregrade", "bean", bean);	
		}else {
			return continueOnAction("confirmar", bean);
		}
	}
	
	/**
	 * Action para confirma��o da expedi��o.
	 *
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService.findForConfirmacao(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService.updateSituacao(String whereIn, Expedicaosituacao situacao)
	 * @see  br.com.linkcom.sined.geral.service.ExpedicaoService.makeMovimentacaoestoqueByExpedicao(List<Movimentacaoestoque> listaMovimentacaoestoque, Expedicao expedicao, Expedicaoitem expedicaoitem, Material material, Double qtde, Movimentacaoestoquetipo movimentacaoestoquetipo)
	 * 
	 * @param request
	 * @return
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView confirmar(WebRequestContext request, BaixarExpedicaoBean bean){
		String whereIn = CollectionsUtil.listAndConcatenate(bean.getListaExpedicao(), "cdexpedicao", ",");
		try{
			String obs = "";
			List<Expedicao> listaExpedicao = bean.getListaExpedicao();
			validaPodeBaixarEstoque(listaExpedicao);
			List<Expedicaoitem> listaExpedicaoitem;
			List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
			for (Expedicao expedicao : listaExpedicao) {
				listaExpedicaoitem = expedicao.getListaExpedicaoitem();
				obs = "";
				if(listaExpedicaoitem != null && listaExpedicaoitem.size() > 0){
					for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
						if(expedicaoitem.isPodeBaixarEstoque()){
							Boolean isServico = null;
							if(expedicaoitem.getMaterial()!=null)
								isServico = Boolean.TRUE.equals(expedicaoitem.getMaterial().getServico());
//								Boolean expedicaoByVendaWithMovimentacaoestoque = expedicaoService.haveExpedicaoByVendaWithMovimentacaoestoque(expedicaoitem, null);
//								if(!expedicaoByVendaWithMovimentacaoestoque){
//								if(expedicaoitem.getMaterial().getProducao() != null && expedicaoitem.getMaterial().getProducao()){
//									Set<Materialproducao> listaProducao = expedicaoitem.getMaterial().getListaProducao();
//									if(listaProducao != null && listaProducao.size() > 0 && 
//											expedicaoitem.getMaterial().getProducaoetapa() == null){
//										for (Materialproducao materialproducao : listaProducao) {											
////												if("FALSE".equalsIgnoreCase(permitirEstoqueNegativo)){
//											if(!localarmazenagemService.getPermitirestoquenegativo(expedicaoitem.getLocalarmazenagem())){
//												Double qtdeProducao = materialproducao.getConsumo() * expedicaoitem.getQtdeexpedicao();
//												expedicaoService.validaQtdeEstoque(materialproducao.getMaterial(), expedicao.getEmpresa(), 
//														expedicaoitem.getLocalarmazenagem(), null, qtdeProducao);
//											}
//											
//											if(isServico==null || !isServico){												
//												expedicaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
//													expedicao, expedicaoitem, materialproducao.getMaterial(),
//													materialproducao.getConsumo() * expedicaoitem.getQtdeexpedicao(), Movimentacaoestoquetipo.SAIDA, true);
//											}
//										}
//										
////											if("FALSE".equalsIgnoreCase(permitirEstoqueNegativo)){
//										if(!localarmazenagemService.getPermitirestoquenegativo(expedicaoitem.getLocalarmazenagem())){
//											expedicaoService.validaQtdeEstoque(expedicaoitem.getMaterial(), expedicao.getEmpresa(), 
//													expedicaoitem.getLocalarmazenagem(), expedicaoitem.getLoteEstoque(), expedicaoitem.getQtdeexpedicao());
//										}			
//										
//										if(!isServico){
//											expedicaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
//													expedicao, expedicaoitem, expedicaoitem.getMaterial(), 
//													expedicaoitem.getQtdeexpedicao(), Movimentacaoestoquetipo.ENTRADA, false);
//											
//											expedicaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
//													expedicao, expedicaoitem, expedicaoitem.getMaterial(), 
//													expedicaoitem.getQtdeexpedicao(), Movimentacaoestoquetipo.SAIDA, false);
//											Movimentacaoestoque mov = listaMovimentacaoestoque.get(listaMovimentacaoestoque.size()-1);
//											if(reservaService.existsReserva(expedicaoitem, expedicaoitem.getMaterial())){
//												reservaService.desfazerReserva(expedicaoitem, expedicaoitem.getMaterial(), mov.getQtde());
//											}else if(expedicaoitem.getVendamaterial() != null && reservaService.existsReserva(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial())){
//												reservaService.desfazerReserva(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial(), mov.getQtde());
//											}
//										}
//									}else {
////											if("FALSE".equalsIgnoreCase(permitirEstoqueNegativo)){
//										if(!localarmazenagemService.getPermitirestoquenegativo(expedicaoitem.getLocalarmazenagem())){
//											expedicaoService.validaQtdeEstoque(expedicaoitem.getMaterial(), expedicao.getEmpresa(), 
//													expedicaoitem.getLocalarmazenagem(), expedicaoitem.getLoteEstoque(), expedicaoitem.getQtdeexpedicao());
//										}
//										
//										if(!isServico){
//											expedicaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
//													expedicao, expedicaoitem, expedicaoitem.getMaterial(), 
//														expedicaoitem.getQtdeexpedicao(), Movimentacaoestoquetipo.SAIDA, false);
//											Movimentacaoestoque mov = listaMovimentacaoestoque.get(listaMovimentacaoestoque.size()-1);
//											if(reservaService.existsReserva(expedicaoitem, expedicaoitem.getMaterial())){
//												reservaService.desfazerReserva(expedicaoitem, expedicaoitem.getMaterial(), mov.getQtde());
//											}else if(expedicaoitem.getVendamaterial() != null && reservaService.existsReserva(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial())){
//												reservaService.desfazerReserva(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial(), mov.getQtde());
//											}
//										}									
//									}
//								} else {
									if(expedicaoitem.getListaMaterialitemmestregrade() != null && 
											!expedicaoitem.getListaMaterialitemmestregrade().isEmpty()){
										for(Expedicaoitem itemGrade : expedicaoitem.getListaMaterialitemmestregrade()){
											if(itemGrade.getQtdeexpedicao() != null && 
													itemGrade.getQtdeexpedicao() > 0){
										
												if(!isServico){									
													expedicaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
															expedicao, expedicaoitem, itemGrade.getMaterial(), 
															itemGrade.getQtdeexpedicao(), Movimentacaoestoquetipo.SAIDA, false);
													Movimentacaoestoque mov = listaMovimentacaoestoque.get(listaMovimentacaoestoque.size()-1);
													if(reservaService.existsReserva(itemGrade, expedicaoitem.getMaterial())){
														reservaService.desfazerReserva(itemGrade, expedicaoitem.getMaterial(), mov.getQtde());
													}else if(itemGrade.getVendamaterial() != null && reservaService.existsReserva(itemGrade.getVendamaterial(), itemGrade.getMaterial())){
														reservaService.desfazerReserva(itemGrade.getVendamaterial(), itemGrade.getMaterial(), mov.getQtde());
													}
												}
											}
										}
									}else {
//											if("FALSE".equalsIgnoreCase(permitirEstoqueNegativo)){
										Double qtdeBaixar = expedicaoitem.getQuantidadePrincipal() != null?
															expedicaoitem.getQuantidadePrincipal(): expedicaoitem.getQtdeexpedicao();
										
										if(!isServico){									
											Double qtde = expedicaoitem.getVendamaterial() != null? expedicaoitem.getQtdeexpedicao(): qtdeBaixar;//Quando possui vinculo com a venda o m�todo makeMovimentacaoestoqueByExpedicao faz a convers�o de unidade
											expedicaoService.makeMovimentacaoestoqueByExpedicao(listaMovimentacaoestoque, 
													expedicao, expedicaoitem, expedicaoitem.getMaterial(), 
														qtde, Movimentacaoestoquetipo.SAIDA, false);
											Movimentacaoestoque mov = listaMovimentacaoestoque.get(listaMovimentacaoestoque.size()-1);
											if(reservaService.existsReserva(expedicaoitem, expedicaoitem.getMaterial())){
												reservaService.desfazerReserva(expedicaoitem, expedicaoitem.getMaterial(), mov.getQtde());
											}else if(expedicaoitem.getVendamaterial() != null && reservaService.existsReserva(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial())){
												reservaService.desfazerReserva(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial(), mov.getQtde());
											}
										}
									}
//								}
//								}
						}
					}
				}
				
				if(SinedUtil.isListNotEmpty(listaMovimentacaoestoque)){
					StringBuilder whereInMovEstoque = new StringBuilder();
					for (Movimentacaoestoque me : listaMovimentacaoestoque) {
						movimentacaoestoqueService.roundQtdeByUnidadeMedida(me);
						movimentacaoestoqueService.saveOrUpdate(me);
						if(!"".equals(whereInMovEstoque.toString())) whereInMovEstoque.append(",");
						whereInMovEstoque.append("<a href=\"javascript:visualizaMovimentacaoestoque(" + me.getCdmovimentacaoestoque() + ");\"> " + me.getCdmovimentacaoestoque() + "</a>");
					}
					
					if(!"".equals(whereInMovEstoque.toString())){
						obs = "Entrada/Sa�da: " + whereInMovEstoque.toString();
					}				
				}
				expedicaoService.updateSituacao(expedicao.getCdexpedicao().toString(), Expedicaosituacao.CONFIRMADA);
				Expedicao expedicaoAux = expedicaoService.loadWithExpedicaoEntregaSituacao(expedicao);
				if(!(Expedicaosituacaoentrega.IMPEDIDA.equals(expedicaoAux.getExpedicaosituacaoentrega()) || Expedicaosituacaoentrega.REALIZADA.equals(expedicaoAux.getExpedicaosituacaoentrega()))){
					expedicaoService.updateSituacaoentrega(expedicao.getCdexpedicao().toString(), Expedicaosituacaoentrega.PENDENTE);
				}
				expedicaohistoricoService.saveHistorico(expedicao.getCdexpedicao().toString(), obs, Expedicaoacao.CONFIRMADA);
			}
			for (Movimentacaoestoque me2 : listaMovimentacaoestoque) {
				movimentacaoEstoqueHistoricoService.preencherHistorico(me2, MovimentacaoEstoqueAcao.CRIAR, "CRIADO VIA EXPEDI��O DE VENDA " + SinedUtil.makeLinkHistorico(me2.getMovimentacaoestoqueorigem().getExpedicao().getCdexpedicao().toString(), "visualizarExpedicaoVenda"), true);
			}
			request.addMessage("Expedi��o(��es) confirmada(s) com sucesso."); 
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			request.addError("Erro na confirma��o da(s) expedi��o(��es).");
		}
		if("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao=" + whereIn);
		} else return sendRedirectToAction("listagem");
	}
	
	private void validaPodeBaixarEstoque(List<Expedicao> listaExpedicao){
		List<Expedicaoitem> listaExpedicaoitem = null;
		for (Expedicao expedicao : listaExpedicao) {
			listaExpedicaoitem = expedicao.getListaExpedicaoitem();
			if(listaExpedicaoitem != null && listaExpedicaoitem.size() > 0){
				for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
					if(expedicaoitem.getVendamaterial() == null || expedicaoitem.getVendamaterial().getVenda() == null || 
							(pedidovendatipoService.gerarExpedicaoVenda(expedicaoitem.getVendamaterial().getVenda().getPedidovendatipo(), false) &&
							 expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getVenda() != null &&
							 expedicaoitem.getVendamaterial().getVenda().getPedidovendatipo() != null &&
							(BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(expedicaoitem.getVendamaterial().getVenda().getPedidovendatipo().getBaixaestoqueEnum()) ||
							 BaixaestoqueEnum.APOS_EMISSAONOTA.equals(expedicaoitem.getVendamaterial().getVenda().getPedidovendatipo().getBaixaestoqueEnum())) &&
							 !movimentacaoestoqueService.existeMovimentacaoestoque(expedicaoitem.getVendamaterial().getVenda(), expedicao) &&
							 !movimentacaoestoqueService.existeMovimentacaoestoqueSomenteVenda(expedicaoitem.getVendamaterial().getVenda()))){
						Boolean isServico = null;
						if(expedicaoitem.getMaterial()!=null)
							isServico = Boolean.TRUE.equals(expedicaoitem.getMaterial().getServico());
						if(expedicaoitem.getListaMaterialitemmestregrade() != null && 
								!expedicaoitem.getListaMaterialitemmestregrade().isEmpty()){
							for(Expedicaoitem itemGrade : expedicaoitem.getListaMaterialitemmestregrade()){
								if(itemGrade.getQtdeexpedicao() != null && 
										itemGrade.getQtdeexpedicao() > 0){
									
									if(!isServico){		
										itemGrade.setPodeBaixarEstoque(true);
									}
								}
							}
						}else {
							materialService.validateObrigarLote(expedicaoitem);
							Double qtdeBaixar = expedicaoitem.getQuantidadePrincipal() != null?
												expedicaoitem.getQuantidadePrincipal(): expedicaoitem.getQtdeexpedicao();
							if(!localarmazenagemService.getPermitirestoquenegativo(expedicaoitem.getLocalarmazenagem())){
								expedicaoService.validaQtdeEstoque(expedicaoitem.getMaterial(), expedicao.getEmpresa(), 
										expedicaoitem.getLocalarmazenagem(), expedicaoitem.getLoteEstoque(), qtdeBaixar);
							}
							
							if(!isServico){									
								expedicaoitem.setPodeBaixarEstoque(true);
							}
						}
					}
				}
			}
		}
	}
	
	public void preencherUnidadeMedida(WebRequestContext request, Expedicaoitem expedicaoitem){
		
		String unidadeMedidaStr = "";
		
		if (expedicaoitem.getMaterial()!=null){
			Material material = materialService.findListaMaterialunidademedida(expedicaoitem.getMaterial());						
			if (material!=null){
				unidadeMedidaStr = material.getUnidademedida().getNome();
			}
		}
		
		View.getCurrent().print("var unidadeMedidaStr = '" + unidadeMedidaStr + "'");
	}
	
	public ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			String id = request.getParameter("selectedItens");
			if(id != null && !id.equals(""))
				action += "&cdexpedicao="+id;
		}
		
		return new ModelAndView("redirect:"+ action);
	}
	
	/**
	 * M�todo que abre a expedi��o para inspe��o
	 *
	 * @param request
	 * @param expedicao
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public ModelAndView inspecionarExpedicao(WebRequestContext request, Expedicao expedicao) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return getControllerModelAndView(request);
		}
		
		List<Expedicao> listaExpedicao = expedicaoService.carregaExpedicaoParaInspecao(whereIn);
		if(listaExpedicao == null || listaExpedicao.isEmpty()){
			request.addError("A expedi��o " + request.getParameter("selectedItens")+ " n�o h� itens para inspecionar.");
			return getControllerModelAndView(request);
		}
		
		Expedicao bean = new Expedicao();
		bean.setListaExpedicao(listaExpedicao);
		bean.setWhereIn(whereIn);
		boolean listaInspecao = Boolean.FALSE;
		boolean inspecaoCompleta = true;
		for(Expedicao e : bean.getListaExpedicao()){
			if(e.getInspecaocompleta() == null || !e.getInspecaocompleta()){
				inspecaoCompleta = false;
			}
			for (Expedicaoitem ei : e.getListaExpedicaoitem()) {
				List<Expedicaoiteminspecao> listaNovaInspecao = new ArrayList<Expedicaoiteminspecao>();
				if (ei.getListaInspecao() == null || ei.getListaInspecao().size() == 0){
					List<Materialinspecaoitem> listaMaterialInspecaoItem = materialinspecaoitemService.findListaMaterialInspecaoByMaterial(ei.getMaterial());
					if (!listaMaterialInspecaoItem.isEmpty() && listaMaterialInspecaoItem.size() > 0){
						listaInspecao = Boolean.TRUE;
					}
					for (Materialinspecaoitem materialinspecaoitem : listaMaterialInspecaoItem) {
						Expedicaoiteminspecao emiBean = new Expedicaoiteminspecao();
						emiBean.setInspecaoitem(materialinspecaoitem.getInspecaoitem());
						emiBean.setExpedicaoitem(ei);
						listaNovaInspecao.add(emiBean);
					}
					ei.setListaInspecao(listaNovaInspecao);
				}  else {
					listaInspecao = true;
				}
			}
		}
		if(!listaInspecao){
			request.addError("A expedi��o " + request.getParameter("selectedItens")+ " n�o h� itens para inspecionar.");
			return getControllerModelAndView(request);
		}
		
		String controller = request.getParameter("controller");
		if(controller.contains("consultar"))
			controller += "&cdexpedicao=" + request.getParameter("selectedItens");
		
		bean.setController(controller);
		bean.setInspecaocompleta(inspecaoCompleta);
		
		setInfoForTemplate(request, bean);
		setEntradaDefaultInfo(request, bean);
		
		if(bean.getInspecaocompleta() != null && bean.getInspecaocompleta() && request.getParameter("editMode") == null)
			request.setAttribute(CONSULTAR, true);
		return new ModelAndView("/process/inspecaoExpedicao", "inspecaoExpedicaoBean", bean);
	}
	
	/**
	 * M�todo que salva a inspe��o da expedi��o
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public ModelAndView salvaInspecao(WebRequestContext request, Expedicao bean){
		if(!bean.getInspecaocompleta()){
			expedicaoService.verificaInspecaoCompleta(bean);
		}
		
		expedicaoService.salvaInspecao(bean);
		
		if(bean.getInspecaocompleta())
			request.addMessage("Inspe��o completa para a(s) expedi��o(�es) " + bean.getWhereIn()+ ".", MessageType.INFO);
		
		request.addMessage("Registro salvo com sucesso.", MessageType.INFO);
		return new ModelAndView("redirect:" + bean.getController());
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public ModelAndView adicionarVenda(WebRequestContext request, ExpedicaoFiltro filtro){
		
		Integer cdexpedicao = null;
		if(Boolean.TRUE.equals(SinedUtil.isRecapagem())){
			request.addError("Fun��o indispon�vel para recapagem .");
			SinedUtil.fechaPopUp(request);
			return sendRedirectToAction("listagem");
		}
		
		if(filtro==null || filtro.getCdexpedicao()==null){
			filtro = new ExpedicaoFiltro();
			String whereIn = SinedUtil.getItensSelecionados(request);
			cdexpedicao = Integer.parseInt(whereIn);
			
			boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_ABERTO);
			
			if(notValido){
				request.addError("A adi��o de vendas s� podem ser realizadas em expedi��es com situa��o 'EM ABERTO'.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		try {
			if(cdexpedicao != null) {
				filtro.setCdexpedicao(cdexpedicao);
				Expedicao expedicao = expedicaoService.loadForEntrada(new Expedicao(cdexpedicao));
				filtro.setEmpresa(expedicao.getEmpresa());
			}
			
			boolean adicionandoVenda = filtro.getVenda()!=null && filtro.getVenda().getCdvenda() != null;
			boolean adicionandoNota = filtro.getNota()!=null && filtro.getNota().getCdNota() != null;
			boolean importandoCsv = filtro.getArquivo()!=null && filtro.getArquivo().getContent() != null;
			
			if(adicionandoVenda || adicionandoNota || importandoCsv){
				
				request.setAttribute("isNotaCarregada", true);
				request.setAttribute("isVendaCarregada", true);
				
				filtro.setListaVendas(new ArrayList<Venda>());
				if(adicionandoVenda){
					filtro.getListaVendas().add(filtro.getVenda());
				}else if(adicionandoNota){
					List<Venda> vendasNota = vendaService.findVendasByNota(filtro.getNota().getCdNota());
					filtro.getListaVendas().addAll(vendasNota);
				}else if(importandoCsv){
					String[] csv = new String(filtro.getArquivo().getContent()).split("\\r?\\n");
					StringBuilder whereInVenda = new StringBuilder();
					StringBuilder whereInNota = new StringBuilder();

					int i = 0;
					for(String linhaCsv: csv){
						if(i==0){
							i++;
							continue;
						}
						String[] arrayColunas = linhaCsv.split(";");
						
						String codigo = arrayColunas[0].replace("\"", "");
						if(StringUtils.isNotEmpty(codigo)){
							whereInNota.append(codigo+"\n");
						}else{
							codigo = arrayColunas[1]!=null? arrayColunas[1].replace("\"", ""): "";
							if(StringUtils.isNotEmpty(codigo)){
								whereInVenda.append(codigo+"\n");
							}
						}
						i++;
					}
					
					String cdempresa = (filtro.getEmpresa() != null ? filtro.getEmpresa().getCdpessoa().toString() : null);
					
					if(whereInVenda.length()>0){
						String whereIn = whereInVenda.toString().replace("\n", ",");;
						whereIn = whereIn.substring(0, whereIn.length()-1);
						List<Venda> vendas = vendaService.findForExpedicao(whereIn, cdempresa);
						filtro.getListaVendas().addAll(vendas);
					}

					if(whereInNota.length()>0){
						String whereIn = whereInNota.toString().replace("\n", ",");
						whereIn = whereIn.substring(0, whereIn.length()-1);
						
						List<Venda> vendasNota = vendaService.findVendaWithNotaForExpedicao(whereIn, cdempresa);
						for(Venda venda: vendasNota){
							if(!filtro.getListaVendas().contains(venda))
								filtro.getListaVendas().add(venda);
						}
					}
				
				}
				
				carregaQtdeAdicaoVenda(request, filtro);
			}
			
			return new ModelAndView("direct:crud/popup/adicionarVendaExpedicao").addObject("filtro", filtro);			
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro no processamento da expedi��o.");
		}
		
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView removerNotasAssociadas (WebRequestContext request, ExpedicaoFiltro filtro){
		if(filtro.getCdexpedicao() ==null){
			request.addError("Erro ao salvar a expedi��o, por favor tente novamente.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if(filtro.getListaNotasDaExpedicao() !=null){
			Expedicao expedicao = new Expedicao(filtro.getCdexpedicao());
			expedicao = expedicaoService.loadForEntrada(expedicao);
			List<Expedicaoitem> listaItens = expedicao.getListaExpedicaoitem(); 
			StringBuffer obsParaHistorico = new StringBuffer();
			obsParaHistorico.append("Removidas as notas ");
			List<Expedicaoitem>listaRemoverItens = new ArrayList<Expedicaoitem>();
			String obsParaNota = "Removida da expedi��o <a href='/w3erp/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao="+expedicao.getCdexpedicao()+"'>"+expedicao.getCdexpedicao()+"</a>&nbsp;";
			for (Notafiscalproduto nota : filtro.getListaNotasDaExpedicao()) {
				Iterator<Expedicaoitem> it = listaItens.iterator();
				while(it.hasNext()){
					Expedicaoitem item = it.next();
					if(item.getNotaFiscalProdutoItem()!=null && item.getNotaFiscalProdutoItem().getNotafiscalproduto()!=null && item.getNotaFiscalProdutoItem().getNotafiscalproduto().equals(nota)){
						listaRemoverItens.add(item);
					}
				}
				obsParaHistorico.append(" <a href='/w3erp/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota="+nota.getCdNota()+"'>"+nota.getCdNota()+"</a>");
				notafiscalprodutoService.atualizaExpedicao(obsParaNota,SituacaoVinculoExpedicao.PENDENTE,nota,NotaStatus.REMOVIDA);
			}
			listaItens.removeAll(listaRemoverItens);
			expedicaoService.saveOrUpdate(expedicao);
			Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
			expedicaohistorico.setExpedicao(expedicao);
			expedicaohistorico.setObservacao(obsParaHistorico.toString().trim());
			expedicaohistorico.setExpedicaoacao(Expedicaoacao.ALTERADA);
			expedicaohistoricoService.saveOrUpdate(expedicaohistorico);		
		}else{
			request.addError("Nenhuma nota foi selecionada.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		request.addMessage("Nota(s) removida(s) com sucesso da expedi��o "+filtro.getCdexpedicao()+".");
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView confirmarAdicaoNota (WebRequestContext request, ExpedicaoFiltro filtro){
		if(filtro.getCdexpedicao() ==null){
			request.addMessage("Erro ao salvar a expedi��o, por favor tente novamente.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		Expedicao expedicao = new Expedicao(filtro.getCdexpedicao());
		expedicao = expedicaoService.loadForEntrada(expedicao);
		if(SinedUtil.isListNotEmpty(filtro.getListaExpedicaoItem())){
			if(filtro.getListaExpedicaoItem().get(0).getNotaFiscalProdutoItem().getNotafiscalproduto() !=null ){
				Notafiscalproduto nota = filtro.getListaExpedicaoItem().get(0).getNotaFiscalProdutoItem().getNotafiscalproduto();
				expedicao.getListaExpedicaoitem().addAll(filtro.getListaExpedicaoItem());
				expedicaoService.saveOrUpdate(expedicao);
				String obsParaHistorico = "Adicionada a expedi��o <a href='/w3erp/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao="+expedicao.getCdexpedicao()+"'>"+expedicao.getCdexpedicao()+"</a> ";
				notafiscalprodutoService.atualizaExpedicao(obsParaHistorico,SituacaoVinculoExpedicao.EXPEDI��O_COMPLETA,nota,NotaStatus.ADICIONADA);
				Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
				expedicaohistorico.setExpedicao(expedicao);
				expedicaohistorico.setObservacao("Adicionada a nota <a href='/w3erp/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota="+nota.getCdNota()+"'>"+nota.getCdNota()+"</a>");
				expedicaohistorico.setExpedicaoacao(Expedicaoacao.ALTERADA);
				expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
			}else{
				request.addMessage("Erro ao salvar a expedi��o, notas n�o encontradas.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}else{
			request.addMessage("Erro ao salvar a expedi��o, notas n�o encontradas.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		request.addMessage("A nota foi adicionada com sucesso na expedi��o.");
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	/**
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 */
	public ModelAndView confirmarAdicaoVenda (WebRequestContext request, ExpedicaoFiltro filtro){
		
		if(filtro.getListaVendamaterial()!=null && !filtro.getListaVendamaterial().isEmpty() && filtro.getCdexpedicao()!=null 
				&& ((filtro.getVenda()!=null && filtro.getVenda().getCdvenda()!=null) || (filtro.getArquivo()!=null) ||
						(filtro.getNota()!=null && filtro.getNota().getCdNota()!=null))){
			
			Integer ordemEntrega = expedicaoService.getOrdemEntregaByExpedicao(new Expedicao(filtro.getCdexpedicao())) + 1;
			
			List<Vendamaterial> listaVendaMaterial = new ArrayList<Vendamaterial>();
			for (Vendamaterial vendamaterial : filtro.getListaVendamaterial()) {
				if(vendamaterial.getIsAdicionarExpedicao() && vendamaterial.getQtdeExpedicao()>0)
					listaVendaMaterial.add(vendamaterial);
			}
			
			if(listaVendaMaterial.isEmpty()){
				request.addError("Para Adicionar Venda a Expedi��o, � necess�rio selecionar pelo menos 1 item da venda com Qtde de Expedi��o maior que zero.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			for (Vendamaterial vendamaterial : listaVendaMaterial) {
				if(vendamaterial.getQuantidade()!=null && vendamaterial.getQtdeExpedicao()!=null && vendamaterial.getQtdeExpedida()!=null){
					if((vendamaterial.getQtdeExpedicao() > vendamaterial.getQuantidade() - vendamaterial.getQtdeExpedida()) ||
							((vendamaterial.getQuantidade() - (vendamaterial.getQtdeExpedida() + vendamaterial.getQtdeExpedicao())) < 0)){
						request.addError("A quantidade de produtos expedidos n�o pode ultrapassar a quantidade de produtos da venda.");
						SinedUtil.fechaPopUp(request);
						return null;
					}
				}else{
					request.addError("Erro no processamento da expedi��o.");
					SinedUtil.fechaPopUp(request);
					return null;
				}
			}
			
			HashMap<Integer, Venda> mapaVendas = new HashMap<Integer, Venda>();
			
			List<Expedicaoitem> listaExpedicaoitem = new ArrayList<Expedicaoitem>();
			Expedicao expedicao = new Expedicao(filtro.getCdexpedicao());
			for (Vendamaterial vendamaterial : listaVendaMaterial) {				
				if(vendamaterial.getQuantidade()!=null && vendamaterial.getQtdeExpedicao()!=null && vendamaterial.getQtdeExpedida()!=null 
						&& vendamaterial.getQtdeExpedicao()>0){
					
					Double qtdeRestane = vendamaterial.getQuantidade() - (vendamaterial.getQtdeExpedida() + vendamaterial.getQtdeExpedicao());
					Expedicaoitem expedicaoitem = new Expedicaoitem();
					expedicaoitem.setCliente(vendamaterial.getVenda().getCliente());
					expedicaoitem.setCodigovenda(vendamaterial.getVenda().getCdvenda());
					expedicaoitem.setDtexpedicaoitem(SinedDateUtils.currentDate());
					expedicaoitem.setMaterial(vendamaterial.getMaterial());
					expedicaoitem.setQtdeexpedicao(vendamaterial.getQtdeExpedicao());
					expedicaoitem.setQtderestante(qtdeRestane);
					expedicaoitem.setQtdevendida(vendamaterial.getQuantidade());
					expedicaoitem.setVendamaterial(vendamaterial);
					expedicaoitem.setExpedicao(expedicao);
					expedicaoitem.setOrdementrega(ordemEntrega);
					expedicaoitem.setLoteEstoque(vendamaterial.getLoteestoque());
					
					if(vendamaterial.getVenda().getContato() != null && vendamaterial.getVenda().getContato().getCdpessoa() != null){
						expedicaoitem.setContatocliente(vendamaterial.getVenda().getContato());
					}
					
					if(vendamaterial.getVenda().getLocalarmazenagem() != null && vendamaterial.getVenda().getLocalarmazenagem().getCdlocalarmazenagem() != null ){
						expedicaoitem.setLocalarmazenagem(vendamaterial.getVenda().getLocalarmazenagem());
					}
					
					if(vendamaterial.getMaterial()!=null && vendamaterial.getMaterial().getUnidademedida()!=null){
						expedicaoitem.setUnidademedida(vendamaterial.getMaterial().getUnidademedida());
					}
					
					listaExpedicaoitem.add(expedicaoitem);
					
					if(!mapaVendas.containsKey(vendamaterial.getVenda().getCdvenda())){
						mapaVendas.put(vendamaterial.getVenda().getCdvenda(), vendamaterial.getVenda());
					}
				}
			}
			
			try {
				
				String whereInVendas = CollectionsUtil.listAndConcatenate(mapaVendas.values(), "cdvenda", ",");
				expedicaoitemService.confirmarAdicaoVenda(listaExpedicaoitem);
				StringBuilder obs = new StringBuilder();
				for(Venda venda: mapaVendas.values()){
					obs.append("Venda adicionada a expedi��o: ");
					obs.append("<a href=\"javascript:visualizaVenda(" + venda.getCdvenda() + ");\"> " + venda.getCdvenda() + "</a>.");
					
					expedicaohistoricoService.saveHistorico(filtro.getCdexpedicao().toString(), obs.toString(), Expedicaoacao.VENDA_ADICIONADA);
					vendahistoricoService.makeHistoricoExpedicaoByVendas(expedicao, whereInVendas);
					
					obs.setLength(0);
				}
				vendaService.atualizarExpedicaoSituacao(whereInVendas);
				
				request.addMessage("A venda foi adicionada com sucesso na expedi��o.");
				SinedUtil.fechaPopUp(request);
				return null;				
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("Erro na gera��o das adi��es de venda � expedi��o.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
		}else{
			request.addError("Par�metros Inv�lidos.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 */
	private void carregaQtdeAdicaoVenda (WebRequestContext request, ExpedicaoFiltro filtro){		
		List<Vendamaterial> listaItensVenda = new ArrayList<Vendamaterial>();
		String msg = "";
		
		List<Venda> vendas = new ArrayList<Venda>();
		if(filtro.getListaVendas()!=null && !filtro.getListaVendas().isEmpty()){
			vendas.addAll(filtro.getListaVendas());
		}else if(filtro.getVenda()!=null && filtro.getVenda().getCdvenda()!=null){
			vendas.add(filtro.getVenda());
		}
		
		for(Venda venda: vendas){
			venda = vendaService.loadWithVendamaterial(venda);
			if(venda!=null && venda.getListavendamaterial()!=null && !venda.getListavendamaterial().isEmpty()){
				
				//SOMAT�RIO DAS QTDES EXPEDIDAS.
				for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
					List<Expedicaoitem> listaExpedicaoitem = vendamaterial.getListaExpedicaoitem();
					Double quantidade = vendamaterial.getQuantidade();
					Double qtde = 0D;
					if(listaExpedicaoitem!=null && !listaExpedicaoitem.isEmpty() && quantidade!=null && quantidade>0){	
						Material materialvenda = vendamaterial.getMaterial();
						for (Expedicaoitem expedicaoitem : listaExpedicaoitem){
							Material materialexpedicao = expedicaoitem.getMaterial();
							if(validaQtdExpedicao(filtro, materialvenda, expedicaoitem, materialexpedicao)){
								qtde += expedicaoitem.getQtdeexpedicao()!=null ? expedicaoitem.getQtdeexpedicao() : 0D;	
							}
						}
						vendamaterial.setQtdeExpedida(qtde);
						vendamaterial.setQtdeExpedicao(quantidade-qtde);
						vendamaterial.setVenda(venda);
					}
				}		
				
				//ADICIONANDO E VALIDANDO OS MATERIAIS DA VENDA A LISTA DE PRODUTOS
				for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
					if(vendamaterial.getQuantidade()!=null && vendamaterial.getQuantidade()>0){
						Double qtdeExpedida = vendamaterial.getQtdeExpedida()!=null ? vendamaterial.getQtdeExpedida() : 0D;
						Double qtdeExpedicao = vendamaterial.getQtdeExpedicao()!=null ? vendamaterial.getQtdeExpedicao() : 0D;
						vendamaterial.setQtdeExpedicao(qtdeExpedicao);
						vendamaterial.setQtdeExpedida(qtdeExpedida);
						vendamaterial.setVenda(venda);
						converterQtdsVendaMaterial(vendamaterial);
						if(vendamaterial.getQuantidade() - vendamaterial.getQtdeExpedida() > 0){
							vendamaterial.setUnidademedida(vendamaterial.getMaterial().getUnidademedida());								
							listaItensVenda.add(vendamaterial);							
						}
					}
				}
			}			
		}
		
		if(listaItensVenda.isEmpty()){
			msg = "N�o existem itens vinculados � venda dispon�veis para expedi��o.";
			request.setAttribute("isVendaCarregada", false);
		}
		
		request.setAttribute("listaItensVenda", listaItensVenda);
		request.setAttribute("msg", msg);
	}
	
	/**
	 * 
	 * @param filtro
	 * @param materialvenda
	 * @param expedicaoitem
	 * @param materialexpedicao
	 * @return
	 */
	private boolean validaQtdExpedicao(ExpedicaoFiltro filtro, Material materialvenda, Expedicaoitem expedicaoitem, Material materialexpedicao) {
		 if(materialvenda==null || materialvenda.getCdmaterial()==null)
			 return false;
		 else if(materialexpedicao==null || materialexpedicao.getCdmaterial()==null )
			 return false;
		 else if(!materialvenda.getCdmaterial().equals(materialexpedicao.getCdmaterial()))
			 return false;
		 else if (expedicaoitem.getExpedicao()==null || expedicaoitem.getExpedicao().getCdexpedicao()==null) 
			 return false;
		 else if (filtro.getCdexpedicao() == expedicaoitem.getExpedicao().getCdexpedicao())
			 return false;
		 
		 return true;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	public ModelAndView unificar(WebRequestContext request) throws Exception{
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_ABERTO);
		
		if(notValido){
			request.addError("Para unificar as expedi��es � necess�rio que todas estejam com situa��o 'Em ABERTO'.");
			return sendRedirectToAction("listagem");
		}
		
		List<Expedicao> lista = expedicaoService.findReservaByExpedicoes(whereIn);
		if(SinedUtil.isListNotEmpty(lista)){
			String expedicoes = CollectionsUtil.listAndConcatenate(lista, "cdexpedicao", ",");
			request.addError("N�o foi poss�vel unificar as expedi��es selecionadas, pois " + 
					(lista.size() > 1 ? " as expedi��es "+expedicoes+" possuem itens reservados." : " a expedi��o "+expedicoes+" possui itens reservados."));
			return sendRedirectToAction("listagem");
		}
		
		return new ModelAndView("redirect:/faturamento/crud/Expedicao?ACAO=criar&whereInUnificacaoExpedicao="+whereIn);
	}
	
	/**
	 * 
	 * @param request
	 * @param expedicaoitem
	 * @return
	 */
	public ModelAndView ajaxConverteUnidademedida(WebRequestContext request, Expedicaoitem expedicaoitem){
		
		MaterialunidademedidaVO materialunidademedidaVO = converteUnidademedida(expedicaoitem.getQtdeexpedicao(), 
																				expedicaoitem.getMaterial(),
																				expedicaoitem.getUnidademedida(),
																				expedicaoitem.getUnidademedidaAntiga());
		
		return new JsonModelAndView().addObject("qtde", SinedUtil.descriptionDecimal(materialunidademedidaVO.getQtde()))
									.addObject("qtdereferencia", materialunidademedidaVO.getQtdereferencia())
									.addObject("fatorconversao", materialunidademedidaVO.getFatorconversao());
	}
	
	/**
	 * 
	 * @param qtde
	 * @param material
	 * @param unidademedida
	 * @param unidademedidaAntiga
	 * @return 
	 */
	private MaterialunidademedidaVO converteUnidademedida (Double qtde, Material material, Unidademedida unidademedida, Unidademedida unidademedidaAntiga){
		
		material = materialService.findListaMaterialunidademedida(material);
		
		MaterialunidademedidaVO materialunidademedidaVOAntigo = unidademedidaService.getFracaoConversaoUnidademedidaForVO(material, unidademedidaAntiga);
		MaterialunidademedidaVO materialunidademedidaVO = unidademedidaService.getFracaoConversaoUnidademedidaForVO(material, unidademedida);
		
		Double fracao1 = materialunidademedidaVOAntigo!=null ? materialunidademedidaVOAntigo.getFracao() : null;
		Double fracao2 = materialunidademedidaVO!=null ? materialunidademedidaVO.getFracao() : null;
		
		if(fracao1 != null && fracao2 != null && materialunidademedidaVOAntigo.getQtdereferencia() < materialunidademedidaVO.getQtdereferencia()){
			qtde = (qtde * fracao1) / fracao2;
		}else if(fracao1 != null && fracao2 != null && materialunidademedidaVO.getQtdereferencia() >= materialunidademedidaVOAntigo.getQtdereferencia()){
			qtde = (qtde * fracao2) / fracao1;
		}else {
			Double fracao = null;
			
			List<Unidademedidaconversao> listaUnidademedidaconversao = unidademedidaconversaoService.conversoesByUnidademedida(unidademedida);		
			if(listaUnidademedidaconversao != null && listaUnidademedidaconversao.size() > 0){
				for (Unidademedidaconversao item : listaUnidademedidaconversao) {
					if(item.getUnidademedida() != null && item.getUnidademedida().getCdunidademedida() != null && item.getFracao() != null){
						if(item.getUnidademedidarelacionada().equals(unidademedida)){
							fracao = item.getFracaoQtdereferencia();
							break;
						}
					}
				}
			}
			
			if(fracao != null && fracao > 0){
				qtde = qtde / fracao;
			}
		}
		
		materialunidademedidaVO.setQtde(qtde);
		
		return materialunidademedidaVO;
	}
	
	private void converterQtdsVendaMaterial(Vendamaterial vendamaterial){
		
		if(vendamaterial.getUnidademedida()!=null && vendamaterial.getMaterial()!=null && 
				vendamaterial.getMaterial().getUnidademedida()!=null && 
				!vendamaterial.getUnidademedida().equals(vendamaterial.getMaterial().getUnidademedida())){		
			
			MaterialunidademedidaVO materialunidademedidaVO = null;
			
			materialunidademedidaVO = converteUnidademedida(vendamaterial.getQuantidade(), 
															vendamaterial.getMaterial(), 
															vendamaterial.getUnidademedida(),
															vendamaterial.getMaterial().getUnidademedida());
			vendamaterial.setQuantidade(materialunidademedidaVO.getQtde());
			
			materialunidademedidaVO = converteUnidademedida(vendamaterial.getQtdeExpedida(), 
															vendamaterial.getMaterial(), 
															vendamaterial.getUnidademedida(),
															vendamaterial.getMaterial().getUnidademedida());
			vendamaterial.setQtdeExpedida(materialunidademedidaVO.getQtde());
			
			materialunidademedidaVO = converteUnidademedida(vendamaterial.getQtdeExpedicao(), 
															vendamaterial.getMaterial(), 
															vendamaterial.getUnidademedida(),
															vendamaterial.getMaterial().getUnidademedida());
			vendamaterial.setQtdeExpedicao(materialunidademedidaVO.getQtde());
		}
	}
	
	public ModelAndView abrirConfirmacaoAgrupamento(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		if(expedicaoService.existExpedicaoNotConfirmada(whereIn)){
			request.addError("Para gerar nota, a(s) expedi��o(�es) deve(m) estar com situa��o 'CONFIRMADA'.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao");
			return null;
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		request.setAttribute("entrada", entrada);
		request.setAttribute("selectedItens", whereIn);
		
		List<Expedicao> listaExpedicao = expedicaoService.findForNota(whereIn);
		if(expedicaoService.verificarVinculoNotaProduco(listaExpedicao)){
			request.addError("N�o � permitido gerar nota de expedi��o que comp�e itens de nota fiscal.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao");
			return null;
		}
		boolean existVariasVendas = expedicaoService.existVariasVendas(listaExpedicao);
		if(existVariasVendas){
			String cnpj = null;
			for(Expedicao expedicao : listaExpedicao){
				if(SinedUtil.isListNotEmpty(expedicao.getListaExpedicaoitem())){
					for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
						if(expedicaoitem.getCliente() != null && expedicaoitem.getCliente().getCnpj() != null){
							cnpj = expedicaoitem.getCliente().getCnpj().getValue();
							break;
						}
					}
					if(cnpj != null) break;
				}
			}
			request.setAttribute("cnpjCliente", cnpj);
			request.setAttribute("mesmaMatriz", cnpj != null && expedicaoService.mesmaMatrizExpedicao(whereIn));
			return new ModelAndView("direct:/crud/popup/confirmacaoAgrupamentoExpedicao");
		} else {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/crud/Notafiscalproduto?ACAO=" + CRIAR + "&whereInexpedicao=" + whereIn+"';</script>");
			return null;
		}
	}
	
	public ModelAndView ajaxBuscaClienteAgrupamentoMatriz(WebRequestContext request){
		List<ClienteVO> listaCliente = clienteService.findByMatrizCNPJ(new Cnpj(request.getParameter("cnpjCliente")));  
		return new JsonModelAndView().addObject("listaCliente", listaCliente);
	}
	
	public ModelAndView gerarFaturamento(WebRequestContext request){
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		Boolean agrupamento = Boolean.valueOf(request.getParameter("agrupamento") != null ? request.getParameter("agrupamento") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		String cdcliente = request.getParameter("cdcliente");
		
		try{
			List<Expedicao> listaExpedicao = expedicaoService.findForNota(whereIn);
			listaExpedicao = expedicaoService.createListaExpedicaoForNota(listaExpedicao);
			
			if(cdcliente != null && !"".equals(cdcliente) && !"null".equals(cdcliente)){
				Cliente cliente = new Cliente(Integer.parseInt(cdcliente));
				cliente = clienteService.loadForEntrada(cliente);
				
				for (Expedicao expedicao : listaExpedicao) {
					expedicao.setCliente(cliente);
				}
			}
			
			notafiscalprodutoService.gerarFaturamentoExpedicao(request, listaExpedicao, agrupamento);
			expedicaoService.updateSituacao(whereIn, Expedicaosituacao.FATURADA);
			request.addMessage("Faturamento(s) gerado(s) com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao" + (entrada ? "?ACAO=consultar&cdvenda="+whereIn : ""));
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView conferenciaMateriais(WebRequestContext request, Expedicao expedicao) throws Exception {
		String whereIn = request.getParameter("whereIn");
		boolean fromVenda = "true".equalsIgnoreCase(request.getParameter("fromVenda"));
		
		
		Expedicaosituacao[] expedicaoSituacoes = new Expedicaosituacao[] {};
		if(expedicaoService.isUsaSeparacaoConferenciaMateriais()){
			expedicaoSituacoes = new Expedicaosituacao[2];
			expedicaoSituacoes[0] = Expedicaosituacao.SEPARACAO_REALIZADA;
			expedicaoSituacoes[1] = Expedicaosituacao.EM_CONFERENCIA;
		}else {
			expedicaoSituacoes = new Expedicaosituacao[1];
			expedicaoSituacoes[0] = Expedicaosituacao.EM_ABERTO;
		}
		
		if(!Boolean.TRUE.equals(expedicao.getGerandoPelaVenda())){
			boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, expedicaoSituacoes);
			if(notValido){
				String mensagemSituacao = "";
				for(Expedicaosituacao expedicaosituacao : expedicaoSituacoes){
					if(mensagemSituacao.length() > 0) mensagemSituacao += ", ";
					mensagemSituacao += "'" + expedicaosituacao.getNome() + "' ";
				}
				request.addError("So � permitido conferir expedi��es com situa��o "+ mensagemSituacao +".");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		List<Vendamaterial> listaVendamaterial = null;
		List<Expedicaoitem> listaExpedicaoitem = null;
		if (fromVenda){
			listaVendamaterial = vendamaterialService.findForConferenciaMateriaisFromVenda(whereIn);
		}else {
			listaExpedicaoitem = expedicaoitemService.findForConferenciaMateriaisFromExpedicao(whereIn);
		}
		
		ConferenciaMateriaisBean bean = new ConferenciaMateriaisBean();
		List<ConferenciaMateriaisVendaBean> listaVenda = new ArrayList<ConferenciaMateriaisVendaBean>();
		List<ConferenciaMateriaisVendaBean> listaAllVenda = new ArrayList<ConferenciaMateriaisVendaBean>();
		Map<Integer, List<ConferenciaMateriaisVendaBean>> mapa = new LinkedHashMap<Integer, List<ConferenciaMateriaisVendaBean>>();
		int index = 0;
		Integer idMapItemAvulso = 0;
		if(SinedUtil.isListNotEmpty(listaVendamaterial)){
			//Cria um mapa com o material e suas unidades de medida para o caso de aparecer o mesmo material
			//mais de uma vez, com unidades de medidas diferentes
			Map<Material, List<Unidademedida>> mapaMaterialUnidadesMedida = new HashMap<Material, List<Unidademedida>>();
			for (Vendamaterial vendamaterial: listaVendamaterial){
				List<Unidademedida> listaUnidadeMedidas = new ArrayList<Unidademedida>();
				if(!mapaMaterialUnidadesMedida.containsKey(vendamaterial.getMaterial())){
					listaUnidadeMedidas.add(vendamaterial.getUnidademedida());
					mapaMaterialUnidadesMedida.put(vendamaterial.getMaterial(), listaUnidadeMedidas);
				}else {
					listaUnidadeMedidas = mapaMaterialUnidadesMedida.get(vendamaterial.getMaterial());
					if(!listaUnidadeMedidas.contains(vendamaterial.getUnidademedida())){
						listaUnidadeMedidas.add(vendamaterial.getUnidademedida());
					}
					mapaMaterialUnidadesMedida.put(vendamaterial.getMaterial(), listaUnidadeMedidas);					
				}
			}			
			
			for (Vendamaterial vendamaterial: listaVendamaterial){	
				if(mapaMaterialUnidadesMedida.get(vendamaterial.getMaterial()).size() > 1 && SinedUtil.isListNotEmpty(vendamaterial.getMaterial().getListaEmbalagem())){
					String codigoBarraEmbalagens = this.montaStringCodigosEmbalagem(vendamaterial.getMaterial().getListaEmbalagem(), vendamaterial.getUnidademedida());
					vendamaterial.setCodigoBarrasEmbalagem(codigoBarraEmbalagens);
					vendamaterial.setUnidadesMedidaDiferentesConferenciaExpedicao(true);
				}else if(SinedUtil.isListNotEmpty(vendamaterial.getMaterial().getListaEmbalagem())){
					String codigoBarraEmbalagens = this.montaStringCodigosEmbalagem(vendamaterial.getMaterial().getListaEmbalagem(), null);
					vendamaterial.setCodigoBarrasEmbalagem(codigoBarraEmbalagens);
					vendamaterial.setUnidadesMedidaDiferentesConferenciaExpedicao(false);
				}
				
				//verifica a quantidade de itens j� expedidos para apresentar apenas a quantidade restante na tela de conferencia
				if(SinedUtil.isListNotEmpty(vendamaterial.getListaExpedicaoitem())){
					Double qtdExpedida = 0D;
					for (Expedicaoitem expedicaoitem : vendamaterial.getListaExpedicaoitem()) {
						if(!Expedicaosituacao.CANCELADA.equals(expedicaoitem.getExpedicao().getExpedicaosituacao())){
							qtdExpedida += expedicaoitem.getQtdeexpedicao();							
						}
					}
					if(vendamaterial.getQuantidade() <= qtdExpedida){
						continue;
					}else{
						vendamaterial.setQuantidade(vendamaterial.getQuantidade() - qtdExpedida);
					}
				}
				
				//verifica se a unidade de medida do item � a principal do material
				if(unidademedidaService.findByMaterial(vendamaterial.getMaterial()).equals(vendamaterial.getUnidademedida())){
					vendamaterial.setUnidadeMedidaPrincipalMaterial(true);
				}
				
				listaVenda = mapa.get(vendamaterial.getVenda().getCdvenda());
				if (listaVenda==null){
					listaVenda = new ArrayList<ConferenciaMateriaisVendaBean>();
				}
				listaVenda.add(new ConferenciaMateriaisVendaBean(index++, vendamaterial));
				mapa.put(vendamaterial.getVenda().getCdvenda(), listaVenda);
			}
		}else {
			if(CollectionsUtil.isListNotEmpty(listaExpedicaoitem)){
				//Cria um mapa com o material e suas unidades de medida para o caso de aparecer o mesmo material
				//mais de uma vez, com unidades de medidas diferentes
				Map<Material, List<Unidademedida>> mapaMaterialUnidadesMedida = new HashMap<Material, List<Unidademedida>>();
				for (Expedicaoitem expedicaoitem: listaExpedicaoitem){
					List<Unidademedida> listaUnidadeMedidas = new ArrayList<Unidademedida>();
					if(!mapaMaterialUnidadesMedida.containsKey(expedicaoitem.getMaterial())){
						listaUnidadeMedidas.add(expedicaoitem.getUnidademedida());
						mapaMaterialUnidadesMedida.put(expedicaoitem.getMaterial(), listaUnidadeMedidas);
					}else {
						listaUnidadeMedidas = mapaMaterialUnidadesMedida.get(expedicaoitem.getMaterial());
						if(!listaUnidadeMedidas.contains(expedicaoitem.getUnidademedida())){
							listaUnidadeMedidas.add(expedicaoitem.getUnidademedida());
						}
						mapaMaterialUnidadesMedida.put(expedicaoitem.getMaterial(), listaUnidadeMedidas);					
					}
				}
				
				for (Expedicaoitem expedicaoitem: listaExpedicaoitem){
					if(mapaMaterialUnidadesMedida.get(expedicaoitem.getMaterial()).size() > 1 && SinedUtil.isListNotEmpty(expedicaoitem.getMaterial().getListaEmbalagem())){
						String codigoBarraEmbalagens = this.montaStringCodigosEmbalagem(expedicaoitem.getMaterial().getListaEmbalagem(), expedicaoitem.getUnidademedida());
						expedicaoitem.setCodigoBarrasEmbalagem(codigoBarraEmbalagens);
						expedicaoitem.setUnidadesMedidaDiferentesConferenciaExpedicao(true);
					}else if(SinedUtil.isListNotEmpty(expedicaoitem.getMaterial().getListaEmbalagem())){
						String codigoBarraEmbalagens = this.montaStringCodigosEmbalagem(expedicaoitem.getMaterial().getListaEmbalagem(), null);
						expedicaoitem.setCodigoBarrasEmbalagem(codigoBarraEmbalagens);
						expedicaoitem.setUnidadesMedidaDiferentesConferenciaExpedicao(false);
					}
					
					//verifica se a unidade de medida do item � a principal do material
					if(unidademedidaService.findByMaterial(expedicaoitem.getMaterial()).equals(expedicaoitem.getUnidademedida())){
						expedicaoitem.setUnidadeMedidaPrincipalMaterial(true);
					}
					
					if(expedicaoitem.getVendamaterial() == null || expedicaoitem.getVendamaterial().getVenda() == null){
						listaVenda = mapa.get(idMapItemAvulso);
						if (listaVenda==null){
							listaVenda = new ArrayList<ConferenciaMateriaisVendaBean>();
						}
						listaVenda.add(new ConferenciaMateriaisVendaBean(index++, expedicaoitem));
						mapa.put(idMapItemAvulso, listaVenda);
					}else {
						listaVenda = mapa.get(expedicaoitem.getVendamaterial().getVenda().getCdvenda());
						if (listaVenda==null){
							listaVenda = new ArrayList<ConferenciaMateriaisVendaBean>();
						}
						listaVenda.add(new ConferenciaMateriaisVendaBean(index++, expedicaoitem));
						mapa.put(expedicaoitem.getVendamaterial().getVenda().getCdvenda(), listaVenda);
					}
					
				}
			}
		}
		for (Entry<Integer, List<ConferenciaMateriaisVendaBean>> entry : mapa.entrySet()) {
			 listaAllVenda.addAll(entry.getValue());
		}
		for (ConferenciaMateriaisVendaBean conferenciaMateriaisVendaBean : listaAllVenda) {
			if(conferenciaMateriaisVendaBean.getExpedicaoitem() != null){
				Expedicaoitem expedicaoItem = conferenciaMateriaisVendaBean.getExpedicaoitem();
				Boolean ignorarQuantidadeConferidaAnteriormente = expedicaoItem.getExpedicao() != null && Expedicaosituacao.SEPARACAO_REALIZADA.equals(expedicaoItem.getExpedicao().getExpedicaosituacao());
				if(!ignorarQuantidadeConferidaAnteriormente){
					ConferenciaMaterialExpedicao conferenciaMaterialExpedicao = conferenciaMaterialExpedicaoService.getLastEmConferenciaByExpedicaoitem(conferenciaMateriaisVendaBean.getExpedicaoitem());
					if(conferenciaMaterialExpedicao != null){
						conferenciaMateriaisVendaBean.setQtdeConferida(conferenciaMaterialExpedicao.getQtdeConferida());
						conferenciaMateriaisVendaBean.setQtdeConferidaSemArredondamento(conferenciaMaterialExpedicao.getQtdeConferida());
					}
				}
			}
		}
		
		bean.setListaConferencia(SinedUtil.listToSet(listaAllVenda, ConferenciaMateriaisVendaBean.class));
		
		request.setAttribute("whereIn", whereIn);
		request.setAttribute("fromVenda", fromVenda);
		request.setAttribute("bean", bean);
		request.setAttribute("mapa", mapa);
		request.setAttribute("qtdeMateriais", index);
		request.setAttribute("CONSIDERAR_CODIGO_NA_CONFERENCIA_EXPEDICAO", parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_CODIGO_NA_CONFERENCIA_EXPEDICAO));
		
		if(expedicaoService.isUsaSeparacaoConferenciaMateriais()){
			List<Expedicao> listaExpedicao = expedicaoService.findForConfereincia(whereIn);
			if(SinedUtil.isListNotEmpty(listaExpedicao)){
				for(Expedicao expedicao2 : listaExpedicao){
					if(!Expedicaosituacao.EM_CONFERENCIA.equals(expedicao2.getExpedicaosituacao())){
						expedicaohistoricoService.saveHistorico(expedicao2.getCdexpedicao().toString(), "", Expedicaoacao.CONFERENCIA_INICIADA);
					}
				}
			}
			expedicaoService.updateSituacao(whereIn, Expedicaosituacao.EM_CONFERENCIA);
		}
		
		return new ModelAndView("direct:crud/popup/popupConferenciaMateriais");
	}
	
	public ModelAndView abrirRemoverNotas(WebRequestContext request,ExpedicaoFiltro filtro){
		String whereIn = SinedUtil.getItensSelecionados(request);
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_ABERTO);
		if(notValido){
			request.addError("A remo��o de notas s� podem ser realizadas em expedi��es com situa��o 'EM ABERTO'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		HashMap<Integer, Notafiscalproduto> mapNota = new HashMap<Integer, Notafiscalproduto>();
		filtro.setCdexpedicao(Integer.parseInt(whereIn));
		List<Expedicaoitem> listaItens = expedicaoitemService.findByWhereInExpedicao(whereIn);
		for (Expedicaoitem item : listaItens) {
			if(item.getNotaFiscalProdutoItem() != null && item.getNotaFiscalProdutoItem().getNotafiscalproduto() != null){
				Notafiscalproduto nota = item.getNotaFiscalProdutoItem().getNotafiscalproduto();
				if(nota.getCdNota() != null && !mapNota.containsKey(nota.getCdNota())){
					mapNota.put(nota.getCdNota(), nota);
				}
			}
		}
		List<Notafiscalproduto> listaNotas = new ArrayList<Notafiscalproduto>();
		for(Map.Entry<Integer, Notafiscalproduto> valorMap :mapNota.entrySet()){
			listaNotas.add(valorMap.getValue());
		}
		
		request.setAttribute("listaNotaFiscal", listaNotas);		
		return new ModelAndView("direct:crud/popup/popupRemoverNotas").addObject("filtro", filtro);
	}

	
	public ModelAndView selecionarNota(WebRequestContext request, ExpedicaoFiltro filtro){
		request.setAttribute("forceConsulta", true);
		Integer cdexpedicao = null;
		
		if(filtro==null || filtro.getCdexpedicao()==null){
			filtro = new ExpedicaoFiltro();
			String whereIn = SinedUtil.getItensSelecionados(request);
			cdexpedicao = Integer.parseInt(whereIn);
			
			boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_ABERTO);
			
			if(notValido){
				request.addError("A adi��o de vendas s� podem ser realizadas em expedi��es com situa��o 'EM ABERTO'.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		try {
			if(cdexpedicao != null) {
				filtro.setCdexpedicao(cdexpedicao);
				Expedicao expedicao = expedicaoService.loadForEntrada(new Expedicao(cdexpedicao));
				filtro.setEmpresa(expedicao.getEmpresa());
			}
			
			boolean adicionandoNota = filtro.getNota()!=null && filtro.getNota().getCdNota() != null;
			
			if(adicionandoNota){
				
				request.setAttribute("isNotaCarregada", true);
				
				filtro.setListaVendas(new ArrayList<Venda>());
				if(adicionandoNota){
					List<Notafiscalprodutoitem> listaNotas = notafiscalprodutoitemService.findByWhereInCdnota(filtro.getNota().getCdNota().toString());
					List<Expedicaoitem> listaItem = new ArrayList<Expedicaoitem>();
					for (Notafiscalprodutoitem nota : listaNotas) {
						Expedicaoitem item = criarItemNota(nota);
						if(item !=null){
							listaItem.add(item);
						}
					}
					filtro.setListaExpedicaoItem(listaItem);
					request.setAttribute("listaExpedicaoItem", listaItem);
					request.setAttribute("msg", "");
				}
			}
			
			return new ModelAndView("direct:crud/popup/popupSelecionarNota").addObject("filtro", filtro);			
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro no processamento da expedi��o.");
		}
		
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	
	
	public ModelAndView conferirPneus(WebRequestContext request, Expedicao expedicao){
		String whereIn = request.getParameter("whereIn");
		if((whereIn == null || whereIn.equals("")) && expedicao.getWhereIn() != null && !expedicao.getWhereIn().trim().equals("")){
			whereIn = expedicao.getWhereIn();
		}
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_ABERTO);
		if(notValido){
			request.addError("Para conferir a expedi��o tem que ter situa��o 'EM ABERTO'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		expedicao.setCdexpedicao(Integer.parseInt(whereIn));
		expedicao.setListaExpedicaoitem(expedicaoitemService.findForConferenciaPneuFromExpedicao(whereIn));
		
		List<ConferienciaPneuBeam> listaConfderencia = new ArrayList<ConferienciaPneuBeam>();	
		for (Expedicaoitem item : expedicao.getListaExpedicaoitem()) {
			//			if(item.getNotaFiscalProdutoItem() != null){
			if(item.getPneu() != null){
				ConferienciaPneuBeam bean = new ConferienciaPneuBeam();
				if(item.getNotaFiscalProdutoItem() != null){
					if(item.getNotaFiscalProdutoItem().getNotafiscalproduto() !=null){
						bean.setOrigem("NF "+item.getNotaFiscalProdutoItem().getNotafiscalproduto().getCdNota());	
					}
				}else if(item.getColetaMaterial() != null){
					if(item.getColetaMaterial().getColeta() !=null){
						bean.setOrigem("Coleta "+item.getColetaMaterial().getColeta().getCdcoleta());	
					}
				}
				bean.setCdExpedicao(item.getExpedicao().getCdexpedicao());
				if(item.getCliente() != null){
					bean.setClienteNome(item.getCliente().getNome());
				}
				bean.setCodBarra(item.getPneu().getCdpneu());
				bean.setPneu(item.getPneu());
				bean.setLabelStatus("Pendente");
				request.setAttribute(CONSULTAR, true);
				listaConfderencia.add(bean);
			}
		}
		expedicao.setListaConfderencia(listaConfderencia);
		if(!SinedUtil.isListEmpty(expedicao.getListaExpedicaoitem())){	
			request.setAttribute("whereIn", whereIn);
			return new ModelAndView("direct:crud/popup/popupConferenciaPneu", "bean", expedicao);
		}
		SinedUtil.fechaPopUp(request);
		request.addError("N�o foi encontrado o pneu");
		return null;
	}
	
	 public ModelAndView salvarConferenciaPneu (WebRequestContext request, Expedicao expedicao) throws Exception {
		String whereIn = request.getParameter("whereIn");
	
		expedicaoService.updateConferenciamateriaisExpedicao(whereIn);
		
		Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
		expedicaohistorico.setExpedicao(new Expedicao(new Integer(whereIn)));
		expedicaohistorico.setExpedicaoacao(Expedicaoacao.CONFERENCIA_REALIZADA);
		expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
		
		request.setAttribute("salvar", true);
		return new ModelAndView("direct:crud/popup/popupConferenciaPneu", "bean", expedicao);
	}
	
	
	public ModelAndView salvarConferenciaMateriais(WebRequestContext request, ConferenciaMateriaisBean bean) throws Exception {
		String whereIn = request.getParameter("whereIn");
		boolean fromVenda = "true".equalsIgnoreCase(request.getParameter("fromVenda"));
		
		String idTela = "conferenciaExpedicao_"+SinedDateUtils.currentTimestamp();
		boolean conferenciaCega = bean.getConferenciaCega() != null && bean.getConferenciaCega();
		
		if(Util.objects.isPersistent(bean.getListaConferencia().iterator().next().getExpedicaoitem())){
			List<Expedicaohistorico> listaHistorico = new ArrayList<Expedicaohistorico>();
			
			if (!fromVenda){
				whereIn = whereIn.replace(" ", "");
				
				expedicaoService.updateConferenciamateriaisExpedicao(whereIn);
				
				for (String id: whereIn.split("\\,")){
					Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
					expedicaohistorico.setExpedicao(new Expedicao(new Integer(id)));
					expedicaohistorico.setExpedicaoacao(conferenciaCega ? Expedicaoacao.FINALIZAR_SEM_CONFERENCIA : Expedicaoacao.CONFERENCIA_REALIZADA);
					expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
					
					listaHistorico.add(expedicaohistorico);
				}
			}else{
				for (String id: whereIn.split("\\,")){
					Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
					expedicaohistorico.setExpedicao(new Expedicao(new Integer(id)));
					expedicaohistorico.setExpedicaoacao(conferenciaCega ? Expedicaoacao.FINALIZAR_SEM_CONFERENCIA : Expedicaoacao.CONFERENCIA_REALIZADA);
					expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
					
					listaHistorico.add(expedicaohistorico);
				}
				/*Expedicaoitem expItem = expedicaoitemService.load(bean.getListaConferencia().get(0).getExpedicaoitem());
				
				ConferenciaMaterialExpedicao conf = new ConferenciaMaterialExpedicao();
				
				conf.setExpedicao(expItem.getExpedicao());
			
				Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
				expedicaohistorico.setExpedicao(new Expedicao(new Integer(id)));
				expedicaohistorico.setExpedicaoacao(Expedicaoacao.CONFERENCIA_REALIZADA);
				expedicaohistoricoService.saveOrUpdate(expedicaohistorico);*/
				
				//listaHistorico.add(expedicaohistorico);
			}
			
			/*Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
			expedicaohistorico.setExpedicao(new Expedicao(new Integer(id)));
			expedicaohistorico.setExpedicaoacao(Expedicaoacao.CONFERENCIA_REALIZADA);
			expedicaohistoricoService.saveOrUpdate(expedicaohistorico);*/
			Expedicaohistorico expedicaohistorico = listaHistorico.get(0);
			
			for(ConferenciaMateriaisVendaBean item: bean.getListaConferencia()){
				if(Util.objects.isPersistent(item.getExpedicaoitem())){
					Expedicaoitem expItem = expedicaoitemService.load(item.getExpedicaoitem());
					
					ConferenciaMaterialExpedicao conf = new ConferenciaMaterialExpedicao();
					
					conf.setExpedicao(expItem.getExpedicao());
					conf.setExpedicaoitem(expItem);
					conf.setMaterial(item.getMaterial());
					conf.setQtdeConferida(item.getQtdeConferida() != null ? item.getQtdeConferida() :  0d);
					conf.setQtdeVenda(item.getQuantidade());
					conf.setUnidadeMedida(item.getUnidademedida());
					conf.setVendaMaterial(item.getVendamaterial());
					conf.setExpedicaoHistorico(expedicaohistorico);
					
					conferenciaMaterialExpedicaoService.saveOrUpdate(conf);
				}
			}
			String obs = "<a href=\"javascript:visualizaConferencia("
					+ expedicaohistorico.getCdexpedicaohistorico() + ");\">Clique aqui para consultar a confer�ncia</a>.";
			expedicaohistorico.setObservacao(obs);
			expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
		}else{
			List<ConferenciaMaterialExpedicao> lista = new ArrayList<ConferenciaMaterialExpedicao>();
			for(ConferenciaMateriaisVendaBean item: bean.getListaConferencia()){
				ConferenciaMaterialExpedicao conf = new ConferenciaMaterialExpedicao();
				
				//conf.setExpedicao(expItem.getExpedicao());
				conf.setMaterial(item.getMaterial());
				conf.setQtdeConferida(item.getQtdeConferida());
				conf.setQtdeVenda(item.getQuantidade());
				conf.setUnidadeMedida(item.getUnidademedida());
				conf.setVendaMaterial(item.getVendamaterial());
				
				lista.add(conf);

			}
			request.getSession().setAttribute("conferencia_"+idTela, lista);
		}
		
		
		request.setAttribute("whereIn", whereIn);
		request.setAttribute("fromVenda", fromVenda);
		request.setAttribute("salvar", true);
		request.setAttribute("idTela", idTela);
		request.addMessage("Confer�ncia salva com sucesso.");
		
		return new ModelAndView("direct:crud/popup/popupConferenciaMateriais");
	}
	
	public ModelAndView ajaxTemplatesEstiquetaDisponiveis(WebRequestContext request){
		return vendaService.ajaxTemplatesEstiquetaDisponiveis(request);
	}
	
	public ModelAndView ajaxTemplatesEstiquetaDisponiveisExpedicao(WebRequestContext request, Expedicao expedicao){	
		
		ModelAndView view = expedicaoService.ajaxTemplatesEstiquetaDisponiveis(request);
		String whereIn = request.getParameter("whereIn");	
		if((whereIn == null || whereIn.equals("")) && expedicao.getWhereIn() != null && !expedicao.getWhereIn().trim().equals("")){
			whereIn = expedicao.getWhereIn();
		}	
		
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.CONFERENCIA_REALIZADA, 
				Expedicaosituacao.CONFIRMADA, Expedicaosituacao.FATURADA);
		if(notValido){
			String message = "Para imprimir etiqueta da expedi��o tem que ter situa��o 'CONFER�NCIA REALIZADA' 'CONFIRMADA' ou 'FATURADA'.";
			view.addObject("error", message);
		}	
		
		view.addObject("hasMany", expedicaoitemService.existeVendasDistintas(whereIn));			
		
		return view;
	}
	
	public ModelAndView abrirSelecaoTemplateEtiquetaDestinatarioRemetente(WebRequestContext request){
		return vendaService.abrirSelecaoTemplateEtiquetaDestinatarioRemetente(request);
	}
	
	public ModelAndView abrirSelecaoTemplateEtiquetaExpedicao(WebRequestContext request){
		return expedicaoService.abrirSelecaoTemplateEtiquetaExpedicao(request);
	}
	
	public ModelAndView emitirEtiquetaDestinatarioRemetente(WebRequestContext request){
		return vendaService.emitirEtiquetaDestinatarioRemetente(request); 
	}
	
	public ModelAndView emitirEtiquetaExpedicao(WebRequestContext request){
		return expedicaoService.emitirEtiquetaExpedicao(request); 
	}

	@Override
	public ModelAndView doExportar(WebRequestContext request,
			ExpedicaoFiltro filtro) throws CrudException, IOException {
		Resource resource = expedicaoService.gerarListagemCSV(filtro);
		return new ResourceModelAndView(resource);
	}
	
	public void ajaxSetDadosSessaoForLoteAutocomplete(WebRequestContext request){
		pedidovendaService.ajaxSetDadosSessaoForLoteAutocomplete(request, "EXPEDICAO");
	}
	
	public ModelAndView ajaxVerificaObrigatoriedadeLote(WebRequestContext request){
		return materialService.ajaxVerificaObrigatoriedadeLote(request);
	}
	
	public ModelAndView abrePopupTrocaLotes(WebRequestContext request, Expedicaoitem expedicaoItem){
//		Expedicaoitem expedicaoItem = expedicao.getListaExpedicaoitem().get(0);
		Expedicaoitem ei = expedicaoitemService.load(expedicaoItem);
		Expedicao expedicao = expedicaoService.loadForEntrada(ei.getExpedicao());
		for(Expedicaoitem expItem: expedicao.getListaExpedicaoitem()){
			if(expItem.getCdexpedicaoitem().equals(expedicaoItem.getCdexpedicaoitem())){
				expedicaoItem = expItem;
				break;
			}
		}
		//String cdEmpresa = request.getParameter("cdempresa");
		ExpedicaoTrocaLoteBean bean = new ExpedicaoTrocaLoteBean();
		Expedicaoitem item = new Expedicaoitem(expedicaoItem);
		expedicaoItem.setEmpresa(expedicao.getEmpresa());
		item.setEmpresa(expedicao.getEmpresa());
		item.setCdexpedicaoitem(expedicaoItem.getCdexpedicaoitem());
		bean.setExpedicaoItem(item);
		Double qtdeDisponivel = loteestoqueService.buscarQtdeDisponivelMaterialLotes(expedicaoItem.getMaterial().getCdmaterial(), expedicaoItem.getLocalarmazenagem().getCdlocalarmazenagem(),
															expedicao.getEmpresa().getCdpessoa(), expedicaoItem.getLoteestoque().getCdloteestoque());
		
		qtdeDisponivel += reservaService.getReservaByVendamaterialOrExpedicaoitem(expedicaoItem.getVendamaterial(), expedicaoItem, expedicaoItem.getMaterial(), expedicaoItem.getLoteestoque());
		
		qtdeDisponivel = unidademedidaService.getQtdeConvertida(expedicaoItem.getMaterial(), expedicaoItem.getUnidademedida(), expedicaoItem.getMaterial().getUnidademedida(), qtdeDisponivel, false);

		bean.getListaExpedicaoItem().add(expedicaoItem);
		List<Loteestoque> listaLotesComEstoque = loteestoqueService.buscarLotesMaterialDisponivel(expedicaoItem.getMaterial().getCdmaterial(), expedicaoItem.getLocalarmazenagem().getCdlocalarmazenagem(),
																							expedicao.getEmpresa().getCdpessoa());
		
		if(Util.objects.isPersistent(expedicaoItem.getUnidademedida())
			&& Util.objects.isPersistent(expedicaoItem.getMaterial())
			&& !expedicaoItem.getUnidademedida().equals(expedicaoItem.getMaterial().getUnidademedida())){
			for(Loteestoque loteEstoque: listaLotesComEstoque){
				Double qtdeConvertida = unidademedidaService.getQtdeConvertida(expedicaoItem.getMaterial(), expedicaoItem.getUnidademedida(), expedicaoItem.getMaterial().getUnidademedida(), loteEstoque.getQtde(), false);
				loteEstoque.setQtde(qtdeConvertida);
			}
		}
		if(qtdeDisponivel != null && qtdeDisponivel < expedicaoItem.getQtdeexpedicao()){
			expedicaoItem.setQtdeexpedicao(qtdeDisponivel);
		}
		request.setAttribute("listaLotesComEstoque", listaLotesComEstoque);
		
		return new ModelAndView("direct:crud/popup/popupTrocaLoteMateriaisExpedicao", "bean", bean);
	}
	
	public ModelAndView saveTrocarLotes(WebRequestContext request, ExpedicaoTrocaLoteBean bean){
		if(!expedicaoService.validaQtdeDisponivelLoteExpedicaoitem(request, bean)){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		Double qtdeExpedicao = 0D;
		for(Expedicaoitem ei: bean.getListaExpedicaoItem()){
			qtdeExpedicao += ei.getQtdeexpedicao();
		}

		if(qtdeExpedicao.compareTo(bean.getExpedicaoItem().getQtdeexpedicao()) != 0){
			request.addError("A quantidade informada � diferente da quantidade expedida.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(bean.getExpedicaoItem().getQtdeexpedicao().equals(0D)){
			request.addError("Favor informar a quantidade para expedi��o.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		vendamaterialService.saveListByExpedicao(bean);
		request.addMessage("Altera��o de lotes realizada com sucesso.");
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	private String montaStringCodigosEmbalagem(List<Embalagem> listaEmbalagem, Unidademedida unidademedida) {
		String codigoBarraEmbalagens = "";
		for (Embalagem embalagem : listaEmbalagem) {
			if(unidademedida != null){
				if(embalagem.getUnidadeMedida().equals(unidademedida) && embalagem.getCodigoBarras() != null && !embalagem.getCodigoBarras().toLowerCase().contains("gtin")){
					codigoBarraEmbalagens += embalagem.getCodigoBarras()+";";
				}
			}else {
				if(embalagem.getCodigoBarras() != null && !embalagem.getCodigoBarras().toLowerCase().contains("gtin")){
					codigoBarraEmbalagens += embalagem.getCodigoBarras()+";";							
				}
			}
		}
		return codigoBarraEmbalagens;
	}

	public ModelAndView abrirPopUpFinalizarConferencia(WebRequestContext request, Expedicao expedicao){
		ModelAndView view = new ModelAndView("direct:crud/popup/popUpFinalizarConferencia", "bean", expedicao);
		
		String whereIn = request.getParameter("whereIn");	
		if((whereIn == null || whereIn.equals("")) && expedicao.getWhereIn() != null && !expedicao.getWhereIn().trim().equals("")){
			whereIn = expedicao.getWhereIn();
		}	
		
		List<Expedicaoitem> listExpedicaoItem = expedicaoitemService.findForFinalizaExpedicao(whereIn);
		List<String> listErro = new ArrayList<String>();
		for (Expedicaoitem expedicaoitem : listExpedicaoItem) {
			ConferenciaMaterialExpedicao conferenciaMaterialExpedicao = conferenciaMaterialExpedicaoService.getLastEmConferenciaByExpedicaoitem(expedicaoitem);
			if(conferenciaMaterialExpedicao != null){
				if(conferenciaMaterialExpedicao.getQtdeConferida() == null || conferenciaMaterialExpedicao.getQtdeConferida() < conferenciaMaterialExpedicao.getQtdeVenda()){
					listErro.add("A quantidade conferida n�o pode ser diferente da quantidade de origem. Item " + conferenciaMaterialExpedicao.getMaterial().getNome());
				}
			} else {
				String msg = "A quantidade conferida n�o pode ser diferente da quantidade de origem.";
				String nomeMaterial = expedicaoitem.getMaterial() != null? expedicaoitem.getMaterial().getNome(): "";
				if(StringUtils.isNotBlank(nomeMaterial)){
					msg += " Item " + nomeMaterial;
				}
				listErro.add(msg);
			}
		}
		if(listErro.size() > 0){
			for (String msg : listErro) {
				request.addError(msg);
			}
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		
		request.setAttribute("hasManyExpedicao", expedicaoService.existeVariasVendas(listExpedicaoItem));
		List<ReportTemplateBean> templates = expedicaoService.templatesEstiquetaDisponiveis();
		request.setAttribute("possuiTemplate",  templates.size() > 0);
		
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_CONFERENCIA);
		if(notValido){
			request.addError("Para finalizar a expedi��o tem que ter situa��o 'EM CONFER�NCIA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}		
		
		return view;
	}
	
	public ModelAndView iniciarSeparacao(WebRequestContext request, Expedicao expedicao){
		String whereIn = request.getParameter("whereIn");	
		if((whereIn == null || whereIn.equals("")) && expedicao.getWhereIn() != null && !expedicao.getWhereIn().trim().equals("")){
			whereIn = expedicao.getWhereIn();
		}
		
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_ABERTO);
		if(notValido){
			request.addError("Para iniciar a separa��o a expedi��o tem que ter situa��o 'EM ABERTO'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		expedicaoService.updateSituacao(whereIn, Expedicaosituacao.EM_SEPARACAO);
		Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
		expedicaohistorico.setExpedicaoacao(Expedicaoacao.SEPARACAO_INICIADA);
		expedicaohistorico.setExpedicao(new Expedicao(new Integer(whereIn)));
		expedicaohistorico.getExpedicao().setExpedicaosituacao(Expedicaosituacao.EM_SEPARACAO);
		expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
		
		request.addMessage("Separa��o iniciado com sucesso.");
		request.getSession().setAttribute("imprimirSeparacao", "true");
		request.getSession().setAttribute("whereInImprimirSeparacao", whereIn);
		SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao");
		
		return null;
	}
	
	public ModelAndView finalizarSeparacao(WebRequestContext request, Expedicao expedicao){
		String whereIn = request.getParameter("whereIn");	
		if((whereIn == null || whereIn.equals("")) && expedicao.getWhereIn() != null && !expedicao.getWhereIn().trim().equals("")){
			whereIn = expedicao.getWhereIn();
		}
		
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_SEPARACAO);
		if(notValido){
			request.addError("Para finalizar a separa��o a expedi��o tem que ter situa��o 'EM SEPARACAO'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		expedicaoService.updateSituacao(whereIn, Expedicaosituacao.SEPARACAO_REALIZADA);
		Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
		expedicaohistorico.setExpedicaoacao(Expedicaoacao.SEPARACAO_FINALIZADA);
		expedicaohistorico.setExpedicao(new Expedicao(new Integer(whereIn)));
		expedicaohistorico.getExpedicao().setExpedicaosituacao(Expedicaosituacao.SEPARACAO_REALIZADA);
		expedicaohistoricoService.saveOrUpdate(expedicaohistorico);

		request.addMessage("Finaliza��o da separa��o realizado com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao");
		return null;
	}
	
	public ModelAndView finalizarConferencia(WebRequestContext request, Expedicao expedicao){
		Boolean entrada = request.getParameter("entrada") != null ? Boolean.parseBoolean(request.getParameter("entrada")) : Boolean.FALSE; 
		String whereIn = request.getParameter("whereIn");	
		if((whereIn == null || whereIn.equals("")) && expedicao.getWhereIn() != null && !expedicao.getWhereIn().trim().equals("")){
			whereIn = expedicao.getWhereIn();
		}
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.EM_CONFERENCIA);
		if(notValido){
			request.addError("Para finalizar a expedi��o tem que ter situa��o 'EM CONFER�NCIA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}			
		
		boolean fromVenda = "true".equalsIgnoreCase(request.getParameter("fromVenda"));		
		if (!fromVenda){			
			whereIn = whereIn.replace(" ", "");	
			if(whereIn.split("\\,").length != 1){
				request.addError("S� � possivel finalizar confer�ncia de uma expedi��o por vez.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			expedicao.setCdexpedicao(Integer.parseInt(whereIn));
			List<Expedicaoitem> listaExpedicaoitem = expedicaoitemService.findForFinalizaExpedicao(whereIn);
			List<String> listErro = new ArrayList<String>();
			for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
				ConferenciaMaterialExpedicao conferenciaMaterialExpedicao = conferenciaMaterialExpedicaoService.getLastEmConferenciaByExpedicaoitem(expedicaoitem);
				if(conferenciaMaterialExpedicao.getQtdeConferida() < conferenciaMaterialExpedicao.getQtdeVenda()){
					listErro.add("A quantidade conferida n�o pode ser diferente da quantidade de origem. Item " + conferenciaMaterialExpedicao.getMaterial().getNome());
				}
			}
			if(listErro.size() > 0){
				for (String msg : listErro) {
					request.addError(msg);
				}
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			expedicao.setListaExpedicaoitem(listaExpedicaoitem);
			
			
			
			Integer cdvenda = null;
			for(Expedicaoitem item: expedicao.getListaExpedicaoitem()){
				if(cdvenda != null && cdvenda != item.getVendamaterial().getVenda().getCdvenda()){
					cdvenda = null;
					break;
				}
				cdvenda = item.getVendamaterial().getVenda().getCdvenda();				
			}
			
			if(expedicao.getQuantidadevolumes() != null){
				expedicaoService.updateSituacao(whereIn, Expedicaosituacao.CONFERENCIA_REALIZADA, expedicao.getQuantidadevolumes());
				if(cdvenda != null){
					Venda venda = vendaService.loadWithPedidovendatipo(new Venda(cdvenda));
					if(venda.getPedidovendatipo() == null || !Boolean.TRUE.equals(venda.getPedidovendatipo().getEntregaFutura())){
						vendaService.updateQtdVolumesVenda(cdvenda.toString(), expedicao.getQuantidadevolumes());												
					}
				}				
			} else {
				expedicaoService.updateSituacao(whereIn, Expedicaosituacao.CONFERENCIA_REALIZADA);
			}			
			
			Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
			expedicaohistorico.setExpedicao(new Expedicao(new Integer(whereIn)));
			expedicaohistorico.setExpedicaoacao(Expedicaoacao.CONFERENCIA_FINALIZADA);
			expedicaohistoricoService.saveOrUpdate(expedicaohistorico);		
			
			boolean existeEtiquetas = expedicaoService.templatesEstiquetaDisponiveis().size() > 0;
			
			if(!expedicaoService.existeVariasVendas(expedicao.getListaExpedicaoitem()) && existeEtiquetas && expedicao.getQuantidadevolumes() != null){
				request.getSession().setAttribute("imprimirEtiquetaExpedicao", "true");
				request.getSession().setAttribute("whereInImprimirEtiquetaExpedicao", whereIn);	
			}	
		}	
		
		request.addMessage("Confer�ncia finalizada com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao" + (entrada ? "?ACAO=consultar&cdexpedicao="+whereIn : ""));
		return null;
	}
	
	public ModelAndView visualizacaoConferencia(WebRequestContext request, Expedicaohistorico bean){
		bean.setListaConferencia(conferenciaMaterialExpedicaoService.findByHistorico(bean));
		return new ModelAndView("direct:crud/popup/popupVisualizacaoConferencia", "bean", bean);
	}	
	
	public ModelAndView ajaxVerificaIdentificadorAutomaticoCarregamento(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		String cdempresaString = request.getParameter("cdempresa");
		
		if(StringUtils.isNotEmpty(cdempresaString)){
			Empresa empresa = new Empresa(Integer.parseInt(cdempresaString));
			
			Integer idProximoCarregamento = empresaService.carregaProximoIdentificadorCarregamento(empresa);
			
			jsonModelAndView.addObject("idProximoCarregamento", idProximoCarregamento);
		}
		
		return jsonModelAndView;
	}
	
	@Override
	protected void validateBean(Expedicao bean, BindException errors) {
		if(bean.getIdentificadorcarregamento() != null){
			if(expedicaoService.existsIdentificadorCarregamentoEmpresa(bean.getCdexpedicao() != null ? bean : null, Integer.parseInt(bean.getIdentificadorcarregamento().toString()), bean.getEmpresa())){
				errors.reject("001", "Identificador (" + bean.getIdentificadorcarregamento() + ") j� cadastrado no sistema.");
			}
		}else if(bean.getIdentificadorAutomatico() != null && bean.getIdentificadorAutomatico() && bean.getCdexpedicao() == null && bean.getEmpresa() != null){
			Integer idProximoCarregamento = empresaService.carregaProximoIdentificadorCarregamento(bean.getEmpresa());	
			if(expedicaoService.existsIdentificadorCarregamentoEmpresa(bean.getCdexpedicao() != null ? bean : null, idProximoCarregamento, bean.getEmpresa())){
				errors.reject("001", "Identificador (" + idProximoCarregamento + ") j� cadastrado no sistema. Favor verificar o identificador no cadastro da empresa.");
			}
		}
		
		//apenas para validar se o lote est� preenchido caso precise fazer reserva de estoque
		expedicaoService.gerarReservaAoSalvar(bean, errors);
	}
	
	public ModelAndView ajaxValidaLimparVolumes(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean notValido = expedicaoService.haveExpedicaoNotInSituacao(whereIn, Expedicaosituacao.CONFIRMADA, Expedicaosituacao.CONFERENCIA_REALIZADA, 
				Expedicaosituacao.CANCELADA, Expedicaosituacao.EM_SEPARACAO, Expedicaosituacao.EM_CONFERENCIA, Expedicaosituacao.CANCELADA, Expedicaosituacao.SEPARACAO_REALIZADA);
		
		if(notValido){
			return new JsonModelAndView()
				.addObject("podeEstornar", false)
				.addObject("msg", "Para estornar a(s) expedi��o(�es) tem que ter situa��o(�es) 'EM SEPARACAO', 'EM CONFER�NCIA', 'CONFIRMADA' ou 'CANCELADA'.");
		}
		
		boolean exibirMensagem = false;
		
		List<Expedicao> listaExpedicao = expedicaoService.findByExpedicaosituacao(whereIn, Expedicaosituacao.CONFERENCIA_REALIZADA);
		if(CollectionsUtil.isListNotEmpty(listaExpedicao)){
			for(Expedicao exp: listaExpedicao){
				List<Venda> listaVendas = expedicaoService.findVendasByExpedicao(exp);
				if(CollectionsUtil.isListNotEmpty(listaVendas) && listaVendas.size() == 1){
					exibirMensagem = true;
					break;
				}
			}
		}
		return new JsonModelAndView()
					.addObject("exibirMensagem", exibirMensagem)
					.addObject("podeEstornar", true)
					.addObject("msg", "A informa��o sobre a quantidade de volumes da expedi��o e da venda ser� apagada.");
	}

	public ModelAndView validaCancelar(WebRequestContext request, Expedicao expedicao){
		String idTelaSession = "cancelamentoExpedicao_"+SinedDateUtils.toString(SinedDateUtils.currentTimestamp(), "dd:MM:yyyy hh:mm:ss");
		List<Venda> listaVenda = vendaService.findByExpedicoes(expedicao.getCdexpedicao().toString());
		if(SinedUtil.isListNotEmpty(listaVenda)){
			for(Venda venda: listaVenda){
				for(Expedicao expedicaoCancelar: venda.getListaExpedicaotrans()){
					expedicaoService.validateReservaByCancelamento(request, venda, expedicaoCancelar, idTelaSession);
				}
			}
		}
		ModelAndView retorno = new JsonModelAndView();
		Object obj = request.getSession().getAttribute(idTelaSession);
		if(obj != null && !((List)obj).isEmpty()){
			System.out.println("Setou idTelaSession no json de retorno");
			retorno.addObject("idTelaSession", idTelaSession);
			retorno.addObject("exibeMensagemVendaSemEstoque", true);
		}else{
			retorno.addObject("exibeMensagemVendaSemEstoque", false);
		}
		
		return retorno;
	}
	
	public ModelAndView abrirPopupMateriaisRepetidos(WebRequestContext request){
		String ids = request.getParameter("ids");
		return new ModelAndView("direct:crud/popup/popupSelecionarMaterialRepetidos").addObject("ids", ids);
	}
	
	public ModelAndView ajaxVerificaCodigosBarraRepetidos(WebRequestContext request, ConferenciaMateriaisBean form){
		String codigoBarras = request.getParameter("codigoBarras");
		String whereIn = request.getParameter("whereIn");
		
		String materialAndUnidade = form.getMaterialAndUnidade();
		List<ConferenciaMateriaisVendaBean> lista = new ArrayList<ConferenciaMateriaisVendaBean>();
		for(ConferenciaMateriaisVendaBean item: materialService.findDuplicadosByCodigoBarras(codigoBarras, whereIn)){
			boolean exists = false;
			for(String str: materialAndUnidade.split("_")){
				String cdmaterial = str.split(",")[0];
				String cdunidademedida = str.split(",")[1];
				if(item.getMaterial().getCdmaterial().toString().equals(cdmaterial) &&
					item.getUnidademedida().getCdunidademedida().toString().equals(cdunidademedida)){
					exists = true;
					break;
				}
			}
			if(exists){
				lista.add(item);
			}
		}
		boolean isDuplicado = CollectionsUtil.isListNotEmpty(lista) && lista.size() > 1;
		
		return new JsonModelAndView().addObject("isDuplicado", isDuplicado);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView abrirPopupCodigosBarraRepetidos(WebRequestContext request, ConferenciaMateriaisBean form){
		String codigoBarras = request.getParameter("codigoBarras");
		String whereIn = request.getParameter("whereIn");
		
		ConferenciaMateriaisBean bean = new ConferenciaMateriaisBean();
		bean.setMaterialAndUnidade(form.getMaterialAndUnidade());
		String materialAndUnidade = form.getMaterialAndUnidade();
		List<ConferenciaMateriaisVendaBean> lista = new ArrayList<ConferenciaMateriaisVendaBean>();
		for(ConferenciaMateriaisVendaBean item: materialService.findDuplicadosByCodigoBarras(codigoBarras, whereIn)){
			boolean exists = false;
			for(String str: materialAndUnidade.split("_")){
				String cdmaterial = str.split(",")[0];
				String cdunidademedida = str.split(",")[1];
				if(item.getMaterial().getCdmaterial().toString().equals(cdmaterial) &&
					item.getUnidademedida().getCdunidademedida().toString().equals(cdunidademedida)){
					exists = true;
					break;
				}
			}
			if(exists){
				lista.add(item);
			}
		}
		bean.setListaConferencia(SinedUtil.listToSet(lista, ConferenciaMateriaisVendaBean.class));
		bean.setQtdeConferir(form.getQtdeConferir());
		
		return new ModelAndView("direct:crud/popup/popupSelecionarMaterialCodigoBarras")
				.addObject("bean", bean);
	}
	
	public ModelAndView converterUnidadesConferenciaExpedicao(WebRequestContext request) {
		ModelAndView json = new JsonModelAndView();

		String codigoBarras = request.getParameter("codigoBarras");
		String cdmaterialString = request.getParameter("cdmaterial");
		String unidademedidaString = request.getParameter("unidadeMedida");
		String qtdeAConferirString  = request.getParameter("qtdeAConferir");
		String unidadeMedidaPrincipalMaterialString = request.getParameter("unidadeMedidaPrincipalMaterial");
		String unidademedidaCodigoBarrasString = request.getParameter("unidademedidaCodigoBarras");
		
		Double qtdeConvertida = -1d;
		if(StringUtils.isNotEmpty(cdmaterialString) && StringUtils.isNotEmpty(unidademedidaString)){
			Integer cdmaterial = Integer.parseInt(cdmaterialString);
			Integer cdunidademedida = Integer.parseInt(unidademedidaString);
			Double qtdeAConferir = Double.parseDouble(qtdeAConferirString);
			Boolean unidadeMedidaPrincipalMaterial = Boolean.parseBoolean(unidadeMedidaPrincipalMaterialString);
			Unidademedida unidademedidaCodigoBarras = StringUtils.isNotBlank(unidademedidaCodigoBarrasString) ? new Unidademedida(Integer.parseInt(unidademedidaCodigoBarrasString)) : null;
			
			Material material = new Material(cdmaterial);
			material = materialService.loadMaterialunidademedida(material);
			
			if(!unidadeMedidaPrincipalMaterial){
				if(unidademedidaCodigoBarras != null){
					qtdeConvertida = unidademedidaService.getQtdeConvertida(material, new Unidademedida(cdunidademedida), unidademedidaCodigoBarras, qtdeAConferir, false);
				}else {
					Embalagem embalagem = embalagemService.findEmbalagemByMaterialCodigoBarras(material, codigoBarras, unidademedidaCodigoBarras);
					if(embalagem != null && embalagem.getUnidadeMedida() != null){				
						qtdeConvertida = unidademedidaService.getQtdeConvertida(material, new Unidademedida(cdunidademedida), embalagem.getUnidadeMedida(), qtdeAConferir, false);
					}
				}
			}else {
				material = materialService.loadMaterialunidademedida(material);
				qtdeConvertida = unidademedidaService.getQtdeConvertida(material, new Unidademedida(cdunidademedida), (unidademedidaCodigoBarras != null ? unidademedidaCodigoBarras : material.getUnidademedida()), qtdeAConferir, false);
			}
		}
		json.addObject("qtdeConvertida", qtdeConvertida);
		
		return json;
	}
	
	public ModelAndView abrirIncluirDadosRastreio(WebRequestContext request, Expedicao expedicao){
		expedicao = expedicaoService.loadForEntrada(expedicao);
		List<Integer> listaCdVendas = new ArrayList<Integer>();
		for(Expedicaoitem ei: expedicao.getListaExpedicaoitem()){
			Integer cdVenda = ei.getCodigovenda();
			if(cdVenda != null && !listaCdVendas.contains(cdVenda)){
				listaCdVendas.add(cdVenda);
			}
		}
		Expedicaoacao acao = expedicaohistoricoService.ultimaAcao(expedicao, Expedicaoacao.FINALIZAR_ENTREGA_EXPEDICAO_ECOMMERCE);
		if(acao != null){
			request.addError("N�o � poss�vel executar a a��o, pois a entrega j� foi finalizada.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(Expedicaosituacao.CANCELADA.equals(expedicao.getExpedicaosituacao())){
			request.addError("Para realizar essa a��o a expedi��o deve estar em uma situa��o diferente de cancelada.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if(SinedUtil.isListEmpty(listaCdVendas)){
			request.addError("Para realizar essa a��o a expedi��o deve estar vinculada a uma venda originada do E-commerce.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(listaCdVendas.size() > 1){
			request.addError("Para realizar essa a��o a expedi��o deve estar vinculada a apenas uma venda.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Integer cdVenda = listaCdVendas.get(0);
		Venda venda = vendaService.loadWithPedidovendatipo(new Venda(cdVenda));
		if(venda == null){
			request.addError("Venda n�o encontrada.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Notafiscalproduto nota = notafiscalprodutoService.findEmitidaNaReceitaByVenda(venda);
		if(Util.objects.isNotPersistent(nota)){
			request.addError("Para realizar essa a��o a venda relacionada tem que possuir nota fiscal j� emitida.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(nota.getCdNota().toString());
		if(SinedUtil.isListEmpty(listaArquivonfnota)){
			request.addError("Para realizar essa a��o a venda relacionada tem que possuir nota fiscal j� emitida.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Pedidovendatipo pedidovendatipo = venda.getPedidovendatipo();
		if(pedidovendatipo == null){
			request.addError("Tipo de pedido de venda n�o encontrado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(!Boolean.TRUE.equals(pedidovendatipo.getVendasEcommerce())){
			request.addError("Para realizar essa a��o a venda deve possuir configura��o E-commerce no tipo de pedido de venda.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Ecom_PedidoVenda bean = ecom_PedidoVendaService.loadByVenda(venda);
		if(bean == null){
			request.addError("Registro do e-commerce do pedido de venda n�o encontrado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		bean.setCdExpedicaoTrans(expedicao.getCdexpedicao());
		bean.setValorCustoFrete(expedicao.getValorCustoFrete());
		bean.setDataPrevisaoEntrega(expedicao.getDataPrevisaoEntrega());
		bean.setRastreamento(expedicao.getRastreamento());
		bean.setUrlRastreamento(expedicao.getUrlRastreamento());
		if(Util.objects.isPersistent(expedicao.getTransportadora())){
			bean.setTransportadora(expedicao.getTransportadora().getNome());
			bean.setTransportadoraTrans(expedicao.getTransportadora());
			expedicao.setTransportadora(FornecedorService.getInstance().load(expedicao.getTransportadora(), "fornecedor.cdpessoa, fornecedor.nome, fornecedor.correios"));
		}		

		if(expedicao.getTransportadora() != null && Boolean.TRUE.equals(expedicao.getTransportadora().getCorreios())){
			request.setAttribute("ISCORREIO", expedicao.getTransportadora().getCorreios());
		}
		else {
			request.setAttribute("ISCORREIO", false);
		}
		request.setAttribute("URL_RASTREAMENTO_CORREIOS", parametrogeralService.buscaValorPorNome(Parametrogeral.URL_RASTREAMENTO_CORREIOS));
		return new ModelAndView("direct:/crud/popup/incluirDadosRastreioExpedicao", "bean", bean);
	}
	
	public void salvarDadosRastreio(WebRequestContext request, Ecom_PedidoVenda bean){
		
		if(StringUtils.isBlank(bean.getRastreamento())){
			request.addError("O c�digo de rastreamento n�o foi encontrado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		if(bean.getCdEcom_pedidovenda() == null){
			request.addError("Registro do e-commerce do pedido de venda n�o encontrado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		Fornecedor transportadora = bean.getTransportadoraTrans();
		bean.setTransportadoraTrans(transportadora);
		if(Util.objects.isPersistent(transportadora)){
			transportadora = FornecedorService.getInstance().load(transportadora, "fornecedor.cdpessoa, fornecedor.nome, fornecedor.correios");
			bean.setTransportadora(transportadora.getNome());
		}
		
		Expedicao expedicao = expedicaoService.loadForEntrada(new Expedicao(bean.getCdExpedicaoTrans()));
		
		if(transportadora != null && Boolean.TRUE.equals(transportadora.getCorreios())){
			if(!StringUtils.isBlank(bean.getUrlRastreamento()) && bean.getUrlRastreamento().length() > 100){
				request.addError("A url de rastreio n�o pode ter mais de 100 caracteres se a transportadora for correios.");
				SinedUtil.fechaPopUp(request);
				return;
			}
		}
	
		if(StringUtils.isBlank(expedicao.getRastreamento())){
			bean.setTransportadoraTrans(expedicao.getTransportadora());
			if(expedicao != null){
				bean.setQuantidadevolumes(expedicao.getQuantidadevolumes());
			}
			bean.setTransportadoraTrans(transportadora);

			ecom_PedidoVendaService.updateRastreamentoExpedicao(bean);
			transportadora=FornecedorService.getInstance().load(transportadora, "fornecedor.cdpessoa, fornecedor.nome, fornecedor.correios");
			
			ecom_PedidoVendaService.insertTabelaSincronizacaForRastreamentoForExpedicao(bean);

			Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
			expedicaohistorico.setExpedicao(new Expedicao(bean.getCdExpedicaoTrans()));
			expedicaohistorico.setExpedicaoacao(Expedicaoacao.RASTREIO);
			expedicaohistorico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			expedicaohistorico.setDtaltera(SinedDateUtils.currentTimestamp());
			expedicaohistorico.setObservacao("Enviado dados de rastreio: "+ bean.getRastreamento() + " "+ bean.getUrlRastreamento());
			expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
			request.addMessage("Dados de rastreio registrados com sucesso.");
		}else{

			ecom_PedidoVendaService.updateRastreamentoExpedicao(bean);
			
			Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
			expedicaohistorico.setExpedicao(new Expedicao(bean.getCdExpedicaoTrans()));
			expedicaohistorico.setExpedicaoacao(Expedicaoacao.RASTREIO);
			expedicaohistorico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			expedicaohistorico.setDtaltera(SinedDateUtils.currentTimestamp());
			expedicaohistorico.setObservacao("Alterado o c�digo de rastreio: "+ bean.getRastreamento() + " "+ bean.getUrlRastreamento());
			expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
			
			request.addMessage("Dados de rastreio registrados com sucesso.");

			transportadora=FornecedorService.getInstance().load(transportadora, "fornecedor.cdpessoa, fornecedor.nome, fornecedor.correios");
			
			if(transportadora.getCorreios()!=null &&  transportadora.getCorreios())
				request.addMessage("N�o foi enviado o c�digo de rastreio, pois j� havia sido feito um envio anteriormente.", MessageType.WARN);
		}
	
		SinedUtil.fechaPopUp(request);
	}
	
	public ModelAndView ajaxTransportadora(WebRequestContext request, Fornecedor transportadora){
		if(Util.objects.isNotPersistent(transportadora)){
			return new JsonModelAndView().addObject("isCorreios", false);
		}
		transportadora = FornecedorService.getInstance().load(transportadora, "fornecedor.cdpessoa, fornecedor.nome, fornecedor.correios");
		return new JsonModelAndView().addObject("isCorreios", transportadora != null && Boolean.TRUE.equals(transportadora.getCorreios()));
	}
	
	public ModelAndView ajaxValidaConsultaEventos(WebRequestContext request, Expedicao expedicao){
		expedicao = expedicaoService.loadForRastreio(expedicao);
		if(StringUtils.isBlank(expedicao.getRastreamento())){
			return new JsonModelAndView()
				.addObject("msg", "Expedi��o n�o possui c�digo de rastreamento dos correios.");
		}
		if(expedicao.getTransportadora() == null || !Boolean.TRUE.equals(expedicao.getTransportadora().getCorreios())){
			return new JsonModelAndView()
				.addObject("msg", "Expedi��o n�o vinculada ao fornecedor do tipo Correios.");
		}
		if(expedicao.getEmpresa() == null || StringUtils.isBlank(expedicao.getEmpresa().getSenhaCorreios()) || StringUtils.isBlank(expedicao.getEmpresa().getUsuarioCorreios())){
			return new JsonModelAndView()
				.addObject("msg", "Empresa n�o possui as configura��es dos correios.");
		}
		ConsultaEventoCorreios consultaEventoCorreios = consultaEventoCorreiosService.loadByData(expedicao.getRastreamento(), SinedDateUtils.currentDate());
		if(consultaEventoCorreios != null && Boolean.TRUE.equals(consultaEventoCorreios.getConsultaManualRealizada())){
			return new JsonModelAndView()
				.addObject("msg", "O sitema possui um limite de uma (1) consulta manual di�ria por objeto.\nEsse objeto j� foi consultado nesse dia.");
		}
		return new JsonModelAndView();
	}
	
	public void consultaEventoCorreios(WebRequestContext request, Expedicao expedicao){
		boolean entrada = "entrada".equals(request.getParameter("origem"));
		expedicao = expedicaoService.loadForRastreio(expedicao);
		List<Expedicao> listaExpedicao = expedicaoService.findForRastreamentoCorreios(expedicao.getEmpresa(), expedicao);
		try {
			CorreiosUtils.consulta(expedicao.getEmpresa(), listaExpedicao, null);
			request.addMessage("Consulta de eventos dos Correios realizada com sucesso.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao" + (entrada ? "?ACAO=consultar&cdexpedicao="+expedicao.getCdexpedicao() : ""));
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}