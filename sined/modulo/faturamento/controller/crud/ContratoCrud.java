package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.sf.json.JSON;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendainteracaocontratomodelo;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratoarquivo;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Contratodesconto;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialitem;
import br.com.linkcom.sined.geral.bean.Contratomateriallocacao;
import br.com.linkcom.sined.geral.bean.Contratoparcela;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresamodelocontrato;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Fatorajuste;
import br.com.linkcom.sined.geral.bean.Fatorajustevalor;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Propostasituacao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.bean.Requisicaoprioridade;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratomateriallocacaotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Romaneiotipo;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.service.AgendainteracaoService;
import br.com.linkcom.sined.geral.service.AgendainteracaocontratomodeloService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratoarquivoService;
import br.com.linkcom.sined.geral.service.ContratocolaboradorService;
import br.com.linkcom.sined.geral.service.ContratodescontoService;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaoService;
import br.com.linkcom.sined.geral.service.ContratohistoricoService;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.ContratomateriallocacaoService;
import br.com.linkcom.sined.geral.service.ContratomaterialredeService;
import br.com.linkcom.sined.geral.service.ContratoparcelaService;
import br.com.linkcom.sined.geral.service.ContratotipoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.DominioService;
import br.com.linkcom.sined.geral.service.DominioemailusuarioService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EmpresamodelocontratoService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EscalaService;
import br.com.linkcom.sined.geral.service.FatorajusteService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FrequenciaService;
import br.com.linkcom.sined.geral.service.GrupotributacaoService;
import br.com.linkcom.sined.geral.service.IndicecorrecaoService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialrelacionadoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OportunidadesituacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.ProducaoagendaService;
import br.com.linkcom.sined.geral.service.ProducaoagendahistoricoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.RequisicaohistoricoService;
import br.com.linkcom.sined.geral.service.RestricaoService;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.geral.service.RomaneioitemService;
import br.com.linkcom.sined.geral.service.RomaneioorigemService;
import br.com.linkcom.sined.geral.service.ServicobancoService;
import br.com.linkcom.sined.geral.service.ServicoftpService;
import br.com.linkcom.sined.geral.service.ServicoservidortipoService;
import br.com.linkcom.sined.geral.service.ServicowebService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.geral.service.VendaorcamentomaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ClienteInfoFinanceiraBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FaturamentoConjuntoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaMaterialItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.RealizarFechamentoRomaneioContrato;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.RealizarFechamentoRomaneioContratoItem;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.MaterialComboBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.ResumoRomaneio;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterial;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterialItem;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.SubstituirLocacaoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.SubstituirLocacaoItemBean;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/Contrato", authorizationModule=CrudAuthorizationModule.class)
public class ContratoCrud extends CrudControllerSined<ContratoFiltro, Contrato, Contrato> {
	
	public static final String ACAO_CONTRATO_LISTAGEM = "contratoListagem";
	public static final String ACAO_CONTRATO_ENTRADA = "contratoEntrada";

	private CentrocustoService centrocustoService;
	private ProjetoService projetoService;
	private RateioService rateioService;
	private ContratohistoricoService contratohistoricoService;
	private TaxaService taxaService;
	private ContratoService contratoService;
	private PropostaService propostaService;
	private MaterialService materialService;	
	private MaterialrelacionadoService materialrelacionadoService;
	private FrequenciaService frequenciaService;
	private RateioitemService rateioitemService;
	private ServicoservidortipoService servicoservidortipoService;
	private ContratomaterialService contratomaterialService;
	private RequisicaoService requisicaoService;
	private EscalaService escalaService;
	private EnderecoService enderecoService;
	private ContratocolaboradorService contratocolaboradorService;
	private OportunidadeService oportunidadeService;
	private ClienteService clienteService;
	private ServicoftpService servicoftpService;
	private ServicowebService servicowebService;
	private DominioemailusuarioService dominioemailusuarioService;
	private DominioService dominioService;
	private ServicobancoService servicobancoService;
	private ContratomaterialredeService contratomaterialredeService;
	private DocumentoService documentoService;
	private OportunidadesituacaoService oportunidadesituacaoService;
	private FatorajusteService fatorajusteService;
	private ParametrogeralService parametrogeralService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ComissionamentoService comissionamentoService;
	private ContratodescontoService contratodescontoService;
	private EmpresaService empresaService;
	private ContatoService contatoService;
	private ContagerencialService contagerencialService;
	private ContratoparcelaService contratoparcelaService;
	private IndicecorrecaoService indicecorrecaoService;
	private RomaneioService romaneioService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private RomaneioorigemService romaneioorigemService;
	private RomaneioitemService romaneioitemService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private ContaService contaService;
	private ContratomateriallocacaoService contratomateriallocacaoService;
	private ContratofaturalocacaoService contratofaturalocacaoService;
	private DocumentotipoService documentotipoService;
	private PatrimonioitemService patrimonioitemService;
	private MovpatrimonioService movpatrimonioService;
	private LocalarmazenagemService localarmazenagemService;
	private ContratotipoService contratotipoService;
	private RestricaoService restricaoService;
	private VendaorcamentoService vendaorcamentoService;
	private VendaorcamentomaterialService vendaorcamentomaterialService;
	private ContratoarquivoService contratoarquivoService;
	private VendaService vendaService;
	private GrupotributacaoService grupotributacaoService;
	private ColaboradorService colaboradorService;
	private AgendainteracaocontratomodeloService agendainteracaocontratomodeloService;
	private AgendainteracaoService agendainteracaoService;
	private RequisicaohistoricoService requisicaohistoricoService;
	private InventarioService inventarioService;
	private ContareceberService contareceberService;
	
	public void setContratoarquivoService(ContratoarquivoService contratoarquivoService) {this.contratoarquivoService = contratoarquivoService;}
	public void setMovpatrimonioService(MovpatrimonioService movpatrimonioService) {this.movpatrimonioService = movpatrimonioService;}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {this.patrimonioitemService = patrimonioitemService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setContratofaturalocacaoService(ContratofaturalocacaoService contratofaturalocacaoService) {this.contratofaturalocacaoService = contratofaturalocacaoService;}
	public void setProducaoagendahistoricoService(ProducaoagendahistoricoService producaoagendahistoricoService) {this.producaoagendahistoricoService = producaoagendahistoricoService;}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {this.producaoagendaService = producaoagendaService;}
	public void setRomaneioitemService(RomaneioitemService romaneioitemService) {this.romaneioitemService = romaneioitemService;}
	public void setRomaneioorigemService(RomaneioorigemService romaneioorigemService) {this.romaneioorigemService = romaneioorigemService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setRomaneioService(RomaneioService romaneioService) {this.romaneioService = romaneioService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setContratodescontoService(ContratodescontoService contratodescontoService) {this.contratodescontoService = contratodescontoService;}
	public void setContratomaterialredeService(ContratomaterialredeService contratomaterialredeService) {this.contratomaterialredeService = contratomaterialredeService;}
	public void setServicobancoService(ServicobancoService servicobancoService) {this.servicobancoService = servicobancoService;}
	public void setDominioService(DominioService dominioService) {this.dominioService = dominioService;}
	public void setDominioemailusuarioService(DominioemailusuarioService dominioemailusuarioService) {this.dominioemailusuarioService = dominioemailusuarioService;}
	public void setServicowebService(ServicowebService servicowebService) {this.servicowebService = servicowebService;}	
	public void setServicoftpService(ServicoftpService servicoftpService) {this.servicoftpService = servicoftpService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setContratocolaboradorService(ContratocolaboradorService contratocolaboradorService) {this.contratocolaboradorService = contratocolaboradorService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setFrequenciaService(FrequenciaService frequenciaService) {this.frequenciaService = frequenciaService;}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {this.materialrelacionadoService = materialrelacionadoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setPropostaService(PropostaService propostaService) {this.propostaService = propostaService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setTaxaService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {this.contratohistoricoService = contratohistoricoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setServicoservidortipoService(ServicoservidortipoService servicoservidortipoService) {this.servicoservidortipoService = servicoservidortipoService;}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {this.contratomaterialService = contratomaterialService;}
	public void setEscalaService(EscalaService escalaService) {this.escalaService = escalaService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}	
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}	
	public void setOportunidadesituacaoService(OportunidadesituacaoService oportunidadesituacaoService) {this.oportunidadesituacaoService = oportunidadesituacaoService;}
	public void setFatorajusteService(FatorajusteService fatorajusteService) {this.fatorajusteService = fatorajusteService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setFechamentofinanceiroService(	FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {this.comissionamentoService = comissionamentoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setContratoparcelaService(ContratoparcelaService contratoparcelaService) {this.contratoparcelaService = contratoparcelaService;}
	public void setIndicecorrecaoService(IndicecorrecaoService indicecorrecaoService) {this.indicecorrecaoService = indicecorrecaoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setContratomateriallocacaoService(ContratomateriallocacaoService contratomateriallocacaoService) {this.contratomateriallocacaoService = contratomateriallocacaoService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setContratotipoService(ContratotipoService contratotipoService) {this.contratotipoService = contratotipoService;}
	public void setRestricaoService (RestricaoService restricaoService){this.restricaoService = restricaoService;}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {this.vendaorcamentoService = vendaorcamentoService;}
	public void setVendaorcamentomaterialService(VendaorcamentomaterialService vendaorcamentomaterialService) {this.vendaorcamentomaterialService = vendaorcamentomaterialService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setAgendainteracaocontratomodeloService(AgendainteracaocontratomodeloService agendainteracaocontratomodeloService) {this.agendainteracaocontratomodeloService = agendainteracaocontratomodeloService;}
	public void setAgendainteracaoService(AgendainteracaoService agendainteracaoService) {this.agendainteracaoService = agendainteracaoService;}
	public void setRequisicaohistoricoService(RequisicaohistoricoService requisicaohistoricoService) {this.requisicaohistoricoService = requisicaohistoricoService;}
	public void setInventarioService(InventarioService inventarioService) {this.inventarioService = inventarioService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	
	@Override
	protected void listagem(WebRequestContext request, ContratoFiltro filtro) throws Exception {
		request.setAttribute("listaSituacao", Arrays.asList(SituacaoContrato.values()));
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		request.setAttribute("MOSTRAR_LIMITE_CREDITO", parametrogeralService.getBoolean(Parametrogeral.MOSTRAR_LIMITE_CREDITO));
		if(filtro.getExibirTotais() != null && filtro.getExibirTotais()){
			String whereIn = CollectionsUtil.listAndConcatenate(contratoService.findTodosContrato(filtro), "cdcontrato", ",");
			List<Contrato> listaContrato = contratoService.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc());
			Money total = new Money();
			for (Contrato contrato : listaContrato) {
				total = total.add(contrato.getValorContratoListagem());
			}
			filtro.setTotal(total);
		}
	}
	
	@Override
	protected ListagemResult<Contrato> getLista(WebRequestContext request, ContratoFiltro filtro) {
		ListagemResult<Contrato> lista = super.getLista(request, filtro);
		List<Contrato> list = lista.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcontrato", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(contratoService.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}
		
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Contrato form) throws Exception {
		request.setAttribute("contratolocacao", form.getContratotipo() != null && form.getContratotipo().getLocacao() != null && form.getContratotipo().getLocacao());
		List<Contato> listaContato = new ArrayList<Contato>();
		
		request.setAttribute("w3mpsbrIntegracao", "TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.W3MPSBR_INTEGRACAO)));
		
		if(form.getCdcontrato() != null){
			List<Contratomaterial> listaContratomaterial = form.getListaContratomaterial();
			if (listaContratomaterial != null && !listaContratomaterial.isEmpty()){
				for (Contratomaterial contratomaterial : listaContratomaterial) {
					if (contratomaterial.getServico() != null && contratomaterial.getServico().getMaterialtipo() != null){
						Servicoservidortipo tipo = servicoservidortipoService.findByMaterialtipo(contratomaterial.getServico().getMaterialtipo());
						if (tipo != null){
							contratomaterial.setCdservicoservidortipo(tipo.getCdservicoservidortipo());
							
							int size = 0;
							if (tipo.equals(Servicoservidortipo.FTP_OBJ)){
								size = servicoftpService.countContratomaterial(null, contratomaterial);
							} else if (tipo.equals(Servicoservidortipo.WEB_OBJ)){
								size = servicowebService.countContratomaterial(null, contratomaterial);
							} else if(tipo.equals(Servicoservidortipo.EMAIL_OBJ) || 
									tipo.equals(Servicoservidortipo.CAIXAPOSTAL_OBJ) || 
									tipo.equals(Servicoservidortipo.SMTP_OBJ) || 
									tipo.equals(Servicoservidortipo.SMTPAV_OBJ)) {
								size = dominioemailusuarioService.countContratomaterial(null, contratomaterial);
							} else if (tipo.equals(Servicoservidortipo.DOMINIO_OBJ)) {
								size = dominioService.countContratomaterial(null, contratomaterial);
							} else if (tipo.equals(Servicoservidortipo.DHCP_OBJ) || 
									tipo.equals(Servicoservidortipo.ARP_OBJ) || 
									tipo.equals(Servicoservidortipo.DHCP_ARP_OBJ) || 
									tipo.equals(Servicoservidortipo.AUTENTICACAO_RADIUS_OBJ) || 
									tipo.equals(Servicoservidortipo.HOTSPOT_OBJ) || 
									tipo.equals(Servicoservidortipo.DHCP_RADIUS_OBJ) || 
									tipo.equals(Servicoservidortipo.FIREWALL_OBJ) || 
									tipo.equals(Servicoservidortipo.NAT_OBJ) || 
									tipo.equals(Servicoservidortipo.BANDA_OBJ)){
								size = contratomaterialredeService.countContratomaterial(null, contratomaterial);
							} else if (tipo.equals(Servicoservidortipo.BANCO_DADOS_OBJ)) {
								size = servicobancoService.countContratomaterial(null, contratomaterial);
							}
							
							if(size == contratomaterial.getQtde().intValue()){
								contratomaterial.setDescricaotip("BRIEFCASE - SERVI�OS ESGOTADOS");
								contratomaterial.setImagedescricao("brief_red_icon.png");
							} else {
								contratomaterial.setDescricaotip("BRIEFCASE");
								contratomaterial.setImagedescricao("brief_green_icon.png");
							}
						}
					}
				}
			}	
		}
		
		if(form.getCdcontrato() == null)
			form.setDtassinatura(form.getDtinicio());
		
		List<Centrocusto> listaCentrocusto = null;		
		if(form.getCdcontrato() != null){
			if(form.getRateio() != null && form.getRateio().getCdrateio() != null && !request.getBindException().hasErrors()){
				form.setRateio(rateioService.findRateio(form.getRateio()));
				listaCentrocusto = (List<Centrocusto>) CollectionsUtil.getListProperty(form.getRateio().getListaRateioitem(), "centrocusto");
			}
			
			if(form.getTaxa() != null && form.getTaxa().getCdtaxa() != null){
				form.setTaxa(taxaService.findTaxa(form.getTaxa()));
			}
		}
		
		//Parametro geral que define se pode ser exibido nova linha para rateio
		String novaLinhaRateio = parametrogeralService.buscaValorPorNome(Parametrogeral.EXIBIR_NOVA_LINHA_RATEIO);
		String retornoNovaLinha = "false";
		if (novaLinhaRateio.toUpperCase().equals("TRUE"))
			retornoNovaLinha = "true";
//		boolean showNewLineButtonRateio = SinedUtil.showNewLineButtonRateio();
		request.setAttribute("exibirNovaLinhaRateio", retornoNovaLinha);
		
		if(form.getCdcontrato() == null && !novaLinhaRateio.toUpperCase().equals("TRUE")){
			List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
			Rateioitem rateioitem = new Rateioitem();
//			if(form.getOportunidade() != null){
//				rateioitem.setProjeto(form.getOportunidade().getProjeto());
//			}
			listaRateioitem.add(rateioitem);
			Rateio rateio = new Rateio();
			rateio.setListaRateioitem(listaRateioitem);
			form.setRateio(rateio);
		}
		
		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getCdcontrato() != null && form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
			List<Projeto> projetos = rateioitemService.getProjetos(form.getRateio().getListaRateioitem());
			listaProjeto = projetoService.findProjetosAbertosSemPermissao(projetos, true, null);
		} else {
			listaProjeto = projetoService.findProjetosAbertos(null, true, null);
		}
		request.setAttribute("listaProjeto", listaProjeto);
		
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("listaComissionamento", comissionamentoService.findForContrato());
		
		List<Frequencia> listaFrequencia = frequenciaService.findForAgendamentoContrato();
		request.setAttribute("listaFrequencia", listaFrequencia);
		
		List<Frequencia> listaFrequenciaRenovacao = new ArrayList<Frequencia>(listaFrequencia);
		listaFrequenciaRenovacao.remove(listaFrequenciaRenovacao.indexOf(new Frequencia(Frequencia.UNICA)));
		request.setAttribute("listaFrequenciaRenovacao", listaFrequenciaRenovacao);
		
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_CREDITO);
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(listaCentrocusto));
		
		request.setAttribute("listaMaterialPromocional", materialService.findServicoPromocional());
		
		
		request.setAttribute("stringOptionServicoJson", materialService.getOptionServicoJson());
//		request.setAttribute("stringOptionServico", materialService.getOptionServico());
		
		
		boolean haveParcelaCobrada = false;
		if(form.getListaContratoparcela() != null){
			for (Contratoparcela cp : form.getListaContratoparcela()) {
				if(cp.getCobrada() != null && cp.getCobrada()){
					haveParcelaCobrada = true;
					break;
				}
			}
		}
		request.setAttribute("haveParcelaCobrada", haveParcelaCobrada);
		
		if(form.getVendedor() != null && form.getVendedortipo() != null && form.getVendedortipo().equals(Vendedortipo.FORNECEDOR)){
			Fornecedor f = new Fornecedor();
			f.setCdpessoa(form.getVendedor().getCdpessoa());
			f.setNome(form.getVendedor().getNome());
			form.setFornecedor(f);
		}else if(form.getVendedor() != null && form.getVendedortipo() != null && form.getVendedortipo().equals(Vendedortipo.COLABORADOR)){
			Colaborador c = new Colaborador();
			c.setCdpessoa(form.getVendedor().getCdpessoa());
			c.setNome(form.getVendedor().getNome());
			form.setColaborador(c);
		}else if(form.getVendedortipo() == null && form.getVendedor() != null && form.getVendedor().getCdpessoa() != null){
			Colaborador c = new Colaborador();
			c.setCdpessoa(form.getVendedor().getCdpessoa());
			c.setNome(form.getVendedor().getNome());
			form.setColaborador(c);
		}
		
		if(form.getCdcontrato() != null){
			if(request.getBindException() == null || !request.getBindException().hasErrors()){
				form.setListaContratodesconto(contratodescontoService.findByContrato(form));
			}
			form.setListaContratohistorico(contratohistoricoService.findByContrato(form));
		}
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		
		String paramDatatermino = parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_FECHAR_CONTRATO_RETROATIVO);
		request.setAttribute("PERMITIR_FECHAR_CONTRATO_RETROATIVO", paramDatatermino != null && "FALSE".equals(paramDatatermino.toUpperCase()));
	
		if(request.getParameter("cdvendaorcamento") != null && !request.getParameter("cdvendaorcamento").equals("")){
			Vendaorcamento vendaorcamento = vendaorcamentoService.loadForEntrada(new Vendaorcamento(Integer.parseInt(request.getParameter("cdvendaorcamento"))));
			if (vendaorcamento.getCliente()!=null && vendaorcamento.getCliente().getCdpessoa()!=null){
				vendaorcamento.setCliente(clienteService.load(vendaorcamento.getCliente(), "cliente.cdpessoa, cliente.nome"));
			}
			if (vendaorcamento.getEmpresa()!=null && vendaorcamento.getEmpresa().getCdpessoa()!=null){
				vendaorcamento.setEmpresa(empresaService.loadForVenda(vendaorcamento.getEmpresa()));
			}
			if (vendaorcamento.getColaborador()!=null && vendaorcamento.getColaborador().getCdpessoa()!=null){
				vendaorcamento.setColaborador(colaboradorService.loadWithoutPermissao(vendaorcamento.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
			}
			
			vendaorcamento.setListavendaorcamentomaterial(vendaorcamentomaterialService.findByMaterial(vendaorcamento));
			form.setEmpresa(vendaorcamento.getEmpresa());
			form.setCliente(vendaorcamento.getCliente());
			form.setContato(vendaorcamento.getContato());
			form.setEnderecoentrega(vendaorcamento.getEndereco());
			
			if (SinedUtil.isListNotEmpty(vendaorcamento.getListavendaorcamentomaterial())){
				List <Contratomaterial> listaContratomaterial = new ListSet<Contratomaterial>(Contratomaterial.class);
				for (Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()){
					if (vendaorcamentomaterial.getMaterial() != null && vendaorcamentomaterial.getMaterial().getPatrimonio() != null && 
							vendaorcamentomaterial.getMaterial().getPatrimonio() && vendaorcamentomaterial.getQuantidade() != null &&
							vendaorcamentomaterial.getQuantidade() > 1) {
						for (int i = 0 ; i < vendaorcamentomaterial.getQuantidade() ; i++) {
							Contratomaterial contratomaterial = new Contratomaterial();
							
							contratomaterial.setServico(vendaorcamentomaterial.getMaterial());
							contratomaterial.setObservacao(vendaorcamentomaterial.getObservacao());
							contratomaterial.setValorunitario(vendaorcamentomaterial.getPreco());
							contratomaterial.setQtde(1d);
							Money descontoItem = new Money();
							if(vendaorcamento.getDesconto() != null){
								descontoItem = vendaorcamentomaterial.getDesconto().add(vendaorcamentoService.getValorDescontoVenda(vendaorcamento.getListavendaorcamentomaterial(), vendaorcamentomaterial, vendaorcamento.getDesconto(), null, true));								
							}else {
								descontoItem = vendaorcamentomaterial.getDesconto();
							}
							contratomaterial.setValordesconto(descontoItem);
							listaContratomaterial.add(contratomaterial);
						}
					} else {
						Contratomaterial contratomaterial = new Contratomaterial();
	
						contratomaterial.setServico(vendaorcamentomaterial.getMaterial());
						contratomaterial.setObservacao(vendaorcamentomaterial.getObservacao());
						contratomaterial.setValorunitario(vendaorcamentomaterial.getPreco());
						contratomaterial.setQtde(vendaorcamentomaterial.getQuantidade());
						Money descontoItem = new Money();
						if(vendaorcamento.getDesconto() != null){
							descontoItem = vendaorcamentomaterial.getDesconto().add(vendaorcamentoService.getValorDescontoVenda(vendaorcamento.getListavendaorcamentomaterial(), vendaorcamentomaterial, vendaorcamento.getDesconto(), null, true));								
						}else {
							descontoItem = vendaorcamentomaterial.getDesconto();
						}
						contratomaterial.setValordesconto(descontoItem);
						listaContratomaterial.add(contratomaterial);
					}
				}
				form.setListaContratomaterial(listaContratomaterial);
			}
			
			form.setValor(vendaorcamento.getValorfinal());
			form.setResponsavel(vendaorcamento.getColaborador());
			form.setVendaorcamento(new Vendaorcamento(vendaorcamento.getCdvendaorcamento()));
			request.setAttribute("cdVendaOrcamento", vendaorcamento.getCdvendaorcamento());
		}
		boolean obrigaCamposArvoreAcesso = StringUtils.isNotBlank(parametrogeralService.buscaValorPorNome(Parametrogeral.URL_SINC_CONTRATO_ARVOREACESSO));
		request.setAttribute("ObrigaCamposArvoreAcesso", obrigaCamposArvoreAcesso );

		if(form != null && form.getCdcontrato() != null && request.getBindException() != null && request.getBindException().hasErrors()){
			form.setListaContratoarquivo(contratoarquivoService.findByContrato(form));
		}
		
		if (form.getCliente()!= null){
			request.setAttribute("listaPrazopagamento", clienteService.findPrazoPagamento(form.getCliente(), null, false));
		}
		
		List<Endereco> listaEnderecoFaturamento = new ArrayList<Endereco>();
		List<Endereco> listaEnderecoEntrega = new ArrayList<Endereco>();
		List<Endereco> listaEnderecoCobranca = new ArrayList<Endereco>();
		
		if(form.getCliente() != null && form.getCliente().getCdpessoa() != null){
			listaEnderecoFaturamento = enderecoService.findEnderecosDoClienteDoTipo(form.getCliente(), Enderecotipo.UNICO, Enderecotipo.FATURAMENTO, Enderecotipo.OUTRO);
			listaEnderecoEntrega = enderecoService.findEnderecosDoClienteDoTipo(form.getCliente(), Enderecotipo.ENTREGA, Enderecotipo.INSTALACAO, Enderecotipo.UNICO, Enderecotipo.OUTRO);
			listaEnderecoCobranca = enderecoService.findEnderecosDoClienteDoTipo(form.getCliente(), Enderecotipo.UNICO, Enderecotipo.COBRANCA, Enderecotipo.OUTRO);
			listaContato = contatoService.getListContatoByCliente(form.getCliente(), Boolean.TRUE);
		}
		
		request.setAttribute("EXIBIR_CAMPO_EDIT_PATRIMONIO", parametrogeralService.getBoolean(Parametrogeral.EXIBIR_CAMPO_EDIT_PATRIMONIO));
		request.setAttribute("listaEnderecoCobranca", listaEnderecoCobranca);
		request.setAttribute("listaEnderecoEntrega", listaEnderecoEntrega);
		request.setAttribute("listaEnderecoFaturamento", listaEnderecoFaturamento);
		request.setAttribute("listaContato", listaContato);
		request.setAttribute("MOSTRAR_LIMITE_CREDITO", parametrogeralService.getBoolean(Parametrogeral.MOSTRAR_LIMITE_CREDITO));
	}
	
	
	@Override
	protected Contrato criar(WebRequestContext request, Contrato form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdcontrato() != null){
			if(SinedUtil.isUserHasAction("COPIAR_CONTRATO")){
				form = contratoService.loadForEntrada(form);
				
				Rateio rateio = null;
				Taxa taxa = null;
				
				if(form.getRateio() != null && form.getRateio().getCdrateio() != null){
					rateio = rateioService.findRateio(form.getRateio());
					rateioService.limpaReferenciaRateio(rateio);
				}
				
				if(form.getTaxa() != null && form.getTaxa().getCdtaxa() != null){
					taxa = taxaService.findTaxa(form.getTaxa());
					taxaService.limpaReferenciasTaxa(taxa);
				}
				
				if(form.getListaContratomaterial() != null && form.getListaContratomaterial().size() > 0){
					for (Contratomaterial item : form.getListaContratomaterial()) {
						item.setCdcontratomaterial(null);
					}
				}
				if(form.getListaContratocolaborador() != null && form.getListaContratocolaborador().size() > 0){
					for (Contratocolaborador item : form.getListaContratocolaborador()) {
						item.setCdcontratocolaborador(null);
					}
				}
				
				form.setTaxa(taxa);
				form.setRateio(rateio);
				
				form.setListaContratodesconto(null);
				form.setListaContratoparcela(null);
				form.setListaContratohistorico(null);
				form.setListaContratoarquivo(null);
				
				form.setIdentificador(null);
				form.setCdcontrato(null);
				form.setDtaltera(null);
				form.setCdusuarioaltera(null);
				form.setCliente(null);
				form.setFrequencia(null);
				form.setPrazopagamento(null);
				form.setDtconfirma(null);
				form.setDtcancelamento(null);
				form.setDtfim(null);
				form.setDtconclusao(null);
				
				return form;
				
			}else{
				throw new SinedException("A��o n�o permitida.");
			}
		}
		
		return super.criar(request, form);
	}
	
	@Override
	protected void validateBean(Contrato bean, BindException errors) {
		if(bean.getContratotipo() != null && bean.getContratotipo().getCdcontratotipo() != null){
			bean.setContratotipo(contratotipoService.load(bean.getContratotipo()));
		}

		if(bean.getFatorajuste() == null || bean.getFatorajuste().getCdfatorajuste() == null){
			if (bean.getValor().compareTo(new Money()) > 0 ) {
				errors.reject("001", "O campo valor n�o pode ser negativo.");
			}
		}
		try {
			rateioService.validateRateio(bean.getRateio(), bean.getValorContrato(bean.getDtproximovencimento()));
		} catch (SinedException e) {
			errors.reject("001",e.getMessage());
		}
		
		if(bean.getIdentificador() != null && !"".equals(bean.getIdentificador())){
			Contrato contrato = null;
			if(bean.getCdcontrato() != null){
				contrato = contratoService.load(bean, "contrato.cdcontrato, contrato.identificador");
			}
			if(((contrato != null && (contrato.getIdentificador() == null || !contrato.getIdentificador().equals(bean.getIdentificador()))) ||
					contrato == null) && 
					contratoService.existsIdentificadorEmpresa((bean.getCdcontrato() != null ? bean : null), bean.getIdentificador(), bean.getEmpresa())){
				errors.reject("001", "Identificador j� cadastrado no sistema.");
			}
		}else if(bean.getIdentificadorautomatico() != null && bean.getIdentificadorautomatico() && 
				bean.getCdcontrato() == null && bean.getEmpresa() != null){
			Integer proxIdentificador = empresaService.carregaProxIdentificadorContrato(bean.getEmpresa());
			if(proxIdentificador == null) proxIdentificador = 1;
			if(contratoService.existsIdentificadorEmpresa(null, proxIdentificador.toString(), bean.getEmpresa())){
				errors.reject("001", "Identificador (" + proxIdentificador + ") j� cadastrado no sistema. Favor verificar o identificador no cadastro da empresa.");
			}
		}
		
		if(bean.getCdcontrato() == null && bean.getCliente() != null && restricaoService.verificaRestricaoSemDtLiberacao(bean.getCliente()) && !SinedUtil.isUserHasAction("INCLUIRCONTRATOCLIENTERESTRICAO")){
			errors.reject("001", "Aten��o! O cliente " + bean.getCliente().getNome() + " tem restri��o sem libera��o.");
		}
		
		if(bean.getDtconclusao() != null && bean.getCdcontrato() != null){
			Contrato contratoValidacao = contratoService.loadForRomaneio(bean);
			List<RealizarFechamentoRomaneioContratoItem> listaFinal = makeListaFechamentoContrato(contratoValidacao, true);
			if (listaFinal != null && !listaFinal.isEmpty()){
				errors.reject("001","N�o � poss�vel finalizar o contrato, pois existe pend�ncia de Devolu��o/Indeniza��o.");			
			}
		}
	
		if(SinedUtil.isListNotEmpty(bean.getListaContratoarquivo())){
			for(Contratoarquivo contratoarquivo : bean.getListaContratoarquivo()){
				if(contratoarquivo.getCdcontratoarquivo() == null && (
						contratoarquivo.getArquivo() == null || contratoarquivo.getArquivo().getContent() == null || contratoarquivo.getArquivo().getContent().length == 0)){
					errors.reject("001","O campo Arquivo � obrigat�rio.");
				}
			}
		}
		
		if(bean.getContratotipo() != null && parametrogeralService.getBoolean(Parametrogeral.TIPOCONTRATO_UNICO) && contratoService.existeContratotipo(bean)){
			errors.reject("001", "J� existe contrato cadastrado para o cliente com o tipo de contrato informado.");
		}
		
		if (SinedUtil.isListNotEmpty(bean.getListaContratomaterial())) {
			String erro = "";
			
//			String whereIn = SinedUtil.listAndConcatenate(bean.getListaContratomaterial(), "patrimonioitem.cdpatrimonioitem", ",");
//			List<Patrimonioitem> listaPatrimonioItemComPlaqueta = patrimonioitemService.loadWithPlaqueta(whereIn);
			
			int i = 0;
			int j = 0;
			for (Contratomaterial cm : bean.getListaContratomaterial()) {
				j = 0;
				for (Contratomaterial cm2 : bean.getListaContratomaterial()) {
					if (i != j && cm.getPatrimonioitem() != null && cm2.getPatrimonioitem() != null && cm.getPatrimonioitem().equals(cm2.getPatrimonioitem())) {
						erro = "Existe(m) c�digo(s) de patrim�nio(s) repetido(s). � necess�rio corrigir antes de salvar.";
						break;
					}
					j++;
				}
				
				i++;
				
				if (!StringUtils.isEmpty(erro)) {
					errors.reject("001", erro);
					break;
				}
			}
		}
		boolean inclusao = bean.getCdcontrato() == null;
		if(inclusao && parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE)){
			if(clienteService.isExcedeLimiteCredito(bean.getCliente(), bean.getValorContrato())){
				ClienteInfoFinanceiraBean limiteCredito = vendaService.criarBeanClienteInfoFinanceira(null, bean.getCliente());
				errors.reject("001", "Cliente n�o possui limite de cr�dito. Saldo "+limiteCredito.getSaldolimite()+". <a href=\"javascript:exibirPopUpInfoFinanceira(" + bean.getCliente().getCdpessoa() + ") \" class=\"globalerror\">Visualizar resumo</a>");
			}
		}
		
		if(inclusao && bean.getCliente() != null){
			if(restricaoService.verificaRestricaoSemDtLiberacao(bean.getCliente())){
				errors.reject("001","N�o foi poss�vel salvar o contrato: Cliente com restri��o sem data de libera��o cadastrada.");
			}
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Contrato bean) throws Exception {
		
		List<Contratohistorico> listaHistorico;
		
		Contratohistorico ch = new Contratohistorico();
		ch.setDthistorico(new Timestamp(System.currentTimeMillis()));
		ch.setUsuario((Usuario)NeoWeb.getUser());
		
		boolean criado = bean.getCdcontrato() == null;
		boolean identificadorAutomatico = bean.getIdentificadorautomatico() != null && bean.getIdentificadorautomatico();
		
		if(criado){
			listaHistorico = new ArrayList<Contratohistorico>();
			ch.setAcao(Contratoacao.CRIADO);
			listaHistorico.add(ch);
			
			if(bean.getConfirmar() != null && bean.getConfirmar()){
				bean.setDtconfirma(new Date(System.currentTimeMillis()));
				Contratohistorico chaux = new Contratohistorico();
				chaux.setDthistorico(new Timestamp(System.currentTimeMillis()));
				chaux.setUsuario((Usuario)NeoWeb.getUser());
				chaux.setAcao(Contratoacao.CONFIRMADO);
				listaHistorico.add(chaux);
			}
			
			
			if(bean.getProposta() != null){
				ch.setObservacao("Origem proposta <a href=\"javascript:visualizarProposta(" + bean.getProposta().getCdproposta() + ")\">" + bean.getProposta().getCdproposta() + "</a>");
			}
			
			if(bean.getOportunidade() != null && bean.getOportunidade().getCdoportunidade() != null){
				Oportunidadesituacao oportunidadesituacao = oportunidadesituacaoService.findSituacaofinal();
				if(oportunidadesituacao != null){
					oportunidadeService.updateSituacao(bean.getOportunidade(), oportunidadesituacao);
					ch.setObservacao("Importa��o da Oportunidade <a href=\"javascript:visualizarOportunidade("+bean.getOportunidade().getCdoportunidade()+")\">"+bean.getOportunidade().getCdoportunidade()+"</a>.");
				}
			}
			
			if (bean.getVendaorcamento() != null){
				ch.setObservacao("Aprova��o de or�amento <a href=\"javascript:visualizarVendaorcamento(" + bean.getVendaorcamento().getCdvendaorcamento()+")\">"+ bean.getVendaorcamento().getCdvendaorcamento() + "</a>");
			}
		} else {
			listaHistorico = contratohistoricoService.findByContrato(bean);
			
			Contrato contrato = contratoService.load(bean);
			contrato.setRateio(rateioService.findRateio(contrato.getRateio()));
			contrato.setListaContratoparcela(contratoparcelaService.findByContrato(contrato));
			String observacao = contratoService.criaObservacaoHistorico(bean, contrato);
			
			ch.setAcao(Contratoacao.ALTERADO);
			
			if (!StringUtils.isEmpty(observacao)) {
				ch.setValor(contrato.getValor());
				ch.setDtproximovencimento(contrato.getDtproximovencimento());
				ch.setFormafaturamento(contrato.getFormafaturamento());
				ch.setHistorico(contrato.getHistorico());
				ch.setAnotacao(contrato.getAnotacao());
				ch.setObservacao((ch.getObservacao() != null ? ch.getObservacao() : "") + observacao);
				ch.setVisualizarContratoHistorico(Boolean.TRUE);
				
				StringBuilder rateio = new StringBuilder();
				for (Rateioitem ri : contrato.getRateio().getListaRateioitem()) {
					rateio.append("Conta gerencial: " + contagerencialService.loadForEntrada(ri.getContagerencial()).getDescricao() + "\n");
					rateio.append("Centro de custo: " + centrocustoService.loadForEntrada(ri.getCentrocusto()).getNome() + "\n");
					
					Projeto projeto = projetoService.loadForEntrada(ri.getProjeto());
					if (projeto != null) {
						rateio.append("Projeto: " + projeto.getNome() + "\n");
					}
					
					rateio.append("Percentual: " + ri.getPercentual() + "%\n");
					rateio.append("Valor: R$" + ri.getValor() + "-quebra-");
				}				
				ch.setRateio(rateio.toString());
				
				StringBuilder parcelas = new StringBuilder();
				for (Contratoparcela p : contrato.getListaContratoparcela()) {
					parcelas.append("Data de vencimento: " + SinedDateUtils.toString(p.getDtvencimento()) + "\n");
					parcelas.append("Valor da NF de Servi�o: R$" + p.getValornfservico() + "\n");
					parcelas.append("Valor da NF de Produto: R$" + p.getValornfproduto() + "\n");
					parcelas.append("Desconto: R$" + p.getDesconto() + "\n");
					parcelas.append("Valor: R$" + p.getValor() + "\n");
					parcelas.append("Aplicar �ndice: " + (p.getAplicarindice() != null && p.getAplicarindice() ? "Sim" : "N�o") + "\n");
					parcelas.append("Cobrada: " + (p.getCobrada() != null && p.getCobrada() ? "Sim" : "N�o") + "-quebra-");
				}				
				ch.setParcelas(parcelas.toString());
			}
			
			listaHistorico.add(ch);
		}
		
		bean.setListaContratohistorico(listaHistorico);
		
		if(Vendedortipo.FORNECEDOR.equals(bean.getVendedortipo()) && bean.getFornecedor() != null){			
			bean.setVendedor(new Pessoa(bean.getFornecedor().getCdpessoa()));
		}else
		if(Vendedortipo.COLABORADOR.equals(bean.getVendedortipo()) && bean.getColaborador() != null){			
			bean.setVendedor(new Pessoa(bean.getColaborador().getCdpessoa()));
		}
		
		try {
			this.validarProdutosServicos(request, bean);
			this.validarDttermino(request, bean);
			
			super.salvar(request, bean);
			
			if(criado && identificadorAutomatico){
				Integer proxIdentificador = empresaService.carregaProxIdentificadorContrato(bean.getEmpresa());
				if(proxIdentificador == null) proxIdentificador = 1;
				
				contratoService.updateIdentificador(bean, proxIdentificador.toString());
				bean.setIdentificador(proxIdentificador.toString());

				proxIdentificador++;
				empresaService.updateProximoIdentificadorContrato(bean.getEmpresa(), proxIdentificador);
			}
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_taxaitem_tipotaxa")) {
				throw new SinedException("N�o deve haver tipos de taxas iguais.");
			}else if (DatabaseError.isKeyPresent(e, "contrato_idx")) {
				throw new SinedException("Identificador j� cadastrado para outro contrato da empresa.");
			}else if(DatabaseError.isKeyPresent(e, "fk_autorizacaotrabalho_10") || DatabaseError.isKeyPresent(e, "fk_contratomateriallocacao_0")){
				throw new SinedException("Produto/Servi�o n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
			}else {
				e.printStackTrace();
				throw new SinedException("Ocorreu uma tentativa de viola��o de integridade de dados. A altera��o n�o pode ser completada");
			}
		}
		
		if(criado && parametrogeralService.getBoolean("ORDEMSERVICO_NOVOCONTRATO")){
			Requisicao requisicao = new Requisicao();
			
			if(bean.getIdentificador() != null && !bean.getIdentificador().trim().equals("")){
				requisicao.setOrdemservico(bean.getIdentificador());
			}
			requisicao.setContrato(bean);
			requisicao.setEmpresa(bean.getEmpresa());
			requisicao.setCliente(bean.getCliente());
			requisicao.setDtrequisicao(SinedDateUtils.currentDate());
			requisicao.setRequisicaoestado(new Requisicaoestado(Requisicaoestado.EMESPERA));
			requisicao.setRequisicaoprioridade(new Requisicaoprioridade(Requisicaoprioridade.MEDIA));
			requisicao.setDescricao("In�cio da execu��o do contrato: " + bean.getDescricao());
			requisicao.setColaboradorresponsavel(bean.getResponsavel());
			
			if(bean.getListaContratomaterial() != null && bean.getListaContratomaterial().size() > 0){
				List<Materialrequisicao> listaMateriaisrequisicao = new ArrayList<Materialrequisicao>();
				
				for(Contratomaterial contratomaterial : bean.getListaContratomaterial()){
					Materialrequisicao materialrequisicao = new Materialrequisicao();
					materialrequisicao.setMaterial(contratomaterial.getServico());
					materialrequisicao.setQuantidade(contratomaterial.getQtde().doubleValue());
					materialrequisicao.setValorunitario(contratomaterial.getValorunitario());
					materialrequisicao.setValortotal(contratomaterial.getValortotal().getValue().doubleValue());
					
					listaMateriaisrequisicao.add(materialrequisicao);
				}
				
				requisicao.setListaMateriaisrequisicao(listaMateriaisrequisicao);
			}
			
			requisicaoService.saveOrUpdate(requisicao);
			
			Contratohistorico contratohistorico = new Contratohistorico(new Contrato(bean.getCdcontrato()),
																		new Timestamp(System.currentTimeMillis()),
																		Contratoacao.GERAR_OS,
																		SinedUtil.getUsuarioLogado());

			contratohistorico.setObservacao("Gerada automaticamente a Ordem de Servi�o <a href=\"javascript:visualizaOS("+requisicao.getCdrequisicao()+");\">"+requisicao.getCdrequisicao() +"</a>.");
			contratohistoricoService.saveOrUpdate(contratohistorico);
			
			String observacao = "(Criado) Origem: Contrato <a href=\"javascript:visualizarContrato("+ bean.getCdcontrato()+");\">"+bean.getCdcontrato()+"</a>.";
			requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, observacao, requisicao));
			
			boolean paramCliente = parametrogeralService.getValorPorNome("NotificacaoClienteRequisicao").toUpperCase().equals("TRUE");
			try {
				requisicaoService.enviaEmailRequisicao(request, requisicao, false, true, false, paramCliente, null, requisicao.getObservacaoInterna());
			} catch (SinedException e) {
//				request.addMessage("N�o foi poss�vel enviar o email para o respons�vel. " + e.getMessage(), MessageType.WARN);
				request.addMessage(e.getMessage(), MessageType.ERROR);
			} catch (Exception e) {
//				request.addMessage("N�o foi poss�vel enviar o email para o respons�vel. " + e.getMessage(), MessageType.WARN);
				request.addMessage("N�o foi poss�vel enviar o e-mail." + e.getMessage(), MessageType.ERROR);
				e.printStackTrace();
			}
			
			if(bean.getProposta() != null){
				propostaService.criaInteracaoByContrato(bean.getProposta(), bean);
			}
		}
		
		if (criado && bean.getVendaorcamento() != null){
			vendaorcamentoService.autorizarComoContrato(bean.getVendaorcamento(), bean);	
		}
		
		if(criado && bean.getContratotipo() != null){
			List<Agendainteracaocontratomodelo> listaModelosAgendainteracao = agendainteracaocontratomodeloService.findBy(bean.getContratotipo(), "cdagendainteracaocontratomodelo");
			if(!listaModelosAgendainteracao.isEmpty()){
				agendainteracaoService.createAgendainteracaoByContratotipo(bean);
			}
		}
		
		if(StringUtils.isNotBlank(parametrogeralService.buscaValorPorNome(Parametrogeral.URL_SINC_CONTRATO_ARVOREACESSO))){
			clienteService.sincronizarClienteArvoreAcesso(bean.getCliente());
		}
	}
	
	public ModelAndView visualizarContrato(WebRequestContext request, Contratohistorico contratohistorico){
		ModelAndView modelAndView = new ModelAndView("direct:crud/popup/visualizacaoContrato");
		
		if(contratohistorico == null)
			throw new SinedException("N�o foi poss�vel encontrar o contrato em nosso sistema.");

		request.setAttribute("TEMPLATE_beanName", contratohistorico);
		request.setAttribute("TEMPLATE_beanClass", Contratohistorico.class);
		request.setAttribute("descricao", "Contrato");
		request.setAttribute("consultar", true);
		request.setAttribute("classe", "receber");

		contratohistorico = contratohistoricoService.findForVisualizacao(contratohistorico);
		
		if (!StringUtils.isEmpty(contratohistorico.getRateio())) {
			contratohistorico.setListaRateio(Arrays.asList(contratohistorico.getRateio().split("-quebra-")));
		}
		
		if (!StringUtils.isEmpty(contratohistorico.getParcelas())) {
			contratohistorico.setListaParcela(Arrays.asList(contratohistorico.getParcelas().split("-quebra-")));
		}
		
		modelAndView.addObject("contratohistorico", contratohistorico);

		return modelAndView;
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Contrato bean) {
		if(bean.getProposta() != null){
			request.clearMessages();
			request.addMessage("Contrato gerado com sucesso.");
			return new ModelAndView("redirect:/crm/crud/Proposta");
		}
		if(bean.getOportunidade() != null){
			request.clearMessages();
			request.addMessage("Contrato gerado com sucesso.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	/**
	 * Action que abre a confirma��o do agrupamento do faturamento do contrato.
	 *
	 * @see br.com.linkcom.sined.geral.service.ContratoService#haveContratoSituacao
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView abrirConfirmacaoAgrupamento(WebRequestContext request){
		Object obj = request.getSession().getAttribute("CODIGOS_CONTRATO_FATURAMENTO");
		if(obj == null || obj.toString().equals("")){
			request.addError("Nenhum item selecionado");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		String whereIn = obj.toString();
		
		if(isFaturamentoWS(request, whereIn, "popup")){
			return null;
		}
		
		//Verifica��o de data limite do ultimo fechamento. 
		Contrato aux = new Contrato();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem contratos cuja data de pr�ximo vencimento refere-se a um per�odo j� fechado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(contratoService.isMunicipioBrasiliaAndNotContratomaterial(whereIn)){
			request.addError("S� � permitido gerar nota de contrato(s) com material(is) para o munic�pio de Bras�lia.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String faturarSomenteClientesAdimplentes = request.getParameter("faturarSomenteClientesAdimplentes");
		
		request.setAttribute("entrada", entrada);
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("faturarSomenteClientesAdimplentes", faturarSomenteClientesAdimplentes);
		
		boolean faturamentolote = contratoService.existeFaturamentolote(whereIn);
		
		if(whereIn.split(",").length > 1 && faturamentolote){  
			return new ModelAndView("direct:/crud/popup/confirmacaoAgrupamento");
		} else {
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/crud/Contrato?ACAO=gerarFaturamento&agrupamento=false&entrada=" + entrada + "&faturarSomenteClientesAdimplentes="+faturarSomenteClientesAdimplentes+"';</script>");
			return null;
		}
	}
	
	/**
	 * M�todo para abrir popup faturarPorMedicao (apenas para 1 contrato)
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirFaturarPorMedicao(WebRequestContext request){
		
		contratoService.atualizaValorContratoFatorAjuste();
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return new ModelAndView();
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		if(isFaturamentoWS(request, whereIn, "redirecionamento")){
			return null;
		}
		
		if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.ATRASADO, SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.EM_ESPERA, SituacaoContrato.CANCELADO, SituacaoContrato.ISENTO, SituacaoContrato.SUSPENSO)){
			request.addError("Para gerar faturamento, o(s) contrato(s) deve(m) estar com situa��o 'A CONSOLIDAR' ou 'ATEN��O'.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
			return null;
		}
		
		if(contratoService.isMunicipioBrasiliaAndNotContratomaterial(whereIn)){
			request.addError("S� � permitido gerar nota de contrato(s) com material(is) para o munic�pio de Bras�lia.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
			return null;
		}
		
		request.setAttribute("entrada", entrada);
		request.setAttribute("selectedItens", whereIn);
				
		List<Contrato> listaContrato = contratoService.findForFaturarPorMedicao(whereIn);
		
		// VALIDA SE EXISTE ALGUM CONTRATO COM PRODUTO E/OU SERVI�O CADASTRADO
		for (Contrato c : listaContrato) {
			if(c.getListaContratomaterial() != null && c.getListaContratomaterial().size() > 0 && (c.getIgnoravalormaterial() == null || !c.getIgnoravalormaterial())){
				request.addError("N�o � poss�vel faturar por medi��o contrato que possui Produtos e/ou Servi�os cadastrados.");						
				SinedUtil.redirecionamento(request, "/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
				return null;						
			}
		}
		
		Contrato contrato = new Contrato();
		if(listaContrato != null && !listaContrato.isEmpty()){			
			contrato = listaContrato.get(0);
			contrato.setEntrada(entrada);
			
			Money valor = contrato.getValorQtde();
			Money valorAcrescimo = contrato.getValoracrescimo();
			if(valorAcrescimo != null && valorAcrescimo.getValue().doubleValue() > 0){
				valor = valor.add(valorAcrescimo);
			}
			Money valorFaturado = contratoService.findForTotalFaturado(contrato);
			
			contrato.setValor(valor);
			contrato.setValorContratoAFaturarPorMedicao(valor);
			contrato.setInscricaoMunicipal(contrato.getCliente() != null && contrato.getCliente().getInscricaomunicipal() != null ? contrato.getCliente().getInscricaomunicipal() : "");
			contrato.setValorContratoFaturado(valorFaturado);
			if(valor != null && valorFaturado != null){
				contrato.setValorContratoAFaturarPorMedicao(valor.subtract(valorFaturado));
			}
		}
					
		request.setAttribute("acaoSalvar", "gerarFaturamentoPorMedicao");	
		request.setAttribute("agrupamento", false);
		
		return new ModelAndView("direct:/crud/popup/faturarPorMedicao", "bean", contrato);
	}
	
	
	/**
	 * M�todo para gerar o faturamento por medi��o (apenas para 1 contrato)
	 *
	 * @param request
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarFaturamentoPorMedicao(WebRequestContext request, Contrato contrato){

		if(contrato != null && contrato.getCdcontrato() != null){			
			if(contrato.getEntrada() != null)
				request.setAttribute("entrada", contrato.getEntrada());
			else
				request.setAttribute("entrada", Boolean.FALSE);
			
			Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
			Boolean agrupamento = Boolean.valueOf(request.getParameter("agrupamento") != null ? request.getParameter("agrupamento") : "false");
			
			try{
				List<Contrato> listaContrato = contratoService.findForCobranca(contrato.getCdcontrato().toString());
				
				// VALIDA SE EXISTE ALGUM CONTRATO COM PRODUTO E/OU SERVI�O CADASTRADO
				for (Contrato c : listaContrato) {
					if(c.getListaContratomaterial() != null && c.getListaContratomaterial().size() > 0 && (c.getIgnoravalormaterial() == null || !c.getIgnoravalormaterial())){
						request.addError("N�o � poss�vel faturar um contrato por medi��o que possui Produtos e/ou Servi�os cadastrados.");						
						return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+contrato.getCdcontrato() : ""));						
					}
				}
				
				if(contrato.getValorContratoAFaturarPorMedicao() != null && listaContrato.get(0).getValor().compareTo(contrato.getValorContratoAFaturarPorMedicao()) != 0){
					listaContrato.get(0).setValor(contrato.getValorContratoAFaturarPorMedicao());
					listaContrato.get(0).setValorContratoAFaturarPorMedicao(contrato.getValorContratoAFaturarPorMedicao());
				}
				if(contrato.getInscricaoMunicipal() != null && !contrato.getInscricaoMunicipal().equals(""))
					listaContrato.get(0).setInscricaoMunicipal(contrato.getInscricaoMunicipal());
				
				String acao;
				if(entrada != null && entrada){
					acao = ACAO_CONTRATO_ENTRADA;
				} else {
					acao = ACAO_CONTRATO_LISTAGEM;
				}
				if(listaContrato != null && listaContrato.size() == 1 && (listaContrato.get(0).getFormafaturamento() != null && listaContrato.get(0).getFormafaturamento().equals(Formafaturamento.SOMENTE_BOLETO)) ){
					if(listaContrato.get(0).getFatorajuste() != null && listaContrato.get(0).getFatorajuste().getCdfatorajuste() != null){
						if(!verificaExistenciaMesAnoFatorajuste(listaContrato.get(0))){						
							request.addError("N�o existe Fator de ajuste lan�ado para este m�s.");						
							return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+contrato.getCdcontrato() : ""));						
						}
					}
					
					request.getServletResponse().setContentType("text/html");
					View.getCurrent().println(
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
							"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/financeiro/crud/Contareceber?ACAO=criar&cdcontrato=" + 
																	listaContrato.get(0).getCdcontrato()+
																	"&nomeProcessoFluxoAnterior=" + acao + 
																	"&faturarPorMedicao=true&valorFatuarPorMedicao=" + listaContrato.get(0).getValor().getValue().doubleValue() + "';</script>");
					return null;	
				} else {
					Boolean erro = Boolean.FALSE;
					for(Contrato c : listaContrato){
						if(c.getFatorajuste() != null && c.getFatorajuste().getCdfatorajuste() != null){
							if(!verificaExistenciaMesAnoFatorajuste(c)){
								request.addError("N�o existe Fator de ajuste lan�ado para este m�s. Contrato " + c.getCdcontrato());
								erro = Boolean.TRUE;
							}
						}
					}
					if(erro) {
						request.getServletResponse().setContentType("text/html");
						View.getCurrent().println(
								"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
								"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
								"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
								"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
								"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+contrato.getCdcontrato() : "") + "';</script>");
						return null;	
					}
					for(Contrato c: listaContrato)
						c = contratoService.selecionarEnderecoAlternativoDeCobranca(c);
					
					String msg = "";
					if(restricaoService.verificaRestricaoSemDtLiberacao(listaContrato.get(0).getCliente()) && parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE)){
						request.addError("N�o foi poss�vel faturar por medi��o: Cliente com restri��o sem data de libera��o cadastrada.");
						SinedUtil.fechaPopUp(request);
						return null;
					}else{
						msg = contratoService.gerarFaturamentoPorMedicao(listaContrato.get(0), agrupamento);
					}
					if(msg != null && !msg.equals("")){
						if(msg.contains("sucesso")){
							String cdnotafiscalservico = msg.substring(msg.indexOf("=")+1);
							if(cdnotafiscalservico != null && !cdnotafiscalservico.equals("") && SinedUtil.validaNumeros(cdnotafiscalservico)){
								request.addMessage("Faturamento por Medi��o gerado com sucesso.");
								SinedUtil.redirecionamento(request, "/faturamento/crud/NotaFiscalServico?ACAO=editar&cdNota=" + cdnotafiscalservico + 
																	"&fluxoFaturarPorMedicao=true" +
																	"&cdcontrato=" + listaContrato.get(0).getCdcontrato() );
								return null;
							}
						}
						request.addMessage(msg, MessageType.INFO);
					}
					request.addMessage("Faturamento por Medi��o gerado com sucesso.");
					
					request.getServletResponse().setContentType("text/html");
					View.getCurrent().println(
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
							"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+contrato.getCdcontrato() : "") + "';</script>");
					return null;										
				}
			} catch (Exception e) {
				e.printStackTrace();
				request.addError(e.getMessage());
			}	
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+contrato.getCdcontrato() : "") + "';</script>");
			return null;				
//			return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+contrato.getCdcontrato() : ""));
		}else{
			request.addError("Nenhum contrato selecionado.");			
			return new ModelAndView("direct:/faturamento/crud/Contrato");
		}
		
	}
	
	/**
	 * Action que gera o faturamento dos contratos futuros.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarFaturamentoFuturo(WebRequestContext request){
		boolean sucesso = false;
		contratoService.atualizaValorContratoFatorAjuste();
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		Integer parcelas = Integer.parseInt(request.getParameter("parcelas") != null ? request.getParameter("parcelas") : "1");
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Contrato> listaContrato;
		
		StringBuilder erros = new StringBuilder();
		for (int i = 0; i < parcelas; i++) {
			listaContrato = contratoService.findForCobranca(whereIn);
			Boolean erro = Boolean.FALSE;
			if(listaContrato != null && !listaContrato.isEmpty()){
				for(Contrato contrato : listaContrato){
					if(contrato.getFatorajuste() != null && contrato.getFatorajuste().getCdfatorajuste() != null &&
							!verificaExistenciaMesAnoFatorajuste(contrato)){						
						request.addError("N�o existe Fator de ajuste lan�ado para este m�s. Contrato " + contrato.getCdcontrato());					
						erro = Boolean.TRUE;
					}
				}
			}
			
			if(erro)
				return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
			
			try {
				for(Contrato c: listaContrato)
					c = contratoService.selecionarEnderecoAlternativoDeCobranca(c);
				String msg = contratoService.gerarFaturamento(listaContrato, false);
				if(msg != null && !msg.equals(""))
					request.addMessage(msg, MessageType.INFO);
				sucesso = true;
			} catch (Exception e) {
				if(erros.indexOf(e.getMessage()) == -1){
					erros.append(e.getMessage());
				}
			}
		}
		
		if(erros.length() > 0){
			request.addError(erros.toString());
		}
		
		if(sucesso){
			request.addMessage("Faturamento(s) gerado(s) com sucesso.");
		}
		return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
	}
	
	/**
	 * Action que gera o faturamento dos contratos.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarFaturamento(WebRequestContext request){
	
		contratoService.atualizaValorContratoFatorAjuste();
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		Boolean agrupamento = Boolean.valueOf(request.getParameter("agrupamento") != null ? request.getParameter("agrupamento") : "false");
		
		Object obj = null;
		if(entrada){
			obj = SinedUtil.getItensSelecionados(request);
		}else {
			obj = request.getSession().getAttribute("CODIGOS_CONTRATO_FATURAMENTO");
		}
		
		if(obj == null || obj.toString().equals("")){
			request.addError("Nenhum item selecionado");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		String whereIn = obj.toString();
		
		if(isFaturamentoWS(request, whereIn, "modelandview")){
			return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
		}
		
		if(contratoService.haveContratoSituacao(whereIn, 
				SituacaoContrato.ATRASADO, 
				SituacaoContrato.FINALIZADO, 
				SituacaoContrato.NORMAL, 
				SituacaoContrato.EM_ESPERA, 
				SituacaoContrato.CANCELADO,
				SituacaoContrato.ISENTO,
				SituacaoContrato.SUSPENSO)){
			request.addError("Para gerar faturamento, o(s) contrato(s) deve(m) estar com situa��o 'A CONSOLIDAR' ou 'ATEN��O'.");
			return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
		}
		
		if(contratoService.isMunicipioBrasiliaAndNotContratomaterial(whereIn)){
			request.addError("S� � permitido gerar nota de contrato(s) com material(is) para o munic�pio de Bras�lia.");
			return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
		}
		
		try{
			List<Contrato> listaContrato = contratoService.findForCobranca(whereIn);
			if("true".equals(request.getParameter("faturarSomenteClientesAdimplentes"))){
				listaContrato = contratoService.removeContratosClientesInadimplemntes(listaContrato);
			}
			
			if(SinedUtil.isListNotEmpty(listaContrato)){
				
				for (Contrato c : listaContrato) {
					c = contratoService.selecionarEnderecoFaturamento(c);
					if(c.getEnderecoPrioritario() == null){
						request.addError("Nenhum endere�o cadastrado para o cliente " + c.getCliente().getNome() + ".");						
						return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
					}
				}
			}
			
			if(listaContrato != null && listaContrato.size() == 1 && (listaContrato.get(0).getFormafaturamento() != null && listaContrato.get(0).getFormafaturamento().equals(Formafaturamento.SOMENTE_BOLETO)) ){
				if(listaContrato.get(0).getFatorajuste() != null && listaContrato.get(0).getFatorajuste().getCdfatorajuste() != null){
					if(!verificaExistenciaMesAnoFatorajuste(listaContrato.get(0))){						
						request.addError("N�o existe Fator de ajuste lan�ado para este m�s.");						
						return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));						
					}
				}
				String acao;
				
				if(entrada != null && entrada){
					acao = ACAO_CONTRATO_ENTRADA;
				} else {
					acao = ACAO_CONTRATO_LISTAGEM;
				}
				return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=criar&cdcontrato=" + listaContrato.get(0).getCdcontrato() + "&nomeProcessoFluxoAnterior=" + acao + "&subfluxo=gerarFaturamento");
			} else {
				Boolean erro = Boolean.FALSE;
				for(Contrato contrato : listaContrato){
					if(contrato.getFatorajuste() != null && contrato.getFatorajuste().getCdfatorajuste() != null){
						if(!verificaExistenciaMesAnoFatorajuste(contrato)){
							request.addError("N�o existe Fator de ajuste lan�ado para este m�s. Contrato " + contrato.getCdcontrato());
							erro = Boolean.TRUE;
						}
					}
				}
				if(erro)
					return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
				
			
				if(parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE)){
					Map<Integer, Boolean> idVerificado = new HashMap<Integer, Boolean>();
					List<Contrato> newListaContrato = new ArrayList<Contrato>();
					String message = "";
					for(Contrato contrato: listaContrato){
						if(!idVerificado.containsKey(contrato.getCliente().getCdpessoa())){
							Boolean contaisRestricao = restricaoService.verificaRestricaoSemDtLiberacao(contrato.getCliente());
							
							if(contaisRestricao){
								if(message.equals(""))
									message += "N�o foi poss�vel faturar os(s) contratos(s): <a href='javascript:visualizarContratoCrud("
											+ contrato.getCdcontrato() +")' title=''>"
											+ contrato.getCdcontrato() +"</a>";
								else
									message += ", <a href='javascript:visualizarContratoCrud("
											+ contrato.getCdcontrato() +")' title=''>"
											+ contrato.getCdcontrato() +"</a>";						
							} else {								
								newListaContrato.add(contrato);							
							}
							idVerificado.put(contrato.getCliente().getCdpessoa(), contaisRestricao);
							
						}else{
						  if(idVerificado.get(contrato.getCliente().getCdpessoa())){
							  message += ", <a href='javascript:visualizarContratoCrud(" + contrato.getCdcontrato()+ ")' title=''>"
									  + contrato.getCdcontrato()+ "</a>";
							  
						  } else {
							  newListaContrato.add(contrato);  							  
						  }
						}
					}
					if(idVerificado.keySet().size() == 1 && !message.equals("")){
						request.addError("N�o foi poss�vel faturar o(s) contrato(s): Cliente com restri��o sem data de libera��o cadastrada.");
					}else{
						if(!message.equals("")) 
							request.addMessage(message + ", pois possui clientes com restri��o sem data de libera��o cadastrada.", MessageType.WARN);
						String msg = contratoService.gerarFaturamento(newListaContrato, agrupamento);
						if(msg != null && !msg.equals("")) 
							request.addMessage(msg, MessageType.INFO);
						request.addMessage(newListaContrato.size() + " Contrato(s) faturado(s) com sucesso.");
					}
				} else {
					String msg = contratoService.gerarFaturamento(listaContrato, agrupamento);
					if(msg != null && !msg.equals("")) 
						request.addMessage(msg, MessageType.INFO);
					request.addMessage("Faturamento(s) gerado(s) com sucesso.");					
				}
			
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}

		return new ModelAndView("redirect:/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
	}
	
	/**
	* M�todo que verifica se existe fator de ajuste lan�ado para o m�s atual
	*
	* @param contrato
	* @return
	* @since Nov 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public Boolean verificaExistenciaMesAnoFatorajuste(Contrato contrato){
		
		if(contrato.getFatorajuste() != null && contrato.getFatorajuste().getCdfatorajuste() != null){
			Fatorajuste fatorajuste = fatorajusteService.carregaFatorajuste(contrato.getFatorajuste());			
			if(fatorajuste != null && fatorajuste.getListaFatorajustevalor() != null){
				String dtAtual = new SimpleDateFormat("MM/yyyy").format(System.currentTimeMillis());
				for(Fatorajustevalor fatorajustevalor : fatorajuste.getListaFatorajustevalor()){
					if(fatorajustevalor.getMesanoAux() != null && fatorajustevalor.getMesanoAux().equals(dtAtual)){
						return Boolean.TRUE;
					}
				}
			}		
		}
		
		return Boolean.FALSE;
	}
	
	/**
	 * A��o que abre a confirma��o do tipo de agrupamento que ser� feito para a ger��o das notas.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("abrirconfirmacao")
	public ModelAndView abrirConfirmacao(WebRequestContext request) {
		String whereIn = request.getParameter("selectedItens");
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		if(isFaturamentoWS(request, whereIn, "popup")){
			return null;
		}
		
		if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.ATRASADO, SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.EM_ESPERA, SituacaoContrato.CANCELADO, SituacaoContrato.ISENTO, SituacaoContrato.SUSPENSO)){
			request.addError("Para gerar faturamento, o(s) contrato(s) deve(m) estar com situa��o 'A CONSOLIDAR' ou 'ATEN��O'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		request.setAttribute("parcelas", request.getParameter("parcelas"));
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("entrada", entrada);
		return new ModelAndView("direct:/crud/popup/confirmacaoAgrupamento");
	}

	@Action("abrirParcelas")
	public ModelAndView abrirParcelas(WebRequestContext request) {
		String whereIn = request.getParameter("selectedItens");
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		if(isFaturamentoWS(request, whereIn, "popup")){
			return null;
		}
		
		if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.ATRASADO, SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.EM_ESPERA, SituacaoContrato.CANCELADO, SituacaoContrato.ISENTO, SituacaoContrato.SUSPENSO)){
			request.addError("Para gerar faturamento, o(s) contrato(s) deve(m) estar com situa��o 'A CONSOLIDAR' ou 'ATEN��O'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(contratoService.haveContratoGerarNotaAntes(whereIn)){
			request.addError("Para gerar faturamento futuro, o(s) contrato(s) deve(m) ter a forma de faturamento 'Somente Boleto' ou 'Nota ap�s Recebimento'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
			
		//Verifica��o de data limite do ultimo fechamento. 
		Contrato aux = new Contrato();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem contratos cuja data de pr�ximo vencimento refere-se a um per�odo j� fechado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		// VERIFICA��O DO N�MERO M�XIMO DE PARCELAS PERMITIDA.
		Integer numMaximoParcelas = 0;
		List<Contrato> listaContratoPrazopagamento = contratoService.findSomenteComParcelasEmAberto(whereIn);
		if(listaContratoPrazopagamento != null){
			for (Contrato contrato : listaContratoPrazopagamento) {
				Integer maximoAux = 0;
				if(contrato.getListaContratoparcela() != null && contrato.getListaContratoparcela().size() > 0){
					for (Contratoparcela cp : contrato.getListaContratoparcela()) {
						if(cp.getCobrada() == null || !cp.getCobrada()){
							maximoAux++;
						}
					}
				}
				if(maximoAux != 0 && (numMaximoParcelas == 0 || maximoAux < numMaximoParcelas)){
					numMaximoParcelas = maximoAux;
				}
			}
		}
		
		request.setAttribute("numMaximoParcelas", numMaximoParcelas);
		request.setAttribute("parcelas", request.getParameter("parcelas"));
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("entrada", entrada);
		
		return new ModelAndView("direct:/crud/popup/parcelasContrato");
	}
	
	/**
	 * Action que tem origem na tela de proposta que sugere alguns campos da proposta.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PropostaService#loadForContrato
	 * @see br.com.linkcom.sined.geral.service.ContratoService#gerarContrato
	 *
	 * @param request
	 * @param proposta
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@Action("gerarContrato")
	public ModelAndView gerarContrato(WebRequestContext request, Proposta proposta) throws Exception{
		if(proposta.getCdproposta() == null){
			request.addError("Nenhum item selecionado");
			return new ModelAndView("redirect:/crm/crud/Proposta");
		}
		
		proposta = propostaService.loadForContrato(proposta);
		if(proposta == null || !Propostasituacao.GANHOU.equals(proposta.getPropostasituacao())){
			request.addError("S� � permitido gerar contrato de proposta com situa��o 'GANHOU'.");
			return new ModelAndView("redirect:/crm/crud/Proposta");
		}
		
		Contrato form = contratoService.gerarContrato(proposta);
		return getCriarModelAndView(request, form);
	}
	
	/**
	 * Via ajax ele busca a lista de material de uma promo��o.
	 *
	 * @param request
	 * @param material
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public void ajaxMateriaisPromocionais(WebRequestContext request, Contrato contrato) throws Exception{
		List<Materialrelacionado> listaMaterial = materialrelacionadoService.findByPromocao(contrato.getMaterialpromocional());
		
		Integer qtdePromocional = contrato.getQtdePromocional() != null ? contrato.getQtdePromocional() : 1; 
		MaterialComboBean comboBean;
		List<MaterialComboBean> lista = new ArrayList<MaterialComboBean>();
		for (Materialrelacionado mat : listaMaterial) {
			if(mat.getMaterial() != null){
				comboBean = new MaterialComboBean();
				
				comboBean.setCdmaterial(mat.getMaterialpromocao().getCdmaterial());
				comboBean.setNome(mat.getMaterialpromocao().getNome());
				if(mat.getMaterialpromocao().getCentrocustovenda() != null){
					comboBean.setCdcentrocustovenda(mat.getMaterialpromocao().getCentrocustovenda().getCdcentrocusto());
				}
				if(mat.getMaterialpromocao().getContagerencialvenda() != null){
					comboBean.setCdcontagerencialvenda(mat.getMaterialpromocao().getContagerencialvenda().getCdcontagerencial());
					comboBean.setDescricaoInputPersonalizadoContagerencialvenda(mat.getMaterialpromocao().getContagerencialvenda().getDescricaoInputPersonalizado());
				}
				comboBean.setProduto(mat.getMaterialpromocao().getProduto());
				comboBean.setServico(mat.getMaterialpromocao().getServico());
				comboBean.setTributacaoestadual(mat.getMaterialpromocao().getTributacaoestadual());
				comboBean.setValorVenda(mat.getValorvenda().getValue().doubleValue());
				
				if(mat.getQuantidade() != null){
					comboBean.setQuantidade(qtdePromocional * mat.getQuantidade());
				}
				
				lista.add(comboBean);
			}
		}
		
		View.getCurrent().convertObjectToJs(lista, "lista");
	}
	
	@Action("enviarbriefcase")
	public ModelAndView enviarBriefcase(WebRequestContext request, Contratomaterial contratomaterial) throws UnsupportedEncodingException {
		contratomaterial = contratomaterialService.loadForEntrada(contratomaterial);
		Integer tipo = Integer.parseInt(request.getParameter("cdservicoservidortipo"));
		
		String urlDestino = "";
		String parametros = "?" + URLEncoder.encode("ACAO", "LATIN1") + "=" + URLEncoder.encode("filtrarcontrato", "LATIN1");
		parametros += "&" + URLEncoder.encode("cdcontratomaterial", "LATIN1") + "=" + URLEncoder.encode(contratomaterial.getCdcontratomaterial().toString(), "LATIN1"); 
		parametros += "&" + URLEncoder.encode("qtde", "LATIN1") + "=" + URLEncoder.encode(contratomaterial.getQtde().toString(), "LATIN1"); 		
		
		if (tipo.equals(Servicoservidortipo.FTP))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Servicoftp" + parametros;
		else if (tipo.equals(Servicoservidortipo.WEB))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Servicoweb" + parametros;		
		else if (tipo.equals(Servicoservidortipo.CAIXAPOSTAL))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Dominioemailusuario" + parametros;		
		else if (tipo.equals(Servicoservidortipo.EMAIL))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Dominioemailusuario" + parametros;		
		else if (tipo.equals(Servicoservidortipo.SMTP))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Dominioemailusuario" + parametros;		
		else if (tipo.equals(Servicoservidortipo.SMTPAV))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Dominioemailusuario" + parametros;		
		else if (tipo.equals(Servicoservidortipo.DOMINIO))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Dominio" + parametros;		
		else if (tipo.equals(Servicoservidortipo.DHCP))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;		
		else if (tipo.equals(Servicoservidortipo.ARP))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;		
		else if (tipo.equals(Servicoservidortipo.DHCP_ARP))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;		
		else if (tipo.equals(Servicoservidortipo.AUTENTICACAO_RADIUS))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;		
		else if (tipo.equals(Servicoservidortipo.HOTSPOT))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;		
		else if (tipo.equals(Servicoservidortipo.DHCP_RADIUS))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;		
		else if (tipo.equals(Servicoservidortipo.BANCO_DADOS))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Servicobanco" + parametros;		
		else if (tipo.equals(Servicoservidortipo.FIREWALL))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;		
		else if (tipo.equals(Servicoservidortipo.NAT))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;		
		else if (tipo.equals(Servicoservidortipo.BANDA))
			urlDestino = "/w3erpbriefcase/briefcase/crud/Contratomaterialrede" + parametros;
		
		request.setAttribute("urlDestino", urlDestino);
		return new ModelAndView("/process/loginBriefcase");
	}
	
	/**
	 * M�todo chamado via ajax que cria parcelas de acordo com o prazo de pagamento selecionado.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratoService#geraParcelamento
	 * 
	 * @param request
	 * @param contrato
	 * @author Tom�s Rabelo
	 */
	public void geraParcelas(WebRequestContext request, Contrato contrato){
		List<String> listaDatas = contratoService.geraParcelamento(contrato);
		View.getCurrent().convertObjectToJs(listaDatas, "listaDocDatas");
	}
	
    /**
     * Action que via ajax busca informa��es para o c�lculo do rateio.
     * 
     * @see br.com.linkcom.sined.geral.service.MaterialService#loadWithContagerencialCentrocusto
     *
     * @param request
     * @param contratomaterial
     * @author Rodrigo Freitas
     */
    public void ajaxInfoMaterial(WebRequestContext request, Contratomaterial contratomaterial) {
    	Material material = contratomaterial.getServico();
    	if(material != null){
    		
    		material = materialService.loadWithContagerencialCentrocusto(material);
    		
    		Contagerencial contagerencialvenda = material.getContagerencialvenda();
    		Centrocusto centrocustovenda = material.getCentrocustovenda();
    		String response = "";
    		
    		Boolean servico = material.getServico();
    		Boolean produto = material.getProduto();
    		Boolean patrimonio = material.getPatrimonio();
    		Boolean tributacaoestadual = material.getTributacaoestadual();
    		
    		response += "var servicoNome = \""+material.getNome()+"\"; ";
    		if(servico != null && servico){
    			response += "var servicoBool = 'true'; ";
    		} else response += "var servicoBool = 'false'; ";
    		
    		if(patrimonio != null && patrimonio){
    			response += "var patrimonioBool = 'true'; ";
    		} else response += "var patrimonioBool = 'false'; ";
    		
    		if(produto != null && produto){
    			response += "var produtoBool = 'true'; ";
    		} else response += "var produtoBool = 'false'; ";
    		
    		if(tributacaoestadual != null && tributacaoestadual){
    			response += "var tributacaoestadualBool = 'true'; ";
    		} else response += "var tributacaoestadualBool = 'false'; ";
    		
    		if(centrocustovenda != null){
				response += "var centrocusto = '"+Util.strings.toStringIdStyled(centrocustovenda)+"'; ";
			} else {
				response += "var centrocusto = '<null>'; ";
			}
    		
    		if(contagerencialvenda != null ){
				response += "var contagerencial = '"+Util.strings.toStringIdStyled(contagerencialvenda)+"'; ";
				response += "var contagerencial_label = \""+contagerencialvenda.getDescricaoInputPersonalizado()+"\"; ";
    		} else {
				response += "var contagerencial = '<null>'; ";
				response += "var contagerencial_label = ''; ";
			}
    		
    		Double valorvenda = contratoService.getValorvendaByTabela(material.getValorvenda(), material, contratomaterial.getCliente(), contratomaterial.getFrequencia(), contratomaterial.getEmpresa());
    		if(valorvenda != null){
    			response += "var valor = " + valorvenda + ";";
    		} else {
    			response += "var valor = 0.0;";
    		}
    		
    		View.getCurrent().eval(response);
    	}
    }
    
    @Action("salvaColaborador")
    public ModelAndView salvaColaboradoresSelecionados(WebRequestContext request) throws CrudException{
    	String whereIn = request.getParameter("cdcontrato");
    	String whereInColaborador = request.getParameter("selectedItensColaborador");
    	String[] idsColaborador = whereInColaborador.split(",");
    	String[] ids = whereIn.split(",");
    	
    	List<Contratocolaborador> listaNaoIncluir = new ArrayList<Contratocolaborador>();
    	for (String contrato : ids) {
    		List<Contratocolaborador> lista = contratocolaboradorService.findColaboradorNoContrato(contrato, whereInColaborador);
    		String colaboradores = "";
    		if(lista != null && !lista.isEmpty()){
    			for (Contratocolaborador contratocolaborador : lista) 
    				colaboradores += contratocolaborador.getColaborador().getNome()+", ";
    			listaNaoIncluir.addAll(lista);
    		}
    		
    		if(colaboradores != null && !colaboradores.equals(""))
    			request.addError("J� existe(m) coloborador(es) "+colaboradores.substring(0, colaboradores.length()-2)+" cadastrado(s) para o contrato "+contrato+".");
		}
    	
    	Escala escala = null;
    	try {
			escala = escalaService.findAll().get(0);
		} catch (Exception e) {}
		
		boolean save = false;
		for (int y = 0; y < ids.length; y++) {
			for(int i = 0; i < idsColaborador.length; i++){
				boolean naoSalvar = false;
				for (Contratocolaborador contratocolaboradorAux : listaNaoIncluir) {
					if(contratocolaboradorAux.getContrato().getCdcontrato().equals(Integer.valueOf(ids[y].trim())) && contratocolaboradorAux.getColaborador().getCdpessoa().equals(Integer.valueOf(idsColaborador[i].trim()))){
						naoSalvar = true;
						break;
					}
				}
				if(naoSalvar)
					continue;
				
				Contratocolaborador cl = new Contratocolaborador();
				cl.setContrato(new Contrato(Integer.parseInt(ids[y].trim())));
				cl.setColaborador(new Colaborador(Integer.parseInt(idsColaborador[i].trim())));
				cl.setHorainicio(new Hora("08:00"));
				cl.setHorafim(new Hora("17:00"));
				cl.setDtinicio(SinedDateUtils.currentDate());
				cl.setEscala(escala);
				
				contratocolaboradorService.saveOrUpdate(cl);
				save = true;
			}
		}
		
		if(save)
			request.addMessage("Colaborador(es) inclu�do(s) no(s) contrato(s) com sucesso.");	
		
		return sendRedirectToAction("listagem");
    }
    
    public ModelAndView estornarCancelamento(WebRequestContext request){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}
    	
    	if(contratoService.validaEstornoIdentificadorContrato(itensSelecionados)){ 
    		request.addError("Contrato n�o pode ser estornado. Existe um identificador j� cadastrado.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Contrato");
			return null;
    	}
    	request.setAttribute("selectedItens", itensSelecionados);
    	
    	request.setAttribute("titulo", "ESTORNAR CANCELAMENTO");
		request.setAttribute("acaoSave", "saveEstornarCancelamento");
    	
    	return new ModelAndView("direct:/crud/popup/cancelaContrato").addObject("contratohistorico", new Contratohistorico());
    }
    
    public void saveEstornarCancelamento(WebRequestContext request, Contratohistorico contratohistorico){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}
    	
    	Contratohistorico ch;
    	String[] ids = itensSelecionados.split(",");
    	for (int i = 0; i < ids.length; i++) {
    		ch = new Contratohistorico();
    		ch.setAcao(Contratoacao.ESTORNADO);
    		ch.setContrato(new Contrato(Integer.parseInt(ids[i])));
    		ch.setDthistorico(new Timestamp(System.currentTimeMillis()));
    		ch.setObservacao(contratohistorico.getObservacao());
    		ch.setUsuario(SinedUtil.getUsuarioLogado());
    		
        	contratohistoricoService.saveOrUpdate(ch);
		}
    	
    	contratoService.updateEstornoCancelamento(itensSelecionados);
    	for (int i = 0; i < ids.length; i++) {
    		contratoService.updateAux(Integer.parseInt(ids[i]));
    	}
    	
    	request.clearMessages();
    	request.addMessage("Estorno de cancelamento feito com sucesso.");
    	SinedUtil.fechaPopUp(request);
    }
    
    /**
     * Action que exibe a tela para justificativa no cancelamento do contrato.
     *
     * @param request
     * @return
     * @author Rodrigo Freitas
     */
    public ModelAndView cancelar(WebRequestContext request){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}

		request.setAttribute("selectedItens", itensSelecionados);
		request.setAttribute("titulo", "CANCELAR CONTRATO");
		request.setAttribute("acaoSave", "saveCancelarContrato");
		
		Boolean haveRomaneio = romaneioService.haveRomaneioOrigemContrato(itensSelecionados);
		request.setAttribute("haveRomaneio", haveRomaneio);
    	
    	return new ModelAndView("direct:/crud/popup/cancelaContrato", "contratohistorico", new Contratohistorico());
    }
    
    /**
     * Action que executa o cancelamento do(s) contrato(s).
     * 
     * @see br.com.linkcom.sined.geral.service.ContratoService#updateDataCancelamento
     * @see br.com.linkcom.sined.geral.service.ContratoService#updateDtsupensaoCancelamento
     * @see br.com.linkcom.sined.geral.service.ContratoService#updateAux
     * @see br.com.linkcom.sined.geral.service.ProducaoagendaService#cancelaEmEsperaByContrato(Contrato contrato)
     *
     * @param request
     * @param contratohistorico
     * @author Rodrigo Freitas
     */
    public void saveCancelarContrato(WebRequestContext request, Contratohistorico contratohistorico){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}
    	
    	String[] ids = itensSelecionados.split(",");
    	for (int i = 0; i < ids.length; i++) {
    		Contrato contrato = new Contrato(Integer.parseInt(ids[i]));
    		
    		Contratohistorico ch = new Contratohistorico();
    		ch.setAcao(Contratoacao.CANCELADO);
			ch.setContrato(contrato);
    		ch.setDthistorico(new Timestamp(System.currentTimeMillis()));
    		ch.setObservacao(contratohistorico.getObservacao());
    		ch.setMotivoCancelamentoFaturamento(contratohistorico.getMotivoCancelamentoFaturamento());
    		ch.setUsuario(SinedUtil.getUsuarioLogado());
    		
        	contratohistoricoService.saveOrUpdate(ch);
        	
        	
        	if(contratohistorico.getCancelarRomaneio() != null && contratohistorico.getCancelarRomaneio()){
    			romaneioService.cancelaRomaneioByContrato(contrato);
    		}
        	producaoagendaService.cancelaEmEsperaByContrato(contrato);
		}
    	
    	contratoService.updateDataCancelamento(itensSelecionados);
    	contratoService.updateDtsupensaoCancelamento(itensSelecionados);
    	for (int i = 0; i < ids.length; i++) {
    		contratoService.updateAux(Integer.parseInt(ids[i]));
    	}
    	
    	vendaorcamentoService.updateVendaorcamentoByCancelamentoOrigemContrato(itensSelecionados);
    	
    	request.clearMessages();
    	request.addMessage("Contrato(s) cancelado(s) com sucesso.");
    	SinedUtil.fechaPopUp(request);
    }
    
    /**
     * M�todo que abre popup para inser��o de pr�xima data de vencimento
     * 
     * @param request
     * @return
     * @author Tom�s Rabelo
     */
    @Action("prepareCofirmacaoContrato")
    public ModelAndView prepareCofirmacaoContrato(WebRequestContext request) {
		String whereIn = request.getParameter("selectedItens");
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		Boolean confirmarAutomaticoAposPrimeiroRomaneio = Boolean.valueOf(request.getParameter("confirmarAutomaticoAposPrimeiroRomaneio") != null ? request.getParameter("confirmarAutomaticoAposPrimeiroRomaneio") : "false");
		boolean confirmarAutomaticamente = false;
		
		if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.A_CONSOLIDAR, SituacaoContrato.ATENCAO, SituacaoContrato.ATRASADO,  SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.CANCELADO)){
			request.addError("Para confirmar o(s) contrato(s) deve(m) estar com situa��o 'EM ESPERA' ou 'SUSPENSO'.");
			SinedUtil.fechaPopUp(request);
			return null;
		} else if (contratoService.verificarConfirmarContratoLocacao(whereIn)){
			request.addError("Para confirmar � necess�rio gerar romaneio.");
			SinedUtil.fechaPopUp(request);
			return null;
		} else {
			List<Contrato> listaContrato = contratoService.findByContratoWithCliente(whereIn);
			if(listaContrato != null && !listaContrato.isEmpty()){
				confirmarAutomaticamente = true;
				for(Contrato contratoBean : listaContrato){
					if(contratoBean.getCliente() != null && restricaoService.verificaRestricaoSemDtLiberacao(contratoBean.getCliente())){
						request.addError("Aten��o! O cliente " + contratoBean.getCliente().getNome() + " tem restri��o sem libera��o.");
						SinedUtil.fechaPopUp(request);
						return null;
					}
					
					//S� posso confirmar automaticamente quando todos os contratos tiverem seu Tipo com o check "Atualizar prox. vencimento ao confirmar contrato" DESMARCADO
					confirmarAutomaticamente = confirmarAutomaticamente && contratoBean.getContratotipo()!=null && !Boolean.TRUE.equals(contratoBean.getContratotipo().getAtualizarProximoVencimentoConfirmar());
				}				
			}
		}
		
		request.setAttribute("confirmarAutomaticoAposPrimeiroRomaneio", confirmarAutomaticoAposPrimeiroRomaneio);
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("entrada", entrada);
		request.setAttribute("confirmarAutomaticamente", confirmarAutomaticamente);
		Contrato contrato = new Contrato();
		contrato.setDtproximovencimento(new Date(System.currentTimeMillis()));
		
		if (confirmarAutomaticamente){
			return continueOnAction("cofirmarContrato", contrato);
		}else {
			return new ModelAndView("direct:/crud/popup/confirmaContrato").addObject("contrato", contrato);
		}		
	}
    
    /**
     * M�todo que confirma os contratos
     * 
     * @param request
     * @param contrato
     * @return
     * @author Tom�s Rabelo
     */
    @Action("cofirmarContrato")
	public ModelAndView cofirmarContrato(WebRequestContext request, Contrato contrato) {
    	request.getServletResponse().setContentType("text/html");
    	
    	String whereIn = SinedUtil.getItensSelecionados(request);
    	Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
    	
    	if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.A_CONSOLIDAR, SituacaoContrato.ATENCAO, SituacaoContrato.ATRASADO,  SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.CANCELADO)){
			request.addError("Para confirmar o(s) contrato(s) deve(m) estar com situa��o 'EM ESPERA' ou 'SUSPENSO'.");
		}else if (contratoService.verificarConfirmarContratoLocacao(whereIn)){
			request.addError("Para confirmar � necess�rio gerar romaneio.");
		}else {
	    	contratoService.confirmaContratos(whereIn, contrato.getDtproximovencimento(), contrato.getMes(), contrato.getAno());
	    	request.clearMessages();
	    	request.addMessage("Contrato(s) confirmado(s) com sucesso.");
		}
    	
    	if(request.getBindException().hasErrors()){
			request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
		}
    	
    	if(entrada)
    		View.getCurrent().println("<script>if(parent.location.href.indexOf('cdcontrato') == -1){parent.location = parent.location+'?ACAO=consultar&cdcontrato="+whereIn+"';} else{parent.location = parent.location;} parent.$.akModalRemove(true);</script>");
    	else
    		View.getCurrent().println("<script>parent.location = parent.location;parent.$.akModalRemove(true);</script>");
    	return null;
    }
    
    /**
     * M�todo que retorna endere�os do cliente
     * 
     * @param request
     * @param bean
     * @return
     * @author Tom�s Rabelo
     */
    public ModelAndView carregaEnderecosCliente(WebRequestContext request, Cliente bean){
    	boolean restricaoSemDtLiberacao = restricaoService.verificaRestricaoSemDtLiberacao(bean);
    	boolean permissaoIncluirContratoClienteRestricao = SinedUtil.isUserHasAction("INCLUIRCONTRATOCLIENTERESTRICAO");
    	return new JsonModelAndView().addObject("lista", enderecoService.findAtivosByCliente(bean))
        	.addObject("restricaoSemDataLiberacao", restricaoSemDtLiberacao)
    		.addObject("permissaoIncluirContratoClienteRestricao", permissaoIncluirContratoClienteRestricao);
	}
    
    public ModelAndView criarContratoOportunidade(WebRequestContext request, Oportunidade oportunidade) throws Exception{
		oportunidade = oportunidadeService.loadForContrato(oportunidade);
		
		Oportunidadesituacao situacaoantesfinal = oportunidadesituacaoService.findSituacaoantesfinal();
		Oportunidadesituacao situacaofinal = oportunidadesituacaoService.findSituacaofinal();
		
		if(situacaoantesfinal == null && situacaofinal == null){
			request.addError("N�o existe nenhuma situa��o da oportunidade cadastrada como antes da final e final.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		} else if(!oportunidade.getOportunidadesituacao().equals(situacaoantesfinal) && !oportunidade.getOportunidadesituacao().equals(situacaofinal)){
			request.addError("Para gerar contrato a oportunidade tem que estar na situa��o cadastrada como antes da final e final.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}
		
		Contrato form = criar(request, new Contrato());
		form.setOportunidade(oportunidade);
		
		Contacrm contacrm = oportunidade.getContacrm();
		
		Cliente cliente = null;
		if(oportunidade.getCliente() != null){
			cliente = oportunidade.getCliente();
		}else if(contacrm != null){
			if(contacrm.getTipo().equals(Tipopessoa.PESSOA_JURIDICA) && contacrm.getCnpj() != null){
				cliente = clienteService.findByCnpj(contacrm.getCnpj());
			} else if(contacrm.getTipo().equals(Tipopessoa.PESSOA_FISICA) && contacrm.getCpf() != null){
				cliente = clienteService.findByCpf(contacrm.getCpf());
			}
		}
		
		if (oportunidade != null && oportunidade.getTiporesponsavel() != null && oportunidade.getResponsavel() != null) {
			if (oportunidade.getTiporesponsavel().equals(Tiporesponsavel.COLABORADOR)) {
				form.setVendedor(oportunidade.getResponsavel());
				form.setVendedortipo(Vendedortipo.COLABORADOR);
			} else if (oportunidade.getTiporesponsavel().equals(Tiporesponsavel.AGENCIA)){
				form.setVendedor(oportunidade.getResponsavel());
				form.setVendedortipo(Vendedortipo.FORNECEDOR);
			}
		}
		
		form.setCliente(cliente);
		form.setDescricao(oportunidade.getNome());
		form.setEmpresa(oportunidade.getEmpresa());
		
		List<Contratomaterial> listaContratomaterial = new ListSet<Contratomaterial>(Contratomaterial.class);
		for(Oportunidadematerial oportunidadematerial : oportunidade.getListaOportunidadematerial()){
			Contratomaterial contratomaterial = new Contratomaterial();
			contratomaterial.setServico(oportunidadematerial.getMaterial());
			contratomaterial.setValorunitario(oportunidadematerial.getValorunitario());
			if(oportunidadematerial.getQuantidade() != null){
				contratomaterial.setQtde(oportunidadematerial.getQuantidade());
			}
			listaContratomaterial.add(contratomaterial);
		}
		form.setListaContratomaterial(listaContratomaterial);
		return getCriarModelAndView(request, form);	
	}
    
    /**
     * Verifica se o contrato possui taxa do tipo Des�gio ou Desconto.
     * 
     * @param request
     * @author Taidson
     * @since 03/08/2010
     */
    public void ajaxDesagioDesconto(WebRequestContext request, Contrato contrato){
		String whereIn = request.getParameter("selectedItens");
		Boolean existeDesagioDesconto = contratoService.haveDesagioDesconto(whereIn);
		View.getCurrent().println("var existeDesagioDesconto = '" + Util.strings.toStringIdStyled(existeDesagioDesconto, false)+"';");
	}
    
    /**
     * Realiza a a��o de suspens�o de contratos
     * @param request
     * @return
     * @author Taidson
     * @since 27/12/2010
     */
    public ModelAndView saveSuspenderContratos(WebRequestContext request, Contratohistorico contratohistorico){
    	String whereIn = SinedUtil.getItensSelecionados(request);
    	if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.FINALIZADO, SituacaoContrato.CANCELADO)){
			request.addError("Para realizar a(s) suspen��o(�es), o(s) contrato(s) n�o pode(em) estar com as situa��es 'FINALIZADO' ou 'CANCELADO'.");
			SinedUtil.fechaPopUp(request);
	    	return null;
		}
    	
    	contratoService.suspendeContratos(whereIn, contratohistorico.getMotivoCancelamentoFaturamento(), contratohistorico.getObservacao());
    	request.clearMessages();
    	request.addMessage("Suspen��o(�es) do(s) contrato(s) realizada(s) com sucesso.");
    	String msg = contratoService.emailContratoSuspenso(whereIn);
    	if(msg == null || msg.equals(""))
    		request.addMessage("Email enviado com sucesso.");
    	
    	SinedUtil.fechaPopUp(request);
    	return null;
    }
    
    public ModelAndView suspenderContratos(WebRequestContext request){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}
    	
    	if(contratoService.haveContratoSituacao(itensSelecionados, SituacaoContrato.FINALIZADO, SituacaoContrato.CANCELADO)){
			request.addError("Para realizar a(s) suspen��o(�es), o(s) contrato(s) n�o pode(em) estar com as situa��es 'FINALIZADO' ou 'CANCELADO'.");
			SinedUtil.fechaPopUp(request);
	    	return null;
		}

		request.setAttribute("selectedItens", itensSelecionados);
		request.setAttribute("titulo", "SUSPENDER CONTRATO");
		request.setAttribute("acaoSave", "saveSuspenderContratos");
		
    	return new ModelAndView("direct:/crud/popup/suspenderContrato", "contratohistorico", new Contratohistorico());
    }
    
    public void ajaxCodigosSecao(WebRequestContext request){
    	try{
	    	String whereIn = SinedUtil.getItensSelecionados(request);
	    	request.getSession().setAttribute("CODIGOS_CONTRATO_FATURAMENTO", whereIn);
	    	View.getCurrent().println("var sucesso = true;");
    	} catch (Exception e) {
    		View.getCurrent().println("var sucesso = false;");
		}
    }
    
    /**
    * M�todo para listar as contas a receber vinculadas ao contrato
    *
    * @param request
    * @param contrato
    * @return
    * @since Jul 27, 2011
    * @author Luiz Fernando F Silva
    */
    @Action ("listarcontarecebervinculadacontrato")
    public ModelAndView listarContaReceberVinculadaContrato(WebRequestContext request, Contrato contrato){    	
    	
    	if(contrato == null){
    		throw new SinedException("Nenhum item selecionado.");    		
    	} 
    	boolean existe = documentoService.findForContasContrato(contrato);
    	
    	if(existe){    		
    		return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=listagem&idContrato=" + contrato.getCdcontrato());
    	}
    	
    	request.addError("N�o existe contas a receber vinculadas ao contrato selecionado");
    	return new ModelAndView("redirect:/faturamento/crud/Contrato");
    	
    }
    
    /**
    * M�todo que verifica contratos selecionados para atualizar e abre popup para preenchimento dos campos a serem atualizados   
    *
    * @param request
    * @return
    * @since Sep 1, 2011
    * @author Luiz Fernando F Silva
    */
    public ModelAndView abrirAtualizarDados(WebRequestContext request){
    	String whereIn = SinedUtil.getItensSelecionados(request);    	
    	Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
    	Contrato contrato = new Contrato();
    	
    	boolean haveProdutoServico = contratoService.haveContratoProdutoServico(whereIn);
    	
    	if(whereIn != null){  
    		request.setAttribute("titulo", "ATUALIZAR DADOS");
			request.setAttribute("acaoSalvar", "atualizarDados");
			request.setAttribute("haveProdutoServico", haveProdutoServico);
			contrato.setWhereIn(whereIn);
			contrato.setEntrada(entrada);
    	}else{
    		request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
    	}
    	
    	return new ModelAndView("direct:/crud/popup/atualizaDadosContrato", "bean", contrato);
    }
    
    /**
     * Action para abrir a popup para o rec�lculo de comiss�o.
     *
     * @param request
     * @return
     * @since 18/06/2012
     * @author Rodrigo Freitas
     */
    public ModelAndView abrirRecalcularComissao(WebRequestContext request){
    	String whereIn = SinedUtil.getItensSelecionados(request);   
    	
    	if(whereIn == null || whereIn.equals("")){  
    		request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
    	}
    	
    	Contrato contrato = new Contrato();
    	contrato.setWhereIn(whereIn);

    	return new ModelAndView("direct:/crud/popup/recalcularComissao", "bean", contrato);
    }
    
    /**
     * Faz refer�ncia ao DAO.
     *
     * @see void br.com.linkcom.sined.geral.service.ContratoService#recalcularComissao(String whereIn, Date dtrecalculo1, Date dtrecalculo2)
     *
     * @param request
     * @param contrato
     * @since 18/06/2012
     * @author Rodrigo Freitas
     */
    public void recalcularComissao(WebRequestContext request, Contrato contrato){
    	try{
	    	contratoService.recalcularComissao(contrato.getWhereIn(), contrato.getDtrecalculo1(), contrato.getDtrecalculo2());
	    	request.addMessage("Comiss�es recalculadas com sucesso.");
    	} catch (Exception e) {
			e.printStackTrace();
			request.addError("Ocorreu um erro no rec�lculo das comiss�es.");
			request.addError(e.getMessage());
		}
    	SinedUtil.redirecionamento(request, "/faturamento/crud/Contrato");
    }
    
    /**
    * M�todo para atualizar os dados do contrato
    *
    * @param request
    * @param contrato
    * @return
    * @since Sep 1, 2011
    * @author Luiz Fernando F Silva
    */
    public ModelAndView atualizarDados(WebRequestContext request, Contrato contrato){
    	
    	String historicoParcelas = "";
    	StringBuilder errosFrequencia = new StringBuilder();
    	boolean erro = false;
    	List<String> idsContratos = new ArrayList<String>();
    	StringBuilder errosParcela = new StringBuilder(); 	
    	
    	if(contrato.getWhereIn() != null && (contrato.getDtVencimentoAtualiza() != null || contrato.getValorAtualiza() != null || 
    			contrato.getResponsavelAtualiza() != null || contrato.getFatorajuste() != null || contrato.getPercentualValorAtualiza() != null
    			|| contrato.getIndicecorrecao() != null || contrato.getDtrenovacao() != null)){
    		List<Contrato> listaContrato = contratoService.findContratoAtualizarDados(contrato.getWhereIn());
    		List<Contrato> listaNova = new ArrayList<Contrato>();
    		
    		if(listaContrato != null && !listaContrato.isEmpty()){
    			for(Contrato c : listaContrato){
    				if(c.getFrequencia() == null && c.getPrazopagamento() == null){
    					if(!errosFrequencia.toString().equals(""))
    						errosFrequencia.append(", " + c.getCdcontrato());
    					else
    						errosFrequencia.append(c.getCdcontrato());
    					erro = true;
    				}
    				if(contrato.getPercentualValorAtualiza() != null && contrato.getPercentualValorAtualiza()<0D){
    		    		request.addError("O percentual de ajuste deve ser maior que 0.");
    		    		erro = true;
    		    	}
    				if(!erro){
    					listaNova.add(c);
    				}
    				erro = false;
    			}
    			if(listaNova != null && !listaNova.isEmpty()){  				
    				if(contrato.getFatorajuste() != null && contrato.getFatorajuste().getCdfatorajuste() != null){
						Fatorajuste fatorajuste = fatorajusteService.carregaFatorajuste(contrato.getFatorajuste());
						if(fatorajuste != null && fatorajuste.getListaFatorajustevalor() != null && !fatorajuste.getListaFatorajustevalor().isEmpty()){
							String dataAtual = new SimpleDateFormat("MM/yyyy").format(System.currentTimeMillis());
							for(Fatorajustevalor fatorajustevalor : fatorajuste.getListaFatorajustevalor()){
								if(fatorajustevalor.getMesanoAux() != null && fatorajustevalor.getMesanoAux().equals(dataAtual)){
									contrato.setValorAtualiza(fatorajustevalor.getValor());
								}
							}
						}
					}
    				
    				String whereInRateio = SinedUtil.listAndConcatenate(listaNova, "rateio.cdrateio", ",");
    				List<Rateio> listaRateio = rateioService.findForAtualizadadosContrato(whereInRateio);
    				Rateio rateio;
    				List<Rateioitem> listaRateioitem;
    				
    				for(Contrato c : listaNova){
    					Money valorAnterior = new Money(c.getValor().getValue());
    					boolean atualizouNovoValor = false;
    					List<Contratoparcela> listaContratoparcela = contratoparcelaService.findByContrato(c);
    					
    					if(!SinedUtil.isListEmpty(listaContratoparcela)){
    						Money valorTotalAjustado =  new Money();
    						Money diferencaTotal = new Money();
    						//soma o do total das parcelas que ainda faltam
							Money valorParcelasRestantes = new Money();
							for (Contratoparcela parcela : listaContratoparcela) {
								if(!Boolean.TRUE.equals(parcela.getCobrada())){
									valorParcelasRestantes = valorParcelasRestantes.add(parcela.getValor());
								}
							}
    						
    						if(contrato.getValorAtualiza() != null){
    							Money valorCobrado = new Money();
    							//soma o total das parcelas que j� foram cobradas
    							for (Contratoparcela parcela : listaContratoparcela) {
    								if(Boolean.TRUE.equals(parcela.getCobrada())){
    									valorCobrado = valorCobrado.add(parcela.getValor());
    								}
								}
    							if(valorCobrado.getValue().doubleValue() > contrato.getValorAtualiza().getValue().doubleValue()){
									request.addError("O valor ajustado n�o pode ser menor que o valor j� cobrado.");
									continue;
								}
    							
    							if(valorCobrado.getValue().doubleValue() == contrato.getValorAtualiza().getValue().doubleValue()){
									request.addError("O valor do contrato a ser ajustado n�o pode ser igual ao valor das parcelas j� cobradas.");
									continue;
								}
    							
    							//valor maior que o valor atual do contrato
    							if(contrato.getValorAtualiza().getValue().doubleValue() > c.getValor().getValue().doubleValue()){
    								diferencaTotal = contrato.getValorAtualiza().subtract(c.getValor());    								
    								Money porcentagemAumentoParcela = new Money();
    								for (Contratoparcela parcela : listaContratoparcela) {
        								if(!Boolean.TRUE.equals(parcela.getCobrada())){
        									if(SinedUtil.moneyIsMaiorQueZero(valorParcelasRestantes)){
        										porcentagemAumentoParcela = parcela.getValor().multiply(new Money(100)).divide(valorParcelasRestantes);        										
        									}else{
        										porcentagemAumentoParcela = parcela.getValor().multiply(new Money(100));        										
        									}
        									parcela.setValor(parcela.getValor().add(new Money(diferencaTotal.multiply(new Money(porcentagemAumentoParcela.getValue().doubleValue() / 100)))));
        									contratoparcelaService.updateValorParcela(parcela);
        								}
        							}
    								Money totalParcelas = new Money();
    								for (Contratoparcela parcela : listaContratoparcela) {
    									totalParcelas = totalParcelas.add(parcela.getValor().getValorArredondadoDuasCasasDecimais(parcela.getValor()));
    								}
    								//caso em que a soma das parcelas difere do total do contrato
    								if(totalParcelas.getValue().doubleValue() > contrato.getValorAtualiza().getValue().doubleValue()){
    									Money diferenca = totalParcelas.subtract(contrato.getValorAtualiza());
    									listaContratoparcela.get(listaContratoparcela.size()-1).setValor(listaContratoparcela.get(listaContratoparcela.size()-1).getValor().subtract(diferenca));
    									contratoparcelaService.updateValorParcela(listaContratoparcela.get(listaContratoparcela.size()-1));
    								}else if(totalParcelas.getValue().doubleValue() < contrato.getValorAtualiza().getValue().doubleValue()){
    									Money diferenca = contrato.getValorAtualiza().subtract(totalParcelas);
    									listaContratoparcela.get(listaContratoparcela.size()-1).setValor(listaContratoparcela.get(listaContratoparcela.size()-1).getValor().add(diferenca));
    									contratoparcelaService.updateValorParcela(listaContratoparcela.get(listaContratoparcela.size()-1));
    								}
    								
    							//valor menor que o valor atual do contrato	
    							}else if(contrato.getValorAtualiza().getValue().doubleValue() < c.getValor().getValue().doubleValue()){
    								diferencaTotal = c.getValor().subtract(contrato.getValorAtualiza());
    								Boolean parcelaZerada = false;
    								for (Contratoparcela parcela : listaContratoparcela) {
    									
        								if(!Boolean.TRUE.equals(parcela.getCobrada())){
        									Money porcentagemAumentoParcela = parcela.getValor().multiply(new Money(100)).divide(valorParcelasRestantes);
        									parcela.setValor(parcela.getValor().subtract(new Money(diferencaTotal.multiply(new Money(porcentagemAumentoParcela.getValue().doubleValue() / 100)))));
        								}
        							} 
    								for (Contratoparcela parcela : listaContratoparcela) {
    									String parcelaString = parcela.getValor().toString();
    									parcelaString = parcelaString.replace(".", "");
    									parcelaString = parcelaString.replaceAll(",", ".");
    									Money valorParcela = new Money(parcelaString);
    									if(valorParcela.getValue().doubleValue() <= 0){
    										parcelaZerada = true;
    										break;
    									}
    								}
    								if(parcelaZerada){
    									request.addError("N�o � permitida a atualiza��o de valores que gerem parcelas com valor igual R$0,00.");
										continue;
    								}else {
    									for (Contratoparcela parcela : listaContratoparcela) {
    										contratoparcelaService.updateValorParcela(parcela);
    									}
    									
        								Money totalParcelas = new Money();
        								for (Contratoparcela parcela : listaContratoparcela) {
        									totalParcelas = totalParcelas.add(parcela.getValor().getValorArredondadoDuasCasasDecimais(parcela.getValor()));
        								}
        								//caso em que a soma das parcelas difere do total do contrato
        								if(totalParcelas.getValue().doubleValue() > contrato.getValorAtualiza().getValue().doubleValue()){
        									Money diferenca = totalParcelas.subtract(contrato.getValorAtualiza());
        									listaContratoparcela.get(listaContratoparcela.size()-1).setValor(listaContratoparcela.get(listaContratoparcela.size()-1).getValor().subtract(diferenca));
        									if(listaContratoparcela.get(listaContratoparcela.size()-1).getValor().getValorArredondadoDuasCasasDecimais(listaContratoparcela.get(listaContratoparcela.size()-1).getValor()).getValue().doubleValue() > 0){
        										contratoparcelaService.updateValorParcela(listaContratoparcela.get(listaContratoparcela.size()-1));        										
        									}else {
        										request.addError("N�o � permitida a atualiza��o de valores que gerem parcelas com valor igual R$0,00.");
        										continue;
        									}
        								}else if(totalParcelas.getValue().doubleValue() < contrato.getValorAtualiza().getValue().doubleValue()){
        									Money diferenca = contrato.getValorAtualiza().subtract(totalParcelas);
        									listaContratoparcela.get(listaContratoparcela.size()-1).setValor(listaContratoparcela.get(listaContratoparcela.size()-1).getValor().add(diferenca));
        									contratoparcelaService.updateValorParcela(listaContratoparcela.get(listaContratoparcela.size()-1));
        								}		
    								}
    							}
    						}else if(contrato.getPercentualValorAtualiza() !=null && contrato.getPercentualValorAtualiza()>0D){
    							valorTotalAjustado = c.getValor().multiply(new Money (1 + (contrato.getPercentualValorAtualiza()/100)));
    							diferencaTotal = valorTotalAjustado.subtract(c.getValor());  
    							int contador = 0;
    							for (Contratoparcela parcela : listaContratoparcela) {
    								contador++;
    								if(!Boolean.TRUE.equals(parcela.getCobrada())){
    									Money porcentagemAumentoParcela = parcela.getValor().multiply(new Money(100)).divide(valorParcelasRestantes);
    									Money parcelaAux = parcela.getValor();
    									parcela.setValor(parcela.getValor().add(new Money(diferencaTotal.multiply(new Money(porcentagemAumentoParcela.getValue().doubleValue() / 100)))));
    									if(parcelaAux != parcela.getValor()){
    										historicoParcelas = historicoParcelas + "<BR> Parcela "+ contador +" De " + "R$"+parcelaAux.toString()+" Para R$"+parcela.getValor().toString();
    									}
    									if(parcela.getValor().getValue().doubleValue() < 0){
    										request.addError("N�o � permitida a atualiza��o de valores que gerem parcelas com valor igual R$0,00.");
    									}
    									
    									contratoparcelaService.updateValorParcela(parcela);
    								}
    							}   							
    							Money totalParcelas = new Money();
    							for (Contratoparcela parcela : listaContratoparcela) {
    								totalParcelas = totalParcelas.add(parcela.getValor().getValorArredondadoDuasCasasDecimais(parcela.getValor()));
    							}
    							//caso em que a soma das parcelas difere do total do contrato
    							if(totalParcelas.getValue().doubleValue() > valorTotalAjustado.getValue().doubleValue()){
    								Money diferenca = totalParcelas.subtract(valorTotalAjustado);
    								listaContratoparcela.get(listaContratoparcela.size()-1).setValor(listaContratoparcela.get(listaContratoparcela.size()-1).getValor().subtract(diferenca));
    								contratoparcelaService.updateValorParcela(listaContratoparcela.get(listaContratoparcela.size()-1));
    							}else if(totalParcelas.getValue().doubleValue() < valorTotalAjustado.getValue().doubleValue()){
    								Money diferenca = valorTotalAjustado.subtract(totalParcelas);
    								listaContratoparcela.get(listaContratoparcela.size()-1).setValor(listaContratoparcela.get(listaContratoparcela.size()-1).getValor().add(diferenca));
    								contratoparcelaService.updateValorParcela(listaContratoparcela.get(listaContratoparcela.size()-1));
								}
							}
						}
    					
    					if(contrato.getValorAtualiza() != null || contrato.getPercentualValorAtualiza() != null){
    						if(contrato.getValorAtualiza() != null){
    							c.setValorAtualiza(contrato.getValorAtualiza());
    						} else if(contrato.getPercentualValorAtualiza() != null && (c.getListaContratomaterial() == null || c.getListaContratomaterial().isEmpty() || (c.getIgnoravalormaterial() != null && c.getIgnoravalormaterial()))){
    							Money valor = c.getValor();
    							Money valorAtualizado = valor.multiply(new Money(contrato.getPercentualValorAtualiza() + 100d)).divide(new Money(100d));
    							
    							c.setValorAtualiza(valorAtualizado);
    							atualizouNovoValor = true;
    						}
    					}
    					if(contrato.getIndicecorrecao() != null){
    						c.setIndicecorrecao(contrato.getIndicecorrecao());
    					}
    					if(c.getIndicecorrecao() != null && contrato.getAtualizarparcelasindiceatual() != null && contrato.getAtualizarparcelasindiceatual()){
	    					if(listaContratoparcela != null && listaContratoparcela.size() > 0){
	    						for(Contratoparcela contratoparcela : listaContratoparcela){
	    							if(contratoparcela.getCdcontratoparcela() != null){
		    							if(contratoparcela.getCobrada() == null || !contratoparcela.getCobrada()){
		    								contratoparcela.setValor(indicecorrecaoService.calculaIndiceCorrecao(c.getIndicecorrecao(), contratoparcela.getValor(), contratoparcela.getDtvencimento(), c.getDtinicio()));
		    								contratoparcelaService.updateValorParcela(contratoparcela);
		    							}
	    							}
	    						}
	    					} 
    					}
    					
    					contratoService.atualizaDados(c, contrato.getDtVencimentoAtualiza(), c.getValorAtualiza(), contrato.getResponsavelAtualiza(), contrato.getQtde(), contrato.getFatorajuste(), contrato.getIndicecorrecao(), contrato.getDtrenovacao());
    					idsContratos.add(c.getIdentificadorOrCdcontrato() + "");
    					
    					if(contrato.getPercentualValorAtualiza() != null && c.getListaContratomaterial() != null && !c.getListaContratomaterial().isEmpty() && (c.getIgnoravalormaterial() == null || !c.getIgnoravalormaterial())){
    						for (Contratomaterial contratomaterial : c.getListaContratomaterial()) {
    							if(contratomaterial.getDtfim() == null || SinedDateUtils.afterOrEqualsIgnoreHour(contratomaterial.getDtfim(), c.getDtproximovencimento())){
    								Double valorfechado = contratomaterial.getValorfechado();
    								if(valorfechado != null){
    									Double valorfechadoatualizado = (valorfechado * (contrato.getPercentualValorAtualiza() + 100d)) / 100d;
    									valorfechadoatualizado = SinedUtil.round(valorfechadoatualizado, 2);
    	    							contratomaterial.setValorfechado(valorfechadoatualizado);
    	    							contratomaterialService.updateValorfechado(contratomaterial, valorfechadoatualizado);
    								}
    								
	    							Double valorunitario = contratomaterial.getValorunitario();
	    							Double valorunitarioatualizado = (valorunitario * (contrato.getPercentualValorAtualiza() + 100d)) / 100d;
	    							valorunitarioatualizado = SinedUtil.round(valorunitarioatualizado, 2);
	    							contratomaterial.setValorunitario(valorunitarioatualizado);
	    							contratomaterialService.updateValorunitario(contratomaterial, valorunitarioatualizado);    							
    							}
    						}
    						contratoService.atualizaValorRateioFaturamento(c);
    						atualizouNovoValor = true;
    					}
    					
    					if(c.getValorAtualiza() != null){
    						if(c.getRateio() != null && c.getRateio().getCdrateio() != null && listaRateio.contains(c.getRateio())){
    							rateio = listaRateio.get(listaRateio.indexOf(c.getRateio()));
    							listaRateioitem = rateio.getListaRateioitem();
    							
    							for (Rateioitem rateioitem : listaRateioitem) {
    								if(rateioitem.getPercentual() != null){
	    								Money valorItem = c.getValorAtualiza().multiply(new Money(rateioitem.getPercentual())).divide(new Money(100d));
	    								rateioitem.setValor(valorItem);
	    								
										rateioitemService.updateValorRateioItem(rateioitem);
    								}
								}
    						}
    					}
    					
    					Contratohistorico contratohistorico = new Contratohistorico(c, new Timestamp(System.currentTimeMillis()), Contratoacao.DADOS_ATUALIZADOS, SinedUtil.getUsuarioLogado());
    					
    					if (contrato.getPercentualValorAtualiza() != null && atualizouNovoValor){
    						String observacaoContratohistorico = "Valor Anterior: R$" + valorAnterior.toString() + " | " +
    								"Percentual de ajuste: " + new DecimalFormat("###,##0.##").format(contrato.getPercentualValorAtualiza()) + "% | " + 
    								"Valor Atualizado: R$" + contratoService.load(c).getValor();
    						
    						if(historicoParcelas.length()>0){
    							observacaoContratohistorico = observacaoContratohistorico + historicoParcelas;
    						}
    						observacaoContratohistorico += "<br>";
    						contratohistorico.setObservacao(observacaoContratohistorico);
    						
    						contratoService.updateReajuste(c, Boolean.TRUE, contrato.getPercentualValorAtualiza());
    					}
    					
    					contratohistorico.setValor(valorAnterior);
    					contratohistorico.setDtproximovencimento(c.getDtproximovencimento());
    					contratohistorico.setFormafaturamento(c.getFormafaturamento());
    					contratohistorico.setHistorico(c.getHistorico());
    					contratohistorico.setAnotacao(c.getAnotacao());
    					contratohistorico.setVisualizarContratoHistorico(Boolean.TRUE);
    					contratohistorico.setResponsavel(c.getResponsavel() != null ? c.getResponsavel().getNome() : "");
    					contratohistorico.setDtrenovacao(c.getDtrenovacao());
    					contratohistorico.setQuantidade(c.getQtde());
    					contratohistorico.setFatorAjuste(c.getFatorajuste());
    					contratohistorico.setIndiceCorrecao(c.getIndicecorrecao());
    					
    					StringBuilder rateioStr = new StringBuilder();
    					for (Rateioitem ri : c.getRateio().getListaRateioitem()) {
    						rateioStr.append("Conta gerencial: " + contagerencialService.loadForEntrada(ri.getContagerencial()).getDescricao() + "\n");
    						rateioStr.append("Centro de custo: " + centrocustoService.loadForEntrada(ri.getCentrocusto()).getNome() + "\n");
    						
    						Projeto projeto = projetoService.loadForEntrada(ri.getProjeto());
    						if (projeto != null) {
    							rateioStr.append("Projeto: " + projeto.getNome() + "\n");
    						}
    						
    						rateioStr.append("Percentual: " + ri.getPercentual() + "%\n");
    						rateioStr.append("Valor: R$" + ri.getValor() + "-quebra-");
    					}				
    					contratohistorico.setRateio(rateioStr.toString());
    					
    					StringBuilder parcelas = new StringBuilder();
    					for (Contratoparcela p : c.getListaContratoparcela()) {
    						parcelas.append("Data de vencimento: " + SinedDateUtils.toString(p.getDtvencimento()) + "\n");
    						parcelas.append("Valor da NF de Servi�o: R$" + p.getValornfservico() + "\n");
    						parcelas.append("Valor da NF de Produto: R$" + p.getValornfproduto() + "\n");
    						parcelas.append("Desconto: R$" + p.getDesconto() + "\n");
    						parcelas.append("Valor: R$" + p.getValor() + "\n");
    						parcelas.append("Aplicar �ndice: " + (p.getAplicarindice() != null && p.getAplicarindice() ? "Sim" : "N�o") + "\n");
    						parcelas.append("Cobrada: " + (p.getCobrada() != null && p.getCobrada() ? "Sim" : "N�o") + "-quebra-");
    					}				
    					contratohistorico.setParcelas(parcelas.toString());
    					
    					contratohistoricoService.saveOrUpdate(contratohistorico);
    				}
    			}
    		}
    	}
    	
    	if(idsContratos.size() > 0){
    		for (String id : idsContratos) {
    			request.addMessage("Contrato " + id + " atualizado com sucesso.");
			}
    	}
    	if(errosFrequencia != null && !errosFrequencia.toString().equals("")){
    		request.addError("O(s) contrato(s) " + errosFrequencia.toString() + " n�o foi(ram) atualizado(s) por n�o ter(em) periodicidade ou prazo de pagamento.");
    	}
    	
    	if(errosParcela != null && !errosParcela.toString().equals("")){
    		request.addError("O(s) contrato(s) " + errosParcela.toString() + " n�o foi(ram) atualizado(s) por ter(em) parcelas.");
    	}
    	
    	request.getServletResponse().setContentType("text/html");
    	if(contrato.getEntrada())
    		View.getCurrent().println("<script>if(parent.location.href.indexOf('cdcontrato') == -1){parent.location = parent.location+'?ACAO=consultar&cdcontrato="+ contrato.getWhereIn()+"';} else{parent.location = parent.location;} parent.$.akModalRemove(true);</script>");
    	else
    		View.getCurrent().println("<script>parent.location = parent.location;parent.$.akModalRemove(true);</script>");
    	
    	return null;
    }
    
    /**
	* M�todo ajax para calculcar o valor do contrato
	*
	* @param request
	* @since Oct 17, 2011
	* @author Luiz Fernando F Silva
	 * @throws ParseException 
	*/
	public void ajaxCalculaValorcontrato(WebRequestContext request) throws ParseException{
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		Boolean sucesso = Boolean.FALSE;
		String cdfatorajuste = request.getParameter("cdfatorajuste");
		String qtdeString = request.getParameter("qtde");
		StringBuilder s = new StringBuilder();
		
		if(cdfatorajuste != null && !cdfatorajuste.equals("")){			
			String dtAtual = new SimpleDateFormat("MM/yyyy").format(System.currentTimeMillis());
			Double qtde = qtdeString != null ? Double.parseDouble(qtdeString.replace(".", "").replace(",", ".")) : 1.0;
			Fatorajuste fatorajuste = fatorajusteService.carregaFatorajuste(new Fatorajuste(Integer.parseInt(cdfatorajuste)));
			if(fatorajuste != null && fatorajuste.getListaFatorajustevalor() != null && !fatorajuste.getListaFatorajustevalor().isEmpty()){
				for(Fatorajustevalor fatorajustevalor : fatorajuste.getListaFatorajustevalor()){
					if(fatorajustevalor.getMesano() != null && dtAtual.equals(fatorajustevalor.getMesanoAux()) &&
							fatorajustevalor.getValor() != null){
						s.append("var valorcontrato = '").append(new Money(fatorajustevalor.getValor().getValue().doubleValue() * qtde)).append("'; ");
						s.append("var valor = '").append(new Money(fatorajustevalor.getValor().getValue().doubleValue())).append("'; ");
						sucesso = Boolean.TRUE;
						break;
					}
				}
				if(!sucesso){
					sucesso = Boolean.TRUE;
					s.append("var valorcontrato = '0';");
					s.append("var valor = '0'; ");
					s.append("var mgs = 'N�o existe fator de ajuste lan�ado para este m�s.';");
				}
			}
		}
		
		if(sucesso){
			s.append("var sucesso = true;");
			view.println(s.toString());
		}else{
			s.append("var sucesso = false;");
			view.println(s.toString());
		}
	}
	
    /**
	 * Verifica se existe um modelo de contrato associado ao Material/Servi�os,
	 * retorna o modelo se existir apenas um, caso contr�rio dever� ser exibida
	 * uma tela para que o usu�rio selecione o contrato.
	 * 
	 * @param request
	 * @author Giovane Freitas
	 * @since 21/01/2012
	 */
    public ModelAndView selecionarContrato(WebRequestContext request, Contrato contrato){
    	
    	List<Empresamodelocontrato> empresamodelocontrato = EmpresamodelocontratoService.getInstance().findByContrato(contrato);
		
		if (empresamodelocontrato != null && empresamodelocontrato.size() == 1){
			return new JsonModelAndView()
						.addObject("cdempresamodelocontrato", empresamodelocontrato.get(0).getCdempresamodelocontrato());
		}else{
			return new JsonModelAndView().addObject("cdempresamodelocontrato", "<null>");
		}
	}

	/**
	 * Abre uma tela para que o usu�rio selecione o modelo de contrato RTF a ser usado para a emiss�o.
	 * 
	 * @author Giovane Freitas
	 * @param request
	 * @return
	 */
	public ModelAndView abrirSelecaoModeloContrato(WebRequestContext request, Contrato contrato) {
		
		if(contrato == null || contrato.getCdcontrato() == null){
			request.addError("Selecione um contrato para realizar a emiss�o.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		request.setAttribute("contrato", contrato);
		request.setAttribute("modelos", EmpresamodelocontratoService.getInstance().findByContrato(contrato));
		
		return new ModelAndView("direct:/crud/popup/selecaoModeloContrato");
	}
	
	/**
	 * M�todo ajax para buscar os contatos do cliente
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaContatoByCliente(WebRequestContext request){
		String listacontato = null;
		String cdcliente = request.getParameter("cdcliente");
		if(cdcliente != null && !cdcliente.equals("")){			
			List<Contato> listaContato = contatoService.getListContatoByCliente(new Cliente(Integer.parseInt(cdcliente)), Boolean.TRUE);
			if(listaContato != null && !listaContato.isEmpty()){
				listacontato = SinedUtil.convertToJavaScript(listaContato, "listacontato", null);
			}
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(listacontato != null ? listacontato : "var listacontato = '';");
	}
	
	/**
	 * Action que realiza a isen��o dos contratos selecionados
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/12/2012
	 */
	public ModelAndView isentar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Contratohistorico ch;
    	String[] ids = whereIn.split(",");
    	for (int i = 0; i < ids.length; i++) {
    		ch = new Contratohistorico();
    		
    		ch.setAcao(Contratoacao.CONTRATO_ISENTO);
    		ch.setContrato(new Contrato(Integer.parseInt(ids[i])));
    		ch.setDthistorico(new Timestamp(System.currentTimeMillis()));
    		ch.setUsuario(SinedUtil.getUsuarioLogado());
    		
        	contratohistoricoService.saveOrUpdate(ch);
		}
    	
    	contratoService.updateIsento(whereIn);
    	for (int i = 0; i < ids.length; i++) {
    		contratoService.updateAux(Integer.parseInt(ids[i]));
    	}
    	
    	request.clearMessages();
    	request.addMessage("Contrato(s) marcado(s) como isento com sucesso.");
		
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView abrirRomaneioFechamento(WebRequestContext request, Contrato contrato){
		List<GerarRomaneioBean> lista = this.montaRomaneioContrato(contrato.getWhereIn(), true);
		
		request.setAttribute("fromContrato", Boolean.TRUE);
		request.setAttribute("fromFechamento", Boolean.TRUE);
		request.setAttribute("finalizarContrato", contrato.getFinalizarcontrato() != null && contrato.getFinalizarcontrato());
		request.setAttribute("CONTRATO_GERAR_ROMANEIO_LIMITE", "TRUE");
		
		return romaneioService.abrirTelaGerarRomaneio(request, lista, contrato.getWhereIn(), false, contrato.getIndenizacao(), null);
	}
	
	public ModelAndView abrirSubstituirLocacao(WebRequestContext request){		
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		SubstituirLocacaoBean bean = new SubstituirLocacaoBean(); 
		List<SubstituirLocacaoItemBean> listaSubstituirLocacaoItemBean = new ListSet<SubstituirLocacaoItemBean>(SubstituirLocacaoItemBean.class);
		
		String[] ids = whereIn.split(",");
		for (String id : ids) {
			Contrato contrato = contratoService.loadForRomaneio(new Contrato(Integer.parseInt(id)));
			List<RealizarFechamentoRomaneioContratoItem> listaRomaneio = this.makeListaFechamentoContrato(contrato, false);
			for (RealizarFechamentoRomaneioContratoItem it : listaRomaneio) {
				if(it.getContratomaterial() != null){
					SubstituirLocacaoItemBean item = new SubstituirLocacaoItemBean();
					boolean patrimonio = it.getPatrimonioitem() != null;
					
					item.setContratomaterial(it.getContratomaterial());
					item.setMaterial_substituido(it.getMaterial());
					item.setPatrimonioitem_substituido(it.getPatrimonioitem());
					item.setQtde_substituido(it.getQtdeutilizada());
					item.setPatrimonio(patrimonio);
					if(patrimonio){
						item.setMaterial_substituto(it.getMaterial());
					}
					item.setLocalarmazenagem_origem_substituido(it.getLocalarmazenagemorigem());
					item.setLocalarmazenagem_destino_substituido(it.getLocalarmazenagemdestino());
					item.setLocalarmazenagem_origem_substituto(it.getLocalarmazenagemdestino());
					item.setLocalarmazenagem_destino_substituto(it.getLocalarmazenagemorigem());
					item.setQtde_disponivel_substituto(0d);
					
					listaSubstituirLocacaoItemBean.add(item);
				}
			}
		}
		
		bean.setDtromaneio(SinedDateUtils.currentDate());
		bean.setListaSubstituirLocacaoItemBean(listaSubstituirLocacaoItemBean);
		
//		request.setAttribute("PERMITIR_ESTOQUE_NEGATIVO", parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO));
		
		return new ModelAndView("direct:/crud/popup/substituirLocacao").addObject("bean", bean);
	}
	
	public void saveSubstituirLocacao(WebRequestContext request, SubstituirLocacaoBean bean){
		
		List<SubstituirLocacaoItemBean> listaSubstituirLocacaoItemBean = bean.getListaSubstituirLocacaoItemBean();
		String observacaoHistorico = "";
		if (bean.getObservacao()!=null && !bean.getObservacao().trim().equals("")){
			observacaoHistorico = " (" + bean.getObservacao() + ")";
		}
		boolean romaneioGerado = false;
		
		if(listaSubstituirLocacaoItemBean != null && listaSubstituirLocacaoItemBean.size() > 0){
			List<Romaneio> listaRomaneio = new ArrayList<Romaneio>();
			List<Romaneio> listaRomaneioFechamento = new ArrayList<Romaneio>();
			
			for (SubstituirLocacaoItemBean it : listaSubstituirLocacaoItemBean) {
				Double qtdeSubstituido = it.getQtde_substituido();
				Contratomaterial contratomaterial = it.getContratomaterial();
				Contrato contrato = contratomaterial.getContrato();
				Empresa empresa = contrato != null ? contrato.getEmpresa() : null;
				Cliente cliente = contrato != null ? contrato.getCliente() : null;
				Endereco endereco = contrato != null ? contrato.getEnderecoentrega() : null;
				
				Material materialSubstituido = it.getMaterial_substituido();
				Patrimonioitem patrimonioitemSubstituido = it.getPatrimonioitem_substituido();
				Localarmazenagem localarmazenagemOrigemSubstituido = it.getLocalarmazenagem_origem_substituido();
				Localarmazenagem localarmazenagemDestinoSubstituido = it.getLocalarmazenagem_destino_substituido();
				
				Material materialSubstituto = it.getMaterial_substituto();
				Patrimonioitem patrimonioitemSubstituto = it.getPatrimonioitem_substituto();
				Localarmazenagem localarmazenagemOrigemSubstituto = it.getLocalarmazenagem_origem_substituto();
				Localarmazenagem localarmazenagemDestinoSubstituto = it.getLocalarmazenagem_destino_substituto();
				
				boolean patrimonio = it.getPatrimonio() != null && it.getPatrimonio();
				boolean hasLocalSubstituto = localarmazenagemOrigemSubstituto != null && localarmazenagemDestinoSubstituto != null;
				boolean hasMaterialSubstituto = (patrimonio && patrimonioitemSubstituto != null) || (!patrimonio && materialSubstituto != null);
				
				if(hasLocalSubstituto && hasMaterialSubstituto){
					// CRIA O ROMANEIO DO SUBSTITU�DO COMO FECHAMENTO
					boolean achouFechamento = false;
					Romaneioitem romaneioitemFechamento = new Romaneioitem();
					romaneioitemFechamento.setMaterial(materialSubstituido);
					romaneioitemFechamento.setMaterialclasse(patrimonio ? Materialclasse.PATRIMONIO : Materialclasse.PRODUTO);
					romaneioitemFechamento.setPatrimonioitem(patrimonioitemSubstituido);
					romaneioitemFechamento.setQtde(qtdeSubstituido);
					romaneioitemFechamento.setContratomaterial(contratomaterial);
					
					for (Romaneio romaneio : listaRomaneioFechamento) {
						if(romaneio.getLocalarmazenagemorigem().equals(localarmazenagemOrigemSubstituido) &&
								romaneio.getLocalarmazenagemdestino().equals(localarmazenagemDestinoSubstituido)){
							if(!romaneio.getListaContrato().contains(contrato)){
								romaneio.getListaContrato().add(contrato);
							}
							romaneio.getListaRomaneioitem().add(romaneioitemFechamento);
							achouFechamento = true;
							break;
						}
					}
					Romaneio romaneiosubstituido = null;
					Romaneioitem romaneioitemsubstituido = null;
					if(!achouFechamento){
						Romaneio romaneio = new Romaneio();
						romaneio.setDtromaneio(bean.getDtromaneio() != null ? bean.getDtromaneio() : SinedDateUtils.currentDate());
						romaneio.setLocalarmazenagemorigem(localarmazenagemOrigemSubstituido);
						romaneio.setLocalarmazenagemdestino(localarmazenagemDestinoSubstituido);
						romaneio.setEmpresa(empresa);
						romaneio.setRomaneiosituacao(Romaneiosituacao.BAIXADA);
						romaneio.setRomaneiotipo(Romaneiotipo.ENTRADA);
						romaneio.setPlaca(bean.getPlaca());
						romaneio.setMotorista(bean.getMotorista());
						romaneio.setCliente(cliente);
						romaneio.setEndereco(endereco);
						
						List<Contrato> listaContrato = new ArrayList<Contrato>();
						listaContrato.add(contrato);
						romaneio.setListaContrato(listaContrato);
						
						List<Romaneioitem> listaRomaneioitem = new ArrayList<Romaneioitem>();
						listaRomaneioitem.add(romaneioitemFechamento);
						romaneio.setListaRomaneioitem(listaRomaneioitem);
						
						listaRomaneioFechamento.add(romaneio);
						romaneiosubstituido = romaneio;
						romaneioitemsubstituido = romaneioitemFechamento;
					}
					
					// CRIA O ROMANEIO DO SUBSTITUTO
					boolean achouRomaneio = false;
					Romaneioitem romaneioitem = new Romaneioitem();
					romaneioitem.setMaterial(materialSubstituto);
					romaneioitem.setMaterialclasse(patrimonio ? Materialclasse.PATRIMONIO : Materialclasse.PRODUTO);
					romaneioitem.setPatrimonioitem(patrimonioitemSubstituto);
					romaneioitem.setQtde(qtdeSubstituido);
					romaneioitem.setContratomaterial(contratomaterial);
					romaneioitem.setRomaneioitemsubstituido(romaneioitemsubstituido);
					
					for (Romaneio romaneio : listaRomaneio) {
						if(romaneio.getLocalarmazenagemorigem().equals(localarmazenagemOrigemSubstituto) &&
								romaneio.getLocalarmazenagemdestino().equals(localarmazenagemDestinoSubstituto)){
							if(!romaneio.getListaContrato().contains(contrato)){
								romaneio.getListaContrato().add(contrato);
							}
							romaneio.getListaRomaneioitem().add(romaneioitem);
							achouRomaneio = true;
							break;
						}
					}
					if(!achouRomaneio){
						Romaneio romaneio = new Romaneio();
						romaneio.setDtromaneio(bean.getDtromaneio() != null ? bean.getDtromaneio() : SinedDateUtils.currentDate());
						romaneio.setLocalarmazenagemorigem(localarmazenagemOrigemSubstituto);
						romaneio.setLocalarmazenagemdestino(localarmazenagemDestinoSubstituto);
						romaneio.setEmpresa(empresa);
						romaneio.setRomaneiosituacao(Romaneiosituacao.BAIXADA);
						romaneio.setRomaneiotipo(Romaneiotipo.SAIDA);
						romaneio.setPlaca(bean.getPlaca());
						romaneio.setMotorista(bean.getMotorista());
						romaneio.setCliente(cliente);
						romaneio.setEndereco(endereco);
						
						List<Contrato> listaContrato = new ArrayList<Contrato>();
						listaContrato.add(contrato);
						romaneio.setListaContrato(listaContrato);
						
						List<Romaneioitem> listaRomaneioitem = new ArrayList<Romaneioitem>();
						listaRomaneioitem.add(romaneioitem);
						romaneio.setListaRomaneioitem(listaRomaneioitem);
						
						romaneio.setRomaneiosubstituido(romaneiosubstituido);
						listaRomaneio.add(romaneio);
					}
				}
			}
			
			List<Contratomateriallocacao> listaContratomateriallocacao = new ArrayList<Contratomateriallocacao>();
			for (Romaneio romaneio : listaRomaneioFechamento) {
				List<Contrato> listaContrato = romaneio.getListaContrato();
				
				listaContrato = contratoService.findWithIdentificador(CollectionsUtil.listAndConcatenate(listaContrato, "cdcontrato", ","));
				StringBuilder descricao = new StringBuilder();
				for (Contrato contrato : listaContrato) {
					if(contrato.getIdentificador() != null && !contrato.getIdentificador().trim().equals("")){
						descricao.append(contrato.getIdentificador()).append(", ");
					} else {
						descricao.append(contrato.getCdcontrato()).append(", ");
					}
				}
				descricao.delete(descricao.length() - 2, descricao.length());
				romaneio.setDescricao("Substitui��o do(s) contrato(s) " + descricao.toString());
				
				if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), 
						romaneio.getLocalarmazenagemorigem(), romaneio.getProjeto())) {
					request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
							"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
					SinedUtil.fechaPopUp(request);
					return;
				}
				
				if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), 
						romaneio.getLocalarmazenagemdestino(), romaneio.getProjeto())) {
					request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
							"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
					SinedUtil.fechaPopUp(request);
					return;
				}
				
				romaneioService.saveOrUpdate(romaneio);
				romaneioGerado = true;
				
				for (Contrato c : listaContrato) {
					Romaneioorigem romaneioorigem = new Romaneioorigem();
					romaneioorigem.setRomaneio(romaneio);
					romaneioorigem.setContratofechamento(c);
					
					romaneioorigemService.saveOrUpdate(romaneioorigem);
					
					Contratohistorico contratohistorico = new Contratohistorico();
					contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
					contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
					contratohistorico.setAcao(Contratoacao.SUBSTITUICAO_LOCACAO);
					contratohistorico.setContrato(c);
					contratohistorico.setObservacao("Romaneio do material substitu�do " + romaneioService.geraLinkRomaneio(romaneio) + " gerado." + observacaoHistorico);
					
					contratohistoricoService.saveOrUpdate(contratohistorico);
				}
				
				List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
				for (Romaneioitem item : listaRomaneioitem) {
					// GERA A ENTRADA E SA�DA DO ROMANEIO
					romaneioService.gerarEntradaSaidaRomaneio(romaneio, item, null, romaneio.getEmpresa(), null, null, romaneio.getEmpresa(), null, false, null, null, null, null);
					
					if(item.getContratomaterial() != null){
						Contratomateriallocacao contratomateriallocacao = new Contratomateriallocacao();
						contratomateriallocacao.setContratomaterial(item.getContratomaterial());
						contratomateriallocacao.setContratomateriallocacaotipo(Contratomateriallocacaotipo.ENTRADA);
						contratomateriallocacao.setDtmovimentacao(romaneio.getDtromaneio() != null ? romaneio.getDtromaneio() : new Date(System.currentTimeMillis()));
						contratomateriallocacao.setQtde(item.getQtde());
						contratomateriallocacao.setDtaltera(new Timestamp(System.currentTimeMillis()));
						contratomateriallocacao.setCdromaneio(romaneio.getCdromaneio());
						contratomateriallocacao.setSubstituicao(Boolean.TRUE);
						listaContratomateriallocacao.add(contratomateriallocacao);
					}
				}
			}
			for (Romaneio romaneio : listaRomaneio) {
				List<Contrato> listaContrato = romaneio.getListaContrato();
				
				listaContrato = contratoService.findWithIdentificador(CollectionsUtil.listAndConcatenate(listaContrato, "cdcontrato", ","));
				StringBuilder descricao = new StringBuilder();
				for (Contrato contrato : listaContrato) {
					if(contrato.getIdentificador() != null && !contrato.getIdentificador().trim().equals("")){
						descricao.append(contrato.getIdentificador()).append(", ");
					} else {
						descricao.append(contrato.getCdcontrato()).append(", ");
					}
				}
				descricao.delete(descricao.length() - 2, descricao.length());
				romaneio.setDescricao("Romaneio referente ao envio do(s) contrato(s) " + descricao.toString());
				
				if(romaneio.getRomaneiosubstituido() != null && romaneio.getRomaneiosubstituido().getCdromaneio() == null){
					romaneio.setRomaneiosubstituido(null);
				}
				
				if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), 
						romaneio.getLocalarmazenagemorigem(), romaneio.getProjeto())) {
					request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
							"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
					SinedUtil.fechaPopUp(request);
					return;
				}
				
				if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), 
						romaneio.getLocalarmazenagemdestino(), romaneio.getProjeto())) {
					request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
							"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
					SinedUtil.fechaPopUp(request);
					return;
				}
				
				romaneioService.saveOrUpdate(romaneio);
				romaneioGerado = true;
				
				for (Contrato c : listaContrato) {
					Romaneioorigem romaneioorigem = new Romaneioorigem();
					romaneioorigem.setRomaneio(romaneio);
					romaneioorigem.setContrato(c);
					
					romaneioorigemService.saveOrUpdate(romaneioorigem);
					
					Contratohistorico contratohistorico = new Contratohistorico();
					contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
					contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
					contratohistorico.setAcao(Contratoacao.SUBSTITUICAO_LOCACAO);
					contratohistorico.setContrato(c);
					contratohistorico.setObservacao("Romaneio do material substituto " + romaneioService.geraLinkRomaneio(romaneio) + " gerado." + observacaoHistorico);
					
					contratohistoricoService.saveOrUpdate(contratohistorico);
				}
				
				List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
				for (Romaneioitem item : listaRomaneioitem) {
					// GERA A ENTRADA E SA�DA DO ROMANEIO
					romaneioService.gerarEntradaSaidaRomaneio(romaneio, item, null, romaneio.getEmpresa(), romaneio.getProjeto(), null, romaneio.getEmpresa(), romaneio.getProjeto(), false, romaneio.getCliente(), null, null, null);
					
					if(item.getContratomaterial() != null){
						contratomaterialService.updateMaterial(item.getContratomaterial(), item.getMaterial());
						if(item.getPatrimonioitem() != null){
							contratomaterialService.updatePatrimonioitem(item.getContratomaterial(), item.getPatrimonioitem());
						}
						
						Contratomateriallocacao contratomateriallocacao = new Contratomateriallocacao();
						contratomateriallocacao.setContratomaterial(item.getContratomaterial());
						contratomateriallocacao.setContratomateriallocacaotipo(Contratomateriallocacaotipo.SAIDA);
						contratomateriallocacao.setDtmovimentacao(romaneio.getDtromaneio() != null ? romaneio.getDtromaneio() : new Date(System.currentTimeMillis()));
						contratomateriallocacao.setQtde(item.getQtde());
						contratomateriallocacao.setDtaltera(new Timestamp(System.currentTimeMillis()));
						contratomateriallocacao.setCdromaneio(romaneio.getCdromaneio());
						contratomateriallocacao.setSubstituicao(Boolean.TRUE);
						listaContratomateriallocacao.add(contratomateriallocacao);
					}
				}
			}
			
			if(listaContratomateriallocacao != null && !listaContratomateriallocacao.isEmpty()){
				for(Contratomateriallocacao cmlocacao : listaContratomateriallocacao){
					contratomateriallocacaoService.saveOrUpdate(cmlocacao);
				}
			}
			
		}
		
		if (!romaneioGerado){
			request.addError("Nenhum romaneio foi gerado.");
		}
		
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * M�todo que abre a popup para gera��o de romaneio.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public ModelAndView abrirRomaneio(WebRequestContext request){		
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<GerarRomaneioBean> lista = this.montaRomaneioContrato(whereIn, false, true);
		
		boolean haveRomaneio = false;
		for (GerarRomaneioBean gerarRomaneioBean : lista) {
			gerarRomaneioBean.setQtdedestinoOriginal(gerarRomaneioBean.getQtdeRestante());
			if(gerarRomaneioBean.getQtdeRestante() > 0){
				haveRomaneio = true;
			}
		}
		
		if(lista == null || lista.size() == 0 || !haveRomaneio){
			request.addError("Nenhum material foi identificado para a gera��o do romaneio.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		// Bloqueio ao gerar romameio para clientes com pend�ncias e clientes diferentes.			
		if(parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE)){
			List<Contrato> contratos = contratoService.loadWithLista(whereIn, "", true);
			Double valoresContrato = 0.0;
			// Verifica se possui clientes diferentes.
			List<Cliente> listaClientesValidacao = new ArrayList<Cliente>();
			for(Contrato contrato: contratos){
				if(Util.objects.isPersistent(contrato.getCliente()) && !listaClientesValidacao.contains(contrato.getCliente())){
					listaClientesValidacao.add(contrato.getCliente());
				}
				valoresContrato += contrato.getValorContrato().doubleValue();
			}
			
			if(listaClientesValidacao.size() > 1){
				request.addError("N�o � poss�vel gerar romaneio para mais de um cliente.");
				SinedUtil.fechaPopUp(request);
				return null;				
			}
			if(SinedUtil.isListNotEmpty(listaClientesValidacao)){
				Cliente cliente = listaClientesValidacao.get(0);
				
				// Verifica se o cliente tem limite de cr�dito
				boolean excedeLimiteCredito = clienteService.isExcedeLimiteCredito(cliente, new Money(valoresContrato));
				if(restricaoService.verificaRestricaoSemDtLiberacao(cliente) || excedeLimiteCredito){
					StringBuilder message = new StringBuilder("N�o foi poss�vel gerar o romaneio: Cliente com restri��o sem data de libera��o cadastrada ou limite de cr�dito insuficiente.");
					if(excedeLimiteCredito && parametrogeralService.getBoolean(Parametrogeral.MOSTRAR_LIMITE_CREDITO))
						message.append(" <a href=\"javascript:exibirPopUpInfoFinanceira(" + cliente.getCdpessoa() + ") \" class=\"globalerror\"><b class=\"globalerror\">Clique aqui</b></a> para visualizar o limite de cr�dito");
					request.addError(message);
					SinedUtil.fechaPopUp(request);
					return null;				
				}
			}
		}
		
		request.setAttribute("fromContrato", Boolean.TRUE);
		request.setAttribute("CONTRATO_GERAR_ROMANEIO_LIMITE", parametrogeralService.getValorPorNome(Parametrogeral.CONTRATO_GERAR_ROMANEIO_LIMITE));

		return romaneioService.abrirTelaGerarRomaneio(request, lista, whereIn, false, false, null);	
	}
	
	private List<GerarRomaneioBean> montaRomaneioContrato(String whereIn, boolean fromFechamento) {
		return montaRomaneioContrato(whereIn, fromFechamento, false);
	}
	
	private List<GerarRomaneioBean> montaRomaneioContrato(String whereIn, boolean fromFechamento, boolean acaoAbrirRomaneioContrato) {
		Map<Contrato, List<RealizarFechamentoRomaneioContratoItem>> mapContrato = new HashMap<Contrato, List<RealizarFechamentoRomaneioContratoItem>>();
		
		List<Contratomaterial> listaContratomaterial = contratomaterialService.findByContrato(whereIn);
		
		if(fromFechamento){
			List<Contrato> listaContrato = new ArrayList<Contrato>();
			List<RealizarFechamentoRomaneioContratoItem> listaFinal = new ArrayList<RealizarFechamentoRomaneioContratoItem>();
			for (Contratomaterial contratomaterial : listaContratomaterial) {
				
				List<RealizarFechamentoRomaneioContratoItem> listaRomaneio = null;
				if(mapContrato.containsKey(contratomaterial.getContrato())){
					listaRomaneio = mapContrato.get(contratomaterial.getContrato());
				} else {
					Contrato contrato = contratoService.loadForRomaneio(contratomaterial.getContrato());
					listaRomaneio = this.makeListaFechamentoContrato(contrato, fromFechamento, acaoAbrirRomaneioContrato);
					mapContrato.put(contrato, listaRomaneio);
					if(!listaContrato.contains(contrato)){
						listaContrato.add(contrato);
						listaFinal.addAll(listaRomaneio);
					}
				}
			}
		
			contratoService.ajustaListaFechamentoVariosContratos(listaFinal, listaContrato);
		}
		
		boolean exibirAlertaAtualizacaoPeriodolocacao = false;
		List<GerarRomaneioBean> lista = new ArrayList<GerarRomaneioBean>();
		for (Contratomaterial contratomaterial : listaContratomaterial) {
			
			List<RealizarFechamentoRomaneioContratoItem> listaRomaneio = null;
			if(mapContrato.containsKey(contratomaterial.getContrato())){
				listaRomaneio = mapContrato.get(contratomaterial.getContrato());
			} else {
				Contrato contrato = contratoService.loadForRomaneio(contratomaterial.getContrato());
				if(!exibirAlertaAtualizacaoPeriodolocacao && contrato.getCdcontrato() != null && contrato.getContratotipo() != null && contrato.getContratotipo().getConfirmarautomaticoprimeiroromaneio() != null &&
						contrato.getContratotipo().getConfirmarautomaticoprimeiroromaneio() && 
						!romaneioService.haveRomaneioOrigemContrato(contrato.getCdcontrato().toString())){
					exibirAlertaAtualizacaoPeriodolocacao = true;
				}
				listaRomaneio = this.makeListaFechamentoContrato(contrato, true, false);
				mapContrato.put(contrato, listaRomaneio);
			}
			
			if(contratomaterial.getListaContratomaterialitem() != null && contratomaterial.getListaContratomaterialitem().size() > 0){
				for (Contratomaterialitem contratomaterialitem : contratomaterial.getListaContratomaterialitem()) {
					Material material = contratomaterialitem.getMaterial();
					Materialclasse materialclasse = null;
					
					List<Materialclasse> listaMaterialclasse = materialService.findClassesWithoutLoad(material);
					if(listaMaterialclasse != null){
						if(listaMaterialclasse.size() == 1) {
							materialclasse = listaMaterialclasse.get(0);
						} else if(listaMaterialclasse.size() > 0){
							Collections.sort(listaMaterialclasse,new Comparator<Materialclasse>(){
								public int compare(Materialclasse o1, Materialclasse o2) {
									return o1.getCdmaterialclasse().compareTo(o2.getCdmaterialclasse());
								}
							});
							
							materialclasse = listaMaterialclasse.get(0);
						}
					}
					
					GerarRomaneioBean bean = new GerarRomaneioBean();
					bean.setContratomaterial(contratomaterial);
					bean.setMaterial(material);
					bean.setMaterialclasse(materialclasse);
					
					if(fromFechamento){
						bean.setQtde(0d);
						bean.setQtdeRestante(0d);
					} else {
						bean.setQtde(contratomaterialitem.getQtde());
						bean.setQtdeRestante(contratomaterialitem.getQtde());
					}
					
					for (RealizarFechamentoRomaneioContratoItem it : listaRomaneio) {
						if(it.getContratomaterial() != null && bean.getContratomaterial() != null && it.getMaterial() != null && bean.getMaterial() != null &&
								it.getContratomaterial().getCdcontratomaterial().equals(bean.getContratomaterial().getCdcontratomaterial()) &&
								it.getMaterial().getCdmaterial().equals(bean.getMaterial().getCdmaterial())){
							if(fromFechamento){
								bean.setQtde(it.getQtdeutilizada());
								bean.setQtdeRestante(it.getQtdeutilizada());
								bean.setLocalarmazenagem(it.getLocalarmazenagemdestino());
							} else {
								bean.setQtdeRestante(bean.getQtde() - it.getQtdeutilizada());
							}
							break;
						}
					}
					
					lista.add(bean);
				}
			} else {
				Material material = contratomaterial.getServico();
				Materialclasse materialclasse = null;
				
				List<Materialclasse> listaMaterialclasse = materialService.findClassesWithoutLoad(material);
				if(listaMaterialclasse != null){
					if(listaMaterialclasse.size() == 1) {
						materialclasse = listaMaterialclasse.get(0);
					} else if(listaMaterialclasse.size() > 0){
						if(listaMaterialclasse.contains(Materialclasse.PATRIMONIO)){
							materialclasse = Materialclasse.PATRIMONIO;
						} else {
							Collections.sort(listaMaterialclasse,new Comparator<Materialclasse>(){
								public int compare(Materialclasse o1, Materialclasse o2) {
									return o1.getCdmaterialclasse().compareTo(o2.getCdmaterialclasse());
								}
							});
							
							materialclasse = listaMaterialclasse.get(0);
						}
					}
				}
				
				GerarRomaneioBean bean = new GerarRomaneioBean();
				bean.setContratomaterial(contratomaterial);
				bean.setMaterial(material);
				bean.setMaterialclasse(materialclasse);
				if(fromFechamento){
					bean.setQtde(0d);
					bean.setQtdeRestante(0d);
				} else {
					bean.setQtde(new Double(contratomaterial.getQtde()));
					bean.setQtdeRestante(new Double(contratomaterial.getQtde()));
				}
				
				bean.setPatrimonioitem(contratomaterial.getPatrimonioitem());
				
				for (RealizarFechamentoRomaneioContratoItem it : listaRomaneio) {
					if(it.getContratomaterial() != null && bean.getContratomaterial() != null && it.getMaterial() != null && bean.getMaterial() != null &&
							it.getContratomaterial().getCdcontratomaterial().equals(bean.getContratomaterial().getCdcontratomaterial()) &&
							it.getMaterial().getCdmaterial().equals(bean.getMaterial().getCdmaterial())){
						if(fromFechamento){
							bean.setQtde(it.getQtdeutilizada());
							bean.setQtdeRestante(it.getQtdeutilizada());
							bean.setLocalarmazenagem(it.getLocalarmazenagemdestino());
						} else {
							bean.setQtdeRestante(bean.getQtde() - it.getQtdeutilizada());
						}
						break;
					}
				}
				
				if(!fromFechamento || bean.getQtde() > 0d){
					bean.setExibirAlertaAtualizacaoPeriodolocacao(exibirAlertaAtualizacaoPeriodolocacao);
					lista.add(bean);
				}
			}
		}
		
		
		Collections.sort(lista, new Comparator<GerarRomaneioBean>(){
			public int compare(GerarRomaneioBean o1, GerarRomaneioBean o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		return lista;
	}
	
	/**
	 * M�todo que salva os romaneios da tela de gerar romaneio.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public void saveRomaneio(WebRequestContext request, RomaneioGerenciamentoMaterial bean){
		boolean finalizarContratoFechamentoIndenizacao = Boolean.valueOf(request.getParameter("finalizarContratoFechamentoIndenizacao") != null ?
															request.getParameter("finalizarContratoFechamentoIndenizacao") : "false");
		Boolean atualizarPeriodolocacao = bean.getAtualizarPeriodolocacao();
		Boolean isIndenizacao = bean.getIsindenizacao();
		List<RomaneioGerenciamentoMaterialItem> listaMateriais = bean.getListaMateriais();
		List<RomaneioGerenciamentoMaterialItem> listaMateriaisNova = new ArrayList<RomaneioGerenciamentoMaterialItem>();
		for (RomaneioGerenciamentoMaterialItem item : listaMateriais) {
			if(item.getQtdedestino() > 0){
				listaMateriaisNova.add(item);
			}
		}
		
		if(listaMateriaisNova.size() == 0){
			request.addMessage("Nenhum romaneio gerado.", MessageType.WARN);
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		String validacaoLimiteParam = parametrogeralService.getValorPorNome(Parametrogeral.CONTRATO_GERAR_ROMANEIO_LIMITE);
		if(validacaoLimiteParam != null && validacaoLimiteParam.trim().toUpperCase().equals("TRUE") && (bean.getFromFechamento() == null || !bean.getFromFechamento())){
			List<GerarRomaneioBean> listaConferencia = this.montaRomaneioContrato(bean.getWhereInContrato(), false);
			for (RomaneioGerenciamentoMaterialItem item : listaMateriaisNova) {
				for (GerarRomaneioBean itConf : listaConferencia) {
					Double qtdeRestante = itConf.getQtdeRestante();
					Contratomaterial contratomaterial = itConf.getContratomaterial();
					Material material = itConf.getMaterial();
					
					boolean materialIgual = material != null && item.getMaterial() != null && material.equals(item.getMaterial());
					boolean itemcontratoIgual = contratomaterial != null && item.getContratomaterial() != null && contratomaterial.equals(item.getContratomaterial());
					
					if(materialIgual && itemcontratoIgual){
						Double qtdedestino = item.getQtdedestino();
						
						if(qtdedestino != null && qtdeRestante != null && qtdedestino > qtdeRestante){
							request.addError("Favor realizar o romaneio novamente, pois as quantidades foram atualizadas durante o processo.");
							SinedUtil.fechaPopUp(request);
							return;
						}
					}
					
				}
			}
		}
		
		List<Contrato> listaContrato = contratoService.findWithIdentificador(bean.getWhereInContrato());
		List<Contrato> listaContratoPrimeiroRomaneio = new ArrayList<Contrato>();
		StringBuilder descricao = new StringBuilder();
		for (Contrato contrato : listaContrato) {
			if(contrato.getIdentificador() != null && !contrato.getIdentificador().trim().equals("")){
				descricao.append(contrato.getIdentificador()).append(", ");
			} else {
				descricao.append(contrato.getCdcontrato()).append(", ");
			}
			if(contrato.getCdcontrato() != null && contrato.getContratotipo() != null && contrato.getContratotipo().getConfirmarautomaticoprimeiroromaneio() != null &&
					contrato.getContratotipo().getConfirmarautomaticoprimeiroromaneio() && 
					!romaneioService.haveRomaneioOrigemContrato(contrato.getCdcontrato().toString())){
				listaContratoPrimeiroRomaneio.add(contrato);
			}
		}
		descricao.delete(descricao.length() - 2, descricao.length());
		
		// CRIA��O DO ROMANEIO
		Romaneio romaneio = new Romaneio();
		romaneio.setDtromaneio(SinedDateUtils.currentDate());
		romaneio.setDescricao("Remessa do(s) contrato(s) " + descricao.toString());
		romaneio.setEmpresa(bean.getEmpresadestino());
		romaneio.setLocalarmazenagemorigem(bean.getLocalarmazenagemorigem());
		romaneio.setLocalarmazenagemdestino(bean.getLocalarmazenagemdestino());
		romaneio.setRomaneiosituacao(Romaneiosituacao.BAIXADA);
		romaneio.setRomaneiotipo(Romaneiotipo.SAIDA);
		romaneio.setPlaca(bean.getPlaca());
		romaneio.setMotorista(bean.getMotorista());
		romaneio.setDtromaneio(bean.getDtromaneio());
		romaneio.setIndenizacao(isIndenizacao);
		if(listaContrato.size() == 1){
			romaneio.setCliente(listaContrato.get(0).getCliente());
			romaneio.setEndereco(listaContrato.get(0).getEnderecoentrega());
		}
		
		if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), 
				romaneio.getLocalarmazenagemorigem(), romaneio.getProjeto())) {
			request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
					"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), 
				romaneio.getLocalarmazenagemdestino(), romaneio.getProjeto())) {
			request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
					"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		romaneioService.saveOrUpdate(romaneio);
		
		Timestamp atual = new Timestamp(System.currentTimeMillis());
		
		// CRIA��O DA ORIGEM DO ROMANEIO
		for (Contrato contrato : listaContrato) {
			Romaneioorigem romaneioorigem = new Romaneioorigem();
			romaneioorigem.setRomaneio(romaneio);
			if(bean.getFromFechamento() != null && bean.getFromFechamento()){
				romaneioorigem.setContratofechamento(contrato);
			} else {
				romaneioorigem.setContrato(contrato);
			}
			
			romaneioorigemService.saveOrUpdate(romaneioorigem);
			
			Contratohistorico contratohistorico = new Contratohistorico();
			contratohistorico.setDthistorico(atual);
			contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
			contratohistorico.setAcao(Contratoacao.ROMANEIO);
			contratohistorico.setContrato(contrato);
			
			String msgGeradoParcial = contratoService.isRomaneioParcial(listaMateriaisNova) ? " parcialmente" : "";
			if(bean.getIsindenizacao()!=null && bean.getIsindenizacao()){
				contratohistorico.setObservacao("Indeniza��o de contrato " + romaneioService.geraLinkRomaneio(romaneio) + " gerado" + msgGeradoParcial + ".");	
			}else{
				contratohistorico.setObservacao("Remessa de contrato " + romaneioService.geraLinkRomaneio(romaneio) + " gerado" + msgGeradoParcial + ".");
			}
			
			contratohistoricoService.saveOrUpdate(contratohistorico);
		}
		
		List<Contratomateriallocacao> listaContratomateriallocacao = new ArrayList<Contratomateriallocacao>();
		Contratomateriallocacao contratomateriallocacao;
		for (RomaneioGerenciamentoMaterialItem item : listaMateriaisNova) {
			// CRIA��O DO ITEM ROMANEIO
			Romaneioitem romaneioitem = new Romaneioitem();
			romaneioitem.setMaterial(item.getMaterial());
			romaneioitem.setQtde(item.getQtdedestino());
			romaneioitem.setMaterialclasse(item.getMaterialclasse());
			romaneioitem.setRomaneio(romaneio);
			romaneioitem.setPatrimonioitem(item.getPatrimonioitem());
			
			if(item.getPatrimonioitem() != null && bean.getFromFechamento() != null && bean.getFromFechamento()){
				patrimonioitemService.updateAtivo(item.getPatrimonioitem(), Boolean.FALSE);
			}
			if(item.getContratomaterial() != null){
				if(item.getPatrimonioitem() != null){
					contratomaterialService.updatePatrimonioitem(item.getContratomaterial(), item.getPatrimonioitem());
				}
				contratomateriallocacao = new Contratomateriallocacao();
				contratomateriallocacao.setContratomaterial(item.getContratomaterial());
				if(isIndenizacao != null && isIndenizacao){
					contratomateriallocacao.setContratomateriallocacaotipo(Contratomateriallocacaotipo.ENTRADA);
				}else {
					contratomateriallocacao.setContratomateriallocacaotipo(Contratomateriallocacaotipo.SAIDA);
				}
				contratomateriallocacao.setDtmovimentacao(romaneio.getDtromaneio() != null ? romaneio.getDtromaneio() : new Date(System.currentTimeMillis()));
				contratomateriallocacao.setQtde(item.getQtdedestino());
				contratomateriallocacao.setDtaltera(new Timestamp(System.currentTimeMillis()));
				contratomateriallocacao.setCdromaneio(romaneio.getCdromaneio());
				listaContratomateriallocacao.add(contratomateriallocacao);
			}
				
			
			romaneioitemService.saveOrUpdate(romaneioitem);
			
			// GERA A ENTRADA E SA�DA DO ROMANEIO
			romaneioService.gerarEntradaSaidaRomaneio(romaneio, romaneioitem, item.getValor(), bean.getEmpresadestino(), bean.getProjetodestino(), bean.getObservacao(), bean.getEmpresaorigem(), bean.getProjetoorigem(), false, bean.getClientedestino(), null, null, null);
		}
		
		if(listaContratomateriallocacao != null && !listaContratomateriallocacao.isEmpty()){
			for(Contratomateriallocacao cmlocacao : listaContratomateriallocacao){
				contratomateriallocacaoService.saveOrUpdate(cmlocacao);
			}
		}
		
		if(bean.getFromFechamento() != null && bean.getFromFechamento()){
			if(finalizarContratoFechamentoIndenizacao){
				for (Contrato contrato : listaContrato) {
					contrato = contratoService.loadForRomaneio(contrato);
					List<RealizarFechamentoRomaneioContratoItem> listaFinal = makeListaFechamentoContrato(contrato, true);
					if (listaFinal != null && !listaFinal.isEmpty() && !Boolean.TRUE.equals(bean.getIsindenizacao())){
						finalizarContratoFechamentoIndenizacao = Boolean.FALSE;
							request.addMessage("N�o � poss�vel finalizar o contrato " + contrato.getIdentificadorOrCdcontrato() + ", pois existe pend�ncia de Devolu��o/Indeniza��o", MessageType.WARN);
					} else {
						contrato.setDtfim(bean.getDtromaneio() != null ? bean.getDtromaneio() : SinedDateUtils.currentDate());
						contratoService.atualizaDataFimContrato(contrato);
						contratoService.atualizaDataConclusaoContrato(contrato);
						
						Contratohistorico contratohistorico = new Contratohistorico();
						contratohistorico.setDthistorico(atual);
						contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
						contratohistorico.setAcao(Contratoacao.FINALIZADO);
						contratohistorico.setContrato(contrato);
						contratohistoricoService.saveOrUpdate(contratohistorico);
					}
				}
			}
			if(bean.getIsindenizacao()!=null && bean.getIsindenizacao() && bean.getValorreposicao() != null){
				SinedUtil.redirecionamento(request, "/faturamento/crud/Contratofaturalocacao?ACAO=criarFaturaLocacaoFechamento&fromFechamento=true&cdcontrato=" + bean.getWhereInContrato() + "&cdromaneio=" + romaneio.getCdromaneio() + "&valorReposicao=" + bean.getValorreposicao());
			} else {
				SinedUtil.redirecionamento(request, "/faturamento/crud/Contratofaturalocacao?ACAO=criarFaturaLocacaoFechamento&fromFechamento=true&cdcontrato=" + bean.getWhereInContrato() + "&cdromaneio=" + romaneio.getCdromaneio());
			}
		} else {
			request.addMessage("Romaneio <a href='" + request.getServletRequest().getContextPath() + "/suprimento/crud/Romaneio?ACAO=consultar&cdromaneio=" + romaneio.getCdromaneio() + "'>" + romaneio.getCdromaneio() + "</a> realizado.");
			if(listaContratoPrimeiroRomaneio != null && !listaContratoPrimeiroRomaneio.isEmpty()){
				for(Contrato c : listaContratoPrimeiroRomaneio){
					if(atualizarPeriodolocacao != null && atualizarPeriodolocacao){
						contratoService.atualizaDtinicioFimLocacao(c, romaneio.getDtromaneio());
					}
				}
				String whereInForConfirmacao = CollectionsUtil.listAndConcatenate(listaContratoPrimeiroRomaneio, "cdcontrato", ",");
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println(
						"<script>parent.confirmarAutomaticoAposPrimeiroRomaneio('" + whereInForConfirmacao + "');</script>");
			}else {
				SinedUtil.redirecionamento(request, "/suprimento/crud/Romaneio?ACAO=consultar&cdromaneio=" + romaneio.getCdromaneio());
			}
		}
	}
	
	/**
	 * Action em ajax para buscar as quantidades dispon�veis no local de armazenagem de origem.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public ModelAndView verificarQtdeDisponivelMaterialRomaneioAjax(WebRequestContext request, RomaneioGerenciamentoMaterial bean){	
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(bean.getLocalarmazenagemorigem() != null){
			List<RomaneioGerenciamentoMaterialItem> listaMateriais = bean.getListaMateriais();
			for (RomaneioGerenciamentoMaterialItem it : listaMateriais) {
				Double qtdeorigem = 0d;
				if(it.getMaterialclasse() != null && it.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
					qtdeorigem = movpatrimonioService.qtdeDisponivelPatrimonio(it.getMaterial(), it.getPatrimonioitem(), bean.getLocalarmazenagemorigem(), bean.getEmpresaorigem(), bean.getProjetoorigem());
				} else {
					qtdeorigem = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(it.getMaterial(), it.getMaterialclasse(), bean.getLocalarmazenagemorigem(), bean.getEmpresaorigem(), bean.getProjetoorigem(), it.getLoteestoque());
				}
				if(qtdeorigem != null){
					it.setQtdeorigem(qtdeorigem);
				}
			}
			jsonModelAndView.addObject("listaMateriaisDisponiveis", listaMateriais);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * Abre a popup de fechamento do contrato, calculando o fechamento do contrato.
	 *
	 * @param request
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public ModelAndView abrirFechamento(WebRequestContext request, Contrato contrato){		
		
		if(contrato == null || contrato.getWhereIn() == null || contrato.getWhereIn().isEmpty()){
			throw new SinedException("Nenhum contrato selecionado.");
		}
		
		List<Contrato> listaContrato = contratoService.loadListForRomaneio(contrato.getWhereIn());
		
		//Valida��o para Fechar/Devolver contrato de um cliente por vez.
		Integer cdcliente = null;		
		for (Contrato contratoCliente : listaContrato) {
			if(cdcliente==null && contratoCliente!=null && contratoCliente.getCliente()!=null && contratoCliente.getCliente().getCdpessoa()!=null){
				cdcliente = contratoCliente.getCliente().getCdpessoa();
			}else if(cdcliente!=null && contratoCliente!=null && contratoCliente.getCliente()!=null && contratoCliente.getCliente().getCdpessoa()!=null){
				if(cdcliente != contratoCliente.getCliente().getCdpessoa()){
					request.addError("Para realizar est� opera��o � necess�rio que todos os contratos estejam vinculados a um �nico cliente");
					SinedUtil.fechaPopUp(request);
					return null;
				}				
			}else{
				request.addError("N�o existe cliente vinculado ao contrato: "+contratoCliente.getCdcontrato());
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		List<RealizarFechamentoRomaneioContratoItem> listaFinal = new ArrayList<RealizarFechamentoRomaneioContratoItem>();
		HashMap<Empresa, List<Localarmazenagem>> mapLocaDestinoByEmpresa = new HashMap<Empresa, List<Localarmazenagem>>();
		for (Contrato contratoAux : listaContrato) {	
			if(contratoAux.getEmpresa() != null && mapLocaDestinoByEmpresa.get(contratoAux.getEmpresa()) == null){
				mapLocaDestinoByEmpresa.put(contratoAux.getEmpresa(), localarmazenagemService.findAtivosByEmpresa(contratoAux.getEmpresa()));
			}
			listaFinal.addAll(this.makeListaFechamentoContrato(contratoAux, true));		
			if(listaFinal.size() == 0){
				request.addError("N�o existe nenhum material para realizar fechamento deste contrato.");
				SinedUtil.fechaPopUp(request);
				return null;
			}		
		}
		request.setAttribute("mapLocaDestinoByEmpresa", mapLocaDestinoByEmpresa);
		
		contratoService.ajustaListaFechamentoVariosContratos(listaFinal, listaContrato);
		
		RealizarFechamentoRomaneioContrato bean = new RealizarFechamentoRomaneioContrato();
		bean.setListaItens(listaFinal);
		bean.setWhereInContrato(contrato.getWhereIn());
		
		boolean locacao = contrato.getContratotipo()!=null && Boolean.TRUE.equals(contrato.getContratotipo().getLocacao());
		request.setAttribute("dataLimiteRomaneio", new SimpleDateFormat("dd/MM/yyyy").format(new Date(System.currentTimeMillis())));
		return new ModelAndView("direct:/crud/popup/romaneioFechamento").addObject("bean", bean).addObject("locacao", locacao);
	}
	
	private List<RealizarFechamentoRomaneioContratoItem> makeListaFechamentoContrato(Contrato contrato, Boolean fromFechamento) {
		return makeListaFechamentoContrato(contrato, fromFechamento, false);
	}
	
	private List<RealizarFechamentoRomaneioContratoItem> makeListaFechamentoContrato(Contrato contrato, Boolean fromFechamento, Boolean acaoAbrirRomaneioContrato) {
		List<RealizarFechamentoRomaneioContratoItem> listaDesagrupada = new ArrayList<RealizarFechamentoRomaneioContratoItem>();
		List<RealizarFechamentoRomaneioContratoItem> listaAgrupada = new ArrayList<RealizarFechamentoRomaneioContratoItem>();
		
		List<Contratomaterial> listaContratomaterial = contrato.getListaContratomaterial();
		for (Contratomaterial contratomaterial : listaContratomaterial) {
			contratomaterial.setQtderomaneio(contratomaterial.getQtde());
		}
		
		// PREEENCHE A LISTA DE ITENS COM OS ROMANEIOS DE ORIGEM O CONTRATO
		List<Romaneio> listaRomaneio = romaneioService.findByContrato(contrato);
		for (Romaneio romaneio : listaRomaneio) {
			List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
			
			//listaContratomaterialUtilizado criada para n�o duplicar os itens quando o romaneio de envio tiver o mesmo material
			List<Contratomaterial> listaContratomaterialUtilizado = new ArrayList<Contratomaterial>();
			for (Romaneioitem romaneioitem : listaRomaneioitem) {
				RealizarFechamentoRomaneioContratoItem item = new RealizarFechamentoRomaneioContratoItem();
				Contratomaterial contratomaterial = contratoService.getContratomaterialByMaterial(listaContratomaterial, romaneioitem.getMaterial(), romaneioitem.getPatrimonioitem(), romaneioitem.getQtde());
				if(contratomaterial != null){
					contratomaterial.setContrato(contrato);
					if(listaContratomaterialUtilizado.contains(contratomaterial)){
						continue;
					}
					listaContratomaterialUtilizado.add(contratomaterial);
				} else continue;
				
//				if(contratomaterial.getQtderomaneio() <= 0) continue;
//				contratomaterial.setQtderomaneio(contratomaterial.getQtderomaneio() - romaneioitem.getQtde().intValue());
				
				item.setMaterial(romaneioitem.getMaterial());
				item.setMaterialclasse(romaneioitem.getMaterialclasse());
				item.setPatrimonioitem(romaneioitem.getPatrimonioitem());
				item.setContratomaterial(contratomaterial);
				item.setQtdeutilizada(romaneioitem.getQtde());
				
				// TROCADO PARA O FECHAMENTO REGISTRAR O RETORNO DO MATERIAL
				item.setLocalarmazenagemorigem(romaneio.getLocalarmazenagemdestino());
				if (fromFechamento){
					//AT 49759 - UC0507
					//Sub-fluxo Fechar/Devolver: Local Destino: combo para local armazenamento (Sugerir local principal da empresa do contrato)
					if (romaneio.getEmpresa()!=null && item.getLocalarmazenagemdestino()==null){
						item.setLocalarmazenagemdestino(localarmazenagemService.loadForRomaneioFromContrato(romaneio.getEmpresa(), null, true, null));
					}
				}
				if (item.getLocalarmazenagemdestino()==null){
					item.setLocalarmazenagemdestino(romaneio.getLocalarmazenagemorigem());
					item.setLocalDestinoSugerido(romaneio.getLocalarmazenagemorigem());
				}
				item.setIsFromFechamento(fromFechamento);
				listaDesagrupada.add(item);
			}
		}
		
		// AGRUPA OS ITENS POR MATERIAL E LOCAIS
		for (RealizarFechamentoRomaneioContratoItem it : listaDesagrupada) {
			if(!it.getMaterialclasse().equals(Materialclasse.PATRIMONIO) && listaAgrupada.contains(it)){
				int idx = listaAgrupada.indexOf(it);
				RealizarFechamentoRomaneioContratoItem it_aux = listaAgrupada.get(idx);
				it_aux.setQtdeutilizada(it_aux.getQtdeutilizada() + it.getQtdeutilizada());
				listaAgrupada.set(idx, it_aux);
			} else {
				boolean adicionar = true;
				if(it.getPatrimonioitem() != null){
					int idx = 0;
					for(RealizarFechamentoRomaneioContratoItem it2 : listaAgrupada){
						if(it2.getPatrimonioitem() != null && it2.getPatrimonioitem().equals(it.getPatrimonioitem())){
							RealizarFechamentoRomaneioContratoItem it_aux = listaAgrupada.get(idx);
							it_aux.setQtdeutilizada(it_aux.getQtdeutilizada() + it.getQtdeutilizada());
							listaAgrupada.set(idx, it_aux);
							adicionar = false;
						}
						idx++;
					}
				}
				if(adicionar){
					listaAgrupada.add(it);
				}
			}
		}

		if(acaoAbrirRomaneioContrato == null || !acaoAbrirRomaneioContrato){
			// VERIFICA OS FECHAMENTOS J� REALIZADOS
			List<Romaneio> listaRomaneioFechamento = romaneioService.findByContratoFechamento(contrato);
			for (Romaneio romaneio : listaRomaneioFechamento) {
				List<Contratomaterial> listaContratomaterialUtilizado = new ArrayList<Contratomaterial>();
				List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
				for (Romaneioitem romaneioitem : listaRomaneioitem) {
					RealizarFechamentoRomaneioContratoItem it = new RealizarFechamentoRomaneioContratoItem();
					Contratomaterial contratomaterial = contratoService.getContratomaterialByMaterial(listaContratomaterial, romaneioitem.getMaterial(), romaneioitem.getPatrimonioitem(), romaneioitem.getQtde());
					if(contratomaterial != null){
						contratomaterial.setContrato(contrato);
						if(listaContratomaterialUtilizado.contains(contratomaterial)){
							continue;
						}
						listaContratomaterialUtilizado.add(contratomaterial);
					} else continue;
					
					it.setMaterial(romaneioitem.getMaterial());
					it.setMaterialclasse(romaneioitem.getMaterialclasse());
					it.setPatrimonioitem(romaneioitem.getPatrimonioitem());
					it.setContratomaterial(contratomaterial);
					it.setIsFromFechamento(fromFechamento);
					
					// TROCADO PARA O FECHAMENTO REGISTRAR O RETORNO DO MATERIAL
					it.setLocalarmazenagemorigem(romaneio.getLocalarmazenagemorigem());
					it.setLocalarmazenagemdestino(romaneio.getLocalarmazenagemdestino());
					it.setLocalDestinoSugerido(romaneio.getLocalarmazenagemdestino());
					
					if(!it.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
						if(listaAgrupada.contains(it)){
							int idx = listaAgrupada.indexOf(it);
							RealizarFechamentoRomaneioContratoItem it_aux = listaAgrupada.get(idx);
							it_aux.setQtdeutilizada(it_aux.getQtdeutilizada() - romaneioitem.getQtde());
							listaAgrupada.set(idx, it_aux);
						} 
					} else {
						if(listaAgrupada != null && !listaAgrupada.isEmpty() && it != null && it.getPatrimonioitem() != null && it.getPatrimonioitem() != null){
							for(RealizarFechamentoRomaneioContratoItem bean : listaAgrupada){
								if(bean.getPatrimonioitem() != null && bean.getPatrimonioitem().equals(it.getPatrimonioitem())){
									bean.setQtdeutilizada(bean.getQtdeutilizada() - romaneioitem.getQtde());
									break;
								}
							}
						}
					}
				}
			}
		}
		
		List<RealizarFechamentoRomaneioContratoItem> listaFinal = new ArrayList<RealizarFechamentoRomaneioContratoItem>();
		
		// SETA VALORES PADR�ES NA LISTA
		for (RealizarFechamentoRomaneioContratoItem it : listaAgrupada) {
			if(it.getQtdeutilizada() != null && it.getQtdeutilizada() > 0d){
				
				it.setQtdecontratada(contratoService.getQtdeMaterial(listaContratomaterial, it.getMaterial()));
				it.setQtdedevolvida(it.getQtdeutilizada());
				
				listaFinal.add(it);
			}
		}
		
		Collections.sort(listaFinal, new Comparator<RealizarFechamentoRomaneioContratoItem>(){
			public int compare(RealizarFechamentoRomaneioContratoItem o1, RealizarFechamentoRomaneioContratoItem o2) {
				if(!o1.getMaterial().getNome().equals(o2.getMaterial().getNome())){
					return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
				} else if(!o1.getLocalarmazenagemorigem().getNome().equals(o2.getLocalarmazenagemorigem().getNome())){
					return o1.getLocalarmazenagemorigem().getNome().compareTo(o2.getLocalarmazenagemorigem().getNome());
				} else {
					return o1.getLocalarmazenagemdestino().getNome().compareTo(o2.getLocalarmazenagemdestino().getNome());
				}
			}
		});
		return listaFinal;
	}
	
	/**
	 * M�todo que salva a popup de fechamento de contrato.
	 * Podendo ser redirecionado para a tela de conta a receber.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 17/12/2012
	 */
	public ModelAndView saveFechamento(WebRequestContext request, RealizarFechamentoRomaneioContrato bean){	
		
		if(bean.getWhereInContrato()==null || bean.getWhereInContrato().isEmpty()){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		boolean romaneioGerado = false;
		
		List<Contrato> listaContrato = contratoService.loadListForRomaneio(bean.getWhereInContrato());
		for (Contrato contrato : listaContrato) {
			String identificadorContrato = contrato.getIdentificador() != null && !contrato.getIdentificador().trim().equals("") ? contrato.getIdentificador() : contrato.getCdcontrato().toString();
			
			List<Contratomaterial> listaContratomaterial = contrato.getListaContratomaterial();
			
			Empresa empresa = contrato.getEmpresa();
			Projeto projeto = null;
			
			Rateio rateio = contrato.getRateio();
			if(rateio != null){
				List<Rateioitem> listaRateioitem = rateio.getListaRateioitem();
				if(listaRateioitem != null){
					for (Rateioitem rateioitem : listaRateioitem) {
						if(projeto == null){
							projeto = rateioitem.getProjeto();
						} else if(projeto != null && 
									rateioitem.getProjeto() != null && 
									!projeto.equals(rateioitem.getProjeto())){
							projeto = null;
							break;
						}
					}
				}
			}
			
			Map<MultiKey, List<RealizarFechamentoRomaneioContratoItem>> map = new HashMap<MultiKey, List<RealizarFechamentoRomaneioContratoItem>>();
			List<RealizarFechamentoRomaneioContratoItem> listaItens = bean.getListaItens();
			
			boolean devolucaoCompleta = true;
			for (RealizarFechamentoRomaneioContratoItem it : listaItens) {
				if(it.getQtdedevolvida() != null && it.getQtdedevolvida() > 0){
					MultiKey key = new MultiKey(new Object[]{it.getLocalarmazenagemorigem(), it.getLocalarmazenagemdestino()});
					List<RealizarFechamentoRomaneioContratoItem> listaIt;
					
					if(map.containsKey(key)){
						listaIt = map.get(key);
					} else {
						listaIt = new ArrayList<RealizarFechamentoRomaneioContratoItem>();
					}
					
					listaIt.add(it);
					map.put(key, listaIt);
				}
				
				if(it.getQtdedevolvida() != null && it.getQtdedevolvida() < it.getQtdeutilizada()){
					devolucaoCompleta = false;
				}
			}
			
			List<Contratomateriallocacao> listaContratomateriallocacao = new ArrayList<Contratomateriallocacao>();
			Contratomateriallocacao contratomateriallocacao;
			
			Timestamp atual = new Timestamp(System.currentTimeMillis());
			List<Romaneio> listaRomaneio = new ArrayList<Romaneio>();
			Date dtromaneio = bean.getDtromaneio() != null ? bean.getDtromaneio() : SinedDateUtils.currentDate();
			
			Set<Entry<MultiKey, List<RealizarFechamentoRomaneioContratoItem>>> entrySet = map.entrySet();
			for (Entry<MultiKey, List<RealizarFechamentoRomaneioContratoItem>> entry : entrySet) {
				
				MultiKey key = entry.getKey();
				List<RealizarFechamentoRomaneioContratoItem> listaMateriais = entry.getValue();
				
				boolean haveMaterial = false;
				for (RealizarFechamentoRomaneioContratoItem item : listaMateriais) {
					if(item.getContratomaterial() == null || (listaContratomaterial == null || 
							listaContratomaterial.isEmpty() || listaContratomaterial.contains(item.getContratomaterial()))){
						haveMaterial = true;
					}
				}
				
				if(haveMaterial){
					Localarmazenagem localarmazenagemorigem = (Localarmazenagem)key.getKey(0);
					Localarmazenagem localarmazenagemdestino = (Localarmazenagem)key.getKey(1);
					
					// CRIA��O DO ROMANEIO
					Romaneio romaneio = new Romaneio();
					romaneio.setDtromaneio(SinedDateUtils.currentDate());
					romaneio.setDescricao("Devolu��o do(s) contrato(s) " + identificadorContrato);
					romaneio.setEmpresa(empresa);
					romaneio.setLocalarmazenagemorigem(localarmazenagemorigem);
					romaneio.setLocalarmazenagemdestino(localarmazenagemdestino);
					romaneio.setRomaneiosituacao(Romaneiosituacao.BAIXADA);
					romaneio.setRomaneiotipo(Romaneiotipo.ENTRADA);
					romaneio.setPlaca(bean.getPlaca());
					romaneio.setMotorista(bean.getMotorista());
					romaneio.setDtromaneio(dtromaneio);
					
					romaneioService.saveOrUpdate(romaneio);
					
					listaRomaneio.add(romaneio);
					
					// CRIA��O DA ORIGEM DO ROMANEIO (FECHAMENTO)
					Romaneioorigem romaneioorigem = new Romaneioorigem();
					romaneioorigem.setRomaneio(romaneio);
					romaneioorigem.setContratofechamento(contrato);
					
					romaneioorigemService.saveOrUpdate(romaneioorigem);
					
					for (RealizarFechamentoRomaneioContratoItem item : listaMateriais) {
						if(item.getContratomaterial() == null || (listaContratomaterial == null || 
								listaContratomaterial.isEmpty() || listaContratomaterial.contains(item.getContratomaterial()))){
							// CRIA��O DO ITEM ROMANEIO
							Romaneioitem romaneioitem = new Romaneioitem();
							romaneioitem.setMaterial(item.getMaterial());
							romaneioitem.setQtde(item.getQtdedevolvida());
							romaneioitem.setMaterialclasse(item.getMaterialclasse());
							romaneioitem.setRomaneio(romaneio);
							romaneioitem.setPatrimonioitem(item.getPatrimonioitem());
							
							romaneioitemService.saveOrUpdate(romaneioitem);
							
							// GERA A ENTRADA E SA�DA DO ROMANEIO
							romaneioService.gerarEntradaSaidaRomaneio(romaneio, romaneioitem, contratoService.getValorUnitarioMaterial(listaContratomaterial, item.getMaterial()), empresa, projeto, null, empresa, projeto, false, null, null, null, null);
							
							if(item.getContratomaterial() != null){
								contratomateriallocacao = new Contratomateriallocacao();
								contratomateriallocacao.setContratomaterial(item.getContratomaterial());
								contratomateriallocacao.setContratomateriallocacaotipo(Contratomateriallocacaotipo.ENTRADA);
								contratomateriallocacao.setDtmovimentacao(romaneio.getDtromaneio() != null ? romaneio.getDtromaneio() : new Date(System.currentTimeMillis()));
								contratomateriallocacao.setQtde(item.getQtdedevolvida());
								contratomateriallocacao.setDtaltera(new Timestamp(System.currentTimeMillis()));
								contratomateriallocacao.setCdromaneio(romaneio.getCdromaneio());
								listaContratomateriallocacao.add(contratomateriallocacao);
							}
						}
					}
				}
			}
			
			if(listaContratomateriallocacao != null && !listaContratomateriallocacao.isEmpty()){
				for(Contratomateriallocacao cmlocacao : listaContratomateriallocacao){
					contratomateriallocacaoService.saveOrUpdate(cmlocacao);
				}
			}
			
			boolean finalizarContrato = Boolean.TRUE.equals(bean.getFinalizarContrato()) && (listaRomaneio.size() > 0 || Boolean.TRUE.equals(bean.getGerarIndenizacao()));
			if(finalizarContrato){
				Contrato contratoValidacao = contratoService.loadForRomaneio(contrato);
				List<RealizarFechamentoRomaneioContratoItem> listaFinal = makeListaFechamentoContrato(contratoValidacao, true);
				if (listaFinal != null && !listaFinal.isEmpty()){
					finalizarContrato = Boolean.FALSE;
					if(bean.getGerarIndenizacao() == null || !bean.getGerarIndenizacao())
						request.addMessage("N�o � poss�vel finalizar o contrato " + contratoValidacao.getIdentificadorOrCdcontrato() + ", pois existe pend�ncia de Devolu��o/Indeniza��o", MessageType.WARN);
				}
			}
			if(listaRomaneio.size() > 0){
				Contratohistorico contratohistorico = new Contratohistorico();
				contratohistorico.setDthistorico(atual);
				contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
				
				StringBuilder sb = new StringBuilder();
				for (Romaneio romaneio : listaRomaneio) {
					if(sb.length() > 0) sb.append(", ");
					sb.append(romaneioService.geraLinkRomaneio(romaneio));
				}
				
				Contratoacao acao = Contratoacao.FECHAMENTO_ROMANEIO;
				String observacao = "Devolu��o do(s) contrato(s) " + sb.toString() + " gerado(s).";
				if(devolucaoCompleta){
					if(finalizarContrato){
						acao = Contratoacao.FECHAMENTO_ROMANEIO;
					} else {
						acao = Contratoacao.DEVOLUCAO_ROMANEIO;
					}
				} else {
					acao = Contratoacao.DEVOLUCAO_ROMANEIO;
					observacao = "Devolu��o do(s) contrato(s) " + sb.toString() + " gerado(s) parcialmente.";
				}
				
				contratohistorico.setAcao(acao);
				contratohistorico.setContrato(contrato);
				contratohistorico.setObservacao(observacao);
				contratohistoricoService.saveOrUpdate(contratohistorico);
				
				request.addMessage("Fechamento de contrato " + identificadorContrato + " realizado com sucesso.");
				romaneioGerado = true;
			} else {
				request.addMessage("Nenhum romaneio gerado para o contrato " + identificadorContrato + ".");
			}
			
			if (finalizarContrato){
				contrato.setDtfim(bean.getDtromaneio() != null ? bean.getDtromaneio() : SinedDateUtils.currentDate());
				contratoService.atualizaDataFimContrato(contrato);
				contratoService.atualizaDataConclusaoContrato(contrato);
				
				Contratohistorico contratohistorico = new Contratohistorico();
				contratohistorico.setDthistorico(atual);
				contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
				contratohistorico.setAcao(Contratoacao.FINALIZADO);
				contratohistorico.setContrato(contrato);
				contratohistoricoService.saveOrUpdate(contratohistorico);
			}
		}
		
		if(bean.getGerarIndenizacao() != null && bean.getGerarIndenizacao()){
			request.clearMessages();
			if(romaneioGerado){
				request.addMessage("Romaneio de devolu��o gerado com sucesso.");
			} else {
				request.addMessage("Nenhum romaneio de devolu��o gerado.");
			}
			
			Contrato contrato = new Contrato();
			contrato.setWhereIn(bean.getWhereInContrato());
			contrato.setIndenizacao(true);
			contrato.setFinalizarcontrato(bean.getFinalizarContrato());
			return continueOnAction("abrirRomaneioFechamento", contrato);
		} else {
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	public ModelAndView abrirGerarProducaoConferencia(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Contratomaterial> listaContratomaterial = contratomaterialService.findByContratoProducao(whereIn);
		if(listaContratomaterial == null || listaContratomaterial.size() == 0){
			request.addError("Nenhum material encontrado para gerar produ��o.");
			return sendRedirectToAction("listagem");
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Timestamp primeiraEntrega = null;
		
		try{
			for (Contratomaterial contratomaterial : listaContratomaterial) {
				if(contratomaterial.getDtinicio() != null){
					String strDate = dateFormat.format(contratomaterial.getDtinicio());
					String strHour = "00:00";
					
					if(contratomaterial.getHrinicio() != null){
						strHour = contratomaterial.getHrinicio().toString();
					}
					
					Date data = SinedDateUtils.stringToDate(strDate + " " + strHour, "dd/MM/yyyy HH:mm");
					if(primeiraEntrega == null){
						primeiraEntrega = new Timestamp(data.getTime());
					} else {
						if(primeiraEntrega.getTime() != data.getTime()){
							primeiraEntrega = null;
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		GerarProducaoConferenciaBean bean = new GerarProducaoConferenciaBean();
		bean.setWhereInContrato(whereIn);
		
		if(primeiraEntrega != null){
			bean.setDtprimeiraentrega(primeiraEntrega);
		}
		
		request.setAttribute("pathAjax", "/faturamento/crud/Contrato");
		
		return new ModelAndView("/process/gerarProducaoConferencia", "bean", bean);
	}
	
	public ModelAndView ajaxBuscarDadosProducao(WebRequestContext request, GerarProducaoConferenciaBean bean){
		List<Contratomaterial> listaContratomaterial = contratomaterialService.findByContratoProducao(bean.getWhereInContrato());
		List<GerarProducaoConferenciaMaterialBean> listaMaterial = new ArrayList<GerarProducaoConferenciaMaterialBean>();
		
		for (Contratomaterial contratomaterial : listaContratomaterial) {
			if(contratomaterial.getListaContratomaterialitem() != null && contratomaterial.getListaContratomaterialitem().size() > 0){
				for (Contratomaterialitem contratomaterialitem : contratomaterial.getListaContratomaterialitem()) {
					listaMaterial.add(new GerarProducaoConferenciaMaterialBean(contratomaterialitem.getMaterial(), 
																				contratomaterialitem.getQtde(), 
																				contratomaterial.getContrato(),
																				contratomaterial.getContrato().getCliente(),
																				contratomaterial.getContrato().getEmpresa()));
				}
			} else {
				listaMaterial.add(new GerarProducaoConferenciaMaterialBean(contratomaterial.getServico(), 
																		contratomaterial.getQtde() != null ? contratomaterial.getQtde().doubleValue() : 0d, 
																		contratomaterial.getContrato(),
																		contratomaterial.getContrato().getCliente(),
																		contratomaterial.getContrato().getEmpresa()));
			}
		}
		
		Collections.sort(listaMaterial, new Comparator<GerarProducaoConferenciaMaterialBean>(){
			public int compare(GerarProducaoConferenciaMaterialBean o1, GerarProducaoConferenciaMaterialBean o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		Integer qtdeentregas = bean.getQtdeentregas();
		Frequencia frequencia = bean.getFrequencia();
		Timestamp dtprimeiraentrega = bean.getDtprimeiraentrega();
		
		for (GerarProducaoConferenciaMaterialBean materialBean : listaMaterial) {
			Timestamp data_aux = new Timestamp(System.currentTimeMillis());
			if(dtprimeiraentrega != null) 
				data_aux.setTime(dtprimeiraentrega.getTime());
			
			List<GerarProducaoConferenciaMaterialItemBean> listaItem = new ArrayList<GerarProducaoConferenciaMaterialItemBean>();
			
			Double qtdedividida = materialBean.getQtde()/qtdeentregas;
			Double qtdetotal = 0d;
			for (int i = 0; i < qtdeentregas; i++) {
				qtdetotal += qtdedividida;
				if(qtdeentregas.equals(i+1) && !qtdetotal.equals(materialBean.getQtde())){
					qtdedividida += (materialBean.getQtde() - qtdetotal);
				}
				
				listaItem.add(new GerarProducaoConferenciaMaterialItemBean(qtdedividida, data_aux));
				
				if(frequencia != null){
					data_aux = SinedDateUtils.incrementTimestampFrequencia(data_aux, frequencia, 1);
				}
			}
			
			materialBean.setListaItem(listaItem);
		}

		request.setAttribute("listaMaterial", listaMaterial);
		request.setAttribute("qtdeentregas", qtdeentregas);
		
		return new ModelAndView("direct:/process/gerarProducaoConferenciaMateriais");
	}
	
	public ModelAndView salvarGerarProducao(WebRequestContext request, GerarProducaoConferenciaBean bean){
		
		try{
			List<GerarProducaoConferenciaMaterialBean> listaMaterial = bean.getListaMaterial();
			List<Producaoagenda> listaProducaoagenda = new ArrayList<Producaoagenda>();
			
			for (GerarProducaoConferenciaMaterialBean materialBean : listaMaterial) {
				Empresa empresa = materialBean.getEmpresa();
				Cliente cliente = materialBean.getCliente();
				Contrato contrato = materialBean.getContrato();
				Material material = materialBean.getMaterial();
				List<GerarProducaoConferenciaMaterialItemBean> listaItem = materialBean.getListaItem();
				
				for (GerarProducaoConferenciaMaterialItemBean itemBean : listaItem) {
					Timestamp data = itemBean.getData();
					Double qtde = itemBean.getQtde();
					
					if(qtde != null && qtde > 0 && data != null){
						Producaoagendamaterial producaoagendamaterial = new Producaoagendamaterial();
						producaoagendamaterial.setMaterial(material);
						producaoagendamaterial.setQtde(qtde);
						
						boolean achou = false;
						for (Producaoagenda producaoagenda : listaProducaoagenda) {
							Timestamp data_producaoagenda = producaoagenda.getDtentrega();
							Contrato contrato_producaoagenda = producaoagenda.getContrato();
							
							if(data.equals(data_producaoagenda) && contrato.equals(contrato_producaoagenda)){
								achou = true;
								
								producaoagenda.getListaProducaoagendamaterial().add(producaoagendamaterial);
								break;
							}
						}
						
						if(!achou){
							Producaoagenda producaoagenda = new Producaoagenda();
							producaoagenda.setCliente(cliente);
							producaoagenda.setContrato(contrato);
							producaoagenda.setEmpresa(empresa);
							producaoagenda.setDtentrega(data);
							
							Set<Producaoagendamaterial> listaProducaoagendamaterial = new ListSet<Producaoagendamaterial>(Producaoagendamaterial.class);
							listaProducaoagendamaterial.add(producaoagendamaterial);
							producaoagenda.setListaProducaoagendamaterial(listaProducaoagendamaterial);
							
							listaProducaoagenda.add(producaoagenda);
						}
					}
				}
			}
			
			Map<Contrato, List<Producaoagenda>> mapContratoHistorico = new HashMap<Contrato, List<Producaoagenda>>();
			
			boolean gerouProducao = false;
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				gerouProducao = true;
				
				producaoagendaService.saveOrUpdate(producaoagenda);
				
				Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
				producaoagendahistorico.setProducaoagenda(producaoagenda);
				producaoagendahistorico.setObservacao("Criado");
				producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
				
				if(mapContratoHistorico.containsKey(producaoagenda.getContrato())){
					List<Producaoagenda> list = mapContratoHistorico.get(producaoagenda.getContrato());
					list.add(producaoagenda);
					mapContratoHistorico.put(producaoagenda.getContrato(), list);
				} else {
					List<Producaoagenda> list = new ArrayList<Producaoagenda>();
					list.add(producaoagenda);
					mapContratoHistorico.put(producaoagenda.getContrato(), list);
				}
			}
			
			Set<Entry<Contrato, List<Producaoagenda>>> entrySet = mapContratoHistorico.entrySet();
			
			for (Entry<Contrato, List<Producaoagenda>> entry : entrySet) {
				Contratohistorico contratohistorico = new Contratohistorico();
				contratohistorico.setAcao(Contratoacao.GERAR_PRODUCAO);
				contratohistorico.setContrato(entry.getKey());
				contratohistorico.setDthistorico(new Timestamp(System.currentTimeMillis()));
				contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
				contratohistorico.setObservacao("Agenda(s) de produ��o(�es) geradas: " + producaoagendaService.makeLinkHistoricoProducaoagenda(entry.getValue()));
				
				contratohistoricoService.saveOrUpdate(contratohistorico);
			}
			
			if(gerouProducao){
				request.addMessage("Produ��o gerada com sucesso.");
			} else {
				request.addMessage("Nenhum registro de produ��o foi gerado.", MessageType.WARN);
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o da produ��o: " + e.getMessage());
		}
		
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * M�todo ajax para buscar as taxas da conta para o contrato
	 *
	 * @param request
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxBuscarTaxaByConta(WebRequestContext request, Conta conta){
		List<Taxaitem> listaTaxaitem = null;
		if(conta.getCdconta() != null){
			conta = contaService.loadWithTaxa(conta);
			if(conta != null && conta.getTaxa() != null && 
					conta.getTaxa().getListaTaxaitem() != null && !conta.getTaxa().getListaTaxaitem().isEmpty()){
				listaTaxaitem = new ArrayList<Taxaitem>();
				listaTaxaitem.addAll(conta.getTaxa().getListaTaxaitem());
			}
		}
		return new JsonModelAndView().addObject("taxas", listaTaxaitem);
	}
	
	/**
	 * M�todo que abre popup para faturar loca��o
	 *
	 * @param request
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirFaturarLocacao(WebRequestContext request){
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		String[] ids = whereIn.split(",");
		
		boolean erro = false;
		
		if(isFaturamentoWS(request, whereIn, "popup")){
			return null;
		}
		
//		if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.ATRASADO, SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.EM_ESPERA, SituacaoContrato.CANCELADO, SituacaoContrato.ISENTO, SituacaoContrato.SUSPENSO)){
//			request.addError("Para gerar faturamento, o(s) contrato(s) deve(m) estar com situa��o 'A CONSOLIDAR' ou 'ATEN��O'.");
//			SinedUtil.fechaPopUp(request);
//			return null;
//		}
//		
//		List<Contrato> listaContratoWithParcela = contratoService.findWithParcela(whereIn);
//		if(SinedUtil.isListNotEmpty(listaContratoWithParcela)){
//			for(Contrato c : listaContratoWithParcela){
//				if(c.getPrazopagamento() != null && SinedUtil.isListNotEmpty(c.getListaContratoparcela())){
//					Contratoparcela contratoparcela = contratoService.getParcelaAtual(c.getListaContratoparcela());
//					if(contratoparcela == null){
//						request.addError("N�o existe parcela a ser cobrada para o contrato " + c.getIdentificadorOrCdcontrato());
//						SinedUtil.fechaPopUp(request);
//						return null;
//					}
//				}
//			}
//		}
//		
//		if("FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.FATURAR_APOS_DEVOLUCAO))){
//			for(String id_contrato : ids){
//				if(romaneioService.isUltimoRomaneioDevolucaoNotCanceladoByContrato(new Contrato(Integer.parseInt(id_contrato)))){
//					request.addError("N�o � permitido gerar fatura de loca��o ap�s um romaneio de devolu��o realizado. Contrato " + id_contrato);
//					SinedUtil.fechaPopUp(request);
//					return null;
//				}
//			}
//		}
		
		List<Contrato> listaContrato = contratoService.findForValidacaoAgrupamentoCliente(whereIn);
		for (Contrato contrato : listaContrato) {	
			String id_contrato = contrato.getIdentificador() != null ? contrato.getIdentificador() : contrato.getCdcontrato().toString();
			
			if(contrato.getFrequencia() != null && 
					contrato.getFrequencia().getCdfrequencia() != null && 
					contrato.getFrequencia().getCdfrequencia().equals(Frequencia.UNICA)){
				List<RealizarFechamentoRomaneioContratoItem> listaFinal = this.makeListaFechamentoContrato(contrato, false);
				if(listaFinal != null && listaFinal.size() > 0){
					request.addError("N�o foi poss�vel faturar o contrato " + id_contrato + " pois este contrato � de periodicidade �NICA e possui materiais a serem devolvidos ou indenizados.");
					erro = true;
				}
			}
			
			if((contrato.getEnderecocobranca() == null || contrato.getEnderecocobranca().getCdendereco() == null) && (contrato.getEndereco() == null || contrato.getEndereco().getCdendereco() == null)){
				request.addError("N�o foi poss�vel faturar o contrato " + id_contrato + " pois est� sem o endere�o de faturamento.");
				erro = true;
			}
			if(contrato.getDtiniciolocacao() == null || contrato.getDtfimlocacao() == null){
				request.addError("Favor preencher o per�odo de loca��o para o faturamento do contrato " + id_contrato + ".");
				erro = true;
			}
		}
		if(erro){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(ids.length == 1){
			listaContrato = contratoService.loadForFaturarlocacao(whereIn);
			Contrato contrato = listaContrato.get(0);
			
			contratoService.calculaValorFaturaLocacao(contrato);
			
			ResumoRomaneio resumoRomaneio = romaneioService.makeResumoRomaneio(contrato.getCdcontrato() + "", null, true, contrato.getListaContratomaterial());
			request.setAttribute("listaResumoMaterialRomaneio", resumoRomaneio.getListaResumoMaterialRomaneio());
			
			request.setAttribute("dataInicio", SinedDateUtils.toString(contrato.getDtiniciolocacao()));
			request.setAttribute("dataFim", SinedDateUtils.toString(contrato.getDtfimlocacao()));
			
			request.setAttribute("entrada", entrada);
			
			Double somaAjudantes = 0.0;
			for(Romaneioorigem item : contrato.getListaRomaneioorigem()){
				if(item.getRomaneio().getQtdeajudantes() != null 
						&& item.getRomaneio().getDtromaneio().getTime() >= contrato.getDtiniciolocacao().getTime()
						&& item.getRomaneio().getDtromaneio().getTime() <= contrato.getDtfimlocacao().getTime()
						&& !item.getRomaneio().getRomaneiosituacao().equals(Romaneiosituacao.CANCELADA)){
					somaAjudantes += item.getRomaneio().getQtdeajudantes();
				}
			}
			
			request.setAttribute("somaAjudantes", SinedUtil.retiraZeroDireita(somaAjudantes));
			
			return new ModelAndView("direct:/crud/popup/abrirFaturarLocacao", "contrato", contrato);
		} else {
			boolean mesmoCliente = false;
			
			List<Integer> listaIdCliente = new ArrayList<Integer>(); 
			for (Contrato c : listaContrato) {
				Cliente cliente = c.getCliente();
				if(cliente != null && cliente.getCdpessoa() != null){
					if(listaIdCliente.contains(cliente.getCdpessoa())){
						mesmoCliente = true;
						break;
					} else {
						listaIdCliente.add(cliente.getCdpessoa());
					}
				}
			}
			
			boolean faturamentolote = contratoService.existeFaturamentolote(whereIn);

			if(mesmoCliente && faturamentolote){
				request.setAttribute("whereIn", whereIn);
				return new ModelAndView("direct:/crud/popup/abrirConfirmacaoFaturarLocacao");
			} else {
				return continueOnAction("gerarFaturamentoLocacao", null);
			}
		}
	}
	
	public void gerarFaturamentoLocacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Boolean agrupamento = Boolean.valueOf(request.getParameter("agrupamento") != null ? request.getParameter("agrupamento") : "false");
		
		Documentotipo documentotipo = documentotipoService.findBoleto();
		List<Contrato> listaContrato = contratoService.loadForFaturarlocacao(whereIn);
		
		List<FaturamentoConjuntoBean> listaFaturamentoConjuntoBean = new ArrayList<FaturamentoConjuntoBean>();
		
		boolean existeContratoIgnorarValorMaterial = false;
		if(Boolean.TRUE.equals(agrupamento)){
			for (Contrato contrato : listaContrato) {
				if(Boolean.TRUE.equals(contrato.getIgnoravalormaterial())){
					existeContratoIgnorarValorMaterial = true;
					break;
				}
			}
		}
		
		for (Contrato contrato : listaContrato) {
			if(contrato.getRateio() != null && contrato.getRateio().getCdrateio() != null){
				contrato.setRateio(rateioService.findRateio(contrato.getRateio()));
			}
			if(contrato.getTaxa() != null && contrato.getTaxa().getCdtaxa() != null){
				contrato.setTaxa(taxaService.findTaxa(contrato.getTaxa()));
			}
			contratoService.calculaValorFaturaLocacao(contrato);
			
			Contratofaturalocacao contratofaturalocacao = contratofaturalocacaoService.criaContratofaturalocacaoByContrato(contrato, contrato.getFaturalocacaoValorfaturar(), existeContratoIgnorarValorMaterial);
			contrato = contratoService.selecionarEnderecoAlternativoDeCobranca(contrato);
			Documento documento = contratoService.criaContaReceberContrato(contrato, documentotipo, true);
			
			documento.setValor(contrato.getFaturalocacaoValorfaturar());
			rateioService.atualizaValorRateio(documento.getRateio(), documento.getValor());
			
			FaturamentoConjuntoBean faturamentoConjuntoBean = new FaturamentoConjuntoBean();
			faturamentoConjuntoBean.setDocumento(documento);
			faturamentoConjuntoBean.setContratofaturalocacao(contratofaturalocacao);
			faturamentoConjuntoBean.setCliente(contrato.getCliente());
			
			List<Contrato> contratos = new ArrayList<Contrato>();
			contratos.add(contrato);
			faturamentoConjuntoBean.setListaContrato(contratos);
			
			listaFaturamentoConjuntoBean.add(faturamentoConjuntoBean);
		}
		
		if(agrupamento){
			List<FaturamentoConjuntoBean> listaFaturamentoConjuntoAgrupado = new ArrayList<FaturamentoConjuntoBean>();
			for (FaturamentoConjuntoBean bean: listaFaturamentoConjuntoBean) {
				Contratofaturalocacao contratofaturalocacao = bean.getContratofaturalocacao();
				Documento documento = bean.getDocumento();
				
				boolean achou = false;
				for (FaturamentoConjuntoBean bean2 : listaFaturamentoConjuntoAgrupado) {
					if(bean.getCliente().equals(bean2.getCliente())){
						achou = true;
						
						Contratofaturalocacao contratofaturalocacao2 = bean2.getContratofaturalocacao();
						contratofaturalocacao2.setContrato(null);
						//contratofaturalocacao2.setDescricao(contratofaturalocacao2.getDescricao() + " " + contratofaturalocacao.getDescricao());
						if(StringUtils.isNotBlank(contratofaturalocacao.getDadosadicionais())){
							if(StringUtils.isNotBlank(contratofaturalocacao2.getDadosadicionais())){
								contratofaturalocacao2.setDadosadicionais(contratofaturalocacao2.getDadosadicionais() + " " + contratofaturalocacao.getDadosadicionais());
							}else {
								contratofaturalocacao2.setDadosadicionais(contratofaturalocacao.getDadosadicionais());
							}
						}
						contratofaturalocacao2.setValortotal(contratofaturalocacao2.getValortotal().add(contratofaturalocacao.getValortotal()));
						contratofaturalocacao2.getListaContratofaturalocacaomaterial().addAll(contratofaturalocacao.getListaContratofaturalocacaomaterial());
						
						Documento documento2 = bean2.getDocumento();
						documento2.setDescricao("Fatura de loca��o");
						documento2.setValor(documento2.getValor().add(documento.getValor()));
						documento2.getRateio().getListaRateioitem().addAll(documento.getRateio().getListaRateioitem());
						rateioService.ajustePercentualRateioByDocumento(documento2);
						rateioitemService.agruparItensRateio(documento2.getRateio().getListaRateioitem());
						
						List<Contrato> contratos2 = bean2.getListaContrato();
						contratos2.addAll(bean.getListaContrato());
						
						break;
					}
				}
				
				if(!achou){
					listaFaturamentoConjuntoAgrupado.add(bean);
				}
			}
			
			listaFaturamentoConjuntoBean = listaFaturamentoConjuntoAgrupado;
		}
		
		
		
		for (FaturamentoConjuntoBean bean : listaFaturamentoConjuntoBean) {
			Documento documento = bean.getDocumento();
			Contratofaturalocacao contratofaturalocacao = bean.getContratofaturalocacao();
			
			List<Contrato> contratos = bean.getListaContrato();
			contratofaturalocacao.setListaContrato(contratos);
			
			if(contratos != null && contratos.size() > 1){
				contratofaturalocacao.setDescricao(contratofaturalocacaoService.getDescricaoForFaturarLocacao(contratos));
			}
			contratofaturalocacaoService.gerarNumeroSequencial(contratofaturalocacao);
			contratofaturalocacaoService.saveFromFaturamento(contratofaturalocacao, documento);
			
			contratofaturalocacaoService.criaDocumentoFaturaContratoLocacao(documento, contratofaturalocacao, contratos, true, true);
		}
		
		for(Contrato contrato : listaContrato){
			if(contrato.getAcrescimoproximofaturamento() != null && contrato.getAcrescimoproximofaturamento()){
				contratoService.updateZeraAcrescimo(contrato);
			}
		}
		
		request.addMessage("Contrato(s) faturado(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	
	/**
	 * M�todo que gerar o faturamento de loca��o
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public void gerarFaturamentoLocacaoIndividual(WebRequestContext request, Contrato contrato){
		try{
			if (Boolean.TRUE.equals(contrato.getFinalizarcontrato()) && contratoService.isLocacao(contrato)){
				Contrato contrato_aux = contratoService.loadForRomaneio(contrato);
				List<RealizarFechamentoRomaneioContratoItem> listaFinal = this.makeListaFechamentoContrato(contrato_aux, false);
				if (!listaFinal.isEmpty()){
					request.addError("N�o � permitido finalizar contrato com Material faltante ou sem Indeniza��o");
					SinedUtil.fechaPopUp(request);
					return;
				}
			}
			
			Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
			
			String acao = ACAO_CONTRATO_LISTAGEM;
			if(entrada != null && entrada){
				acao = ACAO_CONTRATO_ENTRADA;
			}
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/financeiro/crud/Contareceber?ACAO=criar" +
															"&cdcontrato=" + contrato.getCdcontrato()+
															"&nomeProcessoFluxoAnterior=" + acao + 
															"&finalizarContrato=" + (contrato.getFinalizarcontrato() != null ? contrato.getFinalizarcontrato().toString() : "false") + 
															"&faturarlocacao=true" + 
															"&valorFaturarlocacao=" + contrato.getFaturalocacaoValorfaturar().getValue().doubleValue() + "&subfluxo=gerarFaturamentoLocacao';</script>");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
		}
	}
	
	/**
	 * M�todo que abre a popup de lan�amento de desconto
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirLancardesconto(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return new ModelAndView();
		}
		
		List<Contrato> listaContrato = contratoService.findForLancardesconto(whereIn);
		if(listaContrato == null || listaContrato.isEmpty()){
			request.addError("Nenhum contrato encontrado para lan�ar descontos. N�o � poss�vel lan�ar descontos em contratos com prazo de pagamento.");
			SinedUtil.fechaPopUp(request);
			return new ModelAndView();
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		request.setAttribute("entrada", entrada);
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("acaoSalvar", "lancardesconto");	
		
		Contrato contrato = new Contrato();
		return new ModelAndView("direct:/crud/popup/lancardescontocontrato", "bean", contrato);
	}
	
	/**
	 * M�todo que registra o lan�amento de desconto no(s) contrato(s)
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView lancardesconto(WebRequestContext request, Contrato bean){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return new ModelAndView();
		}
		
		if(bean.getListaContratodesconto() == null || bean.getListaContratodesconto().isEmpty()){
			request.addError("Nenhum desconto adicionado.");
			SinedUtil.fechaPopUp(request);
			return new ModelAndView();
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		List<Contrato> listaContrato = contratoService.findForLancardesconto(whereIn);
		if(listaContrato != null && !listaContrato.isEmpty()){
			Contratodesconto contratodesconto;
			for(Contrato contrato : listaContrato){
				for(Contratodesconto cdesconto : bean.getListaContratodesconto()){
					contratodesconto = new Contratodesconto();
					contratodesconto.setContrato(contrato);
					contratodesconto.setDtfim(cdesconto.getDtfim());
					contratodesconto.setDtinicio(cdesconto.getDtinicio());
					contratodesconto.setMotivo(cdesconto.getMotivo());
					contratodesconto.setTipodesconto(cdesconto.getTipodesconto());
					contratodesconto.setValor(cdesconto.getValor());
					
					contratodescontoService.saveOrUpdate(contratodesconto);
				}
			}
		}
		
		request.addMessage("Desconto registrado com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato=" + whereIn : ""));
		return null;
	}
	
	/**
	 * A��o em ajax para buscar os itens de patrim�nio de um material.
	 *
	 * @param request
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2013
	 */
	public ModelAndView buscarPatrimonioitemAjax(WebRequestContext request, Contratomaterial contratomaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(contratomaterial != null && contratomaterial.getServico() != null){
			Integer cdpatrimonioitem = contratomaterial.getPatrimonioitem() != null ? contratomaterial.getPatrimonioitem().getCdpatrimonioitem() : null;
			List<Patrimonioitem> listaPatrimonioitem = patrimonioitemService.findByMaterial(contratomaterial.getServico(), cdpatrimonioitem, Patrimonioitemsituacao.DISPONIVEL);
			jsonModelAndView.addObject("listaPatrimonioitem", listaPatrimonioitem);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * A��o em ajax para buscar a quantidade dispon�vel do patrimonio ou produto.
	 *
	 * @param request
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public ModelAndView buscarQtdeDisponivelAjax(WebRequestContext request, Contratomaterial contratomaterial){
		Double qtdeDisponivel = 0d;
		if(contratomaterial.getLocalarmazenagem() != null){
			if(contratomaterial.getServico() != null){
				qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(contratomaterial.getServico(), Materialclasse.PRODUTO, contratomaterial.getLocalarmazenagem());
			} else if(contratomaterial.getPatrimonioitem() != null){
				qtdeDisponivel = movpatrimonioService.qtdeDisponivelPatrimonio(contratomaterial.getServico(), contratomaterial.getPatrimonioitem(), contratomaterial.getLocalarmazenagem());
			}
		}
		return new JsonModelAndView().addObject("qtdeDisponivel", qtdeDisponivel);
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	private void validarDttermino(WebRequestContext request, Contrato bean) throws Exception {
		String paramDatatermino = parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_FECHAR_CONTRATO_RETROATIVO);
		if(paramDatatermino != null && "FALSE".equals(paramDatatermino.toUpperCase())){
			if(bean.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(bean.getDtfim(), bean.getDtproximovencimento()) && SinedDateUtils.beforeIgnoreHour(bean.getDtfim(),SinedDateUtils.currentDate())){
				throw new SinedException("N�o � poss�vel finalizar o contrato com a data de t�rmino menor que a data atual.");					
			}
		}
		
		if (bean.getDtfim() != null && bean.getContratotipo() != null){
			Contratotipo contratotipo = contratotipoService.load(bean.getContratotipo());
			if (Boolean.TRUE.equals(contratotipo.getLocacao())){
				if(bean.getCdcontrato() == null){
					throw new SinedException("N�o � permitido finalizar contrato com Material faltante ou sem Indeniza��o");
				} else {
					List<RealizarFechamentoRomaneioContratoItem> listaFinal = this.makeListaFechamentoContrato(bean, true);
					if (!listaFinal.isEmpty()){
						throw new SinedException("N�o � permitido finalizar contrato com Material faltante ou sem Indeniza��o");					
					}
				}
			}
		}
	}
	
	public ModelAndView ajaxGetContaGerCentroCustoBtEmpresa (WebRequestContext request, Empresa empresa){
		JsonModelAndView jSonModelAndView = new JsonModelAndView();
		empresa = empresaService.loadComContagerencialCentrocusto(empresa);
		
		jSonModelAndView.addObject("centrocusto", empresa.getCentrocusto());
		jSonModelAndView.addObject("contagerencial", empresa.getContagerencial());
		return jSonModelAndView;
	}
	
	/**
	 * 
	 * Valida se a empresa do contrato tem permiss�o para cadastrar materiais repetidos nos produtos e servi�os, se n�o possuir aborta o processo com 
	 * uma mensagem de erro
	 * 
	 * @param request
	 * @param contrato
	 * @since 05/07/2014
	 * @author Rafael Patr�cio
	 *  
	 */
	public void validarProdutosServicos(WebRequestContext request, Contrato contrato){
		
		Empresa empresa = empresaService.load(contrato.getEmpresa(), "empresa.cdpessoa, empresa.permitirInclusaoMaterialServico");
		
		if(!Boolean.TRUE.equals(empresa.getPermitirInclusaoMaterialServico())){
			if(contrato.getListaContratomaterial()!=null && !contrato.getListaContratomaterial().isEmpty()){
				
				List<Material> listaMaterial = new ArrayList<Material>();
				
				for(Contratomaterial contratomaterial : contrato.getListaContratomaterial()){
					if(contratomaterial.getPatrimonioitem() == null){
						if(contratomaterial.getMaterialpromocional() != null && !listaMaterial.contains(contratomaterial.getMaterialpromocional())){
							checaMaterialRepetido(listaMaterial, contratomaterial.getMaterialpromocional());
						}
						checaMaterialRepetido(listaMaterial, contratomaterial.getServico());
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * Valida se o material passado j� est� contido na lista de materiais passada, se sim aborta o processo com uma mensagem de erro
	 * 
	 * @param listaMaterial, material
	 * @param contrato
	 * @since 05/07/2014
	 * @author Rafael Patr�cio
	 *  
	 */
	private void checaMaterialRepetido(List<Material> listaMaterial, Material material) {
		
		if(material!=null && material.getCdmaterial()!=null && 
				(material.getPatrimonio() == null || !material.getPatrimonio())){
			if(!listaMaterial.isEmpty() && listaMaterial.contains(material)){
				throw new SinedException("N�o � poss�vel incluir o mesmo material/servi�o j� selecionado anteriormente. Este material/servi�o n�o � da classe Patrim�nio.");
			}else {
				listaMaterial.add(material);
			}
		}
	}
	
	/**
	 * Reabrir contrato ao cancelar a indeniza��o no fechamento.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/09/2014
	 */
	public ModelAndView reabrirContratoAtCancelamentoIndenizacao(WebRequestContext request, RomaneioGerenciamentoMaterial bean){
		String whereInContrato = bean.getWhereInContrato();
		if(whereInContrato != null && !whereInContrato.trim().equals("")){
			String[] ids = whereInContrato.split(",");
			for(String cdcontratoStr : ids){
				Contrato contrato = new Contrato(Integer.parseInt(cdcontratoStr));
				
				Contratohistorico contratohistorico = new Contratohistorico();
				contratohistorico.setContrato(contrato);
				contratohistorico.setDthistorico(SinedDateUtils.currentTimestamp());
				contratohistorico.setAcao(Contratoacao.ESTORNADO);
				contratohistorico.setUsuario(SinedUtil.getUsuarioLogado());
				contratohistorico.setObservacao("Reaberto devido ao cancelamento do resgitro da indeniza��o.");
		
				contratohistoricoService.saveOrUpdate(contratohistorico);
				contratoService.updateDtfimContrato(contrato);
			}
		}
		return new JsonModelAndView();
	}
	
	public ModelAndView cancelarRomaneioConfirmarAutomatico(WebRequestContext request){
		String whereInContrato = request.getParameter("whereInContrato");
		romaneioService.cancelar(request, Boolean.TRUE);
    	Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
    	
		if(entrada)
    		View.getCurrent().println("<script>if(parent.location.href.indexOf('cdcontrato') == -1){parent.location = parent.location+'?ACAO=consultar&cdcontrato="+whereInContrato+"';} else{parent.location = parent.location;} parent.$.akModalRemove(true);</script>");
    	else
    		View.getCurrent().println("<script>parent.location = parent.location;parent.$.akModalRemove(true);</script>");
    	return null;
	}
	
	/**
	 * Pesquisa se o tipo de contrato tem o faturamento via webservice
	 * @param whereIn
	 * @param tipo
	 * @return
	 */
	public Boolean isFaturamentoWS(WebRequestContext request, String whereIn, String tipo){
		if(contratoService.existeFaturamentoWebService(whereIn)){
			if("popup".equals(tipo)){
				request.addError("Contrato com 'Faturamento via webservice'");
				SinedUtil.fechaPopUp(request);
				return true;
			} else if("redirecionamento".equals(tipo)){
				request.addError("Contrato com 'Faturamento via webservice'");
				Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
				SinedUtil.redirecionamento(request, "/faturamento/crud/Contrato" + (entrada ? "?ACAO=consultar&cdcontrato="+whereIn : ""));
				return true;
			} else if("modelandview".equals(tipo)){
				request.addError("Contrato com 'Faturamento via webservice'");
				return true;
			}
		}
		return false;
	}
	
	/**
	* M�todo ajax que verifica se existe algum contrato n�o finalizado e com todas as parcelas cobradas e outras verifica��es pertinentes ao faturamento
	*
	* @param request
	* @return
	* @since 24/03/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificarFaturamentoContrato(WebRequestContext request){
		boolean contratoComTodasParcelasCobradas = false;
		String whereIn = SinedUtil.getItensSelecionados(request);
		try {
			contratoComTodasParcelasCobradas = contratoService.existeContratoComTodasParcelasCobradas(whereIn);
		} catch (Exception e) {}
		
		List<Contrato> lista = contratoService.findContratoParaCalculoValorcontrato(whereIn);
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		for(Contrato contrato: lista){
			if(!listaClientes.contains(contrato.getCliente())){
				listaClientes.add(contrato.getCliente());
			}
		}
		ModelAndView retorno = contratoService.createModlAndViewForValidacaoClientesEmAtraso(lista);
		retorno.addObject("contratoComTodasParcelasCobradas", contratoComTodasParcelasCobradas);
		
		if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.ATRASADO, SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.EM_ESPERA, SituacaoContrato.CANCELADO, SituacaoContrato.ISENTO, SituacaoContrato.SUSPENSO)){
			retorno.addObject("msgErro", "Para gerar faturamento, o(s) contrato(s) deve(m) estar com situa��o 'A CONSOLIDAR' ou 'ATEN��O'.");
		}
		
    	return retorno;
	}
	
	/**
	 * M�todo ajax que retorna uma lista JSON com os prazos de pagamentos do cliente
	 *
	 * @param request
	 * @return
	 * @since 27/05/2016
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView getFormasPrazosPagamentoClienteJSON(WebRequestContext request){
		boolean addForma = "true".equalsIgnoreCase(request.getParameter("addForma"));
		boolean addPrazo = "true".equalsIgnoreCase(request.getParameter("addPrazo"));
		return clienteService.getFormasPrazosPagamentoClienteAlertaModelAndView(request, addForma, addPrazo);
	}
	
	/**
	 * Chamada Ajax para carregamento das Formas e Prazos de Pagameneto para determinado Cliente.
	 *
	 * @param request
	 * @param cliente
	 * @author Marcos Lisboa
	 */
	public ModelAndView ajaxLoadPrazoPagamento(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxLoadFormaAndPrazoPagamento(request, cliente);
	}
	
	public ModelAndView ajaxAtualizaListaContas(WebRequestContext request, Contrato contrato){
		List<Conta> contas = new ArrayList<Conta>();
		if(contrato.getEmpresa() != null && contrato.getContatipo() != null){
			contas = contaService.findByTipo(contrato.getContatipo(), contrato.getEmpresa());
		}
		
		return new JsonModelAndView().addObject("listaContas", contas);
	}
	
	public void ajaxComboGrupotributacao(WebRequestContext request, Contrato contrato){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		Cliente cliente = contrato.getCliente() != null && contrato.getCliente().getCdpessoa() != null ? 
				clienteService.loadForTributacao(contrato.getCliente()) : null;
				
		Empresa empresa = null;
		if (contrato.getEmpresa() != null && contrato.getEmpresa().getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(contrato.getEmpresa());
		} else {
			empresa = empresaService.loadPrincipalWithEndereco();
		}		
		
		List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacao(
				grupotributacaoService.createGrupotributacaoVOForTributacao(Operacao.SAIDA, 
																		empresa, 
																		null, 
																		null,
																		cliente,
																		null, 
																		null, 
																		null,
																		null, 
																		null,
																		null,
																		null,
																		null,
																		false,
																		null,
																		null,
																		null));
		
		JSON listaGrupotributacaoJSON = View.convertToJson(listaGrupotributacao);
		String jsonGrupotributacao = listaGrupotributacaoJSON.toString(0);
		view.println("var listaGrupotributacao = " + jsonGrupotributacao);
		
		Grupotributacao grupotributacao = contrato.getGrupotributacao();
		if(grupotributacao == null || grupotributacao.getCdgrupotributacao() == null){
			grupotributacao = null;
		}
		if(grupotributacao != null){
			view.println("var grupotributacaoSelecionado = 'br.com.linkcom.sined.geral.bean.Grupotributacao[cdgrupotributacao=" + grupotributacao.getCdgrupotributacao() + "]';");
		}
	}
	
	public ModelAndView ajaxValidaCancelamentoContratos(WebRequestContext request){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}
    	
    	List<Contrato> contratosCancelados = contratoService.findContratosCancelados(itensSelecionados);
    	String msgRetorno = "";
    	if(contratosCancelados != null && !contratosCancelados.isEmpty()){
    		if(!itensSelecionados.contains(",")){
    			msgRetorno = "O contrato selecionado j� encontra-se cancelado.";
    		}else{
    			msgRetorno = "Existem contratos selecionados que j� foram cancelados.";
    		}
    	}
    	return new JsonModelAndView().addObject("qtdeContratosJaCancelados", contratosCancelados != null? contratosCancelados.size(): 0)
    								.addObject("msgRetorno", msgRetorno);
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView abrirPopUpInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView ajaxDefaultsPorTipoDeContrato(WebRequestContext request, Contrato contrato){
		ModelAndView retorno = new JsonModelAndView();
		if(contrato.getContratotipo() != null && contrato.getContratotipo().getCdcontratotipo() != null){
			Contratotipo contratotipo = contratotipoService.load(contrato.getContratotipo(), "contratotipo.cdcontratotipo, contratotipo.acrescimoproximofaturamento, contratotipo.faturamentoemlote");
			retorno.addObject("acrescimoProximoFaturamento", Boolean.TRUE.equals(contratotipo.getAcrescimoproximofaturamento()));
			retorno.addObject("faturamentoEmLote", Boolean.TRUE.equals(contratotipo.getFaturamentoemlote()));
		}
		return retorno;
	}
	
	public ModelAndView abrirPopUpPatrimonioItem(WebRequestContext request){
		Integer cdmaterial = null;
		if(request.getParameter("cdmaterial") != null && !"".equals(request.getParameter("cdmaterial"))){
			cdmaterial = Integer.parseInt(request.getParameter("cdmaterial"));			
		}
		return contratoService.abrirPopUpPatrimonioItem(request, cdmaterial);
	}
	
	public ModelAndView abrirPopUpInfoFinanceiraCliente(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInfoFinanceiraCliente(request, cliente);
	}
	

	public ModelAndView ajaxValordebitoContasEmAbertoByCliente(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxValordebitoContasEmAbertoByCliente(request, cliente);
	}
	
	public ModelAndView ajaxValidaFaturamentoContratoPorMedicao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Contrato> lista = contratoService.findContratoParaCalculoValorcontrato(whereIn);
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		for(Contrato contrato: lista){
			if(!listaClientes.contains(contrato.getCliente())){
				listaClientes.add(contrato.getCliente());
			}
		}
		
    	return contratoService.createModlAndViewForValidacaoClientesEmAtraso(lista);
	}
}