package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoRPS;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoRPSService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoRPSFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/NotaFiscalServicoRPS", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"notaFiscalServico.empresa", "numero", "notaFiscalServico.cliente", "dataemissao"})
public class NotaFiscalServicoRPSCrud extends CrudControllerSined<NotaFiscalServicoRPSFiltro, NotaFiscalServicoRPS, NotaFiscalServicoRPS> {
	
	private NotaFiscalServicoRPSService notaFiscalServicoRPSService;
	
	public void setNotaFiscalServicoRPSService(
			NotaFiscalServicoRPSService notaFiscalServicoRPSService) {
		this.notaFiscalServicoRPSService = notaFiscalServicoRPSService;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected void salvar(WebRequestContext request, NotaFiscalServicoRPS bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, NotaFiscalServicoRPS bean) throws Exception {
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
		} else super.excluir(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, NotaFiscalServicoRPS form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	public ModelAndView gerarArquivoNFSe(WebRequestContext request, NotaFiscalServicoRPSFiltro filtro){
		try {
			notaFiscalServicoRPSService.gerarArquivoNFSe(filtro);
			request.addMessage("Arquivo de NFS-e gerado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao gerar o arquivo de NFS-e: " + e.getMessage());
		}
		return sendRedirectToAction("listagem");
	}
	
}
