package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Faturamentocontrato;
import br.com.linkcom.sined.geral.service.FaturamentocontratoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.FaturamentocontratoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/Faturamentocontrato", authorizationModule=CrudAuthorizationModule.class)
public class FaturamentocontratoCrud extends CrudControllerSined<FaturamentocontratoFiltro, Faturamentocontrato, Faturamentocontrato> {
	
	private FaturamentocontratoService faturamentocontratoService;
	
	public void setFaturamentocontratoService(
			FaturamentocontratoService faturamentocontratoService) {
		this.faturamentocontratoService = faturamentocontratoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Faturamentocontrato form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Faturamentocontrato bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Faturamentocontrato bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected Faturamentocontrato criar(WebRequestContext request, Faturamentocontrato form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void listagem(WebRequestContext request, FaturamentocontratoFiltro filtro) throws Exception {
		request.setAttribute("dtinicioPadrao", SinedDateUtils.toString(SinedDateUtils.firstDateOfMonth()));
		request.setAttribute("dtfimPadrao", SinedDateUtils.toString(SinedDateUtils.lastDateOfMonth()));
	}
	
	@Override
	protected void setListagemInfo(WebRequestContext request, FaturamentocontratoFiltro filtro) throws Exception {
		if (!listagemVaziaPrimeiraVez() || filtro.isNotFirstTime()) {
			if(request.getParameter("resetCurrentPage") != null) {
				filtro.setCurrentPage(0);
			}
			
			ListagemResult<Faturamentocontrato> listagemResult = getLista(request, filtro);
			List<Faturamentocontrato> lista = listagemResult.list();
			
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdfaturamentocontrato", ",");
			lista = faturamentocontratoService.findFaturamentosForListagem(whereIn);
			faturamentocontratoService.preencheListaFaturamentos(lista);
			
			request.setAttribute("lista", lista);
			request.setAttribute("currentPage", filtro.getCurrentPage());
			request.setAttribute("numberOfPages", filtro.getNumberOfPages());
			request.setAttribute("filtro", filtro);			
		} else {
			request.setAttribute("lista", new ArrayList<Faturamentocontrato>());
			request.setAttribute("currentPage", 0);
			request.setAttribute("numberOfPages", 0);
			request.setAttribute("filtro", filtro);
		}
	}

}
