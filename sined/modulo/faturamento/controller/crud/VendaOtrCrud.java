package br.com.linkcom.sined.modulo.faturamento.controller.crud;


import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresarepresentacao;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialFaixaMarkup;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitemnotaentrada;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendatipo;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Vendaorcamentoformapagamento;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.auxiliar.PedidoVendaParametersBean;
import br.com.linkcom.sined.geral.bean.auxiliar.PneuItemVendaInterface;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Criteriocomissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;
import br.com.linkcom.sined.geral.service.CampoextrapedidovendatipoService;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ClientevendedorService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentocomissaovendaService;
import br.com.linkcom.sined.geral.service.DocumentocomissaovendarepresentacaoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmporiumpdvService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EmpresarepresentacaoService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EntregamaterialService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.ExpedicaoitemService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.GarantiareformaService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialFaixaMarkupService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialdevolucaoService;
import br.com.linkcom.sined.geral.service.MaterialformulavalorvendaService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoService;
import br.com.linkcom.sined.geral.service.MaterialvendaService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaVendaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemnotaentradaService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendamaterialService;
import br.com.linkcom.sined.geral.service.PedidovendapagamentoService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProducaoagendaService;
import br.com.linkcom.sined.geral.service.ProducaoagendahistoricoService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.RestricaoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.geral.service.VendamaterialmestreService;
import br.com.linkcom.sined.geral.service.VendaorcamentoformapagamentoService;
import br.com.linkcom.sined.geral.service.VendapagamentoService;
import br.com.linkcom.sined.geral.service.VendapagamentorepresentacaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ClienteVO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteAnexoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteDestinatarioBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioECFBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Enviocomprovanteemail;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EtiquetaPneuFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EtiquetaPneuItem;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FornecedoragenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaMaterialItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Impostovenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.KitflexivelBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Sugestaovenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.TrocavendedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendaDevolucaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendamaterialDevolucaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.SituacaoNotaEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoRepresentacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.EmitirFichaColetaReport;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.MaterialHistoricoPrecoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.filter.MaterialHistoricoPrecoFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumExportacaoMovimentosUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(path="/faturamento/crud/VendaOtr" , authorizationModule=CrudAuthorizationModule.class)
public class VendaOtrCrud extends CrudControllerSined<VendaFiltro, Venda, Venda> {

	private EmpresaService empresaService;
	private VendamaterialService vendamaterialService;
	private VendaService vendaService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private DocumentoService documentoService;
	private VendahistoricoService vendahistoricoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private VendapagamentoService vendapagamentoService;
	private MaterialService materialService;
	private PrazopagamentoService prazopagamentoService;
	private EnderecoService enderecoService;
	private ColaboradorService colaboradorService;
	private PedidovendaService pedidovendaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private UnidademedidaService unidademedidaService;
	private FornecedorService fornecedorService;
	private ParametrogeralService parametrogeralService;
	private RestricaoService restricaoService;
	private ProdutoService produtoService;
	private MaterialdevolucaoService materialdevolucaoService;
	private DocumentocomissaovendaService documentocomissaovendaService;
	private LocalarmazenagemService localarmazenagemService;
	private DocumentotipoService documentotipoService;
	private ContaService contaService;
	private ContareceberService contareceberService;
	private MaterialtabelaprecoService materialtabelaprecoService;
	private MaterialvendaService materialvendaService;
	private ClienteService clienteService;
	private UsuarioService usuarioService;
	private LoteestoqueService loteestoqueService;
	private ExpedicaoitemService expedicaoitemService;
	private EmporiumpdvService emporiumpdvService;
	private MaterialproducaoService materialproducaoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private ContatoService contatoService;
	private PatrimonioitemService patrimonioitemService;
	private VendamaterialmestreService vendamaterialmestreService;
//	private ArquivoService arquivoService;
	private RegiaoService regiaoService;
	private PedidovendatipoService pedidovendatipoService; 
	private CampoextrapedidovendatipoService campoextrapedidovendatipoService;
//	private MaterialunidademedidaService materialunidademedidaService;
	private EntregamaterialService entregamaterialService;
	private MaterialformulavalorvendaService materialformulavalorvendaService;
	private ClientevendedorService clientevendedorService;
	private ValecompraService valecompraService;
	private EmpresarepresentacaoService empresarepresentacaoService;
	private EnvioemailService envioemailService;
	private VendapagamentorepresentacaoService vendapagamentorepresentacaoService;
//	private MaterialkitflexivelService materialkitflexivelService;
	private NotafiscalprodutoitemnotaentradaService notafiscalprodutoitemnotaentradaService;
//	private MaterialgrupocomissaovendaService materialgrupocomissaovendaService;
	private DocumentocomissaovendarepresentacaoService documentocomissaovendarepresentacaoService;
	private CategoriaService categoriaService;
	private NotaVendaService notaVendaService;
	private GarantiareformaService garantiareformaService;
	private OportunidadeService oportunidadeService;
	private PedidovendapagamentoService pedidovendapagamentoService;
	private EmitirFichaColetaReport emitirFichaColetaReport;
	private MaterialFaixaMarkupService materialFaixaMarkupService;
	private InventarioService inventarioService;
	private ComissionamentoService comissionamentoService;
	private VendaorcamentoformapagamentoService vendaorcamentoformapagamentoService;
	
	public void setEmitirFichaColetaReport(EmitirFichaColetaReport emitirFichaColetaReport) {this.emitirFichaColetaReport = emitirFichaColetaReport;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setNotafiscalprodutoitemnotaentradaService(NotafiscalprodutoitemnotaentradaService notafiscalprodutoitemnotaentradaService) {this.notafiscalprodutoitemnotaentradaService = notafiscalprodutoitemnotaentradaService;}
	public void setEnvioemailService(EnvioemailService envioemailService) {this.envioemailService = envioemailService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setClientevendedorService(ClientevendedorService clientevendedorService) {this.clientevendedorService = clientevendedorService;}
	public void setRegiaoService(RegiaoService regiaoService) {this.regiaoService = regiaoService;}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {this.patrimonioitemService = patrimonioitemService;}
	public void setProducaoagendahistoricoService(ProducaoagendahistoricoService producaoagendahistoricoService) {this.producaoagendahistoricoService = producaoagendahistoricoService;}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {this.producaoagendaService = producaoagendaService;}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {this.materialproducaoService = materialproducaoService;}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {this.emporiumpdvService = emporiumpdvService;}
	public void setExpedicaoitemService(ExpedicaoitemService expedicaoitemService) {this.expedicaoitemService = expedicaoitemService;}
	public void setMaterialvendaService(MaterialvendaService materialvendaService) {this.materialvendaService = materialvendaService;}
	public void setMaterialtabelaprecoService(MaterialtabelaprecoService materialtabelaprecoService) {this.materialtabelaprecoService = materialtabelaprecoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {this.vendahistoricoService = vendahistoricoService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {this.vendapagamentoService = vendapagamentoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {this.unidademedidaconversaoService = unidademedidaconversaoService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setRestricaoService(RestricaoService restricaoService) {this.restricaoService = restricaoService;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setMaterialdevolucaoService(MaterialdevolucaoService materialdevolucaoService) {this.materialdevolucaoService = materialdevolucaoService;}
	public void setDocumentocomissaovendaService(DocumentocomissaovendaService documentocomissaovendaService) {this.documentocomissaovendaService = documentocomissaovendaService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setVendamaterialmestreService(VendamaterialmestreService vendamaterialmestreService) {this.vendamaterialmestreService = vendamaterialmestreService;}
//	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}	
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setCampoextrapedidovendatipoService(CampoextrapedidovendatipoService campoextrapedidovendatipoService) {this.campoextrapedidovendatipoService = campoextrapedidovendatipoService;}
//	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {this.materialunidademedidaService = materialunidademedidaService;}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {this.entregamaterialService = entregamaterialService;}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {this.materialformulavalorvendaService = materialformulavalorvendaService;}
	public void setEmpresarepresentacaoService(EmpresarepresentacaoService empresarepresentacaoService) {this.empresarepresentacaoService = empresarepresentacaoService;}
	public void setVendapagamentorepresentacaoService(VendapagamentorepresentacaoService vendapagamentorepresentacaoService) {this.vendapagamentorepresentacaoService = vendapagamentorepresentacaoService;}
//	public void setMaterialkitflexivelService(MaterialkitflexivelService materialkitflexivelService) {this.materialkitflexivelService = materialkitflexivelService;}
//	public void setMaterialgrupocomissaovendaService(MaterialgrupocomissaovendaService materialgrupocomissaovendaService) {this.materialgrupocomissaovendaService = materialgrupocomissaovendaService;}
	public void setDocumentocomissaovendarepresentacaoService(DocumentocomissaovendarepresentacaoService documentocomissaovendarepresentacaoService) {this.documentocomissaovendarepresentacaoService = documentocomissaovendarepresentacaoService;}
	public void setNotaVendaService(NotaVendaService notaVendaService) {this.notaVendaService = notaVendaService;}
	public void setGarantiareformaService(GarantiareformaService garantiareformaService) {this.garantiareformaService = garantiareformaService;}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setPedidovendapagamentoService(PedidovendapagamentoService pedidovendapagamentoService) {this.pedidovendapagamentoService = pedidovendapagamentoService;}
	public void setMaterialFaixaMarkupService(MaterialFaixaMarkupService materialFaixaMarkupService) {this.materialFaixaMarkupService = materialFaixaMarkupService;}
	public void setInventarioService(InventarioService inventarioService) {this.inventarioService = inventarioService;}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {this.comissionamentoService = comissionamentoService;}
	public void setVendaorcamentoformapagamentoService(VendaorcamentoformapagamentoService vendaorcamentoformapagamentoService) {this.vendaorcamentoformapagamentoService = vendaorcamentoformapagamentoService;}

	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	@Override
	protected Venda criar(WebRequestContext request, Venda form) throws Exception {
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		if(parameters.getCopiar() && form.getCdvenda() != null){
			form = vendaService.criarCopia(form);
			request.setAttribute("copiarvenda", true);
			return form;
		}
		
		request.setAttribute("boquearIdentificador", Boolean.TRUE.equals(parameters.getBloquearIdentificador()));
		
		if(parameters.getCdPedidoVenda() != null){
			Pedidovenda pedidovenda = pedidovendaService.findPedidovendabyTipo(parameters.getCdPedidoVenda().toString());
			request.setAttribute("pedidovendatipo", pedidovenda.getPedidovendatipo());
			
			request.setAttribute("CONFIRMARPERCENTUALPEDIDOVENDA", parametrogeralService.getBoolean(Parametrogeral.CONFIRMARPERCENTUALPEDIDOVENDA));
		}
		
		
		return super.criar(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, VendaFiltro filtro) throws Exception {		
		String vendasaldoproduto = parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTO);
		if(vendasaldoproduto.toUpperCase().equals("TRUE")){
			request.setAttribute("VENDASALDOPRODUTO", Boolean.TRUE);
		}else {
			request.setAttribute("VENDASALDOPRODUTO", Boolean.FALSE);
		}
		
		if(filtro.getPedidovendatipo() != null && filtro.getPedidovendatipo().getCdpedidovendatipo() != null){
			request.setAttribute("listaCampoExtra", campoextrapedidovendatipoService.findByTipo(filtro.getPedidovendatipo()));
		}else{
			request.setAttribute("listaCampoExtra", Collections.emptyList());
		}
		
		List<Vendasituacao> listaSituacao = new ArrayList<Vendasituacao>();
		listaSituacao.add(Vendasituacao.EMPRODUCAO);
		listaSituacao.add(Vendasituacao.REALIZADA);
		listaSituacao.add(Vendasituacao.FATURADA);
		listaSituacao.add(Vendasituacao.CANCELADA);		
		listaSituacao.add(Vendasituacao.AGUARDANDO_APROVACAO);
		
		request.setAttribute("listaSituacao", listaSituacao);
		
		if (filtro.getListaVendasituacao() == null){
			filtro.setListaVendasituacao(new ArrayList<Vendasituacao>());
			filtro.getListaVendasituacao().add(Vendasituacao.EMPRODUCAO);
			filtro.getListaVendasituacao().add(Vendasituacao.REALIZADA);
			filtro.getListaVendasituacao().add(Vendasituacao.FATURADA);
			filtro.getListaVendasituacao().add(Vendasituacao.AGUARDANDO_APROVACAO);
		}
		request.setAttribute("listaCategoriaCliente", categoriaService.findByTipo(true, false));
		request.setAttribute("listaDocumentotipo", documentotipoService.getListaDocumentoTipoUsuarioForVenda(null));
		super.listagem(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, VendaFiltro filtro) throws CrudException {
		filtro.setNaoexibirresultado(null);
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadForFiltro(filtro.getRegiao()));
		}
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			Material material = materialService.load(filtro.getMaterial(), "material.cdmaterial, material.vendapromocional");
			if(material != null && material.getVendapromocional() != null && material.getVendapromocional()){
				filtro.getMaterial().setVendapromocional(material.getVendapromocional());
			}
		}
		//realizando valida��o para evitar excees
		if(filtro.getWhereIncdvenda() != null){
			String [] listcdvenda = filtro.getWhereIncdvenda().split(",");
			String whereIn = "";
			Boolean contemErro = false;
			if(listcdvenda.length > 0){
				for (String cdvenda : listcdvenda) {
					try{
						if(StringUtils.isNumeric(cdvenda.trim()) && Integer.parseInt(cdvenda.trim()) < Integer.MAX_VALUE){
							whereIn += cdvenda + ",";
						}else{
							contemErro = true;
							break;
						}
					}catch(NumberFormatException n){
						contemErro=true;
						break;
					}
				}
			}
			if(!contemErro){
				filtro.setWhereIncdvenda(whereIn.substring(0, whereIn.length() - 1));
			}else{
				filtro.setWhereIncdvenda(null);
				request.addError("Favor preencher com n�mero(s), podendo conter v�rgula(s).");
			}
		}
		
//		request.setAttribute("TIPO_ATUALIZACAO_PRECO_VENDA", parametrogeralService.getValorPorNome("TIPO_ATUALIZACAO_PRECO_VENDA"));
		
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,
			Venda form) {
		if (request.getBindException().hasErrors()) request.setAttribute("ACAO", "salvar");
		return new ModelAndView("crud/vendaOtrEntrada");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request,
			VendaFiltro filtro) {
		return new ModelAndView("crud/vendaOtrListagem");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Venda form)throws Exception {
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		form = vendaService.loadBeanForEntrada(request, form, parameters);
		if(SinedUtil.isListNotEmpty(form.getListavendamaterial())){
			form.getListaPneu().addAll(vendaService.getListaPneuForOtr(form.getListaPneuItemInterface()));
			form.setListaItemAvulso(vendaService.getListaServicoAvulso(form.getListaPneuItemInterface()));
		}

		request.setAttribute("isPedidoVendaOtr", true);
		request.setAttribute("listaItemAvulso", form.getListaItemAvulso());
		request.setAttribute("listaPneu", form.getListaPneu());
	}
	
	/**
	 * Action em ajax para buscar as informa��es das condi��es de pagamento escolhidas anteriormente no or�amento.
	 *
	 * @param request
	 * @param venda
	 * @since 02/07/2012
	 * @author Rodrigo Freitas
	 */
	public void buscarFormapagamentoOrcamento(WebRequestContext request, Venda venda){
		if(venda != null && venda.getVendaformapagamento() != null && venda.getVendaformapagamento().getCdvendaorcamentoformapagamento() != null){
			Vendaorcamentoformapagamento vendaformapagamento = vendaorcamentoformapagamentoService.load(venda.getVendaformapagamento());
			if(vendaformapagamento != null){
				View.getCurrent().println("var prazopagamentoOrcamento = 'br.com.linkcom.sined.geral.bean.Prazopagamento[cdprazopagamento=" + vendaformapagamento.getPrazopagamento().getCdprazopagamento() + "]';");
				View.getCurrent().println("var qtdeParcelasOrcamento = '" + (vendaformapagamento.getQtdeparcelas() != null ? vendaformapagamento.getQtdeparcelas() : "") + "';");
			} else {
				View.getCurrent().println("var prazopagamentoOrcamento = '<null>';");
				View.getCurrent().println("var qtdeParcelasOrcamento = '';");
			}
		} else {
			View.getCurrent().println("var prazopagamentoOrcamento = '<null>';");
			View.getCurrent().println("var qtdeParcelasOrcamento = '';");
		}
	}
	
	@Override
	protected void validateBean(Venda bean, BindException errors) {
		vendaService.validateBean(bean, errors);
		/*boolean erroEdicaoVendaAA = false;
		if(bean.getCdvenda() != null && bean.getVendasituacao() != null && Vendasituacao.AGUARDANDO_APROVACAO.equals(bean.getVendasituacao())){
			Venda venda = vendaService.loadWithVendasituacao(bean);
			if(venda != null && venda.getVendasituacao() != null && Vendasituacao.REALIZADA.equals(venda.getVendasituacao())){
				errors.reject("001", "N�o � poss�vel salvar a venda. A venda j� foi aprovada.");
				erroEdicaoVendaAA = true;
			}
		}
		
		if(!erroEdicaoVendaAA){
			if(bean.getListavendamaterial() == null || bean.getListavendamaterial().isEmpty()){
				errors.reject("Nenhum produto encontrado na venda.");			
			}
			if (!validaPagamentoAntecipado(bean)){
				errors.reject("001","O valor da parcela deve ser menor ou igual ao saldo do pagamento adiantado escolhido.");
			}
			if ("TRUE".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.IDENTIFICADOR_UNICO_PEDIDO_VENDA_ORCAMENTO)) && bean.getEmpresa() != null && StringUtils.isNotBlank(bean.getIdentificador()) && vendaService.isIdentificadorCadastrado(bean)) {
				errors.reject("001","Aten��o! Identificador j� cadastrado no sistema.");
			}
		
			if(bean.getListavendamaterial() == null || bean.getListavendamaterial().isEmpty()){
				errors.reject("Nenhum produto encontrado na venda.");			
			}
			if (!validaPagamentoAntecipado(bean)){
				errors.reject("001","O valor da parcela deve ser menor ou igual ao saldo do pagamento antecipado escolhido.");
			}
			if ("TRUE".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.IDENTIFICADOR_UNICO_PEDIDO_VENDA_ORCAMENTO)) && bean.getEmpresa() != null && StringUtils.isNotBlank(bean.getIdentificador()) && vendaService.isIdentificadorCadastrado(bean)) {
				errors.reject("001","Aten��o! Identificador j� cadastrado no sistema.");
			}
			
			vendaService.validaOrdenacao(errors, bean.getListavendamaterial());
			
			boolean verificarparcelas = true;
			if((bean.getBonificacao() != null && bean.getBonificacao()) || (bean.getComodato() != null && bean.getComodato()) ){
				verificarparcelas = false;
			}else {
				if(bean.getListavendapagamento() != null && !bean.getListavendapagamento().isEmpty()){
					for (Vendapagamento pvp : bean.getListavendapagamento()){
						if(pvp.getDocumento() != null || pvp.getDocumentoantecipacao() != null){
							verificarparcelas = false;
						}
					}
				}
			}
			if(verificarparcelas){
				validaListaVendamaterialComListaVendaPagamento(null, bean, errors);
			}
			
			vendaService.validaObrigatoriedadePneu(bean, null, errors);
		}*/
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Venda form)
			throws CrudException {
		if(!vendaService.validaVendedorprincipal(form, request)) {
			return continueOnAction("entrada", form);
		}
		
		return super.doSalvar(request, form);
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Venda form)	throws CrudException {
		vendaService.validateEditar(request, form);
		List<String> errors = SinedUtil.getMessageError(request.getMessages());
		if("editar".equals(request.getParameter("ACAO")) && SinedUtil.isListNotEmpty(errors)){
			SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOtr?ACAO=consultar&cdvenda=" + form.getCdvenda());
			return null;
		}
		
		return super.doEditar(request, form);
	}
	
	@Override
	protected ListagemResult<Venda> getLista(WebRequestContext request, VendaFiltro filtro) {
	
		ListagemResult<Venda> lista = super.getLista(request, filtro);
		List<Venda> list = lista.list();
		boolean exibiripivenda = false;
		if(filtro.getEmpresa() == null){
			exibiripivenda = empresaService.existeEmpresaExibirIpi();
		}
		
		boolean baixaExpedicao = pedidovendatipoService.gerarExpedicaoVenda(null, false);
		if(list != null && list.size() > 0){
			String whereInVenda = CollectionsUtil.listAndConcatenate(list, "cdvenda", ",");
			List<Vendamaterial> listaVendamaterial = vendamaterialService.findByVenda(whereInVenda);
			
			for (Venda venda : list) {
				venda.setListavendamaterial(vendamaterialService.getVendamaterialByVenda(listaVendamaterial, venda));
				venda.setListaVendamaterialmestre(vendamaterialmestreService.findByVenda(venda));
				venda.setProducaoagendasituacao(producaoagendaService.getProducaoagendasituacaoByVenda(venda));
				if(!exibiripivenda && venda.getEmpresa() != null && venda.getEmpresa().getExibiripivenda() != null &&
						venda.getEmpresa().getExibiripivenda()){
					exibiripivenda = true;
				}
				if(parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE) &&
						(Vendasituacao.AGUARDANDO_APROVACAO.equals(venda.getVendasituacao()) || vendaService.existeVinculoPedidovendamaterial(venda)) &&
						venda.getLimitecreditoexcedido() != null && venda.getLimitecreditoexcedido()){
					venda.setValorSupeiorLimiteCredito(Boolean.TRUE);
				}
				if(!baixaExpedicao && pedidovendatipoService.gerarExpedicaoVenda(venda.getPedidovendatipo(), false)){
					baixaExpedicao = true;
				}
			}
		}
		
		// QUERY PARA O RESUMO DE TIPO DE DOCUMENTO
		if(filtro.getExibirResumoFormapagamento() != null && filtro.getExibirResumoFormapagamento()){
			filtro.setListaResumoDocumentotipoVenda(vendaService.createResumoListagemDocumentotipo(filtro));
		}
		if(filtro.getExibirResumoPedidovendatipo() != null && filtro.getExibirResumoPedidovendatipo()){
			filtro.setListaResumoPedidovendatipo(vendaService.createResumoListagemPedidovendatipo(filtro));
		}
		
		if(filtro.getExibirTotais() != null && filtro.getExibirTotais()){
			if(filtro.getNaoexibirresultado() == null || !filtro.getNaoexibirresultado()){
				VendaFiltro totais = vendaService.calculaTotaisListagem(filtro);
				
				filtro.setTotalgeral(totais.getTotalgeral());
				filtro.setTotalgeralipi(totais.getTotalgeralipi());
				filtro.setQtdegeral(totais.getQtdegeral());
				filtro.setPesototal(totais.getPesototal() != null ? SinedUtil.round(totais.getPesototal(), 2) : 0.0);
				filtro.setPesobrutototal(totais.getPesobrutototal() != null ? SinedUtil.round(totais.getPesobrutototal(), 2) : 0.0);
				filtro.setValortotaldevolvido(materialdevolucaoService.getValortotalDevolvidaListagemVenda(totais.getWhereIn()));
				
				if("TRUE".equals(parametrogeralService.buscaValorPorNome(Parametrogeral.VENDASALDOPRODUTO))){
					filtro.setSaldoprodutos(totais.getSaldoprodutos());
					filtro.setSaldoprodutosbonificacao(totais.getSaldoprodutosbonificacao());
				}
			} else {
				filtro.setTotalgeral(new Money());
				filtro.setTotalgeralipi(new Money());
				filtro.setQtdegeral(0.0);
				filtro.setSaldoprodutos(new Money());
			}
		}
		
		filtro.setNaoexibirresultado(null);		
		
		vendaService.addInfoVendaDevolvida(list);
		
		vendaService.addInfoBoletoEmitido(list);
		request.setAttribute("EXIBIR_IPI_NA_VENDA", exibiripivenda);
		
		request.setAttribute("baixaExpedicao", baixaExpedicao);
		request.setAttribute("VENDA_COM_EXPEDICAO", parametrogeralService.getBoolean(Parametrogeral.VENDA_COM_EXPEDICAO));
		return lista;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}	
	
	/**
	 * Metodo para cancelar uma venda. 
	 * @param request
	 * @param venda
	 * @return
	 * @author Ramon Brazil
	 */
	public ModelAndView cancelar(WebRequestContext request, Venda venda){
		if(!vendaService.validateCancelamento(request, venda)){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		ModelAndView retorno = vendaService.cancelar(request, venda);
		Map mapa = retorno.getModel();
		if (mapa.get("sucessos") != null && (Integer)mapa.get("sucessos") > 0)
			request.addMessage(mapa.get("sucessos") + " venda(s) cancelada(s) com sucesso.");
		if (mapa.get("errosnota") != null && (Integer)mapa.get("errosnota") > 0)
			request.addMessage(mapa.get("errosnota") + " venda(s) com nota fiscal n�o cancelada ou denegada.");
		
		String whereInContasBaixadas = mapa.get("whereInContasBaixadas") != null? mapa.get("whereInContasBaixadas").toString(): "";
		if(Boolean.TRUE.equals(venda.getRegistrarDevolucao()) && whereInContasBaixadas.length() > 0){
			whereInContasBaixadas = whereInContasBaixadas.substring(0, whereInContasBaixadas.length()-1);
			SinedUtil.redirecionamento(request, "/financeiro/crud/Contapagar?ACAO=validaContaReceber&selectedItens=" + whereInContasBaixadas.toString());
			return null;
		}
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOtr");
		return null;
	}
	

	public ModelAndView aprovarVenda(WebRequestContext request, Venda venda){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		String[] vendas = itens.split(",");
		if(vendas.length > 1){
			throw new SinedException("Somente um item pode ser selecionado para esta a��o.");
		}
		if(vendaService.validateAprovar(request, Integer.parseInt(vendas[0]))){
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr?ACAO=editar&aprovarVenda=true&cdvenda=" + vendas[0]);
		}else{
			return getControllerModelAndView(request);
		}
	}
	
	private ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if (request.getParameter("cdvenda") != null) {
			action += "&cdvenda=" + request.getParameter("cdvenda");
		}
		return new ModelAndView("redirect:"+action);
	}
	
	/**
	 * Action em ajax para buscar o JSON com a lista de cliente.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/11/2012
	 */
	public ModelAndView ajaxBuscaClienteAgrupamentoMatriz(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		String[] ids = whereIn.split(",");
		
		Venda venda = new Venda(Integer.parseInt(ids[0]));
		venda = vendaService.loadWithCliente(venda);
		
		List<ClienteVO> listaCliente = venda.getCliente().getCnpj() != null ? clienteService.findByMatrizCNPJ(venda.getCliente().getCnpj()) : new ArrayList<ClienteVO>();  
		return jsonModelAndView.addObject("listaCliente", listaCliente);
	}
	
	/**
	 * Action que abre a confirma��o do agrupamento da venda por cliente.
	 *
	 * @param request
	 * @return
	 * @author Toms Rabelo
	 */
	public ModelAndView abrirConfirmacaoAgrupamento(WebRequestContext request){
		String isNfe = request.getParameter("isNfe");
		String whereIn = request.getParameter("selectedItens");
		if(vendaService.haveVendaSituacao(whereIn, false, Vendasituacao.CANCELADA, Vendasituacao.FATURADA)){
			request.addError("Para gerar nota, a(s) venda(s) deve(m) estar com situa��o 'PRODU��O' ou 'REALIZADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Venda> listaVenda = vendaService.findForCobranca(whereIn);
		for (Venda venda : listaVenda) {
			java.sql.Date data = new java.sql.Date(System.currentTimeMillis());
			if (venda.getPedidovendatipo() != null && BaixaestoqueEnum.APOS_EMISSAONOTA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()) &&
					!SituacaoNotaEnum.AUTORIZADA.equals(venda.getPedidovendatipo().getSituacaoNota()) && 
					!movimentacaoestoqueService.existeMovimentacaoestoque(venda) &&
				    inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(data, venda.getEmpresa(), venda.getLocalarmazenagem(), venda.getProjeto())) {
				request.addError("N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. " +
						"Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		if(vendaService.existeVendaRepresentacao(whereIn)){ 
			request.addError("N�o � poss�vel utilizar as aes de Gerar nota, Gerar boleto e Negociar de venda de representao.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(expedicaoitemService.haveFaturada(whereIn)){
			request.addError("Quando � gerado o faturamento de expedi��o n�o � poss�vel gerar o faturamento normal.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		String url = "/faturamento/crud/Notafiscalproduto";
		if(StringUtils.isNotBlank(isNfe) && isNfe.equals("false")){
			url = "/faturamento/crud/Notafiscalconsumidor";
		}
		request.setAttribute("isNfe", isNfe);
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		request.setAttribute("entrada", entrada);
		request.setAttribute("selectedItens", whereIn);
		
		boolean existVendaWithProdutoAndServico = vendaService.existVendaWithServico(whereIn);
		String whereInCdentregamaterial = request.getParameter("whereInCdentregamaterial");
		String whereInQtdeEntrada = request.getParameter("whereInQtdeEntrada");
		
		if(whereIn.split(",").length > 1){
			request.setAttribute("mesmaMatriz", vendaService.mesmaMatrizVendas(whereIn));
			request.setAttribute("clienteIgualEFreteTerceiroDiferente", vendaService.clienteIgualEFreteTerceiroDiferente(whereIn));
			request.setAttribute("existVendaWithProdutoAndServico", existVendaWithProdutoAndServico);
			request.setAttribute("whereInCdentregamaterial", whereInCdentregamaterial);
			request.setAttribute("whereInQtdeEntrada", whereInQtdeEntrada);
			return new ModelAndView("direct:/crud/popup/confirmacaoAgrupamentoVenda");
		} else {
			if(existVendaWithProdutoAndServico){
				request.setAttribute("whereInCdentregamaterial", whereInCdentregamaterial);
				request.setAttribute("whereInQtdeEntrada", whereInQtdeEntrada);
				return this.abrirPopupGerarnotaprodutoservico(request, whereIn);
			} else {
				if(parametrogeralService.getBoolean(Parametrogeral.GERARNOTA_NF_DIRETO)){
					request.getServletResponse().setContentType("text/html");
					View.getCurrent().println(
							"<script>parent.location = '" + request.getServletRequest().getContextPath() + "/faturamento/crud/VendaOtr?ACAO=gerarFaturamento&selectedItens=" + whereIn + "&agrupamento=false&isNfe=" + isNfe + "';</script>");
				} else {
					request.getServletResponse().setContentType("text/html");
					View.getCurrent().println(
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
							"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
							"<script>parent.location = '" + request.getServletRequest().getContextPath() + url + "?ACAO=" + CRIAR + "&cdvenda=" + whereIn + 
								"&whereInCdentregamaterial=" + Util.strings.emptyIfNull(whereInCdentregamaterial) + 
								"&whereInQtdeEntrada=" + Util.strings.emptyIfNull(whereInQtdeEntrada) + 
								"';</script>");
				}
			}
			return null;
		}
	}
	
	/**
	 * M�todo que abre a popup para escolha de gerar notas de produtos/servios
	 *
	 * @param request
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	private ModelAndView abrirPopupGerarnotaprodutoservico(WebRequestContext request, String whereIn){
		request.setAttribute("existVendaApenasServico", vendaService.existVendaApenasServico(whereIn));
		request.setAttribute("existVendaApenasProduto", vendaService.existVendaApenasProduto(whereIn));
		request.setAttribute("existVendaReceitaAposEmissaoNota", vendaService.existVendaReceitaAposEmissaoNota(whereIn) && !parametrogeralService.getBoolean(Parametrogeral.GERAR_NF_SEPARADA_TIPOPEDIDOVENDA));
		request.setAttribute("gerarNfDireto", parametrogeralService.getBoolean(Parametrogeral.GERARNOTA_NF_DIRETO));
		return new ModelAndView("direct:/crud/popup/popupGerarnotaprodutoservico");
	}
	

	public ModelAndView gerarFaturamentoConfirmacaoagrupamento(WebRequestContext request){
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		Boolean agrupamento = Boolean.valueOf(request.getParameter("agrupamento") != null ? request.getParameter("agrupamento") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		String cdcliente = request.getParameter("cdcliente");
		String whereInCdentregamaterial = request.getParameter("whereInCdentregamaterial");
		
		boolean existVendaWithProdutoAndServico = vendaService.existVendaWithServico(whereIn);
		
		if(existVendaWithProdutoAndServico){
			request.setAttribute("entrada", entrada);
			request.setAttribute("agrupamento", agrupamento);
			request.setAttribute("selectedItens", whereIn);
			request.setAttribute("cdcliente", cdcliente);
			request.setAttribute("confirmacaoagrupamento", true);
			request.setAttribute("whereInCdentregamaterial", whereInCdentregamaterial);
			return this.abrirPopupGerarnotaprodutoservico(request, whereIn);
		}else {
			return gerarFaturamento(request);
		}
	}
	
	/**
	 * Action que gera o faturamento das vendas.
	 *
	 * @param request
	 * @return Toms Rabelo
	 */
	public ModelAndView gerarFaturamento(WebRequestContext request){
		Boolean isNfe = Boolean.valueOf(request.getParameter("isNfe") != null ? request.getParameter("isNfe") : "true");
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		Boolean agrupamento = Boolean.valueOf(request.getParameter("agrupamento") != null ? request.getParameter("agrupamento") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		String cdcliente = request.getParameter("cdcliente");
		Boolean somenteproduto = Boolean.valueOf(request.getParameter("somente_produto") != null ? request.getParameter("somente_produto") : "false");
		Boolean somenteservico = Boolean.valueOf(request.getParameter("somente_servico") != null ? request.getParameter("somente_servico") : "false");
		Boolean notaseparada = Boolean.valueOf(request.getParameter("nota_separada") != null ? request.getParameter("nota_separada") : "false");
		Boolean notatodos = Boolean.valueOf(request.getParameter("nota_todos") != null ? request.getParameter("nota_todos") : "false");
		
		if(!somenteservico && !somenteproduto && !notaseparada){
			notatodos = true;
		}
		
		try {
			List<Venda> listaVendas = vendaService.findForCobranca(whereIn);
			if (agrupamento && vendaService.isAgruparContasPedidovendatipo(listaVendas)) {
				String erro = vendaService.getErroAgruparContasPedidovendatipo(listaVendas);
				
				if (!StringUtils.isEmpty(erro)) {
					request.addError(erro);
					SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOtr?ACAO=listagem");
				} else {
					int qtdParcelas = 0;
					if (!vendaService.isBonificacaoOrNaoGeraConta(listaVendas)) {
						qtdParcelas = listaVendas.get(0).getListavendapagamento().size();
					}
					
					if(somenteservico){
						SinedUtil.redirecionamento(request, "/faturamento/crud/NotaFiscalServico?ACAO=" + CRIAR + "&whereInCdvenda=" + whereIn + "&qtdParcelas=" + qtdParcelas);
					}else {
						if(isNfe){
							SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto?ACAO=" + CRIAR + "&whereInCdvenda=" + whereIn + "&qtdParcelas=" + qtdParcelas);
						} else {
							SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalconsumidor?ACAO=" + CRIAR + "&whereInCdvenda=" + whereIn + "&qtdParcelas=" + qtdParcelas);
						}
					}
				}
			} else {
				if(cdcliente != null && !"".equals(cdcliente) && !"null".equals(cdcliente)){
					Cliente cliente = new Cliente(Integer.parseInt(cdcliente));
					cliente = clienteService.loadForEntrada(cliente);
					
					for (Venda venda : listaVendas) {
						venda.setCliente(cliente);
					}
				}
				
				if(notatodos){
					notafiscalprodutoService.gerarFaturamentoVenda(request, listaVendas, agrupamento, false, false, isNfe);
				}else {
					if(somenteproduto || notaseparada){
						notafiscalprodutoService.gerarFaturamentoVenda(request, listaVendas, agrupamento, true, notaseparada, isNfe);
					}
					if(somenteservico || notaseparada){
						notaFiscalServicoService.gerarFaturamentoVenda(request, listaVendas, agrupamento, true, notaseparada);
					}
				}
				
				for(String id : whereIn.split(",")){
					if(notaVendaService.existeNotaEmitida(new Venda(Integer.parseInt(id)))){
						vendaService.updateSituacaoVenda(id, Vendasituacao.FATURADA);
					}
				}
					
				request.addMessage("Faturamento(s) gerado(s) com sucesso.");
				
				SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOtr" + (entrada ? "?ACAO=consultar&cdvenda="+whereIn : ""));
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		
		return null;
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, Venda bean) throws Exception {
		bean.setOrigemOtr(true);
		vendaService.salvarCrud(request, bean);
	}
	
	/**
	 * Action que faz o recalculo das comisses das vendas selecionadas.
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentocomissaovendaService#recalcularComissaovenda(Venda venda, Colaborador colaborador)
	 *
	 * @param request
	 * @return
	 * @since 14/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView recalcularComissao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String[] ids = whereIn.split(",");
		
		for (int i = 0; i < ids.length; i++) {
			if(!vendaService.haveVendaSituacao(ids[i], false, Vendasituacao.CANCELADA)){
				Venda venda = new Venda(Integer.parseInt(ids[i]));
				documentocomissaovendaService.recalcularComissaovenda(venda, null);
				
				if(venda.getGerouComissionamentoPorFaixas() == null || venda.getGerouComissionamentoPorFaixas().equals(Boolean.FALSE)) {
					documentocomissaovendarepresentacaoService.recalcularComissaovendarepresentacao(venda, null);
				}
			}
		}
		
		request.addMessage("Comiss�o recalculada com sucesso.");
		
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		if(entrada){
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr?ACAO=consultar&cdvenda=" + whereIn);
		}
		
		return sendRedirectToAction("listagem");
	}
	
	public void ajaxMaterial(WebRequestContext request, Material material ){
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		try {
			material = materialService.getUnidademedidaValorvenda(material);
			
			if (material.getUnidademedida() != null)
				view.println("var unidademedida = '" + material.getUnidademedida().getNome() + "';");
			else
				view.println("var unidademedida = '';");
			
			if (material.getValorvenda() != null)
				view.println("var valorvenda = '" + material.getValorvenda() + "';");
			else
				view.println("var valorvenda = '';");
			
			view.println("var sucesso = true;");						
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}
	
	public void ajaxJuros(WebRequestContext request, Prazopagamento prazopagamento ){
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		try {
			prazopagamento = prazopagamentoService.findForPrazopagamentoJuros(prazopagamento);
			
			if(prazopagamento.getJuros() != null){
				view.println("var juros = '" + prazopagamento.getJuros() + "';");
			}else{
				view.println("var juros = '<null>';");
			}
			
			view.println("var sucesso = true;");						
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}
		
	/**
	* M�todo que valida os valores do oramento
	*
	* @param request
	* @param venda
	* @return
	* @since Jul 22, 2011
	* @author Luiz Fernando F Silva
	*/
	public boolean validaTotalVenda(Venda venda, int size){	
				
		Double juros = prazopagamentoService.loadAll(venda.getPrazopagamento()).getJuros();		
		
		Money desconto = venda.getDesconto() != null ? venda.getDesconto() : new Money();
		Money frete = venda.getValorfrete() != null ? venda.getValorfrete() : new Money();
		Money valor;
		Money bean;
		Double vpValores = 0.0;
		Double vmValores = 0.0;
		Money vp_valores_money = new Money();
		Money totalVenda = new Money();
		for(Vendamaterial vm : venda.getListavendamaterial()){
			if(vm.getTotal()!=null)
				totalVenda = totalVenda.add( vm.getTotal());
		}
		
		if(juros == null || juros.equals(0d)){			  
			valor = new Money(venda.getTotalvenda()).divide(new Money(size));
			
			for(Vendapagamento vp : venda.getListavendapagamento()){
				vp_valores_money = new Money(vp_valores_money.add(vp.getValororiginal()));				
			}
			
			if((""+vp_valores_money).equals(""+(totalVenda.add(frete).subtract(desconto)))){
				return true;
			}else return false;
		}else{
			valor = vendaService.calculaJuros(new Money(totalVenda), size, juros);
			valor = new Money(valor.getValue().doubleValue() * size);
		}
		
		for(Vendapagamento vp : venda.getListavendapagamento()){
			vpValores += vp.getValororiginal().getValue().doubleValue();
		}
		for(Vendamaterial vm : venda.getListavendamaterial()){
			vmValores += vm.getPreco() * vm.getQuantidade();
			if(vm.getDesconto() != null){
				vmValores = vmValores - vm.getDesconto().getValue().doubleValue();
			}
		}
		bean = new Money( vmValores + (venda.getValorfrete() != null ? venda.getValorfrete().getValue().doubleValue() : 0) - desconto.getValue().doubleValue());
		
				
		if(!(bean.getValue().doubleValue() == totalVenda.getValue().doubleValue())){
			return false;		
		}else {
			if((""+vpValores).equals(""+valor.getValue())){
				return false;
			}
		}
		
		return true;
	}
	
	
	
//	/**
//	 * M�todo que recebe a chamada da tela para confirmar uma venda
//	 * 
//	 * @author Thiago Augusto
//	 * @return
//	 */
//	public Venda criar(WebRequestContext request, Venda form) throws Exception{
//		Venda venda = super.criar(request, form);
//		
//		if(request.getParameter("cdpedidovenda") != null && !request.getParameter("cdpedidovenda").equals("")){
//			Pedidovenda pedidoVenda = new Pedidovenda(Integer.parseInt(request.getParameter("cdpedidovenda")));
//			pedidoVenda = pedidovendaService.findByPedidoVenda(pedidoVenda);
//			
//			venda.setCliente(pedidoVenda.getCliente());
//			venda.setEndereco(pedidoVenda.getEndereco());
//			venda.setColaborador(pedidoVenda.getColaborador());
//			venda.setDtvenda(pedidoVenda.getDtpedidovenda());
//			venda.setPrazoentrega(pedidoVenda.getDtprazoentrega());
//			venda.setObservacao(pedidoVenda.getObservacao());
//			venda.setVendasituacao(Vendasituacao.REALIZADA);
//			venda.setListavendamaterial(preencheListaVendaMateria(pedidoVenda));
//			venda.setPedidovenda(pedidoVenda);
//			
//			salvaPedidoHistorico(pedidoVenda);
//		}
//		return venda;
//	}
//	
//	public List<Vendamaterial> preencheListaVendaMateria(Pedidovenda pedidovenda){
//		List<Vendamaterial> list = new ArrayList<Vendamaterial>();
//		
//		if(pedidovenda.getListaPedidovendamaterial() != null){
//			Vendamaterial vendamaterial;
//			for (Pedidovendamaterial pedidoVendaMaterial : pedidovenda.getListaPedidovendamaterial()) {
//				vendamaterial = new Vendamaterial();
//				
//				vendamaterial.setMaterial(pedidoVendaMaterial.getMaterial());
//				vendamaterial.setQuantidade(pedidoVendaMaterial.getQuantidade());
//				vendamaterial.setPreco(pedidoVendaMaterial.getPreco());
//				vendamaterial.setUnidademedida(pedidoVendaMaterial.getUnidademedida());
//				vendamaterial.setTotal(pedidoVendaMaterial.getTotal());
//				
//				list.add(vendamaterial);
//			}
//		}
//		return list;
//	}
//	
//	public void salvaPedidoHistorico(Pedidovenda pedidovenda){
//		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
//		pedidovendahistorico.setPedidovenda(pedidovenda);
//		pedidovendahistorico.setObservacao(pedidovenda.getObservacao());
//		pedidovendahistorico.setAcao("Confirma��o do Pedido de Venda: "+ pedidovenda.getCdpedidovenda());
//		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
//	}
	
	/**
	 * M�todo ajax que carrega a forma de pagamento do cliente da �ltima venda
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxSugerirFormapagamentoCliente(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		String cdcliente = request.getParameter("cdcliente");
		String documentotipo = "var documentotipo = '<null>';";
		String antecipacao = "var antecipacao = 'false'";
		if(cdcliente != null && StringUtils.isNumeric(cdcliente)){
			Venda venda = vendaService.buscaUltimaVendaByCliente(new Cliente(Integer.parseInt(cdcliente)));
			if(venda != null && venda.getDocumentotipo() != null){
				documentotipo = "var documentotipo = 'br.com.linkcom.sined.geral.bean.Documentotipo[cddocumentotipo=" + venda.getDocumentotipo().getCddocumentotipo() + "]';";
				if(Boolean.TRUE.equals(venda.getDocumentotipo().getAntecipacao())){
					antecipacao = "var antecipacao = 'true'";
				}
			}
		}
		
		view.println(documentotipo);
		view.println(antecipacao);
	}
	
	
	/**
	 * Gera uma venda, uma conta a raceber e uma movimentao de estoque
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#load
	 * @see br.com.linkcom.sined.geral.service.RateioService#saveOrUpdate
	 * @see br.com.linkcom.sined.geral.service.VendaService#saveOrUpdate
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#saveOrUpdate
	 * @see br.com.linkcom.sined.geral.service.DocumentoorigemService#saveOrUpdate
	 * @see br.com.linkcom.sined.geral.service.MaterialService#load
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#saveOrUpdate
	 * @param request
	 * @param venda
	 * @return
	 * @author Ramon Brazil
	 */
	public ModelAndView comprar(WebRequestContext request, Venda venda){
		venda.setOrigemOtr(true);
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		boolean fromWebService = Boolean.TRUE.equals(parameters.getFromWebService());
		/*if (vendaService.verificarPermitirMaterialMestreVenda(venda.getListavendamaterial())) {
			request.addError("N�o � poss�vel confirmar pedido de venda enquanto existirem materiais mestres de grade no pedido.");
			return continueOnAction("entrada", venda);
		}
		
		vendaService.buscarTabelaPrecoVenda(venda);
		
		String externo = parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO);
		if(externo.equals("TRUE")){
			if(venda.getPedidovenda() !=null){
				request.setAttribute("boquearIdentificador", true);
			}else if(!vendaService.comprarIdentificadorExterno(venda.getIdentificadorexterno(),venda.getCdvenda())){
				request.addError(MSG_IDENTIFICADOR_EXTERNO);
				return continueOnAction("entrada", venda);
			}else if (venda.getIdentificadorexterno() == null || venda.getIdentificadorexterno() == ""){
				request.addError("Campo identificador externo � obrigatrio.");
				return continueOnAction("entrada", venda);
			}	
		}
			
		String paramAtualizaData = parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZAR_VENCIMENTO_CONFIRMACAO_VENDA);
		if(paramAtualizaData != null && paramAtualizaData.trim().toUpperCase().equals("TRUE")){
			if(venda.getPrazopagamento() != null && venda.getPrazopagamento().getCdprazopagamento() != null){
				vendaService.ajustaVencimentos(request, venda);
			}
		}
		Pedidovenda pedidovenda = venda.getPedidovenda();
		if(pedidovenda != null){
			pedidovenda = pedidovendaService.loadForEntrada(pedidovenda);
			
			if(pedidovenda.getPedidovendasituacao() != null && pedidovenda.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO)){
				request.addError("Pedido de venda j� confirmado.");
				return continueOnAction("entrada", venda);
			}
		}
		
		boolean usarSequencial = venda.getIdentificadorAutomatico() != null && venda.getIdentificadorAutomatico();
		boolean isCriar = venda.getCdvenda() == null;
		if(usarSequencial && isCriar){
			Integer proximoIdentificador = empresaService.carregaProximoIdentificadorVenda(venda.getEmpresa());
			if(proximoIdentificador == null){
				proximoIdentificador = 1;
			}
			empresaService.updateProximoIdentificadorVenda(venda.getEmpresa(), proximoIdentificador+1);
			venda.setIdentificador(proximoIdentificador.toString());
		}
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.IDENTIFICADOR_UNICO_PEDIDO_VENDA_ORCAMENTO)) && venda.getEmpresa() != null && StringUtils.isNotBlank(venda.getIdentificador()) && vendaService.isIdentificadorCadastrado(venda)) {
			request.addError("Aten��o! Identificador j� cadastrado no sistema.");
			return continueOnAction("entrada", venda);
		}
		
		Pedidovendatipo pedidovendatipo = null;
		if(venda.getPedidovendatipo() != null){
			pedidovendatipo = pedidovendatipoService.loadForEntrada(venda.getPedidovendatipo());
			venda.setIsNotVerificarestoquePedidovenda(vendaService.isNotVerificarEstoqueByPedidovendatipo(pedidovendatipo));
		}
		List<Vendamaterial> listaVendamaterialForCriarTabela = vendaService.getListavendamaterialForCriarTabela(venda);
		if(vendaService.isQtdeSolicitadaMaiorOrIgualQtdeReservada(request, venda, false)){
			vendaService.setAttributeErroValidacaoVenda(request);
			return continueOnAction("entrada", venda);
		}
		
		if (!vendaService.validaVenda(request, venda)) {
			request.setAttribute("vendainvalida", "true");
			if (venda.getColaboradoraux() != null && venda.getColaboradoraux().getCdpessoa() != null) {
				venda.setColaborador(venda.getColaboradoraux());
			} else {
				if (venda.getColaborador() == null) {
					venda.setColaborador(SinedUtil.getUsuarioComoColaborador());
				}
			}

			if (venda.getEndereco() != null && venda.getEndereco().getCdendereco() != null) {
				request.setAttribute("enderecoEscolhido", "br.com.linkcom.sined.geral.bean.Endereco[cdendereco=" + venda.getEndereco().getCdendereco() + "]");
			} else {
				request.setAttribute("enderecoEscolhido", "<null>");
			}

			return continueOnAction("entrada", venda);
		}
	
		//valida��o j� � feita em vendaService.validaVenda(request, venda)
//		if(!vendaService.validaListaVendamaterial(request, venda)){
//			return continueOnAction("entrada", venda);
//		}
		
		boolean verificarparcelas = true;
		if((venda.getBonificacao() != null && venda.getBonificacao()) || (venda.getComodato() != null && venda.getComodato()) ){
			verificarparcelas = false;
		}
		if(verificarparcelas){
			if(!validaListaVendamaterialComListaVendaPagamento(request, venda, null)){
				vendaService.setAttributeErroValidacaoVenda(request);
				return continueOnAction("entrada", venda);
			}
		}
		
		if(!vendaService.validaListaVendapagamentorepresentacao(request, venda, pedidovendatipo)){
			return continueOnAction("entrada", venda);
		}
		
		if(!vendaService.validaKitFlexivel(request, venda)){
			return continueOnAction("entrada", venda);
		}
		
		if(venda.getColaboradoraux() != null && venda.getColaboradoraux().getCdpessoa() != null){
			venda.setColaborador(venda.getColaboradoraux());
		}else{
			if (venda.getColaborador() == null) {
				if (SinedUtil.getUsuarioComoColaborador() == null) {
					request.addError("Aten��o! Este usu�rio n�o � colaborador.");
					return continueOnAction("entrada", venda);
				} else {
					venda.setColaborador(SinedUtil.getUsuarioComoColaborador());
				}
			}
			
		}
		
		Colaborador colaborador = colaboradorService.findColaboradorusuario(venda.getColaborador().getCdpessoa());
		if(colaborador == null || colaborador.getCdpessoa() == null){
			request.addError("Aten��o! Este usu�rio n�o � colaborador.");
			return new ModelAndView("/faturamento/crud/VendaOtr", "venda", venda);
		}
		if(restricaoService.verificaRestricaoSemDtLiberacao(venda.getCliente())){
			request.addError("O cliente possui restri��o(�es) sem data de libera��o.");
			request.setAttribute("vendainvalida", "true");
			return continueOnAction("entrada", venda);
		}
		
		if (!validaPagamentoAntecipado(venda)){
			request.addError("O valor da parcela deve ser menor ou igual ao saldo do pagamento adiantado escolhido.");
			return continueOnAction("entrada", venda);
		}
		
		if(!vendaService.validaVendedorprincipal(venda, request)){
			return continueOnAction("entrada", venda);
		}
		
		if(!vendaService.validaComissionamento(venda, request)){
			return continueOnAction("entrada", venda);
		}
		
		if(!vendaService.validaObrigatoriedadePneu(venda, request, null)){
			return continueOnAction("entrada", venda);
		}
		
		vendaService.validaOrdenacao(request, venda.getListavendamaterial());
		if(request.getBindException().hasErrors()){
			return continueOnAction("entrada", venda);
		}*/
		if(!vendaService.validateComprar(request, venda)){
			return continueOnAction("entrada", venda);
		}
		Pedidovenda pedidovenda = venda.getPedidovenda();
		if(pedidovenda != null){
			pedidovenda = pedidovendaService.loadForEntrada(pedidovenda);
		}
		List<Vendamaterial> listaVendamaterialForCriarTabela = vendaService.getListavendamaterialForCriarTabela(venda);
		List<String> listaMotivosAguardandoAprovacao = new ArrayList<String>();
		vendaService.comprar(request, venda, pedidovenda, listaVendamaterialForCriarTabela, listaMotivosAguardandoAprovacao);
		
		String mensagem = "Venda realizada com sucesso.";
		String retorno = "VendaOtr?ACAO=criar";
		String alertaQuantidade = "";
		if(pedidovenda != null){
			retorno = request.getServletRequest().getContextPath() + "/faturamento/crud/PedidovendaOtr";
			mensagem = "Pedido de venda " + pedidovenda.getCdpedidovenda() + " confirmado com sucesso.";
			if(Boolean.TRUE.equals(request.getAttribute("isPendenciaCriada"))){
				alertaQuantidade = "alert('A quantidade total dos itens do pedido de venda � diferente da quantidade total dos itens das vendas." + request.getAttribute("stringMaterialQdt") +"'); ";
			}
		}
		if(venda.getVendaorcamento() != null){
			retorno = request.getServletRequest().getContextPath() + "/faturamento/crud/VendaOrcamento";
			mensagem = "Or�amento " + venda.getVendaorcamento().getCdvendaorcamento() + " autorizado como venda com sucesso.";
		}
		
		if(venda.getGerarnotaautomatico() != null && venda.getGerarnotaautomatico()){
			retorno = request.getServletRequest().getContextPath() + "/faturamento/crud/VendaOtr?ACAO=consultar&gerarnotaautomatico=true&cdvenda=" + venda.getCdvenda();
		}
		
		if(Vendasituacao.EMPRODUCAO.equals(venda.getVendasituacao()) && !SinedUtil.isRecapagem()){
			retorno = request.getServletRequest().getContextPath() + "/faturamento/crud/VendaOtr?ACAO=consultar&cdvenda=" + venda.getCdvenda() + "&producaoautomaticavenda=true";
		}
		
		Boolean closeOnSave = parameters.getCloseOnSave();
		Boolean emitirComprovante = parameters.getEmitirComprovante();
		
		request.addMessage(mensagem);
		venda.getMensagemAposSalvar().add(new Message(MessageType.INFO, mensagem));
		
		Boolean emitirComprovanteAguardandoAprovacao = parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO");
		String mensagemMotivoAguardandoAprovacao = pedidovendaService.montaMensagemMotivosAguardandoAprovacao(listaMotivosAguardandoAprovacao);
		String alertaMotivosAguardandoAprovacao = "";
		
		if(StringUtils.isNotBlank(mensagemMotivoAguardandoAprovacao)) {
			mensagemMotivoAguardandoAprovacao = "Esta venda ficar� como Aguardando Aprova��o porque " + mensagemMotivoAguardandoAprovacao;
			
			if(emitirComprovanteAguardandoAprovacao) {
				mensagemMotivoAguardandoAprovacao += ".";
			} else {
				int tamanho = listaMotivosAguardandoAprovacao.size();
				mensagemMotivoAguardandoAprovacao += ". Por " + (tamanho > 1 ? "esses motivos" : "esse motivo") +
						" n�o ser� poss�vel a emiss�o ou envio de comprovante at� a sua aprova��o.";
			}
			venda.getMensagemAposSalvar().add(new Message(MessageType.WARN, mensagemMotivoAguardandoAprovacao));
			if(!fromWebService){
				alertaMotivosAguardandoAprovacao = "alert('" + mensagemMotivoAguardandoAprovacao + "');";
			}
		}
		if(!fromWebService){
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<html><body>" +
					"<script>" +
					alertaQuantidade +
					alertaMotivosAguardandoAprovacao +
					"if("+(emitirComprovanteAguardandoAprovacao && emitirComprovante)+") " +
						"{window.open('../../faturamento/relatorio/Venda?ACAO=gerar&fromCriacaoVenda=true&origemOtr=true&cdvenda="+venda.getCdvenda()+"');" +
					"}" +
					"window.location = '" + retorno + "';" +
					"if("+closeOnSave+"){" +
						"window.close();" +
					"}" +
					"</script>"+
					"</body></html>"
					);
		}
		
		return null;
	}
	
	public ModelAndView abrirPopupSelecaoPneus(WebRequestContext request) throws Exception{
		String whereIn = request.getParameter("whereInVenda");
		
		if(StringUtils.isBlank(whereIn)){
			request.addError("erro ao selecionar");
			return sendRedirectToAction("listagem"); 
		}
		
		return vendaService.abrirPopupSelecaoPneus(request, whereIn);
	}
	
//	/**
//	 * Retorna ao index limpando o filtro
//	 * @param request
//	 * @param venda
//	 * @return
//	 * @author Ramon Brazil
//	 */	
//	public ModelAndView cancelar(WebRequestContext request, Venda venda){	
//		venda = new Venda();
//		request.getSession().removeAttribute("listavendamaterial");
//		return index(request, venda);
//	}


	/**
	 * Carrrga um material de acordo com o codigo informado.
	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadWithCodFlex
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getQuantidadeDeMaterialEntrada
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getQuantidadeDeMaterialSaida
	 * @param request
	 * @param venda
	 * @author Ramon Brazil
	 */
	public void buscarMaterialAJAX(WebRequestContext request, Venda venda){
		ModelAndView modelAndView = pedidovendaService.buscarMaterialAJAXJson(request, venda, true);
		View view = View.getCurrent();
		Map mapa = modelAndView.getModel();
		System.out.println(mapa.get("percentualComissaoAgencia"));
		System.out.println(mapa.get("listaMaterialUnidademedida"));
		if(mapa.containsKey("alturas") || mapa.containsKey("comprimentos") || mapa.containsKey("larguras")){
			view.println("var comprimentos = '" + mapa.get("comprimentos") + "'; ");
			view.println("var alturas = '" + mapa.get("alturas") + "'; ");
			view.println("var larguras = '" + mapa.get("larguras") + "'; ");
//			view.println("var fatorconversaocomprimento = '" + (produto.getFatorconversao() != null ? produto.getFatorconversao() : "") + "'; ");
			view.println("var fatorconversaocomprimento = ''; ");
			view.println("var margemarredondamento = '" + mapa.get("margemarredondamento") + "'; ");
		}
		view.println("var exibirmaterialcoleta = '" + mapa.get("exibirMaterialcoleta") + "';");
		view.println("var materialcoleta_id = '" + mapa.get("materialcoleta_id") + "';");
		view.println("var materialcoleta_label = \"" + mapa.get("materialcoleta_label") + "\";");
		if(mapa.containsKey("exibirMaterialcoleta")){
			view.println("var existematerialsimilarColeta = '" + mapa.get("exibirexistematerialsimilarColeta") + "';");
		}
		view.println("var existecontroleGrade = '" + mapa.get("existecontroleGrade") + "';");
		view.println("var existeitemgrade = '" + mapa.get("existeitemgrade") + "';");
		view.println("var metrocubicovalorvenda = '" + mapa.get("metrocubicovalorvenda") + "';");
		view.println("var identificacaomaterial = '" + mapa.get("identificacaomaterial") + "';");
		view.println("var multiplicador = '" + mapa.get("multiplicador") + "';");
		view.println("var showBotaoLargCompAlt = '" + mapa.get("showBotaoLargCompAlt") + "'; ");
		view.println("var existMaterialsimilar = '" + mapa.get("existMaterialsimilar") + "'; ");
		view.println("var vendapromocional = '" + mapa.get("vendapromocional")+ "';");
		view.println("var kitflexivel = '" + mapa.get("kitflexivel")+ "';");
		view.println("var valorproduto = '" + mapa.get("valorproduto") + "';");
		view.println("var valorminimo = '" + mapa.get("valorminimo") + "';");
		view.println("var valormaximo = '" + mapa.get("valormaximo") + "';");
		view.println("var percentualdescontoTabelapreco ='" + mapa.get("percentualdescontoTabelapreco") + "';");
		view.println("var tabelaprecoComPrazopagamento = '" + mapa.get("tabelaprecoComPrazopagamento") + "';");
		view.println("var naoexibirminmaxvenda = '" + mapa.get("naoexibirminmaxvenda") + "';");
		view.println("var qtddisponivel = '" + mapa.get("qtddisponivel") + "';");
		view.println("var showqtddisponivelAcimaestoque = '" + mapa.get("showqtddisponivelAcimaestoque") + "';");
		view.println("var qtddisponivelAcimaestoque = '" + mapa.get("qtddisponivelAcimaestoque") + "';");
		view.println("var msgacimaestoque = '" + mapa.get("msgacimaestoque") + "';");
		view.println("var pesquisasomentemestre = '" + mapa.get("pesquisasomentemestre") + "';");
		
		view.println("var validaestoque ='" + mapa.get("validaestoque") + "';");
		view.println("var materialservico ='" + mapa.get("materialservico") + "';");
		view.println("var exibiritensproducaovenda ='" + mapa.get("exibiritensproducaovenda") + "';");
		view.println("var exibiritenskitvenda ='" + mapa.get("exibiritenskitvenda") + "';");
		view.println("var producao ='" + mapa.get("producao") + "';");
		view.println("var qtdereservada ='" + mapa.get("qtdreservada") + "';");
		view.println("var qtdeunidade = '" + mapa.get("qtdeunidade") + "'; ");
		view.println("var considerarvendamultiplos = '" + mapa.get("considerarvendamultiplos") + "'; ");
		view.println("var valorcustomaterial = '" + mapa.get("valorcustomaterial")+ "';");
		view.println("var valorvendamaterial = '" + mapa.get("valorvendamaterial")+ "';");
		view.println("var pesoliquidovalorvenda = '" + mapa.get("pesoliquidovalorvenda") + "';");
		view.println("var peso = '" + mapa.get("peso") + "';");
		view.println("var pesobruto = '" + mapa.get("pesobruto") + "';");
		view.println("var tributacaoipi ='" + mapa.get("tributacaoipi") + "';");
		view.println("var materialobrigalote = '" + mapa.get("materialobrigalote") + "';");
		
		view.println("var pneumedida_id = \"" + mapa.get("pneumedida_id") + "\";");
		view.println("var pneumedida_label = \"" + mapa.get("pneumedida_label") + "\";");
		view.println("var materialbanda_id = \"" + mapa.get("materialbanda_id") + "\";");
		view.println("var materialbanda_label = \"" + mapa.get("materialbanda_label") + "\";");
		
		view.println("var pneuSegmento_id = \"" + mapa.get("pneuSegmento_id") + "\";");
		view.println("var pneuSegmento_label = \"" + mapa.get("pneuSegmento_label") + "\";");
		
		view.println("var cdarquivo = '" + mapa.get("cdarquivo") + "';");
		if(mapa.containsKey("anotacoes")){
			view.println("var anotacoes = \"" + mapa.get("anotacoes") + "\"; ");
		}
		else view.println("var anotacoes = '" + "" + "';");
		
		view.println("var listaMaterialUnidademedida = " + mapa.get("listaMaterialUnidadeMedida") + ";");
		view.println("var unidademedidaMaterial = '" + mapa.get("unidadeMedidaMaterial") + "';");
		System.out.println(mapa.get("unidadeMedidaMaterial"));
		view.println("var percentualcomissaoagencia = '" + mapa.get("percentualComissaoAgencia") + "';");
		view.println("var materialpatrimonio = '" + mapa.get("materialpatrimonio") + "';");
		view.println("var abrirPopupPneu = '" + mapa.get("abrirPopupPneu") + "';");
		//vendaService.buscarMaterialAjaxCentralizado(request, venda);
	}
	
	/**
	 * M�todo ajax para atualizar o valor de venda caso o comprimento seja alterado
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxAtualizarvalorvenda(WebRequestContext request, Material material){
		return vendaService.ajaxAtualizarvalorvenda(request, material);
	}
	
	/**
	 * M�todo ajax que busca as informaes do material promocional
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadMaterialPromocionalComMaterialrelacionado(Integer cdmaterial)
	 *
	 * @param request
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView buscarInfMaterialPromocionalAJAX(WebRequestContext request, Venda venda){	
		return vendaService.buscarInfMaterialPromocionalAJAX(request, venda);
	}
	
	public ModelAndView buscarInfMaterialProducaoAJAX(WebRequestContext request, Venda venda){	
		return vendaService.buscarInfMaterialProducaoAJAX(request, venda);
	}
	
	public ModelAndView recalcularMaterialProducaoAJAX(WebRequestContext request, Venda venda){
		Material material = materialService.loadMaterialComMaterialproducao(new Material(Integer.parseInt(venda.getCodigo())));
		venda.setMaterial(material);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
		
		vendaService.trocarMaterialProducao(material, venda);
		
		if(material.getProducao()){
			Vendamaterialmestre vmm = new Vendamaterialmestre();
			vmm.setAltura(venda.getAlturas());
			vmm.setComprimento(venda.getComprimentos());
			vmm.setLargura(venda.getLarguras());
			vmm.setVenda(venda);
			vmm.setMaterial(material);
			venda.getListaVendamaterialmestre().add(vmm);
		}
		
		Double valorcusto = material.getValorcusto();
		try{
			material.setProduto_altura(venda.getAlturas());
			material.setProduto_largura(venda.getLarguras());
			material.setQuantidade(venda.getQuantidades());
			pedidovendaService.preecheValorVendaComTabelaPrecoItensProducao(material, venda.getPedidovendatipo(), venda.getCliente(), venda.getEmpresa(), venda.getPrazopagamento());
			valorcusto = materialproducaoService.getValorvendaproducao(material);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean recapagem = SinedUtil.isRecapagem();
		
		Materialproducao materialproducaoMestre = materialproducaoService.createItemProducaoByMaterialmestre(material.getListaProducao(), material);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), true));
		
		if(recapagem){
			vendaService.preencheValorVendaComTabelaPreco(material, venda.getMaterialtabelapreco(), venda.getPedidovendatipo(), venda.getCliente(), venda.getEmpresa(), material.getUnidademedida(), venda.getPrazopagamento());
			materialproducaoMestre.getMaterial().setValorvenda(material.getValorvenda());
			materialproducaoMestre.getMaterial().setValorcusto(valorcusto);
		}
		
		for (Materialproducao materialproducao : material.getListaProducao()) {
			if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && venda.getQuantidades() != null){
				materialproducao.setConsumo(materialproducao.getConsumo() * venda.getQuantidades());
				
				materialformulavalorvendaService.setValorvendaByFormula(materialproducao.getMaterial(), 
						venda.getAlturas() != null ? venda.getAlturas() : null, 
						venda.getLarguras() != null ? venda.getLarguras() : null, 
						venda.getComprimentos() != null ? venda.getComprimentos() : null,
						venda.getValorfrete() != null ? venda.getValorfrete().getValue().doubleValue() : null);
			}
		}
		if(materialproducaoMestre != null){
			if(venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
				for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
					if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().equals(materialproducaoMestre.getMaterial())){
						materialproducaoMestre.setPrazoentrega(vendamaterial.getPrazoentrega());
						materialproducaoMestre.setLoteestoque(vendamaterial.getLoteestoque());
						materialproducaoMestre.setPedidovendamaterial(vendamaterial.getPedidovendamaterial());
					}
				}
			}
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", material);
		json.addObject("materialproducaoMestre", materialproducaoMestre);
		
		return json;
	}
	
//	/**
//	 * Carrega o material a partir do codigo informado.
//	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadWithCodFlex
//	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getQuantidadeDeMaterialEntrada
//	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getQuantidadeDeMaterialSaida
//	 * @param request
//	 * @param venda
//	 * @author Ramon Brazil
//	 */
//	public void buscarCodigoAJAX(WebRequestContext request, Venda venda){	
//		venda.setMaterial(materialService.loadByCodigoMaterial(venda.getMaterial().getCdmaterial()));
//		Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(venda.getMaterial())== null?new Double(0):movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(venda.getMaterial());
//		Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(venda.getMaterial())== null?new Double(0):movimentacaoestoqueService.getQuantidadeDeMaterialSaida(venda.getMaterial());
//		Integer qtddisponivel = (entrada.intValue()- saida.intValue());
//		request.getServletResponse().setContentType("text/html");
//		View view = View.getCurrent();
//		view.println("var produto = '" + venda.getMaterial().getNome() + "';");
//		view.println("var codigoproduto = '" + venda.getMaterial().getCdmaterial() + "';");
//		view.println("var valorproduto = '" + venda.getMaterial().getValorvenda() + "';");
//		view.println("var valorminimo = '" + venda.getMaterial().getValorvendaminimo() + "';");
//		view.println("var valormaximo = '" + venda.getMaterial().getValorvendamaximo() + "';");
//		view.println("var qtddisponivel = '" + qtddisponivel + "';");
//		Boolean validaestoque = !(venda.getMaterial().getServico() || venda.getMaterial().getProducao());
//		view.println("var validaestoque ='" + validaestoque + "';");
//	}
	
	/**
	 * Gera a quantidade de linhas de pagamento de acordo a quantidade de parcelas 
	 * @see br.com.linkcom.sined.geral.service.prazopagamentoitemService#countParcelas
	 * @param request
	 * @param venda
	 * @author Ramon Brazil
	 */
	public ModelAndView pagamento(WebRequestContext request, Venda venda){
		return vendaService.pagamento(request, venda, request.getParameter("dtprazoentrega"));
	}
	
	
//	/**
//	 * Carrega as linhas de pagamento de acordo a quantidade de parcelas 
//	 * @see br.com.linkcom.sined.geral.service.prazopagamentoitemService#findByPrazoOfParcela
//	 * @see br.com.linkcom.sined.geral.service.prazopagamentoitemService#countParcelas
//	 * @param request
//	 * @param venda
//	 * @author Ramon Brazil
//	 */
//	public void preencherpagamento(WebRequestContext request, Venda venda){
//		Prazopagamentoitem item = prazopagamentoitemService.findByPrazoOfParcela(venda.getPrazopagamento(),venda.getCodigo()+1);
//		Integer parcela = prazopagamentoitemService.countParcelas(venda.getPrazopagamento()).intValue();
//		Double juros = prazopagamentoService.loadAll(venda.getPrazopagamento()).getJuros();
//		Money valor;
//		  if(juros==null){			  
//			  valor = venda.getTotalvenda().divide(new Money(parcela)); 
//		  }else{
//			  valor = calculaJuros(venda.getTotalvenda(), parcela, juros);
//		  }
//		
//		  SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
//		Date dtparcela;
//		if(item.getDias() != null && item.getDias() > 0){
//			dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(venda.getDtvenda().getTime()), item.getDias().intValue(), Calendar.DAY_OF_MONTH);
//		} else if(item.getMeses() != null && item.getMeses() > 0){
//			dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(venda.getDtvenda().getTime()), item.getMeses().intValue(), Calendar.MONTH);
//		} else {
//			dtparcela = venda.getDtvenda();
//		}
//		
//		request.getServletResponse().setContentType("text/html");
//		View view = View.getCurrent();
//		view.println("var valor = '" +valor + "';");
//		view.println("var dtparcela = '" +formatador.format(dtparcela)+ "';");
//	}

	
	
	/**
	 * Retorna a unidade de medida do material para ser setada como default ao selecionar
	 * determinado material em Realizar venda.
	 * @param request
	 * @param bean
	 * @return
	 * @author Taidson
	 * @since 29/12/2010
	 */
	public ModelAndView getUnidademedida(WebRequestContext request, Material bean){
		return vendaService.getUnidademedida(request, bean);
	}
	
	/**
	 * M�todo que busca os lotes com qtde dispon�vel para o material
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView getLoteestoque(WebRequestContext request, Material material){
		return vendaService.getLoteestoque(request, material);
	}
	
	/**
	 * M�todo que busca a qtde do lote
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView getQtdeLoteestoque(WebRequestContext request, Material material){
		String cdloteestoquestr = request.getParameter("cdloteestoque");
		String cdempresa = request.getParameter("cdempresa");
		String cdlocalarmazenagem = request.getParameter("cdlocalarmazenagem");
		Empresa empresa = null;
		Localarmazenagem localarmazenagem = null;
		try {
			if(cdempresa != null && !"".equals(cdempresa)){
				empresa = new Empresa(Integer.parseInt(cdempresa));
			}
			if(cdlocalarmazenagem != null && !"".equals(cdlocalarmazenagem)){
				localarmazenagem = new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem));
			}
		} catch (NumberFormatException e) {}
		Double qtde = 0.0;
		if(cdloteestoquestr != null && !"".equals(cdloteestoquestr)){
			qtde = loteestoqueService.getQtdeLoteestoque(material, localarmazenagem, empresa, new Loteestoque(Integer.parseInt(cdloteestoquestr)), false);
		}
		return new JsonModelAndView().addObject("qtdeloteestoque", qtde);
	}

	
	/**
	 * M�todo responsvel por converter o valor do produto 
	 * de acordo com o fator de converso da nova unidade de medida selecionada.
	 * @param request
	 * @param bean
	 * @return
	 * @author Taidson
	 * @since 30/12/2010
	 */
	public ModelAndView converteUnidademedida(WebRequestContext request, Material bean){
		PedidoVendaParametersBean parameters = vendaService.createParametersFromRequest(request);
		return vendaService.converteUnidademedida(request, bean);
	}
	
	/**
	 * M�todo chamado via AJAX, que carrega os endereos do cliente
	 * 
	 * @param request
	 * @param cliente
	 * @return
	 * @author Toms Rabelo
	 */
	public ModelAndView comboBoxEndereco(WebRequestContext request, Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		List<Endereco> lista = enderecoService.findForVenda(cliente);

		return new JsonModelAndView().addObject("lista", lista);
	}
	
	/**
	 * M�todo chamado via AJAX, que carrega os documentos para antecipao
	 * 
	 * @param request
	 * @param documento
	 * @return
	 * @author Marden Silva
	 */
	public ModelAndView ajaxDocumentoAntecipacao(WebRequestContext request, Venda venda) {
		if (venda == null || venda.getCliente() == null) {
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		
		List<Documento> listaDocumento = new ArrayList<Documento>();
		Boolean isWms = empresaService.isIntegracaoWms(empresaService.loadPrincipal());		
		if ((isWms != null && isWms) || parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_PEDIDO_ANTECIPACAO))
			listaDocumento = documentoService.findForAntecipacaoConsideraPedidoVenda(venda.getCliente(), venda.getDocumentotipo(), venda.getPedidovenda());
		else
			listaDocumento = documentoService.findForAntecipacaoConsideraVenda(venda.getCliente(), venda.getDocumentotipo());
		
		return new JsonModelAndView().addObject("lista", listaDocumento);
	}
	
	/**
	 * M�todo chamado via AJAX, que carrega se o documento tipo � de antecipa��o
	 * 
	 * @param request
	 * @param documento
	 * @return
	 * @author Marden Silva
	 */
	public void ajaxTipoDocumentoAntecipacao(WebRequestContext request, Documentotipo documentotipo) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		boolean antecipacao = false;
		
		if (documentotipo != null && documentotipo.getCddocumentotipo() != null) {
			documentotipo = documentotipoService.load(documentotipo);
			antecipacao = documentotipo.getAntecipacao() != null ? documentotipo.getAntecipacao() : false;			
		}
		
		view.println("antecipacao = " + antecipacao + ";");
	}
	
	/**
	 * M�todo ajax que verifica se o tipo de documento � boleto
	 *
	 * @param request
	 * @param documentotipo
	 * @author Luiz Fernando
	 */
	public void ajaxTipoDocumentoBoleto(WebRequestContext request, Documentotipo documentotipo) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");		
		String boleto = "false";
		String contaboleto = "";
		
		if (documentotipo != null && documentotipo.getCddocumentotipo() != null) {
			documentotipo = documentotipoService.load(documentotipo);
			if(documentotipo.getBoleto() != null && documentotipo.getBoleto()){
				String cdempresa = request.getParameter("cdempresa");
				boleto =  "true";
				
				if(cdempresa != null && !"".equals(cdempresa)){
					try {
						Empresa empresa = empresaService.loadWithContaECarteira(new Empresa(Integer.parseInt(cdempresa)));
						List<Conta> listaConta = null;
						if(empresa != null && empresa.getContabancariacontareceber() != null && empresa.getContabancariacontareceber().getCdconta() != null){
							listaConta = new ArrayList<Conta>();
							listaConta.add(empresa.getContabancariacontareceber());
						}else{
							listaConta = contaService.findContasAtivasComPermissaoByEmpresa(new Empresa(Integer.parseInt(cdempresa)));
						}
						
						if(listaConta != null && listaConta.size() == 1){
							Conta conta  = listaConta.get(0);
							if(conta != null && conta.getCdconta() != null){
								contaboleto = "br.com.linkcom.sined.geral.bean.Conta[cdconta=" + conta.getCdconta() + "]";
							}
						}
					} catch (NumberFormatException e) {}
						catch (Exception e) {}
					
				}
			}
			
		}
		
		view.println("var boleto = '" + boleto + "'; ");
		view.println("var contaboleto = '" + contaboleto + "'; ");
	}
	
	/**
	 * M�todo ajax que verifica se o tipo de documento � boleto
	 *
	 * @param request
	 * @param documentotipo
	 * @author Luiz Fernando
	 */
	public void ajaxTipoDocumentoBoletoPagamento(WebRequestContext request) {
		vendaService.ajaxTipoDocumentoBoletoPagamento(request);
	}
	
	/**
	 * M�todo ajax para buscar prazopagamento de acordo com o parmetro
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaPrazopagamento(WebRequestContext request, Venda bean){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		Boolean prazomedio = Boolean.TRUE.equals(bean.getPrazomedio());
		List<Prazopagamento> listaPrazopagamento = clienteService.findPrazoPagamento(bean.getCliente(), prazomedio, false);
		view.println(SinedUtil.convertToJavaScript(listaPrazopagamento, "listaprazopagamento", ""));		
	}
	
	/**
	 * Efetua sendRedirect para determinada action.
	 */
	protected ModelAndView sendRedirectToAction(String action, String parametros) {
		WebRequestContext requestContext = NeoWeb.getRequestContext();
		String requestQuery = requestContext.getRequestQuery();
		String query = "?" + ACTION_PARAMETER + "=" + action;
		return new ModelAndView("redirect:" + requestQuery + query + (StringUtils.isNotBlank(parametros) ? parametros : ""));
	}

	/**
	 * M�todo que aprova oramento. Nesse m�todo � trocado a situa��o da venda, gera movimenta��o de sa�da e salva a venda
	 * 
	 * @param request
	 * @param venda
	 * @return
	 * @author Toms Rabelo
	 */
	public ModelAndView aprovarOrcamentoProcess(WebRequestContext request, Venda venda){
		if(!vendaService.haveVendaSituacao(venda.getCdvenda().toString(), false, Vendasituacao.AGUARDANDO_APROVACAO)){
			request.addError("A venda j� foi aprovada.");
			return sendRedirectToAction("consultar", "&cdvenda=" + venda.getCdvenda());
		}
		
		if(vendaService.isAprovando(venda.getCdvenda().toString())){
			request.addError("A venda est em processo de aprovao.");
			return sendRedirectToAction("consultar", "&cdvenda=" + venda.getCdvenda());
		}
		
		vendaService.updateAprovandoVenda(venda.getCdvenda().toString(), true);
		
		if (!vendaService.validaVenda(request, venda)){
			return sendRedirectToAction("editar&aprovarVenda=true", "&cdvenda=" + venda.getCdvenda());
		}
		
		Pedidovendatipo pedidovendatipo = venda.getPedidovendatipo();
		if(pedidovendatipo != null){
			pedidovendatipo = pedidovendatipoService.loadForEntrada(pedidovendatipo);
		}
		
		if(!vendaService.validaListaVendapagamentorepresentacao(request, venda, pedidovendatipo)){
			return continueOnAction("entrada", venda);
		}
		
		if(restricaoService.verificaRestricaoSemDtLiberacao(venda.getCliente())){
			request.addError("O cliente possui restri��o(�es) sem data de libera��o.");
			return sendRedirectToAction("editar&aprovarVenda=true", "&cdvenda=" + venda.getCdvenda());
		}
		boolean verificarparcelas = true;
		if((venda.getBonificacao() != null && venda.getBonificacao()) || (venda.getComodato() != null && venda.getComodato()) ){
			verificarparcelas = false;
		}
		if(verificarparcelas){
			if(!vendaService.validaListaVendamaterialComListaVendaPagamento(request, venda, null)){
				vendaService.setAttributeErroValidacaoVenda(request);
				return sendRedirectToAction("editar&aprovarVenda=true", "&cdvenda=" + venda.getCdvenda());
			}
		}
		
		
		for (Vendapagamento vendapagamento : venda.getListavendapagamento()) 
			vendapagamento.setCdvendapagamento(null);
		for (Vendamaterial vendamaterial : venda.getListavendamaterial()) 
			vendamaterial.setCdvendamaterial(null);
		
		vendapagamentoService.deleteAllFromVenda(venda);
		vendamaterialService.deleteAllFromVenda(venda);

		boolean gerarContareceber = true;
		boolean ajusteEstoque = true;
		boolean representacao = false;
		boolean gerarexpedicaovenda = false;
		BaixaestoqueEnum baixaestoqueEnum = BaixaestoqueEnum.APOS_VENDAREALIZADA;
		GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
		
		if(pedidovendatipo != null){
			pedidovendatipo = pedidovendatipoService.loadForEntrada(pedidovendatipo);
			
			if(pedidovendatipo.getBaixaestoqueEnum() != null){
				baixaestoqueEnum = pedidovendatipo.getBaixaestoqueEnum();
			}
			if(pedidovendatipo.getGeracaocontareceberEnum() != null){
				geracaocontareceberEnum = pedidovendatipo.getGeracaocontareceberEnum();
			}
			if(pedidovendatipo.getRepresentacao() != null){
				representacao = pedidovendatipo.getRepresentacao();
			}
			gerarexpedicaovenda = pedidovendatipoService.gerarExpedicaoVenda(pedidovendatipo, false);
		}
		
		Boolean producao = false;
		List<Vendamaterial> listavendamaterial = venda.getListavendamaterial();
		for (Vendamaterial vendamaterial : listavendamaterial){
			if ((vendamaterial.getIsProducaoPedidovenda() == null || !vendamaterial.getIsProducaoPedidovenda()) && 
					vendamaterial.getProducaosemestoque() != null && vendamaterial.getProducaosemestoque()){
				producao = true;
				break;
			}
		}
		if (producao){
			venda.setVendasituacao(Vendasituacao.EMPRODUCAO);
		} else {
			venda.setVendasituacao(Vendasituacao.REALIZADA);
		} 
		
		if(baixaestoqueEnum != null && (
				BaixaestoqueEnum.APOS_EMISSAONOTA.equals(baixaestoqueEnum) || 
				BaixaestoqueEnum.NAO_BAIXAR.equals(baixaestoqueEnum) ||
				BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(baixaestoqueEnum) ||
				BaixaestoqueEnum.APOS_EXPEDICAOWMS.equals(baixaestoqueEnum)
				)){
			ajusteEstoque = false;
		}
		if(geracaocontareceberEnum != null && (GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(geracaocontareceberEnum) || 
											  (GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(geracaocontareceberEnum)) ||
											  (GeracaocontareceberEnum.NAO_GERAR_CONTA.equals(geracaocontareceberEnum)))){
			gerarContareceber = false;
		}
		
		Empresa empresa = empresaService.loadComContagerenciaiscompra(venda.getEmpresa());
		List<Rateioitem> listarateioitem = vendaService.prepareAndSaveVendaMaterial(venda, ajusteEstoque, empresa, false, null);
		boolean pedidoVendaReserva = venda.getPedidovenda() != null ? !pedidovendaService.havePedidovendaDiferenteReserva(venda.getPedidovenda().getCdpedidovenda().toString()) : false; 
		boolean pedidoVendaBonificacao = venda.getPedidovenda() != null ? !pedidovendaService.havePedidovendaDiferenteBonificacao(venda.getPedidovenda().getCdpedidovenda().toString()) : false;
		boolean salvarPagamentos = false;
		if (venda.getPedidovenda() != null && venda.getPedidovenda().getCdpedidovenda() != null) {
			boolean existDocumento = pedidovendapagamentoService.existeDocumento(venda.getPedidovenda());
			boolean existItemSemDocumento = pedidovendapagamentoService.existItemSemDocumento(venda.getPedidovenda());
			salvarPagamentos = SinedUtil.isListNotEmpty(venda.getListavendapagamento());
			if (gerarContareceber && existDocumento && !existItemSemDocumento){ 
				gerarContareceber = false;
			}
		}
		
		if(!pedidoVendaBonificacao && gerarContareceber){
//			venda.setValorMaterialRepresentante(vendaService.prepareAndCriaComissaoFornecedorExclusivo(venda, venda.getEmpresa(), listarateioitem));
			vendaService.prepareAndSaveReceita(venda, listarateioitem, empresa, representacao);
			vendaService.prepareAndSaveReceitaRepresentacao(venda, listarateioitem, empresa, false);
		} else {
			if((!pedidoVendaReserva && !pedidoVendaBonificacao && !gerarContareceber) || salvarPagamentos){
				vendaService.prepareAndSaveReceita(venda, listarateioitem, empresa, true);
				vendaService.prepareAndSaveReceitaRepresentacao(venda, listarateioitem, empresa, true);
			}
			//vendaService.vinculaDocumentoComissinamentoVendaPedidovenda(venda);
		}
		
		Vendahistorico vendahistorico = new Vendahistorico();
		vendahistorico.setVenda(venda);
		vendahistorico.setAcao("Aprova��o da venda");
		
		vendaService.setFaixaVendaMaterial(venda);
		vendahistoricoService.saveOrUpdate(vendahistorico);
		
		if(venda.getListavendamaterial() != null && venda.getListavendamaterial().size() > 0){
			Double valorvalecompra = venda.getValorusadovalecompra() != null ? venda.getValorusadovalecompra().getValue().doubleValue() : 0d;
			Double desconto = venda.getDesconto() != null ? venda.getDesconto().getValue().doubleValue() : 0d;
			Double valortotal = venda.getTotalvendaSemArredondamento().getValue().doubleValue();
			Double valorfrete = venda.getValorfrete() != null ? venda.getValorfrete().getValue().doubleValue() : 0d;
			
			valortotal += desconto + valorvalecompra;
			for (Vendamaterial item : venda.getListavendamaterial()) {
				vendaService.calcularMarkup(venda, item);
			}
		}
		
		vendaService.saveOrUpdate(venda);
		
		Comissionamento comissionamentoPorFaixa = comissionamentoService.findByCriterio(Criteriocomissionamento.FAIXA_MARKUP);
		
		if (comissionamentoPorFaixa != null 
				&& vendaService.existemItensComMaterialFaixaMarkup(venda.getListavendamaterial())
				&& venda.getVendasituacao().equals(Vendasituacao.REALIZADA)) {
			vendaService.gerarComissionamentoPorFaixas(venda, comissionamentoPorFaixa);
			venda.setGerouComissionamentoPorFaixas(true);
		}

		
		boolean registrarValecompra = true;
		String param_utilizacao_valecompra = parametrogeralService.getValorPorNome(Parametrogeral.UTILIZACAO_VALECOMPRA);
		if(!"VENDA".equalsIgnoreCase(param_utilizacao_valecompra)){
			registrarValecompra = false;
		}
		
		if(registrarValecompra){
			vendaService.registrarUsoValecompra(venda, false);
		}
		
		if (gerarexpedicaovenda){
			vendaService.atualizarExpedicaoSituacao(venda.getCdvenda().toString());
		}
		
		if(venda.getPedidovenda() != null){
			Pedidovenda pedidovenda = pedidovendaService.loadForEntrada(venda.getPedidovenda());
			
			boolean confirmado_total = true;
			
			for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
				Double qtdeRestante = pedidovendamaterialService.getQtdeRestante(pedidovendamaterial);
				if(qtdeRestante == null || qtdeRestante > 0){
					confirmado_total = false;
					break;
				}
			}
			
			if(confirmado_total){
				pedidovendaService.updateConfirmacao(venda.getPedidovenda().getCdpedidovenda().toString());
			}
		}
		
		if(parametrogeralService.getBoolean(Parametrogeral.RECAPAGEM)){
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()){
				if(vendamaterial.getGarantiareformaitem() != null){
					vendaService.utilizarGarantia(vendamaterial, venda);
				}
			}
		}
		vendaService.gerarReservaAoSalvar(venda, pedidovendatipo, baixaestoqueEnum, true);
		request.addMessage("Venda "+venda.getCdvenda()+" aprovada com sucesso.");
	
		Boolean closeOnSave = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnSave")) ? request.getParameter("closeOnSave") : "FALSE");
		Boolean emitirComprovante = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("emitirComprovante")) ? request.getParameter("emitirComprovante") : "TRUE");
		
		if(Vendasituacao.EMPRODUCAO.equals(venda.getVendasituacao()) && !SinedUtil.isRecapagem()){
			String retorno = request.getServletRequest().getContextPath() + "/faturamento/crud/VendaOtr?ACAO=consultar&cdvenda=" + venda.getCdvenda() + "&producaoautomaticavenda=true";
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<html><body>" +
					"<script>if("+emitirComprovante+"){window.open('../../faturamento/relatorio/Venda?ACAO=gerar&fromCriacaoVenda=true&cdvenda="+venda.getCdvenda()+"');}" +
					"window.location = '" + retorno + "';" +
					"if("+closeOnSave+"){window.close();}" +
					"</script>" +
					"</body></html>"
					);
			return null;
		}
		return new ModelAndView("redirect:/faturamento/crud/VendaOtr");
	}
	
	/**
	 * M�todo que retorna para a pgina anterior
	 * 
	 * @param request
	 * @param venda
	 * @return
	 * @author Toms Rabelo
	 */
	public ModelAndView cancelarProcess(WebRequestContext request, Venda venda){
		return new ModelAndView("redirect:/faturamento/crud/VendaOtr");
	}
	
	public void buscarMaterialCodigoAJAX(WebRequestContext request, Venda venda){	
		VendaService.getInstance().buscarMaterialCodigoAJAX(request, venda.getCodigo().toString());
	}
	
	/**
	 * M�todo ajax que verifica se o cliente possui restries sem data de liberao
	 *
	 * @param request
	 * @author Rafael Salvio
	 */
	public void ajaxVerificaRestricao(WebRequestContext request){
		restricaoService.verificaRestricaoCliente(request);
	}
	
	/**
	 * M�todo ajax que busca e retorna o ultimo valor de venda de determinado material para o cliente selecionado.
	 * 
	 * @param request
	 * @author Rafael Salvio
	 */
	public void buscarSugestaoUltimoValorVendaAJAX(WebRequestContext request){
		vendamaterialService.buscarSugestaoUltimoValorVendaAJAX(request);
	}
	
	public ModelAndView abrirPopUpCalcularLargCompAlt(WebRequestContext request){
		Produto produto = new Produto();
		
		String cdmaterial = request.getParameter("cdmaterial");
		
		if(cdmaterial != null && !"".equals(cdmaterial)){
			produto.setCdmaterial(Integer.parseInt(cdmaterial));
			produto = produtoService.carregaAlturaComprimentoLargura(produto);
		}		
		
		return new ModelAndView("direct:/crud/popup/popUpCalcularLargCompAlt", "bean", produto);
	}
	
	/**
	 * M�todo que abre popup para registrar a devoluo de material
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirRegistrarDevolucao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String origemEntrada = request.getParameter("origemEntrada");
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum iten selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
			
		String urlRedirecionamento = "/faturamento/crud/VendaOtr" + (origemEntrada != null && origemEntrada.equals("entrada") ? "?ACAO=consultar&cdvenda=" + whereIn : "");
		Venda venda = vendaService.findForRegistrarDevolucao(whereIn);		
		
		if(venda == null){
			request.addError("N�o foi poss�vel carregar as informaes para registrar devoluo.");
			SinedUtil.redirecionamento(request, urlRedirecionamento);
			return null;
		} else if(venda.getVendasituacao() == null || (!Vendasituacao.REALIZADA.equals(venda.getVendasituacao()) && !Vendasituacao.FATURADA.equals(venda.getVendasituacao()))){
			request.addError("S� � poss�vel registrar devolu��o de venda REALIZADA ou FATURADA.");
			SinedUtil.redirecionamento(request, urlRedirecionamento);
			return null;
		}
		
		if (SinedUtil.isRateioMovimentacaoEstoque() && venda.getCentrocusto() == null) {
			request.addError("O preenchimento do centro de custo � obrigat�rio na venda.");
			SinedUtil.redirecionamento(request, urlRedirecionamento);
			return null;
		}
		
		VendaDevolucaoBean bean = vendaService.criaVendaDevolucaoBean(venda);
		if(bean == null || bean.getListaItens() == null || bean.getListaItens().isEmpty()){
			request.addError("N�o existe nenhum material nesta venda que se possa fazer a devolu��o.");
			SinedUtil.redirecionamento(request, urlRedirecionamento);
			return null;
		}
		
		if(SinedUtil.isRateioMovimentacaoEstoque()){
			StringBuilder msgErroContaGerencialRateio = new StringBuilder();
			String msgErro;
			for(VendamaterialDevolucaoBean vendamaterialDevolucaoBean : bean.getListaItens()){
				if(vendamaterialDevolucaoBean.getMaterial() != null && (vendamaterialDevolucaoBean.getMaterial().getMaterialRateioEstoque() == null || 
																		vendamaterialDevolucaoBean.getMaterial().getMaterialRateioEstoque().getContaGerencialDevolucaoEntrada() == null)){
					msgErro = "� obrigat�rio informar a Conta Gerencial de Devolu��o (Entrada) no cadastro de material aba Rateio de Estoque. " + 
									(vendamaterialDevolucaoBean.getMaterial() .getNome() != null ? "(" + vendamaterialDevolucaoBean.getMaterial().getNome() + ")": "" );
					if(msgErroContaGerencialRateio.indexOf(msgErro) == -1){
						msgErroContaGerencialRateio.append(msgErro).append("<br>");
					}
				}
			}
			if(msgErroContaGerencialRateio.length() > 0){
				request.addError(msgErroContaGerencialRateio);
				SinedUtil.redirecionamento(request, urlRedirecionamento);
				return null;
			}
		}
		
		bean.setOrigemEntrada(origemEntrada);	
		bean.setUrl(urlRedirecionamento);
		request.setAttribute("obrigarnumero", venda.getEmpresa() != null && venda.getEmpresa().getIntegracaowms() != null && venda.getEmpresa().getIntegracaowms());
		request.setAttribute("existeDocuemnto", vendapagamentoService.existeDocumento(venda));
		return new ModelAndView("direct:/crud/popup/registrarDevolucao", "bean", bean);		
	}
	
	/**
	 * M�todo que faz o processo de devoluo do material
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView registrarDevolucao(WebRequestContext request, VendaDevolucaoBean bean){	
		return vendaService.registrarDevolucao(request, bean);
	}
	
	public boolean validaPagamentoAntecipado(Venda venda){
		List<Vendapagamento> listaVendapagamento = venda.getListavendapagamento();
		
		if (listaVendapagamento != null && !listaVendapagamento.isEmpty()){
			
			List<Documento> listaAntecipacao = new ArrayList<Documento>();
			Boolean isWms = empresaService.isIntegracaoWms(venda.getEmpresa());
			
			if((isWms != null && isWms) || parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_PEDIDO_ANTECIPACAO))
				listaAntecipacao = documentoService.findForAntecipacaoConsideraPedidoVenda(venda.getCliente(), null, venda.getPedidovenda());
			else
				listaAntecipacao = documentoService.findForAntecipacaoConsideraVenda(venda.getCliente(), null);
			
			Map<Integer, Double> mapaAntecipacao = new HashMap<Integer, Double>();
			
			for (Documento antecipacao : listaAntecipacao){
				mapaAntecipacao.put(antecipacao.getCddocumento(), antecipacao.getValor().getValue().doubleValue());
			}
			
			for (Vendapagamento vendapagamento : listaVendapagamento){
				if (vendapagamento.getDocumentoantecipacao() != null && vendapagamento.getDocumentoantecipacao().getCddocumento() != null){
					Double valor = mapaAntecipacao.get(vendapagamento.getDocumentoantecipacao().getCddocumento());
					if (valor != null &&  vendapagamento.getValororiginal() != null){
						valor = valor - vendapagamento.getValororiginal().getValue().doubleValue();
						if (valor < 0){
							return false;
						} else {
							mapaAntecipacao.put(vendapagamento.getDocumentoantecipacao().getCddocumento(), valor);
						}
					}
				}
			}			
		}		
		
		return true;
	}
	
	public ModelAndView gerarOrdemServico(WebRequestContext request){
		String whereIn = request.getParameter("cdvenda");
		if(vendaService.haveVendaSituacao(whereIn, false, Vendasituacao.CANCELADA, Vendasituacao.FATURADA)){
			request.addError("N�o � poss�vel gerar uma ordem de servi�o para uma venda com a situa��o 'CANCELADA' ou 'FATURADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Integer cdvenda = Integer.parseInt(request.getParameter("cdvenda"));
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>window.open('../../servicointerno/crud/Requisicao?ACAO=" + CRIAR + "&cdvenda=" + cdvenda + "','page','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=no,width=1020,height=800');parent.$.akModalRemove(true);</script>");
		return null;
	}
	
	public ModelAndView abrirGerarNotaFiscalExpedicao(WebRequestContext request, Venda venda){
		if(vendaService.haveVendaSituacao(venda.getCdvenda().toString(), false, Vendasituacao.CANCELADA, Vendasituacao.FATURADA)){
			request.addError("N�o � poss�vel gerar nota fiscal de expedi��o para uma venda com a situa��o 'CANCELADA' ou 'FATURADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Expedicaoitem> listaExpedicaoitem = expedicaoitemService.findByVendaNotFaturada(venda);
		
		if(listaExpedicaoitem == null || listaExpedicaoitem.size() == 0){
			request.addError("N�o � poss�vel gerar nota fiscal de expedi��o, n�o foi encontrado nenhuma expedi��o n�o faturada.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		request.setAttribute("cdvenda", venda.getCdvenda());
		request.setAttribute("listaExpedicaoitem", listaExpedicaoitem);
		return new ModelAndView("direct:/crud/popup/confirmacaoGerarNotaFiscalExpedicao");
	}
	
	/**
	 * M�todo que ajax que busca as contas relacionados a empresa
	 *
	 * @param request
	 * @param empresa
	 * @author Luiz Fernando
	 */
	public void ajaxCarregarContaboleto(WebRequestContext request, Empresa empresa){
		List<Conta> listaConta = contaService.findContasAtivasComPermissao(empresa, true);	
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaConta, "listaConta", ""));
	}
	
	/**
	 * M�todo que redireciona para a opo de emitir boleto ou carn
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView emitirBoletoOuCarne(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum iten selecionado.");
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdvenda=" + request.getParameter("selectedItens") : ""));
		}
		
		if("BOLETO".equals(parametrogeralService.getValorPorNome(Parametrogeral.EMITIR_BOLETO))){
			return new ModelAndView("redirect:/financeiro/relatorio/Boleto?ACAO=gerar&fromVenda=true&selectedItens=" + whereIn);
		}else {
			List<Documento> listadocumento = contareceberService.findByVendasForBoleto(whereIn);
			if(listadocumento == null || listadocumento.size() == 0){ 
				request.addError("Nenhum documento encontrado a partir da(s) venda(s) selecionada(s).");
				return new ModelAndView("redirect:/faturamento/crud/VendaOtr" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdvenda=" + request.getParameter("selectedItens") : ""));
			}
			whereIn = CollectionsUtil.listAndConcatenate(listadocumento, "cddocumento", ",");			
			return new ModelAndView("redirect:/financeiro/relatorio/Carne?ACAO=gerar&fromVenda=true&selectedItens=" + whereIn);
		}
	}
	
	
	public ModelAndView ajaxValidaBoletosNaoGerar(WebRequestContext request){
		JsonModelAndView retorno = new JsonModelAndView();
		if("BOLETO".equals(parametrogeralService.getValorPorNome(Parametrogeral.EMITIR_BOLETO))){
			String whereIn = SinedUtil.getItensSelecionados(request);
			List<Documento> listadocumento = contareceberService.findByVendasForBoleto(whereIn);
			if(listadocumento == null || listadocumento.size() == 0){ 
				retorno.addObject("nenhumDocTipoBoleto", true);
				return retorno;
			}
			whereIn = CollectionsUtil.listAndConcatenate(listadocumento, "cddocumento", ",");
			
			return contareceberService.retornaJsonParaValidarEmissaoDeBoletos(whereIn);
		}
 
		return retorno;
	}
	
	/**
	 * M�todo que redireciona para opo de negociar as contas a receber vinculadas
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/08/2014
	 */
	public ModelAndView negociarContareceber(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum iten selecionado.");
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdvenda=" + request.getParameter("selectedItens") : ""));
		}
		
		if(vendaService.existeVendaRepresentacao(whereIn)){ 
			request.addError("N�o � poss�vel utilizar as a��es de Gerar nota, Gerar boleto e Negociar de venda de representa��o.");
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdvenda=" + request.getParameter("selectedItens") : ""));
		}
		
		if(vendaService.existeVendaSemDocumento(whereIn)){ 
			request.addError("N�o foi(ram) gerada(s) contas a receber a partir de venda.");
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdvenda=" + request.getParameter("selectedItens") : ""));
		}
		
		List<Documento> listadocumento = contareceberService.findByVendasForNegociar(whereIn);
		if(listadocumento == null || listadocumento.size() == 0){ 
			request.addError("Nenhum documento encontrado a partir da(s) venda(s) selecionada(s).");
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdvenda=" + request.getParameter("selectedItens") : ""));
		}
		
		String whereInDocumento = CollectionsUtil.listAndConcatenate(listadocumento, "cddocumento", ",");		
		return new ModelAndView("redirect:/financeiro/process/FluxoStatusConta?ACAO=negociar&controller=/faturamento/crud/Venda&selectedItens=" + whereInDocumento + "&cddocumentoacao=107&backAction=Venda");
	}
	
	/**
	 * M�todo que verifica se o desconto concebido � maior que o desconto do prazo de pagamento.
	 *
	 * @name verificaDesconto
	 * @param request
	 * @return
	 * @return ModelAndView
	 * @author Thiago Augusto
	 * @date 12/06/2012
	 *
	 */
	public ModelAndView ajaxVerificaDesconto(WebRequestContext request, Venda venda){
		return vendaService.ajaxVerificaDescontoByPrazopagamento(venda.getPrazopagamento(), venda.getPercentualdesconto());
	}
	
	/**
	 * Verifica se a empresa da venda possui um comprovante configurvel definido 
	 * @param request
	 * @param venda
	 * @return
	 * @since 25/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarCsv(WebRequestContext request, VendaFiltro filtro){
		List<Venda> listaVenda = vendaService.findForCsv(filtro);
		
		StringBuilder rel = new StringBuilder();
		
		rel.append("Cdigo;");
		rel.append("Empresa;");
		rel.append("Cliente;");
		rel.append("CPF/CNPJ;");
		rel.append("Local de entrega;");
		rel.append("Data;");
		rel.append("Quantidade total;");
		rel.append("Valor total;");
		rel.append("Situao;");
		rel.append("Indicao;");
		rel.append("Criado por;");
		rel.append("Histrico;");
		rel.append("\n");
		
		String obsvendahistorico;
		for (Venda venda : listaVenda) {
			rel.append(venda.getCdvenda());
			rel.append(";");
			
			if(venda.getEmpresa() != null) rel.append(SinedUtil.escapeCsv(venda.getEmpresa().getRazaosocialOuNome()));
			rel.append(";");
			
			if(venda.getCliente() != null) rel.append(SinedUtil.escapeCsv(venda.getCliente().getNome()));
			rel.append(";");
			
			if(venda.getCliente() != null) {
				if(venda.getCliente().getCnpj() != null){
					rel.append(venda.getCliente().getCnpj());
				} else if(venda.getCliente().getCpf() != null){
					rel.append(venda.getCliente().getCpf());
				}
			}
			rel.append(";");
			
			if(venda.getLocalarmazenagem() != null) rel.append(SinedUtil.escapeCsv(venda.getLocalarmazenagem().getNome()));
			rel.append(";");
			
			if(venda.getDtvenda() != null) rel.append(SinedDateUtils.toString(venda.getDtvenda()));
			rel.append(";");
			
			Double totalqtde = venda.getTotalquantidades();
			Money totalvenda = venda.getTotalvenda();
			
			if(totalqtde != null) rel.append(new Money(totalqtde));
			rel.append(";");
			
			if(totalvenda != null) rel.append(totalvenda);
			rel.append(";");
			
			if(venda.getVendasituacao() != null) rel.append(SinedUtil.escapeCsv(venda.getVendasituacao().getDescricao()));
			rel.append(";");
			
			if(venda.getClienteindicacao() != null) rel.append(SinedUtil.escapeCsv(venda.getClienteindicacao().getNome()));
			rel.append(";");
			
			if(venda.getColaborador() != null) rel.append(SinedUtil.escapeCsv(venda.getColaborador().getNome()));
			rel.append(";");

			if(venda.getListavendahistorico() != null && !venda.getListavendahistorico().isEmpty()){
				obsvendahistorico = venda.getListavendahistorico().get(0).getObservacao();
				if(obsvendahistorico != null && !"".equals(obsvendahistorico)){
					if("DEVOLUO DE MATERIAL".equalsIgnoreCase(venda.getListavendahistorico().get(0).getAcao())){
						obsvendahistorico = "Devoluo de material " + 
									obsvendahistorico.substring(obsvendahistorico.indexOf("(")+1,obsvendahistorico.indexOf(")")) ;
					}else if(obsvendahistorico.indexOf("<a href=") > 0){
						obsvendahistorico = obsvendahistorico.substring(0,obsvendahistorico.indexOf("<a href=")) + 
									obsvendahistorico.substring(obsvendahistorico.indexOf(">")+1,obsvendahistorico.indexOf("</a>"));
					}
					rel.append("\"" + obsvendahistorico + "\"");
				}
			}
			rel.append(";");
			
			
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","venda_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
		/**
	 * Verifica se a empresa da venda possui um comprovante configurvel definido 
	 * @param request
	 * @param venda
	 * @return
	 */
	public ModelAndView possuiComprovanteConfiguravel(WebRequestContext request, Venda venda){
		Empresa empresa = venda.getEmpresa();
		
		if(empresa == null){
			venda = vendaService.load(venda);
			empresa = venda.getEmpresa();
		}
		
		boolean possui = empresaService.possuiComprovanteConfiguravel(ComprovanteConfiguravel.TipoComprovante.VENDA, empresa );
		return new JsonModelAndView().addObject("possui", possui);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirJustificativaCancelamentoVenda(WebRequestContext request){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(!vendaService.validateAbrirJustificativaCancelamentoVenda(request, whereIn)){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		Venda venda = new Venda();
		venda.setIds(whereIn);		
		request.setAttribute("descricao", "CANCELAR");
		request.setAttribute("msg", "Justificativa para o cancelamento");
		
		return new ModelAndView("direct:/crud/popup/vendaJustificativacancelar")
								.addObject("venda", venda);
	}
	
	/**
	 * Action que realiza a gera��o da expedi��o a partir da(s) venda(s).
	 *
	 * @see br.com.linkcom.sined.geral.service.VendaService#haveVendaNotInExpedicaoSituacao(String whereIn, Expedicaovendasituacao... expedicaovendasituacao)
	 *
	 * @param request
	 * @return
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarExpedicao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean notValido = vendaService.haveVendaNotInExpedicaoSituacao(whereIn, Expedicaovendasituacao.EXPEDICAO_PENDENTE, Expedicaovendasituacao.EXPEDICAO_PARCIAL);
		if(notValido){
			request.addError("Para gerar expedi��o a expedi��o tem que estar 'PENDENTE' ou 'PARCIAL'.");
			return sendRedirectToAction("listagem");
		}
		
		notValido = vendaService.haveVendaSituacao(whereIn, false, Vendasituacao.AGUARDANDO_APROVACAO, Vendasituacao.CANCELADA);
		if(notValido){
			request.addError("Para gerar expedi��o a venda tem que estar na situa��o 'REALIZADA', 'EM PRODU��O' ou 'FATURADA'.");
			return sendRedirectToAction("listagem");
		}
		
		List<Venda> listaVendaPedidocomodata = vendaService.findPedidovendacomodato(whereIn);
		List<Venda> listaVendaPedidovendaorigem = vendaService.findPedidovendaorigem(whereIn);
		boolean erro = false;
		if(listaVendaPedidocomodata != null && !listaVendaPedidocomodata.isEmpty()){
			boolean existvendapedidovendacomodato = false;
			for(Venda venda : listaVendaPedidocomodata){
				existvendapedidovendacomodato = false;
				if(listaVendaPedidovendaorigem != null && !listaVendaPedidovendaorigem.isEmpty()){
					for(Venda venda2 : listaVendaPedidovendaorigem){
						if(venda.getPedidovenda().getPedidovendaorigem().getCdpedidovenda().equals(venda2.getPedidovenda().getCdpedidovenda())){
							existvendapedidovendacomodato = true;
							break;
						}
					}
				}
				
				if(!existvendapedidovendacomodato){
					erro = true;
					request.addError("A venda " + venda.getCdvenda() + " foi criada a partir de um pedido de comodato, " +
							"portanto a venda criada a partir do pedido de origem " + venda.getPedidovenda().getPedidovendaorigem().getCdpedidovenda() + 
							" deve pertencer � mesma expedi��o.");
				}
			}
		}else if(listaVendaPedidovendaorigem != null && !listaVendaPedidovendaorigem.isEmpty()){
			erro = true;
			for(Venda venda : listaVendaPedidovendaorigem){
				request.addError("A venda " + venda.getCdvenda() + " foi criada a partir de um pedido informado " +
						"como pedido de origem. Portanto, a venda originada do pedido " + venda.getPedidovenda().getCdpedidovenda() + " deve pertencer � mesma expedi��o.");
			}
		}
		
		if(erro){
			return sendRedirectToAction("listagem");
		}
		
		return new ModelAndView("redirect:/faturamento/crud/Expedicao?ACAO=criar&vendas=" + whereIn);
	}
	
	/**
	 * M�todo ajax que verifica se o cliente tem dbito(s) acima do permitido
	 *
	 * @see br.com.linkcom.sined.geral.service.ClienteService#carregarTaxapedidovendaCreditolimitecompraByCliente(Cliente cliente)
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxValordebitoContasEmAbertoByCliente(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxValordebitoContasEmAbertoByCliente(request, cliente);
	}
	
	/**
	 * Action em ajax para buscar as sugest�es dos materiais inclu�dos no pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView buscarSugestoesVendaAJAX(WebRequestContext request, Venda venda){
		String codigos = request.getParameter("codigos");
		List<Sugestaovenda> listaSugestoes = materialvendaService.findForSugestao(codigos);

		Material material;
		if(listaSugestoes != null && !listaSugestoes.isEmpty()){
			for(Sugestaovenda sugestaovenda : listaSugestoes){
				if(sugestaovenda.getCdmaterial() != null){
					material = new Material(sugestaovenda.getCdmaterial());
					material.setUnidademedida(new Unidademedida(sugestaovenda.getCdunidademedida(), null, null));
					vendaService.preencheValorVendaComTabelaPreco(material, venda.getMaterialtabelapreco(), venda.getPedidovendatipo(), venda.getCliente(), venda.getEmpresa(), material.getUnidademedida(), venda.getPrazopagamento());
					if(material != null){
						if(material.getValorvenda() != null){
							sugestaovenda.setValor(material.getValorvenda());
						}
						if(material.getValorcusto() != null){
							sugestaovenda.setValorcustomaterial(material.getValorcusto());
						}
						if(material.getValorvenda() != null){
							sugestaovenda.setValorvendamaterial(material.getValorvenda());
						}
						if(material.getValorvendamaximo() != null){
							sugestaovenda.setValorvendamaximo(material.getValorvendamaximo());
						}
						if(material.getValorvendaminimo() != null){
							sugestaovenda.setValorvendaminimo(material.getValorvendaminimo());
						}
					}
				}
			}
		}
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("listaSugestoes", listaSugestoes);
		return jsonModelAndView;
	}
	
	/**
	 * M�todo que abre uma popup para enviar emails
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopUpEnviarComprovante(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if (whereIn == null || "".equals(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		} else {
			Vendasituacao [] situacoes = { Vendasituacao.AGUARDANDO_APROVACAO };
			
			Boolean emitirComprovante = parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO");
			Boolean existemSelecionadosAguardandoAprovacao = vendaService.haveVendaSituacao(whereIn, false, situacoes);
			
			if(!emitirComprovante && existemSelecionadosAguardandoAprovacao) {
				request.addError("O envio e emiss�o do comprovante n�o s�o permitidos na situa��o de Aguardando Aprova��o.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		List<Venda> listaVenda = vendaService.findForEnviocomprovanteemail(whereIn);
		
		List<EnvioComprovanteDestinatarioBean> listaDestinatario = new ArrayList<EnvioComprovanteDestinatarioBean>();
		String remetente = null;
		Boolean clientesDiferentes = false;
		Boolean empresasDiferentes = false;
		Integer cdcliente_aux = null;
		Integer cdempresa_aux = null;
		
		for(Venda venda : listaVenda){
			// VERIFICA O REMETENTE DO E-MAIL
			if(remetente == null){
				remetente = venda.getColaborador().getEmail();
			} else if(!remetente.equals(venda.getColaborador().getEmail())){
				remetente = "";
			}
			
			// VERIFICA SE � CLIENTE DIFERENTE
			if(cdcliente_aux == null){
				cdcliente_aux = venda.getCliente().getCdpessoa();
			} else if(!cdcliente_aux.equals(venda.getCliente().getCdpessoa())){
				clientesDiferentes = true;
			}
			
			// VERIFICA SE � EMPRESA DIFERENTE
			if(cdempresa_aux == null){
				cdempresa_aux = venda.getEmpresa().getCdpessoa();
			} else if(!cdempresa_aux.equals(venda.getEmpresa().getCdpessoa())){
				empresasDiferentes = true;
			}
			
			// INCLUI O E-MAIL DO CLIENTE
			EnvioComprovanteDestinatarioBean envioComprovanteDestinatario = new EnvioComprovanteDestinatarioBean();
			envioComprovanteDestinatario.setId(venda.getCdvenda());
			envioComprovanteDestinatario.setCdpessoa(venda.getCliente().getCdpessoa());
			envioComprovanteDestinatario.setNome(venda.getCliente().getNome());
			envioComprovanteDestinatario.setEmail(venda.getCliente().getEmail());
			listaDestinatario.add(envioComprovanteDestinatario);
			
			// INCLUI OS CONTATOS DO CLIENTE
			if(venda.getCliente().getListaContato() != null && !venda.getCliente().getListaContato().isEmpty()){
				for (PessoaContato pessoaContato : venda.getCliente().getListaContato()) {
					Contato c = pessoaContato.getContato();
					
					if (c.getEmailcontato() != null && !"".equals(c.getEmailcontato())) {
						EnvioComprovanteDestinatarioBean envioComprovanteDestinatarioContato = new EnvioComprovanteDestinatarioBean();
						envioComprovanteDestinatarioContato.setId(venda.getCdvenda());
						envioComprovanteDestinatarioContato.setCdpessoa(c.getCdpessoa());
						envioComprovanteDestinatarioContato.setNome((StringUtils.isNotBlank(c.getNome()) ? c.getNome() : "") + " " + (StringUtils.isNotBlank(venda.getCliente().getNome()) ? "(" + venda.getCliente().getNome() + ")" : ""));
						envioComprovanteDestinatarioContato.setEmail(c.getEmailcontato());
						listaDestinatario.add(envioComprovanteDestinatarioContato);
					}
				}
			}
		}
		
		String mensagem = "";
		if (clientesDiferentes) {
			mensagem += "Caro,\n\n";
		} else {
			mensagem += "Caro " + listaVenda.get(0).getCliente().getNome() + ",\n\n";
		}
		mensagem += "Envio em anexo o comprovante para seu conhecimento.\n\n";
		if (!empresasDiferentes) {
			mensagem += listaVenda.get(0).getEmpresa().getNomefantasia();
		}
		
		Enviocomprovanteemail enviocomprovanteemail = new Enviocomprovanteemail();
		enviocomprovanteemail.setRemetente(remetente);
		enviocomprovanteemail.setAssunto("Comprovante de Venda");
		enviocomprovanteemail.setEmail(mensagem);
		enviocomprovanteemail.setListaDestinatarios(listaDestinatario);
		enviocomprovanteemail.setWhereIn(whereIn);
		enviocomprovanteemail.setEntrada(request.getParameter("fromentrada"));
		enviocomprovanteemail.setPossibilidadeAvulso(listaVenda.size() == 1);
		request.setAttribute("labelId", "Venda");
		
		return new ModelAndView("direct:/crud/popup/popUpEnviocomprovanteemail", "bean", enviocomprovanteemail);
	}
	
	/**
	 * M�todo para enviar comprovante de venda para o cliente e, caso o material
	 * possua fornecedor exclusivo, envia para o fornecedor tambm
	 * 
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public void enviarComprovante(WebRequestContext request, Enviocomprovanteemail enviocomprovanteemail) {
		String fromentrada = enviocomprovanteemail.getEntrada();
		
		if(enviocomprovanteemail != null){
			Map<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>> map = vendaService.getMapEnvioComprovante(enviocomprovanteemail);
			
			int sucesso = 0;
			int erro = 0;
			int vendacancelada = 0; 
			List<EnvioComprovanteAnexoBean> listaAnexo = enviocomprovanteemail.getListaArquivos();
			Set<Entry<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>>> entrySet = map.entrySet();
			List<Venda> comprovantesEnviadosComSucesso = new ArrayList<Venda>();
			for (Entry<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>> entry : entrySet) {
				EnvioComprovanteBean comproanteBean = entry.getKey();
				List<EnvioComprovanteDestinatarioBean> listaDestinatario = entry.getValue();
				
				if(comproanteBean.getId() == null) continue;
				
				Venda venda = vendaService.loadVenda(comproanteBean.getId());
				
				try {
					if(venda.getVendasituacao() != null && Vendasituacao.CANCELADA.equals(venda.getVendasituacao())){
						vendacancelada++;
					} else {
						Usuario usuario = SinedUtil.getUsuarioLogado();
						if(venda.getColaborador() != null && venda.getColaborador().getCdpessoa() != null){
							usuario = new Usuario(venda.getColaborador().getCdpessoa());
						}
						usuario = usuarioService.load(usuario, "usuario.cdpessoa, usuario.email");
						
						ComprovanteConfiguravel comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(ComprovanteConfiguravel.TipoComprovante.VENDA, venda.getEmpresa());
						Resource resource = vendaService.criaComprovantevendaForEnviaremail(venda, comprovante);
						
						Empresa empresa = empresaService.loadComRepresentantes(venda.getEmpresa());
						List<Vendamaterial> listaVendamaterial = vendamaterialService.findForComissaoRepresentante(venda);
						
						if(listaDestinatario != null && listaDestinatario.size() > 0){
							for(EnvioComprovanteDestinatarioBean comprovanteDestinatarioBean : listaDestinatario){
								try {
									EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
									email
										.setFrom(enviocomprovanteemail.getRemetente())
										.setTo(comprovanteDestinatarioBean.getEmail())
										.setSubject(enviocomprovanteemail.getAssunto())
										.attachFileUsingByteArray(resource.getContents(), resource.getFileName(), resource.getContentType(), resource.getFileName())
										.addHtmlText(enviocomprovanteemail.getEmail().replace("\n", "<br>") + usuarioService.addImagemassinaturaemailUsuario(usuario, email));
									
									if(listaAnexo != null && listaAnexo.size() > 0){
										Integer id = 1;
										for(EnvioComprovanteAnexoBean anexoBean : listaAnexo){
											if(anexoBean.getArquivo() != null && anexoBean.getArquivo().getContent() != null && anexoBean.getArquivo().getContent().length > 0){
												email.attachFileUsingByteArray(anexoBean.getArquivo().getContent(), anexoBean.getArquivo().getNome(), anexoBean.getArquivo().getTipoconteudo(), id.toString());
												id++;							
											}
										}
									}
									envioemailService.registrarEnvio(
											enviocomprovanteemail.getRemetente(), 
											enviocomprovanteemail.getAssunto(), 
											enviocomprovanteemail.getEmail().replace("\n", "<br>"), 
											Envioemailtipo.COMPROVANTE_VENDA,
											new Pessoa(comprovanteDestinatarioBean.getCdpessoa()), 
											comprovanteDestinatarioBean.getEmail(), 
											comprovanteDestinatarioBean.getNome(),
											email);
										
									email.sendMessage();

									sucesso++;
									
									if(!comprovantesEnviadosComSucesso.contains(venda)){
										comprovantesEnviadosComSucesso.add(venda);
									}
									
									// FEITO ISSO PARA NO AGARRAR O MAILTRAP
									if(SinedUtil.isAmbienteDesenvolvimento()){
										Thread.sleep(5000);
									}
								} catch (Exception e) {
									e.printStackTrace();
									request.addError("Falha no envio de e-mail da venda " + venda.getCdvenda() + ": " + e.getMessage());
									erro++;
								}
							}
						}
						
						if (empresa != null && empresa.getListaEmpresarepresentacao() != null && !empresa.getListaEmpresarepresentacao().isEmpty()) {
							for (Empresarepresentacao empresarepresentacao : empresa.getListaEmpresarepresentacao()) {
								if (empresarepresentacao.getFornecedor() != null) {
									if (vendamaterialService.isFornecedorVendamaterial(listaVendamaterial, empresarepresentacao.getFornecedor())){
										boolean sucessofornecedor = vendaService.enviaComprovanteEmailRepresentante(request, venda, empresa, empresarepresentacao.getFornecedor(), resource);
										if (sucessofornecedor) {
											sucesso++;
										} else {
											erro++;
										}
									}
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					erro++;
				}
			}
			
			if (sucesso > 0) {
				vendahistoricoService.makeHistoricoComprovanteEnviado(comprovantesEnviadosComSucesso);
				request.addMessage(sucesso + " comprovante(s) enviado(s) com sucesso.");
			}
			if (erro > 0) {
				request.addError(erro + " comprovante(s) n�o foi(ram) enviado(s).");
			}
			if (vendacancelada > 0) {
				request.addError("N�o foi poss�vel enviar comprovante(s) de " + vendacancelada + " venda(s) por estar(em) cancelado(s).");
			}
		}		
		SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOtr" + (fromentrada != null && fromentrada.equals("true") ? "?ACAO=consultar&cdvenda=" + enviocomprovanteemail.getWhereIn() : ""));
	}
	
	/**
	 * M�todo ajax que carrega o combo de materialtabelapreco
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView comboBoxMaterialtabelapreco(WebRequestContext request) {
		Venda venda = new Venda();
		String cdpessoastr = request.getParameter("clientecdpessoa");
		String cdempresastr = request.getParameter("empresacdpessoa");
		String periododtvenda = request.getParameter("periododtvenda");
		if(cdpessoastr != null && !"".equals(cdpessoastr)){
			venda.setCliente(new Cliente(Integer.parseInt(cdpessoastr)));
		}
		if(cdempresastr != null && !"".equals(cdempresastr)){
			venda.setEmpresa(new Empresa(Integer.parseInt(cdempresastr)));
		}
		if(periododtvenda != null && !"".equals(periododtvenda)){
			try {
				venda.setDtvenda(SinedDateUtils.stringToDate(periododtvenda));
			} catch (ParseException e) {}
		}
		List<Categoria> listaCategoria = null;
		if(venda.getCliente() != null){
			listaCategoria = categoriaService.findByPessoa(venda.getCliente());
		}
		List<Materialtabelapreco> lista = materialtabelaprecoService.findForComboVenda(venda.getCliente(), venda.getDtvenda() != null ? new java.sql.Date(venda.getDtvenda().getTime()) : null, venda.getEmpresa(), listaCategoria);
		return new JsonModelAndView().addObject("lista", lista);
	}
	
	/**
	 * M�todo que abre a popup para troca de material por uma material similar
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirTrocaMaterial(WebRequestContext request, Venda venda){
		return vendaService.abrirTrocaMaterial(request, venda);
	}
	
	/**
	 * M�todo que abre a popup para troca de material de coleta por uma material similar
	 *
	 * @param request
	 * @param vendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/03/2014
	 */
	public ModelAndView abrirTrocaMaterialColeta(WebRequestContext request, Vendamaterial vendamaterial){
		Material material = vendamaterial.getMaterial();
		String index = request.getParameter("index");
		
		if(material != null && material.getCdmaterial() != null && index != null && !"".equals(index)){
			material = materialService.findForTrocaMaterialsimilar(material);
			material.setIndex(index);
		}		
		
		request.getServletResponse().setContentType("text/html");
		if(material != null && material.getListaMaterialsimilar() != null && material.getListaMaterialsimilar().size() > 0){
			return new ModelAndView("direct:/crud/popup/popUpTrocamaterialColeta", "bean", material);
		}else {
			View.getCurrent().println("<script>alert('O material n�o possui materiais similares.');parent.$.akModalRemove(true);</script>");
			return null;
		}
	}
	
	/**
	 * M�todo que redireciona para a listagem de conta a receber, com as contas relacionadas a venda
	 * 
	 * @param request
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView listarContareceberVinculadaVenda(WebRequestContext request, Venda venda){
		if(venda == null){
    		throw new SinedException("Nenhum item selecionado.");    		
    	}  
		venda = vendaService.loadWithPedidovenda(venda);
    	boolean existe = documentoService.findForContasVenda(venda);
    	
    	if(existe){    		
    		return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=listagem&idVenda=" + venda.getCdvenda());
    	}
    	
    	request.addError("N�o existem contas a receber vinculadas a venda selecionada.");
    	return new ModelAndView("redirect:/faturamento/crud/VendaOtr");
	}
	
	public ModelAndView atualizaValoresTabelaprecoMateriaisAjax(WebRequestContext request, Venda venda){
		return vendaService.atualizaValoresTabelaprecoMateriaisAjax(request, venda);
	}
	
	/**
	 * M�todo que atualiza o saldo das vendas
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public ModelAndView atualizarSaldoVenda(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Integer qtdeTotalAtualizada =  0;
		try {
			List<String> listaWhereIn = SinedUtil.getListaWhereIn(whereIn, 30);
			for (String in : listaWhereIn) {
				Integer qtdeAtualizada = vendaService.atualizarSaldoVenda(request, in);
				if(qtdeAtualizada != null && qtdeAtualizada > 0){
					qtdeTotalAtualizada += qtdeAtualizada;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		
		if(qtdeTotalAtualizada > 0){ 
			request.addMessage("Saldo atualizado com sucesso.");
			request.addMessage(qtdeTotalAtualizada + " venda(s) atualizada(s) com sucesso.");
		} else {
			request.addError("N�o foi poss�vel atualizar saldo da(s) venda(s).");
		}
			
		if("true".equals(request.getParameter("entrada"))) 
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr?ACAO=consultar&cdvenda=" + whereIn);
		else
			return sendRedirectToAction("listagem");
	}
	
	/**
	 * M�todo ajax que verifica se o prazo de pagamento
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView verificaJurosPrazopagamento(WebRequestContext request){
		String cdprazopagamentostr = request.getParameter("cdprazopagamento");
		boolean existjuros = false;
		if(cdprazopagamentostr != null && !"".equals(cdprazopagamentostr)){
			try {
				Prazopagamento prazopagamento = prazopagamentoService.load(new Prazopagamento(Integer.parseInt(cdprazopagamentostr)), "prazopagamento.cdprazopagamento, prazopagamento.juros");
				if(prazopagamento != null && prazopagamento.getJuros() != null && prazopagamento.getJuros() > 0){
					existjuros = true;
				}
			} catch (Exception e) {}
		}
		return new JsonModelAndView().addObject("existjuros", existjuros);
	}
	
	/**
	 * M�todo ajax para buscar os produtos de acordo com a tabela de preo do cliente
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxBuscaMateriaisTabelaprecoByCliente(WebRequestContext request){
		return materialtabelaprecoService.buscaMateriaisTabelaprecoByCliente(request);		
	}
	
	/**
	 * Abre pop-up para o envio para o ECF
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 08/05/2013
	 */
	public ModelAndView abrirPopupBuscarCupom(WebRequestContext request){
		EnvioECFBean bean = new EnvioECFBean();
		bean.setEntrada(request.getParameter("fromentrada"));
		
		return new ModelAndView("direct:/crud/popup/popUpBuscarCupom", "bean", bean);
	}
	
	/**
	 * Envia o pedido de venda para o ECF.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/04/2013
	 */
	public void saveBuscarCupom(WebRequestContext request, EnvioECFBean bean){
		try{
			String stringConexaoBanco = SinedUtil.getStringConexaoBanco();
			Emporiumpdv emporiumpdv = emporiumpdvService.loadForIntegracaoEmporium(bean.getEmporiumpdv());
			java.sql.Date data = SinedDateUtils.currentDate();
			if(bean.getData() != null){
				data = bean.getData();
			}
			IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacaoMovimentosCupom(stringConexaoBanco, data, emporiumpdv.getEmpresa(), emporiumpdv.getCodigoemporium(), bean.getCodigocupom());
			request.addMessage("Buscando a cupom... Em alguns minutos a venda estar� dispon�vel...");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na busca de cupom: " + e.getMessage());
		}
		
		IntegracaoEmporiumUtil.util.fechaPopUpExecutaEmporium(request);
	}
	
	/**
	 * M�todo que abre popup para leitura de cdigo de barras de cheque
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#abrirPopupCodigobarrascheque(WebRequestContext request)
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopupCodigobarrascheque(WebRequestContext request){
		return documentoService.abrirPopupCodigobarrascheque(request);
	}
	
	/**
	 * M�todo que abre popup para personaliza��o de emitente de cheque
	 *
	 * @param request
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView abrirPopupEmitenteCheque(WebRequestContext request, Vendapagamento vendapagamento){
		return vendaService.abrirPopupEmitenteCheque(request, vendapagamento);
	}
	
	public ModelAndView converteUnidademedidaDevolucao(WebRequestContext request, Vendamaterial vendamaterial){
		Double qtdejadevolvida = vendamaterial.getQtdejadevolvida();
		Double quantidade = vendamaterial.getQuantidade();
		Double saldo = vendamaterial.getSaldo();
		Double preco = vendamaterial.getPreco();
		Unidademedida unidademedida = vendamaterial.getUnidademedida();
		Unidademedida unidademedidaAntiga = vendamaterial.getUnidademedidaAntiga();
		
		Material material = materialService.findListaMaterialunidademedida(vendamaterial.getMaterial());
		
		Double fracao1 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedidaAntiga);
		Double fracao2 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedida);
		
		if(fracao1 != null && fracao2 != null){
			qtdejadevolvida = (qtdejadevolvida * fracao2) / fracao1;
			quantidade = (quantidade * fracao2) / fracao1;
			saldo = (saldo * fracao2) / fracao1;
			preco = (preco / fracao2) * fracao1;
		} else {
			Double fracao = null;
			
			List<Unidademedidaconversao> listaUnidademedidaconversao = unidademedidaconversaoService.conversoesByUnidademedida(unidademedida);		
			if(listaUnidademedidaconversao != null && listaUnidademedidaconversao.size() > 0){
				for (Unidademedidaconversao item : listaUnidademedidaconversao) {
					if(item.getUnidademedida() != null && 
						item.getUnidademedida().getCdunidademedida() != null && 
						item.getFracao() != null){
						
						if(item.getUnidademedidarelacionada().equals(unidademedida)){
							fracao = item.getFracaoQtdereferencia();
							break;
						}
					}
				}
			}
		
			if(fracao != null && fracao > 0){
				qtdejadevolvida = qtdejadevolvida / fracao;
				quantidade = quantidade / fracao;
				saldo = saldo / fracao;
				preco = preco * fracao;
			}
		}
		
		return new JsonModelAndView()
					.addObject("qtdejadevolvida", SinedUtil.descriptionDecimal(qtdejadevolvida))
					.addObject("quantidade", SinedUtil.descriptionDecimal(quantidade))
					.addObject("saldo", SinedUtil.descriptionDecimal(saldo))
					.addObject("preco", SinedUtil.descriptionDecimal(preco));
	}
	
	public ModelAndView abrirGerarProducaoConferencia(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		if(vendaService.haveVendaSituacao(whereIn, false, Vendasituacao.CANCELADA, Vendasituacao.FATURADA, Vendasituacao.AGUARDANDO_APROVACAO, Vendasituacao.REALIZADA)){
			request.addError("Para gerar produ��o, a(s) venda(s) deve(m) estar com situa��o 'EM PRODU��O'.");
			return sendRedirectToAction("listagem");
		}
		
		GerarProducaoConferenciaBean bean = new GerarProducaoConferenciaBean();
		bean.setWhereInVenda(whereIn);
		
		request.setAttribute("pathAjax", "/faturamento/crud/VendaOtr");
		
		return new ModelAndView("/process/gerarProducaoConferencia", "bean", bean);
	}
	
	public ModelAndView ajaxBuscarDadosProducao(WebRequestContext request, GerarProducaoConferenciaBean bean){
		List<Vendamaterial> listaVendamaterial = vendamaterialService.findByVendaProducao(bean.getWhereInVenda());
		List<GerarProducaoConferenciaMaterialBean> listaMaterial = new ArrayList<GerarProducaoConferenciaMaterialBean>();
		
		for (Vendamaterial vendamaterial : listaVendamaterial) {
			Producaoetapa producaoetapa = vendamaterial.getMaterial().getProducaoetapa();
			Producaoagendatipo producaoagendatipo = null;
			if(vendamaterial.getVenda() != null && vendamaterial.getVenda() != null &&
					vendamaterial.getVenda().getPedidovendatipo() != null && 
					vendamaterial.getVenda().getPedidovendatipo().getProducaoagendatipo() != null){
				producaoagendatipo = vendamaterial.getVenda().getPedidovendatipo().getProducaoagendatipo();
				if(producaoetapa == null && vendamaterial.getVenda().getPedidovendatipo().getProducaoagendatipo().getProducaoetapa() != null){
					producaoetapa = vendamaterial.getVenda().getPedidovendatipo().getProducaoagendatipo().getProducaoetapa();
				}
			}
			listaMaterial.add(new GerarProducaoConferenciaMaterialBean(vendamaterial.getMaterial(), 
					vendamaterial.getQuantidade() != null ? vendamaterial.getQuantidade() : 0d, 
					vendamaterial.getVenda(),
					vendamaterial.getVenda().getCliente(),
					vendamaterial.getLargura(),
					vendamaterial.getAltura(),
					vendamaterial.getComprimento(),
					producaoetapa,
					producaoagendatipo,
					vendamaterial.getVenda().getEmpresa(),
					vendamaterial.getUnidademedida(),
					vendamaterial.getPneu(),
					null));
		}
		
		Collections.sort(listaMaterial, new Comparator<GerarProducaoConferenciaMaterialBean>(){
			public int compare(GerarProducaoConferenciaMaterialBean o1, GerarProducaoConferenciaMaterialBean o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		Integer qtdeentregas = bean.getQtdeentregas();
		Frequencia frequencia = bean.getFrequencia();
		Timestamp dtprimeiraentrega = bean.getDtprimeiraentrega();
		
		for (GerarProducaoConferenciaMaterialBean materialBean : listaMaterial) {
			Timestamp data_aux = new Timestamp(System.currentTimeMillis());
			if(dtprimeiraentrega != null) 
				data_aux.setTime(dtprimeiraentrega.getTime());
			
			List<GerarProducaoConferenciaMaterialItemBean> listaItem = new ArrayList<GerarProducaoConferenciaMaterialItemBean>();
			
			Double qtdedividida = materialBean.getQtde()/qtdeentregas;
			Double qtdetotal = 0d;
			for (int i = 0; i < qtdeentregas; i++) {
				qtdetotal += qtdedividida;
				if(qtdeentregas.equals(i+1) && !qtdetotal.equals(materialBean.getQtde())){
					qtdedividida += (materialBean.getQtde() - qtdetotal);
				}
				
				listaItem.add(new GerarProducaoConferenciaMaterialItemBean(qtdedividida, data_aux));
				
				if(frequencia != null){
					data_aux = SinedDateUtils.incrementTimestampFrequencia(data_aux, frequencia, 1);
				}
			}
			
			materialBean.setListaItem(listaItem);
		}

		request.setAttribute("listaMaterial", listaMaterial);
		request.setAttribute("qtdeentregas", qtdeentregas);
		
		return new ModelAndView("direct:/process/gerarProducaoConferenciaMateriais");
	}
	
	public ModelAndView salvarGerarProducao(WebRequestContext request, GerarProducaoConferenciaBean bean){
		
		try{
			List<GerarProducaoConferenciaMaterialBean> listaMaterial = bean.getListaMaterial();
			List<Producaoagenda> listaProducaoagenda = new ArrayList<Producaoagenda>();
			
			for (GerarProducaoConferenciaMaterialBean materialBean : listaMaterial) {
				Empresa empresa = materialBean.getEmpresa();
				Cliente cliente = materialBean.getCliente();
				Venda venda = materialBean.getVenda();
				Material material = materialBean.getMaterial();
				Producaoetapa producaoetapa = materialBean.getProducaoetapa();
				Producaoagendatipo producaoagendatipo = materialBean.getProducaoagendatipo();
				if(producaoetapa == null) producaoetapa = materialBean.getMaterial().getProducaoetapa();
				Double largura = materialBean.getLargura();
				Double altura = materialBean.getAltura();
				Double comprimento = materialBean.getComprimento();
				Pneu pneu = materialBean.getPneu();
				List<GerarProducaoConferenciaMaterialItemBean> listaItem = materialBean.getListaItem();
				
				for (GerarProducaoConferenciaMaterialItemBean itemBean : listaItem) {
					Timestamp data = itemBean.getData();
					Double qtde = itemBean.getQtde();
					
					if(qtde != null && qtde > 0 && data != null){
						Producaoagendamaterial producaoagendamaterial = new Producaoagendamaterial();
						producaoagendamaterial.setMaterial(material);
						producaoagendamaterial.setQtde(qtde);
						producaoagendamaterial.setLargura(largura);
						producaoagendamaterial.setAltura(altura);
						producaoagendamaterial.setComprimento(comprimento);
						producaoagendamaterial.setProducaoetapa(producaoetapa);
						producaoagendamaterial.setPneu(pneu);
						
						boolean achou = false;
						for (Producaoagenda producaoagenda : listaProducaoagenda) {
							Timestamp data_producaoagenda = producaoagenda.getDtentrega();
							Venda venda_producaoagenda = producaoagenda.getVenda();
							
							if(data.equals(data_producaoagenda) && venda.equals(venda_producaoagenda)){
								achou = true;
								
								producaoagenda.getListaProducaoagendamaterial().add(producaoagendamaterial);
								break;
							}
						}
						
						if(!achou){
							Producaoagenda producaoagenda = new Producaoagenda();
							producaoagenda.setEmpresa(empresa);
							producaoagenda.setCliente(cliente);
							producaoagenda.setVenda(venda);
							producaoagenda.setDtentrega(data);
							producaoagenda.setProducaoagendatipo(producaoagendatipo);
							
							Set<Producaoagendamaterial> listaProducaoagendamaterial = new ListSet<Producaoagendamaterial>(Producaoagendamaterial.class);
							listaProducaoagendamaterial.add(producaoagendamaterial);
							producaoagenda.setListaProducaoagendamaterial(listaProducaoagendamaterial);
							
							listaProducaoagenda.add(producaoagenda);
						}
					}
				}
			}
			
			Map<Venda, List<Producaoagenda>> mapVendaHistorico = new HashMap<Venda, List<Producaoagenda>>();
			
			boolean gerouProducao = false;
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				gerouProducao = true;
				
				producaoagendaService.saveOrUpdate(producaoagenda);
				
				Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
				producaoagendahistorico.setProducaoagenda(producaoagenda);
				producaoagendahistorico.setObservacao("Criado");
				producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
				
				if(mapVendaHistorico.containsKey(producaoagenda.getContrato())){
					List<Producaoagenda> list = mapVendaHistorico.get(producaoagenda.getContrato());
					list.add(producaoagenda);
					mapVendaHistorico.put(producaoagenda.getVenda(), list);
				} else {
					List<Producaoagenda> list = new ArrayList<Producaoagenda>();
					list.add(producaoagenda);
					mapVendaHistorico.put(producaoagenda.getVenda(), list);
				}
			}
			
			Set<Entry<Venda, List<Producaoagenda>>> entrySet = mapVendaHistorico.entrySet();
			
			for (Entry<Venda, List<Producaoagenda>> entry : entrySet) {
				Venda venda = entry.getKey();
				
				Vendahistorico vendahistorico = new Vendahistorico();
				vendahistorico.setAcao("Produ��o gerada");
				vendahistorico.setVenda(venda);
				vendahistorico.setObservacao("Agenda(s) de produo(es) geradas: " + producaoagendaService.makeLinkHistoricoProducaoagenda(entry.getValue()));
				
				vendahistoricoService.saveOrUpdate(vendahistorico);
				
				vendaService.updateSituacaoVenda(venda.getCdvenda().toString(), Vendasituacao.REALIZADA);
			}
			
			if(gerouProducao){
				request.addMessage("Produ��o gerada com sucesso.");
			} else {
				request.addMessage("Nenhum registro de produ��o foi gerado.", MessageType.WARN);
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o da produ��o: " + e.getMessage());
		}
		
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView comboBoxContato(WebRequestContext request, Cliente cliente) {
		return vendaService.comboBoxContato(request, cliente);
	}
	
	public ModelAndView preencherFornecedor(WebRequestContext request, Venda venda){
		
		List<Fornecedor> listaFornecedor1 = fornecedorService.findForFaturamento(venda.getEmpresa(), venda.getMaterial().getCdmaterial().toString());
		List<GenericBean> listaFornecedor = new ArrayList<GenericBean>();
		
		for (Fornecedor fornecedor: listaFornecedor1){
			listaFornecedor.add(new GenericBean(fornecedor.getCdpessoa(), fornecedor.getNome()));
		}
		
		return new JsonModelAndView().addObject("listaFornecedor", listaFornecedor);
	}
	
	public ModelAndView escolherFornecedor(WebRequestContext request, Venda venda){
		return vendaService.escolherFornecedor(request, venda);
	}
	
	/**
	 * 
	 * @param venda
	 * @param empresa
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarRepresentacao(WebRequestContext request, Venda venda){
		boolean representacao = false;
		Money comissao = new Money();
		if(venda.getEmpresa() != null){
			List<Empresarepresentacao> listaRepresentacao = empresarepresentacaoService.findByEmpresa(venda.getEmpresa());
			if(listaRepresentacao != null && listaRepresentacao.size() > 0){
				representacao = true;
				if(venda.getFornecedor() != null){
					for (Empresarepresentacao empresarepresentacao : listaRepresentacao) {
						if(empresarepresentacao != null && 
								empresarepresentacao.getComissaovenda() != null &&
								empresarepresentacao.getComissaovenda().getValue().doubleValue() > 0 &&
								empresarepresentacao.getFornecedor() != null && 
								empresarepresentacao.getFornecedor().equals(venda.getFornecedor())){
							comissao = empresarepresentacao.getComissaovenda();
							break;
						}
					}
				}
			}
		}
		return new JsonModelAndView()
					.addObject("representacao", representacao)
					.addObject("comissao", comissao);
	}
	
	public ModelAndView buscarPatrimonioitemAjax(WebRequestContext request, Vendamaterial vendamaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(vendamaterial != null && vendamaterial.getMaterial() != null){
			Integer cdpatrimonioitem = vendamaterial.getPatrimonioitem() != null ? vendamaterial.getPatrimonioitem().getCdpatrimonioitem() : null;
			List<Patrimonioitem> listaPatrimonioitem = patrimonioitemService.findByMaterial(vendamaterial.getMaterial(), cdpatrimonioitem, Patrimonioitemsituacao.DISPONIVEL);
			jsonModelAndView.addObject("listaPatrimonioitem", listaPatrimonioitem);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * 
	 * @param venda
	 * @param empresa
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarFornecedorProdutos(WebRequestContext request, Venda venda){
		
		String idsMaterial = request.getParameter("idsMaterial");
		Set<Integer> listaCdmaterial = new HashSet<Integer>();
		
		if (!idsMaterial.equals("")){
			List<Fornecedor> listaFornecedor = fornecedorService.findForFaturamento(venda.getEmpresa(), idsMaterial);
			for (Fornecedor fornecedor: listaFornecedor){
				for (Materialfornecedor materialfornecedor: fornecedor.getListaMaterialfornecedor()){
					listaCdmaterial.add(materialfornecedor.getMaterial().getCdmaterial());
				}
			}
		}
		
		return new JsonModelAndView().addObject("listaCdmaterial", listaCdmaterial);
	}
	
	/**
	 * M�todo que abre uma popup para escolha dos itens da grade
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public ModelAndView selecionarItensGrade(WebRequestContext request){
		return new ModelAndView("direct:/crud/popup/popUpSelecionarItensGrade", "bean", vendaService.selecionarItensGrade(request));
	}
	
	public ModelAndView selecionarItensGradeAjax(WebRequestContext request) {
		Material materialMestre = vendaService.selecionarItensGrade(request).getListaMaterial().get(0);
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		
		Pedidovendatipo pedidovendatipo = cdpedidovendatipo != null && !cdpedidovendatipo.equals("") ? new Pedidovendatipo(Integer.parseInt(cdpedidovendatipo)) : null;
		Parametrogeral param = parametrogeralService.findByNome("MATERIAL_MESTRE_TABELAPRECO");
		// se o parametro for true e o material mestre tiver tabela de preco vigente, os valores dos itens de grade sao atualizados
		if(Boolean.parseBoolean(param.getValor())) {
			atualizaValoresItensGradeTabelaPreco(materialMestre, request, pedidovendatipo);
		}
		
		request.setAttribute("listaMaterialitemgrade", materialMestre.getListaMaterialitemgrade());
		request.setAttribute("indexmat", request.getParameter("indexmat"));
		if(parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_DIMENSAO_ITEMGRADE)){
			 request.setAttribute("bloquearDimensao", true);
		}
		
		pedidovendatipo = pedidovendatipoService.load(pedidovendatipo);
		
		if(pedidovendatipo != null){
			if(Boolean.TRUE.equals(pedidovendatipo.getBloqQuantidaMetragem())){
				request.setAttribute("bloquearEdicaoQuandidade", true);
			}
		}
		return new ModelAndView("direct:/crud/popup/itensGradeGrid");
	}
	
	private void atualizaValoresItensGradeTabelaPreco(Material materialMestre, WebRequestContext request, Pedidovendatipo pedidovendatipo) {
		String cdcliente = request.getParameter("cdcliente");
		String cdempresa = request.getParameter("cdempresa");
		String cdprazopagamento = request.getParameter("cdprazopagamento");
		
		Cliente cliente = cdcliente != null && !cdcliente.equals("") ? new Cliente(Integer.parseInt(cdcliente)) : null;
		Empresa empresa = cdempresa != null && !cdempresa.equals("") ? new Empresa(Integer.parseInt(cdempresa)) : null; 
		Prazopagamento prazopagamento = cdprazopagamento != null && !cdprazopagamento.equals("") ? new Prazopagamento(Integer.parseInt(cdprazopagamento)) : null;
		if(pedidovendatipo != null){
			if(Boolean.TRUE.equals(pedidovendatipo.getBloqQuantidaMetragem())){
				request.setAttribute("bloqAlteracaoPreco", true);
			}
		}
		pedidovendaService.preecheValorVendaComTabelaPreco(materialMestre, pedidovendatipo, cliente, empresa, materialMestre.getUnidademedida(), prazopagamento);
		
		if(materialMestre.getTemTabelaPrecoVigenteMaterial() != null && materialMestre.getTemTabelaPrecoVigenteMaterial()) {
			Iterator<Material> it = materialMestre.getListaMaterialitemgrade().iterator();
			
			while(it.hasNext()) {
				Material material = it.next();
				material.setValorvenda(materialMestre.getValorvenda());
			}
		}
	}
	
	/**
	 * M�todo que abre a popup para escolha do novo responsvel da venda
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public ModelAndView abrirPopUpTrocarVendedor(WebRequestContext request){
		String id = request.getParameter("cdvenda");
		
		if(id == null || "".equals(id)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Venda venda = vendaService.loadForTrocarvendedor(new Venda(Integer.parseInt(id)));
		Usuario usuario = usuarioService.load(SinedUtil.getUsuarioLogado(), "usuario.cdpessoa, usuario.restricaoclientevendedor");
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		if(usuario != null && usuario.getRestricaoclientevendedor() != null && usuario.getRestricaoclientevendedor()){
			if(venda != null && venda.getCliente() != null && venda.getCliente().getListaClientevendedor() != null &&
					!venda.getCliente().getListaClientevendedor().isEmpty()){
				for(Clientevendedor clientevendedor : venda.getCliente().getListaClientevendedor()){
					if(clientevendedor.getColaborador() != null && clientevendedor.getColaborador().getCdpessoa() != null){
						listaColaborador.add(clientevendedor.getColaborador());
					}
				}
			}
		}		
		
		request.setAttribute("exibirCombo", listaColaborador != null && !listaColaborador.isEmpty());
		request.setAttribute("listaColaboador", listaColaborador);
		request.setAttribute("pathTrocavendedor", "/faturamento/crud/VendaOtr");
		request.setAttribute("nomeBeanTrocavendedor", "Venda");
		
		TrocavendedorBean trocavendedorBean = new TrocavendedorBean(venda.getCdvenda(), venda.getColaborador());
		return new ModelAndView("direct:/crud/popup/popUpTrocarVendedor", "bean", trocavendedorBean);
	}
	
	/**
	 * M�todo que salva a troca de vendedor do pedido de venda
	 *
	 * @see br.com.linkcom.sined.geral.service.VendaService#processaTrocaVendedor(Venda vendaWithColaboradorAtual)
	 *
	 *
	 * @param request
	 * @param trocavendedorBean
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void salvaTrocaVendedor(WebRequestContext request, TrocavendedorBean trocavendedorBean){
		if(trocavendedorBean.getId() == null){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		if(trocavendedorBean.getColaboradornovo() == null || trocavendedorBean.getColaboradornovo().getCdpessoa() == null){
			request.addError("Nenhum colaborador selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		Venda venda = new Venda(trocavendedorBean.getId());
		venda.setColaborador(trocavendedorBean.getColaboradornovo());
		
		vendaService.processaTrocaVendedor(venda);
		
		request.addMessage("Vendedor alterado com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOtr?ACAO=consultar&cdvenda=" + trocavendedorBean.getId());
	}
	
	/**
	 * 
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, Venda bean){
		return vendaService.ajaxCamposAdicionais(request, bean);
	}
	
	/**
	 * M�todo que busca uma lista de {@link Pedidovendatipo} e a renderiza em formato html para ser inserido na pgina.
	 * @param request
	 * @return
	 */
	public ModelAndView ajaxLoadListaCampoExtra(WebRequestContext request){
		String msgError = "<null>";
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		JsonModelAndView json = new JsonModelAndView();
		
		if(cdpedidovendatipo != null && !cdpedidovendatipo.equals("")){
			Pedidovendatipo tipo = new Pedidovendatipo();
			tipo.setCdpedidovendatipo(Integer.valueOf(cdpedidovendatipo));
			List<Campoextrapedidovendatipo> lista = campoextrapedidovendatipoService.findByTipo(tipo);
		
			StringBuilder html = new StringBuilder();
			if(lista != null && !lista.isEmpty()){
				html.append("<option value='<null>'></option>");
				for(Campoextrapedidovendatipo campo : lista){
					html.append("<option value='br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo[cdcampoextrapedidovendatipo=")
						.append(campo.getCdcampoextrapedidovendatipo().toString())
						.append("]'>")
						.append(campo.getNome())
						.append("</option>");
				}
			}
			else{
				msgError = "N�o existem dados para este tipo.";
				return json.addObject("msgError", msgError);
			}
			
			return json.addObject("html", html.toString()).addObject("msgError", msgError);
		}
		else {
			msgError = "Tipo n�o definido";
			return json.addObject("msgError", msgError);
		}
	}

	public ModelAndView ajaxBuscaObservaocaoByCliente(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaObservaocaoByCliente(request, cliente);
	}
	
	public ModelAndView verificarPopupVendaIndustrializacaoRetorno(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		String isNfe = request.getParameter("isNfe");
		String whereInVenda = request.getParameter("selectedItens");
		
		boolean notValido = vendaService.haveVendaSituacao(whereInVenda, false, Vendasituacao.AGUARDANDO_APROVACAO, Vendasituacao.CANCELADA, Vendasituacao.FATURADA);
		if(notValido){
			jsonModelAndView.addObject("erro", "Para gerar nota a venda tem que estar na situa��o 'REALIZADA' ou 'EM PRODU��O'.");
			return jsonModelAndView;
		}
		
		boolean vendaIndustrializacaoRetornoModal = true;
		vendaIndustrializacaoRetornoModal = !entregamaterialService.findForIndustrializacaoRetorno(null, whereInVenda).isEmpty();
		if(vendaIndustrializacaoRetornoModal && whereInVenda.split(",").length > 1 &&
				vendaService.existeVendaNotIndustrializacaoRetorno(whereInVenda)){
			vendaIndustrializacaoRetornoModal = false;
		}
		
		jsonModelAndView.addObject("vendaIndustrializacaoRetornoModal", vendaIndustrializacaoRetornoModal);
		jsonModelAndView.addObject("url", request.getServletRequest().getContextPath() + "/faturamento/crud/VendaOtr?ACAO=abrirPopupVendaIndustrializacaoRetorno&whereIn=" + whereInVenda + "&isNfe=" + isNfe);
		
		return jsonModelAndView;
	}	
	
	public ModelAndView abrirPopupVendaIndustrializacaoRetorno(WebRequestContext request){
		String whereInVenda = request.getParameter("whereIn");
		String isNfe = request.getParameter("isNfe");
		List<Vendamaterial> listaVendamaterial = vendamaterialService.findByVendaForIndustrializacao(whereInVenda);
		List<Entregamaterial> listaEntregamaterial = entregamaterialService.findForIndustrializacaoRetorno(null, whereInVenda);
		
		String whereInEntregamaterial = CollectionsUtil.listAndConcatenate(listaEntregamaterial, "cdentregamaterial", ",");
		List<Notafiscalprodutoitemnotaentrada> listaNotafiscalprodutoitemnotaentrada = null;
		if(whereInEntregamaterial != null && !whereInEntregamaterial.isEmpty()){
			listaNotafiscalprodutoitemnotaentrada = notafiscalprodutoitemnotaentradaService.findByEntregamaterial(whereInEntregamaterial);
		}
		
		List<Vendamaterial> listaVendamaterialAgrupado = new ArrayList<Vendamaterial>();
		for (Vendamaterial vendamaterial : listaVendamaterial) {
			boolean achou = false;
			for (Vendamaterial vendamaterialAgrupado : listaVendamaterialAgrupado) {
				if(vendamaterialAgrupado.getMaterial() != null &&
						vendamaterial.getMaterial() != null &&
						vendamaterialAgrupado.getMaterial().equals(vendamaterial.getMaterial())){
					vendamaterialAgrupado.setQuantidade(vendamaterialAgrupado.getQuantidade() + vendamaterial.getQuantidade());
					achou = true;
					break;
				}
			}
			
			if(!achou){
				listaVendamaterialAgrupado.add(vendamaterial);
			}
		}
		
		Collections.sort(listaVendamaterialAgrupado,new Comparator<Vendamaterial>(){
			public int compare(Vendamaterial o1, Vendamaterial o2) {
				return o1.getMaterial().getCdmaterial().compareTo(o2.getMaterial().getCdmaterial());
			}
		});
		
		for (Entregamaterial entregamaterial : listaEntregamaterial) {
			entregamaterial.setQtdedisponivelNotaIndustrializacao(entregamaterial.getQtde());
			for (Notafiscalprodutoitemnotaentrada notafiscalprodutoitemnotaentrada : listaNotafiscalprodutoitemnotaentrada) {
				if(notafiscalprodutoitemnotaentrada.getEntregamaterial() != null &&
						notafiscalprodutoitemnotaentrada.getEntregamaterial().getCdentregamaterial() != null &&
						entregamaterial.getCdentregamaterial() != null &&
						entregamaterial.getCdentregamaterial().equals(notafiscalprodutoitemnotaentrada.getEntregamaterial().getCdentregamaterial())){
					entregamaterial.setQtdedisponivelNotaIndustrializacao(entregamaterial.getQtdedisponivelNotaIndustrializacao() - notafiscalprodutoitemnotaentrada.getQtdereferencia());
				}
			}
			
			if(entregamaterial.getQtdedisponivelNotaIndustrializacao() <= 0d){
				entregamaterial.setMarcadoPopupNotaIndustrializacao(false);
				entregamaterial.setQtdedisponivelNotaIndustrializacao(0d);
			}
		}
		
		return new ModelAndView("direct:/crud/popup/popupEntradaFiscalIndustrializacaoRetorno")
					.addObject("whereIn", whereInVenda)
					.addObject("isNfe", isNfe)
					.addObject("listaEntregamaterial", listaEntregamaterial)
					.addObject("listaVendamaterial", listaVendamaterialAgrupado)
					;
	}
	
	/**
	* M�todo ajax que verifica as informaes do tipo de pedido
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#verificarPedidovendatipoAJAX(WebRequestContext request, Pedidovendatipo pedidovendatipo, Empresa empresa)
	*
	* @param request
	* @param venda
	* @since 11/12/2015
	* @author Luiz Fernando
	*/
	public void verificarPedidovendatipoAJAX(WebRequestContext request, Venda venda){
		boolean fromPedidovenda = request.getParameter("fromPedidovenda") != null ? Boolean.parseBoolean(request.getParameter("fromPedidovenda")) : false;
		boolean fromOrcamento = request.getParameter("fromOrcamento") != null ? Boolean.parseBoolean(request.getParameter("fromOrcamento")) : false;
		vendaService.verificarPedidovendatipoAJAX(request, venda.getPedidovendatipo(), venda.getEmpresa(), fromPedidovenda, fromOrcamento);
	}
	
	public void buscarMaterialIdentificadorTabelaprecoAJAX(WebRequestContext request, Venda venda){
		vendaService.buscarMaterialIdentificadorTabelaprecoAJAX(request, venda);
	}
	
	/**
	 * Chamada Ajax para carregamento das Formas e Prazos de Pagameneto para determinado Cliente.
	 *
	 * @param request
	 * @param cliente
	 * @author Marcos Lisboa
	 */
	public ModelAndView ajaxLoadFormaAndPrazoPagamento(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxLoadFormaAndPrazoPagamento(request, cliente);
	}
	
	/**
	 * Busca o nome do vendedor principal do cliente
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/09/2014
	 */
	public ModelAndView ajaxBuscaVendedorprincipalByCliente(WebRequestContext request, Cliente cliente){
		return clientevendedorService.actionVendedorprincipalVendaPedido(cliente);
	}
	
	/**
	 * Ajax que busca o saldo do vale compra pelo cliente
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/09/2014
	 */
	public ModelAndView ajaxBuscaSaldovalecompraByCliente(WebRequestContext request, Cliente cliente){
		JsonModelAndView json = (JsonModelAndView) valecompraService.actionSaldovalecompraVendaPedidoOrcamento(null, cliente);
		
		if (cliente.getPedidovenda() != null && pedidovendapagamentoService.existeDocumento(cliente.getPedidovenda())) {
			json.addObject("documento", Boolean.TRUE);
		}
		
		return json;
	}
	
	public void ajaxSetDadosSessaoForLoteAutocomplete(WebRequestContext request){
		pedidovendaService.ajaxSetDadosSessaoForLoteAutocomplete(request, "VENDA");
	}
	
		/**
	 * Verifica se a empresa � de representa��o
	 * @param request
	 * @param empresa
	 */
	public void ajaxVerificaRepresentacao(WebRequestContext request, Empresa empresa){
		String isRepresentacao = "";
		
		if(empresa != null){
			if(empresarepresentacaoService.isEmpresaRepresentacao(empresa)){
				isRepresentacao += "var isRepresentacao = true;";
			}else{
				isRepresentacao += "var isRepresentacao = false;";
			}
		}else{
			isRepresentacao += "var isRepresentacao = false;";
		}
		
		View.getCurrent().println(isRepresentacao);
	}
	
	/**
	 * Popula o combo de box de contato, via AJAX.
	 * 
	 * @param request
	 * @return
	 */
	public ModelAndView comboBox(WebRequestContext request) {
		String whereInCliente = request.getParameter("whereInCliente");
		List<Contato> listaContato = contatoService.findByPessoaComboWhereIn(whereInCliente, Boolean.TRUE);
		String listaCodigos = CollectionsUtil.listAndConcatenate(listaContato, "cdpessoa",",");
		String listaNomes = CollectionsUtil.listAndConcatenate(listaContato, "nome",",");
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("codigos", listaCodigos);
		json.addObject("nomes", listaNomes);
		return json;
	}
	
	/**
	 * M�todo que carrega o email de um determinado contato
	 * 
	 * @param request
	 * @param contato
	 * @return
	 */
	public void atualizaEmailContato(WebRequestContext request, Contato contato) {
		contato = contatoService.getEmailContato(contato);
		String emailcontato = "var emailcontato = ";
		if(contato != null){
			emailcontato += contato.getEmailcontato() == null ? "'';" : "'"+contato.getEmailcontato()+"';";
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(emailcontato);
	}
	
	/**
	* M�todo ajax para gerar as parcelas de representao
	*
	* @param request
	* @param venda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public ModelAndView pagamentoRepresentacao(WebRequestContext request, Venda venda){	
		AjaxRealizarVendaPagamentoRepresentacaoBean bean = vendaService.pagamentoRepresentacao(venda, venda.getEmpresa());
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", bean);
		return json;
	}
	
	/**
	* M�todo ajax para buscar o percentual de representao
	*
	* @param request
	* @param venda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxPercentualComissaoRepresentacao(WebRequestContext request, Venda venda){
		return VendaService.getInstance().ajaxPercentualComissaoRepresentacao(venda.getEmpresa(), venda.getFornecedor());
	}
	
	/**
	* M�todo que abre a popup de kit flexivel
	*
	* @param request
	* @param bean
	* @return
	* @since 31/03/2015
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpKitFlexivel(WebRequestContext request, KitflexivelBean bean){
		return vendaService.abrirPopUpKitFlexivel(request, bean);
	}
	
	public ModelAndView abrirPopUpKitFlexivelComposicao(WebRequestContext request, KitflexivelBean bean){
		return vendaService.abrirPopUpKitFlexivelComposicao(request, bean);
	}
	
	/**
	* M�todo ajax que verifica se existe lote para o material
	*
	* @param request
	* @return
	* @since 15/05/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxValidaLoteestoqueItensSync(WebRequestContext request){
		request.setAttribute("origemVenda", request.getParameter("origemVenda") != null ? request.getParameter("origemVenda") : "false");
		return vendaService.ajaxValidaLoteestoqueItensSync(request);
	}
	
	/**
	 * M�todo que abre a popup para consulta e edio da porcentagem da comisso das agncias.
	 *
	 * @param request
	 * @return
	 * @author Joo Vitor
	 * @since 02/06/2015
	 */
	public ModelAndView abrirPopUpComissaoAgencia(WebRequestContext request, Vendamaterial vendamaterial){
		return vendaService.abrirPopUpComissaoAgencia(request, vendamaterial);
	}
	
	public ModelAndView changeFornecedoragencia(WebRequestContext request, FornecedoragenciaBean fornecedoragenciaBean) {
		return vendaService.changeFornecedoragencia(request, fornecedoragenciaBean);
	}
	
	/**
	* M�todo ajax que busca o local da empresa, caso a empresa possua apenas um local
	*
	* @param request
	* @return
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxBuscaLocalArmazenagemUnico(WebRequestContext request){	
		return vendaService.ajaxBuscaLocalArmazenagemUnico(request);
	}
	
	/**
	* M�todo M�todo ue retorna o valor aproximado de imposto da venda
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#ajaxCalcularValorAproximadoImposto(WebRequestContext request, Venda venda)
	*
	* @param request
	* @param venda
	* @return
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularValorAproximadoImposto(WebRequestContext request, Venda venda){	
		return vendaService.ajaxCalcularValorAproximadoImposto(request, venda);
	}
	
	/**
	* M�todo ajax para calcular o imposto na venda
	*
	* @param request
	* @param impostovenda
	* @return
	* @since 11/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularImposto(WebRequestContext request, Impostovenda impostovenda){	
		return new JsonModelAndView().addObject("bean", vendaService.calcularImposto(impostovenda));
	}
	
	/**
	* M�todo ajax que verifica se a cria��o da venda foi finalizada ou n�o
	*
	* @param request
	* @return
	* @since 01/02/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificarVendaComCriacaoNaoFinalizada(WebRequestContext request) {
		boolean cricaonaofinalizada = false;
		try {
			String whereIn = SinedUtil.getItensSelecionados(request);
			if(StringUtils.isNotBlank(whereIn) && vendaService.existeVendaComCriacaoNaoFinalizada(whereIn)){
				cricaonaofinalizada = true;
			}
		} catch (Exception e) {}
		return new JsonModelAndView().addObject("cricaonaofinalizada", cricaonaofinalizada);
	}
	
	/**
	* M�todo ajax para calcular o peso do materia
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#ajaxCalcularPesoMaterial(WebRequestContext request, Material material)
	*
	* @param request
	* @param material
	* @return
	* @since 26/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularPesoMaterial(WebRequestContext request, Material material){
		return vendaService.ajaxCalcularPesoMaterial(request, material);
	}
	
	/**
	 * M�todo ajax que retorna uma lista JSON com os prazos de pagamentos do cliente
	 *
	 * @param request
	 * @return
	 * @since 27/05/2016
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView getFormasPrazosPagamentoClienteJSON(WebRequestContext request){
		boolean addForma = "true".equalsIgnoreCase(request.getParameter("addForma"));
		boolean addPrazo = "true".equalsIgnoreCase(request.getParameter("addPrazo"));
		return clienteService.getFormasPrazosPagamentoClienteAlertaModelAndView(request, addForma, addPrazo);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param request
	* @param empresa
	* @return
	* @since 20/06/2016
	* @author Luiz Fernando
	*/
	public ModelAndView carregaInfEmpresa(WebRequestContext request, Empresa empresa){
		return vendaService.carregaInfEmpresa(request, empresa);
	}
	
	public ModelAndView ajaxRecalcularValoresBanda(WebRequestContext request, Venda venda){
		return vendaService.ajaxRecalcularValoresBanda(request, venda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#abrirPopUpInfoFinanceiraCliente(WebRequestContext request, Cliente cliente)
	*
	* @param request
	* @param cliente
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpInfoFinanceiraCliente(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInfoFinanceiraCliente(request, cliente);
	}
 	
	public ModelAndView ajaxBuscaInfPrazo(WebRequestContext request, Prazopagamento prazopagamento){
		return vendaService.ajaxBuscaInfPrazo(request, prazopagamento);
	}
	
	public ModelAndView buscaPrazoEntrega(WebRequestContext request, Venda venda){
		return vendaService.buscaPrazoEntrega(request, venda);
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView comboBoxTerceiro(WebRequestContext request, Venda venda) {
		return vendaService.comboBoxTerceiro(request, venda);
	}
	
	public ModelAndView ajaxTemplatesEstiquetaDisponiveis(WebRequestContext request){
		return vendaService.ajaxTemplatesEstiquetaDisponiveis(request);
	}
	
	public ModelAndView abrirSelecaoTemplateEtiquetaDestinatarioRemetente(WebRequestContext request){
		return vendaService.abrirSelecaoTemplateEtiquetaDestinatarioRemetente(request);
	}
	
	public ModelAndView emitirEtiquetaDestinatarioRemetente(WebRequestContext request){
		return vendaService.emitirEtiquetaDestinatarioRemetente(request); 
	}
	
	public ModelAndView abrirPopUpInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInformacaoClienteVenda(request, cliente);
	}
	
	public void ajaxSugerirPrazopagamentoCliente(WebRequestContext request){
		vendaService.ajaxSugerirPrazopagamentoCliente(request);
	}
	
	public ModelAndView ajaxHabilitaCamposDeACordoComGrupoMaterial(WebRequestContext request, Venda bean){
		return materialService.ajaxHabilitaCamposDeACordoComGrupoMaterial(bean.getMaterial());
	}
	
	public ModelAndView ajaxClientePossuiInformacaovenda(WebRequestContext request) throws Exception {
		boolean retorno = false;
		
		if (parametrogeralService.getBoolean(Parametrogeral.ALERTAR_INF_CLIENTEVENDA)){
			for (Cliente cliente: clienteService.findWhereInCdvenda(request.getParameter("selectedItens"))){
				if (cliente.getInformacaovenda()!=null && !cliente.getInformacaovenda().trim().equals("")){
					retorno = true;
					break;
				}
			}
		}
		
		return new JsonModelAndView().addObject("retorno", retorno);
	}
	
	public ModelAndView ajaxBuscaVendedorPrincipal(WebRequestContext request, Venda bean){
		return clientevendedorService.ajaxBuscaVendedorPrincipal(request, bean.getCliente());
	}
	
	public ModelAndView abrePopupAjudaVendedorprincipal(WebRequestContext request){
		return vendaService.abrePopupAjudaVendedorprincipal(request);
	}
	
	public ModelAndView ajaxObrigarProjeto(WebRequestContext request, Venda venda){
		boolean obrigar = false;
		if (venda.getPedidovendatipo()!=null && venda.getPedidovendatipo().getCdpedidovendatipo()!=null){
			obrigar = Boolean.TRUE.equals(pedidovendatipoService.load(venda.getPedidovendatipo()).getObrigarProjeto());
		}
		return new JsonModelAndView().addObject("obrigar", obrigar);
	}
	
	public ModelAndView msgNotaItensColeta(WebRequestContext request, VendaFiltro filtro) throws Exception {
		request.addError("J� foram emitidas notas de retorno para todos os itens da venda.");
		return new ModelAndView("redirect:/faturamento/crud/VendaOtr?ACAO=listagem");
	}
	
	public ModelAndView ajaxExistsGarantiareforma(WebRequestContext request, Cliente cliente){
		return garantiareformaService.ajaxExistsGarantiareforma(request, cliente);
	}
	
	public ModelAndView abrePopupSelecaoGarantiareformaCliente(WebRequestContext request, Cliente cliente){
		return garantiareformaService.abrePopupSelecaoGarantiareformaCliente(request, cliente);
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Venda form)
			throws CrudException {
		String whereInOportunidade = request.getParameter("whereInOportunidade");
		if(Util.strings.isNotEmpty(whereInOportunidade) &&
			!oportunidadeService.isUsuarioPodeEditarOportunidades(whereInOportunidade)){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.");
			String urlRedirecionamento = "redirect:/crm/crud/Oportunidade";
			if("entrada".equals(request.getParameter("controller"))){
				urlRedirecionamento +=  "?ACAO=consultar&cdoportunidade="+whereInOportunidade;
			}
			return new ModelAndView(urlRedirecionamento);
		}
		
		return super.doCriar(request, form);
	}
	
	public void criarPagamentosVendaSemPagamento(WebRequestContext request){
		//processo utilizado somente para ajustar venda que n�o tem conta a receber
		String whereInVenda = request.getParameter("whereInVendaAjuste");
		String senha = request.getParameter("hash");
		
		String hash1 = Util.crypto.makeHashMd5("linkComAdminAjustaVenda");
		String hash2 = Util.crypto.makeHashMd5(hash1);
		
		if(hash2.equals(senha)){
		
			for(String idVenda : whereInVenda.split(",")){
				Venda form = vendaService.loadForEntrada(new Venda(Integer.parseInt(idVenda)));
				
				if(form.getPedidovendatipo() != null && (Boolean.TRUE.equals(form.getPedidovendatipo().getBonificacao()) ||
						(!GeracaocontareceberEnum.APOS_VENDAREALIZADA.equals(form.getPedidovendatipo().getGeracaocontareceberEnum()) &&
						!GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(form.getPedidovendatipo().getGeracaocontareceberEnum())))){
					continue;
				}
						
				form.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(form));
				
				if(form.getListavendapagamento() == null || form.getListavendapagamento().size() == 0) continue;
				boolean existeDoc = false;
				for(Vendapagamento vendapagamento : form.getListavendapagamento()){
					if(vendapagamento.getDocumento() != null || vendapagamento.getDocumentoantecipacao() != null){
						existeDoc = true;
						break;
					}
				}
				if(existeDoc) continue;
				
				if (form.getColaborador()!=null && form.getColaborador().getCdpessoa()!=null){
					form.setColaborador(colaboradorService.loadWithoutPermissao(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
				}
				if (form.getFornecedorAgencia()!=null && form.getFornecedorAgencia().getCdpessoa()!=null){
					form.setFornecedorAgencia(fornecedorService.load(form.getFornecedorAgencia(), "fornecedor.cdpessoa, fornecedor.nome"));
				}
				if (form.getLocalarmazenagem()!=null && form.getLocalarmazenagem().getCdlocalarmazenagem()!=null){
					form.setLocalarmazenagem(localarmazenagemService.load(form.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
				}
				if (form.getCliente()!=null && form.getCliente().getCdpessoa()!=null){
					form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome"));
					
					if(form.getContato() != null && form.getContato().getCdpessoa() != null){
						form.setContato(contatoService.load(form.getContato(), "contato.cdpessoa, contato.nome"));
					}
				}
				if (form.getEmpresa() !=null && form.getEmpresa().getCdpessoa()!=null){
					form.setEmpresa(empresaService.load(form.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia"));
				}
				
				form.setListavendapagamentorepresentacao(vendapagamentorepresentacaoService.findVendaPagamentoRepresentacaoByVenda(form));
				form.setListavendamaterial(vendamaterialService.findByVenda(form));
				form.setListavendahistorico(vendahistoricoService.findByVenda(form));
				form.setListaVendamaterialmestre(vendamaterialmestreService.findByVenda(form));
				
				Empresa empresa = empresaService.loadComContagerenciaiscompra(form.getEmpresa());
				List<Rateioitem> listarateioitem = vendaService.prepareAndSaveVendaMaterial(form, false, empresa, false, null);
				vendaService.prepareAndSaveReceita(form, listarateioitem, empresa, false);
			}
		}
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOtr");
	}
	
	public ModelAndView recuperarVendasPorMaterial(WebRequestContext request){
			return vendaService.recuperarListaReservados(request);
	}
	
	public ModelAndView buscarPedidoVendaTipoAjax(WebRequestContext request, Venda venda) {
		return pedidovendatipoService.buscarPedidoVendaTipoAjax(venda.getPedidovendatipo());
	}
	
	public ModelAndView verificarPedidoVendaTipoBloqueioQuantidade(WebRequestContext request) {
		return pedidovendatipoService.verificarPedidoVendaTipoBloqueioQuantidade(request);
	}

	public ModelAndView verificarPedidoVendaTipoBloqueioPreco(WebRequestContext request) {
		return pedidovendatipoService.verificarPedidoVendaTipoBloqueioPreco(request);
	}
	
	public ModelAndView ajaxBuscaFaixaMarkup(WebRequestContext request, Venda venda){
		return materialFaixaMarkupService.findJsonFaixaMarkup(venda.getMaterial(), venda.getUnidademedida(), venda.getEmpresa());
	}
	
	public ModelAndView ajaxAbrePopupHistoricoPrecoMaterial(WebRequestContext request, MaterialHistoricoPrecoFiltro filtro){
		return materialService.ajaxAbrePopupHistoricoPrecoMaterial(request, filtro);
	}
	
	public ModelAndView filtrarHistoricoPrecos(WebRequestContext request, MaterialHistoricoPrecoFiltro filtro){
		List<MaterialHistoricoPrecoBean> lista = materialService.findHistoricoPrecos(filtro);
		return new ModelAndView("direct:/crud/popup/listagemPrecos", "lista", lista);
	}
	
	public ModelAndView ajaxMaterialProducaoOrKitOrGrade(WebRequestContext request, Material material){
		return materialService.ajaxMaterialProducaoOrKitOrGrade(request, material);
	}
	
	public ModelAndView recalcularFaixaMarkup(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhuma venda foi selecionada."); 
		}

		vendaService.recalcularFaixaMarkup(request, whereIn);
		request.addMessage("Rec�lculo de faixa de markup realizado com sucesso.");
		if("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/faturamento/crud/VendaOtr?ACAO=consultar&cdvenda="+whereIn);
		}
		return new ModelAndView("redirect:/faturamento/crud/VendaOtr?ACAO=listagem");
	}

	public ModelAndView exibirConfirmGerarValeCompra(WebRequestContext request) {
		JsonModelAndView json = new JsonModelAndView();
		
		Boolean exibir = false;
		String parametroUtilizacaoValeCompra = parametrogeralService.getValorPorNome(Parametrogeral.GERAR_VALECOMPRA);
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		
		if("TRUE".equals(parametroUtilizacaoValeCompra) && StringUtils.isNotBlank(cdpedidovendatipo)) {
			Pedidovendatipo pedidovendatipo = new Pedidovendatipo(Integer.parseInt(cdpedidovendatipo));
			pedidovendatipo = pedidovendatipoService.loadForEntrada(pedidovendatipo);
			
			if(pedidovendatipo != null && pedidovendatipo.getGeracaocontareceberEnum() != null) {
				if(pedidovendatipo.getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA)) {
					String cdpedidovenda = request.getParameter("cdpedidovenda");
					if(StringUtils.isNotBlank(cdpedidovenda)) {
						Pedidovenda pedidoVenda = new Pedidovenda(Integer.parseInt(cdpedidovenda));
						pedidoVenda = pedidovendaService.load(pedidoVenda);
						
						// caso n�o existam contas a receber vinculadas ao pedido confirmado, o confirm deve aparecer
						exibir = !pedidovendapagamentoService.existeDocumento(pedidoVenda);
					}
					
					
				} else if(pedidovendatipo.getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_VENDAREALIZADA)) {
					exibir = true;
				}
			} 
		}
		json.addObject("exibirConfirmGerarValeCompra", exibir);
		
		return json;
	}
	
	public ModelAndView ajaxAtualizaGridOtr(WebRequestContext request, Venda bean){
		/*if(SinedUtil.isListNotEmpty(bean.getListaPedidovendamaterial())){
			Map<Pneu, List<PneuItemVendaInterface>> mapPneuItemVenda = new HashMap<Pneu, List<PneuItemVendaInterface>>();
			bean.setListaPneu(new ArrayList<Pneu>());
			Integer i = 0;
			for(PneuItemVendaInterface pvm: bean.getListaPedidovendamaterial()){
				if(StringUtils.isNotBlank(pvm.getDtprazoentregaStr())){
					try {
						pvm.setDtprazoentrega(new Date(new SimpleDateFormat("dd/MM/yyyy").parse(pvm.getDtprazoentregaStr()).getTime()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(Util.objects.isPersistent(pvm.getPneu())){
					if(!mapPneuItemVenda.containsKey(pvm.getPneu())){
						pvm.setPneu(pneuService.informacoesPneu(request, pvm.getPneu()));
						mapPneuItemVenda.put(pvm.getPneu(), new ArrayList<PneuItemVendaInterface>());
					}
					List<PneuItemVendaInterface> listaItens = mapPneuItemVenda.get(pvm.getPneu());
					listaItens.add(pvm);
				}
				pvm.setIndice(i);
				i++;
			}
			Set<Entry<Pneu, List<PneuItemVendaInterface>>> entrySet = mapPneuItemVenda.entrySet();
			for (Entry<Pneu, List<PneuItemVendaInterface>> entry : entrySet) {
				entry.getKey().setListaItens(entry.getValue());
			}
			bean.getListaPneu().addAll(mapPneuItemVenda.keySet());
		}
		return new ModelAndView("direct:/crud/listagemPneuOtr", "pedidovenda", bean);*/
		request.setAttribute("podeEditar", true);
		request.setAttribute("origemVenda", true);
		request.setAttribute("existeGarantia", bean.getExisteGarantia());
		return vendaService.ajaxAtualizaGridOtr(request, bean.getListaPneuItemInterface());
	}
	
	public ModelAndView abrirPopupAlteracaoItemOtr(WebRequestContext request, Vendamaterial bean){
		if(StringUtils.isNotBlank(bean.getDtprazoentregaStr())){
			try {
				bean.setDtprazoentrega(new Date(new SimpleDateFormat("dd/MM/yyyy").parse(bean.getDtprazoentregaStr()).getTime()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new ModelAndView("direct:/crud/popup/popupAlteraItemOtr", "item", bean)
					.addObject("nameList", "listavendamaterial");
	}
	
	public ModelAndView ajaxValidaInclusaoServicoOtr(WebRequestContext request, Venda form){
		boolean possuiMaterialComMesmoCicloProducao = false;
		Material material = materialService.loadWithProducaoetapa(form.getMaterial());
		boolean possuiCicloDeProducao = Util.objects.isPersistent(material.getProducaoetapa());
		boolean validaCicloProducao = materialService.isMaterialProducao(form.getMaterial());
		if(validaCicloProducao && possuiCicloDeProducao && SinedUtil.isListNotEmpty(form.getListaPneuItemInterface())){
			for(Pneu pneu : form.getListaPneu()) {
				for(PneuItemVendaInterface item : form.getListaPneuItemInterface()) {
					if(item.getPneu().equals(pneu)){
						Material materialJaLancado = materialService.loadWithProducaoetapa(item.getMaterial());
						if(material.getProducaoetapa().equals(materialJaLancado.getProducaoetapa())){
							possuiMaterialComMesmoCicloProducao = true;
						}
						item.setPneu(pneu);
					}
				}
			}
			form.setListaPneu(vendaService.getListaPneuForOtr(form.getListaPneuItemInterface()));			
		}
		return new JsonModelAndView()
					.addObject("possuiCicloProducao", possuiCicloDeProducao)
					.addObject("possuiMaterialComMesmoCicloProducao", possuiMaterialComMesmoCicloProducao)
					.addObject("validaCicloProducao", validaCicloProducao);
	}
	
	public ModelAndView abrePopupImposto(WebRequestContext request, Vendamaterial bean){
		return vendaService.abrePopupImposto(request, bean);
	}
	
	public ModelAndView ajaxMostrarTotalImpostos(WebRequestContext request, Venda bean){
		return vendaService.ajaxMostrarTotalImpostos(request, bean);
	}
	
	public ModelAndView ajaxEnderecoFaturamento(WebRequestContext request, Cliente cliente){
		return enderecoService.ajaxEnderecoFaturamento(request, cliente);
	}
}