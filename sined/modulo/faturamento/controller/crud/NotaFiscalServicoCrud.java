package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.sf.json.JSON;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoRPS;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Propostasituacao;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Notaerrocorrecaotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.CodigotributacaoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.ContacarteiraService;
import br.com.linkcom.sined.geral.service.ContacorrenteService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EmpresaconfiguracaonfService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.GrupotributacaoService;
import br.com.linkcom.sined.geral.service.ItemlistaservicoService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoItemService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoRPSService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotaVendaService;
import br.com.linkcom.sined.geral.service.NotaerrocorrecaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalservicoduplicataService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProducaodiariaService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.RegimetributacaoService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.geral.service.TipotaxaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.geral.service.VendapagamentoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.CalculaAliquotaGrupoTributacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioEmailNotaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GeraReceita;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ParcelasCobrancaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.NotaFiscalEletronicaSubstituicaoProcess;
import br.com.linkcom.sined.modulo.faturamento.controller.process.filter.NotaFiscalEletronicaSubstituicaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/NotaFiscalServico", authorizationModule=CrudAuthorizationModule.class)
public class NotaFiscalServicoCrud extends CrudControllerSined<NotaFiscalServicoFiltro, NotaFiscalServico, NotaFiscalServico> {

	private ProducaodiariaService producaodiariaService;
	private RequisicaoService requisicaoService;
	private NotaHistoricoService notaHistoricoService;
	private TelefoneService telefoneService;
	private NotaService notaService;
	private ClienteService clienteService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private EmpresaService empresaService;
	private EmpresaconfiguracaonfService empresaconfiguracaonfService;
	private ParametrogeralService parametrogeralService;
	private EnderecoService enderecoService;
	private DocumentoService documentoService;
	private ArquivonfnotaService arquivonfnotaService;
	private ItemlistaservicoService itemlistaservicoService;
	private CodigotributacaoService codigotributacaoService;
	private VendapagamentoService vendapagamentoService;
	private VendaService vendaService;
	private NotaDocumentoService notaDocumentoService;
	private DocumentohistoricoService documentohistoricoService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private RegimetributacaoService regimetributacaoService;
	private ConfiguracaonfeService configuracaonfeService;
	private PropostaService propostaService;
	private ColaboradorService colaboradorService;
	private GrupotributacaoService grupotributacaoService;
	private VendamaterialService vendamaterialService;
	private NotaVendaService notaVendaService;	
	private CategoriaService categoriaService;
	private ProjetoService projetoService;
	private UsuarioService usuarioService;
	private NotaFiscalServicoRPSService notaFiscalServicoRPSService;
	private NotaFiscalServicoItemService notaFiscalServicoItemService;
	private NotafiscalservicoduplicataService notafiscalservicoduplicataService;
	private NotaerrocorrecaoService notaerrocorrecaoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private DocumentotipoService documentotipoService;
	private PrazopagamentoService prazopagamentoService;
	private PedidovendaService pedidovendaService;
	private ColetaService coletaService;
	private ContareceberService contareceberService;
	private VendahistoricoService vendahistoricoService;
	private ContacarteiraService contacarteiraService;
	private ContacorrenteService contacorrenteService;
	private TipotaxaService tipotaxaService;
	private DocumentoorigemService documentoorigemService;
	
	public void setNotaFiscalServicoRPSService(NotaFiscalServicoRPSService notaFiscalServicoRPSService) {this.notaFiscalServicoRPSService = notaFiscalServicoRPSService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setRegimetributacaoService(RegimetributacaoService regimetributacaoService) {this.regimetributacaoService = regimetributacaoService;}	
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {this.vendapagamentoService = vendapagamentoService;}
	public void setCodigotributacaoService(CodigotributacaoService codigotributacaoService) {this.codigotributacaoService = codigotributacaoService;}
	public void setItemlistaservicoService(ItemlistaservicoService itemlistaservicoService) {this.itemlistaservicoService = itemlistaservicoService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setProducaodiariaService(ProducaodiariaService producaodiariaService) {this.producaodiariaService = producaodiariaService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setEmpresaconfiguracaonfService(EmpresaconfiguracaonfService empresaconfiguracaonfService) {this.empresaconfiguracaonfService = empresaconfiguracaonfService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setPropostaService(PropostaService propostaService) {this.propostaService = propostaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setNotaVendaService(NotaVendaService notaVendaService) {this.notaVendaService = notaVendaService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setNotaFiscalServicoItemService(NotaFiscalServicoItemService notaFiscalServicoItemService) {this.notaFiscalServicoItemService = notaFiscalServicoItemService;}
	public void setNotafiscalservicoduplicataService(NotafiscalservicoduplicataService notafiscalservicoduplicataService) {this.notafiscalservicoduplicataService = notafiscalservicoduplicataService;}
	public void setNotaerrocorrecaoService(NotaerrocorrecaoService notaerrocorrecaoService) {this.notaerrocorrecaoService = notaerrocorrecaoService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setColetaService(ColetaService coletaService) {this.coletaService = coletaService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {this.vendahistoricoService = vendahistoricoService;}
	public void setContacarteiraService(ContacarteiraService contacarteiraService) {this.contacarteiraService = contacarteiraService;}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {this.contacorrenteService = contacorrenteService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request,	NotaFiscalServico form) throws CrudException {
		Usuario usuario = SinedUtil.getUsuarioLogado();
		String acao = request.getParameter("ACAO");
		if(acao != null && ("editar".equals(acao) || "consultar".equals(acao)) && usuarioService.isRestricaoClienteVendedor(usuario) ){
			Nota nota = notaService.loadWithCliente(form);
			if(nota != null && nota.getCliente() != null && nota.getCliente().getCdpessoa() != null && 
					!clienteService.isUsuarioPertenceRestricaoclientevendedor(nota.getCliente(), usuario)){
				request.addError("Usu�rio n�o tem permiss�o para consultar/editar esta nota!");
				SinedUtil.redirecionamento(request, "/faturamento/crud/NotaFiscalServico");
				return null;
			}
		}
		return super.doEditar(request, form);
	}
	
	@Override
	protected NotaFiscalServico criar(WebRequestContext request, NotaFiscalServico form) throws Exception {
		NotaFiscalServico bean = super.criar(request, form);
		
		if(SinedUtil.isAmbienteDesenvolvimento() && "true".equals(request.getParameter("copiar"))){
			bean = notaFiscalServicoService.criaCopia(form);
		}
		
		if(Boolean.parseBoolean(request.getParameter("fromOrdemServico"))){
			bean.setUsarSequencial(Boolean.TRUE);
		}
		if("true".equals(request.getParameter("fromSubstituicao"))){
			Object object = request.getSession().getAttribute(NotaFiscalEletronicaSubstituicaoProcess.FILTRO_SUBTITUICAO_NFSE);
			if(object != null){
				NotaFiscalEletronicaSubstituicaoFiltro filtro = (NotaFiscalEletronicaSubstituicaoFiltro)object;
				
				bean.setDtEmissao(new Date(filtro.getDtemissao().getTime()));
				bean.setDeducao(filtro.getValordeducoes());
				bean.setEmpresa(filtro.getEmpresa_substituido());
				if(filtro.getEmpresa_substituido() == null && request.getParameter("empresa_substituido_cdpessoa") != null){
					bean.setEmpresa(new Empresa(Integer.parseInt(request.getParameter("empresa_substituido_cdpessoa"))));
				}
				
				Money basecalculo = filtro.getBasecalculo();
				if(basecalculo == null){
					basecalculo = filtro.getValorservicos();
				}
				try{
					Double pis = (filtro.getValorpis().getValue().doubleValue() * 100d)/basecalculo.getValue().doubleValue();
					bean.setPis(pis);
					if(pis > 0 && filtro.getPisretido() != null){
						bean.setIncidepis(filtro.getPisretido());
					}
				} catch (Exception e) {}
				
				try{
					Double cofins = (filtro.getValorcofins().getValue().doubleValue() * 100d)/basecalculo.getValue().doubleValue();
					bean.setCofins(cofins);
					if(cofins > 0 && filtro.getCofinsretido() != null){
						bean.setIncidecofins(filtro.getCofinsretido());
					}
				} catch (Exception e) {}
				
				try{
					Double inss = (filtro.getValorinss().getValue().doubleValue() * 100d)/basecalculo.getValue().doubleValue();
					bean.setInss(inss);
					if(inss > 0 && filtro.getInssretido() != null){
						bean.setIncideinss(filtro.getInssretido());
					}
				} catch (Exception e) {}
				
				try{
					Double ir = (filtro.getValorir().getValue().doubleValue() * 100d)/basecalculo.getValue().doubleValue();
					bean.setIr(ir);
					if(ir > 0 && filtro.getIrretido() != null){
						bean.setIncideir(filtro.getIrretido());
					}
				} catch (Exception e) {}
				
				try{
					Double csll = (filtro.getValorcsll().getValue().doubleValue() * 100d)/basecalculo.getValue().doubleValue();
					bean.setCsll(csll);
					if(csll > 0 && filtro.getCsllretido() != null){
						bean.setIncidecsll(filtro.getCsllretido());
					}
				} catch (Exception e) {}
				
				try{
					Double iss = (filtro.getValoriss().getValue().doubleValue() * 100d)/basecalculo.getValue().doubleValue();
					bean.setIss(SinedUtil.round(iss, 4));
				} catch (Exception e) {}
				
				bean.setIncideiss(filtro.getIssretido());
				
				bean.setOutrasretencoes(filtro.getOutrasretencoes());
				
				bean.setBasecalculo(basecalculo);
				bean.setBasecalculoir(basecalculo);
				bean.setBasecalculoiss(basecalculo);
				bean.setBasecalculopis(basecalculo);
				bean.setBasecalculocofins(basecalculo);
				bean.setBasecalculocsll(basecalculo);
				bean.setBasecalculoinss(basecalculo);
				bean.setBasecalculoicms(basecalculo);
				bean.setDescontocondicionado(filtro.getDescontocondicionado());
				bean.setDescontoincondicionado(filtro.getDescontoincondicionado());
				
				bean.setItemlistaservicoBean(itemlistaservicoService.load(filtro.getItemlistaservico()));
				bean.setCodigotributacao(codigotributacaoService.load(filtro.getCodigotributacao()));
				
				NotaFiscalServicoItem item = new NotaFiscalServicoItem();
				item.setPrecoUnitario(filtro.getValorservicos().getValue().doubleValue());
				item.setQtde(1d);
				item.setDescricao(filtro.getDiscriminacao());
				
				List<NotaFiscalServicoItem> listaItens = new ArrayList<NotaFiscalServicoItem>();
				listaItens.add(item);
				bean.setListaItens(listaItens);
				
				Cliente cliente = null;
				if(filtro.getCpf() != null){
					cliente = clienteService.findByCpf(filtro.getCpf());
				} else if(filtro.getCnpj() != null){
					cliente = clienteService.findByCnpj(filtro.getCnpj());
				}
				bean.setCliente(cliente);
				
				if(cliente != null){
					cliente = clienteService.carregarDadosCliente(cliente);
					if(cliente != null){
						bean.setEnderecoCliente(cliente.getEndereco());
					}
				}
			}		
		}
		
		String whereInCdvenda = request.getParameter("whereInCdvenda");
		String qtdParcelas = request.getParameter("qtdParcelas");
		String cdvenda = request.getParameter("cdvenda");
		if (whereInCdvenda != null) {
			List<Venda> listaVendas = vendaService.findForCobranca(whereInCdvenda);
			Boolean somenteservico = Boolean.valueOf(request.getParameter("somente_servico") != null ? request.getParameter("somente_servico") : "false");
			List<NotaFiscalServico> notas = notaFiscalServicoService.getListaNotaFiscalServico(request, listaVendas, true, somenteservico, false, false);
			
			if (notas != null && notas.size() == 1) {
				bean = notas.get(0);
				bean.setUsarSequencial(true);
				bean.setAgruparContas(true);
				bean.setQtdParcelas(qtdParcelas != null ? Integer.parseInt(qtdParcelas) : null);
				bean.setCadastrarcobranca(true);
				bean.setDocumentotipo(vendaService.getDocumentotipoVenda(bean.getListaVenda()));
				bean.setContaboleto(vendaService.getContaboletoVenda(bean.getListaVenda()));
				if(bean.getContaboleto() != null){
					bean.setContacarteira(contacarteiraService.loadPadraoBoleto(bean.getContaboleto()));
				}
				
				if(bean.getListaItens() != null && !bean.getListaItens().isEmpty()){
					String infoContribuinteTemplate = notaFiscalServicoService.montaInformacoesContribuinteTemplate(bean.getListaVenda(), bean);
					if(!org.apache.commons.lang.StringUtils.isEmpty(infoContribuinteTemplate)){
						bean.setDadosAdicionais(infoContribuinteTemplate);
					}
				}
			} else {
				request.addError("Gerou mais de uma nota para as vendas selecionadas.");
			}
			bean.setWhereInCdvenda(whereInCdvenda);
		}else {
			if (cdvenda != null){
				Venda venda = vendaService.loadVenda(Integer.parseInt(cdvenda));
				if(notaVendaService.existNotaEmitida(venda, NotaTipo.NOTA_FISCAL_SERVICO)){
					throw new SinedException("A venda j� possui nota fiscal de servi�o.");
				}
				String expedicoes = request.getParameter("expedicoes");
				
				Boolean somenteservico = Boolean.valueOf(request.getParameter("somente_servico") != null ? request.getParameter("somente_servico") : "false");
				bean = notaFiscalServicoService.geraNotaVenda(bean, venda, expedicoes, somenteservico);
				
				String infoContribuinteTemplate = notaFiscalServicoService.montaInformacoesContribuinteTemplate(cdvenda, bean);
				if(infoContribuinteTemplate != null){
					bean.setDadosAdicionais(infoContribuinteTemplate);
				}
				
				bean.setSomenteservico(somenteservico);
			}
		}
		
		String faturarRequisicao = request.getParameter("faturarRequisicao");
		String whereInRequisicao = request.getParameter("whereInRequisicao");
		String romaneios = request.getParameter("romaneios");
		if(romaneios != null && !romaneios.equals("")){
			bean = notaFiscalServicoService.geraNotaRomaneios(bean, romaneios);
		}else {
			if(whereInRequisicao != null && faturarRequisicao != null && "TRUE".equalsIgnoreCase(faturarRequisicao)){
				bean = notaFiscalServicoService.preencheNotaForFaturarServicosRequisiscao(bean, whereInRequisicao);
				request.setAttribute("whereInRequisicao", whereInRequisicao);
			}
			
			Naturezaoperacao naturezaoperacao = bean.getNaturezaoperacao();
			Empresa empresa = bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null ? bean.getEmpresa() : empresaService.loadPrincipal();
			notaFiscalServicoService.preencheValoresPadroesEmpresa(empresa, bean);
			
			if((cdvenda != null || whereInCdvenda != null) && naturezaoperacao != null && NotaTipo.NOTA_FISCAL_SERVICO.equals(naturezaoperacao.getNotaTipo())){
				bean.setNaturezaoperacao(naturezaoperacao);
			}
		}
		
		return bean;
	}
	
	@Override
	protected void listagem(WebRequestContext request, NotaFiscalServicoFiltro filtro) throws Exception {
		request.setAttribute("listaNotaStatus", NotaStatus.getTodosEstados());
		
		if("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.LISTARRAZAOSOCIALCLIENTE))){
			request.setAttribute("listarRazaoSocialCliente", Boolean.TRUE);
		} else {
			request.setAttribute("listarRazaoSocialCliente", Boolean.FALSE);
		}
		
		request.setAttribute("haveVotorantim", configuracaonfeService.havePrefixoativo(Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD));
	}
	
	@Override
	protected ListagemResult<NotaFiscalServico> getLista(WebRequestContext request, NotaFiscalServicoFiltro filtro) {
		
		
		ListagemResult<NotaFiscalServico> lista = super.getLista(request, filtro);
		List<NotaFiscalServico> list = lista.list();
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdNota", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(notaFiscalServicoService.carregarInfoParaCalcularTotalServico(whereIn,filtro.getOrderBy(),filtro.isAsc()));
			
			List<NotaFiscalServicoRPS> listaRPS = notaFiscalServicoRPSService.findByNota(whereIn);
			if(listaRPS != null && listaRPS.size() > 0){
				for (NotaFiscalServicoRPS rps : listaRPS) {
					if(rps.getNotaFiscalServico() != null &&
							rps.getNotaFiscalServico().getCdNota() != null &&
							rps.getNotaFiscalServicoLoteRPS() != null &&
							rps.getNotaFiscalServicoLoteRPS().getCdNotaFiscalServicoLoteRPS() != null){
						for (NotaFiscalServico nf : list) {
							if(nf.getCdNota().equals(rps.getNotaFiscalServico().getCdNota())){
								nf.setCdNotaFiscalServicoLoteRPS(rps.getNotaFiscalServicoLoteRPS().getCdNotaFiscalServicoLoteRPS());
							}
						}
					}
				}
			}
			
			List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(whereIn);
			if(listaArquivonfnota != null){
				for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
					if(arquivonfnota.getDtcancelamento() == null){
						for (NotaFiscalServico nf : list) {
							if(nf.getCdNota().equals(arquivonfnota.getNota().getCdNota())){
								nf.setHaveArquivonfnota(Boolean.TRUE);
								if(/*arquivonfnota.getDtcancelamento() == null &&*/ 
										arquivonfnota.getArquivonf() != null && 
										arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
										arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO) &&
										arquivonfnota.getNumeronfse() != null){
									nf.setNumeroNfse(arquivonfnota.getNumeronfse() + (arquivonfnota.getCodigoverificacao() != null ? " / " + arquivonfnota.getCodigoverificacao() : ""));
								}
								if(NotaStatus.EMITIDA.equals(nf.getNotaStatus()) && arquivonfnota.getArquivonf() != null && 
										arquivonfnota.getArquivonf().getConfiguracaonfe() != null && 
										arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice() != null){
									nf.setMsgerrocorrecao(notaerrocorrecaoService.getMensagemErroCorrecao(nf, Notaerrocorrecaotipo.NOTA_FISCAL_SERVICO, arquivonfnota.getArquivonf().getConfiguracaonfe().getPrefixowebservice().getPrefixo()));
								}
								break;
							}
						}
					}
				}
			}
		}
		
		notaFiscalServicoService.ajustaValoresListagem(filtro, lista.list());
		
		if("TRUE".equals(parametrogeralService.getValorPorNome("SomenteNotaCofiguravel"))){
			filtro.setConfiguracao(empresaconfiguracaonfService.verificaConfiguracao());
		} else {
			filtro.setConfiguracao(false);
		}
		return lista;
	}
	
	@Override
	protected void entrada(WebRequestContext request, NotaFiscalServico form) throws Exception {
		Boolean permissaoProjeto = false;
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			permissaoProjeto = StringUtils.isNotBlank(SinedUtil.getListaProjeto());
			request.setAttribute("permissaoProjeto", new Boolean(permissaoProjeto));
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		
		if(form.getCdNota() != null && (form.getNotaStatus().equals(NotaStatus.NFSE_EMITIDA) || form.getNotaStatus().equals(NotaStatus.NFSE_LIQUIDADA))){
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(form);
			if(arquivonfnota != null){
				form.setNumeroNfse(arquivonfnota.getNumeronfse());
				form.setCodVerificacao(arquivonfnota.getCodigoverificacao());
			}
		}
		
		boolean haveParacatu = configuracaonfeService.havePrefixoativo(Prefixowebservice.PARACATUISS_PROD);
		request.setAttribute("haveParacatu", haveParacatu);
		
		boolean haveBrumadinho = configuracaonfeService.havePrefixoativo(Prefixowebservice.BRUMADINHOISS_PROD, Prefixowebservice.IGARAPEISS_PROD);
		request.setAttribute("haveBrumadinho", haveBrumadinho);
		
		boolean podeEditar = notaService.podeEditar(form.getNotaStatus(), false, true, form.getNotaimportadaxml());
		boolean podeEditarNotaNumeroPosterior = notaFiscalServicoService.podeEditarNotaNumeroPosterior(form);
		request.setAttribute("podeEditar", podeEditar);
		
		//Action Editar
		if(AbstractCrudController.EDITAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))){
			if(!podeEditar){
				throw new SinedException("Uma nota liquidada ou no processo de nota fiscal eletr�nica n�o deve ser editada.");
			}else if(!podeEditarNotaNumeroPosterior){
				throw new SinedException("Esta nota n�o pode ser editada, pois existe(m) nota(s) com data de emiss�o anterior que ainda n�o teve NFS-e gerada");
			}
		}
		if (form.getCdNota() != null) {
			if(!request.getBindException().hasErrors()){
				form.setListaDuplicata(notafiscalservicoduplicataService.findByNotafiscalservico(form));
			}
			this.obterHistorico(form);
			this.telefonePersistenteParaTransiente(form);
		}
		
		List<Endereco> listaEnderecoCliente = null;
		List<Telefone> listaTelefoneCliente = null;
		
		if (form.getCliente() != null && form.getCliente().getCdpessoa() != null) {
			listaEnderecoCliente = enderecoService.findByPessoaWithoutInativo(form.getCliente());
			listaTelefoneCliente = telefoneService.carregarListaTelefone(form.getCliente());
		} else {
			listaEnderecoCliente = new ArrayList<Endereco>();
			listaTelefoneCliente = new ArrayList<Telefone>();
		}
		
		request.setAttribute("listaEnderecoCliente", listaEnderecoCliente);
		request.setAttribute("listaTelefoneCliente", listaTelefoneCliente);		
		request.setAttribute("listaNaturezaoperacao", naturezaoperacaoService.find(form.getNaturezaoperacao(), NotaTipo.NOTA_FISCAL_SERVICO, Boolean.TRUE));
		request.setAttribute("listaRegimetributacao", regimetributacaoService.findAtivos());
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));

		request.setAttribute("listaProjeto", projetoService.findProjetosAtivos(form.getProjeto(), permissaoProjeto));
		request.setAttribute("haveVotorantim", configuracaonfeService.havePrefixoativo(Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD));
		
		if("TRUE".equals(parametrogeralService.getValorPorNome("SomenteNotaCofiguravel"))){
			request.setAttribute("configuracao_notafiscal_configuravel", empresaconfiguracaonfService.verificaConfiguracao());
		} else {
			request.setAttribute("configuracao_notafiscal_configuravel", false);
		}
		
		if(form.getCdNota() != null){
			notaFiscalServicoItemService.ordenaListaItens(form);
		}
		
		if(!AbstractCrudController.CONSULTAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))){
	        String whereInVenda = notaVendaService.getWhereInVenda(form);
	        request.setAttribute("cdVenda", whereInVenda);
	        request.setAttribute("infContribuinteTemplateNatureza", SinedUtil.escapeJavascript(notaFiscalServicoService.montaInformacoesContribuinteTemplate(whereInVenda, form)));
        }
		
		if (form.getCdNota()==null && form.getHremissao()==null){
			form.setHremissao(new Hora(SinedDateUtils.currentTimestamp().getTime()));
		}	
		
		if (form.getCdNota()==null && !Boolean.TRUE.equals(form.getCadastrarcobranca())){
			form.setCadastrarcobranca(Boolean.FALSE);
		}
		String whereInCddocumentotipo = null;
		if(form.getCdNota() != null){
			List<String> listaWhereInCddocumentotipo = new ArrayList<String>();
	        if(SinedUtil.isListNotEmpty(form.getListaDuplicata())){
	        	for(Notafiscalservicoduplicata parcela: form.getListaDuplicata()){
	        		if(parcela.getDocumentotipo() != null && parcela.getDocumentotipo().getCddocumentotipo() != null){
	        			listaWhereInCddocumentotipo.add(parcela.getDocumentotipo().getCddocumentotipo().toString());
	        		}
	        	}
	        }
	
	        if(form.getDocumentotipo() != null && form.getDocumentotipo().getCddocumentotipo() != null){
	        	listaWhereInCddocumentotipo.add(form.getDocumentotipo().getCddocumentotipo().toString());
	        }
	        whereInCddocumentotipo = listaWhereInCddocumentotipo.isEmpty()? null: CollectionsUtil.concatenate(listaWhereInCddocumentotipo, ",");
	    }
        List<Documentotipo> listaDocumentotipo = documentotipoService.getListaDocumentoTipoUsuarioForVenda(whereInCddocumentotipo);
        request.setAttribute("listaDocumentotipo", listaDocumentotipo);
		request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
	}
	
	/**
	 * Verifica se o telefone persistido se encontra na lista do combo.
	 * Se estiver, apenas copia o telefone da propriedade persistente (telefoneCliente)
	 * para a propriedade transiente (telefoneClienteTransiente);
	 * Caso n�o esteja, cria um novo item de br.com.linkcom.sined.geral.bean.Telefone
	 * com o cdtelefone=-1 e com o valor igual ao valor do telefone persistente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TelefoneService#findForCombo(String...)
	 * @param form
	 * @author Hugo Ferreira
	 */
	protected void telefonePersistenteParaTransiente(NotaFiscalServico form) {
		List<Telefone> listaTelefone = telefoneService.carregarListaTelefone(form.getCliente());
		Telefone telefone = new Telefone();
		telefone.setCdtelefone(-1);
		telefone.setTelefone(form.getTelefoneCliente());
		
		if (listaTelefone.contains(telefone)) {
			int index = listaTelefone.indexOf(telefone);
			form.setTelefoneClienteTransiente(listaTelefone.get(index));
		} else {
			listaTelefone.add(telefone);
			form.setTelefoneClienteTransiente(telefone);
		}
	}
	
	/**
	 * Carrega e seta o hist�rico da Nota.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaHistoricoService#carregarListaHistorico(Nota)
	 * @param bean
	 * @author Hugo Ferreira
	 */
	private void obterHistorico(NotaFiscalServico bean) {
		List<NotaHistorico> listaHistorico = notaHistoricoService.carregarListaHistorico(bean);
		bean.setListaNotaHistorico(listaHistorico);
	}
	
	@Override
	protected void salvar(WebRequestContext request, NotaFiscalServico bean) throws Exception {
		
		/*
		 * Se a nota estiver sendo salva para faturar produ��es di�rias.
		 */
		if(StringUtils.isNotBlank(bean.getCodProducoesDiaria())){
			
			/*
			 * Procedimento para faturar produ��es di�rias.
			 */
			producaodiariaService.salvarNotaAoFaturarProducaodiaria(bean);
			return;
		}
		boolean isCriar = false;
		String contas = bean.getContas();
		String vendas = bean.getVendas();
		String romaneios = bean.getRomaneios();
		String whereInRequisicao = bean.getWhereInRequisicao();

		Venda vendaassociada = null;
		if (bean.getCdvendaassociada() != null){
			vendaassociada = new Venda(bean.getCdvendaassociada());
			vendaassociada = vendaService.loadForEntrada(vendaassociada);
			if (vendaassociada.getColaborador() != null && vendaassociada.getColaborador().getCdpessoa() != null){
				vendaassociada.setColaborador(colaboradorService.load(vendaassociada.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
			}
			if (vendaassociada.getCliente() != null && vendaassociada.getCliente().getCdpessoa() != null){
				vendaassociada.setCliente(clienteService.load(vendaassociada.getCliente(), "cliente.cdpessoa, cliente.nome"));
			}
			if (vendaassociada.getEmpresa()!=null && vendaassociada.getEmpresa().getCdpessoa()!=null){
				vendaassociada.setEmpresa(empresaService.loadForVenda(vendaassociada.getEmpresa()));
			}
			vendaassociada.setListavendamaterial(vendamaterialService.findByVenda(vendaassociada));
		}
		
		boolean verificarParcelaDocumento = false;
		if (bean.getCdNota() == null) {
			isCriar = true;
			boolean usoSequencial = false;
			if(bean.getUsarSequencial() != null && bean.getUsarSequencial()){
				usoSequencial = true;
				Empresa empresa = bean.getEmpresa();
				if(empresa == null) empresa = empresaService.loadPrincipal();
				
				Integer proximoNumero = empresaService.carregaProxNumNF(empresa);
				if(proximoNumero == null){
					proximoNumero = 1;
				}
				bean.setNumero(proximoNumero.toString());
				proximoNumero++;

				empresaService.updateProximoNumNF(empresa, proximoNumero);
			}
			
			if(vendaassociada == null){
				if((contas == null || contas.equals("")) && (romaneios == null || romaneios.equals(""))){
					notaHistoricoService.adicionaHistoricoEmitida(bean);
				} 
				else if(romaneios != null && !romaneios.equals("")){			
					StringBuilder sb = new StringBuilder("Origem Romaneio(s): ");
					
					String[] ids = romaneios.split(",");
					for (int i = 0; i < ids.length; i++) {
						if(i != 0) sb.append(", ");
						sb
						.append("<a href=\"javascript:visualizarRomaneios(")
						.append(ids[i])
						.append(")\">")
						.append(ids[i])
						.append("</a>");
					}
					
					notaHistoricoService.adicionaHistoricoEmitidaObservacao(bean, sb.toString());			
				}
				else {
					verificarParcelaDocumento = true;
					StringBuilder sb = new StringBuilder("Vinculada a(s) conta(s) a receber ");
					
					String[] ids = contas.split(",");
					for (int i = 0; i < ids.length; i++) {
						if(i != 0) sb.append(", ");
						sb
						.append("<a href=\"javascript:visualizarContaReceber(")
						.append(ids[i])
						.append(")\">")
						.append(ids[i])
						.append("</a>");
					}
					
					notaHistoricoService.adicionaHistoricoEmitidaObservacao(bean, sb.toString());
				}
			}
			
			if(bean.getFromProposta() != null &&  bean.getFromProposta() && bean.getWhereIn() != null && !"".equals(bean.getWhereIn())){
				notaHistoricoService.adicionaHistoricoEmitidaObservacao(bean, "Origem de proposta: <a href=\"javascript:visualizarProposta(" + bean.getWhereIn() + ")\">" + bean.getWhereIn() + "</a>");
			}
			
			if(!usoSequencial){
				notaHistoricoService.adicionaHistoricoEmitidaObservacao(bean,"N�mero da nota n�o foi gerado sequencialmente.");
			}
		}else {
			notaHistoricoService.adicionaHistoricoAlterada(bean);
		}
		
		String numeroAnteriorNota = null;
		boolean atualizarNumeroHistoricovenda = false;
		if(bean.getCdNota() != null && bean.getNumero() != null && !"".equals(bean.getNumero())){
			NotaFiscalServico nf = notaFiscalServicoService.load(bean, "notaFiscalServico.numero");
			if(nf != null && nf.getNumero() != null && !"".equals(nf.getNumero()) && !bean.getNumero().equals(nf.getNumero())){
				atualizarNumeroHistoricovenda = true;
				numeroAnteriorNota = nf.getNumero();
			}
		}
		
		//Atualiza o flag utilizado para calculo de imposto cumulativo, se j� incidiu, quer dizer que j� foi cobrado.
		bean.setImpostocumulativocofins(bean.getIncidecofins() != null ? bean.getIncidecofins() : false);
		bean.setImpostocumulativocsll(bean.getIncidecsll() != null ? bean.getIncidecsll() : false);
		bean.setImpostocumulativoicms(bean.getIncideicms() != null ? bean.getIncideicms() : false);
		bean.setImpostocumulativoinss(bean.getIncideinss() != null ? bean.getIncideinss() : false);
		bean.setImpostocumulativoir(bean.getIncideir() != null ? bean.getIncideir() : false);
		bean.setImpostocumulativoiss(bean.getIncideiss() != null ? bean.getIncideiss() : false);
		bean.setImpostocumulativopis(bean.getIncidepis() != null ? bean.getIncidepis() : false);
		
		this.telefoneTransienteParaPersistente(bean);
		
		if(bean.getGrupotributacao() == null){
			Cliente clienteGrupotributacao = clienteService.load(bean.getCliente(), "cliente.grupotributacao");
			if (clienteGrupotributacao != null && clienteGrupotributacao.getGrupotributacao() != null){
				bean.setGrupotributacao(clienteGrupotributacao.getGrupotributacao());
				
				NotaFiscalServico notaAux = new NotaFiscalServico();
				notaAux.setGrupotributacao(bean.getGrupotributacao());
				notaAux.setEmpresa(bean.getEmpresa());
				notaAux.setCliente(bean.getCliente());
				notaAux.setBasecalculo(bean.getBasecalculo());
				notaAux.setEnderecoCliente(bean.getEnderecoCliente());
				notaAux.setCodigotributacao(bean.getCodigotributacao());
				notaAux.setMunicipioissqn(bean.getMunicipioissqn());

				notaFiscalServicoService.calculaTributacaoNota(notaAux, notaAux.getGrupotributacao(), notaAux.getBasecalculo());
				
				bean.setBasecalculoir(notaAux.getBasecalculoir());
				bean.setIr(notaAux.getIr());
				bean.setIncideir(notaAux.getIncideir());
				bean.setImpostocumulativoir(notaAux.getImpostocumulativoir());
				
				bean.setBasecalculoinss(notaAux.getBasecalculoinss());
				bean.setInss(notaAux.getInss());
				bean.setIncideinss(notaAux.getIncideinss());
				bean.setImpostocumulativoinss(notaAux.getImpostocumulativoinss());
				
				bean.setBasecalculoiss(notaAux.getBasecalculoiss());
				bean.setIss(notaAux.getIss());
				bean.setIncideiss(notaAux.getIncideiss());
				bean.setImpostocumulativoiss(notaAux.getImpostocumulativoiss());
				
				bean.setBasecalculocsll(notaAux.getBasecalculocsll());
				bean.setCsll(notaAux.getCsll());
				bean.setIncidecsll(notaAux.getIncidecsll());
				bean.setImpostocumulativocsll(notaAux.getImpostocumulativocsll());
				
				//bean.setTipocobrancapis(notaAux.getTipocobrancapis());
				bean.setBasecalculopis(notaAux.getBasecalculopis());
				bean.setPis(notaAux.getPis());
				bean.setIncidepis(notaAux.getIncidepis());
				bean.setImpostocumulativopis(notaAux.getImpostocumulativopis());
				bean.setPisretido(notaAux.getPisretido());
				
				//bean.setTipocobrancacofins(notaAux.getTipocobrancacofins());
				bean.setBasecalculocofins(notaAux.getBasecalculocofins());
				bean.setCofins(notaAux.getCofins());
				bean.setIncidecofins(notaAux.getIncidecofins());
				bean.setImpostocumulativocofins(notaAux.getImpostocumulativocofins());
				bean.setCofinsretido(notaAux.getCofinsretido());
				
				bean.setBasecalculoicms(notaAux.getBasecalculoicms());
				bean.setIcms(notaAux.getIcms());
				bean.setIncideicms(notaAux.getIncideicms());
				bean.setImpostocumulativoicms(notaAux.getImpostocumulativoicms());
			
			
			}
		}
		
		if(bean.getCdNota() == null && StringUtils.isNotBlank(contas) && documentoService.isOrigemContratoNotaAposRecebimento(contas)){
			bean.setNotaStatus(NotaStatus.EM_ESPERA);
		}
		
		if (!Boolean.TRUE.equals(bean.getCadastrarcobranca())){
			bean.setListaDuplicata(null);
		}
		
		super.salvar(request, bean);
		
		if(bean.getListaDuplicata() != null && bean.getListaDuplicata().size() > 0){
			Integer i = 1;
			for (Notafiscalservicoduplicata dup : bean.getListaDuplicata()) {
				if(dup.getNumero() == null || dup.getNumero().trim().equals("") || i.toString().equals(dup.getNumero())){
					dup.setNumero((bean.getNumero()!=null && !bean.getNumero().trim().equals("") ? bean.getNumero() + "/" : "") + (parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_VENDA) ? i : ""));
					notafiscalservicoduplicataService.updateNumeroDuplicata(dup, dup.getNumero());
				}
				i++;
			}
		}
		
		if(contas != null && !contas.equals("")){
			String[] ids = contas.split(",");
			for (int i = 0; i < ids.length; i++) {
				Documento documento = new Documento(Integer.parseInt(ids[i]));
				
				NotaDocumento notaDocumento = new NotaDocumento();
				notaDocumento.setNota(bean);
				notaDocumento.setDocumento(documento);
				notaDocumento.setFromwebservice(Boolean.TRUE);
				notaDocumentoService.saveOrUpdate(notaDocumento);
				
				documento = documentoService.carregaDocumento(documento);
				documento.setAcaohistorico(Documentoacao.ALTERADA);
				Documentohistorico documentohistorico = new Documentohistorico(documento);
				String obs = "Vinculado a nota <a href=\"javascript:visualizarNotaFiscalServico(" + bean.getCdNota() + ")\">" + bean.getCdNota() + "</a>. ";
				documentohistorico.setObservacao(obs);
				documentohistoricoService.saveOrUpdate(documentohistorico);
				
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
					String numero = bean.getNumero();
					if(verificarParcelaDocumento && parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_VENDA) &&
							StringUtils.isNotBlank(numero) && StringUtils.isNotBlank(documento.getNumero()) &&
							documento.getNumero().indexOf("/") != -1){
						numero+= documento.getNumero().substring(documento.getNumero().indexOf("/"));
					}
					
					if (StringUtils.isNotBlank(numero)){
						documentoService.updateNumerosNFDocumento(ids[i], numero);
					}
				}
			}
			
		}
		
		if (vendaassociada != null){
			if(isCriar){
				GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
				if(vendaassociada.getPedidovendatipo() != null){
					geracaocontareceberEnum = vendaassociada.getPedidovendatipo().getGeracaocontareceberEnum();
				}
				if(geracaocontareceberEnum != null && GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(geracaocontareceberEnum)){
					vendaService.gerarContareceberAposEmissaonota(request, vendaassociada, bean.getDtEmissao(), null, null, bean.getValorNota(), bean.getListaDuplicata(), NotaTipo.NOTA_FISCAL_SERVICO, null, bean.getSomenteservico() != null && bean.getSomenteservico(), true, bean.getContaboleto(), bean.getPrazopagamentofatura(), true);
				}
			}
			vendaService.notafiscalGerada(vendaassociada, null, bean, null, true, null);
		}
		
		if(isCriar && vendas != null && !vendas.equals("") && !vendas.equals("null")){
			String[] ids = vendas.split(",");
			for (int i = 0; i < ids.length; i++) {
				Venda venda = new Venda(Integer.valueOf(ids[i]));
				
				if(StringUtils.isBlank(contas)){
					NotaVenda notaVenda = new NotaVenda();
					notaVenda.setNota(bean);
					notaVenda.setVenda(venda);
					notaVendaService.saveOrUpdate(notaVenda);
				}
				
				if(vendaService.todasContasReceberVinculadasNotas(venda)){
					VendaService.getInstance().faturaVenda(venda.getCdvenda());
				}
			}
		}
		
		try {
			if (BooleanUtils.isTrue(bean.getAgruparContas()) && !StringUtils.isEmpty(bean.getWhereInCdvenda()) && BooleanUtils.isTrue(bean.getCadastrarcobranca())) {
				GeraReceita geraReceita = notaService.criarGeraReceita(bean);
				geraReceita.setValorUmaParcela(bean.getValorliquidofatura());
				geraReceita.setPrazoPagamento(bean.getPrazopagamentofatura());
				if(bean.getValordescontofatura() != null){
					geraReceita.setDescontoContratualMny(bean.getValordescontofatura());
				}
				geraReceita.setQtdeParcelas(bean.getListaDuplicata().size());
				Documento documento = documentoService.gerarDocumento(geraReceita, true);
				if(documento.getEndereco() == null){
					documento.setEndereco(enderecoService.carregaEnderecoClienteForDocumento(bean.getCliente()));
				}
				
				String msg = "";
				StringBuilder msg2 = new StringBuilder();
				StringBuilder msg3 = new StringBuilder();
				String preposicao = "";
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				Set < Taxaitem > listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
				if (bean.getContaboleto() != null && bean.getContaboleto().getCdconta() != null) {
					Endereco enderecoPessoa = null;
					if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
						enderecoPessoa = enderecoService.getEnderecoPrincipalPessoa(bean.getEmpresa());
					}
					
					Set < Taxaitem > listaTaxaItemOriginal = contacorrenteService.getTaxas(bean.getContaboleto(), documento.getDtvencimento(), enderecoPessoa);
					Cliente cliente = clienteService.load(bean.getCliente(), "cliente.cdpessoa, cliente.naoconsiderartaxaboleto");
					if (cliente != null && !Boolean.TRUE.equals(cliente.getNaoconsiderartaxaboleto())) {
						listaTaxaitem = listaTaxaItemOriginal;
					} else {
						if (SinedUtil.isListNotEmpty(listaTaxaItemOriginal)) {
							for (Taxaitem ti: listaTaxaItemOriginal) {
								if (ti.getTipotaxa() != null && Tipotaxa.TAXABOLETO.equals(ti.getTipotaxa())) {
									continue;
								}
								listaTaxaitem.add(ti);
							}
						}
					}
				}
				
				if (listaTaxaitem != null && !listaTaxaitem.isEmpty()) {
					Taxa taxa = new Taxa();
					taxa.setListaTaxaitem(listaTaxaitem);
					documento.setTaxa(taxa);

					if (documento.getMensagem2() != null) msg2.append(documento.getMensagem2());
					if (documento.getMensagem3() != null) msg3.append(documento.getMensagem3());

					for (Taxaitem txitem: listaTaxaitem) {
						if (txitem.getGerarmensagem() != null && txitem.getGerarmensagem()) {
							if (tipotaxaService.isApos(txitem.getTipotaxa())) {
								preposicao = " ap�s ";
							} else {
								preposicao = " at� ";
							}
							msg = txitem.getTipotaxa().getNome() + " de " + (txitem.isPercentual() ? "": "R$") + txitem.getValor() + (txitem.isPercentual() ? "%": "") + (txitem.getDtlimite() != null ? preposicao + format.format(txitem.getDtlimite()) : "") + ". ";
							if ((msg2.length() + msg.length()) <= 80) {
								msg2.append(msg);
							} else if ((msg3.length() + msg.length()) <= 80) {
								msg3.append(msg);
							}
						}
					}
					if (!msg2.toString().equals("")) {
						documento.setMensagem2(msg2.toString());
					}
					if (!msg3.toString().equals("")) {
						documento.setMensagem3(msg3.toString());
					}
				}
				
				if(documento.getRateio() != null && SinedUtil.isListNotEmpty(documento.getRateio().getListaRateioitem())){
					for(Rateioitem rateioitem : documento.getRateio().getListaRateioitem()){
						if(rateioitem.getContagerencial() == null){
							 throw new Exception(bean.getNumero() + " - Material ou empresa sem centro de custo para o rateio.");
						}else if(rateioitem.getCentrocusto() == null){
							throw new Exception(bean.getNumero() + " - Material ou empresa sem centro de custo para o rateio.");
						}
					}
				}
				
				documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
				documento.setTipopagamento(Tipopagamento.CLIENTE);
				documento.ajustaBeanToSave();
				
				contareceberService.saveOrUpdate(documento);
				
				List<Venda> listaVendas = vendaService.findForCobranca(bean.getWhereInCdvenda());
				StringBuilder linkHistoricoNota = new StringBuilder();
				StringBuilder linkHistoricoDocumento = new StringBuilder();
				
				if(SinedUtil.isListNotEmpty(documento.getListaDocumento())){
					for (Documento documentoFound : documento.getListaDocumento()) {
						linkHistoricoDocumento.append("<a href=\"javascript:visualizarContaReceber("+documentoFound.getCddocumento()+");\">"+documentoFound.getCddocumento()+"</a>,");
					}
				}else {
					linkHistoricoDocumento.append("<a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+");\">"+documento.getCddocumento()+"</a>,");
				}
				
				for (Venda venda : listaVendas) {
					for (int i = 0 ; i < venda.getListavendapagamento().size() ; i++) {
						if(SinedUtil.isListNotEmpty(documento.getListaDocumento())){
							if(venda.getListavendapagamento().size() > i && documento.getListaDocumento().size() > i){
								venda.getListavendapagamento().get(i).setDocumento(documento.getListaDocumento().get(i));
								vendapagamentoService.updateDocumento(venda.getListavendapagamento().get(i));
							}
						}else {
							venda.getListavendapagamento().get(i).setDocumento(documento);
							vendapagamentoService.updateDocumento(venda.getListavendapagamento().get(i));
						}
						
					}
					
					notaVendaService.saveOrUpdate(new NotaVenda(bean, venda));
					
					Vendahistorico vendahistoricoNota = new Vendahistorico();
					vendahistoricoNota.setVenda(venda);
					String numeroNota = bean.getNumero() == null || bean.getNumero().equals("") ? "sem n�mero" : bean.getNumero();
					vendahistoricoNota.setAcao("Gera��o da nota " + numeroNota);
					vendahistoricoNota.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNotaServico("+bean.getCdNota()+");\">"+numeroNota+"</a>.");
					vendahistoricoService.saveOrUpdate(vendahistoricoNota);
					
					Vendahistorico vendahistoricoConta = new Vendahistorico();
					vendahistoricoConta.setVenda(venda);
					String numeroConta = bean.getNumero() == null || bean.getNumero().equals("") ? "sem n�mero" : bean.getNumero();
					vendahistoricoConta.setAcao("GERA��O DA CONTA A RECEBER " + numeroConta);
					vendahistoricoConta.setObservacao("CONTAS A RECEBER GERADAS AP�S NOTA: " + linkHistoricoDocumento.toString().substring(0, linkHistoricoDocumento.toString().length() - 1) + ".");
					vendahistoricoService.saveOrUpdate(vendahistoricoConta);
					
					if(notaVendaService.existeNotaEmitida(venda)){
						vendaService.updateSituacaoVenda(venda.getCdvenda().toString(), Vendasituacao.FATURADA);
					}
					
					venda.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(venda));
					venda.setListavendamaterial(vendamaterialService.findByVenda(venda));
					if(SinedUtil.isListNotEmpty(venda.getListavendapagamento())){
						for (Vendapagamento vendapagamento : venda.getListavendapagamento()) {
							if(vendapagamento.getDocumento()!=null && vendapagamento.getDocumento().getCddocumento()!=null){
								vendaService.verificaComissionamento(vendapagamento, venda.getListavendapagamento().size(), null, null);
							}
						}
					}
					
					linkHistoricoNota.append(" <a href=\"javascript:visualizaVenda(" + venda.getCdvenda() + ");\">" + venda.getCdvenda() + "</a>,");
					
					if(SinedUtil.isListNotEmpty(documento.getListaDocumento())){
						for(Documento documentoVenda : documento.getListaDocumento()){
							Documentoorigem documentoorigem = new Documentoorigem();
							documentoorigem.setVenda(venda);
							documentoorigem.setDocumento(documentoVenda);
							documentoorigemService.saveOrUpdate(documentoorigem);
						}
					}else {
						Documentoorigem documentoorigem = new Documentoorigem();
						documentoorigem.setVenda(venda);
						documentoorigem.setDocumento(documento);
						documentoorigemService.saveOrUpdate(documentoorigem);
					}
					
					if (StringUtils.isNotEmpty(bean.getPneusJaAssociado())) {
						vendahistoricoService.salvarHistoricoPneuJaAssociado(bean, venda, bean.getPneusJaAssociado());
					}
				}
				
				if(SinedUtil.isListNotEmpty(documento.getListaDocumento())){
					for (Documento documentoFound : documento.getListaDocumento()) {
						Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(documentoFound);
						documentohistorico.setObservacao("Origem venda: " + linkHistoricoNota.toString().substring(0, linkHistoricoNota.toString().length() - 1) + ".");
						documentohistoricoService.saveOrUpdate(documentohistorico);
					}
				}else {
					Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
					documentohistorico.setObservacao("Origem venda: " + linkHistoricoNota.toString().substring(0, linkHistoricoNota.toString().length() - 1) + ".");
					documentohistoricoService.saveOrUpdate(documentohistorico);
				}
				
				NotaHistorico notaHistorico = new NotaHistorico(bean.getCdNota(), bean, bean.getNotaStatus(), 
						"Visualizar venda: " + linkHistoricoNota.toString().substring(0, linkHistoricoNota.toString().length() - 1) + ".", 
						SinedUtil.getCdUsuarioLogado(), SinedDateUtils.currentTimestamp());
				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(bean.getFromProposta() != null &&  bean.getFromProposta() && bean.getWhereIn() != null && !"".equals(bean.getWhereIn())){
			propostaService.criaInteracaoByNota(bean.getWhereIn(), bean);
		}
		
		if(atualizarNumeroHistoricovenda){
			notaVendaService.atualizarNumeroHistoricoVenda(bean, numeroAnteriorNota, true, false);
		}
		
		if(whereInRequisicao != null && !whereInRequisicao.isEmpty()){
			requisicaoService.saveHistoricoRequisicao(whereInRequisicao, bean.getCdNota(), true);
			
			if(whereInRequisicao != null && !whereInRequisicao.isEmpty()){
				requisicaoService.saveHistoricoRequisicao(whereInRequisicao, bean.getCdNota(), false);
				
				notaHistoricoService.salvarHistoricoNotaRequisicao(bean, whereInRequisicao);
				
				requisicaoService.updateEstados(whereInRequisicao,new Requisicaoestado(Requisicaoestado.FATURADO));
				requisicaoService.updateVendaFaturado(whereInRequisicao);
			}
			
			requisicaoService.updateEstados(whereInRequisicao,new Requisicaoestado(Requisicaoestado.FATURADO));
			requisicaoService.updateVendaFaturado(whereInRequisicao);
		}
	}
	
	/**
	 * Copia o telefone do cliente do campo transiente (telefoneClienteTransiente) para
	 * o campo persistente (telefoneCliente).
	 * 
	 * @param bean
	 * @author Hugo Ferreira
	 */
	protected void telefoneTransienteParaPersistente(NotaFiscalServico bean) {
		Telefone telefone = bean.getTelefoneClienteTransiente();
		if (telefone != null && telefone.getCdtelefone() != null) {
			if (telefone.getCdtelefone() != -1) {
				telefone = telefoneService.load(telefone, "telefone.telefone");
				bean.setTelefoneCliente(telefone.getTelefone());
			} else {
				bean.setTelefoneCliente(notaService.load(bean, "nota.telefoneCliente").getTelefoneCliente());
			}
		} 
	}
	
	@Override
	protected void validateBean(NotaFiscalServico bean, BindException errors) {
		if (bean.getListaItens() == null || bean.getListaItens().isEmpty()) {
			errors.reject("001", "Deve haver pelo menos um servi�o na Nota Fiscal.");
		}
		
//		COMENTADO POIS PODE EXISTIR NOTAS DE SERVI�O COM NUMEROS DUPLICADOS
//		if(bean.getEmpresa() != null && StringUtils.isNotBlank(bean.getNumero()) &&
//				notaFiscalServicoService.isExisteNota(bean.getEmpresa(), bean.getNumero(), bean)){
//			errors.reject("001", "J� existe uma nota de servi�o com o n�mero informado.");
//		}
		
		super.validateBean(bean, errors);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request,NotaFiscalServico bean) {
		
		/*
		 * Redireciona para a listagem de produ��o di�ria se a nota foi salva para faturar produ��es di�rias.
		 */
		if(StringUtils.isNotBlank(bean.getCodProducoesDiaria())){
			request.clearMessages();
			request.addMessage("Faturamento de medi��es realizado com sucesso.");
			return new ModelAndView("redirect:/projeto/crud/Producaodiaria?ACAO=" + LISTAGEM);
		}else if(bean.getCdvendaassociada() != null){
			Venda venda = vendaService.loadWithPedidovenda(new Venda(bean.getCdvendaassociada()));
			if(venda != null && venda.getPedidovenda() != null && venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getGerarnotaretornovenda())){
				List<Pedidovenda> listaPedidovenda = pedidovendaService.findForNotaRetorno(venda.getPedidovenda().getCdpedidovenda().toString());
				if(SinedUtil.isListNotEmpty(listaPedidovenda)){
					StringBuilder whereInColetaRetornoVenda = new StringBuilder();
					for(Pedidovenda pedidovenda : listaPedidovenda){
						if(SinedUtil.isListNotEmpty(pedidovenda.getListaColeta())){
							whereInColetaRetornoVenda.append(CollectionsUtil.listAndConcatenate(pedidovenda.getListaColeta(), "cdcoleta", ",")).append(",");
						}
					}
					if(whereInColetaRetornoVenda.length() > 0 && !coletaService.isColetaPossuiNotaTodosItensSemSaldo(whereInColetaRetornoVenda.substring(0, whereInColetaRetornoVenda.length()-1), Tipooperacaonota.SAIDA)){
						SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto?ACAO=criar&notaFiscalRetornoPedido=true&tipooperacaoNota=SAIDA&registrarNF=true&whereInVendaRetorno=" + venda.getCdvenda() + "&whereInColeta=" + whereInColetaRetornoVenda.substring(0, whereInColetaRetornoVenda.length()-1));
						return null; 
					}
				}
			}
		}
		
		return super.getSalvarModelAndView(request, bean);
	}
	
	/**
	 * Chama a a��o entrada com um command obtido da sess�o.
	 * A chave deste command deve ser o retorno do m�todo {@link #getBeanName()}.
	 * O command � removido da sess�o logo em seguida.
	 * 
	 * @param request
	 * @return
	 * @author Fl�vio Tavares
	 */
	@OnErrors(LISTAGEM)
	@Action("criarNotaFiscalServico")
	public ModelAndView criarNotaFiscalServico(WebRequestContext request){
		String modelName = getBeanName();
		NotaFiscalServico nfs = (NotaFiscalServico)request.getSession().getAttribute(modelName);
		request.getSession().removeAttribute(modelName);
		return continueOnAction(ENTRADA, nfs);
	}
	
	@Action("doFaturarRequisicao")
	public ModelAndView fromRequisicao(WebRequestContext request, NotaFiscalServico notaFiscalServico) throws Exception {
		notaFiscalServico = (NotaFiscalServico) request.getSession().getAttribute("notaFiscalServico");
		request.getSession().removeAttribute("nota");
		
		return continueOnAction("entrada", notaFiscalServico);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request,	NotaFiscalServico form) throws CrudException {
		if (form.getCdNota() == null && form.getCdvendaassociada() != null && notaVendaService.existeNotaEmitida(new Venda(form.getCdvendaassociada()))){
			request.addError("J� existe uma nota vinculada a esta venda.");
			String complementoOtr = SinedUtil.complementoOtr();
			return new ModelAndView("redirect:/faturamento/crud/Venda"+complementoOtr+"?ACAO=consultar&cdvenda=" + form.getCdvendaassociada());
		}
		
		if(form.getFromRequisicao() != null && form.getFromRequisicao()){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Nota "+form.getCdNota()+" gerada com sucesso.", MessageType.INFO);
			requisicaoService.updateEstados(form.getWhereIn(), new Requisicaoestado(Requisicaoestado.FATURADO));
			vendaService.updateStatusVenda(form.getWhereIn());
			return new ModelAndView("redirect:/servicointerno/crud/Requisicao?ACAO=listagem");
		}
				
		if(form.getFluxoFaturarPorMedicao() != null &&	form.getFluxoFaturarPorMedicao()){			
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Nota " + form.getCdNota() + " salva com sucesso.", MessageType.INFO);
			return new ModelAndView("redirect:/faturamento/crud/Contrato");
		}else if(form.getFluxoFaturarlocacao() != null && form.getFluxoFaturarlocacao()){			
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Nota " + form.getCdNota() + " salva com sucesso.", MessageType.INFO);
			return new ModelAndView("redirect:/faturamento/crud/Contrato");
		}
		
		if(form.getFromProposta() != null && form.getFromProposta()){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Nota "+form.getCdNota()+" gerada com sucesso.", MessageType.INFO);
			return new ModelAndView("redirect:/crm/crud/Proposta?ACAO=listagem");
		}
		
		return super.doSalvar(request, form);
	}
	
	@Override
	protected void excluir(WebRequestContext request, NotaFiscalServico bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir uma Nota Fiscal.");
	}
	
	public void ajaxOnSelectCliente(WebRequestContext request, Notafiscalproduto notaProduto) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");

		if (notaProduto.getCliente() == null || notaProduto.getCliente().getCdpessoa() == null) {
			view.println("var sucesso = false;");
			return;
		}

		try {
			Cliente cliente = notaProduto.getCliente();
			cliente = clienteService.carregarDadosCliente(cliente);
			
			String razaoSocial = cliente.getRazaosocial() != null ? cliente.getRazaosocial() : "";
			String cnpj = cliente.getCpfOuCnpj() != null ? cliente.getCpfOuCnpj() : "";
			String inscricaoEstadual = cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : "";

			view.println("var sucesso = true;");
			view.println("var razaoSocial = '" + razaoSocial + "';");
			view.println("var inscricaoEstadual = '" + inscricaoEstadual + "';");
			view.println("var cnpjCpf = '" + cnpj + "';");
		} catch (Exception e) {
			view.println("var sucesso = false;");
			view.println("var msg = 'Ocorreu um erro ao recuperar os dados do cliente.';");
		}
	}
	
	public ModelAndView selecionarEmitida(WebRequestContext request){
		Empresa empresa = null;
		String cdempresa = request.getParameter("cdempresa");
		if(cdempresa != null && !cdempresa.trim().equals("")){
			empresa = new Empresa(Integer.parseInt(cdempresa));
		}
		
		List<NotaFiscalServico> listaNota = notaFiscalServicoService.carregarInfoCalcularTotal(notaFiscalServicoService.findEmitidasSubstituicao(empresa));
		return new ModelAndView("direct:/crud/popup/emitidasSubstituicao","lista", listaNota);
	}
	
	public ModelAndView carregaDadosClienteAjax(WebRequestContext request, Cliente cliente){
		cliente = clienteService.carregarDadosCliente(cliente); 
		return new JsonModelAndView().addObject("dadosCliente", cliente);
	}	
	
//	/**
//	 * M�todo que envia o email com o xml da nota para o cliente
//	 *
//	 * @param request
//	 * @return
//	 * @author Luiz Fernando
//	 */
//	public ModelAndView enviarXmlParaCliente(WebRequestContext request){
//		String whereIn = SinedUtil.getItensSelecionados(request);
//		
//		int sucesso = 0;
//		int erro = 0;
//		StringBuilder historico;
//		Arquivo arquivo = null;
//		StringBuilder cdNotasErro = new StringBuilder();
//		
//		if(whereIn == null || "".equals(whereIn)){
//			request.addError("Nenhum item selecionado.");
//			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
//		}
//		
//		List<NotaFiscalServico> listaNotafiscalServico = notaFiscalServicoService.findForEnviarXmlParaCliente(whereIn);
//				
//		if(listaNotafiscalServico != null && !listaNotafiscalServico.isEmpty()){
//			String emailCliente = null;
//			for(NotaFiscalServico nfs : listaNotafiscalServico){
//				emailCliente = null;
//				if(nfs.getCliente() != null && nfs.getCliente().getEmail() != null && !"".equals(nfs.getCliente().getEmail())){
//					emailCliente = nfs.getCliente().getEmail();
//					if(nfs.getListaArquivonfnota() != null && !nfs.getListaArquivonfnota().isEmpty()){
//						for(Arquivonfnota arquivonfnota : nfs.getListaArquivonfnota()){
//							if(arquivonfnota.getArquivonf() != null && arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
//									Arquivonfsituacao.PROCESSADO_COM_SUCESSO.equals(arquivonfnota.getArquivonf().getArquivonfsituacao()) &&
//									arquivonfnota.getArquivonf().getArquivoxml() != null){
//								try {
//									arquivo = arquivoService.loadWithContents(arquivonfnota.getArquivonf().getArquivoxml());									
//									if(arquivo != null){								
//										TemplateManager templateAux = null;
//										String template = null;
//										try{
//											templateAux = new TemplateManager("/WEB-INF/template/templateEnvioXmlNfse.tpl");				
//											template = templateAux
//											.assign("numero", arquivonfnota.getNumeronfse())
//											.getTemplate();
//											
//											try {
//												EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
//												
//												email
//												.setFrom("w3erp@w3erp.com.br")
//												.setTo(emailCliente)
//												.setSubject("Assunto: XML da Nota " + arquivonfnota.getNumeronfse())
//												.attachFileUsingByteArray(arquivo.getContent(), arquivo.getNome(), arquivo.getContenttype(), nfs.getCdNota().toString())
//												.addHtmlText(template)
//												.sendMessage();
//												
//												historico = new StringBuilder();
//												historico.append("Email de envio: " + emailCliente);
//												historico.append("  Assunto: XML da Nota " + arquivonfnota.getNumeronfse());
//												historico.append("  Nome do arquivo: " + arquivo.getNome());
////												historico.append("Mensagem: " + template);												
//												
//												notaFiscalServicoService.registrarHistoricoEnvioEmailXmlCliente(nfs, historico.toString());
//												sucesso++;
//											} catch (Exception e) {											
//												e.printStackTrace();
//												erro++;
//												cdNotasErro.append(cdNotasErro.toString().equals("") ? "," + nfs.getCdNota() : nfs.getCdNota());
//											}
//										} catch (IOException e) {
//											erro++;
//											cdNotasErro.append(cdNotasErro.toString().equals("") ? "," + nfs.getCdNota() : nfs.getCdNota());
//										}									
//									}
//								} catch (Exception e) {
//									e.printStackTrace();
//									request.addError("Falha na obten��o do template do cliente " + nfs.getCliente().getNome() + ".");
//									erro++;
//									cdNotasErro.append(cdNotasErro.toString().equals("") ? "," + nfs.getCdNota() : nfs.getCdNota());
//								}
//								break;
//							}
//						}
//					}
//				}else {
//					erro++;
//					cdNotasErro.append(!cdNotasErro.toString().equals("") ? "," + nfs.getCdNota() : nfs.getCdNota());
//				}
//			}
//			
//			if(sucesso > 0){
//				request.addMessage(sucesso + " Xml da(s) NFS-e(s) enviada(s) para o(s) cliente(s) com sucesso.");
//			}
//			
//			if(erro > 0){
//				request.addError(erro + " Xml da(s) NFS-e(s) n�o foi(ram) enviada(s) para o(s) cliente(s).\n" +
//						"(Notas " + cdNotasErro.toString() + ")");
//			}
//		}
//		
//		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
//	}
	
	/**
	* M�todo que cria a nota a partir da conta a receber
	*
	* @param request
	* @param contareceber
	* @return
	* @throws Exception
	* @since Oct 18, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView criarNotafiscalContareceber(WebRequestContext request, Documento contareceber) throws Exception{
	
		NotaFiscalServico nota = new NotaFiscalServico();
		nota.setUsarSequencial(Boolean.TRUE);
		
		String contas = request.getParameter("contas");
		String[] ids = contas.split(",");
		
		
		
		boolean difPessoa = false, difEmpresa = false, difDtvencimento = false;
		Pessoa pessoa = null;
		Endereco endereco = null;
		Empresa empresa = null;
		Date dtvencimento = null;
		
		List<Integer> vendas = new ArrayList<Integer>();
		List<NotaFiscalServicoItem> items = new ArrayList<NotaFiscalServicoItem>();
		NotaFiscalServicoItem item;
		Money valortotal = new Money();
		String notavalorpagamento = parametrogeralService.getValorPorNome(Parametrogeral.NOTA_VALORPAGAMENTO);
		
		for (int i = 0; i < ids.length; i++) {
			Integer id = Integer.valueOf(ids[i]);
			contareceber = documentoService.findForCriarnotafiscal(new Documento(id));
			
			if(contareceber != null){
				if(notavalorpagamento != null && "TRUE".equalsIgnoreCase(notavalorpagamento) && 
						contareceber.getDocumentoacao() != null && Documentoacao.BAIXADA.equals(contareceber.getDocumentoacao()) && 
						contareceber.getAux_documento() != null && contareceber.getAux_documento().getValoratual() != null){
					valortotal = valortotal.add(contareceber.getAux_documento().getValoratual());
				}else {
					valortotal = valortotal.add(contareceber.getValor());
				}
				
				item = new NotaFiscalServicoItem();
				item.setQtde(1d);
				item.setDescricao(contareceber.getDescricao());
				item.setPrecoUnitario(contareceber.getAux_documento().getValoratual().getValue().doubleValue());
				items.add(item);
				
				if(pessoa == null) pessoa = contareceber.getPessoa();
				else if(!pessoa.equals(contareceber.getPessoa())) {
					pessoa = null;
					difPessoa = true;
				}
				
				if(endereco == null) endereco = contareceber.getEndereco();
				else if(!endereco.equals(contareceber.getEndereco())) endereco = null;
				
				if(empresa == null) empresa = contareceber.getEmpresa();
				else if(!empresa.equals(contareceber.getEmpresa())) {
					empresa = null;
					difEmpresa = true;
				}
				
				if(dtvencimento == null) dtvencimento = contareceber.getDtvencimento();
				else if(!dtvencimento.equals(contareceber.getDtvencimento())) {
					dtvencimento = null;
					difDtvencimento = true;
				}
			}
			
			Integer idvenda = vendapagamentoService.findByConta(id);
			if(idvenda != null) vendas.add(idvenda);
		}
		
		request.setAttribute("valorContareceberValidacao", valortotal);
		
		nota.setVendas(CollectionsUtil.concatenate(vendas, ","));
		nota.setDtEmissao(new Date(System.currentTimeMillis()));
		
		if(!difPessoa && pessoa != null){
			nota.setCliente(new Cliente(pessoa.getCdpessoa(), pessoa.getNome()));	
			nota.setEnderecoCliente(endereco);
		}
		if(!difEmpresa) nota.setEmpresa(empresa);
		if(!difDtvencimento) nota.setDtVencimento(dtvencimento);	
		
		if(nota.getEmpresa() != null){
			notaFiscalServicoService.preencheValoresPadroesEmpresa(nota.getEmpresa(), nota);
		}
		
		request.setAttribute("criarNotafiscalContareceber", Boolean.TRUE);
		setInfoForTemplate(request, nota);
		setEntradaDefaultInfo(request, nota);
		entrada(request, nota);
		
		nota.setListaItens(items);
		nota.setContas(contas);
		
		return getEntradaModelAndView(request, nota);
	}
	
	public void ajaxComboGrupotributacao(WebRequestContext request, NotaFiscalServico notaFiscalServico){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		String notaDtEmissaoString = request.getParameter("notaDtEmissao");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date notaDtEmissao = null;
		try {
			notaDtEmissao = new java.sql.Date(sdf.parse(notaDtEmissaoString).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Cliente cliente = notaFiscalServico.getCliente() != null && notaFiscalServico.getCliente().getCdpessoa() != null ? 
				clienteService.loadForTributacao(notaFiscalServico.getCliente()) : null;
		Naturezaoperacao naturezaoperacao = notaFiscalServico.getNaturezaoperacao();
		Empresa empresa = null;
		if (notaFiscalServico.getEmpresa() != null && notaFiscalServico.getEmpresa().getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(notaFiscalServico.getEmpresa());
		} else {
			empresa = empresaService.loadPrincipalWithEndereco();
		}		
		
		Endereco enderecoDestinoCliente = null;
		if(notaFiscalServico.getEnderecoCliente() != null && notaFiscalServico.getEnderecoCliente().getCdendereco() != null){
			enderecoDestinoCliente = enderecoService.carregaEnderecoComUfPais(notaFiscalServico.getEnderecoCliente());
		}
		
		List<Categoria> listaCategoriaCliente = null;
		if(cliente != null){
			listaCategoriaCliente = categoriaService.findByPessoa(cliente);
		}
		
		List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
				grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
																		empresa, 
																		naturezaoperacao, 
																		null,
																		cliente,
																		false, 
																		null, 
																		null,
																		enderecoDestinoCliente, 
																		null,
																		listaCategoriaCliente,
																		null,
																		null,
																		null,
																		null,
																		notaDtEmissao));
		
		JSON listaGrupotributacaoJSON = View.convertToJson(listaGrupotributacao);
		String jsonGrupotributacao = listaGrupotributacaoJSON.toString(0);
		view.println("var listaGrupotributacao = " + jsonGrupotributacao);
		
		Grupotributacao grupotributacao = notaFiscalServico.getGrupotributacao();
		if(grupotributacao == null || grupotributacao.getCdgrupotributacao() == null || (SinedUtil.isListNotEmpty(listaGrupotributacao) && !listaGrupotributacao.contains(grupotributacao))){ 
			grupotributacao = grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao);
		}
		if(grupotributacao != null){
			view.println("var grupotributacaoSelecionado = 'br.com.linkcom.sined.geral.bean.Grupotributacao[cdgrupotributacao=" + grupotributacao.getCdgrupotributacao() + "]';");
		}
	}
	
	public ModelAndView carregaGrupoTributacaoAjax(WebRequestContext request, NotaFiscalServico notaFiscalServico){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(notaFiscalServico != null && notaFiscalServico.getGrupotributacao() != null){
			
			if(notaFiscalServico.getBasecalculo() == null)
				notaFiscalServico.setBasecalculo(new Money());
			
			if(notaFiscalServico.getCliente() != null){
				Cliente cliente = clienteService.load(notaFiscalServico.getCliente(), "cliente.cdpessoa, cliente.crt, cliente.tipopessoa, cliente.incidiriss");
				notaFiscalServico.setCliente(cliente);
				
				if(notaFiscalServico.getEnderecoCliente() != null){
					Endereco enderecoCliente = enderecoService.loadEndereco(notaFiscalServico.getEnderecoCliente());
					notaFiscalServico.setEnderecoCliente(enderecoCliente);
				}
			}
			
			Grupotributacao grupotributacao = grupotributacaoService.loadForEntrada(notaFiscalServico.getGrupotributacao());
			
			if(notaFiscalServico.getEmpresa() != null){
				Empresa empresa = empresaService.loadForEntrada(notaFiscalServico.getEmpresa());
				notaFiscalServico.setEmpresa(empresa);
				notaFiscalServicoService.preencheValoresPadroesEmpresa(empresa, notaFiscalServico);
			}
			
			if(grupotributacao != null){
				if(grupotributacao.getCodigocnae() != null){
					notaFiscalServico.setCodigocnaeBean(grupotributacao.getCodigocnae());
				}
				if(grupotributacao.getCodigotributacao() != null){
					notaFiscalServico.setCodigotributacao(grupotributacao.getCodigotributacao());
				}
				if(grupotributacao.getItemlistaservico() != null){
					notaFiscalServico.setItemlistaservicoBean(grupotributacao.getItemlistaservico());
				}
			}
			
			notaFiscalServicoService.calculaTributacaoNota(notaFiscalServico, grupotributacao, notaFiscalServico.getBasecalculo());
			
			CalculaAliquotaGrupoTributacaoBean bean = new CalculaAliquotaGrupoTributacaoBean();
			
			if(grupotributacao != null){
				bean.setCodigocnae(grupotributacao.getCodigocnae());
				bean.setCodigotributacao(grupotributacao.getCodigotributacao());
				bean.setItemlistaservico(grupotributacao.getItemlistaservico());
				bean.setDadosAdicionais(grupotributacao.getInfoadicionalcontrib());
				bean.setInformacoesComplementares(grupotributacao.getInfoadicionalfisco());
			}
			
			bean.setNaturezaoperacao(notaFiscalServico.getNaturezaoperacao());
			bean.setMunicipioissqn(notaFiscalServico.getMunicipioissqn());
			bean.setIncentivoFiscalIss(notaFiscalServico.getIncentivoFiscalIss());
			
			bean.setBasecalculoir(notaFiscalServico.getBasecalculoir() != null ? notaFiscalServico.getBasecalculoir() : notaFiscalServico.getBasecalculo());
			bean.setIr(notaFiscalServico.getIr());
			bean.setIncideir(notaFiscalServico.getIncideir());
			bean.setImpostocumulativoir(notaFiscalServico.getImpostocumulativoir());
			
			bean.setBasecalculoinss(notaFiscalServico.getBasecalculoinss() != null ? notaFiscalServico.getBasecalculoinss() : notaFiscalServico.getBasecalculo());
			bean.setInss(notaFiscalServico.getInss());
			bean.setIncideinss(notaFiscalServico.getIncideinss());
			bean.setImpostocumulativoinss(notaFiscalServico.getImpostocumulativoinss());
			
			bean.setBasecalculoiss(notaFiscalServico.getBasecalculoiss() != null ? notaFiscalServico.getBasecalculoiss() : notaFiscalServico.getBasecalculo());
			bean.setIss(notaFiscalServico.getIss());
			bean.setIncideiss(notaFiscalServico.getIncideiss());
			bean.setImpostocumulativoiss(notaFiscalServico.getImpostocumulativoiss());
			
			bean.setBasecalculocsll(notaFiscalServico.getBasecalculocsll() != null ? notaFiscalServico.getBasecalculocsll() : notaFiscalServico.getBasecalculo());
			bean.setCsll(notaFiscalServico.getCsll());
			bean.setIncidecsll(notaFiscalServico.getIncidecsll());
			bean.setImpostocumulativocsll(notaFiscalServico.getImpostocumulativocsll());
			
			bean.setTipocobrancapis(grupotributacao.getTipocobrancapis());
			bean.setBasecalculopis(notaFiscalServico.getBasecalculopis() != null ? notaFiscalServico.getBasecalculopis() : notaFiscalServico.getBasecalculo());
			bean.setPis(notaFiscalServico.getPis());
			bean.setIncidepis(notaFiscalServico.getIncidepis());
			bean.setImpostocumulativopis(notaFiscalServico.getImpostocumulativopis());
			bean.setPisretido(notaFiscalServico.getPisretido());
			
			bean.setTipocobrancacofins(grupotributacao.getTipocobrancacofins());
			bean.setBasecalculocofins(notaFiscalServico.getBasecalculocofins() != null ? notaFiscalServico.getBasecalculocofins() : notaFiscalServico.getBasecalculo());
			bean.setCofins(notaFiscalServico.getCofins());
			bean.setIncidecofins(notaFiscalServico.getIncidecofins());
			bean.setImpostocumulativocofins(notaFiscalServico.getImpostocumulativocofins());
			bean.setCofinsretido(notaFiscalServico.getCofinsretido());
			
			bean.setBasecalculoicms(notaFiscalServico.getBasecalculoicms() != null ? notaFiscalServico.getBasecalculoicms() : notaFiscalServico.getBasecalculo());
			bean.setIcms(notaFiscalServico.getIcms());
			bean.setIncideicms(notaFiscalServico.getIncideicms());
			bean.setImpostocumulativoicms(notaFiscalServico.getImpostocumulativoicms());
			
			jsonModelAndView.addObject("nota", bean);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * Action em ajax para fazer o carregamento das informa��es da empresa.
	 *
	 * @param request
	 * @param notaFiscalServico
	 * @return
	 * @since 26/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView carregaDadosEmpresaAjax(WebRequestContext request, NotaFiscalServico notaFiscalServico){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(notaFiscalServico != null && notaFiscalServico.getEmpresa() != null){
			notaFiscalServicoService.preencheValoresPadroesEmpresa(notaFiscalServico.getEmpresa(), notaFiscalServico);
			
			if(notaFiscalServico.getCodigocnaeBean() != null){
				jsonModelAndView.addObject("codigocnae_value", SinedUtil.getIdAutocomplete(notaFiscalServico.getCodigocnaeBean(), "codigocnae.cnae, codigocnae.descricaocnae"));
				jsonModelAndView.addObject("codigocnae_label", notaFiscalServico.getCodigocnaeBean().getDescricaoCombo());
			}
			
			if(notaFiscalServico.getCodigotributacao() != null){
				jsonModelAndView.addObject("codigotributacao_value", SinedUtil.getIdAutocomplete(notaFiscalServico.getCodigotributacao(), "codigotributacao.codigo, codigotributacao.descricao"));
				jsonModelAndView.addObject("codigotributacao_label", notaFiscalServico.getCodigotributacao().getDescricaoCombo());
			}
			
			if(notaFiscalServico.getItemlistaservicoBean() != null){
				jsonModelAndView.addObject("itemlistaservico_value", SinedUtil.getIdAutocomplete(notaFiscalServico.getItemlistaservicoBean(), "itemlistaservico.codigo, itemlistaservico.descricao"));
				jsonModelAndView.addObject("itemlistaservico_label", notaFiscalServico.getItemlistaservicoBean().getDescricaoCombo());
			}
			
			if(notaFiscalServico.getNaturezaoperacao() != null){
				jsonModelAndView.addObject("naturezaoperacao", "br.com.linkcom.sined.geral.bean.Naturezaoperacao[cdnaturezaoperacao=" + notaFiscalServico.getNaturezaoperacao().getCdnaturezaoperacao() + "]");
			}
			
			if(notaFiscalServico.getRegimetributacao() != null){
				jsonModelAndView.addObject("regimetributacao", "br.com.linkcom.sined.geral.bean.Regimetributacao[cdregimetributacao=" + notaFiscalServico.getRegimetributacao().getCdregimetributacao() + "]");
			}
			
			if(notaFiscalServico.getMunicipioissqn() != null){
				jsonModelAndView.addObject("municipio_value", SinedUtil.getIdAutocomplete(notaFiscalServico.getMunicipioissqn(), null));
				jsonModelAndView.addObject("municipio_label", notaFiscalServico.getMunicipioissqn().getNomecompleto());
			}
			
			if(notaFiscalServico.getCstpis() != null){
				jsonModelAndView.addObject("cstpis", notaFiscalServico.getCstpis().getCdnfe());
			}
			
			if(notaFiscalServico.getCstcofins() != null){
				jsonModelAndView.addObject("cstcofins", notaFiscalServico.getCstcofins().getCdnfe());
			}			
			
			if (notaFiscalServico.getIncentivoFiscalIss() != null) {
				jsonModelAndView.addObject("incentivoFiscalIss", notaFiscalServico.getIncentivoFiscalIss());
			}
			
			jsonModelAndView.addObject("sucesso", Boolean.TRUE);
		} else {
			jsonModelAndView.addObject("sucesso", Boolean.FALSE);
		}
		
		return jsonModelAndView;
	}
	
	public void ajaxCodigosSecao(WebRequestContext request){
    	try{
	    	String whereIn = SinedUtil.getItensSelecionados(request);
	    	request.getSession().setAttribute("CODIGOS_NOTAFISCALSERVICO_FATURAMENTO", whereIn);
	    	View.getCurrent().println("var sucesso = true;");
    	} catch (Exception e) {
    		View.getCurrent().println("var sucesso = false;");
		}
    }
	
	public ModelAndView ajaxVerificaNaturezaOperacaoForaMunicipio(WebRequestContext request, NotaFiscalServico notaFiscalServico){
		JsonModelAndView json = new JsonModelAndView();
		if(notaFiscalServico != null && 
				notaFiscalServico.getEmpresa() != null && 
				notaFiscalServico.getEmpresa().getCdpessoa() != null && 
				notaFiscalServico.getEnderecoCliente() != null &&
				notaFiscalServico.getEnderecoCliente().getCdendereco() != null){
			
			Empresa empresa = empresaService.loadForEntrada(notaFiscalServico.getEmpresa());
			Cliente cliente = clienteService.load(notaFiscalServico.getCliente(), "cliente.cdpessoa, cliente.incidiriss");
			
			if((empresa.getTributacaomunicipiocliente() != null && empresa.getTributacaomunicipiocliente()) || 
					(cliente != null && cliente.getIncidiriss() != null && cliente.getIncidiriss())){
				Endereco enderecoEmpresa = empresa.getEndereco();
				Endereco enderecoCliente = enderecoService.loadEndereco(notaFiscalServico.getEnderecoCliente());
				
				boolean municipioDiferente = enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null && enderecoEmpresa.getMunicipio().getCdmunicipio() != null &&
												enderecoCliente != null && enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getCdmunicipio() != null &&
												!enderecoEmpresa.getMunicipio().equals(enderecoCliente.getMunicipio());
				
				if(municipioDiferente){
					json.addObject("naturezaoperacao", "br.com.linkcom.sined.geral.bean.Naturezaoperacao[cdnaturezaoperacao=" + Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO.getCdnaturezaoperacao() + "]");
					json.addObject("municipioissqn", "br.com.linkcom.sined.geral.bean.Municipio[cdmunicipio=" + enderecoCliente.getMunicipio().getCdmunicipio() + "]");
					json.addObject("municipioissqn_label", enderecoCliente.getMunicipio().getNomecompleto());
				}
			}
		}
		return json;
	}
	
	/**
	 * M�todo que gera a nota fiscal a partir da proposta
	 *
	 * @param request
	 * @param proposta
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	@Action("gerarContrato")
	public ModelAndView gerarNotaByProposta(WebRequestContext request, Proposta proposta) throws Exception{
		if(proposta.getCdproposta() == null){
			request.addError("Nenhum item selecionado");
			return new ModelAndView("redirect:/crm/crud/Proposta");
		}
		
		proposta = propostaService.loadForNotafiscalservico(proposta);
		if(proposta == null || !Propostasituacao.GANHOU.equals(proposta.getPropostasituacao())){
			request.addError("S� � permitido gerar nota fiscal de proposta com situa��o 'GANHOU'.");
			return new ModelAndView("redirect:/crm/crud/Proposta");
		}
		
		NotaFiscalServico form = notaFiscalServicoService.gerarNotaByProposta(proposta);
		return getCriarModelAndView(request, form);
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public ModelAndView abrirAtualizarDados(WebRequestContext request){
    	String idnota = SinedUtil.getItensSelecionados(request);
    	if(idnota != null){    		
	    	if(!notaFiscalServicoService.validaNotaStatus(idnota)){	    		
		    		request.addError("As notas precisam ter as situa��es: 'Nota Emitida' ou 'Nota Liberada' para a atualiza��o.");
					SinedUtil.fechaPopUp(request);
					return null;
		    }
	    	request.setAttribute("titulo", "ATUALIZAR DADOS");
	    	request.setAttribute("acaoSalvar", "atualizarDados");
	    	request.setAttribute("clearBase", true);
	    	request.setAttribute("idNota", idnota);
    	}else{
    		request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		return new ModelAndView("direct:/crud/popup/atualizaDadosNFServico");
	}
	
	/**
	 * 
	 * @param request
	 * @param contrato
	 * @return
	 */
	public ModelAndView atualizarDados(WebRequestContext request, NotaFiscalServico notaFiscalServico){		
		String idnota = (String) request.getParameter("idNota");		
		if(idnota!=null){
			notaFiscalServicoService.atualizaDados(idnota,notaFiscalServico);
			notaHistoricoService.adicionaHistoricoAtualizar(idnota);
			request.addMessage("Registros salvos com sucesso.");
		}else
			request.addError("Erro ao atualizar os dados.");
		
    	request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>parent.location = parent.location;parent.$.akModalRemove(true);</script>");
		return null;		
	}
	
	/**
	 * Action que abre a popup para o envio de e-mail de nota
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/07/2014
	 */
	public ModelAndView popupEmailNota(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFSE_EMITIDA, NotaStatus.NFSE_LIQUIDADA)){
			request.addError("Para envio da NF-e as notas tem que estar com a situa��o 'NFS-e EMITIDA' ou 'NFS-e LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		return notaService.preparePopupEnvioEmail(request, whereIn, NotaTipo.NOTA_FISCAL_SERVICO, true);
	}
	
	/**
	 * M�todo que vai realizar o envio de e-mail da nota vinda da popup.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 03/07/2014
	 */
	public void savePopupEmailNota(WebRequestContext request, EnvioEmailNotaBean bean){
		request.setAttribute("selectedItens", bean.getWhereInNota());
		notaService.savePopupEnvioEmailNota(request, bean, NotaTipo.NOTA_FISCAL_SERVICO, true);
	}
	
	/**
	 * Retorna uma a��o em ajax a inscri��o estadual
	 *
	 * @param request
	 * @param notaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/07/2015
	 */
	public ModelAndView carregaInscricaoEstadualAjax(WebRequestContext request, Nota nota){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		String inscricaoestadual = notaService.getInscricaoEstadualClienteEndereco(nota);
		return jsonModelAndView.addObject("inscricaoestadual", inscricaoestadual != null ? inscricaoestadual : "");
	}
	
	/**
	 * A��o de gerar RPS para a NFS-e de Votorantim
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/04/2016
	 */
	public ModelAndView gerarRPS(WebRequestContext request){
		String entrada = request.getParameter("entrada");
		String whereIn = SinedUtil.getItensSelecionados(request);
		ModelAndView redirect = entrada != null && entrada.equalsIgnoreCase("true") ?  sendRedirectToAction("consultar", "cdNota=" + whereIn) : sendRedirectToAction("listagem");
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.EMITIDA, NotaStatus.LIQUIDADA)){
			request.addError("Para gera��o de RPS as notas tem que estar com a situa��o 'EMITIDA' ou 'LIQUIDADA'.");
			return redirect;
		}
		
		int sucesso = notaFiscalServicoRPSService.gerarRPS(request, whereIn);
		if(sucesso > 0){
			request.addMessage(sucesso + " RPS(s) gerado(s) com sucesso.");
		} else {
			request.addError("Nenhum RPS gerado.");
		}
		return redirect;
	}
	
	/**
	 * Verifica se existe a configura��o de NFS-e para a cidade de votorantim
	 *
	 * @param request
	 * @param notaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/04/2016
	 */
	public ModelAndView verificaNumeroDestivadoAjax(WebRequestContext request, NotaFiscalServico notaFiscalServico){
		boolean haveVotorantim = configuracaonfeService.havePrefixoativo(notaFiscalServico.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
		return new JsonModelAndView().addObject("haveVotorantim", haveVotorantim);
	}
	
	public ModelAndView ajaxMontaInfoContribuinteTemplate(WebRequestContext request, NotaFiscalServico bean){
		String infContribuinteTemplate = notaFiscalServicoService.montaInformacoesContribuinteTemplate(bean.getCdvendaassociada() != null ? bean.getCdvendaassociada().toString() : null, bean);
   		return new JsonModelAndView().addObject("infContribuinteTemplate", infContribuinteTemplate);
	}
	
	public void carregaContaByEmpresa(WebRequestContext request, NotaFiscalServico nota) throws Exception{
		notafiscalprodutoService.carregaContaByEmpresa(request, nota);
	}
	
	public ModelAndView parcelasCobranca(WebRequestContext request, ParcelasCobrancaBean bean){	
		return notafiscalprodutoService.parcelasCobranca(request, bean);
	}
	
	public void buscarInfPrazopagamento(WebRequestContext request, NotaFiscalServico nota) throws Exception{
		notafiscalprodutoService.buscarInfPrazopagamento(request, nota.getPrazopagamentofatura());
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView abrirPopUpInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView ajaxInfoDocumentotipo(WebRequestContext request){
		return documentotipoService.ajaxInfoDocumentotipo(request);
	}

	public ModelAndView ajaxPrazopagamento(WebRequestContext request, NotaFiscalServico notaFiscalServico){
		return prazopagamentoService.ajaxPrazopagamentoParcelasiguaisjuros(notaFiscalServico.getPrazopagamentofatura());
	}
}