package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.sf.json.JSON;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Ajustefiscal;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Arquivonfnotaerro;
import br.com.linkcom.sined.geral.bean.Arquivotef;
import br.com.linkcom.sined.geral.bean.Arquivotefitem;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Cfoptipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigoanp;
import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialDevolucao;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Coletahistorico;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresacodigocnae;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.EntregadocumentoColeta;
import br.com.linkcom.sined.geral.bean.Entregadocumentohistorico;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaoproducao;
import br.com.linkcom.sined.geral.bean.Expedicaoproducaoitem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Gnre;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materiallegenda;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocfop;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocst;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalProdutoRestricao;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notaerrocorrecao;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.NotafiscalprodutoColeta;
import br.com.linkcom.sined.geral.bean.NotafiscalprodutoProducaoordem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoduplicata;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitementregamaterial;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoreferenciada;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoretornopedidovenda;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoretornovenda;
import br.com.linkcom.sined.geral.bean.Notaromaneio;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemhistorico;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_MovimentacaoEstoque;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivotefsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.ImpostoEcfEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Inutilizacaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.NotafiscalprodutoreferenciaModeloEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.PagamentoTEFSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tiponotareferencia;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.service.AjustefiscalService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaerroService;
import br.com.linkcom.sined.geral.service.ArquivotefService;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColetaMaterialDevolucaoService;
import br.com.linkcom.sined.geral.service.ColetaMaterialService;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.ColetahistoricoService;
import br.com.linkcom.sined.geral.service.ColetamaterialmotivodevolucaoService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.DeclaracaoExportacaoNotaService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmporiumvendaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EmpresaconfiguracaonfService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.EntregadocumentoColetaService;
import br.com.linkcom.sined.geral.service.EntregadocumentohistoricoService;
import br.com.linkcom.sined.geral.service.ExpedicaoService;
import br.com.linkcom.sined.geral.service.ExpedicaoproducaoService;
import br.com.linkcom.sined.geral.service.ExpedicaoproducaohistoricoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.GarantiareformaService;
import br.com.linkcom.sined.geral.service.GnreService;
import br.com.linkcom.sined.geral.service.GrupotributacaoService;
import br.com.linkcom.sined.geral.service.InutilizacaonfeService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialformulavalorvendaService;
import br.com.linkcom.sined.geral.service.MateriallegendaService;
import br.com.linkcom.sined.geral.service.MaterialnumeroserieService;
import br.com.linkcom.sined.geral.service.MemorandoexportacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.geral.service.MovpatrimonioorigemService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaocfopService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotaVendaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoColetaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoProducaoordemService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoautorizacaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutocorrecaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoduplicataService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitementregamaterialService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoreferenciadaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoretornopedidovendaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoretornovendaService;
import br.com.linkcom.sined.geral.service.NotaromaneioService;
import br.com.linkcom.sined.geral.service.PaisService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProducaoagendaService;
import br.com.linkcom.sined.geral.service.ProducaoordemService;
import br.com.linkcom.sined.geral.service.ProducaoordemmaterialService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.TabelaimpostoService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.geral.service.VendamaterialmestreService;
import br.com.linkcom.sined.geral.service.VendapagamentoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ColetamaterialNotaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioEmailNotaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GeraReceita;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ParcelasCobrancaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.SituacaoNotaEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotafiscalprodutoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PopUpIcmsFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PopUpImpostoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.ImportarXmlDeclaracaoImportacaoProcess;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoitemBean;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.ImpostoUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/faturamento/crud/Notafiscalproduto", "/faturamento/crud/Notafiscalconsumidor"}, authorizationModule=CrudAuthorizationModule.class)
public class NotafiscalprodutoCrud extends CrudControllerSined<NotafiscalprodutoFiltro, Notafiscalproduto, Notafiscalproduto> {
	
	private MaterialService materialService;
	private VendaService vendaService;
	private VendamaterialService vendamaterialService;
	private EmpresaService empresaService;
	private NotaHistoricoService notaHistoricoService;
	private ClienteService clienteService;
	private TelefoneService telefoneService;
	private NotaService notaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private FornecedorService fornecedorService;
	private NotafiscalprodutoduplicataService notafiscalprodutoduplicataService;
	private CfopService cfopService;
	private NotafiscalprodutoreferenciadaService notafiscalprodutoreferenciadaService;
	private EmpresaconfiguracaonfService empresaconfiguracaonfService;
	private EnderecoService enderecoService;
	private ArquivonfnotaService arquivonfnotaService;
	private EntregaService entregaService;
	private MaterialnumeroserieService materialnumeroserieService;
	private NotafiscalprodutocorrecaoService notafiscalprodutocorrecaoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ParametrogeralService parametrogeralService;
	private ColaboradorService colaboradorService;
	private EntradafiscalService entradafiscalService;
	private ExpedicaoproducaoService expedicaoproducaoService;
	private ExpedicaoproducaohistoricoService expedicaoproducaohistoricoService;
	private EntregadocumentohistoricoService entregadocumentohistoricoService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService;
	private UnidademedidaService unidademedidaService;
	private LoteestoqueService loteestoqueService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private NaturezaoperacaocfopService naturezaoperacaocfopService;
	private NotaVendaService notaVendaService;
	private PedidovendaService pedidovendaService;
	private EmporiumvendaService emporiumvendaService;
	private DocumentoService documentoService;
	private MovpatrimonioService movpatrimonioService;
	private MovpatrimonioorigemService movpatrimonioorigemService;
	private VendamaterialmestreService vendamaterialmestreService;
	private ExpedicaoService expedicaoService;
	private ProducaoagendaService producaoagendaService;
	private GrupotributacaoService grupotributacaoService;
	private MunicipioService municipioService;
	private MateriallegendaService materiallegendaService;
	private NotaromaneioService notaromaneioService;
	private ConfiguracaonfeService configuracaonfeService;
	private NotaDocumentoService notaDocumentoService;
	private MaterialformulavalorvendaService materialformulavalorvendaService;
	private ColetaService coletaService;
	private NotafiscalprodutoColetaService notafiscalprodutoColetaService;
	private ColetaMaterialService coletaMaterialService;
	private ColetaMaterialDevolucaoService coletaMaterialDevolucaoService;
	private ReportTemplateService reportTemplateService;
	private ProducaoordemmaterialService producaoordemmaterialService;
	private ColetahistoricoService coletahistoricoService;
	private RequisicaoService requisicaoService; 
	private InutilizacaonfeService inutilizacaonfeService;
	private CategoriaService categoriaService;
	private NotafiscalprodutoautorizacaoService notafiscalprodutoautorizacaoService;
	private AjustefiscalService ajustefiscalService;
	private ProducaoordemService producaoordemService;
	private TabelaimpostoService tabelaimpostoService; 
	private NotafiscalprodutoitementregamaterialService notafiscalprodutoitementregamaterialService;
	private MemorandoexportacaoService memorandoexportacaoService;
	private UsuarioService usuarioService;
	private NotafiscalprodutoProducaoordemService notafiscalprodutoProducaoordemService;
	private ProjetoService projetoService;
	private LocalarmazenagemService localarmazenagemService;
	private PedidovendatipoService pedidovendatipoService;
	private DocumentotipoService documentotipoService;
	private PaisService paisService;
	private PrazopagamentoService prazopagamentoService;
	private NotafiscalprodutoretornopedidovendaService notafiscalprodutoretornopedidovendaService;
	private NotafiscalprodutoretornovendaService notafiscalprodutoretornovendaService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private VendahistoricoService vendahistoricoService;
	private GarantiareformaService garantiareformaService;
	private EntregadocumentoColetaService entregadocumentoColetaService;
	private ColetamaterialmotivodevolucaoService coletamaterialmotivodevolucaoService;
	private ContareceberService contareceberService;
	private VendapagamentoService vendapagamentoService;
	private DocumentohistoricoService documentohistoricoService;
	private DeclaracaoExportacaoNotaService declaracaoExportacaoNotaService;
	private CentrocustoService centrocustoService;
	private RateioService rateioService;
	private ArquivonfnotaerroService arquivonfnotaerroService;
	private InventarioService inventarioService;
	private DocumentoorigemService documentoorigemService;
	private ArquivotefService arquivotefService;
	private GnreService gnreService;
	
	public void setGnreService(GnreService gnreService) {this.gnreService = gnreService;}
	public void setArquivotefService(ArquivotefService arquivotefService) {this.arquivotefService = arquivotefService;}
	public void setArquivonfnotaerroService(ArquivonfnotaerroService arquivonfnotaerroService) {this.arquivonfnotaerroService = arquivonfnotaerroService;}
	public void setDeclaracaoExportacaoNotaService(DeclaracaoExportacaoNotaService declaracaoExportacaoNotaService) {this.declaracaoExportacaoNotaService = declaracaoExportacaoNotaService;}
	public void setTabelaimpostoService(TabelaimpostoService tabelaimpostoService) {this.tabelaimpostoService = tabelaimpostoService;}
	public void setNotafiscalprodutoautorizacaoService(NotafiscalprodutoautorizacaoService notafiscalprodutoautorizacaoService) {this.notafiscalprodutoautorizacaoService = notafiscalprodutoautorizacaoService;}
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setInutilizacaonfeService(InutilizacaonfeService inutilizacaonfeService) {this.inutilizacaonfeService = inutilizacaonfeService;}
	public void setColetahistoricoService(ColetahistoricoService coletahistoricoService) {this.coletahistoricoService = coletahistoricoService;}
	public void setProducaoordemmaterialService(ProducaoordemmaterialService producaoordemmaterialService) {this.producaoordemmaterialService = producaoordemmaterialService;}
	public void setNotaromaneioService(NotaromaneioService notaromaneioService) {this.notaromaneioService = notaromaneioService;}
	public void setMateriallegendaService(MateriallegendaService materiallegendaService) {this.materiallegendaService = materiallegendaService;}
	public void setMovpatrimonioorigemService(MovpatrimonioorigemService movpatrimonioorigemService) {this.movpatrimonioorigemService = movpatrimonioorigemService;}
	public void setMovpatrimonioService(MovpatrimonioService movpatrimonioService) {this.movpatrimonioService = movpatrimonioService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setEmporiumvendaService(EmporiumvendaService emporiumvendaService) {this.emporiumvendaService = emporiumvendaService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setNotafiscalprodutocorrecaoService(NotafiscalprodutocorrecaoService notafiscalprodutocorrecaoService) {this.notafiscalprodutocorrecaoService = notafiscalprodutocorrecaoService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setNotafiscalprodutoreferenciadaService(NotafiscalprodutoreferenciadaService notafiscalprodutoreferenciadaService) {this.notafiscalprodutoreferenciadaService = notafiscalprodutoreferenciadaService;}
	public void setNotafiscalprodutoduplicataService(NotafiscalprodutoduplicataService notafiscalprodutoduplicataService) {this.notafiscalprodutoduplicataService = notafiscalprodutoduplicataService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setCfopService(CfopService cfopService) {this.cfopService = cfopService;}
	public void setEmpresaconfiguracaonfService(EmpresaconfiguracaonfService empresaconfiguracaonfService) {this.empresaconfiguracaonfService = empresaconfiguracaonfService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setEntregaService(EntregaService entregaService) {this.entregaService = entregaService;}
	public void setMaterialnumeroserieService(MaterialnumeroserieService materialnumeroserieService) {this.materialnumeroserieService = materialnumeroserieService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setExpedicaoproducaoService(ExpedicaoproducaoService expedicaoproducaoService) {this.expedicaoproducaoService = expedicaoproducaoService;}
	public void setExpedicaoproducaohistoricoService(ExpedicaoproducaohistoricoService expedicaoproducaohistoricoService) {this.expedicaoproducaohistoricoService = expedicaoproducaohistoricoService;}
	public void setEntregadocumentohistoricoService(EntregadocumentohistoricoService entregadocumentohistoricoService) {this.entregadocumentohistoricoService = entregadocumentohistoricoService;}
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {this.notafiscalprodutoitemService = notafiscalprodutoitemService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setNaturezaoperacaocfopService(NaturezaoperacaocfopService naturezaoperacaocfopService) {this.naturezaoperacaocfopService = naturezaoperacaocfopService;}
	public void setNotaVendaService(NotaVendaService notaVendaService) {this.notaVendaService = notaVendaService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setVendamaterialmestreService(VendamaterialmestreService vendamaterialmestreService) {this.vendamaterialmestreService = vendamaterialmestreService;}
	public void setExpedicaoService(ExpedicaoService expedicaoService) {this.expedicaoService = expedicaoService;}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {this.producaoagendaService = producaoagendaService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {this.materialformulavalorvendaService = materialformulavalorvendaService;}
	public void setColetaService(ColetaService coletaService) {this.coletaService = coletaService;}
	public void setNotafiscalprodutoColetaService(NotafiscalprodutoColetaService notafiscalprodutoColetaService) {this.notafiscalprodutoColetaService = notafiscalprodutoColetaService;}
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {this.coletaMaterialService = coletaMaterialService;}
	public void setColetaMaterialDevolucaoService(ColetaMaterialDevolucaoService coletaMaterialDevolucaoService) {this.coletaMaterialDevolucaoService = coletaMaterialDevolucaoService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setAjustefiscalService(AjustefiscalService ajustefiscalService) {this.ajustefiscalService = ajustefiscalService;}
	public void setProducaoordemService(ProducaoordemService producaoordemService) {this.producaoordemService = producaoordemService;}
	public void setNotafiscalprodutoitementregamaterialService(NotafiscalprodutoitementregamaterialService notafiscalprodutoitementregamaterialService) {this.notafiscalprodutoitementregamaterialService = notafiscalprodutoitementregamaterialService;}
	public void setMemorandoexportacaoService(MemorandoexportacaoService memorandoexportacaoService) {this.memorandoexportacaoService = memorandoexportacaoService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setNotafiscalprodutoProducaoordemService(NotafiscalprodutoProducaoordemService notafiscalprodutoProducaoordemService) {this.notafiscalprodutoProducaoordemService = notafiscalprodutoProducaoordemService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setPaisService(PaisService paisService) {this.paisService = paisService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {this.pedidovendahistoricoService = pedidovendahistoricoService;}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {this.vendahistoricoService = vendahistoricoService;}
	public void setNotafiscalprodutoretornopedidovendaService(NotafiscalprodutoretornopedidovendaService notafiscalprodutoretornopedidovendaService) {this.notafiscalprodutoretornopedidovendaService = notafiscalprodutoretornopedidovendaService;}
	public void setNotafiscalprodutoretornovendaService(NotafiscalprodutoretornovendaService notafiscalprodutoretornovendaService) {this.notafiscalprodutoretornovendaService = notafiscalprodutoretornovendaService;}
	public void setGarantiareformaService(GarantiareformaService garantiareformaService) {this.garantiareformaService = garantiareformaService;}
	public void setEntregadocumentoColetaService(EntregadocumentoColetaService entregadocumentoColetaService) {this.entregadocumentoColetaService = entregadocumentoColetaService;}
	public void setColetamaterialmotivodevolucaoService(ColetamaterialmotivodevolucaoService coletamaterialmotivodevolucaoService) {this.coletamaterialmotivodevolucaoService = coletamaterialmotivodevolucaoService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}	
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}	
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {this.vendapagamentoService = vendapagamentoService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setInventarioService(InventarioService inventarioService) {this.inventarioService = inventarioService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	
	
	
	@Override
	public ModelAndView doEditar(WebRequestContext request,	Notafiscalproduto form) throws CrudException {
		Usuario usuario = SinedUtil.getUsuarioLogado();
		String acao = request.getParameter("ACAO");
		if(acao != null && ("editar".equals(acao) || "consultar".equals(acao)) && usuarioService.isRestricaoClienteVendedor(usuario) ){
			Nota nota = notaService.loadWithCliente(form);
			if(nota != null && nota.getCliente() != null && nota.getCliente().getCdpessoa() != null && 
					!clienteService.isUsuarioPertenceRestricaoclientevendedor(nota.getCliente(), usuario)){
				request.addError("Usu�rio n�o tem permiss�o para consultar/editar esta nota!");
				SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto");
				return null;
			}
		}
		return super.doEditar(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, NotafiscalprodutoFiltro filtro) throws Exception {
		request.setAttribute("isNfe", isNfe());
		request.setAttribute("listaNotaStatus", NotaStatus.getTodosEstadosProduto());
		boolean danfeConfiguravel = false;
		try {
			danfeConfiguravel = reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.DANFE)!=null;
		}catch (Exception e){}
		
		request.setAttribute("danfeConfiguravel", danfeConfiguravel);
		request.setAttribute("isRepagem",SinedUtil.isRecapagem());
		boolean isExpedicaoNota = false;
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("NOTA") && SinedUtil.isRecapagem()){
			isExpedicaoNota = true;
		}
		
		List<SituacaoVinculoExpedicao> listaSituacaoExpedicao = new ArrayList<SituacaoVinculoExpedicao>();
		SituacaoVinculoExpedicao[] arraySitucao = SituacaoVinculoExpedicao.values();
		for (SituacaoVinculoExpedicao situacao : arraySitucao ) {
				if(!SituacaoVinculoExpedicao.EXPEDI��O_PARCIAL.equals(situacao)){
					listaSituacaoExpedicao.add(situacao);
				}
		}

		request.setAttribute("listaSituacaoExpedicao", listaSituacaoExpedicao);
		request.setAttribute("isExpedicaoNota", isExpedicaoNota);
		filtro.setConfiguracao(empresaconfiguracaonfService.verificaConfiguracao());
		configuracaonfeService.setEstadosConfigNFe(request);
	}	
	
	private boolean isNfe() {
		return NeoWeb.getRequestContext().getServletRequest().getRequestURI().endsWith("/faturamento/crud/Notafiscalproduto");
	}
	
	private ModeloDocumentoFiscalEnum getModelo() {
		return isNfe() ? ModeloDocumentoFiscalEnum.NFE : ModeloDocumentoFiscalEnum.NFCE;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, NotafiscalprodutoFiltro filtro) throws CrudException {
		filtro.setModeloDocumentoFiscalEnum(getModelo());
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected Notafiscalproduto criar(WebRequestContext request, Notafiscalproduto form) throws Exception {
		Notafiscalproduto bean = super.criar(request, form);
		String copiar = request.getParameter("copiar");
		if(copiar != null && "true".equals(copiar)){
			bean = notafiscalprodutoService.criaCopia(form);
			bean.setSerienfe(null);
		}			
		
		bean.setDevolucaoColetaConfirmada("true".equals(request.getParameter("devolucaoColetaConfirmada")));
		bean.setRegistrarDevolucaoColeta("true".equals(request.getParameter("registrarDevolucaoColeta")));
		bean.setTipooperacaonota(Tipooperacaonota.SAIDA);
		bean.setFinalidadenfe(Finalidadenfe.NORMAL);
		bean.setFormapagamentonfe(Formapagamentonfe.A_PRAZO);
		bean.setModeloDocumentoFiscalEnum(getModelo());
		Naturezaoperacao naturezaPadrao = naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_PRODUTO);
		if(naturezaPadrao != null){
			bean.setNaturezaoperacao(naturezaPadrao);
			if(naturezaPadrao.getOperacaonfe() != null){
				bean.setOperacaonfe(naturezaPadrao.getOperacaonfe());
			}
			if(naturezaPadrao.getFinalidadenfe() != null){
				bean.setFinalidadenfe(naturezaPadrao.getFinalidadenfe());
			}
		}
		bean.setCadastrarcobranca(Boolean.TRUE);
		bean.setCadastrartransportador(Boolean.TRUE);

		if(bean.getSerienfe() == null && bean.getEmpresa() != null){
			Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.serienfe");
			bean.setSerienfe(empresa.getSerienfe());
		}
		
		List<Venda> listaVenda = new ArrayList<Venda>();
		String cdvenda = request.getParameter("cdvenda");		
		if (cdvenda != null){
			listaVenda.add(new Venda(Integer.parseInt(cdvenda)));
			Venda venda = vendaService.loadWithVendasituacao(new Venda(Integer.parseInt(cdvenda)));
			if ("TRUE".equals(parametrogeralService.buscaValorPorNome("ENDERECO_ENTREGA_NA_NOTA"))){
				Venda venda_aux = vendaService.load(venda, "venda.endereco");
				
				if (venda_aux != null  && venda_aux.getEndereco() != null){
					bean.setInfoadicionalcontrib("Endere�o de entrega: " + venda_aux.getEndereco().getLogradouroCompletoComBairroSemMunicipio() + "; ");
				}			
			}
			
			if(notaVendaService.existNotaEmitida(venda, NotaTipo.NOTA_FISCAL_PRODUTO)){
				throw new SinedException("A venda j� possui nota fiscal de produto.");
			}
			Producaoagendasituacao producaoagendasituacao = producaoagendaService.getProducaoagendasituacaoByVenda(venda); 
			if((Vendasituacao.EMPRODUCAO.equals(venda.getVendasituacao()) && !vendaService.existsProducaoagenda(venda)) ||
				(producaoagendasituacao != null && !Producaoagendasituacao.CONCLUIDA.equals(producaoagendasituacao))){
				throw new SinedException("� preciso finalizar o processo de produ��o antes de criar a nota.");
			}
			String expedicoes = request.getParameter("expedicoes");
			String whereInCdentregamaterial = request.getParameter("whereInCdentregamaterial");
			String whereInQtdeEntrada = request.getParameter("whereInQtdeEntrada");
			Boolean somenteproduto = Boolean.valueOf(request.getParameter("somente_produto") != null ? request.getParameter("somente_produto") : "false");
			bean = notafiscalprodutoService.geraNotaVenda(bean, venda, expedicoes, somenteproduto, whereInCdentregamaterial, whereInQtdeEntrada);
			
			if(emporiumvendaService.haveVenda(venda)){
				request.setAttribute("fromVendaEmporium", Boolean.TRUE);
				request.setAttribute("whereInVenda", cdvenda);
			}
			if(request.getParameter("somente_produto") != null && "true".equals(request.getParameter("somente_produto"))){
				bean.setSomenteproduto(true);
			}
			request.setAttribute("vendaIndustrializacaoRetorno", vendaService.isVendaIndustrializacaoRetorno(cdvenda));
		}

		String whereInCdvenda = request.getParameter("whereInCdvenda");
		String qtdParcelas = request.getParameter("qtdParcelas");
		if (whereInCdvenda != null) {
			List<Venda> listaVendas = vendaService.findForCobranca(whereInCdvenda);
			List<Notafiscalproduto> notas = notafiscalprodutoService.getListaNotafiscalproduto(request, listaVendas, true, false, false);
			
			if (notas != null && notas.size() == 1) {
				bean = notas.get(0);
				bean.setAgruparContas(true);
				bean.setQtdParcelas(Integer.parseInt(qtdParcelas));
			}
			
			bean.setWhereInCdvenda(whereInCdvenda);
		}
		
		String whereInexpedicao = request.getParameter("whereInexpedicao");		
		if (whereInexpedicao != null && !"".equals(whereInexpedicao)){
			List<Expedicao> listaExpedicao = expedicaoService.findForNota(whereInexpedicao);
			listaExpedicao = expedicaoService.createListaExpedicaoForNota(listaExpedicao);
			if(SinedUtil.isListNotEmpty(listaExpedicao)){
				bean = notafiscalprodutoService.geraNotaExpedicao(form, listaExpedicao.get(0));
				bean.setWhereInCdexpedicaoassociada(whereInexpedicao);
			}
		}
		
		boolean notaFiscalRetornoPedidoVenda = Boolean.valueOf(request.getParameter("notaFiscalRetornoPedidoVenda") != null ? request.getParameter("notaFiscalRetornoPedidoVenda") : "false");
		String whereInPedidovendaRetorno = request.getParameter("whereInPedidovendaRetorno");
		String whereInVendaRetorno = request.getParameter("whereInVendaRetorno");
		String whereInColeta = request.getParameter("whereInColeta");
		String whereInProducaoordem = request.getParameter("whereInProducaoordem");
		Boolean interromperProducao = Boolean.valueOf(request.getParameter("interromperProducao") != null ? request.getParameter("interromperProducao") : "false");
		Boolean devolucaoBool = "true".equalsIgnoreCase(request.getParameter("devolucao")) ? Boolean.TRUE : Boolean.FALSE;
		Set<Presencacompradornfe> listaPresencacompradornfe = new HashSet<Presencacompradornfe>();
		
		if((whereInColeta != null && !whereInColeta.isEmpty()) || (StringUtils.isNotBlank(whereInProducaoordem) && devolucaoBool)){
			if(notaFiscalRetornoPedidoVenda){
				bean.setWhereInPedidovendaRetorno(whereInPedidovendaRetorno);
				bean.setWhereInVendaRetorno(whereInVendaRetorno);
			}
			
			bean.setWhereInColeta(whereInColeta);
			bean.setWhereInProducaoordem(whereInProducaoordem);
			bean.setInterromperProducao(interromperProducao);
			bean.setDevolucao(devolucaoBool);
			bean.setCadastrarcobranca(false);
			bean.setCadastrartransportador(false);
			Set<String> listaOrigemOperacao = new HashSet<String>();
			
			ProducaoordemdevolucaoBean producaoordemdevolucaoBean = (ProducaoordemdevolucaoBean)request.getSession().getAttribute("ProducaoordemdevolucaoBean");
			if(producaoordemdevolucaoBean != null){
				if(bean.getIdentificadortela() == null) bean.setIdentificadortela(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
				request.getSession().removeAttribute("ProducaoordemdevolucaoBean");
				request.getSession().setAttribute("ProducaoordemdevolucaoBean" + bean.getIdentificadortela(), form.getProducaoordemdevolucaoBean());
				if(producaoordemdevolucaoBean.getExibirPopupConsumoMateriaprima() != null && producaoordemdevolucaoBean.getExibirPopupConsumoMateriaprima()){
					bean.setProducaoordemdevolucaoBean(producaoordemdevolucaoBean);
				}
				
				if(bean.getListaItens() == null){
					bean.setListaItens(new ListSet<Notafiscalprodutoitem>(Notafiscalprodutoitem.class));
				}
				
				if((producaoordemdevolucaoBean.getEmpresacoleta() != null && (producaoordemdevolucaoBean.getEmpresaproducaoordem() == null || 
								producaoordemdevolucaoBean.getEmpresacoleta().equals(producaoordemdevolucaoBean.getEmpresacoleta()))) ||
					(producaoordemdevolucaoBean.getEmpresaproducaoordem() != null && (producaoordemdevolucaoBean.getEmpresacoleta() == null || 
							producaoordemdevolucaoBean.getEmpresaproducaoordem().equals(producaoordemdevolucaoBean.getEmpresacoleta())))){
					bean.setEmpresa(producaoordemdevolucaoBean.getEmpresacoleta() != null ? producaoordemdevolucaoBean.getEmpresacoleta() : 
						producaoordemdevolucaoBean.getEmpresaproducaoordem());
				}
				if((producaoordemdevolucaoBean.getClientecoleta() != null && (producaoordemdevolucaoBean.getClienteproducaoordem() == null || 
								producaoordemdevolucaoBean.getClientecoleta().equals(producaoordemdevolucaoBean.getClientecoleta()))) ||
					(producaoordemdevolucaoBean.getClienteproducaoordem() != null && (producaoordemdevolucaoBean.getClientecoleta() == null || 
							producaoordemdevolucaoBean.getClienteproducaoordem().equals(producaoordemdevolucaoBean.getClientecoleta())))){
					bean.setCliente(producaoordemdevolucaoBean.getClientecoleta() != null ? producaoordemdevolucaoBean.getClientecoleta() : 
						producaoordemdevolucaoBean.getClienteproducaoordem());
				}
				bean.setNaturezaoperacao(producaoordemdevolucaoBean.getNaturezaoperacaosaidafiscal());
				ReportTemplateBean reportTemplateBeanInfoAdicionaisProduto = naturezaoperacaoService.getReportTemplateBeanInfoAdicionalProduto(bean.getNaturezaoperacao());
				
				for(ProducaoordemdevolucaoitemBean itemBean : producaoordemdevolucaoBean.getListaItens()){
					Notafiscalprodutoitem item;
					if(itemBean.getQuantidadedevolvida() != null && itemBean.getQuantidadedevolvida() > 0){								
						item = new Notafiscalprodutoitem();
						if(itemBean.getColetaMaterial() != null && itemBean.getColetaMaterial().getCdcoletamaterial() != null){
							item.setCdcoletamaterial(itemBean.getColetaMaterial().getCdcoletamaterial());
						}else if(itemBean.getProducaoordemmaterial() != null && itemBean.getProducaoordemmaterial().getCdproducaoordemmaterial() != null){
							item.setCdproducaoordemmaterial(itemBean.getProducaoordemmaterial().getCdproducaoordemmaterial());
						}
						item.setMotivodevolucao(itemBean.getMotivodevolucao());
						item.setListaMotivodevolucao(itemBean.getListaMotivodevolucao());
						item.setMaterial(itemBean.getMaterial());
						item.setUnidademedida(itemBean.getMaterial().getUnidademedida());
						item.setNcmcapitulo(itemBean.getMaterial().getNcmcapitulo());
						item.setNcmcompleto(itemBean.getMaterial().getNcmcompleto());
						item.setNve(itemBean.getMaterial().getNve());
						if(itemBean.getValorunitario() != null && itemBean.getValorunitario() > 0d){
							item.setValorunitario(itemBean.getValorunitario());
						} else {
							item.setValorunitario(itemBean.getMaterial().getValorcusto() != null ? itemBean.getMaterial().getValorcusto() : 0d);
						}
						item.setQtde(itemBean.getQuantidadedevolvida());
						item.setPedidovendamaterial(itemBean.getPedidovendamaterial());
						item.setValorbruto(new Money(item.getQtde() * item.getValorunitario()));
						item.setLocalarmazenagem(itemBean.getLocalarmazenagem());
						item.setInfoadicional(itemBean.getObservacao());
						item.setMaterialclasse(Materialclasse.PRODUTO);
						item.setIncluirvalorprodutos(Boolean.TRUE);
						
						Pedidovendamaterial pedidovendamaterial = itemBean.getPedidovendamaterial() != null ? itemBean.getPedidovendamaterial() : null;
						Pedidovenda pedidovenda = null;
						if(itemBean.getColetaMaterial() != null && itemBean.getColetaMaterial().getColeta() != null && itemBean.getColetaMaterial().getColeta().getPedidovenda() != null){
							pedidovenda = itemBean.getColetaMaterial().getColeta().getPedidovenda();
						}
						
						item.setInformacaoAdicionalProdutoBean(notafiscalprodutoService.criaBeanInfoProduto(itemBean.getMaterial(), itemBean.getPneu(), itemBean.getColetaMaterial(), null, null, pedidovendamaterial, pedidovenda));
						String templateInfoAdicionalItem = notafiscalprodutoService.montaInformacoesAdicionaisItemTemplate(item.getInformacaoAdicionalProdutoBean(), reportTemplateBeanInfoAdicionaisProduto);
						if(templateInfoAdicionalItem != null){
							item.setInfoadicional(templateInfoAdicionalItem);
							item.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
						}else {
							item.setInfoAdicionalItemAnterior("");
						}
						
						bean.getListaItens().add(item);
					}
					bean.setMotivodevolucaocoleta(producaoordemdevolucaoBean.getMotivodevolucao());
					
					if (itemBean.getColetaMaterial()!=null &&
							itemBean.getColetaMaterial().getColeta()!=null &&
							itemBean.getColetaMaterial().getColeta().getPedidovenda()!=null &&
							itemBean.getColetaMaterial().getColeta().getPedidovenda().getPedidovendatipo()!=null &&
							itemBean.getColetaMaterial().getColeta().getPedidovenda().getPedidovendatipo().getPresencacompradornfecoleta()!=null){
						listaOrigemOperacao.add(itemBean.getColetaMaterial().getColeta().getPedidovenda().getPedidovendatipo().getPresencacompradornfecoleta().getDescricao());
						listaPresencacompradornfe.add(itemBean.getColetaMaterial().getColeta().getPedidovenda().getPedidovendatipo().getPresencacompradornfecoleta());
					}
				}
				notafiscalprodutoService.setTotalNota(bean);
				
			} else if(whereInColeta != null && !whereInColeta.isEmpty()){
				bean  = notafiscalprodutoService.gerarNotaFiscalRetorno(request, bean, whereInColeta, whereInPedidovendaRetorno, whereInVendaRetorno, devolucaoBool, listaPresencacompradornfe, false);
			}
		}
		
		String whereInPedidovenda = request.getParameter("whereInPedidovenda");
		boolean fromPedidoColeta = Boolean.valueOf(request.getParameter("fromPedidoColeta") != null ? request.getParameter("fromPedidoColeta") : "false");
		if(whereInPedidovenda != null && !whereInPedidovenda.isEmpty()){
			bean.setProducaoautomatica("true".equals(request.getParameter("producaoautomatica")));
			bean.setWhereInPedidovenda(whereInPedidovenda);
			String tipoNotaFiscal = request.getParameter("tipoOperacaoNotaFiscal");
			if("entrada".equalsIgnoreCase(tipoNotaFiscal)){
				bean.setTipooperacaonota(Tipooperacaonota.ENTRADA);
			}else if("saida".equalsIgnoreCase(tipoNotaFiscal)){
				bean.setTipooperacaonota(Tipooperacaonota.SAIDA);
			}
			
			List<Pedidovenda> listaPv = pedidovendaService.findForGerarNotaByWhereIn(whereInPedidovenda);
			if(listaPv != null && !listaPv.isEmpty()){
				ReportTemplateBean reportTemplateBean = reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.INFO_ADICIONAIS_PRODUTO);
				if(bean.getListaItens() == null){
					bean.setListaItens(new ListSet<Notafiscalprodutoitem>(Notafiscalprodutoitem.class));
				}
				Set<Empresa> listaEmpresas = new ListSet<Empresa>(Empresa.class);
				Set<Cliente> listaClientes = new ListSet<Cliente>(Cliente.class);
				Set<Fornecedor> listaTerceiro = new ListSet<Fornecedor>(Fornecedor.class);
				ListSet<Naturezaoperacao> listaNaturezaoperacao = new ListSet<Naturezaoperacao>(Naturezaoperacao.class);
				
				for(Pedidovenda pv : listaPv){
					listaEmpresas.add(pv.getEmpresa());
					listaClientes.add(pv.getCliente());
					if(pv.getListaPedidovendamaterial() != null && !pv.getListaPedidovendamaterial().isEmpty()){
						List<Pedidovendamaterial> listaPedidovendaMaterialItensNota;
						
						if(fromPedidoColeta) {
							listaPedidovendaMaterialItensNota = new ArrayList<Pedidovendamaterial>();
							for(Pedidovendamaterial pvm : pv.getListaPedidovendamaterial()){
								boolean existeMaterialColeta = false;
								for(Pedidovendamaterial item : listaPedidovendaMaterialItensNota) {
									if(item.getMaterialcoleta().equals(pvm.getMaterialcoleta()) && item.getPneu().equals(pvm.getPneu())) {
										existeMaterialColeta = true;
										break;
									}
								}
								if(!existeMaterialColeta) {
									listaPedidovendaMaterialItensNota.add(pvm);
								}
							}
						} else {
							listaPedidovendaMaterialItensNota = pv.getListaPedidovendamaterial();
						}
						
						for(Pedidovendamaterial pvm : listaPedidovendaMaterialItensNota){
							if(fromPedidoColeta && pvm.getMaterialcoleta() == null){
								continue;
							}
							Notafiscalprodutoitem item = new Notafiscalprodutoitem();
							item.setPneu(pvm.getPneu());
							
							if(fromPedidoColeta && pvm.getMaterialcoleta() != null){
								item.setMaterial(pvm.getMaterialcoleta());
								item.setNcmcapitulo(pvm.getMaterialcoleta().getNcmcapitulo());
								item.setNcmcompleto(pvm.getMaterialcoleta().getNcmcompleto());
								item.setIncluirvalorprodutos(Boolean.TRUE);
							}else {
								item.setMaterial(pvm.getMaterial());
								item.setNcmcapitulo(pvm.getMaterial().getNcmcapitulo());
								item.setNcmcompleto(pvm.getMaterial().getNcmcompleto());
							}
							item.setUnidademedida(pvm.getMaterial().getUnidademedida());
							item.setValorunitario(pvm.getMaterial().getValorcusto());
							item.setQtde(pvm.getQuantidade());
							if(item.getQtde() != null && item.getValorunitario() != null){
								item.setValorbruto(new Money(item.getQtde() * item.getValorunitario()));
							}
							if(Util.objects.isPersistent(pv.getLocalarmazenagem())){
								Localarmazenagem local = localarmazenagemService.load(pv.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome");
								item.setLocalarmazenagem(local);
							}
							item.setInfoadicional(pvm.getObservacao());
							item.setMaterialclasse(Materialclasse.PRODUTO);
							
							item.setInformacaoAdicionalProdutoBean(notafiscalprodutoService.criaBeanInfoProduto(pv, pvm));
							String templateInfoAdicionalItem = notafiscalprodutoService.montaInformacoesAdicionaisItemTemplate(item.getInformacaoAdicionalProdutoBean(), reportTemplateBean);
							if(templateInfoAdicionalItem != null){
								item.setInfoadicional(templateInfoAdicionalItem);
								item.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
							}else {
								item.setInfoAdicionalItemAnterior("");
							}
							
							bean.getListaItens().add(item);
						}						
					}
					if(pv.getTerceiro() != null && 
							!listaTerceiro.contains(pv.getTerceiro())){
						listaTerceiro.add(pv.getTerceiro());
					}
					
					if (pv.getPedidovendatipo()!=null && pv.getPedidovendatipo().getPresencacompradornfecoleta()!=null){
						listaPresencacompradornfe.add(pv.getPedidovendatipo().getPresencacompradornfecoleta());
					}
					if (pv.getPedidovendatipo()!=null){
						if("entrada".equalsIgnoreCase(tipoNotaFiscal)){
							if(pv.getPedidovendatipo().getNaturezaoperacaonotafiscalentrada()!=null && 
									!listaNaturezaoperacao.contains(pv.getPedidovendatipo().getNaturezaoperacaonotafiscalentrada())){
								listaNaturezaoperacao.add(pv.getPedidovendatipo().getNaturezaoperacaonotafiscalentrada());
							}
						}else {
							if(pv.getPedidovendatipo().getNaturezaoperacaosaidafiscal()!=null && 
									!listaNaturezaoperacao.contains(pv.getPedidovendatipo().getNaturezaoperacaosaidafiscal())){
								listaNaturezaoperacao.add(pv.getPedidovendatipo().getNaturezaoperacaosaidafiscal());
							}
						}
					}
				}
				if(listaEmpresas.size() == 1){
					bean.setEmpresa(listaEmpresas.iterator().next());
				}
				if(listaClientes.size() == 1){
					bean.setCliente(listaClientes.iterator().next());
					Cliente cliente = clienteService.carregarDadosCliente(bean.getCliente());
					if(cliente != null && SinedUtil.isListNotEmpty(cliente.getListaEndereco())){
						Endereco enderecoCliente = cliente.getEndereco();
						if(enderecoCliente != null){
							bean.setEnderecoCliente(enderecoCliente);
						}
					}	
				}
				
				String whereInColetaPedido = null;
				if(fromPedidoColeta){
					List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPedidovenda);
					if(listaColeta != null && !listaColeta.isEmpty()){				
						whereInColetaPedido = CollectionsUtil.listAndConcatenate(listaColeta, "cdcoleta", ",");
					}
				}
				if(bean.getCadastrartransportador() != null && bean.getCadastrartransportador() && listaTerceiro.size() == 1){
					bean.setTransportador(listaTerceiro.iterator().next());
				}
				notafiscalprodutoService.loadInfoForTemplateInfoContribuinte(bean);
				String templateInfoContribuinte = notafiscalprodutoService.montaInformacoesContribuinteTemplate(null, whereInColetaPedido, bean);
				if(templateInfoContribuinte != null){
					bean.setInfoadicionalcontrib(templateInfoContribuinte);
				}
				
				if (listaNaturezaoperacao.size()==1){
					bean.setNaturezaoperacao(listaNaturezaoperacao.get(0));
				}
			}
		}
		
		if (listaPresencacompradornfe.size()==1){
			bean.setPresencacompradornfe(new ListSet<Presencacompradornfe>(Presencacompradornfe.class, listaPresencacompradornfe).get(0));
		}
		
		String romaneios = request.getParameter("romaneios");
		if(romaneios != null && !romaneios.equals("")){
			bean = notafiscalprodutoService.geraNotaRomaneios(bean, romaneios);
			request.setAttribute("fromRomaneio", Boolean.TRUE);
		}
		
		if("true".equalsIgnoreCase(request.getParameter("importacaoXmlDi"))){
			Notafiscalproduto notafiscalprodutoDI = (Notafiscalproduto) request.getSession().getAttribute(ImportarXmlDeclaracaoImportacaoProcess.ATTR_NOTAFISCALPRODUTO_DI_SESSAO);
			if(notafiscalprodutoDI != null){
				bean = notafiscalprodutoDI;
				request.getSession().removeAttribute(ImportarXmlDeclaracaoImportacaoProcess.ATTR_NOTAFISCALPRODUTO_DI_SESSAO);
			}
		}

		if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
			Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.naopreencherdtsaida, empresa.marcanf, empresa.especienf, empresa.serienfe");
			
			if(empresa.getNaopreencherdtsaida() == null || !empresa.getNaopreencherdtsaida()){
				bean.setDatahorasaidaentrada(SinedDateUtils.currentTimestamp());
			}
			
			bean.setMarcavolume(empresa.getMarcanf());
			bean.setEspvolume(empresa.getEspecienf());
			
			Endereco endereco = enderecoService.carregaEnderecoAtivoEmpresa(bean.getEmpresa());
			if(endereco != null && bean.getMunicipiogerador() == null){
				bean.setMunicipiogerador(endereco.getMunicipio());
			}
			if(bean.getCdNota() == null){
				bean.setSerienfe(empresa.getSerienfe());
			}
		}
		String faturarRequisicao = request.getParameter("faturarRequisicao");
		String whereInRequisicao = request.getParameter("whereInRequisicao");
		if(whereInRequisicao != null && faturarRequisicao != null && "TRUE".equalsIgnoreCase(faturarRequisicao)){
			bean = notafiscalprodutoService.preencheNotaForFaturarProdutosRequisiscao(bean, whereInRequisicao);
			request.setAttribute("whereInRequisicao", whereInRequisicao);
		}
		List<Coleta> listaColeta = StringUtils.isNotBlank(whereInColeta) ? coletaService.loadForGerarNotafiscalproduto(whereInColeta) : new ArrayList<Coleta>();
		String informacoesContribuinte = "";

		if((!listaVenda.isEmpty()) || !listaColeta.isEmpty()){
			notafiscalprodutoService.loadInfoForTemplateInfoContribuinte(bean);
			informacoesContribuinte = notafiscalprodutoService.montaInformacoesContribuinteTemplate(listaVenda, listaColeta, bean);
		}
		
		if(StringUtils.isNotBlank(informacoesContribuinte)){
			bean.setInfoadicionalcontrib(informacoesContribuinte);
		}
		if(bean.getCdNota() == null && bean.getNaturezaoperacao() != null){
			Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(bean.getNaturezaoperacao(), "naturezaoperacao.razaosocial");
			if(naturezaoperacao != null && Util.strings.isNotEmpty(naturezaoperacao.getRazaosocial())){
				bean.setRazaosocialalternativa(naturezaoperacao.getRazaosocial());
			}
		}
		
		notafiscalprodutoautorizacaoService.preencheAbaAutorizacaoByParametro(bean);
		
		return bean;
	}	
	
	protected void telefonePersistenteParaTransiente(Notafiscalproduto form) {
		List<Telefone> listaTelefoneCliente = telefoneService.carregarListaTelefone(form.getCliente());
		
		Telefone telefoneCiente = new Telefone();
		telefoneCiente.setCdtelefone(-1);
		telefoneCiente.setTelefone(form.getTelefoneCliente());
		
		if (listaTelefoneCliente.contains(telefoneCiente)) {
			int index = listaTelefoneCliente.indexOf(telefoneCiente);
			form.setTelefoneClienteTransiente(listaTelefoneCliente.get(index));
		} else {
			listaTelefoneCliente.add(telefoneCiente);
			form.setTelefoneClienteTransiente(telefoneCiente);
		}
		
		if(form.getTransportador() != null){
			List<Telefone> listaTelefoneTransportador = telefoneService.carregarListaTelefone(form.getTransportador());
			
			Telefone telefoneTransportador = new Telefone();
			telefoneTransportador.setCdtelefone(-1);
			telefoneTransportador.setTelefone(form.getTelefonetransportador());
			
			if (listaTelefoneTransportador.contains(telefoneTransportador)) {
				int index = listaTelefoneTransportador.indexOf(telefoneTransportador);
				form.setTelefoneTransportadorTransient(listaTelefoneTransportador.get(index));
			} else {
				listaTelefoneTransportador.add(telefoneTransportador);
				form.setTelefoneTransportadorTransient(telefoneTransportador);
			}
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Notafiscalproduto form)throws Exception {
		if(form.getModeloDocumentoFiscalEnum() == null) form.setModeloDocumentoFiscalEnum(getModelo());
		boolean isNfe = isNfe();
		request.setAttribute("isNfe", isNfe);
		
		List<Presencacompradornfe> listPresencacompradorNfe = new ArrayList<Presencacompradornfe>();
		if(isNfe){
			listPresencacompradorNfe.add(Presencacompradornfe.NAO_SE_APLICA);
			listPresencacompradorNfe.add(Presencacompradornfe.PRESENCIAL);
			listPresencacompradorNfe.add(Presencacompradornfe.INTERNET);
			listPresencacompradorNfe.add(Presencacompradornfe.TELEATENDIMENTO);
			listPresencacompradorNfe.add(Presencacompradornfe.OUTROS);
			listPresencacompradorNfe.add(Presencacompradornfe.PRESENCIAL_FORA);
		} else {
			listPresencacompradorNfe.add(Presencacompradornfe.PRESENCIAL);
			listPresencacompradorNfe.add(Presencacompradornfe.NFCE_ENTREGA);
		}
		request.setAttribute("listPresencacompradorNfe", listPresencacompradorNfe);
		
		if(!isNfe){
			form.setLocaldestinonfe(Localdestinonfe.INTERNA);
			form.setFinalidadenfe(Finalidadenfe.NORMAL);
			form.setTipooperacaonota(Tipooperacaonota.SAIDA);
		}
		
		int tabelaIBPTAlertaDias = tabelaimpostoService.diasAlertaTabela();
		if(tabelaIBPTAlertaDias < 10){
			request.setAttribute("tabelaIBPTAlerta", Boolean.TRUE);
			if(tabelaIBPTAlertaDias > 0){
				request.setAttribute("tabelaIBPTAlertaDias", "Faltam " + tabelaIBPTAlertaDias + " dias.");
			} else {
				request.setAttribute("tabelaIBPTAlertaDias", "Tabela j� est� vencida.");
			}
		}
		
		if(form.getIdentificadortela() == null) form.setIdentificadortela(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		Boolean permissaoProjeto = false;
		if(!usuario.getTodosprojetos()){				
			permissaoProjeto = StringUtils.isNotBlank(SinedUtil.getListaProjeto());
		}
		request.setAttribute("permissaoProjeto", Boolean.FALSE);
		request.setAttribute("listaProjeto", projetoService.findProjetosAtivos(form.getProjeto(), permissaoProjeto));
		
		boolean podeEditar = notaService.podeEditar(form.getNotaStatus(), true, false, false);
		request.setAttribute("podeEditar", podeEditar);
		
		//Action Editar
		if(AbstractCrudController.EDITAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))){
			if(!podeEditar){
				throw new SinedException("Uma nota liquidada ou no processo de nota fiscal eletr�nica n�o deve ser editada.");
			}
		}
		if (form.getCdNota() != null) {
			if(emporiumvendaService.fromVendaemporium(form)){
				List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(form);
				
				request.setAttribute("fromVendaEmporium", Boolean.TRUE);
				request.setAttribute("whereInVenda", CollectionsUtil.listAndConcatenate(listaNotaVenda, "venda.cdvenda", ","));
			}
			
			form.setListaDuplicata(notafiscalprodutoduplicataService.findByNotafiscalproduto(form));
			form.setListaReferenciada(notafiscalprodutoreferenciadaService.findByNotafiscalproduto(form));
			form.setListaNotaHistorico(notaHistoricoService.carregarListaHistorico(form));
			form.setListaAutorizacao(notafiscalprodutoautorizacaoService.findByNotafiscalproduto(form));
			
			this.telefonePersistenteParaTransiente(form);
			
			if(form.getNotaStatus().equals(NotaStatus.NFE_EMITIDA) || form.getNotaStatus().equals(NotaStatus.NFE_LIQUIDADA)){
				Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProduto(form);
				if(arquivonfnota != null){
					form.setChaveAcesso(arquivonfnota.getChaveacesso());
				}
			}
			notafiscalprodutoService.setNomnfComprimentoAlturalargura(form);
			request.setAttribute("vendaIndustrializacaoRetorno", notaVendaService.isNotaIndustrializacaoRetorno(form));
			
			if(NotaStatus.NFE_EMITIDA.equals(form.getNotaStatus()) || NotaStatus.NFE_LIQUIDADA.equals(form.getNotaStatus())){
				if(notafiscalprodutocorrecaoService.haveCartaCorrecao(form.getCdNota())){
					request.getSession().setAttribute("atualizarCfop", "true");
				}else{
					request.getSession().setAttribute("atualizarCfop", "false");
				}
			}else{
				request.getSession().setAttribute("atualizarCfop", "false");
			}
		}
		if("true".equals(request.getParameter("isCriar"))){
			form.setIsCriar(true);
		}
		
		try{
			if(form.getListaItens() != null && form.getListaItens().size() > 0){
				List<String> listaLegenda = new ArrayList<String>();
				for (Notafiscalprodutoitem item : form.getListaItens()) {
					if (form.getEmpresa() != null) {
						item.setConsiderarIdPneu(form.getEmpresa().getConsiderarIdPneu());
					}
					
					List<Materiallegenda> listaMateriallegendaUnico = materiallegendaService.findByMaterial(item.getMaterial().getCdmaterial() + "");
					if(listaMateriallegendaUnico != null && listaMateriallegendaUnico.size() > 0){
						String itLegenda = item.getMaterial().getIdentificacaoOuCdmaterial() + " - ";
						listaLegenda.add(itLegenda + CollectionsUtil.listAndConcatenate(listaMateriallegendaUnico, "legenda", " | "));
					}
					
					if(materialnumeroserieService.findListaMaterialnumeroserieByMaterial(item.getMaterial()).size() > 0){
						item.setHaveNumeroSerie(Boolean.TRUE);					
					}else{
						item.setHaveNumeroSerie(Boolean.FALSE);
					}
					item.setNaocalcularipi(null);
				}
				
				notafiscalprodutoitemService.ordenaListaItens(form);
				
				if(listaLegenda != null && listaLegenda.size() > 0){
					form.setLegenda(CollectionsUtil.concatenate(listaLegenda, "\n"));
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		List<Endereco> listaEnderecoCliente = null;
		List<Telefone> listaTelefoneCliente = null;
		
		if (form.getCliente() != null && form.getCliente().getCdpessoa() != null) {
//			listaEnderecoCliente = enderecoService.carregarListaEndereco(form.getCliente());
			listaEnderecoCliente = enderecoService.findByPessoaWithoutInativo(form.getCliente(), form.getEnderecoCliente());
			listaTelefoneCliente = telefoneService.carregarListaTelefone(form.getCliente());
		} else {
			listaEnderecoCliente = new ArrayList<Endereco>();
			listaTelefoneCliente = new ArrayList<Telefone>();
		}		
		
		List<Endereco> listaEnderecoTransportador = null;
		List<Telefone> listaTelefoneTransportador = null;
		
		if (form.getTransportador() != null && form.getTransportador().getCdpessoa() != null) {
//			listaEnderecoTransportador = enderecoService.carregarListaEndereco(form.getTransportador());
			listaEnderecoTransportador = enderecoService.findByPessoaWithoutInativo(form.getTransportador(), form.getEnderecotransportador());
			listaTelefoneTransportador = telefoneService.carregarListaTelefone(form.getTransportador());
		} else {
			listaEnderecoTransportador = new ArrayList<Endereco>();
			listaTelefoneTransportador = new ArrayList<Telefone>();
		}			
		
		if(form.getListaItens() == null) form.setListaItens(new ArrayList<Notafiscalprodutoitem>());
		request.getSession().setAttribute("listaItensNotaFiscalProduto" + form.getIdentificadortela(), form.getListaItens());
		request.setAttribute("listaEnderecoCliente", listaEnderecoCliente);
		request.setAttribute("listaTelefoneCliente", listaTelefoneCliente);
		request.setAttribute("listaEnderecoTransportador", listaEnderecoTransportador);
		request.setAttribute("listaTelefoneTransportador", listaTelefoneTransportador);	
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("listaNaturezaoperacao", naturezaoperacaoService.find(form.getNaturezaoperacao(), NotaTipo.NOTA_FISCAL_PRODUTO, Boolean.TRUE));
		request.setAttribute("danfeConfiguravel", reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.DANFE)!=null);
		
		boolean haveServico = false;
		if(form.getCdNota() != null && form.getListaItens() != null && form.getListaItens().size() > 0){
			List<Notafiscalprodutoitem> listaItens = form.getListaItens();
			for (Notafiscalprodutoitem notafiscalprodutoitem : listaItens) {
				if(notafiscalprodutoitem.getTributadoicms() != null && !notafiscalprodutoitem.getTributadoicms()){
					haveServico = true;
					break;
				}
			}
		}
		request.setAttribute("haveServico", haveServico);
		
		if(form.getCdNota() != null){
			form.setListaCorrecao(notafiscalprodutocorrecaoService.findByNotafiscalproduto(form));
		}
		
		boolean exibirLocal = this.exibirLocal(form);
		request.setAttribute("exibirLocal", exibirLocal);
		request.setAttribute("LOCAL_VENDA_OBRIGATORIO", parametrogeralService.getBoolean(Parametrogeral.LOCAL_VENDA_OBRIGATORIO));
		
        List<Configuracaonfe> listaConfiguracaonfe = configuracaonfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, form.getEmpresa());
        boolean atualizarDataEntradaSaida = false;
        for (Configuracaonfe configuracaonfe : listaConfiguracaonfe) {
			if(configuracaonfe.getAtualizacaodataentradasaida() != null && configuracaonfe.getAtualizacaodataentradasaida()){
				atualizarDataEntradaSaida = true;
			}
		}
        String templateInfoContribuinte = "";
        String cdvendaParam = request.getParameter("cdvenda");
        String whereInColetaAux = request.getParameter("whereInColeta") != null && !"<null>".equals(request.getParameter("whereInColeta")) ? request.getParameter("whereInColeta") : form.getWhereInColeta();
        if(cdvendaParam != null){
        	Venda venda = new Venda(Integer.parseInt(cdvendaParam));
        	notafiscalprodutoService.loadInfoForTemplateInfoContribuinte(form);
			templateInfoContribuinte = notafiscalprodutoService.montaInformacoesContribuinteTemplate(venda, null, form);
			if(atualizarDataEntradaSaida){
	        	Vendamaterial vendamaterial = vendamaterialService.findMaiorDataEntrega(venda);
	        	if(vendamaterial != null && vendamaterial.getPrazoentrega()!= null){
	        		Date dataEntrega = new Date(vendamaterial.getPrazoentrega().getTime());
	        		form.setDtEmissao(dataEntrega);
	        	}				
			}
        }else if(StringUtils.isNotEmpty(whereInColetaAux)){
        	notafiscalprodutoService.loadInfoForTemplateInfoContribuinte(form);
			templateInfoContribuinte = notafiscalprodutoService.montaInformacoesContribuinteTemplate(null, whereInColetaAux, form);
        }
		
        form.setPossuiTemplateInfoContribuinte(Boolean.FALSE);		
		if(form.getNaturezaoperacao() != null){
			Naturezaoperacao natureza = naturezaoperacaoService.load(form.getNaturezaoperacao());
			form.setPossuiTemplateInfoContribuinte(natureza.getTemplateinfcontribuinte() != null);
		}
        
		if(form.getCdNota() == null && form.getEmpresa() != null && form.getEmpresa().getCdpessoa() != null &&
			!Boolean.TRUE.equals(form.getPossuiTemplateInfoContribuinte())){
			Empresa empresa = empresaService.buscarInfContribuinte(form.getEmpresa());
			if(empresa != null && empresa.getTextoinfcontribuinte() != null){
				if(form.getInfoadicionalcontrib() == null)
					form.setInfoadicionalcontrib(empresa.getTextoinfcontribuinte());
				else {
					if(!form.getInfoadicionalcontrib().contains(empresa.getTextoinfcontribuinte()))
						form.setInfoadicionalcontrib(empresa.getTextoinfcontribuinte() + ". " + form.getInfoadicionalcontrib());
				}
			}
		}
		
		
		if(AbstractCrudController.EDITAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER)) &&
			!Boolean.TRUE.equals(form.getPossuiTemplateInfoContribuinte())){
			String infadicionais = notafiscalprodutoService.getMsgitemAproveitamentocredito(form.getListaItens());
			if(infadicionais != null && !"".equals(infadicionais)){
				if(form.getInfoadicionalcontrib() == null)
					form.setInfoadicionalcontrib(infadicionais);
				else {
					if(!form.getInfoadicionalcontrib().contains(infadicionais))
						form.setInfoadicionalcontrib(form.getInfoadicionalcontrib() + " " + infadicionais);
				}
			}
		}
		if(!AbstractCrudController.CONSULTAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))){
			notafiscalprodutoService.addListaMateralInfoadicional(form);
		}
        
		if(AbstractCrudController.CONSULTAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))){
			List<Notafiscalproduto> listaNotaProduto = notafiscalprodutoService.findForMemorandoExportacao(form.getCdNota().toString());
			form.setIsExportacao(notafiscalprodutoService.isNotaExportacao(listaNotaProduto));
			form.setIsStatusValido(notafiscalprodutoService.isStatusValido(form.getCdNota().toString()));	
			form.setIsExisteMemorandoexportacao(memorandoexportacaoService.isExisteMemorandoexportacao(form));
			if(form.getIsExportacao()){
				for(Notafiscalprodutoitem notaitem : form.getListaItens()){
					notaitem.setListaNotaprodutoitementregamaterial(notafiscalprodutoitementregamaterialService.loadListaByNotafiscalprodutoitem(notaitem));
//					for(Notafiscalprodutoitementregamaterial notaitementrega : notaitem.getListaNotaprodutoitementregamaterial()){
//						notaitem.setNumeroEntregadocumento(notaitementrega.getEntregamaterial().getEntregadocumento().getNumero());
//					}
					Set<String> listaNumero = new HashSet<String>();
					for (Notafiscalprodutoitementregamaterial notafiscalprodutoitementregamaterial: notaitem.getListaNotaprodutoitementregamaterial()){
						listaNumero.add(notafiscalprodutoitementregamaterial.getEntregamaterial().getEntregadocumento().getNumero());
					}
					if (!listaNumero.isEmpty()){
						notaitem.setNumeroEntregadocumento(CollectionsUtil.concatenate(listaNumero, ", "));
					}
				}
			}
		}
        
        form.setEmpresaAntiga(form.getEmpresa());
        form.setNumeroAntigo(form.getNumero());
        notafiscalprodutoService.setNomeAlternativo(form);
        
        if(form.getProducaoordemdevolucaoBean() != null){
        	request.getSession().setAttribute("ProducaoordemdevolucaoBean" + form.getIdentificadortela(), form.getProducaoordemdevolucaoBean());
        }
        
        if (form.getListaItens()!=null){
        	String patrimonioStr = "";
        	
        	for (Notafiscalprodutoitem nfpi: form.getListaItens()){
        		if (nfpi.getPatrimonioitem()!=null){
        			patrimonioStr += "Patrim�nio: ";
        			if (nfpi.getPatrimonioitem().getPlaquetaSerie()!=null){
        				patrimonioStr += nfpi.getPatrimonioitem().getPlaquetaSerie();
        			}else {
        				patrimonioStr += "-";
        				
        			}
//        			if (nfpi.getMaterial()!=null){
//        				String serie = nfpi.getMaterial().getSerie();
//        				if (serie!=null){
//        					patrimonioStr += " e S�rie " + serie;
//        				}
//        			}
        			patrimonioStr += " / ";
        		}
        	}
        	
        	if (patrimonioStr.endsWith(" / ")){
        		patrimonioStr = patrimonioStr.substring(0, patrimonioStr.length()-3);
        		if (form.getInfoadicionalcontrib()==null){
        			form.setInfoadicionalcontrib(patrimonioStr);
        		}else {
        			if (form.getInfoadicionalcontrib().trim().endsWith(".")){
        				form.setInfoadicionalcontrib(form.getInfoadicionalcontrib() + " " + patrimonioStr);
        			}else {
        				form.setInfoadicionalcontrib(form.getInfoadicionalcontrib() + ". " + patrimonioStr);        				
        			}
        		}
        	}        
        }
        if(form.getNaturezaoperacao() != null && form.getNaturezaoperacao().getSimplesremessa() == null){
        	Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(form.getNaturezaoperacao(), "naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.simplesremessa");
        	form.getNaturezaoperacao().setSimplesremessa(naturezaoperacao != null && naturezaoperacao.getSimplesremessa() != null && naturezaoperacao.getSimplesremessa());
        }
        
        if(form.getCdNota()==null){
        	if(!isNfe){
    			form.setOperacaonfe(Operacaonfe.CONSUMIDOR_FINAL);
    		} else {
    			form.setOperacaonfe(ImpostoUtil.getOperacaonfe(form.getCliente(), form.getNaturezaoperacao()));
    		}
        }
        
        if(!AbstractCrudController.CONSULTAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))){
	        String whereInColeta = notafiscalprodutoColetaService.getWhereInColeta(form);
	        String whereInVenda = notaVendaService.getWhereInVenda(form);
	        request.setAttribute("whereInColeta", whereInColeta);
	        request.setAttribute("cdVenda", whereInVenda);
	        request.setAttribute("infContribuinteTemplateNatureza", SinedUtil.escapeJavascript(StringUtils.isNotBlank(templateInfoContribuinte) ? templateInfoContribuinte :  notafiscalprodutoService.montaInformacoesContribuinteTemplate(whereInVenda, whereInColeta, form)));
        }
        String whereInCddocumentotipo = null;
        if(form.getCdNota() != null){
	        List<String> listaWhereInCddocumentotipo = new ArrayList<String>();
	        if(SinedUtil.isListNotEmpty(form.getListaDuplicata())){
	        	for(Notafiscalprodutoduplicata parcela: form.getListaDuplicata()){
	        		if(parcela.getDocumentotipo() != null && parcela.getDocumentotipo().getCddocumentotipo() != null){
	        			listaWhereInCddocumentotipo.add(parcela.getDocumentotipo().getCddocumentotipo().toString());
	        		}
	        	}
	        }
	
	        if(form.getDocumentotipo() != null && form.getDocumentotipo().getCddocumentotipo() != null){
	        	listaWhereInCddocumentotipo.add(form.getDocumentotipo().getCddocumentotipo().toString());
	        }
	        whereInCddocumentotipo = listaWhereInCddocumentotipo.isEmpty()? null: CollectionsUtil.concatenate(listaWhereInCddocumentotipo, ",");
        }
        List<Documentotipo> listaDocumentotipo = documentotipoService.getListaDocumentoTipoUsuarioForVenda(whereInCddocumentotipo);
        request.setAttribute("listaDocumentotipo", listaDocumentotipo);
        request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
        
        List<Prazopagamento> listaPrazoPagamento;
        if (form.getAgruparContas() != null && form.getAgruparContas()) {
        	listaPrazoPagamento = prazopagamentoService.findByPrazomedio(Boolean.TRUE);
        	
        	if (form.getQtdParcelas() != null) {
        		form.setCadastrarcobranca(form.getQtdParcelas() > 0);
        	}
        } else {
        	listaPrazoPagamento = prazopagamentoService.findAll();
        }
        request.setAttribute("isRepagem", SinedUtil.isRecapagem());
		boolean isExpedicaoNota = false;
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("NOTA")){
			isExpedicaoNota = true;
		}
		request.setAttribute("isExpedicaoNota", isExpedicaoNota);
        request.setAttribute("listaPrazoPagamento", listaPrazoPagamento);
        request.setAttribute("QTDE_VOLUME_NFP", parametrogeralService.getBoolean(Parametrogeral.QTDE_VOLUME_NFP));
	}
	
	private boolean exibirLocal(Notafiscalproduto form) {
		boolean exibirLocal = false;
		if(form.getCdNota() != null){
			if(!notaService.existOrigemForBaixaNota(form.getCdNota() + "")){
				exibirLocal = true;
			}
		} else {
			if(form.getRomaneios() != null && !form.getRomaneios().trim().equals("")){
				exibirLocal = false;
			} else if(form.getCdvendaassociada() == null){
				exibirLocal = true;
			} else {
				Venda venda = vendaService.loadWithPedidovenda(new Venda(form.getCdvendaassociada()));
				if(venda != null && 
						venda.getPedidovendatipo() != null &&
						venda.getPedidovendatipo().getBaixaestoqueEnum() != null &&
						venda.getPedidovendatipo().getBaixaestoqueEnum().equals(BaixaestoqueEnum.APOS_EMISSAONOTA)){
					exibirLocal = true;
				}
			}
		}
		return exibirLocal;
	}
	
	protected ListagemResult<Notafiscalproduto> getLista(WebRequestContext request, NotafiscalprodutoFiltro filtro) {
		filtro.setModeloDocumentoFiscalEnum(getModelo());
		ListagemResult<Notafiscalproduto> lista = super.getLista(request, filtro);
		List<Notafiscalproduto> list = lista.list();
		
		if(list != null && list.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(list, "cdNota", ",");
			
			List<Gnre> listaGnre = gnreService.findByNota(whereIn);
			List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(whereIn);
			List<Arquivotef> listaArquivotef = arquivotefService.findByNota(whereIn);
			
			for (Notafiscalproduto nf : list) {
				for (Gnre gnre : listaGnre) {
					if(nf.getCdNota().equals(gnre.getNota().getCdNota())){
						nf.setHaveGnre(Boolean.TRUE);
					}
				}
				
				for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
					if(nf.getCdNota().equals(arquivonfnota.getNota().getCdNota())){
						nf.setHaveArquivonfnota(Boolean.TRUE);
						if(NotaStatus.EMITIDA.equals(nf.getNotaStatus())){
							if(arquivonfnota != null && arquivonfnota.getArquivonf()!= null) {
								if(arquivonfnota.getArquivonf().getArquivonfsituacao() != null && arquivonfnota.getArquivonf().getArquivonfsituacao().getNome() != null){
									if(arquivonfnota.getArquivonf().getArquivonfsituacao().getNome().equals("Processado com erro")){
										nf.setExisteErroRejeicao(true);										
									}
								}
							}
						}
					}
					if(Util.objects.isPersistent(arquivonfnota.getArquivoxml())){
						DownloadFileServlet.addCdfile(request.getSession(), arquivonfnota.getArquivoxml().getCdarquivo());
					}
				}
				if(listaArquivotef != null && !listaArquivotef.isEmpty()){
					PagamentoTEFSituacao pagamentoTEFSituacao = null;					
					for (Arquivotef arquivotef : listaArquivotef) {
						List<Arquivotefitem> listaArquivotefitem = arquivotef.getListaArquivotefitem(); 
						if(listaArquivotefitem != null && !listaArquivotefitem.isEmpty()){
							for (Arquivotefitem arquivotefitem : listaArquivotefitem) {
								if(arquivotefitem.getVendapagamento() != null && 
										arquivotefitem.getNotafiscalprodutoduplicata().getNotafiscalproduto() != null && 
										arquivotefitem.getNotafiscalprodutoduplicata().getNotafiscalproduto().equals(nf)){
									Arquivotefsituacao situacao = arquivotef.getSituacao();
									if(situacao != null){
										if(situacao.equals(Arquivotefsituacao.GERADO) || situacao.equals(Arquivotefsituacao.ENVIADO)){
											if(pagamentoTEFSituacao == null || 
													pagamentoTEFSituacao.equals(PagamentoTEFSituacao.RECUSADO)) 
												pagamentoTEFSituacao = PagamentoTEFSituacao.EM_ANDAMENTO;
										}
										if(situacao.equals(Arquivotefsituacao.NAO_ENVIADO) || situacao.equals(Arquivotefsituacao.PROCESSADO_COM_ERRO)){
											if(pagamentoTEFSituacao == null) 
												pagamentoTEFSituacao = PagamentoTEFSituacao.RECUSADO;
										}
										if(situacao.equals(Arquivotefsituacao.PROCESSADO_COM_SUCESSO)){
											if(pagamentoTEFSituacao == null || 
													pagamentoTEFSituacao.equals(PagamentoTEFSituacao.RECUSADO) || 
													pagamentoTEFSituacao.equals(PagamentoTEFSituacao.EM_ANDAMENTO)) 
												pagamentoTEFSituacao = PagamentoTEFSituacao.REALIZADO;
										}
									}
								}
							}
						}
					}
					nf.setPagamentoTEFSituacao(pagamentoTEFSituacao);
				}
			}
			
			if(filtro.getExibirTotais() != null && filtro.getExibirTotais()){
				filtro.setTotalnotas(notafiscalprodutoService.findForCalculoTotalGeral(filtro));
			}
		}
		
		return lista;
	}
	
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request,	Notafiscalproduto bean) {
		String identificadortela = bean.getIdentificadortela(); 
		bean.setIdentificadortela(null);
		if(bean.getWhereInEntradaFiscal() != null || bean.getWhereInExpedicaoproducao() != null){
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>parent.location = parent.location;parent.$.akModalRemove(true);</script>");
			return null;
		} else if(bean.getProducaoautomatica() != null && 
				bean.getProducaoautomatica() &&
				bean.getWhereInPedidovenda() != null &&
				!"".equals(bean.getWhereInPedidovenda()) &&
				bean.getWhereInPedidovenda().split(",").length == 1){
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=abrirGerarProducaoConferencia&selectedItens=" + bean.getWhereInPedidovenda());
		} else {
			if(bean.getCdvendaassociada() != null){
				Venda venda = vendaService.loadWithPedidovenda(new Venda(bean.getCdvendaassociada()));
				if(venda != null && venda.getPedidovenda() != null && venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getGerarnotaretornovenda())){
					List<Pedidovenda> listaPedidovenda = pedidovendaService.findForNotaRetorno(venda.getPedidovenda().getCdpedidovenda().toString());
					if(SinedUtil.isListNotEmpty(listaPedidovenda)){
						StringBuilder whereInColetaRetornoVenda = new StringBuilder();
						for(Pedidovenda pedidovenda : listaPedidovenda){
							if(SinedUtil.isListNotEmpty(pedidovenda.getListaColeta())){
								whereInColetaRetornoVenda.append(CollectionsUtil.listAndConcatenate(pedidovenda.getListaColeta(), "cdcoleta", ",")).append(",");
							}
						}
						if(whereInColetaRetornoVenda.length() > 0 && !coletaService.isColetaPossuiNotaTodosItensSemSaldo(whereInColetaRetornoVenda.substring(0, whereInColetaRetornoVenda.length()-1), Tipooperacaonota.SAIDA)){
							SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto?ACAO=criar&notaFiscalRetornoPedido=true&tipooperacaoNota=SAIDA&registrarNF=true&whereInVendaRetorno=" + venda.getCdvenda() + "&whereInColeta=" + whereInColetaRetornoVenda.substring(0, whereInColetaRetornoVenda.length()-1));
							return null; 
						}
					}
				}
			}
					
			String whereInColeta = bean.getWhereInColeta();
			String whereInProducaoordem = bean.getWhereInProducaoordem();
			Boolean devolucao = bean.getDevolucao();
			if((StringUtils.isNotBlank(whereInColeta) || (StringUtils.isNotBlank(whereInProducaoordem) && devolucao != null && devolucao)) &&
					request.getSession().getAttribute("ProducaoordemdevolucaoBean" + identificadortela) != null){
				ProducaoordemdevolucaoBean producaoordemdevolucaoBean = (ProducaoordemdevolucaoBean) request.getSession().getAttribute("ProducaoordemdevolucaoBean" + identificadortela);
				String urlretorno = producaoordemdevolucaoBean.getUrlretorno().indexOf("?") != -1 ? producaoordemdevolucaoBean.getUrlretorno().substring(0, producaoordemdevolucaoBean.getUrlretorno().indexOf("?")) : producaoordemdevolucaoBean.getUrlretorno();
				return new ModelAndView("redirect:" + urlretorno +"?ACAO=abrirPopUpConsumoMateriaprimaAposCriarNotaDevolucao&identificadortelanota=" + identificadortela);
			}
			
			return super.getSalvarModelAndView(request, bean);
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request,	Notafiscalproduto form) throws CrudException {
		if(form.getCdNota() != null && form.getNotaStatus() != null){
			Notafiscalproduto notafiscalproduto = notafiscalprodutoService.load(form, "notafiscalproduto.cdNota, notafiscalproduto.notaStatus");
			if(notafiscalproduto != null && notafiscalproduto.getNotaStatus() != null){
				boolean podeEditar = notaService.podeEditar(notafiscalproduto.getNotaStatus(), true, false, false);
				if(!podeEditar){
					request.addError("Uma nota liquidada ou no processo de nota fiscal eletr�nica n�o deve ser editada.");
					return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota=" + form.getCdNota());
				}
			}
		}
		
		if (form.getCdNota() == null && form.getCdvendaassociada() != null &&
				StringUtils.isBlank(form.getWhereInCdexpedicaoassociada()) &&
				notaVendaService.existeNotaEmitida(new Venda(form.getCdvendaassociada()))){
			request.addError("J� existe uma nota vinculada a esta venda.");
			String complementoOtr = SinedUtil.complementoOtr();
			return new ModelAndView("redirect:/faturamento/crud/Venda"+complementoOtr+"?ACAO=consultar&cdvenda=" + form.getCdvendaassociada());
		}else if (form.getCdNota() == null && form.getCdvendaassociada() != null &&
				StringUtils.isNotBlank(form.getWhereInCdexpedicaoassociada()) &&
				notaVendaService.existeNotaEmitida(new Venda(form.getCdvendaassociada()), form.getWhereInCdexpedicaoassociada())){
			request.addError("J� existe uma nota vinculada a venda/expedi��o.");
			return new ModelAndView("redirect:/faturamento/crud/Expedicao");
		}
		
		return super.doSalvar(request, form);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void salvar(WebRequestContext request, Notafiscalproduto bean)throws Exception {
		bean.setModeloDocumentoFiscalEnum(getModelo());
		
		boolean isNfe = isNfe();
		if(!isNfe){
			bean.setOperacaonfe(Operacaonfe.CONSUMIDOR_FINAL);
			bean.setLocaldestinonfe(Localdestinonfe.INTERNA);
			bean.setFinalidadenfe(Finalidadenfe.NORMAL);
			bean.setTipooperacaonota(Tipooperacaonota.SAIDA);
		}
		
		boolean isCriar = false;
		boolean usarSequencial = false;
		String whereInExpedicaoproducao = bean.getWhereInExpedicaoproducao();
		String whereInColeta = bean.getWhereInColeta();
		String whereInPedidovendaRetorno = bean.getWhereInPedidovendaRetorno();
		String whereInVendaRetorno = bean.getWhereInVendaRetorno();
		String whereInProducaoordem = bean.getWhereInProducaoordem();
		String motivodevolucaocoleta = bean.getMotivodevolucaocoleta();
		String whereInPedidovenda = bean.getWhereInPedidovenda();
		Boolean devolucao = bean.getDevolucao();
		String whereInEntradafiscal = bean.getWhereInEntradaFiscal();
		String whereInRequisicao = bean.getWhereInRequisicao();
		String romaneios = bean.getRomaneios();
		String whereInVenda = bean.getWhereInCdvenda();
		if(bean.getSerienfe() == null){
			Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.serienfe, empresa.serieNfce");
			if(isNfe){
				if(empresa == null || empresa.getSerienfe() == null){
					throw new SinedException("A empresa "+empresa.getNome()+" n�o possui o campo 'S�rie NF-e' preenchido.");
				}
				bean.setSerienfe(empresa.getSerienfe());
			} else {
				if(empresa == null || empresa.getSerieNfce() == null){
					throw new SinedException("A empresa "+empresa.getNome()+" n�o possui o campo 'S�rie NFC-e' preenchido.");
				}
				bean.setSerienfe(empresa.getSerieNfce());
			}
		}
		
		Object attribute = request.getSession().getAttribute("listaItensNotaFiscalProduto" + bean.getIdentificadortela());
		if (attribute != null) {
			bean.setListaItens((List<Notafiscalprodutoitem>)attribute);
		}			
		
		if(bean.getEnderecoavulso() == null || !bean.getEnderecoavulso()){
			bean.setEnderecoavulso(null);
			bean.setEndereconota(null);
		}else {
			if(bean.getEndereconota() != null && bean.getEndereconota().getEnderecotipo() == null){
				bean.getEndereconota().setEnderecotipo(Enderecotipo.UNICO);
			}
		}
		
		if(bean.getUsarSequencial() != null && bean.getUsarSequencial()){
			usarSequencial = true;
		}
		
		if(bean.getCdNota() == null){
			isCriar = true;
			
			if(!usarSequencial && isCriar){
				notaHistoricoService.adicionaHistoricoEmitidaObservacao(bean,"N�mero da nota n�o foi gerado sequencialmente.");
			} else if(whereInRequisicao == null){
				notaHistoricoService.adicionaHistoricoEmitida(bean);
			}
			
			String ids[] = null;
			String link = null;
			String descricao = null;
			if(whereInExpedicaoproducao != null && !whereInExpedicaoproducao.trim().isEmpty()){
				ids = whereInExpedicaoproducao.split(",");
				link = "/w3erp/producao/crud/Expedicaoproducao?ACAO=consultar&cdexpedicaoproducao=";
				descricao = "Referente �s expedi��es: ";
			} else if(whereInEntradafiscal != null && !whereInEntradafiscal.trim().isEmpty()){
				ids = whereInEntradafiscal.split(",");
				link = "/w3erp/fiscal/crud/Entradafiscal?ACAO=consultar&cdentregadocumento=";
				descricao = "Entrada fiscal: ";
			} else if(whereInColeta != null && !whereInColeta.trim().isEmpty()){
				ids = whereInColeta.split(",");
				link = "/w3erp/faturamento/crud/Coleta?ACAO=consultar&cdcoleta=";
				descricao = "Coleta: ";
			} else if(whereInPedidovenda != null && !whereInPedidovenda.trim().isEmpty()){
				ids = whereInPedidovenda.split(",");
				String complementoOtr = SinedUtil.complementoOtr();
				link = "/w3erp/faturamento/crud/Pedidovenda"+complementoOtr+"?ACAO=consultar&cdpedidovenda=";
				
				descricao = "Pedido Venda: ";
			} else if(whereInVenda != null && !whereInVenda.trim().isEmpty()){
				ids = whereInVenda.split(",");
				String complementoOtr = SinedUtil.complementoOtr();
				link =  "/w3erp/faturamento/crud/Venda"+complementoOtr+"?ACAO=consultar&cdvenda=";
				descricao = "Venda: ";
			}
			
			if(ids != null){
				notafiscalprodutoService.setObservacaoHistoricoNota(bean, ids, link, descricao, false);
				String complementoOtr = SinedUtil.complementoOtr();
				if(StringUtils.isNotBlank(whereInPedidovendaRetorno)){
					ids = whereInPedidovendaRetorno.split(",");
					link = "/w3erp/faturamento/crud/Pedidovenda"+complementoOtr+"?ACAO=consultar&cdpedidovenda=";
					
					descricao = "Pedido de Venda: ";
					notafiscalprodutoService.setObservacaoHistoricoNota(bean, ids, link, descricao, true);
				}else if(StringUtils.isNotBlank(whereInVendaRetorno)){
					ids = whereInVendaRetorno.split(",");
					link = "/w3erp/faturamento/crud/Venda"+complementoOtr+"?ACAO=consultar&cdvenda=";
					
					descricao = "Venda: ";
					notafiscalprodutoService.setObservacaoHistoricoNota(bean, ids, link, descricao, true);
				}
			}
			
			if(bean.getDiferencaLancamentoConfirmado() != null && bean.getDiferencaLancamentoConfirmado()){
				NotaHistorico notaHistorico = bean.getListaNotaHistorico().get(0);
				notaHistorico.setObservacao("Inconsist�ncia nos lan�amentos tribut�veis. " + 
						(StringUtils.isNotEmpty(notaHistorico.getObservacao()) ? notaHistorico.getObservacao() : ""));
			}
			
			if (bean.getPossuiTemplateInfoContribuinte() == null || !bean.getPossuiTemplateInfoContribuinte()) {
				notafiscalprodutoService.verificarInfAproveitamentocredito(bean);
			}
			
			if (whereInProducaoordem!=null){
				for (String id: whereInProducaoordem.replace(" ", "").split("\\,")){
					garantiareformaService.saveProducao(new Producaoordem(Integer.valueOf(id)), false, true);
				}
			}
			
		} else if(bean.getIsCriar() != null && bean.getIsCriar()){
			isCriar = true;			
		} 
		Integer cdvendaassociada = bean.getCdvendaassociada();
		String whereInCdexpedicaoassociada = bean.getWhereInCdexpedicaoassociada();
		
		
		this.telefoneTransienteParaPersistente(bean);
		
		List<Notafiscalprodutoitem> listaItens = bean.getListaItens();
		if(bean.getCdNota() != null){
			notaHistoricoService.adicionaHistoricoAlterada(bean);
		}
		String numeroAnteriorNota = null;
		boolean atualizarNumeroHistoricovenda = false;
		if(bean.getCdNota() != null && bean.getNumero() != null && !"".equals(bean.getNumero())){
			Notafiscalproduto nfp = notafiscalprodutoService.load(bean, "notafiscalproduto.numero");
			if(nfp != null && nfp.getNumero() != null && !"".equals(nfp.getNumero()) && !bean.getNumero().equals(nfp.getNumero())){
				atualizarNumeroHistoricovenda = true;
				numeroAnteriorNota = nfp.getNumero();
			}
		}

		if(!isNfe){
			try{
				if(bean.getInutilizarNumero() != null && bean.getInutilizarNumero() && bean.getEmpresaAntiga() != null && bean.getNumeroAntigo() != null){
					boolean havePadrao = configuracaonfeService.havePadrao(null, Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, bean.getEmpresaAntiga());
					
					if(havePadrao){
						Configuracaonfe configuracaonfePadrao = configuracaonfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, bean.getEmpresaAntiga());
						
						Integer numeroAntigo = Integer.parseInt(bean.getNumeroAntigo());
						
						Inutilizacaonfe inutilizacaonfe = new Inutilizacaonfe();
						inutilizacaonfe.setEmpresa(bean.getEmpresaAntiga());
						inutilizacaonfe.setConfiguracaonfe(configuracaonfePadrao);
						inutilizacaonfe.setDtinutilizacao(SinedDateUtils.currentDate());
						inutilizacaonfe.setNuminicio(numeroAntigo);
						inutilizacaonfe.setNumfim(numeroAntigo);
						inutilizacaonfe.setJustificativa("Empresa alterada na nota");
						inutilizacaonfe.setSituacao(Inutilizacaosituacao.GERADO);
						
						inutilizacaonfeService.saveOrUpdate(inutilizacaonfe);
					} else {
						request.addMessage("N�o foi poss�vel realizar a inutiliza�ao do n�mero " + bean.getNumeroAntigo() + ", pois n�o existe nenhuma configura��o padr�o para NF-e para a empresa antiga. Favor realizar a inutiliza��o manualmente.", MessageType.WARN);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				request.addMessage("N�o foi poss�vel realizar a inutiliza�ao do n�mero " + bean.getNumeroAntigo() + "." + e.getMessage() + " Favor realizar a inutiliza��o manualmente.", MessageType.WARN);
			}
		}

		Set<Ajustefiscal> listaAjusteFiscal = null;
		if(bean.getCdNota() != null){
			listaAjusteFiscal = SinedUtil.listToSet(ajustefiscalService.buscarItemComAjusteFiscalNota(bean), Ajustefiscal.class);  
		}
		
		if(bean.getCdNota() ==null && SinedUtil.isRecapagem() && parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("NOTA") && Tipooperacaonota.SAIDA.equals(bean.getTipooperacaonota())){
			bean.setSituacaoExpedicao(SituacaoVinculoExpedicao.PENDENTE);
		}

		BaixaestoqueEnum baixaestoqueEnum = BaixaestoqueEnum.APOS_VENDAREALIZADA;
		GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
		Venda vendaassociada = null;
		if (cdvendaassociada != null){
			vendaassociada = new Venda(cdvendaassociada);
			vendaassociada = vendaService.loadForEntrada(vendaassociada);
			
			if(vendaassociada.getPedidovendatipo() != null){
				baixaestoqueEnum = vendaassociada.getPedidovendatipo().getBaixaestoqueEnum();
				geracaocontareceberEnum = vendaassociada.getPedidovendatipo().getGeracaocontareceberEnum();
			}
			
			vendaassociada.setListavendamaterial(vendamaterialService.findByVenda(vendaassociada));
			vendaassociada.setListaVendamaterialmestre(vendamaterialmestreService.findByVenda(vendaassociada));
						
			if(BaixaestoqueEnum.APOS_EMISSAONOTA.equals(baixaestoqueEnum)){
				boolean abortaItensSemLote = false;
				for (Vendamaterial vendamaterial : vendaassociada.getListavendamaterial()){
					abortaItensSemLote = abortaItensSemLote || !materialService.validateObrigarLote(vendamaterial, request);
				}
				if(abortaItensSemLote){
					throw new SinedException("N�o foi poss�vel salvar a nota.");
				}
			}
		}
		List<String> listaHistoricoAlteracoesLote = new ArrayList<String>();
		for(Notafiscalprodutoitem item: bean.getListaItens()){
			if(item.getLoteestoque() != null && materialService.obrigarLote(item.getMaterial())){
				if(cdvendaassociada != null && Util.objects.isPersistent(item.getVendamaterial())){
					Vendamaterial vm = vendamaterialService.loadWithLoteEstoque(item.getVendamaterial());
					if(vm != null && !item.getLoteestoque().equals(vm.getLoteestoque())){
						Material material = materialService.load(vm.getMaterial(), "material.cdmaterial, material.nome");
						listaHistoricoAlteracoesLote.add("O material "+material.getNome()+" teve seu lote de origem alterado para o lote "+item.getLoteestoque().getNumerovalidade()+".");
					}
				}
			}
		}
		
		super.salvar(request, bean);
		entradafiscalService.verificaExclusaoAjusteFiscal(listaAjusteFiscal, bean);
		
		if (usarSequencial) {
			notafiscalprodutoService.updateNumeroNFProduto(bean);
		}
		
		if(bean.getListaDuplicata() != null && bean.getListaDuplicata().size() > 0){
			Integer i = 1;
			for (Notafiscalprodutoduplicata dup : bean.getListaDuplicata()) {
				if(dup.getNumero() == null || dup.getNumero().trim().equals("") || i.toString().equals(dup.getNumero())){
					dup.setNumero(bean.getNumero() + (parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_VENDA) ? "/" + i : ""));
					notafiscalprodutoduplicataService.updateNumeroDuplicata(dup, dup.getNumero());
				}
				i++;
			}
		}
		
		boolean existeExpedicaoConfirmadaByVenda = false;
		String expedicoes = bean.getExpedicoes();

		if (vendaassociada != null){
			if (vendaassociada.getColaborador() != null && vendaassociada.getColaborador().getCdpessoa() != null){
				vendaassociada.setColaborador(colaboradorService.load(vendaassociada.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
			}
			if (vendaassociada.getCliente() != null && vendaassociada.getCliente().getCdpessoa() != null){
				vendaassociada.setCliente(clienteService.load(vendaassociada.getCliente(), "cliente.cdpessoa, cliente.nome"));
			}
			if (vendaassociada.getEmpresa()!=null && vendaassociada.getEmpresa().getCdpessoa()!=null){
				vendaassociada.setEmpresa(empresaService.loadForVenda(vendaassociada.getEmpresa()));
			}

			if(StringUtils.isBlank(whereInCdexpedicaoassociada)){
				existeExpedicaoConfirmadaByVenda = expedicaoService.haveExpedicaoByVenda(vendaassociada, Expedicaosituacao.CONFIRMADA);
				
				if(!pedidovendatipoService.gerarExpedicaoVenda(vendaassociada.getPedidovendatipo(), false)){
					if(baixaestoqueEnum == null || !BaixaestoqueEnum.NAO_BAIXAR.equals(baixaestoqueEnum)){
						if(StringUtils.isBlank(expedicoes) || (baixaestoqueEnum != null && BaixaestoqueEnum.APOS_EMISSAONOTA.equals(baixaestoqueEnum))){
							if(!existeExpedicaoConfirmadaByVenda && !movimentacaoestoqueService.existeMovimentacaoestoque(vendaassociada)){
								if(BaixaestoqueEnum.APOS_EMISSAONOTA.equals(baixaestoqueEnum) && !SituacaoNotaEnum.AUTORIZADA.equals(vendaassociada.getPedidovendatipo().getSituacaoNota())){
									vendaService.verificarSaidaMaterialproducao(vendaassociada);
									vendaassociada.setDtestoqueEmissaonota(bean.getDtEmissao());
									for (Vendamaterial vendamaterial : vendaassociada.getListavendamaterial()){
										if(vendamaterial.getMaterial() != null){
											if(vendamaterial.getMaterial() != null && (vendamaterial.getMaterial().getServico() == null || !vendamaterial.getMaterial().getServico())){
												vendaService.geraMovimentacaoVenda(vendaassociada, vendamaterial, bean);
											}
										}
									}
								}
								
							}
						}
					}
				}
			}
			
			if(geracaocontareceberEnum != null && GeracaocontareceberEnum.APOS_EMISSAONOTA.equals(geracaocontareceberEnum)){
				Date dtsaida = bean.getDatahorasaidaentrada() != null ? new Date(bean.getDatahorasaidaentrada().getTime()) : null;
				vendaService.gerarContareceberAposEmissaonota(request, vendaassociada, bean.getDtEmissao(), dtsaida, bean.getNumero(), bean.getValor(), bean.getListaDuplicata(), NotaTipo.NOTA_FISCAL_PRODUTO, bean.getSomenteproduto() != null && bean.getSomenteproduto(), null, true, bean.getContaboleto(), bean.getPrazopagamentofatura(), false);
			}
			if(parametrogeralService.getBoolean(Parametrogeral.RECAPAGEM)){
				for (Vendamaterial vendamaterial : vendaassociada.getListavendamaterial()){
					boolean temGarantiaVinculada = vendamaterial.getGarantiareformaitem() != null || (vendamaterial.getPedidovendamaterial() != null && vendamaterial.getPedidovendamaterial().getGarantiareformaitem() != null);
					if(temGarantiaVinculada){
						vendaService.utilizarGarantia(vendamaterial, vendaassociada);
					}
				}
			}
		}
		
		try {
			if (BooleanUtils.isTrue(bean.getAgruparContas()) && !StringUtils.isEmpty(bean.getWhereInCdvenda()) && BooleanUtils.isTrue(bean.getCadastrarcobranca())) {
				GeraReceita geraReceita = notaService.criarGeraReceita(bean);
				geraReceita.setValorUmaParcela(notafiscalprodutoService.getValorTotalReceita(bean));
				geraReceita.setPrazoPagamento(bean.getPrazopagamentofatura());
				if(bean.getValordescontofatura() != null){
					geraReceita.setDescontoContratualMny(bean.getValordescontofatura());
				}
				geraReceita.setQtdeParcelas(bean.getListaDuplicata().size());
				Documento documento = documentoService.gerarDocumento(geraReceita, true);
				
				if(documento.getRateio() != null && SinedUtil.isListNotEmpty(documento.getRateio().getListaRateioitem())){
					for(Rateioitem rateioitem : documento.getRateio().getListaRateioitem()){
						if(rateioitem.getContagerencial() == null){
							 throw new Exception(bean.getNumero() + " - Material ou empresa sem centro de custo para o rateio.");
						}else if(rateioitem.getCentrocusto() == null){
							throw new Exception(bean.getNumero() + " - Material ou empresa sem centro de custo para o rateio.");
						}
					}
				}
				
				documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
				documento.setTipopagamento(Tipopagamento.CLIENTE);
				documento.ajustaBeanToSave();
				
				contareceberService.saveOrUpdate(documento);
				
				List<Venda> vendas = vendaService.findForCobranca(bean.getWhereInCdvenda());
				StringBuilder linkHistoricoNota = new StringBuilder();
				StringBuilder linkHistoricoDocumento = new StringBuilder();
				
				if(SinedUtil.isListNotEmpty(documento.getListaDocumento())){
					for (Documento documentoFound : documento.getListaDocumento()) {
						linkHistoricoDocumento.append("<a href=\"javascript:visualizarContaReceber("+documentoFound.getCddocumento()+");\">"+documentoFound.getCddocumento()+"</a>,");
					}
				}else {
					linkHistoricoDocumento.append("<a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+");\">"+documento.getCddocumento()+"</a>,");
				}
				
				for (Venda venda : vendas) {
					for (int i = 0 ; i < venda.getListavendapagamento().size() ; i++) {
						if(SinedUtil.isListNotEmpty(documento.getListaDocumento())){
							if(venda.getListavendapagamento().size() > i && documento.getListaDocumento().size() > i){
								venda.getListavendapagamento().get(i).setDocumento(documento.getListaDocumento().get(i));
								vendapagamentoService.updateDocumento(venda.getListavendapagamento().get(i));
							}
						}else {
							venda.getListavendapagamento().get(i).setDocumento(documento);
							vendapagamentoService.updateDocumento(venda.getListavendapagamento().get(i));
						}
					}
					
					notaVendaService.saveOrUpdate(new NotaVenda(bean, venda));
					
					Vendahistorico vendahistoricoNota = new Vendahistorico();
					vendahistoricoNota.setVenda(venda);
					String numeroNota = bean.getNumero() == null || bean.getNumero().equals("") ? "sem n�mero" : bean.getNumero();
					vendahistoricoNota.setAcao("Gera��o da nota " + numeroNota);
					if(isNfe){
						vendahistoricoNota.setObservacao("Visualizar nota: <a href=\"javascript:visualizaNota("+bean.getCdNota()+");\">"+numeroNota+"</a>.");
					} else {
						vendahistoricoNota.setObservacao("Visualizar NFC-e: <a href=\"javascript:visualizaNFCe("+bean.getCdNota()+");\">"+numeroNota+"</a>.");
					}
					vendahistoricoService.saveOrUpdate(vendahistoricoNota);
					
					Vendahistorico vendahistoricoConta = new Vendahistorico();
					vendahistoricoConta.setVenda(venda);
					String numeroConta = bean.getNumero() == null || bean.getNumero().equals("") ? "sem n�mero" : bean.getNumero();
					vendahistoricoConta.setAcao("GERA��O DA CONTA A RECEBER " + numeroConta);
					vendahistoricoConta.setObservacao("CONTAS A RECEBER GERADAS AP�S NOTA: " + linkHistoricoDocumento.toString().substring(0, linkHistoricoDocumento.toString().length() - 1) + ".");
					vendahistoricoService.saveOrUpdate(vendahistoricoConta);
					
					if(notaVendaService.existeNotaEmitida(venda)){
						vendaService.updateSituacaoVenda(venda.getCdvenda().toString(), Vendasituacao.FATURADA);
					}
					
					venda.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(venda));
					venda.setListavendamaterial(vendamaterialService.findByVenda(venda));
					for (Vendapagamento vendapagamento : venda.getListavendapagamento()) {
						if(vendapagamento.getDocumento()!=null && vendapagamento.getDocumento().getCddocumento()!=null){
							vendaService.verificaComissionamento(vendapagamento, venda.getListavendapagamento().size(), null, null);
						}
					}
					
					linkHistoricoNota.append(" <a href=\"javascript:visualizaVenda(" + venda.getCdvenda() + ");\">" + venda.getCdvenda() + "</a>,");
				}
				
				for (Documento documentoFound : documento.getListaDocumento()) {
					Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(documentoFound);
					documentohistorico.setObservacao("Origem venda: " + linkHistoricoNota.toString().substring(0, linkHistoricoNota.toString().length() - 1) + ".");
					documentohistoricoService.saveOrUpdate(documentohistorico);
					
					for (Venda venda : vendas) {
						Documentoorigem documentoorigem = new Documentoorigem();
						documentoorigem.setDocumento(documentoFound);
						documentoorigem.setVenda(venda);
						documentoorigemService.saveOrUpdate(documentoorigem);
					}
				}
				
				NotaHistorico notaHistorico = new NotaHistorico(bean.getCdNota(), bean, bean.getNotaStatus(), 
						"Visualizar venda: " + linkHistoricoNota.toString().substring(0, linkHistoricoNota.toString().length() - 1) + ".", 
						SinedUtil.getCdUsuarioLogado(), SinedDateUtils.currentTimestamp());
				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(bean.getCdNota() != null && bean.getEndereconota() != null && bean.getEndereconota().getCdendereco() != null){
			enderecoService.updatePessoa(bean.getEndereconota(), bean);
		}
		
		if (vendaassociada != null){
			vendaService.notafiscalGerada(vendaassociada, whereInCdexpedicaoassociada, bean, expedicoes, false, bean.getModeloDocumentoFiscalEnum());
		}
		if(StringUtils.isNotBlank(whereInCdexpedicaoassociada) && StringUtils.isBlank(expedicoes)){
			expedicaoService.updateSituacao(whereInCdexpedicaoassociada.toString(), Expedicaosituacao.FATURADA);
		}
		
		if(isCriar && ((bean.getListaItens() != null && !bean.getListaItens().isEmpty()) || (listaItens != null && !listaItens.isEmpty()))){
			if(bean.getListaItens() != null && !bean.getListaItens().isEmpty()){
				listaItens = bean.getListaItens();
			}
			for(Notafiscalprodutoitem item : listaItens){
				if(item.getTipocobrancaicms() != null){
					if(item.getTipocobrancaicms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO) ||
							item.getTipocobrancaicms().equals(Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE)){
						entregaService.ajustaQtdeutilizada(item);
					}
				}
			}
		}
		if (bean.getCliente().getRazaosocial()!=null){
			if (bean.getCliente().getRazaosocial().length() > 60){
				request.addMessage("Favor alterar a raz�o social do cliente para no m�ximo 60 caracteres.", MessageType.ERROR);
			}
		}
		
		
		bean.setIsCriar(isCriar);
		
		if(romaneios != null && !romaneios.equals("")){
			String obsHistorico = "<a href=\"javascript:visualizaRomaneio(cdromaneio);\">cdromaneio</a>";
			
			StringBuilder sb = new StringBuilder();
			String[] ids = romaneios.split(",");

			for (int i = 0; i < ids.length; i++) {
				if(sb.length() > 0){
					sb.append(", ");
				}
				sb.append(obsHistorico.replaceAll("cdromaneio", ids[i]));
				
				Notaromaneio notaromaneio = new Notaromaneio();
				notaromaneio.setNota(bean);
				notaromaneio.setRomaneio(new Romaneio(Integer.parseInt(ids[i])));
				
				notaromaneioService.saveOrUpdate(notaromaneio);
			}
			
			NotaHistorico notaHistorico = new NotaHistorico(bean, "Simples remessa do(s) romaneio(s): " + sb.toString(), NotaStatus.EMITIDA);
			notaHistoricoService.saveOrUpdate(notaHistorico);
		}
		
		if(SinedUtil.isListNotEmpty(listaHistoricoAlteracoesLote)){
			for(String hist: listaHistoricoAlteracoesLote){
				NotaHistorico notaHistorico = new NotaHistorico(bean, hist, NotaStatus.EMITIDA);
				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
		}
		
		if(whereInExpedicaoproducao != null && !whereInExpedicaoproducao.trim().isEmpty()){
			expedicaoproducaoService.updateSituacao(whereInExpedicaoproducao, Expedicaosituacao.FATURADA);
			String observacao = "Nota fiscal: <a href='/w3erp/faturamento/crud/" + (isNfe ? "Notafiscalproduto" : "Notafiscalconsumidor") + "?ACAO=consultar&cdNota=";
			observacao += bean.getCdNota() +"'>"+bean.getCdNota()+"</a>";
			expedicaoproducaohistoricoService.saveHistorico(whereInExpedicaoproducao, observacao, Expedicaoacao.FATURADA, null);
		} else if(whereInEntradafiscal != null && !whereInEntradafiscal.trim().isEmpty()){
			String observacao = "Nota fiscal: <a href='/w3erp/faturamento/crud/" + (isNfe ? "Notafiscalproduto" : "Notafiscalconsumidor") + "?ACAO=consultar&cdNota=";
			observacao += bean.getCdNota() +"'>"+bean.getCdNota()+"</a>";
			String cdEntradaFiscal[] = whereInEntradafiscal.split(",");
			for(int i=0; i<cdEntradaFiscal.length; i++){
				Entregadocumentohistorico edh = entradafiscalService.criaEntregadocumentohistorico(
						new Entregadocumento(Integer.parseInt(cdEntradaFiscal[i])), Entregadocumentosituacao.ALTERADA);
				edh.setObservacao(observacao);
				edh.setNotafiscalproduto(bean);
				entregadocumentohistoricoService.saveOrUpdate(edh);
						
			}
			
			for(Notafiscalprodutoitem nfpi : listaItens){
				if(nfpi.getCdcoletamaterial() != null){
					ColetaMaterial coletaMaterial =  coletaMaterialService.loadForDevolucao(new ColetaMaterial(nfpi.getCdcoletamaterial()));
					Coletahistorico coletahistorico = new Coletahistorico();
					coletahistorico.setAcao(Coletaacao.NOTA_EMITIDA);
					coletahistorico.setColeta(coletaMaterial.getColeta());
					if(isNfe){
						coletahistorico.setObservacao(Coletaacao.NOTA_EMITIDA.getNome()+": <a href=\"javascript:visualizaNota("+bean.getCdNota()+");\">"+bean.getNumero()+" ("+bean.getCdNota()+")"+"</a>.");
					} else {
						coletahistorico.setObservacao(Coletaacao.NOTA_EMITIDA.getNome()+": <a href=\"javascript:visualizaNFCe("+bean.getCdNota()+");\">"+bean.getNumero()+" ("+bean.getCdNota()+")"+"</a>.");
					}
					coletahistoricoService.saveOrUpdate(coletahistorico);
					
					coletaMaterialService.updateQtdeDevolvidaNota(coletaMaterial, nfpi.getQtde());
				}
				continue;
			}
		
		} else if(StringUtils.isNotBlank(whereInColeta) || (StringUtils.isNotBlank(whereInProducaoordem) && devolucao != null && devolucao)){
			if(StringUtils.isNotBlank(whereInColeta)){
				NotafiscalprodutoColeta nfpc;
				for(String cdcoleta : whereInColeta.split(",")){
					nfpc = new NotafiscalprodutoColeta(Integer.parseInt(cdcoleta), bean.getCdNota(), devolucao);
					notafiscalprodutoColetaService.saveOrUpdate(nfpc);
					
					if(StringUtils.isNotBlank(whereInPedidovendaRetorno) || StringUtils.isNotBlank(whereInVendaRetorno)){
						coletahistoricoService.gerarHistoricoNotaRetorno(bean, new Coleta(Integer.parseInt(cdcoleta)));
					}
				}
				
				if(StringUtils.isNotBlank(whereInPedidovendaRetorno)){
					for(String cdpedidovenda : whereInPedidovendaRetorno.split(",")){
						notafiscalprodutoretornopedidovendaService.saveOrUpdate(new Notafiscalprodutoretornopedidovenda(Integer.parseInt(cdpedidovenda), bean.getCdNota()));
						pedidovendahistoricoService.gerarHistoricoNotaRetorno(bean, new Pedidovenda(Integer.parseInt(cdpedidovenda)));
					}
				}
				if(StringUtils.isNotBlank(whereInVendaRetorno)){
					for(String cdvenda : whereInVendaRetorno.split(",")){
						notafiscalprodutoretornovendaService.saveOrUpdate(new Notafiscalprodutoretornovenda(Integer.parseInt(cdvenda), bean.getCdNota()));
						vendahistoricoService.gerarHistoricoNotaRetorno(bean, new Venda(Integer.parseInt(cdvenda)));
					}
				}
			}
			if(StringUtils.isNotBlank(whereInProducaoordem)){
				NotafiscalprodutoProducaoordem nfppo;
				for(String cdproducaoordem : whereInProducaoordem.split(",")){
					nfppo = new NotafiscalprodutoProducaoordem(Integer.parseInt(cdproducaoordem), bean.getCdNota(), devolucao);
					notafiscalprodutoProducaoordemService.saveOrUpdate(nfppo);
				}
			}
			
			if(devolucao != null && devolucao && bean.getListaItens() != null && !bean.getListaItens().isEmpty()){
				List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
				HashMap<Producaoordem, Producaoordemhistorico> mapProducaoordemHistorico = new HashMap<Producaoordem, Producaoordemhistorico>();
				List<Producaoordemmaterial> listaProducaoordemmaterial = new ArrayList<Producaoordemmaterial>();
				
				Movimentacaoestoqueorigem movimentacaoestoqueorigem;
				Movimentacaoestoque movimentacaoestoque;
				for(Notafiscalprodutoitem nfpi : listaItens){
					if(StringUtils.isNotBlank(whereInColeta) && !Boolean.TRUE.equals(bean.getDevolucaoColetaConfirmada()) && !Boolean.TRUE.equals(bean.getRegistrarDevolucaoColeta())){
						if(nfpi.getCdcoletamaterial() != null){
							ColetaMaterial coletaMaterial =  coletaMaterialService.loadForDevolucao(new ColetaMaterial(nfpi.getCdcoletamaterial()));
							Coletahistorico coletahistorico = new Coletahistorico();
							coletahistorico.setAcao(Coletaacao.NOTA_EMITIDA);
							coletahistorico.setColeta(coletaMaterial.getColeta());
							if(isNfe){
								coletahistorico.setObservacao(Coletaacao.NOTA_EMITIDA.getNome()+": <a href=\"javascript:visualizaNota("+bean.getCdNota()+");\">"+bean.getNumero()+" ("+bean.getCdNota()+")"+"</a>.");
							} else {
								coletahistorico.setObservacao(Coletaacao.NOTA_EMITIDA.getNome()+": <a href=\"javascript:visualizaNFCe("+bean.getCdNota()+");\">"+bean.getNumero()+" ("+bean.getCdNota()+")"+"</a>.");
							}
							coletahistoricoService.saveOrUpdate(coletahistorico);
							
							coletaMaterialService.updateQtdeDevolvidaNota(coletaMaterial, nfpi.getQtde());
						}
						continue;
					}
					movimentacaoestoque = new Movimentacaoestoque();
					movimentacaoestoque.setMaterial(nfpi.getMaterial());
					movimentacaoestoque.setMaterialclasse(nfpi.getMaterialclasse());
					movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
					movimentacaoestoque.setQtde(nfpi.getQtde());
					movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
					movimentacaoestoque.setLocalarmazenagem(nfpi.getLocalarmazenagem());
					movimentacaoestoque.setEmpresa(bean.getEmpresa());
					movimentacaoestoque.setValor(nfpi.getValorunitario());
					
					movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
					movimentacaoestoqueorigem.setNotafiscalproduto(bean);
					movimentacaoestoqueorigem.setNotaFiscalProdutoItem(nfpi);
					movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
					
					movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
					
					listaMovimentacaoestoque.add(movimentacaoestoque);
					
					if(nfpi.getCdcoletamaterial() != null){
						ColetaMaterial coletaMaterial =  coletaMaterialService.loadForDevolucao(new ColetaMaterial(nfpi.getCdcoletamaterial()));
						
						ColetaMaterialDevolucao coletaMaterialDevolucao = new ColetaMaterialDevolucao();
						coletaMaterialDevolucao.setColetamaterial(coletaMaterial);
						coletaMaterialDevolucao.setNotafiscalprodutoitem(nfpi);
						
						coletaMaterialDevolucaoService.saveOrUpdate(coletaMaterialDevolucao);
						coletaMaterialService.adicionaQtdeDevolvida(coletaMaterial, nfpi.getQtde());
						coletaMaterialService.updateQtdeDevolvidaNota(coletaMaterial, nfpi.getQtde());
//						coletaMaterialService.adicionaMotivodevolucao(coletaMaterial, nfpi.getMotivodevolucao());
//						coletamaterialmotivodevolucaoService.save(coletaMaterial, nfpi.getMotivodevolucao(), true);
						
						if (nfpi.getListaMotivodevolucao()!=null){
							coletamaterialmotivodevolucaoService.delete(coletaMaterial);
							for (Motivodevolucao motivodevolucao: nfpi.getListaMotivodevolucao()){
								coletamaterialmotivodevolucaoService.save(coletaMaterial, motivodevolucao, false);							
							}
						}
						
						StringBuilder obs = new StringBuilder();
						obs.append("Devolu��o do material: " + coletaMaterial.getMaterial().getNome());
						if(coletaMaterial.getObservacao() != null && !coletaMaterial.getObservacao().trim().equals("")){
							obs.append(" (").append(coletaMaterial.getObservacao()).append(")");
						}
						if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
							for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
								if(itemServico.getPedidovendamaterial() != null && itemServico.getPedidovendamaterial().getCdpedidovendamaterial() != null){
									obs.append(" (").append(itemServico.getPedidovendamaterial().getCdpedidovendamaterial()).append(")");
								}					
							}
						}
						
//						if(coletaMaterial.getPedidovendamaterial() != null && coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
//							obs.append(" (").append(coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial()).append(")");
//						}
						
						Coletahistorico coletahistorico = new Coletahistorico();
						coletahistorico.setAcao(Coletaacao.NOTA_EMITIDA);
						coletahistorico.setColeta(coletaMaterial.getColeta());
						if(isNfe){
							coletahistorico.setObservacao(Coletaacao.NOTA_EMITIDA.getNome()+": <a href=\"javascript:visualizaNota("+bean.getCdNota()+");\">"+bean.getNumero()+" ("+bean.getCdNota()+")"+"</a>.");
						} else {
							coletahistorico.setObservacao(Coletaacao.NOTA_EMITIDA.getNome()+": <a href=\"javascript:visualizaNFCe("+bean.getCdNota()+");\">"+bean.getNumero()+" ("+bean.getCdNota()+")"+"</a>.");
						}
						coletahistoricoService.saveOrUpdate(coletahistorico);
						
						if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
							StringBuilder whereInProducaoordemmaterial = new StringBuilder();
							String whereInPedidovendamaterial = "";
							if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())){
								whereInPedidovendamaterial = SinedUtil.listAndConcatenate(coletaMaterial.getListaColetaMaterialPedidovendamaterial(), 
									"pedidovendamaterial.cdpedidovendamaterial", ",");
							}
							
							if(StringUtils.isNotEmpty(whereInPedidovendamaterial)){
								List<Producaoordemmaterial> listaPOM = producaoordemmaterialService.findByPedidovendamaterial(whereInPedidovendamaterial);
								for (Producaoordemmaterial producaoordemmaterial : listaPOM) {
									if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
										whereInProducaoordemmaterial.append(producaoordemmaterial.getCdproducaoordemmaterial()).append(",");
									}
									producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, nfpi.getQtde());
								}
								if(!whereInProducaoordemmaterial.toString().equals("")){
									producaoordemService.verificaCancelamentoProducaoordemQtdeDevolvida(request, whereInProducaoordemmaterial.substring(0, whereInProducaoordemmaterial.length()-1), bean.getInterromperProducao());
								}
							}
						}
						
//						if(coletaMaterial.getPedidovendamaterial() != null && coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
//							StringBuilder whereInProducaoordemmaterial = new StringBuilder();
//							List<Producaoordemmaterial> listaPOM = producaoordemmaterialService.findByPedidovendamaterial(coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial().toString());
//							for (Producaoordemmaterial producaoordemmaterial : listaPOM) {
//								if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
//									whereInProducaoordemmaterial.append(producaoordemmaterial.getCdproducaoordemmaterial()).append(",");
//								}
//								producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, nfpi.getQtde());
//							}
//							if(!whereInProducaoordemmaterial.toString().equals("")){
//								producaoordemService.verificaCancelamentoProducaoordemQtdeDevolvida(request, whereInProducaoordemmaterial.substring(0, whereInProducaoordemmaterial.length()-1), bean.getInterromperProducao());
//							}
//						}
					}else if(nfpi.getCdproducaoordemmaterial() != null){
						Producaoordemmaterial producaoordemmaterial =  producaoordemmaterialService.loadForDevolucao(new Producaoordemmaterial(nfpi.getCdproducaoordemmaterial()));
						producaoordemService.salvarRegistrarDevolucaoItem(producaoordemmaterial, nfpi.getMaterial(), 
								nfpi.getLocalarmazenagem(), bean.getEmpresa(), nfpi.getQtde(), nfpi.getMotivodevolucao(), 
								listaProducaoordemmaterial, mapProducaoordemHistorico,
								listaMovimentacaoestoque,
								bean.getInterromperProducao());
					}
				}
				
				producaoordemService.salvarHistoricoDevolucao(mapProducaoordemHistorico, bean.getMotivodevolucaocoleta());
				if(SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
					producaoordemService.verificaCancelamentoProducaoordemQtdeDevolvida(request, SinedUtil.listAndConcatenate(listaProducaoordemmaterial, "cdproducaoordemmaterial", ","), bean.getInterromperProducao());
				}
				
				String whereInMovimentacaoestoque = CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ",");
				
				String observacaoColeta = "Criada a partir da(s) devolu��o(�es) da(s) coleta(s): " + 
											coletaService.makeLinkHistoricoColeta(whereInColeta) + 
											"<BR/>Sa�da(s) no estoque: " +
											movimentacaoestoqueService.makeLinkHistoricoMovimentacaoestoque(whereInMovimentacaoestoque);
				
				NotaHistorico notaHistorico = new NotaHistorico();
				notaHistorico.setNota(bean);
				notaHistorico.setNotaStatus(NotaStatus.EMITIDA);
				notaHistorico.setObservacao(observacaoColeta);
				notaHistoricoService.saveOrUpdate(notaHistorico);
			}
			
			coletaService.updateSituacaoPedidovendaProducaoordemByColeta(whereInColeta);
			if(motivodevolucaocoleta != null && !"".equals(motivodevolucaocoleta)){
				coletaService.updateMotivoDevolucao(whereInColeta, motivodevolucaocoleta);
			}
		} else if(whereInPedidovenda != null && !whereInPedidovenda.trim().isEmpty()){
			NotafiscalprodutoColeta nfpc;
			List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPedidovenda);
			if(listaColeta != null && !listaColeta.isEmpty()){				
				for(Coleta coleta : listaColeta){
					nfpc = new NotafiscalprodutoColeta(coleta.getCdcoleta(), bean.getCdNota(), Boolean.FALSE);
					notafiscalprodutoColetaService.saveOrUpdate(nfpc);
				}
			}
		}
		
		if(atualizarNumeroHistoricovenda){
			notaVendaService.atualizarNumeroHistoricoVenda(bean, numeroAnteriorNota, false, isNfe);
			notaDocumentoService.atualizarNumeroHistoricoDocumento(bean, numeroAnteriorNota);
			notaHistoricoService.adicionaHistoricoAlterada(bean);
		}
		
		if(whereInRequisicao != null && !whereInRequisicao.isEmpty()){
			requisicaoService.saveHistoricoRequisicao(whereInRequisicao, bean.getCdNota(), false);
			
			notaHistoricoService.salvarHistoricoNotaRequisicao(bean, whereInRequisicao);
			
			requisicaoService.updateEstados(whereInRequisicao,new Requisicaoestado(Requisicaoestado.FATURADO));
			requisicaoService.updateVendaFaturado(whereInRequisicao);
		}
		request.getSession().removeAttribute("listaItensNotaFiscalProduto" + bean.getIdentificadortela());
	}
	
	protected void telefoneTransienteParaPersistente(Notafiscalproduto bean) {
		Telefone telefoneCliente = bean.getTelefoneClienteTransiente();
		if (telefoneCliente != null && telefoneCliente.getCdtelefone() != null) {
			if (telefoneCliente.getCdtelefone() != -1) {
				telefoneCliente = telefoneService.load(telefoneCliente, "telefone.telefone");
				bean.setTelefoneCliente(telefoneCliente.getTelefone());
			} else {
				bean.setTelefoneCliente(notaService.load(bean, "nota.telefoneCliente").getTelefoneCliente());
			}
		} 
		
		Telefone telefoneTransportador = bean.getTelefoneTransportadorTransient();
		if (telefoneTransportador != null && telefoneTransportador.getCdtelefone() != null) {
			if (telefoneTransportador.getCdtelefone() != -1) {
				telefoneTransportador = telefoneService.load(telefoneTransportador, "telefone.telefone");
				bean.setTelefonetransportador(telefoneTransportador.getTelefone());
			} else {
				bean.setTelefonetransportador(notafiscalprodutoService.load(bean, "notafiscalproduto.telefonetransportador").getTelefonetransportador());
			}
		} 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void validateBean(Notafiscalproduto bean, BindException errors) {
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaItensNotaFiscalProduto" + bean.getIdentificadortela());
		if (attribute != null) {
			bean.setListaItens((List<Notafiscalprodutoitem>)attribute);
		}else{
			errors.reject("001", "Sess�o expirada. Favor fazer o processo novamente recarregando a p�gina.");
		}
		
		if (!SinedUtil.isListNotEmpty(bean.getListaItens())) {
			errors.reject("001", "Deve haver pelo menos um produto na Nota Fiscal.");
		}
		
		if(bean.getFormapagamentonfe() != null && bean.getFormapagamentonfe().equals(Formapagamentonfe.OUTROS)){
			errors.reject("001", "N�o � poss�vel utilizar essa forma de pagamento. A op��o permanece apenas para fins de compatibilidade com notas fiscais anteriores a vers�o 4.0 da NFe.");
		}
		
		if(bean.getSerienfe() == null && bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
			Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa,empresa.serienfe,empresa.nome");
			if(empresa.getSerienfe() == null){
				errors.reject("001", "A empresa "+empresa.getNome()+" n�o possui o campo 'S�rie NF-e' preenchido.");
			}
		}

		if(bean.getEmpresa() != null){
			String numeronf = bean.getNumero();
			if(bean.getCdNota() == null && Boolean.TRUE.equals(bean.getUsarSequencial())){
				if(bean.getModeloDocumentoFiscalEnum() != null && bean.getModeloDocumentoFiscalEnum().equals(ModeloDocumentoFiscalEnum.NFCE)){
					numeronf = notafiscalprodutoService.getProximoNumeroNFConsumidor(bean.getEmpresa()).toString();
				}else {
					numeronf = notafiscalprodutoService.getProximoNumeroNFProduto(bean.getEmpresa()).toString();
				}
			}			
			if(StringUtils.isNotBlank(numeronf) &&
				notafiscalprodutoService.isExisteNota(bean.getEmpresa(), numeronf, bean.getSerienfe(), bean.getModeloDocumentoFiscalEnum(), bean)){
				errors.reject("001", "J� existe uma nota de produto com o n�mero e s�rie informado.");
			}
		}
		
		boolean exibirLocal = this.exibirLocal(bean);
		if(bean.getListaItens() != null && exibirLocal){
			if(parametrogeralService.getBoolean(Parametrogeral.LOCAL_VENDA_OBRIGATORIO)){
				for (Notafiscalprodutoitem it : bean.getListaItens()) {
					if(it.getLocalarmazenagem() == null){
						errors.reject("002", "O Local de Armazenagem nos itens da nota � obrigat�rio.");
					}
					
					
				}
			}
		}
		
		// VALIDA��O FEITA NA CRIA��O DO XML DE NF-E TAMB�M
		boolean obrigacaoDocumentoReferenciado = false;
		Finalidadenfe finalidadenfe = bean.getFinalidadenfe();
		if(finalidadenfe != null && finalidadenfe.equals(Finalidadenfe.DEVOLUCAO)){
			obrigacaoDocumentoReferenciado = true;
		}
		List<String> cfopIgnoraDocReferenciado = Arrays.asList(new String[]{
			"1201", "1202", "1410", "1411", "5921", "6921"
		});
		
		boolean obrigarCEST = parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_CEST);
		if(bean.getListaItens() != null){
			Cfopescopo cfopescopo = null;
			for (Notafiscalprodutoitem it : bean.getListaItens()) {
				if(it.getCfop() != null){
					Cfop cfop = cfopService.load(it.getCfop(), "cfop.cdcfop, cfop.codigo, cfop.cfopescopo");
					
					if(obrigacaoDocumentoReferenciado && cfop != null && cfop.getCodigo() != null && cfopIgnoraDocReferenciado.contains(cfop.getCodigo())){
						obrigacaoDocumentoReferenciado = false;
					}
					if(cfop != null && cfop.getCfopescopo() != null){
						if(cfopescopo == null) cfopescopo = cfop.getCfopescopo();
						else {
							if(!cfopescopo.equals(cfop.getCfopescopo())){
								errors.reject("005", "� permitido apenas um escopo de CFOP (Estadual, Interestadual e Exterior).");
							}
						}
					}
				}
				
				if(obrigarCEST && StringUtils.isBlank(it.getCest()) && (
						Tipocobrancaicms.TRIBUTADA_COM_SUBSTITUICAO.equals(it.getTipocobrancaicms()) || 
						Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(it.getTipocobrancaicms()) ||
						Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(it.getTipocobrancaicms()) ||
						Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO.equals(it.getTipocobrancaicms()) ||
						Tipocobrancaicms.OUTROS.equals(it.getTipocobrancaicms()) ||
						(it.getValoricmsst() != null && it.getValoricmsst().compareTo(new Money()) != 0 &&
							(Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(it.getTipocobrancaicms()) ||
							Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(it.getTipocobrancaicms()) ||
							Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(it.getTipocobrancaicms()) ||
							Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(it.getTipocobrancaicms()) ||
							Tipocobrancaicms.OUTROS_SIMPLES_NACIONAL.equals(it.getTipocobrancaicms()))
						))){
					errors.reject("001", "O campo CEST � obrigat�rio. " + (it.getMaterial() != null ? "Material: " + it.getMaterial().getNome() : ""));
				}
				
				if(it.getTipocobrancaipi() != null && it.getTipocalculoipi() == null && (
							Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO.equals(it.getTipocobrancaipi()) ||
							Tipocobrancaipi.ENTRADA_OUTRAS.equals(it.getTipocobrancaipi()) ||
							Tipocobrancaipi.SAIDA_TRIBUTABA.equals(it.getTipocobrancaipi()) ||
							Tipocobrancaipi.SAIDA_OUTRAS.equals(it.getTipocobrancaipi()))){
					errors.reject("001", "O campo Tipo de c�lculo do IPI � obrigat�rio. " + (it.getMaterial() != null ? "Material: " + it.getMaterial().getNome() : ""));
				}
				
			}
		}
		
		if(obrigacaoDocumentoReferenciado && (bean.getListaReferenciada() == null || bean.getListaReferenciada().size() == 0)){
			errors.reject("006", "Para nota com finalidade de devolu��o de mercadoria e o CFOP seja diferente de (" + CollectionsUtil.concatenate(cfopIgnoraDocReferenciado, ", ") + ") � obrigat�rio informar os documentos referenciados.");
		}
		
		if (bean.getCdvendaassociada() != null) {
			Venda vendaFound = vendaService.findForRegistrarDevolucao(bean.getCdvendaassociada().toString());
			Date data = bean.getDtEmissao() != null ? bean.getDtEmissao() : new Date(vendaFound.getDtvenda() != null ? vendaFound.getDtvenda().getTime() : System.currentTimeMillis());
			
			if (vendaFound.getPedidovendatipo() != null && BaixaestoqueEnum.APOS_EMISSAONOTA.equals(vendaFound.getPedidovendatipo().getBaixaestoqueEnum()) &&
					!SituacaoNotaEnum.AUTORIZADA.equals(vendaFound.getPedidovendatipo().getSituacaoNota()) && 
					!movimentacaoestoqueService.existeMovimentacaoestoque(vendaFound)) {
				if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(data, vendaFound.getEmpresa(), vendaFound.getLocalarmazenagem(), vendaFound.getProjeto())) {
					errors.reject("001", "N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.");
				}
			}
		}
		
		if(bean.getListaItens() != null){
			boolean exportacao = false;
			for (Notafiscalprodutoitem it : bean.getListaItens()) {
				if(it.getCfop() != null && 
						it.getCfop().getCodigo() != null && 
						it.getCfop().getCodigo().startsWith("7")){
					exportacao = true;
					break;
				}
			}
			
			if(exportacao){
				if(bean.getUfembarque() == null){
					errors.reject("003", "O campo UF de embarque dos produtos � obrigat�rio para notas de exporta��o (CFOP iniciado com 7).");
				}
				if(bean.getLocalembarque() == null || "".equals(bean.getLocalembarque().trim())){
					errors.reject("004", "O campo Local de embarque � obrigat�rio para notas de exporta��o (CFOP iniciado com 7).");
				}
			}
		}
		
		if(StringUtils.isNotBlank(bean.getInfoadicionalcontrib()) && bean.getInfoadicionalcontrib().length() > 5000){
			errors.reject("001", "O campo Informa��es Adicionais de Interesse do Contribuinte n�o pode ultrapassar o limite de 5000 caracteres.");
		}

		if(StringUtils.isNotBlank(bean.getInfoadicionalfisco()) && bean.getInfoadicionalfisco().length() > 2000){
			errors.reject("001", "O campo Informa��es Adicionais de Interesse do Fisco n�o pode ultrapassar o limite de 2000 caracteres.");
		} 
		
		if(bean.getCdNota() == null && StringUtils.isBlank(bean.getWhereInCdexpedicaoassociada()) && bean.getCdvendaassociada() != null && 
				notafiscalprodutoService.isExisteNotaEmitida(new Venda(bean.getCdvendaassociada()))){
			errors.reject("001", "Existe nota cadastrada para a venda " + bean.getCdvendaassociada() + ".");
		}
		
		super.validateBean(bean, errors);
	}
	
	public void ajaxOnChangeGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao){
		notafiscalprodutoService.ajaxOnChangeGrupotributacao(request, grupotributacao);
	}
	
	public void ajaxCarregaGrupotributacao(WebRequestContext request, Material material){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		Cliente cliente = Util.objects.isPersistent(material.getCliente()) ? clienteService.loadForTributacao(material.getCliente()) : null;
		Naturezaoperacao naturezaoperacao = material.getNaturezaoperacao();
		Empresa empresanota = null; 
		if (material.getEmpresaTributacaoOrigem() != null && material.getEmpresaTributacaoOrigem().getCdpessoa() != null){
			empresanota = material.getEmpresaTributacaoOrigem();
			Empresa empresa = empresaService.loadWithEndereco(material.getEmpresaTributacaoOrigem());
			material.setEnderecoTributacaoOrigem(empresa.getEndereco());
		} else {
			Empresa empresa = empresaService.loadPrincipalWithEndereco();
			material.setEnderecoTributacaoOrigem(empresa.getEndereco());
		}
		
		String notaDtEmissaoString = request.getParameter("notaDtEmissao");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date notaDtEmissao = null;
		try {
			notaDtEmissao = new java.sql.Date(sdf.parse(notaDtEmissaoString).getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Endereco enderecoDestinoCliente = null;
		if(material.getEnderecoavulso() != null && material.getEnderecoavulso() && material.getEndereconotaDestino() != null && 
				material.getEndereconotaDestino().getMunicipio() != null && 
				material.getEndereconotaDestino().getMunicipio().getCdmunicipio() != null){
			enderecoDestinoCliente =  material.getEndereconotaDestino();
			enderecoDestinoCliente.setMunicipio(municipioService.carregaMunicipio(material.getEndereconotaDestino().getMunicipio()));
		}else if(material.getEnderecoTributacaoDestino() != null && material.getEnderecoTributacaoDestino().getCdendereco() != null){
			enderecoDestinoCliente = enderecoService.carregaEnderecoComUfPais(material.getEnderecoTributacaoDestino());
		}
		
		List<Categoria> listaCategoriaCliente = null;
		if(cliente != null){
			listaCategoriaCliente = categoriaService.findByPessoa(cliente);
		}
		
		Material beanMaterial = materialService.loadForTributacao(material);
		
		List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
				grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
																		empresanota, 
																		naturezaoperacao, 
																		beanMaterial.getMaterialgrupo(),
																		cliente,
																		true, 
																		beanMaterial, 
																		null,
																		enderecoDestinoCliente, 
																		null,
																		listaCategoriaCliente,
																		null,
																		material.getOperacaonfe(),
																		getModelo(),
																		material.getLocalDestinoNfe(),
																		notaDtEmissao));
		
		JSON listaGrupotributacaoJSON = View.convertToJson(listaGrupotributacao);
		String jsonGrupotributacao = listaGrupotributacaoJSON.toString(0);
		view.println("var listaGrupotributacao = " + jsonGrupotributacao);
		String cdgrupoTribuacaoJaInformado = request.getParameter("cdGrupoTributacaoDoItem");
		Grupotributacao grupotributacao = null;
		if(StringUtils.isNotEmpty(cdgrupoTribuacaoJaInformado)){
			grupotributacao = new Grupotributacao(Integer.parseInt(cdgrupoTribuacaoJaInformado));
		}else{
			grupotributacao = grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao);
		}
		
		if(grupotributacao != null){
			view.println("var grupotributacaoSelecionado = 'br.com.linkcom.sined.geral.bean.Grupotributacao[cdgrupotributacao=" + grupotributacao.getCdgrupotributacao() + "]';");
		}
	}
	
	/**
	 * M�todo para buscar dados referente ao produto selecionado na listagem de 
	 * produtos no cadastro da nota Fiscal.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialService#getSimboloUnidadeMedida(Material)
	 * @see br.com.linkcom.sined.geral.service.MaterialService#verificaMaterialPatrimonio(Material)
	 * 
	 * @param request
	 * @param material
	 * @author Fl�vio Tavares, Rodrigo Freitas
	 */
	public void ajaxOnChangeMaterial(WebRequestContext request, Material material){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		Empresa empresaTributacaoOrigem = material.getEmpresaTributacaoOrigem();
		Origemproduto origemproduto = material.getOrigemproduto();
		
		
		material = materialService.loadForNFProduto(material);
		materialformulavalorvendaService.setValorvendaByFormula(material, null,null,null,null);
		
		List<Unidademedida> listaUnidademedida = unidademedidaService.getUnidademedidaByMaterial(material);
		
		JSON listaUnidademedidaJSON = View.convertToJson(listaUnidademedida);
		String json = listaUnidademedidaJSON.toString(0);
		view.println("var listaUnidademedida = " + json);
		
		if(material.getUnidademedida() != null && material.getUnidademedida().getCdunidademedida() != null){
			view.println("var unidademedidaSelecionada = 'br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + material.getUnidademedida().getCdunidademedida() + "]';");
		}
		
		if(material.getValorvenda() != null){
			view.println("var valorvenda = '" + (material.getValorvenda() != null ? SinedUtil.descriptionDecimal(material.getValorvenda()) : "")+ "';");
		}else {
			view.println("var valorvenda = '';");
		}
		if(material.getPatrimonio() != null && material.getPatrimonio()){
			view.println("var materialPatrimonio = 'true';");
		} else {
			view.println("var materialPatrimonio = 'false';");
		}
		
		Codigoanp codigoanp = material.getCodigoanp();
		if(codigoanp != null){
			view.println("var codigoanp = 'br.com.linkcom.sined.geral.bean.Codigoanp[cdcodigoanp=" + codigoanp.getCdcodigoanp() + "]';");
			view.println("var descricaoCodigoanp = '" + codigoanp.getCodigo() + "';");
		} else {
			view.println("var codigoanp = '<null>';");
			view.println("var descricaoCodigoanp = '';");
		}
		
		List<Materialclasse> listaMaterialclasse = materialService.findClasses(material);
		if(listaMaterialclasse != null && listaMaterialclasse.size() == 1){
			view.println("var materialclasse = 'br.com.linkcom.sined.geral.bean.Materialclasse[cdmaterialclasse="+listaMaterialclasse.get(0).getCdmaterialclasse()+"]';");
		}else {
			view.println("var materialclasse = '';");
		}
		
		String ncmcompleto = material.getNcmcompleto() != null ? material.getNcmcompleto() : "";
		String extipi = material.getExtipi() != null ? material.getExtipi() : "";
		boolean servico = material.getServico() != null && material.getServico();
		
		view.println("var ncmcapitulo = '" + (material.getNcmcapitulo() != null && material.getNcmcapitulo().getCdncmcapitulo() != null ? ("br.com.linkcom.sined.geral.bean.Ncmcapitulo[cdncmcapitulo=" + material.getNcmcapitulo().getCdncmcapitulo() + "]") : "<null>") + "';");
		view.println("var extipi = '" + extipi + "';");
		view.println("var ncmcompleto = '" + ncmcompleto + "';");
		view.println("var nve = '" + (material.getNve() != null ? material.getNve() : "") + "';");
		view.println("var codigobarras = '" + (material.getCodigobarras() != null ? material.getCodigobarras() : "") + "';");
		view.println("var servico = " + (servico ? "true" : "false") + ";");
		view.println("var cest = '" + (material.getCest() != null ? material.getCest() : "") + "';");
		view.println("var obrigarlote = '" + (material.getObrigarlote() != null && material.getObrigarlote()) + "';");
		
		Double percentualimposto = notafiscalprodutoService.getPercentualImposto(empresaTributacaoOrigem, 
																					material.getPercentualimpostoecf(), 
																					origemproduto,
																					ncmcompleto,
																					extipi,
																					material.getCodigonbs(),
																					material.getCodlistaservico(),
																					!servico,
																					null);
		
		Double percentualImpostoFederal = notafiscalprodutoService.getPercentualImposto(empresaTributacaoOrigem, 
				material.getPercentualimpostoecf(), 
				origemproduto,
				ncmcompleto,
				extipi,
				material.getCodigonbs(),
				material.getCodlistaservico(),
				!servico,
				ImpostoEcfEnum.FEDERAL);
		
		Double percentualImpostoEstadual = notafiscalprodutoService.getPercentualImposto(empresaTributacaoOrigem, 
				material.getPercentualimpostoecf(), 
				origemproduto,
				ncmcompleto,
				extipi,
				material.getCodigonbs(),
				material.getCodlistaservico(),
				!servico,
				ImpostoEcfEnum.ESTADUAL);
		
		Double percentualImpostoMunicipal = notafiscalprodutoService.getPercentualImposto(empresaTributacaoOrigem, 
				material.getPercentualimpostoecf(), 
				origemproduto,
				ncmcompleto,
				extipi,
				material.getCodigonbs(),
				material.getCodlistaservico(),
				!servico,
				ImpostoEcfEnum.MUNICIPAL);
		
		view.println("var percentualimpostoecf = '" + (percentualimposto != null ? SinedUtil.descriptionDecimal(percentualimposto) : "") + "';");
		view.println("var percentualImpostoFederal = '" + (percentualImpostoFederal != null ? SinedUtil.descriptionDecimal(percentualImpostoFederal) : "") + "';");
		view.println("var percentualImpostoEstadual = '" + (percentualImpostoEstadual != null ? SinedUtil.descriptionDecimal(percentualImpostoEstadual) : "") + "';");
		view.println("var percentualImpostoMunicipal = '" + (percentualImpostoMunicipal != null ? SinedUtil.descriptionDecimal(percentualImpostoMunicipal) : "") + "';");
	}
	
	/**
	 * Ajax para buscar o percentual do imposto aproximado.
	 *
	 * @param request
	 * @param notafiscalprodutoitem
	 * @author Rodrigo Freitas
	 * @since 18/08/2015
	 */
	public void ajaxBuscaPercentualImposto(WebRequestContext request, Notafiscalprodutoitem notafiscalprodutoitem){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		if(notafiscalprodutoitem != null && 
				notafiscalprodutoitem.getMaterial() != null && 
				notafiscalprodutoitem.getTributadoicms() != null &&
				notafiscalprodutoitem.getEmpresa() != null){
			Material material = materialService.loadForNFProduto(notafiscalprodutoitem.getMaterial());
			
			Double percentualimposto = notafiscalprodutoService.getPercentualImposto(
					notafiscalprodutoitem.getEmpresa(), 
					material.getPercentualimpostoecf(), 
					notafiscalprodutoitem.getOrigemproduto(),
					notafiscalprodutoitem.getNcmcompleto(),
					notafiscalprodutoitem.getExtipi(),
					material.getCodigonbs(),
					material.getCodlistaservico(),
					notafiscalprodutoitem.getTributadoicms(),
					null);
			
			Double percentualImpostoFederal = notafiscalprodutoService.getPercentualImposto(
					notafiscalprodutoitem.getEmpresa(), 
					material.getPercentualimpostoecf(), 
					notafiscalprodutoitem.getOrigemproduto(),
					notafiscalprodutoitem.getNcmcompleto(),
					notafiscalprodutoitem.getExtipi(),
					material.getCodigonbs(),
					material.getCodlistaservico(),
					notafiscalprodutoitem.getTributadoicms(),
					ImpostoEcfEnum.FEDERAL);
			
			Double percentualImpostoEstadual = notafiscalprodutoService.getPercentualImposto(
					notafiscalprodutoitem.getEmpresa(), 
					material.getPercentualimpostoecf(), 
					notafiscalprodutoitem.getOrigemproduto(),
					notafiscalprodutoitem.getNcmcompleto(),
					notafiscalprodutoitem.getExtipi(),
					material.getCodigonbs(),
					material.getCodlistaservico(),
					notafiscalprodutoitem.getTributadoicms(),
					ImpostoEcfEnum.ESTADUAL);
			
			Double percentualImpostoMunicipal = notafiscalprodutoService.getPercentualImposto(
					notafiscalprodutoitem.getEmpresa(), 
					material.getPercentualimpostoecf(), 
					notafiscalprodutoitem.getOrigemproduto(),
					notafiscalprodutoitem.getNcmcompleto(),
					notafiscalprodutoitem.getExtipi(),
					material.getCodigonbs(),
					material.getCodlistaservico(),
					notafiscalprodutoitem.getTributadoicms(),
					ImpostoEcfEnum.MUNICIPAL);

			view.println("var percentualimpostoecf = '" + (percentualimposto != null ? SinedUtil.descriptionDecimal(percentualimposto) : "") + "';");
			view.println("var percentualImpostoFederal = '" + (percentualImpostoFederal != null ? SinedUtil.descriptionDecimal(percentualImpostoFederal) : "") + "';");
			view.println("var percentualImpostoEstadual = '" + (percentualImpostoEstadual != null ? SinedUtil.descriptionDecimal(percentualImpostoEstadual) : "") + "';");
			view.println("var percentualImpostoMunicipal = '" + (percentualImpostoMunicipal != null ? SinedUtil.descriptionDecimal(percentualImpostoMunicipal) : "") + "';");
		}
	}
	
	/**
	 * M�todo respos�vel por obter o c�digo cfop
	 * @param request
	 * @param cfop
	 * @author Taidson
	 * @since 19/10/2010
	 */
	public void ajaxOnChangeCfop(WebRequestContext request, Notafiscalprodutoitem item){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		
		
		if(item != null && item.getCfop() != null && item.getCfop().getCdcfop() != null){
			Cfop cfop = cfopService.loadForEntrada(item.getCfop());
			
			view.println("var sucesso = true;");
			view.println("var exportacao = " + (cfop != null && 
												cfop.getCfopescopo() != null && 
												cfop.getCfopescopo().equals(Cfopescopo.INTERNACIONAL)  &&
												cfop.getCfoptipo() != null &&
												cfop.getCfoptipo().equals(Cfoptipo.SAIDA) ? "true" : "false") + ";");
			view.println("var importacao = " + (cfop != null && 
												cfop.getCfopescopo() != null && 
												cfop.getCfopescopo().equals(Cfopescopo.INTERNACIONAL)  &&
												cfop.getCfoptipo() != null &&
												cfop.getCfoptipo().equals(Cfoptipo.ENTRADA) ? "true" : "false") + ";");
			view.println("var incluirfinal = " + (cfop != null && cfop.getIncluirvalorfinal() != null && cfop.getIncluirvalorfinal() ? "true" : "false") + ";");
		} else {
			view.println("var sucesso = false;");
		}
		
	}
	
	
	public void ajaxOnSelectCliente(WebRequestContext request, Notafiscalproduto notaProduto) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");

		if (notaProduto.getCliente() == null || notaProduto.getCliente().getCdpessoa() == null) {
			view.println("var sucesso = false;");
			return;
		}

		try {
			Cliente cliente = notaProduto.getCliente();
			cliente = clienteService.carregarDadosCliente(cliente);
			
			String razaoSocial = cliente.getRazaosocial() != null ? cliente.getRazaosocial() : "";
			String cnpj = cliente.getCpfOuCnpj() != null ? cliente.getCpfOuCnpj() : "";
			String inscricaoEstadual = cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : "";

			view.println("var sucesso = true;");
			view.println("var razaoSocial = \"" + razaoSocial + "\";");
			view.println("var inscricaoEstadual = \"" + inscricaoEstadual + "\";");
			view.println("var cnpjCpf = \"" + cnpj + "\";");
		} catch (Exception e) {
			view.println("var sucesso = false;");
			view.println("var msg = 'Ocorreu um erro ao recuperar os dados do cliente.';");
		}
	}
		
	public void ajaxOnSelectTransportador(WebRequestContext request, Notafiscalproduto notaProduto) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		if (notaProduto.getTransportador() == null || notaProduto.getTransportador().getCdpessoa() == null) {
			view.println("var sucesso = false;");
			return;
		}
		
		try {
			Fornecedor fornecedor = notaProduto.getTransportador();
			fornecedor = fornecedorService.carregaFornecedor(fornecedor);
			
			String razaoSocial = fornecedor.getRazaosocial() != null ? fornecedor.getRazaosocial() : "";
			String cnpj = fornecedor.getCpfOuCnpj() != null ? fornecedor.getCpfOuCnpj() : "";
			String inscricaoEstadual = fornecedor.getInscricaoestadual() != null ? fornecedor.getInscricaoestadual() : "";
			
			view.println("var sucesso = true;");
			view.println("var razaoSocial = \"" + razaoSocial + "\";");
			view.println("var inscricaoEstadual = \"" + inscricaoEstadual + "\";");
			view.println("var cnpjCpf = '" + cnpj + "';");
		} catch (Exception e) {
			e.printStackTrace();
			view.println("var sucesso = false;");
			view.println("var msg = 'Ocorreu um erro ao recuperar os dados do transportador.';");
		}
	}
	
	public ModelAndView parcelasCobranca(WebRequestContext request, ParcelasCobrancaBean bean){	
		return notafiscalprodutoService.parcelasCobranca(request, bean);
	}
	
	public ModelAndView popUpImposto(WebRequestContext request, PopUpImpostoFiltro filtro){
		return new ModelAndView("direct:/crud/popup/popUpImposto" + filtro.getTelaImposto(), "filtro", filtro);
	}
	
	public ModelAndView popUpIcms(WebRequestContext request, PopUpIcmsFiltro filtro){
		return new ModelAndView("direct:/crud/popup/popUpImpostoICMS", "filtro", filtro);
	}
	
	public void ajaxPopUpIcms(WebRequestContext request, PopUpIcmsFiltro filtro){	
		List<Tipocobrancaicms> lista = Tipocobrancaicms.getListaCobranca(filtro.getTipotributacaoicms().equals(Tipotributacaoicms.SIMPLES_NACIONAL));
		View.getCurrent().println("var lista = " + SinedUtil.convertEnumToJavaScript(lista) + ";");
	}
	
	public ModelAndView carregaDadosClienteAjax(WebRequestContext request, Cliente cliente){
		cliente = clienteService.carregarDadosCliente(cliente); 
		
		Integer enderecoSelecionado = null;
		int enderecosAtivos = 0;
		Integer enderecoAtivo = null;
		if (cliente.getListaEndereco().size() == 1){
			enderecoSelecionado = cliente.getListaEndereco().iterator().next().getCdendereco();
		}else if (cliente.getListaEndereco().size() > 1){
			for (Endereco endereco : cliente.getListaEndereco()){
				if (Enderecotipo.UNICO.equals(endereco.getEnderecotipo())){
					enderecoSelecionado = endereco.getCdendereco();
					break;
				} else if (Enderecotipo.FATURAMENTO.equals(endereco.getEnderecotipo())){
					enderecoSelecionado = endereco.getCdendereco();
					break;
				} else if (!Enderecotipo.INATIVO.equals(endereco.getEnderecotipo())){
					enderecoAtivo = endereco.getCdendereco();
					enderecosAtivos++;
				} 
			}
			
			if (enderecoSelecionado == null && enderecosAtivos == 1){
				enderecoSelecionado = enderecoAtivo;
			}
		}
		
		ModelAndView modelAndView = new JsonModelAndView().addObject("dadosCliente", cliente);
		
		if (enderecoSelecionado != null)
			modelAndView.addObject("enderecoSelecionado", enderecoSelecionado);
		else
			modelAndView.addObject("enderecoSelecionado", "<null>");
		
		return modelAndView;
	}	

	public ModelAndView carregaDadosTransportadorAjax(WebRequestContext request, Fornecedor fornecedor){
		fornecedor = fornecedorService.carregaFornecedor(fornecedor);
		Set<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class, telefoneService.carregarListaTelefone(fornecedor)); 
		if (listaTelefone != null && !listaTelefone.isEmpty())
			fornecedor.setListaTelefone(listaTelefone);
		return new JsonModelAndView().addObject("dadosTransportador", fornecedor);
	}
	
	/**
	 * M�todo que abre a popup para escolha dos n�meros de s�ries
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public ModelAndView abrirEscolhernumeroserie(WebRequestContext request){
		String cdmaterial = request.getParameter("cdmaterial");
		String cdnota = request.getParameter("cdnota");
		String qtdestr = request.getParameter("qtde");
		String index = request.getParameter("index");
		Integer qtde = 1;
		
		if(cdmaterial == null || "".equals(cdmaterial) || "null".equals(cdmaterial) || cdnota == null || "".equals(cdnota) || "null".equals(cdnota)){
			request.addError("N�o foi poss�vel obter o c�digo do material ou o c�digo da nota.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if(qtdestr != null && !"".equals(qtdestr)){
			try {
				qtde = Integer.parseInt(qtdestr);
			} catch (NumberFormatException e) {
				request.addError("S� � permitido escolher n�mero de s�rie para quantidade inteira.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		List<Materialnumeroserie> listaMaterialnumeroserie = materialnumeroserieService.loadWithNumeroserie(Integer.parseInt(cdmaterial));
		Integer qtdeJaRegistrada = materialnumeroserieService.getQtdeJaRegistrada(Integer.parseInt(cdmaterial), Integer.parseInt(cdnota));
		if(qtdeJaRegistrada != null){
			if(qtdeJaRegistrada >= qtde){
				request.addError("O material da nota selecionado j� tem o(s) n�mero(s) de s�rie(s) registrado(s).");
				SinedUtil.fechaPopUp(request);
				return null;
			}else {
				qtde = qtde-qtdeJaRegistrada;
			}
		}
		if(listaMaterialnumeroserie == null || listaMaterialnumeroserie.isEmpty()){
			request.addError("N�o existe n�mero de s�rie dispon�vel para o material selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Material material = new Material();
		material.setCdmaterial(Integer.parseInt(cdmaterial));
		material.setIndex(index);
		material.setQtdenfp(new Double(qtde));
		material.setListaMaterialnumeroserie(SinedUtil.listToSet(listaMaterialnumeroserie, Materialnumeroserie.class));
		return new ModelAndView("direct:/crud/popup/popUpNumeroserie", "bean", material);
	}
	
	/**
	 * M�todo que registra o(s) n�mero(s) de s�rie(s) escolhidos pelo usu�rio
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void salvaNumeroserie(WebRequestContext request){
		try {
			String cdnota = request.getParameter("cdnota");
			String cdmaterial = request.getParameter("cdmaterial");
			String whereIn = request.getParameter("whereIn");
			
			if(cdnota != null && !"".equals(cdnota) && cdmaterial != null && !"".equals(cdmaterial) && whereIn != null && !"".equals(whereIn)){
				materialnumeroserieService.salvaEscolhaNumeroserie(Integer.parseInt(cdnota), Integer.parseInt(cdmaterial), whereIn);
				View.getCurrent().println("var sucesso = true;");
			}else {
				View.getCurrent().println("var sucesso = false;");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	/**
	 * M�todo ajax que verifica o endere�o do emitente e destinat�rio para atualizar o cfop
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public void ajaxVerificarEnderecoForAtualizarCfop(WebRequestContext request, Notafiscalproduto notafiscalproduto){	
		Empresa empresa = null;
		Cfopescopo cfopescopo = null;
		Uf ufcliente;
		Uf ufempresa;
		Endereco enderecocliente = null;
		Endereco enderecoempresa = null;
		Cliente cliente = notafiscalproduto.getCliente();
		
		if(notafiscalproduto.getEnderecoavulso() != null && notafiscalproduto.getEnderecoavulso() && 
				notafiscalproduto.getEndereconota() != null && notafiscalproduto.getEndereconota().getMunicipio() != null){
			enderecocliente = notafiscalproduto.getEndereconota();
		}else if(notafiscalproduto.getEnderecoCliente() != null){
			enderecocliente = enderecoService.carregaEnderecoComUfPais(notafiscalproduto.getEnderecoCliente());
		}
		if(notafiscalproduto.getEmpresa() != null){
			empresa = notafiscalproduto.getEmpresa();
			enderecoempresa = enderecoService.carregaEnderecoEmpresa(empresa);
		}else {
			empresa = empresaService.loadPrincipalWithEndereco();
			if(empresa != null && empresa.getEndereco() != null){
				enderecoempresa = empresa.getEndereco();
			}
		}
		if(enderecoempresa != null && enderecoempresa.getCdendereco() != null){
			enderecoempresa = enderecoService.carregaEnderecoComUfPais(enderecoempresa);
		}
		
		if(enderecoempresa != null && enderecoempresa.getMunicipio() != null && enderecoempresa.getMunicipio().getUf() != null && enderecoempresa.getMunicipio().getUf().getCduf() != null &&
		   enderecocliente != null && enderecocliente.getMunicipio() != null && enderecocliente.getMunicipio().getUf() != null && enderecocliente.getMunicipio().getUf().getCduf() != null){
			cfopescopo = new Cfopescopo();
			ufcliente = enderecocliente.getMunicipio().getUf();
			ufempresa = enderecoempresa.getMunicipio().getUf();
			
			if(!ufcliente.getCduf().equals(ufempresa.getCduf())){
				enderecoempresa.setPais(Pais.BRASIL);
				if(enderecoempresa.getPais() != null && enderecoempresa.getPais().getCdpais() != null && 
				   enderecocliente.getPais() != null && enderecocliente.getPais().getCdpais() != null && 
				   enderecoempresa.getPais().equals(enderecoempresa.getPais().getCdpais())){
					cfopescopo.setCdcfopescopo(Cfopescopo.INTERNACIONAL.getCdcfopescopo());
				}else {
					cfopescopo.setCdcfopescopo(Cfopescopo.FORA_DO_ESTADO.getCdcfopescopo());
				}
			}else {
				cfopescopo.setCdcfopescopo(Cfopescopo.ESTADUAL.getCdcfopescopo());
			}
		}else if(enderecoempresa != null && enderecocliente != null){
			enderecoempresa.setPais(Pais.BRASIL);
			if(enderecocliente.getPais() != null && enderecoempresa.getPais() != null && 
					enderecocliente.getPais().getCdpais() != null && enderecoempresa.getPais().getCdpais() != null && 
					!enderecocliente.getPais().getCdpais().equals(enderecoempresa.getPais().getCdpais())){
				cfopescopo = new Cfopescopo();
				cfopescopo.setCdcfopescopo(Cfopescopo.INTERNACIONAL.getCdcfopescopo());
				
			}
		}
		
		if(cfopescopo != null){
			Object attribute = request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
			if (attribute != null) {
				boolean existAlteracao = false; 
				List<Notafiscalprodutoitem> lista = (List<Notafiscalprodutoitem>)attribute;
				Cfop cfop = null;
				Material material = null;
				int i = 0;
				request.getServletResponse().setContentType("text/html");
				if(lista != null && !lista.isEmpty()){
					for(Notafiscalprodutoitem item : lista){
						if(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null){
							material = materialService.loadForTributacao(item.getMaterial());
							material.getMaterialgrupo().setCfop(item.getCfop());
							cfop = cfopService.findForNFProduto(material, enderecoempresa, enderecocliente);
//							if (cfop==null){
//								cfop = item.getCfop();
//							}
							if(cfop != null && cfop.getCdcfop() != null){
								if(item.getCfop() != null && item.getCfop().getCdcfop() != null){
									if(!item.getCfop().getCdcfop().equals(cfop.getCdcfop())){
										item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, empresa, cliente, enderecoempresa, enderecocliente, null);
										item.setCfop(cfop);
										existAlteracao = true;
										View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.cfop\\\\.codigo_value').text('" + cfop.getCodigo() + "');");
									}
								}else {
									item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, empresa, cliente, enderecoempresa, enderecocliente, null);
									item.setCfop(cfop);
									existAlteracao = true;
									View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.cfop\\\\.codigo_value').text('" + cfop.getCodigo() + "');");
								}
							}
						}
						i++;
					}
					if(existAlteracao){
						request.getSession().setAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"), lista);
					}
				}
			}
		}		
	}
	
	/**
	 * M�todo ajax que busca informa��es do contribuinte da empresa
	 *
	 * @param request
	 * @param empresa
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public void ajaxBuscaInfcontribuinte(WebRequestContext request, Empresa empresa){
		if(empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			Endereco enderecoEmpresa = empresa.getEndereco();
			Naturezaoperacao naturezaoperacao = empresa.getNaturezaoperacao();
			
			String notaDtEmissaoString = request.getParameter("notaDtEmissao");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date notaDtEmissao = null;
			try {
				notaDtEmissao = new java.sql.Date(sdf.parse(notaDtEmissaoString).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			Cliente cliente = null;
			if(request.getParameter("cdcliente") != null && !"".equals(request.getParameter("cdcliente")) && !"<null>".equals(request.getParameter("cdcliente"))){
				cliente = clienteService.loadForTributacao(new Cliente(Integer.parseInt(request.getParameter("cdcliente"))));
			}
			
			Operacaonfe operacaonfe = null;
			if(request.getParameter("operacaonfe") != null && !"".equals(request.getParameter("operacaonfe")) && !"<null>".equals(request.getParameter("operacaonfe"))){
				operacaonfe = Operacaonfe.valueOf(request.getParameter("operacaonfe"));
			}
			
			Cfopescopo cfopescopo = null;
			Uf ufcliente;
			Uf ufempresa;
			Endereco enderecocliente = null;
			Endereco enderecoempresa = null;
			if(request.getParameter("cdendereco") != null && !"".equals(request.getParameter("cdendereco")) && !"<null>".equals(request.getParameter("cdendereco"))){
				enderecocliente = enderecoService.carregaEnderecoComUfPais(new Endereco(Integer.parseInt(request.getParameter("cdendereco"))));
			}		
			if(enderecoEmpresa != null && enderecoEmpresa.getCdendereco() != null){
				enderecoempresa = enderecoService.carregaEnderecoComUfPais(enderecoEmpresa);
			}
			
			if(enderecoempresa != null && enderecoempresa.getMunicipio() != null && enderecoempresa.getMunicipio().getUf() != null && enderecoempresa.getMunicipio().getUf().getCduf() != null &&
			   enderecocliente != null && enderecocliente.getMunicipio() != null && enderecocliente.getMunicipio().getUf() != null && enderecocliente.getMunicipio().getUf().getCduf() != null){
				cfopescopo = new Cfopescopo();
				ufcliente = enderecocliente.getMunicipio().getUf();
				ufempresa = enderecoempresa.getMunicipio().getUf();
				
				if(!ufcliente.getCduf().equals(ufempresa.getCduf())){
					enderecoempresa.setPais(Pais.BRASIL);
					if(enderecoempresa.getPais() != null && enderecoempresa.getPais().getCdpais() != null && 
					   enderecocliente.getPais() != null && enderecocliente.getPais().getCdpais() != null && 
					   enderecoempresa.getPais().equals(enderecoempresa.getPais().getCdpais())){
						cfopescopo.setCdcfopescopo(Cfopescopo.INTERNACIONAL.getCdcfopescopo());
					}else {
						cfopescopo.setCdcfopescopo(Cfopescopo.FORA_DO_ESTADO.getCdcfopescopo());
					}
				}else {
					cfopescopo.setCdcfopescopo(Cfopescopo.ESTADUAL.getCdcfopescopo());
				}
			}else if(enderecoempresa != null && enderecocliente != null){
				enderecoempresa.setPais(Pais.BRASIL);
				if(enderecocliente.getPais() != null && enderecoempresa.getPais() != null && 
						enderecocliente.getPais().getCdpais() != null && enderecoempresa.getPais().getCdpais() != null && 
						!enderecocliente.getPais().getCdpais().equals(enderecoempresa.getPais().getCdpais())){
					cfopescopo = new Cfopescopo();
					cfopescopo.setCdcfopescopo(Cfopescopo.INTERNACIONAL.getCdcfopescopo());
					
				}
			}
			
			List<Categoria> listaCategoriaCliente = null;
			if(cliente != null){
				listaCategoriaCliente = categoriaService.findByPessoa(cliente);
			}
			
			Object attribute = request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
			if (attribute != null) {
				List<Notafiscalprodutoitem> lista = (List<Notafiscalprodutoitem>)attribute;
				Cfop cfop = null;
				Material material = null;
				int i = 0;
				request.getServletResponse().setContentType("text/html");
				if(lista != null && !lista.isEmpty()){
					for(Notafiscalprodutoitem item : lista){
						if(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null){
							material = materialService.loadForTributacao(item.getMaterial());
							List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
									grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
															empresa, 
															naturezaoperacao, 
															material.getMaterialgrupo(),
															cliente,
															true, 
															material,
															item.getNcmcompleto(),
															enderecocliente, 
															null,
															listaCategoriaCliente,
															null,
															operacaonfe,
															getModelo(),
															null,
															notaDtEmissao));
							item.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
							material.getMaterialgrupo().setCfop(item.getCfop());
							cfop = cfopService.findForNFProduto(material, enderecoempresa, enderecocliente);
							item = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(item, empresa, cliente, enderecoempresa, enderecocliente, null);
							if(item.getCfop() == null)
								item.setCfop(cfop);
							if(item.getCfop() != null){
								View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.cfop\\\\.codigo_value').text('" + item.getCfop().getCodigo() + "');");
							}
						}
						i++;
					}
					request.getSession().setAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"), lista);
				}
			}
		
			empresa = empresaService.buscarInfContribuinte(empresa);
			String infcontribuinte = "";
			String ufveiculo = "";
			if(empresa != null && empresa.getTextoinfcontribuinte() != null){
				infcontribuinte = SinedUtil.escapeJavascript(empresa.getTextoinfcontribuinte());
			}			
			
			Presencacompradornfe presencacompradornfe = empresa.getPresencacompradornfe();
		
			if(enderecoEmpresa.getMunicipio() != null && enderecoEmpresa.getMunicipio().getUf() != null)
				ufveiculo = "br.com.linkcom.sined.geral.bean.Uf[cduf=" + enderecoEmpresa.getMunicipio().getUf().getCduf() + "]";
			
			String nomeMunicipioEmpresa = enderecoEmpresa.getMunicipio().getNomecompleto();
			String municipioEmpresa = Municipio.class.getName() + "[cdmunicipio=" + enderecoEmpresa.getMunicipio().getCdmunicipio() + ",nome=" + enderecoEmpresa.getMunicipio().getNome() + "]";
			
			Empresa empresa_aux = empresaService.load(empresa, "empresa.naopreencherdtsaida, empresa.marcanf, empresa.especienf");
			
			
			View.getCurrent()
				.println("var presencacompradornfe = '" + (presencacompradornfe != null ? presencacompradornfe.name() : "<null>") + "';")
				.println("var infcontribuinte = '" + infcontribuinte + "';")
				.println("var ufveiculo = '" + ufveiculo + "';")
				.println("var naopreencherdtsaida = '" + (empresa_aux.getNaopreencherdtsaida() != null && empresa_aux.getNaopreencherdtsaida()) + "';")
				.println("var especienf = '" + (empresa_aux.getEspecienf() != null ? empresa_aux.getEspecienf() : "") + "';")
				.println("var marcanf = '" + (empresa_aux.getMarcanf() != null ? empresa_aux.getMarcanf() : "") + "';")
				.println("var nomeMunicipioEmpresa = '" + nomeMunicipioEmpresa + "';")
				.println("var municipioEmpresa = '" + municipioEmpresa + "';")
				;
			
		}
	}
	
	public void ajaxBuscaInfEmpresa(WebRequestContext request, Empresa empresa){
		String cnae_value = "";
		String labelcnae = "";
		if(empresa.getCdpessoa() != null){
			empresa = empresaService.loadForInfNota(empresa);
			if(empresa != null && SinedUtil.isListNotEmpty(empresa.getListaEmpresacodigocnae())){
				Codigocnae codigocnae = null;
				for (Empresacodigocnae ec : empresa.getListaEmpresacodigocnae()) {
					if(codigocnae == null) codigocnae = ec.getCodigocnae();
					if(ec.getPrincipal() != null && ec.getPrincipal()){
						codigocnae = ec.getCodigocnae();
						break;
					}
				}
				if(codigocnae != null && codigocnae.getCdcodigocnae() != null){
					cnae_value = "br.com.linkcom.sined.geral.bean.Codigocnae[cdcodigocnae="+ codigocnae.getCdcodigocnae() + 
					",descricaocnae="+ (codigocnae.getDescricaoCombo() != null ? new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(codigocnae.getDescricaocnae()) : "") +"]";
					labelcnae = codigocnae.getDescricaoCombo();
				}
				
			}
		}
		View.getCurrent()
			.println(" var cnae_value = '" + cnae_value + "';")
			.println(" var labelcnae = '" + labelcnae + "';");
	}
	
	public void ajaxBuscaInfTransportador(WebRequestContext request, Empresa empresa){
		String transportador = "";
		String nometransportador = "";
		if(empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithTransportador(empresa);
			if(empresa != null && empresa.getTransportador() != null && empresa.getTransportador().getCdpessoa() != null){
				transportador = "br.com.linkcom.sined.geral.bean.Fornecedor[cdpessoa=" + empresa.getTransportador().getCdpessoa() + "]";
				nometransportador = empresa.getTransportador().getNome();
			}
		}
		View.getCurrent()
			.println(" var transportador = '" + transportador + "';")
			.println(" var nometransportador = '" + nometransportador + "';");
	}
	
	/**
	 * Realiza a��o de baixar estoque de nota fical de produto
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView baixarEstoque(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String entrada = request.getParameter("entrada");
//		String permitirestoquenegativo = parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(notaService.existOrigemForBaixaNota(whereIn)){
			request.addError("N�o � permitido baixar no estoque nota que possui origem de venda ou romaneio.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(movimentacaoestoqueService.existMovimentacaoestoqueByNotas(whereIn) || 
				movpatrimonioService.existMovpatrimonioByNotas(whereIn)){
			if(entrada != null && "true".equals(entrada)){
				request.addError("Nota j� baixada no estoque.");
			}else {
				request.addError("Existem notas que j� foram baixadas no estoque.");
			}
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Notafiscalproduto> listaNfp = notafiscalprodutoService.findForMovimentacaoestoque(whereIn);
		List<Movimentacaoestoque> listaMovimentacoes = new ArrayList<Movimentacaoestoque>();
		List<Movpatrimonio> listaMovpatrimonio = new ArrayList<Movpatrimonio>();
		
		StringBuilder mensagemErroContaGerencialCfop = new StringBuilder();
		if(listaNfp != null && !listaNfp.isEmpty()){
			String msgErroCG = "";
			List<Notafiscalprodutoitem> listaItensAgrupado;
			boolean loteObrigatorioNaoInformado = false;
			boolean achou = false;
			Notafiscalprodutoitem aux_notafiscalprodutoitem;
			for(Notafiscalproduto notafiscalproduto : listaNfp){
				if(Tipooperacaonota.SAIDA.equals(notafiscalproduto.getTipooperacaonota())){
//					if("FALSE".equals(permitirestoquenegativo)){
						if(notafiscalproduto.getListaItens() != null && !notafiscalproduto.getListaItens().isEmpty()){
							listaItensAgrupado = new ArrayList<Notafiscalprodutoitem>();
							for(Notafiscalprodutoitem item : notafiscalproduto.getListaItens()){
								if(SinedUtil.isRateioMovimentacaoEstoque() && (item.getCfop() == null || item.getCfop().getContaGerencial() == null)){
									msgErroCG = "� obrigat�rio informar a conta gerencial de estoque no cadastro de CFOP. " + (item.getCfop() != null ? "(" + item.getCfop().getCodigo() + ")" : "");
									if(mensagemErroContaGerencialCfop.indexOf(msgErroCG) == -1){
										mensagemErroContaGerencialCfop.append(msgErroCG).append("<br>");
									}
								}
								
								if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), notafiscalproduto.getEmpresa(), 
										item.getLocalarmazenagem(), notafiscalproduto.getProjeto())) {
									msgErroCG = "N�o � poss�vel realizar criar ou alterar a movimenta��o de estoque,  pois o per�odo est� vinculado � um registro de invent�rio. Para que consiga prosseguir com a a��o � necess�rio cancelar o invent�rio.";
									if(mensagemErroContaGerencialCfop.indexOf(msgErroCG) == -1){
										mensagemErroContaGerencialCfop.append(msgErroCG).append("<br>");
									}
								}
								
								loteObrigatorioNaoInformado = loteObrigatorioNaoInformado || !materialService.validateObrigarLote(item, request);
								
//								Material material = materialService.getMaterialmestreGradeOrItemGrade(item.getMaterial());
								Material material = item.getMaterial();
								if(material != null && material.getCdmaterial() != null && item.getQtde() != null){
									achou = false;
									if(!listaItensAgrupado.isEmpty()){
										for(Notafiscalprodutoitem itemagrupado : listaItensAgrupado){
//											Material materialagrupado = materialService.getMaterialmestreGradeOrItemGrade(itemagrupado.getMaterial());
											Material materialagrupado = itemagrupado.getMaterial();
											if(materialagrupado != null && material.getCdmaterial().equals(materialagrupado.getCdmaterial())){
												notafiscalprodutoService.verificaUnidademedidaQtde(item);												
												itemagrupado.setQtde(itemagrupado.getQtde()+item.getQtde());
												if(item.getValoripi() != null){
													itemagrupado.setValoripi(itemagrupado.getValoripi().add(item.getValoripi()));
												}
												if(item.getValorfrete() != null){
													itemagrupado.setValorfrete(itemagrupado.getValorfrete().add(item.getValorfrete()));
												}
												if(item.getValoricmsst() != null){
													itemagrupado.setValoricmsst(itemagrupado.getValoricmsst().add(item.getValoricmsst()));
												}
												if(item.getValorseguro() != null){
													itemagrupado.setValorseguro(itemagrupado.getValorseguro().add(item.getValorseguro()));
												}
												if(item.getOutrasdespesas() != null){
													itemagrupado.setOutrasdespesas(itemagrupado.getOutrasdespesas().add(item.getOutrasdespesas()));
												}
												if(item.getValordesconto() != null){
													itemagrupado.setValordesconto(itemagrupado.getValordesconto().add(item.getValordesconto()));
												}
												achou = true;
												break;
											}
										}
									}
									if(!achou){
										notafiscalprodutoService.verificaUnidademedidaQtde(item);	
										aux_notafiscalprodutoitem = new Notafiscalprodutoitem();
										aux_notafiscalprodutoitem.setQtde(item.getQtde());
										if(item.getLocalarmazenagem() != null){
											aux_notafiscalprodutoitem.setLocalarmazenagem(item.getLocalarmazenagem());
										}else if (notafiscalproduto.getEmpresa() != null) {
											List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(notafiscalproduto.getEmpresa());
											if (listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)
												aux_notafiscalprodutoitem.setLocalarmazenagem(listaLocalarmazenagem.get(0));				
										}
										aux_notafiscalprodutoitem.setMaterialclasse(item.getMaterialclasse());
										aux_notafiscalprodutoitem.setMaterial(material);
										aux_notafiscalprodutoitem.setValorunitario(item.getValorunitario());
										aux_notafiscalprodutoitem.setValoripi(item.getValoripi() != null ? item.getValoripi() : new Money());
										aux_notafiscalprodutoitem.setValorfrete(item.getValorfrete() != null ? item.getValorfrete() : new Money());
										aux_notafiscalprodutoitem.setValoricmsst(item.getValoricmsst() != null ? item.getValoricmsst() : new Money());
										aux_notafiscalprodutoitem.setValorseguro(item.getValorseguro() != null ? item.getValorseguro() : new Money());
										aux_notafiscalprodutoitem.setOutrasdespesas(item.getOutrasdespesas() != null ? item.getOutrasdespesas() : new Money());
										aux_notafiscalprodutoitem.setValordesconto(item.getValordesconto() != null ? item.getValordesconto() : new Money());
										aux_notafiscalprodutoitem.setCfop(item.getCfop());
										listaItensAgrupado.add(aux_notafiscalprodutoitem);
									}
								}
							}
						}
//					}
				}
				
				if(loteObrigatorioNaoInformado){
					SinedUtil.fechaPopUp(request);
					return null;
				}
				
				if(mensagemErroContaGerencialCfop.length() > 0){
					request.addError(mensagemErroContaGerencialCfop.toString());
					SinedUtil.fechaPopUp(request);
					return null;
				}
			}
			for(Notafiscalproduto notafiscalproduto : listaNfp){
				listaMovimentacoes.addAll(movimentacaoestoqueService.gerarMovimentacaoNota(notafiscalproduto, false));
				listaMovpatrimonio.addAll(movpatrimonioService.gerarMovimentacaoNota(notafiscalproduto));
			}
		}
		
		listaMovimentacoes = notafiscalprodutoService.getListaCorrigida(listaMovimentacoes);
		
		for (Movimentacaoestoque movimentacaoestoque : listaMovimentacoes) {
			movimentacaoestoque.setQtdeTotal(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(movimentacaoestoque.getMaterial(), movimentacaoestoque.getMaterialclasse(), movimentacaoestoque.getLocalarmazenagem(), movimentacaoestoque.getEmpresa(), null, null, movimentacaoestoque.getLoteestoque()));
//			if(movimentacaoestoque.getListaMovimentacaoEstoqueGradeItem() != null && 
//					!movimentacaoestoque.getListaMovimentacaoEstoqueGradeItem().isEmpty()){
//				for(Movimentacaoestoque itemGrade : movimentacaoestoque.getListaMovimentacaoEstoqueGradeItem()){
//					itemGrade.setQtdeTotal(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(itemGrade.getMaterial(), itemGrade.getMaterialclasse(), itemGrade.getLocalarmazenagem()));
//				}
//			}
		}

//		request.setAttribute("estoqueNegativo", permitirestoquenegativo);
		
		Aux_MovimentacaoEstoque bean = new Aux_MovimentacaoEstoque();
		bean.setListaMovimentacaoEstoque(listaMovimentacoes);
		bean.setListaMovpatrimonio(listaMovpatrimonio);
		
		if(listaMovimentacoes.size() == 0 && listaMovpatrimonio.size() == 0){
			request.addError("Nenhuma movimenta��o detectada.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
//		List<Movimentacaoestoque> listaEstoqueGrade = new ArrayList<Movimentacaoestoque>();
		List<Movimentacaoestoque> listaMovimentacoesCorrigida = new ArrayList<Movimentacaoestoque>();
		for(Movimentacaoestoque movimentacaoestoque : listaMovimentacoes){
//			if(movimentacaoestoque.getListaMovimentacaoEstoqueGradeItem() != null && 
//					!movimentacaoestoque.getListaMovimentacaoEstoqueGradeItem().isEmpty()){
//				listaEstoqueGrade.add(movimentacaoestoque);
//			}else {
				listaMovimentacoesCorrigida.add(movimentacaoestoque);
//			}
		}
		
		if(listaMovimentacoesCorrigida != null && !listaMovimentacoesCorrigida.isEmpty()){
			bean.setListaMovimentacaoEstoque(listaMovimentacoesCorrigida);
		}else {
			bean.setListaMovimentacaoEstoque(null);
		}
//		if(listaEstoqueGrade != null && !listaEstoqueGrade.isEmpty()){
//			bean.setListaMovimentacaoGradeEstoque(listaEstoqueGrade);
//		}
		
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		return new ModelAndView("direct:/crud/popup/baixarEstoque").addObject("bean", bean);	
	}
	
	public ModelAndView buscaQtdeTotalMaterialPorLocalAjax(WebRequestContext requestContext, Movimentacaoestoque movimentacaoestoque){
		JsonModelAndView json = new JsonModelAndView();
		if(movimentacaoestoque != null && 
				movimentacaoestoque.getMaterial() != null && 
				movimentacaoestoque.getLocalarmazenagem() != null && 
				movimentacaoestoque.getMaterialclasse() != null){
			Double qtdeTotal = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(movimentacaoestoque.getMaterial(), movimentacaoestoque.getMaterialclasse(), movimentacaoestoque.getLocalarmazenagem());
			json.addObject("qtdeTotal", qtdeTotal);
		}
		return json;
	}
	
	public void saveBaixarEstoque(WebRequestContext request, Aux_MovimentacaoEstoque bean){
		Map<Notafiscalproduto, List<Integer>> mapNotaMovimentacaoestoque = new HashMap<Notafiscalproduto, List<Integer>>();
		if(SinedUtil.isListNotEmpty(bean.getListaMovimentacaoEstoque())){
			boolean loteObrigatorioNaoInformado = false;
			for (Movimentacaoestoque me : bean.getListaMovimentacaoEstoque()) {
				if (me.getQtde() != null && me.getQtde() > 0) {
					loteObrigatorioNaoInformado = loteObrigatorioNaoInformado || !materialService.validateObrigarLote(me, request);
				}
			}
			if(loteObrigatorioNaoInformado){
				SinedUtil.fechaPopUp(request);
				return;
			}
			for (Movimentacaoestoque me : bean.getListaMovimentacaoEstoque()) {
				if (me.getQtde() != null && me.getQtde() > 0) {
					Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
					movimentacaoestoqueorigem.setNotafiscalproduto(me.getNotafiscalproduto());
					movimentacaoestoqueorigem.setNotaFiscalProdutoItem(me.getNotaFiscalProdutoItem());
					movimentacaoestoqueorigem.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					movimentacaoestoqueorigem.setDtaltera(new Timestamp(System.currentTimeMillis()));	
					
					me.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
					me.setRateio(rateioService.criarRateio(me));
					movimentacaoestoqueService.saveOrUpdate(me);
					movimentacaoestoqueService.salvarHisotirico(me, "Criado a partir da nota " 
							+ (me.getNotafiscalproduto() != null && me.getNotafiscalproduto().getCdNota() != null ? SinedUtil.makeLinkHistorico(me.getNotafiscalproduto().getCdNota().toString(), "visualizarNota") + ".": "."), MovimentacaoEstoqueAcao.CRIAR);
					
					
					List<Integer> ids = new ArrayList<Integer>();
					if(mapNotaMovimentacaoestoque.containsKey(me.getNotafiscalproduto())){
						ids = mapNotaMovimentacaoestoque.get(me.getNotafiscalproduto());
					} else {
						ids = new ArrayList<Integer>();
					}
					ids.add(me.getCdmovimentacaoestoque());
					mapNotaMovimentacaoestoque.put(me.getNotafiscalproduto(), ids);
				}
			}
		}
		
		if(bean.getListaMovimentacaoGradeEstoque() != null && !bean.getListaMovimentacaoGradeEstoque().isEmpty()){
			for (Movimentacaoestoque me : bean.getListaMovimentacaoGradeEstoque()) {
				if(me.getListaMovimentacaoEstoqueGradeItem() != null && !me.getListaMovimentacaoEstoqueGradeItem().isEmpty()){
					for(Movimentacaoestoque meItemGrade : me.getListaMovimentacaoEstoqueGradeItem()){
						if(meItemGrade.getQtde() != null && meItemGrade.getQtde() > 0){
							Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
							movimentacaoestoqueorigem.setNotafiscalproduto(me.getNotafiscalproduto());
							movimentacaoestoqueorigem.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
							movimentacaoestoqueorigem.setDtaltera(new Timestamp(System.currentTimeMillis()));	
							
							meItemGrade.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
							movimentacaoestoqueService.saveOrUpdate(meItemGrade);
							
							List<Integer> ids = new ArrayList<Integer>();
							if(mapNotaMovimentacaoestoque.containsKey(meItemGrade.getNotafiscalproduto())){
								ids = mapNotaMovimentacaoestoque.get(meItemGrade.getNotafiscalproduto());
							} else {
								ids = new ArrayList<Integer>();
							}
							ids.add(meItemGrade.getCdmovimentacaoestoque());
							mapNotaMovimentacaoestoque.put(meItemGrade.getNotafiscalproduto(), ids);
						}
					}
				}
			}
		}
		
		if(bean.getListaMovimentacaoEstoque() != null && !bean.getListaMovimentacaoEstoque().isEmpty()){
			for (Movimentacaoestoque me : bean.getListaMovimentacaoEstoque()) {
				notafiscalprodutoService.calcularCustoEntradaMaterial(me.getNotafiscalproduto());
			}
		}	
		
		
		Set<Entry<Notafiscalproduto, List<Integer>>> entrySetNotaMovimentacaoestoque = mapNotaMovimentacaoestoque.entrySet();
		for (Entry<Notafiscalproduto, List<Integer>> entry : entrySetNotaMovimentacaoestoque) {
			NotaHistorico notaHistorico = new NotaHistorico();
			notaHistorico.setNota(entry.getKey());
			notaHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			notaHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			notaHistorico.setNotaStatus(NotaStatus.BAIXA_ESTOQUE);
			
			List<String> listaLink = new ArrayList<String>();
			List<Integer> listId = entry.getValue();
			for (Integer id : listId) {
				listaLink.add(movimentacaoestoqueService.makeLinkHistoricoMovimentacaoestoque(new Movimentacaoestoque(id)));
			}
			
			notaHistorico.setObservacao("Entrada/Sa�da no estoque: " + CollectionsUtil.concatenate(listaLink, ", "));
			notaHistoricoService.saveOrUpdate(notaHistorico);
		}
		
		
		Map<Notafiscalproduto, List<Integer>> mapNotaMovpatrimonio = new HashMap<Notafiscalproduto, List<Integer>>();
		if(bean.getListaMovpatrimonio() != null && !bean.getListaMovpatrimonio().isEmpty()){
			for (Movpatrimonio movpatrimonio : bean.getListaMovpatrimonio()) {
				if(movpatrimonio.getPatrimonioitem() != null && 
						movpatrimonio.getLocalarmazenagem() != null){
					Movpatrimonioorigem movpatrimonioorigem = new Movpatrimonioorigem();
					movpatrimonioorigem.setNotafiscalproduto(movpatrimonio.getNotafiscalproduto());
					movpatrimonioorigemService.saveOrUpdate(movpatrimonioorigem);
					
					movpatrimonio.setMovpatrimonioorigem(movpatrimonioorigem);
					movpatrimonio.setDtmovimentacao(new Timestamp(System.currentTimeMillis()));
					movpatrimonioService.saveOrUpdate(movpatrimonio);
					
					List<Integer> ids = new ArrayList<Integer>();
					if(mapNotaMovpatrimonio.containsKey(movpatrimonio.getNotafiscalproduto())){
						ids = mapNotaMovpatrimonio.get(movpatrimonio.getNotafiscalproduto());
					} else {
						ids = new ArrayList<Integer>();
					}
					ids.add(movpatrimonio.getCdmovpatrimonio());
					mapNotaMovpatrimonio.put(movpatrimonio.getNotafiscalproduto(), ids);
				}
			}
		}
		
		Set<Entry<Notafiscalproduto, List<Integer>>> entrySetNotaMovpatrimonio = mapNotaMovpatrimonio.entrySet();
		for (Entry<Notafiscalproduto, List<Integer>> entry : entrySetNotaMovpatrimonio) {
			NotaHistorico notaHistorico = new NotaHistorico();
			notaHistorico.setNota(entry.getKey());
			notaHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			notaHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			notaHistorico.setNotaStatus(NotaStatus.BAIXA_ESTOQUE);
			
			List<String> listaLink = new ArrayList<String>();
			List<Integer> listaId = entry.getValue();
			for (Integer id : listaId) {
				listaLink.add(movpatrimonioService.makeLinkHistoricoMovpatrimonio(new Movpatrimonio(id)));
			}
			notaHistorico.setObservacao("Movimenta��es de patrim�nio: " + CollectionsUtil.concatenate(listaLink, ", "));
			
			notaHistoricoService.saveOrUpdate(notaHistorico);
		}
		
		request.getServletResponse().setContentType("text/html");
		request.addMessage("Baixa de estoque realizada com sucesso!");
		SinedUtil.fechaPopUp(request);
	}
	
	public ModelAndView criarForDevolucaoEntradaFiscal(WebRequestContext request) throws Exception{
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.isEmpty())
			throw new Exception("Par�metro inv�lido.");
		
		List<Entregadocumento> listaEd = entradafiscalService.findForDevolucaoEntradafiscal(whereIn);

		Notafiscalproduto nfp = new Notafiscalproduto();
		List<Notafiscalprodutoitem> lista = new ListSet<Notafiscalprodutoitem>(Notafiscalprodutoitem.class);
		
//		entradafiscalService.ajustaEntradaQtdeJaDevolvida(listaEd);
		
		Entregadocumento ed = listaEd.get(0);
		Empresa empresa = ed.getEmpresa();

		nfp.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		nfp.setEmpresa(empresa);
		nfp.setPresencacompradornfe(empresa != null ? empresa.getPresencacompradornfe() : null);
		nfp.setTipooperacaonota(Tipooperacaonota.SAIDA);
		Naturezaoperacao naturezaPadrao = naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_PRODUTO);
		if(naturezaPadrao != null){
			nfp.setNaturezaoperacao(naturezaPadrao);
			nfp.setOperacaonfe(naturezaPadrao.getOperacaonfe());
			if(naturezaPadrao.getFinalidadenfe() != null){
				nfp.setFinalidadenfe(naturezaPadrao.getFinalidadenfe());
			}
		}
		nfp.setResponsavelfrete(ed.getResponsavelfrete());
		nfp.setFormapagamentonfe(ed.getIndpag());
		nfp.setWhereInEntradaFiscal(whereIn);
		
		Cliente cliente = clienteService.load(new Cliente(ed.getFornecedor().getCdpessoa()));
		List<Endereco> listaEnderecoCliente = new ArrayList<Endereco>();
		List<Telefone> listaTelefoneCliente = new ArrayList<Telefone>();
		if(cliente != null && cliente.getCdpessoa() != null){
			cliente.setCpfcnpj(cliente.getCpfOuCnpj());
			nfp.setCliente(cliente);
//			listaEnderecoCliente = enderecoService.carregarListaEndereco(nfp.getCliente());
			listaEnderecoCliente = enderecoService.findByPessoaWithoutInativo(nfp.getCliente());
			listaTelefoneCliente = telefoneService.carregarListaTelefone(nfp.getCliente());
		}
		request.setAttribute("listaEnderecoCliente", listaEnderecoCliente);
		request.setAttribute("listaTelefoneCliente", listaTelefoneCliente);
	
		nfp.setValornaoincluido(new Money());
		nfp.setValorprodutos(new Money());
		nfp.setValorfrete(new Money());
		nfp.setValordesconto(new Money());
		nfp.setValorseguro(new Money());
		nfp.setOutrasdespesas(new Money());
		nfp.setValor(new Money());
		nfp.setCadastrarreferencia(Boolean.TRUE);
		
		List<Notafiscalprodutoreferenciada> listaNotafiscalprodutoreferenciada = new ArrayList<Notafiscalprodutoreferenciada>();
		ReportTemplateBean reportTemplateBeanInfoAdicionaisProduto = naturezaoperacaoService.getReportTemplateBeanInfoAdicionalProduto(nfp.getNaturezaoperacao());
		
		for(Entregadocumento entregadocumento : listaEd){
			nfp.setValornaoincluido(new Money(0d));
			nfp.setValorprodutos(nfp.getValorprodutos().add(entregadocumento.getValormercadoria() != null ? entregadocumento.getValormercadoria() : new Money(0d)));
			nfp.setValorfrete(nfp.getValorfrete().add(entregadocumento.getValorfrete() != null ? entregadocumento.getValorfrete() : new Money(0d)));
			nfp.setValordesconto(nfp.getValordesconto().add(entregadocumento.getValordesconto() != null ? entregadocumento.getValordesconto() : new Money(0d)));
			nfp.setValorseguro(nfp.getValorseguro().add(entregadocumento.getValorseguro() != null ? entregadocumento.getValorseguro() : new Money(0d)));
			nfp.setOutrasdespesas(nfp.getOutrasdespesas().add(entregadocumento.getValoroutrasdespesas() != null ? entregadocumento.getValoroutrasdespesas() : new Money(0d)));
			nfp.setPlacaveiculo(ed.getPlacaveiculo());
			
			Modelodocumentofiscal modelodocumentofiscal = entregadocumento.getModelodocumentofiscal();
			if(modelodocumentofiscal != null && modelodocumentofiscal.getCodigo() != null){
				Notafiscalprodutoreferenciada notafiscalprodutoreferenciada = null;
				if(modelodocumentofiscal.getCodigo().equals("55")){
					notafiscalprodutoreferenciada = new Notafiscalprodutoreferenciada();
					notafiscalprodutoreferenciada.setTiponotareferencia(Tiponotareferencia.NF_E);
					notafiscalprodutoreferenciada.setChaveacesso(entregadocumento.getChaveacesso());
				} else if(modelodocumentofiscal.getCodigo().equals("01")){
					notafiscalprodutoreferenciada = new Notafiscalprodutoreferenciada();
					notafiscalprodutoreferenciada.setTiponotareferencia(Tiponotareferencia.NF);
					notafiscalprodutoreferenciada.setNumero(entregadocumento.getNumero());
					notafiscalprodutoreferenciada.setSerie(entregadocumento.getSerie());
					notafiscalprodutoreferenciada.setModeloNFe(NotafiscalprodutoreferenciaModeloEnum.NF);
					notafiscalprodutoreferenciada.setCnpj(entregadocumento.getFornecedor() != null ? entregadocumento.getFornecedor().getCnpj() : null);
					notafiscalprodutoreferenciada.setMesanoemissao(SinedDateUtils.toString(entregadocumento.getDtemissao(), "MM/yyyy"));
				}
				
				if(notafiscalprodutoreferenciada != null){
					listaNotafiscalprodutoreferenciada.add(notafiscalprodutoreferenciada);
				}
			}
			
			if(entregadocumento.getListaEntregamaterial() != null){
				List<EntregadocumentoColeta> listaEntregadocumentoColeta = entregadocumentoColetaService.findForDevolucaoByEntradafiscal(entregadocumento);
				Map<ColetamaterialNotaBean, Double> mapaSaldoMaterial = notafiscalprodutoService.getMapaSaldoMaterial(listaEntregadocumentoColeta, Tipooperacaonota.SAIDA);
				ColetamaterialNotaBean coletamaterialNotaBean;
				for(Entregamaterial em : entregadocumento.getListaEntregamaterial()){
					ColetaMaterial coletaMaterial = entregadocumentoColetaService.getColetamaterial(listaEntregadocumentoColeta, em);
					
					boolean possuiColeta = entregadocumentoColetaService.possuiColetamaterial(listaEntregadocumentoColeta, em);
					
					if(em.getQtde() > 0 && (coletaMaterial != null || !possuiColeta)){
						ColetaMaterialPedidovendamaterial itemServico = null;
						
						Notafiscalprodutoitem nfpi = new Notafiscalprodutoitem();
						nfpi.setCdentregadocumento(entregadocumento.getCdentregadocumento());

						if(possuiColeta && SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())){
							itemServico = coletaMaterial.getListaColetaMaterialPedidovendamaterial().get(0);
							Integer cdpedidovendamaterial = itemServico.getPedidovendamaterial() != null ? itemServico.getPedidovendamaterial().getCdpedidovendamaterial() : null;
							coletamaterialNotaBean = new ColetamaterialNotaBean(coletaMaterial.getMaterial().getCdmaterial(), cdpedidovendamaterial);
							
							Double saldo = mapaSaldoMaterial.get(coletamaterialNotaBean);
							if (em.getQtde() >= saldo){
								nfpi.setQtde(saldo);
								mapaSaldoMaterial.put(coletamaterialNotaBean, 0D);
							} else {
								nfpi.setQtde(em.getQtde());
								mapaSaldoMaterial.put(coletamaterialNotaBean, saldo - em.getQtde());											
							}
						} else {
							nfpi.setQtde(em.getQtde());	
						}
						
						if(nfpi.getQtde() <= 0) continue;
						
						nfpi.setIncluirvalorprodutos(Boolean.TRUE);
						nfpi.setMaterial(em.getMaterial());
						nfpi.setUnidademedida(em.getUnidademedidacomercial() != null ? em.getUnidademedidacomercial() : em.getMaterial().getUnidademedida());
						nfpi.setNcmcapitulo(em.getMaterial().getNcmcapitulo());
						nfpi.setNcmcompleto(em.getMaterial().getNcmcompleto());
						nfpi.setCfop(em.getCfop());
						if (em.getLocalarmazenagem() == null) {
							Localarmazenagem local = localarmazenagemService.loadPrincipalByEmpresa(empresa);
							nfpi.setLocalarmazenagem(local);
						}else {
							nfpi.setLocalarmazenagem(em.getLocalarmazenagem());
						}
						
						
						if(possuiColeta && SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())){
							nfpi.setPedidovendamaterial(itemServico.getPedidovendamaterial());
							nfpi.setCdcoletamaterial(coletaMaterial.getCdcoletamaterial());
						}
						
						nfpi.setValorunitario(em.getValorunitario());
						nfpi.setValorbruto(new Money(em.getValorunitario() * em.getQtde()));
						nfpi.setValorfrete(em.getValorfrete());
						nfpi.setValordesconto(em.getValordesconto());
						nfpi.setValorseguro(em.getValorseguro());
						nfpi.setOutrasdespesas(em.getValoroutrasdespesas());
						nfpi.setMaterialclasse(em.getMaterialclasse());
						nfpi.setLoteestoque(em.getLoteestoque());
						nfpi.setTributadoicms(true);
						
						if(possuiColeta && SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())){
							nfpi.setPedidovendamaterial(itemServico.getPedidovendamaterial());
							nfpi.setCdcoletamaterial(coletaMaterial.getCdcoletamaterial());
						}
						
						nfpi.setOrigemproduto(Origemproduto.NACIONAL_0);
						
						nfpi.setTipocobrancaicms(em.getCsticms());
						nfpi.setValorbcicms(em.getValorbcicms());
						nfpi.setValoricms(em.getValoricms());
						nfpi.setIcms(em.getIcms());	
						
						nfpi.setValorbcicmsst(em.getValorbcicmsst());
						nfpi.setValoricmsst(em.getValoricmsst());
						nfpi.setIcmsst(em.getIcmsst());	
	
						nfpi.setTipocobrancaipi(em.getCstipi());
						nfpi.setValorbcipi(em.getValorbcipi());
						nfpi.setValoripi(em.getValoripi());
						nfpi.setIpi(em.getIpi());
						nfpi.setQtdevendidaipi(em.getQtde());
						
						if(em.getIpi() != null) {
							nfpi.setAliquotareaisipi(new Money(em.getIpi()));
						}
						
						nfpi.setTipocobrancapis(em.getCstpis());
						nfpi.setValorbcpis(em.getValorbcpis());
						nfpi.setValorpis(em.getValorpis());
						nfpi.setPis(em.getPis());
						nfpi.setQtdevendidapis(em.getQtde());
						
						if(em.getPis() != null) {
							nfpi.setAliquotareaispis(new Money(em.getPis()));
						}
						
						nfpi.setValorbcfcp(em.getValorbcfcp());
						nfpi.setFcp(em.getFcp());
						nfpi.setValorfcp(em.getValorfcp());
						
						nfpi.setValorbcfcpst(em.getValorbcfcpst());
						nfpi.setFcpst(em.getFcpst());
						nfpi.setValorfcpst(em.getValorfcpst());
						
						nfpi.setTipocobrancacofins(em.getCstcofins());
						nfpi.setValorbccofins(em.getValorbccofins());
						nfpi.setValorcofins(em.getValorcofins());
						nfpi.setCofins(em.getCofins());
						nfpi.setQtdevendidacofins(em.getQtde());
						
						if(em.getCofins() != null) {
							nfpi.setAliquotareaiscofins(new Money(em.getCofins()));
						}
						
						if(possuiColeta && SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
							nfpi.setInformacaoAdicionalProdutoBean(notafiscalprodutoService.criaBeanInfoProduto(em.getMaterial(), coletaMaterial != null ? coletaMaterial.getPneu() : null, coletaMaterial, null, null, itemServico.getPedidovendamaterial(), null));
							String templateInfoAdicionalItem = notafiscalprodutoService.montaInformacoesAdicionaisItemTemplate(nfpi.getInformacaoAdicionalProdutoBean(), reportTemplateBeanInfoAdicionaisProduto);
							
							if(templateInfoAdicionalItem != null){
								nfpi.setInfoadicional(templateInfoAdicionalItem);
								nfpi.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
							} else {
								nfpi.setInfoAdicionalItemAnterior("");
							}
						}
						
						lista.add(nfpi);
					}
				}
				nfp.setListaItens(lista);
			}
		}
		
		nfp.setListaReferenciada(listaNotafiscalprodutoreferenciada);
		
		nfp.setValor(new Money(nfp.getValornaoincluido())
			.add(nfp.getValorprodutos())
			.add(nfp.getValorfrete())
			.subtract(nfp.getValordesconto())
			.add(nfp.getValorseguro())
			.add(nfp.getOutrasdespesas()));
		
		request.setAttribute("criarForDevolucaoEntradaFiscal", true);
		
		return doEntrada(request, nfp);
	}
	
	public ModelAndView criarForExpedicaoproducao(WebRequestContext request) throws CrudException{
		String whereIn = SinedUtil.getItensSelecionados(request);
		Notafiscalproduto nfp = new Notafiscalproduto();
		nfp.setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum.NFE);
		
		try{
			if(whereIn == null || whereIn.trim().isEmpty())
				throw new Exception("Par�mentro inv�lido.");
			
			List<Expedicaoproducao> lista = expedicaoproducaoService.findForGerarNotaFiscalProduto(whereIn);
			if(lista != null && !lista.isEmpty() ){
				nfp.setTipooperacaonota(Tipooperacaonota.SAIDA);
				nfp.setWhereInExpedicaoproducao(whereIn);
				Money valorProdutos = new Money(0d);
				Cliente cliente = expedicaoproducaoService.getClienteUnico(lista);
				
				Map<Integer, Notafiscalprodutoitem> mapa = new HashMap<Integer, Notafiscalprodutoitem>();
				for(Expedicaoproducao ep : lista){
					if(ep.getListaExpedicaoproducaoitem() != null){
						for(Expedicaoproducaoitem epi : ep.getListaExpedicaoproducaoitem()){
							if(mapa.containsKey(epi.getMaterial().getCdmaterial())){
								Notafiscalprodutoitem nfpi = mapa.get(epi.getMaterial().getCdmaterial());
								nfpi.setQtde(nfpi.getQtde() + epi.getQtdeexpedicaoproducao());
								mapa.put(epi.getMaterial().getCdmaterial(), nfpi);
								
								if(epi.getMaterial().getValorvenda() != null){
									valorProdutos = valorProdutos.add(new Money(epi.getQtdeexpedicaoproducao() * epi.getMaterial().getValorvenda()));
								} 
								else if(epi.getMaterial().getValorcusto() != null){
									valorProdutos = valorProdutos.add(new Money(epi.getQtdeexpedicaoproducao() * epi.getMaterial().getValorcusto()));
								}
							}
							else{
								Notafiscalprodutoitem nfpi = new Notafiscalprodutoitem();
								Double valorunitario = epi.getMaterial().getValorvenda() != null ? epi.getMaterial().getValorvenda() : 
														(epi.getMaterial().getValorcusto() != null ? epi.getMaterial().getValorcusto() : 0d);
								
								nfpi.setMaterial(epi.getMaterial());
								nfpi.setUnidademedida(epi.getMaterial().getUnidademedida());
								nfpi.setLocalarmazenagem(epi.getLocalarmazenagem());
								nfpi.setQtde(epi.getQtdeexpedicaoproducao());
								nfpi.setValorunitario(valorunitario);
								nfpi.setValorbruto(new Money(valorunitario*epi.getQtdeexpedicaoproducao()));
								nfpi.setValorfrete(new Money(0d));
								nfpi.setValorseguro(new Money(0d));
								nfpi.setValordesconto(new Money(0d));
								nfpi.setOutrasdespesas(new Money(0d));
								nfpi.setMaterialclasse(Materialclasse.PRODUTO);
								nfpi.setOrigemproduto(Origemproduto.NACIONAL_0);
								nfpi.setIncluirvalorprodutos(Boolean.TRUE);
								mapa.put(epi.getMaterial().getCdmaterial(), nfpi);
								valorProdutos = valorProdutos.add(new Money(valorunitario * epi.getQtdeexpedicaoproducao()));
							}
						}
						Collection<Notafiscalprodutoitem> c = mapa.values();
						if(c != null && !c.isEmpty()){
							List<Notafiscalprodutoitem> listaNfpi = new ListSet<Notafiscalprodutoitem>(Notafiscalprodutoitem.class);
							listaNfpi.addAll(mapa.values());
							nfp.setListaItens(listaNfpi);
						}
						else{
							nfp.setListaItens(new ListSet<Notafiscalprodutoitem>(Notafiscalprodutoitem.class));
						}
						
					}
				}
				
				cliente = clienteService.load(cliente);
				List<Endereco> listaEnderecoCliente = new ArrayList<Endereco>();
				List<Telefone> listaTelefoneCliente = new ArrayList<Telefone>();
				if(cliente != null && cliente.getCdpessoa() != null){
					cliente.setCpfcnpj(cliente.getCpfOuCnpj());
					nfp.setCliente(cliente);
					listaEnderecoCliente = enderecoService.findByPessoaWithoutInativo(nfp.getCliente());
					listaTelefoneCliente = telefoneService.carregarListaTelefone(nfp.getCliente());
				}
				request.setAttribute("listaEnderecoCliente", listaEnderecoCliente);
				request.setAttribute("listaTelefoneCliente", listaTelefoneCliente);
		
				nfp.setValornaoincluido(new Money(0d));
				nfp.setValorfrete(new Money(0d));
				nfp.setValordesconto(new Money(0d));
				nfp.setValorseguro(new Money(0d));
				nfp.setOutrasdespesas(new Money(0d));
				nfp.setValorprodutos(valorProdutos);
				nfp.setValor(valorProdutos);
				nfp.setLocaldestinonfe(null);
			}
			else{
				throw new Exception("Lista de expedi��es vazia.");
			}
		}catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			View.getCurrent().println("<script>parent.location = parent.location;parent.$.akModalRemove(true);</script>");
			return null;
		}
		
		return doEntrada(request, nfp);
	}
	
	/**
	 * M�todo que busca os lotes com qtde dispon�vel para o material
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView getLoteestoque(WebRequestContext request, Material material){
		request.setAttribute("naoConsiderarQtde", true);
		List<Loteestoque> listaLoteVinculoMaterial = loteestoqueService.findLoteByMaterial(material);
		return loteestoqueService.getLoteestoque(request, material, listaLoteVinculoMaterial);
	}
	
	@SuppressWarnings("unchecked")
	public void ajaxBuscaInfcontribuinteNaturezaoperacao(WebRequestContext request, Notafiscalproduto notafiscalproduto){
		
		String infcontrib = "";
		String inffisco = "";
		Operacaonfe operacaonfe = null;
		String infoContribuinteTemplate = "";
		Finalidadenfe finalidadenfe = null;
		boolean simplesremessa = false;
		
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
		if (listaNotafiscalprodutoitem!=null){
			notafiscalproduto.setListaItens(listaNotafiscalprodutoitem);
		}
		
		notafiscalproduto.setPossuiTemplateInfoContribuinte(Boolean.FALSE);
		if (notafiscalproduto.getNaturezaoperacao()!=null && notafiscalproduto.getNaturezaoperacao().getCdnaturezaoperacao()!=null){
			Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(notafiscalproduto.getNaturezaoperacao());
			infcontrib = Util.strings.emptyIfNull(naturezaoperacao.getInfoadicionalcontrib());
			inffisco = Util.strings.emptyIfNull(naturezaoperacao.getInfoadicionalfisco());
			operacaonfe = naturezaoperacao.getOperacaonfe();
			finalidadenfe = naturezaoperacao.getFinalidadenfe();
			simplesremessa = naturezaoperacao.getSimplesremessa() != null && naturezaoperacao.getSimplesremessa();
			
			if(naturezaoperacao.getTemplateinfcontribuinte()!=null && notafiscalproduto.getCdNota()==null &&
				(StringUtils.isNotBlank(notafiscalproduto.getWhereInColeta()) || notafiscalproduto.getCdvendaassociada()!=null)){
				List<Coleta> coletas = new ArrayList<Coleta>();
				List<Venda> vendas = new ArrayList<Venda>();
				if(StringUtils.isNotBlank(notafiscalproduto.getWhereInColeta())){
					coletas = coletaService.loadForGerarNotafiscalproduto(notafiscalproduto.getWhereInColeta());
				}
				if(notafiscalproduto.getCdvendaassociada()!=null){
					vendas.add(new Venda(notafiscalproduto.getCdvendaassociada()));
				}
				
				notafiscalprodutoService.loadInfoForTemplateInfoContribuinte(notafiscalproduto);
				infoContribuinteTemplate = notafiscalprodutoService.montaInformacoesContribuinteTemplate(vendas, coletas, notafiscalproduto);
				notafiscalproduto.setPossuiTemplateInfoContribuinte(Boolean.TRUE);				
			}
		}
		infoContribuinteTemplate = SinedUtil.escapeJavascript(infoContribuinteTemplate);
		infcontrib = SinedUtil.escapeJavascript(infcontrib);
		inffisco = SinedUtil.escapeJavascript(inffisco);

		View.getCurrent()
			.println("var infcontrib = '" + infcontrib + "';")
			.println("var inffisco = '" + inffisco + "';")
			.println("var operacaonfe = '" + (operacaonfe != null ? operacaonfe.name() : "<null>") + "';")
			.println("var finalidadenfe = '" + (finalidadenfe != null ? finalidadenfe.name() : "<null>") + "';")
			.println("var infcontribuintetemplate = '" + infoContribuinteTemplate + "';")
			.println("var possuiTemplateInfoContribuinte = '" + (Boolean.TRUE.equals(notafiscalproduto.getPossuiTemplateInfoContribuinte()) ? "true" : "false")+ "';")
			.println("var simplesremessa = " + simplesremessa + ";")
			;
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView trocaEmpresaOuClienteOuNaturezaoperacao(WebRequestContext request, Notafiscalproduto notafiscalproduto){
		Naturezaoperacao naturezaoperacao = notafiscalproduto.getNaturezaoperacao();
		//Ao trocar a natureza de opera��o, caso a natureza opera��o anterior utilize template de informa��es do contribuinte, as informa��es do template devem ser limpas,
		//para que n�o fiquem informa��es do template referente � natureza antiga
		String cdNaturezaOperacaoAnterior = request.getParameter("cdNaturezaOperacaoAnterior");
		if(notafiscalproduto.getCdNota()==null && StringUtils.isNotEmpty(cdNaturezaOperacaoAnterior) && 
			((naturezaoperacao==null)	||(naturezaoperacao!=null &&
			naturezaoperacao.getCdnaturezaoperacao()!=null && !cdNaturezaOperacaoAnterior.equals(naturezaoperacao.getCdnaturezaoperacao().toString())))){
			Naturezaoperacao naturezaoperacaoAnterior = naturezaoperacaoService.load(new Naturezaoperacao(Integer.parseInt(cdNaturezaOperacaoAnterior)),
																					"naturezaoperacao.templateinfcontribuinte");
			if(naturezaoperacaoAnterior !=null && naturezaoperacaoAnterior.getTemplateinfcontribuinte()!=null){
				notafiscalproduto.setInfoadicionalcontrib("");
			}
		}
		Empresa empresa = notafiscalproduto.getEmpresa();
		Endereco enderecoempresa = null;
		Endereco enderecocliente = notafiscalproduto.getEnderecoCliente();
		Endereco endereconota = notafiscalproduto.getEndereconota();
		Boolean enderecoavulso = notafiscalproduto.getEnderecoavulso();
		Cliente cliente = notafiscalproduto.getCliente();
		Operacaonfe operacaonfe = notafiscalproduto.getOperacaonfe();
		
		Uf ufEmpresa = null;
		Uf ufCliente = null;
		Pais paisEmpresa = null;
		Pais paisCliente = null;
		Cfop cfop = null; 
		boolean mudouEmpresa = "empresa".equals(request.getParameter("origemMudanca"));
		boolean mudouNaturezaoperacao = "naturezaoperacao".equals(request.getParameter("origemMudanca"));
		boolean localVendaObrigatorio = parametrogeralService.getBoolean(Parametrogeral.LOCAL_VENDA_OBRIGATORIO);
		Localarmazenagem localarmazenagemprincipal = null; 
		if(mudouEmpresa){
			if(localVendaObrigatorio){
				localarmazenagemprincipal = localarmazenagemService.loadPrincipalByEmpresa(empresa);
			}
		}
		if (empresa!=null){
			enderecoempresa = enderecoService.carregaEnderecoEmpresa(empresa);
		}else {
			empresa = empresaService.loadPrincipalWithEndereco();
			if (empresa!=null && empresa.getEndereco()!=null){
				enderecoempresa = empresa.getEndereco();
			}
		}
		
		if (enderecoempresa!=null && enderecoempresa.getCdendereco()!=null){
			enderecoempresa = enderecoService.carregaEnderecoComUfPais(enderecoempresa);
			if (enderecoempresa.getMunicipio()!=null){
				ufEmpresa = enderecoempresa.getMunicipio().getUf();
			}
			paisEmpresa = enderecoempresa.getPais();
		}
		
		Municipio municipioEntrega = null;
		if(Util.objects.isPersistent(notafiscalproduto.getMunicipioentrega())){
			municipioEntrega = municipioService.carregaMunicipio(notafiscalproduto.getMunicipioentrega());
			if(municipioEntrega != null){
				ufCliente = municipioEntrega.getUf();
			}
			paisCliente = notafiscalproduto.getPaisEntrega();
		}else if(enderecoavulso != null && enderecoavulso && endereconota != null){
			if(endereconota.getMunicipio() != null){
				ufCliente = endereconota.getMunicipio().getUf();
			}
			paisCliente = endereconota.getPais();
		}else {
			if (enderecocliente!=null){
				enderecocliente = enderecoService.carregaEnderecoComUfPais(enderecocliente);
				if (enderecocliente.getMunicipio()!=null){
					ufCliente = enderecocliente.getMunicipio().getUf();
				}
				paisCliente = enderecocliente.getPais();
			}
		}
		if(cliente != null && cliente.getCdpessoa() != null){
			cliente = clienteService.loadForTributacao(cliente);
		}
		
		List<Categoria> listaCategoriaCliente = null;
		if(cliente != null){
			listaCategoriaCliente = categoriaService.findByPessoa(cliente);
		}
		
		Cfopescopo cfopescopo = null;
		if (ufEmpresa!=null && ufCliente!=null){
			if (ufEmpresa.getCduf().equals(ufCliente.getCduf())){
				cfopescopo = Cfopescopo.ESTADUAL;
			}else if (!ufEmpresa.getCduf().equals(ufCliente.getCduf())){
				cfopescopo = Cfopescopo.FORA_DO_ESTADO;
			}
		}else if (paisService.isInternacional(paisEmpresa, paisCliente)){
			cfopescopo = Cfopescopo.INTERNACIONAL;
		}
		
		Cfop cfopNat = null;
		if (naturezaoperacao!=null && naturezaoperacao.getCdnaturezaoperacao()!=null){
			List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(naturezaoperacao, cfopescopo);
			if (!listaNaturezaoperacaocfop.isEmpty()){
				cfopNat = listaNaturezaoperacaocfop.get(0).getCfop();
			}			
		}
		
		Naturezaoperacao natOperacao = null;
		if(naturezaoperacao != null){
			natOperacao = naturezaoperacaoService.load(naturezaoperacao, 
					"naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.simplesremessa, naturezaoperacao.razaosocial");
			
			
			if(mudouNaturezaoperacao){
				View.getCurrent().print("form['razaosocialalternativa'].value = \"" + escapeSingleQuotes(Util.strings.emptyIfNull(natOperacao.getRazaosocial()), true) + "\";");
			}
		}
		
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
		if (listaNotafiscalprodutoitem!=null){
			int i = 0;
			for (Notafiscalprodutoitem notafiscalprodutoitem: listaNotafiscalprodutoitem){
				cfop = cfopNat;
				Material material = materialService.loadForTributacao(notafiscalprodutoitem.getMaterial());
				if(notafiscalprodutoitem.getMaterial() != null){
					List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
							grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.SAIDA, 
													empresa, 
													naturezaoperacao, 
													material.getMaterialgrupo(),
													cliente,
													true, 
													material, 
													notafiscalprodutoitem.getNcmcompleto(),
													enderecocliente, 
													null,
													listaCategoriaCliente,
													null,
													operacaonfe,
													getModelo(),
													notafiscalproduto.getLocaldestinonfe(),
													notafiscalproduto.getDtEmissao()));
					notafiscalprodutoitem.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
				}
				
				if(municipioEntrega != null){
					Endereco enderecoEntrega = new Endereco();
					enderecoEntrega.setEnderecoTransient(Boolean.TRUE);
					enderecoEntrega.setMunicipio(municipioEntrega);
					enderecoEntrega.setPais(paisCliente);
					notafiscalprodutoitem = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(notafiscalprodutoitem, empresa, cliente, enderecoempresa, enderecoEntrega, cfop);
				}else if(enderecoavulso != null && enderecoavulso && endereconota != null){
					notafiscalprodutoitem = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(notafiscalprodutoitem, empresa, cliente, enderecoempresa, endereconota, cfop);
				}else {
					notafiscalprodutoitem = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(notafiscalprodutoitem, empresa, cliente, enderecoempresa, enderecocliente, cfop);
				}
				String cfop_text = "";
				if(notafiscalprodutoitem.getCfop() != null && notafiscalprodutoitem.getCfop().getCodigo() != null){
					material.getMaterialgrupo().setCfop(notafiscalprodutoitem.getCfop());
					cfop = cfopService.findForNFProduto(material, enderecoempresa, enderecocliente);
					if(cfop != null)
						cfop_text = notafiscalprodutoitem.getCfop().getCodigo();
				}else if (cfop != null){
					notafiscalprodutoitem.setCfop(cfop);
					cfop_text = cfop.getCodigo();
				}
				
				notafiscalprodutoService.setBaseCalculoItem(notafiscalprodutoitem.getMaterial(), notafiscalprodutoitem);
				notafiscalprodutoService.verificaBaseCalculoTipoCobran�a(notafiscalprodutoitem);
				notafiscalprodutoitem.setOperacaonfe(operacaonfe);
				grupotributacaoService.calcularValoresFormulaIpi(notafiscalprodutoitem, notafiscalprodutoitem.getGrupotributacao() != null ? notafiscalprodutoitem.getGrupotributacao() : notafiscalprodutoitem.getMaterial().getGrupotributacaoNota());
				grupotributacaoService.calcularValoresFormulaIcms(notafiscalprodutoitem, notafiscalprodutoitem.getGrupotributacao() != null ? notafiscalprodutoitem.getGrupotributacao() : notafiscalprodutoitem.getMaterial().getGrupotributacaoNota());
				notafiscalprodutoService.calculaMva(notafiscalprodutoitem, notafiscalprodutoitem.getMaterial());
				grupotributacaoService.calcularValoresFormulaIcmsST(notafiscalprodutoitem, notafiscalprodutoitem.getGrupotributacao() != null ? notafiscalprodutoitem.getGrupotributacao() : notafiscalprodutoitem.getMaterial().getGrupotributacaoNota());
				notafiscalprodutoitem.getMaterial().setPercentualimpostoecf(material.getPercentualimpostoecf());
				notafiscalprodutoitem.setPercentualimpostoecf(notafiscalprodutoService.getPercentualImposto(empresa, notafiscalprodutoitem, null));
				notafiscalprodutoitem.setValortotaltributos(notafiscalprodutoService.getValorAproximadoImposto(empresa, natOperacao, notafiscalprodutoitem, null));
				
				notafiscalprodutoitem.setPercentualImpostoFederal(notafiscalprodutoService.getPercentualImposto(empresa, notafiscalprodutoitem, ImpostoEcfEnum.FEDERAL));
				notafiscalprodutoitem.setPercentualImpostoEstadual(notafiscalprodutoService.getPercentualImposto(empresa, notafiscalprodutoitem, ImpostoEcfEnum.ESTADUAL));
				notafiscalprodutoitem.setPercentualImpostoMunicipal(notafiscalprodutoService.getPercentualImposto(empresa, notafiscalprodutoitem, ImpostoEcfEnum.MUNICIPAL));
				
				notafiscalprodutoitem.setValorImpostoFederal(notafiscalprodutoService.getValorAproximadoImposto(empresa, natOperacao, notafiscalprodutoitem, ImpostoEcfEnum.FEDERAL));
				notafiscalprodutoitem.setValorImpostoEstadual(notafiscalprodutoService.getValorAproximadoImposto(empresa, natOperacao, notafiscalprodutoitem, ImpostoEcfEnum.ESTADUAL));
				notafiscalprodutoitem.setValorImpostoMunicipal(notafiscalprodutoService.getValorAproximadoImposto(empresa, natOperacao, notafiscalprodutoitem, ImpostoEcfEnum.MUNICIPAL));
				
				if(mudouEmpresa){
					if(localVendaObrigatorio && localarmazenagemprincipal != null){
						notafiscalprodutoitem.setLocalarmazenagem(localarmazenagemprincipal);
					}else{
						notafiscalprodutoitem.setLocalarmazenagem(null);
					}
				}
				
				String valoricms_value = notafiscalprodutoitem.getValoricms() != null ? notafiscalprodutoitem.getValoricms().toString() : "";
				String valoriss_value = notafiscalprodutoitem.getValoriss() != null ? notafiscalprodutoitem.getValoriss().toString() : "";
				String valoripi_value = notafiscalprodutoitem.getValoripi() != null ? notafiscalprodutoitem.getValoripi().toString() : "";
				String valorpis_value = notafiscalprodutoitem.getValorpis() != null ? notafiscalprodutoitem.getValorpis().toString() : "";
				String valorcofins_value = notafiscalprodutoitem.getValorcofins() != null ? notafiscalprodutoitem.getValorcofins().toString() : "";
				
				View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.valoricms_value').text('" + escapeSingleQuotes(valoricms_value) + "');");
				View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.valoriss_value').text('" + escapeSingleQuotes(valoriss_value) + "');");
				View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.valoripi_value').text('" + escapeSingleQuotes(valoripi_value) + "');");
				View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.valorpis_value').text('" + escapeSingleQuotes(valorpis_value) + "');");
				View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.valorcofins_value').text('" + escapeSingleQuotes(valorcofins_value) + "');");
				
				View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.cfop\\\\.codigo_value').text('" + escapeSingleQuotes(cfop_text) + "');");
				i++;
			}
		}
		
		notafiscalproduto.setListaItens(listaNotafiscalprodutoitem);
		notafiscalprodutoService.adicionarInfNota(notafiscalproduto, (notafiscalproduto.getCdvendaassociada() != null ? new Venda(notafiscalproduto.getCdvendaassociada()) : null), empresa);
		
		request.getSession().setAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"), listaNotafiscalprodutoitem);
		
		if(Boolean.TRUE.equals(notafiscalproduto.getPossuiTemplateInfoContribuinte())){
			notafiscalprodutoService.loadInfoForTemplateInfoContribuinte(notafiscalproduto);
			String infoadicionalcontrib = notafiscalprodutoService.montaInformacoesContribuinteTemplate(
							notafiscalproduto.getCdvendaassociada() != null ? notafiscalproduto.getCdvendaassociada().toString() : null, 
							notafiscalproduto.getWhereInColeta(), 
							notafiscalproduto);
			if(infoadicionalcontrib != null){
				notafiscalproduto.setInfoadicionalcontrib(infoadicionalcontrib);
			}
		}
		
		View.getCurrent().print("form['infoadicionalcontrib'].value = \"" +  escapeSingleQuotes(Util.strings.emptyIfNull(notafiscalproduto.getInfoadicionalcontrib()), true) + "\";");
		
		View.getCurrent().print("form['infoadicionalfisco'].value = \"" + escapeSingleQuotes(Util.strings.emptyIfNull(notafiscalproduto.getInfoadicionalfisco()), true) + "\";");
		
		return null;
	}
	
	public void  ajaxLoadRazaosocialalternativaByNaturezaoperacao(WebRequestContext request, Naturezaoperacao naturezaoperacao){
		String razaosocial = "";
		if(naturezaoperacao != null && naturezaoperacao.getCdnaturezaoperacao() != null){
			Naturezaoperacao natOperacao = null;
			if(naturezaoperacao != null){
				natOperacao = naturezaoperacaoService.load(naturezaoperacao, "naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.razaosocial");
				if(natOperacao != null && Util.strings.isNotEmpty(natOperacao.getRazaosocial())){
					razaosocial = natOperacao.getRazaosocial();
				}
			}
		}
		View.getCurrent().print("form['razaosocialalternativa'].value = \"" + escapeSingleQuotes(razaosocial, true) + "\";");
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView trocarNaturezaoperacao(WebRequestContext request, Notafiscalproduto notafiscalproduto){
		Naturezaoperacao naturezaoperacao = notafiscalproduto.getNaturezaoperacao();
		Empresa empresa = notafiscalproduto.getEmpresa();
		Endereco enderecoempresa = null;
		Endereco enderecocliente = notafiscalproduto.getEnderecoCliente();
		Endereco endereconota = notafiscalproduto.getEndereconota();
		Boolean enderecoavulso = notafiscalproduto.getEnderecoavulso();
		Cliente cliente = notafiscalproduto.getCliente();
		
		Uf ufEmpresa = null;
		Uf ufCliente = null;
		Cfop cfop = null; 
		
		if (empresa!=null){
			enderecoempresa = enderecoService.carregaEnderecoEmpresa(empresa);
		}else {
			empresa = empresaService.loadPrincipalWithEndereco();
			if (empresa!=null && empresa.getEndereco()!=null){
				enderecoempresa = empresa.getEndereco();
			}
		}
		
		if (enderecoempresa!=null && enderecoempresa.getCdendereco()!=null){
			enderecoempresa = enderecoService.carregaEnderecoComUfPais(enderecoempresa);
			if (enderecoempresa.getMunicipio()!=null){
				ufEmpresa = enderecoempresa.getMunicipio().getUf();
			}
		}
		
		if(enderecoavulso != null && enderecoavulso && endereconota != null){
			if(endereconota.getMunicipio() != null){
				ufCliente = endereconota.getMunicipio().getUf();
			}
		}else {
			if (enderecocliente!=null){
				enderecocliente = enderecoService.carregaEnderecoComUfPais(enderecocliente);
				if (enderecocliente.getMunicipio()!=null){
					ufCliente = enderecocliente.getMunicipio().getUf();
				}
			}
		}
		
		Cfopescopo cfopescopo = null;
		if (ufEmpresa!=null && ufCliente!=null){
			if (ufEmpresa.getCduf().equals(ufCliente.getCduf())){
				cfopescopo = Cfopescopo.ESTADUAL;
			}else {
				cfopescopo = Cfopescopo.FORA_DO_ESTADO;
			}
		}
		
		if (naturezaoperacao!=null && naturezaoperacao.getCdnaturezaoperacao()!=null){
			List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(naturezaoperacao, cfopescopo);
			if (!listaNaturezaoperacaocfop.isEmpty()){
				cfop = listaNaturezaoperacaocfop.get(0).getCfop();
			}			
		}
		
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
		if (listaNotafiscalprodutoitem!=null){
			int i = 0;
			for (Notafiscalprodutoitem notafiscalprodutoitem: listaNotafiscalprodutoitem){
				if(enderecoavulso != null && enderecoavulso && endereconota != null){
					notafiscalprodutoitem = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(notafiscalprodutoitem, empresa, cliente, enderecoempresa, endereconota, cfop);
				}else {
					notafiscalprodutoitem = notafiscalprodutoitemService.loadTributacaoMaterialByEmpresa(notafiscalprodutoitem, empresa, cliente, enderecoempresa, enderecocliente, cfop);
				}
				String cfop_text = "";
				if (cfop!=null){
					notafiscalprodutoitem.setCfop(cfop);
					cfop_text = cfop.getCodigo();
				}
				View.getCurrent().println("$('#listaItens\\\\[" + i + "\\\\]\\\\.cfop\\\\.codigo_value').text('" + escapeSingleQuotes(cfop_text) + "');");
				i++;
			}
		}
		
//		notafiscalproduto.setInfoadicionalcontrib(null);
//		notafiscalproduto.setInfoadicionalfisco(null);
		notafiscalproduto.setListaItens(listaNotafiscalprodutoitem);
		notafiscalprodutoService.adicionarInfNota(notafiscalproduto, null, empresa);
		
		request.getSession().setAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"), listaNotafiscalprodutoitem);
		
		View.getCurrent().print("form['infoadicionalcontrib'].value = '" + escapeSingleQuotes(Util.strings.emptyIfNull(notafiscalproduto.getInfoadicionalcontrib())) + "';");
		View.getCurrent().print("form['infoadicionalfisco'].value = '" + escapeSingleQuotes(Util.strings.emptyIfNull(notafiscalproduto.getInfoadicionalfisco())) + "';");
		
		return null;
	}
	
	public void ajaxInformacoesContribuinteFiscoMaterial(WebRequestContext request, Notafiscalproduto notafiscalproduto){
		String contribuinte = "";
		String fisco = "";
		
		if (notafiscalproduto.getGrupotributacao() != null){		
			Grupotributacao grupotributacao = grupotributacaoService.loadWithInfAdicional(notafiscalproduto.getGrupotributacao());
			if (grupotributacao != null){
				contribuinte = escapeSingleQuotes(Util.strings.emptyIfNull(grupotributacao.getInfoadicionalcontrib()));
				fisco = escapeSingleQuotes(Util.strings.emptyIfNull(grupotributacao.getInfoadicionalfisco()));
			}
		}
		
		View.getCurrent().print("var contribuinte = '" + contribuinte + "';");
		View.getCurrent().print("var fisco = '" + fisco + "';");
	}
	
	private String escapeSingleQuotes(String string) {
		return escapeSingleQuotes(string, false);
	}
	
	private String escapeSingleQuotes(String string, boolean addCaracter) {
		String s = string.replaceAll("\\\\", "\\\\\\\\").replaceAll("\'", "\\\\'");
		if(addCaracter){
			return s.replaceAll("\r\n", "quebraLinhaInfoAdicionalRN").replaceAll("\n", "quebraLinhaInfoAdicionalN").replaceAll("\"", "aspasDuplas");
		}else {
			return s.replaceAll("\n", " ");
		}
	}
	
	public void ajaxSincronizarClienteVendaContareceber(WebRequestContext request, Nota nota){
		try{
				if(nota.getCliente() != null && nota.getEnderecoCliente() != null && nota.getWhereIn() != null && !nota.getWhereIn().equals("")){
					String whereInVenda = nota.getWhereIn();
					Cliente cliente = nota.getCliente();
					Endereco endereco = nota.getEnderecoCliente();
					
					vendaService.updateClienteEndereco(whereInVenda, cliente, endereco);
					
					String[] ids = whereInVenda.split(",");
					for (String cdvenda : ids) {
						List<Documento> listaDocumento = documentoService.findByVenda(new Venda(Integer.parseInt(cdvenda)));
						for (Documento documento : listaDocumento) {
							documentoService.updatePessoaEndereco(documento, cliente, endereco);
						}
					}
					
					View.getCurrent().println("alert('Venda e Conta a Receber sincronizada com sucesso.');");
				} else {
					View.getCurrent().println("alert('Venda e Conta a Receber n�o sincronizada.');");
				}
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("alert('Venda e Conta a Receber n�o sincronizada. Erro: " + e.getMessage() + "');");
		}
	}
	
	public ModelAndView sincronizarClienteVendaContareceberListagem(WebRequestContext request){
		try{
			String whereIn = SinedUtil.getItensSelecionados(request);
			String[] ids = whereIn.split(",");
			List<Nota> notas = new ArrayList<Nota>();
			for(String id : ids){
				notas.add(notaService.load(new Nota(Integer.parseInt(id))));
			}
			for(Nota nota : notas){
				if(nota.getCliente() != null && nota.getEnderecoCliente() != null && nota.getWhereIn() != null && !nota.getWhereIn().equals("")){
					String whereInVenda = nota.getWhereIn();
					Cliente cliente = nota.getCliente();
					Endereco endereco = nota.getEnderecoCliente();
					
					vendaService.updateClienteEndereco(whereInVenda, cliente, endereco);
					
					for (String cdvenda : ids) {
						List<Documento> listaDocumento = documentoService.findByVenda(new Venda(Integer.parseInt(cdvenda)));
						for (Documento documento : listaDocumento) {
							documentoService.updatePessoaEndereco(documento, cliente, endereco);
						}
					}
					
					request.addMessage("Venda e Conta a Receber sincronizada com sucesso.");
				} else {
					request.addError("Venda e Conta a Receber da nota " + nota.getNumero() + " n�o sincronizada.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("alert('Venda e Conta a Receber n�o sincronizada. Erro: " + e.getMessage() + "');");
		}
		return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=listagem");
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public ModelAndView abrirAtualizarDados(WebRequestContext request){
    	String idnota = SinedUtil.getItensSelecionados(request);
    	Notafiscalproduto nfproduto = null;
    	if(idnota != null){
    		nfproduto = new Notafiscalproduto(Integer.parseInt(idnota));
    		if(!exibirLocal(nfproduto)){
    			request.addError("� permitido atualizar dados somente de nota fiscal avulsa ou com origem de contrato.");
				SinedUtil.fechaPopUp(request);
				return null;   			
    		}
    		nfproduto.setNotaStatus(notaService.getNotaStatus(nfproduto));
    		nfproduto.setListaItens(notafiscalprodutoitemService.findByNotafiscalproduto(nfproduto));
	    	if(!nfproduto.getNotaStatus().equals(NotaStatus.NFE_LIQUIDADA) && !nfproduto.getNotaStatus().equals(NotaStatus.NFE_EMITIDA)){	    		
		    		request.addError("A nota precisa ter as situa��es: 'Nota Fiscal Emitida' ou 'Nota Fiscal Liquidada' para a atualiza��o.");
					SinedUtil.fechaPopUp(request);
					return null;
		    }
	    	request.setAttribute("titulo", "ATUALIZAR DADOS");
	    	request.setAttribute("acaoSalvar", "atualizarDados");
	    	request.setAttribute("clearBase", true);
	    	request.setAttribute("idNota", idnota);
    	}else{
    		request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		return new ModelAndView("direct:/crud/popup/atualizaDadosNFProduto", "nfproduto", nfproduto);
	}
	
	/**
	 * 
	 * @param request
	 * @param notafiscalproduto
	 * @return
	 */
	public ModelAndView atualizarDados(WebRequestContext request, Notafiscalproduto notafiscalproduto){		
		String idnota = (String) request.getParameter("idNota");		
		if(idnota!=null){
			notafiscalprodutoitemService.atualizaDados(notafiscalproduto.getCdNota(),notafiscalproduto.getListaItens());
			notaHistoricoService.adicionaHistoricoAtualizar(idnota);
			request.addMessage("Registros salvos com sucesso.");
		}else
			request.addError("Erro ao atualizar os dados.");
		
    	request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>parent.location = parent.location;parent.$.akModalRemove(true);</script>");
		return null;
	}
	
	/**
	 * M�todo ajax para obter a diferen�a de dias da data de emiss�o e o vencimento
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 24/10/2013
	 */
	public ModelAndView ajaxDiferencaDiasData(WebRequestContext request){
		String dtEmissaoStr = request.getParameter("dtEmissao");
		String dtVencimentoStr = request.getParameter("dtVencimento");
		
		Date dtEmissao = null;
		Date dtVencimento = null;
		Integer diasEntre = null;
		try {
			dtEmissao = SinedDateUtils.stringToDate(dtEmissaoStr);
			dtVencimento = SinedDateUtils.stringToDate(dtVencimentoStr);
			
			diasEntre = SinedDateUtils.diferencaDias(dtVencimento, dtEmissao);
			
		} catch (Exception e) {}
		
		return new JsonModelAndView().addObject("diasEntre", diasEntre != null ? diasEntre : "");
	}
	
	/**
	 * Action que abre a popup para o envio de e-mail de nota
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/07/2014
	 */
	public ModelAndView popupEmailNota(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(notaService.haveNFDiferenteStatus(whereIn, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA)){
			request.addError("Para envio da NF-e as notas tem que estar com a situa��o 'NF-e EMITIDA' ou 'NF-e LIQUIDADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		return notaService.preparePopupEnvioEmail(request, whereIn, NotaTipo.NOTA_FISCAL_PRODUTO, isNfe());
	}
	
	/**
	 * M�todo que vai realizar o envio de e-mail da nota vinda da popup.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 03/07/2014
	 */
	public void savePopupEmailNota(WebRequestContext request, EnvioEmailNotaBean bean){
		notaService.savePopupEnvioEmailNota(request, bean, NotaTipo.NOTA_FISCAL_PRODUTO, isNfe());
	}


	/**
	* M�todo ajax para verificar o CST dos itens com o da natureza de opera��o
	*
	* @param request
	* @param bean
	* @return
	* @since 29/07/2014
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public ModelAndView ajaxVerificaDiferencaLancamento(WebRequestContext request, Notafiscalproduto bean){
		List<Material> listaWithLancamentoDiferente = new ArrayList<Material>();
		boolean existDiferencaLancamento = false;
		try {
			Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaItensNotaFiscalProduto" + bean.getIdentificadortela());
			if (attribute != null && bean.getNaturezaoperacao() != null && bean.getNaturezaoperacao().getCdnaturezaoperacao() != null) {
				bean.setListaItens((List<Notafiscalprodutoitem>)attribute);
				if (SinedUtil.isListNotEmpty(bean.getListaItens())) {
					Naturezaoperacao naturezaoperacao = naturezaoperacaoService.loadForValidaLancamentoImpostoNota(bean.getNaturezaoperacao());
					if(naturezaoperacao != null && SinedUtil.isListNotEmpty(naturezaoperacao.getListaNaturezaoperacaocst())){
						StringBuilder cdnfePis = new StringBuilder();
						StringBuilder cdnfeIcms = new StringBuilder();
						StringBuilder cdnfeCofins = new StringBuilder();
						StringBuilder cdnfeIpi = new StringBuilder();
						boolean existeCstpis = false;
						boolean cstpisDiferente = true;
						boolean existeCsticms = false;
						boolean csticmsDiferente = true;
						boolean existeCstcofins = false;
						boolean cstcofinsDiferente = true;
						boolean existeCstipi = false;
						boolean cstipiDiferente = true;
						
						List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = bean.getListaItens();
						for (Notafiscalprodutoitem item: listaNotafiscalprodutoitem){
							StringBuilder descricaoInconsistenciaLancamento = new StringBuilder();
							for(Naturezaoperacaocst naturezaoperacaocst : naturezaoperacao.getListaNaturezaoperacaocst()){
								if(naturezaoperacaocst.getTipocobrancapis() != null){
									existeCstpis = true;
									if(item.getTipocobrancapis() != null &&
										(naturezaoperacaocst.getTipocobrancapis().equals(item.getTipocobrancapis()))){
										cstpisDiferente = false;
									}else {
										if(!cdnfePis.toString().equals("")) cdnfePis.append(",");
										cdnfePis.append(naturezaoperacaocst.getTipocobrancapis().getCdnfe());
									}
								}
								if(naturezaoperacaocst.getTipocobrancaicms() != null){
									existeCsticms = true;
									if(item.getTipocobrancaicms() != null &&
										(naturezaoperacaocst.getTipocobrancaicms().equals(item.getTipocobrancaicms()))){
										csticmsDiferente = false;
									}else {
										if(!cdnfeIcms.toString().equals("")) cdnfeIcms.append(",");
										cdnfeIcms.append(naturezaoperacaocst.getTipocobrancaicms().getCdnfe());
									}
								}
								if(naturezaoperacaocst.getTipocobrancacofins() != null){
									existeCstcofins = true;
									if(item.getTipocobrancacofins() != null &&
											(naturezaoperacaocst.getTipocobrancacofins().equals(item.getTipocobrancacofins()))){
										cstcofinsDiferente = false;
									}else {
										if(!cdnfeCofins.toString().equals("")) cdnfeCofins.append(",");
										cdnfeCofins.append(naturezaoperacaocst.getTipocobrancacofins().getCdnfe());
									}
								}
								if(naturezaoperacaocst.getTipocobrancaipi() != null){
									existeCstipi = true;
									if(item.getTipocobrancaipi() != null &&
										(naturezaoperacaocst.getTipocobrancaipi().equals(item.getTipocobrancaipi()))){
										cstipiDiferente = false;
									}else {
										if(!cdnfeIpi.toString().equals("")) cdnfeIpi.append(",");
										cdnfeIpi.append(naturezaoperacaocst.getTipocobrancaipi().getCdnfe());
									}
								}
							}
							
							if(existeCstpis && cstpisDiferente){
								descricaoInconsistenciaLancamento.append(
										(item.getTipocobrancapis() != null ? 
												"PIS com tipo " + item.getTipocobrancapis().getCdnfe() :
												"PIS (n�o informado) "
										) +
										" difere do tipo " + cdnfePis.toString() + 
										" conforme natureza da opera��o.<br>");
								existDiferencaLancamento = true;
							}
							if(existeCsticms && csticmsDiferente){
								descricaoInconsistenciaLancamento.append(
										(item.getTipocobrancaicms() != null ? 
												"ICMS com tipo " + item.getTipocobrancaicms().getCdnfe() :
												"ICMS (n�o informado) "
										) +
										" difere do tipo " + cdnfeIcms.toString() + 
										" conforme natureza da opera��o.<br>");
								existDiferencaLancamento = true;
							}
							if(existeCstcofins && cstcofinsDiferente){
								descricaoInconsistenciaLancamento.append(
										(item.getTipocobrancacofins() != null ? 
												"COFINS com tipo " + item.getTipocobrancacofins().getCdnfe() :
												"COFINS (n�o informado) "
										) +
										" difere do tipo " + cdnfeCofins.toString() + 
										" conforme natureza da opera��o.<br>");
								existDiferencaLancamento = true;
							}
							if(existeCstipi && cstipiDiferente){
								descricaoInconsistenciaLancamento.append(
										(item.getTipocobrancaipi() != null ? 
												"IPI com tipo " + item.getTipocobrancaipi().getCdnfe() :
												"IPI (n�o informado) "
										) +
										" difere do tipo " + cdnfeIpi.toString() + 
										" conforme natureza da opera��o.<br>");
								existDiferencaLancamento = true;
							}
							
							if(StringUtils.isNotEmpty(descricaoInconsistenciaLancamento.toString())){
								Material material = new Material();
								material.setNome(item.getMaterial().getNome());
								material.setDescricaoInconsistenciaLancamento(descricaoInconsistenciaLancamento.toString());
								listaWithLancamentoDiferente.add(material);
							}
						}
					}
				}
			}
		} catch (Exception e) {}
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("itens", listaWithLancamentoDiferente);
		json.addObject("existDiferencaLancamento", existDiferencaLancamento);
		return json;
	}
	
	public ModelAndView registrarAjuste(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado");
			return new ModelAndView("/faturamento/crud/Notafiscalproduto");
		}
		
		if(whereIn.split(",").length > 1){
			request.addError("� permitido selecionar apenas um item por vez para esta a��o.");
			return new ModelAndView("/faturamento/crud/Notafiscalproduto");
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		Notafiscalproduto notafiscalproduto = notafiscalprodutoService.carregarDocumentoParaAjusteFiscal(new Notafiscalproduto(Integer.parseInt(whereIn)));
		
		Ajustefiscal ajustefiscal = new Ajustefiscal();
		ajustefiscal.setNotafiscalproduto(notafiscalproduto);
		List<Material> listaMaterial = new ArrayList<Material>();
		if(SinedUtil.isListNotEmpty(notafiscalproduto.getListaItens())){
			for(Notafiscalprodutoitem notafiscalprodutoitem : notafiscalproduto.getListaItens()){
				if(notafiscalprodutoitem.getMaterial() != null && !listaMaterial.contains(notafiscalprodutoitem.getMaterial())){
					listaMaterial.add(notafiscalprodutoitem.getMaterial());
				}
			}
		}
		request.getSession().setAttribute("listaMaterialregistrarAjusteNotaFiscal", listaMaterial);
		request.setAttribute("entrada", entrada);
		return new ModelAndView("direct:/crud/popup/registrarajuste", "bean", ajustefiscal);
	}
	
	public void saveRegistrarAjuste(WebRequestContext request, Ajustefiscal ajustefiscal){
		request.getSession().removeAttribute("listaMaterialregistrarAjusteNotaFiscal");
		
		if(ajustefiscal.getNotafiscalproduto() == null){
			request.addError("Nenhum item selecionado");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto");
		}
		ajustefiscalService.saveOrUpdate(ajustefiscal);
		
		request.addMessage("Ajuste registrado com sucesso.");
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		SinedUtil.redirecionamento(request, "/faturamento/crud/Notafiscalproduto" + (entrada ? "?ACAO=consultar&cdNota="+
				ajustefiscal.getNotafiscalproduto().getCdNota():""));
	}
	
	/**
	* M�todo ajax para carregar a conta na nota
	*
	* @param request
	* @param notafiscalproduto
	* @throws Exception
	* @since 04/03/2015
	* @author Luiz Fernando
	*/
	public void carregaContaByEmpresa(WebRequestContext request, Notafiscalproduto nota) throws Exception{
		notafiscalprodutoService.carregaContaByEmpresa(request, nota);
	}
	
	public void buscarInfPrazopagamento(WebRequestContext request, Notafiscalproduto nota) throws Exception{
		notafiscalprodutoService.buscarInfPrazopagamento(request, nota.getPrazopagamentofatura());
	}

	public ModelAndView gerarReceitaEmLote(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		List<Notafiscalproduto> listaNotafiscalproduto = notafiscalprodutoService.loadForReceita(whereIn);
		if(listaNotafiscalproduto == null || listaNotafiscalproduto.isEmpty()){
			request.addError("Nenhum item encontrado.");
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
		}
		
		for(Notafiscalproduto notafiscalproduto : listaNotafiscalproduto){
			boolean erro = false;
			if(!notafiscalproduto.getNotaStatus().equals(NotaStatus.EMITIDA) && 
					!notafiscalproduto.getNotaStatus().equals(NotaStatus.NFE_EMITIDA)){
				erro = true;
				request.addError("� permitido gerar receita somente de nota(s) na situa��o de Emitida ou NF-e Emitida.");
			}
			if(!erro && notafiscalproduto.getNaturezaoperacao() != null && Boolean.TRUE.equals(notafiscalproduto.getNaturezaoperacao().getSimplesremessa())){
				erro = true;
				request.addError("N�o � permitido gerar receita de uma nota que tenha a natureza de opera��o marcada para n�o gerar receita.");
			}
			if(!erro && (notafiscalproduto.getListaDuplicata() == null || notafiscalproduto.getListaDuplicata().isEmpty())){
				erro = true;
				request.addError("� permitido gerar receita apenas de nota(s) com cobran�a.");
			}
			if(!erro && SinedUtil.isListEmpty(notafiscalproduto.getListaNotaContrato()) && SinedUtil.isListEmpty(notafiscalproduto.getListaNotavenda())){
				erro = true;
				request.addError("N�o � permitido gerar receita em lote de nota(s) avulsa(s). " + (StringUtils.isNotBlank(notafiscalproduto.getNumero()) ? ("Nota(s):" + notafiscalproduto.getNumero()) : "" ));
			}
			if(!erro && notaDocumentoService.haveDocumentoNaoCanceladoByNota(notafiscalproduto)){
				erro = true;
				request.addError("Receita j� gerada para esta nota.");
			}
			if(erro){
				return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto" + (entrada ? "?ACAO=consultar&cdNota=" + listaNotafiscalproduto.get(0).getCdNota() : ""));
			}
		}
		
		return notafiscalprodutoService.gerarReceitaEmLote(request, listaNotafiscalproduto);
	}
	
	/**
	 * Retorna uma a��o em ajax a inscri��o estadual
	 *
	 * @param request
	 * @param notaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/07/2015
	 */
	public ModelAndView carregaInscricaoEstadualAjax(WebRequestContext request, Nota nota){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		String inscricaoestadual = notaService.getInscricaoEstadualClienteEndereco(nota);
		return jsonModelAndView.addObject("inscricaoestadual", inscricaoestadual != null ? inscricaoestadual : "");
	}
	
	/**
	 * Verifica se o usu�rio logado pode acessar a nota fiscal.
	 * @param request
	 * @param form
	 */
	private boolean verificaRestricaoAcesso(Notafiscalproduto form){
		if(form.getListaRestricao()!=null && !form.getListaRestricao().isEmpty() && !SinedUtil.isUsuarioLogadoAdministrador()){
			boolean hasAccess = false;			
			Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
			for(NotaFiscalProdutoRestricao restricao : form.getListaRestricao()){
				if(usuarioLogado.getCdpessoa().equals(restricao.getUsuario().getCdpessoa())){
					hasAccess = true;
					break;
				}
			}
			return hasAccess;
		}else{
			return true;
		}
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Notafiscalproduto form) {
		if(isNfe() && form.getModeloDocumentoFiscalEnum() != null && form.getModeloDocumentoFiscalEnum().equals(ModeloDocumentoFiscalEnum.NFCE)){
			request.addError("Nota com modelo errado para a tela que est� sendo acessada.");
			return sendRedirectToAction("listagem");
		}
		if(!isNfe() && form.getModeloDocumentoFiscalEnum() != null && form.getModeloDocumentoFiscalEnum().equals(ModeloDocumentoFiscalEnum.NFE)){
			request.addError("Nota com modelo errado para a tela que est� sendo acessada.");
			return sendRedirectToAction("listagem");
		}
		if(!verificaRestricaoAcesso(form)){
			request.addError("Voc� n�o tem permiss�o de acessar essa nota.");
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
		}
		return super.getEntradaModelAndView(request, form);
	}
	
	/**
	* M�todo que calcula o valor total da nota
	*
	* @param request
	* @since 17/06/2016
	* @author Luiz Fernando
	*/
	public void ajaxCalculaValorTotal(WebRequestContext request){
		notafiscalprodutoService.ajaxCalculaValorTotal(request);
	}
	
	/**
	* M�todo que verifica se existe algum produto inclu�do na nota tributado por ISSQN
	*
	* @param request
	* @return
	* @since 27/12/2016
	* @author Luiz Fernando
	*/
	@SuppressWarnings("unchecked")
	public ModelAndView existeProdutoTributacaoISSQN(WebRequestContext request){
		boolean tributacaoISSQN = false;
		Object attr = request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = null;
		if (attr != null) {
			listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) attr;
			if(SinedUtil.isListNotEmpty(listaNotafiscalprodutoitem)){
				for(Notafiscalprodutoitem notafiscalprodutoitem : listaNotafiscalprodutoitem){
					if(notafiscalprodutoitem.getTributadoicms() == null || !notafiscalprodutoitem.getTributadoicms()){
						tributacaoISSQN = true;
					}
				}
			}
		}
		return new JsonModelAndView().addObject("tributacaoISSQN", tributacaoISSQN);
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView ajaxCfopNaturezaoperacao(WebRequestContext request, Notafiscalproduto notafiscalproduto) throws Exception {
		String codigoCfopNaturezaoperacao = "";
		Empresa empresa = notafiscalproduto.getEmpresa();
		Endereco enderecoCliente = notafiscalproduto.getEnderecoCliente();
		Naturezaoperacao naturezaoperacao = notafiscalproduto.getNaturezaoperacao();
		
		if (empresa!=null && enderecoCliente!=null && naturezaoperacao!=null){
			empresa = empresaService.loadForEntrada(empresa);
			Endereco enderecoEmpresa = null;
			enderecoCliente = enderecoService.loadEndereco(enderecoCliente);
			
			if (empresa.getListaEndereco()!=null){
				for (Endereco endereco: empresa.getListaEndereco()){
					if (Enderecotipo.FATURAMENTO.equals(endereco.getEnderecotipo())){
						enderecoEmpresa = endereco;
						break;
					}
				}
				
				if (enderecoEmpresa==null){
					for (Endereco endereco: empresa.getListaEndereco()){
						if (!Enderecotipo.INATIVO.equals(endereco.getEnderecotipo())){
							enderecoEmpresa = endereco;
							break;
						}
					}
				}
			}
			
			List<Cfop> listaCfop = new ArrayList<Cfop>();
			
			for (Naturezaoperacaocfop naturezaoperacaocfop: naturezaoperacaocfopService.find(naturezaoperacao, null)){
				listaCfop.add(naturezaoperacaocfop.getCfop());
			}
			
			Cfop cfop = cfopService.getCfop(enderecoEmpresa, enderecoCliente, listaCfop);
			if (cfop!=null){
				codigoCfopNaturezaoperacao = cfop.getCodigo(); 
			}
		}
		
		return new JsonModelAndView().addObject("codigoCfopNaturezaoperacao", codigoCfopNaturezaoperacao);
	}
	
	public ModelAndView identificaConsumidorFinal(WebRequestContext request, Notafiscalproduto notafiscalproduto){
		Operacaonfe operacaonfe = ImpostoUtil.getOperacaonfe(notafiscalproduto.getCliente(), notafiscalproduto.getNaturezaoperacao());
		if(operacaonfe!=null){
			return new JsonModelAndView().addObject("consumidorfinal", Operacaonfe.CONSUMIDOR_FINAL.equals(operacaonfe)? "SIM": "NAO");
		}
		return new JsonModelAndView().addObject("consumidorfinal", "");
	}
	
	public ModelAndView abrirPopUpInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView ajaxInfoDocumentotipo(WebRequestContext request){
		return documentotipoService.ajaxInfoDocumentotipo(request);
	}
	

	public ModelAndView ajaxPrazopagamento(WebRequestContext request, Notafiscalproduto notafiscalproduto){
		return prazopagamentoService.ajaxPrazopagamentoParcelasiguaisjuros(notafiscalproduto.getPrazopagamentofatura());
	}
	public void ajaxBuscaSerienfeEmpresa(WebRequestContext request, Empresa empresa){
		String serienfe_value = "";
		if(empresa.getCdpessoa() != null){
			empresa = empresaService.load(empresa, "empresa.cdpessoa,empresa.serienfe");
			if(empresa != null && empresa.getSerienfe() != null){
				serienfe_value = empresa.getSerienfe().toString();
			}
		}
		View.getCurrent()
			.println(" var serienfe_value = '" + serienfe_value +"';");		
	}
	public ModelAndView resgistrarDeclaracaoDeExportacao(WebRequestContext request) throws IOException{
		String whereIn = SinedUtil.getItensSelecionados(request);
		boolean declarar = true;
		List<Notafiscalproduto> listaNota = notafiscalprodutoService.findByWhereIn(whereIn);
		Empresa empresa = null;
		for (Notafiscalproduto nota : listaNota) {
			NotaStatus status = nota.getNotaStatus();
			if(!NotaStatus.NFE_EMITIDA.equals(status) && !NotaStatus.NFE_LIQUIDADA.equals(status)){
				request.addError("N�o � permitido vincular notas fiscais de situa��o diferente de NF-E EMITIDA ou NF-E LIQUIDADA.");
				declarar = false;
			}
			if(empresa== null){
				empresa = nota.getEmpresa();
			}else{
				if(!empresa.equals(nota.getEmpresa())){
					request.addError("N�o � permitido vincular a Declara��o de Exporta��o, notas fiscais de empresas diferentes.");
					declarar = false;
				}
			}
		}
		if(SinedUtil.isListNotEmpty(declaracaoExportacaoNotaService.buscarNotasVinculadas(whereIn))){
			request.addError("N�o � permitido vincular notas fiscais que j� possuem v�nculo outra Declara��o de Exporta��o.");
		}
		if(declarar){
			return new ModelAndView("redirect:/faturamento/crud/DeclaracaoExportacaoCrud?ACAO=criar&listaNotas="+whereIn);
		}
		return sendRedirectToAction("listagem");
	}
	
	public ModelAndView registrarExpedicao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return sendRedirectToAction("listagem");
		}
	
		List<Notafiscalproduto> listaNota = notafiscalprodutoService.findByWhereIn(whereIn);
		for (Notafiscalproduto nota : listaNota) {
			if(!NotaStatus.NFE_EMITIDA.equals(nota.getNotaStatus()) && !NotaStatus.NFE_LIQUIDADA.equals(nota.getNotaStatus())){
				request.addError("� permitido gerar receita somente de nota(s) na situa��o de NF-e Liquidada ou NF-e Emitida.");
				return sendRedirectToAction("listagem");
			}
			if(SituacaoVinculoExpedicao.EXPEDI��O_COMPLETA.equals(nota.getSituacaoExpedicao())){
				request.addError("Nota j� foi expedida");
				return sendRedirectToAction("listagem");
			}
			if(!Tipooperacaonota.SAIDA.equals(nota.getTipooperacaonota())){
				request.addError("S� � permitido gerar expedi��o de notas fiscais com origem de coleta e de opera��o de sa�da (retorno).");
				return sendRedirectToAction("listagem");
			}
			if(nota.getListaNotafiscalprodutoColeta() == null || nota.getListaNotafiscalprodutoColeta().size() == 0){
				request.addError("S� � permitido gerar expedi��o de notas fiscais com origem de coleta e de opera��o de sa�da (retorno).");
				return sendRedirectToAction("listagem");
			}
		}
		return new ModelAndView("redirect:/faturamento/crud/Expedicao?ACAO=criar&listaNotas="+whereIn);
	}
	
	
	@SuppressWarnings("unchecked")
	public void buscarInfAdicionalProdutoNaturezaoperacao(WebRequestContext request, Notafiscalproduto notafiscalproduto){
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = (List<Notafiscalprodutoitem>) request.getSession().getAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"));
		
		if (SinedUtil.isListNotEmpty(listaNotafiscalprodutoitem) && notafiscalproduto.getNaturezaoperacao()!=null && notafiscalproduto.getNaturezaoperacao().getCdnaturezaoperacao()!=null){
			Naturezaoperacao naturezaoperacao = naturezaoperacaoService.load(notafiscalproduto.getNaturezaoperacao(), "naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.templateinfproduto");
			
			if(notafiscalproduto.getCdNota()==null && (StringUtils.isNotBlank(notafiscalproduto.getWhereInColeta()) || notafiscalproduto.getCdvendaassociada()!=null || 
					StringUtils.isNotBlank(notafiscalproduto.getWhereInEntradaFiscal()))){
				for(Notafiscalprodutoitem item : listaNotafiscalprodutoitem){
					if(StringUtils.isNotBlank(item.getInfoadicional()) && StringUtils.isNotBlank(item.getInfoAdicionalItemAnterior())){
						item.setInfoadicional(item.getInfoadicional().replace(item.getInfoAdicionalItemAnterior(), ""));
					}
					
					String templateInfoAdicionalItem = notafiscalprodutoService.montaInformacoesAdicionaisItemTemplate(item.getInformacaoAdicionalProdutoBean(), naturezaoperacao != null ? naturezaoperacao.getTemplateinfproduto() : null);
					if(templateInfoAdicionalItem != null){
						item.setInfoadicional(templateInfoAdicionalItem);
						item.setInfoAdicionalItemAnterior(templateInfoAdicionalItem);
					}else {
						item.setInfoAdicionalItemAnterior("");
					}
				}
				request.getSession().setAttribute("listaItensNotaFiscalProduto" + request.getParameter("identificadortela"), listaNotafiscalprodutoitem);
			}
		}
	}
	
	public ModelAndView ajaxCompletaDadosFornecedorRetirada(WebRequestContext request, Fornecedor fornecedor) {
		fornecedor = fornecedorService.carregaFornecedor(fornecedor);
		Set<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class, telefoneService.carregarListaTelefone(fornecedor)); 
		
		if (listaTelefone != null && !listaTelefone.isEmpty())
			fornecedor.setListaTelefone(listaTelefone);
		
		return new JsonModelAndView().addObject("fornecedor", fornecedor);
	}
	
	public ModelAndView ajaxCompletaDadosClienteEntrega(WebRequestContext request, Cliente cliente) {
		cliente = clienteService.carregarDadosCliente(cliente);
		Set<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class, telefoneService.carregarListaTelefone(cliente)); 
		
		if (listaTelefone != null && !listaTelefone.isEmpty())
			cliente.setListaTelefone(listaTelefone);
		
		return new JsonModelAndView().addObject("cliente", cliente);
	}
	
	public ModelAndView ajaxMunicipios(WebRequestContext request, Uf uf) {
		return new JsonModelAndView().addObject("listaMunicipio", municipioService.findByUf(uf));
	}
	
	public ModelAndView ajaxVerificaLocalDestino(WebRequestContext request, Notafiscalproduto notafiscalproduto) {
		if(notafiscalproduto.getMunicipiogerador() != null){
			Municipio municipioGerador = municipioService.carregaMunicipio(notafiscalproduto.getMunicipiogerador());			
			notafiscalproduto.setMunicipiogerador(municipioGerador);
		}
		if(notafiscalproduto.getEnderecoCliente() != null){
			Endereco endereco = enderecoService.loadEndereco(notafiscalproduto.getEnderecoCliente());
			notafiscalproduto.setEnderecoCliente(endereco);
		}
		
		if(notafiscalproduto.getMunicipiogerador() != null && notafiscalproduto.getEnderecoCliente() != null){
			notafiscalprodutoService.preencheLocalDestinoNota(notafiscalproduto);
		}
		
		return new JsonModelAndView().addObject("localdestinonfe", notafiscalproduto.getLocaldestinonfe() != null ? notafiscalproduto.getLocaldestinonfe().name() : "<null>");
	}
	
	public ModelAndView popUpNotaErroCorrecao(WebRequestContext request, Notafiscalproduto notafiscalproduto){
		Notaerrocorrecao notaerrocorrecao = new Notaerrocorrecao();
		
		if(notafiscalproduto != null){
			
			Arquivonfnota arquivonfnota = arquivonfnotaService.loadUltimoWithArquivoNf(notafiscalproduto);
			if(arquivonfnota != null){
				List<Arquivonfnotaerro> arquivoNfErros = arquivonfnotaerroService.findByArquivonf(arquivonfnota.getArquivonf());
				if(!arquivoNfErros.isEmpty() && arquivoNfErros.size() > 0) {
					notaerrocorrecao.setCodigo(arquivoNfErros.get(arquivoNfErros.size()-1).getCodigo());
				}	
				notaerrocorrecao = ControleAcessoUtil.util.doVerificaNotaErroCorrecao(request.getServletRequest(), notaerrocorrecao);
			}
			
			/*if (notaerrocorrecao.getCorrecao() == null || "".equals(notaerrocorrecao.getCorrecao())) {
				notafiscalprodutoService.enviaEmailNotaErroCorrecao(notaerrocorrecao);
			}*/
		}
		return new ModelAndView("direct:/crud/popup/popUpNotaErroCorrecao","notaerrocorrecao", notaerrocorrecao);
	}
}