package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Cliente;

public class ClienteVO {
	
	private Integer cdpessoa;
	private String nome;
	
	public ClienteVO() {
	}
	
	public ClienteVO(Integer cdpessoa, String nome) {
		this.cdpessoa = cdpessoa;
		this.nome = nome;
	}

	public ClienteVO(Cliente cliente){
		if(cliente != null){
			this.cdpessoa = cliente.getCdpessoa();
			this.nome = cliente.getNome();
		}
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}
