package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Material;

public class MaterialInfoadicional {
	
	private Material material;
	private String infoadicionalfisco;
	private String infoadicionalcontrib;
	
	public Material getMaterial() {
		return material;
	}
	public String getInfoadicionalfisco() {
		return infoadicionalfisco;
	}
	public String getInfoadicionalcontrib() {
		return infoadicionalcontrib;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setInfoadicionalfisco(String infoadicionalfisco) {
		this.infoadicionalfisco = infoadicionalfisco;
	}
	public void setInfoadicionalcontrib(String infoadicionalcontrib) {
		this.infoadicionalcontrib = infoadicionalcontrib;
	}
}
