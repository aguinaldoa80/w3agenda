package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.modulo.producao.controller.report.bean.PneuReportBean;

public class InformacaoAdicionalProdutoBean {

	private MaterialReportBean material;
	private PneuReportBean pneu;
	private VendamaterialReportBean vendamaterial;
	private PedidovendamaterialReportBean pedidovendamaterial;
	private Boolean recusado;
	private String observacao_coleta;

	public MaterialReportBean getMaterial() {
		return material;
	}
	public PneuReportBean getPneu() {
		return pneu;
	}
	public VendamaterialReportBean getVendamaterial() {
		return vendamaterial;
	}
	public PedidovendamaterialReportBean getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	public Boolean getRecusado() {
		return recusado;
	}
	public String getObservacao_coleta() {
		return observacao_coleta;
	}
	
	public void setMaterial(MaterialReportBean material) {
		this.material = material;
	}
	public void setPneu(PneuReportBean pneu) {
		this.pneu = pneu;
	}
	public void setVendamaterial(VendamaterialReportBean vendamaterial) {
		this.vendamaterial = vendamaterial;
	}
	public void setPedidovendamaterial(PedidovendamaterialReportBean pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public void setRecusado(Boolean recusado) {
		this.recusado = recusado;
	}
	public void setObservacao_coleta(String observacaoColeta) {
		observacao_coleta = observacaoColeta;
	}
}
