package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;

public class EnviarFaturaLocacaoBean {
	
	private String selectedItens;
	private Boolean mesmoCliente;
	private Integer itensSelecionados;
	private Boolean enviarFatura = Boolean.TRUE;
	private Boolean enviarBoleto;
	private List<EnviarFaturaLocacaoEmailBean> listaDestinatario;
	
	public String getSelectedItens() {
		return selectedItens;
	}
	public Boolean getMesmoCliente() {
		return mesmoCliente;
	}
	public Integer getItensSelecionados() {
		return itensSelecionados;
	}
	@DisplayName("Enviar fatura")
	public Boolean getEnviarFatura() {
		return enviarFatura;
	}
	@DisplayName("Enviar boleto")
	public Boolean getEnviarBoleto() {
		return enviarBoleto;
	}
	public List<EnviarFaturaLocacaoEmailBean> getListaDestinatario() {
		return listaDestinatario;
	}
	public String getWhereInCdcontratofaturalocacao() {
		List<Contratofaturalocacao> listaContratofaturalocacao = new ArrayList<Contratofaturalocacao>();
		
		if (listaDestinatario!=null){
			for (EnviarFaturaLocacaoEmailBean enviarFaturaLocacaoEmailBean: listaDestinatario){
				if (enviarFaturaLocacaoEmailBean.getContratofaturalocacao()!=null){
					listaContratofaturalocacao.add(enviarFaturaLocacaoEmailBean.getContratofaturalocacao());
				}
			}
		}
		
		return CollectionsUtil.listAndConcatenate(listaContratofaturalocacao, "cdcontratofaturalocacao", ",");
	}
	
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
	public void setMesmoCliente(Boolean mesmoCliente) {
		this.mesmoCliente = mesmoCliente;
	}
	public void setItensSelecionados(Integer itensSelecionados) {
		this.itensSelecionados = itensSelecionados;
	}
	public void setEnviarFatura(Boolean enviarFatura) {
		this.enviarFatura = enviarFatura;
	}
	public void setEnviarBoleto(Boolean enviarBoleto) {
		this.enviarBoleto = enviarBoleto;
	}
	public void setListaDestinatario(List<EnviarFaturaLocacaoEmailBean> listaDestinatario) {
		this.listaDestinatario = listaDestinatario;
	}	
}