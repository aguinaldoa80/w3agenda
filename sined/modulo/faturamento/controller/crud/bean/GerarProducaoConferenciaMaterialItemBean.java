package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Timestamp;

public class GerarProducaoConferenciaMaterialItemBean {

	private Double qtde;
	private Timestamp data;
	
	public GerarProducaoConferenciaMaterialItemBean() {
	}
	
	public GerarProducaoConferenciaMaterialItemBean(Double qtde, Timestamp data) {
		this.qtde = qtde;
		this.data = data;
	}

	public Double getQtde() {
		return qtde;
	}
	public Timestamp getData() {
		return data;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setData(Timestamp data) {
		this.data = data;
	}
	
}
