package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Pneu;

public class ConferienciaPneuBeam {

	private Integer codBarra;
	private String clienteNome;
	private String origem;
	private Integer cdExpedicao;
	private Pneu pneu;
	private String labelStatus;	
	public Integer getCodBarra() {
		return codBarra;
	}
	public String getClienteNome() {
		return clienteNome;
	}
	public String getOrigem() {
		return origem;
	}
	public Integer getCdExpedicao() {
		return cdExpedicao;
	}
	public String getLabelStatus() {
		return labelStatus;
	}
	public Pneu getPneu() {
		return pneu;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	public void setCodBarra(Integer codBarra) {
		this.codBarra = codBarra;
	}
	public void setClienteNome(String clienteNome) {
		this.clienteNome = clienteNome;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public void setCdExpedicao(Integer cdExpedicao) {
		this.cdExpedicao = cdExpedicao;
	}
	public void setLabelStatus(String labelStatus) {
		this.labelStatus = labelStatus;
	}
	
	
	
}
