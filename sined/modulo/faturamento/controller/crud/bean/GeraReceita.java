package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Prazopagamento;

/**
 * Classe modelo para o formul�rio do caso de uso Gerar Receita
 * 
 * @author hugo
 */
public class GeraReceita {

	private Nota nota;
	private Money descontoContratualPrct;
	private Money descontoContratualMny;
	private Integer qtdeParcelas;
	private Prazopagamento prazoPagamento;
	private Money valorUmaParcela;
	private boolean gerado = false;
	private List<Documento> listaDocumento;
	private Endereco endereco;
	private Date dtemissao;
	private Date dtsaida;
	private Boolean existeDuplicata;
	
	public Nota getNota() {
		return nota;
	}
	
	@Required
	@DisplayName("N� da nota")
	public String getNumero() {
		return nota.getNumero();
	}

	@MinValue(0)
	@MaxValue(100)
	@DisplayName("Desconto contratual (%)")
	public Money getDescontoContratualPrct() {
		return descontoContratualPrct;
	}

	@DisplayName("Desconto contratual (R$)")
	public Money getDescontoContratualMny() {
		return descontoContratualMny;
	}

	@Required
	@MinValue(1)
	@MaxValue(999)
	@DisplayName("N� de parcelas")
	public Integer getQtdeParcelas() {
		return qtdeParcelas;
	}
	
	@Required
	@DisplayName("Prazo de pagamento")
	public Prazopagamento getPrazoPagamento() {
		return prazoPagamento;
	}
	
	@Required
	@DisplayName("Valor de cada parcela")
	public Money getValorUmaParcela() {
		return valorUmaParcela;
	}
	
	@DisplayName("Impostos")
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	/**
	 * Indica se a receita j� foi gerada.
	 * Usado para evitar problemas de exibir/ocultar o bot�o Gerar Receita
	 *
	 * @author Hugo Ferreira
	 */
	public boolean isGerado() {
		return gerado;
	}
	
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	
	public void setNumero(String numero) {
		nota.setNumero(numero);
	}

	public void setDescontoContratualPrct(Money descontoContratualPrct) {
		this.descontoContratualPrct = descontoContratualPrct;
	}

	public void setDescontoContratualMny(Money descontoContratualMny) {
		this.descontoContratualMny = descontoContratualMny;
	}

	public void setQtdeParcelas(Integer qtdeParcelas) {
		this.qtdeParcelas = qtdeParcelas;
	}
	
	public void setPrazoPagamento(Prazopagamento prazoPagamento) {
		this.prazoPagamento = prazoPagamento;
	}
	
	public void setValorUmaParcela(Money valorUmaParcela) {
		this.valorUmaParcela = valorUmaParcela;
	}
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setGerado(boolean gerado) {
		this.gerado = gerado;
	}

	/**
	 * Retorna a diferen�a entre o valor total da nota e o 
	 * valor do desconto contratual.
	 *
	 * @return Money
	 * @author Hugo Ferreira
	 */
	@DisplayName("Valor final")
	public Money getValorFinal() {
		Money valorNota = nota.getValorNota();
		Money valorDesconto = this.descontoContratualMny != null ? this.descontoContratualMny : new Money(0D);
		return valorNota.subtract(valorDesconto);
	}
	
	public Endereco getEndereco() {
		return endereco;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@DisplayName("Data de Emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	
	@DisplayName("Data de Sa�da")
	public Date getDtsaida() {
		return dtsaida;
	}
	
	public void setDtsaida(Date dtsaida) {
		this.dtsaida = dtsaida;
	}

	public Boolean getExisteDuplicata() {
		return existeDuplicata;
	}

	public void setExisteDuplicata(Boolean existeDuplicata) {
		this.existeDuplicata = existeDuplicata;
	}
}
