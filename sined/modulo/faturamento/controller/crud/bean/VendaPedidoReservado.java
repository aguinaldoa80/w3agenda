package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.Date;

public class VendaPedidoReservado {

	Boolean venda;
	String origem;
	Double quantidade;
	String material;
	Date data;
	public Boolean getVenda() {
		return venda;
	}
	public void setVenda(Boolean venda) {
		this.venda = venda;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double double1) {
		this.quantidade = double1;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date date) {
		this.data = date;
	}
	
	
	
}
