package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;

public class MaterialgradeBean {
	
	private Localarmazenagem localarmazenagem;
	private Empresa empresa;
	private Cliente cliente;
	private Materialtabelapreco materialtabelapreco;
	private Loteestoque loteestoque;
	private Double valor;
	private Date prazoentrega; 
	private List<Material> listaMaterial;
	private List<Material> listaBeanVendaMaterial;
	private Integer index;
	
	private Material material;
	private Boolean considerarfiltrolote;
	
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public List<Material> getListaMaterial() {
		return listaMaterial;
	}
	public List<Material> getListaBeanVendaMaterial() {
		return listaBeanVendaMaterial;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}
	public void setListaMaterial(List<Material> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setListaBeanVendaMaterial(List<Material> listaBeanVendaMaterial) {
		this.listaBeanVendaMaterial = listaBeanVendaMaterial;
	}
	
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Double getValor() {
		return valor;
	}
	public Date getPrazoentrega() {
		return prazoentrega;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setPrazoentrega(Date prazoentrega) {
		this.prazoentrega = prazoentrega;
	}
	
	public Boolean getConsiderarfiltrolote() {
		return considerarfiltrolote;
	}
	public void setConsiderarfiltrolote(Boolean considerarfiltrolote) {
		this.considerarfiltrolote = considerarfiltrolote;
	}
}
