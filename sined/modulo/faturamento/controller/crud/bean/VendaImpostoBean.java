package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.Money;

public class VendaImpostoBean {

	protected Money valoraproximadoimposto;
	private List<VendaImpostoItemBean> listavendamaterial;

	public Money getValoraproximadoimposto() {
		return valoraproximadoimposto;
	}
	public List<VendaImpostoItemBean> getListavendamaterial() {
		return listavendamaterial;
	}
	
	public void setValoraproximadoimposto(Money valoraproximadoimposto) {
		this.valoraproximadoimposto = valoraproximadoimposto;
	}
	public void setListavendamaterial(List<VendaImpostoItemBean> listavendamaterial) {
		this.listavendamaterial = listavendamaterial;
	}
}
