package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;

public class FaturamentoConjuntoBean {
	
	private NotaFiscalServico notaFiscalServico;
	private Notafiscalproduto notafiscalproduto;
	private Documento documento;
	private Contratofaturalocacao contratofaturalocacao;
	private Cliente cliente;
	private List<Contrato> listaContrato;
	
	public NotaFiscalServico getNotaFiscalServico() {
		return notaFiscalServico;
	}
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	public Documento getDocumento() {
		return documento;
	}
	public Contratofaturalocacao getContratofaturalocacao() {
		return contratofaturalocacao;
	}
	public List<Contrato> getListaContrato() {
		return listaContrato;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}
	public void setContratofaturalocacao(
			Contratofaturalocacao contratofaturalocacao) {
		this.contratofaturalocacao = contratofaturalocacao;
	}
	public void setNotaFiscalServico(NotaFiscalServico notaFiscalServico) {
		this.notaFiscalServico = notaFiscalServico;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
}
