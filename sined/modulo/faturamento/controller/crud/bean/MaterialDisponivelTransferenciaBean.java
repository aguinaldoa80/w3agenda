package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;

public class MaterialDisponivelTransferenciaBean {

	
	protected Material material;
	protected Double Qtdpedida;
	protected List<Materialrelacionado> listaMaterialrelacionado;
	protected List<Materialrelacionado> listaMaterialrelacionadoNoLocal;
	protected List<Materialrelacionado> listaMaterialrelacionadoSemLocal;
	
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}		
	public List<Materialrelacionado> getListaMaterialrelacionado() {
		return listaMaterialrelacionado;
	}
	public void setListaMaterialrelacionado(
			List<Materialrelacionado> listaMaterialrelacionado) {
		this.listaMaterialrelacionado = listaMaterialrelacionado;
	}
	public List<Materialrelacionado> getListaMaterialrelacionadoSemLocal() {
		return listaMaterialrelacionadoSemLocal;
	}
	public void setListaMaterialrelacionadoSemLocal(
			List<Materialrelacionado> listaMaterialrelacionadoSemLocal) {
		this.listaMaterialrelacionadoSemLocal = listaMaterialrelacionadoSemLocal;
	}
	public Double getQtdpedida() {
		return Qtdpedida;
	}
	public void setQtdpedida(Double qtdpedida) {
		Qtdpedida = qtdpedida;
	}
	public List<Materialrelacionado> getListaMaterialrelacionadoNoLocal() {
		return listaMaterialrelacionadoNoLocal;
	}
	public void setListaMaterialrelacionadoNoLocal(
			List<Materialrelacionado> listaMaterialrelacionadoNoLocal) {
		this.listaMaterialrelacionadoNoLocal = listaMaterialrelacionadoNoLocal;
	}
	
	
	
	

	
	
}
