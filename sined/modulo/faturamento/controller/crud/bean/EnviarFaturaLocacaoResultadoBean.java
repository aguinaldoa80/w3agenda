package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.ArrayList;
import java.util.List;


public class EnviarFaturaLocacaoResultadoBean {
	
	private int sucesso = 0;
	private int erro = 0;
	private List<String> listaErro = new ArrayList<String>();
	
	public int getSucesso() {
		return sucesso;
	}
	public int getErro() {
		return erro;
	}
	public List<String> getListaErro() {
		return listaErro;
	}
	public void setSucesso(int sucesso) {
		this.sucesso = sucesso;
	}
	public void setErro(int erro) {
		this.erro = erro;
	}
	public void setListaErro(List<String> listaErro) {
		this.listaErro = listaErro;
	}
	
	public void addErroComMsg(String erroStr){
		this.listaErro.add(erroStr);
		this.erro++;
	}
	
	public void addSucesso(){
		this.sucesso++;
	}
	
	public void addMsgErro(String erroStr){
		this.listaErro.add(erroStr);
	}
	
	public void addErro(){
		this.erro++;
	}
}