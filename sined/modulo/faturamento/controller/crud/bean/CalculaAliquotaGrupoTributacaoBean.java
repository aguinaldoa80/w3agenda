package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;

public class CalculaAliquotaGrupoTributacaoBean {

	protected Codigocnae codigocnae;
	protected Codigotributacao codigotributacao;
	protected Itemlistaservico itemlistaservico;
	protected Naturezaoperacao naturezaoperacao;
	protected Municipio municipioissqn;
	private Double incentivoFiscalIss;
	
	protected Boolean incideiss;
	protected Boolean incideir;
	protected Boolean incideinss;
	protected Boolean incidepis;
	protected Boolean incidecofins;
	protected Boolean incidecsll;
	protected Boolean incideicms;
	
	protected Money basecalculoir;
	protected Money basecalculoiss;
	protected Money basecalculopis;
	protected Money basecalculocofins;
	protected Money basecalculocsll;
	protected Money basecalculoinss;
	protected Money basecalculoicms;
	
	protected Boolean impostocumulativoiss;
	protected Boolean impostocumulativoinss;
	protected Boolean impostocumulativoir;
	protected Boolean impostocumulativoicms;
	protected Boolean impostocumulativopis;
	protected Boolean impostocumulativocofins;
	protected Boolean impostocumulativocsll;
	
	protected Tipocobrancacofins tipocobrancacofins;
	protected Tipocobrancapis tipocobrancapis;
	protected String tipocobrancacofins_value;
	protected String tipocobrancapis_value;
	
	protected Double iss;
	protected Double ir;
	protected Double inss;
	protected Double pis;
	protected Double cofins;
	protected Double pisretido;
	protected Double cofinsretido;
	protected Double csll;
	protected Double icms;
	
	//informações adicionais grupoTributacao
	protected String dadosAdicionais;
	protected String informacoesComplementares;
	
	public Boolean getIncideiss() {
		return incideiss;
	}
	public Boolean getIncideir() {
		return incideir;
	}
	public Boolean getIncideinss() {
		return incideinss;
	}
	public Boolean getIncidepis() {
		return incidepis;
	}
	public Boolean getIncidecofins() {
		return incidecofins;
	}
	public Boolean getIncidecsll() {
		return incidecsll;
	}
	public Boolean getIncideicms() {
		return incideicms;
	}
	public Money getBasecalculoir() {
		return basecalculoir;
	}
	public Money getBasecalculoiss() {
		return basecalculoiss;
	}
	public Money getBasecalculopis() {
		return basecalculopis;
	}
	public Money getBasecalculocofins() {
		return basecalculocofins;
	}
	public Money getBasecalculocsll() {
		return basecalculocsll;
	}
	public Money getBasecalculoinss() {
		return basecalculoinss;
	}
	public Money getBasecalculoicms() {
		return basecalculoicms;
	}
	public Boolean getImpostocumulativoiss() {
		return impostocumulativoiss;
	}
	public Boolean getImpostocumulativoinss() {
		return impostocumulativoinss;
	}
	public Boolean getImpostocumulativoir() {
		return impostocumulativoir;
	}
	public Boolean getImpostocumulativoicms() {
		return impostocumulativoicms;
	}
	public Boolean getImpostocumulativopis() {
		return impostocumulativopis;
	}
	public Boolean getImpostocumulativocofins() {
		return impostocumulativocofins;
	}
	public Boolean getImpostocumulativocsll() {
		return impostocumulativocsll;
	}
	public Double getIss() {
		return iss;
	}
	public Double getIr() {
		return ir;
	}
	public Double getInss() {
		return inss;
	}
	public Double getPis() {
		return pis;
	}
	public Double getCofins() {
		return cofins;
	}
	public Double getCsll() {
		return csll;
	}
	public Double getIcms() {
		return icms;
	}
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	public Municipio getMunicipioissqn() {
		return municipioissqn;
	}
	public Double getIncentivoFiscalIss() {
		return incentivoFiscalIss;
	}
	public String getDadosAdicionais() {
		return dadosAdicionais;
	}
	public String getInformacoesComplementares() {
		return informacoesComplementares;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setMunicipioissqn(Municipio municipioissqn) {
		this.municipioissqn = municipioissqn;
	}
	public void setIncentivoFiscalIss(Double incentivoFiscalIss) {
		this.incentivoFiscalIss = incentivoFiscalIss;
	}
	public void setIncideiss(Boolean incideiss) {
		this.incideiss = incideiss;
	}
	public void setIncideir(Boolean incideir) {
		this.incideir = incideir;
	}
	public void setIncideinss(Boolean incideinss) {
		this.incideinss = incideinss;
	}
	public void setIncidepis(Boolean incidepis) {
		this.incidepis = incidepis;
	}
	public void setIncidecofins(Boolean incidecofins) {
		this.incidecofins = incidecofins;
	}
	public void setIncidecsll(Boolean incidecsll) {
		this.incidecsll = incidecsll;
	}
	public void setIncideicms(Boolean incideicms) {
		this.incideicms = incideicms;
	}
	public void setBasecalculoir(Money basecalculoir) {
		this.basecalculoir = basecalculoir;
	}
	public void setBasecalculoiss(Money basecalculoiss) {
		this.basecalculoiss = basecalculoiss;
	}
	public void setBasecalculopis(Money basecalculopis) {
		this.basecalculopis = basecalculopis;
	}
	public void setBasecalculocofins(Money basecalculocofins) {
		this.basecalculocofins = basecalculocofins;
	}
	public void setBasecalculocsll(Money basecalculocsll) {
		this.basecalculocsll = basecalculocsll;
	}
	public void setBasecalculoinss(Money basecalculoinss) {
		this.basecalculoinss = basecalculoinss;
	}
	public void setBasecalculoicms(Money basecalculoicms) {
		this.basecalculoicms = basecalculoicms;
	}
	public void setImpostocumulativoiss(Boolean impostocumulativoiss) {
		this.impostocumulativoiss = impostocumulativoiss;
	}
	public void setImpostocumulativoinss(Boolean impostocumulativoinss) {
		this.impostocumulativoinss = impostocumulativoinss;
	}
	public void setImpostocumulativoir(Boolean impostocumulativoir) {
		this.impostocumulativoir = impostocumulativoir;
	}
	public void setImpostocumulativoicms(Boolean impostocumulativoicms) {
		this.impostocumulativoicms = impostocumulativoicms;
	}
	public void setImpostocumulativopis(Boolean impostocumulativopis) {
		this.impostocumulativopis = impostocumulativopis;
	}
	public void setImpostocumulativocofins(Boolean impostocumulativocofins) {
		this.impostocumulativocofins = impostocumulativocofins;
	}
	public void setImpostocumulativocsll(Boolean impostocumulativocsll) {
		this.impostocumulativocsll = impostocumulativocsll;
	}
	public void setIss(Double iss) {
		this.iss = iss;
	}
	public void setIr(Double ir) {
		this.ir = ir;
	}
	public void setInss(Double inss) {
		this.inss = inss;
	}
	public void setPis(Double pis) {
		this.pis = pis;
	}
	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}
	public void setCsll(Double csll) {
		this.csll = csll;
	}
	public void setIcms(Double icms) {
		this.icms = icms;
	}
	public void setDadosAdicionais(String dadosAdicionais) {
		this.dadosAdicionais = dadosAdicionais;
	}
	public void setInformacoesComplementares(String informacoesComplementares) {
		this.informacoesComplementares = informacoesComplementares;
	}
	public Double getPisretido() {
		return pisretido;
	}
	public Double getCofinsretido() {
		return cofinsretido;
	}
	public void setPisretido(Double pisretido) {
		this.pisretido = pisretido;
	}
	public void setCofinsretido(Double cofinsretido) {
		this.cofinsretido = cofinsretido;
	}
	public Tipocobrancacofins getTipocobrancacofins() {
		return tipocobrancacofins;
	}
	public Tipocobrancapis getTipocobrancapis() {
		return tipocobrancapis;
	}
	public void setTipocobrancacofins(Tipocobrancacofins tipocobrancacofins) {
		this.tipocobrancacofins = tipocobrancacofins;
	}
	public void setTipocobrancapis(Tipocobrancapis tipocobrancapis) {
		this.tipocobrancapis = tipocobrancapis;
	}
	public String getTipocobrancacofins_value() {
		if(getTipocobrancacofins() != null)
			return "OPERACAO_" + getTipocobrancacofins().getCdnfe();
		return tipocobrancacofins_value;
	}
	public String getTipocobrancapis_value() {
		if(getTipocobrancapis() != null)
			return "OPERACAO_" + getTipocobrancapis().getCdnfe();
		return tipocobrancapis_value;
	}
	public Codigocnae getCodigocnae() {
		return codigocnae;
	}
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}
	public Itemlistaservico getItemlistaservico() {
		return itemlistaservico;
	}
	public void setCodigocnae(Codigocnae codigocnae) {
		this.codigocnae = codigocnae;
	}
	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}
	public void setItemlistaservico(Itemlistaservico itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}
}
