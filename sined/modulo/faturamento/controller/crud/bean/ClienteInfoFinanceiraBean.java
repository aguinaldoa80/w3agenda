package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class ClienteInfoFinanceiraBean {

	private String clientenome;
	private Money creditolimitecompra;
	private Date dtvalidadelimitecompra;
	private Money valortotalContareceber;
	private Money valortotalChequeDevolvido;
	private Money valortotalChequePendente;
	private Money valortotalPendente;
	private Money saldolimite;
	private Money valorTotalPedidoVenda;
	private Money valorTotalVenda;
	private Integer cdCliente;
	
	public String getClientenome() {
		return clientenome;
	}
	public Money getCreditolimitecompra() {
		return creditolimitecompra;
	}
	public Date getDtvalidadelimitecompra() {
		return dtvalidadelimitecompra;
	}
	public Money getValortotalContareceber() {
		return valortotalContareceber;
	}
	public Money getValortotalChequeDevolvido() {
		return valortotalChequeDevolvido;
	}
	public Money getValortotalChequePendente() {
		return valortotalChequePendente;
	}
	public Money getValortotalPendente() {
		return valortotalPendente;
	}
	public Money getSaldolimite() {
		return saldolimite;
	}

	public void setClientenome(String clientenome) {
		this.clientenome = clientenome;
	}
	public void setCreditolimitecompra(Money creditolimitecompra) {
		this.creditolimitecompra = creditolimitecompra;
	}
	public void setDtvalidadelimitecompra(Date dtvalidadelimitecompra) {
		this.dtvalidadelimitecompra = dtvalidadelimitecompra;
	}
	public void setValortotalContareceber(Money valortotalContareceber) {
		this.valortotalContareceber = valortotalContareceber;
	}
	public void setValortotalChequeDevolvido(Money valortotalChequeDevolvido) {
		this.valortotalChequeDevolvido = valortotalChequeDevolvido;
	}
	public void setValortotalChequePendente(Money valortotalChequePendente) {
		this.valortotalChequePendente = valortotalChequePendente;
	}
	public void setValortotalPendente(Money valortotalPendente) {
		this.valortotalPendente = valortotalPendente;
	}
	public void setSaldolimite(Money saldolimite) {
		this.saldolimite = saldolimite;
	}
	public Money getValorTotalPedidoVenda() {
		return valorTotalPedidoVenda;
	}
	public void setValorTotalPedidoVenda(Money valorTotalPedidoVenda) {
		this.valorTotalPedidoVenda = valorTotalPedidoVenda;
	}
	public Money getValorTotalVenda() {
		return valorTotalVenda;
	}
	public void setValorTotalVenda(Money valorTotalVenda) {
		this.valorTotalVenda = valorTotalVenda;
	}
	public Integer getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(Integer cdCliente) {
		this.cdCliente = cdCliente;
	}
	
	
}
