package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;

public class Enviocomprovanteemail {

	private String whereIn;
	private String entrada;
	
	private String remetente;
	private String assunto;
	private String email;
	
	private Boolean possibilidadeAvulso = Boolean.TRUE;
	private List<EnvioComprovanteDestinatarioBean> listaDestinatarios = new ListSet<EnvioComprovanteDestinatarioBean>(EnvioComprovanteDestinatarioBean.class);
	private List<EnvioComprovanteAnexoBean> listaArquivos = new ListSet<EnvioComprovanteAnexoBean>(EnvioComprovanteAnexoBean.class);
	
	public String getWhereIn() {
		return whereIn;
	}
	public String getEntrada() {
		return entrada;
	}
	public String getRemetente() {
		return remetente;
	}
	public String getAssunto() {
		return assunto;
	}
	public String getEmail() {
		return email;
	}
	public List<EnvioComprovanteDestinatarioBean> getListaDestinatarios() {
		return listaDestinatarios;
	}
	public List<EnvioComprovanteAnexoBean> getListaArquivos() {
		return listaArquivos;
	}
	public Boolean getPossibilidadeAvulso() {
		return possibilidadeAvulso;
	}
	public void setPossibilidadeAvulso(Boolean possibilidadeAvulso) {
		this.possibilidadeAvulso = possibilidadeAvulso;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setListaDestinatarios(
			List<EnvioComprovanteDestinatarioBean> listaDestinatarios) {
		this.listaDestinatarios = listaDestinatarios;
	}
	public void setListaArquivos(List<EnvioComprovanteAnexoBean> listaArquivos) {
		this.listaArquivos = listaArquivos;
	}
	
}