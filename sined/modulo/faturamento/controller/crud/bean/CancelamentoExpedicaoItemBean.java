package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Vendamaterial;

public class CancelamentoExpedicaoItemBean {

	private Localarmazenagem localArmazenagem;
	private Material material;
	private Unidademedida unidadeMedida;
	private Loteestoque loteEstoque;
	private Double quantidade;
	
	private List<Vendamaterial> listaVendaMaterial;
	private List<Expedicaoitem> listaExpedicaoItem;
	
	public CancelamentoExpedicaoItemBean(){
		
	}
	
	public CancelamentoExpedicaoItemBean(Localarmazenagem localArmazenagem, Material material, Unidademedida unidadeMedida, Loteestoque loteEstoque){
		this.material = material;
		this.localArmazenagem = localArmazenagem;
		this.unidadeMedida = unidadeMedida;
		this.loteEstoque = loteEstoque;
	}
	
	
	public Localarmazenagem getLocalArmazenagem() {
		return localArmazenagem;
	}
	public void setLocalArmazenagem(Localarmazenagem localArmazenagem) {
		this.localArmazenagem = localArmazenagem;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Unidademedida getUnidadeMedida() {
		return unidadeMedida;
	}
	public void setUnidadeMedida(Unidademedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public Loteestoque getLoteEstoque() {
		return loteEstoque;
	}
	public void setLoteEstoque(Loteestoque loteEstoque) {
		this.loteEstoque = loteEstoque;
	}
	public Double getQuantidade() {
		if(quantidade == null){
			quantidade = 0D;
		}
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public List<Vendamaterial> getListaVendaMaterial() {
		if(listaVendaMaterial == null){
			listaVendaMaterial = new ArrayList<Vendamaterial>();
		}
		return listaVendaMaterial;
	}
	public void setListaVendaMaterial(List<Vendamaterial> listaVendaMaterial) {
		this.listaVendaMaterial = listaVendaMaterial;
	}
	public List<Expedicaoitem> getListaExpedicaoItem() {
		if(listaExpedicaoItem == null){
			listaExpedicaoItem = new ArrayList<Expedicaoitem>();
		}
		return listaExpedicaoItem;
	}
	public void setListaExpedicaoItem(List<Expedicaoitem> listaExpedicaoItem) {
		this.listaExpedicaoItem = listaExpedicaoItem;
	}
	
	@Override
	public boolean equals(Object obj) {
		CancelamentoExpedicaoItemBean bean = (CancelamentoExpedicaoItemBean)obj;
		return bean.getLocalArmazenagem().equals(localArmazenagem) &&
				bean.getMaterial().equals(material) &&
				bean.getUnidadeMedida().equals(unidadeMedida) &&
				((bean.getLoteEstoque() == null && loteEstoque == null)
				|| bean.getLoteEstoque().equals(localArmazenagem));
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
