package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Loteestoque;

public class Rastro {

	private String nLote;
	private Double qLote;
	private Date dFab;
	private Date dVal;
	private String cAgreg;
	
	private List<Loteestoque> listaLotesW3;
	private Loteestoque loteestoque;
	
	
	
	public String getnLote() {
		return nLote;
	}
	public void setnLote(String nLote) {
		this.nLote = nLote;
	}
	public Double getqLote() {
		return qLote;
	}
	public void setqLote(Double qLote) {
		this.qLote = qLote;
	}
	public Date getdFab() {
		return dFab;
	}
	public void setdFab(Date dFab) {
		this.dFab = dFab;
	}
	public Date getdVal() {
		return dVal;
	}
	public void setdVal(Date dVal) {
		this.dVal = dVal;
	}
	public String getcAgreg() {
		return cAgreg;
	}
	public void setcAgreg(String cAgreg) {
		this.cAgreg = cAgreg;
	}
	public List<Loteestoque> getListaLotesW3() {
		if(listaLotesW3 == null){
			listaLotesW3 = new ArrayList<Loteestoque>();
		}
		return listaLotesW3;
	}
	public void setListaLotesW3(List<Loteestoque> listaLotesW3) {
		this.listaLotesW3 = listaLotesW3;
	}
	public String getNumeroLote() {
		return StringUtils.soNumeroAndLetra(nLote);
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
}
