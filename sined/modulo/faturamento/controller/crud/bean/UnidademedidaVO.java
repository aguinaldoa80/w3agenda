package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Unidademedida;

public class UnidademedidaVO {
	
	private Integer cdunidademedida;
	private String nome;
	
	public UnidademedidaVO() {
	}
	
	public UnidademedidaVO(Integer cdunidademedida, String nome) {
		this.cdunidademedida = cdunidademedida;
		this.nome = nome;
	}

	public UnidademedidaVO(Unidademedida unidademedida){
		if(unidademedida != null){
			this.cdunidademedida = unidademedida.getCdunidademedida();
			this.nome = unidademedida.getNome();
		}
	}

	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public String getNome() {
		return nome;
	}

	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdunidademedida == null) ? 0 : cdunidademedida.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidademedidaVO other = (UnidademedidaVO) obj;
		if (cdunidademedida == null) {
			if (other.cdunidademedida != null)
				return false;
		} else if (!cdunidademedida.equals(other.cdunidademedida))
			return false;
		return true;
	}
}
