package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;


public class Sugestaovenda {
	
	private Integer cdmaterial;
	private String nome_material;
	private String identificacao_material;
	private Integer cdunidademedida;
	private String nome_unidademedida;
	private Double valor;
	private Double valorvendaminimo;
	private Double valorvendamaximo;
	private Double qtde;
	private Boolean vendapromocional;
	private Boolean kitflexivel;
	private Integer qtdeunidade;
	private Boolean considerarvendamultiplos;
	private Double valorcustomaterial;
	private Double valorvendamaterial;
	private Double peso;
	private Double pesobruto;
	private String menorDataValidade;
	private Double qtdeDisponivel;
	private Boolean materialObrigaLote;
	
	public Double getValor() {
		return valor;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}
	public Double getValorvendamaximo() {
		return valorvendamaximo;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public String getNome_material() {
		return nome_material;
	}
	public String getIdentificacao_material() {
		return identificacao_material;
	}
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public String getNome_unidademedida() {
		return nome_unidademedida;
	}
	public Boolean getVendapromocional() {
		return vendapromocional;
	}
	public void setVendapromocional(Boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setNome_material(String nomeMaterial) {
		nome_material = nomeMaterial;
	}
	public void setIdentificacao_material(String identificacaoMaterial) {
		identificacao_material = identificacaoMaterial;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setNome_unidademedida(String nomeUnidademedida) {
		nome_unidademedida = nomeUnidademedida;
	}
	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}
	public void setValorvendamaximo(Double valorvendamaximo) {
		this.valorvendamaximo = valorvendamaximo;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public Integer getQtdeunidade() {
		return qtdeunidade;
	}
	public Boolean getConsiderarvendamultiplos() {
		return considerarvendamultiplos;
	}
	public void setQtdeunidade(Integer qtdeunidade) {
		this.qtdeunidade = qtdeunidade;
	}
	public void setConsiderarvendamultiplos(Boolean considerarvendamultiplos) {
		this.considerarvendamultiplos = considerarvendamultiplos;
	}
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public Double getPesobruto() {
		return pesobruto;
	}
	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}
	public Boolean getKitflexivel() {
		return kitflexivel;
	}
	public void setKitflexivel(Boolean kitflexivel) {
		this.kitflexivel = kitflexivel;
	}
	public String getMenorDataValidade() {
		return menorDataValidade;
	}
	public void setMenorDataValidade(String menorDataValidade) {
		this.menorDataValidade = menorDataValidade;
	}
	public Double getQtdeDisponivel() {
		return qtdeDisponivel;
	}
	public void setQtdeDisponivel(Double qtdeDisponivel) {
		this.qtdeDisponivel = qtdeDisponivel;
	}
	public Boolean getMaterialObrigaLote() {
		return materialObrigaLote;
	}
	public void setMaterialObrigaLote(Boolean materialObrigaLote) {
		this.materialObrigaLote = materialObrigaLote;
	}
}
