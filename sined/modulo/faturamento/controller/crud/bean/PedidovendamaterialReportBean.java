package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;


public class PedidovendamaterialReportBean {

	private String observacao;
	private String cdpedidovenda;
	private String lote;
	
	public PedidovendamaterialReportBean() {}
	
	public PedidovendamaterialReportBean(Pedidovendamaterial bean, Pedidovenda pedidovenda) {
		this.observacao = bean != null && bean.getObservacao() != null ? bean.getObservacao() : "";
		this.cdpedidovenda = bean != null && bean.getPedidovenda() != null && bean.getPedidovenda().getCdpedidovenda() != null ? 
				bean.getPedidovenda().getCdpedidovenda().toString() : 
				bean != null && pedidovenda != null && pedidovenda.getCdpedidovenda() != null ? pedidovenda.getCdpedidovenda().toString() : "";
		this.lote = bean != null && bean.getLoteestoque() != null && bean.getLoteestoque().getNumero() != null ? bean.getLoteestoque().getNumero() : "";
	}
	
	public String getObservacao() {
		return observacao;
	}
	public String getCdpedidovenda() {
		return cdpedidovenda;
	}
	public String getLote() {
		return lote;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdpedidovenda(String cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
}
