package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Material;

public class FornecedoragenciamaterialBean {

	private Material material;
	private Integer index;
	private Double percentualcomissaoagencia;
	
	public Material getMaterial() {
		return material;
	}
	public Integer getIndex() {
		return index;
	}
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}
}
