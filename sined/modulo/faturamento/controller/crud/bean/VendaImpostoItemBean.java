package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.types.Money;

public class VendaImpostoItemBean {

	protected Money valorimposto;
	
	public VendaImpostoItemBean() {}

	public VendaImpostoItemBean(Money valorimposto) {
		this.valorimposto = valorimposto;
	}

	public Money getValorimposto() {
		return valorimposto;
	}

	public void setValorimposto(Money valorimposto) {
		this.valorimposto = valorimposto;
	}
}
