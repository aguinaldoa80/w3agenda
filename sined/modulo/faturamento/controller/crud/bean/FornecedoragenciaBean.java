package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;

public class FornecedoragenciaBean {

	private Fornecedor fornecedor;
	private Fornecedor fornecedorAgencia;
	private Pedidovendatipo pedidovendatipo;
	private Documentotipo documentotipo;
	private List<FornecedoragenciamaterialBean> listaFornecedoragenciamaterialBean;
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Fornecedor getFornecedorAgencia() {
		return fornecedorAgencia;
	}
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public List<FornecedoragenciamaterialBean> getListaFornecedoragenciamaterialBean() {
		return listaFornecedoragenciamaterialBean;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setFornecedorAgencia(Fornecedor fornecedorAgencia) {
		this.fornecedorAgencia = fornecedorAgencia;
	}
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setListaFornecedoragenciamaterialBean(List<FornecedoragenciamaterialBean> listaFornecedoragenciamaterialBean) {
		this.listaFornecedoragenciamaterialBean = listaFornecedoragenciamaterialBean;
	}
}
