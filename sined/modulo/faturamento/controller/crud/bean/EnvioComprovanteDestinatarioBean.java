package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

public class EnvioComprovanteDestinatarioBean {
	
	private Boolean marcado = Boolean.TRUE;
	private Integer id;
	private Integer cdpessoa;
	private String nome;
	private String email;
	
	public Integer getId() {
		return id;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getEmail() {
		return email;
	}
	public String getNome() {
		return nome;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}