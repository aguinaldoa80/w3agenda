package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Materialgrupo;

public class ResumoColaboradorMaterialgrupo {

	private Colaborador colaborador;
	private Materialgrupo materialgrupo;
	private Money valor = new Money();
	private Money desconto = new Money();
	private Double qtde = 0.0;
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public Money getValor() {
		return valor;
	}
	public Double getQtde() {
		return qtde;
	}
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((colaborador == null) ? 0 : colaborador.hashCode());
		result = prime * result
				+ ((materialgrupo == null) ? 0 : materialgrupo.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResumoColaboradorMaterialgrupo other = (ResumoColaboradorMaterialgrupo) obj;
		if (colaborador == null) {
			if (other.colaborador != null)
				return false;
		} else if (!colaborador.equals(other.colaborador))
			return false;
		if (materialgrupo == null) {
			if (other.materialgrupo != null)
				return false;
		} else if (!materialgrupo.equals(other.materialgrupo))
			return false;
		return true;
	}
	
	
	
}
