package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Vendamaterial;

public class ConferenciaMateriaisVendaBean {
	
	private Integer index;
	private Expedicaoitem expedicaoitem;
	private Vendamaterial vendamaterial;
	private Material material;
	private Unidademedida unidademedida;
	private Loteestoque loteestoque;
	private Double quantidade;
	private Double qtdeConferida;
	private Double qtdeConferidaSemArredondamento;
	private String codigoBarrasEmbalagem;
	protected Boolean unidadesMedidaDiferentesConferenciaExpedicao;
	protected Boolean unidadeMedidaPrincipalMaterial;
	
	public ConferenciaMateriaisVendaBean(){
	}
	
	public ConferenciaMateriaisVendaBean(String codigoBarras, Integer cdmaterial, String nomeMaterial, Integer cdunidademedida, String nomeUnidadeMedida,
			String simboloUnidadeMedida, Integer cdembalagem){
		this.material = new Material(cdmaterial, nomeMaterial);
		this.unidademedida = new Unidademedida(cdunidademedida, nomeUnidadeMedida, simboloUnidadeMedida);
		this.material.setCodigobarras(codigoBarras);
	}
	
	public ConferenciaMateriaisVendaBean(Integer index, Vendamaterial vendamaterial){
		this.index = index;
		this.vendamaterial = vendamaterial;
		if(vendamaterial != null){
			this.material = vendamaterial.getMaterial();
			this.unidademedida = vendamaterial.getUnidademedida();
			this.quantidade = vendamaterial.getQuantidade();
			this.codigoBarrasEmbalagem = vendamaterial.getCodigoBarrasEmbalagem();
			this.unidadesMedidaDiferentesConferenciaExpedicao = vendamaterial.getUnidadesMedidaDiferentesConferenciaExpedicao();
			this.unidadeMedidaPrincipalMaterial = vendamaterial.getUnidadeMedidaPrincipalMaterial();
			this.loteestoque = vendamaterial.getLoteestoque();
		}
	}
	
	public ConferenciaMateriaisVendaBean(Integer index, Expedicaoitem expedicaoitem){
		this.index = index;
		this.expedicaoitem = expedicaoitem;
		if(expedicaoitem != null){
			this.material = expedicaoitem.getMaterial();
			this.unidademedida = expedicaoitem.getUnidademedida();
			this.vendamaterial = expedicaoitem.getVendamaterial();
			this.quantidade = expedicaoitem.getQtdeexpedicao();
			this.codigoBarrasEmbalagem = expedicaoitem.getCodigoBarrasEmbalagem();
			this.unidadesMedidaDiferentesConferenciaExpedicao = expedicaoitem.getUnidadesMedidaDiferentesConferenciaExpedicao();
			this.unidadeMedidaPrincipalMaterial = expedicaoitem.getUnidadeMedidaPrincipalMaterial();
			this.loteestoque = expedicaoitem.getLoteestoque();
		}
	}
	
	public Integer getIndex() {
		return index;
	}
	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}
	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}
	public Expedicaoitem getExpedicaoitem() {
		return expedicaoitem;
	}
	public void setExpedicaoitem(Expedicaoitem expedicaoitem) {
		this.expedicaoitem = expedicaoitem;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public Double getQtdeConferida() {
		return qtdeConferida;
	}
	public void setQtdeConferida(Double qtdeConferida) {
		this.qtdeConferida = qtdeConferida;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public String getCodigoBarrasEmbalagem() {
		return codigoBarrasEmbalagem;
	}
	public void setCodigoBarrasEmbalagem(String codigoBarrasEmbalagem) {
		this.codigoBarrasEmbalagem = codigoBarrasEmbalagem;
	}
	public Boolean getUnidadesMedidaDiferentesConferenciaExpedicao() {
		return unidadesMedidaDiferentesConferenciaExpedicao;
	}
	public void setUnidadesMedidaDiferentesConferenciaExpedicao(
			Boolean unidadesMedidaDiferentesConferenciaExpedicao) {
		this.unidadesMedidaDiferentesConferenciaExpedicao = unidadesMedidaDiferentesConferenciaExpedicao;
	}
	public Boolean getUnidadeMedidaPrincipalMaterial() {
		return unidadeMedidaPrincipalMaterial;
	}
	
	public void setUnidadeMedidaPrincipalMaterial(
			Boolean unidadeMedidaPrincipalMaterial) {
		this.unidadeMedidaPrincipalMaterial = unidadeMedidaPrincipalMaterial;
	}
	public Double getQtdeConferidaSemArredondamento() {
		return qtdeConferidaSemArredondamento;
	}
	public void setQtdeConferidaSemArredondamento(
			Double qtdeConferidaSemArredondamento) {
		this.qtdeConferidaSemArredondamento = qtdeConferidaSemArredondamento;
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
}
