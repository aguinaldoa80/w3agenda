package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;

public class EtiquetaPneuFiltro extends ReportTemplateFiltro{
	protected List<EtiquetaPneuItem> listaEtiquetapneuitem = new ArrayList<EtiquetaPneuItem>();

	public List<EtiquetaPneuItem> getListaEtiquetapneuitem() {
		return listaEtiquetapneuitem;
	}

	public void setListaEtiquetapneuitem(List<EtiquetaPneuItem> listaEtiquetapneuitem) {
		this.listaEtiquetapneuitem = listaEtiquetapneuitem;
	}
}
