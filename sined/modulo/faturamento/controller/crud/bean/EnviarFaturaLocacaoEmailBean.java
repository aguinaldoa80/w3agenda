package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;


public class EnviarFaturaLocacaoEmailBean {
	
	private Boolean selecao = Boolean.TRUE;
	private Contratofaturalocacao contratofaturalocacao;
	private String whereInCddocumento;
	private String nome;
	private String email;
	private boolean enviarBoleto = true;
	private boolean avulso = true;
	
	public EnviarFaturaLocacaoEmailBean(){
	}
	
	public EnviarFaturaLocacaoEmailBean(Contratofaturalocacao contratofaturalocacao, String whereInCddocumento, String nome, String email, boolean enviarBoleto){
		this.contratofaturalocacao = contratofaturalocacao;
		this.whereInCddocumento = whereInCddocumento;
		this.nome = nome;
		this.email = email;
		this.enviarBoleto = enviarBoleto;
		this.avulso = false;
	}
	
	public Boolean getSelecao() {
		return selecao;
	}
	public Contratofaturalocacao getContratofaturalocacao() {
		return contratofaturalocacao;
	}
	public String getWhereInCddocumento() {
		return whereInCddocumento;
	}
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	public boolean isEnviarBoleto() {
		return enviarBoleto;
	}
	public boolean isAvulso() {
		return avulso;
	}
	
	public void setSelecao(Boolean selecao) {
		this.selecao = selecao;
	}
	public void setContratofaturalocacao(Contratofaturalocacao contratofaturalocacao) {
		this.contratofaturalocacao = contratofaturalocacao;
	}
	public void setWhereInCddocumento(String whereInCddocumento) {
		this.whereInCddocumento = whereInCddocumento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setEnviarBoleto(boolean enviarBoleto) {
		this.enviarBoleto = enviarBoleto;
	}
	public void setAvulso(boolean avulso) {
		this.avulso = avulso;
	}
}