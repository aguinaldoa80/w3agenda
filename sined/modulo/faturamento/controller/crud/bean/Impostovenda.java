package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.util.SinedUtil;


public class Impostovenda {
	
	private Empresa empresa;
	private Cliente cliente;
	private Endereco endereco;
	private Endereco enderecoFaturamento;
	private Naturezaoperacao naturezaoperacao;
	private Pedidovendatipo pedidovendatipo;
	private List<Impostovendamaterial> listaImpostovendamaterial;
	private List<Impostovendamaterial> listaImpostovendamaterialMestre;
	
	private Money desconto;
	private Money valorusadovalecompra;
	private Money valorfrete;
	@SuppressWarnings("unused")
	private Money totalipi = new Money();
	private Money totalIcms = new Money();
	private Money totalIcmsSt = new Money();
	private Money totalDesoneracaoIcms = new Money();
	private Money totalFcp = new Money();
	private Money totalFcpSt = new Money();
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	public List<Impostovendamaterial> getListaImpostovendamaterial() {
		return listaImpostovendamaterial;
	}
	public List<Impostovendamaterial> getListaImpostovendamaterialMestre() {
		return listaImpostovendamaterialMestre;
	}
	
	public void setTotalDesoneracaoIcms(Money totalDesoneracaoIcms) {
		this.totalDesoneracaoIcms = totalDesoneracaoIcms;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setListaImpostovendamaterial(List<Impostovendamaterial> listaImpostovendamaterial) {
		this.listaImpostovendamaterial = listaImpostovendamaterial;
	}
	public void setListaImpostovendamaterialMestre(
			List<Impostovendamaterial> listaImpostovendamaterialMestre) {
		this.listaImpostovendamaterialMestre = listaImpostovendamaterialMestre;
	}
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Endereco getEnderecoFaturamento() {
		return enderecoFaturamento;
	}
	public void setEnderecoFaturamento(Endereco enderecoFaturamnto) {
		this.enderecoFaturamento = enderecoFaturamnto;
	}
	public Money getValorusadovalecompra() {
		return valorusadovalecompra;
	}
	public void setValorusadovalecompra(Money valorusadovalecompra) {
		this.valorusadovalecompra = valorusadovalecompra;
	}
	public Money getValorfrete() {
		return valorfrete;
	}
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	public Money getTotalipi() {
		Money valor = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaImpostovendamaterial)){
			for (Impostovendamaterial item : this.listaImpostovendamaterial) {
				if(item.getValoripi() != null){
					valor = valor.add(item.getValoripi());
				}
			}
		}
		return valor;
	}
	public Money getTotalDesoneracaoIcms() {
		totalDesoneracaoIcms = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaImpostovendamaterial)){
			for (Impostovendamaterial item : this.listaImpostovendamaterial) {
				totalDesoneracaoIcms = totalDesoneracaoIcms.add(item.getValordesoneracaoicms());
			}
		}
		return totalDesoneracaoIcms;
	}
	
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	
	public Money getTotalFcp() {
		totalFcp = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaImpostovendamaterial)){
			for (Impostovendamaterial item : this.listaImpostovendamaterial) {
				totalFcp = totalFcp.add(item.getValorfcp());
			}
		}
		return totalFcp;
	}
	public void setTotalFcp(Money totalFcp) {
		this.totalFcp = totalFcp;
	}
	public Money getTotalFcpSt() {
		totalFcpSt = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaImpostovendamaterial)){
			for (Impostovendamaterial item : this.listaImpostovendamaterial) {
				totalFcpSt = totalFcpSt.add(item.getValorfcpst());
			}
		}
		return totalFcpSt;
	}
	public void setTotalFcpSt(Money totalFcpSt) {
		this.totalFcpSt = totalFcpSt;
	}
	public Money getTotalIcms() {
		totalIcms = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaImpostovendamaterial)){
			for (Impostovendamaterial item : this.listaImpostovendamaterial) {
				totalIcms = totalIcms.add(item.getValoricms());
			}
		}
		return totalIcms;
	}
	public void setTotalIcms(Money totalIcms) {
		this.totalIcms = totalIcms;
	}
	public Money getTotalIcmsSt() {
		totalIcmsSt = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listaImpostovendamaterial)){
			for (Impostovendamaterial item : this.listaImpostovendamaterial) {
				totalIcmsSt = totalIcmsSt.add(item.getValoricmsst());
			}
		}
		return totalIcmsSt;
	}
	public void setTotalIcmsSt(Money totalIcmsSt) {
		this.totalIcmsSt = totalIcmsSt;
	}
}
