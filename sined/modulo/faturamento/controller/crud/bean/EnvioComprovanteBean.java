package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

public class EnvioComprovanteBean {
	
	private Integer id;
	private String remetente;
	private String assunto;
	private String email;
	
	public Integer getId() {
		return id;
	}
	public String getRemetente() {
		return remetente;
	}
	public String getAssunto() {
		return assunto;
	}
	public String getEmail() {
		return email;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnvioComprovanteBean other = (EnvioComprovanteBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}