package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.util.SinedDateUtils;

public class VendaSomatorioDiaBean {

	private Date data;
	private Emporiumpdv emporiumpdv;
	private Money valortotal = new Money();
	
	public VendaSomatorioDiaBean() {
	}
	
	public VendaSomatorioDiaBean(Date data, Emporiumpdv emporiumpdv) {
		this.data = data;
		this.emporiumpdv = emporiumpdv;
	}

	public Date getData() {
		return data;
	}
	public Money getValortotal() {
		return valortotal;
	}
	public Emporiumpdv getEmporiumpdv() {
		return emporiumpdv;
	}
	public void setEmporiumpdv(Emporiumpdv emporiumpdv) {
		this.emporiumpdv = emporiumpdv;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	
	public VendaSomatorioDiaBean addValortotal(Money valor){
		if(valor != null) this.valortotal = this.valortotal.add(valor);
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((emporiumpdv == null) ? 0 : emporiumpdv.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaSomatorioDiaBean other = (VendaSomatorioDiaBean) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!SinedDateUtils.equalsIgnoreHour(data, other.data))
			return false;
		if (emporiumpdv == null) {
			if (other.emporiumpdv != null)
				return false;
		} else if (!emporiumpdv.equals(other.emporiumpdv))
			return false;
		return true;
	}
	
	
	
}
