package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Documentocomissao;


public class ComissaoMaterialVenda {
	
	private boolean dividirprincipal;
	private boolean comissaogrupomaterial;
	private boolean comissaotabelapreco;
	private boolean achou;
	
	private Double percentualcomissao;
	private Double valorcomissionamentogrupo;
	
	private Double percentualcomissaovendedorprincipal;
	private Double valorcomissionamentogrupovendedorprincipal;
	
	private Double valor;
	
	private Documentocomissao documentocomissaoByMaterial;
	
	public boolean isDividirprincipal() {
		return dividirprincipal;
	}
	public boolean isComissaogrupomaterial() {
		return comissaogrupomaterial;
	}
	public boolean isComissaotabelapreco() {
		return comissaotabelapreco;
	}
	public boolean isAchou() {
		return achou;
	}
	public Double getPercentualcomissao() {
		return percentualcomissao;
	}
	public Double getValorcomissionamentogrupo() {
		return valorcomissionamentogrupo;
	}
	public Double getPercentualcomissaovendedorprincipal() {
		return percentualcomissaovendedorprincipal;
	}
	public Double getValorcomissionamentogrupovendedorprincipal() {
		return valorcomissionamentogrupovendedorprincipal;
	}
	public Double getValor() {
		return valor;
	}
	public Documentocomissao getDocumentocomissaoByMaterial() {
		return documentocomissaoByMaterial;
	}
	
	public void setDividirprincipal(boolean dividirprincipal) {
		this.dividirprincipal = dividirprincipal;
	}
	public void setComissaogrupomaterial(boolean comissaogrupomaterial) {
		this.comissaogrupomaterial = comissaogrupomaterial;
	}
	public void setComissaotabelapreco(boolean comissaotabelapreco) {
		this.comissaotabelapreco = comissaotabelapreco;
	}
	public void setAchou(boolean achou) {
		this.achou = achou;
	}
	public void setPercentualcomissao(Double percentualcomissao) {
		this.percentualcomissao = percentualcomissao;
	}
	public void setValorcomissionamentogrupo(Double valorcomissionamentogrupo) {
		this.valorcomissionamentogrupo = valorcomissionamentogrupo;
	}
	public void setPercentualcomissaovendedorprincipal(Double percentualcomissaovendedorprincipal) {
		this.percentualcomissaovendedorprincipal = percentualcomissaovendedorprincipal;
	}
	public void setValorcomissionamentogrupovendedorprincipal(Double valorcomissionamentogrupovendedorprincipal) {
		this.valorcomissionamentogrupovendedorprincipal = valorcomissionamentogrupovendedorprincipal;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setDocumentocomissaoByMaterial(Documentocomissao documentocomissaoByMaterial) {
		this.documentocomissaoByMaterial = documentocomissaoByMaterial;
	}
	
	public void addPercentualcomissao(Double d){
		if(d != null){
			if(percentualcomissao == null) percentualcomissao = 0d;
			percentualcomissao = percentualcomissao + d;
		}
	}
	
	public void addValorcomissionamentogrupo(Double d){
		if(d != null){
			if(valorcomissionamentogrupo == null) valorcomissionamentogrupo = 0d;
			valorcomissionamentogrupo = valorcomissionamentogrupo + d;
		}
	}
	
	public void addPercentualcomissaovendedorprincipal(Double d){
		if(d != null){
			if(percentualcomissaovendedorprincipal == null) percentualcomissaovendedorprincipal = 0d;
			percentualcomissaovendedorprincipal = percentualcomissaovendedorprincipal + d;
		}
	}
	
	public void addValorcomissionamentogrupovendedorprincipal(Double d){
		if(d != null){
			if(valorcomissionamentogrupovendedorprincipal == null) valorcomissionamentogrupovendedorprincipal = 0d;
			valorcomissionamentogrupovendedorprincipal = valorcomissionamentogrupovendedorprincipal + d;
		}
	}
	
	public void addValor(Double d){
		if(d != null){
			if(valor == null) valor = 0d;
			valor = valor + d;
		}
	}
}
