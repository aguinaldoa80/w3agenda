package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;

public class ExpedicaoTrocaLoteBean {

	private Expedicaoitem expedicaoItem;
	private List<Expedicaoitem> listaExpedicaoItem;
	private Empresa empresa;
	
	
	public Expedicaoitem getExpedicaoItem() {
		return expedicaoItem;
	}
	public void setExpedicaoItem(Expedicaoitem expedicaoItem) {
		this.expedicaoItem = expedicaoItem;
	}
	public List<Expedicaoitem> getListaExpedicaoItem() {
		if(listaExpedicaoItem == null){
			listaExpedicaoItem = new ArrayList<Expedicaoitem>();
		}
		return listaExpedicaoItem;
	}
	public void setListaExpedicaoItem(List<Expedicaoitem> listaExpedicaoItem) {
		this.listaExpedicaoItem = listaExpedicaoItem;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
