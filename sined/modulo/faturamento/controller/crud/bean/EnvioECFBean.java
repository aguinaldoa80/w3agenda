package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.util.SinedDateUtils;

public class EnvioECFBean {

	private String whereIn;
	private String entrada;
	private Emporiumpdv emporiumpdv;
	private String codigocupom;
	private Date data = SinedDateUtils.currentDate();
	
	public String getWhereIn() {
		return whereIn;
	}
	public String getEntrada() {
		return entrada;
	}
	public Emporiumpdv getEmporiumpdv() {
		return emporiumpdv;
	}
	public String getCodigocupom() {
		return codigocupom;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setCodigocupom(String codigocupom) {
		this.codigocupom = codigocupom;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}
	public void setEmporiumpdv(Emporiumpdv emporiumpdv) {
		this.emporiumpdv = emporiumpdv;
	}
	
}
