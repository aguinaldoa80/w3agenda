package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documentotipo;

public class ResumoDocumentotipoVenda {

	private Documentotipo documentotipo;
	private Money valor;
	
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	
	public Money getValor() {
		return valor;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
}
