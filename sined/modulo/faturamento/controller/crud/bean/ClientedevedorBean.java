package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;


public class ClientedevedorBean {
	
	private Boolean contasematraso;
	private Boolean limitecreditoacimapermitido;
	private String mensagem;
	private Boolean chequependente;
	
	public ClientedevedorBean() {
	}
	
	public ClientedevedorBean(Boolean contasematraso, String mensagem) {
		this.contasematraso = contasematraso;
		this.mensagem = mensagem;
	}

	public Boolean getContasematraso() {
		return contasematraso;
	}
	public String getMensagem() {
		return mensagem;
	}

	public void setContasematraso(Boolean contasematraso) {
		this.contasematraso = contasematraso;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Boolean getLimitecreditoacimapermitido() {
		return limitecreditoacimapermitido;
	}

	public void setLimitecreditoacimapermitido(Boolean limitecreditoacimapermitido) {
		this.limitecreditoacimapermitido = limitecreditoacimapermitido;
	}

	public Boolean getChequependente() {
		return chequependente;
	}

	public void setChequependente(Boolean chequependente) {
		this.chequependente = chequependente;
	}
}
