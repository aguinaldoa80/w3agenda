package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;


public class VendamaterialReportBean {

	private String observacao;
	private String cdvenda;
	private String lote;
	
	public VendamaterialReportBean() {}
	
	public VendamaterialReportBean(Vendamaterial bean, Venda venda) {
		this.observacao = bean != null && bean.getObservacao() != null ? bean.getObservacao() : "";
		this.cdvenda = bean != null && bean.getVenda() != null && bean.getVenda().getCdvenda() != null ? 
				bean.getVenda().getCdvenda().toString() : 
				bean != null && venda != null && venda.getCdvenda() != null ? venda.getCdvenda().toString() : "";
		this.lote = bean != null && bean.getLoteestoque() != null && bean.getLoteestoque().getNumero() != null ? bean.getLoteestoque().getNumero() : "";
	}
	
	public String getObservacao() {
		return observacao;
	}
	public String getCdvenda() {
		return cdvenda;
	}
	public String getLote() {
		return lote;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdvenda(String cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
}
