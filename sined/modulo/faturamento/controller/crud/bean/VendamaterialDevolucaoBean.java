package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialdevolucao;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;


public class VendamaterialDevolucaoBean implements ValidateLoteestoqueObrigatorioInterface{

	protected Vendamaterial vendamaterial;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Material material;
	protected Double quantidade;
	protected Double quantidadeTrans;
	protected Double preco;
	protected Double qtdejadevolvida;
	protected Double qtdedevolvida;
	protected Double saldo;
	protected Unidademedida unidademedidaDevolucao;
	protected Unidademedida unidademedida;
	protected Loteestoque loteestoque;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected Double valordevolvido;
	protected Double multiplicador;
	
	protected Materialdevolucao materialdevolucao;
	
	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	public Material getMaterial() {
		return material;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getQuantidadeTrans() {
		return quantidadeTrans;
	}
	public Double getPreco() {
		return preco;
	}
	public Double getQtdejadevolvida() {
		return qtdejadevolvida;
	}
	public Double getQtdedevolvida() {
		return qtdedevolvida;
	}
	public Double getSaldo() {
		return saldo;
	}
	public Unidademedida getUnidademedidaDevolucao() {
		return unidademedidaDevolucao;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public Double getValordevolvido() {
		return valordevolvido;
	}
	
	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setQuantidadeTrans(Double quantidadeTrans) {
		this.quantidadeTrans = quantidadeTrans;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setQtdejadevolvida(Double qtdejadevolvida) {
		this.qtdejadevolvida = qtdejadevolvida;
	}
	public void setQtdedevolvida(Double qtdedevolvida) {
		this.qtdedevolvida = qtdedevolvida;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public void setUnidademedidaDevolucao(Unidademedida unidademedidaDevolucao) {
		this.unidademedidaDevolucao = unidademedidaDevolucao;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setValordevolvido(Double valordevolvido) {
		this.valordevolvido = valordevolvido;
	}

	public Double getMultiplicador() {
		return multiplicador;
	}
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	public Materialdevolucao getMaterialdevolucao() {
		return materialdevolucao;
	}
	
	public void setMaterialdevolucao(Materialdevolucao materialdevolucao) {
		this.materialdevolucao = materialdevolucao;
	}
	
	public Double getFatorconversaoQtdereferencia(){
		Double valor = 1.0;
		if(this.fatorconversao != null)
			valor = (this.fatorconversao / (this.qtdereferencia != null ? this.qtdereferencia : 1.0));
		if(valor == 0) valor = 1d;
		return valor;
	}
}
