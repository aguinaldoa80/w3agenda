package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.util.SinedDateUtils;

public class RealizarFechamentoRomaneioContrato {
	
	private Boolean gerarIndenizacao;
	private Contrato contrato;
	private List<RealizarFechamentoRomaneioContratoItem> listaItens = new ListSet<RealizarFechamentoRomaneioContratoItem>(RealizarFechamentoRomaneioContratoItem.class);
	protected String placa;
	protected String motorista;
	protected Date dtromaneio = SinedDateUtils.currentDate();
	protected Boolean finalizarContrato;
	protected Boolean zerarItensDevolvidos;
	private String whereInContrato;
	
	public List<RealizarFechamentoRomaneioContratoItem> getListaItens() {
		return listaItens;
	}
	
	public Contrato getContrato() {
		return contrato;
	}
	
	public Boolean getGerarIndenizacao() {
		return gerarIndenizacao;
	}
	
	public Date getDtromaneio() {
		return dtromaneio;
	}
	
	public void setDtromaneio(Date dtromaneio) {
		this.dtromaneio = dtromaneio;
	}
	
	public void setGerarIndenizacao(Boolean gerarIndenizacao) {
		this.gerarIndenizacao = gerarIndenizacao;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setListaItens(
			List<RealizarFechamentoRomaneioContratoItem> listaItens) {
		this.listaItens = listaItens;
	}
	
	
	@MaxLength(8)
	@DisplayName("Placa")
	public String getPlaca() {
		return placa;
	}
	@MaxLength(100)
	@DisplayName("Motorista")
	public String getMotorista() {
		return motorista;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}
	
	@DisplayName("Finalizar Contrato")
	public Boolean getFinalizarContrato() {
		return finalizarContrato;
	}
	
	public void setFinalizarContrato(Boolean finalizarContrato) {
		this.finalizarContrato = finalizarContrato;
	}

	public String getWhereInContrato() {
		return whereInContrato;
	}

	public void setWhereInContrato(String whereInContrato) {
		this.whereInContrato = whereInContrato;
	}

	@DisplayName("Zerar itens devolvidos")
	public Boolean getZerarItensDevolvidos() {
		return zerarItensDevolvidos;
	}

	public void setZerarItensDevolvidos(Boolean zerarItensDevolvidos) {
		this.zerarItensDevolvidos = zerarItensDevolvidos;
	}
	
	
}
