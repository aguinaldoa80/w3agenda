package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.NotaTipo;

public class EnvioEmailNotaBean {
	
	private Boolean enviarPdf = Boolean.TRUE;
	private Boolean enviarXml = Boolean.TRUE;
		
	private List<EnvioEmailPessoaBean> listaDestinatario = new ListSet<EnvioEmailPessoaBean>(EnvioEmailPessoaBean.class);
	private Boolean enviarTransportador = Boolean.FALSE;
	
	private String whereInNota;
	private NotaTipo notaTipo;
	private Boolean mesmoCliente;
	private Boolean nfe;
	
	public Boolean getEnviarPdf() {
		return enviarPdf;
	}
	public Boolean getEnviarXml() {
		return enviarXml;
	}
	public List<EnvioEmailPessoaBean> getListaDestinatario() {
		return listaDestinatario;
	}
	public Boolean getEnviarTransportador() {
		return enviarTransportador;
	}
	public String getWhereInNota() {
		return whereInNota;
	}
	public NotaTipo getNotaTipo() {
		return notaTipo;
	}
	public Boolean getMesmoCliente() {
		return mesmoCliente;
	}
	public Boolean getNfe() {
		return nfe;
	}
	public void setNfe(Boolean nfe) {
		this.nfe = nfe;
	}
	public void setEnviarPdf(Boolean enviarPdf) {
		this.enviarPdf = enviarPdf;
	}
	public void setEnviarXml(Boolean enviarXml) {
		this.enviarXml = enviarXml;
	}
	public void setListaDestinatario(List<EnvioEmailPessoaBean> listaDestinatario) {
		this.listaDestinatario = listaDestinatario;
	}
	public void setEnviarTransportador(Boolean enviarTransportador) {
		this.enviarTransportador = enviarTransportador;
	}
	public void setWhereInNota(String whereInNota) {
		this.whereInNota = whereInNota;
	}
	public void setNotaTipo(NotaTipo notaTipo) {
		this.notaTipo = notaTipo;
	}
	public void setMesmoCliente(Boolean mesmoCliente) {
		this.mesmoCliente = mesmoCliente;
	}
	
}
