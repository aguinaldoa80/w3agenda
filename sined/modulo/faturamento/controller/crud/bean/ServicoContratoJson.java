package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

public class ServicoContratoJson {

	private String id;
	private String nome;
	private String complemento;
	
	public String getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
}
