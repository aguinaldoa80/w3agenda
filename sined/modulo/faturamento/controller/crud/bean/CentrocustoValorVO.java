package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;

public class CentrocustoValorVO {

	private Centrocusto centrocusto;
	private Money valor = new Money();
	
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	public Money getValor() {
		return valor;
	}
	
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((centrocusto == null) ? 0 : centrocusto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CentrocustoValorVO other = (CentrocustoValorVO) obj;
		if (centrocusto == null) {
			if (other.centrocusto != null)
				return false;
		} else if (!centrocusto.equals(other.centrocusto))
			return false;
		return true;
	}
	
	
	
}
