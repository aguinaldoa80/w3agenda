package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Venda;


public class VendaDevolucaoBean {

	protected Venda venda;
	protected Pedidovenda pedidovenda;
	protected Empresa empresa;
	protected Cliente cliente;
	protected Boolean creditovalecompradevolucao = Boolean.FALSE;
	protected String origemEntrada;
	
	protected String placaveiculo;
	protected String numero;
	protected Date dtchegada;
	protected Date dtemissao;
	protected Date dtlancamento;
	protected String tipo;
	protected String transportadora;
	protected Localarmazenagem localarmazenagem;
	private Projeto projeto;
	protected Boolean criarEntradaFiscal = Boolean.FALSE;
	protected Arquivo arquivoxmlnfe;
	protected String chaveacesso;
	protected String observacaohistorico;
	protected String url;
	
	protected List<VendamaterialDevolucaoBean> listaItens;
	
	public VendaDevolucaoBean(){}
	
	public VendaDevolucaoBean(Venda venda){
		this.venda = venda;
		this.empresa = venda.getEmpresa();
		this.localarmazenagem = venda.getLocalarmazenagem();
		this.projeto = venda.getProjeto();
		this.cliente = venda.getCliente();
		this.creditovalecompradevolucao = venda.getCreditovalecompradevolucao();
	}

	public VendaDevolucaoBean(Pedidovenda pedidovenda){
		this.pedidovenda = pedidovenda;
		this.empresa = pedidovenda.getEmpresa();
		this.cliente = pedidovenda.getCliente();
		this.localarmazenagem = pedidovenda.getLocalarmazenagem();
		this.projeto = pedidovenda.getProjeto();
	}
	
	public Venda getVenda() {
		return venda;
	}
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Boolean getCreditovalecompradevolucao() {
		return creditovalecompradevolucao;
	}
	public String getOrigemEntrada() {
		return origemEntrada;
	}
	@DisplayName("Placa do Ve�culo")
	public String getPlacaveiculo() {
		return placaveiculo;
	}
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	public Date getDtchegada() {
		return dtchegada;
	}
	public Date getDtemissao() {
		return dtemissao;
	}
	public Date getDtlancamento() {
		return dtlancamento;
	}
	public String getTipo() {
		return tipo;
	}
	public String getTransportadora() {
		return transportadora;
	}
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Boolean getCriarEntradaFiscal() {
		return criarEntradaFiscal;
	}
	public Arquivo getArquivoxmlnfe() {
		return arquivoxmlnfe;
	}
	public String getChaveacesso() {
		return chaveacesso;
	}
	@DisplayName("Produtos")
	public List<VendamaterialDevolucaoBean> getListaItens() {
		return listaItens;
	}
	public String getObservacaohistorico() {
		return observacaohistorico;
	}
	public String getUrl() {
		return url;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setCreditovalecompradevolucao(Boolean creditovalecompradevolucao) {
		this.creditovalecompradevolucao = creditovalecompradevolucao;
	}
	public void setOrigemEntrada(String origemEntrada) {
		this.origemEntrada = origemEntrada;
	}
	public void setPlacaveiculo(String placaveiculo) {
		this.placaveiculo = placaveiculo;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDtchegada(Date dtchegada) {
		this.dtchegada = dtchegada;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setDtlancamento(Date dtlancamento) {
		this.dtlancamento = dtlancamento;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setTransportadora(String transportadora) {
		this.transportadora = transportadora;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCriarEntradaFiscal(Boolean criarEntradaFiscal) {
		this.criarEntradaFiscal = criarEntradaFiscal;
	}
	public void setArquivoxmlnfe(Arquivo arquivoxmlnfe) {
		this.arquivoxmlnfe = arquivoxmlnfe;
	}
	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}
	public void setListaItens(List<VendamaterialDevolucaoBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setObservacaohistorico(String observacaohistorico) {
		this.observacaohistorico = observacaohistorico;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
