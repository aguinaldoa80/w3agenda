package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Pneu;

public class EtiquetaPneuItem {
	protected Integer cdvendamaterial;
	protected Integer cdpedidovendamaterial;
	protected Integer cdpedidovenda;
	protected Integer cdvenda;
	protected String nome;
	protected Pneu pneu;

	public Integer getCdvendamaterial() {
		return cdvendamaterial;
	}
	public void setCdvendamaterial(Integer cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public Integer getCdvenda() {
		return cdvenda;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Pneu getPneu() {
		return pneu;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}

}
