package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Unidademedida;


public class KitflexivelBean {
	
	private List<KitflexivelMestreBean> listaKitflexivelmestreBean;
	private List<KitflexivelitemBean> listaKitflexivelitemBean;

	private String codigo;
	private Material material;
	private Material materialcoleta;
	private String produto_anotacoes;
	private Fornecedor fornecedor;
	private Money percentualRepresentacao;
	private Unidademedida unidademedida;
	private Double qtdestoqueoriginal;
	private Double quantidades;
	private Double qtdestoque;
	private Boolean vendapromocional;
	private Integer qtdeunidade;
	private Boolean considerarvendamultiplos;
	private Double valorcustomaterial;
	private Double valorvendamaterial;
	private Double qtddisponivelAcimaestoque;
	private String msgacimaestoque;
	private Double qtdereservada;
	private Double valor;
	private Double valorminimo;
	private Double valormaximo;
	private Double comprimentos;
	private Double comprimentosoriginal;
	private Double larguras;
	private Double alturas;
	private Double multiplicador;
	private Double fatorconversaocomprimento;
	private Double qtdereferenciacomprimento;
	private Double margemarredondamento;
	private Boolean metrocubicovalorvenda;
	private String identificacaomaterial;
	private Boolean producaosemestoque;
	private Boolean validaestoque;
	private Boolean existematerialsimilar;
	private Boolean existematerialsimilarColeta;
	private Date dtprazoentrega;
	private Loteestoque loteestoque;
	private Double fatorconversao;
	private Double qtdereferencia;
	
	private List<Sugestaovenda> listaSugestaovenda;
	private Localarmazenagem localarmazenagem;
	private Cliente cliente;
	private Empresa empresa;
	private Pedidovendatipo pedidovendatipo;
	private Prazopagamento prazopagamento;
	
	@DisplayName("kit Flex�vel")
	public List<KitflexivelMestreBean> getListaKitflexivelmestreBean() {
		return listaKitflexivelmestreBean;
	}
	public void setListaKitflexivelmestreBean(List<KitflexivelMestreBean> listaKitflexivelmestreBean) {
		this.listaKitflexivelmestreBean = listaKitflexivelmestreBean;
	}
	@DisplayName("Itens do Kit Flex�vel")
	public List<KitflexivelitemBean> getListaKitflexivelitemBean() {
		return listaKitflexivelitemBean;
	}
	public void setListaKitflexivelitemBean(List<KitflexivelitemBean> listaKitflexivelitemBean) {
		this.listaKitflexivelitemBean = listaKitflexivelitemBean;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public Material getMaterial() {
		return material;
	}
	public Material getMaterialcoleta() {
		return materialcoleta;
	}
	public String getProduto_anotacoes() {
		return produto_anotacoes;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Money getPercentualRepresentacao() {
		return percentualRepresentacao;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Double getQtdestoqueoriginal() {
		return qtdestoqueoriginal;
	}
	@DisplayName("Quantidade")
	public Double getQuantidades() {
		return quantidades;
	}
	public Double getQtdestoque() {
		return qtdestoque;
	}
	public Boolean getVendapromocional() {
		return vendapromocional;
	}
	public Integer getQtdeunidade() {
		return qtdeunidade;
	}
	public Boolean getConsiderarvendamultiplos() {
		return considerarvendamultiplos;
	}
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public Double getQtddisponivelAcimaestoque() {
		return qtddisponivelAcimaestoque;
	}
	public String getMsgacimaestoque() {
		return msgacimaestoque;
	}
	public Double getQtdereservada() {
		return qtdereservada;
	}
	public Double getValor() {
		return valor;
	}
	public Double getValorminimo() {
		return valorminimo;
	}
	public Double getValormaximo() {
		return valormaximo;
	}
	public Double getComprimentos() {
		return comprimentos;
	}
	public Double getComprimentosoriginal() {
		return comprimentosoriginal;
	}
	public Double getLarguras() {
		return larguras;
	}
	public Double getAlturas() {
		return alturas;
	}
	public Double getMultiplicador() {
		return multiplicador;
	}
	public Double getFatorconversaocomprimento() {
		return fatorconversaocomprimento;
	}
	public Double getQtdereferenciacomprimento() {
		return qtdereferenciacomprimento;
	}
	public Double getMargemarredondamento() {
		return margemarredondamento;
	}
	public Boolean getMetrocubicovalorvenda() {
		return metrocubicovalorvenda;
	}
	public String getIdentificacaomaterial() {
		return identificacaomaterial;
	}
	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}
	public Boolean getValidaestoque() {
		return validaestoque;
	}
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	public Boolean getExistematerialsimilarColeta() {
		return existematerialsimilarColeta;
	}
	@DisplayName("Prazo de Entrega")
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialcoleta(Material materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	public void setProduto_anotacoes(String produtoAnotacoes) {
		produto_anotacoes = produtoAnotacoes;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setPercentualRepresentacao(Money percentualRepresentacao) {
		this.percentualRepresentacao = percentualRepresentacao;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setQtdestoqueoriginal(Double qtdestoqueoriginal) {
		this.qtdestoqueoriginal = qtdestoqueoriginal;
	}
	public void setQuantidades(Double quantidades) {
		this.quantidades = quantidades;
	}
	public void setQtdestoque(Double qtdestoque) {
		this.qtdestoque = qtdestoque;
	}
	public void setVendapromocional(Boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}
	public void setQtdeunidade(Integer qtdeunidade) {
		this.qtdeunidade = qtdeunidade;
	}
	public void setConsiderarvendamultiplos(Boolean considerarvendamultiplos) {
		this.considerarvendamultiplos = considerarvendamultiplos;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	public void setQtddisponivelAcimaestoque(Double qtddisponivelAcimaestoque) {
		this.qtddisponivelAcimaestoque = qtddisponivelAcimaestoque;
	}
	public void setMsgacimaestoque(String msgacimaestoque) {
		this.msgacimaestoque = msgacimaestoque;
	}
	public void setQtdereservada(Double qtdereservada) {
		this.qtdereservada = qtdereservada;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setValorminimo(Double valorminimo) {
		this.valorminimo = valorminimo;
	}
	public void setValormaximo(Double valormaximo) {
		this.valormaximo = valormaximo;
	}
	public void setComprimentos(Double comprimentos) {
		this.comprimentos = comprimentos;
	}
	public void setComprimentosoriginal(Double comprimentosoriginal) {
		this.comprimentosoriginal = comprimentosoriginal;
	}
	public void setLarguras(Double larguras) {
		this.larguras = larguras;
	}
	public void setAlturas(Double alturas) {
		this.alturas = alturas;
	}
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	public void setFatorconversaocomprimento(Double fatorconversaocomprimento) {
		this.fatorconversaocomprimento = fatorconversaocomprimento;
	}
	public void setQtdereferenciacomprimento(Double qtdereferenciacomprimento) {
		this.qtdereferenciacomprimento = qtdereferenciacomprimento;
	}
	public void setMargemarredondamento(Double margemarredondamento) {
		this.margemarredondamento = margemarredondamento;
	}
	public void setMetrocubicovalorvenda(Boolean metrocubicovalorvenda) {
		this.metrocubicovalorvenda = metrocubicovalorvenda;
	}
	public void setIdentificacaomaterial(String identificacaomaterial) {
		this.identificacaomaterial = identificacaomaterial;
	}
	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	public void setValidaestoque(Boolean validaestoque) {
		this.validaestoque = validaestoque;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}
	public void setExistematerialsimilarColeta(Boolean existematerialsimilarColeta) {
		this.existematerialsimilarColeta = existematerialsimilarColeta;
	}
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}

	public List<Sugestaovenda> getListaSugestaovenda() {
		return listaSugestaovenda;
	}
	public void setListaSugestaovenda(List<Sugestaovenda> listaSugestaovenda) {
		this.listaSugestaovenda = listaSugestaovenda;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
}
