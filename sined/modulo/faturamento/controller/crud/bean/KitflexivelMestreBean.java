package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Unidademedida;


public class KitflexivelMestreBean {

	private Integer indexMestreKitFlexivel;
	private Double altura;
    private Double largura;
    private Double comprimento;
    private Double peso; 
    private Material material;
    private Double qtde;
    private String identificadorinterno;
    private Double preco;
    private Money desconto;
    private Money valorSeguro;
    private Money outrasdespesas;
    private Unidademedida unidademedida;
    private Date dtprazoentrega;
    private Money total;
    private Integer cdpedidovendamaterialmestre;
    private Double valorcustomaterial;
    private Double valorvendamaterial;
    private Double qtdekit;
    private Boolean exibiritenskitflexivel;
    protected Money saldo;
	
    public Double getAltura() {
		return altura;
	}
	public Double getLargura() {
		return largura;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public Double getPeso() {
		return peso;
	}
	public Material getMaterial() {
		return material;
	}
	public Double getQtde() {
		return qtde;
	}
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	@DisplayName("Pre�o")
	public Double getPreco() {
		return preco;
	}
	public Money getDesconto() {
		return desconto;
	}
	public Money getValorSeguro() {
		return valorSeguro;
	}
	@DisplayName("Outras despesas")
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	@DisplayName("Prazo de Entrega")
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	public Integer getCdpedidovendamaterialmestre() {
		return cdpedidovendamaterialmestre;
	}
	public void setCdpedidovendamaterialmestre(Integer cdpedidovendamaterialmestre) {
		this.cdpedidovendamaterialmestre = cdpedidovendamaterialmestre;
	}
	public Integer getIndexMestreKitFlexivel() {
		return indexMestreKitFlexivel;
	}
	public void setIndexMestreKitFlexivel(Integer indexMestreKitFlexivel) {
		this.indexMestreKitFlexivel = indexMestreKitFlexivel;
	}
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	public Double getQtdekit() {
		return qtdekit;
	}
	public void setQtdekit(Double qtdekit) {
		this.qtdekit = qtdekit;
	}
	public Boolean getExibiritenskitflexivel() {
		return exibiritenskitflexivel;
	}
	public void setExibiritenskitflexivel(Boolean exibiritenskitflexivel) {
		this.exibiritenskitflexivel = exibiritenskitflexivel;
	}
	public Money getSaldo() {
		return saldo;
	}
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
}
