package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;

public class RealizarFechamentoRomaneioContratoItem {

	private Material material;
	private Materialclasse materialclasse;
	private Patrimonioitem patrimonioitem;
	private Localarmazenagem localarmazenagemorigem;
	private Localarmazenagem localarmazenagemdestino;
	private Double qtdecontratada;
	private Double qtdeutilizada;
	private Double qtdedevolvida;
	private Boolean isFromFechamento;
	
	private Contratomaterial contratomaterial;
	private Localarmazenagem localDestinoSugerido;
	
	public Material getMaterial() {
		return material;
	}
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	public Localarmazenagem getLocalarmazenagemorigem() {
		return localarmazenagemorigem;
	}
	public Localarmazenagem getLocalarmazenagemdestino() {
		return localarmazenagemdestino;
	}
	public Double getQtdecontratada() {
		return qtdecontratada;
	}
	public Double getQtdeutilizada() {
		return qtdeutilizada;
	}
	public Double getQtdedevolvida() {
		return qtdedevolvida;
	}
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	public Boolean getIsFromFechamento() {
		return isFromFechamento;
	}
	public void setIsFromFechamento(Boolean isFromFechamento) {
		this.isFromFechamento = isFromFechamento;
	}
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setLocalarmazenagemorigem(Localarmazenagem localarmazenagemorigem) {
		this.localarmazenagemorigem = localarmazenagemorigem;
	}
	public void setLocalarmazenagemdestino(Localarmazenagem localarmazenagemdestino) {
		this.localarmazenagemdestino = localarmazenagemdestino;
	}
	public void setQtdecontratada(Double qtdecontratada) {
		this.qtdecontratada = qtdecontratada;
	}
	public void setQtdeutilizada(Double qtdeutilizada) {
		this.qtdeutilizada = qtdeutilizada;
	}
	public void setQtdedevolvida(Double qtdedevolvida) {
		this.qtdedevolvida = qtdedevolvida;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((localarmazenagemdestino == null) ? 0
						: localarmazenagemdestino.hashCode());
		result = prime
				* result
				+ ((localarmazenagemorigem == null) ? 0
						: localarmazenagemorigem.hashCode());
		result = prime * result
				+ ((material == null) ? 0 : material.hashCode());
		result = prime * result
				+ ((materialclasse == null) ? 0 : materialclasse.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RealizarFechamentoRomaneioContratoItem other = (RealizarFechamentoRomaneioContratoItem) obj;
//		if (localarmazenagemdestino == null) {
//			if (other.localarmazenagemdestino != null)
//				return false;
//		} else if (!localarmazenagemdestino
//				.equals(other.localarmazenagemdestino))
//			return false;
		if (localarmazenagemorigem == null) {
			if (other.localarmazenagemorigem != null)
				return false;
		} else if (!localarmazenagemorigem.equals(other.localarmazenagemorigem))
			return false;
		if (material == null) {
			if (other.material != null)
				return false;
		} else if (!material.equals(other.material))
			return false;
		if (materialclasse == null) {
			if (other.materialclasse != null)
				return false;
		} else if (!materialclasse.equals(other.materialclasse))
			return false;
		if(isFromFechamento != null && isFromFechamento){
			if (patrimonioitem == null) {
				if (other.patrimonioitem != null)
					return false;
			} else if (!patrimonioitem.equals(other.patrimonioitem))
				return false;
		}
		return true;
	}
	
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
	public Localarmazenagem getLocalDestinoSugerido() {
		return localDestinoSugerido;
	}
	public void setLocalDestinoSugerido(Localarmazenagem localDestinoSugerido) {
		this.localDestinoSugerido = localDestinoSugerido;
	}
}
