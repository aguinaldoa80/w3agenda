package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoDIFALInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoImpostoInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.util.MoneyUtils;


public class Impostovendamaterial implements CalculoDIFALInterface, CalculoImpostoInterface{
	
	protected Material material;
	protected Double preco;
	protected Double quantidade;
	protected Double multiplicador;
	protected Money valordesconto;
	protected Money valorfrete;
	protected Money valorseguro;
	
	protected Grupotributacao grupotributacao;
	protected Grupotributacao grupotributacaotrans;
	protected Tipocobrancaipi tipocobrancaipi;
	protected String tipocobrancaipiname;
	protected Tipocalculo tipocalculoipi;
	protected String tipocalculoipiname;
	protected Money aliquotareaisipi;
	protected Double ipi;
	protected Money valoripi;
	protected Vendamaterial vendamaterial;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Vendamaterialmestre vendamaterialmestre;
	protected Pedidovendamaterialmestre pedidovendamaterialmestre;
	protected Operacaonfe operacaonfe;
	protected Material materialMestre;
	protected String identificadorInterno;
	protected Double baseCalculoIpi;
	protected Boolean considerarIpiMestre;
	protected Double percentualBcIcmsst;
	protected Double percentualBcfcp;
	protected Tipotributacaoicms tipotributacaoicms;
	protected String tipotributacaoicmsname;
	protected Tipocobrancaicms tipocobrancaicms;
	protected String tipocobrancaicmsname;
	protected Modalidadebcicms modalidadebcicms;
	protected String modalidadebcicmsname;
	protected Money valorbcicms;
	protected Double icms;
	protected Money valoricms;
	
	protected Money valorbcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	protected Modalidadebcicmsst modalidadebcicmsst;
	protected String modalidadebcicmsstname;
	
	protected Double reducaobcicmsst;
	protected Double margemvaloradicionalicmsst;
	
	protected Money valorbcfcp;
	protected Double fcp;
	protected Money valorfcp;
	protected Money valorbcfcpst;
	protected Double fcpst;
	protected Money valorfcpst;
	
	protected Boolean dadosicmspartilha;
	protected Money valorbcdestinatario;
	protected Money valorbcfcpdestinatario;
	protected Double fcpdestinatario;
	protected Double icmsdestinatario;
	protected Double icmsinterestadual;
	protected Double icmsinterestadualpartilha;
	protected Money valorfcpdestinatario;
	protected Money valoricmsdestinatario;
	protected Money valoricmsremetente;
	protected Double difal;
	protected Money valordifal;
	protected Double ie;
	protected Money valorie;
	
	protected Cfop cfop;
	protected Ncmcapitulo ncmcapitulo;
	protected String ncmcompleto;
	
	protected Money valorbruto;
	protected Money outrasdespesas;
	protected Naturezaoperacao naturezaoperacao;
	protected Empresa empresa;
	protected Cliente cliente;
	protected Endereco enderecoCliente;
	protected ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum;
	protected Date dtemissao;
	protected Localdestinonfe localdestinonfe;
	
	
	
	protected Double pis;
	protected Double cofins;
	protected Double iss;
	protected Double ir;
	protected Double inss;
	protected Double csll;
	protected Double percentualBcicms;
	protected Double percentualBcipi;
	protected Double percentualBcpis;
	protected Double percentualBccofins;
	protected Double percentualBciss;
	protected Double percentualBcir;
	protected Double percentualBcinss;
	protected Double percentualBccsll;
	protected Boolean tributadoicms;
	protected Origemproduto origemproduto;
	protected Double reducaobcicms;
	protected ValorFiscal valorFiscalIcms;
	protected Double aliquotacreditoicms;
	protected Boolean incluiricmsvalor;
	protected String cest;
	protected Money bcoperacaopropriaicms;
	protected Uf uficmsst;
	protected Motivodesoneracaoicms motivodesoneracaoicms;
	protected Double percentualdesoneracaoicms;
	protected Money valordesoneracaoicms;
	protected Boolean abaterdesoneracaoicms;
	protected Itemlistaservico itemlistaservico;
	protected Boolean incluirissvalor;					
	protected Tipotributacaoiss tipotributacaoiss;
	protected Boolean incentivofiscal;
	protected Boolean incluircofinsvalor;
	protected Tipocobrancacofins tipocobrancacofins;
	protected Tipocalculo tipocalculocofins;
	protected Money aliquotareaiscofins;
	protected Boolean incluirpisvalor;
	protected Tipocobrancapis tipocobrancapis;
	protected Tipocalculo tipocalculopis;
	protected Money aliquotareaispis;
	protected Boolean incluiripivalor;
	protected String codigoenquadramentoipi;
	protected ValorFiscal valorFiscalIpi;
	
	protected Money valorbciss;
	protected Money valoriss;
	protected Boolean naocalcularipi;
	protected Double qtdevendidaipi;
	protected Money valorbcpis;
	protected Money valorpis;
	protected Money valorbcipi;
	protected Money valorbccofins;
	protected Money valorcofins;
	protected Double qtdevendidacofins;
	protected Money valorcreditoicms;
	protected Double qtdevendidapis;
	protected Boolean calcularIcms;
	protected Boolean calcularFcp;
	protected Boolean calcularIpi;


	public Impostovendamaterial() {}
	
	public Impostovendamaterial(Impostovendamaterial item) {
		this.material = item.getMaterial();
		this.preco = item.getPreco();
		this.quantidade = item.getQuantidade();
		this.multiplicador = item.getMultiplicador();
		this.valordesconto = item.getValordesconto();
		this.vendamaterial = item.getVendamaterial();
		this.pedidovendamaterial = item.getPedidovendamaterial();
		this.considerarIpiMestre = item.getConsiderarIpiMestre();
		this.dtemissao = item.getDtemissao();
		this.outrasdespesas = item.getOutrasdespesas();
		this.valorseguro = item.getValorseguro();
	}
	
	public Impostovendamaterial(Vendaorcamentomaterial item) {
		this.material = item.getMaterial();
		this.preco = item.getPreco();
		this.quantidade = item.getQuantidade();
		this.multiplicador = item.getMultiplicador();
		this.valordesconto = item.getDesconto();
	}

	public Impostovendamaterial(Vendamaterial item) {
		this.material = item.getMaterial();
		this.preco = item.getPreco();
		this.quantidade = item.getQuantidade();
		this.multiplicador = item.getMultiplicador();
		this.valordesconto = item.getDesconto();
	}
	
	public Impostovendamaterial(Pedidovendamaterial item) {
		this.material = item.getMaterial();
		this.preco = item.getPreco();
		this.quantidade = item.getQuantidade();
		this.multiplicador = item.getMultiplicador();
		this.valordesconto = item.getDesconto();
	}

	public Material getMaterial() {
		return material;
	}

	public Double getPreco() {
		if(preco == null){
			preco = 0.0;
		}
		return preco;
	}

	public Double getQuantidade() {
		if(quantidade == null){
			quantidade = 0.0;
		}
		return quantidade;
	}

	public Money getValordesconto() {
		if(valordesconto == null){
			valordesconto = new Money(0.0);
		}
		return valordesconto;
	}

	public Money getValoripi() {
		if(valoripi == null){
			valoripi = new Money(0.0);
		}
		return valoripi;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}

	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}

	public Double getMultiplicador() {
		return multiplicador;
	}

	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}

	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}

	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}

	public Double getIpi() {
		return ipi;
	}

	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}

	public Money getAliquotareaisipi() {
		if(aliquotareaisipi == null){
			aliquotareaisipi = new Money(0.0);
		}
		return aliquotareaisipi;
	}

	public void setAliquotareaisipi(Money aliquotareaisipi) {
		this.aliquotareaisipi = aliquotareaisipi;
	}

	public Money getValorfrete() {
		if(valorfrete == null){
			valorfrete = new Money(0.0);
		}
		return valorfrete;
	}

	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}

	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}

	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}

	public String getTipocobrancaipiname() {
		return tipocobrancaipiname;
	}

	public void setTipocobrancaipiname(String tipocobrancaipiname) {
		this.tipocobrancaipiname = tipocobrancaipiname;
	}

	public Tipocalculo getTipocalculoipi() {
		return tipocalculoipi;
	}

	public String getTipocalculoipiname() {
		return tipocalculoipiname;
	}

	public void setTipocalculoipi(Tipocalculo tipocalculoipi) {
		this.tipocalculoipi = tipocalculoipi;
	}

	public void setTipocalculoipiname(String tipocalculoipiname) {
		this.tipocalculoipiname = tipocalculoipiname;
	}

	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}

	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}

	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}

	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	
	public Vendamaterialmestre getVendamaterialmestre() {
		return vendamaterialmestre;
	}
	
	public void setVendamaterialmestre(Vendamaterialmestre vendamaterialmestre) {
		this.vendamaterialmestre = vendamaterialmestre;
	}
	
	public Pedidovendamaterialmestre getPedidovendamaterialmestre() {
		return pedidovendamaterialmestre;
	}
	
	public void setPedidovendamaterialmestre(
			Pedidovendamaterialmestre pedidovendamaterialmestre) {
		this.pedidovendamaterialmestre = pedidovendamaterialmestre;
	}

	public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}

	public void setOperacaonfe(Operacaonfe operacaonfe) {
		this.operacaonfe = operacaonfe;
	}
	
	public Material getMaterialMestre() {
		return materialMestre;
	}
	
	public void setMaterialMestre(Material materialMestre) {
		this.materialMestre = materialMestre;
	}
	
	public String getIdentificadorInterno() {
		return identificadorInterno;
	}
	
	public void setIdentificadorInterno(String identificadorInterno) {
		this.identificadorInterno = identificadorInterno;
	}
	
	public Double getBaseCalculoIpi() {
		if(baseCalculoIpi == null){
			baseCalculoIpi = 0.0;
		}
		return baseCalculoIpi;
	}
	
	public void setBaseCalculoIpi(Double baseCalculoIpi) {
		this.baseCalculoIpi = baseCalculoIpi;
	}
	
	public Boolean getConsiderarIpiMestre() {
		return considerarIpiMestre;
	}
	
	public void setConsiderarIpiMestre(Boolean considerarIpiMestre) {
		this.considerarIpiMestre = considerarIpiMestre;
	}
	
	public Double getPercentualBcfcp() {
		return percentualBcfcp;
	}
	
	public void setPercentualBcfcp(Double percentualBcfcp) {
		this.percentualBcfcp = percentualBcfcp;
	}
	
	public Double getPercentualBcicms() {
		return percentualBcicms;
	}
	
	public void setPercentualBcicms(Double percentualBcIcms) {
		this.percentualBcicms = percentualBcIcms;
	}
	
	public Double getPercentualBcIcmsst() {
		return percentualBcIcmsst;
	}
	
	public void setPercentualBcIcmsst(Double percentualBcIcmsst) {
		this.percentualBcIcmsst = percentualBcIcmsst;
	}
	
	public Double getPercentualBcipi() {
		return percentualBcipi;
	}
	
	public void setPercentualBcipi(Double percentualBcIpi) {
		this.percentualBcipi = percentualBcIpi;
	}

	public Tipotributacaoicms getTipotributacaoicms() {
		return tipotributacaoicms;
	}

	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms) {
		this.tipotributacaoicms = tipotributacaoicms;
	}
	
	public String getTipotributacaoicmsname() {
		return tipotributacaoicmsname;
	}
	
	public void setTipotributacaoicmsname(String tipotributacaoicmsname) {
		this.tipotributacaoicmsname = tipotributacaoicmsname;
	}
	
	public String getTipocobrancaicmsname() {
		return tipocobrancaicmsname;
	}
	
	public void setTipocobrancaicmsname(String tipocobrancaicmsname) {
		this.tipocobrancaicmsname = tipocobrancaicmsname;
	}

	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}

	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}

	public Modalidadebcicms getModalidadebcicms() {
		return modalidadebcicms;
	}

	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms) {
		this.modalidadebcicms = modalidadebcicms;
	}
	
	public String getModalidadebcicmsname() {
		return modalidadebcicmsname;
	}
	
	public void setModalidadebcicmsname(String modalidadebcicmsname) {
		this.modalidadebcicmsname = modalidadebcicmsname;
	}

	public Money getValorbcicms() {
		return valorbcicms;
	}

	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}

	public Double getIcms() {
		return icms;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public Money getValoricms() {
		return valoricms;
	}

	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}

	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}

	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}

	public Double getIcmsst() {
		return icmsst;
	}

	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}

	public Money getValoricmsst() {
		return valoricmsst;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}

	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}

	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}
	
	public String getModalidadebcicmsstname() {
		return modalidadebcicmsstname;
	}
	
	public void setModalidadebcicmsstname(String modalidadebcicmsstname) {
		this.modalidadebcicmsstname = modalidadebcicmsstname;
	}

	public Double getReducaobcicmsst() {
		return reducaobcicmsst;
	}

	public void setReducaobcicmsst(Double reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}

	public Double getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}

	public void setMargemvaloradicionalicmsst(Double margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}

	public Money getValorbcfcp() {
		return valorbcfcp;
	}

	public void setValorbcfcp(Money valorbcfcp) {
		this.valorbcfcp = valorbcfcp;
	}

	public Double getFcp() {
		return fcp;
	}

	public void setFcp(Double fcp) {
		this.fcp = fcp;
	}

	public Money getValorfcp() {
		return valorfcp;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	public Money getValorbcfcpst() {
		return valorbcfcpst;
	}

	public void setValorbcfcpst(Money valorbcfcpst) {
		this.valorbcfcpst = valorbcfcpst;
	}

	public Double getFcpst() {
		return fcpst;
	}

	public void setFcpst(Double fcpst) {
		this.fcpst = fcpst;
	}

	public Money getValorfcpst() {
		return valorfcpst;
	}

	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}

	public Boolean getDadosicmspartilha() {
		return dadosicmspartilha;
	}

	public void setDadosicmspartilha(Boolean dadosicmspartilha) {
		this.dadosicmspartilha = dadosicmspartilha;
	}

	public Money getValorbcdestinatario() {
		return valorbcdestinatario;
	}

	public void setValorbcdestinatario(Money valorbcdestinatario) {
		this.valorbcdestinatario = valorbcdestinatario;
	}

	public Money getValorbcfcpdestinatario() {
		return valorbcfcpdestinatario;
	}

	public void setValorbcfcpdestinatario(Money valorbcfcpdestinatario) {
		this.valorbcfcpdestinatario = valorbcfcpdestinatario;
	}

	public Double getFcpdestinatario() {
		return fcpdestinatario;
	}

	public void setFcpdestinatario(Double fcpdestinatario) {
		this.fcpdestinatario = fcpdestinatario;
	}

	public Double getIcmsdestinatario() {
		return icmsdestinatario;
	}

	public void setIcmsdestinatario(Double icmsdestinatario) {
		this.icmsdestinatario = icmsdestinatario;
	}

	public Double getIcmsinterestadual() {
		return icmsinterestadual;
	}

	public void setIcmsinterestadual(Double icmsinterestadual) {
		this.icmsinterestadual = icmsinterestadual;
	}

	public Double getIcmsinterestadualpartilha() {
		return icmsinterestadualpartilha;
	}

	public void setIcmsinterestadualpartilha(Double icmsinterestadualpartilha) {
		this.icmsinterestadualpartilha = icmsinterestadualpartilha;
	}

	public Money getValorfcpdestinatario() {
		return MoneyUtils.zeroWhenNull(valorfcpdestinatario);
	}

	public void setValorfcpdestinatario(Money valorfcpdestinatario) {
		this.valorfcpdestinatario = valorfcpdestinatario;
	}

	public Money getValoricmsdestinatario() {
		return MoneyUtils.zeroWhenNull(valoricmsdestinatario);
	}

	public void setValoricmsdestinatario(Money valoricmsdestinatario) {
		this.valoricmsdestinatario = valoricmsdestinatario;
	}

	public Money getValoricmsremetente() {
		return MoneyUtils.zeroWhenNull(valoricmsremetente);
	}

	public void setValoricmsremetente(Money valoricmsremetente) {
		this.valoricmsremetente = valoricmsremetente;
	}

	public Double getDifal() {
		if(difal == null){
			difal = 0D;
		}
		return difal;
	}

	public void setDifal(Double difal) {
		this.difal = difal;
	}

	public Money getValordifal() {
		return MoneyUtils.zeroWhenNull(valordifal);
	}

	public void setValordifal(Money valordifal) {
		this.valordifal = valordifal;
	}

	public Cfop getCfop() {
		return cfop;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}

	public Ncmcapitulo getNcmcapitulo() {
		return ncmcapitulo;
	}

	public void setNcmcapitulo(Ncmcapitulo ncmcapitulo) {
		this.ncmcapitulo = ncmcapitulo;
	}

	public String getNcmcompleto() {
		return ncmcompleto;
	}

	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}

	@Override
	public Money getValorbruto() {
		return MoneyUtils.zeroWhenNull(valorbruto);
	}

	@Override
	public Money getOutrasdespesas() {
		return MoneyUtils.zeroWhenNull(outrasdespesas);
	}
	
	public Money getValorseguro() {
		return MoneyUtils.zeroWhenNull(valorseguro);
	}

	@Override
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	@Override
	public Empresa getEmpresa() {
		return empresa;
	}

	@Override
	public Cliente getCliente() {
		return cliente;
	}

	@Override
	public Endereco getEnderecoCliente() {
		return enderecoCliente;
	}

	@Override
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscalEnum() {
		return modeloDocumentoFiscalEnum;
	}

	@Override
	public Date getDtemissao() {
		return dtemissao;
	}

	@Override
	public Localdestinonfe getLocaldestinonfe() {
		return localdestinonfe;
	}

	public void setValorbruto(Money valorbruto) {
		this.valorbruto = valorbruto;
	}
	
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	
	public void setValoreguro(Money valorseguro) {
		this.valorseguro = valorseguro;
	}
	
	@Override
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public void setEnderecoCliente(Endereco enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}

	@Override
	public void setModeloDocumentoFiscalEnum(
			ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum) {
		this.modeloDocumentoFiscalEnum = modeloDocumentoFiscalEnum;
	}

	@Override
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}

	@Override
	public void setLocaldestinonfe(Localdestinonfe localdestinonfe) {
		this.localdestinonfe = localdestinonfe;
	}
	
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	@Override
	public Double getPis() {
		return pis;
	}

	@Override
	public Double getCofins() {
		return cofins;
	}

	@Override
	public Double getIss() {
		return iss;
	}

	@Override
	public Double getIr() {
		return ir;
	}

	@Override
	public Double getInss() {
		return inss;
	}

	@Override
	public Double getCsll() {
		return csll;
	}

	@Override
	public void setPis(Double pis) {
		this.pis = pis;
	}

	@Override
	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}

	@Override
	public void setIss(Double iss) {
		this.iss = iss;
	}

	@Override
	public void setIr(Double ir) {
		this.ir = ir;
	}

	@Override
	public void setInss(Double inss) {
		this.inss = inss;
	}

	@Override
	public void setCsll(Double csll) {
		this.csll = csll;
	}

		@Override
	public Double getPercentualBcpis() {
		return percentualBcpis;
	}

	@Override
	public Double getPercentualBccofins() {
		return percentualBccofins;
	}

	@Override
	public Double getPercentualBciss() {
		return percentualBciss;
	}

	@Override
	public Double getPercentualBcir() {
		return percentualBcir;
	}

	@Override
	public Double getPercentualBcinss() {
		return percentualBcinss;
	}

	@Override
	public Double getPercentualBccsll() {
		return percentualBccsll;
	}

	@Override
	public void setPercentualBcpis(Double percentualBcpis) {
		this.percentualBcpis = percentualBcpis;
	}

	@Override
	public void setPercentualBccofins(Double percentualBccofins) {
		this.percentualBccofins = percentualBccofins;
	}

	@Override
	public void setPercentualBciss(Double percentualBciss) {
		this.percentualBciss = percentualBciss;
	}

	@Override
	public void setPercentualBcir(Double percentualBcir) {
		this.percentualBcir = percentualBcir;
	}

	@Override
	public void setPercentualBcinss(Double percentualBcinss) {
		this.percentualBcinss = percentualBcinss;
	}

	@Override
	public void setPercentualBccsll(Double percentualBccsll) {
		this.percentualBccsll = percentualBccsll;
	}

	@Override
	public Boolean getTributadoicms() {
		return tributadoicms;
	}

	@Override
	public Origemproduto getOrigemproduto() {
		return origemproduto;
	}

	@Override
	public Double getReducaobcicms() {
		return reducaobcicms;
	}

	@Override
	public ValorFiscal getValorFiscalIcms() {
		return valorFiscalIcms;
	}

	@Override
	public Double getAliquotacreditoicms() {
		return aliquotacreditoicms;
	}

	@Override
	public Boolean getIncluiricmsvalor() {
		return incluiricmsvalor;
	}

	@Override
	public String getCest() {
		return cest;
	}

	@Override
	public Money getBcoperacaopropriaicms() {
		return bcoperacaopropriaicms;
	}

	@Override
	public Uf getUficmsst() {
		return uficmsst;
	}

	@Override
	public Motivodesoneracaoicms getMotivodesoneracaoicms() {
		return motivodesoneracaoicms;
	}
	
	@Override
	public Itemlistaservico getItemlistaservico() {
		return itemlistaservico;
	}

	public Double getPercentualdesoneracaoicms() {
		return percentualdesoneracaoicms;
	}

	public Money getValordesoneracaoicms() {
		return valordesoneracaoicms;
	}
	
	public Boolean getAbaterdesoneracaoicms() {
		return abaterdesoneracaoicms;
	}
	
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms) {
		this.abaterdesoneracaoicms = abaterdesoneracaoicms;
	}

	public void setPercentualdesoneracaoicms(Double percentualdesoneracaoicms) {
		this.percentualdesoneracaoicms = percentualdesoneracaoicms;
	}

	public void setValordesoneracaoicms(Money valordesoneracaoicms) {
		this.valordesoneracaoicms = valordesoneracaoicms;
	}

	@Override
	public void setOrigemproduto(Origemproduto origemprodutoicms) {
		this.origemproduto = origemprodutoicms;
	}

	@Override
	public void setReducaobcicms(Double reducaobcicms) {
		this.reducaobcicms = reducaobcicms;
	}

	@Override
	public void setValorFiscalIcms(ValorFiscal valorFiscal) {
		this.valorFiscalIcms = valorFiscal;
	}

	@Override
	public void setAliquotacreditoicms(Double aliquotacreditoicms) {
		this.aliquotacreditoicms = aliquotacreditoicms;
	}

	@Override
	public void setIncluiricmsvalor(Boolean incluiricmsvalor) {
		this.incluiricmsvalor = incluiricmsvalor;
	}

	@Override
	public void setCest(String cest) {
		this.cest = cest;
	}

	@Override
	public void setBcoperacaopropriaicms(Money bcoperacaopropriaicms) {
		this.bcoperacaopropriaicms = bcoperacaopropriaicms;
	}

	@Override
	public void setUficmsst(Uf uficmsst) {
		this.uficmsst = uficmsst;
	}

	@Override
	public void setMotivodesoneracaoicms(
			Motivodesoneracaoicms motivodesoneracaoicms) {
		this.motivodesoneracaoicms = motivodesoneracaoicms;
	}

	@Override
	public void setItemlistaservico(Itemlistaservico itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}

	@Override
	public Boolean getIncluiripivalor() {
		return incluiripivalor;
	}

	@Override
	public String getCodigoenquadramentoipi() {
		return codigoenquadramentoipi;
	}

	@Override
	public ValorFiscal getValorFiscalIpi() {
		return valorFiscalIpi;
	}

	@Override
	public void setIncluiripivalor(Boolean incluiripivalor) {
		this.incluiripivalor = incluiripivalor;
	}

	@Override
	public void setCodigoenquadramentoipi(String codigoenquadramentoipi) {
		this.codigoenquadramentoipi = codigoenquadramentoipi;
	}

	@Override
	public void setValorFiscalIpi(ValorFiscal valorFiscalIpi) {
		this.valorFiscalIpi = valorFiscalIpi;
	}

	@Override
	public Boolean getIncluirpisvalor() {
		return incluirpisvalor;
	}

	@Override
	public Tipocobrancapis getTipocobrancapis() {
		return tipocobrancapis;
	}

	@Override
	public Tipocalculo getTipocalculopis() {
		return tipocalculopis;
	}

	@Override
	public Money getAliquotareaispis() {
		return aliquotareaispis;
	}

	@Override
	public void setIncluirpisvalor(Boolean incluirpisvalor) {
		this.incluirpisvalor = incluirpisvalor;
	}

	@Override
	public void setTipocobrancapis(Tipocobrancapis tipocobrancapis) {
		this.tipocobrancapis = tipocobrancapis;
	}

	@Override
	public void setTipocalculopis(Tipocalculo tipocalculopis) {
		this.tipocalculopis = tipocalculopis;
	}

	@Override
	public void setAliquotareaispis(Money aliquotareaispis) {
		this.aliquotareaispis = aliquotareaispis;
	}

	@Override
	public Boolean getIncluircofinsvalor() {
		return incluircofinsvalor;
	}

	@Override
	public Tipocobrancacofins getTipocobrancacofins() {
		return tipocobrancacofins;
	}

	@Override
	public Tipocalculo getTipocalculocofins() {
		return tipocalculocofins;
	}

	@Override
	public Money getAliquotareaiscofins() {
		return aliquotareaiscofins;
	}

	@Override
	public void setIncluircofinsvalor(Boolean incluircofinsvalor) {
		this.incluircofinsvalor = incluircofinsvalor;
	}

	@Override
	public void setTipocobrancacofins(Tipocobrancacofins tipocobrancacofins) {
		this.tipocobrancacofins = tipocobrancacofins;
	}

	@Override
	public void setTipocalculocofins(Tipocalculo tipocalculocofins) {
		this.tipocalculocofins = tipocalculocofins;
	}

	@Override
	public void setAliquotareaiscofins(Money aliquotareaiscofins) {
		this.aliquotareaiscofins = aliquotareaiscofins;
	}

	@Override
	public Boolean getIncluirissvalor() {
		return incluirissvalor;
	}

	@Override
	public Tipotributacaoiss getTipotributacaoiss() {
		return tipotributacaoiss;
	}

	@Override
	public Boolean getIncentivofiscal() {
		return incentivofiscal;
	}

	@Override
	public void setIncluirissvalor(Boolean incluirissvalor) {
		this.incluirissvalor = incluirissvalor;
	}

	@Override
	public void setTipotributacaoiss(Tipotributacaoiss tipotributacaoiss) {
		this.tipotributacaoiss = tipotributacaoiss;
	}

	@Override
	public void setIncentivofiscal(Boolean incentivofiscal) {
		this.incentivofiscal = incentivofiscal;
	}

	@Override
	public void setTributadoicms(Boolean tributadoicms) {
		this.tributadoicms = tributadoicms;
	}

	@Override
	public Grupotributacao getGrupotributacaotrans() {
		return grupotributacaotrans;
	}

	@Override
	public void setGrupotributacaotrans(Grupotributacao grupotributacaotrans) {
		this.grupotributacaotrans = grupotributacaotrans;
	}

	@Override
	public Double getValorunitario() {
		return this.getPreco();
	}

	@Override
	public Double getQtde() {
		return getQuantidade();
	}

	@Override
	public void setValorbciss(Money valorbciss) {
		this.valorbciss = valorbciss;
	}

	@Override
	public void setValoriss(Money valoriss) {
		this.valoriss = valoriss;
	}

	@Override
	public void setValorbcpis(Money valorbcpis) {
		this.valorbcpis = valorbcpis;
	}

	@Override
	public void setValorpis(Money valorpis) {
		this.valorpis = valorpis;
	}

	@Override
	public void setValorbcipi(Money valorbcipi) {
		this.valorbcipi = valorbcipi;
	}

	@Override
	public void setValorbccofins(Money valorbccofins) {
		this.valorbccofins = valorbccofins;
	}

	@Override
	public void setValorcofins(Money valorcofins) {
		this.valorcofins = valorcofins;
	}

	@Override
	public void setQtdevendidacofins(Double qtdevendidacofins) {
		this.qtdevendidacofins = qtdevendidacofins;
	}

	@Override
	public void setValorcreditoicms(Money valorcreditoicms) {
		this.valorcreditoicms = valorcreditoicms;
	}

	@Override
	public void setQtdevendidaipi(Double qtdevendidaipi) {
		this.qtdevendidaipi = qtdevendidaipi;
	}

	@Override
	public void setQtdevendidapis(Double qtdevendidapis) {
		this.qtdevendidapis = qtdevendidapis;
	}

	@Override
	public Money getValorbccofins() {
		return valorbccofins;
	}

	@Override
	public Money getValoriss() {
		return valoriss;
	}

	@Override
	public Money getValorpis() {
		return valorpis;
	}

	@Override
	public Money getValorcofins() {
		return valorcofins;
	}

	@Override
	public Money getValorbciss() {
		return valorbciss;
	}

	@Override
	public Boolean getNaocalcularipi() {
		return naocalcularipi;
	}

	@Override
	public Money getValorbcipi() {
		return valorbcipi;
	}

	@Override
	public Money getValorbcpis() {
		return valorbcpis;
	}

	@Override
	public void setValorseguro(Money valorseguro) {
		this.valorseguro = valorseguro;
	}

	@Override
	public Double getQtdevendidapis() {
		return qtdevendidapis;
	}

	@Override
	public Double getQtdevendidacofins() {
		return qtdevendidacofins;
	}

	@Override
	public Double getQtdevendidaipi() {
		return qtdevendidaipi;
	}

	@Override
	public void setNaocalcularipi(Boolean naocalcularipi) {
		this.naocalcularipi = naocalcularipi;
	}

	@Override
	public Boolean getCalcularFcp() {
		return calcularFcp;
	}
	public void setCalcularFcp(Boolean calcularFcp) {
		this.calcularFcp = calcularFcp;
	}
	
	@Override
	public Boolean getCalcularIcms() {
		return calcularIcms;
	}
	public void setCalcularIcms(Boolean calcularIcms) {
		this.calcularIcms = calcularIcms;
	}
	
	@Override
	public Boolean getCalcularIpi() {
		return calcularIpi;
	}
	public void setCalcularIpi(Boolean calcularIpi) {
		this.calcularIpi = calcularIpi;
	}

	@Override
	public Money getValorcreditoicms() {
		return valorcreditoicms;
	}

}