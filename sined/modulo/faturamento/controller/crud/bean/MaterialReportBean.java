package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Material;

public class MaterialReportBean {

	protected Integer cdmaterial;
	protected String nome;
	protected String nomenf;
	protected String identificacao;
	protected String materialgrupo_nome;
	
	public MaterialReportBean(){}
	
	public MaterialReportBean(Material material){
		if(material != null){
			this.cdmaterial = material.getCdmaterial() ;
			this.nome = material.getNome() != null ? material.getNome() : "";
			this.nomenf = material.getNomenf() != null ? material.getNomenf() : "";
			this.identificacao = material.getIdentificacao() != null ? material.getIdentificacao() : "";
			this.materialgrupo_nome = material.getMaterialgrupo() != null && material.getMaterialgrupo().getNome() != null ? material.getMaterialgrupo().getNome() : "";
		}
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public String getNome() {
		return nome;
	}

	public String getNomenf() {
		return nomenf;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getMaterialgrupo_nome() {
		return materialgrupo_nome;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNomenf(String nomenf) {
		this.nomenf = nomenf;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setMaterialgrupo_nome(String materialgrupoNome) {
		materialgrupo_nome = materialgrupoNome;
	}
}
