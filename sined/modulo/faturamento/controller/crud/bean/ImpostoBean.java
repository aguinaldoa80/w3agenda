package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.types.Money;

public class ImpostoBean {

	private Double percentualBC;
	private Money aliquota;
	
	public ImpostoBean(Double percentualBC, Money aliquota){
		this.percentualBC = percentualBC;
		this.aliquota = aliquota;
	}
	
	public Double getPercentualBC() {
		return percentualBC;
	}
	public Money getAliquota() {
		return aliquota;
	}
	public void setPercentualBC(Double percentualBC) {
		this.percentualBC = percentualBC;
	}
	public void setAliquota(Money aliquota) {
		this.aliquota = aliquota;
	}
}
