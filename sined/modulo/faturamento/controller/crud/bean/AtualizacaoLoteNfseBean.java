package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Timestamp;

public class AtualizacaoLoteNfseBean {

	private String numero_rps;
	private String numero_nfse;
	private String codigoverificacao;
	private Timestamp dataemissao;
	private Timestamp competencia;
	
	public String getNumero_rps() {
		return numero_rps;
	}
	public String getNumero_nfse() {
		return numero_nfse;
	}
	public String getCodigoverificacao() {
		return codigoverificacao;
	}
	public Timestamp getDataemissao() {
		return dataemissao;
	}
	public Timestamp getCompetencia() {
		return competencia;
	}
	public void setNumero_rps(String numeroRps) {
		numero_rps = numeroRps;
	}
	public void setNumero_nfse(String numeroNfse) {
		numero_nfse = numeroNfse;
	}
	public void setCodigoverificacao(String codigoverificacao) {
		this.codigoverificacao = codigoverificacao;
	}
	public void setDataemissao(Timestamp dataemissao) {
		this.dataemissao = dataemissao;
	}
	public void setCompetencia(Timestamp competencia) {
		this.competencia = competencia;
	}
	
}
