package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Frequencia;

public class GerarProducaoConferenciaBean {

	private Integer qtdeentregas = 1;
	private Frequencia frequencia;
	private Timestamp dtprimeiraentrega = new Timestamp(System.currentTimeMillis());
	private String whereInContrato;
	private String whereInVenda;
	private String whereInPedidovenda;
	private String whereInPedidovendamaterial;
	private List<GerarProducaoConferenciaMaterialBean> listaMaterial = new ListSet<GerarProducaoConferenciaMaterialBean>(GerarProducaoConferenciaMaterialBean.class);
	
	public Integer getQtdeentregas() {
		return qtdeentregas;
	}
	public Frequencia getFrequencia() {
		return frequencia;
	}
	public Timestamp getDtprimeiraentrega() {
		return dtprimeiraentrega;
	}
	public String getWhereInContrato() {
		return whereInContrato;
	}
	public List<GerarProducaoConferenciaMaterialBean> getListaMaterial() {
		return listaMaterial;
	}
	public String getWhereInVenda() {
		return whereInVenda;
	}
	public String getWhereInPedidovenda() {
		return whereInPedidovenda;
	}
	public void setWhereInVenda(String whereInVenda) {
		this.whereInVenda = whereInVenda;
	}
	public void setListaMaterial(
			List<GerarProducaoConferenciaMaterialBean> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	public void setQtdeentregas(Integer qtdeentregas) {
		this.qtdeentregas = qtdeentregas;
	}
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	public void setDtprimeiraentrega(Timestamp dtprimeiraentrega) {
		this.dtprimeiraentrega = dtprimeiraentrega;
	}
	public void setWhereInContrato(String whereInContrato) {
		this.whereInContrato = whereInContrato;
	}
	public void setWhereInPedidovenda(String whereInPedidovenda) {
		this.whereInPedidovenda = whereInPedidovenda;
	}
	public String getWhereInPedidovendamaterial() {
		return whereInPedidovendamaterial;
	}
	public void setWhereInPedidovendamaterial(String whereInPedidovendamaterial) {
		this.whereInPedidovendamaterial = whereInPedidovendamaterial;
	}
}
