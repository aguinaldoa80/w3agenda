package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Colaborador;

public class TrocavendedorBean {

	private Integer id;
	private Colaborador colaborador;
	private Colaborador colaboradornovo;
	
	public TrocavendedorBean() {
	}
	
	public TrocavendedorBean(Integer id, Colaborador colaborador) {
		this.id = id;
		this.colaborador = colaborador;
	}

	@DisplayName("Id")
	public Integer getId() {
		return id;
	}
	@DisplayName("Responsável")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Novo Responsável")
	public Colaborador getColaboradornovo() {
		return colaboradornovo;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setColaboradornovo(Colaborador colaboradornovo) {
		this.colaboradornovo = colaboradornovo;
	}
}
