package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.Avisonumeronfenaoutilizado;

public class NumeroNfeNaoUtilizadoBean {
	protected Integer cdNota;
	protected Double numeroNFE;
	protected Avisonumeronfenaoutilizado avisonfenaoutilizado;
	protected NotaStatus notaStatus;
	protected Boolean isChecked = Boolean.FALSE;
	protected Integer cdinutilizacaonfe;

	public Integer getCdNota() {
		return cdNota;
	}
	public Double getNumeroNFE() {
		return numeroNFE;
	}
	public Avisonumeronfenaoutilizado getAvisonfenaoutilizado() {
		return avisonfenaoutilizado;
	}
	public NotaStatus getNotaStatus() {
		return notaStatus;
	}
	public Boolean getIsChecked() {
		return isChecked;
	}
	public Integer getCdinutilizacaonfe() {
		return cdinutilizacaonfe;
	}
	public void setCdNota(Integer cdNota) {
		this.cdNota = cdNota;
	}
	public void setNumeroNFE(Double numeroNFE) {
		this.numeroNFE = numeroNFE;
	}
	public void setAvisonfenaoutilizado(Avisonumeronfenaoutilizado avisonfenaoutilizado) {
		this.avisonfenaoutilizado = avisonfenaoutilizado;
	}
	public void setNotaStatus(NotaStatus notaStatus) {
		this.notaStatus = notaStatus;
	}
	public void setIsChecked(Boolean isChecked) {
		this.isChecked = isChecked;
	}
	public void setCdinutilizacaonfe(Integer cdinutilizacaonfe) {
		this.cdinutilizacaonfe = cdinutilizacaonfe;
	}
}
