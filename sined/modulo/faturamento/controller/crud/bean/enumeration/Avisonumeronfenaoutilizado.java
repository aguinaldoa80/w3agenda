package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration;

public enum Avisonumeronfenaoutilizado {

	FORA_DAS_SITUACOES_INDICADAS(1, "Nota fora das situa��es indicadas e com o n�mero n�o utilizado."),
	INUTILIZACAO_NAO_PROCESSADA(2, "Nota inutilizada n�o processada com o n�mero n�o utilizado."),
	INUTILIZACAONFE_CRIADA_NAO_PROCESSADA(3, "Nota com inutiliza��o NF-e gerada e n�o processada."),
	NOTA_NAO_INUTILIZADA(4,"Nota n�o inutilizada e n�o processada"),
	NOTA_PROCESSADA_SUCESSO(5,"Nota Gerada e processada com sucesso"),
	NOTA_PROCESSADA_ERRO(6,"Nota Gerada e processada com erro"),
	NOTA_GERADA_ENVIANDO(7,"Enviando inutiliza��o");
	
	private Integer cdavisonumeronfenaoutilizado;
	private String descricao;
	
	private Avisonumeronfenaoutilizado(Integer cdavisonfenaoutilizado, String descricao) {
		this.cdavisonumeronfenaoutilizado = cdavisonfenaoutilizado;
		this.descricao = descricao;
	}

	public Integer getCdavisonumeronfenaoutilizado() {
		return cdavisonumeronfenaoutilizado;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setCdavisonumeronfenaoutilizado(Integer cdavisonumeronfenaoutilizado) {
		this.cdavisonumeronfenaoutilizado = cdavisonumeronfenaoutilizado;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
