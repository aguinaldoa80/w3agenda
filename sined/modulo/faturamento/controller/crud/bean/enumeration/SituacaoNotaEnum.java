package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoNotaEnum {

	EMITIDA("Emitida"),
	AUTORIZADA("Autorizada");
	
	private SituacaoNotaEnum(String descricao){
		this.descricao = descricao;
	}
	
	private String descricao;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
}
