package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoReservaEnum {

	PEDIDO_VENDA("Pedido de venda"),
	VENDA("Venda");
	
	private TipoReservaEnum(String descricao){
		this.descricao = descricao;
	}
	
	private String descricao;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
}
