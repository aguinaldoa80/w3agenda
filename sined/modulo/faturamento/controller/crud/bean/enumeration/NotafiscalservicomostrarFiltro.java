package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum NotafiscalservicomostrarFiltro {

	CANCELADAS_ELETRONICO	("Canceladas eletronicamente"),
	CANCELADAS_INTERNO		("Canceladas internamente"),
	RPS_GERADO				("RPS gerado"),
	RPS_NAOGERADO			("RPS n�o gerado"),
	LOTERPS_GERADO			("Lote de RPS gerado"),
	LOTERPS_NAOGERADO		("Lote de RPS n�o gerado"),
	;
	
	private NotafiscalservicomostrarFiltro (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
