package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialseparacao;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;


public class KitflexivelitemBean {

	protected Integer indexItemKitFlexivel;
	protected Integer cdpedidovendamaterial;
	protected Integer cdvendamaterial;
	protected Integer cdvendaorcamentomaterial;
	protected Integer ordem;
	protected Material material;
	protected Material materialmestre;
	protected Double quantidade;
	protected Double preco;
	protected Money desconto;
	protected Unidademedida unidademedida;
	protected Date dtprazoentrega;
	protected String observacao;
	protected Loteestoque loteestoque;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected Double valorcustomaterial;
	protected Double valorvendamaterial;
	protected Money saldo;
	protected Double comprimento;
	protected Double comprimentooriginal;
	protected Double largura;
	protected Double altura;
	protected Double multiplicador = 1.0;
	protected Boolean producaosemestoque;
	protected Fornecedor fornecedor;
	protected Money percentualrepresentacao;
	protected Material materialcoleta;
	protected List<Pedidovendamaterialseparacao> listaPedidovendamaterialseparacao = new ListSet<Pedidovendamaterialseparacao>(Pedidovendamaterialseparacao.class);
	protected List<Entregamaterial> listaEntregamaterial;
	protected Set<Vendamaterial> listaVendamaterial;
	protected String identificadorespecifico;
	protected String identificadorinterno;
	
	//Transient
	protected Double total;
	protected Double desconto_it;
	protected Double valordescontocustopedidovenda;
	protected Double percentualdescontocustopedidovenda;
	protected Double margemcustopedidovenda;
	protected Money totalproduto;
	protected Money outrasdespesas;
	protected Money valorSeguro;
	protected Produto produtotrans;
	protected Integer ordemexibicao;
	protected Double valorvendaminimo;
	protected Vendaorcamentomaterial vendaorcamentomaterial;
	protected Boolean isMaterialmestregrade;
	protected String materialcoleta_label;
	protected Double quantidadecoleta;
	protected Double quantidadecoletada;
	protected Localarmazenagem localarmazenagemcoleta;
	protected String observacaocoleta;
	protected Material materialAnterior;
	protected Boolean existematerialsimilar;
	protected Boolean existematerialsimilarColeta;
	protected String historicoTroca;
	protected Boolean existeVinculoColetaProducao = Boolean.FALSE;
	protected Double descontoProporcional;
	protected Boolean considerarDesconto = Boolean.TRUE;
	protected String calcularmargempor;
	protected String fornecedorStr;
	
	
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public Material getMaterial() {
		return material;
	}
	public Material getMaterialmestre() {
		return materialmestre;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	@DisplayName("Pre�o")
	public Double getPreco() {
		return preco;
	}
	public Money getDesconto() {
		return desconto;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	@DisplayName("Prazo de Entrega")
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	public String getObservacao() {
		return observacao;
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public Money getSaldo() {
		return saldo;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public Double getComprimentooriginal() {
		return comprimentooriginal;
	}
	public Double getLargura() {
		return largura;
	}
	public Double getAltura() {
		return altura;
	}
	public Double getMultiplicador() {
		return multiplicador;
	}
	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Money getPercentualrepresentacao() {
		return percentualrepresentacao;
	}
	public Material getMaterialcoleta() {
		return materialcoleta;
	}
	public List<Pedidovendamaterialseparacao> getListaPedidovendamaterialseparacao() {
		return listaPedidovendamaterialseparacao;
	}
	public List<Entregamaterial> getListaEntregamaterial() {
		return listaEntregamaterial;
	}
	public Set<Vendamaterial> getListaVendamaterial() {
		return listaVendamaterial;
	}
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	public Double getTotal() {
		return total;
	}
	public Double getDesconto_it() {
		return desconto_it;
	}
	public Double getValordescontocustopedidovenda() {
		return valordescontocustopedidovenda;
	}
	public Double getPercentualdescontocustopedidovenda() {
		return percentualdescontocustopedidovenda;
	}
	public Double getMargemcustopedidovenda() {
		return margemcustopedidovenda;
	}
	public Money getTotalproduto() {
		return totalproduto;
	}
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	public Money getValorSeguro() {
		return valorSeguro;
	}
	public Produto getProdutotrans() {
		return produtotrans;
	}
	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}
	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}
	public Vendaorcamentomaterial getVendaorcamentomaterial() {
		return vendaorcamentomaterial;
	}
	public Boolean getIsMaterialmestregrade() {
		return isMaterialmestregrade;
	}
	public String getMaterialcoleta_label() {
		return materialcoleta_label;
	}
	public Double getQuantidadecoleta() {
		return quantidadecoleta;
	}
	public Double getQuantidadecoletada() {
		return quantidadecoletada;
	}
	public Localarmazenagem getLocalarmazenagemcoleta() {
		return localarmazenagemcoleta;
	}
	public String getObservacaocoleta() {
		return observacaocoleta;
	}
	public Material getMaterialAnterior() {
		return materialAnterior;
	}
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	public Boolean getExistematerialsimilarColeta() {
		return existematerialsimilarColeta;
	}
	public String getHistoricoTroca() {
		return historicoTroca;
	}
	public Boolean getExisteVinculoColetaProducao() {
		return existeVinculoColetaProducao;
	}
	public Double getDescontoProporcional() {
		return descontoProporcional;
	}
	public Boolean getConsiderarDesconto() {
		return considerarDesconto;
	}
	public String getCalcularmargempor() {
		return calcularmargempor;
	}
	public String getFornecedorStr() {
		return fornecedorStr;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialmestre(Material materialmestre) {
		this.materialmestre = materialmestre;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setComprimentooriginal(Double comprimentooriginal) {
		this.comprimentooriginal = comprimentooriginal;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setPercentualrepresentacao(Money percentualrepresentacao) {
		this.percentualrepresentacao = percentualrepresentacao;
	}
	public void setMaterialcoleta(Material materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	public void setListaPedidovendamaterialseparacao(
			List<Pedidovendamaterialseparacao> listaPedidovendamaterialseparacao) {
		this.listaPedidovendamaterialseparacao = listaPedidovendamaterialseparacao;
	}
	public void setListaEntregamaterial(List<Entregamaterial> listaEntregamaterial) {
		this.listaEntregamaterial = listaEntregamaterial;
	}
	public void setListaVendamaterial(Set<Vendamaterial> listaVendamaterial) {
		this.listaVendamaterial = listaVendamaterial;
	}
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public void setDesconto_it(Double descontoIt) {
		desconto_it = descontoIt;
	}
	public void setValordescontocustopedidovenda(
			Double valordescontocustopedidovenda) {
		this.valordescontocustopedidovenda = valordescontocustopedidovenda;
	}
	public void setPercentualdescontocustopedidovenda(
			Double percentualdescontocustopedidovenda) {
		this.percentualdescontocustopedidovenda = percentualdescontocustopedidovenda;
	}
	public void setMargemcustopedidovenda(Double margemcustopedidovenda) {
		this.margemcustopedidovenda = margemcustopedidovenda;
	}
	public void setTotalproduto(Money totalproduto) {
		this.totalproduto = totalproduto;
	}
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	public void setProdutotrans(Produto produtotrans) {
		this.produtotrans = produtotrans;
	}
	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}
	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}
	public void setVendaorcamentomaterial(
			Vendaorcamentomaterial vendaorcamentomaterial) {
		this.vendaorcamentomaterial = vendaorcamentomaterial;
	}
	public void setIsMaterialmestregrade(Boolean isMaterialmestregrade) {
		this.isMaterialmestregrade = isMaterialmestregrade;
	}
	public void setMaterialcoleta_label(String materialcoletaLabel) {
		materialcoleta_label = materialcoletaLabel;
	}
	public void setQuantidadecoleta(Double quantidadecoleta) {
		this.quantidadecoleta = quantidadecoleta;
	}
	public void setQuantidadecoletada(Double quantidadecoletada) {
		this.quantidadecoletada = quantidadecoletada;
	}
	public void setLocalarmazenagemcoleta(Localarmazenagem localarmazenagemcoleta) {
		this.localarmazenagemcoleta = localarmazenagemcoleta;
	}
	public void setObservacaocoleta(String observacaocoleta) {
		this.observacaocoleta = observacaocoleta;
	}
	public void setMaterialAnterior(Material materialAnterior) {
		this.materialAnterior = materialAnterior;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}
	public void setExistematerialsimilarColeta(Boolean existematerialsimilarColeta) {
		this.existematerialsimilarColeta = existematerialsimilarColeta;
	}
	public void setHistoricoTroca(String historicoTroca) {
		this.historicoTroca = historicoTroca;
	}
	public void setExisteVinculoColetaProducao(Boolean existeVinculoColetaProducao) {
		this.existeVinculoColetaProducao = existeVinculoColetaProducao;
	}
	public void setDescontoProporcional(Double descontoProporcional) {
		this.descontoProporcional = descontoProporcional;
	}
	public void setConsiderarDesconto(Boolean considerarDesconto) {
		this.considerarDesconto = considerarDesconto;
	}
	public void setCalcularmargempor(String calcularmargempor) {
		this.calcularmargempor = calcularmargempor;
	}
	public void setFornecedorStr(String fornecedorStr) {
		this.fornecedorStr = fornecedorStr;
	}
	public Integer getCdvendamaterial() {
		return cdvendamaterial;
	}
	public Integer getCdvendaorcamentomaterial() {
		return cdvendaorcamentomaterial;
	}
	public void setCdvendamaterial(Integer cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	public void setCdvendaorcamentomaterial(Integer cdvendaorcamentomaterial) {
		this.cdvendaorcamentomaterial = cdvendaorcamentomaterial;
	}
	public Integer getIndexItemKitFlexivel() {
		return indexItemKitFlexivel;
	}
	public void setIndexItemKitFlexivel(Integer indexItemKitFlexivel) {
		this.indexItemKitFlexivel = indexItemKitFlexivel;
	}
}
