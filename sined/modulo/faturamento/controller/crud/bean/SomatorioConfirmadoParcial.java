package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Pedidovenda;

public class SomatorioConfirmadoParcial {
	private Pedidovenda pedidovenda;
	private Money taxapedidovendaConfirmado;
	private Money descontoConfirmado;
	private Money valorusadovalecompraConfirmado;
	private Money valorfreteConfirmado;
	
	public SomatorioConfirmadoParcial() { }
	
	public SomatorioConfirmadoParcial(Money taxapedidovendaConfirmado, Money descontoConfirmado, 
			Money valorusadovalecompraConfirmado, Money valorfreteConfirmado) {
		this.taxapedidovendaConfirmado = taxapedidovendaConfirmado;
		this.descontoConfirmado = descontoConfirmado;
		this.valorusadovalecompraConfirmado = valorusadovalecompraConfirmado;
		this.valorfreteConfirmado = valorfreteConfirmado;
	}
	
	public SomatorioConfirmadoParcial(Pedidovenda pedidovenda, Money taxapedidovendaConfirmado, 
			Money descontoConfirmado, Money valorusadovalecompraConfirmado, 
			Money valorfreteConfirmado) {
		this.pedidovenda = pedidovenda;
		this.taxapedidovendaConfirmado = taxapedidovendaConfirmado;
		this.descontoConfirmado = descontoConfirmado;
		this.valorusadovalecompraConfirmado = valorusadovalecompraConfirmado;
		this.valorfreteConfirmado = valorfreteConfirmado;
	}

	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}

	public Money getTaxapedidovendaConfirmado() {
		return taxapedidovendaConfirmado;
	}

	public Money getDescontoConfirmado() {
		return descontoConfirmado;
	}

	public Money getValorusadovalecompraConfirmado() {
		return valorusadovalecompraConfirmado;
	}

	public Money getValorfreteConfirmado() {
		return valorfreteConfirmado;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	public void setTaxapedidovendaConfirmado(Money taxapedidovendaConfirmado) {
		this.taxapedidovendaConfirmado = taxapedidovendaConfirmado;
	}

	public void setDescontoConfirmado(Money descontoConfirmado) {
		this.descontoConfirmado = descontoConfirmado;
	}

	public void setValorusadovalecompraConfirmado(
			Money valorusadovalecompraConfirmado) {
		this.valorusadovalecompraConfirmado = valorusadovalecompraConfirmado;
	}

	public void setValorfreteConfirmado(Money valorfreteConfirmado) {
		this.valorfreteConfirmado = valorfreteConfirmado;
	}
}
