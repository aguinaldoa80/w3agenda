package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;


public class MaterialvalorvendaBean {

	private String index;
	private Double valorvenda;

	public MaterialvalorvendaBean(){}
	
	public MaterialvalorvendaBean(String index, Double valorvenda){
		this.index = index;
		this.valorvenda = valorvenda;
	}
	
	public String getIndex() {
		return index;
	}
	public Double getValorvenda() {
		return valorvenda;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
}
