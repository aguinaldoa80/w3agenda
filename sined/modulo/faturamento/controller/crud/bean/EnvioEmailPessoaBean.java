package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

public class EnvioEmailPessoaBean {

	private Boolean marcado = Boolean.TRUE;
	private Integer cdpessoa = null;
	private String nome = "";
	private String email = "";
	private Boolean receber_boleto = Boolean.FALSE;
	
	public EnvioEmailPessoaBean(){}
	
	public EnvioEmailPessoaBean(Boolean marcado, Integer cdpessoa, String nome, String email, Boolean receber_boleto) {
		this.marcado = marcado;
		this.cdpessoa = cdpessoa;
		this.nome = nome;
		this.email = email;
		this.receber_boleto = receber_boleto;
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getEmail() {
		return email;
	}
	public String getNome() {
		return nome;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public Boolean getReceber_boleto() {
		return receber_boleto;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setReceber_boleto(Boolean receberBoleto) {
		receber_boleto = receberBoleto;
	}
}
