package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;

public class PedidoItenBean {

	private Pedidovendamaterial pedidovendamaterial;
	private Integer cdproducaoagenda;
	private Integer situacao;
	
	public PedidoItenBean(Integer cdpedidovendamaterial, Integer cdproducaoagenda, Integer situacao, Integer cdpneu){
		Pedidovendamaterial pedidovendamaterial = new Pedidovendamaterial();
		pedidovendamaterial.setCdpedidovendamaterial(cdpedidovendamaterial);
		
		if(cdpneu != null){
			pedidovendamaterial.setPneu(new Pneu(cdpneu));
		}
		
		this.pedidovendamaterial = pedidovendamaterial;
		this.cdproducaoagenda = cdproducaoagenda;
		this.situacao = situacao;
	}

	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}

	public Integer getCdproducaoagenda() {
		return cdproducaoagenda;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}

	public void setCdproducaoagenda(Integer cdproducaoagenda) {
		this.cdproducaoagenda = cdproducaoagenda;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}
}
