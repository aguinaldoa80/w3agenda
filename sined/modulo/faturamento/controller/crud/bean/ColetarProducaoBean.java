package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;

public class ColetarProducaoBean {

	private Boolean registrarEntradaFiscal = Boolean.FALSE;
	private Boolean registrarNotaFiscal = Boolean.FALSE;
	private Boolean producaoautomatica = Boolean.FALSE;
	private Tipooperacaonota tipooperacaonota;
	private Arquivo arquivoxmlnfe;
	private String chaveacesso;
	private String htmlChaveacesso;
	private List<Pedidovenda> listaPedidovenda;
	
	private String whereInColeta;

	public List<Pedidovenda> getListaPedidovenda() {
		return listaPedidovenda;
	}
	@DisplayName("Registrar Entrada Fiscal")
	public Boolean getRegistrarEntradaFiscal() {
		return registrarEntradaFiscal;
	}
	public void setRegistrarEntradaFiscal(Boolean registrarEntradaFiscal) {
		this.registrarEntradaFiscal = registrarEntradaFiscal;
	}
	@DisplayName("Registrar Nota Fiscal")
	public Boolean getRegistrarNotaFiscal() {
		return registrarNotaFiscal;
	}
	@DisplayName("Chave de acesso")
	@MaxLength(44)
	public String getChaveacesso() {
		return chaveacesso;
	}
	public String getHtmlChaveacesso() {
		return htmlChaveacesso;
	}
	
	public void setHtmlChaveacesso(String htmlChaveacesso) {
		this.htmlChaveacesso = htmlChaveacesso;
	}
	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}
	public void setRegistrarNotaFiscal(Boolean registrarNotaFiscal) {
		this.registrarNotaFiscal = registrarNotaFiscal;
	}
	public void setListaPedidovenda(List<Pedidovenda> listaPedidovenda) {
		this.listaPedidovenda = listaPedidovenda;
	}
	public Arquivo getArquivoxmlnfe() {
		return arquivoxmlnfe;
	}
	public void setArquivoxmlnfe(Arquivo arquivoxmlnfe) {
		this.arquivoxmlnfe = arquivoxmlnfe;
	}
	public Boolean getProducaoautomatica() {
		return producaoautomatica;
	}
	public void setProducaoautomatica(Boolean producaoautomatica) {
		this.producaoautomatica = producaoautomatica;
	}
	
	@DisplayName("Tipo de opera��o")
	public Tipooperacaonota getTipooperacaonota() {
		return tipooperacaonota;
	}
	public void setTipooperacaonota(Tipooperacaonota tipooperacaonota) {
		this.tipooperacaonota = tipooperacaonota;
	}

	public String getWhereInColeta() {
		return whereInColeta;
	}
	public void setWhereInColeta(String whereInColeta) {
		this.whereInColeta = whereInColeta;
	}
}
