package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.List;

import br.com.linkcom.sined.util.SinedUtil;


public class ColetamaterialNotaBean {

	private Integer cdmaterial;
	private Integer cdpedidovendamaterial;
	private List<Integer> listaCdpedidovendamaterial;
	
	public ColetamaterialNotaBean(){}
	
	public ColetamaterialNotaBean(Integer cdmaterial, Integer cdpedidovendamaterial){
		this.cdmaterial = cdmaterial;
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	
	public ColetamaterialNotaBean(Integer cdmaterial, Integer cdpedidovendamaterial, List<Integer> listaCdpedidovendamaterial){
		this.cdmaterial = cdmaterial;
		this.cdpedidovendamaterial = cdpedidovendamaterial == null && SinedUtil.isListNotEmpty(listaCdpedidovendamaterial) ? listaCdpedidovendamaterial.get(0) : cdpedidovendamaterial;
		this.listaCdpedidovendamaterial = SinedUtil.isListNotEmpty(listaCdpedidovendamaterial) ? listaCdpedidovendamaterial : null;
	}
	
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmaterial == null) ? 0 : cdmaterial.hashCode());
		result = prime
				* result
				+ ((cdpedidovendamaterial == null) ? 0 : cdpedidovendamaterial
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColetamaterialNotaBean other = (ColetamaterialNotaBean) obj;
		if (cdmaterial == null) {
			if (other.cdmaterial != null)
				return false;
		} else if (!cdmaterial.equals(other.cdmaterial))
			return false;
		if (cdpedidovendamaterial == null) {
			if (other.cdpedidovendamaterial != null)
				return false;
		} else if (!cdpedidovendamaterial.equals(other.cdpedidovendamaterial))
			return false;
		return true;
	}
	
	public List<Integer> getListaCdpedidovendamaterial() {
		return listaCdpedidovendamaterial;
	}
	public void setListaCdpedidovendamaterial(List<Integer> listaCdpedidovendamaterial) {
		this.listaCdpedidovendamaterial = listaCdpedidovendamaterial;
	}
}
