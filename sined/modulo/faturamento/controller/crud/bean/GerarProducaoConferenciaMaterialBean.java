package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagendatipo;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Venda;

public class GerarProducaoConferenciaMaterialBean {

	private Empresa empresa;
	private Material material;
	private Double qtde;
	private Double qtdeEntrega;
	private Contrato contrato;
	private Cliente cliente;
	private Venda venda;
	private Pedidovendamaterial pedidovendamaterial;
	private Pneu pneu;
	private Pedidovenda pedidovenda;
	private Producaoetapa producaoetapa;
	private Producaoagendatipo producaoagendatipo;
	private List<GerarProducaoConferenciaMaterialItemBean> listaItem = new ListSet<GerarProducaoConferenciaMaterialItemBean>(GerarProducaoConferenciaMaterialItemBean.class);
	
	private Double largura;
	private Double altura;
	private Double comprimento;
	private String observacao;
	private Unidademedida unidademedida;
	private Unidademedida unidademedidaAux;
	private Date dtPrazoEntrega;
	
	public GerarProducaoConferenciaMaterialBean() {
	}
	
	public GerarProducaoConferenciaMaterialBean(Material material, Double qtde, Contrato contrato, Cliente cliente, Empresa empresa) {
		this.material = material;
		this.qtde = qtde;
		this.contrato = contrato;
		this.cliente = cliente;
		this.empresa = empresa;
	}
	
	public GerarProducaoConferenciaMaterialBean(Material material, Double qtde, Pedidovendamaterial pedidovendamaterial, Pedidovenda pedidovenda, Cliente cliente,
			Double largura, Double altura, Double comprimento, String observacao, Producaoetapa producaoetapa, Producaoagendatipo producaoagendatipo, Empresa empresa) {
		this.material = material;
		this.qtde = qtde;
		this.qtdeEntrega = qtde;
		this.pedidovendamaterial = pedidovendamaterial;
		this.pedidovenda = pedidovenda;
		this.cliente = cliente;
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
		this.observacao = observacao;
		this.producaoetapa = producaoetapa;
		this.producaoagendatipo = producaoagendatipo;
		this.empresa = empresa;
	}
	
	public GerarProducaoConferenciaMaterialBean(Material material, Double qtde, Pedidovendamaterial pedidovendamaterial, Pedidovenda pedidovenda, Cliente cliente,
			Double largura, Double altura, Double comprimento, String observacao, Producaoetapa producaoetapa, Producaoagendatipo producaoagendatipo, 
			Empresa empresa, Unidademedida unidademedida, Pneu pneu, Date dtPrazoEntrega) {
		this.material = material;
		this.qtde = qtde;
		this.qtdeEntrega = qtde;
		this.pedidovendamaterial = pedidovendamaterial;
		this.pedidovenda = pedidovenda;
		this.cliente = cliente;
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
		this.observacao = observacao;
		this.producaoetapa = producaoetapa;
		this.producaoagendatipo = producaoagendatipo;
		this.empresa = empresa;
		this.unidademedida = unidademedida;
		this.unidademedidaAux = unidademedida;
		this.pneu = pneu;
		this.dtPrazoEntrega = dtPrazoEntrega;
	}
	
	public GerarProducaoConferenciaMaterialBean(Material material, Double qtde, Venda venda, Cliente cliente,
			Double largura, Double altura, Double comprimento, Producaoetapa producaoetapa, 
			Producaoagendatipo producaoagendatipo, Empresa empresa, Unidademedida unidademedida, Pneu pneu , Date dtPrazoEntrega) {
		this.material = material;
		this.qtde = qtde;
		this.venda = venda;
		this.cliente = cliente;
		this.largura = largura;
		this.altura = altura;
		this.comprimento = comprimento;
		this.producaoetapa = producaoetapa;
		this.producaoagendatipo = producaoagendatipo;
		this.empresa = empresa;
		this.unidademedida = unidademedida;
		this.pneu = pneu;
		this.dtPrazoEntrega = dtPrazoEntrega;
	}

	public Material getMaterial() {
		return material;
	}
	public List<GerarProducaoConferenciaMaterialItemBean> getListaItem() {
		return listaItem;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getQtdeEntrega() {
		return qtdeEntrega;
	}
	public Contrato getContrato() {
		return contrato;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Venda getVenda() {
		return venda;
	}
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setQtdeEntrega(Double qtdeEntrega) {
		this.qtdeEntrega = qtdeEntrega;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setListaItem(
			List<GerarProducaoConferenciaMaterialItemBean> listaItem) {
		this.listaItem = listaItem;
	}

	public Double getLargura() {
		return largura;
	}
	public Double getAltura() {
		return altura;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public String getObservacao() {
		return observacao;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Unidademedida getUnidademedidaAux() {
		return unidademedidaAux;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public Producaoetapa getProducaoetapa() {
		return producaoetapa;
	}
	public Producaoagendatipo getProducaoagendatipo() {
		return producaoagendatipo;
	}

	public void setProducaoagendatipo(Producaoagendatipo producaoagendatipo) {
		this.producaoagendatipo = producaoagendatipo;
	}
	public void setProducaoetapa(Producaoetapa producaoetapa) {
		this.producaoetapa = producaoetapa;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setUnidademedidaAux(Unidademedida unidademedidaAux) {
		this.unidademedidaAux = unidademedidaAux;
	}

	public Pneu getPneu() {
		return pneu;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	
	public Date getDtPrazoEntrega() {
		return dtPrazoEntrega;
	}
	
	public void setDtPrazoEntrega(Date dtPrazoEntrega) {
		this.dtPrazoEntrega = dtPrazoEntrega;
	}
}
