package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Prazopagamento;


public class ParcelasCobrancaBean {

	private Date data;
	private Prazopagamento prazopagamento;
	private Empresa empresa;
	private Money valor;
	private Integer qtdParcela;
	
	public Date getData() {
		return data;
	}
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Money getValor() {
		return valor;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public Integer getQtdParcela() {
		return qtdParcela;
	}
	public void setQtdParcela(Integer qtdParcela) {
		this.qtdParcela = qtdParcela;
	}
}
