package br.com.linkcom.sined.modulo.faturamento.controller.crud.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;

public class ConferenciaMateriaisBean {

	private String ultimoMaterialConferido;
	
	private Double qtdeConferir = 1D;
	private String codigoBarras;
	private Double ultimaQtdeConferida;
	private Boolean conferenciaCega = false;
	
	private Set<ConferenciaMateriaisVendaBean> listaConferencia =  new ListSet<ConferenciaMateriaisVendaBean>(ConferenciaMateriaisVendaBean.class);
	private String materialAndUnidade;
	
	
	@DisplayName("Qtde. a conferir")
	public Double getQtdeConferir() {
		return qtdeConferir;
	}
	@DisplayName("C�digo de barras")
	public String getCodigoBarras() {
		return codigoBarras;
	}
	
	public void setQtdeConferir(Double qtdeConferir) {
		this.qtdeConferir = qtdeConferir;
	}
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}
	
	@DisplayName("�ltimo material conferido")
	public String getUltimoMaterialConferido() {
		return ultimoMaterialConferido;
	}
	@DisplayName("�ltima qtde. conferida")
	public Double getUltimaQtdeConferida() {
		return ultimaQtdeConferida;
	}
	public Boolean getConferenciaCega() {
		return conferenciaCega;
	}
	public void setConferenciaCega(Boolean conferenciaCega) {
		this.conferenciaCega = conferenciaCega;
	}
	public void setUltimoMaterialConferido(String ultimoMaterialConferido) {
		this.ultimoMaterialConferido = ultimoMaterialConferido;
	}
	public void setUltimaQtdeConferida(Double ultimaQtdeConferida) {
		this.ultimaQtdeConferida = ultimaQtdeConferida;
	}
	public Set<ConferenciaMateriaisVendaBean> getListaConferencia() {
		if(listaConferencia == null){
			listaConferencia = new ListSet<ConferenciaMateriaisVendaBean>(ConferenciaMateriaisVendaBean.class);
		}
		return listaConferencia;
	}
	public void setListaConferencia(Set<ConferenciaMateriaisVendaBean> listaConferencia) {
		this.listaConferencia = listaConferencia;
	}
	public String getMaterialAndUnidade() {
		return materialAndUnidade;
	}
	public void setMaterialAndUnidade(String materialAndUnidade) {
		this.materialAndUnidade = materialAndUnidade;
	}
}