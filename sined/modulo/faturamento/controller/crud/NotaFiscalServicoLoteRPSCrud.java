package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoLoteRPS;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoLoteRPSFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/NotaFiscalServicoLoteRPS", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"arquivo", "arquivotomador", "datacriacao"})
public class NotaFiscalServicoLoteRPSCrud extends CrudControllerSined<NotaFiscalServicoLoteRPSFiltro, NotaFiscalServicoLoteRPS, NotaFiscalServicoLoteRPS> {
	
	@Override
	protected void salvar(WebRequestContext request, NotaFiscalServicoLoteRPS bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, NotaFiscalServicoLoteRPS bean) throws Exception {
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
		} else super.excluir(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, NotaFiscalServicoLoteRPS form) throws Exception {
		if(form.getArquivo() != null && form.getArquivo().getCdarquivo() != null){
			DownloadFileServlet.addCdfile(request.getSession(), form.getArquivo().getCdarquivo());
		}
		if(form.getArquivotomador() != null && form.getArquivotomador().getCdarquivo() != null){
			DownloadFileServlet.addCdfile(request.getSession(), form.getArquivotomador().getCdarquivo());
		}
	}
	
	@Override
	protected ListagemResult<NotaFiscalServicoLoteRPS> getLista(WebRequestContext request, NotaFiscalServicoLoteRPSFiltro filtro) {
		ListagemResult<NotaFiscalServicoLoteRPS> listagemResult = super.getLista(request, filtro);
		List<NotaFiscalServicoLoteRPS> list = listagemResult.list();
		
		for (NotaFiscalServicoLoteRPS loteRPS : list) {
			if(loteRPS.getArquivo() != null && loteRPS.getArquivo().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), loteRPS.getArquivo().getCdarquivo());
			}
			if(loteRPS.getArquivotomador() != null && loteRPS.getArquivotomador().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), loteRPS.getArquivotomador().getCdarquivo());
			}
		}
		
		return listagemResult;
	}
	
	
}
