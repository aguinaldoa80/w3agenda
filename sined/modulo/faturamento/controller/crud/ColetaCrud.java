package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.Coletahistorico;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EntregadocumentoColeta;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.NotafiscalprodutoColeta;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.bean.enumeration.LabelColoboradorFiltro;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.service.ClientevendedorService;
import br.com.linkcom.sined.geral.service.ColetaMaterialService;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.ColetahistoricoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueorigemService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoColetaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.ProducaoordemService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ColetarProducaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ColetaFiltro;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ConsumoMateriaprimaProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/Coleta", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "")
public class ColetaCrud extends CrudControllerSined<ColetaFiltro, Coleta, Coleta> {

	
	private ColetaService coletaService;
	private ProducaoordemService producaoordemService;
	private MaterialService materialService;	
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ColetaMaterialService coletaMaterialService;
	private DocumentoorigemService documentoorigemService;
	private ColetahistoricoService coletahistoricoService;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private PedidovendaService pedidovendaService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private NotafiscalprodutoColetaService notafiscalprodutoColetaService;
	private PneuService pneuService;
	private ContagerencialService contagerencialService;
	private ProjetoService projetoService;
	private ContaService contaService;
	private ParametrogeralService parametrogeralService;
	private ClientevendedorService clienteVendedorService;
	private EmpresaService empresaService;
	
	public void setClienteVendedorService(ClientevendedorService clienteVendedorService) {this.clienteVendedorService = clienteVendedorService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setNotafiscalprodutoColetaService(NotafiscalprodutoColetaService notafiscalprodutoColetaService) {this.notafiscalprodutoColetaService = notafiscalprodutoColetaService;}
	public void setPedidovendahistoricoService(
			PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setColetahistoricoService(
			ColetahistoricoService coletahistoricoService) {
		this.coletahistoricoService = coletahistoricoService;
	}
	public void setDocumentoorigemService(
			DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setColetaMaterialService(
			ColetaMaterialService coletaMaterialService) {
		this.coletaMaterialService = coletaMaterialService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setColetaService(ColetaService coletaService) {
		this.coletaService = coletaService;
	}
	public void setProducaoordemService(ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}
	public void setMovimentacaoestoqueorigemService(MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {
		this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Coleta form) throws Exception {
		if(form != null && form.getCdcoleta() != null){
			form.setListaColetahistorico(coletahistoricoService.findByColeta(form));
			
			List<OrigemsuprimentosBean> listaInfo = new ListSet<OrigemsuprimentosBean>(OrigemsuprimentosBean.class);
			if(form.getListaEntregadocumentoColeta() != null && !form.getListaEntregadocumentoColeta().isEmpty()){
				for(EntregadocumentoColeta edc : form.getListaEntregadocumentoColeta()){
					listaInfo.add(new OrigemsuprimentosBean(OrigemsuprimentosBean.ENTRADASAIDA, "ENTRADA FISCAL", edc.getEntregadocumento().getCdentregadocumento().toString(), null));
				}
			}
			if(form.getListaNotafiscalprodutoColeta() != null && !form.getListaNotafiscalprodutoColeta().isEmpty()){
				List<OrigemsuprimentosBean> listaDevolucao = new ListSet<OrigemsuprimentosBean>(OrigemsuprimentosBean.class);
				for(NotafiscalprodutoColeta nfpc : form.getListaNotafiscalprodutoColeta()){
					listaDevolucao.add(new OrigemsuprimentosBean(OrigemsuprimentosBean.NOTA, coletaService.getDescricaoOrigem(nfpc), nfpc.getNotafiscalproduto().getCdNota().toString(), null));
				}
				listaInfo.addAll(listaDevolucao);
			}
			if(form.getPedidovenda() != null && form.getPedidovenda().getCdpedidovenda() != null){
				Pedidovenda pedido = pedidovendaService.load(form.getPedidovenda(), "pedidovenda.cdpedidovenda, pedidovenda.origemOtr");
				if(Boolean.TRUE.equals(pedido.getOrigemOtr())){
					listaInfo.add(new OrigemsuprimentosBean(OrigemsuprimentosBean.PEDIDOVENDA_OTR, "PEDIDO VENDA",
							form.getPedidovenda().getCdpedidovenda().toString(),
							Util.strings.emptyIfNull(form.getPedidovenda().getIdentificador()).isEmpty()?
									form.getPedidovenda().getCdpedidovenda().toString():
									form.getPedidovenda().getIdentificador()));
				}else{
					listaInfo.add(new OrigemsuprimentosBean(OrigemsuprimentosBean.PEDIDOVENDA, "PEDIDO VENDA",
							form.getPedidovenda().getCdpedidovenda().toString(),
							Util.strings.emptyIfNull(form.getPedidovenda().getIdentificador()).isEmpty()?
									form.getPedidovenda().getCdpedidovenda().toString():
									form.getPedidovenda().getIdentificador()));
				}
				
			}
			request.setAttribute("listaInfo", listaInfo);
			request.setAttribute("isRepagem", SinedUtil.isRecapagem());
			boolean isExpedicaoColeta = false;
			if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("COLETA")){
				isExpedicaoColeta = true;
			}
			request.setAttribute("isExpedicaoColeta", isExpedicaoColeta);
		}
		request.setAttribute("HABILITAR_CONFIGURACAO_OTR", parametrogeralService.getValorPorNome(Parametrogeral.HABILITAR_CONFIGURACAO_OTR).trim().toUpperCase());
	}
	
	@Override
	protected ListagemResult<Coleta> getLista(WebRequestContext request, ColetaFiltro filtro) {
		ListagemResult<Coleta> listagemResult = super.getLista(request, filtro);
		coletaService.processarListagem(listagemResult, filtro, true);
		
		return listagemResult;
	}
	
	@Override
	protected void validateBean(Coleta bean, BindException errors) {
		coletaService.validaObrigatoriedadePneu(bean, null, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Coleta bean) throws Exception {
		boolean isCriar = bean.getCdcoleta() == null;
		List<ColetaMaterial> listaColetaMaterialAnterior = new ArrayList<ColetaMaterial>();

		if(!isCriar){
			listaColetaMaterialAnterior = coletaMaterialService.findByColeta(bean.getCdcoleta().toString());
		}
		
		if(bean.getListaColetaMaterial() != null && bean.getListaColetaMaterial().size() > 0){
			for (ColetaMaterial item : bean.getListaColetaMaterial()) {
				if(item.getPneu() != null){
					if(item.getPneu().existeDados()){
						pneuService.saveOrUpdate(item.getPneu());
					} else {
						item.setPneu(null);
					}
				}
			}
		}
		try{
			super.salvar(request, bean);
		}catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new SinedException ("Erro de integridade do banco, verifique se existe uma expedi��o que contenha refer�ncia para a coleta");
		}
		Coletahistorico coletahistorico = new Coletahistorico();
		
		if(isCriar){
			if(parametrogeralService.getValorPorNome("EXPEDICAO_RECAPAGEM").equals("COLETA")){
				coletaService.updateSituacaoColeta(bean, SituacaoVinculoExpedicao.PENDENTE,null,null,null);
			}
			salvaInclusaoColeta(request, bean);
			request.getSession().setAttribute("whereInCdcoletamaterialNovo", "");
		} else {
			List<ColetaMaterial> listaColetaMaterialAtual = bean.getListaColetaMaterial();
			
			if(listaColetaMaterialAnterior != null && !listaColetaMaterialAnterior.isEmpty()){
				for(ColetaMaterial item : listaColetaMaterialAnterior){
					ColetaMaterial coletaMaterial = containsColetaMaterial(listaColetaMaterialAtual, item, null);
					if(coletaMaterial == null){
						Movimentacaoestoque movimentacaoestoque = movimentacaoestoqueService.findByColetaAvulsaAndMaterial(item.getColeta(), item.getMaterial());
						item.setObservacao("(Cancelada devido a exclus�o do item na coleta)\n" + (StringUtils.isNotBlank(item.getObservacao()) ? item.getObservacao() : ""));
						if(movimentacaoestoque != null){
							movimentacaoestoqueService.updateDataCancelamentoMovimentacao(movimentacaoestoque.getCdmovimentacaoestoque(), SinedDateUtils.currentDate(), item.getObservacao());
							String observacao = "Movimenta��o cancelada <a href=\"/w3erp/suprimento/crud/Movimentacaoestoque?ACAO=consultar&cdmovimentacaoestoque=" + 
							movimentacaoestoque.getCdmovimentacaoestoque() +"\">"+ movimentacaoestoque.getCdmovimentacaoestoque()+"</a> devido a exclus�o do item na coleta.";
							coletahistorico.setObservacao(StringUtils.isNotBlank(coletahistorico.getObservacao()) ? coletahistorico.getObservacao() + "<br />" + observacao : observacao);
						}
					}
				}
			}
			
			List<ColetaMaterial> listaColetaMaterialAux = new ArrayList<ColetaMaterial>();
			if(listaColetaMaterialAtual != null && !listaColetaMaterialAtual.isEmpty()){
				for(ColetaMaterial item : listaColetaMaterialAtual){
					ColetaMaterial coletaMaterial = containsColetaMaterial(listaColetaMaterialAnterior, item, null);
					if(coletaMaterial != null && (!item.getQuantidade().equals(coletaMaterial.getQuantidade()) 
							|| (item.getValorunitario() != null && !item.getValorunitario().equals(coletaMaterial.getValorunitario())))){
						Movimentacaoestoque movimentacaoestoque = movimentacaoestoqueService.findByColetaAvulsaAndMaterial(item.getColeta(), item.getMaterial());
						if(movimentacaoestoque != null && movimentacaoestoque.getCdmovimentacaoestoque() != null){
							movimentacaoestoqueService.updateQuantidadeMovimentacao(movimentacaoestoque.getCdmovimentacaoestoque(), item.getQuantidade(), item.getValorunitario());
							String observacao = "Movimenta��o alterada: <a href=\"/w3erp/suprimento/crud/Movimentacaoestoque?ACAO=consultar&cdmovimentacaoestoque=" + 
							movimentacaoestoque.getCdmovimentacaoestoque() +"\">"+ movimentacaoestoque.getCdmovimentacaoestoque()+"</a>.";
							coletahistorico.setObservacao(StringUtils.isNotBlank(coletahistorico.getObservacao()) ? coletahistorico.getObservacao() + "<br />" + observacao : observacao);
						}
					}
					if(coletaMaterial == null){
						listaColetaMaterialAux.add(item);
					}
				}
			}
			
			String whereInCdcoletamaterialNovo = "";
			if(listaColetaMaterialAux != null && !listaColetaMaterialAux.isEmpty()){
				whereInCdcoletamaterialNovo = SinedUtil.listAndConcatenate(listaColetaMaterialAux, "cdcoletamaterial", ","); 
			}
			if(StringUtils.isNotBlank(whereInCdcoletamaterialNovo)){
				request.getSession().setAttribute("whereInCdcoletamaterialNovo", whereInCdcoletamaterialNovo);
				request.getSession().setAttribute("fromEditar", true);
			}
		}
		coletahistorico.setAcao(isCriar ? Coletaacao.CRIADO : Coletaacao.ALTERADO);
		coletahistorico.setColeta(bean);
		coletahistoricoService.saveOrUpdate(coletahistorico);
	}
	
	private void salvaInclusaoColeta(WebRequestContext request, Coleta coleta){
		List<ColetaMaterial> listaColetamaterialAux = new ArrayList<ColetaMaterial>();
		coleta = coletaService.loadForEntrada(coleta);
		
		Boolean fromEditar = (Boolean) request.getSession().getAttribute("fromEditar");
		fromEditar = (fromEditar != null ? fromEditar : false);
		String whereInCdcoletamaterialNovo = (String) request.getSession().getAttribute("whereInCdcoletamaterialNovo");
		request.getSession().removeAttribute("fromEditar");
		request.getSession().removeAttribute("whereInCdcoletamaterialNovo");
		List<String> listaCdcoletamaterial = new ArrayList<String>();
		if(StringUtils.isNotBlank(whereInCdcoletamaterialNovo)){
			listaCdcoletamaterial = Arrays.asList(whereInCdcoletamaterialNovo.split(","));
		}
		if(!fromEditar){
			listaColetamaterialAux = coleta.getListaColetaMaterial();
		}
		
		if(listaCdcoletamaterial != null && !listaCdcoletamaterial.isEmpty()){
			for(String cdcoletamaterial : listaCdcoletamaterial){
				ColetaMaterial coletaMaterial = containsColetaMaterial(coleta.getListaColetaMaterial(), null, Integer.parseInt(cdcoletamaterial));
				if(coletaMaterial != null && coletaMaterial.getCdcoletamaterial() != null){
					listaColetamaterialAux.add(coletaMaterial);
				}
			}
		}
		
		if(listaColetamaterialAux != null && !listaColetamaterialAux.isEmpty()){
			List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
			StringBuilder linkMe = new StringBuilder();
			for(ColetaMaterial item : listaColetamaterialAux){
				if(item.getQuantidade() != null && item.getQuantidade() > 0){
					Movimentacaoestoque me = new Movimentacaoestoque();
					me.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
					me.setMaterial(item.getMaterial());
					me.setMaterialclasse(Materialclasse.PRODUTO);
					me.setQtde(item.getQuantidade());
					me.setEmpresa(coleta.getEmpresa());
					me.setLocalarmazenagem(item.getLocalarmazenagem());
					me.setValor(item.getValorunitario());
					me.setObservacao(coleta.getObservacao());
					me.setDtmovimentacao(new java.sql.Date(System.currentTimeMillis()));
					me.setObservacao(item.getObservacao());
					listaMovimentacaoestoque.add(me);
				}
			}
			if(listaMovimentacaoestoque != null && !listaMovimentacaoestoque.isEmpty()){
				for (Movimentacaoestoque me : listaMovimentacaoestoque) {
					Movimentacaoestoqueorigem meo = new Movimentacaoestoqueorigem();
					meo.setDtaltera(new Timestamp(System.currentTimeMillis()));
					meo.setColeta(coleta);
					movimentacaoestoqueorigemService.saveOrUpdateNoUseTransaction(meo);
					
					me.setMovimentacaoestoqueorigem(meo);
					movimentacaoestoqueService.saveOrUpdateNoUseTransaction(me);
					
					if(!linkMe.toString().equals("")) linkMe.append(",");
					linkMe.append(movimentacaoestoqueService.makeLinkHistoricoMovimentacaoestoque(me));
					
					Coletahistorico coletahistorico = new Coletahistorico();
					coletahistorico.setAcao(Coletaacao.ENTRADA_ESTOQUE);
					coletahistorico.setColeta(coleta);
					coletahistorico.setObservacao("Entrada: <a href=\"/w3erp/suprimento/crud/Movimentacaoestoque?ACAO=consultar&cdmovimentacaoestoque=" + 
													me.getCdmovimentacaoestoque() +"\">"+ me.getCdmovimentacaoestoque()+"</a>");
					coletahistoricoService.saveOrUpdate(coletahistorico);
				}
			}
		}
	}
	
	/**
	 * M�toque que verifica a exist�ncia de coletaMaterial em uma lista informada
	 * @param listaColetamaterial
	 * @param coletaMaterial
	 * @param cdcoletamaterial
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	private ColetaMaterial containsColetaMaterial(List<ColetaMaterial> listaColetamaterial, ColetaMaterial coletaMaterial, Integer cdcoletamaterial){
		if(listaColetamaterial == null || listaColetamaterial.isEmpty())
			return null;
		
		if(coletaMaterial != null){
			for(ColetaMaterial item : listaColetamaterial){
				if(item.getMaterial().equals(coletaMaterial.getMaterial())){
					return item;
				}
			}
		} else if(cdcoletamaterial != null){
			for(ColetaMaterial item : listaColetamaterial){
				if(item.getCdcoletamaterial().equals(cdcoletamaterial)){
					return item;
				}
			}
		}
		return null;
	}
	
	@Override
	protected void excluir(WebRequestContext request, Coleta bean) throws Exception {
		String whereIn = request.getParameter("itenstodelete");
		List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
		List<Pedidovenda> listaPedidovenda = new ArrayList<Pedidovenda>();
		
		if(StringUtils.isNotBlank(whereIn)){
			listaMovimentacaoestoque = movimentacaoestoqueService.findByColeta(whereIn);
			listaPedidovenda = pedidovendaService.findByColeta(whereIn);
		} else {
			listaMovimentacaoestoque = movimentacaoestoqueService.findByColeta(bean.getCdcoleta().toString());
			listaPedidovenda = pedidovendaService.findByColeta(bean.getCdcoleta().toString());
		}
		
		super.excluir(request, bean);
		
		if(listaMovimentacaoestoque != null && !listaMovimentacaoestoque.isEmpty()){
			for(Movimentacaoestoque item : listaMovimentacaoestoque){
				if(item != null && item.getCdmovimentacaoestoque() != null){
					item.setObservacao("(Cancelada devido a exclus�o da coleta)\n" + (StringUtils.isNotBlank(item.getObservacao()) ? item.getObservacao() : ""));
					movimentacaoestoqueService.updateDataCancelamentoMovimentacao(item.getCdmovimentacaoestoque(), SinedDateUtils.currentDate(), item.getObservacao());
				}
			}
		}
		
		if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
			String ids = "n�o identificadas";
			if(StringUtils.isNotBlank(whereIn)){
				ids = whereIn;
			} else {
				ids = bean.getCdcoleta().toString();
			}
			
			for (Pedidovenda pedidovenda : listaPedidovenda) {
				Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
				pedidovendahistorico.setAcao("Coleta Exclu�da");
				pedidovendahistorico.setObservacao("Coleta(s) " + ids + " exclu�da(s).");
				pedidovendahistorico.setPedidovenda(pedidovenda);
				pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
			}
		}
	}
	
	public ModelAndView estornarDevolucao(WebRequestContext request) throws Exception{
		String whereInProducaoordem = SinedUtil.getItensSelecionados(request);
		if(whereInProducaoordem == null || whereInProducaoordem.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Boolean entradaproducaoordem = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("entradaproducaoordem")) ? request.getParameter("entradaproducaoordem") : "false");  
		String urlRetorno = "/producao/crud/Producaoordem" + (entradaproducaoordem ? "?ACAO=consultar&cdproducaoordem=" + whereInProducaoordem : "");
		
		StringBuilder whereInPedidovenda = new StringBuilder();
		StringBuilder whereInPedidovendamaterial = new StringBuilder();
		List<Producaoordem> listaProducaoordemValidacao = producaoordemService.findForDevolverColeta(whereInProducaoordem);	
		for (Producaoordem producaoordem : listaProducaoordemValidacao) {
			Producaoordemsituacao producaoordemsituacao = producaoordem.getProducaoordemsituacao();
			if(producaoordemsituacao == null || !producaoordemsituacao.equals(Producaoordemsituacao.CANCELADA)){
				request.addError("Somente para situa��o Cancelada.");
				return new ModelAndView("redirect:" + urlRetorno);
			}
			
			if(producaoordem.getListaProducaoordemmaterial() != null){
				for(Producaoordemmaterial pom : producaoordem.getListaProducaoordemmaterial()){
					if(pom.getListaProducaoordemmaterialorigem() != null){
						for(Producaoordemmaterialorigem pomo : pom.getListaProducaoordemmaterialorigem()){
							if(pomo.getProducaoagenda() != null && pomo.getProducaoagenda().getPedidovenda() != null 
									&& pomo.getProducaoagenda().getPedidovenda().getCdpedidovenda() != null){
								whereInPedidovenda.append(pomo.getProducaoagenda().getPedidovenda().getCdpedidovenda()).append(",");
							}
						}
					}
					if(pom.getPedidovendamaterial() != null && pom.getPedidovendamaterial().getCdpedidovendamaterial() != null){
						whereInPedidovendamaterial.append(pom.getPedidovendamaterial().getCdpedidovendamaterial()).append(",");
					}
				}
			}
		}
		
		if(whereInPedidovenda.toString().isEmpty()){
			request.addError("N�o existem pedidos de venda vinculados �(s) Ordem(ns) de produ��o selecionada(s).");
			return new ModelAndView("redirect:" + urlRetorno);
		}
		
		
		List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPedidovenda.substring(0, whereInPedidovenda.length()-1));
		if(listaColeta == null || listaColeta.isEmpty()){
			request.addError("N�o existem coletas v�lidas para a(s) Ordem(ns) de produ��o selecionada(s).");
			return new ModelAndView("redirect:" + urlRetorno);
		}
		
		String whereInColeta = SinedUtil.listAndConcatenate(listaColeta, "cdcoleta", ",");
		
		if(notafiscalprodutoColetaService.haveNotaDevolucaoNaoCanceladaByColeta(whereInColeta, whereInPedidovendamaterial.substring(0, whereInPedidovendamaterial.length()-1))){
			request.addError("� necess�rio cancelar a Nota Fiscal de Produto da devolu��o, antes de estornar a devolu��o da coleta.");
			return new ModelAndView("redirect:" + urlRetorno);
		}
		
		listaColeta = coletaService.loadForGerarNotafiscalproduto(whereInColeta, whereInPedidovendamaterial.substring(0, whereInPedidovendamaterial.length()-1));
		if(listaColeta == null || listaColeta.isEmpty()){
			request.addError("N�o existe(m) coleta(s) v�lida(s) gerar devolu��o(�es).");
			return new ModelAndView("redirect:" + urlRetorno);
		}
		
		Coleta coleta = new Coleta();
		coleta.setListaColetaMaterial(coletaService.prepareForEstornarDevolucaoColeta(listaColeta));
		coleta.setWhereIn(whereInColeta);
		coleta.setWhereInProducaoordem(CollectionsUtil.listAndConcatenate(listaProducaoordemValidacao, "cdproducaoordem", ","));
		
		return new ModelAndView("process/estornarDevolucao", "coleta", coleta);
	}
	
	public ModelAndView salvarEstornarDevolucao(WebRequestContext request, Coleta bean){
		return coletaService.salvarEstornarDevolucao(request, bean);
	}
	
	/**
	 * Action que prepara os dados para abrir a tela de confirma��o do registro de devolu��o.
	 *
	 * @param request
	 * @return
	 * @author Rafael Salvio
	 * @throws Exception 
	 */
	public ModelAndView registrarDevolucao(WebRequestContext request) throws Exception{
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"); 
		String whereInColeta = SinedUtil.getItensSelecionados(request);
		String urlRetorno = "/faturamento/crud/Coleta" + (entrada ? "?ACAO=consultar&cdcoleta="+whereInColeta : "");
		
		if(whereInColeta == null || whereInColeta.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Integer> listaCdPedidovendamaterial = new ArrayList<Integer>();
		StringBuilder msgDevolucao = new StringBuilder();
		StringBuilder msgCancelada = new StringBuilder();
		
		List<Producaoordem> listaProducaoordemValidacao = producaoordemService.findForDevolverColetaByColeta(whereInColeta);
		boolean erro = false;
		boolean existeItemParaDevolver = false;
		for (Producaoordem producaoordem : listaProducaoordemValidacao) {
			if(producaoordem.getProducaoordemsituacao() == null || 
					(!producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ESPERA) 
						&& !producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.EM_ANDAMENTO)) ){
				if(producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.CONCLUIDA)){
					msgDevolucao.append(producaoordem.getCdproducaoordem()).append(",");
				}else if(producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.CANCELADA)){
					msgCancelada.append(producaoordem.getCdproducaoordem()).append(",");
				}
				erro = true;
			} else {
				existeItemParaDevolver = true;
			}
		}
		if(erro && !existeItemParaDevolver){
			if(msgDevolucao.length() > 0){
				request.addError("N�o h� material dispon�vel para devolu��o, a(s) ordem(ns) de produ��o " + msgDevolucao.substring(0, msgDevolucao.length()-1) + " j� est�(�o) conclu�da(s).");
			}
			if(msgCancelada.length() > 0){
				request.addError("N�o h� material dispon�vel para devolu��o, a(s) ordem(ns) de produ��o " + msgCancelada.substring(0, msgCancelada.length()-1) + " est�(�o) canceladas(s).");
			}
			SinedUtil.redirecionamento(request, urlRetorno);
			return null;
		}
		
		List<Coleta> listaColeta = null;
		try {
			listaColeta = coletaService.findForRegistrarDevolucao(whereInColeta, listaCdPedidovendamaterial);
			producaoordemService.setInformacoesProducaoordemmaterial(listaProducaoordemValidacao, listaColeta);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.redirecionamento(request, urlRetorno);
			return null;
		}
		
		ProducaoordemdevolucaoBean bean = producaoordemService.criaProducaoordemdevolucaoBean(null, listaColeta, null, whereInColeta);
		bean.setUrlretorno(urlRetorno);
		request.setAttribute("existeColeta", true);
		return new ModelAndView("process/registrarDevolucao", "bean", bean);
	}
	
	public ModelAndView salvarRegistrarDevolucao(WebRequestContext request, ProducaoordemdevolucaoBean bean){
		boolean isNaoRegistrarMovimentacao = false;
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("COLETA") && SinedUtil.isRecapagem()){
			isNaoRegistrarMovimentacao = true;
		}
		
		
		return producaoordemService.salvarRegistrarDevolucao(request, bean,isNaoRegistrarMovimentacao);
	}
	
	public ModelAndView abrirPopUpConsumoMateriaprimaAposCriarNotaDevolucao(WebRequestContext request) {
		return producaoordemService.abrirPopUpConsumoMateriaprimaAposCriarNotaDevolucao(request);
	}
	
	/**
	 * Ajax para carregar o valor de custo do material no item da coleta
	 *
	 * @param request
	 * @param coletaMaterial
	 * @author Rodrigo Freitas
	 * @since 23/07/2014
	 */
	public void ajaxCarregaValorcusto(WebRequestContext request, ColetaMaterial coletaMaterial){
		if(coletaMaterial.getMaterial() == null || coletaMaterial.getMaterial().getCdmaterial() == null){
			View.getCurrent().println("alert('Material n�o informado.');");
			return;
		}
		
		Material material = materialService.load(coletaMaterial.getMaterial(), "material.cdmaterial, material.valorcusto");
		View.getCurrent().println("var valorcusto = '" + (material.getValorcusto() != null ? SinedUtil.descriptionDecimal(material.getValorcusto()) : "") + "';");
	}
	
	/**
	 * M�todo que abre a tela de conta a pagar para gera��o do pagamento de coleta
	 *
	 * @param request
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/07/2014
	 */
	public ModelAndView gerarPagamento(WebRequestContext request, Coleta coleta){
		if(coleta == null || coleta.getCdcoleta() == null){
			throw new SinedException("Nenhuma coleta selecionada.");
		}
		
		if(documentoorigemService.haveDocumentoByColetaNotCancelada(coleta)){
			request.addError("Pagamento j� gerado para esta coleta.");
			return new ModelAndView("redirect:/faturamento/crud/Coleta?ACAO=consultar&cdcoleta=" + coleta.getCdcoleta());
		}
		
		return new ModelAndView("redirect:/financeiro/crud/Contapagar?ACAO=criar&cdcoleta=" + coleta.getCdcoleta());
	}
	
	/**
	 * M�todo que abre a popup para a confirma��o das devolu��es
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public ModelAndView abrirPopUpConfirmacaoDevolucao(WebRequestContext request){
		try{
			String whereIn = SinedUtil.getItensSelecionados(request);
			if(whereIn == null || whereIn.equals("")){
				throw new SinedException("Nenhum item selecionado.");
			}
			boolean existeQtdeDevolvidaSemNota = false;
			boolean existeQtdeDevolvida = false;
			
			List<ColetaMaterial> listaColetaMaterial = coletaMaterialService.findByColeta(whereIn);
			for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
				if(coletaMaterial.getQuantidade() == null) coletaMaterial.setQuantidade(0d);
				if(coletaMaterial.getQuantidadedevolvida() == null) coletaMaterial.setQuantidadedevolvida(0d);
				if(coletaMaterial.getQuantidadedevolvidanota() == null) coletaMaterial.setQuantidadedevolvidanota(0d);
				
				if(coletaMaterial.getQuantidadedevolvida() != null && coletaMaterial.getQuantidadedevolvida() > 0){
					existeQtdeDevolvida = true;
				}
				
				if(coletaMaterial.getQuantidadedevolvida() - coletaMaterial.getQuantidadedevolvidanota() > 0){
					existeQtdeDevolvidaSemNota = true;
					break;
				}
			}
			
			if(!existeQtdeDevolvida){
				request.addError("� necess�rio pelo menos uma devolu��o registrada para confirma��o da devolu��o.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			request.setAttribute("existeQtdeDevolvidaSemNota", existeQtdeDevolvidaSemNota);
			Coleta coleta = new Coleta();
			coleta.setGerarNota(existeQtdeDevolvidaSemNota);
			coleta.setListaColetaMaterial(listaColetaMaterial);
			coleta.setWhereIn(whereIn);
			return new ModelAndView("direct:/crud/popup/popUpConfirmacaoDevolucao", "bean", coleta);
		} catch (Exception e) {
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	/**
	 * M�todo que faz a confirma��o da devolu��o de coletas
	 *
	 * @param request
	 * @param coleta
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public void saveConfirmacaoDevolucao(WebRequestContext request, Coleta coleta){
		String whereIn = coleta.getWhereIn();
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		List<Integer> listaCdPedidovendamaterial = new ArrayList<Integer>();
		List<Coleta> listaColeta = coletaService.findForRegistrarDevolucao(whereIn, listaCdPedidovendamaterial);
		String devolucaoColetaConfirmada = "true";
		for(Coleta col: listaColeta){
			if(!col.getDevolucaoConfirmada()){
				devolucaoColetaConfirmada = "false";
			}
		}
		coletaService.updateDtConfirmacaoDevolucao(whereIn, SinedDateUtils.currentDate());
		
		String[] ids = whereIn.split(",");
		for (int i = 0; i < ids.length; i++) {
			Coletahistorico coletahistorico = new Coletahistorico();
			coletahistorico.setAcao(Coletaacao.CONFIRMADO);
			coletahistorico.setColeta(new Coleta(Integer.parseInt(ids[i])));
			coletahistoricoService.saveOrUpdate(coletahistorico);
		}
		
		if(Boolean.TRUE.equals(coleta.getGerarNota())){
			request.getServletResponse().setContentType("text/html");
			String script = "<script>" +
							"parent.abreTelaNota("+devolucaoColetaConfirmada+","+whereIn+");"+
							"</script>";
			View.getCurrent().println(script);
		}else{
			request.addMessage("Devolu��o(�es) de coleta(s) confirmada(s) com sucesso.");
			SinedUtil.fechaPopUp(request);
		}
		//return null;
		
	}
	
	/**
	 * Action que abre a popup da a��o de comprar do cliente, mostrando apenas os itens que tem quantidades a serem compradas.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/08/2014
	 */
	public ModelAndView abrirPopUpComprarCliente(WebRequestContext request){
		try{
			String whereIn = SinedUtil.getItensSelecionados(request);
			if(whereIn == null || whereIn.equals("")){
				throw new SinedException("Nenhum item selecionado.");
			}
			
			Coleta coleta = coletaMaterialService.makeListaColetaMaterialForCompra(whereIn);
			request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
			request.setAttribute("listaProjetoUsuarioNotCancelado", projetoService.findForComboByUsuariologadoNotCancelado());
			return new ModelAndView("direct:/crud/popup/popUpComprarCliente", "bean", coleta);
		} catch (Exception e) {
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	/**
	 * Action que salva a compra do cliente
	 *
	 * @param request
	 * @param coleta
	 * @author Rodrigo Freitas
	 * @since 20/08/2014
	 */
	public void saveComprarCliente(WebRequestContext request, Coleta coleta){
		String whereIn = coleta.getWhereIn();
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<ColetaMaterial> listaColetaMaterial = coleta.getListaColetaMaterial();
		Documento documento = null;
		List<Valecompra> listaValecompra = null;
		if(listaColetaMaterial != null && !listaColetaMaterial.isEmpty()){
			for(ColetaMaterial cm : listaColetaMaterial){
				if(cm.getQuantidaderestante() != null && cm.getQuantidaderestante() > 0){
					coletaMaterialService.adicionaQtdeComprada(cm, cm.getQuantidaderestante());
				}
			}
			if(coleta.getRadFormacompra() != null && !coleta.getRadFormacompra()){
				documento = coletaService.inserirContapagar(request, coleta, whereIn);
			}else if(coleta.getRadFormacompra() == null){
				listaValecompra = coletaService.inserirValecompra(coleta);
			} 
		}
		
		String[] ids = whereIn.split(",");
		for (int i = 0; i < ids.length; i++) {
			Coletahistorico coletahistorico = new Coletahistorico();
			coletahistorico.setAcao(Coletaacao.COMPRA_CLIENTE);
			coletahistorico.setColeta(new Coleta(Integer.parseInt(ids[i])));
			if(documento != null){
				coletahistorico.setObservacao("Conta a pagar: <a href=\"/w3erp/financeiro/crud/Contapagar?ACAO=consultar&cddocumento=" + 
				documento.getCddocumento() +"\">"+ documento.getCddocumento() +"</a>.");	
			}else if(SinedUtil.isListNotEmpty(listaValecompra)){
				coletahistorico.setObservacao("Vale compra gerado.");
			}
			coletahistoricoService.saveOrUpdate(coletahistorico);
		}
		
		coletaService.updateSituacaoPedidovendaProducaoordemByColeta(whereIn);
		
		request.addMessage("Compra do cliente de coleta(s) realizada(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	* M�todo ajax para carregar o vinculo de acordo com a forma de pagamento
	*
	* @param request
	* @param formapagamento
	* @return
	* @since 17/11/2016
	* @author Luiz Fernando
	*/
	public ModelAndView comboBoxVinculo(WebRequestContext request, Formapagamento formapagamento) {
		if (formapagamento == null || formapagamento.getCdformapagamento() == null) {
			throw new SinedException("Forma de pagamento n�o pode ser nulo.");
		}
		
		List<Conta> listaConta = new ArrayList<Conta>();
		
		if (formapagamento.equals(Formapagamento.CAIXA)) {
			listaConta = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CAIXA));
		} else if (formapagamento.equals(Formapagamento.DINHEIRO) || formapagamento.equals(Formapagamento.CHEQUE)) {
			listaConta = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CAIXA), new Contatipo(Contatipo.CONTA_BANCARIA));
		} else if (formapagamento.equals(Formapagamento.CARTAOCREDITO)) {
			listaConta = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CARTAO_DE_CREDITO));
		} else {
			listaConta = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CONTA_BANCARIA));
		}

		/**
		 * Para n�o dar pau ao retornar o objeto JSON
		 */
		for (Conta conta : listaConta) {
			conta.setListaContaempresa(null);
		}
		
		return new JsonModelAndView().addObject("lista", listaConta);
	}
	
	/**
	 * M�todo que salva o local de armazenagem a partir da escolha ao salvar coleta avulsa
	 * @param request
	 * @param coleta
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public ModelAndView salvarLocalSelecionadoEstoque(WebRequestContext request, Coleta coleta){
		
		Localarmazenagem localarmazenagem = coleta.getLocalarmazenagem();
		List<ColetaMaterial> listaColetamaterialAux = new ArrayList<ColetaMaterial>();
		coleta = coletaService.loadForEntrada(coleta);
		
		Boolean fromEditar = (Boolean) request.getSession().getAttribute("fromEditar");
		fromEditar = (fromEditar != null ? fromEditar : false);
		String whereInCdcoletamaterialNovo = (String) request.getSession().getAttribute("whereInCdcoletamaterialNovo");
		request.getSession().removeAttribute("fromEditar");
		request.getSession().removeAttribute("whereInCdcoletamaterialNovo");
		List<String> listaCdcoletamaterial = new ArrayList<String>();
		if(StringUtils.isNotBlank(whereInCdcoletamaterialNovo)){
			listaCdcoletamaterial = Arrays.asList(whereInCdcoletamaterialNovo.split(","));
		}
		if(!fromEditar){
			listaColetamaterialAux = coleta.getListaColetaMaterial();
		}
		
		if(listaCdcoletamaterial != null && !listaCdcoletamaterial.isEmpty()){
			for(String cdcoletamaterial : listaCdcoletamaterial){
				ColetaMaterial coletaMaterial = containsColetaMaterial(coleta.getListaColetaMaterial(), null, Integer.parseInt(cdcoletamaterial));
				if(coletaMaterial != null && coletaMaterial.getCdcoletamaterial() != null){
					listaColetamaterialAux.add(coletaMaterial);
				}
			}
		}
		
		if(listaColetamaterialAux != null && !listaColetamaterialAux.isEmpty()){
			List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
			StringBuilder linkMe = new StringBuilder();
			for(ColetaMaterial item : listaColetamaterialAux){
				if(item.getQuantidade() != null && item.getQuantidade() > 0){
					Movimentacaoestoque me = new Movimentacaoestoque();
					me.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
					me.setMaterial(item.getMaterial());
					me.setMaterialclasse(Materialclasse.PRODUTO);
					me.setQtde(item.getQuantidade());
					me.setEmpresa(coleta.getEmpresa());
					me.setLocalarmazenagem(localarmazenagem);
					me.setValor(item.getValorunitario());
					me.setObservacao(coleta.getObservacao());
					me.setDtmovimentacao(new java.sql.Date(System.currentTimeMillis()));
					me.setObservacao(item.getObservacao());
					listaMovimentacaoestoque.add(me);
				}
			}
			if(listaMovimentacaoestoque != null && !listaMovimentacaoestoque.isEmpty()){
				for (Movimentacaoestoque me : listaMovimentacaoestoque) {
					Movimentacaoestoqueorigem meo = new Movimentacaoestoqueorigem();
					meo.setDtaltera(new Timestamp(System.currentTimeMillis()));
					meo.setColeta(coleta);
					movimentacaoestoqueorigemService.saveOrUpdateNoUseTransaction(meo);
					
					me.setMovimentacaoestoqueorigem(meo);
					movimentacaoestoqueService.saveOrUpdateNoUseTransaction(me);
					
					if(!linkMe.toString().equals("")) linkMe.append(",");
					linkMe.append(movimentacaoestoqueService.makeLinkHistoricoMovimentacaoestoque(me));
					
					Coletahistorico coletahistorico = new Coletahistorico();
					coletahistorico.setAcao(Coletaacao.ENTRADA_ESTOQUE);
					coletahistorico.setColeta(coleta);
					coletahistorico.setObservacao("Entrada: <a href=\"/w3erp/suprimento/crud/Movimentacaoestoque?ACAO=consultar&cdmovimentacaoestoque=" + 
													me.getCdmovimentacaoestoque() +"\">"+ me.getCdmovimentacaoestoque()+"</a>");
					coletahistoricoService.saveOrUpdate(coletahistorico);
				}
			}
		}
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView saveConsumoMateriaPrima(WebRequestContext request, ConsumoMateriaprimaProducaoBean bean){
		return producaoordemService.saveConsumoMateriaPrima(request, bean);
	}
	
	@Override
	protected void listagem(WebRequestContext request, ColetaFiltro filtro) throws Exception {
		request.setAttribute("listaTipoNota", Arrays.asList(new String[]{ColetaFiltro.TIPO_NOTA_TODOS, ColetaFiltro.TIPO_NOTA_ENTRADA, ColetaFiltro.TIPO_NOTA_SAIDA, ColetaFiltro.TIPO_NOTA_NENHUM}));
		request.setAttribute("isRepagem", SinedUtil.isRecapagem());
		boolean isExpedicaoColeta = false;
		if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("COLETA")){
			isExpedicaoColeta = true;
			List<SituacaoVinculoExpedicao> listaSituacaoColeta = new ArrayList<SituacaoVinculoExpedicao>();
			SituacaoVinculoExpedicao[] arraySituacaoColeta = SituacaoVinculoExpedicao.values();
			for (SituacaoVinculoExpedicao situacaoColeta : arraySituacaoColeta) {
				listaSituacaoColeta.add(situacaoColeta);
			}
			request.setAttribute("listaSituacaoColeta", listaSituacaoColeta);
		}
		request.setAttribute("isExpedicaoColeta", isExpedicaoColeta);
		request.setAttribute("listaTipoColaborador", LabelColoboradorFiltro.values());
		super.listagem(request, filtro);
	}
	
	public ModelAndView registrarExpedicao(WebRequestContext request) throws Exception{
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return sendRedirectToAction("listagem");
		}
		List<Coleta> listaColeta = coletaService.findByWhereIn(whereIn);
		for (Coleta coleta : listaColeta) {
			if(SituacaoVinculoExpedicao.EXPEDI��O_COMPLETA.equals(coleta.getSituacaoColeta())){
				request.addError("expedi��o ja realizada");
				return sendRedirectToAction("listagem");
			}
		}
		return new ModelAndView("redirect:/faturamento/crud/Expedicao?ACAO=criar&listaColeta="+whereIn);
	}
	
	
	public ModelAndView abrirPopUpRegistrarNF(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Coleta");
			return null;
		}
		
		ColetarProducaoBean bean = new ColetarProducaoBean();
		bean.setWhereInColeta(whereIn);
		return new ModelAndView("direct:/crud/popup/popUpRegistrarNF", "bean", bean);
	}
	
	public ModelAndView msgNotaItensColeta(WebRequestContext request, ColetaFiltro filtro) throws Exception {
		request.addError("J� foram emitidas notas para todos os itens da coleta.");
		return new ModelAndView("redirect:/faturamento/crud/Coleta?ACAO=listagem");
	}
	
	@Override
	public ModelAndView doExportar(WebRequestContext request,ColetaFiltro filtro) throws CrudException, IOException {
		Resource resource = coletaService.gerarListagemCSV(filtro);
		return new ResourceModelAndView(resource);
	}
	
	public ModelAndView ajaxLoadVendendorPrincipal(WebRequestContext request) {
		String codCliente = request.getParameter("cdCliente");
		String strNomeVendedor = "";
		if(codCliente !=null){
			try{
				Integer cod = Integer.parseInt(codCliente);
				Cliente cli = new Cliente(cod);
				Colaborador vendendor = clienteVendedorService.getVendedorPrincipal(cli);
				if(vendendor != null && vendendor.getNome()!=null) {
					strNomeVendedor = vendendor.getNome();
				}else{
					strNomeVendedor = "vendedor n�o encontrado";
				}
			}catch (Exception e){
				strNomeVendedor = "vendedor n�o encontrado";
			}
		}
		ModelAndView retorno = new JsonModelAndView().addObject("nomeVendendor", strNomeVendedor);
		return retorno;
	}
	
	@Override
	protected Coleta criar(WebRequestContext request, Coleta form) throws Exception {
		form = super.criar(request, form);
		List<Empresa> listaEmpresas = empresaService.findAtivos();
		
		if(listaEmpresas.size() == 1){
			form.setEmpresa(listaEmpresas.get(0));
		}
		
		return form;
	}
}