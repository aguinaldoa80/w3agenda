package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialitem;
import br.com.linkcom.sined.geral.bean.Contratomaterialpessoa;
import br.com.linkcom.sined.geral.bean.Indice;
import br.com.linkcom.sined.geral.bean.Indicerecursogeral;
import br.com.linkcom.sined.geral.bean.Indicerecursohumano;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratomaterialitemService;
import br.com.linkcom.sined.geral.service.ContratomaterialpessoaService;
import br.com.linkcom.sined.geral.service.IndiceService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratomaterialFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/Contratomaterial", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"contrato.cliente", "contrato", "servico", "dtinicio", "dtfim", "qtde"})
public class ContratomaterialCrud extends CrudControllerSined<ContratomaterialFiltro, Contratomaterial, Contratomaterial> {

	private ContratomaterialpessoaService contratomaterialpessoaService;
	private ContratomaterialitemService contratomaterialitemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private IndiceService indiceService;
	private ContratoService contratoService;
	private MaterialService materialService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setIndiceService(IndiceService indiceService) {
		this.indiceService = indiceService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setContratomaterialitemService(
			ContratomaterialitemService contratomaterialitemService) {
		this.contratomaterialitemService = contratomaterialitemService;
	}
	public void setContratomaterialpessoaService(
			ContratomaterialpessoaService contratomaterialpessoaService) {
		this.contratomaterialpessoaService = contratomaterialpessoaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Contratomaterial form) throws Exception {
		if("true".equals(request.getParameter("fromContrato"))){
			form.setDetalhamento(Boolean.TRUE);
		}
		
		if(form.getCdcontratomaterial() != null){
			form.setListaContratomaterialpessoa(contratomaterialpessoaService.findByContratomaterial(form));
			form.setListaContratomaterialitem(contratomaterialitemService.findByContratomaterial(form));
			
			if(form.getListaContratomaterialitem() != null && form.getListaContratomaterialitem().size() > 0){
				for (Contratomaterialitem contratomaterialitem : form.getListaContratomaterialitem()) {
					Double qtdedisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(contratomaterialitem.getMaterial(), Materialclasse.PRODUTO, contratomaterialitem.getLocalarmazenagem());
					contratomaterialitem.setQtdedisponivel(qtdedisponivel);
				}
			}
		}
	}
	
	@Override
	protected void listagem(WebRequestContext request, ContratomaterialFiltro filtro) throws Exception {
		if(filtro.getContrato() != null && filtro.getContrato().getCdcontrato() != null)
			request.setAttribute("contratofiltro", "br.com.linkcom.sined.geral.bean.Contrato[cdcontrato=" +filtro.getContrato().getCdcontrato() + "]");
	}
	
	
	/**
	 * Action que busca a quantidade dispon�vel por item.
	 *
	 * @param request
	 * @param contratomaterialitem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/01/2013
	 */
	public ModelAndView verificaDisponibilidadeMaterialAjax(WebRequestContext request, Contratomaterialitem contratomaterialitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(contratomaterialitem.getMaterial() != null){
			Double qtdedisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(contratomaterialitem.getMaterial(), Materialclasse.PRODUTO, contratomaterialitem.getLocalarmazenagem());
			jsonModelAndView.addObject("qtdedisponivel", qtdedisponivel);
		}
		return jsonModelAndView;
	}
	
	/**
	 * Action em ajax que calcula a composi��o do material contrato.
	 *
	 * @param request
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/01/2013
	 */
	public ModelAndView calculaComposicaoAjax(WebRequestContext request, Contratomaterial contratomaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(contratomaterial.getIndice() != null){
			Double qtdeMultiplicacao = contratomaterial.getQtde() != null ? contratomaterial.getQtde() : 1d; 
			
			Indice indice = indiceService.findWithListasFlex(contratomaterial.getIndice());
			
			List<Contratomaterialitem> listaContratomaterialitem = new ArrayList<Contratomaterialitem>();
			List<Indicerecursogeral> listaIndicerecursogeral = indice.getListaIndicerecursogeral();
			if(listaIndicerecursogeral != null && listaIndicerecursogeral.size() > 0){
				for (Indicerecursogeral indicerecursogeral : listaIndicerecursogeral) {
					if(indicerecursogeral.getMaterial() != null){
						Contratomaterialitem contratomaterialitem = new Contratomaterialitem();
						contratomaterialitem.setMaterial(indicerecursogeral.getMaterial());
						contratomaterialitem.setQtde(indicerecursogeral.getQtde() * qtdeMultiplicacao);
						
						Double qtdedisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(contratomaterialitem.getMaterial(), Materialclasse.PRODUTO, contratomaterialitem.getLocalarmazenagem());
						contratomaterialitem.setQtdedisponivel(qtdedisponivel);
						
						listaContratomaterialitem.add(contratomaterialitem);
					}
				}
			}
			jsonModelAndView.addObject("listaContratomaterialitem", listaContratomaterialitem);
			
			List<Contratomaterialpessoa> listaContratomaterialpessoa = new ArrayList<Contratomaterialpessoa>();
			List<Indicerecursohumano> listaIndicerecursohumano = indice.getListaIndicerecursohumano();
			if(listaIndicerecursohumano != null && listaIndicerecursohumano.size() > 0){
				for (Indicerecursohumano indicerecursohumano : listaIndicerecursohumano) {
					if(indicerecursohumano.getCargo() != null){
						Contratomaterialpessoa contratomaterialpessoa = new Contratomaterialpessoa();
						contratomaterialpessoa.setCargo(indicerecursohumano.getCargo());
						contratomaterialpessoa.setQtde(indicerecursohumano.getQtde() * qtdeMultiplicacao);
						
						listaContratomaterialpessoa.add(contratomaterialpessoa);
					}
				}
			}
			jsonModelAndView.addObject("listaContratomaterialpessoa", listaContratomaterialpessoa);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * M�todo ajax para buscar os contratos relacionado ao cliente (Utilizado para o combo de contrato)
	 *
	 * @param request
	 * @param filtro
	 * @author Rodrigo Freitas
	 * @since 29/01/2013
	 */
	public void preencheComboContratoAjax(WebRequestContext request, ContratomaterialFiltro filtro){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		try{
			List<Contrato> lista = contratoService.findByClienteContratotipo(filtro.getCliente(), null);
			String listaContrato = SinedUtil.convertToJavaScript(lista, "listaContrato", null);
			view.println(listaContrato);
			view.println("var sucesso = true;");
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}
	
	/**
	 * Carrega via ajax o grupo de material.
	 *
	 * @param request
	 * @param contratomaterialitem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/03/2013
	 */
	public ModelAndView ajaxCarregaMaterialgrupo(WebRequestContext request, Contratomaterialitem contratomaterialitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(contratomaterialitem.getMaterial() != null){
			Material material = materialService.load(contratomaterialitem.getMaterial(), "material.cdmaterial, material.materialgrupo");
			if(material != null && material.getMaterialgrupo() != null){
				jsonModelAndView.addObject("cdmaterialgrupo", material.getMaterialgrupo().getCdmaterialgrupo());
				jsonModelAndView.addObject("nomematerialgrupo", material.getMaterialgrupo().getNome());
			}
		}
		
		return jsonModelAndView;
	}
	
}