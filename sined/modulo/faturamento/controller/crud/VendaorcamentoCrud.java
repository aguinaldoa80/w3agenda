package br.com.linkcom.sined.modulo.faturamento.controller.crud;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.ObjectUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel.TipoComprovante;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresarepresentacao;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Materialgrupocomissaovenda;
import br.com.linkcom.sined.geral.bean.Materialkitflexivel;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.MotivoReprovacaoFaturamento;
import br.com.linkcom.sined.geral.bean.Orcamentovalorcampoextra;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendamaterialmestre;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentoformapagamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentohistorico;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterialseparacao;
import br.com.linkcom.sined.geral.bean.Vendaorcamentosituacao;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_VendamaterialTabelapreco;
import br.com.linkcom.sined.geral.bean.enumeration.TipoComprovanteEnum;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.CampoextrapedidovendatipoService;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ClientevendedorService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialFaixaMarkupService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialformulavalorvendaService;
import br.com.linkcom.sined.geral.service.MaterialgrupocomissaovendaService;
import br.com.linkcom.sined.geral.service.MaterialkitflexivelService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MaterialrelacionadoService;
import br.com.linkcom.sined.geral.service.MaterialsimilarService;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoService;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoitemService;
import br.com.linkcom.sined.geral.service.MaterialunidademedidaService;
import br.com.linkcom.sined.geral.service.MaterialvendaService;
import br.com.linkcom.sined.geral.service.MotivoReprovacaoFaturamentoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OrcamentovalorcampoextraService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.OrdemcompramaterialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendamaterialService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.RestricaoService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.geral.service.TabelaimpostoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.geral.service.VendaorcamentoformapagamentoService;
import br.com.linkcom.sined.geral.service.VendaorcamentohistoricoService;
import br.com.linkcom.sined.geral.service.VendaorcamentomaterialService;
import br.com.linkcom.sined.geral.service.VendaorcamentomaterialmestreService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteAnexoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioComprovanteDestinatarioBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Enviocomprovanteemail;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FornecedoragenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Impostovenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.KitflexivelBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Sugestaovenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.TrocavendedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaorcamentoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.filter.MaterialHistoricoPrecoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/faturamento/crud/VendaOrcamento" , authorizationModule=CrudAuthorizationModule.class)
public class VendaorcamentoCrud extends CrudControllerSined<VendaorcamentoFiltro, Vendaorcamento, Vendaorcamento> {
	
	private EmpresaService empresaService;
	private VendaorcamentomaterialService vendaorcamentomaterialService;
	private VendaorcamentoService vendaorcamentoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private VendaorcamentohistoricoService vendaorcamentohistoricoService;
	private MaterialService materialService;
	private PrazopagamentoService prazopagamentoService;
	private EnderecoService enderecoService;
	private ColaboradorService colaboradorService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private FornecedorService fornecedorService;
	private ParametrogeralService parametrogeralService;
	private RestricaoService restricaoService;
	private ProdutoService produtoService;
	private LocalarmazenagemService localarmazenagemService;
	private ContaService contaService;
	private VendaorcamentoformapagamentoService vendaorcamentoformapagamentoService;
	private MaterialtabelaprecoService materialtabelaprecoService;
	private MaterialvendaService materialvendaService;
	private ClienteService clienteService;
	private UsuarioService usuarioService;
	private MaterialsimilarService materialsimilarService;
	private LoteestoqueService loteestoqueService;
	private VendaService vendaService;
	private MaterialtabelaprecoitemService materialtabelaprecoitemService;
	private OrdemcompramaterialService ordemcompramaterialService;
	private VendamaterialService vendamaterialService;
	private MaterialproducaoService materialproducaoService;
	private ContatoService contatoService;
	private SolicitacaocompraService solicitacaocompraService;
	private VendaorcamentomaterialmestreService vendaorcamentomaterialmestreService;
	private ArquivoService arquivoService;
	private RegiaoService regiaoService;
	private MaterialrelacionadoService materialrelacionadoService;
	private CampoextrapedidovendatipoService campoextrapedidovendatipoService;
	private OrcamentovalorcampoextraService orcamentovalorcampoextraService;
	private OrdemcompraService ordemcompraService;
	private MaterialunidademedidaService materialunidademedidaService;
	private MaterialformulavalorvendaService materialformulavalorvendaService;
	private ValecompraService valecompraService;
	private PedidovendaService pedidovendaService;
	private EnvioemailService envioemailService;
	private MaterialkitflexivelService materialkitflexivelService;
	private DocumentotipoService documentotipoService;
	private MaterialgrupocomissaovendaService materialgrupocomissaovendaService;
	private CategoriaService categoriaService;
	private TabelaimpostoService tabelaimpostoService;
	private PedidovendatipoService pedidovendatipoService;
	private OportunidadeService oportunidadeService;
	private ContacrmService contacrmService;
	private ClientevendedorService clientevendedorService;
	private ProjetoService projetoService;
	private UnidademedidaService unidademedidaService;
	private MaterialFaixaMarkupService materialFaixaMarkupService;
	private PedidovendamaterialService pedidovendamaterialService;
	
	private static final String  MSG_IDENTIFICADOR_EXTERNO ="N�o foi poss�vel confirmar a or�amento pois o valor do campo 'Identificador Externo' j� est� cadastrado no sistema. Favor conferir o valor informado para a confirma��o do or�amento.";
	
	private MotivoReprovacaoFaturamentoService motivoReprovacaoFaturamentoService;
	public void setCategoriaService(CategoriaService categoriaService) {this.categoriaService = categoriaService;}
	public void setEnvioemailService(EnvioemailService envioemailService) {this.envioemailService = envioemailService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setRegiaoService(RegiaoService regiaoService) {this.regiaoService = regiaoService;}	
	public void setMaterialvendaService(MaterialvendaService materialvendaService) {this.materialvendaService = materialvendaService;}
	public void setMaterialtabelaprecoService(MaterialtabelaprecoService materialtabelaprecoService) {this.materialtabelaprecoService = materialtabelaprecoService;}
	public void setVendaorcamentoformapagamentoService(VendaorcamentoformapagamentoService vendaorcamentoformapagamentoService) {this.vendaorcamentoformapagamentoService = vendaorcamentoformapagamentoService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {this.vendaorcamentoService = vendaorcamentoService;}
	public void setVendaorcamentomaterialService(VendaorcamentomaterialService vendaorcamentomaterialService) {this.vendaorcamentomaterialService = vendaorcamentomaterialService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setVendaorcamentohistoricoService(VendaorcamentohistoricoService vendaorcamentohistoricoService) {this.vendaorcamentohistoricoService = vendaorcamentohistoricoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {this.unidademedidaconversaoService = unidademedidaconversaoService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setRestricaoService(RestricaoService restricaoService) {this.restricaoService = restricaoService;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setMaterialsimilarService(MaterialsimilarService materialsimilarService) {this.materialsimilarService = materialsimilarService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setMaterialtabelaprecoitemService(MaterialtabelaprecoitemService materialtabelaprecoitemService) {this.materialtabelaprecoitemService = materialtabelaprecoitemService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setOrdemcompramaterialService(OrdemcompramaterialService ordemcompramaterialService) {this.ordemcompramaterialService = ordemcompramaterialService;}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {this.materialproducaoService = materialproducaoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {this.solicitacaocompraService = solicitacaocompraService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setVendaorcamentomaterialmestreService(VendaorcamentomaterialmestreService vendaorcamentomaterialmestreService) {this.vendaorcamentomaterialmestreService = vendaorcamentomaterialmestreService;}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {this.materialrelacionadoService = materialrelacionadoService;}
	public void setCampoextrapedidovendatipoService(CampoextrapedidovendatipoService campoextrapedidovendatipoService) {this.campoextrapedidovendatipoService = campoextrapedidovendatipoService;}
	public void setOrcamentovalorcampoextraService(OrcamentovalorcampoextraService orcamentovalorcampoextraService) {this.orcamentovalorcampoextraService = orcamentovalorcampoextraService;}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {this.ordemcompraService = ordemcompraService;}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {this.materialunidademedidaService = materialunidademedidaService;}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {this.materialformulavalorvendaService = materialformulavalorvendaService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setMaterialkitflexivelService(MaterialkitflexivelService materialkitflexivelService) {this.materialkitflexivelService = materialkitflexivelService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setMaterialgrupocomissaovendaService(MaterialgrupocomissaovendaService materialgrupocomissaovendaService) {this.materialgrupocomissaovendaService = materialgrupocomissaovendaService;}
	public void setTabelaimpostoService(TabelaimpostoService tabelaimpostoService) {this.tabelaimpostoService = tabelaimpostoService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setContacrmService(ContacrmService contacrmService) {this.contacrmService = contacrmService;}
	public void setClientevendedorService(ClientevendedorService clientevendedorService) {this.clientevendedorService = clientevendedorService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setMotivoReprovacaoFaturamentoService(MotivoReprovacaoFaturamentoService motivoReprovacaoFaturamentoService) {this.motivoReprovacaoFaturamentoService = motivoReprovacaoFaturamentoService;}
	public void setMaterialFaixaMarkupService(MaterialFaixaMarkupService materialFaixaMarkupService) {this.materialFaixaMarkupService = materialFaixaMarkupService;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	@Override
	protected void listagem(WebRequestContext request, VendaorcamentoFiltro filtro) throws Exception {		
		String vendasaldoproduto = parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTO);
		if(vendasaldoproduto.toUpperCase().equals("TRUE")){
			request.setAttribute("VENDASALDOPRODUTO", Boolean.TRUE);
		}else {
			request.setAttribute("VENDASALDOPRODUTO", Boolean.FALSE);
		}
		
		if(filtro.getPedidovendatipo() != null && filtro.getPedidovendatipo().getCdpedidovendatipo() != null){
			request.setAttribute("listaCampoExtra", campoextrapedidovendatipoService.findByTipo(filtro.getPedidovendatipo()));
		}else{
			request.setAttribute("listaCampoExtra", Collections.emptyList());
		}
		
		List<Vendaorcamentosituacao> listaSituacao = new ArrayList<Vendaorcamentosituacao>();
		listaSituacao.add(Vendaorcamentosituacao.EM_ABERTO);
		listaSituacao.add(Vendaorcamentosituacao.AUTORIZADO);
		listaSituacao.add(Vendaorcamentosituacao.CANCELADA);		
		listaSituacao.add(Vendaorcamentosituacao.REPROVADO);
		request.setAttribute("listaSituacao", listaSituacao);
		
		if (filtro.getListaVendaorcamentosituacao() == null){
			filtro.setListaVendaorcamentosituacao(new ArrayList<Vendaorcamentosituacao>());
			filtro.getListaVendaorcamentosituacao().add(Vendaorcamentosituacao.EM_ABERTO);
			filtro.getListaVendaorcamentosituacao().add(Vendaorcamentosituacao.AUTORIZADO);
		}
		request.setAttribute("listaCategoriaCliente", categoriaService.findByTipo(true, false));
		super.listagem(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, VendaorcamentoFiltro filtro) throws CrudException {
		filtro.setNaoexibirresultado(null);
		
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		filtro.setIsRestricaoVendaVendedor(usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado()));
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadForFiltro(filtro.getRegiao()));
		}
		
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			Material material = materialService.load(filtro.getMaterial(), "material.cdmaterial, material.vendapromocional");
			if(material != null && Boolean.TRUE.equals(material.getVendapromocional())){
				filtro.getMaterial().setVendapromocional(material.getVendapromocional());
			}
		}
		//realizando valida��o para evitar exce��es
		if(filtro.getWhereIncdvendaorcamento() != null){
			String [] listcdvenda = filtro.getWhereIncdvendaorcamento().split(",");
			String whereIn = "";
			Boolean contemErro = false;
			if(listcdvenda.length > 0){
				for (String cdpedido : listcdvenda) {
					try{
						if(StringUtils.isNumeric(cdpedido.trim()) && Integer.parseInt(cdpedido.trim()) < Integer.MAX_VALUE){
							whereIn += cdpedido + ",";
						}else{
							contemErro = true;
							break;
						}
					}catch(NumberFormatException n){
						contemErro=true;
						break;
					}
				}
			}
			if(!contemErro){
				filtro.setWhereIncdvendaorcamento(whereIn.substring(0, whereIn.length() - 1));
			}else{
				filtro.setWhereIncdvendaorcamento(null);
				request.addError("Favor preencher com n�mero(s), podendo conter v�rgula(s).");
			}
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected Vendaorcamento criar(WebRequestContext request, Vendaorcamento form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdvendaorcamento() != null){
			form = vendaorcamentoService.criarCopia(form);
			request.setAttribute("copiarorcamento", true);
			return form;
		}else if(StringUtils.isNotBlank(request.getParameter("whereInOportunidade")) && !request.getParameter("whereInOportunidade").equals("<null>")){
			Integer cdvendaorcamento = null;
			try {
				cdvendaorcamento = Integer.parseInt(request.getParameter("cdOrcamentoCopia"));
			} catch (Exception e) {}
			return vendaorcamentoService.criaOrcamentoByOportunidade(request.getParameter("whereInOportunidade"), cdvendaorcamento);
		}
		Vendaorcamento vendaorcamento = super.criar(request, form);
		
		String observacaoPadrao = parametrogeralService.getValorPorNome(Parametrogeral.OBSERVACAO_CRIACAO_VENDAORCAMENTO);
		vendaorcamento.setObservacao(observacaoPadrao);
		return vendaorcamento;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Vendaorcamento form)throws Exception {
		request.setAttribute("LER_CODIGO_ETIQUETA_BALANCA", parametrogeralService.getBoolean(Parametrogeral.LER_CODIGO_ETIQUETA_BALANCA));
		vendaService.setAtributeDimensaovenda(request);
		request.setAttribute("ignoreHackAndroidDownload", true);
		
		Boolean closeOnSave = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnSave")) ? request.getParameter("closeOnSave") : "FALSE");
		request.setAttribute("closeOnSave", closeOnSave);
		
		Boolean emitirComprovante = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("emitirComprovante")) ? request.getParameter("emitirComprovante") : "TRUE");
		request.setAttribute("emitirComprovante", emitirComprovante);
		
		String parametro_mostrar_materialmestre = parametrogeralService.getValorPorNome(Parametrogeral.MOSTRAR_LISTA_MATERIALMESTRE);
		request.setAttribute("MOSTRAR_LISTA_MATERIALMESTRE", parametro_mostrar_materialmestre != null && parametro_mostrar_materialmestre.trim().toUpperCase().equals("TRUE"));
		
		Boolean permissaoProjeto = false;
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			permissaoProjeto = StringUtils.isNotBlank(SinedUtil.getListaProjeto());
			request.setAttribute("permissaoProjeto", new Boolean(permissaoProjeto));
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		request.setAttribute("listaProjetos", projetoService.findProjetosAtivos(form.getProjeto(), permissaoProjeto));
		String cdcliente = request.getParameter("cdcliente");
		Cliente cliente = null;
		if(cdcliente != null && !cdcliente.isEmpty()) {
			cliente = new Cliente(Integer.parseInt(cdcliente));
			cliente = ClienteService.getInstance().loadForProcessFromPainelInteracao(cliente);
			form.setCliente(cliente);
		}
		
		Empresa empresaPrincipal = empresaService.loadPrincipalOrEmpresaUsuario();
		if(empresaPrincipal.getProximoidentificadorvendaorcamento() == null){
			form.setIdentificadorAutomatico(Boolean.FALSE);
		}
		
		String paramExibirValorMinMaxVenda = parametrogeralService.getValorPorNome(Parametrogeral.EXIBIRVALORMINMAXVENDA);
		request.setAttribute("EXIBIRVALORMINMAXVENDA", paramExibirValorMinMaxVenda.toUpperCase().equals("TRUE"));
		
		Usuario usuario_aux = usuarioService.load(usuario, "usuario.cdpessoa, usuario.limitepercentualdesconto");
		form.setLimitepercentualdesconto(usuario_aux.getLimitepercentualdesconto());		
		
		if(form.getCdvendaorcamento() != null){
			Vendaorcamento vendacomtabelapreco = vendaorcamentoService.loadWithMaterialtabelapreco(form);
			if(vendacomtabelapreco != null && vendacomtabelapreco.getMaterialtabelapreco() != null){
				form.setMaterialtabelapreco(vendacomtabelapreco.getMaterialtabelapreco());
			}
			if (form.getColaborador() != null && form.getColaborador().getCdpessoa() != null){
				form.setColaborador(colaboradorService.loadWithoutPermissao(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
			}
			if (form.getAgencia()!=null && form.getAgencia().getCdpessoa()!=null){
				form.setAgencia(fornecedorService.load(form.getAgencia(), "fornecedor.cdpessoa, fornecedor.nome"));
			}
			if (form.getLocalarmazenagem()!=null && form.getLocalarmazenagem().getCdlocalarmazenagem()!=null){
				form.setLocalarmazenagem(localarmazenagemService.load(form.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
			}
			if (form.getCliente()!=null && form.getCliente().getCdpessoa()!=null){
				form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome"));
				
				if(form.getContato() != null && form.getContato().getCdpessoa() != null){
					form.setContato(contatoService.load(form.getContato(), "contato.cdpessoa, contato.nome"));
				}
			}
			if(form.getContacrm() != null && form.getContacrm().getCdcontacrm() != null){
				form.setContacrm(contacrmService.load(form.getContacrm(), "contacrm.cdcontacrm, contacrm.nome"));
			}
			if(form.getPedidovendatipo() != null && form.getPedidovendatipo().getCdpedidovendatipo() != null){
				//form.setPedidovendatipo(pedidovendatipoService.load(form.getPedidovendatipo(), "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.descricao, pedidovendatipo.bonificacao"));
				form.setPedidovendatipo(pedidovendatipoService.load(form.getPedidovendatipo()));
			}
		}
		request.setAttribute("listaPedidoVendaTipo", pedidovendatipoService.findForComboWithoutVendaEcommerce());
		if(form.getCdvendaorcamento() == null){
			form.setTrocarvendedor(true);
			if(form.getPedidovendatipo() == null){
				form.setPedidovendatipo(pedidovendatipoService.loadPrincipal());
			}
			form.setDtorcamento(new Date(System.currentTimeMillis()));
			form.setColaborador(form.getColaborador()!= null ? colaboradorService.loadWithoutPermissao(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome") : SinedUtil.getUsuarioComoColaborador());
			if(request.getSession().getAttribute("empresaSelecionada") == null && form.getEmpresa() == null)
				form.setEmpresa(empresaPrincipal);
			
			form.setMaterial(null);
			form.setCodigo(null);
			form.setQuantidades(1D);
			form.setValor(null);
			
			request.setAttribute("VENDAORCAMENTO", "Orcamento");
			form.setVendaorcamentosituacao(Vendaorcamentosituacao.EM_ABERTO);
			
			if(form.getEndereco() != null && form.getEndereco().getCdendereco() != null){
				request.setAttribute("enderecoEscolhido", "br.com.linkcom.sined.geral.bean.Endereco[cdendereco=" + form.getEndereco().getCdendereco() + "]");
			} else {
				request.setAttribute("enderecoEscolhido", "<null>");
			}
			
			if(form.getLocalarmazenagem() == null){
				request.setAttribute("buscarLocalUnico", true);
				if(form.getEmpresa() != null && form.getEmpresa().getCdpessoa() != null && new SinedUtil().getUsuarioPermissaoEmpresa(form.getEmpresa())){
					List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(form.getEmpresa());
					if(SinedUtil.isListNotEmpty(listaLocalarmazenagem) && listaLocalarmazenagem.size() == 1){
						form.setLocalarmazenagem(listaLocalarmazenagem.get(0));
					}
				}
			}		
		}else {
			if (form.getEmpresa() == null){
				form.setEmpresa(empresaPrincipal);
			}
			
			if(!request.getBindException().hasErrors()){
				form.setListavendaorcamentomaterial(vendaorcamentomaterialService.findByMaterial(form));
				form.setListavendaorcamentomaterialmestre(vendaorcamentomaterialmestreService.findByVendaOrcamento(form));
				form.setListaOrcamentovalorcampoextra(orcamentovalorcampoextraService.findByVendaorcamento(form));
			}
			
			if(form.getVendaorcamentosituacao() != null && Vendaorcamentosituacao.EM_ABERTO.equals(form.getVendaorcamentosituacao()))
				vendaorcamentoService.verificaTrocaMaterialsimilar(form.getListavendaorcamentomaterial());
			form.setListavendaorcamentohistorico(vendaorcamentohistoricoService.findByVendaorcamento(form));
			form.setListavendaorcamentoformapagamento(SinedUtil.listToSet(vendaorcamentoformapagamentoService.findByVendaorcamento(form), Vendaorcamentoformapagamento.class));
		}
		
		List<Endereco> listaEnderecoByCliente = new ArrayList<Endereco>();
		if(form.getCliente() != null && form.getCliente().getCdpessoa() != null){
//			List<Endereco> listaEnderecoRetorno = enderecoService.findByCliente(form.getCliente());
			List<Endereco> listaEnderecoRetorno = enderecoService.findByPessoaWithoutInativo(form.getCliente());
			request.setAttribute("listaEnderecoFaturamentoByCliente", listaEnderecoRetorno);
			for (Endereco endereco : listaEnderecoRetorno) {
				if (endereco.getEnderecotipo().equals(Enderecotipo.ENTREGA)	|| endereco.getEnderecotipo().equals(Enderecotipo.UNICO) 
						|| endereco.getEnderecotipo().equals(Enderecotipo.OUTRO)){
					listaEnderecoByCliente.add(endereco);
				}
			}
		}else{
			request.setAttribute("listaEnderecoFaturamentoByCliente", new ArrayList<Endereco>());
		}
		request.setAttribute("listaEnderecoByCliente", listaEnderecoByCliente);
		
		Boolean prazomedio = Boolean.FALSE;
		if(form.getPrazomedio() != null && form.getPrazomedio()){
			prazomedio = Boolean.TRUE;
		}
		List<Prazopagamento> listaPrazopagamento = prazopagamentoService.findByPrazomedio(prazomedio);
		request.setAttribute("listaPrazopagamento", listaPrazopagamento);

		request.setAttribute("listaFornecedorTransportadora", fornecedorService.findFornecedoresTransportadora(form.getEmpresa() != null && form.getEmpresa().getCdpessoa() != null ? form.getEmpresa().getCdpessoa().toString() : null));
		request.setAttribute("VENDASALDOPORMINMAX", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX));
		request.setAttribute("VENDASALDOPRODUTO", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTO));
		request.setAttribute("VENDASALDOPRODUTONEGATIVO", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTONEGATIVO));
//		request.setAttribute("PERMITIRESTOQUENEGATIVO", parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO));
		request.setAttribute("BLOQUEIOALTURALARGURAVENDA", parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEIO_ALTURALARGURA_VENDA));
		request.setAttribute("DESCONSIDERARDESCONTOSALDOPRODUTO", parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO));
		
		Boolean exibircomprimentooriginal = Boolean.FALSE;
		if(form.getListavendaorcamentomaterial() != null && !form.getListavendaorcamentomaterial().isEmpty()){
			for(Vendaorcamentomaterial vendaorcamentomaterial : form.getListavendaorcamentomaterial()){
				vendaorcamentomaterial.setExistematerialsimilar(materialsimilarService.existMaterialsimilar(vendaorcamentomaterial.getMaterial()));
				if(form.getCdvendaorcamento() != null && !exibircomprimentooriginal && vendaorcamentomaterial.getComprimentooriginal() != null && vendaorcamentomaterial.getComprimentooriginal() > 0){
					exibircomprimentooriginal = Boolean.TRUE;
//						break;
				}					
				if(vendaorcamentomaterial.getAltura() != null || vendaorcamentomaterial.getLargura() != null && vendaorcamentomaterial.getComprimento() != null){
					vendaorcamentomaterial.setMaterial(vendaorcamentomaterial.getMaterial().copiaMaterial());
					if(vendaorcamentomaterial.getMaterial().getNome() != null && !"".equals(vendaorcamentomaterial.getMaterial().getNome())){
						vendaorcamentomaterial.getMaterial().setNome(vendaorcamentomaterial.getMaterial().getNome() + " " + vendaorcamentoService.getAlturaLarguraComprimentoConcatenados(vendaorcamentomaterial, vendaorcamentomaterial.getMaterial()));
					}else {
						vendaorcamentomaterial.getMaterial().setNome(materialService.load(vendaorcamentomaterial.getMaterial(), "material.nome").getNome() + " " + vendaorcamentoService.getAlturaLarguraComprimentoConcatenados(vendaorcamentomaterial, vendaorcamentomaterial.getMaterial()));
					}
				}
			}
		}
		request.setAttribute("exibircomprimentooriginal", exibircomprimentooriginal);
		
		vendaorcamentoService.ajustaSaldofinalValorvendaMaximoMinimo(form);
		
		if (form.getCdvendaorcamento() != null && form.getListavendaorcamentomaterial() != null){
			
			//Preenchendo a lista de separa��o para que o usu�rio consiga editar
			for (Vendaorcamentomaterial item : form.getListavendaorcamentomaterial()){
				if(SinedUtil.isListNotEmptyAndInitialized(item.getMaterial().getListaMaterialunidademedida())){
					proxima_unidade_conversao:
					for (Materialunidademedida unidadeconversao : item.getMaterial().getListaMaterialunidademedida()){
						for (Vendaorcamentomaterialseparacao itemSeparacao : item.getListaVendaorcamentomaterialseparacao()){
							if (itemSeparacao.getUnidademedida().equals(unidadeconversao.getUnidademedida())){
								itemSeparacao.setFracao(unidadeconversao.getFracao());
								itemSeparacao.setQtdereferencia(unidadeconversao.getQtdereferencia());
								continue proxima_unidade_conversao;
							}
						}
						
						//se chegou aqui � porque n�o tem separa��o para esta unidade de medida
						//Vou criar um novo item para que o usu�rio consiga editar caso deseje
						Vendaorcamentomaterialseparacao novoItem = new Vendaorcamentomaterialseparacao();
						novoItem.setUnidademedida(unidadeconversao.getUnidademedida());
						novoItem.setFracao(unidadeconversao.getFracao());
						novoItem.setQtdereferencia(unidadeconversao.getQtdereferencia());
						item.getListaVendaorcamentomaterialseparacao().add(novoItem);
					}
				}
			
				boolean incluirUnidadePrincipal = true;
				for (Vendaorcamentomaterialseparacao itemSeparacao : item.getListaVendaorcamentomaterialseparacao()){
					if(item.getMaterial().getUnidademedida() == null){
						Material material  = materialService.unidadeMedidaMaterial(item.getMaterial());
						if(material != null && ObjectUtils.isNullOrNotInitialized(item.getMaterial().getUnidademedida())){
							item.getMaterial().setUnidademedida(material.getUnidademedida());
						}
					}
					if (ObjectUtils.isNotNullAndInitialized(item.getMaterial().getUnidademedida()) && item.getMaterial().getUnidademedida().equals(itemSeparacao.getUnidademedida())){
						incluirUnidadePrincipal = false;
						itemSeparacao.setUnidademedida(new Unidademedida(item.getMaterial().getUnidademedida().getCdunidademedida(), "Quantidade restante", "Quantidade restante"));
					}
				}
				if (incluirUnidadePrincipal){
					Vendaorcamentomaterialseparacao novoItem = new Vendaorcamentomaterialseparacao();
					//novoItem.setUnidademedida(item.getMaterial().getUnidademedida());
					novoItem.setUnidademedida(new Unidademedida(item.getMaterial().getUnidademedida().getCdunidademedida(), "Quantidade restante", "Quantidade restante"));
					novoItem.setFracao(1.0);
					novoItem.setQtdereferencia(1.0);
					item.getListaVendaorcamentomaterialseparacao().add(novoItem);
				}
			
				Collections.sort(item.getListaVendaorcamentomaterialseparacao(), new Comparator<Vendaorcamentomaterialseparacao>(){

					@Override
					public int compare(Vendaorcamentomaterialseparacao o1, Vendaorcamentomaterialseparacao o2) {

						if (o1.getFracao() == null || o2.getFracao() == null)
							return -1;
						
						return o2.getFracao().compareTo(o1.getFracao());
					}
					
				});
			}
		}
		
		if(form.getCliente() != null){
			request.setAttribute("listaPrazopagamento", clienteService.findPrazoPagamento(form.getCliente()));
			form.setVendedorprincipal(clientevendedorService.getNomeVendedorPrincipal(form.getCliente()));
		}
		request.setAttribute("listaDocumentotipo", documentotipoService.findDocumentotipoUsuarioLogadoForVenda(form.getCliente(), null));
		
		List<Categoria> listaCategoria = null;
		if(form.getCliente() != null){
			listaCategoria = categoriaService.findByPessoa(form.getCliente());
		}
		List<Materialtabelapreco> listaMaterialtabelapreco = materialtabelaprecoService.findForComboVenda(form.getCliente(), form.getDtorcamento(), form.getEmpresa(), listaCategoria);
		request.setAttribute("listaMaterialtabelapreco", listaMaterialtabelapreco);
		if(form.getCdvendaorcamento() != null){
			request.setAttribute("listaEmpresa", empresaService.findAtivosSemFiltro(form.getEmpresa()));
		} else {
			request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		}
		
		form.setPrazoentrega(vendaService.calculaPrazoEntrega(form.getEmpresa()));
		
		Boolean obrigarLote = Boolean.FALSE;
		if(form.getPedidovendatipo() != null){
			obrigarLote = Boolean.TRUE.equals(form.getPedidovendatipo().getObrigarLoteOrcamento());
		}
		request.setAttribute("obrigarLote", obrigarLote);
		request.setAttribute("localvendaobrigatorio", parametrogeralService.getValorPorNome(Parametrogeral.LOCAL_VENDA_OBRIGATORIO));
		request.setAttribute("ALTERARQTDEITEMPRODUCAOVENDA", SinedUtil.isUserHasAction("ALTERARQTDEITEMPRODUCAOVENDA"));
		vendaService.addAtributoContatoForEntradaOrcamentoVendaPedido(request, form.getCliente());
		//vendaorcamentoService.ordenarMaterialproducao(form);
		request.setAttribute("includeAtalho", Boolean.TRUE);
		request.setAttribute("uriAtalhoPersonalizado", "/faturamento/process/RealizarVendaOrcamento");
		request.setAttribute(Acao.DEFINIR_LOTE_PEDIDO,  (boolean)SinedUtil.isUserHasAction(Acao.DEFINIR_LOTE_PEDIDO));
		request.setAttribute("venderAbaixoMinimo", SinedUtil.getUsuarioLogado().getSalvarAbaixoMinimoOrcamento() != null 
													? SinedUtil.getUsuarioLogado().getSalvarAbaixoMinimoOrcamento() : Boolean.FALSE);
		boolean exibirIpiNaVenda = empresaService.exibirIpiVenda(form.getEmpresa());
		boolean exibirIcmsNaVenda = empresaService.exibirIcmsVenda(form.getEmpresa());
		boolean exibirDifalNaVenda = empresaService.exibirDifalVenda(form.getEmpresa());
		request.setAttribute(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO, parametrogeralService.getBoolean(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO));
		request.setAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO, parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO));
		request.setAttribute("identificadorExterno",parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO));
		request.setAttribute("haveTabelaimposto", tabelaimpostoService.haveTabelaimposto());
		request.setAttribute("EXIBIR_IPI_NA_VENDA", exibirIpiNaVenda);
		request.setAttribute("EXIBIR_ICMS_NA_VENDA", exibirIcmsNaVenda);
		request.setAttribute("EXIBIR_DIFAL_NA_VENDA", exibirDifalNaVenda);
		request.setAttribute("EXIBIR_IMPOSTO_NA_VENDA", exibirIpiNaVenda || exibirIcmsNaVenda || exibirDifalNaVenda);
		request.setAttribute("EXIBIR_REPLICAR_PNEU", true);
		request.setAttribute("PEDIDOVENDAMATERIAL", parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDAMATERIAL));
		request.setAttribute("MOSTRAR_LIMITE_CREDITO", parametrogeralService.getBoolean(Parametrogeral.MOSTRAR_LIMITE_CREDITO));
		request.setAttribute("qtdeDiaUtilPrazoEntrega", parametrogeralService.buscaValorPorNome(Parametrogeral.QTDE_DIAUTIL_PRAZOENTREGA));
		request.setAttribute("EXIBIR_ORDEM_PEDIDOVENDA", parametrogeralService.getBoolean(Parametrogeral.EXIBIR_ORDEM_PEDIDOVENDA));
		request.setAttribute("EXIBIR_NOME_ALTERNATIVO_ALTURA_LARGURA", parametrogeralService.getBoolean(Parametrogeral.EXIBIR_NOME_ALTERNATIVO_ALTURA_LARGURA));
		request.setAttribute("COPIAR_NOMEALTERNATIVO_PARA_OBSMATERIAL", parametrogeralService.getBoolean(Parametrogeral.COPIAR_NOMEALTERNATIVO_PARA_OBSMATERIAL));
		request.setAttribute("HISTORICO_DE_CLIENTE", parametrogeralService.getBoolean(Parametrogeral.HISTORICO_DE_CLIENTE));
		request.setAttribute("BLOQUEIO_DIMENSOES_VENDA", parametrogeralService.getBoolean(Parametrogeral.BLOQUEIO_DIMENSOES_VENDA));
		request.setAttribute("OBRIGAR_TIPOPEDIDOVENDA", parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_TIPOPEDIDOVENDA));
		request.setAttribute("QTDE_DISPONIVEL_CONSIDERAR_RESERVADO", parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO));
		request.setAttribute("SELECAO_AUTOMATICA_LOTE_FATURAMENTO", parametrogeralService.getBoolean(Parametrogeral.SELECAO_AUTOMATICA_LOTE_FATURAMENTO));
		request.setAttribute("ALTERAR_LOTE_FATURAMENTO", SinedUtil.isUserHasAction("ALTERAR_LOTE_FATURAMENTO"));
		
		String paramConsiderarFreteCIF = parametrogeralService.getValorPorNome("CONSIDERAR_FRETE_CIF");
		if(paramConsiderarFreteCIF != null) {
			request.setAttribute("CONSIDERAR_FRETE_CIF", paramConsiderarFreteCIF.equals("TRUE") ? true : false);
		}
		form.setIncluirEnderecoFaturamento(Util.objects.isPersistent(form.getEnderecoFaturamento()));
		request.setAttribute("vendaorcamento", form);
		
		for (Vendaorcamentomaterial vendaorcamentomaterial : form.getListavendaorcamentomaterial()) {
			if(Util.objects.isPersistent(vendaorcamentomaterial.getLoteestoque())){
				vendaorcamentomaterial.setLoteestoque(loteestoqueService.loadWithNumeroValidade(vendaorcamentomaterial.getLoteestoque(), vendaorcamentomaterial.getMaterial(), false));
			}
		}
	}
	
	@Override
	protected void validateBean(Vendaorcamento bean, BindException errors) {
		vendaorcamentoService.calcularParcelasForSalvar(bean);
			
		if(!vendaorcamentoService.validaVendaorcamento(null, bean, errors)){
			if(bean.getColaboradoraux() != null && bean.getColaboradoraux().getCdpessoa() != null){
				bean.setColaborador(bean.getColaboradoraux());
			} else {
				if(bean.getColaborador() == null){
					bean.setColaborador(SinedUtil.getUsuarioComoColaborador());
				}
			}
			
			if(bean.getEndereco() != null && bean.getEndereco().getCdendereco() != null){
				NeoWeb.getRequestContext().setAttribute("enderecoEscolhido", "br.com.linkcom.sined.geral.bean.Endereco[cdendereco=" + bean.getEndereco().getCdendereco() + "]");
			} else {
				NeoWeb.getRequestContext().setAttribute("enderecoEscolhido", "<null>");
			}
		}
		
		vendaService.validaOrdenacao(errors, bean.getListavendaorcamentomaterial());
		
		if(bean.getListavendaorcamentomaterial() == null || bean.getListavendaorcamentomaterial().isEmpty()){
			errors.reject("Nenhum produto encontrado no or�amento.");			
		}
		if ("TRUE".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.IDENTIFICADOR_UNICO_PEDIDO_VENDA_ORCAMENTO)) && bean.getEmpresa() != null && StringUtils.isNotBlank(bean.getIdentificador()) && vendaorcamentoService.isIdentificadorCadastrado(bean)) {
			errors.reject("Aten��o! Identificador j� cadastrado no sistema.");			
		}
		
		Pedidovendatipo pedidovendatipo = null;
		if(bean.getPedidovendatipo() != null && bean.getPedidovendatipo().getCdpedidovendatipo() != null){
			pedidovendatipo = pedidovendatipoService.loadForEntrada(bean.getPedidovendatipo());
			if(Boolean.TRUE.equals(pedidovendatipo.getObrigarLoteOrcamento())){
				for (Vendaorcamentomaterial vom : bean.getListavendaorcamentomaterial()) {
					materialService.validateObrigarLote(vom, errors);
				}
			}
		}		
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Vendaorcamento form)	throws CrudException {		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		String acao = request.getParameter("ACAO");
		if(acao != null && "editar".equals(acao) && usuarioService.isRestricaoClienteVendedor(usuario) ){
			Vendaorcamento vendaorcamento = vendaorcamentoService.loadWithCliente(form);
			if(vendaorcamento != null && vendaorcamento.getCliente() != null && vendaorcamento.getCliente().getCdpessoa() != null && 
					!clienteService.isUsuarioPertenceRestricaoclientevendedor(vendaorcamento.getCliente(), usuario)){
				request.addError("Usu�rio n�o tem permiss�o para editar este or�amento!");
				SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOrcamento?ACAO=consultar&cdvendaorcamento=" + form.getCdvendaorcamento());
				return null;
			}
		}
		if(request.getParameter("ACAO").equalsIgnoreCase("editar")){
			form.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			form.setDtaltera(new Timestamp(System.currentTimeMillis()));
		}
		// Verifica��es E-Commerce		
		String nomeEcommerce = parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE);		
		request.setAttribute("IS_TRAYCORP", nomeEcommerce != null && nomeEcommerce.equals("TRAYCORP"));
		boolean eCommerceEnabled = parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && nomeEcommerce != null; 
		request.setAttribute("ECOMMERCE_ENABLED", eCommerceEnabled);	
		return super.doEditar(request, form);
	}
	
	@Override
	protected ListagemResult<Vendaorcamento> getLista(WebRequestContext request, VendaorcamentoFiltro filtro) {
	
		ListagemResult<Vendaorcamento> lista = super.getLista(request, filtro);
		List<Vendaorcamento> list = lista.list();
		String solicitarcompravenda = parametrogeralService.buscaValorPorNome(Parametrogeral.SOLICITARCOMPRAVENDA);

		boolean exibiripivenda = false;
		if(filtro.getEmpresa() == null){
			exibiripivenda = empresaService.existeEmpresaExibirIpi();
		}
		request.setAttribute("EXIBIR_IMPOSTO_NA_VENDA", empresaService.existeEmpresaExibirIpi() || empresaService.existeEmpresaExibirIcms() || empresaService.existeEmpresaExibirDifal());
		java.util.Date hoje = SinedDateUtils.dateToBeginOfDay(new java.util.Date());
		for (Vendaorcamento vendaorcamento : list) {
			vendaorcamento.setListavendaorcamentomaterial(vendaorcamentomaterialService.findByMaterial(vendaorcamento));
			vendaorcamento.setListavendaorcamentomaterialmestre(vendaorcamentomaterialmestreService.findByVendaOrcamento(vendaorcamento));
			vendaorcamento.setTotalvenda(vendaorcamentoService.totalVendaorcamento(vendaorcamento));
			
			if(!exibiripivenda && vendaorcamento.getEmpresa() != null && vendaorcamento.getEmpresa().getExibiripivenda() != null &&
					vendaorcamento.getEmpresa().getExibiripivenda()){
				exibiripivenda = true;
			}
			
			if("ORDEMCOMPRA".equalsIgnoreCase(solicitarcompravenda)) {
				if(vendaorcamento.getSituacaopedidosolicitacaocompra() == null) {
					vendaorcamento.setSituacaopedidosolicitacaocompra(ordemcompraService.getSituacaoOrdemcompraByVendaorcamento(vendaorcamento));
				}
			}else {
				vendaorcamento.setSituacaopedidosolicitacaocompra(solicitacaocompraService.getSituacaoSolcicitacaocompraByPedidovenda(vendaorcamento));
			}
			
			if(vendaorcamento.getDtvalidade() != null){
				if(hoje.equals(vendaorcamento.getDtvalidade())){
					vendaorcamento.setDtorcamentoClasse("dataAtual");
				}else if(hoje.before(vendaorcamento.getDtvalidade())){
					vendaorcamento.setDtorcamentoClasse("dataPosterior");
				}else if(hoje.after(vendaorcamento.getDtvalidade())){
					vendaorcamento.setDtorcamentoClasse("dataAnterior");
				}
			}
		}
		
		VendaorcamentoFiltro totais;
		if(filtro.getNaoexibirresultado() == null || !filtro.getNaoexibirresultado()){
			totais = vendaorcamentoService.findForCalculoTotalGeralVendas(filtro);
			filtro.setTotalgeral(totais.getTotalgeral());
			filtro.setTotalgeralipi(totais.getTotalgeralipi());
			filtro.setQtdegeral(totais.getQtdegeral());
			
			if("TRUE".equals(parametrogeralService.buscaValorPorNome(Parametrogeral.VENDASALDOPRODUTO))){
				filtro.setSaldoprodutos(vendaorcamentoService.findForCalculoSaldoVendas(filtro));
			}
		}else {
			filtro.setTotalgeral(new Money());
			filtro.setTotalgeralipi(new Money());
			filtro.setQtdegeral(0.0);
			filtro.setSaldoprodutos(new Money());
		}
		
		filtro.setNaoexibirresultado(null);		
		request.setAttribute("EXIBIR_IPI_NA_VENDA", exibiripivenda);
		return lista;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}	
	
	/**
	 * Metodo para cancelar uma venda. 
	 * @param request
	 * @param venda
	 * @return
	 * @author Ramon Brazil
	 */
	public ModelAndView cancelar(WebRequestContext request, Venda venda){
		if (venda == null || venda.getIds() == null || "".equals(venda.getIds())) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(vendaorcamentoService.haveVendaorcamentoSituacao(venda.getIds(), false, Vendaorcamentosituacao.AUTORIZADO, Vendaorcamentosituacao.CANCELADA, Vendaorcamentosituacao.REPROVADO)){
			request.addError("Or�amento(s) com situa��o(�es) diferente de 'EM ABERTO'.");
			return sendRedirectToAction("listagem");
		}
		
		String[] vendas = venda.getIds().split(",");
		String justificativa = venda.getObservacaohistorico() != null ? venda.getObservacaohistorico() : "";
		
		for (String string : vendas) {
			Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
			vendaorcamentohistorico.setVendaorcamento(new Vendaorcamento(Integer.parseInt(string)));
			vendaorcamentohistorico.setAcao("Cancelamento");
			vendaorcamentohistorico.setObservacao(justificativa);
			vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);	
		}
		
		for (String string : vendas) {
			List<Vendaorcamentomaterial> listaItens = vendaorcamentomaterialService.findByMaterial(new Vendaorcamento(Integer.parseInt(string)));
			for(Vendaorcamentomaterial item: listaItens){
				vendaorcamentoService.cancelaAtualizacaoPreco(item);
			}
		}
		
		vendaorcamentoService.updateSituacaoVendaorcamento(venda.getIds(), Vendaorcamentosituacao.CANCELADA);
		request.addMessage("Or�amento(s) cancelado(s) com sucesso.");
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOrcamento");
		return null;
	}
	
	public ModelAndView reprovar(WebRequestContext request, Vendaorcamento vendaorcamento) {
		if (vendaorcamento == null || vendaorcamento.getIds() == null || "".equals(vendaorcamento.getIds())) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		MotivoReprovacaoFaturamento motivo = null;
		if(vendaorcamento.getMotivoreprovacaofaturamento() !=null){
			motivo = motivoReprovacaoFaturamentoService.load(vendaorcamento.getMotivoreprovacaofaturamento());
		}else{
			request.addError("Nenhum motivo de reprova��o selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		try {
			String[] vendaOrcamentos = vendaorcamento.getIds().split(",");
			String justificativa = motivo.getDescricao();
			if(vendaorcamento.getJustificativareprovar() !=null){
				justificativa = motivo.getDescricao().concat(" - " +vendaorcamento.getJustificativareprovar()); //!= null ? vendaorcamento.getJustificativareprovar() : "";
			}
			
					
			for (String vO : vendaOrcamentos) {
				Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
				vendaorcamentohistorico.setVendaorcamento(new Vendaorcamento(Integer.parseInt(vO)));
				vendaorcamentohistorico.setAcao("Reprovada");
				vendaorcamentohistorico.setObservacao(justificativa);
				vendaorcamentohistorico.setUsuarioaltera(SinedUtil.getUsuarioLogado());
				vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);	
			}
			vendaorcamentoService.updateMotivoReprovacao(vendaorcamento.getIds(),motivo,vendaorcamento.getJustificativareprovar());
			vendaorcamentoService.updateSituacaoVendaorcamento(vendaorcamento.getIds(), Vendaorcamentosituacao.REPROVADO);
			request.addMessage("Or�amento(s) reprovado(s) com sucesso.");
		} catch (Exception e) {
			request.addError("Erro ao tentar reprovar or�amento(s).");
		}
		
		SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOrcamento");
		
		return null;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Vendaorcamento bean) throws Exception {
		
		if(bean.getColaboradoraux() != null && bean.getColaboradoraux().getCdpessoa() != null){
			bean.setColaborador(bean.getColaboradoraux());
		}else{
			if(bean.getColaborador() == null){
				bean.setColaborador(SinedUtil.getUsuarioComoColaborador());
			}
		}
		
		String externo = parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO);
		if(externo.equals("TRUE")){
			if(vendaorcamentoService.isIdentificadorExternoRepetido(bean)){
				throw new SinedException(MSG_IDENTIFICADOR_EXTERNO);
			}
		}
		
		if(bean.getListavendaorcamentomaterial() != null && !bean.getListavendaorcamentomaterial().isEmpty()){
			StringBuilder historicoTroca = new StringBuilder();
			Double valorvalecompra = bean.getValorusadovalecompra() != null ? bean.getValorusadovalecompra().getValue().doubleValue() : 0d;
			Double desconto = bean.getDesconto() != null ? bean.getDesconto().getValue().doubleValue() : 0d;
			Double valortotal = bean.getTotalvendaSemArredondamento().getValue().doubleValue();
			
			valortotal += desconto + valorvalecompra;
			for(Vendaorcamentomaterial vm : bean.getListavendaorcamentomaterial()){
				vendaService.calcularMarkup(bean, vm, false);
				if(vm.getHistoricoTroca() != null && !"".equals(vm.getHistoricoTroca())){
					if(!"".equals(historicoTroca.toString())) historicoTroca.append("<br>");
					historicoTroca.append(vm.getHistoricoTroca());
				}
			}
		}
		
		super.salvar(request, bean);
		
		Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
		vendaorcamentohistorico.setVendaorcamento(bean);
		vendaorcamentohistorico.setAcao("Alterado");
		if(bean.getListavendaorcamentomaterial() != null && !bean.getListavendaorcamentomaterial().isEmpty()){
			StringBuilder historicoTroca = new StringBuilder();
			for(Vendaorcamentomaterial vm : bean.getListavendaorcamentomaterial()){
				if(vm.getHistoricoTroca() != null && !"".equals(vm.getHistoricoTroca())){
					if(!"".equals(historicoTroca.toString())) historicoTroca.append("<br>");
					historicoTroca.append(vm.getHistoricoTroca());
				}
			}
			if(!"".equals(historicoTroca.toString())){
				if(vendaorcamentohistorico.getObservacao() != null && !"".equals(vendaorcamentohistorico.getObservacao())){
					vendaorcamentohistorico.setObservacao(vendaorcamentohistorico.getObservacao() + " <br>Trocas de materiais efetuadas: " + historicoTroca.toString());
				}else {
					vendaorcamentohistorico.setObservacao("Trocas de materiais efetuadas: " + historicoTroca.toString());
				}
			}
		}
		vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);
		
		if (bean.getOportunidade() != null && bean.getOportunidade().getCdoportunidade() != null) {
			try {
				oportunidadeService.atualizarMaterialOportunidadeAposOrcamento(bean.getOportunidade(), bean, bean.getListavendaorcamentomaterial());
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("N�o foi poss�vel atualizar a oportunidade " + bean.getOportunidade().getCdoportunidade());
			}
		}
	}
	
	public void ajaxMaterial(WebRequestContext request, Material material ){
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		try {
			material = materialService.getUnidademedidaValorvenda(material);
			
			if (material.getUnidademedida() != null)
				view.println("var unidademedida = '" + material.getUnidademedida().getNome() + "';");
			else
				view.println("var unidademedida = '';");
			
			if (material.getValorvenda() != null)
				view.println("var valorvenda = '" + material.getValorvenda() + "';");
			else
				view.println("var valorvenda = '';");
			
			view.println("var sucesso = true;");						
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}
	
	public void ajaxJuros(WebRequestContext request, Prazopagamento prazopagamento ){
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		try {
			prazopagamento = prazopagamentoService.findForPrazopagamentoJuros(prazopagamento);
			
			if(prazopagamento.getJuros() != null){
				view.println("var juros = '" + prazopagamento.getJuros() + "';");
			}else{
				view.println("var juros = '<null>';");
			}
			
			view.println("var sucesso = true;");						
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}
		
	/**
	 * Gera uma venda, uma conta a raceber e uma movimenta��o de estoque
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#load
	 * @see br.com.linkcom.sined.geral.service.RateioService#saveOrUpdate
	 * @see br.com.linkcom.sined.geral.service.VendaService#saveOrUpdate
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#saveOrUpdate
	 * @see br.com.linkcom.sined.geral.service.DocumentoorigemService#saveOrUpdate
	 * @see br.com.linkcom.sined.geral.service.MaterialService#load
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#saveOrUpdate
	 * @param request
	 * @param venda
	 * @return
	 * @author Ramon Brazil
	 */
	public ModelAndView comprar(WebRequestContext request, Vendaorcamento vendaorcamento){	
		
		if(vendaorcamento.getOportunidade() != null && "TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_TIPOPEDIDOVENDA)) && vendaorcamento.getPedidovendatipo() == null){
			request.addError("O tipo de pedido de venda � obrigat�rio.");
			return continueOnAction("entrada", vendaorcamento);
		}
	
		String externo = parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO);
		if(externo.equals("TRUE")){
			if(vendaorcamentoService.isIdentificadorExternoRepetido(vendaorcamento)){
				request.addError(MSG_IDENTIFICADOR_EXTERNO);
				return continueOnAction("entrada", vendaorcamento);
			}else if (vendaorcamento.getIdentificadorexterno() ==null || vendaorcamento.getIdentificadorexterno() == ""){
				request.addError("Campo identificador externo � obrigat�rio.");
				return continueOnAction("entrada", vendaorcamento);
			}
		}
		if (!vendaorcamentoService.validaVendaorcamento(request, vendaorcamento, null)){
			if(vendaorcamento.getColaboradoraux() != null && vendaorcamento.getColaboradoraux().getCdpessoa() != null){
				vendaorcamento.setColaborador(vendaorcamento.getColaboradoraux());
			} else {
				if(vendaorcamento.getColaborador() == null){
					vendaorcamento.setColaborador(SinedUtil.getUsuarioComoColaborador());
				}
			}
			
			if(vendaorcamento.getEndereco() != null && vendaorcamento.getEndereco().getCdendereco() != null){
				request.setAttribute("enderecoEscolhido", "br.com.linkcom.sined.geral.bean.Endereco[cdendereco=" 
						+ vendaorcamento.getEndereco().getCdendereco() + "]");
			} else {
				request.setAttribute("enderecoEscolhido", "<null>");
			}
			
			return continueOnAction("entrada", vendaorcamento);			
		}
		
		//valida��o j� � feita em vendaorcamentoService.validaVendaorcamento(request, vendaorcamento)
//		if(!vendaorcamentoService.validaListaVendamaterial(request, vendaorcamento)){
//			return continueOnAction("entrada", vendaorcamento);
//		}
		
		if(!vendaorcamentoService.validaKitFlexivel(request, vendaorcamento)){
			return continueOnAction("entrada", vendaorcamento);
		}
		
		if(vendaorcamento.getColaboradoraux() != null && vendaorcamento.getColaboradoraux().getCdpessoa() != null){
			vendaorcamento.setColaborador(vendaorcamento.getColaboradoraux());
		} else {
			if(vendaorcamento.getColaborador() == null){
				if(SinedUtil.getUsuarioComoColaborador() == null){
					request.addError("Aten��o! Este usu�rio n�o � colaborador.");
					return continueOnAction("entrada", vendaorcamento);
				} else {
					vendaorcamento.setColaborador(SinedUtil.getUsuarioComoColaborador());
				}
			}
		}
		
		Colaborador colaborador = colaboradorService.findColaboradorusuario(vendaorcamento.getColaborador().getCdpessoa());
		if(colaborador == null || colaborador.getCdpessoa() == null){
			request.addError("Aten��o! Este usu�rio n�o � colaborador.");
			return new ModelAndView("/faturamento/crud/Venda", "venda", vendaorcamento);
		}
		
//		ATUALIZA O IDENTIFICADOR DA EMPRESA
		boolean usarSequencial = vendaorcamento.getIdentificadorAutomatico() != null && vendaorcamento.getIdentificadorAutomatico();
		boolean isCriar = vendaorcamento.getCdvendaorcamento() == null;
		if(usarSequencial && isCriar){
			Integer proximoIdentificador = empresaService.carregaProximoIdentificadorVendaorcamento(vendaorcamento.getEmpresa());
			if(proximoIdentificador == null){
				proximoIdentificador = 1;
			}
			empresaService.updateProximoIdentificadorVendaorcamento(vendaorcamento.getEmpresa(), proximoIdentificador+1);
			vendaorcamento.setIdentificador(proximoIdentificador.toString());
		}
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.IDENTIFICADOR_UNICO_PEDIDO_VENDA_ORCAMENTO)) && vendaorcamento.getEmpresa() != null && StringUtils.isNotBlank(vendaorcamento.getIdentificador()) && vendaorcamentoService.isIdentificadorCadastrado(vendaorcamento)) {
			request.addError("Aten��o! Identificador j� cadastrado no sistema.");
			return continueOnAction("entrada", vendaorcamento);
		}
		
		if(!vendaService.validaVendedorprincipal(vendaorcamento, request)){
			return continueOnAction("entrada", vendaorcamento);
		}
		vendaService.validaOrdenacao(request, vendaorcamento.getListavendaorcamentomaterial());
		if(request.getBindException().hasErrors()){
			return continueOnAction("entrada", vendaorcamento);
		}
		
		vendaorcamentoService.saveOrUpdate(vendaorcamento);
		
		Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
		vendaorcamentohistorico.setVendaorcamento(vendaorcamento);
		vendaorcamentohistorico.setAcao("Cria��o or�amento");
		
		if(!usarSequencial && isCriar){
			vendaorcamentohistorico.setObservacao("N�o foi usado o sequencial do identificador.");
		}
		if(vendaorcamento.getListavendaorcamentomaterial() != null && !vendaorcamento.getListavendaorcamentomaterial().isEmpty()){
			StringBuilder historicoTroca = new StringBuilder();
			for(Vendaorcamentomaterial vm : vendaorcamento.getListavendaorcamentomaterial()){
				if(vm.getHistoricoTroca() != null && !"".equals(vm.getHistoricoTroca())){
					if(!"".equals(historicoTroca.toString())) historicoTroca.append("<br>");
					historicoTroca.append(vm.getHistoricoTroca());
				}
			}
			if(!"".equals(historicoTroca.toString())){
				if(vendaorcamentohistorico.getObservacao() != null && !"".equals(vendaorcamentohistorico.getObservacao())){
					vendaorcamentohistorico.setObservacao(vendaorcamentohistorico.getObservacao() + " <br>Trocas de materiais efetuadas: " + historicoTroca.toString());
				}else {
					vendaorcamentohistorico.setObservacao("Trocas de materiais efetuadas: " + historicoTroca.toString());
				}
			}
		}
		
		if (vendaorcamento.getOportunidade() != null && vendaorcamento.getOportunidade().getCdoportunidade() != null) {
			try {
				oportunidadeService.updateOportunidadeAposOrcamento(vendaorcamento, vendaorcamento.getOportunidade());
				oportunidadeService.atualizarMaterialOportunidadeAposOrcamento(vendaorcamento.getOportunidade(), vendaorcamento, vendaorcamento.getListavendaorcamentomaterial());
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("N�o foi poss�vel atualizar a oportunidade " + vendaorcamento.getOportunidade().getCdoportunidade());
			}
			vendaorcamentohistorico.setObservacao((StringUtils.isNotBlank(vendaorcamentohistorico.getObservacao()) ? vendaorcamentohistorico.getObservacao() + ". " : "") +
												  "Or�amento gerado a partir da oportunidade: " + SinedUtil.makeLinkHistorico(vendaorcamento.getOportunidade().getCdoportunidade().toString(), "visualizarOportunidade"));
			

			if(vendaorcamento.getCdultimovendaorcamento() != null){
				Vendaorcamento ultimoOrcamento = new Vendaorcamento(vendaorcamento.getCdultimovendaorcamento());
				if(!vendaorcamentoService.haveVendaorcamentoDiferenteSituacao(ultimoOrcamento.getCdvendaorcamento().toString(), Vendaorcamentosituacao.EM_ABERTO)){
					Vendaorcamentohistorico historico = new Vendaorcamentohistorico();
					historico.setVendaorcamento(ultimoOrcamento);
					historico.setAcao("Cancelamento");
					historico.setObservacao("Cancelamento realizado ap�s a cria��o do or�amento " + SinedUtil.makeLinkHistorico(vendaorcamento.getCdvendaorcamento().toString(), "visualizaOrcamento"));
					vendaorcamentohistoricoService.saveOrUpdate(historico);	
					
					vendaorcamentoService.updateSituacaoVendaorcamento(ultimoOrcamento.getCdvendaorcamento().toString(), Vendaorcamentosituacao.CANCELADA);
				}
			}
		}
		
		vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);
		
		Integer cdvendaorcamento = vendaorcamento.getCdvendaorcamento();
		vendaorcamento.setCdvendaorcamento(cdvendaorcamento);
		
		if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.TABELA_VENDA_CLIENTE)) && vendaorcamento.getCliente() != null){
			try {
				List<Vendaorcamentomaterial> listaVendamaterialForCriarTabela = vendaorcamentoService.getListaVendaorcamentomaterialForCriarTabela(vendaorcamento);
				vendaorcamento.setListavendaorcamentomaterial(listaVendamaterialForCriarTabela);
				List<Materialtabelapreco> listaMaterialtabelapreco = materialtabelaprecoService.findByCliente(vendaorcamento.getCliente(),SinedDateUtils.castUtilDateForSqlDate(vendaorcamento.getDtorcamento()),null,null, materialtabelaprecoService.getTipocalculoByParametro());
				if(listaMaterialtabelapreco!=null && !listaMaterialtabelapreco.isEmpty())
					materialtabelaprecoService.atualizaTabelaprecoByCliente(vendaorcamento, listaMaterialtabelapreco);
				else { 
					materialtabelaprecoService.criaTabelaprecoByCliente(vendaorcamento, vendaorcamento.getCliente());
				}
			}  catch (DataIntegrityViolationException e) {
				e.printStackTrace();
				request.addError("N�o foi poss�vel criar Tabela de pre�o para o Cliente");
				if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_1")) {
					request.addError("Existe uma tabela cadastrada para o Cliente.");
				} else if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_2")) {
					request.addError("Existe uma tabela cadastrada para o Tipo de Pedido de venda.");
				}else if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_2")) {
					request.addError("Existe uma tabela cadastrada para o Prazo de pagmento.");
				} 
			} catch (Exception e) {
				request.addError("N�o foi poss�vel criar Tabela de pre�o para o Cliente");
			}
		}
		
		Boolean closeOnSave = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("closeOnSave")) ? request.getParameter("closeOnSave") : "FALSE");
		Boolean emitirComprovante = Boolean.valueOf(StringUtils.isNotBlank(request.getParameter("emitirComprovante")) ? request.getParameter("emitirComprovante") : "TRUE");
		
		request.addMessage("Or�amento realizado com sucesso.");
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<html><body>" +
				"<script>if("+emitirComprovante+"){window.open('../../faturamento/relatorio/VendaOrcamento?ACAO=gerar&cdvendaorcamento="+vendaorcamento.getCdvendaorcamento()+"');}" +
				(vendaorcamento.getOportunidade() != null && vendaorcamento.getOportunidade().getCdoportunidade() != null ?
					"window.location = '/" + SinedUtil.getContexto() + "/crm/crud/Oportunidade?ACAO=consultar&cdoportunidade=" + vendaorcamento.getOportunidade().getCdoportunidade() + "';" : 
					"window.location = 'VendaOrcamento?ACAO=criar';") +
				"if("+closeOnSave+"){window.close();}" +
				"</script>" +
				"</body></html>"
				);
		return null;
	}
	
	/**
	 * Carrrga um material de acordo com o codigo informado.
	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadWithCodFlex
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getQuantidadeDeMaterialEntrada
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getQuantidadeDeMaterialSaida
	 * @param request
	 * @param venda
	 * @author Ramon Brazil
	 */
	public void buscarMaterialAJAX(WebRequestContext request, Vendaorcamento vendaorcamento){	
		Materialtabelapreco materialtabelaprecoVenda = vendaorcamento.getMaterialtabelapreco();
		Material material = materialService.loadWithCodFlex(vendaorcamento.getCodigo().intValue());
		Boolean existMaterialsimilar = materialsimilarService.existMaterialsimilar(material);
		
		Pedidovendatipo pedidovendatipo = vendaorcamento.getPedidovendatipo();
		vendaorcamento.setMaterial(material);
		Double entrada = 0d;
		Double saida = 0d;
		Double qtddisponivel = 0d;
		Double qtde = null;
		Double valorvendaproducao = null;
		Boolean exibiritensproducaovenda = false;
		Boolean exibiritenskitvenda = false;
		Projeto projeto = vendaorcamento.getProjeto();
		Material materialPesquisaKitflexivel = request.getParameter("cdmaterialkitflexivel") != null ? new Material(Integer.parseInt(request.getParameter("cdmaterialkitflexivel"))) : null;
		Double qtdereservada = null;
		
		Materialgrupocomissaovenda mgca = null;
		if(vendaorcamento.getAgencia() != null){
			mgca = materialgrupocomissaovendaService.getComissaoAgencia(
							vendaorcamento.getMaterial(),
							vendaorcamento.getFornecedor(),
							vendaorcamento.getAgencia(),
							null,
							pedidovendatipo
							);
		}
		
		Produto produto = produtoService.carregaAlturaComprimentoLargura(new Produto(material.getCdmaterial(), null, null));
		Boolean showBotaoLargCompAlt = Boolean.FALSE;
		
		boolean alturaNula = vendaorcamento.getAlturas() == null;
		boolean larguraNula = vendaorcamento.getLarguras() == null;
		
		Double altura = null;
		Double largura = null;
		Double comprimento = null;
		if(produto != null && (produto.getAltura() != null || produto.getComprimento() != null || produto.getLargura() != null)){
			showBotaoLargCompAlt = Boolean.TRUE;
			if(produto.getFatorconversao() == null) produto.setFatorconversao(1000d);
			if(produto.getAltura() != null){
				altura  = produto.getAltura()/produto.getFatorconversao();
			}
			if(produto.getLargura() != null){
				largura  = produto.getLargura()/produto.getFatorconversao();
			}
			if(produto.getComprimento() != null){
				comprimento  = produto.getComprimento()/produto.getFatorconversao();
			}
		}
		if(vendaorcamento.getAlturas() == null) vendaorcamento.setAlturas(1d);
		if(vendaorcamento.getLarguras() == null) vendaorcamento.setLarguras(1d);
		
		List<Material> listaMaterialitemgrade = materialService.findMaterialitemByMaterialmestregrade(material);
		boolean existeitemgrade = listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty() && !materialService.isControleMaterialgrademestre(material);
		boolean pesquisasomentemestre = listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty() && materialService.isControleItemGradeEPesquisasomentemestre(material);
		boolean existecontroleGrade = existeitemgrade || material.getMaterialmestregrade() != null;
		
		if (vendaorcamento.getMaterial() != null){
			if(vendaorcamento.getMaterial().getVendapromocional() != null && vendaorcamento.getMaterial().getVendapromocional()){
				vendaorcamento.setCodigo(vendaorcamento.getCodigo());
				vendaorcamento.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
				vendaorcamento.setEmpresa(vendaorcamento.getEmpresa());
				material = materialService.loadMaterialPromocionalComMaterialrelacionado(vendaorcamento.getCodigo().intValue());
				if(material != null){
					if(material.getExibiritenskitvenda() != null){
						exibiritenskitvenda = material.getExibiritenskitvenda();
					}
					try{
						material.setProduto_altura(vendaorcamento.getAlturas());
						material.setProduto_largura(vendaorcamento.getLarguras());
						material.setQuantidade(vendaorcamento.getQuantidades());
						if(exibiritenskitvenda){
							pedidovendaService.preecheValorVendaComTabelaPrecoItensKit(material, pedidovendatipo, vendaorcamento.getCliente(), vendaorcamento.getEmpresa(), null);
							vendaorcamento.getMaterial().setConsiderarValorvendaCalculado(true);
						}
						valorvendaproducao = materialrelacionadoService.getValorvendakit(material);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(!exibiritenskitvenda && (request.getParameter("somentevenda") == null || !"true".equals(request.getParameter("somentevenda")))){
						valorvendaproducao = material.getValorvenda();
					}
				}
				if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
					qtdereservada = 0d;
					for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
						if(materialrelacionado.getMaterialpromocao() != null){	
							entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(materialrelacionado.getMaterialpromocao(), vendaorcamento.getLocalarmazenagem(), vendaorcamento.getEmpresa(), true, projeto);
							if (entrada == null){
								entrada = 0d;
							}
							saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(materialrelacionado.getMaterialpromocao(), vendaorcamento.getLocalarmazenagem(), vendaorcamento.getEmpresa(), true, projeto);
							if (saida == null){
								saida = 0d;
							}
							qtddisponivel = (entrada-saida)/materialrelacionado.getQuantidade();
							qtddisponivel = SinedUtil.roundByUnidademedidaRoundFloor(qtddisponivel, material.getUnidademedida());
							if(qtde == null)
								qtde = qtddisponivel;
							else if(qtddisponivel < qtde){
								qtde = qtddisponivel;
							}
						}
					}
				}
			}else if(vendaorcamento.getMaterial().getKitflexivel() != null && vendaorcamento.getMaterial().getKitflexivel()){
				Vendaorcamento vendaaux = new Vendaorcamento();
				vendaorcamento.setCodigo(vendaorcamento.getCodigo());
				vendaorcamento.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
				vendaorcamento.setEmpresa(vendaorcamento.getEmpresa());
				material = materialService.loadKitFlexivel(vendaorcamento.getMaterial());
				if(material != null){
					try{
						material.setProduto_altura(vendaorcamento.getAlturas());
						material.setProduto_largura(vendaorcamento.getLarguras());
						material.setQuantidade(vendaorcamento.getQuantidades());
						pedidovendaService.preecheValorVendaComTabelaPrecoItensKitflexivel(material, pedidovendatipo, vendaorcamento.getCliente(), vendaorcamento.getEmpresa(), vendaorcamento.getPrazopagamento());
						valorvendaproducao = materialkitflexivelService.getValorvendakitflexivel(material);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(material.getListaMaterialkitflexivel() != null && !material.getListaMaterialkitflexivel().isEmpty()){
					for(Materialkitflexivel materialkitflexivel : material.getListaMaterialkitflexivel()){
						if(materialkitflexivel.getMaterialkit() != null && materialkitflexivel.getQuantidade() != 0d){	
							vendaaux.setMaterial(materialkitflexivel.getMaterialkit());
							vendaaux.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
							vendaaux.setEmpresa(vendaorcamento.getEmpresa());
							entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), true, projeto);
							if (entrada == null){
								entrada = 0d;
							}
							saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), true, projeto);
							if (saida == null){
								saida = 0d;
							}
							qtddisponivel = (entrada-saida)/materialkitflexivel.getQuantidade();
							if(qtde == null)
								qtde = qtddisponivel;
							else if(qtddisponivel < qtde){
								qtde = qtddisponivel;
							}
						}
					}
				}
			}else if(vendaorcamento.getMaterial().getProducao() != null && vendaorcamento.getMaterial().getProducao()){
				material = materialService.loadMaterialComMaterialproducao(vendaorcamento.getMaterial());
				if(material != null){
					Set<Materialproducao> listaProducao = material.getListaProducao();
					for (Materialproducao materialproducao : listaProducao) {
						if(materialproducao != null && materialproducao.getExibirvenda() != null && materialproducao.getExibirvenda()){
							exibiritensproducaovenda = true;
						}
					}
					try{
						material.setProduto_altura(vendaorcamento.getAlturas());
						material.setProduto_largura(vendaorcamento.getLarguras());
						material.setQuantidade(vendaorcamento.getQuantidades());
						if(exibiritensproducaovenda){
							pedidovendaService.preecheValorVendaComTabelaPrecoItensProducao(material, pedidovendatipo, vendaorcamento.getCliente(), vendaorcamento.getEmpresa(), null);
						}
						valorvendaproducao = materialproducaoService.getValorvendaproducao(material);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(/*exibiritensproducaovenda && */material.getValorvenda() != null){
						if(request.getParameter("somentevenda") == null || !"true".equals(request.getParameter("somentevenda"))){
							valorvendaproducao = material.getValorvenda();
						}
					}
					
					entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(vendaorcamento.getMaterial(),vendaorcamento.getLocalarmazenagem(),vendaorcamento.getEmpresa(), true, projeto);
					if (entrada == null){
						entrada = 0d;
					}
					saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(vendaorcamento.getMaterial(),vendaorcamento.getLocalarmazenagem(),vendaorcamento.getEmpresa(), true, projeto);
					if (saida == null){
						saida = 0d;
					}
					
					if(vendaorcamento.getMaterial().getProducao() != null && vendaorcamento.getMaterial().getProducao()){
						qtde = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
						if(qtde <= 0){
							Material materialP = materialService.loadMaterialComMaterialproducao(vendaorcamento.getMaterial());
							if(materialP != null){
								Venda vendaP = new Venda();
								vendaP.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
								vendaP.setEmpresa(vendaorcamento.getEmpresa());
								vendaP.setProjeto(vendaorcamento.getProjeto());
								qtde = vendaService.getQtdeDisponivelMaterialproducao(materialP, vendaP);
							}
						}
					}
				}
			}else{
				entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(vendaorcamento.getMaterial(), vendaorcamento.getLocalarmazenagem(), vendaorcamento.getEmpresa(), true, projeto);
				if (entrada == null){
					entrada = 0d;
				}
				saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(vendaorcamento.getMaterial(), vendaorcamento.getLocalarmazenagem(), vendaorcamento.getEmpresa(), true, projeto);
				if (saida == null){
					saida = 0d;
				}
			}
		}
		if(qtde != null){
			qtddisponivel = qtde.doubleValue();
		}else {
			qtddisponivel = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
		}
		
		Double multiplicador = 1.0;
		vendaorcamento.getMaterial().setVendaunidademedida(vendaorcamento.getMaterial().getUnidademedida());
		Double peso = materialService.getPesoMaterial(vendaorcamento.getMaterial(), altura, largura, comprimento);
		if(vendaorcamento.getMaterial() != null && 
				vendaorcamento.getMaterial().getPesoliquidovalorvenda() != null && 
				vendaorcamento.getMaterial().getPesoliquidovalorvenda() &&
				peso != null){
			multiplicador = peso;
		}
		
		Boolean metrocubicovalorvenda = vendaorcamento.getMaterial().getMetrocubicovalorvenda();
		if(metrocubicovalorvenda == null) metrocubicovalorvenda = Boolean.FALSE;
		
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		if(vendaorcamento.getMaterial() != null){		
			Double valorvenda = vendaorcamento.getMaterial().getValorvenda();
			Double valorvendasemdesconto = valorvenda;
			
			boolean isValorvendaFormula = materialformulavalorvendaService.setValorvendaByFormula(vendaorcamento.getMaterial(), 
					vendaorcamento.getAlturas() != null && !alturaNula ? vendaorcamento.getAlturas() : altura, 
					vendaorcamento.getLarguras() != null && !larguraNula ? vendaorcamento.getLarguras() : largura,
					vendaorcamento.getComprimentos() != null ? vendaorcamento.getComprimentos() : comprimento,
					vendaorcamento.getValorfrete() != null ? vendaorcamento.getValorfrete().getValue().doubleValue() : null);
			
			if(isValorvendaFormula){
				valorvenda = vendaorcamento.getMaterial().getValorvenda();
				valorvendasemdesconto = vendaorcamento.getMaterial().getValorvendaSemDescontoOuValorvenda();
			}
			
			Double valortabelapreco = null;
			Double valortabelaprecosemdesconto = null;
			Materialtabelaprecoitem materialtabelaprecoitem = null;
			Double valorMaximoTabela = null;
			Double valorMinimoTabela = null;
			Double valorMaximo = null;
			Double valorMinimo = null;

			if(!exibiritenskitvenda){
				Aux_VendamaterialTabelapreco vendaorcamentomaterialAuxPreco = new Aux_VendamaterialTabelapreco(material);
				valorMaximo = vendaorcamento.getMaterial().getValorvendamaximo();
				valorMinimo = vendaorcamento.getMaterial().getValorvendaminimo();
				Materialtabelapreco tabelapreco = null;
				if(materialtabelaprecoVenda != null && materialtabelaprecoVenda.getCdmaterialtabelapreco() != null){
					materialtabelaprecoitem = materialtabelaprecoitemService.getItemTabelaPrecoAtual(vendaorcamento.getMaterial(), vendaorcamento.getMaterial().getUnidademedida(), materialtabelaprecoVenda);
					if(materialtabelaprecoitem != null){
						valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimocomtaxa(valorMinimo);
						valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximocomtaxa(valorMaximo);
						vendaService.calculaValoresForVenda(vendaorcamentomaterialAuxPreco, materialtabelaprecoitem.getMaterialtabelapreco(), materialtabelaprecoitem, valorvenda, materialPesquisaKitflexivel != null, true);
						valortabelapreco = vendaorcamentomaterialAuxPreco.getValorvendamaterial();
						valortabelaprecosemdesconto = vendaorcamentomaterialAuxPreco.getValorvendamaterialsemdesconto();
					}else {
						tabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(vendaorcamento.getMaterial(), vendaorcamento.getCliente(), null, vendaorcamento.getPedidovendatipo(), vendaorcamento.getEmpresa(), materialPesquisaKitflexivel, materialtabelaprecoVenda);
						if (tabelapreco != null) {
							vendaService.calculaValoresForVenda(vendaorcamentomaterialAuxPreco, tabelapreco, materialtabelaprecoitem, valorvenda, materialPesquisaKitflexivel != null, false);
							valortabelapreco = vendaorcamentomaterialAuxPreco.getValorvendamaterial();
							valortabelaprecosemdesconto = vendaorcamentomaterialAuxPreco.getValorvendamaterialsemdesconto();
						}
					}
				}
				
				if(materialtabelaprecoitem == null && tabelapreco == null){
					tabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(vendaorcamento.getMaterial(), vendaorcamento.getCliente(), null, vendaorcamento.getPedidovendatipo(), vendaorcamento.getEmpresa(), materialPesquisaKitflexivel, null);
					if(tabelapreco != null){
						vendaService.calculaValoresForVenda(vendaorcamentomaterialAuxPreco, tabelapreco, materialtabelaprecoitem, valorvenda, materialPesquisaKitflexivel != null, false);
						valortabelapreco = vendaorcamentomaterialAuxPreco.getValorvendamaterial();
						valortabelaprecosemdesconto = vendaorcamentomaterialAuxPreco.getValorvendamaterialsemdesconto();
					}else {
						materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(vendaorcamento.getMaterial(), vendaorcamento.getCliente(), null, vendaorcamento.getPedidovendatipo(), vendaorcamento.getEmpresa(), vendaorcamento.getMaterial().getUnidademedida());
						if(materialtabelaprecoitem != null){
							valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimocomtaxa(valorMinimo);
							valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximocomtaxa(valorMaximo);
							vendaService.calculaValoresForVenda(vendaorcamentomaterialAuxPreco, materialtabelaprecoitem.getMaterialtabelapreco(), materialtabelaprecoitem, valorvenda, materialPesquisaKitflexivel != null, true);
							valortabelapreco = vendaorcamentomaterialAuxPreco.getValorvendamaterial();
							valortabelaprecosemdesconto = vendaorcamentomaterialAuxPreco.getValorvendamaterialsemdesconto();
						}
					}
				}
				
				if(vendaorcamentomaterialAuxPreco.getPercentualdesconto() != null){
					material.setPercentualdescontoTabela(vendaorcamentomaterialAuxPreco.getPercentualdesconto().getValue().doubleValue());
				}
				
				if(valorvendaproducao != null){
					valorvenda = valorvendaproducao;
				}
				
				if(valortabelapreco != null){
					valorvenda = valortabelapreco;
				}
				
				if(valortabelaprecosemdesconto != null){
					valorvendasemdesconto = valortabelaprecosemdesconto;
				}
				
				if(valorMaximoTabela != null){
					valorMaximo = valorMaximoTabela;
				}
				if(valorMinimoTabela != null){
					valorMinimo = valorMinimoTabela;
				}
			}else{
				if(valorvendaproducao != null){
					valorvenda = valorvendaproducao;
					valorvendasemdesconto = valorvenda;
				}
			}
			if(! (Boolean.TRUE.equals(vendaorcamento.getMaterial().getKitflexivel()) || Boolean.TRUE.equals(vendaorcamento.getMaterial().getVendapromocional())
					|| Boolean.TRUE.equals(vendaorcamento.getMaterial().getProducao()) || existecontroleGrade)){
				vendaorcamento.getMaterial().setValorcusto(materialService.getValorCustoAtual(vendaorcamento.getMaterial(), vendaorcamento.getUnidademedida(), vendaorcamento.getEmpresa()));
			}
			
			if(qtdereservada == null){
				qtdereservada = pedidovendamaterialService.getQtdeReservada(material, vendaorcamento.getEmpresa(), vendaorcamento.getLocalarmazenagem());
			}
			
			Boolean buscarDtVencimentoLote = Boolean.parseBoolean(request.getParameter("SELECAO_AUTOMATICA_LOTE_FATURAMENTO"));
			String dtMenorVencimentoLote = "";
			if(buscarDtVencimentoLote){
				Date dtMenorVencimento = loteestoqueService.buscarDtVencimentoLote(material.getCdmaterial(), vendaorcamento.getEmpresa().getCdpessoa(), vendaorcamento.getLocalarmazenagem() != null? vendaorcamento.getLocalarmazenagem().getCdlocalarmazenagem() : null);
				if(dtMenorVencimento != null){
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					dtMenorVencimentoLote = sdf.format(dtMenorVencimento);					
				}
				
			}
			
			boolean obrigarLoteOrcamento = false;
			if(pedidovendatipo != null){
				pedidovendatipo = pedidovendatipoService.load(pedidovendatipo, "pedidovendatipo.obrigarLoteOrcamento");
				obrigarLoteOrcamento = pedidovendatipo.getObrigarLoteOrcamento();
			}
			
			view.println("var existecontroleGrade = '" + existecontroleGrade + "';");
			view.println("var existeitemgrade = '" + existeitemgrade + "';");
			view.println("var pesquisasomentemestre = '" + pesquisasomentemestre + "';");
			view.println("var vendapromocional = '" + (vendaorcamento.getMaterial().getVendapromocional() != null ?  vendaorcamento.getMaterial().getVendapromocional() : false )+ "';");
			view.println("var identificacaomaterial = '" + (material.getIdentificacao() != null ? material.getIdentificacao() : "") + "';");
			view.println("var multiplicador = '" + SinedUtil.descriptionDecimal(multiplicador) + "';");
			view.println("var metrocubicovalorvenda = '" + metrocubicovalorvenda + "';");
			view.println("var valorproduto = '" + SinedUtil.descriptionDecimal(valorvenda) + "';");
			view.println("var valorcustomaterial = '" + (vendaorcamento.getMaterial().getValorcusto() != null ? SinedUtil.descriptionDecimal(vendaorcamento.getMaterial().getValorcusto()) : "")+ "';");
			view.println("var valorvendamaterial = '" + (valorvendasemdesconto != null ? SinedUtil.descriptionDecimal(valorvendasemdesconto) : "")+ "';");
			view.println("var valorminimo = '" + SinedUtil.descriptionDecimal(valorMinimo) + "';");
			view.println("var valormaximo = '" + SinedUtil.descriptionDecimal(valorMaximo) + "';");
			view.println("var percentualdescontoTabelapreco ='" + SinedUtil.descriptionDecimal(material.getPercentualdescontoTabela()) + "';");
			view.println("var naoexibirminmaxvenda = '" + (vendaorcamento.getMaterial().getNaoexibirminmaxvenda() != null ? vendaorcamento.getMaterial().getNaoexibirminmaxvenda() : false) + "';");
			view.println("var qtddisponivel = '" + SinedUtil.descriptionDecimal(qtddisponivel) + "';");
			view.println("var qtdereservada ='" +SinedUtil.descriptionDecimal(qtdereservada) + "';");
			view.println("var exibiritensproducaovenda ='" + exibiritensproducaovenda + "';");
			view.println("var exibiritenskitvenda ='" + exibiritenskitvenda + "';");
			view.println("var kitflexivel = '" + (vendaorcamento.getMaterial().getKitflexivel() != null ?  vendaorcamento.getMaterial().getKitflexivel() : false )+ "';");
			view.println("var producao ='" + (vendaorcamento.getMaterial().getProducao() == null ? false : vendaorcamento.getMaterial().getProducao()) + "';");
			view.println("var materialservico = '" + vendaorcamento.getMaterial().getServico() + "';");
			view.println("var pesoliquidovalorvenda = '" + (vendaorcamento.getMaterial().getPesoliquidovalorvenda() != null && vendaorcamento.getMaterial().getPesoliquidovalorvenda()) + "';");
			view.println("var peso = '" + SinedUtil.descriptionDecimal(materialService.getPesoMaterial(vendaorcamento.getMaterial(), altura, largura, comprimento)) + "';");
			view.println("var pesobruto = '" + SinedUtil.descriptionDecimal(vendaorcamento.getMaterial().getPesobruto()) + "';");
			view.println("var tributacaoipi ='" + (vendaorcamento.getMaterial().getTributacaoipi() == null ? false : vendaorcamento.getMaterial().getTributacaoipi()) + "';");
			view.println("var materialobrigalote = '" + (material.getObrigarlote() != null ? material.getObrigarlote() : Boolean.FALSE).toString() + "';");
			view.println("var dtMenorVencimentoLote ='" + dtMenorVencimentoLote + "';");
			
			view.println("var obrigarLoteOrcamento ='" + obrigarLoteOrcamento + "';");

			if((altura != null || comprimento != null || largura != null)){
				view.println("var comprimentos = '" + (comprimento != null ? new DecimalFormat("#.#######").format(comprimento) : "") + "'; ");
				view.println("var alturas = '" + (altura != null ? new DecimalFormat("#.#######").format(altura) : "") + "'; ");
				view.println("var larguras = '" + (largura != null ? new DecimalFormat("#.#######").format(largura) : "") + "'; ");
//				view.println("var fatorconversaocomprimento = '" + (produto.getFatorconversao() != null ? produto.getFatorconversao() : "") + "'; ");
				view.println("var fatorconversaocomprimento = ''; ");
				view.println("var margemarredondamento = '" + (produto.getMargemarredondamento() != null ? new DecimalFormat("#.#####").format(produto.getMargemarredondamento()/1000.0) : "") + "'; ");
			}
			view.println("var showBotaoLargCompAlt = '" + showBotaoLargCompAlt + "'; ");
			view.println("var existMaterialsimilar = '" + existMaterialsimilar + "'; ");
			view.println("var qtdeunidade = '" + (vendaorcamento.getMaterial().getQtdeunidade() != null && vendaorcamento.getMaterial().getConsiderarvendamultiplos() != null && vendaorcamento.getMaterial().getConsiderarvendamultiplos() ? vendaorcamento.getMaterial().getQtdeunidade() : "") + "'; ");
			view.println("var considerarvendamultiplos = '" + (vendaorcamento.getMaterial().getConsiderarvendamultiplos() != null ? vendaorcamento.getMaterial().getConsiderarvendamultiplos() : "") + "'; ");
			if (vendaorcamento.getMaterial().getArquivo() != null){
				carregarImagem(request, vendaorcamento.getMaterial().getArquivo(), "exibefoto");
				view.println("var cdarquivo = '" + vendaorcamento.getMaterial().getArquivo().getCdarquivo() + "';");
			}
			else view.println("var cdarquivo = '" + "" + "';");
			produto = produtoService.load(new Produto(vendaorcamento.getMaterial().getCdmaterial(), null, null));
			if(produto != null && produto.getObservacao() != null){
				view.println("var anotacoes = \"" + Util.strings.addScapesToDescription(produto.getObservacao().replaceAll("\\r?\\n", " ")) + "\"; ");
			}
			else view.println("var anotacoes = '" + "" + "';");
			
			List<Materialunidademedida> unidades = materialunidademedidaService.findByMaterial(produto);
			
			StringBuilder sb = new StringBuilder();
			sb.append("[");
			DecimalFormat df = new DecimalFormat("#.#######");
			boolean incluirUnidadePrincipal = true;
			for (Materialunidademedida unidade : unidades){
				
				if (unidade != unidades.get(0))
					sb.append(",");
				
				String nomeUnidade = unidade.getUnidademedida().getNome().replace("\"", "\\\"");

				if (material.getUnidademedida().equals(unidade.getUnidademedida())){
					incluirUnidadePrincipal = false;
					nomeUnidade = "Quantidade restante";
				}

				sb.append("{");
				sb.append("nomeUnidademedida:\"").append(nomeUnidade).append("\",");
				sb.append("unidademedida:\"").append(Util.strings.toStringIdStyled(unidade.getUnidademedida())).append("\",");
				sb.append("fracao:\"").append(df.format(unidade.getFracao())).append("\",");
				sb.append("qtdereferencia:\"").append(df.format(unidade.getQtdereferencia())).append("\"");
				sb.append("}");
			}
			if (unidades.size() > 0 && incluirUnidadePrincipal){
				sb.append(",{");
				sb.append("nomeUnidademedida:\"").append("Quantidade restante").append("\",");
				sb.append("unidademedida:\"").append(Util.strings.toStringIdStyled(material.getUnidademedida())).append("\",");
				sb.append("fracao:\"1\",");
				sb.append("qtdereferencia:\"1\"");
				sb.append("}");
			}
			sb.append("]");

			
			view.println("var listaMaterialUnidademedida = " + sb.toString() + ";");
			view.println("var unidademedidaMaterial = '" + Util.strings.toStringIdStyled(material.getUnidademedida()) + "';");
			view.println("var percentualcomissaoagencia = '" + (mgca != null && mgca.getComissionamento() != null && mgca.getComissionamento().getPercentual() != null ?  SinedUtil.descriptionDecimal(mgca.getComissionamento().getPercentual()) : "") + "';");
			
			Double quantidadeDisponivelMenosReservada = qtddisponivel - qtdereservada;
			view.println("var quantidadeDisponivelMenosReservada = '" + SinedUtil.descriptionDecimal(quantidadeDisponivelMenosReservada) + "';");
		}
	}
	
	/**
	 * M�todo ajax para atualizar o valor de venda caso o comprimento seja alterado
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxAtualizarvalorvenda(WebRequestContext request, Material material){
		Boolean atualizarvalorvenda = Boolean.FALSE;
		BigDecimal valorvenda = null;
		BigDecimal valorMin = new BigDecimal(0d);
		BigDecimal valorMax = new BigDecimal(0d);
		
		String largurastr = request.getParameter("largura");
		String alturastr = request.getParameter("altura");
		String comprimentostr = request.getParameter("comprimento");
		String cdunidademedida = request.getParameter("cdunidademedida");
		String margemarredondamentostr = request.getParameter("margemarredondamento");
		String fretestr = request.getParameter("valorfrete");
		
		BigDecimal fatorconversao = null;
		boolean considerardimensoesvendaconversao = false;
		if(material != null && material.getCdmaterial() != null && cdunidademedida != null && !"".equals(cdunidademedida)){
			if((largurastr != null && alturastr != null) || (largurastr != null && comprimentostr != null) || (alturastr != null && comprimentostr != null)){
				Double largura = largurastr != null && !"".equals(largurastr) ? Double.parseDouble(largurastr) : 1.0;
				Double altura = alturastr != null && !"".equals(alturastr) ? Double.parseDouble(alturastr) : 1.0;
				Double comprimentooriginal = comprimentostr != null && !"".equals(comprimentostr) ? Double.parseDouble(comprimentostr) : 1.0;
				Double comprimento = comprimentooriginal != null ? comprimentooriginal : 1.0;
				Double frete = fretestr != null && !"".equals(fretestr) ? Double.parseDouble(fretestr.replaceAll("\\.", "").replace(",", ".")) : null;
				if(margemarredondamentostr != null && !"".equals(margemarredondamentostr)){
					Double margemarredondamento = Double.parseDouble(margemarredondamentostr);
					if(margemarredondamento != 0 && (comprimentooriginal % margemarredondamento) != 0){
						comprimento = comprimentooriginal + (margemarredondamento - (comprimentooriginal % margemarredondamento));
					}
				}
				material = materialService.unidadeMedidaMaterial(material);
				boolean isValorvendaFormula = materialformulavalorvendaService.setValorvendaByFormula(material, altura, largura, comprimento, frete);
				Material material_aux = materialService.findListaMaterialunidademedida(material);; 
				if(material_aux != null && material_aux.getUnidademedida() != null && material_aux.getUnidademedida().getCdunidademedida() != null && 
						!material_aux.getUnidademedida().getCdunidademedida().toString().equals(cdunidademedida)){
					if(material_aux != null && SinedUtil.isListNotEmpty(material_aux.getListaMaterialunidademedida())){
						for(Materialunidademedida materialunidademedida : material_aux.getListaMaterialunidademedida()){
							if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().getCdunidademedida() != null &&
									materialunidademedida.getFracao() != null && materialunidademedida.getUnidademedida().getCdunidademedida().toString().equals(cdunidademedida)){
								considerardimensoesvendaconversao = materialunidademedida.getConsiderardimensoesvendaconversao() != null ? materialunidademedida.getConsiderardimensoesvendaconversao() : false;
							}
						}
					}
					BigDecimal qtde = new BigDecimal(1);
					if(largura != 0 && comprimento != 0 && altura != 0){
						qtde = new BigDecimal(altura).multiply(new BigDecimal(largura)).multiply(new BigDecimal(comprimento), MathContext.DECIMAL128);
						fatorconversao =  new BigDecimal(1.0).divide(qtde, MathContext.DECIMAL128);
					}
					BigDecimal fracao = new BigDecimal(1.0);
					if(qtde != null && material.getValorvenda() != null){
						atualizarvalorvenda = Boolean.TRUE;
						if(qtde.doubleValue() != 0){
							fracao = fracao.divide(qtde, MathContext.DECIMAL128);
						}
						valorvenda = new BigDecimal(material.getValorvenda()).divide(fracao, MathContext.DECIMAL128);
						if(material.getValorvendaminimo() != null){
							valorMin = new BigDecimal(material.getValorvendaminimo()).divide(fracao, MathContext.DECIMAL128);
						}
						if(material.getValorvendamaximo() != null){
							valorMax = new BigDecimal(material.getValorvendamaximo()).divide(fracao, MathContext.DECIMAL128);
						}
					}
				}else if(isValorvendaFormula && material.getValorvenda() != null){
					atualizarvalorvenda = Boolean.TRUE;
					valorvenda = new BigDecimal(material.getValorvenda());
				}				
			}
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("atualizarvalorvenda", atualizarvalorvenda);
		json.addObject("valorvenda", valorvenda != null ? valorvenda.doubleValue() : "");
		json.addObject("valormin", valorMin != null ? valorMin.doubleValue() : "");
		json.addObject("valormax", valorMax != null ? valorMax.doubleValue() : "");
		json.addObject("fatorconversao", fatorconversao != null ? fatorconversao.doubleValue() : "");
		json.addObject("considerardimensoesvendaconversao", considerardimensoesvendaconversao);
		return json;
		
	}
	
	/**
	 * M�todo ajax que busca as informa��es do material promocional
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialService#loadMaterialPromocionalComMaterialrelacionado(Integer cdmaterial)
	 *
	 * @param request
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView buscarInfMaterialPromocionalAJAX(WebRequestContext request, Vendaorcamento vendaorcamento){	
		Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vendaorcamento.getCodigo().intValue());
		vendaorcamento.setMaterial(material);
		material.setListaMaterialrelacionado(materialrelacionadoService.ordernarListaKit(material.getListaMaterialrelacionado()));
		
		try{
			material.setProduto_altura(vendaorcamento.getAlturas());
			material.setProduto_largura(vendaorcamento.getLarguras());
			material.setQuantidade(vendaorcamento.getQuantidades());
			pedidovendaService.preecheValorVendaComTabelaPrecoItensKit(material, vendaorcamento.getPedidovendatipo(), vendaorcamento.getCliente(), vendaorcamento.getEmpresa(), null);
			materialrelacionadoService.calculaQuantidadeKit(material);			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean itensInativos = false;
		if(material != null && material.getListaMaterialrelacionado() != null && 
				!material.getListaMaterialrelacionado().isEmpty()){
			for(Materialrelacionado mr : material.getListaMaterialrelacionado()){
				if(mr.getMaterialpromocao() != null){
					mr.setExistematerialsimilar(materialsimilarService.existMaterialsimilar(mr.getMaterialpromocao()));
					if((mr.getMaterialpromocao().getAtivo() == null || 
						!mr.getMaterialpromocao().getAtivo())){
						itensInativos = true;
					}
				}
			}
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", material);
		json.addObject("qtde", vendaorcamento.getQuantidades());
		json.addObject("itensInativos", itensInativos);
		json.addObject("permitiresoquenegativo", localarmazenagemService.getPermitirestoquenegativo(vendaorcamento.getLocalarmazenagem()));
		
		return json;
	}
	
	public ModelAndView buscarInfMaterialProducaoAJAX(WebRequestContext request, Venda venda){	
		Material material = materialService.loadMaterialComMaterialproducao(new Material(Integer.parseInt(venda.getCodigo())));
		venda.setMaterial(material);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
		
		try{
			material.setProduto_altura(venda.getAlturas());
			material.setProduto_largura(venda.getLarguras());
			material.setQuantidade(venda.getQuantidades());
			pedidovendaService.preecheValorVendaComTabelaPrecoItensProducao(material, venda.getPedidovendatipo(), venda.getCliente(), venda.getEmpresa(), null);
			materialproducaoService.getValorvendaproducao(material);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Materialproducao materialproducaoMestre = materialproducaoService.createItemProducaoByMaterialmestre(material.getListaProducao(), material);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), true));
		
		for (Materialproducao materialproducao : material.getListaProducao()) {
			if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && venda.getQuantidades() != null){
				materialproducao.setConsumo(materialproducao.getConsumo() * venda.getQuantidades());
				
				materialformulavalorvendaService.setValorvendaByFormula(materialproducao.getMaterial(), 
						venda.getAlturas() != null ? venda.getAlturas() : null, 
						venda.getLarguras() != null ? venda.getLarguras() : null, 
						venda.getComprimentos() != null ? venda.getComprimentos() : null,
						venda.getValorfrete() != null ? venda.getValorfrete().getValue().doubleValue() : null);
			}
		}
		if(materialproducaoMestre != null){
			materialproducaoMestre.setVendaaltura(venda.getAlturas());
			materialproducaoMestre.setVendacomprimento(venda.getComprimentos());
			materialproducaoMestre.setVendalargura(venda.getLarguras());
		}
		if(materialproducaoMestre != null && materialproducaoMestre.getConsumo() != null && materialproducaoMestre.getConsumo() > 0 && 
				venda.getQuantidades() != null){
			materialproducaoMestre.setConsumo(materialproducaoMestre.getConsumo() * venda.getQuantidades());
		}
		
		Double percentualdescontoTabelapreco = null;
		try {
			percentualdescontoTabelapreco = Double.parseDouble(request.getParameter("percentualdescontoTabelapreco"));
		} catch (Exception e) {}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("identificadorinterno", System.currentTimeMillis() + material.getCdmaterial().toString());
		json.addObject("bean", material);
		json.addObject("materialproducaoMestre", materialproducaoMestre);
		json.addObject("percentualdescontoTabelapreco", percentualdescontoTabelapreco);
		
		return json;
	}
	
	public ModelAndView recalcularMaterialProducaoAJAX(WebRequestContext request, Venda venda){
		Material material = materialService.loadMaterialComMaterialproducao(new Material(Integer.parseInt(venda.getCodigo())));
		venda.setMaterial(material);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
		
		vendaService.trocarMaterialProducao(material, venda);
		
		if(material.getProducao()){
			Vendamaterialmestre vmm = new Vendamaterialmestre();
			vmm.setAltura(venda.getAlturas());
			vmm.setComprimento(venda.getComprimentos());
			vmm.setLargura(venda.getLarguras());
			vmm.setVenda(venda);
			vmm.setMaterial(material);
			venda.getListaVendamaterialmestre().add(vmm);
		}
		
		try{
			material.setProduto_altura(venda.getAlturas());
			material.setProduto_largura(venda.getLarguras());
			material.setQuantidade(venda.getQuantidades());
			pedidovendaService.preecheValorVendaComTabelaPrecoItensProducao(material, venda.getPedidovendatipo(), venda.getCliente(), venda.getEmpresa(), null);
			materialproducaoService.getValorvendaproducao(material);			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Materialproducao materialproducaoMestre = materialproducaoService.createItemProducaoByMaterialmestre(material.getListaProducao(), material);
		material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), true));
		
		for (Materialproducao materialproducao : material.getListaProducao()) {
			if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && venda.getQuantidades() != null){
				materialproducao.setConsumo(materialproducao.getConsumo() * venda.getQuantidades());
				
				materialformulavalorvendaService.setValorvendaByFormula(materialproducao.getMaterial(), 
						venda.getAlturas() != null ? venda.getAlturas() : null, 
						venda.getLarguras() != null ? venda.getLarguras() : null, 
						venda.getComprimentos() != null ? venda.getComprimentos() : null,
						venda.getValorfrete() != null ? venda.getValorfrete().getValue().doubleValue() : null);
			}
		}
		if(materialproducaoMestre != null && materialproducaoMestre.getConsumo() != null && materialproducaoMestre.getConsumo() > 0 && 
				venda.getQuantidades() != null){
			materialproducaoMestre.setConsumo(materialproducaoMestre.getConsumo() * venda.getQuantidades());
			if(venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
				for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
					if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().equals(materialproducaoMestre.getMaterial())){
						materialproducaoMestre.setPrazoentrega(vendamaterial.getPrazoentrega());
						materialproducaoMestre.setLoteestoque(vendamaterial.getLoteestoque());
					}
				}
			}
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", material);
		json.addObject("materialproducaoMestre", materialproducaoMestre);
		
		return json;
	}
	
	/**
	 * Gera a quantidade de linhas de pagamento de acordo a quantidade de parcelas 
	 * @see br.com.linkcom.sined.geral.service.prazopagamentoitemService#countParcelas
	 * @param request
	 * @param venda
	 * @author Ramon Brazil
	 */
	public ModelAndView pagamento(WebRequestContext request, Vendaorcamento vendaorcamento){	
		String dtPrimeiraEntrega = request.getParameter("dtprazoentrega");
		Date dtEntrega = null;
		if(dtPrimeiraEntrega != null && !dtPrimeiraEntrega.equals("")){
			try {
				dtEntrega = SinedDateUtils.stringToDate(dtPrimeiraEntrega);
			} catch (ParseException e) {}
		}
		
		AjaxRealizarVendaPagamentoBean bean = vendaorcamentoService.pagamento(vendaorcamento, dtEntrega);
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", bean);
		json.addObject("isValorminimo", bean.getIsValorminimo());
		
		return json;
	}
	
	/**
	 * Retorna a unidade de medida do material para ser setada como default ao selecionar
	 * determinado material em Realizar venda.
	 * @param request
	 * @param bean
	 * @return
	 * @author Taidson
	 * @since 29/12/2010
	 */
	public ModelAndView getUnidademedida(WebRequestContext request, Material bean){
		return vendaService.getUnidademedida(request, bean);
	}
	
	/**
	 * M�todo que busca os lotes com qtde dispon�vel para o material
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView getLoteestoque(WebRequestContext request, Material material){
		return loteestoqueService.getLoteestoque(request, material);
	}
	
	/**
	 * M�todo que busca a qtde do lote
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView getQtdeLoteestoque(WebRequestContext request, Material material){
		String cdloteestoquestr = request.getParameter("cdloteestoque");
		String cdempresa = request.getParameter("cdempresa");
		String cdlocalarmazenagem = request.getParameter("cdlocalarmazenagem");
		Empresa empresa = null;
		Localarmazenagem localarmazenagem = null;
		try {
			if(cdempresa != null && !"".equals(cdempresa)){
				empresa = new Empresa(Integer.parseInt(cdempresa));
			}
			if(cdlocalarmazenagem != null && !"".equals(cdlocalarmazenagem)){
				localarmazenagem = new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem));
			}
		} catch (NumberFormatException e) {}
		Double qtde = 0.0;
		if(cdloteestoquestr != null && !"".equals(cdloteestoquestr) && material.getCdmaterial() != null){
			qtde = loteestoqueService.getQtdeLoteestoque(material, localarmazenagem, empresa, new Loteestoque(Integer.parseInt(cdloteestoquestr)), false);
		}
		return new JsonModelAndView().addObject("qtdeloteestoque", qtde);
	}

	
	/**
	 * M�todo respons�vel por converter o valor do produto 
	 * de acordo com o fator de convers�o da nova unidade de medida selecionada.
	 * @param request
	 * @param bean
	 * @return
	 * @author Taidson
	 * @since 30/12/2010
	 */
	public ModelAndView converteUnidademedida(WebRequestContext request, Material bean){
		Double valorConvertido = 0.00;
		Double valorMin = 0.00;
		Double valorMax = 0.00;
		Double estoque = 0.00;
		Double fatorConversao = 0.0;
		Materialtabelapreco materialtabelapreco = bean.getMaterialtabelapreco();
		Cliente cliente = bean.getCliente();
		Empresa empresa = bean.getEmpresatrans();
		Pedidovendatipo pedidovendatipo = bean.getPedidovendatipo();
		Projeto projeto = bean.getProjeto();
		
		String origemvenda = request.getParameter("origemvenda");
		String cdlocalarmazenagem = request.getParameter("localarmazenagem");
		String alturastr = request.getParameter("altura");
		String largurastr = request.getParameter("largura");
		String comprimentostr = request.getParameter("comprimento");
		String fretestr = request.getParameter("valorfrete");
		Double altura = null;
		Double largura = null;
		Double comprimento = null;
		Double frete = null;
		Vendaorcamento vendaorcamento = new Vendaorcamento();
		String cdempresa = request.getParameter("empresa");
		try {
			if(cdempresa != null && !"".equals(cdempresa)){
				if(cdempresa.indexOf("=") != -1){
					empresa = new Empresa(Integer.parseInt(cdempresa.substring(cdempresa.lastIndexOf("=")+1, cdempresa.lastIndexOf("]"))));
				}else {
					empresa = new Empresa(Integer.parseInt(cdempresa));
				}
				vendaorcamento.setEmpresa(empresa);
			}
		} catch (Exception e) {}
		try {
			if(cdlocalarmazenagem != null && !"".equals(cdlocalarmazenagem)){
				if(cdlocalarmazenagem.indexOf("=") != -1){
					vendaorcamento.setLocalarmazenagem(new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem.substring(cdlocalarmazenagem.indexOf("=")+1, cdlocalarmazenagem.indexOf(",")))));
				}else {
					vendaorcamento.setLocalarmazenagem(new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem)));
				}
			}
		} catch (Exception e) {}
		
		if(StringUtils.isNotBlank(alturastr)){
			try { altura  = Double.parseDouble(alturastr); } catch (NumberFormatException e){}
		}
		if(StringUtils.isNotBlank(largurastr)){
			try { largura  = Double.parseDouble(largurastr); } catch (NumberFormatException e){}
		}
		if(StringUtils.isNotBlank(comprimentostr)){
			try { comprimento  = Double.parseDouble(comprimentostr); } catch (NumberFormatException e){}
		}
		if(StringUtils.isNotBlank(fretestr)){
			try { frete  = Double.parseDouble(fretestr.replaceAll("\\.", "").replace(",", ".")); } catch (NumberFormatException e){}
		}
		
		Unidademedida unidademedidaescolhida = new Unidademedida();
		unidademedidaescolhida = bean.getUnidademedida();
		
		bean = materialService.unidadeMedidaMaterial(bean);
		materialformulavalorvendaService.setValorvendaByFormula(bean, altura, largura, comprimento, frete);
		vendaService.preencheValorVendaComTabelaPreco(bean, materialtabelapreco, pedidovendatipo, cliente, empresa, unidademedidaescolhida, null);		
		
		vendaorcamento.setMaterial(bean);
				
		Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(vendaorcamento.getMaterial(), vendaorcamento.getLocalarmazenagem(), vendaorcamento.getEmpresa(), projeto);
		Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(vendaorcamento.getMaterial(), vendaorcamento.getLocalarmazenagem(), vendaorcamento.getEmpresa(), projeto);
		Double qtddisponivel = (entrada!=null?entrada:0d)-(saida!=null?saida:0d);
		Double qtdedisponivelprincipal = qtddisponivel;
		
		List<Unidademedidaconversao> lista = new ArrayList<Unidademedidaconversao>();
		Material material = materialService.findListaMaterialunidademedida(bean);
		lista = unidademedidaconversaoService.conversoesByUnidademedida(bean.getUnidademedida());
		
		Boolean achou = Boolean.FALSE;
		Boolean mostrarconversao = Boolean.FALSE;
		Double fracao = null;
		Double qtdeReferencia = 1.0;
		boolean pesoliquidocalculadopelaformula = false;
		
		if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
			for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
				if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().getCdunidademedida() != null &&
						materialunidademedida.getFracao() != null){
					if(materialunidademedida.getUnidademedida().equals(unidademedidaescolhida)){
						fatorConversao = materialunidademedida.getFracao();
						qtdeReferencia = materialunidademedida.getQtdereferencia();
						pesoliquidocalculadopelaformula = materialunidademedida.getPesoliquidocalculadopelaformula() != null && materialunidademedida.getPesoliquidocalculadopelaformula(); 
						
						fracao = materialunidademedida.getFracaoQtdereferencia();
						if(origemvenda != null && "true".equalsIgnoreCase(origemvenda)){
							if((largura != null && altura != null) || (largura != null && comprimento != null) || (altura != null && comprimento != null)){
								if(largura == null || largura == 0) largura  = 1.0;
								if(altura == null || altura == 0) altura = 1.0;
								if(comprimento == null || comprimento == 0) comprimento = 1.0;
								
								fracao = 1/(largura*altura*comprimento);
								
								String fracaostr = new DecimalFormat("#.#######").format(fracao);
								fracao = Double.parseDouble(fracaostr.replace(",", "."));
								
								if(bean.getValorvenda() != null && (bean.getUnidadesecundariaTabelapreco() == null || !bean.getUnidadesecundariaTabelapreco())){
									valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda() / fracao);
								}
								
								if(materialunidademedida.getConsiderardimensoesvendaconversao() != null && materialunidademedida.getConsiderardimensoesvendaconversao()){
									fatorConversao = fracao;
									qtdeReferencia = 1d;
								}
							}else if(materialunidademedida.getValorunitario() != null && materialunidademedida.getValorunitario() > 0){
								if(bean.getUnidadesecundariaTabelapreco() == null || !bean.getUnidadesecundariaTabelapreco()){
									valorConvertido = SinedUtil.roundByParametro(materialunidademedida.getValorunitario());
								}
							}else {
								if(bean.getValorvenda() != null && (bean.getUnidadesecundariaTabelapreco() == null || !bean.getUnidadesecundariaTabelapreco())){
									valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda() / fracao);
								}
							}
						}else {
							if(bean.getUnidadesecundariaTabelapreco() == null || !bean.getUnidadesecundariaTabelapreco()){
								if(materialunidademedida.getValorunitario() != null && materialunidademedida.getValorunitario() > 0){
									valorConvertido = SinedUtil.roundByParametro(materialunidademedida.getValorunitario());
								}else if(bean.getValorvenda() != null){
									valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda() / fracao);
								}
							}
						}
						if(bean.getUnidadesecundariaTabelapreco() == null || !bean.getUnidadesecundariaTabelapreco()){
							if(bean.getValorvendaminimo() != null)
								valorMin = bean.getValorvendaminimo() / fracao;
							if(bean.getValorvendamaximo() != null)
								valorMax = bean.getValorvendamaximo() / fracao;
						}else {
							if(bean.getValorvenda() != null)
								valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda());
							if(bean.getValorvendaminimo() != null)
								valorMin = bean.getValorvendaminimo() ;
							if(bean.getValorvendamaximo() != null)
								valorMax = bean.getValorvendamaximo();
						}

						if(materialunidademedida.getValorminimo() != null){
							valorMin = materialunidademedida.getValorminimo();
						}
						if(materialunidademedida.getValormaximo() != null){
							valorMax = materialunidademedida.getValormaximo();
						}
						
						if(materialunidademedida.getUnidademedida()!=null && materialunidademedida.getUnidademedida().getCasasdecimaisestoque()!=null)
							estoque = SinedUtil.roundByUnidademedida(qtddisponivel * materialunidademedida.getFracaoQtdereferencia(), materialunidademedida.getUnidademedida());
						else
							estoque = qtddisponivel * materialunidademedida.getFracaoQtdereferencia();
						achou = Boolean.TRUE;
						if(materialunidademedida.getMostrarconversao() != null && materialunidademedida.getMostrarconversao()){
							mostrarconversao = materialunidademedida.getMostrarconversao();
						}
						break;
					}
				}
			}
		}
		
		if(!achou) {
			if(lista != null && lista.size() > 0){
				for (Unidademedidaconversao item : lista) {
					if(item.getUnidademedida() != null && item.getUnidademedida().getCdunidademedida() != null && item.getFracao() != null){
						if(item.getUnidademedidarelacionada().equals(unidademedidaescolhida)){
							fracao = item.getFracaoQtdereferencia();
							if(bean.getUnidadesecundariaTabelapreco() == null || !bean.getUnidadesecundariaTabelapreco()){
								if(bean.getValorvenda() != null)
									valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda() / item.getFracaoQtdereferencia());
								if(bean.getValorvendaminimo() != null)
									valorMin = bean.getValorvendaminimo() / item.getFracaoQtdereferencia();
								if(bean.getValorvendamaximo() != null)
									valorMax = bean.getValorvendamaximo() / item.getFracaoQtdereferencia();
							}else {
								if(bean.getValorvenda() != null)
									valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda());
								if(bean.getValorvendaminimo() != null)
									valorMin = bean.getValorvendaminimo();
								if(bean.getValorvendamaximo() != null)
									valorMax = bean.getValorvendamaximo();
							}
							
							fatorConversao = item.getFracao();
							qtdeReferencia = item.getQtdereferencia();
							if(item.getUnidademedidarelacionada()!=null && item.getUnidademedidarelacionada().getCasasdecimaisestoque()!=null)
								estoque = SinedUtil.roundByUnidademedida(qtddisponivel * item.getFracaoQtdereferencia(), item.getUnidademedidarelacionada());
							else
								estoque = qtddisponivel * item.getFracaoQtdereferencia();
							break;
						}else{
							valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda());
							valorMin = bean.getValorvendaminimo();
							valorMax = bean.getValorvendamaximo();
							fatorConversao = 0.00;
							qtdeReferencia = 1.0;
							estoque = qtddisponivel;
						}
					}else{
						valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda());
						valorMin = bean.getValorvendaminimo();
						valorMax = bean.getValorvendamaximo();
						fatorConversao = 0.00;
						qtdeReferencia = 1.0;
						estoque = qtddisponivel;
					}
				}
			}else{
				valorConvertido = SinedUtil.roundByParametro(bean.getValorvenda());
				valorMin = bean.getValorvendaminimo();
				valorMax = bean.getValorvendamaximo();
				fatorConversao = 0.00;
				qtdeReferencia = 1.0;
				estoque = 0.00;
			}
		}
		
		if(material != null && material.getUnidademedida() != null && unidademedidaescolhida != null && 
				unidademedidaescolhida.equals(material.getUnidademedida()) && (estoque == null || estoque == 0)){
			estoque = qtddisponivel;
		}
		
		return new JsonModelAndView().addObject("valorConvertido", SinedUtil.descriptionDecimal(valorConvertido))
									 .addObject("valorMin", SinedUtil.descriptionDecimal(valorMin))
									 .addObject("fatorConversao", fatorConversao)
									 .addObject("qtdeReferencia", qtdeReferencia)
									 .addObject("estoque", estoque)
									 .addObject("valorMax", SinedUtil.descriptionDecimal(valorMax))
									 .addObject("mostrarconversao", mostrarconversao)
									 .addObject("qtdeprincipal", qtdedisponivelprincipal)
									 .addObject("percentualdescontoTabelapreco", SinedUtil.descriptionDecimal(bean.getPercentualdescontoTabela()) + "';")
									 .addObject("pesoliquidocalculadopelaformula", pesoliquidocalculadopelaformula);
	}
	
	/**
	 * M�todo chamado via AJAX, que carrega os endere�os do cliente
	 * 
	 * @param request
	 * @param cliente
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView comboBoxEndereco(WebRequestContext request, Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
//		List<Endereco> lista = enderecoService.findByCliente(cliente);
		List<Endereco> lista = enderecoService.findByPessoaWithoutInativo(cliente);
		if("true".equals(request.getParameter("allTipos"))){
			return new JsonModelAndView().addObject("lista", lista);
		}
		List<Endereco> listaRetorno = new ArrayList<Endereco>();
		for (Endereco endereco : lista) {
			if (endereco.getEnderecotipo().equals(Enderecotipo.ENTREGA) 
					|| endereco.getEnderecotipo().equals(Enderecotipo.UNICO) 
							|| endereco.getEnderecotipo().equals(Enderecotipo.OUTRO) 
								|| endereco.getEnderecotipo().equals(Enderecotipo.INSTALACAO)){
						listaRetorno.add(endereco);
					}
		}
		return new JsonModelAndView().addObject("lista", listaRetorno);
	}
	
	public void buscarMaterialCodigoAJAX(WebRequestContext request, Vendaorcamento vendaorcamento){	
		if(vendaorcamento != null && vendaorcamento.getCodigo() != null){
			List<Material> lista = materialService.findWithCod(vendaorcamento.getCodigo().toString());
			String bean = "";
			String beannome = "";
			if(lista != null && !lista.isEmpty()){
				if(lista.size() == 1){
					Material material = lista.get(0);
					bean = "br.com.linkcom.sined.geral.bean.Material[cdmaterial=" + material.getCdmaterial() + ",nome=" + material.getNome() + "]";
					beannome = material.getNome();
				}
			}
			
			if("".equals(bean)){
				View.getCurrent().println("var unico = false;");
			}else {
				View.getCurrent().println("var unico = true;");
				View.getCurrent().println("var bean = \"" + bean + "\";");
				View.getCurrent().println("var beannome = \"" + beannome + "\";");
			}	
		}
	}
	
	public void buscarMaterialCodigoByBalancaAJAX(WebRequestContext request, Vendaorcamento vendaorcamento){	
		if(vendaorcamento != null && vendaorcamento.getCodigo() != null){
			List<Material> lista = materialService.findCdMaterialAndIdentificacao(vendaorcamento.getCodigo().toString());
			String bean = "";
			String beannome = "";
			if(lista != null && !lista.isEmpty()){
				if(lista.size() == 1){
					Material material = lista.get(0);
					bean = "br.com.linkcom.sined.geral.bean.Material[cdmaterial=" + material.getCdmaterial() + ",nome=" + material.getNome() + "]";
					beannome = material.getNome();
				}
			}
			
			if("".equals(bean)){
				View.getCurrent().println("var unico = false;");
			}else {
				View.getCurrent().println("var unico = true;");
				View.getCurrent().println("var bean = \"" + bean + "\";");
				View.getCurrent().println("var beannome = \"" + beannome + "\";");
			}		
			
		}
	}
	
	
	
	/**
	 * M�todo ajax que verifica se o cliente possui restri��es sem data de libera��o
	 *
	 * @param request
	 * @author Rafael Salvio
	 */
	public void ajaxVerificaRestricao(WebRequestContext request){
		restricaoService.verificaRestricaoCliente(request);
	}
	
	/**
	 * M�todo ajax que busca e retorna o ultimo valor de venda de determinado material para o cliente selecionado.
	 * 
	 * @param request
	 * @author Rafael Salvio
	 */
	public void buscarSugestaoUltimoValorAJAX(WebRequestContext request){
		if(request.getParameter("cdmaterial")!= null && request.getParameter("cdpessoa")!=null){
			View view = View.getCurrent();
			request.getServletResponse().setContentType("text/html");
			Material material = materialService.loadForUltimoValorVenda(new Material(Integer.parseInt(request.getParameter("cdmaterial"))));
			Vendamaterial vm = null;
			Vendaorcamentomaterial vom = null;
			Unidademedida unidademedida = null;
			
			try {
				unidademedida = new Unidademedida(Integer.parseInt(request.getParameter("cdunidademedida")));
			} catch (Exception e) {}
			
			Double ultimovalor = null;
			java.util.Date dtvenda = null;
			boolean sugestaoValorVenda = false;
			if(!Boolean.TRUE.equals(material.getKitflexivel()) && !Boolean.TRUE.equals(material.getExibiritenskitvenda())){
				Cliente cliente = new Cliente(Integer.parseInt(request.getParameter("cdpessoa")));
				Empresa empresa = new Empresa(Integer.parseInt(request.getParameter("cdempresa")));
				if(parametrogeralService.getBoolean(Parametrogeral.SUGERIR_VALOR_VENDA_NO_ORCAMENTO)){
					sugestaoValorVenda = true;
					vm = vendamaterialService.getUltimoValorDataMaterialByCliente(material, cliente, empresa, null);
					if(vm != null && vm.getPreco() != null){
						ultimovalor = vm.getPreco();
						if(vm.getMaterial() != null && vm.getMaterial().getUnidademedida() != null && vm.getUnidademedida() != null && 
								!vm.getMaterial().getUnidademedida().equals(vm.getUnidademedida()) && vm.getQtdereferencia() != null && vm.getFatorconversao() != null){
							ultimovalor = ultimovalor * vm.getFatorconversaoQtdereferencia();
							
						}
						
						if(unidademedida == null || vm.getUnidademedida() == null || !vm.getUnidademedida().equals(unidademedida)){
							if(vm.getMaterial() != null && vm.getMaterial().getUnidademedida() != null && unidademedida != null && !vm.getMaterial().getUnidademedida().equals(unidademedida)){
								Double fatorconversao = unidademedidaService.getFatorconversao(material.getUnidademedida(), 1d, unidademedida, material, null, null);
								ultimovalor = fatorconversao != null ? ultimovalor / fatorconversao : null;
							}else if(material.getUnidademedida() != null && vm.getUnidademedida() != null && !vm.getUnidademedida().equals(material.getUnidademedida())){
								Double fatorconversao = unidademedidaService.getFatorconversao(material.getUnidademedida(), 1d, vm.getUnidademedida(), material, null, null);
								ultimovalor = fatorconversao != null ? ultimovalor / fatorconversao : null;
							}
						}
						if(ultimovalor != null){
							dtvenda = vm.getVenda() != null ? vm.getVenda().getDtvenda() : null;
						}
					}
				}else {
					vom = vendaorcamentomaterialService.getUltimoValorDataMaterialByCliente(material, cliente, empresa);
					if(vom != null && vom.getPreco() != null){
						ultimovalor = vom.getPreco();
						if(vom.getMaterial() != null && vom.getMaterial().getUnidademedida() != null && vom.getUnidademedida() != null && 
								!vom.getMaterial().getUnidademedida().equals(vom.getUnidademedida()) && vom.getQtdereferencia() != null && vom.getFatorconversao() != null){
							ultimovalor = ultimovalor * vom.getFatorconversaoQtdereferencia();
							
						}
						if(unidademedida == null || vom.getUnidademedida() == null || !vom.getUnidademedida().equals(unidademedida)){
							if(vom.getMaterial() != null && vom.getMaterial().getUnidademedida() != null && unidademedida != null && !vom.getMaterial().getUnidademedida().equals(unidademedida)){
								Double fatorconversao = unidademedidaService.getFatorconversao(material.getUnidademedida(), 1d, unidademedida, material, null, null);
								ultimovalor = fatorconversao != null ? ultimovalor / fatorconversao : null;
							}else if(material.getUnidademedida() != null && vom.getUnidademedida() != null && !vom.getUnidademedida().equals(material.getUnidademedida())){
								Double fatorconversao = unidademedidaService.getFatorconversao(material.getUnidademedida(), 1d, vom.getUnidademedida(), material, null, null);
								ultimovalor = fatorconversao != null ? ultimovalor / fatorconversao : null;
							}
						}
						if(ultimovalor != null){
							dtvenda = vom.getVendaorcamento() != null ? vom.getVendaorcamento().getDtorcamento() : null;
						}
					}
				}
			}
			if(ultimovalor != null){
				view.println("var ultimovalor = '"+ SinedUtil.descriptionDecimal(ultimovalor) +"';");
				view.println("var dtvenda = '"+ (dtvenda != null ? SinedDateUtils.toString(dtvenda) : "") +"';");
				view.println("var sugestaoValorVenda = '"+ sugestaoValorVenda +"';");
			}
		}
	}
	
	public ModelAndView abrirPopUpCalcularLargCompAlt(WebRequestContext request){
		Produto produto = new Produto();
		
		String cdmaterial = request.getParameter("cdmaterial");
		
		if(cdmaterial != null && !"".equals(cdmaterial)){
			produto.setCdmaterial(Integer.parseInt(cdmaterial));
			produto = produtoService.carregaAlturaComprimentoLargura(produto);
		}		
		
		return new ModelAndView("direct:/crud/popup/popUpCalcularLargCompAlt", "bean", produto);
	}
	
	public ModelAndView abrirPopupUltimoValorCompravenda(WebRequestContext request){
		Material material = new Material();
		
		String cdmaterial = request.getParameter("cdmaterial");
		String cdempresastr = request.getParameter("cdempresa");
		Integer cdempresa = null;
		
		if(cdmaterial != null && !"".equals(cdmaterial)){
			if(cdempresastr != null && !"".equals(cdempresastr)){
				cdempresa = Integer.parseInt(cdempresastr);
			}
			material.setCdmaterial(Integer.parseInt(cdmaterial));
			material.setUltimovalorcompra(ordemcompramaterialService.getUltimoValorForVenda(material.getCdmaterial(), cdempresa));
			material.setUltimovalorvenda(vendamaterialService.getUltimoValor(material.getCdmaterial(),cdempresa));
		}		
		
		return new ModelAndView("direct:/crud/popup/popUpUltimovalorcompravenda", "bean", material);
	}
	
	/**
	 * M�todo que ajax que busca as contas relacionados a empresa
	 *
	 * @param request
	 * @param empresa
	 * @author Luiz Fernando
	 */
	public void ajaxCarregarContaboleto(WebRequestContext request, Empresa empresa){
		List<Conta> listaConta = contaService.findContasAtivasComPermissao(empresa, true);	
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaConta, "listaConta", ""));
	}
	
	/**
	 * Verifica se a empresa da venda possui um comprovante configur�vel definido 
	 * @param request
	 * @param venda
	 * @return
	 */
	public ModelAndView possuiComprovanteConfiguravel(WebRequestContext request, Vendaorcamento vendaorcamento){
		Empresa empresa = vendaorcamento.getEmpresa();
		
		if(empresa == null){
			vendaorcamento = vendaorcamentoService.load(vendaorcamento);
			empresa = vendaorcamento.getEmpresa();
		}
		TipoComprovante tipoComprovante = ComprovanteConfiguravel.TipoComprovante.ORCAMENTO;
		boolean possui = empresaService.possuiComprovanteConfiguravel(tipoComprovante, empresa);
		return new JsonModelAndView().addObject("possui", possui);
	}
	
	/**
	 * M�todo que abre a popup de justificativa de cancelamento do or�amento
	 * 
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirJustificativaCancelamentoVenda(WebRequestContext request){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(vendaorcamentoService.existOrcamentoNotcancelar(whereIn)){
			request.addError("S� � permitido cancelar um or�amento na situa��o 'EM ABERTO' ou que esteja 'AUTORIZADA' com a venda 'CANCELADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Venda venda = new Venda();
		venda.setIds(whereIn);		
		request.setAttribute("descricao", "CANCELAR");
		request.setAttribute("msg", "Justificativa para o cancelamento");
		
		return new ModelAndView("direct:/crud/popup/vendaJustificativacancelar")
								.addObject("venda", venda);
	}
	
	public ModelAndView abrirPopUpMotivoReprovar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		List<MotivoReprovacaoFaturamento> reprovacao = vendaorcamentohistoricoService.findMotivosReprovacao();
		if(reprovacao == null || reprovacao.isEmpty()){
			request.addError("Para reprovar um or�amento deve ter pelo menos um motivo de reprova��o cadastrado e ativo.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if (vendaorcamentoService.existOrcamentoSomenteEmAberto(whereIn)) {
			request.addError("Somente � poss�vel reprovar or�amento com situa��o Em Aberto.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Vendaorcamento vendaOrcamento = new Vendaorcamento();
		vendaOrcamento.setIds(whereIn);
		
		
		request.setAttribute("reprovacao", reprovacao);
		request.setAttribute("descricao", "REPROVAR");
		request.setAttribute("msg", "Motivo da reprova��o");
		
		return new ModelAndView("direct:/crud/popup/vendaOrcamentoJustificativaReprovar")
								.addObject("vendaorcamento", vendaOrcamento);
	}
	
	/**
	 * Action em ajax para buscar as informa��es das condi��es de pagamento escolhidas anteriormente no or�amento.
	 *
	 * @param request
	 * @param venda
	 * @since 02/07/2012
	 * @author Rodrigo Freitas
	 */
	public void buscarFormapagamentoOrcamento(WebRequestContext request, Vendaorcamento vendaorcamento){
		if(vendaorcamento != null && vendaorcamento.getVendaorcamentoformapagamento() != null && 
				vendaorcamento.getVendaorcamentoformapagamento().getCdvendaorcamentoformapagamento() != null){
			Vendaorcamentoformapagamento vendaorcamentoformapagamento = vendaorcamentoformapagamentoService.load(vendaorcamento.getVendaorcamentoformapagamento());
			if(vendaorcamentoformapagamento != null){
				View.getCurrent().println("var prazopagamentoOrcamento = 'br.com.linkcom.sined.geral.bean.Prazopagamento[cdprazopagamento=" + vendaorcamentoformapagamento.getPrazopagamento().getCdprazopagamento() + "]';");
				View.getCurrent().println("var qtdeParcelasOrcamento = '" + (vendaorcamentoformapagamento.getQtdeparcelas() != null ? vendaorcamentoformapagamento.getQtdeparcelas() : "") + "';");
			} else {
				View.getCurrent().println("var prazopagamentoOrcamento = '<null>';");
				View.getCurrent().println("var qtdeParcelasOrcamento = '';");
			}
			
		} else {
			View.getCurrent().println("var prazopagamentoOrcamento = '<null>';");
			View.getCurrent().println("var qtdeParcelasOrcamento = '';");
		}
	}
	
	/**
	 * M�todo ajax que verifica se o cliente tem d�bito(s) acima do permitido
	 *
	 * @see br.com.linkcom.sined.geral.service.ClienteService#carregarTaxapedidovendaCreditolimitecompraByCliente(Cliente cliente)
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxValordebitoContasEmAbertoByCliente(WebRequestContext request, Cliente cliente){
		String whereInDocumentotipo = request.getParameter("whereInDocumentotipo");
		
		String msgdebitoacimalimitecliente = "";
		if(cliente != null && cliente.getCdpessoa() != null){
			msgdebitoacimalimitecliente = vendaorcamentoService.verificaDebitoAcimaLimiteByCliente(whereInDocumentotipo, cliente);
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("msgdebitoacimalimitecliente", msgdebitoacimalimitecliente);
		
		return json;
	}
	
	/**
	 * Action em ajax para buscar as sugest�es dos materiais inclu�dos no pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView buscarSugestoesVendaAJAX(WebRequestContext request, Venda venda){
		String codigos = request.getParameter("codigos");
		List<Sugestaovenda> listaSugestoes = materialvendaService.findForSugestao(codigos);

		Material material;
		if(listaSugestoes != null && !listaSugestoes.isEmpty()){
			for(Sugestaovenda sugestaovenda : listaSugestoes){
				if(sugestaovenda.getCdmaterial() != null){
					material = new Material(sugestaovenda.getCdmaterial());
					vendaService.preencheValorVendaComTabelaPreco(material, venda.getMaterialtabelapreco(), venda.getPedidovendatipo(), venda.getCliente(), venda.getEmpresa(), new Unidademedida(sugestaovenda.getCdunidademedida()), null);
					if(material != null){
						if(material.getValorvenda() != null){
							sugestaovenda.setValor(material.getValorvenda());
						}
						if(material.getValorcusto() != null){
							sugestaovenda.setValorcustomaterial(material.getValorcusto());
						}
						if(material.getValorvendamaximo() != null){
							sugestaovenda.setValorvendamaximo(material.getValorvendamaximo());
						}
						if(material.getValorvendaminimo() != null){
							sugestaovenda.setValorvendaminimo(material.getValorvendaminimo());
						}
					}
					boolean considerarReserva = false;
					if(Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO))){
						considerarReserva = true;
					}
					
					Boolean buscarDtVencimentoLote = Boolean.parseBoolean(request.getParameter("SELECAO_AUTOMATICA_LOTE_FATURAMENTO"));
					String dtMenorVencimentoLote = "";
					if(buscarDtVencimentoLote){
						java.sql.Date dtMenorVencimento = loteestoqueService.buscarDtVencimentoLote(material.getCdmaterial(), venda.getEmpresa().getCdpessoa(), venda.getLocalarmazenagem() != null ? venda.getLocalarmazenagem().getCdlocalarmazenagem() : null);
						
						if(dtMenorVencimento != null){
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
							dtMenorVencimentoLote = sdf.format(dtMenorVencimento);				
						}
					}
					sugestaovenda.setMenorDataValidade(dtMenorVencimentoLote);
				}
			}
		}
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("listaSugestoes", listaSugestoes);
		return jsonModelAndView;
	}
	
	/**
	 * M�todo que abre uma popup para enviar emails
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopUpEnviarComprovante(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if (whereIn == null || "".equals(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Vendaorcamento> listaVendaorcamento = vendaorcamentoService.findForEnviocomprovanteemail(whereIn);
		
		List<EnvioComprovanteDestinatarioBean> listaDestinatario = new ArrayList<EnvioComprovanteDestinatarioBean>();
		String remetente = null;
		Boolean empresasDiferentes = false;
		Integer cdcliente_aux = null;
		Integer cdempresa_aux = null;
		
		for(Vendaorcamento vendaorcamento : listaVendaorcamento){
			// VERIFICA O REMETENTE DO E-MAIL
			if(remetente == null){
				remetente = vendaorcamento.getColaborador().getEmail();
			} else if(!remetente.equals(vendaorcamento.getColaborador().getEmail())){
				remetente = "";
			}
			
			if(vendaorcamento.getCliente() != null){
				if(cdcliente_aux == null){
					cdcliente_aux = vendaorcamento.getCliente().getCdpessoa();
				}
			}
			
			// VERIFICA SE � EMPRESA DIFERENTE
			if(cdempresa_aux == null){
				cdempresa_aux = vendaorcamento.getEmpresa().getCdpessoa();
			} else if(!cdempresa_aux.equals(vendaorcamento.getEmpresa().getCdpessoa())){
				empresasDiferentes = true;
			}
			
			if(vendaorcamento.getCliente() != null){
				// INCLUI O E-MAIL DO CLIENTE
				EnvioComprovanteDestinatarioBean envioComprovanteDestinatario = new EnvioComprovanteDestinatarioBean();
				envioComprovanteDestinatario.setId(vendaorcamento.getCdvendaorcamento());
				envioComprovanteDestinatario.setCdpessoa(vendaorcamento.getCliente().getCdpessoa());
				envioComprovanteDestinatario.setNome(vendaorcamento.getCliente().getNome());
				envioComprovanteDestinatario.setEmail(vendaorcamento.getCliente().getEmail());
				listaDestinatario.add(envioComprovanteDestinatario);
			
				// INCLUI OS CONTATOS DO CLIENTE
				if(vendaorcamento.getCliente().getListaContato() != null && !vendaorcamento.getCliente().getListaContato().isEmpty()){
					for (PessoaContato pessoaContato : vendaorcamento.getCliente().getListaContato()) {
						Contato c = pessoaContato.getContato();
						
						if (c.getEmailcontato() != null && !"".equals(c.getEmailcontato())) {
							EnvioComprovanteDestinatarioBean envioComprovanteDestinatarioContato = new EnvioComprovanteDestinatarioBean();
							envioComprovanteDestinatarioContato.setId(vendaorcamento.getCdvendaorcamento());
							envioComprovanteDestinatarioContato.setCdpessoa(c.getCdpessoa());
							envioComprovanteDestinatarioContato.setNome((StringUtils.isNotBlank(c.getNome()) ? c.getNome() : "") + " " + (StringUtils.isNotBlank(vendaorcamento.getCliente().getNome()) ? "(" + vendaorcamento.getCliente().getNome() + ")" : ""));
							envioComprovanteDestinatarioContato.setEmail(c.getEmailcontato());
							listaDestinatario.add(envioComprovanteDestinatarioContato);
						}
					}
				}
			}
		}
		
		String mensagem = "";
		mensagem += "Prezado(a),\n\nSegue or�amento, conforme solicitado.\n\nQuaisquer d�vidas, estamos � disposi��o.\n\n";
		if (!empresasDiferentes) {
			mensagem += listaVendaorcamento.get(0).getEmpresa().getNomefantasia();
		}
		
		Enviocomprovanteemail enviocomprovanteemail = new Enviocomprovanteemail();
		enviocomprovanteemail.setRemetente(remetente);
		enviocomprovanteemail.setAssunto("Or�amento");
		enviocomprovanteemail.setEmail(mensagem);
		enviocomprovanteemail.setListaDestinatarios(listaDestinatario);
		enviocomprovanteemail.setWhereIn(whereIn);
		enviocomprovanteemail.setEntrada(request.getParameter("fromentrada"));
		enviocomprovanteemail.setPossibilidadeAvulso(listaVendaorcamento.size() == 1);
		request.setAttribute("labelId", "Or�amento");
		
		return new ModelAndView("direct:/crud/popup/popUpEnviocomprovanteemail", "bean", enviocomprovanteemail);
	}
	
	/**
	 * M�todo para enviar comprovante de venda para o cliente e, caso o material
	 * possua fornecedor exclusivo, envia para o fornecedor tamb�m
	 * 
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public void enviarComprovante(WebRequestContext request, Enviocomprovanteemail enviocomprovanteemail) {
		String fromentrada = enviocomprovanteemail.getEntrada();
		
		if(enviocomprovanteemail != null){
			Map<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>> map = vendaService.getMapEnvioComprovante(enviocomprovanteemail);
			
			int sucesso = 0;
			int erro = 0;
			int vendaorcamentocancelada = 0; 
			List<EnvioComprovanteAnexoBean> listaAnexo = enviocomprovanteemail.getListaArquivos();
			Set<Entry<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>>> entrySet = map.entrySet();
			
			List<Vendaorcamento> comprovantesEnviadosComSucesso = new ArrayList<Vendaorcamento>();
			for (Entry<EnvioComprovanteBean, List<EnvioComprovanteDestinatarioBean>> entry : entrySet) {
				EnvioComprovanteBean comproanteBean = entry.getKey();
				List<EnvioComprovanteDestinatarioBean> listaDestinatario = entry.getValue();
				
				Vendaorcamento vendaorcamento = vendaorcamentoService.loadVendaorcamento(comproanteBean.getId());
				
				try {
					if(vendaorcamento.getVendaorcamentosituacao() != null && Vendaorcamentosituacao.CANCELADA.equals(vendaorcamento.getVendaorcamentosituacao())){
						vendaorcamentocancelada++;
					} else {
						Usuario usuario = SinedUtil.getUsuarioLogado();
						if(vendaorcamento.getColaborador() != null && vendaorcamento.getColaborador().getCdpessoa() != null){
							usuario = new Usuario(vendaorcamento.getColaborador().getCdpessoa());
						}
						usuario = usuarioService.load(usuario, "usuario.cdpessoa, usuario.email");
						
						ComprovanteConfiguravel comprovante = empresaService.findComprovaConfiguravelByTipoAndEmpresa(ComprovanteConfiguravel.TipoComprovante.ORCAMENTO, vendaorcamento.getEmpresa());
						
						Resource resource = vendaorcamentoService.criaComprovantevendaorcamentoForEnviaremail(vendaorcamento, comprovante);
						
						Empresa empresa = empresaService.loadComRepresentantes(vendaorcamento.getEmpresa());
						List<Vendaorcamentomaterial> listaVendaorcamentomaterial = vendaorcamentomaterialService.findForComissaoRepresentante(vendaorcamento);
						
						if(listaDestinatario != null && listaDestinatario.size() > 0){
							for(EnvioComprovanteDestinatarioBean comprovanteDestinatarioBean : listaDestinatario){
								try {
									EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
									email
										.setFrom(enviocomprovanteemail.getRemetente())
										.setTo(comprovanteDestinatarioBean.getEmail())
										.setSubject(enviocomprovanteemail.getAssunto())
										.attachFileUsingByteArray(resource.getContents(), resource.getFileName(), resource.getContentType(), resource.getFileName())
										.addHtmlText(enviocomprovanteemail.getEmail().replace("\n", "<br>") + usuarioService.addImagemassinaturaemailUsuario(usuario, email));
									
									if(listaAnexo != null && listaAnexo.size() > 0){
										Integer id = 1;
										for(EnvioComprovanteAnexoBean anexoBean : listaAnexo){
											if(anexoBean.getArquivo() != null && anexoBean.getArquivo().getContent() != null && anexoBean.getArquivo().getContent().length > 0){
												email.attachFileUsingByteArray(anexoBean.getArquivo().getContent(), anexoBean.getArquivo().getNome(), anexoBean.getArquivo().getTipoconteudo(), id.toString());
												id++;							
											}
										}
									}
									
									envioemailService.registrarEnvio(
											enviocomprovanteemail.getRemetente(), 
											enviocomprovanteemail.getAssunto(), 
											enviocomprovanteemail.getEmail().replace("\n", "<br>"), 
											Envioemailtipo.COMPROVANTE_ORCAMENTO,
											new Pessoa(comprovanteDestinatarioBean.getCdpessoa()), 
											comprovanteDestinatarioBean.getEmail(), 
											comprovanteDestinatarioBean.getNome(),
											email);
										
									email.sendMessage();

									sucesso++;
									if(!comprovantesEnviadosComSucesso.contains(vendaorcamento)){
										comprovantesEnviadosComSucesso.add(vendaorcamento);
									}
									
									// FEITO ISSO PARA N�O AGARRAR O MAILTRAP
									if(SinedUtil.isAmbienteDesenvolvimento()){
										Thread.sleep(5000);
									}
								} catch (Exception e) {
									e.printStackTrace();
									request.addError("Falha no envio de e-mail do or�amento " + vendaorcamento.getCdvendaorcamento() + ": " + e.getMessage());
									erro++;
								}
							}
						}
						
						if (empresa != null && empresa.getListaEmpresarepresentacao() != null && !empresa.getListaEmpresarepresentacao().isEmpty()) {
							for (Empresarepresentacao empresarepresentacao : empresa.getListaEmpresarepresentacao()) {
								if (empresarepresentacao.getFornecedor() != null) {
									if (vendaorcamentomaterialService.isFornecedorVendaorcamentomaterial(listaVendaorcamentomaterial, empresarepresentacao.getFornecedor())) {
										boolean sucessofornecedor = vendaorcamentoService.enviaComprovanteEmailRepresentante(request, vendaorcamento, empresa, empresarepresentacao.getFornecedor(), resource);
										if (sucessofornecedor) {
											sucesso++;
										} else {
											erro++;
										}
									}
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					erro++;
				}
			}
			
			if (sucesso > 0) {
				vendaorcamentohistoricoService.makeHistoricoComprovanteEnviado(comprovantesEnviadosComSucesso);					
				request.addMessage(sucesso + " or�amento(s) enviado(s) com sucesso.");
			}
			if (erro > 0) {
				request.addError(erro + " or�amento(s) n�o foi(ram) enviado(s).");
			}
			if (vendaorcamentocancelada > 0) {
				request.addError("N�o foi poss�vel enviar comprovante(s) de " + vendaorcamentocancelada + " or�amento(s) por estar(em) cancelado(s).");
			}
		}
		SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOrcamento" + (fromentrada != null && fromentrada.equals("true") ? "?ACAO=consultar&cdvendaorcamento=" + enviocomprovanteemail.getWhereIn() : ""));
	}
	
	/**
	 * M�todo ajax que carrega o combo de materialtabelapreco
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView comboBoxMaterialtabelapreco(WebRequestContext request) {
		Vendaorcamento vendaorcamento = new Vendaorcamento();
		String cdpessoastr = request.getParameter("clientecdpessoa");
		String cdempresastr = request.getParameter("empresacdpessoa");
		String periododtorcamento = request.getParameter("periododtorcamento");
		if(cdpessoastr != null && !"".equals(cdpessoastr)){
			vendaorcamento.setCliente(new Cliente(Integer.parseInt(cdpessoastr)));
		}
		if(cdempresastr != null && !"".equals(cdempresastr)){
			vendaorcamento.setEmpresa(new Empresa(Integer.parseInt(cdempresastr)));
		}
		if(periododtorcamento != null && !"".equals(periododtorcamento)){
			try {
				vendaorcamento.setDtorcamento(SinedDateUtils.stringToDate(periododtorcamento));
			} catch (ParseException e) {}
		}
		List<Categoria> listaCategoria = null;
		if(vendaorcamento.getCliente() != null){
			listaCategoria = categoriaService.findByPessoa(vendaorcamento.getCliente());
		}
		List<Materialtabelapreco> lista = materialtabelaprecoService.findForComboVenda(vendaorcamento.getCliente(), vendaorcamento.getDtorcamento(), vendaorcamento.getEmpresa(), listaCategoria);
		return new JsonModelAndView().addObject("lista", lista);
	}
	
	/**
	 * M�todo que abre a popup para troca de material por uma material similar
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirTrocaMaterial(WebRequestContext request, Vendaorcamento vendaOrcamento){
		Venda venda = new Venda();
		venda.setEmpresa(vendaOrcamento.getEmpresa());
		venda.setCliente(vendaOrcamento.getCliente());
		venda.setPedidovendatipo(vendaOrcamento.getPedidovendatipo());
		venda.setMaterialtabelapreco(vendaOrcamento.getMaterialtabelapreco());
		venda.setProjeto(vendaOrcamento.getProjeto());
		venda.setPrazopagamento(vendaOrcamento.getPrazopagamento());
		venda.setCodigo(vendaOrcamento.getCodigo().toString());
		return vendaService.abrirTrocaMaterial(request, venda);
	}
	
	public ModelAndView atualizaValoresTabelaprecoMateriaisAjax(WebRequestContext request, Venda venda){
		return vendaService.atualizaValoresTabelaprecoMateriaisAjax(request, venda);
	}
	
	/**
	 * Action que redireciona para a cria��o de venda ou pedido de venda.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 08/11/2012
	 */
	public ModelAndView gerar(WebRequestContext request){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Vendaorcamento bean = vendaorcamentoService.load(new Vendaorcamento(Integer.parseInt(itens)));
		if(vendaorcamentoService.haveVendaorcamentoSituacao(itens, false, Vendaorcamentosituacao.AUTORIZADO, Vendaorcamentosituacao.CANCELADA, Vendaorcamentosituacao.REPROVADO)){
			if(vendaorcamentoService.isAutorizaAprovado(bean) && 
					vendaorcamentoService.haveVendaorcamentoSituacao(itens, false,Vendaorcamentosituacao.AUTORIZADO)){
				request.addError("Or�amento(s) j� aprovado como venda/pedido venda.");
				return sendRedirectToAction("listagem");
			}else if(vendaorcamentoService.haveVendaorcamentoSituacao(itens, false, Vendaorcamentosituacao.CANCELADA, Vendaorcamentosituacao.REPROVADO)){
				request.addError("Or�amento(s) com situa��o(�es) diferente de 'EM ABERTO'.");
				return sendRedirectToAction("listagem");
			}
		}

		if(!vendaService.validaVendedorprincipal(bean, request)){
			return sendRedirectToAction("listagem");
		}
		
		if ("true".equals(request.getParameter("venda"))) {
			if (vendaorcamentoService.verificarPermitirMaterialMestreVenda(bean)) {
				request.addError("N�o � permitido autorizar o or�amento como venda, caso exista no or�amento, materiais mestre de grade.");
				
				return sendRedirectToAction("listagem");
			}
		}
		
		if("true".equals(request.getParameter("venda"))){
			return new ModelAndView("redirect:/faturamento/process/RealizarVenda?cdvendaorcamento=" + itens);
		} else {
			return new ModelAndView("redirect:/faturamento/process/Realizarpedidovenda?cdvendaorcamento=" + itens);
		}
	}
	
	/**
	 * M�todo ajax para buscar os produtos de acordo com a tabela de pre�o do cliente
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxBuscaMateriaisTabelaprecoByCliente(WebRequestContext request){
		return materialtabelaprecoService.buscaMateriaisTabelaprecoByCliente(request);		
	}
	
	public ModelAndView comboBoxContato(WebRequestContext request, Cliente cliente) {
		return vendaService.comboBoxContato(request, cliente);
	}
	
	public ModelAndView gerarSolicitacaocompra(WebRequestContext request){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}

		if(vendaorcamentoService.haveVendaorcamentoDiferenteSituacao(itens, Vendaorcamentosituacao.EM_ABERTO)){
			request.addError("Or�amento(s) de venda com situa��o(�es) diferente de 'Em Aberto'.");
			return sendRedirectToAction("listagem");
		}
		String solicitarCompraVenda = parametrogeralService.getValorPorNome(Parametrogeral.SOLICITARCOMPRAVENDA);
		if(solicitarCompraVenda != null){
			if("SOLICITACAOCOMPRA".equalsIgnoreCase(solicitarCompraVenda)) {
				return new ModelAndView("redirect:/suprimento/crud/Solicitacaocompra?ACAO=criar&whereInCdvendaorcamento=" + itens);
			}else if("ORDEMCOMPRA".equalsIgnoreCase(solicitarCompraVenda)) {
				return new ModelAndView("redirect:/suprimento/crud/Ordemcompra?ACAO=criar&whereInCdvendaorcamento=" + itens);
			}else if("REQUISICAOMATERIAL".equalsIgnoreCase(solicitarCompraVenda)) {
				return new ModelAndView("redirect:/suprimento/crud/Requisicaomaterial?ACAO=criar&whereInCdvendaorcamento=" + itens);
			}	
		}
		return new ModelAndView("redirect:/suprimento/crud/Solicitacaocompra?ACAO=criar&whereInCdvendaorcamento=" + itens);
	}
	
	public ModelAndView preencherFornecedor(WebRequestContext request, Vendaorcamento vendaorcamento){
		
		List<Fornecedor> listaFornecedor1 = fornecedorService.findForFaturamento(vendaorcamento.getEmpresa(), vendaorcamento.getMaterial().getCdmaterial().toString());
		List<GenericBean> listaFornecedor = new ArrayList<GenericBean>();
		
		for (Fornecedor fornecedor: listaFornecedor1){
			listaFornecedor.add(new GenericBean(fornecedor.getCdpessoa(), fornecedor.getNome()));
		}
		
		return new JsonModelAndView().addObject("listaFornecedor", listaFornecedor);
	}
	
	public ModelAndView escolherFornecedor(WebRequestContext request, Venda venda){
		return vendaService.escolherFornecedor(request, venda);
	}
	
	/**
	 * 
	 * @param vendaorcamento
	 * @param empresa
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarRepresentacao(WebRequestContext request, Vendaorcamento vendaorcamento){
		boolean representacao = false;
		if (vendaorcamento.getEmpresa()!=null && vendaorcamento.getEmpresa().getCdpessoa()!=null){
			representacao = empresaService.isRepresentacao(vendaorcamento.getEmpresa());
		}
		return new JsonModelAndView().addObject("representacao", representacao);
	}
	
	/**
	 * 
	 * @param vendaorcamento
	 * @param empresa
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarFornecedorProdutos(WebRequestContext request, Vendaorcamento vendaorcamento){
		
		String idsMaterial = request.getParameter("idsMaterial");
		Set<Integer> listaCdmaterial = new HashSet<Integer>();
		
		if (!idsMaterial.equals("")){
			List<Fornecedor> listaFornecedor = fornecedorService.findForFaturamento(vendaorcamento.getEmpresa(), idsMaterial);
			for (Fornecedor fornecedor: listaFornecedor){
				for (Materialfornecedor materialfornecedor: fornecedor.getListaMaterialfornecedor()){
					listaCdmaterial.add(materialfornecedor.getMaterial().getCdmaterial());
				}
			}
		}
		
		return new JsonModelAndView().addObject("listaCdmaterial", listaCdmaterial);
	}
	
	private void carregarImagem(WebRequestContext request, Arquivo arquivo, String atributo){
		try {
			arquivoService.loadAsImage(arquivo);
			if(arquivo.getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());
				request.setAttribute(atributo, true);
			}
		} catch (Exception e) {
			request.setAttribute(atributo, false);
			
		}
	}
	
	/**
	 * M�todo que abre uma popup para escolha dos itens da grade
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public ModelAndView selecionarItensGrade(WebRequestContext request){
		return new ModelAndView("direct:/crud/popup/popUpSelecionarItensGrade", "bean", vendaService.selecionarItensGrade(request));
	}
	
	/**
	 * M�todo que abre a popup para escolha do novo respons�vel do or�amento
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public ModelAndView abrirPopUpTrocarVendedor(WebRequestContext request){
		String id = request.getParameter("cdvendaorcamento");
		
		if(id == null || "".equals(id)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Vendaorcamento vendaorcamento = vendaorcamentoService.loadForTrocarvendedor(new Vendaorcamento(Integer.parseInt(id)));
		Usuario usuario = usuarioService.load(SinedUtil.getUsuarioLogado(), "usuario.cdpessoa, usuario.restricaoclientevendedor");
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		if(usuario != null && usuario.getRestricaoclientevendedor() != null && usuario.getRestricaoclientevendedor()){
			if(vendaorcamento != null && vendaorcamento.getCliente() != null && vendaorcamento.getCliente().getListaClientevendedor() != null &&
					!vendaorcamento.getCliente().getListaClientevendedor().isEmpty()){
				for(Clientevendedor clientevendedor : vendaorcamento.getCliente().getListaClientevendedor()){
					if(clientevendedor.getColaborador() != null && clientevendedor.getColaborador().getCdpessoa() != null){
						listaColaborador.add(clientevendedor.getColaborador());
					}
				}
			}
		}		
		
		request.setAttribute("exibirCombo", listaColaborador != null && !listaColaborador.isEmpty());
		request.setAttribute("listaColaboador", listaColaborador);
		request.setAttribute("pathTrocavendedor", "/faturamento/crud/VendaOrcamento");
		request.setAttribute("nomeBeanTrocavendedor", "Or�amento");
		
		TrocavendedorBean trocavendedorBean = new TrocavendedorBean(vendaorcamento.getCdvendaorcamento(), vendaorcamento.getColaborador());
		return new ModelAndView("direct:/crud/popup/popUpTrocarVendedor", "bean", trocavendedorBean);
	}
	
	/**
	 * M�todo que salva a troca de vendedor do pedido de venda
	 *
	 * @see br.com.linkcom.sined.geral.service.VendaorcamentoService#processaTrocaVendedor(Vendaorcamento vendaorcamentoWithColaboradorAtual)
	 *
	 * @param request
	 * @param trocavendedorBean
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void salvaTrocaVendedor(WebRequestContext request, TrocavendedorBean trocavendedorBean){
		if(trocavendedorBean.getId() == null){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		if(trocavendedorBean.getColaboradornovo() == null || trocavendedorBean.getColaboradornovo().getCdpessoa() == null){
			request.addError("Nenhum colaborador selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		Vendaorcamento vendaorcamento = new Vendaorcamento(trocavendedorBean.getId());
		vendaorcamento.setColaborador(trocavendedorBean.getColaboradornovo());
		
		vendaorcamentoService.processaTrocaVendedor(vendaorcamento);
		
		request.addMessage("Vendedor alterado com sucesso.");
		SinedUtil.redirecionamento(request, "/faturamento/crud/VendaOrcamento?ACAO=consultar&cdvendaorcamento=" + trocavendedorBean.getId());
	}	
	
	/**
	 * 
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, Vendaorcamento bean){
		
		if(bean.getPedidovendatipo() == null || bean.getPedidovendatipo().getCdpedidovendatipo() == null){
			throw new SinedException("O tipo de Atividade n�o pode se nulo.");
		}
		
		List<Campoextrapedidovendatipo> lista = campoextrapedidovendatipoService.findByTipo(bean.getPedidovendatipo());
		List<Orcamentovalorcampoextra> listaAtual = orcamentovalorcampoextraService.findByVendaorcamento(bean);
		
		if (listaAtual != null && lista != null){
			for (Orcamentovalorcampoextra campoExtraAtual : listaAtual){
				for (Campoextrapedidovendatipo campoExtra : lista){
					if (campoExtra.getCdcampoextrapedidovendatipo().equals(campoExtraAtual.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo())){
						campoExtra.setCdvalorcampoextraatual(campoExtraAtual.getCdorcamentovalorcampoextra());
						campoExtra.setValorAtual(campoExtraAtual.getValor());
					}
				}
			}
		}

		return new JsonModelAndView()
					.addObject("size", lista.size())
					.addObject("lista", lista);	
	}
	
	/**
	 * M�todo que busca uma lista de {@link Pedidovendatipo} e a renderiza em formato html para ser inserido na p�gina.
	 * @param request
	 * @return
	 */
	public ModelAndView ajaxLoadListaCampoExtra(WebRequestContext request){
		String msgError = "<null>";
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		JsonModelAndView json = new JsonModelAndView();
		
		if(cdpedidovendatipo != null && !cdpedidovendatipo.equals("")){
			Pedidovendatipo tipo = new Pedidovendatipo();
			tipo.setCdpedidovendatipo(Integer.valueOf(cdpedidovendatipo));
			List<Campoextrapedidovendatipo> lista = campoextrapedidovendatipoService.findByTipo(tipo);
		
			StringBuilder html = new StringBuilder();
			if(lista != null && !lista.isEmpty()){
				html.append("<option value='<null>'></option>");
				for(Campoextrapedidovendatipo campo : lista){
					html.append("<option value='br.com.linkcom.sined.geral.bean.Campoextrapedidovendatipo[cdcampoextrapedidovendatipo=")
						.append(campo.getCdcampoextrapedidovendatipo().toString())
						.append("]'>")
						.append(campo.getNome())
						.append("</option>");
				}
			}
			else{
				msgError = "N�o existem dados para este tipo.";
				return json.addObject("msgError", msgError);
			}
			
			return json.addObject("html", html.toString()).addObject("msgError", msgError);
		}
		else {
			msgError = "Tipo n�o definido";
			return json.addObject("msgError", msgError);
		}
	}
	
	public ModelAndView ajaxBuscaObservaocaoByCliente(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaObservaocaoByCliente(request, cliente);	
	}

	/**
	 * M�todo que autoriza o or�amento como contrato
	 * @param request
	 * @return
	 */
	public ModelAndView autorizarComoContrato(WebRequestContext request){
		String item = request.getParameter("selectedItens");
		if (item == null || item.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Vendaorcamento bean = vendaorcamentoService.load(new Vendaorcamento(Integer.parseInt(item)));
		
		if(vendaorcamentoService.haveVendaorcamentoSituacao(item, false, Vendaorcamentosituacao.AUTORIZADO, Vendaorcamentosituacao.CANCELADA, Vendaorcamentosituacao.REPROVADO)){
			if(vendaorcamentoService.isAutorizaContrato(bean) && 
					vendaorcamentoService.haveVendaorcamentoSituacao(item, false,Vendaorcamentosituacao.AUTORIZADO)){
				request.addError("Or�amento(s) j� autorizado como contrato.");
				return sendRedirectToAction("listagem");
			}else if(vendaorcamentoService.haveVendaorcamentoSituacao(item, false, Vendaorcamentosituacao.CANCELADA, Vendaorcamentosituacao.REPROVADO)){
				request.addError("Or�amento(s) com situa��o(�es) diferente de 'EM ABERTO'.");
				return sendRedirectToAction("listagem");
			}
		}
		
//		if(vendaorcamentoService.haveVendaorcamentoSituacao(item, false, Vendaorcamentosituacao.AUTORIZADO, Vendaorcamentosituacao.CANCELADA, Vendaorcamentosituacao.REPROVADO)){
//			request.addError("Or�amento com situa��o diferente de 'EM ABERTO'.");
//			return sendRedirectToAction("listagem");
//		}
		
		if (vendaorcamentoService.hasClienteComRestricoesEmAberto(Integer.parseInt(item))){
			request.addError("Cliente possui restri��o(�es) sem data de libera��o.");
			return sendRedirectToAction("listagem");	
		}
		
		if(!vendaService.validaVendedorprincipal(bean, request)){
			return sendRedirectToAction("listagem");
		}

		return new ModelAndView("redirect:/faturamento/crud/Contrato?ACAO=criar&cdvendaorcamento=" + item);
	}
	
	/**
	 * Chamada Ajax para carregamento das Formas de Pagameneto para determinado Cliente.
	 *
	 * @param request
	 * @param cliente
	 * @author Marcos Lisboa
	 */
	public ModelAndView ajaxLoadFormaAndPrazoPagamento(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxLoadFormaAndPrazoPagamento(request, cliente);
	}
	
	/**
	 * Ajax que busca o saldo do vale compra pelo cliente
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/09/2014
	 */
	public ModelAndView ajaxBuscaSaldovalecompraByCliente(WebRequestContext request, Cliente cliente){
		return valecompraService.actionSaldovalecompraVendaPedidoOrcamento(null, cliente);
	}
	
	/**
	* M�todo ajax que verifica as informa��es do tipo de pedido
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#verificarPedidovendatipoAJAX(WebRequestContext request, Pedidovendatipo pedidovendatipo, Empresa empresa)
	*
	* @param request
	* @param orcamento
	* @since 11/12/2015
	* @author Luiz Fernando
	*/
	public void verificarPedidovendatipoAJAX(WebRequestContext request, Vendaorcamento orcamento){
		boolean fromPedidovenda = request.getParameter("fromPedidovenda") != null ? Boolean.parseBoolean(request.getParameter("fromPedidovenda")) : false;
		boolean fromOrcamento = request.getParameter("fromOrcamento") != null ? Boolean.parseBoolean(request.getParameter("fromOrcamento")) : false;
		vendaService.verificarPedidovendatipoAJAX(request, orcamento.getPedidovendatipo(), orcamento.getEmpresa(), fromPedidovenda, fromOrcamento);
	}
	
	/**
	 * M�todo que armazena os dados necess�rios para executar o autocomplete de lote.
	 * 
	 * @param request
	 * @author Rafael Salvio
	 */
	public void ajaxSetDadosSessaoForLoteAutocomplete(WebRequestContext request){
		pedidovendaService.ajaxSetDadosSessaoForLoteAutocomplete(request, "VENDAORCAMENTO");
	}
	
	/**
	* M�todo que abre a popup de kit flexivel
	*
	* @param request
	* @param bean
	* @return
	* @since 31/03/2015
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpKitFlexivel(WebRequestContext request, KitflexivelBean bean){
		return vendaService.abrirPopUpKitFlexivel(request, bean);
	}
	
	public ModelAndView abrirPopUpKitFlexivelComposicao(WebRequestContext request, KitflexivelBean bean){
		return vendaService.abrirPopUpKitFlexivelComposicao(request, bean);
	}
	
	/**
	 * M�todo que abre a popup para consulta e edi��o da porcentagem da comiss�o das ag�ncias.
	 *
	 * @param request
	 * @return
	 * @author Jo�o Vitor
	 * @since 02/06/2015
	 */
	public ModelAndView abrirPopUpComissaoAgencia(WebRequestContext request, Vendamaterial vendamaterial){
		return vendaService.abrirPopUpComissaoAgencia(request, vendamaterial);
	}
	
	public ModelAndView changeFornecedoragencia(WebRequestContext request, FornecedoragenciaBean fornecedoragenciaBean) {
		return vendaService.changeFornecedoragencia(request, fornecedoragenciaBean);
	}
	
	/**
	* M�todo ajax que busca o local da empresa, caso a empresa possua apenas um local
	*
	* @param request
	* @return
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxBuscaLocalArmazenagemUnico(WebRequestContext request){	
		return vendaService.ajaxBuscaLocalArmazenagemUnico(request);
	}
	
	/**
	* M�todo M�todo ajax que retorna o valor aproximado de imposto da venda
	*
	* @param request
	* @param venda
	* @return
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularValorAproximadoImposto(WebRequestContext request, Vendaorcamento venda){	
		vendaorcamentoService.calcularValorAproximadoImposto(venda);
		return new JsonModelAndView().addObject("bean", venda);
	}
	
	/**
	* M�todo ajax para calcular o imposto na venda
	*
	* @param request
	* @param impostovenda
	* @return
	* @since 11/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularImposto(WebRequestContext request, Impostovenda impostovenda){	
		return new JsonModelAndView().addObject("bean", vendaService.calcularImposto(impostovenda));
	}
	
	/**
	* M�todo ajax para calcular o peso do material
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#ajaxCalcularPesoMaterial(WebRequestContext request, Material material)
	*
	* @param request
	* @param material
	* @return
	* @since 26/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxCalcularPesoMaterial(WebRequestContext request, Material material){
		return vendaService.ajaxCalcularPesoMaterial(request, material);
	}
	
	/**
	 * M�todo ajax que retorna uma lista JSON com os prazos de pagamentos do cliente
	 *
	 * @param request
	 * @return
	 * @since 27/05/2016
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView getFormasPrazosPagamentoClienteJSON(WebRequestContext request){
		boolean addForma = "true".equalsIgnoreCase(request.getParameter("addForma"));
		boolean addPrazo = "true".equalsIgnoreCase(request.getParameter("addPrazo"));
		return clienteService.getFormasPrazosPagamentoClienteAlertaModelAndView(request, addForma, addPrazo);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param request
	* @param empresa
	* @return
	* @since 18/06/2016
	* @author Luiz Fernando
	*/
	public ModelAndView carregaInfEmpresa(WebRequestContext request, Empresa empresa){
		return vendaService.carregaInfEmpresa(request, empresa); 
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#abrirPopUpInfoFinanceiraCliente(WebRequestContext request, Cliente cliente)
	*
	* @param request
	* @param cliente
	* @return
	* @since 22/08/2016
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpInfoFinanceiraCliente(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInfoFinanceiraCliente(request, cliente);
	}
	
	public ModelAndView buscaPrazoEntrega(WebRequestContext request, Venda venda){
		return vendaService.buscaPrazoEntrega(request, venda);
	}
	
	public ModelAndView comboBoxTerceiro(WebRequestContext request, Venda venda) {
		return vendaService.comboBoxTerceiro(request, venda);
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView abrirPopUpInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInformacaoClienteVenda(request, cliente);
	}
	
	public void ajaxSugerirPrazopagamentoCliente(WebRequestContext request){
		vendaService.ajaxSugerirPrazopagamentoCliente(request);
	}
	
	public ModelAndView ajaxBuscaVendedorprincipalByCliente(WebRequestContext request, Cliente cliente){
		return clientevendedorService.actionVendedorprincipalVendaPedido(cliente);
	}
	
	public ModelAndView abrePopupAjudaVendedorprincipal(WebRequestContext request){
		return vendaService.abrePopupAjudaVendedorprincipal(request);
	}
	
	public ModelAndView ajaxObrigarProjeto(WebRequestContext request, Vendaorcamento vendaorcamento){
		boolean obrigar = false;
		if (vendaorcamento.getPedidovendatipo()!=null && vendaorcamento.getPedidovendatipo().getCdpedidovendatipo()!=null){
			obrigar = Boolean.TRUE.equals(pedidovendatipoService.load(vendaorcamento.getPedidovendatipo()).getObrigarProjeto());
		}
		return new JsonModelAndView().addObject("obrigar", obrigar);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Vendaorcamento form)
			throws CrudException {
		if(!vendaService.validaVendedorprincipal(form, request)){
			return continueOnAction("entrada", form);
		}
		return super.doSalvar(request, form);
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Vendaorcamento form)
			throws CrudException {
		String whereInOportunidade = request.getParameter("whereInOportunidade");
		if(Util.strings.isNotEmpty(whereInOportunidade) &&
			!oportunidadeService.isUsuarioPodeEditarOportunidades(whereInOportunidade)){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.");
			String urlRedirecionamento = "redirect:/crm/crud/Oportunidade";
			if("entrada".equals(request.getParameter("controller"))){
				urlRedirecionamento +=  "?ACAO=consultar&cdoportunidade="+whereInOportunidade;
			}
			return new ModelAndView(urlRedirecionamento);
		}
		// Verifica��es E-Commerce		
		String nomeEcommerce = parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE);		
		request.setAttribute("IS_TRAYCORP", nomeEcommerce != null && nomeEcommerce.equals("TRAYCORP"));
		boolean eCommerceEnabled = parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && nomeEcommerce != null; 
		request.setAttribute("ECOMMERCE_ENABLED", eCommerceEnabled);	
		return super.doCriar(request, form);
	}
	
	public ModelAndView abrirSelecaoModelocomprovanteRTF(WebRequestContext request, Vendaorcamento form){
		SinedUtil.validaObjectWithExecption(form);
		
		Vendaorcamento vendaorcamento = vendaorcamentoService.loadReader(form);
		pedidovendaService.setInfoSelecaoModeloComprovanteRTF(request, vendaorcamento.getEmpresa(), TipoComprovanteEnum.ORCAMENTO);
		request.setAttribute("cdvendaorcamento", vendaorcamento.getCdvendaorcamento());
		return new ModelAndView("direct:/crud/popup/emitirComprovanteorcamentoRTF");
	}
	
	public ModelAndView ajaxVerificaTemplatesComprovanteRTF(WebRequestContext request, Vendaorcamento form){
		SinedUtil.validaObjectWithExecption(form);
		
		ModelAndView retorno = new JsonModelAndView();
		Vendaorcamento vendaorcamento = vendaorcamentoService.loadReader(form);
		pedidovendaService.verificaModeloRTF(retorno, vendaorcamento.getEmpresa(), TipoComprovanteEnum.ORCAMENTO);
		return retorno;
	}
	
	public ModelAndView buscarPedidoVendaTipoAjax(WebRequestContext request, Vendaorcamento vendaorcamento) {
		return pedidovendatipoService.buscarPedidoVendaTipoAjax(vendaorcamento.getPedidovendatipo());
	}
	
	public ModelAndView verificarPedidoVendaTipoBloqueioQuantidade(WebRequestContext request) {
		return pedidovendatipoService.verificarPedidoVendaTipoBloqueioQuantidade(request);
	}
	
	public ModelAndView ajaxBuscaFaixaMarkup(WebRequestContext request, Vendaorcamento venda){
		return materialFaixaMarkupService.findJsonFaixaMarkup(venda.getMaterial(), venda.getUnidademedida(), venda.getEmpresa());
	}
	
	public ModelAndView ajaxAbrePopupHistoricoPrecoMaterial(WebRequestContext request, MaterialHistoricoPrecoFiltro filtro){
		return materialService.ajaxAbrePopupHistoricoPrecoMaterial(request, filtro);
	}
	
	public ModelAndView ajaxMaterialProducaoOrKitOrGrade(WebRequestContext request, Material material){
		return materialService.ajaxMaterialProducaoOrKitOrGrade(request, material);
	}
	
	public void buscarSugestaoUltimoValorVendaAJAX(WebRequestContext request){
		vendamaterialService.buscarSugestaoUltimoValorVendaAJAX(request);
	}
	
	public ModelAndView abrePopupImposto(WebRequestContext request, Vendaorcamentomaterial bean){
		return vendaService.abrePopupImposto(request, bean);
	}
	
	public ModelAndView abrePopupImpostoMestre(WebRequestContext request, Pedidovendamaterialmestre bean){
		return vendaService.abrePopupImposto(request, bean);
	}
	
	public ModelAndView ajaxMostrarTotalImpostos(WebRequestContext request, Vendaorcamento bean){
		return vendaService.ajaxMostrarTotalImpostos(request, bean);
	}
	
	public ModelAndView ajaxEnderecoFaturamento(WebRequestContext request, Cliente cliente){
		return enderecoService.ajaxEnderecoFaturamento(request, cliente);
	}
	
	public ModelAndView ajaxBuscarLotesQuantidadeMaterial(WebRequestContext request, Vendaorcamento vendaorcamento){
		return vendaService.ajaxBuscarLotesQuantidadeMaterial(request, vendaorcamento.getListaInclusaoLoteVendaInterface());
	}
	
	public ModelAndView ajaxVerificaCalcularTicketMedio(WebRequestContext request, Pedidovendatipo pedidovendatipo ){
		return vendaService.ajaxVerificaCalcularTicketMedio(request, pedidovendatipo);
	}
}