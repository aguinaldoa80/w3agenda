package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.io.IOException;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Boleto;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoconfiguravelBean;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/financeiro/process/Boletoconfiguravel",
		authorizationModule=ProcessAuthorizationModule.class
)
public class BoletoconfiguravelProcess extends MultiActionController {
	
	private ContareceberService contareceberService;
	private DocumentoService documentoService;
	
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	
	/**
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView escolhaConf(WebRequestContext request, BoletoconfiguravelBean bean){
		return new ModelAndView("direct:/process/popup/escolhaBoleto", "bean", bean);
	}
	
	/**
	 * M�todo que fecha modal e envia para p�gina nova 
	 * 
	 * @param request
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public void confirmaEscolha(WebRequestContext request, BoletoconfiguravelBean bean){
		View.getCurrent().println("<script>window.();</script>");
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>window.open('../../financeiro/process/Boletoconfiguravel?ACAO=carregaBoleto&selectedItens=" + bean.getSelectedItens() + "&cdboleto=" + bean.getBoleto().getCdboleto() + "','page','toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=no,width=1020,height=800');parent.$.akModalRemove(true);</script>");
	}
	
	/**
	 * M�todo que gera boleto em popup HTML
	 * 
	 * @param request
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView carregaBoleto(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String cdboleto = request.getParameter("cdboleto");
		try {
			return new ModelAndView("direct:process/popup/boleto", "boleto", contareceberService.criaBoletoHTML(whereIn, new Boleto(Integer.valueOf(cdboleto))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * M�todo chamado via AJAX que valida os documentos
	 * 
	 * @param request
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView validaDocumentosAjax(WebRequestContext request, String whereIn){
		String msg = null;
		if (documentoService.isDocumentoNotDefitivaNotPrevista(SinedUtil.getItensSelecionados(request))) {
			msg = "S� pode imprimir boleto de uma conta a receber que estiver na situa��o prevista ou definitiva.";
		}
		
		return new JsonModelAndView().addObject("msg", msg);
	}
	
}
