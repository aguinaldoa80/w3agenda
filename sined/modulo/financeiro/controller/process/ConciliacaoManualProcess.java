package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.sql.Date;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ConciliacaoManualBean;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/financeiro/process/ConciliacaoManual",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ConciliacaoManualProcess extends MultiActionController {
	
	private MovimentacaoService movimentacaoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ParametrogeralService parametrogeralService;
	
	public void setParametrogeralService(ParametrogeralService service){
		this.parametrogeralService = service;
	}

	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setFechamentofinanceiroService(
			FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}


	/**
	 * Carrega a lista de movimenta��es a serem exibidas na tela de concilia��o manual.
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	@DefaultAction
	public ModelAndView carregaMovimentacao(WebRequestContext request, Movimentacao bean){
		
		movimentacaoService.callProcedureAtualizaMovimentacao(bean);
		bean = movimentacaoService.carregaMovimentacao(bean);
		
		//Verifica��o de data limite do ultimo fechamento. 
		if(fechamentofinanceiroService.verificaFechamento(bean)){
			SinedUtil.recarregarPaginaWithClose(request);
			request.addError("Esta movimenta��o n�o pode ser conciliada pois refere-se a um per�odo j� fechado.");
			return null;
		}
		List<Movimentacao> listaMovimentacaoVinculada = movimentacaoService.findMovimentacaoVinculoDevolucao(bean);
		if(SinedUtil.isListNotEmpty(listaMovimentacaoVinculada)){
			SinedUtil.recarregarPaginaWithClose(request);
			request.addError("N�o foi poss�vel realizar a concilia��o. � Necess�rio conciliar a movimenta��o " + 
								CollectionsUtil.listAndConcatenate(listaMovimentacaoVinculada, "cdmovimentacao", ",") +	
								" que est� vinculada a esta");
			return null;
		}
		
		Date dtbanco = bean.getDtbanco();
		String checknum = bean.getChecknum();
		String historico = bean.getHistorico();
		
		
		bean.setDtbanco(dtbanco);
		bean.setChecknum(checknum);
		bean.setHistorico(historico != null && !historico.equals("") ? historico : bean.getAux_movimentacao().getMovimentacaodescricao());
		
		// tratar erro caso o parametro geral seja nulo.
		request.setAttribute("CONCILIACAOMANUAL_CAMPO_DOCUMENTO", parametrogeralService.buscaValorPorNome(Parametrogeral.CONCILIACAOMANUAL_CAMPO_DOCUMENTO));
		
		return new ModelAndView("direct:process/popup/conciliacaoManual", "movimentacao", bean);
	}
	
	
	
	/**
	 * Atualiza as movimenta��es e seus hist�ricos correspondentes. 
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#doConciliacaoManual(Movimentacao)
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("saveMovimentacao")
	@Input("carregaMovimentacao")
	@Command(validate=true)
	public void saveMovimentacao(WebRequestContext request, Movimentacao bean){
		//Verifica��o de data limite do ultimo fechamento. 
		if(fechamentofinanceiroService.verificaFechamentoDatabanco(bean)){
			SinedUtil.recarregarPaginaWithClose(request);
			request.addError("Esta movimenta��o n�o pode ser conciliada pois refere-se a um per�odo j� fechado.");
			return;
		}
		
		if(SinedUtil.isListNotEmpty(movimentacaoService.findNotNormal(bean.getCdmovimentacao().toString()))){
			SinedUtil.recarregarPaginaWithClose(request);
			request.addError("Para conciliar, a movimenta��o tem que estar com a situa��o 'Normal'.");
			return;
		}
		
		bean.setObservacao("Concilia��o manual");
		movimentacaoService.doConciliacaoManual(bean);
		request.addMessage("Movimenta��o conciliada com sucesso.");
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
		
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if (obj instanceof Movimentacao) {
			Movimentacao movimentacao = (Movimentacao) obj;
			
			if (movimentacao.getConta() != null && movimentacao.getConta().getDtsaldo() != null && movimentacao.getDtbanco().getTime() < movimentacao.getConta().getDtsaldo().getTime()) {
				errors.reject("001", "A data do banco � inferior a data do saldo inicial da conta.");
			}
		} else if (obj instanceof ConciliacaoManualBean) {
			ConciliacaoManualBean bean = (ConciliacaoManualBean) obj; 
			List<Movimentacao> lista = bean.getListaMovimentacao();
			for (Movimentacao movimentacao : lista) {
				if (movimentacao.getConta() != null && movimentacao.getConta().getDtsaldo() != null && movimentacao.getDtbanco().getTime() < movimentacao.getConta().getDtsaldo().getTime()) {
					errors.reject("001", "A data do banco � inferior a data do saldo inicial da conta.");
				}
			}
		}
	}
	
	public ModelAndView concilicarMovimentacoes(WebRequestContext request, ConciliacaoManualBean bean){
		String whereIn;
		if(bean.getListaMovimentacao() != null && bean.getListaMovimentacao().size() > 0){
			whereIn = CollectionsUtil.listAndConcatenate(bean.getListaMovimentacao(), "cdmovimentacao", ",");
		} else {
			whereIn = SinedUtil.getItensSelecionados(request);
		}
		
		//Verifica��o de data limite do ultimo fechamento. 
		Movimentacao mov = new Movimentacao();
		mov.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(mov)){
			SinedUtil.recarregarPaginaWithClose(request);
			request.addError("Existem movimenta��es cuja data da movimenta��o refere-se a um per�odo j� fechado.");
			return null;
		}
		
		List<Movimentacao> listaMovimentacao = movimentacaoService.carregaMovimentacao(whereIn);
		for (Movimentacao movimentacao : listaMovimentacao) {
			if(!movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.NORMAL)){
				request.addError("Para conciliar, as movimenta��o tem que estar com a situa��o 'NORMAL'.");
				SinedUtil.recarregarPaginaWithClose(request);
				return null;
			}
			
			List<Movimentacao> listaMovimentacaoVinculada = movimentacaoService.findMovimentacaoVinculoDevolucao(movimentacao);
			if(SinedUtil.isListNotEmpty(listaMovimentacaoVinculada)){
				SinedUtil.recarregarPaginaWithClose(request);
				request.addError("N�o foi poss�vel realizar a concilia��o. � Necess�rio conciliar a movimenta��o " + 
									CollectionsUtil.listAndConcatenate(listaMovimentacaoVinculada, "cdmovimentacao", ",") +	
									" que est� vinculada a esta");
				SinedUtil.recarregarPaginaWithClose(request);
				return null;
			}
			
			String historico = movimentacao.getHistorico();
			movimentacao.setHistorico(historico != null && !historico.equals("") ? historico : movimentacao.getAux_movimentacao().getMovimentacaodescricao());
		}
		
		bean = new ConciliacaoManualBean();
		bean.setListaMovimentacao(listaMovimentacao);
		
		// tratar erro caso o parametro geral seja nulo.
		request.setAttribute("CONCILIACAOMANUAL_CAMPO_DOCUMENTO", parametrogeralService.buscaValorPorNome(Parametrogeral.CONCILIACAOMANUAL_CAMPO_DOCUMENTO));
		return new ModelAndView("direct:process/popup/conciliacaoManualVarias", "bean", bean);
	}
	
	@Input("concilicarMovimentacoes")
	@Command(validate=true)
	public void saveMovimentacaoVarias(WebRequestContext request, ConciliacaoManualBean bean){
		
		List<Movimentacao> lista = bean.getListaMovimentacao();
		
		for (Movimentacao movimentacao : lista) {
			//Verifica��o de data limite do ultimo fechamento. 
			if(fechamentofinanceiroService.verificaFechamentoDatabanco(movimentacao)){
				SinedUtil.recarregarPaginaWithClose(request);
				request.addError("Esta movimenta��o n�o pode ser conciliada pois refere-se a um per�odo j� fechado.");
				return;
			}
		}
		String whereIn = CollectionsUtil.listAndConcatenate(bean.getListaMovimentacao(), "cdmovimentacao", ",");
		if(SinedUtil.isListNotEmpty(movimentacaoService.findNotNormal(whereIn))){
			SinedUtil.recarregarPaginaWithClose(request);
			request.addError("Para conciliar, a movimenta��o tem que estar com a situa��o 'Normal'.");
			return;
		}
		for (Movimentacao movimentacao : lista) {
			movimentacao.setObservacao("Concilia��o Manual");
			movimentacaoService.doConciliacaoManual(movimentacao);
		}
		
		request.addMessage("Movimenta��es conciliada com sucesso.");
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.location.href = '" + request.getServletRequest().getContextPath() + "/financeiro/crud/Movimentacao';window.close();</script>");
	}
}
