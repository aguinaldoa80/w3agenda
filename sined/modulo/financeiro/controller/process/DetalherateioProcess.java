package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.List;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/financeiro/process/DetalheRateioProcess", authorizationModule=ProcessAuthorizationModule.class)
public class DetalherateioProcess extends MultiActionController{

	private ContagerencialService contagerencialService;
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	
	/**
	 * M�todo para montar o combo de conta gerencial na inser��o de itens do rateio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findByTipooperacao(Tipooperacao)
	 * @param request
	 * @param tipooperacao
	 * @author Fl�vio Tavares
	 */
	public void findTipooperacao(WebRequestContext request, Tipooperacao tipooperacao){
		List<Contagerencial> listaTipo = contagerencialService.findAnaliticas(tipooperacao);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaTipo, "listatipo", null));
	}
}
