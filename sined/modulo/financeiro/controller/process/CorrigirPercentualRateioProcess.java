package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.thread.CorrigirPercentualRateioThread;

@Controller(path="/financeiro/process/CorrigirRateio")
public class CorrigirPercentualRateioProcess extends MultiActionController {

	@Action("corrigir")
	public ModelAndView doCorrigir(WebRequestContext request) throws IOException{
		boolean paramExecutar = Boolean.parseBoolean(request.getParameter("executar"));
		SinedExcel wb = new SinedExcel();
		HSSFSheet sheet = wb.createSheet("Relatorio");
		sheet.setVerticallyCenter(true);
		sheet.setHorizontallyCenter(true);
		sheet.setDefaultColumnWidth((short) 30);
		sheet.setDefaultRowHeight((short) (10*35));
		Short linha = 0;
		Integer estilo = 0;
		Corrigir(paramExecutar, sheet, linha, estilo, SinedUtil.getSubdominioCliente());
		return new ResourceModelAndView(wb.getWorkBookResource("relatorio_correcao"));
	}
	
	@Action("corrigirTodos")
	public ModelAndView doCorrigirTodos(WebRequestContext request) throws IOException{
		boolean salvarAlteracoes = Boolean.parseBoolean(request.getParameter("executar"));
		CorrigirPercentualRateioThread thread = new CorrigirPercentualRateioThread(salvarAlteracoes, "relatorio");
		thread.start();
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public static void Corrigir(Boolean salvarAlteracoes, HSSFSheet sheet, Short linha, Integer estilo, String banco){
		List<Documento> documentos = DocumentoService.getInstance().getDocumentosForCorrecaoRateio();
		if(documentos != null && !documentos.isEmpty()){
			Integer cdusuarioaltera = 1;
			Timestamp dtaltera = new Timestamp(System.currentTimeMillis());

			linha = imprimeCabecalho(sheet, linha);
			for(Documento documento : documentos){
				try{
					validateRateio(documento.getRateio(), documento.getValor());
					if(possuiErroPercentualTotal(documento.getRateio())){
						linha = imprimeLinha(sheet, linha, estilo++, banco, documento, null, new Exception("Documento possui percentual total do rateio diferente de 100%"));
						Boolean erroCalculoPercentual = Boolean.FALSE;
						for (Rateioitem ri : documento.getRateio().getListaRateioitem()) {
							ri.setCdusuarioaltera(cdusuarioaltera);
							ri.setDtaltera(dtaltera);
							try{
								Double valorAux = ri.getValor().getValue().doubleValue()/documento.getValor().getValue().doubleValue() * 100d;
								valorAux = SinedUtil.round(valorAux, 10);
								ri.setPercentual(valorAux);
							}catch (Exception e) {
								erroCalculoPercentual = Boolean.TRUE;
								linha = imprimeLinha(sheet, linha, estilo++, banco, documento, ri, e);
							}
						}
						if(!erroCalculoPercentual && salvarAlteracoes != null && salvarAlteracoes){
							documento.getRateio().setCdusuarioaltera(cdusuarioaltera);
							documento.getRateio().setDtaltera(dtaltera);
							RateioService.getInstance().saveOrUpdate(documento.getRateio());
						}
					}
				}catch (Exception e) {
					linha = imprimeLinha(sheet, linha, estilo++, banco, documento, null, e);
				}
			}	
		}
	}
	
	private static Short imprimeCabecalho(HSSFSheet sheet, Short linha){
		HSSFRow row;
		HSSFCell cell;

		row = sheet.createRow((short) linha++);
		row.setHeight((short) (12*35));

		cell = row.createCell((short) 0);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Banco");
		cell = row.createCell((short) 1);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Documento");
        cell = row.createCell((short) 2);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Dt. Documento");
        cell = row.createCell((short) 3);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Valor Documento");
        cell = row.createCell((short) 4);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Valor Rateado Documento");
        cell = row.createCell((short) 5);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Percentual Documento");
        cell = row.createCell((short) 6);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Rateioitem");
        cell = row.createCell((short) 7);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Valor Rateioitem");
        cell = row.createCell((short) 8);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Percentual Rateioitem");
        cell = row.createCell((short) 9);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("Mensagem de erro");
        cell = row.createCell((short) 10);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("StackTrace");
        
        return linha;
	}
	
	private static Short imprimeLinha(HSSFSheet sheet, Short linha, Integer estilo, String banco, Documento documento, Rateioitem rateioitem, Exception e){
		HSSFCellStyle style;
		HSSFRow row;
		HSSFCell cell;
		if(estilo%2 == 0){
			style = SinedExcel.STYLE_DETALHE_LEFT_GREY;
		} else{
			style = SinedExcel.STYLE_DETALHE_LEFT_WHITE;
		}
		row = sheet.createRow(linha++);

		cell = row.createCell((short) 0);
		cell.setCellStyle(style);
		cell.setCellValue(banco != null ? banco.toString() : "?");
		cell = row.createCell((short) 1);
		cell.setCellStyle(style);
		cell.setCellValue(documento != null ? documento.getCddocumento().toString() : "");
		cell = row.createCell((short) 2);
		cell.setCellStyle(style);
		cell.setCellValue(documento != null ? documento.getDtemissao().toString() : "");
		cell = row.createCell((short) 3);
		cell.setCellStyle(style);
		cell.setCellValue(documento != null && documento.getValor() != null ? documento.getValor().getValue().toString() : "");
		cell = row.createCell((short) 4);
		cell.setCellStyle(style);
		cell.setCellValue(documento != null && documento.getRateio() != null && documento.getRateio().getValor() != null ? documento.getRateio().getValor().getValue().toString() : "");
		cell = row.createCell((short) 5);
		cell.setCellStyle(style);
		cell.setCellValue(documento != null && documento.getRateio() != null && documento.getRateio().getPercentual() != null ? documento.getRateio().getPercentual().toString() : "");
		cell = row.createCell((short) 6);
		cell.setCellStyle(style);
		cell.setCellValue(rateioitem != null ? rateioitem.getCdrateioitem().toString() : "");
		cell = row.createCell((short) 7);
		cell.setCellStyle(style);
		cell.setCellValue(rateioitem != null &&  rateioitem.getValor() != null ? rateioitem.getValor().getValue().toString() : "");
		cell = row.createCell((short) 8);
		cell.setCellStyle(style);
		cell.setCellValue(rateioitem != null && rateioitem.getPercentual() != null ? rateioitem.getPercentual().toString() : "");
		cell = row.createCell((short) 9);
		cell.setCellStyle(style);
		cell.setCellValue(e.getMessage());
		cell = row.createCell((short) 10);
		cell.setCellStyle(style);
		cell.getCellStyle().setHidden(true);
		StringBuilder st = new StringBuilder();
		if(e.getStackTrace() != null && e.getStackTrace().length > 0){
			for(int i = 0; i < e.getStackTrace().length; i++){
				st.append(e.getStackTrace()[i]).append("\n");
			}
		}
		cell.setCellValue(st.toString());
		row.setHeight( (short) 800);
		return linha;
	}
	
	private static Boolean possuiErroPercentualTotal(Rateio rateio){
		if(rateio != null && rateio.getListaRateioitem() != null && !rateio.getListaRateioitem().isEmpty()){
			Double percentual = 0d;
			for(Rateioitem item : rateio.getListaRateioitem()){
				if(item.getPercentual() != null && !percentual.isNaN()){
					percentual += item.getPercentual();
				}
			}
			if(Math.abs(percentual-100d) < 0.000000001d){
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}
	
	private static void validateRateio(Rateio rateio, Money valorTotal)throws SinedException {
		if(rateio == null){
			throw new SinedException("O rateio n�o pode vazio.");
		}
		if (valorTotal == null) {
			throw new SinedException("O valor total n�o pode ser vazio.");
		}
		
		Money valorRateado = new Money();
		Double percentualRateio = 0d;
		List<Rateioitem> listaRateioitem = rateio.getListaRateioitem();
		
		for (Rateioitem rateioitem : listaRateioitem) {
			if (rateioitem != null){
				if(rateioitem.getValor() != null){
					valorRateado = valorRateado.add(new Money(rateioitem.getValor().getValue(), false));
				}
				if(rateioitem.getPercentual() != null){
					percentualRateio += rateioitem.getPercentual();
				}
			}
			
		}
		rateio.setValor(valorRateado);
		rateio.setPercentual(percentualRateio);
		if(SinedUtil.round(valorRateado.getValue(), 2).compareTo(SinedUtil.round(valorTotal.getValue(), 2)) != 0){
			throw new SinedException("O valor total rateado � diferente do valor total.");
		}
	}
}
