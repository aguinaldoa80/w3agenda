package br.com.linkcom.sined.modulo.financeiro.controller.process;



import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivoconciliacaooperadora;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadora;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraStatusPagamento;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraTipoTransacao;
import br.com.linkcom.sined.geral.service.ArquivoconciliacaooperadoraService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.TaxaparcelaService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoOperadoraBean;
import br.com.linkcom.sined.util.DateUtils;
import br.com.linkcom.sined.util.SinedException;

@Controller(path="/financeiro/process/ArquivoconciliacaooperadoraProcess",authorizationModule=ProcessAuthorizationModule.class)
public class ArquivoconciliacaooperadoraProcess extends MultiActionController {
	
	private ArquivoconciliacaooperadoraService arquivoconciliacaooperadoraService;
	private TaxaparcelaService taxaparcelaService;
	private ContareceberService contareceberService;
	private EmpresaService empresaService;
	
	public void setArquivoconciliacaooperadoraService(ArquivoconciliacaooperadoraService arquivoconciliacaooperadoraService) {
		this.arquivoconciliacaooperadoraService = arquivoconciliacaooperadoraService;
	}
	
	public void setTaxaparcelaService(TaxaparcelaService taxaparcelaService) {
		this.taxaparcelaService = taxaparcelaService;
	}
	
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, ArquivoConciliacaoOperadoraBean bean) throws Exception{
		return new ModelAndView("process/arquivoconciliacaooperadora","filtro",bean);
	}
	
	
	public void processar(WebRequestContext request, ArquivoConciliacaoOperadoraBean arquivo) {
		if (arquivo == null || arquivo.getArquivo() == null || !StringUtils.isNotBlank(arquivo.getArquivoTexto())) {
			System.out.println("Arquivo Inv�lido.");
			return;
		}

		try{
			Integer opcaoExtrato = Integer.parseInt(arquivo.getArquivoTextoHeaderOpcaoExtrato());

			if (opcaoExtrato < 4 ) {
				this.processaArquivoVenda(arquivo);
			} else {
				this.processaArquivoPagamento(arquivo);
			}

		}catch (Exception e) {
			throw new SinedException("Erro ao processar o arquivo de concili��o.");
		}
	}
	
	public void processaArquivoVenda(ArquivoConciliacaoOperadoraBean arquivo){
		String[] linhas = arquivo.getArquivoTexto().split("\\r?\\n");
		String linhaRegistro1 = "";
		Arquivoconciliacaooperadora arquivoConciliacaoOperadora = new Arquivoconciliacaooperadora();
		Empresa empresa = empresaService.loadPrincipal();
		for (String linha : linhas) {
			arquivoConciliacaoOperadora = new Arquivoconciliacaooperadora();

			if (linha.startsWith("0")) {
				Integer numeroEstabelecimento = Integer.parseInt(linha.substring(EnumArquivoConciliacaoOperadora.ESTABELECIMENTO.getInicio(), EnumArquivoConciliacaoOperadora.ESTABELECIMENTO.getFim()));
				if (numeroEstabelecimento != null) {
					if (empresa != null && empresa.getEstabelecimento() == null ) {
						empresa.setEstabelecimento(numeroEstabelecimento);
						empresaService.saveOrUpdateNumeroEstabelecimento(empresa);
					}
				}
			} else if (linha.startsWith("1")) {
				linhaRegistro1 = linha;
			} else if(linha.startsWith("2")) {
				arquivoConciliacaoOperadora.setEstabelecimento(Integer.parseInt(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.ESTABELECIMENTO.getInicio(),EnumArquivoConciliacaoOperadora.ESTABELECIMENTO.getFim())));
				arquivoConciliacaoOperadora.setNumeroro(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.NUMERO_RO.getInicio(),EnumArquivoConciliacaoOperadora.NUMERO_RO.getFim()));
				arquivoConciliacaoOperadora.setDataapresentacao(DateUtils.stringToDate(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.DATA_APRESENTACAO.getInicio(),EnumArquivoConciliacaoOperadora.DATA_APRESENTACAO.getFim()),"yyMMdd"));
				arquivoConciliacaoOperadora.setTipotransacao(EnumArquivoConciliacaoOperadoraTipoTransacao.getEnumTipoTransacao(Integer.parseInt(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.TIPO_TRANSACAO.getInicio(), EnumArquivoConciliacaoOperadora.TIPO_TRANSACAO.getFim()))));
				arquivoConciliacaoOperadora.setDataprevistapagamento(DateUtils.stringToDate(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.DATA_PREVISTA_PAGAMENTO.getInicio(), EnumArquivoConciliacaoOperadora.DATA_PREVISTA_PAGAMENTO.getFim()),"yyMMdd"));
				arquivoConciliacaoOperadora.setDataenviobanco(DateUtils.stringToDate(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.DATA_ENVIO_BANCO.getInicio(), EnumArquivoConciliacaoOperadora.DATA_ENVIO_BANCO.getFim()),"yyMMdd"));
				arquivoConciliacaoOperadora.setValorbruto(Double.parseDouble(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.VALOR_BRUTO.getInicio(), EnumArquivoConciliacaoOperadora.VALOR_BRUTO.getFim()))/100);
				arquivoConciliacaoOperadora.setValorliquido(Double.parseDouble(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.VALOR_LIQUIDO.getInicio(),EnumArquivoConciliacaoOperadora.VALOR_LIQUIDO.getFim()))/100);
				arquivoConciliacaoOperadora.setStatuspagamento(EnumArquivoConciliacaoOperadoraStatusPagamento.getEnumArquivoConciliacaoOperadoraStatusPagamento(Integer.parseInt(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.STATUS_PAGAMENTO.getInicio(),EnumArquivoConciliacaoOperadora.STATUS_PAGAMENTO.getFim()))));
				arquivoConciliacaoOperadora.setDatavenda(DateUtils.stringToDate(linha.substring(EnumArquivoConciliacaoOperadora.DATA_VENDA.getInicio(),EnumArquivoConciliacaoOperadora.DATA_VENDA.getFim()),"yyyyMMdd"));
				arquivoConciliacaoOperadora.setValorvenda(Double.parseDouble(linha.substring(EnumArquivoConciliacaoOperadora.VALOR_VENDA.getInicio(), EnumArquivoConciliacaoOperadora.VALOR_VENDA.getFim()))/100);
				arquivoConciliacaoOperadora.setParcela(Integer.parseInt(linha.substring(EnumArquivoConciliacaoOperadora.PARCELA.getInicio(), EnumArquivoConciliacaoOperadora.PARCELA.getFim())));
				arquivoConciliacaoOperadora.setTotalparcela(Integer.parseInt(linha.substring(EnumArquivoConciliacaoOperadora.TOTAL_PARCELAS.getInicio(), EnumArquivoConciliacaoOperadora.TOTAL_PARCELAS.getFim())));
				arquivoConciliacaoOperadora.setCodigoautoricacao(linha.substring(EnumArquivoConciliacaoOperadora.CODIGO_AUTORIZACAO.getInicio(),EnumArquivoConciliacaoOperadora.CODIGO_AUTORIZACAO.getFim()));
				arquivoConciliacaoOperadora.setTid(linha.substring(EnumArquivoConciliacaoOperadora.TID.getInicio(),EnumArquivoConciliacaoOperadora.TID.getFim()));
				arquivoConciliacaoOperadora.setNsudoc(linha.substring(EnumArquivoConciliacaoOperadora.NSU_DOC.getInicio(),EnumArquivoConciliacaoOperadora.NSU_DOC.getFim()));
				arquivoConciliacaoOperadora.setMotivorejeicao(linha.substring(EnumArquivoConciliacaoOperadora.MOTIVO_REJEICAO.getInicio(),EnumArquivoConciliacaoOperadora.MOTIVO_REJEICAO.getFim()));
				arquivoConciliacaoOperadora.setTaxaparcela(taxaparcelaService.findTaxaParcelaByParcela(arquivoConciliacaoOperadora.getTotalparcela()));
				arquivoConciliacaoOperadora.setEmpresaestabelecimento(empresa);
				if (StringUtils.isNotBlank(arquivoConciliacaoOperadora.getTid())) {
					Documento documentoAssociacao = contareceberService.findForAssociacaoArquivoConciliacaoOperadora(arquivoConciliacaoOperadora.getTid());
					if (documentoAssociacao != null && documentoAssociacao.getCddocumento() != null) {
						arquivoConciliacaoOperadora.setDocumento(documentoAssociacao);
					}
				}
				arquivoconciliacaooperadoraService.saveOrUpdate(arquivoConciliacaoOperadora);
			} 
		}
	}
	
	public void processaArquivoPagamento(ArquivoConciliacaoOperadoraBean arquivo){
		String[] linhas = arquivo.getArquivoTexto().split("\\r?\\n");
		String linhaRegistro1 = "";
		Arquivoconciliacaooperadora arquivoConciliacaoOperadora = new Arquivoconciliacaooperadora();
		
		for (String linha : linhas) {
			arquivoConciliacaoOperadora = new Arquivoconciliacaooperadora();
			
			if (linha.startsWith("1")) {
				linhaRegistro1 = linha;
			} else if (linha.startsWith("2")) {
				arquivoConciliacaoOperadora.setNumeroro(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.NUMERO_RO.getInicio(),EnumArquivoConciliacaoOperadora.NUMERO_RO.getFim()));
				arquivoConciliacaoOperadora.setStatuspagamento(EnumArquivoConciliacaoOperadoraStatusPagamento.getEnumArquivoConciliacaoOperadoraStatusPagamento(Integer.parseInt(linhaRegistro1.substring(EnumArquivoConciliacaoOperadora.STATUS_PAGAMENTO.getInicio(),EnumArquivoConciliacaoOperadora.STATUS_PAGAMENTO.getFim()))));
				arquivoConciliacaoOperadora.setDatavenda(DateUtils.stringToDate(linha.substring(EnumArquivoConciliacaoOperadora.DATA_VENDA.getInicio(),EnumArquivoConciliacaoOperadora.DATA_VENDA.getFim()),"yyyyMMdd"));
				arquivoConciliacaoOperadora.setTid(linha.substring(EnumArquivoConciliacaoOperadora.TID.getInicio(),EnumArquivoConciliacaoOperadora.TID.getFim()));
				arquivoConciliacaoOperadora.setNsudoc(linha.substring(EnumArquivoConciliacaoOperadora.NSU_DOC.getInicio(),EnumArquivoConciliacaoOperadora.NSU_DOC.getFim()));
				this.atualizaStatusPagamento(arquivoConciliacaoOperadora);
			} 
		}
	}
	
	public void atualizaStatusPagamento(Arquivoconciliacaooperadora arquivoconciliacaooperadoraPagamento){
		Arquivoconciliacaooperadora arquivoconciliacaooperadoraVenda = arquivoconciliacaooperadoraService.findForAtulizaStatusPagamento(arquivoconciliacaooperadoraPagamento);
		if (arquivoconciliacaooperadoraVenda != null) {
			arquivoconciliacaooperadoraService.atualizaStatusPagamento(arquivoconciliacaooperadoraVenda, arquivoconciliacaooperadoraPagamento);
			if (arquivoconciliacaooperadoraVenda.getDocumento() != null && arquivoconciliacaooperadoraVenda.getDocumento().getCddocumento() != null) {
				arquivoconciliacaooperadoraService.conciliar(arquivoconciliacaooperadoraVenda.getCdarquivoconciliacaooperadora().toString());
			}
		}
	}
}
