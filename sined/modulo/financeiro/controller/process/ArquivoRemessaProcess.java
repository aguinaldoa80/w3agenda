package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessa240Bean;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaBean;
import br.com.linkcom.lkbanco.filesend.util.GerarArquivoRemessa;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.exception.NotInNeoContextException;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoarquivoremessa;
import br.com.linkcom.sined.geral.service.ArquivobancarioService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContacarteiraService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.TaxaitemService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.GerarArquivoRemessaBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.utils.StringUtils;

@Controller(path="/financeiro/process/Arquivoremessa",authorizationModule=ProcessAuthorizationModule.class)
public class ArquivoRemessaProcess extends MultiActionController{
	
	private DocumentoService documentoService;
	private ArquivobancarioService arquivobancarioService;
	private ContaService contaService;
	private TaxaitemService taxaitemService;
	private ClienteService clienteService;
	private EmpresaService empresaService;
	private ContacarteiraService contacarteiraService;

	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {
		this.arquivobancarioService = arquivobancarioService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setTaxaitemService(TaxaitemService taxaitemService) {
		this.taxaitemService = taxaitemService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setContacarteiraService(ContacarteiraService contacarteiraService) {
		this.contacarteiraService = contacarteiraService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception{
		List<Conta> listaConta = contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA);
		request.setAttribute("listaConta", listaConta);
		return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
	}
	
	/**
	 * M�todo para carregar listagem do filtro no jsp via ajax.
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findColaboradorAltera
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @author Jo�o Paulo Zica
	 */
	public ModelAndView listar(WebRequestContext request, GerarArquivoRemessaBean filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		List<Documento> listaDocumento = documentoService.findDadosForListagemRemessa(filtro);
		return new ModelAndView("direct:ajax/arquivoremessalistagem", "listaDocumento", listaDocumento);
	}
	
	/**
	 * Verifica os itens selecionados na listagem e direciona para a gera��o do arquivo de remessa.
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@Action("gerar")
	public ModelAndView gerar(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		String cds=request.getParameter("cds");
		filtro.setContas(cds);
		Conta conta = contaService.verificaBanco(filtro.getConta(), filtro.getContacarteira());
		
		if(conta.getBanco().getNumero() == 237){
			return gerarArquivoBradesco(request, filtro); // BRADESCO
		}else if(conta.getBanco().getNumero() == 356){
			return gerarArquivoBancoReal(request, filtro); // REAL
		}else if(conta.getBanco().getNumero() == 341){
			return gerarArquivoBancoItau(request, filtro); // ITAU	
		}else if(conta.getBanco().getNumero() == 1 && conta.getContacarteira().getTipoarquivoremessa() != null && conta.getContacarteira().getTipoarquivoremessa().equals(Tipoarquivoremessa.CNAB400)){
			return gerarArquivoBancoBrasil400(request, filtro); // BANCO DO BRASIL
		}else if(conta.getBanco().getNumero() == 1 && conta.getContacarteira().getTipoarquivoremessa() != null && conta.getContacarteira().getTipoarquivoremessa().equals(Tipoarquivoremessa.CNAB240)){
			return gerarArquivoBancoBrasil240(request, filtro); // BANCO DO BRASIL
		}else if(conta.getBanco().getNumero() == 1 && conta.getContacarteira().getTipoarquivoremessa() == null) {
			request.addError("Para a gera��o de remessa do Banco do Brasil � necess�rio preencher o padr�o do arquivo no cadastro da conta banc�ria.");
		}else if(conta.getBanco().getNumero() == 353 || conta.getBanco().getNumero() == 33) {
			return gerarArquivoBancoSantander400(request, filtro); // SANTANDER
		} else if(conta.getBanco().getNumero() == 399){
			return gerarArquivoRemessaHSBC(request, filtro); //HSBC
		}else if(conta.getBanco().getNumero() == 453){
			return gerarArquivoBancoRural400(request, filtro); //RURAL
		}else{
			request.addError("Gera��o de remessa n�o dispon�vel");
		}
		
		return new ModelAndView("redirect:/financeiro/process/Arquivoremessa");
	}
	
	/**
	 * Monta o arquivo bancario de acordo com as especifica��es do Bradesco.
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @throws Exception 
	 */
	private ModelAndView gerarArquivoBradesco(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);		
		
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());

		List<ArquivoRemessaBean> listaRegistros = arquivobancarioService.setBodyRemessa400(conta, listaDocumento);
		
		String seqString = listaRegistros.get(listaRegistros.size()-1).getSequenciaRegMensagem();
		if(seqString == null || seqString.equals("")){
			seqString = listaRegistros.get(listaRegistros.size()-1).getSequencialRegistroBody();
		}
		Integer sequencial = Integer.parseInt(seqString);
		
		ArquivoRemessaBean ht = arquivobancarioService.setHeaderTraillerRemessa400(conta, sequencial);
		arquivo = arquivoRemessa.gerarArquivoRemessaBradesco(ht, listaRegistros);
		
		// Cria o arquivo texto
		format = new SimpleDateFormat("ddMM");
		data = format.format(date);
		String nomeArq = "CB"+data+StringUtils.stringCheia((conta.getSequencial() != null ? conta.getSequencial().toString() : "0"), "0", 2, false)+".REM";
		//Utilizado somente em caso de testes
//		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial().toString(), "0", 2, false)+".TST";
		String sequencialArquivo = data+StringUtils.stringCheia((conta.getSequencial() != null ? conta.getSequencial().toString() : "0"), "0", 2, false);
		
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}	

	/**
	 * Gera arquivo cobran�a 400 registrada - Banco Real.
	 * @author Taidson
	 * @since 22/05/2010 
	 */
	private ModelAndView gerarArquivoBancoReal(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);		
		
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());
		
		conta.setBancoemiteboleto(filtro.getConta().getBancoemiteboleto());
		
		List<ArquivoRemessaBean> listaRegistros = arquivobancarioService.setBodyMensagemRemessa400BancoReal(conta, listaDocumento);
		
		Long vrTotal = 0L; 
		for (Documento item : listaDocumento) {
			vrTotal = vrTotal + item.getAux_documento().getValoratual().toLong();
		}
		ArquivoRemessaBean ht = arquivobancarioService.setHeaderTraillerRemessa400BancoReal(conta, listaDocumento.size(), vrTotal);
		arquivo = arquivoRemessa.gerarArquivoRemessaBancoReal(ht, listaRegistros);
		
		// Cria o arquivo texto
		format = new SimpleDateFormat("ddMM");
		data = format.format(date);
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}	
	
	/**
	 * Gera arquivo cobran�a 400 registrada - Banco ITAU.
	 * @author Tom�s Rabelo
	 */
	private ModelAndView gerarArquivoBancoItau(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);		
		
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());
		
		conta.setBancoemiteboleto(filtro.getConta().getBancoemiteboleto());
		
		List<ArquivoRemessaBean> listaRegistros;
		try {
			listaRegistros = arquivobancarioService.setBodyMensagemRemessa400BancoItau(conta, listaDocumento, filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addMessage(e.getMessage());
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		ArquivoRemessaBean ht = arquivobancarioService.setHeaderTraillerRemessa400BancoItau(conta, listaDocumento.size());
		arquivo = arquivoRemessa.gerarArquivoRemessaBancoItau(ht, listaRegistros);
		arquivo = arquivo.replaceAll("\t", " ");
		
		// Cria o arquivo texto
		format = new SimpleDateFormat("ddMM");
		data = format.format(date);
			
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
//		String nomeArq = "CB"+data+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}	
	
	/**
	 * Gera arquivo cobran�a 240 registrada - Banco do Brasil.
	 * @author Marden Silva
	 */
	private ModelAndView gerarArquivoBancoBrasil240(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();

		if(filtro.getContacarteira() != null){
			filtro.setContacarteira(contacarteiraService.load(filtro.getContacarteira()));
		}
		
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());
		
		if(conta.getSequencial() == null) conta.setSequencial(0);
		
		conta.setBancoemiteboleto(filtro.getConta().getBancoemiteboleto());
		
		List<ArquivoRemessa240Bean> listaHeader;
		try {
			listaHeader = arquivobancarioService.setHeaderRemessa240BancoBrasil(conta, filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addMessage(e.getMessage());
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}		
		
		List<ArquivoRemessa240Bean> listaBody;
		try {
			listaBody = arquivobancarioService.setBodyRemessa240BancoBrasil(conta, listaDocumento, filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addMessage(e.getMessage());
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
			
		List<ArquivoRemessa240Bean> listaTriller;
		try {
			listaTriller = arquivobancarioService.setTrillerRemessa240BancoBrasil(listaHeader.size()+listaBody.size());
		} catch (Exception e) {
			e.printStackTrace();
			request.addMessage(e.getMessage());
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}		
		
		arquivo = arquivoRemessa.gerarArquivoRemessaBancoBrasil240(listaHeader, listaBody, listaTriller);
		
		if(arquivo != null){
			arquivo = Util.strings.tiraAcento(arquivo);
		}
		
		// Cria o arquivo texto
		SimpleDateFormat format = new SimpleDateFormat("ddMM");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}	
	
	/**
	 * Gera arquivo cobran�a 400 - Banco Brasil.
	 * @author Marden Silva
	 */
	private ModelAndView gerarArquivoBancoBrasil400(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());
		
		conta.setBancoemiteboleto(filtro.getConta().getBancoemiteboleto());
		
		List<ArquivoRemessaBean> listaRegistros;
		try {
			listaRegistros = arquivobancarioService.setBodyRemessa400BancoBrasil(conta, listaDocumento, filtro);
			if(request.getMessages().length>0){

				//return new ModelAndView("/process/arquivoremessa");	
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addMessage(e.getMessage());
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}

		ArquivoRemessaBean ht = arquivobancarioService.setHeaderTraillerRemessa400BancoBrasil(conta, listaDocumento.size());
		arquivo = arquivoRemessa.gerarArquivoRemessaBancoBrasil400(ht, listaRegistros);
		
		// Cria o arquivo texto
		format = new SimpleDateFormat("ddMM");
		data = format.format(date);	
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}
	
	/**
	 * Gera arquivo cobran�a 400 registrada - Banco Santander.
	 * @author Marden Silva	
	 * @since 15/07/2011 
	 */
	@SuppressWarnings("unused")
	private ModelAndView gerarArquivoBancoSantander(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);		
		
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());
		
		conta.setBancoemiteboleto(filtro.getConta().getBancoemiteboleto());
		
		List<ArquivoRemessaBean> listaRegistros = arquivobancarioService.setBodyRemessa400BancoSantander(conta, listaDocumento);
		
		Long vrTotal = 0L; 
		for (Documento item : listaDocumento) {
			vrTotal = vrTotal + item.getValor().toLong();
		}
		ArquivoRemessaBean ht = arquivobancarioService.setHeaderTraillerRemessa400BancoSantander(conta, listaDocumento.size(), vrTotal);
		arquivo = arquivoRemessa.gerarArquivoRemessaBancoSantander(ht, listaRegistros);
		
		// Cria o arquivo texto
		format = new SimpleDateFormat("ddMM");
		data = format.format(date);
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}		
	
	@SuppressWarnings("unchecked")
	public ModelAndView gerarArquivoRemessaHSBC(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception{
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);		
		
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}

		for (Documento documento : listaDocumento) {
			Cliente cliente = new Cliente();
			cliente.setCdpessoa(documento.getPessoa().getCdpessoa());
			documento.setCliente(clienteService.carregarDadosCliente(cliente));
			if (documento.getTaxa() != null){
				List<Taxaitem> listaTaxaItem = taxaitemService.findTaxasItens(documento.getTaxa());
				documento.getTaxa().setListaTaxaitem(SinedUtil.listToSet(listaTaxaItem, Taxaitem.class));
			}
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());
		
		conta.setBancoemiteboleto(filtro.getConta().getBancoemiteboleto());
		
		List<ArquivoRemessaBean> listaRegistros = arquivobancarioService.setBodyRemessa400BancoHSBC(conta, listaDocumento);
		
		Long vrTotal = 0L; 
		for (Documento item : listaDocumento) {
			vrTotal = vrTotal + item.getValor().toLong();
		}
		ArquivoRemessaBean ar = listaRegistros.get(listaRegistros.size()-1);
		Integer sequencial = Integer.parseInt(ar.getNumSequencialRegistro());
		sequencial++;
		
		ArquivoRemessaBean ht = arquivobancarioService.setHeaderTraillerRemessa400BancoHSBC(conta, sequencial, vrTotal);
		
		arquivo = arquivoRemessa.gerarArquivoRemessaBancoHSBC(ht, listaRegistros);
		
		// Cria o arquivo texto
		format = new SimpleDateFormat("ddMM");
		data = format.format(date);
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}
	
	private List<Documento> getListaDocumento(GerarArquivoRemessaBean filtro) throws NotInNeoContextException, Exception {
		List<Documento> listaDocumento = documentoService.findDadosArquivoRemessa(filtro);
		
		if (listaDocumento.size() < 1) {
			NeoWeb.getRequestContext().addMessage("N�o existem dados suficientes para gerar um arquivo de remessa.");
			throw new Exception();
		}
		Contacarteira contacarteira = filtro.getContacarteira();
		if(contacarteira.getLimiteposicoesintervalonossonumero() == null){
			contacarteira = contacarteiraService.load(filtro.getContacarteira());
		}

		
		HashMap<MessageType, String> mapaValidacaoSequenciais = documentoService.validaLimiteSequencialNossoNum(listaDocumento, contacarteira);
		if(mapaValidacaoSequenciais.containsKey(MessageType.ERROR)){
			NeoWeb.getRequestContext().addError(mapaValidacaoSequenciais.get(MessageType.ERROR));
			throw new Exception();			
		}
		
		if(SinedUtil.isListNotEmpty(listaDocumento)){
			for(Documento documento : listaDocumento){
				if(filtro.getConta() != null && !filtro.getConta().equals(documento.getConta())){
					NeoWeb.getRequestContext().addError("N�o foi poss�vel gerar o arquivo de remessa. A listagem de documento � diferente do filtro. Favor gerar novamente.");
					throw new Exception();
				}
				if(filtro.getContacarteira() != null && filtro.getContacarteira().getCdcontacarteira() != null && documento.getContacarteira() != null && 
						!filtro.getContacarteira().getCdcontacarteira().equals(documento.getContacarteira().getCdcontacarteira())){
					NeoWeb.getRequestContext().addError("N�o foi poss�vel gerar o arquivo de remessa. A listagem de documento � diferente do filtro. Favor gerar novamente.");
					throw new Exception();
				}
			}
		}
			
		return listaDocumento;
	}
	
	
	/**
	 * Gera arquivo cobran�a 400 registrada - Banco Santander.
	 * @author Marden Silva	
	 * @since 15/07/2011 
	 */
	private ModelAndView gerarArquivoBancoSantander400(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);		
		
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());
		
		conta.setBancoemiteboleto(filtro.getConta().getBancoemiteboleto());
		
//		List<ArquivoRemessaBean> listaRegistros = arquivobancarioService.setBodyRemessa400BancoSantander(conta, listaDocumento);
		List<ArquivoRemessaBean> listaRegistros = arquivobancarioService.setBodyNewRemessa400BancoSantander(conta, listaDocumento);
		
		Money vrTotal = new Money(); 
		for (Documento item : listaDocumento) {
			//vrTotal = vrTotal.add(item.getAux_documento().getValoratual());
			vrTotal = vrTotal.add(item.getValor());
		}
		ArquivoRemessaBean ar = listaRegistros.get(listaRegistros.size()-1);
		Integer sequencial = Integer.parseInt(ar.getSequencialRegistroBody());
		sequencial++;
//		ArquivoRemessaBean ht = arquivobancarioService.setHeaderTraillerRemessa400BancoSantander(conta, listaDocumento.size(), vrTotal);
		ArquivoRemessaBean ht = arquivobancarioService.setNewHeaderTraillerRemessa400BancoSantander(conta, listaRegistros.size(), sequencial, vrTotal.toString());
		
//		arquivo = arquivoRemessa.gerarArquivoRemessaBancoSantander(ht, listaRegistros);
		arquivo = arquivoRemessa.gerarNewArquivoRemessaBancoSantander(ht, listaRegistros);
		
		// Cria o arquivo texto
		format = new SimpleDateFormat("ddMM");
		data = format.format(date);
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}
	
	/**
	 * Gera arquivo cobran�a 400 registrada - Banco Rural.
	 * @author Thiers Euller	
	 * @since 03/05/2012 
	 */
	private ModelAndView gerarArquivoBancoRural400(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		
		String arquivo = new String();
		GerarArquivoRemessa arquivoRemessa = new GerarArquivoRemessa();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);		
		
		List<Documento> listaDocumento;
		try {
			listaDocumento = getListaDocumento(filtro);
		} catch (Exception e) {
			return new ModelAndView("/process/arquivoremessa").addObject("filtro", filtro);
		}
		
		Conta conta = contaService.carregaContaForRemessa(filtro.getConta(), filtro.getContacarteira(), filtro.getEmpresa());
		
		conta.setBancoemiteboleto(filtro.getConta().getBancoemiteboleto());
		
		List<ArquivoRemessaBean> listaRegistros = arquivobancarioService.setBodyNewRemessa400BancoRural(conta, listaDocumento);
		ArquivoRemessaBean ar = listaRegistros.get(listaRegistros.size()-1);
		Integer sequencial = 0;
		if(ar.getSequencialMensagem() != ""){
			sequencial = Integer.parseInt(ar.getSequencialMensagem());
		}else{
			sequencial = Integer.parseInt(ar.getSequencialRegistroBody());
		}
		sequencial++;
		
		ArquivoRemessaBean ht = arquivobancarioService.setNewHeaderTraillerRemessa400BancoRural(conta, sequencial);
		
		arquivo = arquivoRemessa.gerarNewArquivoRemessaBancoRural(ht, listaRegistros);
		
		// Cria o arquivo texto
		format = new SimpleDateFormat("ddMM");
		data = format.format(date);
		String nomeArq = "CB"+data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false)+".REM";
		String sequencialArquivo = data+StringUtils.stringCheia(conta.getSequencial() != null ? conta.getSequencial().toString() : "0", "0", 2, false);
		Resource resource = new Resource("text/plain",nomeArq,arquivo.getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
 		
		documentoService.saveAndUpdateArquivoRemessa(resource, listaDocumento, nomeArq, sequencialArquivo);
		contaService.setProximoSequencial(conta);
		documentoService.gerarHistoricoArquivoRemessa(listaDocumento);
		
		return resourceModelAndView;
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView ajaxCarregaempresa(WebRequestContext request, Conta conta) {
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		if (conta != null && conta.getCdconta() != null) {
			conta = contaService.loadContaForRemessa(conta);
			if(conta != null && conta.getListaContaempresa() != null && conta.getListaContaempresa().size() > 0){
				listaEmpresa = (List<Empresa>) CollectionsUtil.getListProperty(conta.getListaContaempresa(), "empresa");
			} else listaEmpresa.addAll(empresaService.findByUsuario());
		} else listaEmpresa.addAll(empresaService.findByUsuario());
		
		return new JsonModelAndView()
					.addObject("lista", listaEmpresa)
					.addObject("tamanho", listaEmpresa.size());
	}

	public ModelAndView ajaxValidaLimiteNossoNumero(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		String cds=request.getParameter("cds");
		filtro.setContas(cds);
		List<Documento> listaDocumento = documentoService.findDadosArquivoRemessa(filtro);
		
		if (listaDocumento.size() < 1) {
			NeoWeb.getRequestContext().addMessage("N�o existem dados suficientes para gerar um arquivo de remessa.");
			throw new Exception();
		}
		Contacarteira contacarteira = filtro.getContacarteira();
		if(contacarteira.getLimiteposicoesintervalonossonumero() == null){
			contacarteira = contacarteiraService.load(filtro.getContacarteira());
		}

		HashMap<MessageType, String> alertasNossoNum = documentoService.validaLimiteSequencialNossoNum(listaDocumento, contacarteira);
		if(alertasNossoNum.containsKey(MessageType.WARN)){
			return new JsonModelAndView().addObject("alertanossonumero", alertasNossoNum.get(MessageType.WARN));
		}
		return new JsonModelAndView().addObject("alertanossonumero", "");
	}
}
