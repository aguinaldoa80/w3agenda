package br.com.linkcom.sined.modulo.financeiro.controller.process;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ConciliacaoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/financeiro/process/Arquivoconciliacao",authorizationModule=ProcessAuthorizationModule.class)
public class ArquivoConciliacaoProcess extends MultiActionController{
	
	public static final String CONTA_SESSION = "CONTA_SESSION_CONCILIACAO";
	private ContaService contaService;
	private MovimentacaoService movimentacaoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, ArquivoConciliacaoBean bean) throws Exception{
		return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
	}
	
	/**
	 * M�todo que faz o processamento do arquivo de concilia��o banc�ria.
	 * 
	 * OBS: N�o � poss�vel a quebra desse m�todo em mais partes pois com o andamento do processmanto vai fazendo
	 * a verifica��o do arquivo retornando o erro para a tela do usu�rio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#findAllContas
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#comparacaoContas
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#processaDatabanco
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#processaValor
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#separaCheque
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#comparacaoSeparacao
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public ModelAndView processar(WebRequestContext request, ArquivoConciliacaoBean bean) throws Exception{
		
		if (bean == null || bean.getArquivo() == null) {
			request.addError("Arquivo inv�lido.");
			return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
		}
		
		
		String string = new String(bean.getArquivo().getContent());
		
		String[] linhas = string.split("\n");
		bean.setLinhas(linhas);
		
		boolean findId = false; 
		String bankid = null;
		String actid = null;
		List<String> listBankid = new ArrayList<String>();
		List<String> listActid = new ArrayList<String>();
		
		for (String linha : linhas) {
			linha = linha.trim();
			
			if (linha.contains("<BANKID>")) {
				if (linha.contains("</BANKID>")) {
					bankid = linha.substring(linha.indexOf(">")+1, linha.indexOf("</"));
				} else {
					bankid = linha.substring(linha.indexOf(">")+1, linha.length());
				}
				if(!listBankid.contains(bankid)){
					listBankid.add(bankid);
				}
				
			}
			if (linha.contains("<ACCTID>")) {
				if (linha.contains("</ACCTID>")) {
					actid = linha.substring(linha.indexOf(">")+1, linha.indexOf("</"));
				} else {
					actid = linha.substring(linha.indexOf(">")+1, linha.length());
				}
				if(!listActid.contains(actid)){
					listActid.add(actid);
					findId = true;
				}
			}
			
			if (listBankid.size() > 1 || listActid.size() > 1) {
				request.addError("Arquivo inv�lido.");
				return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
			}
		}
		
		if (!findId) {
			request.addError("Arquivo inv�lido.");
			return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
		}
		
		Integer codigobanco = null;
		try{
			codigobanco = Integer.parseInt(bankid);
		} catch (NumberFormatException e) {
			request.addError("C�digo do banco no arquivo inv�lido.");
			return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
		}
		
		List<Conta> listaConta = contaService.findAllContas(codigobanco);
		
		List<Conta> contas = null;
		if (listaConta != null) {
			contas = movimentacaoService.comparacaoContas(actid, listaConta);
		}
		
		request.getSession().setAttribute(CONTA_SESSION, bean);
		
		if (contas == null || contas.isEmpty() || contas.size() > 1) {
			request.setAttribute("lista", contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA));
			return new ModelAndView("process/selecionarConta");
		}		
		
		return continueOnAction("processarMovimentacao", contas.get(0));

	}
	
	public ModelAndView processarMovimentacao(WebRequestContext request, Conta conta) throws ParseException{
		
		if (conta == null || conta.getCdconta() == null) {
			throw new SinedException("Conta n�o pode ser nulo.");
		}
		
		ArquivoConciliacaoBean bean = (ArquivoConciliacaoBean)request.getSession().getAttribute(CONTA_SESSION);
		bean.setConta(contaService.loadConta(conta));
		String[] linhas = bean.getLinhas();
		
		List<ConciliacaoBean> listaConciliacao = new ArrayList<ConciliacaoBean>();
		ConciliacaoBean conciliacaoBean = null;
		String trntype = null;
		String dtposted = null;
		String tramt = null;
		String fitid = null;
		String checknum = null;
		String memo = null;
//		String dtserver = null;
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd"); 
		boolean achouTipoOperaco;
		for (int i = 0; i < linhas.length; i++) {
			
//			if (linhas[i].contains("<DTSERVER>")) {
//				dtserver = linhas[i].trim();
//			}
			
			if (linhas[i].contains("<STMTTRN>")) {
				
				for (int j = i; j < linhas.length; j++) {
					if(linhas[j].contains("<TRNTYPE>")) trntype = linhas[j].trim();
					if(linhas[j].contains("<DTPOSTED>")) dtposted = linhas[j].trim();
					if(linhas[j].contains("<TRNAMT>")) tramt = linhas[j].trim();
					if(linhas[j].contains("<FITID>")) fitid = linhas[j].trim();
					if(linhas[j].contains("<CHECKNUM>")){
						checknum = linhas[j].trim(); 
					}else if(linhas[j].contains("<REFNUM>")){
						checknum = linhas[j].trim(); 
					} 
					if(linhas[j].contains("<MEMO>")) memo = linhas[j].trim();
					
					if(linhas[j].contains("</STMTTRN>")) break;
				}
				
				
				conciliacaoBean = new ConciliacaoBean();
//				trntype = linhas[i+1].trim();
//				dtposted = linhas[i+2].trim();
//				tramt = linhas[i+3].trim();
//				fitid = linhas[i+4].trim();
//				checknum = linhas[i+5].trim();
//				memo = linhas[i+6].trim();
				achouTipoOperaco = false;
				
				if(!trntype.contains("<TRNTYPE>")){
					request.addError("Arquivo inv�lido.");
					return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
				} else {
					if (trntype.contains("</TRNTYPE>")) {
						if (trntype.substring(trntype.indexOf(">")+1, trntype.indexOf("</")).equals("DEBIT")) {
							conciliacaoBean.setTipooperacao(Tipooperacao.TIPO_DEBITO);
							achouTipoOperaco = true;
						} else if (trntype.substring(trntype.indexOf(">")+1, trntype.indexOf("</")).equals("CREDIT")) {
							conciliacaoBean.setTipooperacao(Tipooperacao.TIPO_CREDITO);
							achouTipoOperaco = true;
//						} else {
//							request.addError("Tipo de opera��o no arquivo inv�lido.");
//							return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
						}
					} else {
						if (trntype.substring(trntype.indexOf(">")+1, trntype.length()).equals("DEBIT")) {
							conciliacaoBean.setTipooperacao(Tipooperacao.TIPO_DEBITO);
							achouTipoOperaco = true;
						} else if (trntype.substring(trntype.indexOf(">")+1, trntype.length()).equals("CREDIT")) {
							conciliacaoBean.setTipooperacao(Tipooperacao.TIPO_CREDITO);
							achouTipoOperaco = true;
//						} else {
//							request.addError("Tipo de opera��o no arquivo inv�lido.");
//							return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
						}
					}
					
					
				}
				
				if (!dtposted.contains("<DTPOSTED>")) {
					request.addError("Arquivo inv�lido.");
					return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
				} else {  
					movimentacaoService.processaDatabanco(conciliacaoBean, dtposted, formatter);
				}
				
				if (!tramt.contains("<TRNAMT>")) {
					request.addError("Arquivo inv�lido.");
					return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
				} else {
					movimentacaoService.processaValor(conciliacaoBean, tramt, achouTipoOperaco);
				}
				
				if (!fitid.contains("<FITID>")) {
					request.addError("Arquivo inv�lido.");
					return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
				} else {
					if (fitid.contains("</FITID>")) {
						conciliacaoBean.setFitid(fitid.substring(fitid.indexOf(">")+1, fitid.indexOf("</")));
					} else {
						conciliacaoBean.setFitid(fitid.substring(fitid.indexOf(">")+1, fitid.length()));
					}
				}
				
				if (!checknum.contains("<CHECKNUM>") && !checknum.contains("<REFNUM>")) {
					request.addError("Arquivo inv�lido.");
					return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
				} else {
					if(checknum.contains("<CHECKNUM>")){
						if (checknum.contains("</CHECKNUM>")) {
							conciliacaoBean.setChecknum(checknum.substring(checknum.indexOf(">")+1, checknum.indexOf("</")));
						} else {
							conciliacaoBean.setChecknum(checknum.substring(checknum.indexOf(">")+1, checknum.length()));
						}
					}else if(checknum.contains("<REFNUM>")){
						if (checknum.contains("</REFNUM>")) {
							conciliacaoBean.setChecknum(checknum.substring(checknum.indexOf(">")+1, checknum.indexOf("</")));
						} else {
							conciliacaoBean.setChecknum(checknum.substring(checknum.indexOf(">")+1, checknum.length()));
						}
					} 
				}
				
				if (!memo.contains("<MEMO>")) {
//					memo = linhas[i+7].trim();
//					if (!memo.contains("<MEMO>")) {
						request.addError("Arquivo inv�lido.");
						return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
//					} else {
//						if (memo.contains("</MEMO>")) {
//							conciliacaoBean.setMemo(memo.substring(memo.indexOf(">")+1, memo.indexOf("</")));
//						} else {
//							conciliacaoBean.setMemo(memo.substring(memo.indexOf(">")+1, memo.length()));
//						}
//					}
				} else {
					if (memo.contains("</MEMO>")) {
						conciliacaoBean.setMemo(memo.substring(memo.indexOf(">")+1, memo.indexOf("</")));
					} else {
						conciliacaoBean.setMemo(memo.substring(memo.indexOf(">")+1, memo.length()));
					}
				}
				
//				if(!dtserver.contains("<DTSERVER>")){
//					request.addError("Arquivo inv�lido.");
//					return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
//				} else {
//					movimentacaoService.processaDataServer(conciliacaoBean, dtserver, formatter);
//				}
				movimentacaoService.processaDataServer(conciliacaoBean);

				listaConciliacao.add(conciliacaoBean);				
			}
		}
		
		if (listaConciliacao == null || listaConciliacao.isEmpty()) {
			request.addError("Nenhuma transa��o encontrada.");
			return new ModelAndView("/process/arquivoconciliacao","filtro",bean);
		}
		
		movimentacaoService.comparacaoSeparacao(bean, listaConciliacao);
		
		List<ConciliacaoBean> lista = bean.getListaEncontrada();
		List<Movimentacao> listaMovimentacaoSessao = new ArrayList<Movimentacao>();
		for (ConciliacaoBean conci : lista) {
			if (conci.getMovimentacao() != null && conci.getMovimentacao().getCdmovimentacao() != null) {
				conci.getMovimentacao().setStringMovimentacao(conci.getMovimentacao().getCdmovimentacao().toString());
				listaMovimentacaoSessao.add(conci.getMovimentacao());
			}			
		}
		if(SinedUtil.isListNotEmpty(bean.getListaCreditoEncontrado())){
			for (ConciliacaoBean conci : bean.getListaCreditoEncontrado()) {
				if (conci.getMovimentacao() != null && conci.getMovimentacao().getCdmovimentacao() != null) {
					conci.getMovimentacao().setStringMovimentacao(conci.getMovimentacao().getCdmovimentacao().toString());
					listaMovimentacaoSessao.add(conci.getMovimentacao());
				}			
			}
		}
		if(SinedUtil.isListNotEmpty(bean.getListaDebitoEncontrado())){
			for (ConciliacaoBean conci : bean.getListaDebitoEncontrado()) {
				if (conci.getMovimentacao() != null && conci.getMovimentacao().getCdmovimentacao() != null) {
					conci.getMovimentacao().setStringMovimentacao(conci.getMovimentacao().getCdmovimentacao().toString());
					listaMovimentacaoSessao.add(conci.getMovimentacao());
				}			
			}
		}
		
		request.getSession().setAttribute("listaMovimentacao", listaMovimentacaoSessao);
		request.getSession().setAttribute(CONTA_SESSION, bean);
		
		boolean haveFechamentoFinanceiro = false;
		List<ConciliacaoBean> listaCredito = bean.getListaCredito();
		for (ConciliacaoBean conciBean : listaCredito) {
			if(conciBean.getFechamentofinanceiro() != null && conciBean.getFechamentofinanceiro()){
				haveFechamentoFinanceiro = true;
				break;
			}
		}
		
		if(!haveFechamentoFinanceiro){
			List<ConciliacaoBean> listaDebito = bean.getListaDebito();
			for (ConciliacaoBean conciBean : listaDebito) {
				if(conciBean.getFechamentofinanceiro() != null && conciBean.getFechamentofinanceiro()){
					haveFechamentoFinanceiro = true;
					break;
				}
			}
		}
		
		if(haveFechamentoFinanceiro){
			request.addMessage("Existem movimentos com data menor que a data de fechamento que n�o podem ser conciliados.", MessageType.WARN);
		}
		
		return new ModelAndView("process/conciliacaobancaria","bean",bean);
	}
		
	
	
	/**
	 * Carrega a pop-up de selecionar a movimenta��o a ser associada a uma transa��o do arquivo.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#findByContaTipoOperacao
	 * @param request
	 * @param conta
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public ModelAndView carregaSelecionar(WebRequestContext request, Conta conta) throws Exception{
		if (request.getParameter("tipoop") == null) {
			throw new SinedException("Tipo opera��o n�o pode ser nula.");
		}
		if (conta == null || conta.getCdconta() == null) {
			throw new SinedException("Conta n�o pode ser nula.");
		}
		
		List<Movimentacao> lista = (List<Movimentacao>)request.getSession().getAttribute("listaMovimentacao");
		String whereNotIn = null;
		if (lista != null) {
			whereNotIn = CollectionsUtil.listAndConcatenate(lista, "cdmovimentacao", ",");
		}
		
		List<Movimentacao> listaMovimentacao = null; 
		if ("debito".equals(request.getParameter("tipoop"))) {
			listaMovimentacao = movimentacaoService.findByContaTipoOperacao(conta,Tipooperacao.TIPO_DEBITO,whereNotIn);
		} else {
			listaMovimentacao = movimentacaoService.findByContaTipoOperacao(conta,Tipooperacao.TIPO_CREDITO,whereNotIn);
		}
		String valor = request.getParameter("valor");
		request.setAttribute("valor", valor);
		request.setAttribute("lista", listaMovimentacao);
		
		return new ModelAndView("direct:/process/popup/selecionarMovimentacao");
	}
	
	
	/**
	 * Action que salva as listas encontradadas e n�o encontradas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#doConciliacaoAutomatica(ArquivoConciliacaoBean, String)
	 * @param request
	 * @param bean
	 * @return
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public ModelAndView saveConciliar(WebRequestContext request,ArquivoConciliacaoBean bean){
		if(bean.getListaCreditoEncontrado()!=null && !bean.getListaCreditoEncontrado().isEmpty())
			bean.getListaEncontrada().addAll(bean.getListaCreditoEncontrado());
		if(bean.getListaDebitoEncontrado()!=null && !bean.getListaDebitoEncontrado().isEmpty())
			bean.getListaEncontrada().addAll(bean.getListaDebitoEncontrado());
		
		Boolean sucesso = movimentacaoService.doConciliacaoAutomatica(bean, "Concilia��o autom�tica via OFX");
		if(sucesso != null && sucesso){
			request.addMessage("Movimenta��es conciliadas com sucesso.");
		} else {
			request.addMessage("Nenhuma movimenta��o conciliada.", MessageType.WARN);
		}
		
		request.getSession().setAttribute("listaMovimentacao", null);
		return new ModelAndView("redirect:/financeiro/process/Arquivoconciliacao");
	}
	
	/**
	 * Ajax para adicionar a movimenta��o no escopo de sess�o.
	 * 
	 * @param request
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void ajaxMovimentacao(WebRequestContext request, Movimentacao movimentacao){
		String cdMovimentacao = request.getParameter("listaMovimentacoes");
		String [] listasStrings = cdMovimentacao.split(",");
		List<Movimentacao> lista = (List<Movimentacao>) request.getSession().getAttribute("listaMovimentacao");		
		if (lista == null && listasStrings != null && listasStrings.length > 0)
			lista = new ArrayList<Movimentacao>();		
		Movimentacao bean;
		for (String string : listasStrings) {
			bean = new Movimentacao();
			bean.setCdmovimentacao(Integer.parseInt(string));
			bean = movimentacaoService.load(bean);
			lista.add(bean);
		}
		if (lista != null) {
			request.getSession().setAttribute("listaMovimentacao", lista);
		} else {
			lista = new ArrayList<Movimentacao>();
			lista.add(movimentacao);
			request.getSession().setAttribute("listaMovimentacao", lista);
		}
	}
	
	
	/**
	 * Ajax para adicionar a movimenta��o no escopo de sess�o.
	 * 
	 * @param request
	 * @param movimentacao
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void ajaxMovimentacaoRemove(WebRequestContext request, Movimentacao movimentacao){
		String cdMovimentacao = request.getParameter("listaMovimentacoes");
		String [] listasStrings = cdMovimentacao.split(",");
		List<Movimentacao> lista = (List<Movimentacao>)request.getSession().getAttribute("listaMovimentacao");
		if (lista != null) {
			for (int i = 0; i < lista.size(); i++) {
				for (String string : listasStrings) {
					if(lista.get(i).getCdmovimentacao() == Integer.parseInt(string)){
						lista.remove(i);
					}
				}
				
			}
			request.getSession().setAttribute("listaMovimentacao", lista);
		}
	}
}
