package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaFormaPagamento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaInstrucao;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaTipoPagamento;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamento;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContaPagar;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRemessaService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaPagamentoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/financeiro/process/ArquivoConfiguravelRemessaPagamento",authorizationModule=ProcessAuthorizationModule.class)
public class ArquivoConfiguravelRemessaPagamentoProcess extends MultiActionController{
	
	private DocumentoService documentoService;	
	private DocumentotipoService documentotipoService;
	private ContaService contaService;
	private BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService;
	private CentrocustoService centroCustoService;
	private ProjetoService projetoService;

	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setBancoConfiguracaoRemessaService(BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService) {this.bancoConfiguracaoRemessaService = bancoConfiguracaoRemessaService;}
	public void setCentroCustoService(CentrocustoService centroCustoService) {this.centroCustoService = centroCustoService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ArquivoConfiguravelRemessaPagamentoBean filtro) throws Exception{
		request.setAttribute("listaDocumentotipo", documentotipoService.findAll());	
		filtro.setBancoConfiguracaoRemessa(null);
		
		List<Tipopagamento> listaTipopagamento =  new ArrayList<Tipopagamento>();
		listaTipopagamento.add(Tipopagamento.CLIENTE);
		listaTipopagamento.add(Tipopagamento.COLABORADOR);
		listaTipopagamento.add(Tipopagamento.FORNECEDOR);
		request.setAttribute("listaTipopagamento", listaTipopagamento);
		
		if (filtro.getConta() != null){
			filtro.setConta(contaService.carregaConta(filtro.getConta()));
			BancoConfiguracaoRemessa bcr = bancoConfiguracaoRemessaService.findByBancoAndTipo(filtro.getConta().getBanco(), BancoConfiguracaoRemessaTipoEnum.PAGAMENTO);
			
			List<Documentoacao> listadocumentacao = new ArrayList<Documentoacao>();
			if (bcr != null){
				bcr = bancoConfiguracaoRemessaService.loadForEntrada(bcr);
				filtro.setBancoConfiguracaoRemessa(bcr);
				List<BancoTipoPagamento> listaTipoPagamento = new ArrayList<BancoTipoPagamento>();
				List<BancoFormapagamento> listaFormaPagamento = new ArrayList<BancoFormapagamento>();
				
				for (BancoConfiguracaoRemessaTipoPagamento tp : bcr.getListaBancoConfiguracaoRemessaTipoPagamento())
					listaTipoPagamento.add(tp.getBancoTipoPagamento());
					
				for (BancoConfiguracaoRemessaFormaPagamento fp : bcr.getListaBancoConfiguracaoRemessaFormaPagamento())
					listaFormaPagamento.add(fp.getBancoFormapagamento());
				
				
				Collections.sort(listaTipoPagamento,new Comparator<BancoTipoPagamento>(){
					public int compare(BancoTipoPagamento o1, BancoTipoPagamento o2) {
						return o1.getDescricao().compareTo(o2.getDescricao());
					}
				});
				
				Collections.sort(listaFormaPagamento,new Comparator<BancoFormapagamento>(){
					public int compare(BancoFormapagamento o1, BancoFormapagamento o2) {
						return o1.getDescricao().compareTo(o2.getDescricao());
					}
				});
				
				Collections.sort(bcr.getListaBancoConfiguracaoRemessaInstrucao(),new Comparator<BancoConfiguracaoRemessaInstrucao>(){
					public int compare(BancoConfiguracaoRemessaInstrucao o1, BancoConfiguracaoRemessaInstrucao o2) {
						return o1.getNome().compareTo(o2.getNome());
					}
				});
					bcr = bancoConfiguracaoRemessaService.loadWithBancoConfiguracaoRemessaDocumentoacao(bcr);
						listadocumentacao = bancoConfiguracaoRemessaService.getListDocumentoacao(bcr);
						if(!SinedUtil.isListNotEmpty(listadocumentacao)){
							listadocumentacao.add(Documentoacao.PREVISTA);
							listadocumentacao.add(Documentoacao.DEFINITIVA);
							listadocumentacao.add(Documentoacao.AUTORIZADA);
							listadocumentacao.add(Documentoacao.AUTORIZADA_PARCIAL);
						}
	
				/*List<Tipopagamento> listaTipopagamento =  new ArrayList<Tipopagamento>();
				listaTipopagamento.add(Tipopagamento.CLIENTE);
				listaTipopagamento.add(Tipopagamento.COLABORADOR);
				listaTipopagamento.add(Tipopagamento.FORNECEDOR);*/
				
				request.setAttribute("listaInstrucaoPagamento", bcr.getListaBancoConfiguracaoRemessaInstrucao());
				request.setAttribute("listaTipoPagamento", listaTipoPagamento);
				request.setAttribute("listaFormaPagamento", listaFormaPagamento);
				request.setAttribute("listaCentrocusto", centroCustoService.findAtivos());
				request.setAttribute("listaProjeto", projetoService.findAll());
				request.setAttribute("listaSituacao", listadocumentacao);				
				//request.setAttribute("listaTipopagamento", listaTipopagamento);
				
				if (bcr.getUtilizaTabelaAuxiliar1() != null && bcr.getUtilizaTabelaAuxiliar1().booleanValue()){
					request.setAttribute("nomeAuxiliar1", bcr.getNomeTabelaAuxiliar1());
					request.setAttribute("listaAuxiliar1", bcr.getListaBancoConfiguracaoRemessaAuxiliar1());					
				}
				
				if (bcr.getUtilizaTabelaAuxiliar2() != null && bcr.getUtilizaTabelaAuxiliar2().booleanValue()){
					request.setAttribute("nomeAuxiliar2", bcr.getNomeTabelaAuxiliar2());
					request.setAttribute("listaAuxiliar2", bcr.getListaBancoConfiguracaoRemessaAuxiliar2());
				}
			}
		}	
		
		return new ModelAndView("/process/arquivoConfiguravelRemessaPagamento").addObject("filtro", filtro);
	}
	
	/**
	 * M�todo para carregar listagem do filtro no jsp via ajax.
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findColaboradorAltera
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public ModelAndView listar(WebRequestContext request, ArquivoConfiguravelRemessaPagamentoBean filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		List<Documento> listaDocumento = new ArrayList<Documento>();
		if((filtro.getDtinicioEmissao() != null && filtro.getDtfimEmissao() != null) || 
				(filtro.getDtinicioVencimento() != null && filtro.getDtfimVencimento() != null)){
			filtro.setConta(ContaService.getInstance().carregaConta(filtro.getConta()));
			
			BancoConfiguracaoRemessa bcr = bancoConfiguracaoRemessaService.findByBancoAndTipo(filtro.getConta().getBanco(), BancoConfiguracaoRemessaTipoEnum.PAGAMENTO);
			if (bcr != null){
				bcr = bancoConfiguracaoRemessaService.loadWithBancoConfiguracaoRemessaDocumentoacao(bcr);
				if(bcr != null){
					List<Documentoacao> listaDocumentoacao = bancoConfiguracaoRemessaService.getListDocumentoacao(bcr);
					if(SinedUtil.isListNotEmpty(listaDocumentoacao)&& filtro.getListaDocumentoacao()==null){
						filtro.setListaDocumentoacao(listaDocumentoacao);
					}
				}
			}
			listaDocumento = documentoService.findDadosForListagemRemessaPagamentoConfiguravel(filtro);
		}
		return new ModelAndView("direct:ajax/arquivoconfiguravelremessapagamentolistagem", "listaDocumento", listaDocumento);
	}
	
	/**
	 * Verifica os itens selecionados na listagem e direciona para a gera��o do arquivo de remessa.
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@Action("gerar")
	public ModelAndView gerar(WebRequestContext request, ArquivoConfiguravelRemessaPagamentoBean filtro) throws Exception {		
		String cds = request.getParameter("cds");
		filtro.setCds(cds);
		try {
			return bancoConfiguracaoRemessaService.gerarArquivoRemessaPagamento(filtro);
		} catch (SinedException e) {
			request.addMessage(e.getMessage(), MessageType.ERROR);
			return index(request, filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addMessage("Erro desconhecido: " + e.getMessage(), MessageType.ERROR);
			return index(request, filtro);
		}	
	}
	
}
