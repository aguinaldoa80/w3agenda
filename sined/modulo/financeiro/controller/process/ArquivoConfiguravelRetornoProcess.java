package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariodocumento;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Arquivoconfiguravelretornodocumento;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoAcaoEnum;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivobancarioService;
import br.com.linkcom.sined.geral.service.ArquivobancariodocumentoService;
import br.com.linkcom.sined.geral.service.ArquivoconfiguravelretornodocumentoService;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRetornoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.SelecionarDocumentoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/financeiro/process/ArquivoConfiguravelRetorno",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ArquivoConfiguravelRetornoProcess extends MultiActionController{
	
	private String ARQUIVORETORNO_SESSION = "ARQUIVORETORNO_SESSION";
	private String ARQUIVORETORNO_SESSION_CSV = "ARQUIVORETORNO_SESSION_CSV";
	
	private ContaService contaService;
	private BancoConfiguracaoRetornoService bancoConfiguracaoRetornoService;
	private DocumentoService documentoService;
	private ArquivoService arquivoService;
	private ArquivobancarioService arquivobancarioService;
	private ArquivobancariodocumentoService arquivobancariodocumentoService;
	private ArquivoDAO arquivoDAO;
	private ArquivoconfiguravelretornodocumentoService arquivoconfiguravelretornodocumentoService;
	
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setBancoConfiguracaoRetornoService(BancoConfiguracaoRetornoService bancoConfiguracaoRetornoService) {this.bancoConfiguracaoRetornoService = bancoConfiguracaoRetornoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {this.arquivobancarioService = arquivobancarioService;}
	public void setArquivobancariodocumentoService(ArquivobancariodocumentoService arquivobancariodocumentoService) {this.arquivobancariodocumentoService = arquivobancariodocumentoService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setArquivoconfiguravelretornodocumentoService(ArquivoconfiguravelretornodocumentoService arquivoconfiguravelretornodocumentoService) {
		this.arquivoconfiguravelretornodocumentoService = arquivoconfiguravelretornodocumentoService;
	}
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, ArquivoConfiguravelRetornoBean bean) throws Exception{
		List<Conta> listaConta = contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA);
		request.setAttribute("listaConta", listaConta);
		return new ModelAndView("/process/arquivoConfiguravelRetorno", "filtro", bean);
	}
	
	public ModelAndView processarArquivo(WebRequestContext request, ArquivoConfiguravelRetornoBean bean) throws Exception{				
		try{			
			request.getSession().removeAttribute(ARQUIVORETORNO_SESSION_CSV);
			
			bean = bancoConfiguracaoRetornoService.processarArquivoRetorno(bean);
			
			request.getSession().setAttribute(ARQUIVORETORNO_SESSION_CSV, bean.getArquivo());
			
			if (bean.isProcessado()){
				request.addError("Arquivo de retorno j� processado.");
			}else if (bean.getMsgEstrutura()!=null){
				String msg = bean.getMsgEstrutura().substring(0, bean.getMsgEstrutura().length()-2);
				request.addError("N�o foi poss�vel processar o arquivo de retorno. Favor, verificar a estrutura do arquivo. (" + msg + ")");
			}else {
				request.getSession().setAttribute(ARQUIVORETORNO_SESSION, bean.getArquivo());
			}
			
			boolean todosItensMarcados = !bean.isProcessado();
			for(ArquivoConfiguravelRetornoDocumentoBean retornoBean : bean.getListaDocumento()){
				if(retornoBean.getAcao() != null && (retornoBean.getMarcado() == null || !retornoBean.getMarcado())){
					todosItensMarcados = false;
					break;
				}
			}
			
			calcularTotais(bean);
			
			request.setAttribute("todosItensMarcados", todosItensMarcados);

			Long hash = System.currentTimeMillis();
			request.getSession().setAttribute(hash + "_ListaDocumentoRetornoConfiguravel", bean.getListaDocumento());
			bean.setHash(hash);
			
			try {
				executeControleDocumentoNaoEncontrado(request, bean);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return new ModelAndView("/process/arquivoConfiguravelRetorno_conferencia", "bean", bean);						
		} catch (Exception e) {
			request.addError(e.getMessage());
			return continueOnAction("carregar", bean);
		}
	}
	
	private void executeControleDocumentoNaoEncontrado(WebRequestContext request, ArquivoConfiguravelRetornoBean bean) {
		if(SinedUtil.getUrlWithoutContext(request.getServletRequest()).contains("morecap") && bean != null && SinedUtil.isListNotEmpty(bean.getListaDocumento())){
			for(ArquivoConfiguravelRetornoDocumentoBean beanDocumento : bean.getListaDocumento()){
				if(beanDocumento.getDocumento() == null && BancoConfiguracaoRetornoAcaoEnum.BAIXAR.equals(beanDocumento.getAcao())){
					try {
						arquivoconfiguravelretornodocumentoService.saveOrUpdate(new Arquivoconfiguravelretornodocumento(bean.getArquivo().getNome(), beanDocumento));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private void calcularTotais(ArquivoConfiguravelRetornoBean bean) {
		if(SinedUtil.isListNotEmpty(bean.getListaDocumento())){
			Money valorTotalPago = new Money();
			Money valorTotalPagoTituloEncontrado = new Money();
			
			for(ArquivoConfiguravelRetornoDocumentoBean retornoBean : bean.getListaDocumento()){
				if ((BancoConfiguracaoRetornoAcaoEnum.BAIXAR.equals(retornoBean.getAcao()) || BancoConfiguracaoRetornoAcaoEnum.BAIXA_HISTORICO.equals(retornoBean.getAcao()))
						&& retornoBean.getValorpago()!=null){
					valorTotalPago = valorTotalPago.add(retornoBean.getValorpago());
					if (retornoBean.getDocumento()!=null){
						valorTotalPagoTituloEncontrado = valorTotalPagoTituloEncontrado.add(retornoBean.getValorpago());							
					}
				}
			}
			
			bean.setValorTotalPago(valorTotalPago);
			bean.setValorTotalPagoTituloEncontrado(valorTotalPagoTituloEncontrado);
		}
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView processarDocumentos(WebRequestContext request, final ArquivoConfiguravelRetornoBean bean) throws Exception{
		try{			
			Long hash = bean.getHash();
			
			List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento = (List<ArquivoConfiguravelRetornoDocumentoBean>) request.getSession().getAttribute(hash + "_ListaDocumentoRetornoConfiguravel");
			for (int i = 0; i < listaDocumento.size(); i++) {
				listaDocumento.get(i).setMarcado(bean.getListaDocumento().get(i).getMarcado());
			}
			bean.setListaDocumento(listaDocumento);
			
			request.getSession().removeAttribute(hash + "_ListaDocumentoRetornoConfiguravel");
			
			Arquivo arquivo = (Arquivo) request.getSession().getAttribute(ARQUIVORETORNO_SESSION);
			arquivoService.saveOrUpdate(arquivo);
	
			// Cria e salva arquivo banc�rio
			Arquivobancario ab = new Arquivobancario();
			ab.setNome(arquivo.getNome());
			ab.setArquivo(arquivo);
			ab.setArquivobancariotipo(Arquivobancariotipo.RETORNO);
			ab.setArquivobancariosituacao(Arquivobancariosituacao.PROCESSADO);
			ab.setDtprocessa(SinedDateUtils.currentDate());
			arquivobancarioService.saveOrUpdate(ab);
			arquivoDAO.saveFile(ab, "arquivo");
			
			bancoConfiguracaoRetornoService.processarDocumentosArquivoRetorno(bean, ab);			
		
			if (bean.getIdsBaixado() != null && !"".equals(bean.getIdsBaixado())){			
				Arquivobancariosituacao situacao;
				List<Arquivobancariodocumento> listaNaoBaixado;
				List<Arquivobancario> listaArquivo = arquivobancarioService.findArquivoDocumentos(bean.getIdsBaixado());
				for (Arquivobancario arquivobancario : listaArquivo) {
					listaNaoBaixado = arquivobancariodocumentoService.findDocumentosNaoBaixado(arquivobancario, bean.getIdsBaixado());
					situacao = ((listaNaoBaixado != null && !listaNaoBaixado.isEmpty()) ? Arquivobancariosituacao.PROCESSADO_PARCIAL : Arquivobancariosituacao.PROCESSADO);
					arquivobancarioService.updateArquivoCorrespondenteSituacao(arquivobancario, ab, situacao);
				}
			}
			
			if (bean.getErros() != null && !"".equals(bean.getErros())){
				request.addMessage("Arquivo processado com advert�ncias: \n" + bean.getErros(), MessageType.WARN);
			} else
				request.addMessage("Arquivo processado com sucesso.");
			
			return new ModelAndView("/process/arquivoConfiguravelRetorno_resultado", "bean", bean);		
			
		} catch (Exception e) {
			request.addError(e.getMessage());
			return continueOnAction("carregar", bean);
		}
	}
	
	public ModelAndView openPopupIdentificarDocumento(WebRequestContext request, SelecionarDocumentoBean bean){
		bean.setListaDocumento(documentoService.carregaDocumentos(bean.getCds()));
		return new ModelAndView("direct://process/popup/selecionarDocumentoPopup", "bean", bean);
	}
	
	public ModelAndView openPopupRegistrarDocumento(WebRequestContext request, SelecionarDocumentoBean bean){
		List<Documento> listaDocumento = documentoService.findForSelecaoDocumentoListagem(bean);
		
		if (listaDocumento != null && listaDocumento.size() < bean.limiteListagem)
			bean.setLimitar(false);
		
		bean.setListaDocumento(listaDocumento);
		bean.setFiltrar(true);
		
		return new ModelAndView("direct://process/popup/selecionarDocumentoPopup", "bean", bean);
	}
	
	@SuppressWarnings("unchecked")
	public void carregaDadosDocumentoAjax(WebRequestContext request, Documento documento){
		if (documento != null && documento.getCddocumento() != null){
			documento = documentoService.loadForRetornoConfiguravel(documento);
			
			boolean erroAdicionarDocumento = false;
			try{			
				Long hash = request.getParameter("hashTelaRetorno") != null ? Long.parseLong(request.getParameter("hashTelaRetorno")) : null;
				Integer indexSelecionado = request.getParameter("indexSelecionado") != null ? Integer.parseInt(request.getParameter("indexSelecionado")) : null; 
				
				if(hash != null && indexSelecionado != null){
					List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento = (List<ArquivoConfiguravelRetornoDocumentoBean>) request.getSession().getAttribute(hash + "_ListaDocumentoRetornoConfiguravel");
					documento.setAssociadomanualmente(Boolean.TRUE);
					listaDocumento.get(indexSelecionado).setDocumento(documento);
				}
			}catch (Exception e) {
				erroAdicionarDocumento = true;
			}
			
			View view = View.getCurrent();
			request.getServletResponse().setContentType("text/html");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			view.println("var clientefornecedor = '" + (documento.getPessoa() != null ? documento.getPessoa().getNome() : "") + "';");
			view.println("var dtvencimento = '" + (documento.getDtvencimento() != null ? sdf.format(documento.getDtvencimento()) : "") + "';");
			view.println("var valor = '" + (documento.getAux_documento() != null ? documento.getAux_documento().getValoratual() : "") + "';");
			view.println("var cddocumentoacao = '" + (documento.getDocumentoacao() != null ? documento.getDocumentoacao().getCddocumentoacao() : "" ) + "';");
			view.println("var documentoacao = '" + (documento.getDocumentoacao() != null ? documento.getDocumentoacao().getNome() : "" ) + "';");
			view.println("var erroAdicionarDocumento = " + erroAdicionarDocumento + ";");
		}
	}
	
	public ModelAndView gerarCSV(WebRequestContext request,	ArquivoConfiguravelRetornoBean bean) throws Exception {
		Arquivo arquivo = (Arquivo) request.getSession().getAttribute(ARQUIVORETORNO_SESSION_CSV);
		bean.setArquivo(arquivo);
		
		bean = bancoConfiguracaoRetornoService.processarArquivoRetorno(bean);
		calcularTotais(bean);
		
		StringBuilder sb = new StringBuilder();
		
		sb
			.append("Nosso n�mero;")
			.append("Uso empresa;")
			.append("Cliente ou Fornecedor;")
			.append("Nome sacado;")
			.append("Data de venc. doc;")
			.append("Data de venc. retorno;")
			.append("Data do pagamento;")
			.append("Data do cr�dito;")
			.append("Data de ocorr�ncia;")
			.append("Valor do doc.;")
			.append("Valor do t�tulo;")
			.append("Valor do desconto;")
			.append("Valor do outro desconto;")
			.append("Valor do Juros / Multa;")
			.append("Valor pago;")
			.append("Situa��o;")
			.append("Ocorr�ncia;")
			.append("Mensagem;")
			.append("A��o;");
		
		if(SinedUtil.isListNotEmpty(bean.getListaDocumento())){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			for(ArquivoConfiguravelRetornoDocumentoBean retornoBean : bean.getListaDocumento()){
				sb.append("\n");
				
				if(retornoBean.getDocumento() != null && retornoBean.getDocumento().getCddocumento() != null){
					sb.append(retornoBean.getDocumento().getCddocumento().toString() + " " );
				}
				if(StringUtils.isNotBlank(retornoBean.getNossonumero())){
					sb.append(retornoBean.getNossonumero() + " ");
				}
				sb.append(";");
				
				if(StringUtils.isNotBlank(retornoBean.getUsoempresa())){
					sb.append(retornoBean.getUsoempresa());
				}
				sb.append(";");
				
				if(retornoBean.getDocumento() != null && retornoBean.getDocumento().getPessoa() != null){
					sb.append(retornoBean.getDocumento().getPessoa().getNome());
				}
				sb.append(";");
				
				if(StringUtils.isNotBlank(retornoBean.getNomesacado())){
					sb.append(retornoBean.getNomesacado());
				}
				sb.append(";");
				
				if(retornoBean.getDocumento() != null && retornoBean.getDocumento().getDtvencimento() != null){
					sb.append(sdf.format(retornoBean.getDocumento().getDtvencimento()));
				}
				sb.append(";");
				
				if(retornoBean.getDtvencimento() != null){
					sb.append(sdf.format(retornoBean.getDtvencimento()));
				}
				sb.append(";");
				
				if(retornoBean.getDtpagamento() != null){
					sb.append(sdf.format(retornoBean.getDtpagamento()));
				}
				sb.append(";");
				
				if(retornoBean.getDtcredito() != null){
					sb.append(sdf.format(retornoBean.getDtcredito()));
				}
				sb.append(";");
				
				if(retornoBean.getDtocorrencia() != null){
					sb.append(sdf.format(retornoBean.getDtocorrencia()));
				}
				sb.append(";");
				
				
				if(retornoBean.getDocumento() != null && retornoBean.getDocumento().getAux_documento() != null && retornoBean.getDocumento().getAux_documento().getValoratual() != null){
					sb.append(retornoBean.getDocumento().getAux_documento().getValoratual());
				}
				sb.append(";");
				
				if(retornoBean.getValor() != null){
					sb.append(retornoBean.getValor());
				}
				sb.append(";");
				
				if(retornoBean.getValordesconto() != null){
					sb.append(retornoBean.getValordesconto());
				}
				sb.append(";");
				
				if(retornoBean.getValoroutrodesconto() != null){
					sb.append(retornoBean.getValoroutrodesconto());
				}
				sb.append(";");
				
				if(retornoBean.getValorjurosmulta() != null){
					sb.append(retornoBean.getValorjurosmulta());
				}
				sb.append(";");
				
				
				if((BancoConfiguracaoRetornoAcaoEnum.BAIXAR.equals(retornoBean.getAcao()) ||
						BancoConfiguracaoRetornoAcaoEnum.BAIXA_HISTORICO.equals(retornoBean.getAcao())) &&
						retornoBean.getValorpago() != null){
					sb.append(retornoBean.getValorpago());
				}
				sb.append(";");
				
				if(retornoBean.getDocumento() != null && retornoBean.getDocumento().getDocumentoacao() != null){
					sb.append(retornoBean.getDocumento().getDocumentoacao().getNome());
				}
				sb.append(";");
				
				if(retornoBean.getCodocorrencia() != null){
					sb.append(retornoBean.getCodocorrencia());
				}
				if(retornoBean.getOcorrencia() != null){
					sb.append((retornoBean.getCodocorrencia() != null ? " " : "") + retornoBean.getOcorrencia());
				}
				sb.append(";");
				
				if(retornoBean.getMotivorejeicao() != null){
					sb.append(retornoBean.getMotivorejeicao());
				}
				sb.append(";");
				
				if(retornoBean.getAcao() != null){
					sb.append(retornoBean.getAcao().getNome());
				}else {
					sb.append("A��o n�o identificada");
				}
				sb.append(";");
			}
		}
		
		sb.append("\n\n").append("Total valor pago:;").append(bean.getValorTotalPago());
		sb.append("\n").append("Total valor pago - T�tulos encontrados:;").append(bean.getValorTotalPagoTituloEncontrado());
		

		Resource resource = new Resource("text/csv", "arquivo_retorno_configuravel_" + SinedUtil.datePatternForReport() + "_" + arquivo.getNome() +".csv", sb.toString().getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
}
