package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.BoletodigitalService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRemessaBean;
import br.com.linkcom.sined.util.SinedException;

@Controller(path="/financeiro/process/BoletoDigitalRemessa",authorizationModule=ProcessAuthorizationModule.class)
public class BoletoDigitalRemessaProcess extends MultiActionController{
	
	private DocumentoService documentoService;
	private DocumentotipoService documentotipoService;
	private ParametrogeralService parametrogeralService;
	private BoletodigitalService boletodigitalService;

	public void setBoletodigitalService(BoletodigitalService boletodigitalService) {this.boletodigitalService = boletodigitalService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, BoletoDigitalRemessaBean filtro) throws Exception{
		request.setAttribute("listaDocumentoTipo", documentotipoService.getListaDocumentoTipoUsuario());
		request.setAttribute("FATURA_LOCACAO", parametrogeralService.getBoolean(Parametrogeral.FATURA_LOCACAO));
		return new ModelAndView("/process/boletoDigitalRemessa").addObject("filtro", filtro);
	}
	
	public ModelAndView listar(WebRequestContext request, BoletoDigitalRemessaBean filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		List<Documento> listaDocumento = documentoService.findDadosForListagemRemessaBoletoDigitalConfiguravel(filtro);
		documentoService.preencherNumeroFaturalocacao(listaDocumento);
		
		Money valorTotal = new Money();
		
		for (Documento documento: listaDocumento){
			valorTotal = valorTotal.add(documento.getAux_documento().getValoratual());
		}
		
		request.setAttribute("TotalRegistros", listaDocumento != null ? listaDocumento.size() : 0);
		request.setAttribute("valorTotal", valorTotal);
		request.setAttribute("FATURA_LOCACAO", parametrogeralService.getBoolean(Parametrogeral.FATURA_LOCACAO));
		request.setAttribute("filtro", filtro);
		return new ModelAndView("direct:ajax/boletoDigitalRemessalistagem", "listaDocumento", listaDocumento);
	}
	
	public ModelAndView gerar(WebRequestContext request, BoletoDigitalRemessaBean filtro) throws Exception {		
		filtro.setCds(request.getParameter("cds"));
		try {
			boletodigitalService.gerarRemessa(filtro);
			request.addMessage("Remessa de boleto digital enviado com sucesso.");
			return sendRedirectToAction("index");
		} catch (SinedException e) {
			request.addMessage(e.getMessage(), MessageType.ERROR);
			request.setAttribute("erroGeracaoArquivo", true);
			return continueOnAction("index", filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addMessage("Erro desconhecido: " + e.getMessage(), MessageType.ERROR);
			request.setAttribute("erroGeracaoArquivo", true);
			return continueOnAction("index", filtro);
		}	
	}
	
}
