package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;


public class RegistroE510Apoio {
	
	private String cfop;
	private String cst_ipi;
	private Double vl_cont_ipi;
	private Double vl_bc_ipi;
	private Double vl_ipi;
	
	public RegistroE510Apoio(String cfop, String cst_ipi) {
		this.cfop = cfop;
		this.cst_ipi = cst_ipi;
		
		vl_cont_ipi = 0.0;
		vl_bc_ipi = 0.0;
		vl_ipi = 0.0;
	}
	
	public void addValorcontipi(Double vl_cont_ipi) {
		this.vl_cont_ipi += vl_cont_ipi;
	}

	public void addValorbcipi(Double vl_bc_ipi) {
		this.vl_bc_ipi += vl_bc_ipi;
	}

	public void addValoripi(Double vl_ipi) {
		this.vl_ipi += vl_ipi;
	}

	public String getCfop() {
		return cfop;
	}

	public String getCst_ipi() {
		return cst_ipi;
	}

	public Double getVl_cont_ipi() {
		return vl_cont_ipi;
	}

	public Double getVl_bc_ipi() {
		return vl_bc_ipi;
	}

	public Double getVl_ipi() {
		return vl_ipi;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	public void setCst_ipi(String cst_ipi) {
		this.cst_ipi = cst_ipi;
	}

	public void setVl_cont_ipi(Double vl_cont_ipi) {
		this.vl_cont_ipi = vl_cont_ipi;
	}

	public void setVl_bc_ipi(Double vl_bc_ipi) {
		this.vl_bc_ipi = vl_bc_ipi;
	}

	public void setVl_ipi(Double vl_ipi) {
		this.vl_ipi = vl_ipi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cfop == null) ? 0 : cfop.hashCode());
		result = prime * result + ((cst_ipi == null) ? 0 : cst_ipi.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final RegistroE510Apoio other = (RegistroE510Apoio) obj;
		if (cfop == null) {
			if (other.cfop != null)
				return false;
		} else if (!cfop.equals(other.cfop))
			return false;
		if (cst_ipi == null) {
			if (other.cst_ipi != null)
				return false;
		} else if (!cst_ipi.equals(other.cst_ipi))
			return false;
		return true;
	}
	

}
