package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.util.SinedUtil;


public class ArquivoConciliacaoBean {

	protected Arquivo arquivo;
	protected Conta conta;
	protected List<ConciliacaoBean> listaCreditoEncontrado;
	protected List<ConciliacaoBean> listaDebitoEncontrado;
	protected List<ConciliacaoBean> listaEncontrada = new ArrayList<ConciliacaoBean>();	
	protected List<ConciliacaoBean> listaCredito;
	protected List<ConciliacaoBean> listaDebito;
	protected List<ConciliacaoBean> listaDataPostedPosteriorCredito;
	protected List<ConciliacaoBean> listaDataPostedPosteriorDebito;
	protected String[] linhas;

	
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public Conta getConta() {
		return conta;
	}
	
	public List<ConciliacaoBean> getListaEncontrada() {
		return listaEncontrada;
	}

	public List<ConciliacaoBean> getListaCredito() {
		return listaCredito;
	}

	public List<ConciliacaoBean> getListaDebito() {
		return listaDebito;
	}
	
	public String[] getLinhas() {
		return linhas;
	}
	
	public void setLinhas(String[] linhas) {
		this.linhas = linhas;
	}
	
	public void setListaEncontrada(List<ConciliacaoBean> listaEncontrada) {
		this.listaEncontrada = listaEncontrada;
	}

	public void setListaCredito(List<ConciliacaoBean> listaCredito) {
		this.listaCredito = listaCredito;
	}

	public void setListaDebito(List<ConciliacaoBean> listaDebito) {
		this.listaDebito = listaDebito;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public List<ConciliacaoBean> getListaEncontradaCredito(){
		
		List<ConciliacaoBean> lista = new ArrayList<ConciliacaoBean>();
		
		if (listaEncontrada!=null){
			for (ConciliacaoBean c: listaEncontrada){
				if (c.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
					lista.add(c);				
				}
			}
		}
		
		return lista;
	}
	
	public List<ConciliacaoBean> getListaEncontradaDebito(){
		
		List<ConciliacaoBean> lista = new ArrayList<ConciliacaoBean>();
		
		if (listaEncontrada!=null){
			for (ConciliacaoBean c: listaEncontrada){
				if (c.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
					lista.add(c);				
				}
			}
		}
		
		return lista;
	}
	
	public List<ConciliacaoBean> getListaRelatorio(){
		
		List<ConciliacaoBean> lista = new ArrayList<ConciliacaoBean>();
		
		if (listaCredito!=null){
			for (ConciliacaoBean c: listaCredito){
				c.setOrder(1);
				c.setTipo("Movimenta��es N�o Encontradas - Cr�dito");
				lista.add(c);				
			}
		}
		
		if (listaDebito!=null){
			for (ConciliacaoBean c: listaDebito){
				c.setOrder(2);
				c.setTipo("Movimenta��es N�o Encontradas - D�bito");
				lista.add(c);				
			}
		}
		
//		for (ConciliacaoBean c: getListaEncontradaCredito()){
//			c.setOrder(3);
//			c.setTipo("Movimenta��es Encontradas - Cr�dito");
//			lista.add(c);				
//		}
//		
//		for (ConciliacaoBean c: getListaEncontradaDebito()){
//			c.setOrder(4);
//			c.setTipo("Movimenta��es Encontradas - D�bito");
//			lista.add(c);				
//		}
		
		Collections.sort(lista, new Comparator<ConciliacaoBean>(){
			@Override
			public int compare(ConciliacaoBean o1, ConciliacaoBean o2) {
				return o1.getOrder().compareTo(o2.getOrder());
			}			
		});
		
		if (SinedUtil.isListNotEmpty(getListaCreditoEncontrado())) {
			for (ConciliacaoBean c: getListaCreditoEncontrado()){
				c.setOrder(3);
				c.setTipo("Movimenta��es Encontradas - Cr�dito");
				lista.add(c);				
			}
		}
		
		if (SinedUtil.isListNotEmpty(getListaDebitoEncontrado())) {
			for (ConciliacaoBean c: getListaDebitoEncontrado()){
				c.setOrder(4);
				c.setTipo("Movimenta��es Encontradas - D�bito");
				lista.add(c);				
			}
		}
		
		if (SinedUtil.isListNotEmpty(getListaDataPostedPosteriorCredito())) {
			for (ConciliacaoBean c: getListaDataPostedPosteriorCredito()){
				c.setOrder(5);
				c.setTipo("Movimenta��es com data posterior - Cr�dito");
				lista.add(c);				
			}
		}
		
		if (SinedUtil.isListNotEmpty(getListaDataPostedPosteriorDebito())) {
			for (ConciliacaoBean c: getListaDataPostedPosteriorDebito()){
				c.setOrder(6);
				c.setTipo("Movimenta��es com data posterior - D�bito");
				lista.add(c);				
			}
		}
		
		return lista;
	}

	public List<ConciliacaoBean> getListaCreditoEncontrado() {
		return listaCreditoEncontrado;
	}

	public List<ConciliacaoBean> getListaDebitoEncontrado() {
		return listaDebitoEncontrado;
	}

	public List<ConciliacaoBean> getListaDataPostedPosteriorCredito() {
		return listaDataPostedPosteriorCredito;
	}

	public List<ConciliacaoBean> getListaDataPostedPosteriorDebito() {
		return listaDataPostedPosteriorDebito;
	}

	public void setListaDataPostedPosteriorDebito(
			List<ConciliacaoBean> listaDataPostedPosteriorDebito) {
		this.listaDataPostedPosteriorDebito = listaDataPostedPosteriorDebito;
	}

	public void setListaDataPostedPosteriorCredito(
			List<ConciliacaoBean> listaDataPostedPosteriorCredito) {
		this.listaDataPostedPosteriorCredito = listaDataPostedPosteriorCredito;
	}

	public void setListaCreditoEncontrado(
			List<ConciliacaoBean> listaCreditoEncontrado) {
		this.listaCreditoEncontrado = listaCreditoEncontrado;
	}

	public void setListaDebitoEncontrado(List<ConciliacaoBean> listaDebitoEncontrado) {
		this.listaDebitoEncontrado = listaDebitoEncontrado;
	}	
	
}