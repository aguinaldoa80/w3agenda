package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaAuxiliar;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaInstrucao;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContaPagar;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;

public class ArquivoConfiguravelRemessaPagamentoBean {

	private Empresa empresa;
	private Conta conta;
	
	private Date dtinicioEmissao;
	private Date dtfimEmissao;
	private Date dtinicioVencimento;
	private Date dtfimVencimento;
	private Empresa empresafiltro;
	private Centrocusto centroCusto;
	private Contagerencial contaGerencial;
	private Projeto projeto;
	private Tipopagamento tipoPagamentoFiltro;
	private Colaborador colaborador;
	private Cliente cliente;
	private Fornecedor fornecedor;
	private String outrosPagamento;
	private String pessoa;
	private Money valorMinimo;
	private Money valorMaximo;
	private List<SituacaoContaPagar> listaSituacao;

	private Documentotipo documentotipo;
	private BancoTipoPagamento tipoPagamento;
	private BancoFormapagamento formaPagamento;
	private BancoConfiguracaoRemessaInstrucao instrucaoPagamento;
	private BancoConfiguracaoRemessa bancoConfiguracaoRemessa;
	private BancoConfiguracaoRemessaAuxiliar auxiliar1;
	private BancoConfiguracaoRemessaAuxiliar auxiliar2;
	private List<Documentoacao> listaDocumentoacao;
	
	private String cds;
	
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	@Required
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public Date getDtinicioEmissao() {
		return dtinicioEmissao;
	}
	public void setDtinicioEmissao(Date dtinicioEmissao) {
		this.dtinicioEmissao = dtinicioEmissao;
	}
	public Date getDtfimEmissao() {
		return dtfimEmissao;
	}
	public void setDtfimEmissao(Date dtfimEmissao) {
		this.dtfimEmissao = dtfimEmissao;
	}
	public Date getDtinicioVencimento() {
		return dtinicioVencimento;
	}
	public void setDtinicioVencimento(Date dtinicioVencimento) {
		this.dtinicioVencimento = dtinicioVencimento;
	}
	public Date getDtfimVencimento() {
		return dtfimVencimento;
	}
	public void setDtfimVencimento(Date dtfimVencimento) {
		this.dtfimVencimento = dtfimVencimento;
	}
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	@Required
	@DisplayName("Tipo de pagamento")
	public BancoTipoPagamento getTipoPagamento() {
		return tipoPagamento;
	}
	public void setTipoPagamento(BancoTipoPagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
	@Required
	@DisplayName("Forma de pagamento")
	public BancoFormapagamento getFormaPagamento() {
		return formaPagamento;
	}
	public void setFormaPagamento(BancoFormapagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	@DisplayName("Instru��o de pagamento")
	public BancoConfiguracaoRemessaInstrucao getInstrucaoPagamento() {
		return instrucaoPagamento;
	}
	public void setInstrucaoPagamento(BancoConfiguracaoRemessaInstrucao instrucaoPagamento) {
		this.instrucaoPagamento = instrucaoPagamento;
	}
	@Required
	@DisplayName("Arquivo de remessa")
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa() {
		return bancoConfiguracaoRemessa;
	}
	public BancoConfiguracaoRemessaAuxiliar getAuxiliar1() {
		return auxiliar1;
	}
	public BancoConfiguracaoRemessaAuxiliar getAuxiliar2() {
		return auxiliar2;
	}
	public void setBancoConfiguracaoRemessa(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		this.bancoConfiguracaoRemessa = bancoConfiguracaoRemessa;
	}
	public void setAuxiliar1(BancoConfiguracaoRemessaAuxiliar auxiliar1) {
		this.auxiliar1 = auxiliar1;
	}
	public void setAuxiliar2(BancoConfiguracaoRemessaAuxiliar auxiliar2) {
		this.auxiliar2 = auxiliar2;
	}
	public String getCds() {
		return cds;
	}
	public void setCds(String cds) {
		this.cds = cds;
	}
	
	public List<Documentoacao> getListaDocumentoacao() {
		return listaDocumentoacao;
	}
	public void setListaDocumentoacao(List<Documentoacao> listaDocumentoacao) {
		this.listaDocumentoacao = listaDocumentoacao;
	}
	@DisplayName("Empresa")
	public Empresa getEmpresafiltro() {
		return empresafiltro;
	}
	public void setEmpresafiltro(Empresa empresafiltro) {
		this.empresafiltro = empresafiltro;
	}
	
	
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}
	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	public Contagerencial getContaGerencial() {
		return contaGerencial;
	}
	public void setContaGerencial(Contagerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public Tipopagamento getTipoPagamentoFiltro() {
		return tipoPagamentoFiltro;
	}
	public void setTipoPagamentoFiltro(Tipopagamento tipoPagamentoFiltro) {
		this.tipoPagamentoFiltro = tipoPagamentoFiltro;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public String getOutrosPagamento() {
		return outrosPagamento;
	}
	public void setOutrosPagamento(String outrosPagamento) {
		this.outrosPagamento = outrosPagamento;
	}
	public Money getValorMinimo() {
		return valorMinimo;
	}
	public void setValorMinimo(Money valorMinimo) {
		this.valorMinimo = valorMinimo;
	}
	public Money getValorMaximo() {
		return valorMaximo;
	}
	public void setValorMaximo(Money valorMaximo) {
		this.valorMaximo = valorMaximo;
	}
	public List<SituacaoContaPagar> getListaSituacao() {
		return listaSituacao;
	}
	public void setListaSituacao(List<SituacaoContaPagar> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public String getPessoa() {
		return pessoa;
	}
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	
	
}