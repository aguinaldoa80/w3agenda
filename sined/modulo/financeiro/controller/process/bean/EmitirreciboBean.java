package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documento;


public class EmitirreciboBean {

	protected List<Documento> listaDocumento;

	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}

	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}	
}