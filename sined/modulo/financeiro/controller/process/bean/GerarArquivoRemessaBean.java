package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;


public class GerarArquivoRemessaBean{

	protected Conta conta;
	protected Contacarteira contacarteira;
	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected Date dtemissao1;
	protected Date dtemissao2;
	protected Date dtemissaonota1;
	protected Date dtemissaonota2;
	protected String contas;
	protected String padraoArquivo;
	protected Boolean igualDiferenteDocumentotipo;
	protected Documentotipo documentotipo;
	protected Projeto projeto;
	protected Centrocusto centrocusto;
	private Cliente cliente;
	
	@DisplayName("Padr�o do arquivo")
	public String getPadraoArquivo() {
		return padraoArquivo;
	}

	public void setPadraoArquivo(String padraoArquivo) {
		this.padraoArquivo = padraoArquivo;
	}

	@Required
	@DisplayName("Conta")	
	public Conta getConta() {
		return conta;
	}
	
	@DisplayName("De")
	public Date getDtinicio() {
		return dtinicio;
	}
	
	@DisplayName("At�")
	public Date getDtfim() {
		return dtfim;
	}
	
	public String getContas() {
		return contas;
	}
	
	@Required
	@DisplayName("Carteira")
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Date getDtemissao1() {
		return dtemissao1;
	}
	
	public Date getDtemissao2() {
		return dtemissao2;
	}
	
	public Boolean getIgualDiferenteDocumentotipo() {
		return igualDiferenteDocumentotipo;
	}
	
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	public void setDtemissao1(Date dtemissao1) {
		this.dtemissao1 = dtemissao1;
	}

	public void setDtemissao2(Date dtemissao2) {
		this.dtemissao2 = dtemissao2;
	}

	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	public void setContas(String contas) {
		this.contas = contas;
	}
	
	public void setIgualDiferenteDocumentotipo(Boolean igualDiferenteDocumentotipo) {
		this.igualDiferenteDocumentotipo = igualDiferenteDocumentotipo;
	}
	
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	public Date getDtemissaonota1() {
		return dtemissaonota1;
	}

	public Date getDtemissaonota2() {
		return dtemissaonota2;
	}

	public void setDtemissaonota1(Date dtemissaonota1) {
		this.dtemissaonota1 = dtemissaonota1;
	}

	public void setDtemissaonota2(Date dtemissaonota2) {
		this.dtemissaonota2 = dtemissaonota2;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}