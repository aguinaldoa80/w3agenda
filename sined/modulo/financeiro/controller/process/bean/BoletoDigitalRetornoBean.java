package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Empresa;

public class BoletoDigitalRetornoBean {

	private Empresa empresa;
	private Conta conta;
	private Contacarteira contacarteira;
	
	private Money valorTotalPago;
	
	private List<BoletoDigitalRetornoDocumentoBean> listaDocumento = new ListSet<BoletoDigitalRetornoDocumentoBean>(BoletoDigitalRetornoDocumentoBean.class);
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Conta banc�ria")
	public Conta getConta() {
		return conta;
	}
	@DisplayName("Carteira")
	@Required
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	public List<BoletoDigitalRetornoDocumentoBean> getListaDocumento() {
		return listaDocumento;
	}
	@DisplayName("Valor total pago")
	public Money getValorTotalPago() {
		return valorTotalPago;
	}
	public void setValorTotalPago(Money valorTotalPago) {
		this.valorTotalPago = valorTotalPago;
	}
	public void setListaDocumento(
			List<BoletoDigitalRetornoDocumentoBean> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	
}