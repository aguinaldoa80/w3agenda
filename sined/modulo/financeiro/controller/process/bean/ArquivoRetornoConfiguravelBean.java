package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Bancoretorno;
import br.com.linkcom.sined.geral.bean.Conta;

public class ArquivoRetornoConfiguravelBean {
	
	private Bancoretorno bancoretorno;
	private Banco banco;
	private Arquivo arquivo;
	
	private Conta conta;	
	private List<ArquivoRetornoConfiguravelDocumentoBean> listaDocumento;
	private List<ArquivoRetornoConfiguravelDocumentoBean> listaConferencia;
	
	private Money valorTotal;
	
	@Required
	@DisplayName("Vers�o da configura��o")
	public Bancoretorno getBancoretorno() {
		return bancoretorno;
	}
	@Required
	public Banco getBanco() {
		return banco;
	}
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	public List<ArquivoRetornoConfiguravelDocumentoBean> getListaDocumento() {
		return listaDocumento;
	}
	public Conta getConta() {
		return conta;
	}
	public List<ArquivoRetornoConfiguravelDocumentoBean> getListaConferencia() {
		return listaConferencia;
	}
	public void setListaConferencia(
			List<ArquivoRetornoConfiguravelDocumentoBean> listaConferencia) {
		this.listaConferencia = listaConferencia;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setListaDocumento(List<ArquivoRetornoConfiguravelDocumentoBean> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setBancoretorno(Bancoretorno bancoretorno) {
		this.bancoretorno = bancoretorno;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	@DisplayName("Valor total")
	public Money getValorTotal() {
		Money total = new Money(0.0);
		if(this.listaDocumento != null && !this.listaDocumento.isEmpty())
			for (ArquivoRetornoConfiguravelDocumentoBean bean : listaDocumento) 
				total = total.add(bean.getValor() != null ? bean.getValor() : new Money(0.0));
		
		this.valorTotal = total;
		return valorTotal;
	}
	
	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}
	
}
