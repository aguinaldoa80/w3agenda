package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfop;

public class RegistroAnaliticoSped {
	
	private String cst;
	private Cfop cfop;
	private Double aliq_icms;
	
	private Money valoroperacao;
	private Money valorbcicms;
	private Money valoricms;
	private Money valorbcicmsst;
	private Money valoricmsst;
	private Money valorredbc;
	private Money valoripi;
	
	private Boolean saida;
	private String cst_ipi_aux;
	
	public RegistroAnaliticoSped(String cstOrigemproduto, Cfop cfop, Double aliq_icms) {
		this.cst = cstOrigemproduto;
		this.cfop = cfop;
		this.aliq_icms = aliq_icms;
		
		valoroperacao = new Money();
		valorbcicms = new Money();
		valoricms = new Money();
		valorbcicmsst = new Money();
		valoricmsst = new Money();
		valorredbc = new Money();
		valoripi = new Money();
	}
	
	public void addValoroperacao(Money valoroperacao) {
		this.valoroperacao = this.valoroperacao.add(valoroperacao);
	}

	public void addValorbcicms(Money valorbcicms) {
		this.valorbcicms = this.valorbcicms.add(valorbcicms);
	}

	public void addValoricms(Money valoricms) {
		this.valoricms = this.valoricms.add(valoricms);
	}

	public void addValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = this.valorbcicmsst.add(valorbcicmsst);
	}

	public void addValoricmsst(Money valoricmsst) {
		this.valoricmsst = this.valoricmsst.add(valoricmsst);
	}

	public void addValorredbc(Money valorredbc) {
		this.valorredbc = this.valorredbc.add(valorredbc);
	}

	public void addValoripi(Money valoripi) {
		this.valoripi = this.valoripi.add(valoripi);
	}
	
	public String getCst() {
		return cst;
	}
	public Cfop getCfop() {
		return cfop;
	}
	public Double getAliq_icms() {
		return aliq_icms;
	}
	public Money getValoroperacao() {
		return valoroperacao;
	}
	public Money getValorbcicms() {
		return valorbcicms;
	}
	public Money getValoricms() {
		return valoricms;
	}
	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}
	public Money getValoricmsst() {
		return valoricmsst;
	}
	public Money getValorredbc() {
		return valorredbc;
	}
	public Money getValoripi() {
		return valoripi;
	}
	public void setValoroperacao(Money valoroperacao) {
		this.valoroperacao = valoroperacao;
	}
	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}
	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}
	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}
	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}
	public void setValorredbc(Money valorredbc) {
		this.valorredbc = valorredbc;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public void setCst(String cst) {
		this.cst = cst;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	public void setAliq_icms(Double aliq_icms) {
		this.aliq_icms = aliq_icms;
	}
	
	public Boolean getSaida() {
		return saida;
	}
	public String getCst_ipi_aux() {
		return cst_ipi_aux;
	}

	public void setSaida(Boolean saida) {
		this.saida = saida;
	}
	public void setCst_ipi_aux(String cstIpiAux) {
		cst_ipi_aux = cstIpiAux;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aliq_icms == null) ? 0 : aliq_icms.hashCode());
		result = prime * result + ((cfop == null) ? 0 : cfop.hashCode());
		result = prime * result + ((cst == null) ? 0 : cst.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final RegistroAnaliticoSped other = (RegistroAnaliticoSped) obj;
		if (aliq_icms == null) {
			if (other.aliq_icms != null)
				return false;
		} else if (!aliq_icms.equals(other.aliq_icms))
			return false;
		if (cfop == null) {
			if (other.cfop != null)
				return false;
		} else if (!cfop.equals(other.cfop))
			return false;
		if (cst == null) {
			if (other.cst != null)
				return false;
		} else if (!cst.equals(other.cst))
			return false;
		return true;
	}
	

}
