package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;


public class CusteioFiltroAndBean{
	
	private Date dtPeriodoInicio;
	private Date dtPeriodoFim;
	private Arquivo arquivo;
	private Empresa empresa;
	
	////////////////|
	///	GETS	 ///|
	/////////////////
	@Required
	@DisplayName("data inicial")
	public Date getDtPeriodoInicio() {
		return dtPeriodoInicio;
	}
	@Required
	@DisplayName("data final")
	public Date getDtPeriodoFim() {
		return dtPeriodoFim;
	}
	@DisplayName("Arquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	////////////////|
	///	SETS	 ///|
	/////////////////
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtPeriodoInicio(Date dtPeriodoInicio) {
		this.dtPeriodoInicio = dtPeriodoInicio;
	}
	public void setDtPeriodoFim(Date dtPeriodoFim) {
		this.dtPeriodoFim = dtPeriodoFim;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	
}