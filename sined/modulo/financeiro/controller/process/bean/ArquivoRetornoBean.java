package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoarquivoretorno;


public class ArquivoRetornoBean{

	protected Arquivo arquivo;
	protected List<Documento> listaDocumentoAprovado;
	protected List<Documento> listaDocumentoReprovado;
	protected Money valortotal;
	protected Movimentacao movimentacao;
	protected Tipoarquivoretorno tipoarquivoretorno;
	
	protected List<ArquivoRetornoCAIXABean> listaConta;
	protected List<ArquivoRetornoBradescoBean> listaContaBradesco;
	protected List<ArquivoRetornoItauBean> listaContaItau;
	protected List<ArquivoRetornoBrasilBean> listaContaBB;
	protected List<ArquivoRetornoSantanderBean> listaContaSantander;
	protected List<ArquivoRetornoRuralBean> listaContaRural;
	protected Conta conta;
	
	private Money valorTotalPagamento;
	private Money valorTotalContaReceber;
	private Money valorTotalAprovado;
	
	private Arquivobancario arquivobancario;
	protected List<Documento> listaDocumentoMarcado;
	
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public List<Documento> getListaDocumentoAprovado() {
		return listaDocumentoAprovado;
	}
	
	public List<Documento> getListaDocumentoReprovado() {
		return listaDocumentoReprovado;
	}
	
	public Money getValortotal() {
		return valortotal;
	}
	
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	
	public Tipoarquivoretorno getTipoarquivoretorno() {
		return tipoarquivoretorno;
	}
	
	public List<ArquivoRetornoCAIXABean> getListaConta() {
		return listaConta;
	}
	
	public List<ArquivoRetornoBradescoBean> getListaContaBradesco() {
		return listaContaBradesco;
	}
	
	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setListaConta(List<ArquivoRetornoCAIXABean> listaConta) {
		this.listaConta = listaConta;
	}
	
	public void setListaContaBradesco(
			List<ArquivoRetornoBradescoBean> listaContaBradesco) {
		this.listaContaBradesco = listaContaBradesco;
	}

	public void setTipoarquivoretorno(Tipoarquivoretorno tipoarquivoretorno) {
		this.tipoarquivoretorno = tipoarquivoretorno;
	}

	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	
	public void setListaDocumentoAprovado(List<Documento> listaDocumentoAprovado) {
		this.listaDocumentoAprovado = listaDocumentoAprovado;
	}
	
	public void setListaDocumentoReprovado(
			List<Documento> listaDocumentoReprovado) {
		this.listaDocumentoReprovado = listaDocumentoReprovado;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public Money getValorTotalContaReceber() {
		return valorTotalContaReceber;
	}
	public Money getValorTotalPagamento() {
		return valorTotalPagamento;
	}
	
	public void setValorTotalContaReceber(Money valorTotalContaReceber) {
		this.valorTotalContaReceber = valorTotalContaReceber;
	}
	public void setValorTotalPagamento(Money valorTotalPagamento) {
		this.valorTotalPagamento = valorTotalPagamento;
	}
	
	@DisplayName("Valor total")
	public Money getValorTotalAprovado() {
		Money total = new Money(0.0);
		if(this.listaDocumentoAprovado != null && !this.listaDocumentoAprovado.isEmpty())
			for (Documento documento : listaDocumentoAprovado) 
				total = total.add(documento.getValor() != null ? documento.getValor() : new Money(0.0));
		
		this.valorTotalAprovado = total;
		return valorTotalAprovado;
	}
	
	public void setValorTotalAprovado(Money valorTotalAprovado) {
		this.valorTotalAprovado = valorTotalAprovado;
	}

	public List<ArquivoRetornoItauBean> getListaContaItau() {
		return listaContaItau;
	}

	public void setListaContaItau(List<ArquivoRetornoItauBean> listaContaItau) {
		this.listaContaItau = listaContaItau;
	}

	public List<ArquivoRetornoBrasilBean> getListaContaBB() {
		return listaContaBB;
	}

	public void setListaContaBB(List<ArquivoRetornoBrasilBean> listaContaBB) {
		this.listaContaBB = listaContaBB;
	}
	public List<ArquivoRetornoSantanderBean> getListaContaSantander() {
		return listaContaSantander;
	}
	public void setListaContaSantander(
			List<ArquivoRetornoSantanderBean> listaContaSantander) {
		this.listaContaSantander = listaContaSantander;
	}

	public List<ArquivoRetornoRuralBean> getListaContaRural() {
		return listaContaRural;
	}

	public void setListaContaRural(List<ArquivoRetornoRuralBean> listaContaRural) {
		this.listaContaRural = listaContaRural;
	}
	
	public Arquivobancario getArquivobancario() {
		return arquivobancario;
	}
	
	public void setArquivobancario(Arquivobancario arquivobancario) {
		this.arquivobancario = arquivobancario;
	}
	
	public List<Documento> getListaDocumentoMarcado() {
		return listaDocumentoMarcado;
	}
	
	public void setListaDocumentoMarcado(List<Documento> listaDocumentoMarcado) {
		this.listaDocumentoMarcado = listaDocumentoMarcado;
	}
}