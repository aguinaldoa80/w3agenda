package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.enumeration.FinalidadeDOC;
import br.com.linkcom.sined.geral.bean.enumeration.FinalidadeTED;
import br.com.linkcom.sined.util.SinedDateUtils;

@DisplayName("BAIXAR CONTA")
public class BaixarContaBean{
	
	protected List<Documento> listaDocumento;
	protected Rateio rateio;
	protected Date dtpagamento;
	protected Boolean gerarmovimentacaoparacadacontapagar;
	protected Money valorJuros = new Money(0);
	protected Money valorRecebido = new Money(0);
	protected Money valorDescontos = new Money(0);
	protected Money valorMovimento;
	protected Formapagamento formapagamento;
	protected Contatipo contatipo;
	protected Conta vinculo;
	protected Cheque cheque;
	protected Documentoclasse documentoclasse;
	protected Boolean gerararquivoremessa;
	protected Double valorTotal;
	protected Double valortaxaTotal;
	protected Arquivo arquivo;
	protected Boolean erro;
	protected Boolean isContaBaixada;
	protected String checknum;
	protected String historico;
	protected Boolean fromRetorno = false;
	protected Boolean fromWebservice = false;
	protected Boolean fromBaixaautomatica = false;
	protected Boolean fromBaixamanual = false;
	protected Boolean gerararquivoregistrocobranca;
	protected BancoFormapagamento bancoformapagamento;
	protected BancoTipoPagamento bancotipopagamento;
	protected Boolean gerarsispag;
	protected FinalidadeTED finalidadeted;
	protected FinalidadeDOC finalidadedoc;
	protected Documento documento;
	protected Money valorvalecompra;
	protected Money valormaximovalecompra;
	protected Boolean gerarValeCompra;
	protected Documento documentoVinculado;
	protected Empresa empresa;
	
	protected Arquivo comprovante;
	
	protected Integer cdchequecheque;
	protected Integer bancocheque;
	protected Integer agenciacheque;
	protected Integer contacheque;
	protected String numerocheque;
	protected String emitentecheque;
	protected String cpfcnpjcheque;
	protected Date dtbomparacheque;
	protected Money valorcheque;
	
	protected String chequeauxiliar;
	
	protected List<BaixarContaBeanMovimentacao> listaBaixarContaBeanMovimentacao;
	
	protected Money valortotalmovimentacao;
	protected Money valorrestante;
	protected Date dtcredito;
	protected String whereInMovReferente;
	protected Money valorTotalMovReferente;
	protected String mensagem;
	
	{
		this.dtpagamento = SinedDateUtils.currentDate();
	}
	
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	public Rateio getRateio() {
		return rateio;
	}
	@DisplayName("Data do pagamento")
	@Required
	public Date getDtpagamento() {
		return dtpagamento;
	}
	@DisplayName("Gerar uma movimenta��o para cada conta a pagar")
	public Boolean getGerarmovimentacaoparacadacontapagar() {
		return gerarmovimentacaoparacadacontapagar;
	}
	@Required
	@DisplayName("Forma de pagamento")
	public Formapagamento getFormapagamento() {
		return formapagamento;
	}

	public Conta getVinculo() {
		return vinculo;
	}

	public Contatipo getContatipo() {
		return contatipo;
	}
	@DisplayName("Cheque")
	public Cheque getCheque() {
		return cheque;
	}
	public Documentoclasse getDocumentoclasse() {
		return documentoclasse;
	}	
	@DisplayName("Gerar arquivo remessa")
	public Boolean getGerararquivoremessa() {
		return gerararquivoremessa;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public Boolean getErro() {
		return erro;
	}
	@DisplayName("Hist�rico")
	@MaxLength(500)
	public String getHistorico() {
		return historico;
	}
	@MaxLength(50)
	@DisplayName("Documento")
	public String getChecknum() {
		return checknum;
	}
	public Boolean getFromRetorno() {
		return fromRetorno;
	}
	@DisplayName("Gerar arquivo de registro da baixa")
	public Boolean getGerararquivoregistrocobranca() {
		return gerararquivoregistrocobranca;
	}
	public Boolean getFromWebservice() {
		return fromWebservice;
	}
	@DisplayName("FORMA DE PAGAMENTO")
	public BancoFormapagamento getBancoformapagamento() {
		return bancoformapagamento;
	}
	public void setBancoformapagamento(BancoFormapagamento bancoformapagamento) {
		this.bancoformapagamento = bancoformapagamento;
	}
	@DisplayName("TIPO DE PAGAMENTO")
	public BancoTipoPagamento getBancotipopagamento() {
		return bancotipopagamento;
	}
	public void setBancotipopagamento(BancoTipoPagamento bancotipopagamento) {
		this.bancotipopagamento = bancotipopagamento;
	}
	@DisplayName("Gerar SISPAG")
	public Boolean getGerarsispag() {
		return gerarsispag;
	}
	public void setGerarsispag(Boolean gerarsispag) {
		this.gerarsispag = gerarsispag;
	}
	@DisplayName("FINALIDADE DA TED")
	public FinalidadeTED getFinalidadeted() {
		return finalidadeted;
	}
	public void setFinalidadeted(FinalidadeTED finalidadeted) {
		this.finalidadeted = finalidadeted;
	}
	@DisplayName("FINALIDADE DOC")
	public FinalidadeDOC getFinalidadedoc() {
		return finalidadedoc;
	}
	public void setFinalidadedoc(FinalidadeDOC finalidadedoc) {
		this.finalidadedoc = finalidadedoc;
	}
	@DisplayName("Banco")
	public Integer getBancocheque() {
		return bancocheque;
	}
	@DisplayName("Ag�ncia")
	public Integer getAgenciacheque() {
		return agenciacheque;
	}
	@DisplayName("Conta")
	public Integer getContacheque() {
		return contacheque;
	}
	@DisplayName("N�mero")
	public String getNumerocheque() {
		return numerocheque;
	}
	@DisplayName("Emitente")
	public String getEmitentecheque() {
		return emitentecheque;
	}
	@DisplayName("CNPJ/CPF do emitente")
	@MaxLength(14)
	public String getCpfcnpjcheque() {
		return cpfcnpjcheque;
	}
	@DisplayName("Bom para")
	public Date getDtbomparacheque() {
		return dtbomparacheque;
	}
	
	@DisplayName("Valor vale-compra")
	public Money getValorvalecompra() {
		return valorvalecompra;
	}
	public Money getValormaximovalecompra() {
		return valormaximovalecompra;
	}
	public Boolean getFromBaixaautomatica() {
		return fromBaixaautomatica;
	}
	public void setFromBaixaautomatica(Boolean fromBaixaautomatica) {
		this.fromBaixaautomatica = fromBaixaautomatica;
	}
	public Boolean getFromBaixamanual() {
		return fromBaixamanual;
	}
	public void setFromBaixamanual(Boolean fromBaixamanual) {
		this.fromBaixamanual = fromBaixamanual;
	}
	public void setValormaximovalecompra(Money valormaximovalecompra) {
		this.valormaximovalecompra = valormaximovalecompra;
	}
	public void setValorvalecompra(Money valorvalecompra) {
		this.valorvalecompra = valorvalecompra;
	}
	public Money getValorJuros() {
		return valorJuros;
	}
	public Money getValorRecebido() {
		return valorRecebido;
	}
	public Money getValorDescontos() {
		return valorDescontos;
	}
	public Money getValorMovimento() {
		return valorMovimento;
	}
	public void setValorJuros(Money valorJuros) {
		this.valorJuros = valorJuros;
	}
	public void setValorRecebido(Money valorRecebido) {
		this.valorRecebido = valorRecebido;
	}
	public void setValorDescontos(Money valorDescontos) {
		this.valorDescontos = valorDescontos;
	}
	public void setValorMovimento(Money valorMovimento) {
		this.valorMovimento = valorMovimento;
	}
	public void setFromWebservice(Boolean fromWebservice) {
		this.fromWebservice = fromWebservice;
	}
	public void setGerararquivoregistrocobranca(Boolean gerararquivoregistrocobranca) {
		this.gerararquivoregistrocobranca = gerararquivoregistrocobranca;
	}
	public void setFromRetorno(Boolean fromRetorno) {
		this.fromRetorno = fromRetorno;
	}
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	public void setErro(Boolean erro) {
		this.erro = erro;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setChecknum(String checknum) {
		this.checknum = checknum;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public Double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
	public Double getValortaxaTotal() {
		return valortaxaTotal;
	}
	public void setValortaxaTotal(Double valortaxaTotal) {
		this.valortaxaTotal = valortaxaTotal;
	}
	public void setGerararquivoremessa(Boolean gerararquivoremessa) {
		this.gerararquivoremessa = gerararquivoremessa;
	}
	public void setDocumentoclasse(Documentoclasse documentoclasse) {
		this.documentoclasse = documentoclasse;
	}
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}
	public void setGerarmovimentacaoparacadacontapagar(Boolean gerarmovimentacaoparacadacontapagar) {
		this.gerarmovimentacaoparacadacontapagar = gerarmovimentacaoparacadacontapagar;
	}
	public void setFormapagamento(Formapagamento formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setVinculo(Conta vinculo) {
		this.vinculo = vinculo;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	public void setBancocheque(Integer bancocheque) {
		this.bancocheque = bancocheque;
	}
	public void setAgenciacheque(Integer agenciacheque) {
		this.agenciacheque = agenciacheque;
	}
	public void setContacheque(Integer contacheque) {
		this.contacheque = contacheque;
	}
	public void setNumerocheque(String numerocheque) {
		this.numerocheque = numerocheque;
	}
	public void setEmitentecheque(String emitentecheque) {
		this.emitentecheque = emitentecheque;
	}
	public void setCpfcnpjcheque(String cpfcnpjcheque) {
		this.cpfcnpjcheque = cpfcnpjcheque;
	}
	public void setDtbomparacheque(Date dtbomparacheque) {
		this.dtbomparacheque = dtbomparacheque;
	}
	@DisplayName("")
	public List<BaixarContaBeanMovimentacao> getListaBaixarContaBeanMovimentacao() {
		return listaBaixarContaBeanMovimentacao;
	}
	public void setListaBaixarContaBeanMovimentacao(
			List<BaixarContaBeanMovimentacao> listaBaixarContaBeanMovimentacao) {
		this.listaBaixarContaBeanMovimentacao = listaBaixarContaBeanMovimentacao;
	}
	public Money getValortotalmovimentacao() {
		return valortotalmovimentacao;
	}
	public Money getValorrestante() {
		return valorrestante;
	}
	public void setValortotalmovimentacao(Money valortotalmovimentacao) {
		this.valortotalmovimentacao = valortotalmovimentacao;
	}
	public void setValorrestante(Money valorrestante) {
		this.valorrestante = valorrestante;
	}
	public Arquivo getComprovante() {
		return comprovante;
	}
	public void setComprovante(Arquivo comprovante) {
		this.comprovante = comprovante;
	}
	public Integer getCdchequecheque() {
		return cdchequecheque;
	}
	public void setCdchequecheque(Integer cdchequecheque) {
		this.cdchequecheque = cdchequecheque;
	}
	public String getChequeauxiliar() {
		return chequeauxiliar;
	}
	public void setChequeauxiliar(String chequeauxiliar) {
		this.chequeauxiliar = chequeauxiliar;
	}
	public Date getDtcredito() {
		return dtcredito;
	}
	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}
	@DisplayName("Valor")
	public Money getValorcheque() {
		return valorcheque;
	}
	public void setValorcheque(Money valorcheque) {
		this.valorcheque = valorcheque;
	}
	public Documento getDocumento(){		
		if(listaDocumento!=null && listaDocumento.size()==1)
			documento = listaDocumento.get(0);
		else
			documento = null;
		
		return documento;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	public Boolean getIsContaBaixada() {
		return isContaBaixada;
	}
	public void setIsContaBaixada(Boolean isContaBaixada) {
		this.isContaBaixada = isContaBaixada;
	}
	
	public String getWhereInMovReferente() {
		return whereInMovReferente;
	}
	public void setWhereInMovReferente(String whereInMovReferente) {
		this.whereInMovReferente = whereInMovReferente;
	}
	public Money getValorTotalMovReferente() {
		return valorTotalMovReferente;
	}
	public void setValorTotalMovReferente(Money valorTotalMovReferente) {
		this.valorTotalMovReferente = valorTotalMovReferente;
	}
	
	public Integer getSizeListaDocumento(){
		return getListaDocumento() != null ? getListaDocumento().size() : 0;
	}

	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	@DisplayName("Gerar vale compra")
	public Boolean getGerarValeCompra() {
		return gerarValeCompra;
	}
	public void setGerarValeCompra(Boolean gerarValeCompra) {
		this.gerarValeCompra = gerarValeCompra;
	}

	public Documento getDocumentoVinculado() {
		return documentoVinculado;
	}
	public void setDocumentoVinculado(Documento documentoVinculado) {
		this.documentoVinculado = documentoVinculado;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}