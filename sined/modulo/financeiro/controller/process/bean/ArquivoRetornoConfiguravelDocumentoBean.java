package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;

public class ArquivoRetornoConfiguravelDocumentoBean {

	private List<Documento> listaDocumento;
	private Documento documento;
	private Date dtcredito;
	private String dtcreditoStr;
	private Money valor;
	private String nossonumero;
	private Boolean marcado;
	private String agenciacedente;
	private String contacedente;
	private Money valorTarifa;
	private String motivoTarifa;
	private String motivoTarifaDesc;
	private String codigoinstrucao;
	
	private Boolean baixa;
	private Boolean confirmacao;
	
	private Boolean valordiferente;
	
	
	public Documento getDocumento() {
		return documento;
	}
	public Date getDtcredito() {
		return dtcredito;
	}
	public Money getValor() {
		return valor;
	}
	public String getNossonumero() {
		return nossonumero;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public String getAgenciacedente() {
		return agenciacedente;
	}
	public String getContacedente() {
		return contacedente;
	}
	public Money getValorTarifa() {
		return valorTarifa;
	}
	public String getMotivoTarifa() {
		return motivoTarifa;
	}
	public String getMotivoTarifaDesc() {
		return motivoTarifaDesc;
	}
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	public Boolean getConfirmacao() {
		return confirmacao;
	}
	public String getCodigoinstrucao() {
		return codigoinstrucao;
	}
	public Boolean getBaixa() {
		return baixa;
	}
	public String getDtcreditoStr() {
		return dtcreditoStr;
	}
	public void setDtcreditoStr(String dtcreditoStr) {
		this.dtcreditoStr = dtcreditoStr;
	}
	public void setBaixa(Boolean baixa) {
		this.baixa = baixa;
	}
	public void setCodigoinstrucao(String codigoinstrucao) {
		this.codigoinstrucao = codigoinstrucao;
	}
	public void setConfirmacao(Boolean confirmacao) {
		this.confirmacao = confirmacao;
	}
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setAgenciacedente(String agenciacedente) {
		this.agenciacedente = agenciacedente;
	}
	public void setContacedente(String contacedente) {
		this.contacedente = contacedente;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}
	public void setValorTarifa(Money valorTarifa) {
		this.valorTarifa = valorTarifa;
	}
	public void setMotivoTarifa(String motivoTarifa) {
		this.motivoTarifa = motivoTarifa;
	}
	public void setMotivoTarifaDesc(String motivoTarifaDesc) {
		this.motivoTarifaDesc = motivoTarifaDesc;
	}
	
	public Boolean getValordiferente() {
		valordiferente = Boolean.FALSE;
		
		if(this.getValor() != null && this.getDocumento() != null && 
				this.getDocumento().getAux_documento() != null && 
				this.getDocumento().getAux_documento().getValoratual() != null && 
				this.getValor().compareTo(this.getDocumento().getAux_documento().getValoratual()) != 0){
			valordiferente = Boolean.TRUE;
		}
		
		return valordiferente;
	}
	
	
	public void setValordiferente(Boolean valordiferente) {
		this.valordiferente = valordiferente;
	}
}
