package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

public enum BoletoDigitalRetornoAcaoEnum {

	BAIXAR("Baixar"),
	CANCELAR("Cancelar"),
	;
		
	private String nome;
		
	private BoletoDigitalRetornoAcaoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
		
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}
	
	public Integer getOrdinal() {
		return this.ordinal();
	}
}
