package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadora;

public class ArquivoConciliacaoOperadoraBean {
	private Arquivo arquivo;
	private String texto;
	
	public Arquivo getArquivo() {
		return arquivo;
	}

	public String getArquivoTexto(){
		texto = new String(getArquivo().getContent());
		return texto;
	}
	
	public String getArquivoTextoHeader(){
		return getArquivoTexto().substring(EnumArquivoConciliacaoOperadora.HEADER.getInicio(), EnumArquivoConciliacaoOperadora.HEADER.getFim());
	}
	
	public String getArquivoTextoHeaderOpcaoExtrato(){
		return getArquivoTextoHeader().substring(EnumArquivoConciliacaoOperadora.OPCAO_DO_EXTRATO.getInicio(), EnumArquivoConciliacaoOperadora.OPCAO_DO_EXTRATO.getFim());
	}
	
	public String getArquivoTextoParte(Integer inicio, Integer fim){
		return getArquivoTexto().substring(inicio, fim);
	}
	
	public String getArquivoTextoParte(Integer tamanho){
		return getArquivoTexto().substring(tamanho);
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
}
