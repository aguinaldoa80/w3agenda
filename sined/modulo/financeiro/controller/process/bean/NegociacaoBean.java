package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Taxa;


public class NegociacaoBean{
	
	protected String documentos;
	protected Money valor;
	protected Prazopagamento prazopagamento;
	protected Indicecorrecao indicecorrecao;
	protected List<Documento> parcelas;
	protected Date dtreferencia;
	protected Prazopagamento prazoescolhido;
	protected Boolean dtemissaoatual = Boolean.TRUE;
	protected Boolean checktaxa;
	protected Taxa taxa;
	protected String mensagem2;
	protected String mensagem3;
	protected Boolean fromVenda = Boolean.FALSE;
	protected Integer codigo;
	protected Documentoclasse documentoclasse;
	protected Conta conta;
	protected Contacarteira contacarteira;
		
	public String getDocumentos() {
		return documentos;
	}
	public Money getValor() {
		return valor;
	}
	@DisplayName("Prazo de pagamento")
	@Required
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	@DisplayName("�ndice de corre��o")
	public Indicecorrecao getIndicecorrecao() {
		return indicecorrecao;
	}
	public List<Documento> getParcelas() {
		return parcelas;
	}
	@DisplayName("Data de emiss�o como data atual")
	public Boolean getDtemissaoatual() {
		return dtemissaoatual;
	}
	public void setDtemissaoatual(Boolean dtemissaoatual) {
		this.dtemissaoatual = dtemissaoatual;
	}
	public void setDocumentos(String documentos) {
		this.documentos = documentos;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setIndicecorrecao(Indicecorrecao indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}
	public void setParcelas(List<Documento> parcelas) {
		this.parcelas = parcelas;
	}	

	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	@DisplayName("Refer�ncia")
	public Date getDtreferencia() {
		return dtreferencia;
	}
	
	public void setDtreferencia(Date dtreferencia) {
		this.dtreferencia = dtreferencia;
	}
	
	public Prazopagamento getPrazoescolhido() {
		return prazoescolhido;
	}
	
	public void setPrazoescolhido(Prazopagamento prazoescolhido) {
		this.prazoescolhido = prazoescolhido;
	}
	
	public Boolean getChecktaxa() {
		return checktaxa;
	}
	public void setChecktaxa(Boolean checktaxa) {
		this.checktaxa = checktaxa;
	}
	public Taxa getTaxa() {
		return taxa;
	}
	public void setTaxa(Taxa taxa) {
		this.taxa = taxa;
	}
	public String getMensagem2() {
		return mensagem2;
	}
	public String getMensagem3() {
		return mensagem3;
	}
	public void setMensagem2(String mensagem2) {
		this.mensagem2 = mensagem2;
	}
	public void setMensagem3(String mensagem3) {
		this.mensagem3 = mensagem3;
	}
	public Boolean getFromVenda() {
		return fromVenda;
	}
	public void setFromVenda(Boolean fromVenda) {
		this.fromVenda = fromVenda;
	}
	public Documentoclasse getDocumentoclasse() {
		return documentoclasse;
	}
	public void setDocumentoclasse(Documentoclasse documentoclasse) {
		this.documentoclasse = documentoclasse;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
}