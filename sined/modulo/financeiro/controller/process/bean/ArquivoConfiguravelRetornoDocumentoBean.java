package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetorno;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoCampo;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoOcorrencia;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoRejeicao;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmentoDetalhe;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoAcaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoOperacaoEnum;
import br.com.linkcom.sined.util.SinedUtil;

public class ArquivoConfiguravelRetornoDocumentoBean {

	private Documento documento;
	private List<Documento> listaDocumento = new ListSet<Documento>(Documento.class);
	
	private Integer sequencial;
	private String sequencialArquivo;	
	private BancoConfiguracaoRetornoAcaoEnum acao;
	private Documentoacao situacao;
	private String codocorrencia;
	private String ocorrencia;
	private String nossonumero;
	private String usoempresa;
	private String nomesacado;
	private Date dtvencimento;
	private Date dtpagamento;
	private Date dtpagamentoAgrupado;
	private Date dtocorrencia;
	private Date dtcredito;
	private Money valor;
	private Money valordesconto;
	private Money valoroutrodesconto;
	private Money valoracrecimo;
	private Money valorjuros;
	private Money valormulta;
	private Money valorpago;
	private String motivorejeicao;
	private String mensagem;
	private String historico;
	private Boolean marcado;
	private String motivotarifa;
	private Money valortarifa;
	private Boolean agrupado = Boolean.FALSE;
	private Boolean valordiferente = Boolean.FALSE;
	private Boolean exibirBotaoGerarContaReceber = Boolean.FALSE;
	private boolean addJurosMulta;
	
	private BancoConfiguracaoRetornoOperacaoEnum operacao;
	private BancoConfiguracaoRetornoAcaoEnum acaoValidacao;
	private BancoConfiguracaoRetornoAcaoEnum acaoOriginal;
	
	public ArquivoConfiguravelRetornoDocumentoBean() {}
	public ArquivoConfiguravelRetornoDocumentoBean(ArquivoConfiguravelRetornoDocumentoBean outro){
		this.sequencial = outro.sequencial;
		this.sequencialArquivo = outro.sequencialArquivo;
		this.acao = outro.acao;
		this.situacao = outro.situacao;
		this.codocorrencia = outro.codocorrencia;
		this.ocorrencia = outro.ocorrencia;
		this.nossonumero = outro.nossonumero;
		this.usoempresa = outro.usoempresa;
		this.nomesacado = outro.nomesacado;
		this.dtvencimento = outro.dtvencimento;
		this.dtpagamento = outro.dtpagamento;
		this.dtpagamentoAgrupado = outro.dtpagamentoAgrupado;
		this.dtocorrencia = outro.dtocorrencia;
		this.dtcredito = outro.dtcredito;
		this.valor = outro.valor;
		this.valordesconto = outro.valordesconto;
		this.valoroutrodesconto = outro.valoroutrodesconto;
		this.valoracrecimo = outro.valoracrecimo;
		this.valorjuros = outro.valorjuros;
		this.valormulta = outro.valormulta;
		this.valorpago = outro.valorpago;
		this.motivorejeicao = outro.motivorejeicao;
		this.mensagem = outro.mensagem;
		this.historico = outro.historico;
		this.marcado = outro.marcado;
		this.motivotarifa = outro.motivotarifa;
		this.valortarifa = outro.valortarifa;
		this.agrupado = outro.agrupado;
		this.valordiferente = outro.valordiferente;
		this.exibirBotaoGerarContaReceber = outro.exibirBotaoGerarContaReceber;
		this.addJurosMulta = outro.addJurosMulta;
	}
	
	public Documento getDocumento() {
		return documento;
	}
	
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	
	public Integer getSequencial() {
		return sequencial;
	}
	
	public String getSequencialArquivo() {
		return sequencialArquivo;
	}

	public BancoConfiguracaoRetornoAcaoEnum getAcao() {
		return acao;
	}

	public Documentoacao getSituacao() {
		return situacao;
	}

	public String getCodocorrencia() {
		return codocorrencia;
	}
	
	public String getOcorrencia() {
		return ocorrencia;
	}

	public String getNossonumero() {
		return nossonumero;
	}

	public String getUsoempresa() {
		return usoempresa;
	}

	public String getNomesacado() {
		return nomesacado;
	}

	public Date getDtvencimento() {
		return dtvencimento;
	}

	public Date getDtpagamento() {
		return dtpagamento;
	}
	
	public Date getDtpagamentoAgrupado() {
		return dtpagamentoAgrupado;
	}

	public Date getDtocorrencia() {
		return dtocorrencia;
	}

	public Date getDtcredito() {
		return dtcredito;
	}

	public Money getValor() {
		return valor;
	}

	public Money getValordesconto() {
		return valordesconto;
	}
	
	public Money getValoroutrodesconto() {
		return valoroutrodesconto;
	}

	public Money getValoracrecimo() {
		return valoracrecimo;
	}

	public Money getValorjuros() {
		return valorjuros;
	}

	public Money getValormulta() {
		return valormulta;
	}

	public Money getValorpago() {
		return valorpago;
	}
	
	public String getMotivorejeicao() {
		return motivorejeicao;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public String getHistorico() {
		return historico;
	}

	public Boolean getMarcado() {
		return marcado;
	}
	
	public String getMotivotarifa() {
		return motivotarifa;
	}
	
	public Money getValortarifa() {
		return valortarifa;
	}
	
	public BancoConfiguracaoRetornoOperacaoEnum getOperacao() {
		return operacao;
	}
	
	public Boolean getAgrupado() {
		return agrupado;
	}
	
	public Boolean getValordiferente() {
		return valordiferente;
	}
	
	public void setValordiferente(Boolean valordiferente) {
		this.valordiferente = valordiferente;
	}
	
	public void setAgrupado(Boolean agrupado) {
		this.agrupado = agrupado;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	
	public void setSequencialArquivo(String sequencialArquivo) {
		this.sequencialArquivo = sequencialArquivo;
	}

	public void setAcao(BancoConfiguracaoRetornoAcaoEnum acao) {
		this.acao = acao;
	}

	public void setSituacao(Documentoacao situacao) {
		this.situacao = situacao;
	}

	public void setCodocorrencia(String codocorrencia) {
		this.codocorrencia = codocorrencia;
	}
	
	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}

	public void setUsoempresa(String usoempresa) {
		this.usoempresa = usoempresa;
	}

	public void setNomesacado(String nomesacado) {
		this.nomesacado = nomesacado;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}
	
	public void setDtpagamentoAgrupado(Date dtpagamentoAgrupado) {
		this.dtpagamentoAgrupado = dtpagamentoAgrupado;
	}

	public void setDtocorrencia(Date dtocorrencia) {
		this.dtocorrencia = dtocorrencia;
	}

	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	
	public void setValoroutrodesconto(Money valoroutrodesconto) {
		this.valoroutrodesconto = valoroutrodesconto;
	}

	public void setValoracrecimo(Money valoracrecimo) {
		this.valoracrecimo = valoracrecimo;
	}

	public void setValorjuros(Money valorjuros) {
		this.valorjuros = valorjuros;
	}

	public void setValormulta(Money valormulta) {
		this.valormulta = valormulta;
	}

	public void setValorpago(Money valorpago) {
		this.valorpago = valorpago;
	}
	
	public void setMotivorejeicao(String motivorejeicao) {
		this.motivorejeicao = motivorejeicao;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	
	public void setMotivotarifa(String motivotarifa) {
		this.motivotarifa = motivotarifa;
	}
	
	public void setValortarifa(Money valortarifa) {
		this.valortarifa = valortarifa;
	}
	
	public void setOperacao(BancoConfiguracaoRetornoOperacaoEnum operacao) {
		this.operacao = operacao;
	}
	
	public Long getNossonumeroLong(){
		Long nossoNumeroLong = null;
		
		if (nossonumero != null && !"".equals(nossonumero.trim())){
			try {
				nossoNumeroLong = Long.parseLong(nossonumero.trim());
			} catch (Exception e) {}
		}
		
		return nossoNumeroLong;
	}

	public Long getUsoempresaLong(){
		Long usoEmpresaLong = null;
		
		if (usoempresa != null && !"".equals(usoempresa.trim())){
			try {
				usoEmpresaLong = Long.parseLong(usoempresa.trim());
			} catch (Exception e) {}
		}
		
		return usoEmpresaLong;
	}
	
	public void preparaMultisegmento(String linha, BancoConfiguracaoRetorno conf){
		if (conf.getListaBancoConfiguracaoRetornoSegmentoDetalhe() != null && !conf.getListaBancoConfiguracaoRetornoSegmentoDetalhe().isEmpty()){
			Date datapagamento = null;
			String codOcorrencia = null;
			for(BancoConfiguracaoRetornoSegmentoDetalhe segmento : conf.getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
					if(!conf.isLinhaContainsIdentificador2(linha, segmento.getBancoConfiguracaoRetornoSegmento()))continue;
						for(BancoConfiguracaoRetornoCampo campo : segmento.getListaBancoConfiguracaoRetornoCampo()){
							this.preencheCampos(linha, campo, conf, datapagamento, codOcorrencia);
					}
					
				
			}
			Integer tamanhoCodigo = 2;
			for (BancoConfiguracaoRetornoRejeicao mensagem : conf.getListaBancoConfiguracaoRetornoRejeicao()){
				if (mensagem.getCodigo() != null && mensagem.getCodigo().length() > 0){
					tamanhoCodigo = mensagem.getCodigo().length();
					break;
				}
			}
			for(BancoConfiguracaoRetornoSegmentoDetalhe segmento : conf.getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
				if(!conf.isLinhaContainsIdentificador2(linha, segmento.getBancoConfiguracaoRetornoSegmento()))continue;
					for(BancoConfiguracaoRetornoCampo campo : segmento.getListaBancoConfiguracaoRetornoCampo()){
						this.preencheCampos2(linha, campo, conf, tamanhoCodigo, datapagamento, codOcorrencia);
				}
			}		
			
		}
	}
	public void preencheCampos(String linha, BancoConfiguracaoRetornoCampo campo, BancoConfiguracaoRetorno conf,Date datapagamento,String codOcorrencia){
			switch (campo.getCampo()) {
			case CODIGO_OCORRENCIA: 
				codOcorrencia = campo.getValorString(linha);
				if(codOcorrencia != null && conf != null && conf.getListaBancoConfiguracaoRetornoOcorrencia() != null){
					for (BancoConfiguracaoRetornoOcorrencia ocorrencia : conf.getListaBancoConfiguracaoRetornoOcorrencia()){
						if (ocorrencia.getCodigo() != null && ocorrencia.getCodigo().equals(codOcorrencia)){
							setAcaoValidacao(ocorrencia.getAcao());
							break;
						}
					}
				}
				break;
			case DATA_PAGAMENTO: 
				datapagamento = campo.getValorDate(linha, false);
				break;
			default:
				break;
		}
		
	}
	
	
	public void preencheCampos2(String linha, BancoConfiguracaoRetornoCampo campo, BancoConfiguracaoRetorno conf , Integer tamanhoCodigo,Date datapagamento,String codOcorrencia){
		switch (campo.getCampo()) {
		case NUMERO_SEQUENCIAL: 
			setSequencialArquivo(campo.getValorString(linha)); 
			break;
		case NOME_SACADO: 
			setNomesacado( campo.getValorString(linha)); 
			break;
		case DATA_PAGAMENTO: 
			setDtpagamento(campo.getValorDate(linha)); 
			break;
		case DATA_CREDITO: 
			setDtcredito(campo.getValorDate(linha, (BancoConfiguracaoRetornoAcaoEnum.BAIXAR.equals(acaoValidacao) && datapagamento == null))); 
			break;
		case DATA_OCORRENCIA: 
			setDtocorrencia(campo.getValorDate(linha, !BancoConfiguracaoRetornoAcaoEnum.CANCELAR.equals(acaoValidacao))); 
			break;
		case DATA_VENCIMENTO: 
			setDtvencimento(campo.getValorDate(linha, (BancoConfiguracaoRetornoAcaoEnum.ATUALIZAR_VENCIMENTO.equals(acaoValidacao) || BancoConfiguracaoRetornoAcaoEnum.ATUALIZAR_VENCIMENTO_DATALIMITE_JUROS_MULTA.equals(acaoValidacao)))); 
			break;
		case VALOR_TITULO: 
			setValor(campo.getValorMoney(linha)); 
			break;
		case VALOR_DESCONTO: 
			setValordesconto(campo.getValorMoney(linha)); 
			break;
		case VALOR_OUTRODESCONTO: 
			setValoroutrodesconto(campo.getValorMoney(linha)); 
			break;	
		case VALOR_JUROS: 
			setValorjuros(campo.getValorMoney(linha)); 
			break;
		case VALOR_MULTA: 
			setValormulta(campo.getValorMoney(linha)); 
			break;
		case VALOR_ACRESCIMO: 
			setValoracrecimo(campo.getValorMoney(linha)); 
			break;
		case VALOR_PAGO:
			if (campo.isCalculado()){
				Money total = SinedUtil.zeroIfNull(getValor())
								.subtract(SinedUtil.zeroIfNull(getValordesconto()))
								.subtract(SinedUtil.zeroIfNull(getValoroutrodesconto()))
								.add(SinedUtil.zeroIfNull(getValoracrecimo()))
								.add(SinedUtil.zeroIfNull(getValorjuros()))
								.add(SinedUtil.zeroIfNull(getValormulta()));
				setValorpago(total);
			} else						
				setValorpago(campo.getValorMoney(linha)); 
			break;						
		case NOSSO_NUMERO: 
			setNossonumero(campo.getValorString(linha)); 
			break;
		case USO_EMPRESA: 
			setUsoempresa(campo.getValorString(linha)); 
			break;
		case CODIGO_OCORRENCIA: 
			setCodocorrencia(campo.getValorString(linha)); 
			for (BancoConfiguracaoRetornoOcorrencia ocorrencia : conf.getListaBancoConfiguracaoRetornoOcorrencia()){
				if (ocorrencia.getCodigo() != null && ocorrencia.getCodigo().equals(getCodocorrencia())){
					setAcao(ocorrencia.getAcao());
					setOcorrencia(ocorrencia.getDescricao());
					setHistorico(ocorrencia.getHistorico());
					break;
				}
			}
			break;
		case MOTIVO_REJEICAO: 
			if (BancoConfiguracaoRetornoAcaoEnum.REJEITADO.equals(getAcao())){
				setMotivorejeicao(campo.getValorString(linha));
				if(getMotivorejeicao() != null){
					StringBuilder sb = new StringBuilder();
					for(int i = 0; i < getMotivorejeicao().length()-1; i+= tamanhoCodigo){
						if(getMotivorejeicao().length() >= i+tamanhoCodigo){
							for (BancoConfiguracaoRetornoRejeicao motivorejeicao : conf.getListaBancoConfiguracaoRetornoRejeicao()){
								if (motivorejeicao.getCodigo() != null && motivorejeicao.getCodigo().equals(getMotivorejeicao().substring(i, i+tamanhoCodigo)) &&
									(StringUtils.isBlank(motivorejeicao.getOcorrencia()) || motivorejeicao.getOcorrencia().equals(codOcorrencia))) {
									if(!sb.toString().contains(motivorejeicao.getDescricao())){
										sb.append(motivorejeicao.getDescricao()).append("\n");
									}
									break;
								}
							}
						}
					}
					setMotivorejeicao(sb.length() > 0 ? sb.substring(0, sb.length()-1) : "");
				}
				if (getMotivorejeicao() == null)
					setMotivorejeicao("Documento rejeitado via arquivo de retorno. Motivo n�o identificado.");
			}
			break;
		case MENSAGEM: 
			setMensagem(campo.getValorString(linha));
			if(getMensagem() != null){
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < getMensagem().length()-1; i+= tamanhoCodigo){
					if(getMensagem().length() >= i+2){
						for (BancoConfiguracaoRetornoRejeicao mensagem : conf.getListaBancoConfiguracaoRetornoRejeicao()){
							if (mensagem.getCodigo() != null && mensagem.getCodigo().equals(getMensagem().substring(i, i+2)) &&
								(StringUtils.isBlank(mensagem.getOcorrencia()) || mensagem.getOcorrencia().equals(codOcorrencia))) {
								if(!sb.toString().contains(mensagem.getDescricao())){
									sb.append(mensagem.getDescricao()).append("\n");
								}
								break;
							}
						}
					}
				}
				setMensagem(sb.length() > 0 ? sb.substring(0, sb.length()-1) : "");
			}
			break;
		case MOTIVO_TARIFA:
			setMotivotarifa(campo.getValorString(linha));
			break;
		case VALOR_TARIFA:
			setValortarifa(campo.getValorMoney(linha));
			break;					
	default:
		break;
	}	
	
	if (campo.getMsgEstrutura()!=null){
		if (conf.getMsgEstrutura()==null){
			conf.setMsgEstrutura(campo.getMsgEstrutura());
		}else if (!conf.getMsgEstrutura().contains(campo.getMsgEstrutura())){
			conf.setMsgEstrutura(conf.getMsgEstrutura() + campo.getMsgEstrutura());						
		}
	}
		
		
	}
	
	public void processaLinha(String linha, BancoConfiguracaoRetorno conf) throws Exception{
		
		if (conf.getListaBancoConfiguracaoRetornoCampo() != null && !conf.getListaBancoConfiguracaoRetornoCampo().isEmpty()){
			Date datapagamento = null;
			String codOcorrencia = null;
	
			for (BancoConfiguracaoRetornoCampo bcrc : conf.getListaBancoConfiguracaoRetornoCampo()){
				if (!conf.isLinhaContainsIdentificador(linha, bcrc.getBancoConfiguracaoRetornoSegmento())) continue;	
				this.preencheCampos(linha, bcrc, conf, datapagamento, codOcorrencia);
			}
			
			Integer tamanhoCodigo = 2;
			for (BancoConfiguracaoRetornoRejeicao mensagem : conf.getListaBancoConfiguracaoRetornoRejeicao()){
				if (mensagem.getCodigo() != null && mensagem.getCodigo().length() > 0){
					tamanhoCodigo = mensagem.getCodigo().length();
					break;
				}
			}
			
			for (BancoConfiguracaoRetornoCampo bcrc : conf.getListaBancoConfiguracaoRetornoCampo()){
				
				if (!conf.isLinhaContainsIdentificador(linha, bcrc.getBancoConfiguracaoRetornoSegmento()))
					continue;				
				
				this.preencheCampos2(linha, bcrc, conf, tamanhoCodigo, datapagamento, codOcorrencia);
			}			
		}		
	}
	
	public void addMensagem(String mensagem){
		if((getMensagem() != null && mensagem != null && getMensagem().contains(mensagem)) || mensagem == null){
			return;
		}
		
		if (getMensagem() == null)
			setMensagem("");
		if (getMensagem() != null && !"".equals(getMensagem().trim()))
			setMensagem(getMensagem().concat("\n"));
		setMensagem(getMensagem().concat(mensagem));		
	}
	
	public Money getValorjurosmulta(){
		Money jurosmulta = new Money(0d);
		if (getValormulta() != null)
			jurosmulta = jurosmulta.add(getValormulta());
		if (getValorjuros() != null)
			jurosmulta = jurosmulta.add(getValorjuros());
		if (getValoracrecimo() != null)
			jurosmulta = jurosmulta.add(getValoracrecimo());
		return jurosmulta;
	}
	
	public BancoConfiguracaoRetornoAcaoEnum getAcaoValidacao() {
		return acaoValidacao;
	}
	public void setAcaoValidacao(BancoConfiguracaoRetornoAcaoEnum acaoValidacao) {
		this.acaoValidacao = acaoValidacao;
	}

	public Boolean getExibirBotaoGerarContaReceber() {
		return exibirBotaoGerarContaReceber;
	}
	public void setExibirBotaoGerarContaReceber(Boolean exibirBotaoGerarContaReceber) {
		this.exibirBotaoGerarContaReceber = exibirBotaoGerarContaReceber;
	}
	
	public Money getValorDescontoOutroDesconto(){
		return SinedUtil.zeroIfNull(getValordesconto()).add(SinedUtil.zeroIfNull(getValoroutrodesconto()));
	}
	
	public boolean isAddJurosMulta() {
		return addJurosMulta;
	}
	
	public void setAddJurosMulta(boolean addJurosMulta) {
		this.addJurosMulta = addJurosMulta;
	}
	
	public BancoConfiguracaoRetornoAcaoEnum getAcaoOriginal() {
		return acaoOriginal;
	}
	public void setAcaoOriginal(BancoConfiguracaoRetornoAcaoEnum acaoOriginal) {
		this.acaoOriginal = acaoOriginal;
	}
}