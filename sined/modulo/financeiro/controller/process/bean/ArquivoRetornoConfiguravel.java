package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Bancoretorno;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoOcorrenciaTarifa;
import br.com.linkcom.sined.geral.service.ContacorrenteService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.DocumentoService;

public class ArquivoRetornoConfiguravel {
	
	private Bancoretorno conf;
	private Integer cdbanco;
	private String[] linha;
	
	private String agencia_cedente;
	private String conta_cedente;
	
	private Conta conta;
	private List<ArquivoRetornoConfiguravelDocumentoBean> lista;
	
	public Conta getConta() {
		return conta;
	}
	
	public List<ArquivoRetornoConfiguravelDocumentoBean> getLista() {
		return lista;
	}
	
	public String getAgencia_cedente() {
		return agencia_cedente;
	}

	public String getConta_cedente() {
		return conta_cedente;
	}

	public void setAgencia_cedente(String agencia_cedente) {
		this.agencia_cedente = agencia_cedente;
	}

	public void setConta_cedente(String conta_cedente) {
		this.conta_cedente = conta_cedente;
	}

	public void setLista(List<ArquivoRetornoConfiguravelDocumentoBean> lista) {
		this.lista = lista;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public ArquivoRetornoConfiguravel(Bancoretorno conf, Integer cdbanco, String[] linha) {
		this.conf = conf;
		this.cdbanco = cdbanco;
		this.linha = linha;
		this.lista = new ArrayList<ArquivoRetornoConfiguravelDocumentoBean>();
	}

	public void analisaCabecalho(String linha){
		if(conf.getAgenciacontacabecalho() != null && conf.getAgenciacontacabecalho()) {
			agencia_cedente = linha.substring(conf.getAgenciacedenteposicaoinicial(), conf.getAgenciacedenteposicaoinicial() + conf.getAgenciacedentetamanho());
			conta_cedente = linha.substring(conf.getContacedenteposicaoinicial(), conf.getContacedenteposicaoinicial() + conf.getContacedentetamanho());
			
			this.conta = ContacorrenteService.getInstance().findForRetorno(cdbanco.toString(), agencia_cedente, conta_cedente);
		}
	}
	
	public void analisaLinha(String linha, String linhaSegunda) throws ParseException {
		
		ArquivoRetornoConfiguravelDocumentoBean bean = new ArquivoRetornoConfiguravelDocumentoBean();
		String dtcreditoStr, nossonumero, valorStr, valorTarifaStr = null, motivoTarifa = null, codigoInstrucao;
		
		if((conf.getAgenciacontacabecalho() == null || !conf.getAgenciacontacabecalho()) && this.conta == null) {
			
			// AG�NCIA
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getAgenciacedenteprimeiralinha() != null && !conf.getAgenciacedenteprimeiralinha()){
				agencia_cedente = linhaSegunda.substring(conf.getAgenciacedenteposicaoinicial(), conf.getAgenciacedenteposicaoinicial() + conf.getAgenciacedentetamanho());
			} else {
				agencia_cedente = linha.substring(conf.getAgenciacedenteposicaoinicial(), conf.getAgenciacedenteposicaoinicial() + conf.getAgenciacedentetamanho());
			}
			
			// CONTA
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getContacedenteprimeiralinha() != null && !conf.getContacedenteprimeiralinha()){
				conta_cedente = linhaSegunda.substring(conf.getContacedenteposicaoinicial(), conf.getContacedenteposicaoinicial() + conf.getContacedentetamanho());
			} else {
				conta_cedente = linha.substring(conf.getContacedenteposicaoinicial(), conf.getContacedenteposicaoinicial() + conf.getContacedentetamanho());
			}
			
			this.conta = ContacorrenteService.getInstance().findForRetorno(cdbanco.toString(), agencia_cedente, conta_cedente);
		}
		
		// DATA DE CR�DITO
		if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getDatacreditoprimeiralinha() != null && !conf.getDatacreditoprimeiralinha()){
			dtcreditoStr = linhaSegunda.substring(conf.getDatacreditoposicaoinicial(), conf.getDatacreditoposicaoinicial() + conf.getDatacreditotamanho());
		} else {
			dtcreditoStr = linha.substring(conf.getDatacreditoposicaoinicial(), conf.getDatacreditoposicaoinicial() + conf.getDatacreditotamanho());
		}
		dtcreditoStr = dtcreditoStr.trim();
		Date dtcredito = null;
		if(dtcreditoStr != null && !dtcreditoStr.equals("")){
			dtcredito = new Date(new SimpleDateFormat(conf.getFormatodata()).parse(dtcreditoStr).getTime());
		}
		bean.setDtcreditoStr(dtcreditoStr);
		bean.setDtcredito(dtcredito);
		
		// NOSSO N�MERO
		if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getNossonumeroprimeiralinha() != null && !conf.getNossonumeroprimeiralinha()){
			nossonumero = linhaSegunda.substring(conf.getNossonumeroposicaoinicial(), conf.getNossonumeroposicaoinicial() + conf.getNossonumerotamanho());
		} else {
			nossonumero = linha.substring(conf.getNossonumeroposicaoinicial(), conf.getNossonumeroposicaoinicial() + conf.getNossonumerotamanho());
		}
		nossonumero = nossonumero.trim();
		bean.setNossonumero(nossonumero);
		
		// VALOR COBRADO
		if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getValorcobradoprimeiralinha() != null && !conf.getValorcobradoprimeiralinha()){
			valorStr = linhaSegunda.substring(conf.getValorcobradoposicaoinicial(), conf.getValorcobradoposicaoinicial() + conf.getValorcobradotamanho()); 
		} else {
			valorStr = linha.substring(conf.getValorcobradoposicaoinicial(), conf.getValorcobradoposicaoinicial() + conf.getValorcobradotamanho()); 
		}
		
		// VALOR TARIFA
		if(conf.getValortarifaposicaoinicial() != null &&  conf.getValortarifatamanho() != null){
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getValortarifaprimeiralinha() != null && !conf.getValortarifaprimeiralinha()){
				valorTarifaStr = linhaSegunda.substring(conf.getValortarifaposicaoinicial(), conf.getValortarifaposicaoinicial() + conf.getValortarifatamanho()); 
			} else {
				valorTarifaStr = linha.substring(conf.getValortarifaposicaoinicial(), conf.getValortarifaposicaoinicial() + conf.getValortarifatamanho()); 
			}
		}
		
		// MOTIVO TARIFA
		if(conf.getMotivotarifaposicaoinicial() != null &&  conf.getMotivotarifatamanho() != null){
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getMotivotarifaprimeiralinha() != null && !conf.getMotivotarifaprimeiralinha()){
				motivoTarifa = linhaSegunda.substring(conf.getMotivotarifaposicaoinicial(), conf.getMotivotarifaposicaoinicial() + conf.getMotivotarifatamanho()); 
			} else {
				motivoTarifa = linha.substring(conf.getMotivotarifaposicaoinicial(), conf.getMotivotarifaposicaoinicial() + conf.getMotivotarifatamanho()); 
			} 
		}
		
		if(motivoTarifa != null){
			bean.setMotivoTarifa(motivoTarifa);
			bean.setMotivoTarifaDesc(descricaoMotivoTarifa(motivoTarifa));
		}
		
		// CONFIRMA��O DE ENTRADA
		if(conf.getCodigoconfirmacaoentrada() != null && !conf.getCodigoconfirmacaoentrada().equals("") &&
				conf.getCodigoinstrucaotamanho() != null && conf.getCodigoinstrucaoposicaoinicial() != null){
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getCodigoinstrucaoprimeiralinha() != null && !conf.getCodigoinstrucaoprimeiralinha()){
				codigoInstrucao = linhaSegunda.substring(conf.getCodigoinstrucaoposicaoinicial(), conf.getCodigoinstrucaoposicaoinicial() + conf.getCodigoinstrucaotamanho()); 
			} else {
				codigoInstrucao = linha.substring(conf.getCodigoinstrucaoposicaoinicial(), conf.getCodigoinstrucaoposicaoinicial() + conf.getCodigoinstrucaotamanho()); 
			} 
			
			bean.setCodigoinstrucao(codigoInstrucao);
			
			String[] codigoconfirmacaoEntrada = conf.getCodigoconfirmacaoentrada().split(",");
			List<String> listacodigoconfirmacao = Arrays.asList(codigoconfirmacaoEntrada);
			if(listacodigoconfirmacao != null && listacodigoconfirmacao.size() > 0 &&
					listacodigoconfirmacao.contains(codigoInstrucao)){
				bean.setConfirmacao(Boolean.TRUE);
			} else {
				bean.setConfirmacao(Boolean.FALSE);
			}
		} else {
			bean.setConfirmacao(Boolean.FALSE);
		}
		
		valorStr = valorStr.trim();
		Money valor = null;
		if(valorStr != null && !valorStr.equals("")){
			valor = new Money(Integer.parseInt(valorStr), true);
		}
		bean.setValor(valor);
		
		if(valorTarifaStr != null){
			valorTarifaStr = valorTarifaStr.trim();
			Money valorTarifa = null;
			if(valorTarifaStr != null && !valorTarifaStr.equals("")){
				valorTarifa = new Money(Integer.parseInt(valorTarifaStr), true);
			}
			
			bean.setValorTarifa(valorTarifa);
		}
		
		// CARREGAR O DOCUMENTO A PARTIR DO NOSSO N�MERO
//		Integer cddocumento = Integer.parseInt(nossonumero.trim());
//		Documento doc = ContareceberService.getInstance().loadForRetorno(new Documento(cddocumento));
//		if(doc != null){
//			doc.setValoratual(DocumentoService.getInstance().getValorAtual(doc.getCddocumento(), bean.getDtcredito()));
//		}
//		
//		bean.setDocumento(doc);
		
		if(nossonumero == null || nossonumero.trim().equals("")){
			return;
		}
		
		Integer nossonumeroConvertido = null;
		try {
			nossonumeroConvertido = Integer.parseInt(nossonumero.trim());
		} catch (Exception e) {}
		
		boolean incluirDoc = false;
		List<Documento> listaDoc = ContareceberService.getInstance().findForRetornoConfiguravel(nossonumero.trim(), this.getConta());
		List<Documento> listaDocCorreta = new ArrayList<Documento>();
		if(listaDoc != null && listaDoc.size() > 0){
			if(listaDoc.size() == 1){
				Documento doc = listaDoc.get(0);
				if(doc.getSomentenossonumero() == null || !doc.getSomentenossonumero() ){
					incluirDoc = true;
				}else if(doc.getSomentenossonumero() && nossonumeroConvertido == null){
					incluirDoc = true;
				}else if(doc.getSomentenossonumero() && nossonumeroConvertido != null && 
						doc.getNossonumero() != null && doc.getNossonumero().equals(nossonumero)){
					incluirDoc = true;
				}
				if(incluirDoc){
					doc.setValoratual(DocumentoService.getInstance().getValorAtual(doc.getCddocumento(), bean.getDtcredito()));
					bean.setDocumento(doc);
				}
			} else {
				for (Documento documento : listaDoc) {
					incluirDoc = false;
					if(documento.getSomentenossonumero() == null || !documento.getSomentenossonumero() ){
						incluirDoc = true;
					}else if(documento.getSomentenossonumero() && nossonumeroConvertido == null){
						incluirDoc = true;
					}else if(documento.getSomentenossonumero() && nossonumeroConvertido != null && 
							documento.getNossonumero() != null && documento.getNossonumero().equals(nossonumero)){
						incluirDoc = true;
					}
					if(incluirDoc){
						documento.setValoratual(DocumentoService.getInstance().getValorAtual(documento.getCddocumento(), bean.getDtcredito()));
						listaDocCorreta.add(documento);
					}
				}
				
				if(listaDocCorreta.size() > 0){
					if(listaDocCorreta.size() == 1)
						bean.setDocumento(listaDocCorreta.get(0));
					else
						bean.setListaDocumento(listaDocCorreta);
				}
			}
		}
		
		if(bean.getConfirmacao() == null || !bean.getConfirmacao()){
			if(bean.getDtcredito() != null && 
					bean.getValor() != null &&
					bean.getValor().getValue().doubleValue() > 0){
				bean.setBaixa(Boolean.TRUE);
			} else {
				bean.setBaixa(Boolean.FALSE);
			}
		} else {
			bean.setBaixa(Boolean.FALSE);
		}
		
		if(bean.getDocumento() != null){
			if(bean.getDocumento() != null && 
					bean.getDocumento().getDocumentoacao() != null && 
					bean.getDocumento().getDocumentoacao().equals(Documentoacao.BAIXADA)){
				bean.setBaixa(Boolean.FALSE);
				bean.setConfirmacao(Boolean.FALSE);
			}
		} else if(bean.getListaDocumento() != null && bean.getListaDocumento().size() > 0){
			Documento documento = bean.getListaDocumento().get(0);
			if(documento != null && 
					documento.getDocumentoacao() != null && 
					documento.getDocumentoacao().equals(Documentoacao.BAIXADA)){
				bean.setBaixa(Boolean.FALSE);
				bean.setConfirmacao(Boolean.FALSE);
			}
		} else {
			bean.setBaixa(Boolean.FALSE);
			bean.setConfirmacao(Boolean.FALSE);
		}
		
		this.lista.add(bean);
	}
	
	public void execute() throws ParseException{
		
		String[] cabecalho = new String[conf.getLinhainicial()];
		String[] corpo = new String[linha.length - conf.getLinhainicial() - conf.getLinhafinal()];
		
		System.arraycopy(linha, 0, cabecalho, 0, conf.getLinhainicial());
		System.arraycopy(linha, conf.getLinhainicial(), corpo, 0, linha.length - conf.getLinhainicial() - conf.getLinhafinal());
		
		if(conf.getAgenciacontacabecalho() != null && conf.getAgenciacontacabecalho())
			this.analisaCabecalho(cabecalho[0]);
		
		for (int i = 0; i < corpo.length; i++) {
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas()){
				this.analisaLinha(corpo[i], corpo[i+1]);
				i++;
			} else {
				this.analisaLinha(corpo[i], null);
			}
		}
	}
	

	public void analisaCabecalhoValidarArquivo(String linha){
		if(conf.getAgenciacontacabecalho() != null && conf.getAgenciacontacabecalho()) {
			agencia_cedente = linha.substring(conf.getAgenciacedenteposicaoinicial(), conf.getAgenciacedenteposicaoinicial() + conf.getAgenciacedentetamanho());
			conta_cedente = linha.substring(conf.getContacedenteposicaoinicial(), conf.getContacedenteposicaoinicial() + conf.getContacedentetamanho());
		}
	}
	
	public void analisaLinhaValidarArquivo(String linha, String linhaSegunda) throws ParseException {
		
		ArquivoRetornoConfiguravelDocumentoBean bean = new ArquivoRetornoConfiguravelDocumentoBean();
		String dtcreditoStr, nossonumero, valorStr, valorTarifaStr = null, motivoTarifa = null, codigoInstrucao;
		
		if((conf.getAgenciacontacabecalho() == null || !conf.getAgenciacontacabecalho())) {	
		
			// AG�NCIA
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getAgenciacedenteprimeiralinha() != null && !conf.getAgenciacedenteprimeiralinha()){
				agencia_cedente = linhaSegunda.substring(conf.getAgenciacedenteposicaoinicial(), conf.getAgenciacedenteposicaoinicial() + conf.getAgenciacedentetamanho());
			} else {
				agencia_cedente = linha.substring(conf.getAgenciacedenteposicaoinicial(), conf.getAgenciacedenteposicaoinicial() + conf.getAgenciacedentetamanho());
			}
			
			// CONTA
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getContacedenteprimeiralinha() != null && !conf.getContacedenteprimeiralinha()){
				conta_cedente = linhaSegunda.substring(conf.getContacedenteposicaoinicial(), conf.getContacedenteposicaoinicial() + conf.getContacedentetamanho());
			} else {
				conta_cedente = linha.substring(conf.getContacedenteposicaoinicial(), conf.getContacedenteposicaoinicial() + conf.getContacedentetamanho());
			}
			
		}
		bean.setAgenciacedente(agencia_cedente);
		bean.setContacedente(conta_cedente);
		
		// DATA DE CR�DITO
		if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getDatacreditoprimeiralinha() != null && !conf.getDatacreditoprimeiralinha()){
			dtcreditoStr = linhaSegunda.substring(conf.getDatacreditoposicaoinicial(), conf.getDatacreditoposicaoinicial() + conf.getDatacreditotamanho());
		} else {
			dtcreditoStr = linha.substring(conf.getDatacreditoposicaoinicial(), conf.getDatacreditoposicaoinicial() + conf.getDatacreditotamanho());
		}
		dtcreditoStr = dtcreditoStr.trim();
		bean.setDtcreditoStr(dtcreditoStr);
		
		// NOSSO N�MERO
		if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getNossonumeroprimeiralinha() != null && !conf.getNossonumeroprimeiralinha()){
			nossonumero = linhaSegunda.substring(conf.getNossonumeroposicaoinicial(), conf.getNossonumeroposicaoinicial() + conf.getNossonumerotamanho());
		} else {
			nossonumero = linha.substring(conf.getNossonumeroposicaoinicial(), conf.getNossonumeroposicaoinicial() + conf.getNossonumerotamanho());
		}
		nossonumero = nossonumero.trim();
		bean.setNossonumero(nossonumero);
		
		// VALOR COBRADO
		if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getValorcobradoprimeiralinha() != null && !conf.getValorcobradoprimeiralinha()){
			valorStr = linhaSegunda.substring(conf.getValorcobradoposicaoinicial(), conf.getValorcobradoposicaoinicial() + conf.getValorcobradotamanho()); 
		} else {
			valorStr = linha.substring(conf.getValorcobradoposicaoinicial(), conf.getValorcobradoposicaoinicial() + conf.getValorcobradotamanho()); 
		}
		
		// VALOR TARIFA
		if(conf.getValortarifaposicaoinicial() != null &&  conf.getValortarifatamanho() != null){
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getValortarifaprimeiralinha() != null && !conf.getValortarifaprimeiralinha()){
				valorTarifaStr = linhaSegunda.substring(conf.getValortarifaposicaoinicial(), conf.getValortarifaposicaoinicial() + conf.getValortarifatamanho()); 
			} else {
				valorTarifaStr = linha.substring(conf.getValortarifaposicaoinicial(), conf.getValortarifaposicaoinicial() + conf.getValortarifatamanho()); 
			}
		}
		
		// MOTIVO TARIFA
		if(conf.getMotivotarifaposicaoinicial() != null && conf.getMotivotarifatamanho() != null){
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getMotivotarifaprimeiralinha() != null && !conf.getMotivotarifaprimeiralinha()){
				motivoTarifa = linhaSegunda.substring(conf.getMotivotarifaposicaoinicial(), conf.getMotivotarifaposicaoinicial() + conf.getMotivotarifatamanho()); 
			} else {
				motivoTarifa = linha.substring(conf.getMotivotarifaposicaoinicial(), conf.getMotivotarifaposicaoinicial() + conf.getMotivotarifatamanho()); 
			}
		}
		
		if(motivoTarifa != null){
			bean.setMotivoTarifaDesc(descricaoMotivoTarifa(motivoTarifa));
		}
		
		// CONFIRMA��O DE ENTRADA
		if(conf.getCodigoconfirmacaoentrada() != null && !conf.getCodigoconfirmacaoentrada().equals("") &&
				conf.getCodigoinstrucaotamanho() != null && conf.getCodigoinstrucaoposicaoinicial() != null){
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas() && conf.getCodigoinstrucaoprimeiralinha() != null && !conf.getCodigoinstrucaoprimeiralinha()){
				codigoInstrucao = linhaSegunda.substring(conf.getCodigoinstrucaoposicaoinicial(), conf.getCodigoinstrucaoposicaoinicial() + conf.getCodigoinstrucaotamanho()); 
			} else {
				codigoInstrucao = linha.substring(conf.getCodigoinstrucaoposicaoinicial(), conf.getCodigoinstrucaoposicaoinicial() + conf.getCodigoinstrucaotamanho()); 
			} 
			
			bean.setCodigoinstrucao(codigoInstrucao);
		}
		
		
		valorStr = valorStr.trim();
		Money valor = null;
		if(valorStr != null && !valorStr.equals("")){
			valor = new Money(Integer.parseInt(valorStr), true);
		}
		bean.setValor(valor);
		
		if(valorTarifaStr != null){
			valorTarifaStr = valorTarifaStr.trim();
			Money valorTarifa = null;
			if(valorTarifaStr != null && !valorTarifaStr.equals("")){
				valorTarifa = new Money(Integer.parseInt(valorTarifaStr), true);
			}
			bean.setValorTarifa(valorTarifa);
		}
		
		this.lista.add(bean);
	}
	
	/**
	 * Retorna a nome do motivo de tarifa a partir do c�digo obtido no arquivo CEF.
	 * @param motivoTarifa
	 * @return
	 * @author Taidson
	 * @since 19/08/2010
	 */
	public String descricaoMotivoTarifa(String motivoTarifa){
		String observacao = null;
		switch(Integer.parseInt(motivoTarifa)){
		case 6:
			observacao = MotivoOcorrenciaTarifa.COB_INTERN.getNome();
			break;
		case 2:
			observacao = MotivoOcorrenciaTarifa.COB_LOTERI.getNome();
			break;
		case 3:
			observacao = MotivoOcorrenciaTarifa.COB_AGENC.getNome();
			break;
		case 4:
			observacao = MotivoOcorrenciaTarifa.COB_COMPE.getNome();
			break;
		}
		return observacao;
	}
	
	public void executeValidarArquivo() throws ParseException{
		
		String[] cabecalho = new String[conf.getLinhainicial()];
		String[] corpo = new String[linha.length - conf.getLinhainicial() - conf.getLinhafinal()];
		
		System.arraycopy(linha, 0, cabecalho, 0, conf.getLinhainicial());
		System.arraycopy(linha, conf.getLinhainicial(), corpo, 0, linha.length - conf.getLinhainicial() - conf.getLinhafinal());
		
		if(conf.getAgenciacontacabecalho() != null && conf.getAgenciacontacabecalho())
			this.analisaCabecalhoValidarArquivo(cabecalho[0]);
		
		for (int i = 0; i < corpo.length; i++) {
			if(conf.getDuaslinhas() != null && conf.getDuaslinhas()){
				this.analisaLinhaValidarArquivo(corpo[i], corpo[i+1]);
				i++;
			} else {
				this.analisaLinhaValidarArquivo(corpo[i], null);
			}
		}
	}
	
}
