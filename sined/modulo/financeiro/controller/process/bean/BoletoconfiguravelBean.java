package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Boleto;

public class BoletoconfiguravelBean {
	
	private String selectedItens;
	private Boleto boleto;	
	
	public String getSelectedItens() {
		return selectedItens;
	}
	@Required
	@DisplayName("Boleto template")
	public Boleto getBoleto() {
		return boleto;
	}
	
	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
	
}