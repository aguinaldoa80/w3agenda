package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.util.SinedUtil;

public class EstornaContaBean {

	protected Documento documentoEstornado;
	protected Movimentacao movimentacaoReferente;
	protected Movimentacao movimentacao;
	protected Boolean desconsiderarDiferenca;
	protected Documentoclasse documentoclasse;
	protected Rateio rateio;
	protected Rateio rateioDiferenca;
	protected List<Documento> listaDocumento;
	protected Money valortotalDocumentos;
	protected BaixarContaBean baixarContaBean;
	protected boolean existeBaixa;
	
	protected List<Documento> listaDocumentoAEstornar;
	protected List<Documento> listaDocumentoNaoEstornar;
	
	protected boolean erroInicial = false;
	protected boolean conf;
	protected String whereInMovimentacaoReferente;
	protected String whereInValecompra;
	protected String whereInHistoricoAntecipacao;
	protected String whereInDocumentoEstornado;
	
	public Documento getDocumentoEstornado() {
		return documentoEstornado;
	}
	public Movimentacao getMovimentacaoReferente() {
		return movimentacaoReferente;
	}
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	public Boolean getDesconsiderarDiferenca() {
		return desconsiderarDiferenca;
	}
	public Documentoclasse getDocumentoclasse() {
		return documentoclasse;
	}
	public Rateio getRateio() {
		return rateio;
	}
	public Rateio getRateioDiferenca() {
		return rateioDiferenca;
	}
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	public Money getValortotalDocumentos() {
		return valortotalDocumentos;
	}
	public BaixarContaBean getBaixarContaBean() {
		return baixarContaBean;
	}
	public boolean isExisteBaixa() {
		return existeBaixa;
	}
	public boolean isErroInicial() {
		return erroInicial;
	}
	public boolean isConf() {
		return conf;
	}
	public void setConf(boolean conf) {
		this.conf = conf;
	}
	public void setErroInicial(boolean erroInicial) {
		this.erroInicial = erroInicial;
	}
	public void setExisteBaixa(boolean existeBaixa) {
		this.existeBaixa = existeBaixa;
	}
	public void setDocumentoEstornado(Documento documentoEstornado) {
		this.documentoEstornado = documentoEstornado;
	}
	public void setMovimentacaoReferente(Movimentacao movimentacaoReferente) {
		this.movimentacaoReferente = movimentacaoReferente;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	public void setDesconsiderarDiferenca(Boolean desconsiderarDiferenca) {
		this.desconsiderarDiferenca = desconsiderarDiferenca;
	}
	public void setDocumentoclasse(Documentoclasse documentoclasse) {
		this.documentoclasse = documentoclasse;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setRateioDiferenca(Rateio rateioDiferenca) {
		this.rateioDiferenca = rateioDiferenca;
	}
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
		getListaDocumentoAEstornar();
		getListaDocumentoNaoEstornar();
	}
	public void setValortotalDocumentos(Money valortotalDocumentos) {
		this.valortotalDocumentos = valortotalDocumentos;
	}
	public void setBaixarContaBean(BaixarContaBean baixarContaBean) {
		this.baixarContaBean = baixarContaBean;
	}
	
	/**
	 * Este getter retorna os documentos selecionados para estorno, com base na propriedade listaDocumento.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> getListaDocumentoAEstornar(){
		if(SinedUtil.isListEmpty(listaDocumentoAEstornar) && SinedUtil.isListNotEmpty(listaDocumento)){
			listaDocumentoAEstornar = new ArrayList<Documento>();
			for (Documento d : listaDocumento) {
				if(Util.booleans.isTrue(d.getSelected())){
					listaDocumentoAEstornar.add(d);
				}
			}
		}
		return listaDocumentoAEstornar;
	}
	
	/**
	 * Este getter retorna os documentos n�o selecionados para estorno, com base na propriedade listaDocumento.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documento> getListaDocumentoNaoEstornar(){
		if(SinedUtil.isListEmpty(listaDocumentoNaoEstornar) && SinedUtil.isListNotEmpty(listaDocumento)){
			listaDocumentoNaoEstornar = new ArrayList<Documento>();
			for (Documento d : listaDocumento) {
				if(Util.booleans.isFalse(d.getSelected())){
					listaDocumentoNaoEstornar.add(d);
				}
			}
		}
		return listaDocumentoNaoEstornar;
	}
	
	public void setListaDocumentoAEstornar(List<Documento> listaDocumentoAEstornar) {
		if(listaDocumentoAEstornar != null){
			for (Documento documento : listaDocumentoAEstornar) {
				documento.setSelected(Boolean.TRUE);
			}
		}
		this.listaDocumentoAEstornar = listaDocumentoAEstornar;
	}
	
	public void setListaDocumentoNaoEstornar(List<Documento> listaDocumentoNaoEstornar) {
		if(listaDocumentoNaoEstornar != null){
			for (Documento documento : listaDocumentoNaoEstornar) {
				documento.setSelected(Boolean.FALSE);
			}
		}
		this.listaDocumentoNaoEstornar = listaDocumentoNaoEstornar;
	}

	public String getWhereInMovimentacaoReferente() {
		return whereInMovimentacaoReferente;
	}
	public void setWhereInMovimentacaoReferente(String whereInMovimentacaoReferente) {
		this.whereInMovimentacaoReferente = whereInMovimentacaoReferente;
	}
	
	public String getWhereInValecompra() {
		return whereInValecompra;
	}
	public void setWhereInValecompra(String whereInValecompra) {
		this.whereInValecompra = whereInValecompra;
	}
	
	public String getWhereInHistoricoAntecipacao() {
		return whereInHistoricoAntecipacao;
	}
	public void setWhereInHistoricoAntecipacao(String whereInHistoricoAntecipacao) {
		this.whereInHistoricoAntecipacao = whereInHistoricoAntecipacao;
	}
	public String getWhereInDocumentoEstornado() {
		return whereInDocumentoEstornado;
	}
	public void setWhereInDocumentoEstornado(String whereInDocumentoEstornado) {
		this.whereInDocumentoEstornado = whereInDocumentoEstornado;
	}
}
