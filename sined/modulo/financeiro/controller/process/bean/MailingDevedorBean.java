package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.geral.bean.enumeration.MailingDevedorContasProtestadas;
import br.com.linkcom.sined.geral.bean.enumeration.MailingDevedorMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.MailingDevedorVisaoMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoMailingDevedor;
import br.com.linkcom.sined.geral.bean.enumeration.SimNaoSomenteEnum;


public class MailingDevedorBean extends ReportTemplateFiltro {
	
	private String mensagem;
	private String assunto;
	private Empresa empresa;
	private Categoria categoria;
	private String contas;
	private String email;
	private Contatotipo contatotipo;
	private Date dtvencimento1;
	private Date dtvencimento2;
	private Cliente cliente;
	private MailingDevedorMostrar mostrar;
	private MailingDevedorVisaoMostrar visaoMostrar;
	private OrdenacaoMailingDevedor campoOrdenacao = OrdenacaoMailingDevedor.CLIENTE;
	private Boolean tipoOrdenacao = Boolean.TRUE;
	private Centrocusto centrocusto;
	private Documentotipo documentotipo;
	private MailingDevedorContasProtestadas contasProtestadas = MailingDevedorContasProtestadas.SIM;
	private SimNaoSomenteEnum contasBaixadasParcialmente = SimNaoSomenteEnum.SIM;
	private Colaborador colaborador;
	
	//Registrar Cobran�a
	private Atividadetipo atividadetipo;
	private Meiocontato meiocontato;
	private String descricao;
	
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public String getContas() {
		return contas;
	}
	public void setContas(String contas) {
		this.contas = contas;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Contatotipo getContatotipo() {
		return contatotipo;
	}
	public void setContatotipo(Contatotipo contatotipo) {
		this.contatotipo = contatotipo;
	}
	public Date getDtvencimento1() {
		return dtvencimento1;
	}
	public Date getDtvencimento2() {
		return dtvencimento2;
	}
	public void setDtvencimento1(Date dtvencimento1) {
		this.dtvencimento1 = dtvencimento1;
	}
	public void setDtvencimento2(Date dtvencimento2) {
		this.dtvencimento2 = dtvencimento2;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public MailingDevedorMostrar getMostrar() {
		return mostrar;
	}
	public void setMostrar(MailingDevedorMostrar mostrar) {
		this.mostrar = mostrar;
	}
	public MailingDevedorVisaoMostrar getVisaoMostrar() {
		return visaoMostrar;
	}
	public void setVisaoMostrar(MailingDevedorVisaoMostrar visaoMostrar) {
		this.visaoMostrar = visaoMostrar;
	}
	public OrdenacaoMailingDevedor getCampoOrdenacao() {
		return campoOrdenacao;
	}
	public void setCampoOrdenacao(OrdenacaoMailingDevedor campoOrdenacao) {
		this.campoOrdenacao = campoOrdenacao;
	}
	public Boolean getTipoOrdenacao() {
		return tipoOrdenacao;
	}
	public void setTipoOrdenacao(Boolean tipoOrdenacao) {
		this.tipoOrdenacao = tipoOrdenacao;
	}
	
	@DisplayName("Centro de Custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public Meiocontato getMeiocontato() {
		return meiocontato;
	}
	public void setMeiocontato(Meiocontato meiocontato) {
		this.meiocontato = meiocontato;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	@DisplayName("Exibir contas protestadas")
	public MailingDevedorContasProtestadas getContasProtestadas() {
		return contasProtestadas;
	}
	public void setContasProtestadas(
			MailingDevedorContasProtestadas contasProtestadas) {
		this.contasProtestadas = contasProtestadas;
	}
	@DisplayName("Exibir contas baixadas parcialmente")
	public SimNaoSomenteEnum getContasBaixadasParcialmente() {
		return contasBaixadasParcialmente;
	}
	public void setContasBaixadasParcialmente(
			SimNaoSomenteEnum contasBaixadasParcialmente) {
		this.contasBaixadasParcialmente = contasBaixadasParcialmente;
	}
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
}
