package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Empresa;


public class ArquivoConciliacaoCartaoCreditoBean {

	protected Empresa empresa;
	protected Arquivo arquivo;
	protected List<ArquivoConciliacaoCartaoCreditoItemBean> listaEncontrada;
	protected List<ArquivoConciliacaoCartaoCreditoItemBean> listaResolucao;
	protected List<ArquivoConciliacaoCartaoCreditoItemBean> listaNaoEncontrada;
	protected TipoOperadoraCartaoCredito tipo;
	protected Conta conta;
	protected Money total;
	protected Money totalTaxa;
	protected Date dtmovimentacao;
	
	public enum TipoOperadoraCartaoCredito {
		CIELO, REDE
	}
	
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Origem")
	public TipoOperadoraCartaoCredito getTipo() {
		return tipo;
	}
	
	public List<ArquivoConciliacaoCartaoCreditoItemBean> getListaEncontrada() {
		return listaEncontrada;
	}
	
	public List<ArquivoConciliacaoCartaoCreditoItemBean> getListaNaoEncontrada() {
		return listaNaoEncontrada;
	}
	
	public List<ArquivoConciliacaoCartaoCreditoItemBean> getListaResolucao() {
		return listaResolucao;
	}
	
	public Conta getConta() {
		return conta;
	}
	
	public Money getTotal() {
		return total;
	}
	
	public Money getTotalTaxa() {
		return totalTaxa;
	}
	
	@DisplayName("Data")
	public Date getDtmovimentacao() {
		return dtmovimentacao;
	}
	
	public void setDtmovimentacao(Date dtmovimentacao) {
		this.dtmovimentacao = dtmovimentacao;
	}
	
	public void setTotal(Money total) {
		this.total = total;
	}
	
	public void setTotalTaxa(Money totalTaxa) {
		this.totalTaxa = totalTaxa;
	}
	
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public void setTipo(TipoOperadoraCartaoCredito tipo) {
		this.tipo = tipo;
	}
	
	public void setListaResolucao(
			List<ArquivoConciliacaoCartaoCreditoItemBean> listaResolucao) {
		this.listaResolucao = listaResolucao;
	}
	
	public void setListaNaoEncontrada(
			List<ArquivoConciliacaoCartaoCreditoItemBean> listaNaoEncontrada) {
		this.listaNaoEncontrada = listaNaoEncontrada;
	}
	
	public void setListaEncontrada(
			List<ArquivoConciliacaoCartaoCreditoItemBean> listaEncontrada) {
		this.listaEncontrada = listaEncontrada;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

}
