package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.LinkedList;
import java.util.List;

import br.com.linkcom.neo.types.Money;


public class MailingDevedorRetornoBean {
	
	private LinkedList<DevedoresBean> listaDevedores = new LinkedList<DevedoresBean>();
	private Money valorTotal = new Money(0);
	private Money valorAtualTotal = new Money(0);
	
	public List<DevedoresBean> getListaDevedores() {
		return listaDevedores;
	}
	public Money getValorTotal() {
		return valorTotal;
	}
	public Money getValorAtualTotal() {
		return valorAtualTotal;
	}
	public void setListaDevedores(LinkedList<DevedoresBean> listaDevedores) {
		this.listaDevedores = listaDevedores;
	}
	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}
	public void setValorAtualTotal(Money valorAtualTotal) {
		this.valorAtualTotal = valorAtualTotal;
	}
	
}
