package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;



import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Tipooperacao;


public class ConciliacaoBean{
	
	protected Tipooperacao tipooperacao;
	protected Date databanco;
	protected String datastring;
	protected Money valor;
	protected String checknum;
	protected String memo;
	protected String fitid;
	protected Movimentacao movimentacao;
	protected Boolean associada;
	protected Integer order;
	protected String tipo;
	protected Boolean fechamentofinanceiro;
	protected Date dtserver;
	protected String dtserverstring;
	
	protected Boolean deleteMov;
	protected Boolean conciliar;
	
//	<TRNTYPE>DEBIT
//	<DTPOSTED>20080527100000[-03:EST]
//	<TRNAMT>-50.00
//	<FITID>20080527001
//	<CHECKNUM>20080527001
//	<MEMO>CEI     000232 SAQUE 
	
	public ConciliacaoBean(){
		conciliar = Boolean.TRUE;
	}
	
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	public Date getDatabanco() {
		return databanco;
	}
	public Money getValor() {
		return valor;
	}
	public String getChecknum() {
		return checknum;
	}
	@DisplayName("Histórico do banco")
	public String getMemo() {
		return memo;
	}
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	public Boolean getAssociada() {
		return associada;
	}
	public String getFitid() {
		return fitid;
	}
	public String getDatastring() {
		return datastring;
	}
	public Boolean getDeleteMov() {
		return deleteMov;
	}
	public Boolean getFechamentofinanceiro() {
		return fechamentofinanceiro;
	}
	public Boolean getConciliar() {
		return conciliar;
	}
	public String getDtserverstring() {
		return dtserverstring;
	}
	public void setDtserverstring(String dtserverstring) {
		this.dtserverstring = dtserverstring;
	}
	public void setFechamentofinanceiro(Boolean fechamentofinanceiro) {
		this.fechamentofinanceiro = fechamentofinanceiro;
	}
	public void setDeleteMov(Boolean deleteMov) {
		this.deleteMov = deleteMov;
	}
	public void setDatastring(String datastring) {
		this.datastring = datastring;
	}
	public void setFitid(String fitid) {
		this.fitid = fitid;
	}
	public void setAssociada(Boolean associada) {
		this.associada = associada;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	public void setDatabanco(Date databanco) {
		this.databanco = databanco;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setChecknum(String checknum) {
		this.checknum = checknum;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public void setConciliar(Boolean conciliar) {
		this.conciliar = conciliar;
	}
	public Date getDtserver() {
		return dtserver;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ConciliacaoBean) {
			ConciliacaoBean bean = (ConciliacaoBean) obj;
			return bean.getMovimentacao().getCdmovimentacao().equals(this.getMovimentacao().getCdmovimentacao());
		}
		return super.equals(obj);
	}
	
	public void setDtserver(Date dtserver) {
		this.dtserver = dtserver;
	}
	
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Integer getOrder() {
		return order;
	}
	
	public void setOrder(Integer order) {
		this.order = order;
	}	
}
