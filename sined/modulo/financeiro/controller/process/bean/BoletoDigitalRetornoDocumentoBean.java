package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;

public class BoletoDigitalRetornoDocumentoBean {

	private Documento documento;
	private BoletoDigitalRetornoAcaoEnum acao;
	private String nossonumero;
	private String usoempresa;
	private Date dtvencimento;
	private Date dtcredito;
	private Money valor;
	private Money valorpago;
	private Boolean marcado;
	private String mensagem;
	
	public Documento getDocumento() {
		return documento;
	}
	public BoletoDigitalRetornoAcaoEnum getAcao() {
		return acao;
	}
	public String getNossonumero() {
		return nossonumero;
	}
	public String getUsoempresa() {
		return usoempresa;
	}
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public Date getDtcredito() {
		return dtcredito;
	}
	public Money getValor() {
		return valor;
	}
	public Money getValorpago() {
		return valorpago;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setAcao(BoletoDigitalRetornoAcaoEnum acao) {
		this.acao = acao;
	}
	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}
	public void setUsoempresa(String usoempresa) {
		this.usoempresa = usoempresa;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setValorpago(Money valorpago) {
		this.valorpago = valorpago;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	
	
	public void addMensagem(String mensagem){
		if((getMensagem() != null && mensagem != null && getMensagem().contains(mensagem)) || mensagem == null){
			return;
		}
		
		if (getMensagem() == null)
			setMensagem("");
		if (getMensagem() != null && !"".equals(getMensagem().trim()))
			setMensagem(getMensagem().concat("\n"));
		setMensagem(getMensagem().concat(mensagem));		
	}
	
}