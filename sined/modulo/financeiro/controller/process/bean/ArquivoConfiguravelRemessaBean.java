package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaInstrucao;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoremessaEnum;

public class ArquivoConfiguravelRemessaBean {

	private Empresa empresa;
	private Conta conta;
	private Contacarteira contacarteira;
	private Date dtinicio;
	private Date dtfim;
	private Date dtvencimento1;
	private Date dtvencimento2;
	private Date dtemissaonota1;
	private Date dtemissaonota2;
	private Cliente cliente;
	private BancoConfiguracaoRemessaInstrucao instrucaoCobranca;
	private String cds;
	protected Projeto projeto;
	protected Centrocusto centrocusto;
	protected Boolean criteriodocumentotipo = Boolean.TRUE;
	protected Documentotipo documentotipo;
	protected SituacaoremessaEnum situacaoremessa;
	protected Integer numerofatura1;
	protected Integer numerofatura2;
	protected Integer pageSize = 100;
	protected Integer currentPage = 0;
	protected String whereInCddocumento;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Conta banc�ria")
	public Conta getConta() {
		return conta;
	}
	@DisplayName("Carteira")
	@Required
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Instru��o de cobran�a")
	public BancoConfiguracaoRemessaInstrucao getInstrucaoCobranca() {
		return instrucaoCobranca;
	}
	public String getCds() {
		return cds;
	}
	@Required
	@MaxLength(9)
	public Integer getPageSize() {
		return pageSize;
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	@DisplayName("Contas")
	public String getWhereInCddocumento() {
		return whereInCddocumento;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setInstrucaoCobranca(
			BancoConfiguracaoRemessaInstrucao instrucaoCobranca) {
		this.instrucaoCobranca = instrucaoCobranca;
	}
	public void setCds(String cds) {
		this.cds = cds;
	}
	public Date getDtemissaonota1() {
		return dtemissaonota1;
	}
	public Date getDtemissaonota2() {
		return dtemissaonota2;
	}
	public void setDtemissaonota1(Date dtemissaonota1) {
		this.dtemissaonota1 = dtemissaonota1;
	}
	public void setDtemissaonota2(Date dtemissaonota2) {
		this.dtemissaonota2 = dtemissaonota2;
	}
	@DisplayName("Centro de Custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@DisplayName("Crit�rio do tipo de documento")
	public Boolean getCriteriodocumentotipo() {
		return criteriodocumentotipo;
	}
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@DisplayName("Situa��o de remessa")
	public SituacaoremessaEnum getSituacaoremessa() {
		return situacaoremessa;
	}
	
	public void setCriteriodocumentotipo(Boolean criteriodocumentotipo) {
		this.criteriodocumentotipo = criteriodocumentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setSituacaoremessa(SituacaoremessaEnum situacaoremessa) {
		this.situacaoremessa = situacaoremessa;
	}
	
	public Date getDtvencimento1() {
		return dtvencimento1;
	}
	public Date getDtvencimento2() {
		return dtvencimento2;
	}

	public void setDtvencimento1(Date dtvencimento1) {
		this.dtvencimento1 = dtvencimento1;
	}
	public void setDtvencimento2(Date dtvencimento2) {
		this.dtvencimento2 = dtvencimento2;
	}
	
	public Integer getNumerofatura1() {
		return numerofatura1;
	}
	public Integer getNumerofatura2() {
		return numerofatura2;
	}
	public void setNumerofatura1(Integer numerofatura1) {
		this.numerofatura1 = numerofatura1;
	}
	public void setNumerofatura2(Integer numerofatura2) {
		this.numerofatura2 = numerofatura2;
	}
	public void setWhereInCddocumento(String whereInCddocumento) {
		this.whereInCddocumento = whereInCddocumento;
	}
}