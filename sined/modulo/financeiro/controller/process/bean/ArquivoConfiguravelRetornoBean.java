package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetorno;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;

public class ArquivoConfiguravelRetornoBean {
	
	private Long hash;
	private Empresa empresa;
	private Conta conta;
	private Contacarteira contacarteira;
	private Arquivo arquivo;
	private BancoConfiguracaoRemessaTipoEnum tipo;
	
	private BancoConfiguracaoRetorno bancoConfiguracaoRetorno;
	private List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento = new ListSet<ArquivoConfiguravelRetornoDocumentoBean>(ArquivoConfiguravelRetornoDocumentoBean.class);
	
	private Money valorTotal;
	private String erros;
	private String idsBaixado;
	private boolean processado = false;
	private String msgEstrutura;
	private Money valorTotalPago = new Money();
	private Money valorTotalPagoTituloEncontrado = new Money();
	
	public ArquivoConfiguravelRetornoBean() {}
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	public Conta getConta() {
		return conta;
	}
	@Required
	@DisplayName("Carteira")
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	@Required
	public BancoConfiguracaoRemessaTipoEnum getTipo() {
		return tipo;
	}
	public BancoConfiguracaoRetorno getBancoConfiguracaoRetorno() {
		return bancoConfiguracaoRetorno;
	}
	public List<ArquivoConfiguravelRetornoDocumentoBean> getListaDocumento() {
		return listaDocumento;
	}
	public Long getHash() {
		return hash;
	}
	public void setHash(Long hash) {
		this.hash = hash;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	public void setTipo(BancoConfiguracaoRemessaTipoEnum tipo) {
		this.tipo = tipo;
	}
	public void setBancoConfiguracaoRetorno(
			BancoConfiguracaoRetorno bancoConfiguracaoRetorno) {
		this.bancoConfiguracaoRetorno = bancoConfiguracaoRetorno;
	}
	public void setListaDocumento(List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	@DisplayName("Valor total")
	public Money getValorTotal() {
		Money total = new Money(0.0);
		if(this.listaDocumento != null && !this.listaDocumento.isEmpty())
			for (ArquivoConfiguravelRetornoDocumentoBean bean : listaDocumento) 
				total = total.add(bean.getValor() != null ? bean.getValor() : new Money(0.0));
		
		this.valorTotal = total;
		return valorTotal;
	}
	
	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	public String getErros() {
		return erros;
	}
	
	public void setErros(String erros) {
		this.erros = erros;
	}
	
	public String getIdsBaixado() {
		return idsBaixado;
	}
	
	public void setIdsBaixado(String idsBaixado) {
		this.idsBaixado = idsBaixado;
	}
	
	public boolean isProcessado() {
		return processado;
	}
	
	public void setProcessado(boolean processado) {
		this.processado = processado;
	}
	
	public String getMsgEstrutura() {
		return msgEstrutura;
	}
	
	public void setMsgEstrutura(String msgEstrutura) {
		this.msgEstrutura = msgEstrutura;
	}
	
	public void addErro(String mensagem){
		if (getErros() == null)
			setErros("");
		if (getErros() != null && !"".equals(getErros().trim()))
			getErros().concat("\n");
		setErros(getErros().concat(mensagem));		
	}

	@DisplayName("Total valor pago")
	public Money getValorTotalPago() {
		return valorTotalPago;
	}

	@DisplayName("Total valor pago - T�tulos encontrados")
	public Money getValorTotalPagoTituloEncontrado() {
		return valorTotalPagoTituloEncontrado;
	}

	public void setValorTotalPago(Money valorTotalPago) {
		this.valorTotalPago = valorTotalPago;
	}
	
	public void setValorTotalPagoTituloEncontrado(Money valorTotalPagoTituloEncontrado) {
		this.valorTotalPagoTituloEncontrado = valorTotalPagoTituloEncontrado;
	}	
}
