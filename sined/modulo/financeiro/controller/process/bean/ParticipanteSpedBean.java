package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

public class ParticipanteSpedBean {
	
	private Integer cdpessoa;
	private Integer cdendereco;
	private String nome;
	private String cnpj;
	private String cpf;
	private String ie;
	private String cod_municipio;
	private String end;
	private String num;
	private String compl;
	private String bairro;
	private String cod_pais;
	
	public String getNome() {
		return nome;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public String getIe() {
		return ie;
	}
	public String getCod_municipio() {
		return cod_municipio;
	}
	public String getEnd() {
		return end;
	}
	public String getNum() {
		return num;
	}
	public String getCompl() {
		return compl;
	}
	public String getBairro() {
		return bairro;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public Integer getCdendereco() {
		return cdendereco;
	}
	public void setCdendereco(Integer cdendereco) {
		this.cdendereco = cdendereco;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setIe(String ie) {
		this.ie = ie;
	}
	public void setCod_municipio(String cod_municipio) {
		this.cod_municipio = cod_municipio;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public void setCompl(String compl) {
		this.compl = compl;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCod_pais() {
		return cod_pais;
	}
	public void setCod_pais(String codPais) {
		cod_pais = codPais;
	}
}
