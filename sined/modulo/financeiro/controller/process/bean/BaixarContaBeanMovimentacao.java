package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.enumeration.FinalidadeDOC;
import br.com.linkcom.sined.geral.bean.enumeration.FinalidadeTED;
import br.com.linkcom.sined.util.SinedDateUtils;

@DisplayName("BAIXAR CONTA")
public class BaixarContaBeanMovimentacao implements Cloneable {
	
	protected Rateio rateio;
	protected Date dtpagamento;
	protected Formapagamento formapagamento;
	protected Documento documentoAntecipado;
	protected Money valorAntecipado;
	protected Contatipo contatipo;
	protected Conta vinculo;
	protected Cheque cheque;
	protected Documentoclasse documentoclasse;
	protected Boolean gerararquivoremessa;
	protected String checknum;
	protected String historico;
	protected Boolean gerararquivoregistrocobranca;
	protected BancoFormapagamento bancoformapagamento;
	protected BancoTipoPagamento bancotipopagamento;
	protected FinalidadeTED finalidadeted;
	protected FinalidadeDOC finalidadedoc;
	protected Money valor;
	protected Arquivo arquivo;
	protected Money valormaximovalecompra;
	protected Pessoa pessoa;
	protected Boolean discriminarvalortaxa;
	protected Money valortaxa;
	
	protected Integer cdchequecheque;
	protected Integer bancocheque;
	protected Integer agenciacheque;
	protected Integer contacheque;
	protected String numerocheque;
	protected String emitentecheque;
	protected String cpfcnpjcheque;
	protected Date dtbomparacheque;
	protected Money valorcheque;
	
	protected String chequeauxiliar;
	protected Conta vinculoaux;
	protected Documento documentovinculado;
	protected Rateio rateiovinculado;
	protected Double valorTotalVinculado;
	
	protected Arquivo comprovante;
	
	{
		this.dtpagamento = SinedDateUtils.currentDate();
	}
	
	public Rateio getRateio() {
		return rateio;
	}
	@DisplayName("Data do pagamento")
	@Required
	public Date getDtpagamento() {
		return dtpagamento;
	}
	@Required
	@DisplayName("Forma de pagamento")
	public Formapagamento getFormapagamento() {
		return formapagamento;
	}
	
	public Conta getVinculo() {
		return vinculo;
	}

	public Contatipo getContatipo() {
		return contatipo;
	}
	@DisplayName("Cheque")
	public Cheque getCheque() {
		return cheque;
	}
	public Documentoclasse getDocumentoclasse() {
		return documentoclasse;
	}	
	@DisplayName("Gerar arquivo remessa")
	public Boolean getGerararquivoremessa() {
		return gerararquivoremessa;
	}
	@DisplayName("Hist�rico")
	@MaxLength(500)
	public String getHistorico() {
		return historico;
	}
	@MaxLength(50)
	@DisplayName("Documento")
	public String getChecknum() {
		return checknum;
	}
	@DisplayName("Gerar arquivo de registro da baixa")
	public Boolean getGerararquivoregistrocobranca() {
		return gerararquivoregistrocobranca;
	}
	public Arquivo getCdarquivo() {
		return arquivo;
	}
	public void setGerararquivoregistrocobranca(Boolean gerararquivoregistrocobranca) {
		this.gerararquivoregistrocobranca = gerararquivoregistrocobranca;
	}
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setChecknum(String checknum) {
		this.checknum = checknum;
	}
	public void setGerararquivoremessa(Boolean gerararquivoremessa) {
		this.gerararquivoremessa = gerararquivoremessa;
	}
	public void setDocumentoclasse(Documentoclasse documentoclasse) {
		this.documentoclasse = documentoclasse;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}
	public void setFormapagamento(Formapagamento formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setVinculo(Conta vinculo) {
		this.vinculo = vinculo;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	public void setCdarquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	@DisplayName("FORMA DE PAGAMENTO")
	public BancoFormapagamento getBancoformapagamento() {
		return bancoformapagamento;
	}
	public void setBancoformapagamento(BancoFormapagamento bancoformapagamento) {
		this.bancoformapagamento = bancoformapagamento;
	}
	@DisplayName("TIPO DE PAGAMENTO")
	public BancoTipoPagamento getBancotipopagamento() {
		return bancotipopagamento;
	}
	public void setBancotipopagamento(BancoTipoPagamento bancotipopagamento) {
		this.bancotipopagamento = bancotipopagamento;
	}
	@DisplayName("FINALIDADE DA TED")
	public FinalidadeTED getFinalidadeted() {
		return finalidadeted;
	}
	public void setFinalidadeted(FinalidadeTED finalidadeted) {
		this.finalidadeted = finalidadeted;
	}
	@DisplayName("FINALIDADE DOC")
	public FinalidadeDOC getFinalidadedoc() {
		return finalidadedoc;
	}
	public void setFinalidadedoc(FinalidadeDOC finalidadedoc) {
		this.finalidadedoc = finalidadedoc;
	}
	@DisplayName("Banco")
	public Integer getBancocheque() {
		return bancocheque;
	}
	@DisplayName("Ag�ncia")
	public Integer getAgenciacheque() {
		return agenciacheque;
	}
	@DisplayName("Conta")
	public Integer getContacheque() {
		return contacheque;
	}
	@DisplayName("N�mero")
	public String getNumerocheque() {
		return numerocheque;
	}
	@DisplayName("Emitente")
	public String getEmitentecheque() {
		return emitentecheque;
	}
	@DisplayName("CNPJ/CPF do emitente")
	@MaxLength(14)
	public String getCpfcnpjcheque() {
		return cpfcnpjcheque;
	}
	@DisplayName("Bom para")
	public Date getDtbomparacheque() {
		return dtbomparacheque;
	}
	public void setBancocheque(Integer bancocheque) {
		this.bancocheque = bancocheque;
	}
	public void setAgenciacheque(Integer agenciacheque) {
		this.agenciacheque = agenciacheque;
	}
	public void setContacheque(Integer contacheque) {
		this.contacheque = contacheque;
	}
	public void setNumerocheque(String numerocheque) {
		this.numerocheque = numerocheque;
	}
	public void setEmitentecheque(String emitentecheque) {
		this.emitentecheque = emitentecheque;
	}
	public void setCpfcnpjcheque(String cpfcnpjcheque) {
		this.cpfcnpjcheque = cpfcnpjcheque;
	}
	public void setDtbomparacheque(Date dtbomparacheque) {
		this.dtbomparacheque = dtbomparacheque;
	}
	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	@DisplayName("Comprovante")
	public Arquivo getComprovante() {
		return comprovante;
	}
	public void setComprovante(Arquivo comprovante) {
		this.comprovante = comprovante;
	}
	public Integer getCdchequecheque() {
		return cdchequecheque;
	}
	public void setCdchequecheque(Integer cdchequecheque) {
		this.cdchequecheque = cdchequecheque;
	}
	public String getChequeauxiliar() {
		return chequeauxiliar;
	}
	public void setChequeauxiliar(String chequeauxiliar) {
		this.chequeauxiliar = chequeauxiliar;
	}
	@DisplayName("Valor")
	public Money getValorcheque() {
		return valorcheque;
	}
	public Money getValormaximovalecompra() {
		return valormaximovalecompra;
	}
	public void setValormaximovalecompra(Money valormaximovalecompra) {
		this.valormaximovalecompra = valormaximovalecompra;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public Boolean getDiscriminarvalortaxa() {
		return discriminarvalortaxa;
	}
	public Money getValortaxa() {
		return valortaxa;
	}
	public void setDiscriminarvalortaxa(Boolean discriminarvalortaxa) {
		this.discriminarvalortaxa = discriminarvalortaxa;
	}
	public void setValortaxa(Money valortaxa) {
		this.valortaxa = valortaxa;
	}
	public void setValorcheque(Money valorcheque) {
		this.valorcheque = valorcheque;
	}
	public Conta getVinculoaux() {
		return vinculoaux;
	}
	public void setVinculoaux(Conta vinculoaux) {
		this.vinculoaux = vinculoaux;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	@DisplayName("CONTA - ANTECIPA��O")
	public Documento getDocumentoAntecipado() {
		return documentoAntecipado;
	}
	public void setDocumentoAntecipado(Documento documentoAntecipado) {
		this.documentoAntecipado = documentoAntecipado;
	}
	public Money getValorAntecipado() {
		return valorAntecipado;
	}
	public void setValorAntecipado(Money valorAntecipado) {
		this.valorAntecipado = valorAntecipado;
	}
	
	public Documento getDocumentovinculado() {
		return documentovinculado;
	}
	public void setDocumentovinculado(Documento documentovinculado) {
		this.documentovinculado = documentovinculado;
	}
	
	public Rateio getRateiovinculado() {
		return rateiovinculado;
	}
	
	public void setRateiovinculado(Rateio rateiovinculado) {
		this.rateiovinculado = rateiovinculado;
	}
	
	public Double getValorTotalVinculado() {
		return valorTotalVinculado;
	}
	
	public void setValorTotalVinculado(Double valorTotalVinculado) {
		this.valorTotalVinculado = valorTotalVinculado;
	}
	
	public BaixarContaBeanMovimentacao getClone() {
		try {
			return (BaixarContaBeanMovimentacao) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
}