package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;

public class SelecionarDocumentoBean {
	
	public final Integer limiteListagem = 20;
	
	private String cds;
	private boolean filtrar = false;
	private boolean limitar = true;
	
	private Date dtVencimentoIni;
	private Date dtVencimentoFim;
	private String pessoa;
	private String descricao;
	private String numero;
	private Documentoclasse documentoclasse;
	private Conta conta;
	private Integer cddocumento;
	
	private List<Documento> listaDocumento;

	public String getCds() {
		return cds;
	}

	public boolean isFiltrar() {
		return filtrar;
	}

	public boolean isLimitar() {
		return limitar;
	}

	public Date getDtVencimentoIni() {
		return dtVencimentoIni;
	}

	public Date getDtVencimentoFim() {
		return dtVencimentoFim;
	}

	public String getPessoa() {
		return pessoa;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getNumero() {
		return numero;
	}

	public Documentoclasse getDocumentoclasse() {
		return documentoclasse;
	}

	public Conta getConta() {
		return conta;
	}

	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	
	@MaxLength(9)
	public Integer getCddocumento() {
		return cddocumento;
	}

	public void setCds(String cds) {
		this.cds = cds;
	}

	public void setFiltrar(boolean filtrar) {
		this.filtrar = filtrar;
	}

	public void setLimitar(boolean limitar) {
		this.limitar = limitar;
	}

	public void setDtVencimentoIni(Date dtVencimentoIni) {
		this.dtVencimentoIni = dtVencimentoIni;
	}

	public void setDtVencimentoFim(Date dtVencimentoFim) {
		this.dtVencimentoFim = dtVencimentoFim;
	}

	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setDocumentoclasse(Documentoclasse documentoclasse) {
		this.documentoclasse = documentoclasse;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}

}
