package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Movimentacao;

@DisplayName("CONCILIAÇÃO")
public class ConciliacaoManualBean{
	
	protected List<Movimentacao> listaMovimentacao;
	protected Date dtbanco;
	
	public List<Movimentacao> getListaMovimentacao() {
		return listaMovimentacao;
	}
	
	public Date getDtbanco() {
		return dtbanco;
	}
	
	public void setDtbanco(Date dtbanco) {
		this.dtbanco = dtbanco;
	}
	
	public void setListaMovimentacao(List<Movimentacao> listaMovimentacao) {
		this.listaMovimentacao = listaMovimentacao;
	}

}
