package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacao;


public class ArquivoConciliacaoCartaoCreditoItemBean {

	private Boolean marcado;
	private Boolean permitidoConciliacao = Boolean.FALSE;
	private String pessoa;
	private String descricao;
	private Money valor;
	private Money valorArquivo;
	private Money valorArquivoLiquido;
	private Date dtefetivapagamento;
	private Date dtvenda;
	private String tid;
	private String doc;
	private Documento documento;
	private Movimentacao movimentacao;
	private List<Documento> listaDocumento;
	private Empresa empresa;
	private Money valorArquivoTaxa;
	
	public Boolean getMarcado() {
		return marcado;
	}
	public String getPessoa() {
		return pessoa;
	}
	public Money getValor() {
		return valor;
	}
	public Date getDtefetivapagamento() {
		return dtefetivapagamento;
	}
	public String getTid() {
		return tid;
	}
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	public Boolean getPermitidoConciliacao() {
		return permitidoConciliacao;
	}
	public Documento getDocumento() {
		return documento;
	}
	public Money getValorArquivo() {
		return valorArquivo;
	}
	public String getDescricao() {
		return descricao;
	}
	public Date getDtvenda() {
		return dtvenda;
	}
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Money getValorArquivoLiquido() {
		return valorArquivoLiquido;
	}
	public Money getValorArquivoTaxa() {
		return valorArquivoTaxa;
	}
	
	public void setValorArquivoLiquido(Money valorArquivoLiquido) {
		this.valorArquivoLiquido = valorArquivoLiquido;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setDtvenda(Date dtvenda) {
		this.dtvenda = dtvenda;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValorArquivo(Money valorArquivo) {
		this.valorArquivo = valorArquivo;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setPermitidoConciliacao(Boolean permitidoConciliacao) {
		this.permitidoConciliacao = permitidoConciliacao;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDtefetivapagamento(Date dtefetivapagamento) {
		this.dtefetivapagamento = dtefetivapagamento;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	public String getDoc() {
		return doc;
	}
	public void setDoc(String doc) {
		this.doc = doc;
	}
	public void setValorArquivoTaxa(Money valorArquivoTaxa) {
		this.valorArquivoTaxa = valorArquivoTaxa;
	}
}