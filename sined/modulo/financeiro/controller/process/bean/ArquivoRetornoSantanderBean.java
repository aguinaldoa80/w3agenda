package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;

public class ArquivoRetornoSantanderBean {

	private Documento documento;
	private Date dtcredito;
	private Money valor;
	private Money valorTarifa;
	private String nossonumero;
	private Boolean marcado;
	private String motivoTarifa;
	private String motivoTarifaDesc;
	private String ocorrencia;
	private Date dtocorrencia;
	
	private Boolean valordiferente;
	
	public Documento getDocumento() {
		return documento;
	}
	public Date getDtcredito() {
		return dtcredito;
	}
	public Money getValor() {
		return valor;
	}
	public Money getValorTarifa() {
		return valorTarifa;
	}
	public String getMotivoTarifa() {
		return motivoTarifa;
	}
	public String getMotivoTarifaDesc() {
		return motivoTarifaDesc;
	}
	public String getNossonumero() {
		return nossonumero;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public String getOcorrencia() {
		return ocorrencia;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setValorTarifa(Money valorTarifa) {
		this.valorTarifa = valorTarifa;
	}
	public void setMotivoTarifa(String motivoTarifa) {
		this.motivoTarifa = motivoTarifa;
	}
	public void setMotivoTarifaDesc(String motivoTarifaDesc) {
		this.motivoTarifaDesc = motivoTarifaDesc;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}
	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	
	public Boolean getValordiferente() {
		valordiferente = Boolean.FALSE;
		
		if(this.getValor() != null && this.getDocumento() != null && 
				this.getDocumento().getAux_documento() != null && 
				this.getDocumento().getAux_documento().getValoratual() != null && 
				this.getValor().compareTo(this.getDocumento().getAux_documento().getValoratual()) != 0){
			valordiferente = Boolean.TRUE;
		}
		
		return valordiferente;
	}
	public void setValordiferente(Boolean valordiferente) {
		this.valordiferente = valordiferente;
	}
	public Date getDtocorrencia() {
		return dtocorrencia;
	}
	public void setDtocorrencia(Date dtocorrencia) {
		this.dtocorrencia = dtocorrencia;
	}
}
