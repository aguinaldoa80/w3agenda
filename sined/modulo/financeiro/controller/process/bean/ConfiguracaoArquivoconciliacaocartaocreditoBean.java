package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Configuracaoconciliacaocartao;

public class ConfiguracaoArquivoconciliacaocartaocreditoBean {

	private Configuracaoconciliacaocartao arquivoRede;
	private Configuracaoconciliacaocartao arquivoCielo;
	
	@DisplayName("Rede")
	public Configuracaoconciliacaocartao getArquivoRede() {
		return arquivoRede;
	}
	public void setArquivoRede(Configuracaoconciliacaocartao arquivoRede) {
		this.arquivoRede = arquivoRede;
	}
	
	@DisplayName("Cielo")
	public Configuracaoconciliacaocartao getArquivoCielo() {
		return arquivoCielo;
	}
	public void setArquivoCielo(Configuracaoconciliacaocartao arquivoCielo) {
		this.arquivoCielo = arquivoCielo;
	}
}
