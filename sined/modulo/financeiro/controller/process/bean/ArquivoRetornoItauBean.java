package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;

public class ArquivoRetornoItauBean {

	private Documento documento;
	private Date dtcredito;
	private Date dtvencimento;
	private Money valor;
	private String nossonumero;
	private String ocorrencia;
	private Boolean marcado;
	private Date dtocorrencia;
	
	private Boolean valordiferente;
	
	public Documento getDocumento() {
		return documento;
	}
	public Date getDtcredito() {
		return dtcredito;
	}
	public Money getValor() {
		return valor;
	}
	public String getNossonumero() {
		return nossonumero;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public String getOcorrencia() {
		return ocorrencia;
	}
	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}	
	
	public Boolean getValordiferente() {
		valordiferente = Boolean.FALSE;
		
		if(this.getValor() != null && this.getDocumento() != null && 
				this.getDocumento().getAux_documento() != null && 
				this.getDocumento().getAux_documento().getValoratual() != null && 
				this.getValor().compareTo(this.getDocumento().getAux_documento().getValoratual()) != 0){
			valordiferente = Boolean.TRUE;
		}
		
		return valordiferente;
	}
	
	
	public void setValordiferente(Boolean valordiferente) {
		this.valordiferente = valordiferente;
	}
	public Date getDtocorrencia() {
		return dtocorrencia;
	}
	public void setDtocorrencia(Date dtocorrencia) {
		this.dtocorrencia = dtocorrencia;
	}
}
