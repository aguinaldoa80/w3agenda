package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.util.SinedDateUtils;



public class DevedoresBean{
	
	protected Empresa empresa;
	protected String cliente;
	protected String contato;
	protected String email;
	protected String contaatrasada;
	protected String contaatrasadareport;
	protected Documento documento;
	protected List<String> emailscontatos = new ArrayList<String>();	 
	protected String url;
	protected Pessoa pessoa;
	protected String telefone;
	protected Date ultimaCobranca;
	protected Date proximaCobranca;
	protected Integer cdcliente;
	protected Boolean protestada = Boolean.FALSE;
	
	protected String mensagem;
	protected Money valor;
	protected Money valoratual;
	
	protected List<Documento> listaDocumento;
	protected String whereInDocumento;
	
	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public List<String> getEmailscontatos() {
		return emailscontatos;
	}
	
	public String getCliente() {
		return cliente;
	}
	
	public String getContato() {
		return contato;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getContaatrasada() {
		return contaatrasada;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	
	public void setContato(String contato) {
		this.contato = contato;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setContaatrasada(String contaatrasada) {
		this.contaatrasada = contaatrasada;
	}
	
	public Documento getDocumento() {
		return documento;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setEmailscontatos(List<String> emailscontatos) {
		this.emailscontatos = emailscontatos;
	}
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Boolean getProtestada() {
		return protestada;
	}
	public void setProtestada(Boolean protestada) {
		this.protestada = protestada;
	}
	public Date getUltimaCobranca() {
		return ultimaCobranca;
	}
	public Date getProximaCobranca() {
		return proximaCobranca;
	}
	public void setUltimaCobranca(Date ultimaCobranca) {
		this.ultimaCobranca = ultimaCobranca;
	}
	public void setProximaCobranca(Date proximaCobranca) {
		this.proximaCobranca = proximaCobranca;
	}
	public Money getValoratual() {
		return valoratual;
	}
	public void setValoratual(Money valoratual) {
		this.valoratual = valoratual;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	public String getWhereInDocumento() {
		return whereInDocumento;
	}
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setWhereInDocumento(String whereInDocumento) {
		this.whereInDocumento = whereInDocumento;
	}
	public String getContaatrasadareport() {
		return contaatrasadareport;
	}
	public void setContaatrasadareport(String contaatrasadareport) {
		this.contaatrasadareport = contaatrasadareport;
	}
	
	public void addDocumento(Documento doc) {
		if(doc != null){
			if(this.listaDocumento == null) this.listaDocumento = new ArrayList<Documento>();
			this.listaDocumento.add(doc);
			
			if(StringUtils.isNotEmpty(this.whereInDocumento))
				this.whereInDocumento = this.whereInDocumento + ",";  
			this.whereInDocumento = this.whereInDocumento + doc.getCddocumento();  
			
			if(this.contaatrasada == null) this.contaatrasada = "";
			if(this.contaatrasadareport == null) this.contaatrasadareport = "";
			this.setContaatrasada(
					this.contaatrasada + 
					"<br>" + 
					"<a href=\"#\" onclick=\"redirecionaPaginaContaReceber(" + 
					doc.getCddocumento()+")\"" + 
					getStyleLinkContaAtrasada(doc) + 
					getOnmouseoverDtvencimentoAlteracao(doc) + 
					">"+ 
					doc.getNumeroCddocumento() + 
					"(" + 
					SinedDateUtils.toString(doc.getDtvencimento()) + 
					getAsterisco(doc) + 
					(doc.getAux_documento().getValoratual() != null ? " - " + doc.getAux_documento().getValoratual() : "") +
					(doc.getDtcartorio() != null ? " - <font color=\"red\">P</font>" : "") +
					")</a>");
			
			this.setContaatrasadareport(
					this.contaatrasadareport + 
					" | " + 
					doc.getNumeroCddocumento() +  
					"(" +  
					SinedDateUtils.toString(doc.getDtvencimento()) +  
					getAsterisco(doc) + 
					(doc.getAux_documento().getValoratual() != null ? " - " + doc.getAux_documento().getValoratual() : "") +
					(doc.getDtcartorio() != null ? " - P" : "") +
					")");
			
			if(this.getValor() == null) this.setValor(new Money());
			this.setValor(this.getValor().add(doc.getValor()));
			
			
			if(Documentoacao.BAIXADA_PARCIAL.equals(doc.getDocumentoacao())){
				if(doc.getValorRestante() == null) doc.setValorRestante(new Money());
				this.setValoratual(this.getValoratual().add(doc.getValorRestante()));
			}else if(doc.getAux_documento() != null){
				if(this.getValoratual() == null) this.setValoratual(new Money());
				this.setValoratual(this.getValoratual().add(doc.getAux_documento().getValoratual()));						
			}
			
			if(doc.getDtultimacobranca() != null && (getUltimaCobranca() == null ||
					SinedDateUtils.afterIgnoreHour(doc.getDtultimacobranca(), getUltimaCobranca()))){
				this.setUltimaCobranca(doc.getDtultimacobranca());				
			}
		}
	}

	public String getStyleLinkContaAtrasada(Documento documento){
		String cor = Banco.COR_PADRAO_PAINEL_COBRANCA;
		if (documento!=null && documento.getConta()!=null && documento.getConta().getBanco()!=null && documento.getConta().getBanco().getCorPainelCobranca()!=null){
			cor = documento.getConta().getBanco().getCorPainelCobranca();
		}
		return " style=\"color:#" + cor + "\" ";
	}
	
	public Date getDtvencimentoAlteracao(Documento documento){
		if (documento.getListaDocumentohistorico()!=null && !documento.getListaDocumentohistorico().isEmpty()){
			Date dtvencimento = documento.getDtvencimento();
			List<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class, documento.getListaDocumentohistorico());
			Collections.sort(listaDocumentohistorico, new Comparator<Documentohistorico>() {
				@Override
				public int compare(Documentohistorico o1, Documentohistorico o2) {
					return o2.getCddocumentohistorico().compareTo(o1.getCddocumentohistorico());
				}
			});
			for (Documentohistorico documentohistorico: listaDocumentohistorico){
				if (documentohistorico.getDtvencimento()!=null && SinedDateUtils.diferencaDias(dtvencimento, documentohistorico.getDtvencimento())!=0){
					return documentohistorico.getDtvencimento();
				}
			}
			
		}
		
		return null;
	}
	
	public String getAsterisco(Documento documento){
		return getDtvencimentoAlteracao(documento)==null ? "" : "*";
	}
	
	public String getOnmouseoverDtvencimentoAlteracao(Documento documento){
		Date dtvencimentoAlteracao = getDtvencimentoAlteracao(documento);
		if (dtvencimentoAlteracao!=null){
			return " onmouseover=\"Tip('Data de vencimento anterior: " + NeoFormater.getInstance().format(dtvencimentoAlteracao) + "');\"";
		}else {
			return "";
		}		
	}
}