package br.com.linkcom.sined.modulo.financeiro.controller.process.bean;

/**
 * 
 * @author Bruno Eust�quio
 *
 */

public class ArquivoRemessaBean {

	/* Header */

	protected String idRegistro; 						// Identifica��o registro
	protected String idRemessa; 						// Identifica��o de remessa
	protected String literalRemessa;					// Literal de remessa
	protected String cdServico;							// C�digo de servi�o
	protected String literalServico;					// Literal de servico
	protected String cdEmpresa;							// C�digo da empresa
	protected String nomeEmpresa; 						// Nome da Empresa 
	protected String cdBanco; 							// C�digo do Banco na C�mara de Compensa��o
	protected String nomeBanco; 						// Nome do Banco 
	protected String dtMovimento; 						// Data do Movimento
	protected String idSistema; 						// Identifica��o do sistema
	protected String numSequencialArquivo; 				// N�mero seq�encial do arquivo
	protected String numSequencialRegistro; 			// N�mero Seq�encial do Registro 	

	/* Corpo - D�bito ou cr�dito em conta corrente*/
	
	protected String idRegistroBody;
	protected String agenciaDebitoBody;
	protected String digitoAgenciaDebitoBody;
	protected String razaoContaCorrenteBody;
	protected String contaCorrenteBody;
	protected String digitoContacorrenteBody;
	protected String idEmpresaBody;
	protected String controleParticipanteBody;
	protected String cdBancoBody;
	protected String campoMultaBody;
	protected String percentualMultaBody;
	protected String idTituloBancoBody;
	protected String digitoConfNossoNumeroBody;
	protected String descontoBonificacaoBody;
	protected String condicaoEmissaoBody;
	protected String emitePapeletaCobrancaBody;
	protected String idOperacaoBody;
	protected String idRateioCreditoBody;
	protected String enderecamentoDebitoBody;
	protected String idOcorrenciaBody;
	protected String numeroDocumentoBody;
	protected String dtvencimentoBody;
	protected String valorBody;
	protected String bancoCobrancaBody;
	protected String agenciaDepositariaBody;
	protected String especieTituloBody;
	protected String idBody;
	protected String dtemissaoBody;
	protected String instrucao1Body;
	protected String instrucao2Body;
	protected String valorAtrasoBody;
	protected String dtLimiteDescontoBody;
	protected String valorDescontoBody;
	protected String valorIOFBody;
	protected String valorAbatimentoBody;
	protected String idTipoInscricaoBody;
	protected String numeroInscricaoSacadoBody;
	protected String nomeSacadoBody;
	protected String enderecoSacadoBody;
	protected String mensagem1Body;
	protected String cepBody;
	protected String sufixoCepBody;
	protected String mensagem2Body;
	protected String sequencialRegistroBody;
	
	/* Trailler */
	protected String idRegistroTrailler;
	protected String numSequencialRegistroTrailler; 
	
	/* Getters */

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}
	public String getCdBanco() {
		return cdBanco;
	}
	public String getNomeBanco() {
		return nomeBanco;
	}
	public String getDtMovimento() {
		return dtMovimento;
	}
	public String getNumSequencialArquivo() {
		return numSequencialArquivo;
	}
		
	/*  Setters */
	
	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}
	public void setCdBanco(String cdBanco) {
		this.cdBanco = cdBanco;
	}
	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}
	public void setDtMovimento(String dtMovimento) {
		this.dtMovimento = dtMovimento;
	}
	public void setNumSequencialArquivo(String numSequencialArquivo) {
		this.numSequencialArquivo = numSequencialArquivo;
	}
	public String getIdRegistro() {
		return idRegistro;
	}
	public String getIdRemessa() {
		return idRemessa;
	}
	public String getLiteralRemessa() {
		return literalRemessa;
	}
	public String getCdServico() {
		return cdServico;
	}
	public String getCdEmpresa() {
		return cdEmpresa;
	}
	public String getIdSistema() {
		return idSistema;
	}
	public String getNumSequencialRegistro() {
		return numSequencialRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	public void setIdRemessa(String idRemessa) {
		this.idRemessa = idRemessa;
	}
	public void setLiteralRemessa(String literalRemessa) {
		this.literalRemessa = literalRemessa;
	}
	public void setCdServico(String cdServico) {
		this.cdServico = cdServico;
	}
	public void setCdEmpresa(String cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	public void setIdSistema(String idSistema) {
		this.idSistema = idSistema;
	}
	public void setNumSequencialRegistro(String numSequencialRegistro) {
		this.numSequencialRegistro = numSequencialRegistro;
	}
	public String getLiteralServico() {
		return literalServico;
	}
	public void setLiteralServico(String literalServico) {
		this.literalServico = literalServico;
	}
	public String getIdRegistroBody() {
		return idRegistroBody;
	}
	public String getAgenciaDebitoBody() {
		return agenciaDebitoBody;
	}
	public String getDigitoAgenciaDebitoBody() {
		return digitoAgenciaDebitoBody;
	}
	public String getRazaoContaCorrenteBody() {
		return razaoContaCorrenteBody;
	}
	public String getContaCorrenteBody() {
		return contaCorrenteBody;
	}
	public String getDigitoContacorrenteBody() {
		return digitoContacorrenteBody;
	}
	public String getIdEmpresaBody() {
		return idEmpresaBody;
	}
	public String getControleParticipanteBody() {
		return controleParticipanteBody;
	}
	public String getCdBancoBody() {
		return cdBancoBody;
	}
	public String getCampoMultaBody() {
		return campoMultaBody;
	}
	public String getPercentualMultaBody() {
		return percentualMultaBody;
	}
	public String getIdTituloBancoBody() {
		return idTituloBancoBody;
	}
	public String getDigitoConfNossoNumeroBody() {
		return digitoConfNossoNumeroBody;
	}
	public String getDescontoBonificacaoBody() {
		return descontoBonificacaoBody;
	}
	public String getCondicaoEmissaoBody() {
		return condicaoEmissaoBody;
	}
	public String getEmitePapeletaCobrancaBody() {
		return emitePapeletaCobrancaBody;
	}
	public String getIdOperacaoBody() {
		return idOperacaoBody;
	}
	public String getIdRateioCreditoBody() {
		return idRateioCreditoBody;
	}
	public String getEnderecamentoDebitoBody() {
		return enderecamentoDebitoBody;
	}
	public String getIdOcorrenciaBody() {
		return idOcorrenciaBody;
	}
	public String getNumeroDocumentoBody() {
		return numeroDocumentoBody;
	}
	public String getDtvencimentoBody() {
		return dtvencimentoBody;
	}
	public String getValorBody() {
		return valorBody;
	}
	public String getBancoCobrancaBody() {
		return bancoCobrancaBody;
	}
	public String getAgenciaDepositariaBody() {
		return agenciaDepositariaBody;
	}
	public String getEspecieTituloBody() {
		return especieTituloBody;
	}
	public String getIdBody() {
		return idBody;
	}
	public String getDtemissaoBody() {
		return dtemissaoBody;
	}
	public String getInstrucao1Body() {
		return instrucao1Body;
	}
	public String getInstrucao2Body() {
		return instrucao2Body;
	}
	public String getValorAtrasoBody() {
		return valorAtrasoBody;
	}
	public String getDtLimiteDescontoBody() {
		return dtLimiteDescontoBody;
	}
	public String getValorDescontoBody() {
		return valorDescontoBody;
	}
	public String getValorIOFBody() {
		return valorIOFBody;
	}
	public String getValorAbatimentoBody() {
		return valorAbatimentoBody;
	}
	public String getIdTipoInscricaoBody() {
		return idTipoInscricaoBody;
	}
	public String getNumeroInscricaoSacadoBody() {
		return numeroInscricaoSacadoBody;
	}
	public String getNomeSacadoBody() {
		return nomeSacadoBody;
	}
	public String getEnderecoSacadoBody() {
		return enderecoSacadoBody;
	}
	public String getMensagem1Body() {
		return mensagem1Body;
	}
	public String getCepBody() {
		return cepBody;
	}
	public String getSufixoCepBody() {
		return sufixoCepBody;
	}
	public String getMensagem2Body() {
		return mensagem2Body;
	}
	public String getSequencialRegistroBody() {
		return sequencialRegistroBody;
	}
	public void setIdRegistroBody(String idRegistroBody) {
		this.idRegistroBody = idRegistroBody;
	}
	public void setAgenciaDebitoBody(String agenciaDebitoBody) {
		this.agenciaDebitoBody = agenciaDebitoBody;
	}
	public void setDigitoAgenciaDebitoBody(String digitoAgenciaDebitoBody) {
		this.digitoAgenciaDebitoBody = digitoAgenciaDebitoBody;
	}
	public void setRazaoContaCorrenteBody(String razaoContaCorrenteBody) {
		this.razaoContaCorrenteBody = razaoContaCorrenteBody;
	}
	public void setContaCorrenteBody(String contaCorrenteBody) {
		this.contaCorrenteBody = contaCorrenteBody;
	}
	public void setDigitoContacorrenteBody(String digitoContacorrenteBody) {
		this.digitoContacorrenteBody = digitoContacorrenteBody;
	}
	public void setIdEmpresaBody(String idEmpresaBody) {
		this.idEmpresaBody = idEmpresaBody;
	}
	public void setControleParticipanteBody(String controleParticipanteBody) {
		this.controleParticipanteBody = controleParticipanteBody;
	}
	public void setCdBancoBody(String cdBancoBody) {
		this.cdBancoBody = cdBancoBody;
	}
	public void setCampoMultaBody(String campoMultaBody) {
		this.campoMultaBody = campoMultaBody;
	}
	public void setPercentualMultaBody(String percentualMultaBody) {
		this.percentualMultaBody = percentualMultaBody;
	}
	public void setIdTituloBancoBody(String idTituloBancoBody) {
		this.idTituloBancoBody = idTituloBancoBody;
	}
	public void setDigitoConfNossoNumeroBody(String digitoConfNossoNumeroBody) {
		this.digitoConfNossoNumeroBody = digitoConfNossoNumeroBody;
	}
	public void setDescontoBonificacaoBody(String descontoBonificacaoBody) {
		this.descontoBonificacaoBody = descontoBonificacaoBody;
	}
	public void setCondicaoEmissaoBody(String condicaoEmissaoBody) {
		this.condicaoEmissaoBody = condicaoEmissaoBody;
	}
	public void setEmitePapeletaCobrancaBody(String emitePapeletaCobrancaBody) {
		this.emitePapeletaCobrancaBody = emitePapeletaCobrancaBody;
	}
	public void setIdOperacaoBody(String idOperacaoBody) {
		this.idOperacaoBody = idOperacaoBody;
	}
	public void setIdRateioCreditoBody(String idRateioCreditoBody) {
		this.idRateioCreditoBody = idRateioCreditoBody;
	}
	public void setEnderecamentoDebitoBody(String enderecamentoDebitoBody) {
		this.enderecamentoDebitoBody = enderecamentoDebitoBody;
	}
	public void setIdOcorrenciaBody(String idOcorrenciaBody) {
		this.idOcorrenciaBody = idOcorrenciaBody;
	}
	public void setNumeroDocumentoBody(String numeroDocumentoBody) {
		this.numeroDocumentoBody = numeroDocumentoBody;
	}
	public void setDtvencimentoBody(String dtvencimentoBody) {
		this.dtvencimentoBody = dtvencimentoBody;
	}
	public void setValorBody(String valorBody) {
		this.valorBody = valorBody;
	}
	public void setBancoCobrancaBody(String bancoCobrancaBody) {
		this.bancoCobrancaBody = bancoCobrancaBody;
	}
	public void setAgenciaDepositariaBody(String agenciaDepositariaBody) {
		this.agenciaDepositariaBody = agenciaDepositariaBody;
	}
	public void setEspecieTituloBody(String especieTituloBody) {
		this.especieTituloBody = especieTituloBody;
	}
	public void setIdBody(String idBody) {
		this.idBody = idBody;
	}
	public void setDtemissaoBody(String dtemissaoBody) {
		this.dtemissaoBody = dtemissaoBody;
	}
	public void setInstrucao1Body(String instrucao1Body) {
		this.instrucao1Body = instrucao1Body;
	}
	public void setInstrucao2Body(String instrucao2Body) {
		this.instrucao2Body = instrucao2Body;
	}
	public void setValorAtrasoBody(String valorAtrasoBody) {
		this.valorAtrasoBody = valorAtrasoBody;
	}
	public void setDtLimiteDescontoBody(String dtLimiteDescontoBody) {
		this.dtLimiteDescontoBody = dtLimiteDescontoBody;
	}
	public void setValorDescontoBody(String valorDescontoBody) {
		this.valorDescontoBody = valorDescontoBody;
	}
	public void setValorIOFBody(String valorIOFBody) {
		this.valorIOFBody = valorIOFBody;
	}
	public void setValorAbatimentoBody(String valorAbatimentoBody) {
		this.valorAbatimentoBody = valorAbatimentoBody;
	}
	public void setIdTipoInscricaoBody(String idTipoInscricaoBody) {
		this.idTipoInscricaoBody = idTipoInscricaoBody;
	}
	public void setNumeroInscricaoSacadoBody(String numeroInscricaoSacadoBody) {
		this.numeroInscricaoSacadoBody = numeroInscricaoSacadoBody;
	}
	public void setNomeSacadoBody(String nomeSacadoBody) {
		this.nomeSacadoBody = nomeSacadoBody;
	}
	public void setEnderecoSacadoBody(String enderecoSacadoBody) {
		this.enderecoSacadoBody = enderecoSacadoBody;
	}
	public void setMensagem1Body(String mensagem1Body) {
		this.mensagem1Body = mensagem1Body;
	}
	public void setCepBody(String cepBody) {
		this.cepBody = cepBody;
	}
	public void setSufixoCepBody(String sufixoCepBody) {
		this.sufixoCepBody = sufixoCepBody;
	}
	public void setMensagem2Body(String mensagem2Body) {
		this.mensagem2Body = mensagem2Body;
	}
	public void setSequencialRegistroBody(String sequencialRegistroBody) {
		this.sequencialRegistroBody = sequencialRegistroBody;
	}
	public String getIdRegistroTrailler() {
		return idRegistroTrailler;
	}
	public String getNumSequencialRegistroTrailler() {
		return numSequencialRegistroTrailler;
	}
	public void setIdRegistroTrailler(String idRegistroTrailler) {
		this.idRegistroTrailler = idRegistroTrailler;
	}
	public void setNumSequencialRegistroTrailler(String numSequencialRegistroTrailler) {
		this.numSequencialRegistroTrailler = numSequencialRegistroTrailler;
	}
	
	
	
	
	
}