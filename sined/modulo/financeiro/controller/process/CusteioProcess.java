package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.MetodoOrigemEnum;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.CusteioFiltroAndBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.thread.ProcessarCusteiroThread;

@Controller(path="/financeiro/process/Custeio",authorizationModule=ProcessAuthorizationModule.class)
public class CusteioProcess extends MultiActionController{
	//caso de uso 0103 0104 0105
	private final static String  CASO_USO_03_MSG_1 = "� obrigat�rio informar o per�odo.";
	private final static String  CASO_USO_03_MSG_2 = "� obrigat�rio informar o arquivo para processar o custeio.";
	private final static String  CASO_USO_03_MSG_3 = "N�o � poss�vel realizar o processo de custeio. O per�odo informado corresponde a um per�odo financeiro fechado.";
	private final static String  CASO_USO_03_MSG_5 = "A Data Fim do Per�odo n�o pode ser menor do que a Data In�cio!";
	private final static String  CASO_USO_04_MSG_1 = "N�o existem lan�amentos financeiros (documento ou movimenta��o financeira) ou movimenta��o de estoque para o per�odo informado com centro de custos que participam do custeio.";
	private final static String  CASO_USO_04_MSG_2 = "N�o existem projetos ativos para o per�odo selecionado.";
	private final static String  CASO_USO_05_MSG_7 = "Processamento n�o pode ser realizado. Formato de arquivo inv�lido.";
	
	private static final String SEPARADOR = ";";
	private static final String QUEBRARLINHA = "\n";
	private FechamentofinanceiroService fechamentoFinanceiroService;
	private EmpresaService empresaService;
	private CentrocustoService centroCustoService;
	private ProjetoService projetoService;

	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setCentroCustoService(CentrocustoService centroCustoService) {this.centroCustoService = centroCustoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setFechamentoFinanceiroService(FechamentofinanceiroService fechamentoFinanceiroService) {this.fechamentoFinanceiroService = fechamentoFinanceiroService;}
	
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, CusteioFiltroAndBean filtro) throws Exception{
		request.setAttribute("listaEmpresas", empresaService.findAtivos());
		return new ModelAndView("/process/custeio").addObject("filtro", filtro);
	}
	private boolean validadorGeral(WebRequestContext request, CusteioFiltroAndBean filtro,MetodoOrigemEnum origem) throws Exception {
		boolean isErro = false;
		if(filtro.getDtPeriodoFim()!=null || filtro.getDtPeriodoFim() != null){
			if(!filtro.getDtPeriodoInicio().before(filtro.getDtPeriodoFim())){
				isErro = true;
				request.addError(CASO_USO_03_MSG_5);
			}else if (MetodoOrigemEnum.PROCESSAR.equals(origem) && fechamentoFinanceiroService.haveFechamentoPeriodo(filtro.getEmpresa(),filtro.getDtPeriodoInicio(),filtro.getDtPeriodoFim(),null)){
				isErro = true;
				request.addError(CASO_USO_03_MSG_3);
			}
		}else{
			isErro = true;
			request.addError(CASO_USO_03_MSG_1);
		}
		if(MetodoOrigemEnum.GERAR.equals(origem)){
			if(isErro){
				return true;
			}
		}else if (MetodoOrigemEnum.PROCESSAR.equals(origem)){
			return validadorProcessar(request, filtro, isErro);
		}
		
		return false;
	}
	
	public ModelAndView gerarArquivo(WebRequestContext request, CusteioFiltroAndBean filtro) throws Exception{
		if(validadorGeral(request,filtro,MetodoOrigemEnum.GERAR)){
			return index(request, filtro);
		}
		
		List<Centrocusto> listaCentroCusto = centroCustoService.buscarParaCusteio(true, filtro.getDtPeriodoInicio(),filtro.getDtPeriodoFim(),filtro.getEmpresa());
		List<Projeto> listaProjeto = projetoService.buscarParaCusteio(filtro.getDtPeriodoInicio(),filtro.getDtPeriodoFim());
		
		if(SinedUtil.isListEmpty(listaCentroCusto)){
			request.addError(CASO_USO_04_MSG_1);
			return index(request, filtro);
		}
		if(SinedUtil.isListEmpty(listaProjeto)){
			request.addError(CASO_USO_04_MSG_2);
			return index(request, filtro);
		}
		
		return formatarAquivo(filtro,listaCentroCusto,listaProjeto);
	}
	

	private ModelAndView formatarAquivo(CusteioFiltroAndBean filtro, List<Centrocusto> listaCentroCusto,List<Projeto> listaProjeto) {
		StringBuilder strCSV = new StringBuilder();
		strCSV.append(SEPARADOR);
		strCSV.append(SEPARADOR);
		for (Projeto projeto : listaProjeto) {
			strCSV.append(projeto.getNome());
			strCSV.append(SEPARADOR);
		}
		strCSV.append(QUEBRARLINHA);
		strCSV.append(SEPARADOR);
		strCSV.append(SEPARADOR);
		for (Projeto projeto : listaProjeto) {
			strCSV.append(projeto.getCdprojeto()+"");
			strCSV.append(SEPARADOR);
		}
		strCSV.append(QUEBRARLINHA);
		for (Centrocusto centroCusto: listaCentroCusto) {
			strCSV.append(centroCusto.getNome() +"");
			strCSV.append(SEPARADOR);
			strCSV.append(centroCusto.getCdcentrocusto() +"");
			strCSV.append(QUEBRARLINHA);
		}
		Resource resource = new Resource();
		resource.setContentType("custeio/csv");
		resource.setFileName("custeio_" + SinedUtil.FORMATADOR_DATA.format(filtro.getDtPeriodoInicio()) + "_" +  SinedUtil.FORMATADOR_DATA.format(filtro.getDtPeriodoFim()) + ".csv");
		resource.setContents(strCSV.toString().getBytes());
		return new ResourceModelAndView(resource);
	}
	
	
	public ModelAndView processarArquivo(WebRequestContext request, CusteioFiltroAndBean filtro) throws Exception{
		if(validadorGeral(request,filtro,MetodoOrigemEnum.PROCESSAR)){
			return index(request, filtro);
		}
		String strArquivo = new String(filtro.getArquivo().getContent());
		String[] listaLinhas = strArquivo.split("\\r?\\n");
		HashMap<Integer, Integer> mapCdCentroCusto = new HashMap<Integer, Integer>(); // armazena linha no valor
		HashMap<Integer, Integer> mapCdProjeto = new HashMap<Integer, Integer>();   // armazena ordem no valor 
		
		try{
			int aux = 3;
			for (int i = 0; i < listaLinhas.length; i ++) {
				String[] celulasDaLinha = listaLinhas[i].split(";");
				Double porcentagem = 0.0;
				if (i > 1){
					for(int x =0; x < celulasDaLinha.length; x ++){
						String celula =  celulasDaLinha[x];
						if(x > 1){
							if(celula != null && !celula.equals("")){
								try{
									porcentagem = porcentagem + Double.parseDouble(celula) ;
								}catch (Exception e) {
									request.addError("Erro ao processar a planilha, por favor validar os dados. Percentual: " + (celula != null ? (celula.equals("") ? "em branco" : celula) : "nulo") + " - Linha: " + x + " Coluna: " + (x + 1)); 
									return index(request, filtro);
								}
							}
						}else if(x == 1){
							String celulaNome  =celulasDaLinha[x];
							if(mapCdCentroCusto.containsKey(Integer.parseInt(celulasDaLinha[1]))){
								request.addError("Planilha inv�lida. Existem centro de custos repetidos.  Remova o centro de custo "+celulaNome+" linha "+i+" repetido.");
								return index(request, filtro);
							}else{
								mapCdCentroCusto.put(Integer.parseInt(celulasDaLinha[1]), i);
							}
						}
					}
					if(porcentagem !=100){
						int linha = i +1;
						request.addError("Planilha inv�lida. Os percentuais da linha "+linha+" n�o totaliza 100 porcento.");
						return index(request, filtro);
					}
				}else{
					if(i == 1){
						for(int x =0; x + 2 < celulasDaLinha.length; x ++){
							String[] linhaNome = listaLinhas[i -1].split(";"); 
							String celulaNome =  linhaNome[x + 2];
							Integer cdProjeto =  Integer.parseInt(celulasDaLinha[x + 2]);
							if(mapCdProjeto.containsKey(cdProjeto)){
								request.addError("Planilha inv�lida. Existem projetos repetidos.  Remova o projeto "+celulaNome+" / "+cdProjeto+" repetido.");
								return index(request, filtro);
							}else{
								mapCdProjeto.put(cdProjeto, aux);
								aux++;
							}
						}
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro ao processar a planilha, por favor validar os dados"); 
		}
		HashMap <Integer,Projeto> mapProjeto;
		HashMap <Integer,Centrocusto> mapCentroCusto;
		try{
			mapProjeto = criarMapProjeto(mapCdProjeto);
			mapCentroCusto = criarMapCentroCusto(mapCdCentroCusto);
		}catch (Exception e) {
			request.addError(e.getMessage());
			return new ModelAndView("/process/custeio").addObject("filtro", filtro);
		}
		ProcessarCusteiroThread thread = new ProcessarCusteiroThread(mapCdCentroCusto, mapCdProjeto,filtro,mapProjeto,mapCentroCusto,SinedUtil.getStringConexaoBanco(),SinedUtil.getUsuarioLogado());
//		thread.run();
		thread.start();
		request.addMessage("Planilha est� sendo processada em segundo plano. ap�s o t�rmino o hist�rico com resultado estar� dispon�vel para consulta");
		

		return new ModelAndView("/process/custeio").addObject("filtro", filtro);
	}

	
	
	
		
	private boolean validadorProcessar(WebRequestContext request, CusteioFiltroAndBean filtro,boolean isErro) throws Exception {
		if(filtro.getArquivo() == null || filtro.getArquivo().getContent() == null){
			isErro = true;
			request.addError(CASO_USO_03_MSG_2);
		}
		if(isErro){
			return true;
		}
		String nomeArq = filtro.getArquivo().getNome();
		if(nomeArq.length() < 3){
			request.addMessage(CASO_USO_05_MSG_7, MessageType.ERROR);
			return true;
		}else{
			String extencao = nomeArq.substring(nomeArq.length() - 3);
			if (!extencao.equalsIgnoreCase("CSV")) {
				request.addMessage(CASO_USO_05_MSG_7, MessageType.ERROR);
				return true;
			}
		}
		List<Centrocusto> listaCentroCusto = centroCustoService.buscarParaCusteio(false, filtro.getDtPeriodoInicio(),filtro.getDtPeriodoFim(),filtro.getEmpresa());
		List<Projeto> listaProjeto = projetoService.buscarParaCusteio(filtro.getDtPeriodoInicio(),filtro.getDtPeriodoFim());
		
		if(SinedUtil.isListEmpty(listaCentroCusto)){
			request.addError(CASO_USO_04_MSG_1);
			return true;
		}
		if(SinedUtil.isListEmpty(listaProjeto)){
			request.addError(CASO_USO_04_MSG_2);
			return true;
		}
		return false;
	} 
	
	
	
	private HashMap <Integer,Projeto> criarMapProjeto(HashMap<Integer, Integer> mapCdProjeto){
		ProjetoService projetoService =  ProjetoService.getInstance();
		HashMap<Integer,Projeto> mapProjeto = new HashMap<Integer,Projeto>();
		for(Entry<Integer, Integer> entry :	mapCdProjeto.entrySet()){
			Integer key = entry.getKey();
			Integer	valor =entry.getValue();
			Projeto projeto = projetoService.buscarParaProcessarCusteio(key);
			if(projeto != null && projeto.getCdprojeto() != null){
				mapProjeto.put(key, projeto);
			}else{
				throw new SinedException("Planilha inv�lida. Existem projetos inexistentes na planilha informada.  Remova o projeto "+key+" / "+valor+" ou fa�a a corre��o.");
			}
		}
		return mapProjeto;
	}
	
	private HashMap <Integer,Centrocusto> criarMapCentroCusto(HashMap<Integer, Integer> mapCdCentroCusto){
		CentrocustoService centroCustoService = CentrocustoService.getInstance();
		HashMap<Integer, Centrocusto> mapCentroCusto = new HashMap<Integer, Centrocusto>();
		for(Entry<Integer, Integer> entry :	mapCdCentroCusto.entrySet()){
			Integer key = entry.getKey();
			Integer	valor =entry.getValue();
			Centrocusto centroCusto = centroCustoService.buscarParaProcessarCusteio(key);
			if(centroCusto != null && centroCusto.getCdcentrocusto() != null){
				mapCentroCusto.put(key,centroCusto);
			}else{
				throw new SinedException("Planilha inv�lida. Existem centro de custos inexistentes na planilha informada ou centro de custo que n�o tem o flag 'PARTICIPAR RATEIO' marcada. Remova o centro de custo da linha "+valor+1+" codigo "+key+" ou fa�a a corre��o.");
			}
		}
		return mapCentroCusto;
	}
	
	
	
	
	
	
	
	
	

	
	

	
}
