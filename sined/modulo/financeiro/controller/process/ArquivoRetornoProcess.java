package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroDetalheTRetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroDetalheTRetornoBB240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetorno400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetornoBB400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetornoItau400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetornoRural400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRegistroRetornoSantander400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetorno240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetorno400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoBB240;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoBB400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoItau400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoRural400;
import br.com.linkcom.lkbanco.filereturn.util.EDIRetornoSantander400;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoconfirmacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoOcorrenciaTarifa;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoarquivoretorno;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivobancarioService;
import br.com.linkcom.sined.geral.service.ArquivobancariodocumentoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContacorrenteService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoconfirmacaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoBradescoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoBrasilBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoCAIXABean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoItauBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoRuralBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoSantanderBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.report.MergeReport;

@Controller(
		path="/financeiro/process/ArquivoRetorno",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ArquivoRetornoProcess extends MultiActionController{
	
	private static final String ARQUIVORETORNO_SESSION = "ARQUIVORETORNO_SESSION";
	private ArquivobancarioService arquivobancarioService;
	private MovimentacaoService movimentacaoService;
	private RateioitemService rateioitemService;
	private RateioService rateioService;
	private EntregaService entregaService;
	private ContareceberService contareceberService;
	private ContacorrenteService contacorrenteService;
	private DocumentoService documentoService;
	private ArquivoService arquivoService;
	private ArquivoDAO arquivoDAO;
	private ParametrogeralService parametrogeralService;
	private ContratoService contratoService;
	private NotaDocumentoService notaDocumentoService;
	private ContaService contaService;
	private DocumentoconfirmacaoService documentoconfirmacaoService;
	private ArquivobancariodocumentoService arquivobancariodocumentoService;
	
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {this.contacorrenteService = contacorrenteService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setEntregaService(EntregaService entregaService) {this.entregaService = entregaService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {this.arquivobancarioService = arquivobancarioService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setDocumentoconfirmacaoService(DocumentoconfirmacaoService documentoconfirmacaoService) {this.documentoconfirmacaoService = documentoconfirmacaoService;}
	public void setArquivobancariodocumentoService(ArquivobancariodocumentoService arquivobancariodocumentoService) {this.arquivobancariodocumentoService = arquivobancariodocumentoService;}
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, ArquivoRetornoBean bean) throws Exception{
		
		if (bean != null && bean.getArquivo() != null) {
			if (bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.ITAU)) {
				try {
					arquivobancarioService.processaArquivoRetorno(bean);
				} catch (Exception e) {
					request.addError(e.getMessage());
					return new ModelAndView("/process/arquivoretorno",
							"filtro", new ArquivoRetornoBean());
				}
				Movimentacao movimentacao = bean.getMovimentacao();
				// S� REDIRECIONA PARA OUTRA P�GINA SE TIVER DOCUMENTO REPROVADO 
				if (bean.getListaDocumentoReprovado() != null
						&& bean.getListaDocumentoReprovado().size() > 0) {

					// SE TIVER ALGUM DOCUMENTO APROVADO VAI ABRIR A TELA DE MOVIMENTA��O FINANCEIRA
					if (bean.getListaDocumentoAprovado() != null
							&& bean.getListaDocumentoAprovado().size() > 0) {

						if (movimentacao != null
								&& movimentacao.getCdmovimentacao() != null) {
							List<Rateioitem> listaItens = rateioitemService
									.findByDocumentos(CollectionsUtil
											.listAndConcatenate(
													bean
															.getListaDocumentoAprovado(),
													"cddocumento", ","));

							for (Rateioitem item : listaItens) {
								item.setPercentual(entregaService
										.calculaPercentual(
												bean.getValortotal(), item
														.getValor()));
							}

							entregaService.reCalculaPercentual(listaItens);
							rateioitemService.agruparItensRateio(listaItens);
							Rateio rateio = new Rateio();
							rateio.setListaRateioitem(listaItens);
							rateioService.limpaReferenciaRateio(rateio);
							rateioService.calculaValoresRateio(rateio, bean
									.getValortotal());

							request.getSession().setAttribute(
									"rateioDocumentos", rateio);
							request.getSession().setAttribute(
									"valorTotalDocumentos",
									bean.getValortotal());
							return new ModelAndView(
									"redirect:/financeiro/crud/Movimentacao?ACAO=editar&cdmovimentacao="
											+ movimentacao.getCdmovimentacao()
											+ "&fromArquivoBancario=true");

						} else {

							request
									.addError("N�o foi poss�vel encontrar a movimenta��o das contas que est�o no arquivo retorno.");
							return new ModelAndView("/process/arquivoretorno",
									"filtro", new ArquivoRetornoBean());
						}
					}

					// SE N�O TIVER DOCUMENTO APROVADO VAI DELETAR A MOVIMENTA��O FINANCEIRA DAQUELES DOCUMENTO
					else {

						if (movimentacao != null
								&& movimentacao.getCdmovimentacao() != null) {
							movimentacaoService
									.deleteMovimentacao(movimentacao);
							request
									.addMessage("Todos os pagamentos rejeitados. A movimenta��o financeira foi apagada do sistema.");
							return new ModelAndView("/process/arquivoretorno",
									"filtro", new ArquivoRetornoBean());

						} else {

							request
									.addError("N�o foi poss�vel encontrar a movimenta��o das contas que est�o no arquivo retorno.");
							return new ModelAndView("/process/arquivoretorno",
									"filtro", new ArquivoRetornoBean());

						}
					}
				}
				// SE N�O TIVER NENHUM DOCUMENTO REPROVADO ELE S� MOSTRA NA TELA OS DOCUMENTO APROVADOS
				request.addMessage("Todos os pagamentos aprovados.");
				request.setAttribute("listagem", Boolean.TRUE);
				bean.setArquivo(null);
			} else if(bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.ITAUCNAB400)) {
				try {
					EDIRetornoItau400 retornoItau400 = contareceberService.processaArquivoRetornoItau400(bean.getArquivo());

					request.getSession().setAttribute(ARQUIVORETORNO_SESSION, bean.getArquivo());
					
					String cdbanco = retornoItau400.getHeaderRetorno400().getCdbanco();
					String cdcedente = retornoItau400.getConta();
					String cdagencia = retornoItau400.getAgencia();
					
					Conta conta = contacorrenteService.findByAgenciaNumeroBanco(cdbanco, cdagencia, cdcedente);
					
					bean.setConta(conta);
					
					if(conta == null){
						throw new SinedException("N�o foi poss�vel obter a conta banc�ria.");
					}
					
					List<ArquivoRetornoItauBean> listaContaItau = this.getListaContaReceberByRetornoItau400(bean, retornoItau400.getRegistroRetornoItau400());
					bean.setListaContaItau(listaContaItau);
					
					bean.setArquivo(null);
					
					request.setAttribute("listagemItau", Boolean.TRUE);
				} catch (Exception e) {
					request.addError(e.getMessage());
					return new ModelAndView("/process/arquivoretorno",
							"filtro", new ArquivoRetornoBean());
				}				
			} else if(bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.CAIXASINCO240)){
				
				try {
					EDIRetorno240 retorno240 = contareceberService.processaArquivoRetornoCAIXASINCO240(bean.getArquivo());
					List<ArquivoRetornoCAIXABean> listaConta = this.getListaContaReceberByRetorno(bean, retorno240.getRegistroDetalheTRetorno240());
					
					bean.setListaConta(listaConta);
					
					String cdagencia = retorno240.getHeaderRetorno240().getCdagencia().substring(1, retorno240.getHeaderRetorno240().getCdagencia().length());
					String cdbanco = retorno240.getHeaderRetorno240().getCdbanco();
					String cdcedente = retorno240.getHeaderRetorno240().getCdcedente();
					
					Conta conta = contacorrenteService.findByAgenciaContaBanco(cdbanco, cdagencia, cdcedente);
					
					bean.setConta(conta);
					
					if(conta == null){
						throw new SinedException("N�o foi poss�vel obter a conta banc�ria.");
					}
					
					bean.setArquivo(null);
					
					request.setAttribute("listagemCAIXA", Boolean.TRUE);
				} catch (Exception e) {
					//e.printStackTrace();
					request.addError(e.getMessage());
					return new ModelAndView("/process/arquivoretorno", "filtro", new ArquivoRetornoBean());
				}
			}else if (bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.BRADESCO)) {
				try {
					EDIRetorno400 retorno400 = contareceberService.processaArquivoRetornoBradesco400(bean.getArquivo());
					List<ArquivoRetornoBradescoBean> listaConta = this.getListaContaReceberByRetorno400(bean, retorno400.getRegistroRetorno400());

					bean.setListaContaBradesco(listaConta);
					request.getSession().setAttribute(ARQUIVORETORNO_SESSION, bean.getArquivo());
					
					String cdbanco = retorno400.getHeaderRetorno400().getCdbanco();
					String cdcedente = retorno400.getConta();
					String cdagencia = retorno400.getAgencia();
					
					Conta conta = contacorrenteService.findByAgenciaNumeroBanco(cdbanco, cdagencia, cdcedente);
					
					// RETIRAR ZEROS A ESQUERDA DOS DADOS DO ARQUIVO
					if(conta == null){
						String agencia_aux = new String(cdagencia);
						while(agencia_aux.length() > 0 && agencia_aux.startsWith("0")){
							agencia_aux = agencia_aux.substring(1);
							
							conta = contacorrenteService.findByAgenciaNumeroBanco(cdbanco, agencia_aux, cdcedente);
							if(conta != null) break;
							
							String numero_aux = new String(cdcedente);
							while(numero_aux.length() > 0 && numero_aux.startsWith("0")){
								numero_aux = numero_aux.substring(1);
								
								conta = contacorrenteService.findByAgenciaNumeroBanco(cdbanco, agencia_aux, numero_aux);
								if(conta != null) break;
							}
							if(conta != null) break;
						}
					}
					
					// RETIRAR ZEROS A ESQUERDA DOS DADOS DO W3ERP
					if(conta == null){
						List<Conta> listaContabancaria = contaService.findAllContas(Integer.parseInt(cdbanco));
						for (Conta contabancaria : listaContabancaria) {
							String agencia = contabancaria.getAgencia();
							String numero = contabancaria.getNumero();
							
							String agencia_aux = new String(agencia);
							while(agencia_aux.length() > 0 && agencia_aux.startsWith("0")){
								agencia_aux = agencia_aux.substring(1);
								
								if(cdagencia.equals(agencia_aux) && cdcedente.equals(numero)){
									conta = contabancaria;
									break;
								}
								
								String numero_aux = new String(numero);
								while(numero_aux.length() > 0 && numero_aux.startsWith("0")){
									numero_aux = numero_aux.substring(1);
									
									if(cdagencia.equals(agencia_aux) && cdcedente.equals(numero_aux)){
										conta = contabancaria;
										break;
									}
								}
								
								if(conta != null) break;
							}
							
							if(conta != null) break;
						}
					}
					
					if(conta == null){
						throw new SinedException("N�o foi poss�vel obter a conta banc�ria.");
					}
					
					bean.setConta(conta);
					bean.setArquivo(null);
					
					request.setAttribute("listagemBradesco", Boolean.TRUE);
				} catch (Exception e) {
					request.addError(e.getMessage());
					//e.printStackTrace();
					return new ModelAndView("/process/arquivoretorno", "filtro", new ArquivoRetornoBean());
				}
			}else if (bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.REAL)) {
				try {
					EDIRetorno400 retorno400 = contareceberService.processaArquivoRetornoBradesco400(bean.getArquivo());
					List<ArquivoRetornoBradescoBean> listaConta = this.getListaContaReceberByRetorno400(bean, retorno400.getRegistroRetorno400());

					bean.setListaContaBradesco(listaConta);
					request.getSession().setAttribute(ARQUIVORETORNO_SESSION, bean.getArquivo());
					
					String cdbanco = retorno400.getHeaderRetorno400().getCdbanco();
					String cdcedente = retorno400.getConta();
					String cdagencia = retorno400.getAgencia();
					
					Conta conta = contacorrenteService.findByAgenciaNumeroBanco(cdbanco, cdagencia, cdcedente);
					
					bean.setConta(conta);
					
					if(conta == null){
						throw new SinedException("N�o foi poss�vel obter a conta banc�ria.");
					}
					
					bean.setArquivo(null);
					
					request.setAttribute("listagemBradesco", Boolean.TRUE);
				} catch (Exception e) {
					request.addError(e.getMessage());
					//e.printStackTrace();
					return new ModelAndView("/process/arquivoretorno", "filtro", new ArquivoRetornoBean());
				}
			}else if(bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.BRASILCNAB240)){
				try {
					EDIRetornoBB240 retornoBB240 = contareceberService.processaArquivoRetornoBancoBrasil240(bean.getArquivo());
					List<ArquivoRetornoBrasilBean> listaConta = this.getListaContaReceberByRetornoBB240(bean, retornoBB240.getRegistroDetalheTRetornoBB240());
					
					bean.setListaContaBB(listaConta);
					
					String cdagencia = new Integer(Integer.parseInt(retornoBB240.getHeaderRetornoBB240().getCdagencia())).toString();
					String cdbanco = retornoBB240.getHeaderRetornoBB240().getCdbanco();
					String cdcedente = new Integer(Integer.parseInt(retornoBB240.getHeaderRetornoBB240().getCdcedente())).toString();
					
					Conta conta = contacorrenteService.findByAgenciaContaBanco(cdbanco, cdagencia, cdcedente);
					
					bean.setConta(conta);
					
					if(conta == null){
						throw new SinedException("N�o foi poss�vel obter a conta banc�ria.");
					}
					
					bean.setArquivo(null);
					
					request.setAttribute("listagemBB", Boolean.TRUE);
				} catch (Exception e) {
					//e.printStackTrace();
					request.addError(e.getMessage());
					return new ModelAndView("/process/arquivoretorno", "filtro", new ArquivoRetornoBean());
				}		
			}else if(bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.BRASILCNAB400)){
				try {
					EDIRetornoBB400 retornoBB400 = contareceberService.processaArquivoRetornoBancoBrasil400(bean.getArquivo());
					List<ArquivoRetornoBrasilBean> listaConta = this.getListaContaReceberByRetornoBB400(bean, retornoBB400.getRegistroRetornoBB400());
					
					bean.setListaContaBB(listaConta);
					
					String cdbanco = retornoBB400.getHeaderRetorno400().getCdbanco();
					String cdagencia = retornoBB400.getAgencia();					
					String cdcedente = retornoBB400.getConvenio();
					
					Conta conta = contacorrenteService.findByAgenciaContaBanco(cdbanco, cdagencia, cdcedente);
					
					bean.setConta(conta);
					
					if(conta == null){
						throw new SinedException("N�o foi poss�vel obter a conta banc�ria.");
					}
					
					bean.setArquivo(null);
					
					request.setAttribute("listagemBB", Boolean.TRUE);
				} catch (Exception e) {
					//e.printStackTrace();
					request.addError(e.getMessage());
					return new ModelAndView("/process/arquivoretorno", "filtro", new ArquivoRetornoBean());
				}		
			} else if(bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.SANTANDER)){
				try {
					EDIRetornoSantander400 retornoSantander400 = contareceberService.processaArquivoRetornoSantander400(bean.getArquivo());
					List<ArquivoRetornoSantanderBean> listaContaSantander = this.getListaContaReceberByRetornoSantander400(bean, retornoSantander400.getRegistroRetornoSantander400());

					bean.setListaContaSantander(listaContaSantander);
					request.getSession().setAttribute(ARQUIVORETORNO_SESSION, bean.getArquivo());
					
					String cdcedente = retornoSantander400.getConta();
					String cdagencia = retornoSantander400.getAgencia();
					
					Conta conta = contacorrenteService.findByAgenciaNumeroBanco(null, cdagencia, cdcedente);
					
					bean.setConta(conta);
					
					if(conta == null){
						throw new SinedException("N�o foi poss�vel obter a conta banc�ria.");
					}
					
					bean.setArquivo(null);
					
					request.setAttribute("listagemSantander", Boolean.TRUE);
				} catch (Exception e) {
					request.addError(e.getMessage());
					return new ModelAndView("/process/arquivoretorno",
							"filtro", new ArquivoRetornoBean());
				}			
			}else if(bean.getTipoarquivoretorno().equals(Tipoarquivoretorno.RURAL)){
				try {
					EDIRetornoRural400 retornoRural400 = contareceberService.processaArquivoRetornoRural400(bean.getArquivo());
					List<ArquivoRetornoRuralBean> listaContaRural = this.getListaContaReceberByRetornoRural400(bean, retornoRural400.getRegistroRetornoRural400());

					bean.setListaContaRural(listaContaRural);
					request.getSession().setAttribute(ARQUIVORETORNO_SESSION, bean.getArquivo());
					
					String cdcedente = retornoRural400.getConta();
					String cdagencia = retornoRural400.getAgencia();
					
					Conta conta = contacorrenteService.findByAgenciaNumeroBanco(null, cdagencia, cdcedente);
					
					bean.setConta(conta);
					
					if(conta == null){
						throw new SinedException("N�o foi poss�vel obter a conta banc�ria.");
					}
					
					bean.setArquivo(null);
					
					request.setAttribute("listagemRural", Boolean.TRUE);
				} catch (Exception e) {
					request.addError(e.getMessage());
					return new ModelAndView("/process/arquivoretorno",
							"filtro", new ArquivoRetornoBean());
				}			
			}
		}
		
		return new ModelAndView("/process/arquivoretorno","filtro",bean);
		
	}
	
	private List<ArquivoRetornoCAIXABean> getListaContaReceberByRetorno(ArquivoRetornoBean beanAux, List<EDIRegistroDetalheTRetorno240> list) {
		List<ArquivoRetornoCAIXABean> listaBean = new ArrayList<ArquivoRetornoCAIXABean>();
		ArquivoRetornoCAIXABean bean;
		String dtCreditoStr;
		Date dtCredito;
		String dtDebitoTarifaStr;
		Date dtDebitoTarifa;
		String dtOcorrenciaStr;
		Date dtOcorrencia;
		Integer cddocumento;
		Money valor;
		Money valorTarifa;
		String motivoTarifa;
		Documento doc;
		
		Money valorTotalPagamento = new Money(0.0);
		Money valorTotalReceber = new Money(0.0);
		for (EDIRegistroDetalheTRetorno240 detalheTRetorno240 : list) {
			bean = new ArquivoRetornoCAIXABean();
			dtCreditoStr = detalheTRetorno240.getDtCredito();
			dtOcorrenciaStr = detalheTRetorno240.getDtOcorrencia();
			dtDebitoTarifaStr = detalheTRetorno240.getDtDebitoTarifa();
			try {
				if("00/00/0000".equals(dtCreditoStr)){
					dtCredito = null;
				}else {
					dtCredito = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtCreditoStr).getTime());
				}
				
				if(dtOcorrenciaStr != null && !"".equals(dtOcorrenciaStr)){
					dtOcorrencia = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtOcorrenciaStr).getTime());
				} else dtOcorrencia = null;
				
				if(dtDebitoTarifaStr != null && !"".equals(dtDebitoTarifaStr)){
					dtDebitoTarifa = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtDebitoTarifaStr).getTime());
				} else dtDebitoTarifa = null;
				
				cddocumento = Integer.parseInt(detalheTRetorno240.getUsoEmpresa().substring(2, detalheTRetorno240.getUsoEmpresa().length()));
				valor = new Money(Integer.parseInt(detalheTRetorno240.getValorPrincipal()), true);
				valorTarifa = new Money(Integer.parseInt(detalheTRetorno240.getValortarifa()), true);
				motivoTarifa = detalheTRetorno240.getMotivoTarifa();
			} catch (Exception e) {
				if(!"02".equals(detalheTRetorno240.getCdmovimento()) && !"06".equals(detalheTRetorno240.getCdmovimento())){
					continue;
				}
				throw new SinedException("Erro na transforma��o da data de cr�dito e/ou no nosso n�mero.");
			}
			
			
			doc = contareceberService.loadForRetorno(new Documento(cddocumento));
			bean.setDocumento(doc);
			bean.setValor(valor);
			bean.setNossonumero(detalheTRetorno240.getUsoEmpresa());
			bean.setValorTarifa(valorTarifa);
			bean.setMotivoTarifaDesc(descricaoMotivoTarifa(motivoTarifa));
			bean.setMotivoTarifa(motivoTarifa);
			bean.setDtcredito(dtCredito);
			bean.setDtocorrencia(dtOcorrencia);
			bean.setDtdebitotarifa(dtDebitoTarifa);
			
			valorTotalPagamento = valorTotalPagamento.add(valor);
			if(doc != null)
				valorTotalReceber = valorTotalReceber.add(doc.getAux_documento() != null && doc.getAux_documento().getValoratual() != null ? doc.getAux_documento().getValoratual() : new Money(0.0));
			listaBean.add(bean);
		}
		
		beanAux.setValorTotalPagamento(valorTotalPagamento);
		beanAux.setValorTotalContaReceber(valorTotalReceber);
		return listaBean;
	}
	
	private List<ArquivoRetornoBrasilBean> getListaContaReceberByRetornoBB240(ArquivoRetornoBean beanAux, List<EDIRegistroDetalheTRetornoBB240> list) {
		List<ArquivoRetornoBrasilBean> listaBean = new ArrayList<ArquivoRetornoBrasilBean>();
		ArquivoRetornoBrasilBean bean;
		String dtCreditoStr;
		Date dtCredito;
		String dtOcorrenciaStr;
		Date dtOcorrencia;
		Integer cddocumento;;
		Integer cddocumentoNossonumero;
		String cdocorrencia;
		Money valor;
		Money valorTarifa;
		String motivoTarifa;
		Documento doc;
		Boolean documentoNaoEncontrado = false;
		Boolean erroTransformacao = false;
		List<String> listaDocumentoNaoEncontrado = new ArrayList<String>(); 
		List<String> listaErroTransformacao = new ArrayList<String>();		
		
		Money valorTotalPagamento = new Money(0.0);
		Money valorTotalReceber = new Money(0.0);
		for (EDIRegistroDetalheTRetornoBB240 detalheTRetornoBB240 : list) {
			bean = new ArquivoRetornoBrasilBean();
			dtCreditoStr = detalheTRetornoBB240.getDtCredito();
			dtOcorrenciaStr = detalheTRetornoBB240.getDtOcorrencia();
			dtCredito = null;
			doc = null;
			cddocumento = null;
			cddocumentoNossonumero = null;
			try {
				if(dtCreditoStr != null && !dtCreditoStr.trim().equals(""))
					dtCredito = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtCreditoStr).getTime());
				if(dtOcorrenciaStr != null && !"".equals(dtOcorrenciaStr)){
					dtOcorrencia = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtOcorrenciaStr).getTime());
					bean.setDtocorrencia(dtOcorrencia);
				}
				if(detalheTRetornoBB240.getUsoEmpresa() != null && StringUtils.isNotBlank(detalheTRetornoBB240.getUsoEmpresa())){
					cddocumento = Integer.parseInt(detalheTRetornoBB240.getUsoEmpresa().trim());
				}else {
					cddocumentoNossonumero = Integer.parseInt(detalheTRetornoBB240.getNossoNumero().trim());
				}
				cdocorrencia = detalheTRetornoBB240.getCdmovimento();
				valor = new Money(Integer.parseInt(detalheTRetornoBB240.getValorPrincipal()), true);
				valorTarifa = new Money(Integer.parseInt(detalheTRetornoBB240.getValortarifa()), true);
				motivoTarifa = detalheTRetornoBB240.getMotivoTarifa().trim();
			} catch (Exception e) {
//				e.printStackTrace();
				erroTransformacao = true;
				listaErroTransformacao.add(detalheTRetornoBB240.getNossoNumero());
				continue;
			}
			bean.setDtcredito(dtCredito);
			
			if (cdocorrencia.equals("02") || cdocorrencia.equals("06")) {
				if(cddocumento != null){
					doc = contareceberService.loadForRetorno(new Documento(cddocumento));
				}else if(cddocumentoNossonumero != null){
					doc = contareceberService.loadForRetorno(new Documento(cddocumentoNossonumero));
				}
				
				if (doc != null) {
					bean.setDocumento(doc);
					bean.setValor(valor);
					bean.setNossonumero(detalheTRetornoBB240.getUsoEmpresa() != null ? detalheTRetornoBB240.getUsoEmpresa().trim() : null);
					bean.setValorTarifa(valorTarifa);
					bean.setMotivoTarifaDesc(descricaoMotivoTarifa(motivoTarifa));
					bean.setMotivoTarifa(motivoTarifa);
					bean.setOcorrencia(cdocorrencia);
					
					if(bean.getDocumento() != null && bean.getDocumento().getCddocumento() != null && bean.getDtcredito() != null && "06".equals(bean.getOcorrencia())){
						valorTotalPagamento = valorTotalPagamento.add(valor);
						valorTotalReceber = valorTotalReceber.add(doc.getAux_documento() != null && doc.getAux_documento().getValoratual() != null ? doc.getAux_documento().getValoratual() : new Money(0.0));
					}
					listaBean.add(bean);
				} else {
					documentoNaoEncontrado = true;
					if(cddocumento != null){
						listaDocumentoNaoEncontrado.add(cddocumento.toString());
					}else if(cddocumentoNossonumero != null){
						listaDocumentoNaoEncontrado.add(cddocumentoNossonumero.toString());
					}
				}
			}		
		}
		
		if (erroTransformacao) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui registros que est�o com dados incompletos ou inv�lidos. Nossos n�meros: ");
			for (String erro : listaErroTransformacao) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}
		if (documentoNaoEncontrado) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui documentos que n�o foram encontrados no contas a receber. Nossos n�meros: ");
			for (String erro : listaDocumentoNaoEncontrado) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}	
		
		beanAux.setValorTotalPagamento(valorTotalPagamento);
		beanAux.setValorTotalContaReceber(valorTotalReceber);
		return listaBean;
	}	
	
	private List<ArquivoRetornoBradescoBean> getListaContaReceberByRetorno400(ArquivoRetornoBean beanAux, List<EDIRegistroRetorno400> list) {
		List<ArquivoRetornoBradescoBean> listaBean = new ArrayList<ArquivoRetornoBradescoBean>();
		ArquivoRetornoBradescoBean bean;
		String dtCreditoStr;
		Date dtCredito;
		String dtOcorrenciaStr;
		Date dtOcorrencia;
		Integer cddocumento = null;
		Money valor = null;
		Documento doc = null;
		
		Money valorTotalPagamento = new Money(0.0);
		Money valorTotalReceber = new Money(0.0);
		
		WebRequestContext request = NeoWeb.getRequestContext();
		boolean erro = false;
		for (EDIRegistroRetorno400 detalheTRetorno400 : list) {
			erro = false;
			bean = new ArquivoRetornoBradescoBean();
			dtCreditoStr = detalheTRetorno400.getDtCredito();
			dtOcorrenciaStr = detalheTRetorno400.getDtOcorrencia();
			dtCredito = null;
			try {
				if(dtCreditoStr != null && !dtCreditoStr.trim().equals("")){
					dtCredito = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtCreditoStr).getTime());
				} 	
				if(dtOcorrenciaStr != null && !"".equals(dtOcorrenciaStr)){
					dtOcorrencia = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtOcorrenciaStr).getTime());
					bean.setDtocorrencia(dtOcorrencia);
				}
			} catch (Exception e) {
				erro = true;
				request.addError("Data de cr�dito inv�lida. Nosso n�mero: " + detalheTRetorno400.getUsoEmpresa().trim());
//				throw new SinedException("Data de cr�dito inv�lida. Nosso n�mero: " + detalheTRetorno400.getUsoEmpresa().trim());
			}			
			try {				
				cddocumento = Integer.parseInt(detalheTRetorno400.getUsoEmpresa().trim());
			} catch (Exception e) {
				try {
					cddocumento = Integer.parseInt(detalheTRetorno400.getNossoNumero().trim());
				} catch (Exception e2) {
					erro = true;
					request.addError("O nosso n�mero: " + detalheTRetorno400.getNossoNumero().trim() + " n�o foi identificado pelo sistema.");
				}
				//e.printStackTrace();
				//erro = true;
				//request.addError("O nosso n�mero: " + detalheTRetorno400.getUsoEmpresa().trim() + " n�o foi identificado pelo sistema.");
//				throw new SinedException("O nosso n�mero: " + detalheTRetorno400.getUsoEmpresa().trim() + " n�o foi identificado pelo sistema.");
			}
			try {
				valor = new Money(Integer.parseInt(detalheTRetorno400.getValortitulo()), true);
			} catch (Exception e) {
				erro = true;
				request.addError("Valor do t�tulo inv�lido. Nosso n�mero: " + detalheTRetorno400.getUsoEmpresa().trim());
//				throw new SinedException("Valor do t�tulo inv�lido. Nosso n�mero: " + detalheTRetorno400.getUsoEmpresa().trim());
			}	
			if(!erro){
				bean.setDtcredito(dtCredito);			
				try {
					doc = contareceberService.loadForRetorno(new Documento(cddocumento));
					bean.setDocumento(doc);
					bean.setValor(valor);
					
					bean.setNossonumero(detalheTRetorno400.getNossoNumero());
					bean.setCdOcorrencia(detalheTRetorno400.getCdOcorrencia());
					
					if(bean.getDocumento() != null && bean.getDocumento().getCddocumento() != null && bean.getDtcredito() != null){
						valorTotalPagamento = valorTotalPagamento.add(valor);
						valorTotalReceber = valorTotalReceber.add(doc.getAux_documento() != null && doc.getAux_documento().getValoratual() != null ? doc.getAux_documento().getValoratual() : new Money(0.0));
					}
					listaBean.add(bean);
				} catch (Exception e) {
					e.printStackTrace();
					//throw new SinedException("Documento " + cddocumento + " n�o encontrado.");				
				}
			}
		}
		
		beanAux.setValorTotalPagamento(valorTotalPagamento);
		beanAux.setValorTotalContaReceber(valorTotalReceber);
		return listaBean;
	}
	
	private List<ArquivoRetornoBrasilBean> getListaContaReceberByRetornoBB400(ArquivoRetornoBean beanAux, List<EDIRegistroRetornoBB400> list) {
		List<ArquivoRetornoBrasilBean> listaBean = new ArrayList<ArquivoRetornoBrasilBean>();
		ArquivoRetornoBrasilBean bean;
		String dtCreditoStr;
		Date dtCredito;
		String dtOcorrenciaStr;
		Date dtOcorrencia;
		BigInteger cddocumento;
		BigInteger cddocumentoNossonumero;
		String comando;
		Money valor;
		Documento doc;
		Boolean documentoNaoEncontrado = false;
		Boolean erroTransformacao = false;
		List<String> listaDocumentoNaoEncontrado = new ArrayList<String>(); 
		List<String> listaErroTransformacao = new ArrayList<String>();
		
		Money valorTotalPagamento = new Money(0.0);
		Money valorTotalReceber = new Money(0.0);
		for (EDIRegistroRetornoBB400 detalheTRetornoBB400 : list) {
			bean = new ArquivoRetornoBrasilBean();
			dtCreditoStr = detalheTRetornoBB400.getDtCredito();
			dtOcorrenciaStr = detalheTRetornoBB400.getDtOcorrencia();
			dtCredito = null;
			cddocumento = null;
			cddocumentoNossonumero = null;
			doc = null;
			try {
				if(dtCreditoStr != null && !dtCreditoStr.trim().equals("") && !dtCreditoStr.trim().equals("00/00/2000")){
					dtCredito = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtCreditoStr).getTime());
				} 
				if(dtOcorrenciaStr != null && !"".equals(dtOcorrenciaStr)){
					dtOcorrencia = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtOcorrenciaStr).getTime());
					bean.setDtocorrencia(dtOcorrencia);
				}
				if(detalheTRetornoBB400.getUsoEmpresa() != null && StringUtils.isNotBlank(detalheTRetornoBB400.getUsoEmpresa())){
					try{
						cddocumento  = new BigInteger(detalheTRetornoBB400.getUsoEmpresa().trim());		
					}catch (Exception e){
						erroTransformacao = true;
						listaErroTransformacao.add(detalheTRetornoBB400.getUsoEmpresa());	
						continue;
					}
				}else {
					try{
						cddocumentoNossonumero  = new BigInteger(detalheTRetornoBB400.getNossoNumero().trim());		
					}catch (Exception e){
						erroTransformacao = true;
						listaErroTransformacao.add(detalheTRetornoBB400.getNossoNumero());	
						continue;
					}
				}
				comando = detalheTRetornoBB400.getComando().trim();
				valor = new Money(Integer.parseInt(detalheTRetornoBB400.getValortitulo()), true);
//				valor = valor.subtract(new Money(Integer.parseInt(detalheTRetornoBB400.getValorDesconto()), true));
//				valor = valor.subtract(new Money(Integer.parseInt(detalheTRetornoBB400.getValorAbatimento()), true));
//				valor = valor.add(new Money(Integer.parseInt(detalheTRetornoBB400.getValorJuros()), true));
			} catch (Exception e) {
				erroTransformacao = true;
				listaErroTransformacao.add(detalheTRetornoBB400.getUsoEmpresa());
				continue;
			}
			bean.setDtcredito(dtCredito);		
			if (comando.equals("02") || 
					comando.equals("05") ||
					comando.equals("07") ||
					comando.equals("08") ||
					comando.equals("15") ||
					comando.equals("06")) {
				if((cddocumento != null && cddocumento.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) < 0) ||
						(cddocumentoNossonumero != null && cddocumentoNossonumero.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) < 0)){
					if(cddocumento != null){
						doc = contareceberService.loadForRetorno(new Documento(cddocumento.intValue()));
					}else if(cddocumentoNossonumero != null){
						doc = contareceberService.loadForRetorno(new Documento(cddocumentoNossonumero.intValue()));
					}
					if (doc != null) {
						bean.setDocumento(doc);
						bean.setValor(valor);					
						bean.setNossonumero(detalheTRetornoBB400.getNossoNumero());
						bean.setOcorrencia(comando);
						valorTotalPagamento = valorTotalPagamento.add(valor);
						valorTotalReceber = valorTotalReceber.add(doc.getAux_documento() != null && doc.getAux_documento().getValoratual() != null ? doc.getAux_documento().getValoratual() : new Money(0.0));
						listaBean.add(bean);
					} else {
						documentoNaoEncontrado = true;
						if(cddocumento != null){
							listaDocumentoNaoEncontrado.add(cddocumento.toString());
						}else if(cddocumentoNossonumero != null){
							listaDocumentoNaoEncontrado.add(cddocumentoNossonumero.toString());
						}
					}
				} else {
					documentoNaoEncontrado = true;
					if(cddocumento != null){
						listaDocumentoNaoEncontrado.add(cddocumento.toString());
					}else if(cddocumentoNossonumero != null){
						listaDocumentoNaoEncontrado.add(cddocumentoNossonumero.toString());
					}
				}
			}		
		}		
		
		if (erroTransformacao) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui registros que est�o com dados incompletos ou inv�lidos. Nossos n�meros: ");
			for (String erro : listaErroTransformacao) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}
		if (documentoNaoEncontrado) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui documentos que n�o foram encontrados no contas a receber. Nossos n�meros: ");
			for (String erro : listaDocumentoNaoEncontrado) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}	
		
		beanAux.setValorTotalPagamento(valorTotalPagamento);
		beanAux.setValorTotalContaReceber(valorTotalReceber);
		return listaBean;
	}	
	
	private List<ArquivoRetornoItauBean> getListaContaReceberByRetornoItau400(ArquivoRetornoBean beanAux, List<EDIRegistroRetornoItau400> list) {
		List<ArquivoRetornoItauBean> listaBean = new ArrayList<ArquivoRetornoItauBean>();
		ArquivoRetornoItauBean bean;
		String dtCreditoStr;
		Date dtCredito;
		String dtOcorrenciaStr;
		Date dtOcorrencia;
		Integer cddocumento;
		String nossoNumero;
		String cdOcorrencia;
		Money valor;
		Money valorDocBusca = null;
		Documento doc;
		Boolean documentoNaoEncontrado = false;
		Boolean erroTransformacao = false;
		List<String> listaDocumentoNaoEncontrado = new ArrayList<String>(); 
		List<String> listaErroTransformacao = new ArrayList<String>();
		
		Money valorTotalPagamento = new Money(0.0);
		Money valorTotalReceber = new Money(0.0);
		for (EDIRegistroRetornoItau400 detalheTRetornoItau400 : list) {
			bean = new ArquivoRetornoItauBean();
			dtCreditoStr = detalheTRetornoItau400.getDtCredito();
			dtOcorrenciaStr = detalheTRetornoItau400.getDtOcorrencia();
			dtCredito = null;
			try {
				if(dtCreditoStr != null && !dtCreditoStr.trim().equals("")){
					dtCredito = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtCreditoStr).getTime());
				} 
				if(dtOcorrenciaStr != null && !"".equals(dtOcorrenciaStr)){
					dtOcorrencia = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtOcorrenciaStr).getTime());
					bean.setDtocorrencia(dtOcorrencia);
				}
				try{
					if(beanAux.getConta() != null && beanAux.getConta().getConsiderarnumerodocumentoremessa() != null && 
							beanAux.getConta().getConsiderarnumerodocumentoremessa()){
						cddocumento  = Integer.parseInt(detalheTRetornoItau400.getUsoEmpresa().trim());
					}else {
						cddocumento  = Integer.parseInt(detalheTRetornoItau400.getNossoNumero().trim());
					}
				}catch (Exception e){
					cddocumento  = Integer.parseInt(detalheTRetornoItau400.getNossoNumero().trim());	
				}
				nossoNumero  = detalheTRetornoItau400.getNossoNumero().trim(); 
				cdOcorrencia = detalheTRetornoItau400.getCdOcorrencia().trim();
				valor = new Money(Integer.parseInt(detalheTRetornoItau400.getValortitulo()), true);
				valorDocBusca = valor;
				if (!cdOcorrencia.equals("02")){
					valor = valor.subtract(new Money(Integer.parseInt(detalheTRetornoItau400.getValorDesconto()), true));
					valor = valor.subtract(new Money(Integer.parseInt(detalheTRetornoItau400.getValorAbatimento()), true));
					valor = valor.add(new Money(Integer.parseInt(detalheTRetornoItau400.getValorJuros()), true));
				}
			} catch (Exception e) {
				e.printStackTrace();
				erroTransformacao = true;
				listaErroTransformacao.add(detalheTRetornoItau400.getNossoNumero());
				continue;
			}
			bean.setDtcredito(dtCredito);		
			if (cdOcorrencia.equals("02") || cdOcorrencia.equals("06") || cdOcorrencia.equals("08")) {
				doc = null;
				if(detalheTRetornoItau400.getNossoNumero() != null && !"".equals(detalheTRetornoItau400.getNossoNumero().trim())){
					doc = contareceberService.loadForRetorno(detalheTRetornoItau400.getNossoNumero().trim(), true, false, true, null);
					if(doc == null){
						doc = contareceberService.loadForRetorno(detalheTRetornoItau400.getNossoNumero().trim(), false, false, true, null);
					}
				}
				if(doc == null && detalheTRetornoItau400.getUsoEmpresa() != null && !"".equals(detalheTRetornoItau400.getUsoEmpresa().trim())){
					doc = contareceberService.loadForRetorno(detalheTRetornoItau400.getUsoEmpresa().trim(), false, false, false, null);
					if(doc == null){
						doc = contareceberService.loadForRetorno(detalheTRetornoItau400.getUsoEmpresa().trim(), false, true, false, valorDocBusca);
					}
				}
				if (doc != null) {
					bean.setDocumento(doc);
					bean.setValor(valor);					
					bean.setNossonumero(nossoNumero);
					bean.setOcorrencia(cdOcorrencia);
					
					if(bean.getDocumento() != null && bean.getDocumento().getCddocumento() != null && bean.getDtcredito() != null && "06".equals(bean.getOcorrencia())){
						valorTotalPagamento = valorTotalPagamento.add(valor);
						valorTotalReceber = valorTotalReceber.add(doc.getAux_documento() != null && doc.getAux_documento().getValoratual() != null ? doc.getAux_documento().getValoratual() : new Money(0.0));
					}
					listaBean.add(bean);
				} else {
					documentoNaoEncontrado = true;
					listaDocumentoNaoEncontrado.add(cddocumento.toString());
				}
			}		
		}		
		
		if (erroTransformacao) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui registros que est�o com dados incompletos ou inv�lidos. Nossos n�meros: ");
			for (String erro : listaErroTransformacao) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}
		if (documentoNaoEncontrado) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui documentos que n�o foram encontrados no contas a receber. Nossos n�meros: ");
			for (String erro : listaDocumentoNaoEncontrado) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}	
		beanAux.setValorTotalPagamento(valorTotalPagamento);
		beanAux.setValorTotalContaReceber(valorTotalReceber);
		return listaBean;
	}	
	
	private List<ArquivoRetornoSantanderBean> getListaContaReceberByRetornoSantander400(ArquivoRetornoBean beanAux, List<EDIRegistroRetornoSantander400> list) {
		List<ArquivoRetornoSantanderBean> listaBean = new ArrayList<ArquivoRetornoSantanderBean>();
		ArquivoRetornoSantanderBean bean;
		String dtCreditoStr;
		Date dtCredito;
		String dtOcorrenciaStr;
		Date dtOcorrencia;
		Integer cddocumento;
		String nossoNumero;
		String cdOcorrencia;
		Money valor;
		Documento doc;
		Boolean documentoNaoEncontrado = false;
		Boolean erroTransformacao = false;
		List<String> listaDocumentoNaoEncontrado = new ArrayList<String>(); 
		List<String> listaErroTransformacao = new ArrayList<String>();
		
		Money valorTotalPagamento = new Money(0.0);
		Money valorTotalReceber = new Money(0.0);
		for (EDIRegistroRetornoSantander400 detalheTRetornoSantander400 : list) {
			bean = new ArquivoRetornoSantanderBean();
			dtCreditoStr = detalheTRetornoSantander400.getDtcredito();
			dtOcorrenciaStr = detalheTRetornoSantander400.getDtOcorrencia();
			dtCredito = null;
			try {
				if(dtCreditoStr != null && !dtCreditoStr.trim().equals("") && !dtCreditoStr.trim().equals("00002000")){
					dtCredito = new Date(new SimpleDateFormat("ddMMyyyy").parse(dtCreditoStr).getTime());
				} 
				if(dtOcorrenciaStr != null && !"".equals(dtOcorrenciaStr)){
					dtOcorrencia = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtOcorrenciaStr).getTime());
					bean.setDtocorrencia(dtOcorrencia);
				}
				cddocumento  = Integer.parseInt(detalheTRetornoSantander400.getUsoEmpresa().trim());
				nossoNumero  = detalheTRetornoSantander400.getUsoEmpresa().trim(); 
				cdOcorrencia = detalheTRetornoSantander400.getCdocorrencia().trim();
				valor = new Money(Integer.parseInt(detalheTRetornoSantander400.getValorTitulo()), true);
			} catch (Exception e) {
				e.printStackTrace();
				erroTransformacao = true;
				listaErroTransformacao.add(detalheTRetornoSantander400.getUsoEmpresa());
				continue;
			}
			bean.setDtcredito(dtCredito);		
			if (cdOcorrencia.equals("02") || cdOcorrencia.equals("06")) {
				doc = contareceberService.loadForRetorno(new Documento(cddocumento));
				if (doc != null) {
					bean.setDocumento(doc);
					bean.setValor(valor);					
					bean.setNossonumero(nossoNumero);
					bean.setOcorrencia(cdOcorrencia);
					
					if(bean.getDocumento() != null && bean.getDocumento().getCddocumento() != null && bean.getDtcredito() != null && "06".equals(bean.getOcorrencia())){
						valorTotalPagamento = valorTotalPagamento.add(valor);
						valorTotalReceber = valorTotalReceber.add(doc.getAux_documento() != null && doc.getAux_documento().getValoratual() != null ? doc.getAux_documento().getValoratual() : new Money(0.0));
					}
					listaBean.add(bean);
				} else {
					documentoNaoEncontrado = true;
					listaDocumentoNaoEncontrado.add(cddocumento.toString());
				}
			}		
		}		
		
		if (erroTransformacao) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui registros que est�o com dados incompletos ou inv�lidos. Nossos n�meros: ");
			for (String erro : listaErroTransformacao) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}
		if (documentoNaoEncontrado) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui documentos que n�o foram encontrados no contas a receber. Nossos n�meros: ");
			for (String erro : listaDocumentoNaoEncontrado) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}	
		beanAux.setValorTotalPagamento(valorTotalPagamento);
		beanAux.setValorTotalContaReceber(valorTotalReceber);
		return listaBean;
	}	
	
	public ModelAndView confirmarArquivoBradesco(WebRequestContext request, ArquivoRetornoBean bean){
		
		List<ArquivoRetornoBradescoBean> listaContaBradesco = bean.getListaContaBradesco();
		String idsCP = "";
		for (ArquivoRetornoBradescoBean b : listaContaBradesco) {
			if(b.getDtcredito() == null){
				if(b.getDocumento() != null && b.getDocumento().getCddocumento() != null){
					idsCP += b.getDocumento().getCddocumento() + ",";
				}
			}
		}
		if(!idsCP.equals("")){
			// Salva o arquivo no disco.
			Arquivo arquivo = (Arquivo) request.getSession().getAttribute(ARQUIVORETORNO_SESSION);
			arquivoService.saveOrUpdate(arquivo);
	
			// Cria e salva arquivo banc�rio
			Arquivobancario ab = new Arquivobancario();
			ab.setNome(arquivo.getNome());
			ab.setArquivo(arquivo);
			ab.setArquivobancariotipo(Arquivobancariotipo.RETORNO);
			ab.setArquivobancariosituacao(Arquivobancariosituacao.PROCESSADO);
			ab.setDtgeracao(SinedDateUtils.currentDate());
			arquivobancarioService.saveOrUpdate(ab);
			arquivoDAO.saveFile(ab, "arquivo");
			
			bean.setArquivobancario(ab);
			
			idsCP = idsCP.substring(0, idsCP.length() - 1);
			
			List<Arquivobancario> listaArquivo = arquivobancarioService.findArquivoDocumentos(idsCP);
			
			for (Arquivobancario arquivobancario : listaArquivo) {
				arquivobancarioService.updateArquivoCorrespondenteSituacao(arquivobancario, ab, Arquivobancariosituacao.PROCESSADO);
			}
			
			request.addMessage("O arquivo foi confirmado com sucesso.");
		}
	
		return continueOnAction("processarInfoBradesco", bean);
	}
	
	public ModelAndView confirmarArquivoItau(WebRequestContext request, ArquivoRetornoBean bean){
		
		List<ArquivoRetornoItauBean> listaContaItau = bean.getListaContaItau();
		String idsCP = "";
		if(listaContaItau != null && listaContaItau.size() > 0){
			for (ArquivoRetornoItauBean b : listaContaItau) {
				if(b.getDtcredito() == null){
					if(b.getDocumento() != null && b.getDocumento().getCddocumento() != null){
						idsCP += b.getDocumento().getCddocumento() + ",";
					}
				}
			}
		}
		if(!idsCP.equals("")){
			// Salva o arquivo no disco.
			Arquivo arquivo = (Arquivo) request.getSession().getAttribute(ARQUIVORETORNO_SESSION);
			arquivoService.saveOrUpdate(arquivo);
	
			// Cria e salva arquivo banc�rio
			Arquivobancario ab = new Arquivobancario();
			ab.setNome(arquivo.getNome());
			ab.setArquivo(arquivo);
			ab.setArquivobancariotipo(Arquivobancariotipo.RETORNO);
			ab.setArquivobancariosituacao(Arquivobancariosituacao.PROCESSADO);
			ab.setDtgeracao(SinedDateUtils.currentDate());
			arquivobancarioService.saveOrUpdate(ab);
			arquivoDAO.saveFile(ab, "arquivo");
			
			bean.setArquivobancario(ab);
			
			idsCP = idsCP.substring(0, idsCP.length() - 1);
			
			List<Arquivobancario> listaArquivo = arquivobancarioService.findArquivoDocumentos(idsCP);
			
			for (Arquivobancario arquivobancario : listaArquivo) {
				arquivobancarioService.updateArquivoCorrespondenteSituacao(arquivobancario, ab, Arquivobancariosituacao.PROCESSADO);
			}
			
			request.addMessage("O arquivo foi confirmado com sucesso.");
		}
		
		return continueOnAction("processarInfoItau", bean);
	}	
	
	public ModelAndView confirmarArquivoSantander(WebRequestContext request, ArquivoRetornoBean bean){
		
		List<ArquivoRetornoSantanderBean> listaContaSantander = bean.getListaContaSantander();
		String idsCP = "";
		for (ArquivoRetornoSantanderBean b : listaContaSantander) {
			if(b.getDtcredito() == null){
				if(b.getDocumento() != null && b.getDocumento().getCddocumento() != null){
					idsCP += b.getDocumento().getCddocumento() + ",";
				}
			}
		}
		if(!idsCP.equals("")){
			// Salva o arquivo no disco.
			Arquivo arquivo = (Arquivo) request.getSession().getAttribute(ARQUIVORETORNO_SESSION);
			arquivoService.saveOrUpdate(arquivo);
			
			// Cria e salva arquivo banc�rio
			Arquivobancario ab = new Arquivobancario();
			ab.setNome(arquivo.getNome());
			ab.setArquivo(arquivo);
			ab.setArquivobancariotipo(Arquivobancariotipo.RETORNO);
			ab.setArquivobancariosituacao(Arquivobancariosituacao.PROCESSADO);
			ab.setDtgeracao(SinedDateUtils.currentDate());
			arquivobancarioService.saveOrUpdate(ab);
			arquivoDAO.saveFile(ab, "arquivo");
			
			bean.setArquivobancario(ab);
			
			idsCP = idsCP.substring(0, idsCP.length() - 1);
			
			List<Arquivobancario> listaArquivo = arquivobancarioService.findArquivoDocumentos(idsCP);
			
			for (Arquivobancario arquivobancario : listaArquivo) {
				arquivobancarioService.updateArquivoCorrespondenteSituacao(arquivobancario, ab, Arquivobancariosituacao.PROCESSADO);
			}
			
			request.addMessage("O arquivo foi confirmado com sucesso.");
		}
		
		return continueOnAction("processarInfoSantander", bean);
	}	
	
	public ModelAndView confirmarArquivoRural(WebRequestContext request, ArquivoRetornoBean bean){
		
		List<ArquivoRetornoRuralBean> listaContaRural = bean.getListaContaRural();
		String idsCP = "";
		for (ArquivoRetornoRuralBean b : listaContaRural) {
			if(b.getDtcredito() == null){
				if(b.getDocumento() != null && b.getDocumento().getCddocumento() != null){
					idsCP += b.getDocumento().getCddocumento() + ",";
				}
			}
		}
		if(!idsCP.equals("")){
			// Salva o arquivo no disco.
			Arquivo arquivo = (Arquivo) request.getSession().getAttribute(ARQUIVORETORNO_SESSION);
			arquivoService.saveOrUpdate(arquivo);
			
			// Cria e salva arquivo banc�rio
			Arquivobancario ab = new Arquivobancario();
			ab.setNome(arquivo.getNome());
			ab.setArquivo(arquivo);
			ab.setArquivobancariotipo(Arquivobancariotipo.RETORNO);
			ab.setArquivobancariosituacao(Arquivobancariosituacao.PROCESSADO);
			ab.setDtgeracao(SinedDateUtils.currentDate());
			arquivobancarioService.saveOrUpdate(ab);
			arquivoDAO.saveFile(ab, "arquivo");
			
			bean.setArquivobancario(ab);
			
			idsCP = idsCP.substring(0, idsCP.length() - 1);
			
			List<Arquivobancario> listaArquivo = arquivobancarioService.findArquivoDocumentos(idsCP);
			
			for (Arquivobancario arquivobancario : listaArquivo) {
				arquivobancarioService.updateArquivoCorrespondenteSituacao(arquivobancario, ab, Arquivobancariosituacao.PROCESSADO);
			}
			
			request.addMessage("O arquivo foi confirmado com sucesso.");
		}
		
		return continueOnAction("processarInfoRural", bean);
	}
	
	public ModelAndView processarInfoCaixa(WebRequestContext request, ArquivoRetornoBean bean){
		try{
			List<ArquivoRetornoCAIXABean> listaDocumentoNaoMarcadaByTarifa = new ArrayList<ArquivoRetornoCAIXABean>();
			ArquivoRetornoCAIXABean beanNaoMarcadoByTarifa;
			boolean existBeanByTarifa;
			
			AtomicInteger qtdeMovimentacao = new AtomicInteger(0);
			AtomicLong valor_COB_INTERN = new AtomicLong(0);
			AtomicLong valor_COB_LOTERI = new AtomicLong(0);
			AtomicLong valor_COB_AGENC = new AtomicLong(0);
			AtomicLong valor_COB_COMPE = new AtomicLong(0);
			Date dt_COB_INTERN = null;
			Date dt_COB_LOTERI = null;
			Date dt_COB_AGENC = null;
			Date dt_COB_COMPE = null;
			
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			BaixarContaBean baixarContaBean;
			List<Documento> listaDocumento;
			List<Documento> listaDocumentoValecompra = new ArrayList<Documento>();
			Documento doc;
			Movimentacao movimentacao;
			List<Movimentacao> listaMov = new ArrayList<Movimentacao>();
			Rateio rateio;
			
			for (ArquivoRetornoCAIXABean bean2 : bean.getListaConta()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					
					baixarContaBean = new BaixarContaBean();
					listaDocumento = new ArrayList<Documento>();
					
					doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
					
					if(doc.getValor().getValue().doubleValue() == 0d){
						rateio = contareceberService.atualizaValorRateio(doc, bean2.getValor());
						doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
					} else {
						rateio = rateioService.findByDocumento(doc);
						rateioService.atualizaValorRateio(rateio, bean2.getValor());
					}
					
					doc.setValoratual(bean2.getValor());
					doc.setValoratualbaixa(bean2.getValor());
					doc.setRateio(rateio);
					
					listaDocumento.add(doc);
					listaDocumentoValecompra.add(doc);
					
					baixarContaBean.setListaDocumento(listaDocumento);
					baixarContaBean.setRateio(doc.getRateio());
					baixarContaBean.setDtpagamento(bean2.getDtocorrencia() != null ? bean2.getDtocorrencia() : bean2.getDtcredito());
					baixarContaBean.setDtcredito(bean2.getDtcredito());
					baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
					baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
					baixarContaBean.setVinculo(bean.getConta());
					baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
					baixarContaBean.setChecknum(sdf.format(bean2.getDtcredito()));
					baixarContaBean.setFromRetorno(true);
					movimentacao = documentoService.doBaixarConta(request, baixarContaBean);
					if(movimentacao != null){
						notaDocumentoService.liberaNotaDocumentoWebservice(request, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//						contareceberService.incrementaNumDocumento(doc);
						documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
						movimentacao.setValorTarifa(bean2.getValorTarifa());
						movimentacao.setMotivoTarifa(bean2.getMotivoTarifa());
						movimentacao.setDtmovimentacao(bean2.getDtcredito());
						movimentacao.setDtdebitotarifa(bean2.getDtdebitotarifa());
						listaMov.add(movimentacao);
					}
					if(movimentacao != null && movimentacao.getMotivoTarifa() != null){
						if(movimentacao.getMotivoTarifa().equals("02")){
							valor_COB_LOTERI.addAndGet(movimentacao.getValorTarifa().toLong());
							
							if(movimentacao.getDtdebitotarifa() != null){
								dt_COB_LOTERI = movimentacao.getDtdebitotarifa();
							} else {
								dt_COB_LOTERI = movimentacao.getDtmovimentacao();
							}
						}else if(movimentacao.getMotivoTarifa().equals("03")){
							valor_COB_AGENC.addAndGet(movimentacao.getValorTarifa().toLong());
							
							if(movimentacao.getDtdebitotarifa() != null){
								dt_COB_AGENC = movimentacao.getDtdebitotarifa();
							} else {
								dt_COB_AGENC = movimentacao.getDtmovimentacao();
							}
						}else if(movimentacao.getMotivoTarifa().equals("04")){
							valor_COB_COMPE.addAndGet(movimentacao.getValorTarifa().toLong());
							
							if(movimentacao.getDtdebitotarifa() != null){
								dt_COB_COMPE = movimentacao.getDtdebitotarifa();
							} else {
								dt_COB_COMPE = movimentacao.getDtmovimentacao();
							}
						}else if(movimentacao.getMotivoTarifa().equals("06")){
							valor_COB_INTERN.addAndGet(movimentacao.getValorTarifa().toLong());
							
							if(movimentacao.getDtdebitotarifa() != null){
								dt_COB_INTERN = movimentacao.getDtdebitotarifa();
							} else {
								dt_COB_INTERN = movimentacao.getDtmovimentacao();
							}
						}
						qtdeMovimentacao.addAndGet(1);
					}
					
					arquivobancariodocumentoService.saveProcessamentoArquivoRetorno(getArquivobancario(bean), bean2.getDocumento());
				}else if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && (bean2.getMarcado() == null || !bean2.getMarcado())){
					//FEITO ISTO PARA AGRUPAR O VALOR DAS TARIFAS NAO SELECIONADAS
					if(bean2.getValorTarifa() != null && bean2.getValorTarifa().getValue().doubleValue() > 0 && 
							bean2.getValorTarifa() != null && bean2.getValorTarifa().getValue().doubleValue() > 0 && 
							bean2.getMotivoTarifa() != null && !"".equals(bean2.getMotivoTarifa())){
						
						beanNaoMarcadoByTarifa = new ArquivoRetornoCAIXABean();
						beanNaoMarcadoByTarifa.setMotivoTarifa(bean2.getMotivoTarifa());
						beanNaoMarcadoByTarifa.setValorTarifa(bean2.getValorTarifa());
						beanNaoMarcadoByTarifa.setDtcredito(bean2.getDtcredito());
						beanNaoMarcadoByTarifa.setDtocorrencia(bean2.getDtocorrencia());
						existBeanByTarifa = false;
						
						for(ArquivoRetornoCAIXABean beanNaoMarcado : listaDocumentoNaoMarcadaByTarifa){
							if(beanNaoMarcado.getMotivoTarifa().equals(bean2.getMotivoTarifa())){
								if(bean2.getDtocorrencia() != null){
									if(beanNaoMarcado.getDtocorrencia() != null){
										if(SinedDateUtils.equalsIgnoreHour(bean2.getDtocorrencia(), beanNaoMarcado.getDtocorrencia())){
											beanNaoMarcado.setValorTarifa(beanNaoMarcado.getValorTarifa().add(beanNaoMarcadoByTarifa.getValorTarifa()));
										}
									}else if(beanNaoMarcado.getDtcredito() != null){
										if(SinedDateUtils.equalsIgnoreHour(bean2.getDtocorrencia(), beanNaoMarcado.getDtcredito())){
											beanNaoMarcado.setValorTarifa(beanNaoMarcado.getValorTarifa().add(beanNaoMarcadoByTarifa.getValorTarifa()));
										}
									}
									
								}else if(bean2.getDtcredito() != null){
									if(beanNaoMarcado.getDtocorrencia() != null){
										if(SinedDateUtils.equalsIgnoreHour(bean2.getDtcredito(), beanNaoMarcado.getDtocorrencia())){
											beanNaoMarcado.setValorTarifa(beanNaoMarcado.getValorTarifa().add(beanNaoMarcadoByTarifa.getValorTarifa()));
										}
									}else if(beanNaoMarcado.getDtcredito() != null){
										if(SinedDateUtils.equalsIgnoreHour(bean2.getDtcredito(), beanNaoMarcado.getDtcredito())){
											beanNaoMarcado.setValorTarifa(beanNaoMarcado.getValorTarifa().add(beanNaoMarcadoByTarifa.getValorTarifa()));
										}
									}
								}
								existBeanByTarifa= true;
							}
						}
						if(!existBeanByTarifa){
							beanNaoMarcadoByTarifa.setMotivoTarifaDesc(descricaoMotivoTarifa(bean2.getMotivoTarifa()));
							listaDocumentoNaoMarcadaByTarifa.add(beanNaoMarcadoByTarifa);
						}
					}					
				}
			}
			
			for (ArquivoRetornoCAIXABean bean2 : bean.getListaConta()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(bean2.getDocumento());
				}
			}
			
			for (Movimentacao movimentacao2 : listaMov) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}

			boolean break_COB_LOTERI = false;
			boolean break_COB_AGENC = false;
			boolean break_COB_COMPE = false;
			boolean break_COB_INTERN = false;
			for(int i=0; i < qtdeMovimentacao.get(); i++){
				
				if(valor_COB_LOTERI.get() > 0 && !break_COB_LOTERI){
					listaMov.get(i).setValorTarifa(new Money(valor_COB_LOTERI.get(), true));
					listaMov.get(i).setMotivoTarifa("02");
					listaMov.get(i).setDtdebitotarifa(dt_COB_LOTERI);
					
					gerarMovimentacaoTarifa(listaMov.get(i), listaDocumentoNaoMarcadaByTarifa);
					break_COB_LOTERI = true;
				}
				if(valor_COB_AGENC.get() > 0 && !break_COB_AGENC){
					listaMov.get(i).setValorTarifa(new Money(valor_COB_AGENC.get(), true));
					listaMov.get(i).setMotivoTarifa("03");
					listaMov.get(i).setDtdebitotarifa(dt_COB_AGENC);
					
					gerarMovimentacaoTarifa(listaMov.get(i), listaDocumentoNaoMarcadaByTarifa);
					break_COB_AGENC = true;
				}
				if(valor_COB_COMPE.get() > 0 && !break_COB_COMPE){
					listaMov.get(i).setValorTarifa(new Money(valor_COB_COMPE.get(), true));
					listaMov.get(i).setMotivoTarifa("04");
					listaMov.get(i).setDtdebitotarifa(dt_COB_COMPE);
					
					gerarMovimentacaoTarifa(listaMov.get(i), listaDocumentoNaoMarcadaByTarifa);
					break_COB_COMPE = true;
				}
				if(valor_COB_INTERN.get() > 0 && !break_COB_INTERN){
					listaMov.get(i).setValorTarifa(new Money(valor_COB_INTERN.get(), true));
					listaMov.get(i).setMotivoTarifa("06");
					listaMov.get(i).setDtdebitotarifa(dt_COB_INTERN);
					
					gerarMovimentacaoTarifa(listaMov.get(i), listaDocumentoNaoMarcadaByTarifa);
					break_COB_INTERN = true;
				}
			}
			
			request.addMessage("Informa��es processadas.");
			
			List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
			for (ArquivoRetornoCAIXABean bean2 : bean.getListaConta()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					listaDocumentoForComissionamento.add(bean2.getDocumento());
				}
			}
			
//			valecompraService.createValecompraArquivoRetorno(request, listaDocumentoValecompra, listaMov);
			
			try {
				if(listaDocumentoForComissionamento != null && !listaDocumentoForComissionamento.isEmpty())
					contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		movimentacaoService.callProcedureAtualizaMovimentacaoNecessidade();
		
		bean.setArquivo(null);
		return continueOnAction("carregar", bean);
	}
	
	public ModelAndView processarInfoBradesco(WebRequestContext request, ArquivoRetornoBean bean){
		
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			BaixarContaBean baixarContaBean;
			List<Documento> listaDocumento;
			List<Documento> listaDocumentoValecompra = new ArrayList<Documento>();
			Documento doc;
			Movimentacao movimentacao;
			List<Movimentacao> listaMov = new ArrayList<Movimentacao>();
			
			for (ArquivoRetornoBradescoBean bean2 : bean.getListaContaBradesco()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					
					baixarContaBean = new BaixarContaBean();
					listaDocumento = new ArrayList<Documento>();
					
					doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
					doc.setValoratual(bean2.getValor());
					doc.setValoratualbaixa(bean2.getValor());
					doc.setRateio(rateioService.findByDocumento(doc));
					listaDocumento.add(doc);
					listaDocumentoValecompra.add(doc);
					
					baixarContaBean.setListaDocumento(listaDocumento);
					baixarContaBean.setRateio(doc.getRateio());
					baixarContaBean.setDtpagamento(bean2.getDtocorrencia() != null ? bean2.getDtocorrencia() : bean2.getDtcredito());
					baixarContaBean.setDtcredito(bean2.getDtcredito());
					baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
					baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
					baixarContaBean.setVinculo(bean.getConta());
					baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
					baixarContaBean.setChecknum(sdf.format(bean2.getDtcredito()));
					baixarContaBean.setFromRetorno(true);
					
					movimentacao = documentoService.doBaixarConta(request, baixarContaBean);
					if(movimentacao != null){
						notaDocumentoService.liberaNotaDocumentoWebservice(request, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//						contareceberService.incrementaNumDocumento(doc);
						documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
					}
					listaMov.add(movimentacao);
					
					arquivobancariodocumentoService.saveProcessamentoArquivoRetorno(getArquivobancario(bean), bean2.getDocumento());
				}else if (bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getCdOcorrencia() != null && bean2.getCdOcorrencia().equals("02")) {
					if (bean2.getNossonumero() != null && !bean2.getNossonumero().equals("")) {
						doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
						if (doc.getNossonumero() == null || !doc.getNossonumero().equals(bean2.getNossonumero())) {
							doc.setNossonumero(bean2.getNossonumero());
							documentoService.updateNossoNumero(doc);
						}
					}
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null){
						Documentoconfirmacao documentoconfirmacao = new Documentoconfirmacao();
						documentoconfirmacao.setData(SinedDateUtils.currentDate());
						documentoconfirmacao.setDocumento(bean2.getDocumento());
						documentoconfirmacaoService.saveOrUpdate(documentoconfirmacao);
					}
				}
			}
			
			for (ArquivoRetornoBradescoBean bean2 : bean.getListaContaBradesco()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(bean2.getDocumento());
				}
			}
			
			for (Movimentacao movimentacao2 : listaMov) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}
			
			request.addMessage("Informa��es processadas.");
			
//			valecompraService.createValecompraArquivoRetorno(request, listaDocumentoValecompra, listaMov);
			
			try {
				List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
				for (ArquivoRetornoBradescoBean bean2 : bean.getListaContaBradesco()) {
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
						listaDocumentoForComissionamento.add(bean2.getDocumento());
					}
				}
				contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		}
		
		movimentacaoService.callProcedureAtualizaMovimentacaoNecessidade();
		
		bean.setArquivo(null);
		return continueOnAction("carregar", bean);
	}
	
	public ModelAndView processarInfoItau(WebRequestContext request, ArquivoRetornoBean bean){
		
		try{
			StringBuilder docNaoProcessado = new StringBuilder();
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			BaixarContaBean baixarContaBean;
			List<Documento> listaDocumento;
			List<Documento> listaDocumentoValecompra = new ArrayList<Documento>();
			Documento doc;
			Movimentacao movimentacao;
			List<Movimentacao> listaMov = new ArrayList<Movimentacao>();
			
			for (ArquivoRetornoItauBean bean2 : bean.getListaContaItau()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());							
					if (Documentoacao.PREVISTA.equals(doc.getDocumentoacao()) || Documentoacao.DEFINITIVA.equals(doc.getDocumentoacao())){
						baixarContaBean = new BaixarContaBean();
						listaDocumento = new ArrayList<Documento>();
						
						doc.setValoratual(bean2.getValor());
						doc.setValoratualbaixa(bean2.getValor());
						doc.setRateio(rateioService.findByDocumento(doc));					
						listaDocumento.add(doc);
						listaDocumentoValecompra.add(doc);
						
						baixarContaBean.setListaDocumento(listaDocumento);
						baixarContaBean.setRateio(doc.getRateio());
						baixarContaBean.setDtpagamento(bean2.getDtocorrencia() != null ? bean2.getDtocorrencia() : bean2.getDtcredito());
						baixarContaBean.setDtcredito(bean2.getDtcredito());
						baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
						baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
						baixarContaBean.setVinculo(bean.getConta());
						baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
						baixarContaBean.setChecknum(sdf.format(bean2.getDtcredito()));
						baixarContaBean.setFromRetorno(true);
						
						movimentacao = documentoService.doBaixarConta(request, baixarContaBean);
						if(movimentacao != null){
							notaDocumentoService.liberaNotaDocumentoWebservice(request, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//							contareceberService.incrementaNumDocumento(doc);
							documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
							listaMov.add(movimentacao);
						}
						
						arquivobancariodocumentoService.saveProcessamentoArquivoRetorno(getArquivobancario(bean), bean2.getDocumento());
					} else {
						if (docNaoProcessado.length() == 0)
							docNaoProcessado.append(bean2.getNossonumero());
						else
							docNaoProcessado.append(", " + bean2.getNossonumero());
					}							

				} else if (bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getOcorrencia() != null && bean2.getOcorrencia().equals("02")) {
					if (bean2.getNossonumero() != null && !bean2.getNossonumero().equals("")) {
						doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
						if (doc.getNossonumero() == null || !doc.getNossonumero().equals(bean2.getNossonumero())) {
							doc.setNossonumero(bean2.getNossonumero());
							documentoService.updateNossoNumero(doc);
						}
					}
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null){
						Documentoconfirmacao documentoconfirmacao = new Documentoconfirmacao();
						documentoconfirmacao.setData(SinedDateUtils.currentDate());
						documentoconfirmacao.setDocumento(bean2.getDocumento());
						documentoconfirmacaoService.saveOrUpdate(documentoconfirmacao);
					}
				}
			}
					
			for (ArquivoRetornoItauBean bean2 : bean.getListaContaItau()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(bean2.getDocumento());
				}
			}
			
			for (Movimentacao movimentacao2 : listaMov) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}
			
			if (docNaoProcessado.length() > 0)
				request.addMessage("Aten��o! Alguns documentos que est�o com situa��o diferente de prevista e definitiva n�o foram processados. Nossos n�meros: " + docNaoProcessado.toString(), MessageType.ERROR);
			else
				request.addMessage("Informa��es processadas.");
			
//			valecompraService.createValecompraArquivoRetorno(request, listaDocumentoValecompra, listaMov);
			
			try {
				List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
				for (ArquivoRetornoItauBean bean2 : bean.getListaContaItau()) {
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
						listaDocumentoForComissionamento.add(bean2.getDocumento());
					}
				}
				contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		}
		
		movimentacaoService.callProcedureAtualizaMovimentacaoNecessidade();
		
		bean.setArquivo(null);
		return continueOnAction("carregar", bean);
	}
	
	public ModelAndView processarInfoSantander(WebRequestContext request, ArquivoRetornoBean bean){
		
		try{
			StringBuilder docNaoProcessado = new StringBuilder();
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			BaixarContaBean baixarContaBean;
			List<Documento> listaDocumento;
			List<Documento> listaDocumentoValecompra = new ArrayList<Documento>();
			Documento doc;
			Movimentacao movimentacao;
			List<Movimentacao> listaMov = new ArrayList<Movimentacao>();
			
			for (ArquivoRetornoSantanderBean bean2 : bean.getListaContaSantander()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					
					doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());							
					if (Documentoacao.PREVISTA.equals(doc.getDocumentoacao()) || Documentoacao.DEFINITIVA.equals(doc.getDocumentoacao())){
						baixarContaBean = new BaixarContaBean();
						listaDocumento = new ArrayList<Documento>();
						
						doc.setValoratual(bean2.getValor());
						doc.setValoratualbaixa(bean2.getValor());
						doc.setRateio(rateioService.findByDocumento(doc));					
						listaDocumento.add(doc);
						listaDocumentoValecompra.add(doc);
						
						baixarContaBean.setListaDocumento(listaDocumento);
						baixarContaBean.setRateio(doc.getRateio());
						baixarContaBean.setDtpagamento(bean2.getDtocorrencia() != null ? bean2.getDtocorrencia() : bean2.getDtcredito());
						baixarContaBean.setDtcredito(bean2.getDtcredito());
						baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
						baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
						baixarContaBean.setVinculo(bean.getConta());
						baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
						baixarContaBean.setChecknum(sdf.format(bean2.getDtcredito()));
						baixarContaBean.setFromRetorno(true);
						
						movimentacao = documentoService.doBaixarConta(request, baixarContaBean);
						if(movimentacao != null){
							notaDocumentoService.liberaNotaDocumentoWebservice(request, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//							contareceberService.incrementaNumDocumento(doc);
							documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
							listaMov.add(movimentacao);
						}
						
						arquivobancariodocumentoService.saveProcessamentoArquivoRetorno(getArquivobancario(bean), bean2.getDocumento());
					} else {
						if (docNaoProcessado.length() == 0)
							docNaoProcessado.append(bean2.getNossonumero());
						else
							docNaoProcessado.append(", " + bean2.getNossonumero());
					}							
					
				} else if (bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getOcorrencia() != null && bean2.getOcorrencia().equals("02")) {
					if (bean2.getNossonumero() != null && !bean2.getNossonumero().equals("")) {
						doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
						if (doc.getNossonumero() == null || !doc.getNossonumero().equals(bean2.getNossonumero())) {
							doc.setNossonumero(bean2.getNossonumero());
							documentoService.updateNossoNumero(doc);
						}
					}
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null){
						Documentoconfirmacao documentoconfirmacao = new Documentoconfirmacao();
						documentoconfirmacao.setData(SinedDateUtils.currentDate());
						documentoconfirmacao.setDocumento(bean2.getDocumento());
						documentoconfirmacaoService.saveOrUpdate(documentoconfirmacao);
					}
				}
			}
					
			for (ArquivoRetornoSantanderBean bean2 : bean.getListaContaSantander()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(bean2.getDocumento());
				}
			}
			
			for (Movimentacao movimentacao2 : listaMov) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}
			
			if (docNaoProcessado.length() > 0)
				request.addMessage("Aten��o! Alguns documentos que est�o com situa��o diferente de prevista e definitiva n�o foram processados. Nossos n�meros: " + docNaoProcessado.toString(), MessageType.ERROR);
			else
				request.addMessage("Informa��es processadas.");
			
//			valecompraService.createValecompraArquivoRetorno(request, listaDocumentoValecompra, listaMov);
			
			try {
				List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
				for (ArquivoRetornoSantanderBean bean2 : bean.getListaContaSantander()) {
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
						listaDocumentoForComissionamento.add(bean2.getDocumento());
					}
				}
				contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		}
		
		movimentacaoService.callProcedureAtualizaMovimentacaoNecessidade();
		
		bean.setArquivo(null);
		return continueOnAction("carregar", bean);
	}
	
	public ModelAndView processarInfoRural(WebRequestContext request, ArquivoRetornoBean bean){
		
		try{
			StringBuilder docNaoProcessado = new StringBuilder();
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			BaixarContaBean baixarContaBean;
			List<Documento> listaDocumento;
			List<Documento> listaDocumentoValecompra = new ArrayList<Documento>();
			Documento doc;
			Movimentacao movimentacao;
			List<Movimentacao> listaMov = new ArrayList<Movimentacao>();
			
			for (ArquivoRetornoRuralBean bean2 : bean.getListaContaRural()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					
					doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());							
					if (Documentoacao.PREVISTA.equals(doc.getDocumentoacao()) || Documentoacao.DEFINITIVA.equals(doc.getDocumentoacao())){
						baixarContaBean = new BaixarContaBean();
						listaDocumento = new ArrayList<Documento>();
						
						doc.setValoratual(bean2.getValor());
						doc.setValoratualbaixa(bean2.getValor());
						doc.setRateio(rateioService.findByDocumento(doc));					
						listaDocumento.add(doc);
						listaDocumentoValecompra.add(doc);
						
						baixarContaBean.setListaDocumento(listaDocumento);
						baixarContaBean.setRateio(doc.getRateio());
						baixarContaBean.setDtpagamento(bean2.getDtocorrencia() != null ? bean2.getDtocorrencia() : bean2.getDtcredito());
						baixarContaBean.setDtcredito(bean2.getDtcredito());
						baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
						baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
						baixarContaBean.setVinculo(bean.getConta());
						baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
						baixarContaBean.setChecknum(sdf.format(bean2.getDtcredito()));
						baixarContaBean.setFromRetorno(true);
						
						movimentacao = documentoService.doBaixarConta(request, baixarContaBean);
						if(movimentacao != null){
							notaDocumentoService.liberaNotaDocumentoWebservice(request, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//							contareceberService.incrementaNumDocumento(doc);
							documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
							listaMov.add(movimentacao);
						}
						
						arquivobancariodocumentoService.saveProcessamentoArquivoRetorno(getArquivobancario(bean), bean2.getDocumento());
					} else {
						if (docNaoProcessado.length() == 0)
							docNaoProcessado.append(bean2.getNossonumero());
						else
							docNaoProcessado.append(", " + bean2.getNossonumero());
					}							
					
				} else if (bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getOcorrencia() != null && bean2.getOcorrencia().equals("02")) {
					if (bean2.getNossonumero() != null && !bean2.getNossonumero().equals("")) {
						doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
						if (doc.getNossonumero() == null || !doc.getNossonumero().equals(bean2.getNossonumero())) {
							doc.setNossonumero(bean2.getNossonumero());
							documentoService.updateNossoNumero(doc);
						}
					}
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null){
						Documentoconfirmacao documentoconfirmacao = new Documentoconfirmacao();
						documentoconfirmacao.setData(SinedDateUtils.currentDate());
						documentoconfirmacao.setDocumento(bean2.getDocumento());
						documentoconfirmacaoService.saveOrUpdate(documentoconfirmacao);
					}					
				}
			}
					
			for (ArquivoRetornoRuralBean bean2 : bean.getListaContaRural()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(bean2.getDocumento());
				}
			}
			
			for (Movimentacao movimentacao2 : listaMov) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}
			
			if (docNaoProcessado.length() > 0)
				request.addMessage("Aten��o! Alguns documentos que est�o com situa��o diferente de prevista e definitiva n�o foram processados. Nossos n�meros: " + docNaoProcessado.toString(), MessageType.ERROR);
			else
				request.addMessage("Informa��es processadas.");
			
//			valecompraService.createValecompraArquivoRetorno(request, listaDocumentoValecompra, listaMov);
			
			try {
				List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
				for (ArquivoRetornoRuralBean bean2 : bean.getListaContaRural()) {
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
						listaDocumentoForComissionamento.add(bean2.getDocumento());
					}
				}
				contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		}
		
		movimentacaoService.callProcedureAtualizaMovimentacaoNecessidade();
		
		bean.setArquivo(null);
		return continueOnAction("carregar", bean);
	}
	
	public ModelAndView processarInfoBancoBrasil(WebRequestContext request, ArquivoRetornoBean bean){
		
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
			BaixarContaBean baixarContaBean;
			List<Documento> listaDocumento;
//			List<Documento> listaDocumentoValecompra = new ArrayList<Documento>();
			Documento doc;
			Movimentacao movimentacao;
			List<Movimentacao> listaMov = new ArrayList<Movimentacao>();
			
			for (ArquivoRetornoBrasilBean bean2 : bean.getListaContaBB()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					
					baixarContaBean = new BaixarContaBean();
					listaDocumento = new ArrayList<Documento>();
					
					doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
					doc.setValoratual(bean2.getValor());
					doc.setRateio(rateioService.findByDocumento(doc));
					listaDocumento.add(doc);
					
					baixarContaBean.setListaDocumento(listaDocumento);
					baixarContaBean.setRateio(doc.getRateio());
					baixarContaBean.setDtpagamento(bean2.getDtocorrencia() != null ? bean2.getDtocorrencia() : bean2.getDtcredito());
					baixarContaBean.setDtcredito(bean2.getDtcredito());
					baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
					baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
					baixarContaBean.setVinculo(bean.getConta());
					baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
					baixarContaBean.setChecknum(sdf.format(bean2.getDtcredito()));
					baixarContaBean.setFromRetorno(true);
					
					movimentacao = documentoService.doBaixarConta(null, baixarContaBean);
					notaDocumentoService.liberaNotaDocumentoWebservice(request, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//					contareceberService.incrementaNumDocumento(doc);
					documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
					arquivobancariodocumentoService.saveProcessamentoArquivoRetorno(getArquivobancario(bean), bean2.getDocumento());
					
					listaMov.add(movimentacao);
				}else if (bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getOcorrencia() != null && bean2.getOcorrencia().equals("02")) {
					if (bean2.getNossonumero() != null && !bean2.getNossonumero().equals("")) {
						doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
						if (doc.getNossonumero() == null || !doc.getNossonumero().equals(bean2.getNossonumero())) {
							doc.setNossonumero(bean2.getNossonumero());
							documentoService.updateNossoNumero(doc);
						}
					}
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null){
						Documentoconfirmacao documentoconfirmacao = new Documentoconfirmacao();
						documentoconfirmacao.setData(SinedDateUtils.currentDate());
						documentoconfirmacao.setDocumento(bean2.getDocumento());
						documentoconfirmacaoService.saveOrUpdate(documentoconfirmacao);
					}
				}
			}
					
			for (ArquivoRetornoBrasilBean bean2 : bean.getListaContaBB()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(bean2.getDocumento());
				}
			}
			
			for (Movimentacao movimentacao2 : listaMov) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}
			
			request.addMessage("Informa��es processadas.");
			
//			valecompraService.createValecompraArquivoRetorno(request, listaDocumentoValecompra, listaMov);
			
			try {
				List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
				for (ArquivoRetornoBrasilBean bean2 : bean.getListaContaBB()) {
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
						listaDocumentoForComissionamento.add(bean2.getDocumento());
					}
				}
				contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		}
		
		movimentacaoService.callProcedureAtualizaMovimentacaoNecessidade();
		
		bean.setArquivo(null);
		return continueOnAction("carregar", bean);
	}	
	
	public String agrupaTarifaByTipo(String motivoTarifa){
		String observacao = null;
		switch(Integer.parseInt(motivoTarifa)){
		case 6:
			observacao = MotivoOcorrenciaTarifa.COB_INTERN.getNome();
			break;
		case 2:
			observacao = MotivoOcorrenciaTarifa.COB_LOTERI.getNome();
			break;
		case 3:
			observacao = MotivoOcorrenciaTarifa.COB_AGENC.getNome();
			break;
		case 4:
			observacao = MotivoOcorrenciaTarifa.COB_COMPE.getNome();
			break;
		}
		return observacao;
	}
	
	/**
	 * Caso a conta importada, do arquivo de retorno CEF, possua tarifa, 
	 * ser� gerada uma nova movimenta��o de d�bito para registrar o valor da tarifa.
	 * @param movimentacao
	 * @author Taidson
	 * @param listaDocumentoNaoMarcadaByTarifa 
	 * @since 17/08/2010
	 */
	public void gerarMovimentacaoTarifa(Movimentacao movimentacao, List<ArquivoRetornoCAIXABean> listaDocumentoNaoMarcadaByTarifa){
		if(movimentacao.getValorTarifa() != null && movimentacao.getValorTarifa().getValue().doubleValue() > 0.0){
			Set<Movimentacaohistorico> lista = new ListSet<Movimentacaohistorico>(Movimentacaohistorico.class);
			
			Movimentacao novaMovimentacao = new Movimentacao();
			
			Rateioitem rateioitem = new Rateioitem();
			
			List<Rateioitem> listaRateioitem = new ListSet<Rateioitem>(Rateioitem.class);
			Rateio rateio = new Rateio();
			
			Centrocusto centrocusto = new Centrocusto();
			centrocusto.setCdcentrocusto(Integer.parseInt(parametrogeralService.getValorPorNome("CENTROCUSTO_TARIFA_CEF")));
			
			Contagerencial contagerencial = new Contagerencial();
			contagerencial.setCdcontagerencial(Integer.parseInt(parametrogeralService.getValorPorNome("CONTAGERENCIAL_TARIFA_CEF")));
			
			rateioitem.setCentrocusto(centrocusto);
			rateioitem.setContagerencial(contagerencial);
			
			rateioitem.setValor(movimentacao.getValorTarifa());
			listaRateioitem.add(rateioitem);
			rateio.setListaRateioitem(listaRateioitem);
			
			novaMovimentacao.setDtmovimentacao(movimentacao.getDtdebitotarifa());
			novaMovimentacao.setConta(movimentacao.getConta());
			novaMovimentacao.setValor(movimentacao.getValorTarifa());
			novaMovimentacao.setMovimentacaoacao(Movimentacaoacao.NORMAL);
			novaMovimentacao.setTipooperacao(Tipooperacao.TIPO_DEBITO);
			novaMovimentacao.setRateio(rateio);
			novaMovimentacao.setHistorico(descricaoMotivoTarifa(movimentacao.getMotivoTarifa()));
			Movimentacaohistorico mh = new Movimentacaohistorico();
			mh.setMovimentacaoacao(Movimentacaoacao.NORMAL);
			
			mh.setObservacao(descricaoMotivoTarifa(movimentacao.getMotivoTarifa()));
			mh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			mh.setDtaltera(new Timestamp(System.currentTimeMillis())); 
			
			lista.add(mh);
			novaMovimentacao.setListaMovimentacaohistorico(lista);
			
			if(!movimentacaoService.existDuplicidadeProcessamento(novaMovimentacao, listaDocumentoNaoMarcadaByTarifa)){
				movimentacaoService.saveOrUpdateNoUseTransaction(novaMovimentacao);
				movimentacaoService.callProcedureAtualizaMovimentacao(novaMovimentacao);
			}
		}
	}
	
	/**
	 * Retorna a nome do motivo de tarifa a partir do c�digo obtido no arquivo CEF.
	 * @param motivoTarifa
	 * @return
	 * @author Taidson
	 * @since 17/08/2010
	 */
	public String descricaoMotivoTarifa(String motivoTarifa){
		String observacao = null;
		try{
			switch(Integer.parseInt(motivoTarifa)){
			case 6:
				observacao = MotivoOcorrenciaTarifa.COB_INTERN.getNome();
				break;
			case 2:
				observacao = MotivoOcorrenciaTarifa.COB_LOTERI.getNome();
				break;
			case 3:
				observacao = MotivoOcorrenciaTarifa.COB_AGENC.getNome();
				break;
			case 4:
				observacao = MotivoOcorrenciaTarifa.COB_COMPE.getNome();
				break;
			}
		} catch (Exception e) {
			
		}
		return observacao;
	}
	
	public ModelAndView imprimirReport(WebRequestContext request, ArquivoRetornoBean bean) throws Exception{
		
		Report report = new Report("financeiro/arquivoretorno");
		
		if (bean.getListaContaBradesco() != null)
			report.setDataSource(bean.getListaContaBradesco());
		else if (bean.getListaContaItau() != null)
			report.setDataSource(bean.getListaContaItau());
		else if (bean.getListaContaBB() != null)
			report.setDataSource(bean.getListaContaBB());
		else if (bean.getListaConta() != null)
			report.setDataSource(bean.getListaConta());
		else if (bean.getListaContaSantander() != null)
			report.setDataSource(bean.getListaContaSantander());
		else if (bean.getListaContaRural() != null)
			report.setDataSource(bean.getListaContaRural());
		
		Empresa empresa = EmpresaService.getInstance().loadPrincipal();
		
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocialOuNome());
		report.addParameter("TITULO", "RELAT�RIO DOS REGISTROS NO ARQUIVO DE RETORNO");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		MergeReport mergeReport = new MergeReport("arquivoretorno_" + SinedUtil.datePatternForReport());
		
		mergeReport.addReport(report);
		
		return new ResourceModelAndView(mergeReport.generateResource());
		
	}
	
	private List<ArquivoRetornoRuralBean> getListaContaReceberByRetornoRural400(ArquivoRetornoBean beanAux, List<EDIRegistroRetornoRural400> list) {
		List<ArquivoRetornoRuralBean> listaBean = new ArrayList<ArquivoRetornoRuralBean>();
		ArquivoRetornoRuralBean bean;
		String dtCreditoStr;
		Date dtCredito;
		String dtOcorrenciaStr;
		Date dtOcorrencia;
		Integer cddocumento;
		String nossoNumero;
		String cdOcorrencia;
		Money valor;
		Documento doc;
		Boolean documentoNaoEncontrado = false;
		Boolean erroTransformacao = false;
		List<String> listaDocumentoNaoEncontrado = new ArrayList<String>(); 
		List<String> listaErroTransformacao = new ArrayList<String>();
		
		Money valorTotalPagamento = new Money(0.0);
		Money valorTotalReceber = new Money(0.0);
		for (EDIRegistroRetornoRural400 detalheTRetornoRural400 : list) {
			bean = new ArquivoRetornoRuralBean();
			dtCreditoStr = detalheTRetornoRural400.getDtCredito();
			dtOcorrenciaStr = detalheTRetornoRural400.getDtOcorrencia();
			dtCredito = null;
			try {
				if(dtCreditoStr != null && !dtCreditoStr.trim().equals("") && !dtCreditoStr.trim().equals("00002000")){
					dtCredito = new Date(new SimpleDateFormat("ddMMyyyy").parse(dtCreditoStr).getTime());
				} 
				if(dtOcorrenciaStr != null && !"".equals(dtOcorrenciaStr)){
					dtOcorrencia = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(dtOcorrenciaStr).getTime());
					bean.setDtocorrencia(dtOcorrencia);
				}
				cddocumento  = Integer.parseInt(detalheTRetornoRural400.getUsoEmpresa().trim());
				nossoNumero  = detalheTRetornoRural400.getUsoEmpresa().trim(); 
				cdOcorrencia = detalheTRetornoRural400.getCdOcorrencia().trim();
				valor = new Money(Integer.parseInt(detalheTRetornoRural400.getValortitulo()), true);
			} catch (Exception e) {
				e.printStackTrace();
				erroTransformacao = true;
				listaErroTransformacao.add(detalheTRetornoRural400.getUsoEmpresa());
				continue;
			}
			bean.setDtcredito(dtCredito);		
			if (cdOcorrencia.equals("02") || cdOcorrencia.equals("06")) {
				doc = contareceberService.loadForRetorno(new Documento(cddocumento));
				if (doc != null) {
					bean.setDocumento(doc);
					bean.setValor(valor);					
					bean.setNossonumero(nossoNumero);
					bean.setOcorrencia(cdOcorrencia);
					valorTotalPagamento = valorTotalPagamento.add(valor);
					valorTotalReceber = valorTotalReceber.add(doc.getAux_documento() != null && doc.getAux_documento().getValoratual() != null ? doc.getAux_documento().getValoratual() : new Money(0.0));
					listaBean.add(bean);
				} else {
					documentoNaoEncontrado = true;
					listaDocumentoNaoEncontrado.add(cddocumento.toString());
				}
			}		
		}		
		
		if (erroTransformacao) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui registros que est�o com dados incompletos ou inv�lidos. Nossos n�meros: ");
			for (String erro : listaErroTransformacao) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}
		if (documentoNaoEncontrado) {
			StringBuilder erroSB = new StringBuilder("Aten��o! O arquivo possui documentos que n�o foram encontrados no contas a receber. Nossos n�meros: ");
			for (String erro : listaDocumentoNaoEncontrado) {
				erroSB.append(erro).append(", ");
			}
			erroSB.delete(erroSB.length() - 2, erroSB.length());
			NeoWeb.getRequestContext().addError(erroSB.toString());
		}	
		beanAux.setValorTotalPagamento(valorTotalPagamento);
		beanAux.setValorTotalContaReceber(valorTotalReceber);
		return listaBean;
	}
	
	private Arquivobancario getArquivobancario(ArquivoRetornoBean bean){
		Arquivobancario arquivobancario = bean.getArquivobancario();
		
		if (arquivobancario==null){
			//Para alguns bancos n�o foi implementado a grava��o em Arquivobancario 
			Arquivo arquivo = (Arquivo) NeoWeb.getRequestContext().getSession().getAttribute(ArquivoRetornoProcess.ARQUIVORETORNO_SESSION);
			if (arquivo!=null){
				arquivoService.saveOrUpdate(arquivo);
				
				arquivobancario = new Arquivobancario();
				arquivobancario.setNome(arquivo.getNome());
				arquivobancario.setArquivo(arquivo);
				arquivobancario.setArquivobancariotipo(Arquivobancariotipo.RETORNO);
				arquivobancario.setArquivobancariosituacao(Arquivobancariosituacao.PROCESSADO);
				arquivobancario.setDtgeracao(SinedDateUtils.currentDate());
				
				arquivobancarioService.saveOrUpdate(arquivobancario);
				arquivoDAO.saveFile(arquivobancario, "arquivo");
				
				bean.setArquivobancario(arquivobancario);
			}
		}
		
		return arquivobancario;
	}
}
