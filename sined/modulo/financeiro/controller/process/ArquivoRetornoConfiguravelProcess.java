package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Bancoretorno;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoconfirmacao;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivobancarioService;
import br.com.linkcom.sined.geral.service.ArquivobancariodocumentoService;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.geral.service.BancoretornoService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoconfirmacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoConfiguravel;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoConfiguravelBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoConfiguravelDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.util.SinedDateUtils;

@Controller(
		path="/financeiro/process/ArquivoRetornoConfiguravel",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ArquivoRetornoConfiguravelProcess extends MultiActionController{
	
	private static final String ARQUIVORETORNO_SESSION = "ArquivoRetornoConfiguravelProcess_SESSION";
	private BancoService bancoService;
	private BancoretornoService bancoretornoService;
	private ContareceberService contareceberService;
	private RateioService rateioService;
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private NotaDocumentoService notaDocumentoService;
	private DocumentoconfirmacaoService documentoconfirmacaoService;
	private ValecompraService valecompraService;
	private ArquivoService arquivoService;
	private ArquivobancarioService arquivobancarioService;
	private ArquivobancariodocumentoService arquivobancariodocumentoService;
	private ArquivoDAO arquivoDAO;
	private ContratoService contratoService;
	
	public void setDocumentoconfirmacaoService(DocumentoconfirmacaoService documentoconfirmacaoService) {this.documentoconfirmacaoService = documentoconfirmacaoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setBancoretornoService(BancoretornoService bancoretornoService) {this.bancoretornoService = bancoretornoService;}
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {this.arquivobancarioService = arquivobancarioService;}
	public void setArquivobancariodocumentoService(ArquivobancariodocumentoService arquivobancariodocumentoService) {this.arquivobancariodocumentoService = arquivobancariodocumentoService;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, ArquivoRetornoConfiguravelBean bean) throws Exception{
		request.setAttribute("listaBanco", bancoService.findAtivos());
		return new ModelAndView("/process/arquivoretornoconfiguravel", "filtro", new ArquivoRetornoConfiguravelBean());
	}
	
	@Input("carregar")
	public ModelAndView processarArquivo(WebRequestContext request, ArquivoRetornoConfiguravelBean bean) throws Exception{
		
		Bancoretorno conf = bancoretornoService.load(bean.getBancoretorno());
		Banco banco = bancoService.load(bean.getBanco(), "banco.cdbanco, banco.numero");
		Arquivo arquivo = bean.getArquivo();
		
		String stringArquivo = new String(arquivo.getContent());
		String[] linha = stringArquivo.split("\\r?\\n");
		
		request.getSession().setAttribute(ARQUIVORETORNO_SESSION, arquivo);
		
		ArquivoRetornoConfiguravel execucao = new ArquivoRetornoConfiguravel(conf, banco.getNumero(), linha);
		
		try{
			execucao.execute();
			
			bean.setConta(execucao.getConta());
			bean.setListaDocumento(execucao.getLista());
			
			if(bean.getConta() == null){
				throw new Exception("Conta banc�ria n�o encontrada. Ag�ncia: " + execucao.getAgencia_cedente() + " C�digo do cliente(Conta ou Conv�nio): " + execucao.getConta_cedente());
			}
			
			List<ArquivoRetornoConfiguravelDocumentoBean> listaDocumento = bean.getListaDocumento();
			if(listaDocumento == null || listaDocumento.size() == 0){
				throw new Exception("Nenhum documento encontrado.");
			}
			
			List<ArquivoRetornoConfiguravelDocumentoBean> listaConferencia = new ArrayList<ArquivoRetornoConfiguravelDocumentoBean>();
			boolean conferencia = false;
			for (ArquivoRetornoConfiguravelDocumentoBean b : listaDocumento) {
				if(b.getListaDocumento() != null && b.getListaDocumento().size() > 1){
					conferencia = true;
					listaConferencia.add(b);
				}
			}
			bean.setListaConferencia(listaConferencia);
			
			if(conferencia){
				return new ModelAndView("/process/arquivoretornoconfiguravel_conferencia", "bean", bean);
			}
			
		} catch (Exception e) {
			request.addError("Erro no processamento do arquivo.");
			request.addError(e.getMessage());
			e.printStackTrace();
			return continueOnAction("carregar", bean);
		}
		
		return continueOnAction("processarConferencia", bean);
	}
	
	/**
	 * Processa a confer�ncia de documentos caso tenha algum conflito.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @since 18/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView processarConferencia(WebRequestContext request, ArquivoRetornoConfiguravelBean bean){
		if(bean.getListaConferencia() != null && bean.getListaConferencia().size() > 0){
			for (ArquivoRetornoConfiguravelDocumentoBean b : bean.getListaConferencia()) {
				if(b.getListaDocumento() != null && b.getListaDocumento().size() > 0){
					for (ArquivoRetornoConfiguravelDocumentoBean b2  : bean.getListaDocumento()) {
						if(b.getNossonumero().equals(b2.getNossonumero())){
							for (Documento documento : b.getListaDocumento()) {
								if(documento.getSelected() != null && documento.getSelected()){
									documento = contareceberService.loadForRetorno(documento);
									documento.setValoratual(DocumentoService.getInstance().getValorAtual(documento.getCddocumento(), b2.getDtcredito()));
									b2.setDocumento(documento);
									break;
								}
							}
							break;
						}
					}
				}
			}
		}
		
		return new ModelAndView("/process/arquivoretornoconfiguravel_listadocumento", "bean", bean);
	}
	
	@Input("carregar")
	public ModelAndView processarArquivoValidarArquivo(WebRequestContext request, ArquivoRetornoConfiguravelBean bean) throws Exception{
		
		Bancoretorno conf = bancoretornoService.load(bean.getBancoretorno());
		Banco banco = bancoService.load(bean.getBanco(), "banco.cdbanco, banco.numero");
		Arquivo arquivo = bean.getArquivo();
		
		String stringArquivo = new String(arquivo.getContent());
		String[] linha = stringArquivo.split("\\r?\\n");
		
		ArquivoRetornoConfiguravel execucao = new ArquivoRetornoConfiguravel(conf, banco.getNumero(), linha);
		
		try{
			execucao.executeValidarArquivo();
			
			bean.setConta(execucao.getConta());
			bean.setListaDocumento(execucao.getLista());
			
			if(bean.getListaDocumento() == null || bean.getListaDocumento().size() == 0){
				throw new Exception("Nenhum documento encontrado.");
			}
		} catch (Exception e) {
			request.addError("Erro no processamento do arquivo.");
			request.addError(e.getMessage());
			e.printStackTrace();
			return continueOnAction("carregar", bean);
		}
		
		return new ModelAndView("/process/arquivoretornoconfiguravel_validararquivo", "bean", bean);
	}
	
	public ModelAndView processarDocumentos(WebRequestContext request, ArquivoRetornoConfiguravelBean bean) throws Exception{
		
		try{
			AtomicInteger qtdeMovimentacao = new AtomicInteger(0);
			AtomicLong valor_COB_INTERN = new AtomicLong(0);
			AtomicLong valor_COB_LOTERI = new AtomicLong(0);
			AtomicLong valor_COB_AGENC = new AtomicLong(0);
			AtomicLong valor_COB_COMPE = new AtomicLong(0);
			AtomicLong valor_Outras_Tarifas = new AtomicLong(0);
			
			List<Documento> listaDocumentoValecompra = new ArrayList<Documento>();
			BaixarContaBean baixarContaBean;
			List<Documento> listaDocumento;
			Documento doc;
			Movimentacao movimentacao;
			List<Movimentacao> listaMov = new ArrayList<Movimentacao>();
			Rateio rateio;
			
			Arquivobancario arquivobancario = getArquivobancario(request);
			
			for (ArquivoRetornoConfiguravelDocumentoBean bean2 : bean.getListaDocumento()) {
				if(bean2.getMarcado() != null && bean2.getMarcado()){
					
					if(bean2.getConfirmacao() != null && bean2.getConfirmacao()){
						
						Documentoconfirmacao documentoconfirmacao = new Documentoconfirmacao();
						documentoconfirmacao.setData(SinedDateUtils.currentDate());
						documentoconfirmacao.setDocumento(bean2.getDocumento());
						documentoconfirmacaoService.saveOrUpdate(documentoconfirmacao);
						
					} else {
					
						baixarContaBean = new BaixarContaBean();
						listaDocumento = new ArrayList<Documento>();
						
						doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
						
						if(doc.getValor().getValue().doubleValue() == 0d){
							rateio = contareceberService.atualizaValorRateio(doc, bean2.getValor());
							doc = contareceberService.loadForEntradaWithDadosPessoa(bean2.getDocumento());
						} else {
							rateio = rateioService.findByDocumento(doc);
							rateioService.atualizaValorRateio(rateio, bean2.getValor());
						}
						
						doc.setValoratual(bean2.getValor());
						doc.setValoratualbaixa(bean2.getValor());
						doc.setRateio(rateio);
						
						listaDocumento.add(doc);
						listaDocumentoValecompra.add(doc);
						
						baixarContaBean.setListaDocumento(listaDocumento);
						baixarContaBean.setRateio(doc.getRateio());
						baixarContaBean.setDtpagamento(bean2.getDtcredito());
						baixarContaBean.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
						baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
						baixarContaBean.setVinculo(bean.getConta());
						baixarContaBean.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
						baixarContaBean.setChecknum(bean2.getNossonumero());
						baixarContaBean.setFromRetorno(true);
						
						movimentacao = documentoService.doBaixarConta(null, baixarContaBean);
						notaDocumentoService.liberaNotaDocumentoWebservice(request, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//						contareceberService.incrementaNumDocumento(doc);
						documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
						if(movimentacao != null){
							movimentacao.setValorTarifa(bean2.getValorTarifa());
							movimentacao.setMotivoTarifa(bean2.getMotivoTarifa());
							movimentacao.setDtmovimentacao(bean2.getDtcredito());
						}
						listaMov.add(movimentacao);
						if(movimentacao != null && movimentacao.getValorTarifa() != null && movimentacao.getValorTarifa().toLong() > 0){
							if(movimentacao.getMotivoTarifa() != null){
								if(movimentacao.getMotivoTarifa().equals("02")){
									valor_COB_LOTERI.addAndGet(movimentacao.getValorTarifa().toLong());
								}else if(movimentacao.getMotivoTarifa().equals("03")){
									valor_COB_AGENC.addAndGet(movimentacao.getValorTarifa().toLong());
								}else if(movimentacao.getMotivoTarifa().equals("04")){
									valor_COB_COMPE.addAndGet(movimentacao.getValorTarifa().toLong());
								}else if(movimentacao.getMotivoTarifa().equals("06")){
									valor_COB_INTERN.addAndGet(movimentacao.getValorTarifa().toLong());
								}
							} else {
								valor_Outras_Tarifas.addAndGet(movimentacao.getValorTarifa().toLong());
							}
							
							qtdeMovimentacao.addAndGet(1);
						}
						
						arquivobancariodocumentoService.saveProcessamentoArquivoRetorno(arquivobancario, bean2.getDocumento());
					}
				}
			}
					
			
			
			for (ArquivoRetornoConfiguravelDocumentoBean bean2 : bean.getListaDocumento()) {
				if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(bean2.getDocumento());
				}
			}
			
			for (Movimentacao movimentacao2 : listaMov) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}
			
			/**
			 * Efetua o agrupamento de taxa por motivo de taxa.
			 * Ser� gerada uma movimenta��o por motivo de taxa.
			 * Taidson
			 * 19/08/2010
			 */
			boolean break_COB_LOTERI = false;
			boolean break_CCOB_AGENC = false;
			boolean break_OB_COMPE = false;
			boolean break_COB_INTERN = false;
			boolean break_Outras_Tarifas = false;
			for(int i=0; i < qtdeMovimentacao.get(); i++){
				
				if(valor_COB_LOTERI.get() > 0 && !break_COB_LOTERI){
					listaMov.get(i).setValorTarifa(new Money(valor_COB_LOTERI.get(), true));
					listaMov.get(i).setMotivoTarifa("02");
					movimentacaoService.gerarMovimentacaoTarifa(listaMov.get(i));
					break_COB_LOTERI = true;
				}
				if(valor_COB_AGENC.get() > 0 && !break_CCOB_AGENC){
					listaMov.get(i).setValorTarifa(new Money(valor_COB_AGENC.get(), true));
					listaMov.get(i).setMotivoTarifa("03");
					movimentacaoService.gerarMovimentacaoTarifa(listaMov.get(i));
					break_CCOB_AGENC = true;
				}
				if(valor_COB_COMPE.get() > 0 && !break_OB_COMPE){
					listaMov.get(i).setValorTarifa(new Money(valor_COB_COMPE.get(), true));
					listaMov.get(i).setMotivoTarifa("04");
					movimentacaoService.gerarMovimentacaoTarifa(listaMov.get(i));
					break_OB_COMPE = true;
				}
				if(valor_COB_INTERN.get() > 0 && !break_COB_INTERN){
					listaMov.get(i).setValorTarifa(new Money(valor_COB_INTERN.get(), true));
					listaMov.get(i).setMotivoTarifa("06");
					movimentacaoService.gerarMovimentacaoTarifa(listaMov.get(i));
					break_COB_INTERN = true;
				}
				if(valor_Outras_Tarifas.get() > 0 && !break_Outras_Tarifas){
					listaMov.get(i).setValorTarifa(new Money(valor_Outras_Tarifas.get(), true));
					listaMov.get(i).setMotivoTarifa("99");
					movimentacaoService.gerarMovimentacaoTarifa(listaMov.get(i));
					break_Outras_Tarifas = true;
				}
			}
			
			request.addMessage("Informa��es processadas.");
			
//			valecompraService.createValecompraArquivoRetorno(request, listaDocumentoValecompra, listaMov);
			
			try {
				List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
				for (ArquivoRetornoConfiguravelDocumentoBean bean2 : bean.getListaDocumento()) {
					if(bean2.getDocumento() != null && bean2.getDocumento().getCddocumento() != null && bean2.getMarcado() != null && bean2.getMarcado()){
						listaDocumentoForComissionamento.add(bean2.getDocumento());
					}
				}
				contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		return continueOnAction("carregar", bean);
	}
	
	private Arquivobancario getArquivobancario(WebRequestContext request){
		Arquivo arquivo = (Arquivo) request.getSession().getAttribute(ARQUIVORETORNO_SESSION);
		arquivoService.saveOrUpdate(arquivo);
		
		Arquivobancario arquivobancario  = new Arquivobancario();
		arquivobancario.setNome(arquivo.getNome());
		arquivobancario.setArquivo(arquivo);
		arquivobancario.setArquivobancariotipo(Arquivobancariotipo.RETORNO);
		arquivobancario.setArquivobancariosituacao(Arquivobancariosituacao.PROCESSADO);
		arquivobancario.setDtgeracao(SinedDateUtils.currentDate());
		
		arquivobancarioService.saveOrUpdate(arquivobancario);
		arquivoDAO.saveFile(arquivobancario, "arquivo");
			
		return arquivobancario;
	}
}