package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.GeraMovimentacao;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FormapagamentoService;
import br.com.linkcom.sined.geral.service.GerarMovimentacaoService;
import br.com.linkcom.sined.geral.service.HistoricooperacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.GeraMovimentacaocheque;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/financeiro/process/GerarMovimentacao",
		authorizationModule=ProcessAuthorizationModule.class
)
/**
 * Controller para o processo de Gerar Movimenta��o Financeira (Transfer�ncia interna).
 * 
 * @author Fl�vio Tavares
 */
public class GerarMovimentacaoProcess extends MultiActionController{
	
	public static final String CLOSE_ON_CANCEL = "closeOnCancel";
	
	private MovimentacaoService movimentacaoService;
	private ContaService contaService;
	private RateioService rateioService;
	private CentrocustoService centrocustoService;
	private GerarMovimentacaoService gerarMovimentacaoService;
	private ProjetoService projetoService;
	private ChequeService chequeService;
	private ContagerencialService contagerencialService;
	private ParametrogeralService parametrogeralService;
	private FormapagamentoService formapagamentoService;
	private EmpresaService empresaService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private HistoricooperacaoService historicooperacaoService;
	
	public void setFormapagamentoService(
			FormapagamentoService formapagamentoService) {
		this.formapagamentoService = formapagamentoService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setGerarMovimentacaoService(GerarMovimentacaoService gerarMovimentacaoService) {
		this.gerarMovimentacaoService = gerarMovimentacaoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}	
	public void setChequeService(ChequeService chequeService) {
		this.chequeService = chequeService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}
	public void setHistoricooperacaoService(
			HistoricooperacaoService historicooperacaoService) {
		this.historicooperacaoService = historicooperacaoService;
	}
	
	/**
	 * M�todo para inicializar as movimenta��es de origem e destino e carregar o jsp.
	 * 
	 * @see #entrada(WebRequestContext, GeraMovimentacao)
	 * @param request
	 * @param geraMovimentacao
	 * @return
	 * @throws Exception
	 */
	@DefaultAction
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, GeraMovimentacao geraMovimentacao) throws Exception{
		this.setEntradaValues(request, geraMovimentacao);
		return entrada(request, geraMovimentacao);
	}
	
	@Action("tranferenciaLote")
	@Input("criar")
	public ModelAndView tranferenciaLote(WebRequestContext request, GeraMovimentacao geraMovimentacao) throws Exception{
		String whereInMovimentacao = request.getParameter("whereInMovimentacao");
		if(whereInMovimentacao != null && !whereInMovimentacao.trim().equals("")){
			List<Movimentacao> listaMovimentacao = movimentacaoService.findForTransferenciaLote(whereInMovimentacao);
			
			Integer cdcontagencialcredito = parametrogeralService.getIntegerNullable(Parametrogeral.CONTA_TRANSFERENCIA_CREDITO);
			Integer cdcontagencialdebito = parametrogeralService.getIntegerNullable(Parametrogeral.CONTA_TRANSFERENCIA_DEBITO);
			Integer cdcentrocustocredito = parametrogeralService.getIntegerNullable(Parametrogeral.CENTROCUSTO_TRANSFERENCIA_CREDITO);
			Integer cdcentrocustodebito = parametrogeralService.getIntegerNullable(Parametrogeral.CENTROCUSTO_TRANSFERENCIA_DEBITO);
			
			Contagerencial contagerencialcredito = cdcontagencialcredito != null ? contagerencialService.loadWithIdentificador(new Contagerencial(cdcontagencialcredito)) : null;
			Contagerencial contagerencialdebito = cdcontagencialdebito != null ? contagerencialService.loadWithIdentificador(new Contagerencial(cdcontagencialdebito)) : null;
			Centrocusto centrocustocredito = cdcentrocustocredito != null ? centrocustoService.load(new Centrocusto(cdcentrocustocredito)) : null;
			Centrocusto centrocustodebito = cdcentrocustodebito != null ? centrocustoService.load(new Centrocusto(cdcentrocustodebito)) : null;
			
			List<GeraMovimentacaocheque> listaGeraMovimentacaocheque = new ArrayList<GeraMovimentacaocheque>();
			Money valortotal = new Money();
			Conta contaorigem = null;
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			
			for (Movimentacao movimentacao : listaMovimentacao) {
				valortotal = valortotal.add(movimentacao.getValor());
				contaorigem = movimentacao.getConta();
				
				if(movimentacao.getCheque() != null){
					GeraMovimentacaocheque geraMovimentacaoCheque = new GeraMovimentacaocheque();
					geraMovimentacaoCheque.setCheque(movimentacao.getCheque());
					geraMovimentacaoCheque.setValor(movimentacao.getValor());
					
					listaGeraMovimentacaocheque.add(geraMovimentacaoCheque);
				}
				
				if(movimentacao.getEmpresa() != null && !listaEmpresa.contains(movimentacao.getEmpresa())){
					listaEmpresa.add(movimentacao.getEmpresa());
				}
			}
			
			geraMovimentacao.setListaGeraMovimentacaocheque(listaGeraMovimentacaocheque);
			
			geraMovimentacao.getMovimentacaoorigem().setConta(contaorigem);
			geraMovimentacao.getMovimentacaoorigem().setValor(valortotal);
			if(listaEmpresa.size() == 1){
				geraMovimentacao.getMovimentacaoorigem().setEmpresa(listaEmpresa.get(0));
			}
			
			geraMovimentacao.getMovimentacaodestino().setValor(valortotal);
			
			List<Rateioitem> listaRateioitemcredito = new ArrayList<Rateioitem>();
			listaRateioitemcredito.add(new Rateioitem(contagerencialcredito, centrocustocredito, null, valortotal, 100d));
			
			geraMovimentacao.getRateio().setListaRateioitem(listaRateioitemcredito);
			geraMovimentacao.getRateio().setValortotaux(valortotal.toString());
			geraMovimentacao.getRateio().setValoraux("0,00");
			
			List<Rateioitem> listaRateioitemdebito = new ArrayList<Rateioitem>();
			listaRateioitemdebito.add(new Rateioitem(contagerencialdebito, centrocustodebito, null, valortotal, 100d));
			
			geraMovimentacao.getRateio2().setListaRateioitem(listaRateioitemdebito);
			geraMovimentacao.getRateio2().setValortotaux(valortotal.toString());
			geraMovimentacao.getRateio2().setValoraux("0,00");
		}
		
		
		this.setEntradaValues(request, geraMovimentacao);
		return entrada(request, geraMovimentacao);
	}
	
	/**
	 * M�todo para criar uma entrada em Gerar Movimenta��o com os dados de uma conta.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#findForGerarMovimentacao(Conta)
	 * @see br.com.linkcom.sined.geral.service.GerarMovimentacaoService#createBeanGeraMovimentacao(Conta)
	 * @see #setEntradaValues(WebRequestContext, GeraMovimentacao)
	 * @see #setTemplateParameters(WebRequestContext, GeraMovimentacao)
	 * @see #entrada(WebRequestContext, GeraMovimentacao)
	 * @param request
	 * @param conta
	 * @return
	 * @throws Exception
	 */
	@Action("entradaPagamentos")
	@Input("criar")
	public ModelAndView entradaPagamentos(WebRequestContext request, Conta conta) throws Exception{
		conta = contaService.findForGerarMovimentacao(conta);
		GeraMovimentacao geraMovimentacao = gerarMovimentacaoService.createBeanGeraMovimentacao(conta);
		this.setEntradaValues(request, geraMovimentacao);
		request.setAttribute(CLOSE_ON_CANCEL, true);
		return this.entrada(request, geraMovimentacao);
	}
	
	/**
	 * M�todo para preencher os valores padr�es da tela de Gerar Movimenta��o financeira.
	 * 
	 * @param request
	 * @param geraMovimentacao
	 */
	private void setEntradaValues(WebRequestContext request, GeraMovimentacao geraMovimentacao){
		Movimentacao origem = geraMovimentacao.getMovimentacaoorigem();
		Movimentacao destino = geraMovimentacao.getMovimentacaodestino();
		
		Date hoje = new Date(System.currentTimeMillis());
		
		origem.setTipooperacao(Tipooperacao.TIPO_DEBITO);
		origem.setDtmovimentacao(hoje);
		origem.setFormapagamento(Formapagamento.DINHEIRO);
		
		destino.setTipooperacao(Tipooperacao.TIPO_CREDITO);
		destino.setDtmovimentacao(hoje);
		destino.setFormapagamento(Formapagamento.DINHEIRO);
	}
	
	/**
	 * M�todo para carregar o objeto <code>GeraMovimentacao</code> no jsp.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#valorMaximoFormat(Conta)
	 * @see #setInfoForTemplate(WebRequestContext, GeraMovimentacao)
	 * @param request
	 * @param geraMovimentacao
	 * @return
	 * @throws Exception
	 */
	@Action("entrada")
	public ModelAndView entrada(WebRequestContext request,GeraMovimentacao geraMovimentacao)throws Exception{
		if(request.getBindException().hasErrors()){
			request.setAttribute("valorMaximo", contaService.valorMaximoFormat(geraMovimentacao.getMovimentacaoorigem().getConta()));
		}
		if(SinedUtil.isListNotEmpty(geraMovimentacao.getListaGeraMovimentacaocheque())){
			for(GeraMovimentacaocheque geraMovimentacaocheque : geraMovimentacao.getListaGeraMovimentacaocheque()){
				if(geraMovimentacaocheque.getCheque() != null && geraMovimentacaocheque.getCheque().getCdcheque() != null &&
						StringUtils.isEmpty(geraMovimentacaocheque.getCheque().getNumero())){
					Cheque cheque = chequeService.load(geraMovimentacaocheque.getCheque(), "cheque.numero");
					if(cheque != null)
						geraMovimentacaocheque.getCheque().setNumero(cheque.getNumero());
				}
			}
		}
		setInfoForTemplate(request, geraMovimentacao);
		request.setAttribute("integracao_contabil", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INTEGRACAO_CONTABIL)));
		Historicooperacao historico = historicooperacaoService.findByTipo(Tipohistoricooperacao.TRANSFERENCIA_ENTRE_CONTAS);
		request.setAttribute("possuiHistoricoConfiguravel", historico != null);
		request.setAttribute("possuiHistoricoComplementarConfiguravel", historico != null && StringUtils.isNotEmpty(historico.getHistoricocomplementar()));
		request.setAttribute("permiteComplementoManual", historico != null && Boolean.TRUE.equals(historico.getPermitircomplementomanual()));
		
		return new ModelAndView("process/gerarmovimentacaofinanceira").addObject("geraMovimentacao", geraMovimentacao);
	}
	
	/**
	 * M�todo respons�vel por setar os atributos do jsp de entrada com informa��es do bean.
	 * 
	 * @param request
	 * @param geraMovimentacao
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	private void setInfoForTemplate(WebRequestContext request, GeraMovimentacao geraMovimentacao) throws InstantiationException, IllegalAccessException {
		request.setAttribute("TEMPLATE_beanNameUncaptalized", "geraMovimentacao");
		request.setAttribute("TEMPLATE_beanName", "geraMovimentacao");
		request.setAttribute("TEMPLATE_enviar", "salvar");
		request.setAttribute("TEMPLATE_voltar", "listagem");
		request.setAttribute("TEMPLATE_beanDisplayName", Neo.getApplicationContext().getBeanDescriptor(null, GeraMovimentacao.class).getDisplayName());
		request.setAttribute("TEMPLATE_beanClass", GeraMovimentacao.class);
		request.setAttribute("gerarMovimentacao", true);
		if(geraMovimentacao.getRateio() != null && geraMovimentacao.getRateio().getListaRateioitem()!= null){
			request.setAttribute("listaCentrocusto", centrocustoService.findAtivos((List<Centrocusto>)CollectionsUtil.getListProperty(geraMovimentacao.getRateio().getListaRateioitem(), "centrocusto")));
		}
		request.setAttribute("credito", Tipooperacao.TIPO_CREDITO);
		request.setAttribute("debito", Tipooperacao.TIPO_DEBITO);
		
		List<Contagerencial> listaContGerenDebito = contagerencialService.findForGerarMovimentacao(Tipooperacao.TIPO_DEBITO);
		request.setAttribute("whereInCdcontagerencialOrigem", CollectionsUtil.listAndConcatenate(listaContGerenDebito, "cdcontagerencial", ","));
		request.setAttribute("whereInCdcontagerencialDestino", CollectionsUtil.listAndConcatenate(contagerencialService.findForGerarMovimentacao(Tipooperacao.TIPO_CREDITO), "cdcontagerencial", ","));
		
		boolean alterarValorRateioTransferencia = false;
		Empresa empresa = empresaService.getEmpresaForTransferencia();
		if(empresa != null){
			Rateio rateioDebito, rateioCredito;
			
			if(geraMovimentacao.getMovimentacaoorigem() != null && Tipooperacao.TIPO_CREDITO.equals(geraMovimentacao.getMovimentacaoorigem().getTipooperacao())){
				rateioCredito = geraMovimentacao.getRateio();
				rateioDebito = geraMovimentacao.getRateio2(); 
			}else {
				rateioDebito = geraMovimentacao.getRateio(); 
				rateioCredito = geraMovimentacao.getRateio2();
			}
			
			if(rateioDebito != null && (empresa.getCentrocustotransferencia() != null || 
					empresa.getContagerencialdebitotransferencia() != null)){
				if(SinedUtil.isListEmpty(rateioDebito.getListaRateioitem()) && rateioDebito.getListaRateioitem().size() > 0){
					boolean incluirPercentual = rateioDebito.getListaRateioitem().size() == 1;
					for(Rateioitem rateioitem : rateioDebito.getListaRateioitem()){
						if(rateioitem.getContagerencial() == null) rateioitem.setContagerencial(empresa.getContagerencialdebitotransferencia());
						if(rateioitem.getCentrocusto() == null) rateioitem.setCentrocusto(empresa.getCentrocustotransferencia());
						if(incluirPercentual) rateioitem.setPercentual(100d);
					}
				}else {
					List<Rateioitem> lisRateioitemDebito = new ArrayList<Rateioitem>();
					lisRateioitemDebito.add(new Rateioitem(empresa.getContagerencialdebitotransferencia(), empresa.getCentrocustotransferencia(), null, null, 100d));
					rateioDebito.setListaRateioitem(lisRateioitemDebito);
				}
				alterarValorRateioTransferencia = true;
			}
			if(rateioCredito != null && (empresa.getCentrocustotransferencia() != null || 
					empresa.getContagerencialcreditotransferencia() != null)){
				if(SinedUtil.isListEmpty(rateioCredito.getListaRateioitem()) && rateioCredito.getListaRateioitem().size() > 0){
					boolean incluirPercentual = rateioCredito.getListaRateioitem().size() == 1;
					for(Rateioitem rateioitem : rateioCredito.getListaRateioitem()){
						if(rateioitem.getContagerencial() == null) rateioitem.setContagerencial(empresa.getContagerencialcreditotransferencia());
						if(rateioitem.getCentrocusto() == null) rateioitem.setCentrocusto(empresa.getCentrocustotransferencia());
						if(incluirPercentual) rateioitem.setPercentual(100d);
					}
				}else {
					List<Rateioitem> lisRateioitemCredito = new ArrayList<Rateioitem>();
					lisRateioitemCredito.add(new Rateioitem(empresa.getContagerencialcreditotransferencia(), empresa.getCentrocustotransferencia(), null, null, 100d));
					rateioCredito.setListaRateioitem(lisRateioitemCredito);
				}
				alterarValorRateioTransferencia = true;
			}
		}
		request.setAttribute("alterarValorRateioTransferencia", alterarValorRateioTransferencia);
		request.setAttribute("listaFormapagamentoDebito", formapagamentoService.findDebito());
		request.setAttribute("listaFormapagamentoCredito", formapagamentoService.findCredito());
		request.setAttribute("listaEmpresaOrigem", empresaService.getListaEmpresaByConta(geraMovimentacao.getMovimentacaoorigem().getConta()));
		request.setAttribute("listaEmpresaDestino", empresaService.getListaEmpresaByConta(geraMovimentacao.getMovimentacaodestino().getConta()));
		Integer empresaidorigem = geraMovimentacao.getMovimentacaoorigem().getEmpresa() != null ? geraMovimentacao.getMovimentacaoorigem().getEmpresa().getCdpessoa() : null;
		Integer empresaiddestino = geraMovimentacao.getMovimentacaodestino().getEmpresa() != null ? geraMovimentacao.getMovimentacaodestino().getEmpresa().getCdpessoa() : null;
		
		if(geraMovimentacao.getMovimentacaoorigem().getConta() == null && geraMovimentacao.getMovimentacaodestino().getConta() == null){
			if(empresaidorigem == null && empresaiddestino == null && request.getSession().getAttribute("empresaSelecionada") != null){
				empresaidorigem = ((Empresa) request.getSession().getAttribute("empresaSelecionada")).getCdpessoa();
				empresaiddestino = ((Empresa) request.getSession().getAttribute("empresaSelecionada")).getCdpessoa();
			}
		}
		
		request.setAttribute("empresaidorigem", empresaidorigem != null ? empresaidorigem : "");
		request.setAttribute("empresaiddestino", empresaiddestino != null ? empresaiddestino : "");
	}
	
	/**
	 * M�todo para salvar as movimenta��es financeiras geradas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#saveGeraMovimentacao(GeraMovimentacao)
	 * @see #getSalvarModelAndView(WebRequestContext, GeraMovimentacao)
	 * @param request
	 * @param geraMovimentacao
	 * @return
	 * @throws Exception
	 */
	@Action("salvar")
	@Input("entrada")
	@Command(validate=true)
	public ModelAndView salvar(WebRequestContext request, GeraMovimentacao geraMovimentacao) throws Exception{
		movimentacaoService.saveGeraMovimentacao(geraMovimentacao);
		request.getSession().removeAttribute("contatipoGerarmovimentacaoCheque");
		request.getSession().removeAttribute("contaGerarmovimentacaoCheque");
		return getSalvarModelAndView(request, geraMovimentacao);	
	}
	
	/**
	 * M�todo para "direcionar" a tela ao salvar as movimenta��es.
	 *  
	 * @see #entrada(WebRequestContext, GeraMovimentacao)
	 * @param request
	 * @param acao
	 * @return
	 * @throws Exception
	 * @author Fl�vio Tavares
	 */
	private ModelAndView getSalvarModelAndView(WebRequestContext request, GeraMovimentacao geraMovimentacao) throws Exception{
		request.setAttribute("consultar", true);
		request.addMessage("Movimenta��o gerada com sucesso.");
		if(geraMovimentacao.getListaGeraMovimentacaocheque() != null && geraMovimentacao.getListaGeraMovimentacaocheque().size() > 0){
			List<Cheque> cheques = new ArrayList<Cheque>();
			for(GeraMovimentacaocheque geraMovCheque: geraMovimentacao.getListaGeraMovimentacaocheque()){
				Cheque cheque = chequeService.load(geraMovCheque.getCheque(), "cheque.cdcheque, cheque.numero");
				if(cheque != null)
					cheques.add(cheque);
			}
			String whereInCdCheques = CollectionsUtil.listAndConcatenate(cheques, "cdcheque", ",");
			String whereInNumeroCheques = CollectionsUtil.listAndConcatenate(cheques, "numero", ",");
			request.addMessage("Emitir relat�rio c�pia do(s) cheque(s): <a href=javascript:emiteRelatorioCopiaCheque('" + whereInCdCheques + "')><span>"+whereInNumeroCheques+"</span></a>.");	
		}
		
		if(Boolean.parseBoolean(request.getParameter(CLOSE_ON_CANCEL))){
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		}
		return this.entrada(request, geraMovimentacao);
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if(obj instanceof GeraMovimentacao){
			GeraMovimentacao geraMovimentacao = (GeraMovimentacao) obj;
			Movimentacao origem = geraMovimentacao.getMovimentacaoorigem();
			Movimentacao destino = geraMovimentacao.getMovimentacaodestino();
			origem.setTipooperacao(Tipooperacao.TIPO_DEBITO);
			destino.setTipooperacao(Tipooperacao.TIPO_CREDITO);
			
			if(fechamentofinanceiroService.verificaFechamento(origem) || fechamentofinanceiroService.verificaFechamento(destino)){
				errors.reject("001","A data da movimenta��o refere-se a um per�odo j� fechado.");
			}
			
			if(origem.getConta().equals(destino.getConta())){
				errors.reject("001","Os v�nculos de origem e destino n�o devem ser o mesmo.");
			}
			
			movimentacaoService.validateMovimentacao(errors, origem);
			
			if(!destino.getConta().getContatipo().equals(Contatipo.TIPO_CARTAO_CREDITO)){
				Conta contaDestino = contaService.load(destino.getConta());
				destino.setDtmovimentacao(origem.getDtmovimentacao());
				if(contaDestino.getDtsaldo() != null && destino.getDtmovimentacao().before(contaDestino.getDtsaldo())){
					String msg = "A data da movimenta��o n�o deve ser anterior � data do saldo inicial de " +
							contaDestino.getDescricao() + " (" + SinedDateUtils.toString(contaDestino.getDtsaldo()) + ").";
					errors.reject("001", msg);
					
				}
			}
			
			//VALIDA OS RATEIOS DA MOVIMENTA��O DE ORIGEM E DESTINO
			origem.setRateio(geraMovimentacao.getRateio());
			destino.setRateio(geraMovimentacao.getRateio2());
			try {
				rateioService.validateRateio(origem.getRateio(), origem.getValor());
				rateioService.validateRateio(destino.getRateio(), origem.getValor());
			} catch (SinedException e) {
				errors.reject("001", e.getMessage());
			}
			
			if(geraMovimentacao != null && geraMovimentacao.getMovimentacaoorigem() != null){
				if(geraMovimentacao.getMovimentacaoorigem().getNumerochequeorigem() != null){
					Cheque chequenovo = new Cheque();
					chequenovo.setBanco(geraMovimentacao.getMovimentacaoorigem().getBancochequeorigem());
					chequenovo.setAgencia(geraMovimentacao.getMovimentacaoorigem().getAgenciachequeorigem());
					chequenovo.setConta(geraMovimentacao.getMovimentacaoorigem().getContachequeorigem());
					chequenovo.setNumero(geraMovimentacao.getMovimentacaoorigem().getNumerochequeorigem());
					chequenovo.setEmitente(geraMovimentacao.getMovimentacaoorigem().getEmitentechequeorigem());
					chequenovo.setDtbompara(geraMovimentacao.getMovimentacaoorigem().getDtbomparachequeorigem());
					if(chequeService.verificarDuplicidadeCheque(chequenovo)){
						errors.reject("001", "Cheque j� lan�ado no sistema.");
					}else{
						geraMovimentacao.getMovimentacaoorigem().setCheque(chequenovo);
					}
				}else if( (geraMovimentacao.getMovimentacaoorigem().getBancochequeorigem() != null ||
						geraMovimentacao.getMovimentacaoorigem().getAgenciachequeorigem() != null ||
						geraMovimentacao.getMovimentacaoorigem().getContachequeorigem() != null ||
						(geraMovimentacao.getMovimentacaoorigem().getEmitentechequeorigem() != null &&
								!"".equals(geraMovimentacao.getMovimentacaoorigem().getEmitentechequeorigem())) ||
						geraMovimentacao.getMovimentacaoorigem().getDtbomparachequeorigem() != null) && 
						geraMovimentacao.getMovimentacaoorigem().getNumerochequeorigem() == null){
					errors.reject("001", "O campo n�mero do cheque � obrigat�rio");
				} 
			}
			
//			//VERIFICA SE OS CHEQUES EST�O RELACIONADO AO V�NCULO DE ORIGEM
//			if(geraMovimentacao.getListaGeraMovimentacaocheque() != null && !geraMovimentacao.getListaGeraMovimentacaocheque().isEmpty()){
//				String msg = null;
//				for(GeraMovimentacaocheque geraMovimentacaocheque : geraMovimentacao.getListaGeraMovimentacaocheque()){
//					if(geraMovimentacaocheque.getCheque() != null && geraMovimentacaocheque.getCheque().getCdcheque() != null){
//						List<Movimentacao> listaMovimentacao = movimentacaoService.findForGerarmovimentacao(geraMovimentacaocheque.getCheque());
//						if(listaMovimentacao != null && !listaMovimentacao.isEmpty()){
//							for(Movimentacao m : listaMovimentacao){
//								if(m.getCheque() != null && m.getConta() != null && m.getConta().getCdconta() != null && 
//										origem.getConta() != null && origem.getConta().getCdconta() != null &&
//										!origem.getConta().getCdconta().equals(m.getConta().getCdconta())){
//									//errors.reject("001", "O cheque " + m.getCheque().getNumero() + " n�o est� relacionado ao v�nculo de origem.");
//									msg = "O cheque " + m.getCheque().getNumero() + " n�o est� relacionado ao v�nculo de origem.";
//									break;
//								}
//							}
//						}
//					}
//					if (msg!=null){
//						errors.reject("001", msg);
//						break;
//					}
//				}
//			}
		}
	}
	
	/**
	 * M�todo para obter o valor limite que a movimenta��o poder� ter com base na conta.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#valorMaximoFormat(Conta)
	 * @param request
	 * @param conta
	 */
	@Action("getLimite")
	public void obterValorLimite(WebRequestContext request, Conta conta){
		String format = contaService.valorMaximoFormat(conta);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var valor = \""+format+"\";");
	}
	
	/**
	 * M�todo ajax que obtem o valor do cheque
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	@Action("getValorCheque")
	public ModelAndView obterValorCheque(WebRequestContext request){
		String cdchequestr = request.getParameter("cdcheque");
		Money valorcheque = new Money();
		if(cdchequestr != null && !"".equals(cdchequestr)){
			Cheque cheque = new Cheque();
			cheque.setCdcheque(Integer.parseInt(cdchequestr));
			cheque = chequeService.load(cheque, "cheque.cdcheque, cheque.valor");
			if(cheque != null && cheque.getValor() != null){
				valorcheque = cheque.getValor();
			}
		}		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("valorcheque", valorcheque);
		return json;
	}
	
	/**
	 * M�todo respons�vel por preecher a popup Gerar movimenta��es de acordo com os dados
	 * selecionados em arquivo concilia��o
	 * @param request
	 * @param conta
	 * @return
	 * @throws Exception
	 * @author Taidson
	 * @since 09/08/2010
	 */
	public ModelAndView gerarMovimentacao(WebRequestContext request, Conta conta) throws Exception{
		String movString = request.getParameter("selectedItens");
		String tipoop = request.getParameter("tipoop");
		Tipooperacao tipooperacao = null;
		
		String idsCheck = request.getParameter("idsCheck");
		
		if(tipoop != null){
			if(tipoop.equalsIgnoreCase("credito")){
				tipooperacao = Tipooperacao.TIPO_CREDITO;
			}else {
				tipooperacao = Tipooperacao.TIPO_DEBITO;
			}
		}
		conta = contaService.loadConta(conta);
		String[] list = movString.split("@@,");
		
		Movimentacao itemMovimentacao = null;
		
		List<Movimentacao> listaMovimentacoes = new ListSet<Movimentacao>(Movimentacao.class);
		int listsize = 0;
		
		if(list[list.length -1].equals(",")){
			listsize = 1;
		}
		
		for(int i = 0; i < list.length - listsize; i++){
			
			itemMovimentacao = new Movimentacao();
			itemMovimentacao.setConta(conta);
			itemMovimentacao.setTipooperacao(tipooperacao);
			
			String split = list[i];
			String[] lista = split.split("spl");
			
			itemMovimentacao.setDtmovimentacao(new Date (Long.valueOf(lista[0])));
			
			String valor = lista[1].replace(".", "");
			itemMovimentacao.setValor(new Money(valor.replace(",", ".")));
			
			if(i == list.length - 1){
				itemMovimentacao.setHistorico(lista[2].replace("@@", ""));
			}else{
				itemMovimentacao.setHistorico(lista[2]);
			}
			listaMovimentacoes.add(itemMovimentacao);	
		}
		
		Movimentacao movimentacao = new Movimentacao();

		Rateio rateio = new Rateio();
		Rateioitem rateioitem = new Rateioitem();
		rateioitem.setPercentual(null);
		List<Rateioitem> listaRateioitem = new ListSet<Rateioitem>(Rateioitem.class);
		listaRateioitem.add(rateioitem);
		rateio.setListaRateioitem(listaRateioitem);
		movimentacao.setRateio(rateio);
		movimentacao.setIdsCheck(idsCheck);
		
		ModelAndView modelAndView = new ModelAndView("direct:process/popup/gerarmovimentacaovarias","listaMovimentacoes",listaMovimentacoes);
		modelAndView.addObject("movimentacao", movimentacao);
		
		modelAndView.addObject("tipooperacao", tipooperacao);
		modelAndView.addObject("listaProjetos", projetoService.findProjetosAbertos(null));
		modelAndView.addObject("listaCentrocusto", centrocustoService.findAtivos());
		return modelAndView;
	}
	
	/**
	 * Salva as movimenta��es com os seus respectivos valores de rateio baseados no percentual de rateio. 
	 * @param request
	 * @param movimentacao
	 * @return
	 * @throws Exception
	 * @author Taidson
	 */
	public ModelAndView salvaMovimentacaoVarias(WebRequestContext request, Movimentacao movimentacao) throws Exception{
		
		for (Movimentacao item : movimentacao.getListaMovimentacoes()) {
			Rateio rateio = new Rateio();
			List<Rateioitem> listaRateioitem = new ListSet<Rateioitem>(Rateioitem.class);
			
			for (Rateioitem mri : movimentacao.getRateio().getListaRateioitem()) {
				Rateioitem rateioitem = new Rateioitem();
				rateioitem.setCentrocusto(mri.getCentrocusto());
				
				if(mri.getProjeto() != null && mri.getProjeto().getCdprojeto() != null){
					rateioitem.setProjeto(mri.getProjeto());
				}
				if(mri.getContagerencial() != null && mri.getContagerencial().getCdcontagerencial() != null){
					rateioitem.setContagerencial(mri.getContagerencial());
				}
				rateioitem.setPercentual(mri.getPercentual());
				
				Double valor = new Double(item.getValor().getValue().doubleValue());
				rateioitem.setValor(new Money((valor*mri.getPercentual())/100));
				
				listaRateioitem.add(rateioitem);
			}
			rateio.setListaRateioitem(listaRateioitem);
			
			item.setRateio(rateio);
			
			item.setMovimentacaoacao(Movimentacaoacao.NORMAL);
			
			movimentacaoService.saveOrUpdate(item);
			
		}
		return atualizaTela(request, movimentacao);
	}
	
	/**
	 * Associa os id's das movimenta��es geradas com suas respectivas refer�ncias em arquivo concilia��o.
	 * @param request
	 * @param bean
	 * @return
	 * @author Taidson
	 * @since 09/08/2010
	 */
	public ModelAndView atualizaTela(WebRequestContext request , Movimentacao bean){
		
		NeoWeb.getRequestContext().clearMessages();
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script src=\""+request.getServletRequest().getContextPath()+"/resource/js/util.js\"></script>" +
				"<script type='text/javascript' src='"+request.getServletRequest().getContextPath()+"/js/jquery.js'></script>" + 
				"<script type='text/javascript' src='"+request.getServletRequest().getContextPath()+"/js/utilconciliacao.js'></script>" 
		);
		
		String itens[] = StringUtils.split(bean.getIdsCheck(),",");
		String values = "";
		String virgula = ",";
		String tipo = "";
		
		for(int i=0; i<itens.length; i++){
			if(i == itens.length-1){
				virgula = "";
			}
			
			values += "'"+ itens[i] +"':" +bean.getListaMovimentacoes().get(i).getCdmovimentacao() +virgula;
			
			if(Tipooperacao.TIPO_CREDITO.equals(bean.getListaMovimentacoes().get(i).getTipooperacao())){
				tipo = "credito";
			}else{
				tipo = "debito";
			}
		}
		
		View.getCurrent().println("<script>var movimentacoes ={"+values+"};</script>");
		View.getCurrent().println("<script>var tipooperacao ='"+tipo+"';</script>");
		View.getCurrent().println("<script>window.opener.atualizaTela(movimentacoes, tipooperacao);</script>");
		View.getCurrent().println("<html><script type='text/javascript'>window.close();</script></html>");
	
		return null;
	}

	/**
	 * M�todo ajax que coloca a conta e/ou contatipo na sess�o para busca de cheque
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void colocarContatipoContaSessao(WebRequestContext request){		
		String contatipostr = request.getParameter("contatipo");
		String contastr = request.getParameter("conta");
		
		Contatipo contatipo = null;
		Conta conta = null;
		
		if(contatipostr != null && !"".equals(contatipostr) && !"<null>".equals(contatipostr)){
			contatipo = new Contatipo(Integer.parseInt(contatipostr));
		}
		if(contastr != null && !"".equals(contastr) && !"<null>".equals(contastr)){
			conta = new Conta(Integer.parseInt(contastr));
		}
		
		request.getSession().setAttribute("contatipoGerarmovimentacaoCheque", contatipo);
		request.getSession().setAttribute("contaGerarmovimentacaoCheque", conta);
	}
	
	/**
	 * M�todo ajax que obtem os dados dos cheques
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	@Action("getInfCheque")
	public ModelAndView obterInfCheque(WebRequestContext request){
		String idsCheques = request.getParameter("idsCheque");
		GeraMovimentacao geraMovimentacao = new GeraMovimentacao();
		geraMovimentacao.setMovimentacaodestino(null);
		geraMovimentacao.setMovimentacaoorigem(null);
		geraMovimentacao.setRateio(null);
		geraMovimentacao.setRateio2(null);
		List<GeraMovimentacaocheque> listaGeraMovimentacaocheque = new ArrayList<GeraMovimentacaocheque>();
		if(idsCheques != null && !"".equals(idsCheques)){
			List<Cheque> listacheque = chequeService.findCheques(idsCheques);
			if(listacheque != null && !listacheque.isEmpty()){
				GeraMovimentacaocheque geramovimentacaocheque;
				for(Cheque cheque : listacheque){
					geramovimentacaocheque = new GeraMovimentacaocheque();
					geramovimentacaocheque.setCheque(cheque);
					geramovimentacaocheque.setValor(cheque.getValor());
					listaGeraMovimentacaocheque.add(geramovimentacaocheque);
				}
				geraMovimentacao.setListaGeraMovimentacaocheque(listaGeraMovimentacaocheque);
			}
		}			
		return new JsonModelAndView().addObject("bean",geraMovimentacao);
	}
	
	public ModelAndView montaHistoricoConfiguravel(WebRequestContext request, GeraMovimentacao geraMovimentacao){
		Historicooperacao historicoOperacao = historicooperacaoService.findByTipo(Tipohistoricooperacao.TRANSFERENCIA_ENTRE_CONTAS);
		return historicooperacaoService.findHistoricoByTransferenciaInterna(request, geraMovimentacao, historicoOperacao);
	}
	
	public ModelAndView ajaxTemplatesForCopiaCheque(WebRequestContext request){
		return chequeService.ajaxTemplatesForCopiaCheque(request);
	}
	
	public ModelAndView abrirSelecaoTemplateCopiaCheque(WebRequestContext request){
		return chequeService.abrirSelecaoTemplateCopiaCheque(request);
	}
}