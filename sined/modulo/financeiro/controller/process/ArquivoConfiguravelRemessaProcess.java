package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.HashMap;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivobancariodocumento;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoremessaEnum;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRemessaInstrucaoService;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRemessaService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContacarteiraService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRemessaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.GerarArquivoRemessaBean;
import br.com.linkcom.sined.util.SinedException;

@Controller(path="/financeiro/process/ArquivoConfiguravelRemessa",authorizationModule=ProcessAuthorizationModule.class)
public class ArquivoConfiguravelRemessaProcess extends MultiActionController{
	
	private DocumentoService documentoService;
	private ContaService contaService;
	private BancoConfiguracaoRemessaInstrucaoService bancoConfiguracaoRemessaInstrucaoService;
	private BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService;
	private DocumentotipoService documentotipoService;
	private ContacarteiraService contacarteiraService;
	private ParametrogeralService parametrogeralService;

	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setBancoConfiguracaoRemessaInstrucaoService(BancoConfiguracaoRemessaInstrucaoService bancoConfiguracaoRemessaInstrucaoService) {this.bancoConfiguracaoRemessaInstrucaoService = bancoConfiguracaoRemessaInstrucaoService;}
	public void setBancoConfiguracaoRemessaService(BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService) {this.bancoConfiguracaoRemessaService = bancoConfiguracaoRemessaService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setContacarteiraService(ContacarteiraService contacarteiraService) {this.contacarteiraService = contacarteiraService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ArquivoConfiguravelRemessaBean filtro) throws Exception{
		List<Conta> listaConta = contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA);
		request.setAttribute("listaConta", listaConta);
		request.setAttribute("listaDocumentoTipo", documentotipoService.getListaDocumentoTipoUsuario());
		request.setAttribute("FATURA_LOCACAO", parametrogeralService.getBoolean(Parametrogeral.FATURA_LOCACAO));
		return new ModelAndView("/process/arquivoConfiguravelRemessa").addObject("filtro", filtro);
	}
	
	/**
	 * M�todo para carregar listagem do filtro no jsp via ajax.
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findColaboradorAltera
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @author Marden Silva
	 */
	public ModelAndView listar(WebRequestContext request, ArquivoConfiguravelRemessaBean filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		if (filtro.getInstrucaoCobranca() != null)
			filtro.setInstrucaoCobranca(bancoConfiguracaoRemessaInstrucaoService.load(filtro.getInstrucaoCobranca()));		
		List<Documento> listaDocumento = documentoService.findDadosForListagemRemessaConfiguravel(filtro);
		documentoService.preencherNumeroFaturalocacao(listaDocumento);
		
		Money valorTotal = new Money();
		
		for (Documento documento: listaDocumento){
			valorTotal = valorTotal.add(documento.getAux_documento().getValoratual());
			SituacaoremessaEnum situacaoremessa = null;
			if(documento.getListaArqBancarioDoc() == null || documento.getListaArqBancarioDoc().size() == 0){
				situacaoremessa = SituacaoremessaEnum.NAO_ENVIADO;
			}else{
				for(Arquivobancariodocumento arqBancDoc: documento.getListaArqBancarioDoc()){
					
					if(Arquivobancariosituacao.GERADO.equals(arqBancDoc.getArquivobancario().getArquivobancariosituacao()) ||
						Arquivobancariosituacao.PROCESSADO.equals(arqBancDoc.getArquivobancario().getArquivobancariosituacao()) ||
						Arquivobancariosituacao.PROCESSADO_PARCIAL.equals(arqBancDoc.getArquivobancario().getArquivobancariosituacao())){
						situacaoremessa = SituacaoremessaEnum.ENVIADO;
					}
				}
				if(situacaoremessa == null){
					situacaoremessa = SituacaoremessaEnum.CANCELADO;
				}
			}
			documento.setSituacaoremessa(situacaoremessa);
		}
		
		request.setAttribute("TotalRegistros", listaDocumento != null ? listaDocumento.size() : 0);
		request.setAttribute("valorTotal", valorTotal);
		request.setAttribute("FATURA_LOCACAO", parametrogeralService.getBoolean(Parametrogeral.FATURA_LOCACAO));
		request.setAttribute("filtro", filtro);
		return new ModelAndView("direct:ajax/arquivoconfiguravelremessalistagem", "listaDocumento", listaDocumento);
	}
	
	/**
	 * Verifica os itens selecionados na listagem e direciona para a gera��o do arquivo de remessa.
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@Action("gerar")
	public ModelAndView gerar(WebRequestContext request, ArquivoConfiguravelRemessaBean filtro) throws Exception {		
		String cds = request.getParameter("cds");
		filtro.setCds(cds);
		try {
			return bancoConfiguracaoRemessaService.gerarArquivoRemessa(filtro);
		} catch (SinedException e) {
			request.addMessage(e.getMessage(), MessageType.ERROR);
			request.setAttribute("erroGeracaoArquivo", true);
			return continueOnAction("index", filtro);
//			return new ModelAndView("/process/arquivoConfiguravelRemessa").addObject("filtro", filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addMessage("Erro desconhecido: " + e.getMessage(), MessageType.ERROR);
			request.setAttribute("erroGeracaoArquivo", true);
			return continueOnAction("index", filtro);
//			return new ModelAndView("/process/arquivoConfiguravelRemessa").addObject("filtro", filtro);
		}	
	}
	
	/**
	 * M�todo que valida se o filtro de empresa deve ser obrigat�rio de acordo com um bancoconfiguracaoremessa v�lido 
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @throws Exception 
	 * 
	 * @author Rafael Salvio
	 */
	public ModelAndView ajaxEmpresaObrigatoria(WebRequestContext request) throws Exception {
		Boolean empresaObrigatoria = Boolean.TRUE;
		String cdconta = request.getParameter("cdconta");
		if(cdconta != null && !cdconta.trim().isEmpty()){
			List<BancoConfiguracaoRemessa> lista =  bancoConfiguracaoRemessaService.findByContabancaria(new Conta(Integer.parseInt(cdconta)));
			if(lista != null && !lista.isEmpty() && Boolean.FALSE.equals(lista.get(0).getEmpresaobrigatoria())){
				empresaObrigatoria = Boolean.FALSE;
			}
		}
		
		return new JsonModelAndView().addObject("empresaObrigatoria", empresaObrigatoria);
	}

	public ModelAndView ajaxValidaLimiteNossoNumero(WebRequestContext request, GerarArquivoRemessaBean filtro) throws Exception {
		String cds=request.getParameter("cds");
		filtro.setContas(cds);
		List<Documento> listaDocumento = documentoService.findDadosArquivoRemessa(filtro);
		
		if (listaDocumento.size() < 1) {
			NeoWeb.getRequestContext().addMessage("N�o existem dados suficientes para gerar um arquivo de remessa.");
			throw new Exception();
		}
		Contacarteira contacarteira = filtro.getContacarteira();
		if(contacarteira.getLimiteposicoesintervalonossonumero() == null){
			contacarteira = contacarteiraService.load(filtro.getContacarteira());
		}


		HashMap<MessageType, String> alertasNossoNum = documentoService.validaLimiteSequencialNossoNum(listaDocumento, contacarteira);

		if(alertasNossoNum.containsKey(MessageType.WARN)){
			return new JsonModelAndView().addObject("alertanossonumero", alertasNossoNum.get(MessageType.WARN));
		}
		return new JsonModelAndView().addObject("alertanossonumero", "");
	}
}
