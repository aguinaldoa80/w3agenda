package br.com.linkcom.sined.modulo.financeiro.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.BoletodigitalService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BoletoDigitalRetornoBean;
import br.com.linkcom.sined.util.SinedException;

@Controller(path="/financeiro/process/BoletoDigitalRetorno",authorizationModule=ProcessAuthorizationModule.class)
public class BoletoDigitalRetornoProcess extends MultiActionController{
	
	private BoletodigitalService boletodigitalService;
	
	public void setBoletodigitalService(
			BoletodigitalService boletodigitalService) {
		this.boletodigitalService = boletodigitalService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, BoletoDigitalRetornoBean bean) throws Exception {
		return new ModelAndView("/process/boletoDigitalRetorno").addObject("filtro", bean);
	}
	
	public ModelAndView buscarInformacoes(WebRequestContext request, BoletoDigitalRetornoBean bean) throws Exception {
		try {	
			bean = boletodigitalService.processarRetorno(bean);
			if(bean.getListaDocumento() == null || bean.getListaDocumento().size() == 0){
				throw new SinedException("Nenhuma informação encontrada.");
			}
			return new ModelAndView("/process/boletoDigitalRetorno_conferencia").addObject("bean", bean);
		} catch (Exception e) {
			request.addError(e.getMessage());
			return continueOnAction("index", bean);
		}
	}
	
	public ModelAndView processarDocumentos(WebRequestContext request, BoletoDigitalRetornoBean bean) throws Exception {
		try {
			boletodigitalService.processarDocumentos(bean);
			return new ModelAndView("/process/boletoDigitalRetorno_resultado", "bean", bean);		
		} catch (Exception e) {
			request.addError(e.getMessage());
			return continueOnAction("index", bean);
		}
	}
	
}
