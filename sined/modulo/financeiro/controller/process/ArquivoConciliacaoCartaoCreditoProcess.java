package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Configuracaoconciliacaocartao;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.geral.service.ConfiguracaoconciliacaocartaoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.HistoricooperacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaohistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoorigemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.TaxaitemService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoCartaoCreditoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoCartaoCreditoItemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ConfiguracaoArquivoconciliacaocartaocreditoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoCartaoCreditoBean.TipoOperadoraCartaoCredito;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/financeiro/process/ArquivoConciliacaoCartaoCredito", authorizationModule=ProcessAuthorizationModule.class)
public class ArquivoConciliacaoCartaoCreditoProcess extends MultiActionController{
	
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private ContaService contaService;
	private MovimentacaoorigemService movimentacaoorigemService;
	private EmpresaService empresaService;
	private ParametrogeralService parametrogeralService;
	private ConfiguracaoconciliacaocartaoService configuracaoconciliacaocartaoService;
	private HistoricooperacaoService historicooperacaoService;
	private MovimentacaohistoricoService movimentacaohistoricoService;
	private TaxaitemService taxaitemService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setMovimentacaoorigemService(
			MovimentacaoorigemService movimentacaoorigemService) {
		this.movimentacaoorigemService = movimentacaoorigemService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setConfiguracaoconciliacaocartaoService(
			ConfiguracaoconciliacaocartaoService configuracaoconciliacaocartaoService) {
		this.configuracaoconciliacaocartaoService = configuracaoconciliacaocartaoService;
	}
	public void setHistoricooperacaoService(
			HistoricooperacaoService historicooperacaoService) {
		this.historicooperacaoService = historicooperacaoService;
	}
	public void setMovimentacaohistoricoService(
			MovimentacaohistoricoService movimentacaohistoricoService) {
		this.movimentacaohistoricoService = movimentacaohistoricoService;
	}
	public void setTaxaitemService(TaxaitemService taxaitemService) {
		this.taxaitemService = taxaitemService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ArquivoConciliacaoCartaoCreditoBean filtro) throws Exception{
		return new ModelAndView("/process/arquivoconciliacaocartaocredito", "filtro", filtro);
	}
	
	public ModelAndView listar(WebRequestContext request, ArquivoConciliacaoCartaoCreditoBean bean){
		List<ArquivoConciliacaoCartaoCreditoItemBean> listaEncontrada = new ArrayList<ArquivoConciliacaoCartaoCreditoItemBean>();
		List<ArquivoConciliacaoCartaoCreditoItemBean> listaNaoEncontrada = new ArrayList<ArquivoConciliacaoCartaoCreditoItemBean>();
		List<ArquivoConciliacaoCartaoCreditoItemBean> listaResolucao = new ArrayList<ArquivoConciliacaoCartaoCreditoItemBean>();
		
		if (bean != null && bean.getArquivo() != null) {
			if(!bean.getArquivo().getNome().endsWith(".csv") && !bean.getArquivo().getNome().endsWith(".CSV")){
				request.addError("O arquivo n�o est� no formato esperado.");
				return continueOnAction("index", bean);
			}
			Configuracaoconciliacaocartao config = configuracaoconciliacaocartaoService.loadByTipooperadora(bean.getTipo());
			String strFile = new String(bean.getArquivo().getContent());
			String[] linhas = strFile.split("\\r?\\n");
			
			List<ArquivoConciliacaoCartaoCreditoItemBean> lista = this.makeListaProcessamentoExtrato(linhas, bean.getTipo(), config);
			this.findMovimentacaoExtrato(bean, listaEncontrada, listaNaoEncontrada, listaResolucao, lista);
		}
		
		bean.setDtmovimentacao(getSugestaoDtmovimentacao(listaEncontrada));
		bean.setListaEncontrada(listaEncontrada);
		bean.setListaNaoEncontrada(listaNaoEncontrada);
		bean.setListaResolucao(listaResolucao);
		
		if (listaEncontrada.size() <= 0 && listaNaoEncontrada.size() <= 0) {
			request.addError("O arquivo n�o possui registros a serem processados.");
			return new ModelAndView("/process/arquivoconciliacaocartaocredito", "filtro", bean);
		} else if(listaResolucao != null && listaResolucao.size() > 0){
			return new ModelAndView("/process/arquivoconciliacaocartaocreditoResolucao", "bean", bean);
		} else {
			request.setAttribute("listaContabancaria", contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA, bean.getEmpresa()));
			return new ModelAndView("/process/arquivoconciliacaocartaocreditoListagem", "bean", bean);
		}
	}
	
	private Date getSugestaoDtmovimentacao(List<ArquivoConciliacaoCartaoCreditoItemBean> listaEncontrada) {
		Date dtmovimentacao = null;
		if(SinedUtil.isListNotEmpty(listaEncontrada)){
			for (ArquivoConciliacaoCartaoCreditoItemBean it : listaEncontrada) {
				if(dtmovimentacao == null) {
					dtmovimentacao = it.getDtefetivapagamento();
				} else if(it.getDtefetivapagamento() != null && !SinedDateUtils.equalsIgnoreHour(dtmovimentacao, it.getDtefetivapagamento())){
					dtmovimentacao = null;
					break;
				}
			}
		}
		return dtmovimentacao;
	}
	
	public ModelAndView processarPendencia(WebRequestContext request, ArquivoConciliacaoCartaoCreditoBean bean){
		for (ArquivoConciliacaoCartaoCreditoItemBean it : bean.getListaResolucao()) {
			Documento documento = it.getDocumento();
			if(documento != null){
				if(bean.getListaEncontrada() == null) 
					bean.setListaEncontrada(new ArrayList<ArquivoConciliacaoCartaoCreditoItemBean>());
				
				documento = documentoService.carregaDocumento(documento);
				
				this.verificaDocumentoConciliacao(it, documento);
				bean.getListaEncontrada().add(it);
			}
		}
		
		bean.setDtmovimentacao(getSugestaoDtmovimentacao(bean.getListaEncontrada()));
		request.setAttribute("listaContabancaria", contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA, bean.getEmpresa()));
		return new ModelAndView("/process/arquivoconciliacaocartaocreditoListagem", "bean", bean);
	}
	
	public ModelAndView processar(WebRequestContext request, ArquivoConciliacaoCartaoCreditoBean bean){
		List<ArquivoConciliacaoCartaoCreditoItemBean> listaNaoEncontrada = bean.getListaNaoEncontrada();
		List<ArquivoConciliacaoCartaoCreditoItemBean> listaEncontrada = bean.getListaEncontrada();
		
		if(existeMovimentacaoAssociadaNVezes(listaEncontrada, listaNaoEncontrada)){
			request.addError("A movimenta��o financeira do sistema n�o pode ser associada a mais de um item do extrato de cart�o.");
			bean.setListaEncontrada(bean.getListaEncontrada());
			bean.setListaNaoEncontrada(bean.getListaNaoEncontrada());
			return new ModelAndView("/process/arquivoconciliacaocartaocreditoListagem", "bean", bean);
		}
		if(!validaTipotaxaContareceber(request, listaEncontrada, listaNaoEncontrada)){
			bean.setListaEncontrada(bean.getListaEncontrada());
			bean.setListaNaoEncontrada(bean.getListaNaoEncontrada());
			return new ModelAndView("/process/arquivoconciliacaocartaocreditoListagem", "bean", bean);
		}
		if(bean.getConta() != null){
			Empresa empresa = empresaService.loadWithDadosTransferencia(bean.getEmpresa());
			if(empresa.getContagerencialcreditotransferencia() == null ||
					empresa.getContagerencialdebitotransferencia() == null ||
					empresa.getCentrocustotransferencia() == null){
				request.addError("Favor cadastrar os dados de transfer�ncia interna no cadastro de empresa.");
				return sendRedirectToAction("index");
			}
			
			List<Movimentacao> listaMovimentacaoAConciliar = new ArrayList<Movimentacao>();
			
			if(listaNaoEncontrada != null){
				for (ArquivoConciliacaoCartaoCreditoItemBean it : listaNaoEncontrada) {
					if(it.getMovimentacao() != null){
						listaMovimentacaoAConciliar.add(it.getMovimentacao());
					}
				}
			}
			if(listaEncontrada != null){
				for (ArquivoConciliacaoCartaoCreditoItemBean it : listaEncontrada) {
					if(it.getMarcado() != null && it.getMarcado() && it.getMovimentacao() != null){
						listaMovimentacaoAConciliar.add(it.getMovimentacao());
					}
				}
			}
			
			if(listaMovimentacaoAConciliar.size() > 0){
				Map<Conta, Money> mapContaValor = new HashMap<Conta, Money>();
				
				listaMovimentacaoAConciliar = movimentacaoService.findWithConta(CollectionsUtil.listAndConcatenate(listaMovimentacaoAConciliar, "cdmovimentacao", ","));
				
				for (Movimentacao movimentacao : listaMovimentacaoAConciliar) {
					if(!Movimentacaoacao.CONCILIADA.equals(movimentacao.getMovimentacaoacao()) && movimentacao.getValor() != null){
						Money taxa = movimentacaoService.getTotalTaxaByMovimentacao(movimentacao);
						Money valor = new Money();
						if(mapContaValor.containsKey(movimentacao.getConta())){
							valor = mapContaValor.get(movimentacao.getConta());
						}
						
						valor = valor.add(taxa != null ? movimentacao.getValor().subtract(taxa) : movimentacao.getValor());
						mapContaValor.put(movimentacao.getConta(), valor);
					}
				}
				
				List<Movimentacao> listaMovimentacaoDebitoGerada = new ArrayList<Movimentacao>();
				Date dtmovimentacao = bean.getDtmovimentacao() != null ? bean.getDtmovimentacao() : SinedDateUtils.currentDate();
				
				Set<Entry<Conta, Money>> entrySetContaValor = mapContaValor.entrySet();
				for (Entry<Conta, Money> entry : entrySetContaValor) {
					Conta conta = entry.getKey();
					Money valor = entry.getValue();
					
					Rateio rateio = new Rateio();
					List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
					listaRateioitem.add(new Rateioitem(empresa.getContagerencialdebitotransferencia(), empresa.getCentrocustotransferencia(), null, valor, 100d));
					rateio.setListaRateioitem(listaRateioitem);
					
					Movimentacao movimentacaoDebito = new Movimentacao();
					movimentacaoDebito.setTipooperacao(Tipooperacao.TIPO_DEBITO);
					movimentacaoDebito.setFormapagamento(Formapagamento.DEBITOCONTACORRENTE);
					movimentacaoDebito.setConta(conta);
					movimentacaoDebito.setValor(valor);
					movimentacaoDebito.setDtmovimentacao(dtmovimentacao);
					movimentacaoDebito.setHistorico("Concilia��o de pagamento do extrato do cart�o");
					movimentacaoDebito.setMovimentacaoacao(Movimentacaoacao.NORMAL);
					movimentacaoDebito.setRateio(rateio);
					movimentacaoService.saveOrUpdate(movimentacaoDebito);
					
					listaMovimentacaoDebitoGerada.add(movimentacaoDebito);
				}
				
				StringBuilder historicoCredito = new StringBuilder("Referente �(s) movimenta��o(�es) ");
				for (Movimentacao movimentacao : listaMovimentacaoDebitoGerada) {
					historicoCredito
						.append("<a href=\"javascript:visualizaMovimentacao(")
						.append(movimentacao.getCdmovimentacao())
						.append(")\">")
						.append(movimentacao.getCdmovimentacao())
						.append("</a>, ");
				}
				historicoCredito.delete(historicoCredito.length() - 2, historicoCredito.length());
			
				Rateio rateio = new Rateio();
				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
				listaRateioitem.add(new Rateioitem(empresa.getContagerencialcreditotransferencia(), empresa.getCentrocustotransferencia(), null, bean.getTotal(), 100d));
				rateio.setListaRateioitem(listaRateioitem);
				
				Movimentacao movimentacaoCredito = new Movimentacao();
				movimentacaoCredito.setTipooperacao(Tipooperacao.TIPO_CREDITO);
				movimentacaoCredito.setFormapagamento(Formapagamento.CREDITOCONTACORRENTE);
				movimentacaoCredito.setConta(bean.getConta());
				movimentacaoCredito.setValor(bean.getTotal());
				movimentacaoCredito.setDtmovimentacao(dtmovimentacao);
				movimentacaoCredito.setHistorico("Concilia��o de pagamento do extrato do cart�o");
				movimentacaoCredito.setMovimentacaoacao(Movimentacaoacao.NORMAL);
				movimentacaoCredito.setObservacao(historicoCredito.toString());
				movimentacaoCredito.setRateio(rateio);
				movimentacaoService.saveOrUpdate(movimentacaoCredito);
				
				StringBuilder historicoDebito = new StringBuilder("Referente � movimenta��o ");
				historicoDebito
					.append("<a href=\"javascript:visualizaMovimentacao(")
					.append(movimentacaoCredito.getCdmovimentacao())
					.append(")\">")
					.append(movimentacaoCredito.getCdmovimentacao())
					.append("</a>");
				
				for (Movimentacao movimentacaoDebito: listaMovimentacaoDebitoGerada) {
					movimentacaoDebito.setDtbanco(movimentacaoDebito.getDtmovimentacao());
					movimentacaoDebito.setObservacao(historicoDebito.toString());
					movimentacaoService.salvaMovimentacaoConciliacao(movimentacaoDebito, null);
					
					Movimentacaoorigem movimentacaoorigemDebito = movimentacaoorigemService.createMovimentacaoorigem(movimentacaoDebito);
					movimentacaoorigemDebito.setConta(movimentacaoCredito.getConta());
					movimentacaoorigemDebito.setMovimentacaorelacionada(movimentacaoCredito);
					movimentacaoorigemService.saveOrUpdateNoUseTransaction(movimentacaoorigemDebito);

					Movimentacaoorigem movimentacaoorigemCredito = movimentacaoorigemService.createMovimentacaoorigem(movimentacaoCredito);
					movimentacaoorigemCredito.setConta(movimentacaoDebito.getConta());
					movimentacaoorigemCredito.setMovimentacaorelacionada(movimentacaoDebito);
					movimentacaoorigemService.saveOrUpdateNoUseTransaction(movimentacaoorigemCredito);
				}
				
				for (Movimentacao movimentacao: listaMovimentacaoAConciliar){
					Movimentacaoorigem movimentacaoorigem = new Movimentacaoorigem();
					movimentacaoorigem.setMovimentacao(movimentacaoCredito);
					movimentacaoorigem.setConta(movimentacao.getConta());
					movimentacaoorigem.setMovimentacaorelacionadacartaocredito(movimentacao);
					movimentacaoorigemService.saveOrUpdateNoUseTransaction(movimentacaoorigem);
				}
			}
		}
		
		int count = 0;
		
		if(listaNaoEncontrada != null){
			for (ArquivoConciliacaoCartaoCreditoItemBean it : listaNaoEncontrada) {
				if(it.getMovimentacao() != null && movimentacaoService.existMovimentacaoSituacao(it.getMovimentacao().getCdmovimentacao().toString(), true, Movimentacaoacao.CONCILIADA)){
					Movimentacao movimentacao = it.getMovimentacao();
					Movimentacao mov = movimentacaoService.loadForCriaMovimentacaoTaxaProcessamentoextratocartao(movimentacao);
					Money valor = mov.getValor();
					
					String checknum = it.getTid();
					String fitid = it.getTid();
					String checknumDocumentos = it.getTid();
					String fitidDocumentos = it.getTid();
					
					List<String> docs = new ArrayList<String>();
					List<Documento> listaDocumento = documentoService.findDocumentoByMovimentacao(movimentacao);
					if(SinedUtil.isListNotEmpty(listaDocumento)){
						for(Documento doc: listaDocumento){
							if(StringUtils.isNotBlank(doc.getNumero()) && !docs.contains(doc.getNumero())){
								docs.add(doc.getNumero());	
							}
						}
						checknumDocumentos = CollectionsUtil.concatenate(docs, " | ");
						fitidDocumentos = checknumDocumentos;
						if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MOVIMENTACAO)){
							mov.setChecknum(checknumDocumentos);
							mov.setFitid(fitidDocumentos);
						}else{
							mov.setChecknum(checknum);
							mov.setFitid(fitid);	
						}
						for(Documento doc: listaDocumento){
							if(doc.getDocumentotipo() != null && Boolean.TRUE.equals(doc.getDocumentotipo().getTaxanoprocessamentoextratocartaocredito())){
								this.criaMovimentacaotaxaByTipotaxacontareceber(it, mov, doc, valor);
							}
						}
					}
					
					if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MOVIMENTACAO)){
						movimentacao.setChecknum(checknumDocumentos);
						movimentacao.setFitid(fitidDocumentos);
					}else{
						movimentacao.setChecknum(checknum);
						movimentacao.setFitid(fitid);	
					}
					
					movimentacao.setDtbanco(it.getDtefetivapagamento());
					String historicoconciliacaobanco = parametrogeralService.getValorPorNome(Parametrogeral.HISTORICOCONCILIACAOBANCO );
					if(historicoconciliacaobanco.toUpperCase().equals("TRUE")){
						movimentacao.setHistorico(it.getDescricao());
					}
					movimentacao.setObservacao("Conciliada via extrato (" + (StringUtils.isNotBlank(it.getTid()) ? "TID: " + it.getTid() : "") + (StringUtils.isNotBlank(it.getDoc()) ? " DOC: " + it.getDoc() : "") + ")");
					movimentacaoService.salvaMovimentacaoConciliacao(movimentacao, null);
					
					count++;
				}
			}
		}
		
		if(listaEncontrada != null){
			for (ArquivoConciliacaoCartaoCreditoItemBean it : listaEncontrada) {
				if(it.getMarcado() != null && it.getMarcado() && it.getMovimentacao() != null && movimentacaoService.existMovimentacaoSituacao(it.getMovimentacao().getCdmovimentacao().toString(), true, Movimentacaoacao.CONCILIADA)){
					Movimentacao movimentacao = it.getMovimentacao();
					Movimentacao mov = movimentacaoService.loadForCriaMovimentacaoTaxaProcessamentoextratocartao(movimentacao);
					Money valor = mov.getValor();
					
					String checknum = it.getTid();
					String fitid = it.getTid();
					String checknumDocumentos = it.getTid();
					String fitidDocumentos = it.getTid();
					
					List<String> docs = new ArrayList<String>();
					List<Documento> listaDocumento = documentoService.findDocumentoByMovimentacao(movimentacao);
					if(SinedUtil.isListNotEmpty(listaDocumento)){
						for(Documento doc: listaDocumento){
							if(StringUtils.isNotBlank(doc.getNumero()) && !docs.contains(doc.getNumero())){
								docs.add(doc.getNumero());	
							}
						}
						checknumDocumentos = CollectionsUtil.concatenate(docs, " | ");
						fitidDocumentos = checknumDocumentos;
						if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MOVIMENTACAO)){
							mov.setChecknum(checknumDocumentos);
							mov.setFitid(fitidDocumentos);
						}else{
							mov.setChecknum(checknum);
							mov.setFitid(fitid);	
						}
						for(Documento doc: listaDocumento){
							if(doc.getDocumentotipo() != null && Boolean.TRUE.equals(doc.getDocumentotipo().getTaxanoprocessamentoextratocartaocredito())){
								this.criaMovimentacaotaxaByTipotaxacontareceber(it, mov, doc, valor);
							}
						}
					}

					if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MOVIMENTACAO)){
						movimentacao.setChecknum(checknumDocumentos);
						movimentacao.setFitid(fitidDocumentos);
					}else{
						movimentacao.setChecknum(checknum);
						movimentacao.setFitid(fitid);
					}
					
					movimentacao.setDtbanco(it.getDtefetivapagamento());
					String historicoconciliacaobanco = parametrogeralService.getValorPorNome(Parametrogeral.HISTORICOCONCILIACAOBANCO );
					if(historicoconciliacaobanco.toUpperCase().equals("TRUE")){
						movimentacao.setHistorico(it.getDescricao());
					}
					movimentacao.setObservacao("Conciliada via extrato " + (StringUtils.isNotBlank(it.getTid()) ? ("(TID: " + it.getTid() + ")") : "") + (StringUtils.isNotBlank(it.getDoc()) ? " DOC: " + it.getDoc() : ""));
					movimentacaoService.salvaMovimentacaoConciliacao(movimentacao, null);
					
					count++;
				}
			}
		}
		
		if(count > 0){
			request.addMessage(count + " movimenta��o(�es) conciliada(s) com sucesso.");
		} else {
			request.addError("Nenhuma movimenta��o conciliada.");
		}
		return sendRedirectToAction("index");
	}
	
	private boolean existeMovimentacaoAssociadaNVezes(List<ArquivoConciliacaoCartaoCreditoItemBean> listaEncontrada, List<ArquivoConciliacaoCartaoCreditoItemBean> listaNaoEncontrada){
		if(listaEncontrada == null || listaNaoEncontrada == null){
			return false;
		}

		for (ArquivoConciliacaoCartaoCreditoItemBean it : listaEncontrada) {
			if(it.getMarcado() != null && it.getMarcado() && it.getMovimentacao() != null){
				for (ArquivoConciliacaoCartaoCreditoItemBean it2 : listaNaoEncontrada) {
					if(it.getMovimentacao().equals(it2.getMovimentacao())){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private void findMovimentacaoExtrato(ArquivoConciliacaoCartaoCreditoBean bean, List<ArquivoConciliacaoCartaoCreditoItemBean> listaEncontrada,
			List<ArquivoConciliacaoCartaoCreditoItemBean> listaNaoEncontrada, List<ArquivoConciliacaoCartaoCreditoItemBean> listaResolucao,
			List<ArquivoConciliacaoCartaoCreditoItemBean> lista) {
		
		Pattern pattern_venda = Pattern.compile("Venda ([0-9]+)");
		for (ArquivoConciliacaoCartaoCreditoItemBean it : lista) {
			Movimentacao movimentacao = null;
			
			try {
				String tid = StringUtils.isNotBlank(it.getTid()) ? it.getTid() : null;
				String doc = StringUtils.isNotBlank(it.getDoc()) ? it.getDoc() : null;
				
				if(movimentacao == null && tid != null){
					Documento documento = documentoService.loadContareceberByNumeroEmpresa(bean.getEmpresa(), tid, null);
					movimentacao = this.verificaDocumentoConciliacao(it, documento);
				}
				if(movimentacao == null && doc != null){
					Documento documento = documentoService.loadContareceberByNumeroEmpresa(bean.getEmpresa(), null, doc);
					movimentacao = this.verificaDocumentoConciliacao(it, documento);
				}
				
				if(movimentacao == null){
					Integer cdvenda = getCdvendaByTID(it.getTid(), pattern_venda);
					List<Documento> listaDocumento = null;
					if(cdvenda != null){
						listaDocumento = documentoService.findBaixadasByEmpresaDtemissaoValorCdvenda(bean.getEmpresa(), it.getDtvenda(), it.getValorArquivo(), it.getValorArquivoTaxa(), it.getTid(), cdvenda);
					}else {
						listaDocumento = documentoService.findBaixadasByEmpresaDtemissaoDtvencimentoValor(
											bean.getEmpresa(), 
											it.getDtvenda(), 
											it.getDtefetivapagamento(), 
											it.getValorArquivo(),
											it.getValorArquivoTaxa(), 
											tid, 
											doc
										);
					}
					if(listaDocumento != null && listaDocumento.size() > 0){
						if(listaDocumento.size() == 1){
							movimentacao = this.verificaDocumentoConciliacao(it, listaDocumento.get(0));
						} else {
							it.setListaDocumento(listaDocumento);
							listaResolucao.add(it);
							continue;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(movimentacao != null){
				if (!movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.CONCILIADA)) {
					listaEncontrada.add(it);
				}
			} else {
				listaNaoEncontrada.add(it);
			}
		}
	}
	
	/**
	* M�todo que retorna o cdvenda que est� no TID
	*
	* @param tid
	* @param pattern_venda
	* @return
	* @since 29/08/2016
	* @author Luiz Fernando
	*/
	private Integer getCdvendaByTID(String tid, Pattern pattern_venda) {
		try {
			Matcher m_venda = pattern_venda.matcher(tid);
			while(m_venda.find()){
				return Integer.parseInt(m_venda.group(1));
			}
		} catch (Exception e) {}
		
		return null;
	}
	
	private Movimentacao verificaDocumentoConciliacao(ArquivoConciliacaoCartaoCreditoItemBean it, Documento documento) {
		Movimentacao movimentacao = null;
		
		if(documento != null && documento.getDocumentoacao() != null && documento.getDocumentoacao().equals(Documentoacao.BAIXADA)){
			movimentacao = movimentacaoService.findByDocumento(documento);
			if(movimentacao != null){
				Money valorTaxa = movimentacao.getValorTaxa();
				it.setDocumento(documento);
				if(documento.getPessoa() != null){
					it.setPessoa(documento.getPessoa().getNome());
				} else if(documento.getOutrospagamento() != null && !documento.getOutrospagamento().equals("")){
					it.setPessoa(documento.getOutrospagamento());
				}

				it.setMovimentacao(movimentacao);
				it.setValor(movimentacao.getValor());
				boolean geraTaxaDeMovimentoNoProcessamentoDoextratoDeCartao = Boolean.TRUE.equals(documento.getDocumentotipo().getTaxanoprocessamentoextratocartaocredito());
				List<Taxaitem> listaTaxaitemJaExistente = taxaitemService.findTaxaitemByDocumento(documento, Tipotaxa.TAXAMOVIMENTO, Tipotaxa.TAXABAIXA_DESCONTO);

				if(valorTaxa.getValue().doubleValue() == 0){//Se n�o tem movimenta��o de taxa
					if(geraTaxaDeMovimentoNoProcessamentoDoextratoDeCartao &&//Se a flag taxanoprocessamentoextratocartaocredito est� marcada, quer dizer que vai gerar a movimenta��o de taxa no processamento do arquivo de cart�o
						SinedUtil.isListEmpty(listaTaxaitemJaExistente)){
						valorTaxa = this.calcularTaxa(documento.getDocumentotipo(), documento.getValor(), movimentacao.getValor()); 
						
						if(it.getValorArquivoLiquido() == null){
							it.setValorArquivoLiquido(movimentacao.getValor().subtract(valorTaxa));
						}
					}else{
						if(it.getValorArquivoLiquido() == null){
							it.setValorArquivoLiquido(movimentacao.getValor());
						}
					}
				}else{
					if(it.getValorArquivoLiquido() == null){
						it.setValorArquivoLiquido(movimentacao.getValor().subtract(valorTaxa));
					}						
				}
				
				it.setValorArquivoTaxa(valorTaxa);
				it.setPermitidoConciliacao(movimentacao.getMovimentacaoacao() != null && movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.NORMAL));
			}
		}
		
		return movimentacao;
	}

	private List<ArquivoConciliacaoCartaoCreditoItemBean> makeListaProcessamentoExtrato(String[] linhas, TipoOperadoraCartaoCredito tipo, Configuracaoconciliacaocartao configuracao) {
		List<ArquivoConciliacaoCartaoCreditoItemBean> lista = new ArrayList<ArquivoConciliacaoCartaoCreditoItemBean>();
		
		int idx_dtvenda = configuracao.getColunadtvenda() != null? configuracao.getColunadtvenda(): Integer.MAX_VALUE;
		int idx_dtefetivapagamento = configuracao.getColunadtefetivacao() != null? configuracao.getColunadtefetivacao(): Integer.MAX_VALUE;
		int idx_descricao = configuracao.getColunadescricao() != null? configuracao.getColunadescricao(): Integer.MAX_VALUE;
		int idx_tid = configuracao.getColunatid() != null? configuracao.getColunatid(): Integer.MAX_VALUE;
		int idx_valor = configuracao.getColunavalor() != null? configuracao.getColunavalor(): Integer.MAX_VALUE;
		int idx_valorliquido = configuracao.getColunavalorliquido() != null? configuracao.getColunavalorliquido(): Integer.MAX_VALUE;
		int idx_doc = configuracao.getColunadoc() != null? configuracao.getColunadoc(): Integer.MAX_VALUE;
		
		for (int i = 1; i < linhas.length; i++) {
			if(linhas[i] != null && !linhas[i].equals("")){
				String[] campos = linhas[i].split(";");
				
				ArquivoConciliacaoCartaoCreditoItemBean it = new ArquivoConciliacaoCartaoCreditoItemBean();
				if(campos.length > idx_dtvenda) {
					String data = campos[idx_dtvenda].replaceAll("\"", "");
					if(data != null && !data.trim().equals("")){
						try {
							it.setDtvenda(SinedDateUtils.stringToDate(data));
						} catch (ParseException e) {}
					}
				}
				if(campos.length > idx_dtefetivapagamento) {
					String data = campos[idx_dtefetivapagamento].replaceAll("\"", "");
					if(data != null && !data.trim().equals("")){
						try {
							it.setDtefetivapagamento(SinedDateUtils.stringToDate(data));
						} catch (ParseException e) {}
					}
				}
				if(campos.length > idx_descricao) {
					String descricao = campos[idx_descricao].replaceAll("\"", "").trim();
					it.setDescricao(descricao);
				}
				if(campos.length > idx_tid) {
					String tid = campos[idx_tid].replaceAll("\"", "").trim();
					if(tid.endsWith("*")){
						tid = tid.substring(0, tid.length() - 1);
					}
					it.setTid(tid);
				}
				if(campos.length > idx_doc) {
					String doc = campos[idx_doc].replaceAll("\"", "").trim();
					it.setDoc(doc);
				}
				try{
					if(campos.length > idx_valorliquido) {
						String valor = campos[idx_valorliquido].replaceAll("\"", "").replace("R$", "").trim();
						valor = valor.replace(".", "");
						valor = valor.replace(',', '.');
						it.setValorArquivoLiquido(new Money(valor));
					}
				} catch (Exception e) {}
				try{
					if(campos.length > idx_valor) {
						String valor = campos[idx_valor].replaceAll("\"", "").replace("R$", "").trim();
						valor = valor.replace(".", "");
						valor = valor.replace(',', '.');
						it.setValorArquivo(new Money(valor));
					}
				} catch (Exception e) {}
				
				if(it.getDtvenda() != null && it.getDtefetivapagamento() != null && it.getValorArquivo() != null){
					lista.add(it);
				}
			}
		}
		return lista;
	}
	
	/**
	 * Action que abre popup para associa��o de conta
	 *
	 * @param request
	 * @param it
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/08/2014
	 */
	public ModelAndView abrirPopUpAssociacao(WebRequestContext request, ArquivoConciliacaoCartaoCreditoItemBean it){
		if(StringUtils.isNotBlank(it.getDescricao())) it.setDescricao(SinedUtil.getTextoUTF8(it.getDescricao()));
		if(StringUtils.isNotBlank(it.getTid())) it.setTid(SinedUtil.getTextoUTF8(it.getTid()));
		
		Date dtemissao1 = SinedDateUtils.addDiasData(it.getDtvenda(), -30);
		Date dtemissao2 = SinedDateUtils.addDiasData(it.getDtvenda(), 30);
		List<Documento> lista = documentoService.findBaixadasByEmpresaDtemissaoPeriodo(it.getEmpresa(), dtemissao1, dtemissao2);
		List<Documento> listaNova = new ArrayList<Documento>();
		String valorStr;
		if(SinedUtil.isListNotEmpty(lista) && ((it.getValorArquivo() != null && it.getValorArquivo().getValue().doubleValue() > 0) ||
				(it.getValorArquivoLiquido() != null && it.getValorArquivoLiquido().getValue().doubleValue() > 0))){
			for(Documento documento : lista){
				Money valorTotalTaxaMovimentacao = new Money();
				if(SinedUtil.isListNotEmpty(documento.getListaMovimentacaoOrigem())){
					for(Movimentacaoorigem movimentacaoorigem : documento.getListaMovimentacaoOrigem()){
						if(movimentacaoorigem.getMovimentacao() != null){
							valorTotalTaxaMovimentacao = valorTotalTaxaMovimentacao.add(movimentacaoService.getTotalTaxaByMovimentacao(movimentacaoorigem.getMovimentacao()));
						}
					}
				}
				
				if(valorTotalTaxaMovimentacao.getValue().doubleValue() == 0){
					boolean geraTaxaDeMovimentoNoProcessamentoDoextratoDeCartao = Boolean.TRUE.equals(documento.getDocumentotipo().getTaxanoprocessamentoextratocartaocredito());
					List<Taxaitem> listaTaxaitemJaExistente = taxaitemService.findTaxaitemByDocumento(documento, Tipotaxa.TAXAMOVIMENTO, Tipotaxa.TAXABAIXA_DESCONTO);

					if(geraTaxaDeMovimentoNoProcessamentoDoextratoDeCartao &&//Se a flag taxanoprocessamentoextratocartaocredito est� marcada, quer dizer que vai gerar a movimenta��o de taxa no processamento do arquivo de cart�o
						SinedUtil.isListEmpty(listaTaxaitemJaExistente)){
						valorTotalTaxaMovimentacao = calcularTaxa(documento.getDocumentotipo(), documento.getValor(), documento.getValorTotalMovimentacoesComTaxa());
					}
				}
				documento.setValorTotalTaxaMovimentacao(valorTotalTaxaMovimentacao);
				valorStr = documento.getValorTotalMovimentacoesComTaxa().toString();
				if((it.getValorArquivoLiquido() != null && valorStr.equals(it.getValorArquivoLiquido().toString())) ||
						(documento.getAux_documento() != null && documento.getAux_documento().getValoratual() != null && it.getValorArquivo() != null && documento.getAux_documento().getValoratual().toString().equals(it.getValorArquivo().toString())) || 
						(documento.getValor() != null && it.getValorArquivo() != null && documento.getValor().toString().equals(it.getValorArquivo().toString()))){
					listaNova.add(documento);
				}
			}
		}
		request.setAttribute("lista", listaNova);
		request.setAttribute("indexTop", request.getParameter("index"));
		
		return new ModelAndView("direct:/process/popup/arquivoconciliacaocartaocreditoAssociacao", "it", it);
	}
	
	/**
	 * Ajax para buscar o id da movimenta��o
	 *
	 * @param request
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/08/2014
	 */
	public ModelAndView ajaxBuscarIdMovimentacao(WebRequestContext request, Documento documento){
		Movimentacao movimentacao = movimentacaoService.findByDocumento(documento);
		Money valorTaxa = movimentacao != null ? movimentacao.getValorTaxa() : null;
		if(valorTaxa == null || valorTaxa.getValue().doubleValue() == 0){
			Documento doc = documentoService.carregaDocumento(documento);
			if(doc != null && doc.getDocumentotipo() != null && Boolean.TRUE.equals(doc.getDocumentotipo().getTaxanoprocessamentoextratocartaocredito())){
				List<Taxaitem> listaTaxaitemJaExistente = taxaitemService.findTaxaitemByDocumento(doc, Tipotaxa.TAXAMOVIMENTO, Tipotaxa.TAXABAIXA_DESCONTO);
				if(SinedUtil.isListEmpty(listaTaxaitemJaExistente)){
					valorTaxa = this.calcularTaxa(doc.getDocumentotipo(), doc.getValor(), movimentacao.getValor());
				}
			}
		}
		return new JsonModelAndView()
					.addObject("cdmovimentacao", movimentacao != null ? movimentacao.getCdmovimentacao() : null)
					.addObject("valor", movimentacao != null ? movimentacao.getValor() : null)
					.addObject("valorTaxa", valorTaxa)
					.addObject("valorLiquido", movimentacao != null && movimentacao.getValor() != null ? movimentacao.getValor().subtract(valorTaxa != null ? valorTaxa : new Money()) : null);
	}

	public ModelAndView abrePopupConfiguracaoconciliacao(WebRequestContext request){
		ConfiguracaoArquivoconciliacaocartaocreditoBean bean = new ConfiguracaoArquivoconciliacaocartaocreditoBean();
		bean.setArquivoCielo(configuracaoconciliacaocartaoService.loadByTipooperadora(TipoOperadoraCartaoCredito.CIELO));
		bean.setArquivoRede(configuracaoconciliacaocartaoService.loadByTipooperadora(TipoOperadoraCartaoCredito.REDE));
		if(bean.getArquivoCielo() == null){
			bean.setArquivoCielo(new Configuracaoconciliacaocartao());
			bean.getArquivoCielo().setTipooperadora(TipoOperadoraCartaoCredito.CIELO);
		}
		if(bean.getArquivoRede() == null){
			bean.setArquivoRede(new Configuracaoconciliacaocartao());
			bean.getArquivoRede().setTipooperadora(TipoOperadoraCartaoCredito.REDE);
		}
		return new ModelAndView("direct:/process/popup/configuracaoArquivoconciliacaocartaocredito", "bean", bean);
	}
	
	@Action("saveConfiguracaoconciliacao")
	public ModelAndView saveConfiguracaoconciliacao(WebRequestContext request, ConfiguracaoArquivoconciliacaocartaocreditoBean bean){
		configuracaoconciliacaocartaoService.saveOrUpdate(bean.getArquivoCielo());
		configuracaoconciliacaocartaoService.saveOrUpdate(bean.getArquivoRede());
		request.addMessage("Configura��o de layout gravada com sucesso.");
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	private boolean validaTipotaxaContareceber(WebRequestContext request, List<ArquivoConciliacaoCartaoCreditoItemBean> listaEncontrada, List<ArquivoConciliacaoCartaoCreditoItemBean> listaNaoEncontrada){
		boolean valido = true;
		StringBuilder strMovimentacoes = new StringBuilder();
		if(listaNaoEncontrada != null){
			for (ArquivoConciliacaoCartaoCreditoItemBean it : listaNaoEncontrada) {
				if(it.getMovimentacao() != null){
					strMovimentacoes.append(it.getMovimentacao().getCdmovimentacao()+" ");
				}
			}
		}
		if(listaEncontrada != null){
			for (ArquivoConciliacaoCartaoCreditoItemBean it : listaEncontrada) {
				if(Boolean.TRUE.equals(it.getMarcado())){
					strMovimentacoes.append(it.getMovimentacao().getCdmovimentacao()+" ");
				}
			}
		}
		String whereInMovimentacoes = strMovimentacoes.toString().trim().replace(" ", ",");
		List<Documento> listaDocumentos = documentoService.findForValiationProcessamentoExtratocartao(whereInMovimentacoes);
		List<Tipotaxa> listaSemContagerencial = new ArrayList<Tipotaxa>();
		List<Tipotaxa> listaSemCentrocusto = new ArrayList<Tipotaxa>();
		for(Documento doc: listaDocumentos){
			Tipotaxa tipotaxa = doc.getDocumentotipo().getTipotaxacontareceber();
			if(tipotaxa != null){
				if(this.getContagerencialForTaxaCartaocredito(doc) == null &&
						!listaSemContagerencial.contains(tipotaxa)){
					listaSemContagerencial.add(tipotaxa);
				}
				if(this.getCentrocustoForTaxaCartaocredito(doc) == null &&
						!listaSemCentrocusto.contains(tipotaxa)){
					listaSemCentrocusto.add(tipotaxa);
				}
			}
		}
		
		if(!listaSemContagerencial.isEmpty() || !listaSemCentrocusto.isEmpty()){
			valido = false;
			for(Tipotaxa tipotaxa: listaSemContagerencial){
				request.addError("Favor cadastrar a Conta Gerencial de D�bito no cadastro de Tipo de Taxa "+tipotaxa.getNome()+".");
			}
			for(Tipotaxa tipotaxa: listaSemCentrocusto){
				request.addError("Favor cadastrar o Centro de Custo no cadastro de Tipo de Taxa "+tipotaxa.getNome()+".");
			}
		}
		return valido;
	}
	
	private void criaMovimentacaotaxaByTipotaxacontareceber(ArquivoConciliacaoCartaoCreditoItemBean bean, Movimentacao movimentacao, Documento doc, Money valorMovimentacao){
		if(movimentacaoService.existeMovimentacaotaxaGeradaNoProcessamentoExtratocartao(movimentacao)){
			return;
		}
		List<Taxaitem> listaTaxaitemJaExistente = taxaitemService.findTaxaitemByDocumento(doc, Tipotaxa.TAXAMOVIMENTO, Tipotaxa.TAXABAIXA_DESCONTO);
		if(!SinedUtil.isListEmpty(listaTaxaitemJaExistente)){
			return;
		}
		String tidDoc = "(" + (StringUtils.isNotBlank(bean.getTid()) ? "TID: " + bean.getTid() : "") + (StringUtils.isNotBlank(bean.getDoc()) ? " DOC: " + bean.getDoc() : "") + ")";
		Contagerencial contagerencial = this.getContagerencialForTaxaCartaocredito(doc);
		Centrocusto centrocusto = this.getCentrocustoForTaxaCartaocredito(doc);
		
		Movimentacao movimentacaoTaxa = new Movimentacao();
		movimentacaoTaxa.setTipooperacao(Tipooperacao.TIPO_DEBITO);
		movimentacaoTaxa.setFormapagamento(Formapagamento.DEBITOCONTACORRENTE);
		movimentacaoTaxa.setConta(movimentacao.getConta());
		movimentacaoTaxa.setEmpresa(movimentacao.getEmpresa());
		calcularTaxa(doc.getDocumentotipo(), doc.getValor(), valorMovimentacao).getValue().doubleValue();
		Money valorTaxa = calcularTaxa(doc.getDocumentotipo(), doc.getValor(), valorMovimentacao);
		/*Double valorTaxa = Boolean.TRUE.equals(doc.getDocumentotipo().getPercentual())?
				valorMovimentacao.getValue().doubleValue() * doc.getDocumentotipo().getTaxavenda() / 100:
									doc.getDocumentotipo().getTaxavenda();*/
		movimentacaoTaxa.setValor(valorTaxa);
		
		movimentacaoTaxa.setDtmovimentacao(bean.getDtefetivapagamento());
		movimentacaoTaxa.setDtbanco(bean.getDtefetivapagamento());
		movimentacaoTaxa.setMovimentacaoacao(Movimentacaoacao.CONCILIADA);
		movimentacaoTaxa.setChecknum(movimentacao.getChecknum());
		movimentacaoTaxa.setObservacao("Taxa referente a movimenta��o: "+
				"<a href=\"javascript:visualizaMovimentacao("+ 
				movimentacao.getCdmovimentacao()+");\">"+movimentacao.getCdmovimentacao() +"</a> criada via concilia��o de extrato de cart�o de cr�dito "+tidDoc);
		movimentacaoTaxa.setTaxa(Boolean.TRUE);
		movimentacaoTaxa.setTaxageradaprocessamentocartao(Boolean.TRUE);
		
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		listaRateioitem.add(new Rateioitem(contagerencial, centrocusto, null, movimentacaoTaxa.getValor(), 100d));
		rateio.setListaRateioitem(listaRateioitem);
		
		movimentacaoTaxa.setRateio(rateio);
		String historico = "Taxa da operadora de cart�o de cr�dito criada via concilia��o de extrato "+tidDoc;
		Historicooperacao historicooperacao = historicooperacaoService.findByTipo(Tipohistoricooperacao.TAXA_RECEBIMENTO);
		if(historicooperacao != null){
			List<Documento> listaDocTaxa = new ArrayList<Documento>();
			listaDocTaxa.add(doc);
			historico = historicooperacaoService.findHistoricoByMovimentofinanceiro(movimentacaoTaxa, listaDocTaxa,
																					Tipohistoricooperacao.TAXA_RECEBIMENTO, Documentoclasse.OBJ_RECEBER);
			if(historico.contains(HistoricooperacaoService.VARIAVEL_HISTORICO_VALORVINCULADO) && doc.getValor() != null){
				historico = historico.replace(HistoricooperacaoService.VARIAVEL_HISTORICO_VALORVINCULADO, doc.getValor().toString());
			}
		}
		movimentacaoTaxa.setHistorico(historico);
		movimentacaoService.saveOrUpdate(movimentacaoTaxa);
		
		movimentacaoorigemService.updateMovimentacaorelacionada(movimentacao, doc, movimentacaoTaxa);
		
		Movimentacaohistorico mhTaxa = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacaoTaxa);
		movimentacaoService.saveOrUpdateConciliacao(movimentacaoTaxa, mhTaxa);
	}
	
	private Contagerencial getContagerencialForTaxaCartaocredito(Documento documento){
		Contagerencial contagerencial = null;
		if(documento.getDocumentotipo() != null){
			Tipotaxa tipotaxa = documento.getDocumentotipo().getTipotaxacontareceber();
			contagerencial = documento.getDocumentotipo().getContagerencialtaxa() != null? documento.getDocumentotipo().getContagerencialtaxa():
							tipotaxa != null? tipotaxa.getContadebito(): null;
		}
		return contagerencial;
	}
	
	private Centrocusto getCentrocustoForTaxaCartaocredito(Documento documento){
		Centrocusto centrocusto = null;
		if(documento.getDocumentotipo() != null){
			Tipotaxa tipotaxa = documento.getDocumentotipo().getTipotaxacontareceber();
			centrocusto = documento.getDocumentotipo().getCentrocusto() != null? documento.getDocumentotipo().getCentrocusto():
							tipotaxa != null? tipotaxa.getCentrocusto(): null;
		}
		return centrocusto;
	}
	
	private Money calcularTaxa(Documentotipo documentotipo, Money valoratual, Money valormovimentacao){
		Money valorParaCalculoTaxa = Tipotaxa.TAXAMOVIMENTO.equals(documentotipo.getTipotaxacontareceber()) ? valoratual : valormovimentacao;
		Double valorcalculo = valorParaCalculoTaxa != null ? valorParaCalculoTaxa.getValue().doubleValue() : 0d;
		Double taxavenda = documentotipo != null && documentotipo.getTaxavenda() != null ? documentotipo.getTaxavenda() : 0d;
		return new Money(Boolean.TRUE.equals(documentotipo.getPercentual()) ? valorcalculo * taxavenda / 100d : taxavenda);
	}
}