package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.HistoricoAntecipacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FormapagamentoService;
import br.com.linkcom.sined.geral.service.HistoricoAntecipacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.geral.service.ValecompraorigemService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.EstornaContaBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/financeiro/process/EstornarConta", authorizationModule=ProcessAuthorizationModule.class)
public class EstornarContaProcess extends MultiActionController{

	private MovimentacaoService movimentacaoService;
	private RateioService rateioService;
	private DocumentoService documentoService;
	private ContapagarService contapagarService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ContaService contaService;
	private FormapagamentoService formapagamentoService;
	private ValecompraorigemService valecompraorigemService;
	private ValecompraService valecompraService;
	private HistoricoAntecipacaoService historicoAntecipacaoService;
	
	public void setValecompraorigemService(
			ValecompraorigemService valecompraorigemService) {
		this.valecompraorigemService = valecompraorigemService;
	}
	public void setFormapagamentoService(FormapagamentoService formapagamentoService) {
		this.formapagamentoService = formapagamentoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}
	public void setValecompraService(ValecompraService valecompraService) {
		this.valecompraService = valecompraService;
	}
	public void setHistoricoAntecipacaoService(HistoricoAntecipacaoService historicoAntecipacaoService) {
		this.historicoAntecipacaoService = historicoAntecipacaoService;
	}

	private static final String DEFAULT_ESTORNO = "defaultEstorno";
	private static final String ESTORNAR_BAIXADAS = "estornarBaixadas";
	private static final String SALVAR_ESTORNO_BAIXADAS = "salvaEstornoBaixadas";
	private static final String CONFIRMA_CONTAS_ESTORNO = "confirmaContasEstorno";
	private static final String BEANKEYESTORNO = "BeanKeyEstorno";
	private static final String CONTROLLERKEY = "controllerKey";
	private static final String ESTORNAR_OUTROS = "estornarOutros";
	
	
	/**
	 * M�todo utilizado para obter o ModelAndView de retorno ao estornar contas baixadas ou em caso de erros
	 * no processo.
	 * Pode decidir entre as telas de Contas a pagar ou receber dependendo da propriedade documentoclasse de bean
	 * 
	 * @see #getUrlRetorno(WebRequestContext)
	 * @see #addErrors(WebRequestContext)
	 * @param request
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action(DEFAULT_ESTORNO)
	public ModelAndView getDefaultEstornoModelAndView(WebRequestContext request, EstornaContaBean bean){
		if(bean.isErroInicial())
			guardaUrlRetornoSessao(request, bean.getDocumentoclasse());
		
		String controller = getUrlRetorno(request);
		addErrors(request);
		return new ModelAndView("redirect:" + controller,"estornaContaBean", bean);
	}
	
	/**
	 * M�todo para verificar se existem erros na requisi��o e lan��-los
	 * 
	 * @param request
	 * @author Fl�vio Tavares
	 */
	@SuppressWarnings("unchecked")
	private void addErrors(WebRequestContext request){
		if(request.getBindException().hasErrors()){
			request.addMessage("Estorno n�o permitido.", MessageType.WARN);
			List<ObjectError> allErrors = request.getBindException().getAllErrors();
			for (ObjectError error : allErrors) {
				request.addError(error.getDefaultMessage());
			}
		}
	}
	
	/**
	 * M�todo de entrada para se estornar contas com situa��o igual a Baixada.
	 * Prepara o bean EstornaContaBean para ser exibido na tela.
	 * 
	 * @see #guardaUrlRetornoSessao(WebRequestContext, Documentoclasse)
	 * @param request
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	@SuppressWarnings("unchecked")
	@Input(DEFAULT_ESTORNO)
	@Command(validate=true)
	@Action(ESTORNAR_BAIXADAS)
	public ModelAndView estornarBaixadas(WebRequestContext request, EstornaContaBean bean){
		
		if (bean != null && bean.getDocumentoEstornado() != null && bean.getDocumentoEstornado().getCddocumento() != null) {
			Documento doc = documentoService.load(bean.getDocumentoEstornado());
			if(doc.getPessoa() != null){
				for (Documento iterable : documentoService.findForAntecipacaoEntrega(doc.getPessoa(), null)) {
					if(iterable.getCddocumento().equals(doc.getCddocumento()) && iterable.getValor().getValue().doubleValue() < doc.getValor().getValue().doubleValue()){
						request.addError("Este documento possui v�nculo com Antecipa��o de Conta a Pagar ou Entrada Fiscal.<br> N�o � poss�vel Estorn�-lo.");
						return new ModelAndView("redirect:/financeiro/crud/Contapagar");
					}
				}
			}
		}
		
		Documento aux = new Documento();
		aux.setWhereIn(bean.getDocumentoEstornado().getCddocumento() + "");
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem movimenta��es cuja data da movimenta��o refere-se a um per�odo j� fechado.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		if (!valecompraService.estornarContaReceber(true, bean.getDocumentoEstornado().getCddocumento().toString())){
			request.addError("N�o � poss�vel estornar esta baixa, pois geraria um saldo negativo de vale-compra.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		// ORGANIZA��O DAS INFORMA��ES PARA EXIBI��O NA TELA
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		boolean existeMovimentacao = false;
		if(bean.getWhereInMovimentacaoReferente() != null && !"".equals(bean.getWhereInMovimentacaoReferente())){
			List<Movimentacao> listaMovimentacao = movimentacaoService.findMovimentacaoGerada(bean.getDocumentoEstornado());
			for(Movimentacao movimentacao : listaMovimentacao){
				if(movimentacao.getListaMovimentacaoorigem() != null && !movimentacao.getListaMovimentacaoorigem().isEmpty()){
					existeMovimentacao = true;
					for(Movimentacaoorigem movOrigem : movimentacao.getListaMovimentacaoorigem()){
						if(!listaDocumento.contains(movOrigem.getDocumento())){
							listaDocumento.add(movOrigem.getDocumento());
						}
					}
				}
			}
		} else if(bean.getMovimentacaoReferente() != null && bean.getMovimentacaoReferente().getListaMovimentacaoorigem() != null){
			listaDocumento = (List<Documento>) CollectionsUtil.getListProperty(bean.getMovimentacaoReferente().getListaMovimentacaoorigem(), "documento");
		}
		
		if(StringUtils.isNotBlank(bean.getWhereInValecompra()) || StringUtils.isNotBlank(bean.getWhereInHistoricoAntecipacao())){
			if(StringUtils.isNotBlank(bean.getWhereInValecompra())){
				List<Valecompraorigem> listaValecompraorigem = valecompraorigemService.findByValecompra(bean.getWhereInValecompra());
				for (Valecompraorigem valecompraorigem : listaValecompraorigem) {
					if(valecompraorigem.getDocumento() != null && !listaDocumento.contains(valecompraorigem.getDocumento())){
						List<Movimentacao> listaMovimentacao = movimentacaoService.findMovimentacaoGerada(valecompraorigem.getDocumento());
						if(SinedUtil.isListEmpty(listaMovimentacao)){
							listaDocumento.add(valecompraorigem.getDocumento());
						}
					}
				}
			}
			if(StringUtils.isNotBlank(bean.getWhereInHistoricoAntecipacao())){
				List<HistoricoAntecipacao> listaHistoricoAntecipacao = historicoAntecipacaoService.findByHistoricoAntecipacao(bean.getWhereInHistoricoAntecipacao());
				for (HistoricoAntecipacao historicoAntecipacao : listaHistoricoAntecipacao) {
					if(historicoAntecipacao.getDocumentoReferencia() != null && !listaDocumento.contains(historicoAntecipacao.getDocumentoReferencia())){
						List<Movimentacao> listaMovimentacao = movimentacaoService.findMovimentacaoGerada(historicoAntecipacao.getDocumentoReferencia());
						if(SinedUtil.isListEmpty(listaMovimentacao)){
							listaDocumento.add(historicoAntecipacao.getDocumentoReferencia());
						}
					}
				}
			}
		}
		
		for (Documento doc : listaDocumento) {
			doc.setSelected(doc.equals(bean.getDocumentoEstornado()));
		}
		setInfoTemplate(request, bean);
		bean.setListaDocumento(listaDocumento);
		guardaUrlRetornoSessao(request, bean.getDocumentoclasse());
		
		if(!existeMovimentacao){
			return salvarEstornoContasSemMovimentacao(request, bean);
		}
		return new ModelAndView("process/estornaConta").addObject("estornaContaBean", bean);
	}
	
	/**
	 * Guarda a url para onde o processo deve retornar em caso de erro ou finaliza��o do processo.
	 * 
	 * @param request
	 * @author Fl�vio Tavares
	 */
	private void guardaUrlRetornoSessao(WebRequestContext request, Documentoclasse documentoclasse){
		String controllerKey = request.getParameter(CONTROLLERKEY);
		if(StringUtils.isBlank(controllerKey)){
			controllerKey = "/financeiro/crud/Contapagar";
			if(Documentoclasse.OBJ_RECEBER.equals(documentoclasse)){
				controllerKey = "/financeiro/crud/Contareceber";
			}
		}
		String cddocumento = request.getParameter("cddocumento");
		if(StringUtils.isNotBlank(cddocumento))
			cddocumento = "&cddocumento=" + cddocumento;
		else cddocumento = "";
		
		request.getSession().setAttribute(CONTROLLERKEY, controllerKey + cddocumento);
	}
	
	/**
	 * Obtem a url de retorno do processo de estornar.
	 * 
	 * @param request
	 * @return
	 * @author Fl�vio Tavares
	 */
	private String getUrlRetorno(WebRequestContext request){
		String url = (String) request.getSession().getAttribute(CONTROLLERKEY);
		request.getSession().removeAttribute(CONTROLLERKEY);		
		return url;
	}
	
	/**
	 * Pr�xima etapa do processo de estornar contas baixadas. Este m�todo obt�m as contas escolhidas
	 * para serem estornadas e monta o rateio com base nas contas que n�o foram escolhidas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#valorTotalLista(List, boolean)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#carregaDocumento(Documento)
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#agruparItensRateio(List)
	 * @see br.com.linkcom.sined.geral.service.RateioService#calculaValoresRateio(Rateio, Money)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#carregaMovimentacao(Movimentacao)
	 * @param request
	 * @param bean
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Input(DEFAULT_ESTORNO)
	@Command(validate=true)
	@Action(CONFIRMA_CONTAS_ESTORNO)
	public ModelAndView confirmaContasEstorno(WebRequestContext request, EstornaContaBean bean){
		boolean existeBaixa = true;
		boolean jaEstornado = Boolean.parseBoolean(request.getParameter("jaestornado"));
		boolean hasErrors = hasErrors(request); 
		
		/*
		 * Se existir algum erro no request, os valores devem do bean s�o recompostos.
		 */
		if(hasErrors){
			ajustaEstornoBeanToSave(request, bean);
		}
		
		/*
		 * Se existir contas n�o selecionadas para estorno
		 */
		String whereInDocumentoNaoEstornar = CollectionsUtil.listAndConcatenate(bean.getListaDocumentoNaoEstornar(), "cddocumento", ",");
		request.setAttribute("whereInDocumentoNaoEstornar", whereInDocumentoNaoEstornar);
		
		if(SinedUtil.isListNotEmpty(bean.getListaDocumentoNaoEstornar()) && !jaEstornado){
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			
			Money total = documentoService.valorTotalLista(bean.getListaDocumentoNaoEstornar(), true);
			bean.setValortotalDocumentos(total);
			
			List<Documento> listaDocumentoNaoEstornar = documentoService.carregaDocumentos(whereInDocumentoNaoEstornar);
			bean.setListaDocumentoNaoEstornar(listaDocumentoNaoEstornar);
			
			//Pequeno ajuste para utilizar a fun��o que segue abaixo
			for (Documento d : bean.getListaDocumentoNaoEstornar()) {
				d.setValoratual(d.getValor());
				if(d.getEmpresa() != null){
					listaEmpresa.add(d.getEmpresa());
				}
			}
			
			Rateio rateio = contapagarService.agruparItensRateio(listaDocumentoNaoEstornar, false, null);
			bean.setRateio(rateio);
			rateioService.calculaValoresRateio(bean.getRateio(), total);
			
			BaixarContaBean baixarContaBean = new BaixarContaBean();
			baixarContaBean.setListaDocumento(bean.getListaDocumentoNaoEstornar());
			bean.setBaixarContaBean(baixarContaBean);
			
			if(listaEmpresa != null && listaEmpresa.size() > 0){
				request.setAttribute("empresaWhereIn", CollectionsUtil.listAndConcatenate(listaEmpresa, "cdpessoa", ","));
			}
			
		}else if(bean.getBaixarContaBean() == null){
			existeBaixa = false;
		}else{
			bean.getBaixarContaBean().setListaDocumento(bean.getListaDocumentoNaoEstornar());
		}
		
		Boolean mesmoCliente = null;
		if(bean.getListaDocumentoNaoEstornar() != null){
			mesmoCliente = Boolean.TRUE;
			List<Integer> listaCdpessoa = new ArrayList<Integer>();
			for (Documento d : bean.getListaDocumentoNaoEstornar()) {
				if (bean.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR)) {
					mesmoCliente = Boolean.FALSE;
				} else {
					if(d.getTipopagamento() != null && 
							d.getTipopagamento().equals(Tipopagamento.CLIENTE) && 
							d.getPessoa() != null &&
							d.getPessoa().getCdpessoa() != null){
						listaCdpessoa.add(d.getPessoa().getCdpessoa());
					} else {
						listaCdpessoa.add(-1);
					}
				}
			}
			
			if(mesmoCliente){
				Integer cdpessoa_aux = null;
				for (Integer cdpessoa : listaCdpessoa) {
					if(cdpessoa.equals(-1)){
						mesmoCliente = Boolean.FALSE;
						break;
					} else {
						if(cdpessoa_aux == null){
							cdpessoa_aux = cdpessoa;
						} else if(!cdpessoa_aux.equals(cdpessoa)){
							mesmoCliente = Boolean.FALSE;
							break;
						}
					}
				}
			}
		}
		
		List<Formapagamento> listaFormapagamento = null;
		if (bean.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR)) {
			listaFormapagamento = formapagamentoService.findDebito();
		} else {
			listaFormapagamento = formapagamentoService.findCredito();
			
			if(mesmoCliente != null && mesmoCliente){
				Formapagamento valecompra = Formapagamento.VALECOMPRA;
				listaFormapagamento.add(valecompra);
			}
		}
		request.setAttribute("listaFormapagamento", listaFormapagamento);
		
		
		request.setAttribute("tipooperacaoBaixa", 
				Documentoclasse.OBJ_PAGAR.equals(bean.getDocumentoclasse()) ? Tipooperacao.TIPO_DEBITO : Tipooperacao.TIPO_CREDITO);
		
		bean.setExisteBaixa(existeBaixa);
		
		/*
		 * Se a diferen�a n�o for desconsiderada. Gera nova movimenta��o
		 */
		Tipooperacao tipooperacao = Documentoclasse.OBJ_PAGAR.equals(bean.getDocumentoclasse()) ? Tipooperacao.TIPO_CREDITO : Tipooperacao.TIPO_DEBITO;
		if(Util.booleans.isFalse(bean.getDesconsiderarDiferenca()) && !jaEstornado){
			Movimentacao movimentacao = new Movimentacao();
			
			movimentacao.setValor(documentoService.valorTotalLista(bean.getListaDocumentoAEstornar(),true));
			movimentacao.setTipooperacao(tipooperacao);
			movimentacao.setDtmovimentacao(SinedDateUtils.currentDate());
			movimentacao.setSaldo(movimentacao.getValor());
			
			Rateio rateio = new Rateio();
			rateio.setValortotaux(movimentacao.getValor().toString());
			rateio.setValoraux(movimentacao.getValor().toString());
			rateio.setListaRateioitem(null);
			
			bean.setRateioDiferenca(rateio);
			bean.setMovimentacao(movimentacao);
			
			request.setAttribute("tipooperacaoDiferenca", movimentacao.getTipooperacao());
		} else if(bean.getMovimentacao() != null){
			Movimentacao movimentacao = bean.getMovimentacao();
			movimentacao.setSaldo(movimentacao.getValor());
			movimentacao.setTipooperacao(tipooperacao);
			request.setAttribute("tipooperacaoDiferenca", movimentacao.getTipooperacao());
		}
				
		request.setAttribute("existeBaixa", existeBaixa);
		request.setAttribute("conf", bean.isConf());
		if(!jaEstornado) request.getSession().setAttribute(BEANKEYESTORNO, bean);
		
		
		setInfoTemplate(request, bean);
		return new ModelAndView("process/estornaConta").addObject("estornaContaBean", bean);
	}

	/**
	 * M�todo para verificar se foi lan�ado algum erro no request.
	 * 
	 * @param request
	 * @return
	 * @author Fl�vio Tavares
	 */
	private boolean hasErrors(WebRequestContext request){
		boolean errorBind = request.getBindException().hasErrors();
		boolean errorThrow = false;
		for (Message message : request.getMessages()) {
			errorThrow = message.getType().equals(MessageType.ERROR);
			if(errorBind) break;
		}
		return errorBind || errorThrow;
	}
	
	/**
	 * Envia par�metros ao JSP da tela de estornar conta.
	 * 
	 * @param request
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	private void setInfoTemplate(WebRequestContext request, EstornaContaBean bean){
		Tipooperacao tipooperacao = Documentoclasse.OBJ_RECEBER.equals(bean.getDocumentoclasse()) ? 
				Tipooperacao.TIPO_CREDITO : Tipooperacao.TIPO_DEBITO;
		
		String urlRetornoEstornar = (String) request.getSession().getAttribute(CONTROLLERKEY);
		if(StringUtils.isBlank(urlRetornoEstornar)){
			urlRetornoEstornar = "/financeiro/crud/Contapagar";
			if(Documentoclasse.OBJ_RECEBER.equals(bean.getDocumentoclasse())){
				urlRetornoEstornar = "/financeiro/crud/Contareceber";
			}
		}
		request.setAttribute("urlRetornoEstornar", urlRetornoEstornar);
		request.setAttribute("tipooperacao", tipooperacao);
	}
	
	/**
	 * M�todo chamado para salvar o estorno de contas baixadas.
	 * 
	 * @see #ajustaEstornoBeanToSave(WebRequestContext, EstornaContaBean)
	 * @see #getDefaultEstornoModelAndView(WebRequestContext, EstornaContaBean)
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Fl�vio Tavares
	 */
	@Input(CONFIRMA_CONTAS_ESTORNO)
	@Command(validate=true)
	@Action(SALVAR_ESTORNO_BAIXADAS)
	public ModelAndView salvarEstornoContas(WebRequestContext request, EstornaContaBean bean) throws Exception{
		this.ajustaEstornoBeanToSave(request, bean);
		documentoService.doEstornarContasBaixadas(bean);
		request.addMessage("Conta(s) estornada(s) com sucesso.");
		return getDefaultEstornoModelAndView(request, bean);
	}
	
	public ModelAndView salvarEstornoContasSemMovimentacao(WebRequestContext request, EstornaContaBean bean) {
		try {
			documentoService.doEstornarContasBaixadas(bean);
			request.addMessage("Conta(s) estornada(s) com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao estornar conta(s): " + e.getMessage());
		}
		return getDefaultEstornoModelAndView(request, bean);
	}
	
	/**
	 * M�todo para ajustar o bean de estorno ao ser salvo.
	 * 
	 * @param request
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	private void ajustaEstornoBeanToSave(WebRequestContext request, EstornaContaBean bean){
		EstornaContaBean beanSaved = (EstornaContaBean) request.getSession().getAttribute(BEANKEYESTORNO);
		bean.setListaDocumento(beanSaved.getListaDocumento());
		bean.setDesconsiderarDiferenca(beanSaved.getDesconsiderarDiferenca());
		bean.setDocumentoclasse(beanSaved.getDocumentoclasse());
		bean.setExisteBaixa(beanSaved.isExisteBaixa());
		
		if(bean.isExisteBaixa()){
			bean.getBaixarContaBean().setDocumentoclasse(beanSaved.getDocumentoclasse());
		}
	}
	
	
	/**
	 * Valida��es gerais do controller
	 */
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		/**
		 * Valida as requisi��es referentes aos estornos de contas baixadas
		 */
		if (obj instanceof EstornaContaBean) {
			EstornaContaBean bean = (EstornaContaBean) obj;
			
			/*
			 * Valida��es da a��o de entrada para estornar contas baixadas
			 */
			if(ESTORNAR_BAIXADAS.equals(acao)){
				
				Documento documento = documentoService.contaReceberDevolucao(bean.getDocumentoEstornado().getCddocumento());
				
				if(!SinedUtil.isObjectValid(bean.getDocumentoEstornado())){
					errors.reject("001","Deve ser informado um documento a ser estornado.");
				}else if(documento!=null && documento.getDocumentotipo()!=null && Boolean.TRUE.equals(documento.getDocumentotipo().getAntecipacao())){
					errors.reject("001","A conta selecionada � de antecipa��o. Para estornar a baixa dessa conta, realize o cancelamento da movimenta��o diretamente na tela de Movimenta��o.");
				}else{
					List<Valecompraorigem> listaValecompraorigem = valecompraorigemService.findByDocumento(bean.getDocumentoEstornado());
					List<HistoricoAntecipacao> listaHistoricoAntecipacao = historicoAntecipacaoService.findByDocumentoReferencia(bean.getDocumentoEstornado());
					List<Movimentacao> listaMovimentacao = movimentacaoService.findMovimentacaoGerada(bean.getDocumentoEstornado());
					
					if(SinedUtil.isListEmpty(listaMovimentacao) && SinedUtil.isListEmpty(listaValecompraorigem) && SinedUtil.isListEmpty(listaHistoricoAntecipacao)){
						if(documento != null && documento.getDocumentoclasse() != null && documento.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
							errors.reject("001","Esta conta n�o corresponde a nenhuma movimenta��o e a nenhum vale compra ou antecipa��o.");
						} else {
							errors.reject("001","Esta conta n�o corresponde a nenhuma movimenta��o.");
						}
					}else{
						if(documento != null && documento.getDtdevolucao() != null){
							errors.reject("001", "Est� conta j� foi devolvida.");
						}else {
							StringBuilder whereInMovReferente = new StringBuilder();
							for(Movimentacao movimentacao : listaMovimentacao){
								if(!movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.NORMAL) && 
										!movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.CANCELADA)){
									errors.reject("001", "Movimenta��o financeira j� conciliada.");
								}else{
									if(!"".equals(whereInMovReferente.toString())) whereInMovReferente.append(",");
									whereInMovReferente.append(movimentacao.getCdmovimentacao());
//									bean.setMovimentacaoReferente(movimentacao);
								}
							}
							bean.setWhereInMovimentacaoReferente(whereInMovReferente.toString());
							if(listaMovimentacao.size() == 1) bean.setMovimentacaoReferente(listaMovimentacao.get(0));
							
							StringBuilder whereInValecompra = new StringBuilder();
							for (Valecompraorigem valecompraorigem : listaValecompraorigem) {
								if(!"".equals(whereInValecompra.toString())) whereInValecompra.append(",");
								whereInValecompra.append(valecompraorigem.getValecompra().getCdvalecompra());
							}
							bean.setWhereInValecompra(whereInValecompra.toString());
							
							StringBuilder whereInAntecipacao = new StringBuilder();
							for (HistoricoAntecipacao historicoAntecipacao : listaHistoricoAntecipacao) {
								if(!"".equals(whereInAntecipacao.toString())) whereInAntecipacao.append(",");
								whereInAntecipacao.append(historicoAntecipacao.getCdHistoricoAntecipacao());
							}
							bean.setWhereInHistoricoAntecipacao(whereInAntecipacao.toString());
						} 
					}
				}
				
				bean.setErroInicial(errors.hasErrors());
			}
			
			/*
			 * Valida��es do bean ao confirmar as contas a serem estornadas
			 */
			if(CONFIRMA_CONTAS_ESTORNO.equals(acao)){
				List<Documento> listaDocumentoAEstornar = bean.getListaDocumentoAEstornar();
				if(SinedUtil.isListEmpty(listaDocumentoAEstornar)){
					errors.reject("001","� necess�rio informar pelo menos uma conta a ser estornada.");
				} else {
					Money valortotal = new Money();
					
					for (Documento documento : listaDocumentoAEstornar) {
						valortotal = valortotal.add(documento.getAux_documento() != null ? documento.getAux_documento().getValoratual() : new Money());
					}
					
					Movimentacao movimentacao = movimentacaoService.loadForEntrada(bean.getMovimentacaoReferente());
					if(movimentacao != null && movimentacao.getConta() != null){
						Conta conta = contaService.load(movimentacao.getConta());
				
						if (movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {				
							double valorMov = valortotal.getValue().doubleValue();
							double valorMaximo = contaService.obterValorMaximo(conta).getValue().doubleValue();

							if (valorMov > valorMaximo){
								errors.reject("002","Existe(m) documento(s) que o saldo da Caixa ou Conta Corrente ficar� negativo se ocorrer o estorno.");
							}
						}
					}
					
				}
			}
			
			/*
			 * Valida��es da a��o de salvar os estornos
			 */
			if(SALVAR_ESTORNO_BAIXADAS.equals(acao)){
				
				/*
				 * Valida o rateio da baixa somente se existe baixa.					
				 */
				if(bean.isExisteBaixa()){
					if(bean.getBaixarContaBean() != null && bean.getBaixarContaBean().getFormapagamento() != null && bean.getBaixarContaBean().getFormapagamento().equals(Formapagamento.VALECOMPRA)) {
						if(bean.getBaixarContaBean().getValormaximovalecompra() != null &&
								bean.getValortotalDocumentos().compareTo(bean.getBaixarContaBean().getValormaximovalecompra()) < 0){
							errors.reject("001","Saldo do vale compra n�o suficiente para pagamento.");
						}
					} else {
						try {
							rateioService.validateRateio(bean.getRateio(), bean.getValortotalDocumentos());
						} catch (SinedException se) {
							errors.reject("001","O valor da baixa � diferente do valor total rateado.");
						}
					}
				}
			
				// Se a diferen�a n�o for desconsiderada
				if(bean.getMovimentacao() != null){
					
					// Valida a movimenta��o de diferen�a
					movimentacaoService.validateMovimentacao(errors, bean.getMovimentacao());
					
					try {
						// Valida o rateio da movimenta��o de diferen�a
						rateioService.validateRateio(bean.getRateioDiferenca(), bean.getMovimentacao().getValor());
					} catch (SinedException se) {
						errors.reject("001","O valor da movimenta��o de diferen�a � diferente do valor total rateado.");
					}
				}
			}
			
		} //Fim das valida��es de ESTORNAR
		
	}
	
	/**
	 * M�todo que carrega a p�gina para criar a observa��o do hist�rico do documento.
	 * - A��o: ESTORNAR
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#findContas
	 * @param request
	 * @param acao
	 * @return
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares - Mudan�a da condi��o para estornar uma conta.
	 */
	@Action(ESTORNAR_OUTROS)
	public ModelAndView estornar(WebRequestContext request, Documentoacao acao){
		if (acao == null || acao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
				
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem movimenta��es cuja data da movimenta��o refere-se a um per�odo j� fechado.");
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		}
		
		if (!valecompraService.estornarContaReceber(true, whereIn)){
			request.addError("N�o � poss�vel estornar esta baixa, pois geraria um saldo negativo de vale-compra.");
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		}
		
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setIds(whereIn);
		documentohistorico.setDocumentoacao(acao);

		request.setAttribute("descricao", "ESTORNAR");
		request.setAttribute("msg", "Justificativa para o estorno");
		
		return new ModelAndView("direct:/process/popup/estornoJustificativa")
								.addObject("documentohistorico", documentohistorico);
	}
	
	/**
	 * M�todo que muda o status das contas a pagar selecionadas para PREVISTA(Estorno).
	 * - Gera registros na tabela DocumentoHistorico referente as contas selecionadas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#doEstornar(Documentohistorico)
	 * @param request
	 * @param documentohistorico
	 * @return
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	@Action("saveEstornada")
	public void saveEstornada(WebRequestContext request, Documentohistorico documentohistorico){
		List<String> listaErros = null;
		
		try {
			listaErros = documentoService.doEstornar(documentohistorico);
			
			if (listaErros == null || listaErros.isEmpty()) {
				request.addMessage("Conta(s) estornada(s) com sucesso.");
			} else {
				for (String string : listaErros) {
					request.addError(string);
				}
			}
			
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		} finally {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
		}
	}
	
	/**
	* M�todo que estorna a baixa parcial
	*
	* @param request
	* @param bean
	* @return
	* @throws Exception
	* @since 16/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView estornarBaixadasParcial(WebRequestContext request, EstornaContaBean bean) throws Exception{
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereInDocumentoEstornado = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereInDocumentoEstornado)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		String url = "redirect:/financeiro/crud/Contareceber" + (entrada ? "?ACAO=consultar&cddocumento=" + whereInDocumentoEstornado : "");
		
		if(documentoService.validaDocumentoSituacao(whereInDocumentoEstornado, true, Documentoacao.BAIXADA_PARCIAL)){
			request.addError("N�o � poss�vel estornar. Contas baixadas parcialmente devem ser estornadas separadamente.");
			return new ModelAndView(url);
		}
		
		Documento aux = new Documento();
		aux.setWhereIn(whereInDocumentoEstornado);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem movimenta��es cuja data da movimenta��o refere-se a um per�odo j� fechado.");
			return new ModelAndView(url);
		}
		
		bean.setListaDocumentoAEstornar(new ArrayList<Documento>());
		for(String id : whereInDocumentoEstornado.split(",")){
			Documento documento = new Documento(Integer.parseInt(id));
			if(movimentacaoService.isMovimentacaoCancelada(documento)){
				request.addError("N�o � poss�vel estornar conta(s), pois possui(em) movimenta��o(�es) vinculada(s). Para realizar esta a��o, ser� necess�rio realizar o cancelamento da(s) movimenta��o(�es) relacionada(s).");
				return new ModelAndView(url);
			}
			bean.getListaDocumentoAEstornar().add(documento);
		}
		
		if (!valecompraService.estornarContaReceber(true, whereInDocumentoEstornado)){
			request.addError("N�o � poss�vel estornar esta baixa, pois geraria um saldo negativo de vale-compra.");
			return new ModelAndView(url);
		}
		
		documentoService.doEstornarContasBaixadasParcial(bean);
		request.addMessage("Conta(s) estornada(s) com sucesso.");
		return new ModelAndView(url);
	}
	
	/**
	* M�todo ajax que verifica se existe algum documento com vale compra
	*
	* @param request
	* @return
	* @since 18/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificaValecompraDocumento(WebRequestContext request){
		boolean existeValecompra = false;
		boolean existeAntecipacao = false;
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isNotBlank(whereIn)){
			existeValecompra = valecompraorigemService.existeValecompra(whereIn);
			existeAntecipacao = historicoAntecipacaoService.existeAntecipacao(whereIn);
		}
		return new JsonModelAndView()
			.addObject("existeValecompra", existeValecompra)
			.addObject("existeAntecipacao", existeAntecipacao);
	}
}
