package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentonegociado;
import br.com.linkcom.sined.geral.bean.Documentonegociadoitem;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Parcela;
import br.com.linkcom.sined.geral.bean.Parcelamento;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.ColaboradordespesaService;
import br.com.linkcom.sined.geral.service.ContacorrenteService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentonegociadoService;
import br.com.linkcom.sined.geral.service.DocumentonegociadoitemService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FornecimentoService;
import br.com.linkcom.sined.geral.service.GnreService;
import br.com.linkcom.sined.geral.service.IndicecorrecaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaHistoricoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ParcelamentoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoitemService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.geral.service.TipotaxaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoItemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.AssociarNotaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.EmitirreciboBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.NegociacaoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/financeiro/process/FluxoStatusConta", authorizationModule=ProcessAuthorizationModule.class)
public class FluxoStatusContaProcess extends MultiActionController {
	
	private DocumentoService documentoService;
	private ContapagarService contapagarService;
	private ContareceberService contareceberService;
	private DocumentohistoricoService documentohistoricoService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private PrazopagamentoService prazopagamentoService;
	private RateioService rateioService;
	private IndicecorrecaoService indicecorrecaoService;
	private ParcelamentoService parcelamentoService;
	private DocumentoorigemService documentoorigemService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotaDocumentoService notaDocumentoService;
	private NotaHistoricoService notaHistoricoService;
	private NotaService notaService;
	private ContatoService contatoService;
	private UsuarioService usuarioService;
	private DocumentonegociadoService documentonegociadoService;
	private DocumentonegociadoitemService documentonegociadoitemService;
	private RateioitemService rateioitemService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private FornecimentoService fornecimentoService;
	private DespesaviagemService despesaviagemService;
	private EntradafiscalService entradafiscalService;
	private MovimentacaoService movimentacaoService;
	private ParametrogeralService parametrogeralService;
	private TipotaxaService tipotaxaService;
	private ContacorrenteService contacorrenteService;
	private ColaboradordespesaService colaboradordespesaService;
	private GnreService gnreService;
	
	public void setGnreService(GnreService gnreService) {this.gnreService = gnreService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setParcelamentoService(ParcelamentoService parcelamentoService) {this.parcelamentoService = parcelamentoService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setIndicecorrecaoService(IndicecorrecaoService indicecorrecaoService) {this.indicecorrecaoService = indicecorrecaoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setDocumentonegociadoService(DocumentonegociadoService documentonegociadoService) {this.documentonegociadoService = documentonegociadoService;}
	public void setDocumentonegociadoitemService(DocumentonegociadoitemService documentonegociadoitemService) {this.documentonegociadoitemService = documentonegociadoitemService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setFechamentofinanceiroService(	FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;}
	public void setFornecimentoService(FornecimentoService fornecimentoService) {this.fornecimentoService = fornecimentoService;}
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {this.despesaviagemService = despesaviagemService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {this.contacorrenteService = contacorrenteService;}
	public void setColaboradordespesaService(ColaboradordespesaService colaboradordespesaService) {this.colaboradordespesaService = colaboradordespesaService;}
	
	/**
	 * M�todo que muda o status das contas a pagar selecionadas para AUTORIZADA e CONFIRMAR(DEFINITIVA).
	 * - Gera registros na tabela DocumentoHistorico referente as contas selecionadas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#findContas
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#doAutorizar(Documentoacao, String)
	 * @param request
	 * @param acao
	 * @return
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public ModelAndView autorizar(WebRequestContext request, Documentoacao acao){
		
		if (acao == null || acao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Documento> listaConta = contapagarService.findContas(whereIn);
		for (Documento documento : listaConta) {
			if(acao.equals(Documentoacao.AUTORIZADA) && usuarioService.isValorMaiorLimitevaloraprovacaoautorizacao(whereIn, "contapagar")){
				request.addError("O valor da conta a pagar � maior do que o limite para autoriza��o.");
				return getControllerModelAndView(request);
			}
			if (acao.equals(Documentoacao.DEFINITIVA) && !documento.getDocumentoacao().equals(Documentoacao.PREVISTA)) {
				request.addError("Registro(s) com situa��o diferente de 'prevista'.");
				return getControllerModelAndView(request);
			}
			if (acao.equals(Documentoacao.AUTORIZADA) && !documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)
					&& !documento.getDocumentoacao().equals(Documentoacao.NAO_AUTORIZADA)) {
				request.addError("Registro(s) com situa��o diferente de 'definitiva' e 'n�o autorizada'.");
				return getControllerModelAndView(request);
			}
		}
		
		documentoService.doAutorizar(acao, whereIn);
		
		if (acao.equals(Documentoacao.DEFINITIVA)) {
			request.addMessage("Conta(s) confirmada(s) com sucesso.");
		} else {
			request.addMessage("Conta(s) autorizada(s) com sucesso.");
		}
		
		return getControllerModelAndView(request);
	}
	
	/**
	 * M�todo que carrega a p�gina para criar a observa��o do hist�rico do documento.
	 * - A��o: N�O AUTORIZAR
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#findContas
	 * @param request
	 * @param acao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView naoautorizar(WebRequestContext request, Documentoacao acao){
		
		if (acao == null || acao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Documento> listaConta = contapagarService.findContas(whereIn);
		for (Documento documento : listaConta) {
			if (!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)) {
				request.addError("Registro(s) com situa��o diferente de 'definitiva'.");
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
				return null;
			}
		}
		
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setIds(whereIn);
		documentohistorico.setDocumentoacao(acao);
		
		request.setAttribute("descricao", "N�O AUTORIZAR");
		request.setAttribute("msg", "Justificativa para n�o autoriza��o");
		
		return new ModelAndView("direct:/process/popup/naoAutorizadaJustificativa")
								.addObject("documentohistorico", documentohistorico);
	}
	
	/**
	 * M�todo que muda o status das contas a pagar selecionadas para N�O AUTORIZADA ou CANCELADA.
	 * - Gera registros na tabela DocumentoHistorico referente as contas selecionadas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#doReagendar(Documentohistorico)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#doCancelar(Documentohistorico)
	 * @param request
	 * @param documentohistorico
	 * @return
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	@Action("saveNaoAutorizada")
	public void saveNaoAutorizada(WebRequestContext request, Documentohistorico documentohistorico){
		try{
			if (documentohistorico.getDocumentoacao().equals(Documentoacao.NAO_AUTORIZADA) && documentohistorico.getReagendar() != null && documentohistorico.getReagendar()) {
				documentoService.doReagendar(documentohistorico);
				request.addMessage("Conta(s) reagendada(s) com sucesso.");
			} else {
				documentohistorico.setFromcontareceber(true);
				Documentoacao acao = documentoService.doCancelar(documentohistorico);
				
				if (acao.equals(Documentoacao.NAO_AUTORIZADA)) {
					request.addMessage("Conta(s) n�o autorizada(s) com sucesso.");
				} else if (acao.equals(Documentoacao.CANCELADA)) {
					notaDocumentoService.adicionaMensagensCancelamentoContaWebserviceFalse(request, documentohistorico.getIds());
					notaDocumentoService.cancelaNotaEmespera(request, documentohistorico.getIds());
					documentoService.desmarcarParcelacobradaOrigemContrato(documentohistorico.getIds());
					fornecimentoService.verificaFornecimentoAposCancelamentoDocumento(documentohistorico.getIds());
					gnreService.verificaGnreAposCancelamentoDocumento(documentohistorico.getIds());
					despesaviagemService.verificaDespesaviagemAposCancelamentoDocumento(documentohistorico.getIds());
					entradafiscalService.verificaEntradafiscalAposCancelamentoDocumento(documentohistorico.getIds());
					notaDocumentoService.verificaStatusNota(request, documentohistorico.getIds());
					documentoService.alteraSituacaoAnteriorNegociada(documentohistorico.getIds());
					colaboradordespesaService.verificaDespesaColaboradorAposCancelamentoDocumento(documentohistorico.getIds());
					
					// exclui o historico de antecipa��o apenas das contas geradas a partir do fluxo Compensa��o de Saldo
					for(String id : documentohistorico.getIds().split(",")) {
						Documento documento = new Documento(Integer.parseInt(id));
						documentoService.excluirHistoricoAntecipacaoDocumento(documento, true);
					}
					
					request.addMessage("Conta(s) cancelada(s) com sucesso.");
				}
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
		}
	}
	
	/**
	 * M�todo que carrega a p�gina para criar a observa��o e o valorda parcial autoriza��o do documento.
	 * - A��o: AUTORIZAR PARCIAL
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#findContas
	 * @param request
	 * @param acao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView autorizarparcial(WebRequestContext request, Documentoacao acao){
		
		if (acao == null || acao.getCddocumentoacao() == null) 
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) 
			throw new SinedException("Nenhum item selecionado.");
		
		List<Documento> listaConta = contapagarService.findContas(whereIn);

		if (listaConta.size() == 0)
			throw new SinedException("Nenhum item selecionado.");
		
		Money valorTotal = new Money(0);

		Documento primeiroDocumento = listaConta.get(0);
		for (Documento doc : listaConta){
			if ((primeiroDocumento.getPessoa() != null && doc.getPessoa() == null) ||
					(primeiroDocumento.getPessoa() == null && doc.getPessoa() != null) ||
					(primeiroDocumento.getOutrospagamento() != null &&
					!primeiroDocumento.getOutrospagamento().equals("") && 
					!primeiroDocumento.getOutrospagamento().equals(doc.getOutrospagamento())) ||
					(primeiroDocumento.getPessoa() != null && 
					 doc.getPessoa() != null &&
					 !primeiroDocumento.getPessoa().getCdpessoa().equals(doc.getPessoa().getCdpessoa()))){
				request.addError("Todas as contas devem pertencer � mesma pessoa.");
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
				return null;
			}

			if (!doc.getDocumentoacao().equals(Documentoacao.DEFINITIVA) && !doc.getDocumentoacao().equals(Documentoacao.NAO_AUTORIZADA)) {
				request.addError("Registro(s) com situa��o diferente de 'definitiva' e 'n�o autorizada'.");
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
				return null;
			}
			
			valorTotal = valorTotal.add(doc.getValor());
		}
	
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setDocumentoacao(acao);
		documentohistorico.setValortotaux(valorTotal);
		documentohistorico.setReagendar(Boolean.TRUE);
		documentohistorico.setIds(whereIn);
		
		request.setAttribute("descricao", "AUTORIZAR PARCIAL");
		request.setAttribute("msg", "Justificativa para autoriza��o parcial");
		
		List<Documentoacao> listaAcao = new ArrayList<Documentoacao>();
		listaAcao.add(new Documentoacao(Documentoacao.DEFINITIVA.getCddocumentoacao(),"DEFINITIVA"));
		listaAcao.add(new Documentoacao(Documentoacao.PREVISTA.getCddocumentoacao(),"PREVISTA"));
		request.setAttribute("listaStatus", listaAcao);
		request.setAttribute("dataAtualStr", NeoFormater.getInstance().format(SinedDateUtils.currentDate()));
		
		return new ModelAndView("direct:/process/popup/autorizadaParcialJustificativa")
								.addObject("documentohistorico", documentohistorico);
	}
	
	/**
	 * M�todo que muda o status das contas a pagar selecionadas para AUTORIZADA PARCIAL.
	 * - Gera registros na tabela DocumentoHistorico referente as contas selecionadas.
	 * 
	 * @see 
	 * @param request
	 * @param documentohistorico
	 * @return
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	@Action("saveAutorizadaParcial")
	public void saveAutorizadaParcial(WebRequestContext request, Documentohistorico documentohistorico){
		try{			
			List<Documento> listaConta = contapagarService.findContas(documentohistorico.getIds());
			
			double proporcaoPercentual = documentohistorico.getValor().divide(documentohistorico.getValortotaux()).getValue().doubleValue();
			
			if (documentohistorico.getReagendar()){
				criaNovasContasAPagar(documentohistorico, listaConta);
//				continueOnAction("criaNovasContasAPagar", documentohistorico);
			}
			
			List<Documentohistorico> listaDocumentohistorico = new ArrayList<Documentohistorico>();
			for(Documento doc : listaConta){
				Documentohistorico dochistorico = new Documentohistorico();
				dochistorico.setDocumentoacao(documentohistorico.getDocumentoacao());
				dochistorico.setValortotaux(documentohistorico.getValortotaux());
				dochistorico.setReagendar(documentohistorico.getReagendar());
				dochistorico.setObservacao(documentohistorico.getObservacao());
				dochistorico.setValor(documentohistorico.getValor());
				dochistorico.setDocumento(doc);

				listaDocumentohistorico.add(dochistorico);
			}

			documentoService.doAutorizarParcial(listaDocumentohistorico, proporcaoPercentual);
			
			request.addMessage("Conta autorizada parcialmente com sucesso.");
			request.getServletResponse().getWriter().print("<script>window.opener.location.href = window.opener.location.href;</script>");
		} catch (Exception e) {
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		if (Util.booleans.isTrue(documentohistorico.getReagendar())) {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.close();</script>");
		} else {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
		}
		
	}
	
	/**
	 * M�todo que carrega a p�gina para criar a observa��o do hist�rico do documento.
	 * - A��o: CANCELAR
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#findContas
	 * @see 
	 * @param request
	 * @param acao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cancelar(WebRequestContext request, Documentoacao acao){
		
		if (acao == null || acao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		try{
			documentoService.validacaoCancelamentoDocumento(request, whereIn, false);
		} catch (Exception e) {
			request.addError(e.getMessage());
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		}
		
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setIds(whereIn);
		documentohistorico.setDocumentoacao(acao);
		
		request.setAttribute("descricao", "CANCELAR");
		request.setAttribute("msg", "Justificativa para o cancelamento");
		request.setAttribute("isCancelamento", Boolean.TRUE);
		
		return new ModelAndView("direct:/process/popup/canceladaJustificativa")
								.addObject("documentohistorico", documentohistorico);
	}
	
	/**
	 * M�todo que carrega a p�gina para criar a observa��o do hist�rico do documento.
	 * - A��o: PROTESTADA
	 * 
	 * @param request
	 * @param acao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cartorio(WebRequestContext request, Documentoacao acao){
		
		if (acao == null || acao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		String controller = request.getParameter("controller");
		
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		}
		
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setIds(whereIn);
		documentohistorico.setDocumentoacao(Documentoacao.PROTESTADA);
		documentohistorico.setDtcartorio(new Date(System.currentTimeMillis()));
		
		boolean exibirComboDocumentoacao = false;
		if(controller != null && controller.contains("Contareceber")){
			List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
			listaDocumentoacao.add(Documentoacao.ENVIADO_CARTORIO);
			listaDocumentoacao.add(Documentoacao.ENVIADO_JURIDICO);
			listaDocumentoacao.add(Documentoacao.PROTESTADA);
			listaDocumentoacao.add(Documentoacao.ARQUIVO_MORTO);
			request.setAttribute("listaDocumentoacao", listaDocumentoacao);
			exibirComboDocumentoacao = true;
			documentohistorico.setFromAcaoProtestar(true);
		}
		request.setAttribute("exibirComboDocumentoacao", exibirComboDocumentoacao);

		request.setAttribute("descricao", "PROTESTAR");
		request.setAttribute("msg", "Justificativa para protestar");
		request.setAttribute("saveACAO", "saveCartorio");
		
		return new ModelAndView("direct:/process/popup/cartorioJustificativa")
								.addObject("documentohistorico", documentohistorico);
	}
	
	/**
	 * M�todo que carrega a p�gina para escolher o cliente que ser� gravado na(s) conta(s) selecionadas.
	 * @param request
	 * @param acao
	 * @return
	 * @author Taidson
	 * @since 20/07/2010
	 */
	public ModelAndView mudarCliente (WebRequestContext request, Documentoacao acao){
		
		if (acao == null || acao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}

		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			SinedUtil.fechaPopUp(request);
			request.addError("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
			return null;
		}
		
		List<Documento> listaConta = documentoService.loadDocumentos(whereIn);
		for (Documento documento : listaConta) {
			if (!documento.getDocumentoacao().equals(Documentoacao.PREVISTA)) {
				request.addError("Registro(s) com situa��o diferente de 'prevista'.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			if(!documento.getTipopagamento().equals(Tipopagamento.CLIENTE)){
				request.addError("A mundan�a de cliente s� � permitido para conta(s) com o recebimento de Cliente.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		Documento documento = new Documento();
		documento.setIds(whereIn);
		
		request.setAttribute("descricao", "MUDAN�A DE CLIENTE");
		request.setAttribute("msg", "Cliente");
		
		return new ModelAndView("direct:/process/popup/mudancaCliente")
		.addObject("documento", documento);
	}
	

	/**
	 * M�todo respons�vel por alterar o(s) cliente(s) da(s) conta(s) selecionada(s).
	 * @param request
	 * @param documento
	 * @author Taidson
	 * @since 20/07/2010
	 */
	public void saveMudancaCliente(WebRequestContext request, Documento documento){
		try {
			if (documento.getIds() == null || documento.getIds().equals("")) {
				throw new SinedException("Nenhum item selecionado.");
			}else{
				List<Documento> listacontas = documentoService.loadDocumentos(documento.getIds());
				
				for (Documento doc : listacontas) {
					if(!doc.getDocumentoacao().equals(Documentoacao.PREVISTA)){
						request.addError("Registro(s) com situa��o diferente de 'prevista'.");
					}
				}
				
				for (Documento item : listacontas) {
				
					Documentohistorico documentohistorico = new Documentohistorico();
					documentohistorico.setDocumentoacao(Documentoacao.MUDANCA_CLIENTE);
					documentohistorico.setDtaltera(new Timestamp (Calendar.getInstance().getTimeInMillis()));
					documentohistorico.setObservacao("Mudan�a do cliente: "+item.getPessoa().getNome());
				
					contareceberService.mudarCliente(item, documento.getCliente());
					documentohistorico.setDocumento(item);
					documentohistoricoService.saveOrUpdateNoUseTransaction(documentohistorico);

				}
				request.addMessage("Mudan�a(s) de cliente(s) realizada(s) com sucesso.", MessageType.INFO);
			}
		} catch (Exception e) {
			request.addError("A mudan�a de cliente n�o foi realizada. " + e.getMessage());
		} finally {
			SinedUtil.fechaPopUp(request);
		}
	}
	
	/**
	 * M�todo que gera registros na tabela DocumentoHistorico referente as contas selecionadas.
	 * 
	 * @see  br.com.linkcom.sined.geral.service.DocumentoService#doEmCartorio(Documentohistorico)
	 * @param request
	 * @param acao
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("saveCartorio")
	public void saveCartorio(WebRequestContext request, Documentohistorico documentohistorico){
		try{			
			String nomes = documentoService.doEmCartorio(documentohistorico);
			if (!"".equals(nomes)) {
				nomes = nomes.substring(0, nomes.length()-2);
				request.addError("Conta(s) com id "+nomes+" j� protestadas.");
			} else {
				request.addMessage("Conta(s) protestada(s) com sucesso.");
			}

		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
		}
	}
	
	private ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if (request.getParameter("cddocumento") != null) {
			action += "&cddocumento=" + request.getParameter("cddocumento");
		}
		return new ModelAndView("redirect:"+action);
	}
	
	/**
	 * M�todo que carrega a p�gina para a negocia��o de uma conta a receber, gerando uma ou mais parcelas
	 * @param request
	 * @param acao
	 * @return
	 * @author Thiago Gon�alves
	 */
	public ModelAndView negociar(WebRequestContext request, Documentoacao acao){
		
		if (acao == null || acao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		//Por padr�o a negocia��o � via conta receber. Como a negocia��o de Conta a pagar foi implementado bem ap�s a Conta a receber, existem outros lugares que fazem a chamada de negocia��o da Conta a receber.
		Documentoclasse documentoclasse = NumberUtils.isNumber(request.getParameter("cddocumentoclasse")) ? new Documentoclasse(Integer.valueOf(request.getParameter("cddocumentoclasse"))) : Documentoclasse.OBJ_RECEBER;
		
		boolean fromVenda = false;
		String paramBackAction = request.getParameter("backAction");
		if(paramBackAction != null && paramBackAction.trim().toUpperCase().equals("VENDA")){
			fromVenda = true;
		}
		
		//limita��o de negocia��o quando o documento tem origem de contrato com comissionamento de valor bruto considerando valor de pagamento
		if(Documentoclasse.OBJ_RECEBER.equals(documentoclasse) && whereIn.split(",").length > 1 && documentoService.isComissionamentoValorbrutoConsiderandodiferencavalorpago(whereIn)){
			request.addError("N�o � poss�vel negociar mais de uma conta quando o documento tem rela��o com comissionamento de valor bruto considerando a diferen�a do valor pago.");
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		}
		
		List<Documento> contas = contareceberService.findContas(whereIn);
		
		Pessoa pessoa = contas.get(0).getPessoa();
		String outros = contas.get(0).getOutrospagamento();
		
		Set<Taxaitem> listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
		
		List<Conta> listaConta = new ArrayList<Conta>();
		Money valor = new Money(0);
		for (Documento documento : contas) {
			if (Documentoclasse.OBJ_RECEBER.equals(documentoclasse) && !fromVenda && !documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA) && !documento.getDocumentoacao().equals(Documentoacao.BAIXADA_PARCIAL)){				
				request.addError("Apenas podem ser negociadas contas com situa��o Definitiva ou Baixada parcial.");
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
				return null;				
			}else if (Documentoclasse.OBJ_PAGAR.equals(documentoclasse) && !documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)){				
				request.addError("Apenas podem ser negociadas contas com situa��o Definitiva.");
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
				return null;				
			}
			
			if(pessoa != null && pessoa.getCdpessoa() != null){
				if (pessoa.getCdpessoa() != documento.getPessoa().getCdpessoa()){
					request.addError("Apenas contas de um mesmo cliente/fornecedor/colaborador podem ser negociadas ao mesmo tempo.");
					request.getServletResponse().setContentType("text/html");
					View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
					return null;
				}
			} else {
				if(documento.getPessoa() != null && documento.getPessoa().getCdpessoa() != null){
					request.addError("Apenas contas de um mesmo cliente/fornecedor/colaborador podem ser negociadas ao mesmo tempo.");
					request.getServletResponse().setContentType("text/html");
					View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
					return null;
				}
			}
				
			if(outros != null && !outros.equals("")){
				if (!outros.equals(documento.getOutrospagamento())){
					request.addError("Apenas contas de um mesmo cliente/fornecedor/colaborador podem ser negociadas ao mesmo tempo.");
					request.getServletResponse().setContentType("text/html");
					View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
					return null;
				}
			} else {
				if(documento.getOutrospagamento() != null && !documento.getOutrospagamento().equals("")){
					request.addError("Apenas contas de um mesmo cliente/fornecedor/colaborador podem ser negociadas ao mesmo tempo.");
					request.getServletResponse().setContentType("text/html");
					View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
					return null;
				}
			}
			
			if (Documentoacao.BAIXADA_PARCIAL.equals(documento.getDocumentoacao())){
				valor = valor.add(movimentacaoService.calcularMovimentacoesByDocumento(documento, new Date(System.currentTimeMillis())));
			}else {
				valor = valor.add(documento.getValor());
			}
			
			if(documento.getConta() != null && !listaConta.contains(documento.getConta())){
				listaConta.add(documento.getConta());
			}
		}		
		
		NegociacaoBean negociacaoBean = new NegociacaoBean();
		negociacaoBean.setDocumentos(whereIn);
		negociacaoBean.setValor(valor);
		negociacaoBean.setDtreferencia(new Date(System.currentTimeMillis()));
		negociacaoBean.setFromVenda(fromVenda);
		negociacaoBean.setDocumentoclasse(documentoclasse);
		
		if(listaConta.size() == 1){
			Conta conta = listaConta.get(0);
			negociacaoBean.setConta(conta);
			if(conta.getTaxa() != null && SinedUtil.isListNotEmpty(conta.getTaxa().getListaTaxaitem())){
				Taxaitem taxaitemNovo;
				for(Taxaitem taxaitem : conta.getTaxa().getListaTaxaitem()){
					if(taxaitem.getDias() != null && negociacaoBean.getDtreferencia() != null){
						if(taxaitem.getTipocalculodias() != null && taxaitem.getTipocalculodias().equals(Tipocalculodias.ANTES_VENCIMENTO)){
							taxaitem.setDtlimite(SinedDateUtils.addDiasData(negociacaoBean.getDtreferencia(), -taxaitem.getDias()));
						}else {
							taxaitem.setDtlimite(SinedDateUtils.addDiasData(negociacaoBean.getDtreferencia(), taxaitem.getDias()));
						}
						taxaitemNovo = new Taxaitem(taxaitem);
						taxaitemNovo.setDias(taxaitem.getDias());
						listaTaxaitem.add(taxaitemNovo);
					}
				}
			}
		}
		if(listaTaxaitem.size() > 0){
			negociacaoBean.setChecktaxa(Boolean.TRUE);
			negociacaoBean.setTaxa(new Taxa());
			negociacaoBean.getTaxa().setListaTaxaitem(listaTaxaitem);
		}
		
		List<Documentoacao> listaAcao = new ArrayList<Documentoacao>();
		listaAcao.add(new Documentoacao(Documentoacao.DEFINITIVA.getCddocumentoacao(),"DEFINITIVA"));
		listaAcao.add(new Documentoacao(Documentoacao.PREVISTA.getCddocumentoacao(),"PREVISTA"));
		request.setAttribute("listaStatus", listaAcao);
		
		return new ModelAndView("direct:/process/popup/negociacaoContaReceber")
								.addObject("negociacaoBean", negociacaoBean);
	}
	
	/**
	 * M�todo para salvar as parcelas geradas na negocia��o de uma conta a receber
	 * @param request
	 * @param negociacaoBean
	 * @author Thiago Gon�alves
	 */
	@Action("saveNegociacao")
	public void saveNegociacao(WebRequestContext request, NegociacaoBean negociacaoBean){
		try{	
			String documentos = negociacaoBean.getDocumentos();
			
			String[] lista = documentos.split(",");
			String observacaoNegociacao = "Referente �(s) negocia��o(�es): ";
			for (String string : lista) {
				if (Documentoclasse.OBJ_RECEBER.equals(negociacaoBean.getDocumentoclasse())){
					observacaoNegociacao += "<a href=\"javascript:visualizaContaReceber(" + string + ");\">" + string +"</a>, ";
				}else if (Documentoclasse.OBJ_PAGAR.equals(negociacaoBean.getDocumentoclasse())){
					observacaoNegociacao += "<a href=\"javascript:visualizaContaPagar(" + string + ");\">" + string +"</a>, ";
				}
			}
			
			if (observacaoNegociacao.endsWith(", ")){
				observacaoNegociacao = observacaoNegociacao.substring(0, observacaoNegociacao.length()-2) + ".";
			}
			
			String observacaoVenda = getDescricaoReferenteVenda(documentos);
			
			if(observacaoNegociacao.length() > 10000){
				observacaoNegociacao = "Referente � negocia��o de " + lista.length + " contas.";
			}
			
			List<Documento> listacontas = contareceberService.findContas(documentos);
			List<Rateioitem> listaRateioitemOriginal = new ArrayList<Rateioitem>();
			Documentonegociadoitem documentonegociadoitem = new Documentonegociadoitem();
			Documentonegociado documentonegociado = new Documentonegociado();
			documentonegociadoService.saveOrUpdate(documentonegociado);
			
			Money valorTotalRateio = new Money();
			for (Documento conta : listacontas) {
				Rateio rateio = rateioService.findByDocumento(conta);			
				List<Rateioitem> listaRateios = rateio.getListaRateioitem();
				for (Rateioitem rateioitem : listaRateios) {
					if(rateioitem.getValor() != null){
						valorTotalRateio = valorTotalRateio.add(rateioitem.getValor());
					}
					listaRateioitemOriginal.add(rateioitem);
				}
				
				contareceberService.atualizaContaNegociada(conta, documentonegociado);
			}
			
			Money cem = new Money(100D);
			if(valorTotalRateio.getValue().doubleValue() > 0){
				for (Rateioitem rateioitem : listaRateioitemOriginal) {
					if(rateioitem.getValor() != null){
						rateioitem.setPercentual(rateioitem.getValor().divide(valorTotalRateio).multiply(cem).getValue().doubleValue());
					}
				}
			}
			
			List<Documento> parcelas = negociacaoBean.getParcelas();
			
			Parcelamento parcelamento = new Parcelamento();
			parcelamento.setPrazopagamento(negociacaoBean.getPrazoescolhido());
			parcelamento.setIteracoes(parcelas.size());
			
			Set<Parcela> listaP = new ListSet<Parcela>(Parcela.class);
			Parcela p;
			
			String negociacoes = "";
			Documento contaoriginal = new Documento();
			contaoriginal.setCddocumento(listacontas.get(0).getCddocumento());
			contaoriginal = contareceberService.loadForEntradaWithDadosPessoa(contaoriginal);			
			
			List<Documentoorigem> listaDocumentoOrigem = documentoorigemService.findByDocumento(documentos);
			List<NotaDocumento> listaNotasDocumento = notaDocumentoService.findByDocumento(documentos);
			
			Date dtvencimentodocPrincipal = parcelas.get(0).getDtvencimento();
			int i = 1;
			for (Documento documento : parcelas) {
				String descricaoParcela = "Parcela de negocia��o da(s) conta(s): " + documentos.replaceAll(",", ", ") + " - " + i + "/" + parcelas.size();
				if(observacaoVenda != null && !"".equals(observacaoVenda)){
					descricaoParcela += " Venda (" + observacaoVenda + ") ";
				}
				if(descricaoParcela.length() > 150){
					descricaoParcela = "Parcela de negocia��o de " + listacontas.size() + " contas. - " + i + "/" + parcelas.size();
				}
				
				documento.setEmpresa(contaoriginal.getEmpresa());
				documento.setDocumentotipo(contaoriginal.getDocumentotipo());
				documento.setPessoa(contaoriginal.getPessoa());
				documento.setTipopagamento(contaoriginal.getTipopagamento());
				documento.setDescricao(descricaoParcela);
				documento.setDtemissao(negociacaoBean.getDtemissaoatual() ? SinedDateUtils.currentDate() : contaoriginal.getDtemissao());
				documento.setReferencia(contaoriginal.getReferencia());	
				documento.setConta(negociacaoBean.getConta());
				//Preenche a carteira do documento com a carteira padr�o
				documento.setContacarteira(negociacaoBean.getContacarteira());
				documento.setIndicecorrecao(contaoriginal.getIndicecorrecao());
				documento.setEndereco(contaoriginal.getEndereco());
				documento.setMensagem1(contaoriginal.getMensagem1());
				documento.setMensagem4(contaoriginal.getMensagem4());
				documento.setMensagem5(contaoriginal.getMensagem5());
				documento.setMensagem6(contaoriginal.getMensagem6());
				
				documento.setTaxa(negociacaoBean.getTaxa());
				documento.setMensagem2(negociacaoBean.getMensagem2()); 
				documento.setMensagem3(negociacaoBean.getMensagem3());
				
				Rateio rateio = new Rateio();
				List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
				rateio.setListaRateioitem(listaRateioitem);
				Double totalRateioItem = 0d;
				for (Rateioitem rateioitemoriginal : listaRateioitemOriginal) {
					Rateioitem rateioitem = new Rateioitem();
					rateioitem.setProjeto(rateioitemoriginal.getProjeto());
					rateioitem.setCentrocusto(rateioitemoriginal.getCentrocusto());
					rateioitem.setContagerencial(rateioitemoriginal.getContagerencial());
					rateioitem.setPercentual(rateioitemoriginal.getPercentual());
					rateioitem.setValor(documento.getValor().multiply(new Money(rateioitem.getPercentual())).divide(cem));
					rateioitem.setRateio(rateio);
					listaRateioitem.add(rateioitem);
					
					totalRateioItem += SinedUtil.round(rateioitem.getValor().getValue().doubleValue(), 10);
				}
				
				Double diferenca = documento.getValor().subtract(new Money(SinedUtil.round(totalRateioItem, 2))).getValue().doubleValue();
				if(diferenca != 0){
					Rateioitem rateioitem = listaRateioitem.get(0);
					rateioitem.setValor(rateioitem.getValor().add(new Money(diferenca)));
					double percentualAux = rateioitem.getValor().getValue().doubleValue()/documento.getValor().getValue().doubleValue() * 100d;
					percentualAux = SinedUtil.round(percentualAux, 10);
					rateioitem.setPercentual(percentualAux);
				}
				
				rateioitemService.agruparItensRateio(listaRateioitem);
				documento.setRateio(rateio);
				
				Set<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class); 
				Documentohistorico documentohistorico = new Documentohistorico(documento);				
				documentohistorico.setObservacao(observacaoNegociacao);				
				listaDocumentohistorico.add(documentohistorico);				
				documento.setListaDocumentohistorico(listaDocumentohistorico);
				
				documento.setTaxa(documentoService.arrumaTaxasDocumento(negociacaoBean.getTaxa(), SinedDateUtils.diferencaDias(documento.getDtvencimento(), dtvencimentodocPrincipal), documento.getDtemissao(), documento));
				
				if (Documentoclasse.OBJ_RECEBER.equals(negociacaoBean.getDocumentoclasse())){
					contareceberService.saveOrUpdate(documento);
				}else if (Documentoclasse.OBJ_PAGAR.equals(negociacaoBean.getDocumentoclasse())){
					contapagarService.saveOrUpdate(documento);
				}
				
				
				if(documento.getTaxa() != null && documento.getTaxa().getListaTaxaitem() != null){
					String msg = "";
					StringBuilder msg2 = new StringBuilder();
					StringBuilder msg3 = new StringBuilder();
					String preposicao = "";
					for(Taxaitem txitem : documento.getTaxa().getListaTaxaitem()){
						if(txitem.getGerarmensagem() != null && txitem.getGerarmensagem()){
							if(tipotaxaService.isApos(txitem.getTipotaxa())) {
								preposicao = " ap�s ";
							}else {
								preposicao = " at� ";
							}
							if(txitem.getTipotaxa().getNome() == null){
								txitem.setTipotaxa(tipotaxaService.getTipoTaxa(txitem.getTipotaxa()));
							}
							msg = txitem.getTipotaxa().getNome() + " de " +(txitem.isPercentual() ? "" : "R$") + txitem.getValor() + (txitem.isPercentual() ? "%" : "") + (txitem.getDtlimite() != null ? preposicao + SinedDateUtils.toString(txitem.getDtlimite()) : "") + ". ";
							if((msg2.length() + msg.length()) <= 80){
								msg2.append(msg);
							}else if((msg3.length() + msg.length()) <= 80){
								msg3.append(msg);
							}
						}
					}
					if(!msg2.toString().equals("")){
						documento.setMensagem2(msg2.toString());
					}
					if(!msg3.toString().equals("")){
						documento.setMensagem3(msg3.toString());
					}
				}
				
				documentonegociadoitem = new Documentonegociadoitem();
				documentonegociadoitem.setDocumentonegociado(documentonegociado);
				documentonegociadoitem.setDocumento(documento);
				documentonegociadoitemService.saveOrUpdate(documentonegociadoitem);
				
				if(listaDocumentoOrigem != null && !listaDocumentoOrigem.isEmpty()){
					for (Documentoorigem documentoorigem : listaDocumentoOrigem) {
						Documentoorigem documentoorigemAux = new Documentoorigem(documento, documentoorigem.getEntrega(), documentoorigem.getFornecimento(), documentoorigem.getVeiculodespesa(), documentoorigem.getDespesaviagemadiantamento(), documentoorigem.getDespesaviagemacerto(), documentoorigem.getVenda(), documentoorigem.getContrato(), documento.getAgendamentoservicodocumento());
						documentoorigemService.saveOrUpdate(documentoorigemAux);
					}
				}
				if(listaNotasDocumento != null && !listaNotasDocumento.isEmpty()){
					for (NotaDocumento notaDocumento : listaNotasDocumento) {
						NotaDocumento notaDocumentoAux = new NotaDocumento(documento, notaDocumento.getNota(), notaDocumento.getOrdem(), notaDocumento.getFromwebservice());
						notaDocumentoService.saveOrUpdate(notaDocumentoAux);
					}
				}
				
				p = new Parcela();
				p.setDocumento(documento);
				p.setOrdem(i);
				i++;
				listaP.add(p);
				
				negociacoes += documento.getCddocumento().toString() + ",";
			}
			
			parcelamento.setListaParcela(listaP);
			parcelamentoService.saveOrUpdate(parcelamento);
			
			lista = negociacoes.split(",");
			observacaoNegociacao = "Parcelas geradas na negocia��o: ";
			for (String string : lista) {
				if (Documentoclasse.OBJ_RECEBER.equals(negociacaoBean.getDocumentoclasse())){
					observacaoNegociacao += "<a href=\"javascript:visualizaContaReceber(" + string + ");\">" + string +"</a>, ";
				}else if (Documentoclasse.OBJ_PAGAR.equals(negociacaoBean.getDocumentoclasse())){
					observacaoNegociacao += "<a href=\"javascript:visualizaContaPagar(" + string + ");\">" + string +"</a>, ";
				}
			}
			
			if (observacaoNegociacao.endsWith(", ")){
				observacaoNegociacao = observacaoNegociacao.substring(0, observacaoNegociacao.length()-2) + ".";
			}
			
			if(observacaoNegociacao.length() > 10000){
				observacaoNegociacao = "Foram geradas " + lista.length + " parcelas na negocia��o.";
			}
			
			Documentohistorico documentohistorico = new Documentohistorico();
			documentohistorico.setIds(documentos);
			documentohistorico.setDocumentoacao(Documentoacao.NEGOCIADA);
			documentohistorico.setObservacao(observacaoNegociacao);
			documentoService.doCancelar(documentohistorico);			
			
			request.addMessage("Negocia��o realizada com sucesso.");
			
		} catch (Exception e) {
			request.addError("A negocia��o n�o foi realizada. " + e.getMessage());
		} finally {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
		}
	}
	
	/**
	 * M�todo que retorna a descri��o com o cdvenda
	 *
	 * @param documentos
	 * @return
	 * @author Luiz Fernando
	 */
	private String getDescricaoReferenteVenda(String documentos) {
		StringBuilder s = new StringBuilder();
		if(documentos != null && !"".equals(documentos)){
			List<Documento> lista = contareceberService.findContasOrigemVendas(documentos);
			HashMap<Integer, Integer> cdvendas = new HashMap<Integer, Integer>();
			if(lista != null && !lista.isEmpty()){
				for(Documento d : lista){
					if(d.getListaDocumentoOrigem() != null && !d.getListaDocumentoOrigem().isEmpty()){
						for(Documentoorigem doco : d.getListaDocumentoOrigem()){
							if(doco.getVenda() != null && doco.getVenda().getCdvenda() != null){
								cdvendas.put(doco.getVenda().getCdvenda(), doco.getVenda().getCdvenda());
							}
						}
					}
				}
			}
			
			for(Integer cdvenda : cdvendas.keySet()){
				if(!"".equals(s.toString())) s.append(",");
				s.append(cdvenda);
			}
		}
		
		return s.toString();
	}
	/**
	 * Gera a quantidade de linhas de pagamento de acordo a quantidade de parcelas 
	 * @see br.com.linkcom.sined.geral.service.prazopagamentoitemService#countParcelas
	 * @param request
	 * @param venda
	 * @author Ramon Brazil
	 */
	public ModelAndView parcelasnegociacao(WebRequestContext request, NegociacaoBean negociacaoBean){	
		
		Indicecorrecao indicecorrecao = negociacaoBean.getIndicecorrecao();
		List<Prazopagamentoitem> listaPagItem = prazopagamentoitemService.findByPrazo(negociacaoBean.getPrazopagamento());
		Double juros = prazopagamentoService.loadAll(negociacaoBean.getPrazopagamento()).getJuros();
		Date dtreferencia = negociacaoBean.getDtreferencia();
		
		AjaxRealizarVendaPagamentoBean bean = new AjaxRealizarVendaPagamentoBean();
		bean.setParcela(listaPagItem.size());
		
		AjaxRealizarVendaPagamentoItemBean item;
		Money valor;
		Date dtparcela;
		Date dtPrimeiraParcela = null;
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		List<AjaxRealizarVendaPagamentoItemBean> lista = new ArrayList<AjaxRealizarVendaPagamentoItemBean>();
		
		boolean verificarDiferenca = true;
		Money valorTotal = new Money();
		for (Prazopagamentoitem it : listaPagItem) {
			item = new AjaxRealizarVendaPagamentoItemBean();
			
			if(it.getDias() != null && it.getDias() > 0){
				dtparcela = SinedDateUtils.incrementDate(dtreferencia, it.getDias().intValue(), Calendar.DAY_OF_MONTH);
			} else if(it.getMeses() != null && it.getMeses() > 0){
				dtparcela = SinedDateUtils.incrementDate(dtreferencia, it.getMeses().intValue(), Calendar.MONTH);
			} else {
				dtparcela = dtreferencia;
			}
			item.setDtparcela(formatador.format(dtparcela));
			
			if(dtPrimeiraParcela == null){
				dtPrimeiraParcela = dtparcela;
			}
			
			if(juros == null || juros.equals(0d)){			  
				valor = negociacaoBean.getValor().divide(new Money(bean.getParcela())); 
			}else{
				verificarDiferenca = false;
				valor = calculaJuros(negociacaoBean.getValor(), bean.getParcela(), juros);
			}
			
			if (indicecorrecao != null){
				verificarDiferenca = false;
				valor = indicecorrecaoService.calculaIndiceCorrecao(indicecorrecao, valor, dtparcela, dtreferencia);
			}
			
			item.setValor(valor.toString());
			
			valorTotal = valorTotal.add(new Money(new Double(valor.toString().replaceAll("\\.", "").replace(",", "."))));
			
			lista.add(item);
		}
		Boolean permiteAlterarValor = SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR");
		
		bean.setLista(lista);
		
		if(verificarDiferenca && negociacaoBean.getValor() != null && negociacaoBean.getValor().getValue().doubleValue() > 0 && valorTotal != null && valorTotal.getValue().doubleValue() > 0){
			Double diferenca = negociacaoBean.getValor().subtract(valorTotal).getValue().doubleValue();
			if(diferenca >= -0.05 && diferenca <= 0.05){
				AjaxRealizarVendaPagamentoItemBean ultimoItem = lista.get(lista.size()-1);
				ultimoItem.setValor(new Money(new Double(ultimoItem.getValor().toString().replaceAll("\\.", "").replace(",", "."))).add(new Money(diferenca)).toString());
			}
		}
		
		Set<Taxaitem> listaTaxaitemTela = null;
		if(negociacaoBean.getConta() != null && negociacaoBean.getTaxa() != null && SinedUtil.isListNotEmpty(negociacaoBean.getTaxa().getListaTaxaitem()) &&
				dtPrimeiraParcela != null){
			Set<Taxaitem> listaTaxaitem = contacorrenteService.getTaxas(negociacaoBean.getConta(), dtPrimeiraParcela, null);
			if(SinedUtil.isListNotEmpty(listaTaxaitem) && listaTaxaitem.size() == negociacaoBean.getTaxa().getListaTaxaitem().size()){
				for(Taxaitem taxaitem : listaTaxaitem){
					if(taxaitem.getTipotaxa() != null){
						for(Taxaitem taxaitemTela : negociacaoBean.getTaxa().getListaTaxaitem()){
							if(taxaitem.getTipotaxa().equals(taxaitemTela.getTipotaxa())){
								taxaitemTela.setDtlimite(taxaitem.getDtlimite());
								break;
							}
						}
					}
				}
				listaTaxaitemTela = negociacaoBean.getTaxa().getListaTaxaitem();
			}
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", bean);
		json.addObject("permiteAlterarValor", permiteAlterarValor);
		json.addObject("listaTaxaitemConta", listaTaxaitemTela);
		
		return json;
	}
	
	public static Money calculaJuros(Money totalvenda, Integer parcela, Double juros){
		Double total = totalvenda.getValue().doubleValue();
		
		juros = juros/100.00; 
		Double valor = total*juros*Math.pow((1+juros),parcela)/(Math.pow((1+juros),parcela)-1);
		
		Money resultado = new Money(valor);
		return resultado;
	}
	
	/**
	 * M�todo que envia boleto para o cliente
	 * 
	 * @param request
	 * @param documento
	 * @return
	 * @Auhtor Tom�s Rabelo
	 */
	public ModelAndView enviaBoleto(WebRequestContext request, Documento documento){
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			request.addError("Nenhum item selecionado.");
			return getControllerModelAndView(request);
		}		
		
		List<String> listaErro = documentoService.verificaSituacaoDocumento(whereIn, true);
		
		if(listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			request.addError("Erro na gera��o de boleto.");
			return getControllerModelAndView(request);
		}
		
		List<Documento> listaDocumento = documentoService.findForBoleto(whereIn);
		int sucesso = 0;
		int erro = 0; 
		for (Documento documentoAux : listaDocumento) {
			try{
				if(documentoAux.getContacarteira() != null) {
					Boolean contaHomologada = documentoAux.getContacarteira().getAprovadoproducao();
					Boolean flagNaoGerarBoleto = documentoAux.getContacarteira().getNaogerarboleto();
				
					if(contaHomologada != null && contaHomologada && (flagNaoGerarBoleto == null || !flagNaoGerarBoleto)) {
						List<Contato> listaContato = contatoService.findByPessoa(documentoAux.getPessoa());
						Report report = new Report("/financeiro/relBoleto");
						report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(documentoAux, false)});
						Boolean enviouContato = false;
						Boolean enviouCliente = false;
						String paraEnvioEmailCliente = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.ENVIOFINANCEIROMAILPRINCIPALCLIENTE);
						
						if (!paraEnvioEmailCliente.equals("FALSE") && documentoAux.getPessoa() != null &&
								StringUtils.isNotEmpty(documentoAux.getPessoa().getEmail())){
							try {
								contareceberService.enviaBoleto(documentoAux, report, null, null, null, null);
								enviouCliente = true;
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro no envio do boleto para " + documentoAux.getPessoa().getEmail() + ": " + e.getMessage());
								erro++;
							}
						}
						
						if(SinedUtil.isListNotEmpty(listaContato)){
							for (Contato contato : listaContato) {
								if(contato.getEmailcontato() != null && 
										!contato.getEmailcontato().equals("") && 
										contato.getReceberboleto() != null &&
										contato.getReceberboleto()){
									try {
										contareceberService.enviaBoleto(documentoAux, report, null, null, contato, null);
										enviouContato = true;
									} catch (Exception e) {
										e.printStackTrace();
										listaErro.add("Erro no envio do boleto para " + contato.getEmailcontato() + ": " + e.getMessage());
										erro++;
									}
								}		
							}
						}
						if(enviouCliente || enviouContato){
							documentoAux.setDocumentoacao(Documentoacao.ENVIO_BOLETO);
							
							if (!documentohistoricoService.possuiBoletoEnviado(documentoAux)){
								documentoAux.setObservacaoHistorico("Boleto enviado pela primeira vez");
							}
							documentohistoricoService.saveOrUpdate(new Documentohistorico(documentoAux));
							sucesso++;
						}				
					} else if((contaHomologada == null || !contaHomologada) && (flagNaoGerarBoleto == null || !flagNaoGerarBoleto)) {
						listaErro.add("N�o foi poss�vel enviar o boleto porque ele n�o est� aprovado");
						erro++;
					}
				}
						
			} catch (Exception e) {
				e.printStackTrace();
				listaErro.add("Erro na gera��o do boleto da conta " + documentoAux.getCddocumento() + ": " + e.getMessage());
				erro++;
			}
		}
		
		if(sucesso > 0){
			request.addMessage(sucesso + " boleto(s) enviado(s) com sucesso.");
		}
		if(erro > 0){
			request.addError(erro + " boleto(s) n�o enviado(s). ");
			if(listaErro != null && !listaErro.isEmpty()){
				for(String msgErro : listaErro){
					request.addError(msgErro);
				}
			}
		}
		
		return getControllerModelAndView(request);
	}
	
	/**
	 * M�todo que registra envio de boleto que n�o seja por e-mail.
	 * @param request
	 * @param documento
	 * @return
	 * @author Taidson
	 * @since 04/01/2011
	 */
	public ModelAndView registrarEnvioBoleto(WebRequestContext request, Documento documento){
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			request.addError("Nenhum item selecionado.");
			return getControllerModelAndView(request);
		}
		
//		//Verifica��o de data limite do ultimo fechamento. 
//		Documento aux = new Documento();
//		aux.setWhereIn(whereIn);
//		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
//			request.addError("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
//			return getControllerModelAndView(request);
//		}
		
		if(documentoService.isDocumentoNotDefitivaNotPrevista(whereIn)){
			request.addError("S� pode imprimir boleto de uma conta a receber que estiver na situa��o prevista ou definitiva.");
			return getControllerModelAndView(request);
		}
		
		List<Documento> listaDocumento = documentoService.findForBoleto(whereIn);
		
		for (Documento documentoAux : listaDocumento) {
			if(documentoAux.getConta() == null){
				request.addError("A conta a receber "+documentoAux.getCddocumento()+" n�o possui conta banc�ria cadastrada.");
				return getControllerModelAndView(request);
			} else if(documentoAux.getPessoa() == null){
				request.addError("A conta a receber "+documentoAux.getCddocumento()+" n�o possui cliente ou colaborador cadastrado.");
				return getControllerModelAndView(request);
			} else if(documentoAux.getPessoa().getEmail() == null || documentoAux.getPessoa().getEmail().equals("")){
				request.addError("O cliente "+documentoAux.getPessoa().getNome()+" n�o possui e-mail cadastrado.");
				return getControllerModelAndView(request);
			}
		}
		
		for (Documento documentoAux : listaDocumento) {
			documentoAux.setDocumentoacao(Documentoacao.ENVIO_MANUAL_BOLETO);
			documentohistoricoService.saveOrUpdate(new Documentohistorico(documentoAux));
		}
		
		request.addMessage("Envio manual de boleto(s) registrado.", MessageType.INFO); 
		return getControllerModelAndView(request);
	}
	
	/**
	 * Abre o popup e apresenta nas notas dispon�veis ao processo
	 * de associa��o com conta a receber.
	 * @param request
	 * @return
	 * @author Taidson
	 * @since 15/12/2010
	 */
	public ModelAndView associarNota(WebRequestContext request){
		String selectedItens = SinedUtil.getItensSelecionados(request);
		
		if(selectedItens == null || selectedItens.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(selectedItens);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			SinedUtil.fechaPopUp(request);
			request.addError("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
			return null;
		}
		
		if(notaDocumentoService.existeContaAssociadaNota(selectedItens)){
			SinedUtil.fechaPopUp(request);
			request.addError("Existe(m) conta(s) j� associada(s) a uma nota.");
			return null;
		}
		
//		if(fechamentofinanceiroService.possuiClienteDiferente(aux)){
//			SinedUtil.fechaPopUp(request);
//			request.addError("S� � permitido associar nota de contas a receber de um mesmo cliente.");
//			return null;
//		}
	
		NotaFiscalServico notaFiscalServico = new NotaFiscalServico();
		notaFiscalServico.setDtinicio(SinedDateUtils.firstDateOfMonth());
		String[] arrayIdConta = selectedItens.split(",");
		for(String idconta : arrayIdConta){
			if(!contareceberService.verficaContaBaixadaDefinitiva(Integer.valueOf(idconta))){
				SinedUtil.fechaPopUp(request);
				request.addError("S� � permitido associar conta que estiver na situa��o baixada ou definitiva.");
				return null;
			}
			if(notaFiscalServico.getCliente() == null){
				Documento documento = documentoService.load(new Documento(Integer.parseInt(idconta)), "documento.cddocumento, documento.pessoa");
				if(documento != null && documento.getPessoa() != null){
					notaFiscalServico.setCliente(new Cliente(documento.getPessoa().getNome()));
				}
			}
		}
		
		List<NotaStatus> listaSituacao = new ArrayList<NotaStatus>();
		listaSituacao.add(NotaStatus.EMITIDA);
		listaSituacao.add(NotaStatus.NFSE_EMITIDA);
		notaFiscalServico.setListaSituacao(listaSituacao);
		
		List<NotaStatus> listaSituacaoFiltro = new ArrayList<NotaStatus>();
		listaSituacaoFiltro.add(NotaStatus.EMITIDA);
		listaSituacaoFiltro.add(NotaStatus.LIQUIDADA);
		listaSituacaoFiltro.add(NotaStatus.NFSE_EMITIDA);
		listaSituacaoFiltro.add(NotaStatus.NFSE_LIQUIDADA);

		request.setAttribute("titulo", "ASSOCIAR A UMA NOTA DE SERVI�O");
		request.setAttribute("entrada", request.getParameter("entrada"));
		request.setAttribute("selectedItens", selectedItens);
		request.setAttribute("listaSituacao", listaSituacaoFiltro);
	
		return new ModelAndView("direct:/process/popup/associarNota", "bean", notaFiscalServico);
		
	}
	
	public ModelAndView listar(WebRequestContext request, AssociarNotaBean bean) {
		String selectedItens = SinedUtil.getItensSelecionados(request);
		List<NotaFiscalServico> listaNotas = notaFiscalServicoService.loadNotasServicoAssociar(selectedItens, bean.getDtinicio(), bean.getDtfim(), bean.getListaNotaStatus());
		
		selectedItens = selectedItens.replace(",", "|");
		if(listaNotas != null && listaNotas.size() > 0){
			for (NotaFiscalServico item : listaNotas) {
				item.setWhereInCddocumento(selectedItens);
			}
		}
		return new ModelAndView("direct:process/popup/associarNotaListagem", "listaNotas", listaNotas);
	}
	
	/**
	 * Gera um registro em documento nota e um hist�rico para conta e para a nota
	 * @param request
	 * @return
	 * @author Taidson
	 * @since 15/12/2010
	 */
	public ModelAndView saveAssociarNota(WebRequestContext request){
    	try{
    		String ids = request.getParameter("itensSelecionados");
    		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
    		
    		String idnota = StringUtils.substringBefore(ids, "=");
    		String whereInCddocumento = StringUtils.substringAfter(ids, "=");
    		String[] arrayCddocumento = whereInCddocumento.split("\\|");
    		
    		Nota nota = new Nota();
    		nota.setCdNota(Integer.valueOf(idnota));
    		
    		StringBuilder linkobservacao = new StringBuilder();
    		Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
    		for(String idconta : arrayCddocumento){	
	    		Documento documento = new Documento();
	    		documento.setCddocumento(Integer.valueOf(idconta));
	    		
	    		NotaDocumento notaDocumento = new NotaDocumento();
	    		notaDocumento.setDocumento(documento);
	    		notaDocumento.setNota(nota);
	    		notaDocumento.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
	    		notaDocumento.setDtaltera(dtaltera);
	    		notaDocumentoService.saveOrUpdate(notaDocumento);
	    		
	    		Documentohistorico documentohistorico = new Documentohistorico();
	    		documentohistorico.setDocumento(documento);
	    		documentohistorico.setDtaltera(notaDocumento.getDtaltera());
	    		documentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
	    		documentohistorico.setDocumentoacao(Documentoacao.ASSOCIADA);
	    		documentohistorico.setObservacao("Vinculado a nota <a href=\"javascript:visualizarNotaFiscalServico("+nota.getCdNota()+")\">"+nota.getCdNota()+"</a>.");
	    		documentohistoricoService.saveOrUpdate(documentohistorico); 
	    		
	    		if(!"".equals(linkobservacao.toString())) linkobservacao.append(" , ");
	    		linkobservacao.append("<a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>");
    		}
    		
    		NotaFiscalServico notaFiscalServico = new NotaFiscalServico();
    		notaFiscalServico.setCdNota(nota.getCdNota());
    		notaFiscalServico = notaFiscalServicoService.loadForEntrada(notaFiscalServico);
    		
    		notaService.liquidarNota(notaFiscalServico);
    		
    		NotaHistorico notaHistorico = new NotaHistorico();
    		notaHistorico.setNota(nota);
    		notaHistorico.setDtaltera(dtaltera);
    		notaHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
    		notaHistorico.setNotaStatus(notaFiscalServico.getNotaStatus());
    		if(arrayCddocumento.length > 1){
    			notaHistorico.setObservacao("Vinculado as contas � receber " + linkobservacao.toString() + ".");
    		}else {
    			notaHistorico.setObservacao("Vinculado a conta receber <a href=\"javascript:visualizarContaReceber("+arrayCddocumento[0]+")\">"+arrayCddocumento[0]+"</a>.");
    		}
    		notaHistoricoService.saveOrUpdate(notaHistorico);
    		
    		request.getServletResponse().setContentType("text/html");
    		if(entrada)
        		View.getCurrent().println("<script>if(parent.location.href.indexOf('cddocumento') == -1){parent.location = parent.location+'?ACAO=consultar&cddocumento="+arrayCddocumento[0]+"';} else{parent.location = parent.location;} parent.$.akModalRemove(true);</script>");
        	else
        		View.getCurrent().println("<script>parent.location = parent.location;parent.$.akModalRemove(true);</script>");
    		
			request.addMessage("Associa��o realizada com sucesso.");
			return null;
			
    	} catch (Exception e) {
    		e.printStackTrace();
    		SinedUtil.fechaPopUp(request);
    		request.addError("N�o foi poss�vel associar a conta com a nota fiscal de servi�o.");
    		return null;
    	} finally {
			SinedUtil.fechaPopUp(request);
		}
	}
	
	/**
	 * Gera a quantidade de linhas de pagamento de acordo a quantidade de parcelas 
	 * @see br.com.linkcom.sined.geral.service.prazopagamentoitemService#countParcelas
	 * @param request
	 * @param venda
	 * @author Ramon Brazil
	 */
	public ModelAndView parcelasAutorizacao(WebRequestContext request, Documentohistorico documentohistorico){	
		
		List<Documento> listaConta = contapagarService.findContas(documentohistorico.getIds());
		Money valorDocumentos = new Money(0);
		for (Documento doc : listaConta){
			valorDocumentos = valorDocumentos.add(doc.getValor());
		}
		
		List<Prazopagamentoitem> listaPagItem = prazopagamentoitemService.findByPrazo(documentohistorico.getPrazopagamento());
		Double juros = prazopagamentoService.loadAll(documentohistorico.getPrazopagamento()).getJuros();
		Date dtreferencia = documentohistorico.getDtreferencia();
		
		AjaxRealizarVendaPagamentoBean bean = new AjaxRealizarVendaPagamentoBean();
		bean.setParcela(listaPagItem.size());
		
		AjaxRealizarVendaPagamentoItemBean item;
		Money valorTotal = valorDocumentos.subtract(documentohistorico.getValor());
		Money valor;
		Date dtparcela;
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		List<AjaxRealizarVendaPagamentoItemBean> lista = new ArrayList<AjaxRealizarVendaPagamentoItemBean>();
		
		for (Prazopagamentoitem it : listaPagItem) {
			item = new AjaxRealizarVendaPagamentoItemBean();
			
			if(it.getDias() != null && it.getDias() > 0){
				dtparcela = SinedDateUtils.incrementDate(dtreferencia, it.getDias().intValue(), Calendar.DAY_OF_MONTH);
			} else if(it.getMeses() != null && it.getMeses() > 0){
				dtparcela = SinedDateUtils.incrementDate(dtreferencia, it.getMeses().intValue(), Calendar.MONTH);
			} else {
				dtparcela = dtreferencia;
			}
			item.setDtparcela(formatador.format(dtparcela));
			
			if(juros == null || juros.equals(0d)){			  
				valor = valorTotal.divide(new Money(bean.getParcela())); 
			}else{
				valor = calculaJuros(valorTotal, bean.getParcela(), juros);
			}
			
//			if (indicecorrecao != null)
//				valor = indicecorrecaoService.calculaIndiceCorrecao(indicecorrecao, valor, dtparcela, dtreferencia);
			
			item.setValor(valor.toString());
			
			lista.add(item);
		}
		
		bean.setLista(lista);
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", bean);
		
		return json;
	}
	
	/**
	 * 
	 * M�todo que cria as contas a pagar automaticamente quando uma conta a pagar � aprovada parcialmente.
	 *
	 * @param documentohistorico
	 * @return
	 * @author Thiago Augusto
	 * @param listaConta 
	 * @param proporcaoPercentual 
	 * @date 21/06/2012
	 */
	public void criaNovasContasAPagar(Documentohistorico documentohistorico, List<Documento> listaConta){
		
		Documento beanOrigem = contapagarService.loadForEntradaWtihDadosPessoa(listaConta.get(0));

		String observacaoConta = "Conta a pagar gerada a partir da(s) conta(s) autorizada(s) parcialmente: ";
		for (Documento doc : listaConta) {
			observacaoConta += " <a href=\"javascript:visualizaConta(" + doc.getCddocumento() + ");\">" + doc.getCddocumento() +"</a> ";
		}
		
		if(observacaoConta.length() > 10000){
			observacaoConta = "Referente � autoriza��o parcial de " + listaConta.size() + " contas.";
		}
		
		List<Rateioitem> listaRateioitemOrigem = calculaRateioItem(listaConta);

		List<String> listaIds = new ArrayList<String>();
		
		List<Documentoorigem> listaDocumentoorigem = documentoorigemService.findByDocumento(CollectionsUtil.listAndConcatenate(listaConta, "cddocumento", ","));
		List<String> ids = new ArrayList<String>();
		Boolean adiantamento = false;
		for (Documentoorigem documentoorigem : listaDocumentoorigem) {
			if(documentoorigem.getDespesaviagemadiantamento() != null && documentoorigem.getDespesaviagemadiantamento().getCddespesaviagem() != null){
				ids.add(documentoorigem.getDespesaviagemadiantamento().getCddespesaviagem().toString());
				adiantamento = true;
			} else if(documentoorigem.getDespesaviagemacerto() != null && documentoorigem.getDespesaviagemacerto().getCddespesaviagem() != null){
				ids.add(documentoorigem.getDespesaviagemacerto().getCddespesaviagem().toString());
			}
		}
		
		for (Documento documento : documentohistorico.getParcelas()) {
			documento.setEmpresa(beanOrigem.getEmpresa());
			documento.setDocumentotipo(beanOrigem.getDocumentotipo());
			documento.setPessoa(beanOrigem.getPessoa());
			documento.setTipopagamento(beanOrigem.getTipopagamento());
			documento.setDescricao(beanOrigem.getDescricao());
			documento.setDtemissao(SinedDateUtils.currentDate());
			documento.setReferencia(beanOrigem.getReferencia());	
			documento.setConta(beanOrigem.getConta());
			documento.setIndicecorrecao(beanOrigem.getIndicecorrecao());
			documento.setEndereco(beanOrigem.getEndereco());
			documento.setMensagem1(beanOrigem.getMensagem1());
			documento.setMensagem2(beanOrigem.getMensagem2());
			documento.setMensagem3(beanOrigem.getMensagem3());
			documento.setMensagem4(beanOrigem.getMensagem4());
			documento.setMensagem5(beanOrigem.getMensagem5());
			documento.setMensagem6(beanOrigem.getMensagem6());

			Rateio rateio = new Rateio();
			documento.setRateio(rateio);
			
			List<Rateioitem> listaRateioItem = new ArrayList<Rateioitem>();
			for (Rateioitem ri : listaRateioitemOrigem) {
				Rateioitem rateioitem = new Rateioitem();
				rateioitem = rateioitemService.calculaPercentualRateioItem(ri, documento.getValor());
				rateioitem.setContagerencial(ri.getContagerencial());
				rateioitem.setCentrocusto(ri.getCentrocusto());
				rateioitem.setProjeto(ri.getProjeto());
				listaRateioItem.add(rateioitem);
			}
			documento.getRateio().setListaRateioitem(listaRateioItem);
			documento.setObservacaoHistorico(observacaoConta);	
			
			if(ids.size() > 0){
				documento.setWhereInDespesaviagem(CollectionsUtil.concatenate(ids, ","));
				documento.setAdiatamentodespesa(adiantamento);
			}

			contapagarService.saveOrUpdate(documento);
			
			listaIds.add("<a href=\"javascript:visualizaConta("+ documento.getCddocumento() +");\">" + documento.getCddocumento() + "</a>");
		}
		
		StringBuilder obs = new StringBuilder();
		
		if(documentohistorico.getObservacao() != null && !documentohistorico.getObservacao().equals(""))
			obs.append(documentohistorico.getObservacao()).append(" ");
		
		obs.append("Conta(s) a pagar gerada(s): ");
		obs.append(CollectionsUtil.concatenate(listaIds, ", "));
		
		documentohistorico.setObservacao(obs.toString());
	}
	
	private List<Rateioitem> calculaRateioItem(List<Documento> listaConta) {
		List<Rateioitem> listaCompleta = new ArrayList<Rateioitem>();

		Money valortotal_antigo = new Money();
		
		//Coleta todos os rateios
		for (Documento doc : listaConta){
			valortotal_antigo = valortotal_antigo.add(doc.getValor());
			
			Rateio rateio = rateioService.findByDocumento(doc);
			rateio.setValor(doc.getValor());
			for (Rateioitem ri : rateio.getListaRateioitem()){
				ri.setRateio(rateio);
			}
			listaCompleta.addAll(rateio.getListaRateioitem());
		}
		
		rateioitemService.agruparItensRateio(listaCompleta);
		rateioService.ajustaPercentualRateioitem(valortotal_antigo, listaCompleta);
		
		return listaCompleta;
	}
	
	/**
	 * M�todo que carrega a p�gina para criar a observa��o do hist�rico do documento.
	 * 
	 * @param request
	 * @param acao
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView retirarCartorio(WebRequestContext request, Documentoacao acao){
		
		if (acao == null || acao.getCddocumentoacao() == null) {
			throw new SinedException("DocumentoAcao n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		}
		
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setIds(whereIn);
		documentohistorico.setDocumentoacao(Documentoacao.RETIRADA_PROTESTO);
		

		request.setAttribute("descricao", "RETIRAR PROTESTO");
		request.setAttribute("msg", "Justificativa para retirar protesto");
		request.setAttribute("saveACAO", "saveRetirarCartorio");
		
		return new ModelAndView("direct:/process/popup/cartorioJustificativa")
								.addObject("documentohistorico", documentohistorico);
	}
	
	/**
	 * M�todo que gera registros na tabela DocumentoHistorico referente as contas selecionadas.
	 * 
	 * @param request
	 * @param acao
	 * @return
	 * @author Rafael Salvio
	 */
	@Action("saveRetirarCartorio")
	public void saveRetirarCartorio(WebRequestContext request, Documentohistorico documentohistorico){
		try{			
			String nomes = documentoService.retirarProtesto(documentohistorico);
			if (!"".equals(nomes)) {
				nomes = nomes.substring(0, nomes.length()-2);
				request.addError("Protesto(s) referente(s) �(s) contas com id "+nomes+" n�o foi(foram) retirado(s).");
			} else {
				request.addMessage("Protesto(s) retirado(s) com sucesso.");
			}

		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
		}
	}
	
	public ModelAndView abrirPopUpRecibo(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		boolean isContaReceber = true;
		String entrada = request.getParameter("ENTRADA");
		String from = request.getParameter("conta");
		if(from != null && from.toUpperCase().equals("PAGAR"))
			isContaReceber = false;
		
		String url = "/financeiro/crud/" + (isContaReceber ? "Contareceber" : "Contapagar");
		if(parametrogeralService.getBoolean(Parametrogeral.RECIBO_APENASBAIXADAS)){
			if(documentoService.validaDocumentoSituacao(whereIn, true, Documentoacao.BAIXADA, Documentoacao.BAIXADA_PARCIAL)){
				request.addError("S� � permitido emitir recibo para contas com as situa��es Baixada ou Baixada Parcialmente.");
				SinedUtil.redirecionamento(request, url);
				return null;
			}
		}
		
		if(isContaReceber && documentoService.validaDocumentoSituacao(whereIn, true, 
													Documentoacao.BAIXADA,
													Documentoacao.BAIXADA_PARCIAL,
													Documentoacao.PREVISTA,
													Documentoacao.DEFINITIVA,
													Documentoacao.AUTORIZADA,
													Documentoacao.AUTORIZADA_PARCIAL)){
			request.addError("S� pode emitir recibo de uma conta a receber que estiver na situa��o diferente de Cancelada ou Negociada.");
			SinedUtil.redirecionamento(request, url);
			return null;
		}
		
		if((!isContaReceber) &&
			documentoService.isDocumentoNotBaixadaOrDefinitiva(whereIn)){
			request.addError("S� pode emitir recibo de uma conta a pagar que estiver diferente da situa��o Cancelada e N�o Autorizada.");
			SinedUtil.redirecionamento(request, url);
			return null;
		}
		
		EmitirreciboBean bean = new EmitirreciboBean();
		bean.setListaDocumento(documentoService.findForRecibo(whereIn));
		
		if(SinedUtil.isListNotEmpty(bean.getListaDocumento())){
			for(Documento documento : bean.getListaDocumento()){
				if(SinedUtil.isListNotEmpty(documento.getListaMovimentacaoOrigem())){
					Set<Movimentacaoorigem> listaMO = new ListSet<Movimentacaoorigem>(Movimentacaoorigem.class);
					for(Movimentacaoorigem mo : documento.getListaMovimentacaoOrigem()){
						if(mo.getMovimentacao() != null && !Movimentacaoacao.CANCELADA.equals(mo.getMovimentacao().getMovimentacaoacao())){
							listaMO.add(mo);
						}
					}
					documento.setListaMovimentacaoOrigem(listaMO);
				}
			}
		}
	
		request.setAttribute("conta", from);
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("ENTRADA", entrada);
		return new ModelAndView("direct:/process/popup/popupEmitirrecibo", "bean", bean);
		
	}
}
