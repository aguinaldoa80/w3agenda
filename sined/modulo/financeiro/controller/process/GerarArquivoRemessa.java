package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.List;

import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaBean;
import br.com.linkcom.lkbanco.filesend.util.EDIBodyRemessa400;
import br.com.linkcom.lkbanco.filesend.util.EDIHeaderRemessa400;
import br.com.linkcom.lkbanco.filesend.util.EDITraillerRemessa400;

public class GerarArquivoRemessa {

	public String gerarArquivoRemessaBradesco(ArquivoRemessaBean bean, List<ArquivoRemessaBean> listaCorpo) throws Exception {
		StringBuffer s = new StringBuffer();

		//Header
		EDIHeaderRemessa400.getLinha(bean,s);
		//Body
		EDIBodyRemessa400.getLinha(listaCorpo,s);		
		//Trailler
		EDITraillerRemessa400.getLinha(bean,s);
		
		return s.toString();
	}
	

}