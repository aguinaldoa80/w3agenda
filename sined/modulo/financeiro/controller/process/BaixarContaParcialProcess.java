package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.AuthorizationManager;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.BancoFormapagamento;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.HistoricoAntecipacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.service.BancoformapagamentoService;
import br.com.linkcom.sined.geral.service.BancotipopagamentoService;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.ContatipoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.HistoricoAntecipacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendapagamentoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.geral.service.TipotaxaService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.geral.service.VendapagamentoService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBeanMovimentacao;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/financeiro/process/BaixarContaParcial", authorizationModule=ProcessAuthorizationModule.class)
public class BaixarContaParcialProcess extends MultiActionController {

	private ContapagarService contapagarService;
	private ContaService contaService;
	private DocumentoService documentoService;
	private TaxaService taxaService;
	private MovimentacaoService movimentacaoService;
	private RateioService rateioService;
	private NotaDocumentoService notaDocumentoService;
	private BancoformapagamentoService bancoformapagamentoService;
	private BancotipopagamentoService bancotipopagamentoService;
	private VendapagamentoService vendapagamentoService;
	private ChequeService chequeService;
	private ContratoService contratoService;
	private ContatipoService contatipoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ParametrogeralService parametrogeralService;
	private ValecompraService valecompraService;
	private EmpresaService empresaService;
	private PedidovendapagamentoService pedidovendapagamentoService;
	private TipotaxaService tipotaxaService;
	private HistoricoAntecipacaoService historicoAntecipacaoService;
	private DocumentohistoricoService documentoHistoricoService;
	
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setJurosService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	public void setBancoformapagamentoService(BancoformapagamentoService bancoformapagamentoService) {this.bancoformapagamentoService = bancoformapagamentoService;}
	public void setBancotipopagamentoService(BancotipopagamentoService bancotipopagamentoService) {this.bancotipopagamentoService = bancotipopagamentoService;}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {this.vendapagamentoService = vendapagamentoService;}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setContatipoService(ContatipoService contatipoService) {this.contatipoService = contatipoService;}
	public void setFechamentofinanceiroService(	FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setEmpresaService (EmpresaService empresaService){this.empresaService = empresaService;}
	public void setPedidovendapagamentoService(PedidovendapagamentoService pedidovendapagamentoService) {this.pedidovendapagamentoService = pedidovendapagamentoService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setHistoricoAntecipacaoService(HistoricoAntecipacaoService historicoAntecipacaoService) {this.historicoAntecipacaoService = historicoAntecipacaoService;}
	public void setDocumentoHistoricoService(DocumentohistoricoService documentoHistoricoService) {this.documentoHistoricoService = documentoHistoricoService;}
	
	/**
	 * M�todo que carrega as contas a pagar a serem baixadas.
	 * - Faz a jun��o dos itens de rateio de mesmo centro de custo, projeto e conta gerencial.
	 * 
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#findContasPreenchidas
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#juncaoRateioti
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#setaPercentual
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findAnaliticas
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView carregaContas(WebRequestContext request, BaixarContaBean baixarContaBean){
		
		List<BaixarContaBeanMovimentacao> listaBaixarContaBeanMovimentacao = new ArrayList<BaixarContaBeanMovimentacao>();
		BaixarContaBeanMovimentacao baixarContaBeanMovimentacao = new BaixarContaBeanMovimentacao();
		
		String whereIn = null;
		List<Documento> listaConta = null;
		
		if (baixarContaBean.getListaDocumento() == null) {
			whereIn = request.getParameter("selectedItens");
			if (whereIn == null || whereIn.equals("")) {
				throw new SinedException("Nenhum item selecionado.");
			}
		} else {
			whereIn = CollectionsUtil.listAndConcatenate(baixarContaBean.getListaDocumento(), "cddocumento", ",");
		}
		
		Documentoclasse documentoClasse = documentoService.defineDocumentoClasse(request, baixarContaBean);		
		
		baixarContaBean.setDocumentoclasse(documentoClasse);
		
		listaConta = contapagarService.findContasPreenchidas(whereIn);
		Integer qtdePessoas = countPessoas(listaConta);
		request.setAttribute("selectedItens", whereIn);
		
		Boolean permiteAutorizarContaPagar = contapagarService.permiteAutorizarContaPagar(SinedUtil.getUsuarioLogado());
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		Boolean mesmoCliente = null;
		boolean integracao_contabil = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INTEGRACAO_CONTABIL));
		
		List<Date> listaVencimento = new ArrayList<Date>();
		
		for (Documento documento : listaConta) {
			if (baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)) {
				documento.setChequeBaixarconta(documento.getCheque());
				if (!documento.getDocumentoacao().equals(Documentoacao.AUTORIZADA) && !documento.getDocumentoacao().equals(Documentoacao.AUTORIZADA_PARCIAL)) {
					if(!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA) || !permiteAutorizarContaPagar){
						request.addError("Registro(s) com situa��o diferente de 'autorizada' ou 'autorizada parcial'.");
						return new ModelAndView("redirect:/financeiro/crud/Contapagar");
					}
				}
				if(documento.getListaDocumentoautori()!= null && documento.getListaDocumentoautori().size() > 0){
					documento.setValorautorizado(documento.getListaDocumentoautori().get(0).getValor());
				}
				else{
					documento.setValorautorizado(documento.getAux_documento().getValoratual());
				}				
			} else { //Documentoclasse == RECEBER
				if (!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA) && !documento.getDocumentoacao().equals(Documentoacao.BAIXADA_PARCIAL)) {
					request.addError("Registro(s) com situa��o diferente de 'definitiva' ou 'baixada parcial'.");
					return new ModelAndView("redirect:/financeiro/crud/Contareceber");
				}
				
				if (documento.getTaxa() != null && documento.getTaxa().getListaTaxaitem() != null){
					for (Taxaitem taxaitem : documento.getTaxa().getListaTaxaitem()){
						if ((taxaitem.getAodia() != null && taxaitem.getAodia()) || taxaitem.isPercentual()
								|| taxaitem.getTipotaxa().equals(Tipotaxa.JUROS)
								|| taxaitem.getTipotaxa().equals(Tipotaxa.JUROS_MES)
								|| taxaitem.getTipotaxa().equals(Tipotaxa.DESAGIO)){
							request.addError("Para realizar a Baixa Parcial a conta n�o pode ter taxas calculadas diariamente, mensalmente e/ou taxas em percentual.");
							return new ModelAndView("redirect:/financeiro/crud/Contareceber");
						}
						if(Tipotaxa.TAXAMOVIMENTO.equals(taxaitem.getTipotaxa()) && documento.getDocumentotipo() != null && 
								documento.getDocumentotipo().getGerarmovimentacaoseparadataxa() != null && 
								documento.getDocumentotipo().getGerarmovimentacaoseparadataxa() &&
								documento.getDocumentotipo().getContagerencialtaxa() != null){
							request.addError("N�o � permitido baixar parcial conta que gera taxa separada.");
							return new ModelAndView("redirect:/financeiro/crud/Contareceber");
						}
					}
				}
				
				if(listaConta.size() == 1){
					mesmoCliente = true;
					request.setAttribute("cdcliente", documento.getPessoa() != null ? documento.getPessoa().getCdpessoa() : "");
					if(documento.getCheque() != null){
						baixarContaBean.setBancocheque(documento.getCheque().getBanco());
						baixarContaBean.setAgenciacheque(documento.getCheque().getAgencia());
						baixarContaBean.setContacheque(documento.getCheque().getConta());
						baixarContaBean.setNumerocheque(documento.getCheque().getNumero());
						
						Cheque cheque = chequeService.load(documento.getCheque());
						documento.setChequeBaixarconta(cheque);
						baixarContaBean.setCheque(cheque);
					}else {
						Vendapagamento vendapagamento = vendapagamentoService.findForChequeByDocumento(documento);
						if(vendapagamento != null){						
							baixarContaBean.setBancocheque(vendapagamento.getBanco());
							baixarContaBean.setAgenciacheque(vendapagamento.getAgencia());
							baixarContaBean.setContacheque(vendapagamento.getConta());
							baixarContaBean.setNumerocheque(vendapagamento.getNumero() != null ? vendapagamento.getNumero().toString() : null);
							
							Cheque cheque = new Cheque();
							if(vendapagamento.getCheque() != null){
								cheque = chequeService.load(vendapagamento.getCheque());
							}
							vendapagamento.setCheque(cheque);
							documento.setChequeBaixarconta(cheque);
							baixarContaBean.setCheque(cheque);
						}else{
							Pedidovendapagamento pedidovendapagamento = pedidovendapagamentoService.findForChequeByDocumento(documento);
							if(pedidovendapagamento != null){						
								baixarContaBean.setBancocheque(pedidovendapagamento.getBanco());
								baixarContaBean.setAgenciacheque(pedidovendapagamento.getAgencia());
								baixarContaBean.setContacheque(pedidovendapagamento.getConta());
								baixarContaBean.setNumerocheque(pedidovendapagamento.getNumero() != null ? pedidovendapagamento.getNumero().toString() : null);
								
								Cheque cheque = new Cheque();
								if(pedidovendapagamento.getCheque() != null){
									cheque = chequeService.load(pedidovendapagamento.getCheque());
								}
								pedidovendapagamento.setCheque(cheque);
								documento.setChequeBaixarconta(cheque);
								baixarContaBean.setCheque(cheque);
							}						
						}
					}
				}
			}
			
			if(documento.getEmpresa() != null && documento.getEmpresa().getCdpessoa() != null)
				listaEmpresa.add(documento.getEmpresa());
			
			documento.setValorRestante(movimentacaoService.calcularMovimentacoesByDocumento(documento, baixarContaBean.getDtpagamento()));
			if(request.getBindException() == null || request.getBindException().hasErrors() || baixarContaBean.getValorRecebido() == null){
				baixarContaBean.setValorRecebido(documento.getValorRestante());
			}
			baixarContaBean.setValorMovimento(documento.getValorRestante());
			
			if(documento.getDtvencimento() != null && !listaVencimento.contains(documento.getDtvencimento())){
				listaVencimento.add(documento.getDtvencimento());
			}
		}
		
		if (baixarContaBean.getDtpagamento() == null)
			baixarContaBean.setDtpagamento(SinedDateUtils.currentDate());

		listaConta = documentoService.calculaDocumentoValorByDataRef(listaConta, baixarContaBean.getDtpagamento(), false);
		
//		FAZER AQUI UM M�TODO PARA JOGAR O VALOR DA CONTAGERENCIAL NO RATEIO
		baixarContaBean.setRateio(null); //Limpa para recalcular o rateio
		Map<Documento, Money> mapDocTotalTaxa = new HashMap<Documento, Money>();
		List<Rateioitem> listaRateioitemWithTaxaSeparada = this.getListRateioitemTaxa(listaConta, baixarContaBean, mapDocTotalTaxa);
		
		baixarContaBean = contapagarService.manipulacaoContas(request, listaConta, false, baixarContaBean, mapDocTotalTaxa);
		if(listaRateioitemWithTaxaSeparada != null && !listaRateioitemWithTaxaSeparada.isEmpty() &&  
				baixarContaBean.getRateio() != null && baixarContaBean.getRateio().getListaRateioitem() != null && 
				!baixarContaBean.getRateio().getListaRateioitem().isEmpty()){
			baixarContaBean.getRateio().getListaRateioitem().addAll(listaRateioitemWithTaxaSeparada);
		}
		
		if(listaConta != null && listaConta.size() == 1){
			for (Documento ti : listaConta) {
				baixarContaBean.setHistorico(ti.getDescricao());
			}
		}
		
		documentoService.setarDados(request, baixarContaBean, mesmoCliente, null);
		
		if(listaEmpresa != null && listaEmpresa.size() >0)
			request.setAttribute("empresaWhereIn", CollectionsUtil.listAndConcatenate(listaEmpresa, "cdpessoa", ","));
		
//		FAZER AQUI UM M�TODO PARA JOGAR O VALOR DA CONTAGERENCIAL NO RATEIO
//		baixarContaBean.setRateio(getNovoRateio(listaConta, baixarContaBean));
		
		baixarContaBeanMovimentacao.setFormapagamento(baixarContaBean.getFormapagamento());
		baixarContaBeanMovimentacao.setDtpagamento(baixarContaBean.getDtpagamento());
		baixarContaBeanMovimentacao.setHistorico(baixarContaBean.getHistorico());
		baixarContaBeanMovimentacao.setVinculo(baixarContaBean.getVinculo());
		baixarContaBeanMovimentacao.setGerararquivoremessa(baixarContaBeanMovimentacao.getGerararquivoremessa());
		baixarContaBeanMovimentacao.setGerararquivoregistrocobranca(baixarContaBean.getGerararquivoregistrocobranca());
		
		//Cheque
		if(baixarContaBean.getCheque() != null){
			Cheque cheque = baixarContaBean.getCheque();
			baixarContaBeanMovimentacao.setFormapagamento(Formapagamento.CHEQUE);
			baixarContaBeanMovimentacao.setCdchequecheque(cheque.getCdcheque());
			baixarContaBeanMovimentacao.setBancocheque(cheque.getBanco());
			baixarContaBeanMovimentacao.setAgenciacheque(cheque.getAgencia());
			baixarContaBeanMovimentacao.setContacheque(cheque.getConta());
			baixarContaBeanMovimentacao.setNumerocheque(cheque.getNumero());
			baixarContaBeanMovimentacao.setEmitentecheque(cheque.getEmitente());
			baixarContaBeanMovimentacao.setDtbomparacheque(cheque.getDtbompara());
			baixarContaBeanMovimentacao.setValorcheque(cheque.getValor());
			if(cheque.getValor() != null && cheque.getValor().getValue().doubleValue() > 0){
				baixarContaBeanMovimentacao.setValor(cheque.getValor());
			}
		}
		
		Money valortotalmovimentacao = new Money();
		Money valorrestante = new Money(0.0);
		if(baixarContaBean.getValorRecebido()!=null && !baixarContaBean.getValorRecebido().equals(new Money(0)))
			valortotalmovimentacao = baixarContaBean.getValorRecebido();
		else if(request.getAttribute("valortotal") != null)
			valortotalmovimentacao = (Money) request.getAttribute("valortotal");
		
		baixarContaBean.setValortotalmovimentacao(valortotalmovimentacao);
		baixarContaBean.setValorrestante(valorrestante);
		
		if(request.getBindException() != null && !request.getBindException().hasErrors()){
			if(baixarContaBean.getCheque() == null || baixarContaBeanMovimentacao.getValorcheque() == null || 
					baixarContaBeanMovimentacao.getValorcheque().getValue().doubleValue() == 0){
				baixarContaBeanMovimentacao.setValor(valortotalmovimentacao);
			}
		}
		
		request.setAttribute("valortotalmovimentacao", valortotalmovimentacao);
		request.setAttribute("valorrestante", valorrestante);
		
		List<Documento> documentosAntecipado = new ArrayList<Documento>();
		if(qtdePessoas != null && qtdePessoas == 1 && listaConta != null && !listaConta.isEmpty() && listaConta.get(0).getPessoa() != null){
			documentosAntecipado = documentoService.findForAntecipacaoEntregaContaReceber(listaConta.get(0).getPessoa(), null);
		}
		
		preencherCamposMovimentacao(request, baixarContaBean, listaBaixarContaBeanMovimentacao, baixarContaBeanMovimentacao);
		if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_MOVIMENTACAO)){
			List<String> docs = new ArrayList<String>();
			if(!Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar())){
				for(Documento doc: baixarContaBean.getListaDocumento()){
					if(doc.getNumero()!=null && (!doc.getNumero().isEmpty()) && !docs.contains(doc.getNumero())){
						docs.add(doc.getNumero());	
					}
				}
			}
			baixarContaBeanMovimentacao.setChecknum(CollectionsUtil.concatenate(docs, " | "));			
		}
		
		if(baixarContaBean.getRateio() != null && baixarContaBean.getRateio().getListaRateioitem() != null){
			rateioService.calculaValorRateioitem(baixarContaBean.getValortotalmovimentacao(), baixarContaBean.getRateio().getListaRateioitem());
		}
		
		String bloquearRepeticaoConta = parametrogeralService.buscaValorPorNome(Parametrogeral.BLOQUEAR_REPETICAO_CONTA);
		if(bloquearRepeticaoConta == null || bloquearRepeticaoConta.trim().isEmpty()){
			bloquearRepeticaoConta = "false";
		}
		
		request.setAttribute("listDocumentoAntecipado", documentosAntecipado);
		request.setAttribute("gerarvalecompra", parametrogeralService.getValorPorNome(Parametrogeral.GERAR_VALECOMPRA));
		request.setAttribute("baixaContareceber", Documentoclasse.OBJ_RECEBER.equals(documentoClasse));
		request.setAttribute("integracao_contabil", integracao_contabil);
		request.setAttribute("valorDocumentoRestatne", new Money(100));
		request.setAttribute("bloquearRepeticaoConta", bloquearRepeticaoConta);
		request.setAttribute("LEITOR_CMC7", parametrogeralService.getBoolean(Parametrogeral.LEITOR_CMC7));
		request.setAttribute("BOMPARA_VENCIMENTO", parametrogeralService.getBoolean(Parametrogeral.BOMPARA_VENCIMENTO));
		request.setAttribute("VENCIMENTO", listaVencimento.size() == 1 ? SinedDateUtils.toString(listaVencimento.get(0)) : "");
		movimentacaoService.setAttributeBaixaVinculoDocumento(request, baixarContaBean.getListaDocumento());
		
		if(baixarContaBeanMovimentacao.getFormapagamento() != null && baixarContaBeanMovimentacao.getContatipo() == null && request.getBindException() != null && !request.getBindException().hasErrors()){
			request.setAttribute("executarChangeFormapagamento", true);
		}
		return new ModelAndView("process/baixarContaParcial", "baixarContaBean", baixarContaBean);
	}
	
	private Integer countPessoas(List<Documento> listaConta) {
		if(listaConta != null && !listaConta.isEmpty()) {
			Set<Pessoa> pessoas = new HashSet<Pessoa>();
			for (Documento documento : listaConta) {
				pessoas.add(documento.getPessoa());
			}
			return pessoas.size();
		}
		return 0;
	}
	
	public ModelAndView ajaxFindForBaixarContaReceberAntecipacao(WebRequestContext request, Pessoa pessoa) {
		Documentoclasse documentoclasse = null;
		return new JsonModelAndView().addObject("listDocumento", documentoService.findForAntecipacaoConsideraPedidoVenda(pessoa, null, null, documentoclasse));
	}
	
	public ModelAndView ajaxFindForBaixarContaPagarAntecipacao(WebRequestContext request, Pessoa pessoa) {
		return new JsonModelAndView().addObject("listDocumento", documentoService.findForAntecipacaoEntrega(pessoa,null));
	}
	
	/**
	 * Altera as contas a pagar para o status de baixada e gera uma movimenta��o da conta a pagar.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#transactionSaveBaixarConta
	 * @param request
	 * @param baixarContaBean
	 * @return
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	@Command(validate=true)
	@Input("carregaContas")
	public ModelAndView saveBaixarConta(WebRequestContext request, BaixarContaBean baixarContaBean){
		
		if (baixarContaBean == null) {
			throw new SinedException("Bean n�o pode ser nulo.");
		}
		
		if(baixarContaBean.getListaBaixarContaBeanMovimentacao() == null || baixarContaBean.getListaBaixarContaBeanMovimentacao().isEmpty()){
			throw new SinedException("Movimenta��o n�o pode ser nula");
		}
		
		if (baixarContaBean.getListaDocumento() == null) {
			throw new SinedException("Lista de documentos n�o pode ser nulo.");
		}
		
		//Verifica��o de data limite do ultimo fechamento. 
		if(fechamentofinanceiroService.verificaListaFechamento(baixarContaBean)){
			throw new SinedException("A data de pagamento informada refere-se a um per�odo j� fechado.");
		}
		
		baixarContaBean.setFromBaixamanual(true);
		
		Money valortotalmovimentacao = new Money();
		Money valortotaltaxa = new Money();
		Double valorrestante = 0.0;

		for(BaixarContaBeanMovimentacao baixarContaBeanMovimentacao : baixarContaBean.getListaBaixarContaBeanMovimentacao()){
			baixarContaBeanMovimentacao.setDtpagamento(baixarContaBean.getDtpagamento());
			
			if(baixarContaBeanMovimentacao.getValor() == null || 
					(baixarContaBeanMovimentacao.getValor() != null && baixarContaBeanMovimentacao.getValor().getValue().doubleValue() == 0)){
				throw new SinedException("Valor da movimenta��o n�o pode ser 0.");
			}else{
				valortotalmovimentacao = valortotalmovimentacao.add(baixarContaBeanMovimentacao.getValor());
				if(baixarContaBeanMovimentacao.getValortaxa() != null){
					valortotaltaxa = valortotaltaxa.add(baixarContaBeanMovimentacao.getValortaxa());
				}
			}
			
			if(Boolean.TRUE.equals(baixarContaBeanMovimentacao.getDiscriminarvalortaxa()) && baixarContaBeanMovimentacao.getValortaxa() != null && 
					baixarContaBeanMovimentacao.getValortaxa().getValue().doubleValue() > baixarContaBeanMovimentacao.getValor().getValue().doubleValue()){
				throw new SinedException("O valor da taxa deve ser menor ou igual ao valor da movimenta��o.");
			}
			
			if (baixarContaBeanMovimentacao.getFormapagamento() == null) {
				throw new SinedException("Forma de pagamento n�o pode ser nula.");
			}else {
				if(baixarContaBeanMovimentacao.getFormapagamento().equals(Formapagamento.CHEQUE)){
					if(baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
						if(baixarContaBeanMovimentacao.getNumerocheque() == null){
							throw new SinedException("N�mero do cheque n�o pode ser nulo.");
						}else {
							Cheque cheque = new Cheque();
							
							if(baixarContaBeanMovimentacao.getCdchequecheque() != null){
								cheque.setCdcheque(baixarContaBeanMovimentacao.getCdchequecheque());
								cheque.setResgatado(true);
								cheque.setValor(baixarContaBeanMovimentacao.getValorcheque());
								Cheque chequeBean = chequeService.loadForEntrada(cheque);
								if(chequeBean != null){
									cheque.setEmpresa(chequeBean.getEmpresa());
									cheque.setLinhanumerica(chequeBean.getLinhanumerica());
									cheque.setChequesituacao(chequeBean.getChequesituacao());
								}
							}else {
								cheque.setValor(baixarContaBeanMovimentacao.getValor());
							}
							cheque.setBanco(baixarContaBeanMovimentacao.getBancocheque());
							cheque.setAgencia(baixarContaBeanMovimentacao.getAgenciacheque());
							cheque.setConta(baixarContaBeanMovimentacao.getContacheque());
							cheque.setNumero(baixarContaBeanMovimentacao.getNumerocheque());
							cheque.setEmitente(baixarContaBeanMovimentacao.getEmitentecheque());
							cheque.setCpfcnpj(baixarContaBeanMovimentacao.getCpfcnpjcheque());
							cheque.setDtbompara(baixarContaBeanMovimentacao.getDtbomparacheque());
							cheque.setEmpresa(empresaService.findByDocumento(baixarContaBean.getDocumento()));
							
							baixarContaBeanMovimentacao.setCheque(cheque);
						}
					}else {					
						if(baixarContaBeanMovimentacao.getNumerocheque() == null || "".equals(baixarContaBeanMovimentacao.getNumerocheque())){
							throw new SinedException("Campo cheque ou n�mero do cheque n�o pode ser nulo.");
						}else{
							Cheque cheque = new Cheque();
							if(baixarContaBeanMovimentacao.getCdchequecheque() != null){
								cheque.setCdcheque(baixarContaBeanMovimentacao.getCdchequecheque());
								cheque.setResgatado(true);
								cheque.setValor(baixarContaBeanMovimentacao.getValorcheque());
								Cheque chequeBean = chequeService.loadForEntrada(cheque);
								if(chequeBean != null){
									cheque.setEmpresa(chequeBean.getEmpresa());
									cheque.setLinhanumerica(chequeBean.getLinhanumerica());
									cheque.setChequesituacao(chequeBean.getChequesituacao());
								}
							}else {
								cheque.setValor(baixarContaBeanMovimentacao.getValor());
							}
							cheque.setBanco(baixarContaBeanMovimentacao.getBancocheque());
							cheque.setAgencia(baixarContaBeanMovimentacao.getAgenciacheque());
							cheque.setConta(baixarContaBeanMovimentacao.getContacheque());
							cheque.setNumero(baixarContaBeanMovimentacao.getNumerocheque());
							cheque.setEmitente(baixarContaBeanMovimentacao.getEmitentecheque());
							cheque.setCpfcnpj(baixarContaBeanMovimentacao.getCpfcnpjcheque());
							cheque.setDtbompara(baixarContaBeanMovimentacao.getDtbomparacheque());
							cheque.setEmpresa(empresaService.findByDocumento(baixarContaBean.getDocumento()));
							baixarContaBeanMovimentacao.setCheque(cheque);
						}
					}

					if(chequeService.verificarDuplicidadeCheque(baixarContaBeanMovimentacao.getCheque())){						
						throw new SinedException("Cheque " + baixarContaBeanMovimentacao.getCheque().getNumero() + " j� lan�ado no sistema.");
					}
				}else if(baixarContaBeanMovimentacao.getFormapagamento().equals(Formapagamento.VALECOMPRA)){
					baixarContaBeanMovimentacao.setPessoa(documentoService.carregaDocumento(baixarContaBean.getDocumento()).getPessoa());
				} else if(baixarContaBeanMovimentacao.getFormapagamento().equals(Formapagamento.ANTECIPACAO)){
					saveHistoricoAntecipacao(baixarContaBean, baixarContaBeanMovimentacao);
					saveHistoricoBaixa(baixarContaBean, baixarContaBeanMovimentacao);
				}
			}
		}
		validaChequesnovos(baixarContaBean);
		
		String gerarvalecompra = parametrogeralService.getValorPorNome(Parametrogeral.GERAR_VALECOMPRA);
		valorrestante = baixarContaBean.getValorRecebido().getValue().doubleValue() - valortotalmovimentacao.getValue().doubleValue();
		if(valorrestante < 0 && valortotaltaxa != null && valortotaltaxa.getValue().doubleValue() > 0){
			valorrestante = SinedUtil.round(valorrestante, 2) + valortotaltaxa.getValue().doubleValue();
		}
		if(valorrestante < 0 && (!"TRUE".equalsIgnoreCase(gerarvalecompra) || baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR))){
			throw new SinedException("S� � permitido baixar a(s) conta(s) se o valor restante for igual a zero.");
		}else if("TRUE".equalsIgnoreCase(gerarvalecompra) && baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER) && valorrestante > 0){
			throw new SinedException("S� � permitido baixar a(s) conta(s) se o valor restante for menor ou igual a zero.");
		}
		
		List<Documento> listaDocumentoForValecompra = new ArrayList<Documento>();
		if("TRUE".equalsIgnoreCase(gerarvalecompra) && baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER) && valorrestante < 0){
			Documento documentoValecompra;
			for(Documento documento : baixarContaBean.getListaDocumento()){
				documentoValecompra = new Documento(documento.getCddocumento());
				documentoValecompra.setValoratualbaixa(documento.getValoratual());
				listaDocumentoForValecompra.add(documentoValecompra);
			}
		}
		
		if(baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)){
			for (Documento doc : baixarContaBean.getListaDocumento()) {
				Documento documento = new Documento();
				documento = documentoService.verificaDocumentoacao(doc);			
				doc.setDocumentoacao(documento.getDocumentoacao());
				if(doc.getDocumentoacao().equals(Documentoacao.DEFINITIVA) && contapagarService.permiteAutorizarContaPagar(SinedUtil.getUsuarioLogado())){
					doc.setDocumentoacao(Documentoacao.AUTORIZADA);
					documentoService.doAutorizar(doc.getDocumentoacao(), doc.getCddocumento().toString());
				}
			}
		} else {
			notaDocumentoService.liberaNotaDocumentoWebservice(request, baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//			contareceberService.incrementaNumDocumento(baixarContaBean.getListaDocumento());
			documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);
		}
		
		try {
			for(BaixarContaBeanMovimentacao baixarContaBeanMovimentacao : baixarContaBean.getListaBaixarContaBeanMovimentacao()){
				if(baixarContaBeanMovimentacao.getCheque() != null)
					chequeService.saveOrUpdate(baixarContaBeanMovimentacao.getCheque());
			}
			contapagarService.transactionSaveBaixarContaVariasMovimentacoes(request,baixarContaBean, true);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return continueOnAction("carregaContas", baixarContaBean);
		}
		
		if (baixarContaBean.getErro() == null || !baixarContaBean.getErro()) {
			request.addMessage("Conta(s) baixada(s) com sucesso.");
		
			if("TRUE".equalsIgnoreCase(gerarvalecompra) && baixarContaBean.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER) && valorrestante < 0){
				Double valorrestantedocumento = 0d;
				for(Documento d : baixarContaBean.getListaDocumento()){
					valorrestantedocumento += d.getValorRestante().getValue().doubleValue();
				}
				valecompraService.createValecompraByDocumento(request, baixarContaBean.getListaDocumento(), valortotalmovimentacao.getValue().doubleValue(), valorrestantedocumento, baixarContaBean.getDtpagamento(), listaDocumentoForValecompra);
			}
			if (baixarContaBean.getArquivo() != null){
				if ((baixarContaBean.getGerararquivoremessa()!= null && baixarContaBean.getGerararquivoremessa() && Formapagamento.DEBITOCONTACORRENTE.equals(baixarContaBean.getFormapagamento())) || 
					(baixarContaBean.getGerararquivoregistrocobranca() != null && baixarContaBean.getGerararquivoregistrocobranca())) {
					Arquivo arquivo = baixarContaBean.getArquivo();
					DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdfile());
					request.addMessage("Arquivo banc�rio gerado corretamente."); 
					request.addMessage("Nome do arquivo: "+arquivo.getNome());
					request.addMessage("<a href="+request.getServletRequest().getContextPath()+"/DOWNLOADFILE/"+arquivo.getCdfile()+">Clique aqui</a> para fazer o download.");
				}
			}
						
			for (Documento documento : baixarContaBean.getListaDocumento()){
				// FEITO ISSO PARA ATUALIZAR O VALOR ATUAL DO DOCUMENTO.
				documentoService.callProcedureAtualizaDocumento(documento);
			}
			
			try {
				contratoService.recalcularComissaoContaBaixada(baixarContaBean.getListaDocumento());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (baixarContaBean.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR)) {
			return new ModelAndView("redirect:/financeiro/crud/Contapagar","baixarContaBean",baixarContaBean);
		} else {
			return new ModelAndView("redirect:/financeiro/crud/Contareceber","baixarContaBean",baixarContaBean);
		}		
		
	}
	
	private void saveHistoricoAntecipacao(BaixarContaBean baixarContaBean, BaixarContaBeanMovimentacao movimentacao) {
		HistoricoAntecipacao antecipacao = new HistoricoAntecipacao();
		antecipacao.setValor(movimentacao.getValor());
		antecipacao.setDescricao("Valor descontado na Conta " + (Documentoclasse.OBJ_RECEBER.equals(baixarContaBean.getDocumentoclasse()) ? "receber" : "pagar" ) + ": " + printCdsBaixaContaBean(baixarContaBean));
		antecipacao.setDocumentoReferencia(baixarContaBean.getDocumento());
		antecipacao.setDocumento(movimentacao.getDocumentoAntecipado());
		antecipacao.setDtBaixa(baixarContaBean.getDtpagamento());
		historicoAntecipacaoService.saveOrUpdateNoUseTransaction(antecipacao);
	}
	
	private void saveHistoricoBaixa(BaixarContaBean baixarContaBean, BaixarContaBeanMovimentacao movimentacao) {
		Documentohistorico historico = new Documentohistorico();
		historico.setDocumento(baixarContaBean.getDocumento());
		historico.setDocumentoacao(Documentoacao.BAIXADA);
		historico.setObservacao("Registro Conta " + (Documentoclasse.OBJ_RECEBER.equals(baixarContaBean.getDocumentoclasse()) ? "Receber" : "Pagar" ) + 
				" - Antecipa��o: <a href=\"/w3erp/financeiro/crud/Conta" + (Documentoclasse.OBJ_RECEBER.equals(baixarContaBean.getDocumentoclasse()) ? "receber" : "pagar" ) + "?ACAO=consultar&cddocumento=" + movimentacao.getDocumentoAntecipado().getCddocumento() + "\" >" + 
				movimentacao.getDocumentoAntecipado().getCddocumento() + "</a>");
		documentoHistoricoService.saveOrUpdate(historico);
	}
	
	private String printCdsBaixaContaBean(BaixarContaBean baixarContaBean) {
		String cds = new String("");
		if( baixarContaBean != null &&  baixarContaBean.getListaDocumento() != null){
			for(int i = 0; i < baixarContaBean.getListaDocumento().size(); i++){
				if(baixarContaBean.getListaDocumento().get(i) != null && baixarContaBean.getListaDocumento().get(i).getCddocumento() != null)
					if(i == (baixarContaBean.getListaDocumento().size() -1))
						cds += baixarContaBean.getListaDocumento().get(i).getCddocumento() + "";
					else
						cds += baixarContaBean.getListaDocumento().get(i).getCddocumento() + ", ";
			}
		}
		return cds;
	}
	
	/**
	 * Valida se existem cheques duplicados sendo baixados.
	 *
	 * @param baixarContaBean
	 * @return
	 * @since 05/07/2012
	 * @author Rodrigo Freitas
	 */
	private void validaChequesnovos(BaixarContaBean baixarContaBean) {
		List<Cheque> listaCheque = new ArrayList<Cheque>();
		List<BaixarContaBeanMovimentacao> listaBaixarContaBeanMovimentacao = baixarContaBean.getListaBaixarContaBeanMovimentacao();
		
		for (BaixarContaBeanMovimentacao baixarContaBeanMovimentacao : listaBaixarContaBeanMovimentacao) {
			if(baixarContaBeanMovimentacao.getCheque() != null){
				listaCheque.add(baixarContaBeanMovimentacao.getCheque());
			}
		}
		
//		boolean duplicado = false;

		if(listaCheque.size() > 0){
			Cheque cheque_i, cheque_j;
			for (int i = 0; i < listaCheque.size(); i++) {
				cheque_i = listaCheque.get(i);
				int cont = 0;
				for (int j = 0; j < listaCheque.size(); j++) {
					if(i != j){
						cheque_j = listaCheque.get(j);
						
						boolean agencia = cheque_i.getAgencia() != null && cheque_j.getAgencia() != null && cheque_i.getAgencia().equals(cheque_j.getAgencia()) || (cheque_i.getAgencia()==null && cheque_j.getAgencia()==null);
						boolean conta = cheque_i.getConta() != null && cheque_j.getConta() != null && cheque_i.getConta().equals(cheque_j.getConta()) || (cheque_i.getConta()==null && cheque_j.getConta()==null);
						boolean numero = cheque_i.getNumero() != null && cheque_j.getNumero() != null && cheque_i.getNumero().equals(cheque_j.getNumero()) || (Util.strings.emptyIfNull(cheque_i.getNumero()).equals("") && Util.strings.emptyIfNull(cheque_j.getNumero()).equals(""));
						boolean banco = cheque_i.getBanco() != null && cheque_j.getBanco() != null && cheque_i.getBanco().equals(cheque_j.getBanco()) || (cheque_i.getBanco()==null && cheque_j.getBanco()==null);
						boolean empresa = cheque_i.getEmpresa() != null && cheque_j.getEmpresa() != null && cheque_i.getEmpresa().equals(cheque_j.getEmpresa()) || (cheque_i.getEmpresa()==null && cheque_j.getEmpresa()==null);
						
						if (cheque_i.getCdcheque()!=null && cheque_j.getCdcheque()!=null && cheque_i.getCdcheque().equals(cheque_j.getCdcheque())){
							cont++;
						}
						
						if(agencia && conta && numero && banco && empresa){
//							duplicado = true;
							NeoWeb.getRequestContext().setAttribute("ERRO_BAIXAR_CONTA", Boolean.TRUE);
							throw new SinedException("Existe(m) cheque(s) em duplicidade (Ag�ncia, Banco, Conta e N�mero). N� do cheque: " + cheque_j.getNumero());
						}
						
						if(cont>0){
							NeoWeb.getRequestContext().setAttribute("ERRO_BAIXAR_CONTA", Boolean.TRUE);
							throw new SinedException("Existe(m) cheque(s) em duplicidade. N� do cheque: " + cheque_i.getNumero());
						}
					}
				}
			}
		}
//		return duplicado;
	}
	
	/**
	 * Popula o combo de box de v�cunlo, via AJAX.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#findByTipo
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView comboBox(WebRequestContext request, Formapagamento formapagamento) {
		
		if (formapagamento == null || formapagamento.getCdformapagamento() == null) {
			throw new SinedException("Forma de pagamento n�o pode ser nulo.");
		}
		
		List<Conta> listaConta = new ArrayList<Conta>();
		
		if (formapagamento.equals(Formapagamento.CAIXA)) {
			listaConta = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CAIXA));
		} else if (formapagamento.equals(Formapagamento.DINHEIRO)) {
			listaConta = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CAIXA), new Contatipo(Contatipo.CONTA_BANCARIA));
		} else if(formapagamento.equals(Formapagamento.CHEQUE)){
			List<Conta> listaCaixa = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CAIXA));
			List<Conta> listaBancario = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CONTA_BANCARIA));
			if(listaCaixa !=null && listaCaixa.size() > 0){
				listaConta.addAll(listaCaixa);
			}
			if(listaBancario !=null && listaBancario.size() > 0){
				listaConta.addAll(listaBancario);
			}
		}
		else if (formapagamento.equals(Formapagamento.CARTAOCREDITO)) {
			listaConta = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CARTAO_DE_CREDITO));
		} else {
			listaConta = contaService.findByTipoAndEmpresa(formapagamento.getEmpresaWhereIn(), new Contatipo(Contatipo.CONTA_BANCARIA));
		}

		/**
		 * Para n�o dar pau ao retornar o objeto JSON
		 */
		for (Conta conta : listaConta) {
			conta.setListaContaempresa(null);
		}
		return new JsonModelAndView().addObject("lista", listaConta);
	}
	
	public ModelAndView comboBoxTipoVinculo(WebRequestContext request, Contatipo contatipo) {
		
		boolean sugestao = contatipo.getCdcontatipo() == null;
		Integer cddocumentoclasse = request.getParameter("cddocumentoclasse") != null ? Integer.parseInt(request.getParameter("cddocumentoclasse")) : null;
		Integer cdformapagamento = request.getParameter("cdformapagamento") != null ? Integer.parseInt(request.getParameter("cdformapagamento")) : null;
		
		String whereInEmpresa = request.getParameter("empresaWhereIn");
		if (contatipo == null || contatipo.getCdcontatipo() == null) {
			throw new SinedException("Tipo de v�nculo n�o pode ser nulo.");
		}
		
		List<Conta> listaConta = !sugestao ? contaService.findByTipoAndEmpresa(whereInEmpresa, contatipo) : new ArrayList<Conta>();
		

		String whereIn = request.getParameter("selectedItens");
		Integer selecionaVinculo = null;
		List<Conta> listaContaSugestao = new ArrayList<Conta>();
		
		if (whereIn != null && !whereIn.trim().equals("")) {
			
			List<Documento> documentos = documentoService.loadDocumentos(whereIn);
			for(Documento documento : documentos) {
				
				if(documento.getDocumentotipo().getContadestino() != null){
					if(sugestao){
						if(!listaContaSugestao.contains(documento.getDocumentotipo().getContadestino())){
							listaContaSugestao.add(documento.getDocumentotipo().getContadestino());
						}
					}else if(listaConta.contains(documento.getDocumentotipo().getContadestino())){
						selecionaVinculo = documento.getDocumentotipo().getContadestino().getCdconta();
					}
				}
				
			}
		}
		
		Integer cdempresa = SinedUtil.getIdUnicoWhereIn(whereInEmpresa);
		if(Documentoclasse.CD_RECEBER.equals(cddocumentoclasse) && ((selecionaVinculo == null && !sugestao) || (sugestao && listaContaSugestao.isEmpty())) && cdempresa != null){
			Empresa empresa = empresaService.loadForBaixa(new Empresa(cdempresa));
			if(empresa != null){
				if((Contatipo.CAIXA.equals(contatipo) || sugestao) && Formapagamento.DINHEIRO.getCdformapagamento().equals(cdformapagamento) && empresa.getContabaixadinheiro() != null){
					selecionaVinculo = empresa.getContabaixadinheiro().getCdconta();
					if(sugestao){
						listaContaSugestao.add(empresa.getContabaixadinheiro());
					}
				}else if((Contatipo.CAIXA.equals(contatipo) || sugestao) && Formapagamento.CHEQUE.getCdformapagamento().equals(cdformapagamento) && empresa.getContabaixacheque() != null){
					selecionaVinculo = empresa.getContabaixacheque().getCdconta();
					if(sugestao){
						listaContaSugestao.add(empresa.getContabaixacheque());
					}
				}else if((Contatipo.CONTA_BANCARIA.equals(contatipo) || sugestao) && Formapagamento.CREDITOCONTACORRENTE.getCdformapagamento().equals(cdformapagamento) && empresa.getContabaixacreditoconta() != null){
					selecionaVinculo = empresa.getContabaixacreditoconta().getCdconta();
					if(sugestao){
						listaContaSugestao.add(empresa.getContabaixacreditoconta());
					}
				}
			}
		}
		
		if(listaContaSugestao.size() == 1){
			contatipo = listaContaSugestao.get(0).getContatipo();
			listaConta = contaService.findByTipoAndEmpresa(whereInEmpresa, contatipo);
			if(selecionaVinculo == null){
				selecionaVinculo = listaContaSugestao.get(0).getCdconta();
			}
			
		}
		
		if(selecionaVinculo == null && listaConta != null && listaConta.size() == 1){
			selecionaVinculo = listaConta.get(0).getCdconta();
		}
		
		/**
		 * Para n�o dar pau ao retornar o objeto JSON
		 */
		for (Conta conta : listaConta) {
			conta.setListaContaempresa(null);
		}
		return new JsonModelAndView()
			.addObject("lista", listaConta)
			.addObject("selecionaVinculo", selecionaVinculo)
			.addObject("cdcontatipo", contatipo != null ? contatipo.getCdcontatipo() : "")
			.addObject("sugestao", sugestao);
	}
	
	public ModelAndView comboBoxContatipo(WebRequestContext request, Formapagamento formapagamento) {
		if (formapagamento == null || formapagamento.getCdformapagamento() == null) {
			throw new SinedException("Tipo de v�nculo n�o pode ser nulo.");
		}
		
		List<Contatipo> listaContatipo = new ArrayList<Contatipo>();
		StringBuilder whereInContatipo = new StringBuilder(); 
		if (formapagamento.equals(Formapagamento.CAIXA)) {
			listaContatipo.add(contatipoService.load(new Contatipo(Contatipo.CAIXA)));
		} else if (formapagamento.equals(Formapagamento.DINHEIRO)) {
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CAIXA);
//			if(!"".equals(whereInContatipo.toString()))
//				whereInContatipo.append(",");
//			whereInContatipo.append(Contatipo.CONTA_BANCARIA);			
		} else if (formapagamento.equals(Formapagamento.CARTAOCREDITO)) {
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CARTAO_DE_CREDITO);
		} else {
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CAIXA);
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CONTA_BANCARIA);		
		}
		
		if(whereInContatipo != null && !"".equals(whereInContatipo.toString()))
			listaContatipo = contatipoService.findForComboMovimentacao(whereInContatipo.toString(), formapagamento.getEmpresaWhereIn());
		

		return new JsonModelAndView().addObject("lista", listaContatipo);
	}
	
	/** M�todo que popula os combos de forma de pagamento e de tipo de pagamento via Ajax
	 * @author Thiago Augusto
	 * @param request
	 * @param conta
	 * @return
	 */
	public ModelAndView comboBoxFormaPagamento(WebRequestContext request, Conta conta){
		conta = contaService.carregaConta(conta);
		List<BancoFormapagamento> listaFormaPagamento = bancoformapagamentoService.FindByBanco(conta.getBanco());
		for (BancoFormapagamento banco : listaFormaPagamento) {
			banco.setBanco(null);
		}
		return new JsonModelAndView().addObject("listaBancoFormaPagamento", listaFormaPagamento);
	}
	
	/** M�todo que popula os combos de tipo de pagamento e de tipo de pagamento via Ajax
	 * @author Thiago Augusto
	 * @param request
	 * @param conta
	 * @return
	 */
	public ModelAndView comboBoxTipoPagamento(WebRequestContext request, Conta conta){
		conta = contaService.carregaConta(conta);
		List<BancoTipoPagamento> listaBancoTipoPagamento = bancotipopagamentoService.findByBanco(conta.getBanco());
		for (BancoTipoPagamento bancoTipoPagamento : listaBancoTipoPagamento) {
			bancoTipoPagamento.setBanco(null);
		}
		return new JsonModelAndView().addObject("listaBancoTipoPagamento", listaBancoTipoPagamento);
	}
	
	public void ajaxVinculoCobranca(WebRequestContext request, BaixarContaBean bean) {
		if(bean != null && bean.getVinculo() != null && bean.getVinculo().getCdconta() != null){
			
			if(contaService.isContaBradesco(bean.getVinculo())){
				View.getCurrent().println("bradesco = true;");
			} else {
				View.getCurrent().println("bradesco = false;");
			}
			
		} else {
			View.getCurrent().println("bradesco = false;");
		}
	}
	
	/**
	 * A��o para carregar a pop-up de edi��o de juros do documento.
	 * 
	 * @param request
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView edicaoTaxas(WebRequestContext request, Documento documento){
		return taxaService.edicaoTaxasBaixarConta(request, documento);
	}
	
	/**
	 * Faz o update no juros e fecha a tela de popup fazendo o reload na tela de baixaContaPagar.
	 * 
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#loadForEntrada
	 * - � usado para que seja usado depois o saveOrUpdate sobrescrito no service.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#saveOrUpdate
	 * @param request
	 * @param documento
	 * @author Rodrigo Freitas
	 */
	@Input("edicaoTaxas")
	@Command(validate=true)
	public void saveTaxa(WebRequestContext request, Documento documento)throws Exception{
		taxaService.saveTaxaBaixarConta(request, documento);
	}
	
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if(acao.equals("saveTaxa")){
			taxaService.validateTaxasBaixarConta(obj, errors);
		} else {
			BaixarContaBean bean = (BaixarContaBean) obj;
			Movimentacao movimentacao;
			Money valortotalmovimentacao = new Money(0.0);
			Double valorrestante = 0.0;
			
			if(bean.getListaBaixarContaBeanMovimentacao() == null || bean.getListaBaixarContaBeanMovimentacao().isEmpty()){
				errors.reject("001", "Movimenta��o n�o pode ser nula.");
			}else {
				List<Documento> listaDoc = bean.getListaDocumento();
				List<Empresa> listaEmpresa = new ArrayList<Empresa>();
				for (Documento documento : listaDoc) {
					Documento doc = documentoService.loadWithEmpresa(documento);
					if(doc != null && doc.getEmpresa() != null){
						listaEmpresa.add(doc.getEmpresa());
					}
				}
				for(BaixarContaBeanMovimentacao baixarContaBeanMovimentacao : bean.getListaBaixarContaBeanMovimentacao()){
					movimentacao = new Movimentacao();
					movimentacao.setConta(baixarContaBeanMovimentacao.getVinculo());
					
					if(baixarContaBeanMovimentacao.getValor() == null){
						errors.reject("001", "Valor da movimenta��o n�o pode ser 0.");
					}else{
						valortotalmovimentacao = valortotalmovimentacao.add(baixarContaBeanMovimentacao.getValor());
						movimentacao.setValor(baixarContaBeanMovimentacao.getValor());
					}
					
					if(Boolean.TRUE.equals(baixarContaBeanMovimentacao.getDiscriminarvalortaxa()) && baixarContaBeanMovimentacao.getValortaxa() != null && 
							baixarContaBeanMovimentacao.getValortaxa().getValue().doubleValue() > baixarContaBeanMovimentacao.getValor().getValue().doubleValue()){
						errors.reject("001", "O valor da taxa deve ser menor ou igual ao valor da movimenta��o.");
					}
						
					if (bean.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR)) {
						movimentacao.setTipooperacao(Tipooperacao.TIPO_DEBITO);
						movimentacaoService.validateMovimentacao(errors, movimentacao);
					}
					
					if(listaEmpresa != null && !listaEmpresa.isEmpty() && baixarContaBeanMovimentacao.getCdchequecheque() != null){
						Cheque cheque = chequeService.carregaCheque(new Cheque(baixarContaBeanMovimentacao.getCdchequecheque()));
						if(cheque != null && cheque.getEmpresa() != null && !listaEmpresa.contains(cheque.getEmpresa())){
							errors.reject("001", "N�o � permitido selecionar cheque de outra empresa. Cheque " + (cheque.getNumero() != null ? cheque.getNumero() : ""));
						}
					}
				}
				
				valorrestante = bean.getValorRecebido().subtract(valortotalmovimentacao).getValue().doubleValue(); 
				if(valorrestante > 0){
					errors.reject("001", "S� � permitido baixar a(s) conta(s) se o valor restante for igual a zero.");
				}			
				
				try{
					rateioService.validateRateio(bean.getRateio(), bean.getValorRecebido());
				} catch (SinedException e) {
					errors.reject("001", e.getMessage());
				}
			}
		}
	}
	
	/** M�todo para verificar se o campo sispag do banco est� marcado
	 * @author Thiago Augusto
	 * @param request
	 * @param conta
	 * @return
	 */
	public ModelAndView verificaSispag(WebRequestContext request, Conta conta){
		conta = contaService.carregaConta(conta);
		boolean verificaSispag = false;
		if(conta.getBanco() != null && conta.getBanco().getGerarsispag()!= null && conta.getBanco().getGerarsispag() == true){
			verificaSispag = true;
		}
		return new JsonModelAndView().addObject("verificaSispag", verificaSispag);
	}
	
	public List<Rateioitem> getListRateioitemTaxa(List<Documento> listaDocumento, BaixarContaBean baixarContaBean, Map<Documento, Money> mapDocTotalTaxa){
		//Calcula com base na data de pagamento o novo rateio
		Date atual = new Date(System.currentTimeMillis());
		if (baixarContaBean.getDtpagamento() != null)
			atual = baixarContaBean.getDtpagamento();
		
		int difdias;
		List<Rateioitem> listaRI = new ArrayList<Rateioitem>();
		if (baixarContaBean.getRateio() != null && baixarContaBean.getRateio().getListaRateioitem().size() > 0)
			listaRI.addAll(baixarContaBean.getRateio().getListaRateioitem());
		Rateioitem riMulta = new Rateioitem();
		Rateioitem riJuros = new Rateioitem();
		Rateioitem riTaxaBoleto = new Rateioitem();
		
		for (Documento documento : listaDocumento) {
			documento.setTaxa(taxaService.findByDocumento(documento));
			if (documento.getTaxa() != null){
				if (documento.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
					for (Taxaitem ti : documento.getTaxa().getListaTaxaitem()) {
						if (ti.getTipotaxa().getContacredito() != null && ti.getTipotaxa().getCentrocusto() != null){
							if (ti.getTipotaxa().getCentrocusto() != null && ti.getTipotaxa().getContacredito() != null){
								if(ti.getTipotaxa().equals(Tipotaxa.MULTA) && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), atual)){
									if(ti.getValor() != null){
										if(mapDocTotalTaxa.get(documento) != null){
											mapDocTotalTaxa.put(documento, mapDocTotalTaxa.get(documento).add(ti.getValor()));
										}else {
											mapDocTotalTaxa.put(documento, ti.getValor());
										}
									}
									riMulta.setValor(riMulta.getValor() != null ? riMulta.getValor().add(ti.getValor()) : ti.getValor());
									riMulta.setCentrocusto(ti.getTipotaxa().getCentrocusto());
									riMulta.setContagerencial(ti.getTipotaxa().getContacredito());
								} 
								if(ti.getTipotaxa().equals(Tipotaxa.TAXABOLETO) && SinedDateUtils.beforeOrEqualIgnoreHour(ti.getDtlimite(), atual)){
									if(ti.getValor() != null){
										if(mapDocTotalTaxa.get(documento) != null){
											mapDocTotalTaxa.put(documento, mapDocTotalTaxa.get(documento).add(ti.getValor()));
										}else {
											mapDocTotalTaxa.put(documento, ti.getValor());
										}
									}
									riTaxaBoleto.setValor(riTaxaBoleto.getValor() != null ? riTaxaBoleto.getValor().add(ti.getValor()) : ti.getValor());
									riTaxaBoleto.setCentrocusto(ti.getTipotaxa().getCentrocusto());
									riTaxaBoleto.setContagerencial(ti.getTipotaxa().getContacredito());
								} 
								if (ti.getTipotaxa().equals(Tipotaxa.JUROS) && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), atual)){
									difdias = SinedDateUtils.diferencaDias(atual,ti.getDtlimite());
									Money valorAux = ti.getValor().multiply(new Money(new Double(difdias)));
									if(valorAux != null){
										if(mapDocTotalTaxa.get(documento) != null){
											mapDocTotalTaxa.put(documento, mapDocTotalTaxa.get(documento).add(valorAux));
										}else {
											mapDocTotalTaxa.put(documento, valorAux);
										}
									}
									riJuros.setValor(riJuros.getValor() != null ? riJuros.getValor().add(valorAux) : valorAux);
									riJuros.setCentrocusto(ti.getTipotaxa().getCentrocusto());
									riJuros.setContagerencial(ti.getTipotaxa().getContacredito());
								}
							}
						}
					}
				} else {
					for (Taxaitem ti : documento.getTaxa().getListaTaxaitem()) {
						if (ti.getTipotaxa().getContacredito() != null && ti.getTipotaxa().getCentrocusto() != null){
							if (ti.getTipotaxa().getCentrocusto() != null && ti.getTipotaxa().getContacredito() != null){
								if(ti.getTipotaxa().equals(Tipotaxa.MULTA) && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), atual)){
									if(ti.getValor() != null){
										if(mapDocTotalTaxa.get(documento) != null){
											mapDocTotalTaxa.put(documento, mapDocTotalTaxa.get(documento).add(ti.getValor()));
										}else {
											mapDocTotalTaxa.put(documento, ti.getValor());
										}
									}
									riMulta.setValor(riMulta.getValor() != null ? riMulta.getValor().add(ti.getValor()) : ti.getValor());
									riMulta.setCentrocusto(ti.getTipotaxa().getCentrocusto());
									riMulta.setContagerencial(ti.getTipotaxa().getContadebito());
								} 
								if(ti.getTipotaxa().equals(Tipotaxa.TAXABOLETO) && SinedDateUtils.beforeOrEqualIgnoreHour(ti.getDtlimite(), atual)){
									if(ti.getValor() != null){
										if(mapDocTotalTaxa.get(documento) != null){
											mapDocTotalTaxa.put(documento, mapDocTotalTaxa.get(documento).add(ti.getValor()));
										}else {
											mapDocTotalTaxa.put(documento, ti.getValor());
										}
									}
									riTaxaBoleto.setValor(riTaxaBoleto.getValor() != null ? riTaxaBoleto.getValor().add(ti.getValor()) : ti.getValor());
									riTaxaBoleto.setCentrocusto(ti.getTipotaxa().getCentrocusto());
									riTaxaBoleto.setContagerencial(ti.getTipotaxa().getContadebito());
								} 
								if (ti.getTipotaxa().equals(Tipotaxa.JUROS) && SinedDateUtils.beforeIgnoreHour(ti.getDtlimite(), atual)){
									difdias = SinedDateUtils.diferencaDias(atual,ti.getDtlimite());
									Money valorAux = ti.getValor().multiply(new Money(new Double(difdias)));
									if(valorAux != null){
										if(mapDocTotalTaxa.get(documento) != null){
											mapDocTotalTaxa.put(documento, mapDocTotalTaxa.get(documento).add(valorAux));
										}else {
											mapDocTotalTaxa.put(documento, valorAux);
										}
									}
									riJuros.setValor(riJuros.getValor() != null ? riJuros.getValor().add(valorAux) : valorAux);
									riJuros.setCentrocusto(ti.getTipotaxa().getCentrocusto());
									riJuros.setContagerencial(ti.getTipotaxa().getContadebito());
								}
							}
						}
					}
				}
			} 
		}
		if (riMulta.getContagerencial() != null)
			listaRI.add(riMulta);
		if (riTaxaBoleto.getContagerencial() != null)
			listaRI.add(riTaxaBoleto);
		if (riJuros.getContagerencial() != null)
			listaRI.add(riJuros);
		
		return listaRI;
	}
	
	/**
	 * M�todo para carregar as informa��es do cheque
	 *
	 * @param request
	 * @param cheque
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView carregaInfoCheque(WebRequestContext request, Cheque cheque){
		JsonModelAndView json = new JsonModelAndView();
		cheque = chequeService.load(cheque);
		boolean chequecarregado = false;
		if(cheque != null && cheque.getCdcheque() != null){
			chequecarregado = true;
		}
		
		json.addObject("chequecarregado", chequecarregado);
		json.addObject("cdcheque", cheque.getCdcheque());
		json.addObject("banco", cheque.getBanco());
		json.addObject("agencia", cheque.getAgencia());
		json.addObject("conta", cheque.getConta());
		json.addObject("emitente", cheque.getEmitente());
		json.addObject("numero", cheque.getNumero());
		json.addObject("dtbompara", cheque.getDtbompara());
		json.addObject("valor", cheque.getValor());
		return json;
	}
	
	/**
	 * 
	 * M�todo que verifica se a forma de pagamento escolhida
	 *
	 * @name verificaTED
	 * @param request
	 * @param baixarContaBeanMovimentacao
	 * @return
	 * @return ModelAndView
	 * @author Thiago Augusto
	 * @date 17/04/2012
	 *
	 */
	public ModelAndView verificaFormaPagamento(WebRequestContext request, BaixarContaBeanMovimentacao baixarContaBeanMovimentacao){
		JsonModelAndView json = new JsonModelAndView();
		boolean retornoTED = false;
		boolean retornoDOC = false;
		boolean retornoNF = false;
		BancoFormapagamento bfp = bancoformapagamentoService.findByFormaPagamento(baixarContaBeanMovimentacao.getBancoformapagamento());
		if (bfp.getIdentificador() == 41 || bfp.getIdentificador() == 43)
			retornoTED = true;
		if (bfp.getIdentificador() == 3 || bfp.getIdentificador() == 7)
			retornoDOC = true;
		if (bfp.getIdentificador() == 32)
			retornoNF = true;
		json.addObject("retornoTED", retornoTED);
		json.addObject("retornoDOC", retornoDOC);
		json.addObject("retornoNF", retornoNF);
		return json;
	}
	
	/**
	 * Action que em ajax verifica se o usu�rio logado tem permiss�o para acessar a conta e retorna o saldo.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#carregaContaForSaldoBaixar(Conta conta)
	 * @see br.com.linkcom.sined.geral.service.ContaService#isUsuarioPermissaoConta(Conta conta)
	 * @see br.com.linkcom.sined.geral.service.ContaService#calculaSaldoAtual(Conta conta, Date date, Empresa... empresas)
	 *
	 * @param request
	 * @param baixarContaBeanMovimentacao
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxSaldoVinculo(WebRequestContext request, BaixarContaBeanMovimentacao baixarContaBeanMovimentacao){
		if(baixarContaBeanMovimentacao == null || 
				baixarContaBeanMovimentacao.getVinculo() == null || 
				baixarContaBeanMovimentacao.getVinculo().getCdconta() == null ||
				baixarContaBeanMovimentacao.getDtpagamento() == null){
			return new JsonModelAndView().addObject("permissao", Boolean.FALSE);
		}
		
		Boolean permissao = Boolean.TRUE;
		AuthorizationManager authorizationManager = Neo.getApplicationContext().getAuthorizationManager();
		
		Conta conta = contaService.carregaContaForSaldoBaixar(baixarContaBeanMovimentacao.getVinculo());
		Contatipo contatipo = conta.getContatipo();
		
		// VERIFICA PERMISS�O DA TELA
		if(contatipo.equals(Contatipo.TIPO_CAIXA)){
			boolean autorizadoCaixa = authorizationManager.isAuthorized("/sistema/crud/Caixa", null, SinedUtil.getUsuarioLogado());
			if(!autorizadoCaixa) permissao = Boolean.FALSE;
		} else if(contatipo.equals(Contatipo.TIPO_CONTA_BANCARIA)){
			boolean autorizadoContaBancaria = authorizationManager.isAuthorized("/sistema/crud/Contacorrente", null, SinedUtil.getUsuarioLogado());
			if(!autorizadoContaBancaria) permissao = Boolean.FALSE;
		} else {
			permissao = Boolean.FALSE;
		}
		
		// VERIFICA PERMISS�O NA PR�PRIA CONTA
		if(permissao){
			boolean autorizadoPapel = contaService.isUsuarioPermissaoConta(conta);
			if(!autorizadoPapel) permissao = Boolean.FALSE;
		}
		
		//VERIFICAR O SALDO AO TROCAR A DATA DE PAGAMENTO
		//VERIFICAR O SALDO SE RECARREGAR A P�GINA DEPOIS DO VALIDATEBEAN
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("permissao", permissao);
		
		if(permissao){
			Money saldoatual = contaService.calculaSaldoAtual(conta, baixarContaBeanMovimentacao.getDtpagamento());
			if(saldoatual != null){
				if(saldoatual.getValue().doubleValue() < 0){
					jsonModelAndView.addObject("saldo", "<font color='red'>R$ " + saldoatual.toString() + "</font>");
				} else {
					jsonModelAndView.addObject("saldo", "R$ " + saldoatual.toString());
				}
			}
		}
		
		return jsonModelAndView;
	}
	
	private void preencherCamposMovimentacao(WebRequestContext request, BaixarContaBean baixarContaBean, List<BaixarContaBeanMovimentacao> listaBaixarContaBeanMovimentacao, BaixarContaBeanMovimentacao baixarContaBeanMovimentacao){
		
		List<BaixarContaBeanMovimentacao> listaBaixarContaBeanMovimentacaoTela = baixarContaBean.getListaBaixarContaBeanMovimentacao();
		
		if (!Boolean.TRUE.equals(request.getAttribute("ERRO_BAIXAR_CONTA"))){
			if (listaBaixarContaBeanMovimentacaoTela!=null && !listaBaixarContaBeanMovimentacaoTela.isEmpty()){
				//A primeira posi��o das movimenta��es sempre � criada, assim eu preencho oq vem da tela			
				BaixarContaBeanMovimentacao baixarContaBeanMovimentacaoTela = listaBaixarContaBeanMovimentacaoTela.get(0);
				if (baixarContaBeanMovimentacao.getFormapagamento()==null){
					baixarContaBeanMovimentacao.setFormapagamento(baixarContaBeanMovimentacaoTela.getFormapagamento());
				}
				if (baixarContaBeanMovimentacao.getContatipo()==null){
					baixarContaBeanMovimentacao.setContatipo(baixarContaBeanMovimentacaoTela.getContatipo());
				}
				if (baixarContaBeanMovimentacao.getVinculo()==null && baixarContaBeanMovimentacaoTela.getVinculo()!=null){
					baixarContaBeanMovimentacao.setVinculo(baixarContaBeanMovimentacaoTela.getVinculo());
					request.setAttribute("vinculoid", baixarContaBeanMovimentacaoTela.getVinculo().getCdconta());
				}
				if (baixarContaBeanMovimentacao.getBancoformapagamento()==null){
					baixarContaBeanMovimentacao.setBancoformapagamento(baixarContaBeanMovimentacaoTela.getBancoformapagamento());
				}
				if (baixarContaBeanMovimentacao.getBancotipopagamento()==null){
					baixarContaBeanMovimentacao.setBancotipopagamento(baixarContaBeanMovimentacaoTela.getBancotipopagamento());
				}
				if (baixarContaBeanMovimentacao.getFinalidadedoc()==null){
					baixarContaBeanMovimentacao.setFinalidadedoc(baixarContaBeanMovimentacaoTela.getFinalidadedoc());
				}
				if (baixarContaBeanMovimentacao.getFinalidadeted()==null){
					baixarContaBeanMovimentacao.setFinalidadeted(baixarContaBeanMovimentacaoTela.getFinalidadeted());
				}
				if (baixarContaBeanMovimentacao.getGerararquivoremessa()==null){
					baixarContaBeanMovimentacao.setGerararquivoremessa(baixarContaBeanMovimentacaoTela.getGerararquivoremessa());
				}
				if (baixarContaBeanMovimentacao.getChecknum()==null){
					baixarContaBeanMovimentacao.setChecknum(baixarContaBeanMovimentacaoTela.getChecknum());
				}
				if (baixarContaBeanMovimentacao.getHistorico()==null){
					baixarContaBeanMovimentacao.setHistorico(baixarContaBeanMovimentacaoTela.getHistorico());
				}
				if (baixarContaBeanMovimentacao.getComprovante()==null){
					baixarContaBeanMovimentacao.setComprovante(baixarContaBeanMovimentacaoTela.getComprovante());
				}
				if (baixarContaBeanMovimentacao.getBancocheque()==null){
					baixarContaBeanMovimentacao.setBancocheque(baixarContaBeanMovimentacaoTela.getBancocheque());
				}
				if (baixarContaBeanMovimentacao.getAgenciacheque()==null){
					baixarContaBeanMovimentacao.setAgenciacheque(baixarContaBeanMovimentacaoTela.getAgenciacheque());
				}
				if (baixarContaBeanMovimentacao.getContacheque()==null){
					baixarContaBeanMovimentacao.setContacheque(baixarContaBeanMovimentacaoTela.getContacheque());
				}
				if (baixarContaBeanMovimentacao.getNumerocheque()==null){
					baixarContaBeanMovimentacao.setNumerocheque(baixarContaBeanMovimentacaoTela.getNumerocheque());
				}
				if (baixarContaBeanMovimentacao.getEmitentecheque()==null){
					baixarContaBeanMovimentacao.setEmitentecheque(baixarContaBeanMovimentacaoTela.getEmitentecheque());
				}
				if (baixarContaBeanMovimentacao.getCpfcnpjcheque()==null){
					baixarContaBeanMovimentacao.setCpfcnpjcheque(baixarContaBeanMovimentacaoTela.getCpfcnpjcheque());
				}
				if (baixarContaBeanMovimentacao.getDtbomparacheque()==null){
					baixarContaBeanMovimentacao.setDtbomparacheque(baixarContaBeanMovimentacaoTela.getDtbomparacheque());
				}
				if (baixarContaBeanMovimentacao.getValorcheque()==null){
					baixarContaBeanMovimentacao.setValorcheque(baixarContaBeanMovimentacaoTela.getValorcheque());
				}
				if (baixarContaBeanMovimentacao.getValor()==null){
					baixarContaBeanMovimentacao.setValor(baixarContaBeanMovimentacaoTela.getValor());
				}
				if (baixarContaBeanMovimentacao.getDiscriminarvalortaxa()==null){
					baixarContaBeanMovimentacao.setDiscriminarvalortaxa(baixarContaBeanMovimentacaoTela.getDiscriminarvalortaxa());
				}
				if (baixarContaBeanMovimentacao.getValortaxa()==null){
					baixarContaBeanMovimentacao.setValortaxa(baixarContaBeanMovimentacaoTela.getValortaxa());
				}
				if (baixarContaBeanMovimentacao.getCdchequecheque()==null){
					baixarContaBeanMovimentacao.setCdchequecheque(baixarContaBeanMovimentacaoTela.getCdchequecheque());
				}
				if (baixarContaBeanMovimentacao.getChequeauxiliar()==null){
					baixarContaBeanMovimentacao.setChequeauxiliar(baixarContaBeanMovimentacaoTela.getChequeauxiliar());
				}
				if (baixarContaBeanMovimentacao.getGerararquivoregistrocobranca()==null){
					baixarContaBeanMovimentacao.setGerararquivoregistrocobranca(baixarContaBeanMovimentacaoTela.getGerararquivoregistrocobranca());
				}
			}
			
			listaBaixarContaBeanMovimentacao.add(baixarContaBeanMovimentacao);
			
			//A partir da segunda posi��o, se tiver preenchido na tela, retornar com ela
			if (listaBaixarContaBeanMovimentacaoTela!=null && listaBaixarContaBeanMovimentacaoTela.size()>1){
				listaBaixarContaBeanMovimentacao.addAll(listaBaixarContaBeanMovimentacaoTela.subList(1, listaBaixarContaBeanMovimentacaoTela.size()));
			}
			
			baixarContaBean.setListaBaixarContaBeanMovimentacao(listaBaixarContaBeanMovimentacao);
		}else if (SinedUtil.isListNotEmpty(baixarContaBean.getListaBaixarContaBeanMovimentacao())){
			for(BaixarContaBeanMovimentacao bcbm : baixarContaBean.getListaBaixarContaBeanMovimentacao()){
				if(bcbm.getVinculoaux() == null && bcbm.getVinculo() != null){
					bcbm.setVinculoaux(bcbm.getVinculo());
				}
			}
		}		
	}
	
	/**
	 * M�todo que abre popup para leitura de c�digo de barras de cheque
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#abrirPopupCodigobarrascheque(WebRequestContext request)
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopupCodigobarrascheque(WebRequestContext request){
		return documentoService.abrirPopupCodigobarrascheque(request);
	}
	
	/**
	 * M�todo que retorna o valor de saldo de um valecompra
	 * @param request
	 * @param valecompra
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView ajaxSaldoValecompra(WebRequestContext request, Cliente cliente){
		JsonModelAndView json = new JsonModelAndView();
		
		if(cliente != null && cliente.getCdpessoa() != null){
			json.addObject("saldoValecompra", valecompraService.getSaldoByCliente(cliente));
		}
		
		return json;
	}
	
	public ModelAndView ajaxBuscaHistoricoOperacao(WebRequestContext request, BaixarContaBean bean){
		return documentoService.ajaxBuscaHistoricoOperacaoForBaixarConta(request, bean);
	}
	
	public ModelAndView ajaxBuscaTaxaBaixaAcrescimo(WebRequestContext request){
		Tipotaxa tipotaxa = tipotaxaService.loadForEntrada(Tipotaxa.TAXABAIXA_ACRESCIMO);
		return new JsonModelAndView().addObject("tipotaxa", tipotaxa);
	}
	
	public ModelAndView ajaxBuscaDadosCheque(WebRequestContext request, Cheque cheque) {
		return chequeService.ajaxBuscaDadosCheque(request, cheque);
	}
	
	public ModelAndView ajaxValidaDocumentotipo(WebRequestContext request){
		return documentoService.ajaxValidaDocumentotipoForBaixa(request);
	}
}
