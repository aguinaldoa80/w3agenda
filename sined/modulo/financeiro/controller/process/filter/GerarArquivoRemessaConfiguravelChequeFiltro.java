package br.com.linkcom.sined.modulo.financeiro.controller.process.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacao;

public class GerarArquivoRemessaConfiguravelChequeFiltro {
	
	private Empresa empresa;
	private Conta conta;
	private BancoConfiguracaoRemessa bancoConfiguracaoRemessa;
	private Date dtmovimentacaoInicio;
	private Date dtmovimentacaoFim;
	private List<Movimentacao> listaMovimentacao;
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Conta banc�ria")
	public Conta getConta() {
		return conta;
	}
	@Required
	@DisplayName("Configura��o de remessa banc�ria")
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa() {
		return bancoConfiguracaoRemessa;
	}
	@Required
	@DisplayName("Data (de)")
	public Date getDtmovimentacaoInicio() {
		return dtmovimentacaoInicio;
	}
	@Required
	@DisplayName("Data (at�)")
	public Date getDtmovimentacaoFim() {
		return dtmovimentacaoFim;
	}
	public List<Movimentacao> getListaMovimentacao() {
		return listaMovimentacao;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setBancoConfiguracaoRemessa(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		this.bancoConfiguracaoRemessa = bancoConfiguracaoRemessa;
	}
	public void setDtmovimentacaoInicio(Date dtmovimentacaoInicio) {
		this.dtmovimentacaoInicio = dtmovimentacaoInicio;
	}
	public void setDtmovimentacaoFim(Date dtmovimentacaoFim) {
		this.dtmovimentacaoFim = dtmovimentacaoFim;
	}
	public void setListaMovimentacao(List<Movimentacao> listaMovimentacao) {
		this.listaMovimentacao = listaMovimentacao;
	}	
}