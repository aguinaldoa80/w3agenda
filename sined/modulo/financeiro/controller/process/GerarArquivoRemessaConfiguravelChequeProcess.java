package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaSegmento;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.BancoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.ContaBancariaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.EmpresaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.GeralVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.MovimentacaoVO;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.RemessaVO;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRemessaSegmentoService;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRemessaService;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.filter.GerarArquivoRemessaConfiguravelChequeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.bancoconfiguracao.BancoConfiguracaoRemessaUtil;

@Bean
@Controller(path="/financeiro/process/GerarArquivoRemessaConfiguravelCheque", authorizationModule=ProcessAuthorizationModule.class)
public class GerarArquivoRemessaConfiguravelChequeProcess extends MultiActionController {
	
	private BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService;
	private MovimentacaoService movimentacaoService;
	private ContaService contaService;
	private BancoService bancoService;
	private EmpresaService empresaService;
	private BancoConfiguracaoRemessaSegmentoService bancoConfiguracaoRemessaSegmentoService;
	private DocumentoService documentoService;
	
	public void setBancoConfiguracaoRemessaService(BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService) {this.bancoConfiguracaoRemessaService = bancoConfiguracaoRemessaService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setBancoConfiguracaoRemessaSegmentoService(BancoConfiguracaoRemessaSegmentoService bancoConfiguracaoRemessaSegmentoService) {this.bancoConfiguracaoRemessaSegmentoService = bancoConfiguracaoRemessaSegmentoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, GerarArquivoRemessaConfiguravelChequeFiltro filtro) throws Exception {
		return new ModelAndView("process/gerarArquivoRemessaConfiguravelCheque", "filtro", filtro);
	}
	
	public ModelAndView ajaxVerificaConfiguracaoRemessa(WebRequestContext request, GerarArquivoRemessaConfiguravelChequeFiltro filtro) throws Exception {
		String mensagem = "";
		
		if (filtro.getConta()!=null && !bancoConfiguracaoRemessaService.isPossuiBancoConfiguracaoRemessa(filtro.getConta())){
			mensagem = "Gera��o de remessa n�o dispon�vel.";
		}
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("mensagem", mensagem);
		
		return jsonModelAndView;
	}
	
	public ModelAndView filtrar(WebRequestContext request, GerarArquivoRemessaConfiguravelChequeFiltro filtro) throws Exception {
		filtro.setListaMovimentacao(movimentacaoService.findForGerarArquivoRemessaConfiguravelCheque(filtro));
		return index(request, filtro);
	}	
	
	public ModelAndView gerarArquivo(WebRequestContext request, GerarArquivoRemessaConfiguravelChequeFiltro filtro) throws Exception {
		List<Movimentacao> listaMovimentacao = new ArrayList<Movimentacao>();
		
		for (Movimentacao movimentacao: filtro.getListaMovimentacao()){
			if (Boolean.TRUE.equals(movimentacao.getSelecao())){
				listaMovimentacao.add(movimentacao);
			}
		}
		
		Conta conta = contaService.carregaConta(filtro.getConta(), null);
		
		GeralVO geralVO = new GeralVO();	
		BancoVO bancoVO = bancoService.getBancoForRemessaConfiguravel(conta.getBanco());
		ContaBancariaVO contaBancariaVO = contaService.getContaForRemessaConfiguravel(conta, null);
		EmpresaVO empresaVO = filtro.getEmpresa()!=null ? empresaService.getEmpresaForRemessaConfiguravel(filtro.getEmpresa()) : new EmpresaVO();
		EmpresaVO empresatitularVO = conta.getEmpresatitular()!=null ? empresaService.getEmpresaForRemessaConfiguravel(conta.getEmpresatitular()) : null;
		List<MovimentacaoVO> listaMovimentacaoVO = movimentacaoService.getListaMovimentacaoForRemessaConfiguravel(listaMovimentacao);
		
		RemessaVO remessaVO = new RemessaVO();
		remessaVO.setGeralVO(geralVO);
		remessaVO.setBancoVO(bancoVO);
		remessaVO.setContaBancariaVO(contaBancariaVO);
		remessaVO.setEmpresaVO(empresaVO);
		remessaVO.setEmpresatitularVO(empresatitularVO);
		remessaVO.setListaMovimentacaoVO(listaMovimentacaoVO);
		
		if(conta.getSequencialcheque() != null){
			geralVO.setSequencialcheque(conta.getSequencialcheque());
			remessaVO.setUtilizouSequencialcheque(true);
		}
		
		List<BancoConfiguracaoRemessaSegmento> listaBancoConfiguracaoRemessaSegmento = bancoConfiguracaoRemessaSegmentoService.findAllByBancoConfiguracaoRemessa(filtro.getBancoConfiguracaoRemessa());

		BancoConfiguracaoRemessaUtil bancoConfiguracaoRemessaUtil = new BancoConfiguracaoRemessaUtil();
		String stringArquivo = bancoConfiguracaoRemessaUtil.gerarDadosArquivoRemessaConfiguravel(listaBancoConfiguracaoRemessaSegmento, remessaVO);
		
		if (remessaVO.getListaErro()!=null && !remessaVO.getListaErro().isEmpty()){
			StringBuilder erros = new StringBuilder();
			for (String erro: remessaVO.getListaErro()){
				erros.append(erro).append("<br/>");
			}	
			request.addMessage(erros.toString(), MessageType.ERROR);
			return index(request, filtro);
		}
		
		if(remessaVO.isUtilizouSequencialcheque())
			contaService.setProximoSequencialcheque(conta);
		
		String nomeArquivo = "CHEQUE_" + new SimpleDateFormat("ddMMyyyy").format(SinedDateUtils.currentDate()) + ".REM";
		Resource resource = new Resource("text/plain", nomeArquivo, stringArquivo.getBytes());
		documentoService.saveAndUpdateArquivoRemessa(resource, null, nomeArquivo, null, listaMovimentacao);
		
		return new ResourceModelAndView(resource);
	}	
}