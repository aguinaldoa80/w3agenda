package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Despesaviagemitem;
import br.com.linkcom.sined.geral.bean.Despesaviagemprojeto;
import br.com.linkcom.sined.geral.bean.Despesaviagemtipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemacao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.DespesaviagemitemService;
import br.com.linkcom.sined.geral.service.DespesaviagemtipoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaohistoricoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/financeiro/process/FluxoStatusDespesaviagem", authorizationModule=ProcessAuthorizationModule.class)
public class FluxoStatusDespesaviagemProcess extends MultiActionController {
	
	private DespesaviagemService despesaviagemService;
	private DespesaviagemtipoService despesaviagemtipoService;
	private DespesaviagemitemService despesaviagemitemService;
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ProjetoService projetoService;
	private RateioService rateioService;
	private MovimentacaohistoricoService movimentacaohistoricoService;
	private DocumentohistoricoService documentohistoricoService;
	private ContapagarService contapagarService;
	
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setMovimentacaohistoricoService(
			MovimentacaohistoricoService movimentacaohistoricoService) {
		this.movimentacaohistoricoService = movimentacaohistoricoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setFechamentofinanceiroService(
			FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setDespesaviagemitemService(
			DespesaviagemitemService despesaviagemitemService) {
		this.despesaviagemitemService = despesaviagemitemService;
	}
	public void setDespesaviagemtipoService(
			DespesaviagemtipoService despesaviagemtipoService) {
		this.despesaviagemtipoService = despesaviagemtipoService;
	}
	public void setDespesaviagemService(
			DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	
	/**
	 * A��o para autorizar as despesas de viagem selecionadas.
	 *
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#findForVerificacao
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#updateSituacao
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("autorizar")
	public ModelAndView autorizar(WebRequestContext request){		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Despesaviagem> lista = despesaviagemService.findForVerificacao(whereIn);
		
		for (Despesaviagem despesaviagem : lista) {
			if (!despesaviagem.getSituacaodespesaviagem().equals(Situacaodespesaviagem.EM_ABERTO)) {
				request.addError("Para autorizar, todas as despesas de viagem selecionadas tem que estar com a situa��o igual a 'EM ABERTO'.");
				if("entrada".equals(request.getParameter("controller"))){
					return new ModelAndView("redirect:/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+whereIn);
				} else {
					return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
				}
			}
		}
		
		despesaviagemService.updateSituacao(whereIn, Situacaodespesaviagem.AUTORIZADA);
		despesaviagemService.enviaEmailAutorizacao(whereIn);
		
		despesaviagemService.salvaHistorico(whereIn, Despesaviagemacao.AUTORIZADA, null);
		
		request.addMessage("Despesa(s) de viagem autorizada(s) com sucesso.");
		
		if("entrada".equals(request.getParameter("controller"))){
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+whereIn);
		} else {
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
		}
	}
	
	/**
	 * A��o para autorizar as despesas de viagem selecionadas.
	 *
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#findForVerificacao
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#updateSituacao
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemitemService#updateValoresRealizadas
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#deleteDocumentoFromDespesa
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#deleteFromDespesa
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("estornar")
	public ModelAndView estornar(WebRequestContext request){		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Despesaviagem> lista = despesaviagemService.findForVerificacao(whereIn);
		
		for (Despesaviagem despesaviagem : lista) {
			if (despesaviagem.getSituacaodespesaviagem().equals(Situacaodespesaviagem.EM_ABERTO)) {
				request.addError("Para estornar, todas as despesas de viagem selecionadas tem que estar com a situa��o diferente de 'EM ABERTO'.");
				if("entrada".equals(request.getParameter("controller"))){
					return new ModelAndView("redirect:/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+whereIn);
				} else {
					return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
				}
			}
		}
		
		if (despesaviagemService.podeEstornar(whereIn)) {
			
			despesaviagemService.updateSituacao(whereIn, Situacaodespesaviagem.EM_ABERTO);
			despesaviagemitemService.updateZerarValoresRealizadas(whereIn);
			documentoService.deleteDocumentoFromDespesa(whereIn);
			movimentacaoService.deleteFromDespesa(whereIn);
			despesaviagemService.salvaHistorico(whereIn, Despesaviagemacao.ESTORNADA, null);
			
			request.addMessage("Despesa(s) de viagem estornada(s) com sucesso.");
		} else {
			request.addError("N�o foi poss�vel estornar a(s) despesa(s) de viagem pois a(s) conta(s) a pagar e receber est�o com a situa��o diferente de 'PREVISTA' ou " +
					"a(s) movimenta��o(�es) est�o com situa��o 'NORMAL'.");
		}
		if("entrada".equals(request.getParameter("controller"))){
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+whereIn);
		} else {
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
		}
	}
	
//	/**
//	 * A��o para baixar as despesas de viagem selecionadas.
//	 *
//	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#findForVerificacao
//	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#updateSituacao
//	 * @param request
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	@Action("baixar")
//	public ModelAndView baixar(WebRequestContext request){		
//		String whereIn = SinedUtil.getItensSelecionados(request);
//		
//		List<Despesaviagem> lista = despesaviagemService.findForVerificacao(whereIn);
//		
//		for (Despesaviagem despesaviagem : lista) {
//			if (!despesaviagem.getSituacaodespesaviagem().equals(Situacaodespesaviagem.REALIZADA)) {
//				request.addError("Para baixar, todas as despesas de viagem selecionadas tem que estar com a situa��o igual a 'REALIZADA'.");
//				return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
//			}
//		}
//		
//		despesaviagemService.updateSituacao(whereIn, Situacaodespesaviagem.BAIXADA);
//		
//		request.addMessage("Despesa(s) de viagem baixada(s) com sucesso.");
//		
//		if("entrada".equals(request.getParameter("controller"))){
//			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+whereIn);
//		} else {
//			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
//		}
//	}
	
	/**
	 * A��o para cancelar as despesas de viagem selecionadas.
	 *
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#findForVerificacao
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#updateSituacao
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("cancelar")
	public ModelAndView cancelar(WebRequestContext request){		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Despesaviagem> lista = despesaviagemService.findForVerificacao(whereIn);
		
		for (Despesaviagem despesaviagem : lista) {
			if (!despesaviagem.getSituacaodespesaviagem().equals(Situacaodespesaviagem.EM_ABERTO)) {
				request.addError("Para cancelar, todas as despesas de viagem selecionadas tem que estar com a situa��o igual a 'EM ABERTO'.");
				if("entrada".equals(request.getParameter("controller"))){
					return new ModelAndView("redirect:/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+whereIn);
				} else {
					return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
				}
			}
		}
		
		despesaviagemService.updateSituacao(whereIn, Situacaodespesaviagem.CANCELADA);
		despesaviagemService.salvaHistorico(whereIn, Despesaviagemacao.CANCELADA, null);
		
		request.addMessage("Despesa(s) de viagem cancelada(s) com sucesso.");
		if("entrada".equals(request.getParameter("controller"))){
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+whereIn);
		} else {
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
		}
	}
	
	
	/**
	 * A��o para realizar as despesas de viagem selecionadas.
	 *
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.FluxoStatusDespesaviagemProcess#fecharModalAtualizar
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#findForAcerto
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("gerarAcerto")
	public ModelAndView gerarAcerto(WebRequestContext request){		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Despesaviagem> despesas = despesaviagemService.findForAcerto(whereIn);
		Despesaviagem despesaviagem = new Despesaviagem();
		Colaborador colaborador = null;
		for (Despesaviagem d : despesas) {
			despesaviagem = d;
			if(colaborador == null){
				colaborador = d.getColaborador();
			} else if(!colaborador.equals(d.getColaborador())){
				request.addError("Para gerar acerto, a(s) despesa(s) de viagem selecionada(s) tem que ser do mesmo respons�vel.");
				this.fecharModalAtualizar(request);
				return null;
			}
			if (!d.getSituacaodespesaviagem().equals(Situacaodespesaviagem.REALIZADA)) {
				request.addError("Para gerar acerto, a(s) despesa(s) de viagem selecionada(s) tem que estar com a situa��o igual a 'REALIZADA'.");
				this.fecharModalAtualizar(request);
				return null;
			}
		}
		
		List<Movimentacao> listMovimentacaoAdiantamento = movimentacaoService.findAdiantamentoByDespesaviagem(whereIn);
		if(SinedUtil.isListNotEmpty(listMovimentacaoAdiantamento)){
			for(Movimentacao movimentacao : listMovimentacaoAdiantamento){
				if(SinedUtil.isListNotEmpty(movimentacao.getListaMovimentacaoorigem())){
					for(Movimentacaoorigem mo : movimentacao.getListaMovimentacaoorigem()){
						if(mo.getDespesaviagemadiantamento() != null && !despesas.contains(mo.getDespesaviagemadiantamento())){
							request.addError("A despesa de viagem " + whereIn + 
									" participou de um adiantamento feito de forma agrupada. O acerto deve contemplar as mesmas despesas de viagem do adiantamento.");
							this.fecharModalAtualizar(request);
							return null;
						}
					}
				}
			}
		}
		
		List<Documento> listaContapagarAdiantamento = contapagarService.findAdiantamentoByDespesaviagem(whereIn);
		if(SinedUtil.isListNotEmpty(listaContapagarAdiantamento)){
			for(Documento documento : listaContapagarAdiantamento){
				if(SinedUtil.isListNotEmpty(documento.getListaDocumentoOrigem())){
					for(Documentoorigem docOrigem : documento.getListaDocumentoOrigem()){
						if(docOrigem.getDespesaviagemadiantamento() != null && !despesas.contains(docOrigem.getDespesaviagemadiantamento())){
							request.addError("A despesa de viagem " + whereIn + 
									" participou de um adiantamento feito de forma agrupada. O acerto deve contemplar as mesmas despesas de viagem do adiantamento.");
							this.fecharModalAtualizar(request);
							return null;
						}
					}
				}
			}
		}
		
		despesaviagem.setWhereIn(whereIn);

		if("entrada".equals(request.getParameter("controller"))){
			despesaviagem.setController("entrada");
		} else if("listagem".equals(request.getParameter("controller"))){
			despesaviagem.setController("listagem");
		}
		
		Double diferenca = 0d; 
			
		for (Despesaviagem d : despesas) {
			diferenca += despesaviagemService.calculaDiferencaAcerto(d);
		}
			
		if(diferenca.equals(0.0)){
			despesaviagemService.updateSituacao(whereIn, Situacaodespesaviagem.BAIXADA);
			request.addMessage("Despesa(s) de viagem baixada(s) com sucesso.");
			
			this.fecharModalAtualizar(request);
			return null;
		}
		
		despesaviagem.setPagar(diferenca > 0);
		despesaviagem.setValoracerto(new Money((diferenca > 0 ? diferenca : (diferenca*(-1))), true));

		return new ModelAndView("direct:/crud/popup/despesaviagemAcerto", "despesaviagem", despesaviagem);
	}
	
	@Action("goAcerto")
	public ModelAndView goAcerto(WebRequestContext request, Despesaviagem dv){
		if (dv == null) {
			throw new SinedException("Despesa de viagem n�o pode ser nulo.");
		}
		
		if (dv.getFormapagamento()) {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '"+request.getServletRequest().getContextPath()+"/financeiro/crud/Conta"+(dv.getPagar() ? "pagar" : "receber")+"?ACAO=fromDespesaViagemAcerto&whereInDespesaviagem="+dv.getWhereIn()+"&valor="+dv.getValoracerto().toString()+"&controller="+dv.getController()+"';parent.$.akModalRemove(true);</script>");
			return null;
		} else {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '"+request.getServletRequest().getContextPath()+"/financeiro/crud/Movimentacao?ACAO=fromDespesaViagemAcerto&whereInDespesaviagem="+dv.getWhereIn()+"&valor="+dv.getValoracerto().toString()+"&tipooperacao.cdtipooperacao="+(dv.getPagar() ? Tipooperacao.TIPO_DEBITO.getCdtipooperacao() :  Tipooperacao.TIPO_CREDITO.getCdtipooperacao())+"&controller="+dv.getController()+"';parent.$.akModalRemove(true);</script>");
			return null;
		}
	}
	
	/**
	 * A��o para realizar as despesas de viagem selecionadas.
	 *
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.FluxoStatusDespesaviagemProcess#fecharModalAtualizar
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#findForRelizacao
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("gerarAdiantamento")
	public ModelAndView gerarAdiantamento(WebRequestContext request){		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
//		Despesaviagem despesaviagem = despesaviagemService.findForAdiatamento(whereIn);
		
		Despesaviagem despesaviagem = new Despesaviagem();
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.findForAdiatamentoByDespesaviagem(whereIn);
		
		if("entrada".equals(request.getParameter("controller"))){
			despesaviagem.setController("entrada");
		} else if("listagem".equals(request.getParameter("controller"))){
			despesaviagem.setController("listagem");
		}
		
		StringBuilder ids = new StringBuilder();
		Colaborador colaborador = null;
		for(Despesaviagem dv : listaDespesaviagem){
			if (!dv.getSituacaodespesaviagem().equals(Situacaodespesaviagem.AUTORIZADA)) {
				request.addError("Para gerar adiantamento, a despesa de viagem " + dv.getCddespesaviagem() + " tem que estar com a situa��o igual a 'AUTORIZADA'.");
				this.fecharModalAtualizar(request);
				return null;
			}else {
				if(colaborador == null){
					colaborador = dv.getColaborador();
				}else if(dv.getColaborador() != null && !colaborador.equals(dv.getColaborador())){
					request.addError("S� � permitido gerar adiantamento da(s) despesa(s) de viagem para o mesmo colaborador.");
					this.fecharModalAtualizar(request);
					return null;
				}
				
			}
			if(!"".equals(ids.toString())) ids.append(",");
			ids.append(dv.getCddespesaviagem());
		}

		despesaviagem.setWhereInDespesas(ids.toString());
		return new ModelAndView("direct:/crud/popup/despesaviagemAdiantamento","despesaviagem",despesaviagem);
	}
	
	/**
	 * Vai para a tela de conta a pagar ou movimenta��o financeira para gerar um adiantamento.
	 *
	 * @param request
	 * @param dv
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("goAdiantamento")
	public ModelAndView goAdiantamento(WebRequestContext request, Despesaviagem dv){
		
		if (dv == null || (dv.getWhereInDespesas() == null || "".equals(dv.getWhereInDespesas())) ) {
			throw new SinedException("Despesa de viagem n�o pode ser nulo.");
		}
		
		if (dv.getFormapagamento()) {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '"+request.getServletRequest().getContextPath()+"/financeiro/crud/Contapagar?ACAO=fromDespesaViagemAdiantamento&whereInDespesaviagem="+dv.getWhereInDespesas()+"&controller="+dv.getController()+"';parent.$.akModalRemove(true);</script>");
			return null;
		} else {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
					"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
					"<script>parent.location = '"+request.getServletRequest().getContextPath()+"/financeiro/crud/Movimentacao?ACAO=fromDespesaViagemAdiantamento&whereInDespesaviagem="+dv.getWhereInDespesas()+"&tipooperacao.cdtipooperacao=" + Tipooperacao.TIPO_DEBITO.getCdtipooperacao() +"&controller="+dv.getController()+"';parent.$.akModalRemove(true);</script>");
			return null;
		}
	}
	
	/**
	 * A��o para realizar as despesas de viagem selecionadas.
	 *
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.FluxoStatusDespesaviagemProcess#fecharModalAtualizar
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#findForRelizacao
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("realizar")
	public ModelAndView realizar(WebRequestContext request){		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Despesaviagem> despesas = despesaviagemService.findForRealizacao(whereIn);
		
		Despesaviagem despesaviagem = new Despesaviagem();
		Colaborador colaborador = null;
		List<Despesaviagemitem> listaDespesaviagemitem = new ArrayList<Despesaviagemitem>();
		
		for (Despesaviagem d : despesas) {
			if(colaborador == null){
				colaborador = d.getColaborador();
			} else if(!colaborador.equals(d.getColaborador())){
				request.addError("Para realizar, a(s) despesa(s) de viagem selecionada(s) tem que ser do mesmo respons�vel.");
				this.fecharModalAtualizar(request);
				return null;
			}
			if (!d.getSituacaodespesaviagem().equals(Situacaodespesaviagem.AUTORIZADA)) {
				request.addError("Para realizar, a(s) despesa(s) de viagem selecionada(s) tem que estar com a situa��o igual a 'AUTORIZADA'.");
				this.fecharModalAtualizar(request);
				return null;
			}
			
			listaDespesaviagemitem.addAll(d.getListaDespesaviagemitem());
		}
		
		// AGRUPA POR TIPO DE ITEM 
		listaDespesaviagemitem = despesaviagemitemService.agrupaItensForRealizacao(listaDespesaviagemitem);
		
		Collections.sort(listaDespesaviagemitem, new Comparator<Despesaviagemitem>(){
			public int compare(Despesaviagemitem o1, Despesaviagemitem o2) {
				return o1.getDespesaviagemtipo().getNome().compareTo(o2.getDespesaviagemtipo().getNome());
			}
		});
		
		despesaviagem.setWhereIn(whereIn);
		despesaviagem.setListaDespesaviagem(despesas);
		despesaviagem.setListaDespesaviagemitem(listaDespesaviagemitem);
		
		if("entrada".equals(request.getParameter("controller"))){
			despesaviagem.setController("entrada");
		} else if("listagem".equals(request.getParameter("controller"))){
			despesaviagem.setController("listagem");
		}
		

		return new ModelAndView("direct:/crud/popup/despesaviagemRealizar", "despesaviagem", despesaviagem);
	}
	
	/**
	 * Salva o registro de despesa de viagem depois do popup de realiza��o.
	 *
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#updateDtretornorealizado
	 * @see br.com.linkcom.sined.geral.service.DespesaviagemService#updateSituacao
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.FluxoStatusDespesaviagemProcess#fecharModalAtualizar
	 * @param request
	 * @param despesaviagem
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("saveRealizada")
	public ModelAndView saveRealizada(WebRequestContext request, Despesaviagem despesaviagem){
		
		List<Despesaviagemitem> listaDespesaviagemitemTela = despesaviagem.getListaDespesaviagemitem();
		List<Despesaviagem> listaDespesaviagem = despesaviagem.getListaDespesaviagem();
		
		String whereInDespesa = CollectionsUtil.listAndConcatenate(listaDespesaviagem, "cddespesaviagem", ",");
		String[] whereCdDespesa = null; 
		Integer qtdeDespesa = 1;
		Despesaviagemitem novaDespesaviagemitem;
		if(whereInDespesa != null && !"".equals(whereInDespesa)){
			whereCdDespesa = whereInDespesa.split(",");
			qtdeDespesa = whereCdDespesa.length;
		}
		for (Despesaviagem d : listaDespesaviagem) {
			despesaviagemService.updateDtretornorealizado(d);
		}
		despesaviagemService.updateSituacao(despesaviagem.getWhereIn(), Situacaodespesaviagem.REALIZADA);
		
		String whereInItemTodos = CollectionsUtil.listAndConcatenate(listaDespesaviagemitemTela, "whereInItens", ","); 
		List<Despesaviagemitem> listaDespesaviagemitemBanco = despesaviagemitemService.findForSaveRealizacao(whereInItemTodos);
		
		for (Despesaviagemitem despesaviagemitem : listaDespesaviagemitemTela) {
			Money valorPrevistoAcumulado = new Money();
			Money valorRealizadoAcumulado = new Money();
			String whereInItens = despesaviagemitem.getWhereInItens();
			if(whereInItens == null || "".equals(whereInItens)){
				if(whereCdDespesa != null && whereCdDespesa.length > 0 && despesaviagemitem.getValorrealizado() != null){
					for (int i = 0; i < whereCdDespesa.length; i++) {
						String cddespesastr = whereCdDespesa[i];
						novaDespesaviagemitem = new Despesaviagemitem();
						novaDespesaviagemitem.setDespesaviagem(new Despesaviagem(Integer.parseInt(cddespesastr)));
						novaDespesaviagemitem.setDespesaviagemtipo(despesaviagemitem.getDespesaviagemtipo());
						if(despesaviagemitem.getValorprevisto() != null){
							novaDespesaviagemitem.setValorprevisto(despesaviagemitem.getValorprevisto().divide(new Money(qtdeDespesa)));
							valorPrevistoAcumulado = valorPrevistoAcumulado.add(novaDespesaviagemitem.getValorprevisto().round());
						}
						if(despesaviagemitem.getValorrealizado() != null){
							novaDespesaviagemitem.setValorrealizado(despesaviagemitem.getValorrealizado().divide(new Money(qtdeDespesa)));
							valorRealizadoAcumulado = valorRealizadoAcumulado.add(novaDespesaviagemitem.getValorrealizado().round());
						}
						
						// Caso valor acumulado divergir do valor total, o ultimo registro 
						// sofrer� arredondamento para garantir a integralidade dos valores divididos;
						if(i+1 == whereCdDespesa.length){
							if(!valorPrevistoAcumulado.equals(despesaviagemitem.getValorprevisto())){
								Money diferenca = despesaviagemitem.getValorprevisto().subtract(valorPrevistoAcumulado);
								novaDespesaviagemitem.setValorprevisto(novaDespesaviagemitem.getValorprevisto().add(diferenca));
							}
							if(!valorRealizadoAcumulado.equals(despesaviagemitem.getValorrealizado())){
								Money diferenca = despesaviagemitem.getValorrealizado().subtract(valorRealizadoAcumulado);
								novaDespesaviagemitem.setValorrealizado(novaDespesaviagemitem.getValorrealizado().add(diferenca));
							}
						}
						despesaviagemitemService.saveOrUpdate(novaDespesaviagemitem);
					}
				}
			}else {
				String[] idsItens = whereInItens.split(",");
				if(idsItens.length == 1){
					despesaviagemitem.setCddespesaviagemitem(Integer.parseInt(idsItens[0]));
					despesaviagemitemService.updateValorRealizado(despesaviagemitem, despesaviagemitem.getValorrealizado());
				} else {
					for (int i = 0; i < idsItens.length; i++) {
						Despesaviagemitem it = new Despesaviagemitem();
						it.setCddespesaviagemitem(Integer.parseInt(idsItens[i]));
						
						it = listaDespesaviagemitemBanco.get(listaDespesaviagemitemBanco.indexOf(it));
						
						if(it != null){
							Money percent = new Money();
							if(despesaviagemitem.getValorprevisto() != null && despesaviagemitem.getValorprevisto().getValue().doubleValue() > 0)
								percent = it.getValorprevisto().multiply(new Money(100d)).divide(despesaviagemitem.getValorprevisto());
							Money valorealizadoproporcional = despesaviagemitem.getValorrealizado().multiply(percent).divide(new Money(100d));
							
							valorRealizadoAcumulado = valorRealizadoAcumulado.add(valorealizadoproporcional).round();
							
							// Caso valor acumulado divergir do valor total, o ultimo registro 
							// sofrer� arredondamento para garantir a integralidade dos valores divididos;
							if(i+1 == idsItens.length && !valorRealizadoAcumulado.equals(despesaviagemitem.getValorrealizado())){
								Money diferenca = despesaviagemitem.getValorrealizado().subtract(valorRealizadoAcumulado);
								valorealizadoproporcional = valorealizadoproporcional.add(diferenca);
							}
							despesaviagemitemService.updateValorRealizado(it, valorealizadoproporcional);
						}
					}
				}
			}
		}
		
		despesaviagemService.salvaHistorico(whereInDespesa, Despesaviagemacao.REALIZADA, null);
		
		request.addMessage("Despesa de viagem realizada com sucesso.");
		this.fecharModalAtualizar(request);
		return null;
	}
	
	/**
	 * Ajax para preencher o combo de tipo de despesa de viagem.
	 *
	 * @param request
	 * @author Rodrigo Freitas
	 */
	public void ajaxTipo(WebRequestContext request) {
		List<Despesaviagemtipo> lista = despesaviagemtipoService.findAtivos();
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(lista, "listaCombo", "nome"));
	}
	
	/**
	 * M�todo para fechar e atualizar os pop-up modal.
	 *
	 * @param request
	 * @author Rodrigo Freitas
	 */
	private void fecharModalAtualizar(WebRequestContext request) {
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>parent.location.href = parent.location.href;parent.$.akModalRemove(true);</script>");
	}
	
	/**
	 * M�todo para s� fechar o pop-up modal.
	 *
	 * @param request
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unused")
	private void fecharModal(WebRequestContext request) {
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>parent.$.akModalRemove(true);</script>");
	}
	
	/**
	 * Action que abre a popup para atualizar o rateio.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public ModelAndView atualizarRateio(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		//Despesaviagem despesaviagem = despesaviagemService.loadForAtualizarRateio(whereIn);
		Despesaviagem despesaviagem = new Despesaviagem();
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.loadForAtualizarRateioByDespesaviagem(whereIn);
		
		StringBuilder whereInDespesas = new StringBuilder();
		List<Projeto> listaProjetos = new ArrayList<Projeto>();
		for(Despesaviagem dv : listaDespesaviagem){
			if(fechamentofinanceiroService.verificaFechamento(dv)){
				request.addError("A despesa " + dv.getCddespesaviagem() + " n�o pode ser atualizada pois refere-se a um per�odo j� fechado.");
				this.fecharModalAtualizar(request);
				return null;
			}
			
			if(dv.getSituacaodespesaviagem() == null || 
					(!dv.getSituacaodespesaviagem().equals(Situacaodespesaviagem.AUTORIZADA) && 
							!dv.getSituacaodespesaviagem().equals(Situacaodespesaviagem.BAIXADA) &&
							!dv.getSituacaodespesaviagem().equals(Situacaodespesaviagem.REALIZADA))){
				request.addError("Para atualizar o rateio, a despesa de viagem " + dv.getCddespesaviagem() + " selecionada tem que estar com a situa��o igual a 'AUTORIZADA', 'REALIZADA' ou 'BAIXADA'.");
				this.fecharModalAtualizar(request);
				return null;
			}
			
			if(!"".equals(whereInDespesas.toString())) whereInDespesas.append(",");
			whereInDespesas.append(dv.getCddespesaviagem());
			
			if(dv.getListaDespesaviagemprojeto() != null && !dv.getListaDespesaviagemprojeto().isEmpty()){
				for(Despesaviagemprojeto dvp : dv.getListaDespesaviagemprojeto()){
					if(dvp.getProjeto() != null){
						if(!listaProjetos.contains(dvp.getProjeto())){
							listaProjetos.add(dvp.getProjeto());
						}
					}
				}
			}
		}
		
		despesaviagem.setWhereInDespesas(whereInDespesas.toString());
		despesaviagem.setRateio(despesaviagemService.criaRateioForAtualizarvalores(new Money(), listaDespesaviagem, null, null));
		
		request.setAttribute("listaProjeto", projetoService.findProjetosAbertos(listaProjetos));
		
		return new ModelAndView("direct:/crud/popup/despesaviagemAtualizarrateio", "despesaviagem", despesaviagem);
	}
	
	/**
	 * M�todo que faz atualiza��o dos rateios das despesas de viagem.
	 *
	 * @param request
	 * @param despesaviagem
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	public void saveAtualizarrateio(WebRequestContext request, Despesaviagem despesaviagem){
		if(despesaviagem.getTipooperacao() == null)
			throw new SinedException("Tipo de opera��o n�o pode ser nulo.");
		
		Documentoclasse documentoclasse;
		if(despesaviagem.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
			documentoclasse = Documentoclasse.OBJ_RECEBER;
		} else {
			documentoclasse = Documentoclasse.OBJ_PAGAR;
		}
		
		List<Despesaviagem> listaDVForAtualizar = new ArrayList<Despesaviagem>();
		if(despesaviagem.getWhereInDespesas() != null && !"".equals(despesaviagem.getWhereInDespesas())){
			String[] ids = despesaviagem.getWhereInDespesas().split(",");
			for(String idDespesa : ids){
				listaDVForAtualizar.add(new Despesaviagem(Integer.parseInt(idDespesa)));
			}
		}
		
		int sucesso = 0;
		
		StringBuilder obs = new StringBuilder();
		StringBuilder whereIn = new StringBuilder();
		
		for(Despesaviagem dv : listaDVForAtualizar){
			obs = new StringBuilder("Rateio atualizado referente � Despesa de Viagem ");
			obs.append("<a href=\"javascript:visualizarDespesaviagem(");
			obs.append(dv.getCddespesaviagem());
			obs.append(")\">");
			obs.append(dv.getCddespesaviagem());
			obs.append("</a>");	
			Boolean insere = false;
			
			List<Rateioitem> listaRateioitem = despesaviagem.getRateio().getListaRateioitem();
			
			List<Movimentacao> listaMovimentacao = movimentacaoService.findByDespesaviagem(dv, despesaviagem.getTipooperacao());
			for (Movimentacao movimentacao : listaMovimentacao) {
				try{
					if(movimentacao.getRateio() != null){
						Rateio rateio = movimentacao.getRateio();
						rateio.setListaRateioitem(this.cloneListaRateioitem(listaRateioitem));
						
						rateioService.atualizaValorRateio(rateio, movimentacao.getValor());
						rateioService.saveOrUpdate(rateio);
						
						Movimentacaohistorico movimentacaohistorico = new Movimentacaohistorico();
						movimentacaohistorico.setMovimentacao(movimentacao);
						movimentacaohistorico.setMovimentacaoacao(Movimentacaoacao.RATEIO_ATUALIZADO);
						movimentacaohistorico.setObservacao(obs.toString());
						movimentacaohistoricoService.saveOrUpdate(movimentacaohistorico);
						
						sucesso++;
						insere = true;
					} else throw new SinedException("Rateio n�o encontrado.");
				} catch (Exception e) {
					request.addError("N�o foi poss�vel atualizar o rateio da movimenta��o " + movimentacao.getCdmovimentacao() + ": " + e.getMessage());
					e.printStackTrace();
				}
			}
			
			
			List<Documento> listaDocumento = documentoService.findByDespesaviagem(dv, documentoclasse);
			for (Documento documento : listaDocumento) {
				try{
					if(documento.getRateio() != null){
						Rateio rateio = documento.getRateio();
						rateio.setListaRateioitem(this.cloneListaRateioitem(listaRateioitem));
						
						rateioService.atualizaValorRateio(rateio, documento.getValor());
						rateioService.saveOrUpdate(rateio);
						
						Documentohistorico documentohistorico = new Documentohistorico();
						documentohistorico.setDocumento(documento);
						documentohistorico.setDocumentoacao(Documentoacao.RATEIO_ATUALIZADO);
						documentohistorico.setObservacao(obs.toString());
						documentohistoricoService.saveOrUpdate(documentohistorico);
						
						if(documento.getDocumentoacao() != null && documento.getDocumentoacao().equals(Documentoacao.BAIXADA)){
							List<Movimentacao> movimentacoes = movimentacaoService.findByDocumentoVariasMovimentacoes(documento);
							for (Movimentacao movimentacao : movimentacoes) {
								rateioService.atualizaRateioMovimentacao(movimentacao);
							}
						}
						
						sucesso++;
						insere = true;
					} else throw new SinedException("Rateio n�o encontrado.");
				} catch (Exception e) {
					request.addError("N�o foi poss�vel atualizar o rateio do documento " + documento.getCddocumento() + ": " + e.getMessage());
					e.printStackTrace();
				}
			}
			
			if(insere){
				whereIn.append(dv.getCddespesaviagem());
				whereIn.append(",");
			}
		}
		
		if(sucesso > 0){
			String where = whereIn.substring(0, whereIn.length()-1);
			despesaviagemService.salvaHistorico(where, Despesaviagemacao.RATEIO_ATUALIZADO, null);
			request.addMessage("Rateio(s) atualizado(s) de " + sucesso + " registro(s) com sucesso.");
		} else {
			request.addError("Nenhum registro foi atualizado.");
		}
		
		this.fecharModalAtualizar(request);
	}
	
	public ModelAndView ajaxBuscatotaisDespesaviagem(WebRequestContext request){
//		String whereIn = request.getParameter("cddespesaviagem");
//		String tipooperacao = request.getParameter("tipooperacao");
//		Money total = new Money();
//		if(whereIn != null && !"".equals(whereIn) && tipooperacao != null && !"".equals(tipooperacao)){
//			total = despesaviagemService.getValorTotalAdiantamentosAcertos(whereIn, Tipooperacao.TIPO_CREDITO.getCdtipooperacao().toString().equals(tipooperacao) ? Tipooperacao.TIPO_CREDITO : Tipooperacao.TIPO_DEBITO);
//		}
//		return new JsonModelAndView().addObject("total", total);
		
		String whereIn = request.getParameter("whereInDespesas");
		String tipooperacao = request.getParameter("tipooperacao");
		Money total = new Money();
		if(whereIn != null && !"".equals(whereIn) && tipooperacao != null && !"".equals(tipooperacao)){
			String[] ids = whereIn.split(",");
			for(String cddespesaviagem : ids){
				total = total.add(despesaviagemService.getValorTotalAdiantamentosAcertos(cddespesaviagem, Tipooperacao.TIPO_CREDITO.getCdtipooperacao().toString().equals(tipooperacao) ? Tipooperacao.TIPO_CREDITO : Tipooperacao.TIPO_DEBITO));
			}
		}
		return new JsonModelAndView().addObject("total", total);
	}
	
	/**
	 * Faz a c�pia dos itens de rateio para a atuliza��o do rateio das contas a pagar/receer e movimenta��o.
	 *
	 * @param listaRateioitem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/10/2012
	 */
	private List<Rateioitem> cloneListaRateioitem(List<Rateioitem> listaRateioitem) {
		List<Rateioitem> listaRateioitemNova = new ArrayList<Rateioitem>();
		Rateioitem riNova;
		
		for (Rateioitem ri : listaRateioitem) {
			riNova = new Rateioitem();
			riNova.setContagerencial(ri.getContagerencial());
			riNova.setCentrocusto(ri.getCentrocusto());
			riNova.setProjeto(ri.getProjeto());
			riNova.setPercentual(ri.getPercentual());
			
			listaRateioitemNova.add(riNova);
		}
		
		return listaRateioitemNova;
	}
	
	
}
