package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.AnaliseReceitaDespesaBean;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.enumeration.AnaliseReceitaDespesaOrigem;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;

@Controller(path="/financeiro/process/AnaliseReceitaDespesasProcess", authorizationModule=ProcessAuthorizationModule.class)
public class AnaliseReceitaDespesasProcess extends MultiActionController {
	
	private MovimentacaoService movimentacaoService;
		
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	private DocumentoService documentoService;
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
		
	public ModelAndView listar(WebRequestContext request, AnaliseReceitaDespesaReportFiltro filtro){
		String identificador = request.getParameter("identificador");
		String nome = request.getParameter("nome");
		String campo = request.getParameter("campo");
		String ordenacao = request.getParameter("ordenacao");
		if(StringUtils.isEmpty(ordenacao)){
			ordenacao ="desc";
		}
		Double totalDespesa = 0d;
		filtro = (AnaliseReceitaDespesaReportFiltro)NeoWeb.getRequestContext().getSession().getAttribute(ContagerencialService.FILTRO_SESSION);
		request.setAttribute("identificador", identificador);
		if(nome!=null){
			request.setAttribute("nome", nome.trim());			
		}else{
			request.setAttribute("nome", nome);
		}

		if(AnaliseReceitaDespesaOrigem.CONTA_PAGAR_RECEBER.equals(filtro.getOrigem())){
			List<Documento> documento = documentoService.findForAnaliseRecDespesaFlex(filtro, identificador, false, campo, ordenacao);
			
			Iterator<Documento> iteratorTotalDocumento = documento.iterator();   
	        while (iteratorTotalDocumento.hasNext()) {
	        	Documento doc = iteratorTotalDocumento.next();
	        	if(doc.getValorDouble() != null){
	        		totalDespesa += doc.getValorDouble();	
	        	}
	        }
	        
	        if(filtro.getExibirMovimentacaoSemVinculoDocumento() != null && filtro.getExibirMovimentacaoSemVinculoDocumento()) {
	        	filtro.setCarregarMovimentacoes(Boolean.TRUE);
	        	
	        	List<Documento> movimentacao = documentoService.findForAnaliseRecDespesaFlex(filtro, identificador, false, campo, ordenacao);
				
	        	Iterator<Documento> iteratorTotalMovimentacao = movimentacao.iterator();
				while (iteratorTotalMovimentacao.hasNext()) {
					Documento mov = iteratorTotalMovimentacao.next();
					if(mov.getValorDouble()!=null){
						totalDespesa += mov.getValorDouble(); 	        		        		
					}
				}
				
				documento.addAll(movimentacao);
				
				if(StringUtils.isNotBlank(campo) && StringUtils.isNotBlank(ordenacao)) {
					final String field = new String(campo);
					final String order = new String(ordenacao);
					Collections.sort(documento, new Comparator<Documento>() {
						public int compare(Documento o1, Documento o2) {
							if(field.equals("cddocumento")) {
								return (order.equals("asc") ? o1.getCddocumento().compareTo(o2.getCddocumento()) : o2.getCddocumento().compareTo(o1.getCddocumento()));
							} else if(field.equals("tipo")) {
								return (order.equals("asc") ? o1.getTipoRegistro().compareTo(o2.getTipoRegistro()) : o2.getTipoRegistro().compareTo(o1.getTipoRegistro()));
							} else if(field.equals("descricao")) {
								return (order.equals("asc") ? o1.getDescricao().compareTo(o2.getDescricao()) : o2.getDescricao().compareTo(o1.getDescricao()));
							} else if(field.equals("valor")) {
								return (order.equals("asc") ? o1.getValor().compareTo(o2.getValor()) : o2.getValor().compareTo(o1.getValor()));
							}
							return (order.equals("asc") ? o1.getDtvencimento().compareTo(o2.getDtvencimento()) : o2.getDtvencimento().compareTo(o1.getDtvencimento()));
						}
					});
				}
				
				filtro.setCarregarMovimentacoes(Boolean.FALSE);
				request.setAttribute("mostrarTipo", true);
	        } 
	        
	        request.setAttribute("listaDocumento", documento);
	    } else {
			List<Movimentacao> movimentacao = movimentacaoService.findForAnaliseRecDespesaFlex(filtro, identificador, false, campo, ordenacao);
			Iterator<Movimentacao> iteratorTotal = movimentacao.iterator();   
			while (iteratorTotal.hasNext()) {
				Movimentacao mov = iteratorTotal.next();
				if(mov.getValorDouble()!=null){
					totalDespesa += mov.getValorDouble(); 	        		        		
				}
			}			
			request.setAttribute("lista", movimentacao);
		}
		
		request.setAttribute("total",new Money(totalDespesa));
		request.setAttribute("index2", request.getParameter("index2"));
		request.setAttribute("ordenacao", ordenacao);
		request.setAttribute("campo", campo);
		
		return new ModelAndView("direct:process/analiseReceitaDespesaListagem");
	}

}
