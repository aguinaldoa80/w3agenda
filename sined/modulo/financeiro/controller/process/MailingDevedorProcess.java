package br.com.linkcom.sined.modulo.financeiro.controller.process;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.enumeration.FormaEnvioBoletoEnum;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ClientehistoricoService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.MeiocontatoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.DevedoresBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.MailingDevedorBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.MailingDevedorRetornoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.report.MergeReport;

@Controller(path="/financeiro/process/MailingDevedor", authorizationModule=ProcessAuthorizationModule.class)
public class MailingDevedorProcess extends MultiActionController {
	
	private CategoriaService categoriaService;
	private DocumentoService documentoService;
	private ContatoService contatoService;
	private EmpresaService empresaService;
	private EnvioemailService envioemailService;
	private ClientehistoricoService clientehistoricoService;
	private MeiocontatoService meiocontatoService;
	private ContareceberService contareceberService;
	private ParametrogeralService parametrogeralService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setClientehistoricoService(ClientehistoricoService clientehistoricoService) {
		this.clientehistoricoService = clientehistoricoService;
	}
	public void setMeiocontatoService(MeiocontatoService meiocontatoService) {
		this.meiocontatoService = meiocontatoService;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	/**
	 * Carrega os dados para refentes ao filtro. 
	 * @param request
	 * @param filtro
	 * @return
	 * @author Ramon Brazil
	 */
	@DefaultAction
	public ModelAndView index(WebRequestContext request, MailingDevedorBean filtro) {	
		
		Object filtroSessionObject = request.getSession().getAttribute(filtro.getClass().getCanonicalName());
		if(filtroSessionObject != null){
			filtro = (MailingDevedorBean) filtroSessionObject;
			request.getSession().removeAttribute(filtro.getClass().getCanonicalName());
		}
		
		List<Categoria> listaCategoria = categoriaService.findAll("nome");
		request.setAttribute("listaCategoria", listaCategoria);
		if(filtro.getEmpresa() == null){
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null)
				filtro.setEmpresa(empresa);
		}
		
		filtro.setAssunto("Aviso: Pend�ncias Financeiras");
		
		return new ModelAndView("/process/mailingdevedor", "filtro", filtro);
	}
	/**
	 * Lista o cliente que esta com uma conta vencida. 
	 * @param request
	 * @param filtro
	 * @return
	 * @author Ramon Brazil
	 */
	public ModelAndView listar(WebRequestContext request, MailingDevedorBean filtro){
		MailingDevedorRetornoBean retornoBean = documentoService.montaListagemDevedores(filtro);
		
		return new ModelAndView("direct:ajax/mailingdevedorlistagem", "listaDevedores", retornoBean.getListaDevedores())
								.addObject("valorTotal", retornoBean.getValorTotal().toString())
								.addObject("valorAtualTotal", retornoBean.getValorAtualTotal().toString())
								.addObject("haveEmpresa", filtro.getEmpresa() != null);
	}
	
	/**
	 * Envia um e-mail para o cliente que esta com uma conta vencida. 
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Ramon Brazil
	 */
	@Action("enviar")
	public void enviar(WebRequestContext request, MailingDevedorBean filtro) throws Exception {
		enviarEmails(request, filtro);
	}

	/**
	 * Monta um e-mail para o cliente que esta com uma conta vencida. 
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Ramon Brazil, Rodrigo Freitas
	 */
	private void enviarEmails(WebRequestContext request, MailingDevedorBean filtro) throws Exception {
		List<Documento> listaDocumento = new ArrayList<Documento>();
		
		Empresa empresa = empresaService.loadForEntrada(filtro.getEmpresa() != null ? filtro.getEmpresa() : empresaService.loadPrincipal());
		listaDocumento = documentoService.findByWhereInForMaillingDevedor(filtro.getContas());
		
		Map<Integer, Empresa> mapEmpresa = new HashMap<Integer, Empresa>();
		
		DevedoresBean devedoresBean;
		List<Contato> listacontato;
		String erro = "";
		String semEmail = "";
		List<DevedoresBean> listaDevedores = new ArrayList<DevedoresBean>();
		
		for (Documento documento : listaDocumento) {
			devedoresBean = new DevedoresBean();
			devedoresBean.setMensagem(filtro.getMensagem());
			devedoresBean.setEmail(documento.getPessoa().getEmail());
			devedoresBean.setPessoa(documento.getPessoa());
			devedoresBean.setCliente(documento.getPessoa().getNome());
			devedoresBean.setDocumento(documento);
			
			if(documento.getConta() == null){
				erro += documento.getCddocumento().toString()+", ";	
			}
			
			listacontato = contatoService.findByPessoaContatotipo(documento.getPessoa(), filtro.getContatotipo());
			if(listacontato != null){
				for (int i = 0; i < listacontato.size(); i++) {
					if(listacontato.get(i).getEmailcontato() != null && !listacontato.get(i).getEmailcontato().equals("") &&
							!devedoresBean.getEmailscontatos().contains(listacontato.get(i).getEmailcontato())){
						devedoresBean.getEmailscontatos().add(listacontato.get(i).getEmailcontato());
					}
				}
			}
			
			if(devedoresBean.getEmail() != null || devedoresBean.getEmailscontatos().size() > 0){					
				listaDevedores.add(devedoresBean);		
			} else
				semEmail += documento.getCddocumento().toString()+", ";
			
			if(!mapEmpresa.containsKey(documento.getEmpresa().getCdpessoa())){
				mapEmpresa.put(documento.getEmpresa().getCdpessoa(), empresaService.loadForEntrada(documento.getEmpresa()));
			}
		}

		devedoresBean = new DevedoresBean();
		List<DevedoresBean> listaDevedoresDistinct = new ArrayList<DevedoresBean>(); 
		int i = 0;
		String novaMensagem;
		
		for (DevedoresBean devedores: listaDevedores) {
			i++;
			
			String url = request.getServletRequest().getRequestURL().toString();
			url = url.replace("/w3erp/financeiro/process/MailingDevedor", "");
			devedores.setUrl(url + "/w3cliente/areacliente/process/PendenciaFinanceira");
			
			if (devedores.getCliente().equals(devedoresBean.getCliente()) && devedoresBean.getCliente() != null){
				novaMensagem = devedoresBean.getMensagem();
				novaMensagem = novaMensagem.replace("&lt; DESCRICAO &gt;", devedores.getDocumento().getDescricao() != null ? devedores.getDocumento().getDescricao() : "");
				novaMensagem = novaMensagem.replace("&lt; DTVENCIMENTO &gt;", SinedDateUtils.toString(devedores.getDocumento().getDtvencimento()) != null ? SinedDateUtils.toString(devedores.getDocumento().getDtvencimento()) : "");
				novaMensagem = novaMensagem.replace("&lt; VALOR &gt;", devedores.getDocumento().getValor().toString() != null ? devedores.getDocumento().getValor().toString() : "");
				novaMensagem = novaMensagem.replace("&lt; VALORATUAL &gt;", devedores.getDocumento().getAux_documento() != null && devedores.getDocumento().getAux_documento().getValoratual() != null ? devedores.getDocumento().getAux_documento().getValoratual().toString() : "");
				novaMensagem = novaMensagem.replace("&lt; LINKCONTA &gt;", 
																"<a href=\" " + "URL_BOLETO_AREACLI" + devedores.getDocumento().getCddocumento() + "\">"+"clique aqui"+"</a>" +
																"<tr valign=\"top\">" +
																"<td>&lt; DESCRICAO &gt;</td> " +
																"<td>&lt; DTVENCIMENTO &gt;</td> " +
																"<td>&lt; VALOR &gt;</td> " +
																"<td>&lt; VALORATUAL &gt;</td> " +
																"<td>&lt; LINKCONTA &gt;</td>");
				
				devedoresBean.setMensagem(novaMensagem);
				
				if(listaDevedores.size() == i){
					listaDevedoresDistinct.add(devedoresBean);
				}
				
				if(devedoresBean.getListaDocumento() == null) devedoresBean.setListaDocumento(new ArrayList<Documento>());
				devedoresBean.getListaDocumento().add(devedores.getDocumento());
			} else {
				if(devedoresBean.getCliente() != null){
					listaDevedoresDistinct.add(devedoresBean);
				}
				devedoresBean = devedores;
				if(devedoresBean.getListaDocumento() == null) devedoresBean.setListaDocumento(new ArrayList<Documento>());
				devedoresBean.getListaDocumento().add(devedores.getDocumento());
				
				novaMensagem = devedoresBean.getMensagem();
				novaMensagem = novaMensagem.replace("&lt; DESCRICAO &gt;", devedores.getDocumento().getDescricao() != null ? devedores.getDocumento().getDescricao() : "");
				novaMensagem = novaMensagem.replace("&lt; DTVENCIMENTO &gt;", SinedDateUtils.toString(devedores.getDocumento().getDtvencimento()) != null ? SinedDateUtils.toString(devedores.getDocumento().getDtvencimento()) : "");
				novaMensagem = novaMensagem.replace("&lt; VALOR &gt;", devedores.getDocumento().getValor().toString() != null ? devedores.getDocumento().getValor().toString() : "");
				novaMensagem = novaMensagem.replace("&lt; VALORATUAL &gt;", devedores.getDocumento().getAux_documento() != null && devedores.getDocumento().getAux_documento().getValoratual() != null ? devedores.getDocumento().getAux_documento().getValoratual().toString() : "");
				novaMensagem = novaMensagem.replace("&lt; LINKCONTA &gt;",  
																"<a href=\" " + "URL_BOLETO_AREACLI" + devedores.getDocumento().getCddocumento() + "\">"+"clique aqui"+"</a>" +
																"<tr valign=\"top\">" +
																"<td>&lt; DESCRICAO &gt;</td> " +
																"<td>&lt; DTVENCIMENTO &gt;</td> " +
																"<td>&lt; VALOR &gt;</td> " +
																"<td>&lt; VALORATUAL &gt;</td> " +
																"<td>&lt; LINKCONTA &gt;</td>");
				
				devedoresBean.setMensagem(novaMensagem);
				
				
				if(listaDevedores.size() == i){
					listaDevedoresDistinct.add(devedoresBean);
				}
				
			}		
		}
		
		if(erro.equals("")){
			try {
				String assunto;
				String dtvencimentoAtual = null;
				String boletoStr = "";
				Report report = null;
				MergeReport mergeReport = null;
				
				for (DevedoresBean devedores: listaDevedoresDistinct) {
					mergeReport = null;
					
					if(parametrogeralService.getBoolean(Parametrogeral.ENVIAR_EMAIL_BOLETO_SEGUNDA_VIA)){
						if(SinedUtil.isListNotEmpty(devedores.getListaDocumento())){
							for(Documento documento : devedores.getListaDocumento()){
								if (documento.getEmpresa() != null && FormaEnvioBoletoEnum.DOWNLOAD_CAPTCHA.equals(documento.getEmpresa().getFormaEnvioBoleto())) {
									StringBuilder url = new StringBuilder();
									
									url.append(SinedUtil.getUrlWithContext()).append("/pub/process/BoletoPub?cddocumento=").append(documento.getCddocumento())
										.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(documento.getCddocumento().toString()))));
									
									boletoStr = url.toString();
								} else if (documento.getEmpresa() != null && FormaEnvioBoletoEnum.POR_ANEXO.equals(documento.getEmpresa().getFormaEnvioBoleto())) {
									report = new Report("/financeiro/relBoleto");
									BoletoBean boletoBean = contareceberService.boletoDocumento(documentoService.loadForBoleto(documento), true);
									dtvencimentoAtual = boletoBean.getDtVencimento();
									report.setDataSource(new BoletoBean[]{boletoBean});
									
									if(mergeReport == null){
										mergeReport = new MergeReport("boleto_" + SinedUtil.datePatternForReport() + ".pdf");
									}
									
									mergeReport.addReport(report);
								} else if (documento.getEmpresa() != null && FormaEnvioBoletoEnum.EMAIL_CONFIGURAVEL.equals(documento.getEmpresa().getFormaEnvioBoleto())) {
									StringBuilder url = new StringBuilder();
									
									url.append(SinedUtil.getUrlWithContext()).append("/pub/relatorio/BoletoPubDownload?ACAO=gerar&cddocumento=").append(documento.getCddocumento())
										.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(documento.getCddocumento().toString()))));
									
									boletoStr = url.toString();
								}
								
								if (StringUtils.isNotEmpty(boletoStr)) {
									devedores.setMensagem(devedores.getMensagem().replace("URL_BOLETO_AREACLI" + documento.getCddocumento(), boletoStr));
								} else {
									devedores.setMensagem(devedores.getMensagem().replace("URL_BOLETO_AREACLI" + documento.getCddocumento(), devedores.getUrl()));
								}
							}
						} else {
							if (devedores.getDocumento() != null && devedores.getDocumento().getCddocumento() != null) {
								if (devedores.getDocumento().getEmpresa() != null && FormaEnvioBoletoEnum.DOWNLOAD_CAPTCHA.equals(devedores.getDocumento().getEmpresa().getFormaEnvioBoleto())) {
									StringBuilder url = new StringBuilder();
									
									url.append(SinedUtil.getUrlWithContext()).append("/pub/process/BoletoPub?cddocumento=").append(devedores.getDocumento().getCddocumento())
										.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(devedores.getDocumento().getCddocumento().toString()))));
									
									boletoStr = url.toString();
								} else if (devedores.getDocumento().getEmpresa() != null && FormaEnvioBoletoEnum.POR_ANEXO.equals(devedores.getDocumento().getEmpresa().getFormaEnvioBoleto())) {
									report = new Report("/financeiro/relBoleto");
									BoletoBean boletoBean = contareceberService.boletoDocumento(documentoService.loadForBoleto(devedores.getDocumento()), true);
									dtvencimentoAtual = boletoBean.getDtVencimento();
									report.setDataSource(new BoletoBean[]{boletoBean});
								
									if(mergeReport == null){
										mergeReport = new MergeReport("boleto_" + SinedUtil.datePatternForReport() + ".pdf");
									}
									
									mergeReport.addReport(report);
								} else if (devedores.getDocumento().getEmpresa() != null && FormaEnvioBoletoEnum.EMAIL_CONFIGURAVEL.equals(devedores.getDocumento().getEmpresa().getFormaEnvioBoleto())) {
									StringBuilder url = new StringBuilder();
									
									url.append(SinedUtil.getUrlWithContext()).append("/pub/relatorio/BoletoPubDownload?ACAO=gerar&cddocumento=").append(devedores.getDocumento().getCddocumento())
										.append("&hash=").append(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(Util.crypto.makeHashMd5(devedores.getDocumento().getCddocumento().toString()))));
									
									boletoStr = url.toString();
								}
								
								if (StringUtils.isNotEmpty(boletoStr)) {
									devedores.setMensagem(devedores.getMensagem().replace("URL_BOLETO_AREACLI" + devedores.getDocumento().getCddocumento(), boletoStr));
								} else {
									devedores.setMensagem(devedores.getMensagem().replace("URL_BOLETO_AREACLI" + devedores.getDocumento().getCddocumento(), devedores.getUrl()));
								}
							}
						}
					} else {
						if(SinedUtil.isListNotEmpty(devedores.getListaDocumento())) {
							for (Documento documento : devedores.getListaDocumento()) {
								devedores.setMensagem(devedores.getMensagem().replace("URL_BOLETO_AREACLI" + documento.getCddocumento(), devedores.getUrl()));
							}
						} else {
							if (devedores.getDocumento() != null && devedores.getDocumento().getCddocumento() != null) {
								devedores.setMensagem(devedores.getMensagem().replace("URL_BOLETO_AREACLI" + devedores.getDocumento().getCddocumento(), devedores.getUrl()));
							}
						}
					}
					
					assunto = filtro.getAssunto();
					
					if(devedores.getCliente() != null && !"".equals(devedores.getCliente())){
						assunto += " - " + devedores.getCliente();
					}
					
					String telefonesEmpresa = null;
					String nomeEmpresa = null;
					String emailRemetente = null;
					if(devedores.getDocumento() != null && devedores.getDocumento().getEmpresa() != null){
						Empresa empresaDoc = mapEmpresa.get(devedores.getDocumento().getEmpresa().getCdpessoa());
						empresaService.setInformacoesPropriedadeRural(empresaDoc, true);
						emailRemetente = empresaDoc.getEmailfinanceiro() != null ? empresaDoc.getEmailfinanceiro() : empresaDoc.getEmail();
						nomeEmpresa = empresaDoc.getNomeProprietarioPrincipalOuEmpresa();
						telefonesEmpresa = empresaDoc.getTelefones();
					}
					
					if(StringUtils.isBlank(telefonesEmpresa)) telefonesEmpresa = empresa.getTelefones();
					if(StringUtils.isBlank(nomeEmpresa)) nomeEmpresa = empresa.getNome();
					if(StringUtils.isBlank(emailRemetente)) emailRemetente = empresa.getEmailfinanceiro() != null ? empresa.getEmailfinanceiro() : empresa.getEmail();
					
					novaMensagem = devedores.getMensagem();
					novaMensagem = novaMensagem.replace("&lt; NOMECLIENTE &gt;", devedores.getCliente() != null ? devedores.getCliente() : "");
//					novaMensagem = novaMensagem.replace("&lt; NOMERESPONSAVEL &gt;", devedores.getCliente() != null ? devedores.getCliente() : "");
					novaMensagem = novaMensagem.replace("&lt; NOMEEMPRESA &gt;", nomeEmpresa != null ? nomeEmpresa : "");
					novaMensagem = novaMensagem.replace("&lt; EMAILEMPRESA &gt;", emailRemetente != null ? emailRemetente : "");
					novaMensagem = novaMensagem.replace("&lt; DTVENCIMENTOATUAL &gt;", (dtvencimentoAtual != null ? dtvencimentoAtual : ""));
					novaMensagem = novaMensagem.replace("&lt; TELEMPRESA &gt;", telefonesEmpresa != null ? telefonesEmpresa : "");
					novaMensagem = novaMensagem.replace("<tr valign=\"top\">" +
														"<td>&lt; DESCRICAO &gt;</td> " +
														"<td>&lt; DTVENCIMENTO &gt;</td> " +
														"<td>&lt; VALOR &gt;</td> " +
														"<td>&lt; VALORATUAL &gt;</td> " +
														"<td>&lt; LINKCONTA &gt;</td>" , "");
					
					if(devedores.getEmailscontatos() != null && !devedores.getEmailscontatos().isEmpty()){
						for (String destinatario : devedores.getEmailscontatos()){
							EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
													.setFrom(emailRemetente)
													.setTo(destinatario)
													.setSubject(assunto);
							Envioemail envioemail = envioemailService.registrarEnvio(emailRemetente, assunto, novaMensagem, 
									Envioemailtipo.AVISO_CONTA_VENCIDA, devedores.getPessoa(), destinatario, devedores.getContato(), email);
									
							if(mergeReport != null){
								email.attachFileUsingByteArray(mergeReport
									.generateResource()
									.getContents(), "boleto_" + SinedUtil.datePatternForReport() + ".pdf" ,"application/pdf", "1");
							}
							
							email.addHtmlText(novaMensagem + EmailUtil.getHtmlConfirmacaoEmail(envioemail, destinatario));
							email.sendMessage();
						}
					}else {
						if(devedores.getEmail() != null){
							String destinatario = devedores.getEmail();
							EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
													.setFrom(emailRemetente)
													.setTo(destinatario)
													.setSubject(assunto);
							Envioemail envioemail = envioemailService.registrarEnvio(emailRemetente, assunto, novaMensagem, 
									Envioemailtipo.AVISO_CONTA_VENCIDA, devedores.getPessoa(), destinatario, devedores.getContato(), email);
							
							if(mergeReport != null){
								email.attachFileUsingByteArray(mergeReport
									.generateResource()
									.getContents(), "boleto_" + SinedUtil.datePatternForReport() + ".pdf" ,"application/pdf", "1");
							}
							
							email.addHtmlText(novaMensagem + EmailUtil.getHtmlConfirmacaoEmail(envioemail, destinatario));
							email.sendMessage();
						}
					}
					
					if(SinedUtil.isListNotEmpty(devedores.getListaDocumento())){
						for(Documento doc : devedores.getListaDocumento()){
							documentoService.criarHistoricoERegistrarUltimaCobranca(devedores, doc, null);
						}
					}else if(devedores.getDocumento() != null){
						documentoService.criarHistoricoERegistrarUltimaCobranca(devedores, devedores.getDocumento(), null);
					}
				}

				if (listaDevedoresDistinct != null && listaDevedoresDistinct.size() > 0){					
					request.addMessage("Mailing enviado com sucesso.");
				}
				
				if (!"".equals(semEmail))
					request.addMessage("O cliente da(s) conta(s) a receber "+semEmail+"n�o possui(em) email para contato.", MessageType.WARN);
				
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("N�o foi poss�vel enviar o mailling. Favor contactar o administrador do sistema.");
			}
		}else{
			request.addError("A conta a receber "+erro+" n�o possui conta banc�ria cadastrada.\n");
		}

		request.getSession().setAttribute(filtro.getClass().getCanonicalName(), filtro);
		SinedUtil.fechaPopUp(request);
	}
	
	public void getRemetente(WebRequestContext request, MailingDevedorBean filtro){
		if(filtro != null){
			Empresa empresa = empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.email");
			String remetente = empresa.getEmail();
			View.getCurrent().eval(remetente);
		}
	}
	
	/**
	 * Metodo que abre o modal da a��o enviar
	 * @param request
	 * @param bean
	 * @return
	 * @throws ParseException 
	 */
	public ModelAndView ajaxAcaoEnviar(WebRequestContext request, MailingDevedorBean bean) throws ParseException{
        String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhuma item selecionado.");
			SinedUtil.fechaPopUp(request);
			return new ModelAndView("/financeiro/process/MailingDevedor");
		}
		
		String email = bean.getEmail();
		if(StringUtils.isBlank(email)){
			Empresa empresa = bean.getEmpresa() != null ? empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.email") : empresaService.loadPrincipal();
			if(empresa != null){
				email = empresa.getEmail();
			}
		}
		bean.setEmail(email);
		bean.setAssunto(bean.getAssunto());

		StringBuilder doMensagem = new StringBuilder();
		doMensagem.append("&lt; NOMECLIENTE &gt;, \n");
		doMensagem.append(
				"<p>Consta em nosso controle de pagamentos as  seguintes pend&ecirc;ncias financeiras:</p>" +
				"<table width=\"100%\" border=\"1\" bordercolor=\"#000000\" cellpadding=\"4\" cellspacing=\"0\"><tr valign=\"top\">" +
				"<td><p><strong>Descri&ccedil;&atilde;o</strong></p></td><td><p><strong>Vencimento Original</strong></p>" +
				"</td><td><p><strong>Valor Original</strong></p></td>" +
				"</td><td><p><strong>Valor Atual*</strong></p></td>" +
				"<td><p><strong>Segunda Via</strong></p></td></tr> " +
				"<tr valign=\"top\">" +	
				"<td>&lt; DESCRICAO &gt;</td> " +
				"<td>&lt; DTVENCIMENTO &gt;</td> " +
				"<td>&lt; VALOR &gt;</td> " +
				"<td>&lt; VALORATUAL &gt;</td> " +
				"<td>&lt; LINKCONTA &gt;</td>" +
				"</tr>" +
				"</table>" +
				"<br>" +
				(parametrogeralService.getBoolean(Parametrogeral.ENVIAR_EMAIL_BOLETO_SEGUNDA_VIA) ? 
						"*O valor atual corresponde ao valor at&eacute; a data &lt; DTVENCIMENTOATUAL &gt;, caso n&atilde;o seja pago nessa data, entre em contato para a gera�&atilde;o de um novo boleto.<br>" : 
						""
				) +
				"<br>Como n&atilde;o recebemos nenhuma comunica�&atilde;o sobre as raz&otilde;es que determinaram esse atraso, " +
				(parametrogeralService.getBoolean(Parametrogeral.ENVIAR_EMAIL_BOLETO_SEGUNDA_VIA) ? 
					"encaminhamos-lhe o(s) devido(s) boleto(s) atualizado(s) para pagamento."
					:
					"solicitamos-lhe, por obs&eacute;quio, que tomem as devidas provid&ecirc;ncias."
				) +
				"<br>Tendo em vista que a emiss&atilde;o deste aviso &eacute; autom&aacute;tica,  por computador, caso " +
				"j&aacute; tenha pago at&eacute; a data da  entrega do mesmo, favor ignorar este aviso." +
				"<br> Atenciosamente,<br><br>");
		
		doMensagem.append(	
//							"&lt; NOMERESPONSAVEL &gt;<br>" +
							"&lt; NOMEEMPRESA &gt;<br>" +
							"&lt; EMAILEMPRESA &gt;<br>" +
							"&lt; TELEMPRESA &gt;<br>");
		bean.setMensagem(doMensagem.toString());
		bean.setContas(whereIn);
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("descricao", "ENVIAR E-MAIL");
		
		return new ModelAndView("direct:/process/popup/enviaMailingDevedor").addObject("bean", bean);
	}
	
	/**
	 * A��o que abre o modal para registrar cobran�a 
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView ajaxAcaoRegistrarCobranca(WebRequestContext request, MailingDevedorBean bean){
		String whereIn = SinedUtil.getItensSelecionados(request);
			
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhuma item selecionado.");
			SinedUtil.fechaPopUp(request);
			return new ModelAndView("/financeiro/process/MailingDevedor");
		}
		
		bean.setContas(whereIn);
		request.setAttribute("listaMeioContato", meiocontatoService.findAtivos());
		request.setAttribute("descricao", "REGISTRAR COBRAN�A");
		return new ModelAndView("direct:/process/popup/registraCobrancaDevedor").addObject("bean", bean);
	}
	
	/**
	 * A��o que grava o registro da cobran�a
	 * @param request
	 * @param bean
	 */
	public void registrarCobranca(WebRequestContext request, MailingDevedorBean bean){
		List<Documento> listaDocumento = new ArrayList<Documento>();
		listaDocumento = documentoService.findByWhereInForMaillingDevedor(bean.getContas());
		
		boolean registrado = false;
		boolean erro = false;
		List<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		try {
			for(Documento documento : listaDocumento){
				if(documento.getPessoa() != null && !listaPessoa.contains(documento.getPessoa())){
					listaPessoa.add(documento.getPessoa());
					
					Clientehistorico clientehistorico = new Clientehistorico();
					Cliente cliente = new Cliente(documento.getPessoa().getCdpessoa());
					clientehistorico.setCliente(cliente);
					clientehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					clientehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					clientehistorico.setMeiocontato(bean.getMeiocontato());
					clientehistorico.setObservacao(bean.getDescricao());
					clientehistorico.setAtividadetipo(bean.getAtividadetipo());
					
					clientehistoricoService.saveOrUpdate(clientehistorico);
				}
				documentoService.criarHistoricoERegistrarUltimaCobranca(null, documento, bean.getDescricao());
			}
			if(listaDocumento != null && listaDocumento.size() > 0){
				registrado = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			erro = true;
		}
		request.setAttribute("descricao", "REGISTRAR COBRAN�A");
		request.getSession().setAttribute(bean.getClass().getCanonicalName(), bean);
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		if (erro) {
			StringBuilder msg = new StringBuilder();
			msg.append("<ul>");
			msg.append("<li class=\"info\">N�o foi poss�vel registrar a cobran�a. Favor contactar o administrador do sistema.</li>");
			msg.append("</ul>");
			view.println("<script language='javascript'>" +
					"parent.$('#bindblock').html('"+ msg.toString()+"');" +
					"parent.$('#bindblock').show();</script>");
		}else {
			view.println("<script language='javascript'>" +
					"parent.$('#bindblock').html('');" +
					"parent.$('#bindblock').hide();</script>");
		}
		if (registrado) {
			StringBuilder msg = new StringBuilder();
			msg.append("<ul>");
			msg.append("<li class=\"info\">Registrado com sucesso.</li>");
			msg.append("</ul>");
			view.println("<script language='javascript'>" +
					"parent.$('#messageBlock').html('"+ msg.toString()+"');" +
					"parent.$('#messageBlock').show();" +
					"parent.makeAjaxDocumentos();</script>");
		}else {
			view.println("<script language='javascript'>" +
					"parent.$('#messageBlock').html('');" +
					"parent.$('#messageBlock').hide();</script>");
		}
		view.println("<script language='javascript'>" +
				"function clearMessages(){document.getElementById('messageBlock').style.display = 'none';}" +
				"parent.$.akModalRemove(true);</script>");
	}
	
	public ModelAndView gerarCSV(WebRequestContext request, MailingDevedorBean filtro){
		MailingDevedorRetornoBean retornoBean = documentoService.montaListagemDevedores(filtro);
		
		boolean haveEmpresa = filtro.getEmpresa() != null;
		
		StringBuilder csv = new StringBuilder();
		if(!haveEmpresa) {
			csv.append("Empresa").append(";");
		}
		csv.append("Cliente").append(";");
		csv.append("Contato(s)").append(";");
		csv.append("Telefone(s)").append(";");
		csv.append("E-mail(s)").append(";");
		csv.append("Conta(s) atrasada(s)").append(";");
		csv.append("Valor").append(";");
		csv.append("Valor atual").append(";");
		csv.append("�ltima cobran�a").append(";");
		csv.append("Pr�xima cobran�a").append(";");
		csv.append("\n");
		
		for (DevedoresBean bean : retornoBean.getListaDevedores()) {
			if(!haveEmpresa) {
				csv.append(bean.getEmpresa().getNomefantasia());
				csv.append(";");
			}
			csv.append(bean.getCliente());
			csv.append(";");
			csv.append(bean.getContato().replaceAll("\n", " | "));
			csv.append(";");
			csv.append(bean.getTelefone().replaceAll("\n", " | "));
			csv.append(";");
			csv.append(bean.getEmail().replaceAll("\n", ""));
			csv.append(";");
			csv.append(bean.getContaatrasadareport());
			csv.append(";");
			csv.append(bean.getValor());
			csv.append(";");
			csv.append(bean.getValoratual());
			csv.append(";");
			if(bean.getUltimaCobranca() != null) csv.append(SinedDateUtils.toString(bean.getUltimaCobranca()));
			csv.append(";");
			if(bean.getProximaCobranca() != null) csv.append(SinedDateUtils.toString(bean.getProximaCobranca()));
			csv.append(";");
			
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "cobrancadevedor_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return new ResourceModelAndView(resource);
	}
	
	
}	