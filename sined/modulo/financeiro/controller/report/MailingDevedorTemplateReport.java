package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.LinkedHashMap;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContatotipoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.MailingDevedorBean;

@Controller(path = "/financeiro/relatorio/MailingDevedor", authorizationModule=ReportAuthorizationModule.class)
public class MailingDevedorTemplateReport  extends ReportTemplateController<MailingDevedorBean>{

	private DocumentoService documentoService;
	private CategoriaService categoriaService;
	private DocumentotipoService documentotipoService;
	private ContatotipoService contatotipoService;
	private ColaboradorService colaboradorService;
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setContatotipoService(ContatotipoService contatotipoService) {
		this.contatotipoService = contatotipoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}


	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.MAILING_DEVEDOR_LISTAGEM;
	}

	@Override
	protected String getFileName() {
		return "cobrancadevedor";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	MailingDevedorBean filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.MAILING_DEVEDOR_LISTAGEM);
	}

	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext  request, MailingDevedorBean filtro) throws Exception {
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		mapa.put("bean", documentoService.montaListagemDevedores(filtro));
		
		if(filtro.getEmpresa() != null){
			filtro.setEmpresa(empresaService.load(filtro.getEmpresa()));
		}
		if(filtro.getCategoria() != null){
			filtro.setCategoria(categoriaService.load(filtro.getCategoria()));
		}
		if(filtro.getCliente() != null){
			filtro.setCliente(clienteService.load(filtro.getCliente()));
		}
		if(filtro.getDocumentotipo() != null){
			filtro.setDocumentotipo(documentotipoService.load(filtro.getDocumentotipo()));
		}
		if(filtro.getCentrocusto() != null){
			filtro.setCentrocusto(centrocustoService.load(filtro.getCentrocusto()));
		}
		if(filtro.getContatotipo() != null){
			filtro.setContatotipo(contatotipoService.load(filtro.getContatotipo()));
		}
		if(filtro.getColaborador() != null){
			filtro.setColaborador(colaboradorService.load(filtro.getColaborador()));
		}
		
		mapa.put("filtro", filtro);
		return mapa;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, MailingDevedorBean filtro) throws Exception {
		return new ModelAndView("redirect:/financeiro/process/MailingDevedor");
	}

}
