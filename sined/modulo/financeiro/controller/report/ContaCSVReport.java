package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.DocumentoRelatorioFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/financeiro/relatorio/ContaCSV", authorizationModule=ReportAuthorizationModule.class)
public class ContaCSVReport extends ResourceSenderController<DocumentoRelatorioFiltro>{

	private DocumentoService documentoService;
	private ContagerencialService contagerencialService;
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,DocumentoRelatorioFiltro filtro) throws Exception {
		this.ajustaListasFiltro(filtro);
		String classe = request.getParameter("classe");
		
		if (StringUtils.isNotEmpty(classe)) {
			filtro.setTipoConta(classe);
		}
		request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		return documentoService.gerarRelatorioDocumentoCSV(filtro);
	}
	
	/**
	 * M�todo para ajustar as listas de Pagamento e Recebimento do filtro.]
	 * 
	 * @param filtro
	 * @author Fl�vio Tavares
	 */
	private void ajustaListasFiltro(DocumentoRelatorioFiltro filtro) {
		List<ItemDetalhe> listaFornecedor = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaCliente = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaColaborador = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaOutrospagamento = new ArrayList<ItemDetalhe>();
		
		List<ItemDetalhe> listaPagamentos = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaPagamentoa = filtro.getListaPagamentoa();
		List<ItemDetalhe> listaRecebimentode = filtro.getListaRecebimentode();
		if(SinedUtil.isListNotEmpty(listaPagamentoa)) listaPagamentos.addAll(listaPagamentoa);
		if(SinedUtil.isListNotEmpty(listaRecebimentode)) listaPagamentos.addAll(listaRecebimentode);
		
		if(SinedUtil.isListNotEmpty(listaPagamentos)){
			for (ItemDetalhe id : listaPagamentos) {
				switch(id.getTipopagamento()){
					case CLIENTE:
						listaCliente.add(id);
						break;
					case COLABORADOR:
						listaColaborador.add(id);
						break;
					case FORNECEDOR:
						listaFornecedor.add(id);
						break;
					case OUTROS:
						listaOutrospagamento.add(id);
						break;
				}
			}
		}
		filtro.setListaFornecedor(listaFornecedor);
		filtro.setListaCliente(listaCliente);
		filtro.setListaColaborador(listaColaborador);
		filtro.setListaOutrospagamento(listaOutrospagamento);
		
		if(filtro.getListaContagerencial() != null && filtro.getListaContagerencial().size() > 0){
			filtro.setListaContagerencial(this.ajustaListaContaGerencial(filtro.getListaContagerencial()));
		}
	}
	
	private List<ItemDetalhe> ajustaListaContaGerencial(List<ItemDetalhe> listaContagerencial) {
		List<Contagerencial> listaCG = new ArrayList<Contagerencial>();
		
		for (ItemDetalhe itemDetalhe : listaContagerencial) {
			listaCG.addAll(contagerencialService.findAllFilhos(contagerencialService.loadWithIdentificador(itemDetalhe.getContagerencial())));
		}
		
		List<ItemDetalhe> listaNova = new ArrayList<ItemDetalhe>();
		ItemDetalhe itemDetalhe;
		for (Contagerencial contagerencial : listaCG) {
			itemDetalhe = new ItemDetalhe();
			itemDetalhe.setContagerencial(contagerencial);
			listaNova.add(itemDetalhe);
		}
		
		return listaNova;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, DocumentoRelatorioFiltro filtro) throws Exception {
		return null;
	}

}
