package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sined.util.report.MergeReport;

@Controller(path = "/financeiro/relatorio/Notapromissoria", authorizationModule=ReportAuthorizationModule.class)
public class EmitirNotaPromissoriaReport extends ResourceSenderController<FiltroListagemSined>{

	private DocumentoService documentoService;
	
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	
	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		
		MergeReport mergeReport = new MergeReport("Notapromissoria"+SinedUtil.datePatternForReport()+".pdf");
		
		String selectedItens = request.getParameter("selectedItens");
		List<Documento> listaDocumentos = documentoService.findForNotaPromissoria(selectedItens);
		
		for (Documento documento2 : listaDocumentos) {
			if(!documento2.getDocumentoacao().equals(Documentoacao.PREVISTA) && !documento2.getDocumentoacao().equals(Documentoacao.DEFINITIVA)){
				request.addError("S� � permitido gerar nota promiss�ria de contas com a situa��o 'Prevista' ou 'Definitiva'.");
				SinedUtil.redirecionamento(request, "/financeiro/crud/Contareceber"  + ("true".equalsIgnoreCase(request.getParameter("ENTRADA")) ? "?ACAO=consultar&cddocumento=" + selectedItens : ""));
				return null;
			}
			if(documento2.getPessoa() == null || (documento2.getPessoa() != null && documento2.getPessoa().getCnpj() == null && documento2.getPessoa().getCpf() == null)){
				request.addError("N�o � poss�vel gerar nota promiss�ria. O pagador n�o possui CPF ou CNPJ cadastrados.");
				SinedUtil.redirecionamento(request, "/financeiro/crud/Contareceber"  + ("true".equalsIgnoreCase(request.getParameter("ENTRADA")) ? "?ACAO=consultar&cddocumento=" + selectedItens : ""));
				return null;
			}
		}
		
		for (Documento documento : listaDocumentos) {
			mergeReport.addReport(documentoService.gerarNotaPromissoria(documento));
			
			documentoService.criarRegistroHistorico(documento);
		}
		
		return mergeReport.generateResource();
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
