package br.com.linkcom.sined.modulo.financeiro.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/financeiro/relatorio/Arquivoconciliacao", authorizationModule=ReportAuthorizationModule.class)
public class ArquivoConciliacaoReport extends SinedReport<Object>{

	private MovimentacaoService movimentacaoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	Object filtro) throws Exception {
		return movimentacaoService.createArquivoConciliacaoReport(request);
	}

	@Override
	public String getNomeArquivo() {
		return "conciliacao_bancaria";
	}
	
	@Override
	public String getTitulo(Object filtro) {
		return "Concilia��o Banc�ria";
	}
}
