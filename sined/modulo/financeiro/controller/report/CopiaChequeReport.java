package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.CopiaChequeReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/CopiaCheque", authorizationModule=ReportAuthorizationModule.class)
public class CopiaChequeReport extends SinedReport<CopiaChequeReportFiltro> {

	private MovimentacaoService movimentacaoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, CopiaChequeReportFiltro filtro) throws Exception {		
		return movimentacaoService.createCopiaChequeReport(request);
	}

	@Override
	public String getTitulo(CopiaChequeReportFiltro filtro) {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void filtro(WebRequestContext request, CopiaChequeReportFiltro filtro) throws Exception {
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null");
		}
		
		int currentPage = movimentacaoService.setaOrderBy(filtro);
		
		List<Movimentacao> listaMovimentacao;
		if (request.getSession().getAttribute("listaCopiaCheque") == null || request.getParameter("filtrar") != null) {
			listaMovimentacao = movimentacaoService.findForListagemRelatorio(filtro);
			request.getSession().setAttribute("listaCopiaCheque", listaMovimentacao);
		} else {
			listaMovimentacao = (List<Movimentacao>) request.getSession().getAttribute("listaCopiaCheque");
		}
		
		// ORDENA��O MANUAL
		movimentacaoService.ordenaDtmovimentacao(filtro, listaMovimentacao);
//		movimentacaoService.ordenaNumeroCheque(filtro, listaMovimentacao);
		movimentacaoService.ordenaValor(filtro, listaMovimentacao);
		movimentacaoService.ordenaMovDescricao(filtro, currentPage, listaMovimentacao);
		movimentacaoService.ordenaTodosFornecedores(filtro, currentPage, listaMovimentacao);
		movimentacaoService.ordenaBanco(filtro, currentPage, listaMovimentacao);
		
		movimentacaoService.setaNumeros(filtro, listaMovimentacao);
		
		request.setAttribute("currentPage", filtro.getCurrentPage());
		request.setAttribute("numberOfPages", filtro.getNumberOfPages());
		
		request.setAttribute("listaMovimentacao", listaMovimentacao.subList(filtro.getCurrentPage()*filtro.getPageSize(), (filtro.getCurrentPage()+1)*filtro.getPageSize() > listaMovimentacao.size() ? listaMovimentacao.size() : (filtro.getCurrentPage()+1)*filtro.getPageSize()));
		
		request.setAttribute("listaEmpresa", getEmpresaService().findAtivos());
		
		request.setAttribute("firstDate", SinedDateUtils.toString(SinedDateUtils.firstDateOfMonth()));
		request.setAttribute("lastDate", SinedDateUtils.toString(SinedDateUtils.lastDateOfMonth()));
		
		super.filtro(request, filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "copiacheque";
	}

}
