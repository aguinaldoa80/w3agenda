package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaoService;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path = "/faturamento/relatorio/EmitirFaturaLocacaoMatricial",authorizationModule=ReportAuthorizationModule.class)
public class EmitirFaturaLocacaoMatricialReport extends ResourceSenderController<Contratofaturalocacao>{
	
	private ContratofaturalocacaoService contratofaturalocacaoService;
	
	public void setContratofaturalocacaoService(
			ContratofaturalocacaoService contratofaturalocacaoService) {
		this.contratofaturalocacaoService = contratofaturalocacaoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, Contratofaturalocacao contratofaturalocacao) throws Exception {
		String contratoStr = contratofaturalocacaoService.createReportFaturaLocacaoMatricial(contratofaturalocacao);
		Resource resource = new Resource();
		resource.setContentType("application/w3erp");
		resource.setFileName("faturalocacao_" + SinedUtil.datePatternForReport() + ".w3erp");
	    resource.setContents(contratoStr.getBytes());
	    
		return resource;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, Contratofaturalocacao filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/Contratofaturalocacao");
	}
	
}