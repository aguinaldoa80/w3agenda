package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/Carne", authorizationModule=ReportAuthorizationModule.class)
public class CarneReport extends SinedReport<FiltroListagemSined>{

	private ContareceberService contareceberService;
	
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) {
		if(request.getParameter("fromVenda") != null && request.getParameter("fromVenda").equals("true")){
			return new ModelAndView("redirect:/faturamento/crud/Venda" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdvenda=" + request.getParameter("selectedItens") : ""));
		} else {
			return new ModelAndView("redirect:/financeiro/crud/Contareceber" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cddocumento=" + request.getParameter("selectedItens") : ""));
		}
	}

	@Override
	public IReport createReportSined(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		return contareceberService.createCarne(request);
	}

	@Override
	public String getNomeArquivo() {
		return "carne";
	}

	@Override
	public String getTitulo(FiltroListagemSined filtro) {
		return "Carn�";
	}

}
