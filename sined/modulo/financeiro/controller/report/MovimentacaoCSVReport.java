package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.MovimentacaoFiltro;

@Controller(path = "/financeiro/relatorio/MovimentacaoCSV", authorizationModule=ReportAuthorizationModule.class)
public class MovimentacaoCSVReport extends ResourceSenderController<MovimentacaoFiltro>{

	private MovimentacaoService movimentacaoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,MovimentacaoFiltro filtro) throws Exception {
		return movimentacaoService.createMovimentacaoCSVReport(filtro);
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, MovimentacaoFiltro filtro) throws Exception {
		return null;
	}
	
}
