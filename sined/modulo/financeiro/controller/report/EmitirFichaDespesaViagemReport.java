package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Despesaviagemitem;
import br.com.linkcom.sined.geral.bean.Despesaviagemprojeto;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirDespesaViagemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirDespesaViagemItemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirDespesaViagemProjetoBean;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/financeiro/relatorio/EmitirFichaDespesaViagem", authorizationModule=ReportAuthorizationModule.class)
public class EmitirFichaDespesaViagemReport extends ReportTemplateController<ReportTemplateFiltro> {

	private DespesaviagemService despesaviagemService;
	
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_FICHA_DESPESA_VIAGEM;
	}

	@Override
	protected String getFileName() {
		return "ficha_despesaViagem";
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
	}

	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_FICHA_DESPESA_VIAGEM);
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		LinkedList<EmitirDespesaViagemBean> lista = new LinkedList<EmitirDespesaViagemBean>();
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.findForEmitirFichaDespesaViagem(SinedUtil.getItensSelecionados(request));
		SimpleDateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");
		
		if(listaDespesaviagem != null && listaDespesaviagem.size() > 0){
			for(Despesaviagem despesaviagem : listaDespesaviagem){
				EmitirDespesaViagemBean emitirDespesaViagemBean = new EmitirDespesaViagemBean();
				emitirDespesaViagemBean.setOrigemviagem(despesaviagem.getOrigemviagem());
				emitirDespesaViagemBean.setDestinoviagem(despesaviagem.getDestinoviagem());
				emitirDespesaViagemBean.setDescricao(despesaviagem.getDescricao());
				emitirDespesaViagemBean.setMotivo(despesaviagem.getMotivo());
				emitirDespesaViagemBean.setDtretornoprevisto(despesaviagem.getDtretornoprevisto() != null ? formatadorData.format(despesaviagem.getDtretornoprevisto()) : "");
				emitirDespesaViagemBean.setDtretornorealizado(despesaviagem.getDtretornorealizado() != null ? formatadorData.format(despesaviagem.getDtretornorealizado()) : "");
				emitirDespesaViagemBean.setColaborador(despesaviagem.getColaborador() != null ? despesaviagem.getColaborador().getNome() : "");
				emitirDespesaViagemBean.setSituacao(despesaviagem.getSituacaodespesaviagem() != null ? despesaviagem.getSituacaodespesaviagem().getDescricao() : "");
				emitirDespesaViagemBean.setObservacao(despesaviagem.getObservacao());
				
				if(despesaviagem.getListaDespesaviagemitem() != null && despesaviagem.getListaDespesaviagemitem().size() > 0){
					LinkedList<EmitirDespesaViagemItemBean> listaDespesaViagemItemBean = new LinkedList<EmitirDespesaViagemItemBean>();
					for(Despesaviagemitem despesaviagemitem : despesaviagem.getListaDespesaviagemitem()){
						EmitirDespesaViagemItemBean emitirDespesaViagemItemBean = new EmitirDespesaViagemItemBean();
						emitirDespesaViagemItemBean.setDespesaviagemtipo(despesaviagemitem.getDespesaviagemtipo() != null ? despesaviagemitem.getDespesaviagemtipo().getNome() : "");
						emitirDespesaViagemItemBean.setDocumento(despesaviagemitem.getDocumento());
						emitirDespesaViagemItemBean.setQuantidade(despesaviagemitem.getQuantidade());
						emitirDespesaViagemItemBean.setValorprevisto(despesaviagemitem.getValorprevisto());
						emitirDespesaViagemItemBean.setValorrealizado(despesaviagemitem.getValorrealizado());
						
						listaDespesaViagemItemBean.add(emitirDespesaViagemItemBean);
					}
					emitirDespesaViagemBean.setListaDespesaviagemitem(listaDespesaViagemItemBean);
				}
				
				if(despesaviagem.getListaDespesaviagemprojeto() != null && despesaviagem.getListaDespesaviagemprojeto().size() > 0){
					LinkedList<EmitirDespesaViagemProjetoBean> listaDespesaViagemProjetoBean = new LinkedList<EmitirDespesaViagemProjetoBean>();
					for(Despesaviagemprojeto despesaviagemprojeto : despesaviagem.getListaDespesaviagemprojeto()){
						EmitirDespesaViagemProjetoBean emitirDespesaViagemProjetoBean = new EmitirDespesaViagemProjetoBean();
						emitirDespesaViagemProjetoBean.setProjeto(despesaviagemprojeto.getProjeto() != null ? despesaviagemprojeto.getProjeto().getNome() : "");
						emitirDespesaViagemProjetoBean.setPercentual(despesaviagemprojeto.getPercentual());
						
						listaDespesaViagemProjetoBean.add(emitirDespesaViagemProjetoBean);
					}
					
					emitirDespesaViagemBean.setListaDespesaviagemprojeto(listaDespesaViagemProjetoBean);
				}
				lista.add(emitirDespesaViagemBean);
			}
		}
		mapa.put("lista", lista);
		return mapa;
	}
	
}
