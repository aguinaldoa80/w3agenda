package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.W3ReportTemplateFunctions;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.geradorrelatorio.templateengine.GroovyTemplateEngine;
import br.com.linkcom.geradorrelatorio.templateengine.ITemplateEngine;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.service.ChequemovimentacaoService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ChequeMovimentacaoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ChequeMovimentacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/financeiro/relatorio/ChequeMovimentacao", authorizationModule=ReportAuthorizationModule.class)
public class ChequeMovimentacaoReport extends MultiActionController {

	private MovimentacaoService movimentacaoService;
	private ContapagarService contapagarService;
	private ReportTemplateService reportTemplateService;
	
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}
	
	public ModelAndView popup(WebRequestContext request, ChequeMovimentacaoFiltro filtro) throws CloneNotSupportedException {
		
		ChequemovimentacaoService chequemovimentacaoService = new ChequemovimentacaoService();
		
		String whereIn = request.getParameter("selectedItens");
		String whereInCheque = request.getParameter("whereInCheque");
		
		List<Movimentacao> listaMovimentacao = movimentacaoService.findForChequeMovimentacao(whereIn, whereInCheque);
		 
		List<ChequeMovimentacaoBean> listaBean = chequemovimentacaoService.getListaBean(listaMovimentacao);
		List<ReportTemplateBean> reportTemplateBean = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.CHEQUE_MOVIMENTACAO);
		
		ChequeMovimentacaoBean bean = new ChequeMovimentacaoBean();
		bean.setWhereIn(whereIn);
		bean.setWhereInCheque(whereInCheque);
		
		return new ModelAndView("direct:/relatorio/popup/chequemovimentacao")
					.addObject("chequeMovimentacaoBean", bean)
					.addObject("listaChequemovimentacaoASC", chequemovimentacaoService.getListaOrdenada(listaBean, true))
					.addObject("listaChequemovimentacaoDESC", chequemovimentacaoService.getListaOrdenada(listaBean, false))
					.addObject("descricao", new String("Impress�o de Cheque"))
					.addObject("listaVazia", listaBean.isEmpty())
					.addObject("listaCategoriaTemplate", reportTemplateBean != null ? reportTemplateBean : new ListSet<ReportTemplateBean>(ReportTemplateBean.class));
	}

	public ModelAndView gerar(WebRequestContext request, ChequeMovimentacaoFiltro filtro) throws Exception {
		ChequemovimentacaoService chequemovimentacaoService = new ChequemovimentacaoService();
		
		String whereIn = request.getParameter("selectedItens");
		String whereInCheque = request.getParameter("whereInCheque");
		String order = request.getParameter("order");
		boolean asc = order==null || order.equals("asc");
		
		List<Movimentacao> listaMovimentacao = movimentacaoService.findForChequeMovimentacao(whereIn, whereInCheque);
		List<ChequeMovimentacaoBean> listaBean = chequemovimentacaoService.getListaBean(listaMovimentacao);
		List<ChequeMovimentacaoBean> listaRelatorio = chequemovimentacaoService.getListaOrdenada(listaBean, asc);
		
		return movimentacaoService.createChequeMovimentacaoReport(listaRelatorio);
	}
	
	public ModelAndView gerarContapagar(WebRequestContext request, ChequeMovimentacaoFiltro filtro) throws Exception {
		ChequemovimentacaoService chequemovimentacaoService = new ChequemovimentacaoService();
		
		String whereIn = request.getParameter("selectedItens");
		String order = request.getParameter("order");
		boolean asc = order==null || order.equals("asc");
		
		List<Documento> listaContapagar = contapagarService.findContasPreenchidas(whereIn);
		List<ChequeMovimentacaoBean> listaBean = chequemovimentacaoService.getListaBeanDocumento(listaContapagar);
		List<ChequeMovimentacaoBean> listaRelatorio = chequemovimentacaoService.getListaOrdenada(listaBean, asc);
		
		return movimentacaoService.createChequeMovimentacaoReport(listaRelatorio);
	}
	
	
	
	public ModelAndView popupContapagar(WebRequestContext request, ChequeMovimentacaoFiltro filtro) throws CloneNotSupportedException {
		ChequemovimentacaoService chequemovimentacaoService = new ChequemovimentacaoService();
		
		String whereIn = request.getParameter("selectedItens");
		String whereInCheque = request.getParameter("whereInCheque");
		List<ReportTemplateBean> reportTemplateBean = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.CHEQUE_MOVIMENTACAO);
		
		if(StringUtils.isNotBlank(whereInCheque)){
			List<Movimentacao> listaMovimentacao = movimentacaoService.findForChequeMovimentacao(null, whereInCheque);
			if(SinedUtil.isListNotEmpty(listaMovimentacao)){
				whereIn = CollectionsUtil.listAndConcatenate(listaMovimentacao, "cdmovimentacao", ",");
			}
		}
		
		List<Documento> listaContapagar = contapagarService.findContasPreenchidas(whereIn);
		
		for (Documento documento : listaContapagar) {
			if (!documento.getDocumentoacao().equals(Documentoacao.AUTORIZADA) &&
					!documento.getDocumentoacao().equals(Documentoacao.BAIXADA) &&
					!documento.getDocumentoacao().equals(Documentoacao.BAIXADA_PARCIAL) &&
					!documento.getDocumentoacao().equals(Documentoacao.AUTORIZADA_PARCIAL)) {
				request.addError("Conta(s) a pagar difirente de 'AUTORIZADA', 'BAIXADA', 'AUTORIZADA PARCIAL'.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		List<ChequeMovimentacaoBean> listaBean = chequemovimentacaoService.getListaBeanDocumento(listaContapagar);
		
		return new ModelAndView("direct:/relatorio/popup/chequemovimentacao")
					.addObject("chequeMovimentacaoBean", new ChequeMovimentacaoBean(request.getParameter("selectedItens")))
					.addObject("listaChequemovimentacaoASC", chequemovimentacaoService.getListaOrdenada(listaBean, true))
					.addObject("listaChequemovimentacaoDESC", chequemovimentacaoService.getListaOrdenada(listaBean, false))
					.addObject("descricao", new String("Impress�o de Cheque"))
					.addObject("listaVazia", listaBean.isEmpty())
					.addObject("acao", "Contapagar")
					.addObject("listaCategoriaTemplate", reportTemplateBean != null ? reportTemplateBean : new ListSet<ReportTemplateBean>(ReportTemplateBean.class));
	}
	
	public void gerarSemPdf(WebRequestContext request, ChequeMovimentacaoFiltro filtro) throws Exception {
		String relatorioHTML = gerarReportTemplate(request);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(relatorioHTML);
	}
	
	public ModelAndView gerarPrintW3erp(WebRequestContext request, ChequeMovimentacaoFiltro filtro) throws Exception {
		String relatorioHTML = gerarReportTemplate(request);
		
		Resource resource = new Resource("application/remote_printing", "impressaocheque_" + SinedUtil.datePatternForReport() + ".w3erp", relatorioHTML.getBytes());
		return new ResourceModelAndView(resource);
	}
	
	private String gerarReportTemplate(WebRequestContext request) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		String whereInCheque = request.getParameter("whereInCheque");
		
		String cdreporttemplate = request.getParameter("cdreporttemplate");
		ReportTemplateBean reportTemplateBean = null;
		if (StringUtils.isNotBlank(cdreporttemplate)) {
			reportTemplateBean = reportTemplateService.load(new ReportTemplateBean(Integer.parseInt(cdreporttemplate)));
		} else {
			reportTemplateBean = reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.CHEQUE_MOVIMENTACAO);
		}
		ChequemovimentacaoService chequemovimentacaoService = new ChequemovimentacaoService();
		ITemplateEngine engine = new GroovyTemplateEngine().build(reportTemplateBean.getLeiaute());
		
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		mapa.put("lista", chequemovimentacaoService.geraListaChequeMovimentacaoBean(whereIn, whereInCheque, request.getParameter("order")));
		mapa.put("Funcao", new W3ReportTemplateFunctions());

		return engine.make(mapa);
	}

}