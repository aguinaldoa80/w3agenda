package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ExtratoContaReportFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/ExtratoConta", authorizationModule=ReportAuthorizationModule.class)
public class ExtratoContaReport extends SinedReport<ExtratoContaReportFiltro>{

	private ContaService contaService;
	private MovimentacaoService movimentacaoService;
	private MovimentacaoacaoService movimentacaoacaoService;
	private CentrocustoService centrocustoService;

	public void setContaService(ContaService contaService) {this.contaService = contaService; }
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService; }
	public void setMovimentacaoacaoService(MovimentacaoacaoService movimentacaoacaoService) {this.movimentacaoacaoService = movimentacaoacaoService; }
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;	}

	@Override
	public IReport createReportSined(WebRequestContext request,ExtratoContaReportFiltro filtro) throws Exception {
		return movimentacaoService.gerarRelatorioExtratoConta(filtro);
	}
	
	@Override
	public String getTitulo(ExtratoContaReportFiltro filtro) {
		return "EXTRATO DE CONTA";
	}
	
	@Override
	public String getNomeArquivo() {
		return "extratoConta";
	}

	@Override
	protected Empresa getEmpresa(ExtratoContaReportFiltro filtro) {
		if (filtro.getEmpresa() != null) {
			return super.getEmpresa(filtro);
		}

		Empresa empresa = null;
		Conta conta = filtro.getDescricaoVinculo();
		empresa = contaService.loadContaForRemessa(conta).getEmpresa();
		if (empresa != null)
			empresa = getEmpresaService().loadComArquivo(empresa);
		
		return empresa;
	}
	
	@Override
	protected void filtro(WebRequestContext request, ExtratoContaReportFiltro filtro) throws Exception {
		List<Conta> listaConta = null;
		List<Empresa> empresas = empresaService.findAll();
		if(empresas != null && empresas.size() == 1){
			filtro.setEmpresa(empresas.get(0));
		}
		if (filtro.getContaTipo() != null && filtro.getContaTipo().getCdcontatipo() != null) {
			listaConta = contaService.findByTipo(filtro.getContaTipo(), filtro.getEmpresa());
		} else {
			listaConta = new ArrayList<Conta>();
		}
		filtro.setModoPaisagem(Boolean.TRUE);
		
		request.setAttribute("listaConta", listaConta);
		List<Movimentacaoacao> listaAcao = movimentacaoacaoService.findForCombo();
		listaAcao.remove(Movimentacaoacao.CANCELADA);
		filtro.setListaMovimentacaoacao(listaAcao);
		request.setAttribute("listaAcaoCompleta", listaAcao);
		request.setAttribute("listCentroCusto", centrocustoService.findAtivos());
		super.filtro(request, filtro);
	}
	
	/**
	 * Fun��o que carrega dados para o combo de Descri��o do V�nculo
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#findByTipo(Contatipo, Empresa)
	 * @see br.com.linkcom.sined.util.SinedUtil#convertToJavaScript(List, String, String)
	 * @param request
	 * @param filtro
	 * @author Hugo Ferreira
	 */
	public void ajaxComboDescricaoVinculo(WebRequestContext request, ExtratoContaReportFiltro filtro) {
		List<Conta> listaConta = null;
		if (filtro.getContaTipo() == null) {
			listaConta = new ArrayList<Conta>();
		} else {
			listaConta = contaService.findByTipo(filtro.getContaTipo(), filtro.getEmpresa());
		}
		String valoresCombo = SinedUtil.convertToJavaScript(listaConta, "listaConta", "");
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(valoresCombo);
	}
}
