package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.LinkedHashMap;
import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.geral.service.ChequemovimentacaoService;

@Controller(path = "/financeiro/relatorio/CopiaChequeMovimentacaoTemplate", authorizationModule=ReportAuthorizationModule.class)
public class CopiaChequeMovimentacaoTemplate extends ReportTemplateController<ReportTemplateFiltro> {

	private ChequeService chequeService;
	
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_COPIA_CHEQUE_MOVIMENTACAO_CHEQUE;
	}

	@Override
	protected String getFileName() {
		return "c�pia de cheque";
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(
			WebRequestContext request, ReportTemplateFiltro filtro)
			throws Exception {
		String whereInCheque = request.getParameter("whereInCheques");
		String whereInMovimentacoes = request.getParameter("whereInMovimentacoes");
		
		List<Cheque> listaCheques = chequeService.findForCopiaChequeTemplate(whereInCheque, whereInMovimentacoes);
		
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		ChequemovimentacaoService chequemovimentacaoService = new ChequemovimentacaoService();
		mapa.put("lista", chequemovimentacaoService.geraListaCopiaChequeBean(listaCheques));
		return mapa;
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,
			ReportTemplateFiltro filtro) {
		if (filtro.getTemplate() != null) {
			return getReportTemplateService().load(filtro.getTemplate());
		} else {
			return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COPIA_CHEQUE_MOVIMENTACAO_CHEQUE);
		}
	}
}
