package br.com.linkcom.sined.modulo.financeiro.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.MovimentacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/Movimentacao", authorizationModule=ReportAuthorizationModule.class)
public class MovimentacaoReport extends SinedReport<MovimentacaoFiltro>{

	private MovimentacaoService movimentacaoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,MovimentacaoFiltro filtro) throws Exception {
		return movimentacaoService.createMovimentacaoReport(filtro);
	}

	@Override
	public String getTitulo(MovimentacaoFiltro filtro) {
		return "MOVIMENTAÇÃO FINANCEIRA";
	}
	
	@Override
	public String getNomeArquivo() {
		return "movimentacaofinanceira";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, MovimentacaoFiltro filtro) {
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null)
			report.addParameter("projeto", projetoService.load(filtro.getProjeto(), "projeto.nome").getNome());
		
		if(filtro.getCentrocusto() != null && filtro.getCentrocusto().getCdcentrocusto() != null)
			report.addParameter("centroCusto", centrocustoService.load(filtro.getCentrocusto(), "centrocusto.nome").getNome());
		
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null)
			report.addParameter("empresaFiltro", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		
		if(filtro.getDtperiodoDe() != null || filtro.getDtperiodoAte() != null)
			report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtperiodoDe(), filtro.getDtperiodoAte()));
	}
	
}
