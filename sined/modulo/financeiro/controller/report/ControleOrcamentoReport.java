package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ControleorcamentoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ControleorcamentoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Controller(path = "/sistema/relatorio/ControleOrcamento", authorizationModule=ReportAuthorizationModule.class)
@DisplayName("Controle de Or�amento")
public class ControleOrcamentoReport extends SinedReport<ControleorcamentoFiltro>{
	
	private ControleorcamentoService controleorcamentoService;
	
	public void setControleorcamentoService(ControleorcamentoService controleorcamentoService) {
		this.controleorcamentoService = controleorcamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ControleorcamentoFiltro filtro) throws Exception {
		return controleorcamentoService.gerarRelatorioControleOrcamento(filtro);
	}
	
	@Override
	public String getTitulo(ControleorcamentoFiltro filtro) {
		return "CONTROLE DE OR�AMENTO";
	}
	
	@Override
	public String getNomeArquivo() {
		return "controleOrcamento";
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request,
			ControleorcamentoFiltro filtro) throws Exception {
		request.setAttribute("titulo", getTitulo(filtro));
		return super.doGerar(request, filtro);
	}
}
