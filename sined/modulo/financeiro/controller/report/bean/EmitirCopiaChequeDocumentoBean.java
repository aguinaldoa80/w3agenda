package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;

public class EmitirCopiaChequeDocumentoBean {
	protected String classe;
	protected Integer conta;
	protected String pessoa;
	protected String identificadorpessoa;
	protected String documento;
	protected Double valor;
	protected LinkedList<EmitirCopiaChequeChequeDoc> listaCheque;
	protected LinkedList<EmitirCopiaChequeMovimentacaoDoc> listaMovimentacao;
	
	public EmitirCopiaChequeDocumentoBean(Documento documento){
		this.setClasse(documento.getDocumentoclasse() != null? documento.getDocumentoclasse().getNome(): "");
		this.setConta(documento.getConta() != null? documento.getConta().getCdconta(): null);
		this.setPessoa(documento.getDescricaoPagamento());
		this.setIdentificadorpessoa(documento.getIdentificadorpessoa());
		this.setDocumento(documento.getNumeroCddocumento());
		this.setValor(documento.getValorDouble());
		this.setListaMovimentacao(new LinkedList<EmitirCopiaChequeMovimentacaoDoc>());
		this.setListaCheque(new LinkedList<EmitirCopiaChequeChequeDoc>());
		if(documento.getCheque() != null){
			listaCheque.add(new EmitirCopiaChequeChequeDoc(documento.getCheque()));
		}
		for(Movimentacaoorigem movOrigem: documento.getListaMovimentacaoOrigem()){
			if(movOrigem.getMovimentacao()!=null && (!Movimentacaoacao.CANCELADA.equals(movOrigem.getMovimentacao().getMovimentacaoacao())) &&
				(!Movimentacaoacao.CANCEL_MOVIMENTACAO.equals(movOrigem.getMovimentacao().getMovimentacaoacao()))){
				Movimentacao movimentacao = movOrigem.getMovimentacao();
				listaMovimentacao.add(new EmitirCopiaChequeMovimentacaoDoc(movimentacao));
				if(movimentacao.getCheque() != null && !movimentacao.getCheque().equals(documento.getCheque())){
					listaCheque.add(new EmitirCopiaChequeChequeDoc(movimentacao.getCheque()));
				}
			}
		}
	}
	
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public Integer getConta() {
		return conta;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public String getPessoa() {
		return pessoa;
	}
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public String getIdentificadorpessoa() {
		return identificadorpessoa;
	}
	public void setIdentificadorpessoa(String identificadorpessoa) {
		this.identificadorpessoa = identificadorpessoa;
	}
	public LinkedList<EmitirCopiaChequeChequeDoc> getListaCheque() {
		return listaCheque;
	}
	public void setListaCheque(LinkedList<EmitirCopiaChequeChequeDoc> listaCheque) {
		this.listaCheque = listaCheque;
	}
	public LinkedList<EmitirCopiaChequeMovimentacaoDoc> getListaMovimentacao() {
		return listaMovimentacao;
	}
	public void setListaMovimentacao(
			LinkedList<EmitirCopiaChequeMovimentacaoDoc> listaMovimentacao) {
		this.listaMovimentacao = listaMovimentacao;
	}
}
