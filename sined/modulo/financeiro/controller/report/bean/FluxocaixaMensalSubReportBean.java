package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class FluxocaixaMensalSubReportBean {

	protected Integer mes;
	protected Integer ano;
	protected String descricao;
	protected Money valor;
	protected Money valorOrcado;
	private Boolean valorAlteradoOrcamento;
	
	public FluxocaixaMensalSubReportBean(Integer mes, Integer ano) {
		this.mes = mes;
		this.ano = ano;
	}	
	public FluxocaixaMensalSubReportBean(String descricao, Money valor) {
		this.descricao = descricao;
		this.valor = valor;
	}
	public FluxocaixaMensalSubReportBean(String descricao, Money valor, Money valorOrcado) {
		this.descricao = descricao;
		this.valor = valor;
		this.valorOrcado = valorOrcado;
	}
	
	public Integer getMes() {
		return mes;
	}
	public Integer getAno() {
		return ano;
	}
	public String getDescricao() {
		if ((this.descricao == null || this.descricao.equals("")) && this.mes != null && this.ano != null){
			this.descricao = getMesFormatado(this.mes).concat(" / ").concat(this.ano.toString());
		}
		return descricao;
	}
	public Money getValor() {
		return valor;
	}
	public Money getValorOrcado() {
		return valorOrcado;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}	
	public void setValorOrcado(Money valorOrcado) {
		this.valorOrcado = valorOrcado;
	}
	
	public String getMesAno() {
		if (this.mes != null && this.ano != null){
			return this.mes + "/" + this.ano;
		}
		return "";
	}
	
	private String getMesFormatado(Integer mes2) {
		if(this.mes != null){
			if(this.mes <= 9){
				return "0" + this.mes.toString();
			}
			return this.mes.toString();
		}
		return "";
	}

	public Boolean getValorAlteradoOrcamento() {
		return valorAlteradoOrcamento;
	}
	public void setValorAlteradoOrcamento(Boolean valorAlteradoOrcamento) {
		this.valorAlteradoOrcamento = valorAlteradoOrcamento;
	}
	


}
