package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.util.SinedDateUtils;

public class ChequeMovimentacaoBean implements Comparable<ChequeMovimentacaoBean>{

	private Integer ordem;
	private String chequenumero;
	private Money valor;
	private String descricao;
	private Date data;
	private String whereIn;
	private String whereInCheque;
	private String cidade;
	private String valorextenso;
	private String valorextenso1;
	private String valorextenso2;
	private String nominal;
	private ReportTemplateBean templateChequeMovimentacao;
	
	private Cheque cheque;
	
	public ChequeMovimentacaoBean(){
	}
	
	public ChequeMovimentacaoBean(String whereIn){
		this.whereIn = whereIn;
	}
	
	public ChequeMovimentacaoBean(Cheque cheque, Money valor, String descricao, Date data, String cidade, String nominal){	
		this.cheque = cheque;
		if(cheque != null && cheque.getNumero() != null){
			this.chequenumero =  cheque.getNumero().toString();
		}
		this.valor = valor;
		this.descricao = descricao;
		this.data = data;
		this.cidade = cidade;
		this.nominal = nominal;
		ajustaValorExtenso(valor);
	}
	
	public ChequeMovimentacaoBean(String chequenumero, Money valor, String descricao, Date data, String cidade, String nominal){		
		this.chequenumero = chequenumero;
		this.valor = valor;
		this.descricao = descricao;
		this.data = data;
		this.cidade = cidade;
		this.nominal = nominal;
		ajustaValorExtenso(valor);
	}	
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		ChequeMovimentacaoBean bean = new ChequeMovimentacaoBean();		
		bean.ordem = this.ordem;
		bean.chequenumero = this.chequenumero;
		bean.valor = this.valor;
		bean.descricao = this.descricao;
		bean.data = this.data;
		bean.whereIn = this.whereIn;
		bean.cidade = this.cidade;
		bean.valorextenso = this.valorextenso;
		bean.valorextenso1 = this.valorextenso1;
		bean.valorextenso2 = this.valorextenso2;
		bean.nominal = this.nominal;
		
		return bean;
	}
	
	public void ajustaValorExtensoInterno(){
		ajustaValorExtenso(this.valor);
	}
	
	private void ajustaValorExtenso(Money valor){
		String valorextenso = Util.strings.tiraAcento(new Extenso(valor).toString());
		
		this.valorextenso = valorextenso;
		
		int tamanho = valorextenso.length(); 
		if (tamanho>90){
			for (int i=100; i<=tamanho; i++){
				char caracter = valorextenso.charAt(i);
				if (caracter==' '){
					setValorextenso1(valorextenso.substring(0, i));
					setValorextenso2(valorextenso.substring(i+1));
					break;
				}
			}
		}else {
			setValorextenso1(valorextenso);		
			setValorextenso2("");
		}
	}
		
	@DisplayName("Ordem")
	public Integer getOrdem() {
		return ordem;
	}
	
	@DisplayName("Cheque N�")
	public String getChequenumero() {
		return chequenumero;
	}
	
	@DisplayName("Valor")
	public Money getValor() {
		return valor;
	}
	
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Data baixa")
	public Date getData() {
		return data;
	}	
	
	public String getWhereIn() {
		return whereIn;
	}	
	
	public String getValorextenso() {
		return valorextenso;
	}

	public String getValorextenso1() {
		return valorextenso1;
	}
	
	public String getValorextenso2() {
		return valorextenso2;
	}

	public String getCidade() {
		return cidade;
	}
	public String getDia() {
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(getData());
		
		DecimalFormat formatador = new DecimalFormat("00");	
		
		return formatador.format(calendar.get(Calendar.DAY_OF_MONTH));
	}

	public String getMes() {
		return SinedDateUtils.getDescricaoMes(getData());
	}

	public String getAno() {
		
		SimpleDateFormat formatador = new SimpleDateFormat("yy");
		
		return formatador.format(getData());		
	}	

	public String getNominal() {
		return nominal;
	}
	
	@DisplayName("Modelo Cheque")
	public ReportTemplateBean getTemplateChequeMovimentacao() {
		return templateChequeMovimentacao;
	}
	public void setTemplateChequeMovimentacao(ReportTemplateBean templateChequeMovimentacao) {
		this.templateChequeMovimentacao = templateChequeMovimentacao;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}	
	
	public void setValorextenso(String valorextenso) {
		this.valorextenso = valorextenso;
	}

	public void setValorextenso1(String valorextenso1) {
		this.valorextenso1 = valorextenso1;
	}

	public void setValorextenso2(String valorextenso2) {
		this.valorextenso2 = valorextenso2;
	}	

	public void setNominal(String nominal) {
		this.nominal = nominal;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}

	public int compareTo(ChequeMovimentacaoBean o) {
		
		String cheque1 = this.getChequenumero()!=null ? this.getChequenumero() : "";
		String cheque2 = o.getChequenumero()!=null ? o.getChequenumero() : "";
		
		return cheque1.compareTo(cheque2);
	}

	public Cheque getCheque() {
		return cheque;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}

	public String getWhereInCheque() {
		return whereInCheque;
	}

	public void setWhereInCheque(String whereInCheque) {
		this.whereInCheque = whereInCheque;
	}
}