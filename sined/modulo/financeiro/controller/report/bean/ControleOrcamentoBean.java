package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipo;

public class ControleOrcamentoBean {
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected String ano;
	protected List<ControleOrcamentoItemBean> listaItens;
	protected Projetotipo projetotipo;
	protected Date anoInicio;
	protected Date anoFim;
	protected Integer qtdMes;
	protected String descricao;
	
		
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public String getAno() {
		return ano;
	}
	public List<ControleOrcamentoItemBean> getListaItens() {
		return listaItens;
	}
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}
	public Date getAnoFim() {
		return anoFim;
	}
	public Date getAnoInicio() {
		return anoInicio;
	}
	public Integer getQtdMes() {
		return qtdMes;
	}
	public String getDescricao() {
		return descricao;
	}
	
	public void setQtdMes(Integer qtdMes) {
		this.qtdMes = qtdMes;
	}
	public void setAnoFim(Date anoFim) {
		this.anoFim = anoFim;
	}
	public void setAnoInicio(Date anoInicio) {
		this.anoInicio = anoInicio;
	}
	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public void setListaItens(List<ControleOrcamentoItemBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj != null){
			Boolean equal = Boolean.TRUE;
			ControleOrcamentoBean other = (ControleOrcamentoBean) obj;
			if((this.ano == null && other.getAno() == null) 
					|| (this.ano != null && other.getAno() != null && this.ano.equals(other.getAno()))){
				if((this.centrocusto == null && other.getCentrocusto() == null) 
						|| (this.centrocusto != null && other.getCentrocusto() != null && this.centrocusto.equals(other.getCentrocusto()))){
					if((this.projeto == null && other.getProjeto() == null) 
							|| (this.projeto != null && other.getProjeto()!= null && this.projeto.equals(other.getProjeto()))){
						equal = Boolean.TRUE;
					} else{
						equal = Boolean.FALSE;
					}
				} else{
					equal = Boolean.FALSE;
				}
			} else{
				equal = Boolean.FALSE;
			}
			return equal;
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		Integer hash = 0;
		if(this.ano != null){
			try{
				hash += Integer.parseInt(this.ano);
			} catch (Exception e) {	}
		}
		if(this.centrocusto != null && this.centrocusto.getCdcentrocusto() != null){
			try{
				hash += this.centrocusto.getCdcentrocusto();
			} catch (Exception e) {	}
		}
		if(this.projeto != null && this.projeto.getCdprojeto() != null){
			try{
				hash += this.projeto.getCdprojeto();
			} catch (Exception e) {	}
		}
		return hash;
	}
}
