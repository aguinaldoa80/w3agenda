package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import br.com.linkcom.neo.types.Money;

/**
 * @author Luiz Romario
 *
 */
public class ExtratoContaSubReportBean {
	private String mes;
	private Money totalCredito;
	private Money totalDebito;
	private Money saldo;
	
	public String getMes() {
		return mes;
	}
	public Money getTotalCredito() {
		if(totalCredito == null){
			totalCredito = new Money(0);
		}
		return totalCredito;
	}
	public Money getTotalDebito() {
		if(totalDebito == null){
			totalDebito = new Money(0);
		}
		return totalDebito;
	}
	public Money getSaldo() {
		saldo = getTotalCredito().subtract(getTotalDebito());
		return saldo;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public void setTotalCredito(Money totalCredito) {
		this.totalCredito = totalCredito;
	}
	public void setTotalDebito(Money totalDebito) {
		this.totalDebito = totalDebito;
	}
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
}
