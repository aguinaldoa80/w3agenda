package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Movimentacao;

import com.ibm.icu.text.SimpleDateFormat;

public class EmitirCopiaChequeBean {

	protected String empresa;
	protected String conta;
	protected String banco;
	protected String agencia;
	protected String numero;
	protected String emitente;
	protected String bompara;
	protected Double valor;
	protected String linhanumerica;
	protected String observacao;
	protected LinkedList<EmitirCopiaChequeMovimentacao> listaMovimentacao;

	public EmitirCopiaChequeBean(Cheque cheque) {
		this.setEmpresa(cheque.getEmpresa() != null ? cheque.getEmpresa()
				.getRazaosocialOuNome() : "");
		this.setConta(cheque.getConta() != null ? cheque.getConta().toString()
				: "");
		this.setBanco(cheque.getBanco() != null ? cheque.getBanco().toString()
				: "");
		this.setBompara(cheque.getDtbompara() != null ? new SimpleDateFormat(
				"dd/MM/yyyy").format(cheque.getDtbompara()) : "");
		this.setAgencia(cheque.getAgencia() != null ? cheque.getAgencia()
				.toString() : "");
		this.setNumero(cheque.getNumero());
		this.setEmitente(cheque.getEmitente());
		this.setValor(cheque.getValor() != null ? cheque.getValor().getValue()
				.doubleValue() : null);
		this.setLinhanumerica(cheque.getLinhanumerica());
		this.setObservacao(cheque.getObservacao());
		this.setListaMovimentacao(new LinkedList<EmitirCopiaChequeMovimentacao>());

		if(cheque.getListaMovimentacao()!=null){
			for (Movimentacao mov : cheque.getListaMovimentacao()){
				this.getListaMovimentacao().add(
						new EmitirCopiaChequeMovimentacao(mov));
			}			
		}
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getEmitente() {
		return emitente;
	}

	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}

	public String getBompara() {
		return bompara;
	}

	public void setBompara(String bompara) {
		this.bompara = bompara;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getLinhanumerica() {
		return linhanumerica;
	}

	public void setLinhanumerica(String linhanumerica) {
		this.linhanumerica = linhanumerica;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public LinkedList<EmitirCopiaChequeMovimentacao> getListaMovimentacao() {
		return listaMovimentacao;
	}

	public void setListaMovimentacao(
			LinkedList<EmitirCopiaChequeMovimentacao> listaMovimentacao) {
		this.listaMovimentacao = listaMovimentacao;
	}
}
