package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class EmitirDespesaViagemItemBean {
	
	private String despesaviagemtipo;
	private String documento;
	private Integer quantidade;
	private Money valorprevisto;
	private Money valorrealizado;
	
	public String getDespesaviagemtipo() {
		return despesaviagemtipo;
	}
	public String getDocumento() {
		return documento;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public Money getValorprevisto() {
		return valorprevisto;
	}
	public Money getValorrealizado() {
		return valorrealizado;
	}
	public void setDespesaviagemtipo(String despesaviagemtipo) {
		this.despesaviagemtipo = despesaviagemtipo;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public void setValorprevisto(Money valorprevisto) {
		this.valorprevisto = valorprevisto;
	}
	public void setValorrealizado(Money valorrealizado) {
		this.valorrealizado = valorrealizado;
	}
}
