package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Taxa;

public class ContaReceberPagarBeanReport {	
	
	protected Integer cdconta;
	protected Date dtVencimento;
	protected Date dtPrimeiroVencimento;
	protected Date dtEmissao;
	protected String descricao;
	protected String numero;
	protected String descricaoPagamento;
	protected String parcelas;
	protected Money valor;
	protected Money taxa;
	protected Money valorAtual;
	protected Centrocusto centrocusto;
	protected Integer cdcentrocusto;
	protected Contagerencial contagerencial;
	protected Projeto projeto;
	protected Money valorRateio;
	protected Double percentualRateio;
	protected List<Rateioitem> listaRateioitem;
	protected Money valorCorrigido;
	protected String situacao;
	protected String situacaoabreviada;
	protected Integer cdindicecorrecao;
	protected String identificador;
	protected List<Centrocusto> listaCentrocusto = new ListSet<Centrocusto>(Centrocusto.class);
	protected String codigobarras;
	protected Money valorPago;
	protected Money valorOriginal;
	protected Taxa taxaConta;
	protected Money valorrestante;
	protected String formapagamento;
	
	protected String cheque;
	protected String datasPagto;
	protected Money valorMovimentacoes;
	protected Money valorValecompra;
	
	protected Documentotipo documentotipo;
	protected String clienteIdentificador;
	protected String observacaoItemRateio;
	protected Integer cdRateioItem;
	protected String numeroNota;
	protected String nomePessoa;
	protected String razaosocialPessoa;
	protected String outrospagamento;
	protected Integer tipopessoa;
	protected Integer tipopagamento;
	protected Money valorJurosMulta;
	protected Date dtcompetencia;
	protected Money valorAntecipado;
	
	public ContaReceberPagarBeanReport() {}

	public ContaReceberPagarBeanReport(Integer cdconta, Date dtVencimento, String descricao, String numero, String descricaoPagamento,
			String parcelas, Money valor, Money taxa, Money valorAtual, String centrocusto, Integer cdcentrocusto, String contagerencial, 
			String identificador, String projeto, Money valorRateio, String situacao, Date dtEmissao, Integer cdindicecorrecao, String cheque, String datasPagto) {
		this.cdconta = cdconta;
		this.dtVencimento = dtVencimento;
		this.dtEmissao = dtEmissao;
		this.descricao = descricao;
		this.numero = numero;
		this.descricaoPagamento = descricaoPagamento;
		this.parcelas = parcelas;
		this.valor = valor;
		this.taxa = taxa != null ? taxa : new Money();
		this.valorAtual = valorAtual;
		this.centrocusto = new Centrocusto(centrocusto);
		if(cdcentrocusto != null){
			this.getCentrocusto().setCdcentrocusto(cdcentrocusto);
		}
		this.cdcentrocusto = cdcentrocusto;
		this.contagerencial = new Contagerencial(contagerencial);
		this.identificador = identificador;
		this.projeto = new Projeto(projeto);
		this.valorRateio = valorRateio;
		this.situacao = situacao;
		this.cdindicecorrecao = cdindicecorrecao;
		this.cheque = cheque;
		this.datasPagto = datasPagto;
	}
	
	public ContaReceberPagarBeanReport(Integer cdconta, Date dtVencimento, String descricao, String numero,String formapagamento,
			String parcelas, Money valor, Money taxa, Money valorAtual, Money valorPago, String centrocusto, Integer cdcentrocusto, String contagerencial, Integer cdcontagerencial,
			String identificador, String projeto, Integer cdProjeto, Money valorRateio, Double percentualRateio, String situacao, Date dtEmissao, Integer cdindicecorrecao, String cheque, String datasPagto, 
			String _codigobarras, Integer _cdtaxa, Date dtPrimeiroVencimento, Money valorMovimentacoes, Money valorValecompra, Money valoraAntecipado, Documentotipo documentotipo, Integer cddocumentoacao,
			String nomePessoa, String razaosocialPessoa, String outrospagamento, Integer tipopagamento, Integer tipopessoa, Money valorJurosMulta, Date dtcompetencia) {
		this.cdconta = cdconta;
		this.dtVencimento = dtVencimento;
		this.dtPrimeiroVencimento = dtPrimeiroVencimento;
		this.dtEmissao = dtEmissao;
		this.dtcompetencia = dtcompetencia;
		this.descricao = descricao;
		this.numero = numero;
		this.formapagamento = formapagamento;
		this.parcelas = parcelas;
		this.valor = valor;
		this.taxa = taxa != null ? taxa : new Money();
		this.valorAtual = valorAtual;
		this.valorPago = valorPago != null ? valorPago : new Money();
		this.centrocusto = new Centrocusto(centrocusto);
		if(cdcentrocusto != null){
			this.getCentrocusto().setCdcentrocusto(cdcentrocusto);
		}
		this.cdcentrocusto = cdcentrocusto;
		this.contagerencial = new Contagerencial(cdcontagerencial, contagerencial);
		this.identificador = identificador;
		this.projeto = new Projeto(projeto);
		if(cdProjeto != null){
			this.getProjeto().setCdprojeto(cdProjeto);
		}
		this.valorRateio = valorRateio;
		this.percentualRateio = percentualRateio;
		this.situacao = situacao;
		this.cdindicecorrecao = cdindicecorrecao;
		this.cheque = cheque;
		this.datasPagto = datasPagto;
		this.codigobarras = _codigobarras;
		if (_cdtaxa != null){
			taxaConta = new Taxa();
			taxaConta.setCdtaxa(_cdtaxa);
		}
		this.valorMovimentacoes = valorMovimentacoes;
		this.valorValecompra = valorValecompra;
		this.valorAntecipado = valoraAntecipado;
		this.documentotipo = documentotipo;
		if(cddocumentoacao != null){
			Documentoacao documentoacao = new Documentoacao(cddocumentoacao);
			this.situacaoabreviada = documentoacao.toSigla();
		}
		this.nomePessoa = nomePessoa;
		this.razaosocialPessoa = razaosocialPessoa;
		this.outrospagamento = outrospagamento;
		this.tipopagamento = tipopagamento;
		this.tipopessoa = tipopessoa;
		this.valorJurosMulta = valorJurosMulta;
	}
	
	
	//get
	public Integer getCdconta() {
		return cdconta;
	}
	public Date getDtVencimento() {
		return dtVencimento;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getNumero() {
		return numero;
	}
	public String getDescricaoPagamento() {
		return descricaoPagamento;
	}
	public String getParcelas() {
		return parcelas;
	}
	public Money getValor() {
		return valor;
	}
	public Money getValorAtual() {
		return valorAtual;
	}
	public Money getTaxa() {
		return taxa;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public Money getValorRateio() {
		return valorRateio;
	}
	public List<Rateioitem> getListaRateioitem() {
		if(listaRateioitem == null){
			listaRateioitem = new ArrayList<Rateioitem>();
		}
		return listaRateioitem;
	}
	public String getCheque() {
		return cheque;
	}

	public Money getValorCorrigido() {
		return valorCorrigido;
	}
	
	public String getSituacao() {
		return situacao;
	}
	
	public String getSituacaoabreviada() {
		return situacaoabreviada;
	}

	public Date getDtEmissao() {
		return dtEmissao;
	}
	
	public Integer getCdindicecorrecao() {
		return cdindicecorrecao;
	}
	
	public String getCentrocustoNome(){
		if(getCentrocusto() != null){
			return getCentrocusto().getNome();
		} else return null;
	}
	public String getDatasPagto() {
		return datasPagto;
	}
	public String getIdentificador() {
		return identificador;
	}
	public List<Centrocusto> getListaCentrocusto() {
		return listaCentrocusto;
	}
	
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	
	public Date getDtPrimeiroVencimento() {
		return dtPrimeiroVencimento;
	}
	
	public Money getValorMovimentacoes() {
		return valorMovimentacoes;
	}	
	
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	public void setValorMovimentacoes(Money valorMovimentacoes) {
		this.valorMovimentacoes = valorMovimentacoes;
	}

	public void setDtPrimeiroVencimento(Date dtPrimeiroVencimento) {
		this.dtPrimeiroVencimento = dtPrimeiroVencimento;
	}
	
	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}

	public void setDatasPagto(String datasPagto) {
		this.datasPagto = datasPagto;
	}
	public void setCdindicecorrecao(Integer cdindicecorrecao) {
		this.cdindicecorrecao = cdindicecorrecao;
	}
	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public void setSituacaoabreviada(String situacaoabreviada) {
		this.situacaoabreviada = situacaoabreviada;
	}
	public void setValorCorrigido(Money valorCorrigido) {
		this.valorCorrigido = valorCorrigido;
	}
	public void setCheque(String cheque) {
		this.cheque = cheque;
	}
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setValorAtual(Money valorAtual) {
		this.valorAtual = valorAtual;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setParcelas(String parcelas) {
		this.parcelas = parcelas;
	}
	public void setDescricaoPagamento(String descricaoPagamento) {
		this.descricaoPagamento = descricaoPagamento;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setValorRateio(Money valorRateio) {
		this.valorRateio = valorRateio;
	}
	public void setPercentualRateio(Double percentualRateio) {
		this.percentualRateio = percentualRateio;
	}
	public void setTaxa(Money taxa) {
		this.taxa = taxa;
	}
	public void setListaRateioitem(List<Rateioitem> listaRateioitem) {
		this.listaRateioitem = listaRateioitem;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setListaCentrocusto(List<Centrocusto> listaCentrocusto) {
		this.listaCentrocusto = listaCentrocusto;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ContaReceberPagarBeanReport) {
			ContaReceberPagarBeanReport bean = (ContaReceberPagarBeanReport) obj;
			return this.getCdconta().equals(bean.getCdconta());
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		return "ID: " + cdconta + " Itens: " + getListaRateioitem().size();
	}

	public String getCodigobarras() {
		return codigobarras;
	}

	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}

	public Money getValorPago() {
		return valorPago;
	}

	public void setValorPago(Money valorPago) {
		this.valorPago = valorPago;
	}

	public Money getValorOriginal() {
		return valorOriginal;
	}

	public void setValorOriginal(Money valorOriginal) {
		this.valorOriginal = valorOriginal;
	}

	public Taxa getTaxaConta() {
		return taxaConta;
	}

	public void setTaxaConta(Taxa taxaConta) {
		this.taxaConta = taxaConta;
	}

	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	public String getClienteIdentificador() {
		return clienteIdentificador;
	}

	public void setClienteIdentificador(String clienteIdentificador) {
		this.clienteIdentificador = clienteIdentificador;
	}

	public Money getValorValecompra() {
		return valorValecompra;
	}

	public void setValorValecompra(Money valorValecompra) {
		this.valorValecompra = valorValecompra;
	}

	public Money getValorrestante() {
		return valorrestante;
	}
	
	public Double getPercentualRateio() {
		return percentualRateio;
	}

	public void setValorrestante(Money valorrestante) {
		this.valorrestante = valorrestante;
	}

	public String getObservacaoItemRateio() {
		return observacaoItemRateio;
	}
	
	public Integer getCdRateioItem() {
		return cdRateioItem;
	}

	public void setObservacaoItemRateio(String observacaoItemRateio) {
		this.observacaoItemRateio = observacaoItemRateio;
	}
	
	public void setCdRateioItem(Integer cdRateioItem) {
		this.cdRateioItem = cdRateioItem;
	}
	
	public String getNumeroNota() {
		return numeroNota;
	}
	
	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}
	
	public String getFormapagamento() {
		return formapagamento;
	}
	
	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}
	
	public String getNomePessoa() {
		return nomePessoa;
	}
	
	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}
	
	public String getRazaosocialPessoa() {
		return razaosocialPessoa;
	}
	
	public void setRazaosocialPessoa(String razaosocialPessoa) {
		this.razaosocialPessoa = razaosocialPessoa;
	}
	
	public String getOutrospagamento() {
		return outrospagamento;
	}
	
	public void setOutrospagamento(String outrospagamento) {
		this.outrospagamento = outrospagamento;
	}
	
	public Integer getTipopessoa() {
		return tipopessoa;
	}
	
	public void setTipopessoa(Integer tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	
	public Integer getTipopagamento() {
		return tipopagamento;
	}
	
	public void setTipopagamento(Integer tipopagamento) {
		this.tipopagamento = tipopagamento;
	}
	
	public Money getValorJurosMulta() {
		return valorJurosMulta;
	}
	
	public Date getDtcompetencia() {
		return dtcompetencia;
	}
	
	public void setValorJurosMulta(Money valorJurosMulta) {
		this.valorJurosMulta = valorJurosMulta;
	}
	
	public void setDtcompetencia(Date dtcompetencia) {
		this.dtcompetencia = dtcompetencia;
	}

	public Money getValorAntecipado() {
		return valorAntecipado;
	}
	
	public void setValorAntecipado(Money valorAntecipado) {
		this.valorAntecipado = valorAntecipado;
	}
}
