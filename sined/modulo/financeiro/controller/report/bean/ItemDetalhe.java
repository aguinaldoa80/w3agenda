package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;

public class ItemDetalhe {

	protected Colaborador colaborador;
	protected Fornecedor fornecedor;
	protected Projeto projeto;
	protected Centrocusto centrocusto;
	protected Empresa empresa;
	protected Contagerencial contagerencial;
	protected Projetotipo projetotipo;
	protected Categoria categoria;
	
	protected Conta conta;
	protected Cliente cliente;
	
	protected String outrospagamento;
	protected Tipopagamento tipopagamento;
	
	protected Boolean recebidode;
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Centro de custo")
	@Required
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Conta gerencial")
	@Required
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Tipo de projeto")
	@Required
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	@Required
	public Conta getConta() {
		return conta;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Outros")
	public String getOutrospagamento() {
		return outrospagamento;
	}
	@DisplayName("Tipo de pagamento")
	@Required
	public Tipopagamento getTipopagamento() {
		return tipopagamento;
	}
	public Boolean getRecebidode() {
		return recebidode;
	}
	public void setRecebidode(Boolean recebidode) {
		this.recebidode = recebidode;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setOutrospagamento(String outrospagamento) {
		this.outrospagamento = outrospagamento;
	}
	public void setTipopagamento(Tipopagamento tipopagamento) {
		this.tipopagamento = tipopagamento;
	}
}
