package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.SinedUtil;

public class AnaliseRecDespBean{
	
	private Integer cdcontagerencial;
	private String identificador;
	private Boolean ativo;
	private String nome;
	private String operacao;
	private Double percentual;
	private Money valor;
	private Double valorDouble;
	private List<AnaliseRecDespBean> listaFilha;

	private Integer mes;
	private Integer ano;
	private Integer rowPai;
	
	private Integer cdcentrocusto;
	private String nomecentrocusto;
	private Boolean itemIncluidoControleOrcamento;
	private Integer codigoPai;
	
	private Integer codigo;
	private Boolean aberto = Boolean.FALSE;
	private Boolean grafico = Boolean.TRUE;
	private Boolean retirarGrafico = Boolean.FALSE;
	private Boolean haveFilhos = Boolean.FALSE;
	
	public Integer getCodigoPai() {
		return codigoPai;
	}

	public void setCodigoPai(Integer codigoPai) {
		this.codigoPai = codigoPai;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Boolean getAberto() {
		return aberto;
	}

	public void setAberto(Boolean aberto) {
		this.aberto = aberto;
	}

	public Boolean getGrafico() {
		return grafico;
	}

	public void setGrafico(Boolean grafico) {
		this.grafico = grafico;
	}

	public Boolean getRetirarGrafico() {
		return retirarGrafico;
	}

	public void setRetirarGrafico(Boolean retirarGrafico) {
		this.retirarGrafico = retirarGrafico;
	}

	public Boolean getHaveFilhos() {
		return SinedUtil.isListNotEmpty(this.getListaFilha());
	}

	public void setHaveFilhos(Boolean haveFilhos) {
		this.haveFilhos = haveFilhos;
	}

	
	public AnaliseRecDespBean() {}
		
	public AnaliseRecDespBean(Integer codigo, String identificador,
			String nome, Double valorDouble, Integer codigoPai, Boolean aberto,
			Boolean grafico, Boolean haveFilhos, String operacao, Double percentual) {
		super();
		this.codigo = codigo;
		this.identificador = identificador;
		this.nome = nome;
		this.valorDouble = valorDouble;
		this.codigoPai = codigoPai;
		this.haveFilhos = haveFilhos;
		this.operacao = operacao;
		this.percentual = percentual;
		if(codigoPai!=null){
			this.grafico = false;
			this.aberto = false;
			this.retirarGrafico = false;
		}else{
			this.retirarGrafico = true;
			this.grafico = true;
			this.aberto = true;			
		}
	}
	
	public AnaliseRecDespBean(String identificador) {
		this.identificador = identificador;
	}

	public AnaliseRecDespBean(String identificador, String nome, String operacao, Long valor) {
		this.identificador = identificador;
		this.nome = nome;
		this.operacao = operacao;
		this.percentual = null;
		this.valor = new Money(valor, true);
		this.valorDouble = this.valor.getValue().doubleValue();
	}
	
	public AnaliseRecDespBean(String identificador, String nome, String operacao, Integer codigoPai, Integer codigo, Long valor) {
		this.identificador = identificador;
		this.nome = nome;
		this.operacao = operacao;
		this.percentual = null;
		this.valor = new Money(valor, true);
		this.valorDouble = this.valor.getValue().doubleValue();
		this.codigoPai = codigoPai;
		this.codigo = codigo;
	}

	public AnaliseRecDespBean(String identificador, String nome, String operacao, Long valor, Integer mes, Integer ano) {
		this.identificador = identificador;
		this.nome = nome;
		this.percentual = null;
		this.valor = new Money(valor, true);
		this.valorDouble = this.valor.getValue().doubleValue();
		this.mes = mes>0 ? mes : null;
		this.ano = ano>0 ? ano : null;
		this.operacao = operacao;
	}

	public AnaliseRecDespBean(String identificador, String nome, String operacao) {
		this.identificador = identificador;
		this.nome = nome;
		this.operacao = operacao;
		this.percentual = null;
		this.valor = new Money();
		this.valorDouble = 0d;
	}
	
	public AnaliseRecDespBean(String identificador, String nome, Integer codigo, Integer codigoPai, String operacao, Boolean ativo) {
		this.identificador = identificador;
		this.nome = nome;
		this.operacao = operacao;
		this.percentual = null;
		this.valor = new Money();
		this.valorDouble = 0d;
		this.ativo = ativo;
		this.codigoPai = codigoPai;
		this.codigo = codigo;
		if(codigoPai!=null){
			this.grafico = false;
			this.aberto = false;
			this.retirarGrafico = false;
		}else{
			this.retirarGrafico = true;
			this.grafico = true;
			this.aberto = true;			
		}
	}
	
	public AnaliseRecDespBean(Integer cdcontagerencial, String identificador, String nome, String operacao, Boolean ativo) {
		this.identificador = identificador;
		this.nome = nome;
		this.operacao = operacao;
		this.percentual = null;
		this.valor = new Money();
		this.valorDouble = 0d;
		this.ativo = ativo;
	}
	
	public AnaliseRecDespBean(Integer cdcentrocusto, String nomecentrocusto, Integer cdcontagerencial, String identificador, String nome, String operacao, Long valor, Integer mes, Integer ano) {
		this.cdcentrocusto = cdcentrocusto;
		this.nomecentrocusto = nomecentrocusto;
		
		this.cdcontagerencial = cdcontagerencial; 
		this.identificador = identificador;
		this.nome = nome;
		this.percentual = null;
		this.valor = new Money(valor, true);
		this.valorDouble = this.valor.getValue().doubleValue();
		this.mes = mes;
		this.ano = ano;
		this.operacao = operacao;
	}

	public String getIdentificador() {
		return identificador;
	}
	public String getNome() {
		return nome;
	}
	public String getOperacao() {
		return operacao;
	}
	public Double getPercentual() {
		return percentual;
	}
	public Money getValor() {
		return valor;
	}
	public Double getValorDouble() {
		return valorDouble;
	}
	public Integer getAno() {
		return ano;
	}
	public Integer getMes() {
		return mes;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public List<AnaliseRecDespBean> getListaFilha() {
		return listaFilha;
	}
	public void setListaFilha(List<AnaliseRecDespBean> listaFilha) {
		this.listaFilha = listaFilha;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public void setValor(Money valor) {
		this.valor = valor;
		this.valorDouble = this.valor.getValue().doubleValue();
	}
	public void setValorDouble(Double valorDouble) {
		this.valorDouble = valorDouble;
	}
	
	public Integer getRowPai() {
		return rowPai;
	}
	public void setRowPai(Integer rowPai) {
		this.rowPai = rowPai;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identificador == null) ? 0 : identificador.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnaliseRecDespBean other = (AnaliseRecDespBean) obj;
		if (identificador == null) {
			if (other.identificador != null)
				return false;
		} else if (!identificador.equals(other.identificador))
			return false;
		return true;
	}

	/**
	 * Comparator usado para pesquisa bin�ria na lista de contas gerenciais.
	 * Utiliza como chave o atrinuto 'identificador' dos objetos da lista.
	 * Para encontrar o objeto pai, remove-se do filho os 3 �ltimos caracteres (da direita)
	 * da chave. O que restar do n�mero � o identificador do objeto pai. 
	 */
	public static final Comparator<AnaliseRecDespBean> PAI_COMPARATOR = new Comparator<AnaliseRecDespBean>(){
		public int compare(AnaliseRecDespBean o1, AnaliseRecDespBean o2) {
			String identificadorO1 = StringUtils.deleteWhitespace(o1.getIdentificador());
			String identificadorO2 = StringUtils.deleteWhitespace(o2.getIdentificador());

			if (identificadorO2.split("\\.").length > 1) {
				identificadorO2 = identificadorO2.substring( 0, (identificadorO2.length() - (identificadorO2.split("\\.")[identificadorO2.split("\\.").length-1].length() + 1)) );
			} else {
				identificadorO2 = "";
			}
			return identificadorO1.compareTo(identificadorO2);
		}
	};

	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	public String getNomecentrocusto() {
		return nomecentrocusto;
	}

	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	public void setNomecentrocusto(String nomecentrocusto) {
		this.nomecentrocusto = nomecentrocusto;
	}

	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}

	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getItemIncluidoControleOrcamento() {
		return itemIncluidoControleOrcamento;
	}

	public void setItemIncluidoControleOrcamento(Boolean itemIncluidoControleOrcamento) {
		this.itemIncluidoControleOrcamento = itemIncluidoControleOrcamento;
	}
}
