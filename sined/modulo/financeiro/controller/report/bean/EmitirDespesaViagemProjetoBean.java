package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class EmitirDespesaViagemProjetoBean {

	private String projeto;
	private Money percentual;
	
	public String getProjeto() {
		return projeto;
	}
	public Money getPercentual() {
		return percentual;
	}
	
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	public void setPercentual(Money percentual) {
		this.percentual = percentual;
	}
}
