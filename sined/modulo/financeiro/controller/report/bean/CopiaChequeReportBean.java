package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.awt.Image;
import java.util.List;



public class CopiaChequeReportBean{

	protected String empresa = "";
	protected String bancosacado = "";
	protected String numerocheque = "";
	protected String autorizado = "";
	protected String assinadopor = "";
	protected String datacheque = "";
	protected String fornecedor = "";
	protected String dataatual = "";
	protected String cp = "";
	protected String tipodocumento = "";
	protected String numerodocumento = "";
	protected String dataemissao = "";
	protected String datavenc = "";
	protected String valordocumento = "";
	protected String descricaodocumento = "";
	protected String agenciaconta = "";
	protected String solicitante = "";
	protected String digitadopor = "";
	protected String observacao = "";
	protected String itensrateio1 = "";
	protected String itensrateio2 = "";
	protected String itensrateio3 = "";
	protected List<CopiaChequeSubReportBean> listaItens;
	protected Image logo;
	
	
	public String getBancosacado() {
		return bancosacado;
	}
	public String getNumerocheque() {
		return numerocheque;
	}
	public String getAutorizado() {
		return autorizado;
	}
	public String getAssinadopor() {
		return assinadopor;
	}
	public String getDatacheque() {
		return datacheque;
	}
	public String getFornecedor() {
		return fornecedor;
	}
	public String getDataatual() {
		return dataatual;
	}
	public String getCp() {
		return cp;
	}
	public String getTipodocumento() {
		return tipodocumento;
	}
	public String getNumerodocumento() {
		return numerodocumento;
	}
	public String getDataemissao() {
		return dataemissao;
	}
	public String getDatavenc() {
		return datavenc;
	}
	public String getValordocumento() {
		return valordocumento;
	}
	public String getDescricaodocumento() {
		return descricaodocumento;
	}
	public String getAgenciaconta() {
		return agenciaconta;
	}
	public String getSolicitante() {
		return solicitante;
	}
	public String getDigitadopor() {
		return digitadopor;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getItensrateio1() {
		return itensrateio1;
	}
	public String getItensrateio2() {
		return itensrateio2;
	}
	public String getItensrateio3() {
		return itensrateio3;
	}
	public String getEmpresa() {
		return empresa;
	}
	public Image getLogo() {
		return logo;
	}
	public void setLogo(Image logo) {
		this.logo = logo;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setItensrateio1(String itensrateio1) {
		this.itensrateio1 = itensrateio1;
	}
	public void setItensrateio2(String itensrateio2) {
		this.itensrateio2 = itensrateio2;
	}
	public void setItensrateio3(String itensrateio3) {
		this.itensrateio3 = itensrateio3;
	}
	public List<CopiaChequeSubReportBean> getListaItens() {
		return listaItens;
	}
	public void setListaItens(List<CopiaChequeSubReportBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setBancosacado(String bancosacado) {
		this.bancosacado = bancosacado;
	}
	public void setNumerocheque(String numerocheque) {
		this.numerocheque = numerocheque;
	}
	public void setAutorizado(String autorizado) {
		this.autorizado = autorizado;
	}
	public void setAssinadopor(String assinadopor) {
		this.assinadopor = assinadopor;
	}
	public void setDatacheque(String datacheque) {
		this.datacheque = datacheque;
	}
	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setDataatual(String dataatual) {
		this.dataatual = dataatual;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}
	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}
	public void setDataemissao(String dataemissao) {
		this.dataemissao = dataemissao;
	}
	public void setDatavenc(String datavenc) {
		this.datavenc = datavenc;
	}
	public void setValordocumento(String valordocumento) {
		this.valordocumento = valordocumento;
	}
	public void setDescricaodocumento(String descricaodocumento) {
		this.descricaodocumento = descricaodocumento;
	}
	public void setAgenciaconta(String agenciaconta) {
		this.agenciaconta = agenciaconta;
	}
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}
	public void setDigitadopor(String digitadopor) {
		this.digitadopor = digitadopor;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
