package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.util.SinedUtil;

public class SPEDFiscalApoio {
	
	private static final DecimalFormat FORMAT = new DecimalFormat("000000");
	
	private int contadorParticipante;
	private int contadorInfoComplementar;
	private int contadorObsLancamentoFiscal;
	private int contadorUnidade;
	private int contadorNatureza;	
	
	private Map<MultiKey, String> mapParticipante;
	private Map<String, String> mapInfoComplementar;
	private Map<String, String> mapObsLancamentoFiscal;
	private Map<String, Map<String, String>> mapProdutoServico;
	private Map<Integer, String> mapUnidade;
	private Map<String, String> mapNatureza;
	private Map<String, Integer> mapTotalizadorRegistros;
	private Map<String, String> mapDescricaoIten;
	private Map<String, Map<String, String>> mapMaterialProducao;
	private Map<Integer, ContaContabil> mapNaturezaContacontabil;
	private Map<String, ContaContabil> mapEmpresaMaterialContaContabil;
	
	private List<Integer> listaIdEntrega;
	private List<Integer> listaIdNota;
	
	public SPEDFiscalApoio() {
		mapParticipante = new HashMap<MultiKey, String>();
		mapInfoComplementar = new HashMap<String, String>();
		mapObsLancamentoFiscal = new HashMap<String, String>();
		mapProdutoServico = new HashMap<String, Map<String, String>>();
		mapMaterialProducao = new HashMap<String, Map<String, String>>();
		mapUnidade = new HashMap<Integer, String>();
		mapNatureza = new HashMap<String, String>();
		mapDescricaoIten = new HashMap<String, String>();
		mapTotalizadorRegistros = new HashMap<String, Integer>();
		mapNaturezaContacontabil = new HashMap<Integer, ContaContabil>();
		mapEmpresaMaterialContaContabil = new HashMap<String, ContaContabil>();
		
		listaIdEntrega = new ArrayList<Integer>();
		listaIdNota = new ArrayList<Integer>();
		
		contadorParticipante = 0;
		contadorInfoComplementar = 0;
		contadorObsLancamentoFiscal= 0;
		contadorUnidade = 0;
		contadorNatureza = 0;
	}
	
	public Map<String, String> getMapNatureza() {
		return mapNatureza;
	}
	
	public Map<Integer, String> getMapUnidade() {
		return mapUnidade;
	}
	
	public Map<String, String> getMapInfoComplementar() {
		return mapInfoComplementar;
	}
	
	public Map<String, String> getMapObsLancamentoFiscal() {
		return mapObsLancamentoFiscal;
	}
	
	public Map<MultiKey, String> getMapParticipante() {
		return mapParticipante;
	}
	
	public Map<String, Map<String, String>> getMapProdutoServico() {
		return mapProdutoServico;
	}
	
	public Map<String, String> getDescricaoIten() {
		return mapDescricaoIten;
	}
	
	public Map<String, Integer> getMapTotalizadorRegistros() {
		return mapTotalizadorRegistros;
	}
	
	public List<Integer> getListaIdEntrega() {
		return listaIdEntrega;
	}
	
	public List<Integer> getListaIdNota() {
		return listaIdNota;
	}
	
	public Map<String, Map<String, String>> getMapMaterialProducao() {
		return mapMaterialProducao;
	}

	public String getCodNaturezaOperacao(String naturezaOperacao) {
		String formatado = null;
		
		if (naturezaOperacao != null && mapNatureza.containsKey(naturezaOperacao)) {
			return mapNatureza.get(naturezaOperacao);
		}
		
		return formatado;
	}
	
	public String addNatureza(String info){
		if(info == null || info.trim().equals("")){
			return null;
		}
		
		if(mapNatureza.containsKey(info)){
			return mapNatureza.get(info);			
		} else {
			contadorNatureza++;
			String formatado = FORMAT.format(contadorNatureza);
			mapNatureza.put(info, formatado);
			return formatado;
		}
	}
	
	public String addUnidade(Integer codigo){
		if(mapUnidade.containsKey(codigo)){
			return mapUnidade.get(codigo);			
		} else {
			contadorUnidade++;
			String formatado = FORMAT.format(contadorUnidade);
			mapUnidade.put(codigo, formatado);
			return formatado;
		}
	}
	
	public String addProdutoServico(Integer codigo, String identificadorespecifico){
		return addProdutoServico(codigo.toString(), identificadorespecifico);
	}
	
	public String addProdutoServico(String codigo, String identificadorespecifico){
		if(SinedUtil.isSpedMapaProduto()){
			identificadorespecifico = null;
		}
		if(mapProdutoServico.containsKey(codigo)){
			Map<String, String> mapCodigoIdentificador = mapProdutoServico.get(codigo);
			if(StringUtils.isNotBlank(identificadorespecifico)){
				if(mapCodigoIdentificador.containsKey(identificadorespecifico)){
					return mapCodigoIdentificador.get(identificadorespecifico);
				}else {
					String formatado = identificadorespecifico; //SinedUtil.formataTamanho(identificadorespecifico, 10, '0', false);
					mapCodigoIdentificador.put(identificadorespecifico, formatado);
					
					return formatado;
				}
			}else {
				if(mapCodigoIdentificador.containsKey(codigo)){
					return mapCodigoIdentificador.get(codigo);
				}else {
					String formatado = codigo; //SinedUtil.formataTamanho(codigo, 10, '0', false);
					mapCodigoIdentificador.put(codigo, formatado);
					
					return formatado;
				}
			}		
		} else {
			String formatado = null;
			Map<String, String> mapCodigoIdentificador = new HashMap<String, String>();
			if(StringUtils.isNotBlank(identificadorespecifico)){
				formatado = identificadorespecifico; //SinedUtil.formataTamanho(identificadorespecifico, 10, '0', false);
				mapCodigoIdentificador.put(identificadorespecifico, formatado);
			}else {
				formatado = codigo; //SinedUtil.formataTamanho(codigo, 10, '0', 0false);
				mapCodigoIdentificador.put(codigo, formatado);
			}
			
			mapProdutoServico.put(codigo, mapCodigoIdentificador);
			return formatado;
		}
	}
	
	public String addParticipante(Integer cdpessoa){
		return addParticipante(cdpessoa, null);
	}
	
	public String addParticipante(Integer cdpessoa, Integer cdendereco){
		MultiKey key = new MultiKey(cdpessoa, cdendereco);
		if(mapParticipante.containsKey(key)){
			return mapParticipante.get(key);			
		} else {
			contadorParticipante++;
			String formatado = FORMAT.format(contadorParticipante);
			mapParticipante.put(key, formatado);
			return formatado;
		}
	}
	
	public String addInfoComplementar(String info){
		if(mapInfoComplementar.containsKey(info)){
			return mapInfoComplementar.get(info);			
		} else {
			contadorInfoComplementar++;
			String formatado = FORMAT.format(contadorInfoComplementar);
			mapInfoComplementar.put(info, formatado);
			return formatado;
		}
	}
	
	public String addObsLancamentoFiscal(String info){
		if(mapObsLancamentoFiscal.containsKey(info)){
			return mapObsLancamentoFiscal.get(info);			
		} else {
			contadorObsLancamentoFiscal++;
			String formatado = FORMAT.format(contadorObsLancamentoFiscal);
			mapObsLancamentoFiscal.put(info, formatado);
			return formatado;
		}
	}
	
	public void addRegistros(String reg){
		if(mapTotalizadorRegistros.containsKey(reg)){
			Integer soma = mapTotalizadorRegistros.get(reg);
			soma++;
			mapTotalizadorRegistros.remove(reg);			
			mapTotalizadorRegistros.put(reg, soma);
		} else {
			mapTotalizadorRegistros.put(reg, 1);
		}
	}
	
	public void removeRegistro(String reg){
		if(mapTotalizadorRegistros.containsKey(reg)){
			Integer soma = mapTotalizadorRegistros.get(reg);
			soma--;
			mapTotalizadorRegistros.remove(reg);			
			mapTotalizadorRegistros.put(reg, soma);
		}
	}
	
	public void addEntrega(Integer id){
		listaIdEntrega.add(id);
	}
	
	public void addNota(Integer id){
		listaIdNota.add(id);
	}
	
	public String addMaterialProducao(Integer codigo, String identificadorespecifico){
		return addMaterialProducao(codigo.toString(), identificadorespecifico);
	}
	
	public String addMaterialProducao(String codigo, String identificadorespecifico){
		if(mapProdutoServico.containsKey(codigo)){
			Map<String, String> mapCodigoIdentificador = mapProdutoServico.get(codigo);
			if(StringUtils.isNotBlank(identificadorespecifico)){
				if(mapCodigoIdentificador.containsKey(identificadorespecifico)){
					return mapCodigoIdentificador.get(identificadorespecifico);
				}else {
					String formatado = identificadorespecifico; //SinedUtil.formataTamanho(identificadorespecifico, 6, '0', false);
					mapCodigoIdentificador.put(identificadorespecifico, formatado);
					
					return formatado;
				}
			}else {
				if(mapCodigoIdentificador.containsKey(codigo)){
					return mapCodigoIdentificador.get(codigo);
				}else {
					String formatado = codigo; //SinedUtil.formataTamanho(codigo, 6, '0', false);
					mapCodigoIdentificador.put(codigo, formatado);
					
					return formatado;
				}
			}		
		} else {
			String formatado = null;
			Map<String, String> mapCodigoIdentificador = new HashMap<String, String>();
			if(StringUtils.isNotBlank(identificadorespecifico)){
				formatado = identificadorespecifico;//SinedUtil.formataTamanho(codigo, 6, '0', false);
				mapCodigoIdentificador.put(identificadorespecifico, formatado);
			}else {
				formatado = codigo;//SinedUtil.formataTamanho(codigo, 6, '0', false);
				mapCodigoIdentificador.put(codigo, formatado);
			}
			
			mapProdutoServico.put(codigo, mapCodigoIdentificador);
			return formatado;
		}
	}
	
	public Map<String, ContaContabil> getMapEmpresaMaterialContaContabil() {
		return mapEmpresaMaterialContaContabil;
	}
	
	public Map<Integer, ContaContabil> getMapNaturezaContacontabil() {
		return mapNaturezaContacontabil;
	}
	
}
