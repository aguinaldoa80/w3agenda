package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.util.SinedDateUtils;

public class FluxocaixaMensalReportBean {

	protected Integer cdcontagerencial;
	protected String identificador;
	protected String nome;
	protected Tipooperacao tipooperacao;
	protected Integer nivel;
	protected Money valoresAnteriores = new Money();
	protected List<FluxocaixaMensalSubReportBean> listaMensal;
	protected List<FluxocaixaMensalSubReportBean> listaTotalMensal;
	private Integer cdcentrocusto;
	private Boolean possuiLancamento;
	private Boolean mostrarValorAnterior;
	private Boolean ultimoNivel;
	
	public FluxocaixaMensalReportBean(Contagerencial contagerencial, Date inicio, Date fim) {
		this.cdcontagerencial = contagerencial.getCdcontagerencial();
		this.identificador = contagerencial.getVcontagerencial().getIdentificador();
		this.nome = contagerencial.getNome();
		this.tipooperacao = contagerencial.getTipooperacao();
		this.nivel = contagerencial.getVcontagerencial().getNivel();		
		this.listaMensal = new ArrayList<FluxocaixaMensalSubReportBean>();
		this.listaTotalMensal = new ArrayList<FluxocaixaMensalSubReportBean>();
		this.possuiLancamento = Boolean.FALSE;
		
		Integer meses = SinedDateUtils.mesesEntreInicioFim(inicio, fim) + 1;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(inicio.getTime());
		
		for (int i = 1; i <= meses; i++){
			this.listaMensal.add(new FluxocaixaMensalSubReportBean(cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR)));
			this.listaTotalMensal.add(new FluxocaixaMensalSubReportBean(cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR)));
			cal.add(Calendar.MONTH, 1);
		}
	}
	
	public FluxocaixaMensalReportBean(String nome, Date inicio, Date fim) {
		this.identificador = nome;
		this.listaMensal = new ArrayList<FluxocaixaMensalSubReportBean>();
		this.listaTotalMensal = new ArrayList<FluxocaixaMensalSubReportBean>();
		
		Integer meses = SinedDateUtils.mesesEntreInicioFim(inicio, fim) + 1;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(inicio.getTime());
		
		for (int i = 1; i <= meses; i++){
			this.listaMensal.add(new FluxocaixaMensalSubReportBean(cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR)));
			this.listaTotalMensal.add(new FluxocaixaMensalSubReportBean(cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR)));
			cal.add(Calendar.MONTH, 1);
		}
	}

	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}

	public String getIdentificador() {
		return identificador;
	}

	public String getNome() {
		return nome;
	}

	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}

	public Integer getNivel() {
		return nivel;
	}

	public List<FluxocaixaMensalSubReportBean> getListaMensal() {
		return listaMensal;
	}
	
	public List<FluxocaixaMensalSubReportBean> getListaTotalMensal() {
		return listaTotalMensal;
	}

	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public void setListaMensal(List<FluxocaixaMensalSubReportBean> listaMensal) {
		this.listaMensal = listaMensal;
	}
	
	public void setListaTotalMensal(List<FluxocaixaMensalSubReportBean> listaTotalMensal) {
		this.listaTotalMensal = listaTotalMensal;
	}
	
	public Money getValoresAnteriores() {
		return valoresAnteriores;
	}

	public void setValoresAnteriores(Money valoresAnteriores) {
		this.valoresAnteriores = valoresAnteriores;
	}

	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}

	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}

	public Boolean getPossuiLancamento() {
		return possuiLancamento;
	}
	public void setPossuiLancamento(Boolean possuiLancamento) {
		this.possuiLancamento = possuiLancamento;
	}

	public boolean isFilho(){
		return StringUtils.isNotEmpty(getIdentificador()) && getIdentificador().split(",").length > 1;
	}
	
	public String getIdentificadorPai(int nivel){
		if(StringUtils.isNotEmpty(getIdentificador())){
			String[] arrayIdentificador = getIdentificador().split("\\.");
			if(arrayIdentificador.length > 0 && nivel < arrayIdentificador.length ){
				StringBuilder identificadorPai = new StringBuilder();
				for(int i = 0; i < nivel; i++){
					if(!identificadorPai.toString().equals("")) identificadorPai.append(".");
					identificadorPai.append(arrayIdentificador[i]);
				}
				return identificadorPai.toString();
			}
		}
		return getIdentificador(); 
	}

	public Boolean getMostrarValorAnterior() {
		return mostrarValorAnterior;
	}

	public void setMostrarValorAnterior(Boolean mostrarValorAnterior) {
		this.mostrarValorAnterior = mostrarValorAnterior;
	}

	public Boolean getUltimoNivel() {
		return ultimoNivel;
	}

	public void setUltimoNivel(Boolean ultimoNivel) {
		this.ultimoNivel = ultimoNivel;
	}
}
