package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.util.SinedUtil;

public class SPEDPiscofinsApoio {
	
	private static final DecimalFormat FORMAT = new DecimalFormat("000000");
	
	private int contadorParticipante;
	private int contadorInfoComplementar;
	private long contadorProdutoServico;
	private int contadorUnidade;
	private int contadorNatureza;	
	
	private Map<String, Map<MultiKey, String>> mapParticipante;
	private Map<String, String> mapParticipanteCpfCnpj;
	private Map<String, Map<String, String>> mapInfoComplementar;
	private Map<String, Map<String, Map<String, String>>> mapProdutoServico;
	private Map<String, Map<Integer, String>> mapUnidade;
	private Map<String, Map<String, String>> mapNatureza;
	private Map<String, Integer> mapTotalizadorRegistros;
	private Map<String, Map<String, String>> mapDescricaoIten;
	private Map<Integer, ContaContabil> mapNaturezaContacontabil;
	private Map<String, ContaContabil> mapEmpresaMaterialContaContabil;
	
	private List<Integer> listaIdEntrega;
	private List<Integer> listaIdDocumento;
	private List<Integer> listaIdNota;
	private Set<String> listaIdentificador;
	
	public SPEDPiscofinsApoio(Long contadorProdutoServico) {
		mapParticipante =  new HashMap<String, Map<MultiKey, String>>();
		mapParticipanteCpfCnpj =  new HashMap<String, String>();
		mapInfoComplementar = new HashMap<String, Map<String, String>>();
		mapProdutoServico = new HashMap<String, Map<String, Map<String, String>>>();
		mapUnidade = new HashMap<String, Map<Integer, String>>();
		mapNatureza = new HashMap<String, Map<String, String>>();
		mapDescricaoIten = new HashMap<String, Map<String, String>>();
		mapTotalizadorRegistros = new HashMap<String, Integer>();
		mapNaturezaContacontabil = new HashMap<Integer, ContaContabil>();
		mapEmpresaMaterialContaContabil = new HashMap<String, ContaContabil>();
		
		listaIdEntrega = new ArrayList<Integer>();
		listaIdDocumento = new ArrayList<Integer>();
		listaIdNota = new ArrayList<Integer>();
		listaIdentificador = new HashSet<String>();
		
		contadorParticipante = 0;
		contadorInfoComplementar = 0;
		this.contadorProdutoServico = contadorProdutoServico != null ? contadorProdutoServico : 0;
		contadorUnidade = 0;
		contadorNatureza = 0;
	}
	
	public Map<String, Map<String, String>> getMapNatureza() {
		return mapNatureza;
	}
	
	public Map<String, Map<Integer, String>> getMapUnidade() {
		return mapUnidade;
	}
	
	public Map<String, Map<String, String>> getMapInfoComplementar() {
		return mapInfoComplementar;
	}
	
	public Map<String, Map<MultiKey, String>> getMapParticipante() {
		return mapParticipante;
	}
	
	public Map<String, String> getMapParticipanteCpfCnpj() {
		return mapParticipanteCpfCnpj;
	}

	public Map<String, Map<String, Map<String, String>>> getMapProdutoServico() {
		return mapProdutoServico;
	}
	
	public Map<String, Map<String, String>> getDescricaoIten() {
		return mapDescricaoIten;
	}
	
	public Map<String, Integer> getMapTotalizadorRegistros() {
		return mapTotalizadorRegistros;
	}
	
	public List<Integer> getListaIdDocumento() {
		return listaIdDocumento;
	}
	
	public List<Integer> getListaIdEntrega() {
		return listaIdEntrega;
	}
	
	public List<Integer> getListaIdNota() {
		return listaIdNota;
	}
	
	public Set<String> getListaIdentificador() {
		return listaIdentificador;
	}
	
	public String addNatureza(String info, String cnpj){
		if(info == null || info.trim().equals("")){
			return null;
		}
		
		if(mapNatureza.containsKey(cnpj)){
			Map<String, String> map = mapNatureza.get(cnpj);
			if(map.containsKey(info)){
				return map.get(info);			
			} else {
				contadorNatureza++;
				String formatado = FORMAT.format(contadorNatureza);
				map.put(info, formatado);
				mapNatureza.put(cnpj, map);
				return formatado;
			}
		}else {
			contadorNatureza++;
			String formatado = FORMAT.format(contadorNatureza);
			mapNatureza.put(cnpj, new HashMap<String, String>());
			mapNatureza.get(cnpj).put(info, formatado);
			return formatado;
		}
	}
	
	public String addUnidade(Integer codigo, String cnpj){
		if(mapUnidade.containsKey(cnpj)){
			Map<Integer, String> map = mapUnidade.get(cnpj);
			if(map.containsKey(codigo)){
				return map.get(codigo);			
			} else {
				contadorUnidade++;
				String formatado = FORMAT.format(contadorUnidade);
				map.put(codigo, formatado);
				mapUnidade.put(cnpj, map);
				return formatado;
			}
		}else {
			contadorUnidade++;
			String formatado = FORMAT.format(contadorUnidade);
			mapUnidade.put(cnpj, new HashMap<Integer, String>());
			mapUnidade.get(cnpj).put(codigo, formatado);
			return formatado;
		}
	}
	
	public String addProdutoServico(Integer codigo, String identificadorespecifico, String cnpj){
		return addProdutoServico(codigo.toString(), identificadorespecifico, cnpj);
	}
	
	public String addProdutoServico(String codigo, String identificadorespecifico, String cnpj){
		if(SinedUtil.isSpedMapaProduto()){
			identificadorespecifico = null;
		}
		if(mapProdutoServico.containsKey(cnpj)){
			return addCodigoIdentificador(mapProdutoServico.get(cnpj), codigo, identificadorespecifico);
		}else {
			mapProdutoServico.put(cnpj, new HashMap<String, Map<String, String>>());
			mapProdutoServico.get(cnpj).put(codigo, new HashMap<String, String>());
			return addCodigoIdentificador(mapProdutoServico.get(cnpj), codigo, identificadorespecifico);
		}
	}
	
	private String addCodigoIdentificador(Map<String, Map<String, String>> map, String codigo, String identificadorespecifico){
		if(map.containsKey(codigo)){
			Map<String, String> mapCodigoIdentificador = map.get(codigo);
			if(StringUtils.isNotBlank(identificadorespecifico)){
				if(mapCodigoIdentificador.containsKey(identificadorespecifico)){
					return mapCodigoIdentificador.get(identificadorespecifico);
				}else {
					String formatado = identificadorespecifico; //SinedUtil.formataTamanho(identificadorespecifico, 10, '0', false);
					mapCodigoIdentificador.put(identificadorespecifico, formatado);
					
					map.put(codigo, mapCodigoIdentificador);
					return formatado;
				}
			}else {
				if(mapCodigoIdentificador.containsKey(codigo)){
					return mapCodigoIdentificador.get(codigo);
				}else {
					String formatado = codigo; //SinedUtil.formataTamanho(codigo, 10, '0', false);
					mapCodigoIdentificador.put(codigo, formatado);
					
					map.put(codigo, mapCodigoIdentificador);
					return formatado;
				}
			}
		}else {
			String formatado = null;
			Map<String, String> mapCodigoIdentificador = new HashMap<String, String>();
			if(StringUtils.isNotBlank(identificadorespecifico)){
				formatado = identificadorespecifico; //SinedUtil.formataTamanho(identificadorespecifico, 10, '0', false);
				mapCodigoIdentificador.put(identificadorespecifico, formatado);
			}else {
				formatado = codigo; //SinedUtil.formataTamanho(codigo, 10, '0', false);
				mapCodigoIdentificador.put(codigo, formatado);
			}
			map.put(codigo, mapCodigoIdentificador);
			return formatado;
		}
	}
	
	public String addParticipante(Integer codigo, String cnpj){
		return addParticipante(codigo, cnpj, null, null);
	}
	
	public String addParticipante(Integer codigo, String cnpj, Integer cdendereco, String cpfOuCnpj){
		MultiKey key = new MultiKey(codigo, cdendereco);
		if(mapParticipante.containsKey(cnpj)){
			Map<MultiKey, String> map = mapParticipante.get(cnpj);
			if(map.containsKey(key)){
				return map.get(key);			
			} else {
				contadorParticipante++;
				String formatado = FORMAT.format(contadorParticipante);
				map.put(key, formatado);
				mapParticipante.put(cnpj, map);
				mapParticipanteCpfCnpj.put(formatado, cpfOuCnpj);
				return formatado;
			}
		}else {
			contadorParticipante++;
			String formatado = FORMAT.format(contadorParticipante);
			mapParticipante.put(cnpj, new HashMap<MultiKey, String>());
			mapParticipante.get(cnpj).put(key, formatado);
			mapParticipanteCpfCnpj.put(formatado, cpfOuCnpj);
			return formatado;
		}
	}
	
	public String addInfoComplementar(String info, String cnpj){
		if(mapInfoComplementar.containsKey(cnpj)){
			Map<String, String> map = mapInfoComplementar.get(cnpj);
			if(map.containsKey(info)){
				return map.get(info);			
			} else {
				contadorInfoComplementar++;
				String formatado = FORMAT.format(contadorInfoComplementar);
				map.put(info, formatado);
				mapInfoComplementar.put(cnpj, map);
				return formatado;
			}
		}else {
			contadorInfoComplementar++;
			String formatado = FORMAT.format(contadorInfoComplementar);
			mapInfoComplementar.put(cnpj, new HashMap<String, String>());
			mapInfoComplementar.get(cnpj).put(info, formatado);
			return formatado;
		}
	}
	
	public String addDescricaoIten(String info, String cnpj){
		if(mapDescricaoIten.containsKey(cnpj)){
			Map<String, String> map = mapDescricaoIten.get(cnpj);
			if(map.containsKey(info)){
				return map.get(info);			
			} else {
				contadorProdutoServico++;
				String formatado = FORMAT.format(contadorProdutoServico);
				map.put(info, formatado);
				mapDescricaoIten.put(cnpj, map);
				return formatado;
			}
		}else {
			contadorProdutoServico++;
			String formatado = FORMAT.format(contadorProdutoServico);
			mapDescricaoIten.put(cnpj, new HashMap<String, String>());
			mapDescricaoIten.get(cnpj).put(info, formatado);
			return formatado;
		}
	}
	
	public void addRegistros(String reg){
		if(mapTotalizadorRegistros.containsKey(reg)){
			Integer soma = mapTotalizadorRegistros.get(reg);
			soma++;
			mapTotalizadorRegistros.remove(reg);			
			mapTotalizadorRegistros.put(reg, soma);
		} else {
			mapTotalizadorRegistros.put(reg, 1);
		}
	}
	
	public void removeRegistro(String reg){
		if(mapTotalizadorRegistros.containsKey(reg)){
			Integer soma = mapTotalizadorRegistros.get(reg);
			soma--;
			mapTotalizadorRegistros.remove(reg);			
			mapTotalizadorRegistros.put(reg, soma);
		}
	}
	
	public void addEntrega(Integer id){
		listaIdEntrega.add(id);
	}
	
	public void addNota(Integer id){
		listaIdNota.add(id);
	}
	
	public void addDocumento(Integer id){
		listaIdDocumento.add(id);
	}
	
	public void addIdentificador(String identificador){
		if (identificador!=null){
			listaIdentificador.add(identificador);
		}
	}
	
	public Map<String, Map<String, String>> getMapProdutoServico(String cnpj) {
		return mapProdutoServico.get(cnpj);
	}
	
	public Map<MultiKey, String> getMapParticipante(String cnpj) {
		return mapParticipante.get(cnpj);
	}
	
	public Map<String, String> getMapInfoComplementar(String cnpj) {
		return mapInfoComplementar.get(cnpj);
	}
	
	public Map<Integer, String> getMapUnidade(String cnpj) {
		return mapUnidade.get(cnpj);
	}
	
	public Map<String, String> getMapNatureza(String cnpj) {
		return mapNatureza.get(cnpj);
	}
	
	public Map<String, String> getDescricaoIten(String cnpj) {
		return mapDescricaoIten.get(cnpj);
	}
	
	public String getCpfOuCnpjMapParticipante(Object key) {
		return mapParticipanteCpfCnpj.get(key);
	}

	public Map<Integer, ContaContabil> getMapNaturezaContacontabil() {
		return mapNaturezaContacontabil;
	}
	public Map<String, ContaContabil> getMapEmpresaMaterialContaContabil() {
		return mapEmpresaMaterialContaContabil;
	}
	
}
