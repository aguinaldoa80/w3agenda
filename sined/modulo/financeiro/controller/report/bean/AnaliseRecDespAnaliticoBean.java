package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.sql.Date;
import java.util.Comparator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;

public class AnaliseRecDespAnaliticoBean {
	
	private String identificador;
	private String cgnome;
	private Integer cdcontagerencial;
	private String numeroduplicata;
	private Date vencimento;
	private Date dtmovimentacao;
	private Date dtbanco;
	private String nomepessoa;
	private String historico;
	private Double valorrateadodouble;
	private Double valorduplicatadouble;	
	private Money valorpago;
	private Money valorrateado;
	private Money valorrateadoCG;
	private Money valorduplicata;
	private String operacao;
	private String numerodocumento;
	private String vinculo;
	
	private Boolean naoconsiderartotalrateado;
	
	public AnaliseRecDespAnaliticoBean(){}	
	
	public AnaliseRecDespAnaliticoBean(String identificador, String cgnome, Integer cdcontagerencial, String numeroduplicata,
			Date vencimento, String nomepessoa, String historico, Long valorrateado, Long valorrateadoCG,
			Long valorduplicata, String operacao, String numerodocumento, String vinculo) {
		
		this.identificador = identificador;
		this.cgnome = cgnome;
		this.cdcontagerencial = cdcontagerencial;
		this.numeroduplicata = numeroduplicata;
		this.vencimento = vencimento;
		this.nomepessoa = nomepessoa;
		this.historico = historico;
		this.valorrateado = new Money(valorrateado, true);	
		this.valorrateadoCG = new Money(valorrateadoCG, true);	
		this.valorduplicata = new Money(valorduplicata, true);
		this.valorrateadodouble = this.valorrateado.getValue().doubleValue();
		this.valorduplicatadouble = this.valorduplicata.getValue().doubleValue();
		this.operacao = operacao;
		this.numerodocumento = numerodocumento;
		this.vinculo = vinculo;
	}
	
	public AnaliseRecDespAnaliticoBean(String identificador, String cgnome, Integer cdcontagerencial, String numeroduplicata,
			Date dtmovimentacao, Date dtbanco, String nomepessoa, String historico, Long valorrateado, Long valorrateadoCG,
			Long valorduplicata, String operacao, String numerodocumento, String vinculo) {
		
		this.identificador = identificador;
		this.cgnome = cgnome;
		this.cdcontagerencial = cdcontagerencial;
		this.numeroduplicata = numeroduplicata;
		this.dtmovimentacao = dtmovimentacao;
		this.dtbanco = dtbanco;
		this.nomepessoa = nomepessoa;
		this.historico = historico;
		this.valorrateado = new Money(valorrateado, true);
		this.valorrateadoCG = new Money(valorrateadoCG, true);	
		this.valorduplicata = new Money(valorduplicata, true);
		this.valorrateadodouble = this.valorrateado.getValue().doubleValue();
		this.valorduplicatadouble = this.valorduplicata.getValue().doubleValue();
		this.operacao = operacao;
		this.numerodocumento = numerodocumento;
		this.vinculo = vinculo;
	}

	public String getIdentificador() {
		return identificador;
	}
	public String getCgnome() {
		return cgnome;
	}
	public String getNomepessoa() {
		return nomepessoa;
	}
	public String getHistorico() {
		return historico;
	}
	public Double getValorrateadodouble() {
		return valorrateadodouble;
	}
	public Double getValorduplicatadouble() {
		return valorduplicatadouble;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setCgnome(String cgnome) {
		this.cgnome = cgnome;
	}
	public void setNomepessoa(String nomepessoa) {
		this.nomepessoa = nomepessoa;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setValorrateadodouble(Double valorrateadodouble) {
		this.valorrateadodouble = valorrateadodouble;
	}
	public void setValorduplicatadouble(Double valorduplicatadouble) {
		this.valorduplicatadouble = valorduplicatadouble;
	}

	public String getNumeroduplicata() {
		return numeroduplicata;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setNumeroduplicata(String numeroduplicata) {
		this.numeroduplicata = numeroduplicata;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public Date getDtmovimentacao() {
		return dtmovimentacao;
	}

	public Date getDtbanco() {
		return dtbanco;
	}

	public void setDtmovimentacao(Date dtmovimentacao) {
		this.dtmovimentacao = dtmovimentacao;
	}

	public void setDtbanco(Date dtbanco) {
		this.dtbanco = dtbanco;
	}

	public Money getValorpago() {
		return valorpago;
	}
	
	public Money getValorrateado() {
		return valorrateado;
	}
	
	public Money getValorrateadoCG() {
		return valorrateadoCG;
	}
	
	public Money getValorduplicata() {
		return valorduplicata;
	}

	public void setValorpago(Money valorpago) {
		this.valorpago = valorpago;
	}

	public void setValorrateado(Money valorrateado) {
		this.valorrateado = valorrateado;
	}
	
	public void setValorrateadoCG(Money valorrateadoCG) {
		this.valorrateadoCG = valorrateadoCG;
	}
	
	public void setValorduplicata(Money valorduplicata) {
		this.valorduplicata = valorduplicata;
	}
	
	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public String getNumerodocumento() {
		return numerodocumento;
	}

	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}
	
	public String getVinculo() {
		return vinculo;
	}
	
	public void setVinculo(String vinculo) {
		this.vinculo = vinculo;
	}
	
	public Boolean getNaoconsiderartotalrateado() {
		return naoconsiderartotalrateado;
	}

	public void setNaoconsiderartotalrateado(Boolean naoconsiderartotalrateado) {
		this.naoconsiderartotalrateado = naoconsiderartotalrateado;
	}
	
	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}

	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AnaliseRecDespAnaliticoBean) {
			AnaliseRecDespAnaliticoBean outro = (AnaliseRecDespAnaliticoBean)obj;
			return this.identificador.equals(outro.identificador);
		}
		return false;
	}

	/**
	 * Comparator que usa o identificador da conta gerencial.
	 */
	public static final Comparator<AnaliseRecDespAnaliticoBean> IDENTIFICADOR_COMPARATOR = new Comparator<AnaliseRecDespAnaliticoBean>(){
		public int compare(AnaliseRecDespAnaliticoBean o1, AnaliseRecDespAnaliticoBean o2) {
			String identificadorO1 = StringUtils.deleteWhitespace(o1.getIdentificador());
			String identificadorO2 = StringUtils.deleteWhitespace(o2.getIdentificador());

			return identificadorO1.compareTo(identificadorO2);
		}
	};

	/**
	 * Comparator usado para pesquisa bin�ria na lista de contas gerenciais.
	 * Utiliza como chave o atrinuto 'identificador' dos objetos da lista.
	 * Para encontrar o objeto pai, remove-se do filho os 3 �ltimos caracteres (da direita)
	 * da chave. O que restar do n�mero � o identificador do objeto pai. 
	 */
	public static final Comparator<AnaliseRecDespAnaliticoBean> PAI_COMPARATOR = new Comparator<AnaliseRecDespAnaliticoBean>(){
		public int compare(AnaliseRecDespAnaliticoBean o1, AnaliseRecDespAnaliticoBean o2) {
			String identificadorO1 = StringUtils.deleteWhitespace(o1.getIdentificador());
			String identificadorO2 = StringUtils.deleteWhitespace(o2.getIdentificador());

			if (identificadorO2.split("\\.").length > 1) {
				identificadorO2 = identificadorO2.substring( 0, (identificadorO2.length() - (identificadorO2.split("\\.")[identificadorO2.split("\\.").length-1].length() + 1)) );
			} else {
				identificadorO2 = "";
			}
			return identificadorO1.compareTo(identificadorO2);
		}
	};
	

}
