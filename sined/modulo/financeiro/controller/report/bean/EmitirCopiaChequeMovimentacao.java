package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.util.LinkedList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;

import com.ibm.icu.text.SimpleDateFormat;

public class EmitirCopiaChequeMovimentacao {
	
	protected String historico;
	protected String observacao;
	protected Double valor;
	protected String empresa;
	protected String dataMovimentacao;
	protected String tipooperacao;
	protected LinkedList<EmitirCopiaChequeDocumento> listaDocumento;

	public String getHistorico() {
		return historico;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getDataMovimentacao() {
		return dataMovimentacao;
	}
	public void setDataMovimentacao(String dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}
	public String getTipooperacao() {
		return tipooperacao;
	}
	public void setTipooperacao(String tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	public List<EmitirCopiaChequeDocumento> getListaDocumento() {
		return listaDocumento;
	}
	public void setListaDocumento(
			LinkedList<EmitirCopiaChequeDocumento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	
	public EmitirCopiaChequeMovimentacao(Movimentacao movimentacao){
		this.setEmpresa(movimentacao.getEmpresa() != null? movimentacao.getEmpresa().getRazaosocialOuNome(): "");
		this.setObservacao(movimentacao.getObservacao());
		this.setValor(movimentacao.getValorDouble());
		this.setEmpresa(movimentacao.getHistorico());
		this.setDataMovimentacao(new SimpleDateFormat("dd/MM/yyyy").format(movimentacao.getDataMovimentacao()));
		this.setListaDocumento(new LinkedList<EmitirCopiaChequeDocumento>());
		this.setTipooperacao(Tipooperacao.TIPO_CREDITO.equals(movimentacao.getTipooperacao())? "C":
											Tipooperacao.TIPO_DEBITO.equals(movimentacao.getTipooperacao())? "D": "");
		if(movimentacao.getListaMovimentacaoorigem()!=null){
			for(Movimentacaoorigem movOrig: movimentacao.getListaMovimentacaoorigem()){
				if(movOrig.getDocumento() != null){
					this.getListaDocumento().add(new EmitirCopiaChequeDocumento(movOrig.getDocumento()));	
				}
			}
		
		}
	}
}