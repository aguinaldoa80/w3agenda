package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;



public class CopiaChequeSubReportBean{

	protected String projeto;
	protected String valor;
	protected String contagerencial;
	
	public String getProjeto() {
		return projeto;
	}
	public String getValor() {
		return valor;
	}
	public String getContagerencial() {
		return contagerencial;
	}
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public void setContagerencial(String contagerencial) {
		this.contagerencial = contagerencial;
	}
}
