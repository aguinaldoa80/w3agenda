package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Fluxocaixa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.util.SinedDateUtils;

public class FluxocaixaBeanReport implements Comparable<FluxocaixaBeanReport>{

	public static final int AGENDAMENTO = 1;
	public static final int DOCUMENTO = 2;
	public static final int MOVIMENTACAO = 3;
	public static final int FLUXOCAIXA = 4;
	public static final int CONTRATO = 5;
	public static final int CARTAO = 6;
	public static final int CHEQUE = 7;
	public static final String A = "A";
	public static final String M = "M";
	public static final String C = "C";
	public static final String CT = "CT";
	public static final String DP = "DP";
	public static final String PV = "PV";
	public static final String CC = "CC";
	public static final String CH = "CH";
	
	protected Boolean showDetail;
	protected Date data;
	protected Date dataAtrasada;
	protected String descricao;
	protected Money credito;
	protected Money debito;
	protected Money saldoD;
	protected Money saldoAC;
	protected Conta conta;
	protected String documento;
	protected Tipooperacao tipooperacao;
	protected String pessoadescricao;
	protected String contagerencial;
	protected String projeto;
	
	protected String indicador;
	protected String indicadorOrigem;
	protected int tipo;
	protected boolean header = false;
	protected boolean headerOrigem = false;
	protected Boolean movimentacaoSemOrigem;
	protected Boolean isMedicao;
	protected Movimentacaoacao movimentacaoacao;
	protected boolean totalizador = false;
	
	
	//Utilizado para montar o acumulado mensal por conta gerencial
	protected List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
	
	public FluxocaixaBeanReport() {
	}
	
	public FluxocaixaBeanReport(FluxocaixaBeanReport fluxo, Rateioitem rateioitem) {
		this.showDetail = fluxo.getShowDetail();
		this.data = fluxo.getData();
		this.dataAtrasada = fluxo.getDataAtrasada();
		this.descricao = fluxo.getDescricao();
		this.saldoD = fluxo.getSaldoD();
		this.saldoAC = fluxo.getSaldoAC();
		this.conta = fluxo.getConta();
		this.tipooperacao = fluxo.getTipooperacao();
	
		if(Tipooperacao.TIPO_CREDITO.equals(tipooperacao)){
			if(fluxo.getIsMedicao() != null && fluxo.getIsMedicao()){
				this.credito = fluxo.getCredito().multiply(new Money(rateioitem.getPercentual() != null ? rateioitem.getPercentual() : 0d)).divide(new Money(100d));
			}else {
				this.credito = rateioitem.getValor();
			}
		}else {
			this.debito = rateioitem.getValor();
		}
		
		this.pessoadescricao = fluxo.getPessoadescricao();
		this.listaRateioitem.add(rateioitem);
		if(rateioitem.getContagerencial() != null)
			this.contagerencial = rateioitem.getContagerencial().getNome();
		if(rateioitem.getProjeto() != null)
			this.projeto = rateioitem.getProjeto().getNome();
		
		this.indicador = fluxo.getIndicador();
		this.tipo = fluxo.getTipo();
		this.header = fluxo.isHeader();
		this.documento = fluxo.getDocumento();
	}
	
	public FluxocaixaBeanReport(Movimentacao mov, boolean filtroOrigem) {
		this.setData(mov.getDataMovimentacao());
		this.setDataAtrasada(mov.getDataAtrasada());
		this.setDocumento(mov.getChecknum());
		this.setDescricao(mov.getAux_movimentacao().getMovimentacaodescricao());
		if(this.getDescricao() == null){
			this.setDescricao(mov.getHistorico());
		}
		if(this.getDescricao() == null){
			this.setDescricao("");
		}
		if(mov.getCheque() != null && !Movimentacaoacao.CONCILIADA.equals(mov.getMovimentacaoacao())){
				this.descricao = this.descricao + ", Chq. N�: " + mov.getCheque().getNumero();
		}
		this.setConta(mov.getConta());
		this.setTipooperacao(mov.getTipooperacao());
		this.setTipo(MOVIMENTACAO);
		this.setIndicador(M);
		this.setMovimentacaoacao(mov.getMovimentacaoacao());
		
		if(filtroOrigem){
			if(mov.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
				if(mov.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO) && mov.isCartaocredito()){
					this.setTipo(MOVIMENTACAO);
					this.setIndicador(M);
				}else if(Formapagamento.CHEQUE.equals(mov.getFormapagamento()) && mov.getCheque() != null && Movimentacaoacao.NORMAL.equals(mov.getMovimentacaoacao())){
					this.setTipo(CHEQUE);
					this.setIndicador(CH);
				}
			}
		}
		
		if(mov.isMovCartao()){
			this.setCredito(mov.getCredito());
			this.setDebito(mov.getDebito());
			this.setDescricao(mov.getConta().getNome());
		}
		
		if(mov.getRateio() != null && mov.getRateio().getListaRateioitem() != null && mov.getRateio().getListaRateioitem().size() > 0){
			this.setListaRateioitem(mov.getRateio().getListaRateioitem());
			List<String> listaProjetos = new ArrayList<String>();
			Money valor = new Money();
			for (Rateioitem it : mov.getRateio().getListaRateioitem()) {
				if(it.getProjeto() != null && it.getProjeto().getNome() != null){
					listaProjetos.add(it.getProjeto().getNome());
				}
				valor = valor.add(it.getValor());
			}
			if(!mov.isMovCartao()){
				if(mov.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
					this.setCredito(valor);
				} else if(mov.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
					this.setDebito(valor);
				}
			}
			this.setProjeto(CollectionsUtil.concatenate(listaProjetos, ", "));
		} else if(!mov.isMovCartao()){
			if(mov.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
				this.setCredito(mov.getValor());
			}
			if(mov.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
				this.setDebito(mov.getValor());
			}
		}
		
		if(mov.getListaMovimentacaoorigem() != null && mov.getListaMovimentacaoorigem().size() > 0){
			this.movimentacaoSemOrigem = false;
			List<String> lista = new ArrayList<String>();
			Integer contPessoas = 0;
			for (Movimentacaoorigem movimentacaoorigem : mov.getListaMovimentacaoorigem()) {
				if(movimentacaoorigem.getDocumento() != null && movimentacaoorigem.getDocumento().getTipopagamento() != null){
					if(movimentacaoorigem.getDocumento().getTipopagamento().equals(Tipopagamento.OUTROS) && 
							movimentacaoorigem.getDocumento().getOutrospagamento() != null){
						if(!lista.contains(movimentacaoorigem.getDocumento().getOutrospagamento())){
							if(lista.size() > 4){
								contPessoas++;
							}else {	
								lista.add(movimentacaoorigem.getDocumento().getOutrospagamento());
							}
						}
					} else if(movimentacaoorigem.getDocumento().getPessoa() != null){
						if(!lista.contains(movimentacaoorigem.getDocumento().getPessoa().getNome())){
							if(lista.size() > 4){
								contPessoas++;
							}else {
								lista.add(movimentacaoorigem.getDocumento().getPessoa().getNome());
							}
						}
					}
				}
			}
			if(contPessoas > 0){
				lista.add(" e mais " + contPessoas + " pessoa(s)");
			}
			this.setPessoadescricao(CollectionsUtil.concatenate(lista, ", "));
		}else {
			this.movimentacaoSemOrigem = true;
		}
	}
	
	public FluxocaixaBeanReport(Agendamento ag) {
		this.setData(ag.getDtproximo());
		this.setTipooperacao(ag.getTipooperacao());
		this.setDescricao(ag.getDescricao());
		this.setPessoadescricao((ag.getPessoa() != null ? ag.getPessoa().getNome() : ag.getOutrospagamento()));
		this.setConta(ag.getVinculoProvisionado());
		this.setTipo(AGENDAMENTO);
		this.setIndicador(A);
		this.setDocumento(ag.getDocumento());
		
		if(ag.getRateio() != null && ag.getRateio().getListaRateioitem() != null && ag.getRateio().getListaRateioitem().size() > 0){
			this.setListaRateioitem(ag.getRateio().getListaRateioitem());
			List<String> listaProjeto = new ArrayList<String>();
			Money valor = new Money();
			for (Rateioitem it : ag.getRateio().getListaRateioitem()) {
				if(it.getProjeto() != null && it.getProjeto().getNome() != null){
					listaProjeto.add(it.getProjeto().getNome());
				}
				valor = valor.add(it.getValor());
			}
			if(ag.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
				this.setCredito(valor);
			}
			if(ag.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
				this.setDebito(valor);
			}
			this.setProjeto(CollectionsUtil.concatenate(listaProjeto, ", "));
		} else {
			if(ag.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
				this.setCredito(ag.getValor());
			}
			if(ag.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
				this.setDebito(ag.getValor());
			}
		}
	}
	
	public FluxocaixaBeanReport(Documento doc, boolean useValorAtual, boolean origem) {
		this.setData(doc.getDtvencimentoFluxoCaixa(origem));
		this.setDataAtrasada(doc.getDataAtrasada());
		
		if(doc.getRateio() != null && doc.getRateio().getListaRateioitem() != null && doc.getRateio().getListaRateioitem().size() > 0){
			this.setListaRateioitem(doc.getRateio().getListaRateioitem());
			List<String> listaProjeto = new ArrayList<String>();
			Money valor = new Money();
			for (Rateioitem it : doc.getRateio().getListaRateioitem()) {
				if(it.getProjeto() != null && it.getProjeto().getNome() != null && 
						!listaProjeto.contains(it.getProjeto().getNome())){
					listaProjeto.add(it.getProjeto().getNome());
				}
				valor = valor.add(it.getValor());
			}
			if(doc.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)){
				this.setDebito(valor);
			}
			if(doc.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
				this.setCredito(valor);
			}
			this.setProjeto(CollectionsUtil.concatenate(listaProjeto, ", "));
		} else {	
			if(useValorAtual) doc.setValor(doc.getAux_documento().getValoratual());
			if(doc.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)){
				this.setDebito(doc.getValor());
			}
			if(doc.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
				this.setCredito(doc.getValor());
			}
		}

		if(doc.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)){
			this.setDescricao(doc.getDescricao());
			this.setTipooperacao(Tipooperacao.TIPO_DEBITO);
			this.setConta(doc.getVinculoProvisionado());
		}
		if(doc.getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
			this.setDescricao(doc.getDescricao());
			this.setTipooperacao(Tipooperacao.TIPO_CREDITO);
			this.setConta(doc.getConta());
		}
		
		if(doc.getTipopagamento().equals(Tipopagamento.OUTROS)){
			this.setPessoadescricao(doc.getOutrospagamento());
		} else {
			if(doc.getPessoa() != null) this.setPessoadescricao(doc.getPessoa().getNome());
		}
		this.setTipo(DOCUMENTO);
		this.setIndicador(C);
		this.setDocumento(doc.getNumero());
		
		if(origem){
			if(doc.getDocumentotipo() != null && Boolean.TRUE.equals(doc.getDocumentotipo().getCartao())){
				this.setTipo(CARTAO);
				this.setIndicador(CC);
			}
		}
	}
	
	public FluxocaixaBeanReport(Fluxocaixa fc){
		
		this.setData(fc.getDtinicio());
		this.setDescricao(fc.getDescricao());
		this.setPessoadescricao(fc.getOrdemcompra() != null && fc.getOrdemcompra().getFornecedor() != null && fc.getOrdemcompra().getFornecedor().getNome() != null ? fc.getOrdemcompra().getFornecedor().getNome() : "");
		this.setTipooperacao(fc.getTipooperacao());
		
		if(fc.getRateio() != null && fc.getRateio().getListaRateioitem() != null && fc.getRateio().getListaRateioitem().size() > 0){
			this.setListaRateioitem(fc.getRateio().getListaRateioitem());
			List<String> listaProjeto = new ArrayList<String>();
			Money valor = new Money();
			for (Rateioitem it : fc.getRateio().getListaRateioitem()) {
				if(it.getProjeto() != null && it.getProjeto().getNome() != null){
					listaProjeto.add(it.getProjeto().getNome());
				}
				valor = valor.add(it.getValor());
			}
			if(fc.getTipooperacao() != null && fc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
				this.setCredito(valor);
				this.setDebito(new Money());
			} else	if(fc.getTipooperacao() != null && fc.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
				this.setDebito(valor);
				this.setCredito(new Money());
			}
			this.setProjeto(CollectionsUtil.concatenate(listaProjeto, ", "));
		}
		
		if(fc.getFluxocaixaTipo() != null){
			this.setIndicador(fc.getFluxocaixaTipo().getSigla());
		}
		
		this.setTipo(FLUXOCAIXA);
		
		/* Alterado pois a query n�o faz mais o agrupamento
			ele passou a ser feito aqui.
		
		this.data = fc.getDtfim();
//		this.descricao = fc.getOrdemcompra() != null && fc.getOrdemcompra().getCdordemcompra() != null && !fc.getOrdemcompra().getCdordemcompra().equals(0) ? fc.getOrdemcompra().getObservacao() : fc.getDescricao();
		this.descricao = fc.getDescricao();
		this.pessoadescricao = fc.getOrdemcompra() != null && fc.getOrdemcompra().getFornecedor() != null && fc.getOrdemcompra().getFornecedor().getNome() != null ? fc.getOrdemcompra().getFornecedor().getNome() : "";
		this.tipooperacao = fc.getTipooperacao();
		if (fc.getRateio() != null && fc.getRateio().getListaRateioitem() != null && fc.getRateio().getListaRateioitem().size() > 0){
			this.setListaRateioitem(fc.getRateio().getListaRateioitem());
		}
		if(fc.getTipooperacao() != null && fc.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
			this.credito = fc.getValor();
			this.debito = new Money();
		}else{
			this.debito = fc.getValor();
			this.credito = new Money();
		}
		if(fc.getFluxocaixaTipo() != null){
			this.setIndicador(fc.getFluxocaixaTipo().getSigla());
		}
		this.tipo = FLUXOCAIXA;
		this.projeto = fc.getProjeto();
		*/
	}
	
	public FluxocaixaBeanReport(Contrato ct, Boolean radEvento) {
		this.setData(ct.getDtproximovencimento());
		this.setTipooperacao(Tipooperacao.TIPO_CREDITO);
		
		Money valorContrato = new Money();
		valorContrato = ct.getValorContrato();
		
		if(ContratoService.getInstance().isContratoMedicao(ct)){
			this.isMedicao = true;
			Money valorFaturado = ContratoService.getInstance().findForTotalFaturado(ct);
			if(valorFaturado != null){
				valorContrato = (valorContrato.subtract(valorFaturado)).divide(new Money(ct.getMesesFinalContrato()));
			}
		}
		
		this.setDescricao(radEvento == null || radEvento ? ct.getCliente().getNome() : ct.getDescricao());
		if(ct.getCliente() != null && ct.getCliente().getNome() != null) this.setPessoadescricao(ct.getCliente().getNome());
		this.setTipo(CONTRATO);
		this.setIndicador(CT);	
		
		if(ct.getRateio() != null && ct.getRateio().getListaRateioitem() != null && ct.getRateio().getListaRateioitem().size() > 0){
			Money valorRateado = new Money();
			List<Rateioitem> listaRateioitemContrato = ct.getRateio().getListaRateioitem();
			for (Rateioitem rateioitem : listaRateioitemContrato) {
				valorRateado = valorRateado.add(valorContrato.multiply(new Money(rateioitem.getPercentual()).divide(new Money(100.0))));
			}
			valorContrato = valorRateado;
			this.setListaRateioitem(ct.getRateio().getListaRateioitem());
			
			List<String> listaProjeto = new ArrayList<String>();
			for (Rateioitem it : listaRateioitemContrato) {
				if(it.getProjeto() != null && it.getProjeto().getNome() != null){
					listaProjeto.add(it.getProjeto().getNome());
				}
			}
			this.setProjeto(CollectionsUtil.concatenate(listaProjeto, ", "));
		}
		this.setCredito(valorContrato);
	}

	public FluxocaixaBeanReport(Colaboradordespesa colaboradordespesa) {
		this.setData(colaboradordespesa.getDtdeposito());
		this.setTipooperacao(Tipooperacao.TIPO_DEBITO);
		this.setPessoadescricao(colaboradordespesa.getColaborador().getNome());
		this.setDebito(colaboradordespesa.getTotal());
		this.setDescricao(colaboradordespesa.getColaboradordespesamotivo().getDescricao());
		
		this.setTipo(FLUXOCAIXA);
		this.setIndicador(DP);	
	}

	public FluxocaixaBeanReport(Pedidovenda pedidovenda, Pedidovendapagamento pedidovendapagamento) {
		this.setData(pedidovendapagamento.getDataparcela());
		this.setTipooperacao(Tipooperacao.TIPO_CREDITO);
		this.setPessoadescricao(pedidovenda.getCliente().getNome());
		this.setCredito(pedidovendapagamento.getValororiginal());
		this.setDescricao("Pedido de venda " + pedidovenda.getCdpedidovenda());
		
		this.setTipo(FLUXOCAIXA);
		this.setIndicador(PV);
	}

	public Boolean getShowDetail() {
		return showDetail;
	}
	public Date getData() {
		return data;
	}
	public String getDescricao() {
		return descricao;
	}
	public Money getCredito() {
		return credito;
	}
	public Money getDebito() {
		return debito;
	}
	public Money getSaldoD() {
		return saldoD;
	}
	public Money getSaldoAC() {
		return saldoAC;
	}
	public Conta getConta() {
		return conta;
	}
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	public boolean isHeader() {
		return header;
	}
	public boolean isHeaderOrigem() {
		return headerOrigem;
	}
	public String getIndicador() {
		return indicador;
	}
	public String getIndicadorOrigem() {
		return indicadorOrigem;
	}
	public int getTipo() {
		return tipo;
	}
	public String getContagerencial() {
		return contagerencial;
	}
	public String getProjeto() {
		return projeto;
	}
	public Date getDataAtrasada() {
		return dataAtrasada;
	}
	public List<Rateioitem> getListaRateioitem() {
		return listaRateioitem;
	}
	public void setDataAtrasada(Date dataAtrasada) {
		this.dataAtrasada = dataAtrasada;
	}
	public void setContagerencial(String contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public void setShowDetail(Boolean teste) {
		this.showDetail = teste;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	public void setHeader(boolean header) {
		this.header = header;
	}
	public void setHeaderOrigem(boolean headerOrigem) {
		this.headerOrigem = headerOrigem;
	}
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}
	public void setIndicadorOrigem(String indicadorOrigem) {
		this.indicadorOrigem = indicadorOrigem;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCredito(Money credito) {
		this.credito = credito;
	}
	public void setDebito(Money debito) {
		this.debito = debito;
	}
	public void setSaldoD(Money saldoD) {
		this.saldoD = saldoD;
	}
	public void setSaldoAC(Money saldoAC) {
		this.saldoAC = saldoAC;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setListaRateioitem(List<Rateioitem> listaRateioitem) {
		this.listaRateioitem = listaRateioitem;
	}
	
	public String getDocumento() {
		return documento;
	}
	
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	@Override
	public String toString() {
		SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		return "Data: " + f.format(this.data) + "\t"
				+ (this.indicador != null ? this.indicador : "") + "\t"
				+ " Descricao: "+this.descricao + "\t" 
				+ (this.credito != null ? "Credito: "+this.credito+" " : "") + "\t"
				+ (this.debito != null ? "Debito: "+this.debito+" " : "")
				;
	}

	
	public int compareTo(FluxocaixaBeanReport fc) {
		int compare = this.getData().compareTo(fc.getData());
		if(this.getDataAtrasada() != null && fc.getDataAtrasada() != null){
			compare = this.getDataAtrasada().compareTo(fc.getDataAtrasada());
		}
		if(compare == 0){
			compare = this.getCompareValue().compareTo(fc.getCompareValue());
		}
		return compare;
	}
	
	/**
	 * Obt�m o valor para comparar o cr�dito e d�bito do fluxo de caixa.
	 *  
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Integer getCompareValue(){
		int compare = 0;
		if(this.credito != null && this.credito.compareTo(BigDecimal.ZERO) != 0){
			compare = 1;
		}
		if(this.debito != null && this.debito.compareTo(BigDecimal.ZERO) != 0){
			compare = 2;
		}
		return compare;
	}
	
	/**
	 * Verifica se o Fluxo de caixa � oriundo de um cart�o de cr�dito.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public boolean fluxoDeCartaocredito(){
		if(this.conta != null && this.conta.getContatipo() != null){
			return this.conta.getContatipo().equals(Contatipo.TIPO_CARTAO_CREDITO);
		}
		return false;
	}
	
	/**
	 * Verifica se o Fluxo de caixa � oriundo de uma caixa.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public boolean fluxoDeCaixa(){
		if(this.conta != null){
			return this.conta.getContatipo().equals(Contatipo.TIPO_CAIXA);
		}
		return false;
	}
	
	/**
	 * Verifica se o Fluxo de caixa � oriundo de uma conta banc�ria.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public boolean fluxoDeContabancaria(){
		if(this.conta != null){
			return this.conta.getContatipo().equals(Contatipo.TIPO_CONTA_BANCARIA);
		}
		return false;
	}
	
	public String getPessoadescricao() {
		return this.pessoadescricao;
	}
	
	public void setPessoadescricao(String pessoadescricao) {
		this.pessoadescricao = pessoadescricao;
	}

	public Boolean getMovimentacaoSemOrigem() {
		return movimentacaoSemOrigem;
	}

	public void setMovimentacaoSemOrigem(Boolean movimentacaoSemOrigem) {
		this.movimentacaoSemOrigem = movimentacaoSemOrigem;
	}

	public Boolean getIsMedicao() {
		return isMedicao;
	}

	public void setIsMedicao(Boolean isMedicao) {
		this.isMedicao = isMedicao;
	}
	
	public Date getDataAtrasadaOuData(){
		return getDataAtrasada() != null ? getDataAtrasada() : getData();
	}

	public Movimentacaoacao getMovimentacaoacao() {
		return movimentacaoacao;
	}

	public void setMovimentacaoacao(Movimentacaoacao movimentacaoacao) {
		this.movimentacaoacao = movimentacaoacao;
	}
	
	public boolean isTotalizador() {
		return totalizador;
	}

	public void setTotalizador(boolean totalizador) {
		this.totalizador = totalizador;
	}

	public String getDescricaoIndicador(){
		String s = "";
		if("CR".equalsIgnoreCase(getIndicadorOrigem())) s = "CONTA A RECEBER ";
		if("CHQ".equalsIgnoreCase(getIndicadorOrigem())) s = "CHEQUE ";
		if("CC".equalsIgnoreCase(getIndicadorOrigem())) s = "CART�O ";
		if("CT".equalsIgnoreCase(getIndicadorOrigem())) s = "CONTRATOS ";
		if("AC".equalsIgnoreCase(getIndicadorOrigem())) s = "AGENDAMENTO ";
		if("PV".equalsIgnoreCase(getIndicadorOrigem())) s = "PEDIDO DE VENDA ";
		if("MNC".equalsIgnoreCase(getIndicadorOrigem())) s = "MOVIMENTA��ES NORMAIS ";
		if("MCC".equalsIgnoreCase(getIndicadorOrigem())) s = "MOVIMENTA��ES CONCILIADAS ";
		if("DPC".equalsIgnoreCase(getIndicadorOrigem())) s = "DESPESAS COM PESSOAL ";
		if("CP".equalsIgnoreCase(getIndicadorOrigem())) s = "CONTAS A PAGAR ";
		if("OC".equalsIgnoreCase(getIndicadorOrigem())) s = "ORDEM DE COMPRA ";
		if("AD".equalsIgnoreCase(getIndicadorOrigem())) s = "AGENDAMENTO ";
		if("CF".equalsIgnoreCase(getIndicadorOrigem())) s = "CONTRATO DE FORNECIMENTO ";
		if("DPD".equalsIgnoreCase(getIndicadorOrigem())) s = "DESPESAS COM PESSOAL ";
		if("MND".equalsIgnoreCase(getIndicadorOrigem())) s = "MOVIMENTA��ES NORMAIS ";
		if("MCD".equalsIgnoreCase(getIndicadorOrigem())) s = "MOVIMENTA��ES CONCILIADAS ";
		
		if(StringUtils.isNotBlank(s) && getData() != null){
			s += SinedDateUtils.toString(getData());
		}
		
		return s;
	}
}
