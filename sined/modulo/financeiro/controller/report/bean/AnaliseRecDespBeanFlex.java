package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.sql.Date;
import java.util.List;


public class AnaliseRecDespBeanFlex{
	
	private List<AnaliseRecDespBean> listaDP;
	private Double totaldebito;
	private Double totalcredito;
	private Double saldo;
	private Double saldoinicial;
	private Double saldofinal;
	private Date dtinicio;
	private Date dtfim;
	
	private Boolean movimentacoes;
	
	public List<AnaliseRecDespBean> getListaDP() {
		return listaDP;
	}
	public Double getTotaldebito() {
		return totaldebito;
	}
	public Double getTotalcredito() {
		return totalcredito;
	}
	public Double getSaldo() {
		return saldo;
	}
	public Double getSaldofinal() {
		return saldofinal;
	}
	public Double getSaldoinicial() {
		return saldoinicial;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public Boolean getMovimentacoes() {
		return movimentacoes;
	}
	public void setMovimentacoes(Boolean movimentacoes) {
		this.movimentacoes = movimentacoes;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setSaldofinal(Double saldofinal) {
		this.saldofinal = saldofinal;
	}
	public void setSaldoinicial(Double saldoinicial) {
		this.saldoinicial = saldoinicial;
	}
	public void setListaDP(List<AnaliseRecDespBean> listaDP) {
		this.listaDP = listaDP;
	}
	public void setTotaldebito(Double totaldebito) {
		this.totaldebito = totaldebito;
	}
	public void setTotalcredito(Double totalcredito) {
		this.totalcredito = totalcredito;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

}
