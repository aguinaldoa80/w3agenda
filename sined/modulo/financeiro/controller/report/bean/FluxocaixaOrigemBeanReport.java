package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.util.Date;

import br.com.linkcom.neo.types.Money;

public class FluxocaixaOrigemBeanReport {

	private boolean totalizador;
	private String indicador; 
	private Date data;
	private Date dataAtrasada;
	private Money vl_credito_cr = new Money();
	private Money vl_credito_chq = new Money();
	private Money vl_credito_cc = new Money();
	private Money vl_credito_ct = new Money();
	private Money vl_credito_ac = new Money();
	private Money vl_credito_mnc = new Money();
	private Money vl_credito_mcc = new Money();
	private Money vl_credito_pv = new Money();
	private Money vl_credito_dpc = new Money();
	
	private Money vl_debito_cp = new Money();
	private Money vl_debito_oc = new Money();
	private Money vl_debito_ad = new Money();
	private Money vl_debito_cf = new Money();
	private Money vl_debito_mnd = new Money();
	private Money vl_debito_mcd = new Money();
	private Money vl_debito_dpd = new Money();
	
	@SuppressWarnings("unused")
	private Money vl_saldo_dia = new Money();
	private Money vl_saldo_acumulado = new Money();
	
	
	public boolean isTotalizador() {
		return totalizador;
	}

	public void setTotalizador(boolean totalizador) {
		this.totalizador = totalizador;
	}

	public String getIndicador() {
		return indicador;
	}

	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}

	public Date getData() {
		return data;
	}

	public Date getDataAtrasada() {
		return dataAtrasada;
	}

	public Money getVl_credito_cr() {
		return vl_credito_cr;
	}

	public Money getVl_credito_chq() {
		return vl_credito_chq;
	}

	public Money getVl_credito_cc() {
		return vl_credito_cc;
	}

	public Money getVl_credito_ct() {
		return vl_credito_ct;
	}

	public Money getVl_credito_ac() {
		return vl_credito_ac;
	}

	public Money getVl_credito_mnc() {
		return vl_credito_mnc;
	}

	public Money getVl_credito_mcc() {
		return vl_credito_mcc;
	}

	public Money getVl_debito_cp() {
		return vl_debito_cp;
	}
	
	public Money getVl_credito_pv() {
		return vl_credito_pv;
	}
	
	public void setVl_credito_pv(Money vlCreditoPv) {
		vl_credito_pv = vlCreditoPv;
	}
	
	public Money getVl_credito_dpc() {
		return vl_credito_dpc;
	}

	public void setVl_credito_dpc(Money vlCreditoDpc) {
		vl_credito_dpc = vlCreditoDpc;
	}

	public Money getVl_debito_oc() {
		return vl_debito_oc;
	}

	public Money getVl_debito_ad() {
		return vl_debito_ad;
	}

	public Money getVl_debito_cf() {
		return vl_debito_cf;
	}

	public Money getVl_debito_mnd() {
		return vl_debito_mnd;
	}

	public Money getVl_debito_mcd() {
		return vl_debito_mcd;
	}

	public Money getVl_debito_dpd() {
		return vl_debito_dpd;
	}

	public Money getVl_saldo_dia() {
		return getVl_total_credito().subtract(getVl_total_debito());
	}
	

	public void setData(Date data) {
		this.data = data;
	}

	public void setDataAtrasada(Date dataAtrasada) {
		this.dataAtrasada = dataAtrasada;
	}

	public void setVl_credito_cr(Money vlCreditoCr) {
		vl_credito_cr = vlCreditoCr;
	}

	public void setVl_credito_chq(Money vlCreditoChq) {
		vl_credito_chq = vlCreditoChq;
	}

	public void setVl_credito_cc(Money vlCreditoCc) {
		vl_credito_cc = vlCreditoCc;
	}

	public void setVl_credito_ct(Money vlCreditoCt) {
		vl_credito_ct = vlCreditoCt;
	}

	public void setVl_credito_ac(Money vlCreditoAc) {
		vl_credito_ac = vlCreditoAc;
	}

	public void setVl_credito_mnc(Money vlCreditoMnc) {
		vl_credito_mnc = vlCreditoMnc;
	}

	public void setVl_credito_mcc(Money vlCreditoMcc) {
		vl_credito_mcc = vlCreditoMcc;
	}

	public void setVl_debito_cp(Money vlDebitoCp) {
		vl_debito_cp = vlDebitoCp;
	}

	public void setVl_debito_oc(Money vlDebitoOc) {
		vl_debito_oc = vlDebitoOc;
	}

	public void setVl_debito_ad(Money vlDebitoAd) {
		vl_debito_ad = vlDebitoAd;
	}

	public void setVl_debito_cf(Money vlDebitoCf) {
		vl_debito_cf = vlDebitoCf;
	}

	public void setVl_debito_mnd(Money vlDebitoMnd) {
		vl_debito_mnd = vlDebitoMnd;
	}

	public void setVl_debito_mcd(Money vlDebitoMcd) {
		vl_debito_mcd = vlDebitoMcd;
	}

	public void setVl_debito_dpd(Money vlDebitoDpd) {
		vl_debito_dpd = vlDebitoDpd;
	}

	public Money getVl_saldo_acumulado() {
		return vl_saldo_acumulado;
	}

	public void setVl_saldo_dia(Money vlSaldoDia) {
		vl_saldo_dia = vlSaldoDia;
	}

	public void setVl_saldo_acumulado(Money vlSaldoAcumulado) {
		vl_saldo_acumulado = vlSaldoAcumulado;
	}
	
	public Money getVl_total_credito(){
		return new Money( 
			getVl_credito_cr().getValue().doubleValue() + 
			getVl_credito_chq().getValue().doubleValue() +
			getVl_credito_cc().getValue().doubleValue() +
			getVl_credito_ct().getValue().doubleValue() +
			getVl_credito_ac().getValue().doubleValue() +
			getVl_credito_mnc().getValue().doubleValue() +
			getVl_credito_mcc().getValue().doubleValue() +
			getVl_credito_pv().getValue().doubleValue() +
			getVl_credito_dpc().getValue().doubleValue() 
		);
	}
	
	public Money getVl_total_debito(){
		return new Money(
			getVl_debito_cp().getValue().doubleValue() +
			getVl_debito_oc().getValue().doubleValue() +
			getVl_debito_ad().getValue().doubleValue() +
			getVl_debito_cf().getValue().doubleValue() +
			getVl_debito_mnd().getValue().doubleValue() +
			getVl_debito_mcd().getValue().doubleValue() +
			getVl_debito_dpd().getValue().doubleValue()
			);
	}

	public void addVl_credito_cr(Money vl){
		if(vl != null){
			if(this.vl_credito_cr == null) this.vl_credito_cr = new Money();
			this.vl_credito_cr = this.vl_credito_cr.add(vl);
		}
	}
	public void addVl_credito_chq(Money vl){
		if(vl != null){
			if(this.vl_credito_chq == null) this.vl_credito_chq = new Money();
			this.vl_credito_chq = this.vl_credito_chq.add(vl);
		}
	}
	public void addVl_credito_cc(Money vl){
		if(vl != null){
			if(this.vl_credito_cc == null) this.vl_credito_cc = new Money();
			this.vl_credito_cc = this.vl_credito_cc.add(vl);
		}
	}
	public void addVl_credito_ct(Money vl){
		if(vl != null){
			if(this.vl_credito_ct == null) this.vl_credito_ct = new Money();
			this.vl_credito_ct = this.vl_credito_ct.add(vl);
		}
	}
	public void addVl_credito_ac(Money vl){
		if(vl != null){
			if(this.vl_credito_ac == null) this.vl_credito_ac = new Money();
			this.vl_credito_ac = this.vl_credito_ac.add(vl);
		}
	}
	public void addVl_credito_mnc(Money vl){
		if(vl != null){
			if(this.vl_credito_mnc == null) this.vl_credito_mnc = new Money();
			this.vl_credito_mnc = this.vl_credito_mnc.add(vl);
		}
	}
	public void addVl_credito_mcc(Money vl){
		if(vl != null){
			if(this.vl_credito_mcc == null) this.vl_credito_mcc = new Money();
			this.vl_credito_mcc = this.vl_credito_mcc.add(vl);
		}
	}
	public void addVl_credito_pv(Money vl){
		if(vl != null){
			if(this.vl_credito_pv == null) this.vl_credito_pv = new Money();
			this.vl_credito_pv = this.vl_credito_pv.add(vl);
		}
	}
	public void addVl_credito_dpc(Money vl){
		if(vl != null){
			if(this.vl_credito_dpc == null) this.vl_credito_dpc = new Money();
			this.vl_credito_dpc = this.vl_credito_dpc.add(vl);
		}
	}
	
	public void addVl_debito_cp(Money vl){
		if(vl != null){
			if(this.vl_debito_cp == null) this.vl_debito_cp = new Money();
			this.vl_debito_cp = this.vl_debito_cp.add(vl);
		}
	}
	public void addVl_debito_oc(Money vl){
		if(vl != null){
			if(this.vl_debito_oc == null) this.vl_debito_oc = new Money();
			this.vl_debito_oc = this.vl_debito_oc.add(vl);
		}
	}
	public void addVl_debito_ad(Money vl){
		if(vl != null){
			if(this.vl_debito_ad == null) this.vl_debito_ad = new Money();
			this.vl_debito_ad = this.vl_debito_ad.add(vl);
		}
	}
	public void addVl_debito_cf(Money vl){
		if(vl != null){
			if(this.vl_debito_cf== null) this.vl_debito_cf = new Money();
			this.vl_debito_cf = this.vl_debito_cf.add(vl);
		}
	}
	public void addVl_debito_mnd(Money vl){
		if(vl != null){
			if(this.vl_debito_mnd == null) this.vl_debito_mnd = new Money();
			this.vl_debito_mnd = this.vl_debito_mnd.add(vl);
		}
	}
	public void addVl_debito_mcd(Money vl){
		if(vl != null){
			if(this.vl_debito_mcd == null) this.vl_debito_mcd = new Money();
			this.vl_debito_mcd = this.vl_debito_mcd.add(vl);
		}
	}
	public void addVl_debito_dpd(Money vl){
		if(vl != null){
			if(this.vl_debito_dpd == null) this.vl_debito_dpd = new Money();
			this.vl_debito_dpd = this.vl_debito_dpd.add(vl);
		}
	}
}
