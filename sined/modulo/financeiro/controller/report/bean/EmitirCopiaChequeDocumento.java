package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Documento;

public class EmitirCopiaChequeDocumento {
	
	protected String classe;
	protected Integer conta;
	protected String pessoa;
	protected String identificadorpessoa;
	protected String documento;
	protected Double valor;
	
	public EmitirCopiaChequeDocumento(Documento documento){
		this.setClasse(documento.getDocumentoclasse() != null? documento.getDocumentoclasse().getNome(): "");
		this.setConta(documento.getConta() != null? documento.getConta().getCdconta(): null);
		this.setPessoa(documento.getDescricaoPagamento());
		this.setIdentificadorpessoa(documento.getIdentificadorpessoa());
		this.setDocumento(documento.getNumeroCddocumento());
		this.setValor(documento.getValorDouble());
	}
	
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public Integer getConta() {
		return conta;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public String getPessoa() {
		return pessoa;
	}
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public String getIdentificadorpessoa() {
		return identificadorpessoa;
	}
	public void setIdentificadorpessoa(String identificadorpessoa) {
		this.identificadorpessoa = identificadorpessoa;
	}
}
