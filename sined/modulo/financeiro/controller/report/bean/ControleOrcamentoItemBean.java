package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;


import java.util.Date;

import br.com.linkcom.neo.types.Money;

public class ControleOrcamentoItemBean {
	protected String contagerencial;
	protected String mesano;
	protected Money valor;
	protected Date data;
	
	
	public ControleOrcamentoItemBean() {
		this.valor = new Money(0);
	}
	
	public ControleOrcamentoItemBean(String contagerencial, String mesano,Date data) {
		this.contagerencial = contagerencial;
		this.mesano = mesano;
		this.valor = new Money(0);
		this.data = data;
	}
	
	
	public String getContagerencial() {
		return contagerencial;
	}
	public void setContagerencial(String contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	public String getMesano() {
		return mesano;
	}
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	public void setMesano(String mesano) {
		this.mesano = mesano;
	}

	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj != null){
			ControleOrcamentoItemBean other = (ControleOrcamentoItemBean) obj;
			if((this.contagerencial == null && other.getContagerencial() == null) 
					|| (this.contagerencial != null && other.getContagerencial() != null && this.contagerencial.equals(other.getContagerencial()))){
				if((this.mesano == null && other.getMesano() == null) 
						|| (this.mesano != null && other.getMesano() != null && this.mesano.equals(other.getMesano()))){
					return true;
				} else{
					return false;
				}
			} else{
				return false;
			}
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		Integer hash = 0;
		if(this.mesano != null){
			try{
				hash += this.mesano.hashCode();
			} catch (Exception e) {	}
		}
		if(this.contagerencial != null){
			try{
				hash += this.contagerencial.hashCode();
			} catch (Exception e) {	}
		}
		return hash;
	}
}
