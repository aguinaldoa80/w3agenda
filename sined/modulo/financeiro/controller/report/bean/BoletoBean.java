package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.awt.Image;

public class BoletoBean {

	private Image logo;
	private Image logoBanco;
	private String nomeBanco;
	private String codigoBanco;
	private String cdBanco;
	private String localPagamento;
	private String dtVencimento;
	private String cedente;
	private String cedenteComEndereco;
	private String agCdCedente;
	private String dtDocumento;
	private String numDocumento;
	private String espDocumento;
	private String aceite;
	private String dtProcessamento;
	private String nossoNumero;
	private String usoBanco;
	private String carteira;
	private String especie;
	private String valorDocumento;
	private String instrucoes1;
	private String instrucoes2;
	private String instrucoes3;
	private String instrucoes4;
	private String instrucoes5;
	private String instrucoes6;
	private String instrucoes7;
	private String desconto;
	private String outrasDeducoes;
	private String moraMulta;
	private String outrosAcrescimos;
	private String valorCobrado;
	private String nomeSacado;
	private String enderecoBairroSacado;
	private String cidadeEstadoCepSacado;
	private String caixaPostalSacado;
	private String cdBaixa;
	private String cpfCnpj;
	private String numCodBarras;
	private String numCodBarrasSemFormatacao;
	private String valor;
	private String quantidade;
	private Image codigoBarra;
	private String msgcedente;
	private String clienteIdentificador;
	private String contratoId;
	private String cpfCnpjCedente;
	private String marcadagua;
	private String avalistasacador;
	private String cpfCnpjAvalistasacador;
	private String enderecoCedente;
	private String cepCedente;
	private String enderecoPagador;
	private String cepPagador;
	
	public Image getLogoBanco() {
		return logoBanco;
	}
	public String getNomeBanco() {
		return nomeBanco;
	}
	public String getCodigoBanco() {
		return codigoBanco;
	}
	public String getLocalPagamento() {
		return localPagamento;
	}
	public String getDtVencimento() {
		return dtVencimento;
	}
	public String getCedente() {
		return cedente;
	}
	public String getCedenteComEndereco() {
		return cedenteComEndereco;
	}
	public String getAgCdCedente() {
		return agCdCedente;
	}
	public String getDtDocumento() {
		return dtDocumento;
	}
	public String getNumDocumento() {
		return numDocumento;
	}
	public String getEspDocumento() {
		return espDocumento;
	}
	public String getAceite() {
		return aceite;
	}
	public String getDtProcessamento() {
		return dtProcessamento;
	}
	public String getNossoNumero() {
		return nossoNumero;
	}
	public String getUsoBanco() {
		return usoBanco;
	}
	public String getCarteira() {
		return carteira;
	}
	public String getEspecie() {
		return especie;
	}
	public String getValorDocumento() {
		return valorDocumento;
	}
	public String getInstrucoes1() {
		return instrucoes1;
	}
	public String getInstrucoes2() {
		return instrucoes2;
	}
	public String getInstrucoes3() {
		return instrucoes3;
	}
	public String getInstrucoes4() {
		return instrucoes4;
	}
	public String getInstrucoes5() {
		return instrucoes5;
	}
	public String getInstrucoes6() {
		return instrucoes6;
	}
	public String getDesconto() {
		return desconto;
	}
	public String getOutrasDeducoes() {
		return outrasDeducoes;
	}
	public String getMoraMulta() {
		return moraMulta;
	}
	public String getOutrosAcrescimos() {
		return outrosAcrescimos;
	}
	public String getValorCobrado() {
		return valorCobrado;
	}
	public String getNomeSacado() {
		return nomeSacado;
	}
	public String getEnderecoBairroSacado() {
		return enderecoBairroSacado;
	}
	public String getCidadeEstadoCepSacado() {
		return cidadeEstadoCepSacado;
	}
	public String getCaixaPostalSacado() {
		return caixaPostalSacado;
	}
	public String getCdBaixa() {
		return cdBaixa;
	}
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public String getNumCodBarras() {
		return numCodBarras;
	}
	public Image getCodigoBarra() {
		return codigoBarra;
	}
	public String getValor() {
		return valor;
	}
	public String getQuantidade() {
		return quantidade;
	}
	public String getCdBanco() {
		return cdBanco;
	}
	public String getInstrucoes7() {
		return instrucoes7;
	}
	public String getNumCodBarrasSemFormatacao() {
		return numCodBarrasSemFormatacao;
	}
	public String getMsgcedente() {
		return msgcedente;
	}
	public Image getLogo() {
		return logo;
	}
	public String getContratoId() {
		return contratoId;
	}
	public String getClienteIdentificador() {
		return clienteIdentificador;
	}
	public String getMarcadagua() {
		return marcadagua;
	}
	
	public void setLogo(Image logo) {
		this.logo = logo;
	}
	public void setMsgcedente(String msgcedente) {
		this.msgcedente = msgcedente;
	}
	public void setNumCodBarrasSemFormatacao(String numCodBarrasSemFormatacao) {
		this.numCodBarrasSemFormatacao = numCodBarrasSemFormatacao;
	}
	public void setInstrucoes7(String instrucoes7) {
		this.instrucoes7 = instrucoes7;
	}
	public void setCdBanco(String cdBanco) {
		this.cdBanco = cdBanco;
	}
	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}
	public void setLogoBanco(Image logoBanco) {
		this.logoBanco = logoBanco;
	}
	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}
	public void setCodigoBanco(String codigoBanco) {
		this.codigoBanco = codigoBanco;
	}
	public void setLocalPagamento(String localPagamento) {
		this.localPagamento = localPagamento;
	}
	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}
	public void setCedente(String cedente) {
		this.cedente = cedente;
	}
	public void setCedenteComEndereco(String cedenteComEndereco) {
		this.cedenteComEndereco = cedenteComEndereco;
	}
	public void setAgCdCedente(String agCdCedente) {
		this.agCdCedente = agCdCedente;
	}
	public void setDtDocumento(String dtDocumento) {
		this.dtDocumento = dtDocumento;
	}
	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}
	public void setEspDocumento(String espDocumento) {
		this.espDocumento = espDocumento;
	}
	public void setAceite(String aceite) {
		this.aceite = aceite;
	}
	public void setDtProcessamento(String dtProcessamento) {
		this.dtProcessamento = dtProcessamento;
	}
	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}
	public void setUsoBanco(String usoBanco) {
		this.usoBanco = usoBanco;
	}
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public void setValorDocumento(String valorDocumento) {
		this.valorDocumento = valorDocumento;
	}
	public void setInstrucoes1(String instrucoes1) {
		this.instrucoes1 = instrucoes1;
	}
	public void setInstrucoes2(String instrucoes2) {
		this.instrucoes2 = instrucoes2;
	}
	public void setInstrucoes3(String instrucoes3) {
		this.instrucoes3 = instrucoes3;
	}
	public void setInstrucoes4(String instrucoes4) {
		this.instrucoes4 = instrucoes4;
	}
	public void setInstrucoes5(String instrucoes5) {
		this.instrucoes5 = instrucoes5;
	}
	public void setInstrucoes6(String instrucoes6) {
		this.instrucoes6 = instrucoes6;
	}
	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}
	public void setOutrasDeducoes(String outrasDeducoes) {
		this.outrasDeducoes = outrasDeducoes;
	}
	public void setMoraMulta(String moraMulta) {
		this.moraMulta = moraMulta;
	}
	public void setOutrosAcrescimos(String outrosAcrescimos) {
		this.outrosAcrescimos = outrosAcrescimos;
	}
	public void setValorCobrado(String valorCobrado) {
		this.valorCobrado = valorCobrado;
	}
	public void setNomeSacado(String nomeSacado) {
		this.nomeSacado = nomeSacado;
	}
	public void setEnderecoBairroSacado(String enderecoBairroSacado) {
		this.enderecoBairroSacado = enderecoBairroSacado;
	}
	public void setCidadeEstadoCepSacado(String cidadeEstadoCepSacado) {
		this.cidadeEstadoCepSacado = cidadeEstadoCepSacado;
	}
	public void setCaixaPostalSacado(String caixaPostalSacado) {
		this.caixaPostalSacado = caixaPostalSacado;
	}
	public void setCdBaixa(String cdBaixa) {
		this.cdBaixa = cdBaixa;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public void setNumCodBarras(String numCodBarras) {
		this.numCodBarras = numCodBarras;
	}
	public void setCodigoBarra(Image codigoBarra) {
		this.codigoBarra = codigoBarra;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public void setContratoId(String contratoId) {
		this.contratoId = contratoId;
	}
	public void setClienteIdentificador(String clienteIdentificador) {
		this.clienteIdentificador = clienteIdentificador;
	}
	public String getCpfCnpjCedente() {
		return cpfCnpjCedente;
	}
	public void setCpfCnpjCedente(String cpfCnpjCedente) {
		this.cpfCnpjCedente = cpfCnpjCedente;
	}
	public void setMarcadagua(String marcadagua) {
		this.marcadagua = marcadagua;
	}
	public String getAvalistasacador() {
		return avalistasacador;
	}
	public String getCpfCnpjAvalistasacador() {
		return cpfCnpjAvalistasacador;
	}
	public void setAvalistasacador(String avalistasacador) {
		this.avalistasacador = avalistasacador;
	}
	public void setCpfCnpjAvalistasacador(String cpfCnpjAvalistasacador) {
		this.cpfCnpjAvalistasacador = cpfCnpjAvalistasacador;
	}
	public String getEnderecoCedente() {
		return enderecoCedente;
	}
	public void setEnderecoCedente(String enderecoCedente) {
		this.enderecoCedente = enderecoCedente;
	}
	public String getCepCedente() {
		return cepCedente;
	}
	public void setCepCedente(String cepCedente) {
		this.cepCedente = cepCedente;
	}
	public String getEnderecoPagador() {
		return enderecoPagador;
	}
	public void setEnderecoPagador(String enderecoPagador) {
		this.enderecoPagador = enderecoPagador;
	}
	public String getCepPagador() {
		return cepPagador;
	}
	public void setCepPagador(String cepPagador) {
		this.cepPagador = cepPagador;
	}
}
