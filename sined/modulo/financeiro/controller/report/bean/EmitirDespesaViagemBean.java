package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import java.util.LinkedList;


public class EmitirDespesaViagemBean {

	private String origemviagem;
	private String destinoviagem;
	private String descricao;
	private String motivo;
	private String dtsaida;
	private String dtretornoprevisto;
	private String dtretornorealizado;
	private String colaborador;
	private String situacao;
	private String observacao;
	
	private LinkedList<EmitirDespesaViagemItemBean> listaDespesaviagemitem = new LinkedList<EmitirDespesaViagemItemBean>();
	private LinkedList<EmitirDespesaViagemProjetoBean> listaDespesaviagemprojeto = new LinkedList<EmitirDespesaViagemProjetoBean>();
	
	public String getOrigemviagem() {
		return origemviagem;
	}
	public String getDestinoviagem() {
		return destinoviagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getMotivo() {
		return motivo;
	}
	public String getDtsaida() {
		return dtsaida;
	}
	public String getDtretornoprevisto() {
		return dtretornoprevisto;
	}
	public String getDtretornorealizado() {
		return dtretornorealizado;
	}
	public String getColaborador() {
		return colaborador;
	}
	public String getSituacao() {
		return situacao;
	}
	public String getObservacao() {
		return observacao;
	}
	public LinkedList<EmitirDespesaViagemItemBean> getListaDespesaviagemitem() {
		return listaDespesaviagemitem;
	}
	public LinkedList<EmitirDespesaViagemProjetoBean> getListaDespesaviagemprojeto() {
		return listaDespesaviagemprojeto;
	}
	public void setOrigemviagem(String origemviagem) {
		this.origemviagem = origemviagem;
	}
	public void setDestinoviagem(String destinoviagem) {
		this.destinoviagem = destinoviagem;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public void setDtsaida(String dtsaida) {
		this.dtsaida = dtsaida;
	}
	public void setDtretornoprevisto(String dtretornoprevisto) {
		this.dtretornoprevisto = dtretornoprevisto;
	}
	public void setDtretornorealizado(String dtretornorealizado) {
		this.dtretornorealizado = dtretornorealizado;
	}
	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setListaDespesaviagemitem(LinkedList<EmitirDespesaViagemItemBean> listaDespesaviagemitem) {
		this.listaDespesaviagemitem = listaDespesaviagemitem;
	}
	public void setListaDespesaviagemprojeto(LinkedList<EmitirDespesaViagemProjetoBean> listaDespesaviagemprojeto) {
		this.listaDespesaviagemprojeto = listaDespesaviagemprojeto;
	}
}
