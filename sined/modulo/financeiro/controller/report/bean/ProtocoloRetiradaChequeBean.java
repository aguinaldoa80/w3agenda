package br.com.linkcom.sined.modulo.financeiro.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Cheque;

public class ProtocoloRetiradaChequeBean {
	
	private String codigosCliente;
	private String nomesCliente;
	private String codigosNomesCliente;
	private String cidadesCliente;
	private Cheque cheque;
	private String motivoDevolucao;
	
	
	public String getCodigosCliente() {
		return codigosCliente;
	}
	public String getNomesCliente() {
		return nomesCliente;
	}
	public String getCodigosNomesCliente() {
		return codigosNomesCliente;
	}
	public String getCidadesCliente() {
		return cidadesCliente;
	}
	public Cheque getCheque() {
		return cheque;
	}
	public String getMotivoDevolucao() {
		return motivoDevolucao;
	}
	
	public void setCodigosCliente(String codigosCliente) {
		this.codigosCliente = codigosCliente;
	}
	public void setNomesCliente(String nomesCliente) {
		this.nomesCliente = nomesCliente;
	}
	public void setCodigosNomesCliente(String codigosNomesCliente) {
		this.codigosNomesCliente = codigosNomesCliente;
	}
	public void setCidadesCliente(String cidadesCliente) {
		this.cidadesCliente = cidadesCliente;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	public void setMotivoDevolucao(String motivoDevolucao) {
		this.motivoDevolucao = motivoDevolucao;
	}
}