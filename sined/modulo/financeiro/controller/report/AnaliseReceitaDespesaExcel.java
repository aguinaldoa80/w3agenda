package br.com.linkcom.sined.modulo.financeiro.controller.report;

import javax.servlet.http.Cookie;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;


@Controller(path = "/financeiro/relatorio/AnaliseReceitaDespesaExcel", authorizationModule=ReportAuthorizationModule.class)
public class AnaliseReceitaDespesaExcel extends ResourceSenderController<AnaliseReceitaDespesaReportFiltro>{

	private ContagerencialService contagerencialService;
	private ProjetoService projetoService;
	
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	AnaliseReceitaDespesaReportFiltro filtro) throws Exception {
		request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		contagerencialService.ajustaListasAnaliseReceitaDespesaFiltro(filtro);
		if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("sinteticocentrocusto")){
			return contagerencialService.gerarExcelAnaliseReceitaDespesaPorCentrocusto(filtro);
		}else if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("analiticocsv")){
			contagerencialService.ajustaListasAnaliseReceitaDespesaFiltro(filtro);
			return contagerencialService.gerarRelatorioAnaliticoCSVAnaliseReceitaDespesa(filtro);
		}		
		return contagerencialService.gerarExcelAnaliseReceitaDespesa(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	AnaliseReceitaDespesaReportFiltro filtro) throws Exception {
		request.setAttribute("listaProjetoitem", projetoService.getListaProjetoFiltro());
		return new ModelAndView("relatorio/analiseReceitaDespesa").addObject("filtro", filtro);
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, AnaliseReceitaDespesaReportFiltro filtro) throws Exception {
		try {
			return super.doGerar(request, filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao gerar o relatório Excel.");
			return new ModelAndView("redirect:/financeiro/relatorio/AnaliseReceitaDespesa");
		}
	}
}
