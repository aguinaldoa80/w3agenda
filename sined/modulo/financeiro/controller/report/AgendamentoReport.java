package br.com.linkcom.sined.modulo.financeiro.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.AgendamentoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/Agendamento", authorizationModule=ReportAuthorizationModule.class)
public class AgendamentoReport extends SinedReport<AgendamentoFiltro>{

	private AgendamentoService agendamentoService;
	
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	AgendamentoFiltro filtro) throws Exception {
		return agendamentoService.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "agendamento";
	}
	
	@Override
	public String getTitulo(AgendamentoFiltro filtro) {
		return "AGENDAMENTO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, AgendamentoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getProjeto() != null){
			Projeto projeto = getProjetoService().load(filtro.getProjeto(), "projeto.nome");
			report.addParameter("projeto", projeto.getNome());
		}
		if(filtro.getEmpresa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		if(filtro.getCentrocusto() != null){
			Centrocusto centrocusto = getCentrocustoService().load(filtro.getCentrocusto(), "centrocusto.nome");
			report.addParameter("centroCusto", centrocusto.getNome());
		}
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtVencimentoInicio(), filtro.getDtVencimentoFim()));
	}
	
}
