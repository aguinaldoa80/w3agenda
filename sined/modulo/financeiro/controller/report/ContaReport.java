package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoacaoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.DocumentoRelatorioFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/Conta", authorizationModule=ReportAuthorizationModule.class)
public class ContaReport extends SinedReport<DocumentoRelatorioFiltro>{

	private DocumentoService documentoService;
	private DocumentoacaoService documentoacaoService;
	private ContagerencialService contagerencialService;
	private DocumentotipoService documentotipoService;
	private ContaService contaService;
	
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setDocumentoacaoService(DocumentoacaoService documentoacaoService) {
		this.documentoacaoService = documentoacaoService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	
	@Override
	protected ModelAndView getFiltroModelAndView(WebRequestContext request, DocumentoRelatorioFiltro filtro) {
		String fromListagem = request.getParameter("fromListagem");
		if(fromListagem != null && fromListagem.equals("true")){
			String classe = request.getParameter("classe");
			if(classe != null && classe.equals("PAGAR")){
				return new ModelAndView("redirect:/financeiro/crud/Contapagar");
			} else {
				return new ModelAndView("redirect:/financeiro/crud/Contareceber");
			}
		} else {
			return super.getFiltroModelAndView(request, filtro);
		}
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,DocumentoRelatorioFiltro filtro) throws Exception {
		this.ajustaListasFiltro(filtro);
		String classe = request.getParameter("classe");
//		String orderBy = (String) request.getSession().getAttribute("orderBy");
//		
//		/*
//		 *	Manipula��o do orderBy para evitar asc ou desc duplicados.
//		 *	N�o sei o motivo da ocorr�ncia do erro.
//		 */
//		orderBy = StringUtils.trimToEmpty(orderBy);
//		if(orderBy.matches(".*\\s.*\\s.*")){
//			orderBy = orderBy.substring(0, orderBy.lastIndexOf(" "));
//		}
//		
//		filtro.setOrderBy(orderBy == null ? "" : orderBy);
//		request.getSession().removeAttribute("orderBy");
		
		if (StringUtils.isNotEmpty(classe)) {
			filtro.setTipoConta(classe);
		}
		if(filtro.isTiporelatorioSimplificado()){
			return  documentoService.gerarRelarorioDocumentoSimplificado(filtro);
		}else 
			return  documentoService.gerarRelarorioDocumento(filtro);
	}
	
	/**
	 * M�todo para ajustar as listas de Pagamento e Recebimento do filtro.]
	 * 
	 * @param filtro
	 * @author Fl�vio Tavares
	 */
	private void ajustaListasFiltro(DocumentoRelatorioFiltro filtro) {
		List<ItemDetalhe> listaFornecedor = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaCliente = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaColaborador = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaOutrospagamento = new ArrayList<ItemDetalhe>();
		
		List<ItemDetalhe> listaPagamentos = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaPagamentoa = filtro.getListaPagamentoa();
		List<ItemDetalhe> listaRecebimentode = filtro.getListaRecebimentode();
		if(SinedUtil.isListNotEmpty(listaPagamentoa)) listaPagamentos.addAll(listaPagamentoa);
		if(SinedUtil.isListNotEmpty(listaRecebimentode)) listaPagamentos.addAll(listaRecebimentode);
		
		if(SinedUtil.isListNotEmpty(listaPagamentos)){
			for (ItemDetalhe id : listaPagamentos) {
				switch(id.getTipopagamento()){
					case CLIENTE:
						listaCliente.add(id);
						break;
					case COLABORADOR:
						listaColaborador.add(id);
						break;
					case FORNECEDOR:
						listaFornecedor.add(id);
						break;
					case OUTROS:
						listaOutrospagamento.add(id);
						break;
				}
			}
		}
		filtro.setListaFornecedor(listaFornecedor);
		filtro.setListaCliente(listaCliente);
		filtro.setListaColaborador(listaColaborador);
		filtro.setListaOutrospagamento(listaOutrospagamento);
		
		if(filtro.getListaContagerencial() != null && filtro.getListaContagerencial().size() > 0){
			filtro.setListaContagerencial(this.ajustaListaContaGerencial(filtro.getListaContagerencial()));
		}
	}
	
	private List<ItemDetalhe> ajustaListaContaGerencial(List<ItemDetalhe> listaContagerencial) {
		List<Contagerencial> listaCG = new ArrayList<Contagerencial>();
		
		for (ItemDetalhe itemDetalhe : listaContagerencial) {
			listaCG.addAll(contagerencialService.findAllFilhos(contagerencialService.loadWithIdentificador(itemDetalhe.getContagerencial())));
		}
		
		List<ItemDetalhe> listaNova = new ArrayList<ItemDetalhe>();
		ItemDetalhe itemDetalhe;
		for (Contagerencial contagerencial : listaCG) {
			itemDetalhe = new ItemDetalhe();
			itemDetalhe.setContagerencial(contagerencial);
			listaNova.add(itemDetalhe);
		}
		
		return listaNova;
	}
	
	@Override
	protected void filtro(WebRequestContext request, DocumentoRelatorioFiltro filtro) throws Exception {
		super.filtro(request, filtro);
		
		List<Documentoacao> listaCartorio = new ArrayList<Documentoacao>();
		
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("PAGAR", "Conta a pagar");
		map.put("RECEBER", "Conta a receber");
		
		filtro.setListaDocumentoacao(documentoacaoService.findAcoesContaPagar());
		List<Documentoacao> listacompleta = documentoacaoService.findForContapagar();
		
		request.setAttribute("listaDocumentotipo", documentotipoService.getListaDocumentoTipoUsuario());
		request.setAttribute("listaProjetoitem", getProjetoService().getListaProjetoFiltro());
		request.setAttribute("listaCentro", getCentrocustoService().findAtivos());
		request.setAttribute("listaFornecedor", getFornecedorService().findAtivos());
		request.setAttribute("tipo", map);
		
		if(filtro.getTipoConta() == null || filtro.getTipoConta().equals("PAGAR") || filtro.getTipoConta().equals("")){
			request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
			listaCartorio.add(Documentoacao.PROTESTADA);
		} else {
			request.setAttribute("tipooperacao", Tipooperacao.TIPO_CREDITO);
			listaCartorio.add(Documentoacao.ENVIADO_CARTORIO);
			listaCartorio.add(Documentoacao.ENVIADO_JURIDICO);
			listaCartorio.add(Documentoacao.PROTESTADA);
			listaCartorio.add(Documentoacao.ARQUIVO_MORTO);
			
			listacompleta.add(Documentoacao.BAIXADA_PARCIAL);
		}
		request.setAttribute("listaCompleta", listacompleta);
		request.setAttribute("listaCartorio", listaCartorio);
		request.setAttribute("listaTipopagamentoPag", documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_DEBITO));
		request.setAttribute("listaTipopagamentoRec", documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_CREDITO));
		request.setAttribute("listaCaixa", contaService.findByTipo(Contatipo.TIPO_CAIXA));
		request.setAttribute("listaCartao", contaService.findByTipo(Contatipo.TIPO_CARTAO_CREDITO));
		request.setAttribute("listaContabancaria", contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA));
	}

	@Override
	public String getTitulo(DocumentoRelatorioFiltro filtro) {
		if(filtro.getTipoConta().equals("PAGAR"))
			return "RELAT�RIO " + (filtro.isTiporelatorioSimplificado() ? "SIMPLIFICADO" : "DETALHADO") + " DE CONTAS A PAGAR";
		else
			return "RELAT�RIO " + (filtro.isTiporelatorioSimplificado() ? "SIMPLIFICADO" : "DETALHADO") + " DE CONTAS A RECEBER";
	}
	
	@Override
	public String getNomeArquivo() {
		return "conta";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, DocumentoRelatorioFiltro filtro) {
		if(filtro.getProjeto() != null && filtro.getProjeto().getCdprojeto() != null){
			report.addParameter("PROJETO", projetoService.load(filtro.getProjeto(), "projeto.nome").getNome());
		}
		
		if(filtro.getCentrocusto() != null && filtro.getCentrocusto().getCdcentrocusto() != null){
			report.addParameter("CENTROCUSTO", centrocustoService.load(filtro.getCentrocusto(), "centrocusto.nome").getNome());
		}
		
		if(filtro.getDtvencimento1() != null || filtro.getDtvencimento2() != null){
			report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtvencimento1(), filtro.getDtvencimento2()));
		}
		
		if(filtro.getDtbaixa1() != null || filtro.getDtbaixa2() != null){
			report.addParameter("PERIODOBAIXA", SinedUtil.getDescricaoPeriodo(filtro.getDtbaixa1(), filtro.getDtbaixa2()));
		}
		
		if(filtro.getListaProjeto() != null && filtro.getListaProjeto().size() > 0){
			report.addParameter("PROJETO", projetoService.getNomeProjetos(filtro.getListaProjeto()));
		}
		
		if(filtro.getListaCentrocusto() != null && filtro.getListaCentrocusto().size() > 0){
			report.addParameter("CENTROCUSTO", centrocustoService.getNomeCentroCustos(filtro.getListaCentrocusto()));
		}
		
		if(filtro.getListaCaixa() != null && filtro.getListaCaixa().size() > 0){
			report.addParameter("CAIXA", contaService.getNomeContas(filtro.getListaCaixa()));
		}
		
		if(filtro.getListaCartao() != null && filtro.getListaCartao().size() > 0){
			report.addParameter("CARTAO", contaService.getNomeContas(filtro.getListaCartao()));
		}
		
		if(filtro.getListaContabancaria() != null && filtro.getListaContabancaria().size() > 0){
			report.addParameter("CONTABANCARIA", contaService.getNomeContas(filtro.getListaContabancaria()));
		}
	}
	
}
