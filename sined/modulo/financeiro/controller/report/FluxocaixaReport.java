package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.DocumentoacaoService;
import br.com.linkcom.sined.geral.service.FluxocaixaService;
import br.com.linkcom.sined.geral.service.FluxocaixaTipoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.TipoRelatorioFluxoCaixa;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
		path = "/financeiro/relatorio/Fluxocaixa",
		authorizationModule=ReportAuthorizationModule.class
	)
public class FluxocaixaReport extends SinedReport<FluxocaixaFiltroReport> {
	
	private FluxocaixaService fluxocaixaService;
	private DocumentoacaoService documentoacaoService;
	private ContaService contaService;
	private FluxocaixaTipoService fluxocaixaTipoService;
	
	public void setFluxocaixaService(FluxocaixaService fluxocaixaService) {
		this.fluxocaixaService = fluxocaixaService;
	}
	public void setDocumentoacaoService(DocumentoacaoService documentoacaoService) {
		this.documentoacaoService = documentoacaoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setFluxocaixaTipoService(FluxocaixaTipoService fluxocaixaTipoService) {
		this.fluxocaixaTipoService = fluxocaixaTipoService;
	}
	
	@Override
	protected void filtro(WebRequestContext request, FluxocaixaFiltroReport filtro)throws Exception {
		super.filtro(request, filtro);
		this.initializeFilter(request, filtro);		
	}
	
	/**
	 * M�todo para inicializar os par�metros do filtro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoacaoService#findAcoesContaPagar()
	 * @see br.com.linkcom.sined.geral.service.DocumentoacaoService#findAcoesContaReceber()
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#findAtivos()
	 * @see br.com.linkcom.sined.geral.service.ContaService#findByTipo(Contatipo)
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#getListaProjetoFiltro()
	 * @param request
	 * @param filtro
	 * @author Fl�vio Tavares
	 */
	private void initializeFilter(WebRequestContext request, FluxocaixaFiltroReport filtro){
		//DEFINI��O DO PADR�O DA LISTA DE A��ES DE CONTAS A PAGAR/RECEBER
		List<Documentoacao> listaPagar = documentoacaoService.findAcoesContaPagar();
		List<Documentoacao> listaReceber = documentoacaoService.findAcoesContaReceber();
		
		listaPagar.remove(Documentoacao.PROTESTADA);
		listaReceber.remove(Documentoacao.PROTESTADA);
		
		request.setAttribute("listaAcaoContaPagar", listaPagar);
		request.setAttribute("listaAcaoContaReceber", listaReceber);
		
		List<Documentoacao> listaPagarValue = documentoacaoService.findAcoesContaPagar();
		listaPagarValue.remove(Documentoacao.PROTESTADA);
		listaPagarValue.remove(Documentoacao.NEGOCIADA);
		
		if(StringUtils.isBlank(filtro.getHashFiltro())){
			filtro.setListaAcoesPagar(listaPagarValue);
			filtro.setListaAcoesReceber(listaReceber);
		}
		
		request.setAttribute("listaCaixa", contaService.findByTipo(Contatipo.TIPO_CAIXA));
		request.setAttribute("listaCartaocredito", contaService.findByTipo(Contatipo.TIPO_CARTAO_CREDITO));
		request.setAttribute("listaContabancaria", contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA));
		request.setAttribute("listaProjetoitem", getProjetoService().getListaProjetoFiltro());
		request.setAttribute("listaFluxocaixaTipoCompleta", fluxocaixaTipoService.findForCombo());
		
		List<NaturezaContagerencial> listanaturezaContaGerencial = new ArrayList<NaturezaContagerencial>();
		listanaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_ATIVO);
		listanaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		listanaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_PASSIVO);
		listanaturezaContaGerencial.add(NaturezaContagerencial.CONTAS_RESULTADO);
		listanaturezaContaGerencial.add(NaturezaContagerencial.PATRIMONIO_LIQUIDO);
		listanaturezaContaGerencial.add(NaturezaContagerencial.CONTA_GERENCIAL);		
		listanaturezaContaGerencial.add(NaturezaContagerencial.OUTRAS);
		request.setAttribute("listanaturezaContaGerencial", listanaturezaContaGerencial);
		if(StringUtils.isBlank(filtro.getHashFiltro())){
			List<NaturezaContagerencial> listaFiltroNaturezaContaGerencial = new ArrayList<NaturezaContagerencial>();
			listaFiltroNaturezaContaGerencial.add(NaturezaContagerencial.CONTA_GERENCIAL);
			filtro.setListanaturezaContaGerencial(listaFiltroNaturezaContaGerencial);
		}
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, FluxocaixaFiltroReport filtro)throws Exception {
		if(TipoRelatorioFluxoCaixa.ANALITICO_ORIGEM_PDF.equals(filtro.getTipo()) || TipoRelatorioFluxoCaixa.SINTETICO_ORIGEM_PDF.equals(filtro.getTipo())){
			return fluxocaixaService.createReportFluxocaixaOrigem(request, filtro);
		}else {
			return fluxocaixaService.createReportFluxocaixa(request, filtro);
		}
	}
	
	@Override
	public String getNomeArquivo() {
		return "fluxocaixa";
	}

	@Override
	public String getTitulo(FluxocaixaFiltroReport filtro) {
		return "FLUXO DE CAIXA";
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	/**
	 * Complementa os par�metros que devem ser enviados para o cabe�alho do filtro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoacaoService#getNomeAcoes(List)
	 * @see br.com.linkcom.sined.geral.service.ContaService#getNomeContas(List)
	 * @author Hugo Ferreira
	 */
	@Override
	protected void adicionaParametrosFiltro(Report report, FluxocaixaFiltroReport filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		String caixa = "Desconsiderada";
		String cartao = "Desconsiderado";
		String contaBancaria = "Desconsiderada";
		String contaPagarSituacao = documentoacaoService.getNomeAcoes(filtro.getListaAcoesPagar());
		String contaReceberSituacao = documentoacaoService.getNomeAcoes(filtro.getListaAcoesReceber());
		
		
		if (filtro.getRadCaixa() != null) {
			caixa = contaService.getNomeContas(filtro.getListaCaixa());
		}
		if (filtro.getRadCartao() != null) {
			cartao = contaService.getNomeContas(filtro.getListaCartao());
		}
		if (filtro.getRadContbanc() != null) {
			contaBancaria = contaService.getNomeContas(filtro.getListaContabancaria());
		}
		
		if (filtro.getRadProjeto().getId().equals("comProjeto")) {
			report.addParameter("projeto", "Apenas com projeto");
		} else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
			report.addParameter("projeto", "Apenas sem projeto");
		}
		
		report.addParameter("caixa", caixa);
		report.addParameter("cartao", cartao);
		report.addParameter("contaBancaria", contaBancaria);
		report.addParameter("contaPagarSituacao", contaPagarSituacao != null ? contaPagarSituacao : "Nenhum");
		report.addParameter("contaReceberSituacao", contaReceberSituacao != null ? contaReceberSituacao : "Nenhum");
		report.addParameter("agendamento", getInfAgendamento(filtro));
	}
	
	/**
	 * Retonra as informa��es selecionadas no filtro, que condizem a Agendamento,
	 * para colocar no cabe�alho do relat�rio.
	 * 
	 * @param filtro
	 * @return
	 * @author Hugo Ferreira
	 */
	private String getInfAgendamento(FluxocaixaFiltroReport filtro) {
		List<String> listaAgendamento = new ArrayList<String>();
		
		if (BooleanUtils.isTrue(filtro.getAgendCredito())) {
			listaAgendamento.add("Cr�ditos");
		}
		if (BooleanUtils.isTrue(filtro.getAgendDebito())) {
			listaAgendamento.add("D�bitos");
		}
		if (BooleanUtils.isTrue(filtro.getConsDocumentos())) {
			listaAgendamento.add("Considerar v�nculo em conta a pagar/receber");
		}
		
		return listaAgendamento.isEmpty() ? null : CollectionsUtil.concatenate(listaAgendamento, ", ");
	}
		
}
