package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.LinkedHashMap;
import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.service.DocumentoService;

@Controller(path = "/financeiro/relatorio/CopiaChequeDocumentoTemplate", authorizationModule=ReportAuthorizationModule.class)
public class CopiaChequeDocumentoTemplate extends ReportTemplateController<ReportTemplateFiltro> {

	private DocumentoService documentoService;
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_COPIA_CHEQUE_DOCUMENTO;
	}

	@Override
	protected String getFileName() {
		return "c�pia de cheque";
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(
			WebRequestContext request, ReportTemplateFiltro filtro)
			throws Exception {
		String whereInDocumentos = request.getParameter("whereInDocumentos");
		
		List<Documento> listaDocumentos = documentoService.findDocsCopiaChequeForTemplate(whereInDocumentos);
		
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();

		mapa.put("lista", documentoService.geraListaCopiaChequeDocumentoBean(listaDocumentos));
		return mapa;
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,
			ReportTemplateFiltro filtro) {
		if (filtro.getTemplate() != null) {
			return getReportTemplateService().load(filtro.getTemplate());
		} else {
			return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COPIA_CHEQUE_DOCUMENTO);
		}
	}
}
