package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.FluxocaixaService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path = "/financeiro/relatorio/Fluxocaixacsvmensal",
		authorizationModule=ReportAuthorizationModule.class
	)
public class FluxocaixaCSVMensalReport extends ResourceSenderController<FluxocaixaFiltroReport> {
	
	private FluxocaixaService fluxocaixaService;
	
	public void setFluxocaixaService(FluxocaixaService fluxocaixaService) {
		this.fluxocaixaService = fluxocaixaService;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	FluxocaixaFiltroReport filtro) throws Exception {
		return new ModelAndView("redirect:/financeiro/relatorio/Fluxocaixa");
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	FluxocaixaFiltroReport filtro) throws Exception {
		SinedUtil.setCookieBlockButton(request);
		return fluxocaixaService.createReportFluxocaixaMensalCSV(filtro);
	}
	
}
