package br.com.linkcom.sined.modulo.financeiro.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ProtocoloRetiradaChequeFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/ProtocoloRetiradaCheque", authorizationModule=ReportAuthorizationModule.class)
public class ProtocoloRetiradaChequeReport extends SinedReport<ProtocoloRetiradaChequeFiltro>{
	
	private ChequeService chequeService;
	
	public void setChequeService(ChequeService chequeService) {
		this.chequeService = chequeService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ProtocoloRetiradaChequeFiltro filtro) throws Exception {
		return chequeService.createReportProtocoloRetiradaCheque(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "protocolo_retirada_cheque";
	}

	@Override
	public String getTitulo(ProtocoloRetiradaChequeFiltro filtro) {
		return "Protocolo de retirada de cheques";
	}	
}
