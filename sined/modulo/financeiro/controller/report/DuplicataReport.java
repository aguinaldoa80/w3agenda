package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path = "/financeiro/relatorio/Duplicata", authorizationModule=ReportAuthorizationModule.class)
public class DuplicataReport extends ResourceSenderController<FiltroListagemSined>{

	private DocumentoService documentoService;
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,
			FiltroListagemSined filtro) throws Exception {
		return documentoService.createDuplicata(request);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			FiltroListagemSined filtro) throws Exception {
		return new ModelAndView("redirect:/financeiro/crud/Contareceber" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cddocumento=" + request.getParameter("selectedItens") : ""));
	}

}
