package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path = "/financeiro/relatorio/Boleto", authorizationModule=ReportAuthorizationModule.class)
public class BoletoReport extends ResourceSenderController<FiltroListagemSined>{

	private ContareceberService contareceberService;
	
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		if(request.getParameter("fromVenda") != null && request.getParameter("fromVenda").equals("true")){
			return new ModelAndView("redirect:/faturamento/crud/Venda" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cdvenda=" + request.getParameter("selectedItens") : ""));
		} else {
			return new ModelAndView("redirect:/financeiro/crud/Contareceber" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cddocumento=" + request.getParameter("selectedItens") : ""));
		}
	}

	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		return contareceberService.createBoleto(request, null);
	}
}