package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.AgendamentoFiltro;

@Controller(path = "/financeiro/relatorio/AgendamentoCSV", authorizationModule=ReportAuthorizationModule.class)
public class AgendamentoCSVReport extends ResourceSenderController<AgendamentoFiltro>{

	private AgendamentoService agendamentoService;
	
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	AgendamentoFiltro filtro) throws Exception {
		return agendamentoService.gerarRelatorioCSV(filtro);
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	AgendamentoFiltro filtro) throws Exception {
		return null;
	}
	
}
