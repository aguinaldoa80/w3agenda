package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ContratoFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path = "/faturamento/relatorio/Emitircontratomatricial", authorizationModule=ReportAuthorizationModule.class)
public class EmitirContratoMatricialReport extends ResourceSenderController<ContratoFiltro>{
	
	private ContratoService contratoService;
	
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	
	@Override
	public Resource generateResource(WebRequestContext request, ContratoFiltro filtro) throws Exception {
		String contatoStr = contratoService.createReportMatricial(filtro);
		Resource resource = new Resource();
		resource.setContentType("application/w3erp");
		resource.setFileName("contrato_" + SinedUtil.datePatternForReport() + ".w3erp");
	    resource.setContents(contatoStr.getBytes());
	    
		return resource;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ContratoFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/faturamento/crud/Contrato");
	}
	
}