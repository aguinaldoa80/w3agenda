package br.com.linkcom.sined.modulo.financeiro.controller.report.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FluxocaixaTipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroSalvoReport;

public class FluxocaixaFiltroReport extends FiltroSalvoReport {

	protected TipoRelatorioFluxoCaixa tipo;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Fornecedor fornecedor;
	protected Cliente cliente;
	protected Date periodoDe;
	protected Date periodoAte;
	protected Contatipo contatipo;
	protected Conta conta;
	protected Tipooperacao tipooperacao;
	protected List<Contatipo> listaContatipoLancamentos;
	protected List<Contatipo> listaContatipoSaldos;
	protected List<FluxocaixaTipo> listaFluxocaixaTipo;
	
	protected Money saldoAC;
	protected Money saldoACTela;
	
	// GERAL
	protected Boolean mostrarDetalhes;
	protected Boolean valoresDocAtualizados;
	protected Boolean consLimiteContabancSaldo;
	protected Boolean consLimiteCartaoSaldo;
	protected Boolean showMovimentacoes;
	
	protected Boolean radCaixa;
	protected Boolean radCartao;
	protected Boolean radContbanc;
	protected Boolean radVinculoprovisionado;
	protected Boolean radFornecedor;
	protected Boolean radCliente;
	protected Boolean radCentrocusto;
	protected GenericBean radProjeto;
	protected Boolean radProjetotipo;
	protected Boolean radEmpresa;
	
	protected Boolean radEvento;
	
	protected Boolean lancContapagarAberto;
	protected Boolean lancContapagarAtraso;
	protected Boolean lancContareceberAberto;
	protected Boolean lancContareceberAtraso;
	
	protected Boolean lancAgendamentoAnteriores;
	protected Boolean lancMovimentacoesAnteriores;
	protected Boolean lancContratoAnteriores;
	
	protected Boolean consDespesacolaborador;
	protected Boolean consPedidovenda;
	protected Controleorcamento controleOrcamentario;
	protected Boolean agendCredito;
	protected Boolean agendDebito;
	protected Boolean contratos;
	protected Boolean valoresAnteriores;
	protected Boolean consDocumentos;
	
	
	protected Boolean agrupamentoMensal;
	
	protected List<Documentoacao> listaAcoesPagar;
	protected List<Documentoacao> listaAcoesReceber;
	protected Boolean exibirContaProtestada;
	protected List<ItemDetalhe> listaCaixa;
	protected List<ItemDetalhe> listaCartao;
	protected List<ItemDetalhe> listaContabancaria;
	protected List<ItemDetalhe> listaProjeto;
	protected List<ItemDetalhe> listaFornecedor;
	protected List<ItemDetalhe> listaCliente;
	protected List<ItemDetalhe> listaCentrocusto;
	protected List<ItemDetalhe> listaEmpresa;
	protected List<ItemDetalhe> listaProjetotipo;
	protected List<NaturezaContagerencial> listanaturezaContaGerencial;
	protected String whereInNaturezaContaGerencial;
	protected Empresa empresa;
	
	protected Boolean mostrarLimiteContabancariaResumo;
	protected Boolean mostrarContasInativadas;
	protected Boolean mostrarContasgerenciaisSemLanc;
	protected Boolean consControleOrcamentario;
	
	protected Date dtInicioBase;
	protected Money saldoInicioBancarias = new Money();
	protected Money saldoInicioCaixas = new Money();
	protected Money saldoInicioCartoes = new Money();
	protected Money saldoInicioOutros = new Money();
	protected Money saldoInicioOutrosResumo = new Money();

	protected Money limiteBancarias = new Money();
	protected Money limiteCartoes = new Money();
	private List<Conta> listConta;
	private Empresa[] empresas;
	

	{
		this.periodoDe = SinedDateUtils.firstDateOfMonth();
		this.periodoAte = SinedDateUtils.lastDateOfMonth();
		this.mostrarDetalhes = Boolean.FALSE;
		this.valoresDocAtualizados = Boolean.TRUE;
		this.consLimiteContabancSaldo = Boolean.FALSE;
		this.consLimiteCartaoSaldo = Boolean.FALSE;
		this.showMovimentacoes = Boolean.TRUE;
		
		this.valoresAnteriores = Boolean.FALSE;
		
		this.radCaixa = Boolean.TRUE;
		this.radCartao = Boolean.TRUE;
		this.radContbanc = Boolean.TRUE;
		
//		this.lancContapagarAberto = Boolean.TRUE;
//		this.lancContapagarAtraso = Boolean.TRUE;
//		this.lancContareceberAberto = Boolean.TRUE;
//		this.lancContareceberAtraso = Boolean.TRUE;
		
		this.agendCredito = Boolean.TRUE;
		this.agendDebito = Boolean.TRUE;
		this.consDocumentos = Boolean.TRUE; 
		this.contratos = Boolean.TRUE;
		
		this.radProjeto = new GenericBean("todosProjetos", "Todos");
	}
	
	@Required
	@DisplayName("Tipo")
	public TipoRelatorioFluxoCaixa getTipo() {
		return tipo;
	}
	
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@Required
	public Date getPeriodoDe() {
		return periodoDe;
	}
	@Required
	public Date getPeriodoAte() {
		return periodoAte;
	}
	@DisplayName("V�nculo")
	public Contatipo getContatipo() {
		return contatipo;
	}
	@DisplayName("Descri��o do v�nculo")
	public Conta getConta() {
		return conta;
	}
	
	// GERAL
	@DisplayName("Detalhar eventos de fluxo de caixa")
	public Boolean getMostrarDetalhes() {
		return mostrarDetalhes;
	}
	@DisplayName("Exibir valores de documentos a pagar e receber atualizados")
	public Boolean getValoresDocAtualizados() {
		return valoresDocAtualizados;
	}
	@DisplayName("Considerar limite de contas banc�rias como saldo")
	public Boolean getConsLimiteCartaoSaldo() {
		return consLimiteCartaoSaldo;
	}
	@DisplayName("Mostrar movimenta��es")
	public Boolean getShowMovimentacoes() {
		return showMovimentacoes;
	}
	@DisplayName("Considerar limite de cart�es de cr�dito como saldo")
	public Boolean getConsLimiteContabancSaldo() {
		return consLimiteContabancSaldo;
	}
	@DisplayName("Caixa")
	public Boolean getRadCaixa() {
		return radCaixa;
	}
	@DisplayName("Cart�o de cr�dito")
	public Boolean getRadCartao() {
		return radCartao;
	}
	@DisplayName("Contas banc�rias")
	public Boolean getRadContbanc() {
		return radContbanc;
	}
	@DisplayName("V�nculo Provisionado")
	public Boolean getRadVinculoprovisionado() {
		return radVinculoprovisionado;
	}
	@DisplayName("Centro de custo")
	public Boolean getRadCentrocusto() {
		return radCentrocusto;
	}
	@DisplayName("Cliente")
	public Boolean getRadCliente() {
		return radCliente;
	}
	@DisplayName("Empresa")
	public Boolean getRadEmpresa() {
		return radEmpresa;
	}
	@DisplayName("Fornecedor")
	public Boolean getRadFornecedor() {
		return radFornecedor;
	}
	@DisplayName("Projeto")
	public GenericBean getRadProjeto() {
		return radProjeto;
	}
	
	@DisplayName("Lan�amentos em contas a pagar (em aberto)")
	public Boolean getLancContapagarAberto() {
		return lancContapagarAberto;
	}
	@DisplayName("Lan�amentos em contas a pagar (em atraso)")
	public Boolean getLancContapagarAtraso() {
		return lancContapagarAtraso;
	}
	@DisplayName("Lan�amentos em contas a receber (em aberto)")
	public Boolean getLancContareceberAberto() {
		return lancContareceberAberto;
	}
	@DisplayName("Lan�amentos em contas a receber (em atraso)")
	public Boolean getLancContareceberAtraso() {
		return lancContareceberAtraso;
	}
	@DisplayName("Agendamentos futuros a cr�dito")
	public Boolean getAgendCredito() {
		return agendCredito;
	}
	@DisplayName("Agendamentos futuros a d�bito")
	public Boolean getAgendDebito() {
		return agendDebito;
	}
	@DisplayName("Contratos")
	public Boolean getContratos() {
		return contratos;
	}
	@DisplayName("Mostrar valores anteriores")
	public Boolean getValoresAnteriores() {
		return valoresAnteriores;
	}
	@DisplayName("Considerar v�nculo em conta a pagar/receber")
	public Boolean getConsDocumentos() {
		return consDocumentos;
	}
	@DisplayName("Situa��es de contas a pagar")
	public List<Documentoacao> getListaAcoesPagar() {
		return listaAcoesPagar;
	}
	@DisplayName("Situa��es de contas a receber")
	public List<Documentoacao> getListaAcoesReceber() {
		return listaAcoesReceber;
	}
	@DisplayName("Exibir contas protestadas")
	public Boolean getExibirContaProtestada() {
		return exibirContaProtestada;
	}
	@DisplayName("Tipo de opera��o")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	@DisplayName("Lan�amento em contas")
	public List<Contatipo> getListaContatipoLancamentos() {
		return listaContatipoLancamentos;
	}
	@DisplayName("Saldo em contas")
	public List<Contatipo> getListaContatipoSaldos() {
		return listaContatipoSaldos;
	}
	public List<FluxocaixaTipo> getListaFluxocaixaTipo() {
		return listaFluxocaixaTipo;
	}
	public Money getSaldoAC() {
		if (saldoACTela!=null){
			return saldoACTela;
		}else {
			return saldoAC;
		}
	}
	public Money getSaldoACTela() {
		return saldoACTela;
	}
	@DisplayName("Caixa")
	public List<ItemDetalhe> getListaCaixa() {
		return listaCaixa;
	}
	@DisplayName("Cart�o de cr�dito")
	public List<ItemDetalhe> getListaCartao() {
		return listaCartao;
	}
	@DisplayName("Conta banc�ria")
	public List<ItemDetalhe> getListaContabancaria() {
		return listaContabancaria;
	}
	@DisplayName("Centro de custo")
	public List<ItemDetalhe> getListaCentrocusto() {
		return listaCentrocusto;
	}
	@DisplayName("Cliente")
	public List<ItemDetalhe> getListaCliente() {
		return listaCliente;
	}
	@DisplayName("Empresa")
	public List<ItemDetalhe> getListaEmpresa() {
		return listaEmpresa;
	}
	@DisplayName("Fornecedor")
	public List<ItemDetalhe> getListaFornecedor() {
		return listaFornecedor;
	}
	@DisplayName("Projeto")
	public List<ItemDetalhe> getListaProjeto() {
		return listaProjeto;
	}
	public Boolean getAgrupamentoMensal() {
		return agrupamentoMensal;
	}
	public Boolean getConsPedidovenda() {
		return consPedidovenda;
	}
	@DisplayName("")
	public Boolean getConsControleOrcamentario() {
		return consControleOrcamentario;
	}
	@DisplayName("Controle Or�ament�rio")
	public Controleorcamento getControleOrcamentario() {
		return controleOrcamentario;
	}
	@DisplayName("Tipo de projeto")
	public Boolean getRadProjetotipo() {
		return radProjetotipo;
	}
	@DisplayName("Tipo de projeto")
	public List<ItemDetalhe> getListaProjetotipo() {
		return listaProjetotipo;
	}
	public void setRadProjetotipo(Boolean radProjetotipo) {
		this.radProjetotipo = radProjetotipo;
	}
	public void setListaProjetotipo(List<ItemDetalhe> listaProjetotipo) {
		this.listaProjetotipo = listaProjetotipo;
	}
	public void setConsControleOrcamentario(Boolean consControleOrcamentario) {
		this.consControleOrcamentario = consControleOrcamentario;
	}
	public void setControleOrcamentario(Controleorcamento controleOrcamentario) {
		this.controleOrcamentario = controleOrcamentario;
	}
	public void setConsPedidovenda(Boolean consPedidovenda) {
		this.consPedidovenda = consPedidovenda;
	}
	public void setTipo(TipoRelatorioFluxoCaixa tipo) {
		this.tipo = tipo;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setPeriodoDe(Date periodoDe) {
		this.periodoDe = periodoDe;
	}
	public void setPeriodoAte(Date periodoAte) {
		this.periodoAte = periodoAte;
	}
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setMostrarDetalhes(Boolean mostrarDetalhes) {
		this.mostrarDetalhes = mostrarDetalhes;
	}
	public void setValoresDocAtualizados(Boolean valoresDocAtualizados) {
		this.valoresDocAtualizados = valoresDocAtualizados;
	}
	public void setRadCaixa(Boolean vincCaixa) {
		this.radCaixa = vincCaixa;
	}
	public void setRadCartao(Boolean vincCartao) {
		this.radCartao = vincCartao;
	}
	public void setRadContbanc(Boolean vincContbanc) {
		this.radContbanc = vincContbanc;
	}
	public void setRadVinculoprovisionado(Boolean radVinculoprovisionado) {
		this.radVinculoprovisionado = radVinculoprovisionado;
	}
	public void setRadFornecedor(Boolean radFornecedor) {
		this.radFornecedor = radFornecedor;
	}
	public void setRadCliente(Boolean radCliente) {
		this.radCliente = radCliente;
	}
	public void setRadCentrocusto(Boolean radCentrocusto) {
		this.radCentrocusto = radCentrocusto;
	}
	public void setRadProjeto(GenericBean radProjeto) {
		this.radProjeto = radProjeto;
	}
	public void setRadEmpresa(Boolean radEmpresa) {
		this.radEmpresa = radEmpresa;
	}
	public void setLancContapagarAberto(Boolean lancContapagarAberto) {
		this.lancContapagarAberto = lancContapagarAberto;
	}
	public void setLancContapagarAtraso(Boolean lancContapagarAtraso) {
		this.lancContapagarAtraso = lancContapagarAtraso;
	}
	public void setLancContareceberAberto(Boolean lancContareceberAberto) {
		this.lancContareceberAberto = lancContareceberAberto;
	}
	public void setLancContareceberAtraso(Boolean lancContareceberAtraso) {
		this.lancContareceberAtraso = lancContareceberAtraso;
	}
	public void setAgendCredito(Boolean agendCredito) {
		this.agendCredito = agendCredito;
	}
	public void setAgendDebito(Boolean agendDebito) {
		this.agendDebito = agendDebito;
	}
	public void setConsDocumentos(Boolean consDocumentos) {
		this.consDocumentos = consDocumentos;
	}
	public void setListaAcoesPagar(List<Documentoacao> listaAcoesPagar) {
		this.listaAcoesPagar = listaAcoesPagar;
	}
	public void setListaAcoesReceber(List<Documentoacao> listaAcoesReceber) {
		this.listaAcoesReceber = listaAcoesReceber;
	}
	public void setExibirContaProtestada(Boolean exibirContaProtestada) {
		this.exibirContaProtestada = exibirContaProtestada;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	public void setListaContatipoLancamentos(List<Contatipo> listaContatipoLancamentos) {
		this.listaContatipoLancamentos = listaContatipoLancamentos;
	}
	public void setListaContatipoSaldos(List<Contatipo> listaContatipoSaldos) {
		this.listaContatipoSaldos = listaContatipoSaldos;
	}
	public void setListaFluxocaixaTipo(List<FluxocaixaTipo> listaFluxocaixaTipo) {
		this.listaFluxocaixaTipo = listaFluxocaixaTipo;
	}
	public void setSaldoAC(Money saldoAC) {
		this.saldoAC = saldoAC;
	}
	public void setConsLimiteContabancSaldo(Boolean consLimiteContabancSaldo) {
		this.consLimiteContabancSaldo = consLimiteContabancSaldo;
	}
	public void setConsLimiteCartaoSaldo(Boolean consLimiteCartaoSaldo) {
		this.consLimiteCartaoSaldo = consLimiteCartaoSaldo;
	}
	public void setShowMovimentacoes(Boolean showMovimentacoes) {
		this.showMovimentacoes = showMovimentacoes;
	}
	public void setListaCaixa(List<ItemDetalhe> listaCaixa) {
		this.listaCaixa = listaCaixa;
	}
	public void setListaCartao(List<ItemDetalhe> listaCartao) {
		this.listaCartao = listaCartao;
	}
	public void setListaContabancaria(List<ItemDetalhe> listaContabancaria) {
		this.listaContabancaria = listaContabancaria;
	}
	public void setListaProjeto(List<ItemDetalhe> listaProjeto) {
		this.listaProjeto = listaProjeto;
	}
	public void setListaFornecedor(List<ItemDetalhe> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}
	public void setListaCliente(List<ItemDetalhe> listaCliente) {
		this.listaCliente = listaCliente;
	}
	public void setListaCentrocusto(List<ItemDetalhe> listaCentrocusto) {
		this.listaCentrocusto = listaCentrocusto;
	}
	public void setListaEmpresa(List<ItemDetalhe> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
	public void setContratos(Boolean contratos) {
		this.contratos = contratos;
	}
	public void setValoresAnteriores(Boolean valoresAnteriores) {
		this.valoresAnteriores = valoresAnteriores;
	}
	public void setAgrupamentoMensal(Boolean agrupamentoMensal) {
		this.agrupamentoMensal = agrupamentoMensal;
	}
	@DisplayName("Caixa")
	public Boolean getRadEvento() {
		return radEvento;
	}
	public void setRadEvento(Boolean radEvento) {
		this.radEvento = radEvento;
	}
	
	@DisplayName("Natureza do Plano de Contas")
	public List<NaturezaContagerencial> getListanaturezaContaGerencial() {
		return listanaturezaContaGerencial;
	}
	public void setListanaturezaContaGerencial(
			List<NaturezaContagerencial> listanaturezaContaGerencial) {
		this.listanaturezaContaGerencial = listanaturezaContaGerencial;
	}
	public String getWhereInNaturezaContaGerencial() {
		return whereInNaturezaContaGerencial;
	}
	public void setWhereInNaturezaContaGerencial(
			String whereInNaturezaContaGerencial) {
		this.whereInNaturezaContaGerencial = whereInNaturezaContaGerencial;
	}
	
	@DisplayName("Mostrar limite de conta banc�ria no resumo")
	public Boolean getMostrarLimiteContabancariaResumo() {
		return mostrarLimiteContabancariaResumo;
	}
	@DisplayName("Mostrar contas inativadas (caixa/contas banc�rias/cart�o de cr�dito)")
	public Boolean getMostrarContasInativadas() {
		return mostrarContasInativadas;
	}

	public void setMostrarContasInativadas(Boolean mostrarContasInativadas) {
		this.mostrarContasInativadas = mostrarContasInativadas;
	}
	public void setMostrarLimiteContabancariaResumo(Boolean mostrarLimiteContabancariaResumo) {
		this.mostrarLimiteContabancariaResumo = mostrarLimiteContabancariaResumo;
	}
	@DisplayName("Considerar Despesa de Colaborador")
	public Boolean getConsDespesacolaborador() {
		return consDespesacolaborador;
	}
	public void setConsDespesacolaborador(Boolean consDespesacolaborador) {
		this.consDespesacolaborador = consDespesacolaborador;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Date getDtInicioBase() {
		return dtInicioBase;
	}
	public void setDtInicioBase(Date dtInicioBase) {
		this.dtInicioBase = dtInicioBase;
	}
	public Money getSaldoInicioBancarias() {
		return saldoInicioBancarias;
	}

	public void setSaldoInicioBancarias(Money saldoInicioBancarias) {
		this.saldoInicioBancarias = saldoInicioBancarias;
	}

	public Money getSaldoInicioCaixas() {
		return saldoInicioCaixas;
	}

	public void setSaldoInicioCaixas(Money saldoInicioCaixas) {
		this.saldoInicioCaixas = saldoInicioCaixas;
	}

	public Money getSaldoInicioCartoes() {
		return saldoInicioCartoes;
	}
	
	public Money getSaldoInicioOutros() {
		return saldoInicioOutros;
	}

	public void setSaldoInicioCartoes(Money saldoInicioCartoes) {
		this.saldoInicioCartoes = saldoInicioCartoes;
	}
	
	public void setSaldoInicioOutros(Money saldoInicioOutros) {
		this.saldoInicioOutros = saldoInicioOutros;
	}

	public Money getLimiteBancarias() {
		return limiteBancarias;
	}

	public void setLimiteBancarias(Money limiteBancarias) {
		this.limiteBancarias = limiteBancarias;
	}

	public Money getLimiteCartoes() {
		return limiteCartoes;
	}

	public void setLimiteCartoes(Money limiteCartoes) {
		this.limiteCartoes = limiteCartoes;
	}

	public List<Conta> getListConta() {
		return listConta;
	}
	public void setListaConta(List<Conta> listConta) {
		this.listConta = listConta;
	}

	public Empresa[] getEmpresas() {
		return empresas;
	}
	public void setListaEmpresa(Empresa[] empresas) {
		this.empresas = empresas;
	}

	public Boolean getLancAgendamentoAnteriores() {
		return lancAgendamentoAnteriores;
	}
	public Boolean getLancMovimentacoesAnteriores() {
		return lancMovimentacoesAnteriores;
	}
	public Boolean getLancContratoAnteriores() {
		return lancContratoAnteriores;
	}

	public void setLancAgendamentoAnteriores(Boolean lancAgendamentoAnteriores) {
		this.lancAgendamentoAnteriores = lancAgendamentoAnteriores;
	}
	public void setLancMovimentacoesAnteriores(Boolean lancMovimentacoesAnteriores) {
		this.lancMovimentacoesAnteriores = lancMovimentacoesAnteriores;
	}
	public void setLancContratoAnteriores(Boolean lancContratoAnteriores) {
		this.lancContratoAnteriores = lancContratoAnteriores;
	}

	public Boolean getMostrarContasgerenciaisSemLanc() {
		return mostrarContasgerenciaisSemLanc;
	}
	public void setMostrarContasgerenciaisSemLanc(Boolean mostrarContasgerenciaisSemLanc) {
		this.mostrarContasgerenciaisSemLanc = mostrarContasgerenciaisSemLanc;
	}

	public Money getSaldoInicioOutrosResumo() {
		return saldoInicioOutrosResumo;
	}

	public void setSaldoInicioOutrosResumo(Money saldoInicioOutrosResumo) {
		this.saldoInicioOutrosResumo = saldoInicioOutrosResumo;
	}
	public void setSaldoACTela(Money saldoACTela) {
		this.saldoACTela = saldoACTela;
	}
	
	public boolean isFiltroOrigem(){
		return TipoRelatorioFluxoCaixa.ANALITICO_ORIGEM_PDF.equals(this.getTipo()) || TipoRelatorioFluxoCaixa.SINTETICO_ORIGEM_PDF.equals(this.getTipo()) ||
		 	   TipoRelatorioFluxoCaixa.ANALITICO_ORIGEM_CSV.equals(this.getTipo()) || TipoRelatorioFluxoCaixa.SINTETICO_ORIGEM_CSV.equals(this.getTipo());
	}
}
