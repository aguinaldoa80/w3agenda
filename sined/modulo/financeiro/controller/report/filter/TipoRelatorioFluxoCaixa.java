package br.com.linkcom.sined.modulo.financeiro.controller.report.filter;

public enum TipoRelatorioFluxoCaixa {

	SINTETICO_PDF					("Sint�tico - Resumo di�rio (Formato PDF)"),
	ANALITICO_PDF					("Anal�tico (Formato PDF)"),
	ANALITICO_CSV					("Anal�tico (Formato CSV)"),
	MENSAL_POR_CONTA_GERENCIAL_CSV	("Mensal por conta gerencial (Formato CSV)"),
	MENSAL_POR_CENTRO_CUSTO_CSV		("Mensal por centro de custo (Formato CSV)"),
	MENSAL_POR_PROJECAO_XLS			("Mensal por proje��o (Formato XLS)"),
	SINTETICO_ORIGEM_PDF			("Sint�tico por Origem (Formato PDF)"),
	SINTETICO_ORIGEM_CSV			("Sint�tico por Origem (Formato CSV)"),
	ANALITICO_ORIGEM_PDF			("Anal�tico por Origem (Formato PDF)"),
	ANALITICO_ORIGEM_CSV			("Anal�tico por Origem (Formato CSV)"),
	;
	
	String nome;
	
	private TipoRelatorioFluxoCaixa(String nome){
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {

		return getNome();
	}
}
