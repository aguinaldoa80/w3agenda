package br.com.linkcom.sined.modulo.financeiro.controller.report.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.AnaliseReceitaDespesaOrigem;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroSalvoReport;

public class AnaliseReceitaDespesaReportFiltro extends FiltroSalvoReport {
	// Geral
	private Boolean radBuscavencimento = Boolean.FALSE;
	private Date dtInicio = SinedDateUtils.firstDateOfMonth();
	private Date dtFim;

	// Outros:
	private Boolean exibirMovimentacaoSemVinculo = Boolean.FALSE;
	private Boolean exibirMovimentacaoSemVinculoDocumento = Boolean.FALSE;
	private Boolean radFornecedor = Boolean.TRUE;
	private Boolean radCliente = Boolean.TRUE;
	private Boolean radColaborador = Boolean.TRUE;
	private Boolean radCentrocusto = Boolean.TRUE;
	private Boolean radContagerencial = Boolean.TRUE;
	private Boolean radProjetotipo = Boolean.TRUE;

	private Boolean radPagamentoa = Boolean.TRUE;
	private Boolean radRecebimentode = Boolean.TRUE;

	private GenericBean radProjeto;
	private Boolean radEmpresa = Boolean.TRUE;
	private Boolean radCategoria = Boolean.TRUE;
	protected Boolean radCaixa = Boolean.TRUE;
	protected Boolean radCartao = Boolean.TRUE;
	protected Boolean radContbanc = Boolean.TRUE;
	protected Boolean radSemVinculo = Boolean.TRUE;

	private List<ItemDetalhe> listaProjeto;
	private List<ItemDetalhe> listaFornecedor;
	private List<ItemDetalhe> listaCliente;
	private List<ItemDetalhe> listaColaborador;
	private List<ItemDetalhe> listaCentrocusto;
	private List<ItemDetalhe> listaContagerencial;
	private List<ItemDetalhe> listaProjetotipo;
	private List<ItemDetalhe> listaEmpresa;
	private List<ItemDetalhe> listaPagamentoa;
	private List<ItemDetalhe> listaRecebimentode;
	private List<ItemDetalhe> listaOutrospagamento;
	private List<ItemDetalhe> listaCategoria;
	protected List<ItemDetalhe> listaCaixa;
	protected List<ItemDetalhe> listaCartao;
	protected List<ItemDetalhe> listaContabancaria;

	private Boolean receitas = Boolean.TRUE;
	private Boolean despesas = Boolean.TRUE;
	private Boolean contassemlancamentos = Boolean.FALSE;
	private Boolean exibirTotalizadorAnaliticoCSV = Boolean.TRUE;
	private Boolean carregarMovimentacoes = Boolean.FALSE;

	private AnaliseReceitaDespesaOrigem origem;
	private List<Documentoacao> listaSituacaoDocumentos;
	private Boolean movConciliadas;
	private Boolean movNormais;
	private Contagerencial contagerencial;
	private String tiporelatorio;
	
	public AnaliseReceitaDespesaReportFiltro() {
		listaSituacaoDocumentos = new ArrayList<Documentoacao>();
		listaSituacaoDocumentos.add(Documentoacao.PREVISTA);
		listaSituacaoDocumentos.add(Documentoacao.DEFINITIVA);
		listaSituacaoDocumentos.add(Documentoacao.AUTORIZADA);
		listaSituacaoDocumentos.add(Documentoacao.AUTORIZADA_PARCIAL);
	}

	@DisplayName("Data in�cio")
	@Required
	public Date getDtInicio() {
		return dtInicio;
	}

	@DisplayName("Data Fim")
	public Date getDtFim() {
		return dtFim;
	}

	@DisplayName("Colaborador")
	public List<ItemDetalhe> getListaColaborador() {
		return listaColaborador;
	}

	@DisplayName("Centro de custo")
	public List<ItemDetalhe> getListaCentrocusto() {
		return listaCentrocusto;
	}

	@DisplayName("Conta Gerencial")
	public List<ItemDetalhe> getListaContagerencial() {
		return listaContagerencial;
	}
	
	@DisplayName("Tipo de projeto")
	public List<ItemDetalhe> getListaProjetotipo() {
		return listaProjetotipo;
	}

	@DisplayName("Cliente")
	public List<ItemDetalhe> getListaCliente() {
		return listaCliente;
	}

	@DisplayName("Empresa")
	public List<ItemDetalhe> getListaEmpresa() {
		return listaEmpresa;
	}

	@DisplayName("Fornecedor")
	public List<ItemDetalhe> getListaFornecedor() {
		return listaFornecedor;
	}

	@DisplayName("Projeto")
	public List<ItemDetalhe> getListaProjeto() {
		return listaProjeto;
	}

	@DisplayName("Centro de custo")
	public Boolean getRadCentrocusto() {
		return radCentrocusto;
	}

	@DisplayName("Conta Gerencial")
	public Boolean getRadContagerencial() {
		return radContagerencial;
	}
	
	@DisplayName("Tipo de projeto")
	public Boolean getRadProjetotipo() {
		return radProjetotipo;
	}

	@DisplayName("Pago a")
	public Boolean getRadPagamentoa() {
		return radPagamentoa;
	}

	@DisplayName("Recebido de")
	public Boolean getRadRecebimentode() {
		return radRecebimentode;
	}

	@DisplayName("Cliente")
	public Boolean getRadCliente() {
		return radCliente;
	}

	@DisplayName("Colaborador")
	public Boolean getRadColaborador() {
		return radColaborador;
	}

	@DisplayName("Empresa")
	public Boolean getRadEmpresa() {
		return radEmpresa;
	}

	@DisplayName("Categoria")
	public Boolean getRadCategoria() {
		return radCategoria;
	}

	@DisplayName("Fornecedor")
	public Boolean getRadFornecedor() {
		return radFornecedor;
	}

	@DisplayName("Projeto")
	public GenericBean getRadProjeto() {
		return radProjeto;
	}

	@DisplayName("Caixa")
	public Boolean getRadCaixa() {
		return radCaixa;
	}

	@DisplayName("Cart�o de cr�dito")
	public Boolean getRadCartao() {
		return radCartao;
	}

	@DisplayName("Contas banc�rias")
	public Boolean getRadContbanc() {
		return radContbanc;
	}

	@DisplayName("Exibir contas gerenciais sem lan�amentos")
	public Boolean getContassemlancamentos() {
		return contassemlancamentos;
	}
	
	@DisplayName("Exibir totalizador (Anal�tico csv)")
	public Boolean getExibirTotalizadorAnaliticoCSV() {
		return exibirTotalizadorAnaliticoCSV;
	}

	public List<Documentoacao> getListaSituacaoDocumentos() {
		return listaSituacaoDocumentos;
	}

	@DisplayName("Caixa")
	public List<ItemDetalhe> getListaCaixa() {
		return listaCaixa;
	}

	@DisplayName("Cart�o de cr�dito")
	public List<ItemDetalhe> getListaCartao() {
		return listaCartao;
	}

	@DisplayName("Conta banc�ria")
	public List<ItemDetalhe> getListaContabancaria() {
		return listaContabancaria;
	}

	public void setListaSituacaoDocumentos(List<Documentoacao> listaSituacaoDocumentos) {
		this.listaSituacaoDocumentos = listaSituacaoDocumentos;
	}

	public void setContassemlancamentos(Boolean contassemlancamentos) {
		this.contassemlancamentos = contassemlancamentos;
	}
	
	public void setExibirTotalizadorAnaliticoCSV(Boolean exibirTotalizadorAnaliticoCSV) {
		this.exibirTotalizadorAnaliticoCSV = exibirTotalizadorAnaliticoCSV;
	}

	public Boolean getDespesas() {
		return despesas;
	}

	public Boolean getReceitas() {
		return receitas;
	}

	@DisplayName("Pagamento a")
	public List<ItemDetalhe> getListaPagamentoa() {
		return listaPagamentoa;
	}

	@DisplayName("Recebimento de")
	public List<ItemDetalhe> getListaRecebimentode() {
		return listaRecebimentode;
	}

	@DisplayName("Outros")
	public List<ItemDetalhe> getListaOutrospagamento() {
		return listaOutrospagamento;
	}

	@DisplayName("Categoria")
	public List<ItemDetalhe> getListaCategoria() {
		return listaCategoria;
	}

	@Required
	public AnaliseReceitaDespesaOrigem getOrigem() {
		return origem;
	}
	
	@DisplayName("Conciliada")
	public Boolean getMovConciliadas() {
		return movConciliadas;
	}
	
	@DisplayName("Normal")
	public Boolean getMovNormais() {
		return movNormais;
	}
	
	public void setMovConciliadas(Boolean movConciliadas) {
		this.movConciliadas = movConciliadas;
	}
	
	public void setMovNormais(Boolean movNormais) {
		this.movNormais = movNormais;
	}

	public void setOrigem(AnaliseReceitaDespesaOrigem origem) {
		this.origem = origem;
	}

	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public void setListaCentrocusto(List<ItemDetalhe> listaCentrocusto) {
		this.listaCentrocusto = listaCentrocusto;
	}

	public void setListaContagerencial(List<ItemDetalhe> listaContagerencial) {
		this.listaContagerencial = listaContagerencial;
	}
	
	public void setListaProjetotipo(List<ItemDetalhe> listaProjetotipo) {
		this.listaProjetotipo = listaProjetotipo;
	}

	public void setListaCliente(List<ItemDetalhe> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public void setListaColaborador(List<ItemDetalhe> listaColaborador) {
		this.listaColaborador = listaColaborador;
	}

	public void setListaEmpresa(List<ItemDetalhe> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}

	public void setListaFornecedor(List<ItemDetalhe> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}

	public void setListaProjeto(List<ItemDetalhe> listaProjeto) {
		this.listaProjeto = listaProjeto;
	}

	public void setRadCentrocusto(Boolean radCentrocusto) {
		this.radCentrocusto = radCentrocusto;
	}

	public void setRadContagerencial(Boolean radContagerencial) {
		this.radContagerencial = radContagerencial;
	}
	
	public void setRadProjetotipo(Boolean radProjetotipo) {
		this.radProjetotipo = radProjetotipo;
	}

	public void setRadPagamentoa(Boolean radPagamentoa) {
		this.radPagamentoa = radPagamentoa;
	}

	public void setRadRecebimentode(Boolean radRecebimentode) {
		this.radRecebimentode = radRecebimentode;
	}

	public void setRadCliente(Boolean radCliente) {
		this.radCliente = radCliente;
	}

	public void setRadColaborador(Boolean radColaborador) {
		this.radColaborador = radColaborador;
	}

	public void setRadEmpresa(Boolean radEmpresa) {
		this.radEmpresa = radEmpresa;
	}

	public void setRadCategoria(Boolean radCategoria) {
		this.radCategoria = radCategoria;
	}

	public void setRadFornecedor(Boolean radFornecedor) {
		this.radFornecedor = radFornecedor;
	}

	public void setRadProjeto(GenericBean radProjeto) {
		this.radProjeto = radProjeto;
	}

	public void setReceitas(Boolean receitas) {
		this.receitas = receitas;
	}

	public void setDespesas(Boolean despesas) {
		this.despesas = despesas;
	}

	public void setListaPagamentoa(List<ItemDetalhe> listaPagamentoa) {
		this.listaPagamentoa = listaPagamentoa;
	}

	public void setListaRecebimentode(List<ItemDetalhe> listaRecebimentode) {
		this.listaRecebimentode = listaRecebimentode;
	}

	public void setListaOutrospagamento(List<ItemDetalhe> listaOutrospagamento) {
		this.listaOutrospagamento = listaOutrospagamento;
	}

	public void setListaCategoria(List<ItemDetalhe> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	@DisplayName("Busca por")
	public Boolean getRadBuscavencimento() {
		return radBuscavencimento;
	}

	public void setRadBuscavencimento(Boolean radBuscavencimento) {
		this.radBuscavencimento = radBuscavencimento;
	}

	public String getTiporelatorio() {
		return tiporelatorio;
	}

	public void setTiporelatorio(String tiporelatorio) {
		this.tiporelatorio = tiporelatorio;
	}

	public void setListaCaixa(List<ItemDetalhe> listaCaixa) {
		this.listaCaixa = listaCaixa;
	}

	public void setListaCartao(List<ItemDetalhe> listaCartao) {
		this.listaCartao = listaCartao;
	}

	public void setListaContabancaria(List<ItemDetalhe> listaContabancaria) {
		this.listaContabancaria = listaContabancaria;
	}

	public void setRadCaixa(Boolean radCaixa) {
		this.radCaixa = radCaixa;
	}

	public void setRadCartao(Boolean radCartao) {
		this.radCartao = radCartao;
	}

	public void setRadContbanc(Boolean radContbanc) {
		this.radContbanc = radContbanc;
	}
	
	@DisplayName("Sem V�nculo")
	public Boolean getRadSemVinculo() {
		return radSemVinculo;
	}

	public void setRadSemVinculo(Boolean radSemVinculo) {
		this.radSemVinculo = radSemVinculo;
	}

	public Empresa[] getEmpresas(){
		Empresa[] empresas = new Empresa[] {};
		if(listaEmpresa != null && !listaEmpresa.isEmpty()){
			Object[] objetos = CollectionsUtil.getListProperty(listaEmpresa, "empresa").toArray();
			empresas = new Empresa[objetos.length];
			for (int i = 0; i < objetos.length; i++) {
				empresas[i] = (Empresa) objetos[i];
			}
		}
		return empresas;
	}

	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	@DisplayName("Exibir contas a receber/pagar com adiantamento")
	public Boolean getExibirMovimentacaoSemVinculo() {
		return exibirMovimentacaoSemVinculo;
	}

	public void setExibirMovimentacaoSemVinculo(
			Boolean exibirMovimentacaoSemVinculo) {
		this.exibirMovimentacaoSemVinculo = exibirMovimentacaoSemVinculo;
	}
	
	@DisplayName("Exibir movimentacoes sem vinculo com conta a receber/pagar")
	public Boolean getExibirMovimentacaoSemVinculoDocumento() {
		return exibirMovimentacaoSemVinculoDocumento;
	}
	
	public void setExibirMovimentacaoSemVinculoDocumento(Boolean exibirMovimentacaoSemVinculoDocumento) {
		this.exibirMovimentacaoSemVinculoDocumento = exibirMovimentacaoSemVinculoDocumento;
	}
	
	public Boolean getCarregarMovimentacoes() {
		return carregarMovimentacoes;
	}
	
	public void setCarregarMovimentacoes(Boolean carregarMovimentacoes) {
		this.carregarMovimentacoes = carregarMovimentacoes;
	}
}
