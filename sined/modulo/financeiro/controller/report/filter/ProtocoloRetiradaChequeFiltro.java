package br.com.linkcom.sined.modulo.financeiro.controller.report.filter;

import java.util.List;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ProtocoloRetiradaChequeBean;


public class ProtocoloRetiradaChequeFiltro {
	
	public static final String TIPO_DECLARANTE_CLIENTE = "Cliente";
	public static final String TIPO_DECLARANTE_COLABORADOR = "Colaborador";
	public static final String TIPO_DECLARANTE_OUTRO = "Outro";
	
	private Empresa empresa;
	private String whereInCdmovimentacao;
	private String whereInCdcheque;
	private String referente;
	private String tipoDeclarante = TIPO_DECLARANTE_COLABORADOR;
	private Cliente cliente;
	private Colaborador colaborador;
	private String nomeDeclarante;
	private String empresaCheques;
	private String cidadeCheques;
	private List<ProtocoloRetiradaChequeBean> listaBean;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public String getWhereInCdmovimentacao() {
		return whereInCdmovimentacao;
	}
	public String getWhereInCdcheque() {
		return whereInCdcheque;
	}
	@Required
	@MaxLength(200)
	public String getReferente() {
		return referente;
	}
	@Required
	public String getTipoDeclarante() {
		return tipoDeclarante;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public String getNomeDeclarante() {
		return nomeDeclarante;
	}
	public String getDeclarante() {
		if (cliente!=null){
			return cliente.getNome();
		}else if (colaborador!=null){
			return colaborador.getNome();
		}else if (nomeDeclarante!=null){
			return nomeDeclarante;
		}else {
			return null;
		}
	}
	public String getEmpresaCheques() {
		return empresaCheques;
	}
	public String getCidadeCheques() {
		return cidadeCheques;
	}
	public String getWhereInCdchequeLista() {
		String whereInCdchequeLista = ""; 
		
		if (listaBean!=null){
			for (ProtocoloRetiradaChequeBean bean: listaBean){
				whereInCdchequeLista += bean.getCheque().getCdcheque().toString() + ", ";
			}
		}
		
		if (!whereInCdchequeLista.equals("")){
			whereInCdchequeLista = whereInCdchequeLista.substring(0, whereInCdchequeLista.length()-2);
		}
		
		return whereInCdchequeLista;
	}
	public List<ProtocoloRetiradaChequeBean> getListaBean() {
		return listaBean;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setWhereInCdmovimentacao(String whereInCdmovimentacao) {
		this.whereInCdmovimentacao = whereInCdmovimentacao;
	}
	public void setWhereInCdcheque(String whereInCdcheque) {
		this.whereInCdcheque = whereInCdcheque;
	}
	public void setReferente(String referente) {
		this.referente = referente;
	}
	public void setTipoDeclarante(String tipoDeclarante) {
		this.tipoDeclarante = tipoDeclarante;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setNomeDeclarante(String nomeDeclarante) {
		this.nomeDeclarante = nomeDeclarante;
	}
	public void setEmpresaCheques(String empresaCheques) {
		this.empresaCheques = empresaCheques;
	}
	public void setCidadeCheques(String cidadeCheques) {
		this.cidadeCheques = cidadeCheques;
	}
	public void setListaBean(List<ProtocoloRetiradaChequeBean> listaBean) {
		this.listaBean = listaBean;
	}	
}