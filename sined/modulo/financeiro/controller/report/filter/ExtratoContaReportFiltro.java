package br.com.linkcom.sined.modulo.financeiro.controller.report.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoExtratoContaReport;
import br.com.linkcom.sined.util.SinedDateUtils;

public class ExtratoContaReportFiltro {

	protected Empresa empresa;
	protected Contatipo contaTipo;
	protected Conta descricaoVinculo;
	protected Date dtInicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtFim = SinedDateUtils.lastDateOfMonth();
	protected List<Movimentacaoacao> listaMovimentacaoacao; 
	protected Boolean agruparPorDocumento;
	protected Boolean exibircontasvinculadas;
	protected Documentotipo documentotipo;
	protected OrdenacaoExtratoContaReport ordenacaoExtratoContaReport = OrdenacaoExtratoContaReport.DATA;
	protected Boolean ascReport = Boolean.TRUE;
	protected Centrocusto centrocusto;
	protected Boolean modoPaisagem;
	protected Boolean exibirRateio;
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Tipo de V�nculo")
	@Required
	public Contatipo getContaTipo() {
		return contaTipo;
	}
	@DisplayName("V�nculo")
	@Required
	public Conta getDescricaoVinculo() {
		return descricaoVinculo;
	}
	@DisplayName("Data in�cio")
	@Required
	public Date getDtInicio() {
		return dtInicio;
	}
	@DisplayName("Data fim")
	@Required
	public Date getDtFim() {
		return dtFim;
	}
	@DisplayName("Situa��o da movimenta��o")
	public List<Movimentacaoacao> getListaMovimentacaoacao() {
		return listaMovimentacaoacao;
	}
	@DisplayName("Agrupar por documento")
	public Boolean getAgruparPorDocumento() {
		return agruparPorDocumento;
	}
	@DisplayName("Exibir contas vinculadas")
	public Boolean getExibircontasvinculadas() {
		return exibircontasvinculadas;
	}
	@DisplayName("Tipo de Documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@DisplayName("Ordenar por")
	public OrdenacaoExtratoContaReport getOrdenacaoExtratoContaReport() {
		return ordenacaoExtratoContaReport;
	}
	@DisplayName("Tipo de Ordena��o")
	public Boolean getAscReport() {
		return ascReport;
	}
	@DisplayName("Centro de Custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Boolean getModoPaisagem() {
		return modoPaisagem;
	}
	@DisplayName("Exibir Rateio")
	public Boolean getExibirRateio() {
		return exibirRateio;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setContaTipo(Contatipo contaTipo) {
		this.contaTipo = contaTipo;
	}
	public void setDescricaoVinculo(Conta descricaoVinculo) {
		this.descricaoVinculo = descricaoVinculo;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setListaMovimentacaoacao(List<Movimentacaoacao> listaMovimentacaoacao) {
		this.listaMovimentacaoacao = listaMovimentacaoacao;
	}
	public void setAgruparPorDocumento(Boolean agruparPorDocumento) {
		this.agruparPorDocumento = agruparPorDocumento;
	}
	public void setExibircontasvinculadas(Boolean exibircontasvinculadas) {
		this.exibircontasvinculadas = exibircontasvinculadas;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setOrdenacaoExtratoContaReport(OrdenacaoExtratoContaReport ordenacaoExtratoContaReport) {
		this.ordenacaoExtratoContaReport = ordenacaoExtratoContaReport;
	}
	public void setAscReport(Boolean ascReport) {
		this.ascReport = ascReport;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setModoPaisagem(Boolean modoPaisagem) {
		this.modoPaisagem = modoPaisagem;
	}
	public void setExibirRateio(Boolean exibirRateio) {
		this.exibirRateio = exibirRateio;
	}
}