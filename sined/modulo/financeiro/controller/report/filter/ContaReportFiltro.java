package br.com.linkcom.sined.modulo.financeiro.controller.report.filter;

import java.sql.Date;
import java.util.Set;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Projeto;

public class ContaReportFiltro {

	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected Date dtvencimento1;
	protected Date dtvencimento2;
	protected Date dtemissao1;
	protected Date dtemissao2;
	protected String documento;
	protected Money valorminimo;
	protected Money valormaximo;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	protected Documentotipo documentotipo;
	protected Integer cddocumento; 
	protected Set<Documentoacao> listaDocumentoacao;
	protected String numero;
	
	public Cliente getCliente() {
		return cliente;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Date getDtvencimento1() {
		return dtvencimento1;
	}
	public Date getDtvencimento2() {
		return dtvencimento2;
	}
	public Date getDtemissao1() {
		return dtemissao1;
	}
	public Date getDtemissao2() {
		return dtemissao2;
	}
	@MaxLength(15)
	public String getDocumento() {
		return documento;
	}
	@DisplayName("")
	public Money getValorminimo() {
		return valorminimo;
	}
	public Money getValormaximo() {
		return valormaximo;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Conta Gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	@DisplayName("Centro de Custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	@DisplayName("Tipo de Documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	
	@DisplayName("Id")
	@MaxLength(10)
	public Integer getCddocumento() {
		return cddocumento;
	}
	
	@DisplayName("Situa��o")
	public Set<Documentoacao> getListaDocumentoacao() {
		return listaDocumentoacao;
	}
	
	@DisplayName("Documento")
	@MaxLength(50)
	public String getNumero() {
		return numero;
	}
	
	//============================================================
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public void setListaDocumentoacao(Set<Documentoacao> listaDocumentoacao) {
		this.listaDocumentoacao = listaDocumentoacao;
	}
	
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setDtvencimento1(Date dtvencimento1) {
		this.dtvencimento1 = dtvencimento1;
	}
	public void setDtvencimento2(Date dtvencimento2) {
		this.dtvencimento2 = dtvencimento2;
	}
	public void setDtemissao1(Date dtemissao1) {
		this.dtemissao1 = dtemissao1;
	}
	public void setDtemissao2(Date dtemissao2) {
		this.dtemissao2 = dtemissao2;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public void setValorminimo(Money valorminimo) {
		this.valorminimo = valorminimo;
	}
	public void setValormaximo(Money valormaximo) {
		this.valormaximo = valormaximo;
	}
}
