package br.com.linkcom.sined.modulo.financeiro.controller.report.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;


public class CopiaChequeReportFiltro extends FiltroListagemSined{

	private Money valormaximo;
	private Money valorminimo;
	private Empresa empresa;
	private Conta conta;
	private String numCheque;
	private Date dtInicio;
	private Date dtFinal;
	private Boolean ordenaMovDescricao;
	private Boolean ordenaTodosFornecedores;
	
	{
		this.dtInicio = SinedDateUtils.firstDateOfMonth();
		this.dtFinal = SinedDateUtils.lastDateOfMonth();
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Conta")
	public Conta getConta() {
		return conta;
	}

	@MaxLength(15)
	@DisplayName("N� cheque")
	public String getNumCheque() {
		return numCheque;
	}
	
	@DisplayName("Data da movimentacao")
	public Date getDtInicio() {
		return dtInicio;
	}
	
	public Date getDtFinal() {
		return dtFinal;
	}
	
	public Money getValormaximo() {
		return valormaximo;
	}
	@DisplayName("Valor")
	public Money getValorminimo() {
		return valorminimo;
	}
	
	public Boolean getOrdenaMovDescricao() {
		return ordenaMovDescricao;
	}
	
	public Boolean getOrdenaTodosFornecedores() {
		return ordenaTodosFornecedores;
	}
	
	public void setOrdenaMovDescricao(Boolean ordenaMovDescricao) {
		this.ordenaMovDescricao = ordenaMovDescricao;
	}
	
	public void setOrdenaTodosFornecedores(Boolean ordenaTodosFornecedores) {
		this.ordenaTodosFornecedores = ordenaTodosFornecedores;
	}
	
	public void setValormaximo(Money valormaximo) {
		this.valormaximo = valormaximo;
	}

	public void setValorminimo(Money valorminimo) {
		this.valorminimo = valorminimo;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setNumCheque(String numCheque) {
		this.numCheque = numCheque;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public void setDtFinal(Date dtFinal) {
		this.dtFinal = dtFinal;
	}
	
	

}
