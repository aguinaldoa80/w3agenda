package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.LinkedHashMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ChequemovimentacaoService;

@Controller(path = "/financeiro/relatorio/ChequeMovimentacaoTemplate", authorizationModule=ReportAuthorizationModule.class)
public class ChequeMovimentacaoTemplate extends ReportTemplateController<ReportTemplateFiltro> {
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.CHEQUE_MOVIMENTACAO;
	}

	@Override
	protected String getFileName() {
		return "emiss�o de cheque";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		String cdreporttemplate = request.getParameter("cdreporttemplate");
		if (StringUtils.isNotBlank(cdreporttemplate)) {
			ReportTemplateBean bean = new ReportTemplateBean(Integer.parseInt(cdreporttemplate));
			return getReportTemplateService().load(bean);
		} else {
			return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.CHEQUE_MOVIMENTACAO);
		}
	}
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext  request, ReportTemplateFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		String whereInCheque = request.getParameter("whereInCheque");
		
		ChequemovimentacaoService chequemovimentacaoService = new ChequemovimentacaoService();
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		mapa.put("lista", chequemovimentacaoService.geraListaChequeMovimentacaoBean(whereIn, whereInCheque, request.getParameter("order")));
		return mapa;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/suprimento/crud/Contapagar");
	}

}
