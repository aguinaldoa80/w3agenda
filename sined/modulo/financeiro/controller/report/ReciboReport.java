package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.utils.StringUtils;

@Controller(path = "/financeiro/relatorio/Recibo", authorizationModule=ReportAuthorizationModule.class)
public class ReciboReport extends ResourceSenderController<FiltroListagemSined>{

	private DocumentoService documentoService;
	private ParametrogeralService parametrogeralService;
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}

	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		boolean apenasBaixadas = this.parametrogeralService.getBoolean(Parametrogeral.RECIBO_APENASBAIXADAS);
		String whereIn = request.getParameter("selectedItens");
		String whereInMovimentacoes = request.getParameter("whereInMovimentacao");
		if(apenasBaixadas && documentoService.validaDocumentoSituacao(whereIn, true, Documentoacao.BAIXADA, Documentoacao.BAIXADA_PARCIAL)){
			request.addError("S� � permitido emitir recibo para contas com as situa��es Baixada ou Baixada Parcialmente.");
			return null;
		}
		
		if("receber".equals(request.getParameter("conta"))){
			String formasPagamento = StringUtils.limpaNull(this.parametrogeralService.buscaValorPorNome(Parametrogeral.RECIBO_FORMAPAGAMENTO));
			
			if(!formasPagamento.trim().equals("")){
				List<Documento> listaDocumento = documentoService.findForFormapagamento(whereIn, whereInMovimentacoes);
				List<String> listaFormasPermitidas = (List<String>)Arrays.asList(formasPagamento.split(";"));
				for(Documento doc: listaDocumento){
					for(Movimentacaoorigem movOrigem: doc.getListaMovimentacaoOrigem()){
						Formapagamento formapagamentoDocumento = movOrigem.getMovimentacao().getFormapagamento();

						boolean movimentacaoCancelada = movOrigem.getMovimentacao() != null &&
														Movimentacaoacao.CANCELADA.equals(movOrigem.getMovimentacao().getMovimentacaoacao());
						if(!movimentacaoCancelada && formapagamentoDocumento != null && formapagamentoDocumento.getCdformapagamento() != null){
							String cdformapagamentoStr = formapagamentoDocumento.getCdformapagamento().toString();
							if(!listaFormasPermitidas.contains(cdformapagamentoStr)){
								request.addError("A forma de pagamento da movimenta��o financeira n�o permite a impress�o do recibo.");
								return null;
							}
						}
					}
				}
			}			
		}

		return documentoService.createRecibo(request);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		if("pagar".equals(request.getParameter("conta"))){
			return new ModelAndView("redirect:/financeiro/crud/Contapagar" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cddocumento=" + request.getParameter("selectedItens") : ""));//			
		}else{
			return new ModelAndView("redirect:/financeiro/crud/Contareceber" + (request.getParameter("ENTRADA") != null &&  request.getParameter("ENTRADA").equals("true")? "?ACAO=consultar&cddocumento=" + request.getParameter("selectedItens") : ""));//	
		}
		
	}
}