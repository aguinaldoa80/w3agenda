package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Memorandoexportacao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.MemorandoexportacaoService;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/faturamento/relatorio/EmitirMemorandoexportacaomatricial", authorizationModule=ReportAuthorizationModule.class)
public class EmitirMemorandoexportacaoMatricialReport extends ResourceSenderController<Object> {
	MemorandoexportacaoService memorandoexportacaoService;
	public void setMemorandoexportacaoService(
			MemorandoexportacaoService memorandoexportacaoService) {
		this.memorandoexportacaoService = memorandoexportacaoService;
	}
	@Override
	public Resource generateResource(WebRequestContext request, Object filtro) throws Exception {
		String whereIn = request.getParameter("whereIn");
		String cdrepresentante = request.getParameter("representante");
		Colaborador representante = ColaboradorService.getInstance().carregaColaborador(cdrepresentante);
		List<Memorandoexportacao> listaMemorandoexportacao = memorandoexportacaoService.loadForMemorando(whereIn);
		String memorando = memorandoexportacaoService.createReportMatricial(listaMemorandoexportacao, representante);
		Resource resource = new Resource();
		resource.setContentType("application/w3erp");
		resource.setFileName("memorando_exportacao_" + SinedUtil.datePatternForReport() + ".w3erp");
	    resource.setContents(memorando.getBytes());
	    
		return resource;	
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Object filtro)
			throws Exception {		
		return null;
	}



}
