package br.com.linkcom.sined.modulo.financeiro.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContagerencialFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/Contagerencial", 
			authorizationModule=ReportAuthorizationModule.class)
public class ContagerencialReport extends SinedReport<ContagerencialFiltro>{

	protected ContagerencialService contagerencialService;
	
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	
	
	@Override
	public IReport createReportSined(WebRequestContext request, ContagerencialFiltro filtro) throws Exception {
		return contagerencialService.createRelatorioContagerencial(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "contagerencial";
	}

	@Override
	public String getTitulo(ContagerencialFiltro filtro) {
		return "RELATÓRIO DE CONTAS GERENCIAIS";
	}

}
