package br.com.linkcom.sined.modulo.financeiro.controller.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.SinedReport;


@Controller(path = "/financeiro/relatorio/AnaliseReceitaDespesa", authorizationModule=ReportAuthorizationModule.class)
public class AnaliseReceitaDespesaReport extends SinedReport<AnaliseReceitaDespesaReportFiltro>{
	
	private ContagerencialService contagerencialService;
	private DocumentoService documentoService;
	private DocumentoacaoService documentoacaoService;
	private ContaService contaService;
	
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setDocumentoacaoService(DocumentoacaoService documentoacaoService) {this.documentoacaoService = documentoacaoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			AnaliseReceitaDespesaReportFiltro filtro)
			throws ResourceGenerationException {	
		AnaliseReceitaDespesaReportFiltro filtroExistente = (AnaliseReceitaDespesaReportFiltro)NeoWeb.getRequestContext().getSession().getAttribute(ContagerencialService.FILTRO_SESSION);
		if("true".equals(request.getParameter("origemProcessAnaliseRecDesp")) && filtroExistente!=null){
			return super.doFiltro(request, filtroExistente);			
		}
		return super.doFiltro(request, filtro);
	}
	@Override
	protected void filtro(WebRequestContext request, AnaliseReceitaDespesaReportFiltro filtro) throws Exception {
		super.filtro(request, filtro);
		request.setAttribute("listaTipopagamentoPag", documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_DEBITO));
		request.setAttribute("listaTipopagamentoRec", documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_CREDITO));
		request.setAttribute("listaProjetoitem", getProjetoService().getListaProjetoFiltro()); 
		request.setAttribute("listaSituacaoDocumentos", documentoacaoService.findAcoesForDocumento());
		
		request.setAttribute("listaCaixa", contaService.findByTipo(Contatipo.TIPO_CAIXA));
		request.setAttribute("listaCartaocredito", contaService.findByTipo(Contatipo.TIPO_CARTAO_CREDITO));
		request.setAttribute("listaContabancaria", contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA));
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, AnaliseReceitaDespesaReportFiltro filtro) throws Exception {
		if(filtro.getTiporelatorio() != null && filtro.getTiporelatorio().equals("analitico")){
			contagerencialService.ajustaListasAnaliseReceitaDespesaFiltro(filtro);
			return contagerencialService.gerarRelatorioAnaliticoAnaliseReceitaDespesa(filtro);
		}
		contagerencialService.ajustaListasAnaliseReceitaDespesaFiltro(filtro);
		return contagerencialService.gerarRelatorioAnaliseReceitaDespesa(filtro);
	}
	
	@Override
	public String getTitulo(AnaliseReceitaDespesaReportFiltro filtro) {
		return "AN�LISE DE RECEITAS E DESPESAS";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, AnaliseReceitaDespesaReportFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		if (filtro.getRadProjeto().getId().equals("comProjeto")) {
			report.addParameter("projeto", "Apenas com projeto");
		} else if (filtro.getRadProjeto().getId().equals("semProjeto")) {
			report.addParameter("projeto", "Apenas sem projeto");
		}
		
		/**
		 * Seleciona a logo da empresa de acordo com a lista de empresas escolhidas
		 * Se n�o tiver escolhido: vai a principal
		 * Se escolher uma: vale ela
		 * Se escolher mais de uma: vale a principal ou a primeira da lista.
		 */
		Empresa empresa;
		if(filtro.getListaEmpresa() == null || filtro.getListaEmpresa().isEmpty()){
			empresa = empresaService.loadPrincipal();
		}else if(filtro.getListaEmpresa().size() == 1){
			empresa = filtro.getListaEmpresa().get(0).getEmpresa();
		}else{
			Empresa principal = empresaService.loadPrincipal();
			empresa = filtro.getListaEmpresa().get(0).getEmpresa();
			for(ItemDetalhe item : filtro.getListaEmpresa()){
				if(item.getEmpresa().equals(principal)){
					empresa = principal;
					break;
				}
			}
		}
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		
		String caixa = "Desconsiderada";
		String cartao = "Desconsiderado";
		String contaBancaria = "Desconsiderada";
		
		if (filtro.getRadCaixa() != null) {
			caixa = contaService.getNomeContas(filtro.getListaCaixa());
		}
		if (filtro.getRadCartao() != null) {
			cartao = contaService.getNomeContas(filtro.getListaCartao());
		}
		if (filtro.getRadContbanc() != null) {
			contaBancaria = contaService.getNomeContas(filtro.getListaContabancaria());
		}
		
		report.addParameter("caixa", caixa);
		report.addParameter("cartao", cartao);
		report.addParameter("contaBancaria", contaBancaria);
	}
	
	public ModelAndView mostrarFlex(WebRequestContext request, AnaliseReceitaDespesaReportFiltro filtro) throws Exception {
		contagerencialService.ajustaListasAnaliseReceitaDespesaFiltro(filtro);
		
		//COLOCADO ISSO POIS NA CONVERS�O PARA O FLEX ESTAVA ATRASANDO UM DIA.
		filtro.setDtInicio(SinedDateUtils.setDateProperty(filtro.getDtInicio(), 12, Calendar.HOUR_OF_DAY));
		
		request.getSession().setAttribute(ContagerencialService.FILTRO_SESSION, filtro);
		AnaliseRecDespBean analiseBean = contagerencialService.findForGraficoDistribuicaoReceitaDespesaFlex(filtro.getDtInicio(), filtro.getDtFim());
		List<AnaliseRecDespBean> listaFilha = analiseBean.getListaFilha();
		if(SinedUtil.isListNotEmpty(listaFilha)){
			desbloquearTela(request);
			request.setAttribute("lista", listaFilha);
			
			Money totalCredito = new Money();
			Money totalDebito = new Money();
			
			for (AnaliseRecDespBean analiseRecDespBean : listaFilha) {
				if (analiseRecDespBean.getOperacao() != null && analiseRecDespBean.getValor() != null) {
					if ("C".equals(analiseRecDespBean.getOperacao())) {
						totalCredito = totalCredito.add(analiseRecDespBean.getValor());
					} else if ("D".equals(analiseRecDespBean.getOperacao())) {
						totalDebito = totalDebito.add(analiseRecDespBean.getValor());
					}
				}
			}
			
			request.setAttribute("totalCredito", totalCredito);
			request.setAttribute("totalDebito", totalDebito);
			
			Money saldo = totalCredito.subtract(totalDebito);
			
			request.setAttribute("saldo", saldo);
			
			return new ModelAndView("/process/analiseReceitaDespesaVisualizar", "filtro" ,filtro);			
		}else{
			request.addError("Nenhum resultado encontrado.");
			return new ModelAndView("redirect:/financeiro/relatorio/AnaliseReceitaDespesa");
		}
	}
	
	@Override
	public String getNomeArquivo() {
		return "analiseReceitaDespesa";
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	public ModelAndView ajaxComboProjeto(WebRequestContext request, AnaliseReceitaDespesaReportFiltro filtro) throws Exception {
		String cdsprojetotipo = request.getParameter("cdsprojetotipo");
		List<Projeto> listaProjeto;
		
		if (cdsprojetotipo.trim().equals("")){
			listaProjeto = projetoService.findAll();
		}else {
			listaProjeto = projetoService.findByCdsprojetotipo(cdsprojetotipo);
		}
		
		List<GenericBean> listaGenericBean = new ArrayList<GenericBean>();
		
		for (Projeto projeto: listaProjeto){
			listaGenericBean.add(new GenericBean(projeto.getCdprojeto(), projeto.getNome().replace("'", "''")));
		}		
		
		return new JsonModelAndView().addObject("listaProjetoBean", listaGenericBean);
	}
}