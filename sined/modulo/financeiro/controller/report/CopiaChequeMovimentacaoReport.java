package br.com.linkcom.sined.modulo.financeiro.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.CopiaChequeReportFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/financeiro/relatorio/CopiaChequeMovimentacao", authorizationModule=ReportAuthorizationModule.class)
public class CopiaChequeMovimentacaoReport extends SinedReport<CopiaChequeReportFiltro> {

	private MovimentacaoService movimentacaoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, CopiaChequeReportFiltro filtro) throws Exception {		
		return movimentacaoService.createCopiaChequeReport(request);
	}

	@Override
	public String getTitulo(CopiaChequeReportFiltro filtro) {
		return null;
	}
	
	@Override
	protected ModelAndView getFiltroModelAndView(WebRequestContext request, CopiaChequeReportFiltro filtro) {
		return new ModelAndView("redirect:/financeiro/crud/Movimentacao");
	}
	
	@Override
	public String getNomeArquivo() {
		return "copiachequemovimentacao";
	}

}
