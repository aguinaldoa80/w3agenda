package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivoconciliacaooperadora;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.service.ArquivoconciliacaooperadoraService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.ArquivoconciliacaooperadoraFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/financeiro/crud/Arquivoconciliacaooperadora",authorizationModule=ProcessAuthorizationModule.class)
public class ArquivoconciliacaooperadoraCrud extends CrudControllerSined<ArquivoconciliacaooperadoraFiltro, Arquivoconciliacaooperadora, Arquivoconciliacaooperadora> {

	private ArquivoconciliacaooperadoraService arquivoconciliacaooperadoraService;
	private ContareceberService contareceberService;
	private MovimentacaoService movimentacaoService;
	
	public void setArquivoconciliacaooperadoraService(ArquivoconciliacaooperadoraService arquivoconciliacaooperadoraService) {
		this.arquivoconciliacaooperadoraService = arquivoconciliacaooperadoraService;
	}
	
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
    
	@Override
    protected ListagemResult<Arquivoconciliacaooperadora> getLista(WebRequestContext request, ArquivoconciliacaooperadoraFiltro filtro) {
		ListagemResult<Arquivoconciliacaooperadora> lista = super.getLista(request, filtro);
		List<Arquivoconciliacaooperadora> listaBean = lista.list();
		Double valorTotalVendas = new Double(0);
		Double valorTotalMovimentacoes = new Double(0);
		Double valorTotalCalculado = new Double(0);
    	for (Arquivoconciliacaooperadora arquivoconciliacaooperadora : listaBean) {
    		if(arquivoconciliacaooperadora.getDocumento() != null && arquivoconciliacaooperadora.getDocumento().getValor() != null){
				valorTotalVendas += arquivoconciliacaooperadora.getDocumento().getValor().getValue().doubleValue();
			}
    		if (arquivoconciliacaooperadora != null 
    				&& arquivoconciliacaooperadora.getDocumento() != null 
    				&& arquivoconciliacaooperadora.getDocumento().getCddocumento() != null
    				&& arquivoconciliacaooperadora.getDocumento().getDocumentoacao() != null
    				&& arquivoconciliacaooperadora.getDocumento().getDocumentoacao().equals(Documentoacao.BAIXADA)) {
    			Movimentacao movimentacao = movimentacaoService.findByDocumento(arquivoconciliacaooperadora.getDocumento());
    			arquivoconciliacaooperadora.setMovimentacao(movimentacao);
    			if(arquivoconciliacaooperadora.getMovimentacao() != null && arquivoconciliacaooperadora.getMovimentacao().getValor() != null){
    				valorTotalMovimentacoes += arquivoconciliacaooperadora.getMovimentacao().getValor().getValue().doubleValue();
    			}
    		}
    		if(arquivoconciliacaooperadora.getMoneyvalorliquidocalculado() != null){
				valorTotalCalculado += arquivoconciliacaooperadora.getMoneyvalorliquidocalculado().getValue().doubleValue();
			}
		}
    	filtro.setValortotalvenda(new Money (valorTotalVendas.doubleValue()));
		filtro.setValortotalmovimentacao(new Money (valorTotalMovimentacoes.doubleValue()));
		filtro.setValortotalcalculado(new Money (valorTotalCalculado.doubleValue()));
    	return lista;
    }
    
    public ModelAndView abrirPopUpAssociacaoArquivoOperadora(WebRequestContext request){    	
    	String cdarquivoconciliacaooperadora = request.getParameter("cdarquivoconciliacaooperadora");
    	Integer cdarquivoconciliacaooperadoraInteger = null;
    	if (StringUtils.isNotBlank(cdarquivoconciliacaooperadora)) {
    		cdarquivoconciliacaooperadoraInteger = Integer.valueOf(cdarquivoconciliacaooperadora);
		}
    	Arquivoconciliacaooperadora arquivoconciliacaooperadora = arquivoconciliacaooperadoraService.findArquivoForAssociacaoArquivoOperadora(cdarquivoconciliacaooperadoraInteger);
    	List<Documento> lista = contareceberService.findListaDocumentosForAssociacaoArquivoOperadora(arquivoconciliacaooperadora);
    	request.setAttribute("lista", lista);
    	request.setAttribute("cdarquivoconciliacaooperadora", arquivoconciliacaooperadora.getCdarquivoconciliacaooperadora());
    	return new ModelAndView("direct:/process/popup/arquivoconciliacaooperadoraAssociacao");
    }
    
    public void associar(WebRequestContext request){
    	String cdarquivoconciliacaooperadora = request.getParameter("cdarquivoconciliacaooperadora");
    	String cddocumento = request.getParameter("cddocumento");
    	if (StringUtils.isNotBlank(cdarquivoconciliacaooperadora) && StringUtils.isNotBlank(cddocumento)) {
    		arquivoconciliacaooperadoraService.associar(cdarquivoconciliacaooperadora, cddocumento);
    		request.addMessage("Associação realizada com sucesso.");
		} else {
			request.addError("Nenhuma associação realizada.");
		}
    }
    
    public void conciliar(WebRequestContext request){
    	String cdarquivoconciliacaooperadora = request.getParameter("cdarquivoconciliacaooperadora");
    	if (StringUtils.isNotBlank(cdarquivoconciliacaooperadora)) {
    		arquivoconciliacaooperadoraService.conciliar(cdarquivoconciliacaooperadora);
    		request.addMessage("Conciliação realizada com sucesso.");
    	} else {
    		request.addError("Erro ao realizar Conciliação.");
    	}
    }
}
