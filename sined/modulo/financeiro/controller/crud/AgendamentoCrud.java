package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.AgendamentoHistorico;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateiomodelo;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipovinculo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_agendamento;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.state.AgendamentoAcao;
import br.com.linkcom.sined.geral.service.AgendamentoHistoricoService;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ConfiguracaoFolhaService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContatipoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FormapagamentoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.FrequenciaService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoorigemService;
import br.com.linkcom.sined.geral.service.OperacaocontabilService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.geral.service.RateiomodeloService;
import br.com.linkcom.sined.geral.service.TipooperacaoService;
import br.com.linkcom.sined.geral.service.TipovinculoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.AgendamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/financeiro/crud/Agendamento", authorizationModule=CrudAuthorizationModule.class)
public class AgendamentoCrud extends CrudControllerSined<AgendamentoFiltro, Agendamento, Agendamento> {

	private AgendamentoService agendamentoService;
	private ContaService contaService;
	private TipovinculoService tipovinculoService;
	private AgendamentoHistoricoService agendamentoHistoricoService;
	private RateioService rateioService;
	private RateioitemService rateioitemService;
	private ContatipoService contatipoService;
	private ParametrogeralService parametrogeralService;
	private CentrocustoService centrocustoService;
	private EmpresaService empresaService;
	private DocumentoService documentoService;
	private ProjetoService projetoService;
	private FrequenciaService frequenciaService;
	private MovimentacaoService movimentacaoService;
	private DocumentotipoService documentotipoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ContagerencialService contagerencialService;
	private OperacaocontabilService operacaocontabilService;
	private MovimentacaoorigemService movimentacaoorigemService;
	private TipooperacaoService tipooperacaoService;
	private RateiomodeloService rateiomodeloService;
	private ConfiguracaoFolhaService configuracaoFolhaService; 
	
	public void setFrequenciaService(FrequenciaService frequenciaService) {
		this.frequenciaService = frequenciaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setTipovinculoService(TipovinculoService tipovinculoService) {
		this.tipovinculoService = tipovinculoService;
	}
	public void setAgendamentoHistoricoService(AgendamentoHistoricoService agendamentoHistoricoService) {
		this.agendamentoHistoricoService = agendamentoHistoricoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setContatipoService(ContatipoService contatipoService) {
		this.contatipoService = contatipoService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setOperacaocontabilService(OperacaocontabilService operacaocontabilService) {
		this.operacaocontabilService = operacaocontabilService;
	}
	public void setMovimentacaoorigemService(MovimentacaoorigemService movimentacaoorigemService) {
		this.movimentacaoorigemService = movimentacaoorigemService;
	}
	public void setTipooperacaoService(TipooperacaoService tipooperacaoService) {
		this.tipooperacaoService = tipooperacaoService;
	}
	public void setRateiomodeloService(RateiomodeloService rateiomodeloService) {
		this.rateiomodeloService = rateiomodeloService;
	}
	public void setConfiguracaoFolhaService(ConfiguracaoFolhaService configuracaoFolhaService) {
		this.configuracaoFolhaService = configuracaoFolhaService;
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Agendamento bean) {
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, AgendamentoFiltro filtro) throws CrudException {
		if (filtro.getLimparFiltro()) {
			filtro = new AgendamentoFiltro();
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Agendamento bean)throws Exception {
		if(bean.getCdagendamento() == null && !isAlowToSaveBean(request,bean)){
			throw new SinedException("N�o � poss�vel salvar o mesmo registro novamente.");
		}
		bean.ajustaBeanToSave();
		super.salvar(request, bean);
	}
	
	
	
	
	private boolean isAlowToSaveBean(WebRequestContext request, Agendamento bean) {
		Agendamento save = (Agendamento) request.getSession().getAttribute(bean.getClass().getName());
		if(save != null && save.equalsToSave(bean)){
			return false;
		}
		request.getSession().setAttribute(bean.getClass().getName(), bean);
		return true;
	}
	
	/**
	 * Apenas coloca na requisi��o os valores padr�o dos campos ativo, dtInicio e dtFim do filtro.
	 * 
	 * @author Hugo Ferreira
	 */
	@Override
	protected void listagem(WebRequestContext request, AgendamentoFiltro filtro) throws Exception {
		Integer limiteInicial = parametrogeralService.getPositiveInteger(Parametrogeral.LI_CONSOLIDAR_AGENDAMENTO);
		Date hoje = new Date(System.currentTimeMillis());

		//Monta o campo do filtro "Situa��o"
		List<SituacaoAgendamento> listaSituacao = new ArrayList<SituacaoAgendamento>();
		for (SituacaoAgendamento situacaoAgendamento : SituacaoAgendamento.values()) {
			listaSituacao.add(situacaoAgendamento);
		}
		//Fim

		request.setAttribute("ativo", true);
		request.setAttribute("dtVencimentoInicio", SinedDateUtils.toString(hoje)); //Usado para setar o valor padr�o do filtro ao clicar em 'Reiniciar'
		request.setAttribute("dtVencimentoFim", SinedDateUtils.toString(SinedDateUtils.incrementDate(hoje, limiteInicial, Calendar.DAY_OF_MONTH))); //Usado para setar o valor padr�o do filtro ao clicar em 'Reiniciar'
		request.setAttribute("listaSituacao", listaSituacao);
		
		request.setAttribute("listaCredito", SinedUtil.convertEnumToJavaScript(
				documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_CREDITO, true)));
		request.setAttribute("listaDebito", SinedUtil.convertEnumToJavaScript(
				documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_DEBITO, true)));
		request.setAttribute("listaCompleta", SinedUtil.convertEnumToJavaScript(
				documentoService.findTipopagamentoForDocumento(null, true)));
		request.setAttribute("listaTipopagamento", documentoService.findTipopagamentoForDocumento(filtro.getTipooperacao(), true));
		request.setAttribute("labelPessoa", filtro.getTipopagamento());
		request.setAttribute("listaTipooperacao", tipooperacaoService.findDebitoCredito());

	}
	
	/**
	 * <p>Faz ajustes de valores padr�o e transientes na tela nas a��es EDITAR e CRIAR.</p>
	 * <p>Os ajustes s�o: </p>
	 * 
	 * <ul>
	 * 	<li> Seta o valor padr�o do campo 'frequencia'
	 * 	<li> Marca o campo "Este agendamento tem prazo para t�rminio" de acordo com o valor das propriedades
	 * 		 'frequencia' e 'dtFim'
	 * 	<li> Coloca a lista de Conta gerencial na requisi��o
	 * 	<li> Se houverem erros no binder, toma atitudes para que a tela n�o volte com valores errados ou que oculte bot�es.
	 * 	<li> Adiciona na requisi��o uma lista de 'Conta banc�ria'
	 * 	<li> Adiciona na requisi��o uma lista de 'Caixa'
	 * 	<li> Adiciona na requisi��o uma lista de 'Cart�o de cr�dito'
	 * </ul>
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findAnaliticasDebito()
	 * @see br.com.linkcom.sined.geral.service.TipovinculoService#buscarPor(Tipooperacao)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoHistoricoService#carregarListaHistorico(Agendamento)
	 * @see br.com.linkcom.sined.geral.service.RateioService#calculaValoresRateio(Rateio, Money)
	 * @see br.com.linkcom.sined.geral.service.ContaService#findByTipo(Contatipo)
	 * @param request
	 * @param form
	 * @author Hugo Ferreira
	 */
	
	
	public ModelAndView doEditar(WebRequestContext request, Agendamento form) throws CrudException {
		Integer finalizado = SituacaoAgendamento.FINALIZADO.getValue();
		form = agendamentoService.load(form);		
		if(request.getParameter("ACAO") !=null && request.getParameter("ACAO").equals("editar")){ 
			if(form.getAux_agendamento() !=null && form.getAux_agendamento().getSituacao() !=null){
				if(form.getAux_agendamento().getSituacao().equals(finalizado)){
					request.addError("N�o � poss�vel editar um agendamento finalizado");
					return sendRedirectToAction("listagem");
				}
			}
		}
		return super.doEditar(request, form);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Agendamento form) throws Exception {
		//seta o valor padr�o do campo frequ�ncia. N�o funciona na a��o CRIAR porque
		//na chamada do super dessa a��o � instanciado um novo form de Agendamento.
		List<Centrocusto> listaCentroCusto = null;
		
			
		if (form.getCdagendamento() == null) {
			request.setAttribute("ACAO", "criar");
		} else {
			if (form.getTipovinculo() != null) {
				Integer tipovinculo = form.getTipovinculo().getCdtipovinculo();
				if (tipovinculo.equals(Tipovinculo.CREDITO_LANCAMENTO_EM_CONTA_BANCARIA) || tipovinculo.equals(Tipovinculo.DEBITO_LANCAMENTO_EM_CONTA_BANCARIA)) {
					form.setContaBancaria(form.getConta());
				} else if (tipovinculo.equals(Tipovinculo.CREDITO_LANCAMENTO_EM_CAIXA) || tipovinculo.equals(Tipovinculo.DEBITO_LANCAMENTO_EM_CAIXA)) {
					form.setCaixa(form.getConta());
				} else if (tipovinculo.equals(Tipovinculo.DEBITO_LANCAMENTO_EM_CARTAO_DE_CREDITO)) {
					form.setCartaoCredito(form.getConta());
				}
			}
			
			if(form.getAux_agendamento()!=null && form.getAux_agendamento().getSituacao()!=null){
				SituacaoAgendamento[] agendamentos = SituacaoAgendamento.values();
				for (SituacaoAgendamento situacaoAgendamento : agendamentos) {
					if(situacaoAgendamento.getValue() == form.getAux_agendamento().getSituacao()){
						form.setSituacao(situacaoAgendamento);
						break;
					}
				}
			}
			
			if (form.getEmpresa() != null && form.getEmpresa().getCdpessoa() != null){
				form.setEmpresa(empresaService.load(form.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia"));
			}

			listaCentroCusto = (List<Centrocusto>) CollectionsUtil.getListProperty(form.getRateio().getListaRateioitem(), "centrocusto");

			List<AgendamentoHistorico> listaHistorico = agendamentoHistoricoService.carregarListaHistorico(form);
			form.setListaAgendamentoHistorico(new ListSet<AgendamentoHistorico>(AgendamentoHistorico.class, listaHistorico));
			request.setAttribute("ACAO", "editar");
		}
		String cddocumentotipo = form.getCdagendamento() != null && form.getDocumentotipo() != null && form.getDocumentotipo().getCddocumentotipo() != null? form.getDocumentotipo().getCddocumentotipo().toString(): "";
		if(Tipooperacao.TIPO_CREDITO.equals(form.getTipooperacao())){
			request.setAttribute("listaDocumentoTipo", documentotipoService.getListaDocumentoTipoUsuarioForVenda(cddocumentotipo));
		}else if(Tipooperacao.TIPO_DEBITO.equals(form.getTipooperacao())){
			request.setAttribute("listaDocumentoTipo", documentotipoService.getListaDocumentoTipoUsuarioForCompra(cddocumentotipo));
		}
		
		request.setAttribute("isFinalizado", false);
		if(form.getSituacao() != null && form.getSituacao().equals(SituacaoAgendamento.FINALIZADO)){
			request.setAttribute("isFinalizado", true);
		}
		
		//Seta o campo 'Este agendamento tem prazo para t�rminio'
		if (!form.getFrequencia().getCdfrequencia().equals(Frequencia.UNICA) && form.getIteracoes()!=null && form.getDtfim() != null) {
			form.setAgendamentoTemPrazoParaTerminio(true);
		}
		Tipooperacao tipooperacao = null;
		if(Tipooperacao.TIPO_CREDITO.equals(form.getTipooperacao())){
			tipooperacao = Tipooperacao.TIPO_CREDITO;
			
		}else if(Tipooperacao.TIPO_DEBITO.equals(form.getTipooperacao())){
			tipooperacao = Tipooperacao.TIPO_DEBITO;
		}else {
			tipooperacao = Tipooperacao.TIPO_DEBITO;
		}
		request.setAttribute("tipooperacao", tipooperacao);
		
		List<Rateiomodelo> listaModeloRateio = rateiomodeloService.findBy(tipooperacao, true);
		request.setAttribute("listaModeloRateio", listaModeloRateio);
 
		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getCdagendamento() != null && form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
			List<Projeto> projetos = rateioitemService.getProjetos(form.getRateio().getListaRateioitem());
			listaProjeto = projetoService.findProjetosAbertosSemPermissao(projetos, true, true);
		} else {
			listaProjeto = projetoService.findProjetosAbertos(null, true, true);
		}
		request.setAttribute("listaProjetosEmAndamento", listaProjeto);
		
		if(form.getTipooperacao() != null && 
				form.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO) && 
				form.getTipopagamento() != null &&
				form.getTipopagamento().equals(Tipopagamento.FORNECEDOR)){
			form.setTipopagamento(Tipopagamento.CLIENTE);
		}

		request.setAttribute("listaFrequencia", frequenciaService.findForAgendamentoContrato());
		request.setAttribute("listaTipopagamento", documentoService.findTipopagamentoForDocumento(form.getTipooperacao(), true));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(listaCentroCusto));
		request.setAttribute("listaTipoVinculo", tipovinculoService.buscarPor(form.getTipooperacao()));
		request.setAttribute("listaContaBancaria", contaService.findByTipo(new Contatipo(Contatipo.CONTA_BANCARIA)));
		request.setAttribute("listaCaixa", contaService.findByTipo(new Contatipo(Contatipo.CAIXA)));
		request.setAttribute("listaCartaoCredito", contaService.findByTipo(new Contatipo(Contatipo.CARTAO_DE_CREDITO)));
		
		List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
		listaDocumentoacao.add(Documentoacao.PREVISTA);
		listaDocumentoacao.add(Documentoacao.DEFINITIVA);
		request.setAttribute("listaDocumentoacao", listaDocumentoacao);
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}	
		
		if (Tipopagamento.COLABORADOR.equals(form.getTipopagamento())){
			form.setFornecedor(null);
			form.setCliente(null);
			form.setOutrospagamento(null);
		}else if (Tipopagamento.FORNECEDOR.equals(form.getTipopagamento())){
			form.setColaborador(null);
			form.setCliente(null);
			form.setOutrospagamento(null);
		}else if (Tipopagamento.CLIENTE.equals(form.getTipopagamento())){
			form.setColaborador(null);
			form.setFornecedor(null);
			form.setOutrospagamento(null);
		}else if (Tipopagamento.OUTROS.equals(form.getTipopagamento())){
			form.setColaborador(null);
			form.setFornecedor(null);
			form.setCliente(null);
		}
		
		if(form.getFornecedor() != null && form.getFornecedor().getCdpessoa() != null){
			form.setFornecedor(FornecedorService.getInstance().load(form.getFornecedor()));
		}
		if(form.getCliente() != null && form.getCliente().getCdpessoa() != null){
			form.setCliente(ClienteService.getInstance().load(form.getCliente()));
		}
		if(form.getColaborador() != null && form.getColaborador().getCdpessoa() != null){
			form.setColaborador(ColaboradorService.getInstance().load(form.getColaborador()));
		}
		
		
		
//		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		request.setAttribute("integracao_contabil", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INTEGRACAO_CONTABIL)));
		request.setAttribute("listaTipooperacao", tipooperacaoService.findDebitoCredito());
	}

	/**
	 * <p>Faz ajustes de valores padr�o e transientes na tela quando a a��o for EDITAR um registro.</p>
	 * <p>Os ajustes s�o: </p>
	 * 
	 * <ul>
	 * 	<li> Recupera a lista de rateios referente ao agendamento atual (RateioItem)
	 * 	<li> Calcula o valor rateado
	 * 	<li> Recupera o hist�rico do agendamento corrente
	 * </ul>
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#findByRateio(Rateio)
	 * @see br.com.linkcom.sined.geral.service.AgendamentoHistoricoService#carregarListaHistorico(Agendamento)
	 * @see br.com.linkcom.sined.geral.service.RateioService#calculaValoresRateio(Rateio, Money)
	 * @param request
	 * @param bean
	 * @author Hugo Ferreira
	 */
	
	
	@Override
	protected Agendamento carregar(WebRequestContext request, Agendamento bean) throws Exception {
		bean = super.carregar(request, bean);
		
		if(bean == null){
			throw new SinedException("Registro n�o encontrado no sistema");
		}

		if(bean.getCdagendamento() != null){
			Agendamento aux = agendamentoService.loadWithPessoa(bean);
			if(aux != null){
				if(aux.getEmpresa() != null)
					bean.setEmpresa(aux.getEmpresa());
				if(aux.getPessoa() != null)
					bean.setPessoa(aux.getPessoa());
			}
		}
		
		bean.ajustaBeanToLoad();

		bean.getRateio().setListaRateioitem(rateioitemService.findByRateio(bean.getRateio()));

		//Carrega o hist�rico separadamente para que venha ordenado pelo �ltimo evento
		List<AgendamentoHistorico> listaHistorico = agendamentoHistoricoService.carregarListaHistorico(bean);
		bean.setListaAgendamentoHistorico(new ListSet<AgendamentoHistorico>(AgendamentoHistorico.class, listaHistorico));
		rateioService.calculaValoresRateio(bean.getRateio(), bean.getValor());

		
		
		return bean;
		
		
	}
	
	

	@Override
	protected void validateBean(Agendamento bean, BindException errors) {
		Calendar hoje = SinedDateUtils.javaSqlDateToCalendar(new Date(System.currentTimeMillis()));
		Calendar dtProximo = SinedDateUtils.javaSqlDateToCalendar(bean.getDtproximo());
		
		if (bean.getCdagendamento() != null) {
			
			Agendamento agenda = agendamentoService.findByCdAgendamento(bean.getCdagendamento());
				if(agenda.getAux_agendamento()!=null && agenda.getAux_agendamento().getSituacao() !=null
					&&	agenda.getAux_agendamento().getSituacao() == SituacaoAgendamento.FINALIZADO.getValue()){
					errors.reject("003", "Agendamento finalizado");
				}
			}
		
		if (dtProximo != null && dtProximo.before(hoje)) {
			errors.reject("002", "O campo pr�ximo vencimento n�o pode ser anterior a data atual.");
		}

		if (bean.getValor().compareTo(new Money()) > 0) {
			errors.reject("001", "O campo valor n�o pode ser negativo");
		}
		try {
			rateioService.validateRateio(bean.getRateio(), bean.getValor());
		} catch (SinedException e) {
			errors.reject("001",e.getMessage());
		}
	}

	@Override
	protected void validateFilter(AgendamentoFiltro filtro, BindException errors) {
		
		if (filtro.getDtVencimentoInicio() != null && filtro.getDtVencimentoFim() != null) {
			if (filtro.getDtVencimentoInicio().after(filtro.getDtVencimentoFim())) {
				errors.reject("001", "O valor inicial campo 'Pr�ximo vencimento' � maior que o valor final.");
			}
		}

		if (filtro.getValorInicio() != null && filtro.getValorFim() != null) {
			if (filtro.getValorInicio().compareTo(filtro.getValorFim()) < 0) {
				errors.reject("001", "O valor inicial do campo 'Valor' � maior que o valor final.");
			}
		}

	}

	/**
	 * <p>Action de Ajax respons�vel por atualizar os valores do combo 'tipovinculo' do jsp.</p>
	 * 
	 * <p>Recebe um form de Agendamento com a propriedade 'tipoOperacao' setada. A partir desta
	 * propriedade, carrega uma lista de TipoVinculo, transforma esta lista em elementos de
	 * um combo e envia esses elementos de volta ao cliente.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.TipovinculoService#buscarPor(Tipooperacao)
	 * @see br.com.linkcom.sined.util.SinedUtil#convertToJavaScript(List, String, String)
	 * @param request
	 * @param form bean da classe Agendamento com a propriedade 'tipoOperacao' setada.
	 * @author Hugo Ferreira
	 */
	public void ajaxComboVinculo(WebRequestContext request, Agendamento form) {
		List<Tipovinculo> listaTipoVinculo = tipovinculoService.buscarPor(form.getTipooperacao());
		String valoresCombo = SinedUtil.convertToJavaScript(listaTipoVinculo, "listaTipoVinculo", "");
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();

		view.println(valoresCombo);
	}

	/**
	 * Action de Ajax. Apenas serve para consultar o tipo de conta vinculado a um agendamento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContatipoService#carregarPor(Agendamento)
	 * @param request
	 * @param form
	 * @author Hugo Ferreira
	 */
	public void ajaxConsultaVinculo(WebRequestContext request, Agendamento form) {
		String value = "'';";
		form = agendamentoService.load(form);
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		if(configuracaoFolhaService.existeConfiguracaoFolha(form)){
			view.println("alert('N�o � poss�vel consolidar um agendamento com configura��o de folha!');");
			view.println("document.location.reload();");
			return;
		}

		if (!form.getSituacao().equals(SituacaoAgendamento.A_CONSOLIDAR) && 
				!form.getSituacao().equals(SituacaoAgendamento.ATENCAO) && 
				!form.getSituacao().equals(SituacaoAgendamento.ATRASADO)) {
			view.println("var contaTipo = " + value);
			view.println("alert('N�o � poss�vel consolidar este agendamento pois sua situa��o n�o � \"A Consolidar\", \"Aten��o\" ou \"Atrasado\".');");
			view.println("document.location.reload();");
			return;
		}
		if (!form.getAtivo()) {
			view.println("var contaTipo = " + value);
			view.println("alert('N�o � poss�vel consolidar este agendamento pois ele est� inativo.');");
			return;
		}

		Contatipo contaTipo = contatipoService.carregarPor(form);

		if (contaTipo == null) {
			value = "null;";
		} else {
			value = "'" + contaTipo.getNome() + "';";
		}
		view.println("var contaTipo = " + value);
	}

	/**
	 * <p>Calcula a data da �ltima itera��o de um agendamento e envia o resultado
	 * para a view.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.AgendamentoService#calcularDtFim(Agendamento)
	 * @param request
	 * @param form <code>Agendamento</code> com as propriedades  
	 * @author Hugo Ferreira
	 */
	public void calcularDtFim(WebRequestContext request, Agendamento form) {
		View view = View.getCurrent();
		Date dtFim = null;
		request.getServletResponse().setContentType("text/html");
		try {
			//O tratamento dos par�metros � feito dentro da fun��o calcularUltimaParcela
			dtFim = agendamentoService.calcularDataUltimaParcela(form.getFrequencia(), form.getIteracoes(), form.getDtproximo());
		} catch (SinedException e) { //Caso hajam erros:
			view.println("var houveErro = true;");
			view.println("var mensagem = \"" + e.getMessage() + "\";");
			return;
		}
		view.println("var houveErro = false;");
		view.println("var dtFim = '" + SinedDateUtils.toString(dtFim) + "';");
	}
	
	/**
	 * 
	 * M�todo Para consolidar os Agendamentos
	 *
	 * @author Thiago Augusto
	 * @date 23/01/2012
	 * @param request
	 * @param bean
	 */
	public void consolidarAgendamentos(WebRequestContext request, Agendamento bean){
		try{
			String whereIn = request.getParameter("selectedItens");
			if (whereIn == null || whereIn.equals("")) {
				throw new SinedException("Nenhum item selecionado.");
			}
			
			//Verifica��o de data limite do ultimo fechamento. 
			Agendamento aux = new Agendamento();
			aux.setWhereIn(whereIn);
			if(fechamentofinanceiroService.verificaListaFechamento(aux)){
				throw new SinedException("Existem agendamentos cuja data de vencimento refere-se a um per�odo j� fechado.");
			}
			
			Agendamento agendamento;
			String [] listaCdAgendamentos = whereIn.split(",");
			for (String string : listaCdAgendamentos) {
				agendamento = new Agendamento();
				agendamento = agendamentoService.carregarParaConsolidar(Integer.parseInt(string));
				
				if(configuracaoFolhaService.existeConfiguracaoFolha(agendamento)){
					throw new Exception("N�o � poss�vel consolidar um agendamento com configura��o de folha!");
				}
				if (agendamento.getSituacao().equals(SituacaoAgendamento.FINALIZADO)){
					throw new Exception("N�o � poss�vel consolidar um agendamento com a situa��o Finalizado!");
				}
				if (!agendamento.getSituacao().equals(SituacaoAgendamento.A_CONSOLIDAR) &&
						!agendamento.getSituacao().equals(SituacaoAgendamento.ATENCAO) &&
						!agendamento.getSituacao().equals(SituacaoAgendamento.ATRASADO)){
					throw new Exception("N�o � poss�vel consolidar agendamento(s) com situa��o diferente de \"A Consolidar\", \"Aten��o\" ou \"Atrasado\".");
				}
				if (!agendamento.getAtivo()){
					throw new Exception("N�o � poss�vel consolidar um agendamento que esteja Inativo!");
				}
			}
			
			for (String string : listaCdAgendamentos) {
				agendamento = new Agendamento();
				agendamento = agendamentoService.carregarParaConsolidar(Integer.parseInt(string));
				
				if (agendamento.getSituacao().equals(SituacaoAgendamento.FINALIZADO)){
					throw new Exception("N�o � poss�vel consolidar um agendamento com a situa��o Finalizado!");
				} 
				if (!agendamento.getAtivo()){
					throw new Exception("N�o � poss�vel consolidar um agendamento que esteja Inativo!");
				}
				
				if(agendamento.getTipovinculo().getCdtipovinculo().equals(Tipovinculo.CONTA_A_RECEBER)
						|| agendamento.getTipovinculo().getCdtipovinculo().equals(Tipovinculo.CONTA_A_PAGAR)){
					Documento documento = new Documento();
					documento = documentoService.gerarDocumento(agendamento,agendamento.getTipovinculo().getCdtipovinculo());
					rateioService.calculaValoresRateio(documento.getRateio(), documento.getValor());
					documento.setCdAgendamento(agendamento.getCdagendamento());
					documento.setNumero(agendamento.getDocumento());
					if(agendamento.getTipovinculo().getCdtipovinculo().equals(Tipovinculo.CONTA_A_RECEBER)){
						documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
						documento.setConta(agendamento.getVinculoProvisionado());
						if(documento.getDocumentotipo() != null && Boolean.TRUE.equals(documento.getDocumentotipo().getBoleto()) && 
								documento.getPessoa() != null && documento.getPessoa().getListaEndereco() != null && 
								documento.getPessoa().getListaEndereco().size() == 1){
							documento.setEndereco(documento.getPessoa().getListaEndereco().iterator().next());
						}
					}else if(agendamento.getTipovinculo().getCdtipovinculo().equals(Tipovinculo.CONTA_A_PAGAR)){
						documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);						
						documento.setVinculoProvisionado(agendamento.getVinculoProvisionado());
					}
					documento.ajustaBeanToSave();
					agendamentoService.doConsolidar(documento);
				}else{
					Movimentacao movimentacao = new Movimentacao(); 
					agendamento = agendamentoService.carregarParaConsolidar(Integer.parseInt(string));
					movimentacao = movimentacaoService.gerarMovimentacao(agendamento);
					rateioService.calculaValoresRateio(movimentacao.getRateio(), movimentacao.getValor());
					movimentacao.setCdAgendamento(agendamento.getCdagendamento());
					movimentacao.setMovimentacaoacao(Movimentacaoacao.NORMAL);
					movimentacao.setObservacaocopiacheque(agendamento.getDocumento());
					agendamentoService.doConsolidar(movimentacao);
				}
			}
			request.addMessage("As consolida��es foram feitas sucesso!");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
	}
	
	public void ajaxBuscaTipoOperacao(WebRequestContext request, Agendamento form) {
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();

		form = agendamentoService.loadTipooperacao(form);
		view.println("var tipoop = "+form.getTipooperacao().getCdtipooperacao().toString());
	}
	
	public ModelAndView buscarOperacaocontabil(WebRequestContext request, Agendamento form) throws Exception {
		if (form.getCliente()!=null && form.getCliente().getCdpessoa()!=null){
			form.setOperacaocontabil(operacaocontabilService.load(form.getCliente(), "operacaocontabil"));
		}else if (form.getFornecedor()!=null && form.getFornecedor().getCdpessoa()!=null){
			form.setOperacaocontabil(operacaocontabilService.load(form.getFornecedor(), "operacaocontabil"));
		}else {
			form.setOperacaocontabil(null);
		}
		return doEntrada(request, form);
	}
	
	public void estornarAgendamentos(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		String error = "";
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Agendamento> listaAgendamento = agendamentoService.loadListAgendamentoEstorno(whereIn);
		if(listaAgendamento!=null && !listaAgendamento.isEmpty()){
			error += validaSituacaoAgendamento(listaAgendamento);
			error += validaConciliacaoAgendamento(listaAgendamento);
			
			if(!error.equals("")){
				request.addError(error);
			}else{
				agendamentoService.estornarAgendamento(listaAgendamento);
				for (Agendamento agendamento : listaAgendamento) {
					AgendamentoAcao agendamentoAcao =  new AgendamentoAcao(AgendamentoAcao.ESTORNADO);
					agendamentoHistoricoService.salvarHistoricoAgendamento(agendamento,agendamentoAcao,"");	
				}				
				request.addMessage("Estorno(s) realizado(s) com sucesso!");
			}
		}else{
			throw new SinedException("Nenhum item selecionado.");
		}
		
		SinedUtil.recarregarPaginaWithoutClose(request);
	}
	
	private String validaConciliacaoAgendamento(List<Agendamento> listaAgendamento){
		List<Movimentacaoorigem> listaMovimentacaoorigem = movimentacaoorigemService.findByAgendamento(listaAgendamento);
		if(listaMovimentacaoorigem!=null && !listaMovimentacaoorigem.isEmpty()){
			for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
				Movimentacaoacao movimentacaoacao = movimentacaoorigem.getMovimentacao().getMovimentacaoacao();
				if(!movimentacaoacao.equals(Movimentacaoacao.CANCELADA))
					return "N�o � poss�vel estornar este agendamento: "+movimentacaoorigem.getAgendamento().getCdagendamento()+", pois a �ltima intera��o (consolida��o) n�o est� cancelada.";
			}			
		}
		return "";
	}
	
	private String validaSituacaoAgendamento(List<Agendamento> listaAgendamento) {
		for (Agendamento agendamento : listaAgendamento) {
			Aux_agendamento auxAgendamento = agendamento.getAux_agendamento();
			if(auxAgendamento == null || auxAgendamento.getSituacao() == null || auxAgendamento.getSituacao()!=5){
				return "Agendamento(s) com a situa��o diferente de 'Finalizado'.";	
			}		
		}
		return "";
	}
	
	public ModelAndView ajaxCarregaFormapagamento(WebRequestContext request){
		String tipoVinculoParam = request.getParameter("tipoVinculo");
		String tipoOperacao = request.getParameter("tipoOperacao");
		String cdformapagamento = request.getParameter("formaPagamento");
		Integer cdtipovinculo = null;
		Tipovinculo tipovinculo = null;
		Formapagamento formapagamento = null;
		List<Formapagamento> listaFormapagamento = null;
		
		try{
			if(tipoVinculoParam!=null && !tipoVinculoParam.isEmpty()){
				cdtipovinculo = Integer.parseInt(tipoVinculoParam);
				tipovinculo = new Tipovinculo(cdtipovinculo);
			}			
		}catch (Exception e) {
			throw new RuntimeException("O parametro 'tipoVinculo' n�o corresponde a um n�mero");
		}
		
		if(tipovinculo!=null && tipovinculo.getCdtipovinculo()!=null && tipoOperacao!=null && !tipoOperacao.isEmpty()){
			listaFormapagamento = new ArrayList<Formapagamento>();		
			
			if(tipoOperacao.equals("DEBITO")){
				if(tipovinculo.getCdtipovinculo().equals(2)){ // Lan�amento em Conta Banc�ria (Debito)
					listaFormapagamento.add(Formapagamento.DINHEIRO);
					listaFormapagamento.add(Formapagamento.CHEQUE);
					listaFormapagamento.add(Formapagamento.DEBITOCONTACORRENTE);
				}
				
				if(tipovinculo.getCdtipovinculo().equals(3)){ // Lan�amento em Caixa (Debito)
					listaFormapagamento.add(Formapagamento.DINHEIRO);
					listaFormapagamento.add(Formapagamento.CHEQUE);
					listaFormapagamento.add(Formapagamento.CAIXA);
					listaFormapagamento.add(Formapagamento.DEBITOCONTACORRENTE);
				}
				
				if(tipovinculo.getCdtipovinculo().equals(4)){ // Lan�amento em Cart�o de Cr�dito (Debito)
					listaFormapagamento.add(Formapagamento.CARTAOCREDITO);
				}
			}
			
			if(tipoOperacao.equals("CR�DITO")){
				if(tipovinculo.getCdtipovinculo().equals(6)){ // Lan�amento em Conta Banc�ria (Cr�dito)
					listaFormapagamento.add(Formapagamento.DINHEIRO);
					listaFormapagamento.add(Formapagamento.CHEQUE);
					listaFormapagamento.add(Formapagamento.CREDITOCONTACORRENTE);
				}
				
				if(tipovinculo.getCdtipovinculo().equals(7)){ // Lan�amento em Caixa (Cr�dito)
					listaFormapagamento.add(Formapagamento.DINHEIRO);
					listaFormapagamento.add(Formapagamento.CHEQUE);
					listaFormapagamento.add(Formapagamento.CAIXA);
					listaFormapagamento.add(Formapagamento.CREDITOCONTACORRENTE);
				}
			}
		}
		
		if(cdformapagamento!=null && !cdformapagamento.isEmpty() && !cdformapagamento.equals("<null>")){
			if(StringUtils.isNumeric(cdformapagamento))
				formapagamento = FormapagamentoService.getInstance().load(new Formapagamento(Integer.parseInt(cdformapagamento)));
		}
		
		return new JsonModelAndView().addObject("listaFormapagamento", listaFormapagamento).addObject("formaPagamentoSelecionado", formapagamento);
	}
	
	@Action("carregaVinculoByEmpresa")
	public void carregaVinculoByEmpresa(WebRequestContext request, Agendamento agendamento){
		List<Conta> listaVinculo = contaService.findVinculosAtivosByEmpresa(agendamento.getEmpresa());
		View.getCurrent().convertObjectToJs(listaVinculo, "lista");
	}
	
	@Action("createRateioByModelo")
	public void createRateioByModelo(WebRequestContext request, Agendamento agendamento){
		if(agendamento.getRateiomodelo() != null && agendamento.getRateiomodelo().getCdrateiomodelo() != null){
			Rateio rateio = rateioService.createRateioByModelo(agendamento.getRateiomodelo());
			View.getCurrent().convertObjectToJs(rateio, "rateio");
		}
	}

	@Override
	protected void excluir(WebRequestContext request, Agendamento bean)
			throws Exception {
		throw new SinedException("A��o n�o permitida.");
	}
	
	@Override
	protected Agendamento criar(WebRequestContext request, Agendamento form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdagendamento() != null){
			form = agendamentoService.criarCopia(form);
			request.setAttribute("copiaragendamento", true);
		}
		
		
		
		
		return form;
		
	}
}