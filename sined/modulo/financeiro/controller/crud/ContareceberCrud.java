package br.com.linkcom.sined.modulo.financeiro.controller.crud;
 
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemacao;
import br.com.linkcom.sined.geral.bean.enumeration.FormaEnvioBoletoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ClienteDAO;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.BoletoService;
import br.com.linkcom.sined.geral.service.CalendarioService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaoService;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.DocumentoApropriacaoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoacaoService;
import br.com.linkcom.sined.geral.service.DocumentoclasseService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FaturamentohistoricoService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.HistoricoAntecipacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotaVendaService;
import br.com.linkcom.sined.geral.service.OperacaocontabilService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.ProjetotipoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.geral.service.RateiomodeloService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.geral.service.TipotaxaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.ValecompraorigemService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendapagamentoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.ContratoCrud;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GeraReceita;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DocumentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/financeiro/crud/Contareceber", authorizationModule=CrudAuthorizationModule.class)
public class ContareceberCrud extends CrudControllerSined<DocumentoFiltro, Documento, Documento> {

	private DocumentoService documentoService;
	private DocumentoacaoService documentoacaoService;
	private DocumentohistoricoService documentohistoricoService;
	private AgendamentoService agendamentoService;
	private RateioService rateioService;
	private TaxaService taxaService;
	private CentrocustoService centrocustoService;
	private MovimentacaoService movimentacaoService;
	private PrazopagamentoService prazopagamentoService;
	private DespesaviagemService despesaviagemService;
	private NotaService notaService;
	private ContaService contaService;
	private ProjetoService projetoService;
	private RateioitemService rateioitemService;
	private ContareceberService contareceberService;
	private ContratoService contratoService;
	private PessoaService pessoaService;
	private BoletoService boletoService;
	private DocumentotipoService documentotipoService;
	private NotaDocumentoService notaDocumentoService;
	private VendapagamentoService vendapagamentoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private FaturamentohistoricoService faturamentohistoricoService;
	private DocumentoorigemService documentoorigemService;
	private EmpresaService empresaService;
	private ContagerencialService contagerencialService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private UsuarioService usuarioService;
	private NotaVendaService notaVendaService;
	private OperacaocontabilService operacaocontabilService;
	private ParametrogeralService parametrogeralService;
	private ContratofaturalocacaoService contratofaturalocacaoService;
	private ClienteService clienteService;
	private ValecompraorigemService valecompraorigemService;
	private ProjetotipoService projetotipoService;
	private ChequeService chequeService;
	private VendaService vendaService;
	private RateiomodeloService rateiomodeloService;
	private CalendarioService calendarioService;
	private TipotaxaService tipotaxaService;
	private ReportTemplateService reportTemplateService;
	private HistoricoAntecipacaoService historicoAntecipacaoService;
	private DocumentoApropriacaoService documentoApropriacaoService;
	
	public void setValecompraorigemService(ValecompraorigemService valecompraorigemService) {this.valecompraorigemService = valecompraorigemService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setFaturamentohistoricoService(FaturamentohistoricoService faturamentohistoricoService) {this.faturamentohistoricoService = faturamentohistoricoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setContratoService(ContratoService contratoService) { this.contratoService = contratoService; }
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {this.despesaviagemService = despesaviagemService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setDocumentoacaoService(DocumentoacaoService documentoacaoService) {this.documentoacaoService = documentoacaoService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setAgendamentoService(AgendamentoService agendamentoService) {this.agendamentoService = agendamentoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setJurosService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setBoletoService(BoletoService boletoService) {this.boletoService = boletoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {this.vendapagamentoService = vendapagamentoService;}
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setNotaVendaService(NotaVendaService notaVendaService) {this.notaVendaService = notaVendaService;}
	public void setOperacaocontabilService(OperacaocontabilService operacaocontabilService) {this.operacaocontabilService = operacaocontabilService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setContratofaturalocacaoService(ContratofaturalocacaoService contratofaturalocacaoService) {this.contratofaturalocacaoService = contratofaturalocacaoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setProjetotipoService(ProjetotipoService projetotipoService) {this.projetotipoService = projetotipoService;}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setRateiomodeloService(RateiomodeloService rateiomodeloService) {this.rateiomodeloService = rateiomodeloService;}
	public void setCalendarioService(CalendarioService calendarioService) {this.calendarioService = calendarioService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setHistoricoAntecipacaoService(HistoricoAntecipacaoService historicoAntecipacaoService) {this.historicoAntecipacaoService = historicoAntecipacaoService;}
	public void setDocumentoApropriacaoService(DocumentoApropriacaoService documentoApropriacaoService) {this.documentoApropriacaoService = documentoApropriacaoService;}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Documento form) throws CrudException {
		if(fechamentofinanceiroService.verificaFechamento(form)){
			throw new SinedException("A data de vencimento refere-se a um per�odo j� fechado.");
		}
		
		if(form != null && form.getDtvencimento() != null && form.getDtvencimentoAux() != null
				&& !SinedDateUtils.equalsIgnoreHour(form.getDtvencimento(), form.getDtvencimentoAux())
				&& form.getCopiadocumeto() != null && !form.getCopiadocumeto()){
			form.setDtvencimentoantiga(form.getDtvencimentoAux());
		}
		
		/*Se vier da tela de entrega retorna ou para a listagem ou para tela de entrada*/
		if(form.getController() != null && form.getWhereInDespesaviagem() != null && !form.getWhereInDespesaviagem().equals("")){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Conta(s) a receber gerada(s) com sucesso.", MessageType.INFO);
			String observacao = "<a href=/w3erp/financeiro/crud/Contareceber?ACAO=consultar&cddocumento="+form.getCddocumento()+">"+form.getCddocumento()+"</a>";
			despesaviagemService.salvaHistorico(form.getWhereInDespesaviagem(), Despesaviagemacao.ACERTO, observacao);
			
			if(StringUtils.isNotBlank(form.getWhereInDespesaviagem())){
				StringBuilder obs = new StringBuilder("Acerto referente � despesa de viagem:");
				for(String idDespesaviagem : form.getWhereInDespesaviagem().split(",")){
					obs.append(" <a href=/w3erp/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+idDespesaviagem+">"+idDespesaviagem+"</a>,");
				}
				
				Documentohistorico dh = new Documentohistorico();
				dh.setDocumento(form);
				dh.setDocumentoacao(form.getDocumentoacao());
				dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
				dh.setObservacao(obs.substring(0, obs.length()-1));
				documentohistoricoService.saveOrUpdate(dh);
			}
			
			return new ModelAndView("redirect:"+ form.getController());
		}
		if(form.getController() != null && form.getWhereInDespesaviagemReembolso() != null && !form.getWhereInDespesaviagemReembolso().equals("")){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Reembolso gerado com sucesso.", MessageType.INFO);
			String observacao = "Conta a receber: <a href=/w3erp/financeiro/crud/Contareceber?ACAO=consultar&cddocumento="+form.getCddocumento()+">"+form.getCddocumento()+"</a>";
			despesaviagemService.salvaHistorico(form.getWhereInDespesaviagemReembolso(), Despesaviagemacao.REEMBOLSO, observacao);
			
			if(StringUtils.isNotBlank(form.getWhereInDespesaviagemReembolso())){
				StringBuilder obs = new StringBuilder("Reembolso referente � despesa de viagem:");
				for(String idDespesaviagem : form.getWhereInDespesaviagemReembolso().split(",")){
					obs.append(" <a href=/w3erp/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+idDespesaviagem+">"+idDespesaviagem+"</a>,");
				}
				
				Documentohistorico dh = new Documentohistorico();
				dh.setDocumento(form);
				dh.setDocumentoacao(form.getDocumentoacao());
				dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
				dh.setObservacao(obs.substring(0, obs.length()-1));
				documentohistoricoService.saveOrUpdate(dh);
			}
			
			return new ModelAndView("redirect:"+ form.getController());
		}
		if(form.getWhereInNota() != null && !"".equals(form.getWhereInNota())){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Receita gerada com sucesso.");
			
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO=listagem");
		}
		
		if (Boolean.TRUE.equals(form.getGerarreceitafaturalocacao())){
			super.doSalvar(request, form);
			request.addMessage("Receita(s) gerada(s) com sucesso.");
			return new ModelAndView("redirect:/faturamento/crud/Contratofaturalocacao?ACAO=listagem");
		}
		
		return super.doSalvar(request, form);
	}

	@Override
	protected void salvar(WebRequestContext request, Documento bean)throws Exception {
		/* Essa verifica��o est� sendo feita por outro m�todo Ajax. 
		 * 
		if(bean.getCddocumento() == null && !documentoService.isAlowToSaveBean(request,bean)){
			throw new SinedException("N�o � poss�vel salvar o mesmo registro novamente.");	
		}
		*/
		bean.ajustaBeanToSave();
		
		if(bean.getCddocumento() != null){
			Documento documento_aux = documentoService.load(bean, "documento.cddocumento, documento.documentoacao");
			bean.setAcaoanterior(documento_aux.getDocumentoacao());
			
			if(!bean.getAcaoanterior().equals(Documentoacao.PREVISTA) && !bean.getAcaoanterior().equals(Documentoacao.DEFINITIVA)){
				throw new SinedException("S� � poss�vel alterar as informa��es de uma conta a receber que estiver na situa��o prevista ou definitiva.");
			}
			bean.setAcaohistorico(Documentoacao.ALTERADA);
		}
		
		if(bean.getDtcompetencia() == null) {
			bean.setDtcompetencia(bean.getDtemissao() != null ? bean.getDtemissao() : new Date(System.currentTimeMillis()));
		}
		
		try {		
			if (bean.getContaRestanteDe() != null){
				Documentohistorico dh = documentohistoricoService.geraHistoricoDocumento(bean);
				dh.setObservacao("Conta referente a diferen�a de pagamento da conta <a href=\"Contareceber?ACAO=consultar&cddocumento="+bean.getContaRestanteDe().getCddocumento()+"\">"+bean.getContaRestanteDe().getCddocumento()+"</a> ");
				dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
				dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				
				if(bean.getListaDocumentohistorico()==null)
					bean.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class));
				bean.getListaDocumentohistorico().add(dh);

				documentoService.saveOrUpdate(bean);
				
				if (bean.getValorPago() != null)
					contareceberService.atualizaValorRateio(bean.getContaRestanteDe(), bean.getValorPago());
				
			}else if (bean.getCdAgendamento() != null) {
				//consolidar agendamento (financeiro)
				agendamentoService.doConsolidar(bean);
//				request.addMessage("Registro salvo com sucesso.");
//				request.setAttribute("closeWindow", true);
//				request.setAttribute("isPopup", true);
				request.getServletResponse().setContentType("text/html");
				View.getCurrent()
					.eval("<script type='text/javascript'>")
						.eval("alert('Registro salvo com sucesso');")
						.eval("window.top.close();")
					.eval("</script>")
					.flush();
			}else if (bean.getCdNota() != null) {
				//gerar receita (faturamento)
				notaService.liquidar(bean.getCdNota(), bean.getNumeroNota(), bean);
				
				Nota nota = notaService.load(new Nota(bean.getCdNota()), "nota.cdNota, nota.notaTipo");
				String urlNota = "/faturamento/crud/"  + (nota != null && NotaTipo.NOTA_FISCAL_SERVICO.equals(nota.getNotaTipo()) ? "NotaFiscalServico" : "Notafiscalproduto");
				
				if(bean.getExisteDuplicata() != null && bean.getExisteDuplicata()){
					request.addMessage("Receita gerada com sucesso.");
				}
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent()
					.eval("<script type='text/javascript'>if(window.opener != null && window.opener.disableGerarReceita != null && typeof window.opener.disableGerarReceita != 'undefined'){")
						.eval("window.opener.disableGerarReceita();")
						.eval("window.opener.$('input[name=gerado]').val(true);")
						.eval("window.opener.$s.showNoticeMessage('Receita gerada com sucesso.');")
						.eval("window.top.close();")
					.eval("}else {parent.location = '" + request.getServletRequest().getContextPath() + urlNota + "';}</script>")
					.flush();
			} else {
				//Manter conta a receber (padr�o)
				if(bean.getNomeProcessoFluxoAnterior()!=null && (
						bean.getNomeProcessoFluxoAnterior().equals(ContratoCrud.ACAO_CONTRATO_ENTRADA)
						|| bean.getNomeProcessoFluxoAnterior().equals(ContratoCrud.ACAO_CONTRATO_LISTAGEM))){
					Documentohistorico dh = contratoService.criaDocumentoHistoricoFaturamentoContrato(bean, bean.getContrato().getCdcontrato());
					if(bean.getListaDocumentohistorico()==null)
						bean.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class));
					bean.getListaDocumentohistorico().add(dh);
				}
				
				String textoAlterouVencimento = bean.getTextoAlterouVencimento(); 
				
				super.salvar(request, bean);
				
				if(bean.getObservacaoHistorico() != null){
					if (textoAlterouVencimento!=null){
						bean.setObservacaoHistorico(textoAlterouVencimento + ". " + Util.strings.emptyIfNull(bean.getObservacaoHistorico()));
					}
					documentohistoricoService.updateObservacaoDocumento(bean, bean.getObservacaoHistorico());
				}
				
				if((bean.getFromGerarreceitanota() != null && bean.getFromGerarreceitanota())){
					if(bean.getWhereInNota() != null && !"".equals(bean.getWhereInNota())){
						notaFiscalServicoService.gerarHistoricoLiquidarNota(bean, bean.getWhereInNota(), false);						
					}
					request.addMessage("Conta a pagar da comiss�o gerada.");
				}
				
				if(StringUtils.isNotBlank(bean.getWhereInDespesaviagemReembolso())){
					StringBuilder obs = new StringBuilder("Acerto referente � despesa de viagem:");
					for(String idDespesaviagem : bean.getWhereInDespesaviagemReembolso().split(",")){
						obs.append(" <a href=/w3erp/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+idDespesaviagem+">"+idDespesaviagem+"</a>,");
					}
					
					Documentohistorico dh = new Documentohistorico();
					dh.setDocumento(bean);
					dh.setDocumentoacao(bean.getDocumentoacao());
					dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
					dh.setObservacao(obs.substring(0, obs.length()-1));
					documentohistoricoService.saveOrUpdate(dh);
				}
				
				if (bean.getContrato()!=null && bean.getContrato().getCdcontrato()!=null){
					contratoService.updateReajuste(bean.getContrato(), Boolean.FALSE, null);
				}
				
				if(StringUtils.isNotBlank(bean.getWhereInDocumentoAdiantamento())) {
					documentoService.gerarHistoricoCompensacao(bean);
					
					Documentohistorico dh = new Documentohistorico();
					dh.setDocumento(bean);
					dh.setDocumentoacao(bean.getDocumentoacao());
					dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
					
					StringBuilder observacao = new StringBuilder("Conta criada a partir de compensa��o de saldo das contas ");
					for(String id : bean.getWhereInDocumentoAdiantamento().split(",")) {
						observacao.append("<a href=\"javascript:visualizaContapagar(" + id + ");\">" + id + "</a> ");
					}
					dh.setObservacao(observacao.toString());
					
					documentohistoricoService.saveOrUpdate(dh);
				}
			}
		} catch(Exception e){
			e.printStackTrace();
			if (e instanceof DataIntegrityViolationException) {
				DataIntegrityViolationException divException = (DataIntegrityViolationException) e;
				if(DatabaseError.isKeyPresent(divException, "IDX_TAXAITEM_TIPOTAXA")){
					throw new SinedException("N�o deve haver tipos de taxas iguais.");
				}else throw e;
			}else{
				if(bean.getCdNota() != null){
					request.getServletResponse().setContentType("text/html");
					View.getCurrent()
					.eval("<script type='text/javascript'>")
						.eval("window.opener.$s.showAlertMessage('Problema ao gerar receita.');")
						.eval("window.opener.$('input[name=gerado]').val(false);")
						.eval("window.top.close();")
					.eval("</script>")
					.flush();
				}
			}
		}
	}

	@Override
	protected void excluir(WebRequestContext request, Documento bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir uma conta a receber.");		
	}

	@Override
	public ModelAndView doEntrada(WebRequestContext request, Documento form) throws CrudException {
		if(request.getAttribute("CONSULTAR") == null && request.getParameter("ACAO") != null && request.getParameter("ACAO").equals("editar") ){	
			if(fechamentofinanceiroService.verificaFechamento(form)){
				request.addError("Esta conta n�o pode ser editada pois refere-se a um per�odo j� fechado.");
				request.setAttribute("CONSULTAR", true);
				return super.doConsultar(request, form);
			}
		}
		if(form != null && form.getDtvencimentoAux() == null){
			form.setDtvencimentoAux(form.getDtvencimento());
		}
		
		return super.doEntrada(request, form);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void entrada(WebRequestContext request, Documento form)throws Exception {
		documentoService.carregaDadosPessoaDocumentoeForEntrada(form);
		
		List<Centrocusto> listaCentroCusto = null;
		if(request.getParameter("cdmovimentacao") != null){
			Movimentacao m = movimentacaoService.findForRegistrarDevolucao(request.getParameter("cdmovimentacao")).get(0);
			
			form.setCddocumento(null);
			form.setObservacaoHistorico("Cheque Devolvido");
			form.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
			form.setDocumentoacao(Documentoacao.DEFINITIVA);
			form.setDtvencimento(new Date(System.currentTimeMillis()));
			form.setValor(m.getValor());
			form.setRateio(null);
			form.setFinanciamento(null);
			form.setPrazo(null);
			form.setListaParcela(null);
			form.setTaxa(null);
			form.setMensagem1(null);
			form.setMensagem2(null);
			form.setMensagem3(null);
			form.setMensagem4(null);
			form.setMensagem5(null);
			form.setMensagem6(null);
		}

		//Cria��o de conta a receber com valor restante referente a pagamento parcial via boleto
		if (request.getParameter("cddocumentooriginal") != null){
			Documento documentoOriginal = documentoService.loadForEntrada(new Documento(Integer.parseInt(request.getParameter("cddocumentooriginal"))));
			
			form.setValorPago(new Money(request.getParameter("valorpago")));
			form.setContaRestanteDe(documentoOriginal);
			//form.setCddocumentorelacionado(documentoOriginal.getCddocumento());
			form.setEmpresa(documentoOriginal.getEmpresa());
			form.setNumero(documentoOriginal.getNumero());
			form.setDocumentotipo(documentoOriginal.getDocumentotipo());
			form.setTipopagamento(documentoOriginal.getTipopagamento());
			form.setDtemissao(new Date(System.currentTimeMillis()));
			form.setDocumentoacao(Documentoacao.PREVISTA);
			form.setPessoa(documentoOriginal.getPessoa());
			
			if (Tipopagamento.CLIENTE.equals(documentoOriginal.getTipopagamento())) {
				form.setCliente(ClienteService.getInstance().load(new Cliente(documentoOriginal.getPessoa().getCdpessoa())));
			} else if (Tipopagamento.FORNECEDOR.equals(documentoOriginal.getTipopagamento())) {
				form.setFornecedor(FornecedorService.getInstance().load(new Fornecedor(documentoOriginal.getPessoa().getCdpessoa())));
			} else if (Tipopagamento.COLABORADOR.equals(documentoOriginal.getTipopagamento())) {
				form.setColaborador(ColaboradorService.getInstance().load(new Colaborador(documentoOriginal.getPessoa().getCdpessoa())));
			}

			if (request.getParameter("valorrestante") != null){
				form.setValor(new Money(request.getParameter("valorrestante")));
			}
			
			form.setRateio(new Rateio());
			Rateio rateio = rateioService.findByDocumento(documentoOriginal);
			if (rateio != null){
				for (Rateioitem ri : rateio.getListaRateioitem()){
					Rateioitem novoRateioitem = new Rateioitem();
					novoRateioitem.setRateio(form.getRateio());
					novoRateioitem.setCentrocusto(ri.getCentrocusto());
					novoRateioitem.setProjeto(ri.getProjeto());
					novoRateioitem.setContagerencial(ri.getContagerencial());
					novoRateioitem.setPercentual(ri.getPercentual());
					novoRateioitem.setValor(form.getValor().multiply(new Money(ri.getPercentual()/100)));
					
					form.getRateio().getListaRateioitem().add(novoRateioitem);
				}
			}
			
			String identificadorContaReceber = form.getNumero();
			if (request.getParameter("nossonumero") != null){
				identificadorContaReceber += "/" + request.getParameter("nossonumero");
			}
			form.setDescricao("Referente a diferen�a de pagamento da conta " + identificadorContaReceber );
		}
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}

		//Setar lista completar somente no consultar
		List<Documentoacao> listaAcao = null;
		if (!CONSULTAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))) {
			listaAcao = new ArrayList<Documentoacao>();
			listaAcao.add(new Documentoacao(Documentoacao.DEFINITIVA.getCddocumentoacao(),"DEFINITIVA"));
			listaAcao.add(new Documentoacao(Documentoacao.PREVISTA.getCddocumentoacao(),"PREVISTA"));
		} else {
			listaAcao = documentoacaoService.findForContareceber();
		}

		if (request.getBindException().hasErrors()) {
			if (form.getCddocumento() == null) {
				request.setAttribute(MultiActionController.ACTION_PARAMETER, CrudControllerSined.CRIAR);
			} else {
				request.setAttribute(MultiActionController.ACTION_PARAMETER, CrudControllerSined.EDITAR);
			}
		}

		if (form.getCddocumento() != null) {
			listaCentroCusto = (List<Centrocusto>) CollectionsUtil.getListProperty(form.getRateio().getListaRateioitem(), "centrocusto");
			
			if (form.getDocumentoacao().equals(Documentoacao.BAIXADA) || form.getDocumentoacao().equals(Documentoacao.BAIXADA_PARCIAL) || form.getDocumentoacao().equals(Documentoacao.NEGOCIADA)){
				List<Movimentacao> listaMovimentacao = movimentacaoService.findByDocumentoVariasMovimentacoes(form);
				request.setAttribute("listaVariasMovimentacoes", listaMovimentacao);
				
				List<Valecompraorigem> listaValecompraorigem = valecompraorigemService.findByDocumento(form);
				request.setAttribute("listaValecompraorigem", listaValecompraorigem);
			}
			
			if (Documentoacao.BAIXADA_PARCIAL.equals(form.getDocumentoacao())){
				form.setValorRestante(movimentacaoService.calcularMovimentacoesByDocumento(form, new Date(System.currentTimeMillis())));
			}
			form.setDtvencimentooinicial(form.getDtvencimento());
			form.setListaApropriacao(documentoApropriacaoService.findByDocumento(form));
			
			if(historicoAntecipacaoService.isContaGeradaAPartirDeCompensacaoDeSaldo(form)) {
				form.setIsCompensarSaldo(true);
			}
		}else{
			if(form.getTipopagamento() == null){
				form.setTipopagamento(Tipopagamento.CLIENTE);
			}
			if(form.getConta() == null){
				Empresa empresa = null;
				if(request.getSession().getAttribute("empresaSelecionada") != null){
					empresa = empresaService.loadWithContaECarteira((Empresa) request.getSession().getAttribute("empresaSelecionada"));
				}/*else{
					empresa = empresaService.loadWithContaECarteira(null);
				}*/
				
				if(empresa != null){					
					form.setConta(empresa.getContabancariacontareceber());
					form.setContacarteira(empresa.getContacarteiracontareceber());
				}
			}
		}

		if (form.getPrazo() == null) {
			Prazopagamento prazo = form.getListaParcela() != null && form.getListaParcela().iterator().hasNext() ? form.getListaParcela().iterator().next().getParcelamento().getPrazopagamento() : null;
			form.setPrazo(prazo);
		}
		
		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getCddocumento() != null && form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
			List<Projeto> projetos = rateioitemService.getProjetos(form.getRateio().getListaRateioitem());
			listaProjeto = projetoService.findProjetosAbertosSemPermissao(projetos, true, true);
		} else {
			listaProjeto = projetoService.findProjetosAbertos(null, true, true);
		}
		request.setAttribute("listaProjeto", listaProjeto);
		
		if(form.getCliente() != null){
			form.getCliente().setNome(pessoaService.load(form.getCliente(), "pessoa.nome").getNome());
			form.getCliente().setListaTelefone(new ListSet<Telefone>(Telefone.class));
		}
		if(form.getColaborador() != null){
			form.getColaborador().setNome(pessoaService.load(form.getColaborador(), "pessoa.nome").getNome());
		}
		if(form.getFornecedor() != null){
			form.getFornecedor().setNome(pessoaService.load(form.getFornecedor(), "pessoa.nome").getNome());
		}
		if(form.getCddocumento() != null){
			Agendamento agendamentoOrigem = documentoorigemService.findDocumentoOrigemAgendamentoByDocumento(form);
			if(agendamentoOrigem != null){
				request.setAttribute("agendamentos", Arrays.asList(new Agendamento[]{agendamentoOrigem}));
			}
		}
		request.setAttribute("listaPrazoPagamento", prazopagamentoService.findAtivos(form.getPrazo()));
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(listaCentroCusto));
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_CREDITO);
		request.setAttribute("listaStatus", listaAcao);
		request.setAttribute("clearBase", form.getCdAgendamento());
		
		String cddocumentotipo = form.getCddocumento() != null && form.getDocumentotipo() != null && form.getDocumentotipo().getCddocumentotipo() != null? form.getDocumentotipo().getCddocumentotipo().toString(): "";
		
		request.setAttribute("listaDocumentoTipo", documentotipoService.getListaDocumentoTipoUsuarioForVenda(cddocumentotipo));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("listaRateioModelo", rateiomodeloService.findBy(Tipooperacao.TIPO_CREDITO, true));
		
		List<Tipopagamento> listaTipopagamento = documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_CREDITO);
		if(form.getFornecedor() != null && form.getFornecedor().getCdpessoa() != null){
			if(listaTipopagamento == null) listaTipopagamento = new ArrayList<Tipopagamento>();
			listaTipopagamento.add(Tipopagamento.FORNECEDOR);
		}
		request.setAttribute("listaTipopagamento", listaTipopagamento);
		verificaExistenciaBoleto(request);
//		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		request.setAttribute("integracao_contabil", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INTEGRACAO_CONTABIL)));
		
		if(Util.booleans.isTrue(parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_DOCUMENTO_CR_CP))){
			request.setAttribute("obrigaDocumento", true);
		}
		
		rateioService.ajustePercentualRateioByDocumento(form);
		
//		request.setAttribute("podeAlterarValor", SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR") || documentoService.isPodeAlterarValorConta(request, form));
		if (form.getPodeAlterarValor()==null){
			form.setPodeAlterarValor(SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR") || documentoService.isPodeAlterarValorConta(request, form));
		}
		if(form.getPessoa() != null &&  form.getEndereco() != null)
			request.setAttribute("listaVaziaEndereco", contareceberService.carregaEnderecosAPartirTipoPagamento(form));
		else
			request.setAttribute("listaVaziaEndereco", new ArrayList<Endereco>());
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, DocumentoFiltro filtro) throws CrudException {		
		filtro.setIsRestricaoClienteVendedor(usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()));
		
		String clienteid = request.getParameter("clienteid");		
		
		if (clienteid != null){
			Cliente cliente = ClienteDAO.getInstance().loadForEntrada(new Cliente(Integer.parseInt(clienteid)));
			filtro.setCliente(cliente);
			filtro.setPessoa(cliente.getNome());
			
			List<Documentoacao> lista = new ArrayList<Documentoacao>();
			lista.add(Documentoacao.PREVISTA);
			lista.add(Documentoacao.DEFINITIVA);
			lista.add(Documentoacao.BAIXADA_PARCIAL);
			filtro.setListaDocumentoacao(lista);
			filtro.setNotFirstTime(true);
		}
		
		String vendaId = request.getParameter("idVenda");
		if(vendaId != null){
			filtro.setNotFirstTime(true);
		}
		
		filtro.setPermitirFiltroDocumentoDeAte(false);
		if(filtro.getDocumentoDe() != null || filtro.getDocumentoAte() != null){
			filtro.setPermitirFiltroDocumentoDeAte(documentoService.permitirFiltroDocumentoDeAte());
			if(filtro.getPermitirFiltroDocumentoDeAte() == null || !filtro.getPermitirFiltroDocumentoDeAte()){
				if(filtro.getDocumentoDe() == null || filtro.getDocumentoAte() == null || !filtro.getDocumentoDe().equals(filtro.getDocumentoAte())){
					filtro.setDocumentoDe(null);
					filtro.setDocumentoAte(null);
					request.addMessage("N�o � poss�vel filtrar o campo n�mero do documento por intervalo. Existe documento com caracteres inv�lidos.", MessageType.WARN);
				}
			}
		}
		
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request, DocumentoFiltro filtro)throws Exception {
//		if (filtro ==  null)
//			throw new SinedException("Dados inv�lidos");
			
		request.setAttribute("listaAcaoCompleta", documentoacaoService.findForContareceber());
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		request.setAttribute("listaTipopagamento", documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_CREDITO));
		request.setAttribute("listaTipoProjeto", projetotipoService.findAll());
		
		if (!filtro.getOrderBy().isEmpty()){
			String isAsc = "";
			if (filtro.getOrderBy() != null && !filtro.getOrderBy().isEmpty()) {
				isAsc = filtro.isAsc() ? " asc " : " desc ";
			}
			request.getSession().setAttribute("orderBy", filtro.getOrderBy() + isAsc);
		}

		List<Documentoacao> listaAcoes = new ArrayList<Documentoacao>();
		listaAcoes.add(Documentoacao.ATRASADA);
		listaAcoes.add(Documentoacao.ATRASADA_CONTRATO_ATIVO);
		listaAcoes.add(Documentoacao.ATRASADA_CONTRATO_CANCELADO);
		listaAcoes.add(Documentoacao.ATRASADA_CONTRATO_SUSPENSO);
		listaAcoes.add(Documentoacao.ATRASADA_SEM_PROTESTO);
		listaAcoes.add(Documentoacao.BOLETO_GERADO);
		listaAcoes.add(Documentoacao.BOLETO_NAO_GERADO);
		listaAcoes.add(Documentoacao.DEVOLVIDA);
		listaAcoes.add(Documentoacao.ENVIO_MANUAL_BOLETO);
		listaAcoes.add(Documentoacao.PAG_ATRASADO);
		listaAcoes.add(Documentoacao.PENDENTE);
		listaAcoes.add(Documentoacao.PROTESTADA);
		request.setAttribute("listaAcao", listaAcoes);
		verificaExistenciaBoleto(request);

		request.setAttribute("listarRazaoSocialCliente", parametrogeralService.getBoolean(Parametrogeral.LISTARRAZAOSOCIALCLIENTE));
		request.setAttribute(Parametrogeral.LISTAR_RAZAOSOCIAL_E_NOMEFANTASIA, parametrogeralService.getBoolean(Parametrogeral.LISTAR_RAZAOSOCIAL_E_NOMEFANTASIA));
		
		List<Documentoacao> listaAcoesProtesto = new ArrayList<Documentoacao>();
		listaAcoesProtesto.add(Documentoacao.ENVIADO_CARTORIO);
		listaAcoesProtesto.add(Documentoacao.ENVIADO_JURIDICO);
		listaAcoesProtesto.add(Documentoacao.PROTESTADA);
		listaAcoesProtesto.add(Documentoacao.ARQUIVO_MORTO);
		request.setAttribute("listaAcaoProtesto", listaAcoesProtesto);
		request.setAttribute("listaContaboleto", contaService.findContaBoletoFiltro());
		request.setAttribute("CONTARECEBER_MOSTRAR_VALOR", parametrogeralService.getValorPorNome(Parametrogeral.CONTARECEBER_MOSTRAR_VALOR));
		request.setAttribute("listaDocumentoTipo", documentotipoService.getListaDocumentoTipoUsuarioForVenda(null));
	}
	
	/**
	 * M�todo que verifica se existe boleto. Feito para mostrar a op��o na tela
	 * 
	 * @param request
	 * @author Tom�s Rabelo
	 */
	private void verificaExistenciaBoleto(WebRequestContext request) {
		request.setAttribute("existeBoletos", boletoService.existeBoletos());
	}
	
//	/**
//	 * Agrupa fun��es para marcar valores-padr�o no filtro.
//	 * 
//	 * @see #marcaAcaoPadrao(WebRequestContext, DocumentoFiltro)
//	 * @param request
//	 * @param filtro
//	 * @author Hugo Ferreira
//	 */
//	private void marcaValorPadraoFiltro(WebRequestContext request, DocumentoFiltro filtro) {
//		this.marcaAcaoPadrao(request, filtro);
//	}
//	
//	/**
//	 * Marca as a��es-padr�o no filtro, de acordo com a permiss�o do usu�rio logado.
//	 * 
//	 * @param request
//	 * @param filtro
//	 * @author Hugo Ferreira
//	 */
//	private void marcaAcaoPadrao(WebRequestContext request, DocumentoFiltro filtro) {
//		
//		boolean isFirstTime = !filtro.isNotFirstTime();
//		
//		if(isFirstTime){
//			List<Documentoacao> listaAcaoMarcadas = new ArrayList<Documentoacao>();
//			
//			if (SinedUtil.isUserHasAction("CONFIRMAR_CONTARECEBER")) {
//				listaAcaoMarcadas.add(Documentoacao.PREVISTA);
//			}
//			
//			if (SinedUtil.isUserHasAction("BAIXAR_CONTARECEBER")) {
//				listaAcaoMarcadas.add(Documentoacao.DEFINITIVA);
//			}
//			
//			if (SinedUtil.isUserHasAction("ESTORNAR_CONTARECEBER")) {
//				listaAcaoMarcadas.add(Documentoacao.DEFINITIVA);
//				listaAcaoMarcadas.add(Documentoacao.BAIXADA);
//			}
//			if (SinedUtil.isUserHasAction("CANCELAR_CONTARECEBER")) {
//				listaAcaoMarcadas.add(Documentoacao.PREVISTA);
//			}
//
////			listaAcaoMarcadas.add(Documentoacao.NEGOCIADA);
//			
//			filtro.setListaDocumentoacao(listaAcaoMarcadas);
//		}
//	}
	
	@Override
	protected ListagemResult<Documento> getLista(WebRequestContext request,DocumentoFiltro filtro) {
		filtro.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		ListagemResult<Documento> listResult = super.getLista(request, filtro);
		List<Documento> lista = listResult.list();
		
		filtro.setTotal(new Money());
		filtro.setTotalOriginal(new Money());
		filtro.setEmAberto(new Money());
		filtro.setEmAtraso(new Money());
		filtro.setTotalValorContaGerencial(new Money());
		filtro.setTotalValorCentroCusto(new Money());
		filtro.setTotalValorProjeto(new Money());

		if(lista != null && lista.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cddocumento", ",");
			lista.removeAll(lista);
			lista.addAll(documentoService.loadForListagem(whereIn, filtro, filtro.getOrderBy(), filtro.isAsc()));
			
			documentoService.ajustaValoresListagem(filtro, lista);
			for (Documento doc : lista) {
				documentoService.ajustaNumeroParcelaDocumento(doc);
				if(doc.getDocumentoacao() !=null && doc.getDocumentoacao().equals(Documentoacao.BAIXADA_PARCIAL)){
					doc.setValorRestante(movimentacaoService.calcularMovimentacoesByDocumento(doc, new Date(System.currentTimeMillis())));
				}
				
				if(BooleanUtils.isTrue(doc.getDocumentotipo().getAntecipacao()) && doc.getDocumentoacao().equals(Documentoacao.BAIXADA)) {
					documentoService.verificarSaldoAdiantamentoPorDocumento(doc, Documentoclasse.OBJ_RECEBER);
				}
				
				if(doc.getPossuiSaldoNaoCompensado() == null && documentoService.possuiContasComSaldoNaoCompensado(doc)) {
					// atualizar contas com a mesma pessoa e tipopagamento para evitar repetir a valida��o
					for(Documento aux : lista) {
						if(doc.getRecebimentode().equals(aux.getRecebimentode()) && doc.getTipopagamento().equals(aux.getTipopagamento())) {
							doc.setPossuiSaldoNaoCompensado(true);
						}
					}
				}
			}
			
			documentoService.setDatasBaixaContaListagem(filtro, lista, whereIn);
		}
		
		return listResult;
	}

	@Override
	protected Documento carregar(WebRequestContext request, Documento bean)throws Exception {
		Documento documento = super.carregar(request, bean);
		if (documento == null) {
			throw new SinedException("Registro n�o encontrado no sistema");
		}
		
		documento.ajustaBeanToLoad();
		
		// necess�rio recarregar a pessoa porque quando o mesmo CDPESSOA �
		// referenciado em mais de uma tabela o Hibernate carrega a primeira
		// ocorr�ncia, fazendo com que uma conta a receber de fornecedor receba
		// um objeto "pessoa" que na verdade � um Cliente ao inv�s do Fornecedor.
		if (Tipopagamento.CLIENTE.equals(documento.getTipopagamento())) {
			documento.setCliente(ClienteService.getInstance().load(documento.getCliente()));
		} else if (Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento())) {
			documento.setFornecedor(FornecedorService.getInstance().load(documento.getFornecedor()));
		} else if (Tipopagamento.COLABORADOR.equals(documento.getTipopagamento())) {
			documento.setColaborador(ColaboradorService.getInstance().load(documento.getColaborador()));
		}
		
		String acao = request.getParameter(MultiActionController.ACTION_PARAMETER);
		if("editar".equals(acao) &&
				request.getParameter("cdmovimentacao")==null &&
				!documento.getDocumentoacao().equals(Documentoacao.PREVISTA) && 
				!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)) {
			throw new SinedException("N�o � poss�vel editar uma conta a receber com situa��o diferente de prevista ou definitiva.");
		}


		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class,documentohistoricoService.carregaHistorico(documento)));
		documento.setTaxa(taxaService.findByDocumento(documento));
		documento.setRateio(rateioService.findByDocumento(documento));

		documentoService.calculaValores(documento);

		if(Util.booleans.isTrue(documento.getFinanciamento())) request.setAttribute("financiado", true);
		
		request.setAttribute("showEditar", documento.getDocumentoacao().equals(Documentoacao.PREVISTA) || documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA));

		documento.setAcaoanterior(documento.getDocumentoacao());

		boolean disabled = "consultar".equals(request.getParameter(MultiActionController.ACTION_PARAMETER)) 
		|| "salvar".equals(request.getParameter(MultiActionController.ACTION_PARAMETER)) 
		&& request.getBindException().getErrorCount() == 0;
		request.setAttribute("disabled", disabled);

		return documento;
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Documento form)
			throws CrudException {
		String isCompensacaoSaldo = request.getParameter("compensar");
		
		if(isCompensacaoSaldo != null && isCompensacaoSaldo.equals("true")) {
			String whereIn = SinedUtil.getItensSelecionados(request);
			
			Documentoclasse documentoClasse = DocumentoclasseService.getInstance().load(Documentoclasse.OBJ_PAGAR);
			List<Documento> listaDocumentosACompensar = documentoService.findForCompensarSaldo(whereIn);
			
			try {
				documentoService.verificarDocumentosAntesDeCompensar(listaDocumentosACompensar, documentoClasse);
			} catch(SinedException e) {
				request.addError(e.getMessage());
				return new ModelAndView("redirect:/financeiro/crud/Contapagar");
			}
		}
		
		return super.doCriar(request, form);
	}
	
	@Override
	protected Documento criar(WebRequestContext request, Documento form) throws Exception {
		Boolean compensacaoSaldo = Boolean.valueOf(request.getParameter("compensar") != null && !request.getParameter("compensar").equals("") ? 
				request.getParameter("compensar") : "false");
		
		String cdcontrato = request.getParameter("cdcontrato");
		if(cdcontrato != null){
			String nomeProcessoFluxoAnterior = request.getParameter("nomeProcessoFluxoAnterior");
						
			Documentotipo documentotipo = documentotipoService.findForFaturamentoContareceber();
			Contrato contrato = contratoService.findForCobranca(cdcontrato).get(0);
			
			if(Formafaturamento.SOMENTE_BOLETO.equals(contrato.getFormafaturamento())){
				Documentotipo documentotipoBoleto = documentotipoService.findBoleto();
				if(documentotipoBoleto != null){
					documentotipo = documentotipoBoleto;
				}
			}
			
			if(contrato.getRateio() != null && contrato.getRateio().getCdrateio() != null){
				contrato.setRateio(rateioService.findRateio(contrato.getRateio()));
			}
			if(contrato.getTaxa() != null && contrato.getTaxa().getCdtaxa() != null){
				contrato.setTaxa(taxaService.findTaxa(contrato.getTaxa()));
				request.setAttribute("createByContrato", true);
			}
			if(!StringUtils.isBlank((String) request.getAttribute("subfluxo")) || "gerarFaturamento".equals(request.getAttribute("subfluxo")))
				contrato = contratoService.selecionarEnderecoFaturamento(contrato);
			else
				contrato = contratoService.selecionarEnderecoAlternativoDeCobranca(contrato);
			form = contratoService.criaContaReceberContrato(contrato, documentotipo, false);
			form.setNomeProcessoFluxoAnterior(nomeProcessoFluxoAnterior);
		}
		
		String cdAgendamento = request.getParameter("cdAgendamento");
		if (!StringUtils.isEmpty(cdAgendamento)) {
			//Procedimento de consolidar agendamento:
			Agendamento agendamento = agendamentoService.carregarParaConsolidar(Integer.parseInt(cdAgendamento));
			form = documentoService.gerarDocumento(agendamento,Documentoclasse.CD_RECEBER);
			rateioService.calculaValoresRateio(form.getRateio(), form.getValor());
			form.setCdAgendamento(agendamento.getCdagendamento());
			request.setAttribute("clearBase", true);
			
			if (form.getDocumentotipo() != null) {
				Documentotipo documentoTipo = documentotipoService.findDocumentoTipo(form.getDocumentotipo());
				if (documentoTipo != null && BooleanUtils.isFalse(documentoTipo.getBoleto())) {
					form.setConta(null);
				}
			}
		}
		
		if ("true".equals(request.getParameter("gerarreceitafaturalocacao"))){
			//Fatura avulsa
			Contratofaturalocacao contratofaturalocacao = contratofaturalocacaoService.findForGerarReceita(request.getParameter("cdcontratofaturalocacao")).get(0);
			form =  contratoService.criaContaReceberFaturaLocacaoAvulsaOrIndenizacao(contratofaturalocacao);
			form.setGerarreceitafaturalocacao(true);
			form.setCdcontratofaturalocacao(Integer.valueOf(request.getParameter("cdcontratofaturalocacao")));
		}
		
		if("true".equals(request.getParameter("copiar")) && form.getCddocumento() != null){
			form = documentoService.criarCopia(form);
			form.setDocumentoacao(Documentoacao.PREVISTA);
		} else if (cdcontrato == null && StringUtils.isEmpty(cdAgendamento) && !"gerarReceita".equals(request.getParameter("ACAO")) && !"true".equals(request.getParameter("gerarreceitafaturalocacao"))){
			//Quando � uma nova conta pelo fluxo padr�o, sempre inicia com um rateio com percentual 100%
			form = super.criar(request, form);
			
			if(compensacaoSaldo) { 
				String whereIn = SinedUtil.getItensSelecionados(request);
				List<Documento> listaDocumentosACompensar = documentoService.findForCompensarSaldo(whereIn);
				
				documentoService.preencherDocumentoCompensacao(listaDocumentosACompensar, form);
			}
			
			Rateioitem rateioitem = new Rateioitem();
			rateioitem.setPercentual(100.0);
			
			Boolean gerarContaByDevolucaoCheque = Boolean.valueOf(request.getParameter("gerarContaByDevolucaoCheque") != null ? request.getParameter("gerarContaByDevolucaoCheque") : "false");
			if(gerarContaByDevolucaoCheque != null && gerarContaByDevolucaoCheque){
				documentoService.setInfDevolucaoCheque(request, form);
			}
			if(form.getValor() != null){
				rateioitem.setValor(form.getValor());
			}
			
			if (form.getRateio() == null)
				form.setRateio(new Rateio());
			if (form.getRateio().getListaRateioitem() == null)
				form.getRateio().setListaRateioitem(new ListSet<Rateioitem>(Rateioitem.class));
			form.getRateio().getListaRateioitem().add(rateioitem);

		}
		
		form.setDocumentoclasse(new Documentoclasse(Documentoclasse.CD_RECEBER));
		
		String faturarPorMedicao = request.getParameter("faturarPorMedicao");
		if(faturarPorMedicao != null && faturarPorMedicao.equals("true") && request.getParameter("valorFatuarPorMedicao") != null
				&& !request.getParameter("valorFatuarPorMedicao").equals("")){
			Double valorContratoFaturarPorMedicao = (Double) Double.parseDouble(request.getParameter("valorFatuarPorMedicao"));
			form.setValor(new Money(valorContratoFaturarPorMedicao));			
			if(form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
				List<Rateioitem> listaNova = form.getRateio().getListaRateioitem();				
				for(Rateioitem rateioitem : listaNova){
					if(rateioitem.getPercentual() != null){
						rateioitem.setValor(form.getValor().multiply(new Money(rateioitem.getPercentual())).divide(new Money(100.0)));
					}
				}
				form.getRateio().setListaRateioitem(listaNova);
			}
		}
		
		String faturarlocacao = request.getParameter("faturarlocacao");
		String valorFaturarlocacao = request.getParameter("valorFaturarlocacao");
		String finalizarContrato = request.getParameter("finalizarContrato");
		
		if(faturarlocacao != null && faturarlocacao.equals("true") && valorFaturarlocacao != null && !valorFaturarlocacao.equals("")){
			form.setFaturarlocacao(Boolean.TRUE);
			form.setValor(new Money(Double.parseDouble(valorFaturarlocacao)));	
			form.setFinalizarContrato(Boolean.valueOf(finalizarContrato != null && !finalizarContrato.equals("") ? finalizarContrato : "false"));
			
			if(form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
				List<Rateioitem> listaNova = form.getRateio().getListaRateioitem();				
				for(Rateioitem rateioitem : listaNova){
					if(rateioitem.getPercentual() != null){
						rateioitem.setValor(form.getValor().multiply(new Money(rateioitem.getPercentual())).divide(new Money(100.0)));
					}
				}
				form.getRateio().setListaRateioitem(listaNova);
			}
		}
		
		return form;
	}

	@Override
	protected void validateBean(Documento bean, BindException errors) {

		//VALIDA OS JUROS
		taxaService.validateTaxas(bean, errors);

		//VALIDA O RATEIO
		try {
			rateioService.validateRateio(bean.getRateio(), bean.getValor());
		} catch (SinedException e) {
			errors.reject("001",e.getMessage());
		}

		//VERIFICA PERIODO DE DATAS
		Date dtemissao = bean.getDtemissao();
		if(dtemissao!= null){
			if(dtemissao.after(bean.getDtvencimento())){
				errors.reject("001","A data de emiss�o deve ser anterior � data de vencimento.");
			}else if(Boolean.TRUE.equals(bean.getFinanciamento()) && SinedUtil.isListNotEmpty(bean.getListaDocumento())){
				for(Documento beanFinanciamento : bean.getListaDocumento()){
					if(beanFinanciamento.getDtvencimento() != null && dtemissao.after(beanFinanciamento.getDtvencimento())){
						errors.reject("001","A data de emiss�o deve ser anterior � data de vencimento da parcela.");
						break;
					}
				}
			}
		}
		
		documentoService.validateDocumento(bean, errors);
		
		if(bean.getCheque() != null && bean.getEmpresa() != null){
			Cheque cheque = chequeService.carregaCheque(bean.getCheque());
			if(cheque != null && cheque.getEmpresa() != null && bean.getEmpresa() != null && !bean.getEmpresa().equals(cheque.getEmpresa())){
				errors.reject("001", "N�o � permitido selecionar cheque de outra empresa.");
			}
		}
	}



	/**
	 * M�todo respons�vel por gerar a lista de financiamento do documento via ajax.
	 * 
	 * @param request
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	@Action("geraFinanciamento")
	public void geraFinanciamento(WebRequestContext request, Documento documento){
		List<Documento> listaDocumento = documentoService.geraFinanciamento(documento);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().convertObjectToJs(listaDocumento, "listaDocFinanciado");
	}
	
	@Action("carregaContaByEmpresa")
	public void carregaContaByEmpresa(WebRequestContext request, Documento documento) throws Exception{
		Empresa empresa = documento.getEmpresa() != null ? documento.getEmpresa() : empresaService.loadPrincipal();
		empresa = empresaService.loadWithContaECarteira(empresa);
	
		Integer cdconta = empresa != null && empresa.getContabancariacontareceber() != null ? empresa.getContabancariacontareceber().getCdconta() : 0;
		Integer cdcontacarteira = empresa != null && empresa.getContacarteiracontareceber() != null ? empresa.getContacarteiracontareceber().getCdcontacarteira() : 0;
		
		if(empresa != null && empresa.getContabancariacontareceber() == null && documento.getConta() != null && documento.getConta().getCdconta() != null){
			cdconta = documento.getConta().getCdconta();
		}
		
		List<Conta> listaConta = contaService.findContaBoletoByEmpresa(empresa);
		List<Contacarteira> listaContacarteira = new ListSet<Contacarteira>(Contacarteira.class);

		for(Conta conta : listaConta){
			if(conta.getCdconta().equals(cdconta) && conta.getListaContacarteira() != null){
				listaContacarteira = conta.getListaContacarteira();
			}
		}
		
		View.getCurrent().convertObjectToJs(listaConta, "lista");
		View.getCurrent().convertObjectToJs(listaContacarteira, "listaContacarteira");
		View.getCurrent().println("var cdconta = " + cdconta + ";");
		View.getCurrent().println("var cdcontacarteira = " + cdcontacarteira + ";");
	}
	
	@Action("carregaVinculoByEmpresa")
	public void carregaVinculoByEmpresa(WebRequestContext request, Documento documento){
		List<Conta> listaVinculo = contaService.findVinculosAtivosByEmpresa(documento.getEmpresa());
		View.getCurrent().convertObjectToJs(listaVinculo, "lista");
	}
	
	/**
	 * A��o vindo da tela de despesa de viagem gerando acerto.
	 *
	 * @param request
	 * @param documento
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@Action("fromDespesaViagemAcerto")
	public ModelAndView fromDespesaViagemAcerto(WebRequestContext request, Documento documento) throws Exception {
		if (documento == null || documento.getWhereInDespesaviagem() == null || documento.getWhereInDespesaviagem().equals("")) {
			request.addError("Despesa de viagem n�o pode ser nulo.");
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
		}			
		

		if("entrada".equals(documento.getController())){
			documento.setController("/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+documento.getWhereInDespesaviagem());
		} else {
			documento.setController("/financeiro/crud/Despesaviagem");
		}
		
		Money valortotal = new Money();
		Colaborador colaborador = null;
		Empresa empresa = null;
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.findForGerarAcerto(documento.getWhereInDespesaviagem());
		if(listaDespesaviagem != null && !listaDespesaviagem.isEmpty()){
			for(Despesaviagem dv : listaDespesaviagem){
				if(colaborador == null) colaborador = dv.getColaborador();
				if(empresa == null) empresa = dv.getEmpresa();
				
				valortotal = valortotal.add(dv.getTotalprevisto());
			}
		}
		
		documento.setEmpresa(empresa);
		documento.setColaborador(colaborador);
		documento.setTipopagamento(Tipopagamento.COLABORADOR);
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		documento.setAdiatamentodespesa(false);
		documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDescricao("Acerto referente � despesa de viagem " + documento.getWhereInDespesaviagem());
		
		Rateio rateio = despesaviagemService.criaRateioForAtualizarvalores(documento.getValor(), listaDespesaviagem, false, true);
		rateio.setValortotaux(documento.getValor().toString());
		rateio.setValoraux(documento.getValor().toString());
		rateioService.atualizaValorRateio(rateio, documento.getValor());
		documento.setRateio(rateio);
		
		Date dataAtual = new Date(System.currentTimeMillis());
		documento.setDtemissao(dataAtual);
		documento.setDtvencimento(dataAtual);
		return continueOnAction("entrada", documento);
	}

	/**
	 * Ajax para obter o n�mero de itens de um prazo de pagamento
	 * 
	 * @see br.com.linkcom.sined.geral.service.PrazopagamentoService#countItens(Prazopagamento)
	 * @param request
	 * @param documento
	 * @author Hugo Ferreira
	 */
	public void ajaxCountPrazoItem(WebRequestContext request, Documento documento) {
		Long count = prazopagamentoService.countItens(documento.getPrazo());
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var count = " + count.toString());
	}
	
	public void getInfoBoleto(WebRequestContext request, Documento documento) {
		Conta conta = contaService.carregaContaComMensagem(documento.getConta(), documento.getContacarteira());
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var mensagem4 = '" + (conta.getContacarteira().getMsgboleto1() != null ? conta.getContacarteira().getMsgboleto1() : "") + "'; var mensagem5 = '" + (conta.getContacarteira().getMsgboleto2() != null ? conta.getContacarteira().getMsgboleto2() : "") + "';");
	}
	
	/**
	 * M�todo para carregar jsp com a informa��o do documento a ser visualizado no pop-up.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#findForVisualizacao(Documentohistorico)
	 * @param request
	 * @param documentohistorico
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("visualizaConta")
	public ModelAndView visualizarDocumento(WebRequestContext request, Documentohistorico documentohistorico){
		if(documentohistorico == null)
			throw new SinedException("N�o foi poss�vel encontrar o documento em nosso sistema.");

		request.setAttribute("TEMPLATE_beanName", documentohistorico);
		request.setAttribute("TEMPLATE_beanClass", Documentohistorico.class);
		request.setAttribute("descricao", "Conta a receber");
		request.setAttribute("consultar", true);
		request.setAttribute("classe", "receber");

		documentohistorico = documentohistoricoService.findForVisualizacao(documentohistorico);

		return new ModelAndView("direct:crud/popup/visualizacaoDocumento").addObject("documentohistorico", documentohistorico);
	}
	
	/**
	 * Action de GERAR RECEITA (LIQUIDAR) de uma nota fiscal
	 *
	 * @see br.com.linkcom.sined.geral.service.NotaService#liquidar(Nota)
	 *
	 * @param request
	 * @param geraReceita
	 *
	 * @author Hugo Ferreira
	 * @throws CrudException 
	 */
	public ModelAndView gerarReceita(WebRequestContext request, GeraReceita geraReceita) throws CrudException {
		if(request.getSession().getAttribute("GERAR_RECEITA__NOTA_BEAN") != null){
			geraReceita = (GeraReceita) request.getSession().getAttribute("GERAR_RECEITA__NOTA_BEAN");
			request.getSession().removeAttribute("GERAR_RECEITA__NOTA_BEAN");
		}else {
			request.setAttribute("clearBase", true);
			request.setAttribute("closeOnCancel", true);
			request.setAttribute("showListagemLink", false);
			request.setAttribute("INSELECTONE", true);
		}
		
		Long countItens = 0l;
		
		if(geraReceita.getPrazoPagamento() != null && geraReceita.getPrazoPagamento().getCdprazopagamento() != null){
			countItens = prazopagamentoService.countItens(geraReceita.getPrazoPagamento());
		}
		
		if (countItens == 1) {
			geraReceita.setQtdeParcelas(geraReceita.getQtdeParcelas() - 1);
		} else {
			if(geraReceita.getPrazoPagamento() != null && geraReceita.getPrazoPagamento().equals(Prazopagamento.UNICA)){
				geraReceita.setQtdeParcelas(0);
			} else {
				geraReceita.setQtdeParcelas(1);
				request.setAttribute("disableRepeticoes", true);
			}
		}
		
		if(geraReceita.getNota() == null || geraReceita.getNota().getCdNota() == null){
			throw new SinedException("Nota n�o pode ser nula.");
		}
		
		Documento documento = documentoService.gerarDocumento(geraReceita);
		documento.setExisteDuplicata(geraReceita.getExisteDuplicata());
		if(documento.getRateio() != null && SinedUtil.isListNotEmpty(documento.getRateio().getListaRateioitem()) && 
				documento.getValor() != null){
			rateioitemService.agruparItensRateio(documento.getRateio().getListaRateioitem());
			rateioService.atualizaValorRateio(documento.getRateio(), documento.getValor());
			rateioService.limpaReferenciaRateio(documento.getRateio());
		}
		
		request.setAttribute("naoCriarInstancia", true);
		request.setAttribute("fromGerarReceita", true);
		return doCriar(request, documento);
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,Documento form) {
		return new ModelAndView("crud/contareceberEntrada");
	}

	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, DocumentoFiltro filtro) {
		return new ModelAndView("crud/contareceberListagem");
	}
	
	public ModelAndView carregaEnderecoCobrancaAjax(WebRequestContext request, Documento documento){
		List<Endereco> listaEnderecos = new ArrayList<Endereco>();
		List<Endereco> listaEnderecosSemInativo = new ArrayList<Endereco>();
		
		if(documento.getPessoa() != null){
			listaEnderecos = contareceberService.carregaEnderecosAPartirTipoPagamento(documento);
			
			//Retirando Inativos
			if (listaEnderecos!=null){
				for (Endereco endereco: listaEnderecos){
					if (!Enderecotipo.INATIVO.equals(endereco.getEnderecotipo())){
						listaEnderecosSemInativo.add(endereco);
					}
				}
			}
		}
		Pessoa pessoa = documento.getPessoa() != null ? pessoaService.loadWithEndereco(new Cliente(documento.getPessoa().getCdpessoa())) : null;
		return new JsonModelAndView().addObject("listaEndereco", listaEnderecosSemInativo).addObject("endereco", pessoa != null ? pessoa.getEndereco() : new ArrayList<Endereco>());
	}

	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Documento bean) {

		String nomeProcessoFluxoAnterior = bean.getNomeProcessoFluxoAnterior();
		
		if(nomeProcessoFluxoAnterior != null && nomeProcessoFluxoAnterior.equals(ContratoCrud.ACAO_CONTRATO_ENTRADA)){
			
			request.clearMessages();
			request.addMessage("Conta receber gerada com sucesso.");
			return new ModelAndView("redirect:/faturamento/crud/Contrato?ACAO=consultar&cdcontrato="+bean.getContrato().getCdcontrato());
			
		}else if(nomeProcessoFluxoAnterior != null && nomeProcessoFluxoAnterior.equals(ContratoCrud.ACAO_CONTRATO_LISTAGEM)){
			
			request.clearMessages();
			request.addMessage("Conta receber gerada com sucesso.");
			return new ModelAndView("redirect:/faturamento/crud/Contrato");
		} else if ("true".equals(request.getParameter("criarNovoAposSalvar"))){
			return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=criar");
		} else if ("true".equals(request.getParameter("fromSelecionarDocumentoPopup"))){
			request.clearMessages();
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>top.parent.parent.selecionarDocumento("+bean.getCddocumento()+");</script>");
			return null;
		} else if ("true".equals(request.getParameter("isAkModal"))){
			request.clearMessages();
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>parent.$.akModalRemove();</script>");
			return null;			
		}
		
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		
		return super.getSalvarModelAndView(request, bean);
	}
	
	/**
	 * Action que lista as contas a receber da pessoa passada por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#findByPessoaDocumentoclasse(Pessoa pessoa, Documentoclasse classe, boolean listagemTotal)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#ajustaNumeroParcelaDocumento(Documento documento)
	 *
	 * @param request
	 * @param pessoa
	 * @return
	 * @since 10/11/2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView listarContas(WebRequestContext request, Pessoa pessoa){
		
		String param = request.getParameter("listagemTotal");
		boolean listagemTotal = param != null && !param.equals("") && param.toLowerCase().equals("true");
		
		List<Documento> listaDocumento = documentoService.findByPessoaDocumentoclasse(pessoa, Documentoclasse.OBJ_RECEBER, listagemTotal);
		for (Documento documento : listaDocumento) {
			documentoService.ajustaNumeroParcelaDocumento(documento);
		}
		
		request.setAttribute("listaDocumento", listaDocumento);
		request.setAttribute("tamanhoLista", listaDocumento.size());
		request.setAttribute("cdpessoa", pessoa.getCdpessoa());
		request.setAttribute("listagemTotal", listagemTotal);
		request.setAttribute("titulo", "CONTAS A RECEBER");
		request.setAttribute("controller", "Contareceber");
		
		request.setAttribute("cddocumentoclasse", Documentoclasse.CD_RECEBER);
		
		return new ModelAndView("direct:crud/popup/listagemDocumento");
	}
	
	/**
	 * M�todo que verifica se a conta a ser salva � uma duplicata de outra j� existente.
	 * @author Rafael Salvio
	 * @date 01/06/2012 
	 */
	public void ajaxVerificaDuplicidade(WebRequestContext request) throws Exception{
		Boolean isContaDuplicada = Boolean.FALSE;
		Boolean bloquearRepeticaoConta = Boolean.FALSE;
		try{
			Documento documento = new Documento();
			
			documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
			documento.setValor(new Money(request.getParameter("valor").replaceAll("\\.", "").replace(",", ".")));
			documento.setDtemissao(SinedDateUtils.stringToDate(request.getParameter("dtemissao")));
			documento.setDtvencimento(SinedDateUtils.stringToDate(request.getParameter("dtvencimento")));
			
			if(request.getParameter("cddocumento")!=null && !"".equals(request.getParameter("cddocumento")) ){
				documento.setCddocumento(Integer.parseInt(request.getParameter("cddocumento")));
			}
			
			if(request.getParameter("empresa")!=null && !"<null>".equals(request.getParameter("empresa"))){
				documento.setEmpresa(new Empresa(Integer.parseInt(request.getParameter("empresa"))));
			}
			
			switch (Integer.parseInt(request.getParameter("tipopagamento"))){
				case 0:{
					documento.setTipopagamento(Tipopagamento.CLIENTE);
					if (StringUtils.isNumeric(request.getParameter("pessoa")))
						documento.setCliente(new Cliente(Integer.parseInt(request.getParameter("pessoa"))));
					break;
				}
				case 1:{
					documento.setTipopagamento(Tipopagamento.COLABORADOR);
					if (StringUtils.isNumeric(request.getParameter("pessoa")))
						documento.setColaborador(new Colaborador(Integer.parseInt(request.getParameter("pessoa"))));
					break;
				}
				case 3:{
					documento.setTipopagamento(Tipopagamento.OUTROS);
					documento.setOutrospagamento(request.getParameter("pessoa"));
					break;
				}
			}

			isContaDuplicada = documentoService.verificaDuplicidade(documento);
		}catch (Exception e) {}
		
		try{
			if(isContaDuplicada != null && isContaDuplicada){
				String param = parametrogeralService.buscaValorPorNome(Parametrogeral.BLOQUEAR_REPETICAO_CONTA);
				if(param != null && !param.trim().isEmpty()){
					bloquearRepeticaoConta = Boolean.parseBoolean(param);
				}
			}
		} catch (Exception e) {}
		
		View.getCurrent().println("var isContaDuplicada = '"+isContaDuplicada.toString()+"';");
		View.getCurrent().println("var bloquearRepeticaoConta = '"+bloquearRepeticaoConta.toString()+"';");
	}

	public ModelAndView verificarTaxaBoleto(WebRequestContext request, Conta conta){
		JsonModelAndView json = new JsonModelAndView();
		List<Taxaitem> listaTaxaitem = null;
		boolean verificador = false;
		String msg = "";
		String mensagem2Form = request.getParameter("mensagem2") != null ? request.getParameter("mensagem2") : "";
		String mensagem3Form = request.getParameter("mensagem3") != null ? request.getParameter("mensagem3") : "";
		StringBuilder msg2 = new StringBuilder();
		StringBuilder msg3 = new StringBuilder();
		String preposicao = "";
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		boolean naoconsiderartaxaboleto = false;
		
		String msgJUROS = "";
		String msgMULTA = "";
		String msgDESCONTO = "";
		String msgDESAGIO = "";
		String msgTAXABOLETO = "";
		String msgTERMO = "";
		String msgTAXAMOVIMENTO = "";
		String msgTAXABAIXA = "";
		
		if (conta != null){
			String paramCdcliente = request.getParameter("cdcliente");
			if(StringUtils.isNotBlank(paramCdcliente)){
				Cliente cliente = clienteService.load(new Cliente(Integer.parseInt(paramCdcliente)), "cliente.cdpessoa, cliente.naoconsiderartaxaboleto");
				if(cliente != null && cliente.getNaoconsiderartaxaboleto() != null){
					naoconsiderartaxaboleto = cliente.getNaoconsiderartaxaboleto();
				}
			}
			conta = contaService.loadWithTaxa(conta);
			if (conta.getTaxaboleto() != null && conta.getTaxaboleto().toLong() > 0){
				verificador = true;
				json.addObject("dataRetorno", SinedDateUtils.currentDate());
				json.addObject("valorRetorno", conta.getTaxaboleto());
			}
			if(conta.getTaxa() != null && conta.getTaxa().getListaTaxaitem() != null && !conta.getTaxa().getListaTaxaitem().isEmpty()){
				listaTaxaitem = new ArrayList<Taxaitem>();
				String dtvencimento = request.getParameter("dtvencimento");
				if(dtvencimento != null && !"".equals(dtvencimento)){
					try {
						Date dataparcela = SinedDateUtils.stringToDate(dtvencimento);
						for(Taxaitem taxaitem : conta.getTaxa().getListaTaxaitem()){
							if(taxaitem.getDias() != null){
								if(Tipocalculodias.ANTES_VENCIMENTO.equals(taxaitem.getTipocalculodias())){
									taxaitem.setDtlimite(SinedDateUtils.addDiasData(dataparcela, -taxaitem.getDias()));
								}else {
									taxaitem.setDtlimite(SinedDateUtils.addDiasData(dataparcela, taxaitem.getDias()));
								}
								if(taxaitem.getGerarmensagem() != null && taxaitem.getGerarmensagem()){
									if(tipotaxaService.isApos(taxaitem.getTipotaxa())){
										preposicao = " ap�s ";
									}else {
										preposicao = " at� ";
									}
									msg = taxaitem.getTipotaxa().getNome() + " de " +(taxaitem.isPercentual() ? "" : "R$") + taxaitem.getValor() + (taxaitem.isPercentual() ? "%" : "") + preposicao + format.format(taxaitem.getDtlimite()) + ". ";
									if((msg2.length() + msg.length() + mensagem2Form.length()) <= 80){
										msg2.append(msg);
									}else if((msg3.length() + msg.length() + mensagem3Form.length()) <= 80){
										msg3.append(msg);
									}
									
									if(Tipotaxa.JUROS.equals(taxaitem.getTipotaxa())) msgJUROS = msg;
									else if(Tipotaxa.MULTA.equals(taxaitem.getTipotaxa())) msgMULTA = msg;
									else if(Tipotaxa.DESCONTO.equals(taxaitem.getTipotaxa())) msgDESCONTO = msg;
									else if(Tipotaxa.DESAGIO.equals(taxaitem.getTipotaxa())) msgDESAGIO = msg;
									else if(Tipotaxa.TAXABOLETO.equals(taxaitem.getTipotaxa())) msgTAXABOLETO = msg;
									else if(Tipotaxa.TERMO.equals(taxaitem.getTipotaxa())) msgTERMO = msg;
									else if(Tipotaxa.TAXAMOVIMENTO.equals(taxaitem.getTipotaxa())) msgTAXAMOVIMENTO = msg;
									else if(Tipotaxa.TAXABAIXA_DESCONTO.equals(taxaitem.getTipotaxa())) msgTAXABAIXA = msg;
								}
							}
						}
					}catch (Exception e) {} 
				}
				listaTaxaitem.addAll(conta.getTaxa().getListaTaxaitem());
			}
		}
		json.addObject("verificador", verificador);
		json.addObject("taxas", listaTaxaitem);
		json.addObject("msg2", msg2.toString());
		json.addObject("msg3", msg3.toString());
		json.addObject("naoconsiderartaxaboleto", naoconsiderartaxaboleto);
		
		json.addObject("msgJUROS", msgJUROS);
		json.addObject("msgMULTA", msgMULTA);
		json.addObject("msgDESCONTO", msgDESCONTO);
		json.addObject("msgDESAGIO", msgDESAGIO);
		json.addObject("msgTAXABOLETO", msgTAXABOLETO);
		json.addObject("msgTERMO", msgTERMO);
		json.addObject("msgTAXAMOVIMENTO", msgTAXAMOVIMENTO);
		json.addObject("msgTAXABAIXA", msgTAXABAIXA);
		return json;
	}
	
	/**
	 * M�todo ajax para verificar se os documentos s�o de um mesmo cliente
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxVerificaClienteDocumento(WebRequestContext request){
		JsonModelAndView json = new JsonModelAndView();
		boolean permitirassociar = true;
		String cds = request.getParameter("selectedItens");
		if (cds != null && !"".equals(cds)){
			List<Documento> listaDocumento = contareceberService.findForPermitirAssociar(cds);
			if(listaDocumento != null && !listaDocumento.isEmpty()){
				for(Documento d1 : listaDocumento){
					for(Documento d2 : listaDocumento){
						if(!d1.getPessoa().getCdpessoa().equals(d2.getPessoa().getCdpessoa())){
							permitirassociar = false;
							break;
						}
					}
					if(!permitirassociar) break;
				}
			}
			
		}
		json.addObject("permitirassociar", permitirassociar);
		return json;
	}
	
	/**
	* M�todo para criar nota fiscal a partir da conta a receber
	*
	* @param request
	* @return
	* @since Oct 18, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView criarNotafiscalContareceber(WebRequestContext request) throws CrudException{
		
		String contas = SinedUtil.getItensSelecionados(request);
		if(contas == null || contas.trim().equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(contas);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		String[] ids = contas.split(",");
		
		Integer cdcliente = null;
		
		Boolean hasError = Boolean.FALSE;
		for (int i = 0; i < ids.length; i++) {
			Integer id = Integer.valueOf(ids[i]);
			
			Integer vendaFaturada = vendapagamentoService.verificaContaVendaFaturada(id);
			
			Documento documento = documentoService.carregaDocumento(new Documento(id));
			if(i == 0 && documento.getPessoa() != null){
				cdcliente = documento.getPessoa().getCdpessoa();
			} else if(documento.getPessoa() == null || !documento.getPessoa().getCdpessoa().equals(cdcliente)){
				request.addError("S� � permitido gerar nota fiscal de contas que tenham o mesmo cliente.");
				hasError = Boolean.TRUE;
			}

			if(!contareceberService.verficaContaBaixadaDefinitiva(id)){			
				request.addError("S� � permitido gerar nota fiscal da conta que estiver na situa��o baixada ou definitiva.");
				hasError = Boolean.TRUE;
			}
			else if(notaDocumentoService.verificaContaNota(id)){
				request.addError("N�o � permitido gerar nota fiscal de uma conta a receber proveniente de uma nota.");
				hasError = Boolean.TRUE;
			}
			else if(vendaFaturada != null){
				request.addError("N�o � permitido gerar nota fiscal de uma conta a receber proveniente de uma venda faturada.");
				hasError = Boolean.TRUE;
			}
		}
		
		if(hasError)
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		else{
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO=criarNotafiscalContareceber&contas="+contas);
		}
	}
	
	public ModelAndView criarNotas(WebRequestContext request){
		try{
			String whereIn = SinedUtil.getItensSelecionados(request);
			if(whereIn == null || whereIn.trim().equals(""))
				throw new SinedException("Nenhum item selecionado.");
			
			//Verifica��o de data limite do ultimo fechamento. 
			Documento aux = new Documento();
			aux.setWhereIn(whereIn);
			if(fechamentofinanceiroService.verificaListaFechamento(aux)){
				throw new SinedException("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
			}
			
			List<Documento> listaDocumentoContrato = new ArrayList<Documento>();
			
			String[] ids = whereIn.split(",");
			for (int i = 0; i < ids.length; i++) {
				Documento documento = new Documento(Integer.valueOf(ids[i]));
				
//				COMENTADO ISSO PARA O BRIEFCASE
//				if(!contareceberService.verficaContaBaixadaDefinitiva(documento.getCddocumento())){			
//					request.addError("Conta a receber " + documento.getCddocumento() + " - S� � permitido gerar nota fiscal da conta que estiver na situa��o baixada ou definitiva.");
//				}
//				else 
				if(notaDocumentoService.verificaContaNota(documento.getCddocumento())){
					request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de uma conta a receber que tenha nota vinculada.");
				}
				else if(vendapagamentoService.verificaContaVendaFaturada(documento.getCddocumento()) != null){
					request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de uma conta a receber proveniente de uma venda faturada.");
				}
				else if(!faturamentohistoricoService.haveHistoricoDocumento(documento)){
					request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal avulsa na op��o de 'Criar Nota(s)', favor utilizar 'Criar NF de Servi�o'.");
				}
				else if(documentoorigemService.isOrigemVenda(documento)){
					request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de origem da venda na op��o de 'Criar Nota(s)', favor utilizar 'Criar NF de Produto'.");
				}  else {
					listaDocumentoContrato.add(documento);
				}
			}
			
			if(listaDocumentoContrato != null && listaDocumentoContrato.size() > 0){
				faturamentohistoricoService.realizarFaturamentoServicoDocumento(request, CollectionsUtil.listAndConcatenate(listaDocumentoContrato, "cddocumento", ","), false, false);
				faturamentohistoricoService.realizarFaturamentoProdutoDocumento(request, listaDocumentoContrato, null);
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		}
		
		return new ModelAndView("redirect:/financeiro/crud/Contareceber");
	}
	
	/**
	 * Action que cria NF de servi�o a partir das contas a receber.
	 *
	 * @param request
	 * @return
	 * @throws CrudException
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView criarNFServico(WebRequestContext request) throws CrudException {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.trim().equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		List<Documento> listaDocumentoContrato = new ArrayList<Documento>();
		List<Documento> listaDocumentoAvulsa = new ArrayList<Documento>();
		
		boolean error = false;
		String[] ids = whereIn.split(",");
		for (int i = 0; i < ids.length; i++) {
			Documento documento = new Documento(Integer.valueOf(ids[i]));
			
			if(!contareceberService.verficaContaBaixadaDefinitiva(documento.getCddocumento())){			
				request.addError("Conta a receber " + documento.getCddocumento() + " - S� � permitido gerar nota fiscal da conta que estiver na situa��o baixada ou definitiva.");
				error = true;
			}
			else if(notaDocumentoService.verificaContaNota(documento.getCddocumento(), NotaTipo.NOTA_FISCAL_SERVICO)){
				request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de uma conta a receber que tenha nota vinculada.");
				error = true;
			}
			else if(vendapagamentoService.verificaContaVendaFaturada(documento.getCddocumento()) != null){
				request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de uma conta a receber proveniente de uma venda faturada.");
				error = true;
			}
			else if(notaVendaService.existNotaEmitida(documento, null)){
				request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de uma conta a receber proveniente de uma venda que tenha nota vinculada.");
				error = true;
			}
			else if(faturamentohistoricoService.haveHistoricoDocumento(documento)){
				listaDocumentoContrato.add(documento);
			} else {
				listaDocumentoAvulsa.add(documento);
			}
		}
		
		if(error) {	
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		if(listaDocumentoAvulsa != null && listaDocumentoAvulsa.size() > 0 &&
				listaDocumentoContrato != null && listaDocumentoContrato.size() > 0){
			request.addError("N�o � permitido gerar nota fiscal de conta(s) a receber avulsa junto com conta(s) a receber proveniente de faturamento de contrato. Favor fazer essa gera��o separadamente.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		if(listaDocumentoAvulsa != null && listaDocumentoAvulsa.size() > 0){
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO=criarNotafiscalContareceber&contas="+whereIn);
		}
		
		if(listaDocumentoContrato != null && listaDocumentoContrato.size() > 0){
			faturamentohistoricoService.realizarFaturamentoServicoDocumento(request, whereIn, false, false);
		}
		
		return new ModelAndView("redirect:/financeiro/crud/Contareceber");
	}
	
	public ModelAndView criarNFProduto(WebRequestContext request) throws CrudException {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.trim().equals(""))
			throw new SinedException("Nenhum item selecionado.");
		
		//Verifica��o de data limite do ultimo fechamento. 
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem contas cuja data de vencimento refere-se a um per�odo j� fechado.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		List<Documento> listaDocumentoContrato = new ArrayList<Documento>();
		List<Documento> listaDocumentoVenda = new ArrayList<Documento>();
		
		boolean error = false;
		String[] ids = whereIn.split(",");
		for (int i = 0; i < ids.length; i++) {
			Documento documento = new Documento(Integer.valueOf(ids[i]));
			
			if(!contareceberService.verficaContaBaixadaDefinitiva(documento.getCddocumento())){			
				request.addError("Conta a receber " + documento.getCddocumento() + " - S� � permitido gerar nota fiscal da conta que estiver na situa��o baixada ou definitiva.");
				error = true;
			}
			else if(notaDocumentoService.verificaContaNota(documento.getCddocumento(), NotaTipo.NOTA_FISCAL_PRODUTO)){
				request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de uma conta a receber que tenha nota vinculada.");
				error = true;
			}
			else if(vendapagamentoService.verificaContaVendaFaturada(documento.getCddocumento()) != null){
				request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de uma conta a receber proveniente de uma venda faturada.");
				error = true;
			}
			else if(notaVendaService.existNotaEmitida(documento, null)){
				request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de uma conta a receber proveniente de uma venda que tenha nota vinculada.");
				error = true;
			}
			else if(faturamentohistoricoService.haveHistoricoDocumento(documento)){
				listaDocumentoContrato.add(documento);
			} 
			else if(documentoorigemService.isOrigemVenda(documento)){
				listaDocumentoVenda.add(documento);
			} 
			else {
				request.addError("Conta a receber " + documento.getCddocumento() + " - N�o � permitido gerar nota fiscal de produto de uma conta a receber avulsa.");
				error = true;
			}
		}
		
		if(error) {	
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		faturamentohistoricoService.realizarFaturamentoProdutoDocumento(request, listaDocumentoContrato, listaDocumentoVenda);
		
		return new ModelAndView("redirect:/financeiro/crud/Contareceber");
	}
	
	/**
	 * M�todo que abre a tela de conta a receber para Gerar receita
	 *
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public ModelAndView criarGerarreceitaServico(WebRequestContext request) throws Exception {
		String whereIn = request.getParameter("selectedNotasGerarreceita");
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico");
		}
		
		Documento documento = contareceberService.gerarReceitaNota(whereIn);
		if(documento.getRateio() != null && SinedUtil.isListNotEmpty(documento.getRateio().getListaRateioitem())){
			rateioService.ajustaPercentualRateioitem(documento.getValor(), documento.getRateio().getListaRateioitem());
		}
		rateioService.atualizaValorRateio(documento.getRateio(), documento.getValor());
		
		boolean boleto = false;
		if(documento.getDocumentotipo() != null && documento.getDocumentotipo().getBoleto() != null && documento.getDocumentotipo().getBoleto()){
			boleto = true;
		}
		if(!boleto && SinedUtil.isListNotEmpty(documento.getListaDocumento())){
			for(Documento doc : documento.getListaDocumento()){
				if(doc.getDocumentotipoTransient() != null && doc.getDocumentotipoTransient().getBoleto() != null && doc.getDocumentotipoTransient().getBoleto()){
					boleto = true;
					break;
				}
			}
		}
		
		if(!boleto){
			documento.setConta(null);
			documento.setContacarteira(null);
		}
		
		documento.setFromGerarreceitanota(true);
		documento.setWhereInNota(whereIn);
		setInfoForTemplate(request, documento);
		setEntradaDefaultInfo(request, documento);
		entrada(request, documento);
		request.setAttribute("tipoBoleto", boleto);
//		request.setAttribute("podeAlterarValor", SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR"));
		if (documento.getPodeAlterarValor()==null){
			documento.setPodeAlterarValor(SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR"));
		}
		return getEntradaModelAndView(request, documento);
	}
	
	public void ajaxVerificarNotaFiscalServicoEmEspera(WebRequestContext request){
		boolean possuiNotaFiscalServicoEmEspera = notaFiscalServicoService.isPossuiNotaFiscalServico(request.getParameter("selectedItens"), new NotaStatus[]{NotaStatus.EM_ESPERA});
		View.getCurrent().print("var possuiNotaFiscalServicoEmEspera = " + possuiNotaFiscalServicoEmEspera + ";");
	}
	
	public ModelAndView buscarOperacaocontabil(WebRequestContext request, Documento form) throws Exception {
		
		Operacaocontabil operacaocontabil = null;
		if (form.getCliente()!=null && form.getCliente().getCdpessoa()!=null){
			operacaocontabil = operacaocontabilService.load(form.getCliente(), "operacaocontabil");
		}else if (form.getFornecedor()!=null && form.getFornecedor().getCdpessoa()!=null){
			operacaocontabil = operacaocontabilService.load(form.getFornecedor(), "operacaocontabil");
		}
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("encontrou", operacaocontabil!=null);
		if (operacaocontabil!=null){
			jsonModelAndView.addObject("value", "br.com.linkcom.sined.geral.bean.Operacaocontabil[cdoperacaocontabil=" + operacaocontabil.getCdoperacaocontabil() + "]");			
			jsonModelAndView.addObject("label", operacaocontabil.getDescricaoAutocomplete());			
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView alterarLote(WebRequestContext request) throws Exception {
		request.setAttribute("selectedItens", SinedUtil.getItensSelecionados(request));
		request.setAttribute("listaConta", contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA));
		request.setAttribute("podeAlterarValor", SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR"));
		return new ModelAndView("direct:crud/popup/alterarContaReceberLote");
	}	
	
	public ModelAndView salvarAlterarLote(WebRequestContext request, DocumentoFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhum item selecionado");
		}
		
		documentoService.alterarContaReceberLote(whereIn, filtro);
		if(Boolean.TRUE.equals(filtro.getAtualizarBoletoETaxas()))
			documentoService.updateBoletoETaxa(whereIn, filtro.getContaLote(), filtro.getContacarteiraLote());
		documentoService.atualizaValorRateio(whereIn, filtro.getValorLote());
		documentoService.callProcedureAtualizaDocumento(whereIn);
		
		request.addMessage("Contas a receber alteradas com sucesso.", MessageType.INFO);
		request.getServletResponse().setContentType("text/html");
		
		View.getCurrent().println("<script>" +
								  //"window.parent.location = window.parent.location;" +
								  "window.parent.location = '" + request.getServletRequest().getContextPath() + "/financeiro/crud/Contareceber';"+
								  "window.parent.$.akModalRemove(true);" +
								  "</script>");
		
		return null;
	}	
	
	/**
	 * M�todo que busca a conta bancaria do Tipo de Documento selecionado.
	 * 
	 * @param request, documentotipo
	 * @return
	 * @throws Exception
	 * @author Lucas Costa
	 */
	public ModelAndView buscarContaBancaria(WebRequestContext request, Documentotipo documentotipo) throws Exception {
		Documentotipo documentotipoAux = documentotipoService.findForContaReceber(documentotipo);
		JsonModelAndView json = new JsonModelAndView();
		boolean boleto = false;
		if(documentotipoAux != null){
			boleto = documentotipoAux.getBoleto() != null && documentotipoAux.getBoleto();
			if(documentotipoAux.getContadestino() != null){
				json.addObject("conta", documentotipoAux.getContadestino());
				if (documentotipoAux.getContadestino().getListaContacarteira() != null && !documentotipoAux.getContadestino().getListaContacarteira().isEmpty()){
					for(Contacarteira contacarteira : documentotipoAux.getContadestino().getListaContacarteira()){
						if (contacarteira.isPadrao()){
							json.addObject("contacarteira", contacarteira);
							break;
						}
					}
				}
				else{
					json.addObject("contacarteira", null);			
				}
			}
		}
		if(!boleto){
			String whereInDocumentoTipoTransient = request.getParameter("whereInDocumentoTipoTransient");
			if(StringUtils.isNotBlank(whereInDocumentoTipoTransient)){
				List<Documentotipo> lista = documentotipoService.findDocumentoTipoBoleto(whereInDocumentoTipoTransient);
				boleto = SinedUtil.isListNotEmpty(lista);
			}
		}
		json.addObject("boleto", boleto);
		return json;
	}
	
	public ModelAndView buscarContaBancariaVinculoprovisionado(WebRequestContext request, Documentotipo documentotipo){
		Documentotipo documentotipoAux = documentotipoService.findForContaReceber(documentotipo);
		JsonModelAndView json = new JsonModelAndView();
		
		if(documentotipoAux != null && documentotipoAux.getContadestino() != null){
			json.addObject("conta", documentotipoAux.getContadestino());
		}
		return json;
	}
	
	/**
	 * M�todo utilizado para abrir o popup de observa��o
	 * ao editar uma conta a receber
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public ModelAndView abrirPopupObservacao(WebRequestContext request, Documento bean) throws Exception{
		request.setAttribute("msg", "Justificativa para a altera��o");
		return new ModelAndView("direct:crud/popup/contaReceberObservacao").addObject("documento", bean);
	}
	
	/**
	* M�todo que cria o reembolso de despesa de viagem
	*
	* @param request
	* @return
	* @throws Exception
	* @since 02/07/2015
	* @author Luiz Fernando
	*/
	public ModelAndView reembolsoDespesaviagem(WebRequestContext request) throws Exception{
		String urlRedirectDespesaviagem = "redirect:/financeiro/crud/Despesaviagem";
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView(urlRedirectDespesaviagem);
		}
		
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		if(entrada){
			urlRedirectDespesaviagem += "?ACAO=consultar&cddespesaviagem=" + whereIn;
		}
		
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.findForReembolso(whereIn);
		if(SinedUtil.isListEmpty(listaDespesaviagem)){
			request.addError("Nenhum item encontrado.");
			return new ModelAndView(urlRedirectDespesaviagem);
		}
		
		Integer cdpessoa = null;
		for(Despesaviagem despesaviagem : listaDespesaviagem){
			if(!Situacaodespesaviagem.BAIXADA.equals(despesaviagem.getSituacaodespesaviagem()) || 
					(despesaviagem.getReembolsavel() == null || !despesaviagem.getReembolsavel())){
				request.addError("� permitido gerar reembolso somente para despesa na situa��o de 'Baixada' e que tenha o flag 'reembolso' marcado");
				return new ModelAndView(urlRedirectDespesaviagem);
			}
			
			if(despesaviagem.getReembolsogerado() != null && despesaviagem.getReembolsogerado()){
				request.addError("� permitido gerar reembolso somente para despesa que n�o tem reembolso gerado.");
				return new ModelAndView(urlRedirectDespesaviagem);
			}
			
			boolean clienteFornecedorDiferente = false;
			if(despesaviagem.getFornecedor() != null){
				if(cdpessoa == null){
					cdpessoa = despesaviagem.getFornecedor().getCdpessoa();
				}else if(!cdpessoa.equals(despesaviagem.getFornecedor().getCdpessoa())){
					clienteFornecedorDiferente = true;
				}
			}else if(despesaviagem.getCliente() != null){
				if(cdpessoa == null){
					cdpessoa = despesaviagem.getCliente().getCdpessoa();
				}else if(!cdpessoa.equals(despesaviagem.getCliente().getCdpessoa())){
					clienteFornecedorDiferente = true;
				}
			} 
			
			if(clienteFornecedorDiferente){
				request.addError("� permitido gerar reembolso somente para o mesmo Cliente ou Fornecedor");
				return new ModelAndView(urlRedirectDespesaviagem);
			}
		}
		
		
		Documento form = despesaviagemService.criaDocumentoReemboso(listaDespesaviagem);
		Date dataAtual = new Date(System.currentTimeMillis());
		form.setDtemissao(dataAtual);
		form.setDtvencimento(dataAtual);
		
		form.setController("/financeiro/crud/Despesaviagem" + (entrada ? "?ACAO=consultar&cddespesaviagem="+whereIn : "?ACAO=listagem&mostrarReembolso=true"));
		setInfoForTemplate(request, form);
		setEntradaDefaultInfo(request, form);
		entrada(request, form);
		return getEntradaModelAndView(request, form);
	}
	
	/**
	 * Atecipar uma ou mais contas a receber.
	 * @param request
	 * @author Murilo
	 */
	public ModelAndView antecipar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		List<Documento> lstContaReceber = contareceberService.findForAntecipacao(whereIn, false, Documentoacao.DEFINITIVA, Documentoacao.PREVISTA);
		if(lstContaReceber == null || lstContaReceber.size() != whereIn.split(",").length){
			request.addError("N�o � poss�vel antecipar contas com situa��o Baixada, Baixada Parcial, Cancelada, Negociada ou Antecipada.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		contareceberService.modificarAntecipadoDaConta(lstContaReceber, true);
		request.addMessage("Conta(s) antecipada(s) com sucesso!", MessageType.INFO);
		return new ModelAndView("redirect:/financeiro/crud/Contareceber");
	}
	/**
	 * Cancelar a antecipa��o de uma ou mais contas a receber.
	 * @param request
	 * @return
	 * @author Murilo
	 */
	public ModelAndView cancelarAntecipacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		List<Documento> listaContaReceber = contareceberService.findForAntecipacao(whereIn, true);
		if(listaContaReceber == null || listaContaReceber.size() != whereIn.split(",").length){
			request.addError("A(s) conta(s) selecionadas " + whereIn + " n�o foram Antecipada(s), selecione apenas conta(s) Antecipada(s)");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		contareceberService.modificarAntecipadoDaConta(listaContaReceber, false);
		request.addMessage("Cancelamento de antecipa��o realizado com sucesso!", MessageType.INFO);
		return new ModelAndView("redirect:/financeiro/crud/Contareceber");
	}
	
	public ModelAndView ajaxValidaBoletosNaoGerar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		return contareceberService.retornaJsonParaValidarEmissaoDeBoletos(whereIn);
	}
	
	public ModelAndView sugereTaxa(WebRequestContext request, Documento documento){
		Documentotipo documentotipo = documentotipoService.load(documento.getDocumentotipo());
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(!Boolean.TRUE.equals(documentotipo.getTaxanoprocessamentoextratocartaocredito())){
			if(documentotipo.getTipotaxacontareceber() != null && documentotipo.getTipotaxacontareceber().getCdtipotaxa() != null){
				jsonModelAndView.addObject("tipotaxaVal", Tipotaxa.class.getName() + "[cdtipotaxa=" + documentotipo.getTipotaxacontareceber().getCdtipotaxa() + "]");
			}else{
				jsonModelAndView.addObject("tipotaxaVal", Tipotaxa.class.getName() + "[cdtipotaxa=" + Tipotaxa.TAXAMOVIMENTO.getCdtipotaxa() + "]");
			}
			jsonModelAndView.addObject("percentualVal", String.valueOf(Boolean.TRUE.equals(documentotipo.getPercentual())));
			jsonModelAndView.addObject("valorVal", documentotipo.getTaxavenda()!=null ? new Money(documentotipo.getTaxavenda()) : "");
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxValidaCheque(WebRequestContext request, Cheque cheque){
		Money valorcheque = chequeService.getValor(cheque);
		return new JsonModelAndView().addObject("valorcheque", valorcheque != null ? SinedUtil.descriptionDecimal(valorcheque.getValue().doubleValue(), true) : null);
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.ajaxBuscaInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView abrirPopUpInformacaoClienteVenda(WebRequestContext request, Cliente cliente){
		return vendaService.abrirPopUpInformacaoClienteVenda(request, cliente);
	}
	
	public ModelAndView alterarTipoDocumento(WebRequestContext request) throws Exception {
		return documentoService.abrirPopupAlterarTipoDocumento(request);
	}
	
	public ModelAndView salvarAlterarTipoDocumento(WebRequestContext request, DocumentoFiltro filtro) throws Exception {
		documentoService.salvarPopupAlterarTipoDocumento(request, filtro, "/financeiro/crud/Contareceber");
		filtro.setDocumentotipo(null);
		return null;
	}
	
	@Action("createRateioByModelo")
	public void createRateioByModelo(WebRequestContext request, Documento documento){
		if(documento.getRateiomodelo() != null && documento.getRateiomodelo().getCdrateiomodelo() != null){
			Rateio rateio = rateioService.createRateioByModelo(documento.getRateiomodelo());
			View.getCurrent().convertObjectToJs(rateio, "rateio");			
		}
	}
	
	public void ajaxVerificaFeriado(WebRequestContext request){
		calendarioService.ajaxVerificaFeriado(request);
	}
	
	public void ajaxProximoDiaUtil(WebRequestContext request){
		calendarioService.ajaxProximoDiaUtil(request);
	}
	
	public ModelAndView ajaxListaTemplatesReport(WebRequestContext request) {
		String selectedItens = request.getParameter("selectedItens");
		JsonModelAndView json = new JsonModelAndView();
		
		Empresa empresa = empresaService.getEmpresaByDocumentos(selectedItens);
		if (empresa != null && FormaEnvioBoletoEnum.EMAIL_CONFIGURAVEL.equals(empresa.getFormaEnvioBoleto())) {
			List<ReportTemplateBean> reportTemplateList = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMAIL_BOLETO);
			json.addObject("reportTemplateListSize", reportTemplateList.size());
			
			if(reportTemplateList.size() == 1){
				json.addObject("cdreporttemplate", reportTemplateList.get(0).getCdreporttemplate());
			}
		} else {
			json.addObject("reportTemplateListSize", null);
		}
		
		return json;
	}
	
	public ModelAndView abrirImprimirReportTemplate(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request,"/financeiro/crud/Contareceber");
			return null;
		}
		
		List<ReportTemplateBean> templates = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMAIL_BOLETO, "assunto");
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("reportTemplateList", templates);
		
		return new ModelAndView("direct:/crud/popup/reportTemplate");
	}
	
	public ModelAndView abrirRateio(WebRequestContext request){
		return rateioService.abrirRateio(request);
	}
	
	public ModelAndView ajaxIsApropriacao(WebRequestContext request, Documento documento){
		return documentoService.ajaxIsApropriacao(request, documento);
	}
	
	public void ajaxMontaApropriacoes(WebRequestContext request, Documento documento){
		documentoService.ajaxMontaApropriacoes(request, documento);
	}
}