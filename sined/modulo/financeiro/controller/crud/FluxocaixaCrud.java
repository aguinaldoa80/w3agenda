package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Fluxocaixa;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.FluxocaixaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/financeiro/crud/Fluxocaixa", authorizationModule=CrudAuthorizationModule.class)
public class FluxocaixaCrud extends CrudControllerSined<FluxocaixaFiltro, Fluxocaixa, Fluxocaixa>{

	private RateioService rateioService;
	private CentrocustoService centrocustoService;
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Fluxocaixa form)throws Exception {
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		request.setAttribute("tipooperacao", form.getTipooperacao());
	}
	
	@Override
	protected Fluxocaixa carregar(WebRequestContext request, Fluxocaixa bean)throws Exception {
		Fluxocaixa fluxocaixa = super.carregar(request, bean);
		rateioService.calculaValoresRateio(fluxocaixa.getRateio(), fluxocaixa.getValor());
		return fluxocaixa;
	}
	
	@Override
	protected void validateBean(Fluxocaixa bean, BindException errors) {
		
		//VALIDA O RATEIO
		try {
			rateioService.validateRateio(bean.getRateio(), bean.getValor());
		} catch (SinedException e) {
			errors.reject("001",e.getMessage());
		}

	}
}
