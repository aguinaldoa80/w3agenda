package br.com.linkcom.sined.modulo.financeiro.controller.crud;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoacaoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.EmailCobrancaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobranca;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobrancaDocumento;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.EmailCobrancaFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


/**
 * @author Andrey Leonardo
 * @since 23/07/2015
 */
@Controller(path="/financeiro/crud/EmailCobranca", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"documentos", "listaEmailCobrancaDocumento[0].documento.pessoa.nome", "email", "dataenvio", "ultimaverificacao", "situacaoemail", "envioAutomatico"})
public class EmailCobrancaCrud extends CrudControllerSined<EmailCobrancaFiltro, EmailCobranca, EmailCobranca>{
	EmailCobrancaService emailCobrancaService;
	DocumentoacaoService documentoacaoService;
	DocumentoService documentoService;
	ContareceberService contareceberService;
	DocumentohistoricoService documentohistoricoService;
	
	public void setDocumentoacaoService(DocumentoacaoService documentoacaoService) {
		this.documentoacaoService = documentoacaoService;
	}
	public void setEmailCobrancaService(EmailCobrancaService emailCobrancaService) {
		this.emailCobrancaService = emailCobrancaService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected void listagem(WebRequestContext request, EmailCobrancaFiltro filtro) throws Exception {
		List<EmailStatusEnum> listaEmailStatus = new ArrayList<EmailStatusEnum>();
		listaEmailStatus.add(EmailStatusEnum.CAIXA_DE_SPAM);
		listaEmailStatus.add(EmailStatusEnum.EMAIL_INVALIDO);
		listaEmailStatus.add(EmailStatusEnum.ENVIADO);
		listaEmailStatus.add(EmailStatusEnum.LIDO);
		listaEmailStatus.add(EmailStatusEnum.ENTREGUE);
		
		request.setAttribute("listaAcaoCompleta", documentoacaoService.findForContareceber());
		request.setAttribute("listaStatus", listaEmailStatus);
	}
	
	@Override
	protected void excluir(WebRequestContext request, EmailCobranca bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir um registro de email.");		
	}	
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, EmailCobranca form)
			throws CrudException {
		throw new SinedException("N�o � poss�vel salvar um registro de email.");
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, EmailCobranca form)
			throws CrudException {
		throw new SinedException("N�o � poss�vel criar ou alterar um registro de email.");
	}
	
	public void ajaxIsReenvioValido(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		boolean isEmailLido = emailCobrancaService.isEmailLido(whereIn);
		boolean isEmailInvalido = emailCobrancaService.isEmailInvalido(whereIn);		
		
		View.getCurrent().print("var isEmailLido = " + isEmailLido + ";" +
				"var isEmailInvalido = " + isEmailInvalido + ";");
	}
	
	/**
	 * @param request
	 * @return
	 * @author Andrey Leonardo
	 */
	public ModelAndView reenviarEmail(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		String whereInDocumento = emailCobrancaService.createWhereInReenvioBoleto(whereIn);
		List<String> listaErro = documentoService.verificaSituacaoDocumento(whereInDocumento, true);
		
		if(listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			request.addError("Erro na gera��o de boleto.");
			return new ModelAndView("redirect:/financeiro/crud/EmailCobranca");
		}
		 
		int sucesso = 0;
		int erro = 0; 
		List<EmailCobranca> listaEmailCobranca = emailCobrancaService.loadEmailCobrancaToReenvio(whereIn);		
		for(EmailCobranca emailCobranca : listaEmailCobranca){
			String whereInDocumentoEmailCobranca = "";
			
			try {
				for (EmailCobrancaDocumento emailCobrancaDocumento : emailCobranca.getListaEmailCobrancaDocumento()) {
					whereInDocumentoEmailCobranca += emailCobrancaDocumento.getDocumento().getCddocumento() + ",";
				}
				
				if (StringUtils.isNotEmpty(whereInDocumentoEmailCobranca) && whereInDocumentoEmailCobranca.length() > 1) {
					whereInDocumentoEmailCobranca = whereInDocumentoEmailCobranca.substring(0, whereInDocumentoEmailCobranca.length() - 1);
				}
				
				List<Documento> listaDocumento = documentoService.findForBoleto(whereInDocumentoEmailCobranca);
				
				String paraEnvioEmailCliente = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.ENVIOFINANCEIROMAILPRINCIPALCLIENTE);
				if (!paraEnvioEmailCliente.equals("FALSE") && StringUtils.isNotEmpty(emailCobranca.getEmail())){					
					try {
						for (Documento documento : listaDocumento) {
							Report report = new Report("/financeiro/relBoleto");
							report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(documento, false)});
							
							contareceberService.enviaBoleto(documento, report, null, null, null, emailCobranca);
						}
						
						sucesso++;
					} catch (Exception e) {
						e.printStackTrace();
						listaErro.add("Erro ao enviar o email " + whereInDocumentoEmailCobranca + ": " + e.getMessage());
						erro++;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				listaErro.add("Erro na gera��o do boleto da conta " + whereInDocumentoEmailCobranca + ": " + e.getMessage());
				erro++;
			}
		}
		if(sucesso > 0){
			request.addMessage(sucesso + " e-mail(s) enviado(s) com sucesso.");
		}
		if(erro > 0){
			request.addError(erro + " email(s) n�o enviado(s). ");
			if(listaErro != null && !listaErro.isEmpty()){
				for(String msgErro : listaErro){
					request.addError(msgErro);
				}
			}
		}
		return new ModelAndView("redirect:/financeiro/crud/EmailCobranca");
	}
	
	
	/**
	 * @param request
	 * @param emailCobranca
	 * @return
	 * @author Andrey Leonardo
	 * @since 03/08/2015 
	 */
	public ModelAndView abrirModalAlterarEmail(WebRequestContext request, EmailCobranca emailCobranca){
		emailCobranca = emailCobrancaService.loadEmailCobranca(emailCobranca);
		emailCobranca = emailCobrancaService.loadEmailPessoaContato(emailCobranca);		
		return new ModelAndView("direct:/crud/popup/alterarEnderecoEmailCobranca","emailCobranca", emailCobranca);
	}
	
	/**
	 * @param request
	 * @param emailCobranca
	 * @return
	 * @author Andrey Leonardo
	 * @since 06/08/2015
	 */
	public ModelAndView abrirModalHistoricoEmailCobranca(WebRequestContext request, EmailCobranca emailCobranca){
		emailCobranca = emailCobrancaService.loadEmailCobranca(emailCobranca);
		return new ModelAndView("direct:/crud/popup/visualizarHistoricoEmailCobranca", "listaHistoricoEmailCobranca", emailCobranca.getHistoricoEmailCobranca());
	}
	
	/**
	 * @param request
	 * @param emailCobranca
	 * @author Andrey Leonardo
	 * @since 03/08/2015
	 */
	public void alterarEmail(WebRequestContext request, EmailCobranca emailCobranca){
		if(emailCobranca.getAlterarCadastro()){
			if(emailCobranca.getContato() != null && emailCobranca.getContato().getEmailcontato() != null
					&& !emailCobranca.getContato().getEmailcontato().isEmpty()){
				Contato contato = new Contato();
				contato.setCdpessoa(emailCobranca.getPessoa().getCdpessoa());
				contato.setEmailcontato(emailCobranca.getEmail());
				ContatoService.getInstance().updateContatoEmail(emailCobranca.getContato());
			}else{
				Pessoa pessoa = emailCobranca.getPessoa();
				pessoa.setEmail(emailCobranca.getEmail());
				PessoaService.getInstance().updateEmail(pessoa);
			}
		}		
		emailCobrancaService.updateEmailCobrancaEmail(emailCobranca);		
		SinedUtil.redirecionamento(request, "/financeiro/crud/EmailCobranca?ACAO=reenviarEmail&selectedItens="+emailCobranca.getCdemailcobranca());
	}
}
