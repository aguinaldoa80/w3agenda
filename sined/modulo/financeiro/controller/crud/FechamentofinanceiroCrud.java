package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FechamentoFinanceiroHistorico;
import br.com.linkcom.sined.geral.bean.Fechamentofinanceiro;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FechamentoFinanceiroHistoricoService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.FechamentofinanceiroFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(path="/financeiro/crud/Fechamentofinanceiro", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"datainicio", "datalimite", "motivo", "dtaltera", "responsavel", "ativo"})
public class FechamentofinanceiroCrud extends CrudControllerSined<FechamentofinanceiroFiltro, Fechamentofinanceiro, Fechamentofinanceiro> {
	
	private FechamentofinanceiroService fechamentofinanceiroService;
	private EmpresaService empresaService;
	private FechamentoFinanceiroHistoricoService fechamentoFinanceiroHistoricoService;
	private ContaService contaService;
		
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}
	public void setFechamentoFinanceiroHistoricoService(FechamentoFinanceiroHistoricoService fechamentoFinanceiroHistoricoService) {
		this.fechamentoFinanceiroHistoricoService = fechamentoFinanceiroHistoricoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	
	@Override
	protected Fechamentofinanceiro criar(WebRequestContext request, Fechamentofinanceiro form) throws Exception {
		if("true".equalsIgnoreCase(request.getParameter("origemAviso")) && form.getConta() != null){
			Fechamentofinanceiro bean = new Fechamentofinanceiro();
			String dtaviso = request.getParameter("dtaviso");
			if(dtaviso != null){
				try {
					bean.setDatainicio(SinedDateUtils.firstDateOfMonth(SinedDateUtils.addMesData(SinedDateUtils.stringToDate(dtaviso, "yyyy-MM-dd"), -1)));
					bean.setDatalimite(SinedDateUtils.lastDateOfMonth(form.getDatainicio()));
				} catch (Exception e) {}
			}
			Conta conta = contaService.loadContaWithEmpresa(form.getConta());
			if(conta != null){
				bean.setConta(conta);
				bean.setContatipo(conta.getContatipo());
				if(conta.getListaContaempresa() != null && conta.getListaContaempresa().size() == 1){
					bean.setEmpresa(conta.getListaContaempresa().iterator().next().getEmpresa());
				}
			}
			return bean;
		}
		return super.criar(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request,	Fechamentofinanceiro form) throws CrudException {
		if(form.getProxDatalimite() != null && form.getProxDatalimite().after(form.getDatalimite()) && (form.getMotivo() == null || form.getMotivo().trim() == "")){
			request.clearMessages();
			request.addError("A data limite fornecida n�o � v�lida.");
			return getSalvarModelAndView(request, form);
		}
		else{
			form.setDtaltera(new Timestamp(System.currentTimeMillis()));
			form.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		}
		return super.doSalvar(request, form);
	}
	
	@Override
	protected Fechamentofinanceiro carregar(WebRequestContext request, Fechamentofinanceiro bean) throws Exception {
		Fechamentofinanceiro fechamentofinanceiro = super.carregar(request, bean);
		
		if(fechamentofinanceiro == null || (fechamentofinanceiro.getCdfechamentofinanceiro() != null && fechamentofinanceiro.getConta() != null &&
				!SinedUtil.isUsuarioLogadoAdministrador() && !contaService.isUsuarioPermissaoConta(fechamentofinanceiro.getConta()))){
			throw new SinedException("Registro n�o encontrado no sistema.");
		}
		
		return fechamentofinanceiro;
	}


	@Override
	protected void entrada(WebRequestContext request, Fechamentofinanceiro form) throws Exception {
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		
		if(form.getCdfechamentofinanceiro() != null){
			List<FechamentoFinanceiroHistorico> listaFechamentoFinanceiroHistorico = fechamentoFinanceiroHistoricoService.findHistoricoByFechamentoFinanceiro(form);
			form.setListaFechamentoFinanceiroHistorico(listaFechamentoFinanceiroHistorico);			
		}
	}

	public void ajaxLoadProxDatalimite(WebRequestContext request) throws Exception{
		String cdempresa = request.getParameter("empresa");
		if(cdempresa != null && cdempresa != ""){
			Empresa empresa = new Empresa(Integer.parseInt(cdempresa));
			
			Date loadUltimaDatalimite = fechamentofinanceiroService.loadUltimaDatalimite(empresa, null);
			if(loadUltimaDatalimite != null){
				Date proxDatalimite = SinedDateUtils.addDiasData(loadUltimaDatalimite, 1);
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				
				View.getCurrent().println("var proxDatalimite = '"+dateFormat.format(proxDatalimite).toString()+"';");
				return;
			}
		}
		View.getCurrent().println("var proxDatalimite = '';");
	}
	
	@Override
	protected void validateBean(Fechamentofinanceiro bean, BindException errors) {
		if(bean.getAtivo() != null && bean.getAtivo()){
			if(fechamentofinanceiroService.haveFechamento(bean)){
				errors.reject("001", "J� existe um fechamento financeiro com os mesmos dados cadastrados.");
			}else if(fechamentofinanceiroService.haveFechamentoPeriodo(bean)){
				errors.reject("001", "J� existe um fechamento financeiro no mesmo intervalo de datas cadastrado para a empresa, tipo de vinculo e v�nculo informados");
			}
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Fechamentofinanceiro bean)	throws Exception {

		String observacao = "";
		boolean criado = true;
		if(bean.getCdfechamentofinanceiro() != null){
			criado = false;
			Fechamentofinanceiro fechamentofinanceiro = fechamentofinanceiroService.findFechamentoFinanceiroAntigoByCdFechamento(bean.getCdfechamentofinanceiro());

			if(bean.getHistorico() != null){
				observacao += bean.getHistorico() + "<br>";
			}
			
			if(fechamentofinanceiro.getAtivo() != null && bean.getAtivo() != null && !bean.getAtivo().equals(fechamentofinanceiro.getAtivo())){
				if(fechamentofinanceiro.getAtivo()){
					observacao += " Alterado a marca��o do campo ativo: Inativado"+"<br>";
					
				}else{
					observacao += " Alterado a marca��o do campo ativo: Ativado"+"<br>";
				}
			}

			if(fechamentofinanceiro.getDatainicio() != null && bean.getDatainicio() != null && !bean.getDatainicio().equals( fechamentofinanceiro.getDatainicio())){
				observacao += " Mudan�a no periodo de fechamento, data de inicio alterada de: "	+ new SimpleDateFormat("dd/MM/yyyy").format(fechamentofinanceiro.getDatainicio())+ " para: "+ new SimpleDateFormat("dd/MM/yyyy").format(bean.getDatainicio()) + "<br>";
			}

			if(fechamentofinanceiro.getDatalimite() != null && bean.getDatalimite() != null && !bean.getDatalimite().equals(fechamentofinanceiro.getDatalimite())){
				observacao += " Mudan�a no periodo de fechamento, data fim alterada de: "+ new SimpleDateFormat("dd/MM/yyyy").format(fechamentofinanceiro.getDatalimite())+ " para: "+ new SimpleDateFormat("dd/MM/yyyy").format(bean.getDatalimite()) + "<br>";
			}

			if(fechamentofinanceiro.getConta() != null	&& bean.getConta() != null && !bean.getConta().equals(fechamentofinanceiro.getConta())){
				observacao += " Mudan�a do vinculo fechado. Vinculo anterior : "+ fechamentofinanceiro.getConta().getNome() + "<br>";
			}
		} else {
			if(bean.getHistorico() != null){
				observacao += bean.getHistorico() + "<br>";
			}
		}

		super.salvar(request, bean);

		FechamentoFinanceiroHistorico fechamentoFinanceiroHistorico = new FechamentoFinanceiroHistorico();
		fechamentoFinanceiroHistorico.setFechamentofinanceiro(bean);
		fechamentoFinanceiroHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		fechamentoFinanceiroHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		if (criado && "".equals(observacao)) {
			observacao = "Criado";
		} else if (criado && !"".equals(observacao)) {
			observacao = "Criado<br>" + observacao;
		} else if (!"".equals(observacao)) {
			observacao = observacao.substring(0, observacao.length() - 1);
		} else {
			observacao = "Alterado";
		}
		
		fechamentoFinanceiroHistorico.setObservacao(observacao);

		fechamentoFinanceiroHistoricoService.saveOrUpdate(fechamentoFinanceiroHistorico);
	}
}
