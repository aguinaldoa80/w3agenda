package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresahistorico;
import br.com.linkcom.sined.geral.bean.Empresaproprietario;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ComprovanteConfiguravelService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContratofaturalocacaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EmpresahistoricoService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PessoadapService;
import br.com.linkcom.sined.geral.service.RegimetributacaoService;
import br.com.linkcom.sined.geral.service.UsuarioempresaService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.EmpresaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/financeiro/crud/Empresa", "/sistema/crud/Empresa"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nomefantasia", "razaosocial", "cnpj", "principal", "ativo"})
public class EmpresaCrud extends CrudControllerSined<EmpresaFiltro, Empresa, Empresa>{

	private ArquivoService arquivoService;
	private UsuarioempresaService usuarioempresaService;
	private EmpresaService empresaService;
	private ContagerencialService contagerencialService;
	private MunicipioService municipioService;
	private ConfiguracaonfeService configuracaonfeService;
	private ComprovanteConfiguravelService comprovanteConfiguravelService;
	private MaterialgrupoService materialgrupoService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private RegimetributacaoService regimetributacaoService;
	private ClienteService clienteService;
	private FornecedorService fornecedorService;
	private ReportTemplateService reportTemplateService;
	private ContratofaturalocacaoService contratofaturalocacaoService;
	private EmpresahistoricoService empresahistoricoService;
	private CentrocustoService centrocustoService;
	private PessoadapService pessoadapService;
	private ParametrogeralService parametrogeralService;
	private PessoaService pessoaService;
	private ContaService contaService;
	private EnderecoService enderecoService;
	private ColaboradorService colaboradorService;
	
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPessoadapService(PessoadapService pessoadapService) {
		this.pessoadapService = pessoadapService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setRegimetributacaoService(
			RegimetributacaoService regimetributacaoService) {
		this.regimetributacaoService = regimetributacaoService;
	}	
	public void setNaturezaoperacaoService(
			NaturezaoperacaoService naturezaoperacaoService) {
		this.naturezaoperacaoService = naturezaoperacaoService;
	}
	public void setConfiguracaonfeService(
			ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setUsuarioempresaService(UsuarioempresaService usuarioempresaService) {
		this.usuarioempresaService = usuarioempresaService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setComprovanteConfiguravelService(ComprovanteConfiguravelService comprovanteConfiguravelService) {
		this.comprovanteConfiguravelService = comprovanteConfiguravelService;
	}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setContratofaturalocacaoService(ContratofaturalocacaoService contratofaturalocacaoService) {this.contratofaturalocacaoService = contratofaturalocacaoService;}
	public void setEmpresahistoricoService(EmpresahistoricoService empresahistoricoService) {
		this.empresahistoricoService = empresahistoricoService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Empresa bean) throws Exception {
		
		int larguraideal = 150;
		if(bean.getListaEndereco() == null){
			Set<Endereco> end = new HashSet<Endereco>();
			bean.setListaEndereco(end);
		}
		
		if(bean.getListaEndereco() == null){
			Set<Endereco> end = new HashSet<Endereco>();
			bean.setListaEndereco(end);
		}
		
		Pessoa pessoaJaCadastrada = null;
		String descricaoCpfOrCnpj = "";
		if(bean.getCpf()!=null && Tipopessoa.PESSOA_FISICA.equals(bean.getTipopessoa())){
			pessoaJaCadastrada = pessoaService.findPessoaByCpf(bean.getCpf());
			descricaoCpfOrCnpj = "CPF";
		}else if(bean.getCnpj()!=null && Tipopessoa.PESSOA_JURIDICA.equals(bean.getTipopessoa())){
			pessoaJaCadastrada = pessoaService.findPessoaByCnpj(bean.getCnpj());
			descricaoCpfOrCnpj = "CNPJ";
		}
		if(pessoaJaCadastrada != null && !pessoaJaCadastrada.getCdpessoa().equals(bean.getCdpessoa())){
			throw new SinedException(descricaoCpfOrCnpj+" de empresa j� cadastrado no sistema.");
		}
		
		if(bean.getLogomarca()!=null && bean.getLogomarca().getContent().length > 0){
			Image image = ImageIO.read(new ByteArrayInputStream(bean.getLogomarca().getContent()));							
			int largura = image.getWidth(null);
			if(largura < larguraideal){
				throw new SinedException("Logomarca para relat�rio com dimens�o inferior a permitida.");
			}else{
				float fatorconversao = largura/larguraideal;
				BufferedImage imagem = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
				if(fatorconversao != 0){
					int altura = (int) (imagem.getHeight()/fatorconversao);
					
					Graphics2D g2d = (Graphics2D)imagem.createGraphics();
					g2d.drawImage(image, 0, 0, null);
					g2d.dispose();
					imagem = SinedUtil.getScaledInstance(imagem, larguraideal, altura, BufferedImage.TYPE_INT_ARGB, RenderingHints.VALUE_INTERPOLATION_BICUBIC, Boolean.TRUE, Boolean.FALSE);
					
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write(imagem, "png", baos);
					byte[] bytes= baos.toByteArray();
					baos.close();
					Arquivo arquivo = new Arquivo(bytes, bean.getLogomarca().getNome(), bean.getLogomarca().getContenttype());
					bean.setLogomarca(arquivo);
				}
			}
		}
		
		if(bean.getListaEmpresaproprietario()!=null){
			List<Colaborador> colaboradores = new ArrayList<Colaborador>();
			for(Empresaproprietario empresaProprietario: bean.getListaEmpresaproprietario()){
				if(colaboradores.contains(empresaProprietario.getColaborador())){
					throw new SinedException("Colaboraborar duplicado na aba Propriet�rios.");
				}else{
					colaboradores.add(empresaProprietario.getColaborador());
				}
			}
		}
		
		if(bean.getLogomarcasistema()!=null && bean.getLogomarcasistema().getContent().length > 0){
			Image image = ImageIO.read(new ByteArrayInputStream(bean.getLogomarcasistema().getContent()));			
			int largura = image.getWidth(null);
			if(largura < larguraideal){
				throw new SinedException("Logomarca para o sistema com dimens�o inferior a permitida.");
			}else{
				float fatorconversao = larguraideal/largura;
				BufferedImage imagem = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
				if(fatorconversao != 0){
					int altura = (int) (imagem.getHeight()/fatorconversao);
					
					Graphics2D g2d = (Graphics2D)imagem.createGraphics();
					g2d.drawImage(image, 0, 0, null);
					g2d.dispose();
					imagem = SinedUtil.getScaledInstance(imagem, larguraideal, altura, BufferedImage.TYPE_INT_ARGB, RenderingHints.VALUE_INTERPOLATION_BICUBIC, Boolean.TRUE, Boolean.FALSE);
					
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write(imagem, "png", baos);
					byte[] bytes= baos.toByteArray();
					baos.close();
					Arquivo arquivo = new Arquivo(bytes, bean.getLogomarcasistema().getNome(), bean.getLogomarcasistema().getContenttype());
					bean.setLogomarcasistema(arquivo);
				}
			}
		}
		
		try{
			Empresa empresaAnterior = empresaService.loadForEntrada(bean);
			
			super.salvar(request, bean);
			
			Usuario usuario = (Usuario)Neo.getUser();
			if(!usuarioempresaService.haveRegistro(usuario, bean)){
				Usuarioempresa usuarioempresa = new Usuarioempresa();
				usuarioempresa.setUsuario(usuario);
				usuarioempresa.setEmpresa(bean);
				usuarioempresaService.saveOrUpdate(usuarioempresa);
				NeoWeb.getRequestContext().getSession().removeAttribute("W3ERP_EMPRESA");
			}
			
			gerarHistoricoAlteracao(bean, empresaAnterior);
			
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "endereco")){
				request.setAttribute("ErroAoDeletarEnd", "true");
				throw new SinedException("Endere�o n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_EMPRESA_PRINCIPAL")){
				empresaService.saveOrUpdatePrincipal(bean);
				request.addMessage("Registro salvo com sucesso.", MessageType.INFO);
				request.addMessage("Esta empresa � considerada como principal no lugar da anterior.", MessageType.WARN);
			}else if(DatabaseError.isKeyPresent(e, "fk_material_10")){
				throw new SinedException("Modelo de contrato n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");	
			}
		}
		NeoWeb.getRequestContext().getSession().setAttribute("listaEmpresaLogado",null);
	}
	
	private void gerarHistoricoAlteracao(Empresa registroAtual, Empresa registroAnterior) {
		
		Empresahistorico historico = new Empresahistorico();
		historico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		historico.setEmpresa(registroAtual);
		
		if (registroAnterior != null){
			historico.setObservacao("Altera��o realizada.");
			historico.setNomefantasia(registroAnterior.getNomefantasia());
			historico.setProximonumnf(registroAnterior.getProximonumnf());
			historico.setProximonumnfproduto(registroAnterior.getProximonumnfproduto());
			historico.setNaturezaoperacao(registroAnterior.getNaturezaoperacao());
			historico.setRegimetributacao(registroAnterior.getRegimetributacao());
			historico.setCodigotributacao(registroAnterior.getCodigotributacao());
			historico.setItemlistaservico(registroAnterior.getItemlistaservico());
			historico.setCrt(registroAnterior.getCrt());
			historico.setTributacaomunicipiocliente(registroAnterior.getTributacaomunicipiocliente());
			historico.setNaopreencherdtsaida(registroAnterior.getNaopreencherdtsaida());
			historico.setEspecienf(registroAnterior.getEspecienf());
			historico.setMarcanf(registroAnterior.getMarcanf());
			historico.setDiscriminarservicoqtdevalor(registroAnterior.getDiscriminarservicoqtdevalor());
			historico.setIndiceperfilsped(registroAnterior.getIndiceperfilsped());
			historico.setIndiceatividadesped(registroAnterior.getIndiceatividadesped());
			historico.setIndiceatividadespedpiscofins(registroAnterior.getIndiceatividadespedpiscofins());
			historico.setUfsped(registroAnterior.getUfsped());
			historico.setMunicipiosped(registroAnterior.getMunicipiosped());
			historico.setGeracaospedpiscofins(registroAnterior.getGeracaospedpiscofins());
			historico.setGeracaospedicmsipi(registroAnterior.getGeracaospedicmsipi());
			historico.setCstpisnfs(registroAnterior.getCstpisnfs());
			historico.setCstcofinsnfs(registroAnterior.getCstcofinsnfs());
			historico.setEscritoriocontabilista(registroAnterior.getEscritoriocontabilista());
			historico.setColaboradorcontabilista(registroAnterior.getColaboradorcontabilista());
			historico.setCrccontabilista(registroAnterior.getCrccontabilista());
			historico.setCalcularQuantidadeTransporte(registroAnterior.getCalcularQuantidadeTransporte());
		} else {
			historico.setObservacao("Registro criado.");
		}
		
		empresahistoricoService.saveOrUpdate(historico );
	}
	
	@Override
	protected void entrada(WebRequestContext request, Empresa form) throws Exception {
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(form.getCentrocusto(), form.getCentrocustodespesaviagem(), form.getCentrocustotransferencia(), form.getCentrocustoveiculouso()));
		request.setAttribute("listaContagerencial", contagerencialService.findAnaliticas(Tipooperacao.TIPO_CREDITO));
		request.setAttribute("listaContagerencialcompra", contagerencialService.findAnaliticas(Tipooperacao.TIPO_DEBITO));
		
		if(request.getAttribute("ErroAoDeletarEnd") !=null && request.getAttribute("ErroAoDeletarEnd").equals("true")){
			pessoaService.setListaEnderecoForEntrada(form);
		}
		
		if (form != null && form.getCdpessoa() == null) {
			form.setAtivo(Boolean.TRUE);
			if(form.getTipopessoa() == null){
				form.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);				
			}
		}
		
		if(form.getCdpessoa() != null){
			if(!request.getBindException().hasErrors()){
				empresaService.setInfoPessoaconfiguracaoEntrada(form);
			}
			List<Municipio> listaMunicipio = new ArrayList<Municipio>();
			if(form.getListaEndereco()!=null){
				for (Endereco endereco : form.getListaEndereco()) {
					if(endereco != null && endereco.getMunicipio() != null){
						listaMunicipio.add(endereco.getMunicipio());
						endereco.setUf(endereco.getMunicipio().getUf());
					}
				}				
			}
			
			request.setAttribute("listaMunicipioEndereco", listaMunicipio);
		}
		
		request.setAttribute("listaNaturezaoperacao", naturezaoperacaoService.find(form.getNaturezaoperacao(), NotaTipo.NOTA_FISCAL_SERVICO, Boolean.TRUE));
		request.setAttribute("listaRegimetributacao", regimetributacaoService.findAtivos());
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		request.setAttribute("listaConfProduto", configuracaonfeService.findByTipo(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO));
		request.setAttribute("listaConfServico", configuracaonfeService.findByTipo(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO));
		request.setAttribute("listaComprovanteConfiguravelPedidoVenda", comprovanteConfiguravelService.findAtivosByPedidovenda());
		request.setAttribute("listaComprovanteConfiguravelVenda", comprovanteConfiguravelService.findAtivosByVenda());
		request.setAttribute("listaComprovanteConfiguravelOrcamento", comprovanteConfiguravelService.findAtivosByOrcamento());
		request.setAttribute("listaComprovanteConfiguravelColeta", comprovanteConfiguravelService.findAtivosByColeta());
		request.setAttribute("listaPresencacompradornfe", Presencacompradornfe.getListaPresencacompradornfeForVenda());
		request.setAttribute("listaReportTemplateBoleto", reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMAIL_BOLETO));
		
		try {
			request.setAttribute("listaDanfeConfiguravel", reportTemplateService.loadTemplatesByCategoria(EnumCategoriaReportTemplate.DANFE));
		} catch (Exception e) {
			request.setAttribute("listaDanfeConfiguravel", null);
		}
		request.setAttribute("listaContaBancaria", contaService.findContaBoletoByEmpresa(form));
		
		if (form.getCdpessoa() != null){
			form.setListaEmpresahistorico(empresahistoricoService.findByEmpresa(form));
			if(form.getListaEmpresahistorico() != null && form.getListaEmpresahistorico().size() > 0){
				request.setAttribute("primeiroHistorico", form.getListaEmpresahistorico().get(0).getCdempresahistorico());
			}
			if(request.getBindException() != null && !request.getBindException().hasErrors()){
				form.setListaPessoadap(pessoadapService.findByPessoa(form));
			}
		}
		
		request.setAttribute("ignoreHackAndroidDownload", Boolean.TRUE);
		request.setAttribute("COOPERATIVAS", parametrogeralService.getBoolean(Parametrogeral.COOPERATIVAS));
		request.setAttribute("EMPRESA_IDENTIFICACAO", parametrogeralService.buscaValorPorNome(Parametrogeral.EMPRESA_IDENTIFICACAO));
		
		String emp = null;
		if(form.getCdpessoa() !=null){
			emp = form.getCdpessoa().toString();
		}
		request.setAttribute("listaContabaixadinheiro", contaService.findByTipoAndEmpresa(emp, Contatipo.TIPO_CAIXA));
		request.setAttribute("listaContabaixacheque", contaService.findByTipoAndEmpresa(emp, Contatipo.TIPO_CAIXA, Contatipo.TIPO_CONTA_BANCARIA));
		request.setAttribute("listaContabaixacreditoconta", contaService.findByTipoAndEmpresa(emp, Contatipo.TIPO_CONTA_BANCARIA));
		request.setAttribute("listaContadevolucao", contaService.findByTipoAndEmpresa(emp, Contatipo.TIPO_CAIXA));
		request.setAttribute("LCDPR", parametrogeralService.getValorPorNome("LCDPR"));
		request.setAttribute("INTEGRACAO_CORREIOS", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_CORREIOS));
	}
	
	@Override
	protected Empresa carregar(WebRequestContext request, Empresa bean) throws Exception {
		bean = super.carregar(request, bean);
		
		// LOAD DA LOGOMARCA DOS RELAT�RIOS
		try {
			arquivoService.loadAsImage(arquivoService.load(bean.getLogomarca()));
			
			if(bean.getLogomarca().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), bean.getLogomarca().getCdarquivo());
				request.setAttribute("exibefoto", true);
			}
		} catch (Exception e) {
			request.setAttribute("exibefoto", false);
		}
		
		// LOAD DA LOGOMARCA DO SISTEMA
		try {
			arquivoService.loadAsImage(arquivoService.load(bean.getLogomarcasistema()));
			
			if(bean.getLogomarcasistema().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), bean.getLogomarcasistema().getCdarquivo());
				request.setAttribute("exibefotosistema", true);
			}
		} catch (Exception e) {
			request.setAttribute("exibefotosistema", false);
		}
		
		return bean;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, EmpresaFiltro filtro) throws CrudException {
		if (!filtro.isNotFirstTime()) {
			filtro.setAtivo(true);
		}
		Usuario user = SinedUtil.getUsuarioLogado();
		if(user.getCdpessoa()==1){
			request.setAttribute("isAdm", true);
		}else 
			request.setAttribute("isAdm", false);
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void validateBean(Empresa bean, BindException errors) {
		enderecoService.validateListaEndereco(errors, bean.getListaEndereco());	
		Empresa principal = empresaService.loadPrincipal();
		Empresa clone = null;
		if (bean.getCdpessoa() != null) {
			clone = empresaService.loadComArquivo(bean);
		}
		
		if (principal == null && !bean.getPrincipal()) {
			errors.reject("001", "Esta empresa deve ser marcada como principal, pois n�o existe nenhuma marcada no momento.");
			bean.setPrincipal(Boolean.TRUE);
		}
		
//		if (clone == null) {// inserindo...
//			if (bean.getPrincipal() && (bean.getLogomarca() == null || bean.getLogomarca().getTamanho() == 0))
//				errors.reject("001", "A empresa principal deve ter uma logomarca para relat�rio.");
//		} else { //editando...
//			//Existia uma logomarca, a empresa � principal e o usu�rio excluiu a logomarca:
//			if (bean.getPrincipal() && clone.getLogomarca() != null && clone.getLogomarca().getCdarquivo() != null && bean.getLogomarca() == null)
//				errors.reject("001", "A empresa principal deve ter uma logomarca para relat�rio.");
//			
//			//A empresa � principal, n�o existia a logomarca e o usu�rio n�o selecionou uma:
//			if (bean.getPrincipal() && clone.getLogomarca() == null && bean.getLogomarca() == null)
//				errors.reject("001", "A empresa principal deve ter uma logomarca para relat�rio.");
//		}
//		
//		if (clone == null) {// inserindo...
//			if (bean.getPrincipal() && (bean.getLogomarcasistema() == null || bean.getLogomarcasistema().getTamanho() == 0))
//				errors.reject("001", "A empresa principal deve ter uma logomarca para sistema.");
//		} else { //editando...
//			//Existia uma logomarca, a empresa � principal e o usu�rio excluiu a logomarca:
//			if (bean.getPrincipal() && clone.getLogomarcasistema() != null && clone.getLogomarcasistema().getCdarquivo() != null && bean.getLogomarcasistema() == null)
//				errors.reject("001", "A empresa principal deve ter uma logomarca para sistema.");
//			
//			//A empresa � principal, n�o existia a logomarca e o usu�rio n�o selecionou uma:
//			if (bean.getPrincipal() && clone.getLogomarcasistema() == null && (bean.getLogomarcasistema() == null || bean.getLogomarcasistema().getTamanho() == 0))
//				errors.reject("001", "A empresa principal deve ter uma logomarca para sistema.");
//		}
		
		if (bean.getLogomarca() != null && bean.getLogomarca().getTamanho() != 0 && !SinedUtil.isImage(bean.getLogomarca())) {
			// Se n�o for uma imagem v�lida
			errors.reject("001", "A logomarca para relat�rio n�o � uma imagem suportada pelo sistema");
		}
		
		if (bean.getLogomarcasistema() != null && bean.getLogomarcasistema().getTamanho() != 0 && !SinedUtil.isImage(bean.getLogomarcasistema())) {
			// Se n�o for uma imagem v�lida
			errors.reject("001", "A logomarca para sistema n�o � uma imagem suportada pelo sistema");
		}
		
		Cliente cliente = null;
		Fornecedor fornecedor = null;
		if(bean.getCdpessoa() != null){
			cliente = clienteService.load(new Cliente(bean.getCdpessoa()), "cliente.cdpessoa, cliente.nome");
			fornecedor = fornecedorService.load(new Fornecedor(bean.getCdpessoa()), "fornecedor.cdpessoa, fornecedor.nome");
		}
		if(bean.getCdpessoa() == null || (cliente == null && fornecedor == null)){
			if(SinedUtil.isListNotEmpty(bean.getListaEndereco())){
				if(bean.getListaEndereco().size() > 1){
					errors.reject("001","Uma empresa s� deve ter um endere�o.");
				}
			}
		}
		
		if(bean.getProximonumnfproduto() != null){

			try {
				Boolean isProducao = false;
				List<Configuracaonfe> listaConfiguracaonfe = configuracaonfeService.listaConfiguracaoNfproduto(bean);
				
				if(!listaConfiguracaonfe.isEmpty()){
					for(Configuracaonfe item : listaConfiguracaonfe){
						if(item.getPrefixowebservice().name().endsWith("_PROD")){
							isProducao = true;
							break;
						}
					}
				}
				
				if(isProducao){
					errors.reject("002","N�o � poss�vel registrar o pr�ximo n�mero da nota, pois, existem n�meros anteriores com a mesma s�rie que precisam ser inutilizados.");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if(errors.hasErrors()){
			try{
				if(clone != null && clone.getLogomarca() != null && clone.getLogomarca().getCdarquivo() != null){
					bean.setLogomarca(arquivoService.loadWithContents(clone.getLogomarca()));
					Neo.getRequestContext().setAttribute("exibefoto", Boolean.TRUE);
				}
				if(clone != null && clone.getLogomarcasistema() != null && clone.getLogomarcasistema().getCdarquivo() != null){
					bean.setLogomarcasistema(arquivoService.loadWithContents(clone.getLogomarcasistema()));
					Neo.getRequestContext().setAttribute("exibefotosistema", Boolean.TRUE);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(bean.getListaEndereco() != null){
				for (Endereco endereco : bean.getListaEndereco()) {
					if(endereco != null && endereco.getMunicipio() != null){
						endereco.setMunicipio(municipioService.load(endereco.getMunicipio()));
						endereco.setUf(endereco.getMunicipio().getUf());
					}
				}
			}
		}
		
		if(Boolean.TRUE.equals(bean.getPropriedadeRural()) && SinedUtil.isListNotEmpty(bean.getListaEmpresaproprietario())){
			Colaborador colaborador;
			for(Empresaproprietario empresaproprietario : bean.getListaEmpresaproprietario()){
				if(empresaproprietario.getColaborador() != null && Boolean.TRUE.equals(empresaproprietario.getPrincipal())){
					colaborador = colaboradorService.loadReaderWithoutWhereEmpresa(empresaproprietario.getColaborador(), "colaborador.cdpessoa, colaborador.cpf");
					if(colaborador != null && colaborador.getCpf() == null ){
						errors.reject("001", "O propriet�rio principal precisa ter o CPF preenchido.");
					}
				}
			}
		}
			
	}
	
	@Override
	protected void excluir(WebRequestContext request, Empresa bean) throws Exception {
		
		String itens = request.getParameter("itenstodelete");	
		if(empresaService.existCdpessoaTabelaFornecedorCliente(itens)){
			throw new SinedException("N�o � possivel excluir uma empresa vinculada a um cliente ou fornecedor.");
		}
		List<Usuarioempresa> listaUsuarioempresa = usuarioempresaService.findByEmpresa(itens);
		for (Usuarioempresa usuarioempresa : listaUsuarioempresa) {
			usuarioempresaService.delete(usuarioempresa);
		}
		
		try{
			super.excluir(request, bean);
		} catch (SinedException e) {
			for (Usuarioempresa usuarioempresa : listaUsuarioempresa) {
				usuarioempresa.setCdusuarioempresa(null);
				usuarioempresaService.saveOrUpdate(usuarioempresa);
			}
			throw new SinedException(e.getMessage());
		}
		
	}

	public void ajaxConsultaEmpresaPrincipal(WebRequestContext request, Empresa empresa) {
		String hasPrincipal = "var hasPrincipal = ";
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		Empresa principal = empresaService.loadPrincipal();
		
		if (principal == null || ( empresa.getCdpessoa() != null && principal.getCdpessoa().equals(empresa.getCdpessoa()) ) ) {
			hasPrincipal += "false;";
		} else {
			hasPrincipal += "true;";
			hasPrincipal += "var nome = '" + empresaService.getEmpresaRazaosocialOuNome(empresa) + "';";
		}
		
		view.println(hasPrincipal);
	}
	
	/**
	* M�todo ajax para buscar o munic�pio da empresa principal
	*
	* @since Aug 30, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxBuscaMunicipioEmpresaPrincipal(WebRequestContext request){
		
		String sucesso = "var sucesso = ";
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		Empresa empresaPrincipal = empresaService.buscaMunicipioEmpresaPrincipal();
		
		if(empresaPrincipal != null && empresaPrincipal.getListaEndereco() != null && !empresaPrincipal.getListaEndereco().isEmpty()){
			for(Endereco endereco : empresaPrincipal.getListaEndereco()){
				if(endereco.getMunicipio() != null && endereco.getMunicipio().getNome() != null){
					sucesso += "true; ";
					sucesso += "var municipio = '" + endereco.getMunicipio().getNome() + "';";			
					if(endereco.getMunicipio().getUf() != null && endereco.getMunicipio().getUf().getSigla() != null){
						sucesso += "var ufsigla = '" + endereco.getMunicipio().getUf().getSigla() + "';";
					}
				}
				break;
			}
		}else{
			sucesso += "false";
		}
		
		view.println(sucesso);
	}
	
	/**
	 * 
	 * M�todo chamado pelo Realizar Pedido Venda e Realizar Venda que coloca a empresa selecionada no combo na sess�o para que na busca de material
	 * essa empresa seja utilizada como filtro. 
	 *
	 * @name colocarEmpresaSessao
	 * @param request
	 * @param filtro
	 * @return void
	 * @author Thiago Augusto
	 * @date 09/04/2012
	 *
	 */
	public void colocarEmpresaSessao(WebRequestContext request){
		String empresa = request.getParameter("empresa");
		if(empresa != null && !empresa.equals("")){
			String []aux = empresa.split("=");
			empresa = aux[1].replace("]", "");
		}
		request.getSession().setAttribute("EMPRESA_FILTRO_MATERIAIS", empresa);	
	}
	
	/**
	 * 
	 * M�todo que busca o n�mero do pr�ximo identificador da venda.
	 *
	 * @name verificarIdentificador
	 * @param request
	 * @param empresa
	 * @return
	 * @return ModelAndView
	 * @author Thiago Augusto
	 * @date 12/07/2012
	 *
	 */
	public ModelAndView verificarIdentificadorVenda(WebRequestContext request, Empresa empresa){
		
		Integer cdpessoa = Integer.parseInt(request.getParameter("cdpessoa"));
		
		JsonModelAndView json = new JsonModelAndView();
		Empresa bean = empresaService.buscarProximoIdentificadorVenda(new Empresa(cdpessoa));
		json.addObject("identificador", bean != null && bean.getProximoidentificadorvenda() != null ? bean.getProximoidentificadorvenda() : "");
		return json;
	}
	
	/**
	* M�todo que abre a popup para o usu�rio informar o n�mero para reiniciar a nume��o das faturas
	*
	* @param request
	* @param empresa
	* @return
	* @since 10/09/2014
	* @author Luiz Fernando
	*/
	public ModelAndView abrirReiniciarNumfaturalocacao(WebRequestContext request, Empresa empresa){
		if(empresa == null || empresa.getCdpessoa() == null){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request, "/sistema/crud/Empresa");
			return null;
		}
		
		request.setAttribute("acaoSalvar", "saveReiniciarNumfaturalocacao");
		return new ModelAndView("direct:/crud/popup/abrirReiniciarNumfaturalocacao", "bean", empresa);
	}
	
	/**
	* M�todo que executa o processo de reiniciar a numera��o das faturas
	*
	* @param request
	* @param empresa
	* @since 10/09/2014
	* @author Luiz Fernando
	*/
	public void saveReiniciarNumfaturalocacao(WebRequestContext request, Empresa empresa){
		if(empresa != null && empresa.getCdpessoa() != null && empresa.getNumfaturalocacaoReiniciar() != null){
			contratofaturalocacaoService.ignorarNumeracaoByEmpresa(empresa, empresa.getNumfaturalocacaoReiniciar());
			empresaService.updateProximoNumFaturalocacao(empresa, empresa.getNumfaturalocacaoReiniciar());
			request.addMessage("Numera��o das faturas reiniciadas.");
		}
		SinedUtil.redirecionamento(request, "/sistema/crud/Empresa?ACAO=consultar&cdpessoa=" + empresa.getCdpessoa());
	}
	
	/**
	 * M�todo para carregar jsp com a informa��o dos principais campos alterados.
	 * 
	 * @param request
	 * @param empresahistorico
	 * @return
	 * @author Giovane Freitas
	 */
	public ModelAndView visualizarHistorico(WebRequestContext request, Empresahistorico empresahistorico){
		
		if(empresahistorico == null || empresahistorico.getCdempresahistorico() == null)
			throw new SinedException("Registro de hist�rico inv�lido.");

		request.setAttribute("TEMPLATE_beanName", empresahistorico);
		request.setAttribute("TEMPLATE_beanClass", Empresahistorico.class);
		request.setAttribute("descricao", "Empresa");
		request.setAttribute("consultar", true);
		empresahistorico = empresahistoricoService.loadForVisualizacao(empresahistorico);
		
		
		
		return new ModelAndView("direct:crud/popup/visualizacaoHistoricoEmpresa").addObject("empresahistorico", empresahistorico);
	}
	
	public ModelAndView validaIdentificadorUnico(WebRequestContext request, Empresa bean){
		ModelAndView retorno = new JsonModelAndView();
		String nomeCampo = (String)request.getParameter("nomeCampo");
		Object valorCampo = null;
		Field campoId = null;
		for(Field campo: bean.getClass().getDeclaredFields()){
			if(campo.getName().toUpperCase().equals(nomeCampo.toUpperCase())){
				campoId = campo;
				valorCampo = bean.retornaValorCampo(campoId);
				break;
			}
		}

		if(campoId!=null){
			List<Empresa> empresas = empresaService.findByCampo(campoId.getName(), valorCampo);
			for(Empresa empresa: empresas){
				if(bean.getCdpessoa() ==null || !empresa.getCdpessoa().equals(bean.getCdpessoa())){
					retorno.addObject("identificadorEmOutraEmpresa", true);
					break;
				}
			}
		}
		return retorno;
	}
	
	public ModelAndView recuperarPessoa(WebRequestContext request, Empresa empresa) throws CrudException{	
    	if(!empresaService.existeEmpresa(empresa)){
    		empresaService.insertEmpresaCliente(empresa);
    		Usuarioempresa user = new Usuarioempresa();
    		user.setUsuario(SinedUtil.getUsuarioLogado());
    		user.setEmpresa(empresa);
    		usuarioempresaService.saveOrUpdate(user);
    		request.getSession().removeAttribute("listaEmpresaLogado");
    		return doEditar(request, empresa);    		
    	}
    	return doEditar(request, empresa);
	}
}
