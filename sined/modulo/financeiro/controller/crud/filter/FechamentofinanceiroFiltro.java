package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FechamentofinanceiroFiltro extends FiltroListagemSined {
	
	protected Empresa empresa;
	protected Date datalimiteDe;
	protected Date datalimiteAte;
	protected Contatipo contatipo;
	protected Conta conta;
	protected Boolean ativo;
	
	public FechamentofinanceiroFiltro(){
		empresa = EmpresaService.getInstance().loadPrincipal(); 
		ativo = Boolean.TRUE;
	}
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Date getDatalimiteDe() {
		return datalimiteDe;
	}
	public Conta getConta() {
		return conta;
	}
	public Contatipo getContatipo() {
		return contatipo;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	public void setDatalimiteDe(Date datalimiteDe) {
		this.datalimiteDe = datalimiteDe;
	}
	public Date getDatalimiteAte() {
		return datalimiteAte;
	}
	public void setDatalimiteAte(Date datalimiteAte) {
		this.datalimiteAte = datalimiteAte;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
