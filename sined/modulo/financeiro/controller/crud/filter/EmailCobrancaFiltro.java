package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.ibm.icu.util.Calendar;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmailCobrancaFiltro extends FiltroListagemSined{
	private String cliente;
	private String email;
	private List<EmailStatusEnum> listaEmailStatus;
	private List<Documentoacao> listaDocumentoacao;
	private Date envioDe;
	private Date envioAte;
	
	public EmailCobrancaFiltro(){
		if(listaDocumentoacao == null){
			listaDocumentoacao = new ArrayList<Documentoacao>();
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
		}		
		envioDe = SinedDateUtils.incrementDate(new Date(System.currentTimeMillis()), -15, Calendar.DAY_OF_YEAR);
		envioAte = new Date(System.currentTimeMillis());
	}
	
	@DisplayName("Recebimento de")
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	@DisplayName("E-Mail")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@DisplayName("Situa��o do E-Mail")
	public List<EmailStatusEnum> getListaEmailStatus() {
		return listaEmailStatus;
	}

	public void setListaEmailStatus(List<EmailStatusEnum> listaEmailStatus) {
		this.listaEmailStatus = listaEmailStatus;
	}

	public Date getEnvioDe() {
		return envioDe;
	}

	public void setEnvioDe(Date envioDe) {
		this.envioDe = envioDe;
	}

	public Date getEnvioAte() {
		return envioAte;
	}

	public void setEnvioAte(Date envioAte) {
		this.envioAte = envioAte;
	}
	@DisplayName("Situa��o")
	public List<Documentoacao> getListaDocumentoacao() {
		return listaDocumentoacao;
	}
	public void setListaDocumentoacao(List<Documentoacao> listaDocumentoacao) {
		this.listaDocumentoacao = listaDocumentoacao;
	}
	
	
}
