package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivobancarioProcessFiltro extends FiltroListagemSined{

	protected Banco banco;
	protected Date dtvenc1;
	protected Date dtvenc2;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	protected Boolean enviadabanco;
	protected List<Documento> listaContapagar = new ArrayList<Documento>();
	protected Money valortotal;
	
	
	@DisplayName("Banco")
	public Banco getBanco() {
		return banco;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Enviada para o banco")
	public Boolean getEnviadabanco() {
		return enviadabanco;
	}
	
	public Date getDtvenc1() {
		return dtvenc1;
	}
	public Date getDtvenc2() {
		return dtvenc2;
	}
	public List<Documento> getListaContapagar() {
		return listaContapagar;
	}
	@DisplayName("Valor total")
	public Money getValortotal() {
		return valortotal;
	}
	
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setEnviadabanco(Boolean enviadabanco) {
		this.enviadabanco = enviadabanco;
	}
	public void setDtvenc1(Date dtvenc1) {
		this.dtvenc1 = dtvenc1;
	}
	public void setDtvenc2(Date dtvenc2) {
		this.dtvenc2 = dtvenc2;
	}
	public void setListaContapagar(List<Documento> listaContapagar) {
		this.listaContapagar = listaContapagar;
	}
	
	
}
