package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import net.sourceforge.jeval.function.math.ToRadians;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Motivocancelamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoContapagarReport;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoContareceberReport;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration.Tiporelatorio;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentoFiltro extends FiltroListagemSined {

	protected Integer cddocumento;
	protected String documentoDescricao;
	protected Boolean permitirFiltroDocumentoDeAte;
	protected String documentoDe;
	protected String documentoAte;
	protected Empresa empresa;
	protected Tipopagamento tipopagamento;
	protected Colaborador colaborador;
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected String outrospagamento;
	protected String pessoa;
	protected List<Documentoacao> listaDocumentoacao;
	protected Documentoacao documentoacao;
	protected Documentoacao documentoacaoprotesto;
	protected Documentoclasse documentoclasse;
	protected Date dtemissao1;
	protected Date dtemissao2;
	protected Date dtvencimento1 = SinedDateUtils.addMesData(SinedDateUtils.firstDateOfMonth(), -12);
	protected Date dtvencimento2;
	protected Date dtprimeirovencimento1;
	protected Date dtprimeirovencimento2;
	protected Date dtbaixa1;
	protected Date dtbaixa2;
	protected Date dtcompetencia1;
	protected Date dtcompetencia2;
	protected Money valorminimo;
	protected Money valormaximo;
	protected Boolean protestada;
	protected String cheque;
	protected Contatipo contatipo;
	protected Conta conta;
	protected Conta vinculoProvisionado;
	protected Colaborador vendedor;
	protected Motivocancelamento motivocancelamento;
	protected Contratotipo contratotipo;
	protected Boolean antecipado;
	protected Fornecedor associacao;
	
	protected String vendaDe;
	protected String vendaAte;
	
	protected Boolean projetoAberto;
	protected Projeto projeto;
	protected Projetotipo projetotipo;
	
	protected Centrocusto centrocusto;
	protected Contagerencial contagerencial;
	protected Documentotipo documentotipo;
	protected List<Documentotipo> listaDocumentotipo;
	
	protected List<ItemDetalhe> listaPagamentoa;
	protected List<ItemDetalhe> listaRecebimentode;
	protected List<ItemDetalhe> listaColaborador;
	protected List<ItemDetalhe> listaFornecedor;
	protected List<ItemDetalhe> listaCliente;
	protected List<ItemDetalhe> listaProjeto;
	protected List<ItemDetalhe> listaCentrocusto;
	protected List<ItemDetalhe> listaEmpresa;
	protected List<ItemDetalhe> listaOutrospagamento;
	protected List<ItemDetalhe> listaContagerencial;
	protected List<ItemDetalhe> listaCaixa;
	protected List<ItemDetalhe> listaCartao;
	protected List<ItemDetalhe> listaContabancaria;
	
	protected Date dtiniciolancamento;
	protected Date dtfimlancamento;
	protected Boolean isRestricaoClienteVendedor;
	
	//Vencimento
	protected Boolean avencer;
	protected Boolean vencida;
	protected Boolean quitada;
	
	protected Boolean allPagamentoa;
	protected Boolean allRecebimentode;
	protected Boolean allFornecedor;
	protected Boolean allCentrocusto;
	protected Boolean allContagerencial;
	protected Boolean allEmpresa;
	protected Boolean allCaixa;
	protected Boolean allCartao;
	protected Boolean allContbanc;
	protected GenericBean radProjeto;
	
	//Receber
	protected Boolean allCliente;
	
	protected Boolean exibirTotais = Boolean.FALSE;
	protected Money emAtraso;
	protected Money emAberto;
	protected Money total;
	protected Money totalOriginal;
	
	protected String tipoConta;
	
	protected String motivo;
	
	protected OrdenacaoContapagarReport ordenacaoContapagarReport;
	protected OrdenacaoContareceberReport ordenacaoContareceberReport;
	protected Boolean ascReport = Boolean.TRUE;
	protected Tiporelatorio tiporelatorio;  
	
	protected Tipotaxa tipotaxa;
	private Categoria categoria;
	
	private Date dtemissaonotaInicio;
	private Date dtemissaonotaFim;
	
	private Money valorLote;
	private Boolean atualizarDtlimiteTaxa;
	private Boolean atualizarBoletoETaxas;
	private Date dtvencimentoLote;
	private Conta contaLote;
	private Contacarteira contacarteiraLote;
	private Cliente clienteLote;
	private Indicecorrecao indicecorrecaoLote;
	private Conta vinculoProvisionadoLote;
	
	private Integer numeroFaturaFrom;
	private Integer numeroFaturaTo;
	private Long numeroNotaFrom;
	private Long numeroNotaTo;
	private Integer numeroNfseTo;
	private Integer numeroNfseFrom;
	private String diferencaDiasDe;
	private String diferencaDiasAte;
	private Boolean exibeBaixados;
	
	
	protected Conta contaboleto;

	
	//relatorio csv
	protected Boolean exibeRateio = Boolean.TRUE;
	
	//totalizadores
	protected Money totalValorContaGerencial = new Money();
	protected Money totalValorCentroCusto = new Money();
	protected Money totalValorProjeto = new Money();
	protected Money totalSelecionado;
	
	{
		this.radProjeto = new GenericBean("todosProjetos", "Todos");
		
		listaDocumentoacao = new ListSet<Documentoacao>(Documentoacao.class);
		
		if (SinedUtil.isUserHasAction("CONFIRMAR_CONTAPAGAR")) {
			listaDocumentoacao.add(Documentoacao.PREVISTA);
		}
		
		if (SinedUtil.isUserHasAction("AUTORIZAR_CONTAPAGAR")) {
			listaDocumentoacao.add(Documentoacao.PREVISTA);
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
			listaDocumentoacao.add(Documentoacao.NAO_AUTORIZADA);
			listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);
		}
		
		if (SinedUtil.isUserHasAction("AUTORIZARPARCIAL_CONTAPAGAR")) {
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
			listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);
			listaDocumentoacao.add(Documentoacao.NAO_AUTORIZADA);
		}
		
		if (SinedUtil.isUserHasAction("NAOAUTORIZAR_CONTAPAGAR")) {
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
			listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);
		}
		
		if (SinedUtil.isUserHasAction("BAIXAR_CONTAPAGAR")) {
			listaDocumentoacao.add(Documentoacao.AUTORIZADA);
			listaDocumentoacao.add(Documentoacao.AUTORIZADA_PARCIAL);
			listaDocumentoacao.add(Documentoacao.PREVISTA);
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
			listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);
			listaDocumentoacao.add(Documentoacao.NAO_AUTORIZADA);
		}
		
		if (SinedUtil.isUserHasAction("ESTORNAR_CONTAPAGAR")) {
			listaDocumentoacao.add(Documentoacao.AUTORIZADA);
			listaDocumentoacao.add(Documentoacao.AUTORIZADA_PARCIAL);
			listaDocumentoacao.add(Documentoacao.NAO_AUTORIZADA);
		}
		
		if (SinedUtil.isUserHasAction("CANCELAR_CONTAPAGAR")) {
			listaDocumentoacao.add(Documentoacao.PREVISTA);
		}
		
		if (SinedUtil.isUserHasAction("CONFIRMAR_CONTARECEBER")) {
			listaDocumentoacao.add(Documentoacao.PREVISTA);
		}
		
		if (SinedUtil.isUserHasAction("BAIXAR_CONTARECEBER")) {
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
			listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);
		}
		
		if (SinedUtil.isUserHasAction("ESTORNAR_CONTARECEBER")) {
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
			listaDocumentoacao.add(Documentoacao.BAIXADA_PARCIAL);
		}
		
		if (SinedUtil.isUserHasAction("CANCELAR_CONTARECEBER")) {
			listaDocumentoacao.add(Documentoacao.PREVISTA);
		}
		
	}

	@DisplayName("V�nculo Provisionado")
	public Conta getVinculoProvisionado() {
		return vinculoProvisionado;
	}
	@DisplayName("ID")
	@MaxLength(9)
	public Integer getCddocumento() {
		return cddocumento;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Tipopagamento getTipopagamento() {
		return tipopagamento;
	}
	@DisplayName("Outros")
	public String getOutrospagamento() {
		return outrospagamento;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Situa��o")
	public List<Documentoacao> getListaDocumentoacao() {
		return listaDocumentoacao;
	}
	@DisplayName("Situa��o")
	public Documentoacao getDocumentoacao() {
		return documentoacao;
	}
	public Documentoclasse getDocumentoclasse() {
		return documentoclasse;
	}
	public Date getDtemissao1() {
		return dtemissao1;
	}
	@DisplayName("At�")
	public Date getDtemissao2() {
		return dtemissao2;
	}
	public Date getDtvencimento1() {
		return dtvencimento1;
	}
	@DisplayName("At�")
	public Date getDtvencimento2() {
		return dtvencimento2;
	}
	public Money getValorminimo() {
		return valorminimo;
	}
	@DisplayName("At�")
	public Money getValormaximo() {
		return valormaximo;
	}
	public Money getEmAberto() {
		return emAberto;
	}
	public Money getEmAtraso() {
		return emAtraso;
	}
	public Money getTotal() {
		return total;
	}
	public Money getTotalOriginal() {
		return totalOriginal;
	}
	public Boolean getProtestada() {
		return protestada;
	}
	@DisplayName("Projeto") 
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Centro de Custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Conta Gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@DisplayName("Pagamento a")
	public List<ItemDetalhe> getListaPagamentoa() {
		return listaPagamentoa;
	}
	@DisplayName("Recebimento de")
	public List<ItemDetalhe> getListaRecebimentode() {
		return listaRecebimentode;
	}
	@DisplayName("Tipos de documento")
	public List<Documentotipo> getListaDocumentotipo() {
		return listaDocumentotipo;
	}
	@DisplayName("Centro de custo")
	public List<ItemDetalhe> getListaCentrocusto() {
		return listaCentrocusto;
	}
	@DisplayName("Empresa")
	public List<ItemDetalhe> getListaEmpresa() {
		return listaEmpresa;
	}
	public List<ItemDetalhe> getListaOutrospagamento() {
		return listaOutrospagamento;
	}
	@DisplayName("Conta gerencial")
	public List<ItemDetalhe> getListaContagerencial() {
		return listaContagerencial;
	}
	@DisplayName("Caixas")
	public List<ItemDetalhe> getListaCaixa() {
		return listaCaixa;
	}
	@DisplayName("Cart�es de cr�dito")
	public List<ItemDetalhe> getListaCartao() {
		return listaCartao;
	}
	@DisplayName("Contas banc�rias")
	public List<ItemDetalhe> getListaContabancaria() {
		return listaContabancaria;
	}
	public List<ItemDetalhe> getListaColaborador() {
		return listaColaborador;
	}
	@DisplayName("Fornecedor")
	public List<ItemDetalhe> getListaFornecedor() {
		return listaFornecedor;
	}
	@DisplayName("Projeto")
	public List<ItemDetalhe> getListaProjeto() {
		return listaProjeto;
	}
	@DisplayName("Centro de custo")
	public Boolean getAllCentrocusto() {
		return allCentrocusto;
	}
	@DisplayName("Conta gerencial")
	public Boolean getAllContagerencial() {
		return allContagerencial;
	}
	@DisplayName("Empresas")
	public Boolean getAllEmpresa() {
		return allEmpresa;
	}
	@DisplayName("Projetos")
	public GenericBean getRadProjeto() {
		return radProjeto;
	}
	@DisplayName("Fornecedor")
	public Boolean getAllFornecedor() {
		return allFornecedor;
	}
	@DisplayName("A vencer")
	public Boolean getAvencer() {
		return avencer;
	}
	@DisplayName("Cliente")
	public Boolean getAllCliente() {
		return allCliente;
	}
	@DisplayName("Caixas")
	public Boolean getAllCaixa() {
		return allCaixa;
	}
	@DisplayName("Cart�es de cr�dito")
	public Boolean getAllCartao() {
		return allCartao;
	}
	@DisplayName("Contas banc�rias")
	public Boolean getAllContbanc() {
		return allContbanc;
	}
	public Boolean getQuitada() {
		return quitada;
	}
	public Boolean getVencida() {
		return vencida;
	}
	@DisplayName("Tipo de conta")
	public String getTipoConta() {
		return tipoConta;
	}
	@DisplayName("Cliente")
	public List<ItemDetalhe> getListaCliente() {
		return listaCliente;
	}

	@DisplayName("Mostrar projetos")
	public Boolean getProjetoAberto() {
		return projetoAberto;
	}
	public Date getDtbaixa1() {
		return dtbaixa1;
	}
	public Date getDtbaixa2() {
		return dtbaixa2;
	}
	@DisplayName("Tipo de v�nculo")
	public Contatipo getContatipo() {
		return contatipo;
	}
	@DisplayName("V�nculo")
	public Conta getConta() {
		return conta;
	}
	public Colaborador getVendedor() {
		return vendedor;
	}
	@DisplayName("Motivo do cancelamento")
	public Motivocancelamento getMotivocancelamento() {
		return motivocancelamento;
	}
	public Money getTotalValorContaGerencial() {
		return totalValorContaGerencial;
	}
	@DisplayName("Total valor centro de custo")
	public Money getTotalValorCentroCusto() {
		return totalValorCentroCusto;
	}
	@DisplayName("Exibir Itens do Rateio")
	public Boolean getExibeRateio() {
		return exibeRateio;
	}
	
	public Money getTotalValorProjeto() {
		return totalValorProjeto;
	}
	
	public Contratotipo getContratotipo() {
		return contratotipo;
	}
	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}

	public void setMotivocancelamento(Motivocancelamento motivocancelamento) {
		this.motivocancelamento = motivocancelamento;
	}
	public void setVendedor(Colaborador vendedor) {
		this.vendedor = vendedor;
	}
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setDtbaixa1(Date dtbaixa1) {
		this.dtbaixa1 = dtbaixa1;
	}
	public void setDtbaixa2(Date dtbaixa2) {
		this.dtbaixa2 = dtbaixa2;
	}
	public void setProjetoAberto(Boolean projetoAberto) {
		this.projetoAberto = projetoAberto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setListaDocumentotipo(List<Documentotipo> listaDocumentotipo) {
		this.listaDocumentotipo = listaDocumentotipo;
	}
	public void setListaPagamentoa(List<ItemDetalhe> listaPagamentoa) {
		this.listaPagamentoa = listaPagamentoa;
	}
	public void setListaRecebimentode(List<ItemDetalhe> listaRecebimentode) {
		this.listaRecebimentode = listaRecebimentode;
	}
	public void setListaCentrocusto(List<ItemDetalhe> listaCentrocusto) {
		this.listaCentrocusto = listaCentrocusto;
	}
	public void setListaEmpresa(List<ItemDetalhe> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
	public void setListaOutrospagamento(List<ItemDetalhe> listaOutrospagamento) {
		this.listaOutrospagamento = listaOutrospagamento;
	}
	public void setListaContagerencial(List<ItemDetalhe> listaContagerencial) {
		this.listaContagerencial = listaContagerencial;
	}
	public void setListaCaixa(List<ItemDetalhe> listaCaixa) {
		this.listaCaixa = listaCaixa;
	}
	public void setListaCartao(List<ItemDetalhe> listaCartao) {
		this.listaCartao = listaCartao;
	}
	public void setListaContabancaria(List<ItemDetalhe> listaContabancaria) {
		this.listaContabancaria = listaContabancaria;
	}
	public void setListaColaborador(List<ItemDetalhe> listaColaborador) {
		this.listaColaborador = listaColaborador;
	}
	public void setListaFornecedor(List<ItemDetalhe> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}
	public void setListaProjeto(List<ItemDetalhe> listaProjeto) {
		this.listaProjeto = listaProjeto;
	}
	public void setAllCentrocusto(Boolean allCentrocusto) {
		this.allCentrocusto = allCentrocusto;
	}
	public void setAllCaixa(Boolean allCaixa) {
		this.allCaixa = allCaixa;
	}
	public void setAllCartao(Boolean allCartao) {
		this.allCartao = allCartao;
	}
	public void setAllContbanc(Boolean allContbanc) {
		this.allContbanc = allContbanc;
	}
	public void setAllContagerencial(Boolean allContagerencial) {
		this.allContagerencial = allContagerencial;
	}
	public void setAllEmpresa(Boolean allEmpresa) {
		this.allEmpresa = allEmpresa;
	}
	public void setRadProjeto(GenericBean radProjeto) {
		this.radProjeto = radProjeto;
	}
	public void setAllFornecedor(Boolean allFornecedor) {
		this.allFornecedor = allFornecedor;
	}
	public void setQuitada(Boolean quitada) {
		this.quitada = quitada;
	}
	public void setVencida(Boolean vencida) {
		this.vencida = vencida;
	}
	public void setAvencer(Boolean avencer) {
		this.avencer = avencer;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setProtestada(Boolean protestada) {
		this.protestada = protestada;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setTipopagamento(Tipopagamento tipopagamento) {
		this.tipopagamento = tipopagamento;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setOutrospagamento(String outrospagamento) {
		this.outrospagamento = outrospagamento;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setListaDocumentoacao(List<Documentoacao> listaDocumentoacao) {
		this.listaDocumentoacao = listaDocumentoacao;
	}
	public void setDocumentoacao(Documentoacao documentoacao) {
		this.documentoacao = documentoacao;
	}
	public void setDocumentoclasse(Documentoclasse documentoclasse) {
		this.documentoclasse = documentoclasse;
	}
	public void setDtemissao1(Date dtemissao) {
		this.dtemissao1 = dtemissao;
	}
	public void setDtvencimento1(Date dtvencimento) {
		this.dtvencimento1 = dtvencimento;
	}
	public void setValorminimo(Money valor) {
		this.valorminimo = valor;
	}
	public void setDtemissao2(Date dtemissao2) {
		this.dtemissao2 = dtemissao2;
	}
	public void setDtvencimento2(Date dtvencimento2) {
		this.dtvencimento2 = dtvencimento2;
	}
	public void setValormaximo(Money valor2) {
		this.valormaximo = valor2;
	}
	public void setEmAberto(Money emAberto) {
		this.emAberto = emAberto;
	}
	public void setEmAtraso(Money emAtraso) {
		this.emAtraso = emAtraso;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	public void setTotalOriginal(Money totalOriginal) {
		this.totalOriginal = totalOriginal;
	}
	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}
	public void setAllCliente(Boolean allCliente) {
		this.allCliente = allCliente;
	}
	public void setListaCliente(List<ItemDetalhe> listaCliente) {
		this.listaCliente = listaCliente;
	}
	@DisplayName("Pago a")
	public Boolean getAllPagamentoa() {
		return allPagamentoa;
	}
	@DisplayName("Recebido de")
	public Boolean getAllRecebimentode() {
		return allRecebimentode;
	}
	public void setAllPagamentoa(Boolean allPagamentoa) {
		this.allPagamentoa = allPagamentoa;
	}
	public void setAllRecebimentode(Boolean allRecebimentode) {
		this.allRecebimentode = allRecebimentode;
	}
	@DisplayName("")
	public String getPessoa() {
		return pessoa;
	}
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	@MaxLength(15)
	@DisplayName("Cheque")
	public String getCheque() {
		return cheque;
	}
	public void setCheque(String cheque) {
		this.cheque = cheque;
	}
	@DisplayName("Campo do relat�rio")
	public OrdenacaoContapagarReport getOrdenacaoContapagarReport() {
		return ordenacaoContapagarReport;
	}
	@DisplayName("Campo do relat�rio")
	public OrdenacaoContareceberReport getOrdenacaoContareceberReport() {
		return ordenacaoContareceberReport;
	}
	@DisplayName("Tipo de ordena��o do relat�rio")
	public Boolean getAscReport() {
		return ascReport;
	}
	public void setOrdenacaoContapagarReport(OrdenacaoContapagarReport ordenacaoContapagarReport) {
		this.ordenacaoContapagarReport = ordenacaoContapagarReport;
	}
	public void setOrdenacaoContareceberReport(OrdenacaoContareceberReport ordenacaoContareceberReport) {
		this.ordenacaoContareceberReport = ordenacaoContareceberReport;
	}
	public void setAscReport(Boolean ascReport) {
		this.ascReport = ascReport;
	}
	public Date getDtiniciolancamento() {
		return dtiniciolancamento;
	}
	public Date getDtfimlancamento() {
		return dtfimlancamento;
	}
	public void setDtiniciolancamento(Date dtiniciolancamento) {
		this.dtiniciolancamento = dtiniciolancamento;
	}	
	public void setDtfimlancamento(Date dtfimlancamento) {
		this.dtfimlancamento = dtfimlancamento;
	}
	@DisplayName("Motivo da Taxa")
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	@DisplayName("Tipo de Taxa")
	public Tipotaxa getTipotaxa() {
		return tipotaxa;
	}
	public void setTipotaxa(Tipotaxa tipotaxa) {
		this.tipotaxa = tipotaxa;
	}
	public void setTotalValorCentroCusto(Money totalValorCentroCusto) {
		this.totalValorCentroCusto = totalValorCentroCusto;
	}
	public void setTotalValorContaGerencial(Money totalValorContaGerencial) {
		this.totalValorContaGerencial = totalValorContaGerencial;
	}
	public void setTotalValorProjeto(Money totalValorProjeto) {
		this.totalValorProjeto = totalValorProjeto;
	}
	
	@DisplayName("Conta banc�ria do boleto")
	public Conta getContaboleto() {
		return contaboleto;
	}
	public void setContaboleto(Conta contaboleto) {
		this.contaboleto = contaboleto;
	}
	
	public Date getDtprimeirovencimento1() {
		return dtprimeirovencimento1;
	}
	
	public Date getDtprimeirovencimento2() {
		return dtprimeirovencimento2;
	}
	
	public void setDtprimeirovencimento1(Date dtprimeirovencimento1) {
		this.dtprimeirovencimento1 = dtprimeirovencimento1;
	}
	
	public void setDtprimeirovencimento2(Date dtprimeirovencimento2) {
		this.dtprimeirovencimento2 = dtprimeirovencimento2;
	}

	public Boolean getIsRestricaoClienteVendedor() {
		return isRestricaoClienteVendedor;
	}
	public void setIsRestricaoClienteVendedor(Boolean isRestricaoClienteVendedor) {
		this.isRestricaoClienteVendedor = isRestricaoClienteVendedor;
	}
	
	@DisplayName("Categoria")
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	public Date getDtemissaonotaInicio() {
		return dtemissaonotaInicio;
	}
	
	public Date getDtemissaonotaFim() {
		return dtemissaonotaFim;
	}
	
	public void setDtemissaonotaInicio(Date dtemissaonotaInicio) {
		this.dtemissaonotaInicio = dtemissaonotaInicio;
	}
	
	public void setDtemissaonotaFim(Date dtemissaonotaFim) {
		this.dtemissaonotaFim = dtemissaonotaFim;
	}
	
	@DisplayName("Valor")
	public Money getValorLote() {
		return valorLote;
	}
	@DisplayName("Ajustar vencimento das taxas?")
	public Boolean getAtualizarDtlimiteTaxa() {
		return atualizarDtlimiteTaxa;
	}
	@DisplayName("Data Vencimento")
	public Date getDtvencimentoLote() {
		return dtvencimentoLote;
	}
	@DisplayName("Conta Banc�ria")
	public Conta getContaLote() {
		return contaLote;
	}
	@DisplayName("Carteira")
	public Contacarteira getContacarteiraLote() {
		return contacarteiraLote;
	}
	@DisplayName("Cliente")
	public Cliente getClienteLote() {
		return clienteLote;
	}
	@DisplayName("�ndice de Corre��o")
	public Indicecorrecao getIndicecorrecaoLote() {
		return indicecorrecaoLote;
	}
	@DisplayName("V�nculo provisionado")
	public Conta getVinculoProvisionadoLote() {
		return vinculoProvisionadoLote;
	}

	public void setValorLote(Money valorLote) {
		this.valorLote = valorLote;
	}
	public void setAtualizarDtlimiteTaxa(Boolean atualizarDtlimiteTaxa) {
		this.atualizarDtlimiteTaxa = atualizarDtlimiteTaxa;
	}
	public void setDtvencimentoLote(Date dtvencimentoLote) {
		this.dtvencimentoLote = dtvencimentoLote;
	}
	public void setContaLote(Conta contaLote) {
		this.contaLote = contaLote;
	}
	public void setContacarteiraLote(Contacarteira contacarteiraLote) {
		this.contacarteiraLote = contacarteiraLote;
	}
	public void setClienteLote(Cliente clienteLote) {
		this.clienteLote = clienteLote;
	}
	public void setIndicecorrecaoLote(Indicecorrecao indicecorrecaoLote) {
		this.indicecorrecaoLote = indicecorrecaoLote;
	}	
	public void setVinculoProvisionadoLote(Conta vinculoProvisionadoLote) {
		this.vinculoProvisionadoLote = vinculoProvisionadoLote;
	}
	public void setVinculoProvisionado(Conta vinculoProvisionado) {
		this.vinculoProvisionado = vinculoProvisionado;
	}
	public Integer getNumeroFaturaFrom() {
		return numeroFaturaFrom;
	}
	public Integer getNumeroFaturaTo() {
		return numeroFaturaTo;
	}
	public Long getNumeroNotaFrom() {
		return numeroNotaFrom;
	}
	public Long getNumeroNotaTo() {
		return numeroNotaTo;
	}
	public Integer getNumeroNfseFrom() {
		return numeroNfseFrom;
	}
	public Integer getNumeroNfseTo() {
		return numeroNfseTo;
	}
	public void setNumeroNfseFrom(Integer numeroNfseFrom) {
		this.numeroNfseFrom = numeroNfseFrom;
	}
	public void setNumeroNfseTo(Integer numeroNfseTo) {
		this.numeroNfseTo = numeroNfseTo;
	}
	public void setNumeroFaturaFrom(Integer numeroFaturaFrom) {
		this.numeroFaturaFrom = numeroFaturaFrom;
	}
	public void setNumeroFaturaTo(Integer numeroFaturaTo) {
		this.numeroFaturaTo = numeroFaturaTo;
	}
	public void setNumeroNotaFrom(Long numeroNotaFrom) {
		this.numeroNotaFrom = numeroNotaFrom;
	}
	public void setNumeroNotaTo(Long numeroNotaTo) {
		this.numeroNotaTo = numeroNotaTo;
	}
	
	public Boolean getExibirTotais() {
		return exibirTotais;
	}
	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}
	
	
	public String getDocumentoDe() {
		return documentoDe;
	}
	public String getDocumentoAte() {
		return documentoAte;
	}
	public void setDocumentoDe(String documentoDe) {
		this.documentoDe = documentoDe;
	}
	public void setDocumentoAte(String documentoAte) {
		this.documentoAte = documentoAte;
	}
	@DisplayName("Tipo do relat�rio")
	public Tiporelatorio getTiporelatorio() {
		return tiporelatorio;
	}
	public void setTiporelatorio(Tiporelatorio tiporelatorio) {
		this.tiporelatorio = tiporelatorio;
	}
	
	public boolean isTiporelatorioSimplificado(){
		return getTiporelatorio() != null && Tiporelatorio.SIMPLIFICADO.equals(getTiporelatorio());
	}
	public Boolean getPermitirFiltroDocumentoDeAte() {
		return permitirFiltroDocumentoDeAte;
	}
	public void setPermitirFiltroDocumentoDeAte(Boolean permitirFiltroDocumentoDeAte) {
		this.permitirFiltroDocumentoDeAte = permitirFiltroDocumentoDeAte;
	}
	
	@DisplayName("Situa��o do Protesto")
	public Documentoacao getDocumentoacaoprotesto() {
		return documentoacaoprotesto;
	}
	public void setDocumentoacaoprotesto(Documentoacao documentoacaoprotesto) {
		this.documentoacaoprotesto = documentoacaoprotesto;
	}
	
	public String getVendaDe() {
		return vendaDe;
	}
	public void setVendaDe(String vendaDe) {
		this.vendaDe = vendaDe;
	}
	
	public String getVendaAte() {
		return vendaAte;
	}
	public void setVendaAte(String vendaAte) {
		this.vendaAte = vendaAte;
	}
	
	@DisplayName("Tipo de Projeto")
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}
	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}
	@DisplayName("Documento / Descri��o")
	@MaxLength(100)
	public String getDocumentoDescricao() {
		return documentoDescricao;
	}
	public void setDocumentoDescricao(String documentoDescricao) {
		this.documentoDescricao = documentoDescricao;
	}
	
	@MaxLength(9)
	public String getDiferencaDiasDe() {
		return diferencaDiasDe;
	}
	public void setDiferencaDiasAte(String diferencaDiasAte) {
		this.diferencaDiasAte = diferencaDiasAte;
	}
	
	@MaxLength(9)
	public String getDiferencaDiasAte() {
		return diferencaDiasAte;
	}
	public void setDiferencaDiasDe(String diferencaDiasDe) {
		this.diferencaDiasDe = diferencaDiasDe;
	}
	
	public Boolean getExibeBaixados() {
		exibeBaixados = (this.listaDocumentoacao != null &&
						(this.listaDocumentoacao.contains(Documentoacao.BAIXADA) || this.listaDocumentoacao.contains(Documentoacao.BAIXADA_PARCIAL)));
		return exibeBaixados;
	}
	public void setExibeBaixados(Boolean exibeBaixados) {
		this.exibeBaixados = exibeBaixados;
	}
	@DisplayName("Documento antecipado")
	public Boolean getAntecipado() {
		return antecipado;
	}
	@DisplayName("Associa��o")
	public Fornecedor getAssociacao() {
		return associacao;
	}
	public void setAssociacao(Fornecedor associacao) {
		this.associacao = associacao;
	}
	
	public void setAntecipado(Boolean antecipado) {
		this.antecipado = antecipado;
	}
	public Boolean getAtualizarBoletoETaxas() {
		return atualizarBoletoETaxas;
	}
	public void setAtualizarBoletoETaxas(Boolean atualizarBoletoETaxas) {
		this.atualizarBoletoETaxas = atualizarBoletoETaxas;
	}
	public void setExibeRateio(Boolean exibeRateio) {
		this.exibeRateio = exibeRateio;
	}
	public Money getTotalSelecionado() {
		Money totalSelecionado = new Money();
		return totalSelecionado;
	}
	public void setTotalSelecionado(Money totalSelecionado) {
		this.totalSelecionado = totalSelecionado;
	}
	public Date getDtcompetencia1() {
		return dtcompetencia1;
	}
	public void setDtcompetencia1(Date dtcompetencia1) {
		this.dtcompetencia1 = dtcompetencia1;
	}
	public Date getDtcompetencia2() {
		return dtcompetencia2;
	}
	public void setDtcompetencia2(Date dtcompetencia2) {
		this.dtcompetencia2 = dtcompetencia2;
	}
}