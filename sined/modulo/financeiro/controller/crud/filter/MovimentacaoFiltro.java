package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoReport;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MovimentacaoFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Contatipo contatipo;
	protected Conta conta;
	protected Money valorDe;
	protected Money valorAte;
	protected Date dtperiodomovimentacaoDe;
	protected Date dtperiodomovimentacaoAte;
	protected Date dtperiodoDe;
	protected Date dtperiodoAte;
	protected Date dtbancoDe;
	protected Date dtbancoAte;
	protected Date dtalteracaoDe;
	protected Date dtalteracaoAte;
	protected Tipooperacao tipooperacao;
	protected String chequeHistoricoChecknum;
	protected Centrocusto centrocusto;
	protected Contagerencial contagerencial;
	protected Projeto projeto;
	protected List<Movimentacaoacao> listaAcao;
	protected Formapagamento formapagamento;
	protected NaturezaContagerencial naturezaContagerencial;
	protected String cddocumentos;
	protected String cdmovimentacao;
	protected Integer cdvenda;
	protected Documentotipo documentotipo;
	protected Tipopagamento tipopagamento;
	protected Colaborador colaborador;
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected String outrospagamento;
	protected String pessoa;
	protected String bordero;
	
	protected Money credito;
	protected Money debito;
	protected String saldo;
	protected Boolean antecipada;
	protected Date dtbomparade;
	protected Date dtbomparaate;
	protected OrdenacaoReport ordenacaoReport;
	protected Boolean ascReport;
	protected Date dtcompravendaDe;
	protected Date dtcompravendaAte;
	protected Boolean exibirPessoa;
	
	//totalizadores
	protected Money totalCreditoContaGerencial;
	protected Money totalDebitoContaGerencial;
	protected Money saldoContaGerencial;
	protected Money totalCreditoCentroCusto;
	protected Money totalDebitoCentroCusto;
	protected Money saldoCentroCusto;
	protected Money totalCreditoProjeto;
	protected Money totalDebitoProjeto;
	protected Money saldoProjeto;
	
	public MovimentacaoFiltro() {
		this.dtperiodoDe = SinedDateUtils.firstDateOfMonth();
		this.naturezaContagerencial = NaturezaContagerencial.CONTA_GERENCIAL;
		listaAcao = new ArrayList<Movimentacaoacao>();
		listaAcao.add(Movimentacaoacao.NORMAL);
		listaAcao.add(Movimentacaoacao.CONCILIADA);	
	}

	public MovimentacaoFiltro(String cdmovimentacao, Boolean exibirPessoa) {
		this.cdmovimentacao = cdmovimentacao;
		this.exibirPessoa = exibirPessoa;
	}

	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("V�nculo")
	public Contatipo getContatipo() {
		return contatipo;
	}
	public Conta getConta() {
		return conta;
	}
	public Money getValorDe() {
		return valorDe;
	}
	public Money getValorAte() {
		return valorAte;
	}
	@DisplayName("Per�odo")
	public Date getDtperiodoDe() {
		return dtperiodoDe;
	}
	@DisplayName("Data do banco")
	public Date getDtbancoDe() {
		return dtbancoDe;
	}
	public Date getDtbancoAte() {
		return dtbancoAte;
	}
	@DisplayName("Data de compra/venda")
	public Date getDtcompravendaDe() {
		return dtcompravendaDe;
	}
	public Date getDtcompravendaAte() {
		return dtcompravendaAte;
	}
	@DisplayName("Natureza do Plano de Contas")
	public NaturezaContagerencial getNaturezaContagerencial() {
		return naturezaContagerencial;
	}	
	public String getCddocumentos() {
		return cddocumentos;
	}	
	public String getCdmovimentacao() {
		return cdmovimentacao;
	}
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setCdmovimentacao(String cdmovimentacao) {
		this.cdmovimentacao = cdmovimentacao;
	}
	public void setCddocumentos(String cddocumentos) {
		this.cddocumentos = cddocumentos;
	}
	public void setDtbancoDe(Date dtbancoDe) {
		this.dtbancoDe = dtbancoDe;
	}
	public void setDtbancoAte(Date dtbancoAte) {
		this.dtbancoAte = dtbancoAte;
	}
	public void setDtcompravendaDe(Date dtcompravendaDe) {
		this.dtcompravendaDe = dtcompravendaDe;
	}
	public void setDtcompravendaAte(Date dtcompravendaAte) {
		this.dtcompravendaAte = dtcompravendaAte;
	}
	public Date getDtperiodoAte() {
		return dtperiodoAte;
	}
	public Date getDtalteracaoDe() {
		return dtalteracaoDe;
	}
	public Date getDtalteracaoAte() {
		return dtalteracaoAte;
	}
	@DisplayName("Tipo de opera��o")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	@DisplayName("N�mero do Cheque / Hist�rico/Observa��o / Documento")
	@MaxLength(100)
	public String getChequeHistoricoChecknum() {
		return chequeHistoricoChecknum;
	}
	@DisplayName("Situa��o")
	public List<Movimentacaoacao> getListaAcao() {
		return listaAcao;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Total de cr�dito")
	public Money getCredito() {
		return credito;
	}
	@DisplayName("Total de d�bito")
	public Money getDebito() {
		return debito;
	}
	@DisplayName("Saldo total")
	public String getSaldo() {
		return saldo;
	}
	@DisplayName("Forma de pagamento")
	public Formapagamento getFormapagamento() {
		return formapagamento;
	}
	
	@DisplayName("Ordenar dados por:")
	public OrdenacaoReport getOrdenacaoReport() {
		return ordenacaoReport;
	}
	
	@DisplayName("Tipo de ordena��o do relat�rio")
	public Boolean getAscReport() {
		return ascReport;
	}
	
	@DisplayName("Total de cr�dito conta gerencial")
	public Money getTotalCreditoContaGerencial() {
		return totalCreditoContaGerencial;
	}
	
	@DisplayName("Total de d�bito conta gerencial")
	public Money getTotalDebitoContaGerencial() {
		return totalDebitoContaGerencial;
	}
	
	public Money getSaldoContaGerencial() {
		return saldoContaGerencial;
	}
	
	@DisplayName("Total de cr�dito centro de custo")
	public Money getTotalCreditoCentroCusto() {
		return totalCreditoCentroCusto;
	}
	@DisplayName("Total de d�bito centro de custo")
	public Money getTotalDebitoCentroCusto() {
		return totalDebitoCentroCusto;
	}
	public Money getSaldoCentroCusto() {
		return saldoCentroCusto;
	}
	@DisplayName("Total de cr�dito projeto")
	public Money getTotalCreditoProjeto() {
		return totalCreditoProjeto;
	}
	@DisplayName("Total de d�bito projeto")
	public Money getTotalDebitoProjeto() {
		return totalDebitoProjeto;
	}
	public Money getSaldoProjeto() {
		return saldoProjeto;
	}
	
	
	public void setAscReport(Boolean ascReport) {
		this.ascReport = ascReport;
	}
	
	public void setOrdenacaoReport(OrdenacaoReport ordenacaoReport) {
		this.ordenacaoReport = ordenacaoReport;
	}
	public void setFormapagamento(Formapagamento formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setValorDe(Money valorDe) {
		this.valorDe = valorDe;
	}
	public void setValorAte(Money valorAte) {
		this.valorAte = valorAte;
	}
	public void setDtperiodoDe(Date dtmovimentacaoDe) {
		this.dtperiodoDe = dtmovimentacaoDe;
	}
	public void setDtperiodoAte(Date dtperiodoAte) {
		this.dtperiodoAte = dtperiodoAte;
	}
	public void setDtalteracaoDe(Date dtalteracaoDe) {
		this.dtalteracaoDe = dtalteracaoDe;
	}
	public void setDtalteracaoAte(Date dtalteracaoAte) {
		this.dtalteracaoAte = dtalteracaoAte;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	public void setListaAcao(List<Movimentacaoacao> listaAcao) {
		this.listaAcao = listaAcao;
	}
	public void setCredito(Money credito) {
		this.credito = credito;
	}
	public void setDebito(Money debito) {
		this.debito = debito;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	public void setNaturezaContagerencial(
			NaturezaContagerencial naturezaContagerencial) {
		this.naturezaContagerencial = naturezaContagerencial;
	}
	public Boolean getAntecipada() {
		return antecipada;
	}
	public void setAntecipada(Boolean antecipada) {
		this.antecipada = antecipada;
	}
	public Date getDtbomparade() {
		return dtbomparade;
	}
	public Date getDtbomparaate() {
		return dtbomparaate;
	}
	@DisplayName("Venda")
	public Integer getCdvenda() {
		return cdvenda;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setDtbomparade(Date dtbomparade) {
		this.dtbomparade = dtbomparade;
	}
	public void setDtbomparaate(Date dtbomparaate) {
		this.dtbomparaate = dtbomparaate;
	}
	public Tipopagamento getTipopagamento() {
		return tipopagamento;
	}
	@DisplayName("Outros")
	public String getOutrospagamento() {
		return outrospagamento;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("")
	public String getPessoa() {
		return pessoa;
	}
	public void setTipopagamento(Tipopagamento tipopagamento) {
		this.tipopagamento = tipopagamento;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setOutrospagamento(String outrospagamento) {
		this.outrospagamento = outrospagamento;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	public void setTotalCreditoContaGerencial(Money totalCreditoContaGerencial) {
		this.totalCreditoContaGerencial = totalCreditoContaGerencial;
	}
	public void setTotalDebitoContaGerencial(Money totalDebitoContaGerencial) {
		this.totalDebitoContaGerencial = totalDebitoContaGerencial;
	}
	public void setSaldoContaGerencial(Money saldoContaGerencial) {
		this.saldoContaGerencial = saldoContaGerencial;
	}
	public void setTotalCreditoCentroCusto(Money totalCreditoCentroCusto) {
		this.totalCreditoCentroCusto = totalCreditoCentroCusto;
	}
	public void setTotalDebitoCentroCusto(Money totalDebitoCentroCusto) {
		this.totalDebitoCentroCusto = totalDebitoCentroCusto;
	}
	public void setSaldoCentroCusto(Money saldoCentroCusto) {
		this.saldoCentroCusto = saldoCentroCusto;
	}
	public void setTotalCreditoProjeto(Money totalCreditoProjeto) {
		this.totalCreditoProjeto = totalCreditoProjeto;
	}
	public void setTotalDebitoProjeto(Money totalDebitoProjeto) {
		this.totalDebitoProjeto = totalDebitoProjeto;
	}
	public void setSaldoProjeto(Money saldoProjeto) {
		this.saldoProjeto = saldoProjeto;
	}	
	public void setChequeHistoricoChecknum(String chequeHistoricoChecknum) {
		this.chequeHistoricoChecknum = chequeHistoricoChecknum;
	}
	@DisplayName("Border�")
	@MaxLength(10)
	public String getBordero() {
		return bordero;
	}	
	public void setBordero(String bordero) {
		this.bordero = bordero;
	}
	
	@DisplayName("Exibir cliente/fornecedor")
	public Boolean getExibirPessoa() {
		return exibirPessoa;
	}
	public void setExibirPessoa(Boolean exibirPessoa) {
		this.exibirPessoa = exibirPessoa;
	}
	
	@DisplayName("Per�odo de Movimenta��o")
	public Date getDtperiodomovimentacaoDe() {
		return dtperiodomovimentacaoDe;
	}
	public void setDtperiodomovimentacaoDe(Date dtperiodomovimentacaoDe) {
		this.dtperiodomovimentacaoDe = dtperiodomovimentacaoDe;
	}
	public Date getDtperiodomovimentacaoAte() {
		return dtperiodomovimentacaoAte;
	}
	public void setDtperiodomovimentacaoAte(Date dtperiodomovimentacaoAte) {
		this.dtperiodomovimentacaoAte = dtperiodomovimentacaoAte;
	}
}