package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.util.Date;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.SituacaoCusteio;
import br.com.linkcom.sined.geral.bean.TipoDocumentoCusteioEnum;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CusteioProcessadoFiltro  extends FiltroListagemSined{

	
	private Empresa empresa;
	private Date dtInicio;
	private Date dtFim;
	private Date dtProcessamento;
	private Usuario usuario;
	private SituacaoCusteio situacaoCusteio;
	private TipoDocumentoCusteioEnum tipoDocumentoCusteioEnum;
	private Integer codObjetoOrigem;
	
	//GET
	public Empresa getEmpresa() {
		return empresa;
	}
	public Date getDtInicio() {
		return dtInicio;
	}
	public Date getDtFim() {
		return dtFim;
	}
	public Date getDtProcessamento() {
		return dtProcessamento;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public SituacaoCusteio getSituacaoCusteio() {
		return situacaoCusteio;
	}
	public TipoDocumentoCusteioEnum getTipoDocumentoCusteioEnum() {
		return tipoDocumentoCusteioEnum;
	}
	public Integer getCodObjetoOrigem() {
		return codObjetoOrigem;
	}
	//SET
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setDtProcessamento(Date dtProcessamento) {
		this.dtProcessamento = dtProcessamento;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setSituacaoCusteio(SituacaoCusteio situacaoCusteio) {
		this.situacaoCusteio = situacaoCusteio;
	}
	public void setTipoDocumentoCusteioEnum(
			TipoDocumentoCusteioEnum tipoDocumentoCusteioEnum) {
		this.tipoDocumentoCusteioEnum = tipoDocumentoCusteioEnum;
	}
	public void setCodObjetoOrigem(Integer codObjetoOrigem) {
		this.codObjetoOrigem = codObjetoOrigem;
	}
	
	
	
	
}
