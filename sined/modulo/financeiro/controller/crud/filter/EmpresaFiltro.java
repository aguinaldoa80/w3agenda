package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmpresaFiltro extends FiltroListagemSined{

	protected String nome;
	protected String razaosocial;
	protected Boolean ativo;
	protected Boolean propriedadeRural;

	@DisplayName("Nome")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@DisplayName("Raz�o Social")
	@MaxLength(100)
	public String getRazaosocial() {
		return razaosocial;
	}
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Fazenda")
	public Boolean getPropriedadeRural() {
		return propriedadeRural;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public void setPropriedadeRural(Boolean propriedadeRural) {
		this.propriedadeRural = propriedadeRural;
	}
}
