package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.util.Util;

public enum LancDocumento {

	
	PAGAR_ABERTO("Lanšamentos em contas a pagar (em aberto)",null),
	PAGAR_ATRASO("Lanšamentos em contas a pagar (em atraso)",null),
	RECEBER_ABERTO("Lanšamentos em contas a receber (em aberto)",null),
	RECEBER_ATRASO("Lanšamentos em contas a receber (em atraso)",null);
	
	private LancDocumento(String descricao, Boolean check){
		this.descricao = descricao;
		this.check = Util.booleans.isTrue(check);
	}
	
	private String descricao;
	private boolean check;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public boolean isCheck() {
		return check;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
	
}
