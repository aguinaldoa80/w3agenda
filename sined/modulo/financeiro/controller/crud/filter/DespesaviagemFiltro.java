package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemdestino;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration.ReembolsoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DespesaviagemFiltro extends FiltroListagemSined{

	protected String origemviagem;
	protected String destinoviagem;
	protected Date dtsaida1;
	protected Date dtsaida2;
	protected Date dtretornorealizado1;
	protected Date dtretornorealizado2;
	protected Colaborador colaborador;
	protected Projeto projeto;
	protected List<Situacaodespesaviagem> listaSituacao;
	protected Integer cddespesaviagem;
	protected Despesaviagemdestino despesaviagemdestino;
	protected Contacrm contacrm;
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected Endereco endereco;
	protected Contacrmcontato contacrmcontato;
	
	protected Money totalPrevisto;
	protected Money totalRealizado;
	
	protected Boolean restricaodespesaviagem;
	protected Boolean reembolsavel;
	
	protected ReembolsoEnum reembolso;
	
	@DisplayName("Origem da viagem")
	@MaxLength(100)
	public String getOrigemviagem() {
		return origemviagem;
	}
	@DisplayName("Destino da viagem")
	@MaxLength(100)
	public String getDestinoviagem() {
		return destinoviagem;
	}
	@DisplayName("Respons�vel pela despesa de viagem")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public List<Situacaodespesaviagem> getListaSituacao() {
		return listaSituacao;
	}
	public Date getDtsaida1() {
		return dtsaida1;
	}
	public Date getDtsaida2() {
		return dtsaida2;
	}
	public Date getDtretornorealizado1() {
		return dtretornorealizado1;
	}
	public Date getDtretornorealizado2() {
		return dtretornorealizado2;
	}
	@DisplayName("Destino �")
	public Despesaviagemdestino getDespesaviagemdestino() {
		return despesaviagemdestino;
	}
	public Boolean getReembolsavel() {
		return reembolsavel;
	}
		
	public ReembolsoEnum getReembolso() {
		return reembolso;
	}
	public void setReembolso(ReembolsoEnum reembolso) {
		this.reembolso = reembolso;
	}
	public void setDtsaida1(Date dtsaida1) {
		this.dtsaida1 = dtsaida1;
	}
	public void setDtsaida2(Date dtsaida2) {
		this.dtsaida2 = dtsaida2;
	}
	public void setDtretornorealizado1(Date dtretornorealizado1) {
		this.dtretornorealizado1 = dtretornorealizado1;
	}
	public void setDtretornorealizado2(Date dtretornorealizado2) {
		this.dtretornorealizado2 = dtretornorealizado2;
	}
	public void setListaSituacao(List<Situacaodespesaviagem> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setOrigemviagem(String origemviagem) {
		this.origemviagem = origemviagem;
	}
	public void setDestinoviagem(String destinoviagem) {
		this.destinoviagem = destinoviagem;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public Boolean getRestricaodespesaviagem() {
		return restricaodespesaviagem;
	}
	public void setRestricaodespesaviagem(Boolean restricaodespesaviagem) {
		this.restricaodespesaviagem = restricaodespesaviagem;
	}
	
	@DisplayName("C�digo")
	public Integer getCddespesaviagem() {
		return cddespesaviagem;
	}
	public void setCddespesaviagem(Integer cddespesaviagem) {
		this.cddespesaviagem = cddespesaviagem;
	}
	
	public Money getTotalPrevisto() {
		return totalPrevisto;
	}
	public void setTotalPrevisto(Money totalPrevisto) {
		this.totalPrevisto = totalPrevisto;
	}
	
	public Money getTotalRealizado() {
		return totalRealizado;
	}
	public void setTotalRealizado(Money totalRealizado) {
		this.totalRealizado = totalRealizado;
	}
	public void setDespesaviagemdestino(Despesaviagemdestino despesaviagemdestino) {
		this.despesaviagemdestino = despesaviagemdestino;
	}
	public void setReembolsavel(Boolean reembolsavel) {
		this.reembolsavel = reembolsavel;
	}
	
	@DisplayName("CONTA CRM")
	public Contacrm getContacrm() {
		return contacrm;
	}
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@DisplayName("Endere�o")
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	@DisplayName("Endere�o")
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}
	
	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}
}
