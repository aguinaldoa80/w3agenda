package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AgendamentoFiltro extends FiltroListagemSined {
	
	private Integer idagendamento;
	private String descricao;
	private Tipooperacao tipooperacao;
	private Tipopagamento tipopagamento;
	private Frequencia frequencia;
	private List<SituacaoAgendamento> listaSituacao;
	private Date dtVencimentoInicio;
	private Date dtVencimentoFim;
	private Money valorInicio;
	private Money valorFim;
	private Money totalDebito;
	private Money totalCredito;
	private Boolean ativo = true;
	private Empresa empresa;
	private String pessoa;
	private Centrocusto centrocusto;
	private Cliente cliente;
	private Colaborador colaborador;
	private Fornecedor fornecedor;
	private Conta vinculoProvisionado;
	
	private Projeto projeto;
	private Boolean projetoAberto;
	
	public AgendamentoFiltro() {
		List<SituacaoAgendamento> listaSituacao = new ArrayList<SituacaoAgendamento>();
		listaSituacao.add(SituacaoAgendamento.A_CONSOLIDAR);
		listaSituacao.add(SituacaoAgendamento.ATENCAO);
		listaSituacao.add(SituacaoAgendamento.ATRASADO);
		
		this.listaSituacao = listaSituacao;
	}
	
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("At�")
	public Date getDtVencimentoFim() {
		return dtVencimentoFim;
	}

	@DisplayName("De")
	public Date getDtVencimentoInicio() {
		return dtVencimentoInicio;
	}

	@DisplayName("Frequ�ncia")
	public Frequencia getFrequencia() {
		return frequencia;
	}
	
	@DisplayName("Id")
	@MaxLength(9)
	public Integer getIdagendamento() {
		return idagendamento;
	}
	
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}

	@DisplayName("Situa��o")
	public List<SituacaoAgendamento> getListaSituacao() {
		return listaSituacao;
	}

	@DisplayName("Opera��o")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}

	@DisplayName("Total cr�dito:")
	public Money getTotalCredito() {
		return totalCredito;
	}

	@DisplayName("Total d�bito:")
	public Money getTotalDebito() {
		return totalDebito;
	}

	@DisplayName("At�")
	public Money getValorFim() {
		return valorFim;
	}

	@DisplayName("De")
	public Money getValorInicio() {
		return valorInicio;
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public String getPessoa() {
		return pessoa;
	}

	@DisplayName("Tipo de pagamento")
	public Tipopagamento getTipopagamento() {
		return tipopagamento;
	}
		
	public Projeto getProjeto() {
		return projeto;
	}

	@DisplayName("Mostrar projetos")
	public Boolean getProjetoAberto() {
		return projetoAberto;
	}
	
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@DisplayName("V�nculo Provisionado")
	public Conta getVinculoProvisionado() {
		return vinculoProvisionado;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setProjetoAberto(Boolean projetoAberto) {
		this.projetoAberto = projetoAberto;
	}

	public void setTipopagamento(Tipopagamento tipopagamento) {
		this.tipopagamento = tipopagamento;
	}
	
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDtVencimentoFim(Date dtVencimentoFim) {
		this.dtVencimentoFim = dtVencimentoFim;
	}

	public void setDtVencimentoInicio(Date dtVencimentoInicio) {
		this.dtVencimentoInicio = dtVencimentoInicio;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setIdagendamento(Integer idagendamento) {
		this.idagendamento = idagendamento;
	}

	public void setListaSituacao(List<SituacaoAgendamento> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}

	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}

	public void setTotalCredito(Money totalCredito) {
		this.totalCredito = totalCredito;
	}

	public void setTotalDebito(Money totalDebito) {
		this.totalDebito = totalDebito;
	}
	
	public void setValorFim(Money valorFim) {
		this.valorFim = valorFim;
	}
	
	public void setValorInicio(Money valorInicio) {
		this.valorInicio = valorInicio;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setVinculoProvisionado(Conta vinculoProvisionado) {
		this.vinculoProvisionado = vinculoProvisionado;
	}
}
