package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraStatusPagamento;
import br.com.linkcom.sined.geral.bean.enumeration.EnumArquivoConciliacaoOperadoraTipoTransacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivoconciliacaooperadoraFiltro extends FiltroListagemSined {
	protected Integer cdarquivoconciliacaooperadora;
	protected Empresa estabelecimento;
	protected Date dataapresentacaoincio;
	protected Date dataapresentacaofim;
	protected String numeroro;
	protected String nsudoc;
	protected String tid;
	protected Double valorvenda;
	protected Date datavendaincio;
	protected Date datavendafim;
	protected Integer totalparcela;
	protected Date dataprevistapagamentoinicio;
	protected Date dataprevistapagamentofim;
	protected EnumArquivoConciliacaoOperadoraStatusPagamento statuspagamento;
	protected Money valortotalvenda;
	protected Money valortotalmovimentacao;
	protected Money valortotalcalculado;
	protected Money valortotalliquido;
	protected EnumArquivoConciliacaoOperadoraTipoTransacao tipotransacao;
	
	public Integer getCdarquivoconciliacaooperadora() {
		return cdarquivoconciliacaooperadora;
	}
	
	@DisplayName("Estabelecimento")
	public Empresa getEstabelecimento() {
		return estabelecimento;
	}
	
	public Date getDataapresentacaoincio() {
		return dataapresentacaoincio;
	}
	
	public Date getDataapresentacaofim() {
		return dataapresentacaofim;
	}
	
	@DisplayName("Numero RO")
	public String getNumeroro() {
		return numeroro;
	}
	
	@DisplayName("NSU/DOC")
	public String getNsudoc() {
		return nsudoc;
	}
	
	@DisplayName("TID")
	public String getTid() {
		return tid;
	}
	
	@DisplayName("Valor da venda")
	public Double getValorvenda() {
		return valorvenda;
	}

	public Date getDatavendaincio() {
		return datavendaincio;
	}

	public Date getDatavendafim() {
		return datavendafim;
	}
	
	@DisplayName("Total de parcelas")
	public Integer getTotalparcela() {
		return totalparcela;
	}
	
	public Date getDataprevistapagamentoinicio() {
		return dataprevistapagamentoinicio;
	}
	
	public Date getDataprevistapagamentofim() {
		return dataprevistapagamentofim;
	}
	
	@DisplayName("Situação")
	public EnumArquivoConciliacaoOperadoraStatusPagamento getStatuspagamento() {
		return statuspagamento;
	}
	
	@DisplayName("Valor total liquido")
	public Money getValortotalliquido() {
		return valortotalliquido;
	}
	
	@DisplayName("Valor total de vendas")
	public Money getValortotalvenda() {
		return valortotalvenda;
	}
	
	@DisplayName("Valor total de movimentações")
	public Money getValortotalmovimentacao() {
		return valortotalmovimentacao;
	}

	@DisplayName("Valor total calculado")
	public Money getValortotalcalculado() {
		return valortotalcalculado;
	}

	@DisplayName("Tipo")
	public EnumArquivoConciliacaoOperadoraTipoTransacao getTipotransacao() {
		return tipotransacao;
	}
	
	public void setCdarquivoconciliacaooperadora(Integer cdarquivoconciliacaooperadora) {
		this.cdarquivoconciliacaooperadora = cdarquivoconciliacaooperadora;
	}

	public void setEstabelecimento(Empresa estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	
	public void setNumeroro(String numeroro) {
		this.numeroro = numeroro;
	}
	
	public void setNsudoc(String nsudoc) {
		this.nsudoc = nsudoc;
	}
	
	public void setTid(String tid) {
		this.tid = tid;
	}
	
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	
	public void setDatavendaincio(Date datavendaincio) {
		this.datavendaincio = datavendaincio;
	}
	
	public void setDatavendafim(Date datavendafim) {
		this.datavendafim = datavendafim;
	}
	
	public void setTotalparcela(Integer totalparcela) {
		this.totalparcela = totalparcela;
	}
	
	public void setDataapresentacaoincio(Date dataapresentacaoincio) {
		this.dataapresentacaoincio = dataapresentacaoincio;
	}
	
	public void setDataapresentacaofim(Date dataapresentacaofim) {
		this.dataapresentacaofim = dataapresentacaofim;
	}
	
	public void setStatuspagamento(
			EnumArquivoConciliacaoOperadoraStatusPagamento statuspagamento) {
		this.statuspagamento = statuspagamento;
	}
	
	public void setValortotalliquido(Money valortotalliquido) {
		this.valortotalliquido = valortotalliquido;
	}
	
	public void setValortotalvenda(Money valortotalvenda) {
		this.valortotalvenda = valortotalvenda;
	}

	public void setValortotalmovimentacao(Money valortotalmovimentacao) {
		this.valortotalmovimentacao = valortotalmovimentacao;
	}

	public void setValortotalcalculado(Money valortotalcalculado) {
		this.valortotalcalculado = valortotalcalculado;
	}

	public void setDataprevistapagamentoinicio(Date dataprevistapagamentoinicio) {
		this.dataprevistapagamentoinicio = dataprevistapagamentoinicio;
	}

	public void setDataprevistapagamentofim(Date dataprevistapagamentofim) {
		this.dataprevistapagamentofim = dataprevistapagamentofim;
	}
	public void setTipotransacao(EnumArquivoConciliacaoOperadoraTipoTransacao tipotransacao) {
		this.tipotransacao = tipotransacao;
	}
}
