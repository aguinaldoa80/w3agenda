package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.FluxocaixaTipo;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FluxocaixaFiltro extends FiltroListagemSined {

	protected String descricao;
	protected Money valorDe;
	protected Money valorAte;
	protected FluxocaixaTipo fluxocaixaTipo;
	protected Tipooperacao tipooperacao;
	
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public Money getValorDe() {
		return valorDe;
	}
	public Money getValorAte() {
		return valorAte;
	}
	@DisplayName("Tipo de fluxo de caixa")
	public FluxocaixaTipo getFluxocaixaTipo() {
		return fluxocaixaTipo;
	}
	@DisplayName("Tipo de opera��o")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValorDe(Money valorDe) {
		this.valorDe = valorDe;
	}
	public void setValorAte(Money valorAte) {
		this.valorAte = valorAte;
	}
	public void setFluxocaixaTipo(FluxocaixaTipo fluxocaixaTipo) {
		this.fluxocaixaTipo = fluxocaixaTipo;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
}
