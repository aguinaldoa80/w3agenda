package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FluxocaixaTipoFiltro extends FiltroListagemSined {

	protected String descricao;
	protected String sigla;
	
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@MaxLength(2)
	public String getSigla() {
		return sigla;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
