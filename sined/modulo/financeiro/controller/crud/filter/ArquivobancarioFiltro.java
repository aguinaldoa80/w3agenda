package br.com.linkcom.sined.modulo.financeiro.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivobancarioFiltro extends FiltroListagemSined {

	protected String arquivoremessa;
	protected String arquivoretorno;
	protected Arquivobancariotipo arquivobancariotipo;
	protected Date dtgeracao1;
	protected Date dtgeracao2;
	protected Date dtprocessa1;
	protected Date dtprocessa2;
	protected List<Arquivobancariosituacao> listaSituacao;
	protected String sequencial;
	
	@MaxLength(50)
	@DisplayName("Arquivo de remessa")
	public String getArquivoremessa() {
		return arquivoremessa;
	}
	@MaxLength(50)
	@DisplayName("Arquivo de retorno correspondente")
	public String getArquivoretorno() {
		return arquivoretorno;
	}
	@DisplayName("Tipo de arquivo")
	public Arquivobancariotipo getArquivobancariotipo() {
		return arquivobancariotipo;
	}

	public Date getDtgeracao1() {
		return dtgeracao1;
	}

	public Date getDtgeracao2() {
		return dtgeracao2;
	}

	public Date getDtprocessa1() {
		return dtprocessa1;
	}

	public Date getDtprocessa2() {
		return dtprocessa2;
	}
	
	@DisplayName("Situa��o")
	public List<Arquivobancariosituacao> getListaSituacao() {
		return listaSituacao;
	}
	@MaxLength(6)
	@DisplayName("Sequencial do Arquivo")
	public String getSequencial() {
		return sequencial;
	}
	
	public void setListaSituacao(List<Arquivobancariosituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	
	
	public void setArquivoremessa(String nome) {
		this.arquivoremessa = nome;
	}
	public void setArquivoretorno(String arquivoretorno) {
		this.arquivoretorno = arquivoretorno;
	}
	public void setArquivobancariotipo(Arquivobancariotipo arquivobancariotipo) {
		this.arquivobancariotipo = arquivobancariotipo;
	}
	public void setDtgeracao1(Date dtgeracao1) {
		this.dtgeracao1 = dtgeracao1;
	}

	public void setDtgeracao2(Date dtgeracao2) {
		this.dtgeracao2 = dtgeracao2;
	}

	public void setDtprocessa1(Date dtprocessa1) {
		this.dtprocessa1 = dtprocessa1;
	}

	public void setDtprocessa2(Date dtprocessa2) {
		this.dtprocessa2 = dtprocessa2;
	}
	
	public void setSequencial(String sequencial) {
		this.sequencial = sequencial;
	}
	
	
	
	
}
