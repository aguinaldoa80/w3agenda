package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.HistoricoEmailCobranca;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;

/**
 * @author Andrey Leonardo
 *
 */
@Entity
@SequenceGenerator(name="sq_emailcobranca", sequenceName="sq_emailcobranca")
public class EmailCobranca {
	private Integer cdemailcobranca;
	private List<EmailCobrancaDocumento> listaEmailCobrancaDocumento;
	private String email;
	private Timestamp dataenvio;
	private Timestamp ultimaverificacao;
	private EmailStatusEnum situacaoemail;
	private Pessoa pessoa;	
	List<HistoricoEmailCobranca> historicoEmailCobranca;
	private Boolean envioAutomatico;
	
	//Transient
	private Contato contato;
	private Boolean alterarCadastro = true;
	
	public EmailCobranca(){}
	
	public EmailCobranca(Integer cdemailcobranca){
		this.setCdemailcobranca(cdemailcobranca);
	}
	
	public EmailCobranca(String email, Integer cdpessoa){
		this.setEmail(email);
		this.pessoa = new Pessoa(cdpessoa);
	}
	
	public EmailCobranca(List<EmailCobrancaDocumento> listaEmailCobrancaDocumento, String email, Integer cdpessoa){
		this.setListaEmailCobrancaDocumento(listaEmailCobrancaDocumento);
		this.setEmail(email);
		this.pessoa = new Pessoa(cdpessoa);
	}
	
	public EmailCobranca(List<EmailCobrancaDocumento> listaEmailCobrancaDocumento, String email, Integer cdpessoa, Boolean envioAutomatico){
		this.setListaEmailCobrancaDocumento(listaEmailCobrancaDocumento);
		this.setEmail(email);
		this.pessoa = new Pessoa(cdpessoa);
		this.envioAutomatico = envioAutomatico;
	}
	
	public EmailCobranca(String email, Integer cdpessoa, Boolean envioAutomatico){
		this.setEmail(email);
		this.pessoa = new Pessoa(cdpessoa);
		this.envioAutomatico = envioAutomatico;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emailcobranca")
	public Integer getCdemailcobranca() {
		return cdemailcobranca;
	}
	public void setCdemailcobranca(Integer cdemailcobranca) {
		this.cdemailcobranca = cdemailcobranca;
	}
	@OneToMany(mappedBy="emailCobranca", fetch=FetchType.LAZY)
	public List<EmailCobrancaDocumento> getListaEmailCobrancaDocumento() {
		return listaEmailCobrancaDocumento;
	}
	
	@OneToMany(mappedBy="emailcobranca", fetch=FetchType.LAZY)
	public List<HistoricoEmailCobranca> getHistoricoEmailCobranca() {
		return historicoEmailCobranca;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setListaEmailCobrancaDocumento(List<EmailCobrancaDocumento> listaEmailCobrancaDocumento) {
		this.listaEmailCobrancaDocumento = listaEmailCobrancaDocumento;
	}
	
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@DisplayName("Data de Envio")
	public Timestamp getDataenvio() {
		return dataenvio;
	}
	public void setDataenvio(Timestamp dataenvio) {
		this.dataenvio = dataenvio;
	}
	@DisplayName("�ltima Verifica��o")
	public Timestamp getUltimaverificacao() {
		return ultimaverificacao;
	}
	public void setUltimaverificacao(Timestamp ultimaverificacao) {
		this.ultimaverificacao = ultimaverificacao;
	}
	@DisplayName("Situa��o do E-mail")
	public EmailStatusEnum getSituacaoemail() {
		return situacaoemail;
	}
	public void setSituacaoemail(EmailStatusEnum situacaoemail) {
		this.situacaoemail = situacaoemail;
	}
	
	public void setHistoricoEmailCobranca(
			List<HistoricoEmailCobranca> historicoEmailCobranca) {
		this.historicoEmailCobranca = historicoEmailCobranca;
	}	

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public Boolean getEnvioAutomatico() {
		return envioAutomatico;
	}
	
	public void setEnvioAutomatico(Boolean envioAutomatico) {
		this.envioAutomatico = envioAutomatico;
	}

	@Transient
	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}
	@Transient
	@DisplayName("Alterar Cadastro")
	public Boolean getAlterarCadastro() {
		return alterarCadastro;
	}

	public void setAlterarCadastro(Boolean alterarCadastro) {
		this.alterarCadastro = alterarCadastro;
	}
	
	@Transient
	@DisplayName("Conta(s) a Receber")
	public String getDocumentos() {
		String documentos = "";
		
		for (EmailCobrancaDocumento emailCobrancaDocumento : listaEmailCobrancaDocumento) {
			documentos += emailCobrancaDocumento.getDocumento().getCddocumento() + " - " + 
					"(R$" + emailCobrancaDocumento.getDocumento().getAux_documento().getValoratual() + ") - " + 
					emailCobrancaDocumento.getDocumento().getDocumentoacao().getNome() + "<br>";
		}
		
		documentos = documentos.substring(0, documentos.length() - 4);
		
		return documentos;
	}
}
