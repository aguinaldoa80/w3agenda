package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Aux_Chequeorigem {

	public final static String VENDA = "Venda";
	public final static String MOVIMENTACAO_FINANCEIRA = "Movimentação Financeira";
	
	protected String tipo;
	protected Integer cdmovimentacao;
	protected Date data;
	protected Integer cdvenda;
	
	protected String datastr;	
	
	public String getTipo() {
		return tipo;
	}
	public Integer getCdmovimentacao() {
		return cdmovimentacao;
	}
	public Date getData() {
		return data;
	}
	public Integer getCdvenda() {
		return cdvenda;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setCdmovimentacao(Integer cdmovimentacao) {
		this.cdmovimentacao = cdmovimentacao;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	
	
	public String getDatastr() {
		String s = "";
		if(this.data != null){
			if(this.cdvenda != null){
				s = "Parcela de ";
			}
			s += new SimpleDateFormat("dd/MM/yyyy").format(this.data);
		}
		return s;
	}
	
	
	
}
