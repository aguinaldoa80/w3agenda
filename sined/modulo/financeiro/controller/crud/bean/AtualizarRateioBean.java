package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Rateio;


public class AtualizarRateioBean {
	protected String selectedItens;
	protected String tipo;
	protected Date dtinicio;
	protected Date dtfim;
	protected List<Rateio> listaRateio;
	
	public String getSelectedItens() {
		return selectedItens;
	}
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Date getDtinicio() {
		return dtinicio;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	public List<Rateio> getListaRateio() {
		return listaRateio;
	}
	public void setListaRateio(List<Rateio> listaRateio) {
		this.listaRateio = listaRateio;
	}
}
