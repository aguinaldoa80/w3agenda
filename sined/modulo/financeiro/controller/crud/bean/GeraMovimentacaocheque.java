package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import java.sql.Timestamp;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cheque;

@DisplayName("Gerar Movimentação Financeira")
public class GeraMovimentacaocheque {

	protected Cheque cheque;
	protected Money valor;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Cheque getCheque() {
		return cheque;
	}
	public Money getValor() {
		return valor;
	}

	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
