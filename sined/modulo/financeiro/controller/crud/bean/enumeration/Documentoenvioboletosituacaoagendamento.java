package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration;

public enum Documentoenvioboletosituacaoagendamento {

	ATIVO(0, "Ativo"),
	CONCLUIDO(1, "Conclu�do"),
	CANCELADO(2, "Cancelado");
	
	private String nome;
	private Integer value;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	
	private Documentoenvioboletosituacaoagendamento(Integer value, String descricao){
		this.nome = descricao;
		this.value = value;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}
