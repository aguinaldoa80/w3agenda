package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ReembolsoEnum {

	GERADOS (0,"Gerados"),
	PENDENTES (1,"Pendentes"),
	SEM_REEMBOLSO (2,"Sem reembolso");
	
	private ReembolsoEnum(Integer valor, String nome) {
		this.valor = valor;
		this.nome = nome;
	}
	
	private String nome;
	private Integer valor;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
	
}
