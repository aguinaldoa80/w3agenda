package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tiporelatorio {

	COMPLETO					("Completo "), 
	SIMPLIFICADO				("Simplificado ")
	;
	
	private Tiporelatorio (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Integer getOrdinal() {
		return this.ordinal();
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
