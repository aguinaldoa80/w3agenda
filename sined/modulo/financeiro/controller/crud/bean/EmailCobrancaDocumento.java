package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.geral.bean.Documento;

@Entity
@SequenceGenerator(name="sq_emailcobrancadocumento", sequenceName="sq_emailcobrancadocumento")
public class EmailCobrancaDocumento {

	private Integer cdEmailCobrancaDocumento;
	private EmailCobranca emailCobranca;
	private Documento documento;
	
	public EmailCobrancaDocumento() {
	}
	
	public EmailCobrancaDocumento(Documento documento) {
		this.documento = documento;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emailcobrancadocumento")
	public Integer getCdEmailCobrancaDocumento() {
		return cdEmailCobrancaDocumento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdemailcobranca")
	public EmailCobranca getEmailCobranca() {
		return emailCobranca;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	
	public void setCdEmailCobrancaDocumento(Integer cdEmailCobrancaDocumento) {
		this.cdEmailCobrancaDocumento = cdEmailCobrancaDocumento;
	}
	
	public void setEmailCobranca(EmailCobranca emailCobranca) {
		this.emailCobranca = emailCobranca;
	}
	
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
}
