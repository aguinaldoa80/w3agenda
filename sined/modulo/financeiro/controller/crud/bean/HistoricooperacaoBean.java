package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import br.com.linkcom.neo.util.Util;

public class HistoricooperacaoBean {
	
	private String numdocumento;
	private String descricao;
	private String pagamentoa;
	private String cliente;
	private String fornecedor;
	private String parcela;
	private String contaorigem;
	private String contadestino;
	private String nomecontaorigem;
	private String nomecontadestino;
	private String data;
	private String cheque;
	private String datavencimento;
	private String databanco;
	private String valorvinculado;
	private String recebimentode;
	private String valor;
	protected String numeronota;
	private String dataentrada;
	private String dataemissao;
	private String numero;
	private String valordocumento;
	private String situacao;
	
	public String getNumdocumento() {
		return numdocumento;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getPagamentoa() {
		return pagamentoa;
	}
	public String getCliente() {
		return cliente;
	}
	public String getFornecedor() {
		return fornecedor;
	}
	public String getParcela() {
		return parcela;
	}
	public String getContaorigem() {
		return contaorigem;
	}
	public String getContadestino() {
		return contadestino;
	}
	public String getNomecontaorigem() {
		return nomecontaorigem;
	}
	public String getNomecontadestino() {
		return nomecontadestino;
	}
	public String getData() {
		return data;
	}
	public String getCheque() {
		return cheque;
	}
	public String getDatavencimento() {
		return datavencimento;
	}
	public String getDatabanco() {
		return databanco;
	}
	public String getValorvinculado() {
		return valorvinculado;
	}
	
	public void setNumdocumento(String numdocumento) {
		this.numdocumento = numdocumento;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setPagamentoa(String pagamentoa) {
		this.pagamentoa = pagamentoa;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setParcela(String parcela) {
		this.parcela = parcela;
	}
	public void setContaorigem(String contaorigem) {
		this.contaorigem = contaorigem;
	}
	public void setContadestino(String contadestino) {
		this.contadestino = contadestino;
	}
	public void setNomecontaorigem(String nomecontaorigem) {
		this.nomecontaorigem = nomecontaorigem;
	}
	public void setNomecontadestino(String nomecontadestino) {
		this.nomecontadestino = nomecontadestino;
	}
	public void setData(String data) {
		this.data = data;
	}
	public void setCheque(String cheque) {
		this.cheque = cheque;
	}
	public void setDatavencimento(String datavencimento) {
		this.datavencimento = datavencimento;
	}
	public void setDatabanco(String databanco) {
		this.databanco = databanco;
	}
	public void setValorvinculado(String valorvinculado) {
		this.valorvinculado = valorvinculado;
	}
	
	public String getNumdocumentoReplace() {
		return numdocumento != null ? numdocumento : "";
	}
	public String getDescricaoReplace() {
		return descricao != null ? descricao : "";
	}
	public String getPagamentoaReplace() {
		return pagamentoa != null ? pagamentoa : "";
	}
	public String getClienteReplace() {
		return cliente != null ? cliente : "";
	}
	public String getFornecedorReplace() {
		return fornecedor != null ? fornecedor : "";
	}
	public String getParcelaReplace() {
		return parcela != null ? parcela : "";
	}
	public String getContaorigemReplace() {
		return contaorigem != null ? contaorigem : "";
	}
	public String getContadestinoReplace() {
		return contadestino != null ? contadestino : "";
	}
	public String getNomecontaorigemReplace() {
		return nomecontaorigem != null ? nomecontaorigem : "";
	}
	public String getNomecontadestinoReplace() {
		return nomecontadestino != null ? nomecontadestino : "";
	}
	public String getDataReplace() {
		return data != null ? data : "";
	}
	public String getChequeReplace() {
		return cheque != null ? cheque : "";
	}
	public String getDatavencimentoReplace() {
		return Util.strings.emptyIfNull(datavencimento);
	}
	public String getDatabancoReplace() {
		return Util.strings.emptyIfNull(databanco);
	}
	public String getValorvinculadoReplace() {
		return Util.strings.emptyIfNull(valorvinculado);
	}
	public String getRecebimentode() {
		return recebimentode;
	}
	public void setRecebimentode(String recebimentode) {
		this.recebimentode = recebimentode;
	}
	
	public String getRecebimentodeReplace() {
		return Util.strings.emptyIfNull(recebimentode);
	}
	public String getValorReplace() {
		return Util.strings.emptyIfNull(valor);
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getValor() {
		return valor;
	}
	public String getNumeronota() {
		return numeronota != null ? numeronota : "";
	}
	public void setNumeronota(String numeronota) {
		this.numeronota = numeronota;
	}
	
	public String getDataentradaReplace() {
		return dataentrada != null ? dataentrada : "";
	}
	
	public String getDataentrada() {
		return dataentrada;
	}
	public void setDataentrada(String dataentrada) {
		this.dataentrada = dataentrada;
	}
	
	public String getDataemissaoReplace() {
		return dataemissao != null ? dataemissao : "";
	}
	
	public String getDataemissao() {
		return dataemissao;
	}
	public void setDataemissao(String dataemissao) {
		this.dataemissao = dataemissao;
	}
	
	public String getNumeroReplace() {
		return numero != null ? numero : "";
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getValordocumentoReplace() {
		return valordocumento != null ? valordocumento : "";
	}
	
	public String getValordocumento() {
		return valordocumento;
	}
	public void setValordocumento(String valordocumento) {
		this.valordocumento = valordocumento;
	}
	
	public String getSituacaoReplace() {
		return situacao != null ? situacao : "";
	}
	
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
}
