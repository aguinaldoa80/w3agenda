package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import br.com.linkcom.neo.types.Money;


public class OrigemBean {

	public static final Integer AGENDAMENTO = 1;
	public static final Integer CONTAPAGAR = 2;
	public static final Integer CONTARECEBER = 3;
	public static final Integer TRANSFERENCIA = 4;
	public static final Integer CADASTRO = 5;
	public static final Integer DEVOLUCAOCHEQUE = 6;
	public static final Integer DESPESAVIAGEMACERTO = 7;
	public static final Integer DESPESAVIAGEMAADIANTAMENTO = 8;
	public static final Integer CONCILIACAOCARTAOCREDITO = 9;
	
	protected Integer typeorigem;
	protected String tipo;
	protected String id;
	protected String descricao;
	protected String valor;
	protected Money valorMoney;
	
	
	public OrigemBean() {}
	public OrigemBean(String descricao) {
		this.descricao = descricao;
		this.typeorigem = CADASTRO;
	}
	
	
	public String getTipo() {
		return tipo;
	}
	public String getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getValor() {
		return valor;
	}
	public Integer getTypeorigem() {
		return typeorigem;
	}
	public void setTypeorigem(Integer typeorigem) {
		this.typeorigem = typeorigem;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public Integer getIdInteger(){
		return Integer.parseInt(id);
	}
	
	public Money getValorMoney(){
		return this.valorMoney;
	}
	public void setValorMoney(Money valorMoney) {
		this.valorMoney = valorMoney;
	}
}
