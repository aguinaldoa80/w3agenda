package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class DadosEstatisticosBean {

	public static String CONTA_PAGAR = "Conta a pagar";
	public static String CONTA_RECEBER = "Conta a receber";
	public static String MOVIMENTACAO_FINANCEIRA = "Movimentação financeira";
	public static String CONTRATO = "Contrato";
	public static String NOTA = "Nota";
	public static String VENDA = "Venda";
	public static String ORDEM_COMPRA = "Ordem de compra";
	public static String ESTOQUE = "Estoque";
	
	protected String dadoEstatistico;
	protected String ultimocadastro;
	protected String ultimabaixa;
	protected String ultimaconciliacao;
	protected Integer qtde1;
	protected Integer qtde2;
	protected Integer qtde3;
	
	protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	public DadosEstatisticosBean(){
	}

	public DadosEstatisticosBean(String dadoEstatistico, Date ultimocadastro, Date ultimabaixa, Integer qtde1, Integer qtde2){
		this.dadoEstatistico = dadoEstatistico;
		this.ultimocadastro = getDataFormatada(ultimocadastro);
		this.ultimabaixa = getDataFormatada(ultimabaixa);
		this.qtde1 = qtde1;
		this.qtde2 = qtde2;
	}
	
	public DadosEstatisticosBean(String dadoEstatistico, Date ultimocadastro, Integer qtde1, Integer qtde2, Date ultimaconciliacao){
		this.dadoEstatistico = dadoEstatistico;
		this.ultimocadastro = getDataFormatada(ultimocadastro);
		this.ultimaconciliacao = getDataFormatada(ultimaconciliacao);
		this.qtde1 = qtde1;
		this.qtde2 = qtde2;
	}
	
	public DadosEstatisticosBean(String dadoEstatistico, Date ultimocadastro, Integer qtde1, Integer qtde2, Integer qtde3){
		this.dadoEstatistico = dadoEstatistico;
		this.ultimocadastro = getDataFormatada(ultimocadastro);
		this.qtde1 = qtde1;
		this.qtde2 = qtde2;
		this.qtde3 = qtde3;
	}
	
	public DadosEstatisticosBean(String dadoEstatistico, Date ultimocadastro, Integer qtde1, Integer qtde2){
		this.dadoEstatistico = dadoEstatistico;
		this.ultimocadastro = getDataFormatada(ultimocadastro);
		this.qtde1 = qtde1;
		this.qtde2 = qtde2;
	}
	
	public DadosEstatisticosBean(String dadoEstatistico, Date ultimocadastro, Integer qtde1){
		this.dadoEstatistico = dadoEstatistico;
		this.ultimocadastro = getDataFormatada(ultimocadastro);
		this.qtde1 = qtde1;
	}
	
	public String getDadoEstatistico() {
		return dadoEstatistico;
	}
	public String getUltimocadastro() {
		return ultimocadastro;
	}
	public String getUltimabaixa() {
		return ultimabaixa;
	}
	
	public Integer getQtde1() {
		return qtde1;
	}

	public Integer getQtde2() {
		return qtde2;
	}

	public Integer getQtde3() {
		return qtde3;
	}

	public void setQtde1(Integer qtde1) {
		this.qtde1 = qtde1;
	}

	public void setQtde2(Integer qtde2) {
		this.qtde2 = qtde2;
	}

	public void setQtde3(Integer qtde3) {
		this.qtde3 = qtde3;
	}
	
	public String getUltimaconciliacao() {
		return ultimaconciliacao;
	}
	
	public void setUltimaconciliacao(String ultimaconciliacao) {
		this.ultimaconciliacao = ultimaconciliacao;
	}

	public void setUltimocadastro(String ultimocadastro) {
		this.ultimocadastro = ultimocadastro;
	}
	public void setUltimabaixa(String ultimabaixa) {
		this.ultimabaixa = ultimabaixa;
	}
	public void setDadoEstatistico(String dadoEstatistico) {
		this.dadoEstatistico = dadoEstatistico;
	}
	
	private String getDataFormatada(Date data) {
		return data != null ? dateFormat.format(data) : null;
	}
	
}
