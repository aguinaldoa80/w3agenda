package br.com.linkcom.sined.modulo.financeiro.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.NotaStatus;

public class AssociarNotaBean {
	
	protected Date dtinicio;
	protected Date dtfim;
	protected List<NotaStatus> listaNotaStatus;
	
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public List<NotaStatus> getListaNotaStatus() {
		return listaNotaStatus;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setListaNotaStatus(List<NotaStatus> listaNotaStatus) {
		this.listaNotaStatus = listaNotaStatus;
	}
}

