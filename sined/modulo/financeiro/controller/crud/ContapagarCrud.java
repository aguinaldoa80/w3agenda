package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Chequehistorico;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcomissao;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.Coletahistorico;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Comissionamentometa;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Veiculodespesa;
import br.com.linkcom.sined.geral.bean.Veiculodespesadefinirprojeto;
import br.com.linkcom.sined.geral.bean.Veiculodespesaitem;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentodesempenhoconsiderar;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemacao;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentocomissaodesempenho;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.CalendarioService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.geral.service.ChequehistoricoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradorcomissaoService;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.ColetahistoricoService;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.DocumentoApropriacaoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoacaoService;
import br.com.linkcom.sined.geral.service.DocumentoclasseService;
import br.com.linkcom.sined.geral.service.DocumentocomissaoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.FornecimentoService;
import br.com.linkcom.sined.geral.service.FrequenciaService;
import br.com.linkcom.sined.geral.service.HistoricoAntecipacaoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.OperacaocontabilService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.OrdemcomprahistoricoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.ProjetotipoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.geral.service.RateiomodeloService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.geral.service.VeiculodespesaService;
import br.com.linkcom.sined.geral.service.VwdocumentocomissaodesempenhoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.AtualizarRateioBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DocumentoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwdocumentocomissaodesempenhoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/financeiro/crud/Contapagar", authorizationModule=CrudAuthorizationModule.class)
public class ContapagarCrud extends CrudControllerSined<DocumentoFiltro, Documento, Documento> {

	public static final String CONTA_PAGAR_EMPRESA = "ContaPagarEmpresa";
	private FrequenciaService frequenciaService;
	private DocumentoService documentoService;
	private DocumentoacaoService documentoacaoService;
	private DocumentohistoricoService documentohistoricoService;
	private AgendamentoService agendamentoService;
	private RateioService rateioService;
	private TaxaService taxaService;
	private CentrocustoService centrocustoService;
	private MovimentacaoService movimentacaoService;
	private PrazopagamentoService prazopagamentoService;
	private FornecimentoService fornecimentoService;
	private EntregaService entregaService;
	private RateioitemService rateioitemService;
	private DespesaviagemService despesaviagemService;
	private DocumentoorigemService documentoorigemService;
	private ContapagarService contapagarService;
	private VeiculodespesaService veiculodespesaService;
	private ProjetoService projetoService;
	private PessoaService pessoaService;
	private DocumentocomissaoService documentocomissaoService;
	private DocumentotipoService documentotipoService;
	private ColaboradorService colaboradorService;
	private ColaboradorcomissaoService colaboradorcomissaoService;
	private VwdocumentocomissaodesempenhoService vwdocumentocomissaodesempenhoService;
	private ComissionamentoService comissionamentoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ChequeService chequeService;
	private ChequehistoricoService chequehistoricoService; 
	private EmpresaService empresaService;
	private ContagerencialService contagerencialService;
	private MaterialService materialService;
	private EntradafiscalService entradafiscalService;
	private OperacaocontabilService operacaocontabilService;
	private ParametrogeralService parametrogeralService;
	private OrdemcompraService ordemcompraService;
	private OrdemcomprahistoricoService ordemcomprahistoricoService;
	private ContaService contaService;
	private ColetaService coletaService;
	private ProjetotipoService projetotipoService;
	private ColetahistoricoService coletahistoricoService;
	private MunicipioService municipioService;
	private RateiomodeloService rateiomodeloService;
	private ReportTemplateService reporttemplateService;
	private CalendarioService calendarioService;
	private HistoricoAntecipacaoService historicoAntecipacaoService;
	private DocumentoApropriacaoService documentoApropriacaoService;
	private AvisoService avisoService;
	
	public void setColetaService(ColetaService coletaService) {this.coletaService = coletaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {this.despesaviagemService = despesaviagemService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setEntregaService(EntregaService entregaService) {this.entregaService = entregaService;}
	public void setFornecimentoService(FornecimentoService fornecimentoService) {this.fornecimentoService = fornecimentoService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setFrequenciaService(FrequenciaService frequenciaService) {this.frequenciaService = frequenciaService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setDocumentoacaoService(DocumentoacaoService documentoacaoService) {this.documentoacaoService = documentoacaoService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setAgendamentoService(AgendamentoService agendamentoService) {this.agendamentoService = agendamentoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setJurosService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	public void setVeiculodespesaService(VeiculodespesaService veiculodespesaService) {this.veiculodespesaService = veiculodespesaService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {this.documentocomissaoService = documentocomissaoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setColaboradorcomissaoService(ColaboradorcomissaoService colaboradorcomissaoService) {this.colaboradorcomissaoService = colaboradorcomissaoService;}
	public void setVwdocumentocomissaodesempenhoService(VwdocumentocomissaodesempenhoService vwdocumentocomissaodesempenhoService) {		this.vwdocumentocomissaodesempenhoService = vwdocumentocomissaodesempenhoService;	}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {this.comissionamentoService = comissionamentoService;}
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {	this.fechamentofinanceiroService = fechamentofinanceiroService;	}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	public void setChequehistoricoService(ChequehistoricoService chequehistoricoService) {this.chequehistoricoService = chequehistoricoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setOperacaocontabilService(OperacaocontabilService operacaocontabilService) {this.operacaocontabilService = operacaocontabilService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {this.ordemcompraService = ordemcompraService;}
	public void setOrdemcomprahistoricoService(OrdemcomprahistoricoService ordemcomprahistoricoService) {	this.ordemcomprahistoricoService = ordemcomprahistoricoService;	}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setProjetotipoService(ProjetotipoService projetotipoService) {this.projetotipoService = projetotipoService;	}
	public void setColetahistoricoService(ColetahistoricoService coletahistoricoService) {this.coletahistoricoService = coletahistoricoService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setRateiomodeloService(RateiomodeloService rateiomodeloService) {this.rateiomodeloService = rateiomodeloService;}
	public void setReporttemplateService(ReportTemplateService reporttemplateService) {this.reporttemplateService = reporttemplateService;}
	public void setCalendarioService(CalendarioService calendarioService) {this.calendarioService = calendarioService;}
	public void setTaxaService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setHistoricoAntecipacaoService(HistoricoAntecipacaoService historicoAntecipacaoService) {this.historicoAntecipacaoService = historicoAntecipacaoService;}
	public void setDocumentoApropriacaoService(DocumentoApropriacaoService documentoApropriacaoService) {this.documentoApropriacaoService = documentoApropriacaoService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected void listagem(WebRequestContext request, DocumentoFiltro filtro) throws Exception {
		if (filtro ==  null)
			throw new SinedException("Dados inv�lidos");
		
		if (!filtro.getOrderBy().isEmpty()){
			WebRequestContext context = NeoWeb.getRequestContext();
			String isAsc = "";
			if (filtro.getOrderBy() != null && !filtro.getOrderBy().isEmpty())
			isAsc = filtro.isAsc() ? " asc " : " desc ";
			context.getSession().setAttribute("orderBy", filtro.getOrderBy() + isAsc);
		}	
		request.setAttribute("listaAcaoCompleta", documentoacaoService.findForContapagar());
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
		request.setAttribute("listaTipoProjeto", projetotipoService.findAll());
		request.setAttribute("listaVinculoProvisionado", contaService.findVinculos());
		request.setAttribute("listaDocumentoTipo", documentotipoService.getListaDocumentoTipoUsuarioForCompra(null));
		
		request.setAttribute("listaTipopagamento", documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_DEBITO));
		request.setAttribute(Parametrogeral.LISTAR_RAZAOSOCIAL_E_NOMEFANTASIA, parametrogeralService.getBoolean(Parametrogeral.LISTAR_RAZAOSOCIAL_E_NOMEFANTASIA));

		List<Documentoacao> listaAcoes = new ArrayList<Documentoacao>();
		listaAcoes.add(Documentoacao.ATRASADA);
		listaAcoes.add(Documentoacao.PROTESTADA);
		listaAcoes.add(Documentoacao.REAGENDAMENTO);
		request.setAttribute("listaAcao", listaAcoes);
		
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, DocumentoFiltro filtro) throws CrudException {
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<Documento> getLista(WebRequestContext request, DocumentoFiltro filtro) {
		filtro.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		
		ListagemResult<Documento> listResult = super.getLista(request, filtro);
		List<Documento> lista = listResult.list();
		
		filtro.setTotalValorContaGerencial(new Money());
		filtro.setTotalValorCentroCusto(new Money());
		filtro.setTotalValorProjeto(new Money());
		filtro.setTotal(new Money());
		filtro.setTotalOriginal(new Money());
		filtro.setEmAberto(new Money());
		filtro.setEmAtraso(new Money());
		
		if(lista != null && lista.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cddocumento", ",");
			
			lista.removeAll(lista);
			lista.addAll(documentoService.loadForListagem(whereIn, filtro, filtro.getOrderBy(), filtro.isAsc()));
			documentoService.ajustaValoresListagem(filtro, lista);
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			for (Documento doc : lista) {
				documentoService.ajustaNumeroParcelaDocumento(doc);
				
				if(doc.getDocumentoacao().equals(Documentoacao.BAIXADA)){
					Movimentacao mov = movimentacaoService.findByDocumento(doc);
					if(mov != null && mov.getDataMovimentacao() != null)
						doc.setStrDtbaixa(" - " + simpleDateFormat.format(mov.getDataMovimentacao()));
					else
						doc.setStrDtbaixa("");
				} else {
					doc.setStrDtbaixa("");
				}
				
				if(BooleanUtils.isTrue(doc.getDocumentotipo().getAntecipacao()) && doc.getDocumentoacao().equals(Documentoacao.BAIXADA)) {
					documentoService.verificarSaldoAdiantamentoPorDocumento(doc, Documentoclasse.OBJ_PAGAR);
				}
				
                if(doc.getPossuiSaldoNaoCompensado() == null && documentoService.possuiContasComSaldoNaoCompensado(doc)) {
					// atualizar contas com a mesma pessoa e tipopagamento para evitar repetir a valida��o
					for(Documento aux : lista) {
						if(doc.getRecebimentode() != null && doc.getRecebimentode().equals(aux.getRecebimentode()) && doc.getTipopagamento() != null && doc.getTipopagamento().equals(aux.getTipopagamento())) {
							aux.setPossuiSaldoNaoCompensado(true);
						}
					}
				}
			}
			
			documentoService.setDatasBaixaContaListagem(filtro, lista, whereIn);
		}
		
		return listResult;
	}

	/**
	 * Cria um conta a pagar quando esta trata-se de pagamento de comiss�o.
	 * @param request
	 * @param form
	 * @return
	 * @throws Exception
	 * @author Taidson
	 * @since 05/11/2010
	 */
	public ModelAndView criarPagamentoComissao(WebRequestContext request, Documento form) throws Exception{
		String whereIn = request.getParameter("selectedItens");
		
		List<Documentocomissao> listaDocumentocomissao = documentocomissaoService.findByPagamento(whereIn);
		
		if(!existeSomenteumcolaborador(listaDocumentocomissao)){
			request.addError("S� � permitido gerar o pagamento da comiss�o para um colaborador por vez.");
			SinedUtil.redirecionamento(request, "/rh/crud/Documentocomissao");
			return null;
		}
		Documentocomissao dc;
		documentocomissaoService.ajustaDadosDasNotas(listaDocumentocomissao);
		
		Money valor = new Money(0);
		if(listaDocumentocomissao != null && listaDocumentocomissao.size() > 0){
			for (Documentocomissao item : listaDocumentocomissao) {
				dc = documentocomissaoService.findByPagamento(item.getContrato());
				if(item.getDocumento() != null && item.getDocumento().getCddocumento() != null && item.getComissionamento() != null &&
						item.getComissionamento().getDeduzirvalor() != null && item.getComissionamento().getDeduzirvalor()){
					if(item.getPessoa().equals(dc.getContrato().getVendedor())){						
						if(dc.getContrato().getVendedortipo().equals(Vendedortipo.COLABORADOR)){							
							form.setColaborador(new Colaborador(item.getPessoa().getCdpessoa()));
						}else if(dc.getContrato().getVendedortipo().equals(Vendedortipo.FORNECEDOR)){
							form.setFornecedor(new Fornecedor(item.getPessoa().getCdpessoa()));
							form.setTipopagamento(Tipopagamento.FORNECEDOR);
						}
					}else{
						form.setColaborador(new Colaborador(item.getPessoa().getCdpessoa()));
					}
					
					if(item.getValorcomissao() != null && item.getValorcomissao().getValue().doubleValue() != 0)
						valor = valor.add(item.getValorcomissao());
					else{
						if(dc != null && dc.getContrato() != null && dc.getContrato().getListaContratocolaborador() != null)
							valor = valor.add(contapagarService.calculaComissao(dc.getContrato().getListaContratocolaborador(), item.getPessoa(), item.getDocumento().getAux_documento().getValoratual(), dc.getContrato()));
					}
				}else{
					if(item.getPessoa().equals(dc.getContrato().getVendedor())){						
						if(dc.getContrato().getVendedortipo().equals(Vendedortipo.COLABORADOR)){							
							form.setColaborador(new Colaborador(item.getPessoa().getCdpessoa()));
						}else if(dc.getContrato().getVendedortipo().equals(Vendedortipo.FORNECEDOR)){
							form.setFornecedor(new Fornecedor(item.getPessoa().getCdpessoa()));
							form.setTipopagamento(Tipopagamento.FORNECEDOR);
						}
					}else{
						form.setColaborador(new Colaborador(item.getPessoa().getCdpessoa()));
					}
					if(item.getValorcomissao() != null && item.getValorcomissao().getValue().doubleValue() != 0)
						valor = valor.add(item.getValorcomissao());
					else{
						valor = new Money(item.getComissao().add(valor));
					}
					
				}
				form.setPessoa(item.getPessoa());
				form.setEmpresa(item.getContrato().getEmpresa());
			}
		}
		form.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		form.setListaDocumentocomissao(listaDocumentocomissao);
		form.setFromPagamentoComissao(true);
		form.setDtemissao(SinedDateUtils.currentDate());
		form.setDtvencimento(SinedDateUtils.currentDate());
		form.setValor(valor);		
		if(form.getTipopagamento() == null)
			form.setTipopagamento(Tipopagamento.COLABORADOR);
		form.setDescricao("Repasse Comissionamento");
		
		setInfoForTemplate(request, form);
		setEntradaDefaultInfo(request, form);
		entrada(request, form);
		return getEntradaModelAndView(request, form);
	}
	
	/**
	 * M�todo que verifica se existe mais de uma pessoa na gera��o do pagamento da comiss�o
	 *
	 * @param listaDocumentocomissao
	 * @return
	 * @author Luiz Fernando
	 */
	private boolean existeSomenteumcolaborador(List<Documentocomissao> listaDocumentocomissao) {
		Boolean existe = Boolean.TRUE;
		Pessoa pessoa = null;
		if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
			for(Documentocomissao documentocomissao : listaDocumentocomissao){
				if(pessoa != null && documentocomissao.getPessoa() != null && !pessoa.equals(documentocomissao.getPessoa())){
					existe = Boolean.FALSE;
					break;
				}else {
					pessoa = documentocomissao.getPessoa();
				}
			}
		}
		
		return existe;
	}
	
	public ModelAndView criarPagamentoComissaovenda(WebRequestContext request, Documento form) throws Exception{
		String whereIn = request.getParameter("selectedItens");
		
		List<Documentocomissao> listaDocumentocomissao = documentocomissaoService.findByPagamentoVenda(whereIn);
		
		if(listaDocumentocomissao == null || listaDocumentocomissao.size() == 0){
			request.addError("Nenhuma comiss�o selecionada.");
			return new ModelAndView("redirect:/rh/crud/Documentocomissaovenda");
		}
		
		Integer cdpessoa = null;
		for (Documentocomissao item : listaDocumentocomissao) {
			if(item.getPessoa() != null && item.getPessoa().getCdpessoa() != null){
				if(cdpessoa == null){
					cdpessoa = item.getPessoa().getCdpessoa();
				} else if(!item.getPessoa().getCdpessoa().equals(cdpessoa)){
					request.addError("N�o se pode gerar pagamento de mais de um comissionamento que tenha o colaborador/vendedor diferentes.");
					return new ModelAndView("redirect:/rh/crud/Documentocomissaovenda");
				}
			}
		}
		
		StringBuilder whereInVenda = new StringBuilder();
		StringBuilder whereInPedidovenda = new StringBuilder();
		Money valor = new Money(0);
		
		for (Documentocomissao item : listaDocumentocomissao) {		
			if(item.getIndicacao() != null && item.getIndicacao()){
				form.setCliente(new Cliente(item.getPessoa().getCdpessoa()));
				form.setTipopagamento(Tipopagamento.CLIENTE);
			}else if(item.getAgencia() != null && item.getAgencia()){
				form.setFornecedor(new Fornecedor(item.getPessoa().getCdpessoa()));
				form.setTipopagamento(Tipopagamento.FORNECEDOR);
			}else{
				form.setColaborador(new Colaborador(item.getPessoa().getCdpessoa()));
			}
			
			form.setPessoa(item.getPessoa());
			
			if (item.getPedidovenda() != null) {
				form.setEmpresa(item.getPedidovenda().getEmpresa());
			} else {
				form.setEmpresa(item.getVenda().getEmpresa());
			}
			
			valor = valor.add(item.getValorcomissao());

			if (item.getPedidovenda() != null && item.getPedidovenda().getCdpedidovenda() != null) {
				if (!whereInPedidovenda.toString().contains(item.getPedidovenda().getCdpedidovenda().toString())) {
					if (!whereInPedidovenda.toString().equals("")) {
						whereInPedidovenda.append(",");
					}
					
					whereInPedidovenda.append(item.getPedidovenda().getCdpedidovenda());
				}
			} else {
				if (item.getVenda() != null && item.getVenda().getCdvenda() != null) {
					if (!whereInVenda.toString().contains(item.getVenda().getCdvenda().toString())) {
						if (!whereInVenda.toString().equals("")) {
							whereInVenda.append(",");
						}
						
						whereInVenda.append(item.getVenda().getCdvenda());
					}
				} 
			}
		}
		
		String isComissionamentoPorFaixas = request.getParameter("porFaixas");
		if(StringUtils.isNotBlank(isComissionamentoPorFaixas) && isComissionamentoPorFaixas.equals("true")) {
			form.setFromPagamentoComissaoPorFaixas(true);
		} else {
			form.setFromPagamentoComissaoVenda(true);
		}
		
		form.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		form.setListaDocumentocomissao(listaDocumentocomissao);
		form.setDtemissao(SinedDateUtils.currentDate());
		form.setDtvencimento(SinedDateUtils.currentDate());
		form.setValor(valor);	
		if(form.getTipopagamento() == null)
			form.setTipopagamento(Tipopagamento.COLABORADOR);
		form.setDescricao("Repasse Comissionamento Venda (Produto)");
		
		if(whereInPedidovenda != null && !whereInPedidovenda.toString().equals("") && whereInPedidovenda.length() < 80){
			form.setDescricao(form.getDescricao() + " - Pedidos de venda: " + whereInPedidovenda.toString());
		}
		
		if(whereInVenda != null && !whereInVenda.toString().equals("") && whereInVenda.length() < 80){
			form.setDescricao(form.getDescricao() + " - Vendas: " + whereInVenda.toString());
		}
		
		setInfoForTemplate(request, form);
		setEntradaDefaultInfo(request, form);
		entrada(request, form);
		return getEntradaModelAndView(request, form);
	}
	
	public ModelAndView criarPagamentoComissaodesempenho(WebRequestContext request, Documento form) throws Exception{
		String dtiniciostr = request.getParameter("filtrodtinicio");
		String dtfimstr = request.getParameter("filtrodtfim");
		String cdpessoastr = request.getParameter("filtropessoa");
		String valorcomissaostr = request.getParameter("filtrovalorcomissao");
		String mostrar = request.getParameter("mostrar");
		String considerar = request.getParameter("considerar");
		String documentocomissaotipoperiodo = request.getParameter("documentocomissaotipoperiodo");
		String considerardtvenda = request.getParameter("considerardtvenda");
		
		if(dtiniciostr != null && !"".equals(dtiniciostr) && dtfimstr != null && !"".equals(dtfimstr) 
				&& cdpessoastr != null && !"".equals(cdpessoastr) && valorcomissaostr != null &&
				!"".equals(valorcomissaostr) && mostrar != null && !"".equals(mostrar)){
			
			VwdocumentocomissaodesempenhoFiltro filtro = new VwdocumentocomissaodesempenhoFiltro();
			filtro.setDtinicio(SinedDateUtils.stringToDate(dtiniciostr));
			filtro.setDtfim(SinedDateUtils.stringToDate(dtfimstr));
			filtro.setColaborador(colaboradorService.load(new Colaborador(Integer.parseInt(cdpessoastr)), "colaborador.cdpessoa, colaborador.nome"));
			if(mostrar.equals("Pagos com pendencia de repasse")){
				filtro.setMostrar("Pagos com pend�ncia de repasse");
			}else {
				filtro.setMostrar("Todos");
			}
			if(considerar != null && Comissionamentodesempenhoconsiderar.SALDO_VENDA.getValue().toString().equals(considerar)){
				filtro.setConsiderar(Comissionamentodesempenhoconsiderar.SALDO_VENDA);
			}else if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.MARKUP_VENDA.getValue().toString().equals(considerar)){
				filtro.setConsiderar(Comissionamentodesempenhoconsiderar.MARKUP_VENDA);
			}else if(filtro.getConsiderar() != null && Comissionamentodesempenhoconsiderar.TOTAL_VENDA.getValue().toString().equals(considerar)){
				filtro.setConsiderar(Comissionamentodesempenhoconsiderar.TOTAL_VENDA);
			}else {
				filtro.setConsiderar(Comissionamentodesempenhoconsiderar.TOTAL_CONTARECEBER);
			}
			if(documentocomissaotipoperiodo != null){
				if(Documentocomissaotipoperiodo.EMISSAO.getValue().toString().equals(documentocomissaotipoperiodo)){
					filtro.setDocumentocomissaotipoperiodo(Documentocomissaotipoperiodo.EMISSAO);
				}else if(Documentocomissaotipoperiodo.VENCIMENTO.getValue().toString().equals(documentocomissaotipoperiodo)){
					filtro.setDocumentocomissaotipoperiodo(Documentocomissaotipoperiodo.VENCIMENTO);
				}else if(Documentocomissaotipoperiodo.PAGAMENTO.getValue().toString().equals(documentocomissaotipoperiodo)){
					filtro.setDocumentocomissaotipoperiodo(Documentocomissaotipoperiodo.PAGAMENTO);
				}
			}
			if(StringUtils.isNotEmpty(considerardtvenda) && "true".equalsIgnoreCase(considerardtvenda)){
				filtro.setConsiderardtvenda(true);
			}
			
			String whereInVenda = null;
			Money totalvenda = new Money();
			Money totalarepassar = new Money();
			if(Comissionamentodesempenhoconsiderar.TOTAL_VENDA.equals(filtro.getConsiderar())){
				List<Vwdocumentocomissaodesempenho> listaVwdocumentocomissaodesempenho = vwdocumentocomissaodesempenhoService.findForContapagar(filtro);
				vwdocumentocomissaodesempenhoService.addVendaForTotal(filtro, listaVwdocumentocomissaodesempenho);
				whereInVenda = CollectionsUtil.listAndConcatenate(listaVwdocumentocomissaodesempenho, "venda.cdvenda", ",");
				totalvenda = filtro.getTotalvenda();
				totalarepassar = filtro.getTotalarepassar();
			}else {
			 totalvenda = vwdocumentocomissaodesempenhoService.findForTotalcomissionamentodesempenho(filtro);
			 totalarepassar = vwdocumentocomissaodesempenhoService.findForTotalarepassarcomissionamentodesempenho(filtro);
			}
			List<Vwdocumentocomissaodesempenho> lista = vwdocumentocomissaodesempenhoService.findForCalcularValorComissao(filtro);
			Double percentualcomissao = 0.0;
			Double valorcomissao = 0.0;
			Double valorcomissao_aux = 0d;
			Double totalmetavenda_aux  = 0d;
			Comissionamento comissionamento = null;
			if(lista != null && !lista.isEmpty() && lista.get(0).getCdcomissionamento() != null)
				comissionamento = comissionamentoService.findForCalcularmeta(new Comissionamento(lista.get(0).getCdcomissionamento()));
			
			if(comissionamento != null && comissionamento.getListaComissionamentometa() != null &&
					!comissionamento.getListaComissionamentometa().isEmpty()){
				Boolean acumulativo = comissionamento.getDesempenhocumulativo();
				Boolean diferencaentremetas = comissionamento.getConsiderardiferencaentremetas();
				
				if(acumulativo != null && acumulativo && diferencaentremetas != null && diferencaentremetas){
					HashMap<Integer, Comissionamentometa> mapPosicaoValor = new HashMap<Integer, Comissionamentometa>();
					//ordem deve ser por maior valor
					Collections.sort(comissionamento.getListaComissionamentometa(),new Comparator<Comissionamentometa>(){
						public int compare(Comissionamentometa o1, Comissionamentometa o2) {
							try {
								return o1.getMetavenda().compareTo(o2.getMetavenda());
							}catch (Exception e) {return 0;}
						}
					});
					
					int posicao = 0;
					for(int i = 0; i < comissionamento.getListaComissionamentometa().size(); i++){
						Comissionamentometa cm = comissionamento.getListaComissionamentometa().get(i);
						if(totalvenda != null && cm.getMetavenda() != null &&
								totalvenda.getValue().doubleValue() >= (cm.getMetavenda().getValue().doubleValue() + totalmetavenda_aux)){
							totalmetavenda_aux += cm.getMetavenda().getValue().doubleValue();
							mapPosicaoValor.put(posicao, cm);
							posicao++;
						}
					}
					if(mapPosicaoValor.size() > 0){
						totalmetavenda_aux = totalvenda.getValue().doubleValue();
						for(int i = mapPosicaoValor.size()-1; i >= 0; i--){
							Comissionamentometa cm = mapPosicaoValor.get(i);
							if(cm != null && cm.getMetavenda() != null){
								if(i == 0){
									valorcomissao_aux += totalmetavenda_aux * cm.getComissao();
								}else {
									valorcomissao_aux += cm.getMetavenda().getValue().doubleValue() * cm.getComissao();
									totalmetavenda_aux -= cm.getMetavenda().getValue().doubleValue();
								}
							}
						}
						
						if(valorcomissao_aux != null && valorcomissao_aux != 0){
							valorcomissao = valorcomissao_aux;
						}
					}
				}else {
					for(Comissionamentometa cm : comissionamento.getListaComissionamentometa()){
						if(totalvenda != null && cm.getMetavenda() != null &&
							totalvenda.getValue().doubleValue() >= cm.getMetavenda().getValue().doubleValue()){
								if(acumulativo != null && acumulativo){
									percentualcomissao += cm.getComissao();
								} else {
									percentualcomissao = cm.getComissao();
								}
						}
					}
					if(percentualcomissao != null && percentualcomissao > 0){
						valorcomissao = totalarepassar.getValue().doubleValue() * percentualcomissao;
					}
				}
				
				if(valorcomissao != null && valorcomissao != 0){
					valorcomissao = valorcomissao/100d;
					filtro.setValorcomissao(new Money(valorcomissao));
				}
			}
			
			
			String whereInDocumentoorigem = null; 
			String whereInNotadocumento = null;
			if(!Comissionamentodesempenhoconsiderar.TOTAL_VENDA.equals(filtro.getConsiderar())){
				whereInDocumentoorigem = vwdocumentocomissaodesempenhoService.findDocumentoorigem(filtro);
				whereInNotadocumento = vwdocumentocomissaodesempenhoService.findNotadocumento(filtro);
			}
			form.setColaborador(filtro.getColaborador());
			form.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
			form.setDtemissao(SinedDateUtils.currentDate());
			form.setDtvencimento(SinedDateUtils.currentDate());
			form.setValor(filtro.getValorcomissao());	
			form.setTipopagamento(Tipopagamento.COLABORADOR);
			form.setDescricao("Repasse Comissionamento Desempenho");
			form.setFromPagamentoComissaoDesempenho(Boolean.TRUE);
			form.setWhereInDocumentoorigemDesempenho(whereInDocumentoorigem);
			form.setWhereInNotadocumentoDesempenho(whereInNotadocumento);
			form.setWhereInVendaDesempenho(whereInVenda);
			
			setInfoForTemplate(request, form);
			setEntradaDefaultInfo(request, form);
			entrada(request, form);
			
		}else {
			request.addError("N�o foi poss�vel gerar o pagamento de comiss�o por desempenho. Par�metros inv�lidos.");
			SinedUtil.redirecionamento(request, "/rh/crud/Documentocomissaodesempenho");
			return null;
		}
		
		return getEntradaModelAndView(request, form);
	}
	
	public ModelAndView criarFaturamentoEntrega(WebRequestContext request, Documento form) throws Exception{
		String entregasSelecionadas = request.getParameter("selectedEntregasFaturar");
		
		if(entregasSelecionadas == null || "".equals(entregasSelecionadas)){
			request.addError("N�o foi poss�vel gerar a conta a pagar. Par�metros inv�lidos.");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Entrega");
			return null;
		}
		boolean entregaUnica = "true".equals(request.getParameter("entregaUnica"));

		if(entregaUnica){
			form = entregaService.criaFaturaEntregaUnica(entregasSelecionadas, form.getDocumentotipo(), null);
			form.setWhereInEntrega(entregasSelecionadas);
		}else{
		
			Double valorTotalEntrega = null;
			Set<Entregapagamento> listaEntregaPagamento = null;
			Documento documento = new Documento();
			Rateio rateio = new Rateio();
			
			List<Documentoorigem> listaDocumentoorigemAntecipacao = new ArrayList<Documentoorigem>();
			Documentoorigem documentoorigem;
			Documentotipo documentotipo = null;
			boolean existeDocumentotipoDiferente = false;
			
			Conta vinculoProvisionado = null;
			if(form.getDocumentotipo() != null && form.getDocumentotipo().getCddocumentotipo() != null){
				Documentotipo docTipo = documentotipoService.loadWithContadestino(form.getDocumentotipo());
				if(docTipo != null && docTipo.getContadestino() != null){
					vinculoProvisionado = docTipo.getContadestino();
				}
			}
			
			List<Entrega> listaEntrega = entregaService.listEntregaForFaturar(entregasSelecionadas);			
			int tam = listaEntrega.size();
			int i = 1;
			valorTotalEntrega = 0.0;
			
			for(Entrega e: listaEntrega){
				for(Entregadocumento ed : e.getListaEntregadocumento()){
					if(ed.getDocumentotipo() != null){
						if(documentotipo == null){
							documentotipo = ed.getDocumentotipo();
						}else if(!documentotipo.equals(ed.getDocumentotipo())){
							existeDocumentotipoDiferente = true;
						}
					}
				}
			}
			for(Entrega entrega: listaEntrega){				
				if( i == tam){
					valorTotalEntrega += entrega.getValorTotalPagamento().getValue().doubleValue();
					documento.setEmpresa(entrega.getEmpresa());
					documento.setNumero(null);
					if(form.getDocumentotipo() != null){
						documento.setDocumentotipo(form.getDocumentotipo());
						if(documento.getVinculoProvisionado() == null){
							documento.setVinculoProvisionado(vinculoProvisionado);
						}
					}else if(!existeDocumentotipoDiferente){
						documento.setDocumentotipo(documentotipo);
						if(documento.getVinculoProvisionado() == null){
							documento.setVinculoProvisionado(documentotipo.getContadestino());
						}
					}
					documento.setDescricao("Conta a pagar referente as entregas: "+ entregasSelecionadas);
					documento.setDtemissao(new Date(System.currentTimeMillis()));
					documento.setDtvencimento(entregaService.getDtvencimentoMenor(listaEntrega));
					documento.setValor(new Money(valorTotalEntrega));
					documentoService.defineDocumentoacaoParametroGeral(documento, Documentoacao.PREVISTA, parametrogeralService.getBoolean(Parametrogeral.NOTACOMPRA_GERA_CONTADEFINITIVA));
					documento.setReferencia(Referencia.MES_ANO_CORRENTE);
					
					documento.setTipopagamento(Tipopagamento.FORNECEDOR);
					documento.setPrazo(null);
					documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
					if(documento.getEmpresa() == null){
						if(entrega.getEmpresa() != null && entrega.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(entrega.getEmpresa());
						} else if(entrega.getHaveOrdemcompra()){
							Set<Ordemcompraentrega> listaOrdemcompraentrega = entrega.getListaOrdemcompraentrega();
							for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
								if(ordemcompraentrega.getOrdemcompra() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa().getCdpessoa() != null){
									documento.setEmpresa(ordemcompraentrega.getOrdemcompra().getEmpresa());
									break;
								}
							}
						}
					}
				}
				else{
					valorTotalEntrega += entrega.getValorTotalPagamento().getValue().doubleValue();
					if(documento.getEmpresa() == null){
						if(entrega.getEmpresa() != null && entrega.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(entrega.getEmpresa());
						} else if(entrega.getHaveOrdemcompra()){
							Set<Ordemcompraentrega> listaOrdemcompraentrega = entrega.getListaOrdemcompraentrega();
							for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
								if(ordemcompraentrega.getOrdemcompra() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa() != null && 
										ordemcompraentrega.getOrdemcompra().getEmpresa().getCdpessoa() != null){
									documento.setEmpresa(ordemcompraentrega.getOrdemcompra().getEmpresa());
									break;
								}
							}
						}
					}
				}
				i++;
				
			}			
						
			Entregapagamento entregapagamentoaux = new Entregapagamento();
			Set<Entregamaterial> listaEntregaMaterialaux = new ListSet<Entregamaterial>(Entregamaterial.class);
			double valorTotalEntregapagamento = 0.0;
			
			boolean existDocumento = false;
			boolean existDocumentoAntecipacao = false;
			for(Entrega e: listaEntrega){
				for(Entregadocumento ed : e.getListaEntregadocumento()){
					if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
						if(ed.getEmpresa() != null && ed.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(ed.getEmpresa());
						}
						documento.setFornecedor(ed.getFornecedor());
						documento.setPessoa(ed.getFornecedor());
						documento.setDescricao(documento.getDescricao() + " - NF " + ed.getNumero());
						listaEntregaPagamento = ed.getListadocumento();	
						for(Entregapagamento entregapagamento : listaEntregaPagamento){
							if(entregapagamento.getDocumentoantecipacao() == null){
								valorTotalEntregapagamento += entregapagamento.getValor().getValue().doubleValue();
								existDocumento = true;
							}else {
								documentoorigem = new Documentoorigem();
								documentoorigem.setEntrega(e);
								documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
								if(!entregaService.existeDocumentoantecipacaoEntrega(documentoorigem, listaDocumentoorigemAntecipacao)){
									listaDocumentoorigemAntecipacao.add(documentoorigem);
								}
								existDocumentoAntecipacao = true;
							}
						}
						
						for(Entregamaterial entregamaterial : ed.getListaEntregamaterial()){
							listaEntregaMaterialaux.add(entregamaterial);
						}
					}
				}
				
			}
			
			if(existDocumento){
				if(existDocumentoAntecipacao){
					documento.setValor(new Money(valorTotalEntregapagamento));
				}
				entregapagamentoaux.setValor(new Money(valorTotalEntregapagamento));
				List<Rateioitem> listaRateioitem = entregaService.calculaRateioVariasEntregas(listaEntrega);
				rateioitemService.agruparItensRateio(listaRateioitem);
				rateio.setListaRateioitem(listaRateioitem);
				if(rateio != null)
					rateioService.limpaReferenciaRateio(rateio);
				documento.setRateio(rateio);						
				documento.setWhereInEntrega(entregasSelecionadas);
			}
			
			form = documento;
		}
		request.setAttribute("fromEntrega", true);
		setInfoForTemplate(request, form);
		setEntradaDefaultInfo(request, form);
		entrada(request, form);
		return getEntradaModelAndView(request, form);
	}
	
	
	/**
	 * Redireciona para listagem de controle de comissionamento ap�s salvar caso
	 * a conta � pagar estiver relacionada � pagamento de comiss�o.
	 * @author Taidson
	 * @since 05/11/2010
	 */
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Documento bean) {
		if ("true".equals(request.getParameter("criarNovoAposSalvar"))){
			return new ModelAndView("redirect:/financeiro/crud/Contapagar?ACAO=criar");
		} else if(bean.getFromPagamentoComissao()){
			return new ModelAndView("redirect:/rh/crud/Documentocomissao");
		} else if(bean.getFromPagamentoComissaoVenda()){
			return new ModelAndView("redirect:/rh/crud/Documentocomissaovenda");
		} else if(bean.getFromPagamentoComissaoDesempenho()){
			return new ModelAndView("redirect:/rh/crud/Documentocomissaodesempenho");
		} else if(bean.getColeta() != null){
			return new ModelAndView("redirect:/faturamento/crud/Coleta?ACAO=consultar&cdcoleta=" + bean.getColeta().getCdcoleta());
		} else if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		} else if(bean.getFromPagamentoComissaoPorFaixas()) {
			return new ModelAndView("redirect:/rh/crud/ComissionamentoPorFaixas");
		} else {
			return super.getSalvarModelAndView(request, bean);
		}
	}
	
	
	/**
	 * Recebe os itens selecionados para devolu��o em contas a receber.
	 * Verifica quais contas devem ser devolvidas e gera uma conta a pagar.
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public ModelAndView validaContaReceber (WebRequestContext request, Documento form) throws Exception{
		String itens[] = request.getParameter("selectedItens").split(",");
		form.setContaReceberDevolucao(request.getParameter("selectedItens"));
	
		//Verifica��o de data limite do ultimo fechamento. 
		String whereIn = request.getParameter("selectedItens");
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Nenhum item selecionado");
		}
		Documento aux = new Documento();
		aux.setWhereIn(whereIn);
		if(fechamentofinanceiroService.verificaListaFechamento(aux)){
			request.addError("Existem  refere-se a um per�odo j� fechado.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber");
		}
		
		Integer cddocumento = Integer.valueOf(itens[0]);
		Documento documento = documentoService.contaReceberDevolucao(cddocumento);
		if(documento.getDtdevolucao() != null){
			request.addError("Esta conta j� foi devolvida.");
			return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=listagem");
		}
		if(!contapagarService.validaClienteRepetido(request.getParameter("selectedItens"), documento.getPessoa().getCdpessoa())){
			request.addError("� permitido devolver contas somente de um �nico cliente.");
			 return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=listagem");
		}
		if(contapagarService.verificaEmpresasEmComun(request.getParameter("selectedItens"))){
			form.setEmpresa(documento.getEmpresa());
		}
		if(contapagarService.verificaDocumentosEmComun(request.getParameter("selectedItens"))){
			form.setNumero(documento.getNumero());
		}
		if(contapagarService.verificaTiposDocumentosEmComun(request.getParameter("selectedItens"))){
			form.setDocumentotipo(documento.getDocumentotipo());
		}
		
		form.setTipopagamento(Tipopagamento.CLIENTE);
		form.setDtemissao(new Date(System.currentTimeMillis()));
		form.setDtvencimento(new Date(System.currentTimeMillis()));
		
		form.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		Cliente cliente = new Cliente();
		cliente.setCdpessoa(documento.getPessoa().getCdpessoa());
		cliente.setNome(documento.getPessoa().getNome());
		form.setCliente(cliente);
		
		if(itens.length == 1){
			form.setDescricao(documento.getDescricao());
		}
		
		form.setRateio(new Rateio());
		
		Money valorTotal = new Money();
		for (String item : itens) {
			cddocumento = Integer.valueOf(item);
			documento.setCddocumento(cddocumento);
			documento = documentoService.contaReceberDevolucao(cddocumento);
			
			if(!documento.getDocumentoacao().equals(Documentoacao.BAIXADA)){
				request.addError("Apenas contas baixadas podem ser devolvidas.");
				return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=listagem");
			}
			if(documento.getTipopagamento() != null && documento.getTipopagamento() != (Tipopagamento.CLIENTE)){
				request.addError("� permitido devolver somente contas cujo tipo de pagamento � cliente.");
				return new ModelAndView("redirect:/financeiro/crud/Contareceber?ACAO=listagem");
			}
			Rateio rateio = new Rateio();
			rateio = rateioService.findByDocumento(documento);
			for (Rateioitem ri : rateio.getListaRateioitem()) {
				
				Rateioitem rateioItem = new Rateioitem();
				
				rateioItem.setCdusuarioaltera(ri.getCdusuarioaltera());
				rateioItem.setDtaltera(ri.getDtaltera());
				rateioItem.setNome(ri.getNome());
				rateioItem.setPercentual(ri.getPercentual());
				rateioItem.setProjeto(ri.getProjeto());
				rateioItem.setQtdeaux(ri.getQtdeaux());
				rateioItem.setValor(ri.getValor());
				
				form.getRateio().getListaRateioitem().add(rateioItem);
				
			}
			valorTotal = valorTotal.add(documento.getValor());
		}
		form.setValor(valorTotal);
		
		setInfoForTemplate(request, form);
		setEntradaDefaultInfo(request, form);
		entrada(request, form);
//		request.setAttribute("podeAlterarValor", SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR"));
		if (form.getPodeAlterarValor()==null){
			form.setPodeAlterarValor(SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR"));
		}
		return getEntradaModelAndView(request, form);
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Documento form)throws CrudException {
		if(request.getAttribute("CONSULTAR") == null && request.getParameter("ACAO") != null && request.getParameter("ACAO").equals("editar") ){	
			if(fechamentofinanceiroService.verificaFechamento(form)){
				request.addError("Esta conta n�o pode ser editada pois refere-se a um per�odo j� fechado.");
				request.setAttribute("CONSULTAR", true);
				return super.doConsultar(request, form);
			}
		}
		return super.doEntrada(request, form);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void entrada(WebRequestContext request, Documento form)throws Exception {
		documentoService.carregaDadosPessoaDocumentoeForEntrada(form);
		
		List<Centrocusto> listaCentroCusto = null;
		List<Frequencia> listaFreq = frequenciaService.findForContas();
		
		if(form != null && form.getDtvencimentoAux() == null){
			form.setDtvencimentoAux(form.getDtvencimento());
		}
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		
		//Setar lista completar somente no consultar
		List<Documentoacao> listaAcao = null;
		if (!CONSULTAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER))) {
			listaAcao = getListaStatus(form, request);
		} else {
			listaAcao = documentoacaoService.findForContapagar();
		}
		
		if (request.getBindException().hasErrors()) {
			if (form.getCddocumento() == null) {
				request.setAttribute(MultiActionController.ACTION_PARAMETER, CrudControllerSined.CRIAR);
			} else {
				request.setAttribute(MultiActionController.ACTION_PARAMETER, CrudControllerSined.EDITAR);
				Documento documento = documentoService.getDocumentoComArquivo(form);
				form.setImagem(documento.getImagem());
			}
			
		}

		if (form.getCddocumento() != null) {
			listaCentroCusto = (List<Centrocusto>) CollectionsUtil.getListProperty(form.getRateio().getListaRateioitem(), "centrocusto");
			
			if (form.getDocumentoacao().equals(Documentoacao.BAIXADA)) {
				List<Movimentacao> listaMovimentacao = movimentacaoService.findByDocumentoVariasMovimentacoes(form);
				request.setAttribute("listaVariasMovimentacoes", listaMovimentacao);	
			}
			form.setDtvencimentooinicial(form.getDtvencimento());
			form.setListaApropriacao(documentoApropriacaoService.findByDocumento(form));
			
			if(historicoAntecipacaoService.isContaGeradaAPartirDeCompensacaoDeSaldo(form)) {
				form.setIsCompensarSaldo(true);
			}
			
		}else{
			if(form.getTipopagamento() == null){
				form.setTipopagamento(Tipopagamento.FORNECEDOR);
			}
		}
		if(form.getPrazo() == null){
			Prazopagamento prazo = form.getListaParcela() != null && form.getListaParcela().iterator().hasNext() ? form.getListaParcela().iterator().next().getParcelamento().getPrazopagamento() : null;
			form.setPrazo(prazo);
		}
		
		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
			List<Projeto> projetos = rateioitemService.getProjetos(form.getRateio().getListaRateioitem());
			listaProjeto = projetoService.findProjetosAbertosSemPermissao(projetos, true, true);
		} else {
			listaProjeto = projetoService.findProjetosAbertos(null, true, true);
		}
		request.setAttribute("listaProjeto", listaProjeto);
		
		if(form.getFornecedor() != null){
			form.getFornecedor().setNome(pessoaService.load(form.getFornecedor(), "pessoa.nome").getNome());
		}else if(form.getCliente() != null){
			form.getCliente().setNome(pessoaService.load(form.getCliente(), "pessoa.nome").getNome());
		}else if(form.getColaborador() != null){
			form.getColaborador().setNome(pessoaService.load(form.getColaborador(), "pessoa.nome").getNome());
		}
		if(form.getCddocumento() != null){
			Agendamento agendamentoOrigem = documentoorigemService.findDocumentoOrigemAgendamentoByDocumento(form);
			if(agendamentoOrigem != null){
				request.setAttribute("agendamentos", Arrays.asList(new Agendamento[]{agendamentoOrigem}));
			}
		}
		request.setAttribute("listaPrazo", prazopagamentoService.findAtivos(form.getPrazo()));
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(listaCentroCusto));
		request.setAttribute("frequencia", listaFreq);
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
		request.setAttribute("listaStatus", listaAcao);
		request.setAttribute("clearBase", form.getCdAgendamento());
		
		String cddocumentotipo = form.getCddocumento() != null && form.getDocumentotipo() != null && form.getDocumentotipo().getCddocumentotipo() != null? form.getDocumentotipo().getCddocumentotipo().toString(): "";
		
		request.setAttribute("listaDocumentoTipo", documentotipoService.getListaDocumentoTipoUsuarioForCompra(cddocumentotipo));
		request.setAttribute("listaTipopagamento", documentoService.findTipopagamentoForDocumento(Tipooperacao.TIPO_DEBITO));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
//		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		request.setAttribute("integracao_contabil", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INTEGRACAO_CONTABIL)));
		request.setAttribute("listaRateioModelo", rateiomodeloService.findBy(Tipooperacao.TIPO_DEBITO, true));
		
		documentoService.adicionaInformacoesByEntrega(form);
		
		rateioService.ajustePercentualRateioByDocumento(form);
		if (form.getEmpresa() != null){
			request.getSession().setAttribute(CONTA_PAGAR_EMPRESA, form.getEmpresa().getCdpessoa());
		}
		else request.getSession().setAttribute(CONTA_PAGAR_EMPRESA, null);
		
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		if (form.getUf()!=null && form.getUf()!=null){
			listaMunicipio = municipioService.findByUf(form.getUf());
		}
		request.setAttribute("listaMunicipio", listaMunicipio);
		
		if(Util.booleans.isTrue(parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_DOCUMENTO_CR_CP))){
			request.setAttribute("obrigaDocumento", true);
		}
		
//		request.setAttribute("podeAlterarValor", SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR") || documentoService.isPodeAlterarValorConta(request, form));
		if (form.getPodeAlterarValor()==null){
			form.setPodeAlterarValor(SinedUtil.isUserHasAction("ALTERAR_VALOR_CONTA_RECEBER_PAGAR") || documentoService.isPodeAlterarValorConta(request, form));
		}
	}
	private List<Documentoacao> getListaStatus(Documento form, WebRequestContext request) {
		List<Documentoacao> listaAcao;
		listaAcao = new ArrayList<Documentoacao>();
		listaAcao.add(new Documentoacao(Documentoacao.DEFINITIVA.getCddocumentoacao(),"DEFINITIVA"));
		if(form.getCddocumento() != null || !"TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.NOTACOMPRA_GERA_CONTADEFINITIVA)) || Boolean.TRUE.equals(form.getAdiatamentodespesa())){
			listaAcao.add(new Documentoacao(Documentoacao.PREVISTA.getCddocumentoacao(),"PREVISTA"));
			if (request.getParameter("cdAgendamento") != null && !StringUtils.isEmpty(request.getParameter("cdAgendamento")) && SinedUtil.isUserHasAction("AUTORIZAR_CONTAPAGAR")){
				listaAcao.add(new Documentoacao(Documentoacao.AUTORIZADA.getCddocumentoacao(),"AUTORIZADA"));
			}
		}
		return listaAcao;
	}
	
	@Override
	protected Documento carregar(WebRequestContext request, Documento bean)throws Exception {
		Documento documento = super.carregar(request, bean);
		if (documento == null) {
			throw new SinedException("Registro n�o encontrado no sistema.");
		}else if(documento.getVinculoProvisionado() != null && !SinedUtil.isUsuarioLogadoAdministrador() && !contaService.isUsuarioPermissaoConta(documento.getVinculoProvisionado())){
			throw new SinedException("Registro n�o encontrado no sistema.");
		}
		
		documento.ajustaBeanToLoad();
		
		// necess�rio recarregar a pessoa porque quando o mesmo CDPESSOA �
		// referenciado em mais de uma tabela o Hibernate carrega a primeira
		// ocorr�ncia, fazendo com que uma conta a receber de fornecedor receba
		// um objeto "pessoa" que na verdade � um Cliente ao inv�s do Fornecedor.
		if (Tipopagamento.CLIENTE.equals(documento.getTipopagamento())) {
			documento.setCliente(ClienteService.getInstance().loadReader(documento.getCliente()));
		} else if (Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento())) {
			documento.setFornecedor(FornecedorService.getInstance().loadReader(documento.getFornecedor()));
		} else if (Tipopagamento.COLABORADOR.equals(documento.getTipopagamento())) {
			documento.setColaborador(ColaboradorService.getInstance().loadReader(documento.getColaborador()));
		}

		String acao = request.getParameter(MultiActionController.ACTION_PARAMETER);
		if("editar".equals(acao) &&
				!documento.getDocumentoacao().equals(Documentoacao.PREVISTA) && 
				!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)  
		){
			throw new SinedException("N�o � poss�vel editar uma conta a pagar com situa��o diferente de prevista ou definitiva.");
		}

		documento.setListaDocumentohistorico(new ListSet<Documentohistorico>(Documentohistorico.class,documentohistoricoService.carregaHistorico(documento)));
		documento.setTaxa(taxaService.findByDocumento(documento));
		documento.setRateio(rateioService.findByDocumento(documento));

		documentoService.calculaValores(documento);

		if(Util.booleans.isTrue(documento.getFinanciamento())) request.setAttribute("financiado", true);
		request.setAttribute("showEditar", documento.getDocumentoacao().equals(Documentoacao.PREVISTA) || documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA));

		documento.setAcaoanterior(documento.getDocumentoacao());

		boolean disabled = "consultar".equals(request.getParameter(MultiActionController.ACTION_PARAMETER)) 
		|| "salvar".equals(request.getParameter(MultiActionController.ACTION_PARAMETER)) 
		&& request.getBindException().getErrorCount() == 0;
		request.setAttribute("disabled", disabled);

		return documento;
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Documento form)	throws CrudException {
		
		if(fechamentofinanceiroService.verificaFechamento(form)){
			throw new SinedException("A data de vencimento refere-se a um per�odo j� fechado.");
		}
		if(form != null && form.getDtvencimento() != null && form.getDtvencimentoAux() != null
				&& !SinedDateUtils.equalsIgnoreHour(form.getDtvencimento(), form.getDtvencimentoAux())
				&& form.getCopiadocumeto() != null && !form.getCopiadocumeto()){
			form.setDtvencimentoantiga(form.getDtvencimentoAux());
		}
		
		/*Se vier da tela fornecimento do suprimentos retorna para a listagem do fornecimento
		 * no modulo suprimentos*/
		if((form.getFromFornecimento() != null && form.getFromFornecimento()) || (form.getFromDespesaVeiculo() != null && form.getFromDespesaVeiculo())) {
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Conta a pagar "+form.getCddocumento()+" gerada com sucesso.", MessageType.INFO);
			if(form.getFromFornecimento() != null && form.getFromFornecimento())
				return new ModelAndView("redirect:/suprimento/crud/Fornecimento?ACAO=listagem");
			else
				return new ModelAndView("redirect:" + form.getController());
		}
		if(form.getWhereInEntrega() != null && !"".equals(form.getWhereInEntrega())){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Faturamento gerado com sucesso.");
			return new ModelAndView("redirect:/suprimento/crud/Entrega?ACAO=listagem");
		}
		if(form.getWhereInOrdemcompra() != null && !"".equals(form.getWhereInOrdemcompra())){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Antecipa��o gerada com sucesso.");
			if(form.getController() != null && !"".equals(form.getController())){
				return new ModelAndView("redirect:" + form.getController());
			}else {
				return new ModelAndView("redirect:/suprimento/crud/Ordemcompra?ACAO=listagem");
			}
		}
		if(form.getCdAgendamento() !=null){
			Integer cdAgendamento = form.getCdAgendamento();
			Agendamento agendamento = agendamentoService.findByCdAgendamento(cdAgendamento);
			Integer situacao = agendamento.getAux_agendamento().getSituacao();
			//SituacaoAgendamento.ATENCAO.getValue()
			if(situacao != 2 && situacao != 3
					&& situacao != 4){
				request.addError("N�o � poss�vel consolidar este agendamento pois sua situa��o n�o � \"A Consolidar\", \"Aten��o\" ou \"Atrasado\".");
				return new ModelAndView("redirect:/financeiro/crud/Contapagar?ACAO=criar");
			}
		}
		if(form.getWhereInEntregadocumento() != null && !"".equals(form.getWhereInEntregadocumento())){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Faturamento gerado com sucesso.");
			return new ModelAndView("redirect:/fiscal/crud/Entradafiscal?ACAO=listagem");
		}
		/*Se vier da tela de entrega retorna ou para a listagem ou para tela de entrada*/
		if(form.getController() != null && (form.getEntrega() != null || form.getWhereIn() != null)){
			String controller = form.getController();
			if(controller.contains("consultar") && !controller.contains("cdentrega")){
				controller += "&cdentrega="+form.getEntrega().getCdentrega();
			}
			
			if(documentoorigemService.existeDocumentoComOrigemEntrega(form.getEntrega()) && form.getWhereIn() == null){
				request.addError("A entrega "+form.getEntrega().getCdentrega()+" j� foi faturada.");
				return new ModelAndView("redirect:"+controller);
			}
			
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Conta(s) a pagar "+documentoService.getCodigosContasPagar(form)+" gerada(s) com sucesso.", MessageType.INFO);
			return new ModelAndView("redirect:"+controller);
		}
		
		/*Se vier da tela de entrega retorna ou para a listagem ou para tela de entrada*/
		if(form.getController() != null && form.getWhereInDespesaviagem() != null && !form.getWhereInDespesaviagem().equals("")){
			super.doSalvar(request, form);
			
			String observacao = "<a href=/w3erp/financeiro/crud/Contapagar?ACAO=consultar&cddocumento="+form.getCddocumento()+">"+form.getCddocumento()+"</a>";
			Despesaviagemacao acao;
			if(form.getAdiatamentodespesa()){
				acao = Despesaviagemacao.ADIANTAMENTO;
			}else{
				acao = Despesaviagemacao.ACERTO;
			}
			despesaviagemService.salvaHistorico(form.getWhereInDespesaviagem(), acao, observacao);
			request.clearMessages();
			request.addMessage("Conta(s) a pagar gerada(s) com sucesso.", MessageType.INFO);
			return new ModelAndView("redirect:"+ form.getController());
		}
		
		return super.doSalvar(request, form);
	}

	@Override
	protected void salvar(WebRequestContext request, Documento bean)throws Exception {
		boolean criar = bean.getCddocumento() == null;
		/* Essa verifica��o est� sendo feita por outro m�todo Ajax.
		if(bean.getCddocumento() == null && !documentoService.isAlowToSaveBean(request,bean)){
			throw new SinedException("N�o � poss�vel salvar o mesmo registro novamente.");	
		}
		*/
		boolean criaHistoricoCheque = false;
		if(bean.getCheque() != null && bean.getCheque().getCdcheque() != null){
			if(bean.getCddocumento() != null){
				if(!documentoService.isChequeAssociado(bean)){
					criaHistoricoCheque = true;
				}
			}else {
				criaHistoricoCheque = true;
			}
		}
		if(bean.getCddocumento() != null){
			Documento documento_aux = documentoService.load(bean, "documento.cddocumento, documento.documentoacao");
			bean.setAcaoanterior(documento_aux.getDocumentoacao());
			
			if(!bean.getAcaoanterior().equals(Documentoacao.PREVISTA) && !bean.getAcaoanterior().equals(Documentoacao.DEFINITIVA)){
				throw new SinedException("S� � poss�vel alterar as informa��es de uma conta a pagar que estiver na situa��o prevista ou definitiva.");
			}
			bean.setAcaohistorico(Documentoacao.ALTERADA);
		}
		
		bean.ajustaBeanToSave();
		
		//Limpa os campos de financiamento:
		if (BooleanUtils.isFalse(bean.getFinanciamento())) {
			bean.setPrazo(null);
			bean.setRepeticoes(null);
			bean.setListaDocumento(null);
		}

		if(!request.getParameter("whereInOrdemcompra").equals("<null>") && !request.getParameter("whereInOrdemcompra").isEmpty()){
			String nome = request.getParameter("whereInOrdemcompra");
			Integer cdordemcompra = Integer.parseInt(nome);
			Ordemcompra ordemcompra = new Ordemcompra(cdordemcompra);
			ordemcompra.setOrdemcompraantecipada(true);
			ordemcompraService.salvarCompraantecipada(ordemcompra);
		}
		boolean imposto = "true".equalsIgnoreCase(request.getParameter("imposto"));
		if(imposto){
			contapagarService.preparaListaNotaDocumento(bean);
		}
		
		if (bean.getListaDocumento() != null){
			for (Documento doc : bean.getListaDocumento()) {
				if(doc.getDocumentoacao() == null)
					doc.setDocumentoacao(Documentoacao.PREVISTA);
			}
		}
		
		if(bean.getDtcompetencia() == null) {
			bean.setDtcompetencia(bean.getDtemissao() != null ? bean.getDtemissao() : new Date(System.currentTimeMillis()));
		}
		
		try {
			if (bean.getCdAgendamento() != null) {
				agendamentoService.doConsolidar(bean);
				request.addMessage("Registro salvo com sucesso.");
				request.setAttribute("closeWindow", true);
			}else {
				String textoAlterouVencimento = bean.getTextoAlterouVencimento(); 
				
				super.salvar(request, bean);
				if(parametrogeralService.getBoolean(Parametrogeral.ABRIR_TELA_CONTAPAGAR_NA_ENTREGA) &&
					Util.strings.isNotEmpty(bean.getWhereInEntrega())){
					saveAssociacaoEntregaComDocumentoantecipado(bean.getWhereInEntrega());
				}
									
				if(bean.getObservacaoHistorico() != null){
					if (textoAlterouVencimento!=null){
						bean.setObservacaoHistorico(textoAlterouVencimento + ". " + Util.strings.emptyIfNull(bean.getObservacaoHistorico()));
					}
					documentohistoricoService.updateObservacaoDocumento(bean, bean.getObservacaoHistorico());
				}
				
				if((bean.getFromPagamentoComissao() != null && bean.getFromPagamentoComissao()) || 
						(bean.getFromPagamentoComissaoVenda() != null && bean.getFromPagamentoComissaoVenda()) ||
						(bean.getFromPagamentoComissaoDesempenho() != null && bean.getFromPagamentoComissaoDesempenho())){
					if(bean.getFromPagamentoComissaoDesempenho() != null && bean.getFromPagamentoComissaoDesempenho() && 
							((bean.getWhereInDocumentoorigemDesempenho() != null && !"".equals(bean.getWhereInDocumentoorigemDesempenho())) ||
							 (bean.getWhereInNotadocumentoDesempenho() != null && !"".equals(bean.getWhereInNotadocumentoDesempenho()))) ||
							 (bean.getWhereInVendaDesempenho() != null && !"".equals(bean.getWhereInVendaDesempenho()))){
						salvaColaboradorcomissaoPorDesempenho(bean);						
					}
					request.addMessage("Conta a pagar da comiss�o gerada.");
				}
				if(criaHistoricoCheque){
					Chequehistorico chequehistorico = chequehistoricoService.criaHistorico(bean.getCheque(), bean, null, Documentoclasse.OBJ_PAGAR);
					if(chequehistorico != null) chequehistoricoService.saveOrUpdate(chequehistorico);
				}
				if(bean.getWhereInOrdemcompra() != null && !"".equals(bean.getWhereInOrdemcompra())){
					ordemcomprahistoricoService.criaHistoricoAntecipacao(bean);
				}
				
				if(StringUtils.isNotBlank(bean.getWhereInDespesaviagem())){
					StringBuilder obs = new StringBuilder();
					if(bean.getAdiatamentodespesa() != null && bean.getAdiatamentodespesa()){
						obs.append("Adiantamento da(s) despesa(s) de viagem:");
					}else{
						obs.append("Acerto da(s) despesa(s) de viagem:");
					}
					for(String idDespesaviagem : bean.getWhereInDespesaviagem().split(",")){
						obs.append(" <a href=/w3erp/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+idDespesaviagem+">"+idDespesaviagem+"</a>,");
					}
					
					Documentohistorico dh = new Documentohistorico();
					dh.setDocumento(bean);
					dh.setDocumentoacao(bean.getDocumentoacao());
					dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
					dh.setObservacao(obs.substring(0, obs.length()-1));
					documentohistoricoService.saveOrUpdate(dh);
				}
				if(bean.getColeta() != null && bean.getColeta().getCdcoleta() != null){
					Coletahistorico coletahistorico = new Coletahistorico();
					coletahistorico.setColeta(bean.getColeta());
					coletahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					coletahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					coletahistorico.setAcao(Coletaacao.PAGAMENTO);
					coletahistorico.setObservacao("Conta a pagar: <a href=\"javascript:visualizaContapagar("+bean.getCddocumento()+");\">"+bean.getCddocumento()+"</a>");
					coletahistoricoService.saveOrUpdate(coletahistorico);
				}
				if(StringUtils.isNotBlank(bean.getWhereInDocumentoAdiantamento())) {
					documentoService.gerarHistoricoCompensacao(bean);
					
					Documentohistorico dh = new Documentohistorico();
					dh.setDocumento(bean);
					dh.setDocumentoacao(bean.getDocumentoacao());
					dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
					
					StringBuilder observacao = new StringBuilder("Conta criada a partir de compensa��o de saldo das contas ");
					for(String id : bean.getWhereInDocumentoAdiantamento().split(",")) {
						observacao.append("<a href=\"javascript:visualizaContareceber(" + id + ");\">" + id + "</a>");
					}
					dh.setObservacao(observacao.toString());
					
					documentohistoricoService.saveOrUpdate(dh);
				}
			}
		} catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_TAXAITEM_TIPOTAXA")){
				throw new SinedException("N�o deve haver tipos de taxas iguais.");
			}else{
				throw e;
			}
		} 
		
		if(imposto){
			request.getServletResponse().setContentType("text/html");
			View.getCurrent()
				.eval("<script type='text/javascript'>")
					.eval("window.opener.afterSaveDocumento(" + bean.getCddocumento() + ");")
					.eval("window.top.close();")
				.eval("</script>")
				.flush();
		}
		
		boolean consolidar = "true".equalsIgnoreCase(request.getParameter("fecharJanelaAposSalvar"));
		if(consolidar) {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent()
				.eval("<script type='text/javascript'>")
					.eval("alert('Registro salvo com sucesso');")
					.eval("window.top.close();")
				.eval("</script>")
				.flush();
		}
		
		if(criar && Documentoacao.DEFINITIVA.equals(bean.getDocumentoacao()) && !SinedUtil.isUserHasAction("AUTORIZARPARCIAL_CONTAPAGAR")) {
			List<Documento> listaDocumento = new ArrayList<Documento>();
			listaDocumento.add(bean);
			avisoService.salvarAvisos(documentoService.montaAvisoDocumento(MotivoavisoEnum.AUTORIZAR_CONTAPAGAR, listaDocumento, "Conta a pagar autorizada"), true);
		}
	}
	
	/**
	 * M�todo que cria e salva o colaboradorcomissao pela comiss�o de desempenho
	 *
	 * @param bean
	 * @author Luiz Fernando
	 */
	private void salvaColaboradorcomissaoPorDesempenho(Documento bean) {
		String[] cdsDocumentoorigem = null;
		String[] cdsNotadocumento = null;
		String[] cdsVenda = null;
		if(bean.getWhereInDocumentoorigemDesempenho() != null && !"".equals(bean.getWhereInDocumentoorigemDesempenho()))
			cdsDocumentoorigem = bean.getWhereInDocumentoorigemDesempenho().split(",");
		if(bean.getWhereInNotadocumentoDesempenho() != null && !"".equals(bean.getWhereInNotadocumentoDesempenho()))
			 cdsNotadocumento = bean.getWhereInNotadocumentoDesempenho().split(",");
		if(bean.getWhereInVendaDesempenho() != null && !"".equals(bean.getWhereInVendaDesempenho()))
			cdsVenda = bean.getWhereInVendaDesempenho().split(",");
		if(cdsDocumentoorigem != null && cdsDocumentoorigem.length > 0){
			for(int i = 0; i < cdsDocumentoorigem.length; i++){
				Colaboradorcomissao colaboradorcomissao = new Colaboradorcomissao();
				Documentoorigem origem = new Documentoorigem();
				origem.setCddocumentoorigem(Integer.parseInt(cdsDocumentoorigem[i]));
				colaboradorcomissao.setDesempenho(Boolean.TRUE);
				colaboradorcomissao.setDocumento(bean);
				colaboradorcomissao.setDocumentoorigem(origem);
				colaboradorcomissao.setColaborador(bean.getColaborador());
				if(SinedUtil.getUsuarioComoColaborador() != null)
					colaboradorcomissao.setCdusuarioaltera(SinedUtil.getUsuarioComoColaborador().getCdpessoa());
				colaboradorcomissao.setDtaltera(new Timestamp(System.currentTimeMillis()));
				colaboradorcomissaoService.saveOrUpdate(colaboradorcomissao);
			}
		}
		if(cdsNotadocumento != null && cdsNotadocumento.length > 0){
			for(int i = 0; i < cdsNotadocumento.length; i++){
				Colaboradorcomissao colaboradorcomissao = new Colaboradorcomissao();
				NotaDocumento notadocumento = new NotaDocumento();
				notadocumento.setCdNotaDocumento(Integer.parseInt(cdsNotadocumento[i]));
				colaboradorcomissao.setDesempenho(Boolean.TRUE);
				colaboradorcomissao.setDocumento(bean);
				colaboradorcomissao.setNotaDocumento(notadocumento);
				colaboradorcomissao.setColaborador(bean.getColaborador());
				if(SinedUtil.getUsuarioComoColaborador() != null)
					colaboradorcomissao.setCdusuarioaltera(SinedUtil.getUsuarioComoColaborador().getCdpessoa());
				colaboradorcomissao.setDtaltera(new Timestamp(System.currentTimeMillis()));
				colaboradorcomissaoService.saveOrUpdate(colaboradorcomissao);
			}
		}
		if(cdsVenda != null && cdsVenda.length > 0){
			for(int i = 0; i < cdsVenda.length; i++){
				Colaboradorcomissao colaboradorcomissao = new Colaboradorcomissao();
				Venda venda = new Venda();
				venda.setCdvenda(Integer.parseInt(cdsVenda[i]));
				colaboradorcomissao.setDesempenho(Boolean.TRUE);
				colaboradorcomissao.setDocumento(bean);
				colaboradorcomissao.setVenda(venda);
				colaboradorcomissao.setColaborador(bean.getColaborador());
				if(SinedUtil.getUsuarioComoColaborador() != null)
					colaboradorcomissao.setCdusuarioaltera(SinedUtil.getUsuarioComoColaborador().getCdpessoa());
				colaboradorcomissao.setDtaltera(new Timestamp(System.currentTimeMillis()));
				colaboradorcomissaoService.saveOrUpdate(colaboradorcomissao);
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Documento bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir uma conta a pagar.");		
	}
	
	/**
	 * M�todo de suporte para Baixar conta pagar. Cria uma conta a pagar com alguns valores j� preenchidos.
	 * 
	 * @see #carregar(WebRequestContext, Documento)
	 * @see #doEntrada(WebRequestContext, Documento)
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Fl�vio Tavares
	 */
	@Action("carregarConta")
	@Input(LISTAGEM)
	public ModelAndView carregarConta(WebRequestContext request, Documento bean) throws Exception{		
		Documento documento = this.carregar(request, bean);
		documento.setCddocumento(null);
		documento.setListaDocumento(null);
		documento.setFinanciamento(null);
		documento.setRepeticoes(null);
		documento.setPrazo(null);
		documento.setListaParcela(null);
		
		documento.setObservacaoHistorico("Valor restante de uma conta autorizada parcialmente.");
		Set<Documentohistorico> listaHistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
		listaHistorico.add(new Documentohistorico(documento));
		
		documento.setListaDocumentohistorico(listaHistorico);
		documento.setValor(bean.getValor());
		
		rateioService.atualizaValorRateio(documento.getRateio(), bean.getValor());
		
		Boolean autorizadaParcial = Boolean.valueOf(request.getParameter("autorizadaParcial") != null ? request.getParameter("autorizadaParcial") : "FALSE");
		if(autorizadaParcial != null && autorizadaParcial)
			rateioService.limpaReferenciaRateio(documento.getRateio());
		
		request.setAttribute("change", true);
		request.setAttribute("showHistorico", true);
		request.setAttribute("financiado", false);
		return this.doEntrada(request, documento);
	}
	
	
	
	/**
	 * A��o que vem com algumas informa��es dos fornecimentos selecionados na tela.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@Action("fromFornecimento")
	@Input(LISTAGEM)
	public ModelAndView fromFornecimento(WebRequestContext request, Documento bean) throws Exception {
		
		if (bean == null || bean.getIdsFornecimento() == null || bean.getIdsFornecimento().equals("")) {
			throw new SinedException("Ids de fornecimento n�o pode ser nulo.");
		}
		
		List<Fornecimento> listaFornecimento = fornecimentoService.findForFaturar(bean.getIdsFornecimento());
		
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		Rateioitem rateioitem = null;
		Money valortotal = new Money();
		Fornecimentocontrato fc = null;
		boolean difFc = false;
		
		for (Fornecimento fornecimento : listaFornecimento) {
			
			if(!difFc){
				fc = fornecimento.getFornecimentocontrato();
				difFc = true;
			} else if(!fc.equals(fornecimento.getFornecimentocontrato())){
				request.addError("Os fornecimentos devem ter o mesmo fornecedor.");
				return new ModelAndView("redirect:/suprimento/crud/Fornecimento");
			}
			
			
			if (fornecimento.getBaixado()) {
				request.addError("Fornecimento(s) j� baixados.");
				return new ModelAndView("redirect:/suprimento/crud/Fornecimento");
			}
			
			rateioitem = new Rateioitem();
			rateioitem.setCentrocusto(fornecimento.getFornecimentocontrato().getCentrocusto());
			rateioitem.setProjeto(fornecimento.getFornecimentocontrato().getProjeto());
			rateioitem.setContagerencial(fornecimento.getFornecimentocontrato().getContagerencial());
			rateioitem.setValor(fornecimento.getValor());
			
			listaRateioitem.add(rateioitem);
			
			valortotal = valortotal.add(fornecimento.getValor());			
		}
		
		bean.setValor(valortotal);
		
		for (Rateioitem item : listaRateioitem) {
			item.setPercentual(entregaService.calculaPercentual(bean.getValor(), item.getValor()));
		}
		
		entregaService.reCalculaPercentual(listaRateioitem);
		rateioitemService.agruparItensRateio(listaRateioitem);
		
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(listaRateioitem);
		
		rateioService.calculaValoresRateio(rateio, bean.getValor());
		
		bean.setRateio(rateio);

		bean.setFornecedor(fc.getFornecedor());
		bean.setDtemissao(new Date(System.currentTimeMillis()));
		bean.setDtvencimento(fc.getDtproximovencimento());
		bean.setPrazo(fc.getPrazopagamento());
		bean.setFinanciamento(true);
		bean.setReferencia(Referencia.MES_ANO_CORRENTE);
		bean.setDocumentoacao(Documentoacao.PREVISTA);
		if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.NOTACOMPRA_GERA_CONTADEFINITIVA))){
			bean.setDocumentoacao(Documentoacao.DEFINITIVA);
		}
		bean.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		bean.setFromFornecimento(Boolean.TRUE);
		
		return this.doEntrada(request, bean);
	}
	
	/**
	 * A��o que vem com algumas informa��es dos fornecimentos selecionados na tela.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@Action("fromFornecimento")
	@Input(LISTAGEM)
	public ModelAndView fromVeiculoDespesa(WebRequestContext request, Documento bean) throws Exception {
		
		if (bean == null || bean.getIdsVeiculosDespesa() == null || bean.getIdsVeiculosDespesa().equals("")) {
			throw new SinedException("Ids de fornecimento n�o pode ser nulo.");
		}
		
		List<Veiculodespesa> listaDespesas = veiculodespesaService.findForFaturar(bean.getIdsVeiculosDespesa());
		
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		Rateioitem rateioitem = null;
		Money valortotal = new Money();
		Fornecedor f = null;
		boolean difFc = false;
		
		for (Veiculodespesa veiculodespesa : listaDespesas) {
			
			if(!difFc){
				f = veiculodespesa.getFornecedor();
				difFc = true;
			} else if(!f.equals(veiculodespesa.getFornecedor())){
				request.addError("As despesas devem ter o mesmo fornecedor.");
				return new ModelAndView("redirect:/veiculo/crud/Veiculodespesa" + ("true".equals(request.getParameter("ENTRADA")) ? "?ACAO=consultar&cdveiculodespesa=" + bean.getIdsVeiculosDespesa() : ""));
			}
			
			if ((Boolean.TRUE.equals(veiculodespesa.getFaturado()) && contapagarService.verificaContaCanceladaVeiculoDespesa(veiculodespesa)) || veiculodespesa.getValortotal()==null || veiculodespesa.getValortotal().toLong()==0L) {
//				request.addError("Despesa(s) de ve�culo j� faturado.");
				request.addError("Despesa j� faturada ou com valor zerado.");
				return new ModelAndView("redirect:/veiculo/crud/Veiculodespesa" + ("true".equals(request.getParameter("ENTRADA")) ? "?ACAO=consultar&cdveiculodespesa=" + bean.getIdsVeiculosDespesa() : ""));
			}
			
			if (SinedUtil.isListNotEmpty(veiculodespesa.getListaveiculodespesaitem()) && veiculodespesa.getVeiculodespesadefinirprojeto() != null && veiculodespesa.getVeiculodespesadefinirprojeto().equals(Veiculodespesadefinirprojeto.POR_ITEM_DA_DESPESA)) {
				for (int i = 0; i < veiculodespesa.getListaveiculodespesaitem().size(); i++) {
					boolean adicionadoListaReteioItem = false;
					Veiculodespesaitem veiculodespesaitem1 =  veiculodespesa.getListaveiculodespesaitem().get(i);
					for (int j = 0; j < veiculodespesa.getListaveiculodespesaitem().size(); j++) {
						Veiculodespesaitem veiculodespesaitem2 = veiculodespesa.getListaveiculodespesaitem().get(j);
						if (j != i && veiculodespesaitem1.getProjeto() != null && veiculodespesaitem2.getProjeto() != null
								&& veiculodespesaitem1.getProjeto().equals(veiculodespesaitem2.getProjeto())) {
								rateioitem = new Rateioitem();
								rateioitem.setCentrocusto(veiculodespesa.getCentrocusto());
								rateioitem.setProjeto(veiculodespesaitem1.getProjeto());
								Double valorTotal = veiculodespesaitem1.calculaValorTotal().getValue().doubleValue() + veiculodespesaitem2.calculaValorTotal().getValue().doubleValue(); 
								rateioitem.setValor(new Money(valorTotal));
								listaRateioitem.add(rateioitem);
								adicionadoListaReteioItem = true;
								veiculodespesa.getListaveiculodespesaitem().remove(j);
						} 
					}
					if (!adicionadoListaReteioItem) {
							rateioitem = new Rateioitem();
							rateioitem.setCentrocusto(veiculodespesa.getCentrocusto());
							rateioitem.setProjeto(veiculodespesaitem1.getProjeto());
							rateioitem.setValor(veiculodespesaitem1.calculaValorTotal());
							listaRateioitem.add(rateioitem);
					}
				}
			} else if(veiculodespesa.getVeiculodespesadefinirprojeto() == null || (veiculodespesa.getVeiculodespesadefinirprojeto() != null && veiculodespesa.getVeiculodespesadefinirprojeto().equals(Veiculodespesadefinirprojeto.POR_DESPESA_DO_VEICULO))) {
				rateioitem = new Rateioitem();
				rateioitem.setCentrocusto(veiculodespesa.getCentrocusto());
				rateioitem.setProjeto(veiculodespesa.getProjeto());
				rateioitem.setValor(veiculodespesa.getValortotal());
				listaRateioitem.add(rateioitem);
			}
			
			valortotal = valortotal.add(veiculodespesa.getValortotal());			
		}
		
		bean.setValor(valortotal);
		
		for (Rateioitem item : listaRateioitem) {
			item.setPercentual(entregaService.calculaPercentual(bean.getValor(), item.getValor()));
		}
		
		entregaService.reCalculaPercentual(listaRateioitem);
//		rateioitemService.agruparItensRateio(listaRateioitem);
		
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(listaRateioitem);
		
		rateioService.calculaValoresRateio(rateio, bean.getValor());
		
		bean.setRateio(rateio);

		bean.setFornecedor(f);
		bean.setDtemissao(new Date(System.currentTimeMillis()));
//		bean.setDtvencimento(fc.getDtproximovencimento());
//		bean.setPrazo(fc.getPrazopagamento());
//		bean.setFinanciamento(true);
		bean.setReferencia(Referencia.MES_ANO_CORRENTE);
		bean.setDocumentoacao(Documentoacao.PREVISTA);
		bean.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		bean.setFromDespesaVeiculo(Boolean.TRUE);
		bean.setController("/veiculo/crud/Veiculodespesa" + ("true".equals(request.getParameter("ENTRADA")) ? "?ACAO=consultar&cdveiculodespesa=" + bean.getIdsVeiculosDespesa() : ""));
		
		return this.doEntrada(request, bean);
	}

	/**
	 * M�todo chamado do Crud Entrega. Chamando quando for faturar uma entrega
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioService#calculaValoresRateio(Rateio, Money)
	 * @param request
	 * @param documento
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	@Action("doFaturar")
	public ModelAndView fromEntrega(WebRequestContext request, Documento documento) throws Exception {
		documento = (Documento) request.getSession().getAttribute("documento");
		request.getSession().removeAttribute("documento");
		
		rateioService.calculaValoresRateio(documento.getRateio(), documento.getValor());
		
		return continueOnAction("entrada", documento);
	}
	
	/**
	 * A��o vindo da tela de despesa de viagem gerando adiantamento.
	 *
	 * @param request
	 * @param documento
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@Action("fromDespesaViagemAdiantamento")
	public ModelAndView fromDespesaViagemAdiantamento(WebRequestContext request, Documento documento) throws Exception {
		if (documento == null || documento.getWhereInDespesaviagem() == null || documento.getWhereInDespesaviagem().equals("")) {
			request.addError("Despesa de viagem n�o pode ser nulo.");
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
		}				
		
		if("entrada".equals(documento.getController())){
			documento.setController("/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+documento.getWhereInDespesaviagem());
		} else {
			documento.setController("/financeiro/crud/Despesaviagem");
		}
		
		Money valortotal = new Money();
		Colaborador colaborador = null;
		Empresa empresa = null;
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.findForGerarAcerto(documento.getWhereInDespesaviagem());
		if(listaDespesaviagem != null && !listaDespesaviagem.isEmpty()){
			for(Despesaviagem dv : listaDespesaviagem){
				if(colaborador == null) colaborador = dv.getColaborador();
				if(empresa == null) empresa = dv.getEmpresa();
				
				valortotal = valortotal.add(dv.getTotalprevisto());
			}
		}
		documento.setValor(valortotal);
		
		Rateio rateio = despesaviagemService.criaRateioForAtualizarvalores(documento.getValor(), listaDespesaviagem, true, false);
		rateio.setValortotaux(valortotal.toString());
		rateio.setValoraux(valortotal.toString());
		
		rateioService.atualizaValorRateio(rateio, valortotal);
		
		documento.setRateio(rateio);
		
		documento.setEmpresa(empresa);
		documento.setColaborador(colaborador);
		documento.setTipopagamento(Tipopagamento.COLABORADOR);
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		documento.setAdiatamentodespesa(true);
		documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDescricao("Adiantamento referente � despesa de viagem " + documento.getWhereInDespesaviagem());
		
		Date dataAtual = new Date(System.currentTimeMillis());
		documento.setDtemissao(dataAtual);
		documento.setDtvencimento(dataAtual);
		
		return continueOnAction("entrada", documento);
	}
	
	/**
	 * A��o vindo da tela de despesa de viagem gerando acerto.
	 *
	 * @param request
	 * @param documento
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@Action("fromDespesaViagemAcerto")
	public ModelAndView fromDespesaViagemAcerto(WebRequestContext request, Documento documento) throws Exception {
		if (documento == null || documento.getWhereInDespesaviagem() == null || documento.getWhereInDespesaviagem().equals("")) {
			request.addError("Despesa de viagem n�o pode ser nulo.");
			return new ModelAndView("redirect:/financeiro/crud/Despesaviagem");
		}	
		
		if("entrada".equals(documento.getController())){
			documento.setController("/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+documento.getWhereInDespesaviagem());
		} else {
			documento.setController("/financeiro/crud/Despesaviagem");
		}
		
		Colaborador colaborador = null;
		Empresa empresa = null;
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.findForGerarAcerto(documento.getWhereInDespesaviagem());
		if(listaDespesaviagem != null && !listaDespesaviagem.isEmpty()){
			for(Despesaviagem dv : listaDespesaviagem){
				if(colaborador == null) colaborador = dv.getColaborador();
				if(empresa == null) empresa = dv.getEmpresa();
			}
		}
		
		documento.setEmpresa(empresa);
		documento.setColaborador(colaborador);
		documento.setTipopagamento(Tipopagamento.COLABORADOR);
		documento.setDocumentoacao(Documentoacao.PREVISTA);
		if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.NOTACOMPRA_GERA_CONTADEFINITIVA))){
			documento.setDocumentoacao(Documentoacao.DEFINITIVA);
		}
		documento.setAdiatamentodespesa(false);
		documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDescricao("Acerto referente � despesa de viagem " + documento.getWhereInDespesaviagem());
		
		Rateio rateio = despesaviagemService.criaRateioForAtualizarvalores(documento.getValor(), listaDespesaviagem, false, false);
		rateio.setValortotaux(documento.getValor().toString());
		rateio.setValoraux(documento.getValor().toString());
		documento.setRateio(rateio);
		
		if(rateio != null && SinedUtil.isListNotEmpty(rateio.getListaRateioitem())){
			for(Rateioitem rateioitem : rateio.getListaRateioitem()){
				if(rateioitem.getValor() == null && rateioitem.getPercentual() != null){
					rateioitem.setValor(documento.getValor().multiply(new Money(rateioitem.getPercentual()).divide(new Money(100))));
				}
			}
		}
		
		Date dataAtual = new Date(System.currentTimeMillis());
		documento.setDtemissao(dataAtual);
		documento.setDtvencimento(dataAtual);
		return continueOnAction("entrada", documento);
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Documento form)	throws CrudException {
		String whereInOrdemcompra = request.getParameter("whereInOrdemcompra");
		String isCompensacaoSaldo = request.getParameter("compensar");
		String controller = "";
		
		if(whereInOrdemcompra != null && !whereInOrdemcompra.equals("")){
			Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
			controller = "/suprimento/crud/Ordemcompra" + (entrada ? "?ACAO=consultar&cdordemcompra=" + whereInOrdemcompra : "");
			Ordemcompra ordemcompra = ordemcompraService.load(new Ordemcompra(Integer.parseInt(whereInOrdemcompra)), "ordemcompra.cdordemcompra, ordemcompra.ordemcompraantecipada");
			
			List<Documentoorigem> listaDocOrigem = documentoorigemService.findByOrdemcompra(ordemcompra.getCdordemcompra().toString());
			if((ordemcompra.getOrdemcompraantecipada()!= null && ordemcompra.getOrdemcompraantecipada() && SinedUtil.isListEmpty(listaDocOrigem)) ||
					documentoorigemService.existeDocumentosNaoCancelado(listaDocOrigem)){
				request.addError("A Ordem de compra j� foi antecipada.");
				SinedUtil.redirecionamento(request, controller);
				return null;
			}
		}
		
		if((isCompensacaoSaldo != null && isCompensacaoSaldo.equals("true"))
				|| (form.getIsCompensarSaldo() != null && form.getIsCompensarSaldo().equals(Boolean.TRUE))) {
			String whereIn = SinedUtil.getItensSelecionados(request);
			
			Documentoclasse documentoClasse = DocumentoclasseService.getInstance().load(Documentoclasse.OBJ_RECEBER);
			List<Documento> listaDocumentosACompensar = documentoService.findForCompensarSaldo(whereIn);
			
			try {
				documentoService.verificarDocumentosAntesDeCompensar(listaDocumentosACompensar, documentoClasse);
			} catch(SinedException e) {
				request.addError(e.getMessage());
				return new ModelAndView("redirect:/financeiro/crud/Contareceber");
			}
		}
		
		return super.doCriar(request, form);
	}

	@Override
	protected Documento criar(WebRequestContext request, Documento form) throws Exception {
		String cdAgendamento = request.getParameter("cdAgendamento");
		Documento documento = null;
		Boolean gerarAntecipacaoOrdemcompra = Boolean.valueOf(request.getParameter("gerarAntecipacaoOrdemcompra") != null ? 
				request.getParameter("gerarAntecipacaoOrdemcompra") : "false");
		Boolean gerarContaByDevolucaoCheque = Boolean.valueOf(request.getParameter("gerarContaByDevolucaoCheque") != null ? 
				request.getParameter("gerarContaByDevolucaoCheque") : "false");
		Boolean compensacaoSaldo = Boolean.valueOf(request.getParameter("compensar") != null && !request.getParameter("compensar").equals("") ? 
				request.getParameter("compensar") : "false");
		
		if("true".equals(request.getParameter("copiar")) && form.getCddocumento() != null){
			documento = documentoService.criarCopia(form);
			form.setDocumentoacao(Documentoacao.PREVISTA);
		} else if(gerarAntecipacaoOrdemcompra){
			documento = ordemcompraService.criarAntecipacaoOrdemcompra(request);
		} else if (StringUtils.isEmpty(cdAgendamento)) {
			Money valor = form.getValor();
			Date dtemissao = form.getDtemissao();
			Empresa empresa = form.getEmpresa();
			Tipopagamento tipopagamento = form.getTipopagamento();
			String descricao = form.getDescricao();
			Pessoa pessoa = form.getPessoa();
			Contagerencial contagerencial = form.getContagerencial();
			if(contagerencial != null && contagerencial.getCdcontagerencial() != null){
				contagerencial = contagerencialService.loadWithIdentificador(contagerencial);
			}
			Centrocusto centrocusto = form.getCentrocusto();
			Fornecedor fornecedor = null;
			if(pessoa != null && tipopagamento != null && tipopagamento.equals(Tipopagamento.FORNECEDOR)){
				fornecedor = new Fornecedor(pessoa.getCdpessoa());
			}
			
			documento = super.criar(request, form);
			documento.setReferencia(Referencia.MES_ANO_CORRENTE);
			documento.setDocumentoacao(Documentoacao.DEFINITIVA);
			documento.setValor(valor);
			documento.setDtemissao(dtemissao);
			if(empresa != null) { 
				documento.setEmpresa(empresa);
			}
			documento.setTipopagamento(tipopagamento);
			documento.setPessoa(pessoa);
			documento.setFornecedor(fornecedor);
			documento.setDescricao(descricao);
			
			if(compensacaoSaldo) { 
				String whereIn = SinedUtil.getItensSelecionados(request);
				if(StringUtils.isNotBlank(whereIn)){
					List<Documento> listaDocumentosACompensar = documentoService.findForCompensarSaldo(whereIn);
					
					documentoService.preencherDocumentoCompensacao(listaDocumentosACompensar, documento);
				}
			}
			
			//Sempre inicia com um rateio com percentual 100%
			Rateioitem rateioitem = new Rateioitem();
			rateioitem.setPercentual(100.0);
			rateioitem.setValor(documento.getValor());
			rateioitem.setContagerencial(contagerencial);
			rateioitem.setCentrocusto(centrocusto);
			
			if(gerarContaByDevolucaoCheque != null && gerarContaByDevolucaoCheque){
				documentoService.setInfDevolucaoCheque(request, documento);
				if(documento.getValor() != null){
					rateioitem.setValor(documento.getValor());
				}
			}
			
			if (documento.getRateio() == null)
				documento.setRateio(new Rateio());
			if (documento.getRateio().getListaRateioitem() == null)
				documento.getRateio().setListaRateioitem(new ListSet<Rateioitem>(Rateioitem.class));
			documento.getRateio().getListaRateioitem().add(rateioitem);
			
		} else { //Procedimento de consolidar agendamento:
			Agendamento agendamento = agendamentoService.carregarParaConsolidar(Integer.parseInt(cdAgendamento));
			documento = documentoService.gerarDocumento(agendamento,Documentoclasse.CD_PAGAR);
			rateioService.calculaValoresRateio(documento.getRateio(), documento.getValor());
			documento.setCdAgendamento(agendamento.getCdagendamento());
			request.setAttribute("clearBase", true);
		}
		
		String cdNota = request.getParameter("cdNota");
		if(StringUtils.isNotBlank(cdNota)){
			documento.setNota(new Nota(Integer.valueOf(cdNota)));
		}
		
		String cdcoletaParam = request.getParameter("cdcoleta");
		if(cdcoletaParam != null && !"".equals(cdcoletaParam)){
			Coleta coleta = coletaService.loadForPagamento(new Coleta(Integer.parseInt(cdcoletaParam)));
			
			Money valor = new Money();
			List<ColetaMaterial> listaColetaMaterial = coleta.getListaColetaMaterial();
			if(listaColetaMaterial != null && listaColetaMaterial.size() > 0){
				for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
					Double valorunitario = coletaMaterial.getValorunitario();
					if(valorunitario != null && valorunitario > 0){
						Double quantidade = coletaMaterial.getQuantidade();
						Double quantidadedevolvida = coletaMaterial.getQuantidadedevolvida();
						
						if(quantidade == null) quantidade = 0d;
						if(quantidadedevolvida == null) quantidadedevolvida = 0d;
						
						valor = valor.add(new Money((quantidade - quantidadedevolvida) * valorunitario));
					}
				}
			}
			
			if(coleta.getFornecedor() != null){
				documento.setTipopagamento(Tipopagamento.FORNECEDOR);
				documento.setFornecedor(coleta.getFornecedor());
			}else if(coleta.getCliente() != null){
				documento.setTipopagamento(Tipopagamento.CLIENTE);
				documento.setCliente(coleta.getCliente());
			}
			documento.setDtemissao(SinedDateUtils.currentDate());
			documento.setValor(valor);
			documento.setColeta(coleta);
			documento.setEmpresa(coleta.getEmpresa());
			documento.setDescricao("Pagamento referente � coleta: " + coleta.getCdcoleta());
			
			Rateio rateio = new Rateio();
			List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
			
			Rateioitem rateioitem = new Rateioitem();
			if(coleta.getFornecedor() != null && coleta.getFornecedor().getContagerencial() != null){
				rateioitem.setContagerencial(coleta.getFornecedor().getContagerencial());
			}
			rateioitem.setPercentual(100d);
			rateioitem.setValor(valor);
			listaRateioitem.add(rateioitem);
			
			rateio.setListaRateioitem(listaRateioitem);
			documento.setRateio(rateio);
		}
		
		documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		return documento;
	}

	@Override
	protected void validateBean(Documento bean, BindException errors) {

		//VALIDA OS JUROS
		taxaService.validateTaxas(bean, errors);

		//VALIDA O RATEIO
		try {
			rateioService.validateRateio(bean.getRateio(), bean.getValor());
		} catch (SinedException e) {
			errors.reject("001",e.getMessage());
		}

		//VERIFICA PERIODO DE DATAS
		Date dtemissao = bean.getDtemissao();
		if(dtemissao!= null){
			if(dtemissao.after(bean.getDtvencimento())){
				errors.reject("001","A data de emiss�o deve ser anterior � data de vencimento.");
			}else if(Boolean.TRUE.equals(bean.getFinanciamento()) && SinedUtil.isListNotEmpty(bean.getListaDocumento())){
				for(Documento beanFinanciamento : bean.getListaDocumento()){
					if(beanFinanciamento.getDtvencimento() != null && dtemissao.after(beanFinanciamento.getDtvencimento())){
						errors.reject("001","A data de emiss�o deve ser anterior � data de vencimento da parcela.");
						break;
					}
				}
			}
		}
		
		documentoService.validateDocumento(bean,errors);
		
		if(bean.getCheque() != null && bean.getCheque().getCdcheque() != null){
			Money valorcheque = chequeService.getValor(bean.getCheque());
			Money valortotalcontas = contapagarService.getTotalDocumentoByCheque(bean, bean.getCheque());
			valortotalcontas = valortotalcontas.add(bean.getValor());
			if(valorcheque == null || valortotalcontas.getValue().compareTo(valorcheque.getValue()) > 0){
				errors.reject("001","O valor do(s) documento(s) n�o pode ser maior que o valor do cheque.");
			}
		}
	}



	/**
	 * M�todo respons�vel por gerar a lista de financiamento do documento via ajax.
	 * 
	 * @param request
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	@Action("geraFinanciamento")
	public void geraFinanciamento(WebRequestContext request, Documento documento){
		List<Documento> listaDocumento = documentoService.geraFinanciamento(documento);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().convertObjectToJs(listaDocumento, "listaDocFinanciado");
	}
	
	/**
	 * Ajax para obter o n�mero de itens de um prazo de pagamento
	 * 
	 * @see br.com.linkcom.sined.geral.service.PrazopagamentoService#countItens(Prazopagamento)
	 * @param request
	 * @param documento
	 * @author Hugo Ferreira
	 */
	public void ajaxCountPrazoItem(WebRequestContext request, Documento documento) {
		Long count = prazopagamentoService.countItens(documento.getPrazo());
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var count = " + count.toString());
	}

	/**
	 * M�todo para carregar jsp com a informa��o do documento a ser visualizado no pop-up.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#findForVisualizacao(Documentohistorico)
	 * @param request
	 * @param documentohistorico
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("visualizaConta")
	public ModelAndView visualizarDocumento(WebRequestContext request, Documentohistorico documentohistorico){
		if(documentohistorico == null)
			throw new SinedException("N�o foi poss�vel encontrar o documento em nosso sistema.");

		request.setAttribute("TEMPLATE_beanName", documentohistorico);
		request.setAttribute("TEMPLATE_beanClass", Documentohistorico.class);
		request.setAttribute("descricao", "Conta a pagar");
		request.setAttribute("consultar", true);

		documentohistorico = documentohistoricoService.findForVisualizacao(documentohistorico);

		return new ModelAndView("direct:crud/popup/visualizacaoDocumento").addObject("documentohistorico", documentohistorico);
	}
	
	/**
	 * M�todo para carregar uma conta a pagar para ser exibida na tela de Gerar Receita.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#loadForReceita(Documento)
	 * @param request
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void ajaxCarregaDocumento(WebRequestContext request, Documento documento){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		try{
			documento = documentoService.loadForReceita(documento);
			if(StringUtils.isBlank(documento.getOutrospagamento()) && documento.getPessoa() != null){
				documento.setOutrospagamento(documento.getPessoa().getNome());
				documento.setPessoa(null);
			}
			view.convertObjectToJs(documento,"doc");
			view.println("var success = true;");
		}catch(Exception e){
			view.println("var success = false;");
		}
	}
	
	/**
	 * M�todo para deletar um documento criado na tela de Gerar Receita.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#delete(Documento)
	 * @param request
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	@Action("deleteDocumento")
	public void deleteDocumento(WebRequestContext request, Documento documento){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		try {
			documentoService.delete(documento);
			view.println("var success = true;");
		} catch (Exception e) {
			view.println("var success = false;");
			view.println("var erro = " + e.getMessage() + ";");
		}
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,Documento form) {
		return new ModelAndView("crud/contapagarEntrada");
	}

	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, DocumentoFiltro filtro) {
		return new ModelAndView("crud/contapagarListagem");
	}
	
	/**
	 * Verifica se existe projeto vinculado ao rateio e se este primeiro possui CNPJ.
	 * @param request
	 * @param clientehistorico
	 * @author Taidson
	 * @since 19/07/2010
	 */
	public void ajaxProjetoCNPJ(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		String msg = ""; 
		String verificarSituacao = "false";
		if(parametrogeralService.getBoolean(Parametrogeral.RECIBO_APENASBAIXADAS)){
			if(documentoService.validaDocumentoSituacao(whereIn, true, Documentoacao.BAIXADA, Documentoacao.BAIXADA_PARCIAL)){
				msg = "S� � permitido emitir recibo para contas com as situa��es Baixada ou Baixada Parcialmente.";
				verificarSituacao = "true";
			}
		}
		if((!"true".equals(verificarSituacao)) &&
			documentoService.isDocumentoNotBaixadaOrDefinitiva(whereIn)){
			verificarSituacao = "true";
			msg = "S� pode emitir recibo de uma conta a pagar que estiver diferente da situa��o Cancelada e N�o Autorizada.";
		}
		
		Boolean existeProjetoCNPJ = null;
		existeProjetoCNPJ = documentoService.haveProjetoCNPJ(whereIn);
		View.getCurrent().println("var verificarSituacao = '" + verificarSituacao + "';");
		View.getCurrent().println("var existeProjetoCNPJ = '" + Util.strings.toStringIdStyled(existeProjetoCNPJ, false)+"';");
		View.getCurrent().println("var msg = '" + msg + "';");
	}
	
	/**
	 * Action que lista as contas a pagar da pessoa passada por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#findByPessoaDocumentoclasse(Pessoa pessoa, Documentoclasse classe, boolean listagemTotal)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#ajustaNumeroParcelaDocumento(Documento documento)
	 *
	 * @param request
	 * @param pessoa
	 * @return
	 * @since 10/11/2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView listarContas(WebRequestContext request, Pessoa pessoa){
		
		String param = request.getParameter("listagemTotal");
		boolean listagemTotal = param != null && !param.equals("") && param.toLowerCase().equals("true");
		
		List<Documento> listaDocumento = documentoService.findByPessoaDocumentoclasse(pessoa, Documentoclasse.OBJ_PAGAR, listagemTotal);
		for (Documento documento : listaDocumento) {
			documentoService.ajustaNumeroParcelaDocumento(documento);
		}
		
		request.setAttribute("listaDocumento", listaDocumento);
		request.setAttribute("tamanhoLista", listaDocumento.size());
		request.setAttribute("cdpessoa", pessoa.getCdpessoa());
		request.setAttribute("listagemTotal", listagemTotal);
		request.setAttribute("titulo", "CONTAS A PAGAR");
		request.setAttribute("controller", "Contapagar");
		
		request.setAttribute("cddocumentoclasse", Documentoclasse.CD_PAGAR);
		
		return new ModelAndView("direct:crud/popup/listagemDocumento");
	}
	
	/**
	 * M�todo para verificar limite de or�amento no m�s das Contas Gerenciais
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void verificaLimiteMesContagerencial(WebRequestContext request){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		String msgm = contapagarService.verificaLimiteMesContagerencial(request);
		if(msgm != null && !"".equals(msgm))
			view.println("var msg = '" + msgm + "';");
		else
			view.println("var msg = '';");		
	}
	
	/**
	 * 
	 * M�todo que redireciona para o jsp do c�digo de barras para o mesmo ser convertido
	 *
	 * @name inserirCodigoBarras
	 * @param request
	 * @return
	 * @return ModelAndView
	 * @author Thiago Augusto
	 * @date 18/04/2012
	 *
	 */
	public ModelAndView inserirCodigoBarras(WebRequestContext request){
		return new ModelAndView("direct:crud/popup/codigobarrapopup");
	}
	
	/**
	 * M�todo que verifica se a conta a ser salva � uma duplicata de outra j� existente.
	 * @author Rafael Salvio
	 * @date 01/06/2012 
	 */
	public void ajaxVerificaDuplicidade(WebRequestContext request) throws Exception{
		Boolean isContaDuplicada = Boolean.FALSE;
		Boolean bloquearRepeticaoConta = Boolean.FALSE;

		try {
			Documento documento = new Documento();
			documento.setValor(new Money(request.getParameter("valor").replaceAll("\\.", "").replace(",", ".")));
			documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
			documento.setDtemissao(SinedDateUtils.stringToDate(request.getParameter("dtemissao")));
			documento.setDtvencimento(SinedDateUtils.stringToDate(request.getParameter("dtvencimento")));
			
			if(request.getParameter("cddocumento")!=null && !"".equals(request.getParameter("cddocumento")) ){
				documento.setCddocumento(Integer.parseInt(request.getParameter("cddocumento")));
			}
			
			if(request.getParameter("empresa")!=null && !"<null>".equals(request.getParameter("empresa"))){
				documento.setEmpresa(new Empresa(Integer.parseInt(request.getParameter("empresa"))));
			}
			
			switch (Integer.parseInt(request.getParameter("tipopagamento"))){
				case 0:{
					documento.setTipopagamento(Tipopagamento.CLIENTE);
					if (StringUtils.isNumeric(request.getParameter("pessoa")))
						documento.setCliente(new Cliente(Integer.parseInt(request.getParameter("pessoa"))));
					break;
				}
				case 1:{
					documento.setTipopagamento(Tipopagamento.COLABORADOR);
					if (StringUtils.isNumeric(request.getParameter("pessoa")))
						documento.setColaborador(new Colaborador(Integer.parseInt(request.getParameter("pessoa"))));
					break;
				}
				case 2:{
					documento.setTipopagamento(Tipopagamento.FORNECEDOR);
					if (StringUtils.isNumeric(request.getParameter("pessoa")))
						documento.setFornecedor(new Fornecedor(Integer.parseInt(request.getParameter("pessoa"))));
					break;
				}
				case 3:{
					documento.setTipopagamento(Tipopagamento.OUTROS);
					documento.setOutrospagamento(request.getParameter("pessoa"));
					break;
				}
			}
			
			isContaDuplicada = documentoService.verificaDuplicidade(documento);
		} catch (Exception e) {}
		
		try{
			if(isContaDuplicada != null && isContaDuplicada){
				String param = parametrogeralService.buscaValorPorNome(Parametrogeral.BLOQUEAR_REPETICAO_CONTA);
				if(param != null && !param.trim().isEmpty()){
					bloquearRepeticaoConta = Boolean.parseBoolean(param);
				}
			}
		} catch (Exception e) {}
		
		View.getCurrent().println("var isContaDuplicada = '"+isContaDuplicada.toString()+"';");
		View.getCurrent().println("var bloquearRepeticaoConta = '"+bloquearRepeticaoConta.toString()+"';");
	}
	
	/**
	 * M�todo que redireciona para registrar entrada fiscal. Caso o fornecedor n�o tenha servi�o relacionado, redireciona para o 
	 * cadastro do material 
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView registrarEntradaFiscal(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/financeiro/crud/Contapagar");
		}
		
//		Documento documento = contapagarService.loadForEntradaWtihDadosPessoa(new Documento(Integer.parseInt(cddocumento)));
		
		List<Documento> listDoc = contapagarService.findContas(whereIn);
		

		for (Documento documento: listDoc) {
			if(documento.getTipopagamento() == null || !Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento())){
				request.addError("S� � permitido registrar Entrada Fiscal de uma conta de fornecedor.");
				return new ModelAndView("redirect:/financeiro/crud/Contapagar" + (entrada ? "?ACAO=consultar&cddocumento="+whereIn : ""));
			}else{
				documento.ajustaBeanToLoad();
				documento.setFornecedor(FornecedorService.getInstance().loadReader(documento.getFornecedor()));
			}
			
			
			if(!listDoc.get(0).getFornecedor().getCdpessoa().equals(documento.getFornecedor().getCdpessoa())){
				request.addError("S� � permitido registrar Entrada Fiscal de contas do mesmo fornecedor.");
				return new ModelAndView("redirect:/financeiro/crud/Contapagar" + (entrada ? "?ACAO=consultar&cddocumento="+whereIn : ""));
			}
			if(listDoc.get(0).getEmpresa().getCdpessoa() != documento.getEmpresa().getCdpessoa()){
				request.addError("S� � permitido registrar Entrada Fiscal de contas da mesma empresa.");
				return new ModelAndView("redirect:/financeiro/crud/Contapagar" + (entrada ? "?ACAO=consultar&cddocumento="+whereIn : ""));
			}
			
			if(contapagarService.isOrigemEntregaOrEntradafiscalAvulsa(documento)){
				request.addError("S� � permitido registrar Entrada Fiscal de uma conta que n�o tem origem de Recebimento ou Entrada Fiscal.");
				return new ModelAndView("redirect:/financeiro/crud/Contapagar" + (entrada ? "?ACAO=consultar&cddocumento="+whereIn : ""));
			}
			if(entradafiscalService.existEntradafiscalByDocumento(documento)){
				request.addError("Existe uma entrada fiscal j� registrada para o documento.");
				return new ModelAndView("redirect:/financeiro/crud/Contapagar" + (entrada ? "?ACAO=consultar&cddocumento="+whereIn : ""));
			}
		}
		
		
		Documento doc = contapagarService.loadForRegistrarEntradafiscal(listDoc.get(0));
		List<Material> listaMaterial = materialService.findServicoByFornecedor(new Fornecedor(doc.getPessoa().getCdpessoa()), doc.getEmpresa());
		if(listaMaterial == null || listaMaterial.isEmpty()){
			return new ModelAndView("redirect:/suprimento/crud/Material?ACAO=criar&fromRegistrarentradafiscal=true&fromInsertOne=true&cddocumento=" + doc.getCddocumento() + "&cdfornecedor=" + doc.getPessoa().getCdpessoa() + "&nomefornecedor=" + doc.getPessoa().getNome()+"&whereIn="+whereIn );
		}
		
		return new ModelAndView("redirect:/fiscal/crud/Entradafiscal?ACAO=criar&fromRegistrarentradafiscal=true&cddocumento=" + whereIn);
	}
	
	public ModelAndView buscarOperacaocontabil(WebRequestContext request, Documento form) throws Exception {
		
		Operacaocontabil operacaocontabil = null;
		if (form.getCliente()!=null && form.getCliente().getCdpessoa()!=null){
			operacaocontabil = operacaocontabilService.load(form.getCliente(), "operacaocontabil");
		}else if (form.getFornecedor()!=null && form.getFornecedor().getCdpessoa()!=null){
			operacaocontabil = operacaocontabilService.load(form.getFornecedor(), "operacaocontabil");
		}
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("encontrou", operacaocontabil!=null);
		if (operacaocontabil!=null){
			jsonModelAndView.addObject("value", "br.com.linkcom.sined.geral.bean.Operacaocontabil[cdoperacaocontabil=" + operacaocontabil.getCdoperacaocontabil() + "]");			
			jsonModelAndView.addObject("label", operacaocontabil.getDescricaoAutocomplete());			
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView setSessaoEmpresa (WebRequestContext request, Empresa empresa){
		request.getSession().setAttribute(CONTA_PAGAR_EMPRESA, empresa.getCdpessoa());
		return new JsonModelAndView().addObject("success", true);
	
	}
	
	@Action("carregaVinculoByEmpresa")
	public void carregaVinculoByEmpresa(WebRequestContext request, Documento documento){
		List<Conta> listaVinculo = contaService.findVinculosAtivosByEmpresa(documento.getEmpresa());
		View.getCurrent().convertObjectToJs(listaVinculo, "lista");
	}
	
	public ModelAndView popupAtualizarRateio(WebRequestContext request){
		return new ModelAndView("direct:/crud/popup/atualizarRateioPopup")
			.addObject("selectedItens", request.getParameter("selectedItens"));
	}
	
	public ModelAndView exibirRateioAtualizado(WebRequestContext request, AtualizarRateioBean bean){
		List<Documento> lista = contapagarService.findForAtualizarRateio(bean.getSelectedItens());
		Map<Projeto, Double> mapa = null;
		
		if(bean.getTipo().equals("peso")){
			mapa = contapagarService.montaMapaProjetosByPeso();
		}else if(bean.getTipo().equals("apontamento")){
			mapa = contapagarService.montaMapaProjetosByApontamento(bean.getDtinicio(), bean.getDtfim());
		} 
		
		if(mapa != null && !mapa.isEmpty()){
			Double total = 0.;
			for(Double valor : mapa.values()){
				total += valor;
			}
			
			//Ajuste das porcentagens relativas ao total
			for(Projeto proj : mapa.keySet()){
				mapa.put(proj,mapa.get(proj)/total);
			}
			
			
			bean.setListaRateio(new ListSet<Rateio>(Rateio.class));
			for(Documento doc : lista){
				Rateio rateio = new Rateio();
				rateio.setCdrateio(doc.getRateio().getCdrateio());
				rateio.setDocumento(doc);
				rateio.setListaRateioitem(new ListSet<Rateioitem>(Rateioitem.class));
				
				Centrocusto cc = doc.getRateio().getListaRateioitem().get(0).getCentrocusto();
				Contagerencial cg = doc.getRateio().getListaRateioitem().get(0).getContagerencial();
				Boolean ccUnico= Boolean.TRUE;
				Boolean cgUnico = Boolean.TRUE;
				for(Rateioitem rateioitem : doc.getRateio().getListaRateioitem()){
					if(!rateioitem.getCentrocusto().equals(cc)){
						ccUnico = Boolean.FALSE;
					}
					if(!rateioitem.getContagerencial().equals(cg)){
						cgUnico = Boolean.FALSE;
					}
				}
				if(ccUnico){
					rateio.setCentrocusto(cc);
				}
				if(cgUnico){
					rateio.setContagerencial(cg);
				}
				
				BigDecimal somaValor = new BigDecimal(0);
				Double somaPercentual = 0.;
				for(Projeto proj : mapa.keySet()){
					Double percentual = mapa.get(proj);
					Rateioitem rateioitem = new Rateioitem();
					rateioitem.setProjeto(proj);
					rateioitem.setPercentual(new BigDecimal(percentual*100).setScale(2, RoundingMode.HALF_UP).doubleValue());
					rateioitem.setValor(new Money(doc.getValor().multiply(new Money(percentual))));
					somaValor = somaValor.add(rateioitem.getValor().getValue());
					somaPercentual += rateioitem.getPercentual();
					rateio.getListaRateioitem().add(rateioitem);
				}
				Rateioitem ultimorateioitem = rateio.getListaRateioitem().get(rateio.getListaRateioitem().size()-1);
				if(somaValor.compareTo(doc.getValor().getValue()) != 0){
					ultimorateioitem.setValor(new Money(ultimorateioitem.getValor().getValue().add(doc.getValor().getValue().subtract(somaValor))));
				}
				if(somaPercentual.compareTo(100.) != 0){
					ultimorateioitem.setPercentual(new BigDecimal(ultimorateioitem.getPercentual() + (100.-somaPercentual)).setScale(2, RoundingMode.HALF_UP).doubleValue());
				}
				
				bean.getListaRateio().add(rateio);
			}
		}
		
		List<Centrocusto> listaCentroCusto = null;
		return new ModelAndView("direct:/crud/popup/atualizarRateioPopup")
							.addObject("atualizarRateioBean", bean)
							.addObject("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras())
							.addObject("listaCentrocusto", centrocustoService.findAtivos(listaCentroCusto));
	}
	
	public void atualizarRateio(WebRequestContext request, AtualizarRateioBean bean){
		if(bean.getListaRateio() != null){
			StringBuilder erros = new StringBuilder();
			Integer sucesso = 0;
			for(Rateio rateio : bean.getListaRateio()){
				try{
					if(rateio.getListaRateioitem() == null || rateio.getListaRateioitem().isEmpty()
							|| rateio.getCentrocusto() == null || rateio.getContagerencial() == null){
						throw new Exception();
					}
					for(Rateioitem rateioitem : rateio.getListaRateioitem()){
						rateioitem.setRateio(rateio);
						rateioitem.setContagerencial(rateio.getContagerencial());
						rateioitem.setCentrocusto(rateio.getCentrocusto());
					}
					
					rateioService.saveOrUpdate(rateio);
					sucesso++;
				}catch (Exception e) {
					erros.append(rateio.getDocumento().getCddocumento()+",");
				}
			}
			if(erros.length() > 0){
				erros = erros.deleteCharAt(erros.length()-1);
				request.addError("Os seguintes rateios apresentaram erro ao serem atualizados: " + erros.toString());
				if(sucesso > 0)
					request.addMessage("Os Demais rateios foram atualizados com sucesso.");
			}else{
				request.addMessage("Rateios Atualizados com sucesso.");
			}
		}
		View.getCurrent().println("<script>parent.window.location.reload()</script>");
	}
	
	public ModelAndView ajaxBuscaInfDocumentotipo(WebRequestContext request, Documentotipo documentotipo) throws Exception {
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(documentotipo.getCddocumentotipo() != null){
			documentotipo = documentotipoService.load(documentotipo, "documentotipo.cddocumentotipo, documentotipo.tipotributo");
			jsonModelAndView.addObject("tipotributo", documentotipo != null && documentotipo.getTipotributo() != null ? ""+documentotipo.getTipotributo().ordinal() : null);
		}
		return jsonModelAndView;
	}
//	
	public void ajaxComboMunicipio(WebRequestContext request, Uf uf){
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		if (uf!=null && uf.getCduf()!=null){
			listaMunicipio = municipioService.findByUf(uf);			
		}
				
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaMunicipio, "listaMunicipio", ""));
	}
	
	public ModelAndView alterarTipoDocumento(WebRequestContext request) throws Exception {
		return documentoService.abrirPopupAlterarTipoDocumento(request);
	}	
	
	public ModelAndView salvarAlterarTipoDocumento(WebRequestContext request, DocumentoFiltro filtro) throws Exception {
		documentoService.salvarPopupAlterarTipoDocumento(request, filtro, "/financeiro/crud/Contapagar");
		filtro.setDocumentotipo(null);
		return null;
	}
	
	public ModelAndView ajaxTemplatesForCopiaCheque(WebRequestContext request){
		List<ReportTemplateBean> templates = reporttemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COPIA_CHEQUE_DOCUMENTO);
		
		return new JsonModelAndView()
				.addObject("cdtemplate", templates.size() == 1? templates.get(0).getCdreporttemplate(): null)
				.addObject("qtdeTemplates", templates.size());
	}
	
	public ModelAndView abrirSelecaoTemplateCopiaCheque(WebRequestContext request){
		return chequeService.abrirSelecaoTemplateCopiaChequeDocumento(request);
	}
	
	@Action("createRateioByModelo")
	public void createRateioByModelo(WebRequestContext request, Documento documento){
		if(documento.getRateiomodelo() != null && documento.getRateiomodelo().getCdrateiomodelo() != null){
			Rateio rateio = rateioService.createRateioByModelo(documento.getRateiomodelo());
			View.getCurrent().convertObjectToJs(rateio, "rateio");
		}
	}
	
	public void ajaxVerificaFeriado(WebRequestContext request){
		calendarioService.ajaxVerificaFeriado(request);
	}
	
	public void ajaxProximoDiaUtil(WebRequestContext request){
		calendarioService.ajaxProximoDiaUtil(request);
	}
	
	public ModelAndView criaContaByEntradafiscal(WebRequestContext request, Documento form) throws Exception{
		Object obj = request.getSession().getAttribute("documentoCriadoByEntradafiscal");
		request.getSession().removeAttribute("documentoCriadoByEntradafiscal");
		request.setAttribute("fromEntradafiscal", true);
		form = (Documento)obj;
		setInfoForTemplate(request, form);
		setEntradaDefaultInfo(request, form);
		entrada(request, form);
		return this.getEntradaModelAndView(request, form);
	}
	
	public void saveAssociacaoEntregadocumentoComDocumentoantecipado(String whereInEntregadocumento){
		List<Entregadocumento> listaEntregadocumento = entradafiscalService.findForFaturar(whereInEntregadocumento);
		for(Entregadocumento ed : listaEntregadocumento){
			if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
				for (Entregapagamento entregapagamento : ed.getListadocumento()) {
					if(entregapagamento.getDocumentoantecipacao() != null){
						Documentoorigem documentoorigem = new Documentoorigem();
						
						documentoorigem.setEntregadocumento(ed);
						documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
						documentoorigemService.saveOrUpdate(documentoorigem);
					}
				}
			}
		}
	}
	
	public void saveAssociacaoEntregaComDocumentoantecipado(String whereInEntrega){
		List<Entregadocumento> listaEntregadocumento = null;

		List<Entrega> listaEntrega = entregaService.listEntregaForFaturar(whereInEntrega);
		listaEntregadocumento = new ArrayList<Entregadocumento>();
		for(Entrega entrega: listaEntrega){
			listaEntregadocumento.addAll(entrega.getListaEntregadocumento());
		}

		for(Entregadocumento ed : listaEntregadocumento){
			if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
				for (Entregapagamento entregapagamento : ed.getListadocumento()) {
					if(entregapagamento.getDocumentoantecipacao() != null){
						Documentoorigem documentoorigem = new Documentoorigem();
						documentoorigem.setEntrega(ed.getEntrega());
						documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
						documentoorigemService.saveOrUpdate(documentoorigem);
					}
				}
			}
		}
	}
	
	public ModelAndView abrirRateio(WebRequestContext request){
		return rateioService.abrirRateio(request);
	}
	
	public ModelAndView ajaxIsApropriacao(WebRequestContext request, Documento documento){
		return documentoService.ajaxIsApropriacao(request, documento);
	}
	
	public void ajaxMontaApropriacoes(WebRequestContext request, Documento documento){
		documentoService.ajaxMontaApropriacoes(request, documento);
	}
}