package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemacao;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.geral.service.ChequehistoricoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContatipoService;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FormapagamentoService;
import br.com.linkcom.sined.geral.service.HistoricooperacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaohistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoorigemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.geral.service.TipooperacaoService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.OrigemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.MovimentacaoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ProtocoloRetiradaChequeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/financeiro/crud/Movimentacao", authorizationModule=CrudAuthorizationModule.class)
public class MovimentacaoCrud extends CrudControllerSined<MovimentacaoFiltro, Movimentacao, Movimentacao>{

	private MovimentacaoacaoService movimentacaoacaoService;
	private MovimentacaoService movimentacaoService;
	private AgendamentoService agendamentoService;
	private RateioService rateioService;
	private RateioitemService rateioitemService;
	private ContaService contaService;
	private ContatipoService contatipoService;
	private MovimentacaohistoricoService movimentacaohistoricoService;
	private MovimentacaoorigemService movimentacaoorigemService;
	private ProjetoService projetoService;
	private DespesaviagemService despesaviagemService;
	private DocumentoService documentoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private ChequehistoricoService chequehistoricoService;
	private CentrocustoService centrocustoService;
	private ChequeService chequeService;
	private ParametrogeralService parametrogeralService;
	private FormapagamentoService formapagamentoService;
	private TipooperacaoService tipooperacaoService;
	private EmpresaService empresaService;
	private ValecompraService valecompraService;
	private HistoricooperacaoService historicooperacaoService;
	
	public void setDespesaviagemService(DespesaviagemService despesaviagemService) {this.despesaviagemService = despesaviagemService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setMovimentacaoorigemService(MovimentacaoorigemService movimentacaoorigemService) {this.movimentacaoorigemService = movimentacaoorigemService;}
	public void setMovimentacaoacaoService(MovimentacaoacaoService movimentacaoacaoService) {this.movimentacaoacaoService = movimentacaoacaoService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setAgendamentoService(AgendamentoService agendamentoService) {this.agendamentoService = agendamentoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setContatipoService(ContatipoService contatipoService) {this.contatipoService = contatipoService;}
	public void setMovimentacaohistoricoService(MovimentacaohistoricoService movimentacaohistoricoService) {this.movimentacaohistoricoService = movimentacaohistoricoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setFechamentofinanceiroService(	FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService; }
	public void setChequehistoricoService(ChequehistoricoService chequehistoricoService) {this.chequehistoricoService = chequehistoricoService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setFormapagamentoService(FormapagamentoService formapagamentoService) {this.formapagamentoService = formapagamentoService;}
	public void setTipooperacaoService(TipooperacaoService tipooperacaoService) {this.tipooperacaoService = tipooperacaoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setHistoricooperacaoService(HistoricooperacaoService historicooperacaoService) {this.historicooperacaoService = historicooperacaoService;}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Movimentacao form)	throws CrudException {
		if(request.getAttribute("CONSULTAR") == null && request.getParameter("ACAO") != null && request.getParameter("ACAO").equals("editar") ){	
			if(fechamentofinanceiroService.verificaFechamento(form)){
				request.addError("Esta movimenta��o n�o pode ser editada pois refere-se a um per�odo j� fechado.");
				request.setAttribute("CONSULTAR", true);
				request.setLastAction("consultar");
				return super.doConsultar(request, form);
			}
		}
		return super.doEntrada(request, form);
	}
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Movimentacao form) throws Exception {
		String string = request.getParameter("conciliacao");
		if (string != null && !string.equals("")){
			form.setConciliacao(Boolean.parseBoolean(string));
		}
		List<Centrocusto> listaCentroCusto = null;
		
		if (form.getCdAgendamento() != null) 
			request.setAttribute("clearBase", true);

		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getCdmovimentacao() != null && form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
			List<Projeto> projetos = rateioitemService.getProjetos(form.getRateio().getListaRateioitem());
			listaProjeto = projetoService.findProjetosAbertosSemPermissao(projetos, true, true);
		} else {
			if(form.getWhereInDespesaviagem() != null && !"".equals(form.getWhereInDespesaviagem()) && 
					form.getRateio() != null && form.getRateio().getListaRateioitem() != null && !form.getRateio().getListaRateioitem().isEmpty()){
				List<Projeto> projetos = rateioitemService.getProjetos(form.getRateio().getListaRateioitem());
				listaProjeto = projetoService.findProjetosAbertos(projetos, true, true);
			}else {
				listaProjeto = projetoService.findProjetosAbertos(null, true, true);
			}
		}
		request.setAttribute("listaProjeto", listaProjeto);
		
		if(form.getTipooperacao() != null){
			if(form.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
				request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
			} else {
				request.setAttribute("tipooperacao", Tipooperacao.TIPO_CREDITO);
			}
		} else {
			request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
		}

		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		String whereInEmpresas = null;
		if(form.getCdmovimentacao() != null){
			if(form.getEmpresa() != null){
				form.setEmpresa(empresaService.load(form.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.nomefantasia"));
			}
			if(form.getRateio() != null && form.getRateio().getListaRateioitem() != null && !form.getRateio().getListaRateioitem().isEmpty()){
				listaCentroCusto = (List<Centrocusto>) CollectionsUtil.getListProperty(form.getRateio().getListaRateioitem(), "centrocusto");
			}
				
			form.setListaMovimentacaohistorico(new ListSet<Movimentacaohistorico>(Movimentacaohistorico.class,movimentacaohistoricoService.findByMovimentacao(form)));
			List<Movimentacaoorigem> listaOrigem = movimentacaoorigemService.findByMovimentacao(form);
			List<OrigemBean> listaBean = new ArrayList<OrigemBean>();
			OrigemBean bean = null;
			
			for (Movimentacaoorigem obj : listaOrigem) {
				bean = this.descibrirOrigem(form, obj);					
				listaBean.add(bean);
				
				if(obj.getDocumento() != null && obj.getDocumento().getEmpresa() != null && obj.getDocumento().getEmpresa().getCdpessoa() != null){
					listaEmpresa.add(obj.getDocumento().getEmpresa());
				}
			}
			
			if(listaEmpresa != null && listaEmpresa.size() >0){
				whereInEmpresas = CollectionsUtil.listAndConcatenate(listaEmpresa, "cdpessoa", ",");
			}
			request.setAttribute("listaOrigem", listaBean);
			
			if("true".equals(request.getParameter("fromArquivoBancario"))){
				// QUANDO VIER DA TELA DE PROCESSAMENTO DE ARQUIVO RETORNO SETAR O RATEIO E O VALOR DA MOVIMENTA��O
				Rateio rateio = (Rateio) request.getSession().getAttribute("rateioDocumentos");
				Money valor = (Money) request.getSession().getAttribute("valorTotalDocumentos");
				
				request.getSession().removeAttribute("rateioDocumentos");
				request.getSession().removeAttribute("valorTotalDocumentos");
				
				if (rateio != null) {
					form.setRateio(rateio);
				}
				if (valor != null) {
					form.setValor(valor);
				}
				movimentacaoService.saveOrUpdate(form);
				movimentacaoService.callProcedureAtualizaMovimentacao(form);
				request.setAttribute("fromArquivoBancario", Boolean.TRUE);
			}
		}
		
		if(request.getParameter(MultiActionController.ACTION_PARAMETER).equals(EDITAR)
				&& Movimentacaoacao.CONCILIADA.equals(form.getMovimentacaoacao())){
			throw new SinedException("N�o � poss�vel editar uma movimenta��o conciliada.");
		}
			
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		
		List<Formapagamento> listaFormapagamento = new ArrayList<Formapagamento>();
		if(form.getTipooperacao() != null && form.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
			listaFormapagamento = formapagamentoService.findCredito();
		}else if(form.getTipooperacao() != null && form.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){
			listaFormapagamento = formapagamentoService.findDebito();
		}
		
		List<Contatipo> listaContatipo = new ArrayList<Contatipo>();
		if(form.getFormapagamento() != null && form.getFormapagamento().getCdformapagamento() != null){
			form.getFormapagamento().setEmpresaWhereIn(whereInEmpresas);
			listaContatipo = contatipoService.findForMovimentacao(form.getFormapagamento());
		} 
		
		if(form.getConta() != null && form.getConta().getContatipo() != null && (listaContatipo == null || listaContatipo.isEmpty())){
			listaContatipo.add(form.getConta().getContatipo());
		}
		List<Conta> listaConta = new ArrayList<Conta>();
		if(form.getConta() != null && form.getConta().getContatipo() != null && form.getConta().getContatipo().getCdcontatipo() != null){
			listaConta = contaService.findByTipoAndEmpresa(whereInEmpresas, form.getConta().getContatipo());
		}
		
		if(whereInEmpresas != null)
			request.setAttribute("empresaWhereIn", whereInEmpresas);
		
		String bloquearRepeticaoConta = parametrogeralService.buscaValorPorNome(Parametrogeral.BLOQUEAR_REPETICAO_CONTA);
		if(bloquearRepeticaoConta == null || bloquearRepeticaoConta.trim().isEmpty()){
			bloquearRepeticaoConta = "false";
		}
		
		boolean transferencia = movimentacaoService.isTransferencia(form);
		boolean movTransferenciasNormais = false;
		
		if (transferencia){
			List<Movimentacao> listaMovimentacaoRelacionadaTransferencia = movimentacaoService.findMovimentacaoRelacionadaTransferencia(form, true, false);
			movTransferenciasNormais = movimentacaoService.verificaSituacao(listaMovimentacaoRelacionadaTransferencia, Movimentacaoacao.NORMAL);
			boolean movTransferenciaCheque = false;
			for (Movimentacao movimentacao: listaMovimentacaoRelacionadaTransferencia){
				if (movimentacao.getCheque()!=null){
					if(request.getParameter(MultiActionController.ACTION_PARAMETER).equals(EDITAR)){
						request.addError("N�o � poss�vel alterar esta movimenta��o financeira, pois a forma de pagamento est� relacionada ao tipo Cheque. � aconselh�vel cancelar as movimenta��es financeiras de origem e destino e refazer toda a opera��o.");
					}
					movTransferenciaCheque = true;
					break;
				}
			}
			
			if (!movTransferenciaCheque){
				request.setAttribute("exibeAlertaMovTransferencia", Boolean.TRUE);
			}
		}
		
		request.setAttribute("listaVinculo", listaConta);
		request.setAttribute("listaTipos", listaContatipo);
		request.setAttribute("listaFormapagamento", listaFormapagamento);
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(listaCentroCusto));
		request.setAttribute("integracao_contabil", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INTEGRACAO_CONTABIL)));
		request.setAttribute("bloquearRepeticaoConta", bloquearRepeticaoConta);
		request.setAttribute("listaTipooperacao", tipooperacaoService.findDebitoCredito());
		request.setAttribute("listaEmpresa", empresaService.getListaEmpresaByConta(form.getConta()));
		Integer empresaId = form.getEmpresa() != null ? form.getEmpresa().getCdpessoa() : null;
		if(empresaId == null && request.getSession().getAttribute("empresaSelecionada") != null){
			empresaId = ((Empresa) request.getSession().getAttribute("empresaSelecionada")).getCdpessoa();
		}
		request.setAttribute("empresaid", empresaId != null ? empresaId : "");
		request.setAttribute("transferencia", transferencia);
		request.setAttribute("movTransferenciasNormais", movTransferenciasNormais);
	}
	
	/**
	 * M�todo para descobrir qual a origem de uma movimenta��o.
	 * 
	 * @see #verificaOrigemAgendamento(Movimentacaoorigem)
	 * @see #verificaOrigemDocumento(Movimentacao, Movimentacaoorigem)
	 * @see #verificaOrigemTransferencia(Movimentacao, Movimentacaoorigem)
	 * @param movimentacao
	 * @param mvOrigem
	 * @return  OrigemBean - quando a origem foi detectada.
	 * 			null - se n�o encontrou origem.
	 * @author Fl�vio Tavares
	 */
	private OrigemBean descibrirOrigem(Movimentacao movimentacao, Movimentacaoorigem mvOrigem){
		OrigemBean bean = null;
		
		bean = this.verificaOrigemAgendamento(mvOrigem);
		if (bean != null) return bean;
		
		bean = this.verificaOrigemDocumento(movimentacao, mvOrigem);
		if (bean != null) return bean;
		
		bean = this.verificaOrigemTransferenciaInterna(movimentacao, mvOrigem);
		if(bean != null) return bean;
		
		bean = this.verificaOrigemTransferencia(movimentacao, mvOrigem);
		if(bean != null) return bean;
		
		bean = this.verificaOrigemDevolucao(movimentacao, mvOrigem);
		if(bean != null) return bean;
		
		bean = this.verificaOrigemDespesaviagemAdiantamento(movimentacao, mvOrigem);
		if(bean != null) return bean;
		
		bean = this.verificaOrigemDespesaviagemAcerto(movimentacao, mvOrigem);
		if(bean != null) return bean;
		
		bean = this.verificaOrigemCartaocredito(movimentacao, mvOrigem);
		if(bean != null) return bean;
		
		bean = new OrigemBean("CADASTRO");
		return bean; 
	}
	
	private OrigemBean verificaOrigemCartaocredito(Movimentacao movimentacao, Movimentacaoorigem mvOrigem) {
		OrigemBean bean = null;
		if (mvOrigem.getMovimentacaorelacionadacartaocredito() != null) {
			bean = new OrigemBean();
			bean.setTipo("");
			bean.setTypeorigem(OrigemBean.CONCILIACAOCARTAOCREDITO);
			bean.setDescricao("CONCILIA��O DE CART�O DE CR�DITO - " + mvOrigem.getMovimentacaorelacionadacartaocredito().getCdmovimentacao());
			bean.setId(mvOrigem.getMovimentacaorelacionadacartaocredito().getCdmovimentacao()+"");
		}
		return bean;
	}
	
	private OrigemBean verificaOrigemDespesaviagemAcerto(Movimentacao movimentacao, Movimentacaoorigem mvOrigem) {
		OrigemBean bean = null;
		if (mvOrigem.getDespesaviagemacerto() != null) {
			bean = new OrigemBean();
			bean.setTipo("");
			bean.setTypeorigem(OrigemBean.DESPESAVIAGEMACERTO);
			bean.setDescricao("DESPESA DE VIAGEM (ACERTO) - " + mvOrigem.getDespesaviagemacerto().getCddespesaviagem());
			bean.setId(mvOrigem.getDespesaviagemacerto().getCddespesaviagem()+"");
		}
		return bean;
	}
	
	private OrigemBean verificaOrigemDespesaviagemAdiantamento(Movimentacao movimentacao, Movimentacaoorigem mvOrigem) {
		OrigemBean bean = null;
		if (mvOrigem.getDespesaviagemadiantamento() != null) {
			bean = new OrigemBean();
			bean.setTipo("");
			bean.setTypeorigem(OrigemBean.DESPESAVIAGEMAADIANTAMENTO);
			bean.setDescricao("DESPESA DE VIAGEM (ADIANTAMENTO) - " + mvOrigem.getDespesaviagemadiantamento().getCddespesaviagem());
			bean.setId(mvOrigem.getDespesaviagemadiantamento().getCddespesaviagem()+"");
		}
		return bean;
	}
	
	/**
	* M�todo que verifica se a movimenta��o tem origem de devolu��o
	*
	* @param movimentacao
	* @param mvOrigem
	* @return
	* @since 13/10/2015
	* @author Luiz Fernando
	*/
	private OrigemBean verificaOrigemDevolucao(Movimentacao movimentacao, Movimentacaoorigem mvOrigem){
		OrigemBean bean = null;
		if (mvOrigem.getMovimentacaodevolucao() != null) {
			bean = new OrigemBean();
			bean.setTipo("");
			bean.setTypeorigem(OrigemBean.DEVOLUCAOCHEQUE);
			bean.setDescricao("DEVOLU��O DE CHEQUE");
			bean.setId(mvOrigem.getMovimentacaodevolucao().getCdmovimentacao()+"");
		}
		return bean;
	}
	
	private OrigemBean verificaOrigemTransferenciaInterna(Movimentacao movimentacao, Movimentacaoorigem mvOrigem){
		OrigemBean bean = null;
		if (mvOrigem.getMovimentacaorelacionada() != null && mvOrigem.getMovimentacaorelacionada().getCdmovimentacao() != null) {
			bean = new OrigemBean();
			bean.setTipo("");
			bean.setDescricao("TRANSFER�NCIA INTERNA - "+mvOrigem.getConta().getNome());
			bean.setId(mvOrigem.getMovimentacaorelacionada().getCdmovimentacao()+"");
			bean.setTypeorigem(OrigemBean.TRANSFERENCIA);
		}
		return bean;
	}
	
	/**
	 * Verifica se a origem da movimenta��o foi de uma transfer�ncia.
	 * 
	 * @param movimentacao
	 * @param mvOrigem
	 * @return
	 * @author Fl�vio Tavares
	 */
	private OrigemBean verificaOrigemTransferencia(Movimentacao movimentacao, Movimentacaoorigem mvOrigem){
		OrigemBean bean = null;
		if (mvOrigem.getConta() != null && mvOrigem.getConta().getNome() != null) {
			Movimentacao destino = movimentacaoService.findMovimentacaoDestino(movimentacao);
			if (destino != null) {
				bean = new OrigemBean();
				bean.setTipo("");
				if (movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
					bean.setDescricao("TRANSFER�NCIA PARA "+mvOrigem.getConta().getNome());
					bean.setId(destino.getCdmovimentacao()+"");
				} else {
					bean.setDescricao("TRANSFER�NCIA DE "+mvOrigem.getConta().getNome());
					bean.setId(destino.getCdmovimentacao()+"");
				}
				bean.setTypeorigem(OrigemBean.TRANSFERENCIA);
			}
		}
		return bean;
	}
	
	/**
	 * Verifica se a origem da movimenta��o foi em um documento.
	 * 
	 * @param movimentacao
	 * @param mvOrigem
	 * @return
	 * @author Fl�vio Tavares
	 */
	private OrigemBean  verificaOrigemDocumento(Movimentacao movimentacao, Movimentacaoorigem mvOrigem){
		OrigemBean bean = null;
		if (mvOrigem.getDocumento() != null && mvOrigem.getDocumento().getCddocumento() != null) {
			bean = new OrigemBean();
			if (movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)) {
				bean.setTipo("CONTA A PAGAR");
				bean.setTypeorigem(OrigemBean.CONTAPAGAR);
			} else {
				bean.setTipo("CONTA A RECEBER");
				bean.setTypeorigem(OrigemBean.CONTARECEBER);
			}
			bean.setId(mvOrigem.getDocumento().getCddocumento()+"");
			bean.setDescricao(mvOrigem.getDocumento().getDescricao());
			bean.setValor("R$" + mvOrigem.getDocumento().getAux_documento().getValoratual());
		}
		
		return bean;
	}
	
	/**
	 * Verifica se a origem da movimenta��o foi em uma agendamento.
	 * 
	 * @param mvOrigem
	 * @return
	 * @author Fl�vio Tavares
	 */
	private OrigemBean verificaOrigemAgendamento(Movimentacaoorigem mvOrigem){
		OrigemBean bean = null;
		if (mvOrigem.getAgendamento() != null && mvOrigem.getAgendamento().getCdagendamento() != null) {
			bean = new OrigemBean();
			bean.setTipo("AGENDAMENTO");
			bean.setId(mvOrigem.getAgendamento().getCdagendamento() != null ? mvOrigem.getAgendamento().getCdagendamento().toString() : "");
			bean.setDescricao(mvOrigem.getAgendamento().getDescricao());
			bean.setValor("R$" + mvOrigem.getAgendamento().getValor().toString());
			bean.setTypeorigem(OrigemBean.AGENDAMENTO);
		}
		return bean;
	}
	
	@Override
	protected Movimentacao carregar(WebRequestContext request, Movimentacao bean) throws Exception {
		Movimentacao movimentacao = super.carregar(request, bean);
		
		if(movimentacao == null || (movimentacao.getCdmovimentacao() != null && movimentacao.getConta() != null &&
				!SinedUtil.isUsuarioLogadoAdministrador() && !contaService.isUsuarioPermissaoConta(movimentacao.getConta()))){
			throw new SinedException("Registro n�o encontrado no sistema.");
		}

		movimentacao.getRateio().setListaRateioitem(rateioitemService.findByRateio(movimentacao.getRateio()));
		rateioService.calculaValoresRateio(movimentacao.getRateio(), movimentacao.getValor());
		
		List<Contatipo> listaContatipo = contatipoService.findAll();
		List<Conta> listaConta = contaService.findByTipo(movimentacao.getConta().getContatipo(),null);

		request.setAttribute("listaContatipo", listaContatipo);
		request.setAttribute("listaConta", listaConta);
		return movimentacao;
	}
	
	@Override
	protected Movimentacao criar(WebRequestContext request, Movimentacao form) throws Exception {
		form = super.criar(request, form);
		String cdAgendamento = request.getParameter("cdAgendamento");
		form.setDtmovimentacao(SinedDateUtils.currentDate());
		form.setTipooperacao(Tipooperacao.TIPO_DEBITO);
		if (StringUtils.isNotEmpty(cdAgendamento)) { //Procedimento de consolidar agendamento:
			Agendamento agendamento = agendamentoService.carregarParaConsolidar(Integer.parseInt(cdAgendamento));
			form = movimentacaoService.gerarMovimentacao(agendamento);
			rateioService.calculaValoresRateio(form.getRateio(), form.getValor());
			form.setCdAgendamento(agendamento.getCdagendamento());
			request.setAttribute("clearBase", true);
			request.setAttribute("tipooperacao", agendamento.getTipooperacao());
		}
		return form;
	}
	
	@Override
	protected void excluir(WebRequestContext request, Movimentacao bean) throws Exception {
		throw new SinedException("N�o � poss�vel excluir uma movimenta��o financeira.");
	}
	
	@Override
	protected ListagemResult<Movimentacao> getLista(WebRequestContext request,MovimentacaoFiltro filtro) {
		ListagemResult<Movimentacao> listResult = super.getLista(request, filtro);
		
		/* 
		 * SE A CONTA FOI INFORMADA NO FILTRO � NECESS�RIO CALCULAR OS VALORES DAS MOVIMENTA��ES COM BASE NO SALDO
		 * ATUAL DA CONTA, DEPENDENDO DO TIPO DE OPERA��O DA MOVIMENTA��O. 
		*/
		boolean show = filtro.getConta() != null;
		Money saldo = null;
		if(show){
			saldo = contaService.calculaSaldoAtual(filtro.getConta(), 
					SinedDateUtils.incrementDate(filtro.getDtperiodoDe()!=null ? filtro.getDtperiodoDe() : SinedDateUtils.currentDate(), -1, Calendar.DAY_OF_MONTH));
			
			for (Movimentacao mov : listResult.list()) {
				if(mov.getDtbanco() != null)
					mov.setDtmovimentacao(mov.getDtbanco());
				
				if(Tipooperacao.TIPO_CREDITO.equals(mov.getTipooperacao())){
					saldo = saldo.add(mov.getValor());
				}
				if(Tipooperacao.TIPO_DEBITO.equals(mov.getTipooperacao())){
					saldo = saldo.subtract(mov.getValor());
				}
				mov.setSaldo(saldo);
				
			}
		}
		
		
		List<Movimentacao> list = movimentacaoService.findForSomaListagem(filtro);
		Money credito = new Money();
		Money debito = new Money();
		
		filtro.setTotalCreditoContaGerencial(new Money());
		filtro.setTotalDebitoContaGerencial(new Money());
		filtro.setSaldoContaGerencial(new Money());
		filtro.setTotalCreditoCentroCusto(new Money());
		filtro.setTotalDebitoCentroCusto(new Money());
		filtro.setSaldoCentroCusto(new Money());
		filtro.setTotalCreditoProjeto(new Money());
		filtro.setTotalDebitoProjeto(new Money());
		filtro.setSaldoProjeto(new Money());		
		
		Money totalCreditoContaGerencial = new Money();
		Money totalDebitoContaGerencial = new Money();
		Money saldoContaGerencial = new Money();
		Money totalCreditoCentroCusto = new Money();
		Money totalDebitoCentroCusto = new Money();
		Money saldoCentroCusto = new Money();
		Money totalCreditoProjeto = new Money();
		Money totalDebitoProjeto = new Money();
		Money saldoProjeto = new Money();
		
		if(SinedUtil.isListNotEmpty(list)){
			
			for (Movimentacao mov : list) {
				if(Tipooperacao.TIPO_CREDITO.equals(mov.getTipooperacao())){
					credito = credito.add(mov.getValor());
				}
				if(Tipooperacao.TIPO_DEBITO.equals(mov.getTipooperacao())){
					debito = debito.add(mov.getValor());
				}
			}
			
			if(filtro.getContagerencial()!=null){
				for (Movimentacao mov : list){
					if(Tipooperacao.TIPO_CREDITO.equals(mov.getTipooperacao()) && mov.getRateio()!=null && SinedUtil.isListNotEmpty(mov.getRateio().getListaRateioitem())){
						for (Rateioitem item: mov.getRateio().getListaRateioitem()){
							if(item.getContagerencial().equals(filtro.getContagerencial()) && item.getValor() != null){
								totalCreditoContaGerencial = totalCreditoContaGerencial.add(item.getValor());
							}
						}
					}
					else if(Tipooperacao.TIPO_DEBITO.equals(mov.getTipooperacao()) && mov.getRateio()!=null && SinedUtil.isListNotEmpty(mov.getRateio().getListaRateioitem())){
						for (Rateioitem item: mov.getRateio().getListaRateioitem()){
							if(item.getContagerencial().equals(filtro.getContagerencial()) && item.getValor() != null){
								totalDebitoContaGerencial = totalDebitoContaGerencial.add(item.getValor());
							}
						}
					}
				}
				
				saldoContaGerencial = totalCreditoContaGerencial.subtract(totalDebitoContaGerencial);
				filtro.setTotalCreditoContaGerencial(totalCreditoContaGerencial);
				filtro.setTotalDebitoContaGerencial(totalDebitoContaGerencial);
				filtro.setSaldoContaGerencial(saldoContaGerencial);
			}
			
			if(filtro.getCentrocusto()!=null){
				for (Movimentacao mov : list){
					if(Tipooperacao.TIPO_CREDITO.equals(mov.getTipooperacao()) && mov.getRateio()!=null && SinedUtil.isListNotEmpty(mov.getRateio().getListaRateioitem())){
						for (Rateioitem item: mov.getRateio().getListaRateioitem()){
							if(item.getCentrocusto().equals(filtro.getCentrocusto()) && item.getValor() != null){
								totalCreditoCentroCusto = totalCreditoCentroCusto.add(item.getValor());
							}
						}
					}
					else if(Tipooperacao.TIPO_DEBITO.equals(mov.getTipooperacao()) && mov.getRateio()!=null && SinedUtil.isListNotEmpty(mov.getRateio().getListaRateioitem())){
						for (Rateioitem item: mov.getRateio().getListaRateioitem()){
							if(item.getCentrocusto().equals(filtro.getCentrocusto()) && item.getValor() != null){
								totalDebitoCentroCusto = totalDebitoCentroCusto.add(item.getValor());
							}
						}
					}
				}
				saldoCentroCusto = totalCreditoCentroCusto.subtract(totalDebitoCentroCusto);
				filtro.setTotalCreditoCentroCusto(totalCreditoCentroCusto);
				filtro.setTotalDebitoCentroCusto(totalDebitoCentroCusto);
				filtro.setSaldoCentroCusto(saldoCentroCusto);
			}
			
			if(filtro.getProjeto()!=null){
				for (Movimentacao mov : list){
					if(Tipooperacao.TIPO_CREDITO.equals(mov.getTipooperacao()) && mov.getRateio()!=null && SinedUtil.isListNotEmpty(mov.getRateio().getListaRateioitem())){
						for (Rateioitem item: mov.getRateio().getListaRateioitem()){
							if(item.getProjeto().equals(filtro.getProjeto())){
								totalCreditoProjeto = totalCreditoProjeto.add(item.getValor());
							}
						}
					}
					else if(Tipooperacao.TIPO_DEBITO.equals(mov.getTipooperacao())&& mov.getRateio()!=null && SinedUtil.isListNotEmpty(mov.getRateio().getListaRateioitem())){
						for (Rateioitem item: mov.getRateio().getListaRateioitem()){
							if(item.getProjeto().equals(filtro.getProjeto())){
								totalDebitoProjeto = totalDebitoProjeto.add(item.getValor());
							}
						}
					}
				}
				saldoProjeto = totalCreditoProjeto.subtract(totalDebitoProjeto);
				filtro.setTotalCreditoProjeto(totalCreditoProjeto);
				filtro.setTotalDebitoProjeto(totalDebitoProjeto);
				filtro.setSaldoProjeto(saldoProjeto);
			}

		}
		filtro.setCredito(credito);
		filtro.setDebito(debito);
		filtro.setSaldo(SinedUtil.formatMoney(credito.subtract(debito)));
		
		request.setAttribute("showSaldo", show);
		return listResult;
	}
	
	@Override
	protected void listagem(WebRequestContext request, MovimentacaoFiltro filtro) throws Exception {
		
		List<NaturezaContagerencial> listaNaturezaContagerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContagerencial.add(NaturezaContagerencial.CONTA_GERENCIAL);
		listaNaturezaContagerencial.add(NaturezaContagerencial.OUTRAS);
		
		
		Collections.sort(listaNaturezaContagerencial, new Comparator<NaturezaContagerencial>() {
			@Override
			public int compare(NaturezaContagerencial o1, NaturezaContagerencial o2) {
				return o1.getCdsped().compareToIgnoreCase(o2.getCdsped());
			}
		});
		
		List<Formapagamento> listaFormaPagamento = formapagamentoService.findCreditoAndDebito();
		List<Movimentacaoacao> listaAcao = movimentacaoacaoService.createListaForChecklist();
		
		request.setAttribute("listaAcaoCompleta", listaAcao);
		request.setAttribute("listaFormaPagamento", listaFormaPagamento);
		request.setAttribute("firstDate", SinedDateUtils.toString(SinedDateUtils.firstDateOfMonth()));
		request.setAttribute("listaNaturezaContagerencial", listaNaturezaContagerencial);
		request.setAttribute("whereInNaturezaContagerencialMovimentacao", CollectionsUtil.listAndConcatenate(listaNaturezaContagerencial, "value", ","));
		request.setAttribute("listaTipooperacao", tipooperacaoService.findDebitoCredito());
	}
	
	@Override
	protected void validateBean(Movimentacao bean, BindException errors) {
		if(bean.getHistorico() == null){
			errors.reject("001", "O campo Hist�rico � obrigat�rio.");
		}
		
		movimentacaoService.validateMovimentacao(errors, bean);
		
		if (movimentacaoService.isTransferencia(bean)){
			List<Movimentacao> listaMovimentacaoTransferenciaRelacionada = movimentacaoService.findMovimentacaoRelacionadaTransferencia(bean, false, false);
			for (Movimentacao movimentacaoRelacionada: listaMovimentacaoTransferenciaRelacionada){
				movimentacaoRelacionada.setTransferencia(true);
				movimentacaoRelacionada.setValor(bean.getValor());
				movimentacaoRelacionada.setDtmovimentacao(bean.getDtmovimentacao());
				movimentacaoService.validateMovimentacao(errors, movimentacaoRelacionada);
			}
		}
		try {
			rateioService.validateRateio(bean.getRateio(), bean.getValor());
		} catch (SinedException e) {
			errors.reject("001",e.getMessage());
//			e.printStackTrace();
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Movimentacao form) throws CrudException {
		if(fechamentofinanceiroService.verificaFechamento(form)){
			throw new SinedException("A data da movimenta��o refere-se a um per�odo j� fechado.");
		}
		
		/*Se vier da tela de entrega retorna ou para a listagem ou para tela de entrada*/
		if(form.getController() != null && form.getWhereInDespesaviagem() != null && !form.getWhereInDespesaviagem().equals("")){
			super.doSalvar(request, form);
			
			String observacao = "<a href=/w3erp/financeiro/crud/Movimentacao?ACAO=consultar&cdmovimentacao=" + form.getCdmovimentacao() +">"+form.getCdmovimentacao()+"</a>";
			Despesaviagemacao acao;
			if(form.getAdiantamento()){
				acao = Despesaviagemacao.ADIANTAMENTO;
			}else{
				acao = Despesaviagemacao.ACERTO;
			}
			despesaviagemService.salvaHistorico(form.getWhereInDespesaviagem(), acao, observacao);
			request.clearMessages();
			request.addMessage("Movimenta��o financeira gerada com sucesso.", MessageType.INFO);
			return new ModelAndView("redirect:"+ form.getController());
		}
		
		return super.doSalvar(request, form);
	}
		
	@Override
	protected void salvar(WebRequestContext request, Movimentacao bean) throws Exception {
		if(bean.getCdmovimentacao() == null){
			bean.setMovimentacaoacao(Movimentacaoacao.NORMAL);
			
			if (bean.getWhereInDespesaviagem() != null && !bean.getWhereInDespesaviagem().equals("")) {
				
				String ids[] = bean.getWhereInDespesaviagem().split(",");
				List<Movimentacaoorigem> listaOrigem = new ArrayList<Movimentacaoorigem>();
				for (int i = 0; i < ids.length; i++) {
					Movimentacaoorigem movimentacaoorigem = new Movimentacaoorigem();
					if (bean.getAdiantamento()) {
						movimentacaoorigem.setDespesaviagemadiantamento(new Despesaviagem(Integer.parseInt(ids[i])));
					} else {
						movimentacaoorigem.setDespesaviagemacerto(new Despesaviagem(Integer.parseInt(ids[i])));
					}
					listaOrigem.add(movimentacaoorigem);
				}
				
				bean.setListaMovimentacaoorigem(listaOrigem);
			}
		}
		if (bean.getCdAgendamento() == null) {
			movimentacaoService.verificaConciliaAutomaticoCaixa(bean);
			boolean isCriarHistoricoCheque = false; 
			if(bean.getCdmovimentacao() == null && bean.getCheque() != null && bean.getCheque().getCdcheque() != null){
				isCriarHistoricoCheque = true;
			}
			
			boolean transferencia = movimentacaoService.isTransferencia(bean);
			Movimentacao movimentacaoAnterior = null;
			
			if (transferencia){
				movimentacaoAnterior = movimentacaoService.loadForEntrada(bean);
				validaTransferenciaSalvar(bean, movimentacaoAnterior);
			}
			
			super.salvar(request, bean);
			
			if (transferencia){
				atualizaTransferenciaSalvar(request, bean, movimentacaoAnterior);
			}
			
			if(isCriarHistoricoCheque){
				chequeService.updateSituacao(bean.getCheque().getCdcheque().toString(), Chequesituacao.BAIXADO);
				chequehistoricoService.criaHistorico(bean.getCheque(), null, null, bean);
			}
			
			if(StringUtils.isNotBlank(bean.getWhereInDespesaviagem())){
				StringBuilder obs = new StringBuilder();
				if(bean.getAdiantamento() != null && bean.getAdiantamento()){
					obs.append("Adiantamento da(s) despesa(s) de viagem:");
				}else{
					obs.append("Acerto da(s) despesa(s) de viagem:");
				}
				for(String idDespesaviagem : bean.getWhereInDespesaviagem().split(",")){
					obs.append(" <a href=/w3erp/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+idDespesaviagem+">"+idDespesaviagem+"</a>,");
				}
				
				Movimentacaohistorico dh = new Movimentacaohistorico();
				dh.setMovimentacao(bean);
				dh.setMovimentacaoacao(bean.getMovimentacaoacao());
				dh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				dh.setDtaltera(new Timestamp(System.currentTimeMillis()));
				dh.setObservacao(obs.substring(0, obs.length()-1));
				movimentacaohistoricoService.saveOrUpdate(dh);
			}
		} else {
			agendamentoService.doConsolidar(bean);
			request.addMessage("Registro salvo com sucesso.");
			request.setAttribute("closeWindow", true);
		}
		if (bean.isConciliacao()){
			Movimentacaohistorico movimentacaohistorico = new Movimentacaohistorico();
			movimentacaohistorico.setObservacao("Concilia��o Autom�tica");
			movimentacaohistorico.setMovimentacao(bean);
			movimentacaohistoricoService.saveOrUpdate(movimentacaohistorico);
		}
	}
	
	/**
	 * M�todo respons�vel por exibir a tela para justificativa de mudan�a de a��o da movimenta��o.
	 * 
	 * @param request
	 * @param movimentacaohistorico
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("exibirJustificativa")
	public ModelAndView openWindow(WebRequestContext request, Movimentacaohistorico movimentacaohistorico){
		
		String ids = request.getParameter("ids");
		
		//Verifica��o de data limite do ultimo fechamento. 
		Movimentacao mov = new Movimentacao();
		mov.setWhereIn(ids);
		if(fechamentofinanceiroService.verificaListaFechamento(mov)){
			SinedUtil.recarregarPaginaWithClose(request);
			request.addError("Existem movimenta��es cuja data da movimenta��o refere-se a um per�odo j� fechado.");
			return null;
		}
	
		request.setAttribute("editar", true);
		
		Movimentacaoacao acao = movimentacaohistorico.getMovimentacaoacao();
		String msg = "Justificativa";
		String desc = "Justificar";
		
		List<Movimentacao> lista = movimentacaoService.carregaLista(ids);
		if(acao.equals(Movimentacaoacao.ESTORNADA)){
			for (Movimentacao m : lista) {
				List<Movimentacao> listaMovimentacaoVinculada = movimentacaoService.findMovimentacaoVinculoDevolucao(m);
				if(!m.estornar() || SinedUtil.isListNotEmpty(listaMovimentacaoVinculada)){
					int size = lista.size();
					if(size == 1){
						request.addError("Esta movimenta��o n�o pode ser estornada.");
					}else if(size > 1){
						request.addError("Existem movimenta��es que n�o podem ser estornadas.");
					}
					if(SinedUtil.isListNotEmpty(listaMovimentacaoVinculada)){
						request.addError("� Necess�rio estornar a movimenta��o " + 
								CollectionsUtil.listAndConcatenate(listaMovimentacaoVinculada, "cdmovimentacao", ",") +	
								" que est� vinculada a movimentaca��o " + m.getCdmovimentacao() + ".");
					}
					SinedUtil.recarregarPaginaWithClose(request);
					return null;
				}
			}
			msg = "Informe o motivo do estorno da movimenta��o";
			desc = "Estornar";
		} else if(acao.equals(Movimentacaoacao.CANCELADA)){
			if(movimentacaoorigemService.existeOrigemVariosDocumentos(ids, true)){
				if(lista.size() > 1){
					request.addError("Esta movimenta��o n�o pode ser cancelada. � preciso realizar o estorno da conta a receber.");
				}else {
					request.addError("Existe(m) movimenta��o(�es) que n�o podem ser canceladas. � preciso realizar o estorno da conta a receber.");
				}
				SinedUtil.recarregarPaginaWithClose(request);
				return null;
			} else if(movimentacaoService.saldoValeCompraNegativoAposCancelamento(ids)){
				request.addError("N�o � poss�vel cancelar a(s) movimenta��o(�es)/vale compra. O saldo do cliente ficar� negativo.");
				SinedUtil.recarregarPaginaWithClose(request);
				return null;
			}
			
			List<Movimentacao> list = movimentacaoService.verificaTipoDocumentoAntecipacaoContaVinculadaBaixada(ids);
			String docs = "";
			for (Movimentacao m : list) {
				docs += m.getListaMovimentacaoorigem().get(0).getDocumento().getListaHistoricoAntecipacao().get(0).getDocumento().getCddocumento() + ", ";
			}
			
			if (StringUtils.isNotEmpty(docs)) {
				docs = docs.substring(0, docs.length() - 2);
			}
			
			if (list != null && list.size() > 0) {
				request.addError("A movimenta��o n�o poder� ser cancelada pois est� vinculada � baixa(s) da(s) conta(s) " + docs);
				SinedUtil.recarregarPaginaWithClose(request);
				return null;
			}
			
			String whereInCddocumento = "-1";
			
			for (Movimentacao m : lista) {
				List<Movimentacao> listaMovimentacaoVinculada = movimentacaoService.findMovimentacaoVinculoDevolucao(m);
				if(!m.cancelar() || SinedUtil.isListNotEmpty(listaMovimentacaoVinculada)){
					int size = lista.size();
					if(size == 1){
						request.addError("Esta movimenta��o n�o pode ser cancelada.");
					}else if(size > 1){
						request.addError("Existem movimenta��es que n�o podem ser canceladas.");
					}
					if(SinedUtil.isListNotEmpty(listaMovimentacaoVinculada)){
						request.addError("� Necess�rio cancelar a movimenta��o " + 
								CollectionsUtil.listAndConcatenate(listaMovimentacaoVinculada, "cdmovimentacao", ",") +	
								" que est� vinculada a movimentaca��o " + m.getCdmovimentacao() + ".");
					}
					SinedUtil.recarregarPaginaWithClose(request);
					return null;
				/*} else if(this.saldoCaixaContabancariaNegativo(m)){					
					request.addError("Existe(m) movimenta��o(�es) que o saldo da Caixa ou Conta Corrente ficar� negativo se ocorrer o cancelamento.");
					SinedUtil.recarregarPaginaWithClose(request);
					return null;
				}*/
				} else {
					if (movimentacaoService.isTransferencia(m)){
						List<Movimentacao> listaMovimentacaoTransferenciaRelacionada = movimentacaoService.findMovimentacaoRelacionadaTransferencia(m, true, false);
						for (Movimentacao movimentacao: listaMovimentacaoTransferenciaRelacionada){
							if(this.saldoCaixaContabancariaNegativo(movimentacao)){					
								request.addError("Existe(m) movimenta��o(�es) que o saldo da Caixa ou Conta Corrente ficar� negativo se ocorrer o cancelamento.");
								SinedUtil.recarregarPaginaWithClose(request);
								return null;
							}
						}
					}else {
						if(this.saldoCaixaContabancariaNegativo(m)){				
							request.addError("Existe(m) movimenta��o(�es) que o saldo da Caixa ou Conta Corrente ficar� negativo se ocorrer o cancelamento.");
							SinedUtil.recarregarPaginaWithClose(request);
							return null;
						}
					}
				}
				
				if (!m.getWhereInCddocumento().equals("")){
					whereInCddocumento += "," + m.getWhereInCddocumento(); 
				}
			}
			
			if (!valecompraService.estornarContaReceber(true, whereInCddocumento)){
				request.addError("N�o � poss�vel cancelar esta movimenta��o financeira, pois geraria um saldo negativo de vale-compra.");
				SinedUtil.recarregarPaginaWithClose(request);
				return null;
			}
			
			msg = "Informe o motivo do cancelamento da movimenta��o";
			desc = "Cancelar";
		}
		
		request.setAttribute("descricao", desc);
		request.setAttribute("msg", msg);
		request.setAttribute("ids", ids);
		
		if ("true".equalsIgnoreCase(request.getParameter("transferencia"))){
			List<Movimentacao> listaMovimentacaoTransferenciaRelacionada = new ArrayList<Movimentacao>();
			
			for (String id: ids.replace(" ", "").split(",")){
				Movimentacao movimentacao = new Movimentacao(Integer.valueOf(id));
				if (movimentacaoService.isTransferencia(movimentacao)){
					for (Movimentacao movimentacaoRelacionada: movimentacaoService.findMovimentacaoRelacionadaTransferencia(movimentacao, true, false)){
						if (!listaMovimentacaoTransferenciaRelacionada.contains(movimentacaoRelacionada)){
							listaMovimentacaoTransferenciaRelacionada.add(movimentacaoRelacionada);
						}
					}
				}			
			}
			
			movimentacaohistorico.setListaMovimentacaoTransferencia(listaMovimentacaoTransferenciaRelacionada);
			return new ModelAndView("direct:crud/popup/movimentacaoJustificativaTransferencia").addObject("movimentacaohistorico", movimentacaohistorico);
		}else {
			return new ModelAndView("direct:crud/popup/movimentacaoJustificativa").addObject("movimentacaohistorico", movimentacaohistorico);
		}		
	}
	
	/**
	 * M�todo que verifica se o saldo da Caixa ou Conta Corrente ficar� negativo se ocorrer o estorno.
	 * TRUE - se ficar� negativo
	 * FALSE - se n�o ficar� negativo
	 *
	 * @param m
	 * @return
	 * @author Luiz Fernando
	 */
	private Boolean saldoCaixaContabancariaNegativo(Movimentacao movimentacao) {
		if(movimentacao != null && movimentacao.getConta() != null){
			Conta conta = contaService.load(movimentacao.getConta());
	
			if (movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)) {				
				double valorMov = movimentacao.getValor().getValue().doubleValue();
				double valorMaximo = contaService.obterValorMaximo(conta).getValue().doubleValue();

				if (valorMov > valorMaximo)
					return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;		
	}
	
	/**
	 * M�todo respons�vel por salvar a justificativa no historico da movimenta��o ao mudar a a��o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#estornar(String, String)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#cancelar(String, String)
	 * @param request
	 * @param movimentacaohistorico
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("saveJustificativa")
	public ModelAndView saveJustificativa(WebRequestContext request, Movimentacaohistorico movimentacaohistorico){
		String ids = request.getParameter("ids");
		String verbo = "salva(s)";
		int sucesso = 0;
		
		if(Movimentacaoacao.ESTORNADA.equals(movimentacaohistorico.getMovimentacaoacao())){
			sucesso = movimentacaoService.estornar(ids, movimentacaohistorico.getObservacao());
			verbo = "estornada";
		}
		if(Movimentacaoacao.CANCELADA.equals(movimentacaohistorico.getMovimentacaoacao())){
			ids = getIdsTransferencia(ids);
			sucesso = movimentacaoService.cancelar(ids, movimentacaohistorico.getObservacao());
			verbo = "cancelada";
		}
		
		Set<Integer> listaId = new HashSet<Integer>();
		
		for (String id: ids.replace(" ", "").split(",")){
			listaId.add(Integer.valueOf(id));
		}
		
		int erro = listaId.size() - sucesso;
		
		if(sucesso > 0){
			if(sucesso == 1)
				request.addMessage("Movimenta��o " + verbo + " com sucesso.", MessageType.INFO);
			else
				request.addMessage(sucesso + " movimenta��es foram " + verbo + "s com sucesso.", MessageType.INFO);
		}
		if(erro > 0){
			if(erro == 1)
				request.addError("Movimenta��o n�o p�de ser " + verbo + ".");
			else
				request.addMessage(erro + " movimenta��es n�o puderam ser " + verbo + "s.", MessageType.WARN);
		}
		
		SinedUtil.recarregarPaginaWithClose(request);
		return null;
	}
	
	/**
	 * M�todo para obter o valor limite que a movimenta��o poder� ter com base na conta.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#obterValorMaximo(Conta)
	 * @param request
	 * @param conta
	 * @author Fl�vio Tavares
	 */
	@Action("getLimite")
	public void obterValorLimite(WebRequestContext request, Conta conta){
		String format = contaService.valorMaximoFormat(conta);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var valor = \""+format+"\";");
	}
	
	/**
	 * M�todo de suporte para Concilia��o banc�ria. Cria uma movimentacao com alguns valores j� preenchidos.
	 * 
	 * @see #doEntrada(WebRequestContext, Movimentacao)
	 * @see br.com.linkcom.sined.geral.service.ContaService#load(Conta, String)
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Fl�vio Tavares
	 */
	@Input(LISTAGEM)
	@Action("carregarMovimentacao")
	public ModelAndView carregarMovimentacao(WebRequestContext request, Movimentacao movimentacao) throws Exception{
		
		movimentacao.setConta(contaService.carregaConta(movimentacao.getConta()));
		Rateio rateio = movimentacao.getRateio();
		if (rateio == null) {
			rateio = new Rateio();
		}
		rateio.setValoraux(movimentacao.getValor().toString().replaceAll("\\.", ""));
		rateio.setValortotaux(movimentacao.getValor().toString().replaceAll("\\.", ""));
		movimentacao.setRateio(rateio);
		
		movimentacao.setDtmovimentacao(movimentacao.getDtbanco());
		movimentacao.setReadonly(true);
		
		return this.doEntrada(request, movimentacao);
	}
	
	@Action("fromDespesaViagemAdiantamento")
	public ModelAndView fromDespesaViagemAdiantamento(WebRequestContext request, Movimentacao movimentacao) throws Exception {
		
		if(movimentacao.getWhereInDespesaviagem() == null || movimentacao.getWhereInDespesaviagem().equals("")){
			throw new SinedException("Despesa de viagem n�o pode ser nulo.");
		}
		
		if("entrada".equals(movimentacao.getController())){
			movimentacao.setController("/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+movimentacao.getWhereInDespesaviagem());
		} else {
			movimentacao.setController("/financeiro/crud/Despesaviagem");
		}
		
		Money valortotal = new Money();
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.findForGerarAcerto(movimentacao.getWhereInDespesaviagem());
		if(listaDespesaviagem != null && !listaDespesaviagem.isEmpty()){
			for(Despesaviagem dv : listaDespesaviagem){
				valortotal = valortotal.add(dv.getTotalprevisto());
			}
		}
		
		movimentacao.setValor(valortotal);
		
		Rateio rateio = despesaviagemService.criaRateioForAtualizarvalores(valortotal, listaDespesaviagem, true, Tipooperacao.TIPO_CREDITO.equals((movimentacao.getTipooperacao())) ? true : false);
		rateio.setValortotaux(valortotal.toString());
		rateio.setValoraux(valortotal.toString());
		rateioService.atualizaValorRateio(rateio, valortotal);
		movimentacao.setRateio(rateio);
		
		movimentacao.setAdiantamento(true);
		movimentacao.setHistorico("Adiantamento referente � despesa de viagem " + movimentacao.getWhereInDespesaviagem());
		
		movimentacao.setDtmovimentacao(new Date(System.currentTimeMillis()));
		return this.doEntrada(request, movimentacao);
	}
	
	@Action("fromDespesaViagemAcerto")
	public ModelAndView fromDespesaViagemAcerto(WebRequestContext request, Movimentacao movimentacao) throws Exception {
		
		if(movimentacao.getWhereInDespesaviagem() == null || movimentacao.getWhereInDespesaviagem().equals("")){
			throw new SinedException("Despesa de viagem n�o pode ser nulo.");
		}
		
		if("entrada".equals(movimentacao.getController())){
			movimentacao.setController("/financeiro/crud/Despesaviagem?ACAO=consultar&cddespesaviagem="+movimentacao.getWhereInDespesaviagem());
		} else {
			movimentacao.setController("/financeiro/crud/Despesaviagem");
		}
		
		Money valortotal = new Money();
		List<Despesaviagem> listaDespesaviagem = despesaviagemService.findForGerarAcerto(movimentacao.getWhereInDespesaviagem());
		if(listaDespesaviagem != null && !listaDespesaviagem.isEmpty()){
			for(Despesaviagem dv : listaDespesaviagem){
				valortotal = valortotal.add(dv.getTotalprevisto());
			}
		}
		
		Rateio rateio = despesaviagemService.criaRateioForAtualizarvalores(valortotal, listaDespesaviagem, false, Tipooperacao.TIPO_CREDITO.equals((movimentacao.getTipooperacao())) ? true : false);
		rateio.setValortotaux(movimentacao.getValor().toString());
		rateio.setValoraux(movimentacao.getValor().toString());
		rateioService.atualizaValorRateio(rateio, movimentacao.getValor());
		movimentacao.setRateio(rateio);
		movimentacao.setAdiantamento(false);
		movimentacao.setHistorico("Acerto referente � despesa de viagem " + movimentacao.getWhereInDespesaviagem());
		
		movimentacao.setDtmovimentacao(new Date(System.currentTimeMillis()));
		return this.doEntrada(request, movimentacao);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Movimentacao bean) {
		if("true".equals(request.getParameter("fromInsertOneConciliacao"))){
			List<Movimentacao> lista = (List<Movimentacao>)request.getSession().getAttribute("listaMovimentacao");
			if (lista != null) {
				lista.add(bean);
				request.getSession().setAttribute("listaMovimentacao", lista);
			} else {
				lista = new ArrayList<Movimentacao>();
				lista.add(bean);
				request.getSession().setAttribute("listaMovimentacao", lista);
			}

//			Object id = Util.strings.toStringIdStyled(bean);
			String description = Util.strings.toStringDescription(bean);
			NeoWeb.getRequestContext().clearMessages();
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script src=\""+request.getServletRequest().getContextPath()+"/resource/js/util.js\"></script>" +
					"<script type='text/javascript' src='"+request.getServletRequest().getContextPath()+"/js/jquery.js'></script>" + 
					"<script type='text/javascript' src='"+request.getServletRequest().getContextPath()+"/js/utilconciliacao.js'></script>" 
			);


			View.getCurrent().println("<script>function callfunction(){selecionarConciliacao('br.com.linkcom.sined.geral.bean.Movimentacao[stringMovimentacao="+bean.getCdmovimentacao()+"]', '"+super.addEscape(description)+"', false);}</script>");
			View.getCurrent().println("<script>window.location.href = \"javascript:callfunction();\";</script>");

			return null;
		}
		if ("true".equals(request.getParameter("editaMovimentacao"))){
			SinedUtil.recarregarPaginaWithClose(request);
			return null;
		}
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	public ModelAndView abrirPopupAnteciparcredito(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/financeiro/crud/Movimentacao");
			return null;
		}
		
		Boolean movimentacaoDiferenteCreditoNormal = movimentacaoService.isMovimentacaoDiferenteCreditoNormal(whereIn);
		if(movimentacaoDiferenteCreditoNormal != null && movimentacaoDiferenteCreditoNormal){
			request.addError("S� � permitido antecipar cr�dito para movimenta��es com situa��o NORMAL e tipo de opera��o CR�DITO.");
			SinedUtil.redirecionamento(request, "/financeiro/crud/Movimentacao");
			return null;
		}
		
		Movimentacao movimentacao = new Movimentacao();		
		movimentacao.setValorantecipacao(movimentacaoService.getValorTotalMovimentacoes(whereIn));
		movimentacao.setWhereIn(whereIn);
		movimentacao.setTipooperacao(Tipooperacao.TIPO_DEBITO);
		movimentacao.setTaxavalor(new Money());
		movimentacao.setDtbanco(new Date(System.currentTimeMillis()));
		movimentacao.setObservacao("Antecipa��o de cr�dito.");
		request.setAttribute("acaoSalvar", "salvaAnteciparcredito" );
		return new ModelAndView("direct:/crud/popup/anteciparCredito", "bean", movimentacao);
		
	}
	
	public ModelAndView salvaAnteciparcredito(WebRequestContext request, Movimentacao movimentacao){
		
		if(movimentacao == null || movimentacao.getWhereIn() == null || "".equals(movimentacao.getWhereIn())){
			request.addError("Nenhuma movimenta��o encontrada.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		String erro = validaAnteciparcredito(movimentacao); 
		if(erro == null || "".equals(erro)){
			
			Movimentacao movimentacaoDebito= new Movimentacao();
			movimentacaoDebito.setMovimentacaoacao(Movimentacaoacao.NORMAL);
			movimentacaoDebito.setAntecipada(Boolean.TRUE);			
			movimentacaoDebito.setValor(movimentacao.getTaxavalor());
			movimentacaoDebito.setConta(movimentacao.getConta());
			movimentacaoDebito.setRateio(movimentacao.getRateio());
			movimentacaoDebito.setObservacao("Antecipa��o de cr�dito." + movimentacao.getObservacao() != null ? movimentacao.getObservacao() : "");
			movimentacaoDebito.setHistorico("Antecipa��o de cr�dito. " + movimentacao.getFornecedorAntecipacao().getNome());
			movimentacaoDebito.setDtmovimentacao(new Date(System.currentTimeMillis()));
			movimentacaoDebito.setTipooperacao(movimentacao.getTipooperacao());
			movimentacaoService.saveOrUpdate(movimentacaoDebito);	
			
			Movimentacaohistorico mhDebito = movimentacaohistoricoService.geraHistoricoMovimentacao(movimentacaoDebito);			
			movimentacaohistoricoService.saveOrUpdate(mhDebito);
			
			String[] itens = movimentacao.getWhereIn().split(",");
			Movimentacao movimentacaoOriginal;
			for(String cd : itens){
				movimentacaoOriginal = movimentacaoService.load(new Movimentacao(Integer.parseInt(cd)));
				movimentacaoOriginal.setDtbanco(new Date(System.currentTimeMillis()));
				if(movimentacaoOriginal.getChecknum() == null)
					movimentacaoOriginal.setChecknum("");
				movimentacaoService.salvaMovimentacaoConciliacaoAntecipacaoCredito(movimentacaoOriginal, movimentacao.getObservacao() != null ? movimentacao.getObservacao() : "");
			}
		
			request.addMessage("Antecipa��o de cr�dito gerada com sucesso.");
			SinedUtil.redirecionamento(request, "/financeiro/crud/Movimentacao");
			return null;
		}else {
			request.addError(erro);
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
	}
	private String validaAnteciparcredito(Movimentacao movimentacao) {
		String erros = "";
		if(movimentacao == null || movimentacao.getConta() == null || movimentacao.getDtbanco() == null || movimentacao.getRateio() == null
				|| (movimentacao.getTaxatransacaovalor() == null && movimentacao.getTaxatransacaopercentual() == null) 
				|| movimentacao.getValorantecipacao() == null || movimentacao.getFornecedorAntecipacao() == null){
			erros += "N�o foi poss�vel antecipar cr�dito por existir campo(s) obrigat�rio(s) que n�o foram preenchidos.";
		}
		
		Money valorMaximo = contaService.obterValorMaximo(movimentacao.getConta());
		if(valorMaximo != null && movimentacao.getTaxavalor().getValue().doubleValue() > valorMaximo.getValue().doubleValue()){
			erros += "O valor da movimenta��o n�o pode ser maior do que o valor m�ximo.\n";
		}
		
		try {
			rateioService.validateRateio(movimentacao.getRateio(), movimentacao.getTaxavalor());
		} catch (SinedException e) {			
			e.printStackTrace();
			erros += e.getMessage();
		}
		
		return erros;
	}
	
	public ModelAndView validaVincularConta(WebRequestContext request){
		String msg = "";
		boolean error = false;
		
		Movimentacao movimentacao = new Movimentacao();
		try{
			movimentacao = movimentacaoService.loadWithConta(new Movimentacao(Integer.parseInt(request.getParameter("selectedItens"))));	
		}catch (Exception e) {
			e.printStackTrace();
			error = true;
			msg = "N�o � possivel vincular contas com v�rias movimenta��es selecionadas";
		}
		
		if(!error){
			Money diferenca = movimentacaoorigemService.somaDosDocumentos(movimentacao);
			Money total = new Money();
			total = movimentacao.getValor().subtract(diferenca);
			
			if(!movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.CONCILIADA)){
				msg = "N�o � possivel vincular contas essa movimenta��o.";
				error=true;
			}
			
			if(total.toLong() <= 0){
				msg = "N�o � possivel vincular mais contas � essa movimenta��o.";
				error=true;
			}
		}
		
		return new JsonModelAndView()
			.addObject("error", error)
			.addObject("msg", msg);
		
	}
	
	
	/**
	 * M�todo que carrega os dados dos documentos a serem vinculados a movimenta��o
	 * 
	 * @author Filipe Santos
	 * @since 07/05/2012 
	 * @param request
	 * @return
	 */	
	public ModelAndView vincularConta (WebRequestContext request){
		Movimentacao movimentacao = movimentacaoService.loadWithConta(new Movimentacao(Integer.parseInt(request.getParameter("selectedItens"))));		
		List<Documento> listaDocumentos = documentoService.findDocumentosSemMovimentacao(movimentacao.getTipooperacao());
		movimentacao.setListaDocumento(listaDocumentos);
		Money diferenca = movimentacaoorigemService.somaDosDocumentos(movimentacao);
		
		request.setAttribute("lista", listaDocumentos);
		request.setAttribute("valorEsperado", movimentacao.getValor());
		request.setAttribute("valorDiferenca", diferenca);
		return new ModelAndView("direct:/crud/popup/vincularConta","movimentacao",movimentacao);
	}
	
	/**
	 * M�todo que salva as contas seleciondas e vincula a movimenta��o.
	 * 
	 * @author Filipe Santos
	 * @since 09/05/2012
	 * @param request
	 * @param filtro
	 */
	public ModelAndView salvarVinculosConta (WebRequestContext request, MovimentacaoFiltro filtro){
		Movimentacao movimentacao = movimentacaoService.loadWithConta(new Movimentacao(Integer.parseInt(filtro.getCdmovimentacao())));
		String[] listaCddocumentos = filtro.getCddocumentos().split(",");
		movimentacaoService.salvarVinculosDocumentos(listaCddocumentos, movimentacao);
		return null;
	}
	
	public void setarValorByCheque(WebRequestContext request){
		Cheque cheque = chequeService.load(new Cheque(Integer.valueOf(request.getParameter("cdcheque"))));
		if (cheque.getValor()!=null){
			View.getCurrent().print("form['valor'].value = '" + cheque.getValor().toString() + "';");
		}
	}
	
	public ModelAndView comboBoxTipoVinculo(WebRequestContext request, Contatipo contatipo) {
		String whereInEmpresa = request.getParameter("empresaWhereIn");
		if (contatipo == null || contatipo.getCdcontatipo() == null) {
			throw new SinedException("Tipo de v�nculo n�o pode ser nulo.");
		}
		
		List<Conta> listaConta = contaService.findByTipoAndEmpresa(whereInEmpresa, contatipo);
		

		/**
		 * Para n�o dar pau ao retornar o objeto JSON
		 */
		for (Conta conta : listaConta) {
			conta.setListaContaempresa(null);
		}
		return new JsonModelAndView().addObject("lista", listaConta);
	}
	
	public ModelAndView comboBoxContatipo(WebRequestContext request, Formapagamento formapagamento) {
		if (formapagamento == null || formapagamento.getCdformapagamento() == null) {
			throw new SinedException("Tipo de v�nculo n�o pode ser nulo.");
		}
		
		List<Contatipo> listaContatipo = new ArrayList<Contatipo>();
		StringBuilder whereInContatipo = new StringBuilder(); 
		if (formapagamento.equals(Formapagamento.CAIXA)) {
			listaContatipo.add(contatipoService.load(new Contatipo(Contatipo.CAIXA)));
		} else if (formapagamento.equals(Formapagamento.DINHEIRO)) {
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CAIXA);
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CONTA_BANCARIA);			
		} else if (formapagamento.equals(Formapagamento.CARTAOCREDITO)) {
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CARTAO_DE_CREDITO);
		} else {
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CAIXA);
			if(!"".equals(whereInContatipo.toString()))
				whereInContatipo.append(",");
			whereInContatipo.append(Contatipo.CONTA_BANCARIA);		
		}
		
		if(whereInContatipo != null && !"".equals(whereInContatipo.toString()))
			listaContatipo = contatipoService.findForComboMovimentacao(whereInContatipo.toString(), formapagamento.getEmpresaWhereIn());
		
		if(listaContatipo != null && !listaContatipo.isEmpty()){
			for(Contatipo contatipo : listaContatipo){
				contatipo.setListaConta(null);
				contatipo.setListaTipoVinculo(null);
			}
		}

		return new JsonModelAndView().addObject("lista", listaContatipo);
	}
	
	/**
	 * Action que dispara a transfer�ncia de v�rias movimenta��es
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2014
	 */
	public ModelAndView transferirMovimentacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.trim().equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		boolean erroConta = false;
		boolean erroSituacao = false;
		boolean erroTipooperacao = false;
		
		List<Movimentacao> listaMovimentacao = movimentacaoService.findForTransferenciaLote(whereIn);
		Conta conta = null;
		for (Movimentacao movimentacao : listaMovimentacao) {
			if(conta == null) conta = movimentacao.getConta();
			
			if(!conta.equals(movimentacao.getConta())){
				erroConta = true;
			}
			if(movimentacao.getTipooperacao() == null || !movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){
				erroTipooperacao = true;
			}
			if(movimentacao.getMovimentacaoacao() == null || !movimentacao.getMovimentacaoacao().equals(Movimentacaoacao.NORMAL)){
				erroSituacao = true;
			}
			if(erroConta && erroTipooperacao && erroSituacao){
				break;
			}
		}
		
		if(erroConta) request.addError("S� � permitido transferir movimenta��es de um mesmo v�nculo.");
		if(erroTipooperacao) request.addError("S� � permitido transferir movimenta��es do tipo 'CR�DITO'.");
		if(erroSituacao) request.addError("S� � permitido transferir movimenta��es que est�o na situa��o 'NORMAL'.");
		
		if(erroConta || erroTipooperacao || erroSituacao){
			return sendRedirectToAction("listagem");
		}
		
		return new ModelAndView("redirect:/financeiro/process/GerarMovimentacao?ACAO=tranferenciaLote&TABPANEL_janelaEntrada=1&whereInMovimentacao=" + whereIn);
	}
	
	/**
	 * M�todo que verifica se as movimenta��es a serem salvas s�o duplicatas de outras j� existentes.
	 * @author Rafael Salvio
	 * @date 26/03/2015 
	 */
	public void ajaxVerificaDuplicidade(WebRequestContext request) throws Exception{
		Boolean isContaDuplicada = Boolean.FALSE;
		Boolean bloquearRepeticaoConta = Boolean.FALSE;
		
		try{
			String param = parametrogeralService.buscaValorPorNome(Parametrogeral.BLOQUEAR_REPETICAO_CONTA);
			if(param != null && !param.trim().isEmpty()){
				bloquearRepeticaoConta = Boolean.parseBoolean(param);
			}
		} catch (Exception e) {
		} finally{
			if(bloquearRepeticaoConta == null){
				bloquearRepeticaoConta = Boolean.FALSE;
			}
		}
		if(bloquearRepeticaoConta){
			try {
				Object valores = request.getParameter("whereInValor");
				String whereInvaloresMovimentacao = null;
				if(valores != null && !valores.toString().trim().isEmpty()){
					whereInvaloresMovimentacao = valores.toString().replaceAll("\\.", "").replaceAll(",", "").replaceAll(";", ",");
					whereInvaloresMovimentacao = whereInvaloresMovimentacao.substring(0, whereInvaloresMovimentacao.length()-1);
					
					Integer limSuperior = whereInvaloresMovimentacao.lastIndexOf(";");
					if(limSuperior > 0){
						whereInvaloresMovimentacao = whereInvaloresMovimentacao.substring(0, limSuperior);
					}
				}
				
				Object dtpagamento = request.getParameter("dtpagamento");
				Date dtmovimentacao = SinedDateUtils.stringToDate((String)dtpagamento);
				
				isContaDuplicada = movimentacaoService.verificaDuplicidade(whereInvaloresMovimentacao, dtmovimentacao);
			} catch (Exception e) {}
		}
		
		View.getCurrent().println("var isContaDuplicada = '"+isContaDuplicada.toString()+"';");
		View.getCurrent().println("var bloquearRepeticaoConta = '"+bloquearRepeticaoConta.toString()+"';");
	}	
	
	public ModelAndView abrirFiltroPdfCsvReport(WebRequestContext request){
		MovimentacaoFiltro movimentacaoFiltro = new MovimentacaoFiltro();
		return new ModelAndView("direct:/crud/popup/filtroPdfCsvReport", "movimentacaoFiltro", movimentacaoFiltro).addObject("tipo", request.getParameter("tipoReport"));
	}
	
	/**
	* M�todo que carrega o combo de empresa de acordo com a conta
	*
	* @param request
	* @param conta
	* @return
	* @since 05/05/2017
	* @author Luiz Fernando
	*/
	public ModelAndView comboBoxEmpresa(WebRequestContext request, Conta conta) {
		return new JsonModelAndView().addObject("lista", empresaService.getListaEmpresaByConta(request, conta));
	}
	
	public ModelAndView ajaxTemplatesForCopiaCheque(WebRequestContext request){
		return chequeService.ajaxTemplatesForCopiaCheque(request);
	}
	
	public ModelAndView abrirSelecaoTemplateCopiaCheque(WebRequestContext request){
		return chequeService.abrirSelecaoTemplateCopiaCheque(request);
	}
	
	private void validaTransferenciaSalvar(Movimentacao bean, Movimentacao movimentacaoAnterior) {
		List<Movimentacao> listaMovimentacaoTransferenciaRelacionadaTodas = movimentacaoService.findMovimentacaoRelacionadaTransferencia(bean, true, false);
		List<Movimentacao> listaMovimentacaoTransferenciaRelacionada = movimentacaoService.findMovimentacaoRelacionadaTransferencia(bean, false, false);
		
		if (!movimentacaoService.verificaSituacao(listaMovimentacaoTransferenciaRelacionadaTodas, Movimentacaoacao.NORMAL)){
			throw new SinedException("N�o � poss�vel alterar esta movimenta��o financeira. Para realizar qualquer altera��o nesse registro � obrigat�rio que as movimenta��es financeiras relacionadas (origem e destino) estejam na situa��o 'Normal'.");
		}			
		
		for (Movimentacao movimentacao: listaMovimentacaoTransferenciaRelacionadaTodas){
			if (movimentacao.getCheque()!=null){
				throw new SinedException("N�o � poss�vel alterar esta movimenta��o financeira, pois a forma de pagamento est� relacionada ao tipo Cheque. � aconselh�vel cancelar as movimenta��es financeiras de origem e destino e refazer toda a opera��o.");
			}
		}
		
		for (Movimentacao movimentacao: listaMovimentacaoTransferenciaRelacionada){
			if (bean.getConta()!=null 
					&& movimentacaoAnterior.getConta()!=null 
					&& !bean.getConta().equals(movimentacaoAnterior.getConta())
					&& bean.getConta().equals(movimentacao.getConta())){
				throw new SinedException("N�o � poss�vel alterar esta movimenta��o financeira, pois o valor do campo V�nculo � igual na movimenta��o financeira relacionada.");					
			}
		}
	}
	
	private void atualizaTransferenciaSalvar(WebRequestContext request, Movimentacao bean, Movimentacao movimentacaoAnterior) {
		List<Movimentacao> listaMovimentacaoTransferenciaRelacionada = movimentacaoService.findMovimentacaoRelacionadaTransferencia(bean, false, false);
		boolean alterouValor = bean.getValor().toLong()!=movimentacaoAnterior.getValor().toLong();
		boolean alterouDtmovimentacao = SinedDateUtils.diferencaDias(bean.getDtmovimentacao(), movimentacaoAnterior.getDtmovimentacao())!=0;
		boolean alterouHistorico = !Util.strings.emptyIfNull(bean.getHistorico()).equals(Util.strings.emptyIfNull(movimentacaoAnterior.getHistorico()));
		String camposAlterados = "";
		
		if (alterouValor){
			camposAlterados += "Valor, ";
		}
		
		if (alterouDtmovimentacao){
			camposAlterados += "Data da Movimenta��o, ";
		}
		
		if (alterouHistorico){
			camposAlterados += "Hist�rico, ";
		}
		
		for (Movimentacao movimentacaoRelacionada: listaMovimentacaoTransferenciaRelacionada){
			if (alterouValor){
				movimentacaoService.updateValorMovimentacao(movimentacaoRelacionada, bean.getValor());
				Money total = new Money();
				
				for (int i=0; i<movimentacaoRelacionada.getRateio().getListaRateioitem().size(); i++){
					Rateioitem rateioitem = movimentacaoRelacionada.getRateio().getListaRateioitem().get(i);
					Money valor = new Money(rateioitem.getPercentual() * bean.getValor().getValue().doubleValue() / 100).round();
					total = total.add(new Money(valor)).round();
					
					if (i==movimentacaoRelacionada.getRateio().getListaRateioitem().size()-1){ //�ltima
						if (bean.getValor().toLong()>total.toLong()){
							valor = valor.add(bean.getValor().subtract(total));
						}else if (bean.getValor().toLong()<total.toLong()){
							valor = valor.subtract(total.subtract(bean.getValor()));
						}
					}
					
					rateioitem.setValor(valor);
					rateioitemService.updateValorRateioItem(rateioitem);
				}
			}
			
			if (alterouDtmovimentacao){
				movimentacaoService.updateDtmovimentacao(movimentacaoRelacionada, bean.getDtmovimentacao());
			}
			
			if (alterouHistorico){
				if (bean.getHistorico()!=null && !historicooperacaoService.isPossuiTipo(Tipohistoricooperacao.TRANSFERENCIA_ENTRE_CONTAS)){
					movimentacaoService.updateHistorico(movimentacaoRelacionada, bean.getHistorico());
				}
			}
			
			if (!camposAlterados.equals("")){
				Movimentacaohistorico movimentacaohistorico = new Movimentacaohistorico();
				movimentacaohistorico.setMovimentacao(movimentacaoRelacionada);
				movimentacaohistorico.setMovimentacaoacao(movimentacaoRelacionada.getMovimentacaoacao());
				movimentacaohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				movimentacaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				movimentacaohistorico.setObservacao("Campos Alterados.");
				movimentacaohistoricoService.saveOrUpdate(movimentacaohistorico);
			}
		}		
		
		if (!camposAlterados.equals("")){
			Movimentacaohistorico movimentacaohistorico = new Movimentacaohistorico();
			movimentacaohistorico.setMovimentacao(bean);
			movimentacaohistorico.setMovimentacaoacao(bean.getMovimentacaoacao());
			movimentacaohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			movimentacaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			movimentacaohistorico.setObservacao("Campos Alterados.");
			movimentacaohistoricoService.saveOrUpdate(movimentacaohistorico);
			
			camposAlterados = camposAlterados.substring(0, camposAlterados.length()-2);
			request.addMessage("A altera��o efetuada no campo " + camposAlterados + " foi atualizada em todas as movimenta��es financeiras vinculadas a esse registro.");
		}
	}
	
	public ModelAndView ajaxTransferencia(WebRequestContext request ) {
		String msgErro = "";
		String msgTransferencia = "";
		
		if ("0".equals(request.getParameter("idacao"))){
			String ids = request.getParameter("ids").replace(" ", "");
			
			for (String id: ids.split(",")){
				Movimentacao movimentacao = new Movimentacao(Integer.valueOf(id));
				if (movimentacaoService.isTransferencia(movimentacao)){
					List<Movimentacao> listaMovimentacaoTransferenciaRelacionada = movimentacaoService.findMovimentacaoRelacionadaTransferencia(movimentacao, true, false);
					if (!movimentacaoService.verificaSituacao(listaMovimentacaoTransferenciaRelacionada, Movimentacaoacao.NORMAL)){
						msgErro = "Para acontecer o cancelamento de uma movimenta��o financeira que a origem seja uma transfer�ncia interna, todas as movimenta��es relacionadas a ela devem estar na situa��o de 'Normal'.";
						break;
					}
				}
				
			}
			
			if (msgErro.equals("")){
				for (String id: ids.split(",")){
					Movimentacao movimentacao = new Movimentacao(Integer.valueOf(id));
					if (movimentacaoService.isTransferencia(movimentacao)){
						msgTransferencia = "Foi solicitado o cancelamento de uma movimenta��o financeira com origem de transfer�ncia interna, neste caso, a contra-partida tamb�m ser� cancelada. Deseja continuar?";
						break;
					}					
				}
			}			
		}
		
		return new JsonModelAndView().addObject("msgErro", msgErro).addObject("msgTransferencia", msgTransferencia);
	}
	
	private String getIdsTransferencia(String ids){
		for (String id: ids.replace(" ", "").split(",")){
			Movimentacao movimentacao = new Movimentacao(Integer.valueOf(id));
			if (movimentacaoService.isTransferencia(movimentacao)){
				List<Movimentacao> listaMovimentacaoTransferenciaRelacionada = movimentacaoService.findMovimentacaoRelacionadaTransferencia(movimentacao, false, false);
				ids += ",";
				ids += CollectionsUtil.listAndConcatenate(listaMovimentacaoTransferenciaRelacionada, "cdmovimentacao", ",");
			}			
		}
		
		return ids;
	}
	
	public ModelAndView abrirPopupProtocoloRetiradaCheque(WebRequestContext request, ProtocoloRetiradaChequeFiltro filtro){
		filtro.setWhereInCdmovimentacao(SinedUtil.getItensSelecionados(request));
		filtro.setWhereInCdcheque(null);
		return chequeService.abrirPopupProtocoloRetiradaCheque(request, filtro);
	}
	
	public ModelAndView abrirRateio(WebRequestContext request){
		return rateioService.abrirRateio(request);
	}
}