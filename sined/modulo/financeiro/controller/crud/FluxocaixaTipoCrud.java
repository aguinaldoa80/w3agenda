package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.FluxocaixaTipo;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.FluxocaixaTipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/financeiro/crud/FluxocaixaTipo", authorizationModule=CrudAuthorizationModule.class)
public class FluxocaixaTipoCrud extends CrudControllerSined<FluxocaixaTipoFiltro, FluxocaixaTipo, FluxocaixaTipo>{

	@Override
	protected void salvar(WebRequestContext request, FluxocaixaTipo bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if(DatabaseError.isKeyPresent(e, "IDX_FLUXOCAIXATIPO_NOME")){
				throw new SinedException("Descri��o j� cadastrada no sistema.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_FLUXOCAIXATIPO_SIGLA")){
				throw new SinedException("Sigla j� cadastrada no sistema.");
			}
			throw e;
		}
		
	}
}
