package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariodocumento;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.service.ArquivobancarioService;
import br.com.linkcom.sined.geral.service.ArquivobancariodocumentoService;
import br.com.linkcom.sined.geral.service.ArquivobancariosituacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.ArquivobancarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(
		path="/financeiro/crud/Arquivobancario",
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = {"clientes", "nome", "arquivobancariocorrespondente", "data", "sequencial", "arquivobancariosituacao"})
public class ArquivobancarioCrud extends CrudControllerSined<ArquivobancarioFiltro, Arquivobancario, Arquivobancario> {

	private ArquivobancarioService arquivobancarioService;
	private ArquivobancariosituacaoService arquivobancariosituacaoService;
	private MovimentacaoService movimentacaoService;
	private ArquivobancariodocumentoService arquivobancariodocumentoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setArquivobancariosituacaoService(ArquivobancariosituacaoService arquivobancariosituacaoService) {
		this.arquivobancariosituacaoService = arquivobancariosituacaoService;
	}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {
		this.arquivobancarioService = arquivobancarioService;
	}
	public void setArquivobancariodocumentoService(
			ArquivobancariodocumentoService arquivobancariodocumentoService) {
		this.arquivobancariodocumentoService = arquivobancariodocumentoService;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected void listagem(WebRequestContext request,ArquivobancarioFiltro filtro) throws Exception {
		request.setAttribute("listaSituacao", arquivobancariosituacaoService.findAll("arquivobancariosituacao.cdarquivobancariosituacao"));
	}
	
	@SuppressWarnings("unchecked")
	protected void setListagemInfo(WebRequestContext request, ArquivobancarioFiltro filtro) throws Exception {
		if (!listagemVaziaPrimeiraVez() || filtro.isNotFirstTime()) {
			if(request.getParameter("resetCurrentPage") != null) {
				filtro.setCurrentPage(0);
			}
			
			ListagemResult<Arquivobancario> listagemResult = getLista(request, filtro);
			List<Arquivobancario> list = listagemResult.list();
			
			// CHAVES DOS ARQUIVOS PARA DOWNLOAD
			for (Arquivobancario arquivobancario : list) {
				if(arquivobancario.getArquivo()!=null && (arquivobancario.getArquivobancariosituacao().equals(Arquivobancariosituacao.GERADO) ||
						arquivobancario.getArquivobancariotipo().equals(Arquivobancariotipo.RETORNO)))
					DownloadFileServlet.addCdfile(request.getSession(), new Long(arquivobancario.getArquivo().getCdarquivo()));
				
				List<Arquivobancariodocumento> listaArquivoDocumento = arquivobancariodocumentoService.findListaForArquivoBancario(arquivobancario);
				int contadorClientes = 0;
				for (Arquivobancariodocumento abd : listaArquivoDocumento) {
					if(abd.getDocumento() != null && abd.getDocumento().getPessoa() != null){
						contadorClientes ++;
					}
				}
				if (contadorClientes <= 5){
					arquivobancario.setClientes(SinedUtil.listAndConcatenate(listaArquivoDocumento, "documento.pessoa.nome", ","));
				} else {
					arquivobancario.setClientes("Diversos");
				}
				
			}
			
			request.setAttribute("lista", list);
			request.setAttribute("currentPage", filtro.getCurrentPage());
			request.setAttribute("numberOfPages", filtro.getNumberOfPages());
			request.setAttribute("filtro", filtro);			
		} else {
			request.setAttribute("lista", new ArrayList());
			request.setAttribute("currentPage", 0);
			request.setAttribute("numberOfPages", 0);
			request.setAttribute("filtro", filtro);
		}
	}
	
	@Action("cancelar")
	public ModelAndView cancelar(WebRequestContext request) throws Exception{
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/financeiro/crud/Arquivobancario");
		}
		
		List<Arquivobancario> listaArquivos = arquivobancarioService.findArquivosBancarios(whereIn);
		
		for (Arquivobancario arquivobancario : listaArquivos) {
			if (!arquivobancario.getArquivobancariosituacao().equals(Arquivobancariosituacao.GERADO)) {
				request.addError("N�o � poss�vel cancelar um arquivo com situa��o diferente de 'GERADO'.");
				return new ModelAndView("redirect:/financeiro/crud/Arquivobancario");
			}
			if (arquivobancario.getArquivobancariocorrespondente() != null) {
				request.addError("N�o � poss�vel cancelar um arquivo com um arquivo correspondente relacionado.");
				return new ModelAndView("redirect:/financeiro/crud/Arquivobancario");
			}
		}
		movimentacaoService.doCancelar(whereIn);
		request.addMessage("Arquivo(s) banc�rio(s) cancelado(s) com sucesso.");
		return new ModelAndView("redirect:/financeiro/crud/Arquivobancario");
	}
}
