package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.CusteioProcessado;
import br.com.linkcom.sined.geral.bean.CusteioTipoDocumento;
import br.com.linkcom.sined.geral.bean.SituacaoCusteio;
import br.com.linkcom.sined.geral.bean.TipoDocumentoCusteioEnum;
import br.com.linkcom.sined.geral.service.CusteioProcessadoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.CusteioProcessadoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/financeiro/crud/HistoricoCusteio", authorizationModule=CrudAuthorizationModule.class)
public class CusteioProcessadoCrud extends CrudControllerSined<CusteioProcessadoFiltro,CusteioProcessado,CusteioProcessado>{

	private CusteioProcessadoService custeioProcessadoService;
	private RateioService rateioService;
	
	public void setCusteioProcessadoService(CusteioProcessadoService custeioProcessadoService) {this.custeioProcessadoService = custeioProcessadoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}

	@Override
	protected void listagem(WebRequestContext request,CusteioProcessadoFiltro filtro) throws Exception {
		request.setAttribute("listaSituacaoCusteio", SituacaoCusteio.values());
		
		super.listagem(request, filtro);
	}

	
	@Override
	protected void entrada(WebRequestContext request, CusteioProcessado form)throws Exception {
		if(SinedUtil.isListNotEmpty(form.getListaCusteioTipoDocumento())){
			for (CusteioTipoDocumento tipo : form.getListaCusteioTipoDocumento()) {
				if(TipoDocumentoCusteioEnum.PAGAR.equals(tipo.getTipoDocumentoCusteioEnum())){
					form.getListaTipoPagar().add(tipo);
				}else if(TipoDocumentoCusteioEnum.RECEBER.equals(tipo.getTipoDocumentoCusteioEnum())){
					form.getListaTipoReceber().add(tipo);
				} else if(TipoDocumentoCusteioEnum.FINANCEIRA.equals(tipo.getTipoDocumentoCusteioEnum())){
					form.getListaTipoMovimentacao().add(tipo);
				} else {
					form.getListaTipoEstoque().add(tipo);
				}
			}
			
		}
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,CusteioProcessadoFiltro filtro) throws CrudException {
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<CusteioProcessado> getLista(WebRequestContext request, CusteioProcessadoFiltro filtro) {
		return super.getLista(request, filtro);
	}
	
	public ModelAndView abrirPopUpPreencheObsercao(WebRequestContext request){
	String whereIn = (String) request.getParameter("cdCusteio");
		if(whereIn == null || whereIn.equals("")){
			request.addError("� necess�rio selecionar um registro de custeio para fazer o estorno.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		CusteioProcessado custeio = custeioProcessadoService.buscarCusteioPorId(Integer.parseInt(whereIn));
		if(custeio == null ||SinedUtil.isListEmpty(custeio.getListaCusteioTipoDocumento()) ){
			request.addError("O custeio n�o possui documentos para serem estornados");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if(SituacaoCusteio.ESTORNADO.equals(custeio.getSituacaoCusteio())){
			request.addError("� permitido realizar o estorno apenas de registros com a diferente de 'cancelado'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		
		request.setAttribute("acao", "estornarCusteioProcessado");
		request.setAttribute("descricao", "Observa��o");
		return new ModelAndView("direct:/crud/popup/popupJustificavaEstorno" ,"custeioProcessado", custeio);
	}
	
	public ModelAndView estornarCusteioProcessado(WebRequestContext request,CusteioProcessado custeioProcessado){
		custeioProcessadoService.estornarCusteioProcessado(custeioProcessado);
		request.addMessage("O processo de estorno foi realizado com sucesso!");
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView abrirRateio(WebRequestContext request){
		return rateioService.abrirRateio(request);
	}

}
