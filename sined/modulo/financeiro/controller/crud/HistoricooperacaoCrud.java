package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.geral.service.HistoricooperacaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.HistoricooperacaoFiltro;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Historicooperacao", authorizationModule=CrudAuthorizationModule.class)
public class HistoricooperacaoCrud extends CrudControllerSined<HistoricooperacaoFiltro, Historicooperacao, Historicooperacao>{

	private HistoricooperacaoService historicooperacaoService;
	
	public void setHistoricooperacaoService(
			HistoricooperacaoService historicooperacaoService) {
		this.historicooperacaoService = historicooperacaoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Historicooperacao form)
			throws Exception {
		if(form.getCdhistoricooperacao() == null &&
			!request.getBindException().hasErrors()){
			form.setTipohistoricooperacao(Tipohistoricooperacao.TRANSFERENCIA_ENTRE_CONTAS);
		}
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(Historicooperacao bean, BindException errors) {
		Historicooperacao historicoExistente = historicooperacaoService.findByTipo(bean.getTipohistoricooperacao());
		if(historicoExistente != null && !historicoExistente.getCdhistoricooperacao().equals(bean.getCdhistoricooperacao())){
			errors.reject( "001", "╔ permitido apenas um registro por 'Tipo de lanšamento'.");
		}
		super.validateBean(bean, errors);
	}
	
	public ModelAndView popUpVariaveisBase(WebRequestContext request, Historicooperacao bean) {
		String nomeCampoHistorico = request.getParameter("nomeCampoHistorico");
		List<GenericBean> listaVariavel = historicooperacaoService.findVariaveisByTipolancamento(bean.getTipohistoricooperacao());
		
		Collections.sort(listaVariavel, new Comparator<GenericBean>() {
			@Override
			public int compare(GenericBean o1, GenericBean o2) {
				return o1.getValue().toString().compareToIgnoreCase(o2.getValue().toString());
			}
		});
		
		return new ModelAndView("direct:/crud/popup/popUpVariavelHistoricoBase", "variavelshistoricobase", listaVariavel).addObject("nomeCampoHistorico", nomeCampoHistorico);
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Historicooperacao form) {
		return new ModelAndView("crud/historicooperacaoEntrada");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, HistoricooperacaoFiltro filtro) {
		return new ModelAndView("crud/historicooperacaoListagem");
	}
	
	public ModelAndView ajaxPreencheVariaveis(WebRequestContext request, Historicooperacao form){
		historicooperacaoService.findVariaveisByTipolancamento(form.getTipohistoricooperacao());
		return null;
	}
}
