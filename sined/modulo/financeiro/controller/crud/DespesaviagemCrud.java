package br.com.linkcom.sined.modulo.financeiro.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.DespesaviagemHistorico;
import br.com.linkcom.sined.geral.bean.Despesaviagemitem;
import br.com.linkcom.sined.geral.bean.Despesaviagemprojeto;
import br.com.linkcom.sined.geral.bean.Despesaviagemtipo;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemacao;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemdestino;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.service.ContacrmcontatoService;
import br.com.linkcom.sined.geral.service.DespesaviagemHistoricoService;
import br.com.linkcom.sined.geral.service.DespesaviagemService;
import br.com.linkcom.sined.geral.service.DespesaviagemtipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.OrigemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.filter.DespesaviagemFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/financeiro/crud/Despesaviagem", 
		authorizationModule=CrudAuthorizationModule.class
)
public class DespesaviagemCrud extends CrudControllerSined<DespesaviagemFiltro, Despesaviagem, Despesaviagem>{
	
	private DespesaviagemService despesaviagemService;
	private DespesaviagemtipoService despesaviagemtipoService;
	private EmpresaService empresaService;
	private ProjetoService projetoService;
	private DespesaviagemHistoricoService despesaviagemHistoricoService;
	private EnderecoService enderecoService;
	private ContacrmcontatoService contacrmcontatoService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setDespesaviagemtipoService(
			DespesaviagemtipoService despesaviagemtipoService) {
		this.despesaviagemtipoService = despesaviagemtipoService;
	}
	public void setDespesaviagemService(
			DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setDespesaviagemHistoricoService(DespesaviagemHistoricoService despesaviagemHistoricoService) {
		this.despesaviagemHistoricoService = despesaviagemHistoricoService;
	}
	public void setContacrmcontatoService(ContacrmcontatoService contacrmcontatoService) {
		this.contacrmcontatoService = contacrmcontatoService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, DespesaviagemFiltro filtro) throws Exception {
		String gerado = request.getParameter("mostrarReembolso");
		if(gerado != null && !StringUtils.isBlank(gerado)){
			request.setAttribute("mostrarReembolso", Boolean.TRUE);
		}
		List<Endereco> listaEndereco = new ArrayList<Endereco>();
		List<Contacrmcontato> listaContacrmcontato = new ArrayList<Contacrmcontato>();
			if(filtro.getDespesaviagemdestino()!=null){
				if (Despesaviagemdestino.CLIENTE.equals(filtro.getDespesaviagemdestino())&& filtro.getCliente()!=null) {
						listaEndereco = enderecoService.findAtivosByCliente(filtro.getCliente());					
				}else if (Despesaviagemdestino.FORNECEDOR.equals(filtro.getDespesaviagemdestino()) && filtro.getFornecedor()!=null){
						listaEndereco = enderecoService.findAtivosByFornecedor(filtro.getFornecedor());						
				}else if(Despesaviagemdestino.CONTA_CRM.equals(filtro.getDespesaviagemdestino()) && filtro.getContacrm()!=null){
					String cdcontacrm =  filtro.getContacrm().getCdcontacrm() != null ? filtro.getContacrm().getCdcontacrm()+"":"";
					listaContacrmcontato = contacrmcontatoService.findContatoEnderecoByContacrm(cdcontacrm);
				}				
			}
		request.setAttribute("listaEndereco", listaEndereco);
		request.setAttribute("listaContacrmcontato",listaContacrmcontato);
		request.setAttribute("listaSituacao", Situacaodespesaviagem.findAll());
	}
	
	@Override
	protected Despesaviagem criar(WebRequestContext request, Despesaviagem form) throws Exception {
		
		if ("true".equals(request.getParameter("copiar")) && form.getCddespesaviagem()!=null){
			form = despesaviagemService.loadForEntrada(form);
			form.setCddespesaviagem(null);
			form.setDtretornorealizado(null);
			form.setTotalrealizado(null);
			form.setSituacaodespesaviagem(Situacaodespesaviagem.EM_ABERTO);
			form.setReembolsogerado(null);
			
			List<Despesaviagemitem> listaDespesaviagemitem = form.getListaDespesaviagemitem();
			for (Despesaviagemitem despesaviagemitem : listaDespesaviagemitem) {
				despesaviagemitem.setCddespesaviagemitem(null);
				despesaviagemitem.setValorrealizado(null);
			}
			form.setListaDespesaviagemitem(listaDespesaviagemitem);
			
			List<Despesaviagemprojeto> listaDespesaviagemprojeto = form.getListaDespesaviagemprojeto();
			for (Despesaviagemprojeto despesaviagemprojeto : listaDespesaviagemprojeto) {
				despesaviagemprojeto.setCddespesaviagemprojeto(null);
			}
			form.setListaDespesaviagemprojeto(listaDespesaviagemprojeto);
			
			return form;
		}else {
			return super.criar(request, form);
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Despesaviagem form) throws Exception {
		if("editar".equals(request.getParameter("ACAO")) && !form.getSituacaodespesaviagem().equals(Situacaodespesaviagem.EM_ABERTO)){
			throw new SinedException("N�o pode editar uma despesa de viagem que n�o esteja com a situa��o 'EM ABERTO'");
		}
		
		if (form.getCddespesaviagem() != null) {
			request.setAttribute("listaAdiantamento", despesaviagemService.buildAdiantamentos(form, null));
			request.setAttribute("listaAcerto", despesaviagemService.buildAcertos(form));
			
			List<DespesaviagemHistorico> listaHistorico = despesaviagemHistoricoService.findForEntradaDespesaViagem(form);
			form.setListaDespesaviagemHistorico(listaHistorico);
		}
		
		List<Despesaviagemtipo> listaTipo = getListaTipo(form);
		request.setAttribute("listaTipo", despesaviagemtipoService.findAtivos(listaTipo));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		
		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getCddespesaviagem() != null && form.getListaDespesaviagemprojeto() != null && !form.getListaDespesaviagemprojeto().isEmpty()){
			List<Projeto> projetos = despesaviagemService.getProjetos(form.getListaDespesaviagemprojeto());
			listaProjeto = projetoService.findProjetosAtivos(new ArrayList<Projeto>(), true);
			if(listaProjeto != null && projetos != null && !projetos.isEmpty()){
				for(Projeto projeto : projetos){
					if(!listaProjeto.contains(projeto)){
						listaProjeto.add(projeto);
					}
				}
			}				
		} else {
			listaProjeto = projetoService.findProjetosAtivos(new ArrayList<Projeto>(), true);
		}
		request.setAttribute("listaProjeto", listaProjeto);
	}
	
	/**
	 * Retorna a lista de tipos de despesa de viagem da despesa de viagem passada por par�metro.
	 *
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Despesaviagemtipo> getListaTipo(Despesaviagem form) {
		List<Despesaviagemtipo> listaTipo = new ArrayList<Despesaviagemtipo>();
		List<Despesaviagemitem> lista = form.getListaDespesaviagemitem();
		for (Despesaviagemitem item : lista) {
			listaTipo.add(item.getDespesaviagemtipo());
		}
		return listaTipo;
	}
	
	public ModelAndView gerarCSV(WebRequestContext request, DespesaviagemFiltro filtro){
		filtro.setPageSize(Integer.MAX_VALUE);
		List<Despesaviagem> listaDespesaviagem = getLista(request, filtro).list();
		if(listaDespesaviagem == null || listaDespesaviagem.isEmpty()){
			request.addError("Nenhum dado para gerar o relat�rio.");
			return sendRedirectToAction(LISTAGEM);
		}
		
		StringBuilder csv = new StringBuilder();
		csv
		.append("Origem da viagem;")
		.append("Destino da viagem;")
		.append("Data da sa�da;")
		.append("Data de retorno realizado;")
		.append("Respons�vel pela despesa de viagem;")
		.append("Projeto(s);")
		.append("Situa��o;")
		.append("Itens;")
		.append("Valor Previsto;")
		.append("Valor Realizado;")
		.append("Adiantado;")
		.append("\n");
		
		for (Despesaviagem despesaviagem : listaDespesaviagem) {
			csv.append(despesaviagem.getOrigemviagem()!= null ? despesaviagem.getOrigemviagem() : "" ).append(";");

			if(despesaviagem.getCliente()==null && despesaviagem.getFornecedor()==null && despesaviagem.getContacrm()==null){
				csv.append(despesaviagem.getDestinoviagem() != null ? despesaviagem.getDestinoviagem() : "").append(";");				
			}else{
				if(despesaviagem.getCliente()!=null){
					csv.append(despesaviagem.getCliente().getNome()!=null?despesaviagem.getCliente().getNome():" ");
				}
				if(despesaviagem.getFornecedor()!=null){
					csv.append(despesaviagem.getFornecedor().getNome()!=null?despesaviagem.getFornecedor().getNome():" ");
				}
				if(despesaviagem.getContacrm()!=null){
					csv.append(despesaviagem.getContacrm().getNome()!=null?despesaviagem.getContacrm().getNome():" ");
				}
				csv.append(";");
			}
			
			csv.append(despesaviagem.getDtsaida() != null ? despesaviagem.getDtsaida() : "").append(";");
			csv.append(despesaviagem.getDtretornorealizado() != null ? despesaviagem.getDtretornorealizado() : "").append(";");
			csv.append(despesaviagem.getColaborador() != null ? despesaviagem.getColaborador().getNome() : "").append(";");
			csv.append(despesaviagem.getProjetos() != null ? despesaviagem.getProjetos() : "").append(";");
			csv.append(despesaviagem.getSituacaodespesaviagem() != null ? despesaviagem.getSituacaodespesaviagem().getDescricao() : "");
			csv.append(";");
			
			int tamanho = despesaviagem.getListaDespesaviagemitem()!= null ? despesaviagem.getListaDespesaviagemitem().size() : 0;
			Despesaviagemitem despesaviagemitem;
			for (int i = 0; i < tamanho; i++) {
				despesaviagemitem = despesaviagem.getListaDespesaviagemitem().get(i);
				csv.append(despesaviagemitem.getDespesaviagemtipo() != null ? despesaviagemitem.getDespesaviagemtipo().getNome() + ";" : ";");
				csv.append(despesaviagemitem.getValorprevisto() != null ? despesaviagemitem.getValorprevisto() + ";" : ";");
				csv.append(despesaviagemitem.getValorrealizado() != null ? despesaviagemitem.getValorrealizado() + ";" : ";");
				if(i != tamanho -1) csv.append("\n;;;;;;;");
			}
			csv.append(despesaviagem.getTotaladiantamento() != null ? despesaviagem.getTotaladiantamento().toString()+";" : ";");
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "despesaviagem_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	public void buscarValorbaseAjax(WebRequestContext request, Despesaviagemitem despesaviagemitem){
		
		String valorbaseStr = "";
		
		if (despesaviagemitem.getDespesaviagemtipo()!=null){
			Money valorbase = despesaviagemtipoService.load(despesaviagemitem.getDespesaviagemtipo()).getValorbase();
			if (valorbase!=null && valorbase.toLong()>0){
				valorbaseStr = valorbase.toString();
			}
		}
		
		View.getCurrent().print("var valorbaseStr = '" + valorbaseStr + "';");
	}
	
	@Override
	protected ListagemResult<Despesaviagem> getLista(WebRequestContext request,DespesaviagemFiltro filtro) {
		ListagemResult<Despesaviagem> listResult = super.getLista(request, filtro);
		
		List<Despesaviagem> list = listResult.list();	
		
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cddespesaviagem", ",");
		if(whereIn != null && StringUtils.isNotBlank(whereIn)){
			list.removeAll(list);
			list.addAll(despesaviagemService.loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));
			List<OrigemBean> listaAdiantamento = despesaviagemService.buildAdiantamentos(null, whereIn);
			despesaviagemService.calculaTotalAdiantamento(list, listaAdiantamento);
		}
		
		despesaviagemService.preencheTotais(filtro);
		
		return listResult;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Despesaviagem bean)throws Exception {
		
		DespesaviagemHistorico despesaviagemHistorico = new DespesaviagemHistorico();
		despesaviagemHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		despesaviagemHistorico.setUsuario(SinedUtil.getUsuarioLogado());
		
		if(bean.getCddespesaviagem() == null){
			despesaviagemHistorico.setAcao(Despesaviagemacao.CRIADA);
		}else{
			despesaviagemHistorico.setAcao(Despesaviagemacao.ALTERADA);
		}
		super.salvar(request, bean);
		
		despesaviagemHistorico.setDespesaviagem(bean);
		despesaviagemHistoricoService.saveOrUpdate(despesaviagemHistorico);
	}

	/**
	 * M�todo ajax para buscar os enderecos do cliente/fornecedor/contato crm
	 *
	 * @param request
	 * @author Jo�o Vitor
	 */
	public void ajaxBuscaEndereco(WebRequestContext request){
		String cdcampo = request.getParameter("cdcampo");
		String destinoViagem = request.getParameter("destinoViagem");
		
		if(StringUtils.isNotBlank(cdcampo) && StringUtils.isNotBlank(destinoViagem)){		
			if ("CLIENTE".equalsIgnoreCase(destinoViagem)) {
				List<Endereco> listaEndereco = enderecoService.findAtivosByCliente(new Cliente(Integer.parseInt(cdcampo)));
				View.getCurrent().println(SinedUtil.convertToJavaScript(listaEndereco, "listaEndereco", null));
			} else if ("FORNECEDOR".equalsIgnoreCase(destinoViagem)){
				List<Endereco> listaEndereco = enderecoService.findAtivosByFornecedor(new Fornecedor(Integer.parseInt(cdcampo)));
				View.getCurrent().println(SinedUtil.convertToJavaScript(listaEndereco, "listaEndereco", null));
			} else if("CONTA_CRM".equalsIgnoreCase(destinoViagem)){
				List<Contacrmcontato> listaContacrmcontato = contacrmcontatoService.findContatoEnderecoByContacrm(cdcampo);
				View.getCurrent().println(SinedUtil.convertToJavaScript(listaContacrmcontato, "listaContacrmcontato", "enderecoForCombo"));
			}
		}
		request.getServletResponse().setContentType("text/html");
	}
}