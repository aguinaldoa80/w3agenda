package br.com.linkcom.sined.modulo.rh.exportacao;

import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradordependente;
import br.com.linkcom.sined.geral.bean.Dadobancario;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.util.SinedDateUtils;

public class ExportacaoBodyDetalhe {
	private static final String  TIPO = "F";
	private static final String TIPODEPENDENTE = "D";

	private Colaborador colaborador;
	protected Set<Colaboradordependente> listaColaboradordependente;
	protected Colaboradorcargo colaboradorCargo;

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public Set<Colaboradordependente> getListaColaboradordependente() {
		return listaColaboradordependente;
	}
	public void setListaColaboradordependente(Set<Colaboradordependente> listaColaboradordependente) {
		this.listaColaboradordependente = listaColaboradordependente;
	}
	public Colaboradorcargo getColaboradorCargo() {
		return colaboradorCargo;
	}
	public void setColaboradorCargo(Colaboradorcargo colaboradorCargo) {
		this.colaboradorCargo = colaboradorCargo;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}

	public String getMatricula() {
		if (this.colaborador.getAlterado()) {
			return "A";
		}
		return "N";
	}

	/**
	 * M�todo para montar o corpo do arquivo
	 * 
	 * @return
	 * @author Jo�o Paulo Zica
	 * @author Fernando Boldrini
	 */
	public String gerarLinha(){
		StringBuilder builder = new StringBuilder();
		
		//Variaveis auxiliares
		String municipioNaturalidade = "";
		String ufNaturalidade = "";
		if(this.getColaborador().getMunicipionaturalidade() != null) {
			municipioNaturalidade = this.getColaborador().getMunicipionaturalidade().getNome();
			ufNaturalidade = this.getColaborador().getMunicipionaturalidade().getUf().getSigla();
		}


		builder.append(ExportacaoBodyDetalhe.TIPO);//Tipo
		builder.append(this.getMatricula()); //Nova matricula ou alterar cadastro

		//Dados Pessoais
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getNome(), 70));//Nome do funcionario
		builder.append(SinedDateUtils.toString(this.getColaborador().getDtnascimento()));//Data de nascimento do funcionario
		builder.append(this.getColaborador().getSexo().getCdsexo().equals(Sexo.MASCULINO) ? "M" : "F");//Sexo
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getNacionalidade().getDescricao(), 20)); //nacionalidade
		builder.append(AbstractExport.fillAlfa(ufNaturalidade, 2));//UF da naturalidade
		builder.append(AbstractExport.fillAlfa(municipioNaturalidade, 60));//cidade da naturalidade
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getEmail(), 50));
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getMae(), 50)); //nome da m�e
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getPai(), 50));//nome do pai
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getEtnia().getCdetnia().toString(), 4));//Ra�a ou Etnia
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getColaboradortipo().getCdcolaboradortipo().toString(), 4));//Tipo de empregado
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getColaboradordeficiente() != null ? this.getColaborador().getColaboradordeficiente().getCdcolaboradordeficiencia().toString() : "0", 4));//Tipo de deficiencia
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getGrauinstrucao().getCdgrauinstrucao().toString(), 4));//Grau de instru��o
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getEstadocivil().getCdestadocivil().toString(), 4));//Estado civil
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getTelefoneColaborador() != null ? this.getColaborador().getTelefoneColaborador() : "", 10));//Telefone
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getCelularColaborador() != null ? this.getColaborador().getCelularColaborador() : "", 10));//Celular
		
		//Endere�o
		if(this.getColaborador().getEndereco() != null) {
			builder.append(AbstractExport.fillAlfa(this.getColaborador().getEndereco().getLogradouro(), 240));//Logradouro
			builder.append(AbstractExport.fillAlfa(this.getColaborador().getEndereco().getNumero(), 5));//Numero
			builder.append(AbstractExport.fillAlfa(this.getColaborador().getEndereco().getComplemento(), 60));//Complemento
			builder.append(AbstractExport.fillAlfa(this.getColaborador().getEndereco().getBairro(), 60));//Bairro
			builder.append(AbstractExport.fillAlfa(this.getColaborador().getEndereco().getMunicipio().getUf().getSigla(), 2));//UF
			builder.append(AbstractExport.fillAlfa(this.getColaborador().getEndereco().getMunicipio().getNome(), 60));//Municipio
			builder.append(AbstractExport.fillAlfa(this.getColaborador().getEndereco().getCep() != null ? StringUtils.replace(this.getColaborador().getEndereco().getCep().getValue(), "-", "") : "", 8));//Cep
			builder.append(AbstractExport.fillAlfa(this.getColaborador().getEndereco().getCaixapostal(), 50));//Caixa postal
		}
		
		//CTPS
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getCtps(), 7));//CTPS
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getSeriectps().toString(), 5));//Serie CTPS
		builder.append(this.getColaborador().getUfctps().getSigla());//UF do CTPS
		builder.append(SinedDateUtils.toString(this.getColaborador().getDtEmissaoCtps()));//Data emiss�o do CTPS
	
		//RG
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getRg(), 10));//RG
		builder.append(SinedDateUtils.toString(this.getColaborador().getDtemissaorg()));//Data emiss�o  
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getOrgaoemissorrg(), 10));//UF do RG	      
		
		//CPF
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getCpf().getValue(), 15));//CPF
		
		//DIRF
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getDirf().getCddirf().toString(),4));
		
		//Inscri��o
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getTipoinscricao().getCdtipoinscricao().toString(), 3));//Tipo de inscri��o
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getNumeroinscricao(), 11));//Numero da inscricao
		
		//Titulo Eleitoral
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getTituloeleitoral(), 15));//Titulo eleitoral
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getZonaeleitoral(), 10));//Zona eleitoral
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getSecaoeleitoral(), 10));//Se��o Eleitoral
		
		//Documento militar
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getTipodocumentomilitar() != null ? this.getColaborador().getTipodocumentomilitar().getCdtipodocumentomilitar().toString() : "", 4));//Categoria documento militar
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getDocumentomilitar(), 20));//Documento militar
		
		//Dados banc�rios
		
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getColaboradorformapagamento().getCdcolaboradorformapagamento().toString(), 3));//Forma de pagamento
		Dadobancario dadobancario = this.getColaborador().getDadobancario();
		if(dadobancario != null){
			builder.append(AbstractExport.fillAlfa(dadobancario.getBanco() != null ? dadobancario.getBanco().getNumero().toString() : "", 3));//Banco
			builder.append(AbstractExport.fillAlfa(dadobancario.getTipoconta() != null ? dadobancario.getTipoconta().getCdtipoconta().toString() : "", 3));//Tipo de conta
			builder.append(AbstractExport.fillAlfa(dadobancario.getAgencia(), 10));//Agencia
			builder.append(AbstractExport.fillAlfa(dadobancario.getDvagencia(), 2));//DV da Agencia
			builder.append(AbstractExport.fillAlfa(dadobancario.getConta(), 10));//Conta
			builder.append(AbstractExport.fillAlfa(dadobancario.getDvconta(), 2));//DV da conta
			builder.append(AbstractExport.fillAlfa(dadobancario.getOperacao(), 5));//Opera��o
		}
		
		//Dados Profissionais
		builder.append(SinedDateUtils.toString(this.getColaborador().getDtadmissao()));//Data admiss�o
		builder.append(AbstractExport.fillAlfa(this.getColaboradorCargo().getDepartamento().getCodigofolha().toString(), 10));//Departamento
		builder.append(AbstractExport.fillAlfa(this.getColaboradorCargo().getCargo().getCodigoFolha() != null ? this.getColaboradorCargo().getCargo().getCodigoFolha().toString() : "", 10));//Cargo
		builder.append(AbstractExport.fillAlfa(this.getColaboradorCargo().getSindicato() != null ? this.getColaboradorCargo().getSindicato().getCodigofolha().toString() : "", 10));//Sindicato
		builder.append(AbstractExport.fillAlfa(this.getColaboradorCargo().getSalario() != null ? this.getColaboradorCargo().getSalario().toString().replace(",", "").replace(".", "") : "", 13));//Salario
		builder.append(AbstractExport.fillAlfa(this.getColaboradorCargo().getRegimecontratacao().getCdregimecontratacao().toString(), 4));//Tipo de Contratacao
		builder.append(AbstractExport.fillAlfa(this.getColaborador().getColaboradorsituacao().getCdcolaboradorsituacao().toString(), 4));//Situa��o do colaborador
		//Outros (Aguardar implementa��o dessas categorias)
		//builder.append(fillAlfa(this.getColaborador().getCentroResultado().getCdCentroResultado().toString(), 10));//Centro de resultado
		//builder.append(fillAlfa(this.getColaborador().getTomador().getCdTomador().toString(), 10));//Tomador
		//builder.append(fillAlfa(this.getColaborador().getUnidadeAdministrativa().getCdUnidadeAdministrativa().toString(), 10));//Unidade administrativa
		//builder.append(fillAlfa(this.getColaborador().getCentroCusto().getCdCentroCusto().toString(), 10));//Centro de custo
		builder.append(AbstractExport.fillBranco(40));//pular as 4 categorias anteriores
		builder.append(SinedDateUtils.toString(this.getColaborador().getDtaltera()));
		
		//Dependentes
		if(this.getListaColaboradordependente() != null) {
			//builder.append("\n");
			builder.append( this.getListaDependentes(this.getListaColaboradordependente()));	    
		}

		return builder.toString();
	}

	private String getListaDependentes(Set<Colaboradordependente> listaColaboradordependente) {
		String strDependentes = "";

		for (Colaboradordependente colaboradordependente : listaColaboradordependente) {
			strDependentes += "\r\n" + this.getLinhaDependente(colaboradordependente);
		}

		return strDependentes;
	}

	/**
	 * 
	 * @param dependente
	 * @return
	 * @author Fernando Boldrini
	 */
	private String getLinhaDependente(Colaboradordependente dependente) {

		StringBuilder strDependente= new StringBuilder();

		strDependente.append(AbstractExport.fillAlfa(ExportacaoBodyDetalhe.TIPODEPENDENTE, 2)); //Tipo de dependente "D"
		strDependente.append(AbstractExport.fillAlfa(dependente.getNome(), 70));//Nome
		strDependente.append(AbstractExport.fillAlfa(dependente.getTipodependente().getCdtipodependente().toString(), 3));//Tipo de dependencia
		strDependente.append(dependente.getDtbaixairrf() != null ? 
				SinedDateUtils.toString(dependente.getDtbaixairrf()) : AbstractExport.fillBranco(10));//Data da baixa do IRRF
		strDependente.append(SinedDateUtils.toString(dependente.getDtnascimento()));//Data de nascimento
		strDependente.append(AbstractExport.fillAlfa(dependente.getCartorio(), 15));//Cartorio
		strDependente.append(AbstractExport.fillAlfa(dependente.getRegistro(), 10));//Registro
		strDependente.append(AbstractExport.fillAlfa(dependente.getLivro(), 10));//Livro
		strDependente.append(AbstractExport.fillAlfa(dependente.getFolha(), 5));//Folha
		strDependente.append(dependente.getDtbaixasf() != null ?
				SinedDateUtils.toString(dependente.getDtbaixasf()) : AbstractExport.fillBranco(10));//Data da baixa salario familia
		strDependente.append(dependente.getDtentregacertidao() != null ?
				SinedDateUtils.toString(dependente.getDtentregacertidao()) : AbstractExport.fillBranco(10));//data da entrega da certid�o
		strDependente.append(dependente.getDtpensao() != null ?
				SinedDateUtils.toString(dependente.getDtpensao()) : AbstractExport.fillBranco(10));//Data aquisi��o da pens�o
		strDependente.append(dependente.getDtbaixapensao() != null ?
				SinedDateUtils.toString(dependente.getDtbaixapensao()) : AbstractExport.fillBranco(10));//Data da baixa da pens�o alimenticia
		strDependente.append(AbstractExport.fillAlfa(dependente.getColaboradorformapagamento() != null ?
				dependente.getColaboradorformapagamento().getCdcolaboradorformapagamento().toString() : "", 3));//Forma de pagamento da pens�o
		
		//Sexo:
		strDependente.append(AbstractExport.fillAlfa(dependente.getSexo() != null ? dependente.getSexo().toChar() : "M", 1));//Caso o sexo n�o foi determinado, padronizar como masculino
		
		return strDependente.toString();

	}

}
