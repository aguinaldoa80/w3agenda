package br.com.linkcom.sined.modulo.rh.exportacao;

public class ExportacaoHeader {

	protected static final Integer TIPO_ARQUIVO = 1;
	protected static final String  TEXTO = "E";
	protected String nomeArquivo;
	protected String identificadorArquivo;
	
	public String getNomeArquivo(){
	    return nomeArquivo;
	}
	
	public String getIdentificadorArquivo(){
	    return this.identificadorArquivo;
	}
	
	public Integer getTIPO_ARQUIVO(){
	    return TIPO_ARQUIVO;
	}
	
	public String getTEXTO(){
	    return TEXTO;
	}
		
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	
	public void setIdentificadorArquivo(String identificadorArquivo) {
		this.identificadorArquivo = identificadorArquivo;
	}
	
	/**
	 * M�todo para montar o cabe�alho do arquivo.
	 * 
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public String gerarLinha(){
		StringBuilder builder = new StringBuilder();
		
		//cabe�alho
		builder.append(ExportacaoHeader.TIPO_ARQUIVO);
		builder.append(AbstractExport.fillAlfa(ExportacaoHeader.TEXTO, 10));
		builder.append(AbstractExport.fillAlfa(getNomeArquivo(), 50));
		builder.append(AbstractExport.fillAlfa(getIdentificadorArquivo(), 7));
		    
		return builder.toString();
	}
}
