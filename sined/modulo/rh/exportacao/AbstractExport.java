package br.com.linkcom.sined.modulo.rh.exportacao;

import br.com.linkcom.neo.types.Money;

public class AbstractExport {
	
	/**
	 * M�todo para formatacao de tipo Money
	 * @param money
	 * @param tamanho
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public static String formatMoney(Money money, int tamanho){
		if(money == null){
			money = new Money("0");
		}
		return fillNum(money.toLong(), tamanho);
	}

	/**
	 * M�todo para preencher um campo inteiro com zeros o tamanho informado
	 * 
	 * @param i
	 * @param tamanho
	 * @return
	 * @author Jo�o Paulo
	 */
	public static String fillNum(Long i, int tamanho){
		if(i == null)
			i = 0l;
		
		StringBuilder res;
		if(i.toString().length() > tamanho){
			res = new StringBuilder(i.toString().substring(0, tamanho)); 
//			throw new RuntimeException("O n�mero "+i+" excedeu o tamanho m�ximo "+tamanho);
		}else{
			res = new StringBuilder("");
			while(i.toString().length() < tamanho){
				res.append("0");
				tamanho--;
			}
			res.append(i);
		}
		return res.toString();
	}

	/**
	 * M�todo para preencher um campo inteiro com espa�os o tamanho informado
	 * 
	 * @param i
	 * @param tamanho
	 * @return
	 * @author Jo�o Paulo, Fernando Boldrini
	 */
	public static String fillAlfa(String res, int tamanho){
				
		if(res == null)
			res = "";
		
		
		StringBuilder str;// = new StringBuilder(res);
		
		if(res.length() > tamanho){
			str = new StringBuilder(res.substring(0, tamanho));
		}else{
			str = new StringBuilder(res);
			while(res.length() < tamanho){
				str.append(" ");
				tamanho--;
			}
		}
		
		return str.toString();
	}

	/**
	 * M�todo para preencher com string o tamanho informado
	 * 
	 * @param s
	 * @param tamanho
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public static String fill(String s, int tamanho){
		String res = "";
		while(res.length() < tamanho){
		    res = res+s;
		}
		return res;
	}

	/**
	 * M�todo para preencher com espa�os o tamanho informado
	 * 
	 * @param tamanho
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public static String fillBranco(int tamanho){
		return fill(" ", tamanho);
	}

	/**
	 * M�todo para preencher com zeros o tamanho informado
	 * 
	 * @param tamanho
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public static String fillZero(int tamanho){
		return fill("0", tamanho);
	}
}
