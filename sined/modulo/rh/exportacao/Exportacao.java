package br.com.linkcom.sined.modulo.rh.exportacao;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.util.SinedException;

public class Exportacao {
	public static int numSequencia;
	protected ExportacaoHeader exportacaoHeader;
	protected List<ExportacaoBody> listaExportacaoBody = new ArrayList<ExportacaoBody>();
	
	public Exportacao() {
		numSequencia = 0;
	}
	
	public boolean addColaborador(ExportacaoBody o) {
		return listaExportacaoBody.add(o);
	}
	public List<ExportacaoBody> getListaExportacaoBody() {
		return listaExportacaoBody;
	}
	public ExportacaoHeader getExportacaoHeader() {
		return exportacaoHeader;
	}
	public void setListaExportacaoBody(List<ExportacaoBody> exportacaoBodyList) {
		this.listaExportacaoBody = exportacaoBodyList;
	}
	public void setExportacaoHeader(ExportacaoHeader exportacaoHeader) {
		this.exportacaoHeader = exportacaoHeader;
	}
	public static int incNumSequencia() {
		return ++numSequencia;
	}
		
	/**
	 * M�todo que vai fazer a grava��o no arquivo
	 * 
	 * @param outputStream
	 * @param sequenciaArquivo
	 * @author Jo�o Paulo Zica
	 */
	public void writeFile(OutputStream outputStream){
		PrintWriter out = new PrintWriter(outputStream);
		String[] linhas = getLinhas().split("\r\n");
		for (String string : linhas) {
			out.println(string);
		}
		out.flush();
	}	
	
	/**
	 * M�todo para montar o cabe�alho e corpo do arquivo.
	 * 
	 * @param sequenciaArquivo
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public String getLinhas() {
		StringBuilder builder = new StringBuilder();
		try {
			builder.append(exportacaoHeader.gerarLinha() + "\r\n");
		} catch (Exception e) {
			throw new SinedException("Erro ao gerar Header do arquivo", e);
		}		
		for (ExportacaoBody exportacaoBody : listaExportacaoBody) {
			try {
				builder.append(exportacaoBody.getExportacaoBodyDetalhe().gerarLinha() + "\r\n");		
			} catch (Exception e) {
				throw new SinedException("Erro ao gerar Corpo do arquivo", e);
			}
		}
		
		return builder.toString();
		
	}
}
