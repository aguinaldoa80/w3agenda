package br.com.linkcom.sined.modulo.rh.exportacao;

public class ExportacaoBody {
	protected ExportacaoBodyDetalhe exportacaoBodyDetalhe;
	
	public ExportacaoBodyDetalhe getExportacaoBodyDetalhe() {
		return exportacaoBodyDetalhe;
	}
	public void setExportacaoBodyDetalhe(ExportacaoBodyDetalhe exportacaoBodyDetalhe) {
		this.exportacaoBodyDetalhe = exportacaoBodyDetalhe;
	}
}
