package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwdocumentocomissaodesempenhoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/rh/relatorio/Emitircomissionamentodesempenho", authorizationModule=ReportAuthorizationModule.class)
public class ComissionamentodesempenhoReport extends SinedReport<VwdocumentocomissaodesempenhoFiltro>{

	private ComissionamentoService comissionamentoService;
	
	
	public void setComissionamentoService(
			ComissionamentoService comissionamentoService) {
		this.comissionamentoService = comissionamentoService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	VwdocumentocomissaodesempenhoFiltro filtro) throws Exception {
		return comissionamentoService.gerarRelatorioComissionamentodesempenho(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "comissionamentoDesempenho";
	}

	@Override
	public String getTitulo(VwdocumentocomissaodesempenhoFiltro filtro) {
		return "Comissionamento por Desempenho";
	}

}
