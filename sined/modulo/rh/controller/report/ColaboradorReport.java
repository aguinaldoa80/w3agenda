package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ColaboradorReportFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/rh/relatorio/Colaborador",
	authorizationModule=ReportAuthorizationModule.class
)
public class ColaboradorReport extends SinedReport<ColaboradorReportFiltro> {

	private ColaboradorService colaboradorService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ColaboradorReportFiltro filtro) throws Exception {
		return colaboradorService.createRelatorioColaboradores(filtro);
	}
	
	@Override
	public String getTitulo(ColaboradorReportFiltro filtro) {
		return "RELATÓRIO DE COLABORADORES";
	}
	
	@Override
	public String getNomeArquivo() {
		return "colaborador";
	}
}
