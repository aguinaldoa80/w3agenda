package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.util.controller.SinedReport;


@Controller(path = "/rh/relatorio/FichaColaborador", authorizationModule=ReportAuthorizationModule.class)
public class FichacolaboradorReport extends SinedReport<Object>{
	private ColaboradorService colaboradorService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, Object filtro) throws Exception {
		String colaboradoresSelecionados = request.getParameter("selectedItens");
		return colaboradorService.gerarRelatorioFichaColaborador(colaboradoresSelecionados);
	}
	
	@Override
	public String getTitulo(Object filtro) {
		return null;
	}
	
	@Override
	public String getNomeArquivo() {
		return "fichaColaborador";
	}

}
