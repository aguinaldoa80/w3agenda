package br.com.linkcom.sined.modulo.rh.controller.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.geral.service.ColaboradordespesaService;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ColaboradorPagamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ColaboradorPagamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;


@Controller(path="/rh/relatorio/ColaboradorPagamento", authorizationModule=CrudAuthorizationModule.class)
public class ColaboradorPagamentoReport extends SinedReport<ColaboradorPagamentoFiltro>{

	private ColaboradordespesaService colaboradordespesaService;
	
	public void setColaboradordespesaService(
			ColaboradordespesaService colaboradordespesaService) {
		this.colaboradordespesaService = colaboradordespesaService;
	}
	
	@Override
	protected ModelAndView getFiltroModelAndView(WebRequestContext request,
			ColaboradorPagamentoFiltro filtro) {
		request.setAttribute("listaSituacao", new ArrayList<Colaboradordespesasituacao>(Arrays.asList(Colaboradordespesasituacao.values())));
		return super.getFiltroModelAndView(request, filtro);
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			ColaboradorPagamentoFiltro filtro)
			throws ResourceGenerationException {
		// TODO Auto-generated method stub
		return super.doFiltro(request, filtro);
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			ColaboradorPagamentoFiltro filtro) throws Exception {
		Report report = new Report("/rh/colaboradorPagamento");
		if(filtro.getDtPagamentoIni() != null && filtro.getDtPagamentoFim() != null && filtro.getDtPagamentoIni().compareTo(filtro.getDtPagamentoFim()) > 0){
			throw new SinedException("Data inicial n�o pode ser maior que data final.");
		}
		List<ColaboradorPagamentoBean> lista = colaboradordespesaService.findForReportPagamentoColaborador(filtro);
		if(SinedUtil.isListEmpty(lista)){
			throw new SinedException("N�o existem dados para os filtros informados.");
		}
		report.setDataSource(lista);
		return report;
	}

	@Override
	public String getNomeArquivo() {
		return "pagamento_colaborador";
	}

	@Override
	public String getTitulo(ColaboradorPagamentoFiltro filtro) {
		return "Pagamento por colaborador";
	}

}
