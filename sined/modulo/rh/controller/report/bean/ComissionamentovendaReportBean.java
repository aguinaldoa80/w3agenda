package br.com.linkcom.sined.modulo.rh.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class ComissionamentovendaReportBean {

	private Date vencimento;
	private String nomecliente;
	private String pvnomecliente;
	private String nomevendedor;
	private Integer cdvenda;
	private Integer cdpedidovenda;
	private Money valorcomissao;
	private Money valorvenda;
	
	public ComissionamentovendaReportBean(){}
		
	public ComissionamentovendaReportBean(Date vencimento, String nomecliente, String pvnomecliente,
			String nomevendedor, Integer cdvenda, Integer cdpedidovenda, Long valorcomissao) {
		super();
		this.vencimento = vencimento;
		this.nomecliente = nomecliente;
		this.pvnomecliente = pvnomecliente;
		this.nomevendedor = nomevendedor;
		this.cdvenda = cdvenda;
		this.cdpedidovenda = cdpedidovenda;
		this.valorcomissao = valorcomissao != null ? new Money(valorcomissao,true) : null;		
	}

	public Date getVencimento() {
		return vencimento;
	}
	public String getNomecliente() {
		return nomecliente;
	}
	public String getNomevendedor() {
		return nomevendedor;
	}
	public Integer getCdvenda() {
		return cdvenda;
	}
	public Money getValorcomissao() {
		return valorcomissao;
	}
	public Money getValorvenda() {
		return valorvenda;
	}
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public String getPvnomecliente() {
		return pvnomecliente;
	}
	
	public void setPvnomecliente(String pvnomecliente) {
		this.pvnomecliente = pvnomecliente;
	}	
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}
	public void setNomecliente(String nomecliente) {
		this.nomecliente = nomecliente;
	}
	public void setNomevendedor(String nomevendedor) {
		this.nomevendedor = nomevendedor;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setValorcomissao(Money valorcomissao) {
		this.valorcomissao = valorcomissao;
	}
	public void setValorvenda(Money valorvenda) {
		this.valorvenda = valorvenda;
	}
}
