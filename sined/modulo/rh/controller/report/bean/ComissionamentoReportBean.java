package br.com.linkcom.sined.modulo.rh.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class ComissionamentoReportBean {
	
	private Date vencimento;
	private Money valordocumento;
	private String nomecliente;
	private String responsavelcontrato;
	private String descricaocontrato;
	private Money valorbrutocontrato;
	private Money valorliquidocontrato;
	private String contratotipo;
	private Integer ordemcomissao;
	private String nomecolaboradorfornecedor;
	private Double percentual;
	private Money valorcomissao;
	private String nomevendedor;	
	private Double percentualvendedor;
	private Money valorcomissaovendedor;
	private Money valorrepassado;
	private Integer cdcontrato;
	private Integer cddocumento;
	
	private Integer cdvendedor;
	private Integer cdcolaborador;
	private Integer cdpessoacomissao;
	private Integer cddocumentocomissao;
	private Date proximovencimento;
	
	public ComissionamentoReportBean(){}	
	
	public ComissionamentoReportBean(Integer cddocumentocomissao, Integer cddocumento, Integer cdpessoacomissao, Integer cdcontrato, Date vencimento, Long valordocumento, 
			String nomecliente, String responsavelcontrato,	String descricaocontrato, Long valorbrutocontrato, String contratotipo, 
			Integer ordemcomissao, String nomecolaboradorfornecedor, Double percentual, Long valoratualdoc, Long valorcomissionamento, 
			String nomevendedor, Double percentualvendedor, Long valorcomissionamentovendedor, Integer cdvendedor, Integer cdcolaborador, Long _valorRepassado, Date _proximoVencimento) {
		this.cdcontrato = cdcontrato;
		this.vencimento = vencimento;
		this.valordocumento = valordocumento != null ? new Money(valordocumento/100) : null;
		this.nomecliente = nomecliente;
		this.responsavelcontrato = responsavelcontrato;
		this.descricaocontrato = descricaocontrato;
		this.valorbrutocontrato = new Money(valorbrutocontrato/100);		
		this.contratotipo = contratotipo;
		this.ordemcomissao = ordemcomissao;
		this.nomecolaboradorfornecedor = nomecolaboradorfornecedor;
		this.percentual = percentual;
		this.valorcomissao = valorcomissionamento != null ? new Money(valorcomissionamento/100) : null;
		this.nomevendedor = nomevendedor;
		this.percentualvendedor = percentualvendedor;
		this.valorcomissaovendedor = valorcomissionamentovendedor != null ? new Money(valorcomissionamentovendedor/100) : null;
		this.cdvendedor = cdvendedor;
		this.cdcolaborador = cdcolaborador;
		this.cdpessoacomissao = cdpessoacomissao;
		this.cddocumentocomissao = cddocumentocomissao;
		this.cddocumento = cddocumento;
		this.valorrepassado = _valorRepassado != null && _valorRepassado > 0 ? new Money(_valorRepassado/100) : null;
		this.proximovencimento = _proximoVencimento;
	}
	
	public ComissionamentoReportBean(Integer cddocumentocomissao, Integer cddocumento, Integer cdpessoacomissao, Integer cdcontrato, Date vencimento, 
			Long valordocumento, String nomecolaboradorfornecedor, String nomecliente, String descricaocontrato, Long valorcomissionamento,
			Double percentual, Long valoratualdoc, Long valorbrutocontrato, String contratotipo, String nomevendedor, 
			Double percentualvendedor, Long valorcomissionamentovendedor, Integer cdvendedor, Integer cdcolaborador, Long valorrepassado, Date _proximoVencimento) {		
		this.cdcontrato = cdcontrato;
		this.vencimento = vencimento;
		this.valordocumento = valordocumento != null ? new Money(valordocumento/100) : null;
		this.nomecliente = nomecliente;		
		this.descricaocontrato = descricaocontrato;		
		this.valorcomissao = valorcomissionamento != null ? new Money(valorcomissionamento/100) : null;
		this.contratotipo = contratotipo;		
		this.nomecolaboradorfornecedor = nomecolaboradorfornecedor;	
		this.percentual = percentual;
		this.valorbrutocontrato = new Money(valorbrutocontrato/100);
		this.nomevendedor = nomevendedor;
		this.percentualvendedor = percentualvendedor;
		this.valorcomissaovendedor = valorcomissionamentovendedor != null &&  valorcomissionamentovendedor > 0 ? new Money(valorcomissionamentovendedor/100) : 
			percentualvendedor != null ? new Money(this.valorbrutocontrato.getValue().doubleValue()*percentualvendedor/100) : null;
		this.cdvendedor = cdvendedor;
		this.cdcolaborador = cdcolaborador;
		this.cdpessoacomissao = cdpessoacomissao;
		this.cddocumentocomissao = cddocumentocomissao;
		this.cddocumento = cddocumento;
		this.valorrepassado = valorrepassado != null && valorrepassado > 0 ? new Money(valorrepassado/100) : null;
		this.proximovencimento = _proximoVencimento;
	}
	
	public ComissionamentoReportBean(Integer cdcontrato, Integer cddocumento, Integer cdpessoacomissao, Date vencimento, Money valordocumento,
			String nomecolaboradorfornecedor, String nomecliente, String descricaocontrato, Money valorcomissionamento, Double percentual,
			Money valorbrutocontrato, String contratotipo, Money _valorRepassado) {		
		this.cdcontrato = cdcontrato;
		this.vencimento = vencimento;
		this.valordocumento = valordocumento;
		this.nomecliente = nomecliente;		
		this.descricaocontrato = descricaocontrato;		
		this.valorcomissao = valorcomissionamento;
		this.contratotipo = contratotipo;		
		this.nomecolaboradorfornecedor = nomecolaboradorfornecedor;	
		this.percentual = percentual;
		this.valorbrutocontrato = valorbrutocontrato;
		this.cdpessoacomissao = cdpessoacomissao;
		this.cddocumento = cddocumento;
		this.valorrepassado = _valorRepassado != null ? _valorRepassado : new Money();
	}

	public Date getVencimento() {
		return vencimento;
	}	
	public Money getValordocumento() {
		return valordocumento;
	}
	public String getNomecliente() {
		return nomecliente;
	}
	public String getResponsavelcontrato() {
		return responsavelcontrato;
	}
	public String getDescricaocontrato() {
		return descricaocontrato;
	}
	public Money getValorbrutocontrato() {
		return valorbrutocontrato;
	}
	public Money getValorliquidocontrato() {
		return valorliquidocontrato;
	}
	public String getContratotipo() {
		return contratotipo;
	}
	public Integer getOrdemcomissao() {
		return ordemcomissao;
	}
	public String getNomecolaboradorfornecedor() {
		return nomecolaboradorfornecedor;
	}
	public Double getPercentual() {
		return percentual;
	}
	public Money getValorcomissao() {
		return valorcomissao;
	}
	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}
	public void setValordocumento(Money valordocumento) {
		this.valordocumento = valordocumento;
	}
	public void setNomecliente(String nomecliente) {
		this.nomecliente = nomecliente;
	}
	public void setResponsavelcontrato(String responsavelcontrato) {
		this.responsavelcontrato = responsavelcontrato;
	}
	public void setDescricaocontrato(String descricaocontrato) {
		this.descricaocontrato = descricaocontrato;
	}
	public void setValorbrutocontrato(Money valorbrutocontrato) {
		this.valorbrutocontrato = valorbrutocontrato;
	}
	public void setValorliquidocontrato(Money valorliquidocontrato) {
		this.valorliquidocontrato = valorliquidocontrato;
	}
	public void setContratotipo(String contratotipo) {
		this.contratotipo = contratotipo;
	}
	public void setOrdemcomissao(Integer ordemcomissao) {
		this.ordemcomissao = ordemcomissao;
	}
	public void setNomecolaboradorfornecedor(String nomecolaboradorfornecedor) {
		this.nomecolaboradorfornecedor = nomecolaboradorfornecedor;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public void setValorcomissao(Money valorcomissao) {
		this.valorcomissao = valorcomissao;
	}

	public Integer getCdcontrato() {
		return cdcontrato;
	}
	
	public Integer getCddocumento() {
		return cddocumento;
	}

	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}

	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}

	public String getNomevendedor() {
		return nomevendedor;
	}

	public Double getPercentualvendedor() {
		return percentualvendedor;
	}

	public Money getValorcomissaovendedor() {
		return valorcomissaovendedor;
	}

	public void setNomevendedor(String nomevendedor) {
		this.nomevendedor = nomevendedor;
	}

	public void setPercentualvendedor(Double percentualvendedor) {
		this.percentualvendedor = percentualvendedor;
	}

	public void setValorcomissaovendedor(Money valorcomissaovendedor) {
		this.valorcomissaovendedor = valorcomissaovendedor;
	}

	public Integer getCdvendedor() {
		return cdvendedor;
	}

	public void setCdvendedor(Integer cdvendedor) {
		this.cdvendedor = cdvendedor;
	}

	public Integer getCdcolaborador() {
		return cdcolaborador;
	}

	public void setCdcolaborador(Integer cdcolaborador) {
		this.cdcolaborador = cdcolaborador;
	}

	public Integer getCdpessoacomissao() {
		return cdpessoacomissao;
	}

	public void setCdpessoacomissao(Integer cdpessoacomissao) {
		this.cdpessoacomissao = cdpessoacomissao;
	}

	public Integer getCddocumentocomissao() {
		return cddocumentocomissao;
	}

	public void setCddocumentocomissao(Integer cddocumentocomissao) {
		this.cddocumentocomissao = cddocumentocomissao;
	}

	public Money getValorrepassado() {
		return valorrepassado;
	}

	public void setValorrepassado(Money valorrepassado) {
		this.valorrepassado = valorrepassado;
	}

	public Date getProximovencimento() {
		return proximovencimento;
	}

	public void setProximovencimento(Date proximovencimento) {
		this.proximovencimento = proximovencimento;
	}	
}
