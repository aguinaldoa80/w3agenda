package br.com.linkcom.sined.modulo.rh.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class ComissionamentodesempenhoReportBean {

	private String nomevendedor;
	private Money valor;
	private String tipo;
	private String repassado;
		
	private Integer cdcomissionamento;
	
	public ComissionamentodesempenhoReportBean(){}
		
	public ComissionamentodesempenhoReportBean(String nomevendedor, Long valordocumento,
			Integer cdvenda, Integer cdcontrato, Integer cdcolaboradorcomissao, Integer cdcomissionamento) {
		this.nomevendedor = nomevendedor;
		this.valor = valordocumento != null && valordocumento != 0 ? new Money(valordocumento/100):new Money();
		
		if(cdvenda != null && cdvenda != 0)
			this.tipo = "Venda";
		else if(cdcontrato != null && cdcontrato != 0)
			this.tipo = "Contrato";
		
		this.repassado = cdcolaboradorcomissao != null && cdcolaboradorcomissao != 0 ? "Sim" : "N�o";
		this.cdcomissionamento = cdcomissionamento;
	}

	public String getNomevendedor() {
		return nomevendedor;
	}

	public Money getValor() {
		return valor;
	}

	public String getTipo() {
		return tipo;
	}

	public String getRepassado() {
		return repassado;
	}

	public Integer getCdcomissionamento() {
		return cdcomissionamento;
	}

	public void setNomevendedor(String nomevendedor) {
		this.nomevendedor = nomevendedor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setRepassado(String repassado) {
		this.repassado = repassado;
	}

	public void setCdcomissionamento(Integer cdcomissionamento) {
		this.cdcomissionamento = cdcomissionamento;
	}

}
