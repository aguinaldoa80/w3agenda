package br.com.linkcom.sined.modulo.rh.controller.report.bean;


public class ColaboradorEtiqueta {

	protected String nomea;
	protected String endereco;
	protected String cep;
	protected String cidade;
	protected String bairro;
	protected String uf;

	//|..GET..|
	public String getNomea() {
		return nomea;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getCep() {
		return cep;
	}
	public String getCidade() {
		return cidade;
	}
	public String getBairro() {
		return bairro;
	}
	public String getUf() {
		return uf;
	}

	//|..SET..|
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setNomea(String nomea) {
		this.nomea = nomea;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
}