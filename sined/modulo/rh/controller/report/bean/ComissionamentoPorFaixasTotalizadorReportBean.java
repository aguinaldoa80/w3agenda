package br.com.linkcom.sined.modulo.rh.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class ComissionamentoPorFaixasTotalizadorReportBean {

	private String colaborador;
	private Integer cdPessoa;
	private Money valorTotalComissao;
	private Money valorTotalVenda;
	
	public String getColaborador() {
		return colaborador;
	}
	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}
	
	public Integer getCdPessoa() {
		return cdPessoa;
	}
	public void setCdPessoa(Integer cdPessoa) {
		this.cdPessoa = cdPessoa;
	}
	
	public Money getValorTotalComissao() {
		return valorTotalComissao;
	}
	public void setValorTotalComissao(Money valorTotalComissao) {
		this.valorTotalComissao = valorTotalComissao;
	}
	
	public Money getValorTotalVenda() {
		return valorTotalVenda;
	}
	public void setValorTotalVenda(Money valorTotalVenda) {
		this.valorTotalVenda = valorTotalVenda;
	}
}
