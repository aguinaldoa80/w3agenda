package br.com.linkcom.sined.modulo.rh.controller.report.bean;

public class CargoSubReportBean {
	
	private String departamento;

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
}
