package br.com.linkcom.sined.modulo.rh.controller.report.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Pessoa;

public class ResumoComissionamento {

	private Colaborador colaborador;
	private Money valor;
	
	public ResumoComissionamento(){}
	
	public ResumoComissionamento(Pessoa pessoa, Money valor){
		if(pessoa != null)
			this.colaborador = new Colaborador(pessoa.getCdpessoa(), pessoa.getNome());
		this.valor = valor;
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Money getValor() {
		return valor;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
}
