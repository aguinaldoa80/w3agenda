package br.com.linkcom.sined.modulo.rh.controller.report.bean;

import java.util.Date;

import br.com.linkcom.neo.types.Money;

public class ComissionamentoPorFaixasReportBean {

	private String venda;
	private Date dtVenda;
	private String vendedor;
	private String identificacaoProduto;
	private String material;
	private String markup;
	private String comissionamento;
	private Money valorVenda;
	private Money valorComissao;
	private String pago;
	
	
	public String getVenda() {
		return venda;
	}
	public Date getDtVenda() {
		return dtVenda;
	}
	public String getVendedor() {
		return vendedor;
	}
	public String getIdentificacaoProduto() {
		return identificacaoProduto;
	}
	public String getMaterial() {
		return material;
	}
	public String getMarkup() {
		return markup;
	}
	public String getComissionamento() {
		return comissionamento;
	}
	public Money getValorVenda() {
		return valorVenda;
	}
	public Money getValorComissao() {
		return valorComissao;
	}
	public String getPago() {
		return pago;
	}
	public void setVenda(String venda) {
		this.venda = venda;
	}
	public void setDtVenda(Date dtVenda) {
		this.dtVenda = dtVenda;
	}
	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}
	public void setIdentificacaoProduto(String identificacaoProduto) {
		this.identificacaoProduto = identificacaoProduto;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public void setMarkup(String markup) {
		this.markup = markup;
	}
	public void setComissionamento(String comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setValorVenda(Money valorVenda) {
		this.valorVenda = valorVenda;
	}
	public void setValorComissao(Money valorComissao) {
		this.valorComissao = valorComissao;
	}
	public void setPago(String pago) {
		this.pago = pago;
	}
}
