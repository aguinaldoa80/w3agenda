package br.com.linkcom.sined.modulo.rh.controller.report.bean;

import java.util.List;

public class CargoReportBean {

	private String nome;
	private String cbo;
	private String codigoFolha;
	private String ativo;
	private List<CargoSubReportBean> listaDepartamento;
	
	public CargoReportBean() {
		// TODO Auto-generated constructor stub
	}

	public CargoReportBean(String _nome, String _cbo, String _codigoFolha, String _ativo, List<CargoSubReportBean> _listaDepartamento) {
		this.nome = _nome;
		this.cbo = _cbo;
		this.codigoFolha = _codigoFolha;
		this.ativo = _ativo;
		this.listaDepartamento = _listaDepartamento;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCbo() {
		return cbo;
	}
	public void setCbo(String cbo) {
		this.cbo = cbo;
	}
	public String getCodigoFolha() {
		return codigoFolha;
	}
	public void setCodigoFolha(String codigoFolha) {
		this.codigoFolha = codigoFolha;
	}
	public String getAtivo() {
		return ativo;
	}
	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}
	public List<CargoSubReportBean> getListaDepartamento() {
		return listaDepartamento;
	}
	public void setListaDepartamento(List<CargoSubReportBean> listaDepartamento) {
		this.listaDepartamento = listaDepartamento;
	}
}
