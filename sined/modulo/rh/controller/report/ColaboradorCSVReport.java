package br.com.linkcom.sined.modulo.rh.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorFiltro;

@Controller(path = "/rh/relatorio/ColaboradorCSV", authorizationModule=ReportAuthorizationModule.class)
public class ColaboradorCSVReport extends ResourceSenderController<ColaboradorFiltro> {

	private ColaboradorService colaboradorService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	@Action("gerar")
	public Resource generateResource(WebRequestContext request, ColaboradorFiltro filtro) throws Exception {
		return colaboradorService.gerarRelatorioCSVListagem(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	ColaboradorFiltro filtro) throws Exception {
		return null;
	}
	
	
}