package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwcolaboradoretiquetaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/rh/relatorio/ColaboradorEtiqueta", authorizationModule=ReportAuthorizationModule.class)
public class ColaboradorEtiquetaReport extends SinedReport<VwcolaboradoretiquetaFiltro>{

	private ColaboradorService colaboradorService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, VwcolaboradoretiquetaFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		return colaboradorService.createRelatorioEtiqueta(filtro, whereIn);
	}

	@Override
	public String getNomeArquivo() {
		return "etiqueta";
	}

	@Override
	public String getTitulo(VwcolaboradoretiquetaFiltro filtro) {
		return "";
	}

}
