package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.bean.enumeration.Visaorelatorio;
import br.com.linkcom.sined.geral.service.DocumentocomissaoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/rh/relatorio/Emitircomissionamento", authorizationModule=ReportAuthorizationModule.class)
public class ComissionamentoReport extends SinedReport<DocumentocomissaoFiltro>{

	private DocumentocomissaoService documentocomissaoService;

	public void setDocumentocomissaoService(
			DocumentocomissaoService documentocomissaoService) {
		this.documentocomissaoService = documentocomissaoService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	DocumentocomissaoFiltro filtro) throws Exception {		
		if(filtro.getVisaorelatorio() != null){
			if(filtro.getVisaorelatorio().equals(Visaorelatorio.COLABORADOR)){
				return documentocomissaoService.gerarRelatorioComissionamentoColaborador(filtro);
			} else if(filtro.getVisaorelatorio().equals(Visaorelatorio.CONTRATO)){
				return documentocomissaoService.gerarRelatorioComissionamentoContrato(filtro);
			}
		}
		return null;
	}

	@Override
	public String getNomeArquivo() {		
		return "comissionamento";
	}

	@Override
	public String getTitulo(DocumentocomissaoFiltro filtro) {		
		return "Comissionamento";
	}
	
}
