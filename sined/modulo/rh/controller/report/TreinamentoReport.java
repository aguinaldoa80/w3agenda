package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.TurmaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.TurmaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/rh/relatorio/Treinamento",
	authorizationModule=ReportAuthorizationModule.class
)
public class TreinamentoReport extends SinedReport<TurmaFiltro> {

	private TurmaService turmaService;
	
	public void setTurmaService(TurmaService turmaService) {
		this.turmaService = turmaService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, TurmaFiltro filtro) throws Exception {
		return turmaService.createRelatorioTreinamento(filtro);
	}
	
	@Override
	public String getTitulo(TurmaFiltro filtro) {
		return "RELATÓRIO DE TREINAMENTO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, TurmaFiltro filtro) {

	}
	
	@Override
	public String getNomeArquivo() {
		return "treinamento";
	}
}
