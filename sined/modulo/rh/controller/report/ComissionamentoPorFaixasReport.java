package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ComissionamentoPorFaixasService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ComissionamentoPorFaixasFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/rh/relatorio/EmitirComissionamentoPorFaixas", authorizationModule=ReportAuthorizationModule.class)
public class ComissionamentoPorFaixasReport extends SinedReport<ComissionamentoPorFaixasFiltro>{
	
	private ComissionamentoPorFaixasService comissionamentoPorFaixasService;
	
	public void setComissionamentoPorFaixasService(
			ComissionamentoPorFaixasService comissionamentoPorFaixasService) {
		this.comissionamentoPorFaixasService = comissionamentoPorFaixasService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,
			ComissionamentoPorFaixasFiltro filtro) throws Exception {
		return comissionamentoPorFaixasService.gerarRelatorioComissionament(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "comissionamentoPorFaixa";
	}

	@Override
	public String getTitulo(ComissionamentoPorFaixasFiltro filtro) {
		return "COMISSIONAMENTO POR FAIXAS";
	}

}
