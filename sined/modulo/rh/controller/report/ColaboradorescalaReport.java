package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.ColaboradorescalaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorescalaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/rh/relatorio/Colaboradorescala",
	authorizationModule=ReportAuthorizationModule.class
)
public class ColaboradorescalaReport extends SinedReport<ColaboradorescalaFiltro> {

	private ColaboradorescalaService colaboradorescalaService;
	
	public void setColaboradorescalaService(ColaboradorescalaService colaboradorescalaService) {this.colaboradorescalaService = colaboradorescalaService;}

	@Override
	public IReport createReportSined(WebRequestContext request, ColaboradorescalaFiltro filtro) throws Exception {
		return colaboradorescalaService.createRelatorioListagem(filtro);
	}
	
	@Override
	public String getTitulo(ColaboradorescalaFiltro filtro) {
		return "RELATÓRIO DE ESCALA DO COLABORADOR";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ColaboradorescalaFiltro filtro) {

	}
	
	@Override
	public String getNomeArquivo() {
		return "colaboradorescala";
	}
}
