package br.com.linkcom.sined.modulo.rh.controller.report;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Exameclinica;
import br.com.linkcom.sined.geral.bean.Exameconvenio;
import br.com.linkcom.sined.geral.bean.Exameresponsavel;
import br.com.linkcom.sined.geral.bean.Exametipo;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ExameclinicaService;
import br.com.linkcom.sined.geral.service.ExameconvenioService;
import br.com.linkcom.sined.geral.service.ExameresponsavelService;
import br.com.linkcom.sined.geral.service.ExametipoService;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EmitirautorizacaoReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/rh/relatorio/Emitirautorizacao",
	authorizationModule=ReportAuthorizationModule.class
)
public class EmitirautorizacaoReport extends SinedReport<EmitirautorizacaoReportFiltro> {

	private ColaboradorService colaboradorService;
	private ExametipoService exametipoService;
	private ExameclinicaService exameclinicaService;
	private ExameresponsavelService exameresponsavelService;
	private ExameconvenioService exameconvenioService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	public void setExameclinicaService(ExameclinicaService exameclinicaService) {
		this.exameclinicaService = exameclinicaService;
	}
	
	public void setExametipoService(ExametipoService exametipoService) {
		this.exametipoService = exametipoService;
	}
	
	public void setExameresponsavelService(ExameresponsavelService exameresponsavelService) {
		this.exameresponsavelService = exameresponsavelService;
	}
	
	public void setExameconvenioService(ExameconvenioService exameconvenioService) {
		this.exameconvenioService = exameconvenioService;
	}

	/**
	 * M�todo para carregar listagem do filtro, setar o combo de clinicas, mandar a listagem  de tipo de exame
	 * e fazer a ordena��o pelo cabe�alho da coluna (check).
	 *
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#findForAutorizacaoExame(EmitirautorizacaoReportFiltro)
	 * @see br.com.linkcom.sined.geral.service.ExametipoService#findAll()
	 * @see br.com.linkcom.sined.geral.service.ExameclinicaService#findAll(String, String)
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @author Jo�o Paulo Zica
	 */
	@Command(session=true)
	public ModelAndView listagem(WebRequestContext request, EmitirautorizacaoReportFiltro filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null");
		}
		if(!filtro.isNotFirstTime() && request.getSession().getAttribute("empresaSelecionada") != null){
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null)
				filtro.setEmpresa(empresa);
		}
			
		String order = (String)request.getParameter("orderBy");
		if (order != null) {
			filtro.setOrderBy(order);
		}
		List<Colaborador> listaColaborador = colaboradorService.findForAutorizacaoExame(filtro);
		for(Colaborador colaborador : listaColaborador)
			colaborador.setListaExameTipoNatureza();
		filtro.setListaColaborador(listaColaborador);
		
		List<Exametipo> listaExametipo = exametipoService.findAll();
		request.setAttribute("listaExametipo", listaExametipo);
		
		//List<Exameclinica> listaExameclinica = exameclinicaService.findAll("exameclinica.cdexameclinica, exameclinica.nome","exameclinica.nome asc");
		//List<Exameclinica> listaExameclinica = exameclinicaService.findAll("","");
		List<Exameclinica> listaExameclinica = exameclinicaService.findAll(null, "exameclinica.nome asc");
		request.setAttribute("listaExameclinica", listaExameclinica);
		
		List<Exameresponsavel> listaExameresponsavel = exameresponsavelService.findAll(null, "exameresponsavel.nome asc");
		request.setAttribute("listaExameresponsavel", listaExameresponsavel);
		
		List<Exameconvenio> listaExameconvenio = exameconvenioService.findAll(null, "exameconvenio.nome asc");
		request.setAttribute("listaExameconvenio", listaExameconvenio);
		
		
		
		if(listaExameclinica != null && listaExameclinica.size() > 0){
			request.setAttribute("showFormResult", true);
		}else{
			request.setAttribute("showFormResult", false);
		}

		request.setAttribute("acao","listagem");
		return new ModelAndView("relatorio/emitirautorizacao", "filtro", filtro);
	}
		
	/**
	 * Popula a combo Conv�nio de acordo com a Cl�nica e/ou o Profissional escolhidos.
	 * @param request
	 * @author Taidson
	 * @since 22/09/2010
	 */
	public void ajaxComboConvenio(WebRequestContext request){
		List<Exameconvenio> listaConvenio = new ListSet<Exameconvenio>(Exameconvenio.class);
		
		String exameclinicaString = request.getParameter("cdexameclinica");
		String exameresponsavelString = request.getParameter("cdexameresponsavel");
		
		Exameclinica exameclinica = new Exameclinica();
		Exameresponsavel exameresponsavel = new Exameresponsavel();
		
		if(!"<null>".equals(exameclinicaString))
			exameclinica.setCdexameclinica(Integer.valueOf(StringUtils.substringBetween(exameclinicaString, "=", "]")));
		if(!"<null>".equals(exameresponsavelString))
			exameresponsavel.setCdexameresponsavel(Integer.valueOf(StringUtils.substringBetween(exameresponsavelString, "=", "]")));
		
		if(!"<null>".equals(exameclinicaString) || !"<null>".equals(exameresponsavelString)){
			listaConvenio = exameconvenioService.findByClinicaResponsavel(exameclinica, exameresponsavel);
		}
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaConvenio, "listaConvenio", ""));
	}
	

	@Override
	public IReport createReportSined(WebRequestContext request, EmitirautorizacaoReportFiltro filtro) throws Exception {
		return colaboradorService.createRelatorioAutorizacao(filtro);
	}

	@Override
	public String getTitulo(EmitirautorizacaoReportFiltro filtro) {
		return null;
	}
	
	@Override
	public String getNomeArquivo() {
		return "autorizacao";
	}
	
	/**
	 * M�todo para gerar o csv da listagem
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarCSV(WebRequestContext request, EmitirautorizacaoReportFiltro filtro){
		return colaboradorService.createCSVAutorizacaoExame(filtro);
	}
}
