package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.TurmaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.TurmaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/rh/relatorio/TreinamentoListapresenca",
	authorizationModule=ReportAuthorizationModule.class
)
public class TreinamentoListapresencaReport extends SinedReport<TurmaFiltro> {

	private TurmaService turmaService;
	
	public void setTurmaService(TurmaService turmaService) {
		this.turmaService = turmaService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, TurmaFiltro filtro) throws Exception {
		Integer cdturma = Integer.parseInt(request.getParameter("cdturma"));
		return turmaService.createRelatorioTreinamentoListapresenca(cdturma);
	}
	
	@Override
	public String getTitulo(TurmaFiltro filtro) {
		return "LISTA DE PRESEN�A";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, TurmaFiltro filtro) {

	}
	
	@Override
	public String getNomeArquivo() {
		return "treinamento_listaPresenca";
	}
}
