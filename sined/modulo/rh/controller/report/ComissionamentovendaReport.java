package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/rh/relatorio/Emitircomissionamentovenda", authorizationModule=ReportAuthorizationModule.class)
public class ComissionamentovendaReport extends SinedReport<DocumentocomissaovendaFiltro>{

	private ComissionamentoService comissionamentoService;
	
	
	public void setComissionamentoService(
			ComissionamentoService comissionamentoService) {
		this.comissionamentoService = comissionamentoService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	DocumentocomissaovendaFiltro filtro) throws Exception {
		return comissionamentoService.gerarRelatorioComissionamentovenda(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "comissionamentoVenda";
	}

	@Override
	public String getTitulo(DocumentocomissaovendaFiltro filtro) {
		return "Comissionamento Venda";
	}

}
