package br.com.linkcom.sined.modulo.rh.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ColaboradordespesaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradordespesaFiltro;

@Controller(path = "/rh/relatorio/ColaboradorDespesaCSV",	authorizationModule=ReportAuthorizationModule.class)
public class ColaboradorDespesaCSVReport extends ResourceSenderController<ColaboradordespesaFiltro>{

	private ColaboradordespesaService colaboradordespesaService;
	
	public void setColaboradordespesaService(
			ColaboradordespesaService colaboradordespesaService) {
		this.colaboradordespesaService = colaboradordespesaService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,
			ColaboradordespesaFiltro filtro) throws Exception {
		return colaboradordespesaService.gerarRelarorioCSV(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			ColaboradordespesaFiltro filtro) throws Exception {
		return null;
	}
	
	

}
