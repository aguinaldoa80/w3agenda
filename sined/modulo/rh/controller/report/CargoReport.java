package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.CargoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CargoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/rh/relatorio/CargoReport", authorizationModule=ReportAuthorizationModule.class)
public class CargoReport extends SinedReport<CargoFiltro>{

	private CargoService cargoService;
	
	public void setCargoService(CargoService cargoService) {
		this.cargoService = cargoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, CargoFiltro filtro) throws Exception {
		return cargoService.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "cargo";
	}

	@Override
	public String getTitulo(CargoFiltro filtro) {
		return "RELATÓRIO DE CARGOS";
	}

}
