package br.com.linkcom.sined.modulo.rh.controller.report.filter;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargomaterialseguranca;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EntregaEPIReportFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private Projeto projeto;
	private Cargo cargo;
	private Date dtinicio;
	private Date dtfim;
	private Colaborador colaborador;
	private Boolean considerarepivencidos = Boolean.TRUE;
	
	private Integer cdcolaborador;
	private List<Cargomaterialseguranca> listaCargomaterialseguranca;
	private Localarmazenagem localarmazenagem;
	

	
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	public Cargo getCargo() {
		return cargo;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Considerar EPIs vencidos")
	public Boolean getConsiderarepivencidos() {
		return considerarepivencidos;
	}
	public void setConsiderarepivencidos(Boolean considerarepivencidos) {
		this.considerarepivencidos = considerarepivencidos;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public Integer getCdcolaborador() {
		return cdcolaborador;
	}
	public void setCdcolaborador(Integer cdcolaborador) {
		this.cdcolaborador = cdcolaborador;
	}
	public List<Cargomaterialseguranca> getListaCargomaterialseguranca() {
		return listaCargomaterialseguranca;
	}
	public void setListaCargomaterialseguranca(List<Cargomaterialseguranca> listaCargomaterialseguranca) {
		this.listaCargomaterialseguranca = listaCargomaterialseguranca;
	}
	
	
	@Required
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
}
