package br.com.linkcom.sined.modulo.rh.controller.report.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FormularioPPPReportFiltro extends FiltroListagemSined{

	protected Integer matricula;
	protected Colaborador colaborador;
	protected Empresa empresa;
	protected Projeto projeto;
	protected Departamento departamento;
	protected Cargo cargo;
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Cargo getCargo() {
		return cargo;
	}
	@DisplayName("Matrícula")
	@MaxLength(9)
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
}
