package br.com.linkcom.sined.modulo.rh.controller.report.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;

public class ColaboradorPagamentoFiltro {

	private Colaborador colaborador;
	private Cargo cargo;
	private Departamento departamento;
	private Empresa empresa;
	private Date dtPagamentoIni;
	private Date dtPagamentoFim;
	private List<Colaboradordespesasituacao> listaSituacao;
	private Colaboradordespesamotivo tipoRemuneracao;
	
	
	public ColaboradorPagamentoFiltro() {
		listaSituacao = new ArrayList<Colaboradordespesasituacao>();
		listaSituacao.add(Colaboradordespesasituacao.EM_ABERTO);
		listaSituacao.add(Colaboradordespesasituacao.AUTORIZADO);
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Date getDtPagamentoIni() {
		return dtPagamentoIni;
	}
	public Date getDtPagamentoFim() {
		return dtPagamentoFim;
	}
	@DisplayName("Situações")
	public List<Colaboradordespesasituacao> getListaSituacao() {
		return listaSituacao;
	}
	@DisplayName("Tipo de remuneração")
	public Colaboradordespesamotivo getTipoRemuneracao() {
		return tipoRemuneracao;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtPagamentoIni(Date dtPagamentoIni) {
		this.dtPagamentoIni = dtPagamentoIni;
	}
	public void setDtPagamentoFim(Date dtPagamentoFim) {
		this.dtPagamentoFim = dtPagamentoFim;
	}
	public void setListaSituacao(List<Colaboradordespesasituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setTipoRemuneracao(Colaboradordespesamotivo tipoRemuneracao) {
		this.tipoRemuneracao = tipoRemuneracao;
	}
}
