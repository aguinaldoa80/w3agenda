package br.com.linkcom.sined.modulo.rh.controller.report.filter;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventoPagamento;


public class ConsultaSaldoEventoProvisionamentoReportFiltro {
	
	private Empresa empresa;
	private Colaborador colaborador;
	private Date dtinicio;
	private Date dtfim;
	private EventoPagamento eventoPagamento;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public EventoPagamento getEventoPagamento() {
		return eventoPagamento;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setEventoPagamento(EventoPagamento eventoPagamento) {
		this.eventoPagamento = eventoPagamento;
	}
}
