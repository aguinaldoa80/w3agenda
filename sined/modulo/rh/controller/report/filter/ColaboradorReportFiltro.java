package br.com.linkcom.sined.modulo.rh.controller.report.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;


public class ColaboradorReportFiltro {
	
	protected Integer matricula;
	protected String nome;
	protected Departamento departamento;
	protected Cargo cargo;
	protected Date dtinicio;
	protected Date dtfim;
	protected Colaboradorsituacao colaboradorsituacao;
	protected Boolean recontratar = true;
	protected Empresa empresa;

	@DisplayName("Colaborador")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	@DisplayName("Departamento")
	public Departamento getDepartamento() {
		return departamento;
	}
	
	@DisplayName("Cargo")
	public Cargo getCargo() {
		return cargo;
	}
	
	@DisplayName("Per�odo de admiss�o")
	public Date getDtinicio() {
		return dtinicio;
	}
	
	@DisplayName(" at� ")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Situa��o")
	public Colaboradorsituacao getColaboradorsituacao() {
		return colaboradorsituacao;
	}
	public Boolean getRecontratar() {
		return recontratar;
	}
	
	@MaxLength(9)
	@DisplayName("Matr�cula")
	public Integer getMatricula() {
		return matricula;
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setColaboradorsituacao(Colaboradorsituacao colaboradorsituacao) {
		this.colaboradorsituacao = colaboradorsituacao;
	}
	public void setRecontratar(Boolean recontratar) {
		this.recontratar = recontratar;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
