package br.com.linkcom.sined.modulo.rh.controller.report.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Exameclinica;
import br.com.linkcom.sined.geral.bean.Exameconvenio;
import br.com.linkcom.sined.geral.bean.Examenatureza;
import br.com.linkcom.sined.geral.bean.Exameresponsavel;
import br.com.linkcom.sined.geral.bean.Exametipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.EmitirautorizacaoReportFiltroMostrar;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmitirautorizacaoReportFiltro extends FiltroListagemSined{

	protected Integer matricula;
	protected String nome;
	protected Empresa empresa;
	protected Cpf cpf;
	protected Projeto projeto;
	protected Departamento departamento;
	protected Cargo cargo;
	protected Date dtinicio;
	protected Date dtfim;
	protected Date dataExame;
	protected Hora horaExame;
	protected String local;
	protected List<Exametipo> listExametipo;
	protected Examenatureza exameNatureza;
	protected Exameclinica exameclinica;
	protected Exameresponsavel exameresponsavel;
	protected Exameconvenio exameconvenio;
	protected List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
	protected List<Colaborador> listaColaboradoresRh = new ArrayList<Colaborador>();
	protected Integer cdpessoa;
	protected String outro;
	protected List<Colaborador> listaColaboradoraux= new ArrayList<Colaborador>(); 
	protected EmitirautorizacaoReportFiltroMostrar mostrar;
	protected Date dtproximoexame;
	protected Date dtproximoexameInicio;
	protected Date dtproximoexameFim;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@MaxLength(9)
	@DisplayName("Matr�cula")
	public Integer getMatricula() {
		return matricula;
	}
	
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	@DisplayName("DE")
	public Date getDtinicio() {
		return dtinicio;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	@DisplayName(" AT� ")
	public Date getDtfim() {
		return dtfim;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public Cpf getCpf() {
		return cpf;
	}
	
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	
	@MaxLength(40)
	@DisplayName("LOCAL")
	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@DisplayName("TIPO")
	public List<Exametipo> getListExametipo() {
		return listExametipo;
	}

	public void setListExametipo(List<Exametipo> listExametipo) {
		this.listExametipo = listExametipo;
	}

	@DisplayName("NATUREZA EXAME")
	public Examenatureza getExameNatureza() {
		return exameNatureza;
	}

	public void setExameNatureza(Examenatureza exameNatureza) {
		this.exameNatureza = exameNatureza;
	}

	@DisplayName("CL�NICA")
	public Exameclinica getExameclinica() {
		return exameclinica;
	}
	
	@DisplayName("PROFISSIONAL")
	public Exameresponsavel getExameresponsavel() {
		return exameresponsavel;
	}
	
	@DisplayName("CONV�NIO")
	public Exameconvenio getExameconvenio() {
		return exameconvenio;
	}
	
	public void setExameclinica(Exameclinica exameclinica) {
		this.exameclinica = exameclinica;
	}
	
	public void setExameresponsavel(Exameresponsavel exameresponsavel) {
		this.exameresponsavel = exameresponsavel;
	}
	
	public void setExameconvenio(Exameconvenio exameconvenio) {
		this.exameconvenio = exameconvenio;
	}

	public Integer getCdpessoa() {
		return cdpessoa;
	}
	
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	
	@DisplayName("EXAME")
	@MaxLength(30)
	public String getOutro() {
		return outro;
	}
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setOutro(String outro) {
		this.outro = outro;
	}

	public List<Colaborador> getListaColaboradoraux() {
		return listaColaboradoraux;
	}
	
	public void setListaColaboradoraux(List<Colaborador> listaColaboradoraux) {
		this.listaColaboradoraux = listaColaboradoraux;
	}
	
	public List<Colaborador> getListaColaborador() {
		return listaColaborador;
	}
	
	public void setListaColaborador(List<Colaborador> listaColaborador) {
		this.listaColaborador = listaColaborador;
	}
	
	@DisplayName("DATA")
	public Date getDataExame() {
		return dataExame;
	}
	
	public void setDataExame(Date dataExame) {
		this.dataExame = dataExame;
	}
	
	@DisplayName("HOR�RIO")
	public Hora getHoraExame() {
		return horaExame;
	}
	
	public void setHoraExame(Hora horaExame) {
		this.horaExame = horaExame;
	}
	
	@DisplayName("Colaboradores")
	public List<Colaborador> getListaColaboradoresRh() {
		return listaColaboradoresRh;
	}
	
	public void setListaColaboradoresRh(List<Colaborador> listaColaboradoresRh) {
		this.listaColaboradoresRh = listaColaboradoresRh;
	}

	@DisplayName("Mostrar")
	public EmitirautorizacaoReportFiltroMostrar getMostrar() {
		return mostrar;
	}

	public void setMostrar(EmitirautorizacaoReportFiltroMostrar mostrar) {
		this.mostrar = mostrar;
	}
	
	@DisplayName("PR�XIMO EXAME")
	public Date getDtproximoexame() {
		return dtproximoexame;
	}

	public void setDtproximoexame(Date dtproximoexame) {
		this.dtproximoexame = dtproximoexame;
	}

	public Date getDtproximoexameInicio() {
		return dtproximoexameInicio;
	}
	public Date getDtproximoexameFim() {
		return dtproximoexameFim;
	}
	public void setDtproximoexameInicio(Date dtproximoexameInicio) {
		this.dtproximoexameInicio = dtproximoexameInicio;
	}
	public void setDtproximoexameFim(Date dtproximoexameFim) {
		this.dtproximoexameFim = dtproximoexameFim;
	}
}
