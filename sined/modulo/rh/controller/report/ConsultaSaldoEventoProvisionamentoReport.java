package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ProvisaoService;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ConsultaSaldoEventoProvisionamentoReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/rh/relatorio/ConsultaSaldoEventoProvisionamento",
	authorizationModule=ReportAuthorizationModule.class
)
public class ConsultaSaldoEventoProvisionamentoReport extends SinedReport<ConsultaSaldoEventoProvisionamentoReportFiltro> {

	private ProvisaoService provisaoService;
	
	public void setProvisaoService(ProvisaoService provisaoService) {this.provisaoService = provisaoService;}

	@Override
	public Resource generateResource(WebRequestContext request, ConsultaSaldoEventoProvisionamentoReportFiltro filtro) throws Exception {
		Resource resource = null;
		try {			
			if("true".equalsIgnoreCase(request.getParameter("analitico"))){
				resource = provisaoService.createRelatorioConsultaSaldoEventoProvisionamentoReportAnalitico(filtro);
			}else if("true".equalsIgnoreCase(request.getParameter("sintetico"))){
				resource = provisaoService.createRelatorioConsultaSaldoEventoProvisionamentoReportSintetico(filtro);
			}
		} catch (SinedException e) {
			request.addError(e.getMessage());	
		}
		desbloquearTela(request);
		
		if (resource != null){
	        return resource;
		} else
			return null;		
	}
	
	@Override
	public String getTitulo(ConsultaSaldoEventoProvisionamentoReportFiltro filtro) {
		return "Relatório de consulta de saldo de eventos de provisionamentos";
	}
	
	@Override
	public String getNomeArquivo() {
		return "consultaSaldoEventoProvisionamento";
	}

	@Override
	public IReport createReportSined(WebRequestContext request, ConsultaSaldoEventoProvisionamentoReportFiltro filtro) throws Exception {
		return null;
	}
}
