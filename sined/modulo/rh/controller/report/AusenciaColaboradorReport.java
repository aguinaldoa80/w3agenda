package br.com.linkcom.sined.modulo.rh.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.AusenciacolaboradorService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.AusenciacolaboradorFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/rh/relatorio/AusenciaColaborador", authorizationModule=ReportAuthorizationModule.class)
public class AusenciaColaboradorReport extends SinedReport<AusenciacolaboradorFiltro>{

	private AusenciacolaboradorService ausenciacolaboradorService;
	
	public void setAusenciacolaboradorService(
			AusenciacolaboradorService ausenciacolaboradorService) {
		this.ausenciacolaboradorService = ausenciacolaboradorService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			AusenciacolaboradorFiltro filtro) throws Exception {
		return ausenciacolaboradorService.gerarReportAusenciaColaborador(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "ausenciacolaborador";
	}

	@Override
	public String getTitulo(AusenciacolaboradorFiltro filtro) {
		return "AUS�NCIA COLABORADOR";
	}

}
