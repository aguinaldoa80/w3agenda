package br.com.linkcom.sined.modulo.rh.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.FormularioPPPReportFiltro;


@Bean
@Controller(
	path = "/rh/relatorio/FormularioPPP",
	authorizationModule=ReportAuthorizationModule.class
)
public class FormularioPPPReport extends ResourceSenderController<FormularioPPPReportFiltro> {

	private ColaboradorService colaboradorService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, FormularioPPPReportFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		filtro.setEmpresa(new Empresa(Integer.valueOf(request.getParameter("cdempresa"))));
		if(request.getParameter("matricula") != ""){
			filtro.setMatricula(Integer.valueOf(request.getParameter("matricula")));
		}
		return colaboradorService.geraFormularioPPP(filtro, whereIn);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	FormularioPPPReportFiltro filtro) throws Exception {
		return null;
	}

}
