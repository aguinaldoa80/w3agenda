package br.com.linkcom.sined.modulo.rh.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.DocumentocomissaoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaoFiltro;

@Controller(path = "/rh/relatorio/DocumentocomissaoCSV", authorizationModule=ReportAuthorizationModule.class)
public class DocumentocomissaoCSVReport extends ResourceSenderController<DocumentocomissaoFiltro> {
	
	private DocumentocomissaoService documentocomissaoService;
	
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {
		this.documentocomissaoService = documentocomissaoService;
	}

	@Override
	public Resource generateResource(WebRequestContext request, DocumentocomissaoFiltro filtro) throws Exception {
		return documentocomissaoService.gerarRelarorioCSVListagem(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	DocumentocomissaoFiltro filtro) throws Exception {
		return null;
	}
	
	
}
