package br.com.linkcom.sined.modulo.rh.controller.report;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargomaterialseguranca;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorequipamento;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.service.CargomaterialsegurancaService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradorcargoService;
import br.com.linkcom.sined.geral.service.ColaboradorequipamentoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EntregaEPIReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path = "/rh/relatorio/EntregaEPI", authorizationModule=ReportAuthorizationModule.class)
public class EntregaEPIReport extends ResourceSenderController<EntregaEPIReportFiltro>{
	
	private ColaboradorService colaboradorService;
	private ColaboradorcargoService colaboradorcargoService;
	private ColaboradorequipamentoService colaboradorequipamentoService;
	private CargomaterialsegurancaService cargomaterialsegurancaService;
	private ParametrogeralService parametrogeralService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	
	public void setColaboradorequipamentoService(ColaboradorequipamentoService colaboradorequipamentoService) {this.colaboradorequipamentoService = colaboradorequipamentoService;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setCargomaterialsegurancaService(CargomaterialsegurancaService cargomaterialsegurancaService) {this.cargomaterialsegurancaService = cargomaterialsegurancaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	
	
	@Override
	public Resource generateResource(WebRequestContext request, EntregaEPIReportFiltro filtro) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request);
		String tipo = request.getParameter("tipo");
		if ("controleExcel".equals(tipo)) {
			return colaboradorService.gerarRelatorioEntregaEPIExcel(filtro, whereIn);
		}else {
			return colaboradorService.gerarRelatorioEntregaEPI(filtro, whereIn, tipo);
		}
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, EntregaEPIReportFiltro filtro) throws ResourceGenerationException {
		if(request.getSession().getAttribute("EntregaEPIReportFiltro") != null){
			filtro = (EntregaEPIReportFiltro) request.getSession().getAttribute("EntregaEPIReportFiltro");
		}
		return new ModelAndView("relatorio/entregaEPI", "filtro", filtro);
	}
	
	/**
	 * Action que retorna a listagem da tela via ajax.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @since 24/05/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView listar(WebRequestContext request, EntregaEPIReportFiltro filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		request.getSession().setAttribute("EntregaEPIReportFiltro", filtro);
		List<Colaboradorcargo> listaColaboradores = colaboradorcargoService.findForRelatorioEntregaEPI(filtro, null);
		
		for (Colaboradorcargo colaboradorcargo : listaColaboradores) {
			List<Colaboradorequipamento> listaEquipamentoNaoVencido = new ArrayList<Colaboradorequipamento>();
			
			List<Colaboradorequipamento> listaColaboradorequipamento = colaboradorcargo.getColaborador().getListaColaboradorequipamento();
			if(listaColaboradorequipamento != null){
				for (Colaboradorequipamento colaboradorequipamento : listaColaboradorequipamento) {
					Date dtentrega = colaboradorequipamento.getDtentrega();
					colaboradorequipamento.setDtvencimentotrans(colaboradorequipamento.getDtvencimento() != null? colaboradorequipamento.getDtvencimento():
																dtentrega != null && colaboradorequipamento.getEpi().getDuracao() != null?
																SinedDateUtils.addSemanasData(dtentrega, colaboradorequipamento.getEpi().getDuracao()): null);
					
					if(dtentrega != null && colaboradorequipamento.getEpi() != null &&
							colaboradorequipamento.getDtvencimentotrans() != null &&
							SinedDateUtils.afterIgnoreHour(SinedDateUtils.currentDate(), colaboradorequipamento.getDtvencimentotrans())){
						colaboradorcargo.setPendencia(Boolean.TRUE);
					} else {
						listaEquipamentoNaoVencido.add(colaboradorequipamento);
					}
				}
			}
			
			Set<Cargomaterialseguranca> listaCargomaterialseguranca = colaboradorcargo.getCargo().getListaCargomaterialseguranca();
			if(listaCargomaterialseguranca != null){
				boolean achou = false;
				for (Cargomaterialseguranca cargomaterialseguranca : listaCargomaterialseguranca) {
					achou = false;
					for (Colaboradorequipamento colaboradorequipamento : listaEquipamentoNaoVencido) {
						if(colaboradorequipamento.getEpi() != null && 
								cargomaterialseguranca.getEpi() != null && 
								cargomaterialseguranca.getEpi().equals(colaboradorequipamento.getEpi())){
							achou = true;
							break;
						}
					}
					
					if(!achou){
						colaboradorcargo.setPendencia(Boolean.TRUE);
					}
				}
			}
		}
		
		if(filtro.getConsiderarepivencidos() != null && filtro.getConsiderarepivencidos()){
			for (Iterator<Colaboradorcargo> iterator = listaColaboradores.iterator(); iterator.hasNext();) {
				Colaboradorcargo colaboradorcargo = (Colaboradorcargo) iterator.next();
				if(colaboradorcargo.getPendencia() == null || !colaboradorcargo.getPendencia()){
					iterator.remove();
				}
			}
		}
		
		request.getSession().setAttribute("listaColaboradorEntregaEPI", listaColaboradores);
		
		return new ModelAndView("direct:ajax/entregaEPIlistagem", "listaColaborador", listaColaboradores);
	}
	
	public void atualizarEntrega(WebRequestContext request){
		try {
			
			Integer cdcolaborador = Integer.parseInt(request.getParameter("cdcolaborador"));
			Localarmazenagem localarmazenagem = (Localarmazenagem)request.getSession().getAttribute("localarmazenagem");
			
			validaEntregaEPI(new Colaborador(cdcolaborador), localarmazenagem);
			
			List<Colaboradorcargo> listaColaboradores = colaboradorcargoService.findForRelatorioEntregaEPI(new EntregaEPIReportFiltro(), cdcolaborador.toString());
			
			List<Colaboradorequipamento> listaNova = new ArrayList<Colaboradorequipamento>();
			
			for (Colaboradorcargo colaboradorcargo : listaColaboradores) {
				List<Colaboradorequipamento> listaEquipamentoNaoVencido = new ArrayList<Colaboradorequipamento>();
				
				List<Colaboradorequipamento> listaColaboradorequipamento = colaboradorcargo.getColaborador().getListaColaboradorequipamento();
				if(listaColaboradorequipamento != null){
					for (Colaboradorequipamento colaboradorequipamento : listaColaboradorequipamento) {
						Date dtentrega = colaboradorequipamento.getDtentrega();
						colaboradorequipamento.setDtvencimentotrans(colaboradorequipamento.getDtvencimento() != null? colaboradorequipamento.getDtvencimento():
																	dtentrega != null && colaboradorequipamento.getEpi().getDuracao() != null?
																	SinedDateUtils.addSemanasData(dtentrega, colaboradorequipamento.getEpi().getDuracao()): null);
						if(dtentrega != null && colaboradorequipamento.getEpi() != null && colaboradorequipamento.getDtvencimentotrans() != null){
							if(SinedDateUtils.afterIgnoreHour(SinedDateUtils.currentDate(), colaboradorequipamento.getDtvencimentotrans())){
								listaEquipamentoNaoVencido.add(colaboradorequipamento);
							} else {
								Colaboradorequipamento colaboradorequipamentoNovo = new Colaboradorequipamento();
								colaboradorequipamentoNovo.setDtentrega(SinedDateUtils.currentDate());
								colaboradorequipamentoNovo.setColaborador(colaboradorcargo.getColaborador());
								colaboradorequipamentoNovo.setEpi(colaboradorequipamento.getEpi());
								colaboradorequipamentoNovo.setQtde(colaboradorequipamento.getQtde());
								
								listaNova.add(colaboradorequipamentoNovo);
								
								colaboradorequipamentoService.updateDtdevolucao(colaboradorequipamento);
							}
						} else {
							listaEquipamentoNaoVencido.add(colaboradorequipamento);
						}
					}
				}
				
				Set<Cargomaterialseguranca> listaCargomaterialseguranca = colaboradorcargo.getCargo().getListaCargomaterialseguranca();
				if(listaCargomaterialseguranca != null){
					boolean achou = false;
					for (Cargomaterialseguranca cargomaterialseguranca : listaCargomaterialseguranca) {
						achou = false;
						// PROCURA NOS EQUIPAMENTOS N�O VENCIDOS
						for (Colaboradorequipamento colaboradorequipamento : listaEquipamentoNaoVencido) {
							if(colaboradorequipamento.getEpi() != null && 
									cargomaterialseguranca.getEpi() != null && 
									cargomaterialseguranca.getEpi().equals(colaboradorequipamento.getEpi())){
								achou = true;
								break;
							}
						}
						
						// PROCURA NOS EQUIPAMENTOS QUE SER�O CRIADOS
						for (Colaboradorequipamento colaboradorequipamento : listaNova) {
							if(colaboradorequipamento.getEpi() != null && 
									cargomaterialseguranca.getEpi() != null && 
									cargomaterialseguranca.getEpi().equals(colaboradorequipamento.getEpi())){
								achou = true;
								break;
							}
						}
						
						if(!achou){
							Colaboradorequipamento colaboradorequipamentoNovo = new Colaboradorequipamento();
							colaboradorequipamentoNovo.setDtentrega(SinedDateUtils.currentDate());
							colaboradorequipamentoNovo.setColaborador(colaboradorcargo.getColaborador());
							colaboradorequipamentoNovo.setEpi(cargomaterialseguranca.getEpi());
							colaboradorequipamentoNovo.setQtde(cargomaterialseguranca.getQuantidade().intValue());
							
							listaNova.add(colaboradorequipamentoNovo);
						}
					}
				}
			}
			
			for (Colaboradorequipamento colaboradorequipamento : listaNova) {
				//Gera a sa�da dos materiais no estoque
				movimentacaoestoqueService.geraSaidaEntradaEquipamentoColaborador(colaboradorequipamento, localarmazenagem, Movimentacaoestoquetipo.SAIDA);
				colaboradorequipamentoService.saveOrUpdate(colaboradorequipamento);
			}
			
			View.getCurrent().println("var sucesso = true;");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false;");
			View.getCurrent().println("var msgerro = 'Erro:" + e.getMessage() + "';");
		}
		
		
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView gearCSV(WebRequestContext request){
		
		StringBuilder csv = new StringBuilder();
		csv.append("\"Nome\";\"Cpf\";");
		
		List<Colaboradorcargo> listaColaboradorcargo = (List<Colaboradorcargo>) request.getSession().getAttribute("listaColaboradorEntregaEPI");
		
		if (listaColaboradorcargo!=null && !listaColaboradorcargo.isEmpty()){
			for (Colaboradorcargo colaboradorcargo: listaColaboradorcargo){
				csv.append("\n");
				csv.append("\"" + colaboradorcargo.getColaborador().getNome() + "\";");
				csv.append("\"" + (colaboradorcargo.getColaborador().getCpf()!=null ? colaboradorcargo.getColaborador().getCpf().toString() : "") + "\";");
			}
		}
		
		Resource resource = new Resource();
        resource.setContentType("text/csv");
        resource.setFileName("controle_entrega_epi.csv");
        resource.setContents(csv.toString().getBytes());
        
        return new ResourceModelAndView(resource);
	}

	public ModelAndView mostraMateriais(WebRequestContext request, EntregaEPIReportFiltro filtro){

		//cdcolaborador n�o chega a esse ponto com o valor null
		Integer cdcolaborador = Integer.parseInt(request.getParameter("cdcolaborador"));
		Colaborador colaborador = colaboradorService.load(new Colaborador(cdcolaborador));
		Colaboradorcargo colaboradorcargo = colaboradorcargoService.carregaDadosCargo(colaborador);
		List<Cargo> listaCargo = new ArrayList<Cargo>();
		listaCargo.add(colaboradorcargo.getCargo());
		filtro.setListaCargomaterialseguranca(cargomaterialsegurancaService.obtemListaCargomaterialseguranca(colaborador));
		
		if(filtro.getListaCargomaterialseguranca()==null || filtro.getListaCargomaterialseguranca().isEmpty()){
			request.addError("N�o existem EPIs cadastrados para esse cargo.");			
			SinedUtil.redirecionamento(request, "/rh/relatorio/EntregaEPI");
			return null;
		}
		
		filtro.setColaborador(colaborador);
		return new ModelAndView("direct:relatorio/popup/popupMateriais").addObject("filtro", filtro);
	}
	
	public void disparaAtualizacaoEPI(WebRequestContext request, EntregaEPIReportFiltro filtro){
		View view = View.getCurrent();
		request.getSession().setAttribute("localarmazenagem", filtro.getLocalarmazenagem());
		view.println("<script>parent.confirma(" + filtro.getColaborador().getCdpessoa() + ")</script>");
		view.println("<script>parent.$.akModalRemove(true)</script>");
	}
	
	private void validaEntregaEPI(Colaborador colaborador, Localarmazenagem localarmazenagem) {
		List<Cargomaterialseguranca> ListaCargomaterialseguranca = cargomaterialsegurancaService.obtemListaCargomaterialseguranca(colaborador);
		
		Boolean estoqueNegativo = parametrogeralService.getBoolean("PERMITIR_ESTOQUE_NEGATIVO");
		
		//Se n�o for permitido estoque negativo ent�o eu fa�o a valida��o de estoque
		if(!estoqueNegativo){
			
			for(Cargomaterialseguranca cargomaterialseguranca : ListaCargomaterialseguranca){
				movimentacaoestoqueService.validaEntregaEPI(localarmazenagem, cargomaterialseguranca.getEpi(), cargomaterialseguranca.getQuantidade());
			}
		
		}
	}
}
