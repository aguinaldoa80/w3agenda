package br.com.linkcom.sined.modulo.rh.controller.process;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargodepartamento;
import br.com.linkcom.sined.geral.bean.Cbo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorformapagamento;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Colaboradortipo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Estadocivil;
import br.com.linkcom.sined.geral.bean.Etnia;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Regimecontratacao;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.CAGED;
import br.com.linkcom.sined.geral.bean.enumeration.Nacionalidade;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.geral.service.CargoService;
import br.com.linkcom.sined.geral.service.CargodepartamentoService;
import br.com.linkcom.sined.geral.service.CboService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradortipoService;
import br.com.linkcom.sined.geral.service.DepartamentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.GrauinstrucaoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.SindicatoService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportarcolaboradoresFPWBean;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ImportarcolaboradoresFPWFiltro;
import br.com.linkcom.sined.util.SinedException;

@Bean
@Controller(path="/rh/process/ImportarcolaboradoresFPW", authorizationModule=ProcessAuthorizationModule.class)
public class ImportarcolaboradoresFPWProcess extends MultiActionController {

	private GrauinstrucaoService grauinstrucaoService;
	private MunicipioService municipioService;
	private DepartamentoService departamentoService;
	private CargoService cargoService;
	private CboService cboService;
	private SindicatoService sindicatoService;
	private UfService ufService;
	private ProjetoService projetoService;
	private ColaboradorService colaboradorService;
	private CargodepartamentoService cargodepartamentoService;
	private BancoService bancoService;
	private EmpresaService empresaService;
	private ColaboradortipoService colaboradortipoService;
	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setCargodepartamentoService(CargodepartamentoService cargodepartamentoService) {this.cargodepartamentoService = cargodepartamentoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setUfService(UfService ufService) {this.ufService = ufService;}
	public void setSindicatoService(SindicatoService sindicatoService) {this.sindicatoService = sindicatoService;}
	public void setCboService(CboService cboService) {this.cboService = cboService;}
	public void setCargoService(CargoService cargoService) {this.cargoService = cargoService;}
	public void setDepartamentoService(DepartamentoService departamentoService) {this.departamentoService = departamentoService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setGrauinstrucaoService(GrauinstrucaoService grauinstrucaoService) {this.grauinstrucaoService = grauinstrucaoService;}
	public void setColaboradortipoService(ColaboradortipoService colaboradortipoService) {this.colaboradortipoService = colaboradortipoService;}
	
	/**
	 * A��o default para exibir o formul�rio para envio do arquivo para importa��o.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ImportarcolaboradoresFPWFiltro filtro){
		return new ModelAndView("process/importarcolaboradores", "filtro", filtro);
	}
	
	@SuppressWarnings("unchecked")
	@Input("index")
	public ModelAndView processar(WebRequestContext request, ImportarcolaboradoresFPWFiltro filtro){
		if(filtro.getArquivo() == null){
			throw new SinedException("O arquivo n�o pode ser nulo.");
		}
		
		List<ImportarcolaboradoresFPWBean> listaBean = this.preencherListaComStrings(filtro);
		this.preencheObjetosLista(listaBean);
		
		Map<Integer, Object> resultados = this.updateInsertColaboradores(listaBean);
		
		request.addMessage("Arquivo processado com sucesso.");
		if((Integer)resultados.get(0) > 0) request.addMessage(resultados.get(0) + " colaboradores foram inseridos.");
		if((Integer)resultados.get(1) > 0) request.addMessage(resultados.get(1) + " colaboradores foram atualizados.");
		if((Integer)resultados.get(2) > 0) request.addError(resultados.get(2) + " colaboradores n�o foram atualizados.");
		
		List<String> listaErros = (List<String>) resultados.get(3);
		for (String string : listaErros) {
			 request.addError(string);
		}
		
		return sendRedirectToAction("index");
	}
	
	private Map<Integer, Object> updateInsertColaboradores(List<ImportarcolaboradoresFPWBean> lista) {
		Integer inseridos = 0;
		Integer atualizados = 0;
		Integer erros = 0;
		List<String> listaErros = new ArrayList<String>();
		Colaborador colaborador;
		
		for (ImportarcolaboradoresFPWBean bean : lista) {
			try{
				colaborador = this.buscarColaborador(bean);
				if(colaborador == null){
					colaboradorService.criarColaboradorFromImportacao(bean);
					inseridos++;
				} else {
					colaborador = colaboradorService.loadForEntrada(colaborador);
					colaboradorService.atualizaColaboradorFromImportacao(bean, colaborador, listaErros);
					atualizados++;
				}
			} catch (Exception e) {
				e.printStackTrace();
				erros++;
				listaErros.add("O colaborador " + bean.getNome() + " n�o foi atualizado.");
			}
		}
		
		Map<Integer, Object> resultado = new HashMap<Integer, Object>();
		resultado.put(0, inseridos);
		resultado.put(1, atualizados);
		resultado.put(2, erros);
		resultado.put(3, listaErros);
		return resultado;
	}
	
	private Colaborador buscarColaborador(ImportarcolaboradoresFPWBean bean) {
		Colaborador colaborador = null;
		try{
			// BUSCA PRIMEIRO PELO CPF DO COLABORADOR
			if(bean.getCpfObj() != null){
				colaborador = colaboradorService.findByCpf(bean.getCpfObj());
			}
			//O sistema permite colaboradores com mesmo nome e matricula, portanto a busca deve ser feita apenas por CPF
			// SEN�O ACHAR PELO CPF BUSCA PELO NOME
/*			if(colaborador == null && bean.getNome() != null && !bean.getNome().equals("")){
				colaborador = colaboradorService.findByNome(bean.getNome());
			}
			// SEN�O ACHAR PELO NOME BUSCA PELA MATRICULA
			if(colaborador == null && bean.getMatricula() != null && !bean.getMatricula().equals("")){
				Long matricula = Long.parseLong(StringUtils.soNumero(bean.getMatricula()));
				colaborador = colaboradorService.findByMatricula(matricula);
			}*/
			return colaborador;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private void preencheObjetosLista(List<ImportarcolaboradoresFPWBean> listaBean) {
		int linhadaplanilha = 1;
		for (ImportarcolaboradoresFPWBean b : listaBean) {
			
			try{
				if(b.getSexo() != null && !b.getSexo().equals("")){
					String ch = b.getSexo().substring(0, 1).toUpperCase();
					if(ch.equals(Sexo.FEMININO_CHAR)){
						b.setSexoObj(new Sexo(Sexo.FEMININO));
					} else {
						b.setSexoObj(new Sexo(Sexo.MASCULINO));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getData_nascimento() != null && !b.getData_nascimento().equals("") && !b.getData_nascimento().equals("00/00/0000")){
					Date data = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(b.getData_nascimento()).getTime());
					b.setDtnascimento(data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getEstado_civil() != null && !b.getEstado_civil().equals("")){
					if(b.getEstado_civil().trim().toUpperCase().startsWith("CASADO")){
						b.setEstadocivil(Estadocivil.CASADO);
					} else if(b.getEstado_civil().trim().toUpperCase().startsWith("DIVORCIADO")){
						b.setEstadocivil(Estadocivil.DIVORCIADO);
					}  else if(b.getEstado_civil().trim().toUpperCase().startsWith("SEPARADO")){
						b.setEstadocivil(Estadocivil.SEPARADO_JUDICIALMENTE);
					} else if(Util.strings.tiraAcento(b.getEstado_civil().trim().toUpperCase()).startsWith("VIUVO")){
						b.setEstadocivil(Estadocivil.VIUVO);
					} else if(b.getEstado_civil().trim().toUpperCase().startsWith("SOLTEIRO")){
						b.setEstadocivil(Estadocivil.SOLTEIRO);
					} else {
						b.setEstadocivil(Estadocivil.OUTROS);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				b.setEstadocivil(Estadocivil.OUTROS);
			}
			
			try{
				if(b.getCodigo_grau_instrucao() != null && !b.getCodigo_grau_instrucao().equals("")){
					Integer codigo_grau_instrucao = Integer.parseInt(StringUtils.soNumero(b.getCodigo_grau_instrucao()));
					Grauinstrucao grauinstrucao = new Grauinstrucao(codigo_grau_instrucao);
					grauinstrucao = grauinstrucaoService.load(grauinstrucao);
					b.setGrauinstrucao(grauinstrucao);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try{
				if(b.getNacionalidade() != null && !b.getNacionalidade().equals("")){
					if(b.getNacionalidade().toUpperCase().equals("BRASILEIRO NATO") || b.getNacionalidade().toUpperCase().equals("BRASILEIRA")){
						b.setNacionalidadeObj(Nacionalidade.BRASILEIRA);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				b.setNacionalidadeObj(Nacionalidade.BRASILEIRA);
			}
			
				if(b.getNaturalidade() != null && !b.getNaturalidade().equals("")){
					Municipio municipio = municipioService.getByMunicipioNome(b.getNaturalidade());
					if(municipio == null) {
						throw new SinedException("O munic�pio de naturalidade n�o existe no sistema, favor cadastr�-lo. Linha n�mero:"+linhadaplanilha);
					}
					b.setMunicipionaturalidade(municipio);
				}
			
				if(b.getNome_empresa() != null && !"".equals(b.getNome_empresa())){
					Empresa empresa = empresaService.findByEmpresaNome(b.getNome_empresa());
					if(empresa == null) {
						throw new SinedException("Empresa n�o existe no sistema, favor cadastr�-la. Linha n�mero:"+linhadaplanilha);
					}
					b.setEmpresa(empresa);
				}

			try{
				if(b.getCodigo_municipio() != null && !b.getCodigo_municipio().equals("")){
					Municipio municipio = municipioService.findByCdibge(b.getCodigo_municipio());
					b.setMunicipio(municipio);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			if(b.getUf() != null && !b.getUf().equals("")){
				Uf uf = ufService.findBySigla(b.getUf());
				if(uf == null){
					throw new SinedException("UF n�o existe no sistema. Linha n�mero:"+linhadaplanilha);	
				}
				b.setUfObj(uf);
			}
			
			try{
				if(b.getCep() != null && !b.getCep().equals("")){
					b.setCepObj(new Cep(b.getCep()));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getCodigo_caged() != null && !b.getCodigo_caged().equals("")){
					Integer codigo = Integer.parseInt(StringUtils.soNumero(b.getCodigo_caged()));
					CAGED[] itens = CAGED.values();
					
					for (int i = 0; i < itens.length; i++) {
						if(itens[i].getValue().equals(codigo)){
							b.setCaged(itens[i]);
							break;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getData_admissao() != null && !b.getData_admissao().equals("") && !b.getData_admissao().equals("00/00/0000")){
					Date data = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(b.getData_admissao()).getTime());
					b.setDtadmissao(data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getNome_departamento() != null && !b.getNome_departamento() .equals("")){
					Departamento departamento = departamentoService.findByNome(b.getNome_departamento());
					b.setDepartamento(departamento);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			try{
				if(b.getData_inicio_cargo() != null && !b.getData_inicio_cargo().equals("") && !b.getData_inicio_cargo().equals("00/00/0000")){
					Date data = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(b.getData_inicio_cargo()).getTime());
					b.setDtinicio_cargo(data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(b.getSituacao_cargo() != null ){
				String cargoUpper = b.getSituacao_cargo().trim().toUpperCase();;
				if(cargoUpper.startsWith("NORMAL")){
					b.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.NORMAL));
				} else if(cargoUpper.startsWith("AFASTADO")){
					b.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.AFASTADO));
				} else if(cargoUpper.startsWith("DEMITIDO")){
					b.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.DEMITIDO));
				}   else if(Util.strings.tiraAcento(cargoUpper).startsWith("AVISO")){
					b.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.AVISO_PREVIO));
				} else if(Util.strings.tiraAcento(cargoUpper).startsWith("CONTRATO")){
					b.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.CONTRATO_EXPERIENCIA));
				}  else if(Util.strings.tiraAcento(cargoUpper).contains("CONTRATACAO")){
					b.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.EM_CONTRATACAO));
				}  else if(Util.strings.tiraAcento(cargoUpper).startsWith("FERIAS")){
					b.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.FERIAS));
				} else if(Util.strings.tiraAcento(cargoUpper).startsWith("PEDIDO")){
					b.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.PEDIDO_DE_DEMISSAO));
				}else {
					throw new SinedException("Situa��o do cargo n�o existe no sistema. Linha n�mero:"+linhadaplanilha);
				}
			}
		
			if(b.getCodigo_cargo() != null){
				Cargo cargo = cargoService.findByCodigofolha(b.getCodigo_cargo());

				if(cargo == null) {
					throw new SinedException("O cargo n�o existe no sistema, favor cadastr�-lo. Linha n�mero:"+linhadaplanilha);
				}
				b.setCargo(cargo);
			}
		
			try{
				if(b.getCodigo_cbo() != null && !b.getCodigo_cbo().equals("")){
					Cbo cbo = new Cbo(Integer.parseInt(StringUtils.soNumero(b.getCodigo_cbo())));
					cbo = cboService.load(cbo);
					b.setCbo(cbo);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getCodigo_sindicato() != null && !b.getCodigo_sindicato().equals("")){
					Integer codigofolha = Integer.parseInt(StringUtils.soNumero(b.getCodigo_sindicato()));
					Sindicato sindicato = sindicatoService.findByCodigofolha(codigofolha);
					b.setSindicato(sindicato);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try{
				if(b.getRg_data_emissao() != null && !b.getRg_data_emissao().equals("") && !b.getRg_data_emissao().equals("00/00/0000")){
					Date data = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(b.getRg_data_emissao()).getTime());
					b.setRgdtemissao(data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try{
				if(b.getRg_uf() != null && !b.getRg_uf().equals("")){
					Uf uf = ufService.findBySigla(b.getRg_uf());
					b.setRguf(uf);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try{
				if(b.getCpf() != null && !b.getCpf().equals("") && !b.getCpf().equals("000.000.000-00") && !b.getCpf().equals("00000000000")){
					b.setCpfObj(new Cpf(b.getCpf()));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getCodigo_projeto() != null && !b.getCodigo_projeto().equals("")){
					Projeto projeto = projetoService.load(new Projeto(Integer.parseInt(b.getCodigo_projeto())), "projeto.cdprojeto, projeto.nome");
					b.setProjeto(projeto);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getCtps_dataemissao() != null && !b.getCtps_dataemissao().equals("") && !b.getCtps_dataemissao().equals("00/00/0000")){
					Date data = new Date(new SimpleDateFormat("dd/MM/yyyy").parse(b.getCtps_dataemissao()).getTime());
					b.setCtpsdtemissao(data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try{
				if(b.getCtps_uf() != null && !b.getCtps_uf().equals("")){
					Uf uf = ufService.findBySigla(b.getCtps_uf());
					b.setCtpsuf(uf);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(b.getForma_pagamento() != null && !b.getForma_pagamento().equals("")){
				String formapagamentoLower = b.getForma_pagamento().toLowerCase();
				if(formapagamentoLower.contains("sal�rio")){
					b.setColaboradorformapagamento(new Colaboradorformapagamento(Colaboradorformapagamento.CARTAO_SALARIO));
				} else if (formapagamentoLower.contains("corrente")) {
					b.setColaboradorformapagamento(new Colaboradorformapagamento(Colaboradorformapagamento.DEPOSITO_CONTA_CORRENTE));
				} else if (formapagamentoLower.contains("poupan�a")) {
					b.setColaboradorformapagamento(new Colaboradorformapagamento(Colaboradorformapagamento.DEPOSITO_CONTA_POUPANCA));
				} else if (formapagamentoLower.contains("cheque")) {
					b.setColaboradorformapagamento(new Colaboradorformapagamento(Colaboradorformapagamento.CHEQUE));
				} else if (formapagamentoLower.contains("dinheiro")) {
					b.setColaboradorformapagamento(new Colaboradorformapagamento(Colaboradorformapagamento.DINHEIRO));
				} else {
					throw new SinedException("Forma de pagamento n�o existe no sistema. Linha n�mero:"+linhadaplanilha);	
				}
			}
			
			try{
				if(b.getCodigo_banco() != null && !b.getCodigo_banco().equals("")){
					Integer numero = Integer.parseInt(StringUtils.soNumero(b.getCodigo_banco()));
					Banco banco = bancoService.findByNumero(numero);
					b.setBanco(banco);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getDepartamento() == null){
					Departamento departamentoNovo = new Departamento();
					departamentoNovo.setNome(b.getNome_departamento());
					departamentoService.saveOrUpdate(departamentoNovo);
					b.setDepartamento(departamentoNovo);
				}
				
				if(b.getCargo() == null){
					Cargo cargoNovo = new Cargo();
					cargoNovo.setAtivo(Boolean.TRUE);
					cargoNovo.setCbo(b.getCbo());
					cargoNovo.setCodigoFolha(b.getCodigo_cargo());
					cargoNovo.setTipocargo(Tipocargo.MOD);
					cargoService.saveOrUpdate(cargoNovo);
					b.setCargo(cargoNovo);
				}
				
				if(!cargodepartamentoService.haveCargoDepartamento(b.getCargo(), b.getDepartamento())){
					Cargodepartamento cargodepartamentoNovo = new Cargodepartamento();
					cargodepartamentoNovo.setCargo(b.getCargo());
					cargodepartamentoNovo.setDepartamento(b.getDepartamento());
					cargodepartamentoService.saveOrUpdate(cargodepartamentoNovo);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				if(b.getEtnia() != null && !b.getEtnia().equals("")){
					if(b.getEtnia().trim().toUpperCase().startsWith("INDIGENA")){
						b.setEtniaObj(Etnia.INDIGENA);
					} else if(b.getEtnia().trim().toUpperCase().startsWith("BRANCA")){
						b.setEtniaObj(Etnia.BRANCA);
					} else if(b.getEtnia().trim().toUpperCase().startsWith("NEGRA")){
						b.setEtniaObj(Etnia.NEGRA);
					} else if(b.getEtnia().trim().toUpperCase().startsWith("AMARELA")){
						b.setEtniaObj(Etnia.AMARELA);
					} else if(b.getEtnia().trim().toUpperCase().startsWith("PARDA")){
						b.setEtniaObj(Etnia.PARDA);
					}else {
						b.setEtniaObj(Etnia.NAO_INFORMADO);
					}
				}else {
					b.setEtniaObj(Etnia.NAO_INFORMADO);
				}
			} catch (Exception e) {
				e.printStackTrace();
				b.setEtniaObj(Etnia.NAO_INFORMADO);
			}
			
			try{
				if(b.getCodigo_tipo_empregado() != null && !b.getCodigo_tipo_empregado().equals("")){
					Integer codigo_tipo_empregado = Integer.parseInt(StringUtils.soNumero(b.getCodigo_tipo_empregado()));
					Colaboradortipo colaboradortipo = new Colaboradortipo(codigo_tipo_empregado);
					colaboradortipo = colaboradortipoService.load(colaboradortipo);
					b.setColaboradortipoObj(colaboradortipo);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}			
			
			try{
				if(b.getRegime_contratacao() != null && !b.getRegime_contratacao().equals("")){
					if(b.getRegime_contratacao().trim().toUpperCase().startsWith("AUTONOMO")){
						b.setRegimecontratacaoObj(Regimecontratacao.AUTONOMO);
					} else if(b.getRegime_contratacao().trim().toUpperCase().startsWith("ESTAGIARIO")){
						b.setRegimecontratacaoObj(Regimecontratacao.ESTAGIARIO);
					} else if(b.getRegime_contratacao().trim().toUpperCase().startsWith("EMPREGADO")){
						b.setRegimecontratacaoObj(Regimecontratacao.EMPREGADO);
					} else if(b.getRegime_contratacao().trim().toUpperCase().startsWith("SOCIO")){
						b.setRegimecontratacaoObj(Regimecontratacao.SOCIO);
					} else if(b.getRegime_contratacao().trim().toUpperCase().startsWith("TRANSPORTADOR")){
						b.setRegimecontratacaoObj(Regimecontratacao.TRANSPORTADOR);
					} else if(b.getRegime_contratacao().trim().toUpperCase().startsWith("TERCEIRIZADO")){
						b.setRegimecontratacaoObj(Regimecontratacao.TERCEIRIZADO);
					} else {
						b.setRegimecontratacaoObj(Regimecontratacao.EMPREGADO);
					}
				}else {
					b.setRegimecontratacaoObj(Regimecontratacao.EMPREGADO);
				}
			} catch (Exception e) {
				e.printStackTrace();
				b.setRegimecontratacaoObj(Regimecontratacao.EMPREGADO);
			}
			
			linhadaplanilha ++;
		}
	}
	
	private List<ImportarcolaboradoresFPWBean> preencherListaComStrings(ImportarcolaboradoresFPWFiltro filtro) {
		
		String stringTotal = new String(filtro.getArquivo().getContent());
		if(!filtro.getArquivo().getContenttype().equals("application/vnd.ms-excel") && !filtro.getArquivo().getContenttype().equals("text/plain")) {
			throw new SinedException("Formato de arquivo incorreto!");
		} 
		String linha[] = stringTotal.split("\\r?\\n");
		String campo[];
		ImportarcolaboradoresFPWBean bean;
		List<ImportarcolaboradoresFPWBean> listaBean = new ArrayList<ImportarcolaboradoresFPWBean>();
		for (int i = 1; i < linha.length; i++) {
			campo = linha[i].replaceAll("\t", "").split(";");
			if(campo.length == 49){
				bean = new ImportarcolaboradoresFPWBean();
				
				bean.setMatricula(campo[0].trim());
				if(campo[1].trim().isEmpty() || campo[1].trim().length() < 3) {
					throw new SinedException("Valor do campo nome inv�lido. Linha n�mero:"+i);
				} else {
					bean.setNome(campo[1].trim());
				}
				if(campo[2].trim().isEmpty()) {
					throw new SinedException("Valor do campo sexo inv�lido. Linha n�mero:"+i);
				} else {
					String ch = campo[2].trim().substring(0, 1).toUpperCase();
					if(!ch.equals(Sexo.FEMININO_CHAR) && !ch.equals(Sexo.MASCULINO_CHAR)){
						throw new SinedException("Valor do campo sexo inv�lido. Linha n�mero:"+i);
					}
					bean.setSexo(campo[2].trim());
				}
				bean.setData_nascimento(campo[3].trim());
				bean.setEtnia(campo[4].trim());
				
				if(campo[5].trim().isEmpty()) {
					throw new SinedException("Valor do campo estado civil inv�lido. Linha n�mero:"+i);
				} else {
					bean.setEstado_civil(campo[5].trim());
				}
				
				try {
					Integer.parseInt(campo[6].trim());
					bean.setCodigo_grau_instrucao(campo[6].trim());
				} catch (NumberFormatException e) {
					throw new SinedException("Valor do campo grau de instru��o inv�lido (deve ser usado o c�digo). Linha n�mero:"+i);
				}
				
				bean.setNacionalidade(campo[7].trim()); 
				bean.setNaturalidade(campo[8].trim());
				if(campo[9].trim().isEmpty()) {
					throw new SinedException("Valor do campo empresa inv�lido. Linha n�mero:"+i);
				} else {
					bean.setNome_empresa(campo[9].trim());
				}			
				
				try {
					Integer.parseInt(campo[10].trim());				
					bean.setCodigo_tipo_empregado(campo[10].trim());
				} catch (NumberFormatException e) {
					throw new SinedException("Valor do campo c�digo tipo de colaborador inv�lido. Linha n�mero:"+i);
				}
				
				bean.setLogradouro(campo[11].trim());
				bean.setNumero_endereco(campo[12].trim());
				bean.setBairro(campo[13].trim());
				if(campo[14].trim().isEmpty() || campo[14].trim().length() != 7) {
					throw new SinedException("Valor do campo c�digo do munic�pio inv�lido. Linha n�mero:"+i);
				}else {
					bean.setCodigo_municipio(campo[14].trim());
				}
				bean.setUf(campo[15].trim());
				if(campo[16].isEmpty() || campo[16].trim().length() != 8){
					throw new SinedException("Valor do campo CEP inv�lido. Linha n�mero:"+i);
				}else {
					bean.setCep(campo[16].trim());
				}
				bean.setTelefone(campo[17].trim());
				bean.setCelular(campo[18].trim());
				if(campo[19].trim().isEmpty() || campo[19].trim().length() < 3) {
					throw new SinedException("Valor do campo nome da m�e inv�lido. Linha n�mero:"+i);
				}else {
					bean.setNome_mae(campo[19].trim());
				}
				bean.setCodigo_caged(campo[20].trim());
				bean.setData_admissao(campo[21].trim());
				bean.setNome_departamento(campo[22].trim());
				if(campo[23].isEmpty()){
					throw new SinedException("Valor do campo data in�cio cargo. Linha n�mero:"+i);
				}else {
					bean.setData_inicio_cargo(campo[23].trim());
				}
				if(campo[24].isEmpty()){
					throw new SinedException("Valor do campo situa��o do cargo. Linha n�mero:"+i);
				}else {
					bean.setSituacao_cargo(campo[24].trim());
				}
				if(campo[25].isEmpty()){
					throw new SinedException("Valor do campo c�digo do cargo inv�lido. Linha n�mero:"+i);
				}else {
					try {
						bean.setCodigo_cargo(Integer.parseInt(campo[25].trim()));						
					} catch (NumberFormatException e) {
						throw new SinedException("Valor do campo c�digo do cargo inv�lido. Linha n�mero:"+i);
					}
				}
				bean.setCodigo_cbo(campo[26].trim());
				bean.setRegime_contratacao(campo[27].trim());
				bean.setCodigo_sindicato(campo[28].trim());
				if(campo[29].trim().isEmpty() || campo[29].trim().length() < 8) {
					throw new SinedException("Valor do campo n�mero do RG inv�lido. Linha n�mero:"+i);
				}else {
					bean.setRg_numero(campo[29].trim());
				}
				if(campo[30].trim().isEmpty() || campo[30].trim().length() < 8) {
					throw new SinedException("Valor do campo data da emiss�o inv�lido. Linha n�mero:"+i);
				}else {
					bean.setRg_data_emissao(campo[30].trim());
				}
				if(campo[31].trim().isEmpty() || campo[31].trim().length() < 3) {
					throw new SinedException("Valor do campo org�o emissor inv�lido. Linha n�mero:"+i);
				}else {
					bean.setRg_orgao_emissor(campo[31].trim());
				}
				if(campo[32].trim().isEmpty() || campo[32].trim().length() < 2) {
					throw new SinedException("Valor do campo UF do RG inv�lido. Linha n�mero:"+i);
				}else {
					bean.setRg_uf(campo[32].trim());
				}
				bean.setTitulo_eleitor_numero(campo[33].trim());
				bean.setTitulo_eleitor_secao(campo[34].trim());
				bean.setTitulo_eleitor_zona(campo[35].trim());
				bean.setPis_pasep(campo[36].trim());
				if(campo[37].trim().isEmpty() || campo[37].trim().length() < 11) {
					throw new SinedException("Valor do campo CPF inv�lido. Linha n�mero:"+i);
				}else {
					bean.setCpf(campo[37].trim());	
				}
				bean.setCodigo_projeto(campo[38].trim());
				bean.setNome_pai(campo[39].trim());
				bean.setCtps_dataemissao(campo[40].trim());
				bean.setCtps_numero(campo[41].trim());
				bean.setCtps_serie(campo[42].trim());
				bean.setCtps_uf(campo[43].trim());
				if(campo[44].trim().isEmpty()) {
					throw new SinedException("Valor do campo forma de pagamento inv�lido. Linha n�mero:"+i);
				}else {
					bean.setForma_pagamento(campo[44].trim());
				}
				
				if(!campo[45].trim().isEmpty() && campo[45].trim().length() > 3) {
					throw new SinedException("Valor do campo c�digo do banco inv�lido. Linha n�mero:"+i);
				}else {
					bean.setCodigo_banco(campo[45].trim());
				}
				
				bean.setConta_agencia(campo[46].trim());
				
				if(!campo[47].trim().isEmpty() && campo[47].trim().length() < 2){
					throw new SinedException("Valor do campo N�mero da Conta Banc�ria inv�lido. Deve ter no m�nimo 2 caracteres. Linha n�mero:"+i);
				}else{
					bean.setConta_numero(campo[47].trim());
				}
				
				if(!campo[48].trim().isEmpty() && campo[48].trim().length() > 5) {
					throw new SinedException("Valor do campo Opera��o da Conta Banc�ria inv�lido. Deve ter no m�ximo 5 caracteres. Linha n�mero:"+i);
				}else {
					bean.setConta_operacao(campo[48].trim());					
				}
		
				listaBean.add(bean);	
				
			} else if (campo.length < 49) {
				throw new SinedException("Arquivo fora do padr�o de importa��o. O n�mero de colunas � menor do que 47");
			} else if (campo.length > 49){
				throw new SinedException("Arquivo fora do padr�o de importa��o. O n�mero de colunas � maior do que 47");
			}
		
		}
		
		return listaBean;
	}
	
}
