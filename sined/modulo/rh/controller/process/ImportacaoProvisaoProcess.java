package br.com.linkcom.sined.modulo.rh.controller.process;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.geral.bean.ImportacaoProvisao;
import br.com.linkcom.sined.geral.bean.ImportacaoProvisaoColuna;
import br.com.linkcom.sined.geral.bean.Provisao;
import br.com.linkcom.sined.geral.bean.enumeration.ImportacaoProvisaoCampo;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EventoPagamentoService;
import br.com.linkcom.sined.geral.service.FechamentoFolhaService;
import br.com.linkcom.sined.geral.service.ImportacaoProvisaoColunaService;
import br.com.linkcom.sined.geral.service.ImportacaoProvisaoService;
import br.com.linkcom.sined.geral.service.ProvisaoService;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoProvisaoErroBean;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ImportacaoProvisaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/rh/process/ImportacaoProvisao", authorizationModule=ProcessAuthorizationModule.class)
public class ImportacaoProvisaoProcess extends MultiActionController {

	
	private EmpresaService empresaService;
	private ColaboradorService colaboradorService;
	private EventoPagamentoService eventoPagamentoService;
	private ProvisaoService provisaoService;
	private ImportacaoProvisaoColunaService importacaoProvisaoColunaService;
	private ImportacaoProvisaoService importacaoProvisaoService;
	private ArquivoService arquivoService;
	private FechamentoFolhaService fechamentoFolhaService;
	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setEventoPagamentoService(EventoPagamentoService eventoPagamentoService) {this.eventoPagamentoService = eventoPagamentoService;}
	public void setProvisaoService(ProvisaoService provisaoService) {this.provisaoService = provisaoService;}
	public void setImportacaoProvisaoColunaService(ImportacaoProvisaoColunaService importacaoProvisaoColunaService) {this.importacaoProvisaoColunaService = importacaoProvisaoColunaService;}
	public void setImportacaoProvisaoService(ImportacaoProvisaoService importacaoProvisaoService) {this.importacaoProvisaoService = importacaoProvisaoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setFechamentoFolhaService(FechamentoFolhaService fechamentoFolhaService) {this.fechamentoFolhaService = fechamentoFolhaService;}
	
	@DefaultAction
	@Command(session=false)
	public ModelAndView index(WebRequestContext request, ImportacaoProvisaoFiltro filtro) {
		filtro.setArquivo(null);
		
		return new ModelAndView("process/importacaoProvisao", "filtro", filtro);
	}
	
	public ModelAndView carregarArquivo(WebRequestContext request, ImportacaoProvisaoFiltro filtro){
		if(filtro.getArquivo() == null || filtro.getArquivo().getContent() == null || filtro.getArquivo().getContent().length == 0) {
			throw new SinedException("O arquivo deve ser informado.");		
		} else if(filtro.getArquivo() != null && !filtro.getArquivo().getNome().endsWith(".csv")) {
			request.addError("N�o � possivel importar arquivos que n�o estejam no formato CSV.");
			return index(request, filtro);
		}
			
		String strFile = new String(filtro.getArquivo().getContent());
		String[] linhas = strFile.split("\\r?\\n");
		List<String> linhasPreenchidas = new ArrayList<String>();
		for (int i = 1; i < linhas.length; i++) {
			String linha = linhas[i];
			if(linha != null && !linha.replaceAll(";", "").trim().equals("")){
				linhasPreenchidas.add(linha);
			}
		}
		if(linhasPreenchidas.size() == 0){
			throw new SinedException("Nenhum registro a ser importado.");		
		}
		
		List<String> listaErro = new ArrayList<String>();
		Integer numeroLinha = 1;
		List<Provisao> lista = new ArrayList<Provisao>();
		List<ImportacaoProvisaoErroBean> listaBeanErro = new ArrayList<ImportacaoProvisaoErroBean>();
		List<EventoPagamento> listaEvento = eventoPagamentoService.findForImportacaoProvisionamento();
		for (String linha : linhasPreenchidas) {
			numeroLinha++;
			String erro = "";
			String[] campo = linha.split(";");
			String strStatus = null;
			String strDtReferencia = campo[4].trim();
			String cnpjEmpresa = br.com.linkcom.neo.util.StringUtils.soNumero(getCampo(campo, ImportacaoProvisaoCampo.MATRICULA, 2).trim());
			if(campo.length < 19 ){
				msgErroLinha(numeroLinha, "Matr�cula n�o preenchida.");
				continue;
			}
			
			String strMatricula = getCampo(campo, ImportacaoProvisaoCampo.MATRICULA, 18);
			
			strDtReferencia = ajustaAnoComDoisDigitos(strDtReferencia);
			
			
			try {
				strStatus = campo[7].trim();
			} catch (Exception e) {
				continue;
			}
			
			if(!strStatus.equalsIgnoreCase("2")){
				continue;
			}
			Money valor = null;
			Cnpj cnpj = null;
			Long matricula = null;
			Date dtReferencia = null;
			Colaborador colaborador = null;
			Empresa empresa = null;
			
			try {
				try {
					dtReferencia = SinedDateUtils.stringToDate(strDtReferencia);
				} catch (ParseException e) {
					erro = msgErroLinha(numeroLinha, "Data de refer�ncia inv�lida.");
					continue;
				}
				try {
					cnpj = new Cnpj(cnpjEmpresa);
				} catch (Exception e) {
					erro = msgErroLinha(numeroLinha, "CNPJ inv�lido.");
					continue;
				}
				
				empresa = empresaService.carregaEmpresaByCnpj(cnpj);
				if(empresa == null){
					erro = msgErroLinha(numeroLinha, "Empresa n�o encontrada com o CNPJ informado.");
					continue;
				}
				
				try {
					matricula = Long.parseLong(strMatricula);
				} catch (Exception e) {
					erro = msgErroLinha(numeroLinha, "N�mero de matr�cula inv�lido.");
					continue;
				}
				
				colaborador = colaboradorService.findByMatricula(matricula, empresa);
				if(colaborador == null){
					List<Colaborador> listaColaborador = colaboradorService.findAllByMatricula(matricula);
					if(SinedUtil.isListNotEmpty(listaColaborador)){
						List<String> listaCdColaborador = new ArrayList<String>();
						boolean error = false;
						for(Colaborador colaboradorBean: listaColaborador){
							if(!listaCdColaborador.contains(colaboradorBean.getCdpessoa().toString())){
								if(SinedUtil.isListNotEmpty(listaCdColaborador)){
									erro = msgErroLinha(numeroLinha, "Existe mais de um colaborador com o n� de matr�cula "+matricula+" cadastrado no sistema.");
									error = true;
									break;
								}
								listaCdColaborador.add(colaboradorBean.getCdpessoa().toString());
							}
						}
						if(error){
							continue;
						}
						colaborador = listaColaborador.get(0);
						colaborador.setEmpresa(colaborador.getListaColaboradorcargo().iterator().next().getEmpresa());
						

						if(colaborador.getEmpresa() != null && !empresa.equals(listaColaborador.get(0).getEmpresa())){
							erro = msgErroLinha(numeroLinha, "Colaborador n�o encontrado com o n� de matr�cula "+matricula+" para a empresa "+empresa.getNome()+".");
							continue;
						}
					}else{
						erro = msgErroLinha(numeroLinha, "Colaborador n�o encontrado com o n� de matr�cula "+matricula+".");
						continue;
					}
				}
				for(EventoPagamento eventoPagamento: listaEvento){
					int i = eventoPagamento.getNumeroColuna();
					if(campo.length > i && campo[i] != null){
						String strValor = campo[i].trim().replace("\"", "").replace("'", "");
						if(StringUtils.isNotBlank(strValor)){
							if(eventoPagamento.getColaboradorDespesaItemTipoImportacao() == null){
								request.addError("� necess�rio preencher o tipo de opera��o (Importa��o) no cadastro do evento. " + (StringUtils.isNotBlank(eventoPagamento.getNome()) ? "(" + eventoPagamento.getNome() + ")" : ""));
								erro = msgErroLinha(numeroLinha, "Valor inv�lido.");
							}else {
								try {
									valor = new Money(new Double(strValor.replace(",", ".")));
									Provisao bean = new Provisao();
									bean.setColaborador(colaborador);
									bean.setDtProvisao(dtReferencia);
									bean.setEmpresa(empresa);
									bean.setEvento(eventoPagamento);
									bean.setTipo(eventoPagamento.getColaboradorDespesaItemTipoImportacao());
									
									
									bean.setValor(valor);
									lista.add(bean);
									
									
								} catch (Exception e) {
									erro = msgErroLinha(numeroLinha, "Valor inv�lido.");
								}					
							}
						}
					}
				}
				

			} catch (Exception e) {
				e.printStackTrace();
				listaErro.add(e.getMessage());
			} finally{
				
				//bean.setArquivo(filtro.getArquivo());
				if(StringUtils.isNotBlank(erro)){
					ImportacaoProvisaoErroBean beanErro = new ImportacaoProvisaoErroBean();
					beanErro.setMatricula(strMatricula);
					beanErro.setCnpj(cnpjEmpresa);
					beanErro.setData(strDtReferencia);
					beanErro.setErro(erro);
					listaBeanErro.add(beanErro);	
				}
			}
		}
		filtro.setListaBean(lista);
		filtro.setListaBeanComErro(listaBeanErro);
		
		if(listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			return sendRedirectToAction("index");
		}
		String identificadorSessao = SinedDateUtils.currentTimestamp().toString();
		request.getSession().setAttribute("importacaoprovisao_" + identificadorSessao, filtro);
		request.setAttribute("identificador", "importacaoprovisao_" + identificadorSessao);
		
		return new ModelAndView("process/importacaoProvisaoListagem", "filtro", filtro);
	}
	
	private String ajustaAnoComDoisDigitos(String data){
		String result = data;
		if(data.split("/").length > 1 && data.split("/")[2].length()==2){
			result = data.split("/")[0]+"/"+
					data.split("/")[1]+"/20"+
					data.split("/")[2];
		}
		return result;
	}
	
	private String msgErroLinha(Integer linha, String msg){
		return "Linha "+linha.toString()+". "+msg;
	}
	
	public ModelAndView save(WebRequestContext request, ImportacaoProvisaoFiltro filtro){
		String identificador = request.getParameter("identificador");
		
		ImportacaoProvisaoFiltro  objFiltro = (ImportacaoProvisaoFiltro) request.getSession().getAttribute(identificador);
		if(SinedUtil.isListEmpty(objFiltro.getListaBean())){
			throw new SinedException("N�o existem item a serem importados."); 
		}
		
		for (Provisao itemProvisao : objFiltro.getListaBean()) {
			if (fechamentoFolhaService.existeFechamentoPeriodo(itemProvisao.getEmpresa(), itemProvisao.getDtProvisao())){
				request.addError("N�o foi poss�vel importar. J� existe um fechamento de folha correspondente � data informada.");
				return sendRedirectToAction("index");
			}
		}
		
		provisaoService.saveOrUpdateListNoUseTransaction(objFiltro.getListaBean());		
		
		arquivoService.saveOrUpdate(objFiltro.getArquivo());
		
		ImportacaoProvisao importacaoProvisao = new ImportacaoProvisao();
		importacaoProvisao.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		importacaoProvisao.setDtaltera(new Timestamp(System.currentTimeMillis()));
		importacaoProvisao.setArquivo(objFiltro.getArquivo());
			
		importacaoProvisaoService.saveOrUpdate(importacaoProvisao);
		
		request.addMessage("Importa��o realizada com sucesso!");
		return sendRedirectToAction("index");
	}
	
	private String getCampo(String[] campo, ImportacaoProvisaoCampo importacaoProvisaoCampo, int coluna) {
		String campoImportacao = null;
		
		if(campo != null && campo.length > 0){
			campoImportacao = campo[coluna];
			List<ImportacaoProvisaoColuna> lista = importacaoProvisaoColunaService.findByTipo(importacaoProvisaoCampo);
			if(SinedUtil.isListNotEmpty(lista)){
				for(ImportacaoProvisaoColuna importacaoProvisaoColuna : lista){
					if(importacaoProvisaoColuna.getColuna() != null && campo.length > importacaoProvisaoColuna.getColuna()){
						campoImportacao = campo[importacaoProvisaoColuna.getColuna()];
						if(StringUtils.isNotBlank(campoImportacao)){
							break;
						}
					}
				}
			}
		}
		return campoImportacao;
	}
}
