package br.com.linkcom.sined.modulo.rh.controller.process;

import java.lang.reflect.Method;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sourceforge.jeval.EvaluationException;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradordespesaService;
import br.com.linkcom.sined.geral.service.FormularhService;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.Colaboradorholerite;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.HoleriteFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;

@Bean
@Controller(path="/rh/process/Gerarholerite", authorizationModule=CrudAuthorizationModule.class)
public class HoleriteProcess extends MultiActionController{
	
	private ColaboradorService colaboradorService;	
	private FormularhService formularhService;
	private ColaboradordespesaService colaboradordespesaService;
	
	public void setFormularhService(FormularhService formularhService) {this.formularhService = formularhService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setColaboradordespesaService(ColaboradordespesaService colaboradordespesaService) {this.colaboradordespesaService = colaboradordespesaService;}
	
	@DefaultAction
	public ModelAndView alterar(WebRequestContext request, HoleriteFiltro filtro) throws Exception {		
		if(filtro.getEmpresa() == null){
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null){
				Method methodSetEmpresa = Util.beans.getSetterMethod(filtro.getClass(), "empresa");
				if(methodSetEmpresa != null && methodSetEmpresa.getParameterTypes()[0].equals(Empresa.class)){
					methodSetEmpresa.invoke(filtro, empresa);
				}
			}
		}
		return new ModelAndView("process/gerarholerite", "filtro", filtro);
	}
	
	/**
	 * M�todo para carregar listagem do filtro no jsp via ajax.
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#findColaboradorAltera
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @author Filipe Santos
	 */
	public ModelAndView listar(WebRequestContext request, HoleriteFiltro filtro){
		if (filtro == null || filtro.getDtreferencia1() == null || filtro.getDtreferencia2() == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		request.getSession().setAttribute("HoleriteFiltro", filtro);
		
		List<Colaboradorholerite> listaColaborador = colaboradorService.findByHolerite(filtro);
		
		Projeto projeto = filtro.getProjeto();
		Empresa empresa = filtro.getEmpresa();
		Date dtreferencia1 = filtro.getDtreferencia1();
		Date dtreferencia2 = filtro.getDtreferencia2();
		Boolean holeritePorProjeto = filtro.getHoleritePorProjeto();
		
		for (Colaboradorholerite colaboradorholerite : listaColaborador) {
			Integer cdpessoa = colaboradorholerite.getCdpessoa();
			List<Date> listDate = SinedDateUtils.getListDateByPeriodo(dtreferencia1, dtreferencia2);
			
			List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForListagemHolerite(dtreferencia1, dtreferencia2, empresa, projeto, new Colaborador(cdpessoa), holeritePorProjeto);
			for (Colaboradordespesa colaboradordespesa : listaColaboradordespesa) {
				for (Iterator<Date> iterator = listDate.iterator(); iterator.hasNext();) {
					Date data = (Date) iterator.next();
					if(SinedDateUtils.beforeOrEqualIgnoreHour(colaboradordespesa.getDtholeriteinicio(), data) &&
						SinedDateUtils.afterOrEqualsIgnoreHour(colaboradordespesa.getDtholeritefim(), data)){
						iterator.remove();
					}
				}
			}
			
			colaboradorholerite.setGerado(listaColaboradordespesa.size() > 0);
			colaboradorholerite.setParcial(listDate.size() > 0);
		}
		
		return new ModelAndView("direct:ajax/gerarholeritelistagem", "listaColaborador", listaColaborador);
	}
	
	/**
	 * M�todo que gera a holerite dos colaboradores, em cima das formulas cadastradas para seus respectivos cargos.
	 * @author Filipe Santos
	 * @param request
	 * @param filtro
	 * @return
	 * @throws EvaluationException 
	 */
	public ModelAndView calcularHolerite(WebRequestContext request, HoleriteFiltro filtro) throws EvaluationException{
		if(filtro.getCodigos() == null || filtro.getCodigos().equals("")){
			request.addError("Nenhum colaborador encontrado para gerar holerite.");
			return new ModelAndView("process/gerarholerite", "filtro", filtro);
		}
		
		try{
			formularhService.calcularAndGerarHolerite(filtro.getEmpresa(), filtro.getCodigos(), filtro.getDtreferencia1(), filtro.getDtreferencia2(), filtro.getProjeto(), filtro.getHoleritePorProjeto());	
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return new ModelAndView("process/gerarholerite", "filtro", filtro);
		}
		
		request.addMessage("Pagamento gerado com sucesso.");
		return sendRedirectToAction("alterar");
	}
	
	public ModelAndView consultarDespesacolaborador(WebRequestContext request, HoleriteFiltro filtro){
		List<Colaboradordespesa> listaColaboradordespesa = new ArrayList<Colaboradordespesa>();
		
		listaColaboradordespesa = colaboradordespesaService.findForConsultarColaboradordespesa(filtro.getCdcolaborador(), filtro.getDtreferencia1(), filtro.getDtreferencia2());
		if(listaColaboradordespesa != null && listaColaboradordespesa.size() > 0){
			if(listaColaboradordespesa.size() == 1)
				return new ModelAndView("redirect:/rh/crud/Colaboradordespesa?ACAO=consultar&cdcolaboradordespesa=" + listaColaboradordespesa.get(0).getCdcolaboradordespesa());
			else {								
				return new ModelAndView("redirect:/rh/crud/Colaboradordespesa?ACAO=listagem&cdcolaborador=" + filtro.getCdcolaborador() 
						+ "&dtreferencia1=" + (filtro.getDtreferencia1() != null ? SinedDateUtils.toString(filtro.getDtreferencia1()) : "") 
						+ "&dtreferencia2=" + (filtro.getDtreferencia2() != null ? SinedDateUtils.toString(filtro.getDtreferencia2()) : ""));
			}
		}
				
		View.getCurrent().println("<script>window.close();</script>");
		return null;
	}
}