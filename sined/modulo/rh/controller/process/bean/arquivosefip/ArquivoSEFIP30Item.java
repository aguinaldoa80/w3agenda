package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;


public class ArquivoSEFIP30Item {
	
	private Integer cdcolaborador;
	private String nome;
	private String pisPasepCi;
	private String cbo;
	private Money remuneracaoSem13;
	private Money valorDescontadoSegurado;
	private Cliente cliente;
	
	public Integer getCdcolaborador() {
		return cdcolaborador;
	}
	public String getNome() {
		return nome;
	}
	public String getPisPasepCi() {
		return pisPasepCi;
	}
	public String getCbo() {
		return cbo;
	}
	public Money getRemuneracaoSem13() {
		return remuneracaoSem13;
	}
	public Money getValorDescontadoSegurado() {
		return valorDescontadoSegurado;
	}
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCdcolaborador(Integer cdcolaborador) {
		this.cdcolaborador = cdcolaborador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setPisPasepCi(String pisPasepCi) {
		this.pisPasepCi = pisPasepCi;
	}
	public void setCbo(String cbo) {
		this.cbo = cbo;
	}
	public void setRemuneracaoSem13(Money remuneracaoSem13) {
		this.remuneracaoSem13 = remuneracaoSem13;
	}
	public void setValorDescontadoSegurado(Money valorDescontadoSegurado) {
		this.valorDescontadoSegurado = valorDescontadoSegurado;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}