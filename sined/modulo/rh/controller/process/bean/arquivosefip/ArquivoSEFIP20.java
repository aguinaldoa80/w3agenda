package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip;

import br.com.linkcom.sined.util.SinedUtil;


public class ArquivoSEFIP20 {
	
	private long posicao_1_2 = 20;
	private long posicao_3_3 = 1;
	private long posicao_4_17;
	private long posicao_18_18 = 1;
	private long posicao_19_32;
	private long posicao_33_53;
	private String posicao_54_93 = "";
	private String posicao_94_143 = "";
	private String posicao_144_163 = "";
	private long posicao_164_171;
	private String posicao_172_191 = "";
	private String posicao_192_193 = "";
	private String posicao_194_197 = "";
	private long posicao_198_212;
	private long posicao_213_227;
	private long posicao_228_228;
	private long posicao_229_242;
	private long posicao_243_257;
	private long posicao_258_272;
	private long posicao_273_317;
	private String posicao_318_359 = "";
	private String posicao_360_360 = "*";

	public String gerarLinha(){
		
		StringBuilder linha = new StringBuilder();
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_1_2), 2, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_3_3), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_4_17), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_18_18), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_19_32), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_33_53), 21, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_54_93), 40, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_94_143), 50, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_144_163), 20, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_164_171), 8, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_172_191), 20, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_192_193), 2, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_194_197), 4, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_198_212), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_213_227), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_228_228), 1, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_229_242), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_243_257), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_258_272), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_273_317), 45, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_318_359), 42, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_360_360), 1, ' ', false));
		
		return linha.toString();		
	}

	public long getPosicao_1_2() {
		return posicao_1_2;
	}

	public long getPosicao_3_3() {
		return posicao_3_3;
	}

	public long getPosicao_4_17() {
		return posicao_4_17;
	}

	public long getPosicao_18_18() {
		return posicao_18_18;
	}

	public long getPosicao_19_32() {
		return posicao_19_32;
	}

	public long getPosicao_33_53() {
		return posicao_33_53;
	}

	public String getPosicao_54_93() {
		return posicao_54_93;
	}

	public String getPosicao_94_143() {
		return posicao_94_143;
	}

	public String getPosicao_144_163() {
		return posicao_144_163;
	}

	public long getPosicao_164_171() {
		return posicao_164_171;
	}

	public String getPosicao_172_191() {
		return posicao_172_191;
	}

	public String getPosicao_192_193() {
		return posicao_192_193;
	}

	public String getPosicao_194_197() {
		return posicao_194_197;
	}

	public long getPosicao_198_212() {
		return posicao_198_212;
	}

	public long getPosicao_213_227() {
		return posicao_213_227;
	}

	public long getPosicao_228_228() {
		return posicao_228_228;
	}

	public long getPosicao_229_242() {
		return posicao_229_242;
	}

	public long getPosicao_243_257() {
		return posicao_243_257;
	}

	public long getPosicao_258_272() {
		return posicao_258_272;
	}

	public long getPosicao_273_317() {
		return posicao_273_317;
	}

	public String getPosicao_318_359() {
		return posicao_318_359;
	}

	public String getPosicao_360_360() {
		return posicao_360_360;
	}

	public void setPosicao_1_2(long posicao_1_2) {
		this.posicao_1_2 = posicao_1_2;
	}

	public void setPosicao_3_3(long posicao_3_3) {
		this.posicao_3_3 = posicao_3_3;
	}

	public void setPosicao_4_17(long posicao_4_17) {
		this.posicao_4_17 = posicao_4_17;
	}

	public void setPosicao_18_18(long posicao_18_18) {
		this.posicao_18_18 = posicao_18_18;
	}

	public void setPosicao_19_32(long posicao_19_32) {
		this.posicao_19_32 = posicao_19_32;
	}

	public void setPosicao_33_53(long posicao_33_53) {
		this.posicao_33_53 = posicao_33_53;
	}

	public void setPosicao_54_93(String posicao_54_93) {
		this.posicao_54_93 = posicao_54_93;
	}

	public void setPosicao_94_143(String posicao_94_143) {
		this.posicao_94_143 = posicao_94_143;
	}

	public void setPosicao_144_163(String posicao_144_163) {
		this.posicao_144_163 = posicao_144_163;
	}

	public void setPosicao_164_171(long posicao_164_171) {
		this.posicao_164_171 = posicao_164_171;
	}

	public void setPosicao_172_191(String posicao_172_191) {
		this.posicao_172_191 = posicao_172_191;
	}

	public void setPosicao_192_193(String posicao_192_193) {
		this.posicao_192_193 = posicao_192_193;
	}

	public void setPosicao_194_197(String posicao_194_197) {
		this.posicao_194_197 = posicao_194_197;
	}

	public void setPosicao_198_212(long posicao_198_212) {
		this.posicao_198_212 = posicao_198_212;
	}

	public void setPosicao_213_227(long posicao_213_227) {
		this.posicao_213_227 = posicao_213_227;
	}

	public void setPosicao_228_228(long posicao_228_228) {
		this.posicao_228_228 = posicao_228_228;
	}

	public void setPosicao_229_242(long posicao_229_242) {
		this.posicao_229_242 = posicao_229_242;
	}

	public void setPosicao_243_257(long posicao_243_257) {
		this.posicao_243_257 = posicao_243_257;
	}

	public void setPosicao_258_272(long posicao_258_272) {
		this.posicao_258_272 = posicao_258_272;
	}

	public void setPosicao_273_317(long posicao_273_317) {
		this.posicao_273_317 = posicao_273_317;
	}

	public void setPosicao_318_359(String posicao_318_359) {
		this.posicao_318_359 = posicao_318_359;
	}

	public void setPosicao_360_360(String posicao_360_360) {
		this.posicao_360_360 = posicao_360_360;
	}	
}