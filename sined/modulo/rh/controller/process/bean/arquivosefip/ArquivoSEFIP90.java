package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip;

import br.com.linkcom.sined.util.SinedUtil;


public class ArquivoSEFIP90 {
	
	private long posicao_1_2 = 90;
	private String posicao_3_53 = "9";
	private String posicao_54_359 = "";
	private String posicao_360_360 = "*";
	
	public String gerarLinha(){
		
		StringBuilder linha = new StringBuilder();
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_1_2), 2, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_3_53), 51, '9', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_54_359), 306, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_360_360), 1, ' ', true));
		
		return linha.toString();		
	}

	public long getPosicao_1_2() {
		return posicao_1_2;
	}

	public String getPosicao_3_53() {
		return posicao_3_53;
	}

	public String getPosicao_54_359() {
		return posicao_54_359;
	}

	public String getPosicao_360_360() {
		return posicao_360_360;
	}

	public void setPosicao_1_2(long posicao_1_2) {
		this.posicao_1_2 = posicao_1_2;
	}

	public void setPosicao_3_53(String posicao_3_53) {
		this.posicao_3_53 = posicao_3_53;
	}

	public void setPosicao_54_359(String posicao_54_359) {
		this.posicao_54_359 = posicao_54_359;
	}

	public void setPosicao_360_360(String posicao_360_360) {
		this.posicao_360_360 = posicao_360_360;
	}	
}