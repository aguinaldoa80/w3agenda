package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip;

import br.com.linkcom.sined.util.SinedUtil;


public class ArquivoSEFIP10 {
	
	private long posicao_1_2 = 10;
	private long posicao_3_3 = 1;
	private long posicao_4_17;
	private long posicao_18_53;
	private String posicao_54_93 = "";
	private String posicao_94_143 = "";
	private String posicao_144_163 = "";
	private long posicao_164_171;
	private String posicao_172_191 = "";
	private String posicao_192_193 = "";
	private long posicao_194_205;
	private String posicao_206_206 = "N";
	private long posicao_207_213 = 6023201;
	private String posicao_214_214 = "P";
	private long posicao_215_216 = 30;
	private long posicao_217_217 = 0;
	private long posicao_218_218 = 1;
	private long posicao_219_221 = 612;
	private long posicao_222_225 = 4163;
	private long posicao_226_229 = 2127;
	private String posicao_230_234 = "";
	private long posicao_235_249;
	private long posicao_250_264;
	private long posicao_265_279;
	private long posicao_280_280;
	private long posicao_281_294;
	private String posicao_295_297 = "";
	private String posicao_298_301 = "";
	private String posicao_302_310 = "";
	private long posicao_311_355;
	private String posicao_356_359 = "";
	private String posicao_360_360 = "*";
	
	public String gerarLinha(){
		
		StringBuilder linha = new StringBuilder();
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_1_2), 2, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_3_3), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_4_17), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_18_53), 36, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_54_93), 40, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_94_143), 50, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_144_163), 20, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_164_171), 8, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_172_191), 20, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_192_193), 2, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_194_205), 12, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_206_206), 1, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_207_213), 7, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_214_214), 1, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_215_216), 2, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_217_217), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_218_218), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_219_221), 3, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_222_225), 4, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_226_229), 4, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_230_234), 5, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_235_249), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_250_264), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_265_279), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_280_280), 1, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_281_294), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_295_297), 3, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_298_301), 4, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_302_310), 9, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_311_355), 45, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_356_359), 4, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_360_360), 1, ' ', true));
		
		return linha.toString();		
	}

	public long getPosicao_1_2() {
		return posicao_1_2;
	}

	public long getPosicao_3_3() {
		return posicao_3_3;
	}

	public long getPosicao_4_17() {
		return posicao_4_17;
	}

	public long getPosicao_18_53() {
		return posicao_18_53;
	}

	public String getPosicao_54_93() {
		return posicao_54_93;
	}

	public String getPosicao_94_143() {
		return posicao_94_143;
	}

	public String getPosicao_144_163() {
		return posicao_144_163;
	}

	public long getPosicao_164_171() {
		return posicao_164_171;
	}

	public String getPosicao_172_191() {
		return posicao_172_191;
	}

	public String getPosicao_192_193() {
		return posicao_192_193;
	}

	public long getPosicao_194_205() {
		return posicao_194_205;
	}

	public String getPosicao_206_206() {
		return posicao_206_206;
	}

	public long getPosicao_207_213() {
		return posicao_207_213;
	}

	public String getPosicao_214_214() {
		return posicao_214_214;
	}

	public long getPosicao_215_216() {
		return posicao_215_216;
	}

	public long getPosicao_217_217() {
		return posicao_217_217;
	}

	public long getPosicao_218_218() {
		return posicao_218_218;
	}

	public long getPosicao_219_221() {
		return posicao_219_221;
	}

	public long getPosicao_222_225() {
		return posicao_222_225;
	}

	public long getPosicao_226_229() {
		return posicao_226_229;
	}

	public String getPosicao_230_234() {
		return posicao_230_234;
	}

	public long getPosicao_235_249() {
		return posicao_235_249;
	}

	public long getPosicao_250_264() {
		return posicao_250_264;
	}

	public long getPosicao_265_279() {
		return posicao_265_279;
	}

	public long getPosicao_280_280() {
		return posicao_280_280;
	}

	public long getPosicao_281_294() {
		return posicao_281_294;
	}

	public String getPosicao_295_297() {
		return posicao_295_297;
	}

	public String getPosicao_298_301() {
		return posicao_298_301;
	}

	public String getPosicao_302_310() {
		return posicao_302_310;
	}

	public long getPosicao_311_355() {
		return posicao_311_355;
	}

	public String getPosicao_356_359() {
		return posicao_356_359;
	}

	public String getPosicao_360_360() {
		return posicao_360_360;
	}

	public void setPosicao_1_2(long posicao_1_2) {
		this.posicao_1_2 = posicao_1_2;
	}

	public void setPosicao_3_3(long posicao_3_3) {
		this.posicao_3_3 = posicao_3_3;
	}

	public void setPosicao_4_17(long posicao_4_17) {
		this.posicao_4_17 = posicao_4_17;
	}

	public void setPosicao_18_53(long posicao_18_53) {
		this.posicao_18_53 = posicao_18_53;
	}

	public void setPosicao_54_93(String posicao_54_93) {
		this.posicao_54_93 = posicao_54_93;
	}

	public void setPosicao_94_143(String posicao_94_143) {
		this.posicao_94_143 = posicao_94_143;
	}

	public void setPosicao_144_163(String posicao_144_163) {
		this.posicao_144_163 = posicao_144_163;
	}

	public void setPosicao_164_171(long posicao_164_171) {
		this.posicao_164_171 = posicao_164_171;
	}

	public void setPosicao_172_191(String posicao_172_191) {
		this.posicao_172_191 = posicao_172_191;
	}

	public void setPosicao_192_193(String posicao_192_193) {
		this.posicao_192_193 = posicao_192_193;
	}

	public void setPosicao_194_205(long posicao_194_205) {
		this.posicao_194_205 = posicao_194_205;
	}

	public void setPosicao_206_206(String posicao_206_206) {
		this.posicao_206_206 = posicao_206_206;
	}

	public void setPosicao_207_213(long posicao_207_213) {
		this.posicao_207_213 = posicao_207_213;
	}

	public void setPosicao_214_214(String posicao_214_214) {
		this.posicao_214_214 = posicao_214_214;
	}

	public void setPosicao_215_216(long posicao_215_216) {
		this.posicao_215_216 = posicao_215_216;
	}

	public void setPosicao_217_217(long posicao_217_217) {
		this.posicao_217_217 = posicao_217_217;
	}

	public void setPosicao_218_218(long posicao_218_218) {
		this.posicao_218_218 = posicao_218_218;
	}

	public void setPosicao_219_221(long posicao_219_221) {
		this.posicao_219_221 = posicao_219_221;
	}

	public void setPosicao_222_225(long posicao_222_225) {
		this.posicao_222_225 = posicao_222_225;
	}

	public void setPosicao_226_229(long posicao_226_229) {
		this.posicao_226_229 = posicao_226_229;
	}

	public void setPosicao_230_234(String posicao_230_234) {
		this.posicao_230_234 = posicao_230_234;
	}

	public void setPosicao_235_249(long posicao_235_249) {
		this.posicao_235_249 = posicao_235_249;
	}

	public void setPosicao_250_264(long posicao_250_264) {
		this.posicao_250_264 = posicao_250_264;
	}

	public void setPosicao_265_279(long posicao_265_279) {
		this.posicao_265_279 = posicao_265_279;
	}

	public void setPosicao_280_280(long posicao_280_280) {
		this.posicao_280_280 = posicao_280_280;
	}

	public void setPosicao_281_294(long posicao_281_294) {
		this.posicao_281_294 = posicao_281_294;
	}

	public void setPosicao_295_297(String posicao_295_297) {
		this.posicao_295_297 = posicao_295_297;
	}

	public void setPosicao_298_301(String posicao_298_301) {
		this.posicao_298_301 = posicao_298_301;
	}

	public void setPosicao_302_310(String posicao_302_310) {
		this.posicao_302_310 = posicao_302_310;
	}

	public void setPosicao_311_355(long posicao_311_355) {
		this.posicao_311_355 = posicao_311_355;
	}

	public void setPosicao_356_359(String posicao_356_359) {
		this.posicao_356_359 = posicao_356_359;
	}

	public void setPosicao_360_360(String posicao_360_360) {
		this.posicao_360_360 = posicao_360_360;
	}
}