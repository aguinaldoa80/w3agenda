package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip;

import br.com.linkcom.sined.util.SinedUtil;



public class ArquivoSEFIP00 {
	
	private long posicao_1_2 = 0;
	private String posicao_3_53 = "";
	private long posicao_54_54 = 1;
	private long posicao_55_55 = 1;
	private long posicao_56_69;
	private String posicao_70_99 = "";
	private String posicao_100_119 = "";
	private String posicao_120_169 = "";
	private String posicao_170_189 = "";
	private long posicao_190_197;
	private String posicao_198_217 = "";
	private String posicao_218_219 = "";
	private long posicao_220_231;
	private String posicao_232_291 = "";
	private String posicao_292_297 = "";
	private long posicao_298_300 = 211;
	private String posicao_301_301 = "";
	private long posicao_302_302 = 1;
	private String posicao_303_310 = "";
	private long posicao_311_311 = 1;
	private String posicao_312_319 = "";
	private String posicao_320_326 = "";
	private long posicao_327_327 = 1;
	private long posicao_328_341;
	private String posicao_342_359 = "";
	private String posicao_360_360 = "*";
	
	public String gerarLinha(){
		
		StringBuilder linha = new StringBuilder();
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_1_2), 2, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_3_53), 51, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_54_54), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_55_55), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_56_69), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_70_99), 30, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_100_119), 20, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_120_169), 50, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_170_189), 20, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_190_197), 8, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_198_217), 20, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_NUMEROS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_218_219), 2, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_220_231), 12, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_232_291), 60, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_292_297), 6, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_298_300), 3, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_301_301), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_302_302), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_303_310), 8, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_311_311), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_312_319), 8, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_320_326), 7, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_327_327), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_328_341), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_342_359), 18, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_360_360), 1, ' ', true));
		
		return linha.toString();		
	}

	public long getPosicao_1_2() {
		return posicao_1_2;
	}

	public String getPosicao_3_53() {
		return posicao_3_53;
	}

	public long getPosicao_54_54() {
		return posicao_54_54;
	}

	public long getPosicao_55_55() {
		return posicao_55_55;
	}

	public long getPosicao_56_69() {
		return posicao_56_69;
	}

	public String getPosicao_70_99() {
		return posicao_70_99;
	}

	public String getPosicao_100_119() {
		return posicao_100_119;
	}

	public String getPosicao_120_169() {
		return posicao_120_169;
	}

	public String getPosicao_170_189() {
		return posicao_170_189;
	}

	public long getPosicao_190_197() {
		return posicao_190_197;
	}

	public String getPosicao_198_217() {
		return posicao_198_217;
	}

	public String getPosicao_218_219() {
		return posicao_218_219;
	}

	public long getPosicao_220_231() {
		return posicao_220_231;
	}

	public String getPosicao_232_291() {
		return posicao_232_291;
	}

	public String getPosicao_292_297() {
		return posicao_292_297;
	}

	public long getPosicao_298_300() {
		return posicao_298_300;
	}

	public String getPosicao_301_301() {
		return posicao_301_301;
	}

	public long getPosicao_302_302() {
		return posicao_302_302;
	}

	public String getPosicao_303_310() {
		return posicao_303_310;
	}

	public long getPosicao_311_311() {
		return posicao_311_311;
	}

	public String getPosicao_312_319() {
		return posicao_312_319;
	}

	public String getPosicao_320_326() {
		return posicao_320_326;
	}

	public long getPosicao_327_327() {
		return posicao_327_327;
	}

	public long getPosicao_328_341() {
		return posicao_328_341;
	}

	public String getPosicao_342_359() {
		return posicao_342_359;
	}

	public String getPosicao_360_360() {
		return posicao_360_360;
	}

	public void setPosicao_1_2(long posicao_1_2) {
		this.posicao_1_2 = posicao_1_2;
	}

	public void setPosicao_3_53(String posicao_3_53) {
		this.posicao_3_53 = posicao_3_53;
	}

	public void setPosicao_54_54(long posicao_54_54) {
		this.posicao_54_54 = posicao_54_54;
	}

	public void setPosicao_55_55(long posicao_55_55) {
		this.posicao_55_55 = posicao_55_55;
	}

	public void setPosicao_56_69(long posicao_56_69) {
		this.posicao_56_69 = posicao_56_69;
	}

	public void setPosicao_70_99(String posicao_70_99) {
		this.posicao_70_99 = posicao_70_99;
	}

	public void setPosicao_100_119(String posicao_100_119) {
		this.posicao_100_119 = posicao_100_119;
	}

	public void setPosicao_120_169(String posicao_120_169) {
		this.posicao_120_169 = posicao_120_169;
	}

	public void setPosicao_170_189(String posicao_170_189) {
		this.posicao_170_189 = posicao_170_189;
	}

	public void setPosicao_190_197(long posicao_190_197) {
		this.posicao_190_197 = posicao_190_197;
	}

	public void setPosicao_198_217(String posicao_198_217) {
		this.posicao_198_217 = posicao_198_217;
	}

	public void setPosicao_218_219(String posicao_218_219) {
		this.posicao_218_219 = posicao_218_219;
	}

	public void setPosicao_220_231(long posicao_220_231) {
		this.posicao_220_231 = posicao_220_231;
	}

	public void setPosicao_232_291(String posicao_232_291) {
		this.posicao_232_291 = posicao_232_291;
	}

	public void setPosicao_292_297(String posicao_292_297) {
		this.posicao_292_297 = posicao_292_297;
	}

	public void setPosicao_298_300(long posicao_298_300) {
		this.posicao_298_300 = posicao_298_300;
	}

	public void setPosicao_301_301(String posicao_301_301) {
		this.posicao_301_301 = posicao_301_301;
	}

	public void setPosicao_302_302(long posicao_302_302) {
		this.posicao_302_302 = posicao_302_302;
	}

	public void setPosicao_303_310(String posicao_303_310) {
		this.posicao_303_310 = posicao_303_310;
	}

	public void setPosicao_311_311(long posicao_311_311) {
		this.posicao_311_311 = posicao_311_311;
	}

	public void setPosicao_312_319(String posicao_312_319) {
		this.posicao_312_319 = posicao_312_319;
	}

	public void setPosicao_320_326(String posicao_320_326) {
		this.posicao_320_326 = posicao_320_326;
	}

	public void setPosicao_327_327(long posicao_327_327) {
		this.posicao_327_327 = posicao_327_327;
	}

	public void setPosicao_328_341(long posicao_328_341) {
		this.posicao_328_341 = posicao_328_341;
	}

	public void setPosicao_342_359(String posicao_342_359) {
		this.posicao_342_359 = posicao_342_359;
	}

	public void setPosicao_360_360(String posicao_360_360) {
		this.posicao_360_360 = posicao_360_360;
	}	
}