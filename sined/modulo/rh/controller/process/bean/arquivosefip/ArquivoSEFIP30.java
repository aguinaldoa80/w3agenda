package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip;

import br.com.linkcom.sined.util.SinedUtil;


public class ArquivoSEFIP30 {
	
	private long posicao_1_2 = 30;
	private long posicao_3_3 = 1;
	private long posicao_4_17;
	private long posicao_18_18 = 1;
	private long posicao_19_32;
	private long posicao_33_43;
	private String posicao_44_51 = "";
	private long posicao_52_53 = 17;
	private String posicao_54_123 = "";
	private String posicao_124_134 = "";
	private String posicao_135_141 = "";
	private String posicao_142_146 = "";
	private String posicao_147_154 = "";
	private String posicao_155_162 = "";
	private long posicao_163_167 = 7823;
	private long posicao_168_182;
	private long posicao_183_197;
	private String posicao_198_199 = "";
	private String posicao_200_201 = "";
	private long posicao_202_216;
	private long posicao_217_231;
	private long posicao_232_246;
	private long posicao_247_261;
	private String posicao_262_359 = "";
	private String posicao_360_360 = "*";
	
	public String gerarLinha(){
		
		StringBuilder linha = new StringBuilder();
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_1_2), 2, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_3_3), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_4_17), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_18_18), 1, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_19_32), 14, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_33_43), 11, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_44_51), 8, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_52_53), 2, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_54_123), 70, ' ', true, SinedUtil.REGEX_APENAS_LETRAS_SEFIP));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_124_134), 11, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_135_141), 7, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_142_146), 5, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_147_154), 8, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_155_162), 8, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_163_167), 5, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_168_182), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_183_197), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_198_199), 2, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_200_201), 2, ' ', true));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_202_216), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_217_231), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_232_246), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_247_261), 15, '0', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_262_359), 98, ' ', false));
		linha.append(SinedUtil.formataTamanho(String.valueOf(posicao_360_360), 1, ' ', false));
		
		return linha.toString();		
	}

	public long getPosicao_1_2() {
		return posicao_1_2;
	}

	public long getPosicao_3_3() {
		return posicao_3_3;
	}

	public long getPosicao_4_17() {
		return posicao_4_17;
	}

	public long getPosicao_18_18() {
		return posicao_18_18;
	}

	public long getPosicao_19_32() {
		return posicao_19_32;
	}

	public long getPosicao_33_43() {
		return posicao_33_43;
	}

	public String getPosicao_44_51() {
		return posicao_44_51;
	}

	public long getPosicao_52_53() {
		return posicao_52_53;
	}

	public String getPosicao_54_123() {
		return posicao_54_123;
	}

	public String getPosicao_124_134() {
		return posicao_124_134;
	}

	public String getPosicao_135_141() {
		return posicao_135_141;
	}

	public String getPosicao_142_146() {
		return posicao_142_146;
	}

	public String getPosicao_147_154() {
		return posicao_147_154;
	}

	public String getPosicao_155_162() {
		return posicao_155_162;
	}

	public long getPosicao_163_167() {
		return posicao_163_167;
	}

	public long getPosicao_168_182() {
		return posicao_168_182;
	}

	public long getPosicao_183_197() {
		return posicao_183_197;
	}

	public String getPosicao_198_199() {
		return posicao_198_199;
	}

	public String getPosicao_200_201() {
		return posicao_200_201;
	}

	public long getPosicao_202_216() {
		return posicao_202_216;
	}

	public long getPosicao_217_231() {
		return posicao_217_231;
	}

	public long getPosicao_232_246() {
		return posicao_232_246;
	}

	public long getPosicao_247_261() {
		return posicao_247_261;
	}

	public String getPosicao_262_359() {
		return posicao_262_359;
	}

	public String getPosicao_360_360() {
		return posicao_360_360;
	}

	public void setPosicao_1_2(long posicao_1_2) {
		this.posicao_1_2 = posicao_1_2;
	}

	public void setPosicao_3_3(long posicao_3_3) {
		this.posicao_3_3 = posicao_3_3;
	}

	public void setPosicao_4_17(long posicao_4_17) {
		this.posicao_4_17 = posicao_4_17;
	}

	public void setPosicao_18_18(long posicao_18_18) {
		this.posicao_18_18 = posicao_18_18;
	}

	public void setPosicao_19_32(long posicao_19_32) {
		this.posicao_19_32 = posicao_19_32;
	}

	public void setPosicao_33_43(long posicao_33_43) {
		this.posicao_33_43 = posicao_33_43;
	}

	public void setPosicao_44_51(String posicao_44_51) {
		this.posicao_44_51 = posicao_44_51;
	}

	public void setPosicao_52_53(long posicao_52_53) {
		this.posicao_52_53 = posicao_52_53;
	}

	public void setPosicao_54_123(String posicao_54_123) {
		this.posicao_54_123 = posicao_54_123;
	}

	public void setPosicao_124_134(String posicao_124_134) {
		this.posicao_124_134 = posicao_124_134;
	}

	public void setPosicao_135_141(String posicao_135_141) {
		this.posicao_135_141 = posicao_135_141;
	}

	public void setPosicao_142_146(String posicao_142_146) {
		this.posicao_142_146 = posicao_142_146;
	}

	public void setPosicao_147_154(String posicao_147_154) {
		this.posicao_147_154 = posicao_147_154;
	}

	public void setPosicao_155_162(String posicao_155_162) {
		this.posicao_155_162 = posicao_155_162;
	}

	public void setPosicao_163_167(long posicao_163_167) {
		this.posicao_163_167 = posicao_163_167;
	}

	public void setPosicao_168_182(long posicao_168_182) {
		this.posicao_168_182 = posicao_168_182;
	}

	public void setPosicao_183_197(long posicao_183_197) {
		this.posicao_183_197 = posicao_183_197;
	}

	public void setPosicao_198_199(String posicao_198_199) {
		this.posicao_198_199 = posicao_198_199;
	}

	public void setPosicao_200_201(String posicao_200_201) {
		this.posicao_200_201 = posicao_200_201;
	}

	public void setPosicao_202_216(long posicao_202_216) {
		this.posicao_202_216 = posicao_202_216;
	}

	public void setPosicao_217_231(long posicao_217_231) {
		this.posicao_217_231 = posicao_217_231;
	}

	public void setPosicao_232_246(long posicao_232_246) {
		this.posicao_232_246 = posicao_232_246;
	}

	public void setPosicao_247_261(long posicao_247_261) {
		this.posicao_247_261 = posicao_247_261;
	}

	public void setPosicao_262_359(String posicao_262_359) {
		this.posicao_262_359 = posicao_262_359;
	}

	public void setPosicao_360_360(String posicao_360_360) {
		this.posicao_360_360 = posicao_360_360;
	}
}
