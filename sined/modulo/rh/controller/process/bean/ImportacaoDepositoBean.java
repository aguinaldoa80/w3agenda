package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;


public class ImportacaoDepositoBean {
	
	private Integer cdarquivofolha;
	private String matricula;
	private Colaborador colaborador;
	private String banco;
	private String agencia;
	private String conta;
	private Money valor;
	private Date dtdeposito;
	private Boolean marcado;
	
	public Integer getCdarquivofolha() {
		return cdarquivofolha;
	}
	public String getMatricula() {
		return matricula;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public String getAgencia() {
		return agencia;
	}
	public String getConta() {
		return conta;
	}
	public Money getValor() {
		return valor;
	}
	public Date getDtdeposito() {
		return dtdeposito;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public void setCdarquivofolha(Integer cdarquivofolha) {
		this.cdarquivofolha = cdarquivofolha;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDtdeposito(Date dtdeposito) {
		this.dtdeposito = dtdeposito;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	
}