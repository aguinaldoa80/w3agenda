package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;


public class ImportacaoEnvioBancarioBean {
	
	private String banco;
	private String nomebanco;
	private Date dtdeposito;
	private Money valor = new Money();
	private Boolean marcado = Boolean.TRUE;
	
	private List<Integer> listaCdarquivofolha = new ArrayList<Integer>();
	private String whereInArquivofolha;
	
	private List<ImportacaoEnvioBancarioRateioBean> listaRateio = new ArrayList<ImportacaoEnvioBancarioRateioBean>();
	
	public ImportacaoEnvioBancarioBean() {}
	
	public ImportacaoEnvioBancarioBean(String banco, Date dtdeposito) {
		this.banco = banco;
		this.dtdeposito = dtdeposito;
	}

	public Date getDtdeposito() {
		return dtdeposito;
	}
	public Money getValor() {
		return valor;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public String getBanco() {
		return banco;
	}
	public List<Integer> getListaCdarquivofolha() {
		return listaCdarquivofolha;
	}
	public String getWhereInArquivofolha() {
		return whereInArquivofolha;
	}
	public List<ImportacaoEnvioBancarioRateioBean> getListaRateio() {
		return listaRateio;
	}
	public String getNomebanco() {
		return nomebanco;
	}
	public void setNomebanco(String nomebanco) {
		this.nomebanco = nomebanco;
	}
	public void setListaRateio(List<ImportacaoEnvioBancarioRateioBean> listaRateio) {
		this.listaRateio = listaRateio;
	}
	public void setWhereInArquivofolha(String whereInArquivofolha) {
		this.whereInArquivofolha = whereInArquivofolha;
	}
	public void setListaCdarquivofolha(List<Integer> listaCdarquivofolha) {
		this.listaCdarquivofolha = listaCdarquivofolha;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public void setDtdeposito(Date dtdeposito) {
		this.dtdeposito = dtdeposito;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	
	public void addCdarquivofolha(Integer cdarquivofolha){
		if(this.listaCdarquivofolha == null) this.listaCdarquivofolha = new ArrayList<Integer>();
		if(cdarquivofolha != null && !this.listaCdarquivofolha.contains(cdarquivofolha)) this.listaCdarquivofolha.add(cdarquivofolha);
		this.makeWhereInCdarquivofolha();
	}
	
	public void addValor(Money valor){
		if(this.valor == null) this.valor = new Money();
		if(valor != null) this.valor = this.valor.add(valor);
	}
	
	public void addRateio(String projeto, Money valor){
		if(this.listaRateio == null) this.listaRateio = new ArrayList<ImportacaoEnvioBancarioRateioBean>();
		if(projeto != null && valor != null){
			ImportacaoEnvioBancarioRateioBean envioBancarioRateioBean = new ImportacaoEnvioBancarioRateioBean(projeto);
			
			if(this.listaRateio.contains(envioBancarioRateioBean)){
				envioBancarioRateioBean = this.listaRateio.get(this.listaRateio.indexOf(envioBancarioRateioBean));
				envioBancarioRateioBean.addValor(valor);
			} else {
				envioBancarioRateioBean.setValor(valor);
				this.listaRateio.add(envioBancarioRateioBean);
			}
		}
	}
	
	private void makeWhereInCdarquivofolha(){
		if(this.listaCdarquivofolha != null){
			this.whereInArquivofolha = CollectionsUtil.concatenate(this.listaCdarquivofolha, ",");
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((banco == null) ? 0 : banco.hashCode());
		result = prime * result
				+ ((dtdeposito == null) ? 0 : dtdeposito.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImportacaoEnvioBancarioBean other = (ImportacaoEnvioBancarioBean) obj;
		if (banco == null) {
			if (other.banco != null)
				return false;
		} else if (!banco.equals(other.banco))
			return false;
		if (dtdeposito == null) {
			if (other.dtdeposito != null)
				return false;
		} else if (!dtdeposito.equals(other.dtdeposito))
			return false;
		return true;
	}
	
}