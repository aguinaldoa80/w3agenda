package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Tipocargo;


public class ImportacaoSalarioBean {
	
	private Integer matricula;
	private Money salario;
	private Tipocargo tipocargo;
	
	public ImportacaoSalarioBean() {
	}
	
	public ImportacaoSalarioBean(Integer matricula) {
		this.matricula = matricula;
	}
	
	public Money getSalario() {
		return salario;
	}
	public Tipocargo getTipocargo() {
		return tipocargo;
	}
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public void setTipocargo(Tipocargo tipocargo) {
		this.tipocargo = tipocargo;
	}
	public void setSalario(Money salario) {
		this.salario = salario;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((matricula == null) ? 0 : matricula.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ImportacaoSalarioBean other = (ImportacaoSalarioBean) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		return true;
	}
	
	
	
}