package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Projeto;


public class ImportacaoArquivopontoItemBean {
	
	private Integer cdarquivoponto;
	private Date data;
	private String matricula;
	private Colaborador colaborador;
	private Hora horatotal;
	private Projeto projeto;
	private Boolean existe = Boolean.FALSE;
	private Boolean marcado;
	private Hora horaEntrada;
	private Hora horaSaida;
	
	public Date getData() {
		return data;
	}
	public String getMatricula() {
		return matricula;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Hora getHoratotal() {
		return horatotal;
	}
	public Boolean getExiste() {
		return existe;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public Integer getCdarquivoponto() {
		return cdarquivoponto;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Hora getHoraEntrada() {
		return horaEntrada;
	}
	public Hora getHoraSaida() {
		return horaSaida;
	}
	public void setHoraEntrada(Hora horaEntrada) {
		this.horaEntrada = horaEntrada;
	}
	public void setHoraSaida(Hora horaSaida) {
		this.horaSaida = horaSaida;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCdarquivoponto(Integer cdarquivoponto) {
		this.cdarquivoponto = cdarquivoponto;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setHoratotal(Hora horatotal) {
		this.horatotal = horatotal;
	}
	public void setExiste(Boolean existe) {
		this.existe = existe;
	}
	
}