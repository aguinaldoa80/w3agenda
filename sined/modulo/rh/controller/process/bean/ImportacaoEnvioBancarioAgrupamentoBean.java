package br.com.linkcom.sined.modulo.rh.controller.process.bean;



public class ImportacaoEnvioBancarioAgrupamentoBean {
	
	private Integer cdarquivofolha;
	private String banco;
	private String valor;
	private String dtdeposito;
	private String projeto;
	
	public ImportacaoEnvioBancarioAgrupamentoBean(Integer cdarquivofolha, String banco, String valor, String dtdeposito, String projeto) {
		this.cdarquivofolha = cdarquivofolha;
		this.banco = banco;
		this.valor = valor;
		this.dtdeposito = dtdeposito;
		this.projeto = projeto;
	}

	public Integer getCdarquivofolha() {
		return cdarquivofolha;
	}

	public String getBanco() {
		return banco;
	}

	public String getValor() {
		return valor;
	}

	public String getDtdeposito() {
		return dtdeposito;
	}

	public String getProjeto() {
		return projeto;
	}

	public void setCdarquivofolha(Integer cdarquivofolha) {
		this.cdarquivofolha = cdarquivofolha;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public void setDtdeposito(String dtdeposito) {
		this.dtdeposito = dtdeposito;
	}

	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	
	
}