package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.auxiliar.ColaboradorVO;


public class ImportacaoArquivopontoBean {
	
	private Boolean entrada;
	private String whereIn;
	private Projeto projeto;
	private List<ImportacaoArquivopontoItemBean> listaItens;
	private List<ColaboradorVO> listaColaboradorVO;
	
	public Boolean getEntrada() {
		return entrada;
	}
	public String getWhereIn() {
		return whereIn;
	}
	public List<ImportacaoArquivopontoItemBean> getListaItens() {
		return listaItens;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public List<ColaboradorVO> getListaColaboradorVO() {
		return listaColaboradorVO;
	}
	public void setListaColaboradorVO(List<ColaboradorVO> listaColaboradorVO) {
		this.listaColaboradorVO = listaColaboradorVO;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setListaItens(List<ImportacaoArquivopontoItemBean> listaItens) {
		this.listaItens = listaItens;
	}
	
}