package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cbo;
import br.com.linkcom.sined.geral.bean.Colaboradorformapagamento;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Colaboradortipo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Estadocivil;
import br.com.linkcom.sined.geral.bean.Etnia;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Regimecontratacao;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.CAGED;
import br.com.linkcom.sined.geral.bean.enumeration.Nacionalidade;



public class ImportarcolaboradoresFPWBean {
	
	//Colaborador
	private String nome;
	private String sexo;
	private String data_nascimento;
	private String estado_civil;
	private String codigo_grau_instrucao;
	private String nacionalidade;
	private String naturalidade;
	private String nome_mae;
	private String nome_pai;
	private String rg_numero;
	private String rg_data_emissao;
	private String rg_orgao_emissor;
	private String rg_uf;
	private String titulo_eleitor_numero;
	private String titulo_eleitor_secao;
	private String titulo_eleitor_zona;
	private String pis_pasep;
	private String cpf;
	private String etnia;
	private String codigo_tipo_empregado;
	
	//Endere�o
	private String logradouro;
	private String numero_endereco;
	private String bairro;
	private String uf;
	private String codigo_municipio;
	private String cep;
	
	//Telefone
	private String telefone;
	private String celular;
	
	//Colaboradorcargo
	private String nome_empresa;
	private String matricula;
	private String codigo_caged;
	private String data_admissao;
	private String nome_departamento;
	private Integer codigo_cargo;
	private String data_inicio_cargo;
	private String situacao_cargo;
	private String codigo_cbo;
	private String codigo_sindicato;
	private String codigo_projeto;
	private String regime_contratacao;
	
	//CTPS
	private String ctps_dataemissao;
	private String ctps_numero;
	private String ctps_serie;
	private String ctps_uf;
	
	//Conta banc�ria
	private String forma_pagamento;
	private String conta_agencia;
	private String conta_operacao;
	private String conta_numero; 
	private String codigo_banco; 
	
	//Objetos
	private Sexo sexoObj;
	private Date dtnascimento;
	private Estadocivil estadocivil;
	private Grauinstrucao grauinstrucao;
	private Nacionalidade nacionalidadeObj;
	private Municipio municipio;
	private Uf ufObj;
	private Municipio municipionaturalidade;	
	private Cep cepObj;
	private CAGED caged;
	private Date dtadmissao;
	private Date dtinicio_cargo;
	private Departamento departamento;
	private Cargo cargo;
	private Cbo cbo;
	private Sindicato sindicato;
	private Date rgdtemissao;
	private Uf rguf;
	private Cpf cpfObj;
	private Projeto projeto;
	private Date ctpsdtemissao;
	private Uf ctpsuf;
	private Banco banco;
	private Empresa empresa;
	private Colaboradorsituacao colaboradorsituacao;
	private Colaboradorformapagamento colaboradorformapagamento;
	private Etnia etniaObj;
	private Colaboradortipo colaboradortipoObj;
	private Regimecontratacao regimecontratacaoObj; 
	
	
	
	public String getMatricula() {
		return matricula;
	}
	public String getNome() {
		return nome;
	}
	public String getSexo() {
		return sexo;
	}
	public String getData_nascimento() {
		return data_nascimento;
	}
	public String getEstado_civil() {
		return estado_civil;
	}
	public String getCodigo_grau_instrucao() {
		return codigo_grau_instrucao;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public String getNaturalidade() {
		return naturalidade;
	}
	public Municipio getMunicipionaturalidade() {
		return municipionaturalidade;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero_endereco() {
		return numero_endereco;
	}
	public String getBairro() {
		return bairro;
	}
	public String getUf() {
		return uf;
	}
	public String getCodigo_municipio() {
		return codigo_municipio;
	}
	public String getCep() {
		return cep;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getCelular() {
		return celular;
	}
	public String getNome_empresa() {
		return nome_empresa;
	}
	public String getNome_mae() {
		return nome_mae;
	}
	public String getCodigo_caged() {
		return codigo_caged;
	}
	public String getData_admissao() {
		return data_admissao;
	}
	public Date getDtinicio_cargo() {
		return dtinicio_cargo;
	}
	public String getNome_departamento() {
		return nome_departamento;
	}
	public Integer getCodigo_cargo() {
		return codigo_cargo;
	}
	public String getData_inicio_cargo() {
		return data_inicio_cargo;
	}
	public String getSituacao_cargo() {
		return situacao_cargo;
	}
	public String getCodigo_cbo() {
		return codigo_cbo;
	}
	public String getCodigo_sindicato() {
		return codigo_sindicato;
	}
	public String getRg_numero() {
		return rg_numero;
	}
	public String getRg_data_emissao() {
		return rg_data_emissao;
	}
	public String getRg_orgao_emissor() {
		return rg_orgao_emissor;
	}
	public String getRg_uf() {
		return rg_uf;
	}
	public String getTitulo_eleitor_numero() {
		return titulo_eleitor_numero;
	}
	public String getTitulo_eleitor_secao() {
		return titulo_eleitor_secao;
	}
	public String getTitulo_eleitor_zona() {
		return titulo_eleitor_zona;
	}
	public String getPis_pasep() {
		return pis_pasep;
	}
	public String getCpf() {
		return cpf;
	}
	public String getEtnia() {
		return etnia;
	}
	public String getCodigo_tipo_empregado() {
		return codigo_tipo_empregado;
	}
	public String getNome_pai() {
		return nome_pai;
	}
	public Sexo getSexoObj() {
		return sexoObj;
	}
	public Date getDtnascimento() {
		return dtnascimento;
	}
	public Estadocivil getEstadocivil() {
		return estadocivil;
	}
	public Grauinstrucao getGrauinstrucao() {
		return grauinstrucao;
	}
	public Nacionalidade getNacionalidadeObj() {
		return nacionalidadeObj;
	}
	public Municipio getMunicipio() {
		return municipio;
	}
	public Uf getUfObj() {
		return ufObj;
	}
	public Cep getCepObj() {
		return cepObj;
	}
	public CAGED getCaged() {
		return caged;
	}
	public Date getDtadmissao() {
		return dtadmissao;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public Cbo getCbo() {
		return cbo;
	}
	public Sindicato getSindicato() {
		return sindicato;
	}
	public Date getRgdtemissao() {
		return rgdtemissao;
	}
	public Uf getRguf() {
		return rguf;
	}
	public Cpf getCpfObj() {
		return cpfObj;
	}
	public String getCodigo_projeto() {
		return codigo_projeto;
	}
	public String getRegime_contratacao() {
		return regime_contratacao;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public String getCtps_dataemissao() {
		return ctps_dataemissao;
	}
	public String getCtps_numero() {
		return ctps_numero;
	}
	public String getCtps_serie() {
		return ctps_serie;
	}
	public String getCtps_uf() {
		return ctps_uf;
	}
	public String getForma_pagamento() {
		return forma_pagamento;
	}
	public String getConta_agencia() {
		return conta_agencia;
	}
	public String getConta_operacao() {
		return conta_operacao;
	}
	public String getConta_numero() {
		return conta_numero;
	}
	public String getCodigo_banco() {
		return codigo_banco;
	}
	public Date getCtpsdtemissao() {
		return ctpsdtemissao;
	}
	public Uf getCtpsuf() {
		return ctpsuf;
	}
	public Banco getBanco() {
		return banco;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Colaboradorsituacao getColaboradorsituacao() {
		return colaboradorsituacao;
	}
	public Colaboradorformapagamento getColaboradorformapagamento() {
		return colaboradorformapagamento;
	}
	
	//Setters
	
	public void setCtps_dataemissao(String ctpsDataemissao) {
		ctps_dataemissao = ctpsDataemissao;
	}
	public void setCtps_numero(String ctpsNumero) {
		ctps_numero = ctpsNumero;
	}
	public void setCtps_serie(String ctpsSerie) {
		ctps_serie = ctpsSerie;
	}
	public void setCtps_uf(String ctpsUf) {
		ctps_uf = ctpsUf;
	}
	public void setConta_agencia(String contaAgencia) {
		conta_agencia = contaAgencia;
	}
	public void setConta_operacao(String conta_operacao) {
		this.conta_operacao = conta_operacao;
	}
	public void setConta_numero(String contaNumero) {
		conta_numero = contaNumero;
	}
	public void setCodigo_banco(String codigo_banco) {
		this.codigo_banco = codigo_banco;
	}
	public void setCtpsdtemissao(Date ctpsdtemissao) {
		this.ctpsdtemissao = ctpsdtemissao;
	}
	public void setCtpsuf(Uf ctpsuf) {
		this.ctpsuf = ctpsuf;
	}
	public void setForma_pagamento(String forma_pagamento) {
		this.forma_pagamento = forma_pagamento;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setCodigo_projeto(String codigo_projeto) {
		this.codigo_projeto = codigo_projeto;
	}
	public void setRegime_contratacao(String regime_contratacao) {
		this.regime_contratacao = regime_contratacao;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setSexoObj(Sexo sexoObj) {
		this.sexoObj = sexoObj;
	}
	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	public void setEstadocivil(Estadocivil estadocivil) {
		this.estadocivil = estadocivil;
	}
	public void setGrauinstrucao(Grauinstrucao grauinstrucao) {
		this.grauinstrucao = grauinstrucao;
	}
	public void setMunicipionaturalidade(Municipio municipionaturalidade) {
		this.municipionaturalidade = municipionaturalidade;
	}
	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}
	public void setNacionalidadeObj(Nacionalidade nacionalidadeObj) {
		this.nacionalidadeObj = nacionalidadeObj;
	}
	public void setUfObj(Uf ufObj) {
		this.ufObj = ufObj;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setCepObj(Cep cepObj) {
		this.cepObj = cepObj;
	}
	public void setCaged(CAGED caged) {
		this.caged = caged;
	}
	public void setDtadmissao(Date dtadmissao) {
		this.dtadmissao = dtadmissao;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setData_inicio_cargo(String data_inicio_cargo) {
		this.data_inicio_cargo = data_inicio_cargo;
	}
	public void setSituacao_cargo(String situacao_cargo) {
		this.situacao_cargo = situacao_cargo;
	}
	public void setCbo(Cbo cbo) {
		this.cbo = cbo;
	}
	public void setSindicato(Sindicato sindicato) {
		this.sindicato = sindicato;
	}
	public void setRgdtemissao(Date rgdtemissao) {
		this.rgdtemissao = rgdtemissao;
	}
	public void setRguf(Uf rguf) {
		this.rguf = rguf;
	}
	public void setCpfObj(Cpf cpfObj) {
		this.cpfObj = cpfObj;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public void setData_nascimento(String data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	public void setEstado_civil(String estado_civil) {
		this.estado_civil = estado_civil;
	}
	public void setCodigo_grau_instrucao(String codigo_grau_instrucao) {
		this.codigo_grau_instrucao = codigo_grau_instrucao;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero_endereco(String numero_endereco) {
		this.numero_endereco = numero_endereco;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCodigo_municipio(String codigo_municipio) {
		this.codigo_municipio = codigo_municipio;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setNome_empresa(String nome_empresa) {
		this.nome_empresa = nome_empresa;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public void setNome_mae(String nome_mae) {
		this.nome_mae = nome_mae;
	}
	public void setCodigo_caged(String codigo_caged) {
		this.codigo_caged = codigo_caged;
	}
	public void setDtinicio_cargo(Date dtinicio_cargo) {
		this.dtinicio_cargo = dtinicio_cargo;
	}
	public void setData_admissao(String data_admissao) {
		this.data_admissao = data_admissao;
	}
	public void setNome_departamento(String nome_departamento) {
		this.nome_departamento = nome_departamento;
	}
	public void setCodigo_cargo(Integer codigo_cargo) {
		this.codigo_cargo = codigo_cargo;
	}
	public void setCodigo_cbo(String codigo_cbo) {
		this.codigo_cbo = codigo_cbo;
	}
	public void setCodigo_sindicato(String codigo_sindicato) {
		this.codigo_sindicato = codigo_sindicato;
	}
	public void setRg_numero(String rg_numero) {
		this.rg_numero = rg_numero;
	}
	public void setRg_data_emissao(String rg_data_emissao) {
		this.rg_data_emissao = rg_data_emissao;
	}
	public void setRg_orgao_emissor(String rg_orgao_emissor) {
		this.rg_orgao_emissor = rg_orgao_emissor;
	}
	public void setRg_uf(String rg_uf) {
		this.rg_uf = rg_uf;
	}
	public void setTitulo_eleitor_numero(String titulo_eleitor_numero) {
		this.titulo_eleitor_numero = titulo_eleitor_numero;
	}
	public void setTitulo_eleitor_secao(String titulo_eleitor_secao) {
		this.titulo_eleitor_secao = titulo_eleitor_secao;
	}
	public void setTitulo_eleitor_zona(String titulo_eleitor_zona) {
		this.titulo_eleitor_zona = titulo_eleitor_zona;
	}
	public void setPis_pasep(String pis_pasep) {
		this.pis_pasep = pis_pasep;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setEtnia(String etnia) {
		this.etnia = etnia;
	}
	public void setCodigo_tipo_empregado(String codigo_tipo_empregado) {
		this.codigo_tipo_empregado = codigo_tipo_empregado;
	}
	public void setNome_pai(String nome_pai) {
		this.nome_pai = nome_pai;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaboradorsituacao(Colaboradorsituacao colaboradorsituacao) {
		this.colaboradorsituacao = colaboradorsituacao;
	}
	public void setColaboradorformapagamento(
			Colaboradorformapagamento colaboradorformapagamento) {
		this.colaboradorformapagamento = colaboradorformapagamento;
	}
	public Etnia getEtniaObj() {
		return etniaObj;
	}
	public void setEtniaObj(Etnia etniaObj) {
		this.etniaObj = etniaObj;
	}
	public Colaboradortipo getColaboradortipoObj() {
		return colaboradortipoObj;
	}
	public void setColaboradortipoObj(Colaboradortipo colaboradortipoObj) {
		this.colaboradortipoObj = colaboradortipoObj;
	}
	public Regimecontratacao getRegimecontratacaoObj() {
		return regimecontratacaoObj;
	}
	public void setRegimecontratacaoObj(Regimecontratacao regimecontratacaoObj) {
		this.regimecontratacaoObj = regimecontratacaoObj;
	}
}
