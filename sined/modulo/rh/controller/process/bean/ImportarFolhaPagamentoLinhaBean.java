package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;


//Classe que representa as informações de uma linha do arquivo
public class ImportarFolhaPagamentoLinhaBean {
	
	private Integer processamento;
	private Integer codigoCentroCusto;
	private String nome;
	private Cnpj cnpj;
	private Cpf cpf;
	private Long salario;
	
	public Integer getProcessamento() {
		return processamento;
	}
	public Integer getCodigoCentroCusto() {
		return codigoCentroCusto;
	}
	public String getNome() {
		return nome;
	}
	public Cnpj getCnpj() {
		return cnpj;
	}
	public Cpf getCpf() {
		return cpf;
	}
	public Long getSalario() {
		return salario;
	}
	
	public void setProcessamento(Integer processamento) {
		this.processamento = processamento;
	}
	public void setCodigoCentroCusto(Integer codigoCentroCusto) {
		this.codigoCentroCusto = codigoCentroCusto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setSalario(Long salario) {
		this.salario = salario;
	}	
}