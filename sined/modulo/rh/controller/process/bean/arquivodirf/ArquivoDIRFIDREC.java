package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf;

import java.util.List;

import br.com.linkcom.sined.util.SinedUtil;

public class ArquivoDIRFIDREC {

	private final String ID_REG = "IDREC";
	
	private String codigoReceita;
	
	private List<ArquivoDIRFBPFDEC> listaArquivoDIRFBPFDEC;

	public String getID_REG() {
		return ID_REG;
	}
	public String getCodigoReceita() {
		return codigoReceita;
	}
	public List<ArquivoDIRFBPFDEC> getListaArquivoDIRFBPFDEC() {
		return listaArquivoDIRFBPFDEC;
	}
	public void setCodigoReceita(String codigoReceita) {
		this.codigoReceita = codigoReceita;
	}
	public void setListaArquivoDIRFBPFDEC(
			List<ArquivoDIRFBPFDEC> listaArquivoDIRFBPFDEC) {
		this.listaArquivoDIRFBPFDEC = listaArquivoDIRFBPFDEC;
	}
	
	@Override
	public String toString() {
		return ID_REG + "|" +
				codigoReceita + "|" + "\n" +
				
				SinedUtil.imprimeListaRegistroDIRF(listaArquivoDIRFBPFDEC)
				;
	}
	
	
}
