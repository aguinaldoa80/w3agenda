package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.util.SinedUtil;

public class ArquivoDIRFDECPJ {

	private final String ID_REG = "DECPJ";
	
	private String cnpj;
	private String nomeEmpresarial;
	private Integer naturezaDeclarante;
	private String cpfResponsavel;
	private String indicadorSocioOstensivo;
	private String indicadorDeclaranteDepositario;
	private String indicadorDeclaranteInstituicaoAdministradora;
	private String indicadorDeclaranteRedimentosPagosResidentes;
	private String indicadorPlanoPrivadoAssistenciaSaude;
	private String indicadorPagamentosCopa;
	private String indicadorSituacaoEspecial;
	private Date dataevento;
	
	private List<ArquivoDIRFIDREC> listaArquivoDIRFIDREC;
	
	public String getID_REG() {
		return ID_REG;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getNomeEmpresarial() {
		return nomeEmpresarial;
	}
	public Integer getNaturezaDeclarante() {
		return naturezaDeclarante;
	}
	public String getCpfResponsavel() {
		return cpfResponsavel;
	}
	public String getIndicadorSocioOstensivo() {
		return indicadorSocioOstensivo;
	}
	public String getIndicadorDeclaranteDepositario() {
		return indicadorDeclaranteDepositario;
	}
	public String getIndicadorDeclaranteInstituicaoAdministradora() {
		return indicadorDeclaranteInstituicaoAdministradora;
	}
	public String getIndicadorDeclaranteRedimentosPagosResidentes() {
		return indicadorDeclaranteRedimentosPagosResidentes;
	}
	public String getIndicadorPlanoPrivadoAssistenciaSaude() {
		return indicadorPlanoPrivadoAssistenciaSaude;
	}
	public String getIndicadorPagamentosCopa() {
		return indicadorPagamentosCopa;
	}
	public String getIndicadorSituacaoEspecial() {
		return indicadorSituacaoEspecial;
	}
	public Date getDataevento() {
		return dataevento;
	}
	public List<ArquivoDIRFIDREC> getListaArquivoDIRFIDREC() {
		return listaArquivoDIRFIDREC;
	}
	public void setListaArquivoDIRFIDREC(
			List<ArquivoDIRFIDREC> listaArquivoDIRFIDREC) {
		this.listaArquivoDIRFIDREC = listaArquivoDIRFIDREC;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setNomeEmpresarial(String nomeEmpresarial) {
		this.nomeEmpresarial = nomeEmpresarial;
	}
	public void setNaturezaDeclarante(Integer naturezaDeclarante) {
		this.naturezaDeclarante = naturezaDeclarante;
	}
	public void setCpfResponsavel(String cpfResponsavel) {
		this.cpfResponsavel = cpfResponsavel;
	}
	public void setIndicadorSocioOstensivo(String indicadorSocioOstensivo) {
		this.indicadorSocioOstensivo = indicadorSocioOstensivo;
	}
	public void setIndicadorDeclaranteDepositario(
			String indicadorDeclaranteDepositario) {
		this.indicadorDeclaranteDepositario = indicadorDeclaranteDepositario;
	}
	public void setIndicadorDeclaranteInstituicaoAdministradora(
			String indicadorDeclaranteInstituicaoAdministradora) {
		this.indicadorDeclaranteInstituicaoAdministradora = indicadorDeclaranteInstituicaoAdministradora;
	}
	public void setIndicadorDeclaranteRedimentosPagosResidentes(
			String indicadorDeclaranteRedimentosPagosResidentes) {
		this.indicadorDeclaranteRedimentosPagosResidentes = indicadorDeclaranteRedimentosPagosResidentes;
	}
	public void setIndicadorPlanoPrivadoAssistenciaSaude(
			String indicadorPlanoPrivadoAssistenciaSaude) {
		this.indicadorPlanoPrivadoAssistenciaSaude = indicadorPlanoPrivadoAssistenciaSaude;
	}
	public void setIndicadorPagamentosCopa(String indicadorPagamentosCopa) {
		this.indicadorPagamentosCopa = indicadorPagamentosCopa;
	}
	public void setIndicadorSituacaoEspecial(String indicadorSituacaoEspecial) {
		this.indicadorSituacaoEspecial = indicadorSituacaoEspecial;
	}
	public void setDataevento(Date dataevento) {
		this.dataevento = dataevento;
	}
	
	@Override
	public String toString() {
		return ID_REG + "|" +
				cnpj + "|" +
				nomeEmpresarial + "|" +
				naturezaDeclarante + "|" +
				cpfResponsavel + "|" +
				indicadorSocioOstensivo + "|" +
				indicadorDeclaranteDepositario + "|" +
				indicadorDeclaranteInstituicaoAdministradora + "|" +
				indicadorDeclaranteRedimentosPagosResidentes + "|" +
				indicadorPlanoPrivadoAssistenciaSaude + "|" +
				indicadorPagamentosCopa + "|" +
				indicadorSituacaoEspecial + "|" +
				SinedUtil.imprimeDataDIRF(dataevento) + "|" + "\n" +
				
				SinedUtil.imprimeListaRegistroDIRF(listaArquivoDIRFIDREC)
				;
	}
	
}
