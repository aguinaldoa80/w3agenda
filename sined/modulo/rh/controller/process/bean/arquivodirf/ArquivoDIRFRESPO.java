package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf;

import br.com.linkcom.neo.util.Util;

public class ArquivoDIRFRESPO {

	private final String ID_REG = "RESPO";
	
	private String cpf;
	private String nome;
	private String ddd;
	private String telefone;
	private String ramal;
	private String fax;
	private String correioEletronico;
	
	public String getID_REG() {
		return ID_REG;
	}
	public String getCpf() {
		return cpf;
	}
	public String getNome() {
		return nome;
	}
	public String getDdd() {
		return ddd;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getRamal() {
		return ramal;
	}
	public String getFax() {
		return fax;
	}
	public String getCorreioEletronico() {
		return correioEletronico;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public void setCorreioEletronico(String correioEletronico) {
		this.correioEletronico = correioEletronico;
	}
	
	@Override
	public String toString() {
		return ID_REG + "|" +
				cpf + "|" +
				nome + "|" +
				ddd + "|" +
				telefone + "|" +
				Util.strings.emptyIfNull(ramal) + "|" +
				Util.strings.emptyIfNull(fax) + "|" +
				correioEletronico + "|" + "\n"
				;
	}
	
}
