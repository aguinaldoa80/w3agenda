package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.util.SinedUtil;

public class ArquivoDIRF {
	
	private final String ID_REG = "DIRF";
	
	private Integer anoreferencia;
	private Integer anocalendario;
	private String indicadorRetificadora;
	private String numeroRecibo;
	private String identificadorEstruturaLeiaute;
	
	private ArquivoDIRFRESPO arquivoDIRFRESPO;
	private ArquivoDIRFDECPJ arquivoDIRFDECPJ;
	
	public String getID_REG() {
		return ID_REG;
	}
	public Integer getAnoreferencia() {
		return anoreferencia;
	}
	public Integer getAnocalendario() {
		return anocalendario;
	}
	public String getIndicadorRetificadora() {
		return indicadorRetificadora;
	}
	public String getNumeroRecibo() {
		return numeroRecibo;
	}
	public String getIdentificadorEstruturaLeiaute() {
		return identificadorEstruturaLeiaute;
	}
	public ArquivoDIRFDECPJ getArquivoDIRFDECPJ() {
		return arquivoDIRFDECPJ;
	}
	public ArquivoDIRFRESPO getArquivoDIRFRESPO() {
		return arquivoDIRFRESPO;
	}
	public void setAnoreferencia(Integer anoreferencia) {
		this.anoreferencia = anoreferencia;
	}
	public void setAnocalendario(Integer anocalendario) {
		this.anocalendario = anocalendario;
	}
	public void setIndicadorRetificadora(String indicadorRetificadora) {
		this.indicadorRetificadora = indicadorRetificadora;
	}
	public void setNumeroRecibo(String numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}
	public void setIdentificadorEstruturaLeiaute(
			String identificadorEstruturaLeiaute) {
		this.identificadorEstruturaLeiaute = identificadorEstruturaLeiaute;
	}
	public void setArquivoDIRFDECPJ(ArquivoDIRFDECPJ arquivoDIRFDECPJ) {
		this.arquivoDIRFDECPJ = arquivoDIRFDECPJ;
	}
	public void setArquivoDIRFRESPO(ArquivoDIRFRESPO arquivoDIRFRESPO) {
		this.arquivoDIRFRESPO = arquivoDIRFRESPO;
	}
	
	@Override
	public String toString() {
		return ID_REG + "|" +
			anoreferencia + "|" +
			anocalendario + "|" +
			indicadorRetificadora + "|" +
			Util.strings.emptyIfNull(numeroRecibo) + "|" +
			identificadorEstruturaLeiaute + "|" + "\n" +
			
			SinedUtil.imprimeRegistroDIRF(arquivoDIRFRESPO) +
			SinedUtil.imprimeRegistroDIRF(arquivoDIRFDECPJ) +
			"FIMDIRF|" 
			;
	}
	
	

}
