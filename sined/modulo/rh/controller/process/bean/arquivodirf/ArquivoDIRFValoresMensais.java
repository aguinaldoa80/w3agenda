package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf;

import br.com.linkcom.sined.util.SinedUtil;

public class ArquivoDIRFValoresMensais {

	private String identificadorRegistro;
	private Long janeiro;
	private Long fevereiro;
	private Long marco;
	private Long abril;
	private Long maio;
	private Long junho;
	private Long julho;
	private Long agosto;
	private Long setembro;
	private Long outubro;
	private Long novembro;
	private Long dezembro;
	private Long decimoTerceiro;
	
	public String getIdentificadorRegistro() {
		return identificadorRegistro;
	}
	public Long getJaneiro() {
		return janeiro;
	}
	public Long getFevereiro() {
		return fevereiro;
	}
	public Long getMarco() {
		return marco;
	}
	public Long getAbril() {
		return abril;
	}
	public Long getMaio() {
		return maio;
	}
	public Long getJunho() {
		return junho;
	}
	public Long getJulho() {
		return julho;
	}
	public Long getAgosto() {
		return agosto;
	}
	public Long getSetembro() {
		return setembro;
	}
	public Long getOutubro() {
		return outubro;
	}
	public Long getNovembro() {
		return novembro;
	}
	public Long getDezembro() {
		return dezembro;
	}
	public Long getDecimoTerceiro() {
		return decimoTerceiro;
	}
	public void setIdentificadorRegistro(String identificadorRegistro) {
		this.identificadorRegistro = identificadorRegistro;
	}
	public void setJaneiro(Long janeiro) {
		this.janeiro = janeiro;
	}
	public void setFevereiro(Long fevereiro) {
		this.fevereiro = fevereiro;
	}
	public void setMarco(Long marco) {
		this.marco = marco;
	}
	public void setAbril(Long abril) {
		this.abril = abril;
	}
	public void setMaio(Long maio) {
		this.maio = maio;
	}
	public void setJunho(Long junho) {
		this.junho = junho;
	}
	public void setJulho(Long julho) {
		this.julho = julho;
	}
	public void setAgosto(Long agosto) {
		this.agosto = agosto;
	}
	public void setSetembro(Long setembro) {
		this.setembro = setembro;
	}
	public void setOutubro(Long outubro) {
		this.outubro = outubro;
	}
	public void setNovembro(Long novembro) {
		this.novembro = novembro;
	}
	public void setDezembro(Long dezembro) {
		this.dezembro = dezembro;
	}
	public void setDecimoTerceiro(Long decimoTerceiro) {
		this.decimoTerceiro = decimoTerceiro;
	}
	
	@Override
	public String toString() {
		return identificadorRegistro + "|" + 
				SinedUtil.imprimeLongDIRF(janeiro) + "|" + 
				SinedUtil.imprimeLongDIRF(fevereiro) + "|" + 
				SinedUtil.imprimeLongDIRF(marco) + "|" + 
				SinedUtil.imprimeLongDIRF(abril) + "|" + 
				SinedUtil.imprimeLongDIRF(maio) + "|" + 
				SinedUtil.imprimeLongDIRF(junho) + "|" + 
				SinedUtil.imprimeLongDIRF(julho) + "|" + 
				SinedUtil.imprimeLongDIRF(agosto) + "|" + 
				SinedUtil.imprimeLongDIRF(setembro) + "|" + 
				SinedUtil.imprimeLongDIRF(outubro) + "|" + 
				SinedUtil.imprimeLongDIRF(novembro) + "|" + 
				SinedUtil.imprimeLongDIRF(dezembro) + "|" + 
				SinedUtil.imprimeLongDIRF(decimoTerceiro)  + "|" + "\n";
	}
}
