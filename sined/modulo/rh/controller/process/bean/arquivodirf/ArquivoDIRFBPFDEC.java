package br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf;

import java.sql.Date;

import br.com.linkcom.sined.util.SinedUtil;

public class ArquivoDIRFBPFDEC {

	private final String ID_REG = "BPFDEC";
	
	private String cpf;
	private String nome;
	private Date dataLaudoMolestiaGrave;
	
	private ArquivoDIRFValoresMensais arquivoDIRFRTRT;
	private ArquivoDIRFValoresMensais arquivoDIRFRTPO;
	private ArquivoDIRFValoresMensais arquivoDIRFRTIRF;
	
	public String getID_REG() {
		return ID_REG;
	}
	public String getCpf() {
		return cpf;
	}
	public String getNome() {
		return nome;
	}
	public Date getDataLaudoMolestiaGrave() {
		return dataLaudoMolestiaGrave;
	}
	public ArquivoDIRFValoresMensais getArquivoDIRFRTRT() {
		return arquivoDIRFRTRT;
	}
	public ArquivoDIRFValoresMensais getArquivoDIRFRTPO() {
		return arquivoDIRFRTPO;
	}
	public ArquivoDIRFValoresMensais getArquivoDIRFRTIRF() {
		return arquivoDIRFRTIRF;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDataLaudoMolestiaGrave(Date dataLaudoMolestiaGrave) {
		this.dataLaudoMolestiaGrave = dataLaudoMolestiaGrave;
	}
	public void setArquivoDIRFRTRT(ArquivoDIRFValoresMensais arquivoDIRFRTRT) {
		this.arquivoDIRFRTRT = arquivoDIRFRTRT;
	}
	public void setArquivoDIRFRTPO(ArquivoDIRFValoresMensais arquivoDIRFRTPO) {
		this.arquivoDIRFRTPO = arquivoDIRFRTPO;
	}
	public void setArquivoDIRFRTIRF(ArquivoDIRFValoresMensais arquivoDIRFRTIRF) {
		this.arquivoDIRFRTIRF = arquivoDIRFRTIRF;
	}
	
	@Override
	public String toString() {
		return ID_REG + "|" +
				cpf + "|" +
				nome + "|" +
				SinedUtil.imprimeDataDIRF(dataLaudoMolestiaGrave) + "|" + "\n" +
				
				SinedUtil.imprimeRegistroDIRF(arquivoDIRFRTRT) +
				SinedUtil.imprimeRegistroDIRF(arquivoDIRFRTPO) +
				SinedUtil.imprimeRegistroDIRF(arquivoDIRFRTIRF)
				;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArquivoDIRFBPFDEC other = (ArquivoDIRFBPFDEC) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	
}
