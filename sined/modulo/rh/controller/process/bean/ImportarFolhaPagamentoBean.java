package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;

//Classe que representa as informa��es da Conta a pagar que ser� criada
public class ImportarFolhaPagamentoBean {
	
	private ImportarFolhaPagamentoLinhaBean linhaBean;
	private Empresa empresa;
	private Colaborador colaborador;
	private String descricao;
	private Money valor;
	private Contagerencial contagerencial;
	private Centrocusto centrocusto;
	
	public ImportarFolhaPagamentoLinhaBean getLinhaBean() {
		return linhaBean;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public String getDescricao() {
		return descricao;
	}
	public Money getValor() {
		return valor;
	}
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	public void setLinhaBean(ImportarFolhaPagamentoLinhaBean linhaBean) {
		this.linhaBean = linhaBean;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}	
}