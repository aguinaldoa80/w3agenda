package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;


public class ImportacaoFolhaPagamentoBeanErro {

	private String cnpj;
	private String matricula;
	private String dtReferencia;
	private String dtPagamento;
	private String evento;
	private String valor;
	private String erro;
	public String getCnpj() {
		return cnpj;
	}
	@DisplayName("Matr�cula")
	public String getMatricula() {
		return matricula;
	}
	@DisplayName("Data de refer�ncia")
	public String getDtReferencia() {
		return dtReferencia;
	}
	@DisplayName("Data de pagamento")
	public String getDtPagamento() {
		return dtPagamento;
	}
	public String getEvento() {
		return evento;
	}
	public String getValor() {
		return valor;
	}
	public String getErro() {
		return erro;
	}
	public void setCnpj(String empresa) {
		this.cnpj = empresa;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public void setDtReferencia(String dtReferencia) {
		this.dtReferencia = dtReferencia;
	}
	public void setDtPagamento(String dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	
	
}
