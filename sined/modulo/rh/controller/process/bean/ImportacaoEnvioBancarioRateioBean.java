package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Projeto;


public class ImportacaoEnvioBancarioRateioBean {
	
	private String siglaprojeto;
	private Projeto projeto;
	private Centrocusto centrocusto;
	private Contagerencial contagerencial;
	private Money valor = new Money();
	
	public ImportacaoEnvioBancarioRateioBean() {}
	
	public ImportacaoEnvioBancarioRateioBean(String siglaprojeto) {
		this.siglaprojeto = siglaprojeto;
	}

	public String getSiglaprojeto() {
		return siglaprojeto;
	}
	public Money getValor() {
		return valor;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setSiglaprojeto(String siglaprojeto) {
		this.siglaprojeto = siglaprojeto;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void addValor(Money valor){
		if(this.valor == null) this.valor = new Money();
		if(valor != null) this.valor = this.valor.add(valor);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((siglaprojeto == null) ? 0 : siglaprojeto.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImportacaoEnvioBancarioRateioBean other = (ImportacaoEnvioBancarioRateioBean) obj;
		if (siglaprojeto == null) {
			if (other.siglaprojeto != null)
				return false;
		} else if (!siglaprojeto.equals(other.siglaprojeto))
			return false;
		return true;
	}
	
}