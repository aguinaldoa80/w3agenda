package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.geral.bean.Projeto;

public class ImportacaoFolhaPagamentoBean {

	private Empresa empresa;
	private Colaboradordespesamotivo tipoRemuneracao;
	private Colaborador colaborador;
	private Date dtReferencia;
	private Date dtPagamento;
	private EventoPagamento evento;
	private Money valor;
	private String erro;
	private Contagerencial contaGerencial;
	private Projeto projeto;
	private Centrocusto centroCusto;
	
	
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Colaboradordespesamotivo getTipoRemuneracao() {
		return tipoRemuneracao;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Data de referÍncia")
	public Date getDtReferencia() {
		return dtReferencia;
	}
	@DisplayName("Data de pagamento")
	public Date getDtPagamento() {
		return dtPagamento;
	}
	public EventoPagamento getEvento() {
		return evento;
	}
	public Money getValor() {
		return valor;
	}
	public String getErro() {
		return erro;
	}
	public Contagerencial getContaGerencial() {
		return contaGerencial;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}
	
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setTipoRemuneracao(Colaboradordespesamotivo tipoRemuneracao) {
		this.tipoRemuneracao = tipoRemuneracao;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtReferencia(Date dtReferencia) {
		this.dtReferencia = dtReferencia;
	}
	public void setDtPagamento(Date dtPagamento) {
		this.dtPagamento = dtPagamento;
	}
	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public void setContaGerencial(Contagerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}
}
