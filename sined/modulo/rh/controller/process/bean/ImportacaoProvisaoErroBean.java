package br.com.linkcom.sined.modulo.rh.controller.process.bean;

public class ImportacaoProvisaoErroBean {

	private String cnpj;
	private String matricula;
	private String data;
	private String erro;
	private String evento;
	
	
	public String getCnpj() {
		return cnpj;
	}
	public String getMatricula() {
		return matricula;
	}
	public String getData() {
		return data;
	}
	public String getErro() {
		return erro;
	}
	public String getEvento() {
		return evento;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public void setData(String data) {
		this.data = data;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
}
