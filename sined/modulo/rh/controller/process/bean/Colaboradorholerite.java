package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class Colaboradorholerite{
	
	protected String nome;
	protected Date dtadmissaoColaborador;
	protected Double salarioColaborador;
	protected Integer cdpessoa;
	protected Boolean gerado;
	protected Boolean parcial;
	
	protected Money salarioColaboradorMoeda;
	
	public String getNome() {
		return nome;
	}
	public Double getSalarioColaborador() {
		return salarioColaborador;
	}
	public Boolean getGerado() {
		return gerado;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public Date getDtadmissaoColaborador() {
		return dtadmissaoColaborador;
	}
	public Boolean getParcial() {
		return parcial;
	}
	public void setParcial(Boolean parcial) {
		this.parcial = parcial;
	}
	public void setDtadmissaoColaborador(Date dtadmissaoColaborador) {
		this.dtadmissaoColaborador = dtadmissaoColaborador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSalarioColaborador(Double salarioColaborador) {
		this.salarioColaborador = salarioColaborador;
	}
	public void setGerado(Boolean gerado) {
		this.gerado = gerado;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	
	public Money getSalarioColaboradorMoeda() {
		if(salarioColaborador != null && salarioColaborador != 0){
			return new Money(salarioColaborador/100d);
		}else
			return new Money(0.0);
	}
	
}
