package br.com.linkcom.sined.modulo.rh.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;


public class ImportacaoArquivofolhaBean {
	
	private Boolean entrada;
	private String whereIn;
	private List<ImportacaoEnvioBancarioBean> listaEnviobancario;
	private List<ImportacaoDepositoBean> listaDeposito;
	
	private Empresa empresa;
	private Documentotipo documentotipo;
	private Contagerencial contagerencial;
	private Centrocusto centrocusto;
	
	private Colaboradordespesamotivo colaboradordespesamotivo;
	
	public Boolean getEntrada() {
		return entrada;
	}
	public String getWhereIn() {
		return whereIn;
	}
	public List<ImportacaoEnvioBancarioBean> getListaEnviobancario() {
		return listaEnviobancario;
	}
	public List<ImportacaoDepositoBean> getListaDeposito() {
		return listaDeposito;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@DisplayName("Motivo")
	public Colaboradordespesamotivo getColaboradordespesamotivo() {
		return colaboradordespesamotivo;
	}
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setColaboradordespesamotivo(
			Colaboradordespesamotivo colaboradordespesamotivo) {
		this.colaboradordespesamotivo = colaboradordespesamotivo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setListaEnviobancario(
			List<ImportacaoEnvioBancarioBean> listaEnviobancario) {
		this.listaEnviobancario = listaEnviobancario;
	}
	public void setListaDeposito(List<ImportacaoDepositoBean> listaDeposito) {
		this.listaDeposito = listaDeposito;
	}
	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
}