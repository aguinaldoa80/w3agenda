package br.com.linkcom.sined.modulo.rh.controller.process;


import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.service.EscalaService;

@Controller(
		path={"/rh/process/Escala"},
		authorizationModule=ProcessAuthorizationModule.class
)
public class EscalaProcess extends MultiActionController{
	
	private EscalaService escalaService;
	
	public void setEscalaService(EscalaService escalaService) {
		this.escalaService = escalaService;
	}
	
	/**
	 * Carrega a popup de cria��o de um contato.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("editar")
	public ModelAndView editar(WebRequestContext request, Escala escala){
		escala = escalaService.loadForEntrada(escala);
		return new ModelAndView("direct:process/popup/editEscala","escala",escala);
	}
	
	/**
	 * M�todo que salva a Escala
	 * 
	 * @param request
	 * @param escala
	 * @author Tom�s Rabelo
	 */
	@Command(validate=true)
	@Input("criar")
	@Action("save")
	public void save(WebRequestContext request, Escala escala){
		escalaService.saveOrUpdate(escala);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.close();</script>");
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		Escala bean = (Escala)obj;
		if(bean.getListaescalahorario() != null && !bean.getListaescalahorario().isEmpty()){
			boolean achouIntercalado = false;
			for (Escalahorario escalahorario : bean.getListaescalahorario()) {
				if(escalahorario.getHorafim().getTime() <= escalahorario.getHorainicio().getTime()){
					errors.reject("001", "Hora final deve ser maior que hora inicial.");
					break;
				}
				
				for (Escalahorario escalahorarioAux : bean.getListaescalahorario()) {
					if(escalahorario.getDiasemana() == null || (escalahorario.getDiasemana() != null && escalahorario.getDiasemana().equals(escalahorarioAux.getDiasemana()))){
						if((escalahorario.getHorainicio().before(escalahorarioAux.getHorafim()) && escalahorario.getHorainicio().after(escalahorarioAux.getHorainicio())) || 
						   (escalahorario.getHorafim().before(escalahorarioAux.getHorafim()) && escalahorario.getHorafim().after(escalahorarioAux.getHorainicio()))){
							errors.reject("002", "Existem hor�rios intercalados.");
							achouIntercalado = true;
							break;
						}
					}
				}
				if(achouIntercalado)
					break;
			}
		}
	}
	
}
