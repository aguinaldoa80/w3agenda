package br.com.linkcom.sined.modulo.rh.controller.process.filter; 

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Regimecontratacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AlterarsituacaocolaboradorFilter extends FiltroListagemSined{
	
	protected Integer matricula;
	protected String nome;
	protected Cpf cpf;
	protected Departamento departamento;
	protected Cargo cargo;
	protected Date dtadminicio;
	protected Date dtadmfim;
	protected Colaboradorsituacao colaboradorsituacao;
	protected Regimecontratacao regimecontratacao;
	protected Date vencimentosituacaoInicio;
	protected Date vencimentosituacaoFim;

	@DisplayName("Nome")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public Cpf getCpf() {
		return cpf;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}
	
	public Cargo getCargo() {
		return cargo;
	}
	
	@DisplayName(" De ")
	public Date getDtadminicio() {
		return dtadminicio;
	}
	
	@DisplayName(" at� ")
	public Date getDtadmfim() {
		return dtadmfim;
	}
	
	@DisplayName("Regime de contrata��o")
	public Regimecontratacao getRegimecontratacao() {
		return regimecontratacao;
	}
	@MaxLength(9)
	@DisplayName("Matr�cula")
	public Integer getMatricula() {
		return matricula;
	}
	public Date getVencimentosituacaoInicio() {
		return vencimentosituacaoInicio;
	}
	public Date getVencimentosituacaoFim() {
		return vencimentosituacaoFim;
	}
	
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public void setDtadminicio(Date dtadminicio) {
		this.dtadminicio = dtadminicio;
	}
	
	public void setDtadmfim(Date dtadmfim) {
		this.dtadmfim = dtadmfim;
	}
	
	@DisplayName("Situa��o")
	public Colaboradorsituacao getColaboradorsituacao() {
		return colaboradorsituacao;
	}
	
	public void setColaboradorsituacao(Colaboradorsituacao colaboradorsituacao) {
		this.colaboradorsituacao = colaboradorsituacao;
	}
	public void setRegimecontratacao(Regimecontratacao regimecontratacao) {
		this.regimecontratacao = regimecontratacao;
	}

	public void setVencimentosituacaoInicio(Date vencimentosituacaoInicio) {
		this.vencimentosituacaoInicio = vencimentosituacaoInicio;
	}

	public void setVencimentosituacaoFim(Date vencimentosituacaoFim) {
		this.vencimentosituacaoFim = vencimentosituacaoFim;
	}
}
