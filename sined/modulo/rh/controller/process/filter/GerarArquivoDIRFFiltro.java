package br.com.linkcom.sined.modulo.rh.controller.process.filter; 

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Empresa;

public class GerarArquivoDIRFFiltro {
	
	private Colaboradordespesamotivo colaboradordespesamotivo;
	private Empresa empresa;
	private Integer ano;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Motivo da despesa")
	public Colaboradordespesamotivo getColaboradordespesamotivo() {
		return colaboradordespesamotivo;
	}
	
	@MaxLength(4)
	@MinLength(4)
	@Required
	public Integer getAno() {
		return ano;
	}
	
	public void setColaboradordespesamotivo(
			Colaboradordespesamotivo colaboradordespesamotivo) {
		this.colaboradordespesamotivo = colaboradordespesamotivo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	
}