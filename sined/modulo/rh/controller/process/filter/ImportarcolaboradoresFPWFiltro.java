package br.com.linkcom.sined.modulo.rh.controller.process.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;


public class ImportarcolaboradoresFPWFiltro {

	private Arquivo arquivo;

	@Required
	@DisplayName("Arquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

}
