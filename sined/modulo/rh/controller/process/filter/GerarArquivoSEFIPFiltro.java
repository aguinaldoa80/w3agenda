package br.com.linkcom.sined.modulo.rh.controller.process.filter; 

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;

public class GerarArquivoSEFIPFiltro {
	
	private Empresa empresa;
	private Departamento departamento;
	private Cargo cargo;
	private Date dtinicio;
	private Date dtfim;
	private Boolean considerarContabilidade = Boolean.TRUE;
	private Boolean considerarTomadorServico = Boolean.TRUE;
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Departamento")
	public Departamento getDepartamento() {
		return departamento;
	}
	@DisplayName("Cargo")
	public Cargo getCargo() {
		return cargo;
	}
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Considerar Contabilidade")
	public Boolean getConsiderarContabilidade() {
		return considerarContabilidade;
	}
	@DisplayName("Considerar Cliente como Tomador de Servi�o")
	public Boolean getConsiderarTomadorServico() {
		return considerarTomadorServico;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setConsiderarContabilidade(Boolean considerarContabilidade) {
		this.considerarContabilidade = considerarContabilidade;
	}
	public void setConsiderarTomadorServico(Boolean considerarTomadorServico) {
		this.considerarTomadorServico = considerarTomadorServico;
	}
}