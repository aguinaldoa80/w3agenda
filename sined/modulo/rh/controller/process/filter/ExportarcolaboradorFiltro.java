package br.com.linkcom.sined.modulo.rh.controller.process.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorstatusfolha;
import br.com.linkcom.sined.geral.bean.Departamento;

public class ExportarcolaboradorFiltro {

	protected Long matricula;
	protected String nome;
	protected Departamento departamento;
	protected Cargo cargo;
	protected Colaboradorstatusfolha colaboradorstatusfolha;
	protected Date dtinicio;
	protected Date dtfim;
	protected List<Colaborador> listaColaborador =  new ArrayList<Colaborador>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	@DisplayName("")
	public Date getDtinicio() {
		return dtinicio;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	@DisplayName("")
	public Date getDtfim() {
		return dtfim;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public List<Colaborador> getListaColaborador() {
		return listaColaborador;
	}

	public void setListaColaborador(List<Colaborador> listaColaborador) {
		this.listaColaborador = listaColaborador;
	}

	@DisplayName("Status Colaborador Folha")
	public Colaboradorstatusfolha getColaboradorstatusfolha() {
		return colaboradorstatusfolha;
	}
	
	public void setColaboradorstatusfolha(Colaboradorstatusfolha colaboradorstatusfolha) {
		this.colaboradorstatusfolha = colaboradorstatusfolha;
	}
	
	@MaxLength(9)
	@DisplayName("Matrícula")
	public Long getMatricula() {
		return matricula;
	}
	
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}

}
