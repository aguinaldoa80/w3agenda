package br.com.linkcom.sined.modulo.rh.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documentotipo;


public class ImportarFolhaPagamentoFiltro {

	private Arquivo arquivo;
	private Documentotipo documentotipo;
	private Date dtvencimento;
	private Contagerencial contagerencial;
	private Centrocusto centrocusto;
	private String mensagemErro;

	@Required
	@DisplayName("Arquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@Required
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@Required
	@DisplayName("Vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public String getMensagemErro() {
		return mensagemErro;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setMensagemErro(String mensagemErro) {
		this.mensagemErro = mensagemErro;
	}
}