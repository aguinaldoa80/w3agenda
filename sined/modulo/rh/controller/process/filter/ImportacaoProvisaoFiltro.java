package br.com.linkcom.sined.modulo.rh.controller.process.filter;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Provisao;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoProvisaoErroBean;

public class ImportacaoProvisaoFiltro {

	private Arquivo arquivo;
	private List<Provisao> listaBean;
	private List<ImportacaoProvisaoErroBean> listaBeanComErro;
	
	public Arquivo getArquivo() {
		return arquivo;
	}
	public List<Provisao> getListaBean() {
		return listaBean;
	}
	public List<ImportacaoProvisaoErroBean> getListaBeanComErro() {
		return listaBeanComErro;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setListaBean(List<Provisao> listaBean) {
		this.listaBean = listaBean;
	}
	public void setListaBeanComErro(List<ImportacaoProvisaoErroBean> listaBeanComErro) {
		this.listaBeanComErro = listaBeanComErro;
	}
}
