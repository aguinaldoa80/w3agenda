package br.com.linkcom.sined.modulo.rh.controller.process.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class HoleriteFiltro extends FiltroListagemSined{
	
	protected Date dtreferencia1;
	protected Date dtreferencia2;
	protected Empresa empresa;
	protected Departamento departamento;
	protected Cargo cargo;
	protected Projeto projeto;
	protected String codigos;
	protected List<Colaboradordespesaitem> listaColaboradordespesaitems;
	protected Integer cdcolaborador;
	protected Boolean holeritePorProjeto;
	protected Colaborador colaborador;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Cargo getCargo() {
		return cargo;
	}	
	public String getCodigos() {
		return codigos;
	}	
	public List<Colaboradordespesaitem> getListaColaboradordespesaitems() {
		return listaColaboradordespesaitems;
	}	
	public Integer getCdcolaborador() {
		return cdcolaborador;
	}
	@Required
	@DisplayName("Per�odo de refer�ncia")
	public Date getDtreferencia1() {
		return dtreferencia1;
	}
	@Required
	@DisplayName("Per�odo de refer�ncia")
	public Date getDtreferencia2() {
		return dtreferencia2;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Gerar holerite de colaborador por projeto?")
	public Boolean getHoleritePorProjeto() {
		return holeritePorProjeto;
	}
	public void setHoleritePorProjeto(Boolean holeritePorProjeto) {
		this.holeritePorProjeto = holeritePorProjeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDtreferencia1(Date dtreferencia1) {
		this.dtreferencia1 = dtreferencia1;
	}
	public void setDtreferencia2(Date dtreferencia2) {
		this.dtreferencia2 = dtreferencia2;
	}
	public void setCdcolaborador(Integer cdcolaborador) {
		this.cdcolaborador = cdcolaborador;
	}
	public void setListaColaboradordespesaitems(
			List<Colaboradordespesaitem> listaColaboradordespesaitems) {
		this.listaColaboradordespesaitems = listaColaboradordespesaitems;
	}
	public void setCodigos(String codigos) {
		this.codigos = codigos;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
}
