package br.com.linkcom.sined.modulo.rh.controller.process.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.service.ColaboradorsituacaoService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AlterarcargocolaboradorFilter extends FiltroListagemSined{
	
	protected Integer matricula;
	protected String nome;
	protected Cpf cpf;
	protected Departamento departamentoAtual;
	protected Cargo cargoAtual;
	protected List<Colaboradorsituacao> colaboradorSituacao;
	protected Date vencimentosituacaoInicio;
	protected Date vencimentosituacaoFim;
	
	public AlterarcargocolaboradorFilter() {
		colaboradorSituacao = ColaboradorsituacaoService.getInstance().findAll();
	}
	
	@DisplayName("Nome")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@DisplayName("Cpf")
	public Cpf getCpf() {
		return cpf;
	}
	@DisplayName("Departamento atual")
	public Departamento getDepartamentoAtual() {
		return departamentoAtual;
	}
	@DisplayName("Cargo atual")
	public Cargo getCargoAtual() {
		return cargoAtual;
	}
	@DisplayName("Situa��es")
	public List<Colaboradorsituacao> getColaboradorSituacao() {
		return colaboradorSituacao;
	}
	@MaxLength(9)
	@DisplayName("Matr�cula")
	public Integer getMatricula() {
		return matricula;
	}
	public Date getVencimentosituacaoInicio() {
		return vencimentosituacaoInicio;
	}
	public Date getVencimentosituacaoFim() {
		return vencimentosituacaoFim;
	}
	
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public void setNome(String colaborador) {
		this.nome = colaborador;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setDepartamentoAtual(Departamento departamento) {
		this.departamentoAtual = departamento;
	}
	public void setCargoAtual(Cargo cargo) {
		this.cargoAtual = cargo;
	}
	public void setColaboradorSituacao(List<Colaboradorsituacao> status) {
		this.colaboradorSituacao = status;
	}
	public void setVencimentosituacaoInicio(Date vencimentosituacaoInicio) {
		this.vencimentosituacaoInicio = vencimentosituacaoInicio;
	}
	public void setVencimentosituacaoFim(Date vencimentosituacaoFim) {
		this.vencimentosituacaoFim = vencimentosituacaoFim;
	}
}
