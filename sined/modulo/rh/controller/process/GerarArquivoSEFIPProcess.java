package br.com.linkcom.sined.modulo.rh.controller.process;


import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sourceforge.jeval.Evaluator;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.geral.bean.Formularhitem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.service.CargoService;
import br.com.linkcom.sined.geral.service.ColaboradorcargoService;
import br.com.linkcom.sined.geral.service.DocumentocomissaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FormularhService;
import br.com.linkcom.sined.geral.service.FormularhitemService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip.ArquivoSEFIP00;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip.ArquivoSEFIP10;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip.ArquivoSEFIP20;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip.ArquivoSEFIP30;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip.ArquivoSEFIP30Item;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivosefip.ArquivoSEFIP90;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.GerarArquivoSEFIPFiltro;
import br.com.linkcom.sined.util.SinedException;

@Bean
@Controller(path="/rh/process/GerarArquivoSEFIP", authorizationModule=ProcessAuthorizationModule.class)
public class GerarArquivoSEFIPProcess extends MultiActionController {
	
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private TelefoneService telefoneService;
	private ColaboradorcargoService colaboradorcargoService;
	private FormularhService formularhService;
	private CargoService cargoService;
	private DocumentocomissaoService documentocomissaoService;
	private FormularhitemService formularhitemService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setTelefoneService(TelefoneService telefoneService) {
		this.telefoneService = telefoneService;
	}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {
		this.colaboradorcargoService = colaboradorcargoService;
	}
	public void setFormularhService(FormularhService formularhService) {
		this.formularhService = formularhService;
	}
	public void setCargoService(CargoService cargoService) {
		this.cargoService = cargoService;
	}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {
		this.documentocomissaoService = documentocomissaoService;
	}
	public void setFormularhitemService(FormularhitemService formularhitemService) {
		this.formularhitemService = formularhitemService;
	}

	@DefaultAction
	public ModelAndView index(WebRequestContext request, GerarArquivoSEFIPFiltro filtro) throws Exception {
		
		if (filtro.getDtinicio()==null && filtro.getDtfim()==null){
			Calendar calendar1 = Calendar.getInstance();
			calendar1.set(Calendar.DAY_OF_MONTH, calendar1.getActualMinimum(Calendar.DAY_OF_MONTH));
			filtro.setDtinicio(new Date(calendar1.getTimeInMillis()));
			
			Calendar calendar2 = Calendar.getInstance();
			calendar2.set(Calendar.DAY_OF_MONTH, calendar2.getActualMaximum(Calendar.DAY_OF_MONTH));
			filtro.setDtfim(new Date(calendar2.getTimeInMillis()));
		}
		
		return new ModelAndView("process/gerarArquivoSEFIP", "filtro", filtro);
	}
	
	public ModelAndView gerarArquivo(WebRequestContext request, GerarArquivoSEFIPFiltro filtro) throws Exception {
		
		if (validarGerarArquivoFiltro(request, filtro)){
			String retorno = "";
			if (Boolean.TRUE.equals(filtro.getConsiderarTomadorServico())){
			
				Empresa empresa = empresaService.loadForArquivoSEFIP(filtro.getEmpresa());
				List<Endereco> listaEnderecoEmpresa = enderecoService.carregarListaEndereco(empresa);
				List<Telefone> listaTelefoneEmpresa = telefoneService.findByPessoa(empresa);
				
				if (validarGerarArquivo(request, filtro, empresa, listaEnderecoEmpresa, listaTelefoneEmpresa)){
					
					StringBuilder arquivoSEFIP = new StringBuilder();
					Endereco enderecoEmpresa = listaEnderecoEmpresa.get(0);
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(filtro.getDtinicio().getTime());
					String competencia = new DecimalFormat("0000").format((calendar.get(Calendar.YEAR))) + new DecimalFormat("00").format((calendar.get(Calendar.MONTH)+1));
					List<Colaboradorcargo> listaColaboradorcargo = colaboradorcargoService.findForGerarArquivoSEFIP(filtro); 
					
					Map<Integer, Formularh> mapaCdcargoFormularh = new HashMap<Integer, Formularh>();
					Map<Integer, Cliente> mapaCliente = new HashMap<Integer, Cliente>();				
					Map<Integer, List<ArquivoSEFIP30Item>> mapaCdclienteListaArquivoSEFIP30Item = new HashMap<Integer, List<ArquivoSEFIP30Item>>();
					Map<Integer, Money> mapaCdclienteValor = new HashMap<Integer, Money>();				
					Evaluator evaluator = formularhService.getEvaluator();
					
					for (Cargo cargo: cargoService.findAll()){
						mapaCdcargoFormularh.put(cargo.getCdcargo(), formularhService.findByCargo(cargo));
					}
					
					for (Colaboradorcargo colaboradorcargo: listaColaboradorcargo){
						
						Formularh formularh = mapaCdcargoFormularh.get(colaboradorcargo.getCargo().getCdcargo());
						if (formularh==null){
							throw new SinedException("N�o existe f�rmula cadastrada para: <ul><li>Cargo: " + colaboradorcargo.getCargo().getNome() + "</li><li>Colaborador: " + colaboradorcargo.getColaborador().getNome() + "</li></ul>");
						}
						
						Formularhitem formularhitemBase = formularhitemService.loadForGerarArquivoSEFIP(formularh, "BASE");
						Formularhitem formularhitemInss = formularhitemService.loadForGerarArquivoSEFIP(formularh, "INSS");
						
						if (formularhitemBase==null || formularhitemInss==null){
							throw new SinedException("Identificador BASE e INSS n�o cadastrado para a f�rmula " + formularh.getNome());
						}
						
						for (Documentocomissao documentocomissao: documentocomissaoService.findForGerarArquivoSEFIP(colaboradorcargo.getColaborador(), filtro.getDtinicio(), filtro.getDtfim())){
							
							evaluator.putVariable("COMISSAO_VENCIMENTO", formularhService.formatoEvaluator(documentocomissao.getValorcomissaoListagem()));
							String remuneracaoSem13Str = evaluator.evaluate(formularhService.formatoFuncao(formularhitemBase.getFormula()));
							String valorDescontadoSeguradoStr = evaluator.evaluate(formularhService.formatoFuncao(formularhitemInss.getFormula()));
							remuneracaoSem13Str = remuneracaoSem13Str.replace("-", "");
							valorDescontadoSeguradoStr = valorDescontadoSeguradoStr.replace("-", "");
							Money remuneracaoSem13 = new Money(Double.parseDouble(remuneracaoSem13Str));
							Money valorDescontadoSegurado = new Money(Double.parseDouble(valorDescontadoSeguradoStr));
							
							if (remuneracaoSem13.toLong()>0){
								
								Cliente cliente = documentocomissao.getContrato().getCliente();
								Integer cdcolaborador = colaboradorcargo.getColaborador().getCdpessoa();
								Integer cdcliente = cliente.getCdpessoa();
								mapaCliente.put(cdcliente, cliente);
								
								List<ArquivoSEFIP30Item> listaArquivoSEFIP30Item = mapaCdclienteListaArquivoSEFIP30Item.get(cdcliente);
								if (listaArquivoSEFIP30Item==null){
									listaArquivoSEFIP30Item = new ArrayList<ArquivoSEFIP30Item>();
								}
								
								boolean encontrou = false;
								for (ArquivoSEFIP30Item arquivoSEFIP30Item: listaArquivoSEFIP30Item){
									if (arquivoSEFIP30Item.getCdcolaborador().equals(cdcolaborador)){
										arquivoSEFIP30Item.setRemuneracaoSem13(arquivoSEFIP30Item.getRemuneracaoSem13().add(remuneracaoSem13));
										arquivoSEFIP30Item.setValorDescontadoSegurado(arquivoSEFIP30Item.getValorDescontadoSegurado().add(valorDescontadoSegurado));
										mapaCdclienteValor.put(cdcliente, mapaCdclienteValor.get(cdcliente).add(remuneracaoSem13));
										encontrou = true;
									}
								}
								
								if (encontrou==false){
									ArquivoSEFIP30Item arquivoSEFIP30Item = new ArquivoSEFIP30Item();
									arquivoSEFIP30Item.setCdcolaborador(cdcolaborador);
									arquivoSEFIP30Item.setNome(colaboradorcargo.getColaborador().getNome());
									arquivoSEFIP30Item.setPisPasepCi(colaboradorcargo.getColaborador().getNumeroinscricao().replaceAll("[^0-9]", ""));
									arquivoSEFIP30Item.setCbo(colaboradorcargo.getCargo().getCbo().getCdcbo().toString());
									arquivoSEFIP30Item.setRemuneracaoSem13(remuneracaoSem13);
									arquivoSEFIP30Item.setValorDescontadoSegurado(valorDescontadoSegurado);
									arquivoSEFIP30Item.setCliente(cliente);
									listaArquivoSEFIP30Item.add(arquivoSEFIP30Item);
									if (mapaCdclienteValor.get(cdcliente)==null){
										mapaCdclienteValor.put(cdcliente, remuneracaoSem13);
									}else {
										mapaCdclienteValor.put(cdcliente, mapaCdclienteValor.get(cdcliente).add(remuneracaoSem13));
									}
								}
								mapaCdclienteListaArquivoSEFIP30Item.put(cdcliente, listaArquivoSEFIP30Item);
							}						
						}					
					}				
					
					ArquivoSEFIP00 arquivoSEFIP00 = new ArquivoSEFIP00();
					
					if (Boolean.TRUE.equals(filtro.getConsiderarContabilidade())){
						Fornecedor escritoriocontabilista = empresa.getEscritoriocontabilista();
						Fornecedor colaboradorcontabilista = empresa.getColaboradorcontabilista();
						Endereco enderecoContabilidade = new ListSet<Endereco>(Endereco.class, escritoriocontabilista.getListaEndereco()).get(0);
						arquivoSEFIP00.setPosicao_56_69(Long.valueOf(escritoriocontabilista.getCnpj().getValue()));
						arquivoSEFIP00.setPosicao_70_99(escritoriocontabilista.getNome());
						arquivoSEFIP00.setPosicao_100_119(colaboradorcontabilista.getNome());
						arquivoSEFIP00.setPosicao_120_169(enderecoContabilidade.getLogradouroNumeroComplemento());
						arquivoSEFIP00.setPosicao_170_189(enderecoContabilidade.getBairro());
						arquivoSEFIP00.setPosicao_190_197(Long.valueOf(enderecoContabilidade.getCep().getValue()));
						arquivoSEFIP00.setPosicao_198_217(enderecoContabilidade.getMunicipio().getNome());
						arquivoSEFIP00.setPosicao_218_219(enderecoContabilidade.getMunicipio().getUf().getSigla());
						arquivoSEFIP00.setPosicao_220_231(Long.valueOf(new ListSet<Telefone>(Telefone.class, escritoriocontabilista.getListaTelefone()).get(0).getTelefone().replaceAll("[^0-9]", "")));
						arquivoSEFIP00.setPosicao_292_297(competencia);
						arquivoSEFIP00.setPosicao_328_341(Long.valueOf(escritoriocontabilista.getCnpj().getValue()));
					}else {
						arquivoSEFIP00.setPosicao_56_69(Long.valueOf(empresa.getCnpj().getValue()));
						arquivoSEFIP00.setPosicao_70_99(empresa.getRazaosocialOuNome());
						arquivoSEFIP00.setPosicao_100_119(empresa.getResponsavel().getNome());
						arquivoSEFIP00.setPosicao_120_169(enderecoEmpresa.getLogradouroNumeroComplemento());
						arquivoSEFIP00.setPosicao_170_189(enderecoEmpresa.getBairro());
						arquivoSEFIP00.setPosicao_190_197(Long.valueOf(enderecoEmpresa.getCep().getValue()));
						arquivoSEFIP00.setPosicao_198_217(enderecoEmpresa.getMunicipio().getNome());
						arquivoSEFIP00.setPosicao_218_219(enderecoEmpresa.getMunicipio().getUf().getSigla());
						arquivoSEFIP00.setPosicao_220_231(Long.valueOf(listaTelefoneEmpresa.get(0).getTelefone().replaceAll("[^0-9]", "")));
						arquivoSEFIP00.setPosicao_292_297(competencia);
						arquivoSEFIP00.setPosicao_328_341(Long.valueOf(empresa.getCnpj().getValue()));
					}
					
					ArquivoSEFIP10 arquivoSEFIP10 = new ArquivoSEFIP10();
					arquivoSEFIP10.setPosicao_4_17(Long.valueOf(empresa.getCnpj().getValue()));
					arquivoSEFIP10.setPosicao_54_93(empresa.getRazaosocialOuNome());
					arquivoSEFIP10.setPosicao_94_143(enderecoEmpresa.getLogradouroNumeroComplemento());
					arquivoSEFIP10.setPosicao_144_163(enderecoEmpresa.getBairro());
					arquivoSEFIP10.setPosicao_164_171(Long.valueOf(enderecoEmpresa.getCep().getValue()));
					arquivoSEFIP10.setPosicao_172_191(enderecoEmpresa.getMunicipio().getNome());
					arquivoSEFIP10.setPosicao_192_193(enderecoEmpresa.getMunicipio().getUf().getSigla());
					arquivoSEFIP10.setPosicao_194_205(Long.valueOf(listaTelefoneEmpresa.get(0).getTelefone().replaceAll("[^0-9]", "")));
					
					arquivoSEFIP.append(arquivoSEFIP00.gerarLinha());
					arquivoSEFIP.append("\r\n");
					arquivoSEFIP.append(arquivoSEFIP10.gerarLinha());
					
					List<Cliente> listaClienteOrdenada = new ArrayList<Cliente>();
					
					for (Integer cdcliente: mapaCdclienteListaArquivoSEFIP30Item.keySet()){
						Cliente cliente = mapaCliente.get(cdcliente);
						listaClienteOrdenada.add(cliente);
					}
					
					Collections.sort(listaClienteOrdenada, new Comparator<Cliente>(){
						@Override
						public int compare(Cliente o1, Cliente o2) {
							return o1.getCnpj().getValue().compareTo(o2.getCnpj().getValue());
						}					
					});
					
					for (Cliente cliente: listaClienteOrdenada){	
						
						Integer cdcliente = cliente.getCdpessoa();
						Endereco enderecoCliente = new ListSet<Endereco>(Endereco.class, cliente.getListaEndereco()).get(0);
						
						if (enderecoCliente.getLogradouroNumeroComplemento()!=null
								&& !enderecoCliente.getLogradouroNumeroComplemento().trim().equals("")
								&& enderecoCliente.getBairro()!=null
								&& !enderecoCliente.getBairro().trim().equals("")
								&& enderecoCliente.getCep()!=null){	
						
							ArquivoSEFIP20 arquivoSEFIP20 = new ArquivoSEFIP20();
							arquivoSEFIP20.setPosicao_4_17(Long.valueOf(empresa.getCnpj().getValue()));
							arquivoSEFIP20.setPosicao_19_32(Long.valueOf(cliente.getCnpj().getValue()));
							arquivoSEFIP20.setPosicao_54_93(cliente.getNome());
							arquivoSEFIP20.setPosicao_94_143(enderecoCliente.getLogradouroNumeroComplemento());
							arquivoSEFIP20.setPosicao_144_163(enderecoCliente.getBairro());
							arquivoSEFIP20.setPosicao_164_171(Long.valueOf(enderecoCliente.getCep().getValue()));
							arquivoSEFIP20.setPosicao_172_191(enderecoCliente.getMunicipio().getNome());
							arquivoSEFIP20.setPosicao_192_193(enderecoCliente.getMunicipio().getUf().getSigla());
							arquivoSEFIP20.setPosicao_258_272(mapaCdclienteValor.get(cdcliente).toLong());
							
							arquivoSEFIP.append("\r\n");
							arquivoSEFIP.append(arquivoSEFIP20.gerarLinha());
							
							List<ArquivoSEFIP30Item> listaArquivoSEFIP30Item = mapaCdclienteListaArquivoSEFIP30Item.get(cdcliente);
							Collections.sort(listaArquivoSEFIP30Item, new Comparator<ArquivoSEFIP30Item>(){
								@Override
								public int compare(ArquivoSEFIP30Item o1, ArquivoSEFIP30Item o2) {
									return Long.valueOf(o1.getPisPasepCi()).compareTo(Long.valueOf(o2.getPisPasepCi()));
								}					
							});
							
							for (ArquivoSEFIP30Item arquivoSEFIP30Item: listaArquivoSEFIP30Item){
								ArquivoSEFIP30 arquivoSEFIP30 = new ArquivoSEFIP30();
								arquivoSEFIP30.setPosicao_4_17(Long.valueOf(empresa.getCnpj().getValue()));
								arquivoSEFIP30.setPosicao_19_32(Long.valueOf(cliente.getCnpj().getValue()));
								arquivoSEFIP30.setPosicao_33_43(Long.valueOf(arquivoSEFIP30Item.getPisPasepCi()));
								arquivoSEFIP30.setPosicao_54_123(arquivoSEFIP30Item.getNome());
								//arquivoSEFIP30.setPosicao_163_167(arquivoSEFIP30Item.getCbo());
								arquivoSEFIP30.setPosicao_168_182(arquivoSEFIP30Item.getRemuneracaoSem13().toLong());
								//arquivoSEFIP30.setPosicao_202_216(arquivoSEFIP30Item.getValorDescontadoSegurado().toLong());
								arquivoSEFIP.append("\r\n");
								arquivoSEFIP.append(arquivoSEFIP30.gerarLinha());
							}
						}
					}				
					
					ArquivoSEFIP90 arquivoSEFIP90 = new ArquivoSEFIP90(); 
					arquivoSEFIP.append("\r\n");
					arquivoSEFIP.append(arquivoSEFIP90.gerarLinha());
					
					retorno = arquivoSEFIP.toString();
					retorno = retorno.toUpperCase();
					retorno = Util.strings.tiraAcento(retorno);
					
				}
			}else {
				//TODO Implementar futuramente quando a demanda chegar
			}
			
			Resource resource = new Resource();
			resource.setContentType("text/plain");
			resource.setFileName("SEFIP.RE");
		    resource.setContents(retorno.getBytes());
			
		    return new ResourceModelAndView(resource);
		}
		
		return index(request, filtro);
	}
	
	private boolean validarGerarArquivoFiltro(WebRequestContext request, GerarArquivoSEFIPFiltro filtro) throws Exception {
		
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(filtro.getDtinicio().getTime());
		
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTimeInMillis(filtro.getDtfim().getTime());
		
		if (calendar1.get(Calendar.MONTH)!=calendar2.get(Calendar.MONTH)){
			request.addMessage("O intervalo de datas deve ser dentro de um m�s", MessageType.ERROR);
			return false;
		}
		
		return true;
	}
	
	private boolean validarGerarArquivo(WebRequestContext request, GerarArquivoSEFIPFiltro filtro, Empresa empresa, 
										List<Endereco> listaEnderecoEmpresa, List<Telefone> listaTelefoneEmpresa) throws Exception {
		
		if (empresa.getCnpj()==null){
			request.addMessage("O Cnpj da empresa n�o est� cadastrado corretamente", MessageType.ERROR);
			return false;
		}
		
		if (listaEnderecoEmpresa==null || listaEnderecoEmpresa.isEmpty()){
			request.addMessage("O Endere�o da empresa n�o est� cadastrado corretamente", MessageType.ERROR);
			return false;
		}
		
		if (listaTelefoneEmpresa==null || listaTelefoneEmpresa.isEmpty()){
			request.addMessage("O Telefone da empresa n�o est� cadastrado corretamente", MessageType.ERROR);
			return false;
		}
		
		if (Boolean.TRUE.equals(filtro.getConsiderarContabilidade())){
			
			Fornecedor escritoriocontabilista = empresa.getEscritoriocontabilista();
			Fornecedor colaboradorcontabilista = empresa.getColaboradorcontabilista();
			
			if (escritoriocontabilista==null || escritoriocontabilista.getCnpj()==null || colaboradorcontabilista==null){
				request.addMessage("A Contabilidade da empresa n�o est� cadastrado corretamente", MessageType.ERROR);
				return false;
			}
			
			Set<Endereco> listaEnderecoEmpresaContabilista = escritoriocontabilista.getListaEndereco();
			Set<Telefone> listaTelefoneEmpresaContabilista = escritoriocontabilista.getListaTelefone();
			
			if (listaEnderecoEmpresaContabilista==null || listaEnderecoEmpresaContabilista.isEmpty()){
				request.addMessage("O Endere�o da Contabilidade da empresa n�o est� cadastrado corretamente", MessageType.ERROR);
				return false;
			}
			
			if (listaTelefoneEmpresaContabilista==null || listaTelefoneEmpresaContabilista.isEmpty()){
				request.addMessage("O Telefone da Contabilidade da empresa n�o est� cadastrado corretamente", MessageType.ERROR);
				return false;
			}
		}else {
			if (empresa.getResponsavel()==null){
				request.addMessage("O Respons�vel para Contato da empresa n�o est� cadastrado corretamente", MessageType.ERROR);
				return false;
			}
		}
		
		return true;
	}
}