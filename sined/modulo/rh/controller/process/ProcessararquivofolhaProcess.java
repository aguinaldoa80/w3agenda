package br.com.linkcom.sined.modulo.rh.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivofolha;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhaorigem;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhasituacao;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivofolhaService;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoArquivofolhaBean;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/rh/process/Processararquivofolha", authorizationModule=ProcessAuthorizationModule.class)
public class ProcessararquivofolhaProcess extends MultiActionController {
	
	private ArquivofolhaService arquivofolhaService;
	private ArquivoService arquivoService;
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivofolhaService(ArquivofolhaService arquivofolhaService) {
		this.arquivofolhaService = arquivofolhaService;
	}

	/**
	 * Action padr�o que abre a tela de confirma��o para o processamento do arquivo de folha.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#findForProcessamento(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.ArquivoService#loadWithContents(Arquivo bean)
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#preencheDeposito(List<Arquivofolha> lista) 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#preencheEnvioBancario(List<Arquivofolha> lista)
	 * 
	 * @param request
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	@DefaultAction
	public ModelAndView processar(WebRequestContext request){
		
		ImportacaoArquivofolhaBean bean = new ImportacaoArquivofolhaBean();
		bean.setWhereIn(SinedUtil.getItensSelecionados(request));
		bean.setEntrada("true".equals(request.getParameter("entrada")));
		
		Arquivo arquivo;
		String string;
		
		List<String> erros = new ArrayList<String>();
		
		List<Arquivofolha> lista = arquivofolhaService.findForProcessamento(bean.getWhereIn());
		for (Arquivofolha af : lista) {
			if(!af.getArquivofolhasituacao().equals(Arquivofolhasituacao.AUTORIZADO)){
				erros.add("Arquivo de folha " + af.getCdarquivofolha() + " n�o est� com a situa��o 'AUTORIZADO'.");
				continue;
			}
			
			if(!af.getArquivofolhaorigem().equals(Arquivofolhaorigem.IMPORTACAO_FOLHA)){
				erros.add("Arquivo de folha " + af.getCdarquivofolha() + " n�o � Importa��o de Folha.");
				continue;
			}
			
			arquivo = arquivoService.loadWithContents(af.getArquivo());
			string = new String(arquivo.getContent());
			af.setStringArquivo(string.split("\\r?\\n"));
		}
		
		if(erros.size() > 0){
			request.addError("N�o foi poss�vel processar o(s) arquivo(s) selecionado(s)."); 
			for (String erro : erros) {
				request.addError(erro);
			}
			return new ModelAndView("redirect:/rh/crud/Arquivofolha" + (bean.getEntrada() ? "?ACAO=consultar&cdarquivofolha=" + bean.getWhereIn() : ""));
		}
		
		try {
			bean.setListaDeposito(arquivofolhaService.preencheDeposito(lista));
			request.setAttribute("haveDeposito", bean.getListaDeposito() != null && bean.getListaDeposito().size() > 0);
			
			bean.setListaEnviobancario(arquivofolhaService.preencheEnvioBancario(lista));
			request.setAttribute("haveEnviobancario", bean.getListaEnviobancario() != null && bean.getListaEnviobancario().size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return new ModelAndView("redirect:/rh/crud/Arquivofolha" + (bean.getEntrada() ? "?ACAO=consultar&cdarquivofolha=" + bean.getWhereIn() : ""));
		}
		
		return new ModelAndView("process/processarArquivofolha", "bean", bean);
	}
	
	
	
	/**
	 * Action que salva os registros do processamento do(s) arquivo(s) de folha.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#processarDeposito(ImportacaoArquivofolhaBean bean)
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#processarEnviobancario(ImportacaoArquivofolhaBean bean)
	 * 
	 * @param request
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView saveProcessamento(WebRequestContext request, ImportacaoArquivofolhaBean bean){
		try{
			arquivofolhaService.processarDeposito(bean);
			arquivofolhaService.processarEnviobancario(bean);
		
			request.addMessage("Arquivo(s) processado(s) com sucesso.");
		} catch (Exception e) {
			request.addError(e.getMessage());
			e.printStackTrace();
		}
		
		if(bean.getEntrada()){
			return new ModelAndView("redirect:/rh/crud/Arquivofolha?ACAO=consultar&cdarquivofolha=" + bean.getWhereIn());
		} else {
			return new ModelAndView("redirect:/rh/crud/Arquivofolha");
		}
	}
	
}
