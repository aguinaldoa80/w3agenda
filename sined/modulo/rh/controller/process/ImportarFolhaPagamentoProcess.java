package br.com.linkcom.sined.modulo.rh.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportarFolhaPagamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportarFolhaPagamentoLinhaBean;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ImportarFolhaPagamentoFiltro;

@Bean
@Controller(path="/rh/process/ImportarFolhaPagamento", authorizationModule=ProcessAuthorizationModule.class)
public class ImportarFolhaPagamentoProcess extends MultiActionController {
	
	private static final int QTDE_COLUNAS_LINHA = 25;
	private static final int POSICAO_PROCESSAMENTO = 0;
	private static final int POSICAO_CENTRO_CUSTO = 2;
	private static final int POSICAO_NOME = 15;
	private static final int POSICAO_CNPJ = 16;
	private static final int POSICAO_CPF = 17;
	private static final int POSICAO_SALARIO = 22;
	private static final int CODIGO_PROCESSAMENTO_1 = 1;
	private static final int CODIGO_PROCESSAMENTO_2 = 2;
	private static final int CODIGO_PROCESSAMENTO_3 = 3;
	private static final int CODIGO_PROCESSAMENTO_4 = 4;
	private static final String DESCRICAO_PROCESSAMENTO_1 = "Adiantamento de sal�rio";
	private static final String DESCRICAO_PROCESSAMENTO_2 = "Pagamento de sal�rio";
	private static final String DESCRICAO_PROCESSAMENTO_3 = "Primeira parcela de 13� sal�rio";
	private static final String DESCRICAO_PROCESSAMENTO_4 = "Segunda parcela de 13� sal�rio";
	private static final String MSG_VALIDAR_ARQUIVO_1 = "Quantidade de campos no arquivo diferente do esperado.";
	private static final String MSG_VALIDAR_ARQUIVO_2 = "Formato incorreto do campo n�mero ";
	private ContagerencialService contagerencialService;
	private CentrocustoService centrocustoService;
	private EmpresaService empresaService;
	private ColaboradorService colaboradorService;
	private ContapagarService contapagarService;
	
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ImportarFolhaPagamentoFiltro filtro) throws Exception {
		//Autocomplete
		if (filtro.getContagerencial()!=null){
			filtro.setContagerencial(contagerencialService.loadForEntrada(filtro.getContagerencial()));
		}
		
		return new ModelAndView("process/importarFolhaPagamento", "filtro", filtro);
	}
	
	public ModelAndView importar(WebRequestContext request, ImportarFolhaPagamentoFiltro filtro) throws Exception {
		if (validarArquivo(filtro)){
			List<ImportarFolhaPagamentoBean> listaBean = getListaBean(filtro);
			if (validarImportar(request, listaBean)){
				contapagarService.saveTransactionImportarFolhaPagamento(filtro, listaBean);
				request.addMessage("Os registros de conta a pagar foram gerados com sucesso.", MessageType.INFO);
				
				filtro.setArquivo(null);
				filtro.setDocumentotipo(null);
				filtro.setDtvencimento(null);
				filtro.setContagerencial(null);
				filtro.setCentrocusto(null);
			}		
		}else {
			if (filtro.getMensagemErro()!=null){
				request.addMessage(filtro.getMensagemErro(), MessageType.ERROR);			
			}else {
				request.addMessage("Arquivo inv�lido.", MessageType.ERROR);			
			}
		}		
		
		return index(request, filtro);
	}
	
	private String[] getLinhas(ImportarFolhaPagamentoFiltro filtro) throws Exception {
		return new String(filtro.getArquivo().getContent()).split("\\r\\n");
	}
	
	private List<ImportarFolhaPagamentoLinhaBean> getListaLinhaBean(ImportarFolhaPagamentoFiltro filtro) throws Exception {
		List<ImportarFolhaPagamentoLinhaBean> listaLinhaBean = new ArrayList<ImportarFolhaPagamentoLinhaBean>();
		
		String[] linhas = getLinhas(filtro);
		
		for (String linha: linhas){
			String[] colunas = linha.split("\\|");
			
			colunas[POSICAO_CNPJ] = colunas[POSICAO_CNPJ].replaceAll("[^0-9]", "");
			colunas[POSICAO_CPF] = colunas[POSICAO_CPF].replaceAll("[^0-9]", "");
			
			ImportarFolhaPagamentoLinhaBean linhaBean = new ImportarFolhaPagamentoLinhaBean();
			linhaBean.setProcessamento(Integer.valueOf(colunas[POSICAO_PROCESSAMENTO]));
			linhaBean.setCodigoCentroCusto(NumberUtils.isNumber(colunas[POSICAO_CENTRO_CUSTO]) ? Integer.valueOf(colunas[POSICAO_CENTRO_CUSTO]) : null);
			linhaBean.setNome(colunas[POSICAO_NOME]);
			linhaBean.setCnpj(Cnpj.cnpjValido(colunas[POSICAO_CNPJ]) ? new Cnpj(colunas[POSICAO_CNPJ]) : null);
			linhaBean.setCpf(Cpf.cpfValido(colunas[POSICAO_CPF]) ? new Cpf(colunas[POSICAO_CPF]) : null);
			linhaBean.setSalario(Long.valueOf(colunas[POSICAO_SALARIO]));
			listaLinhaBean.add(linhaBean);
				
		}
		
		return listaLinhaBean;
	}
	
	private List<ImportarFolhaPagamentoBean> getListaBean(ImportarFolhaPagamentoFiltro filtro) throws Exception {
		List<ImportarFolhaPagamentoBean> listaBean = new ArrayList<ImportarFolhaPagamentoBean>();
		List<ImportarFolhaPagamentoLinhaBean> listaLinhaBean = getListaLinhaBean(filtro);
		List<Integer> listaCodigoCentroCusto = new ArrayList<Integer>();
		List<Cnpj> listaCnpj = new ArrayList<Cnpj>();
		List<Cpf> listaCpf = new ArrayList<Cpf>();
		
		for (ImportarFolhaPagamentoLinhaBean linhaBean: listaLinhaBean){
			if (linhaBean.getCodigoCentroCusto()!=null){
				listaCodigoCentroCusto.add(linhaBean.getCodigoCentroCusto());
			}
			if (linhaBean.getCnpj()!=null){
				listaCnpj.add(linhaBean.getCnpj());
			}
			if (linhaBean.getCpf()!=null){
				listaCpf.add(linhaBean.getCpf());
			}
		}
		
		List<Centrocusto> listaCentrocusto = centrocustoService.findByListaCodigoalternativo(listaCodigoCentroCusto);
		List<Empresa> listaEmpresa = empresaService.findByListaCnpj(listaCnpj);
		List<Colaborador> listaColaborador = colaboradorService.findForImportacaoFolhaPagamento(listaCpf);
		
		for (ImportarFolhaPagamentoLinhaBean linhaBean: listaLinhaBean){
			ImportarFolhaPagamentoBean bean = new ImportarFolhaPagamentoBean();
			bean.setLinhaBean(linhaBean);
			
			//Centro de custo
			if (linhaBean.getCodigoCentroCusto()!=null){
				for (Centrocusto centrocusto: listaCentrocusto){
					if (centrocusto.getCodigoalternativo().equals(linhaBean.getCodigoCentroCusto())){
						bean.setCentrocusto(centrocusto);
						break;
					}
				}
			}
			if (bean.getCentrocusto()==null && filtro.getCentrocusto()!=null){
				bean.setCentrocusto(filtro.getCentrocusto());
			}
			
			//Empresa
			if (linhaBean.getCnpj()!=null){
				for (Empresa empresa: listaEmpresa){
					if (empresa.getCnpj().equals(linhaBean.getCnpj())){
						bean.setEmpresa(empresa);
						break;
					}
				}
			}
			
			//Colaborador e Conta gerencial
			if (linhaBean.getCpf()!=null){
				for (Colaborador colaborador: listaColaborador){
					if (colaborador.getCpf().equals(linhaBean.getCpf())){
						bean.setColaborador(colaborador);
						bean.setContagerencial(new ListSet<Colaboradorcargo>(Colaboradorcargo.class, colaborador.getListaColaboradorcargo()).get(0).getCargo().getContagerencial());
						break;
					}
				}
			}
			if (bean.getContagerencial()==null && filtro.getContagerencial()!=null){
				bean.setContagerencial(filtro.getContagerencial());
			}
			
			//Descri��o
			if (linhaBean.getProcessamento().equals(CODIGO_PROCESSAMENTO_1)){
				bean.setDescricao(DESCRICAO_PROCESSAMENTO_1);
			}else if (linhaBean.getProcessamento().equals(CODIGO_PROCESSAMENTO_2)){
				bean.setDescricao(DESCRICAO_PROCESSAMENTO_2);
			}else if (linhaBean.getProcessamento().equals(CODIGO_PROCESSAMENTO_3)){
				bean.setDescricao(DESCRICAO_PROCESSAMENTO_3);
			}else if (linhaBean.getProcessamento().equals(CODIGO_PROCESSAMENTO_4)){
				bean.setDescricao(DESCRICAO_PROCESSAMENTO_4);
			}
			
			//Valor
			bean.setValor(new Money(linhaBean.getSalario(), true));
			
			listaBean.add(bean);
		}
		
		return listaBean;
	}
	
	private boolean validarArquivo(ImportarFolhaPagamentoFiltro filtro) throws Exception {
		try {
			String[] linhas = getLinhas(filtro);
			
			if (linhas.length>0){
				for (String linha: linhas){
					if (StringUtils.countOccurrencesOf(linha, "|")!=QTDE_COLUNAS_LINHA-1){
						filtro.setMensagemErro(MSG_VALIDAR_ARQUIVO_1);
						return false;					
					}else {
						String[] colunas = linha.split("\\|");
						if (!NumberUtils.isNumber(colunas[POSICAO_PROCESSAMENTO]) || Integer.valueOf(colunas[POSICAO_PROCESSAMENTO])<1 || Integer.valueOf(colunas[POSICAO_PROCESSAMENTO])>4){
							filtro.setMensagemErro(MSG_VALIDAR_ARQUIVO_2 + "1.");
							return false;
						}else if (!colunas[POSICAO_CENTRO_CUSTO].trim().equals("") && (!NumberUtils.isNumber(colunas[POSICAO_CENTRO_CUSTO]) || Integer.valueOf(colunas[POSICAO_CENTRO_CUSTO])<0)){
							filtro.setMensagemErro(MSG_VALIDAR_ARQUIVO_2 + "3.");
							return false;
						}else if (!NumberUtils.isNumber(colunas[POSICAO_SALARIO]) || Integer.valueOf(colunas[POSICAO_SALARIO])<=0){
							filtro.setMensagemErro(MSG_VALIDAR_ARQUIVO_2 + "23.");
							return false;
						}
					}
				}
				
				return true;
			}			
		} catch (Exception e) {
		}
		
		return false;
	}
	
	private boolean validarImportar(WebRequestContext request, List<ImportarFolhaPagamentoBean> listaBean) throws Exception {
		for (ImportarFolhaPagamentoBean bean: listaBean){
			if (bean.getEmpresa()==null){
				request.addMessage("N�o foi poss�vel identificar a Empresa para o registro de " + Util.strings.emptyIfNull(bean.getLinhaBean().getNome()) + ".", MessageType.ERROR);
				return false;
			}else if (bean.getColaborador()==null){
				request.addMessage("N�o foi poss�vel identificar cadastro do colaborador " + Util.strings.emptyIfNull(bean.getLinhaBean().getNome()) + ".", MessageType.ERROR);
				return false;
			}else if (bean.getContagerencial()==null){
				request.addMessage("N�o foi poss�vel identificar a 'Conta gerencial' para o cargo do colaborador " + Util.strings.emptyIfNull(bean.getLinhaBean().getNome()) + ".", MessageType.ERROR);
				return false;
			}else if (bean.getCentrocusto()==null){
				request.addMessage("N�o foi poss�vel identificar o 'Centro de custo' para o registro de " + Util.strings.emptyIfNull(bean.getLinhaBean().getNome()) + ".", MessageType.ERROR);
				return false;
			}
		}
		
		return true;
	}
}