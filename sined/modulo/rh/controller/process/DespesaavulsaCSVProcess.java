package br.com.linkcom.sined.modulo.rh.controller.process;


import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.DespesaavulsarhService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DespesaavulsarhFiltro;

@Controller(
		path={"/rh/process/DespesaavulsaCSV"},
		authorizationModule=ProcessAuthorizationModule.class
)
public class DespesaavulsaCSVProcess extends ResourceSenderController<DespesaavulsarhFiltro>{
	
	private DespesaavulsarhService despesaavulsarhService;
	
	public void setDespesaavulsarhService(DespesaavulsarhService despesaavulsarhService) {this.despesaavulsarhService = despesaavulsarhService;}
	
	@Override
	public Resource generateResource(WebRequestContext request,DespesaavulsarhFiltro filtro) throws Exception {
		return despesaavulsarhService.gerarRelatorioDespesaavulsaCSV(request, filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, DespesaavulsarhFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/rh/crud/Despesaavulsarh?ACAO=" + AbstractCrudController.LISTAGEM);
	}
	
	
}
