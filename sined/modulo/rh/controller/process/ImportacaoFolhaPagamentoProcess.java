package br.com.linkcom.sined.modulo.rh.controller.process;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ColaboradorDespesaMotivoItem;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipovalorreceber;
import br.com.linkcom.sined.geral.service.ColaboradorDespesaMotivoItemService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradordespesaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EventoPagamentoService;
import br.com.linkcom.sined.geral.service.FechamentoFolhaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ImportacaoFolhaPagamentoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoFolhaPagamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoFolhaPagamentoBeanErro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/rh/process/ImportacaoFolhaPagamento", authorizationModule=ProcessAuthorizationModule.class)
public class ImportacaoFolhaPagamentoProcess extends MultiActionController {
	
	private EmpresaService empresaService;
	private ColaboradorService colaboradorService;
	private EventoPagamentoService eventoPagamentoService;
	private ColaboradordespesaService colaboradordespesaService;
	private ColaboradorDespesaMotivoItemService colaboradorDespesaMotivoItemService;
	private FechamentoFolhaService fechamentoFolhaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setEventoPagamentoService(
			EventoPagamentoService eventoPagamentoService) {
		this.eventoPagamentoService = eventoPagamentoService;
	}
	public void setColaboradordespesaService(
			ColaboradordespesaService colaboradordespesaService) {
		this.colaboradordespesaService = colaboradordespesaService;
	}
	public void setColaboradorDespesaMotivoItemService(
			ColaboradorDespesaMotivoItemService colaboradorDespesaMotivoItemService) {
		this.colaboradorDespesaMotivoItemService = colaboradorDespesaMotivoItemService;
	}
	public void setFechamentoFolhaService(
			FechamentoFolhaService fechamentoFolhaService) {
		this.fechamentoFolhaService = fechamentoFolhaService;
	}
	
	@DefaultAction
	@Command(session=false)
	public ModelAndView index(WebRequestContext request, ImportacaoFolhaPagamentoFiltro filtro) {
		filtro.setArquivo(null);
		
		return new ModelAndView("process/importacaoFolhaPagamento", "filtro", filtro);
	}
	
	public ModelAndView carregarArquivo(WebRequestContext request, ImportacaoFolhaPagamentoFiltro filtro){
		if(filtro.getArquivo() == null || filtro.getArquivo().getContent() == null || filtro.getArquivo().getContent().length == 0) {
			throw new SinedException("O arquivo deve ser informado.");		
		} else if(filtro.getArquivo() != null && !filtro.getArquivo().getNome().endsWith(".csv") && !filtro.getArquivo().getNome().endsWith(".CSV")) {
			request.addError("N�o � possivel importar arquivos que n�o estejam no formato CSV.");
			return index(request, filtro);
		}
			
		if(filtro.getTipoRemuneracao() == null || filtro.getTipoRemuneracao().getCdcolaboradordespesamotivo() == null){
			throw new SinedException("Tipo de remunera��o deve ser informado.");
		}
		String strFile = new String(filtro.getArquivo().getContent());
		String[] linhas = strFile.split("\\r?\\n");
		List<String> linhasPreenchidas = new ArrayList<String>();
		for (int i = 1; i < linhas.length; i++) {
			String linha = linhas[i];
			if(linha != null && !linha.replaceAll(";", "").trim().equals("")){
				linhasPreenchidas.add(linha);
			}
		}
		if(linhasPreenchidas.size() == 0){
			throw new SinedException("Nenhum registro a ser importado.");		
		}
		
		List<String> listaErro = new ArrayList<String>();
		Integer numeroLinha = 1;
		List<ImportacaoFolhaPagamentoBean> lista = new ArrayList<ImportacaoFolhaPagamentoBean>();
		List<ImportacaoFolhaPagamentoBeanErro> listaBeanErro = new ArrayList<ImportacaoFolhaPagamentoBeanErro>();
		for (String linha : linhasPreenchidas) {
			if(linha.indexOf("\"") != -1) {
				linha = linha.replace("\"", "");
			}
			
			numeroLinha++;
			ImportacaoFolhaPagamentoBean  bean = new ImportacaoFolhaPagamentoBean();
			String erro = "";
			
			
			String[] campo = linha.split(";");
			String strStatusEvento = null;
			
            String strMatricula = campo[1].trim();
			String strEvento = campo[4].trim();
			String strValor = campo[6].trim();
			String strDtReferencia = campo[2].trim();
			strDtReferencia = ajustaAnoComDoisDigitos(strDtReferencia);
			
			
			Contagerencial contaGerencial = null;
			try {
				strStatusEvento = campo[36].trim();
			} catch (Exception e) {
				continue;
			}
			
			if(!strStatusEvento.equalsIgnoreCase("S")){
				continue;
			}
			String strDtPagamento = campo[31];
			strDtPagamento = ajustaAnoComDoisDigitos(strDtPagamento);
            String cnpjEmpresa = br.com.linkcom.neo.util.StringUtils.soNumero(campo[17].trim());
			Money valor = null;
			EventoPagamento evento = null;
			Cnpj cnpj = null;
			Long matricula = null;
			Date dtPagamento = null;
			Date dtReferencia = null;
			Colaborador colaborador = null;
			Empresa empresa = null;
			Projeto projeto = null;
			Centrocusto centroCusto = null;
			
			try {
				try {
					dtPagamento = SinedDateUtils.stringToDate(strDtPagamento);
				} catch (ParseException e) {
					erro = msgErroLinha(numeroLinha, "Data de pagamento inv�lida.");
					continue;
				}
				try {
					dtReferencia = SinedDateUtils.stringToDate(strDtReferencia);
				} catch (ParseException e) {
					erro = msgErroLinha(numeroLinha, "Data de refer�ncia inv�lida.");
					continue;
				}
				try {
					cnpj = new Cnpj(cnpjEmpresa);
				} catch (Exception e) {
					erro = msgErroLinha(numeroLinha, "CNPJ inv�lido.");
					continue;
				}
				
				empresa = empresaService.carregaEmpresaByCnpj(cnpj);
				if(empresa == null){
					erro = msgErroLinha(numeroLinha, "Empresa n�o encontrada com o CNPJ informado.");
					continue;
				}
				List<ColaboradorDespesaMotivoItem> listaMotivoItem = colaboradorDespesaMotivoItemService.findByMotivo(filtro.getTipoRemuneracao(), empresa);
				if(SinedUtil.isListEmpty(listaMotivoItem)){
					erro = msgErroLinha(numeroLinha, "Conta gerencial n�o cadastrada no tipo de remunera��o para a empresa de cnpj "+cnpjEmpresa+".");
					continue;
				}
				contaGerencial = listaMotivoItem.get(0).getContaGerencial();
				
				try {
					matricula = Long.parseLong(strMatricula);
				} catch (Exception e) {
					erro = msgErroLinha(numeroLinha, "N�mero de matr�cula inv�lido.");
					continue;
				}
				
				colaborador = colaboradorService.findByMatricula(matricula, empresa);
				if(colaborador == null){
					erro = msgErroLinha(numeroLinha, "Colaborador n�o encontrado com a matr�cula informada.");
					continue;
				}
				Colaboradorcargo colaboradorCargo = colaborador.getListaColaboradorcargo().iterator().next();
				if(colaboradorCargo.getCentroCusto() == null){
					erro = msgErroLinha(numeroLinha, "Cargo do Colaborador n�o possui centro de custo cadastrado.");
					continue;
				}
				centroCusto = colaboradorCargo.getCentroCusto();
				projeto = colaboradorCargo.getProjeto();
				
				try {
					evento = eventoPagamentoService.loadForImportarFolhaPagamento(strEvento, empresa);
					
					if(evento == null){
						erro = msgErroLinha(numeroLinha, "Evento n�o encontrado para a empresa "+empresa.getNome()+".");
						continue;
					}
				} catch (Exception e) {
					erro = msgErroLinha(numeroLinha, "C�digo de evento inv�lido.");
					continue;
				}
				
				
				try {
					valor = new Money(strValor.replace(",", "."));
				} catch (Exception e) {
					erro = msgErroLinha(numeroLinha, "Valor inv�lido.");
					continue;
				}
			} catch (Exception e) {
				e.printStackTrace();
				listaErro.add(e.getMessage());
			} finally{
				
				//bean.setArquivo(filtro.getArquivo());
				if(StringUtils.isNotBlank(erro)){
					ImportacaoFolhaPagamentoBeanErro beanErro = new ImportacaoFolhaPagamentoBeanErro();
					beanErro.setMatricula(strMatricula);
					beanErro.setCnpj(cnpjEmpresa);
					beanErro.setDtPagamento(strDtPagamento);
					beanErro.setDtReferencia(strDtReferencia);
					beanErro.setEvento(strEvento);
					beanErro.setValor(strValor);
					beanErro.setErro(erro);
					listaBeanErro.add(beanErro);	
				}else{
					bean.setColaborador(colaborador);
					bean.setEmpresa(empresa);
					bean.setContaGerencial(contaGerencial);
					bean.setProjeto(projeto);
					bean.setCentroCusto(centroCusto);
					bean.setDtPagamento(dtPagamento);
					bean.setDtReferencia(dtReferencia);
					bean.setEvento(evento);
					bean.setTipoRemuneracao(filtro.getTipoRemuneracao());
					bean.setValor(valor);
					lista.add(bean);
				}
			}
		}
		filtro.setListaBean(lista);
		filtro.setListaBeanComErro(listaBeanErro);
		
		if(listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			return sendRedirectToAction("index");
		}
		String identificadorSessao = SinedDateUtils.currentTimestamp().toString();
		request.getSession().setAttribute("importacaofolhapagamento_" + identificadorSessao, filtro);
		request.setAttribute("identificador", "importacaofolhapagamento_" + identificadorSessao);
		
		return new ModelAndView("process/importacaoFolhaPagamentoListagem", "filtro", filtro);
	}
	
	private String ajustaAnoComDoisDigitos(String data){
		String result = data;
		if(data.split("/").length > 1 && data.split("/")[2].length()==2){
			result = data.split("/")[0]+"/"+
					data.split("/")[1]+"/20"+
					data.split("/")[2];
		}
		return result;
	}
	
	private String msgErroLinha(Integer linha, String msg){
		return "Linha "+linha.toString()+". "+msg;
	}
	
	public ModelAndView save(WebRequestContext request, ImportacaoFolhaPagamentoFiltro filtro){
		String identificador = request.getParameter("identificador");
		
		ImportacaoFolhaPagamentoFiltro  objFiltro = (ImportacaoFolhaPagamentoFiltro) request.getSession().getAttribute(identificador);
		if(SinedUtil.isListEmpty(objFiltro.getListaBean())){
			throw new SinedException("N�o existem item a serem importados."); 
		}
		
		for (ImportacaoFolhaPagamentoBean itemFolha : objFiltro.getListaBean()) {
			if (fechamentoFolhaService.existeFechamentoPeriodo(itemFolha.getEmpresa(), itemFolha.getDtReferencia())){
				request.addError("N�o foi poss�vel importar. J� existe um fechamento de folha correspondente � data informada.");
				return sendRedirectToAction("index");
			}
		}
		
		HashMap<Colaborador, List<ImportacaoFolhaPagamentoBean>> mapa = criaMapaValoresPorColaborador(objFiltro.getListaBean());
		
		List<Colaboradordespesa> listaDespesa = agrupaDespesasPorColaboradorAndData(mapa, objFiltro.getTipoRemuneracao());
		
		ajustaTotalizacao(listaDespesa);
		
		colaboradordespesaService.saveOrUpdateList(listaDespesa);
		
		request.addMessage("Importa��o realizada com sucesso!");
		return sendRedirectToAction("index");
	}
	
	private HashMap<Colaborador, List<ImportacaoFolhaPagamentoBean>> criaMapaValoresPorColaborador(List<ImportacaoFolhaPagamentoBean> lista){
		HashMap<Colaborador, List<ImportacaoFolhaPagamentoBean>> mapa = new HashMap<Colaborador, List<ImportacaoFolhaPagamentoBean>>();
		for(ImportacaoFolhaPagamentoBean bean: lista){
			if(!mapa.containsKey(bean.getColaborador())){
				mapa.put(bean.getColaborador(), new ArrayList<ImportacaoFolhaPagamentoBean>());
			}
			mapa.get(bean.getColaborador()).add(bean);
		}
		return mapa;
	}
	
	private List<Colaboradordespesa> agrupaDespesasPorColaboradorAndData(HashMap<Colaborador, List<ImportacaoFolhaPagamentoBean>> mapa, Colaboradordespesamotivo tipoRemuneracao){
		List<Colaboradordespesa> listaDespesa = new ArrayList<Colaboradordespesa>();
		for(Colaborador colaborador: mapa.keySet()){
			List<ImportacaoFolhaPagamentoBean> lista = mapa.get(colaborador);
			Collections.sort(lista, new Comparator<ImportacaoFolhaPagamentoBean>() {

				@Override
				public int compare(ImportacaoFolhaPagamentoBean o1,
						ImportacaoFolhaPagamentoBean o2) {
					return o1.getDtReferencia().compareTo(o2.getDtReferencia());
				}
				
			});
			Colaboradordespesa despesa = criaColaboradorDespesa(lista.get(0), tipoRemuneracao);
			Date dtReferencia = lista.get(0).getDtReferencia();
			Date dtPagamento = lista.get(0).getDtPagamento();
			listaDespesa.add(despesa);
			for(ImportacaoFolhaPagamentoBean bean: lista){
				if(SinedDateUtils.afterIgnoreHour(bean.getDtReferencia(), dtReferencia) || !SinedDateUtils.equalsIgnoreHour(dtPagamento, bean.getDtPagamento())){
					dtReferencia = bean.getDtReferencia();
					despesa = criaColaboradorDespesa(bean, tipoRemuneracao);
					listaDespesa.add(despesa);					
				}
				Colaboradordespesaitem item = new Colaboradordespesaitem();
				item.setDtaltera(SinedDateUtils.currentTimestamp());
				item.setEvento(bean.getEvento());
				item.setValor(bean.getValor());

				despesa.getListaColaboradordespesaitem().add(item);
			}
		}
		return listaDespesa;
	}
	
	private void ajustaTotalizacao(List<Colaboradordespesa> listaDespesa){
		for(Colaboradordespesa despesa: listaDespesa){
			Double total = 0.0;
			for(Colaboradordespesaitem item: despesa.getListaColaboradordespesaitem()){
				if(Colaboradordespesaitemtipo.CREDITO.equals(item.getEvento().getTipoEvento())){
					total += item.getValor().getValue().doubleValue();
				}else if(true){
					total -= item.getValor().getValue().doubleValue();
				}
			}
			despesa.setTotal(new Money(total));
			if(total < 0){
				despesa.setTipovalorreceber(Tipovalorreceber.CONTA_RECEBER);
			}
			despesa.getRateio().getListaRateioitem().get(0).setValor(new Money(Math.abs(total)));
		}
	}
	
	private Colaboradordespesa criaColaboradorDespesa(ImportacaoFolhaPagamentoBean bean, Colaboradordespesamotivo motivo){
		Colaboradordespesa despesa = new Colaboradordespesa();
		despesa.setColaborador(bean.getColaborador());
		despesa.setEmpresa(bean.getEmpresa());
		despesa.setDtdeposito(bean.getDtPagamento() != null ? bean.getDtPagamento() : bean.getDtReferencia());
		despesa.setDtinsercao(bean.getDtReferencia());
		despesa.setColaboradordespesamotivo(motivo);
		despesa.setSituacao(Colaboradordespesasituacao.EM_ABERTO);
		despesa.setListaColaboradordespesaitem(new ArrayList<Colaboradordespesaitem>());
		despesa.setRateio(new Rateio());
		despesa.getRateio().setListaRateioitem(new ArrayList<Rateioitem>());
		
		Rateioitem ri = new Rateioitem();
		ri.setValor(new Money());
		ri.setContagerencial(bean.getContaGerencial());
		ri.setCentrocusto(bean.getCentroCusto());
		ri.setProjeto(bean.getProjeto());
		despesa.getRateio().getListaRateioitem().add(ri);
		
		return despesa;
	}
}
