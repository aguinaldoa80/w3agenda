package br.com.linkcom.sined.modulo.rh.controller.process;


import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.service.ColaboradordespesaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf.ArquivoDIRF;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf.ArquivoDIRFBPFDEC;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf.ArquivoDIRFDECPJ;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf.ArquivoDIRFIDREC;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf.ArquivoDIRFRESPO;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.arquivodirf.ArquivoDIRFValoresMensais;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.GerarArquivoDIRFFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/rh/process/GerarArquivoDIRF", authorizationModule=ProcessAuthorizationModule.class)
public class GerarArquivoDIRFProcess extends MultiActionController {
	
	private EmpresaService empresaService;
	private ColaboradordespesaService colaboradordespesaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}

	public void setColaboradordespesaService(
			ColaboradordespesaService colaboradordespesaService) {
		this.colaboradordespesaService = colaboradordespesaService;
	}

	@DefaultAction
	public ModelAndView index(WebRequestContext request, GerarArquivoDIRFFiltro filtro) throws Exception {
		return new ModelAndView("process/gerarArquivoDIRF", "filtro", filtro);
	}
	
	public ModelAndView gerarArquivo(WebRequestContext request, GerarArquivoDIRFFiltro filtro) throws Exception {
		
		try {
			Set<String> listaErro = new ListSet<String>(String.class);
			
			Empresa empresa = empresaService.loadForArquivoDIRF(filtro.getEmpresa());
			Integer ano = filtro.getAno();
			Colaboradordespesamotivo colaboradordespesamotivo = filtro.getColaboradordespesamotivo();
			
			Calendar calendarInicio = Calendar.getInstance();
			calendarInicio.set(Calendar.DAY_OF_MONTH, 1);
			calendarInicio.set(Calendar.MONTH, 0);
			calendarInicio.set(Calendar.YEAR, ano);
			Date dtinicio = new Date(calendarInicio.getTimeInMillis());
			
			Calendar calendarFim = Calendar.getInstance();
			calendarFim.set(Calendar.DAY_OF_MONTH, 31);
			calendarFim.set(Calendar.MONTH, 11);
			calendarFim.set(Calendar.YEAR, ano);
			Date dtfim = new Date(calendarFim.getTimeInMillis());
			
			List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForArquivoDIRF(empresa, dtinicio, dtfim, colaboradordespesamotivo);
			
			ArquivoDIRF arquivoDIRF = new ArquivoDIRF();
			
			Calendar calendarAtual = Calendar.getInstance();
			calendarAtual.setTimeInMillis(System.currentTimeMillis());
			Integer anoreferencia = calendarAtual.get(Calendar.YEAR);
			
			arquivoDIRF.setAnoreferencia(anoreferencia);
			arquivoDIRF.setAnocalendario(ano);
			arquivoDIRF.setIndicadorRetificadora("N");
			arquivoDIRF.setNumeroRecibo(null);
			if(anoreferencia.equals(2013)){
				arquivoDIRF.setIdentificadorEstruturaLeiaute("7C2DE7J");
			} else {
				listaErro.add("Leiaute do ano refer�ncia n�o encontrado.");
			}
			
			ArquivoDIRFRESPO arquivoDIRFRESPO = new ArquivoDIRFRESPO();
			arquivoDIRF.setArquivoDIRFRESPO(arquivoDIRFRESPO);
			
			Fornecedor colaboradorcontabilista = empresa.getColaboradorcontabilista();
			if(colaboradorcontabilista == null){
				listaErro.add("Contabilista da empresa n�o encontrado.");
			} else {
				if(colaboradorcontabilista.getCpf() != null){
					arquivoDIRFRESPO.setCpf(colaboradorcontabilista.getCpf().getValue());
				} else {
					listaErro.add("CPF do contanilista n�o encontrado");
				}
				arquivoDIRFRESPO.setNome(colaboradorcontabilista.getNome());
				arquivoDIRFRESPO.setRamal(null);
				
				if(colaboradorcontabilista.getEmail() != null){
					arquivoDIRFRESPO.setCorreioEletronico(colaboradorcontabilista.getEmail());
				} else {
					listaErro.add("E-mail do contanilista n�o encontrado");
				}
				
				Telefone telefone = colaboradorcontabilista.getTelefone();
				if(telefone != null){
					String telStr = telefone.getTelefone();
					if(telStr != null){
						telStr = StringUtils.soNumero(telStr);
						
						if(telStr.startsWith("0")){
							telStr = telStr.substring(1, telStr.length());
						}
						
						if(telStr.length() == 10 || telStr.length() == 11){
							String dddStr = telStr.substring(0, 2);
							telStr = telStr.substring(2); 
								
							arquivoDIRFRESPO.setDdd(dddStr);
							arquivoDIRFRESPO.setTelefone(telStr);
						} else listaErro.add("Telefone do contanilista inv�lido, formato (XX) XXXX-XXXX ou (XX) XXXXX-XXXX.");
					} else listaErro.add("Telefone do contabilista n�o encontrado.");
				} else listaErro.add("Telefone do contabilista n�o encontrado.");
				
				Telefone telefoneFax = colaboradorcontabilista.getTelefoneFax();
				if(telefoneFax != null){
					String telStr = telefoneFax.getTelefone();
					if(telStr != null){
						telStr = StringUtils.soNumero(telStr);
						
						if(telStr.startsWith("0")){
							telStr = telStr.substring(1, telStr.length());
						}
						
						if(telStr.length() == 10 || telStr.length() == 11){
							telStr = telStr.substring(2); 
							arquivoDIRFRESPO.setFax(telStr);
						}
					}
				}
			}
			
			ArquivoDIRFDECPJ arquivoDIRFDECPJ = new ArquivoDIRFDECPJ();
			arquivoDIRF.setArquivoDIRFDECPJ(arquivoDIRFDECPJ);
			
			Colaborador responsavel = empresa.getResponsavel();
			if(responsavel != null){
				if(responsavel.getCpf() != null){
					arquivoDIRFDECPJ.setCpfResponsavel(responsavel.getCpf().getValue());
				} else listaErro.add("CPF do respons�vel da empresa n�o encontrado.");
			} else listaErro.add("Respons�vel da empresa n�o encontrado.");
			
			arquivoDIRFDECPJ.setCnpj(empresa.getCnpj().getValue());
			arquivoDIRFDECPJ.setNomeEmpresarial(empresa.getRazaosocialOuNome());
			arquivoDIRFDECPJ.setNaturezaDeclarante(0);
			arquivoDIRFDECPJ.setIndicadorSocioOstensivo("N");
			arquivoDIRFDECPJ.setIndicadorDeclaranteDepositario("N");
			arquivoDIRFDECPJ.setIndicadorDeclaranteInstituicaoAdministradora("N");
			arquivoDIRFDECPJ.setIndicadorDeclaranteRedimentosPagosResidentes("N");
			arquivoDIRFDECPJ.setIndicadorPlanoPrivadoAssistenciaSaude("N");
			arquivoDIRFDECPJ.setIndicadorPagamentosCopa("N");
			arquivoDIRFDECPJ.setIndicadorSituacaoEspecial("N");
			arquivoDIRFDECPJ.setDataevento(null);
			
			List<ArquivoDIRFIDREC> listaArquivoDIRFIDREC = new ArrayList<ArquivoDIRFIDREC>();
			arquivoDIRFDECPJ.setListaArquivoDIRFIDREC(listaArquivoDIRFIDREC);
			
			ArquivoDIRFIDREC arquivoDIRFIDREC = new ArquivoDIRFIDREC();
			listaArquivoDIRFIDREC.add(arquivoDIRFIDREC);
			
			arquivoDIRFIDREC.setCodigoReceita("0588");
			
			List<ArquivoDIRFBPFDEC> listaArquivoDIRFBPFDEC = new ArrayList<ArquivoDIRFBPFDEC>();
			arquivoDIRFIDREC.setListaArquivoDIRFBPFDEC(listaArquivoDIRFBPFDEC);
			
			for (Colaboradordespesa colaboradordespesa : listaColaboradordespesa) {
				Colaborador colaborador = colaboradordespesa.getColaborador();
				Date dataDespesa = colaboradordespesa.getDtinsercao();
				
				if(colaborador == null || colaborador.getNome() == null){
					continue;
				}
				
				if(colaborador.getCpf() == null){
					listaErro.add("CPF do colaborador '" + colaborador.getNome() + "' n�o encontrado.");
					continue;
				}

				if(dataDespesa == null){
					listaErro.add("Data da despesa do colaborador '" + colaborador.getNome() + "' n�o encontrado.");
					continue;
				}
				
				ArquivoDIRFBPFDEC arquivoDIRFBPFDEC = new ArquivoDIRFBPFDEC();
				arquivoDIRFBPFDEC.setNome(colaborador.getNome());
				arquivoDIRFBPFDEC.setCpf(colaborador.getCpf().getValue());
				
				if(listaArquivoDIRFBPFDEC.contains(arquivoDIRFBPFDEC)){
					arquivoDIRFBPFDEC = listaArquivoDIRFBPFDEC.get(listaArquivoDIRFBPFDEC.indexOf(arquivoDIRFBPFDEC));
				} else {
					listaArquivoDIRFBPFDEC.add(arquivoDIRFBPFDEC);
				}
				
				Money valorRendimentoTributavel = new Money();
				Money valorInss = new Money();
				Money valorIr = new Money();
				
				List<Colaboradordespesaitem> listaColaboradordespesaitem = colaboradordespesa.getListaColaboradordespesaitem();
				for (Colaboradordespesaitem colaboradordespesaitem : listaColaboradordespesaitem) {
					if(colaboradordespesaitem.getMotivo() != null && colaboradordespesaitem.getValor() != null){
						if(colaboradordespesaitem.getMotivo().equals("CORRIDA")){
							valorRendimentoTributavel = valorRendimentoTributavel.add(colaboradordespesaitem.getValor());
						} else if(colaboradordespesaitem.getMotivo().equals("IR")){
							valorIr = valorIr.add(colaboradordespesaitem.getValor());
						} else if(colaboradordespesaitem.getMotivo().equals("INSS")){
							valorInss = valorInss.add(colaboradordespesaitem.getValor());
						}
					}
				}
				
				if(valorRendimentoTributavel != null && valorRendimentoTributavel.toLong() > 0){
					ArquivoDIRFValoresMensais arquivoDIRFRTRT = arquivoDIRFBPFDEC.getArquivoDIRFRTRT();
					if(arquivoDIRFRTRT == null){
						arquivoDIRFRTRT = new ArquivoDIRFValoresMensais();
						arquivoDIRFRTRT.setIdentificadorRegistro("RTRT");
						arquivoDIRFBPFDEC.setArquivoDIRFRTRT(arquivoDIRFRTRT);
					}
					
					this.addValorMensal(dataDespesa, valorRendimentoTributavel, arquivoDIRFRTRT);
				}
				
				if(valorInss != null && valorInss.toLong() > 0){
					ArquivoDIRFValoresMensais arquivoDIRFRTPO = arquivoDIRFBPFDEC.getArquivoDIRFRTPO();
					if(arquivoDIRFRTPO == null){
						arquivoDIRFRTPO = new ArquivoDIRFValoresMensais();
						arquivoDIRFRTPO.setIdentificadorRegistro("RTPO");
						arquivoDIRFBPFDEC.setArquivoDIRFRTPO(arquivoDIRFRTPO);
					}
					
					this.addValorMensal(dataDespesa, valorInss, arquivoDIRFRTPO);
				}
				
				if(valorIr != null && valorIr.toLong() > 0){
					ArquivoDIRFValoresMensais arquivoDIRFRTIRF = arquivoDIRFBPFDEC.getArquivoDIRFRTIRF();
					if(arquivoDIRFRTIRF == null){
						arquivoDIRFRTIRF = new ArquivoDIRFValoresMensais();
						arquivoDIRFRTIRF.setIdentificadorRegistro("RTIRF");
						arquivoDIRFBPFDEC.setArquivoDIRFRTIRF(arquivoDIRFRTIRF);
					}
					
					this.addValorMensal(dataDespesa, valorIr, arquivoDIRFRTIRF);
				}
			}
			
			Collections.sort(listaArquivoDIRFBPFDEC, new Comparator<ArquivoDIRFBPFDEC>(){
				public int compare(ArquivoDIRFBPFDEC o1, ArquivoDIRFBPFDEC o2) {
					return o1.getCpf().compareTo(o2.getCpf());
				}
			});
			
			Collections.sort(listaArquivoDIRFIDREC, new Comparator<ArquivoDIRFIDREC>(){
				public int compare(ArquivoDIRFIDREC o1, ArquivoDIRFIDREC o2) {
					return o1.getCodigoReceita().compareTo(o2.getCodigoReceita());
				}
			});
			
			if(listaErro.size() > 0){
				request.addError("Erro na gera��o do arquivo DIRF");
				for (String erro : listaErro) {
					request.addError(erro);
				}
				return sendRedirectToAction("index");
			}
			
			String retorno = Util.strings.tiraAcento(arquivoDIRF.toString().toUpperCase());
			
			Resource resource = new Resource();
			resource.setContentType("text/plain");
			resource.setFileName("DIRF_" + SinedUtil.datePatternForReport() + ".DEC");
		    resource.setContents(retorno.getBytes());
			
		    return new ResourceModelAndView(resource);
		} catch (Exception e) {
			request.addError("Erro na gera��o do arquivo: " + e.getMessage());
			e.printStackTrace();
			return sendRedirectToAction("index");
		}
	}

	private void addValorMensal(Date dataDespesa, Money valor,
			ArquivoDIRFValoresMensais arquivoDIRFValoresMensais) {
		int mes = SinedDateUtils.getDateProperty(dataDespesa, Calendar.MONTH);
		switch (mes) {
			case 0:
				if(arquivoDIRFValoresMensais.getJaneiro() != null){
					arquivoDIRFValoresMensais.setJaneiro(arquivoDIRFValoresMensais.getJaneiro() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setJaneiro(valor.toLong());
				}
				break;

			case 1:
				if(arquivoDIRFValoresMensais.getFevereiro() != null){
					arquivoDIRFValoresMensais.setFevereiro(arquivoDIRFValoresMensais.getFevereiro() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setFevereiro(valor.toLong());
				}
				break;
				
			case 2:
				if(arquivoDIRFValoresMensais.getMarco() != null){
					arquivoDIRFValoresMensais.setMarco(arquivoDIRFValoresMensais.getMarco() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setMarco(valor.toLong());
				}
				break;
				
			case 3:
				if(arquivoDIRFValoresMensais.getAbril() != null){
					arquivoDIRFValoresMensais.setAbril(arquivoDIRFValoresMensais.getAbril() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setAbril(valor.toLong());
				}
				break;
				
			case 4:
				if(arquivoDIRFValoresMensais.getMaio() != null){
					arquivoDIRFValoresMensais.setMaio(arquivoDIRFValoresMensais.getMaio() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setMaio(valor.toLong());
				}
				break;
				
			case 5:
				if(arquivoDIRFValoresMensais.getJunho() != null){
					arquivoDIRFValoresMensais.setJunho(arquivoDIRFValoresMensais.getJunho() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setJunho(valor.toLong());
				}
				break;
				
			case 6:
				if(arquivoDIRFValoresMensais.getJulho() != null){
					arquivoDIRFValoresMensais.setJulho(arquivoDIRFValoresMensais.getJulho() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setJulho(valor.toLong());
				}
				break;
				
			case 7:
				if(arquivoDIRFValoresMensais.getAgosto() != null){
					arquivoDIRFValoresMensais.setAgosto(arquivoDIRFValoresMensais.getAgosto() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setAgosto(valor.toLong());
				}
				break;
				
			case 8:
				if(arquivoDIRFValoresMensais.getSetembro() != null){
					arquivoDIRFValoresMensais.setSetembro(arquivoDIRFValoresMensais.getSetembro() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setSetembro(valor.toLong());
				}
				break;
				
			case 9:
				if(arquivoDIRFValoresMensais.getOutubro() != null){
					arquivoDIRFValoresMensais.setOutubro(arquivoDIRFValoresMensais.getOutubro() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setOutubro(valor.toLong());
				}
				break;
				
			case 10:
				if(arquivoDIRFValoresMensais.getNovembro() != null){
					arquivoDIRFValoresMensais.setNovembro(arquivoDIRFValoresMensais.getNovembro() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setNovembro(valor.toLong());
				}
				break;
				
			case 11:
				if(arquivoDIRFValoresMensais.getDezembro() != null){
					arquivoDIRFValoresMensais.setDezembro(arquivoDIRFValoresMensais.getDezembro() + valor.toLong());
				} else {
					arquivoDIRFValoresMensais.setDezembro(valor.toLong());
				}
				break;
				
				
			default:
				break;
		}
	}
	
}