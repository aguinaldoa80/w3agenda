package br.com.linkcom.sined.modulo.rh.controller.process;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargohistorico;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradorcargohistoricoacao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradorcargoService;
import br.com.linkcom.sined.geral.service.ColaboradorcargohistoricoService;
import br.com.linkcom.sined.geral.service.ColaboradorsituacaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.AlterarsituacaocolaboradorFilter;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;


@Bean
@Controller(
		path="/rh/process/Alterarsituacaocolaborador",
		authorizationModule=ProcessAuthorizationModule.class
)
public class AlterarsituacaocolaboradorProcess extends MultiActionController {

	private ColaboradorService colaboradorService;
	private ColaboradorcargoService colaboradorcargoService;
	private ColaboradorsituacaoService colaboradorsituacaoService;
	private UsuarioService usuarioService;
	private ColaboradorcargohistoricoService colaboradorcargohistoricoService;
	
	public void setColaboradorcargohistoricoService(
			ColaboradorcargohistoricoService colaboradorcargohistoricoService) {
		this.colaboradorcargohistoricoService = colaboradorcargohistoricoService;
	}
	public void setColaboradorsituacaoService(
			ColaboradorsituacaoService colaboradorsituacaoService) {
		this.colaboradorsituacaoService = colaboradorsituacaoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}

	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {
		this.colaboradorcargoService = colaboradorcargoService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	/**
	 * M�todo default do controler, carrega o filtro no jsp
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Jo�o Paulo Zica
	 */
	@DefaultAction
	public ModelAndView alterar(WebRequestContext request, AlterarsituacaocolaboradorFilter filtro) throws Exception {
		if(request.getSession().getAttribute("AlterarsituacaocolaboradorFilter") != null){
			filtro = (AlterarsituacaocolaboradorFilter) request.getSession().getAttribute("AlterarsituacaocolaboradorFilter");
		}
		request.setAttribute("hoje", SinedDateUtils.toString(new Date()));
		request.setAttribute("listaSituacoes", colaboradorsituacaoService.findAll());
		return new ModelAndView("process/alterarsituacaocolaborador", "filtro", filtro);
	}

	/**
	 * M�todo para carregar listagem do filtro no jsp via ajax.
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findColaboradorAltera
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @author Jo�o Paulo Zica
	 */
	public ModelAndView listar(WebRequestContext request, AlterarsituacaocolaboradorFilter filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		request.getSession().setAttribute("AlterarsituacaocolaboradorFilter", filtro);
		
		List<Colaboradorcargo> listaColaborador = colaboradorcargoService.findColaboradorAltera(filtro);
		return new ModelAndView("direct:ajax/alterarsituacaocolaboradorlistagem", "listaColaborador", listaColaborador);
	}

	/**
	 * M�todo para atualizar os dados alterados do colaborador nas tabelas correspondentes.
	 *
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#updateColaborador(Colaborador)
	 * @param request
	 * @param colaborador
	 * @author Jo�o Paulo Zica
	 */
	public void recebedados(WebRequestContext request,Colaborador colaborador){
		if (colaborador == null) {
			throw new SinedException("O par�metro colaborador n�o pode ser null.");
		}
		String[] cds = request.getParameter("cds").split(",");
		//Colaboradorsituacao colaboradorsituacao =  new Colaboradorsituacao(Integer.parseInt(request.getParameter("colaboradorsituacao")));
		List<String> nomes = new ArrayList<String>();
		int i = 0;
		
		if(cds!=null && cds.length!=0){
			for (String cd : cds) {
				colaborador.setCdpessoa(Integer.parseInt(cd));
				
				Colaborador load = colaboradorService.load(colaborador,"colaborador.cdpessoa,colaborador.nome,colaborador.recontratar,colaborador.colaboradorsituacao");
				if(load == null) continue;
				
				Boolean estavademitido = load.getColaboradorsituacao().getCdcolaboradorsituacao().equals(Colaboradorsituacao.DEMITIDO);
				Boolean recontratar = load.getRecontratar();
				
				if(colaborador != null && colaborador.getCdpessoa() != null && colaborador.getColaboradorsituacao() != null){
					
					// se alterar a situacao, apagar o vencimento da situacao
					if(!load.getColaboradorsituacao().equals(colaborador.getColaboradorsituacao())) {
						if(colaborador.getVencimentosituacao() == null){
							colaborador.setApagaVencimento(true);
						}
						
						//grava no hist�rico do cargo do colaborador
						Colaboradorcargo colaboradorcargo = colaboradorcargoService.findCargoAtual(load);
						if(colaboradorcargo != null && colaboradorcargo.getCdcolaboradorcargo() != null){
							Colaboradorcargohistorico colaboradorcargohistorico = new Colaboradorcargohistorico();
							
							colaboradorcargohistorico.setColaboradorcargo(colaboradorcargo);
							colaboradorcargohistorico.setAcao(Colaboradorcargohistoricoacao.ALTERADO);
							
							colaboradorcargohistoricoService.saveOrUpdate(colaboradorcargohistorico);
						}
					}
					
					if(colaborador.getColaboradorsituacao().getCdcolaboradorsituacao().equals(Colaboradorsituacao.NORMAL) && !recontratar){
						nomes.add(load.getNome());
						i++;
					}else{
						if(cds.length == 1 && estavademitido && colaborador.getColaboradorsituacao().getCdcolaboradorsituacao().equals(Colaboradorsituacao.NORMAL)){
							colaborador.setCdpessoa(Integer.parseInt(cd));
							colaborador.setAlterardtinicio(Boolean.TRUE);
						}
						
						colaboradorService.updateColaborador(colaborador);
						if(colaborador.getColaboradorsituacao().getCdcolaboradorsituacao() == 6 && colaborador.getDtdemissao() != null){
							colaboradorcargoService.demissaoColaborador(colaborador);
							usuarioService.updateBloqueado(colaborador.getCdpessoa());
						}
					}
				}
			}
		}
		colaborador.setObservacao(manipulaMensagem(nomes));
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().convertObjectToJs(colaborador, "colaborador");
	}
	
	/**
	 * M�todo para manipular a mensagem de notifica��o da tela de alterar situa��o.
	 * 
	 * @param colaboradores
	 * @return String com a frase
	 * @author Fl�vio Tavares
	 */
	private String manipulaMensagem(List<String> colaboradores){
		
		if(colaboradores.size()==0) return "";
		else if(colaboradores.size()==1) return "O colaborador "+colaboradores.get(0)+" n�o pode ser recontratado(a). Consultar perfil do colaborador.";
		else{
			StringBuilder msg = new StringBuilder("Os colaboradores "); 
			for (String string : colaboradores) {
				msg.append(string+", ");
			}
			msg.delete(msg.lastIndexOf(","), msg.length());
			msg.replace(msg.lastIndexOf(","), msg.lastIndexOf(",")+1, " e");
			msg.append(" n�o podem ser recontratados. Consultar perfil dos colaboradores.");
			return msg.toString();
		}
	}
}