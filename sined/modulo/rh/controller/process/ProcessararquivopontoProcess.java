package br.com.linkcom.sined.modulo.rh.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoponto;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.auxiliar.ColaboradorVO;
import br.com.linkcom.sined.geral.bean.auxiliar.PeriodoVO;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivopontoService;
import br.com.linkcom.sined.geral.service.CargoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoArquivopontoBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoArquivopontoItemBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoSalarioBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/rh/process/Processararquivoponto", authorizationModule=ProcessAuthorizationModule.class)
public class ProcessararquivopontoProcess extends MultiActionController {
	
	private ArquivopontoService arquivopontoService;
	private ArquivoService arquivoService;
	private ColaboradorService colaboradorService;
	private ApontamentoService apontamentoService;
	private PlanejamentoService planejamentoService;
	private CargoService cargoService;
	
	public void setCargoService(CargoService cargoService) {
		this.cargoService = cargoService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setApontamentoService(ApontamentoService apontamentoService) {
		this.apontamentoService = apontamentoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivopontoService(ArquivopontoService arquivopontoService) {
		this.arquivopontoService = arquivopontoService;
	}
	

	/**
	 * Salva as informa��es do processamento do arquivo de ponto.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivopontoService#updateProcessamento(String ids)
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#findAutorizadosByProjeto(Projeto projeto)
	 * @see br.com.linkcom.sined.util.SinedDateUtils#hoursToDouble(String tmpHours) 
	 * @see br.com.linkcom.sined.modulo.rh.controller.process.ProcessararquivopontoProcess#makeMapSalarios(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.CargoService#findByMatricula(Integer matricula)
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView saveProcessamento(WebRequestContext request, ImportacaoArquivopontoBean bean){
		
		Apontamento apontamento;
		Projeto projeto;
		List<Planejamento> listaPlanejamentos;
		List<Apontamento> listaApontamento = new ArrayList<Apontamento>();
		List<ImportacaoSalarioBean> listaSalarios;
		ImportacaoSalarioBean salarioBean;
		Cargo cargo;
		Double salarioDbl;
		Integer matricula;
		
		Map<Integer, List<ImportacaoSalarioBean>> map = this.makeMapSalarios(bean.getWhereIn());
		
		List<ImportacaoArquivopontoItemBean> listaItens = bean.getListaItens();
		for (ImportacaoArquivopontoItemBean item : listaItens) {
			if(item.getMarcado() != null && item.getMarcado()){
				
				apontamento = new Apontamento();
				apontamento.setDtapontamento(item.getData());
				apontamento.setColaborador(item.getColaborador());
				
				projeto = item.getProjeto() != null ? item.getProjeto() : bean.getProjeto();
				listaPlanejamentos = planejamentoService.findAutorizadosByProjeto(projeto);
				if(listaPlanejamentos == null || listaPlanejamentos.size() == 0){
					request.addError("Projeto sem planejamento autorizado.");
					if(bean.getEntrada()){
						return new ModelAndView("redirect:/rh/crud/Arquivoponto?ACAO=consultar&cdarquivoponto=" + bean.getWhereIn());
					} else {
						return new ModelAndView("redirect:/rh/crud/Arquivoponto");
					}
				}
						
				apontamento.setPlanejamento(listaPlanejamentos.get(0));				
				apontamento.setApontamentoTipo(ApontamentoTipo.FUNCAO_HORA);
				apontamento.setQtdehoras(SinedDateUtils.hoursToDouble(item.getHoratotal().toString()));
				apontamento.setSomentetotalhoras(Boolean.TRUE);
				apontamento.setListaApontamentoHoras(new ArrayList<ApontamentoHoras>());									
				
				if(item.getHoraEntrada()!=null && item.getHoraSaida()!=null){
					ApontamentoHoras apontamentoHoras = new ApontamentoHoras();
					apontamentoHoras.setHrinicio(item.getHoraEntrada());
					apontamentoHoras.setHrfim(item.getHoraSaida());
					apontamento.getListaApontamentoHoras().add(apontamentoHoras);
				}				
				
				try{
					matricula = Integer.parseInt(item.getMatricula());
					cargo = cargoService.findByMatricula(matricula);
					listaSalarios = map.get(item.getCdarquivoponto());
					salarioBean = new ImportacaoSalarioBean(matricula);
					if(listaSalarios != null && listaSalarios.size() > 0 && listaSalarios.contains(salarioBean)){
						salarioBean = listaSalarios.get(listaSalarios.indexOf(salarioBean));
						
						apontamento.setTipocargo(salarioBean.getTipocargo());
						if(cargo != null && cargo.getTotalhorasemana() != null && cargo.getTotalhorasemana() > 0 && salarioBean.getSalario() != null){
							salarioDbl = salarioBean.getSalario().getValue().doubleValue() / (cargo.getTotalhorasemana() * 4d);
							apontamento.setValorhora(new Money(salarioDbl));
						}
					} else if(cargo != null){
						apontamento.setTipocargo(cargo.getTipocargo());
						apontamento.setValorhora(cargo.getCustohora());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				listaApontamento.add(apontamento);
			}
		}
		
		for (Apontamento ap : listaApontamento) {
			apontamentoService.saveOrUpdate(ap);
		}
		arquivopontoService.updateProcessamento(bean.getWhereIn());
		
		request.addMessage("Arquivo(s) processado(s) com sucesso."); 
		if(bean.getEntrada()){
			return new ModelAndView("redirect:/rh/crud/Arquivoponto?ACAO=consultar&cdarquivoponto=" + bean.getWhereIn());
		} else {
			return new ModelAndView("redirect:/rh/crud/Arquivoponto");
		}
	}
	
	/**
	 * Monta um Map com todas as matr�culas com seus sal�rios separados por arquivo de ponto.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivopontoService#findForProcessamento(String whereIn)
	 * @see br.com.linkcom.sined.modulo.rh.controller.process.ProcessararquivopontoProcess#preencheListaSalario(Arquivoponto ap)
	 *
	 * @param whereIn
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	private Map<Integer, List<ImportacaoSalarioBean>> makeMapSalarios(String whereIn) {
		List<Arquivoponto> listaArquivoponto = arquivopontoService.findForProcessamento(whereIn);
		Map<Integer, List<ImportacaoSalarioBean>> map = new HashMap<Integer, List<ImportacaoSalarioBean>>();
		for (Arquivoponto ap : listaArquivoponto) {
			map.put(ap.getCdarquivoponto(), this.preencheListaSalario(ap));
		}
		
		return map;
	}
	
	/**
	 * Preenche uma lista de ImportacaoSalarioBean para montar um Map com todas
	 * as matr�culas com seus sal�rios separados por arquivo de ponto.
	 * 
	 * @param ap
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	private List<ImportacaoSalarioBean> preencheListaSalario(Arquivoponto ap) {
		List<ImportacaoSalarioBean> lista = new ArrayList<ImportacaoSalarioBean>();
		
		if(ap.getArquivosalario() != null){
			Arquivo arquivosalario = arquivoService.loadWithContents(ap.getArquivosalario());
			String string = new String(arquivosalario.getContent()).trim();
			String[] linhas = string.split("\\r?\\n");
			String[] campos;
			ImportacaoSalarioBean bean;
			String tipocargoStr;
			
			for (int i = 0; i < linhas.length; i++) {
				campos = linhas[i].split(";");
				if(campos != null && campos.length == 5 && campos[4].trim().equals("N")){
					bean = new ImportacaoSalarioBean();
					bean.setMatricula(Integer.parseInt(campos[0]));
					bean.setSalario(new Money(campos[1].replaceAll("\\.", "").replace(",", ".")).add(new Money(campos[2].replaceAll("\\.", "").replace(",", "."))));
					
					tipocargoStr = campos[3];
					if(tipocargoStr.equals("MENSALISTA INDIRETO")){
						bean.setTipocargo(Tipocargo.MOI);
					} else if(tipocargoStr.equals("MENSALISTA DIRETO")){
						bean.setTipocargo(Tipocargo.MOD);
					}
					
					lista.add(bean);
				}
			}
		}
		
		return lista;
	}
	
	/**
	 * Action padr�o que abre a tela de confirma��o para o processamento do arquivo de ponto.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivopontoService#findForProcessamento(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#findByMatricula(Integer matricula)
	 * @see br.com.linkcom.sined.geral.service.ApontamentoService#existeByColaboradorData(Colaborador colaborador, Date data)
	 *
	 * @param request
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	@DefaultAction
	public ModelAndView processar(WebRequestContext request){
		
		ImportacaoArquivopontoBean bean = new ImportacaoArquivopontoBean();
		bean.setWhereIn(SinedUtil.getItensSelecionados(request));
		bean.setEntrada("true".equals(request.getParameter("entrada")));
		
		Arquivo arquivo;
		String string;
		
		List<String> erros = new ArrayList<String>();
		
		List<Arquivoponto> lista = arquivopontoService.findForProcessamento(bean.getWhereIn());
		for (Arquivoponto ap : lista) {
			if(ap.getProcessado() != null && ap.getProcessado()){
				erros.add("Arquivo de ponto " + ap.getCdarquivoponto() + " j� processado.");
				continue;
			}
			
			arquivo = arquivoService.loadWithContents(ap.getArquivo());
			string = new String(arquivo.getContent());
			ap.setStringArquivo(string.split("\\r?\\n"));
		}
		
		if(erros.size() > 0){
			request.addError("N�o foi poss�vel processar o(s) arquivo(s) selecionado(s)."); 
			for (String erro : erros) {
				request.addError(erro);
			}
			return new ModelAndView("redirect:/rh/crud/Arquivoponto" + (bean.getEntrada() ? "?ACAO=consultar&cdarquivoponto=" + bean.getWhereIn() : ""));
		}
		
		boolean haveArquivoSemProjeto = false;
		List<ImportacaoArquivopontoItemBean> listaItens = new ArrayList<ImportacaoArquivopontoItemBean>();
		List<ColaboradorVO> listaColaboradorVO = new ArrayList<ColaboradorVO>();
		
		for (Arquivoponto ap : lista) {
			String[] linhas = ap.getStringArquivo();
			for (String linha : linhas) {
				try{
//					String nsrStr = linha.substring(0, 9).trim(); 				// N�mero Sequencial de Registro
					String tipoRegistroStr = linha.substring(9, 10).trim(); 	// Tipo de Registro '3'
					String dataStr = linha.substring(10, 18).trim(); 			// Data do lan�amento (ddMMyyyy)
					String horaStr = linha.substring(18, 22).trim();			// Hora e minuto do lan�amento (hhmm)
					String pisStr = linha.substring(22, 34).trim(); 			// n�mero do PIS do funcion�rio
					
					if(!tipoRegistroStr.equals("3")) continue;
					
					ImportacaoArquivopontoItemBean item = new ImportacaoArquivopontoItemBean();
					
					String dia = dataStr.substring(0,2);
					String mes = dataStr.substring(2,4);
					String ano = dataStr.substring(4);
					dataStr = dia+"/"+mes+"/"+ano;
					
					String hora = horaStr.substring(0,2);
					String minuto = horaStr.substring(2,4);
					horaStr = hora+":"+minuto;
					
					Long matricula = Long.parseLong(pisStr);
					Boolean isNewColaborador = true;
					
					for (ColaboradorVO colaboradorVO : listaColaboradorVO){
						if(colaboradorVO.getMatricula().equals(matricula)){
							isNewColaborador = false;
							Boolean saida = false;									
							for (PeriodoVO periodoVO : colaboradorVO.getListaPeriodo()){
								if(periodoVO.getData().equals(SinedDateUtils.stringToDate(dataStr)) && periodoVO.getEntrada()!=null && periodoVO.getSaida()==null){
									periodoVO.setSaida(new Hora(horaStr));
									saida = true;
								}
							}if(!saida){
								PeriodoVO periodoVO = new PeriodoVO();
								periodoVO.setData(SinedDateUtils.stringToDate(dataStr));
								periodoVO.setEntrada(new Hora(horaStr));
								colaboradorVO.getListaPeriodo().add(periodoVO);
							}
						}
					}if(isNewColaborador){
						ColaboradorVO colaboradorVO = new ColaboradorVO();
						colaboradorVO.setMatricula(matricula);
						PeriodoVO periodoVO = new PeriodoVO();
						periodoVO.setData(SinedDateUtils.stringToDate(dataStr));
						periodoVO.setEntrada(new Hora(horaStr));
						colaboradorVO.getListaPeriodo().add(periodoVO);
						listaColaboradorVO.add(colaboradorVO);
					}					
					
					item.setData(SinedDateUtils.stringToDate(dataStr));
					item.setMatricula(pisStr);
					item.setHoratotal(new Hora(horaStr));
					
//					if(pisStr.startsWith("01")) pisStr = pisStr.substring(2);
					while(pisStr.startsWith("0")){
						pisStr = pisStr.substring(1);
					}						
					
					item.setColaborador(colaboradorService.findByNumeroinscricao(pisStr));
					
					if(item.getColaborador() != null){
						item.setExiste(apontamentoService.existeByColaboradorData(item.getColaborador(), item.getData()));
					}
					
					item.setCdarquivoponto(ap.getCdarquivoponto());
					item.setProjeto(ap.getProjeto());
					
					if(ap.getProjeto() == null){
						haveArquivoSemProjeto = true;
					}
					
					listaItens.add(item);
				} catch (Exception e){
					request.addError(e.getMessage());
					e.printStackTrace();						
				}
			}
		}
		
		ajustaListadeLancamentos(listaItens, listaColaboradorVO);
		bean.setListaColaboradorVO(listaColaboradorVO);
		bean.setListaItens(listaItens);
		request.setAttribute("haveArquivoSemProjeto", haveArquivoSemProjeto);
		
		return new ModelAndView("process/processarArquivoponto", "bean", bean);
	}
	
	/**
	 * M�todo que ajusta e val�da os lan�amentos da lista principal (List<ImportacaoArquivopontoItemBean> listaItens) 
	 * uma vez que o arquivo registra individualmente os lan�amentos dos hor�rios de entrada e sa�da.
	 * 
	 * @param listaItens
	 * @param listaColaboradorVO
	 */
	private void ajustaListadeLancamentos(List<ImportacaoArquivopontoItemBean> listaItens,List<ColaboradorVO> listaColaboradorVO) {
		for (ImportacaoArquivopontoItemBean iap: listaItens) {
			for (ColaboradorVO colaboradorVO : listaColaboradorVO){
				if(Long.parseLong(iap.getMatricula()) == colaboradorVO.getMatricula()){
					for (PeriodoVO periodoVO : colaboradorVO.getListaPeriodo()) {
						if(periodoVO.getIsProcessado()==null || !periodoVO.getIsProcessado()){
//							System.out.println("HORARIO: "+periodoVO.getData()+" - Entrada: "+periodoVO.getEntrada()+" Saida: "+periodoVO.getSaida() );
							iap.setHoraEntrada(periodoVO.getEntrada());
							iap.setHoraSaida(periodoVO.getSaida());
							if(periodoVO.getEntrada()!=null && periodoVO.getSaida()!=null){
								iap.setHoratotal(colaboradorVO.getTotalByEntradaSaida(periodoVO));
							}
							iap.setData((Date) periodoVO.getData());
							periodoVO.setIsProcessado(true);
							break;
						}
					}
				}
			}
		}
		
		Iterator<ImportacaoArquivopontoItemBean> iterator = listaItens.iterator();
		while (iterator.hasNext()){
			ImportacaoArquivopontoItemBean iap = iterator.next();
			if(iap.getHoraEntrada()==null && iap.getHoraSaida()==null){
				iterator.remove();
			}
		}
	}
	
	/**
	 * 
	 * @param request
	 * @param iap
	 * @return
	 */
	public ModelAndView ajaxRecalculaHoraTotal(WebRequestContext request, ImportacaoArquivopontoItemBean iap){		
		Hora horaTotal = null;
		if(iap.getHoraSaida()!=null && iap.getHoraEntrada()!=null){
			try{
				double total = SinedDateUtils.hoursToDouble(iap.getHoraSaida().toString()) - SinedDateUtils.hoursToDouble(iap.getHoraEntrada().toString());
				horaTotal = new Hora(SinedDateUtils.doubleToHours(total));				
			}catch (Exception e) {	
				e.printStackTrace();
				horaTotal = null;	
				throw new RuntimeException("O 'Hor�rio de Sa�da' n�o pode ser menor que o 'H�rairo de Entrada'");											
			}
		}
		return new JsonModelAndView().addObject("horaTotal", horaTotal);
	}
	
}
