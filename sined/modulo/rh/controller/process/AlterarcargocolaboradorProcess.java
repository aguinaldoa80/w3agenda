package br.com.linkcom.sined.modulo.rh.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.service.ColaboradorcargoService;
import br.com.linkcom.sined.geral.service.ColaboradorsituacaoService;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.AlterarcargocolaboradorFilter;
import br.com.linkcom.sined.util.SinedException;

@Bean
@Controller(path="/rh/process/Alterarcargocolaborador",authorizationModule=ProcessAuthorizationModule.class)
public class AlterarcargocolaboradorProcess extends MultiActionController {

	private ColaboradorcargoService colaboradorcargoService;
	private ColaboradorsituacaoService colaboradorsituacaoService;

	public void setColaboradorsituacaoService(
			ColaboradorsituacaoService colaboradorsituacaoService) {
		this.colaboradorsituacaoService = colaboradorsituacaoService;
	}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {
		this.colaboradorcargoService = colaboradorcargoService;
	}

	/**
	 * M�todo para carregar a listagem no jsp via ajax.
	 *
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findColaboradorcargoAltera(AlterarcargocolaboradorFilter)
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Flavio Tavares
	 */
	@DefaultAction
	public ModelAndView alterar(WebRequestContext request, AlterarcargocolaboradorFilter filtro)throws Exception{
		if(request.getSession().getAttribute("AlterarcargocolaboradorFilter") != null){
			filtro = (AlterarcargocolaboradorFilter) request.getSession().getAttribute("AlterarcargocolaboradorFilter");
		}
		request.setAttribute("listaSituacoes", colaboradorsituacaoService.findAll());
		return new ModelAndView("process/alterarcargocolaborador", "filtro", filtro);
	}
	
	/**
	 * Action que retorna a listagem da tela via ajax.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @since 09/03/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView listar(WebRequestContext request, AlterarcargocolaboradorFilter filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		request.getSession().setAttribute("AlterarcargocolaboradorFilter", filtro);
		
		List<Colaboradorcargo> listaColaborador = colaboradorcargoService.findColaboradorcargoAltera(filtro);
		return new ModelAndView("direct:ajax/alterarcargocolaboradorlistagem", "listaColaborador", listaColaborador);
	}
	
}
