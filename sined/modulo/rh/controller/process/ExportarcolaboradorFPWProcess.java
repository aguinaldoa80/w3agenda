package br.com.linkcom.sined.modulo.rh.controller.process;

import java.sql.Date;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Tipoinscricao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EscalacolaboradorService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ExportarcolaboradorFiltro;
import br.com.linkcom.sined.modulo.rh.exportacao.AbstractExport;
import br.com.linkcom.sined.util.SinedException;

import com.ibm.icu.text.SimpleDateFormat;

@Bean
@Controller(path="/rh/report/ExportarcolaboradorFPW", authorizationModule=ProcessAuthorizationModule.class)
public class ExportarcolaboradorFPWProcess extends ResourceSenderController<ExportarcolaboradorFiltro>{

	private ColaboradorService colaboradorService;
	private ParametrogeralService parametrogeralService;
	private EscalacolaboradorService escalacolaboradorService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setEscalacolaboradorService(
			EscalacolaboradorService escalacolaboradorService) {
		this.escalacolaboradorService = escalacolaboradorService;
	}

	/**
	 * M�todo default do controler, carrega o filtro no jsp
	 * @author Tom�s Rabelo
	 */
	@DefaultAction
	public ModelAndView doFiltro(WebRequestContext request, ExportarcolaboradorFiltro filtro){
		return new ModelAndView("process/exportarcolaborador", "filtro", filtro);
	}

	/**
	 * M�todo para carregar listagem do filtro no jsp.
	 *
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findForListagemExportarColaborador(ExportarcolaboradorFiltro)
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	@Input("doFiltro")
	@Command(validate = true)
	public ModelAndView listagem(WebRequestContext request, ExportarcolaboradorFiltro filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null");
		}
		List<Colaborador> listaColaborador = colaboradorService.findForListagemExportarColaborador(filtro);
		filtro.setListaColaborador(listaColaborador);
		request.setAttribute("acao","listagem");
		return new ModelAndView("process/exportarcolaborador", "filtro", filtro);
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	ExportarcolaboradorFiltro filtro) throws Exception {
		String colaboradores = request.getParameter("colaboradores");
		List<Colaborador> listaColaborador = colaboradorService.gerarListaParaArquivoExportacao(colaboradores);
		for (Colaborador colaborador : listaColaborador) {
			//M�todo colocado aqui, pois estava carregando items duplicados no updateentrada
			colaborador.setListaEscalacolaborador(escalacolaboradorService.findByColaborador(colaborador));
		}
		StringBuilder sb = gerarArquivoExportacaoFPW(request, listaColaborador);
		
		Resource resource = new Resource("text/csv", "colaboradores.txt", sb.toString().getBytes());
		return resource;
	}

	private StringBuilder gerarArquivoExportacaoFPW(WebRequestContext request, List<Colaborador> listaColaborador) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String empresa = parametrogeralService.getValorPorNome(Parametrogeral.CODIGOEMPRESAAFPW);
		StringBuilder sb = new StringBuilder();
		for (Colaborador colaborador : listaColaborador) {
			if(sb.toString() != null && !sb.toString().equals(""))
				sb.append("\n");
			Colaboradorcargo colaboradorcargo = colaborador.getListaColaboradorcargo() != null && !colaborador.getListaColaboradorcargo().isEmpty() ? colaborador.getListaColaboradorcargo().iterator().next() : null; 
			sb.append(AbstractExport.fillNum(Long.valueOf(empresa), 3)).append("\t");//C�digo da Empresa X
			try {
				sb.append(AbstractExport.fillNum((long)colaboradorcargo.getMatricula(), 9)).append("\t");//Matr�cula X
			} catch (Exception e) {
				if(colaboradorcargo != null){
					throw new SinedException("Matr�cula inv�lida no cadastro do cargo do colaborador <a href=\"" + request.getServletRequest().getContextPath() + "/rh/crud/Colaboradorcargo?ACAO=consultar&cdcolaboradorcargo=" + colaboradorcargo.getCdcolaboradorcargo() + "\">" + colaborador.getNome() + "</a>");
				}
				throw new SinedException("Matr�cula inv�lida do colaborador <a href=\"" + request.getServletRequest().getContextPath() + "/rh/crud/Colaborador?ACAO=consultar&cdpessoa=" + colaborador.getCdpessoa() + "\">" + colaborador.getNome() + "</a>");
			}
			sb.append(AbstractExport.fillAlfa(colaborador.getNome(), 45)).append("\t");//Nome X
			sb.append(colaborador.getSexo().getCdsexo().equals(Sexo.MASCULINO) ? "M" : "F").append("\t");//Sexo X
			sb.append(AbstractExport.fillAlfa(colaborador.getDtnascimento()!= null ? format.format(colaborador.getDtnascimento()).toString() : null, 8)).append("\t");//Dt nascimento X
			switch (colaborador.getEstadocivil().getCdestadocivil()) {//Estado civil X
			case 1://Casado
				sb.append(2);break;
			case 2://Solteiro
				sb.append(1);break;
			case 3://Viuvo
				sb.append(5);break;
			case 4://Separado judicialmente
				sb.append(3);break;
			case 5://Divorciado
				sb.append(4);break;
			default:
				sb.append(7);break;
			}
			sb.append("\t");
			sb.append(AbstractExport.fillNum(colaborador.getGrauinstrucao().getCdgrauinstrucao() != null ? Long.valueOf(colaborador.getGrauinstrucao().getCdgrauinstrucao()) : null, 2)).append("\t");//Grau instru��o X
			sb.append(AbstractExport.fillNum(colaborador.getNacionalidade()!= null ? Long.valueOf(colaborador.getNacionalidade().getValue()) : null, 2)).append("\t");//Nacionalidade X
			sb.append(AbstractExport.fillNum(null,2)).append("\t");//Ano chegada
			//ENDERECO OBRIGATORIO
			sb.append(AbstractExport.fillAlfa(colaborador.getEndereco().getLogradouroCompleto(), 50)).append("\t");//Endere�o X
			sb.append(AbstractExport.fillAlfa(colaborador.getEndereco().getBairro(), 20)).append("\t");//Bairro X
			sb.append(AbstractExport.fillNum(Long.valueOf(colaborador.getEndereco().getMunicipio().getCdibge()), 7)).append("\t");//C�digo munic�pio X
			sb.append(AbstractExport.fillNum(Long.valueOf(colaborador.getEndereco().getCep().getValue()), 8)).append("\t");//CEP X
			sb.append(AbstractExport.fillAlfa(colaborador.getTelefone() != null ? colaborador.getTelefone().getTelefone() : null, 15)).append("\t");//Telefone
			//sb.append(AbstractExport.fillNum(colaborador.getTelefone() != null ? Long.valueOf(colaborador.getTelefone().getTelefone()) : null, 8)).append("\t");//Telefone 
			sb.append(AbstractExport.fillAlfa(colaborador.getMae(), 45)).append("\t");//Nome m�e
			sb.append(AbstractExport.fillNum(colaborador.getColaboradorsituacao().getCdcolaboradorsituacao() != null ? Long.valueOf(colaborador.getColaboradorsituacao().getCdcolaboradorsituacao()) : null, 3)).append("\t");//C�digo situa��o anterior X 
			sb.append(AbstractExport.fillNum(null, 7)).append("\t");//C�digo munic�pio nasc
			sb.append(AbstractExport.fillNum((long)colaborador.getColaboradorsituacao().getCdcolaboradorsituacao(), 3)).append("\t");//C�digo situa��o X
			sb.append(format.format(colaboradorcargo.getDtinicio())).append("\t");//Dt in�cio situa��o X
			sb.append(format.format(new Date(System.currentTimeMillis()))).append("\t");//Dt in�cio per aqui X
			/*switch (colaboradorcargo.getRegimecontratacao().getCdregimecontratacao()) {//Tipo admiss�o X
			case 1:
				break;
			default:
				break;
			}*/
			sb.append(1).append("\t");//Mudar isso!!!!
			sb.append(AbstractExport.fillNum(colaboradorcargo.getCaged() != null && colaboradorcargo.getCaged().getValue()!= null ? Long.valueOf(colaboradorcargo.getCaged().getValue()): null, 2)).append("\t");//Cod Mov CAGED X
			sb.append(format.format(colaboradorcargo.getDtinicio())).append("\t");//Dt admiss�o X
			sb.append(AbstractExport.fillAlfa((colaboradorcargo.getDepartamento().getCodigofolha() != null ? colaboradorcargo.getDepartamento().getCodigofolha(): null), 20)).append("\t");//Cod lota��o X
			sb.append(colaboradorcargo.getTiposalario() != null && colaboradorcargo.getTiposalario().getValue() != null ? colaboradorcargo.getTiposalario().getValue(): null).append("\t");//Tipo sal�rio X
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//Tabela de Sal�rio
			sb.append(AbstractExport.fillNum(colaboradorcargo.getCargo().getCodigoFolha() != null ? Long.valueOf(colaboradorcargo.getCargo().getCodigoFolha()) : null, 6)).append("\t");//Cod cargo X
			sb.append(AbstractExport.fillAlfa(null, 10)).append("\t");//N�vel
			sb.append(AbstractExport.fillAlfa((colaboradorcargo.getCargo().getCbo() != null && colaboradorcargo.getCargo().getCbo().getCdcbo() != null ? colaboradorcargo.getCargo().getCbo().getCdcbo().toString() : null), 6)).append("\t");//CBO
			sb.append(AbstractExport.fillNum(colaboradorcargo.getSindicato() != null && colaboradorcargo.getSindicato().getCodigofolha() != null ? Long.valueOf(colaboradorcargo.getSindicato().getCodigofolha()): null, 3)).append("\t");//Cod Sindicato
			sb.append(AbstractExport.fillNum(colaborador.getListaEscalacolaborador() != null && colaborador.getListaEscalacolaborador().size()>0 ? Long.valueOf(colaborador.getListaEscalacolaborador().get(colaborador.getListaEscalacolaborador().size()-1).getEscala().getCdescala()) : null, 4)).append("\t");//Cod Hor�rio X
			sb.append(AbstractExport.fillNum(null, 7)).append("\t");//Horas Mensais
			sb.append(AbstractExport.fillNum(null, 7)).append("\t");//Horas Semanais
			sb.append(AbstractExport.fillNum(colaborador.getColaboradortipo() != null && colaborador.getColaboradortipo().getCdcolaboradortipo() != null ? Long.valueOf(colaborador.getColaboradortipo().getCdcolaboradortipo()) : null, 2)).append("\t");//V�nculo X
			sb.append(AbstractExport.fillAlfa(null, 1)).append("\t");//Indic Bate Ponto
			sb.append(AbstractExport.fillAlfa(null, 1)).append("\t");//Indic Contr Sindical
			sb.append(AbstractExport.fillAlfa(null, 1)).append("\t");//Indic Contr Exper
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Dt Vencto Contrato
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//Cod Banco Pag 
			sb.append(AbstractExport.fillAlfa(null, 5)).append("\t");//Cod Agencia Pag
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//Tipo Conta Corrente
			sb.append(AbstractExport.fillAlfa(null, 11)).append("\t");//Conta Corrente Pag 
			sb.append(colaborador.getIndiceprevidencia() == null ? 3 : colaborador.getIndiceprevidencia() ? 1 : 2).append("\t");//Indic Previd�ncia X
			sb.append(AbstractExport.fillNum(null, 12)).append("\t");//Matr Previd�ncia
			sb.append(AbstractExport.fillAlfa(null, 1)).append("\t");//Ind IRRF
			sb.append(colaborador.getFgtsopcao() != null ? colaborador.getFgtsopcao() ?  1 : 2 : null).append("\t");//FGTS Op��o X
			sb.append(colaborador.getFgtsdata() != null ? format.format(colaborador.getFgtsdata()) : null).append("\t");//FGTS Dt Op��o
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//FGTS Retrata��o
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//Agente Nocivo
			sb.append(AbstractExport.fillAlfa(null, 11)).append("\t");//FGTS Conta
			switch (colaborador.getEtnia().getCdetnia()) {//Cod Ra�a/Cor 
			case 5:
				sb.append(AbstractExport.fillAlfa("8", 5));
				break;
			default:
				sb.append(AbstractExport.fillAlfa(colaborador.getEtnia().getCdetnia().toString(), 5));
				break;
			}
			sb.append("\t");
			sb.append(AbstractExport.fillAlfa(colaborador.getRg() != null ? colaborador.getRg() : null, 20)).append("\t");//Ident N�mero
			sb.append(colaborador.getDtemissaorg() != null ? format.format(colaborador.getDtemissaorg()) : null).append("\t");//Ident Dt Emiss�o
			sb.append(AbstractExport.fillAlfa(colaborador.getOrgaoemissorrg() != null ? colaborador.getOrgaoemissorrg() : null, 15)).append("\t");//Ident �rg�o Exp
			sb.append(AbstractExport.fillAlfa(null, 2)).append("\t");//Ident UF
			sb.append(AbstractExport.fillNum(colaborador.getCtps() != null ? Long.valueOf(colaborador.getCtps()) : null, 8)).append("\t");//CTrab N�mero X S� pode numero
			sb.append(AbstractExport.fillAlfa(colaborador.getSeriectps() != null ? colaborador.getSeriectps().toString() : null, 5)).append("\t");//CTrab S�rie X
			sb.append(colaborador.getDtemissaocarteiratrabalho() != null ? format.format(colaborador.getDtemissaocarteiratrabalho()) : AbstractExport.fillNum(null, 8)).append("\t");//CTrab Dt Emiss�o
			sb.append(AbstractExport.fillAlfa(colaborador.getUfctps() != null && colaborador.getUfctps().getCdibge() != null ? colaborador.getUfctps().getCdibge().toString() : null, 2)).append("\t");//CTrab UF X
			sb.append(AbstractExport.fillNum(colaborador.getTituloeleitoral() != null ? Long.valueOf(colaborador.getTituloeleitoral()): null, 14)).append("\t");//Tit N�mero s� numero 
			sb.append(AbstractExport.fillNum(colaborador.getSecaoeleitoral() != null ? Long.valueOf(colaborador.getSecaoeleitoral()) : null, 4)).append("\t");//Tit Se��o s� numero 
			sb.append(AbstractExport.fillAlfa(colaborador.getZonaeleitoral() != null ? colaborador.getZonaeleitoral() : null, 3)).append("\t");//Tit Zona
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Tit Dt Emiss�o
			sb.append(AbstractExport.fillAlfa(null, 2)).append("\t");//Tit UF
			sb.append(AbstractExport.fillAlfa(colaborador.getDocumentomilitar(), 5)).append("\t");//CMil Documento
			sb.append(AbstractExport.fillNum(null, 12)).append("\t");//CMil N�mero
			sb.append(AbstractExport.fillAlfa(null, 2)).append("\t");//CMil S�rie 
			sb.append(AbstractExport.fillAlfa(colaborador.getTipodocumentomilitar() != null ? colaborador.getTipodocumentomilitar().getNome() : null, 3)).append("\t");//CMil Categoria
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//CMil RM DN Comar
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//CMil CSM OAM 
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Dt Vencto Ates Sau 
			if(colaborador.getTipoinscricao() != null && colaborador.getTipoinscricao().getCdtipoinscricao() != null && 
			  (colaborador.getTipoinscricao().getCdtipoinscricao().equals(Tipoinscricao.PIS) ||colaborador.getTipoinscricao().getCdtipoinscricao().equals(Tipoinscricao.PASEP))){
				sb.append(AbstractExport.fillNum(colaborador.getNumeroinscricao() != null ? Long.valueOf(colaborador.getNumeroinscricao()) : null, 11));//PIS PASEP s� numero
			}else{
				sb.append(AbstractExport.fillNum(null, 11));//PIS PASEP
			}
			sb.append("\t");
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//PIS Dt Cadastro
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//PIS Cod Banco
			sb.append(AbstractExport.fillAlfa(null, 5)).append("\t");//PIS Cod Agencia
			sb.append(AbstractExport.fillNum((colaborador.getCpf() != null && colaborador.getCpf().getValue() != null ? Long.valueOf(colaborador.getCpf().getValue()) : null), 11)).append("\t");//CPF
			sb.append(AbstractExport.fillAlfa(null, 4)).append("\t");//Outro Doc Tipo
			sb.append(AbstractExport.fillNum(null, 10)).append("\t");//Outro Doc N�mero
			sb.append(AbstractExport.fillAlfa(null, 1)).append("\t");//Cert Tipo
			sb.append(AbstractExport.fillNum(null, 6)).append("\t");//Cert N�mero
			sb.append(AbstractExport.fillAlfa(null, 10)).append("\t");//Cert Livro
			sb.append(AbstractExport.fillAlfa(null, 5)).append("\t");//Cert Folha 
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Cert Dt Emiss�o
			sb.append(AbstractExport.fillNum(null, 7)).append("\t");//Cert Cod Municipio
			sb.append(AbstractExport.fillNum(null, 2)).append("\t");//Anos Trabalhados
			sb.append(AbstractExport.fillNum(null, 2)).append("\t");//Idade
			sb.append(AbstractExport.fillNum(null, 2)).append("\t");//Dependentes SF
			sb.append(AbstractExport.fillNum(null, 2)).append("\t");//Dependentes IRRF 
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//Cod Sit Prog 
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Dt Ini Sit Prog 
			sb.append(AbstractExport.fillNum(null, 6)).append("\t");//C�digo Cargo Comissionado
			sb.append(AbstractExport.fillAlfa(null, 1)).append("\t");//Tipo Sal.Cargo Comissionado
			sb.append(AbstractExport.fillAlfa(null, 10)).append("\t");//N�vel Cargo Comissionado
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//Alvar� judicial para trabalhar
			sb.append(AbstractExport.fillAlfa(colaboradorcargo.getProjeto() != null && colaboradorcargo.getProjeto().getSigla() != null ? colaboradorcargo.getProjeto().getSigla() : null, 20)).append("\t");//Cod Centro de Custo   
			sb.append(AbstractExport.fillNum(null, 2)).append("\t");//Nr Dependentes Ass M�dica
			sb.append(AbstractExport.fillNum(null, 2)).append("\t");//Nr Dependentes Outros
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//Regime de tempo parcial 
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Dt C�lculo Benef�cios
			sb.append(AbstractExport.fillAlfa(null, 10)).append("\t");//Identifica��o do Funcion�rio
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//�rgao Respons�vel
			sb.append(AbstractExport.fillAlfa(null, 10)).append("\t");//Sigla
			sb.append(AbstractExport.fillAlfa(colaborador.getPai() != null ? colaborador.getPai() : null, 45)).append("\t");//Nome do Pai
			sb.append(AbstractExport.fillNum((long)1, 3)).append("\t");//Natureza do Profissional
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//C�digo da Estabilidade
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Data In�cio Estabilidade
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Data Final Estabilidade 
			sb.append(AbstractExport.fillAlfa(null, 5)).append("\t");//Tipo de Visto para Estrangeiro
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Validade CTPS Estrangeiro  
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//DDD  
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Controle  
			sb.append(AbstractExport.fillNum(null, 9)).append("\t");//Reg. do funcion�rio na DRT
			sb.append(AbstractExport.fillAlfa(colaborador.getEmail() != null ? colaborador.getEmail() : null, 60)).append("\t");//Email do funcion�rio
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Dt Val do RG p\ Estrangeiros
			sb.append(AbstractExport.fillAlfa(null, 15)).append("\t");//Telefone Celular
			sb.append(AbstractExport.fillNum(null, 3)).append("\t");//Cod. Sindicato Prof. Liberal
			sb.append(AbstractExport.fillAlfa(null, 5)).append("\t");//Cod. CBO oficial 94
			sb.append(AbstractExport.fillNum(null, 15)).append("\t");//N�mero da CNH 
			sb.append(AbstractExport.fillAlfa(null, 3)).append("\t");//fillAlfa
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Data de Validade da CNH
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Data da primeira habilita��o
			sb.append(AbstractExport.fillNum(null, 5)).append("\t");//Local de trabalho do funcion�rio N�O UTILIZADO
			sb.append(AbstractExport.fillAlfa(null, 50)).append("\t");//Complemento do endere�o N�O UTILIZADO
			sb.append(AbstractExport.fillNum(null, 9)).append("\t");//C�digo da Equipe N�O UTILIZADO
			sb.append(AbstractExport.fillAlfa(null, 30)).append("\t");//C�digo Identificador de Pessoa 
			sb.append(AbstractExport.fillNum(null, 8)).append("\t");//Data de Atribui��o do ID
			sb.append(AbstractExport.fillAlfa(null, 30)).append("\t");//Cod. Grupo Hier�rquico
			sb.append(AbstractExport.fillAlfa(null, 40)).append("\t");//Segundo e-mail do funcion�rio
			sb.append(AbstractExport.fillNum(null, 3));//Tipo de Necessidade Especial
		}
		return sb;
	}

}
