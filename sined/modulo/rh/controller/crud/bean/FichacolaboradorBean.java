package br.com.linkcom.sined.modulo.rh.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;

public class FichacolaboradorBean {
	protected String dadosPessoais;
	protected String matricula;
	protected String nome;
	protected String dtNascimento;
	protected String sexo;
	protected String nacionalidade;
	protected String uf;
	protected String naturalidade;
	protected String escolaridade;
	protected String email;
	protected String nomeMae;
	protected String nomePai;
	protected String tipoSanguineo;
	protected String etnia;
	protected String tipoEmpregado;
	protected String brPdh;
	protected String deficiencia;
	protected String estadoCivil;
	protected String escalaHorario;
	protected String telefones;
	protected List<String> listaTelefones = new ListSet<String>(String.class); 
	protected String endereco;
	protected String logradouro;
	protected String numero;
	protected String complemento;
	protected String bairro;
	protected String ufEndereco;
	protected String municipio;
	protected String cep;
	protected String caixaPostal;
	protected String documentos;
	protected String rgNumero;
	protected String rgOrgaoEmissor; 
	protected String rgDtEmissao;
	protected String cpf;
	protected String tituloEleitoral;
	protected String zona;
	protected String secao;
	protected String dirf;
	protected String ctpsNumero;
	protected String ctpsSerie;
	protected String ctpsUf;
	protected String ctpsDtEmissao;
	protected String documentoMilitarCategoria;
	protected String documentoMilitarNumero;
	protected String tipoInscricao;
	protected String numeroInscricao;
	protected String classifCarteiraProfissional;
	protected String cnhNumero;
	protected String cnhUf;
	protected String cnhCategoria;
	protected String cnhDtEmissao;
	protected String cnhDtValidade;
	protected String dadosBancarios;
	protected String formaPagamento;
	protected String tipoConta;
	protected String banco;
	protected String dadosProfissionais;
	protected String dtAdmissao; 
	protected String departamento;
	protected String cargo;
	protected String sindicato;
	protected String salario;
	protected String regimeContratacao;
	protected String situacao;
	public String getDadosPessoais() {
		return dadosPessoais;
	}
	public String getMatricula() {
		return matricula;
	}
	public String getNome() {
		return nome;
	}
	public String getDtNascimento() {
		return dtNascimento;
	}
	public String getSexo() {
		return sexo;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public String getUf() {
		return uf;
	}
	public String getNaturalidade() {
		return naturalidade;
	}
	public String getEscolaridade() {
		return escolaridade;
	}
	public String getEmail() {
		return email;
	}
	public String getNomeMae() {
		return nomeMae;
	}
	public String getNomePai() {
		return nomePai;
	}
	public String getTipoSanguineo() {
		return tipoSanguineo;
	}
	public String getEtnia() {
		return etnia;
	}
	public String getTipoEmpregado() {
		return tipoEmpregado;
	}
	public String getBrPdh() {
		return brPdh;
	}
	public String getDeficiencia() {
		return deficiencia;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public String getEscalaHorario() {
		return escalaHorario;
	}
	public String getTelefones() {
		return telefones;
	}
	public List<String> getListaTelefones() {
		return listaTelefones;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getUfEndereco() {
		return ufEndereco;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getCep() {
		return cep;
	}
	public String getCaixaPostal() {
		return caixaPostal;
	}
	public String getDocumentos() {
		return documentos;
	}
	public String getRgNumero() {
		return rgNumero;
	}
	public String getRgOrgaoEmissor() {
		return rgOrgaoEmissor;
	}
	public String getRgDtEmissao() {
		return rgDtEmissao;
	}
	public String getCpf() {
		return cpf;
	}
	public String getTituloEleitoral() {
		return tituloEleitoral;
	}
	public String getZona() {
		return zona;
	}
	public String getSecao() {
		return secao;
	}
	public String getDirf() {
		return dirf;
	}
	public String getCtpsNumero() {
		return ctpsNumero;
	}
	public String getCtpsSerie() {
		return ctpsSerie;
	}
	public String getCtpsUf() {
		return ctpsUf;
	}
	public String getCtpsDtEmissao() {
		return ctpsDtEmissao;
	}
	public String getDocumentoMilitarCategoria() {
		return documentoMilitarCategoria;
	}
	public String getDocumentoMilitarNumero() {
		return documentoMilitarNumero;
	}
	public String getTipoInscricao() {
		return tipoInscricao;
	}
	public String getNumeroInscricao() {
		return numeroInscricao;
	}
	public String getClassifCarteiraProfissional() {
		return classifCarteiraProfissional;
	}
	public String getCnhNumero() {
		return cnhNumero;
	}
	public String getCnhUf() {
		return cnhUf;
	}
	public String getCnhCategoria() {
		return cnhCategoria;
	}
	public String getCnhDtEmissao() {
		return cnhDtEmissao;
	}
	public String getCnhDtValidade() {
		return cnhDtValidade;
	}
	public String getDadosBancarios() {
		return dadosBancarios;
	}
	public String getFormaPagamento() {
		return formaPagamento;
	}
	public String getTipoConta() {
		return tipoConta;
	}
	public String getBanco() {
		return banco;
	}
	public String getDadosProfissionais() {
		return dadosProfissionais;
	}
	public String getDtAdmissao() {
		return dtAdmissao;
	}
	public String getDepartamento() {
		return departamento;
	}
	public String getCargo() {
		return cargo;
	}
	public String getSindicato() {
		return sindicato;
	}
	public String getSalario() {
		return salario;
	}
	public String getRegimeContratacao() {
		return regimeContratacao;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setDadosPessoais(String dadosPessoais) {
		this.dadosPessoais = dadosPessoais;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}
	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}
	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}
	public void setTipoSanguineo(String tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}
	public void setEtnia(String etnia) {
		this.etnia = etnia;
	}
	public void setTipoEmpregado(String tipoEmpregado) {
		this.tipoEmpregado = tipoEmpregado;
	}
	public void setBrPdh(String brPdh) {
		this.brPdh = brPdh;
	}
	public void setDeficiencia(String deficiencia) {
		this.deficiencia = deficiencia;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public void setEscalaHorario(String escalaHorario) {
		this.escalaHorario = escalaHorario;
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	public void setListaTelefones(List<String> listaTelefones) {
		this.listaTelefones = listaTelefones;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setUfEndereco(String ufEndereco) {
		this.ufEndereco = ufEndereco;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setCaixaPostal(String caixaPostal) {
		this.caixaPostal = caixaPostal;
	}
	public void setDocumentos(String documentos) {
		this.documentos = documentos;
	}
	public void setRgNumero(String rgNumero) {
		this.rgNumero = rgNumero;
	}
	public void setRgOrgaoEmissor(String rgOrgaoEmissor) {
		this.rgOrgaoEmissor = rgOrgaoEmissor;
	}
	public void setRgDtEmissao(String rgDtEmissao) {
		this.rgDtEmissao = rgDtEmissao;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setTituloEleitoral(String tituloEleitoral) {
		this.tituloEleitoral = tituloEleitoral;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public void setSecao(String secao) {
		this.secao = secao;
	}
	public void setDirf(String dirf) {
		this.dirf = dirf;
	}
	public void setCtpsNumero(String ctpsNumero) {
		this.ctpsNumero = ctpsNumero;
	}
	public void setCtpsSerie(String ctpsSerie) {
		this.ctpsSerie = ctpsSerie;
	}
	public void setCtpsUf(String ctpsUf) {
		this.ctpsUf = ctpsUf;
	}
	public void setCtpsDtEmissao(String ctpsDtEmissao) {
		this.ctpsDtEmissao = ctpsDtEmissao;
	}
	public void setDocumentoMilitarCategoria(String documentoMilitarCategoria) {
		this.documentoMilitarCategoria = documentoMilitarCategoria;
	}
	public void setDocumentoMilitarNumero(String documentoMilitarNumero) {
		this.documentoMilitarNumero = documentoMilitarNumero;
	}
	public void setTipoInscricao(String tipoInscricao) {
		this.tipoInscricao = tipoInscricao;
	}
	public void setNumeroInscricao(String numeroInscricao) {
		this.numeroInscricao = numeroInscricao;
	}
	public void setClassifCarteiraProfissional(String classifCarteiraProfissional) {
		this.classifCarteiraProfissional = classifCarteiraProfissional;
	}
	public void setCnhNumero(String cnhNumero) {
		this.cnhNumero = cnhNumero;
	}
	public void setCnhUf(String cnhUf) {
		this.cnhUf = cnhUf;
	}
	public void setCnhCategoria(String cnhCategoria) {
		this.cnhCategoria = cnhCategoria;
	}
	public void setCnhDtEmissao(String cnhDtEmissao) {
		this.cnhDtEmissao = cnhDtEmissao;
	}
	public void setCnhDtValidade(String cnhDtValidade) {
		this.cnhDtValidade = cnhDtValidade;
	}
	public void setDadosBancarios(String dadosBancarios) {
		this.dadosBancarios = dadosBancarios;
	}
	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public void setDadosProfissionais(String dadosProfissionais) {
		this.dadosProfissionais = dadosProfissionais;
	}
	public void setDtAdmissao(String dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public void setSindicato(String sindicato) {
		this.sindicato = sindicato;
	}
	public void setSalario(String salario) {
		this.salario = salario;
	}
	public void setRegimeContratacao(String regimeContratacao) {
		this.regimeContratacao = regimeContratacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
}