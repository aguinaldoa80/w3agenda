package br.com.linkcom.sined.modulo.rh.controller.crud.bean;

import java.util.Date;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargoprofissiografia;
import br.com.linkcom.sined.geral.bean.Colaboradorcargorisco;
import br.com.linkcom.sined.geral.bean.Colaboradorexame;
import br.com.linkcom.sined.geral.bean.Empresa;

public class FormularioPPPBean {

	protected Colaborador colaborador;
	protected Empresa empresa;
	protected Codigocnae codigocnae;
	protected Date dtCAT1;
	protected String registroCAT1;
	protected Date dtCAT2;
	protected String registroCAT2;
	protected Colaboradorcargo colaboradorcargoPrincipal;
	protected Colaborador colaboradorusuario;
	protected List<Colaboradorcargo> listaCargosColaborador = new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
	protected List<Colaboradorcargoprofissiografia> listaColaboradorcargoprofissiografia = new ListSet<Colaboradorcargoprofissiografia>(Colaboradorcargoprofissiografia.class);
	protected List<Colaboradorcargorisco> listaColaboradorcargorisco = new ListSet<Colaboradorcargorisco>(Colaboradorcargorisco.class);
	protected Set<Colaboradorexame> listaColaboradorexame = new ListSet<Colaboradorexame>(Colaboradorexame.class);
	
	protected Integer codigocfip;
	protected Boolean implementacaomedidas;
	protected Boolean usoepiiniterrupto;
	protected Boolean prazovalidade;
	protected Boolean trocaprograma;
	protected Boolean higienizacao;
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Date getDtCAT1() {
		return dtCAT1;
	}
	public String getRegistroCAT1() {
		return registroCAT1;
	}
	public Date getDtCAT2() {
		return dtCAT2;
	}
	public String getRegistroCAT2() {
		return registroCAT2;
	}
	public Colaboradorcargo getColaboradorcargoPrincipal() {
		return colaboradorcargoPrincipal;
	}
	public List<Colaboradorcargo> getListaCargosColaborador() {
		return listaCargosColaborador;
	}
	public List<Colaboradorcargoprofissiografia> getListaColaboradorcargoprofissiografia() {
		return listaColaboradorcargoprofissiografia;
	}
	public List<Colaboradorcargorisco> getListaColaboradorcargorisco() {
		return listaColaboradorcargorisco;
	}
	public Set<Colaboradorexame> getListaColaboradorexame() {
		return listaColaboradorexame;
	}
	public Colaborador getColaboradorusuario() {
		return colaboradorusuario;
	}
	public Codigocnae getCodigocnae() {
		return codigocnae;
	}
	public void setCodigocnae(Codigocnae codigocnae) {
		this.codigocnae = codigocnae;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtCAT1(Date dtCAT1) {
		this.dtCAT1 = dtCAT1;
	}
	public void setRegistroCAT1(String registroCAT1) {
		if(registroCAT1 != null && !registroCAT1.equals("") && registroCAT1.length() == 13)
			this.registroCAT1 = registroCAT1.substring(0, 10)+"-"+registroCAT1.substring(10, 11)+"/"+registroCAT1.substring(11, 13);
		else
			this.registroCAT1 = registroCAT1;
	}
	public void setDtCAT2(Date dtCAT2) {
		this.dtCAT2 = dtCAT2;
	}
	public void setRegistroCAT2(String registroCAT2) {
		if(registroCAT2 != null && !registroCAT2.equals("") && registroCAT2.length() == 13)
			this.registroCAT2 = registroCAT2.substring(0, 10)+"-"+registroCAT2.substring(10, 11)+"/"+registroCAT2.substring(11, 13);
		else
			this.registroCAT2 = registroCAT2;
	}
	public void setListaCargosColaborador(
			List<Colaboradorcargo> listaCargosColaborador) {
		this.listaCargosColaborador = listaCargosColaborador;
	}
	public void setListaColaboradorcargoprofissiografia(
			List<Colaboradorcargoprofissiografia> listaColaboradorcargoprofissiografia) {
		this.listaColaboradorcargoprofissiografia = listaColaboradorcargoprofissiografia;
	}
	public void setListaColaboradorcargorisco(
			List<Colaboradorcargorisco> listaColaboradorcargorisco) {
		this.listaColaboradorcargorisco = listaColaboradorcargorisco;
	}
	public void setListaColaboradorexame(Set<Colaboradorexame> listaColaboradorexame) {
		this.listaColaboradorexame = listaColaboradorexame;
	}
	public void setColaboradorcargoPrincipal(
			Colaboradorcargo colaboradorcargoPrincipal) {
		this.colaboradorcargoPrincipal = colaboradorcargoPrincipal;
	}
	public void setColaboradorusuario(Colaborador colaboradorusuario) {
		this.colaboradorusuario = colaboradorusuario;
	}
	public Integer getCodigocfip() {
		return codigocfip;
	}
	public Boolean getImplementacaomedidas() {
		return implementacaomedidas;
	}
	public Boolean getUsoepiiniterrupto() {
		return usoepiiniterrupto;
	}
	public Boolean getPrazovalidade() {
		return prazovalidade;
	}
	public Boolean getTrocaprograma() {
		return trocaprograma;
	}
	public Boolean getHigienizacao() {
		return higienizacao;
	}
	public void setCodigocfip(Integer codigocfip) {
		this.codigocfip = codigocfip;
	}
	public void setImplementacaomedidas(Boolean implementacaomedidas) {
		this.implementacaomedidas = implementacaomedidas;
	}
	public void setUsoepiiniterrupto(Boolean usoepiiniterrupto) {
		this.usoepiiniterrupto = usoepiiniterrupto;
	}
	public void setPrazovalidade(Boolean prazovalidade) {
		this.prazovalidade = prazovalidade;
	}
	public void setTrocaprograma(Boolean trocaprograma) {
		this.trocaprograma = trocaprograma;
	}
	public void setHigienizacao(Boolean higienizacao) {
		this.higienizacao = higienizacao;
	}
	
}
