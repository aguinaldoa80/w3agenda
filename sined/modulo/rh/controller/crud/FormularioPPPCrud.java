package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.view.Vwformularioppp;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.FormularioPPPReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/FormularioPPP", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "cpf"})
public class FormularioPPPCrud extends CrudControllerSined<FormularioPPPReportFiltro, Vwformularioppp, Vwformularioppp>{

	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, FormularioPPPReportFiltro filtro) {
		return new ModelAndView("crud/formulariopppListagem");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Vwformularioppp bean) throws Exception {
		throw new SinedException("A��o inv�lida");
	}
	
	@Override
	protected Vwformularioppp criar(WebRequestContext request, Vwformularioppp form) throws Exception {
		throw new SinedException("A��o inv�lida");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Vwformularioppp form) throws Exception {
		throw new SinedException("A��o inv�lida");
	}
	
}
