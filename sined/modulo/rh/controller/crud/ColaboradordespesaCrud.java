package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesahistorico;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Provisao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.ApontamentoSituacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesahistoricoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipovalorreceber;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradordespesaService;
import br.com.linkcom.sined.geral.service.ColaboradordespesahistoricoService;
import br.com.linkcom.sined.geral.service.ColaboradordespesaitemService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.EventoPagamentoService;
import br.com.linkcom.sined.geral.service.FechamentoFolhaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.ProvisaoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.modulo.financeiro.controller.process.FluxoStatusContaProcess;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradordespesaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Colaboradordespesa", authorizationModule=CrudAuthorizationModule.class)
public class ColaboradordespesaCrud extends CrudControllerSined<ColaboradordespesaFiltro, Colaboradordespesa, Colaboradordespesa>{

	private ColaboradordespesahistoricoService colaboradordespesahistoricoService;
	private ColaboradordespesaService colaboradordespesaService;
	private ColaboradordespesaitemService colaboradordespesaitemService;
	private DocumentoorigemService documentoorigemService;	
	private FluxoStatusContaProcess fluxoStatusContaProcess;
	private ColaboradorService colaboradorService;
	private ApontamentoService apontamentoService;
	private RateioService rateioService;
	private EventoPagamentoService eventoPagamentoService;
	private RateioitemService rateioitemService;
	private ProvisaoService provisaoService;
	private FechamentoFolhaService fechamentoFolhaService;

	public void setApontamentoService(ApontamentoService apontamentoService) {this.apontamentoService = apontamentoService;}
	public void setColaboradordespesaService(ColaboradordespesaService colaboradordespesaService){this.colaboradordespesaService = colaboradordespesaService;}
	public void setColaboradordespesaitemService(ColaboradordespesaitemService colaboradordespesaitemService){this.colaboradordespesaitemService = colaboradordespesaitemService;}
	public void setColaboradordespesahistoricoService(ColaboradordespesahistoricoService colaboradordespesahistoricoService){this.colaboradordespesahistoricoService = colaboradordespesahistoricoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService){this.documentoorigemService = documentoorigemService;}
	public void setFluxoStatusContaProcess(FluxoStatusContaProcess fluxoStatusContaProcess){this.fluxoStatusContaProcess = fluxoStatusContaProcess;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setEventoPagamentoService(EventoPagamentoService eventoPagamentoService) {this.eventoPagamentoService = eventoPagamentoService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setProvisaoService(ProvisaoService provisaoService) {this.provisaoService = provisaoService;}
	public void setFechamentoFolhaService(FechamentoFolhaService fechamentoFolhaService) {this.fechamentoFolhaService = fechamentoFolhaService;}
	
	@Override
	protected Colaboradordespesa criar(WebRequestContext request, Colaboradordespesa form) throws Exception {		
		if("true".equals(request.getParameter("copiar")) && form.getCdcolaboradordespesa() != null){
			form = colaboradordespesaService.criarCopia(form);
			return form;
		}
		
		Colaboradordespesa colaboradordespesa =  super.criar(request, form);
		
		colaboradordespesa.setSituacao(Colaboradordespesasituacao.EM_ABERTO);
		colaboradordespesa.setDtinsercao(SinedDateUtils.currentDate());
		colaboradordespesa.setValor(new Money());
		
		return colaboradordespesa;
	}
	
	@Override
	protected void listagem(WebRequestContext request, ColaboradordespesaFiltro filtro) throws Exception {
		request.setAttribute("listaSituacao", new ArrayList<Colaboradordespesasituacao>(Arrays.asList(Colaboradordespesasituacao.values())));
		boolean agruparContas = ParametrogeralService.getInstance().getBoolean(Parametrogeral.AGRUPAR_GERACAO_CONTAS_DESPESA_COLABORADOR);
		request.setAttribute("agruparContas", agruparContas);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Colaboradordespesa bean) throws Exception {
		String whereIn = request.getParameter("itenstodelete");
		if(whereIn == null || whereIn.equals("")){
			whereIn = bean.getCdcolaboradordespesa() + "";
		}
		
		List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForAutorizarCancelarEstornar(whereIn);
		List<Apontamento> listaApontamento = new ArrayList<Apontamento>();
		for (Colaboradordespesa colaboradordespesa : listaColaboradordespesa) {
			if(colaboradordespesa.getDtholeriteinicio() != null && colaboradordespesa.getDtholeritefim() != null){
				listaApontamento.addAll(apontamentoService.findByColaborador(colaboradordespesa.getEmpresa(), colaboradordespesa.getColaborador(), colaboradordespesa.getDtholeriteinicio(), colaboradordespesa.getDtholeritefim(), null, colaboradordespesa.getProjeto(), ApontamentoSituacaoEnum.PAGO));
			}
		}
		
		super.excluir(request, bean);
		
		if(listaApontamento != null && !listaApontamento.isEmpty()){
			apontamentoService.updateSituacao(ApontamentoSituacaoEnum.AUTORIZADO, SinedUtil.listAndConcatenate(listaApontamento, "cdapontamento", ","));
		}
	}
	
	@Override
	protected void validateBean(Colaboradordespesa bean, BindException errors) {
		if(bean.getRateio() == null || SinedUtil.isListEmpty(bean.getRateio().getListaRateioitem())){
			errors.reject("001", "Rateio � obrigat�rio.");
		}
		if(fechamentoFolhaService.existeFechamentoPeriodo(bean.getEmpresa(), bean.getDtinsercao())){
			errors.reject("001", "J� existe fechamento de folha correspondente � data informada.");
		}
	}

	@Override
	protected void salvar(WebRequestContext request, Colaboradordespesa bean) throws Exception {
		
		boolean isCriar = bean.getCdcolaboradordespesa() == null;		
		
		try {
			
			super.salvar(request, bean);
			
			Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
			colaboradordespesahistorico.setColaboradordespesa(bean);
			
			if(isCriar){
				colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.CRIADO);
			}
			else{
				colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.ALTERADO);
			}
			
			colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);	
			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_colaboradordespesa_unique")) 
				throw new SinedException("Existe uma despesa com este motivo e mesma data de inser��o j� cadastrada para o colaborador.");
		}	
			
	}	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, ColaboradordespesaFiltro filtro) throws CrudException {
		Integer cdcolaborador = request.getParameter("cdcolaborador") != null && !"".equals(request.getParameter("cdcolaborador")) ? Integer.parseInt(request.getParameter("cdcolaborador")) : null;
		String dtreferencia1 = request.getParameter("dtreferencia1") != null && !"".equals(request.getParameter("dtreferencia1")) ? request.getParameter("dtreferencia1") : null;
		String dtreferencia2 = request.getParameter("dtreferencia2") != null && !"".equals(request.getParameter("dtreferencia2")) ? request.getParameter("dtreferencia2") : null;
		
		if(cdcolaborador != null){
			filtro.setColaborador(colaboradorService.load(new Colaborador(cdcolaborador), "colaborador.nome, colaborador.cdpessoa"));
			
			if(dtreferencia1 != null){
				try {
					filtro.setDtreferenciaHolerite1(SinedDateUtils.stringToDate(dtreferencia1));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(dtreferencia2 != null){
				try {
					filtro.setDtreferenciaHolerite2(SinedDateUtils.stringToDate(dtreferencia2));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		return super.doListagem(request, filtro);
	}
	/**
	*
	*
	* @param request
	* @return
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView abrirJustificativa(WebRequestContext request){
		
		Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
		String whereIn = SinedUtil.getItensSelecionados(request);
		colaboradordespesahistorico.setEntrada("true".equals(request.getParameter("entrada")));	
		colaboradordespesahistorico.setWhereIn(whereIn);
		
		List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForAutorizarCancelarEstornar(whereIn);
		
		String action = request.getParameter("action");
		
		if("cancelar".equals(action)){			
			for (Colaboradordespesa colaboradordespesa : listaColaboradordespesa) {
				if(colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.CANCELADO)){
					request.addError("A situa��o da despesa do colaborador j� est� como: CANCELADO");
					SinedUtil.fechaPopUp(request);
					return null;
				}
			}
			
			request.setAttribute("titulo", "CANCELAR");
			request.setAttribute("acaoSalvar", "cancelar");
			request.setAttribute("labelCampo", "Justificativa para cancelamento");
		}
		else if("estornar".equals(action)){
			for (Colaboradordespesa colaboradordespesa : listaColaboradordespesa){
				if(!colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.AUTORIZADO) &&
						!colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.CANCELADO)){
					request.addError("S� � poss�vel estornar a despesa do colaborador com situa��o 'AUTORIZADO' ou 'CANCELADO'.");
					SinedUtil.fechaPopUp(request);
					return null;
				}
			}
			
			request.setAttribute("titulo", "ESTORNAR");
			request.setAttribute("acaoSalvar", "estornar");
			request.setAttribute("labelCampo", "Justificativa para estorno");
			
		}
		
		return new ModelAndView("direct:/crud/popup/justificativaColaboradordespesa", "bean", colaboradordespesahistorico);
	}
	
	
	
	/**
	* M�todo que autoriza despesa do colaborador
	* S� � poss�vel autorizar despesa de colaborador na situa��o 'EM ABERTO'
	* 
	* @see	br.com.linkcom.sined.geral.service.ColaboradordespesaService#findForAutorizarCancelarEstornar(String whereIn)
	* @see  br.com.linkcom.sined.geral.service.ColaboradordespesaService#autorizar(Colaboradordespesa colaboradordespesa)
	*
	* @param request
	* @return
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView autorizar(WebRequestContext request){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForAutorizarCancelarEstornar(whereIn);
		
		for(Colaboradordespesa colaboradordespesa : listaColaboradordespesa){
			if(!colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.EM_ABERTO)){
				request.addError("S� � poss�vel autorizar despesa de colaborador na situa��o 'EM ABERTO'.");
				return new ModelAndView("redirect:/rh/crud/Colaboradordespesa");
			}
			
			if(fechamentoFolhaService.existeFechamentoPeriodo(colaboradordespesa.getEmpresa(), colaboradordespesa.getDtinsercao())){
				request.addError("N�o foi poss�vel autorizar. J� existe fechamento de folha correspondente � data informada.");
				return new ModelAndView("redirect:/rh/crud/Colaboradordespesa");
			}
		}
		
		for(Colaboradordespesa colaboradordespesa : listaColaboradordespesa){
			colaboradordespesaService.autorizar(colaboradordespesa);
		}
		
		request.addMessage("Autoriza��o(�es) executada(s) com sucesso.");
		if("consultar".equals(request.getParameter("action"))){
			return new ModelAndView("redirect:/rh/crud/Colaboradordespesa?ACAO=consultar&cdcolaboradordespesa=" + whereIn);
		} else {
			return sendRedirectToAction(request.getParameter("action"));
		}
		
	}
	
	
	/**
	* M�todo para cancelar despesa do colaborador
	* S� � poss�vel cancelar se a situa��o da despesa do colaborador for diferente de: CANCELADO e PROCESSADO
	* Se estiver como PROCESSADO, a conta a pagar tem que estar como PREVISTA 
	*
	* @see	br.com.linkcom.sined.geral.service.ColaboradordespesaService#findForAutorizarCancelarEstornar(String whereIn)
	* @see	br.com.linkcom.sined.geral.service.ColaboradordespesaService#cancelar(Colaboradordespesa colaboradordespesa, String obs)
	* 
	* @param request
	* @param colaboradordespesahistorico
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void cancelar(WebRequestContext request, Colaboradordespesahistorico colaboradordespesahistorico){		
		
		String whereIn = colaboradordespesahistorico.getWhereIn();
		String ids = "";
		
		List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForAutorizarCancelarEstornar(whereIn);
		
		for(Colaboradordespesa colaboradordespesa : listaColaboradordespesa){
			if(!ids.equals("")) ids += ",";
			if(colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.CANCELADO)){
				request.addError("A situa��o da despesa do colaborador j� est� como: CANCELADO");
				SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa" + (colaboradordespesahistorico.getEntrada() ? "?ACAO=consultar&cdcolaboradordespesa=" + whereIn : ""));
				return;
			}else if(colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.PROCESSADO)){
				
				Documentoorigem documentoorigem = documentoorigemService.contaColaboradordespesa(colaboradordespesa);
				ids = ids + documentoorigem.getDocumento().getCddocumento(); 
				if(!documentoorigem.getDocumento().getDocumentoacao().equals(Documentoacao.PREVISTA) &&
						!documentoorigem.getDocumento().getDocumentoacao().equals(Documentoacao.CANCELADA)){
					request.addError("N�o � poss�vel cancelar. Pagamento confirmado no Financeiro");
					SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa" + (colaboradordespesahistorico.getEntrada() ? "?ACAO=consultar&cdcolaboradordespesa=" + whereIn : ""));
					return;
				}
				
			}
			if(fechamentoFolhaService.existeFechamentoPeriodo(colaboradordespesa.getEmpresa(), colaboradordespesa.getDtinsercao())){
				request.addError("N�o foi poss�vel cancelar. J� existe fechamento de folha correspondente � data informada.");
				SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa" + (colaboradordespesahistorico.getEntrada() ? "?ACAO=consultar&cdcolaboradordespesa=" + whereIn : ""));
				return;
			}
		}
		
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setIds(ids);
		documentohistorico.setDocumentoacao(Documentoacao.CANCELADA);
		documentohistorico.setObservacao("Conta cancelada a partir do cancelamento da despesa do colaborador.");
		fluxoStatusContaProcess.saveNaoAutorizada(request, documentohistorico);
		
		request.clearMessages();
		
		List<Apontamento> listaApontamento = new ArrayList<Apontamento>();
		for(Colaboradordespesa colaboradordespesa : listaColaboradordespesa){
			if(colaboradordespesa.getDtholeriteinicio() != null && colaboradordespesa.getDtholeritefim() != null){
				listaApontamento.addAll(apontamentoService.findByColaborador(colaboradordespesa.getEmpresa(), colaboradordespesa.getColaborador(), colaboradordespesa.getDtholeriteinicio(), colaboradordespesa.getDtholeritefim(), null, colaboradordespesa.getProjeto(), ApontamentoSituacaoEnum.PAGO));
			}
			
			colaboradordespesaService.cancelar(colaboradordespesa, colaboradordespesahistorico.getObservacao());			
		}	
		
		if(listaApontamento != null && !listaApontamento.isEmpty()){
			apontamentoService.updateSituacao(ApontamentoSituacaoEnum.AUTORIZADO, SinedUtil.listAndConcatenate(listaApontamento, "cdapontamento", ","));
		}
		
		request.addMessage("Cancelamento(s) executado(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa" + (colaboradordespesahistorico.getEntrada() ? "?ACAO=consultar&cdcolaboradordespesa=" + whereIn : ""));
	}
	
	
	
	/**
	* M�todo para estornar despesa do colaborador
	* S� � poss�vel estornar a despesa do colaborador com situa��o 'AUTORIZADO' ou 'CANCELADO'
	*
	* @see	br.com.linkcom.sined.geral.service.ColaboradordespesaService#findForAutorizarCancelarEstornar(String whereIn)
	* @see	br.com.linkcom.sined.geral.service.ColaboradordespesaService#estornar(Colaboradordespesa colaboradordespesa, String observacao)
	* 
	* @param request
	* @param colaboradordespesahistorico
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void estornar(WebRequestContext request, Colaboradordespesahistorico colaboradordespesahistorico){
		
		String whereIn = colaboradordespesahistorico.getWhereIn();
		
		List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForAutorizarCancelarEstornar(whereIn);
		
		for(Colaboradordespesa colaboradordespesa : listaColaboradordespesa){
			if(!colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.AUTORIZADO) &&
					!colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.CANCELADO)){
				request.addError("S� � poss�vel estornar a despesa do colaborador com situa��o 'AUTORIZADO' ou 'CANCELADO'.");
				SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa" + (colaboradordespesahistorico.getEntrada() ? "?ACAO=consultar&cdcolaboradordespesa=" + whereIn : ""));
				return;
			}
			if(fechamentoFolhaService.existeFechamentoPeriodo(colaboradordespesa.getEmpresa(), colaboradordespesa.getDtinsercao())){
				request.addError("N�o foi poss�vel estornar. J� existe fechamento de folha correspondente � data informada.");
				SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa" + (colaboradordespesahistorico.getEntrada() ? "?ACAO=consultar&cdcolaboradordespesa=" + whereIn : ""));
				return;
			}
		}		
		
		for(Colaboradordespesa colaboradordespesa : listaColaboradordespesa){
			colaboradordespesaService.estornar(colaboradordespesa, colaboradordespesahistorico.getObservacao());
		}
		
		request.addMessage("Estorno(s) executado(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa" + (colaboradordespesahistorico.getEntrada() ? "?ACAO=consultar&cdcolaboradordespesa=" + whereIn : ""));
	}
	
	
	/**
	* M�todo para processar despesa do colaborador
	*
	* @param request
	* @param colaboradordespesahistorico
	* @return
	* @since Jun 9, 2011
	* @author Luiz Fernando F Silva
	*/
	@SuppressWarnings("unchecked")
	public void processar(WebRequestContext request, Colaboradordespesa colaboradordespesaDocumento){
		
		boolean possuiRateioEmBranco = false;
		for (Documento documento : colaboradordespesaDocumento.getContas()){
			
			for (Rateioitem rateioitem : documento.getRateio().getListaRateioitem()){
				if (rateioitem.getCentrocusto() == null || rateioitem.getCentrocusto().getCdcentrocusto() == null
						|| rateioitem.getContagerencial() == null || rateioitem.getContagerencial().getCdcontagerencial() == null){
					
					possuiRateioEmBranco = true;
					break;
				}
			}
		}
		
		if(colaboradordespesaDocumento.getDocumentotipo() == null || colaboradordespesaDocumento.getDocumentotipo().equals("") || 
				colaboradordespesaDocumento.getEmpresa() == null || colaboradordespesaDocumento.getEmpresa().equals("")
				|| possuiRateioEmBranco){
			
			request.addError("Dados necess�rios para o processamento n�o foram preenchidos.");
			SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa");
			return ;
		}
	
		for (Documento documento : colaboradordespesaDocumento.getContas()){
			
			List<Colaboradordespesa> listaDespesa = colaboradordespesaService.findForProcessar(documento.getWhereIn());
			
			for (Colaboradordespesa colaboradordespesa : listaDespesa){
				if(!colaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.AUTORIZADO)){
					request.addError("S� � poss�vel processar a despesa do colaborador com situa��o 'AUTORIZADO'.");
					SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa");
					return ;
				}
				if(fechamentoFolhaService.existeFechamentoPeriodo(colaboradordespesa.getEmpresa(), colaboradordespesa.getDtinsercao())){
					request.addError("N�o foi poss�vel processar. J� existe fechamento de folha correspondente � data informada.");
					SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa");
					return ;
				}
			}
			
			if(colaboradordespesaService.getDocumentoclasse(listaDespesa) == null){
				request.addError("N�o � permitido processar despesas que gerem contas a pagar/receber ao mesmo tempo.");
				SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa");
				return ;
			}
		}
			
		for (Documento documento : colaboradordespesaDocumento.getContas()){
			
			List<Colaboradordespesa> listaDespesa = colaboradordespesaService.findForProcessar(documento.getWhereIn());
			
			documento.setDocumentotipo(colaboradordespesaDocumento.getDocumentotipo());
			documento.setEmpresa(colaboradordespesaDocumento.getEmpresa());
			colaboradordespesaService.processar(listaDespesa, documento);
			Object obj = request.getSession().getAttribute(colaboradordespesaDocumento.getIdTela());
			if(obj != null && SinedUtil.isListNotEmpty(((List<Provisao>)obj))){
				List<Provisao> listaProvisao = ((List<Provisao>)obj);
				provisaoService.saveOrUpdateListNoUseTransaction(listaProvisao);
				
				for (Colaboradordespesa colaboradordespesa : listaDespesa){
					Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
					colaboradordespesahistorico.setColaboradordespesa(colaboradordespesa);
					colaboradordespesahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					colaboradordespesahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					colaboradordespesahistorico.setObservacao("Provis�o: " + SinedUtil.makeLinkHistorico(CollectionsUtil.listAndConcatenate(listaProvisao, "cdProvisao", ","), "visualizarProvisao"));
					colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);
				}
				
			}
			
		}
		
		request.addMessage("Processamento(s) executado(s) com sucesso.");
		if("consultar".equals(request.getParameter("action"))){
			SinedUtil.redirecionamento(request,"/rh/crud/Colaboradordespesa?ACAO=consultar&cdcolaboradordespesa=" + colaboradordespesaDocumento.getWhereIn());
		} else {
			SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa");
		}
	}
	
	/**
	* Abre tela para informar os dados necess�rios para gerar o documento
	*
	* @param request
	* @return
	* @since Jul 4, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView abrirTelaProcessar(WebRequestContext request){

		Colaboradordespesa colaboradordespesa = new Colaboradordespesa();

		
		boolean agruparContas = ParametrogeralService.getInstance().getBoolean(Parametrogeral.AGRUPAR_GERACAO_CONTAS_DESPESA_COLABORADOR);
		String whereIn = SinedUtil.getItensSelecionados(request);
		colaboradordespesa.setWhereIn(whereIn);
		
		boolean separarAcrescimos = "true".equalsIgnoreCase(request.getParameter("separarAcrescimos"));
		
		List<Colaboradordespesa> list = colaboradordespesaService.findForAutorizarCancelarEstornar(whereIn);
		for(Colaboradordespesa bean: list){
			bean.setDtVencimentoTrans(bean.getDtdeposito() != null ? bean.getDtdeposito() : SinedDateUtils.currentDate());
		}
		
		List<Colaboradordespesamotivo> listaMotivo = new ArrayList<Colaboradordespesamotivo>();
		for(Colaboradordespesa listcolaboradordespesa : list){
			if(!listcolaboradordespesa.getSituacao().equals(Colaboradordespesasituacao.AUTORIZADO)){
				request.addError("S� � poss�vel processar a despesa do colaborador com situa��o 'AUTORIZADO'.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			if(!listaMotivo.contains(listcolaboradordespesa.getColaboradordespesamotivo())){
				listaMotivo.add(listcolaboradordespesa.getColaboradordespesamotivo());
			}
		}
		if(agruparContas && listaMotivo.size() > 1){
			request.addError("Essa a��o s� � permitida para despesas que possuam o mesmo tipo de remunera��o.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Documentoclasse documentoclasse = colaboradordespesaService.getDocumentoclasse(list); 
		if(documentoclasse == null){
			request.addError("N�o � permitido processar despesas que gere contas a pagar/receber ao mesmo tempo.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Collections.sort(list, new Comparator<Colaboradordespesa>(){
			public int compare(Colaboradordespesa o1, Colaboradordespesa o2) {
				int compareData = o1.getDtVencimentoTrans().compareTo(o2.getDtVencimentoTrans());
				if(compareData != 0){
					return compareData;
				}
				int compareColaborador = o1.getColaborador().getCdpessoa().
					compareTo(o2.getColaborador().getCdpessoa());

				if (compareColaborador != 0)
					return compareColaborador;
				else
					return o1.getColaboradordespesamotivo().getCdcolaboradordespesamotivo().
						compareTo(o2.getColaboradordespesamotivo().getCdcolaboradordespesamotivo());
			}
		});
		
		List<Provisao> listaProvisao = new ArrayList<Provisao>();
		
		//Gerando as contas para exibi��o
		Integer cdcolaboradoranterior = null;
		Integer cdmotivoanterior = null;
		Documento contaCriada = null;
		Date dtVencimento = null;
		Tipooperacao tipoOperacao = Documentoclasse.OBJ_RECEBER.equals(documentoclasse)? Tipooperacao.TIPO_CREDITO: Tipooperacao.TIPO_DEBITO;

		
		for(Colaboradordespesa item : list){
			
			boolean trocouColaborador = cdcolaboradoranterior != null && !cdcolaboradoranterior.equals(item.getColaborador().getCdpessoa());
			boolean trocouMotivo = cdmotivoanterior != null && !cdmotivoanterior.equals(item.getColaboradordespesamotivo().getCdcolaboradordespesamotivo());
			boolean trocouData = dtVencimento != null && !dtVencimento.equals(item.getDtVencimentoTrans());
			
			Money valor = item.getValor();
			Money valortaxadespesa = new Money();
			Money valorCredito = new Money();
			Money valorDebito = new Money();
			for (Colaboradordespesaitem despesaitem : item.getListaColaboradordespesaitem()) {
				if (Colaboradordespesaitemtipo.CREDITO.equals(despesaitem.getEvento().getTipoEvento())){
					valor = valor.add(despesaitem.getValor());
					valorCredito = valorCredito.add(despesaitem.getValor());
				} else {
					valor = valor.subtract(despesaitem.getValor());
					valorDebito = valorDebito.add(despesaitem.getValor());
				}
				if(Boolean.TRUE.equals(despesaitem.getEvento().getProvisionamento())){
					if(despesaitem.getEvento() == null || despesaitem.getEvento().getColaboradorDespesaItemTipoProcessar() == null){
						request.addError("� necess�rio preencher o tipo de opera��o (Processar) no cadastro do evento. " + (despesaitem.getEvento() != null && StringUtils.isNotBlank(despesaitem.getEvento().getNome()) ? "(" + despesaitem.getEvento() .getNome()+ ")" : ""));
						SinedUtil.fechaPopUp(request);
						return null;
					}
					
					Provisao provisao = new Provisao();
					
					provisao.setValor(despesaitem.getValor());
					provisao.setColaborador(item.getColaborador());
					provisao.setEmpresa(item.getEmpresa());
					provisao.setColaboradorDespesaOrigem(item);
					provisao.setEvento(despesaitem.getEvento());
					provisao.setTipo(despesaitem.getEvento().getColaboradorDespesaItemTipoProcessar());
					provisao.setDtaltera(SinedDateUtils.currentTimestamp());
					provisao.setDtProvisao(item.getDtinsercao());
					provisao.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
					
					listaProvisao.add(provisao);
				}
			}
			
			if(valor.getValue().doubleValue() < 0 && !Tipovalorreceber.CONTA_RECEBER.equals(item.getTipovalorreceber())){
				valor = valorCredito;
				valortaxadespesa = valorDebito;
			}
			dtVencimento = item.getDtVencimentoTrans();
			
			if (contaCriada == null || trocouData || ((trocouColaborador || trocouMotivo || !separarAcrescimos) && !agruparContas)){
				
				contaCriada = new Documento();
				contaCriada.setValor(new Money(Math.abs(valor.getValue().doubleValue())));
				contaCriada.setValortaxadespesa(valortaxadespesa);
				if(agruparContas){
					contaCriada.setOutrospagamento("Despesa com pessoal");
					contaCriada.setTipopagamento(Tipopagamento.OUTROS);
					contaCriada.setDtvencimento(dtVencimento);
				}else{
					contaCriada.setPessoa(item.getColaborador());
					contaCriada.setColaborador(item.getColaborador());
					contaCriada.setDtvencimento(dtVencimento);
				}
				contaCriada.setWhereIn(item.getCdcolaboradordespesa().toString());
				contaCriada.setDescricao(item.getColaboradordespesamotivo().getDescricao());
				contaCriada.setDescricao(item.getColaboradordespesamotivo().getDescricao());
				contaCriada.setRateio(new Rateio());
				
				List<Rateioitem> listaRateioItem = rateioitemService.findByRateio(item.getRateio());
				for(Rateioitem ri: listaRateioItem){
					Rateioitem rateioitem = new Rateioitem();
					rateioitem.setProjeto(ri.getProjeto());
					rateioitem.setCentrocusto(ri.getCentrocusto());
					if(tipoOperacao.equals(ri.getContagerencial().getTipooperacao())){
						rateioitem.setContagerencial(ri.getContagerencial());
					}
					rateioitem.setValor(ri.getValor());
					rateioitem.setPercentual(ri.getPercentual());
					
					contaCriada.getRateio().getListaRateioitem().add(rateioitem);
				}
				
				colaboradordespesa.getContas().add(contaCriada);
				
			} else {
				if(agruparContas){
					contaCriada.setValor(contaCriada.getValor().add(new Money(Math.abs(valor.getValue().doubleValue()))));
					contaCriada.setValortaxadespesa(contaCriada.getValortaxadespesa().add(valortaxadespesa));
					contaCriada.setWhereIn(contaCriada.getWhereIn() + "," + item.getCdcolaboradordespesa().toString());
					
					List<Rateioitem> listaRateioItem2 = rateioitemService.findByRateio(item.getRateio());
					
					for(Rateioitem ri: listaRateioItem2){
						Rateioitem rateioitem = getRateioitem(contaCriada, ri.getCentrocusto(), ri.getProjeto(), ri.getContagerencial());
						if(rateioitem == null){
							rateioitem = new Rateioitem();
							rateioitem.setProjeto(ri.getProjeto());
							rateioitem.setCentrocusto(ri.getCentrocusto());
							if(tipoOperacao.equals(ri.getContagerencial().getTipooperacao())){
								rateioitem.setContagerencial(ri.getContagerencial());
							}
							rateioitem.setValor(ri.getValor());
							rateioitem.setPercentual(ri.getPercentual());
						
							contaCriada.getRateio().getListaRateioitem().add(rateioitem);
						}else{
							rateioitem.setValor(rateioitem.getValor().add(ri.getValor()));
						}
					}
				}else{
					contaCriada.setValor(contaCriada.getValor().add(new Money(Math.abs(valor.getValue().doubleValue()))));
					contaCriada.setValortaxadespesa(contaCriada.getValortaxadespesa().add(valortaxadespesa));
					contaCriada.setWhereIn(contaCriada.getWhereIn() + "," + item.getCdcolaboradordespesa().toString());
					
					List<Rateioitem> listaRateioItem = rateioitemService.findByRateio(item.getRateio());
					for(Rateioitem ri: listaRateioItem){
						Rateioitem rateioitem = new Rateioitem();
						rateioitem.setProjeto(ri.getProjeto());
						rateioitem.setCentrocusto(ri.getCentrocusto());
						if(tipoOperacao.equals(ri.getContagerencial().getTipooperacao())){
							rateioitem.setContagerencial(ri.getContagerencial());
						}
						rateioitem.setValor(ri.getValor());
						rateioitem.setPercentual(ri.getPercentual());
						
						contaCriada.getRateio().getListaRateioitem().add(rateioitem);
					}
				}
			}
			cdcolaboradoranterior = item.getColaborador().getCdpessoa();
			cdmotivoanterior = item.getColaboradordespesamotivo().getCdcolaboradordespesamotivo();
			
		}
		if(agruparContas){
			for(Documento doc: colaboradordespesa.getContas()){
				rateioService.ajustaPercentualRateioitem(doc.getValor(), doc.getRateio().getListaRateioitem());
			}
		}
		String idTela = "listaProvisao_"+SinedDateUtils.toString(SinedDateUtils.currentTimestamp(), "dd/MM/yyyy HH:mm");
		colaboradordespesa.setIdTela(idTela);
		request.getSession().setAttribute(idTela, listaProvisao);
		request.setAttribute("separarAcrescimos", separarAcrescimos);
		request.setAttribute("acaoSalvar", "processar");
		request.setAttribute("tipooperacao", Documentoclasse.OBJ_RECEBER.equals(documentoclasse)? "C": "D");
		request.setAttribute("listaCentrocusto", CentrocustoService.getInstance().findAtivos());
		request.setAttribute("listaProjetos", ProjetoService.getInstance().findProjetosAbertos(null));
		request.setAttribute("action", request.getParameter("action") != null ? request.getParameter("action") : "listagem");
		
		return new ModelAndView("direct:/crud/popup/selecionaDadosProcessar", "bean", colaboradordespesa);
		
	}
	
	public Rateioitem getRateioitem(Documento documento, Centrocusto centroCusto, Projeto projeto, Contagerencial contaGerencial){
		for(Rateioitem ri: documento.getRateio().getListaRateioitem()){
			if(((ri.getContagerencial() == null && contaGerencial == null) || contaGerencial != null && contaGerencial.equals(ri.getContagerencial())) &&
				((ri.getProjeto() == null && projeto == null) || projeto != null && projeto.equals(ri.getProjeto())) &&
				((ri.getCentrocusto() == null && centroCusto == null) || centroCusto != null && centroCusto.equals(ri.getCentrocusto()))){
					return ri;
				}
		}
		return null;
	}
	
	/**
	* Abre tela para informar os dados necess�rios para gerar o lan�amento em lote
	*
	* @param request
	* @return
	* @author Giovane Freitas
	*/
	public ModelAndView abrirTelaLancarLote(WebRequestContext request){
		
		ColaboradordespesaFiltro filtro = new ColaboradordespesaFiltro();
		String whereIn = SinedUtil.getItensSelecionados(request);
		filtro.setWhereIn(whereIn);
		
		List<Colaboradordespesa> list = colaboradordespesaService.findForAutorizarCancelarEstornar(whereIn);
		
		for(Colaboradordespesa despesa : list){
			if(!despesa.getSituacao().equals(Colaboradordespesasituacao.AUTORIZADO)){
				request.addError("S� � poss�vel processar a despesa do colaborador com situa��o 'AUTORIZADO'.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			if(fechamentoFolhaService.existeFechamentoPeriodo(despesa.getEmpresa(), despesa.getDtinsercao())){
				request.addError("N�o foi poss�vel processar. J� existe fechamento de folha correspondente � data informada.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			Colaboradordespesaitem colaboradordespesaitem = new Colaboradordespesaitem();
			colaboradordespesaitem.setColaboradordespesa(despesa);
			filtro.getListaColaboradordespesa().add(colaboradordespesaitem);
		}
		
		return new ModelAndView("direct:/crud/popup/lancarEmLote", "bean", filtro );
	}
	

	/**
	* M�todo para realizar o lan�amento em lote das despesas do colaborador
	*
	* @param request
	* @param colaboradordespesahistorico
	* @return
	* @author Giovane Freitas
	*/
	public void lancarEmLote(WebRequestContext request, ColaboradordespesaFiltro filtro){
		
		if(filtro.getEvento() == null || filtro.getEvento().getCdEventoPagamento() == null){
			request.addError("O campo Evento � obrigat�rio.");
			SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa");
			return ;
		}
		
		for (Colaboradordespesaitem item : filtro.getListaColaboradordespesa()){
			item.setEvento(filtro.getEvento());
			colaboradordespesaitemService.saveOrUpdate(item);
			EventoPagamento evento = eventoPagamentoService.loadWithTipo(filtro.getEvento());
			Colaboradordespesa colaboradordespesa = colaboradordespesaService.load(item.getColaboradordespesa());
			Colaboradordespesa beanAux = colaboradordespesaService.loadWithRateio(colaboradordespesa);
			beanAux.getRateio().setListaRateioitem(rateioitemService.findByRateio(beanAux.getRateio()));
			colaboradordespesa.setRateio(beanAux.getRateio());
			Double valorAnterior = colaboradordespesa.getTotal().getValue().doubleValue();
			if (evento.getTipoEvento().equals(Colaboradordespesaitemtipo.CREDITO))
				colaboradordespesa.setTotal(colaboradordespesa.getTotal().add(item.getValor()));
			else
				colaboradordespesa.setTotal(colaboradordespesa.getTotal().subtract(item.getValor()));
			
			if(colaboradordespesa.getTotal().getValue().doubleValue() < 0 && valorAnterior >= 0){
				colaboradordespesa.setTipovalorreceber(Tipovalorreceber.CONTA_RECEBER);
				colaboradordespesaitemService.saveOrUpdate(item);
			}
			rateioService.calculaValorRateioitem(new Money(Math.abs(colaboradordespesa.getTotal().getValue().doubleValue())), colaboradordespesa.getRateio().getListaRateioitem());
			colaboradordespesaService.saveOrUpdate(colaboradordespesa);
		}

		request.addMessage("Lan�amento executado com sucesso.");
		if("consultar".equals(request.getParameter("action"))){
			SinedUtil.redirecionamento(request,"redirect:/rh/crud/Colaboradordespesa?ACAO=consultar&cdcolaboradordespesa=" + filtro.getWhereIn());
		} else {
			SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa");
		}
	}
	
	@Action("createRateioByModelo")
	public void createRateioByModelo(WebRequestContext request, Colaboradordespesa bean){
		if(bean.getRateiomodelo() != null && bean.getRateiomodelo().getCdrateiomodelo() != null){
			Rateio rateio = rateioService.createRateioByModelo(bean.getRateiomodelo());
			View.getCurrent().convertObjectToJs(rateio, "rateio");
		}
	}
	
	public ModelAndView ajaxBuscarTipoEvento(WebRequestContext request, Colaboradordespesaitem bean){
		ModelAndView retorno = new JsonModelAndView();
		String eventoTipo = "";
		if(bean.getEvento() != null && bean.getEvento().getCdEventoPagamento() != null){
			EventoPagamento evento = eventoPagamentoService.load(bean.getEvento());
			if(evento.getTipoEvento() != null){
				eventoTipo = evento.getTipoEvento().name();
			}
		}
		retorno.addObject("eventoTipo", eventoTipo);
		return retorno;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Colaboradordespesa form)
			throws Exception {
		if(form.getCdcolaboradordespesa() != null){
			if(form.getRateio() != null && form.getRateio().getCdrateio() != null){
				form.getRateio().setListaRateioitem(rateioitemService.findByRateio(form.getRateio())); 
			}
		}
		request.setAttribute("listaEvento", eventoPagamentoService.findByEmpresa(null));
		super.entrada(request, form);
	}
	
	public void ajaxCarregaEventosByEmpresa(WebRequestContext request, Colaboradordespesa bean){
		List<EventoPagamento> listaEvento = eventoPagamentoService.findByEmpresa(null);
		View.getCurrent().convertObjectToJs(listaEvento, "listaEvento");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Colaboradordespesa form) throws CrudException {
		if(form.getCdcolaboradordespesa() != null){
			Colaboradordespesa colaboradorDespesa = colaboradordespesaService.load(form);
			if(fechamentoFolhaService.existeFechamentoPeriodo(colaboradorDespesa.getEmpresa(), colaboradorDespesa.getDtinsercao())){
				throw new SinedException("N�o foi poss�vel excluir. J� existe fechamento de folha correspondente � data informada.");
			}
		}else {
			String whereIn = request.getParameter("itenstodelete");
			List<Colaboradordespesa> listaColaboradorDespesa = colaboradordespesaService.findByIds(whereIn);
			if(SinedUtil.isListNotEmpty(listaColaboradorDespesa)){
				for (Colaboradordespesa colaboradorDespesa : listaColaboradorDespesa) {
					if(fechamentoFolhaService.existeFechamentoPeriodo(colaboradorDespesa.getEmpresa(), colaboradorDespesa.getDtinsercao())){
						throw new SinedException("N�o foi poss�vel excluir. J� existe fechamento de folha correspondente � data informada.");
					}
				}				
			}
		}
		return super.doExcluir(request, form);
	}
	
}
