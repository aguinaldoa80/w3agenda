package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Arquivofolhamotivo;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhaorigem;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhatipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivofolhaFiltro extends FiltroListagemSined {
	
	protected Arquivofolhaorigem arquivofolhaorigem;
	protected Arquivofolhatipo arquivofolhatipo;
	protected Arquivofolhamotivo arquivofolhamotivo;
	protected Date dtinsercao1;
	protected Date dtinsercao2;
	protected List<Arquivofolhasituacao> listaSituacao;
	
	public ArquivofolhaFiltro() {
		listaSituacao = new ArrayList<Arquivofolhasituacao>();
		listaSituacao.add(Arquivofolhasituacao.EM_ABERTO);
		listaSituacao.add(Arquivofolhasituacao.AUTORIZADO);
	}
	
	@DisplayName("Origem")
	public Arquivofolhaorigem getArquivofolhaorigem() {
		return arquivofolhaorigem;
	}
	@DisplayName("Tipo")
	public Arquivofolhatipo getArquivofolhatipo() {
		return arquivofolhatipo;
	}
	@DisplayName("Motivo")
	public Arquivofolhamotivo getArquivofolhamotivo() {
		return arquivofolhamotivo;
	}
	public Date getDtinsercao1() {
		return dtinsercao1;
	}
	public Date getDtinsercao2() {
		return dtinsercao2;
	}
	public List<Arquivofolhasituacao> getListaSituacao() {
		return listaSituacao;
	}
	public void setArquivofolhaorigem(Arquivofolhaorigem arquivofolhaorigem) {
		this.arquivofolhaorigem = arquivofolhaorigem;
	}
	public void setArquivofolhatipo(Arquivofolhatipo arquivofolhatipo) {
		this.arquivofolhatipo = arquivofolhatipo;
	}
	public void setArquivofolhamotivo(Arquivofolhamotivo arquivofolhamotivo) {
		this.arquivofolhamotivo = arquivofolhamotivo;
	}
	public void setDtinsercao1(Date dtinsercao1) {
		this.dtinsercao1 = dtinsercao1;
	}
	public void setDtinsercao2(Date dtinsercao2) {
		this.dtinsercao2 = dtinsercao2;
	}
	public void setListaSituacao(List<Arquivofolhasituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	
}
