package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ColaboradorescalaFiltro extends FiltroListagemSined {
	
	private Colaborador colaborador;
	private Date dtescala1;
	private Date dtescala2;
	
	private Contrato contrato;
	
	private Double horaPrevista;
	private Double horaRealizada;
	
	{
		this.dtescala1 = SinedDateUtils.firstDateOfMonth();
		this.dtescala2 = SinedDateUtils.lastDateOfMonth();
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public Date getDtescala1() {
		return dtescala1;
	}

	public Date getDtescala2() {
		return dtescala2;
	}
	
	public Double getHoraPrevista() {
		return horaPrevista;
	}

	public Double getHoraRealizada() {
		return horaRealizada;
	}
	
	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setHoraPrevista(Double horaPrevista) {
		this.horaPrevista = horaPrevista;
	}

	public void setHoraRealizada(Double horaRealizada) {
		this.horaRealizada = horaRealizada;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setDtescala1(Date dtescala1) {
		this.dtescala1 = dtescala1;
	}

	public void setDtescala2(Date dtescala2) {
		this.dtescala2 = dtescala2;
	}
	
	
}
