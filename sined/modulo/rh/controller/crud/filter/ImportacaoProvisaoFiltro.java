package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ImportacaoProvisaoFiltro extends FiltroListagemSined{

	private Empresa empresa;
	private Date dtInicio;
	private Date dtFim;
	
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Date getDtInicio() {
		return dtInicio;
	}
	public Date getDtFim() {
		return dtFim;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
}
