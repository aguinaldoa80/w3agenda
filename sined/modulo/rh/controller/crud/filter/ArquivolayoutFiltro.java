package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivolayoutFiltro extends FiltroListagemSined {
	protected String nome;
	
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
}
