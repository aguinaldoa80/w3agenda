package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ComissionamentoPorFaixasFiltro extends FiltroListagemSined {
	private Cliente cliente;
	private Pedidovendatipo pedidovendatipo;
	private Colaborador colaborador;
	private Date dtinicio = SinedDateUtils.firstDateOfMonth();
	private Date dtfim;
	private Boolean pago;
	
	// Totalizadores
	private Integer quantidade;
	private Money totalVendas;
	private Money totalComissao;
	
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Tipo de Pedido de Venda")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Pago")
	public Boolean getPago() {
		return pago;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setPago(Boolean pago) {
		this.pago = pago;
	}
	
	// Totalizadores
	public Integer getQuantidade() {
		return quantidade;
	}
	public Money getTotalVendas() {
		return totalVendas;
	}
	public Money getTotalComissao() {
		return totalComissao;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public void setTotalComissao(Money totalComissao) {
		this.totalComissao = totalComissao;
	}
	public void setTotalVendas(Money totalVendas) {
		this.totalVendas = totalVendas;
	}
}
