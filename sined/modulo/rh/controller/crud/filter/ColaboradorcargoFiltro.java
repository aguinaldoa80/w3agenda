package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Regimecontratacao;
import br.com.linkcom.sined.geral.service.ColaboradorsituacaoService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ColaboradorcargoFiltro extends FiltroListagemSined {
	
	protected Long matricula;
	protected Colaborador colaborador;
	protected Departamento departamento;
	protected Cargo cargo;
	protected Date dtinicio;
	protected Date dtfim;
	protected Regimecontratacao regimecontratacao;
	protected Empresa empresa;
	protected Projeto projeto;
	protected List<Colaboradorsituacao> colaboradorSituacao;
	protected Date vencimentosituacaoInicio;
	protected Date vencimentosituacaoFim;
	
	public ColaboradorcargoFiltro() {
		colaboradorSituacao = ColaboradorsituacaoService.getInstance().findAll();
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Cargo getCargo() {
		return cargo;
	}
	@DisplayName("De")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("At�")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Regime contrata��o")
	public Regimecontratacao getRegimecontratacao() {
		return regimecontratacao;
	}
	@MaxLength(9)
	@DisplayName("Matr�cula")
	public Long getMatricula() {
		return matricula;
	}
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}
	public void setRegimecontratacao(Regimecontratacao regimecontratacao) {
		this.regimecontratacao = regimecontratacao;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@DisplayName("Situa��o")
	public List<Colaboradorsituacao> getColaboradorSituacao() {
		return colaboradorSituacao;
	}
	public Date getVencimentosituacaoInicio() {
		return vencimentosituacaoInicio;
	}
	public Date getVencimentosituacaoFim() {
		return vencimentosituacaoFim;
	}
	
	public void setColaboradorSituacao(List<Colaboradorsituacao> colaboradorSituacao) {
		this.colaboradorSituacao = colaboradorSituacao;
	}
	public void setVencimentosituacaoInicio(Date vencimentosituacaoInicio) {
		this.vencimentosituacaoInicio = vencimentosituacaoInicio;
	}
	public void setVencimentosituacaoFim(Date vencimentosituacaoFim) {
		this.vencimentosituacaoFim = vencimentosituacaoFim;
	}
}
