package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EventoPagamentoFiltro extends FiltroListagemSined{

	private String nome;
	private ContaContabil contaContabil;
	private Colaboradordespesaitemtipo tipoEvento;
	private Boolean provisionamento;
	private Empresa empresa;
	
	
	public String getNome() {
		return nome;
	}
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	@DisplayName("Tipo de opera��o")
	public Colaboradordespesaitemtipo getTipoEvento() {
		return tipoEvento;
	}
	public Boolean getProvisionamento() {
		return provisionamento;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	public void setTipoEvento(Colaboradordespesaitemtipo tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	public void setProvisionamento(Boolean provisionamento) {
		this.provisionamento = provisionamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
