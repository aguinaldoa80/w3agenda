package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ClassificacaoCandidatoFiltro extends FiltroListagemSined{
	
	protected String nome;

	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
