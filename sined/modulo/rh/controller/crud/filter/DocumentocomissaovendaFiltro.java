package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaoreferenciatipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentocomissaovendaFiltro extends FiltroListagemSined{
	
	protected Colaborador colaboradorvendedor;
	protected Colaborador colaboradorcomissao;
	protected Cliente clientecomissao;
	protected Fornecedor fornecedoragencia;
	protected String mostrar;
	protected String tipocolaborador;
	protected Mes mes;
	protected Integer ano;	
	private Date dtinicio;
	private Date dtfim;
	private Cliente cliente;
	protected Documentocomissaotipoperiodo documentocomissaotipoperiodo;	
	protected Documentocomissaoreferenciatipoperiodo documentocomissaoreferenciatipoperiodo;
	private Money total;
	private Money totalComissao;
	private Materialgrupo materialgrupo;
	private Money totalGeral;
	private Boolean considerarchequedevolvido;
	private Projeto projeto;
	private Boolean retornocanhoto;
	private Boolean exibirComissao;
	
	public DocumentocomissaovendaFiltro() {
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		exibirComissao = Boolean.TRUE;
		if(usuarioLogado.getRestricaovendavendedor() != null && usuarioLogado.getRestricaovendavendedor() && colaborador != null){
			this.colaboradorvendedor = colaborador;
			this.colaboradorcomissao = colaborador;
		}
	}
	
	@DisplayName("Vendedor")
	public Colaborador getColaboradorvendedor() {
		return colaboradorvendedor;
	}
	@DisplayName("Colaborador")
	public Colaborador getColaboradorcomissao() {
		return colaboradorcomissao;
	}

	public String getMostrar() {
		return mostrar;
	}
	public Mes getMes() {
		return mes;
	}
	public Integer getAno() {
		return ano;
	}	
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Refer�ncia do Tipo do Per�odo")
	public Documentocomissaoreferenciatipoperiodo getDocumentocomissaoreferenciatipoperiodo() {
		return documentocomissaoreferenciatipoperiodo;
	}
	@DisplayName("Com registro de canhoto?")
	public Boolean getRetornocanhoto() {
		return retornocanhoto;
	}
	
	public void setRetornocanhoto(Boolean retornocanhoto) {
		this.retornocanhoto = retornocanhoto;
	}
	public void setColaboradorvendedor(Colaborador colaboradorvendedor) {
		this.colaboradorvendedor = colaboradorvendedor;
	}
	public void setDocumentocomissaoreferenciatipoperiodo(
			Documentocomissaoreferenciatipoperiodo documentocomissaoreferenciatipoperiodo) {
		this.documentocomissaoreferenciatipoperiodo = documentocomissaoreferenciatipoperiodo;
	}
	public void setColaboradorcomissao(Colaborador colaboradorcomissao) {
		this.colaboradorcomissao = colaboradorcomissao;
	}
	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}
	public void setMes(Mes mes) {
		this.mes = mes;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@DisplayName("Total p�gina")
	public Money getTotal() {
		return total;
	}

	public void setTotal(Money total) {
		this.total = total;
	}
	
	@DisplayName("Tipo de Per�odo")
	public Documentocomissaotipoperiodo getDocumentocomissaotipoperiodo() {
		return documentocomissaotipoperiodo;
	}
	public void setDocumentocomissaotipoperiodo(Documentocomissaotipoperiodo documentocomissaotipoperiodo) {
		this.documentocomissaotipoperiodo = documentocomissaotipoperiodo;
	}
	public String getTipocolaborador() {
		return tipocolaborador;
	}
	public void setTipocolaborador(String tipocolaborador) {
		this.tipocolaborador = tipocolaborador;
	}
	
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	@DisplayName("Total comiss�o")
	public Money getTotalComissao() {
		return totalComissao;
	}
	public void setTotalComissao(Money totalComissao) {
		this.totalComissao = totalComissao;
	}

	@DisplayName("Total geral")
	public Money getTotalGeral() {
		return totalGeral;
	}
	public void setTotalGeral(Money totalGeral) {
		this.totalGeral = totalGeral;
	}
	@DisplayName("Considerar cheque devolvido")
	public Boolean getConsiderarchequedevolvido() {
		return considerarchequedevolvido;
	}
	public void setConsiderarchequedevolvido(Boolean considerarchequedevolvido) {
		this.considerarchequedevolvido = considerarchequedevolvido;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@DisplayName("Cliente Indica��o")
	public Cliente getClientecomissao() {
		return clientecomissao;
	}

	public void setClientecomissao(Cliente clientecomissao) {
		this.clientecomissao = clientecomissao;
	}

	@DisplayName("Ag�ncia")
	public Fornecedor getFornecedoragencia() {
		return fornecedoragencia;
	}

	public void setFornecedoragencia(Fornecedor fornecedoragencia) {
		this.fornecedoragencia = fornecedoragencia;
	}
	
	@DisplayName("Exibir Comiss�o")
	public Boolean getExibirComissao() {
		return exibirComissao;
	}

	public void setExibirComissao(Boolean exibirComissao) {
		this.exibirComissao = exibirComissao;
	}
}
