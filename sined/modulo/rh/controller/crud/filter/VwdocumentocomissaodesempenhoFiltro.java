package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentodesempenhoconsiderar;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VwdocumentocomissaodesempenhoFiltro extends FiltroListagemSined{
	
	protected Colaborador colaborador;
	protected String mostrar;
	protected Date dtinicio;
	protected Date dtfim;
	protected Comissionamentodesempenhoconsiderar considerar = Comissionamentodesempenhoconsiderar.TOTAL_CONTARECEBER;
	protected Comissionamento comissionamento;
	protected Documentocomissaotipoperiodo documentocomissaotipoperiodo;
	protected Boolean considerardtvenda;
	
	protected Boolean exibirtotais;
	protected Boolean gerarpagamento;
	protected Money totalrepassado;
	protected Money totalvenda;
	protected Money totalarepassar;
	protected Double percentualcomissao;
	protected Money valorcomissao;
	
	@DisplayName("Colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public String getMostrar() {
		return mostrar;
	}
	
	public Date getDtinicio() {
		return dtinicio;
	}
	
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Considerar")
	public Comissionamentodesempenhoconsiderar getConsiderar() {
		return considerar;
	}
	@DisplayName("Tipo de Comiss�o")
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}	
	
	public Boolean getGerarpagamento() {
		return gerarpagamento;
	}	
	public Boolean getExibirtotais() {
		return exibirtotais;
	}	
	public void setConsiderar(Comissionamentodesempenhoconsiderar considerar) {
		this.considerar = considerar;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	
	@DisplayName("Total repassado")
	public Money getTotalrepassado() {
		return totalrepassado;
	}
	@DisplayName("Total de vendas")
	public Money getTotalvenda() {
		return totalvenda;
	}
	@DisplayName("Total a repassar")
	public Money getTotalarepassar() {
		return totalarepassar;
	}
	@DisplayName("Percentual de comiss�o")
	public Double getPercentualcomissao() {
		return percentualcomissao;
	}
	@DisplayName("Valor comiss�o")
	public Money getValorcomissao() {
		return valorcomissao;
	}	
	
	public void setGerarpagamento(Boolean gerarpagamento) {
		this.gerarpagamento = gerarpagamento;
	}
	public void setExibirtotais(Boolean exibirtotais) {
		this.exibirtotais = exibirtotais;
	}
	public void setTotalrepassado(Money totalrepassado) {
		this.totalrepassado = totalrepassado;
	}
	public void setTotalvenda(Money totalvenda) {
		this.totalvenda = totalvenda;
	}
	public void setTotalarepassar(Money totalarepassar) {
		this.totalarepassar = totalarepassar;
	}
	public void setPercentualcomissao(Double percentualcomissao) {
		this.percentualcomissao = percentualcomissao;
	}
	public void setValorcomissao(Money valorcomissao) {
		this.valorcomissao = valorcomissao;
	}
	
	@DisplayName("Tipo de Per�odo")
	public Documentocomissaotipoperiodo getDocumentocomissaotipoperiodo() {
		return documentocomissaotipoperiodo;
	}
	public void setDocumentocomissaotipoperiodo(Documentocomissaotipoperiodo documentocomissaotipoperiodo) {
		this.documentocomissaotipoperiodo = documentocomissaotipoperiodo;
	}
	@DisplayName("Considerar Data da venda")
	public Boolean getConsiderardtvenda() {
		return considerardtvenda;
	}
	public void setConsiderardtvenda(Boolean considerardtvenda) {
		this.considerardtvenda = considerardtvenda;
	}
}
