package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivopontoFiltro extends FiltroListagemSined {
	
	private Projeto projeto;
	private Usuario usuarioinsercao;
	private Boolean processado;
	private Usuario usuarioprocessado;
	
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Inserido por")
	public Usuario getUsuarioinsercao() {
		return usuarioinsercao;
	}
	@DisplayName("Processado?")
	public Boolean getProcessado() {
		return processado;
	}
	@DisplayName("Processado por")
	public Usuario getUsuarioprocessado() {
		return usuarioprocessado;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setUsuarioinsercao(Usuario usuarioinsercao) {
		this.usuarioinsercao = usuarioinsercao;
	}
	public void setProcessado(Boolean processado) {
		this.processado = processado;
	}
	public void setUsuarioprocessado(Usuario usuarioprocessado) {
		this.usuarioprocessado = usuarioprocessado;
	}
	
	
	
}
