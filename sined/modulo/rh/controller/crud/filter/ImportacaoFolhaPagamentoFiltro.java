package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoFolhaPagamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoFolhaPagamentoBeanErro;

public class ImportacaoFolhaPagamentoFiltro {

	private Arquivo arquivo;
	private Colaboradordespesamotivo tipoRemuneracao;
	private List<ImportacaoFolhaPagamentoBean> listaBean;
	private List<ImportacaoFolhaPagamentoBeanErro> listaBeanComErro;
	
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	@Required
	@DisplayName("Tipo de remuneração")
	public Colaboradordespesamotivo getTipoRemuneracao() {
		return tipoRemuneracao;
	}
	public List<ImportacaoFolhaPagamentoBean> getListaBean() {
		return listaBean;
	}
	public List<ImportacaoFolhaPagamentoBeanErro> getListaBeanComErro() {
		return listaBeanComErro;
	}
	
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setTipoRemuneracao(Colaboradordespesamotivo tipoRemuneracao) {
		this.tipoRemuneracao = tipoRemuneracao;
	}
	public void setListaBean(List<ImportacaoFolhaPagamentoBean> listaBean) {
		this.listaBean = listaBean;
	}
	public void setListaBeanComErro(
			List<ImportacaoFolhaPagamentoBeanErro> listaBeanComErro) {
		this.listaBeanComErro = listaBeanComErro;
	}
}
