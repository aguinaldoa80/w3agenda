package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.enumeration.DocumentocomissaoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentocomissaovendedorFiltro extends FiltroListagemSined{

	private Vendedortipo vendedorTipo;
	private Colaborador colaborador;
	private Fornecedor fornecedor;
	private Date dataDe;
	private Date dataAte;
	private Integer cdvenda;
	private Integer cdcontrato;
	private Integer cdpedidovenda;
	private DocumentocomissaoOrigem documentocomissaoOrigem;
	
	public DocumentocomissaovendedorFiltro() {}

	public Vendedortipo getVendedorTipo() {
		return vendedorTipo;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("De")
	public Date getDataDe() {
		return dataDe;
	}
	@DisplayName("At�")
	public Date getDataAte() {
		return dataAte;
	}
	
	@DisplayName("Origem")
	public DocumentocomissaoOrigem getDocumentocomissaoOrigem() {
		return documentocomissaoOrigem;
	}
	
	@DisplayName("C�digo da Venda")
	public Integer getCdvenda() {
		return cdvenda;
	}

	@DisplayName("C�digo do Contrato")
	public Integer getCdcontrato() {
		return cdcontrato;
	}

	@DisplayName("C�digo do Pedido de Venda")
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}

	public void setVendedorTipo(Vendedortipo vendedorTipo) {
		this.vendedorTipo = vendedorTipo;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setDataDe(Date dataDe) {
		this.dataDe = dataDe;
	}

	public void setDataAte(Date dataAte) {
		this.dataAte = dataAte;
	}
	
	public void setDocumentocomissaoOrigem(DocumentocomissaoOrigem documentocomissaoOrigem) {
		this.documentocomissaoOrigem = documentocomissaoOrigem;
	}
	
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}

	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}

	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
}