package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargomaterialseguranca;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Grupoemail;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Regimecontratacao;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.geral.bean.Turma;
import br.com.linkcom.sined.geral.bean.enumeration.ColaboradorFiltroMostrar;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ColaboradorFiltro extends FiltroListagemSined {
	
	protected String nome;
	protected Integer matricula;
	protected String cpf;
	protected String rg;
	protected Sindicato sindicato;
	protected Departamento departamento;
	protected Cargo cargo;
	protected List<Colaboradorsituacao> listaColaboradorsituacao;
	protected Turma turma;
	protected Projeto projeto;
	protected Date dtAdmissaoDe;
	protected Date dtAdmissaoAte;
	protected Mes nascimentoDe;
	protected Mes nascimentoAte;
	protected Empresa empresa;
	protected ColaboradorFiltroMostrar mostrar;
	protected Regimecontratacao regimecontratacao;
	
	protected Contrato contrato;
	protected Escala escala;
	protected Date dtinicio;
	protected Date dtfim;
	protected Hora horainicio;
	protected Hora horafim;
	
	protected Grupoemail grupoemail;
	
	protected String idscontrato;
	protected String idstarefa;
	
	//Atributos para intera��es do detalhe de Equipamento
	private Material material;
	private Localarmazenagem localarmazenagem;
	private Integer quantidadedisponivellocal;
	private Integer indexColaborador;
	protected List<Vgerenciarmaterial> listaGerenciamentomaterial;
	protected Integer cdlocalarmazenagem;
	private List<Cargomaterialseguranca> listaCargomaterialseguranca;
	protected Double quantidadeDesejada;
		
	public ColaboradorFiltro() {
		List<Colaboradorsituacao> listaColaboradorsituacao = new ArrayList<Colaboradorsituacao>();
		listaColaboradorsituacao.add(new Colaboradorsituacao(Colaboradorsituacao.AFASTADO));
		listaColaboradorsituacao.add(new Colaboradorsituacao(Colaboradorsituacao.AVISO_PREVIO));
		listaColaboradorsituacao.add(new Colaboradorsituacao(Colaboradorsituacao.CONTRATO_EXPERIENCIA));
		listaColaboradorsituacao.add(new Colaboradorsituacao(Colaboradorsituacao.EM_CONTRATACAO));
		listaColaboradorsituacao.add(new Colaboradorsituacao(Colaboradorsituacao.FERIAS));
		listaColaboradorsituacao.add(new Colaboradorsituacao(Colaboradorsituacao.NORMAL));
		
		this.listaColaboradorsituacao = listaColaboradorsituacao;		
	}
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	@MaxLength(9)
	@DisplayName("Matr�cula")
	public Integer getMatricula() {
		return matricula;
	}
	
	public Sindicato getSindicato() {
		return sindicato;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Cargo getCargo() {
		return cargo;
	}
	@DisplayName("Situa��o")
	public List<Colaboradorsituacao> getListaColaboradorsituacao() {
		return listaColaboradorsituacao;
	}
	public Turma getTurma() {
		return turma;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	@DisplayName("CPF")
	@MaxLength(11)
	public String getCpf() {
		return cpf;
	}
	public Date getDtAdmissaoDe() {
		return dtAdmissaoDe;
	}
	public Date getDtAdmissaoAte() {
		return dtAdmissaoAte;
	}
	public Grupoemail getGrupoemail() {
		return grupoemail;
	}
	public ColaboradorFiltroMostrar getMostrar() {
		return mostrar;
	}
	@DisplayName("Regime de Contrata��o")
	public Regimecontratacao getRegimecontratacao() {
		return regimecontratacao;
	}
	public void setMostrar(ColaboradorFiltroMostrar mostrar) {
		this.mostrar = mostrar;
	}
	public void setDtAdmissaoDe(Date dtAdmissaoDe) {
		this.dtAdmissaoDe = dtAdmissaoDe;
	}

	public void setDtAdmissaoAte(Date dtAdmissaoAte) {
		this.dtAdmissaoAte = dtAdmissaoAte;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getRg() {
		return rg;
	}
	
	
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public void setSindicato(Sindicato sindicato) {
		this.sindicato = sindicato;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public void setListaColaboradorsituacao(List<Colaboradorsituacao> colaboradorsituacao) {
		this.listaColaboradorsituacao = colaboradorsituacao;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setGrupoemail(Grupoemail grupoemail) {
		this.grupoemail = grupoemail;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Escala getEscala() {
		return escala;
	}

	public Date getDtinicio() {
		return dtinicio;
	}

	public Date getDtfim() {
		return dtfim;
	}

	public Hora getHorainicio() {
		return horainicio;
	}

	public Hora getHorafim() {
		return horafim;
	}

	public void setEscala(Escala escala) {
		this.escala = escala;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setHorainicio(Hora horainicio) {
		this.horainicio = horainicio;
	}

	public void setHorafim(Hora horafim) {
		this.horafim = horafim;
	}

	public Mes getNascimentoDe() {
		return nascimentoDe;
	}
	
	public Mes getNascimentoAte() {
		return nascimentoAte;
	}

	public void setNascimentoDe(Mes nascimentoDe) {
		this.nascimentoDe = nascimentoDe;
	}
	
	public void setNascimentoAte(Mes nascimentoAte) {
		this.nascimentoAte = nascimentoAte;
	}
	
	public String getIdscontrato() {
		return idscontrato;
	}
	
	public void setIdscontrato(String idscontrato) {
		this.idscontrato = idscontrato;
	}

	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setRegimecontratacao(Regimecontratacao regimecontratacao) {
		this.regimecontratacao = regimecontratacao;
	}
	
	//aba Equipamento
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public Integer getIndexColaborador() {
		return indexColaborador;
	}
	public void setIndexColaborador(Integer indexColaborador) {
		this.indexColaborador = indexColaborador;
	}
	public List<Vgerenciarmaterial> getListaGerenciamentomaterial() {
		return listaGerenciamentomaterial;
	}
	public void setListaGerenciamentomaterial(List<Vgerenciarmaterial> listaGerenciamentomaterial) {
		this.listaGerenciamentomaterial = listaGerenciamentomaterial;
	}
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}

	public List<Cargomaterialseguranca> getListaCargomaterialseguranca() {
		return listaCargomaterialseguranca;
	}

	public void setListaCargomaterialseguranca(
			List<Cargomaterialseguranca> listaCargomaterialseguranca) {
		this.listaCargomaterialseguranca = listaCargomaterialseguranca;
	}

	public Integer getQuantidadedisponivellocal() {
		return quantidadedisponivellocal;
	}

	public void setQuantidadedisponivellocal(Integer quantidadedisponivellocal) {
		this.quantidadedisponivellocal = quantidadedisponivellocal;
	}

	public Double getQuantidadeDesejada() {
		return quantidadeDesejada;
	}

	public void setQuantidadeDesejada(Double quantidadeDesejada) {
		this.quantidadeDesejada = quantidadeDesejada;
	}

	public String getIdstarefa() {
		return idstarefa;
	}

	public void setIdstarefa(String idstarefa) {
		this.idstarefa = idstarefa;
	}
	
	
}
