package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProvisaoFiltro extends FiltroListagemSined{

	private Empresa empresa;
	private Colaborador colaborador;
	private EventoPagamento evento;
	private Date dtInicio;
	private Date dtFim;
	
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}

	public EventoPagamento getEvento() {
		return evento;
	}

	public Date getDtInicio() {
		return dtInicio;
	}
	
	public Date getDtFim() {
		return dtFim;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
}
