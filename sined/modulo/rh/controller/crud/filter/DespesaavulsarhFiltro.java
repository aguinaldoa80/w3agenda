package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Despesaavulsarhmotivo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoDespesaAvulsaRH;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DespesaavulsarhFiltro extends FiltroListagemSined {

	private Despesaavulsarhmotivo despesaavulsarhmotivo;
	private Date dtinsercaoinicial;
	private Date dtinsercaofinal;
	private Date dtdepositoinicial;
	private Date dtdepositofinal;
	private Fornecedor fornecedor;
	private List<SituacaoDespesaAvulsaRH> listaSituacao;
	
	public DespesaavulsarhFiltro() {
		listaSituacao = new ListSet<SituacaoDespesaAvulsaRH>(SituacaoDespesaAvulsaRH.class);
		listaSituacao.add(SituacaoDespesaAvulsaRH.EM_ABERTO);
		listaSituacao.add(SituacaoDespesaAvulsaRH.AUTORIZADA);
	}

	@DisplayName("Motivo")
	public Despesaavulsarhmotivo getDespesaavulsarhmotivo() {
		return despesaavulsarhmotivo;
	}

	public void setDespesaavulsarhmotivo(
			Despesaavulsarhmotivo despesaavulsarhmotivo) {
		this.despesaavulsarhmotivo = despesaavulsarhmotivo;
	}

	@DisplayName("Data de inser��o inicial")
	public Date getDtinsercaoinicial() {
		return dtinsercaoinicial;
	}

	public void setDtinsercaoinicial(Date dtinsercao) {
		this.dtinsercaoinicial = dtinsercao;
	}

	@DisplayName("Data do dep�sito inicial")
	public Date getDtdepositoinicial() {
		return dtdepositoinicial;
	}

	public void setDtdepositoinicial(Date dtdeposito) {
		this.dtdepositoinicial = dtdeposito;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@DisplayName("Situa��o")
	public List<SituacaoDespesaAvulsaRH> getListaSituacao() {
		return listaSituacao;
	}

	public void setListaSituacao(List<SituacaoDespesaAvulsaRH> situacao) {
		this.listaSituacao = situacao;
	}

	@DisplayName("Data de inser��o final")
	public Date getDtinsercaofinal() {
		return dtinsercaofinal;
	}

	public void setDtinsercaofinal(Date dtinsercaofinal) {
		this.dtinsercaofinal = dtinsercaofinal;
	}

	@DisplayName("Data do dep�sito final")
	public Date getDtdepositofinal() {
		return dtdepositofinal;
	}

	public void setDtdepositofinal(Date dtdepositofinal) {
		this.dtdepositofinal = dtdepositofinal;
	}

}
