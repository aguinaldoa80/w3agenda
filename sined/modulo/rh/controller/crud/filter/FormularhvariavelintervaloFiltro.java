package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FormularhvariavelintervaloFiltro extends FiltroListagemSined{
	
	protected Integer cdformularhvariavel;
	protected Boolean considerarintervalo;
	protected String nome;	
	protected Boolean ativa;
	protected Integer valor;
	protected Integer cdusuarioaltera;
	protected Date dtaltera;
	
	public Integer getCdformularhvariavel() {
		return cdformularhvariavel;
	}
	public Boolean getConsiderarintervalo() {
		return considerarintervalo;
	}
	public String getNome() {
		return nome;
	}
	public Boolean getAtiva() {
		return ativa;
	}
	public Integer getValor() {
		return valor;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Date getDtaltera() {
		return dtaltera;
	}
	public void setCdformularhvariavel(Integer cdformularhvariavel) {
		this.cdformularhvariavel = cdformularhvariavel;
	}
	public void setConsiderarintervalo(Boolean considerarintervalo) {
		this.considerarintervalo = considerarintervalo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Date dtaltera) {
		this.dtaltera = dtaltera;
	}	
	
}