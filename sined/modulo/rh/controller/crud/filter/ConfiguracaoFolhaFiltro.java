package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ConfiguracaoFolhaFiltro extends FiltroListagemSined{

	private Agendamento agendamento;
	private Empresa empresa;
	private EventoPagamento evento;
	
	
	public Agendamento getAgendamento() {
		return agendamento;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public EventoPagamento getEvento() {
		return evento;
	}
	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}
}
