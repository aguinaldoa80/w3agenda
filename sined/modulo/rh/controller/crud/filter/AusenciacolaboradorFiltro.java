package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Ausenciamotivo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@DisplayName("Filtro de Aus�ncia Colaborador")
public class AusenciacolaboradorFiltro extends FiltroListagemSined{

	private Colaborador colaborador;
	private String descricao;
	private Ausenciamotivo ausenciamotivo;
	private Date dtde;
	private Date dtate;
	private Empresa empresa;
	
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Motivo")
	public Ausenciamotivo getAusenciamotivo() {
		return ausenciamotivo;
	}
	@DisplayName("De")
	public Date getDtde() {
		return dtde;
	}
	@DisplayName("At�")
	public Date getDtate() {
		return dtate;
	}
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAusenciamotivo(Ausenciamotivo ausenciamotivo) {
		this.ausenciamotivo = ausenciamotivo;
	}
	public void setDtde(Date dtde) {
		this.dtde = dtde;
	}
	public void setDtate(Date dtate) {
		this.dtate = dtate;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
