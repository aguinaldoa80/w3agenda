package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Candidatosituacao;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.ClassificacaoCandidato;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CandidatoFiltro extends FiltroListagemSined{
	
	protected String nome;
	protected Sexo sexo;
	protected Candidatosituacao candidatosituacao;
	protected Uf uf;
	protected Municipio municipio;
	protected String curso;
	protected Grauinstrucao grauinstrucao;
	protected List<Candidatosituacao> listaCandidatoSituacao;
	protected String formacaoprofissional;
	protected String objetivo;
	protected Cargo cargo;
	protected ClassificacaoCandidato classificacaoCandidato;
	
	protected Date dtalterainicio;
	protected Date dtalterafim;
	
	public CandidatoFiltro() {
		listaCandidatoSituacao = new ArrayList<Candidatosituacao>();
		listaCandidatoSituacao.add(Candidatosituacao.EM_ABERTO);
	}
	
	  //*************//
	 //**Get & Set**//
	//*************//

	@DisplayName("Nome")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Sexo")
	public Sexo getSexo() {
		return sexo;
	}
	
	@DisplayName("Situa��o")
	public Candidatosituacao getCandidatosituacao() {
		return candidatosituacao;
	}
	
	@DisplayName("Uf")
	public Uf getUf() {
		return uf;
	}
	
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	
	@DisplayName("Curso")
	@MaxLength(100)
	public String getCurso() {
		return curso;
	}

	@DisplayName("Escolaridade")
	public Grauinstrucao getGrauinstrucao() {
		return grauinstrucao;
	}
	
	@DisplayName("Situa��o")
	public List<Candidatosituacao> getListaCandidatoSituacao() {
		return listaCandidatoSituacao;
	}
	
	@DisplayName("Forma��o profissional")
	public String getFormacaoprofissional() {
		return formacaoprofissional;
	}
	
	public Date getDtalterainicio() {
		return dtalterainicio;
	}
	public Date getDtalterafim() {
		return dtalterafim;
	}
	public String getObjetivo() {
		return objetivo;
	}
	
	@DisplayName("Classifica��o Candidato")
	public ClassificacaoCandidato getClassificacaoCandidato() {
		return classificacaoCandidato;
	}
	
	public void setFormacaoprofissional(String formacaoprofissional) {
		this.formacaoprofissional = formacaoprofissional;
	}
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	
	public void setCandidatosituacao(Candidatosituacao candidatosituacao) {
		this.candidatosituacao = candidatosituacao;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	public void setCurso(String curso) {
		this.curso = curso;
	}

	public void setGrauinstrucao(Grauinstrucao grauinstrucao) {
		this.grauinstrucao = grauinstrucao;
	}
	
	public void setListaCandidatoSituacao(
			List<Candidatosituacao> listaCandidatoSituacao) {
		this.listaCandidatoSituacao = listaCandidatoSituacao;
	}
	public void setDtalterainicio(Date dtalterainicio) {
		if(dtalterainicio != null){
			dtalterainicio = SinedDateUtils.dateToBeginOfDay(dtalterainicio);
		}
		this.dtalterainicio = dtalterainicio;
	}
	public void setDtalterafim(Date dtalterafim) {
		if(dtalterafim != null){
			dtalterafim = SinedDateUtils.dataToEndOfDay(dtalterafim);
		}
		this.dtalterafim = dtalterafim;
	}

	@DisplayName("Cargo")
	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public void setClassificacaoCandidato(
			ClassificacaoCandidato classificacaoCandidato) {
		this.classificacaoCandidato = classificacaoCandidato;
	}
	
}
