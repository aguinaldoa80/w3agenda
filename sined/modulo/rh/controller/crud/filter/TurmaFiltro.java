package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Instrutor;
import br.com.linkcom.sined.geral.bean.Treinamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TurmaFiltro extends FiltroListagemSined {
	
	private Treinamento treinamento;
	private Date dtinicio;
	private Date dtfim;
	private Instrutor instrutor;
	private Departamento departamento;
	private Colaborador colaborador;
	private String empresa;
	
	public Treinamento getTreinamento() {
		return treinamento;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Ministrador")
	public Instrutor getInstrutor() {
		return instrutor;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	@DisplayName("Participante")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Empresa responsável")
	@MaxLength(50)
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setTreinamento(Treinamento treinamento) {
		this.treinamento = treinamento;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setInstrutor(Instrutor instrutor) {
		this.instrutor = instrutor;
	}

}
