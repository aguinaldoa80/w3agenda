package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CursoFiltro extends FiltroListagemSined {
	protected String nome;
	protected Date dtinicio;
	
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	@DisplayName("Data de In�cio")
	public Date getDtinicio(){
		return dtinicio;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}	
	
}
