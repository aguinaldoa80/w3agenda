package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FechamentoFolhaFiltro extends FiltroListagemSined {
	
	protected Empresa empresa;
	protected Date dtInicio;
	protected Date dtFim;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data de In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	@Required
	@DisplayName("Data Final")
	public Date getDtFim() {
		return dtFim;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
}
