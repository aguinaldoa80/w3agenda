package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VwcolaboradoretiquetaFiltro extends FiltroListagemSined {

	private Colaboradorsituacao colaboradorsituacao;
	private Departamento departamento;
	private Tipoetiqueta tipoetiqueta;
	private Integer apartirlinha;
	private Integer apartircoluna;
	

	@DisplayName("Situa��o")
	public Colaboradorsituacao getColaboradorsituacao() {
		return colaboradorsituacao;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	@DisplayName("Modelo de etiqueta")
	public Tipoetiqueta getTipoetiqueta() {
		return tipoetiqueta;
	}
	@DisplayName("Imprimir a partir da linha")
	public Integer getApartirlinha() {
		return apartirlinha;
	}
	@DisplayName("Imprimir a partir da coluna")
	public Integer getApartircoluna() {
		return apartircoluna;
	}

	public void setColaboradorsituacao(Colaboradorsituacao colaboradorsituacao) {
		this.colaboradorsituacao = colaboradorsituacao;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setTipoetiqueta(Tipoetiqueta tipoetiqueta) {
		this.tipoetiqueta = tipoetiqueta;
	}
	public void setApartirlinha(Integer apartirlinha) {
		this.apartirlinha = apartirlinha;
	}
	public void setApartircoluna(Integer apartircoluna) {
		this.apartircoluna = apartircoluna;
	}
	
}
