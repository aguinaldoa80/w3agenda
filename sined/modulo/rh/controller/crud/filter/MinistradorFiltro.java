package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MinistradorFiltro extends FiltroListagemSined {
	
	private String nome;
	private Boolean ativo;
	
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
