package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TreinamentoFiltro extends FiltroListagemSined {
	protected String nome;
	protected Boolean mostrar = Boolean.valueOf(true);

	public TreinamentoFiltro() {
	}

	@DisplayName("Mostrar")
	public Boolean getMostrar() {
		return mostrar;
	}

	@DisplayName("T�tulo")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	public void setMostrar(Boolean mostrar) {
		this.mostrar = mostrar;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
