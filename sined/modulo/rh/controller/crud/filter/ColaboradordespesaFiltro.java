package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ColaboradordespesaFiltro extends FiltroListagemSined{
	
	protected Colaboradordespesamotivo motivo;
	protected Colaborador colaborador;
	protected Date dtinsercao;
	protected Date dtdeposito;
	protected List<Colaboradordespesasituacao> listaSituacao;
	protected Money total;
	protected List<Colaboradordespesaitem> listaColaboradordespesa = new ListSet<Colaboradordespesaitem>(Colaboradordespesaitem.class);
	protected Colaboradordespesaitemtipo tipo;
	protected String whereIn;
	protected EventoPagamento evento;
	
	protected Date dtreferenciaHolerite1;
	protected Date dtreferenciaHolerite2;
	
	public ColaboradordespesaFiltro() {
		listaSituacao = new ArrayList<Colaboradordespesasituacao>();
		listaSituacao.add(Colaboradordespesasituacao.EM_ABERTO);
		listaSituacao.add(Colaboradordespesasituacao.AUTORIZADO);
	}
	
	@DisplayName ("Tipo de remuneração")
	public Colaboradordespesamotivo getMotivo() {
		return motivo;
	}
	
	@DisplayName ("Colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@DisplayName("Data de referência")
	public Date getDtinsercao() {
		return dtinsercao;
	}
	
	@DisplayName("Data de pagamento")
	public Date getDtdeposito() {
		return dtdeposito;
	}
	
	@DisplayName("Total")
	public Money getTotal() {
		return total;
	}	

	public List<Colaboradordespesasituacao> getListaSituacao() {
		return listaSituacao;
	}
	
	public List<Colaboradordespesaitem> getListaColaboradordespesa() {
		return listaColaboradordespesa;
	}
	
	public Colaboradordespesaitemtipo getTipo() {
		return tipo;
	}
	
	public String getWhereIn() {
		return whereIn;
	}
	
	public EventoPagamento getEvento() {
		return evento;
	}
	
	public void setMotivo(Colaboradordespesamotivo motivo) {
		this.motivo = motivo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtinsercao(Date dtinsercao) {
		this.dtinsercao = dtinsercao;
	}
	public void setDtdeposito(Date dtdeposito) {
		this.dtdeposito = dtdeposito;
	}

	public void setListaSituacao(List<Colaboradordespesasituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	
	public void setListaColaboradordespesa(List<Colaboradordespesaitem> listaColaboradordespesa) {
		this.listaColaboradordespesa = listaColaboradordespesa;
	}
	
	public void setTipo(Colaboradordespesaitemtipo tipo) {
		this.tipo = tipo;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public Date getDtreferenciaHolerite1() {
		return dtreferenciaHolerite1;
	}

	public Date getDtreferenciaHolerite2() {
		return dtreferenciaHolerite2;
	}

	public void setDtreferenciaHolerite1(Date dtreferenciaHolerite1) {
		this.dtreferenciaHolerite1 = dtreferenciaHolerite1;
	}

	public void setDtreferenciaHolerite2(Date dtreferenciaHolerite2) {
		this.dtreferenciaHolerite2 = dtreferenciaHolerite2;
	}
	
	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}

}
