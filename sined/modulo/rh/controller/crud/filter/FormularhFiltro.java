package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FormularhFiltro extends FiltroListagemSined{

	protected String descricao;
	protected Cargo cargo;
	protected Boolean ativo;
	
	public String getDescricao() {
		return descricao;
	}
	public Cargo getCargo() {
		return cargo;
	}		
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

}
