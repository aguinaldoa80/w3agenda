package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Formularhvariavel;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FormularhvariavelFiltro extends FiltroListagemSined{
	
	protected String nome;
	protected Boolean ativa;
	protected Integer valor;
	protected String idLista;
	protected List<Formularhvariavel> listaFormulas;
	
	public String getNome() {
		return nome;
	}
	public Boolean getAtiva() {
		return ativa;
	}
	public Integer getValor() {
		return valor;
	}	
	public List<Formularhvariavel> getListaFormulas() {
		return listaFormulas;
	}	
	public String getIdLista() {
		return idLista;
	}
	public void setIdLista(String idLista) {
		this.idLista = idLista;
	}
	public void setListaFormulas(List<Formularhvariavel> listaFormulas) {
		this.listaFormulas = listaFormulas;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
}
