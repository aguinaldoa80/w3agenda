package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MunicipioFiltro extends FiltroListagemSined {
	protected String nome;
	protected Uf uf;
	
	@DisplayName("Nome")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	public Uf getUf() {
		return uf;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
}
