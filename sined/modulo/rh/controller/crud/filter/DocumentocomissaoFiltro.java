package br.com.linkcom.sined.modulo.rh.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaoreferenciatipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.bean.enumeration.Visaorelatorio;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentocomissaoFiltro extends FiltroListagemSined{
	
	protected Vendedortipo vendedortipo;
	protected Colaborador colaborador;
	protected Fornecedor fornecedor;
	protected String mostrar;
	protected Documentocomissaotipoperiodo documentocomissaotipoperiodo;
	protected Documentocomissaoreferenciatipoperiodo documentocomissaoreferenciatipoperiodo;
	protected Mes mes;
	protected Integer ano;
	protected Pedidovendatipo pedidovendatipo;
	
	private Date dtinicio;
	private Date dtfim;
	private Cliente cliente;
	private Contrato contrato;	
	private Contratotipo contratotipo;
	private Visaorelatorio visaorelatorio;
	
	private Money total;
	private Money totaldocumento;
	private Money totalcomissao;
	
	public DocumentocomissaoFiltro() {
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(usuarioLogado.getRestricaovendavendedor() != null && usuarioLogado.getRestricaovendavendedor() && colaborador != null){
			this.vendedortipo = Vendedortipo.COLABORADOR;
			this.colaborador = colaborador;
		}
	}
	
	@DisplayName("Tipo do vendedor")
	public Vendedortipo getVendedortipo() {
		return vendedortipo;
	}
	
	@DisplayName("Colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	@DisplayName("M�s")
	public Mes getMes() {
		return mes;
	}

	@DisplayName("Ano")
	public Integer getAno() {
		return ano;
	}
	
	@DisplayName("Tipo de Pedido de Venda")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	
	@Required
	@DisplayName("Tipo do per�odo")
	public Documentocomissaotipoperiodo getDocumentocomissaotipoperiodo() {
		return documentocomissaotipoperiodo;
	}	
	
	@DisplayName("Refer�ncia do Tipo do Per�odo")
	public Documentocomissaoreferenciatipoperiodo getDocumentocomissaoreferenciatipoperiodo() {
		return documentocomissaoreferenciatipoperiodo;
	}
	
	public String getMostrar() {
		return mostrar;
	}
	
	public void setDocumentocomissaoreferenciatipoperiodo(
			Documentocomissaoreferenciatipoperiodo documentocomissaoreferenciatipoperiodo) {
		this.documentocomissaoreferenciatipoperiodo = documentocomissaoreferenciatipoperiodo;
	}
	public void setVendedortipo(Vendedortipo vendedortipo) {
		this.vendedortipo = vendedortipo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}
	public void setMes(Mes mes) {
		this.mes = mes;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Contrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@DisplayName("Tipo de contrato")
	public Contratotipo getContratotipo() {
		return contratotipo;
	}
	@DisplayName("Vis�o")
	public Visaorelatorio getVisaorelatorio() {
		return visaorelatorio;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}
	public void setVisaorelatorio(Visaorelatorio visaorelatorio) {
		this.visaorelatorio = visaorelatorio;
	}
	public void setDocumentocomissaotipoperiodo(Documentocomissaotipoperiodo documentocomissaotipoperiodo) {
		this.documentocomissaotipoperiodo = documentocomissaotipoperiodo;
	}

	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}

	public Money getTotaldocumento() {
		return totaldocumento;
	}

	public Money getTotalcomissao() {
		return totalcomissao;
	}

	public void setTotaldocumento(Money totaldocumento) {
		this.totaldocumento = totaldocumento;
	}

	public void setTotalcomissao(Money totalcomissao) {
		this.totalcomissao = totalcomissao;
	}
	
	
}
