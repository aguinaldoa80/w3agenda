package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.sql.Timestamp;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivoponto;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ArquivopontoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Arquivoponto", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdarquivoponto", "arquivo", "arquivosalario", "dtinicio", "dtfim", "processado", "observacao"})
public class ArquivopontoCrud extends CrudControllerSined<ArquivopontoFiltro, Arquivoponto, Arquivoponto> {

	@Override
	protected Arquivoponto criar(WebRequestContext request, Arquivoponto form) throws Exception {
		Arquivoponto bean = super.criar(request, form);
		
		// VALORES PADR�ES
		bean.setDtinsercao(new Timestamp(System.currentTimeMillis()));
		bean.setUsuarioinsercao(SinedUtil.getUsuarioLogado());
		
		return bean;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Arquivoponto bean) throws Exception {
		if(bean.getProcessado() == null) bean.setProcessado(Boolean.FALSE);
		super.salvar(request, bean);
	}
	
}
