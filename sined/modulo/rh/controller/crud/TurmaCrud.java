package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Turma;
import br.com.linkcom.sined.geral.bean.Turmaparticipante;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.InstrutorService;
import br.com.linkcom.sined.geral.service.TreinamentoService;
import br.com.linkcom.sined.geral.service.TurmaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.TurmaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Turma", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"treinamento", "periodo", "instrutor"})
public class TurmaCrud extends CrudControllerSined<TurmaFiltro, Turma, Turma> {
	
	private ColaboradorService colaboradorService;
	private TurmaService turmaService;
	private InstrutorService instrutorService;
	private TreinamentoService treinamentoService;
	
	public void setTreinamentoService(TreinamentoService treinamentoService) {
		this.treinamentoService = treinamentoService;
	}
	public void setInstrutorService(InstrutorService instrutorService) {
		this.instrutorService = instrutorService;
	}	
	public void setTurmaService(TurmaService turmaService) {
		this.turmaService = turmaService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Turma bean)throws Exception {
		Set<Turmaparticipante> listaTurmaparticipante = bean.getListaTurmaparticipante();
		if (bean != null && listaTurmaparticipante != null) {
			for (Turmaparticipante turmaparticipante : listaTurmaparticipante) {
				turmaparticipante.setTurma(bean);
			}
			bean.setListaTurmaparticipante(listaTurmaparticipante);
		}
		
		super.salvar(request, bean);
	}	
	
	@Override
	protected void listagem(WebRequestContext request, TurmaFiltro filtro)throws Exception {
		request.setAttribute("listaInstrutor", instrutorService.findAtivos());
		request.setAttribute("listaTreinamento", treinamentoService.findAtivos());
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Turma form) throws Exception {
		Set<Turmaparticipante> lista = form.getListaTurmaparticipante();
		
		if (form.getCdturma() != null && form.getListaTurmaparticipante() != null) {
			if (!request.getBindException().hasErrors()) {
				Object[] array = null;
				Set<Colaboradorcargo> listaCargo = null;
				for (Turmaparticipante turmaparticipante : lista) {
					listaCargo = turmaparticipante.getColaborador().getListaColaboradorcargo();
					if (listaCargo != null) {
						array = listaCargo.toArray();
						turmaparticipante.setDepartamento(array[0] != null ? ((Colaboradorcargo)array[0]).getDepartamento() : null);
					} else {
						turmaparticipante.setDepartamento(null);
					}
					
				}
			}
		}
		
		request.setAttribute("listaInstrutor", instrutorService.findAtivos(form.getInstrutor()));
		request.setAttribute("listaTreinamento", treinamentoService.findAtivos(form.getTreinamento()));
		request.setAttribute("tamanholista", lista != null ? lista.size() : 0);
		
		super.entrada(request, form);
	}
	
	/**
	 * Popula o combo de box de colaborador, via AJAX.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView comboBox(WebRequestContext request, Departamento departamento) {
		departamento = departamento.getDepartamento();
		List<Colaborador> listaColaborador = colaboradorService.findByDepartamento(departamento);
		return new JsonModelAndView().addObject("listaColaborador", listaColaborador);
	}
	
	@Override
	protected void validateBean(Turma bean, BindException errors) {
		
		Set<Turmaparticipante> listaTurmaparticipante = bean.getListaTurmaparticipante();
		if (listaTurmaparticipante != null) {
			Object[] array = listaTurmaparticipante.toArray();
			
			boolean ok = false;
			Colaborador colaborador = null;
			for (int i = 0; i < array.length; i++) {
				colaborador = ((Turmaparticipante)array[i]).getColaborador();
				for (int j = 0; j < array.length; j++) {
					if (i != j) {
						if (colaborador.equals(((Turmaparticipante)array[j]).getColaborador())) {
							errors.reject("001","Participante(s) escolhido(s) mais de uma vez.");
							ok = true;
							break;
						}
					}
				}
				if (ok) {
					break;
				}
			}
			
			List<String> listaNomes = new ArrayList<String>();
			for (Turmaparticipante turmaparticipante : listaTurmaparticipante) {
				Long numTurma = turmaService.findTurmaConflitante(turmaparticipante.getColaborador(),bean);
				if (numTurma != null && numTurma > 0) {
					listaNomes.add(colaboradorService.load(turmaparticipante.getColaborador(), "colaborador.nome").getNome());
				}
			}
			
			if (listaNomes != null && listaNomes.size() > 0) {
				for (String string : listaNomes) {
					errors.reject("001","O participante "+string+" est� alocado em outro treinamento neste per�odo.");
				}
			}
		}
		
		if (bean.getInstrutor() != null) {
			Long numTurmaConflitante = turmaService.findTurmaConflitante(bean.getInstrutor(), bean);
			if (numTurmaConflitante != null && numTurmaConflitante > 0) {
				errors.reject("001","Ministrador(a) est� alocado(a) em outro treinamento neste per�odo.");
			}
		}
		
		
	}
	
}
