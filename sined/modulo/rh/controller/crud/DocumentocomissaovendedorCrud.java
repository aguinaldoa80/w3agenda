package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.DocumentocomissaoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.DocumentocomissaovendedorService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendedorFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Documentocomissaovendedor", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"pessoa", "valorcomissao", "percentualcomissao", "origem"})
public class DocumentocomissaovendedorCrud extends CrudControllerSined<DocumentocomissaovendedorFiltro, Documentocomissao, Documentocomissao>{
	
	public static final String ORIGEM_TIPO_COMISSAO_VENDEDOR = "OrigemTipoComissaoVendedor";
	public static final String ORIGEM_COMISSAO_VENDEDOR = "OrigemComissaoVendedor";
	private FornecedorService fornecedorService;
	private ColaboradorService colaboradorService;
	private DocumentocomissaovendedorService documentocomissaovendedorService;
	
	public void setDocumentocomissaovendedorService(DocumentocomissaovendedorService documentocomissaovendedorService) {
		this.documentocomissaovendedorService = documentocomissaovendedorService;
	}

	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}

	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}

	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, DocumentocomissaovendedorFiltro filtro) {
		return new ModelAndView("crud/documentocomissaovendedorListagem");
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Documentocomissao form) {
		return new ModelAndView("crud/documentocomissaovendedorEntrada");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Documentocomissao form)throws Exception {
		if (form.getCddocumentocomissao() != null){
			if (form.getColaboradorcomissao() != null && form.getColaboradorcomissao().getCdcolaboradorcomissao() != null){
				form.setPagoDocumentoComissaoVendedor(true);
			}
			else form.setPagoDocumentoComissaoVendedor(false);

					
			if (form.getVendedortipo() != null){
				if (form.getVendedortipo().equals(Vendedortipo.COLABORADOR)){
					if (form.getColaborador() == null){
						form.setColaborador(colaboradorService.load(new Colaborador(form.getPessoa().getCdpessoa())));
					}
					else form.setPessoa(form.getColaborador()); 
				}
				else if (form.getVendedortipo().equals(Vendedortipo.FORNECEDOR)){
					if(form.getFornecedor() == null){
						form.setFornecedor(fornecedorService.load(form.getPessoa().getCdpessoa()));
					}
					else form.setPessoa(form.getFornecedor());
				} 
			}
			else {
				Colaborador colaborador = colaboradorService.load(new Colaborador(form.getPessoa().getCdpessoa()));
				if (colaborador!= null && colaborador.getCdpessoa() !=null){
					form.setVendedortipo(Vendedortipo.COLABORADOR);
					form.setColaborador(colaborador);
				}
				else {
					Fornecedor fornecedor = fornecedorService.load(form.getPessoa().getCdpessoa());
					if(fornecedor != null && fornecedor.getCdpessoa() != null){
						form.setVendedortipo(Vendedortipo.FORNECEDOR);
						form.setFornecedor(fornecedor);
					}
				}
			}
			
			if (form.getVenda() != null){
				form.setDocumentocomissaoOrigem(DocumentocomissaoOrigem.VENDA);
				request.getSession().setAttribute(ORIGEM_TIPO_COMISSAO_VENDEDOR, DocumentocomissaoOrigem.VENDA);
				request.getSession().setAttribute(ORIGEM_COMISSAO_VENDEDOR, form.getVenda());
			}
			else if (form.getPedidovenda() != null){
				form.setDocumentocomissaoOrigem(DocumentocomissaoOrigem.PEDIDOVENDA);
				request.getSession().setAttribute(ORIGEM_TIPO_COMISSAO_VENDEDOR, DocumentocomissaoOrigem.PEDIDOVENDA);
				request.getSession().setAttribute(ORIGEM_COMISSAO_VENDEDOR, form.getPedidovenda());
			}
			else if (form.getContrato() != null){
				form.setDocumentocomissaoOrigem(DocumentocomissaoOrigem.CONTRATO);
				request.getSession().setAttribute(ORIGEM_TIPO_COMISSAO_VENDEDOR, DocumentocomissaoOrigem.CONTRATO);
				request.getSession().setAttribute(ORIGEM_COMISSAO_VENDEDOR, form.getContrato());
			}
			else {
				request.getSession().setAttribute(ORIGEM_TIPO_COMISSAO_VENDEDOR, null);
				request.getSession().setAttribute(ORIGEM_COMISSAO_VENDEDOR, null);
			}
		}
		
	}
	
	@Override
	protected void salvar(WebRequestContext request, Documentocomissao bean) throws Exception {
		documentocomissaovendedorService.ajusteSave(bean);
		
		if (bean.getFornecedor()!=null ){
			bean.setPessoa(bean.getFornecedor());
		}
		else if (bean.getColaborador()!=null ){
			bean.setPessoa(bean.getColaborador());
		}
		
		if (bean.getCddocumentocomissao() != null){
			documentocomissaovendedorService.alterarDocumentoComissaoVendedor (bean);
		}
		else super.salvar(request, bean);
	}
	
	@Override
	protected void validateBean(Documentocomissao bean, BindException errors) {	

		if(bean.getVendedortipo().equals(Vendedortipo.COLABORADOR)){
			if (bean.getColaborador() == null){
				errors.reject("001", "Colaborador deve ser informado.");
			}		
		}
		else if(bean.getVendedortipo().equals(Vendedortipo.FORNECEDOR)){
			if (bean.getFornecedor() == null){
				errors.reject("001", "Fornecedor deve ser informado.");
			}
		}
			
		if(bean.getDocumentocomissaoOrigem().equals(DocumentocomissaoOrigem.VENDA)){
			if (bean.getVenda() == null){
				errors.reject("002", "Venda deve ser informada.");
			}
		}
		else if(bean.getDocumentocomissaoOrigem().equals(DocumentocomissaoOrigem.CONTRATO)){
			if (bean.getContrato() == null){
				errors.reject("002", "Contrato deve ser informado.");
			}
		}
		else if(bean.getDocumentocomissaoOrigem().equals(DocumentocomissaoOrigem.PEDIDOVENDA)){
			if (bean.getPedidovenda() == null){
				errors.reject("002", "Pedido de Venda deve ser informado.");
			}
		}

		super.validateBean(bean, errors);
	}
	
	public ModelAndView setSessaoOrigemVenda (WebRequestContext request, Venda origem){
		if (origem.getCdvenda() != null){
			request.getSession().setAttribute(ORIGEM_TIPO_COMISSAO_VENDEDOR, DocumentocomissaoOrigem.VENDA);
			request.getSession().setAttribute(ORIGEM_COMISSAO_VENDEDOR, origem);
		}
		return new JsonModelAndView().addObject("success", true);
	
	}
	
	public ModelAndView setSessaoOrigemContrato (WebRequestContext request, Contrato origem){
		if (origem.getCdcontrato() != null){
			request.getSession().setAttribute(ORIGEM_TIPO_COMISSAO_VENDEDOR, DocumentocomissaoOrigem.CONTRATO);
			request.getSession().setAttribute(ORIGEM_COMISSAO_VENDEDOR, origem);
		}
		return new JsonModelAndView().addObject("success", true);
	
	}
	
	public ModelAndView setSessaoOrigemPedidovenda (WebRequestContext request, Pedidovenda origem){
		if (origem.getCdpedidovenda() != null){
			request.getSession().setAttribute(ORIGEM_TIPO_COMISSAO_VENDEDOR, DocumentocomissaoOrigem.PEDIDOVENDA);
			request.getSession().setAttribute(ORIGEM_COMISSAO_VENDEDOR, origem);
		}
		return new JsonModelAndView().addObject("success", true);
	}
	
	/**
	* M�todo que abre a popup para atualizar o valor ou percentual da comiss�o
	*
	* @param request
	* @param documentocomissao
	* @return
	* @since 01/09/2014
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopupAtualizarValorPercentual(WebRequestContext request, Documentocomissao documentocomissao){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum iten selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		documentocomissao.setWhereIn(whereIn);
		request.setAttribute("acaoSalvar", "saveAtualizarValorPercentual");
		request.setAttribute("entrada", Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"));
		return new ModelAndView("direct:/crud/popup/popUpAtualizarValorPercentualComissao", "bean", documentocomissao);
	}
	
	/**
	* M�todo para atualizar o valor ou percentual da comiss�o
	*
	* @see br.com.linkcom.sined.geral.service.DocumentocomissaovendedorService#atualizarValorPercentual(Money valor, Money percentual, String whereIn)
	*
	* @param request
	* @param documentocomissao
	* @since 01/09/2014
	* @author Luiz Fernando
	*/
	public void saveAtualizarValorPercentual(WebRequestContext request, Documentocomissao documentocomissao){
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");  
		String whereIn = documentocomissao.getWhereIn();
		
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum iten selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		if(documentocomissao.getPercentualcomissao() == null && documentocomissao.getValorcomissao() == null){
			request.addError("� preciso informar o percentual ou valor para atualizar a comiss�o.");
			SinedUtil.fechaPopUp(request);
			return;
		}else if(documentocomissao.getPercentualcomissao() != null && documentocomissao.getValorcomissao() != null){
			request.addError("N�o � permitido informar o percentual e valor para atualizar a comiss�o.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		try {
			documentocomissaovendedorService.atualizarValorPercentual(documentocomissao.getValorcomissao(), 
					documentocomissao.getPercentualcomissao(), whereIn);
			request.addMessage("Comiss�o atualizada com sucesso");
		} catch (Exception e) {
			request.addError("Erro ao atualizar a comiss�o: " + e.getMessage());
		}
		
		SinedUtil.redirecionamento(request, "/rh/crud/Documentocomissaovendedor" + (entrada ? "?ACAO=consultar&cddocumentocomissao="+whereIn : ""));
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,
			DocumentocomissaovendedorFiltro filtro) throws CrudException {
		if(!filtro.isNotFirstTime()){
			filtro.setDocumentocomissaoOrigem(DocumentocomissaoOrigem.VENDA);
		}
		return super.doListagem(request, filtro);
	}
}
