package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.MunicipioFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Municipio", authorizationModule=CrudAuthorizationModule.class)
public class MunicipioCrud extends CrudControllerSined<MunicipioFiltro, Municipio, Municipio> {
	
	@Override
	protected void salvar(WebRequestContext request, Municipio bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_MUNICIPIO_NOME")) throw new SinedException("Cidade j� cadastrada no sistema.");
			
		}
	}	
	
	@Override
	protected void listagem(WebRequestContext request, MunicipioFiltro filtro)throws Exception {
		super.listagem(request, filtro);
	}
}
