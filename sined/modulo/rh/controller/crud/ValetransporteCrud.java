package br.com.linkcom.sined.modulo.rh.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Valetransporte;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path="/sistema/crud/Valetransporte", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class ValetransporteCrud extends CrudControllerSined<FiltroListagemSined, Valetransporte, Valetransporte> {

}
