package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentocomissaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Documentocomissao", authorizationModule=CrudAuthorizationModule.class)
public class DocumentocomissaoCrud extends CrudControllerSined<DocumentocomissaoFiltro, Documentocomissao, Documentocomissao>{
	
	private DocumentocomissaoService documentocomissaoService;
	private ContratoService contratoService;
	private UsuarioService usuarioService;
	
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {this.documentocomissaoService = documentocomissaoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}

	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected ListagemResult<Documentocomissao> getLista(WebRequestContext request, DocumentocomissaoFiltro filtro) {
		ListagemResult<Documentocomissao> listagemResult = super.getLista(request, filtro);
		List<Documentocomissao> list = listagemResult.list();
				
		documentocomissaoService.ajustaDadosListagem(list);
		
		filtro.setTotal(documentocomissaoService.findForTotalDocumentocomissao(filtro));
		filtro.setTotaldocumento(documentocomissaoService.findForTotaldocumentoDocumentocomissao(filtro));
		
		request.setAttribute("exibirnegociados", Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo()) ? false : true);
		return listagemResult;
	}
	
	@Override
	protected void listagem(WebRequestContext request,  DocumentocomissaoFiltro filtro) throws Exception {
		List<String> listaMostrar = new ArrayList<String>();
		List<String> listaPeriodovencimentopagamento = new ArrayList<String>();
		
		listaMostrar.add("Pagos com pend�ncia de repasse");
		listaMostrar.add("Todos");
		
		listaPeriodovencimentopagamento.add("Vencimento");
		listaPeriodovencimentopagamento.add("Pagamento");
		
		request.setAttribute("listaMostrar", listaMostrar);
		request.setAttribute("listaPeriodovencimentopagamento", listaPeriodovencimentopagamento);
		
		if(filtro.getContrato() != null && filtro.getContrato().getCdcontrato() != null)
			request.setAttribute("contratofiltro", "br.com.linkcom.sined.geral.bean.Contrato[cdcontrato=" +filtro.getContrato().getCdcontrato() + "]");
		
		Boolean isUsuarioLogadoRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado());
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(isUsuarioLogadoRestricaoVendaVendedor != null && isUsuarioLogadoRestricaoVendaVendedor && colaborador != null){
			filtro.setColaborador(colaborador);
			filtro.setVendedortipo(Vendedortipo.COLABORADOR);
			request.setAttribute("haveResticaoVendaVendedor", Boolean.TRUE);
		}
		
		super.listagem(request, filtro);
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	/**
	* M�todo ajax para buscar os contratos relacionado ao cliente (Utilizado para o combo de contrato)
	*
	* @param request
	* @since Nov 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxPreencheComboContrato(WebRequestContext request, DocumentocomissaoFiltro filtro){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		try{
			List<Contrato> lista = contratoService.findByClienteContratotipo(filtro.getCliente(), filtro.getContratotipo());
			String listaContrato = SinedUtil.convertToJavaScript(lista, "listaContrato", null);
			view.println(listaContrato);
			view.println("var sucesso = true;");
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}

}
