package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Candidato;
import br.com.linkcom.sined.geral.bean.Candidatosituacao;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.geral.service.CandidatoService;
import br.com.linkcom.sined.geral.service.CandidatosituacaoService;
import br.com.linkcom.sined.geral.service.ClassificacaoCandidatoService;
import br.com.linkcom.sined.geral.service.GrauinstrucaoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.CandidatoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

import com.ibm.icu.text.SimpleDateFormat;


@CrudBean
@Controller(path="/rh/crud/Candidato", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "email", "municipio", "curso", "instituicao", "cargo", "candidatosituacao"})
public class CandidatoCrud extends CrudControllerSined<CandidatoFiltro, Candidato, Candidato>{
	
	protected CandidatoService candidatoService;
	protected MunicipioService municipioService;
	protected CandidatosituacaoService candidatosituacaoService;
	protected GrauinstrucaoService grauinstrucaoService;
	protected UfService ufService;
	protected ClassificacaoCandidatoService classificacaoCandidatoService;
	
	
	
	public void setCandidatoService(CandidatoService candidatoService) {
		this.candidatoService = candidatoService;
	}
	
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	
	public void setCandidatosituacaoService(
			CandidatosituacaoService candidatosituacaoService) {
		this.candidatosituacaoService = candidatosituacaoService;
	}
	
	public void setGrauinstrucaoService(
			GrauinstrucaoService grauinstrucaoService) {
		this.grauinstrucaoService = grauinstrucaoService;
	}
	
	public void setClassificacaoCandidatoService(
			ClassificacaoCandidatoService classificacaoCandidatoService) {
		this.classificacaoCandidatoService = classificacaoCandidatoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Candidato form)throws Exception {
		request.setAttribute("hoje", new SimpleDateFormat("dd/MM/yyyy").format(new java.sql.Date(System.currentTimeMillis())));
		List<Candidatosituacao> listCandidadoSituacao = new ArrayList<Candidatosituacao>();
		listCandidadoSituacao.add(Candidatosituacao.EM_ABERTO);
		listCandidadoSituacao.add(Candidatosituacao.ELIMINADO);
		listCandidadoSituacao.add(Candidatosituacao.CONTRATADO);
		form.getArquivo();
		request.setAttribute("listCandidadoSituacao", listCandidadoSituacao);
		if(EDITAR.equals(request.getParameter("ACAO"))){
			form.setDtaltera(new Timestamp(System.currentTimeMillis()));
		}
		
	}
	
	
	protected void listagem(WebRequestContext request, CandidatoFiltro filtro) throws Exception {

		List<Candidatosituacao> listaCandidatoSituacao = candidatosituacaoService.createListaForChecklist();
		List<Grauinstrucao> listaGrauinstrucao = grauinstrucaoService.findAll();
		
		request.setAttribute("listaCandidatoSituacaoCompleta", listaCandidatoSituacao);
		request.setAttribute("listaGrauinstrucaoCompleta", listaGrauinstrucao);
		
		super.listagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Candidato bean) throws Exception {
		
		boolean contratado = false;

		try {	
			if(bean.getCandidatosituacao().getCdcandidatosituacao() == 3){
				contratado = true;
			}
			request.setAttribute("iscontratado", contratado);
			super.salvar(request, bean);
		
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_candidato_cpf")) throw new SinedException("Cpf j� cadastrado!");
			else throw e;
		}
	}
	
	@Action("redirect")
	public ModelAndView redirect(Integer cdcandidato){
		return new ModelAndView("redirect:rh/crud/Colaborador?ACAO=criar&cdcandidato=" + cdcandidato);
	}
		
}
