package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ClassificacaoCandidato;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ClassificacaoCandidatoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/ClassificacaoCandidato", authorizationModule=CrudAuthorizationModule.class)
public class ClassificacaoCandidatoCrud extends CrudControllerSined<ClassificacaoCandidatoFiltro, ClassificacaoCandidato, ClassificacaoCandidato>{

	@Override
	protected void salvar(WebRequestContext request, ClassificacaoCandidato bean)throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_classificacaocandidato_nome")) {
				throw new SinedException("Classifica��o j� cadastrado no sistema.");
			}			
		}		
	}	
}
