package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cargoexameobrigatorio;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorcargohistorico;
import br.com.linkcom.sined.geral.bean.Colaboradorexame;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradorcargohistoricoacao;
import br.com.linkcom.sined.geral.service.CargoatividadeService;
import br.com.linkcom.sined.geral.service.CargoexameobrigatorioService;
import br.com.linkcom.sined.geral.service.ColaboradorcargoService;
import br.com.linkcom.sined.geral.service.ColaboradorcargohistoricoService;
import br.com.linkcom.sined.geral.service.ColaboradorexameService;
import br.com.linkcom.sined.geral.service.ColaboradorsituacaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EpiService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RiscotrabalhoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorcargoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Colaboradorcargo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"matricula", "colaborador", "departamento", "cargo", "dtinicio", "dtfim"})
public class ColaboradorcargoCrud extends CrudControllerSined<ColaboradorcargoFiltro, Colaboradorcargo, Colaboradorcargo> {
	
	private EmpresaService empresaService;
	private ColaboradorcargoService colaboradorcargoService;
	private CargoatividadeService cargoatividadeService;
	private RiscotrabalhoService riscotrabalhoService;
	private EpiService epiService;
	private ColaboradorcargohistoricoService colaboradorcargohistoricoService;
	private ColaboradorsituacaoService colaboradorsituacaoService;
	private ProjetoService projetoService;
	private CargoexameobrigatorioService cargoexameobrigatorioService;
	private ColaboradorexameService colaboradorexameService;
	
	public void setCargoexameobrigatorioService(
			CargoexameobrigatorioService cargoexameobrigatorioService) {
		this.cargoexameobrigatorioService = cargoexameobrigatorioService;
	}
	public void setColaboradorexameService(
			ColaboradorexameService colaboradorexameService) {
		this.colaboradorexameService = colaboradorexameService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setColaboradorcargoService(
			ColaboradorcargoService colaboradorcargoService) {
		this.colaboradorcargoService = colaboradorcargoService;
	}
	public void setCargoatividadeService(
			CargoatividadeService cargoatividadeService) {
		this.cargoatividadeService = cargoatividadeService;
	}
	public void setRiscotrabalhoService(
			RiscotrabalhoService riscotrabalhoService) {
		this.riscotrabalhoService = riscotrabalhoService;
	}
	public void setEpiService(EpiService epiService) {
		this.epiService = epiService;
	}
	public void setColaboradorcargohistoricoService(ColaboradorcargohistoricoService colaboradorcargohistoricoService) {
		this.colaboradorcargohistoricoService = colaboradorcargohistoricoService;
	}
	public void setColaboradorsituacaoService(ColaboradorsituacaoService colaboradorsituacaoService) {
		this.colaboradorsituacaoService = colaboradorsituacaoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, ColaboradorcargoFiltro filtro) throws Exception {
		request.setAttribute("listaSituacoes", colaboradorsituacaoService.findAll());
		super.listagem(request, filtro);
	}
	
	@Override
	protected Colaboradorcargo criar(WebRequestContext request, Colaboradorcargo form) throws Exception {
		Colaboradorcargo bean = super.criar(request, form);
		
		String paramCdpessoa = request.getParameter("cdpessoa");
		if(paramCdpessoa != null && !paramCdpessoa.equals("")){
			bean.setColaborador(new Colaborador(Integer.parseInt(paramCdpessoa)));
		}
		
		String paramDtadmissao = request.getParameter("dtinicio");
		if(paramDtadmissao != null && !paramDtadmissao.equals("")){
			bean.setDtinicio(SinedDateUtils.stringToDate(paramDtadmissao));
		}
		
		return bean;
	}
	
	/**
	 * M�todo respons�vel por verificar se a a��o est� relacionada � altera��o de cargos de colaboradores
	 * 
	 * @author Taidson
	 * @since 13/05/2010
	 */
	public ModelAndView validaAlteracaoCargo(WebRequestContext request, Colaboradorcargo form)throws Exception{
		if(SinedUtil.permissaoAcaoTela(request, CRIAR)){
			form.setIdColaboradores(request.getParameter("selectedItens"));
			setInfoForTemplate(request, form);
			setEntradaDefaultInfo(request, form);
			entrada(request, form);
		}
		return getEntradaModelAndView(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Colaboradorcargo form)throws Exception {
		if(form.getCargo() != null){
			request.setAttribute("listaGrafia", cargoatividadeService.getAtividadesByCargo(form.getCargo()));
			request.setAttribute("listaRiscos", riscotrabalhoService.getRiscosByCargo(form.getCargo()));
		}
		request.setAttribute("listaMaterialEpi", epiService.findAllEpi());	
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));	
		
		List<Projeto> projeto = new ArrayList<Projeto>();
		if(form.getProjeto() != null){
			projeto.add(form.getProjeto());
		}
		List<Projeto> listaProjeto = projetoService.findForComboByUsuariologado(projeto, true);
		request.setAttribute("listaProjeto", listaProjeto);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Colaboradorcargo bean) {
		if(bean.getIdColaboradores() != null && !bean.getIdColaboradores().equals("")){
			return new ModelAndView ("redirect:/rh/process/Alterarcargocolaborador");
		}else return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	protected void validateBean(Colaboradorcargo bean, BindException errors) {
		if(colaboradorcargoService.existeColaboradorCargoData(bean) && bean.getIdColaboradores() == null){
			errors.reject("001", "J� existe cargo neste mesmo per�odo.");
		}
	}
	
	/**
	 * M�todo invocado via ajax para carregar as atividades referentes ao cargo
	 * 
	 * @param request
	 * @param cargo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView comboBoxCargoAtividade(WebRequestContext request, Colaboradorcargo colaboradorcargo) {
		return new JsonModelAndView().addObject("listaAtividade", cargoatividadeService.getAtividadesByCargo(colaboradorcargo.getCargo()));
	}

	/**
	 * M�todo invocado via ajax para carregar os riscos de trabalho referentes ao cargo
	 * 
	 * @param request
	 * @param colaboradorcargo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView comboBoxRisco(WebRequestContext request, Colaboradorcargo colaboradorcargo) {
		return new JsonModelAndView().addObject("listaRisco", riscotrabalhoService.getRiscosByCargo(colaboradorcargo.getCargo()));
	}
	
	@Override
	protected void salvar(WebRequestContext request, Colaboradorcargo bean) throws Exception {
		
		try {
			if(bean != null){
				if(bean.getIdColaboradores() == null){
					Colaboradorcargo colaboradorcargoAtual = colaboradorcargoService.findCargoAtual(bean.getColaborador());
					
					boolean usarSequencial = bean.getMatriculaautomatico()!= null && bean.getMatriculaautomatico();
					boolean isCriar = bean.getCdcolaboradorcargo() == null;
					
					Integer cdcolaboradorcargoAtual = colaboradorcargoAtual != null && colaboradorcargoAtual.getEmpresa() != null && colaboradorcargoAtual.getEmpresa().getCdpessoa() != null ? colaboradorcargoAtual.getEmpresa().getCdpessoa() : null;
					Integer cdcolaboradorcargo = bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null ? bean.getEmpresa().getCdpessoa() : null;
					
					if(bean.getEmpresa() != null && ((usarSequencial && isCriar) || (cdcolaboradorcargoAtual != null && cdcolaboradorcargo != null && !cdcolaboradorcargoAtual.equals(cdcolaboradorcargo)))){
						Integer proximoNumeroMatricula = empresaService.carregaProximoNumeroMatricula(bean.getEmpresa());
						if(proximoNumeroMatricula == null){
							proximoNumeroMatricula = 1;
						}
						empresaService.updateProximoNumeroMatricula(bean.getEmpresa(), proximoNumeroMatricula + 1);
						bean.setMatricula(proximoNumeroMatricula.longValue());
					}
					
					Colaboradorcargohistorico colaboradorcargohistorico = new Colaboradorcargohistorico();
					
					colaboradorcargohistorico.setColaboradorcargo(bean);
					colaboradorcargohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					colaboradorcargohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					
					if(bean.getCdcolaboradorcargo() == null){
						colaboradorcargohistorico.setAcao(Colaboradorcargohistoricoacao.CRIADO);
					}else{
						colaboradorcargohistorico.setAcao(Colaboradorcargohistoricoacao.ALTERADO);
					}
					
					// verfica se houve mudanca na situacao e se o vencimento foi preenchido ou alterado
					// caso o vencimento nao tenha sido alterado apagar o vencimento
					Colaboradorcargo colaboradorBD = bean.getCdcolaboradorcargo() != null ?  colaboradorcargoService.load(bean) : null;
					if(colaboradorBD != null && !bean.getColaboradorsituacao().equals(colaboradorBD.getColaboradorsituacao())) {
						if(colaboradorBD.getVencimentosituacao()!= null && colaboradorBD.getVencimentosituacao().equals(bean.getVencimentosituacao())) {
							bean.setVencimentosituacao(null);
						}
					}
					
					super.salvar(request, bean);
					colaboradorcargohistoricoService.saveOrUpdate(colaboradorcargohistorico);
					
					if(verificaExamesObrigatorios(bean)){
						request.addMessage("<br/>Existem exames obrigat�rios para esse cargo, adicionados ao cadastro do colaborador." +
								"<br/>Ver aba 'Exames Cl�nicos' no cadastro do colaborador.<br/>", MessageType.WARN);
					}
					
				}else{
					super.salvar(request, bean);
					
					boolean examesSalvos = false;
					
					for(String id : bean.getIdColaboradores().split(",")){
						Colaborador colaborador = new Colaborador(Integer.valueOf(id));
						bean.setColaborador(colaborador);
						
						if(verificaExamesObrigatorios(bean)){
							examesSalvos = true;
						}
					}

					if(examesSalvos){
						request.addMessage("<br/>Existem exames obrigat�rios para esse cargo, adicionados ao(s) cadastro(s) do(s) colaborador(es)." +
								"<br/>Ver aba 'Exames Cl�nicos' no cadastro do colaborador.<br/>", MessageType.WARN);
					}
				}
			}
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_colaboradorcargo_cdcolaboradorcargo")) 
				throw new SinedException("J� cadastrado no sistema.");	
		}
		
	}
	
	/**
	 * M�todo que Verifica se o cargo tem exames obrigat�rios e inclui no cadastro de exames do colaborador.<br/>
	 * A��o tem efeito nas telas de cadastro de cargos do colaborador e na tela de altera��o de cargo do colaborador. 
	 * @param Colaboradorcargo bean
	 * @return boolean
	 * @author Danilo Guimar�es
	 */
	protected boolean verificaExamesObrigatorios(Colaboradorcargo bean){
		boolean existeExame = false;
		
		List<Cargoexameobrigatorio> listaCargoexameobrigatorio = cargoexameobrigatorioService.findExameobrigatorioByCargo(bean.getCargo());
		List<Colaboradorexame> aux_listaColaboradorexame = new ArrayList<Colaboradorexame>();
		if(listaCargoexameobrigatorio != null && !listaCargoexameobrigatorio.isEmpty()){
			List<Colaboradorexame> listaColaboradorexame = colaboradorexameService.findExamesByColaborador(bean.getColaborador());
			if(listaColaboradorexame != null && !listaColaboradorexame.isEmpty()){
				for(Cargoexameobrigatorio cargoexame : listaCargoexameobrigatorio){
					for(Colaboradorexame colaboradorexame : listaColaboradorexame){
						if((cargoexame.getExamenatureza().getCdexamenatureza()).equals(colaboradorexame.getExamenatureza().getCdexamenatureza())){
							// Inclui os exames vencidos
							if(colaboradorexame.getDtexame() != null){
								Date dataProximoExame = SinedDateUtils.incrementDateFrequencia(SinedDateUtils.castUtilDateForSqlDate(colaboradorexame.getDtexame()), cargoexame.getFrequencia(), 1);
								Date hoje = SinedDateUtils.currentDate();
								if(SinedDateUtils.diferencaDias(hoje, dataProximoExame) >= 0){
									Colaboradorexame exame = new Colaboradorexame();
									exame.setColaborador(bean.getColaborador());
									exame.setExamenatureza(cargoexame.getExamenatureza());
									exame.setDtexame(SinedDateUtils.currentDate());
									aux_listaColaboradorexame.add(exame);
									break;
								}
							}
						} else {
							Colaboradorexame exame = new Colaboradorexame();
							exame.setColaborador(bean.getColaborador());
							exame.setExamenatureza(cargoexame.getExamenatureza());
							exame.setDtexame(SinedDateUtils.currentDate());
							aux_listaColaboradorexame.add(exame);
							break;
						}
					}
				}
			} else {
				for(Cargoexameobrigatorio cargoexame : listaCargoexameobrigatorio){
					Colaboradorexame exame = new Colaboradorexame();
					exame.setColaborador(bean.getColaborador());
					exame.setExamenatureza(cargoexame.getExamenatureza());
					exame.setDtexame(SinedDateUtils.currentDate());
					aux_listaColaboradorexame.add(exame);
				}
			}
		}
		
		if(aux_listaColaboradorexame != null && !aux_listaColaboradorexame.isEmpty()){
			//salva os registros de exames do colaborador
			for(Colaboradorexame item : aux_listaColaboradorexame){
				colaboradorexameService.saveOrUpdate(item);
			}
			existeExame = true;
		}
		
		return existeExame;
	}
	
	@Override
	protected void excluir(WebRequestContext request, Colaboradorcargo bean)
			throws Exception {
		String itens = request.getParameter("itenstodelete");
		if(itens == null || itens.equals("")){
			itens = bean.getCdcolaboradorcargo() + "";
		}
		List<Colaboradorcargo> listaColaboradorCargo = colaboradorcargoService.carregaColaboradoresCargos(itens);
		List<Colaboradorcargo> listaColaboradorCargoExclusao;
		for (Colaboradorcargo colaboradorcargo : listaColaboradorCargo) {
			listaColaboradorCargoExclusao = new ArrayList<Colaboradorcargo>();
			listaColaboradorCargoExclusao = colaboradorcargoService.findListColaboradorCargoForColaborador(colaboradorcargo.getColaborador());
			if (listaColaboradorCargoExclusao.size() < 2){
				throw new SinedException("Cargo n�o pode ser exclu�do, � necess�rio que o colaborador "+colaboradorcargo.getColaborador().getNome()+" tenha a menos um cargo.");
			} else {
				genericService.delete(colaboradorcargo);
				request.addMessage("Cargo do colaborador "+colaboradorcargo.getColaborador().getNome()+" exclu�do com sucesso!");
			}
		}
	}
}
