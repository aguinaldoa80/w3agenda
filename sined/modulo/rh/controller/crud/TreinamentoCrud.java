package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Treinamento;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.TreinamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Treinamento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class TreinamentoCrud extends CrudControllerSined<TreinamentoFiltro, Treinamento, Treinamento> {
	
	
	@Override
	protected void salvar(WebRequestContext request, Treinamento bean)throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_treinamento_nome")) {throw new SinedException("T�tulo j� cadastrado no sistema.");}	
		}
		
	}
	
	@Override
	protected void validateBean(Treinamento bean, BindException errors) {
		
	}
	
}
