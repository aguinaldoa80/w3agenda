package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.service.ComissionamentoPorFaixasService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ComissionamentoPorFaixasFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@ExportCSV(fields = "")
@Controller(path="/rh/crud/ComissionamentoPorFaixas", authorizationModule=CrudAuthorizationModule.class)
public class ComissionamentoPorFaixasCrud extends CrudControllerSined<ComissionamentoPorFaixasFiltro, Documentocomissao, Documentocomissao> {
	
	private ComissionamentoPorFaixasService comissionamentoPorFaixasService;
	private VendamaterialService vendamaterialService;
	
	public void setComissionamentoPorFaixasService(ComissionamentoPorFaixasService comissionamentoPorFaixasService) {this.comissionamentoPorFaixasService = comissionamentoPorFaixasService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}

	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, ComissionamentoPorFaixasFiltro filtro) {
		return new ModelAndView("crud/comissionamentoPorFaixasListagem");
	}
	
	@Override
	protected ListagemResult<Documentocomissao> getLista(WebRequestContext request, ComissionamentoPorFaixasFiltro filtro) {
		comissionamentoPorFaixasService.preencheTotalizadores(filtro);
		ListagemResult<Documentocomissao> lista = comissionamentoPorFaixasService.findForListagem(filtro);
		Map<Venda, List<Vendamaterial>> mapa = new HashMap<Venda, List<Vendamaterial>>();
		for(Documentocomissao bean: lista.list()){
			if(Util.objects.isPersistent(bean.getVenda())){
				if(!mapa.containsKey(bean.getVenda())){
					mapa.put(bean.getVenda(), vendamaterialService.findForTotalizacaoVenda(bean.getVenda()));
				}
				bean.getVenda().setListavendamaterial(mapa.get(bean.getVenda()));
			}
		}
		return lista;
	}
	
	@Override
	public ModelAndView doExportar(WebRequestContext request, ComissionamentoPorFaixasFiltro filtro) throws CrudException, IOException {
		String [] campos = {};
		Resource resource = comissionamentoPorFaixasService.generateCSV(filtro, campos);
		return new ResourceModelAndView(resource);
	}
}
