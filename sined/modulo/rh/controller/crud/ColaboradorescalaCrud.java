package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorescala;
import br.com.linkcom.sined.geral.bean.enumeration.ColaboradorescalaSituacao;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradorescalaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorescalaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Colaboradorescala", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"dtescala", "diaSemana", "contrato", "horaInicioFormat", "horaFimFormat", "horaprevista", "horarealizada", "situacao"})
public class ColaboradorescalaCrud extends CrudControllerSined<ColaboradorescalaFiltro, Colaboradorescala, Colaboradorescala> {
	
	private ColaboradorescalaService colaboradorescalaService;
	private ColaboradorService colaboradorService;
	private ApontamentoService apontamentoService;
	
	public void setApontamentoService(ApontamentoService apontamentoService) {this.apontamentoService = apontamentoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setColaboradorescalaService(ColaboradorescalaService colaboradorescalaService) {this.colaboradorescalaService = colaboradorescalaService;}
	
	@Override
	protected void entrada(WebRequestContext request, Colaboradorescala form) throws Exception {
		
		if(form.getCdcolaboradorescala() == null){
			//CRIAR
			this.setSituacaoEdicao(request);
			
		} else {
			if(EDITAR.equals(request.getParameter("ACAO")) && form.getSituacao().equals(ColaboradorescalaSituacao.FALTA)){
				throw new SinedException("N�o � poss�vel editar um resgistro com a situa��o 'FALTA'.");
			}
			
			//EDITAR
			if(form.getSituacao().equals(ColaboradorescalaSituacao.TRABALHO) || form.getSituacao().equals(ColaboradorescalaSituacao.FOLGA)){
				this.setSituacaoEdicao(request);
			} 
			
		}
		
	}
	
	@Override
	protected void listagem(WebRequestContext request, ColaboradorescalaFiltro filtro) throws Exception {
		colaboradorescalaService.totalizarListagem(filtro);				
	}
	
	@Override
	protected void excluir(WebRequestContext request, Colaboradorescala bean) throws Exception {
		Colaboradorescala falta = null;
		if("true".equals(request.getParameter("SUBSTITUTA"))){
			falta = colaboradorescalaService.getBeanFalta(bean);
		}
		super.excluir(request, bean);
		bean.setEscalasubstituida(falta);
	}
	
	@Override
	protected ModelAndView getExcluirModelAndView(WebRequestContext request, Colaboradorescala bean) {
		if("true".equals(request.getParameter("SUBSTITUTA"))){
			return new ModelAndView("redirect:/rh/crud/Colaboradorescala?ACAO=consultar&cdcolaboradorescala=" + bean.getEscalasubstituida().getCdcolaboradorescala());
		} else {
			return super.getExcluirModelAndView(request, bean);
		}
	}

	/**
	 * Cria a lista de situa��es com 'FOLGA' e 'TRABALHO' e coloca na requisi��o.
	 *
	 * @param request
	 * @author Rodrigo Freitas
	 */
	private void setSituacaoEdicao(WebRequestContext request) {
		List<ColaboradorescalaSituacao> lista = new ArrayList<ColaboradorescalaSituacao>();
		lista.add(ColaboradorescalaSituacao.FOLGA);
		lista.add(ColaboradorescalaSituacao.TRABALHO);
		
		request.setAttribute("listaSituacao", lista);
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Action("lancarFalta")
	public ModelAndView lancarFalta(WebRequestContext request, Colaboradorescala colaboradorescala){
		List<Colaboradorescala> list = new ArrayList<Colaboradorescala>();
		list.add(new Colaboradorescala());
		
		colaboradorescala = colaboradorescalaService.loadForEntrada(colaboradorescala);
		if(apontamentoService.getTotalHoras(colaboradorescala) > 0){
			request.addError("N�o � poss�vel inserir faltas em data com apontamento.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		colaboradorescala.setListaSubstituto(list);
		colaboradorescala.setEscolheSubtituto(true);
		
		Colaborador colaborador = colaboradorescalaService.getColaborador(colaboradorescala);
		
		List<Colaborador> listaColaborador = colaboradorService.findAll();
		listaColaborador.remove(colaborador);
		
		request.setAttribute("substituto", request.getParameter("SUBSTITUTO"));
		request.setAttribute("listaColaborador", listaColaborador);
		
		return new ModelAndView("direct:/crud/popup/lancarFalta", "colaboradorescala", colaboradorescala);
	}
	
	@Action("geraFalta")
	public ModelAndView geraFalta(WebRequestContext request, Colaboradorescala colaboradorescala) {
		
		List<Colaboradorescala> listaSubstituto = colaboradorescala.getListaSubstituto();
		Colaboradorescala load = colaboradorescalaService.loadForEntrada(colaboradorescala);
		
		if(colaboradorescala.getEscolheSubtituto() != null && colaboradorescala.getEscolheSubtituto()){
			for (Colaboradorescala ce : listaSubstituto) {
				ce.setContrato(load.getContrato());
				ce.setDtescala(load.getDtescala());
				ce.setEscalasubstituida(load);
				ce.setSituacao(ColaboradorescalaSituacao.SUBSTITUTO);
				
				colaboradorescalaService.saveOrUpdate(ce);
			}
		}
		
		colaboradorescalaService.atualizaSituacao(load, ColaboradorescalaSituacao.FALTA);		
		
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	@Action("estornarFalta")
	public ModelAndView estornarFalta(WebRequestContext request, Colaboradorescala colaboradorescala) {
		
		colaboradorescalaService.atualizaSituacao(colaboradorescala, ColaboradorescalaSituacao.TRABALHO);		
		colaboradorescalaService.deletaSubstitutos(colaboradorescala);
		
		return new ModelAndView("redirect:/rh/crud/Colaboradorescala");
	}
	
	@Action("gerarEscala")
	public ModelAndView gerarEscala(WebRequestContext request, ColaboradorescalaFiltro filtro){
		
		try{
			colaboradorescalaService.gerarEscala(filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o de escala: " + e.getMessage());
		}
		
		return new ModelAndView("redirect:/rh/crud/Colaboradorescala");
	}
	
}
