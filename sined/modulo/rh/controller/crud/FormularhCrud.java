package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.jeval.Evaluator;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.geral.bean.Formularhcargo;
import br.com.linkcom.sined.geral.bean.Formularhitem;
import br.com.linkcom.sined.geral.service.FormularhService;
import br.com.linkcom.sined.geral.service.FormularhcargoService;
import br.com.linkcom.sined.geral.service.FormularhvariavelService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.FormularhFiltro;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.FormularhvariavelFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Formularh", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativa"})
public class FormularhCrud extends CrudControllerSined<FormularhFiltro, Formularh, Formularh>{
	
	private FormularhvariavelService formularhvariavelService;
	private FormularhService formularhService;
	private FormularhcargoService formularhcargoService;
	
	public void setFormularhService(FormularhService formularhService) {this.formularhService = formularhService;}
	public void setFormularhvariavelService(FormularhvariavelService formularhvariavelService) {this.formularhvariavelService = formularhvariavelService;}
	public void setFormularhcargoService(FormularhcargoService formularhcargoService) {this.formularhcargoService = formularhcargoService;}

	@Override
	protected void validateBean(Formularh bean, BindException errors) {
		if(!this.validaCargo(bean, errors)){
			errors.reject("001", "N�o � possivel salvar mais de uma form�la para o cargo selecionado.");
		}
		
		if(!this.validaFormula(bean, errors)) {
			errors.reject("004", "F�rmula inv�lida.");
		}
	}
	
	public ModelAndView popUpVariavelExtra(WebRequestContext request, FormularhvariavelFiltro filtro){	
		filtro.setListaFormulas(formularhvariavelService.findByFitro(filtro));
		return new ModelAndView("direct:/crud/formularhvariavelListagem", "filtro", filtro);
	}
	
	public ModelAndView popUpVariaveisBase(WebRequestContext request, FormularhvariavelFiltro filtro) {
		List<GenericBean> formulasBase = new ArrayList<GenericBean>();
		formulasBase.add(new GenericBean(filtro.getIdLista(), "SALARIO"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "VALE_TRANSPORTE"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "DIAS_AUSENTES"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "COMISSAO_VENCIMENTO"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "COMISSAO_PAGAMENTO"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "COMISSAO_EMISSAO"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "COMISSAO_CONTRATO"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "ATIVIDADES"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "ATIVIDADE_HORA"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "DESPESA_VIAGEM"));
		formulasBase.add(new GenericBean(filtro.getIdLista(), "QTD_DEPENDENTES"));
		
		request.setAttribute("idLista", filtro.getIdLista());
		
		return new ModelAndView("direct:/crud/popup/popUpVariavelBase", "formulasBase", formulasBase);
	}
	
    public boolean validaFormula(Formularh formularh, BindException errors) {
        Evaluator evaluator = formularhService.getEvaluator();
        
        String identificador[] = formularhService.getIdentificadores(formularh);
        
        for (Formularhitem formularhitem : formularh.getFormularhitem()) {
        	if(formularhitem.getFormula().contains("<" + formularhitem.getIdentificador() + ">")){
        		errors.reject("003", "O identificador da f�rmula n�o pode ter refer�ncia na pr�pria f�rmula. Identificador da f�rmula com erro: " + formularhitem.getIdentificador());
                return false;
        	}
        	
            try{
            	for(String id : identificador)
            		evaluator.putVariable(id, "0");
            	
            	evaluator.putVariable("SALARIO", "100.00");
                evaluator.putVariable("VALE_TRANSPORTE", "100.00");
                evaluator.putVariable("DIAS_AUSENTES", "0");
                evaluator.putVariable("COMISSAO_VENCIMENTO", "100.0");
                evaluator.putVariable("COMISSAO_PAGAMENTO", "100.0");
                evaluator.putVariable("COMISSAO_EMISSAO", "100.0");
                evaluator.putVariable("COMISSAO_CONTRATO", "100.0");
                evaluator.putVariable("QTD_DEPENDENTES", "0");
                evaluator.putVariable("ATIVIDADES", "0");
                evaluator.putVariable("ATIVIDADE_HORA", "0");
                evaluator.putVariable("DESPESA_VIAGEM", "0");
                
                evaluator.evaluate(formularhService.formatoFuncao(formularhService.formataIdentificador(formularhitem.getFormula(), identificador)));
            } catch (Exception e) {
                e.printStackTrace();
                errors.reject("003", "A sintaxe da(s) formula(s) contem erro(s).Por favor verifique");
                return false;
            }            
        }
        return true;
    }
    
	public boolean validaCargo(Formularh formularh, BindException errors){
		if(formularh.getFormularhcargo() == null || formularh.getFormularhcargo().isEmpty()){
			errors.reject("002", "� necess�rio escolher pelo menos um cargo para a f�rmula.");
			return false;
		}
		
		String cargos = CollectionsUtil.listAndConcatenate(formularh.getFormularhcargo(), "cargo.cdcargo", ",");
    	StringBuilder whereIn = new StringBuilder();
    	
    	for(Formularhcargo formularhcargo : formularh.getFormularhcargo()) {
			if(formularhcargo.getCdformularhcargo() != null){
				whereIn.append(formularhcargo.getCdformularhcargo());
				whereIn.append(",");			
			}
		}
    	
    	if(whereIn.length() > 0){
    		whereIn.delete(whereIn.length()-1, whereIn.length());
    	}
    	
    	if(formularhService.validaCargo(whereIn.toString(), cargos))
    		return true;
    	else
    		return false;	    		
    }
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Formularh form) throws Exception {
		if(form.getCdformularh() != null){
			List<Formularhcargo> listaFormularioRhCargo = new ListSet<Formularhcargo>(Formularhcargo.class);
			listaFormularioRhCargo = formularhcargoService.getListaFormulaRhCargoByFormulaRh(form);
			form.setFormularhcargo(SinedUtil.listToSet(listaFormularioRhCargo, Formularhcargo.class));
		}
	}
}