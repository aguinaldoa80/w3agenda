package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ConfiguracaoFolha;
import br.com.linkcom.sined.geral.bean.FechamentoFolha;
import br.com.linkcom.sined.geral.service.ConfiguracaoFolhaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FechamentoFolhaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.FechamentoFolhaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/FechamentoFolha", authorizationModule=CrudAuthorizationModule.class)
public class FechamentoFolhaCrud extends CrudControllerSined<FechamentoFolhaFiltro, FechamentoFolha, FechamentoFolha> {
	
	private FechamentoFolhaService fechamentoFolhaService;
	private EmpresaService empresaService;
	private ConfiguracaoFolhaService configuracaoFolhaService;
	
	public void setFechamentoFolhaService(FechamentoFolhaService fechamentoFolhaService) {
		this.fechamentoFolhaService = fechamentoFolhaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setConfiguracaoFolhaService(ConfiguracaoFolhaService configuracaoFolhaService) {
		this.configuracaoFolhaService = configuracaoFolhaService;
	}
	
	@Override
	protected void validateBean(FechamentoFolha bean, BindException errors) {
		if(fechamentoFolhaService.existeFechamentoPeriodo(bean)){
			errors.reject("001", "J� existe um fechamento de folha para o per�odo informado.");
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, FechamentoFolha form) throws Exception {
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}

	@Override
	protected void salvar(WebRequestContext request, FechamentoFolha bean)	throws Exception {
		ConfiguracaoFolha configuracaoFolha = null;
		if(bean.getEmpresa() != null){
			configuracaoFolha = configuracaoFolhaService.loadForFechamentoFolha(bean.getEmpresa());
		}
		
		super.salvar(request, bean);
		
		if(configuracaoFolha != null && configuracaoFolha.getAgendamento() != null && SinedUtil.isListNotEmpty(configuracaoFolha.getListaEvento())){
			boolean agendamentoAtualizado = fechamentoFolhaService.atualizarAgendamentoAposFechamentoFolha(bean, configuracaoFolha);
			if(agendamentoAtualizado){
				request.addMessage("Agendamento " + SinedUtil.makeLinkHtml(configuracaoFolha.getAgendamento().getCdagendamento().toString(), 
													"/financeiro/crud/Agendamento?ACAO=consultar&cdagendamento=", null) 
												  + " atualizado ap�s fechamento.");
			}
		}
			
	}
}
