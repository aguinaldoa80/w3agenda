package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Despesaavulsarh;
import br.com.linkcom.sined.geral.bean.Despesaavulsarhhistorico;
import br.com.linkcom.sined.geral.bean.Despesaavulsarhrateio;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaavulsarhacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoDespesaAvulsaRH;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.DespesaavulsarhService;
import br.com.linkcom.sined.geral.service.DespesaavulsarhhistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.FechamentoFolhaService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DespesaavulsarhFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Despesaavulsarh", authorizationModule=CrudAuthorizationModule.class)
public class DespesaavulsarhCrud extends CrudControllerSined<DespesaavulsarhFiltro, Despesaavulsarh, Despesaavulsarh> {

	private DespesaavulsarhService despesaavulsarhService;
	private DespesaavulsarhhistoricoService despesaavulsarhhistoricoService;
	private DocumentoorigemService documentoorigemService;
	private DocumentoService documentoService;
	private FechamentoFolhaService fechamentoFolhaService;
	
	public void setDespesaavulsarhService(DespesaavulsarhService despesaavulsarhService) {
		this.despesaavulsarhService = despesaavulsarhService;
	}
	
	public void setDespesaavulsarhhistoricoService(DespesaavulsarhhistoricoService despesaavulsarhhistoricoService) {
		this.despesaavulsarhhistoricoService = despesaavulsarhhistoricoService;
	}
	
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	
	public void setFechamentoFolhaService(
			FechamentoFolhaService fechamentoFolhaService) {
		this.fechamentoFolhaService = fechamentoFolhaService;
	}
	
	@Override
	protected Despesaavulsarh criar(WebRequestContext request, Despesaavulsarh form) throws Exception {

		Despesaavulsarh despesaavulsarh = super.criar(request, form);
		despesaavulsarh.setDtinsercao(new Date(System.currentTimeMillis()));
		despesaavulsarh.setSituacao(SituacaoDespesaAvulsaRH.EM_ABERTO);
		despesaavulsarh.setListaDespesaavulsarhrateio(new ListSet<Despesaavulsarhrateio>(Despesaavulsarhrateio.class));
		Despesaavulsarhrateio rateio = new Despesaavulsarhrateio();
		rateio.setPercentual(100.0);
		despesaavulsarh.getListaDespesaavulsarhrateio().add(rateio );
		return despesaavulsarh;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Despesaavulsarh form) throws Exception {
		super.entrada(request, form);
		
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
		request.setAttribute("listaCentrocusto", CentrocustoService.getInstance().findAtivos());
		request.setAttribute("listaProjetos", ProjetoService.getInstance().findProjetosAbertos(null));		
	}
	
	@Override
	protected void salvar(WebRequestContext request, Despesaavulsarh bean) throws Exception {
		
		Money totalRateio = new Money(0);
		for (Despesaavulsarhrateio rateio : bean.getListaDespesaavulsarhrateio()){
			totalRateio = totalRateio.add(rateio.getValor());
		}
	
		if (totalRateio.getValue().compareTo(bean.getValor().getValue()) != 0){
			throw new SinedException("O valor total rateado � diferente do valor total.");
		}
		
		try{
			Despesaavulsarhhistorico historico = new Despesaavulsarhhistorico();
			historico.setUsuario(SinedUtil.getUsuarioLogado());
			historico.setDespesaavulsarh(bean);
			historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			if (bean.getCddespesaavulsarh() == null)
				historico.setAcao(Despesaavulsarhacao.CRIADA);
			else
				historico.setAcao(Despesaavulsarhacao.ALTERADA);
			
			super.salvar(request, bean);
			
			despesaavulsarhhistoricoService.saveOrUpdate(historico);
			bean.getListaDespesaavulsarhhistorico().add(historico);
			
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "idx_despesaavulsarh_unique")){
				throw new SinedException("Despesa j� cadastrada no sistema.");
			}else{
				throw new SinedException("Ocorreu um erro ao salvar. Entre em contato com o suporte t�cnico.");
			}
		}
	}
	
	@Override
	protected void listagem(WebRequestContext request, DespesaavulsarhFiltro filtro) throws Exception {
		super.listagem(request, filtro);
		
		List<SituacaoDespesaAvulsaRH> listaSituacao = new ArrayList<SituacaoDespesaAvulsaRH>();
		listaSituacao.add(SituacaoDespesaAvulsaRH.EM_ABERTO);
		listaSituacao.add(SituacaoDespesaAvulsaRH.AUTORIZADA);
		listaSituacao.add(SituacaoDespesaAvulsaRH.PROCESSADA);
		listaSituacao.add(SituacaoDespesaAvulsaRH.CANCELADA);
		request.setAttribute("listaSituacaoCompleta", listaSituacao);
		
		
	}
	
	/**
	 * Muda a situa��o das despesas para {@link SituacaoDespesaAvulsaRH#AUTORIZADA}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param request
	 * @param filtro
	 * @return
	 * @throws CrudException
	 */
	public ModelAndView autorizar(WebRequestContext request, DespesaavulsarhFiltro filtro) throws CrudException{
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	
		try{
			despesaavulsarhService.autorizar(itensSelecionados);
			request.addMessage("Despesa(s) avulsa(s) de RH autorizada(s) com sucesso.");
		}catch (SinedException e) {
			request.addError(e.getMessage());
		}
    	return doListagem(request, filtro);
	}
	
	/**
	 * Muda a situa��o das despesas para {@link SituacaoDespesaAvulsaRH#EM_ABERTO}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param request
	 * @param filtro
	 * @return
	 * @throws CrudException
	 */
	public ModelAndView estornar(WebRequestContext request, DespesaavulsarhFiltro filtro) throws CrudException{
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	
		try{
			despesaavulsarhService.estornar(itensSelecionados);
	    	request.addMessage("Despesa(s) avulsa(s) de RH estornada(s) com sucesso.");
		}catch (SinedException e) {
			request.addError(e.getMessage());
		}

		return doListagem(request, filtro);
	}
	
	/**
	 * Muda a situa��o das despesas para {@link SituacaoDespesaAvulsaRH#CANCELADA}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param request
	 * @param filtro
	 * @return
	 * @throws CrudException
	 */
	public ModelAndView cancelar(WebRequestContext request, DespesaavulsarhFiltro filtro) throws CrudException{
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		List<Despesaavulsarh> list = despesaavulsarhService.findByIds(itensSelecionados, false);
		
		String ids = "";
		
		for(Despesaavulsarh despesa : list){
			if(!ids.equals("")) ids += ",";
			if(despesa.getSituacao().equals(Colaboradordespesasituacao.CANCELADO)){
				request.addError("A situa��o da despesa avulsa de RH j� est� como: \"" + SituacaoDespesaAvulsaRH.CANCELADA.getNome() + "\"");
				return doListagem(request, filtro);
			}else if(despesa.getSituacao().equals(SituacaoDespesaAvulsaRH.PROCESSADA)){
				
				Documentoorigem documentoorigem = documentoorigemService.contaDespesaavulsarh(despesa);
				ids = ids + documentoorigem.getDocumento().getCddocumento(); 
				if(!documentoorigem.getDocumento().getDocumentoacao().equals(Documentoacao.PREVISTA)){
					request.addError("N�o � poss�vel cancelar. Pagamento confirmado no Financeiro");
					return doListagem(request, filtro);
				}
				
			}
		}
		
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setIds(ids);
		documentohistorico.setDocumentoacao(Documentoacao.CANCELADA);
		documentohistorico.setObservacao("Conta cancelada a partir do cancelamento da despesa avulsa de RH.");

		try{
			documentoService.doCancelar(documentohistorico);
			despesaavulsarhService.cancelar(list);
	    	request.addMessage("Despesa(s) avulsa(s) de RH cancelada(s) com sucesso.");
		}catch (SinedException e) {
			request.addError(e.getMessage());
		}

		return doListagem(request, filtro);
	}
	
	/**
	 * Abre tela para informar os dados necess�rios para gerar o documento
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * 
	 * @param request
	 * @return
	 * @throws CrudException
	 */
	public ModelAndView abrirTelaProcessar(WebRequestContext request) throws CrudException{
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		
		Despesaavulsarh despesaavulsarh = new Despesaavulsarh();
		despesaavulsarh.setItensSelecionados(itensSelecionados);
		request.setAttribute("action", request.getParameter("action"));
		return new ModelAndView("direct:/crud/popup/selecionaDadosProcessarDespesaavulsarh", "despesaavulsarh", despesaavulsarh);
		
	}
	
	/**
	 * Muda a situa��o das despesas para {@link SituacaoDespesaAvulsaRH#PROCESSADA} 
	 * e gera as contas a pagar para cada despesa selecionada.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param request
	 * @return
	 */
	public void processar(WebRequestContext request, Despesaavulsarh filtro){
		if (StringUtils.isEmpty(filtro.getItensSelecionados()))
			throw new SinedException("Nenhum item selecionado.");
		
		List<Despesaavulsarh> list = despesaavulsarhService.findByIds(filtro.getItensSelecionados(), true);
		
		for(Despesaavulsarh despesa : list){
			if(!despesa.getSituacao().equals(SituacaoDespesaAvulsaRH.AUTORIZADA)){
				request.addError("S� � poss�vel processar a despesa avulsa de RH com situa��o \"" + SituacaoDespesaAvulsaRH.AUTORIZADA.getNome() + "\".");
				SinedUtil.redirecionamento(request, "/rh/crud/Despesaavulsarh");
				return ;
			}
			if(filtro.getDocumentotipo() == null || filtro.getDocumentotipo().getCddocumentotipo() == null){
				request.addError("O tipo de documento � de preenchimento obrigat�rio.");
				SinedUtil.redirecionamento(request, "/rh/crud/Colaboradordespesa");
				return ;
			}
		}			
		
		boolean sucesso = true;
		
		for(Despesaavulsarh colaboradordespesa : list){
			try{
				despesaavulsarhService.processar(colaboradordespesa, filtro.getEmpresa(), filtro.getDocumentotipo());
			} catch (Exception e) {
				sucesso = false;
				request.addError(e.getMessage());

				if("consultar".equals(request.getParameter("action"))){
					SinedUtil.redirecionamento(request,"/rh/crud/Despesaavulsarh?ACAO=consultar&cddespesaavulsarh=" + filtro.getItensSelecionados());
				} else {
					SinedUtil.redirecionamento(request, "/rh/crud/Despesaavulsarh");
				}
			}
		}
		
		if (sucesso)
			request.addMessage("Processamento executado com sucesso.");
		
		if("consultar".equals(request.getParameter("action"))){
			SinedUtil.redirecionamento(request,"redirect:/rh/crud/Despesaavulsarh?ACAO=consultar&cddespesaavulsarh=" + filtro.getItensSelecionados());
		} else {
			SinedUtil.redirecionamento(request, "/rh/crud/Despesaavulsarh");
		}
	}
	
	@Override
	protected void validateBean(Despesaavulsarh bean, BindException errors) {
		if(fechamentoFolhaService.existeFechamentoPeriodo(bean.getEmpresa(), bean.getDtinsercao())){
			errors.reject("001", "J� existe fechamento de folha correspondente � data informada.");
		}
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Despesaavulsarh form) throws CrudException {
		if(form.getCddespesaavulsarh() != null){
			Despesaavulsarh despesa = despesaavulsarhService.load(form);
			if(fechamentoFolhaService.existeFechamentoPeriodo(despesa.getEmpresa(), despesa.getDtinsercao())){
				throw new SinedException("N�o foi poss�vel excluir. J� existe fechamento de folha correspondente � data informada.");
			}
		}else {
			String whereIn = request.getParameter("itenstodelete");
			List<Despesaavulsarh> listaDespesas = despesaavulsarhService.findByIds(whereIn, false);
			if(SinedUtil.isListNotEmpty(listaDespesas)){
				for (Despesaavulsarh despesa : listaDespesas) {
					if(fechamentoFolhaService.existeFechamentoPeriodo(despesa.getEmpresa(), despesa.getDtinsercao())){
						throw new SinedException("N�o foi poss�vel excluir. J� existe fechamento de folha correspondente � data informada.");
					}
				}				
			}
		}
		return super.doExcluir(request, form);
	}
}
