package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.view.Vwcolaboradoretiqueta;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwcolaboradoretiquetaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Vwcolaboradoretiqueta", authorizationModule=CrudAuthorizationModule.class)
public class VwcolaboradoretiquetaCrud extends CrudControllerSined<VwcolaboradoretiquetaFiltro, Vwcolaboradoretiqueta, Vwcolaboradoretiqueta>{
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Vwcolaboradoretiqueta form) throws CrudException {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Vwcolaboradoretiqueta bean) throws Exception {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Vwcolaboradoretiqueta bean) throws Exception {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
}
