package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Instrutor;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.MinistradorFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Ministrador", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class MinistradorCrud extends CrudControllerSined<MinistradorFiltro, Instrutor, Instrutor> {
	
	@Override
	protected void salvar(WebRequestContext request, Instrutor bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_INSTRUTOR_NOME")) 
				throw new SinedException("Ministrador j� cadastrada no sistema.");
			
		}
	}	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, MinistradorFiltro filtro) throws CrudException {
		if (!filtro.isNotFirstTime()) {
			filtro.setAtivo(true);
		}
		
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Instrutor form) throws Exception {
		if (form.getCdinstrutor() == null) {
			form.setAtivo(true);
		}
		super.entrada(request, form);
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Instrutor form) {
		return new ModelAndView("crud/ministradorEntrada");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, MinistradorFiltro filtro) {
		return new ModelAndView("crud/ministradorListagem");
	}
}
