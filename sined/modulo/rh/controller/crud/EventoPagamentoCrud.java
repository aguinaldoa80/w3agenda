package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.geral.bean.EventoPagamentoEmpresa;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.EventoPagamentoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/EventoPagamento", authorizationModule=CrudAuthorizationModule.class)
public class EventoPagamentoCrud extends CrudControllerSined<EventoPagamentoFiltro, EventoPagamento, EventoPagamento>{

	private ParametrogeralService parametrogeralService;
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@Override
	protected void entrada(WebRequestContext request, EventoPagamento form) throws Exception {
		request.setAttribute("PLANO_CONTAS_CONTABIL_POR_EMPRESA", parametrogeralService.getValorPorNome(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA));
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(EventoPagamento bean, BindException errors) {
		if(parametrogeralService.getBoolean(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA) && SinedUtil.isListNotEmpty(bean.getListaEmpresa())) {
			for(EventoPagamentoEmpresa e : bean.getListaEmpresa()) {
				if(e.getEmpresa() == null) {
					errors.reject("001", "O campo empresa � obrigat�rio.");
					break;
				}
			}
		}
	}
}
