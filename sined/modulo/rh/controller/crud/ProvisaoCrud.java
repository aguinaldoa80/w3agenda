package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesahistorico;
import br.com.linkcom.sined.geral.bean.Provisao;
import br.com.linkcom.sined.geral.service.ColaboradordespesahistoricoService;
import br.com.linkcom.sined.geral.service.FechamentoFolhaService;
import br.com.linkcom.sined.geral.service.ProvisaoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ProvisaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/Provisao", authorizationModule=CrudAuthorizationModule.class)
public class ProvisaoCrud extends CrudControllerSined<ProvisaoFiltro, Provisao, Provisao>{

	private ProvisaoService provisaoService;
	private ColaboradordespesahistoricoService colaboradordespesahistoricoService;
	private FechamentoFolhaService fechamentoFolhaService;
	
	public void setProvisaoService(ProvisaoService provisaoService) {
		this.provisaoService = provisaoService;
	}
	public void setColaboradordespesahistoricoService(ColaboradordespesahistoricoService colaboradordespesahistoricoService) {
		this.colaboradordespesahistoricoService = colaboradordespesahistoricoService;
	}
	public void setFechamentoFolhaService(
			FechamentoFolhaService fechamentoFolhaService) {
		this.fechamentoFolhaService = fechamentoFolhaService;
	}

	@Override
	protected void excluir(WebRequestContext request, Provisao bean)
			throws Exception {
		String itens = request.getParameter("itenstodelete");
		List<Provisao> listaProvisao = null;
		
		if(fechamentoFolhaService.existeFechamentoPeriodo(bean.getEmpresa(), bean.getDtProvisao())){
			request.addError("J� existe fechamento de folha correspondente � data informada.");
		}
		
		if(StringUtils.isNotBlank(itens)){
			listaProvisao = provisaoService.findWithOrigem(itens);
		}
		
		super.excluir(request, bean);
		
		if(SinedUtil.isListNotEmpty(listaProvisao)){
			HashMap<Colaboradordespesa, StringBuilder> mapColaboradorDespesaProvisao = new HashMap<Colaboradordespesa, StringBuilder>();
			
			for(Provisao provisao : listaProvisao){
				if(provisao.getColaboradorDespesaOrigem() != null && provisao.getColaboradorDespesaOrigem().getCdcolaboradordespesa() != null){
					if(mapColaboradorDespesaProvisao.get(provisao.getColaboradorDespesaOrigem()) == null){
						mapColaboradorDespesaProvisao.put(provisao.getColaboradorDespesaOrigem(), new StringBuilder(provisao.getColaboradorDespesaOrigem().getCdcolaboradordespesa().toString()));
					}else {
						mapColaboradorDespesaProvisao.get(provisao.getColaboradorDespesaOrigem()).append(", ").append(provisao.getColaboradorDespesaOrigem().getCdcolaboradordespesa().toString());
					}
				}
			}
			
			if(mapColaboradorDespesaProvisao.size() > 0){
				for(Colaboradordespesa colaboradordespesa : mapColaboradorDespesaProvisao.keySet()){
					Colaboradordespesahistorico colaboradordespesahistorico = new Colaboradordespesahistorico();
					colaboradordespesahistorico.setColaboradordespesa(colaboradordespesa);
					colaboradordespesahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					colaboradordespesahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					colaboradordespesahistorico.setObservacao("Provis�o exclu�da: " + mapColaboradorDespesaProvisao.get(colaboradordespesa));
					colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);
				}
			}
		}
	}
	
	@Override
	protected void validateBean(Provisao bean, BindException errors) {
		if(fechamentoFolhaService.existeFechamentoPeriodo(bean.getEmpresa(), bean.getDtProvisao())){
			errors.reject("001", "J� existe fechamento de folha correspondente � data informada.");
		}
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Provisao form) throws CrudException {
		if(form.getCdProvisao() != null){
			Provisao provisao = provisaoService.load(form);
			if(fechamentoFolhaService.existeFechamentoPeriodo(provisao.getEmpresa(), provisao.getDtProvisao())){
				throw new SinedException("N�o foi poss�vel excluir. J� existe fechamento de folha correspondente � data informada.");
			}
		}else {
			String whereIn = request.getParameter("itenstodelete");
			List<Provisao> listaProvisoes = provisaoService.findByIds(whereIn);
			if(SinedUtil.isListNotEmpty(listaProvisoes)){
				for (Provisao provisao : listaProvisoes) {
					if(fechamentoFolhaService.existeFechamentoPeriodo(provisao.getEmpresa(), provisao.getDtProvisao())){
						throw new SinedException("N�o foi poss�vel excluir. J� existe fechamento de folha correspondente � data informada.");
					}
				}				
			}
		}
		return super.doExcluir(request, form);
	}
	
}
