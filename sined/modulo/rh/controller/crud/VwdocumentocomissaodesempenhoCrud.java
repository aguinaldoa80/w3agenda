package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentodesempenhoconsiderar;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentocomissaodesempenho;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.geral.service.VdocumentonegociadoService;
import br.com.linkcom.sined.geral.service.VwdocumentocomissaodesempenhoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwdocumentocomissaodesempenhoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Documentocomissaodesempenho", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "")
public class VwdocumentocomissaodesempenhoCrud extends CrudControllerSined<VwdocumentocomissaodesempenhoFiltro, Vwdocumentocomissaodesempenho, Vwdocumentocomissaodesempenho>{

	private VwdocumentocomissaodesempenhoService vwdocumentocomissaodesempenhoService;
	private VdocumentonegociadoService vdocumentonegociadoService;
	private ComissionamentoService comissionamentoService;
	
	public void setVdocumentonegociadoService(VdocumentonegociadoService vdocumentonegociadoService) {
		this.vdocumentonegociadoService = vdocumentonegociadoService;
	}
	public void setVwdocumentocomissaodesempenhoService(VwdocumentocomissaodesempenhoService vwdocumentocomissaodesempenhoService) {
		this.vwdocumentocomissaodesempenhoService = vwdocumentocomissaodesempenhoService;
	}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {
		this.comissionamentoService = comissionamentoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected ListagemResult<Vwdocumentocomissaodesempenho> getLista(WebRequestContext request,VwdocumentocomissaodesempenhoFiltro filtro) {
		ListagemResult<Vwdocumentocomissaodesempenho> lista = super.getLista(request, filtro);
		List<Vwdocumentocomissaodesempenho> list = vwdocumentocomissaodesempenhoService.findForContapagar(filtro);
		
		vwdocumentocomissaodesempenhoService.ajustaLista(list, filtro);
		
		if(list != null && list.size() > 0){
			if(filtro.getConsiderar() == null || !filtro.getConsiderar().equals(Comissionamentodesempenhoconsiderar.TOTAL_VENDA)){
				String whereInDocumentos = CollectionsUtil.listAndConcatenate(list, "cddocumento", ",");
				List<Vdocumentonegociado> listaNegociados = vdocumentonegociadoService.findByDocumentos(whereInDocumentos);
				
				for (Vwdocumentocomissaodesempenho vwdocumentocomissaodesempenho : list) {
					if(vwdocumentocomissaodesempenho.getCddocumento() != null) {
						List<Documento> documentosNegociados = new ArrayList<Documento>();
						for (Vdocumentonegociado v : listaNegociados) {
							if(v.getCddocumento().equals(vwdocumentocomissaodesempenho.getCddocumento())){
								documentosNegociados.add(new Documento(v.getCddocumentonegociado(), 
																		v.getCddocumentoacaonegociado(), 
																		v.getDtvencimentonegociado(), 
																		v.getDtemissaonegociado()));
							}
						}
						vwdocumentocomissaodesempenho.setDocumentosNegociados(documentosNegociados);
					}
				}
			}
		}
		
		return lista;
	}
	
	@Override
	protected void listagem(WebRequestContext request, VwdocumentocomissaodesempenhoFiltro filtro) throws Exception {
		List<Comissionamento> listaComissionamento = new ArrayList<Comissionamento>();
		if(filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
			listaComissionamento = comissionamentoService.findComissionamentoByColaborador(filtro.getColaborador());
		}
		
		List<String> listaMostrar = new ArrayList<String>();		
		listaMostrar.add("Pagos com pend�ncia de repasse");
		listaMostrar.add("Todos");
		
		request.setAttribute("listaComissionamento", listaComissionamento);		
		request.setAttribute("listaMostrar", listaMostrar);
		String label_considerar = "";
		if(filtro.getConsiderar() == null || Comissionamentodesempenhoconsiderar.TOTAL_CONTARECEBER.equals(filtro.getConsiderar())){
			label_considerar = "Valor da Conta a Receber";
		}else if(Comissionamentodesempenhoconsiderar.TOTAL_VENDA.equals(filtro.getConsiderar())){
			label_considerar = "Total de Venda";
		}else if(Comissionamentodesempenhoconsiderar.MARKUP_VENDA.equals(filtro.getConsiderar())){
			label_considerar = "Markup de Venda";
		}else if(Comissionamentodesempenhoconsiderar.SALDO_VENDA.equals(filtro.getConsiderar())){
			label_considerar = "Saldo de Venda";
		}
		request.setAttribute("label_considerar", label_considerar);
	}
	
	/**
	 * M�todo ajax para buscar o comissionamento relacionado ao colaborador
	 *
	 * @param request
	 * @param colaborador
	 * @author Luiz Fernando
	 */
	public void ajaxBuscarComissionamento(WebRequestContext request, Colaborador colaborador){
		List<Comissionamento> listaComissionamento = new ArrayList<Comissionamento>();
		if(colaborador != null && colaborador.getCdpessoa() != null){
			listaComissionamento = comissionamentoService.findComissionamentoByColaborador(colaborador);
		}
	
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaComissionamento, "listaComissionamento", ""));
	}

	@Override
	public ModelAndView doExportar(WebRequestContext request,
			VwdocumentocomissaodesempenhoFiltro filtro) {
		try {
			listagem(request, filtro);
			Resource resource = vwdocumentocomissaodesempenhoService.gerarListagemCSV(request, filtro);
			
			return new ResourceModelAndView(resource);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Houve um erro a gerar o relat�rio CSV.");
		}
		
	}
}

