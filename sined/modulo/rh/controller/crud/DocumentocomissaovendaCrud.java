package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.geral.service.DocumentocomissaovendaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VdocumentonegociadoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Documentocomissaovenda", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "")
public class DocumentocomissaovendaCrud extends CrudControllerSined<DocumentocomissaovendaFiltro, Documentocomissao, Documentocomissao>{

	private VdocumentonegociadoService vdocumentonegociadoService;
	private DocumentocomissaovendaService documentocomissaovendaService;
	private UsuarioService usuarioService;
	private ParametrogeralService parametrogeralService;
	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setVdocumentonegociadoService(
			VdocumentonegociadoService vdocumentonegociadoService) {
		this.vdocumentonegociadoService = vdocumentonegociadoService;
	}
	public void setDocumentocomissaovendaService(
			DocumentocomissaovendaService documentocomissaovendaService) {
		this.documentocomissaovendaService = documentocomissaovendaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}

	@Override
	protected void listagem(WebRequestContext request,  DocumentocomissaovendaFiltro filtro) throws Exception {
		List<String> listaMostrar = new ArrayList<String>();
		List<String> listaTipocolaborador = new ArrayList<String>();
		List<String> listaConsiderarPedidoVenda = new ArrayList<String>();
		
		listaMostrar.add("Pagos com pend�ncia de repasse");
		listaMostrar.add("Todos");
		
		listaTipocolaborador.add("Vendedor respons�vel");
		listaTipocolaborador.add("Outros vendedores");
		listaTipocolaborador.add("Indica��o Cliente");
		
		listaConsiderarPedidoVenda.add("Com venda");
		listaConsiderarPedidoVenda.add("Sem venda");
		
		request.setAttribute("listaTipocolaborador", listaTipocolaborador);
		request.setAttribute("listaMostrar", listaMostrar);
		request.setAttribute("listaConsiderarPedidoVenda", listaConsiderarPedidoVenda);
		
		if (filtro.getTotal() != null)
			filtro.setTotalGeral(documentocomissaovendaService.findForTotalGeralDocumentocomissaovenda(filtro));
		else
			filtro.setTotalGeral(null);
		
		
		Boolean isUsuarioLogadoRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado());
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(isUsuarioLogadoRestricaoVendaVendedor != null && isUsuarioLogadoRestricaoVendaVendedor && colaborador != null){
			filtro.setColaboradorvendedor(colaborador);
			filtro.setColaboradorcomissao(colaborador);
			request.setAttribute("haveResticaoVendaVendedor", Boolean.TRUE);
		}
		
		boolean comissionamento_canhoto_venda = parametrogeralService.getBoolean(Parametrogeral.COMISSIONAMENTO_CANHOTO_NOTA);
		if(comissionamento_canhoto_venda){
			filtro.setRetornocanhoto(Boolean.TRUE);
		}
		request.setAttribute("COMISSIONAMENTO_CANHOTO_NOTA", comissionamento_canhoto_venda);
		
		super.listagem(request, filtro);  
	}
	
	@Override
	protected ListagemResult<Documentocomissao> getLista(WebRequestContext request, DocumentocomissaovendaFiltro filtro) {
		ListagemResult<Documentocomissao> listagemResult = super.getLista(request, filtro);
		List<Documentocomissao> list = listagemResult.list();
		
//		SETA O VALOR TOTAL DOS DOCUMENTOS E DE COMISS�O
		Money totalDocumentos = new Money();
		Money totalComissao = new Money();
		List<Documento> listaDocumentosUtilizados = new ArrayList<Documento>();
		for (Documentocomissao documentocomissao : list) {
//			if (documentocomissao.getDocumento() != null && documentocomissao.getDocumento().getValor() != null)
//				totalDocumentos = totalDocumentos.add(documentocomissao.getDocumento().getValor());
			if (documentocomissao.getDocumento() != null && documentocomissao.getDocumento().getAux_documento() != null && 
					documentocomissao.getDocumento().getAux_documento().getValoratual() != null){
				if(!listaDocumentosUtilizados.contains(documentocomissao.getDocumento())){
					totalDocumentos = totalDocumentos.add(documentocomissao.getDocumento().getAux_documento().getValoratual());
					listaDocumentosUtilizados.add(documentocomissao.getDocumento());
				}
			}
			if (documentocomissao.getValorcomissao() != null)
				totalComissao = totalComissao.add(documentocomissao.getValorcomissao());
		}
		
		filtro.setTotal(totalDocumentos);
		filtro.setTotalComissao(totalComissao);
		
		String whereInDocumentos = CollectionsUtil.listAndConcatenate(list, "documento.cddocumento", ",");
		if(whereInDocumentos != null && !whereInDocumentos.equals("")){
			List<Vdocumentonegociado> listaNegociados = vdocumentonegociadoService.findByDocumentos(whereInDocumentos);
			
			for (Documentocomissao documentocomissao : list) {
				if(documentocomissao.getDocumento() != null) {
					List<Documento> documentosNegociados = new ArrayList<Documento>();
					for (Vdocumentonegociado v : listaNegociados) {
						if(v.getCddocumento().equals(documentocomissao.getDocumento().getCddocumento())){
							documentosNegociados.add(new Documento(v.getCddocumentonegociado(), 
																	v.getCddocumentoacaonegociado(), 
																	v.getDtvencimentonegociado(), 
																	v.getDtemissaonegociado()));
						}
					}
					documentocomissao.setDocumentosNegociados(documentosNegociados);
				}
			}
		}
		
		return listagemResult;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, DocumentocomissaovendaFiltro filtro) {
		return new ModelAndView("crud/documentocomissaovendaListagem");
	}
	
	@Override
	public ModelAndView doExportar(WebRequestContext request,
			DocumentocomissaovendaFiltro filtro) throws CrudException, IOException {
		try {
			listagem(request, filtro);
			
			Resource resource = documentocomissaovendaService.gerarListagemCSV(filtro);
			return new ResourceModelAndView(resource);
		} catch(Exception e) {
			e.printStackTrace();
			throw new SinedException("Houve um problema ao gerar o relat�rio CSV.");
		}
	}
}
