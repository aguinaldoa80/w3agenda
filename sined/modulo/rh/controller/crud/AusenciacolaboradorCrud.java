package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Ausenciacolaborador;
import br.com.linkcom.sined.geral.bean.Ausenciacolaboradorhistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Ausenciacolaboradorhistoricoacao;
import br.com.linkcom.sined.geral.service.AusenciacolaboradorhistoricoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.AusenciacolaboradorFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/rh/crud/Ausenciacolaborador", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"colaborador", "ausenciamotivo", "descricao", "dtinicio", "dtfim"})
public class AusenciacolaboradorCrud extends CrudControllerSined<AusenciacolaboradorFiltro, Ausenciacolaborador, Ausenciacolaborador>{
	
	protected AusenciacolaboradorhistoricoService ausenciacolaboradorhistoricoService;
	
	
	public AusenciacolaboradorhistoricoService getAusenciacolaboradorhistoricoService() {
		return ausenciacolaboradorhistoricoService;
	}

	public void setAusenciacolaboradorhistoricoService(AusenciacolaboradorhistoricoService ausenciacolaboradorhistoricoService) {
		this.ausenciacolaboradorhistoricoService = ausenciacolaboradorhistoricoService;
	}

	@Override
	protected void validateBean(Ausenciacolaborador bean, BindException errors) {
		Date dateInicio = SinedDateUtils.dateToBeginOfDay(bean.getDtinicio());
		Date dateFim = SinedDateUtils.dateToBeginOfDay(bean.getDtfim());
		if(dateFim.before(dateInicio))
			errors.reject("001", "A Data fim n�o pode ser anterior � data in�cio.");
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Ausenciacolaborador bean) throws Exception {
		
		try {			
			if(bean != null){
				Ausenciacolaboradorhistorico ausenciacolaboradorhistorico = new Ausenciacolaboradorhistorico();
				
				ausenciacolaboradorhistorico.setCdausenciacolaborador(bean);
				ausenciacolaboradorhistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				ausenciacolaboradorhistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				
				if(bean.getCdausenciacolaborador() == null){
					ausenciacolaboradorhistorico.setAcao(Ausenciacolaboradorhistoricoacao.CRIADO);
				}else{
					ausenciacolaboradorhistorico.setAcao(Ausenciacolaboradorhistoricoacao.ALTERADO);
				}
				
				super.salvar(request, bean);	
				ausenciacolaboradorhistoricoService.saveOrUpdate(ausenciacolaboradorhistorico);
			}
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_ausenciacolaborador_cdausenciacolaborador")) 
				throw new SinedException("J� cadastrado no sistema.");	
		}
		
	}
}
