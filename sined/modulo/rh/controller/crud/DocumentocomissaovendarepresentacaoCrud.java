package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.service.DocumentocomissaovendarepresentacaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendarepresentacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Documentocomissaovendarepresentacao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "")
public class DocumentocomissaovendarepresentacaoCrud extends CrudControllerSined<DocumentocomissaovendarepresentacaoFiltro, Documentocomissao, Documentocomissao>{

	private UsuarioService usuarioService;
	private DocumentocomissaovendarepresentacaoService documentocomissaovendarepresentacaoService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setDocumentocomissaovendarepresentacaoService(
			DocumentocomissaovendarepresentacaoService documentocomissaovendarepresentacaoService) {
		this.documentocomissaovendarepresentacaoService = documentocomissaovendarepresentacaoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}

	@Override
	protected void listagem(WebRequestContext request,  DocumentocomissaovendarepresentacaoFiltro filtro) throws Exception {
		List<String> listaMostrar = new ArrayList<String>();
		List<String> listaTipocolaborador = new ArrayList<String>();
		
		listaMostrar.add("Pagos com pend�ncia de repasse");
		listaMostrar.add("Todos");
		
		listaTipocolaborador.add("Vendedor principal");
		listaTipocolaborador.add("Outros vendedores");
		listaTipocolaborador.add("Ag�ncia");
		
		request.setAttribute("listaTipocolaborador", listaTipocolaborador);
		request.setAttribute("listaMostrar", listaMostrar);
		
		Boolean isUsuarioLogadoRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado());
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		if(isUsuarioLogadoRestricaoVendaVendedor != null && isUsuarioLogadoRestricaoVendaVendedor && colaborador != null){
			filtro.setColaboradorvendedor(colaborador);
			filtro.setColaboradorcomissao(colaborador);
			request.setAttribute("haveResticaoVendaVendedor", Boolean.TRUE);
		}
		
		super.listagem(request, filtro);  
	}
	
	@Override
	protected ListagemResult<Documentocomissao> getLista(WebRequestContext request, DocumentocomissaovendarepresentacaoFiltro filtro) {
		ListagemResult<Documentocomissao> listagemResult = super.getLista(request, filtro);
		List<Documentocomissao> list = listagemResult.list();
		
//		SETA O VALOR TOTAL DOS DOCUMENTOS E DE COMISS�O
		Money totalComissao = new Money();
		for (Documentocomissao documentocomissao : list) {
			if (documentocomissao.getValorcomissao() != null)
				totalComissao = totalComissao.add(documentocomissao.getValorcomissao());
		}
		
		filtro.setTotalComissao(totalComissao);
		return listagemResult;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, DocumentocomissaovendarepresentacaoFiltro filtro) {
		return new ModelAndView("crud/documentocomissaovendarepresentacaoListagem");
	}

	@Override
	public ModelAndView doExportar(WebRequestContext request,
			DocumentocomissaovendarepresentacaoFiltro filtro)
			throws CrudException, IOException {
		try {
			listagem(request, filtro);

			Resource resource = documentocomissaovendarepresentacaoService.gerarListagemCSV(filtro);
			return new ResourceModelAndView(resource);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Houve um problema ao gerar o relat�rio.");
		}
		
	}
}
