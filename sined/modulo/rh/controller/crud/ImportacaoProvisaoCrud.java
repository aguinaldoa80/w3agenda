package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.sined.geral.bean.ImportacaoProvisao;
import br.com.linkcom.sined.geral.service.ImportacaoProvisaoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ImportacaoProvisaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/rh/crud/ImportacaoProvisao", authorizationModule=ProcessAuthorizationModule.class)
public class ImportacaoProvisaoCrud extends CrudControllerSined<ImportacaoProvisaoFiltro, ImportacaoProvisao, ImportacaoProvisao>{

	private ImportacaoProvisaoService importacaoProvisaoService;
	
	public void setImportacaoProvisaoService(
			ImportacaoProvisaoService importacaoProvisaoService) {
		this.importacaoProvisaoService = importacaoProvisaoService;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request,
			ImportacaoProvisao form) throws CrudException {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request,
			ImportacaoProvisao form) throws CrudException {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request,
			ImportacaoProvisao form) throws CrudException {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	protected ListagemResult<ImportacaoProvisao> getLista(WebRequestContext request, ImportacaoProvisaoFiltro filtro) {
		ListagemResult<ImportacaoProvisao> lista = super.getLista(request, filtro);
		List<ImportacaoProvisao> list = lista.list();
		
		for (ImportacaoProvisao importacaoProvisao : list) {
			if(importacaoProvisao.getArquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), importacaoProvisao.getArquivo().getCdarquivo());					
			}
		}
		
		return lista;
	}
	
}
