package br.com.linkcom.sined.modulo.rh.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Despesaavulsarhmotivo;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DespesaavulsarhmotivoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Despesaavulsarhmotivo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class DespesaavulsarhmotivoCrud extends CrudControllerSined<DespesaavulsarhmotivoFiltro, Despesaavulsarhmotivo, Despesaavulsarhmotivo> {

}
