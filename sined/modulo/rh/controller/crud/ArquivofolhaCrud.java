package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivofolha;
import br.com.linkcom.sined.geral.bean.Arquivofolhahistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhasituacao;
import br.com.linkcom.sined.geral.service.ArquivofolhaService;
import br.com.linkcom.sined.geral.service.ArquivofolhahistoricoService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ArquivofolhaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Arquivofolha", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdarquivofolha", "arquivofolhaorigem", "arquivofolhamotivo", "arquivo", "dtinsercao", "arquivofolhasituacao"})
public class ArquivofolhaCrud extends CrudControllerSined<ArquivofolhaFiltro, Arquivofolha, Arquivofolha> {

	private ArquivofolhaService arquivofolhaService;
	private ArquivofolhahistoricoService arquivofolhahistoricoService;
	
	public void setArquivofolhahistoricoService(
			ArquivofolhahistoricoService arquivofolhahistoricoService) {
		this.arquivofolhahistoricoService = arquivofolhahistoricoService;
	}
	public void setArquivofolhaService(ArquivofolhaService arquivofolhaService) {
		this.arquivofolhaService = arquivofolhaService;
	}
	
	@Override
	protected Arquivofolha criar(WebRequestContext request, Arquivofolha form) throws Exception {
		Arquivofolha bean = super.criar(request, form);
		
		// VALORES PADR�ES
		bean.setDtinsercao(SinedDateUtils.currentDate());
		bean.setArquivofolhasituacao(Arquivofolhasituacao.EM_ABERTO);
		
		return bean;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Arquivofolha bean) throws Exception {
		boolean isCriar = bean.getCdarquivofolha() == null;
		
		super.salvar(request, bean);
		
		// GRAVA NO HIST�RICO TODA VEZ QUE SALVA UM ARQUIVOFOLHA
		Arquivofolhahistorico arquivofolhahistorico = new Arquivofolhahistorico();
		if(isCriar){
			arquivofolhahistorico.setAcao(Arquivofolhaacao.CRIADO);
		} else {
			arquivofolhahistorico.setAcao(Arquivofolhaacao.ALTERADO);
		}
		arquivofolhahistorico.setArquivofolha(bean);
		arquivofolhahistoricoService.saveOrUpdate(arquivofolhahistorico);
	}
	
	@Override
	protected void listagem(WebRequestContext request, ArquivofolhaFiltro filtro) throws Exception {
		request.setAttribute("listaSituacao", new ArrayList<Arquivofolhasituacao>(Arrays.asList(Arquivofolhasituacao.values())));
	}
	
	/**
	 * M�todo que autoriza os arquivos do sistema de folha.
	 * S� se pode autorizar um arquivo se estiver na situa��o 'EM ABERTO'.
	 *
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#findForAutorizarEstornarCancelar(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#autorizar(Arquivofolha arquivofolha)
	 * 
	 * @param request
	 * @return
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView autorizar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Arquivofolha> lista = arquivofolhaService.findForAutorizarEstornarCancelar(whereIn);
		boolean entrada = "true".equals(request.getParameter("entrada"));
		
		for (Arquivofolha af1 : lista) {
			if(!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.EM_ABERTO)){
				request.addError("S� � poss�vel autorizar arquivos de folha na situa��o 'EM ABERTO'.");
				
				if(entrada){
					return new ModelAndView("redirect:/rh/crud/Arquivofolha?ACAO=consultar&cdarquivofolha=" + whereIn);
				} else {
					return sendRedirectToAction("listagem");
				}
			}
		}
		
		for (Arquivofolha af2 : lista) {
			arquivofolhaService.autorizar(af2);
		}
		
		request.addMessage("Arquivo(s) autorizado(s) com sucesso."); 
		if(entrada){
			return new ModelAndView("redirect:/rh/crud/Arquivofolha?ACAO=consultar&cdarquivofolha=" + whereIn);
		} else {
			return sendRedirectToAction("listagem");
		}
	}
	
	/**
	 * Action que faz a valida��o e abre uma pop-up 
	 * para inserir a justificativa do cancelamento ou estorno de um arquivo.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#findForAutorizarEstornarCancelar(String whereIn)
	 *
	 * @param request
	 * @return
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView abrirJustificativa(WebRequestContext request){
		
		Arquivofolhahistorico arquivofolhahistorico = new Arquivofolhahistorico();
		arquivofolhahistorico.setWhereIn(SinedUtil.getItensSelecionados(request));
		arquivofolhahistorico.setEntrada("true".equals(request.getParameter("entrada")));
		
		List<Arquivofolha> lista = arquivofolhaService.findForAutorizarEstornarCancelar(arquivofolhahistorico.getWhereIn());
		
		String action = request.getParameter("action");
		if("cancelar".equals(action)){
			// VALIDA��O DO CANCELAMENTO
			for (Arquivofolha af1 : lista) {
				if(!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.AUTORIZADO) &&
						!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.EM_ABERTO)){
					request.addError("S� � poss�vel cancelar arquivos de folha na situa��o 'AUTORIZADO' ou 'EM ABERTO'.");
					SinedUtil.redirecionamento(request, "/rh/crud/Arquivofolha" + (arquivofolhahistorico.getEntrada() ? "?ACAO=consultar&cdarquivofolha=" + arquivofolhahistorico.getWhereIn() : ""));
					return null;
				}
			}
			
			request.setAttribute("titulo", "CANCELAR");
			request.setAttribute("acaoSalvar", "cancelar");
			request.setAttribute("labelCampo", "Justificativa para cancelamento");
		} else if("estornar".equals(action)){
			// VALIDA��O DO ESTORNO
			for (Arquivofolha af1 : lista) {
				if(!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.AUTORIZADO) &&
						!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.CANCELADO)){
					request.addError("S� � poss�vel estornar arquivos de folha na situa��o 'AUTORIZADO' ou 'CANCELADO'.");
					SinedUtil.redirecionamento(request, "/rh/crud/Arquivofolha" + (arquivofolhahistorico.getEntrada() ? "?ACAO=consultar&cdarquivofolha=" + arquivofolhahistorico.getWhereIn() : ""));
					return null;
				}
			}
			
			request.setAttribute("titulo", "ESTORNAR");
			request.setAttribute("acaoSalvar", "estornar");
			request.setAttribute("labelCampo", "Justificativa para estorno");
		}
		
		return new ModelAndView("direct:/crud/popup/justificativaArquivofolha", "bean", arquivofolhahistorico);
	}
	
	/**
	 * M�todo que estorna os arquivos do sistema de folha.
	 * S� se pode estornar um arquivo se estiver na situa��o 'AUTORIZADO' ou 'CANCELADO'.
	 *
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#findForAutorizarEstornarCancelar(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#estornar(Arquivofolha arquivofolha)
	 * 
	 * @param request
	 * @return
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public void estornar(WebRequestContext request, Arquivofolhahistorico arquivofolhahistorico){
		String whereIn = arquivofolhahistorico.getWhereIn();
		List<Arquivofolha> lista = arquivofolhaService.findForAutorizarEstornarCancelar(whereIn);
		
		for (Arquivofolha af1 : lista) {
			if(!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.AUTORIZADO) &&
					!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.CANCELADO)){
				request.addError("S� � poss�vel estornar arquivos de folha na situa��o 'AUTORIZADO' ou 'CANCELADO'.");
				SinedUtil.redirecionamento(request, "/rh/crud/Arquivofolha" + (arquivofolhahistorico.getEntrada() ? "?ACAO=consultar&cdarquivofolha=" + whereIn : ""));
				return;
			}
		}
		
		for (Arquivofolha af2 : lista) {
			arquivofolhaService.estornar(af2, arquivofolhahistorico.getObservacao());
		}	
		
		request.addMessage("Arquivo(s) estornado(s) com sucesso."); 
		SinedUtil.redirecionamento(request, "/rh/crud/Arquivofolha" + (arquivofolhahistorico.getEntrada() ? "?ACAO=consultar&cdarquivofolha=" + whereIn : ""));
	}
	
	/**
	 * M�todo que cancela os arquivos do sistema de folha.
	 * S� se pode cancelar um arquivo se estiver na situa��o 'AUTORIZADO' ou 'EM ABERTO'.
	 *
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#findForAutorizarEstornarCancelar(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#cancelar(Arquivofolha arquivofolha)
	 * 
	 * @param request
	 * @return
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public void cancelar(WebRequestContext request, Arquivofolhahistorico arquivofolhahistorico){
		String whereIn = arquivofolhahistorico.getWhereIn();
		List<Arquivofolha> lista = arquivofolhaService.findForAutorizarEstornarCancelar(whereIn);
		
		for (Arquivofolha af1 : lista) {
			if(!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.AUTORIZADO) &&
					!af1.getArquivofolhasituacao().equals(Arquivofolhasituacao.EM_ABERTO)){
				request.addError("S� � poss�vel cancelar arquivos de folha na situa��o 'AUTORIZADO' ou 'EM ABERTO'.");
				SinedUtil.redirecionamento(request, "/rh/crud/Arquivofolha" + (arquivofolhahistorico.getEntrada() ? "?ACAO=consultar&cdarquivofolha=" + whereIn : ""));
				return;
			}
		}
		
		for (Arquivofolha af2 : lista) {
			arquivofolhaService.cancelar(af2, arquivofolhahistorico.getObservacao());
		}	
		
		request.addMessage("Arquivo(s) cancelados(s) com sucesso."); 
		SinedUtil.redirecionamento(request, "/rh/crud/Arquivofolha" + (arquivofolhahistorico.getEntrada() ? "?ACAO=consultar&cdarquivofolha=" + whereIn : ""));
	}
	
}
