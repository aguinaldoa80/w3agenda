package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Formularhvariavel;
import br.com.linkcom.sined.geral.service.FormularhvariavelService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.FormularhvariavelFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/rh/crud/Formularhvariavel", authorizationModule=CrudAuthorizationModule.class)
public class FormularhvariavelCrud extends CrudControllerSined<FormularhvariavelFiltro, Formularhvariavel, Formularhvariavel>{
	
	protected FormularhvariavelService formularhvariavelService;	
	
	public void setFormularhvariavelService(FormularhvariavelService formularhvariavelService){this.formularhvariavelService = formularhvariavelService;}

	@DefaultAction
	protected ModelAndView index (WebRequestContext request, FormularhvariavelFiltro filtro) {
		return new ModelAndView("crud/formularhvariavelListagem?clearBase=1");
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, FormularhvariavelFiltro filtro) throws CrudException {
			request.setAttribute("clearBase", true);
			filtro.setListaFormulas(formularhvariavelService.findByFitro(filtro));
		return super.doListagem(request, filtro);
	}
	
	protected void entrada(WebRequestContext request, Formularhvariavel form) throws Exception{
			request.setAttribute("clearBase", true);			
		super.entrada(request, form);
	}	
		
	@Override
	protected void salvar(WebRequestContext request, Formularhvariavel bean){
		try{
			super.salvar(request, bean);
		}catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("N�o � poss�vel salvar essa f�rmula. Verifique se n�o existe um nome igual j� salvo ou uma f�rmula igual.");
		}
	}
	
	@Action("popUpVariavelExtra")
	public ModelAndView popUpVariavelExtra(WebRequestContext request, FormularhvariavelFiltro filtro){	
			filtro.setListaFormulas(formularhvariavelService.findByFitro(filtro));			
		return new ModelAndView("direct:/crud/popup/popUpFormulaVariavel", "filtro.listaFormulas", filtro.getListaFormulas());
	}
}