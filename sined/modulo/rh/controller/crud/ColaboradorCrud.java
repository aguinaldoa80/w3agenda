package br.com.linkcom.sined.modulo.rh.controller.crud;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Beneficiovalor;
import br.com.linkcom.sined.geral.bean.Candidato;
import br.com.linkcom.sined.geral.bean.Candidatosituacao;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Categoriacnh;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ColaboradorOcorrencia;
import br.com.linkcom.sined.geral.bean.Colaboradorarea;
import br.com.linkcom.sined.geral.bean.Colaboradorbeneficio;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorcategoriacnh;
import br.com.linkcom.sined.geral.bean.Colaboradordeficiencia;
import br.com.linkcom.sined.geral.bean.Colaboradordependente;
import br.com.linkcom.sined.geral.bean.Colaboradorequipamento;
import br.com.linkcom.sined.geral.bean.Colaboradorformapagamento;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalacolaborador;
import br.com.linkcom.sined.geral.bean.Grupoemail;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tipocarteiratrabalho;
import br.com.linkcom.sined.geral.bean.Tipodependente;
import br.com.linkcom.sined.geral.bean.Tipoinscricao;
import br.com.linkcom.sined.geral.bean.Turma;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Nacionalidade;
import br.com.linkcom.sined.geral.service.AgendamentoservicoService;
import br.com.linkcom.sined.geral.service.AreaService;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.geral.service.BeneficioService;
import br.com.linkcom.sined.geral.service.BeneficiovalorService;
import br.com.linkcom.sined.geral.service.CandidatoService;
import br.com.linkcom.sined.geral.service.CargoService;
import br.com.linkcom.sined.geral.service.CategoriacnhService;
import br.com.linkcom.sined.geral.service.ColaboradorOcorrenciaService;
import br.com.linkcom.sined.geral.service.ColaboradorOcorrenciaTipoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColaboradorbeneficioService;
import br.com.linkcom.sined.geral.service.ColaboradorcargoService;
import br.com.linkcom.sined.geral.service.ColaboradorcategoriacnhService;
import br.com.linkcom.sined.geral.service.ColaboradorequipamentoService;
import br.com.linkcom.sined.geral.service.ColaboradorsituacaoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EpiService;
import br.com.linkcom.sined.geral.service.EscalaService;
import br.com.linkcom.sined.geral.service.EscalacolaboradorService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.geral.service.TurmaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/rh/crud/Colaborador", authorizationModule=CrudAuthorizationModule.class)
public class ColaboradorCrud extends CrudControllerSined<ColaboradorFiltro, Colaborador, Colaborador> {
	
	private ArquivoService arquivoService;
	private BancoService bancoService;
	private CargoService cargoService;
	private	ColaboradorcategoriacnhService colaboradorcategoriacnhService;
	private	CategoriacnhService categoriacnhService;
	private	ColaboradorcargoService colaboradorcargoService;
	private	ColaboradorService colaboradorService;
	private	PessoaService pessoaService;
	private ColaboradorsituacaoService colaboradorsituacaoService;
	private TurmaService turmaService;
	private EpiService epiService;
	private ContratoService contratoService;
	private CandidatoService candidatoService;
	private TelefoneService telefoneService;
	private AreaService areaService;
	private EscalacolaboradorService escalacolaboradorService;
	private EscalaService escalaService;
	private AgendamentoservicoService agendamentoservicoService;
	private UsuarioService usuarioService;
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private ColaboradorOcorrenciaTipoService colaboradorocorrenciaTiposervice;
	private ColaboradorequipamentoService colaboradorequipamentoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ParametrogeralService parametrogeralService;
	private ColaboradorOcorrenciaService colaboradorOcorrenciaService;
	private ColaboradorbeneficioService colaboradorbeneficioService;
	private BeneficioService beneficioService;
	private BeneficiovalorService beneficiovalorService;
	
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setAreaService(AreaService areaService) {this.areaService = areaService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setEpiService(EpiService epiService) {this.epiService = epiService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setCargoService(CargoService cargoService) {this.cargoService = cargoService;}
	public void setColaboradorcategoriacnhService(ColaboradorcategoriacnhService colaboradorcategoriacnhService) {this.colaboradorcategoriacnhService = colaboradorcategoriacnhService;}
	public void setCategoriacnhService(CategoriacnhService categoriacnhService) {this.categoriacnhService = categoriacnhService;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setColaboradorsituacaoService(ColaboradorsituacaoService colaboradorsituacaoService) {this.colaboradorsituacaoService = colaboradorsituacaoService;}
	public void setTurmaService(TurmaService turmaService) {this.turmaService = turmaService;}
	public void setCandidatoService(CandidatoService candidatoService) {this.candidatoService = candidatoService;}
	public void setEscalacolaboradorService(EscalacolaboradorService escalacolaboradorService) {this.escalacolaboradorService = escalacolaboradorService;}
	public void setEscalaService(EscalaService escalaService) {this.escalaService = escalaService;}
	public void setAgendamentoservicoService(AgendamentoservicoService agendamentoservicoService) {this.agendamentoservicoService = agendamentoservicoService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setColaboradorocorrenciaTiposervice(ColaboradorOcorrenciaTipoService colaboradorocorrenciaTiposervice) {this.colaboradorocorrenciaTiposervice = colaboradorocorrenciaTiposervice;}
	public void setColaboradorequipamentoService(ColaboradorequipamentoService colaboradorequipamentoService) {this.colaboradorequipamentoService = colaboradorequipamentoService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setColaboradorOcorrenciaService(ColaboradorOcorrenciaService colaboradorOcorrenciaService) {this.colaboradorOcorrenciaService = colaboradorOcorrenciaService;}
	public void setColaboradorbeneficioService(ColaboradorbeneficioService colaboradorbeneficioService) {this.colaboradorbeneficioService = colaboradorbeneficioService;}
	public void setBeneficioService(BeneficioService beneficioService) {this.beneficioService = beneficioService;}
	public void setBeneficiovalorService(BeneficiovalorService beneficiovalorService) {this.beneficiovalorService = beneficiovalorService;}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected Colaborador criar(WebRequestContext request, Colaborador colaborador) throws Exception {
		colaborador = super.criar(request, colaborador);
		if(request.getParameter("cdcandidato") != null){
			colaborador.setCdcandidato(Integer.parseInt(request.getParameter("cdcandidato")));
			Candidato candidato = new Candidato(Integer.parseInt(request.getParameter("cdcandidato")));
			candidato = candidatoService.carregaCandidatoContratado(candidato);
			
			colaborador.setNome(candidato.getNome());
			colaborador.setSexo(candidato.getSexo());
			colaborador.setEstadocivil(candidato.getEstadocivil());
			colaborador.setCpf(candidato.getCpf());
			colaborador.setEmail(candidato.getEmail());
			if(candidato.getDtnascimento() != null) colaborador.setDtnascimento(new Date(candidato.getDtnascimento().getTime()));
			
			Set<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class);
			if(candidato.getTelefone() != null){
				Telefone telefone = new Telefone();
				telefone.setTelefone(candidato.getTelefone().getValue());
				telefone.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
				
				listaTelefone.add(telefone);
			}
			
			if(candidato.getCelular() != null){
				Telefone celular = new Telefone();
				celular.setTelefone(candidato.getCelular().getValue());
				celular.setTelefonetipo(new Telefonetipo(Telefonetipo.CELULAR));
				
				listaTelefone.add(celular);
			}
			colaborador.setListaTelefone(listaTelefone);
			
			Endereco end = new Endereco(
					candidato.getLogradouro(), candidato.getNumero(), candidato.getComplemento(),
					candidato.getCaixapostal(), candidato.getCep(),candidato.getBairro(),
					candidato.getMunicipio(), candidato.getMunicipio().getUf());
			colaborador.setEndereco(end);
			
			colaborador.setGrauinstrucao(candidato.getGrauinstrucao());
			colaborador.setPerfilprofissional(candidato.getFormacaoprofissional());
			colaborador.setVerificaCpf(candidato.getCpf());
			colaborador.setDtadmissao(candidato.getDtaltera() != null ? new Date(candidato.getDtaltera().getTime()) : new Date(System.currentTimeMillis()));
		}
		
		
		colaborador.setNacionalidade(Nacionalidade.BRASILEIRA);
		colaborador.setTipoinscricao(new Tipoinscricao(Tipoinscricao.PIS));
		colaborador.setTipocarteiratrabalho(new Tipocarteiratrabalho(Tipocarteiratrabalho.NENHUMA));
		colaborador.setColaboradorsituacao(new Colaboradorsituacao(Colaboradorsituacao.EM_CONTRATACAO));
		colaborador.setRecontratar(true);
		request.setAttribute("cdsexo", 1);
		return colaborador;
	}

	@Override
	protected void excluir(WebRequestContext request, Colaborador bean) throws Exception {
		throw new SinedException("N�o � permitida a exclus�o de um colaborador.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Colaborador bean)throws Exception {
		
		BindException errors = null;
		validateBean(bean, errors);
		validaEstoqueNegativo(bean,errors);
		
		colaboradorService.preparaColaboradorSalvar(bean);
		
		try {
			colaboradorService.geraSaidaEntradaEquipamentoColaborador(bean);
			
			if(bean.getCdcandidato() != null){
				Candidato candidato = new Candidato(bean.getCdcandidato());
				candidato = candidatoService.carregaCandidatoContratado(candidato);
				candidato.setCandidatosituacao(Candidatosituacao.CONTRATADO);
				candidatoService.saveOrUpdate(candidato);
			}
			super.salvar(request, bean);
			
			
			
			if(bean.getCdpessoa() != null && bean.getEndereco() != null && bean.getEndereco().getCdendereco() != null){
				enderecoService.updatePessoa(bean.getEndereco(), bean);
			}
			
		} catch (DataIntegrityViolationException e) {
			if ((DatabaseError.isKeyPresent(e, "IDX_PESSOA_CPF"))
					||(DatabaseError.isKeyPresent(e, "IDX_COLABORADOR_CTPS")) 
					|| (DatabaseError.isKeyPresent(e, "IDX_PESSOA_EMAIL"))
					|| (DatabaseError.isKeyPresent(e, "IDX_COLABORADOR_PIS"))) {
				throw new SinedException("Colaborador j� cadastrado no sistema.");
			}else {
				e.printStackTrace();
				throw e;			
			}
		}		
	} 	
	
	protected void validaEstoqueNegativo(Colaborador bean,BindException errors){
			
		if(!parametrogeralService.getBoolean("PERMITIR_ESTOQUE_NEGATIVO")){
			
			if(bean.getListaColaboradorequipamento() != null && !bean.getListaColaboradorequipamento().isEmpty()){
				for(Colaboradorequipamento colaboradorequipamento : bean.getListaColaboradorequipamento()){
					if(colaboradorequipamento.getCdlocalarmazenagem() != null && colaboradorequipamento.getQtde() != null && colaboradorequipamento.getDtdevolucao() == null){
						movimentacaoestoqueService.validaEntregaEPI(new Localarmazenagem(colaboradorequipamento.getCdlocalarmazenagem()), colaboradorequipamento.getEpi(), colaboradorequipamento.getQtde().doubleValue());
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Colaborador colaborador){
		pessoaService.ajustaPessoaToLoad(colaborador);

		
		// COMBO BANCO
		request.setAttribute("bancos", bancoService.findAtivos());
		request.setAttribute("areas", areaService.findAllForFlex());
		request.setAttribute("hoje", new SimpleDateFormat("dd/MM/yyyy").format(new Date(System.currentTimeMillis())));
		
		if(colaborador.getCdpessoa()!=null){
			//colaborador.setCpf(colaboradorService.getCpfColaborador(colaborador));
			//M�todo colocado aqui, pois estava carregando items duplicados no updateentrada
			colaborador.setListaEscalacolaborador(escalacolaboradorService.findByColaborador(colaborador));
			colaborador.setListaColaboradorocorrencia(SinedUtil.listToSet(colaboradorOcorrenciaService.findByColaborador(colaborador), ColaboradorOcorrencia.class));
			colaborador.setListaColaboradorequipamento(colaboradorequipamentoService.loadByColaborador(colaborador));
			if (request.getAttribute("validate_beneficio")==null){
				colaborador.setListaColaboradorbeneficio(colaboradorbeneficioService.findByColaborador(colaborador));
			}
		}
		
		if (request.getAttribute(CONSULTAR) != null) {
			colaborador.setValidaaux("verdadeiro");
			colaborador.setValidaColaboradorcargo("verdadeiro");
		}
		if(colaborador.getColaboradordeficiente() == null){
			colaborador.setColaboradordeficiente(Colaboradordeficiencia.NAO_POSSUI);
		}
		//categoria cnh do colaborador
		Set<Colaboradorcategoriacnh> listaColaboradorcategoriacnh = new ListSet<Colaboradorcategoriacnh>(Colaboradorcategoriacnh.class);
		Set<Categoriacnh> listaCategoriacnh = colaboradorcategoriacnhService.carregaCategoriacnh(listaColaboradorcategoriacnh, colaborador);
		if(request.getBindException().getErrorCount() == 0){
			colaborador.setListaCategoriacnh(listaCategoriacnh);
		}
		
		if (colaborador.getContaContabil() != null){
			colaborador.setContaContabil(new ContaContabil(colaborador.getContaContabil().getCdcontacontabil()));
		}
		
		if (colaborador.getFornecedor()!=null && colaborador.getFornecedor().getCdpessoa()!=null){
			colaborador.setFornecedor(FornecedorService.getInstance().load(colaborador.getFornecedor()));
		}
		
		request.setAttribute("listacolaboradorOcorrenciaTipo", colaboradorocorrenciaTiposervice.findAll());
		request.setAttribute("listaCategoriacnhCompleta", categoriacnhService.findAll());
		request.setAttribute("listaEpi", epiService.findAllEpisAtivos());
		request.setAttribute("listaEmpresa", empresaService.findAtivos(colaborador.getEmpresa()));
		request.setAttribute("possuiAutorizacaoRemoverAnexo", SinedUtil.isUserHasAction("REMOVER_ANEXO_COLABORADOR"));
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Colaborador form)throws CrudException {
		request.setAttribute("validacpf", false);
		return super.doEditar(request, form);
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	protected ListagemResult<Colaborador> getLista(WebRequestContext request, ColaboradorFiltro filtro) {
		ListagemResult<Colaborador> listagemResult = super.getLista(request, filtro);
		List<Colaborador> list = listagemResult.list();
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdpessoa", ",");
		List<Usuario> listaUsuario = usuarioService.findForGerenciaPlanejamento(whereIn);
		for (Colaborador c : list) {
			c.setListaTelefone(SinedUtil.listToSet(telefoneService.findByPessoa(c), Telefone.class));
			
			for (Usuario u : listaUsuario) {
				if (c.getCdpessoa().equals(u.getCdpessoa())){
					c.setPossuiUsuario(true);
				}
			}
			
			if(c.getListaTelefone()!= null){
				c.setTelefones(telefoneService.createListaTelefone(c.getListaTelefone(), "<br>"));
			}
		}
		return listagemResult;
	}
	
	@Override
	protected Colaborador carregar(WebRequestContext request, Colaborador bean)throws Exception {
		Colaborador colaborador = super.carregar(request, bean); 
		
		// CARREGA O HISTORICO
		ListSet<Colaboradorcargo> historico = new ListSet<Colaboradorcargo>(Colaboradorcargo.class,
				colaboradorcargoService.findHistoricoProfissional(colaborador));
		
		colaborador.setListaColaboradorcargo(historico);
		
		// CARREGA OS DADOS DO CARGO ATUAL
		colaborador = colaboradorService.carregaColaboradorCargoAtual(colaborador);
		
		// CARREGA A DATA DE DEMISSAO (DATA FIM DO ULTIMO CARGO)
		if (colaborador.getCdcolaboradorcargo() == null && historico.size() > 0) {
			Colaboradorcargo ultimocargo = historico.get(historico.size()-1);
			colaborador.setDtdemissao(ultimocargo.getDtfim());
			colaborador.setCargotransient(ultimocargo.getCargo());
			colaborador.setDepartamentotransient(ultimocargo.getDepartamento());
			colaborador.setRegimecontratacao(ultimocargo.getRegimecontratacao());
			colaborador.setEmpresa(ultimocargo.getEmpresa());
			colaborador.setSindicato(ultimocargo.getSindicato());
		}			 
		
		request.setAttribute("cdsexo", colaborador.getSexo().getCdsexo());
		request.setAttribute("disabled", "disabled");
		
		// LOAD DA FOTO
		try {
			arquivoService.loadAsImage(colaborador.getFoto());
			if(colaborador.getFoto().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), colaborador.getFoto().getCdarquivo());
				request.setAttribute("exibeFoto", true);
			}
		} catch (Exception e) {
			request.setAttribute("exibeFoto", false);
		}
		
		return colaborador;
	}
	
	@Override
	protected void validateBean(Colaborador bean, BindException errors) {
		WebRequestContext request = (WebRequestContext) Neo.getRequestContext();
		
		
		//VALIDA A INCLUS�O DE �REAS REPETIDAS
		Set<Colaboradorarea> areas = bean.getListaColaboradorarea();
		if(areas != null){
			int j = 0;
			int k = 0;
			for (Colaboradorarea colaboradorarea : areas) {
				k++;
				int codigoArea = colaboradorarea.getArea().getCdarea();

				for (Colaboradorarea colaboradorarea2 : areas) {
					if(codigoArea == colaboradorarea2.getArea().getCdarea()){
						j++;
					}
				}
			}
			if(j > k){
				errors.reject("001","N�o � permitido cadastrar �reas repetidas.");
			}
		}
		
		
		// VALIDA EXTENS�O DA FOTO
		Arquivo foto = bean.getFoto();
		if(foto!=null && foto.getContent().length != 0){
			String content = bean.getFoto().getContenttype().toUpperCase(); 
			if(!(content.contains("JPEG") || content.contains("JPG") || content.contains("BMP") || content.contains("GIF") || content.contains("PNG"))){
				errors.reject("001","Extens�o de imagem n�o permitida. Extens�es permitidas: png, jpg, jpeg, gif e bmp.");
				request.setAttribute("exibeFoto", false);
			}
		}
		
		// VERIFICA DATAS
		Date dtnascimento = bean.getDtnascimento();
		if(dtnascimento != null && dtnascimento.after(new Date(System.currentTimeMillis()))){
			errors.reject("001","A Data de nascimento deve ser anterior � data de hoje.");
		}
		Date dtemissaocarteiratrabalho = bean.getDtemissaocarteiratrabalho();
		if(dtemissaocarteiratrabalho != null && dtemissaocarteiratrabalho.after(new Date(System.currentTimeMillis()))){
			errors.reject("001","A Data de emiss�o da carteira profissional deve ser anterior � data de hoje.");
		}
		Date dtemissaocnh = bean.getDtemissaocnh();
		if(dtemissaocnh != null && dtemissaocnh.after(new Date(System.currentTimeMillis()))){
			errors.reject("001","A Data de emiss�o da CNH deve ser anterior � data de hoje.");
		}
		Date dtemissaoctps = bean.getDtEmissaoCtps();
		if(dtemissaoctps != null && dtemissaoctps.after(new Date(System.currentTimeMillis()))){
			errors.reject("001","A Data de emiss�o da CTPS deve ser anterior � data de hoje.");
		}
		Date dtemissaorg = bean.getDtemissaorg();
		if(dtemissaorg!= null && dtemissaorg.after(new Date(System.currentTimeMillis()))){
			errors.reject("001","A Data de emiss�o do RG deve ser anterior � data de hoje.");
		}
		
				
		// MONTA O CAMPO DO CPF NOVAMENTE
		if(bean.getCdpessoa()!=null) 
			bean.setCpf(colaboradorService.getCpfColaborador(bean));
		if(bean.getCpf()==null) 
			bean.setCpf(bean.getVerificaCpf());
		
		request.setAttribute("validacpf", false);
		if (bean.getCdpessoa() != null) {
			Colaborador colaborador = colaboradorService.load(bean);
			bean.setCpf(colaborador.getCpf());
		}
		
		// VERIFICA E-MAIL
		if (bean.getEmail()!=null && !bean.getEmail().equals("") && !SinedUtil.verificaEmail(bean.getEmail())) {
			errors.reject("001", "E-mail inv�lido.");
		}
		
		bean.setValidaaux("verdadeiro");
		
		// VALIDA N�MERO DE C�NJUGES
		Set<Colaboradordependente> dependentes = bean.getListaColaboradordependente();
		if(dependentes != null){
			int i = 0;
			for (Colaboradordependente colaboradordependente : dependentes) {
				if(colaboradordependente.getTipodependente() != null && colaboradordependente.getTipodependente().getCdtipodependente().equals(Tipodependente.CONJUGE)) i++;
				if(i>1){
					errors.reject("001","N�o deve haver mais de um c�njuje cadastrado em dependentes.");
					break;
				}
			}

			//VERIFICA DATAS EM DEPENDENTE
			boolean erroPensao = false;
			for (Colaboradordependente dependent : dependentes) {
				if(dependent.getDtbaixapensao() != null && dependent.getDtpensao() != null){
					if(dependent.getDtpensao().after(dependent.getDtbaixapensao()) && !erroPensao){
						errors.reject("001","Data de aquisi��o da pens�o deve ser anterior � data de baixa da pens�o aliment�cia.");
						erroPensao = true;
					}
				}
			}
		}
		
		
		//VERIFICA NO HIST�RICO DATAS FUTURAS E INTERSE��O DE DTINICIO E DTFIM
		if(bean.getListaColaboradorcargo() != null){
			List<Colaboradorcargo> historico = new ArrayList<Colaboradorcargo>();
			for (Colaboradorcargo colaboradorcargo : bean.getListaColaboradorcargo()) {
				historico.add(colaboradorcargo);
			}
			boolean errorAfter = false;
			boolean errorInter = false;
			boolean errorPeriodo = false;
			Date hoje = new Date(System.currentTimeMillis());
			for (int i=0; i<historico.size(); i++) {
				for(int j=i+1; j<historico.size(); j++){
					
					Date dtinicioI = historico.get(i).getDtinicio();
					Date dtfimI = historico.get(i).getDtfim();
					Date dtinicioJ = historico.get(j).getDtinicio();
					Date dtfimJ = historico.get(j).getDtfim();
					
					if(dtinicioI.after(hoje) || (dtfimI != null && dtfimI.after(hoje)) || dtinicioJ.after(hoje) || (dtfimJ != null && dtfimJ.after(hoje))){
						if(!errorAfter){
							errors.reject("001","Todas as datas no hist�rico devem ser anteriores � data atual.");
							errorAfter = true;
						}
					}
					
					if((dtinicioI.after(dtinicioJ) && dtfimJ != null && dtinicioI.before(dtfimJ)) ||
						(dtfimI != null && dtfimI.after(dtinicioJ) && dtfimI.before(dtfimJ)))
						{
							if(!errorInter){
								SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
								errors.reject("001","Um colaborador n�o pode ter dois cargos no mesmo per�odo. " +
										"Verifique, no hist�rico, o intervalo "+format.format(dtinicioJ)+" e "+format.format(dtfimJ)+".");
								errorInter = true;
							}
						}
				}
			}
			for (Colaboradorcargo colcargo : historico) {
				if(colcargo.getDtfim() != null && colcargo.getDtinicio().after(colcargo.getDtfim()) && !errorPeriodo){
					errors.reject("001","Data in�cio deve ser anterior � data fim no hist�rico.");
					errorPeriodo = true;
				}
			}
		}
		
		//VERIFICA DATAS CNH
		if(bean.getCnh()!=null && !bean.getCnh().equals("")){
			if(bean.getDtemissaocnh()==null)
				errors.reject("001","O campo CNH - Data de emiss�o � obrigat�rio.");
			
			if(bean.getDtvalidadecnh()==null)
				errors.reject("001","O campo CNH - Data de validade � obrigat�rio.");
		}
		
		// VALIDA O PIS/PASEP
		if(bean.getTipoinscricao() != null){
			if((Tipoinscricao.PIS.equals(bean.getTipoinscricao().getCdtipoinscricao()) || 
					Tipoinscricao.PASEP.equals(bean.getTipoinscricao().getCdtipoinscricao())) &&
					bean.getNumeroinscricao()!=null && !bean.getNumeroinscricao().equals("") && 
					!SinedUtil.verificaPisPasep(bean.getNumeroinscricao())){
				errors.reject("001","PIS/PASEP inv�lido.");
			}
		}
		
		//VERIFICA DADOS BANCARIOS
		if(bean.getColaboradorformapagamento()!=null &&
				(Colaboradorformapagamento.DEPOSITO_CONTA_CORRENTE.equals(bean.getColaboradorformapagamento().getCdcolaboradorformapagamento())
				|| Colaboradorformapagamento.DEPOSITO_CONTA_POUPANCA.equals(bean.getColaboradorformapagamento().getCdcolaboradorformapagamento()))){
			if(bean.getDadobancario().getTipoconta()==null){
				errors.reject("001","O campo Tipo de conta � obrigat�rio.");
			}
			if(bean.getDadobancario().getBanco()==null){
				errors.reject("001","O campo Banco � obrigat�rio.");
			}
			if(bean.getDadobancario().getAgencia().equals("")){
				errors.reject("001","O campo Ag�ncia � obrigat�rio.");
			}
			if(bean.getDadobancario().getConta().equals("")){
				errors.reject("001","O campo Conta � obrigat�rio.");
			}
		}
		if(bean.getCdpessoa()!= null){
			Money salario = bean.getSalario();
			Sindicato sindicato = bean.getSindicato();
			Empresa empresa = bean.getEmpresa();
			bean = colaboradorService.carregaColaboradorCargoAtual(bean);
			bean.setSalario(salario);
			bean.setSindicato(sindicato);
			bean.setEmpresa(empresa);
			request.setAttribute("disabled", "disabled");
		}
		
		//VERIFICA ESCALA DE HOR�RIO EXCLUIDA OU ALTERADA SE POSSUI AGENDAMENTO SERVI�O
		if(bean.getCdpessoa() != null){
			List<Escala> listaEscala = escalaService.findByColaborador(bean);
			List<Escala> escalasAlteradas = new ArrayList<Escala>();
			if(listaEscala != null && !listaEscala.isEmpty()){
				for (Escala escala : listaEscala) {
					boolean alterado = true;
					if(bean.getListaEscalacolaborador() != null && !bean.getListaEscalacolaborador().isEmpty()){
						for (Escalacolaborador escalacolaborador : bean.getListaEscalacolaborador()) {
							if(escalacolaborador.getEscala() != null && escalacolaborador.getEscala().equals(escala)){
								alterado = false;
								break;
							}
						}
					}
					if(alterado)
						escalasAlteradas.add(escala);
				}
				if(escalasAlteradas != null && !escalasAlteradas.isEmpty())
					for (Escala escalaAux : escalasAlteradas) 
						if(agendamentoservicoService.verificaAgendamentoColaboradorEmAbertoOuAgendado(bean, escalaAux))
							request.addMessage("A escala "+escalaAux.getNome()+" possui agendamentos com a situa��o EM ANDAMENTO OU AGENDADO.", MessageType.WARN);
			}
		}

		//Verifica Escalas colaborador
		if(bean.getCdpessoa() != null){
			if(bean.getListaEscalacolaborador() != null && !bean.getListaEscalacolaborador().isEmpty()){
				for (Escalacolaborador escalacolaborador : bean.getListaEscalacolaborador()) {
					if(escalacolaborador.getCdescalacolaborador() != null){
						if(agendamentoservicoService.isEscalacolaboradorForaPeriodo(bean, escalacolaborador)){
							Escala escala = escalaService.load(escalacolaborador.getEscala());
							request.addMessage("Existe(m) marca��o(�es) ativa(s) para a escala "+escala.getNome()+" fora do per�odo informado.", MessageType.WARN);
						}
					}
				}
			}
		}
		
		//VALIDA Equipamento de colaborador
		if(bean.getCdpessoa()!=null){
			//Antes da intera��o
			List<Colaboradorequipamento> listaAntes = colaboradorequipamentoService.loadByColaborador(bean);
			//Ap�s
			List<Colaboradorequipamento> listaDepois = bean.getListaColaboradorequipamento();
			
			if(listaAntes!=null && !listaAntes.isEmpty()){
				if(listaDepois==null || listaDepois.isEmpty())
					errors.reject("001", "N�o � poss�vel excluir um registro de equipamento salvo anteriormente, preencher data de devolu��o");
				
				for(Colaboradorequipamento antes : listaAntes){
					
					boolean achou = false;
					for(Colaboradorequipamento depois : listaDepois){
						if(antes.getCdcolaboradorequipamento()!=null && antes.getCdcolaboradorequipamento().equals(depois.getCdcolaboradorequipamento())){
							achou = true;
							break;
						}
					}
					if(!achou){
						errors.reject("001", "N�o � poss�vel excluir um registro de equipamento salvo anteriormente, preencher data de devolu��o");
					}
				}
			}
		}
		
		if (bean.getListaColaboradorbeneficio()!=null && !bean.getListaColaboradorbeneficio().isEmpty()){
			for (Colaboradorbeneficio colaboradorbeneficio1: bean.getListaColaboradorbeneficio()){
				int cont = 0;
				for (Colaboradorbeneficio colaboradorbeneficio2: bean.getListaColaboradorbeneficio()){
					if (colaboradorbeneficio1.getBeneficio().getCdbeneficio().equals(colaboradorbeneficio2.getBeneficio().getCdbeneficio())){
						cont++;
					}
				}
				if (cont>1){
					errors.reject("001", "O benef�cio s� pode ser informado uma vez por colaborador.");
					request.setAttribute("validate_beneficio", new Object());
					break;
				}
			}
		}
	}	
	
	@Override
	protected void listagem(WebRequestContext request, ColaboradorFiltro filtro)throws Exception {
		request.setAttribute("listaSituacoes", colaboradorsituacaoService.findAll());
		request.setAttribute("listaTurma", turmaService.findForListagemColaborador());
		
		String limpa = request.getParameter("limpa");
		if(limpa != null && limpa.toUpperCase().equals("TRUE")){
			filtro.setIdscontrato(null);
			filtro.setIdstarefa(null);
		}
		if(!StringUtils.isBlank(request.getParameter("open"))){
			String idscontrato = request.getParameter("idscontrato");
			String idstarefa = request.getParameter("idstarefa");
			if(!StringUtils.isBlank(idscontrato) && !"<null>".equals(idscontrato)){
				filtro.setIdstarefa(null);
			}
			if(!StringUtils.isBlank(idstarefa) && !"<null>".equals(idstarefa)){
				filtro.setIdscontrato(null);
			}
		}
		
		boolean isPopup = filtro.getIdscontrato() != null || filtro.getIdstarefa() != null;
		request.setAttribute("isPopup", isPopup);
		
		if(isPopup) request.setAttribute("clearBase", isPopup);
		
		super.listagem(request, filtro);
	}
	
	/**
	 * M�todo para verificar se j� existe pessoa cadastrada com o cpf informado e 
	 * retorna os dados solicitados para o cadastro de colaborador.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PessoaService#findPessoaByCpf(Cpf)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#findColaboradorusuario(Integer)
	 * @see br.com.linkcom.neo.view.ajax.View#convertObjectToJs(Object, String)
	 * @param request
	 * @author Jo�o Paulo Zica
	 * 		   Fl�vio Tavares
	 */
	public void getInfoCpf(WebRequestContext request) {
		String cpf = request.getParameter("cpf");
		
		Pessoa pessoa = pessoaService.findPessoaByCpf(new Cpf(cpf));
		request.getServletResponse().setContentType("text/html");
		View current = View.getCurrent();
		if (pessoa == null){
			current.eval("var status = 'pessoa';");
		}else{
			Integer cdpessoa = pessoa.getCdpessoa();
			Colaborador colaborador = colaboradorService.findColaboradorusuario(cdpessoa);
			if(colaborador == null){
				current.eval("var status = 'usuario';");
			}else{
				current.eval("var status = 'colaborador';");
				if(!colaborador.getRecontratar()) current.eval("var recontratar = true;");
				else current.eval("var recontratar = false;");
			}
		}
		current.convertObjectToJs(pessoa, "pessoa");
	}
	
	/**
	 * M�todo para filtrar os cargos pelo departamento na aba de hist�rico profissional.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CargoService#findCargo(Departamento)
	 * @see br.com.linkcom.sined.geral.service.CargoService#findAll()
	 * @param request
	 * @param departamento
	 * @author Fl�vio
	 */
	public void makeAjaxCargo(WebRequestContext request, Departamento departamento){
		List<Cargo> listCargo = null;
		String par = request.getParameter("departamento");
		try {
			Integer cddepartamento = Integer.parseInt(
					par.substring(par.lastIndexOf("=")+1, par.lastIndexOf("]"))
			);
			departamento.setCddepartamento(cddepartamento);
			listCargo = cargoService.findCargo(departamento);
		} catch (Exception e) {
			listCargo = cargoService.findAll();
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listCargo, "listaCargo", ""));
	}
	
	public void makeAjaxContrato(WebRequestContext request, Contrato contrato){
		List<Contrato> listContrato = contrato.getCliente() != null ? contratoService.findBy(contrato.getCliente(), true) : contratoService.findAll();
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listContrato, "listContrato", ""));
	}
	
	
	
	/**
	 * M�todo para carregar o pop-up para o cadastro dos colaboradores na turma.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TurmaService#findForListagemColaborador()
	 * @param request
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("exibirEntradaTurma")
	@Input(LISTAGEM)
	public ModelAndView exibeColaboradorTurma(WebRequestContext request){
		String ids = request.getParameter("ids");
		request.setAttribute("clearBase", true);
		List<Turma> listaTurma = turmaService.findForListagemColaborador();
		return new ModelAndView("direct:crud/popup/selecionaColaboradorTurma")
					.addObject("listaTurma", listaTurma)
					.addObject("ids", ids);
	}
	
	/**
	 * M�todo para cadastrar colaboradores em uma turma.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#adicionaColaboradorTurma(java.util.Collection, Turma, StringBuilder, StringBuilder)
	 * @param request
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("saveColaboradorTurma")
	public void saveColaboradorTurma(WebRequestContext request, ColaboradorFiltro filtro){
		Turma turma = filtro.getTurma();
		String ids = request.getParameter("ids");
		if(turma == null || turma.getCdturma() == null || ids == null || ids.equals("")){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		String[] cds = ids.split(",");
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		for (String id : cds) {
			listaColaborador.add(new Colaborador(Integer.parseInt(id)));
		}
		StringBuilder conflitantes = new StringBuilder();
		StringBuilder existentes = new StringBuilder();
		boolean test = colaboradorService.adicionaColaboradorTurma(listaColaborador, turma, conflitantes, existentes);
		
		if(test) request.addMessage("Colaborador(es) cadastrado(s) com sucesso para a turma.");
		if(!conflitantes.toString().equals("")){
			request.addMessage("O(s) colaborador(es) "+conflitantes+" est�(�o) alocado(s) em outro treinamento neste per�dodo.", MessageType.WARN);
		}
		if(!existentes.toString().equals("")){
			request.addMessage("O(s) colaborador(es) "+existentes+" j� est�(�o) cadastrado(s) para esta turma.", MessageType.WARN);
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
	}
	
	/**
	 * M�todo para carregar o pop-up para o cadastro dos colaboradores no projeto.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#findAll();
	 * @param request
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("exibirEntradaContrato")
	@Input(LISTAGEM)
	public ModelAndView exibirEntradaContrato(WebRequestContext request){
		String ids = request.getParameter("ids");
		request.setAttribute("clearBase", true);
		List<Contrato> listaProjetos = contratoService.findAll();
		
		ColaboradorFiltro filtro = new ColaboradorFiltro();
		filtro.setDtinicio(new Date(System.currentTimeMillis()));
		
		return new ModelAndView("direct:crud/popup/selecionaColaboradorContrato")
					.addObject("filtro", filtro)
					.addObject("listaProjetos", listaProjetos)
					.addObject("ids", ids);
	}
	
	/**
	 * M�todo para cadastrar colaboradores em um contrato.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService.adicionaColaboradorContrato
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("saveColaboradorContrato")
	public void saveColaboradorContrato(WebRequestContext request, ColaboradorFiltro filtro){
		Contratocolaborador cc = new Contratocolaborador(
				filtro.getContrato(), 
				filtro.getEscala(), 
				filtro.getDtinicio(), 
				filtro.getDtfim(), 
				filtro.getHorainicio(), 
				filtro.getHorafim());
		
		String ids = request.getParameter("ids");
		
		String[] cds = ids.split(",");
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		for (String id : cds) {
			listaColaborador.add(new Colaborador(Integer.parseInt(id)));
		}
		
		StringBuilder existentes = new StringBuilder();
		boolean test = colaboradorService.adicionaColaboradorContrato(listaColaborador, cc, existentes);
		
		if(test) request.addMessage("Colaborador(es) cadastrado(s) com sucesso para o contrato.");
		if(!existentes.toString().equals("")){
			request.addMessage("O(s) colaborador(es) "+existentes+" j� est�(�o) cadastrado(s) para este contrato.", MessageType.WARN);
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
	}
	
	/**
	 * M�todo para carregar o pop-up para o cadastro dos colaboradores no grupo de email.
	 * 
	 * @param request
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("exibirEntradaGrupoemail")
	@Input(LISTAGEM)
	public ModelAndView exibeColaboradorGrupoemail(WebRequestContext request){
		if(!SinedUtil.isUserHasAction("INCLUIR_GRUPO")){
			throw new SinedException("Infelizmente o usu�rio logado n�o tem permiss�o para executar esta a��o." +
					" Entre em contato com o administrador do sistema para soliciar a permiss�o.");
		}
		String ids = request.getParameter("ids");
		request.setAttribute("clearBase", true);
		return new ModelAndView("direct:crud/popup/selecionaColaboradorGrupoemail")
					.addObject("ids", ids);
	}
	
	/**
	 * M�todo para cadastrar colaboradores em uma grupo de email.
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("saveColaboradorGrupoemail")
	public void saveColaboradorGrupoemail(WebRequestContext request, ColaboradorFiltro filtro){
		Grupoemail grupoemail = filtro.getGrupoemail();
		String ids = request.getParameter("ids");
		if(!SinedUtil.isObjectValid(grupoemail) || ids == null || ids.equals("")){
			throw new SinedException("Par�metros inv�lidos.");
		}
		
		String[] cds = ids.split(",");
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		for (String id : cds) {
			listaColaborador.add(new Colaborador(Integer.parseInt(id)));
		}
		StringBuilder existentes = new StringBuilder();
		boolean test = colaboradorService.adicionaColaboradorGrupoemail(listaColaborador, grupoemail, existentes);
		
		if(test) request.addMessage("Colaborador(es) cadastrado(s) com sucesso para o grupo.");
		if(!existentes.toString().equals("")){
			request.addMessage("Colaborador(es) "+existentes+" j� cadastrado(s) para este grupo.", MessageType.WARN);
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
	}
	
	/**
	 * Retorna uma lista com todos os cargos para combo cargo
	 * @param request
	 * @autthor Taidson
	 * @since 22/06/2010
	 */
	public void ajaxCarregaCargos(WebRequestContext request){
		List<Cargo> listaCargos = cargoService.carregaCargos();
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaCargos, "listaCargos", ""));
	}
	
	/**
	 * 
	 * Obt�m a quantidade de itens dispon�vel no local especificado
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @since 25/09/2014
	 * @author Rafael Patr�cio
	 */
	public ModelAndView getQuantidadeDisponivel(WebRequestContext request, ColaboradorFiltro filtro){
		return new JsonModelAndView().addObject("quantidade", 
				movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(filtro.getMaterial(), Materialclasse.EPI, filtro.getLocalarmazenagem()).toString());
	}
	
	/**
	 * 
	 * Abre um pop-up para o usu�rio escolher o local de armazenagem do EPI
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @since 25/07/2014
	 * @author Rafael Patr�cio
	 */
	public ModelAndView abrirPopupSelecionarLocalOrigemEPI(WebRequestContext request, ColaboradorFiltro filtro){
		request.setAttribute("indexColaborador", filtro.getIndexColaborador());
		request.setAttribute("estoqueNegativo", parametrogeralService.getBoolean("PERMITIR_ESTOQUE_NEGATIVO"));
		request.setAttribute("devolucao", request.getParameter("devolucao"));
		return new ModelAndView("direct:crud/popup/popupMateriais").addObject("filtro", filtro);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView beneficiotipoAjax(WebRequestContext request, Colaborador colaborador){
		String beneficiotipoStr = "";
		Money valor = new Money();
		
		if (colaborador.getBeneficio()!=null){
			beneficiotipoStr = beneficioService.load(colaborador.getBeneficio(), "beneficio.beneficiotipo").getBeneficiotipo().getNome();
			List<Beneficiovalor> listaBeneficiovalor = beneficiovalorService.findByCdsbeneficio(colaborador.getBeneficio().getCdbeneficio().toString());
			
			if (!listaBeneficiovalor.isEmpty()){
				Collections.sort(listaBeneficiovalor, new BeanComparator("dtinicio"));
				valor = listaBeneficiovalor.get(listaBeneficiovalor.size()-1).getValor();
			}			
		}
		
		return new JsonModelAndView().addObject("beneficiotipoStr", beneficiotipoStr).addObject("valor", valor);
	}	
}