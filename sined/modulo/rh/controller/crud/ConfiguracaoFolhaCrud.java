package br.com.linkcom.sined.modulo.rh.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.ConfiguracaoFolha;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ConfiguracaoFolhaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/ConfiguracaoFolha", authorizationModule=CrudAuthorizationModule.class)
public class ConfiguracaoFolhaCrud extends CrudControllerSined<ConfiguracaoFolhaFiltro, ConfiguracaoFolha, ConfiguracaoFolha>{

	private EmpresaService empresaService;
	private AgendamentoService agendamentoService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {
		this.agendamentoService = agendamentoService;
	}

	@Override
	protected void entrada(WebRequestContext request, ConfiguracaoFolha form) throws Exception {
		if(form.getEmpresa() != null){
			form.setEmpresa(empresaService.loadWithoutWhereEmpresa(form.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia"));
		}
		request.setAttribute("listaEmpresa", empresaService.findAtivos());
	}
	
	@Override
	protected void validateBean(ConfiguracaoFolha bean, BindException errors) {
		if(bean.getEmpresa() != null && bean.getAgendamento() != null){
			Agendamento agendamento = agendamentoService.loadWithPessoa(bean.getAgendamento());
			if(agendamento != null && agendamento.getEmpresa() != null && !agendamento.getEmpresa().equals(bean.getEmpresa())){
				errors.reject("001", "A empresa da configura��o da folha � diferente da empresa do agendamento.");
			}
		}
	}
}
