package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Dominioemailalias;
import br.com.linkcom.sined.geral.bean.Dominiozona;
import br.com.linkcom.sined.geral.bean.Servicoservidor;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.DominioService;
import br.com.linkcom.sined.geral.service.DominioemailService;
import br.com.linkcom.sined.geral.service.ServicoservidorService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominioFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Dominio", 
		authorizationModule=CrudAuthorizationModule.class
)
public class DominioCrud extends CrudControllerSined<DominioFiltro, Dominio, Dominio>{
	
	private ContratomaterialService contratomaterialService;
	private ServicoservidorService servicoservidorService;
	private DominioemailService dominioemailService;
	private DominioService dominioService;
	
	public void setDominioService(DominioService dominioService) {
		this.dominioService = dominioService;
	}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {
		this.contratomaterialService = contratomaterialService;
	}
	public void setServicoservidorService(ServicoservidorService servicoservidorService) {
		this.servicoservidorService = servicoservidorService;
	}
	public void setDominioemailService(DominioemailService dominioemailService) {
		this.dominioemailService = dominioemailService;
	}
	
	@Override
	protected Dominio carregar(WebRequestContext request, Dominio bean) throws Exception {
		Dominio dominio = super.carregar(request, bean);
		if (dominio.getContratomaterial() != null)
			dominio.setCliente(dominio.getContratomaterial().getContrato().getCliente());
		return dominio;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Dominio bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_DOMINIO_NOME")){
				throw new SinedException("Dom�nio j� cadastrado no sistema.");
			}
		}		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Dominio bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Dom�nio n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
	
	@Override
	protected void validateBean(Dominio bean, BindException errors) {
		Set<Dominioemailalias> lista = bean.getListaEmailalias();
		if (lista != null && !lista.isEmpty()){
			for (Dominioemailalias dominioemailalias : lista) {
				if (!dominioemailalias.getUsuarioorigem().matches("[A-Za-z0-9\\.\\-_]+")){
					errors.reject("001", "Caracteres inv�lidos em usu�rio de origem.");		
				}			
			}
		}
		
		
		int numReg = dominioService.countContratomaterial(bean.getCddominio(), bean.getContratomaterial());
		Double qtde = contratomaterialService.load(bean.getContratomaterial(), "contratomaterial.qtde").getQtde();
		
		if(qtde <= numReg){
			errors.reject("002","N�o se pode criar mais registros que a quantidade do servi�o no contrato.");
		}
	}
	
	@Action("filtrarcontrato")
	public ModelAndView filtrarContrato(WebRequestContext request, Contratomaterial contratomaterial) throws CrudException {
		Cliente cliente = contratomaterialService.findCliente(contratomaterial);
		DominioFiltro filtro = new DominioFiltro();
		filtro.setCliente(cliente);
		filtro.setContratomaterial(contratomaterial);
		ListagemResult<Dominio> lista = getLista(request, filtro);
		if (lista != null && !lista.list().isEmpty() && lista.list().size() >= contratomaterial.getQtde())
			return doListagem(request, filtro);
		else {
			Dominio form = new Dominio();
			form.setCliente(cliente);
			form.setContratomaterial(contratomaterial);
			
			((DefaultWebRequestContext)request).setLastAction("entrada");
			return doEntrada(request, form);			
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Dominio form) throws Exception {
		Map<Integer, List<Servicoservidor>> listaServicoServidor = new HashMap<Integer, List<Servicoservidor>>();
		if(form.getListaDominiozona() != null){
			int i = 0;			
			Set<Dominiozona> lista = form.getListaDominiozona();
			for (Dominiozona dominiozona : lista) {									
				if (dominiozona.getServicoservidor() != null){
					Servidor servidor = dominiozona.getServicoservidor().getServidor();
					listaServicoServidor.put(i, servicoservidorService.find(servidor));
				}				
				i++;
			}
		}
		request.setAttribute("listaServicoServidor", listaServicoServidor);
	}
	
	/**
	 * Retorna lista de servicoservidor de acordo com o servidor
	 * @param request
	 * @param servidor
	 * @author Thiago Gon�alves
	 */
	public void makeAjaxDominiozona(WebRequestContext request, Servidor servidor){
		List<Servicoservidor> listServicoservidor = null;
		String par = request.getParameter("servidor");
		try {
			Integer cdservidor = Integer.parseInt(par.substring(par.lastIndexOf("=")+1, par.lastIndexOf("]")));
			servidor.setCdservidor(cdservidor);
			listServicoservidor = servicoservidorService.find(servidor);
		} catch (Exception e) {
			listServicoservidor = new ArrayList<Servicoservidor>();
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listServicoservidor, "listaServicoservidor", ""));
	}
	
	/**
	 * M�todo chamado via ajax. Este m�todo � disparado qnd o usu�rio desmarca o checkbox email. Verifica se h� algum dominioEmail 
	 * referente ao dominio
	 * 
	 * @param request
	 * @param dominio
	 * @Auhtor Tom�s Rabelo
	 */
	public void verificaDominioEmailAjax(WebRequestContext request, Dominio dominio){
		boolean existeEmailParaDominio = dominioemailService.existeDominioEmail(dominio);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var existeEmailParaDominio = "+existeEmailParaDominio);
	}
	
}
