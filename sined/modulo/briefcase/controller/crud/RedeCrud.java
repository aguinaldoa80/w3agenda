package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Rede;
import br.com.linkcom.sined.geral.bean.Redeservidorinterface;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.bean.Servidorinterface;
import br.com.linkcom.sined.geral.service.RedeService;
import br.com.linkcom.sined.geral.service.ServidorinterfaceService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.RedeFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Rede", 
		authorizationModule=CrudAuthorizationModule.class
)
public class RedeCrud extends CrudControllerSined<RedeFiltro, Rede, Rede>{
	
	private ServidorinterfaceService servidorinterfaceService;
	private RedeService redeService;
	
	public void setServidorinterfaceService(ServidorinterfaceService servidorinterfaceService) {
		this.servidorinterfaceService = servidorinterfaceService;
	}
	public void setRedeService(RedeService redeService) {
		this.redeService = redeService;
	}
			
	@Override
	protected void salvar(WebRequestContext request, Rede bean)throws Exception {
		try{
			String enderecoip = redeService.ipValido(bean.getEnderecoip(), bean.getRedemascara());
			bean.setEnderecoip(enderecoip);
			
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_REDE_ENDERECOIP")){
				throw new SinedException("Rede j� cadastrada no sistema.");
			}
		}		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Rede bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Rede n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
	
	@Override
	protected void entrada(WebRequestContext request, Rede form) throws Exception {
		Map<Integer, List<Servidorinterface>> listaServidorInterface = new HashMap<Integer, List<Servidorinterface>>();
		if(form != null && form.getListaServidorinterface() != null && !form.getListaServidorinterface().isEmpty()){
			int i = 0;			
			Set<Redeservidorinterface> lista = form.getListaServidorinterface();
			for (Redeservidorinterface redeservidorinterface : lista) {									
				if (redeservidorinterface.getServidorinterface() != null){
					Servidor servidor = redeservidorinterface.getServidorinterface().getServidor();
					listaServidorInterface.put(i, servidorinterfaceService.findByServidor(servidor));
				}				
				i++;
			}
		}
		request.setAttribute("listaServidorInterface", listaServidorInterface);
	}
		
	/**
	 * Retorna lista de servidorinterface de acordo com o servidor
	 * @param request
	 * @param servidor
	 * @author Thiago Gon�alves
	 */
	public void makeAjaxServidorinterface(WebRequestContext request, Servidor servidor){
		List<Servidorinterface> listServidorinterface = null;
		String par = request.getParameter("servidor");
		try {
			Integer cdservidor = Integer.parseInt(par.substring(par.lastIndexOf("=")+1, par.lastIndexOf("]")));
			servidor.setCdservidor(cdservidor);
			listServidorinterface = servidorinterfaceService.findByServidor(servidor);
		} catch (Exception e) {
			listServidorinterface = new ArrayList<Servidorinterface>();
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listServidorinterface, "listaServicoservidor", ""));
	}
		
}
