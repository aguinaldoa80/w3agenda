package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Radiushistorico;
import br.com.linkcom.sined.geral.service.RadiushistoricoService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
		path="/briefcase/crud/Radiushistorico", 
		authorizationModule=CrudAuthorizationModule.class
)
public class RadiushistoricoCrud extends CrudControllerSined<FiltroListagemSined, Radiushistorico, Radiushistorico>{
	
	private RadiushistoricoService radiushistoricoService;
	public void setRadiushistoricoService(RadiushistoricoService radiushistoricoService) {
		this.radiushistoricoService = radiushistoricoService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Radiushistorico bean)throws Exception {
		throw new SinedException("N�o � poss�vel salvar o registro.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Radiushistorico bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir o registro.");
	}	
	
	@Override
	protected Radiushistorico criar(WebRequestContext request, Radiushistorico form) throws Exception {
		throw new SinedException("N�o � poss�vel criar registro.");
	}
	
	/**
	 * M�todo para concluir as sess�es abertas
	 * @param request
	 * @param form
	 * @return
	 * @author Thiago Gon�alves
	 */
	public ModelAndView concluirtempo(WebRequestContext request, Radiushistorico form){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		radiushistoricoService.confirmartempo(itens);
		request.addMessage("Tempo conclu�do para os registros marcados.");
		return sendRedirectToAction("listagem");
		
	}
		
}
