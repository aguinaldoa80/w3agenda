package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Ambienterede;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
		path="/briefcase/crud/Ambienterede", 
		authorizationModule=CrudAuthorizationModule.class
)
public class AmbienteredeCrud extends CrudControllerSined<FiltroListagemSined, Ambienterede, Ambienterede>{
	
	@Override
	protected void salvar(WebRequestContext request, Ambienterede bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_AMBIENTEREDE_NOME")){
				throw new SinedException("Ambiente de rede j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Ambienterede bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Ambiente de rede n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
		
}
