package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.geral.bean.Restricaocontratomaterial;
import br.com.linkcom.sined.geral.bean.Restricaodocumento;
import br.com.linkcom.sined.geral.bean.enumeration.Monitoradevedoracao;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentonotacontrato;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.RestricaoService;
import br.com.linkcom.sined.geral.service.RestricaocontratomaterialService;
import br.com.linkcom.sined.geral.service.RestricaodocumentoService;
import br.com.linkcom.sined.geral.service.VwdocumentonotacontratoService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.RestricaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/briefcase/crud/Restricao", authorizationModule=CrudAuthorizationModule.class)
public class RestricaoCrud extends CrudControllerSined<RestricaoFiltro , Restricao , Restricao>{

	private ContratoService contratoService;
	private RestricaoService restricaoService;
	private RestricaocontratomaterialService restricaocontratomaterialService;
	private ContratomaterialService contratomaterialService;
	private VwdocumentonotacontratoService vwdocumentonotacontratoService;
	private RestricaodocumentoService restricaodocumentoService;
	
	public void setRestricaodocumentoService(RestricaodocumentoService restricaodocumentoService) {this.restricaodocumentoService = restricaodocumentoService;}
	public void setVwdocumentonotacontratoService(VwdocumentonotacontratoService vwdocumentonotacontratoService) {this.vwdocumentonotacontratoService = vwdocumentonotacontratoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setRestricaoService(RestricaoService restricaoService) {this.restricaoService = restricaoService;}
	public void setRestricaocontratomaterialService(RestricaocontratomaterialService restricaocontratomaterialService) {this.restricaocontratomaterialService = restricaocontratomaterialService;}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {this.contratomaterialService = contratomaterialService;}
	
	
	/**
	 * Respons�vel por exibir os servi�os, no popup, para bloqueio parcial
	 * @param request
	 * @return
	 * @author Taidson
	 * @since 30/11/2010
	 */
	public ModelAndView bloqueioParcialServicos(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Contrato contrato;
    	contrato = new Contrato();
    	contrato.setCdcontrato(Integer.valueOf(whereIn));
    	contrato = contratoService.servicosByContrato(contrato);
    		
    	List<Material> listaServicos = new ArrayList<Material>();
    	
    	Restricao restricao = restricaoService.restricaoDesbloquear(contrato);
    	
    	for (Contratomaterial item : contrato.getListaContratomaterial()) {
    		Restricaocontratomaterial bean = restricaocontratomaterialService.loadServicoBloqueadoDesbloqueado(item, restricao);
    		item.getServico().setCdcontratomaterial(item.getCdcontratomaterial());
    		item.getServico().setContrato(contrato);
    		if(bean != null && bean.getCdrestricaocontratomaterial() != null && bean.getDtdesbloqueio() == null && restricao != null){
    			item.getServico().setCdrestricaocontratomaterial(bean.getCdrestricaocontratomaterial());
    			item.getServico().setBloqueado(true);
    			listaServicos.add(item.getServico());
    		}else{
    			item.getServico().setBloqueado(false);
				listaServicos.add(item.getServico());
    		}
    	}
    	
		request.setAttribute("listaServicos", listaServicos);
	
		request.setAttribute("titulo", "BLOQUEIO PARCIAL DE SERVI�OS");
		request.setAttribute("saveBloqueioParcialServicos", "saveBloqueioParcialServicos");
	
		return new ModelAndView("direct:/crud/popup/bloqueioParcialServico");
		
	}
	
	/**
	 * Salva o(s) servi�o(s) selecionado(s) no popup de bloqueio parcial
	 * @param request
	 * @return
	 * @author Taidson
	 * @since 30/11/2010
	 */
	public ModelAndView saveBloqueioParcialServicos(WebRequestContext request){
		String whereIn = request.getParameter("itensSelecionados");
		Restricao restricao = null;
		
		Contrato contrato = new Contrato();
		for (String id : whereIn.split(",")){
			String idContratoMaterial = StringUtils.substringBefore(id, "=");
			String idRestricaoContratoMaterial = StringUtils.substringAfter(id, "=");
			idRestricaoContratoMaterial = StringUtils.substringBefore(idRestricaoContratoMaterial, "-");
			Contratomaterial contratomaterial = new Contratomaterial();
			contratomaterial.setCdcontratomaterial(Integer.valueOf(idContratoMaterial));
			contratomaterial.setCdcontratomaterial(Integer.valueOf(StringUtils.substringBefore(contratomaterial.getCdcontratomaterial().toString(), "-")));
			contratomaterial = contratomaterialService.loadContratomaterial(contratomaterial);
    		contrato.setCdcontrato(Integer.valueOf(StringUtils.substringAfter(id, "-")));
    		contrato = contratoService.servicosByContrato(contrato);
    		
    		Restricaocontratomaterial restricaocontratomaterial1;
    		int validador = 0;
    		
			if(!idRestricaoContratoMaterial.equals("")){
				if(contrato.getListaContratomaterial() != null && contrato.getListaContratomaterial().size() > 0){
					for (@SuppressWarnings("unused")
							Contratomaterial item : contrato.getListaContratomaterial()) {
						restricaocontratomaterial1 = new Restricaocontratomaterial();
						restricaocontratomaterial1 = restricaocontratomaterialService.loadServicoBloqueadoDesbloqueado(Integer.valueOf(idContratoMaterial), Integer.valueOf(idRestricaoContratoMaterial));
						if(restricaocontratomaterial1 != null && restricaocontratomaterial1.getCdrestricaocontratomaterial() != null){
							restricaocontratomaterial1.setDtdesbloqueio(SinedDateUtils.currentDate());
							validador ++;
							restricao = restricaoService.loadForEntrada(restricaocontratomaterial1.getRestricao());
						}
					}
				}
			}
    	}
		
    	try{
    		
    		Restricaocontratomaterial restricaocontratomaterial;
    		restricao = restricaoService.restricaoDesbloquear(contrato);
    		if(restricao == null) restricao = new Restricao();
    		Contratomaterial contratomaterial;
    		contratomaterial = new Contratomaterial();
    		int validador = 0;
    		
    		restricao.setCdsituacaorestricao(Monitoradevedoracao.BLOQUEAR_PARCIAL.getCdmonitoraclienteacao());
    		restricao.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
    		restricao.setDtaltera(new Timestamp(System.currentTimeMillis()));
    		restricao.setDtrestricao(SinedDateUtils.currentDate());
    		restricao.setMotivorestricao("Bloqueio de servi�os devido a atraso de pagamento.");
    		contrato = contratoService.clienteByContrato(contrato.getCdcontrato());
			restricao.setContrato(contrato);
			restricao.setPessoa(contrato.getCliente());
			restricaoService.saveOrUpdate(restricao);
			
			List<Vwdocumentonotacontrato> listaItens = vwdocumentonotacontratoService.listaDocumentonotacontrato(contrato);
			if(listaItens != null && listaItens.size() > 0){
				Restricaodocumento restricaodocumento;
				for (Vwdocumentonotacontrato vnc : listaItens) {
					restricaodocumento = new Restricaodocumento();
					restricaodocumento.setRestricao(restricao);
					restricaodocumento.setDocumento(new Documento(vnc.getCddocumento()));
					restricaodocumentoService.saveOrUpdate(restricaodocumento);
				}
			}
    		
    		for (String id : whereIn.split(",")){
    			String idContratoMaterial = StringUtils.substringBefore(id, "=");
        		String idRestricaoContratoMaterial = StringUtils.substringAfter(id, "=");
        		idRestricaoContratoMaterial = StringUtils.substringBefore(idRestricaoContratoMaterial, "-");
        		contratomaterial.setCdcontratomaterial(Integer.valueOf(idContratoMaterial));
        		contratomaterial.setCdcontratomaterial(Integer.valueOf(StringUtils.substringBefore(contratomaterial.getCdcontratomaterial().toString(), "-")));
    			contratomaterial = contratomaterialService.loadContratomaterial(contratomaterial);
    			
    			if(!idRestricaoContratoMaterial.equals("")){
	    			restricaocontratomaterial = new Restricaocontratomaterial();
	    			restricaocontratomaterial = restricaocontratomaterialService.loadServicoBloqueadoDesbloqueado(Integer.valueOf(idContratoMaterial), Integer.valueOf(idRestricaoContratoMaterial));
	    			if(restricaocontratomaterial == null) restricaocontratomaterial = new Restricaocontratomaterial();
	    			restricaocontratomaterial.setCdrestricaocontratomaterial(null);
	    			restricaocontratomaterial.setDtdesbloqueio(null);
	    			restricaocontratomaterial.setContratomaterial(contratomaterial);
	    			restricaocontratomaterial.setDtbloqueio(SinedDateUtils.currentDate());
	    			restricaocontratomaterial.setRestricao(restricao);
	    			restricaocontratomaterialService.saveOrUpdate(restricaocontratomaterial);
	    			validador ++;
	    			restricao = restricaoService.loadForEntrada(restricaocontratomaterial.getRestricao());
    			}else{
    				restricaocontratomaterial = new Restricaocontratomaterial();
	    			restricaocontratomaterial.setContratomaterial(contratomaterial);
	    			restricaocontratomaterial.setDtbloqueio(SinedDateUtils.currentDate());
	    			restricaocontratomaterial.setRestricao(restricao);
	    			restricaocontratomaterial.setDtbloqueio(SinedDateUtils.currentDate());
	    			restricaocontratomaterialService.saveOrUpdate(restricaocontratomaterial);
    			}
    		}	
    		List<Restricaocontratomaterial> lista = restricaocontratomaterialService.restricoesContratoMaterialDesbloquear(restricao);
    		Contrato contratoServicos = contratoService.servicosByContrato(contrato);
    		
    		if(lista.size() == contratoServicos.getListaContratomaterial().size()){
    			restricao.setCdsituacaorestricao(Monitoradevedoracao.BLOQUEAR_TOTAL.getCdmonitoraclienteacao());
    			restricaoService.saveOrUpdate(restricao);
    		}

			SinedUtil.fechaPopUp(request);
			request.addMessage("Bloqueio parcial efetuado com sucesso.");
			return null;
    	} catch (Exception e) {
    		e.printStackTrace();
    		SinedUtil.fechaPopUp(request);
    		request.addError("N�o foi poss�vel efetuar o bloqueio parcial do(s) servi�o(s).");
    		return null;
    	}finally {
			SinedUtil.fechaPopUp(request);
		}
	}
	
	
	/**
	 * Realiza o bloqueio total, ou seja, bloqueio todos os servi�os dos contrato selelcionados.
	 * Se houve restri��o pendente de desbloqueio para o mesmo contrato, esta ser� desbloqueada 
	 * antes de efetuar novo bloqueio.
	 * @param request
	 * @return
	 */
	public ModelAndView bloqueiaServicos(WebRequestContext request){
    	String whereIn = request.getParameter("itensSelecionados");
    	Contrato contrato;
		Restricao restricao = null;
		for (String id : whereIn.split(",")){
			contrato = new Contrato();
			contrato.setCdcontrato(Integer.valueOf(id));
			contrato = contratoService.servicosByContrato(contrato);
			
			restricao = restricaoService.restricaoDesbloquear(contrato);
			if(restricao != null && restricao.getCdrestricao() != null){
				restricao.setCdsituacaorestricao(Monitoradevedoracao.DESBLOQUEAR.getCdmonitoraclienteacao());
				restricao.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				restricao.setDtaltera(new Timestamp(System.currentTimeMillis()));
				
				List<Restricaocontratomaterial> listaDesbloquear = restricaocontratomaterialService.restricoesContratoMaterialDesbloquear(restricao);
				
				if(listaDesbloquear != null && listaDesbloquear.size() > 0){
					for (Restricaocontratomaterial item : listaDesbloquear) {
						item.setDtdesbloqueio(SinedDateUtils.currentDate());
						restricaocontratomaterialService.saveOrUpdate(item);
					}
				}
			}
		}
    	try{
    		for (String id2 : whereIn.split(",")){
    			contrato = new Contrato();
    			contrato.setCdcontrato(Integer.valueOf(id2));
    			contrato = contratoService.servicosByContrato(contrato);
    			restricao = new Restricao();
    			restricao.setDtrestricao(SinedDateUtils.currentDate());
    			restricao.setDtaltera(new Timestamp(System.currentTimeMillis()));
    			restricao.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
    			restricao.setPessoa(contrato.getCliente());
    			restricao.setContrato(contrato);
    			restricao.setCdsituacaorestricao(Monitoradevedoracao.BLOQUEAR_TOTAL.getCdmonitoraclienteacao());
    			restricao.setMotivorestricao("Bloqueio de servi�os devido a atraso de pagamento.");
    			restricaoService.saveOrUpdate(restricao);
    			
    			List<Vwdocumentonotacontrato> listaItens = vwdocumentonotacontratoService.listaDocumentonotacontrato(contrato);
    			if(listaItens != null && listaItens.size() > 0){
    				Restricaodocumento restricaodocumento;
    				for (Vwdocumentonotacontrato vnc : listaItens) {
    					restricaodocumento = new Restricaodocumento();
    					restricaodocumento.setRestricao(restricao);
    					restricaodocumento.setDocumento(new Documento(vnc.getCddocumento()));
    					restricaodocumentoService.saveOrUpdate(restricaodocumento);
    				}
    			}
    			
    			Restricaocontratomaterial restricaocontratomaterial2;
    			for (Contratomaterial item : contrato.getListaContratomaterial()) {
    				restricaocontratomaterial2 = new Restricaocontratomaterial();
    				restricaocontratomaterial2.setRestricao(restricao);
    				restricaocontratomaterial2.setContratomaterial(item);
    				restricaocontratomaterial2.setDtbloqueio(restricao.getDtrestricao());
    				restricaocontratomaterialService.saveOrUpdate(restricaocontratomaterial2);
    			}
    		}
    		request.addMessage("Bloqueio do(s) servi�o(s) efetuado com sucesso.");
    		return new ModelAndView("redirect:/briefcase/crud/Vwmonitoradevedor");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("N�o foi poss�vel efetuar o bloqueio do(s) servi�o(s).");
			return new ModelAndView("redirect:/briefcase/crud/Vwmonitoradevedor");
		}
    	
	}
   
	/**
	 * Abre popup para usu�rio digitar motivo libera��o quando do desbloqueio de um contrato
	 * @param request
	 * @return
	 * @author Taidson
	 * @since 30/11/2010
	 */   
	public ModelAndView desbloqueiaServicos(WebRequestContext request){
	 	String itensSelecionados = SinedUtil.getItensSelecionados(request);
		if(itensSelecionados == null || itensSelecionados.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		request.setAttribute("selectedItens", itensSelecionados);
	
		request.setAttribute("titulo", "DESBLOQUEAR SERVI�OS");
		request.setAttribute("unlocked", "unlocked");
	
		return new ModelAndView("direct:/crud/popup/desbloqueiaServico").addObject("restricao", new Restricao());
	}
	
	/**
	 * Desbloqueia todos os servi�os do(s) contrato(s) selecionado(s).
	 * @param request
	 * @return
	 * @author Taidson
	 * @since 30/11/2010
	 */   
	public ModelAndView unlocked(WebRequestContext request, Restricao bean){
    	try{
    		String whereIn = request.getParameter("selectedItens");
    		Contrato contrato;
    		Restricao restricao = new Restricao();
    		for (String id : whereIn.split(",")){
    			contrato = new Contrato();
    			contrato.setCdcontrato(Integer.valueOf(id));
    			contrato = contratoService.servicosByContrato(contrato);
    			
    			restricao = restricaoService.restricaoDesbloquear(contrato);
    			restricao.setCdsituacaorestricao(Monitoradevedoracao.DESBLOQUEAR.getCdmonitoraclienteacao());
    			restricao.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
    			restricao.setDtaltera(new Timestamp(System.currentTimeMillis()));
    			restricao.setMotivoliberacao(bean.getMotivoliberacao());
    			restricao.setDtliberacao(SinedDateUtils.currentDate());
    			restricaoService.saveOrUpdate(restricao);
    			
    			restricaodocumentoService.deletaDocumentos(restricao);
    			
    			List<Restricaocontratomaterial> listaDesbloquear = restricaocontratomaterialService.restricoesContratoMaterialDesbloquear(restricao);
    			
    			if(listaDesbloquear != null && listaDesbloquear.size() > 0){
    				for (Restricaocontratomaterial item : listaDesbloquear) {
						item.setDtdesbloqueio(SinedDateUtils.currentDate());
						restricaocontratomaterialService.saveOrUpdate(item);
					}
    			}
    		}
    		SinedUtil.fechaPopUp(request);
    		request.addMessage("Desbloqueio do(s) servi�o(s) efetuado com sucesso.");
    		return null;	
       	} catch (Exception e) {
    		e.printStackTrace();
    		SinedUtil.fechaPopUp(request);
    		request.addError("N�o foi poss�vel efetuar o desbloqueio do(s) servi�o(s).");
    		return null;
    	} finally {
			SinedUtil.fechaPopUp(request);
		}
	}
}
