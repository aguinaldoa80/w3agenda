package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Dominiozonadkim;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominiozonadkimFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Dominiozonadkim", 
		authorizationModule=CrudAuthorizationModule.class
)
public class DominiozonadkimCrud extends CrudControllerSined<DominiozonadkimFiltro, Dominiozonadkim, Dominiozonadkim>{
	
	@Override
	protected void salvar(WebRequestContext request, Dominiozonadkim bean)throws Exception {
		try {
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_DOMINIOZONADKIM_NOME")){
				throw new SinedException("Nome de Zona Dkim j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Dominiozonadkim bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Zona Dkim n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
		
}
