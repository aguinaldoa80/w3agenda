package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Bandaredecomercial;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.BandaredecomercialFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Bandaredecomercial", 
		authorizationModule=CrudAuthorizationModule.class
)
public class BandaredecomercialCrud extends CrudControllerSined<BandaredecomercialFiltro, Bandaredecomercial, Bandaredecomercial>{
	
	@Override
	protected void salvar(WebRequestContext request, Bandaredecomercial bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_BANDAREDECOMERCIAL_NOME")){
				throw new SinedException("Banda comercial j� cadastrada no sistema.");
			}
		}		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Bandaredecomercial bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Banda comercial n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
	
	@Override
	protected void validateBean(Bandaredecomercial bean, BindException errors) {
		
	}
		
}
