package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Plataforma;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
		path="/briefcase/crud/Plataforma", 
		authorizationModule=CrudAuthorizationModule.class
)
public class PlataformaCrud extends CrudControllerSined<FiltroListagemSined, Plataforma, Plataforma>{
	
	@Override
	protected void salvar(WebRequestContext request, Plataforma bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_PLATAFORMA_NOME")){
				throw new SinedException("Plataforma j� cadastrada no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Plataforma bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Plataforma  n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
		
}
