package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Bandarede;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.BandaredeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Bandarede", 
		authorizationModule=CrudAuthorizationModule.class
)
public class BandaredeCrud extends CrudControllerSined<BandaredeFiltro, Bandarede, Bandarede>{
	
	@Override
	protected void salvar(WebRequestContext request, Bandarede bean)throws Exception {
		super.salvar(request, bean);		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Bandarede bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Banda n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
		
}
