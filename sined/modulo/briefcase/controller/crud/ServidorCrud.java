package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.bean.Servidorfirewall;
import br.com.linkcom.sined.geral.service.ServidorService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServidorFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Servidor", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ServidorCrud extends CrudControllerSined<ServidorFiltro, Servidor, Servidor>{
	
	private ServidorService servidorService;
	
	public void setServidorService(ServidorService servidorService) {
		this.servidorService = servidorService;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void salvar(WebRequestContext request, Servidor bean)throws Exception {
		Object attribute = request.getSession().getAttribute("listaServidorFirewall");
		if (attribute != null) 
			bean.setListaServidorfirewall((List<Servidorfirewall>)attribute);
		
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_SERVIDOR_NOME")){
				throw new SinedException("Servidor j� cadastrado no sistema.");
			}
		}		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Servidor bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Servidor n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Servidor form)throws Exception {
		request.setAttribute("modulo", "briefcase");
		
		if(request.getBindException().hasErrors()){
			Object attribute = request.getSession().getAttribute("listaServidorFirewall");
			if (attribute != null) {
				form.setListaServidorfirewall((List<Servidorfirewall>)attribute);		
			}
		}
		request.getSession().setAttribute("listaServidorFirewall", form.getListaServidorfirewall());
	}
	
	@Override
	protected void validateBean(Servidor bean, BindException errors) {
		if (bean.getBriefcase()){
			Servidor briefcase = servidorService.findBriefcase();
			if (briefcase != null && !briefcase.getCdservidor().equals(bean.getCdservidor()))
				errors.reject("001", "Somente um registro pode ser marcado como BriefCase.");
		}
	}
		
	/**
	 * M�todo que atualiza uma lista de servidores de acordo com o servi�o
	 * 
	 * @param request
	 * @param servidor
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView atulizaServicos(WebRequestContext request, Servidor servidor){
		String whereIn = request.getParameter("selectedItens");
		String servico = request.getParameter("servico");
		String acaoServico = request.getParameter("acaoServico");
		if(whereIn != null && !whereIn.equals("") && servico != null && !servico.equals("") && acaoServico != null && !acaoServico.equals(""))
			servidorService.atualizaServicos(whereIn.split(","), servico, acaoServico);
		
		request.addMessage("Servi�o(s) "+(Integer.valueOf(servico).equals(Servicoservidortipo.SERVICO_SERVIDOR) ? "reiniciado(os)" : "atualizado(s)")+" com sucesso.", MessageType.INFO);
		return continueOnAction("listagem", (ServidorFiltro)request.getSession().getAttribute(this.getClass().getSimpleName()+"CONTROLLER"+ServidorFiltro.class.getName()));
	}
	
}
