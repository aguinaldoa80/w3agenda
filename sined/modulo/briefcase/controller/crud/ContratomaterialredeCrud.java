package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Ambienterede;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialrede;
import br.com.linkcom.sined.geral.bean.Rede;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.bean.Servidorinterface;
import br.com.linkcom.sined.geral.service.AmbienteredeService;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.ContratomaterialredeService;
import br.com.linkcom.sined.geral.service.RedeService;
import br.com.linkcom.sined.geral.service.ServicoservidortipoService;
import br.com.linkcom.sined.geral.service.ServidorService;
import br.com.linkcom.sined.geral.service.ServidorinterfaceService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ContratomaterialredeFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Contratomaterialrede", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ContratomaterialredeCrud extends CrudControllerSined<ContratomaterialredeFiltro, Contratomaterialrede, Contratomaterialrede>{

	private ContratomaterialService contratomaterialService;
	private ContratomaterialredeService contratomaterialredeService;
	private ServicoservidortipoService servicoservidortipoService;
	private AmbienteredeService ambienteredeService;
	private ServidorService servidorService;
	private ServidorinterfaceService servidorinterfaceService;
	private RedeService redeService;
	
	public void setContratomaterialredeService(
			ContratomaterialredeService contratomaterialredeService) {
		this.contratomaterialredeService = contratomaterialredeService;
	}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {
		this.contratomaterialService = contratomaterialService;
	}
	public void setServicoservidortipoService(
			ServicoservidortipoService servicoservidortipoService) {
		this.servicoservidortipoService = servicoservidortipoService;
	}
	public void setAmbienteredeService(AmbienteredeService ambienteredeService) {
		this.ambienteredeService = ambienteredeService;
	}
	public void setServidorService(ServidorService servidorService) {
		this.servidorService = servidorService;
	}
	public void setServidorinterfaceService(
			ServidorinterfaceService servidorinterfaceService) {
		this.servidorinterfaceService = servidorinterfaceService;
	}
	public void setRedeService(RedeService redeService) {
		this.redeService = redeService;
	}

	@Override
	protected Contratomaterialrede carregar(WebRequestContext request, Contratomaterialrede bean) throws Exception {
		Contratomaterialrede contratomaterialrede = super.carregar(request, bean);
		if (contratomaterialrede.getContratomaterial() != null)
			contratomaterialrede.setCliente(contratomaterialrede.getContratomaterial().getContrato().getCliente());
		if (contratomaterialrede.getServidorinterfacepreferencial() != null){
			Servidor servidor = contratomaterialrede.getServidorinterfacepreferencial().getServidor();
			contratomaterialrede.setServidor(servidor);
			contratomaterialrede.setAmbienterede(servidor.getAmbienterede());
		}
		if (contratomaterialrede.getRedeip() != null)
			contratomaterialrede.setRede(contratomaterialrede.getRedeip().getRede());
		return contratomaterialrede;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Contratomaterialrede bean)throws Exception {
		try{
			if(bean.getMac() != null){
				bean.setMac(bean.getMac().toUpperCase());
			}
			super.salvar(request, bean);
		} catch(DataIntegrityViolationException e) {
			if(DatabaseError.isKeyPresent(e, "uk_contratomaterialrede")){
				throw new SinedException("Usu�rio j� cadastrado no sistema.");
			}
			throw e;
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Contratomaterialrede bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Servi�o de rede n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
	
	@Override
	protected void validateBean(Contratomaterialrede bean, BindException errors) {		
		if (!bean.getUsuario().matches("[A-Za-z0-9\\.\\-_]+")){
			errors.reject("001", "Caracteres inv�lidos em usu�rio.");		
		}
		
		if (!bean.getMac().matches("[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}")){
			errors.reject("001", "Formato do Mac inv�lido.");		
		}		
		
		int numReg = contratomaterialredeService.countContratomaterial(bean.getCdcontratomaterialrede(), bean.getContratomaterial());
		Double qtde = contratomaterialService.load(bean.getContratomaterial(), "contratomaterial.qtde").getQtde();
		
		if(qtde <= numReg){
			errors.reject("002","N�o se pode criar mais registros que a quantidade do servi�o no contrato.");
		}
	}
	
	@Action("filtrarcontrato")
	public ModelAndView filtrarContrato(WebRequestContext request, Contratomaterial contratomaterial) throws CrudException {
		Cliente cliente = contratomaterialService.findCliente(contratomaterial);
		ContratomaterialredeFiltro filtro = new ContratomaterialredeFiltro();
		filtro.setCliente(cliente);
		filtro.setContratomaterial(contratomaterial);
		ListagemResult<Contratomaterialrede> lista = getLista(request, filtro);
		if (lista != null && !lista.list().isEmpty() && lista.list().size() >= contratomaterial.getQtde())
			return doListagem(request, filtro);
		else {
			Contratomaterialrede form = new Contratomaterialrede();
			form.setCliente(cliente);
			form.setContratomaterial(contratomaterial);
			
			((DefaultWebRequestContext)request).setLastAction("entrada");
			return doEntrada(request, form);			
		}
	}
	
	@Override
	protected ListagemResult<Contratomaterialrede> getLista(WebRequestContext request, ContratomaterialredeFiltro filtro) {
		
		List<Servicoservidortipo> listaServicoservidortipo = new ListSet<Servicoservidortipo>(Servicoservidortipo.class);
		listaServicoservidortipo = servicoservidortipoService.findByServicorede();
		
		List<Ambienterede> listaAmbienterede = new ListSet<Ambienterede>(Ambienterede.class);
		listaAmbienterede = ambienteredeService.findAll();
		
		request.setAttribute("listaServicoservidortipo", listaServicoservidortipo);
		request.setAttribute("listaAmbienterede", listaAmbienterede);
		
		return super.getLista(request, filtro);
	}
	
	/**
	 * Popula a combo servidor. Apresenta resultado diferente conforme sele��o ou n�o
	 * de servicoservidortipo e ambiente.
	 * @param request
	 * @author Taidson
	 * @since 15/07/2010
	 */
	public void ajaxComboServidor(WebRequestContext request){
		List<Servidor> listaServidor = new ListSet<Servidor>(Servidor.class);
		
		String tipoString = request.getParameter("cdtipo");
		String ambienteredeString = request.getParameter("cdambienterede");
		
		if("<null>".equals(tipoString) && "<null>".equals(ambienteredeString)){
			listaServidor = servidorService.findAll();
		}else{
			Servicoservidortipo tipo = null;
			Ambienterede ambienterede = null;
			
			if(!"<null>".equals(tipoString)){
				tipo = new Servicoservidortipo();
				tipo.setCdservicoservidortipo(Integer.valueOf(StringUtils.substringBetween(tipoString, "=", "]")));
			}
			if(!"<null>".equals(ambienteredeString)){
				ambienterede = new Ambienterede();
				ambienterede.setCdambienterede(Integer.valueOf(StringUtils.substringBetween(ambienteredeString, "=", "]")));
			}
			listaServidor = servidorService.findByServicoAmbiente(tipo, ambienterede);
		}
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaServidor, "listaServidor", ""));
	}
	
	/**
	 * Popula a combo interface preferencial. Apresenta resultado diferente conforme sele��o ou n�o
	 * de servidor.
	 * @param request
	 * @author Taidson
	 * @since 15/07/2010
	 */
	public void ajaxComboInterface(WebRequestContext request){
		List<Servidorinterface> listaInterface = new ListSet<Servidorinterface>(Servidorinterface.class);
		
		String servidorString = request.getParameter("cdservidor");
		
		if("<null>".equals(servidorString)){
			listaInterface = servidorinterfaceService.findAll();
		}else{
			Servidor servidor = new Servidor();
			servidor.setCdservidor(Integer.valueOf(StringUtils.substringBetween(servidorString, "=", "]")));
			listaInterface = servidorinterfaceService.findByServidor(servidor);
		}
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaInterface, "listaInterface", ""));
	}
	
	
	/**
	 * Popula a combo rede. Apresenta resultado diferente conforme sele��o ou n�o
	 * de contrato material e interface.
	 * @param request
	 * @author Taidson
	 * @since 15/07/2010
	 */
	public void ajaxComboRede(WebRequestContext request){
		List<Rede> listaRede = new ListSet<Rede>(Rede.class);
		
		String interfaceString = request.getParameter("cdinterface");
		String contratomaterialString = request.getParameter("cdcontratomaterial");
		
		if("<null>".equals(interfaceString) && "<null>".equals(contratomaterialString)){
			listaRede = redeService.findAll();
		}else{
			Servidorinterface interfacePreferencial = null;
			Contratomaterial contratomaterial = null;
			
			if(!"<null>".equals(interfaceString)){
				interfacePreferencial = new Servidorinterface();
				interfacePreferencial.setCdservidorinterface(Integer.valueOf(StringUtils.substringBetween(interfaceString, "=", "]")));
			}
			if(!"<null>".equals(contratomaterialString)){
				contratomaterial = new Contratomaterial();
				contratomaterial.setCdcontratomaterial(Integer.valueOf(StringUtils.substringBetween(contratomaterialString, "=", "]")));
			}
			listaRede = redeService.findByInterfaceAndContratoMaterial(interfacePreferencial, contratomaterial);
		}
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaRede, "listaRede", ""));
	}
	
}
