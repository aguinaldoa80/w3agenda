package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Dominiospf;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominiospfFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Dominiospf", 
		authorizationModule=CrudAuthorizationModule.class
)
public class DominiospfCrud extends CrudControllerSined<DominiospfFiltro, Dominiospf, Dominiospf>{
	
	@Override
	protected Dominiospf carregar(WebRequestContext request, Dominiospf bean) throws Exception {
		Dominiospf dominiospf = super.carregar(request, bean);
		if (dominiospf.getContratomaterial() != null)
			dominiospf.setCliente(dominiospf.getContratomaterial().getContrato().getCliente());
		return dominiospf;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Dominiospf bean)throws Exception {
		super.salvar(request, bean);				
	}
	
	@Override
	protected void excluir(WebRequestContext request, Dominiospf bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Dom�nio Spf n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
		
}
