package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Servicodominiorede;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServicodominioredeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Servicodominiorede", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ServicodominioredeCrud extends CrudControllerSined<ServicodominioredeFiltro, Servicodominiorede, Servicodominiorede>{
	
	@Override
	protected void salvar(WebRequestContext request, Servicodominiorede bean)throws Exception {
		Servicoservidortipo servicoservidortipo = new Servicoservidortipo();
		servicoservidortipo.setCdservicoservidortipo(Servicoservidortipo.DOMINIO);
		bean.setServicoservidortipo(servicoservidortipo);
		super.salvar(request, bean);		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Servicodominiorede bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Dom�nio de rede n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
		
}
