package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Servidorinterfaceip;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServidorinterfaceipFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Servidorinterfaceip", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ServidorinterfaceipCrud extends CrudControllerSined<ServidorinterfaceipFiltro, Servidorinterfaceip, Servidorinterfaceip>{
	
	@Override
	protected void salvar(WebRequestContext request, Servidorinterfaceip bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_SERVIDORINTERFACEIP_IP")){
				throw new SinedException("IP j� cadastrado no sistema para esta interface.");
			}
		}		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Servidorinterfaceip bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Interface IP n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
		
}
