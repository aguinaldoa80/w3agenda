package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicoftp;
import br.com.linkcom.sined.geral.bean.Servicoservidor;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.ServicoftpService;
import br.com.linkcom.sined.geral.service.ServicoservidorService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServicoftpFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Servicoftp", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ServicoftpCrud extends CrudControllerSined<ServicoftpFiltro, Servicoftp, Servicoftp>{
	
	private ServicoservidorService servicoservidorService;
	private ContratomaterialService contratomaterialService;
	private ServicoftpService servicoftpService;
	
	public void setServicoftpService(ServicoftpService servicoftpService) {
		this.servicoftpService = servicoftpService;
	}
	public void setServicoservidorService(ServicoservidorService servicoservidorService) {
		this.servicoservidorService = servicoservidorService;
	}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {
		this.contratomaterialService = contratomaterialService;
	}

	@Override
	protected Servicoftp carregar(WebRequestContext request, Servicoftp bean)throws Exception {
		Servicoftp servicoftp = super.carregar(request, bean);
		if (servicoftp.getContratomaterial() != null)
			servicoftp.setCliente(servicoftp.getContratomaterial().getContrato().getCliente());
		if (servicoftp.getServicoservidor() != null)
			servicoftp.setServidor(servicoftp.getServicoservidor().getServidor());
		return servicoftp;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Servicoftp bean)throws Exception {
		super.salvar(request, bean);		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Servicoftp bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Servi�o de FTP n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	

	public void ajaxOnSelectServico(WebRequestContext request, Servicoftp servicoftp) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");

		if (servicoftp.getServicoservidor() == null || servicoftp.getServicoservidor().getCdservicoservidor() == null) {
			view.println("var sucesso = false;");
			return;
		}

		try {
			Servicoservidor servicoservidor = servicoftp.getServicoservidor();
			servicoservidor = servicoservidorService.loadForEntrada(servicoservidor);			
			String parametro = servicoservidor.getParametro();  
			String usuario = servicoftp.getUsuario();
			if (parametro == null || "".equals(parametro) ||
				usuario == null || "".equals(usuario)){
				view.println("var diretorio = '';");				
			} else {				
				parametro += "/" + usuario;
				parametro = parametro.replace("\\", "\\\\");
				view.println("var diretorio = '" + parametro + "';");				
			}
			view.println("var sucesso = true;");						
		} catch (Exception e) {
			view.println("var sucesso = false;");
			view.println("var msg = 'Ocorreu um erro ao recuperar os dados do diret�rio referente ao servi�o.';");
		}
	}	
	
	@Override
	protected void validateBean(Servicoftp bean, BindException errors) {
		String usuario = bean.getUsuario();
		if (!usuario.matches("[A-Za-z0-9\\.\\-_]+")){
			errors.reject("001", "Caracteres inv�lidos em usu�rio.");		
		}
		
		int numReg = servicoftpService.countContratomaterial(bean.getCdservicoftp(), bean.getContratomaterial());
		Double qtde = contratomaterialService.load(bean.getContratomaterial(), "contratomaterial.qtde").getQtde();
		
		if(qtde <= numReg){
			errors.reject("002","N�o se pode criar mais registros que a quantidade do servi�o no contrato.");
		}
	}
	
	@Action("filtrarcontrato")
	public ModelAndView filtrarContrato(WebRequestContext request, Contratomaterial contratomaterial) throws CrudException {
		Cliente cliente = contratomaterialService.findCliente(contratomaterial);
		ServicoftpFiltro filtro = new ServicoftpFiltro();
		filtro.setCliente(cliente);
		filtro.setContratomaterial(contratomaterial);
		ListagemResult<Servicoftp> lista = getLista(request, filtro);
		if (lista != null && !lista.list().isEmpty() && lista.list().size() >= contratomaterial.getQtde())
			return doListagem(request, filtro);
		else {
			Servicoftp servicoftp = new Servicoftp();
			servicoftp.setCliente(cliente);
			servicoftp.setContratomaterial(contratomaterial);
			
			((DefaultWebRequestContext)request).setLastAction("entrada");
			return doEntrada(request, servicoftp);			
		}
	}
}
