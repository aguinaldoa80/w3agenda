package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
		path="/briefcase/crud/Servicoservidortipo", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ServicoservidortipoCrud extends CrudControllerSined<FiltroListagemSined, Servicoservidortipo, Servicoservidortipo>{
	
	@Override
	protected void salvar(WebRequestContext request, Servicoservidortipo bean)throws Exception {
		super.salvar(request, bean);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Servicoservidortipo bean)throws Exception {
		throw new SinedException("Tipo de servi�o de servidor n�o pode ser exclu�do.");
	}	
	
	@Override
	protected Servicoservidortipo criar(WebRequestContext request, Servicoservidortipo form) throws Exception {
		throw new SinedException("Tipo de servi�o de servidor n�o pode ser criado.");
	}
		
}
