package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@DisplayName("E-mail de usu�rio")
public class DominioemailusuarioFiltro extends FiltroListagemSined {

	protected Cliente cliente;
	protected Contratomaterial contratomaterial;
	protected Dominio dominio;
	protected String usuario;
	
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	@DisplayName("Dom�nio")
	public Dominio getDominio() {
		return dominio;
	}
	
	@DisplayName("Nome")
	@MaxLength(50)
	public String getUsuario() {
		return usuario;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
