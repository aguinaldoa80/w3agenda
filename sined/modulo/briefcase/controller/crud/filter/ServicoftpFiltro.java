package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicoservidor;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ServicoftpFiltro extends FiltroListagemSined {

	protected Servidor servidor;
	protected Servicoservidor servicoservidor;
	protected String usuario;
	protected Cliente cliente;
	protected Contratomaterial contratomaterial;
	protected Boolean ativo = true;
		
	public Servidor getServidor() {
		return servidor;
	}
	
	@DisplayName("Servi�o de servidor")
	public Servicoservidor getServicoservidor() {
		return servicoservidor;
	}
	
	@DisplayName("Usu�rio")
	@MaxLength(50)
	public String getUsuario() {
		return usuario;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	public void setServicoservidor(Servicoservidor servicoservidor) {
		this.servicoservidor = servicoservidor;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}	
}
