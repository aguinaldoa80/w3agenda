package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DominioFiltro extends FiltroListagemSined {

	protected Dominio dominioprincipal;	
	protected Cliente cliente;
	protected Contratomaterial contratomaterial;
	protected String nome;
	
	@DisplayName("Dom�nio principal")
	public Dominio getDominioprincipal() {
		return dominioprincipal;
	}
		
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	@MaxLength(40)
	public String getNome() {
		return nome;
	}
	
	public void setDominioprincipal(Dominio dominioprincipal) {
		this.dominioprincipal = dominioprincipal;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
			
}
