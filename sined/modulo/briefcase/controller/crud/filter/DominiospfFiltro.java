package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DominiospfFiltro extends FiltroListagemSined {

	protected Dominio dominio;	
	protected Cliente cliente;
	protected Contratomaterial contratomaterial;	
	
	@DisplayName("Dom�nio")
	public Dominio getDominio() {
		return dominio;
	}
		
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}			
}
