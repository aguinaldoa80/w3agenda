package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicoftp;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ServicowebFiltro extends FiltroListagemSined {

	protected Cliente cliente;
	protected Contratomaterial contratomaterial;
	protected Servicoftp servicoftp;
		
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	@DisplayName("FTP")
	public Servicoftp getServicoftp() {
		return servicoftp;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
	public void setServicoftp(Servicoftp servicoftp) {
		this.servicoftp = servicoftp;
	}	
}
