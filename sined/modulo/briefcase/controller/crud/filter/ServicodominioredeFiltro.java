package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ServicodominioredeFiltro extends FiltroListagemSined {

	protected String endereco;

	@DisplayName("Endere�o IP")
	@MaxLength(25)
	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
}
