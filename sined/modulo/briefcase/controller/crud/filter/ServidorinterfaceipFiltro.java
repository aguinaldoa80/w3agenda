package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Rede;
import br.com.linkcom.sined.geral.bean.Redeip;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.bean.Servidorinterface;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ServidorinterfaceipFiltro extends FiltroListagemSined {

	protected Servidor servidor;
	protected Servidorinterface servidorinterface;
	protected Rede rede;
	protected Redeip redeip;

	public Servidor getServidor() {
		return servidor;
	}
	
	@DisplayName("Interface")
	public Servidorinterface getServidorinterface() {
		return servidorinterface;
	}
	
	public Rede getRede() {
		return rede;
	}
	
	@DisplayName("IP")
	public Redeip getRedeip() {
		return redeip;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	
	public void setServidorinterface(Servidorinterface servidorinterface) {
		this.servidorinterface = servidorinterface;
	}
	
	public void setRede(Rede rede) {
		this.rede = rede;
	}
	
	public void setRedeip(Redeip redeip) {
		this.redeip = redeip;
	}
}
