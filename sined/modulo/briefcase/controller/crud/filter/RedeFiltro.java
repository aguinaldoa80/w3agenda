package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Bandaredecomercial;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RedeFiltro extends FiltroListagemSined {

	protected String enderecoip;
	protected Bandaredecomercial bandaredecomercial;
		
	@DisplayName("Endere�o IP")
	@MaxLength(25)
	public String getEnderecoip() {
		return enderecoip;
	}
	
	@DisplayName("Banda comercial")
	public Bandaredecomercial getBandaredecomercial() {
		return bandaredecomercial;
	}
	
	public void setEnderecoip(String enderecoip) {
		this.enderecoip = enderecoip;
	}
	
	public void setBandaredecomercial(Bandaredecomercial bandaredecomercial) {
		this.bandaredecomercial = bandaredecomercial;
	}
}
