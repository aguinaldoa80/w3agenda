package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DominiozonadkimFiltro extends FiltroListagemSined {

	protected Cliente cliente;
	protected Contrato contrato;
	protected Dominio dominio;
	protected String nome;
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public Contrato getContrato() {
		return contrato;
	}

	@DisplayName("Dom�nio")
	public Dominio getDominio() {
		return dominio;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
