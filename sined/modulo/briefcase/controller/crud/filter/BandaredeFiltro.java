package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BandaredeFiltro extends FiltroListagemSined {

	protected Integer valor;
	protected Unidademedida unidademedida;
		
	@DisplayName("Banda")
	@MaxLength(6)
	public Integer getValor() {
		return valor;
	}
	
	@DisplayName("Velocidade")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
}
