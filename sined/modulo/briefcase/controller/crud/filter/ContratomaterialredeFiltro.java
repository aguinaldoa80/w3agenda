package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Ambienterede;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Rede;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.bean.Servidorinterface;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContratomaterialredeFiltro extends FiltroListagemSined {

	protected Cliente cliente;
	protected Contratomaterial contratomaterial;
	protected String mac;
	protected Servicoservidortipo servicoservidortipo;
	protected String enderecocliente;
	protected Ambienterede ambienterede;
	protected Servidor servidor;
	protected Servidorinterface servidorinterfacepreferencial;
	protected Rede rede;
	protected String redeip;
	protected String usuario;
	protected Boolean ativo;
	
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	@MaxLength(50)
	public String getMac() {
		return mac;
	}
	
	@DisplayName("Tipo do servi�o")
	public Servicoservidortipo getServicoservidortipo() {
		return servicoservidortipo;
	}
	
	@DisplayName("Endere�o de instala��o")
	public String getEnderecocliente() {
		return enderecocliente;
	}
	
	@DisplayName("Ambiente")
	public Ambienterede getAmbienterede() {
		return ambienterede;
	}

	public Servidor getServidor() {
		return servidor;
	}

	@DisplayName("Interface preferencial")
	public Servidorinterface getServidorinterfacepreferencial() {
		return servidorinterfacepreferencial;
	}

	public Rede getRede() {
		return rede;
	}

	@DisplayName("Endere�o IP")
	public String getRedeip() {
		return redeip;
	}

	@DisplayName("Usu�rio")
	public String getUsuario() {
		return usuario;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public void setServicoservidortipo(Servicoservidortipo servicoservidortipo) {
		this.servicoservidortipo = servicoservidortipo;
	}

	public void setEnderecocliente(String enderecocliente) {
		this.enderecocliente = enderecocliente;
	}

	public void setAmbienterede(Ambienterede ambienterede) {
		this.ambienterede = ambienterede;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public void setServidorinterfacepreferencial(
			Servidorinterface servidorinterfacepreferencial) {
		this.servidorinterfacepreferencial = servidorinterfacepreferencial;
	}

	public void setRede(Rede rede) {
		this.rede = rede;
	}

	public void setRedeip(String redeip) {
		this.redeip = redeip;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
			
}
