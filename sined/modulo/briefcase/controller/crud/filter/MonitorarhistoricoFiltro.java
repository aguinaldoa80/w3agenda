package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MonitorarhistoricoFiltro extends FiltroListagemSined {

	protected String cliente;
	protected String usuario;
	protected String nasip;
	protected Date dtinicio;
	protected Date dtfim;
	protected String estacaoservidor;
	protected String estacaocliente;
	protected Double totalUpload;
	protected Double totalDownload;
	protected String totalTemposessao;
	protected String framedipaddress;
	protected Boolean pesquisacompleta = Boolean.FALSE;

	@DisplayName("Cliente")
	@MaxLength(64)
	public String getCliente() {
		return cliente;
	}
	@DisplayName("Login")
	@MaxLength(64)
	public String getUsuario() {
		return usuario;
	}
	@DisplayName("Ip da torre")
	@MaxLength(15)
	public String getNasip() {
		return nasip;
	}
	@DisplayName("Endere�o IP")
	@MaxLength(15)
	public String getFramedipaddress() {
		return framedipaddress;
	}
	@DisplayName("De")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("At�")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Servidor")
	@MaxLength(64)
	public String getEstacaoservidor() {
		return estacaoservidor;
	}
	@DisplayName("MAC")
	@MaxLength(64)
	public String getEstacaocliente() {
		return estacaocliente;
	}
	@DisplayName("Uploads (MB)")
	public Double getTotalUpload() {
		return totalUpload;
	}
	@DisplayName("Downloads (MB)")
	public Double getTotalDownload() {
		return totalDownload;
	}
	@DisplayName("Dura��o (h:m:s)")
	public String getTotalTemposessao() {
		return totalTemposessao;
	}
	
	
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public void setNasip(String nasip) {
		this.nasip = nasip;
	}
	public void setFramedipaddress(String framedipaddress) {
		this.framedipaddress = framedipaddress;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setEstacaoservidor(String estacaoservidor) {
		this.estacaoservidor = estacaoservidor;
	}
	public void setEstacaocliente(String estacaocliente) {
		this.estacaocliente = estacaocliente;
	}
	public void setTotalUpload(Double totalUpload) {
		this.totalUpload = totalUpload;
	}
	public void setTotalDownload(Double totalDownload) {
		this.totalDownload = totalDownload;
	}
	public void setTotalTemposessao(String totalTemposessao) {
		this.totalTemposessao = totalTemposessao;
	}
	@DisplayName("Pesquisar Inicio?")
	public Boolean getPesquisacompleta() {
		return pesquisacompleta;
	}
	public void setPesquisacompleta(Boolean pesquisacompleta) {
		this.pesquisacompleta = pesquisacompleta;
	}
}
