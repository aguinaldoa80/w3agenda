package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ServidorwirelessclienteFiltro extends FiltroListagemSined {

	protected Servidor servidor;
	protected String mac;
	protected String sinal;

	public Servidor getServidor() {
		return servidor;
	}

	@MaxLength(20)
	public String getMac() {
		return mac;
	}
	
	@MaxLength(4)
	public String getSinal() {
		return sinal;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	
	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public void setSinal(String sinal) {
		this.sinal = sinal;
	}
}
