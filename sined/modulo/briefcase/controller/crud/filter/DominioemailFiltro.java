package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Servicoservidor;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DominioemailFiltro extends FiltroListagemSined {

	protected Servidor servidor;
	protected Servicoservidor servicoservidor;
	protected Dominio dominio;
	
	public Servidor getServidor() {
		return servidor;
	}
	
	@DisplayName("Servi�o")
	public Servicoservidor getServicoservidor() {
		return servicoservidor;
	}
	
	@DisplayName("Dom�nio")
	public Dominio getDominio() {
		return dominio;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	public void setServicoservidor(Servicoservidor servicoservidor) {
		this.servicoservidor = servicoservidor;
	}
	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	} 
	
}
