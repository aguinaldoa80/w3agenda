package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Ambienterede;
import br.com.linkcom.sined.geral.bean.Plataforma;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ServidorFiltro extends FiltroListagemSined {

	protected Ambienterede ambienterede;
	protected Plataforma plataforma;
	
	@DisplayName("Ambiente de rede")
	public Ambienterede getAmbienterede() {
		return ambienterede;
	}
	@DisplayName("Plataforma")
	public Plataforma getPlataforma() {
		return plataforma;
	}
	public void setAmbienterede(Ambienterede ambienterede) {
		this.ambienterede = ambienterede;
	}
	public void setPlataforma(Plataforma plataforma) {
		this.plataforma = plataforma;
	}
		
	
}
