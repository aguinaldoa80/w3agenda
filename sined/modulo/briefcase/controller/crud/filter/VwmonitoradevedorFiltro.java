package br.com.linkcom.sined.modulo.briefcase.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaorestricao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VwmonitoradevedorFiltro extends FiltroListagemSined{
	private Empresa empresa;
	private Categoria categoria;
	private Cliente cliente;
	private Situacaorestricao situacaorestricao;
	private Integer qtdecontas1;
	private Integer qtdecontas2;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@MaxLength(9)
	public Integer getQtdecontas1() {
		return qtdecontas1;
	}
	@MaxLength(9)
	public Integer getQtdecontas2() {
		return qtdecontas2;
	}
	@DisplayName("Situa��o")
	public Situacaorestricao getSituacaorestricao() {
		return situacaorestricao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setSituacaorestricao(Situacaorestricao situacaorestricao) {
		this.situacaorestricao = situacaorestricao;
	}
	public void setQtdecontas1(Integer qtdecontas1) {
		this.qtdecontas1 = qtdecontas1;
	}
	public void setQtdecontas2(Integer qtdecontas2) {
		this.qtdecontas2 = qtdecontas2;
	}
	
}
