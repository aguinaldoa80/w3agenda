package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Radiusaccounting;
import br.com.linkcom.sined.geral.service.RadiusaccountingService;
import br.com.linkcom.sined.geral.service.ServidorService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
		path="/briefcase/crud/Radiusaccounting", 
		authorizationModule=CrudAuthorizationModule.class
)
public class RadiusaccountingCrud extends CrudControllerSined<FiltroListagemSined, Radiusaccounting, Radiusaccounting>{
	
	private RadiusaccountingService radiusaccountingService;
	private ServidorService servidorService;
	
	public void setServidorService(ServidorService servidorService) {
		this.servidorService = servidorService;
	}
	public void setRadiusaccountingService(RadiusaccountingService radiusaccountingService) {
		this.radiusaccountingService = radiusaccountingService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Radiusaccounting bean)throws Exception {
		throw new SinedException("N�o � poss�vel salvar o registro.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Radiusaccounting bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir o registro.");
	}	
	
	@Override
	protected Radiusaccounting criar(WebRequestContext request, Radiusaccounting form) throws Exception {
		throw new SinedException("N�o � poss�vel criar registro.");
	}
	
	@Override
	protected ListagemResult<Radiusaccounting> getLista(WebRequestContext request, FiltroListagemSined filtro) {
		ListagemResult<Radiusaccounting> list = super.getLista(request, filtro);
		List<Radiusaccounting> lista = list.list();
		
		for (Radiusaccounting rc : lista) {
			rc.setServidor(servidorService.findByIp(rc.getFramedipaddress()));
		}
		
		return list;
	}
	
	/**
	 * M�todo para concluir as sess�es abertas
	 * @param request
	 * @param form
	 * @return
	 * @author Thiago Gon�alves
	 */
	public ModelAndView concluirtempo(WebRequestContext request, Radiusaccounting form){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		radiusaccountingService.confirmartempo(itens);
		request.addMessage("Tempo conclu�do para os registros marcados.");
		return sendRedirectToAction("listagem");
		
	}
		
}
