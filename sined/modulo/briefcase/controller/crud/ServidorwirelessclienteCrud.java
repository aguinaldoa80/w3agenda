package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Servidorwirelesscliente;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServidorwirelessclienteFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Servidorwirelesscliente", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ServidorwirelessclienteCrud extends CrudControllerSined<ServidorwirelessclienteFiltro, Servidorwirelesscliente, Servidorwirelesscliente>{
	
	@Override
	protected void salvar(WebRequestContext request, Servidorwirelesscliente bean)throws Exception {
		throw new SinedException("N�o � poss�vel salvar o registro.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Servidorwirelesscliente bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir o registro.");
	}	
	
	@Override
	protected Servidorwirelesscliente criar(WebRequestContext request, Servidorwirelesscliente form) throws Exception {
		throw new SinedException("N�o � poss�vel criar registro.");
	}
		
}
