package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicobanco;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.ServicobancoService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServicobancoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Servicobanco", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ServicobancoCrud extends CrudControllerSined<ServicobancoFiltro, Servicobanco, Servicobanco>{
	
	private ContratomaterialService contratomaterialService;
	private ServicobancoService servicobancoService;
	
	public void setServicobancoService(ServicobancoService servicobancoService) {
		this.servicobancoService = servicobancoService;
	}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {
		this.contratomaterialService = contratomaterialService;
	}
	
	@Override
	protected Servicobanco carregar(WebRequestContext request, Servicobanco bean) throws Exception {
		Servicobanco servicobanco = super.carregar(request, bean);
		if (servicobanco.getContratomaterial() != null)
			servicobanco.setCliente(servicobanco.getContratomaterial().getContrato().getCliente());
		if (servicobanco.getServicoservidor() != null)
			servicobanco.setServidor(servicobanco.getServicoservidor().getServidor());
		return servicobanco;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Servicobanco bean)throws Exception {
		super.salvar(request, bean);		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Servicobanco bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Servi�o de banco de dados n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
	
	@Override
	protected void validateBean(Servicobanco bean, BindException errors) {
		String usuario = bean.getUsuario();
		if (!usuario.matches("[A-Za-z0-9\\.\\-_]+")){
			errors.reject("001", "Caracteres inv�lidos em usu�rio.");		
		}
		
		int numReg = servicobancoService.countContratomaterial(bean.getCdservicobanco(), bean.getContratomaterial());
		Double qtde = contratomaterialService.load(bean.getContratomaterial(), "contratomaterial.qtde").getQtde();
		
		if(qtde <= numReg){
			errors.reject("002","N�o se pode criar mais registros que a quantidade do servi�o no contrato.");
		}
	}
	
	@Action("filtrarcontrato")
	public ModelAndView filtrarContrato(WebRequestContext request, Contratomaterial contratomaterial) throws CrudException {
		Cliente cliente = contratomaterialService.findCliente(contratomaterial);
		ServicobancoFiltro filtro = new ServicobancoFiltro();
		filtro.setCliente(cliente);
		filtro.setContratomaterial(contratomaterial);
		ListagemResult<Servicobanco> lista = getLista(request, filtro);
		if (lista != null && !lista.list().isEmpty() && lista.list().size() >= contratomaterial.getQtde())
			return doListagem(request, filtro);
		else {
			Servicobanco form = new Servicobanco();
			form.setCliente(cliente);
			form.setContratomaterial(contratomaterial);
			
			((DefaultWebRequestContext)request).setLastAction("entrada");
			return doEntrada(request, form);			
		}
	}
	
	/**
	 * Faz o ajax buscando a quantidade do <code>Contratomaterial</code> para colocar no campo cota.
	 *
	 * @param request
	 * @param servicobanco
	 * @author Rodrigo Freitas
	 */
	public void ajaxCota(WebRequestContext request, Servicobanco servicobanco){
		if(servicobanco.getContratomaterial() != null && servicobanco.getContratomaterial().getCdcontratomaterial() != null){
			Contratomaterial contratomaterial = contratomaterialService.load(servicobanco.getContratomaterial(), "contratomaterial.cdcontratomaterial, contratomaterial.qtde");
			View.getCurrent().println("var sucesso = true; var cota = '" + contratomaterial.getQtde() + "';");
		} else {
			View.getCurrent().println("var sucesso = false;");
		}
	}
}
