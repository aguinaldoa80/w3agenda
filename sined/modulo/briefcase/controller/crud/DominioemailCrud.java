package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Dominioemail;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominioemailFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Dominioemail", 
		authorizationModule=CrudAuthorizationModule.class
)
public class DominioemailCrud extends CrudControllerSined<DominioemailFiltro, Dominioemail, Dominioemail>{
		
	@Override
	protected Dominioemail carregar(WebRequestContext request, Dominioemail bean) throws Exception {
		Dominioemail dominioemail = super.carregar(request, bean);		
		if (dominioemail.getServicoservidor() != null)
			dominioemail.setServidor(dominioemail.getServicoservidor().getServidor());
		return dominioemail;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Dominioemail bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_DOMINIOEMAIL_SERVDOMINIO")){
				throw new SinedException("E-mail j� cadastrado no sistema.");
			}
		}		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Dominioemail bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("E-mail n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
			
}
