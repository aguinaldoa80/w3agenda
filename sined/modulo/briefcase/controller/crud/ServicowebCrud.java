package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Dominiozona;
import br.com.linkcom.sined.geral.bean.Servicoweb;
import br.com.linkcom.sined.geral.bean.Servicowebalias;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.DominiozonaService;
import br.com.linkcom.sined.geral.service.ServicowebService;
import br.com.linkcom.sined.geral.service.ServidorService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ServicowebFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Servicoweb", 
		authorizationModule=CrudAuthorizationModule.class
)
public class ServicowebCrud extends CrudControllerSined<ServicowebFiltro, Servicoweb, Servicoweb>{
	
	private DominiozonaService dominiozonaService;
	private ServidorService servidorService;
	private ContratomaterialService contratomaterialService;
	private ServicowebService servicowebService;
	
	public void setServicowebService(ServicowebService servicowebService) {
		this.servicowebService = servicowebService;
	}
	public void setContratomaterialService(
			ContratomaterialService contratomaterialService) {
		this.contratomaterialService = contratomaterialService;
	}
	public void setDominiozonaService(DominiozonaService dominiozonaService) {
		this.dominiozonaService = dominiozonaService;
	}
	public void setServidorService(ServidorService servidorService) {
		this.servidorService = servidorService;
	}
	
	@Override
	protected Servicoweb carregar(WebRequestContext request, Servicoweb bean)throws Exception {
		Servicoweb servicoweb = super.carregar(request, bean);
		if (servicoweb.getContratomaterial() != null)
			servicoweb.setCliente(servicoweb.getContratomaterial().getContrato().getCliente());		
		return servicoweb;
	}
	
	@Override
	protected void validateBean(Servicoweb bean, BindException errors) {
		int numReg = servicowebService.countContratomaterial(bean.getCdservicoweb(), bean.getContratomaterial());
		Double qtde = contratomaterialService.load(bean.getContratomaterial(), "contratomaterial.qtde").getQtde();
		
		if(qtde <= numReg){
			errors.reject("002","N�o se pode criar mais registros que a quantidade do servi�o no contrato.");
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Servicoweb bean)throws Exception {
		super.salvar(request, bean);		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Servicoweb bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Servi�o Web n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
	
	@Override
	protected void entrada(WebRequestContext request, Servicoweb form) throws Exception {
		Map<Integer, List<Dominiozona>> listaDominioZona = new HashMap<Integer, List<Dominiozona>>();
		if(form.getListaServicowebalias() != null){
			int i = 0;			
			Set<Servicowebalias> lista = form.getListaServicowebalias();
			for (Servicowebalias servicowebalias : lista) {									
				if (servicowebalias.getDominiozona() != null){
					Dominio dominio = servicowebalias.getDominiozona().getDominio();
					listaDominioZona.put(i, dominiozonaService.findByDominio(dominio));
				}				
				i++;
			}
		}
		
		if(form.getCdservicoweb() == null)
			request.setAttribute("listaServidores", servidorService.findForComboServicoWeb());
		
		request.setAttribute("listaDominioZona", listaDominioZona);
	}
	
	/**
	 * Retorna lista de dominiozona de acordo com o dominio
	 * @param request
	 * @param servidor
	 * @author Thiago Gon�alves
	 */
	public void makeAjaxDominiozona(WebRequestContext request, Dominio dominio){
		List<Dominiozona> listDominiozona = null;
		String par = request.getParameter("dominio");
		try {
			Integer cddominio = Integer.parseInt(par.substring(par.lastIndexOf("=")+1, par.lastIndexOf("]")));
			dominio.setCddominio(cddominio);
			listDominiozona = dominiozonaService.findByDominio(dominio);
		} catch (Exception e) {
			listDominiozona = new ArrayList<Dominiozona>();
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listDominiozona, "listaDominiozona", ""));
	}
	
	
	@Action("filtrarcontrato")
	public ModelAndView filtrarContrato(WebRequestContext request, Contratomaterial contratomaterial) throws CrudException {
		Cliente cliente = contratomaterialService.findCliente(contratomaterial);
		ServicowebFiltro filtro = new ServicowebFiltro();
		filtro.setCliente(cliente);
		filtro.setContratomaterial(contratomaterial);
		ListagemResult<Servicoweb> lista = getLista(request, filtro);
		if (lista != null && !lista.list().isEmpty() && lista.list().size() >= contratomaterial.getQtde())
			return doListagem(request, filtro);
		else {
			Servicoweb form = new Servicoweb();
			form.setCliente(cliente);
			form.setContratomaterial(contratomaterial);
			
			((DefaultWebRequestContext)request).setLastAction("entrada");
			return doEntrada(request, form);			
		}
	}
}
