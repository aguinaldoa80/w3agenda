package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Radiushistorico;
import br.com.linkcom.sined.geral.service.MonitorarhistoricoService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.MonitorarhistoricoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Monitorarhistorico", 
		authorizationModule=CrudAuthorizationModule.class
)
public class MonitorarhistoricoCrud extends CrudControllerSined<MonitorarhistoricoFiltro, Radiushistorico, Radiushistorico>{
	
	private MonitorarhistoricoService monitorarhistoricoService;
	
	public void setMonitorarhistoricoService(MonitorarhistoricoService monitorarhistoricoService) {
		this.monitorarhistoricoService = monitorarhistoricoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected ListagemResult<Radiushistorico> getLista(	WebRequestContext request, MonitorarhistoricoFiltro filtro) {
		
		ListagemResult<Radiushistorico> lista = super.getLista(request, filtro);
		for (Radiushistorico item : lista.list()) {
			
			Double resultadoEntrada = item.getDadoentrada() != null ? item.getDadoentrada().doubleValue() : 0d;
			//Converte bytes para megabytes
			item.setDadoentradaMb(resultadoEntrada/1048576);
			
			Double resultadoSaida = item.getDadosaida() != null ? item.getDadosaida().doubleValue() : 0d;
			//Converte bytes para megabytes
			item.setDadosaidaMb(resultadoSaida/1048576);
		}

		if("true".equals(request.getParameter("calcularTotalUpload"))){
			Long somaUploads  = monitorarhistoricoService.somaUploads(filtro);
			Double totalUploads = somaUploads != null ? somaUploads.doubleValue() : 0d;
			filtro.setTotalUpload(totalUploads/1048576);
		} else {
			filtro.setTotalUpload(null);
		}
		
		if("true".equals(request.getParameter("calcularTotalDownload"))){
			Long somaDownloads = monitorarhistoricoService.somaDownloads(filtro);
			Double totalDownloads = somaDownloads != null ? somaDownloads.doubleValue() : 0d;
			filtro.setTotalDownload(totalDownloads/1048576);
		} else {
			filtro.setTotalDownload(null);
		}
		
		if("true".equals(request.getParameter("calcularTempoSessao"))){
			Long totalTemposessao = monitorarhistoricoService.somaTemposessao(filtro);
			filtro.setTotalTemposessao(monitorarhistoricoService.convertSecoundsToHour(totalTemposessao));
		} else {
			filtro.setTotalTemposessao(null);
		}
		
		return lista;
	}


	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, MonitorarhistoricoFiltro filtro) {
		return new ModelAndView("crud/monitorarhistoricoListagem");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Radiushistorico bean)throws Exception {
		throw new SinedException("N�o � poss�vel salvar o registro.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Radiushistorico bean)throws Exception {
		throw new SinedException("N�o � poss�vel excluir o registro.");
	}	
	
	@Override
	protected Radiushistorico criar(WebRequestContext request, Radiushistorico form) throws Exception {
		throw new SinedException("N�o � poss�vel criar registro.");
	}
	
		
}
