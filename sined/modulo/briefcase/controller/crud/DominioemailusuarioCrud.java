package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Dominioemailusuario;
import br.com.linkcom.sined.geral.service.ContratomaterialService;
import br.com.linkcom.sined.geral.service.DominioService;
import br.com.linkcom.sined.geral.service.DominioemailusuarioService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.DominioemailusuarioFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/briefcase/crud/Dominioemailusuario", 
		authorizationModule=CrudAuthorizationModule.class
)
public class DominioemailusuarioCrud extends CrudControllerSined<DominioemailusuarioFiltro, Dominioemailusuario, Dominioemailusuario>{
	
	private DominioService dominioService;
	private ContratomaterialService contratomaterialService;
	private DominioemailusuarioService dominioemailusuarioService;
	
	public void setDominioemailusuarioService(
			DominioemailusuarioService dominioemailusuarioService) {
		this.dominioemailusuarioService = dominioemailusuarioService;
	}
	public void setDominioService(DominioService dominioService) {
		this.dominioService = dominioService;
	}
	public void setContratomaterialService(ContratomaterialService contratomaterialService) {
		this.contratomaterialService = contratomaterialService;
	}
	
	@Override
	protected Dominioemailusuario carregar(WebRequestContext request, Dominioemailusuario bean) throws Exception {
		Dominioemailusuario dominioemailusuario = super.carregar(request, bean);
		if (dominioemailusuario.getContratomaterial() != null)
			dominioemailusuario.setCliente(dominioemailusuario.getContratomaterial().getContrato().getCliente());		
		return dominioemailusuario;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Dominioemailusuario bean)throws Exception {
		Dominio dominio = dominioService.loadForEntrada(bean.getDominio());		
		bean.setEmail(bean.getUsuario() + '@' + dominio.getNome());
		
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_dominioemailusuario_unico")) {
				throw new SinedException("E-mail j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Dominioemailusuario bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Servi�o de e-mail n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}	
	
	@Override
	protected void validateBean(Dominioemailusuario bean, BindException errors) {		
		if (!bean.getUsuario().matches("[A-Za-z0-9\\.\\-_]+")){
			errors.reject("001", "Caracteres inv�lidos em nome.");		
		}
		
		int numReg = dominioemailusuarioService.countContratomaterial(bean.getCddominioemailusuario(), bean.getContratomaterial());
		Double qtde = contratomaterialService.load(bean.getContratomaterial(), "contratomaterial.qtde").getQtde();
		
		if(qtde <= numReg){
			errors.reject("002","N�o se pode criar mais registros que a quantidade do servi�o no contrato.");
		}
	}
	
	@Action("filtrarcontrato")
	public ModelAndView filtrarContrato(WebRequestContext request, Contratomaterial contratomaterial) throws CrudException {
		Cliente cliente = contratomaterialService.findCliente(contratomaterial);
		DominioemailusuarioFiltro filtro = new DominioemailusuarioFiltro();
		filtro.setCliente(cliente);
		filtro.setContratomaterial(contratomaterial);
		ListagemResult<Dominioemailusuario> lista = getLista(request, filtro);
		if (lista != null && !lista.list().isEmpty() && lista.list().size() >= contratomaterial.getQtde())
			return doListagem(request, filtro);
		else {
			Dominioemailusuario form = new Dominioemailusuario();
			form.setCliente(cliente);
			form.setContratomaterial(contratomaterial);
			
			((DefaultWebRequestContext)request).setLastAction("entrada");
			return doEntrada(request, form);			
		}
	}
	
	/**
	 * M�todo para buscar dados referente ao material do contrato de material selecionado
	 *  na tela de email do m�dulo Briefcase
	 * 
	 * @param request
	 * @param contratomaterial
	 * @author Marden Silva
	 */
	public void ajaxOnChangeContratomaterial(WebRequestContext request, Contratomaterial contratomaterial){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		contratomaterial = contratomaterialService.loadForEmail(contratomaterial);
		view.println("var qtdecota = '" + (contratomaterial.getQtde() != null ? contratomaterial.getQtde() : "") + "';");
		view.println("var unidademedida = '" + (contratomaterial.getServico() != null && contratomaterial.getServico().getUnidademedida() != null ? contratomaterial.getServico().getUnidademedida().getNome() : "") + "';");
	}
}
