package br.com.linkcom.sined.modulo.briefcase.controller.crud;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Restricaodocumento;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentonotacontrato;
import br.com.linkcom.sined.geral.bean.view.Vwmonitoradevedor;
import br.com.linkcom.sined.geral.service.RestricaodocumentoService;
import br.com.linkcom.sined.geral.service.VwdocumentonotacontratoService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.VwmonitoradevedorFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/briefcase/crud/Vwmonitoradevedor", authorizationModule=CrudAuthorizationModule.class)
public class VwmonitoradevedorCrud extends CrudControllerSined<VwmonitoradevedorFiltro, Vwmonitoradevedor, Vwmonitoradevedor>{

	private VwdocumentonotacontratoService vwdocumentonotacontratoService;
	private RestricaodocumentoService restricaodocumentoService;
	
	public void setRestricaodocumentoService(RestricaodocumentoService restricaodocumentoService) {this.restricaodocumentoService = restricaodocumentoService;}
	public void setVwdocumentonotacontratoService(VwdocumentonotacontratoService vwdocumentonotacontratoService) {this.vwdocumentonotacontratoService = vwdocumentonotacontratoService;}
	
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected ListagemResult<Vwmonitoradevedor> getLista(WebRequestContext request, VwmonitoradevedorFiltro filtro) {
		
		ListagemResult<Vwmonitoradevedor> lista = super.getLista(request, filtro);
		List<Vwdocumentonotacontrato> listaItens;
		List<Restricaodocumento> listaDocs;
		Contrato contrato;
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		Iterator<Vwmonitoradevedor> iterator = lista.list().iterator();
		while(iterator.hasNext()){
			Vwmonitoradevedor item = iterator.next();
			listaItens = new ArrayList<Vwdocumentonotacontrato>();
			
			contrato = new Contrato();
			contrato.setCdcontrato(item.getId());
			
			listaDocs = restricaodocumentoService.findByContratoUltimaRestricao(contrato);
			StringBuilder blo = new StringBuilder();
			for (Restricaodocumento r : listaDocs) {
				blo.append("<a href=\"javascript:visualizarContaReceber("+r.getDocumento().getCddocumento()+")\">"+r.getDocumento().getCddocumento()+"</a>" + "("+r.getDocumento().getDocumentoacao().getNome()+"); ");
			}
			item.setContasbloqueio(blo.toString());
			
			listaItens = vwdocumentonotacontratoService.listaDocumentonotacontrato(contrato);
			StringBuilder doc = new StringBuilder();
			for (Vwdocumentonotacontrato d : listaItens) {
				doc.append("<a href=\"javascript:visualizarContaReceber("+d.getCddocumento()+")\">"+d.getCddocumento()+"</a>" + "("+format.format(d.getDtvencimento())+"); ");
			}
			item.setDocumentos(doc.toString());
		}
		return lista;
	}
}
