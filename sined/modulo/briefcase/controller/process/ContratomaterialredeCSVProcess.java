package br.com.linkcom.sined.modulo.briefcase.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ContratomaterialredeService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.ContratomaterialredeFiltro;

@Controller(
		path="/briefcase/process/ContratomaterialredeCSV",
			authorizationModule=ProcessAuthorizationModule.class
)
public class ContratomaterialredeCSVProcess extends ResourceSenderController<ContratomaterialredeFiltro>{
	
	private ContratomaterialredeService contratomaterialredeService;
	
	public void setContratomaterialredeService(ContratomaterialredeService contratomaterialredeService) {
		this.contratomaterialredeService = contratomaterialredeService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,	ContratomaterialredeFiltro filtro) throws Exception {
		return contratomaterialredeService.preparaArquivoContratomaterialredeCSV(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	ContratomaterialredeFiltro filtro) throws Exception {		
		return null;
	}

	
}
