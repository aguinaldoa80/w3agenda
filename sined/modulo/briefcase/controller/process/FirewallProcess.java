package br.com.linkcom.sined.modulo.briefcase.controller.process;


import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Servidorfirewall;
import br.com.linkcom.sined.geral.bean.Servidorfirewallsentido;

@Controller(
		path={"/briefcase/process/Firewall"},
		authorizationModule=ProcessAuthorizationModule.class
)
public class FirewallProcess extends MultiActionController{
	
	/**
	 * Carrega a popup de cria��o de um contato.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, Servidorfirewall servidorfirewall){
		return new ModelAndView("direct:process/popup/criaFirewall","servidorfirewall",servidorfirewall);
	}
	
	
	/**
	 * Exclui o contato da lista no escopo de sess�o.
	 * 
	 * @param request
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	@Action("excluir")
	public void excluir(WebRequestContext request){
		
		List<Servidorfirewall> listaFirewall = null;
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			String parameter = object.toString();
			parameter = parameter.substring(parameter.indexOf("indice=")+7, parameter.length()-1);
			Integer index = Integer.parseInt(parameter)-1;
			Object attr = request.getSession().getAttribute("listaServidorFirewall");
			if (attr != null) {
				listaFirewall = (List<Servidorfirewall>) attr;
				listaFirewall.remove(index.intValue());
			}
		}
		request.getSession().setAttribute("listaServidorFirewall", listaFirewall);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.close();</script>");
	}
	
	
	
	/**
	 * Salva o item de intera��o no objeto interacao e logo depois joga o objeto para a sess�o.
	 * 
	 * @param request
	 * @param interacaoitem
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	@Command(validate=true)
	@Input("criar")
	@Action("salvainteracao")
	public void salvaInteracao(WebRequestContext request, Servidorfirewall servidorfirewall){
		
		Object attribute = request.getSession().getAttribute("listaServidorFirewall");
		List<Servidorfirewall> listaFirewall = null;
		
		if (attribute == null) {
			listaFirewall = new ArrayList<Servidorfirewall>();
			listaFirewall.add(servidorfirewall);
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>" +
										"window.opener.montaListaFirewall('"+servidorfirewall.getOrdem()+
										"','"+servidorfirewall.getAcao()+
										"','"+servidorfirewall.getProtocolo()+
										"','"+servidorfirewall.getIporigem()+
										"','"+servidorfirewall.getPortaorigem()+
										"','"+servidorfirewall.getIpdestino()+
										"','"+servidorfirewall.getPortadestino()+
										"','"+(servidorfirewall.getServidorfirewallsentido() == null ? "" : servidorfirewall.getServidorfirewallsentido().equals(Servidorfirewallsentido.ENTRADA) ? Servidorfirewallsentido.ENTRADA.getNome() : Servidorfirewallsentido.SAIDA.getNome())+
										"','"+servidorfirewall.getCampo_interface()+
										"');" +
										"window.close();" +
										"</script>");
		} else {
			listaFirewall = (List<Servidorfirewall>)attribute;
			
			if (servidorfirewall.getIndexlista() != null) {
				listaFirewall.set(servidorfirewall.getIndexlista(), servidorfirewall);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"window.opener.editaItem('"+servidorfirewall.getIndexlista()+
											"','"+servidorfirewall.getOrdem()+
											"','"+servidorfirewall.getAcao()+
											"','"+servidorfirewall.getProtocolo()+
											"','"+servidorfirewall.getIporigem()+
											"','"+servidorfirewall.getPortaorigem()+
											"','"+servidorfirewall.getIpdestino()+
											"','"+servidorfirewall.getPortadestino()+
											"','"+(servidorfirewall.getServidorfirewallsentido() == null ? "" : servidorfirewall.getServidorfirewallsentido().equals(Servidorfirewallsentido.ENTRADA) ? Servidorfirewallsentido.ENTRADA.getNome() : Servidorfirewallsentido.SAIDA.getNome())+
											"','"+servidorfirewall.getCampo_interface()+
											"');" +
											"window.close();" +
											"</script>");
			} else {
				listaFirewall.add(servidorfirewall);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"window.opener.montaListaFirewall('"+servidorfirewall.getOrdem()+
											"','"+servidorfirewall.getAcao()+
											"','"+servidorfirewall.getProtocolo()+
											"','"+servidorfirewall.getIporigem()+
											"','"+servidorfirewall.getPortaorigem()+
											"','"+servidorfirewall.getIpdestino()+
											"','"+servidorfirewall.getPortadestino()+
											"','"+(servidorfirewall.getServidorfirewallsentido() == null ? "" : servidorfirewall.getServidorfirewallsentido().equals(Servidorfirewallsentido.ENTRADA) ? Servidorfirewallsentido.ENTRADA.getNome() : Servidorfirewallsentido.SAIDA.getNome())+
											"','"+servidorfirewall.getCampo_interface()+
											"');" +
											"window.close();" +
											"</script>");
			}
		}	
		
		request.getSession().setAttribute("listaServidorFirewall", listaFirewall);
	}
	
	
	
	
	/**
	 * Edita a intera��o da lista na entrada de prospec��o; 
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#carregaIndexLista
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	@Action("editar")
	public ModelAndView editar(WebRequestContext request){
		
		Servidorfirewall servidorfirewall = null;
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			Integer index = Integer.parseInt((String)object);
			Object attr = request.getSession().getAttribute("listaServidorFirewall");
			List<Servidorfirewall> listaServidorFirewall = null;
			if (attr != null) {
				listaServidorFirewall = (List<Servidorfirewall>) attr;
				servidorfirewall = listaServidorFirewall.get(index);
				servidorfirewall.setIndexlista(index);

			}
		}			
		return new ModelAndView("direct:process/popup/criaFirewall","servidorfirewall",servidorfirewall);
	}
	
	
//	@Override
//	protected void validate(Object obj, BindException errors, String acao) {
//		Contato contato = (Contato) obj;
//		enderecoService.validateListaEndereco(errors, contato.getListaEndereco());
//	}
	

	
}
