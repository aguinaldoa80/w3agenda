package br.com.linkcom.sined.modulo.briefcase.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.MonitorarhistoricoService;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.MonitorarhistoricoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/briefcase/relatorio/Monitorarhistorico", 
			authorizationModule=ReportAuthorizationModule.class
)
public class MonitorarhistoricoReport extends SinedReport<MonitorarhistoricoFiltro>{

	private MonitorarhistoricoService monitorarhistoricoService;
	
	public void setMonitorarhistoricoService(
			MonitorarhistoricoService monitorarhistoricoService) {
		this.monitorarhistoricoService = monitorarhistoricoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	MonitorarhistoricoFiltro filtro) throws Exception {
		return monitorarhistoricoService.gerarMonitoramentoHistorico(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "historico";
	}

	@Override
	public String getTitulo(MonitorarhistoricoFiltro filtro) {
		return "HISTÓRICO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report,	MonitorarhistoricoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
		report.addParameter("nasip", filtro.getNasip());
		report.addParameter("cliente", filtro.getUsuario());
	}
	
}
