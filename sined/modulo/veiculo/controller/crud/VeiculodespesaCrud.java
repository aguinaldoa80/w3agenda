package br.com.linkcom.sined.modulo.veiculo.controller.crud;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculodespesa;
import br.com.linkcom.sined.geral.bean.Veiculodespesaitem;
import br.com.linkcom.sined.geral.bean.Veiculodespesatipo;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueorigemService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculodespesaService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculodespesaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/veiculo/crud/Veiculodespesa", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdveiculodespesa", "veiculo", "veiculodespesatipo", "motivo", "empresa", "dtentrada", "valortotal", "faturado", "baixado"})
public class VeiculodespesaCrud extends CrudControllerSined<VeiculodespesaFiltro, Veiculodespesa, Veiculodespesa>{
	
	private MaterialService materialService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private VeiculodespesaService veiculodespesaService;
	private LocalarmazenagemService localarmazenagemService;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private VeiculoService veiculoService;
	private RateioService rateioService;
	private CentrocustoService centrocustoService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setVeiculodespesaService(VeiculodespesaService veiculodespesaService) {
		this.veiculodespesaService = veiculodespesaService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setMovimentacaoestoqueorigemService(MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {
		this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	
	@Override
	protected Veiculodespesa criar(WebRequestContext request, Veiculodespesa form)throws Exception {
		String cdveiculo = request.getParameter("cdveiculo");
		if(cdveiculo != null && !cdveiculo.equals("")){
			form.setVeiculo(veiculoService.loadForEntrada(new Veiculo(Integer.valueOf(cdveiculo))));
			return form;
		}
		return super.criar(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request,	VeiculodespesaFiltro filtro) throws Exception {
		List<Veiculodespesatipo> listatipo = new ArrayList<Veiculodespesatipo>();
		listatipo.add(Veiculodespesatipo.ABASTECIMENTO);
		listatipo.add(Veiculodespesatipo.IMPOSTO);
		listatipo.add(Veiculodespesatipo.INFRACAO);
		listatipo.add(Veiculodespesatipo.MANUTENCAO);
		listatipo.add(Veiculodespesatipo.PNEU);
		
		request.setAttribute("listadespesatipo", listatipo);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Veiculodespesa form)	throws Exception {
		if(form == null || form.getCdveiculodespesa() == null){
			form.setDtentrada(new Date(System.currentTimeMillis()));
			form.setBaixado(Boolean.FALSE);
		}

		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		request.setAttribute("listamaterials", materialService.getListaMateriaisProdutoEpiServicoByClasse(Materialclasse.PRODUTO));
		request.setAttribute("CASAS_DECIMAIS_ARREDONDAMENTO", SinedUtil.getParametroCasasDecimaisArredondamento());
	}
	
	@Override
	protected void excluir(WebRequestContext request, Veiculodespesa bean) throws Exception {
		try {
			super.excluir(request, bean);
		} catch (SinedException e) {
			throw new SinedException("Despesa do ve�culo n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	/**
	 * M�todo que sugere valor do material para o usu�rio
	 * 
	 * @see MaterialService#getPrecoCustoProduto(Material)
	 * @param request
	 * @param material
	 * @author Tom�s Rabelo
	 */
	public ModelAndView getPrecoCustoProdutoAjax(WebRequestContext request, Material material){
		Material materialAux = materialService.getPrecoCustoProduto(material);
		Double precoCusto = 0.0;
		if(materialAux != null && materialAux.getValorcusto() != null){
			precoCusto = SinedUtil.roundByParametro(materialAux.getValorcusto());
		}
		
		request.getServletResponse().setContentType("text/html");
		return new JsonModelAndView().addObject("valor", SinedUtil.descriptionDecimal(precoCusto))
									 .addObject("nome", (materialAux == null ? "" : materialAux.getNome()));
	}
	
	public ModelAndView ajaxQtdeMaterialAjax(WebRequestContext request, Veiculodespesaitem veiculodespesaitem){
		Double qtdedisponivel = null;
		
		if (veiculodespesaitem.getMaterial()!=null && veiculodespesaitem.getMaterial().getCdmaterial()!=null){
			qtdedisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(veiculodespesaitem.getMaterial(), Materialclasse.PRODUTO, veiculodespesaitem.getLocalarmazenagem(), veiculodespesaitem.getEmpresa(), null);
			return new JsonModelAndView().addObject("qtdedisponivel", new DecimalFormat("#,##0.#########").format(qtdedisponivel));
		}else {
			return new JsonModelAndView().addObject("qtdedisponivel", null);
		}
	}
	
	public ModelAndView doBaixar(WebRequestContext request) throws Exception {
		String selectedItens = request.getParameter("selectedItens").replace(" ", "");
		boolean baixar = true;
		
		for (String id: selectedItens.split(",")){
			Veiculodespesa veiculodespesa = veiculodespesaService.loadForEntrada(new Veiculodespesa(Integer.valueOf(id)));
			
			if (Boolean.TRUE.equals(veiculodespesa.getBaixado())){
				request.addMessage("Despesa j� baixada.", MessageType.ERROR);
				baixar = false;
				break;
			}
			
			if (veiculodespesa.getListaveiculodespesaitem()!=null){
				for (Veiculodespesaitem veiculodespesaitem: veiculodespesa.getListaveiculodespesaitem()){
					if (Boolean.TRUE.equals(veiculodespesaitem.getConsumoproprio())
							&& veiculodespesaitem.getMaterial()!=null
							&& veiculodespesaitem.getQuantidade()!=null
							&& veiculodespesaitem.getQuantidade()>0
							&& (veiculodespesaitem.getLocalarmazenagem()==null || !localarmazenagemService.getPermitirestoquenegativo(veiculodespesaitem.getLocalarmazenagem()))
							&& veiculodespesaitem.getQuantidade().doubleValue()>movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(veiculodespesaitem.getMaterial(), Materialclasse.PRODUTO, veiculodespesaitem.getLocalarmazenagem())){
						request.addMessage("A despesa n�o pode ser baixada pois o estoque do produto " + veiculodespesaitem.getMaterial().getAutocompleteDescription() + " ficaria negativo.", MessageType.ERROR);
						baixar = false;
						break;							
					}
					if(SinedUtil.isRateioMovimentacaoEstoque()){
						if(veiculodespesaitem.getCentrocusto() == null){
							request.addMessage("O campo Centro de custo � obrigat�rio. (" + veiculodespesaitem.getMaterial().getAutocompleteDescription() + ")", MessageType.ERROR);
							baixar = false;

						}
						if(veiculodespesaitem.getMaterial() != null && (veiculodespesaitem.getMaterial().getMaterialRateioEstoque() == null || veiculodespesaitem.getMaterial().getMaterialRateioEstoque().getContaGerencialConsumo() == null)){
							request.addMessage("� obrigat�rio o preenchimento da conta gerencial de consumo na aba Rateio de Estoque do cadastro de material. (" + veiculodespesaitem.getMaterial().getAutocompleteDescription() + ")", MessageType.ERROR);
							baixar = false;
						}
						
						if(!baixar){
							break;
						}
					}
				}
			}
		}
		
		if (baixar){
			String whereIn = "";
			
			for (String id: selectedItens.split(",")){
				Veiculodespesa veiculodespesa = veiculodespesaService.loadForEntrada(new Veiculodespesa(Integer.valueOf(id)));
				
				if (veiculodespesa.getListaveiculodespesaitem()!=null){
					for (Veiculodespesaitem veiculodespesaitem : veiculodespesa.getListaveiculodespesaitem()){
						if (Boolean.TRUE.equals(veiculodespesaitem.getConsumoproprio())
								&& (veiculodespesaitem.getMaterial()!=null || (veiculodespesaitem.getNome()!=null && !veiculodespesaitem.getNome().trim().equals("")))
								&& veiculodespesaitem.getQuantidade()!=null
								&& veiculodespesaitem.getQuantidade()>0){
							if (veiculodespesaitem.getMaterial()!=null){
								Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
								movimentacaoestoqueorigem.setVeiculodespesa(veiculodespesa);
								movimentacaoestoqueorigemService.saveOrUpdate(movimentacaoestoqueorigem);
								
								Movimentacaoestoque movimentacaoestoque = 
										new Movimentacaoestoque(veiculodespesaitem.getMaterial(),
												Movimentacaoestoquetipo.SAIDA,
												veiculodespesaitem.getQuantidade().doubleValue(),
												veiculodespesaitem.getLocalarmazenagem(),
												veiculodespesa.getEmpresa(),
												movimentacaoestoqueorigem,
												Materialclasse.PRODUTO,
												veiculodespesaitem.getProjeto());
								
								movimentacaoestoque.setValorcusto(veiculodespesaitem.getMaterial().getValorcusto());
								if(SinedUtil.isRateioMovimentacaoEstoque()){
									if(veiculodespesaitem.getMaterial().getMaterialRateioEstoque() != null && veiculodespesaitem.getMaterial().getMaterialRateioEstoque().getContaGerencialConsumo() != null){
										movimentacaoestoque.setContaGerencial(veiculodespesaitem.getMaterial().getMaterialRateioEstoque().getContaGerencialConsumo());
									}
									movimentacaoestoque.setCentrocusto(veiculodespesaitem.getCentrocusto());
									movimentacaoestoque.setProjetoRateio(veiculodespesa.getProjeto());
									movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque));
								}
								movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
								movimentacaoestoqueService.salvarHisotirico(movimentacaoestoque, "Criado a partir da despesa de ve�culo " 
										+ (veiculodespesa.getCdveiculodespesa() != null ? SinedUtil.makeLinkHistorico(veiculodespesa.getCdveiculodespesa().toString(), "visualizarVeiculoDespesa") + ".": "."), MovimentacaoEstoqueAcao.CRIAR);
							}
							
							whereIn += veiculodespesa.getCdveiculodespesa() + ",";
						}						
					}
				}
			}
			
			if (whereIn.endsWith(",")){
				whereIn = whereIn.substring(0, whereIn.length()-1);
				veiculodespesaService.updateBaixado(whereIn);
				request.addMessage("Despesa(s) baixada(s) com sucesso.", MessageType.INFO);
			}else {
				request.addMessage("Para realizar a baixa de despesa (s), esta(s) tem que possuir item (ns) de consumo pr�prio.", MessageType.ERROR);
			}
		}		
		
		if ("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/veiculo/crud/Veiculodespesa?ACAO=consultar&cdveiculodespesa=" + selectedItens);
		}else {
			return new ModelAndView("redirect:/veiculo/crud/Veiculodespesa");			
		}
	}
}