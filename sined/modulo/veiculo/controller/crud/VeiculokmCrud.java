package br.com.linkcom.sined.modulo.veiculo.controller.crud;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculokm;
import br.com.linkcom.sined.geral.service.VeiculokmService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculokmFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/veiculo/crud/Veiculokm", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"veiculo", "dtentrada", "kmnovo", "descriptionProperty"})
public class VeiculokmCrud extends CrudControllerSined<VeiculokmFiltro, Veiculokm, Veiculokm>{
	
	private VeiculokmService veiculokmService;
	
	public void setVeiculokmService(VeiculokmService veiculokmService) {
		this.veiculokmService = veiculokmService;
	}
	
	@Override
	protected Veiculokm criar(WebRequestContext request, Veiculokm form)throws Exception {
		String cdveiculo = request.getParameter("cdveiculo");
		if(cdveiculo != null && !cdveiculo.equals("")){
			form.setVeiculo(new Veiculo(Integer.valueOf(cdveiculo)));
			return form;
		}
		return super.criar(request, form);
	}
	
	@Override
	protected ListagemResult<Veiculokm> getLista(WebRequestContext request,	VeiculokmFiltro filtro) {
		ListagemResult<Veiculokm> listagemResult = super.getLista(request, filtro);
		List<Veiculokm> lista = listagemResult.list();
		
		veiculokmService.adicionaKmAtualRegistros(lista);
		
		return listagemResult;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Veiculokm form) throws Exception {
		if(form.getCdveiculokm() == null || form.getCdveiculokm() == null)
			form.setDtentrada(new Date(System.currentTimeMillis()));
	}
	
}
