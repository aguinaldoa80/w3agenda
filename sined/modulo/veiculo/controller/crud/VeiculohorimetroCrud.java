package br.com.linkcom.sined.modulo.veiculo.controller.crud;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculohorimetro;
import br.com.linkcom.sined.geral.service.VeiculohorimetroService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculohorimetroFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/veiculo/crud/Veiculohorimetro", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"veiculo", "veiculo.placa", "veiculo.veiculomodelo", "horimetronovo", "descriptionProperty", "dtentrada"})
public class VeiculohorimetroCrud extends CrudControllerSined<VeiculohorimetroFiltro, Veiculohorimetro, Veiculohorimetro>{
	
private VeiculohorimetroService veiculohorimetroService;
	
	public void setVeiculohorimetroService(VeiculohorimetroService veiculohorimetroService) {
		this.veiculohorimetroService = veiculohorimetroService;
	}
	
	@Override
	protected Veiculohorimetro criar(WebRequestContext request, Veiculohorimetro form)throws Exception {
		String cdveiculo = request.getParameter("cdveiculo");
		if(cdveiculo != null && !cdveiculo.equals("")){
			form.setVeiculo(new Veiculo(Integer.valueOf(cdveiculo)));
			return form;
		}
		return super.criar(request, form);
	}
	
	@Override
	protected ListagemResult<Veiculohorimetro> getLista(WebRequestContext request,	VeiculohorimetroFiltro filtro) {
		ListagemResult<Veiculohorimetro> listagemResult = super.getLista(request, filtro);
		List<Veiculohorimetro> lista = listagemResult.list();
		
		veiculohorimetroService.adicionaHorimetroAtualRegistros(lista);
		
		return listagemResult;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Veiculohorimetro form) throws Exception {
		if(form.getCdveiculohorimetro() == null || form.getCdveiculohorimetro() == null)
			form.setDtentrada(new Date(System.currentTimeMillis()));
	}
}
