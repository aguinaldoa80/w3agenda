package br.com.linkcom.sined.modulo.veiculo.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculomodeloService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/veiculo/crud/Veiculo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"patrimonioitem", "empresa.nome", "ultimoProjeto.nome", "veiculomodelo", "colaborador", "placa", "dtentrada", "aux_veiculo.situacao"})
public class VeiculoCrud extends CrudControllerSined<VeiculoFiltro, Veiculo, Veiculo>{
	
	private PatrimonioitemService patrimonioitemService;
	private EmpresaService empresaService;
	private VeiculousoService veiculousoService;
	private VeiculoService veiculoService;
	private VeiculomodeloService veiculomodeloService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setVeiculomodeloService(VeiculomodeloService veiculomodeloService) {
		this.veiculomodeloService = veiculomodeloService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Veiculo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_VEICULO_PLACA")) {
				throw new SinedException("Placa de ve�culo j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void listagem(WebRequestContext request, VeiculoFiltro filtro) throws Exception {
		List<SituacaoVeiculo> listasituacao = new ArrayList<SituacaoVeiculo>();
		List<String> listaseguro = new ArrayList<String>();

		listasituacao.add(SituacaoVeiculo.EM_TRANSITO);
		listasituacao.add(SituacaoVeiculo.PARADO);
		listasituacao.add(SituacaoVeiculo.INATIVO);		
		
		listaseguro.add("Vencidos");
		listaseguro.add("A Vencer");
		
		request.setAttribute("listasituacao", listasituacao);
		request.setAttribute("listaseguro", listaseguro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Veiculo form)	throws Exception {
		request.setAttribute("listaPatrimonioVeiculo", patrimonioitemService.findComboTipoVeiculo(form.getPatrimonioitem()));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}
	
	@Override
	protected ListagemResult<Veiculo> getLista(WebRequestContext request, VeiculoFiltro filtro) {
		
		ListagemResult<Veiculo> listagemResult = super.getLista(request, filtro);
		List<Veiculo> listaVeiculo = listagemResult.list();
		
		for (Veiculo veiculo: listaVeiculo){
			Veiculouso ultimoUso = veiculousoService.getUltimoUso(veiculo);
			if (ultimoUso!=null){
				veiculo.setUltimoProjeto(ultimoUso.getProjeto());
			}
		}
		
		return listagemResult;
	}
	
	public ModelAndView ajaxBuscaInfVeiculo(WebRequestContext request, Veiculo veiculo){	
		if(veiculo.getCdveiculo() != null){
			veiculo = veiculoService.load(veiculo); 
		}
		
		JsonModelAndView json = new JsonModelAndView();
		if(veiculo != null){
			json.addObject("renavam", veiculo.getRenavam() != null ? veiculo.getRenavam() : "");
			json.addObject("uf", veiculo.getUf() != null ? "br.com.linkcom.sined.geral.bean.Uf[cduf="+veiculo.getUf().getCduf()+"]" : "");
			json.addObject("municipio", veiculo.getMunicipio() != null ? "br.com.linkcom.sined.geral.bean.Municipio[cdmunicipio="+veiculo.getMunicipio().getCdmunicipio()+"]" : "");
			json.addObject("placa", veiculo.getPlaca() != null ? veiculo.getPlaca() : "");
		}
		return json;
	}
	
	public ModelAndView makeAjaxModelo(WebRequestContext request, Veiculo veiculo){	
		Tipocontroleveiculo tipocontroleveiculo = Tipocontroleveiculo.HODOMETRO;
		if(veiculo.getVeiculomodelo() != null){
			Veiculomodelo veiculomodelo = veiculomodeloService.loadWithCategoria(veiculo.getVeiculomodelo());
			if(veiculomodelo != null && veiculomodelo.getCategoriaveiculo() != null && veiculomodelo.getCategoriaveiculo().getTipocontroleveiculo() != null){
				tipocontroleveiculo = veiculomodelo.getCategoriaveiculo().getTipocontroleveiculo();
			}
		}
		return new JsonModelAndView().addObject("tipocontroleveiculo", tipocontroleveiculo);
	}
}
