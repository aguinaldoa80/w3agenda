package br.com.linkcom.sined.modulo.veiculo.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.dao.VeiculoagendamentoDAO;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculoagendamentoService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoagendamentoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.ComprovanteagendamentoReport;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/veiculo/crud/Veiculoagendamento",authorizationModule = CrudAuthorizationModule.class)
@ExportCSV(fields = {"veiculo.colaborador", "veiculo.placa", "veiculo.prefixo", "dtprevista", "enviaemail"})
public class VeiculoagendamentoCrud extends CrudControllerSined<VeiculoagendamentoFiltro, Veiculoordemservico, Veiculoordemservico>{

	private VeiculoordemservicoService veiculoordemservicoService;
	private VeiculoagendamentoDAO veiculoagendamentoDAO;
	private VeiculousoService veiculousoService;
	private VeiculoService veiculoService;
	private VeiculoagendamentoService veiculoagendamentoService;
	
	public void setVeiculoagendamentoDAO(
			VeiculoagendamentoDAO veiculoagendamentoDAO) {
		this.veiculoagendamentoDAO = veiculoagendamentoDAO;
	}
	public void setVeiculoordemservicoService(
			VeiculoordemservicoService veiculoordemservicoService) {
		this.veiculoordemservicoService = veiculoordemservicoService;
	}	
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	
	public void setVeiculoagendamentoService(
			VeiculoagendamentoService veiculoagendamentoService) {
		this.veiculoagendamentoService = veiculoagendamentoService;
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, VeiculoagendamentoFiltro filtro) {
		return new ModelAndView("crud/veiculoagendamentoListagem","filtro",filtro);
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Veiculoordemservico form) {		
		form.setOrdemservicotipo(Ordemservicotipo.AGENDAMENTO);
		return new ModelAndView("crud/veiculoagendamentoEntrada", "filtro", form);
	}
	
	public ModelAndView comprovante(WebRequestContext requestContext, Veiculoordemservico ordemservico) throws Exception{
		ordemservico = veiculoordemservicoService.load(ordemservico);
		br.com.linkcom.sined.modulo.veiculo.controller.report.filter.VeiculoagendamentoFiltro filtro = new br.com.linkcom.sined.modulo.veiculo.controller.report.filter.VeiculoagendamentoFiltro();
		filtro.setVeiculo(ordemservico.getVeiculo());
		filtro.setDtagendamento(ordemservico.getDtprevista());
		filtro.setDtfim(ordemservico.getDtprevista());
		return NeoWeb.getObject(ComprovanteagendamentoReport.class).doGerar(requestContext, filtro);
	}
	
	/**
	 * Salva o bean
	 * - verifica se j� existe agendamento (data, veiculo, tipo (0))
	 * 
	 * @author Rafael Odon
	 * @throws Exception
	 */
	@Override
	protected void salvar(WebRequestContext request, Veiculoordemservico bean)
			throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if (DatabaseError.isKeyPresent(e, "ordemservico_idx"))						
				throw new Exception("Agendamento j� cadastrado no sistema.");			
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Veiculoordemservico bean) throws Exception {	
		
		String itens = request.getParameter("itenstodelete");
		
		if(itens != null && !itens.equals("")){			
			String[] codes = itens.split(",");
			for (String code : codes) {
				if(!"".equals(code) && code != null){
					Veiculoordemservico obj = new Veiculoordemservico();
					obj.setCdveiculoordemservico(Integer.parseInt(code));																									
					try{
						veiculoagendamentoDAO.delete(obj);
						request.addMessage("Registro(s) exclu�do(s) com sucesso.");
					}catch(SinedException e){
						throw e;
					}
				}
			}
		}else{
			throw new SinedException("Nenhum registro foi selecionado para exclus�o.");
		}
	}
	@Override
	protected void validateBean(Veiculoordemservico bean, BindException errors) {
		if(veiculoagendamentoService.existeAgendamentoData(bean))
			errors.reject("001", "Agendamento j� cadastrado no sistema para esse ve�culo.");
		if(this.veiculousoService.existeVeiculoAlocadoData(bean) || veiculoordemservicoService.existeInspecaoData(bean))
			errors.reject("002", "Este ve�culo j� est� alocado para uso nesta data.");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Veiculoordemservico form)	throws Exception {
		if(request.getBindException().hasErrors())
			form.setVeiculo(veiculoService.findVeiculo(form.getVeiculo()));
		
		super.entrada(request, form);
	}

}
