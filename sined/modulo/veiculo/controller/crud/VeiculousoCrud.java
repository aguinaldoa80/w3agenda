package br.com.linkcom.sined.modulo.veiculo.controller.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Categoriaveiculo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Tipokmajuste;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.VeiculoImplemento;
import br.com.linkcom.sined.geral.bean.Veiculohorimetro;
import br.com.linkcom.sined.geral.bean.Veiculokm;
import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.geral.bean.Veiculousoescolta;
import br.com.linkcom.sined.geral.bean.enumeration.Horimetrotipoajuste;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculouso;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.service.CategoriaveiculoService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculohorimetroService;
import br.com.linkcom.sined.geral.service.VeiculokmService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.geral.service.VeiculousotipoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculousoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/veiculo/crud/Veiculouso", authorizationModule=CrudAuthorizationModule.class)
public class VeiculousoCrud extends CrudControllerSined<VeiculousoFiltro, Veiculouso, Veiculouso>{
	
	private PatrimonioitemService patrimonioitemService;
	private VeiculoService veiculoService;
	private VeiculousotipoService veiculousotipoService;
	private VeiculousoService veiculousoService;
	private VeiculoordemservicoService veiculoordemservicoServico;
	private EmpresaService empresaService;
	private VeiculokmService veiculokmService;
	private CategoriaveiculoService categoriaveiculoService;
	private ContatoService contatoService;
	private VeiculohorimetroService veiculohorimetroService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	public void setPatrimonioitemService(
			PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setVeiculousotipoService(
			VeiculousotipoService veiculousotipoService) {
		this.veiculousotipoService = veiculousotipoService;
	}	
	public void setVeiculoordemservicoServico(
			VeiculoordemservicoService veiculoordemservicoServico) {
		this.veiculoordemservicoServico = veiculoordemservicoServico;
	}
	public void setCategoriaveiculoService(
			CategoriaveiculoService categoriaveiculoService) {
		this.categoriaveiculoService = categoriaveiculoService;
	}
	public void setVeiculokmService(VeiculokmService veiculokmService) {
		this.veiculokmService = veiculokmService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setVeiculohorimetroService(VeiculohorimetroService veiculohorimetroService) {
		this.veiculohorimetroService = veiculohorimetroService;
	}
	
	@Override
	protected void excluir(WebRequestContext request, Veiculouso bean) throws Exception {
		try {
			super.excluir(request, bean);
		} catch (SinedException e) {
			throw new SinedException("Uso do ve�culo n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@Override
	protected Veiculouso criar(WebRequestContext request, Veiculouso form)throws Exception {
		String cdveiculo = request.getParameter("cdveiculo");
		if(cdveiculo != null && !cdveiculo.equals("")){
			form.setVeiculo(veiculoService.findVeiculo(new Veiculo(Integer.valueOf(cdveiculo))));
			return form;
		}
		return super.criar(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, VeiculousoFiltro filtro) throws Exception {
		List<SituacaoVeiculouso> listasituacao = new ArrayList<SituacaoVeiculouso>();
		listasituacao.add(SituacaoVeiculouso.AGUARDANDO);
		listasituacao.add(SituacaoVeiculouso.EM_USO);
		listasituacao.add(SituacaoVeiculouso.FINALIZADO);
		listasituacao.add(SituacaoVeiculouso.CANCELADO);
		
		request.setAttribute("listasituacao", listasituacao);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Veiculouso form)	throws Exception {
		if(request.getBindException().hasErrors()){
			if(form.getVeiculo() != null && form.getVeiculo().getCdveiculo() != null && form.getVeiculo().getTipocontroleveiculo() == null){
				form.getVeiculo().setTipocontroleveiculo(veiculoService.load(form.getVeiculo(), "veiculo.cdveiculo, veiculo.tipocontroleveiculo").getTipocontroleveiculo());
			}
		}
		
		if(form.getAux_veiculouso() != null && form.getAux_veiculouso().getSituacao().equals(SituacaoVeiculouso.CANCELADO)){
			if(request.getParameter("ACAO") != null && request.getParameter("ACAO").equals("editar"))
				throw new SinedException("N�o � poss�vel editar um uso de ve�culo cancelado.");
			else
				request.setAttribute("showEditar", false);
		}else{
			request.setAttribute("showEditar", true);
		}
		
		if(form.getCdveiculouso() != null){
			if(form.getListaescolta() != null && form.getListaescolta().size() > 0){
				List<Municipio> listaMunicipio = new ArrayList<Municipio>();
				
				for (Veiculousoescolta veiculousoescolta : form.getListaescolta()) {
					if(veiculousoescolta != null && veiculousoescolta.getMunicipio() != null){
						listaMunicipio.add(veiculousoescolta.getMunicipio());
					}
				}
			
				request.setAttribute("listaMunicipioEscolta", listaMunicipio);
			}
			
			request.setAttribute("listaOrigem", veiculousoService.montaOrigem(form));
		}
		if (form.getVeiculo() != null && form.getVeiculo().getCdveiculo() != null) {
			Veiculo veiculo = veiculoService.findVeiculoForValidaItemCategoriaVencido(form.getVeiculo());

			List<Categoriaveiculo> listaCategorias = null;
			HashMap<Integer, List<Categoriaiteminspecao>> itemInspecaoCategoriaMap = new HashMap<Integer, List<Categoriaiteminspecao>>();

			if (veiculo != null) {
				if (veiculo.getVeiculomodelo()!=null && veiculo.getVeiculomodelo().getCategoriaveiculo() != null) {
					listaCategorias = categoriaveiculoService.loadCategoriaveiculoWithListaItemInspecao(veiculo.getVeiculomodelo().getCategoriaveiculo().getCdcategoriaveiculo().toString(), "", false);
				}
			}		

			if (SinedUtil.isListNotEmpty(listaCategorias)) {
				for (Categoriaveiculo categoria : listaCategorias) {
					itemInspecaoCategoriaMap.put(categoria.getCdcategoriaveiculo(), categoria.getListaCategoriaiteminspecao());
				}
			}	

			Boolean isItemInspecaoVencido = veiculoService.validaVeiculoItemInspecaoVencido(veiculo, itemInspecaoCategoriaMap, listaCategorias);


			if(isItemInspecaoVencido)
			{
				request.addMessage("Aten��o! Existem inspe��es que devem ser realizadas.");
			}
		}
		
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("contatoEscolhido", form.getContato() != null && form.getContato().getCdpessoa() != null ? 
				"br.com.linkcom.sined.geral.bean.Contato[cdpessoa=" + form.getContato().getCdpessoa() + "]" : "<null>");
	}
	/**
	 * M�todo ajax que carrega patrimonio item pela plaqueta
	 * 
	 * @see PatrimonioitemService#findPatrimonioItemByPlaqueta(String)
	 * @param request
	 * @param patrimonioitem
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView carregaPatrimonioItemAjax(WebRequestContext request, Patrimonioitem patrimonioitem){
		List<Patrimonioitem> listapatrimonio = patrimonioitemService.findPatrimonioItemAtivosByPlaqueta(patrimonioitem.getPlaqueta());
		String patrimonio = "";
		String patrimonio_label = "";
		if(listapatrimonio != null && listapatrimonio.size() > 0){
			Patrimonioitem patrimonioitem2 = listapatrimonio.get(0);
			patrimonio = patrimonioitem2.getCdpatrimonioitem()+"";
			patrimonio_label = new StringUtils().addScapesToDescription(patrimonioitem2.getDescricao());
		}
		
		return new JsonModelAndView()
					.addObject("patrimonio",patrimonio)
					.addObject("patrimonio_label", patrimonio_label);
	}
	
	/**
	 * M�todo ajax que carrega Km inicio por veiculo
	 * 
	 * @param request
	 * @param veiculo
	 * @return
	 * @author Jo�o Vitor
	 */
	public ModelAndView carregaInicioKmOuHorimetroVeiculoAjax(WebRequestContext request, Veiculouso veiculouso){
		Integer kmInicio = null;
		Double horimetroInicio = null;
		Tipocontroleveiculo tipocontroleveiculo = null;
		if (veiculouso != null && veiculouso.getVeiculo() != null) {
			Veiculo veiculo = veiculoService.load(veiculouso.getVeiculo(), "veiculo.cdveiculo, veiculo.tipocontroleveiculo");
			tipocontroleveiculo = veiculo.getTipocontroleveiculo();
			if(Tipocontroleveiculo.HODOMETRO.equals(tipocontroleveiculo)){
				Veiculokm veiculokmatual = veiculokmService.getKmAtualDoVeiculo(veiculouso.getVeiculo());
				if(veiculokmatual != null){
					kmInicio = veiculokmatual.getKmnovo() != null ? veiculokmatual.getKmnovo() : 0;
				}
			}else if(Tipocontroleveiculo.HORIMETRO.equals(tipocontroleveiculo)){
				Veiculohorimetro veiculohorimetroatual = veiculohorimetroService.getHorimetroAtualDoVeiculo(veiculouso.getVeiculo());
				if(veiculohorimetroatual != null){
					horimetroInicio = veiculohorimetroatual.getHorimetronovo() != null ? veiculohorimetroatual.getHorimetronovo() : 0d;
				}
			}
		}

		return new JsonModelAndView()
					.addObject("kmInicio",kmInicio)
					.addObject("horimetroInicio",horimetroInicio)
					.addObject("tipocontroleveiculo", tipocontroleveiculo);
	}
	
	/**
	 * Fun��o ajax que retorna a escala do veiculo
	 * @param request
	 * @param veiculouso
	 * @author Tom�s Rabelo
	 */
	public void getEscalaVeiculoAjax(WebRequestContext request, Veiculouso veiculouso){
		String escala = null;
		if(veiculouso != null && veiculouso.getVeiculo() != null){
			Veiculo veiculo = veiculoService.getEscalaVeiculo(veiculouso.getVeiculo());
			if(veiculo != null)
				escala = veiculo.getEscala().getNome();
		}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(escala == null ? "" : escala.toString());
	}

	/**
	 * Fun��o ajax que retorna a escala do veiculo
	 * @param request
	 * @param veiculouso
	 * @author Tom�s Rabelo
	 */
	public void isVeiculoUsoTipoEscoltaAjax(WebRequestContext request, Veiculouso veiculouso){
		Boolean tipoEscolta = Boolean.FALSE;
		if(veiculouso != null && veiculouso.getVeiculousotipo() != null){
			tipoEscolta = veiculousotipoService.isVeiculoUsoTipoEscolta(veiculouso.getVeiculousotipo());
		}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(tipoEscolta.toString());
	}
	
	public ModelAndView cancelar(WebRequestContext request){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		try {
			veiculousoService.doCancelar(whereIn);
			request.addMessage("Uso(s) de ve�culo cancelado(s) com sucesso.");
		} catch (Exception e) {
			request.addError(e.getMessage());
		}
		
		if("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/veiculo/crud/Veiculouso?ACAO=consultar&cdveiculouso=" + whereIn);
		} else {
			return new ModelAndView("redirect:/veiculo/crud/Veiculouso");
		}
	}
	
	@Override
	protected void validateBean(Veiculouso bean, BindException errors) {
		//if(bean.getEscalahorario() != null && bean.getEscalahorario().getCdescalahorario() != null)
			if(veiculousoService.existeVeiculoAlocadoMesmoHorarioData(bean))
				errors.reject("001", "J� existe um uso cadastrado para este ve�culo.");
			else if(this.veiculoordemservicoServico.existeOrdemServicoMesmaData(bean))
				errors.reject("002", "H� manuten��es ou inspe��es agendadas dentro deste intervalo de uso.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Veiculouso bean) throws Exception {
		if (bean != null && bean.getVeiculo() != null) {
			Veiculo veiculo = veiculoService.load(bean.getVeiculo(), "veiculo.cdveiculo, veiculo.tipocontroleveiculo");
			if(Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo())){
				Veiculokm veiculokmatual = veiculokmService.getKmAtualDoVeiculo(bean.getVeiculo());
				if (veiculokmatual == null || (veiculokmatual.getKmnovo() != null && bean.getKmfim() != null && bean.getDtiniciouso() != null
						&& veiculokmatual.getKmnovo() < bean.getKmfim())) {
					Veiculokm veiculokm = new Veiculokm();
					veiculokm.setVeiculo(bean.getVeiculo());
					veiculokm.setKmnovo(bean.getKmfim());
					veiculokm.setMotivo("Uso de Ve�culo");
					veiculokm.setTipokmajuste(Tipokmajuste.OUTRO);
					veiculokm.setDtentrada(bean.getDtiniciouso());
					veiculokmService.saveOrUpdate(veiculokm);
				}
			}else if(Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo())){
				Veiculohorimetro veiculohorimetroatual = veiculohorimetroService.getHorimetroAtualDoVeiculo(bean.getVeiculo());
				if (veiculohorimetroatual == null || (veiculohorimetroatual.getHorimetronovo() != null 
						&& bean.getHorimetrofim() != null && bean.getDtiniciouso() != null 
						&& veiculohorimetroatual.getHorimetronovo() < bean.getHorimetrofim())) {
					Veiculohorimetro veiculohorimetro = new Veiculohorimetro();
					veiculohorimetro.setVeiculo(bean.getVeiculo());
					veiculohorimetro.setHorimetronovo(bean.getHorimetrofim());
					veiculohorimetro.setMotivo("Uso de Ve�culo");
					veiculohorimetro.setHorimetrotipoajuste(Horimetrotipoajuste.OUTROS);
					veiculohorimetro.setDtentrada(bean.getDtiniciouso());
					veiculohorimetroService.saveOrUpdate(veiculohorimetro);
				}
			}
			if(bean.getAux_veiculouso() != null && bean.getDtfimuso() != null){
				bean.getAux_veiculouso().setSituacao(SituacaoVeiculouso.FINALIZADO);
			}
		}		
		super.salvar(request, bean);
		if(bean.getListaVeiculoImplemento() != null){
			for(VeiculoImplemento veiculoimplemento : bean.getListaVeiculoImplemento()){
				if(veiculoimplemento.getKmfim() != null){
					Veiculokm veiculokmatual = veiculokmService.getKmAtualDoVeiculo(veiculoimplemento.getImplemento());
					if (veiculokmatual == null || (veiculokmatual.getKmnovo() != null 
							&& veiculokmatual.getKmnovo() < veiculoimplemento.getKmfim())) {
						Veiculokm veiculokm = new Veiculokm();
						veiculokm.setVeiculo(veiculoimplemento.getImplemento());
						veiculokm.setKmnovo(veiculoimplemento.getKmfim());
						veiculokm.setMotivo("Uso de Implemento");
						veiculokm.setTipokmajuste(Tipokmajuste.OUTRO);
						veiculokm.setDtentrada(SinedDateUtils.currentDate());
						veiculokmService.saveOrUpdate(veiculokm);
					}
				}
				if(veiculoimplemento.getHorimetrofim() != null){
					Veiculohorimetro veiculohorimetroatual = veiculohorimetroService.getHorimetroAtualDoVeiculo(veiculoimplemento.getImplemento());
					if (veiculohorimetroatual == null || (veiculohorimetroatual.getHorimetronovo() != null 
							&& veiculohorimetroatual.getHorimetronovo() < veiculoimplemento.getHorimetrofim())) {
						Veiculohorimetro veiculohorimetro = new Veiculohorimetro();
						veiculohorimetro.setVeiculo(veiculoimplemento.getImplemento());
						veiculohorimetro.setHorimetronovo(veiculoimplemento.getHorimetrofim());
						veiculohorimetro.setMotivo("Uso de Implemento");
						veiculohorimetro.setHorimetrotipoajuste(Horimetrotipoajuste.OUTROS);
						veiculohorimetro.setDtentrada(SinedDateUtils.currentDate());
						veiculohorimetroService.saveOrUpdate(veiculohorimetro);
					}
				}
			}
		}
	}
	
	/**
	* M�todo que carrega os contatos de acordo com o cliente
	*
	* @param request
	* @param cliente
	* @return
	* @since 04/11/2016
	* @author Luiz Fernando
	*/
	public ModelAndView comboBoxContato(WebRequestContext request, Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		List<Contato> lista = contatoService.findByPessoa(cliente);
		return new JsonModelAndView().addObject("lista", lista);
	}
}
	

