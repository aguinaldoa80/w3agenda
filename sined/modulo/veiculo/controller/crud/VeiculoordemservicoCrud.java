package br.com.linkcom.sined.modulo.veiculo.controller.crud;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.service.CategoriaiteminspecaoService;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoitemService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoordemservicoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/veiculo/crud/Veiculoordemservico",authorizationModule = CrudAuthorizationModule.class)
@ExportCSV(fields = {"veiculo.placa", "veiculo.prefixo", "dtrealizada", "veiculo.colaborador.nome"})
public class VeiculoordemservicoCrud extends CrudControllerSined<VeiculoordemservicoFiltro, Veiculoordemservico, Veiculoordemservico>{

	private VeiculoordemservicoitemService veiculoordemservicoitemService;
	private VeiculoordemservicoService veiculoordemservicoService;
	private VeiculoService veiculoService;
	private CategoriaiteminspecaoService categoriaiteminspecaoService;
	private VeiculousoService veiculousoService;

	public void setCategoriaiteminspecaoService(CategoriaiteminspecaoService categoriaiteminspecaoService) {
		this.categoriaiteminspecaoService = categoriaiteminspecaoService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setVeiculoordemservicoitemService(
			VeiculoordemservicoitemService veiculoordemservicoitemService) {
		this.veiculoordemservicoitemService = veiculoordemservicoitemService;
	}
	public void setVeiculoordemservicoService(
			VeiculoordemservicoService veiculoordemservicoService) {
		this.veiculoordemservicoService = veiculoordemservicoService;
	}
	
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	@Override
	protected void excluir(WebRequestContext request, Veiculoordemservico bean) throws Exception {
		try {
			super.excluir(request, bean);
		} catch (SinedException e) {
			throw new SinedException("Inspe��o n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@Override
	protected Veiculoordemservico criar(WebRequestContext request, Veiculoordemservico form) throws Exception {
		String whereInVeiculouso = request.getParameter("whereInVeiuculouso");
		if(StringUtils.isNotBlank(whereInVeiculouso)){
			if(whereInVeiculouso.split(",").length > 1){
				throw new SinedException("� permitido somente um item por vez.");
			}
			Veiculouso veiculouso = new Veiculouso(Integer.parseInt(request.getParameter("whereInVeiuculouso")));
			return veiculoordemservicoService.criaVeiculoordemservicoByVeiculouso(veiculouso);
		}
		return super.criar(request, form);
	}

	/**
	 * @author Biharck, Pedro Gon�alves
	 * <B>M�todo respons�vel pelo tratamento lista de ordemservico, onde<br>
	 * carrega uma lista de ordem de servico item atualizada</b>
	 * @param request
	 * @param form
	 */
	@Override
	protected void entrada(WebRequestContext request, Veiculoordemservico form) throws Exception {
		if(request.getBindException().hasErrors())
			form.setVeiculo(veiculoService.findVeiculo(form.getVeiculo()));
		
		if(form.getCdveiculoordemservico() != null){
			boolean editar = veiculoordemservicoService.inspecaoPossuiManutencoesEmAberto(form);
			if(!editar && request.getParameter("ACAO") != null && request.getParameter("ACAO").equals("editar"))
				throw new SinedException("N�o � possivel editar esta inspe��o. Os itens de manuten��o foram realizados.");
			request.setAttribute("edicao", editar);
		}
		
		if (form.getCdveiculoordemservico() == null) {
			veiculoordemservicoService.calculaDataPadrao(form);
		}else{
			Veiculoordemservico os = veiculoordemservicoService.load(form);	
			form.setKm(os!=null&&os.getKm()!=null?os.getKm():null);
		} 
	}
	
	@Override
	protected Veiculoordemservico carregar(WebRequestContext request, Veiculoordemservico bean) throws Exception {
		Veiculoordemservico veiculoordemservico = super.carregar(request, bean);
		if(veiculoordemservico.getCdveiculoordemservico() != null){
			List<Veiculoordemservicoitem> listaOrdemServicoItem = veiculoordemservico.getListaVeiculoordemservicoitem();
			if(listaOrdemServicoItem == null || listaOrdemServicoItem.isEmpty())
				veiculoordemservico.setItensListaOk(Boolean.TRUE);
			else {
				veiculoordemservico.setItensListaOk(Boolean.FALSE);
				veiculoordemservico.setDiasPrevistos(veiculoordemservicoService.calculaDias(veiculoordemservico).intValue());
			}
			
		} 
		return veiculoordemservico;
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Veiculoordemservico form) throws CrudException {
		if(form.getFromVeiculouso() != null && form.getFromVeiculouso()){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Inspe��o criado com sucesso.");
			return new ModelAndView("redirect:/veiculo/crud/Veiculouso");
		}
		
		return super.doSalvar(request, form);
	}

	/**
	 * @author Biharck
	 * <b>M�todo onde � tratato dados ao salvar e em criar uma ordem de servico inspe��o ou ordem de servico de manuten��o</br>
	 * dependendo da sele��o de alguns campos</b>
	 * @param request
	 * @param bean
	 */
	@Override
	protected void salvar(WebRequestContext request, Veiculoordemservico bean) throws Exception {
		
		//lista de itens de inspecao se true = pendente false = ok
		List<Veiculoordemservicoitem> listaOrdemServicoItem = new ArrayList<Veiculoordemservicoitem>();
		
		List<Veiculoordemservicoitem> listaOrdemServicoItem2 = new ArrayList<Veiculoordemservicoitem>(); 
		
		if(bean.getListaVeiculoordemservicoitem() != null && !bean.getListaVeiculoordemservicoitem().isEmpty() ) {
			listaOrdemServicoItem2 = bean.getListaVeiculoordemservicoitem();
			for (Veiculoordemservicoitem ordemservicoitem2 : listaOrdemServicoItem2) {
				if(ordemservicoitem2.getStatus() != null && ordemservicoitem2.getStatus())
					listaOrdemServicoItem.add(ordemservicoitem2);
			}
		}
		
		
		//gera a mensagem da quantidade de itens.
		boolean test = qtdElementosInspecao(request, bean, listaOrdemServicoItem);
		
		bean.setOrdemservicotipo(Ordemservicotipo.INSPECAO);
		
		if(bean.getItensListaOk()){
			bean.setListaVeiculoordemservicoitem(null);
		}

		try {
			veiculoordemservicoService.saveOrUpdate(bean);
			
			if(test){
				veiculoordemservicoService.generateOrdemManutencao(bean);
			}
			
		}catch (DataIntegrityViolationException e) {
			throw new SinedException("N�o foi poss�vel criar uma inspe��o para um mesmo ve�culo no mesmo dia.");	
		}
		
	}



	/**
	 * <b> M�todo respons�vel em verificar a quantidade de itens de inspe��o que foram geradas<br>
	 * manuten��es
	 * @param request
	 * @param bean
	 * @param listaOrdemServicoItem
	 * @return quantidade de itens que foram geradaos manuten��es
	 * @author Biharck
	 */
	private boolean qtdElementosInspecao(WebRequestContext request, Veiculoordemservico bean, List<Veiculoordemservicoitem> listaOrdemServicoItem) {
		Integer qtdElementos =0;
		qtdElementos = listaOrdemServicoItem.size();
		boolean test = false;
		if (qtdElementos>0) {
			test = true;
			request.addMessage("Foi(ram) gerada(s) " + qtdElementos + " item(ns) pendente(s) neste lan�amento de inspe��o.");
		} else {
			request.addMessage("N�o foram geradas manuten��es neste lan�amento de inspe��o.");
		}
		return test;
	}


	/**
	 * <b>M�todo respons�vel em listar os dados da lista de item inspe��o</b>
	 * @param request
	 * @param bean
	 * @throws Exception
	 * @author Biharck
	 */
	public ModelAndView listar(WebRequestContext request, Veiculoordemservico bean) throws Exception {

		List<Veiculoordemservicoitem> listaOrdemServicoitemMarcados = null;
		List<Veiculoordemservicoitem> listaOrdemServicoitem = new ArrayList<Veiculoordemservicoitem>();
		
		//Caso esteja editando o registro pela primeira vez, carregar a lista com os �tens.
		if("true".equals(request.getParameter("editar"))){
			listaOrdemServicoitemMarcados = veiculoordemservicoitemService.carregarOSIMarcados(bean);
		}
		
		//Carrega o ve�culo e todas as categorias itens inspe��o.
		Veiculo veiculo = veiculoService.findVeiculo(bean.getVeiculo());
		
		//bean.getTipoinspecao() - Caso seja true, dever� filtrar por itens visuais.
		
		if(veiculo.getVeiculomodelo() != null){
			List<Categoriaiteminspecao> listaCategoriaItemInspecao = categoriaiteminspecaoService.loadCategoriaItemInspecao(veiculo.getVeiculomodelo(),bean.getTipoinspecao());
			for (Categoriaiteminspecao categoriaiteminspecao : listaCategoriaItemInspecao) {
				Veiculoordemservicoitem ordemservicoitem = new Veiculoordemservicoitem();
				ordemservicoitem.setInspecaoitem(categoriaiteminspecao.getInspecaoitem());
				
				//Vai carregar os �tens em caso de edi��o.
				if(listaOrdemServicoitemMarcados != null){
					int indexOf = listaOrdemServicoitemMarcados.indexOf(categoriaiteminspecao.getInspecaoitem());
					if(indexOf >= 0) {
						//Foi encontrado
						ordemservicoitem = listaOrdemServicoitemMarcados.get(indexOf);
					}
						
				}
				
				listaOrdemServicoitem.add(ordemservicoitem);
			}
		}
		
		return new ModelAndView("direct:crud/detalheOrdemServicoEditar", "listaCategoriaItemInspecao", listaOrdemServicoitem);
	}


	/**
	 * <b>M�todo respons�vel em tratar soma em datas</b>
	 * @param context
	 * @param form Ordemservico
	 * @author Biharck
	 * @throws ParseException 
	 */
	public void validaData(WebRequestContext context, Veiculoordemservico form) throws ParseException{
		View view = View.getCurrent();
		String dataFinal = veiculoordemservicoService.validaData(context, form, Boolean.TRUE, Long.valueOf(10));
		view.println("var dtrealizada = '"+ dataFinal +"';");
	}

	/**
	 * <b>M�todo respons�vel em tratar as opera��es aritm�ticas com datas<br>
	 * calculando assim os dias �teis em rela��o a uma data X</b>
	 * @author Biharck
	 * @param context
	 * @param ordemservico
	 */
	public void calculaDias(WebRequestContext context, Veiculoordemservico ordemservico){
		View view = View.getCurrent();
		long diferencaEntraDatas = veiculoordemservicoService.calculaDias(ordemservico);
		view.println("var dataCalculada = '"+ diferencaEntraDatas +"';");
	}

	/**
	 * <b>M�todo respons�vel em calcular a data padr�o a partir da data de inspe��o selecionada pelo usu�rio</b>
	 * @param context
	 * @param ordemservico
	 * @author Biharck
	 * @throws ParseException 
	 */
	public void alteraDataInspecao(WebRequestContext context, Veiculoordemservico ordemservico) throws ParseException{
		View view = View.getCurrent();
		Long diferencaEntraDatas = Long.valueOf(10);
		String  dataFinal = veiculoordemservicoService.validaData(context, ordemservico, Boolean.FALSE, diferencaEntraDatas);
		view.println("var dtalterada = '"+ dataFinal +"';");
	}
	
	public void buscarkmHorimetro(WebRequestContext context, Veiculoordemservico ordemservico){
		View view = View.getCurrent();
		Veiculo veiculo = ordemservico.getVeiculo() != null ? veiculoService.load(ordemservico.getVeiculo(), "veiculo.cdveiculo, veiculo.tipocontroleveiculo") : null;
		Long km = null;
		Double horimetro = null;
		Tipocontroleveiculo tipocontroleveiculo = null;
		if(veiculo != null){
			tipocontroleveiculo = veiculo.getTipocontroleveiculo();
			if(Tipocontroleveiculo.HODOMETRO.equals(tipocontroleveiculo)){
				km = veiculoService.loadForAgendamento(ordemservico.getVeiculo()).getVwveiculo().getKmatual().longValue();
			}else if(Tipocontroleveiculo.HORIMETRO.equals(tipocontroleveiculo)){
				horimetro = veiculoService.loadForAgendamento(ordemservico.getVeiculo()).getVwveiculo().getHorimetroatual();
			}
		}
		view.println("var kmatual = '"+ (km != null ? km : "") +"';");
		view.println("var horimetroatual = '"+ (horimetro != null ? SinedUtil.descriptionDecimal(horimetro) : "") +"';");
		view.println("var tipocontroleveiculo = '"+ (tipocontroleveiculo != null ? tipocontroleveiculo.getNome() : "") +"';");
	}
	
	@Override
	protected void validateBean(Veiculoordemservico bean, BindException errors) {
		if(this.veiculousoService.existeVeiculoAlocadoData(bean))
			errors.reject("001", "Este ve�culo j� est� alocado para uso nesta data.");
		if(this.veiculoordemservicoService.existeInspecaoData(bean))
			errors.reject("002", "J� existe inspe��o cadastrada nesta data para este ve�culo.");
	}
}