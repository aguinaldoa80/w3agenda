package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.geral.bean.enumeration.EnumSituacaomanutencao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculomanutencaoFiltro extends FiltroListagemSined {

	protected Colaborador colaborador;
	protected String placa;
	protected String prefixo;
	protected EnumSituacaomanutencao enumSituacaomanutencao;
	
	protected List<Veiculoordemservicoitem> listaOrdemservicoitem = new ArrayList<Veiculoordemservicoitem>();
	
	
	public String getPrefixo() {
		return prefixo;
	}
	public String getPlaca() {
		return placa;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Situa��o da manuten��o")
	public EnumSituacaomanutencao getEnumSituacaomanutencao() {
		return enumSituacaomanutencao;
	}
	
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setEnumSituacaomanutencao(EnumSituacaomanutencao enumSituacaomanutencao) {
		this.enumSituacaomanutencao = enumSituacaomanutencao;
	}
	
	public List<Veiculoordemservicoitem> getListaOrdemservicoitem() {
		return listaOrdemservicoitem;
	}
	
	public void setListaOrdemservicoitem(
			List<Veiculoordemservicoitem> listaOrdemservicoitem) {
		this.listaOrdemservicoitem = listaOrdemservicoitem;
	}
}

