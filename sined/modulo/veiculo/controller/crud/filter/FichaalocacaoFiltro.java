package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FichaalocacaoFiltro extends FiltroListagemSined{

	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected Cliente cliente;
	protected Veiculo veiculo;
	
	public FichaalocacaoFiltro(){
		this.dtinicio = new Date(System.currentTimeMillis());
		this.dtfim = new Date(System.currentTimeMillis());
	}
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Data de in�cio")
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data de fim")
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	
	
}
