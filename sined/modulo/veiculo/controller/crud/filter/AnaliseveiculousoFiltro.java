package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.util.Calendar;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AnaliseveiculousoFiltro extends FiltroListagemSined{

	protected Empresa empresa;
	protected Integer ano;
	protected Veiculo veiculo;
	
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(SinedDateUtils.currentDate());
		this.ano = calendar.get(Calendar.YEAR);
	}
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@MaxLength(4)
	@MinLength(4)
	public Integer getAno() {
		return ano;
	}
	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
}
