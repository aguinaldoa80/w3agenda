package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculoordemservicoFiltro extends FiltroListagemSined {

	protected String placa;
	protected Date dataInicio;
	protected Date dataFim;
	protected Boolean ultimaInspecao;

	@DisplayName("�ltima Inspe��o")
	public Boolean getUltimaInspecao() {
		return ultimaInspecao;
	}
	@DisplayName("Data In�cio")
	public Date getDataInicio() {
		return dataInicio;
	}
	@DisplayName("Data Fim")
	public Date getDataFim() {
		return dataFim;
	}
	@MaxLength(8)
	public String getPlaca() {
		return placa;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setUltimaInspecao(Boolean ultimaInspecao) {
		this.ultimaInspecao = ultimaInspecao;
	}
}
