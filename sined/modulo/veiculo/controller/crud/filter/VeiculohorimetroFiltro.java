package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.Horimetrotipoajuste;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculohorimetroFiltro extends FiltroListagemSined{

	protected Veiculo veiculo;
	protected String prefixo;
	protected String placa;
	protected Horimetrotipoajuste horimetrotipoajuste;

	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public String getPrefixo() {
		return prefixo;
	}
	public String getPlaca() {
		return placa;
	}
	@DisplayName("Tipo Ajuste")
	public Horimetrotipoajuste getHorimetrotipoajuste() {
		return horimetrotipoajuste;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setHorimetrotipoajuste(Horimetrotipoajuste horimetrotipoajuste) {
		this.horimetrotipoajuste = horimetrotipoajuste;
	}
}
