package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Escalahorario;

public class Veiculousoapoio {

	public static final int QTDE_DIAS_TABELA = 7;
	
	protected Escalahorario escalahorario;
	protected List<Veiculousoitemapoio> listaveiculousoitemapoio = new ListSet<Veiculousoitemapoio>(Veiculousoitemapoio.class);
	
	protected String descriptionHour;
	
	public Veiculousoapoio(){
	}

	public Veiculousoapoio( Escalahorario escalahorario){
		this.escalahorario = escalahorario;
	}
	
	public Escalahorario getEscalahorario() {
		return escalahorario;
	}
	public List<Veiculousoitemapoio> getListaveiculousoitemapoio() {
		return listaveiculousoitemapoio;
	}
	public String getDescriptionHour() {
		return escalahorario.getHorainicio() + " - " + escalahorario.getHorafim();
	}

	public void setEscalahorario(Escalahorario escalahorario) {
		this.escalahorario = escalahorario;
	}
	public void setListaveiculousoitemapoio(
			List<Veiculousoitemapoio> listaveiculousoitemapoio) {
		this.listaveiculousoitemapoio = listaveiculousoitemapoio;
	}
	public void setDescriptionHour(String descriptionHour) {
		this.descriptionHour = descriptionHour;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Veiculousoapoio){
			Escalahorario escalahorario = ((Veiculousoapoio)obj).getEscalahorario();
			return escalahorario.getHorainicio().getTime() == this.escalahorario.getHorainicio().getTime() && escalahorario.getHorafim().getTime() == this.escalahorario.getHorafim().getTime();
		}
		return super.equals(obj);
	}
	
}
