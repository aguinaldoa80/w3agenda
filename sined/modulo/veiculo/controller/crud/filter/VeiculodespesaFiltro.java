package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculodespesatipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculodespesaFiltro extends FiltroListagemSined{

	protected Veiculo veiculo;
	protected Empresa empresa;
	protected Date dtentradade;
	protected Date dtentradaate;
	protected Colaborador colaborador;
	protected String placa;
	protected Boolean baixado;
	protected Boolean faturado;
	
	protected List<Veiculodespesatipo> listadespesatipo;
	
	public VeiculodespesaFiltro(){
		if(this.getListadespesatipo() == null){
			this.setListadespesatipo(new ArrayList<Veiculodespesatipo>());
			this.getListadespesatipo().add(Veiculodespesatipo.ABASTECIMENTO);
			this.getListadespesatipo().add(Veiculodespesatipo.IMPOSTO);
			this.getListadespesatipo().add(Veiculodespesatipo.INFRACAO);
			this.getListadespesatipo().add(Veiculodespesatipo.MANUTENCAO);
			this.getListadespesatipo().add(Veiculodespesatipo.PNEU);
		}
	}
	
	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("De")
	public Date getDtentradade() {
		return dtentradade;
	}
	@DisplayName("At�")
	public Date getDtentradaate() {
		return dtentradaate;
	}
	@DisplayName("Tipo de despesa")
	public List<Veiculodespesatipo> getListadespesatipo() {
		return listadespesatipo;
	}
	@DisplayName("Colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Placa")
	public String getPlaca() {
		return placa;
	}
	@DisplayName("Baixado")
	public Boolean getBaixado() {
		return baixado;
	}
	@DisplayName("Faturado")
	public Boolean getFaturado() {
		return faturado;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtentradaate(Date dtentradaate) {
		this.dtentradaate = dtentradaate;
	}
	public void setDtentradade(Date dtentradade) {
		this.dtentradade = dtentradade;
	}
	public void setListadespesatipo(List<Veiculodespesatipo> listadespesatipo) {
		this.listadespesatipo = listadespesatipo;
	}
	public void setBaixado(Boolean baixado) {
		this.baixado = baixado;
	}
	public void setFaturado(Boolean faturado) {
		this.faturado = faturado;
	}
}