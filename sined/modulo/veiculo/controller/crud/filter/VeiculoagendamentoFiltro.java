package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculoagendamentoFiltro extends FiltroListagemSined{
	
	protected String placa;
	protected Date dataInicio;
	protected Date dataFim;

	//getters
	public String getPlaca() {
		return placa;
	}
	
	@DisplayName("Data �nicio")
	public Date getDataInicio() {
		return dataInicio;
	}
	
	@DisplayName("Data Fim")
	public Date getDataFim() {
		return dataFim;
	}
	
	//setters	
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
}
