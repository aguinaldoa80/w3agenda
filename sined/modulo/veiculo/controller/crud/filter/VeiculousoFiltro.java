package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculousotipo;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculouso;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculousoFiltro extends FiltroListagemSined{

//	protected Timestamp datade;
//	protected Timestamp datate;
	protected Veiculo veiculo;
	protected Veiculousotipo veiculousotipo;
	protected Cliente cliente;
	protected Empresa empresa;
	protected Localarmazenagem localarmazenagem;
	protected Projeto projeto;
	protected List<SituacaoVeiculouso> listaveiculo;
	protected String placa;
	protected Date dtiniciouso;
	protected Date dtfimuso;
	
	public VeiculousoFiltro(){
		if(this.getListaveiculo() == null){
			this.setListaveiculo(new ArrayList<SituacaoVeiculouso>());
			this.getListaveiculo().add(SituacaoVeiculouso.EM_USO);
			this.getListaveiculo().add(SituacaoVeiculouso.FINALIZADO);
			this.getListaveiculo().add(SituacaoVeiculouso.AGUARDANDO);
		}
	}
	
	@DisplayName("Situa��o")
	public List<SituacaoVeiculouso> getListaveiculo() {
		return listaveiculo;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Local de destino")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
//	public Timestamp getDatade() {
//		return datade;
//	}
//	public Timestamp getDatate() {
//		return datate;
//	}
	@DisplayName("Tipo de uso")
	public Veiculousotipo getVeiculousotipo() {
		return veiculousotipo;
	}
	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@DisplayName("Placa")
	public String getPlaca() {
		return placa;
	}
	@DisplayName("De")
	public Date getDtiniciouso() {
		return dtiniciouso;
	}
	@DisplayName("At�")
	public Date getDtfimuso() {
		return dtfimuso;
	}

	public void setDtiniciouso(Date dtiniciouso) {
		this.dtiniciouso = dtiniciouso;
	}

	public void setDtfimuso(Date dtfimuso) {
		this.dtfimuso = dtfimuso;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setListaveiculo(List<SituacaoVeiculouso> listaveiculo) {
		this.listaveiculo = listaveiculo;
	}
//	public void setDatade(Timestamp datade) {
//		this.datade = datade;
//	}
//	public void setDatate(Timestamp datate) {
//		this.datate = datate;
//	}
	public void setVeiculousotipo(Veiculousotipo veiculousotipo) {
		this.veiculousotipo = veiculousotipo;
	}

	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
}
