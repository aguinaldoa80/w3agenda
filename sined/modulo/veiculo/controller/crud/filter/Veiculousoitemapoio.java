package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;


import java.sql.Date;
import java.text.SimpleDateFormat;

import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.util.SinedDateUtils;

public class Veiculousoitemapoio {

	protected Date data;
	protected String dataAux;
	protected Veiculouso veiculouso;
	
	public Veiculousoitemapoio(){
	}

	public Veiculousoitemapoio(Date data){
		this.data = data;
	}

	public Veiculousoitemapoio(Date data, Veiculouso veiculouso){
		this.data = data;
		this.dataAux = new SimpleDateFormat("dd/MM/yy").format(data)+" - "+SinedDateUtils.stringDayOfWeek(data);
		this.veiculouso = veiculouso;
	}

	public Date getData() {
		return data;
	}
	public Veiculouso getVeiculouso() {
		return veiculouso;
	}
	public String getDataAux() {
		return dataAux;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	public void setVeiculouso(Veiculouso veiculouso) {
		this.veiculouso = veiculouso;
	}
	public void setDataAux(String dataAux) {
		this.dataAux = dataAux;
	}
	
}
