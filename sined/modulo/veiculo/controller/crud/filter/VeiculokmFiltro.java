package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Tipokmajuste;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculokmFiltro extends FiltroListagemSined {
	
	protected Tipokmajuste tipokmajuste;
	protected String prefixo;
	protected String placa;
	protected Veiculomodelo veiculomodelo;
	protected Veiculo veiculo;
	
	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	@DisplayName("Tipo ajuste")
	public Tipokmajuste getTipokmajuste() {
		return tipokmajuste;
	}
	
		public String getPrefixo() {
		return prefixo;
	}

	public String getPlaca() {
		return placa;
	}

	@DisplayName("Modelo")
	public Veiculomodelo getVeiculomodelo() {
		return veiculomodelo;
	}

	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public void setVeiculomodelo(Veiculomodelo veiculomodelo) {
		this.veiculomodelo = veiculomodelo;
	}

	public void setTipokmajuste(Tipokmajuste tipokmajuste) {
		this.tipokmajuste = tipokmajuste;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
}
