package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculoFiltro extends FiltroListagemSined{

	protected String placa;
	protected Empresa empresa;
	protected Projeto projeto;
	protected Veiculomodelo veiculomodelo;
	protected Fornecedor fornecedor;
	protected Date dtvencimentoinicio;
	protected Date dtvencimentofim;
	protected String seguro;
	
	protected List<SituacaoVeiculo> listaveiculo;
	
	public VeiculoFiltro(){
		if(this.getListaveiculo() == null){
			this.setListaveiculo(new ArrayList<SituacaoVeiculo>());
			this.getListaveiculo().add(SituacaoVeiculo.EM_TRANSITO);
			this.getListaveiculo().add(SituacaoVeiculo.INATIVO);
			this.getListaveiculo().add(SituacaoVeiculo.PARADO);
		}
	}
	
	@MaxLength(10)
	public String getPlaca() {
		return placa;
	}
	@DisplayName("Situa��o")
	public List<SituacaoVeiculo> getListaveiculo() {
		return listaveiculo;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Modelo")
	public Veiculomodelo getVeiculomodelo() {
		return veiculomodelo;
	}
	public void setVeiculomodelo(Veiculomodelo veiculomodelo) {
		this.veiculomodelo = veiculomodelo;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setListaveiculo(List<SituacaoVeiculo> listaveiculo) {
		this.listaveiculo = listaveiculo;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Date getDtvencimentoinicio() {
		return dtvencimentoinicio;
	}
	public Date getDtvencimentofim() {
		return dtvencimentofim;
	}
	public void setDtvencimentoinicio(Date dtvencimentoinicio) {
		this.dtvencimentoinicio = dtvencimentoinicio;
	}
	public void setDtvencimentofim(Date dtvencimentofim) {
		this.dtvencimentofim = dtvencimentofim;
	}
	public String getSeguro() {
		return seguro;
	}
	public void setSeguro(String seguro) {
		this.seguro = seguro;
	}
}
