package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculotipomarcacorcombustivelFiltro extends FiltroListagemSined{

	protected String descricao;
	
	@DisplayName("Descri��o")
	@MaxLength(20)
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
