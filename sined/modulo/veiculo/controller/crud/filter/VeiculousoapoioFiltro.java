package br.com.linkcom.sined.modulo.veiculo.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Veiculo;


public class VeiculousoapoioFiltro {

	protected String dataReferencia;
	protected Colaborador colaborador;
	protected Veiculo veiculo; 
	
	public String getDataReferencia() {
		return dataReferencia;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
}
