package br.com.linkcom.sined.modulo.veiculo.controller.crud;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Inspecaoitem;
import br.com.linkcom.sined.geral.bean.Ordemservicotipo;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.geral.bean.enumeration.EnumSituacaomanutencao;
import br.com.linkcom.sined.geral.service.CategoriaiteminspecaoService;
import br.com.linkcom.sined.geral.service.InspecaoitemService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculoagendamentoService;
import br.com.linkcom.sined.geral.service.VeiculomanutencaoService;
import br.com.linkcom.sined.geral.service.VeiculomodeloService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoitemService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculomanutencaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/veiculo/crud/Veiculomanutencao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"veiculo.placa", "manutencao", "vencida", "veiculo.prefixo", "dtprevista", "veiculo.colaborador"})
public class VeiculomanutencaoCrud extends CrudControllerSined<VeiculomanutencaoFiltro, Veiculoordemservico, Veiculoordemservico> {

	protected VeiculoordemservicoitemService veiculoordemservicoitemService; 
	protected VeiculomanutencaoService manutencaoService;
	protected InspecaoitemService inspecaoitemService;
	protected VeiculoordemservicoService veiculoordemservicoService;
	protected VeiculoService veiculoService;
	protected VeiculomodeloService veiculomodeloService;
	protected CategoriaiteminspecaoService categoriaiteminspecaoService;
	protected PessoaService pessoaService;
	protected VeiculousoService veiculousoService;
	protected VeiculomanutencaoService veiculomanutencaoService;
	protected VeiculoagendamentoService veiculoagendamentoService;
	
	public void setVeiculoordemservicoitemService(
			VeiculoordemservicoitemService veiculoordemservicoitemService) {
		this.veiculoordemservicoitemService = veiculoordemservicoitemService;
	}
	public void setManutencaoService(VeiculomanutencaoService manutencaoService) {
		this.manutencaoService = manutencaoService;
	}
	public void setInspecaoitemService(InspecaoitemService inspecaoitemService) {
		this.inspecaoitemService = inspecaoitemService;
	}
	public void setVeiculoordemservicoService(
			VeiculoordemservicoService veiculoordemservicoService) {
		this.veiculoordemservicoService = veiculoordemservicoService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setVeiculomodeloService(
			VeiculomodeloService veiculomodeloService) {
		this.veiculomodeloService = veiculomodeloService;
	}
	public void setCategoriaiteminspecaoService(CategoriaiteminspecaoService categoriaiteminspecaoService) {
		this.categoriaiteminspecaoService = categoriaiteminspecaoService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
		
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	public void setVeiculomanutencaoService(
			VeiculomanutencaoService veiculomanutencaoService) {
		this.veiculomanutencaoService = veiculomanutencaoService;
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, VeiculomanutencaoFiltro filtro) {
		return new ModelAndView("crud/veiculomanutencaoListagem");
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Veiculoordemservico form) {
		request.getSession().setAttribute("isEditar", Boolean.TRUE);
		return new ModelAndView("crud/veiculomanutencaoEntrada", "filtro", form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Veiculoordemservico bean)throws Exception {
		String cd = request.getParameter("listaItens");
		List<Veiculoordemservicoitem> listaOrdemServicoItem = new ArrayList<Veiculoordemservicoitem>();
		if (cd == null || cd.equals("")) {
			if (bean.getListaVeiculoordemservicoitem() != null) {
				for (Veiculoordemservicoitem veiculoordemservicoitem : bean.getListaVeiculoordemservicoitem()) {
					veiculoordemservicoitem.setOrdemservico(bean);
					listaOrdemServicoItem.add(veiculoordemservicoitem);
					veiculoordemservicoitemService.saveOrUpdate(veiculoordemservicoitem);
				}
			}
			bean.setListaVeiculoordemservicoitem(listaOrdemServicoItem);
			Veiculoordemservico veiculoOrdemServico_aux = veiculoordemservicoService.loadWithVeiculo(bean);
			bean.setVeiculo(veiculoOrdemServico_aux.getVeiculo());
			//bean = veiculoordemservicoService.loadWithVeiculo(bean);
			bean.setOrdemservicotipo(Ordemservicotipo.MANUTENCAO);
			super.salvar(request, bean);
		}else {
			String cds[] = cd.split(",");
			Veiculoordemservicoitem ordemservicoitem = null;
			for (String codigo : cds) {
				ordemservicoitem = new Veiculoordemservicoitem();
				Inspecaoitem iteminspecao = new Inspecaoitem();
				Integer id = Integer.parseInt(codigo);
				iteminspecao.setCdinspecaoitem(id);
				ordemservicoitem.setInspecaoitem(iteminspecao);
				ordemservicoitem.setStatus(Boolean.FALSE);
				ordemservicoitem.setAtivo(Boolean.TRUE);
				listaOrdemServicoItem.add(ordemservicoitem);
			}
			bean.setListaVeiculoordemservicoitem(listaOrdemServicoItem);
			bean.setOrdemservicotipo(Ordemservicotipo.MANUTENCAO);
			veiculoordemservicoService.saveOrUpdate(bean);
			request.addMessage("Registro salvo com sucesso.");
		}
	}
	
	@Override
	/**
	 * <B>Se a acao � edi��o, carrega uma lista de ordemservicoitem</B> 
	 * 
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoitemService#findOrdemServicoItemByOrdemServico(Ordemservico)
	 * @author Jo�o Paulo Zica
	 * 
	 */
	protected void entrada(WebRequestContext request, Veiculoordemservico form) throws Exception {
		if(request.getBindException().hasErrors())
			form.setVeiculo(veiculoService.findVeiculo(form.getVeiculo()));
		
		if (form.getCdveiculoordemservico()!=null) {
			if(form.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(EnumSituacaomanutencao.CANCELADA.getIdSitucao()))
				request.setAttribute("showEdit", false);
			else
				request.setAttribute("showEdit", true);
			
			if(request.getParameter("ACAO") != null && request.getParameter("ACAO").equals("editar") && form.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(EnumSituacaomanutencao.CANCELADA.getIdSitucao()))
				throw new SinedException("N�o � possivel editar um uma manuten��o cancelada.");
			//Ordemservico load = ordemservicoService.load(form);
			request.getSession().setAttribute("edicao", Boolean.TRUE);
			List<Veiculoordemservicoitem> listaOrdemservicoitem = veiculoordemservicoitemService.findOrdemServicoItemByOrdemServico(form); 
//			List<Iteminspecao> listaItemInspecao = new ArrayList<Iteminspecao>();
//			Iteminspecao iteminspecao = null;
//			for (Ordemservicoitem ordemservicoitem : listaOrdemservicoitem) {
//				iteminspecao = ordemservicoitem.getIteminspecao();
//				iteminspecao.setAtivo(ordemservicoitem.getAtivo());
//				iteminspecao.setStatus(ordemservicoitem.getStatus());
//				iteminspecao.setDtcancelamento(ordemservicoitem.getDtcancelamento());
//				iteminspecao.setCdordemservicoitem(ordemservicoitem.getCdordemservicoitem());
//				iteminspecao.setMotivocancela(ordemservicoitem.getMotivocancela());
//				listaItemInspecao.add(iteminspecao);
//			}
//			form.setVeiculo(load.getVeiculo());
//			form.setListaIteminspecao(listaItemInspecao);
			form.setListaVeiculoordemservicoitem(listaOrdemservicoitem);
			request.setAttribute("size", form.getListaVeiculoordemservicoitem().size());
			super.entrada(request, form);
		}else{
			form.setDtrealizada(new Date(System.currentTimeMillis()));
			form.setOrdemservicotipo(Ordemservicotipo.MANUTENCAO);
			request.setAttribute("size", 0);
			request.getSession().setAttribute("edicao", Boolean.FALSE);
			super.entrada(request, form);
		}
	}
	
	@Override
	protected ListagemResult<Veiculoordemservico> getLista(WebRequestContext request,VeiculomanutencaoFiltro filtro) {
		ListagemResult<Veiculoordemservico> listagemResult = super.getLista(request, filtro);
		List<Veiculoordemservico> lista = listagemResult.list();
		for (Veiculoordemservico ordemservico : lista) {
			ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao();
			if (ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(1)) {
				ordemservico.setManutencao(EnumSituacaomanutencao.PENDENTE.getDesricao());
			}else if (ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(2)) {
				ordemservico.setManutencao(EnumSituacaomanutencao.EM_ANDAMENTO.getDesricao());
			}else if (ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(3)) {
				ordemservico.setManutencao(EnumSituacaomanutencao.REALIZADA.getDesricao());
			}else if (ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(4)) {
				ordemservico.setManutencao(EnumSituacaomanutencao.CANCELADA.getDesricao());
			}
			Date dataatual = new Date(System.currentTimeMillis());
			if (ordemservico.getDtrealizada().before(dataatual)) {
				ordemservico.setVencida(Boolean.TRUE);
			}else if (ordemservico.getDtrealizada().after(dataatual) || ordemservico.getManutencao().equals("Realizada")){
				ordemservico.setVencida(Boolean.FALSE);
			}
		}
		return listagemResult;
	}	
	
	/**
	 * <b>M�todo respons�vel em listar os dados da lista de item inspe��o</b>
	 * 
	 * @author Biharck, Jo�o Paulo Zica
	 */
	public ModelAndView listar(WebRequestContext request, Veiculoordemservico bean) throws Exception {
		
		//pega a placa do veiculo selecionada pelo usu�rio
		String orderBy = request.getParameter("orderBy");
		Boolean asc; 
		if (orderBy.equals("")) {
			orderBy = "inspecaoitemtipo.nome";
			asc = Boolean.TRUE;
		}else{
			asc = Boolean.valueOf(request.getParameter("asc"));
		}
		Veiculomodelo modelo = veiculoService.findVeiculo(bean.getVeiculo()).getVeiculomodelo();
		List<Categoriaiteminspecao> listaCategoriaItemInspecao = categoriaiteminspecaoService.loadCategoriaItemInspecao(modelo, orderBy, asc);
//		bean.setListaCategoriaItemInspecao(listaCategoriaItemInspecao);
		return new ModelAndView("direct:crud/detalheManutencao", "listaCategoriaItemInspecao", listaCategoriaItemInspecao);
	}
	@Override
	protected void validateBean(Veiculoordemservico bean, BindException errors) {
		if(veiculomanutencaoService.existeManutencaoData(bean))
			errors.reject("001", "Manuten��o j� cadastrada no sistema para esse ve�culo.");
		if(this.veiculousoService.existeVeiculoAlocadoData(bean))
			errors.reject("002", "Este ve�culo j� est� alocado para uso nesta data.");
		if(bean.getListaVeiculoordemservicoitem() != null && !bean.getListaVeiculoordemservicoitem().isEmpty())
			for (Veiculoordemservicoitem veiculoordemservicoitem : bean.getListaVeiculoordemservicoitem()) 
				if(veiculoordemservicoitem.getStatus() != null && veiculoordemservicoitem.getStatus() && !veiculoordemservicoitem.getAtivo()){
					errors.reject("003", "Item(ns) realizado(s) n�o pode(m) ser cancelado(s).");
					break;
				}else if(veiculoordemservicoitem.getDtcancelamento() != null && 
				   SinedDateUtils.dateToBeginOfDay(new Date(System.currentTimeMillis())).before(SinedDateUtils.dateToBeginOfDay(veiculoordemservicoitem.getDtcancelamento()))){
					errors.reject("003", "A data de cancelamento n�o pode ser maior que a data atual.");
					break;
				}
	}
	
}


