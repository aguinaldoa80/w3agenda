package br.com.linkcom.sined.modulo.veiculo.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.VeiculoagendamentoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;



@Bean
@Controller(
	path = "/veiculo/relatorio/Comprovanteagendamento",
	authorizationModule=ReportAuthorizationModule.class
)

public class ComprovanteagendamentoReport extends SinedReport<VeiculoagendamentoFiltro>{
	
	private VeiculoordemservicoService veiculoordemservicoService;
	
	public void setVeiculoordemservicoService(
			VeiculoordemservicoService veiculoordemservicoService) {
		this.veiculoordemservicoService = veiculoordemservicoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, VeiculoagendamentoFiltro filtro) throws Exception {
		return veiculoordemservicoService.createReportComprovanteAgendamento(filtro);
	}
	
	@Override
	public String getTitulo(VeiculoagendamentoFiltro filtro) {
		return "Comprovante de Agendamento";
	}

	@Override
	public String getNomeArquivo() {
		return "comprovanteagendamento";
	}
	
	
}




