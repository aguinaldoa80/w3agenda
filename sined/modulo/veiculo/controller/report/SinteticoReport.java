package br.com.linkcom.sined.modulo.veiculo.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculokmService;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.AnaliticoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;



@Bean
@Controller(
	path = "/veiculo/relatorio/Sintetico",
	authorizationModule=ReportAuthorizationModule.class
)
public class SinteticoReport extends SinedReport<AnaliticoFiltro>{
	
	private VeiculokmService veiculokmService;
	private VeiculoService veiculoService;
	
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setVeiculokmService(VeiculokmService veiculokmService) {
		this.veiculokmService = veiculokmService;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, AnaliticoFiltro filtro) throws ResourceGenerationException {
		
		if(filtro.getVeiculo() != null && filtro.getVeiculo().getCdveiculo() != null){
			filtro.setVeiculo(veiculoService.findVeiculo(filtro.getVeiculo()));
		}
		return super.doFiltro(request, filtro);
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, AnaliticoFiltro filtro) throws Exception {
		return veiculokmService.createReportSintetico(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "sintetico";
	}
	
	@Override
	public String getTitulo(AnaliticoFiltro filtro) {
		return "RELAT�RIO SINT�TICO DE VARIA��O DAS QUILOMETRAGEM DE VE�CULO";
	}
}




