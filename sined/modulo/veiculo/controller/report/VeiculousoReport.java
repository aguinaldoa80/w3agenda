package br.com.linkcom.sined.modulo.veiculo.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculousotipo;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculouso;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.geral.service.VeiculousotipoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculousoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/veiculo/relatorio/Veiculouso", 
			authorizationModule=ReportAuthorizationModule.class
)
public class VeiculousoReport extends SinedReport<VeiculousoFiltro>{

	private VeiculousoService veiculousoService;
	private LocalarmazenagemService localarmazenagemService;
	private VeiculoService veiculoService;
	private VeiculousotipoService veiculousotipoService;

	public void setVeiculousotipoService(VeiculousotipoService veiculousotipoService) {this.veiculousotipoService = veiculousotipoService;}
	public void setVeiculoService(VeiculoService veiculoService) {this.veiculoService = veiculoService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setVeiculousoService(VeiculousoService veiculousoService) {this.veiculousoService = veiculousoService;}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	VeiculousoFiltro filtro) throws Exception {
		return veiculousoService.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "veiculouso";
	}
	
	@Override
	public String getTitulo(VeiculousoFiltro filtro) {
		return "USO DO VE�CULO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, VeiculousoFiltro filtro) {
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){
			Cliente cliente = getClienteService().load(filtro.getCliente(), "cliente.nome");
			report.addParameter("CLIENTE", cliente.getNome());
		}
		
		if(filtro.getLocalarmazenagem() != null && filtro.getLocalarmazenagem().getCdlocalarmazenagem() != null){
			Localarmazenagem localarmazenagem = localarmazenagemService.load(filtro.getLocalarmazenagem(), "localarmazenagem.nome");		
			report.addParameter("LOCALARMAZENAGEM", localarmazenagem.getNome());
		}
		
		if(filtro.getVeiculo() != null && filtro.getVeiculo().getCdveiculo() != null){
			Veiculo veiculo = veiculoService.findVeiculo(filtro.getVeiculo());
			report.addParameter("VEICULO", veiculo.getDescriptionProperty());
		}
		
		if(filtro.getVeiculousotipo() != null && filtro.getVeiculousotipo().getCdveiculousotipo() != null){
			Veiculousotipo veiculousotipo = veiculousotipoService.load(filtro.getVeiculousotipo(), "veiculousotipo.descricao");
			report.addParameter("VEICULOUSOTIPO", veiculousotipo.getDescricao());
		}
		
		if (filtro.getListaveiculo() != null && filtro.getListaveiculo().size() > 0) {
			String situacoes = "";
			for (Object object : filtro.getListaveiculo()) {
				situacoes += SituacaoVeiculouso.valueOf(object.toString()).getNome() + ", ";
			}
			if(!situacoes.equals("")){
				situacoes = situacoes.substring(0, situacoes.length()-2);
				report.addParameter("SITUACOES", situacoes);
			}
		
		}
	}

}
