package br.com.linkcom.sined.modulo.veiculo.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.FichaalocacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/veiculo/relatorio/Fichaalocacao", 
			authorizationModule=ReportAuthorizationModule.class
)
public class FichaalocacaoReport extends SinedReport<FichaalocacaoFiltro>{
	
	private VeiculousoService veiculousoService;
	private VeiculoService veiculoService;
	
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	FichaalocacaoFiltro filtro) throws Exception {
		return veiculousoService.gerarFichaAlocacaoRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "fichaalocacao";
	}
	
	@Override
	public String getTitulo(FichaalocacaoFiltro filtro) {
		return "FICHA DE ALOCA��O";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, FichaalocacaoFiltro filtro) {
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			report.addParameter("EMPRESAFILTRO", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){
			Cliente cliente = getClienteService().load(filtro.getCliente(), "cliente.nome");
			report.addParameter("CLIENTE", cliente.getNome());
		}
		
		if(filtro.getVeiculo() != null && filtro.getVeiculo().getCdveiculo() != null)
			report.addParameter("VEICULO", veiculoService.findVeiculo(filtro.getVeiculo()).getDescriptionProperty());

		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}

}
