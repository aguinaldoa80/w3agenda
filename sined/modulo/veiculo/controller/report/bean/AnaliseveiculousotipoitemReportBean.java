package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Veiculousotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;

public class AnaliseveiculousotipoitemReportBean{

	protected Veiculousotipo veiculousotipo;
	protected List<AnaliseveiculousoitemReportBean> listaItens = new ArrayList<AnaliseveiculousoitemReportBean>();
	
	public AnaliseveiculousotipoitemReportBean(){
	}
	
	public AnaliseveiculousotipoitemReportBean(Veiculousotipo veiculousotipo){
		this.veiculousotipo = veiculousotipo;
		for (Mes mes : Mes.values()) 
			listaItens.add(new AnaliseveiculousoitemReportBean(mes));
	}

	public Veiculousotipo getVeiculousotipo() {
		return veiculousotipo;
	}

	public List<AnaliseveiculousoitemReportBean> getListaItens() {
		return listaItens;
	}

	public void setVeiculousotipo(Veiculousotipo veiculousotipo) {
		this.veiculousotipo = veiculousotipo;
	}

	public void setListaItens(List<AnaliseveiculousoitemReportBean> listaItens) {
		this.listaItens = listaItens;
	}

}
