package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculousotipo;



public class AnaliseveiculousoReportBean{

	protected Veiculo veiculo;
	protected Colaborador condutor;
	protected List<AnaliseveiculousotipoitemReportBean> listaItens = new ArrayList<AnaliseveiculousotipoitemReportBean>();
	
	public AnaliseveiculousoReportBean(){
	}
	
	public AnaliseveiculousoReportBean(Veiculo veiculo, Colaborador condutor, Veiculousotipo veiculousotipo){
		this.veiculo = veiculo;
		this.condutor = condutor;
		listaItens.add(new AnaliseveiculousotipoitemReportBean(veiculousotipo));
	}
	
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public Colaborador getCondutor() {
		return condutor;
	}
	public List<AnaliseveiculousotipoitemReportBean> getListaItens() {
		return listaItens;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setListaItens(List<AnaliseveiculousotipoitemReportBean> listaItens) {
		this.listaItens = listaItens;
	}
	public void setCondutor(Colaborador condutor) {
		this.condutor = condutor;
	}
	
}
