package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.Date;



public class VeiculoagendamentoReportBean{

	protected String prefixo;
	protected String placa;
	protected String cooperado; 
	protected String modelo;
	protected Date dtagendamento;
	
	
	public String getCooperado() {
		return cooperado;
	}
	public String getPlaca() {
		return placa;
	}
	public String getPrefixo() {
		return prefixo;
	}
	
	public String getModelo() {
		return modelo;
	}

	public Date getDtagendamento() {
		return dtagendamento;
	}

	public void setCooperado(String cooperado) {
		this.cooperado = cooperado;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public void setDtagendamento(Date dtagendamento) {
		this.dtagendamento = dtagendamento;
	}
	
	

}
