package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import br.com.linkcom.sined.geral.bean.enumeration.Mes;

public class AnaliseveiculousoitemReportBean{

	protected Mes mes;
	protected Integer qtdePrevista;
	protected Integer qtdeRealizada;
	
	public AnaliseveiculousoitemReportBean(){
	}
	
	public AnaliseveiculousoitemReportBean(Mes mes){
		this.qtdePrevista = 0;
		this.qtdeRealizada = 0;
		this.mes = mes;
	}
	
	public Integer getQtdePrevista() {
		return qtdePrevista;
	}
	public Integer getQtdeRealizada() {
		return qtdeRealizada;
	}
	public Mes getMes() {
		return mes;
	}
	public void setQtdePrevista(Integer qtdePrevista) {
		this.qtdePrevista = qtdePrevista;
	}
	public void setQtdeRealizada(Integer qtdeRealizada) {
		this.qtdeRealizada = qtdeRealizada;
	}
	public void setMes(Mes mes) {
		this.mes = mes;
	}

}
