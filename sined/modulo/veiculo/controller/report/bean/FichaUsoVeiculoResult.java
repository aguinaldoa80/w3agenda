package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;


public class FichaUsoVeiculoResult {
	private Integer cdveiculo;
	private Integer cdescalahorario;
	private String empresa;
	private String veiculo;
	private String responsavel;
	private String cliente;
	private String horaInicial;
	private String horaFinal;
	
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public Integer getCdveiculo() {
		return cdveiculo;
	}
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}
	public Integer getCdescalahorario() {
		return cdescalahorario;
	}
	public void setCdescalahorario(Integer cdescalahorario) {
		this.cdescalahorario = cdescalahorario;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getHoraInicial() {
		return horaInicial;
	}
	public void setHoraInicial(String horaInicial) {
		this.horaInicial = horaInicial;
	}
	public String getHoraFinal() {
		return horaFinal;
	}
	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}	
	
}
