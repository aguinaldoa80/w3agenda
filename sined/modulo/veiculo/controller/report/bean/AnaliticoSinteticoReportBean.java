package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.Date;



public class AnaliticoSinteticoReportBean{

	protected String prefixo;
	protected String placa;
	protected String cooperado; 
	protected String modelo;
	protected Integer kmentrada;
	protected Integer kmatual;
	protected Integer kmrodado;
	protected Integer kmreal;
	protected Double horimetroentrada;
	protected Double horimetroatual;
	protected Double horimetrorodado;
	protected Double horimetroreal;
	protected Date dtajuste;
	protected Integer kmanterior;
	protected Integer kmposterior;
	protected String tipoajuste;	
	protected Double horimetroanterior;
	protected Double horimetroposterior;
	
	public String getCooperado() {
		return cooperado;
	}
	public String getPlaca() {
		return placa;
	}
	public String getPrefixo() {
		return prefixo;
	}
	
	public String getModelo() {
		return modelo;
	}
	public Integer getKmentrada() {
		return kmentrada;
	}
	public Integer getKmatual() {
		return kmatual;
	}
	public Integer getKmrodado() {
		return kmrodado;
	}
	public Integer getKmreal() {
		return kmreal;
	}
	public Date getDtajuste() {
		return dtajuste;
	}
	public Integer getKmanterior() {
		return kmanterior;
	}
	public Integer getKmposterior() {
		return kmposterior;
	}
	public String getTipoajuste() {
		return tipoajuste;
	}
	public void setCooperado(String cooperado) {
		this.cooperado = cooperado;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public void setKmentrada(Integer kmentrada) {
		this.kmentrada = kmentrada;
	}
	public void setKmatual(Integer kmatual) {
		this.kmatual = kmatual;
	}
	public void setKmrodado(Integer kmrodado) {
		this.kmrodado = kmrodado;
	}
	public void setKmreal(Integer kmreal) {
		this.kmreal = kmreal;
	}
	public void setDtajuste(Date dtajuste) {
		this.dtajuste = dtajuste;
	}
	public void setKmanterior(Integer kmanterior) {
		this.kmanterior = kmanterior;
	}
	public void setKmposterior(Integer kmposterior) {
		this.kmposterior = kmposterior;
	}
	public void setTipoajuste(String tipoajuste) {
		this.tipoajuste = tipoajuste;
	}
	public Double getHorimetroentrada() {
		return horimetroentrada;
	}
	public Double getHorimetroatual() {
		return horimetroatual;
	}
	public Double getHorimetrorodado() {
		return horimetrorodado;
	}
	public Double getHorimetroreal() {
		return horimetroreal;
	}
	public Double getHorimetroanterior() {
		return horimetroanterior;
	}
	public Double getHorimetroposterior() {
		return horimetroposterior;
	}
	public void setHorimetroentrada(Double horimetroentrada) {
		this.horimetroentrada = horimetroentrada;
	}
	public void setHorimetroatual(Double horimetroatual) {
		this.horimetroatual = horimetroatual;
	}
	public void setHorimetrorodado(Double horimetrorodado) {
		this.horimetrorodado = horimetrorodado;
	}
	public void setHorimetroreal(Double horimetroreal) {
		this.horimetroreal = horimetroreal;
	}
	public void setHorimetroanterior(Double horimetroanterior) {
		this.horimetroanterior = horimetroanterior;
	}
	public void setHorimetroposterior(Double horimetroposterior) {
		this.horimetroposterior = horimetroposterior;
	}
}
