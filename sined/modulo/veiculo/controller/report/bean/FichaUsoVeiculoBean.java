package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import br.com.linkcom.neo.types.Hora;

public class FichaUsoVeiculoBean {
	private String cliente;
	private Hora horaInicio;
	private Hora horaFim;
	
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public Hora getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(Hora inicio) {
		this.horaInicio = inicio;
	}
	public Hora getHoraFim() {
		return horaFim;
	}
	public void setHoraFim(Hora fim) {
		this.horaFim = fim;
	}
	public String getHorario() {
		if(this.horaFim != null)
			return this.horaInicio.toString() + " - " + this.horaFim.toString();
		return null;
	}
}
