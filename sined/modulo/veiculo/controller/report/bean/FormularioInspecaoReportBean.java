package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;

public class FormularioInspecaoReportBean{

	protected String prefixo;
	protected String placa;
	protected String cooperado;
	protected Tipocontroleveiculo tipocontroleveiculo;
	protected Integer km;
	protected Double horimetro;
	protected String observacao;
	protected String nomeultimoprojetousoveiculo;
	protected String nomeultimocondutorusoveiculo;
	protected List<FormularioInspecaoReportBeanItem> listaItem;
	
	public String getPrefixo() {
		return prefixo;
	}
	public String getPlaca() {
		return placa;
	}
	public String getCooperado() {
		return cooperado;
	}
	public Integer  getKm() {
		return km;
	}
	public List<FormularioInspecaoReportBeanItem> getListaItem() {
		return listaItem;
	}
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setCooperado(String cooperado) {
		this.cooperado = cooperado;
	}
	public void setKm(Integer km) {
		this.km = km;
	}
	public void setListaItem(List<FormularioInspecaoReportBeanItem> listaItem) {
		this.listaItem = listaItem;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getNomeultimoprojetousoveiculo() {
		return nomeultimoprojetousoveiculo;
	}
	public String getNomeultimocondutorusoveiculo() {
		return nomeultimocondutorusoveiculo;
	}
	public void setNomeultimoprojetousoveiculo(String nomeultimoprojetousoveiculo) {
		this.nomeultimoprojetousoveiculo = nomeultimoprojetousoveiculo;
	}
	public void setNomeultimocondutorusoveiculo(String nomeultimocondutorusoveiculo) {
		this.nomeultimocondutorusoveiculo = nomeultimocondutorusoveiculo;
	}
	public Double getHorimetro() {
		return horimetro;
	}
	public void setHorimetro(Double horimetro) {
		this.horimetro = horimetro;
	}
	public Tipocontroleveiculo getTipocontroleveiculo() {
		return tipocontroleveiculo;
	}
	public void setTipocontroleveiculo(Tipocontroleveiculo tipocontroleveiculo) {
		this.tipocontroleveiculo = tipocontroleveiculo;
	}
}
