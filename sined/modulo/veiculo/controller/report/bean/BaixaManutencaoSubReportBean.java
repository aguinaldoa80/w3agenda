package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.Date;

public class BaixaManutencaoSubReportBean{
	protected String tipoitens;
	protected String descricaoitens;
	protected Boolean status = Boolean.FALSE;
	protected Date dtmanutencao;
	protected String local;
	protected String observacao;
	
	public String getDescricaoitens() {
		return descricaoitens;
	}
	public String getTipoitens() {
		return tipoitens;
	}

	public Date getDtmanutencao() {
		return dtmanutencao;
	}
	public String getLocal() {
		return local;
	}
	public String getObservacao() {
		return observacao;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setDescricaoitens(String descricaoitens) {
		this.descricaoitens = descricaoitens;
	}
	public void setTipoitens(String tipoitens) {
		this.tipoitens = tipoitens;
	}
	
	public void setDtmanutencao(Date  dtmanutencao) {
		this.dtmanutencao = dtmanutencao;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
}
