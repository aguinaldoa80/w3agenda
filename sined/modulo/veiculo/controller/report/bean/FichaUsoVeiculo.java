package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

public class FichaUsoVeiculo {
	//private Integer cdveiculo;
	//private Integer cdescalahorario;
	private String empresa;
	private String veiculo;
	private java.util.Date data;
	private String responsavel;
	
	
	private int totalAulas;
	private List<FichaUsoVeiculoBean> listHorarioCliente = new ArrayList<FichaUsoVeiculoBean>();
	
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}
	public java.util.Date getData() {
		return data;
	}
	public void setData(java.util.Date data) {
		this.data = data;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public List<FichaUsoVeiculoBean> getListHorarioCliente() {
		return listHorarioCliente;
	}
	public void setListHorarioCliente(List<FichaUsoVeiculoBean> listHorarioCliente) {
		this.listHorarioCliente = listHorarioCliente;
	}
	public void setTotalAulas(int totalAulas) {
		this.totalAulas = totalAulas;
	}
	public int getTotalAulas() {
		return totalAulas;
	}
	
}
