package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.Date;

public class FormularioInspecaoReportBeanItem{
	
	protected String tipoitens;
	protected String descricaoitens;
	protected Boolean vencido = Boolean.FALSE;
	protected Integer kminspecao;
	protected Double horimetroinspecao;
	protected Date dtmanutencao;
	
	public String getTipoitens() {
		return tipoitens;
	}
	public String getDescricaoitens() {
		return descricaoitens;
	}
	
	public Date getDtmanutencao() {
		return dtmanutencao;
	}
	public Boolean getVencido() {
		return vencido;
	}
	public void setDescricaoitens(String descricaoitens) {
		this.descricaoitens = descricaoitens;
	}
	
	public void setDtmanutencao(Date dtmanutencao) {
		this.dtmanutencao = dtmanutencao;
	}
	public void setVencido(Boolean vencido) {
		this.vencido = vencido;
	}
	public void setTipoitens(String tipoitens) {
		this.tipoitens = tipoitens;
	}
	public Integer getKminspecao() {
		return kminspecao;
	}
	public void setKminspecao(Integer kminspecao) {
		this.kminspecao = kminspecao;
	}
	
	public Double getHorimetroinspecao() {
		return horimetroinspecao;
	}
	public void setHorimetroinspecao(Double horimetroinspecao) {
		this.horimetroinspecao = horimetroinspecao;
	}
}
