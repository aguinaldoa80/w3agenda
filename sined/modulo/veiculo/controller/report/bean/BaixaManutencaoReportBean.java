package br.com.linkcom.sined.modulo.veiculo.controller.report.bean;

import java.util.List;



public class BaixaManutencaoReportBean{

	protected String prefixo;
	protected String placa;
	protected String cooperado; 
	protected List<BaixaManutencaoSubReportBean> listaManutencao;
	
	
	public String getCooperado() {
		return cooperado;
	}
	public String getPlaca() {
		return placa;
	}
	public String getPrefixo() {
		return prefixo;
	}
	public List<BaixaManutencaoSubReportBean> getListaManutencao() {
		return listaManutencao;
	}
	public void setCooperado(String cooperado) {
		this.cooperado = cooperado;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public void setListaManutencao(
			List<BaixaManutencaoSubReportBean> listaManutencao) {
		this.listaManutencao = listaManutencao;
	}

}
