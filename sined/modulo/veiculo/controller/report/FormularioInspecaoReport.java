package br.com.linkcom.sined.modulo.veiculo.controller.report;

import java.util.HashMap;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FormularioinspecaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;



@Bean
@Controller(
	path = "/veiculo/relatorio/FormularioInspecao",
	authorizationModule=ReportAuthorizationModule.class
)

public class FormularioInspecaoReport extends SinedReport<FormularioinspecaoFiltro>{
	
	private VeiculoService veiculoService;
	private VeiculoordemservicoService veiculoordemservicoService;
	
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	public void setVeiculoordemservicoService(VeiculoordemservicoService veiculoordemservicoService) {
		this.veiculoordemservicoService = veiculoordemservicoService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, FormularioinspecaoFiltro filtro) throws Exception {
		if(request.getParameter("whereInInspecao") != null && !"".equals(request.getParameter("whereInInspecao"))){
			String whereInInspecao = request.getParameter("whereInInspecao");
			return veiculoordemservicoService.createReportFormularioInspecao(whereInInspecao);
		}
		
		return veiculoService.createReportFormularioInspecao(filtro);
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	FormularioinspecaoFiltro filtro) throws ResourceGenerationException {
		if(request.getParameter("whereInInspecao") != null && !"".equals(request.getParameter("whereInInspecao"))){
			return new ModelAndView("redirect:/veiculo/crud/Veiculoordemservico");
		}
		return super.doFiltro(request, filtro);
	}

	@Override
	protected void filtro(WebRequestContext request, FormularioinspecaoFiltro filtro) throws Exception {
		HashMap<String, String> mapaMetodos = new HashMap<String, String>();
		mapaMetodos.put("cooperado", "Respons�vel"); 
		mapaMetodos.put("modelo", "Modelo");
		mapaMetodos.put("placa", " Placa");
		mapaMetodos.put("tipo", "Tipo");

		request.setAttribute("listaMetodo", mapaMetodos);
		
		super.filtro(request, filtro);
	}
	
	@Override
	public String getTitulo(FormularioinspecaoFiltro filtro) {
		return "INSPE��O VEICULAR";
	}

	@Override
	public String getNomeArquivo() {
		return "formularioinspecao";
	}
}




