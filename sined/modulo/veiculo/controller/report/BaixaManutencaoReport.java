package br.com.linkcom.sined.modulo.veiculo.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.BaixamanutencaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;



@Bean
@Controller(
	path = "/veiculo/relatorio/BaixaManutencao",
	authorizationModule=ReportAuthorizationModule.class
)

public class BaixaManutencaoReport extends SinedReport<BaixamanutencaoFiltro>{
	
	private VeiculoordemservicoService veiculoordemservicoService;
	
	public void setVeiculoordemservicoService(VeiculoordemservicoService veiculoordemservicoService) {
		this.veiculoordemservicoService = veiculoordemservicoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,BaixamanutencaoFiltro filtro) throws Exception {
		return veiculoordemservicoService.createReportBaixaManutencao(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "baixamanutencao";
	}

	@Override
	public String getTitulo(BaixamanutencaoFiltro filtro) {
		return "Baixa de Manutenção";
	}
}




