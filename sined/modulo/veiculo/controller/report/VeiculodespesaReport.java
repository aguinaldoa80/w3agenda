package br.com.linkcom.sined.modulo.veiculo.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.VeiculodespesaService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculodespesaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/veiculo/relatorio/Veiculodespesa", 
			authorizationModule=ReportAuthorizationModule.class
)
public class VeiculodespesaReport extends SinedReport<VeiculodespesaFiltro>{

	private VeiculodespesaService veiculodespesaService;
	
	public void setVeiculodespesaService(
			VeiculodespesaService veiculodespesaService) {
		this.veiculodespesaService = veiculodespesaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	VeiculodespesaFiltro filtro) throws Exception {
		return veiculodespesaService.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "veiculodespesa";
	}
	
	@Override
	public String getTitulo(VeiculodespesaFiltro filtro) {
		return "VE�CULO DESPESA";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, VeiculodespesaFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getEmpresa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
	}

}
