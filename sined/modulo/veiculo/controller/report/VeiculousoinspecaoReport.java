package br.com.linkcom.sined.modulo.veiculo.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/veiculo/relatorio/Veiculousoinspecao", 
			authorizationModule=ReportAuthorizationModule.class
)
public class VeiculousoinspecaoReport extends SinedReport<VeiculoFiltro>{

	private VeiculousoService veiculousoService;
	
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	VeiculoFiltro filtro) throws Exception {
		if(request.getParameter("REPORTVEICULOUSO") != null && "true".equals(request.getParameter("REPORTVEICULOUSO"))){
			String whereInVeiuculouso = request.getParameter("whereInVeiuculouso");
			if(whereInVeiuculouso != null && !"".equals(whereInVeiuculouso))
				return veiculousoService.createReportFormularioInspecao(whereInVeiuculouso);
		}
		throw new  SinedException("Nenhum item selecionado");
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, VeiculoFiltro filtro) throws ResourceGenerationException {
		return new ModelAndView("redirect:/veiculo/crud/Veiculouso");
	}

	@Override
	public String getNomeArquivo() {
		return "inspe��o_usoveiculo";
	}
	
	@Override
	public String getTitulo(VeiculoFiltro filtro) {
		return "INSPE��O";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, VeiculoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
	}
}
