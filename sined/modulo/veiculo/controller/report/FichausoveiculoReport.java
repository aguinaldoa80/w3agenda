package br.com.linkcom.sined.modulo.veiculo.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FichausoveiculoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/veiculo/relatorio/Fichausoveiculo", 
			authorizationModule=ReportAuthorizationModule.class
)
public class FichausoveiculoReport extends SinedReport<FichausoveiculoFiltro>{

	private VeiculousoService veiculousoService;
	
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	FichausoveiculoFiltro filtro) throws Exception {
		return veiculousoService.gerarRelatorioFichaUso(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "fichausoveiculo";
	}
	
	@Override
	public String getTitulo(FichausoveiculoFiltro filtro) {
		return "Ficha de uso";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, FichausoveiculoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
	}

}
