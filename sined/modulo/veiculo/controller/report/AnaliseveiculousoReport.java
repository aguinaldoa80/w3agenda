package br.com.linkcom.sined.modulo.veiculo.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.AnaliseveiculousoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/veiculo/relatorio/Analiseveiculouso", 
			authorizationModule=ReportAuthorizationModule.class
)
public class AnaliseveiculousoReport extends SinedReport<AnaliseveiculousoFiltro>{
	
	private VeiculousoService veiculousoService;
	private VeiculoService veiculoService;
	
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	AnaliseveiculousoFiltro filtro) throws Exception {
		return veiculousoService.gerarAnaliseVeiculoUsoRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "fichaalocacao";
	}
	
	@Override
	public String getTitulo(AnaliseveiculousoFiltro filtro) {
		return "AN�LISE USO DO VE�CULO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, AnaliseveiculousoFiltro filtro) {
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		report.addParameter("ANO", filtro.getAno());
		
		if(filtro.getVeiculo() != null && filtro.getVeiculo().getCdveiculo() != null)
			report.addParameter("VEICULO", veiculoService.findVeiculo(filtro.getVeiculo()).getDescriptionProperty());

	}

}
