package br.com.linkcom.sined.modulo.veiculo.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculomodeloService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/veiculo/relatorio/Veiculo", 
			authorizationModule=ReportAuthorizationModule.class
)
public class VeiculoReport extends SinedReport<VeiculoFiltro>{

	private VeiculoService veiculoService;
	private VeiculomodeloService veiculomodeloService;
	
	public void setVeiculomodeloService(
			VeiculomodeloService veiculomodeloService) {
		this.veiculomodeloService = veiculomodeloService;
	}
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	VeiculoFiltro filtro) throws Exception {
		return veiculoService.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "veiculo";
	}
	
	@Override
	public String getTitulo(VeiculoFiltro filtro) {
		return "VE�CULO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, VeiculoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getEmpresa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		
		if(filtro.getVeiculomodelo() != null){
			Veiculomodelo modelo = veiculomodeloService.load(filtro.getVeiculomodelo(), "veiculomodelo.nome");
			report.addParameter("modelo", modelo.getNome());
		}
	}

}
