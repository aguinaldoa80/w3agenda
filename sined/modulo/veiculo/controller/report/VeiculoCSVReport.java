package br.com.linkcom.sined.modulo.veiculo.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoFiltro;

@Controller(
			path = "/veiculo/relatorio/VeiculoCSV", 
			authorizationModule=ReportAuthorizationModule.class
)
public class VeiculoCSVReport extends ResourceSenderController<VeiculoFiltro>{

	private VeiculoService veiculoService;
	
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,	VeiculoFiltro filtro) throws Exception {
		return veiculoService.gerarRelatorioCSV(filtro);
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	VeiculoFiltro filtro) throws Exception {
		return null;
	}

}
