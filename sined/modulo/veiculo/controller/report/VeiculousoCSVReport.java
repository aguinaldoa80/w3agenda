package br.com.linkcom.sined.modulo.veiculo.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculousoFiltro;

@Controller(
			path = "/veiculo/relatorio/VeiculousoCSV", 
			authorizationModule=ReportAuthorizationModule.class
)
public class VeiculousoCSVReport extends ResourceSenderController<VeiculousoFiltro>{

	private VeiculousoService veiculousoService;
	
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,	VeiculousoFiltro filtro) throws Exception {
		return veiculousoService.gerarRelatorioCSV(filtro);
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	VeiculousoFiltro filtro) throws Exception {
		return null;
	}

}
