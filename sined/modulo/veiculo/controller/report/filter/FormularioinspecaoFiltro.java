package br.com.linkcom.sined.modulo.veiculo.controller.report.filter;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.bean.Veiculotipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FormularioinspecaoFiltro extends FiltroListagemSined{
	
	protected Boolean visual = Boolean.FALSE;
	protected Colaborador cooperado;
	protected Veiculo veiculo;
	protected Veiculomodelo modelo;
	protected Veiculotipo tipo;
	protected Date dtultima;
	protected Date dtagendamento;
	protected String ordenacao;
	
	protected String whereInVeiculo;
	
	{
		this.ordenacao = "cooperado";
	}
	
	//GET's
	@Required
	@DisplayName("Tipos de Itens de Inspe��o")
	public Boolean getVisual() {
		return visual;
	}
	@DisplayName("Cooperado")
	public Colaborador getCooperado() {
		return cooperado;
	}
	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public Veiculotipo getTipo() {
		return tipo;
	}
	public Veiculomodelo getModelo() {
		return modelo;
	}
	
	@DisplayName("Data da �ltima inspe��o")
	public Date getDtultima() {
		return dtultima;
	}

	@DisplayName("Data de agendamento")
	public Date getDtagendamento() {
		return dtagendamento;
	}
	@Required
	@DisplayName("Ordena��o")
	public String getOrdenacao() {
		return ordenacao;
	}
	public String getWhereInVeiculo() {
		return whereInVeiculo;
	}
	
	//SET's
	public void setVisual(Boolean visual) {
		this.visual = visual;
	}
	public void setCooperado(Colaborador cooperado) {
		this.cooperado = cooperado;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setTipo(Veiculotipo tipo) {
		this.tipo = tipo;
	}
	public void setModelo(Veiculomodelo modelo) {
		this.modelo = modelo;
	}
	public void setDtultima(Date dtultima) {
		this.dtultima = dtultima;
	}
	public void setDtagendamento(Date dtagendamento) {
		this.dtagendamento = dtagendamento;
	}
	public void setOrdenacao(String ordenacao) {
		this.ordenacao = ordenacao;
	}
	public void setWhereInVeiculo(String whereInVeiculo) {
		this.whereInVeiculo = whereInVeiculo;
	}
}
