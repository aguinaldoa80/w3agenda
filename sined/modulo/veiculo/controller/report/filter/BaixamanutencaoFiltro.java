package br.com.linkcom.sined.modulo.veiculo.controller.report.filter;


import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;


public class BaixamanutencaoFiltro extends FiltroListagemSined{
	
	protected Boolean status = Boolean.FALSE;
	protected Date dtinicio;
	protected Date dtfim;
	protected String placa;
	
	@Required
	@DisplayName("Situa��o da manuten��o")
	public Boolean getStatus() {
		return status;
	}
	@Required
	@DisplayName("DE")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("AT�")
	public Date getDtfim() {
		return dtfim;
	}
	
	@DisplayName("Placa")
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
}
