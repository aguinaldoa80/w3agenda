package br.com.linkcom.sined.modulo.veiculo.controller.report.filter;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FichausoveiculoFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Veiculo veiculo;
	protected Date data;

	{
		this.data = SinedDateUtils.currentDate();
	}

	@DisplayName("Empresa")
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}

	@DisplayName("Data")
	@Required
	public Date getData() {
		return data;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public void setData(Date dtinicio) {
		this.data = dtinicio;
	}

}
