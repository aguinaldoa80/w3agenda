package br.com.linkcom.sined.modulo.veiculo.controller.report.filter;


import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;


public class AnaliticoFiltro extends FiltroListagemSined{
	
	protected Colaborador colaborador;
	protected Veiculo veiculo;
	protected Veiculomodelo modelo;
	protected Date dtinicio;
	protected Date dtfim;


	@DisplayName("Colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Placa")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public Veiculomodelo getModelo() {
		return modelo;
	}
	@Required
	@DisplayName("DE")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("AT�")
	public Date getDtfim() {
		return dtfim;
	}


	//SET's
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setModelo(Veiculomodelo modelo) {
		this.modelo = modelo;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
}
