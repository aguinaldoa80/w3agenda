package br.com.linkcom.sined.modulo.veiculo.controller.report.filter;


import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;


public class VeiculoagendamentoFiltro extends FiltroListagemSined{
	
	protected Colaborador cooperado;
	protected Veiculo veiculo;
	protected Date dtagendamento;
	protected Date dtfim;

	
	//GET's
	
	@DisplayName("Cooperado")
	public Colaborador getCooperado() {
		return cooperado;
	}
	@DisplayName("Ve�culo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@Required
	@DisplayName("DE")
	public Date getDtagendamento() {
		return dtagendamento;
	}
	@Required
	@DisplayName("AT�")
	public Date getDtfim() {
		return dtfim;
	}
	//SET's

	public void setCooperado(Colaborador cooperado) {
		this.cooperado = cooperado;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public void setDtagendamento(Date dtagendamento) {
		this.dtagendamento = dtagendamento;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
}
