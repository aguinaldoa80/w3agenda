package br.com.linkcom.sined.modulo.veiculo.controller.process;

import java.util.List;
import java.util.Vector;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ArquivoQuilometragem;
import br.com.linkcom.sined.geral.bean.ConsistenciaKmTO;
import br.com.linkcom.sined.geral.bean.Veiculokm;
import br.com.linkcom.sined.geral.service.VeiculokmService;
import br.com.linkcom.sined.util.SinedException;

@Controller(
		path="/veiculo/process/ImportarQuilometragemMemphis",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ImportarQuilometragemMemphisProcess extends MultiActionController {

	private VeiculokmService veiculokmService;
	
	public void setVeiculokmService(VeiculokmService veiculokmService) {
		this.veiculokmService = veiculokmService;
	}
		
	/**
	 * A��o Padr�o - exibe formul�rio para escolha do arquivo de importa��o de quilometragem
	 * 
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @author Rafael Odon
	 */
	@DefaultAction
	public ModelAndView exibir(WebRequestContext request, ArquivoQuilometragem bean){
		return new ModelAndView("process/importarQuilometragemMemphisFormulario","filtro",bean);
	}
	
	/**
	 * Recebe um arquivo de texto, coleta dados das suas linhas
	 * importando quilometragens dos ve�culos para o sistema.
	 * 
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @throws W3AutoException
	 * @author Rafael Odon
	 * 
	 * @see br.com.linkcom.w3auto.geral.service.KmService#importarArquivo(Arquivo, List, List)
	 */
	public ModelAndView importar(WebRequestContext request, ArquivoQuilometragem bean) throws Exception{			
											
		try{
			//chama a importa��o do service passando por refer�ncia as listas a serem usadas na vis�o
			List<ConsistenciaKmTO> consistencia = new Vector<ConsistenciaKmTO>();
			List<Veiculokm> kmAcima5000 = new Vector<Veiculokm>();			
			int tam = veiculokmService.importarArquivo(bean.getArquivo(), consistencia, kmAcima5000);
						
			//sucesso
			if(tam == 0 || (consistencia.size() > 0 && consistencia.size() == tam))
				request.addError("A importa��o n�o foi realizada com sucesso.");
			else
			if(!consistencia.isEmpty())				
				request.addError("A importa��o foi realizada com sucesso mas cont�m inconsist�ncias.");
			else				
				request.addMessage("A importa��o do arquivo foi realizada com sucesso.");
			
			request.setAttribute("tam", tam);
			
			//adiciona lista de consistencia e de quilometragem acima de 500 ao request
			request.setAttribute("consistencia", consistencia);
			request.setAttribute("kmAcima5000", kmAcima5000);	
			
		}catch(SinedException e){
			request.addError(e.getMessage());
			return new ModelAndView("process/importarQuilometragemMemphisFormulario","filtro",bean);
		}
		
		return new ModelAndView("process/importarQuilometragemMemphisConfirmacao");
	}
}
