package br.com.linkcom.sined.modulo.veiculo.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.view.Vwveiculosseminspecao3;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.modulo.veiculo.controller.process.filter.BuscarVeiculosSemInspecaoFiltro;


@Controller(
		path="/veiculo/process/BuscarVeiculosSemInspecao",
		authorizationModule=ProcessAuthorizationModule.class
)
public class BuscarVeiculosSemInspecaoProcess extends MultiActionController{

	protected VeiculoService veiculoService;
		
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	
	/**
	 * A��o Padr�o - exibe veiculos com items vencidos
	 * 
	 * @see br.com.linkcom.w3auto.geral.service.ParametroService#valorParameterDias()
	 * @see br.com.linkcom.w3auto.geral.service.ParametroService#saveNewParameterDias(String)
	 * @see br.com.linkcom.w3auto.geral.service.VeiculoService#findVeiculosAgendados(BuscarVeiculosSemInspecaoFiltro)
	 * @see br.com.linkcom.w3auto.util.W3AutoUtil#isLogadoComPapel(Papel papel)
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @author Rafael Odon, Jo�o Paulo Zica
	 */
	@DefaultAction
	@Command(session = true)
	public ModelAndView exibir(WebRequestContext request, BuscarVeiculosSemInspecaoFiltro filtro){
		
		Boolean todos = Boolean.FALSE;
		
//		String valorDias = parametroService.valorParameterDias().getValor();
		if (filtro.isAsc() && !filtro.getOrderBy().equals("")) {
			filtro.setOrdenacao(filtro.getOrderBy() + " asc");
		}else if (!filtro.isAsc() && !filtro.getOrderBy().equals("")) {
			filtro.setOrdenacao(filtro.getOrderBy() + " desc");
		}
		
//		if (filtro.getValor() != null && !filtro.getValor().equals("") && !filtro.getValor().equals(valorDias)) {
//			parametroService.saveNewParameterDias(filtro.getValor());
//		}else{
//			if(filtro.getValor() == null){
//				filtro.setValor(valorDias);
//			}
//		}

		
		
		//busca os veiculos sem inspe��o a partir da view no banco
//		List<Vwveiculosseminspecao2> lista = veiculoService.findVeiculosAgendados(filtro);
		List<Vwveiculosseminspecao3> lista = veiculoService.findVeiculosAgendados(filtro);
		
		//adiciona as lista ao request
		if (filtro.getInspecao() == null) {
			todos = Boolean.TRUE;
		}
		request.setAttribute("todos", todos);
		request.setAttribute("lista", lista);
			
		return new ModelAndView("process/buscarVeiculosSemInspecaoListagem", "filtro", filtro);
	}
}
