package br.com.linkcom.sined.modulo.veiculo.controller.process.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AgendarVeiculoInspecaoFiltro extends FiltroListagemSined {

	protected String ordenacao;

	public String getOrdenacao() {
		return ordenacao;
	}
	
	public void setOrdenacao(String ordenacao) {
		this.ordenacao = ordenacao;
	}
	
}

