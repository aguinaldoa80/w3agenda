package br.com.linkcom.sined.modulo.veiculo.controller.process.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BuscarVeiculosSemInspecaoFiltro extends FiltroListagemSined{

	protected String valor;
	protected Boolean status;
	protected String ordenacao;
	protected Boolean inspecao;

	@DisplayName("")	
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	@DisplayName("Status")
	public Boolean getStatus() {
		return status;
	}
	
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	public String getOrdenacao() {
		return ordenacao;
	}
	
	public void setOrdenacao(String ordenacao) {
		this.ordenacao = ordenacao;
	}
	
	@DisplayName("Inspe��o")	
	public Boolean getInspecao() {
		return inspecao;
	}
	
	public void setInspecao(Boolean inspecao) {
		this.inspecao = inspecao;
	}
	
}

