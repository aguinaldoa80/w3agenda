package br.com.linkcom.sined.modulo.veiculo.controller.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import org.springframework.web.servlet.ModelAndView;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Categoriaveiculo;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.service.CategoriaveiculoService;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.util.SinedUtil;


@Controller(
		path="/veiculo/process/LocalizarVeiculoItemVencido",
		authorizationModule=ProcessAuthorizationModule.class
)
public class LocalizarVeiculoItemVencidoProcess extends MultiActionController{

	private VeiculoService veiculoService;
	private CategoriaveiculoService categoriaveiculoService;
	
	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}
	
	public void setCategoriaveiculoService(
			CategoriaveiculoService categoriaveiculoService) {
		this.categoriaveiculoService = categoriaveiculoService;
	}
	
	/**
	 * A��o Padr�o - exibe v�iculos com items vencidos
	 * 
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @author Rafael Odon
	 */
	@DefaultAction
	public ModelAndView exibir(WebRequestContext request){
		
		//busca os veiculos vencidos a partir da view no banco
		//List<Vwveiculosvencidos> lista = veiculoService.findVencidos();
		
		//gera tr�s listas, uma para cada tipo de vencimento
		List<Veiculo> listaTipo1 = new Vector<Veiculo>();
		List<Veiculo> listaTipo2 = new Vector<Veiculo>();
		List<Veiculo> listaTipo3 = new Vector<Veiculo>();
		List<Veiculo> listaTipo4 = new Vector<Veiculo>();
	
		List<Veiculo> listaVeiculos = veiculoService.findVeiculoListForValidaItemCategoriaVencido();
		HashMap<Integer, List<Categoriaiteminspecao>> itemInspecaoCategoriaMap = new HashMap<Integer, List<Categoriaiteminspecao>>();
		StringBuilder whereIn = new StringBuilder();
		
		for (Veiculo veiculo: listaVeiculos) {
			if (veiculo.getVeiculomodelo() != null) {
				if (veiculo.getVeiculomodelo().getCategoriaveiculo() != null) {
					whereIn.append(veiculo.getVeiculomodelo().getCategoriaveiculo().getCdcategoriaveiculo().toString() + ",");
				}
			}		
		}

		List<Categoriaveiculo> listaCategorias = whereIn.length() > 0 ? categoriaveiculoService.loadCategoriaveiculoWithListaItemInspecao(whereIn.deleteCharAt(whereIn.length() - 1).toString(), "", false) : new ArrayList<Categoriaveiculo>();
		
		if (SinedUtil.isListNotEmpty(listaCategorias)) {
			for (Categoriaveiculo categoria : listaCategorias) {
				itemInspecaoCategoriaMap.put(categoria.getCdcategoriaveiculo(), categoria.getListaCategoriaiteminspecao());
			}
		}	

		for (Veiculo veiculo : listaVeiculos) {
			
			
			Boolean isItemInspecaoVencido = veiculoService.validaVeiculoItemInspecaoVencido(veiculo, itemInspecaoCategoriaMap, listaCategorias);
			Integer listaTipo = veiculoService.validaVeiculoItemInspecaoSituacao(veiculo, itemInspecaoCategoriaMap, listaCategorias);
			
			if (isItemInspecaoVencido && listaTipo < 3) {
				listaTipo4.add(veiculo);
			} else if(listaTipo == 1){
					listaTipo1.add(veiculo);
			} else if(listaTipo == 2){
					listaTipo2.add(veiculo);
			} else if(listaTipo == 3){
					listaTipo3.add(veiculo);
				}
			} 
		
//		//percorre a lista de vencidos, jogando cada veiculo vencimento em sua respectiva lista
//		for(Vwveiculosvencidos vwveiculosvencidos:lista){
//			if(vwveiculosvencidos.getTipovencimento() == Vwveiculosvencidos.TIPO_VENCIMENTO_1)
//				listaTipo1.add(vwveiculosvencidos.getVeiculo());
//			else
//			if(vwveiculosvencidos.getTipovencimento() == Vwveiculosvencidos.TIPO_VENCIMENTO_2)
//				listaTipo2.add(vwveiculosvencidos.getVeiculo());
//			else
//			if(vwveiculosvencidos.getTipovencimento() == Vwveiculosvencidos.TIPO_VENCIMENTO_3)
//				listaTipo3.add(vwveiculosvencidos.getVeiculo());	
//			else
//			if(vwveiculosvencidos.getTipovencimento() == Vwveiculosvencidos.TIPO_VENCIMENTO_4)
//				listaTipo4.add(vwveiculosvencidos.getVeiculo());
//				
//		}
		
		//adiciona as listas ao request
		request.setAttribute("listaTipo1", listaTipo1);
		request.setAttribute("listaTipo2", listaTipo2);
		request.setAttribute("listaTipo3", listaTipo3);
		request.setAttribute("listaTipo4", listaTipo4);
			
		return new ModelAndView("process/localizarVeiculoItemVencidoListagem");
	}
	
	/**
	 * detalha os �tens vencidos do ve�culo
	 * 
	 * @param request, veiculo
	 * @param filtro
	 * @see br.com.linkcom.w3auto.geral.service.VeiculoService#findComItensVecidos(Veiculo veiculo)
	 * @return ModelAndView
	 * @author Rafael Odon
	 */
	public ModelAndView detalhar(WebRequestContext request, Veiculo veiculo){
		
		//buscao o veiculo com os detalhes de vencimento
		veiculo = veiculoService.findComItensVecidos(veiculo);
								
		return new ModelAndView("process/localizarVeiculoItemVencidoDetalhe","veiculo",veiculo);
	}
}
