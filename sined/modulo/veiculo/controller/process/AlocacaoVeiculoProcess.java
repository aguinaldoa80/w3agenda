package br.com.linkcom.sined.modulo.veiculo.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculousoapoioFiltro;

@Controller(
		path="/veiculo/process/AlocacaoVeiculo",
		authorizationModule=ProcessAuthorizationModule.class
)
public class AlocacaoVeiculoProcess extends MultiActionController {
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, VeiculousoapoioFiltro filtro) {
		return new ModelAndView("process/alocacaoveiculo","filtro",new VeiculousoapoioFiltro());
	}
	

}
