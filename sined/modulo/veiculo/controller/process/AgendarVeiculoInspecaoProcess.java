package br.com.linkcom.sined.modulo.veiculo.controller.process;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.view.Vwveiculoseminspecaovencido3;
import br.com.linkcom.sined.geral.service.VeiculoService;
import br.com.linkcom.sined.geral.service.VeiculoagendamentoService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.geral.service.VeiculousoService;
import br.com.linkcom.sined.modulo.veiculo.controller.process.filter.AgendarVeiculoInspecaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;

@Controller(
		path="/veiculo/process/AgendarVeiculoInspecao",
		authorizationModule=ProcessAuthorizationModule.class
)
public class AgendarVeiculoInspecaoProcess extends MultiActionController {

	private VeiculoService veiculoService;
	private VeiculoordemservicoService veiculoordemservicoService;
	private VeiculousoService veiculousoService;
	private VeiculoagendamentoService veiculoagendamentoService;

	public void setVeiculoordemservicoService(
			VeiculoordemservicoService veiculoordemservicoService) {
		this.veiculoordemservicoService = veiculoordemservicoService;
	}

	public void setVeiculoService(VeiculoService veiculoService) {
		this.veiculoService = veiculoService;
	}

	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}

	public void setVeiculoagendamentoService(
			VeiculoagendamentoService veiculoagendamentoService) {
		this.veiculoagendamentoService = veiculoagendamentoService;
	}
	
	/**
	 * A��o Padr�o - exibe a listas dos ve�culo com itens vencidos
	 * 
	 * @param request
	 * @return ModelAndView
	 * @see br.com.linkcom.w3auto.geral.service.VeiculoService#findForAgendamento()
	 * @author Rafael Odon
	 */
	@DefaultAction
	public ModelAndView exibir(WebRequestContext request, AgendarVeiculoInspecaoFiltro filtro){
		
		if (filtro.isAsc() && !filtro.getOrderBy().equals("")) {
			filtro.setOrdenacao(filtro.getOrderBy() + " asc");
		}else if (!filtro.isAsc() && !filtro.getOrderBy().equals("")) {
			filtro.setOrdenacao(filtro.getOrderBy() + " desc");
		}
		
				
		//busca os veiculos vencidos a partir da view no banco
//		List<Vwveiculoseminspecaovencido2> lista = veiculoService.findForAgendamento(filtro);
		List<Vwveiculoseminspecaovencido3> lista = veiculoService.findForAgendamento(filtro);
		//adiciona as lista ao request
		request.setAttribute("lista", lista);
		
		//adiciona o valor do parametro dias ao request
//		request.setAttribute("valorDias",parametroService.valorParameterDias().getValor());

		return new ModelAndView("process/agendarVeiculoInspecaoListagem");
	}

	/**
	 * Exibe tela de agendamento para um determinado ve�culo
	 * 
	 * @param request
	 * @return ModelAndView
	 * @see br.com.linkcom.w3auto.geral.service.VeiculoService#loadForAgendamento(Veiculo veiculo)
	 * @author Rafael Odon
	 */
	public ModelAndView agendar(WebRequestContext request, Veiculo veiculo){

		veiculo = veiculoService.loadForAgendamento(veiculo);
		Veiculoordemservico ordemservico = new Veiculoordemservico();
		ordemservico.setVeiculo(veiculo);		
		return new ModelAndView("process/agendarVeiculoInspecaoEntrada","ordemservico",ordemservico);
	}

	/**
	 * <b>Exibe tela de agendamento para um determinado ve�culo<br>
	 * � feito tamb�m o tratamento da op��o do usu�rio de enviar email, caso<br>
	 * sim o tratamento � feito neste m�todo onde � redirecionado para outra p�gina<br>
	 * respectiva</b>
	 * 
	 * @param request
	 * @return ModelAndView
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoService#saveAgendamento(Ordemservico ordemservico)
	 * @see br.com.linkcom.w3auto.geral.service.VeiculoService#loadForAgendamento(Veiculo veiculo)
	 * @throws Exception
	 * @author Rafael Odon, Biharck Ara�jo
	 */

	public ModelAndView concluir(WebRequestContext request, Veiculoordemservico ordemservico) throws Exception{
		if(veiculoagendamentoService.existeAgendamentoData(ordemservico)){
			request.addError("Agendamento j� cadastrado no sistema para esse ve�culo.");
			return new ModelAndView("redirect:/veiculo/process/AgendarVeiculoInspecao?ACAO=agendar&cdveiculo=" + ordemservico.getVeiculo().getCdveiculo());
		}else if(this.veiculousoService.existeVeiculoAlocadoData(ordemservico)) {
			request.addError("Ve�culo j� possui aloca��o na data.");
			return new ModelAndView("redirect:/veiculo/process/AgendarVeiculoInspecao?ACAO=agendar&cdveiculo=" + ordemservico.getVeiculo().getCdveiculo());
		}
		//salva o agendamento primeiro...
		try{
			veiculoordemservicoService.saveAgendamento(ordemservico, Boolean.TRUE);							
			//se for necess�rio enviar email...
			if(ordemservico.getEnviaemail()){
				Veiculo veiculo = veiculoService.loadForAgendamento(ordemservico.getVeiculo());
				ordemservico.setVeiculo(veiculo);
				//se a pessoa n�o tiver email...
				if(veiculo.getColaborador().getEmail()!=null && !veiculo.getColaborador().getEmail().equals("")){
					request.getSession().setAttribute("teste", Boolean.TRUE);
					return new ModelAndView("process/enviarEmailAgendamento","ordemservico",ordemservico);
				}else{
					request.getSession().setAttribute("teste", Boolean.FALSE);
					return new ModelAndView("process/enviarEmailAgendamento","ordemservico",ordemservico);
				}
			}
			request.addMessage("Agendamento realizado com sucesso.");
					
		}catch(DataIntegrityViolationException e){
			if (DatabaseError.isKeyPresent(e, "ordemservico_idx"))						
				throw new Exception("Agendamento j� cadastrado no sistema.");			
		}
		
		AgendarVeiculoInspecaoFiltro filtro = new AgendarVeiculoInspecaoFiltro();
		return exibir(request, filtro);
	}

	/**
	 * <b>M�todo repons�vel em receber informa��es do jsp evniarEmailAgendamento.jsp, onde s�o feitos os tratamentos<br>
	 * nescess�rio antes de enviar o email, verificando se o cooperado possui email ou n�o sendo redirecionado para tal<br>
	 * tratamento respectivamente</b>
	 * @param requestContext
	 * @param ordemservico
	 * @return exibir(requestContext)
	 * @see br.com.linkcom.w3auto.geral.service.VeiculoService#loadForAgendamento(Veiculo veiculo)
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoService#loadDtPrevista(Ordemservico ordemservico)
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoService#enviaEmailAgendamento(Ordemservico ordemservico)
	 * @throws W3AutoException
	 * @author Biharck
	 */
	public ModelAndView enviarEmail(WebRequestContext requestContext, Veiculoordemservico ordemservico){
		try {			
			Veiculo veiculo = veiculoService.loadForAgendamento(ordemservico.getVeiculo());
			ordemservico.setVeiculo(veiculo);
			String email = ordemservico.getVeiculo().getColaborador().getEmail();
			
			ordemservico.setDtprevista(veiculoordemservicoService.loadDtPrevista(ordemservico).getDtprevista());
			if(email == null || email.equals("")){
				
				requestContext.addMessage("Agendamento realizado com sucesso.");
				requestContext.addMessage("Colaborador n�o possui e-mail cadastrado no sistema.");
				requestContext.addMessage("Gentileza entrar em contato com o colaborador para a assinatura do comprovante de agendamento.");
				ordemservico.setEnviaemail(Boolean.FALSE);
				
			}else{
				ordemservico.getVeiculo().getColaborador().setEmail(email);
				
				veiculoordemservicoService.enviaEmailAgendamento(ordemservico);
				requestContext.addMessage("Agendamento realizado com sucesso.");
				requestContext.addMessage("E-mail enviado com sucesso.");
			}
			
			veiculoordemservicoService.saveAgendamento(ordemservico, Boolean.FALSE);
			veiculoordemservicoService.saveOrUpdate(ordemservico);
		} catch (Exception e) {
			throw new SinedException("Ocorreram problemas ao enviar email :"+e);
		}
		AgendarVeiculoInspecaoFiltro filtro = new AgendarVeiculoInspecaoFiltro();
		return exibir(requestContext,filtro);
	}
}
