package br.com.linkcom.sined.modulo.veiculo.controller.process;


import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.geral.service.InspecaoitemService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoService;
import br.com.linkcom.sined.geral.service.VeiculoordemservicoitemService;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculomanutencaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;


@Bean
@Controller(path="/veiculo/process/BaixaManutencao",
		authorizationModule=ProcessAuthorizationModule.class
)
public class BaixaManutencaoProcess extends MultiActionController {

	protected VeiculoordemservicoitemService veiculoordemservicoitemService;
	protected InspecaoitemService inspecaoitemService;
	protected VeiculoordemservicoService veiculoordemservicoService;
	
	public void setVeiculoordemservicoitemService(
			VeiculoordemservicoitemService veiculoordemservicoitemService) {
		this.veiculoordemservicoitemService = veiculoordemservicoitemService;
	}
	public void setInspecaoitemService(InspecaoitemService inspecaoitemService) {
		this.inspecaoitemService = inspecaoitemService;
	}
	public void setVeiculoordemservicoService(
			VeiculoordemservicoService veiculoordemservicoService) {
		this.veiculoordemservicoService = veiculoordemservicoService;
	}
	
	/**
	 * M�todo default do controler, carrega o filtro no jsp
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Jo�o Paulo Zica
	 */
	@DefaultAction
	public ModelAndView alterar(WebRequestContext request, VeiculomanutencaoFiltro filtro) throws Exception {
		return new ModelAndView("process/baixamanutencao", "filtro", filtro);
	}

	/**
	 * M�todo para carregar listagem do filtro no jsp via ajax.
	 * 
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoitemService#findOrdemServicoItemByManutencaoFilter(ManutencaoFiltro, Boolean)
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @author Jo�o Paulo Zica
	 */
	@Command(session = true)
	public ModelAndView listar(WebRequestContext request, VeiculomanutencaoFiltro filtro){
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		List<Veiculoordemservicoitem> listaOrdemservicoitem = veiculoordemservicoitemService.findOrdemServicoItemByManutencaoFilter(filtro);
		request.getSession().setAttribute("listaOrdemservicoitem", listaOrdemservicoitem);
		return new ModelAndView("direct:process/baixamanutencaolistagem", "listaOrdemservicoitem", listaOrdemservicoitem);
	}

	/**
	 * M�todo para atualizar os dados alterados no itens selecionado.
	 * 
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoitemService#carregaOrdemServicoItem(Ordemservicoitem)
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoService#saveOrdemServico(Integer, Date)
	 * @param request
	 * @param ordemservicoitem
	 * @author Jo�o Paulo Zica
	 * @throws ParseException 
	 */
	public void recebedados(WebRequestContext request,Veiculoordemservicoitem ordemservicoitem) throws ParseException{
		
		View view = View.getCurrent();

		String[] cds = request.getParameter("cd").split(",");
		
		Date dataatual = new Date(System.currentTimeMillis());
		Date data = SinedDateUtils.stringToDate(request.getParameter("data"));
		
		DateFormat df = DateFormat.getDateInstance();
		
		if (dataatual.before(data) && !df.format(dataatual).equals(df.format(data))) {
			view.println("var msgData = 'A Data da manuten��o n�o pode ser uma data futura!';");
		}else{			
			String local = request.getParameter("local");
			String observacao = request.getParameter("observacao");
			for (String cd : cds) {
				ordemservicoitem.setCdveiculoordemservicoitem(Integer.parseInt(cd));
				Veiculoordemservicoitem ordemservicoitemLoad = veiculoordemservicoitemService.carregaOrdemServicoItem(ordemservicoitem);
				ordemservicoitemLoad.setObservacao(observacao);
				ordemservicoitemLoad.setLocalmanut(local);
				ordemservicoitemLoad.setStatus(Boolean.TRUE);
				veiculoordemservicoService.saveOrdemServico(ordemservicoitemLoad.getOrdemservico().getCdveiculoordemservico(), data);
				veiculoordemservicoitemService.saveOrUpdate(ordemservicoitemLoad);
				if (veiculoordemservicoitemService.findOrdemServicoItemInspecao(ordemservicoitemLoad) != null) {				
					Integer cdordemservicoiteminspecao = veiculoordemservicoitemService.findOrdemServicoItemInspecao(ordemservicoitemLoad).getCdveiculoordemservicoitem();
					veiculoordemservicoitemService.saveOrdemServicoItem(cdordemservicoiteminspecao);
				}
			}
			view.println("var msgData = '';");
		}
	}
	
	/**
	 * M�todo para atualizar os dados alterados em todos os itens da lista.
	 *
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoitemService#carregaOrdemServicoItem(Ordemservicoitem)
	 * @see br.com.linkcom.w3auto.geral.service.OrdemservicoService#saveOrdemServico(Integer, Date)
	 * @param request
	 * @param ordemservicoitem
	 * @author Jo�o Paulo Zica
	 * @throws ParseException 
	 */
	@SuppressWarnings("unchecked")
	public void recebealldados(WebRequestContext request,Veiculoordemservicoitem ordemservicoitem) throws ParseException{
		
		View view = View.getCurrent();
		
		List<Veiculoordemservicoitem> lista = (List<Veiculoordemservicoitem>) request.getSession().getAttribute("listaOrdemservicoitem");
		
		Date dataatual = new Date(System.currentTimeMillis());
		Date data = SinedDateUtils.stringToDate(request.getParameter("data"));
		String local = request.getParameter("local");
		String observacao = request.getParameter("observacao");
		DateFormat df = DateFormat.getDateInstance();
		if (dataatual.before(data) && !df.format(dataatual).equals(df.format(data))) {
			view.println("var msgData = 'A Data da manuten��o n�o pode ser uma data futura!';");
		}else{
			for (Veiculoordemservicoitem ordemservicoitem2 : lista) {
				ordemservicoitem = veiculoordemservicoitemService.carregaOrdemServicoItem(ordemservicoitem2);
				ordemservicoitem.setObservacao(observacao);
				ordemservicoitem.setLocalmanut(local);
				ordemservicoitem.setStatus(Boolean.TRUE); 
				veiculoordemservicoService.saveOrdemServico(ordemservicoitem.getOrdemservico().getCdveiculoordemservico(), data);
				veiculoordemservicoitemService.saveOrUpdate(ordemservicoitem);
				if (veiculoordemservicoitemService.findOrdemServicoItemInspecao(ordemservicoitem) != null) {				
					Integer cdordemservicoiteminspecao = veiculoordemservicoitemService.findOrdemServicoItemInspecao(ordemservicoitem).getCdveiculoordemservicoitem();
					veiculoordemservicoitemService.saveOrdemServicoItem(cdordemservicoiteminspecao);
				}
			}
			view.println("var msgData = '';");
		}	
	}
}