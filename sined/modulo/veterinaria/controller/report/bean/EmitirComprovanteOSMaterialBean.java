package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class EmitirComprovanteOSMaterialBean {

	private String material_servico;
	private String unidade_medida;
	private Double valor_unitario;
	private String via;
	private Integer ritmo;
	private Integer quantidade_prevista;
	private Integer quantidade_usada;
	private Money valor_total;
	private String informacoes;
	private String lote;

	public String getMaterial_servico() {
		return material_servico;
	}

	public String getUnidade_medida() {
		return unidade_medida;
	}

	public Double getValor_unitario() {
		return valor_unitario;
	}

	public String getVia() {
		return via;
	}

	public Integer getRitmo() {
		return ritmo;
	}

	public Integer getQuantidade_prevista() {
		return quantidade_prevista;
	}

	public Integer getQuantidade_usada() {
		return quantidade_usada;
	}

	public Money getValor_total() {
		return valor_total;
	}

	public String getInformacoes() {
		return informacoes;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setMaterial_servico(String materialServico) {
		material_servico = materialServico;
	}

	public void setUnidade_medida(String unidadeMedida) {
		unidade_medida = unidadeMedida;
	}

	public void setValor_unitario(Double valorUnitario) {
		valor_unitario = valorUnitario;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public void setRitmo(Integer ritmo) {
		this.ritmo = ritmo;
	}

	public void setQuantidade_prevista(Integer quantidadePrevista) {
		quantidade_prevista = quantidadePrevista;
	}

	public void setQuantidade_usada(Integer quantidadeUsada) {
		quantidade_usada = quantidadeUsada;
	}

	public void setValor_total(Money valorTotal) {
		valor_total = valorTotal;
	}

	public void setInformacoes(String informacoes) {
		this.informacoes = informacoes;
	}
}