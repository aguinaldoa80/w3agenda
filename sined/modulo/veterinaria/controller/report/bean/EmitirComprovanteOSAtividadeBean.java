package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

public class EmitirComprovanteOSAtividadeBean {
	
	private String data;
	private String inicio;
	private String termino;
	private String descricao;
	private String tipo_atividade;
	
	public String getData() {
		return data;
	}
	public String getInicio() {
		return inicio;
	}
	public String getTermino() {
		return termino;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getTipo_atividade() {
		return tipo_atividade;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public void setTermino(String termino) {
		this.termino = termino;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setTipo_atividade(String tipoAtividade) {
		tipo_atividade = tipoAtividade;
	}
}