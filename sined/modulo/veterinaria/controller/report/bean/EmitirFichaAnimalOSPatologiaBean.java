package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

public class EmitirFichaAnimalOSPatologiaBean {

	private String data;
	private String nome;
	private String cdAnimalPatologia;
	private String situacaoPatologia;

	public String getData() {
		return data;
	}

	public String getNome() {
		return nome;
	}
	
	public String getCdAnimalPatologia() {
		return cdAnimalPatologia;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public String getSituacaoPatologia() {
		return situacaoPatologia;
	}
	
	public void setSituacaoPatologia(String situacaoPatologia) {
		this.situacaoPatologia = situacaoPatologia;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCdAnimalPatologia(String cdAnimalPatologia) {
		this.cdAnimalPatologia = cdAnimalPatologia;
	}
}
