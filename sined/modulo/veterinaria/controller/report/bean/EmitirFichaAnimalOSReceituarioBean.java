package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

public class EmitirFichaAnimalOSReceituarioBean {
	
	private String data;
	private String responsavel;
	private String observacao;
	
	public String getData() {
		return data;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public String getObservacao() {
		return observacao;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}	
}
