package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

public class EmitirFichaAnimalOSAnamneseBean {

	private String cdanimalanamnese;
	private String anamnese;
	private String data;

	public String getCdanimalanamnese() {
		return cdanimalanamnese;
	}

	public String getAnamnese() {
		return anamnese;
	}

	public String getData() {
		return data;
	}

	public void setCdanimalanamnese(String cdanimalanamnese) {
		this.cdanimalanamnese = cdanimalanamnese;
	}

	public void setAnamnese(String anamnese) {
		this.anamnese = anamnese;
	}

	public void setData(String data) {
		this.data = data;
	}
}
