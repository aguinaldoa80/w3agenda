package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

import java.util.LinkedList;

public class EmitirComprovanteOSBean {
	
	private Boolean ultimoRegistro = Boolean.FALSE;
	private Integer codigo;
	private String data;
	private String previsao;
	private String ordemservico;
	private String empresa;
	private String cliente;
	private String cliente_cpf;
	private String cliente_rg;
	private String cliente_telefone;
	private String cliente_endereco;
	private String animal;
	private String animal_ficha;
	private String animal_idade;
	private String tipo_atividade;
	private String responsavel;
	private String cliente_responsavel_finaiceiro;
	private String situacao;
	private String prioridade;
	private String descricao;
	private String especie;
	private String raca;
	private String pelagem;
	private String sexo;
	private String data_atual_extenso;
	private Double peso_atual;
	private Double peso_anterior;
	private LinkedList<EmitirComprovanteOSMaterialBean> materiais = new LinkedList<EmitirComprovanteOSMaterialBean>();
	private LinkedList<EmitirComprovanteOSAtividadeBean> atividades = new LinkedList<EmitirComprovanteOSAtividadeBean>();
	private LinkedList<EmitirComprovanteOSHistoricoBean> historicos = new LinkedList<EmitirComprovanteOSHistoricoBean>();
	private LinkedList<EmitirComprovanteOSItemBean> camposAdicionais = new LinkedList<EmitirComprovanteOSItemBean>();
	
	
	public Boolean getUltimoRegistro() {
		return ultimoRegistro;
	}
	public void setUltimoRegistro(Boolean ultimoRegistro) {
		this.ultimoRegistro = ultimoRegistro;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getPrevisao() {
		return previsao;
	}
	public void setPrevisao(String previsao) {
		this.previsao = previsao;
	}
	public String getOrdemservico() {
		return ordemservico;
	}
	public void setOrdemservico(String ordemservico) {
		this.ordemservico = ordemservico;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getCliente_cpf() {
		return cliente_cpf;
	}
	public void setCliente_cpf(String clienteCpf) {
		cliente_cpf = clienteCpf;
	}
	public String getCliente_rg() {
		return cliente_rg;
	}
	public void setCliente_rg(String clienteRg) {
		cliente_rg = clienteRg;
	}
	public String getCliente_telefone() {
		return cliente_telefone;
	}
	public void setCliente_telefone(String clienteTelefone) {
		cliente_telefone = clienteTelefone;
	}
	public String getCliente_endereco() {
		return cliente_endereco;
	}
	public void setCliente_endereco(String clienteEndereco) {
		cliente_endereco = clienteEndereco;
	}
	public String getAnimal() {
		return animal;
	}
	public void setAnimal(String animal) {
		this.animal = animal;
	}
	public String getAnimal_ficha() {
		return animal_ficha;
	}
	public void setAnimal_ficha(String animalFicha) {
		animal_ficha = animalFicha;
	}
	public String getAnimal_idade() {
		return animal_idade;
	}
	public void setAnimal_idade(String animalIdade) {
		animal_idade = animalIdade;
	}
	public String getTipo_atividade() {
		return tipo_atividade;
	}
	public void setTipo_atividade(String tipoAtividade) {
		tipo_atividade = tipoAtividade;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getPrioridade() {
		return prioridade;
	}
	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}
	
	public String getCliente_responsavel_finaiceiro() {
		return cliente_responsavel_finaiceiro;
	}
	public void setCliente_responsavel_finaiceiro(
			String clienteResponsavelFinaiceiro) {
		cliente_responsavel_finaiceiro = clienteResponsavelFinaiceiro;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getEspecie() {
		return especie;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public String getRaca() {
		return raca;
	}
	public void setRaca(String raca) {
		this.raca = raca;
	}
	public String getPelagem() {
		return pelagem;
	}
	public void setPelagem(String pelagem) {
		this.pelagem = pelagem;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getData_atual_extenso() {
		return data_atual_extenso;
	}
	public void setData_atual_extenso(String dataAtualExtenso) {
		data_atual_extenso = dataAtualExtenso;
	}
	public Double getPeso_atual() {
		return peso_atual;
	}
	public void setPeso_atual(Double pesoAtual) {
		peso_atual = pesoAtual;
	}
	public Double getPeso_anterior() {
		return peso_anterior;
	}
	public void setPeso_anterior(Double pesoAnterior) {
		peso_anterior = pesoAnterior;
	}
	public LinkedList<EmitirComprovanteOSMaterialBean> getMateriais() {
		return materiais;
	}
	public void setMateriais(LinkedList<EmitirComprovanteOSMaterialBean> materiais) {
		this.materiais = materiais;
	}
	public LinkedList<EmitirComprovanteOSAtividadeBean> getAtividades() {
		return atividades;
	}
	public void setAtividades(
			LinkedList<EmitirComprovanteOSAtividadeBean> atividades) {
		this.atividades = atividades;
	}
	public LinkedList<EmitirComprovanteOSHistoricoBean> getHistoricos() {
		return historicos;
	}
	public void setHistoricos(
			LinkedList<EmitirComprovanteOSHistoricoBean> historicos) {
		this.historicos = historicos;
	}
	public LinkedList<EmitirComprovanteOSItemBean> getCamposAdicionais() {
		return camposAdicionais;
	}
	public void setCamposAdicionais(
			LinkedList<EmitirComprovanteOSItemBean> camposAdicionais) {
		this.camposAdicionais = camposAdicionais;
	}
}