package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

public class EmitirFichaAnimalOSVacinaBean {

	private String data;
	private String nome;

	public String getData() {
		return data;
	}

	public String getNome() {
		return nome;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
