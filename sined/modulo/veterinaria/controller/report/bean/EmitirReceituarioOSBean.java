package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EmitirReceituarioOSBean {

	private String numero_ficha;
	private String data_atual = (new SimpleDateFormat("dd/MM/yyyy - HH:mm").format(new Date()));
	private String data_receitual;
	private String animal_nome;
	private String animal_especie;
	private String animal_raca;
	private String animal_sexo;
	private String animal_pelagem;
	private String animal_idade;
	private String cliente_nome;
	private String cliente_telefone;
	private String cliente_endereco;
	private String receituario;
	private String empresa;
	private String empresa_telefone;

	public String getNumero_ficha() {
		return numero_ficha;
	}

	public String getAnimal_nome() {
		return animal_nome;
	}

	public String getAnimal_especie() {
		return animal_especie;
	}

	public String getAnimal_raca() {
		return animal_raca;
	}

	public String getAnimal_sexo() {
		return animal_sexo;
	}

	public String getAnimal_pelagem() {
		return animal_pelagem;
	}

	public String getAnimal_idade() {
		return animal_idade;
	}

	public String getCliente_nome() {
		return cliente_nome;
	}

	public String getCliente_telefone() {
		return cliente_telefone;
	}

	public String getCliente_endereco() {
		return cliente_endereco;
	}

	public String getReceituario() {
		return receituario;
	}

	public void setNumero_ficha(String numeroFicha) {
		numero_ficha = numeroFicha;
	}

	public void setAnimal_nome(String animalNome) {
		animal_nome = animalNome;
	}

	public void setAnimal_especie(String animalEspecie) {
		animal_especie = animalEspecie;
	}

	public void setAnimal_raca(String animalRaca) {
		animal_raca = animalRaca;
	}

	public void setAnimal_sexo(String animalSexo) {
		animal_sexo = animalSexo;
	}

	public void setAnimal_pelagem(String animalPelagem) {
		animal_pelagem = animalPelagem;
	}

	public void setAnimal_idade(String animalIdade) {
		animal_idade = animalIdade;
	}

	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}

	public void setCliente_telefone(String clienteTelefone) {
		cliente_telefone = clienteTelefone;
	}

	public void setCliente_endereco(String clienteEndereco) {
		cliente_endereco = clienteEndereco;
	}

	public void setReceituario(String receituario) {
		this.receituario = receituario;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getEmpresa_telefone() {
		return empresa_telefone;
	}

	public void setEmpresa_telefone(String empresaTelefone) {
		empresa_telefone = empresaTelefone;
	}

	public String getData_atual() {
		return data_atual;
	}

	public void setData_atual(String dataAtual) {
		data_atual = dataAtual;
	}
	
	public String getData_receitual() {
		return data_receitual;
	}
	
	public void setData_receitual(String data_receitual) {
		this.data_receitual = data_receitual;
	}
}