package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

public class EmitirFichaAnimalOSPesoBean {

	private String data;
	private String descricao;

	public String getData() {
		return data;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
