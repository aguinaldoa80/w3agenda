package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

public class EmitirFichaAnimalOSBean {

	private String data_atual = (new SimpleDateFormat("dd/MM/yyyy �s HH:mm").format(new Date()));
	private String animal_nome;
	private String animal_especie;
	private String animal_raca;
	private String animal_sexo;
	private String animal_pelagem;
	private String animal_idade;
	private String animal_nascimento;
	private String animal_informacao_complementar;
	private String cliente_nome;
	private String cliente_telefone;
	private String cliente_endereco;
	private String empresa;
	private String empresa_telefone;
	private Boolean ultimo_registro = Boolean.FALSE;
	private Integer animal_numero;
	private String animal_chip;
	private String animal_pedigree;
	private String animal_cdAnimalSituacao;
	private String animal_animalSituacaoNome;
	private String animal_peso;
	private String animal_anamnese;
	private String animal_cdpatologia;
	private String animal_patologia;
	private String animal_situacaoPatologia;

	private LinkedList<EmitirFichaAnimalOSHistoricoBean> historicos = new LinkedList<EmitirFichaAnimalOSHistoricoBean>();
	private LinkedList<EmitirFichaAnimalOSVacinaBean> vacinas = new LinkedList<EmitirFichaAnimalOSVacinaBean>();
	private LinkedList<EmitirFichaAnimalOSPesoBean> pesos = new LinkedList<EmitirFichaAnimalOSPesoBean>();
	private LinkedList<EmitirFichaAnimalOSPatologiaBean> patologias = new LinkedList<EmitirFichaAnimalOSPatologiaBean>();
	private LinkedList<EmitirFichaAnimalOSReceituarioBean> receituario = new LinkedList<EmitirFichaAnimalOSReceituarioBean>();
	private LinkedList<EmitirFichaAnimalOSAnamneseBean> anamneses = new LinkedList<EmitirFichaAnimalOSAnamneseBean>();

	public String getData_atual() {
		return data_atual;
	}

	public String getAnimal_nome() {
		return animal_nome;
	}

	public String getAnimal_especie() {
		return animal_especie;
	}

	public String getAnimal_raca() {
		return animal_raca;
	}

	public String getAnimal_sexo() {
		return animal_sexo;
	}

	public String getAnimal_pelagem() {
		return animal_pelagem;
	}

	public String getAnimal_idade() {
		return animal_idade;
	}

	public String getAnimal_nascimento() {
		return animal_nascimento;
	}

	public String getAnimal_informacao_complementar() {
		return animal_informacao_complementar;
	}

	public String getCliente_nome() {
		return cliente_nome;
	}

	public String getCliente_telefone() {
		return cliente_telefone;
	}

	public String getCliente_endereco() {
		return cliente_endereco;
	}

	public String getEmpresa() {
		return empresa;
	}

	public String getEmpresa_telefone() {
		return empresa_telefone;
	}

	public LinkedList<EmitirFichaAnimalOSHistoricoBean> getHistoricos() {
		return historicos;
	}

	public LinkedList<EmitirFichaAnimalOSVacinaBean> getVacinas() {
		return vacinas;
	}

	public LinkedList<EmitirFichaAnimalOSPesoBean> getPesos() {
		return pesos;
	}

	public LinkedList<EmitirFichaAnimalOSPatologiaBean> getPatologias() {
		return patologias;
	}

	public void setData_atual(String dataAtual) {
		data_atual = dataAtual;
	}

	public String getAnimal_chip() {
		return animal_chip;
	}

	public String getAnimal_pedigree() {
		return animal_pedigree;
	}

	public String getAnimal_cdAnimalSituacao() {
		return animal_cdAnimalSituacao;
	}

	public String getAnimal_animalSituacaoNome() {
		return animal_animalSituacaoNome;
	}

	public String getAnimal_peso() {
		return animal_peso;
	}

	public LinkedList<EmitirFichaAnimalOSAnamneseBean> getAnamneses() {
		return anamneses;
	}

	public String getAnimal_anamnese() {
		return animal_anamnese;
	}

	public String getAnimal_cdpatologia() {
		return animal_cdpatologia;
	}

	public String getAnimal_patologia() {
		return animal_patologia;
	}

	public String getAnimal_situacaoPatologia() {
		return animal_situacaoPatologia;
	}

	public Integer getAnimal_numero() {
		return animal_numero;
	}
	
	public void setAnimal_numero(Integer animal_numero) {
		this.animal_numero = animal_numero;
	}
	
	public void setAnimal_anamnese(String animal_anamnese) {
		this.animal_anamnese = animal_anamnese;
	}

	public void setAnimal_cdpatologia(String animal_cdpatologia) {
		this.animal_cdpatologia = animal_cdpatologia;
	}

	public void setAnimal_patologia(String animal_patologia) {
		this.animal_patologia = animal_patologia;
	}

	public void setAnimal_situacaoPatologia(String animal_situacaoPatologia) {
		this.animal_situacaoPatologia = animal_situacaoPatologia;
	}

	public void setAnimal_chip(String animal_chip) {
		this.animal_chip = animal_chip;
	}

	public void setAnimal_pedigree(String animal_pedigree) {
		this.animal_pedigree = animal_pedigree;
	}

	public void setAnimal_cdAnimalSituacao(String animal_cdAnimalSituacao) {
		this.animal_cdAnimalSituacao = animal_cdAnimalSituacao;
	}

	public void setAnimal_animalSituacaoNome(String animal_animalSituacaoNome) {
		this.animal_animalSituacaoNome = animal_animalSituacaoNome;
	}

	public void setAnimal_peso(String animal_peso) {
		this.animal_peso = animal_peso;
	}

	public void setAnamneses(LinkedList<EmitirFichaAnimalOSAnamneseBean> anamneses) {
		this.anamneses = anamneses;
	}

	public void setAnimal_nome(String animalNome) {
		animal_nome = animalNome;
	}

	public void setAnimal_especie(String animalEspecie) {
		animal_especie = animalEspecie;
	}

	public void setAnimal_raca(String animalRaca) {
		animal_raca = animalRaca;
	}

	public void setAnimal_sexo(String animalSexo) {
		animal_sexo = animalSexo;
	}

	public void setAnimal_pelagem(String animalPelagem) {
		animal_pelagem = animalPelagem;
	}

	public void setAnimal_idade(String animalIdade) {
		animal_idade = animalIdade;
	}

	public void setAnimal_nascimento(String animalNascimento) {
		animal_nascimento = animalNascimento;
	}

	public void setAnimal_informacao_complementar(String animalInformacaoComplementar) {
		animal_informacao_complementar = animalInformacaoComplementar;
	}

	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}

	public void setCliente_telefone(String clienteTelefone) {
		cliente_telefone = clienteTelefone;
	}

	public void setCliente_endereco(String clienteEndereco) {
		cliente_endereco = clienteEndereco;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public void setEmpresa_telefone(String empresaTelefone) {
		empresa_telefone = empresaTelefone;
	}

	public void setHistoricos(LinkedList<EmitirFichaAnimalOSHistoricoBean> historicos) {
		this.historicos = historicos;
	}

	public void setVacinas(LinkedList<EmitirFichaAnimalOSVacinaBean> vacinas) {
		this.vacinas = vacinas;
	}

	public void setPesos(LinkedList<EmitirFichaAnimalOSPesoBean> pesos) {
		this.pesos = pesos;
	}

	public void setPatologias(LinkedList<EmitirFichaAnimalOSPatologiaBean> patologias) {
		this.patologias = patologias;
	}

	public Boolean getUltimo_registro() {
		return ultimo_registro;
	}

	public void setUltimo_registro(Boolean ultimoRegistro) {
		ultimo_registro = ultimoRegistro;
	}

	public LinkedList<EmitirFichaAnimalOSReceituarioBean> getReceituario() {
		return receituario;
	}

	public void setReceituario(LinkedList<EmitirFichaAnimalOSReceituarioBean> receituario) {
		this.receituario = receituario;
	}
	
}