package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;


public class EmitirAtestadoObitoBean {

	private String nome_animal;
	private String data_nascimento;
	private String sexo;
	private String especie;
	private String raca;
	private String pelagem;	
	private String nome_cliente;
	private String causa_morte;
	private String data_obito;
	private String hora_obito;
	private String texto_padrao;
	private String observacao;
	private String nome_veterinario;
	
	public String getNome_animal() {
		return nome_animal;
	}
	public String getData_nascimento() {
		return data_nascimento;
	}
	public String getSexo() {
		return sexo;
	}
	public String getEspecie() {
		return especie;
	}
	public String getRaca() {
		return raca;
	}
	public String getPelagem() {
		return pelagem;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public String getCausa_morte() {
		return causa_morte;
	}
	public String getData_obito() {
		return data_obito;
	}
	public String getHora_obito() {
		return hora_obito;
	}
	public String getTexto_padrao() {
		return texto_padrao;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getNome_veterinario() {
		return nome_veterinario;
	}
	
	public void setNome_animal(String nomeAnimal) {
		nome_animal = nomeAnimal;
	}
	public void setData_nascimento(String dataNascimento) {
		data_nascimento = dataNascimento;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public void setRaca(String raca) {
		this.raca = raca;
	}
	public void setPelagem(String pelagem) {
		this.pelagem = pelagem;
	}
	public void setNome_cliente(String nomeCliente) {
		nome_cliente = nomeCliente;
	}
	public void setCausa_morte(String causaMorte) {
		causa_morte = causaMorte;
	}
	public void setData_obito(String dataObito) {
		data_obito = dataObito;
	}
	public void setHora_obito(String horaObito) {
		hora_obito = horaObito;
	}
	public void setTexto_padrao(String textoPadrao) {
		texto_padrao = textoPadrao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setNome_veterinario(String nomeVeterinario) {
		nome_veterinario = nomeVeterinario;
	}
}