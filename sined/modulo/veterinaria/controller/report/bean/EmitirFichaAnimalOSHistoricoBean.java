package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

public class EmitirFichaAnimalOSHistoricoBean {

	private String data;
	private String responsavel;
	private String receita;

	public String getData() {
		return data;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public String getReceita() {
		return receita;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public void setReceita(String receita) {
		this.receita = receita;
	}

}
