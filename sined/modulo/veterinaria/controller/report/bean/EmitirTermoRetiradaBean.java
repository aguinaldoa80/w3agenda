package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;


public class EmitirTermoRetiradaBean {

	private String termo;
	
	public String getTermo() {
		return termo;
	}
	
	public void setTermo(String termo) {
		this.termo = termo;
	}
}