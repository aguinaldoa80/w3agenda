package br.com.linkcom.sined.modulo.veterinaria.controller.report.bean;

public class EmitirComprovanteOSItemBean {
	
	private String campo;
	private String valor;
	
	public String getCampo() {
		return campo;
	}
	public String getValor() {
		return valor;
	}
	
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}	
}
