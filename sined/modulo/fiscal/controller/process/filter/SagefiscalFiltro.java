package br.com.linkcom.sined.modulo.fiscal.controller.process.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.SagefiscalApoio;
import br.com.linkcom.sined.util.SinedDateUtils;

public class SagefiscalFiltro {

	protected Empresa empresa;
	protected Date dtinicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtfim = SinedDateUtils.currentDate();
	protected Boolean buscarentradas = Boolean.TRUE;
	protected Boolean buscarsaidas = Boolean.TRUE;
	protected Boolean gerarinventario = Boolean.FALSE;
	protected Inventario inventario;
	
	private SagefiscalApoio apoio;
	private List<String> listaErro;
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("Data Fim")
	public Date getDtfim() {
		return dtfim;
	}
	
	public Boolean getBuscarentradas() {
		return buscarentradas;
	}
	
	public Boolean getBuscarsaidas() {
		return buscarsaidas;
	}
	
	@DisplayName("INvent�rio")
	public Inventario getInventario() {
		return inventario;
	}
	
	public Boolean getGerarinventario() {
		return gerarinventario;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setBuscarentradas(Boolean buscarentradas) {
		this.buscarentradas = buscarentradas;
	}

	public void setBuscarsaidas(Boolean buscarsaidas) {
		this.buscarsaidas = buscarsaidas;
	}
	
	public SagefiscalApoio getApoio() {
		return apoio;
	}

	public List<String> getListaErro() {
		return listaErro;
	}

	public void setApoio(SagefiscalApoio apoio) {
		this.apoio = apoio;
	}

	public void setListaErro(List<String> listaErro) {
		this.listaErro = listaErro;
	}
	
	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}
	
	public void setGerarinventario(Boolean gerarinventario) {
		this.gerarinventario = gerarinventario;
	}
}
