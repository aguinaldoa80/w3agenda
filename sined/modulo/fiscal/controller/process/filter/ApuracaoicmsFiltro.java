package br.com.linkcom.sined.modulo.fiscal.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.SinedDateUtils;

public class ApuracaoicmsFiltro {

	protected Empresa empresa;
	protected Date dtinicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtfim = SinedDateUtils.lastDateOfMonth();
	protected Boolean entrada = Boolean.TRUE;
	protected Boolean saida = Boolean.TRUE;
		
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("Data Fim")
	public Date getDtfim() {
		return dtfim;
	}
	
	public Boolean getEntrada() {
		return entrada;
	}
	
	@DisplayName("Sa�da")
	public Boolean getSaida() {
		return saida;
	}
	
	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}
	public void setSaida(Boolean saida) {
		this.saida = saida;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
}
