package br.com.linkcom.sined.modulo.fiscal.controller.process.filter;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.GerarLancamentoContabilCentrocustoProjetoBean;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GerarLancamentoContabilFiltro extends FiltroListagemSined implements Cloneable {
	
	private Long millis;
	private Empresa empresa;
	private Date dtPeriodoInicio;
	private Date dtPeriodoFim;
	private Projeto projeto;
	private List<OperacaoContabilTipoLancamento> listaTipoLancamento;
	private List<Movimentacaocontabil> listaMovimentacaocontabil;
	
	private String dtFechamentoContabil;
	
	//Atributos para auxiliar a cria��o da lista de Movimentacaocontabil
	private Map<OperacaoContabilTipoLancamento, List<Operacaocontabil>> mapaTipoLancamentoOperacaoContabil;
	private Centrocusto centrocustoEmpresa;
	
	public GerarLancamentoContabilFiltro(){
		this.listaTipoLancamento = OperacaoContabilTipoLancamento.getLista();
	}
	
	public Long getMillis() {
		return millis;
	}
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data do per�odo inicial")
	public Date getDtPeriodoInicio() {
		return dtPeriodoInicio;
	}
	@Required
	@DisplayName("Data do per�odo final")
	public Date getDtPeriodoFim() {
		return dtPeriodoFim;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Tipo de lan�amento")
	public List<OperacaoContabilTipoLancamento> getListaTipoLancamento() {
		return listaTipoLancamento;
	}
	public List<Movimentacaocontabil> getListaMovimentacaocontabil() {
		return listaMovimentacaocontabil;
	}
	public boolean isTipoLancamento(OperacaoContabilTipoLancamento tipoLancamento){
		return listaTipoLancamento.contains(tipoLancamento);
	}
	
	public Map<OperacaoContabilTipoLancamento, List<Operacaocontabil>> getMapaTipoLancamentoOperacaoContabil() {
		return mapaTipoLancamentoOperacaoContabil;
	}
	public Centrocusto getCentrocustoEmpresa() {
		return centrocustoEmpresa;
	}
	public List<GerarLancamentoContabilCentrocustoProjetoBean> getListaCentrocustoProjetoBean(){
		return Arrays.asList(new GerarLancamentoContabilCentrocustoProjetoBean(centrocustoEmpresa, null));
	}
	public String getDtFechamentoContabil() {
		return dtFechamentoContabil;
	}
	public void setMillis(Long millis) {
		this.millis = millis;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtPeriodoInicio(Date dtPeriodoInicio) {
		this.dtPeriodoInicio = dtPeriodoInicio;
	}
	public void setDtPeriodoFim(Date dtPeriodoFim) {
		this.dtPeriodoFim = dtPeriodoFim;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setListaTipoLancamento(List<OperacaoContabilTipoLancamento> listaTipoLancamento) {
		this.listaTipoLancamento = listaTipoLancamento;
	}
	public void setListaMovimentacaocontabil(List<Movimentacaocontabil> listaBean) {
		this.listaMovimentacaocontabil = listaBean;
	}
	public void setMapaTipoLancamentoOperacaoContabil(Map<OperacaoContabilTipoLancamento, List<Operacaocontabil>> map) {
		this.mapaTipoLancamentoOperacaoContabil = map;
	}
	public void setCentrocustoEmpresa(Centrocusto centrocustoEmpresa) {
		this.centrocustoEmpresa = centrocustoEmpresa;
	}
	public void setDtFechamentoContabil(String dtFechamentoContabil) {
		this.dtFechamentoContabil = dtFechamentoContabil;
	}
	public GerarLancamentoContabilFiltro getClone() throws Exception {
		return (GerarLancamentoContabilFiltro) clone();
	}	
}