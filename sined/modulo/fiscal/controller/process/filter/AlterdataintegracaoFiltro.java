package br.com.linkcom.sined.modulo.fiscal.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.AlterdataModelointegracao;
import br.com.linkcom.sined.util.SinedDateUtils;

public class AlterdataintegracaoFiltro {

	protected Empresa empresa;
	protected Date dtinicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtfim = new Date(System.currentTimeMillis());
	protected AlterdataModelointegracao alterdataModelointegracao;
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data Inicial")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("Data Final")
	public Date getDtfim() {
		return dtfim;
	}
	@Required
	@DisplayName("Tipo")
	public AlterdataModelointegracao getAlterdataModelointegracao() {
		return alterdataModelointegracao;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setAlterdataModelointegracao(AlterdataModelointegracao alterdataModelointegracao) {
		this.alterdataModelointegracao = alterdataModelointegracao;
	}
}
