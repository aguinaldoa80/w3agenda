package br.com.linkcom.sined.modulo.fiscal.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.DominioTipointegracao;
import br.com.linkcom.sined.util.SinedDateUtils;

public class DominiointegracaoFiltro {

	protected Empresa empresa;
	protected Date dtinicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtfim = new Date(System.currentTimeMillis());
	protected DominioTipointegracao dominioTipointegracao;
	protected Integer codigoempresa;
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data Inicial")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("Data Final")
	public Date getDtfim() {
		return dtfim;
	}
	@Required
	@DisplayName("Tipo")
	public DominioTipointegracao getDominioTipointegracao() {
		return dominioTipointegracao;
	}
	@DisplayName("C�digo da empresa")
	public Integer getCodigoempresa() {
		return codigoempresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setDominioTipointegracao(DominioTipointegracao dominioTipointegracao) {
		this.dominioTipointegracao = dominioTipointegracao;
	}
	public void setCodigoempresa(Integer codigoempresa) {
		this.codigoempresa = codigoempresa;
	}	
}
