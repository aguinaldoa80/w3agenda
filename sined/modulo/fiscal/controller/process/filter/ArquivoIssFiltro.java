package br.com.linkcom.sined.modulo.fiscal.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.SinedDateUtils;

public class ArquivoIssFiltro {
	
	private Date data1;
	private Date data2;
	private Empresa empresa;
	
	public ArquivoIssFiltro() {
		data1 = SinedDateUtils.firstDateOfMonth();
		data2 = SinedDateUtils.lastDateOfMonth();
	}
	
	@Required
	@DisplayName("Data in�cio")
	public Date getData1() {
		return data1;
	}
	@Required
	@DisplayName("Data fim")
	public Date getData2() {
		return data2;
	}
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setData1(Date data1) {
		this.data1 = data1;
	}
	public void setData2(Date data2) {
		this.data2 = data2;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
	
	
}