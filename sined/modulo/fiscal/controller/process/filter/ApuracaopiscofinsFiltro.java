package br.com.linkcom.sined.modulo.fiscal.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.SinedDateUtils;

public class ApuracaopiscofinsFiltro {

	protected Empresa empresa;
	protected Date dtinicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtfim = SinedDateUtils.lastDateOfMonth();
	protected Boolean entrada = Boolean.TRUE;
	protected Boolean saida = Boolean.TRUE;
	
	protected Money receitafaturamento = new Money();
	
	protected Money bcpis = new Money();
	protected Money pis = new Money();
	protected Money piscredito = new Money();
	protected Money pisretido = new Money();
	protected Money pisdevido = new Money();
	
	protected Money bccofins = new Money();
	protected Money cofins = new Money();
	protected Money cofinscredito = new Money();
	protected Money cofinsretido = new Money();
	protected Money cofinsdevido = new Money();
	
	protected Money aquisicaobensrevenda = new Money();
	protected Money aquisicaobensinsumos = new Money();
	protected Money aquisicaoservicosutilizado = new Money();
	protected Money energiaeletrica = new Money();
	
	protected Money bccreditos = new Money();
	protected Money saldopis = new Money();
	protected Money saldocofins = new Money();
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("Data Fim")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Receita de Faturamento")
	public Money getReceitafaturamento() {
		return receitafaturamento;
	}
	@DisplayName("Base de C�lculo PIS")
	public Money getBcpis() {
		return bcpis;
	}
	@DisplayName("PIS")
	public Money getPis() {
		return pis;
	}
	@DisplayName("PIS Cr�dito")
	public Money getPiscredito() {
		return piscredito;
	}
	@DisplayName("(-) PIS Retido")
	public Money getPisretido() {
		return pisretido;
	}
	@DisplayName("PIS DEVIDO")
	public Money getPisdevido() {
		return pisdevido;
	}
	@DisplayName("Base de C�lculo COFINS")
	public Money getBccofins() {
		return bccofins;
	}
	@DisplayName("COFINS")
	public Money getCofins() {
		return cofins;
	}
	@DisplayName("COFINS Cr�dito")
	public Money getCofinscredito() {
		return cofinscredito;
	}
	@DisplayName("(-) COFINS Retido")
	public Money getCofinsretido() {
		return cofinsretido;
	}
	@DisplayName("COFINS DEVIDO")
	public Money getCofinsdevido() {
		return cofinsdevido;
	}
	@DisplayName("Aquisi��o de bens para revenda")
	public Money getAquisicaobensrevenda() {
		return aquisicaobensrevenda;
	}
	@DisplayName("Aquisi��o de bens utilizados como insumos")
	public Money getAquisicaobensinsumos() {
		return aquisicaobensinsumos;
	}
	@DisplayName("Aquisi��o de servi�os utilizados")
	public Money getAquisicaoservicosutilizado() {
		return aquisicaoservicosutilizado;
	}
	@DisplayName("Energia el�trica")
	public Money getEnergiaeletrica() {
		return energiaeletrica;
	}
	@DisplayName("Base de C�lculo dos Cr�ditos")	
	public Money getBccreditos() {
		return bccreditos;
	}
	@DisplayName("Saldo PIS")
	public Money getSaldopis() {
		return saldopis;
	}
	@DisplayName("Saldo COFINS")
	public Money getSaldocofins() {
		return saldocofins;
	}

	public Boolean getEntrada() {
		return entrada;
	}

	@DisplayName("Sa�da")
	public Boolean getSaida() {
		return saida;
	}

	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}

	public void setSaida(Boolean saida) {
		this.saida = saida;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setReceitafaturamento(Money receitafaturamento) {
		this.receitafaturamento = receitafaturamento;
	}
	public void setBcpis(Money bcpis) {
		this.bcpis = bcpis;
	}
	public void setPis(Money pis) {
		this.pis = pis;
	}
	public void setPiscredito(Money piscredito) {
		this.piscredito = piscredito;
	}
	public void setPisretido(Money pisretido) {
		this.pisretido = pisretido;
	}
	public void setPisdevido(Money pisdevido) {
		this.pisdevido = pisdevido;
	}
	public void setBccofins(Money bccofins) {
		this.bccofins = bccofins;
	}
	public void setCofins(Money cofins) {
		this.cofins = cofins;
	}
	public void setCofinscredito(Money cofinscredito) {
		this.cofinscredito = cofinscredito;
	}
	public void setCofinsretido(Money cofinsretido) {
		this.cofinsretido = cofinsretido;
	}
	public void setCofinsdevido(Money cofinsdevido) {
		this.cofinsdevido = cofinsdevido;
	}
	public void setAquisicaobensrevenda(Money aquisicaobensrevenda) {
		this.aquisicaobensrevenda = aquisicaobensrevenda;
	}
	public void setAquisicaobensinsumos(Money aquisicaobensinsumos) {
		this.aquisicaobensinsumos = aquisicaobensinsumos;
	}
	public void setAquisicaoservicosutilizado(Money aquisicaoservicosutilizado) {
		this.aquisicaoservicosutilizado = aquisicaoservicosutilizado;
	}
	public void setEnergiaeletrica(Money energiaeletrica) {
		this.energiaeletrica = energiaeletrica;
	}
	public void setBccreditos(Money bccreditos) {
		this.bccreditos = bccreditos;
	}
	public void setSaldopis(Money saldopis) {
		this.saldopis = saldopis;
	}
	public void setSaldocofins(Money saldocofins) {
		this.saldocofins = saldocofins;
	}
}
