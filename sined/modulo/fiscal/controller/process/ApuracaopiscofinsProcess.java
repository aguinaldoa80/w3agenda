package br.com.linkcom.sined.modulo.fiscal.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaopiscofinsFiltro;

@Controller(
		path="/fiscal/process/Apuracaopiscofins",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ApuracaopiscofinsProcess extends MultiActionController{
	
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private EntradafiscalService entradafiscalService;
	
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	
	@DefaultAction
	public ModelAndView handle(WebRequestContext request){
		ApuracaopiscofinsFiltro filtro = new ApuracaopiscofinsFiltro();
		return new ModelAndView("process/apuracaopiscofins", "filtro", filtro);
	}
	
	@Action("gerarApuracao")
	public ModelAndView gerarApuracao(WebRequestContext request, ApuracaopiscofinsFiltro filtro){		
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null){
			List<Entregadocumento> listaEntregadocumento = null;
			if(filtro.getEntrada() != null && filtro.getEntrada()){
				listaEntregadocumento = entradafiscalService.findforApuracaopiscofins(filtro);
			}
			
			List<NotaFiscalServico> listaNotafiscalservico = null;
			List<Notafiscalproduto> listanNotafiscalproduto = null;
			if(filtro.getSaida() != null && filtro.getSaida()){
				listaNotafiscalservico = notaFiscalServicoService.findforApuracaopiscofins(filtro);
			}
			if((filtro.getSaida() != null && filtro.getSaida()) || (filtro.getEntrada() != null && filtro.getEntrada())){
				listanNotafiscalproduto = notafiscalprodutoService.findforApuracaopiscofins(filtro);
			}
			
			Money receitafaturamento = new Money();
			
			Money bcpis = new Money();
			Money pis = new Money();
			Money piscredito = new Money();
			Money pisretido = new Money();
			Money pisdevido = new Money();
			
			Money bccofins = new Money();
			Money cofins = new Money();
			Money cofinscredito = new Money();
			Money cofinsretido = new Money();
			Money cofinsdevido = new Money();
			
			Money aquisicaobensrevenda = new Money();
			Money aquisicaobensinsumos = new Money();
			Money aquisicaoservicosutilizado = new Money();
			Money energiaeletrica = new Money();
			Money bccreditos = new Money();
			
			Money saldopis = new Money();
			Money saldocofins = new Money();
			
			if(listaNotafiscalservico != null && !listaNotafiscalservico.isEmpty()){
				for(NotaFiscalServico nfs : listaNotafiscalservico){
					if(nfs.getValorTotalServicos() != null){
						receitafaturamento = receitafaturamento.add(nfs.getValorNota());
					}
					
					if(nfs.getBasecalculopis() != null){
						bcpis = bcpis.add(nfs.getBasecalculopis()); 
					} else if(nfs.getBasecalculo() != null){
						bcpis = bcpis.add(nfs.getBasecalculo()); 
					}
					if(nfs.getValorPis() != null){
						pis = pis.add(nfs.getValorPis()); 
					}
					if(nfs.getIncidepis() != null && nfs.getIncidepis()){
						pisretido = pisretido.add(nfs.getValorPis());
					}
					
					if(nfs.getBasecalculocofins() != null){
						bccofins = bccofins.add(nfs.getBasecalculocofins()); 
					} else if(nfs.getBasecalculo() != null){
						bccofins = bccofins.add(nfs.getBasecalculo()); 
					}
					if(nfs.getValorCofins() != null){
						cofins = cofins.add(nfs.getValorCofins()); 
					}
					if(nfs.getIncidecofins() != null && nfs.getIncidecofins()){
						cofinsretido = cofinsretido.add(nfs.getValorCofins());
					}
				}
			}
			
			if(listanNotafiscalproduto != null && !listanNotafiscalproduto.isEmpty()){
				for(Notafiscalproduto nfp : listanNotafiscalproduto){
					boolean entrada = Tipooperacaonota.ENTRADA.equals(nfp.getTipooperacaonota());
					if(nfp.getValorprodutos() != null){
						receitafaturamento = receitafaturamento.add(nfp.getValorprodutos());
					}
					if(nfp.getListaItens() != null && !nfp.getListaItens().isEmpty()){
						for(Notafiscalprodutoitem item : nfp.getListaItens()){
							if (!entrada || Tipocobrancapis.isCreditoPis(item.getTipocobrancapis())){
								if(item.getValorbcpis() != null){
									bcpis = bcpis.add(item.getValorbcpis()); 
								}
								if(item.getValorpis() != null){
									pis = pis.add(item.getValorpis()); 
								}
							}
							
							if (!entrada || Tipocobrancacofins.isCreditoCofins(item.getTipocobrancacofins())){
								if(item.getValorbccofins() != null){
									bccofins = bccofins.add(item.getValorbccofins()); 
								}
								if(item.getValorcofins() != null){
									cofins = cofins.add(item.getValorcofins()); 
								}
							}
							
						}
					}
				}
			}
			
			if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
				for(Entregadocumento ed : listaEntregadocumento){
					if(ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty()){
						for(Entregamaterial em : ed.getListaEntregamaterial()){
							if(em.getValordesconto() == null) em.setValordesconto(new Money());
							if(em.getValorfrete() == null) em.setValorfrete(new Money());
							
							if(em.getNaturezabcc() != null){
								if(Naturezabasecalculocredito.AQUISICAO_BENS_REVENDA.equals(em.getNaturezabcc())){
									aquisicaobensrevenda = aquisicaobensrevenda.add(new Money(em.getQtde()*em.getValorunitario())).add(em.getValorfrete()).subtract(em.getValordesconto());									
								}else if(Naturezabasecalculocredito.AQUISICAO_BENS_INSUMO.equals(em.getNaturezabcc())){
									aquisicaobensinsumos = aquisicaobensinsumos.add(new Money(em.getQtde()*em.getValorunitario())).add(em.getValorfrete()).subtract(em.getValordesconto());
								}else if(Naturezabasecalculocredito.AQUISICAO_SERVICOS_INSUMO.equals(em.getNaturezabcc())){
									aquisicaoservicosutilizado = aquisicaoservicosutilizado.add(new Money(em.getQtde()*em.getValorunitario())).add(em.getValorfrete()).subtract(em.getValordesconto());
								}else if(Naturezabasecalculocredito.ENERGIA_ELETRICA_ESTABELECIMENTO_PESSOA_JURIDICA.equals(em.getNaturezabcc())){
									energiaeletrica = energiaeletrica.add(new Money(em.getQtde()*em.getValorunitario())).add(em.getValorfrete()).subtract(em.getValordesconto());
								}
							}
							if(em.getCstpis() != null && em.getValorpis() != null){
								if(Tipocobrancapis.isCreditoPis(em.getCstpis())){
									piscredito = piscredito.add(em.getValorpis());
								}
							}
							if(em.getCstcofins() != null && em.getValorcofins() != null){
								if(Tipocobrancacofins.isCreditoCofins(em.getCstcofins())){
									cofinscredito = cofinscredito.add(em.getValorcofins());
								}
							}
						}
					}
				}
			}
			
			pisdevido = pis.subtract(pisretido);
			cofinsdevido = cofins.subtract(cofinsretido);
			
			bccreditos = aquisicaobensinsumos.add(aquisicaobensrevenda).add(aquisicaoservicosutilizado).add(energiaeletrica);
			saldopis = pisdevido.subtract(piscredito);
			saldocofins = cofinsdevido.subtract(cofinscredito);
			
			filtro.setReceitafaturamento(receitafaturamento);
			filtro.setBcpis(bcpis);
			filtro.setPis(pis);
			filtro.setPiscredito(piscredito);
			filtro.setPisretido(pisretido);
			filtro.setPisdevido(pisdevido);
			filtro.setBccofins(bccofins);
			filtro.setCofins(cofins);
			filtro.setCofinscredito(cofinscredito);
			filtro.setCofinsretido(cofinsretido);
			filtro.setCofinsdevido(cofinsdevido);
			filtro.setAquisicaobensrevenda(aquisicaobensrevenda);
			filtro.setAquisicaobensinsumos(aquisicaobensinsumos);
			filtro.setAquisicaoservicosutilizado(aquisicaoservicosutilizado);
			filtro.setEnergiaeletrica(energiaeletrica);
			filtro.setBccreditos(bccreditos);
			filtro.setSaldopis(saldopis);
			filtro.setSaldocofins(saldocofins);
			
			request.setAttribute("exibirApuracao", true);
		}
		return new ModelAndView("process/apuracaopiscofins", "filtro", filtro);
	}	
}
