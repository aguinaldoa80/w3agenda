package br.com.linkcom.sined.modulo.fiscal.controller.process;

import java.lang.reflect.Method;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Operacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.auxiliar.DominioIntegracaoRegistro0000;
import br.com.linkcom.sined.geral.bean.auxiliar.DominioIntegracaoRegistro6000;
import br.com.linkcom.sined.geral.bean.auxiliar.DominioIntegracaoRegistro6100;
import br.com.linkcom.sined.geral.bean.auxiliar.DominiointegracaoRegistro02;
import br.com.linkcom.sined.geral.bean.auxiliar.DominiointegracaoRegistro03;
import br.com.linkcom.sined.geral.bean.auxiliar.DominiointegracaoRegistro11;
import br.com.linkcom.sined.geral.bean.auxiliar.DominiointegracaoRegistro22;
import br.com.linkcom.sined.geral.bean.auxiliar.Dominiointegracaocabecalho;
import br.com.linkcom.sined.geral.bean.auxiliar.Dominiointegracaoentrada;
import br.com.linkcom.sined.geral.bean.auxiliar.Dominiointegracaoproduto;
import br.com.linkcom.sined.geral.bean.auxiliar.Dominiointegracaoprodutodnf;
import br.com.linkcom.sined.geral.bean.auxiliar.Dominiointegracaosaida;
import br.com.linkcom.sined.geral.bean.auxiliar.Dominiointegracaoservico;
import br.com.linkcom.sined.geral.bean.enumeration.DominioTipointegracao;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadortipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodocumento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipofrete;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.SintegraService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.DominiointegracaoCompleto;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro0000;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro0010;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro0020;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro1000;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro1020;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro2000;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro2020;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro3000;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao.Registro3020;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.DominiointegracaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.utils.SIntegraUtil;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(path="/fiscal/process/Dominiointegracao", authorizationModule=ProcessAuthorizationModule.class)
public class DominiointegracaoProcess extends ResourceSenderController<DominiointegracaoFiltro>{
	
	private EntradafiscalService entradafiscalService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaFiscalServicoService notafiscalServicoService;
	private EmpresaService empresaService;
	private FornecedorService fornecedorService;
	private ClienteService clienteService;
	private MovimentacaocontabilService movimentacaocontabilService;
	private UsuarioService usuarioService;
	private SintegraService sintegraService;
	private ArquivonfnotaService arquivonfnotaService; 
	
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotafiscalServicoService(NotaFiscalServicoService notafiscalServicoService) {this.notafiscalServicoService = notafiscalServicoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setMovimentacaocontabilService(MovimentacaocontabilService movimentacaocontabilService) {this.movimentacaocontabilService = movimentacaocontabilService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setSintegraService(SintegraService sintegraService) {this.sintegraService = sintegraService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	
	@Override
	public Resource generateResource(WebRequestContext request,	DominiointegracaoFiltro filtro) throws Exception {
		return gerarArquivointegracao(request, filtro);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, DominiointegracaoFiltro filtro) throws Exception {
		List<DominioTipointegracao> lista = new ArrayList<DominioTipointegracao>();
		lista.add(DominioTipointegracao.COMPLETO);
		lista.add(DominioTipointegracao.ENTRADA);
		lista.add(DominioTipointegracao.LANCAMENTO_CONTABIL);
		lista.add(DominioTipointegracao.SAIDA);
		lista.add(DominioTipointegracao.SERVICO);
		lista.add(DominioTipointegracao.CADASTRO_FORNECEDOR_CLIENTE);
		request.setAttribute("listaDominioTipointegracao", lista);
		
		if(filtro.getEmpresa() == null && request.getSession().getAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase()) == null){
			request.getSession().setAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase(), Boolean.TRUE);
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null){
				Method methodSetEmpresa = Util.beans.getSetterMethod(filtro.getClass(), "empresa");
				if(methodSetEmpresa != null && methodSetEmpresa.getParameterTypes()[0].equals(Empresa.class)){
					methodSetEmpresa.invoke(filtro, empresa);
				}
			}
		}
		
		return new ModelAndView("process/dominiointegracao").addObject("filtro", filtro);
	}
	
	/**
	 * Gera o arquivo de integra��o de acordo com o filtro
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public Resource gerarArquivointegracao(WebRequestContext request, DominiointegracaoFiltro filtro) throws Exception {		
		String arquivoString = new String();				
		StringBuilder s = new StringBuilder();		
		
		if(filtro.getDominioTipointegracao() != null){
			if(filtro.getDominioTipointegracao().equals(DominioTipointegracao.CADASTRO_FORNECEDOR_CLIENTE)){
				gerarArquivoCadastroFornecedorClienteRemetenteDestinatario(s, filtro);
			}else {
				if(filtro.getEmpresa() != null)
					filtro.setEmpresa(empresaService.load(filtro.getEmpresa(),"empresa.cdpessoa, empresa.cnpj, empresa.cpf, empresa.principal, empresa.codigoEmpresaDominioIntegracao"));
				if(filtro.getDominioTipointegracao().equals(DominioTipointegracao.ENTRADA)){
					gerarArquivoEntrada(s, filtro);
				}else if(filtro.getDominioTipointegracao().equals(DominioTipointegracao.SAIDA)){
					gerarArquivoSaida(s, filtro);
				}else if(filtro.getDominioTipointegracao().equals(DominioTipointegracao.SERVICO)){
					gerarArquivoServico(s, filtro);
				}else if(filtro.getDominioTipointegracao().equals(DominioTipointegracao.LANCAMENTO_CONTABIL)){
					Boolean isEmpresaPrincipal = filtro.getEmpresa().getPrincipal();
					String codigo = filtro.getCodigoempresa() != null ? filtro.getCodigoempresa().toString() : "";
					
					if(!isEmpresaPrincipal && StringUtils.isBlank(codigo)) {
						throw new SinedException("N�o � poss�vel gerar o arquivo. Favor informar o c�digo da empresa.");
					}
					
					gerarArquivoLancamentocontabil(s, filtro);
				}else if(filtro.getDominioTipointegracao().equals(DominioTipointegracao.COMPLETO)){
					gerarArquivoCompleto(s, filtro);
				}
			}
			arquivoString = s != null ? s.toString() : "";
		}	
		
		if(s != null && "".equals(s.toString()))
			throw new SinedException("N�o existem dados a serem exportados neste per�odo.");
		
		Resource resource = new Resource("text/plain", "dominio_integracao_" + 
										 new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio()) + 
										 "_" + 
										 new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim()) +
										 "_" +
										 filtro.getDominioTipointegracao().getNome() +
										 ".txt", arquivoString.getBytes());
		return resource;
	}
	
	private void gerarArquivoCompleto(StringBuilder s, DominiointegracaoFiltro filtro) {
		List<Entregadocumento> lista = entradafiscalService.findForDominiointegracaoEntrada(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
		List<Notafiscalproduto> listaNfp = notafiscalprodutoService.findForDominiointegracaoSaida(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
		List<NotaFiscalServico> listaNfs = notafiscalServicoService.findForDominiointegracaoSaida(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
//		List<Movimentacaocontabil> listaMovimentacaocontabil = movimentacaocontabilService.findForDominiointegracaoLancamentocontabil(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
		
		List<Fornecedor> listaFornecedor = fornecedorService.findForDominiointegracaoCadastro(filtro);
		List<Cliente> listaCliente = clienteService.findForDominiointegracaoCadastro(filtro);
		
		DominiointegracaoCompleto dominiointegracaoCompleto = new DominiointegracaoCompleto();
		dominiointegracaoCompleto.setListaRegistro0000(this.createRegistro0000(filtro));
		dominiointegracaoCompleto.setListaRegistro0010(this.createRegistro0010(listaCliente, filtro));
		dominiointegracaoCompleto.setListaRegistro0020(this.createRegistro0020(listaFornecedor, filtro));
		dominiointegracaoCompleto.setListaRegistro1000(this.createRegistro1000(lista, filtro));
		dominiointegracaoCompleto.setListaRegistro2000(this.createRegistro2000(listaNfp, filtro));
		dominiointegracaoCompleto.setListaRegistro3000(this.createRegistro3000(listaNfs, filtro));
		
		s.append(dominiointegracaoCompleto.toString());
	}
	
	private List<Registro0000> createRegistro0000(DominiointegracaoFiltro filtro) {
		List<Registro0000> listaRegistro0000 = new ArrayList<Registro0000>();
		
		if(filtro.getEmpresa() != null){
			Empresa empresa = empresaService.carregaEmpresa(filtro.getEmpresa());
			Registro0000 registro0000 = new Registro0000();
			registro0000.setIdentificacao(Registro0000.REG);
			registro0000.setInscricaoempresa(empresa.getCpfOuCnpjValue());
			
			listaRegistro0000.add(registro0000);
		}
		
		return listaRegistro0000;
	}
	
	private List<Registro0020> createRegistro0020(List<Fornecedor> listaFornecedor, DominiointegracaoFiltro filtro) {
		List<Registro0020> listaRegistro0020 = new ArrayList<Registro0020>();
		if(listaFornecedor != null && !listaFornecedor.isEmpty()){			
			for(Fornecedor fornecedor : listaFornecedor){
				Registro0020 registro0020 = new Registro0020();
				
				registro0020.setIdentificacao(Registro0020.REG);
				registro0020.setInscricao(fornecedor.getCpfOuCnpjValue());
				registro0020.setRazaosocial(fornecedor.getRazaosocial());
				registro0020.setApelido(fornecedor.getNome());
				
				Endereco endereco = fornecedor.getEndereco();
				if(endereco != null){
					registro0020.setEndereco(endereco.getLogradouro());
					try {
						if(endereco.getNumero() != null && !"".equals(endereco.getNumero())){
							Integer numeroEnd = Integer.parseInt(endereco.getNumero());
							registro0020.setNumeroendereco(numeroEnd.toString());
						}
					} catch (Exception e) {
						registro0020.setNumeroendereco("");
					}
					registro0020.setComplemento(endereco.getComplemento());
					registro0020.setBairro(endereco.getBairro());
					
					if(endereco.getMunicipio() != null){
						registro0020.setCodigomunicipio(endereco.getMunicipio().getCdibge());
						
						if(endereco.getPais() != null && !endereco.getPais().equals(Pais.BRASIL)){
							registro0020.setUf("EX");
							registro0020.setCodigopais(null);
						}else if(endereco.getMunicipio().getUf() != null){
							registro0020.setUf(endereco.getMunicipio().getUf().getNome());
						}
					}
					
					registro0020.setCep(endereco.getCep() != null ? endereco.getCep().getValue() : null);
				}
				
				registro0020.setInscricaoestadual(fornecedor.getInscricaoestadual());
				registro0020.setInscricaomunicipal(fornecedor.getInscricaomunicipal());
				registro0020.setInscricaosuframa(null);
				registro0020.setDdd(null);
				registro0020.setTelefone(fornecedor.getFoneDominiointegracao());
				registro0020.setFax(fornecedor.getTelefoneFax() != null ? fornecedor.getTelefoneFax().getTelefone() : null);
				registro0020.setDatacadastro(fornecedor.getDtaltera() != null ? new Date(fornecedor.getDtaltera().getTime()) : null);
				registro0020.setContacontabil(fornecedor.getContaContabil() != null ? fornecedor.getContaContabil().getCodigoalternativo() : null);
				registro0020.setContacontabilcliente(null);
				registro0020.setAgropecuario("N");
				registro0020.setNaturezajuridica(null);
				registro0020.setRegimeapuracao("N");
				registro0020.setContribuinteicms("N");
				registro0020.setAliquotaicms(null);
				registro0020.setCategoriaestabelecimento(null);
				registro0020.setInscricaoestadualst(null);
				registro0020.setEmail(fornecedor.getEmail());
				registro0020.setInterdependenciacomempresa(null);
				
				listaRegistro0020.add(registro0020);
			}
		}
		
		return listaRegistro0020;
	}
	
	private List<Registro0010> createRegistro0010(List<Cliente> listaCliente, DominiointegracaoFiltro filtro) {
		List<Registro0010> listaRegistro0010 = new ArrayList<Registro0010>();
		if(listaCliente != null && !listaCliente.isEmpty()){			
			for(Cliente cleinte : listaCliente){
				Registro0010 registro0010 = new Registro0010();
				
				registro0010.setIdentificacao(Registro0010.REG);
				registro0010.setInscricao(cleinte.getCpfOuCnpjValue());
				registro0010.setRazaosocial(cleinte.getRazaosocial());
				registro0010.setApelido(cleinte.getNome());
				
				Endereco endereco = cleinte.getEndereco();
				if(endereco != null){
					registro0010.setEndereco(endereco.getLogradouro());
					try {
						if(endereco.getNumero() != null && !"".equals(endereco.getNumero())){
							Integer numeroEnd = Integer.parseInt(endereco.getNumero());
							registro0010.setNumeroendereco(numeroEnd.toString());
						}
					} catch (Exception e) {
						registro0010.setNumeroendereco("");
					}
					registro0010.setComplemento(endereco.getComplemento());
					registro0010.setBairro(endereco.getBairro());
					
					if(endereco.getMunicipio() != null){
						registro0010.setCodigomunicipio(endereco.getMunicipio().getCdibge());
						
						if(endereco.getPais() != null && !endereco.getPais().equals(Pais.BRASIL)){
							registro0010.setUf("EX");
							registro0010.setCodigopais(null);
						}else if(endereco.getMunicipio().getUf() != null){
							registro0010.setUf(endereco.getMunicipio().getUf().getNome());
						}
					}
					
					registro0010.setCep(endereco.getCep() != null ? endereco.getCep().getValue() : null);
				}
				
				registro0010.setInscricaoestadual(cleinte.getInscricaoestadual());
				registro0010.setInscricaomunicipal(cleinte.getInscricaomunicipal());
				registro0010.setInscricaosuframa(null);
				registro0010.setDdd(null);
				registro0010.setTelefone(cleinte.getFoneDominiointegracao());
				registro0010.setFax(cleinte.getTelefoneFax() != null ? cleinte.getTelefoneFax().getTelefone() : null);
				registro0010.setDatacadastro(cleinte.getDtaltera() != null ? new Date(cleinte.getDtaltera().getTime()) : null);
				registro0010.setContacontabil(null);
				registro0010.setContacontabilfornecedor(null);
				registro0010.setAgropecuario("N");
				registro0010.setNaturezajuridica("7");
				registro0010.setRegimeapuracao("N");
				registro0010.setContribuinteicms("N");
				registro0010.setAliquotaicms(null);
				registro0010.setCategoriaestabelecimento(null);
				registro0010.setInterdependenciacomempresa(null);
				
				listaRegistro0010.add(registro0010);
			}
		}
		
		return listaRegistro0010;
	}
	private List<Registro3000> createRegistro3000(List<NotaFiscalServico> lista, DominiointegracaoFiltro filtro) {
		List<Registro3000> listaRegistro3000 = new ArrayList<Registro3000>();
		if(lista != null && !lista.isEmpty()){
			for(NotaFiscalServico nfs : lista){
				
				Registro3000 registro3000 = new Registro3000();
				registro3000.setIdentificacao(Registro3000.REG);
				
				Integer modelodocfiscal = 31; // NFS
				if(nfs.getNotaStatus() != null && (nfs.getNotaStatus().equals(NotaStatus.NFSE_EMITIDA) || 
						nfs.getNotaStatus().equals(NotaStatus.NFSE_LIQUIDADA))){
					modelodocfiscal = 38; // NFS-e
				}
				registro3000.setCodigo_especie(modelodocfiscal);
				if(nfs.getCliente() != null){
					registro3000.setInscricao_cliente(nfs.getCliente().getCpfOuCnpjValue());
				}
				if(nfs.getEnderecoCliente() != null && nfs.getEnderecoCliente().getMunicipio() != null && 
						nfs.getEnderecoCliente().getMunicipio().getUf() != null){
					registro3000.setSigla_estado_cliente(nfs.getEnderecoCliente().getMunicipio().getUf().getSigla());
				}
				
				if(nfs.getNaturezaoperacao() != null && nfs.getIndicadortipopagamento() != null){
					if(nfs.getIndicadortipopagamento().equals(Indicadortipopagamento.A_VISTA)){
						if(nfs.getNaturezaoperacao().getOperacaocontabilavista() != null){
							registro3000.setCodigo_acumulador(nfs.getNaturezaoperacao().getOperacaocontabilavista().getCodigointegracao());
						}
					}else if(nfs.getIndicadortipopagamento().equals(Indicadortipopagamento.A_PRAZO)){
						if(nfs.getNaturezaoperacao().getOperacaocontabilaprazo() != null){
							registro3000.setCodigo_acumulador(nfs.getNaturezaoperacao().getOperacaocontabilaprazo().getCodigointegracao());
						}
					}
				}
				
//				registro3000.setSerie(null);
				registro3000.setNumero_documento(nfs.getNumero());
//				registro3000.setSeguimento(null);
//				registro3000.setDocumento_final(null);
				registro3000.setData_servico(nfs.getDtEmissao());
				registro3000.setData_emissao(nfs.getDtEmissao());
				registro3000.setValor_contabil(nfs.getValorNota() != null ? nfs.getValorNota().getValue().doubleValue() : null);
//				registro3000.setObsevacao(null);
//				registro3000.setCodigo_observacao(null);
//				registro3000.setFato_gerador_crf(null);
//				registro3000.setFato_gerador_irrf(null);
//				registro3000.setFato_gerador_crfop(null);
//				registro3000.setFato_gerador_irrfp(null);
				
				Endereco endereco = nfs.getEnderecoCliente() != null ? nfs.getEnderecoCliente() : nfs.getCliente().getEndereco();
				if(endereco != null &&
						endereco.getMunicipio() != null && 
						endereco.getMunicipio().getCdibge() != null){
					registro3000.setCodigo_municipio(endereco.getMunicipio().getCdibge());
				}
				
//				registro3000.setCodigo_modelo_doc(null);
//				registro3000.setCodigo_fiscal_prestacao_servicos(null);
//				registro3000.setSub_serie(null);
				
				if(nfs.getCliente() != null){
					registro3000.setInscricao_estadual_cliente(nfs.getCliente().getInscricaoestadual());
					registro3000.setInscricao_municipal_cliente(nfs.getCliente().getInscricaomunicipal());
				}
				
				registro3000.setListaRegistro3020(this.createRegistro3020(nfs));
				
				listaRegistro3000.add(registro3000);
			}
		}
		
		return listaRegistro3000;
	}
	
	private List<Registro3020> createRegistro3020(NotaFiscalServico nfs) {
		List<Registro3020> listaRegistro2020 = new ArrayList<Registro3020>();
		
		if(nfs != null){
			if(nfs.getIncideicms() != null && nfs.getIncideicms()){
				Registro3020 registro3020 = new Registro3020();
				
				registro3020.setIdentificacao(Registro3020.REG);
				registro3020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.ICMS));
				registro3020.setPercentual_reducao_bc(null);
				registro3020.setBase_calculo(nfs.getBasecalculoicms() != null ? nfs.getBasecalculoicms().getValue().doubleValue():null);
				registro3020.setAliquota(nfs.getIcms());
				registro3020.setValor_imposto(nfs.getValorIcms() != null ? nfs.getValorIcms().getValue().doubleValue():null);
				registro3020.setValor_isentas(null);
				registro3020.setValor_outras(null);
				registro3020.setValor_contabil(nfs.getValorNota() != null ? nfs.getValorNota().getValue().doubleValue():null);
				
				listaRegistro2020.add(registro3020);
			}
			if(nfs.getIncideiss() != null && nfs.getIncideiss()){
				Registro3020 registro3020 = new Registro3020();
				
				registro3020.setIdentificacao(Registro3020.REG);
				registro3020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.ISS));
				registro3020.setPercentual_reducao_bc(null);
				registro3020.setBase_calculo(nfs.getBasecalculoiss() != null ? nfs.getBasecalculoiss().getValue().doubleValue():null);
				registro3020.setAliquota(nfs.getIss());
				registro3020.setValor_imposto(nfs.getValorIss() != null ? nfs.getValorIss().getValue().doubleValue():null);
				registro3020.setValor_isentas(null);
				registro3020.setValor_outras(null);
				registro3020.setValor_contabil(nfs.getValorNota() != null ? nfs.getValorNota().getValue().doubleValue():null);
				listaRegistro2020.add(registro3020);
			}
			if(nfs.getIncidepis() != null && nfs.getIncidepis()){
				Registro3020 registro3020 = new Registro3020();
				
				registro3020.setIdentificacao(Registro3020.REG);
				registro3020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.PIS));
				registro3020.setPercentual_reducao_bc(null);
				registro3020.setBase_calculo(nfs.getBasecalculopis() != null ? nfs.getBasecalculopis().getValue().doubleValue():null);
				registro3020.setAliquota(nfs.getPis());
				registro3020.setValor_imposto(nfs.getValorPis() != null ? nfs.getValorPis().getValue().doubleValue():null);
				registro3020.setValor_isentas(null);
				registro3020.setValor_outras(null);
				registro3020.setValor_contabil(nfs.getValorNota() != null ? nfs.getValorNota().getValue().doubleValue():null);
				listaRegistro2020.add(registro3020);
			}
			if(nfs.getIncidecofins() != null && nfs.getIncidecofins()){
				Registro3020 registro3020 = new Registro3020();
				
				registro3020.setIdentificacao(Registro3020.REG);
				registro3020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.COFINS));
				registro3020.setPercentual_reducao_bc(null);
				registro3020.setBase_calculo(nfs.getBasecalculocofins() != null ? nfs.getBasecalculocofins().getValue().doubleValue():null);
				registro3020.setAliquota(nfs.getPis());
				registro3020.setValor_imposto(nfs.getValorCofins() != null ? nfs.getValorCofins().getValue().doubleValue():null);
				registro3020.setValor_isentas(null);
				registro3020.setValor_outras(null);
				registro3020.setValor_contabil(nfs.getValorNota() != null ? nfs.getValorNota().getValue().doubleValue():null);
				listaRegistro2020.add(registro3020);
			}
		}
		
		return listaRegistro2020;
	}
	
	private List<Registro2000> createRegistro2000(List<Notafiscalproduto> lista, DominiointegracaoFiltro filtro) {
		List<Registro2000> listaRegistro2000 = new ArrayList<Registro2000>();
		if(lista != null && !lista.isEmpty()){
			for(Notafiscalproduto nfp : lista){
				nfp.setListaItens(sintegraService.agruparCfopAliquotaNfp(nfp.getListaItens()));
				Integer modelodocfiscal = 1; // NF
				if(nfp.getNotaStatus() != null && (nfp.getNotaStatus().equals(NotaStatus.NFE_EMITIDA) || 
						nfp.getNotaStatus().equals(NotaStatus.NFE_LIQUIDADA))){
					modelodocfiscal = 36; // NF-e
				}
				
				for(Notafiscalprodutoitem item : nfp.getListaItens()){
					Registro2000 registro2000 = new Registro2000();
					registro2000.setIdentificacao(Registro2000.REG);
					registro2000.setCodigo_especie(modelodocfiscal);
					if(nfp.getCliente() != null){
						registro2000.setInscricao_cliente(nfp.getCliente().getCpfOuCnpjValue());
					}

					if(nfp.getNaturezaoperacao() != null && nfp.getFormapagamentonfe() != null){
						if(nfp.getFormapagamentonfe().equals(Formapagamentonfe.A_VISTA)){
							if(nfp.getNaturezaoperacao().getOperacaocontabilavista() != null){
								registro2000.setCodigo_acumulador(nfp.getNaturezaoperacao().getOperacaocontabilavista().getCodigointegracao());
							}
						}else if(nfp.getFormapagamentonfe().equals(Formapagamentonfe.A_PRAZO)){
							if(nfp.getNaturezaoperacao().getOperacaocontabilaprazo() != null){
								registro2000.setCodigo_acumulador(nfp.getNaturezaoperacao().getOperacaocontabilaprazo().getCodigointegracao());
							}
						}
					}
					
					registro2000.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
//					registro2000.setCodigo_exclusao_dief(null);
					if(nfp.getEnderecoCliente() != null && nfp.getEnderecoCliente().getMunicipio() != null && 
							nfp.getEnderecoCliente().getMunicipio().getUf() != null){
						registro2000.setSigla_estado_cliente(nfp.getEnderecoCliente().getMunicipio().getUf().getSigla());
					}
//					registro2000.setSeguimento(null);
					registro2000.setNumero_documento(nfp.getNumero());
//					registro2000.setSerie(null);
//					registro2000.setDocumento_final(null);
					if(nfp.getDatahorasaidaentrada() != null){
						registro2000.setData_saida(new Date(nfp.getDatahorasaidaentrada().getTime()));
					}
					registro2000.setData_emissao(nfp.getDtEmissao());
					
					double valortotal = item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0d;
					valortotal += item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : 0d;
					valortotal += item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : 0d;
					valortotal += item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0d;
					valortotal += item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d;
					valortotal += item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : 0d;
					valortotal -= item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d;
					
					registro2000.setValor_contabil(valortotal);
//					registro2000.setValor_exclusao_dief(null);
//					registro2000.setObsevacao(null);
					
					Endereco endereco = nfp.getEnderecoCliente() != null ? nfp.getEnderecoCliente() : nfp.getCliente().getEndereco();
					if(endereco != null &&
							endereco.getMunicipio() != null && 
							endereco.getMunicipio().getCdibge() != null){
						registro2000.setCodigo_municipio(endereco.getMunicipio().getCdibge());
					}
					
//					registro2000.setModalidade_frete(null);					
//					registro2000.setCfop_estendido_detalhamento(null);
//					registro2000.setCodigo_transferencia_credito(null);
//					registro2000.setCodigo_observacao(null);
//					registro2000.setData_visto_notas_transf_cred_icms(null);
//					registro2000.setCodigo_antecipacao_tributaria(null);
//					registro2000.setFato_gerador_crf(null);
//					registro2000.setFato_gerador_crfop(null);
//					registro2000.setFato_gerador_irrf(null);
//					registro2000.setTipo_receita(null);
					registro2000.setValor_frete(item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : null);
					registro2000.setValor_seguro(item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : null);
					registro2000.setValor_despesas_acessorias(item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : null);
					registro2000.setValor_produtos(nfp.getValorBruto() != null ? nfp.getValorBruto().getValue().doubleValue():null);
					registro2000.setValor_bc_icms_st(nfp.getValorbcicmsst() != null ? nfp.getValorbcicmsst().getValue().doubleValue():null);
//					registro2000.setOutras_saidas_isentas(null);
//					registro2000.setSaidas_isentas(null);
//					registro2000.setSaidas_isentas_cupomfiscal(null);
//					registro2000.setSaidas_isentas_nf_mod02(null);
//					registro2000.setCodigo_modelo_doc_fiscal(null);
//					registro2000.setCodigo_fiscal_prestacao_servicos(null);
//					registro2000.setCodigo_situacao_tributaria(null);
//					registro2000.setSub_serie(null);
//					registro2000.setTipo_titulo(null);
//					registro2000.setIdentificacao_titulo(null);
					
					if(nfp.getCliente() != null){
						registro2000.setInscricao_estadual_cliente(nfp.getCliente().getInscricaoestadual());
						registro2000.setInscricao_municipal_cliente(nfp.getCliente().getInscricaomunicipal());
					}
					
					Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProdutoAndCancelada(nfp);
					if(arquivonfnota != null){
						registro2000.setChave_nfe(arquivonfnota.getChaveacesso());
					}
					
//					registro2000.setCodigo_recolhimento_fethab(null);
//					registro2000.setResponsavel_recolhimento_fethab(null);
//					registro2000.setTipo_cte(0);
//					registro2000.setCte_referencia(null);
//					registro2000.setCodigo_informacao_complementar(null);
					registro2000.setInformacao_complementar(item.getInfoadicional());
					registro2000.setCst_piscofins(item.getTipocobrancapis() != null ? item.getTipocobrancapis().getCdnfe() : null);
//					registro2000.setNatureza_receita(null);
//					registro2000.setValor_servico_itens_piscofins(null);
					registro2000.setBase_calculo_piscofins(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue():null);
					registro2000.setAliquota_pis(item.getPis());
					registro2000.setAliquota_cofins(item.getCofins());
//					registro2000.setQuantidade_kwh(null);
					
					registro2000.setListaRegistro2020(this.createRegistro2020(item));
					
					listaRegistro2000.add(registro2000);
				}
			}
		}
		
		return listaRegistro2000;
	}
	
	private List<Registro2020> createRegistro2020(Notafiscalprodutoitem item) {
		List<Registro2020> listaRegistro2020 = new ArrayList<Registro2020>();
		
		if(item != null){
			if(item.getIcms() != null && item.getIcms() > 0){
				Registro2020 registro2020 = new Registro2020();
				
				registro2020.setIdentificacao(Registro2020.REG);
				registro2020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.ICMS));
				registro2020.setPercentual_reducao_bc(null);
				registro2020.setBase_calculo(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue():null);
				registro2020.setAliquota(item.getIcms());
				registro2020.setValor_imposto(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue():null);
				registro2020.setValor_isentas(null);
				registro2020.setValor_outras(null);
				registro2020.setValor_ipi(null);
				registro2020.setValor_st(null);
				registro2020.setCodigo_recolhimento_imposto(null);
				registro2020.setValor_nao_tributado(null);
				registro2020.setValor_parcela_reduzida(null);
				
				listaRegistro2020.add(registro2020);
			}
			if(item.getPis() != null && item.getPis() > 0){
				Registro2020 registro2020 = new Registro2020();
				
				registro2020.setIdentificacao(Registro2020.REG);
				registro2020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.PIS));
				registro2020.setPercentual_reducao_bc(null);
				registro2020.setBase_calculo(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue():null);
				registro2020.setAliquota(item.getPis());
				registro2020.setValor_imposto(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue():null);
				registro2020.setValor_isentas(null);
				registro2020.setValor_outras(null);
				registro2020.setValor_ipi(null);
				registro2020.setValor_st(null);
				registro2020.setCodigo_recolhimento_imposto(null);
				registro2020.setValor_nao_tributado(null);
				registro2020.setValor_parcela_reduzida(null);
				
				listaRegistro2020.add(registro2020);
			}
			if(item.getCofins() != null && item.getCofins() > 0){
				Registro2020 registro2020 = new Registro2020();
				
				registro2020.setIdentificacao(Registro2020.REG);
				registro2020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.COFINS));
				registro2020.setPercentual_reducao_bc(null);
				registro2020.setBase_calculo(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue():null);
				registro2020.setAliquota(item.getCofins());
				registro2020.setValor_imposto(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue():null);
				registro2020.setValor_isentas(null);
				registro2020.setValor_outras(null);
				registro2020.setValor_ipi(null);
				registro2020.setValor_st(null);
				registro2020.setCodigo_recolhimento_imposto(null);
				registro2020.setValor_nao_tributado(null);
				registro2020.setValor_parcela_reduzida(null);
				
				listaRegistro2020.add(registro2020);
			}
		}
		
		return listaRegistro2020;
	}
	
	private List<Registro1000> createRegistro1000(List<Entregadocumento> lista,	DominiointegracaoFiltro filtro) {
		List<Registro1000> listaRegistro1000 = new ArrayList<Registro1000>();
		if(lista != null && !lista.isEmpty()){
			for(Entregadocumento ed : lista){
				Ordemcompra oc = null;
				if(ed.getEntrega() != null && ed.getEntrega().getHaveOrdemcompra()){
					Set<Ordemcompraentrega> listaOrdemcompraentrega = ed.getEntrega().getListaOrdemcompraentrega();
					for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
						if(ordemcompraentrega.getOrdemcompra() != null){
							oc = ordemcompraentrega.getOrdemcompra();
							break;
						}
					}
				}
				
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					em.setValorproduto(em.getQtde() * em.getValorunitario());
				}
				ed.setListaEntregamaterial(sintegraService.agruparCfopAliquotaEntrega(ed.getListaEntregamaterial()));
				
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					Registro1000 registro1000 = new Registro1000();
					registro1000.setIdentificacao(Registro1000.REG);
					
					if(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigodominio() != null){
						registro1000.setCodigo_especie(ed.getModelodocumentofiscal().getCodigodominio());
					}
					
					if(ed.getFornecedor() != null){
						registro1000.setInscricao_fornecedor(ed.getFornecedor().getCpfOuCnpjValue());
					}
					
//					registro1000.setCodigo_exclusao_dief(null);
					
					if(ed.getNaturezaoperacao() != null && ed.getIndpag() != null){
						if(ed.getIndpag().equals(Formapagamentonfe.A_VISTA)){
							if(ed.getNaturezaoperacao().getOperacaocontabilavista() != null){
								registro1000.setCodigo_acumulador(ed.getNaturezaoperacao().getOperacaocontabilavista().getCodigointegracao());
							}
						}else if(ed.getIndpag().equals(Formapagamentonfe.A_PRAZO)){
							if(ed.getNaturezaoperacao().getOperacaocontabilaprazo() != null){
								registro1000.setCodigo_acumulador(ed.getNaturezaoperacao().getOperacaocontabilaprazo().getCodigointegracao());
							}
						}
					}
					
					registro1000.setCfop(em.getCfop() != null ? em.getCfop().getCodigo() : null);
//					registro1000.setSeguimento(null);
					registro1000.setNumero_documento(ed.getNumero());
					registro1000.setSerie(ed.getSerie());
//					registro1000.setNumero_documento_final(null);
					registro1000.setData_entrada(ed.getDtentrada());
					registro1000.setData_emissao(ed.getDtemissao());
					
					double valortotal = em.getValorproduto() != null ? em.getValorproduto() : 0d;
					valortotal += em.getValorfrete() != null ? em.getValorfrete().getValue().doubleValue() : 0d;
					valortotal += em.getValorseguro() != null ? em.getValorseguro().getValue().doubleValue() : 0d;
					valortotal += em.getValoricmsst() != null ? em.getValoricmsst().getValue().doubleValue() : 0d;
					valortotal += em.getValoripi() != null ? em.getValoripi().getValue().doubleValue() : 0d;
					valortotal += em.getValoroutrasdespesas() != null ? em.getValoroutrasdespesas().getValue().doubleValue() : 0d;
					valortotal -= em.getValordesconto() != null ? em.getValordesconto().getValue().doubleValue() : 0d;
					
					registro1000.setValor_contabil(valortotal);
//					registro1000.setValor_exclusao_dief(null);
//					registro1000.setObsevacao(null);
					
					if(oc != null && oc.getTipofrete() != null){
						if(oc.getTipofrete().equals(Tipofrete.CIF))
							registro1000.setModalidade_frete("C");
						else if(oc.getTipofrete().equals(Tipofrete.FOB))
							registro1000.setModalidade_frete("F");
					}
					
					registro1000.setEmitente_notafiscal("T");
//					registro1000.setCfop_estendido_detalhamento(null);
//					registro1000.setCodigo_transferencia_credito(null);
//					registro1000.setCodigo_recolhimento_iss_retido(null);
//					registro1000.setCodigo_recolhimento_irrf(null);
//					registro1000.setCodigo_observacao(null);
//					registro1000.setData_visto_notas_transf_cred_icms(null);
//					registro1000.setFato_gerador_crf(null);
//					registro1000.setFato_gerador_irrf(null);
					registro1000.setValor_frete(em.getValorfrete() != null ? em.getValorfrete().getValue().doubleValue() : null);
					registro1000.setValor_seguro(em.getValorseguro() != null ? em.getValorseguro().getValue().doubleValue() : null);
					registro1000.setValor_despesas_acessorias(em.getValoroutrasdespesas() != null ? em.getValoroutrasdespesas().getValue().doubleValue() : null);
					registro1000.setValor_pis(em.getValorpis() != null ? em.getValorpis().getValue().doubleValue() : null);
					registro1000.setCodigo_iden_tipo_antecipacao_tributaria(null);
					registro1000.setValor_cofins(em.getValorcofins() != null ? em.getValorcofins().getValue().doubleValue() : null);
//					registro1000.setValor_calc_ref_tare_nota(null);
//					registro1000.setAliquota_valor_calc_ref_tarf_nota(null);
//					registro1000.setValor_bc_icms_st(null);
//					registro1000.setEntradasaida_isenta(null);
//					registro1000.setOutras_entradas_isentas(null);
//					registro1000.setValor_transporte_incluido_base(null);
//					registro1000.setCodigo_ressarcimento(null);
					registro1000.setValor_produtos(em.getValorproduto());
					
					if(ed.getFornecedor() != null && ed.getFornecedor().getEndereco() != null && 
							 ed.getFornecedor().getEndereco().getMunicipio() != null && 
							 ed.getFornecedor().getEndereco().getMunicipio().getCdibge() != null){
						registro1000.setCodigo_municipio(ed.getFornecedor().getEndereco().getMunicipio().getCdibge());
					}
					
//					if(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null){
//						registro1000.setCodigo_modelo_doc_fiscal(ed.getModelodocumentofiscal().getCodigo());
//					}
					
					Integer codigo = 0;
					if(ed.getSituacaodocumento() != null){
						if(Situacaodocumento.DOCUMENTO_REGULAR.equals(ed.getSituacaodocumento())){
							codigo = 0;
						}else if(Situacaodocumento.ESCRITURACAO_EXTEMP_DOC_REGULAR.equals(ed.getSituacaodocumento())){
							codigo = 1;
						}else if(Situacaodocumento.DOCUMENTO_CANCELADO.equals(ed.getSituacaodocumento())){
							codigo = 2;
						}else if(Situacaodocumento.DOCUMENTO_FISCAL_COMPLEMENTAR.equals(ed.getSituacaodocumento())){
							codigo = 6;
						}  
					}
					registro1000.setCodigo_situacao_tributaria(codigo);
					
//					registro1000.setSub_serie(null);
				
					if(ed.getFornecedor() != null){
						registro1000.setInscricao_estadual_fornecedor(ed.getFornecedor().getInscricaoestadual());
						registro1000.setInscricao_municipal_fornecedor(ed.getFornecedor().getInscricaomunicipal());
					}
					
//					registro1000.setCodigo_operacao_prestacao(null);
//					registro1000.setValor_deduzido_receita_tributavel(null);
//					registro1000.setCompetencia(null);
//					registro1000.setOperacao(null);
//					registro1000.setNumero_parecer_fiscal(null);
//					registro1000.setData_parecer_fiscal(null);
//					registro1000.setNumero_declaracao_importacao(null);
//					registro1000.setPossui_benficio_fiscal(null);
					
					if(ed.getModelodocumentofiscal() != null && 
							ed.getModelodocumentofiscal().getNotafiscalprodutoeletronica() != null &&
							ed.getModelodocumentofiscal().getNotafiscalprodutoeletronica()){
						registro1000.setChave_notafiscal_eletronica(ed.getChaveacesso());
					}
					registro1000.setCodigo_recolhimento_fethab(null);
					registro1000.setResponsavel_recolhimento_fethab(null);
					registro1000.setCfop_doc_fiscal(null);
					registro1000.setTipo_cte(0);
					registro1000.setCte_referencia(null);
					registro1000.setModalidade_importacao(null);
					registro1000.setCodigo_informacao_complementar(null);
					registro1000.setInformacao_complementar(ed.getInfoadicionalfisco());
//					registro1000.setClasse_consumo(null);
//					registro1000.setTipo_ligacao(null);
//					registro1000.setGrupo_tensao(null);
//					registro1000.setTipo_assinante(null);
//					registro1000.setKwh_consumido(null);
//					registro1000.setValor_fornecido_consumido_gas_energiaeletrica(null);
//					registro1000.setValor_cobrado_terceiros(null);
//					registro1000.setTipo_doc_importacao(null);
//					registro1000.setNumero_ato_concessorio_regime_drawback(null);
//					registro1000.setNatureza_frete_piscofins(null);
					registro1000.setCst_piscofins(em.getCstpis() != null ? em.getCstpis().getCdnfe() : null);
					registro1000.setBase_credito_piscofins(em.getNaturezabcc() != null ? em.getNaturezabcc().getCdsped() : null);
//					registro1000.setValor_servico_itens_piscofins(null);
					registro1000.setBase_calculo_piscofins(em.getValorbcpis() != null ? em.getValorbcpis().getValue().doubleValue():null);
					registro1000.setAliquota_pis(em.getPis());
					registro1000.setAliquota_cofins(em.getCofins());
					
					if(ed.getModelodocumentofiscal() != null && 
							ed.getModelodocumentofiscal().getNotafiscalservicoeletronica() != null &&
							ed.getModelodocumentofiscal().getNotafiscalservicoeletronica()){
						registro1000.setChave_nfse(ed.getChaveacesso());
					}
					
//					registro1000.setNumero_processo_ato_concessorio(null);
//					registro1000.setOrigem_processo(null);
					if(registro1000.getCodigo_situacao_tributaria() != null && 
							registro1000.getCodigo_situacao_tributaria().equals(1)){
						registro1000.setData_escrituracao(ed.getDtemissao());
					}
//					registro1000.setCfps(null);
//					registro1000.setNatureza_receita_piscofins(null);
					
					registro1000.setListaRegistro1020(this.createRegistro1020(em));
					
					listaRegistro1000.add(registro1000);
				}
			}
		}
		
		return listaRegistro1000;
	}
	
	private List<Registro1020> createRegistro1020(Entregamaterial entregamaterial){
		List<Registro1020> listaRegistro1020 = new ArrayList<Registro1020>();
		
		if(entregamaterial != null){
			if(entregamaterial.getIcms() != null && entregamaterial.getIcms() > 0){
				Registro1020 registro1020 = new Registro1020();
				
				registro1020.setIdentificacao(Registro1020.REG);
				registro1020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.ICMS));
				registro1020.setPercentual_reducao_bc(null);
				registro1020.setBase_calculo(entregamaterial.getValorbcicms() != null ? entregamaterial.getValorbcicms().getValue().doubleValue():null);
				registro1020.setAliquota(entregamaterial.getIcms());
				registro1020.setValor_imposto(entregamaterial.getValoricms() != null ? entregamaterial.getValoricms().getValue().doubleValue():null);
				registro1020.setValor_isentas(null);
				registro1020.setValor_outras(null);
				registro1020.setValor_ipi(null);
				registro1020.setValor_st(null);
				registro1020.setCodigo_recolhimento_imposto(null);
				registro1020.setValor_nao_tributado(null);
				registro1020.setValor_parcela_reduzida(null);
				
				listaRegistro1020.add(registro1020);
			}
			if(entregamaterial.getPis() != null && entregamaterial.getPis() > 0){
				Registro1020 registro1020 = new Registro1020();
				
				registro1020.setIdentificacao(Registro1020.REG);
				registro1020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.PIS));
				registro1020.setPercentual_reducao_bc(null);
				registro1020.setBase_calculo(entregamaterial.getValorbcpis() != null ? entregamaterial.getValorbcpis().getValue().doubleValue():null);
				registro1020.setAliquota(entregamaterial.getPis());
				registro1020.setValor_imposto(entregamaterial.getValorpis() != null ? entregamaterial.getValorpis().getValue().doubleValue():null);
				registro1020.setValor_isentas(null);
				registro1020.setValor_outras(null);
				registro1020.setValor_ipi(null);
				registro1020.setValor_st(null);
				registro1020.setCodigo_recolhimento_imposto(null);
				registro1020.setValor_nao_tributado(null);
				registro1020.setValor_parcela_reduzida(null);
				
				listaRegistro1020.add(registro1020);
			}
			if(entregamaterial.getCofins() != null && entregamaterial.getCofins() > 0){
				Registro1020 registro1020 = new Registro1020();
				
				registro1020.setIdentificacao(Registro1020.REG);
				registro1020.setCodigo_imposto(getCodigoImpostoDominio(Faixaimpostocontrole.COFINS));
				registro1020.setPercentual_reducao_bc(null);
				registro1020.setBase_calculo(entregamaterial.getValorbccofins() != null ? entregamaterial.getValorbccofins().getValue().doubleValue():null);
				registro1020.setAliquota(entregamaterial.getCofins());
				registro1020.setValor_imposto(entregamaterial.getValorcofins() != null ? entregamaterial.getValorcofins().getValue().doubleValue():null);
				registro1020.setValor_isentas(null);
				registro1020.setValor_outras(null);
				registro1020.setValor_ipi(null);
				registro1020.setValor_st(null);
				registro1020.setCodigo_recolhimento_imposto(null);
				registro1020.setValor_nao_tributado(null);
				registro1020.setValor_parcela_reduzida(null);
				
				listaRegistro1020.add(registro1020);
			}
		}
		
		return listaRegistro1020;
	}
	
	private String getCodigoImpostoDominio(Faixaimpostocontrole faixaimpostocontrole) {
		if(faixaimpostocontrole == null) return "";
		
		if(faixaimpostocontrole.equals(Faixaimpostocontrole.ICMS)) return "1";
		else if(faixaimpostocontrole.equals(Faixaimpostocontrole.IPI)) return "2";
		else if(faixaimpostocontrole.equals(Faixaimpostocontrole.ISS)) return "3";
		else if(faixaimpostocontrole.equals(Faixaimpostocontrole.PIS)) return "4";
		else if(faixaimpostocontrole.equals(Faixaimpostocontrole.COFINS)) return "5";
		else return "";
	}
	
	private void gerarArquivoLancamentocontabil(StringBuilder lancamentocontabil, DominiointegracaoFiltro filtro){
		List<Movimentacaocontabil> listaMovimentacaocontabil = movimentacaocontabilService.findForDominiointegracaoLancamentocontabil(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
		
		if(SinedUtil.isListNotEmpty(listaMovimentacaocontabil)) {
			montarRegistro0000(lancamentocontabil, filtro);
			
			for(Movimentacaocontabil movimentacaoContabil : listaMovimentacaocontabil) {
				montarRegistro6000(lancamentocontabil, movimentacaoContabil, filtro);
			}
		}
		
		/*
		if(listaMovimentacaocontabil != null && !listaMovimentacaocontabil.isEmpty()){			
			Integer i = 1;			
			montaCabecalho(lancamentocontabil, filtro, filtro.getDtinicio(), filtro.getDtfim(), filtro.getDominioTipointegracao());
			for(Movimentacaocontabil movimentacaocontabil : listaMovimentacaocontabil){
				montaRegistro02(lancamentocontabil, filtro, movimentacaocontabil, i);		
				i++;
			}		
			montaFinalizador(lancamentocontabil, filtro);
		}
		*/			
	}
	
	private void montarRegistro0000(StringBuilder lancamentocontabil, DominiointegracaoFiltro filtro) {
		DominioIntegracaoRegistro0000 registro0000 = new DominioIntegracaoRegistro0000();
		
		Empresa empresa = empresaService.load(filtro.getEmpresa(), "empresa.cnpj, empresa.cpf");
		registro0000.setInscricaoEmpresa(empresa.getCpfOuCnpj());
		
		lancamentocontabil.append(registro0000.toString());
	}
	
	private void montarRegistro6000(StringBuilder lancamentocontabil, Movimentacaocontabil mc, DominiointegracaoFiltro filtro) {
		DominioIntegracaoRegistro6000 registro6000 = new DominioIntegracaoRegistro6000();
		
		registro6000.setTipoLancamento(mc.getTipo().getSigla());
		
		lancamentocontabil.append(registro6000.toString());
		montarRegistro6100(lancamentocontabil, mc, filtro);
	}
	

	private void montarRegistro6100(StringBuilder lancamentocontabil, Movimentacaocontabil mc, DominiointegracaoFiltro filtro) {
		if(mc.getTipo().equals(MovimentacaocontabilTipoEnum.UM_DEBITO_UM_CREDITO)) {
			montarRegistro6100UmCreditoUmDebito(lancamentocontabil, mc, filtro);
		} else if(mc.getTipo().equals(MovimentacaocontabilTipoEnum.N_DEBITOS_UM_CREDITO)) {
			montarRegistrosCredito6100(lancamentocontabil, mc, filtro);
			montarRegistrosDebito6100(lancamentocontabil, mc, filtro);
		} else {
			montarRegistrosDebito6100(lancamentocontabil, mc, filtro);
			montarRegistrosCredito6100(lancamentocontabil, mc, filtro);
		}
	}
	
	
	private void montarRegistrosDebito6100(StringBuilder lancamentocontabil, Movimentacaocontabil mc, DominiointegracaoFiltro filtro) {
		if(SinedUtil.isListNotEmpty(mc.getListaDebito())) {
			Integer codigoHistoricoContabil = buscarCodigoHistoricoContabil(mc);
			
			for(Movimentacaocontabildebitocredito m : mc.getListaDebito()) {
				DominioIntegracaoRegistro6100 registro6100 = new DominioIntegracaoRegistro6100();
				String historico;
				
				if(m.getHistorico() != null) {
					historico = m.getHistorico();
				} else {
					historico = mc.getHistorico();
				}
				
				registro6100.setContaContabilDebito(m.getContaContabil() != null ? m.getContaContabil().getCodigoalternativo() : null);
				registro6100.setContaContabilCredito(null);
				registro6100.setValor(m.getValor().getValue().doubleValue());
				registro6100.setCodigoHistoricoContabil(codigoHistoricoContabil);
				registro6100.setDescricaoHistoricoContabil(historico != null ? historico : "");
				registro6100.setCodigoFilial(filtro.getCodigoempresa());
				registro6100.setUsuario("");
				
				if(mc.getDtlancamento() != null) {
					registro6100.setDtLancamento(new Date(mc.getDtlancamento().getTime()));
				}
				
				lancamentocontabil.append(registro6100.toString());
			}
		}
	}
	
	private void montarRegistrosCredito6100(StringBuilder lancamentocontabil, Movimentacaocontabil mc, DominiointegracaoFiltro filtro) {
		if(SinedUtil.isListNotEmpty(mc.getListaCredito())) {
			Integer codigoHistoricoContabil = buscarCodigoHistoricoContabil(mc);
			
			for(Movimentacaocontabildebitocredito m : mc.getListaCredito()) {
				DominioIntegracaoRegistro6100 registro6100 = new DominioIntegracaoRegistro6100();
				String historico;
				
				if(m.getHistorico() != null) {
					historico = m.getHistorico();
				} else {
					historico = mc.getHistorico();
				}
				
				registro6100.setContaContabilDebito(null);
				registro6100.setContaContabilCredito(m.getContaContabil() != null ? m.getContaContabil().getCodigoalternativo() : null);
				registro6100.setValor(m.getValor().getValue().doubleValue());
				registro6100.setCodigoHistoricoContabil(codigoHistoricoContabil);
				registro6100.setDescricaoHistoricoContabil(historico != null ? historico : "");
				registro6100.setCodigoFilial(filtro.getCodigoempresa());
				registro6100.setUsuario("");
				
				if(mc.getDtlancamento() != null) {
					registro6100.setDtLancamento(new Date(mc.getDtlancamento().getTime()));
				}
				
				lancamentocontabil.append(registro6100.toString());
			}
		}
	}
	
	private void montarRegistro6100UmCreditoUmDebito(StringBuilder lancamentocontabil, Movimentacaocontabil mc, DominiointegracaoFiltro filtro) {
		DominioIntegracaoRegistro6100 registro6100 = new DominioIntegracaoRegistro6100();
		Integer codigoCredito = null, codigoDebito = null;
		
		if(SinedUtil.isListNotEmpty(mc.getListaDebito())) {
			codigoDebito = mc.getListaDebito().get(0).getContaContabil() != null ? mc.getListaDebito().get(0).getContaContabil().getCodigoalternativo() : null;
		}
		if(SinedUtil.isListNotEmpty(mc.getListaCredito())) {
			codigoCredito = mc.getListaCredito().get(0).getContaContabil() != null ? mc.getListaCredito().get(0).getContaContabil().getCodigoalternativo() : null;
		}
		
		registro6100.setContaContabilDebito(codigoDebito);
		registro6100.setContaContabilCredito(codigoCredito);
		registro6100.setValor(mc.getValor().getValue().doubleValue());
		registro6100.setCodigoHistoricoContabil(buscarCodigoHistoricoContabil(mc));
		registro6100.setDescricaoHistoricoContabil(mc.getHistorico());
		registro6100.setCodigoFilial(filtro.getCodigoempresa());
		registro6100.setUsuario("");
		
		if(mc.getDtlancamento() != null) {
			registro6100.setDtLancamento(new Date(mc.getDtlancamento().getTime()));
		}
		
		lancamentocontabil.append(registro6100.toString());
	}
	
	private Integer buscarCodigoHistoricoContabil(Movimentacaocontabil mc) {
		if(mc.getOperacaocontabil() != null && mc.getOperacaocontabil().getListaDebito() != null) {
			for(Operacaocontabildebitocredito o : mc.getOperacaocontabil().getListaDebito()) {
				if(o.getCodigointegracao() != null)
					return o.getCodigointegracao();
			}
		} 
		if(mc.getOperacaocontabil() != null && mc.getOperacaocontabil().getListaCredito() != null) {
			for(Operacaocontabildebitocredito o : mc.getOperacaocontabil().getListaCredito()) {
				if(o.getCodigointegracao() != null)
					return o.getCodigointegracao();
			}
		}
		if(mc.getOperacaocontabil() != null && mc.getOperacaocontabil().getCodigointegracao() != null) {
			return mc.getOperacaocontabil().getCodigointegracao();
		}
		
		return null;
	}
	
	/**
	 * Cria o registro 03 do lan�amento cont�bil
	 *
	 * @param lancamentocontabil
	 * @param filtro
	 * @param movimentacaocontabil
	 * @param codigosequencial
	 * @author Luiz Fernando
	 */
	private void montaRegistro03(StringBuilder lancamentocontabil, DominiointegracaoFiltro filtro, Movimentacaocontabil movimentacaocontabil, Integer codigosequencial) {
		DominiointegracaoRegistro03 registro03 = new DominiointegracaoRegistro03();
		
		registro03.setIdentificador(DominiointegracaoRegistro03.IDENTIFICADOR);
		registro03.setCodigosequencial(codigosequencial);
		
		if(movimentacaocontabil.getDebito() != null)
			registro03.setContadebito(movimentacaocontabil.getDebito().getCodigoalternativo());
		if(movimentacaocontabil.getCredito() != null)
			registro03.setContacredito(movimentacaocontabil.getCredito().getCodigoalternativo());
		
		registro03.setValorlancamento(movimentacaocontabil.getValor() != null ? movimentacaocontabil.getValor().getValue().doubleValue() : 0.0);
		// pegar codigo na opera��o contabil vinculada (cr�dito, d�bito ou registro de opera��o)
		registro03.setCodigohistorico(1);
		registro03.setHistorico(movimentacaocontabil.getHistorico());
		registro03.setCodigofilialmatriz(filtro.getCodigoempresa());
		
		lancamentocontabil.append(registro03.toString());
	}
	
	/**
	 * Cria o registro 02 do lan�amento cont�bil
	 *
	 * @param lancamentocontabil
	 * @param filtro
	 * @param movimentacaocontabil
	 * @param codigosequencial
	 * @author Luiz Fernando
	 */
	private void montaRegistro02(StringBuilder lancamentocontabil, DominiointegracaoFiltro filtro, Movimentacaocontabil movimentacaocontabil, Integer codigosequencial) {
		
		DominiointegracaoRegistro02 registro02 = new DominiointegracaoRegistro02();
		
		registro02.setIdentificador(DominiointegracaoRegistro02.IDENTIFICADOR);
		registro02.setCodigosequencial(codigosequencial);
		registro02.setTipo("X"); //X = um d�bito para um cr�dito
		if(movimentacaocontabil.getDtlancamento() != null)
			registro02.setDtlancamento(new Date(movimentacaocontabil.getDtlancamento().getTime()));
		if(movimentacaocontabil.getCdusuarioaltera() != null){
			// setar usuario logado
			registro02.setUsuario(usuarioService.load(new Usuario(movimentacaocontabil.getCdusuarioaltera()), "usuario.nome").getNome());
		}
		lancamentocontabil.append(registro02.toString());
		montaRegistro03(lancamentocontabil, filtro, movimentacaocontabil, 1);
	}
	/**
	 * M�todo que cria o arquivo de Cadastro de Fornecedores, Clientes, Remetentes e Destinat�rios
	 *
	 * @param s
	 * @param filtro
	 * @author Luiz Fernando
	 */
	private void gerarArquivoCadastroFornecedorClienteRemetenteDestinatario(StringBuilder s, DominiointegracaoFiltro filtro) {
		List<Fornecedor> listaFornecedor = fornecedorService.findForDominiointegracaoCadastro(filtro);
		List<Cliente> listaCliente = clienteService.findForDominiointegracaoCadastro(filtro);
		DominiointegracaoRegistro11 registro11;
		DominiointegracaoRegistro22 registro22;
		
		if(listaFornecedor != null && !listaFornecedor.isEmpty()){			
			for(Fornecedor fornecedor : listaFornecedor){
				registro11 = new DominiointegracaoRegistro11();
				registro11.setIdentificador(DominiointegracaoRegistro11.IDENTIFICADOR);
				registro11.setCodigoempresa(filtro.getCodigoempresa());
				registro11.setSiglaestado(fornecedor.getEnderecoSiglaEstadoIntegracaoDominio());
//				registro11.setCodigoconta()
				if(fornecedor.getEndereco() != null && fornecedor.getEndereco().getMunicipio() != null)
					registro11.setCodigomunicipio(fornecedor.getEndereco().getMunicipio().getCdibge());
				registro11.setNomereduzido(fornecedor.getNome());
				registro11.setEndereco(fornecedor.getEnderecoLogradouro());
				registro11.setNumeroendereco(fornecedor.getEnderecoNumero());
				registro11.setBranco123(SIntegraUtil.geraStringVazia(30));
				registro11.setCep(fornecedor.getEnderecoCep());
//				registro11.setInscricao();
				registro11.setInscricaoestadual(fornecedor.getInscricaoestadual());
				registro11.setFone(fornecedor.getFoneDominiointegracao());
				registro11.setFax(fornecedor.getTelefoneFax() != null ? fornecedor.getTelefoneFax().getTelefone():null);
				registro11.setAgropecuario("N");
				registro11.setIcms("N");
				if(fornecedor.getTipopessoa() != null){
					if(Tipopessoa.PESSOA_JURIDICA.equals(fornecedor.getTipopessoa()))
						registro11.setTipoinscricao(1);
					else if(Tipopessoa.PESSOA_FISICA.equals(fornecedor.getTipopessoa()))
						registro11.setTipoinscricao(2);
					else {
						registro11.setTipoinscricao(4);
					}
				}
				registro11.setInscricaomunicipal(fornecedor.getInscricaomunicipal());
				registro11.setBairro(fornecedor.getEnderecoBairro());
				registro11.setDddfone(fornecedor.getDDDFoneDominiointegracao());
//				registro11.setCodigopais();
//				registro11.setNumeroinscricaosuframa();
//				registro11.setDatacadastro();
				registro11.setComplementoendereco(fornecedor.getEnderecoComplemento());
//				registro11.setBranco();
				
				s.append(registro11.toString());
			}
		}
		
		if(listaCliente != null && !listaCliente.isEmpty()){			
			for(Cliente cliente : listaCliente){
				registro22 = new DominiointegracaoRegistro22();
				registro22.setIdentificador(DominiointegracaoRegistro22.IDENTIFICADOR);
				registro22.setCodigoempresa(filtro.getCodigoempresa());
				registro22.setSiglaestado(cliente.getEnderecoSiglaEstadoIntegracaoDominio());
//				registro22.setCodigoconta()
				if(cliente.getEndereco() != null && cliente.getEndereco().getMunicipio() != null)
					registro22.setCodigomunicipio(cliente.getEndereco().getMunicipio().getCdibge());
				registro22.setNomereduzido(cliente.getNome());
				registro22.setEndereco(cliente.getEnderecoLogradouro());
				registro22.setNumeroendereco(cliente.getEnderecoNumero());
//				registro22.setBranco123();
				registro22.setCep(cliente.getEnderecoCep());
//				registro22.setInscricao();
				registro22.setInscricaoestadual(cliente.getInscricaoestadual());
				registro22.setFone(cliente.getFoneDominiointegracao());
				registro22.setFax(cliente.getTelefoneFax() != null ? cliente.getTelefoneFax().getTelefone():null);
//				registro22.setAgropecuario();
//				registro22.setIcms();
				if(cliente.getTipopessoa() != null){
					if(Tipopessoa.PESSOA_JURIDICA.equals(cliente.getTipopessoa()))
						registro22.setTipoinscricao(1);
					else if(Tipopessoa.PESSOA_FISICA.equals(cliente.getTipopessoa()))
						registro22.setTipoinscricao(2);
					else {
						registro22.setTipoinscricao(4);
					}
				}
				registro22.setInscricaomunicipal(cliente.getInscricaomunicipal());
				registro22.setBairro(cliente.getEnderecoBairro());
				registro22.setDddfone(cliente.getDDDFoneDominiointegracao());
//				registro22.setAliquotaicms();
//				registro22.setCodigopais();
//				registro22.setNumeroinscricaosuframa();
//				registro22.setDatacadastro();
				registro22.setComplementoendereco(cliente.getEnderecoComplemento());
//				registro22.setBranco();
				
				s.append(registro22.toString());
			}
		}
		
	}
	
	/**
	 * Monta o arquivo saida
	 *
	 * @param saida
	 * @param filtro
	 * @author Luiz Fernando
	 */
	private void gerarArquivoSaida(StringBuilder saida, DominiointegracaoFiltro filtro) {
		List<Notafiscalproduto> listaNfp = notafiscalprodutoService.findForDominiointegracaoSaida(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());

		if(listaNfp != null && !listaNfp.isEmpty()){			
			Integer i = 1;			
			montaCabecalho(saida, filtro, filtro.getDtinicio(), filtro.getDtfim(), filtro.getDominioTipointegracao());
			if(listaNfp != null && !listaNfp.isEmpty()){
				for(Notafiscalproduto nfp : listaNfp){
					montaDetalheSaida(saida, i, nfp, filtro);
					i++;
					i = montaDetalheImpostos(saida, i, null, null, nfp);
					if(nfp.getListaItens() != null && !nfp.getListaItens().isEmpty()){
						for(Notafiscalprodutoitem item : nfp.getListaItens()){
							montaProdutoComInformacoesDNF(saida, filtro, item, i);
							i++;							
						}
					}else
						i++;
				}
			}			
			montaFinalizador(saida, filtro);
		}			
	}
	
	private void gerarArquivoServico(StringBuilder servico, DominiointegracaoFiltro filtro){
		List<NotaFiscalServico> listaNfs = notafiscalServicoService.findForDominiointegracaoSaida(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
		if(listaNfs != null && !listaNfs.isEmpty()){			
			Integer i = 1;			
			montaCabecalho(servico, filtro, filtro.getDtinicio(), filtro.getDtfim(), filtro.getDominioTipointegracao());
			for(NotaFiscalServico nfs : listaNfs){
				montaDetalheServico(servico, filtro, nfs, i);				
				i++;
				i = montaDetalheImpostos(servico, i, null, nfs, null);
			}			
			montaFinalizador(servico, filtro);
		}			
	}

	/**
	 * Monta o arquivo entrada
	 *
	 * @param entrada
	 * @param filtro
	 * @author Luiz Fernando
	 */
	private void gerarArquivoEntrada(StringBuilder entrada, DominiointegracaoFiltro filtro) {		
		List<Entregadocumento> lista = entradafiscalService.findForDominiointegracaoEntrada(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());		
		
		if(lista != null && !lista.isEmpty()){			
			Integer i = 1;
			montaCabecalho(entrada, filtro, filtro.getDtinicio(), filtro.getDtfim(), filtro.getDominioTipointegracao());
			for(Entregadocumento ed : lista){	
				montaDetalheEntrada(entrada, i, ed, filtro);
				i++;
				i = montaDetalheImpostos(entrada, i, ed, null, null);
				if(ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty()){	
					for(Entregamaterial em : ed.getListaEntregamaterial()){
						montaProduto(entrada, filtro, em,i);
						i++;
					}
				}else
					i++;
			}
			montaFinalizador(entrada, filtro);
		}
	}

	/**
	 * Monta o detalhe entrada do arquivo
	 *
	 * @param entrada
	 * @param filtro
	 * @param sequencial
	 * @param e
	 * @author Luiz Fernando
	 */
	private void montaDetalheEntrada(StringBuilder entradaarquivo, Integer sequencial, Entregadocumento ed, DominiointegracaoFiltro filtro) {
		
		Dominiointegracaoentrada entrada = new Dominiointegracaoentrada();
		
		Entregamaterial em = ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty() ?  
				ed.getListaEntregamaterial().iterator().next() : null;		
		Entregapagamento ep = ed.getListadocumento() != null && !ed.getListadocumento().isEmpty() ?
				ed.getListadocumento().iterator().next() : null;
		Ordemcompra oc = null;
		if(ed.getEntrega() != null && ed.getEntrega().getHaveOrdemcompra()){
			Set<Ordemcompraentrega> listaOrdemcompraentrega = ed.getEntrega().getListaOrdemcompraentrega();
			for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
				if(ordemcompraentrega.getOrdemcompra() != null){
					oc = ordemcompraentrega.getOrdemcompra();
					break;
				}
			}
		}
		
		// T -> TAMANHO DO CAMPO
		// TIPO -> A = ALFANUMERICO, N = NUM�RICO
		
//		VALOR FIXO "02" - T02 A
		entrada.setEntrada(Dominiointegracaoentrada.ENTRADA);
//		SEQUENCIAL - T07 N
		entrada.setSequencial(sequencial);
//		C�DIGO DA EMPRESA - T07 N
		entrada.setCodigoempresa(filtro.getCodigoempresa());		
//		INSCRI��O DO FORNECEDOR CNPJ/CPF/CEI/OUTROS - T14 A
		entrada.setInscricaofornecedorCpfCnpjCei(ed.getFornecedor() != null ? ed.getFornecedor().getCpfOuCnpj():null);
//		C�DIGO DA ESP�CIE - T07 N
		entrada.setCodigoespecie(ed.getEspecie());	
//		C�DIGO DA EXCLUS�O DAS DIEF - T02 N
//		entrada.setCodigoexclusaodief();	
//		C�DIGO DO ACUMULADOR - T07 N
		if(ed.getNaturezaoperacao() != null && ed.getIndpag() != null){
			if(ed.getIndpag().equals(Formapagamentonfe.A_VISTA)){
				if(ed.getNaturezaoperacao().getOperacaocontabilavista() != null && 
						ed.getNaturezaoperacao().getOperacaocontabilavista().getCodigointegracao() != null){
					entrada.setCodigoacumulador(ed.getNaturezaoperacao().getOperacaocontabilavista().getCodigointegracao().toString());
				}
			}else if(ed.getIndpag().equals(Formapagamentonfe.A_PRAZO)){
				if(ed.getNaturezaoperacao().getOperacaocontabilaprazo() != null && 
						ed.getNaturezaoperacao().getOperacaocontabilaprazo().getCodigointegracao() != null){
					entrada.setCodigoacumulador(ed.getNaturezaoperacao().getOperacaocontabilaprazo().getCodigointegracao().toString());
				}
			}
		}
//		CFOP - T07 N
		entrada.setCfop(em.getCfop() != null ? em.getCfop().getCodigo():null);
//		SEGUIMENTO - T02 N
//		entrada.setSeguimento();													
//		N�MERO DO DOCUMENTO - T07 N
		entrada.setNumerodocumento(ed.getNumero());				
//		S�RIE - T07 A
		entrada.setSerie(ed.getSerie() != null ? ed.getSerie().toString():null);	
//		DOCUMENTO FINAL - T07 N
//		entrada.setDocumentofinal();
//		DATA DE ENTRADA - T10
		entrada.setDataentrada(ed.getDtentrada());
		
//		DATA DE EMISS�O - T10
		if(ep != null && ep.getDtemissao() != null)
			entrada.setDataemissao(ep.getDtemissao());		
		else 
			entrada.setDataemissao(ed.getDtentrada());
		
//		VALOR CONT�BIL(13,2) - T13 DECIMAL(2)
		entrada.setValorcontabil(ed.getValor() != null ? ed.getValor().getValue().doubleValue():null);
//		VALOR DA EXCLUS�O DA DIEF(13,2) - T13 DECIMAL(2)
//		entrada.setValorexcusaodief();		
//		RESERVADO - T30 A
//		entrada.setReservado();		
		
//		ESTADO DO FORNECEDOR - T02 A
		entrada.setUffornecedor(ed.getFornecedor() != null ? ed.getFornecedor().getEnderecoSiglaEstadoIntegracaoDominio() : null);
//		MODALIDADE DO FRETE - T01 A (C=CIF OU F=FOB)
		if(oc != null && oc.getTipofrete() != null){
			if(oc.getTipofrete().equals(Tipofrete.CIF))
				entrada.setModalidadefrete("C");
			else if(oc.getTipofrete().equals(Tipofrete.FOB))
				entrada.setModalidadefrete("F");
		}
				 
//		EMITENTE DA NOTA - T01 A (P=PR�PRIO OU T=TERCEIRO)
		entrada.setEmitentenota("P");		
//		FATO GERADOR DA CRF - T01 A (E=EMISS�O OU P=PAGAMENTO)
		entrada.setFatogeradorcrf("E");		
//		FATO GERADOR DO IRRF - T01 A (E=EMISS�O OU P=PAGAMENTO)
		entrada.setFatogeradorirrf("E");			
		
//		C�DIGO DO MUNIC�PIO - T07 N
		if(ed.getFornecedor() != null && ed.getFornecedor().getEndereco() != null && 
						 ed.getFornecedor().getEndereco().getMunicipio() != null && 
				 ed.getFornecedor().getEndereco().getMunicipio().getUf() != null && 
				 ed.getFornecedor().getEndereco().getMunicipio().getUf().getCdibge() != null){
			entrada.setCodigomunicipio(ed.getFornecedor().getEndereco().getMunicipio().getUf().getCdibge());
		}
		
//		APENAS PARA O ESTADO DE SE
//		CFOP ESTENDIDO/DETALHAMENTO - T07 N
//		entrada.setCfopestendidodetalhamento();		
		
//		APENAS PARA O ESDADO DO RS
//		C�DIGO DA TRANSFER�NCIA UTILIZADA PARA NOTAS DE TRANSFER�NCIA DE CR�DITOS - T07 N
//		entrada.setCodigotransferencianotacredito();	
				
//		BRANCOS - T06 A
		entrada.setBrancos180(SIntegraUtil.geraStringVazia(6));		
//		BRANCOS - T06 A
		entrada.setBrancos186(SIntegraUtil.geraStringVazia(6));
//		C�DIGO DA OBSERVA��O - T07 N
//		entrada.setCodigoobservacao();		
		
//		PARA NOTAS DE TRANSFER�NCIA DE CR�DITO DE ICMS - APENAS PARA O ESTADO DE MG
//		DATA DO VISTO - T10 DATA
//		entrada.setDatavisto();		
		
		
//		VALOR DO FRETE - T13 DECIMAL(2)
		entrada.setValorfrete(ed.getValorfrete() != null ? ed.getValorfrete().getValue().doubleValue():null);
//		VALOR DO SEGURO - T13 DECIMAL(2)
		entrada.setValorseguro(ed.getValorseguro() != null ? ed.getValorseguro().getValue().doubleValue():null);
//		VALOR DAS DESPESAS ACESS�RIAS - T13 DECIMAL(2)
		entrada.setValordespesasacessorias(ed.getValoroutrasdespesas() != null ? ed.getValoroutrasdespesas().getValue().doubleValue():null);
//		C�DIGO QUE IDENTIFICA O TIPO DA ANTECIPA��O TRIBUT�RIA - T07 N
//		entrada.getCodigoantecipacaotributaria();			
//		VALOR DO PIS - T13 DECIMAL(2)
		entrada.setValorpis(ed.getValortotalpis() != null ? ed.getValortotalpis().getValue().doubleValue():null);
//		VALOR DO COFINS - T13 DECIMAL(2)
		entrada.setValorcofins(ed.getValortotalcofins() != null ? ed.getValortotalcofins().getValue().doubleValue():null);		
//		VALOR DOS PRODUTOS - T13 DECIMAL(2)
		entrada.setValorprodutos(ed.getValormercadoriaForDominio() != null ? ed.getValormercadoriaForDominio().getValue().doubleValue():null);		
		
//		(0=INFORMA��ES COMPLEMENTARES OU 1=QUADRO DE C�LCULO DO IMPOSTO)		
//		APENAS PARA SCANC-CTB		
//		VALOR B.C ICMS ST - T01 N
//		entrada.setValorbcicmsst();		 
		
//		ENTRADAS CUJA SA�DA � ISENTA. APENAS PARA MG - T13 DECIMAL(2)
//		entrada.setEntradassaidaisenta();		
//		OUTRAS ENTRADAS ISENTAS. APENAS PARA MG - T13 DECIMAL(2)
//		entrada.setOutrasentradasisentas();		

//		(SUBST. TRIBUT�RIA) APENAS PARA MG
//		VALOR TRASNPORTE INCLU�DO NA BASE - T13 DECIMAL(2)
//		entrada.setValortransporteincluidobase();
//		RESSARCIMENTO	- T01 N (0=ICMS-SUBTRI A RECOLHER NO PER�ODO OU 1=ICMS-SUBTRI EM OPERA��ES PR�PRIAS)
//		entrada.setRessarcimento();		
				
//		C�DIGO DO MODELO DO DOCUMENTO FISCAL - T07 N
		if(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null){
			entrada.setCodigomodelodocumentofiscal(ed.getModelodocumentofiscal().getCodigo());
		}
//		C�DIGO DA SITUA��O TRIBUT�RIA - T07 N
//		entrada.setCodigosituacaotributaria();		
//		SUB S�RIE - T07 N
//		entrada.setSubserie();		
		
		if(ed.getFornecedor() != null){
//			INSCRI��O ESTADUAL DO FORNECEDOR - T20 A
			entrada.setInscricaoestadualfornecedor(ed.getFornecedor().getInscricaoestadual());
//			INSCRI��O MUNICIPAL DO FORNECEDOR - T20 A
			entrada.setInscricaomunicipalfornecedor(ed.getFornecedor().getInscricaomunicipal());	
		}
		
//		OBSERVA��O - T300 A
//		entrada.setObservacao();
//		CHAVE DA NFe (APENAS CARACTERES N�MERICOS) - T44 A
		entrada.setChavenfe(ed.getChaveacesso());
//		C�DIGO DE RECOLHIMENTO FETHAB - T06 A
//		entrada.setCodigorecolhimentofethab();		
//		RESPONS�VEL PELO RECOLHIMENTO FETHAB - T01 A (E=EMPRESA)
		entrada.setResponsavelrecolhimentofethab("E");		
		
//		POR ENQUANTO � PARA DEIXAR COMO CT-e NORMAL (AT� SABERMOS AO CERTO O QUE ISTO SIGNIFICA)
//		TIPO DE CT-e - T01 N (0=CT-e NORMAL/1=CT-e DE COMPLEMENTO DE VALORES/2=CT-e EMITIDO EM HIP�TESE DE ANULA��O DE D�BITO)
		entrada.setTipocte(0);		
		
//		CTE- REFER�NCIA - T44 A (APENAS CARACTERES NUM�RICOS)
//		entrada.setCtereferencia();		
//		BRANCOS - T48 A
		entrada.setBrancos(SIntegraUtil.geraStringVazia(48));
		
		entradaarquivo.append(entrada.toString());
	}
	
	private Integer montaDetalheImpostos(StringBuilder imposto, Integer sequencial, Entregadocumento ed, NotaFiscalServico nfs, Notafiscalproduto nfp){
		if(ed != null){			
			if(ed.getValortotalicms() != null){
				imposto.append("03"); //FIXO 03
				imposto.append(printNumero(sequencial.toString(), 7)); //SEQUENCIAL
				imposto.append(printNumero("1", 7));	//C�DIGO DO IMPOSTO - N7
				
				incluirImposto(imposto, null, ed.getValortotalbcicms(), null, ed.getValortotalicms(), null, ed.getValoroutrasdespesas(), null, ed.getValortotalipi(), true);
				
				imposto.append(printNumero("", 13));	//VALOR N�O TRIBUTADOS (APENAS PARA GO) - DECIMAL 13,2
				imposto.append(printNumero("", 13));	//VALOR PARCELA REDUZIDA (APENAS PARA GO) - DECIMAL 13,2
				imposto.append(printString("", 74));	//BRANCOS - A74
				imposto.append("\r\n");
				sequencial++;				
			}
			if(ed.getValortotalpis() != null){
				imposto.append("03"); //FIXO 03
				imposto.append(printNumero(sequencial.toString(), 7)); //SEQUENCIAL
				imposto.append(printNumero("2", 7));	//C�DIGO DO IMPOSTO - N7
				
				incluirImposto(imposto, null, null, null, ed.getValortotalpis(), null, ed.getValoroutrasdespesas(), null, ed.getValortotalipi(), true);
				
				imposto.append(printNumero("", 13));	//VALOR N�O TRIBUTADOS (APENAS PARA GO) - DECIMAL 13,2
				imposto.append(printNumero("", 13));	//VALOR PARCELA REDUZIDA (APENAS PARA GO) - DECIMAL 13,2
				imposto.append(printString("", 74));	//BRANCOS - A74
				imposto.append("\r\n");
				sequencial++;
			}			
			
		}else if(nfs != null){
			if(nfs.getValorIcms() != null){
				imposto.append("03"); //FIXO 03
				imposto.append(printNumero(sequencial.toString(), 7)); //SEQUENCIAL
				imposto.append(printNumero("1", 7));	//C�DIGO DO IMPOSTO - N7
				
				incluirImposto(imposto, null, nfs.getBasecalculoicms(), null, nfs.getValorIcms(), null, null, null, null, false);
				
				imposto.append(printString("", 74));	//BRANCOS - A74
				imposto.append("\r\n");
				sequencial++;
			}
			if(nfs.getValorPis() != null){
				imposto.append("03"); //FIXO 03
				imposto.append(printNumero(sequencial.toString(), 7)); //SEQUENCIAL
				imposto.append(printNumero("2", 7));	//C�DIGO DO IMPOSTO - N7
				
				incluirImposto(imposto, null, nfs.getBasecalculopis(), null, nfs.getValorPis(), null, null, null, null, false);
				
				imposto.append(printString("", 74));	//BRANCOS - A74
				imposto.append("\r\n");
				sequencial++;
			}
			if(nfs.getValorIss() != null){
				imposto.append("03"); //FIXO 03
				imposto.append(printNumero(sequencial.toString(), 7)); //SEQUENCIAL
				imposto.append(printNumero("3", 7));	//C�DIGO DO IMPOSTO - N7
				
				incluirImposto(imposto, null, nfs.getBasecalculoiss(), null, nfs.getValorIss(), null, null, null, null, false);
				
				imposto.append(printString("", 74));	//BRANCOS - A74
				imposto.append("\r\n");
				sequencial++;
			}
		}else if(nfp != null){
			if(nfp.getValoricms() != null){
				imposto.append("03"); //FIXO 03
				imposto.append(printNumero(sequencial.toString(), 7)); //SEQUENCIAL
				imposto.append(printNumero("1", 7));	//C�DIGO DO IMPOSTO - N7
				
				incluirImposto(imposto, null, nfp.getValorbcicms(), null, nfp.getValoricms(), null, null, null, nfp.getValoripi(), true);
				
				imposto.append(printString("", 74));	//BRANCOS - A74
				imposto.append("\r\n");
				sequencial++;
			}
			if(nfp.getValorpis() != null){
				imposto.append("03"); //FIXO 03
				imposto.append(printNumero(sequencial.toString(), 7)); //SEQUENCIAL
				imposto.append(printNumero("2", 7));	//C�DIGO DO IMPOSTO - N7
				
				incluirImposto(imposto, null, null, null, nfp.getValorpis(), null, null, null, nfp.getValoripi(), true);
				
				imposto.append(printString("", 74));	//BRANCOS - A74
				imposto.append("\r\n");
				sequencial++;
			}
		}		
		return sequencial;
	}
	
	private void incluirImposto(StringBuilder imposto, Money perc, Money bc, Double aliq, Money valorimposto, Money isentas, Money outrasdespesa, Money valorcontabil, Money valoripi, boolean isipi) {		
		//PERCENTUAL DE REDU��O DA BASE DE CALC - DECIMAL 5,2
		imposto.append(printNumero(perc != null ? formatarDecimal(perc.toString(),2):"", 5));
		
		// BASE DE C�LCULO - DECIMAL 5,2
		imposto.append(printNumero(bc != null ? formatarDecimal(bc.toString(),2):"", 13));	
		
		//ALIQUOTA - DECIMAL 5,2
		imposto.append(printNumero(aliq != null ? formatarDecimal(aliq.toString(),2):"", 5));
		
		//VALOR DO IMPOSTO - DECIMAL 13,2
		imposto.append(printNumero(valorimposto != null ? formatarDecimal(valorimposto.toString(),2):"", 13));
		
		// VALOR ISENTAS - DECIMAL 13,2
		imposto.append(printNumero(isentas != null ? formatarDecimal(isentas.toString(),2):"", 13));
		
		//VALOR DE OUTRAS - DECIMAL 13,2
		imposto.append(printNumero(outrasdespesa != null ? formatarDecimal(outrasdespesa.toString(), 2):"", 13));
		
		if(isipi){
			//VALOR IPI - DECIMAL 13,2
			imposto.append(printNumero(valoripi != null ? formatarDecimal(valoripi.toString(),2):"", 13));
			
			//VALOR DA SUBSTITU���O TRIBUT�RIA - DECIMAL 13,2
			imposto.append(printNumero("", 13));
		}
		
		//VALOR CONT�BIL - DECIMAL 13,2
		imposto.append(printNumero(valorcontabil != null ? formatarDecimal(valorcontabil.toString(),2):"", 13));
		
		//C�DIGO RECOLHIMENTO - DECIMAL 13,2
		imposto.append(printNumero("", 6));
	}
	
	/**
	 * Monta o detalhe saida do arquivo com Nota Fiscal de Produto
	 *
	 * @param saida
	 * @param filtro
	 * @param sequencial
	 * @param nfp
	 * @author Luiz Fernando
	 */
	private void montaDetalheSaida(StringBuilder saidaarquivo, Integer sequencial, Notafiscalproduto nfp, DominiointegracaoFiltro filtro) {		
		
		// T -> TAMANHO DO CAMPO
		// TIPO -> A = ALFANUMERICO, N = NUM�RICO
		
		Dominiointegracaosaida saida = new Dominiointegracaosaida();
		
//		VALOR FIXO "02" - T02 A
		saida.setSaida(Dominiointegracaosaida.SAIDA);
//		SEQUENCIAL - T07 N
		saida.setSequencial(sequencial);
//		C�DIGO DA EMPRESA - T07 N
		saida.setCodigoempresa(filtro.getCodigoempresa() != null ? filtro.getCodigoempresa():null);
//		INSCRI��O DO CLIENTE CNPJ/CPF/CEI/OUTROS - T14 A
		saida.setInscricaoclienteCpfCnpjCei(nfp.getCliente() != null ? nfp.getCliente().getCpfOuCnpj():null);
//		C�DIGO DA ESP�CIE - T07 N
		Integer codigoespecie = 1; // NF
		if(nfp.getNotaStatus() != null && (nfp.getNotaStatus().equals(NotaStatus.NFE_EMITIDA) || 
				nfp.getNotaStatus().equals(NotaStatus.NFE_LIQUIDADA))){
			codigoespecie = 36; // NF-e
		}
		if(codigoespecie != null){
			saida.setCodigoespecie(codigoespecie);
		}
//		C�DIGO DA EXCLUS�O DAS DIEF - T02 N
//		saida.setCodigoexclusaodief();
//		C�DIGO DO ACUMULADOR - T07 N
		if(nfp.getNaturezaoperacao() != null && nfp.getFormapagamentonfe() != null){
			if(nfp.getFormapagamentonfe().equals(Formapagamentonfe.A_VISTA)){
				if(nfp.getNaturezaoperacao().getOperacaocontabilavista() != null){
					saida.setCodigoacumulador(nfp.getNaturezaoperacao().getOperacaocontabilavista().getCodigointegracao());
				}
			}else if(nfp.getFormapagamentonfe().equals(Formapagamentonfe.A_PRAZO)){
				if(nfp.getNaturezaoperacao().getOperacaocontabilaprazo() != null){
					saida.setCodigoacumulador(nfp.getNaturezaoperacao().getOperacaocontabilaprazo().getCodigointegracao());
				}
			}
		}
//		CFOP - T07 N
//		saida.setCfop();
//		SIGLA DO ESTADO DO CLIENTE - T02 A
		saida.setSiglaestadocliente(nfp.getCliente() != null ? nfp.getCliente().getEnderecoSiglaEstado():null);
//		SEGUIMENTO - T02 N
//		saida.setSeguimento();
//		N�MERO DO DOCUMENTO - T07 N
		saida.setNumerodocumento(nfp.getNumero());	
//		S�RIE - T07 A
//		saida.setSerie();		
//		DOCUMENTO FINAL - T07 N
//		saida.setDocumentofinal();
//		DATA DE ENTRADA - T10
		if(nfp.getDatahorasaidaentrada() != null){
			saida.setDatasaida(new Date(nfp.getDatahorasaidaentrada().getTime()));
		}
//		DATA DE EMISS�O - T10
		saida.setDataemissao(nfp.getDtEmissao());		
//		VALOR CONT�BIL(13,2) - T13 DECIMAL(2)
		saida.setValorcontabil(nfp.getValor() != null ? nfp.getValor().getValue().doubleValue():null);
//		VALOR DA EXCLUS�O DA DIEF(13,2) - T13 DECIMAL(2)
//		saida.setValorexcusaodief();		 
//		RESERVADO - T30 A
		saida.setReservado(SIntegraUtil.geraStringVazia(30));
//		MODALIDADE DO FRETE - T01 A (C=CIF OU F=FOB)
//		saida.setModalidadefrete();
//		C�DIGO DO MUNIC�PIO - T02 A
//		saida.setCodigomunicipio();
//		FATO GERADOR DA CRF - T01 A (E=EMISS�O OU P=PAGAMENTO)
//		saida.setFatogeradorcrf();				
//		FATO GERADOR DA CRFOP - T01 A (E=EMISS�O OU P=PAGAMENTO)
//		saida.setFatogeradorcrfop();			
//		FATO GERADOR DA IRRFP - T01 A (E=EMISS�O OU P=PAGAMENTO)
//		saida.setFatogeradorirrfp();				
//		TIPO DA RECEITA - T01 N
//		saida.setTiporeceita();	
//		BRANCO - T01 A
		saida.setBranco167(SIntegraUtil.geraStringVazia(1));		
		
//		APENAS PARA O ESTADO DE SE
//		CFOP ESTENDIDO/DETALHAMENTO - T07 N
//		saida.setCfopestendidodetalhamento();	
		
//		APENAS PARA O ESTADO DO RS		
//		C�DIGO DA TRANSFER�NCIA UTILIZADO PARA NOTAS DE TRANSFER�NCIA - T07 N
//		saida.setCodigotransferencianotacredito();				
					
//		C�DIGO DA OBSERVA��O - T07 N
//		saida.setCodigoobservacao();		
		
//		APENAS PARA O ESTADO DE MG - PARA NOTAS DE TRANSFER�NCIA DE CR�DITO DE ICMS
//		DATA DO VISTO - T10 DATA
//		saida.setDatavisto();
//		C�DIGO QUE IDENTIFICA O TIPO DA ANTECIPA��O TRIBUT�RIA - T07 N
//		saida.setCodigotipoantecipacaotributaria();		
//		VALOR DO FRETE - T13 DECIMAL(2)
		saida.setValorfrete(nfp.getValorfrete() != null ? nfp.getValorfrete().getValue().doubleValue():null);		
//		VALOR DO SEGURO - T13 DECIMAL(2)
		saida.setValorseguro(nfp.getValorseguro() != null ? nfp.getValorseguro().getValue().doubleValue():null);		
//		VALOR DAS DESPESAS ACESS�RIAS - T13 DECIMAL(2)
		saida.setValordespesasacessorias(nfp.getOutrasdespesas() != null ? nfp.getOutrasdespesas().getValue().doubleValue():null);
//		VALOR DOS PRODUTOS - T13 DECIMAL(2)
		saida.setValorprodutos(nfp.getValorprodutos() != null ? nfp.getValorprodutos().getValue().doubleValue():null);
//		APENAS PARA SCANC-CTB
//		VALOR B.C. ICMS ST - T01 N (0=INFORMA��ES COMPLEMENTARES OU 1=QUADRO C�LCULO IMPOSTO)
//		saida.setValorbcicmsst();	
		
//		APENAS PARA MG
//		OUTRAS SA�DAS - T13 DECIMAL(2)
//		saida.setOutrassaidas();		
//		SA�DAS ISENTAS - T13 DECIMAL(2)
//		saida.setSaidasisentas();		
//		SA�DAS ISENTAS(CUPOM FISCAL) - T13 DECIMAL(2)
//		saida.setSaidasisentascupomfiscal();		
//		SA�DAS ISENTAS(N. FISCAL MOD. 02) - T13 DECIMAL(2)
//		saida.setSaidasisentasmodelo02();	
		
//		C�DIGO DO MODELO DO DOCUMENTO FISCAL - T07 N
//		saida.setCodigomodelodocumentofiscal();		
//		C�DIGO FISCAL DA PRESTA��O DE SERVI�OS - T07 N
//		saida.setCodigofiscalprestacaoservico();
//		C�DIGO DA SITUA��O TRIBUT�RIA - T07 N
//		saida.setCodigosituacaotributaria();
//		SUB-S�RIE - T07 N
//		saida.setSubserie();		
		
//		TIPO DO T�TULO - T02 A (00=DUPLICATA/01=CHEQUE/02=PROMISS�RIA/03=RECIBO/99=OUTRO)
//		if(e.getTipotitulo() != null){
//			if(e.getTipotitulo().equals(Tipotitulocredito.DUPLICATA))
//				saida.setTipotitulo("00");
//			else if (e.getTipotitulo().equals(Tipotitulocredito.CHEQUE))
//				saida.setTipotitulo("01");
//			else if(e.getTipotitulo().equals(Tipotitulocredito.PROMISSORIA))
//				saida.setTipotitulo("02");
//			else if(e.getTipotitulo().equals(Tipotitulocredito.RECIBO))
//				saida.setTipotitulo("03");
//			else if(e.getTipotitulo().equals(Tipotitulocredito.OUTROS))
//				saida.setTipotitulo("99");		
//		}
		
//		IDENTIFICA��O DO T�TULO - T50 A
//		saida.setIndentificacaotitulo();
		
		if(nfp.getCliente() != null){
//			INSCRI��O ESTADUAL DO CLIENTE - T20 A
			saida.setInscricaoestadualcliente(nfp.getCliente().getInscricaoestadual());
//			INSCRI��O MUNICIPAL DO CLIENTE - T20 A
			saida.setInscricaomunicipalcliente(nfp.getCliente().getInscricaomunicipal());
		}			
		
//		OBSERVA��O - T300 A
		saida.setObservacao(nfp.getDadosAdicionais());		
//		CHAVE NFe (APENAS CARACTERES NUM�RICOS) - T44 A
		saida.setChavenfe(nfp.getChaveAcesso());
//		C�DIGO DO RECOLHIMENTO FETHAB - T06 A
//		saida.setCodigorecolhimentofethab();
//		RESPONS�VEL PELO RECOLHIMENTO FETHAB - T01 A (E=EMPRESA/C=CLIENTE)
//		saida.setResponsavelrecolhimentofethab();					
		
//		TIPO DE CT-e - T01 N (0=CT-e NORMAL/1=CT-e DE COMPLEMENTO VALORES/2=CT-e EMITIDO EM HIP�TESE DE ANULA��O DE D�BITO)
		saida.setTipocte(0);
//		CT-e REFER�NCIA - T44 A (APENAS CARACTERES NUM�RICOS)
//		saida.setCtereferencia();
//		BRANCOS - T48 A
		saida.setBrancos(SIntegraUtil.geraStringVazia(48));
		
		saidaarquivo.append(saida.toString());
	}	
	
	/**
	 * Monta o detalhe produto da entrega do arquivo
	 *
	 * @param produto
	 * @param filtro
	 * @param em
	 * @param sequencial
	 * @author Luiz Fernando
	 */
	private void montaProduto(StringBuilder produtoarquivo, DominiointegracaoFiltro filtro, Entregamaterial em, Integer sequencial){
		Dominiointegracaoproduto produto = new Dominiointegracaoproduto();
		Money valortotal = new Money();
		Double custototal = 0.0;
		
//		PRODUTOS VALOR FIXO "04" - T02 A
		produto.setProduto(Dominiointegracaoproduto.PRODUTO);
//		SEQUENCIAL - T07 N
		produto.setSequencial(sequencial);
//		C�DIGO DO PRODUTO - T14 A
		produto.setCodigoproduto(em.getMaterial() != null && em.getMaterial().getCdmaterial() != null ? em.getMaterial().getCdmaterial().toString():null);
//		QUANTIDADE - T13 DECIMAL(4)
		produto.setQuantidade(em.getQtde());
		
//		VALOR TOTAL (BASE CALC. + IPI)(13,3)- T13 DECIMAL(3)		
		if(em.getValorbcipi() != null)
			valortotal = valortotal.add(em.getValorbcipi());			
		if(em.getValoripi() != null)
			valortotal = valortotal.add(em.getValoripi());
		produto.setValortotal(valortotal != null ? valortotal.getValue().doubleValue():null);		
		
//		VALOR IPI(13,3) - T13 DECIMAL(3)
		produto.setValoripi(em.getValoripi() != null ? em.getValoripi().getValue().doubleValue():null);
//		BASE DE CALC.(13,3) - T13 DECIMAL(3)
		produto.setBasecalculo(em.getValorbcipi() != null ? em.getValorbcipi().getValue().doubleValue():null);
//		1=NOTA 2=EXTRA - T01 N
		produto.setNotaextra(1);
//		DATA - T10 DATA
//		produto.setData();
//		C�DIGO DA SITUA��O TRIBUT�RIA DO ICMS - T07 N
		produto.setCodigosituacaotributariaicms(em.getCsticms() != null ? em.getCsticms().getCdnfe() : null);		
//		VALOR BRUTO DO PRODUTO - T13 DECIMAL(2)		
		produto.setValorbrutoproduto(em.getValorproduto());
//		BASE DE CALCULO DO ICMS - T13 DECIMAL(2)
		produto.setBasecalculoicms(em.getValorbcicms() != null ? em.getValorbcicms().getValue().doubleValue():null);
//		BASE DE CALCULO DO ICMS P/ SUBSTITUI��O TRIBUT�RIA T-13 DECIMAL(2)
		produto.setBasecalculoicmsst(em.getValorbcicmsst() != null ? em.getValorbcicmsst().getValue().doubleValue():null);
//		ALIQUOTA DO ICMS - T3 DECIMAL(2)
		produto.setAliquotaicms(em.getIcms());
		
//		APENAS PARA O ESTADO DE PE
//		PRODUTO INCENTIVADO (S/N) - T01 A
//		produto.setProdutoincentivado();	
//		C�DIGO DA APURA��O - T07 N
//		produto.setCodigoapuracao();	
		
//		VALOR DO FRETE(13,2) - T13 DECIMAL(2)
		produto.setValorfrete(em.getValorfrete() != null ? em.getValorfrete().getValue().doubleValue():null);			
//		VALOR SEGURO(13,2) - T13 DECIMAL(2)
		produto.setValorseguro(em.getValorseguro() != null ? em.getValorseguro().getValue().doubleValue():null);
//		VALOR DAS DESPESAS ACESS�RIAS(13,2) - T13 DECIMAL(2)
		produto.setValordespesasacessorias(em.getValoroutrasdespesas() != null ? em.getValoroutrasdespesas().getValue().doubleValue():null);
//		QUANTIDADE DE GASOLINA A(13,3) - T13 DECIMAL(3)
//		produto.setQuantidadegasolina();
//		VALOR DO ICMS(13,2) - T13 DECIMAL(2)
		produto.setValoricms(em.getValoricms() != null ? em.getValoricms().getValue().doubleValue():null);
		
//		VALOR DA SUBTRI(13,2) - T13 DECIMAL(2)
//		produto.setValorsubtri();	
//		VALOR DE ISENTAS IPI(13,2) - T13 DECIMAL(2)
//		produto.setValorisentasipi();	
//		VALOR DE OUTRAS IPI(13,2) - T13 DECIMAL(2)
//		produto.setValoroutrasipi();	
//		ICMS NFP(13,2) - T13 DECIMAL(2)
//		produto.setIcmsnfp();	
		
//		VALOR UNIT�RIO(15,6) - T15 DECIMAL(6)
		produto.setValorunitario(em.getValorunitario());
//		ALIQUOTA DA SUBSTITUICAO TRIBUT�RIA(5,2) - T05 DECIMAL(2)
		produto.setAliquotasubstituicaotributaria(em.getIcms());
//		C�DIGO DE TRIBUTA��O DO IPI - T07 N
		produto.setCodigotributacaoipi(em.getCstipi() != null ? em.getCstipi().getCdnfe() : null);
//		ALIQUOTA DE IPI(5,2) - T05 DECIMAL(2)
		produto.setAliquotaipi(em.getIpi());
		
//		BASE DE CALCULO ISSQN(13,2) - T13 DECIMAL(2)
//		produto.setBasecalculoissqn();	
//		ALIQUOTA DE ISSQN(5,2) - T05 DECIMAL(2)
//		produto.setAliquotaissqn();	
//		VALOR ISSQN(13,2) - T13 DECIMAL(2)
//		produto.setValorissqn();	
		
//		CFOP - T07 N
		produto.setCfop(em.getCfop() != null ? em.getCfop().getCodigo():null);
//		S�RIE DO ECF - T20 A
//		produto.setSerieecf();
//		ALIQUOTA DE PIS(5,2) - T05 DECIMAL(2)
		produto.setAliquotapis(em.getPis());
//		VALOR PIS(13,2) - T13 DECIMAL(2)
		produto.setValorpis(em.getValorpis() != null ? em.getValorpis().getValue().doubleValue():null);
//		ALIQUOTA DE COFINS(5,2) - T05 DECIMAL(2)
		produto.setAliquotacofins(em.getCofins());
//		VALOR COFINS(13,2) - T13 DECIMAL(2)
		produto.setValorcofins(em.getValorcofins() != null ? em.getValorcofins().getValue().doubleValue():null);
		
//		CUSTO TOTAL(13,2) - T13 DECIMAL(2)
		if(em.getValorunitario() != null)
			custototal += em.getValorunitario();
		if(em.getQtde() != null)
			custototal += em.getQtde();
		produto.setCustototal(custototal);
		
//		CST DO PIS - T07 N
		produto.setCstpis(em.getCstpis() != null ? em.getCstpis().getCdnfe() : null);
//		BASE DE CALCULO DO PIS(13,2) - T13 DECIMAL(2)
		produto.setBasecalculopis(em.getValorbcpis() != null ? em.getValorbcpis().getValue().doubleValue():null);
//		CST DO COFINS - T07 N
		produto.setCstcofins(em.getCstcofins() != null ? em.getCstcofins().getCdnfe() : null);
//		BASE DE CALCULO DO COFINS(13,2) - T13 DECIMAL(2)
		produto.setBasecalculocofins(em.getValorbccofins() != null ? em.getValorbccofins().getValue().doubleValue():null);
		
//		CHASSI DO VE�CULO - T17 A
//		produto.setChassiveiculo();	
//		TIPO DE OPERA��O COM VE�CULO - T01 N (0=CONCESSIONARIA/1=FATURAMENTO/2=VENDA DIRETA/9=OUTRAS)
//		produto.setTipooperacaocomveiculo();					
//		LOTE DO MEDICAMENTO - T255 A
//		produto.setLotemedicamento();	
//		QUANTIDADE DE ITEM POR LOTE DE MEDICAMENTO - T07 N
//		produto.setQuantidadeitemporlotemedicamento();		
//		DATA DE VALIDADE - T10 DATA
//		produto.setDatavalidade();	
//		DATA DE FABRICA��O DO MEDICAMENTO - T10 DATA
//		produto.setDatafabricacaomedicamento();	
//		REFER�NCIA BASE DE C�LCULO - T01 N (0=TABELADO/1=NEGATIVA/2=POSITIVA/3=NEUTRA/4=MARGEM VALOR AGREGADO)
//		produto.setReferenciabasecalculo();					
//		VALOR TABELADO/MAXIMO(13,2) - T13 DECIMAL(2)
//		produto.setValortabeladomaximo();	
//		NUMERO DE SERIE DA ARMA - T255 A
//		produto.setNumeroseriearma();
//		NUMERO DE SERIE DO CANO - T255 A
//		produto.setNumeroseriecano();	
//		ENQUADRAMENTO DO IPI - T03 A
//		produto.setEnquadramentoipi();	
//		BRANCOS - T100 A
		produto.setBrancos(SIntegraUtil.geraStringVazia(100));
		
		produtoarquivo.append(produto.toString());
		
	}
	
	/**
	 * Monta o detalhe produto da nota fiscal de produto do arquivo
	 *
	 * @param produto
	 * @param filtro
	 * @param item
	 * @param sequencial
	 * @author Luiz Fernando
	 */
	private void montaProdutoComInformacoesDNF(StringBuilder produtodnfarquivo, DominiointegracaoFiltro filtro, Notafiscalprodutoitem item, Integer sequencial){

		Dominiointegracaoprodutodnf produtodnf = new Dominiointegracaoprodutodnf();
		Money valortotal = new Money();
		
//		PRODUTOS VALOR FIXO "06" - T02 A
		produtodnf.setProduto(Dominiointegracaoprodutodnf.PRODUTO);
//		SEQUENCIAL - T07 N
		produtodnf.setSequencial(sequencial);
//		C�DIGO DO PRODUTO - T14 A
		produtodnf.setCodigoproduto(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null ?
				item.getMaterial().getCdmaterial().toString():null);		
//		QUANTIDADE - T13 DECIMAL(4)
		produtodnf.setQuantidade(item.getQtde() != null ? item.getQtde():null);
		
		
//		VALOR TOTAL (BASE CALC. + IPI)(13,3)- T13 DECIMAL(3)
		if(item.getValorbcipi() != null)
			valortotal = valortotal.add(item.getValorbcipi());
		if(item.getValoripi() != null)
			valortotal = valortotal.add(item.getValoripi());
		produtodnf.setValortotal(valortotal != null ? valortotal.getValue().doubleValue():null);
		
//		VALOR IPI(13,3) - T13 DECIMAL(3)
		produtodnf.setValoripi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue():null);
//		BASE DE CALC.(13,3) - T13 DECIMAL(3)
		produtodnf.setBasecalculo(item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue():null);
//		1=NOTA 2=EXTRA - T01 N
		produtodnf.setNotaextra(1);	
//		DATA - T10 DATA
//		produtodnf.setData();
//		N�MERO DE REGISTRO DA DECLARA��O DE IMPORTA��O - T10 N
//		produtodnf.setNumeroregistrodeclaracaoimportacao();
//		C�DIGO DA SITUA��O TRIBUT�RIA DO ICMS - T07 N
		produtodnf.setCodigosituacaotributariaicms(item.getTipocobrancaicms() != null ? item.getTipocobrancaicms().getCdnfe():null);
//		VALOR BRUTO DO PRODUTO - T13 DECIMAL(2)
		produtodnf.setValorbrutoproduto(item.getValorunitario() != null ? item.getValorunitario() : null);
//		VALOR DESCONTO - T13 DECIMAL(2)
		produtodnf.setValordesconto(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue():null);
//		BASE DE CALCULO DO ICMS - T13 DECIMAL(2)
		produtodnf.setValoricms(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue():null);		
//		BASE DE CALCULO DO ICMS P/ SUBSTITUI��O TRIBUT�RIA T-13 DECIMAL(2)
		produtodnf.setBasecalculoicmsst(item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue():null);
//		ALIQUOTA DO ICMS - T3 DECIMAL(2)
		produtodnf.setAliquotaicms(item.getIcms());
//		APENAS PARA O ESTADO DE PE	
//		PRODUTO INCENTIVADO (S/N) - T01 A
//		produtodnf.setProdutoincentivado();		
//		C�DIGO DA APURA��O - T07 N
//		produtodnf.setCodigoapuracao();
//		VALOR DO FRETE(13,2) - T13 DECIMAL(2)
		produtodnf.setValorfrete(item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue():null);
//		VALOR SEGURO(13,2) - T13 DECIMAL(2)
		produtodnf.setValorseguro(item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue():null);			
//		VALOR DAS DESPESAS ACESS�RIAS(13,2) - T13 DECIMAL(2)
		produtodnf.setValordespesasacessorias(item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue():null);
//		QUANTIDADE DE GASOLINA A(13,3) - T13 DECIMAL(3)
//		produtodnf.setQuantidadegasolina();
//		VALOR DO ICMS(13,2) - T13 DECIMAL(2)
		produtodnf.setValoricms(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue():null);
//		VALOR DA SUBTRI(13,2) - T13 DECIMAL(2)
//		produtodnf.setValorsubtri();	
//		VALOR DE ISENTAS IPI(13,2) - T13 DECIMAL(2)
//		produtodnf.setValorisentasipi()	
//		VALOR DE OUTRAS IPI(13,2) - T13 DECIMAL(2)
//		produtodnf.setValoroutrasipi();	
//		ICMS NFP(13,2) - T13 DECIMAL(2)
//		produtodnf.setIcmsnfp();	
//		VALOR UNIT�RIO(15,6) - T15 DECIMAL(6)
		produtodnf.setValorbrutoproduto(item.getValorunitario() != null ? item.getValorunitario() : null);
//		ALIQUOTA DA SUBSTITUICAO TRIBUT�RIA(5,2) - T05 DECIMAL(2)
		produtodnf.setAliquotasubstituicaotributaria(item.getIcmsst());
//		C�DIGO DE TRIBUTA��O DO IPI - T07 N
		produtodnf.setCodigotributacaoipi(item.getTipocobrancaipi() != null ? item.getTipocalculoipi().getValue():null);			
//		ALIQUOTA DE IPI(5,2) - T05 DECIMAL(2)
		produtodnf.setAliquotaipi(item.getIpi());
//		BASE DE CALCULO ISSQN(13,2) - T13 DECIMAL(2)
//		produtodnf.setBasecalculoissqn();	
//		ALIQUOTA DE ISSQN(5,2) - T05 DECIMAL(2)
//		produtodnf.setAliquotaissqn();	
//		VALOR ISSQN(13,2) - T13 DECIMAL(2)
//		produtodnf.setValorissqn();
//		CFOP - T07 N
		produtodnf.setCfop(item.getCfop() != null ? item.getCfop().getCodigo():null);
//		S�RIE DO ECF - T20 A
//		produtodnf.setSerieecf();
//		ALIQUOTA DE PIS(5,2) - T05 DECIMAL(2)
		produtodnf.setAliquotapis(item.getPis());		
//		VALOR PIS(13,2) - T13 DECIMAL(2)
		produtodnf.setValorpis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue():null);
//		ALIQUOTA DE COFINS(5,2) - T05 DECIMAL(2)
		produtodnf.setAliquotacofins(item.getCofins());
//		VALOR COFINS(13,2) - T13 DECIMAL(2)
		produtodnf.setValorcofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue():null);
//		CUSTO TOTAL(13,2) - T13 DECIMAL(2)
//		produtodnf.setCustototal();		
//		CST DO PIS - T07 N
		produtodnf.setCstpis(item.getTipocobrancapis() != null ? item.getTipocalculopis().getValue():null );
//		BASE DE CALCULO DO PIS(13,2) - T13 DECIMAL(2)
		produtodnf.setBasecalculopis(item.getPis());
//		CST DO COFINS - T07 N
		produtodnf.setCstcofins(item.getTipocobrancacofins() != null ? item.getTipocalculocofins().getValue():null);
//		BASE DE CALCULO DO COFINS(13,2) - T13 DECIMAL(2)
		produtodnf.setBasecalculocofins(item.getCofins());
//		CHASSI DO VE�CULO - T17 A
//		produtodnf.setChassiveiculo();		
//		TIPO DE OPERA��O COM VE�CULO - T01 N (0=CONCESSIONARIA/1=FATURAMENTO/2=VENDA DIRETA/9=OUTRAS)
//		produtodnf.setTipooperacaocomveiculo();
//		LOTE DO MEDICAMENTO - T255 A
//		produtodnf.setLotemedicamento();	
//		QUANTIDADE DE ITEM POR LOTE DE MEDICAMENTO - T07 N
//		produtodnf.setQuantidadeitemporlotemedicamento();
//		DATA DE VALIDADE - T10 DATA
//		produtodnf.setDatavalidade();
//		DATA DE FABRICA��O DO MEDICAMENTO - T10 DATA
//		produtodnf.setDatafabricacaomedicamento();
//		REFER�NCIA BASE DE C�LCULO - T01 N (0=TABELADO/1=NEGATIVA/2=POSITIVA/3=NEUTRA/4=MARGEM VALOR AGREGADO)
//		produtodnf.setReferenciabasecalculo();
//		VALOR TABELADO/MAXIMO(13,2) - T13 DECIMAL(2)
//		produtodnf.setValortabeladomaximo();
//		NUMERO DE SERIE DA ARMA - T255 A
//		produtodnf.setNumeroseriearma();
//		NUMERO DE SERIE DO CANO - T255 A
//		produtodnf.setNumeroseriecano();
//		ENQUADRAMENTO DO IPI - T03 A
//		produtodnf.setEnquadramentoipi();
//		BRANCOS - T100 A
		produtodnf.setBrancos(SIntegraUtil.geraStringVazia(100));
		
		produtodnfarquivo.append(produtodnf.toString());
	}

	/**
	 * Monta o detalhe servi�o da nota fiscal de servi�o do arquivo
	 *
	 * @param servico
	 * @param filtro
	 * @param item
	 * @param nfs
	 * @param sequencial
	 * @author Luiz Fernando
	 */
	private void montaDetalheServico(StringBuilder servicoarquivo, DominiointegracaoFiltro filtro, NotaFiscalServico nfs, Integer sequencial){
		
		Dominiointegracaoservico servico = new Dominiointegracaoservico();
//		SERVI�OS VALOR FIXO "02" - T02 A
		servico.setServico(Dominiointegracaoservico.SERVICO);
//		SEQUENCIAL - T07 N
		servico.setSequencial(sequencial);
//		C�DIGO DA EMPRESA - T07 N
		servico.setCodigoempresa(filtro.getCodigoempresa() != null ? filtro.getCodigoempresa().toString():null);
//		INSCRI��O DO CLIENTE CNPJ/CPF/CEI/OUTROS - T14 A
		servico.setInscricaofornecedorCpfCnpjCei(nfs.getCliente() != null ?	nfs.getCliente().getCpfOuCnpj():"");				
//		C�DIGO DA ESP�CIE - T07 N
		Integer codigoespecie = 31;
		if(nfs.getNotaStatus() != null && (nfs.getNotaStatus().equals(NotaStatus.NFSE_EMITIDA) || 
				nfs.getNotaStatus().equals(NotaStatus.NFSE_LIQUIDADA))){
			codigoespecie = 38;
		}
		if(codigoespecie != null){
			servico.setCodigoespecie(codigoespecie);
		}		
		
//		C�DIGO DO ACUMULADOR - T07 N
		if(nfs.getNaturezaoperacao() != null && nfs.getIndicadortipopagamento() != null){
			if(nfs.getIndicadortipopagamento().equals(Indicadortipopagamento.A_VISTA)){
				if(nfs.getNaturezaoperacao().getOperacaocontabilavista() != null){
					servico.setCodigoacumulador(nfs.getNaturezaoperacao().getOperacaocontabilavista().getCodigointegracao());
				}
			}else if(nfs.getIndicadortipopagamento().equals(Indicadortipopagamento.A_PRAZO)){
				if(nfs.getNaturezaoperacao().getOperacaocontabilaprazo() != null){
					servico.setCodigoacumulador(nfs.getNaturezaoperacao().getOperacaocontabilaprazo().getCodigointegracao());
				}
			}
		}
//		SIGLA DO ESTADO DO CLIENTE - T02 A
		servico.setSiglaestadocliente(nfs.getCliente() != null ? nfs.getCliente().getEnderecoSiglaEstado(): null);		
//		SEGUIMENTO - T02 N
//		servico.setSeguimento();
//		N�MERO DO DOCUMENTO - T07 N		
		servico.setNumerodocumento(nfs.getNumero() != null ? nfs.getNumero():"");
//		DOCUMENTO FINAL - T07 N		
//		servico.setDocumentofinal();		
//		S�RIE - T07 A
//		servico.setSerie();		
//		DATA DE SERVI�O - T10 DATA
//		servico.setDatasservico();
//		DATA DE EMISS�O - T10
		servico.setDataemissao(nfs.getDtEmissao());
//		VALOR CONTABIL - T13 N
		servico.setValorcontabil(nfs.getValorNota() != null ? nfs.getValorNota().getValue().doubleValue():null);
//		RESERVADO - T30 A
		servico.setReservado(SIntegraUtil.geraStringVazia(30));
//		FATO GERADOR DA CRF - T01 A (E=EMISS�O OU P=PAGAMENTO)
//		servico.setFatogeradorcrf();					
//		FATO GERADOR DA IRRF - T01 A (E=EMISS�O OU P=PAGAMENTO)
//		servico.setFatogeradorirrf();					
//		FATO GERADOR DA CRFOP - T01 A (E=EMISS�O OU P=PAGAMENTO)
//		servico.setFatogeradorcrfop();			
//		FATO GERADOR DA IRRFP - T01 A (E=EMISS�O OU P=PAGAMENTO)
//		servico.setFatogeradorirrfp();			
//		BRANCO - T01 A
		servico.setBranco142(SIntegraUtil.geraStringVazia(1));
//		C�DIGO MUNIC�PIO - T07 N
//		servico.setCodigomunicipio();
//		C�DIGO DA OBSERVA��O - T07 N
//		servico.setCodigoobservacao();		
//		C�DIGO DO MODELO DO DOCUMENTO FISCAL - T07 N
//		servico.setCodigomodelodocumentofiscal();		
//		C�DIGO FISCAL DA PRESTA��O DE SERVI�OS - T07 N
//		servico.setCodigofiscalprestacaoservico();		
//		SUB-S�RIE - T07 N
//		servico.setSubserie();	
		
		if(nfs.getCliente() != null){
//			INSCRI��O ESTADUAL DO CLIENTE - T20 A
			servico.setInscricaoestadualcliente(nfs.getCliente().getInscricaoestadual());		
//			INSCRI��O MUNICIPAL DO CLIENTE - T20 A
			if(nfs.getInscricaomunicipal() != null && !nfs.getInscricaomunicipal().equals(""))
				servico.setInscricaomunicipalcliente(nfs.getInscricaomunicipal());		
			else 
				servico.setInscricaomunicipalcliente(nfs.getCliente().getInscricaomunicipal());
		}
//		OBSERVA��O - T300 A
		servico.setObservacao(nfs.getDadosAdicionais());			
//		BRANCOS - T100 A
		servico.setBrancos(SIntegraUtil.geraStringVazia(100));
		
		servicoarquivo.append(servico.toString());
	}
	
	/**
	 * Monta o final do arquivo
	 *
	 * @param finalizador
	 * @param filtro
	 * @author Luiz Fernando
	 */
	private void montaFinalizador(StringBuilder finalizador, DominiointegracaoFiltro filtro) {
		for(int i = 0; i < 100; i++)
			finalizador.append("9"); 	//VALOR 9 - T100 N
	}

	/**
	 * Monta o cabe�alho do arquivo
	 *
	 * @param cabecalho
	 * @param filtro
	 * @author Luiz Fernando
	 */
	private void montaCabecalho(StringBuilder cabecalhoarquivo, DominiointegracaoFiltro filtro, Date dtinicio, Date dtfim, DominioTipointegracao tipo) {
		// T -> TAMANHO DO CAMPO
		Dominiointegracaocabecalho cabecalho = new Dominiointegracaocabecalho();
		
//		VALOR FIXO "01"	- T02
		cabecalho.setCabecalho(Dominiointegracaocabecalho.CABECALHO);		
//		C�DIGO DA EMPRESA - T07
		cabecalho.setCodigoempresa(filtro.getCodigoempresa()); 
//		CGC DA EMPRESA - T14
		cabecalho.setCgcempresa(filtro.getEmpresa() != null ? filtro.getEmpresa().getCpfOuCnpj():null);		
//		DATA INICIAL - T10
		cabecalho.setDatainicial(dtinicio);
//		DATA FINAL - T10
		cabecalho.setDatafinal(dtfim);
//		VALOR FIXO "N" - T01
		cabecalho.setValorfixoN(Dominiointegracaocabecalho.VALORFIXON);
//		TIPO DE NOTA - T02
		cabecalho.setTiponota(tipo.getValue());	
//		VALOR FIXO - CONSTANTTE - T05
		cabecalho.setConstante(Dominiointegracaocabecalho.CONSTANTE);					
//		SISTEMA - T01  (1=CONTABILIDADE, 2=CAIXA, 0=OUTRO)
		cabecalho.setSistema(0);						
//		VALOR FIXO "18" ou "17" - T02
		if(!tipo.equals(DominioTipointegracao.LANCAMENTO_CONTABIL))
			cabecalho.setValorfixo(Dominiointegracaocabecalho.VALORFIXO18);
		else
			cabecalho.setValorfixo(Dominiointegracaocabecalho.VALORFIXO17);
		
		cabecalhoarquivo.append(cabecalho.toString());
	}
	
	/**
	 * M�todo para preencher string com zeros a esquerda
	 *
	 * @param campo
	 * @param qtdePosicoes
	 * @return
	 * @author Luiz Fernando
	 */
	private String printNumero(String campo, Integer qtdePosicoes){
		StringBuilder novoCampo = new StringBuilder();
		
		if(qtdePosicoes != null && campo != null){
			if(campo.length() > qtdePosicoes){
				return campo.substring(0, qtdePosicoes );
			}
			Integer qtde = qtdePosicoes - campo.length();
			for(int i = 0; i < qtde; i++)
				novoCampo.append("0");
			novoCampo.append(campo);
		}else if(qtdePosicoes != null){
			for(int i = 0; i < qtdePosicoes; i++)
				novoCampo.append("0");
		}
		
		return novoCampo.toString();
	}
	
	/**
	 * M�todo para preencher string com espa�os a direita
	 *
	 * @param campo
	 * @param qtdePosicoes
	 * @return
	 * @author Luiz Fernando
	 */
	private String printString(String campo, Integer qtdePosicoes){
		StringBuilder novoCampo = new StringBuilder();
		if(qtdePosicoes != null && campo != null){
			if(campo.length() > qtdePosicoes){
				return campo.substring(0, qtdePosicoes );
			}
			novoCampo.append(campo);
			Integer qtde = qtdePosicoes - campo.length();
			for(int i = 0; i < qtde; i++)
				novoCampo.append(" ");
			
		}else if(qtdePosicoes != null){
			for(int i = 0; i < qtdePosicoes; i++)
				novoCampo.append(" ");
		}
		
		return novoCampo.toString();
	}
	
	/**
	 * Formata valor Decimal de acordo com a quantidade de casas decimais
	 *
	 * @param valor
	 * @param qtdeCasadecimal
	 * @return
	 * @author Luiz Fernando
	 */
	private String formatarDecimal(String valor, Integer qtdeCasadecimal){
		StringBuilder campo = new StringBuilder();
		if(valor != null && qtdeCasadecimal != null){
			String str[] = valor.split(",");
			if(str != null){
				campo.append(str[0] != null ? str[0]:"").append(str.length > 1 && str[1] != null ? str[1] : "");
				if(str.length > 1 && str[1].length() < qtdeCasadecimal){
					for(int i = 0; i < (qtdeCasadecimal - str[1].length()); i++){
						campo.append("0");
					}
				}
			}			
		}
		
		return campo.toString().replace(".", "").replace(" ", "");
	}
	
	public ModelAndView getCodigoEmpresaAjax(WebRequestContext request) {
		JsonModelAndView json = new JsonModelAndView();
		String cdempresa = request.getParameter("cdempresa");
		
		if(StringUtils.isNotBlank(cdempresa)) {
			Empresa empresa = empresaService.load(new Empresa(Integer.parseInt(cdempresa)), 
					"empresa.cdpessoa, empresa.codigoEmpresaDominioIntegracao, empresa.principal");
			
			if(empresa != null) {
				if(empresa.getPrincipal() != null && empresa.getPrincipal()) {
					json.addObject("codigoempresa", "");
				} else {
					json.addObject("codigoempresa", empresa.getCodigoEmpresaDominioIntegracao() != null ? empresa.getCodigoEmpresaDominioIntegracao() : "");
				}
			}
		}
		
		return json;
	}
}
