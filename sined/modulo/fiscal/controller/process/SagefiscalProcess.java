package br.com.linkcom.sined.modulo.fiscal.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sagefiscal.registros.RegistroE001;
import br.com.linkcom.sagefiscal.registros.RegistroE010;
import br.com.linkcom.sagefiscal.registros.RegistroE017;
import br.com.linkcom.sagefiscal.registros.RegistroE018;
import br.com.linkcom.sagefiscal.registros.RegistroE020;
import br.com.linkcom.sagefiscal.registros.RegistroE021;
import br.com.linkcom.sagefiscal.registros.RegistroE022;
import br.com.linkcom.sagefiscal.registros.RegistroE027;
import br.com.linkcom.sagefiscal.registros.RegistroE200;
import br.com.linkcom.sagefiscal.registros.RegistroE201;
import br.com.linkcom.sagefiscal.registros.RegistroE207;
import br.com.linkcom.sagefiscal.registros.RegistroE209;
import br.com.linkcom.sagefiscal.registros.RegistroE210;
import br.com.linkcom.sagefiscal.registros.RegistroE216;
import br.com.linkcom.sagefiscal.registros.RegistroE221;
import br.com.linkcom.sagefiscal.registros.RegistroE222;
import br.com.linkcom.sagefiscal.registros.RegistroE233;
import br.com.linkcom.sagefiscal.registros.RegistroE285;
import br.com.linkcom.sagefiscal.registros.RegistroE330;
import br.com.linkcom.sagefiscal.registros.RegistroE332;
import br.com.linkcom.sagefiscal.registros.RegistroE333;
import br.com.linkcom.sagefiscal.registros.RegistroE336;
import br.com.linkcom.sagefiscal.registros.RegistroE342;
import br.com.linkcom.sagefiscal.registros.RegistroE400;
import br.com.linkcom.sagefiscal.registros.RegistroE600_002;
import br.com.linkcom.sagefiscal.registros.RegistroE602;
import br.com.linkcom.sagefiscal.registros.RegistroE603;
import br.com.linkcom.sagefiscal.sage.Sagefiscal;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.geral.bean.Entregadocumentoreferenciado;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoreferenciada;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.ResponsavelFrete;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Codigoregimetributario;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadortipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.ModelodocumentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.bean.enumeration.Tiponotareferencia;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.InutilizacaonfeService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.PaisService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.SageClienteEndereco;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.SagefiscalApoio;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.utils.SagefiscalUtil;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(path="/fiscal/process/Sagefiscal", authorizationModule=ProcessAuthorizationModule.class)
public class SagefiscalProcess extends ResourceSenderController<SagefiscalFiltro>{
	
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private EntradafiscalService entradafiscalService;
	private ArquivonfnotaService arquivonfnotaService;
	private EmpresaService empresaService;
	private ClienteService clienteService;
	private FornecedorService fornecedorService;
	private MaterialService materialService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private UnidademedidaService unidademedidaService;
	private MaterialgrupoService materialgrupoService;
	private InventarioService inventarioService;
	private ParametrogeralService parametrogeralService;
	private PaisService paisService;
//	private NotaHistoricoService notaHistoricoService;
	private InutilizacaonfeService inutilizacaonfeService;
	private EnderecoService enderecoService;
	
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setInutilizacaonfeService(
			InutilizacaonfeService inutilizacaonfeService) {
		this.inutilizacaonfeService = inutilizacaonfeService;
	}
//	public void setNotaHistoricoService(
//			NotaHistoricoService notaHistoricoService) {
//		this.notaHistoricoService = notaHistoricoService;
//	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {
		this.naturezaoperacaoService = naturezaoperacaoService;
	}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPaisService(PaisService paisService) {
		this.paisService = paisService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	SagefiscalFiltro filtro) throws Exception {
		return gerarArquivoSAGE(request, filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, SagefiscalFiltro filtro) throws Exception {
		return new ModelAndView("process/sagefiscal").addObject("filtro", filtro);
	}
	
	public void ajaxGetListaInventario(WebRequestContext request, Empresa empresa){
		View.getCurrent().println(SinedUtil.convertToJavaScript(empresa.getCdpessoa() != null ? inventarioService.findByEmpresa(empresa) : new ArrayList<Inventario>(), "listaInventario", ""));
	}
	
	/**
	 * Gera o arquivo SAGE de acordo com o filtro
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public Resource gerarArquivoSAGE(WebRequestContext request, SagefiscalFiltro filtro) throws Exception {
		if(filtro.getEmpresa() == null){
			throw new SinedException("O campo Empresa � obrigat�rio.");
		}
		if(filtro.getDtinicio() == null || filtro.getDtfim() == null){
			throw new SinedException("Per�odo inv�lido.");
		}
		
		filtro.setEmpresa(empresaService.loadForSageFiscal(filtro.getEmpresa()));
		
		boolean entrada = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		boolean saida = filtro.getBuscarsaidas() != null && filtro.getBuscarsaidas();
		
		List<Notafiscalproduto> listaNotafiscalproduto = notafiscalprodutoService.findForSagefiscal(filtro);
		List<NotaFiscalServico> listaNotaFiscalServico = saida ? notaFiscalServicoService.findForSagefiscal(filtro) : null;
		List<Entregadocumento> listaEntregadocumento = entrada ? entradafiscalService.findForSagefiscal(filtro) : null;
		List<Inutilizacaonfe> listaInutilizacaonfe = saida ? inutilizacaonfeService.findForSageFiscal(filtro) : null;
		
//		List<Notafiscalproduto> listaNotafiscalproduto = null;
//		List<NotaFiscalServico> listaNotaFiscalServico = null;
//		List<Entregadocumento> listaEntregadocumento = null;
//		List<Inutilizacaonfe> listaInutilizacaonfe = null;
		
		Sagefiscal sagefiscal = new Sagefiscal();
		filtro.setApoio(new SagefiscalApoio());
		sagefiscal.setRegistroE001(this.createRegistroE001(filtro));
		sagefiscal.setListaRegistroE200(this.createRegistroE200(filtro, listaNotafiscalproduto, listaEntregadocumento, listaInutilizacaonfe));
		sagefiscal.setListaRegistroE285(this.createRegistroE285(filtro, listaEntregadocumento));
		sagefiscal.setListaRegistroE400(this.createRegistroE400(filtro));
		sagefiscal.setListaRegistroE600_002(this.createRegistroE600_002(filtro, listaNotaFiscalServico));
		
		sagefiscal.setListaRegistroE010(this.createRegistroE010(filtro, filtro.getApoio().getListaCliente(), filtro.getApoio().getListaFornecedor()));
		sagefiscal.setListaRegistroE017(this.createRegistroE017(filtro.getApoio().getListaMaterialgrupo()));
		sagefiscal.setListaRegistroE020(this.createRegistroE020(filtro, filtro.getApoio().getListaProdutoServico()));
		sagefiscal.setListaRegistroE022(this.createRegistroE022(filtro, filtro.getApoio()));
		sagefiscal.setListaRegistroE018(this.createRegistroE018(filtro.getApoio().getListaUnidadeComercial()));
		sagefiscal.setListaRegistroE027(this.createRegistroE027(filtro.getApoio().getListaNaturezaOperacao()));

		String entradaSaida = "";
		if(entrada && !saida){
			entradaSaida = "_ENTRADA"; 
		} else if(!entrada && saida){
			entradaSaida = "_SAIDA";
		}
		
		Resource resource = new Resource("text/plain", filtro.getEmpresa().getNome().replaceAll(" ", "_") + "_" +
										 new SimpleDateFormat("yyyy_MM").format(filtro.getDtinicio()) + 
										 entradaSaida +
										 ".fml", sagefiscal.toString().getBytes());
		return resource;
	}
	
	private List<RegistroE285> createRegistroE285(SagefiscalFiltro filtro, List<Entregadocumento> listaEntregadocumento) {
		List<RegistroE285> lista = new ArrayList<RegistroE285>();
		
		List<Naturezabasecalculocredito> listaNaturezabccredito = new ArrayList<Naturezabasecalculocredito>();
		listaNaturezabccredito.add(Naturezabasecalculocredito.ALUGUEIS_PREDIOS);
		listaNaturezabccredito.add(Naturezabasecalculocredito.ALUGUEIS_MAQUINAS_EQUIPAMENTOS);
		listaNaturezabccredito.add(Naturezabasecalculocredito.ARMAZENAGEM_MERCADORIA_FRETE_OP_VENDA);
		listaNaturezabccredito.add(Naturezabasecalculocredito.CONTRAPRESTACAO);
		
		if(SinedUtil.isListNotEmpty(listaEntregadocumento)){
			for(Entregadocumento ed : listaEntregadocumento){
				Modelodocumentofiscal modelodocumentofiscal = ed.getModelodocumentofiscal();
				if(modelodocumentofiscal == null || 
						modelodocumentofiscal.getSpedpiscofins_f100() == null || 
						!modelodocumentofiscal.getSpedpiscofins_f100()){
					continue;
				}
				
				Set<Entregamaterial> listaEntregamaterial = ed.getListaEntregamaterial();
				if(SinedUtil.isListNotEmpty(listaEntregamaterial)){
					for (Entregamaterial item : listaEntregamaterial) {
						if(item.getNaturezabcc() != null && listaNaturezabccredito.contains(item.getNaturezabcc())){
							boolean pisCredito = item.getCstpis() != null && item.getCstpis().getValue() >= 10 && item.getCstpis().getValue() <= 23;
							boolean cofinsCredito = item.getCstcofins() != null && item.getCstcofins().getValue() >= 10 && item.getCstcofins().getValue() <= 23;
							
							if(pisCredito || cofinsCredito){
								RegistroE285 registroE285 = new RegistroE285();
								registroE285.setNome_registro(RegistroE285.NOME_REGISTRO);
								registroE285.setData_operacao(ed.getDtentrada() != null ? ed.getDtentrada() : ed.getDtemissao());
								registroE285.setCodigo_fornecedor(parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? filtro.getApoio().addFornecedor(ed.getFornecedor().getCdpessoa(), null) : filtro.getApoio().addFornecedor(ed.getFornecedor().getCdpessoa(), ed.getFornecedor().getCpfOuCnpjValue()));
								registroE285.setCodigo_produto_servico(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), item.getCstpis(), item.getCstcofins()) : null);
								registroE285.setValor_operacao(item.getValortotalmaterial());
								registroE285.setValor_bc_pis(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : 0d);
								registroE285.setAliquota_pis(item.getPis() != null ? item.getPis() : 0d);
								registroE285.setValor_pis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : 0d);
								registroE285.setSit_trib_pis(item.getCstpis() != null ? item.getCstpis().getCdnfe() : null);
								registroE285.setValor_bc_cofins(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : 0);
								registroE285.setAliquota_cofins(item.getCofins() != null ? item.getCofins() : 0d);
								registroE285.setValor_cofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : 0d);
								registroE285.setSit_trib_cofins(item.getCstcofins() != null ? item.getCstcofins().getCdnfe() : null);
								registroE285.setDescricao_complementar(null);
								registroE285.setConta_analitica_contabil(null);
								registroE285.setCodigo_bc_credito(item.getNaturezabcc() != null ? item.getNaturezabcc().getCdsped()  : null);
								registroE285.setControle_sistema(0);
								
								lista.add(registroE285);
							}
						}
					}
				}
			}
		}
		return lista;
	}
	
	private RegistroE001 createRegistroE001(SagefiscalFiltro filtro) {
		RegistroE001 registroE001 = new RegistroE001();
		registroE001.setNome_registro(RegistroE001.NOME_REGISTRO);
		registroE001.setNumero_empresa(filtro.getEmpresa().getIntegracaocontabil() != null ? Integer.parseInt(filtro.getEmpresa().getIntegracaocontabil()) : null);
		registroE001.setVersao_layout(Sagefiscal.VER_2_0_06);
		registroE001.setControle_sistema(0);
		
		return registroE001;
	}
	
	private List<RegistroE010> createRegistroE010(SagefiscalFiltro filtro, List<Integer> listaCdCliente, List<Integer> listaCdFornecedor) {
		List<RegistroE010> lista = new ArrayList<RegistroE010>();
		List<Cliente> listaCliente = SinedUtil.isListNotEmpty(listaCdCliente) ? clienteService.findForSAGE(CollectionsUtil.concatenate(listaCdCliente, ",")) : null;
		List<Fornecedor> listaFornecedor = SinedUtil.isListNotEmpty(listaCdFornecedor) ? fornecedorService.findForSAGE(CollectionsUtil.concatenate(listaCdFornecedor, ",")) : null;
		
		Pais paisBrasil = paisService.load(Pais.BRASIL);
		
		if(SinedUtil.isListNotEmpty(listaCliente)){
			for(Cliente cliente : listaCliente){
				Endereco endereco = this.getEnderecoCliente(cliente);
				RegistroE010 registroE010 = getRegistroE010Cliente(filtro, paisBrasil, cliente, endereco, null);
				lista.add(registroE010);
				
				List<SageClienteEndereco> listaClienteEndereco = filtro.getApoio().getListaClienteEndereco();
				for (SageClienteEndereco sageClienteEndereco : listaClienteEndereco) {
					if(sageClienteEndereco.getCodigoCliente().equals(cliente.getCdpessoa())){
						Endereco enderecoClienteCadastro = enderecoService.loadEndereco(new Endereco(sageClienteEndereco.getCodigoEndereco()));
						RegistroE010 registroE010_Endereco = getRegistroE010Cliente(filtro, paisBrasil, cliente, enderecoClienteCadastro, sageClienteEndereco.getDtalteracao());
						lista.add(registroE010_Endereco);
					}
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaFornecedor)){
			for(Fornecedor fornecedor : listaFornecedor){
				RegistroE010 registroE010 = new RegistroE010();
				registroE010.setNome_registro(RegistroE010.NOME_REGISTRO);
				registroE010.setCodigo_cliente_fornecedor(parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? fornecedor.getCdpessoa().toString() : fornecedor.getCpfOuCnpjValue());
				registroE010.setNome(fornecedor.getRazaosocial() != null && !fornecedor.getRazaosocial().equals("") ? fornecedor.getRazaosocial() : fornecedor.getNome());
				registroE010.setData_inclusao(getDataInclusao(fornecedor));
				
				Endereco endereco = fornecedor.getEndereco();
				if(endereco != null){
					registroE010.setTipo_logradouro("R");
					registroE010.setLogradouro(endereco.getLogradouro());
					registroE010.setNumero_logradouro(endereco.getNumero());
					registroE010.setComplemento_logradouro(endereco.getComplemento());
					registroE010.setBairro(endereco.getBairro());
					if(endereco.getMunicipio() != null){
						registroE010.setEstado(endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : null);
						registroE010.setCidade(endereco.getMunicipio().getCdibge() != null ? Integer.parseInt(endereco.getMunicipio().getCdibge()) : null);
					}
					
					
					registroE010.setCep(endereco.getCep() != null ? Integer.parseInt(endereco.getCep().getValue()) : null);
					registroE010.setPais(Integer.parseInt((endereco.getPais() != null && endereco.getPais().getCdbacen() != null ? endereco.getPais().getCdbacen() : paisBrasil.getCdbacen())));
				}
				registroE010.setCnpjcpf(fornecedor.getCpfOuCnpjValue());
				registroE010.setInscricao_estadual(fornecedor.getInscricaoestadual());
				registroE010.setInscricao_municipal(fornecedor.getInscricaomunicipal() != null ? fornecedor.getInscricaomunicipal().replaceAll(SinedUtil.REGEX_APENAS_NUMEROS_SEFIP, "") : null);
				registroE010.setInscricao_suframa(null);
				registroE010.setContato(fornecedor.getContatoEspecifico());
				registroE010.setTelefone(fornecedor.getTelefoneprincipal());
				registroE010.setFax(fornecedor.getFax());
//				registroE010.setData_alteracao_cadastro(fornecedor.getDtaltera() != null ? new Date(fornecedor.getDtaltera().getTime()) : null);
				registroE010.setCliente("S");
				registroE010.setFornecedor("S");
				registroE010.setProdutor_rural("N");
				registroE010.setFornecedor_substituto_tributario("N");
				registroE010.setSimples_nacional("N");
				registroE010.setInscrito_municipio(getInscritoMunicipio(fornecedor.getInscricaomunicipal(), endereco, filtro.getEmpresa().getEndereco()));
				registroE010.setEmail_contato(fornecedor.getEmailContatoEspecifico());
				registroE010.setEndereco_website("N");
				registroE010.setHospedagem_sites_banco_dados("N");
				registroE010.setEndereco_ip_site_clientefornecedor(null);
				registroE010.setEndereco_website_clientefornecedor(null);
				registroE010.setData_inicio_contrato_hospedagem(null);
				registroE010.setData_final_contrato_hospedagem(null);
				registroE010.setGateway_pagamentos("N");
				registroE010.setData_inicio_contrato_gateway_pagamentos(null);
				registroE010.setData_final_contrato_gateway_pagamentos(null);
				registroE010.setProvimentos_solucoes_lojas_virtuais("N");
				registroE010.setEndereco_ip_lojas_virtuais(null);
				registroE010.setEndereco_website_lojas_virtuais(null);
				registroE010.setData_inicio_contrato_lojas_virtuais(null);
				registroE010.setData_final_contrato_lojas_virtuais(null);
				registroE010.setControle_sistema(0);
				
				lista.add(registroE010);
			}
		}
		
		return lista;
	}
	private RegistroE010 getRegistroE010Cliente(SagefiscalFiltro filtro, Pais paisBrasil, Cliente cliente, Endereco endereco, Date dtalteracao) {
		RegistroE010 registroE010 = new RegistroE010();
		registroE010.setNome_registro(RegistroE010.NOME_REGISTRO);
		registroE010.setCodigo_cliente_fornecedor(parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? cliente.getCdpessoa().toString() : cliente.getCpfOuCnpjValue());
		registroE010.setNome(cliente.getRazaosocial() != null && !cliente.getRazaosocial().equals("") ? cliente.getRazaosocial() : cliente.getNome());
		registroE010.setData_inclusao(getDataInclusao(cliente));
		
		if(endereco != null){
			registroE010.setTipo_logradouro("R");
			registroE010.setLogradouro(endereco.getLogradouro());
			registroE010.setNumero_logradouro(endereco.getNumero());
			registroE010.setComplemento_logradouro(endereco.getComplemento());
			registroE010.setBairro(endereco.getBairro());
			if(endereco.getMunicipio() != null){
				registroE010.setEstado(endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : null);
				registroE010.setCidade(endereco.getMunicipio().getCdibge() != null ? Integer.parseInt(endereco.getMunicipio().getCdibge()) : null);
			}
			registroE010.setCep(endereco.getCep() != null ? Integer.parseInt(endereco.getCep().getValue()) : null);
			registroE010.setPais(Integer.parseInt(endereco.getPais() != null && endereco.getPais().getCdbacen() != null ? endereco.getPais().getCdbacen() : paisBrasil.getCdbacen()));
		}
		
		String inscricaoestadual = cliente.getInscricaoestadual();
		if(StringUtils.isNotBlank(endereco.getInscricaoestadual())){
			inscricaoestadual = endereco.getInscricaoestadual();
		}
		
		registroE010.setCnpjcpf(cliente.getCpfOuCnpjValue());
		registroE010.setInscricao_estadual(inscricaoestadual);
		registroE010.setInscricao_municipal(cliente.getInscricaomunicipal() != null ? cliente.getInscricaomunicipal().replaceAll(SinedUtil.REGEX_APENAS_NUMEROS_SEFIP, "") : null);
		registroE010.setInscricao_suframa(null);
		registroE010.setContato(cliente.getContatoEspecifico());
		registroE010.setTelefone(cliente.getTelefoneprincipal());
		registroE010.setFax(cliente.getFax());
		registroE010.setData_alteracao_cadastro(dtalteracao);
		registroE010.setCliente("S");
		registroE010.setFornecedor("S");
		registroE010.setProdutor_rural("N");
		registroE010.setFornecedor_substituto_tributario("N");
		registroE010.setSimples_nacional(Codigoregimetributario.SIMPLES_NACIONAL.equals(cliente.getCrt()) || Codigoregimetributario.SIMPLES_NACIONAL_EXCESSO.equals(cliente.getCrt()) ? "S" : "N");
		registroE010.setInscrito_municipio(getInscritoMunicipio(cliente.getInscricaomunicipal(), endereco, filtro.getEmpresa().getEndereco()));
		registroE010.setEmail_contato(cliente.getEmailContatoEspecifico());
		registroE010.setEndereco_website("N");
		registroE010.setHospedagem_sites_banco_dados("N");
		registroE010.setEndereco_ip_site_clientefornecedor(null);
		registroE010.setEndereco_website_clientefornecedor(null);
		registroE010.setData_inicio_contrato_hospedagem(null);
		registroE010.setData_final_contrato_hospedagem(null);
		registroE010.setGateway_pagamentos("N");
		registroE010.setData_inicio_contrato_gateway_pagamentos(null);
		registroE010.setData_final_contrato_gateway_pagamentos(null);
		registroE010.setProvimentos_solucoes_lojas_virtuais("N");
		registroE010.setEndereco_ip_lojas_virtuais(null);
		registroE010.setEndereco_website_lojas_virtuais(null);
		registroE010.setData_inicio_contrato_lojas_virtuais(null);
		registroE010.setData_final_contrato_lojas_virtuais(null);
		registroE010.setControle_sistema(0);
		return registroE010;
	}
	
	private Endereco getEnderecoCliente(Cliente cliente) {
		return cliente.getEndereco();
	}
	
	private String getInscritoMunicipio(String inscricaomunicipal, Endereco enderecoClienteFornecedor, Endereco enderecoEmpresa) {
		return StringUtils.isNotBlank(inscricaomunicipal) && 
			enderecoClienteFornecedor != null && enderecoEmpresa != null &&
			enderecoClienteFornecedor.getMunicipio() != null && enderecoEmpresa.getMunicipio() != null &&
			enderecoClienteFornecedor.getMunicipio().equals(enderecoEmpresa.getMunicipio()) ? "S" : "N";
	}
	
	private List<RegistroE017> createRegistroE017(List<Integer> listaCdMaterialgrupo) {
		List<RegistroE017> listaRegistroE017 = new ArrayList<RegistroE017>();
		List<Materialgrupo> listaMaterialgrupo = SinedUtil.isListNotEmpty(listaCdMaterialgrupo) ? materialgrupoService.findForSagefiscal(CollectionsUtil.concatenate(listaCdMaterialgrupo, ",")) : null;
		
		if(SinedUtil.isListNotEmpty(listaRegistroE017)){
			RegistroE017 registroE017 = null;
			for(Materialgrupo materialgrupo : listaMaterialgrupo){
				registroE017 = new RegistroE017();
				registroE017.setNome_registro(RegistroE017.NOME_REGISTRO);
				registroE017.setCodigo_tipo_inventario(materialgrupo.getCdmaterialgrupo());
				registroE017.setDescricao_tipo_inventario(materialgrupo.getNome());
				registroE017.setControle_sistema(0);
				
				listaRegistroE017.add(registroE017);
			}
		}
		return listaRegistroE017;
	}
	
	private List<RegistroE018> createRegistroE018(List<String> listaSimboloUnidademedida) {
		List<RegistroE018> listaRegistroE018 = new ArrayList<RegistroE018>();
		List<Unidademedida> listaUnidademedida = SinedUtil.isListNotEmpty(listaSimboloUnidademedida) ? unidademedidaService.findForSagefiscal(listaSimboloUnidademedida) : null;
		if(SinedUtil.isListNotEmpty(listaUnidademedida)){
			RegistroE018 registroE018;
			for(Unidademedida unidademedida : listaUnidademedida){
				registroE018 = new RegistroE018();
				registroE018.setNome_registro(RegistroE018.NOME_REGISTRO);
				registroE018.setCodigo_unidademedida(unidademedida.getSimbolo());
				registroE018.setDescricao_codigo_unidademedida(unidademedida.getNome());
				registroE018.setControle_sistema(0);
				
				listaRegistroE018.add(registroE018);
			}
		}
		return listaRegistroE018;
	}
	
	private List<RegistroE020> createRegistroE020(SagefiscalFiltro filtro, List<Integer> listaCdmaterial) {
		List<RegistroE020> listaRegistroE020 = new ArrayList<RegistroE020>();
		List<Material> listaMaterial = SinedUtil.isListNotEmpty(listaCdmaterial) ? materialService.findForSagefiscal(CollectionsUtil.concatenate(listaCdmaterial, ",")) : null;
		if(SinedUtil.isListNotEmpty(listaMaterial)){
			RegistroE020 registroE020;
			for(Material material : listaMaterial){
				registroE020 = new RegistroE020();
				registroE020.setNome_registro(RegistroE020.NOME_REGISTRO);
				registroE020.setCodigo_produtoservico(material.getCdmaterial().toString());
				registroE020.setDescricao_produtoservico(material.getNome());
				registroE020.setData_inclusao(getDataInclusao(material));
//				registroE020.setData_alteracao_descricao(getDataInclusao(material));
				registroE020.setPeriodo_inicial_utilizacao(getDataInclusao(material));
//				registroE020.setPeriodo_final_utilizacao(getDataInclusao(material));
				registroE020.setCodigo_anterior(null);
				registroE020.setGenero_item(material.getNcmcapitulo() != null ? material.getNcmcapitulo().getCodeFormat() : null);
				String tipoProduto = material.getTipoitemsped() != null ? material.getTipoitemsped().getValue() : null;
				registroE020.setTipo_produto(tipoProduto);
				registroE020.setTipo_inventario(material.getMaterialgrupo() != null ? material.getMaterialgrupo().getCdmaterialgrupo() : null);
				if(material.getNcmcompleto() != null && !material.getNcmcompleto().equals("99999999") && (material.getTipoitemsped() == null || !material.getTipoitemsped().equals(Tipoitemsped.SERVICOS))){
					registroE020.setNbm_sh(SagefiscalUtil.formatNcm(material.getNcmcompleto()));
				}
				registroE020.setCodigo_barra(material.getCodigobarras());
				registroE020.setCombustivel_solvente("N");
				registroE020.setCodigo_sefaz(null);
				registroE020.setCodigo_anp(material.getCodigoanp() != null ? material.getCodigoanp().getCodigo() : null);
				registroE020.setCod_unidade_comercial(material.getUnidademedida() != null ? filtro.getApoio().addUnidadeComercial(material.getUnidademedida().getSimbolo()) : null);
				registroE020.setCod_unidade_estoque(material.getUnidademedida() != null ? filtro.getApoio().addUnidadeComercial(material.getUnidademedida().getSimbolo()) : null);
				registroE020.setFator_conversao(null);
				registroE020.setCodigo_produto_dnf(null);
				registroE020.setFator_conversao_unid_med_est_dnf(null);
				registroE020.setCapacidade_volumetrica_dnf(null);
				registroE020.setAliq_ipi(null);
				registroE020.setSit_trib_ipi(null);
				registroE020.setServicos_lc(material.getCodlistaservico());
				registroE020.setConta_analitica_contabil(null);
				registroE020.setAliq_icms(null);
				registroE020.setAdicional_aliq_icms(null);
				registroE020.setAdicional_aliq_icms_rj("N");
				registroE020.setPerc_red_base_calc_icms(null);
				registroE020.setValor_unit_base_icms_st(null);
				registroE020.setSit_trib_icms_tab_a(null);
				registroE020.setSit_trib_icms_tab_b(null);
				
				Tributacaoecf tributacaoecf = material.getTributacaoecf();
				registroE020.setSit_trib_pis(tributacaoecf != null && tributacaoecf.getCstpis() != null ? tributacaoecf.getCstpis().getCdnfe() : null);
				registroE020.setSit_trib_cofins(tributacaoecf != null && tributacaoecf.getCstcofins() != null ? tributacaoecf.getCstcofins().getCdnfe() : null);
				
				registroE020.setTipo_incidencia("");
				registroE020.setTabela_babidas_frias(null);
				registroE020.setCodigo_grupo(null);
				registroE020.setMarca_comercial(null);
				registroE020.setCodigo_sefaz_mg(null);
				registroE020.setControle_sistema(0);
				
				if(tipoProduto != null && (tipoProduto.equals(Tipoitemsped.PRODUTO_PROCESSO) || tipoProduto.equals(Tipoitemsped.PRODUTO_ACABADO))){
					registroE020.setListaRegistroE021(createRegistroE021(registroE020, material.getListaProducao()));
				}
				
				listaRegistroE020.add(registroE020);
			}
		}
		return listaRegistroE020;
	}
	
	private List<RegistroE021> createRegistroE021(RegistroE020 registroE020, Set<Materialproducao> listaMaterialproducao) {
		List<RegistroE021> listaRegistroE021 = new ArrayList<RegistroE021>();
		if(SinedUtil.isListNotEmpty(listaMaterialproducao)){
			RegistroE021 registroE021;
			for(Materialproducao materialproducao : listaMaterialproducao){
				registroE021 = new RegistroE021();
				registroE021.setNome_registro(RegistroE021.NOME_REGISTRO);
				registroE021.setCodigo_produto(registroE020.getCodigo_produtoservico());
				registroE021.setPeriodo_inicial(getDataInclusao(materialproducao.getMaterial()));
				registroE021.setPeriodo_final(null);
				registroE021.setComponente_insumo(materialproducao.getMaterial().getCdmaterial().toString());
				registroE021.setQuantidade(materialproducao.getConsumo());
				registroE021.setPerda_quebra(null);
				
				listaRegistroE021.add(registroE021);
			}
		}
		return listaRegistroE021;
	}
	
	private List<RegistroE022> createRegistroE022(SagefiscalFiltro filtro, SagefiscalApoio apoio) {
		List<Integer> listaCdmaterial = apoio.getListaServico();
		
		List<RegistroE022> listaRegistroE022 = new ArrayList<RegistroE022>();
		List<Material> listaMaterial = SinedUtil.isListNotEmpty(listaCdmaterial) ? materialService.findForSagefiscal(CollectionsUtil.concatenate(listaCdmaterial, ",")) : null;
		if(SinedUtil.isListNotEmpty(listaMaterial)){
			RegistroE022 registroE022;
			Tipocobrancapis tipocobrancapis;
			Tipocobrancacofins tipocobrancacofins;
			
			for(Material material : listaMaterial){
				tipocobrancapis = apoio.getMapMaterialTipocobrancapis().get(material.getCdmaterial());
				tipocobrancacofins = apoio.getMapMaterialTipocobrancacofins().get(material.getCdmaterial());
				
				registroE022 = new RegistroE022();
				registroE022.setNome_registro(RegistroE022.NOME_REGISTRO);
				registroE022.setCodigo_servico(material.getCdmaterial());
				registroE022.setDescricao(material.getNome());
				registroE022.setData_inclusao_alteracao(new Date(System.currentTimeMillis()));
				registroE022.setServicos_lc(material.getCodlistaservico());
				registroE022.setCodigo_unidade_estoque(null);
				registroE022.setConta_analitica_contabil(null);
				registroE022.setSit_trib_pis(tipocobrancapis != null ? Integer.parseInt(tipocobrancapis.getCdnfe()) : null);
				registroE022.setSit_trib_cofins(tipocobrancacofins != null ? Integer.parseInt(tipocobrancacofins.getCdnfe()) : null);
				registroE022.setTipo_incidencia("");
				registroE022.setControle_sistema(0);
				
				listaRegistroE022.add(registroE022);
			}
		}
		return listaRegistroE022;
	}
	
	private List<RegistroE027> createRegistroE027(List<Integer> listaCdNaturezaoperacao) {
		List<RegistroE027> listaRegistroE027 = new ArrayList<RegistroE027>();
		RegistroE027 registroE027;
		List<Naturezaoperacao> listaMaterial = SinedUtil.isListNotEmpty(listaCdNaturezaoperacao) ? naturezaoperacaoService.findForSagefiscal(CollectionsUtil.concatenate(listaCdNaturezaoperacao, ",")) : null;
		if(SinedUtil.isListNotEmpty(listaMaterial)){
			for(Naturezaoperacao naturezaoperacao : listaMaterial){
				registroE027 = new RegistroE027();
				registroE027.setNome_registro(RegistroE027.NOME_REGISTRO);
				registroE027.setCodigo_natureza_operacao(naturezaoperacao.getCdnaturezaoperacao().toString());
				registroE027.setDescricao_codigo_natureza_operacao(naturezaoperacao.getDescricao());
				registroE027.setData_inclusao_alteracao(naturezaoperacao.getDtaltera() != null ? new Date(naturezaoperacao.getDtaltera().getTime()) : null);
				registroE027.setControle_sistema(0);
				listaRegistroE027.add(registroE027);
			}
		}
		
		return listaRegistroE027;
	}
	
	private List<RegistroE200> createRegistroE200(SagefiscalFiltro filtro, List<Notafiscalproduto> listaNotafiscalproduto, List<Entregadocumento> listaEntregadocumento, List<Inutilizacaonfe> listaInutilizacaonfe) {
		List<RegistroE200> listaRegistroE200 = new ArrayList<RegistroE200>();
		RegistroE200 registroE200;
		String numeroNf, serie; 
		Arquivonfnota arquivonfnota;
		Integer tipo_frete;
		
		if(SinedUtil.isListNotEmpty(listaInutilizacaonfe)){
			for (Inutilizacaonfe inut : listaInutilizacaonfe) {
				Integer numero = inut.getNuminicio();
				Integer numfim = inut.getNumfim();
				
				while(numero <= numfim){
					Notafiscalproduto nfp = notafiscalprodutoService.findNotaByNumeroEmpresa(filtro.getEmpresa(), numero + "", true);
					if(nfp == null){
						numero++;
						continue;
					}
					
					nfp = notafiscalprodutoService.loadForSagefiscal(nfp.getCdNota());
					if(nfp == null){
						numero++;
						continue;
					}
					nfp.setNotaStatus(NotaStatus.CANCELADA);
					nfp.setTipooperacaonota(Tipooperacaonota.SAIDA);
					
					registroE200 = new RegistroE200();
					registroE200.setNome_registro(RegistroE200.NOME_REGISTRO);
					registroE200.setEntradas_saidas("S");
					registroE200.setEspecie_nf("NFe");
					
					numeroNf = nfp.getNumero();
					serie = nfp.getSerienfe() != null ? nfp.getSerienfe() + "" : "1"; 
					
					Endereco enderecoCliente = null;
					if(nfp.getEnderecoCliente() != null){
						enderecoCliente = nfp.getEnderecoCliente();
					} else if(nfp.getEndereconota() != null){
						enderecoCliente = nfp.getEndereconota();
					} else {
						enderecoCliente = nfp.getCliente().getEndereco();
					}
					
					registroE200.setSerie_nf(serie);
					registroE200.setSubserie_nf(null);
					registroE200.setNumero_inicial_nf(numeroNf != null ? Integer.parseInt(numeroNf) : null);
					registroE200.setNumero_final_nf(numeroNf != null ? Integer.parseInt(numeroNf) : null);
					registroE200.setCodigo_cliente_fornecedor(parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? filtro.getApoio().addCliente(nfp.getCliente().getCdpessoa(), null) : filtro.getApoio().addCliente(nfp.getCliente().getCdpessoa(), nfp.getCliente().getCpfOuCnpjValue()) );
					registroE200.setData_emissao(nfp.getDtEmissao());
					registroE200.setData_entrada_saida(nfp.getDatahorasaidaentrada() != null ? new Date(nfp.getDatahorasaidaentrada().getTime()) : null);
					
					String uf_nf = null;
					if(enderecoCliente != null){
						if(enderecoCliente.getPais() != null && !Pais.BRASIL.getCdpais().equals(enderecoCliente.getPais().getCdpais())){
							uf_nf = "EX";
						} else if(enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null){
							uf_nf = enderecoCliente.getMunicipio().getUf().getSigla();
						}
					}
					registroE200.setUf_nf(uf_nf);
					registroE200.setModelo_nf("55");
					registroE200.setEmitente_nf("P");
					registroE200.setSituacao_nf(5);
					registroE200.setHora_emissao_nf(null);
					registroE200.setHora_entrada_saida_nf(null);
					
					tipo_frete = 1;
					if(ResponsavelFrete.EMITENTE.equals(nfp.getResponsavelfrete())) tipo_frete = 1;
					if(ResponsavelFrete.DESTINATARIO.equals(nfp.getResponsavelfrete())) tipo_frete = 2;
					if(ResponsavelFrete.TERCEIROS.equals(nfp.getResponsavelfrete())) tipo_frete = 3;
					
					registroE200.setTipo_frete(tipo_frete);
					registroE200.setValor_contabil(null);
					registroE200.setCodigo_int_contabil_imposto_retidos(null);
					registroE200.setCodigo_conta(null);
					registroE200.setNf_conjugada(null);
					
					registroE200.setListaRegistroE201(createRegistroE201(registroE200, nfp, true));
					registroE200.setRegistroE207(createRegistroE207(registroE200, nfp));
					registroE200.setRegistroE209(createRegistroE209(registroE200, nfp));
					registroE200.setRegistroE210(createRegistroE210(registroE200, nfp));
					registroE200.setListaRegistroE330(createRegistroE330(registroE200, nfp));
					registroE200.setRegistroE336(createRegistroE336(registroE200, nfp));
					
					registroE200.setNumero_linhas_lancamento(registroE200.getListaRegistroE201() != null ? registroE200.getListaRegistroE201().size() : 0);
					registroE200.setData_lancamento_sistema(nfp.getDtEmissao());
					
					listaRegistroE200.add(registroE200);
					
					numero++;
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaNotafiscalproduto)){
			for(Notafiscalproduto nfp : listaNotafiscalproduto){
				boolean cancelada = NotaStatus.CANCELADA.equals(nfp.getNotaStatus());
				if(cancelada){
					nfp.setTipooperacaonota(Tipooperacaonota.SAIDA);
				}
				
				boolean saida = Tipooperacaonota.SAIDA.equals(nfp.getTipooperacaonota());
				
				registroE200 = new RegistroE200();
				registroE200.setNome_registro(RegistroE200.NOME_REGISTRO);
				registroE200.setEntradas_saidas(saida ? "S" : "E");
				registroE200.setEspecie_nf("NFe");
				
				numeroNf = nfp.getNumero();
				serie = nfp.getSerienfe() != null ? nfp.getSerienfe() + "" : "1"; 
				arquivonfnota = arquivonfnotaService.findByNotaProdutoAndCancelada(nfp);
				
				String codigoClienteFornecedor = filtro.getApoio().addCliente(nfp.getCliente().getCdpessoa(), parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? null : nfp.getCliente().getCpfOuCnpjValue());
				
				Endereco enderecoClienteCadastro = null;
				if(nfp.getEndereconota() != null){
					enderecoClienteCadastro = nfp.getEndereconota();
				} else {
					enderecoClienteCadastro = nfp.getCliente().getEndereco();
				}
				Endereco enderecoCliente = nfp.getEnderecoCliente() != null ? nfp.getEnderecoCliente() : enderecoClienteCadastro;
				
				if(enderecoClienteCadastro != null && 
						enderecoCliente != null && 
						!enderecoCliente.equals(enderecoClienteCadastro)){
					 filtro.getApoio().addClienteEndereco(nfp.getCliente().getCdpessoa(), 
							 								enderecoCliente.getCdendereco(), 
							 								nfp.getDtEmissao());
				}
				
				registroE200.setSerie_nf(serie);
				registroE200.setSubserie_nf(null);
				registroE200.setNumero_inicial_nf(numeroNf != null ? Integer.parseInt(numeroNf) : null);
				registroE200.setNumero_final_nf(numeroNf != null ? Integer.parseInt(numeroNf) : null);
				registroE200.setCodigo_cliente_fornecedor(codigoClienteFornecedor);
				registroE200.setData_emissao(nfp.getDtEmissao());
				registroE200.setData_entrada_saida(nfp.getDatahorasaidaentrada() != null ? new Date(nfp.getDatahorasaidaentrada().getTime()) : nfp.getDtEmissao());
				
				String uf_nf = null;
				if(enderecoCliente != null){
					if(enderecoCliente.getPais() != null && !Pais.BRASIL.getCdpais().equals(enderecoCliente.getPais().getCdpais())){
						uf_nf = saida ? "EX" : "IM";
					}else if(enderecoCliente.getMunicipio() != null && enderecoCliente.getMunicipio().getUf() != null){
						uf_nf = enderecoCliente.getMunicipio().getUf().getSigla();
					}
				}
				registroE200.setUf_nf(uf_nf);
				registroE200.setModelo_nf("55");
				registroE200.setEmitente_nf("P");
				registroE200.setSituacao_nf(cancelada ? 2 : 0);
//				if(cancelada){
//					NotaHistorico notaHistorico = notaHistoricoService.carregarUltimoHistorico(nfp, NotaStatus.NFE_CANCELANDO);
//					registroE200.setJustificativa_nf_cancelada(notaHistorico != null ? notaHistorico.getObservacao() : null);
//				}
				registroE200.setHora_emissao_nf(null);
				registroE200.setHora_entrada_saida_nf(null);
				
				tipo_frete = 1;
				if(ResponsavelFrete.EMITENTE.equals(nfp.getResponsavelfrete())) tipo_frete = 1;
				if(ResponsavelFrete.DESTINATARIO.equals(nfp.getResponsavelfrete())) tipo_frete = 2;
				if(ResponsavelFrete.TERCEIROS.equals(nfp.getResponsavelfrete())) tipo_frete = 3;
				
				registroE200.setTipo_frete(tipo_frete);
				if(!cancelada) registroE200.setValor_contabil(nfp.getValor() != null ? nfp.getValor().getValue().doubleValue() : null);
				registroE200.setCodigo_int_contabil_imposto_retidos(null);
				registroE200.setCodigo_conta(null);
				registroE200.setNf_conjugada(null);
				
				registroE200.setListaRegistroE201(createRegistroE201(registroE200, nfp, false));
				registroE200.setRegistroE207(createRegistroE207(registroE200, nfp));
				if(saida){
					registroE200.setRegistroE209(createRegistroE209(registroE200, nfp));
					registroE200.setRegistroE210(createRegistroE210(registroE200, nfp));
				}else if(Tipooperacaonota.ENTRADA.equals(nfp.getTipooperacaonota())){
					registroE200.setRegistroE216(createRegistroE216(registroE200, nfp));
				}
				if(!cancelada){
					registroE200.setRegistroE221(createRegistroE221(filtro, registroE200, nfp));
				}
				registroE200.setListaRegistroE330(createRegistroE330(registroE200, nfp));
				registroE200.setListaRegistroE333(createRegistroE333(filtro, registroE200, nfp));
				registroE200.setRegistroE336(createRegistroE336(registroE200, nfp));
				registroE200.setRegistroE342(createRegistroE342(registroE200, arquivonfnota));
				
				registroE200.setNumero_linhas_lancamento(registroE200.getListaRegistroE201() != null ? registroE200.getListaRegistroE201().size() : 0);
				if(saida){
					registroE200.setData_lancamento_sistema(nfp.getDtEmissao());
				} else {
					registroE200.setData_lancamento_sistema(nfp.getDatahorasaidaentrada() != null ? new Date(nfp.getDatahorasaidaentrada().getTime()) : nfp.getDtEmissao());
				}
				
				listaRegistroE200.add(registroE200);
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaEntregadocumento)){
			String uf_nf;
			for(Entregadocumento ed : listaEntregadocumento){
				Modelodocumentofiscal modelodocumentofiscal = ed.getModelodocumentofiscal();
				if(modelodocumentofiscal != null && modelodocumentofiscal.getSpedpiscofins_f100() != null && modelodocumentofiscal.getSpedpiscofins_f100()){
					continue;
				}
				
				String modelo = modelodocumentofiscal != null ? modelodocumentofiscal.getCodigo() : null;
				
				uf_nf = null;
				if(ed.getFornecedor() != null && SinedUtil.isListNotEmpty(ed.getFornecedor().getListaEndereco())){
					Endereco endereco = ed.getFornecedor().getListaEndereco().iterator().next();
					if(endereco != null){
						if(endereco.getPais() != null && !Pais.BRASIL.equals(endereco.getPais())){
							uf_nf = "IM";
						}else if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
							uf_nf = endereco.getMunicipio().getUf().getSigla();
						}
					}
				}
				
				registroE200 = new RegistroE200();
				registroE200.setNome_registro(RegistroE200.NOME_REGISTRO);
				registroE200.setEntradas_saidas("E");
				if(modelo.equals("55")){
					registroE200.setEspecie_nf("NFE");
				} else if(modelo.equals("57")){
					registroE200.setEspecie_nf("CTE");
				} else if(modelo.equals("22")){
					registroE200.setEspecie_nf("NFFT");
				} else if(modelo.equals("06")){
					registroE200.setEspecie_nf("NFEL");
				} else if(modelo.equals("01")){
					registroE200.setEspecie_nf("NF");
				} else {
					registroE200.setEspecie_nf(ed.getEspecie() != null ? ed.getEspecie().toString() : null);
				}
				registroE200.setSerie_nf(ed.getSerie());
				registroE200.setSubserie_nf(ed.getSubserie());
				try{
					registroE200.setNumero_inicial_nf(ed.getNumero() != null ? Integer.parseInt(ed.getNumero()) : null);
					registroE200.setNumero_final_nf(ed.getNumero() != null ? Integer.parseInt(ed.getNumero()) : null);
				} catch (Exception e) {}
				registroE200.setCodigo_cliente_fornecedor(parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? filtro.getApoio().addFornecedor(ed.getFornecedor().getCdpessoa(), null) : filtro.getApoio().addFornecedor(ed.getFornecedor().getCdpessoa(), ed.getFornecedor().getCpfOuCnpjValue()));
				registroE200.setData_emissao(ed.getDtemissao());
				registroE200.setData_entrada_saida(ed.getDtentrada());
				registroE200.setUf_nf(uf_nf);
				registroE200.setModelo_nf(modelo);
				registroE200.setEmitente_nf("T");
				registroE200.setSituacao_nf(ed.getSituacaodocumento() != null ? Integer.parseInt(ed.getSituacaodocumento().getValue()) : null);
				registroE200.setJustificativa_nf_cancelada(null);
				registroE200.setHora_emissao_nf(null);
				registroE200.setHora_entrada_saida_nf(null);
				
				tipo_frete = 1;
				if(ResponsavelFrete.EMITENTE.equals(ed.getResponsavelfrete())) tipo_frete = 1;
				if(ResponsavelFrete.DESTINATARIO.equals(ed.getResponsavelfrete())) tipo_frete = 2;
				if(ResponsavelFrete.TERCEIROS.equals(ed.getResponsavelfrete())) tipo_frete = 3;
				
				registroE200.setTipo_frete(tipo_frete);
				registroE200.setValor_contabil(ed.getValortotaldocumento().getValue().doubleValue());
				registroE200.setCodigo_int_contabil_imposto_retidos(null);
				registroE200.setCodigo_conta(null);
				registroE200.setNf_conjugada(null);
				
				registroE200.setListaRegistroE201(createRegistroE201(registroE200, ed));
				if(modelo != null && (modelo.equals("55") || modelo.equals("57"))){
					registroE200.setRegistroE207(createRegistroE207(registroE200, ed));
				}
				registroE200.setRegistroE216(createRegistroE216(registroE200, ed));
				registroE200.setRegistroE221(createRegistroE221(filtro, registroE200, ed));
				if(modelo != null && (modelo.equals("01") || modelo.equals("1B") || modelo.equals("04") || modelo.equals("55"))){
					registroE200.setListaRegistroE330(createRegistroE330(registroE200, ed));
				}
				registroE200.setListaRegistroE332(createRegistroE332(registroE200, ed));
				registroE200.setListaRegistroE333(createRegistroE333(filtro, registroE200, ed));
				registroE200.setRegistroE336(createRegistroE336(registroE200, ed));
				if(modelo != null && (modelo.equals("55") || modelo.equals("57") || modelo.equals("59") || modelo.equals("65"))){
					registroE200.setRegistroE342(createRegistroE342(registroE200, ed));
				}
				
				registroE200.setNumero_linhas_lancamento(registroE200.getListaRegistroE201() != null ? registroE200.getListaRegistroE201().size() : 0);
				registroE200.setData_lancamento_sistema(ed.getDtentrada() != null ? ed.getDtentrada() : ed.getDtemissao());
				
				listaRegistroE200.add(registroE200);
			}
		}
		
		return listaRegistroE200;
	}
	
	private RegistroE342 createRegistroE342(RegistroE200 registroE200, Arquivonfnota arquivonfnota) {
		if(arquivonfnota == null) return null;
		RegistroE342 registroE342 = new RegistroE342();
		registroE342.setNome_registro(RegistroE342.NOME_REGISTRO);
		registroE342.setEntradas_saidas(registroE200.getEntradas_saidas());
		registroE342.setEspecie_nf(registroE200.getEspecie_nf());
		registroE342.setSerie_nf(registroE200.getSerie_nf());
		registroE342.setSubserie_nf(registroE200.getSubserie_nf());
		registroE342.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE342.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE342.setChave_nfe_cte_cfe_nfce(arquivonfnota.getChaveacesso());
		
		return registroE342;
	}
	
	private RegistroE342 createRegistroE342(RegistroE200 registroE200, Entregadocumento entregadocumento) {
		if(entregadocumento.getChaveacesso() == null || entregadocumento.getChaveacesso().equals("")) return null;
		RegistroE342 registroE342 = new RegistroE342();
		registroE342.setNome_registro(RegistroE342.NOME_REGISTRO);
		registroE342.setEntradas_saidas(registroE200.getEntradas_saidas());
		registroE342.setEspecie_nf(registroE200.getEspecie_nf());
		registroE342.setSerie_nf(registroE200.getSerie_nf());
		registroE342.setSubserie_nf(registroE200.getSubserie_nf());
		registroE342.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE342.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		if(registroE200.getModelo_nf().equals("57")){
			registroE342.setTipo_cte("0");
		}
		registroE342.setChave_nfe_cte_cfe_nfce(entregadocumento.getChaveacesso());
		
		return registroE342;
	}
	
	private RegistroE221 createRegistroE221(SagefiscalFiltro filtro, RegistroE200 registroE200, Entregadocumento ed) {
		RegistroE221 registroE221 = new RegistroE221();
		registroE221.setNome_registro(RegistroE221.NOME_REGISTRO);
		registroE221.setEntrada_saidas(registroE200.getEntradas_saidas());
		registroE221.setEspecie_nf(registroE200.getEspecie_nf());
		registroE221.setSerie_nf(registroE200.getSerie_nf());
		registroE221.setSubserie_nf(registroE200.getSubserie_nf());
		registroE221.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE221.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE221.setFrete(ed.getValorfrete() != null ? ed.getValorfrete().getValue().doubleValue() : null);
		registroE221.setSeguro(ed.getValorseguro() != null ? ed.getValorseguro().getValue().doubleValue() : null);
		registroE221.setDespesas_acessorias(ed.getValoroutrasdespesas() != null ? ed.getValoroutrasdespesas().getValue().doubleValue() : null);
		registroE221.setQuantidade_itens_nf(ed.getListaEntregamaterial().size());
		registroE221.setPis_cofins(ed.getValortotalpisretido() != null ? ed.getValortotalpisretido().getValue().doubleValue() : null);
		
		Double peso_bruto = 0d;
		Double peso_liquido = 0d;
		if(SinedUtil.isListNotEmpty(ed.getListaEntregamaterial())){
			for(Entregamaterial entregamaterial : ed.getListaEntregamaterial()){
				if(entregamaterial.getMaterial() != null && entregamaterial.getQtde() != null){
					if(entregamaterial.getMaterial().getPeso() != null) peso_liquido += entregamaterial.getQtde() * entregamaterial.getMaterial().getPeso();
					if(entregamaterial.getMaterial().getPesobruto() != null) peso_bruto += entregamaterial.getQtde() * entregamaterial.getMaterial().getPesobruto();
				}
			}
		}
		registroE221.setPeso_bruto(peso_bruto);
		registroE221.setPeso_liquido(peso_liquido);
		registroE221.setVia_transporte(null);
		registroE221.setCodigo_transportador(null);
		registroE221.setIdentificacao_veiculo_1(ed.getPlacaveiculo());
		registroE221.setIe_substituto_tributario(null);
		registroE221.setQtde_volumes(null);
		registroE221.setEspecia_volumes(null);
		registroE221.setIe_transportador(null);
		registroE221.setEstado_trasnportador(null);
		registroE221.setEstado_placa_veiculo_1(null);
		registroE221.setIdentificacao_veiculo_2(null);
		registroE221.setEstado_placa_veiculo_2(null);
		registroE221.setIdentificacao_veiculo_3(null);
		registroE221.setEstado_placa_veiculo_3(null);
		registroE221.setLocal_saida_mercadoria(null);
		registroE221.setCnpj_cpf_saida_mercadoria(null);
		registroE221.setEstado_saida_mercadoria(null);
		registroE221.setIe_saida_mercadoria(null);
		registroE221.setLocal_recebimento_mercadoria(null);
		registroE221.setCnpj_cpf_recebimento_mercadoria(null);
		registroE221.setEstado_recebimento_mercadoria(null);
		registroE221.setIe_recebimento_mercadoria(null);
		registroE221.setUf_transportador(null);
		registroE221.setControle_sistema(0);
		
		registroE221.setListaRegistroE222(createRegistroE222(filtro, registroE200, ed));
		return registroE221;
	}
	
	
	private RegistroE221 createRegistroE221(SagefiscalFiltro filtro,RegistroE200 registroE200, Notafiscalproduto nfp) {
		RegistroE221 registroE221 = new RegistroE221();
		registroE221.setNome_registro(RegistroE221.NOME_REGISTRO);
		registroE221.setEntrada_saidas(registroE200.getEntradas_saidas());
		registroE221.setEspecie_nf(registroE200.getEspecie_nf());
		registroE221.setSerie_nf(registroE200.getSerie_nf());
		registroE221.setSubserie_nf(registroE200.getSubserie_nf());
		registroE221.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE221.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE221.setFrete(nfp.getValorfrete() != null ? nfp.getValorfrete().getValue().doubleValue() : null);
		registroE221.setSeguro(nfp.getValorseguro() != null ? nfp.getValorseguro().getValue().doubleValue() : null);
		registroE221.setDespesas_acessorias(nfp.getOutrasdespesas() != null ? nfp.getOutrasdespesas().getValue().doubleValue() : null);
		registroE221.setQuantidade_itens_nf(nfp.getListaItens().size());
		registroE221.setPis_cofins(nfp.getValorpisretido() != null ? nfp.getValorpisretido().getValue().doubleValue() : null);
		registroE221.setPeso_bruto(nfp.getPesobruto());
		registroE221.setPeso_liquido(nfp.getPesoliquido());
		registroE221.setVia_transporte(null);
		registroE221.setCodigo_transportador(nfp.getTransportador() != null ? (parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? filtro.getApoio().addFornecedor(nfp.getTransportador().getCdpessoa(), null) : filtro.getApoio().addFornecedor(nfp.getTransportador().getCdpessoa(), nfp.getTransportador().getCpfOuCnpjValue())) : null);
		registroE221.setIdentificacao_veiculo_1(nfp.getPlacaveiculo());
		registroE221.setIe_substituto_tributario(null);
		registroE221.setQtde_volumes(nfp.getQtdevolume());
		registroE221.setEspecia_volumes(nfp.getEspvolume());
		registroE221.setIe_transportador(null);
		registroE221.setEstado_trasnportador(nfp.getUfveiculo() != null ? nfp.getUfveiculo().getSigla() : null);
		registroE221.setEstado_placa_veiculo_1(null);
		registroE221.setIdentificacao_veiculo_2(null);
		registroE221.setEstado_placa_veiculo_2(null);
		registroE221.setIdentificacao_veiculo_3(null);
		registroE221.setEstado_placa_veiculo_3(null);
		registroE221.setLocal_saida_mercadoria(null);
		registroE221.setCnpj_cpf_saida_mercadoria(null);
		registroE221.setEstado_saida_mercadoria(null);
		registroE221.setIe_saida_mercadoria(null);
		registroE221.setLocal_recebimento_mercadoria(null);
		registroE221.setCnpj_cpf_recebimento_mercadoria(null);
		registroE221.setEstado_recebimento_mercadoria(null);
		registroE221.setIe_recebimento_mercadoria(null);
		registroE221.setUf_transportador(null);
		registroE221.setControle_sistema(0);
		
		registroE221.setListaRegistroE222(createRegistroE222(filtro, registroE200, nfp));
		return registroE221;
	}
	
	private List<RegistroE222> createRegistroE222(SagefiscalFiltro filtro, RegistroE200 registroE200, Notafiscalproduto nfp) {
		boolean cancelada = NotaStatus.CANCELADA.equals(nfp.getNotaStatus());
		
		List<RegistroE222> listaRegistroE222 = new ArrayList<RegistroE222>();
		if(SinedUtil.isListNotEmpty(nfp.getListaItens())){
			RegistroE222 registroE222;
			Integer sequencial = 1;
			for(Notafiscalprodutoitem item : nfp.getListaItens()){
				registroE222 = new RegistroE222();
				
				registroE222.setNome_registro(RegistroE222.NOME_REGISTRO);
				registroE222.setEnrada_saidas(registroE200.getEntradas_saidas());
				registroE222.setEspecie_nf(registroE200.getEspecie_nf());
				registroE222.setSerie_nf(registroE200.getSerie_nf());
				registroE222.setSubserie_nf(registroE200.getSubserie_nf());
				registroE222.setNumero_nf(registroE200.getNumero_inicial_nf());
				registroE222.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
				registroE222.setNumero_item(sequencial);
				registroE222.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
				registroE222.setCodigo_produto_servico(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), item.getTipocobrancapis(), item.getTipocobrancacofins()) : null);
				registroE222.setQuantidade(item.getQtde());
				
				if(!cancelada){
					registroE222.setVlr_mercadoria_servico(item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : null);
					registroE222.setValor_desconto(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : null);
					registroE222.setValor_unitario(item.getValorunitario());
					registroE222.setNumero_di_dsi(null);
					
					if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(item.getValorFiscalIcms())){
						registroE222.setBase_calculo_icms(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : null);
						registroE222.setAliquota_icms(item.getIcms());
						registroE222.setValor_icms(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue() : null);
						registroE222.setReducao_base_calc_icms(item.getReducaobcicms());
						
						Double valorbcicms = item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : 0d;
						Double valor = item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0d;
						double valoroutrasicms = valor - valorbcicms;
						if(valoroutrasicms > 0){
							registroE222.setOutros_icms(valoroutrasicms);
						}
					}
					if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(item.getValorFiscalIcms())){
						registroE222.setIsento_icms(item.getValorbruto().getValue().doubleValue());
					}
					if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(item.getValorFiscalIcms())){
						registroE222.setOutros_icms(item.getValorbruto().getValue().doubleValue());
					}
					if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(item.getValorFiscalIpi())){
						registroE222.setBc_ipi(item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : null);
						registroE222.setValor_ipi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : null);
						registroE222.setIpi_negativo(true);
						
						Double valorbcipi = item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0d;
						Double valor = item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0d;
						double valoroutrasipi = valor - valorbcipi;
						if(valoroutrasipi > 0){
							registroE222.setOutros_ipi(valoroutrasipi);
						}
					}
					if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(item.getValorFiscalIpi())){
						registroE222.setIsentos_ipi(item.getValorbruto().getValue().doubleValue());
					}
					if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(item.getValorFiscalIpi())){
						registroE222.setOutros_ipi(item.getValorbruto().getValue().doubleValue());
					}
					
					registroE222.setBase_subst_trib(item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : null);
					registroE222.setAliquota_icms_st(item.getIcmsst());
					registroE222.setIcms_subst_trib(item.getValoricmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : null);
					registroE222.setNumero_coo(null);
					registroE222.setMovimentacao_fisica_mercadoria("S");
					registroE222.setFrete(item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : null);
					registroE222.setSeguro(item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : null);
					registroE222.setDespesas_acessorias(item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : null);
					registroE222.setDesconto_incondicional(registroE222.getValor_desconto() != null && registroE222.getValor_desconto() > 0 ? "S" : "N");
				} else {
					registroE222.setMovimentacao_fisica_mercadoria("N");
					registroE222.setDesconto_incondicional("N");
				}
				
				registroE222.setBase_st_origem_destino(null);
				registroE222.setIcms_st_repassar(null);
				registroE222.setIcms_st_complementar(null);
				registroE222.setItem_cancelado("N");
				registroE222.setOperacoes_prestacoes_regime_subst_trib_saida(null);
				registroE222.setIss("N");
				registroE222.setCodigo_unid_comercial(item.getUnidademedida() != null ? filtro.getApoio().addUnidadeComercial(item.getUnidademedida().getSimbolo()) : null);
				registroE222.setCodigo_natureza_operacao(nfp.getNaturezaoperacao() != null ? filtro.getApoio().addNaturezaOperacao(nfp.getNaturezaoperacao().getCdnaturezaoperacao()) : null);
				registroE222.setDescricao_complementar_produto(null);
				
				if(item.getUnidademedida() != null && 
						item.getMaterial() != null && 
						item.getMaterial().getUnidademedida() != null &&
						!item.getUnidademedida().equals(item.getMaterial().getUnidademedida())){
					Double conversao = unidademedidaService.getFracaoconversaoUnidademedida(item.getUnidademedida(), item.getQtde(), item.getMaterial().getUnidademedida(), item.getMaterial(), null);
					if(conversao == null) conversao = 1d; 
					registroE222.setFator_conversao(conversao);
				}
				
				Origemproduto origemproduto = item.getOrigemproduto();
				Tipocobrancaicms tipocobrancaicms = item.getTipocobrancaicms();
				
				registroE222.setSit_trib_icms_tab_a(origemproduto != null ? origemproduto.getValue().toString() : null);
				registroE222.setSit_trib_icms_tab_b(tipocobrancaicms != null ? tipocobrancaicms.getCdnfe() : null);
				registroE222.setSit_trib_ipi(item.getTipocobrancaipi() != null ? item.getTipocobrancaipi().getCdnfe() : null);
				registroE222.setApuracao_distintas_ipi(null);
				registroE222.setCodigo_conta(null);
				registroE222.setFrete_no_total_nf("S");
				registroE222.setSeguro_no_total_nf("S");
				registroE222.setDespesas_acessorias_no_total_nf("S");
				registroE222.setAcrescimo(null);
				registroE222.setValor_nao_tributado_red_bc_icms(null);
				registroE222.setQuantidade_cancelada(null);
				registroE222.setBase_icms_reduzida(null);
				registroE222.setDados_redf(4);
				registroE222.setNumero_totalizador(null);
				registroE222.setCsosn(null);
				registroE222.setControle_sistema(0);
				
				registroE222.setRegistroE233(createRegistroE233(registroE222, nfp, item));
				
				listaRegistroE222.add(registroE222);
				sequencial++;
			}
		}
		return listaRegistroE222;
	}
	
	
	private List<RegistroE222> createRegistroE222(SagefiscalFiltro filtro, RegistroE200 registroE200, Entregadocumento ed) {
		List<RegistroE222> listaRegistroE222 = new ArrayList<RegistroE222>();
		if(SinedUtil.isListNotEmpty(ed.getListaEntregamaterial())){
			RegistroE222 registroE222;
			Integer sequencial = 1;
			for(Entregamaterial item : ed.getListaEntregamaterial()){
				registroE222 = new RegistroE222();
				
				registroE222.setNome_registro(RegistroE222.NOME_REGISTRO);
				registroE222.setEnrada_saidas(registroE200.getEntradas_saidas());
				registroE222.setEspecie_nf(registroE200.getEspecie_nf());
				registroE222.setSerie_nf(registroE200.getSerie_nf());
				registroE222.setSubserie_nf(registroE200.getSubserie_nf());
				registroE222.setNumero_nf(registroE200.getNumero_inicial_nf());
				registroE222.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
				registroE222.setNumero_item(sequencial);
				registroE222.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
				registroE222.setCodigo_produto_servico(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), item.getCstpis(), item.getCstcofins()) : null);
				registroE222.setVlr_mercadoria_servico(SinedUtil.round(item.getValortotalmaterial(), 2));
				registroE222.setValor_desconto(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : null);
//				registroE222.setBase_subst_trib(item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : null);
//				registroE222.setAliquota_icms_st(item.getIcmsst());
//				registroE222.setIcms_subst_trib(item.getValoricmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : null);
				registroE222.setNumero_di_dsi(null);
				
				ValorFiscal valorFiscalIpi = item.getValorFiscalIpi();
				if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi) && item.getValoripi().getValue().doubleValue() <= 0){
					valorFiscalIpi = ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS;
				}
				
				Double valortotaloperacaoIcms = item.getValortotaloperacao(!ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi));
				if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(item.getValorFiscalIcms())){
					registroE222.setBase_calculo_icms(item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : null);
					registroE222.setAliquota_icms(item.getIcms());
					registroE222.setValor_icms(item.getValoricms() != null ? item.getValoricms().getValue().doubleValue() : null);
					
					Double valorbcicms = item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : 0d;
					Double valor = valortotaloperacaoIcms != null ? valortotaloperacaoIcms : 0d;
					double valoroutrasicms = valor - valorbcicms;
					if(valoroutrasicms > 0){
						registroE222.setOutros_icms(valoroutrasicms);
					}
				}
				if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(item.getValorFiscalIcms())){
					registroE222.setIsento_icms(valortotaloperacaoIcms);
				}
				if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(item.getValorFiscalIcms())){
					registroE222.setOutros_icms(valortotaloperacaoIcms);
				}
				if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi)){
					registroE222.setBc_ipi(item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : null);
					registroE222.setValor_ipi(item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : null);
					registroE222.setIpi_negativo(true);
					
					Double valorbcipi = item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0d;
					Double valor = item.getValortotaloperacao(false) != null ? item.getValortotaloperacao(false) : 0d;
					double valoroutrasipi = valor - valorbcipi;
					if(valoroutrasipi > 0){
						registroE222.setOutros_ipi(valoroutrasipi);
					}
				}
				if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(valorFiscalIpi)){
					registroE222.setIsentos_ipi(item.getValortotaloperacao());
				}
				if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(valorFiscalIpi)){
					registroE222.setOutros_ipi(item.getValortotaloperacao());
				}
				
				registroE222.setNumero_coo(null);
				registroE222.setMovimentacao_fisica_mercadoria("S");
				registroE222.setBase_st_origem_destino(null);
				registroE222.setIcms_st_repassar(null);
				registroE222.setIcms_st_complementar(null);
				registroE222.setItem_cancelado("N");
				registroE222.setOperacoes_prestacoes_regime_subst_trib_saida(null);
				registroE222.setIss("N");
				
				Material materialIt = item.getMaterial();
				Double qtdeIt = item.getQtde();
				Double valorunitarioIt = item.getValorunitario();
				Unidademedida unidIt = item.getUnidademedidacomercial();
				Unidademedida unidMat = materialIt != null ? materialIt.getUnidademedida() : null;
				
				if(unidIt != null && unidMat != null && !unidIt.equals(unidMat)){
					Double qtdeItConv = unidademedidaService.getQtdeUnidadePrincipal(materialIt, unidIt, qtdeIt);
					Double valortotalIt = qtdeIt * valorunitarioIt;
					Double valorunitarioItConv = valortotalIt / qtdeItConv;
					registroE222.setQuantidade(qtdeItConv);
					registroE222.setCodigo_unid_comercial(unidMat != null ? filtro.getApoio().addUnidadeComercial(unidMat.getSimbolo()) : null);
					registroE222.setValor_unitario(SinedUtil.round(valorunitarioItConv, 5));
				} else {
					registroE222.setQuantidade(qtdeIt);
					registroE222.setCodigo_unid_comercial(unidIt != null ? filtro.getApoio().addUnidadeComercial(unidIt.getSimbolo()) : null);
					registroE222.setValor_unitario(valorunitarioIt);
				}
				
				if(registroE200.getModelo_nf().equals("55")){
					registroE222.setCodigo_natureza_operacao(ed.getNaturezaoperacao() != null ? filtro.getApoio().addNaturezaOperacao(ed.getNaturezaoperacao().getCdnaturezaoperacao()) : null);
				}
				registroE222.setDescricao_complementar_produto(null);
				registroE222.setFator_conversao(null);
				
				Origemproduto origemproduto = item.getMaterial().getOrigemproduto();
				Tipocobrancaicms tipocobrancaicms = item.getCsticms();
				if(item.getGrupotributacao() != null && 
						item.getGrupotributacao().getTipocobrancaicms() != null &&
						tipocobrancaicms == null){
					tipocobrancaicms = item.getGrupotributacao().getTipocobrancaicms();
				}
				
				registroE222.setSit_trib_icms_tab_a(origemproduto != null ? origemproduto.getValue().toString() : "0");
				registroE222.setSit_trib_icms_tab_b(tipocobrancaicms != null ? tipocobrancaicms.getCdnfe() : null);
				
				registroE222.setSit_trib_ipi(item.getCstipi() != null ? item.getCstipi().getCdnfe() : null);
				registroE222.setApuracao_distintas_ipi(null);
				registroE222.setCodigo_conta(null);
				registroE222.setFrete(item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : null);
				registroE222.setFrete_no_total_nf("S");
				registroE222.setSeguro(item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : null);
				registroE222.setSeguro_no_total_nf("S");
				registroE222.setDespesas_acessorias(item.getValoroutrasdespesas() != null ? item.getValoroutrasdespesas().getValue().doubleValue() : null);
				registroE222.setDespesas_acessorias_no_total_nf("S");
				registroE222.setAcrescimo(null);
				registroE222.setReducao_base_calc_icms(null);
				registroE222.setValor_nao_tributado_red_bc_icms(null);
				registroE222.setQuantidade_cancelada(null);
				registroE222.setBase_icms_reduzida(null);
				registroE222.setDados_redf(4);
				registroE222.setNumero_totalizador(null);
				registroE222.setDesconto_incondicional(registroE222.getValor_desconto() != null && registroE222.getValor_desconto() > 0 ? "S" : "N");
				registroE222.setCsosn(null);
				registroE222.setControle_sistema(0);
				
				registroE222.setRegistroE233(createRegistroE233(registroE222, ed, item));
				
				listaRegistroE222.add(registroE222);
				sequencial++;
			}
		}
		return listaRegistroE222;
	}
	
	
	private RegistroE233 createRegistroE233(RegistroE222 registroE222, Notafiscalproduto notafiscalproduto, Notafiscalprodutoitem item) {
		Tipocobrancacofins cstcofins = item.getTipocobrancacofins();
		Tipocobrancapis cstpis = item.getTipocobrancapis();
		
		boolean cstcofinsCredito = Tipocobrancacofins.isCreditoCofins(cstcofins);
		boolean cstcpisCredito = Tipocobrancapis.isCreditoPis(cstpis);
		boolean cstcofinsDebito = Tipocobrancacofins.isDebitoCofins(cstcofins);
		boolean cstcpisDebito = Tipocobrancapis.isDebitoPis(cstpis);
		
		boolean cancelada = NotaStatus.CANCELADA.equals(notafiscalproduto.getNotaStatus());
		boolean entrada = Tipooperacaonota.ENTRADA.equals(notafiscalproduto.getTipooperacaonota());
		
		if(entrada){
			if(cstcofinsCredito || cstcpisCredito){
				RegistroE233 registroE233 = new RegistroE233();
				registroE233.setNome_registro(RegistroE233.NOME_REGISTRO);
				registroE233.setEntradas_saidas(entrada ? "E" : "S");
				registroE233.setEspecie_nf(registroE222.getEspecie_nf());
				registroE233.setSerie_nf(registroE222.getSerie_nf());
				registroE233.setSubserie_nf(registroE222.getSubserie_nf());
				registroE233.setNumero_nf(registroE222.getNumero_nf());
				registroE233.setCodigo_cliente_fornecedor(registroE222.getCodigo_cliente_fornecedor());
				registroE233.setNumero_ordem_item(registroE222.getNumero_item());
				
				if(!cancelada){
					if(item.getTipocalculopis() != null && item.getTipocalculopis().equals(Tipocalculo.EM_VALOR)){
						registroE233.setBase_pis(null);
						registroE233.setAliq_pis(null);
						registroE233.setQuantida_base_pis(item.getQtdevendidapis());
						registroE233.setAli_pis_em_reais(item.getAliquotareaispis() != null ? item.getAliquotareaispis().getValue().doubleValue() : null);
					} else {
						registroE233.setBase_pis(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : null);
						registroE233.setAliq_pis(item.getPis());
						registroE233.setQuantida_base_pis(null);
						registroE233.setAli_pis_em_reais(null);
					}
					registroE233.setValor_pis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : null);
					registroE233.setStpis(item.getTipocobrancapis() != null ? item.getTipocobrancapis().getCdnfe() : null);
					if(item.getTipocalculocofins() != null && item.getTipocalculocofins().equals(Tipocalculo.EM_VALOR)){
						registroE233.setBase_cofins(null);
						registroE233.setAliq_cofins(null);
						registroE233.setQuantida_base_cofins(item.getQtdevendidacofins());
						registroE233.setAli_cofins_em_reais(item.getAliquotareaiscofins() != null ? item.getAliquotareaiscofins().getValue().doubleValue() : null);
					} else {
						registroE233.setBase_cofins(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : null);
						registroE233.setAliq_cofins(item.getCofins());
						registroE233.setQuantida_base_cofins(null);
						registroE233.setAli_cofins_em_reais(null);
					}
					registroE233.setValor_cofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : null);
					registroE233.setStcofins(item.getTipocobrancacofins() != null ? item.getTipocobrancacofins().getCdnfe() : null);
				}
				
				registroE233.setData_apropriacao(notafiscalproduto.getDtEmissao());
				registroE233.setValor_pis_vinculado_receita_exportacao(null);
				registroE233.setValor_pis_vinculado_receita_tributada_mercado_interno(null);
				registroE233.setValor_pis_vinculado_receita_nao_tributada_mercado_interno(null);
				registroE233.setValor_cofins_vinculado_receita_exportacao(null);
				registroE233.setValor_cofins_vinculado_receita_tributada_mercado_interno(null);
				registroE233.setValor_cofins_vinculado_receita_nao_tributada_mercado_interno(null);
				registroE233.setConta_analitica_contabil_pis(null);
				registroE233.setConta_analitica_contabil_cofins(null);
				registroE233.setDescricao_complementar_documento_operacao_pis(null);
				registroE233.setDescricao_complementar_documento_operacao_cofins(null);
				registroE233.setTipo_incidencia_pis_cofins("");
				registroE233.setNumero_coo(registroE222.getNumero_coo());
				
				return registroE233;
			} else {
//				COMENTADO POR MOTIVO DO ERRO ABAIXO:
//				A EFD PIS/COFINS, aceita valores de cr�ditos apenas para as situa��es tribut�rias 50, 51, 53, 54, 55, 56, 60, 61, 62, 63, 64, 65 ou 66. 
//				Utilizando uma situa��o diferente dos descritos, ocorrer�o erros na valida��o do arquivo da EFD PIS/COFINS.
//				
//				RegistroE233 registroE233 = new RegistroE233();
//				registroE233.setNome_registro(RegistroE233.NOME_REGISTRO);
//				registroE233.setEntradas_saidas(entrada ? "E" : "S");
//				registroE233.setEspecie_nf(registroE222.getEspecie_nf());
//				registroE233.setSerie_nf(registroE222.getSerie_nf());
//				registroE233.setSubserie_nf(registroE222.getSubserie_nf());
//				registroE233.setNumero_nf(registroE222.getNumero_nf());
//				registroE233.setCodigo_cliente_fornecedor(registroE222.getCodigo_cliente_fornecedor());
//				registroE233.setNumero_ordem_item(registroE222.getNumero_item());
//				registroE233.setStpis(item.getTipocobrancapis() != null ? item.getTipocobrancapis().getCdnfe() : null);
//				registroE233.setStcofins(item.getTipocobrancacofins() != null ? item.getTipocobrancacofins().getCdnfe() : null);
//				registroE233.setTipo_incidencia_pis_cofins("");
//				registroE233.setNumero_coo(registroE222.getNumero_coo());
//				
//				return registroE233;
				return null;
			}
		} else {
			if(cstcofinsDebito || cstcpisDebito){
				RegistroE233 registroE233 = new RegistroE233();
				registroE233.setNome_registro(RegistroE233.NOME_REGISTRO);
				registroE233.setEntradas_saidas(entrada ? "E" : "S");
				registroE233.setEspecie_nf(registroE222.getEspecie_nf());
				registroE233.setSerie_nf(registroE222.getSerie_nf());
				registroE233.setSubserie_nf(registroE222.getSubserie_nf());
				registroE233.setNumero_nf(registroE222.getNumero_nf());
				registroE233.setCodigo_cliente_fornecedor(registroE222.getCodigo_cliente_fornecedor());
				registroE233.setNumero_ordem_item(registroE222.getNumero_item());
				
				if(!cancelada){
					if(item.getTipocalculopis() != null && item.getTipocalculopis().equals(Tipocalculo.EM_VALOR)){
						registroE233.setBase_pis(null);
						registroE233.setAliq_pis(null);
						registroE233.setQuantida_base_pis(item.getQtdevendidapis());
						registroE233.setAli_pis_em_reais(item.getAliquotareaispis() != null ? item.getAliquotareaispis().getValue().doubleValue() : null);
					} else {
						registroE233.setBase_pis(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : null);
						registroE233.setAliq_pis(item.getPis());
						registroE233.setQuantida_base_pis(null);
						registroE233.setAli_pis_em_reais(null);
					}
					registroE233.setValor_pis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : null);
					registroE233.setStpis(item.getTipocobrancapis() != null ? item.getTipocobrancapis().getCdnfe() : null);
					if(item.getTipocalculocofins() != null && item.getTipocalculocofins().equals(Tipocalculo.EM_VALOR)){
						registroE233.setBase_cofins(null);
						registroE233.setAliq_cofins(null);
						registroE233.setQuantida_base_cofins(item.getQtdevendidacofins());
						registroE233.setAli_cofins_em_reais(item.getAliquotareaiscofins() != null ? item.getAliquotareaiscofins().getValue().doubleValue() : null);
					} else {
						registroE233.setBase_cofins(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : null);
						registroE233.setAliq_cofins(item.getCofins());
						registroE233.setQuantida_base_cofins(null);
						registroE233.setAli_cofins_em_reais(null);
					}
					registroE233.setValor_cofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : null);
					registroE233.setStcofins(item.getTipocobrancacofins() != null ? item.getTipocobrancacofins().getCdnfe() : null);
				}
				
				registroE233.setData_apropriacao(notafiscalproduto.getDtEmissao());
				registroE233.setValor_pis_vinculado_receita_exportacao(null);
				registroE233.setValor_pis_vinculado_receita_tributada_mercado_interno(null);
				registroE233.setValor_pis_vinculado_receita_nao_tributada_mercado_interno(null);
				registroE233.setValor_cofins_vinculado_receita_exportacao(null);
				registroE233.setValor_cofins_vinculado_receita_tributada_mercado_interno(null);
				registroE233.setValor_cofins_vinculado_receita_nao_tributada_mercado_interno(null);
				registroE233.setConta_analitica_contabil_pis(null);
				registroE233.setConta_analitica_contabil_cofins(null);
				registroE233.setDescricao_complementar_documento_operacao_pis(null);
				registroE233.setDescricao_complementar_documento_operacao_cofins(null);
				registroE233.setTipo_incidencia_pis_cofins("");
				registroE233.setNumero_coo(registroE222.getNumero_coo());
				
				return registroE233;
			} else {
				RegistroE233 registroE233 = new RegistroE233();
				registroE233.setNome_registro(RegistroE233.NOME_REGISTRO);
				registroE233.setEntradas_saidas(entrada ? "E" : "S");
				registroE233.setEspecie_nf(registroE222.getEspecie_nf());
				registroE233.setSerie_nf(registroE222.getSerie_nf());
				registroE233.setSubserie_nf(registroE222.getSubserie_nf());
				registroE233.setNumero_nf(registroE222.getNumero_nf());
				registroE233.setCodigo_cliente_fornecedor(registroE222.getCodigo_cliente_fornecedor());
				registroE233.setNumero_ordem_item(registroE222.getNumero_item());
				registroE233.setStpis(item.getTipocobrancapis() != null ? item.getTipocobrancapis().getCdnfe() : null);
				registroE233.setStcofins(item.getTipocobrancacofins() != null ? item.getTipocobrancacofins().getCdnfe() : null);
				registroE233.setTipo_incidencia_pis_cofins("");
				registroE233.setNumero_coo(registroE222.getNumero_coo());
				
				return registroE233;
			}
		}
	}
	
	private RegistroE233 createRegistroE233(RegistroE222 registroE222, Entregadocumento entregadocumento, Entregamaterial item) {
		Tipocobrancacofins cstcofins = item.getCstcofins();
		Tipocobrancapis cstpis = item.getCstpis();
		
		String codigoModeloDoc = null;
		Modelodocumentofiscal modelodocumentofiscal = entregadocumento.getModelodocumentofiscal();
		if(modelodocumentofiscal != null && modelodocumentofiscal.getCodigo() != null){
			codigoModeloDoc = modelodocumentofiscal.getCodigo();
		}
		
		boolean cstcofinsCredito = Tipocobrancacofins.isCreditoCofins(cstcofins);
		boolean cstcpisCredito = Tipocobrancapis.isCreditoPis(cstpis);
		if(cstcpisCredito || cstcofinsCredito){
			RegistroE233 registroE233 = new RegistroE233();
			registroE233.setNome_registro(RegistroE233.NOME_REGISTRO);
			registroE233.setEntradas_saidas("E");
			registroE233.setEspecie_nf(registroE222.getEspecie_nf());
			registroE233.setSerie_nf(registroE222.getSerie_nf());
			registroE233.setSubserie_nf(registroE222.getSubserie_nf());
			registroE233.setNumero_nf(registroE222.getNumero_nf());
			registroE233.setCodigo_cliente_fornecedor(registroE222.getCodigo_cliente_fornecedor());
			if(codigoModeloDoc != null && 
					(
						codigoModeloDoc.equals("01") || 
						codigoModeloDoc.equals("1B") || 
						codigoModeloDoc.equals("02") || 
						codigoModeloDoc.equals("04") || 
						codigoModeloDoc.equals("06") || 
						codigoModeloDoc.equals("21") || 
						codigoModeloDoc.equals("22") || 
						codigoModeloDoc.equals("28") || 
						codigoModeloDoc.equals("55")
					)){
				registroE233.setBase_pis(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : null);
				registroE233.setAliq_pis(item.getPis());
				registroE233.setQuantida_base_pis(item.getQtdebcpis());
				registroE233.setAli_pis_em_reais(item.getValorunidbcpis());
				registroE233.setValor_pis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : null);
				registroE233.setBase_cofins(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : null);
				registroE233.setAliq_cofins(item.getCofins());
				registroE233.setQuantida_base_cofins(item.getQtdebccofins());
				registroE233.setAli_cofins_em_reais(item.getValorunidbccofins());
				registroE233.setValor_cofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : null);
			}
			registroE233.setNumero_ordem_item(registroE222.getNumero_item());
			registroE233.setStpis(cstpis != null ? cstpis.getCdnfe() : null);
			registroE233.setStcofins(cstcofins != null ? cstcofins.getCdnfe() : null);
			registroE233.setData_apropriacao(entregadocumento.getDtemissao());
			registroE233.setValor_pis_vinculado_receita_exportacao(null);
			registroE233.setValor_pis_vinculado_receita_tributada_mercado_interno(null);
			registroE233.setValor_pis_vinculado_receita_nao_tributada_mercado_interno(null);
			registroE233.setValor_cofins_vinculado_receita_exportacao(null);
			registroE233.setValor_cofins_vinculado_receita_tributada_mercado_interno(null);
			registroE233.setValor_cofins_vinculado_receita_nao_tributada_mercado_interno(null);
			registroE233.setConta_analitica_contabil_pis(null);
			registroE233.setConta_analitica_contabil_cofins(null);
			registroE233.setDescricao_complementar_documento_operacao_pis(null);
			registroE233.setDescricao_complementar_documento_operacao_cofins(null);
			registroE233.setTipo_incidencia_pis_cofins("");
			registroE233.setNumero_coo(registroE222.getNumero_coo());
			
			return registroE233;
		} else {
//			COMENTADO POR MOTIVO DO ERRO ABAIXO:
//			A EFD PIS/COFINS, aceita valores de cr�ditos apenas para as situa��es tribut�rias 50, 51, 53, 54, 55, 56, 60, 61, 62, 63, 64, 65 ou 66. 
//			Utilizando uma situa��o diferente dos descritos, ocorrer�o erros na valida��o do arquivo da EFD PIS/COFINS.
//			
//			RegistroE233 registroE233 = new RegistroE233();
//			registroE233.setNome_registro(RegistroE233.NOME_REGISTRO);
//			registroE233.setEntradas_saidas("E");
//			registroE233.setEspecie_nf(registroE222.getEspecie_nf());
//			registroE233.setSerie_nf(registroE222.getSerie_nf());
//			registroE233.setSubserie_nf(registroE222.getSubserie_nf());
//			registroE233.setNumero_nf(registroE222.getNumero_nf());
//			registroE233.setCodigo_cliente_fornecedor(registroE222.getCodigo_cliente_fornecedor());
//			registroE233.setNumero_ordem_item(registroE222.getNumero_item());
//			registroE233.setStpis(cstpis != null ? cstpis.getCdnfe() : null);
//			registroE233.setStcofins(cstcofins != null ? cstcofins.getCdnfe() : null);
//			registroE233.setTipo_incidencia_pis_cofins("");
//			registroE233.setNumero_coo(registroE222.getNumero_coo());
//			
//			return registroE233;
			return null;
		}
	}
	
	private List<RegistroE330> createRegistroE330(RegistroE200 registroE200, Notafiscalproduto nfp) {
		List<RegistroE330> listaRegistroE330 = new ArrayList<RegistroE330>();
		
		RegistroE330 registroE330 = new RegistroE330();
		registroE330.setNome_registro(RegistroE330.NOME_REGISTRO);
		registroE330.setTipo_nf(registroE200.getEntradas_saidas());
		registroE330.setEspecie_nf(registroE200.getEspecie_nf());
		registroE330.setSerie_nf(registroE200.getSerie_nf());
		registroE330.setSubserie_nf(registroE200.getSubserie_nf());
		registroE330.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE330.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE330.setCodigo("NF-E99");
		
		StringBuilder infoComplementar = new StringBuilder();
		if(StringUtils.isNotBlank(nfp.getInfoadicionalcontrib())){
			infoComplementar.append(nfp.getInfoadicionalcontrib()).append(" ");
		}
		if(StringUtils.isNotBlank(nfp.getInfoadicionalfisco())){
			infoComplementar.append(nfp.getInfoadicionalfisco()).append(" ");
		}
		registroE330.setComplemento_descricao(infoComplementar.toString().replaceAll("\r\n", " ").replaceAll("\n", ""));
		registroE330.setControle_sistema(0);
		
		listaRegistroE330.add(registroE330);
		return listaRegistroE330;
	}
	
	private List<RegistroE330> createRegistroE330(RegistroE200 registroE200, Entregadocumento ed) {
		List<RegistroE330> listaRegistroE330 = new ArrayList<RegistroE330>();
		RegistroE330 registroE330 = new RegistroE330();
		registroE330.setNome_registro(RegistroE330.NOME_REGISTRO);
		registroE330.setTipo_nf(registroE200.getEntradas_saidas());
		registroE330.setEspecie_nf(registroE200.getEspecie_nf());
		registroE330.setSerie_nf(registroE200.getSerie_nf());
		registroE330.setSubserie_nf(registroE200.getSubserie_nf());
		registroE330.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE330.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE330.setCodigo("NF-E99");
		StringBuilder infoComplementar = new StringBuilder();
		if(StringUtils.isNotBlank(ed.getInfoadicionalcontrib())){
			infoComplementar.append(ed.getInfoadicionalcontrib()).append(" ");
		}
		if(StringUtils.isNotBlank(ed.getInfoadicionalfisco())){
			infoComplementar.append(ed.getInfoadicionalfisco()).append(" ");
		}
		registroE330.setComplemento_descricao(infoComplementar.toString());
		registroE330.setControle_sistema(0);
		
		listaRegistroE330.add(registroE330);
		return listaRegistroE330;
	}
	
	private List<RegistroE332> createRegistroE332(RegistroE200 registroE200, Entregadocumento ed) {
		List<RegistroE332> listaRegistroE332 = new ArrayList<RegistroE332>();
		if(SinedUtil.isListNotEmpty(ed.getListaEntregadocumentoreferenciado())){
			for(Entregadocumentoreferenciado entregadocumentoreferenciado : ed.getListaEntregadocumentoreferenciado()){
				RegistroE332 registroE332 = new RegistroE332();
				registroE332.setNome_registro(RegistroE332.NOME_REGISTRO);
				registroE332.setEspecie_nf(registroE200.getEspecie_nf());
				registroE332.setSerie_nf(registroE200.getSerie_nf());
				registroE332.setSubserie_nf(registroE200.getSubserie_nf());
				registroE332.setNumero_nf(registroE200.getNumero_inicial_nf());
				registroE332.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
				registroE332.setCodigo_inf_complementar(registroE200.getCodigo_info_complementar_observacao());
				
				String documento_arrecadacao = null;
				if(ModelodocumentoEnum.DAE.equals(entregadocumentoreferenciado.getModelodocumento())){
					documento_arrecadacao = "0";
				}else if(ModelodocumentoEnum.GNRE.equals(entregadocumentoreferenciado.getModelodocumento())){
					documento_arrecadacao = "1";
				} 
				registroE332.setDocumento_arrecadacao(documento_arrecadacao);
				registroE332.setUf_beneficiaria_recolhimento(entregadocumentoreferenciado.getUf() != null ? entregadocumentoreferenciado.getUf().getSigla() : null);
				registroE332.setNumero_documento_arrecadacao(entregadocumentoreferenciado.getNumero());
				registroE332.setCodigo_autenticacao_bancaria(entregadocumentoreferenciado.getAutenticacaobancaria());
				registroE332.setValor_documento_arrecadacao(entregadocumentoreferenciado.getValor() != null ? entregadocumentoreferenciado.getValor().getValue().doubleValue() : null);
				registroE332.setData_vencimento(entregadocumentoreferenciado.getDtvencimento());
				registroE332.setData_pagamento(entregadocumentoreferenciado.getDtpagamento());
				registroE332.setControle_sistema(0);
				
				listaRegistroE332.add(registroE332);
			}
			
		}
		return listaRegistroE332;
	}
	
	private List<RegistroE333> createRegistroE333(SagefiscalFiltro filtro, RegistroE200 registroE200, Notafiscalproduto nfp) {
		List<RegistroE333> listaRegistroE333 = new ArrayList<RegistroE333>();
		if(SinedUtil.isListNotEmpty(nfp.getListaReferenciada())){
			RegistroE333 registroE333;
			for(Notafiscalprodutoreferenciada notafiscalprodutoreferenciada : nfp.getListaReferenciada()){
				registroE333 = new RegistroE333();
				registroE333.setNome_registro(RegistroE333.NOME_REGISTRO);
				registroE333.setTipo_nf(registroE200.getEntradas_saidas());
				registroE333.setEspecie_nf(registroE200.getEspecie_nf());
				registroE333.setSerie_nf(registroE200.getSerie_nf());
				registroE333.setSubserie_nf(registroE200.getSubserie_nf());
				registroE333.setNumero_nf(registroE200.getNumero_inicial_nf());
				registroE333.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
				registroE333.setCodigo_inf_complementar("NF-E99");
				registroE333.setTipo_nf_referenciada("0");
				
				Tiponotareferencia tiponotareferencia = notafiscalprodutoreferenciada.getTiponotareferencia();
				if(tiponotareferencia.equals(Tiponotareferencia.NF_E) || tiponotareferencia.equals(Tiponotareferencia.CT_E)){
					String chaveacesso = notafiscalprodutoreferenciada.getChaveacesso();
					
					if(chaveacesso != null && chaveacesso.length() == 44){
						Integer cdpessoa = null;
						String cpfcnpj = null;
						String modelo = null;
						String serie = null;
						String subserie = null;
						String numero = null;
						Date dataemissao = null;
						boolean fornecedor = false;
						boolean emissaopropria = true;
						
						List<Entregadocumento> listaEntradafiscal = entradafiscalService.findByChaveacesso(chaveacesso);
						List<Notafiscalproduto> listaNotafiscalproduto = notafiscalprodutoService.findByChaveacesso(chaveacesso);
						if(listaEntradafiscal != null && listaEntradafiscal.size() > 0){
							Entregadocumento entregadocumento = listaEntradafiscal.get(0);
							
							cdpessoa = entregadocumento.getFornecedor() != null ? entregadocumento.getFornecedor().getCdpessoa() : null;
							cpfcnpj = entregadocumento.getFornecedor() != null ? entregadocumento.getFornecedor().getCpfOuCnpjValue() : null;
							modelo = entregadocumento.getModelodocumentofiscal() != null ? entregadocumento.getModelodocumentofiscal().getCodigo() : null;
							serie = entregadocumento.getSerie();
							subserie = entregadocumento.getSubserie();
							numero = entregadocumento.getNumero();
							dataemissao = entregadocumento.getDtemissao();
							fornecedor = true;
							emissaopropria = false;
						} else if(listaNotafiscalproduto != null && listaNotafiscalproduto.size() > 0){
							Notafiscalproduto notafiscalproduto = listaNotafiscalproduto.get(0);
							
							cdpessoa = notafiscalproduto.getCliente() != null ? notafiscalproduto.getCliente().getCdpessoa() : null;
							cpfcnpj = notafiscalproduto.getCliente() != null ? notafiscalproduto.getCliente().getCpfOuCnpjValue() : null;
							modelo = "55";
							serie = notafiscalproduto.getSerienfe() != null ? notafiscalproduto.getSerienfe() + "" : "1";
							subserie = "000";
							numero = notafiscalproduto.getNumero();
							dataemissao = notafiscalproduto.getDtEmissao();
							fornecedor = false;
							emissaopropria = true;
						} else {
							// TODO AQUI FAZER O DESTRINCHAMENTO DA CHAVE DE ACESSO
							
//							cUF - C�digo da UF do emitente do Documento Fiscal;
//							AAMM - Ano e M�s de emiss�o da NF-e;
//							CNPJ - CNPJ do emitente;
//							mod - Modelo do Documento Fiscal;
//							serie - S�rie do Documento Fiscal;
//							nNF - N�mero do Documento Fiscal;
//							tpEmis � forma de emiss�o da NF-e;
//							cNF - C�digo Num�rico que comp�e a Chave de Acesso;
//							cDV - D�gito Verificador da Chave de Acesso.
							
//							String chaveacesso_cUF = chaveacesso.substring(0, 2);
//							String chaveacesso_AAMM = null;
//							String chaveacesso_CNPJ = null;
//							String chaveacesso_mod = null;
//							String chaveacesso_serie = null;
//							String chaveacesso_nNF = null;
//							String chaveacesso_tpEmis = null;
//							String chaveacesso_cNF = null;
//							String chaveacesso_cDV = null;
						}
						
						registroE333.setEmitente_nf_referenciada(emissaopropria ? "0" : "1");
						if(fornecedor){
							registroE333.setCodigo_cliente_fornecedor_nf_referenciada(cdpessoa != null ? (parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? filtro.getApoio().addFornecedor(cdpessoa, null) : filtro.getApoio().addFornecedor(cdpessoa, cpfcnpj)) : null);
						} else {
							registroE333.setCodigo_cliente_fornecedor_nf_referenciada(cdpessoa != null ? (parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? filtro.getApoio().addCliente(cdpessoa, null) : filtro.getApoio().addCliente(cdpessoa, cpfcnpj)) : null);
						}
						registroE333.setModelo_nf_referenciada(modelo);
						registroE333.setSerie_nf_referenciada(serie);
						try {
							registroE333.setSubserie_nf_referenciada(subserie != null ? Integer.parseInt(subserie) : 0);
						} catch (Exception e) {
							registroE333.setSubserie_nf_referenciada(0);
						}
						try {
							registroE333.setNumero_nf_referenciada(numero != null ? Integer.parseInt(numero) : 0);
						} catch (Exception e) {
							registroE333.setNumero_nf_referenciada(0);
						}
						registroE333.setData_emissao_nf_referenciada(dataemissao);
					}
					
				}
				
				registroE333.setControle_sistema(0);
				
				listaRegistroE333.add(registroE333);
			}
			
		}
		return listaRegistroE333;
	}
	
	private List<RegistroE333> createRegistroE333(SagefiscalFiltro filtro, RegistroE200 registroE200, Entregadocumento ed) {
		List<RegistroE333> listaRegistroE333 = new ArrayList<RegistroE333>();
		if(SinedUtil.isListNotEmpty(ed.getListaEntregadocumentofrete())){
			RegistroE333 registroE333;
			for(Entregadocumentofrete entregadocumentofrete : ed.getListaEntregadocumentofrete()){
				if(entregadocumentofrete.getEntregadocumentovinculo() != null){
					registroE333 = new RegistroE333();
					registroE333.setNome_registro(RegistroE333.NOME_REGISTRO);
					registroE333.setEspecie_nf(registroE200.getEspecie_nf());
					registroE333.setSerie_nf(registroE200.getSerie_nf());
					registroE333.setSubserie_nf(registroE200.getSubserie_nf());
					registroE333.setNumero_nf(registroE200.getNumero_inicial_nf());
					registroE333.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
					registroE333.setCodigo_inf_complementar(registroE200.getCodigo_info_complementar_observacao());
					registroE333.setTipo_nf_referenciada("1");
					registroE333.setCodigo_cliente_fornecedor_nf_referenciada(entregadocumentofrete.getEntregadocumentovinculo().getFornecedor() != null ? (parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? filtro.getApoio().addFornecedor(entregadocumentofrete.getEntregadocumentovinculo().getFornecedor().getCdpessoa(), null) : filtro.getApoio().addFornecedor(entregadocumentofrete.getEntregadocumentovinculo().getFornecedor().getCdpessoa(), entregadocumentofrete.getEntregadocumentovinculo().getFornecedor().getCpfOuCnpjValue())) : null);
					registroE333.setModelo_nf_referenciada(entregadocumentofrete.getEntregadocumentovinculo().getModelodocumentofiscal() != null ? entregadocumentofrete.getEntregadocumentovinculo().getModelodocumentofiscal().getCodigo() : null);
					registroE333.setSerie_nf_referenciada(entregadocumentofrete.getEntregadocumentovinculo().getSerie());
					try {
						registroE333.setSubserie_nf_referenciada(entregadocumentofrete.getEntregadocumentovinculo().getSubserie() != null ? Integer.parseInt(entregadocumentofrete.getEntregadocumentovinculo().getSubserie()) : 0);
					} catch (Exception e) {
						registroE333.setSubserie_nf_referenciada(0);
					}
					try {
						registroE333.setNumero_nf_referenciada(entregadocumentofrete.getEntregadocumentovinculo().getNumero() != null ? Integer.parseInt(entregadocumentofrete.getEntregadocumentovinculo().getNumero()) : 0);
					} catch (Exception e) {
						registroE333.setNumero_nf_referenciada(0);
					}
					registroE333.setData_emissao_nf_referenciada(entregadocumentofrete.getEntregadocumentovinculo().getDtemissao());
					registroE333.setControle_sistema(0);
					
					listaRegistroE333.add(registroE333);
				}
			}
			
		}
		return listaRegistroE333;
	}
	
	private RegistroE336 createRegistroE336(RegistroE200 registroE200, Entregadocumento entregadocumento) {
		RegistroE336 registroE336 = new RegistroE336();
		registroE336.setNome_registro(RegistroE336.NOME_REGISTRO);
		registroE336.setTipo_nf("E");
		registroE336.setEspecie_nf(registroE200.getEspecie_nf());
		registroE336.setSerie_nf(registroE200.getSerie_nf());
		registroE336.setSubserie_nf(registroE200.getSubserie_nf());
		registroE336.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE336.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE336.setValor_total_desconto(entregadocumento.getValordesconto() != null ? entregadocumento.getValordesconto().getValue().doubleValue() : null);
		registroE336.setAbatimento_nao_tributado_nao_comercial(null);
		registroE336.setValor_frete(entregadocumento.getValorfrete() != null ? entregadocumento.getValorfrete().getValue().doubleValue() : null);
		registroE336.setValor_seguro(entregadocumento.getValorseguro() != null ? entregadocumento.getValorseguro().getValue().doubleValue() : null);
		registroE336.setValor_outras_despesas_acessorias(entregadocumento.getValoroutrasdespesas() != null ? entregadocumento.getValoroutrasdespesas().getValue().doubleValue() : null);
		registroE336.setBase_pis(entregadocumento.getValortotalbcpis() != null ? entregadocumento.getValortotalbcpis().getValue().doubleValue() : null);
		registroE336.setAliquota_pis(entregadocumento.getValortotalpis() != null && entregadocumento.getValortotalpis().getValue().doubleValue() > 0 ? entregadocumento.getValortotalbcpis().divide(entregadocumento.getValortotalpis()).getValue().doubleValue() : null);
		registroE336.setValor_pis(entregadocumento.getValortotalpis() != null ? entregadocumento.getValortotalpis().getValue().doubleValue() : null);
		registroE336.setBase_cofins(entregadocumento.getValortotalbccofins() != null ? entregadocumento.getValortotalbccofins().getValue().doubleValue() : null);
		registroE336.setAliquota_cofins(entregadocumento.getValortotalcofins() != null && entregadocumento.getValortotalcofins().getValue().doubleValue() > 0 ? entregadocumento.getValortotalbccofins().divide(entregadocumento.getValortotalcofins()).getValue().doubleValue() : null);
		registroE336.setValor_cofins(entregadocumento.getValortotalcofins() != null ? entregadocumento.getValortotalcofins().getValue().doubleValue() : null);
		registroE336.setCi_pis_cofins(null);
		registroE336.setControle_sistema(0);
		
		return registroE336;
	}
	
	private RegistroE336 createRegistroE336(RegistroE200 registroE200, Notafiscalproduto nota) {
		boolean cancelada = NotaStatus.CANCELADA.equals(nota.getNotaStatus());
		
		RegistroE336 registroE336 = new RegistroE336();
		registroE336.setNome_registro(RegistroE336.NOME_REGISTRO);
		registroE336.setTipo_nf(Tipooperacaonota.ENTRADA.equals(nota.getTipooperacaonota()) ? "E" : "S");
		registroE336.setEspecie_nf(registroE200.getEspecie_nf());
		registroE336.setSerie_nf(registroE200.getSerie_nf());
		registroE336.setSubserie_nf(registroE200.getSubserie_nf());
		registroE336.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE336.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		if(!cancelada){
			registroE336.setValor_total_desconto(nota.getValordesconto() != null ? nota.getValordesconto().getValue().doubleValue() : null);
			registroE336.setAbatimento_nao_tributado_nao_comercial(null);
			registroE336.setValor_frete(nota.getValorfrete() != null ? nota.getValorfrete().getValue().doubleValue() : null);
			registroE336.setValor_seguro(nota.getValorseguro() != null ? nota.getValorseguro().getValue().doubleValue() : null);
			registroE336.setValor_outras_despesas_acessorias(null);
			registroE336.setBase_pis(null);
			registroE336.setAliquota_pis(null);
			registroE336.setValor_pis(nota.getValorpis() != null ? nota.getValorpis().getValue().doubleValue() : null);
			registroE336.setBase_cofins(null);
			registroE336.setAliquota_cofins(null);
			registroE336.setValor_cofins(nota.getValorcofins() != null ? nota.getValorcofins().getValue().doubleValue() : null);
			registroE336.setCi_pis_cofins(null);
		}
		registroE336.setControle_sistema(0);
		
		return registroE336;
	}
	
	private List<RegistroE400> createRegistroE400(SagefiscalFiltro filtro) {
		List<RegistroE400> listaRegistroE400 = new ArrayList<RegistroE400>();
		if(filtro.getInventario() != null){
			Inventario inventario = inventarioService.findForSagefiscal(filtro);
			if(inventario != null && SinedUtil.isListNotEmpty(inventario.getListaInventariomaterial())){
				RegistroE400 registroE400;
				for(Inventariomaterial item : inventario.getListaInventariomaterial()){
					registroE400 = new RegistroE400();
					registroE400.setNome_registro(RegistroE400.NOME_REGISTRO);
					registroE400.setCodigo_produto(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), null, null) : null);
					registroE400.setData_inventario(inventario.getDtinventario());
					registroE400.setValor_bruto(null);
					registroE400.setMarg_lucro(null);
					registroE400.setValor_unitario(item.getValorUnitario() != null ? item.getValorUnitario() : null);
					registroE400.setQuantidade(item.getQtde());
					registroE400.setValor_mercadoria(item.getValorUnitario() != null && item.getQtde() != null ? item.getValorUnitario() * item.getQtde() : null);
					registroE400.setAliq_icms(null);
					registroE400.setAliq_interna_met_anual(null);
					registroE400.setSubstituicao_tributaria(null);
					registroE400.setConta_analitica_contabil(StringUtils.isNotBlank(item.getContaanaliticacontabil()) ? item.getContaanaliticacontabil() : inventario.getCodigocontaanalitica());
					registroE400.setCodigo_posse_mercadorias(item.getIndicadorpropriedade() != null ? item.getIndicadorpropriedade().getCdSIntegra() : null);
					registroE400.setDescricao_complementar(null);
					registroE400.setControle_sistema(0);
					listaRegistroE400.add(registroE400);
				}
			}
		}
		return listaRegistroE400;
	}
	
	private List<RegistroE201> createRegistroE201(RegistroE200 registroE200, Notafiscalproduto nfp, boolean inutilizada) {
		List<RegistroE201> listaRegistroE201 = new ArrayList<RegistroE201>();
		RegistroE201 registroE201;
		RegistroE201 registroE201OutrasIcms;
		RegistroE201 registroE201OutrasIpi;
		Integer idx;
		boolean cancelada = NotaStatus.CANCELADA.equals(nfp.getNotaStatus());
		boolean saida = Tipooperacaonota.SAIDA.equals(nfp.getTipooperacaonota());
		boolean contribuinteICMS = nfp.getCliente().getContribuinteICMS() != null ? nfp.getCliente().getContribuinteICMS() : false;
		boolean addRegistro = true;
		boolean addRegistroOutrasIcms = true;
		boolean addRegistroOutrasIpi = true;
		Integer sequencial = 1;
		
		Integer tipo_pagamento = 0;
		if(Formapagamentonfe.A_VISTA.equals(nfp.getFormapagamentonfe())){
			tipo_pagamento = 1;
		}else if(Formapagamentonfe.A_PRAZO.equals(nfp.getFormapagamentonfe())){
			tipo_pagamento = 2;
		}
		
		if((nfp.getListaItens() == null || nfp.getListaItens().size() == 0) && cancelada){
			registroE201 = new RegistroE201(null, null, null, null);
			registroE201.setNome_registro(RegistroE201.NOME_REGISTRO);
			registroE201.setEntradas_saidas(registroE200.getEntradas_saidas());
			registroE201.setEspecie_nf(registroE200.getEspecie_nf());
			registroE201.setSerie_nf(registroE200.getSerie_nf());
			registroE201.setSubserie_nf(registroE200.getSubserie_nf());
			registroE201.setNumero_nf(registroE200.getNumero_inicial_nf());
			registroE201.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
			registroE201.setNumero_sequencial_lancamento(sequencial);
			if(!inutilizada){
				registroE201.setContribuinte_icms((contribuinteICMS || Tipooperacaonota.ENTRADA.equals(nfp.getTipooperacaonota())) ? "S" : "N");
				registroE201.setVenda_entrega_faturada("S");
				registroE201.setSit_trib_ipi_ctipi(Tipocobrancaipi.SAIDA_OUTRAS.getCdnfe());
			}
			listaRegistroE201.add(registroE201);
			sequencial++;
		} else {
			for(Notafiscalprodutoitem item : nfp.getListaItens()){
				String codigoCFOP = item.getCfop() != null ? item.getCfop().getCodigo() : null; 
				if(inutilizada){
					codigoCFOP = null;
				}
				if(cancelada && saida && codigoCFOP != null){
					if(codigoCFOP.startsWith("1")){
						codigoCFOP = "5" + codigoCFOP.substring(1);
					}
					if(codigoCFOP.startsWith("2")){
						codigoCFOP = "6" + codigoCFOP.substring(1);
					}
					if(codigoCFOP.startsWith("3")){
						codigoCFOP = "7" + codigoCFOP.substring(1);
					}
				}
				
				Double icms = item.getIcms();
				ValorFiscal valorFiscalIcms = item.getValorFiscalIcms();
				ValorFiscal valorFiscalIpi = item.getValorFiscalIpi();
				Tipocobrancaipi tipocobrancaipi = item.getTipocobrancaipi();
				
				if(item.getGrupotributacao() != null){
					if(valorFiscalIcms == null){
						valorFiscalIcms = item.getGrupotributacao().getValorFiscalIcms();
					}
					if(valorFiscalIpi == null){
						valorFiscalIpi = item.getGrupotributacao().getValorFiscalIpi();
					}
					if(tipocobrancaipi == null){
						tipocobrancaipi = item.getGrupotributacao().getTipocobrancaipi();
					}
				}
				
				if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(valorFiscalIcms) || 
					ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(valorFiscalIcms)){
					icms = null;
				}
				if(inutilizada){
					icms = null;
				}
				
				Integer cfop = codigoCFOP != null ? Integer.parseInt(codigoCFOP) : null;
				registroE201 = new RegistroE201(cfop, icms, valorFiscalIcms != null ? valorFiscalIcms.ordinal() : null, valorFiscalIpi != null ? valorFiscalIpi.ordinal() : null);
				
				addRegistro = true;
				if(listaRegistroE201.contains(registroE201)){
					idx = listaRegistroE201.indexOf(registroE201);
					registroE201 = listaRegistroE201.get(idx);
					addRegistro = false;
				}
				
				registroE201.setNome_registro(RegistroE201.NOME_REGISTRO);
				registroE201.setEntradas_saidas(registroE200.getEntradas_saidas());
				registroE201.setEspecie_nf(registroE200.getEspecie_nf());
				registroE201.setSerie_nf(registroE200.getSerie_nf());
				registroE201.setSubserie_nf(registroE200.getSubserie_nf());
				registroE201.setNumero_nf(registroE200.getNumero_inicial_nf());
				registroE201.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
				
				Money valorbruto = item.getValorbruto();
				if(item.getValordesconto() != null) valorbruto = valorbruto.subtract(item.getValordesconto());
				if(item.getValorfrete() != null) valorbruto = valorbruto.add(item.getValorfrete());
				if(item.getOutrasdespesas() != null) valorbruto = valorbruto.add(item.getOutrasdespesas());
				
				if(!cancelada) {
					registroE201.setTipo_pagamento(tipo_pagamento);
					
					if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIcms)){
						registroE201.addBc_icms(item.getValorbcicms());
						registroE201.addValor_icms(item.getValoricms());
						registroE201.addReducao_base_cal_icms(item.getReducaobcicms());
						
						Double valorbcicms = item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : 0d;
						Double valor = valorbruto != null ? valorbruto.getValue().doubleValue() : 0d;
						double valoroutrasicms = valor - valorbcicms;
						if(valoroutrasicms > 0){
							registroE201OutrasIcms = new RegistroE201(cfop, null, ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.ordinal(), null);
							
							addRegistroOutrasIcms = true;
							if(listaRegistroE201.contains(registroE201OutrasIcms)){
								idx = listaRegistroE201.indexOf(registroE201OutrasIcms);
								registroE201OutrasIcms = listaRegistroE201.get(idx);
								addRegistroOutrasIcms = false;
							}
							
							registroE201OutrasIcms.setNome_registro(RegistroE201.NOME_REGISTRO);
							registroE201OutrasIcms.setEntradas_saidas(registroE200.getEntradas_saidas());
							registroE201OutrasIcms.setEspecie_nf(registroE200.getEspecie_nf());
							registroE201OutrasIcms.setSerie_nf(registroE200.getSerie_nf());
							registroE201OutrasIcms.setSubserie_nf(registroE200.getSubserie_nf());
							registroE201OutrasIcms.setNumero_nf(registroE200.getNumero_inicial_nf());
							registroE201OutrasIcms.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
							registroE201OutrasIcms.setTipo_pagamento(tipo_pagamento);
							registroE201OutrasIcms.addOutras_icms(new Money(valoroutrasicms));
							registroE201OutrasIcms.setContribuinte_icms(contribuinteICMS || Tipooperacaonota.ENTRADA.equals(nfp.getTipooperacaonota()) ? "S" : "N");
							registroE201OutrasIcms.setVenda_entrega_faturada(getVendaEntregaFaturada(item.getCfop()));
							registroE201OutrasIcms.setSit_trib_ipi_ctipi(tipocobrancaipi != null ? tipocobrancaipi.getCdnfe() : null);
							registroE201OutrasIcms.setControle_sistema(0);
							
							if(addRegistroOutrasIcms){
								registroE201OutrasIcms.setNumero_sequencial_lancamento(sequencial);
								listaRegistroE201.add(registroE201OutrasIcms);
								sequencial++;
							}
						}
					}
					if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(valorFiscalIcms)){
						registroE201.addIsento_icms(valorbruto);
						if(!ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi) && item.getValoripi() != null && item.getValoripi().getValue().doubleValue() > 0){
							registroE201.addIsento_icms(item.getValoripi());
						}
					}
					if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(valorFiscalIcms)){
						registroE201.addOutras_icms(valorbruto);
						if(!ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi) && item.getValoripi() != null && item.getValoripi().getValue().doubleValue() > 0){
							registroE201.addOutras_icms(item.getValoripi());
						}
					}
					if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi)){
						registroE201.addBc_ipi(item.getValorbcipi());
						registroE201.addValor_ipi(item.getValoripi(), !saida);
						
						Double valorbcipi = item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0d;
						Double valor = valorbruto != null ? valorbruto.getValue().doubleValue() : 0d;
						double valoroutrasipi = valor - valorbcipi;
						if(valoroutrasipi > 0){
							registroE201OutrasIpi = new RegistroE201(cfop, null, null, ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.ordinal());
							
							addRegistroOutrasIpi = true;
							if(listaRegistroE201.contains(registroE201OutrasIpi)){
								idx = listaRegistroE201.indexOf(registroE201OutrasIpi);
								registroE201OutrasIpi = listaRegistroE201.get(idx);
								addRegistroOutrasIpi = false;
							}
							
							registroE201OutrasIpi.setNome_registro(RegistroE201.NOME_REGISTRO);
							registroE201OutrasIpi.setEntradas_saidas(registroE200.getEntradas_saidas());
							registroE201OutrasIpi.setEspecie_nf(registroE200.getEspecie_nf());
							registroE201OutrasIpi.setSerie_nf(registroE200.getSerie_nf());
							registroE201OutrasIpi.setSubserie_nf(registroE200.getSubserie_nf());
							registroE201OutrasIpi.setNumero_nf(registroE200.getNumero_inicial_nf());
							registroE201OutrasIpi.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
							registroE201OutrasIpi.setTipo_pagamento(tipo_pagamento);
							registroE201OutrasIpi.addOutras_ipi(new Money(valoroutrasipi));
							registroE201OutrasIpi.setContribuinte_icms(contribuinteICMS || Tipooperacaonota.ENTRADA.equals(nfp.getTipooperacaonota()) ? "S" : "N");
							registroE201OutrasIpi.setVenda_entrega_faturada(getVendaEntregaFaturada(item.getCfop()));
							registroE201OutrasIpi.setSit_trib_ipi_ctipi(tipocobrancaipi != null ? tipocobrancaipi.getCdnfe() : null);
							registroE201OutrasIpi.setControle_sistema(0);
							
							if(addRegistroOutrasIpi){
								registroE201OutrasIpi.setNumero_sequencial_lancamento(sequencial);
								listaRegistroE201.add(registroE201OutrasIpi);
								sequencial++;
							}
						}
					}
					if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(valorFiscalIpi)){
						registroE201.addIsento_ipi(valorbruto);
						if(item.getValoripi() != null && item.getValoripi().getValue().doubleValue() > 0){
							registroE201.addIsento_ipi(item.getValoripi());
						}
					}
					if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(valorFiscalIpi)){
						registroE201.addOutras_ipi(valorbruto);
						if(item.getValoripi() != null && item.getValoripi().getValue().doubleValue() > 0){
							registroE201.addOutras_ipi(item.getValoripi());
						}
					}
					
					registroE201.addIcms_subst_trib(item.getValoricmsst());
					registroE201.addBase_subst_trib(item.getValorbcicmsst());
					if(item.getValoricmsst() != null && item.getValoricmsst().getValue().doubleValue() > 0){
						registroE201.setTipo_antecipacao_tributaria("0");
					}
				}
				if(!inutilizada){
					registroE201.setVenda_entrega_faturada(getVendaEntregaFaturada(item.getCfop()));
					registroE201.setContribuinte_icms(contribuinteICMS || Tipooperacaonota.ENTRADA.equals(nfp.getTipooperacaonota()) ? "S" : "N");
					registroE201.setSit_trib_ipi_ctipi(tipocobrancaipi != null ? tipocobrancaipi.getCdnfe() : null);
				}
				registroE201.setControle_sistema(0);
				
				if(addRegistro){
					registroE201.setNumero_sequencial_lancamento(sequencial);
					listaRegistroE201.add(registroE201);
					sequencial++;
				}
				
				boolean cstcofinsCredito = Tipocobrancacofins.isCreditoCofins(item.getTipocobrancacofins());
				boolean cstcpisCredito = Tipocobrancapis.isCreditoPis(item.getTipocobrancapis());
				if(cstcofinsCredito || cstcpisCredito){
					registroE200.addPisCofins(registroE201, valorbruto.getValue().doubleValue(), item.getPis(), item.getCofins(), item.getCfop() != null ? item.getCfop().getCodigo() : null);
				}
			}
		}
		
		return listaRegistroE201;
	}
	
	private List<RegistroE201> createRegistroE201(RegistroE200 registroE200, Entregadocumento ed) {
		List<RegistroE201> listaRegistroE201 = new ArrayList<RegistroE201>();
		RegistroE201 registroE201;
		RegistroE201 registroE201OutrasIcms;
		RegistroE201 registroE201OutrasIpi;
		Integer idx;
		boolean contribuinteICMS = true;
		boolean addRegistro = true;
		boolean addRegistroOutrasIcms = true;
		boolean addRegistroOutrasIpi = true;
		Integer sequencial = 1;
		
		Integer tipo_pagamento = 0;
		if(Formapagamentonfe.A_VISTA.equals(ed.getIndpag())){
			tipo_pagamento = 1;
		}else if(Formapagamentonfe.A_PRAZO.equals(ed.getIndpag())){
			tipo_pagamento = 2;
		}
		
		for(Entregamaterial item : ed.getListaEntregamaterial()){
			Double icms = item.getIcms();
			ValorFiscal valorFiscalIcms = item.getValorFiscalIcms();
			ValorFiscal valorFiscalIpi = item.getValorFiscalIpi();
			Tipocobrancaipi cstipi = item.getCstipi();
			
			if(item.getGrupotributacao() != null){
				if(valorFiscalIcms == null){
					valorFiscalIcms = item.getGrupotributacao().getValorFiscalIcms();
				}
				if(valorFiscalIpi == null){
					valorFiscalIpi = item.getGrupotributacao().getValorFiscalIpi();
				}
				if(cstipi == null){
					cstipi = item.getGrupotributacao().getTipocobrancaipi();
				}
			}
			
			if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(valorFiscalIcms) || 
				ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(valorFiscalIcms)){
				icms = null;
			}
			
			if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi) && item.getValoripi().getValue().doubleValue() <= 0){
				valorFiscalIpi = ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS;
			}
			
			registroE201 = new RegistroE201(item.getCfop() != null ? Integer.parseInt(item.getCfop().getCodigo()) : null, icms, valorFiscalIcms != null ? valorFiscalIcms.ordinal() : null, valorFiscalIpi != null ? valorFiscalIpi.ordinal() : null);
			
			addRegistro = true;
			if(listaRegistroE201.contains(registroE201)){
				idx = listaRegistroE201.indexOf(registroE201);
				registroE201 = listaRegistroE201.get(idx);
				addRegistro = false;
			}
			
			registroE201.setNome_registro(RegistroE201.NOME_REGISTRO);
			registroE201.setEntradas_saidas(registroE200.getEntradas_saidas());
			registroE201.setEspecie_nf(registroE200.getEspecie_nf());
			registroE201.setSerie_nf(registroE200.getSerie_nf());
			registroE201.setSubserie_nf(registroE200.getSubserie_nf());
			registroE201.setNumero_nf(registroE200.getNumero_inicial_nf());
			registroE201.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
			registroE201.setTipo_pagamento(tipo_pagamento);
			
			Double valortotaloperacaoIcms = item.getValortotaloperacao(!ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi));
			if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIcms)){
				registroE201.addBc_icms(item.getValorbcicms());
				registroE201.addValor_icms(item.getValoricms());
				
				Double valorbcicms = item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : 0d;
				Double valor = valortotaloperacaoIcms != null ? valortotaloperacaoIcms : 0d;
				double valoroutrasicms = valor - valorbcicms;
				if(valoroutrasicms > 0){
					registroE201OutrasIcms = new RegistroE201(item.getCfop() != null ? Integer.parseInt(item.getCfop().getCodigo()) : null, null, ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.ordinal(), null);
					
					addRegistroOutrasIcms = true;
					if(listaRegistroE201.contains(registroE201OutrasIcms)){
						idx = listaRegistroE201.indexOf(registroE201OutrasIcms);
						registroE201OutrasIcms = listaRegistroE201.get(idx);
						addRegistroOutrasIcms = false;
					}
					
					registroE201OutrasIcms.setNome_registro(RegistroE201.NOME_REGISTRO);
					registroE201OutrasIcms.setEntradas_saidas(registroE200.getEntradas_saidas());
					registroE201OutrasIcms.setEspecie_nf(registroE200.getEspecie_nf());
					registroE201OutrasIcms.setSerie_nf(registroE200.getSerie_nf());
					registroE201OutrasIcms.setSubserie_nf(registroE200.getSubserie_nf());
					registroE201OutrasIcms.setNumero_nf(registroE200.getNumero_inicial_nf());
					registroE201OutrasIcms.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
					registroE201OutrasIcms.setTipo_pagamento(tipo_pagamento);
					registroE201OutrasIcms.addOutras_icms(new Money(valoroutrasicms));
					registroE201OutrasIcms.setContribuinte_icms(contribuinteICMS ? "S" : "N");
					registroE201OutrasIcms.setVenda_entrega_faturada(getVendaEntregaFaturada(item.getCfop()));
					if(registroE200.getModelo_nf().equals("57")){
						Origemproduto origemproduto = item.getMaterial().getOrigemproduto();
						Tipocobrancaicms tipocobrancaicms = item.getCsticms();
						registroE201OutrasIcms.setSit_trib_icms_tab_a(origemproduto != null ? origemproduto.getValue().toString() : null);
						registroE201OutrasIcms.setSit_trib_icms_tab_b(tipocobrancaicms != null ? tipocobrancaicms.getCdnfe() : null);
					} else {
						registroE201OutrasIcms.setSit_trib_icms_tab_a(null);
						registroE201OutrasIcms.setSit_trib_icms_tab_b(null);
					}
					registroE201OutrasIcms.setSit_trib_ipi_ctipi(cstipi != null ? cstipi.getCdnfe() : null);
					registroE201OutrasIcms.setControle_sistema(0);
					
					if(addRegistroOutrasIcms){
						registroE201OutrasIcms.setNumero_sequencial_lancamento(sequencial);
						listaRegistroE201.add(registroE201OutrasIcms);
						sequencial++;
					}
				}
			}
			if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(valorFiscalIcms)){
				registroE201.addIsento_icms(new Money(valortotaloperacaoIcms));
			}
			if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(valorFiscalIcms)){
				registroE201.addOutras_icms(new Money(valortotaloperacaoIcms));
			}
			if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi)){
				registroE201.addBc_ipi(item.getValorbcipi());
				registroE201.addValor_ipi(item.getValoripi(), true);
				
				Double valorbcipi = item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0d;
				Double valor = item.getValortotaloperacao(false) != null ? item.getValortotaloperacao(false) : 0d;
				double valoroutrasipi = valor - valorbcipi;
				if(valoroutrasipi > 0){
					registroE201OutrasIpi = new RegistroE201(item.getCfop() != null ? Integer.parseInt(item.getCfop().getCodigo()) : null, null, null, ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.ordinal());
					
					addRegistroOutrasIpi = true;
					if(listaRegistroE201.contains(registroE201OutrasIpi)){
						idx = listaRegistroE201.indexOf(registroE201OutrasIpi);
						registroE201OutrasIpi = listaRegistroE201.get(idx);
						addRegistroOutrasIpi = false;
					}
					
					registroE201OutrasIpi.setNome_registro(RegistroE201.NOME_REGISTRO);
					registroE201OutrasIpi.setEntradas_saidas(registroE200.getEntradas_saidas());
					registroE201OutrasIpi.setEspecie_nf(registroE200.getEspecie_nf());
					registroE201OutrasIpi.setSerie_nf(registroE200.getSerie_nf());
					registroE201OutrasIpi.setSubserie_nf(registroE200.getSubserie_nf());
					registroE201OutrasIpi.setNumero_nf(registroE200.getNumero_inicial_nf());
					registroE201OutrasIpi.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
					registroE201OutrasIpi.setTipo_pagamento(tipo_pagamento);
					registroE201OutrasIpi.addOutras_ipi(new Money(valoroutrasipi));
					registroE201OutrasIpi.setContribuinte_icms(contribuinteICMS ? "S" : "N");
					registroE201OutrasIpi.setVenda_entrega_faturada(getVendaEntregaFaturada(item.getCfop()));
					if(registroE200.getModelo_nf().equals("57")){
						Origemproduto origemproduto = item.getMaterial().getOrigemproduto();
						Tipocobrancaicms tipocobrancaicms = item.getCsticms();
						registroE201OutrasIpi.setSit_trib_icms_tab_a(origemproduto != null ? origemproduto.getValue().toString() : null);
						registroE201OutrasIpi.setSit_trib_icms_tab_b(tipocobrancaicms != null ? tipocobrancaicms.getCdnfe() : null);
					} else {
						registroE201OutrasIpi.setSit_trib_icms_tab_a(null);
						registroE201OutrasIpi.setSit_trib_icms_tab_b(null);
					}
					registroE201OutrasIpi.setSit_trib_ipi_ctipi(cstipi != null ? cstipi.getCdnfe() : null);
					registroE201OutrasIpi.setControle_sistema(0);
					
					if(addRegistroOutrasIpi){
						registroE201OutrasIpi.setNumero_sequencial_lancamento(sequencial);
						listaRegistroE201.add(registroE201OutrasIpi);
						sequencial++;
					}
				}
			}
			if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(valorFiscalIpi)){
				registroE201.addIsento_ipi(new Money(item.getValortotaloperacao()));
			}
			if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(valorFiscalIpi)){
				registroE201.addOutras_ipi(new Money(item.getValortotaloperacao()));
			}
			
//			registroE201.addIcms_subst_trib(item.getValoricmsst());
//			registroE201.addBase_subst_trib(item.getValorbcicmsst());
//			if(item.getValoricmsst() != null && item.getValoricmsst().getValue().doubleValue() > 0){
//				registroE201.setTipo_antecipacao_tributaria("0");
//			}
			registroE201.setContribuinte_icms(contribuinteICMS ? "S" : "N");
			registroE201.setVenda_entrega_faturada(getVendaEntregaFaturada(item.getCfop()));
			if(registroE200.getModelo_nf().equals("57")){
				Origemproduto origemproduto = item.getMaterial().getOrigemproduto();
				Tipocobrancaicms tipocobrancaicms = item.getCsticms();
				registroE201.setSit_trib_icms_tab_a(origemproduto != null ? origemproduto.getValue().toString() : null);
				registroE201.setSit_trib_icms_tab_b(tipocobrancaicms != null ? tipocobrancaicms.getCdnfe() : null);
			} else {
				registroE201.setSit_trib_icms_tab_a(null);
				registroE201.setSit_trib_icms_tab_b(null);
			}
			registroE201.setSit_trib_ipi_ctipi(cstipi != null ? cstipi.getCdnfe() : null);
			registroE201.setCiap(null);
			registroE201.setControle_sistema(0);
			
			if(addRegistro){
				registroE201.setNumero_sequencial_lancamento(sequencial);
				listaRegistroE201.add(registroE201);
				sequencial++;
			}
			
			boolean cstcofinsCredito = Tipocobrancacofins.isCreditoCofins(item.getCstcofins());
			boolean cstcpisCredito = Tipocobrancapis.isCreditoPis(item.getCstpis());
			if(cstcofinsCredito || cstcpisCredito){
				registroE200.addPisCofins(registroE201, item.getValortotalmaterial(), item.getPis(), item.getCofins(), item.getCfop() != null ? item.getCfop().getCodigo() : null);
			}
		}
		return listaRegistroE201;
	}
	
	private RegistroE207 createRegistroE207(RegistroE200 registroE200, Notafiscalproduto nfp) {
		boolean cancelada = NotaStatus.CANCELADA.equals(nfp.getNotaStatus());
		
		RegistroE207 registroE207 = new RegistroE207();
		registroE207.setNome_registro(RegistroE207.NOME_REGISTRO);
		registroE207.setTipo_nf(registroE200.getEntradas_saidas());
		registroE207.setEspecie_nf(registroE200.getEspecie_nf());
		registroE207.setSerie_nf(registroE200.getSerie_nf());
		registroE207.setSubserie_nf(registroE200.getSubserie_nf());
		registroE207.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE207.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		if(!cancelada){
			registroE207.setValor_total_rel_fundo_combate_pobreza(nfp.getValorfcpdestinatario() != null ? nfp.getValorfcpdestinatario().getValue().doubleValue() : 0d); 
			registroE207.setValor_total_icms_interestadual_destino(nfp.getValoricmsdestinatario() != null ? nfp.getValoricmsdestinatario().getValue().doubleValue() : 0d); 
			registroE207.setValor_total_icms_interestadual_remetente(nfp.getValoricmsremetente() != null ? nfp.getValoricmsremetente().getValue().doubleValue() : 0d);
		}
		
		return registroE207;
	}
	
	private RegistroE207 createRegistroE207(RegistroE200 registroE200, Entregadocumento ed) {
		RegistroE207 registroE207 = new RegistroE207();
		registroE207.setNome_registro(RegistroE207.NOME_REGISTRO);
		registroE207.setTipo_nf("E");
		registroE207.setEspecie_nf(registroE200.getEspecie_nf());
		registroE207.setSerie_nf(registroE200.getSerie_nf());
		registroE207.setSubserie_nf(registroE200.getSubserie_nf());
		registroE207.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE207.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE207.setValor_total_rel_fundo_combate_pobreza(null);
		registroE207.setValor_total_icms_interestadual_destino(null);
		registroE207.setValor_total_icms_interestadual_remetente(null);
		return registroE207;
	}
	
	private RegistroE209 createRegistroE209(RegistroE200 registroE200, Notafiscalproduto nfp) {
		boolean cancelada = NotaStatus.CANCELADA.equals(nfp.getNotaStatus());
		
		RegistroE209 registroE209 = new RegistroE209();
		registroE209.setNome_registro(RegistroE209.NOME_REGISTRO);
		registroE209.setEspecie_nf(registroE200.getEspecie_nf());
		registroE209.setSerie_nf(registroE200.getSerie_nf());
		registroE209.setSubserie_nf(registroE200.getSubserie_nf());
		registroE209.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE209.setData_compensacao(nfp.getDtEmissao());
		registroE209.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		if(!cancelada){
			registroE209.setValor_bc_retencao(nfp.getValorbccsll() != null ? nfp.getValorbccsll().getValue().doubleValue() : null);
			registroE209.setPis_retido_fonte(nfp.getValorpisretido() != null ? nfp.getValorpisretido().getValue().doubleValue() : null);
			registroE209.setCofins_retido_fonte(nfp.getValorcofinsretido() != null ? nfp.getValorcofinsretido().getValue().doubleValue() : null);
			registroE209.setCsll_retido_fonte(nfp.getValorcsll() != null ? nfp.getValorcsll().getValue().doubleValue() : null);
		}
		registroE209.setCodigo_recolhimento(null);
		registroE209.setIndicador_condicao_pessoa_juridica_declarante(null);
		registroE209.setTipo_incidencia(null);
		registroE209.setControle_sistema(0);
		
		return registroE209;
	}
		
	private RegistroE210 createRegistroE210(RegistroE200 registroE200, Notafiscalproduto nfp) {
		boolean cancelada = NotaStatus.CANCELADA.equals(nfp.getNotaStatus());
		
		RegistroE210 registroE210 = new RegistroE210();
		registroE210.setNome_registro(RegistroE210.NOME_REGISTRO);
		registroE210.setEspecie_nf(registroE200.getEspecie_nf());
		registroE210.setSerie_nf(registroE200.getSerie_nf());
		registroE210.setSubserie_nf(registroE200.getSubserie_nf());
		registroE210.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE210.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		
		if(!cancelada){
			registroE210.setBase_calculo_irrf(nfp.getValorbcirrf() != null ? nfp.getValorbcirrf().getValue().doubleValue() : null);
			registroE210.setAliquota_irrf(nfp.getValorbcirrf() != null && nfp.getValorirrf() != null && nfp.getValorirrf().getValue().doubleValue() > 0 ? nfp.getValorbcirrf().divide(nfp.getValorirrf()).getValue().doubleValue() : null);
			registroE210.setIrrf(nfp.getValorirrf() != null ? nfp.getValorirrf().getValue().doubleValue() : null);
			registroE210.setBase_calculo_seguro_social(nfp.getValorbcprevsocial() != null ? nfp.getValorbcprevsocial().getValue().doubleValue() : null);
			registroE210.setAliquota_seguro_social(nfp.getValorbcprevsocial() != null && nfp.getValorprevsocial() != null && nfp.getValorprevsocial().getValue().doubleValue() > 0 ? nfp.getValorbcprevsocial().divide(nfp.getValorprevsocial()).getValue().doubleValue() : null);
			registroE210.setSeguro_social(nfp.getValorprevsocial() != null ? nfp.getValorprevsocial().getValue().doubleValue() : null);
			registroE210.setCodigo_receita_irrf(null);
			registroE210.setImposto_retido_fonte((nfp.getValorirrf() != null && nfp.getValorirrf().getValue().doubleValue() > 0) && (nfp.getValorcsll() != null && nfp.getValorcsll().getValue().doubleValue() > 0) ? "S" : "N");
			registroE210.setCodigo_receita_csll_retida(null);
		} else {
			registroE210.setImposto_retido_fonte("N");
		}
		registroE210.setIndicador_natureeza_retencao_fonte("03");
		registroE210.setControle_sistema(0);
		return registroE210;
	}
	
	private RegistroE216 createRegistroE216(RegistroE200 registroE200, Entregadocumento ed) {
		ed.criaTotaisimposto();
		
		RegistroE216 registroE216 = new RegistroE216();
		registroE216.setNome_registro(RegistroE216.NOME_REGISTRO);
		registroE216.setEspecie_nf(registroE200.getEspecie_nf());
		registroE216.setSerie_nf(registroE200.getSerie_nf());
		registroE216.setSubserie_nf(registroE200.getSubserie_nf());
		registroE216.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE216.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE216.setBc_irrf(ed.getValormercadoria().getValue().doubleValue());
		registroE216.setAliquota_irrf(ed.getValortotalir() != null && ed.getValortotalir().getValue().doubleValue() > 0 ? ed.getValormercadoria().divide(ed.getValortotalir()).getValue().doubleValue() : null);
		registroE216.setIrrf(ed.getValortotalir() != null ? ed.getValortotalir().getValue().doubleValue() : null);
		registroE216.setBc_seguro_social(ed.getValormercadoria().getValue().doubleValue());
		registroE216.setSeguro_social(ed.getValortotalinss() != null ? ed.getValortotalinss().getValue().doubleValue() : null);
		registroE216.setBc_seguro_social_sem_producao_rural(null);
		registroE216.setAliquota_seguro_social_sem_producao_rural(null);
		registroE216.setSeguro_social_sem_producao_rural(null);
		registroE216.setBc_retencao(null);
		registroE216.setAliquota_pis(ed.getValortotalbcpis() != null && ed.getValortotalpis() != null && ed.getValortotalpis().getValue().doubleValue() > 0 ? ed.getValortotalbcpis().divide(ed.getValortotalpis()).getValue().doubleValue() : null);
		registroE216.setPis(ed.getValortotalpis() != null ? ed.getValortotalpis().getValue().doubleValue() : null);
		registroE216.setAliquota_cofins(ed.getValortotalbccofins() != null && ed.getValortotalcofins() != null && ed.getValortotalcofins().getValue().doubleValue() > 0  ? ed.getValortotalbccofins().divide(ed.getValortotalcofins()).getValue().doubleValue() : null);
		registroE216.setCofins(ed.getValortotalcofins() != null ? ed.getValortotalcofins().getValue().doubleValue() : null);
		registroE216.setAliquota_csll(null);
		registroE216.setCsll(null);
		
		return registroE216;
	}
	
	private RegistroE216 createRegistroE216(RegistroE200 registroE200, Notafiscalproduto nfp) {
		RegistroE216 registroE216 = new RegistroE216();
		registroE216.setNome_registro(RegistroE216.NOME_REGISTRO);
		registroE216.setEspecie_nf(registroE200.getEspecie_nf());
		registroE216.setSerie_nf(registroE200.getSerie_nf());
		registroE216.setSubserie_nf(registroE200.getSubserie_nf());
		registroE216.setNumero_nf(registroE200.getNumero_inicial_nf());
		registroE216.setCodigo_cliente_fornecedor(registroE200.getCodigo_cliente_fornecedor());
		registroE216.setBc_irrf(nfp.getValorbcirrf() != null ? nfp.getValorbcirrf().getValue().doubleValue() : null);
		registroE216.setAliquota_irrf(null);
		registroE216.setIrrf(nfp.getValorbcirrf() != null ? nfp.getValorbcirrf().getValue().doubleValue() : null);
		registroE216.setBc_seguro_social(nfp.getValorbcprevsocial() != null ? nfp.getValorbcprevsocial().getValue().doubleValue() : null);
		registroE216.setSeguro_social(nfp.getValorprevsocial() != null ? nfp.getValorprevsocial().getValue().doubleValue() : null);
		registroE216.setBc_seguro_social_sem_producao_rural(null);
		registroE216.setAliquota_seguro_social_sem_producao_rural(null);
		registroE216.setSeguro_social_sem_producao_rural(null);
		registroE216.setBc_retencao(null);
		registroE216.setAliquota_pis(null);
		registroE216.setPis(null);
		registroE216.setAliquota_cofins(null);
		registroE216.setCofins(null);
		registroE216.setAliquota_csll(null);
		registroE216.setCsll(null);
		
		return registroE216;
	}
	
	private List<RegistroE600_002> createRegistroE600_002(SagefiscalFiltro filtro, List<NotaFiscalServico> listaNotaFiscalServico) {
		List<RegistroE600_002> listaRegistroE600_002 = new ArrayList<RegistroE600_002>();
		if(SinedUtil.isListNotEmpty(listaNotaFiscalServico)){
			RegistroE600_002 registroE600_002;
			String numeroNf;
			Endereco enderecoCliente;
			Endereco enderecoEmpresa;
			Arquivonfnota arquivonfnota;
			for(NotaFiscalServico nfs : listaNotaFiscalServico){
				boolean cancelada = NotaStatus.CANCELADA.equals(nfs.getNotaStatus());
				
				registroE600_002 = new RegistroE600_002();
				registroE600_002.setNome_registro(RegistroE600_002.NOME_REGISTRO);
				registroE600_002.setCodigo_arquivo_municipal(RegistroE600_002.CODIGO_ARQUIVO_MUNICIPAL);
				
				if(cancelada){
					arquivonfnota = arquivonfnotaService.findByNotaCanceladaNaPrefeitura(nfs);
					if(arquivonfnota == null){
						arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nfs);
					}
				} else {
					arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nfs);
				}
				numeroNf = nfs.getNumero();
				if(arquivonfnota != null){
					numeroNf = arquivonfnota.getNumero_filtro().toString();
				} else {
					continue;
				}
				
				enderecoCliente = nfs.getCliente().getEndereco();
				enderecoEmpresa = nfs.getEmpresa().getEndereco();
				
				registroE600_002.setModelo_livro("51");
				registroE600_002.setServicos(nfs.getItemlistaservicoBean()!=null ? nfs.getItemlistaservicoBean().getCodigo() : null);
				registroE600_002.setNumero_inicial_nota(numeroNf != null ? Integer.parseInt(numeroNf) : null);
				registroE600_002.setNumero_final_nota(numeroNf != null ? Integer.parseInt(numeroNf) : null);
				registroE600_002.setData(nfs.getDtEmissao());
				registroE600_002.setSerie("X");
				registroE600_002.setSub_serie(null);
				registroE600_002.setCod_int(null);
				registroE600_002.setAtividade(null);
				registroE600_002.setCodigo(parametrogeralService.getBoolean(Parametrogeral.SAGE_ID_PESSOA) ? filtro.getApoio().addCliente(nfs.getCliente().getCdpessoa(), null) : filtro.getApoio().addCliente(nfs.getCliente().getCdpessoa(), nfs.getCliente().getCpfOuCnpjValue()) );
				registroE600_002.setValor_total_nota(null);
				registroE600_002.setValor_material(null);
				registroE600_002.setValor_subempreitada(null);
				if(cancelada){
					registroE600_002.setIss_retido(null);
					registroE600_002.setValor(null);
					registroE600_002.setAliquota(null);
					registroE600_002.setImposto(null);
				} else {
					registroE600_002.setIss_retido(Boolean.TRUE.equals(nfs.getIncideiss()) && nfs.getValoriss() != null ? nfs.getValoriss().getValue().doubleValue() : null);
					registroE600_002.setValor(nfs.getBasecalculoiss() != null ? nfs.getBasecalculoiss().getValue().doubleValue() : null);
					registroE600_002.setAliquota(nfs.getIss());
					registroE600_002.setImposto(nfs.getValoriss() != null ? nfs.getValoriss().getValue().doubleValue() : null);
				}
				registroE600_002.setIsentos(null);
				registroE600_002.setRem_dev(null);
				registroE600_002.setPercentual_irpj(32);
				registroE600_002.setVenda(Indicadortipopagamento.A_VISTA.equals(nfs.getIndicadortipopagamento()) ? 1 : 2);
				registroE600_002.setCsll_12_percentual("N");
				registroE600_002.setServico_para_exterior("N");
				
				if (Boolean.TRUE.equals(nfs.getIncideiss()
						&& nfs.getEmpresa()!=null 
						&& (Codigoregimetributario.SIMPLES_NACIONAL.equals(nfs.getEmpresa().getCrt())
								|| Codigoregimetributario.SIMPLES_NACIONAL_EXCESSO.equals(nfs.getEmpresa().getCrt())))){
					registroE600_002.setIss_retido_fonte("S");
				}else {
					registroE600_002.setIss_retido_fonte("N");
				}				
				
				
				registroE600_002.setSituacao_nf(cancelada ? "S" : "N");
				registroE600_002.setNf_simplificada_cupom_fiscal("N");
				
				if(enderecoCliente != null && enderecoCliente.getMunicipio() != null){
					registroE600_002.setCidade(registroE600_002.getNf_simplificada_cupom_fiscal().equalsIgnoreCase("S") && enderecoCliente.getMunicipio().getCdibge() != null ? enderecoCliente.getMunicipio().getCdibge() : null);
				}
				
				String prestacaoServicoDentroForaMunicipio = "";
				if (nfs.getEmpresa()!=null && (Codigoregimetributario.SIMPLES_NACIONAL.equals(nfs.getEmpresa().getCrt())
						|| Codigoregimetributario.SIMPLES_NACIONAL_EXCESSO.equals(nfs.getEmpresa().getCrt()))){
					if(enderecoCliente != null && enderecoCliente.getMunicipio() != null && enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null){
						prestacaoServicoDentroForaMunicipio = enderecoCliente.getMunicipio().equals(enderecoEmpresa.getMunicipio()) ? "1" : "2";
					}
				}
				
				registroE600_002.setPrestacao_servico_dentro_fora_municipio(prestacaoServicoDentroForaMunicipio);
				registroE600_002.setObservacao(nfs.getInfocomplementar() != null ? nfs.getInfocomplementar().replace("\n", " ").replace("\r", " ") : null);
				registroE600_002.setDigito_verificador("N");
				registroE600_002.setIncidencia_simultanea_ipi_iss(0);
				
				if (nfs.getEmpresa()!=null && (Codigoregimetributario.SIMPLES_NACIONAL.equals(nfs.getEmpresa().getCrt())
						|| Codigoregimetributario.SIMPLES_NACIONAL_EXCESSO.equals(nfs.getEmpresa().getCrt()))){
					if(enderecoCliente != null && enderecoCliente.getMunicipio() != null){
						registroE600_002.setLocal_prestacao_servico(enderecoCliente.getMunicipio().getCdibge() != null ? enderecoCliente.getMunicipio().getCdibge() : null);
					}
				}
				
				if (nfs.getPis()!=null && nfs.getPis()>0 || nfs.getCofins()!=null && nfs.getCofins()>0){
					registroE600_002.setData_execucao_concl_servico(nfs.getDtEmissao());
				}
				
				registroE600_002.setServico_prestado_hospedagem_sites_e_banco_dados(null);
				registroE600_002.setServico_prestado_gateway_pagamentos(null);
				registroE600_002.setServico_prestado_provimentos_solucoes_abertura_gerenciamento_lojas_virtuais(null);
				registroE600_002.setServico_prestado_intermediacao_finaceira(null);
				registroE600_002.setSituacao_extemporanea(null);
				registroE600_002.setData_extemporanea(null);
				registroE600_002.setEspaco_reservado(null);
				registroE600_002.setControle_sistema(0);
			
				if(!cancelada){
					List<RegistroE603> listaRegistroE603 = createRegistroE603(filtro, nfs, registroE600_002);
					Double valorTotalIss = 0d;
					Double valorTotalBcIss = 0d;
					Double valorTotalDesconto = 0d;
					Double valorTotalServico = 0d;
					for (RegistroE603 registroE603 : listaRegistroE603) {
						if(registroE603.getValor_iss() != null){
							valorTotalIss += registroE603.getValor_iss();
						}
						if(registroE603.getBase_calculo_iss() != null){
							valorTotalBcIss += registroE603.getBase_calculo_iss();
						}
						if(registroE603.getValor_desconto() != null){
							valorTotalDesconto += registroE603.getValor_desconto();
						}
						if(registroE603.getValor_servico() != null){
							valorTotalServico += registroE603.getValor_servico();
						}
					}
					
					RegistroE602 registroE602 = createRegistroE602(nfs, registroE600_002);
					registroE602.setBase_calculo_iss(SinedUtil.round(valorTotalBcIss, 2));
					registroE602.setValor_iss(SinedUtil.round(valorTotalIss, 2));
					registroE602.setTotal_servicos(SinedUtil.round(valorTotalServico, 2));
					registroE600_002.setRegistroE602(registroE602);
					
					registroE600_002.setValor(valorTotalBcIss);
					registroE600_002.setImposto(valorTotalIss);
					
					registroE600_002.setListaRegistroE603(listaRegistroE603);
				}
				
				listaRegistroE600_002.add(registroE600_002);
			}
		}
		return listaRegistroE600_002;
	}
	
	private List<RegistroE603> createRegistroE603(SagefiscalFiltro filtro, NotaFiscalServico nfs, RegistroE600_002 registroE600_002) {
		List<RegistroE603> listaRegistroE603 = new ArrayList<RegistroE603>();
		List<NotaFiscalServicoItem> listaItens = nfs.getListaItens();
		if(SinedUtil.isListNotEmpty(listaItens)){
			Collections.sort(listaItens, new Comparator<NotaFiscalServicoItem>(){
				public int compare(NotaFiscalServicoItem o1, NotaFiscalServicoItem o2) {
					return o2.getTotalParcial().compareTo(o1.getTotalParcial());
				}
			});
			
			
			RegistroE603 registroE603;
			Integer contador = 1;
			Material material;
			
//			Money valorIssMoney =  nfs.getValorIss();
//			Double valorIssTotal = valorIssMoney != null ? valorIssMoney.getValue().doubleValue() : 0d;
//			Double valorIssRestante = valorIssTotal;
			
			Double valorServicosTotal = nfs.getValorTotalServicos().getValue().doubleValue();
			Double valorDescontoTotal = nfs.getValorTotalDescontoNotaMunicipal().getValue().doubleValue();
			Double valorDescontoRestante = valorDescontoTotal;
			
			for (int j = 0; j < listaItens.size(); j++) {
				NotaFiscalServicoItem item = listaItens.get(j);
				if(item.getPrecoUnitario() > 0d){
					
					Double valorIt = item.getTotalParcial().getValue().doubleValue();
					valorIt = SinedUtil.round(valorIt, 2);
					Double percent =  (valorIt * 100d)/valorServicosTotal;
					Double valorDescontoIt = SinedUtil.truncateFormat((percent * valorDescontoTotal)/100d, 2);
					valorDescontoRestante -= valorDescontoIt;
					valorDescontoRestante = SinedUtil.round(valorDescontoRestante, 2);
					if((j+1) == listaItens.size()){
						valorDescontoIt += valorDescontoRestante;
						valorDescontoIt = SinedUtil.round(valorDescontoIt, 2);
					}
					
					Double valorBaseCalculo = valorIt - valorDescontoIt;
					valorBaseCalculo = SinedUtil.round(valorBaseCalculo, 2);
					
					registroE603 = new RegistroE603();
					registroE603.setNome_registro(RegistroE603.NOME_REGISTRO);
					registroE603.setModelo_livro(registroE600_002.getModelo_livro());
					registroE603.setNumero_nf(registroE600_002.getNumero_inicial_nota());
					registroE603.setSerie_nf(registroE600_002.getSerie());
					registroE603.setNumero_ordem_item(contador);
					
					material = null;
					String cstCofins = nfs.getCstcofins() != null ? nfs.getCstcofins().getCdnfe() : null;
					String cstPis = nfs.getCstpis() != null ? nfs.getCstpis().getCdnfe() : null;
					if(item.getVendamaterial() != null && item.getVendamaterial().getMaterial() != null && item.getVendamaterial().getMaterial().getCdmaterial() != null){
						material = item.getVendamaterial().getMaterial();
						String codigo = filtro.getApoio().addServico(item.getVendamaterial().getMaterial().getCdmaterial(), nfs.getCstpis(), nfs.getCstcofins());
						registroE603.setCodigo_servico(codigo);
						
					} else if(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null) {
						material = item.getMaterial();
						String codigo = filtro.getApoio().addServico(item.getMaterial().getCdmaterial(), nfs.getCstpis(), nfs.getCstcofins());
						registroE603.setCodigo_servico(codigo);
					} else {
						registroE603.setCodigo_servico(item.getCdNotaFiscalServicoItem() != null ? item.getCdNotaFiscalServicoItem().toString() : null);
					}
										
					if (item.getDescricao() != null && !item.getDescricao().equals("")) {
						registroE603.setDescricao_servico(item.getDescricao().replaceAll("\\r?\\n", " "));
					} else {
						if (item.getMaterial() != null && item.getMaterial().getNome() != null && !item.getMaterial().getNome().equals("")) {
							registroE603.setDescricao_servico(item.getMaterial().getNome());
						}
					}
					
					registroE603.setValor_servico(valorIt);
					registroE603.setValor_desconto(valorDescontoIt);
					registroE603.setBase_calculo_iss(valorBaseCalculo);
					registroE603.setAliquota_iss(nfs.getIss());
					if(nfs.getIss() != null){
						registroE603.setValor_iss(SinedUtil.roundUp((valorBaseCalculo * nfs.getIss() / 100d), 2));
					}
					registroE603.setSit_trib_pis(cstPis);
					registroE603.setBase_pis(valorBaseCalculo);
					registroE603.setAliq_pis(nfs.getPis());
					registroE603.setQuantidade_base_pis(null);
					registroE603.setAliq_pis_emreais(null);
					if(nfs.getPis() != null){
						registroE603.setValor_pis(valorBaseCalculo * nfs.getPis() / 100d);
					}
					registroE603.setSit_trib_cofins(cstCofins);
					registroE603.setBase_cofins(valorBaseCalculo);
					registroE603.setAliq_cofins(nfs.getCofins());
					registroE603.setQuantidade_base_cofins(null);
					registroE603.setAliq_cofins_emreais(null);
					if(nfs.getCofins() != null){
						registroE603.setValor_cofins(valorBaseCalculo * nfs.getCofins() / 100d);
					}
					registroE603.setData_apropriacao(nfs.getDtEmissao());
					registroE603.setCodigo_arquivo_mnunicipal(registroE600_002.getCodigo_arquivo_municipal());
					registroE603.setConta_analita_contabil(material != null && material.getContaContabil() != null && material.getContaContabil().getVcontacontabil() != null ? 
							material.getContaContabil().getVcontacontabil().getIdentificador() : null);
					registroE603.setTipo_incidencia("");
					registroE603.setControle_sistema(0);
					
					listaRegistroE603.add(registroE603);
					contador++;
				}
			}
		}
		return listaRegistroE603;
	}
	
	private RegistroE602 createRegistroE602(NotaFiscalServico nfs, RegistroE600_002 registroE600_002) {
		RegistroE602 registroE602 = new RegistroE602();
		registroE602 = new RegistroE602();
		registroE602.setNome_registro(RegistroE602.NOME_REGISTRO);
		registroE602.setModelo_livro(registroE600_002.getModelo_livro());
		registroE602.setNumero_nf(registroE600_002.getNumero_inicial_nota());
		registroE602.setSerie_nf(registroE600_002.getSerie());
		registroE602.setTotal_servicos(nfs.getValorTotalServicos() != null ? nfs.getValorTotalServicos().getValue().doubleValue() : null);
		registroE602.setBase_calculo_iss(nfs.getBasecalculoiss() != null ? nfs.getBasecalculoiss().getValue().doubleValue() : null);
		registroE602.setDescontos_nf(nfs.getValorTotalDescontoNotaMunicipal().getValue().doubleValue());
		registroE602.setValor_iss(nfs.getValoriss() != null ? nfs.getValoriss().getValue().doubleValue() : null);
		registroE602.setCodigo_arquivo_municipal(registroE600_002.getCodigo_arquivo_municipal());
		
		return registroE602;
	}
	
	public Date getDataInclusao(Object bean) {
		Date dataInclusao = null;
		try {
			if(bean instanceof Cliente){
				Cliente cliente = (Cliente) bean;
				if(SinedUtil.isListNotEmpty(cliente.getListClientehistorico())){
					dataInclusao = new Date(cliente.getListClientehistorico().get(0).getDtaltera().getTime());
				}else {
					dataInclusao = new Date(cliente.getDtaltera().getTime());
				}
			}else if(bean instanceof Fornecedor){
				Fornecedor fornecedor = (Fornecedor) bean;
				if(SinedUtil.isListNotEmpty(fornecedor.getHistorico())){
					dataInclusao = new Date(fornecedor.getHistorico().get(0).getDtaltera().getTime());
				}else {
					dataInclusao = new Date(fornecedor.getDtaltera().getTime());
				}
			}else if(bean instanceof Material){
				Material material = (Material) bean;
				if(SinedUtil.isListNotEmpty(material.getListaMaterialhistorico())){
					dataInclusao = new Date(material.getListaMaterialhistorico().get(0).getDtaltera().getTime());
				}else {
					dataInclusao = new Date(material.getDtaltera().getTime());
				}
			}
		} catch (Exception e) {}
		
		return dataInclusao;
	}
	
	private String getVendaEntregaFaturada(Cfop cfop) {
		return cfop != null ? RegistroE200.getVendaEntregaFaturada(cfop.getCodigo()) : "N";
	}
	
}
