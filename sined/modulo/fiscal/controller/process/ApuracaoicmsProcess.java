package br.com.linkcom.sined.modulo.fiscal.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoicmsFiltro;

@Controller(
		path="/fiscal/process/Apuracaoicms",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ApuracaoicmsProcess extends MultiActionController{
	
	@DefaultAction
	public ModelAndView handle(WebRequestContext request){
		ApuracaoicmsFiltro filtro = new ApuracaoicmsFiltro();
		return new ModelAndView("process/apuracaoicms", "filtro", filtro);
	}
}
