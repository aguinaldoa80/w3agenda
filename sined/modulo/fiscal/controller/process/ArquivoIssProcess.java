package br.com.linkcom.sined.modulo.fiscal.controller.process;

import java.lang.reflect.Method;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ArquivoIssFiltro;

@Controller(
		path="/fiscal/process/ArquivoIss",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ArquivoIssProcess extends ResourceSenderController<ArquivoIssFiltro>{

	private NotaFiscalServicoService notaFiscalServicoService;
	
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	ArquivoIssFiltro filtro) throws Exception {
		return notaFiscalServicoService.gerarArquivoIss(filtro);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, ArquivoIssFiltro filtro) throws Exception {
		if(filtro.getEmpresa() == null && request.getSession().getAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase()) == null){
			request.getSession().setAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase(), Boolean.TRUE);
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null){
				Method methodSetEmpresa = Util.beans.getSetterMethod(filtro.getClass(), "empresa");
				if(methodSetEmpresa != null && methodSetEmpresa.getParameterTypes()[0].equals(Empresa.class)){
					methodSetEmpresa.invoke(filtro, empresa);
				}
			}
		}
		return new ModelAndView("process/arquivoIss").addObject("filtro", filtro);
	}
	
}
