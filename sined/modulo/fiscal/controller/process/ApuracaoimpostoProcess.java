package br.com.linkcom.sined.modulo.fiscal.controller.process;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.Faixaimpostoapuracao;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ApuracaoimpostoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Aux_apuracaoimposto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/fiscal/process/Apuracaoimposto",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ApuracaoimpostoProcess extends MultiActionController {
	
	private NotaFiscalServicoService notaFiscalServicoService;
	private EntradafiscalService entradafiscalService;
	private ArquivonfnotaService arquivonfnotaService;
	private EmpresaService empresaService;
	
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@DefaultAction
	public ModelAndView alterar(WebRequestContext request, ApuracaoimpostoFiltro filtro)throws Exception{
		if(request.getSession().getAttribute("ApuracaoimpostoFilter") != null){
			filtro = (ApuracaoimpostoFiltro) request.getSession().getAttribute("ApuracaoimpostoFilter");
		}
		
		request.setAttribute("firstDate", SinedDateUtils.toString(SinedDateUtils.firstDateOfMonth()));
		request.setAttribute("lastDate", SinedDateUtils.toString(SinedDateUtils.lastDateOfMonth()));
		return new ModelAndView("process/apuracaoimposto", "filtro", filtro);
	}
	
	/**
	 * M�todo que lista as notas de sa�da/entrada para apura��o de outros impostos
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView listar(WebRequestContext request, ApuracaoimpostoFiltro filtro) throws Exception {
		if (filtro == null) {
			throw new SinedException("O par�metro filtro n�o pode ser null.");
		}
		
		try {
			String imposto = "";
			if(filtro.getFaixaimpostoapuracao() != null)
				imposto = filtro.getFaixaimpostoapuracao().getNome();
			
			request.setAttribute("imposto", imposto);
			request.setAttribute("firstDate", SinedDateUtils.toString(SinedDateUtils.firstDateOfMonth()));
			request.setAttribute("lastDate", SinedDateUtils.toString(SinedDateUtils.lastDateOfMonth()));
			
			List<NotaFiscalServico> listaNFS = null;
			if(filtro.getSaida() != null && filtro.getSaida()){
				listaNFS = notaFiscalServicoService.findForApuracaoimposto(filtro);
			}
			
			List<Entregadocumento> listaEntregadocumento = null;
			if(filtro.getEntrada() != null && filtro.getEntrada()){
				listaEntregadocumento = entradafiscalService.findForApuracaoimposto(filtro);
			}
			
			List<Aux_apuracaoimposto> listaAux_apuracaoimposto = this.criaApuracao(listaNFS, listaEntregadocumento, filtro);
			
			Money totalvalorbruto = new Money();
			Money totalbasecalculo = new Money();
			Double totalaliquota = 0.0;
			Money totalvalor = new Money();
			Money totalvalorliquido = new Money();
			Money totalentrada = new Money();
			Money totalsaida = new Money();
			Money totalretencoes = new Money();
			
			if(listaAux_apuracaoimposto != null && !listaAux_apuracaoimposto.isEmpty()){
				for(Aux_apuracaoimposto auxApuracaoimposto : listaAux_apuracaoimposto){
					
					if(auxApuracaoimposto.getValorbruto() != null){
						totalvalorbruto = totalvalorbruto.add(auxApuracaoimposto.getValorbruto());
					}
					if(auxApuracaoimposto.getBasecalculo() != null){
						totalbasecalculo = totalbasecalculo.add(auxApuracaoimposto.getBasecalculo());
					}
					if(auxApuracaoimposto.getAliquota() != null){
						totalaliquota += auxApuracaoimposto.getAliquota();
					}
					if(auxApuracaoimposto.getValor() != null){
						totalvalor = totalvalor.add(auxApuracaoimposto.getValor());
						
						if(auxApuracaoimposto.getEntrada() != null){
							if(auxApuracaoimposto.getEntrada()){
								totalentrada = totalentrada.add(auxApuracaoimposto.getValor());
							} else {
								totalsaida = totalsaida.add(auxApuracaoimposto.getValor());
							}
						}
						
					}
					if(auxApuracaoimposto.getValorretido() != null){
						totalretencoes = totalretencoes.add(auxApuracaoimposto.getValorretido());
					}
					if(auxApuracaoimposto.getValorliquido() != null){
						totalvalorliquido = totalvalorliquido.add(auxApuracaoimposto.getValorliquido());
					}			
				}
			}
			
			filtro.setTotalvalorbruto(totalvalorbruto);
			filtro.setTotalbasecalculo(totalbasecalculo);
			filtro.setTotalaliquota(totalaliquota);
			filtro.setTotalvalor(totalvalor);
			filtro.setTotalvalorliquido(totalvalorliquido);
			filtro.setTotalentrada(totalentrada);
			filtro.setTotalsaida(totalsaida);
			filtro.setTotalretencoes(totalretencoes);
			
			request.getSession().setAttribute("ApuracaoimpostoFilter", filtro);
			request.getSession().setAttribute("imposto", imposto);
			return new ModelAndView("direct:ajax/apuracaoimpostoListagem", "listaAux_apuracaoimposto", listaAux_apuracaoimposto);
			
		} catch (Exception e) {
			request.addError("Erro: " + e.getMessage());
			SinedUtil.redirecionamento(request, "/fiscal/process/Apuracaoimposto");
			return null;
		}
	}
	
	/**
	 * M�todo que cria apura��o
	 *
	 * @param listaNotafiscalservico
	 * @author Luiz Fernando
	 */
	public List<Aux_apuracaoimposto> criaApuracao(List<NotaFiscalServico> listaNotafiscalservico, List<Entregadocumento> listaEntregadocumento, ApuracaoimpostoFiltro filtro) {
		List<Aux_apuracaoimposto> listaAuxApuracaoimposto = new ArrayList<Aux_apuracaoimposto>();
		Aux_apuracaoimposto aux_apuracaoimposto;
		if(listaNotafiscalservico != null && !listaNotafiscalservico.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(listaNotafiscalservico, "cdNota", ",");
			List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(whereIn);
			
			for(NotaFiscalServico nfs : listaNotafiscalservico){
				for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
					if(arquivonfnota.getNota() != null && 
							arquivonfnota.getNota().getCdNota() != null &&
							arquivonfnota.getNota().getCdNota().equals(nfs.getCdNota()) &&
							arquivonfnota.getArquivonf() != null && 
							arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
							arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO) &&
							arquivonfnota.getNumeronfse() != null &&
							arquivonfnota.getCodigoverificacao() != null){
						nfs.setNumeroNfse(arquivonfnota.getNumeronfse());
						nfs.setCodVerificacao(arquivonfnota.getCodigoverificacao());
						break;
					}
				}
				aux_apuracaoimposto = new Aux_apuracaoimposto();
				aux_apuracaoimposto.setEntrada(false);
				aux_apuracaoimposto.setCdnota(nfs.getCdNota());
				aux_apuracaoimposto.setDtemissao(nfs.getDtEmissao());
				if(nfs.getNumero() != null && !"".equals(nfs.getNumero())){
					aux_apuracaoimposto.setNumero(nfs.getNumero());
				}
				if(nfs.getNumeroNfse() != null && !"".equals(nfs.getNumeroNfse())){
					aux_apuracaoimposto.setNumeronfse(nfs.getNumeroNfse());
				}
				if(nfs.getCodVerificacao() != null && !"".equals(nfs.getCodVerificacao())){
					aux_apuracaoimposto.setCodverificacao(nfs.getCodVerificacao());
				}
				if(nfs.getCliente() != null){
					if(nfs.getCliente().getCpf() != null){
						aux_apuracaoimposto.setCpf(nfs.getCliente().getCpf());
					}else if(nfs.getCliente().getCnpj() != null){
						aux_apuracaoimposto.setCnpj(nfs.getCliente().getCnpj());
					}
					aux_apuracaoimposto.setPessoa(new Pessoa(nfs.getCliente().getNome()));
				}
				aux_apuracaoimposto.setCliente(nfs.getCliente());
				if(nfs.getValorTotalServicos() != null){
					aux_apuracaoimposto.setValorbruto(nfs.getValorTotalServicos());
				}
				if(nfs.getValorNota() != null){
					aux_apuracaoimposto.setValorliquido(nfs.getValorNota());
				}
				
				this.addInfImposto(aux_apuracaoimposto, filtro.getFaixaimpostoapuracao(), nfs, null);
				
				nfs.setAux_apuracaoimposto(aux_apuracaoimposto);
				listaAuxApuracaoimposto.add(aux_apuracaoimposto);
			}
		}
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					aux_apuracaoimposto = new Aux_apuracaoimposto();
					aux_apuracaoimposto.setEntrada(true);
					aux_apuracaoimposto.setCdentregadocumento(ed.getCdentregadocumento());
					aux_apuracaoimposto.setDtemissao(ed.getDtemissao());
					if(ed.getNumero() != null && !"".equals(ed.getNumero())){
						aux_apuracaoimposto.setNumero(ed.getNumero());
					}
					if(ed.getNumero() != null && !"".equals(ed.getNumero())){
						aux_apuracaoimposto.setNumeronfse(ed.getNumero());
					}
					
					if(ed.getFornecedor() != null){
						if(ed.getFornecedor().getCpf() != null){
							aux_apuracaoimposto.setCpf(ed.getFornecedor().getCpf());
						}else if(ed.getFornecedor().getCnpj() != null){
							aux_apuracaoimposto.setCnpj(ed.getFornecedor().getCnpj());
						}
						aux_apuracaoimposto.setFornecedor(ed.getFornecedor());
						aux_apuracaoimposto.setPessoa(new Pessoa(ed.getFornecedor().getNome()));
					}
					if(em.getValortotalmaterial() != null){
						aux_apuracaoimposto.setValorbruto(new Money(em.getValortotalmaterial()));
					}
					if(ed.getValor() != null){
						aux_apuracaoimposto.setValorliquido(ed.getValor());
					}
					
					this.addInfImposto(aux_apuracaoimposto, filtro.getFaixaimpostoapuracao(), null, em);
					
					addAux_apuracaoimposto(listaAuxApuracaoimposto, aux_apuracaoimposto);
				}
			}
		}
		
		return listaAuxApuracaoimposto;
	}
	
	/**
	 * M�todo que adiciona informa��o de imposto
	 *
	 * @param aux_apuracaoimposto
	 * @param faixaimpostoapuracao
	 * @param nfs
	 * @param em
	 * @author Luiz Fernando
	 */
	private void addInfImposto(Aux_apuracaoimposto aux_apuracaoimposto, Faixaimpostoapuracao faixaimpostoapuracao, NotaFiscalServico nfs, Entregamaterial em) {
		if(Faixaimpostoapuracao.ISS.equals(faixaimpostoapuracao)){
			if(nfs != null){
				if(nfs.getBasecalculoiss() != null){
					aux_apuracaoimposto.setBasecalculo(nfs.getBasecalculoiss());
				}
				if(nfs.getIss() != null){
					aux_apuracaoimposto.setAliquota(nfs.getIss()); 
				}
				if(nfs.getValorIss() != null){
					if(Boolean.TRUE.equals(nfs.getIncideiss())){
						aux_apuracaoimposto.setValorretido(nfs.getValorIss());
					}else{
						aux_apuracaoimposto.setValor(nfs.getValorIss());
					}
				}
			}else if(em != null){
				if(em.getValorbciss() != null){
					aux_apuracaoimposto.setBasecalculo(em.getValorbciss());
				}
				if(em.getIss() != null){
					aux_apuracaoimposto.setAliquota(em.getIss()); 
				}
				if(em.getValoriss() != null){
					if(Tipotributacaoiss.RETIDA.equals(em.getTipotributacaoiss())){
						aux_apuracaoimposto.setValorretido(em.getValoriss()); 
					}else{
						aux_apuracaoimposto.setValor(em.getValoriss());
					}
				}
			}
		} else if(Faixaimpostoapuracao.INSS.equals(faixaimpostoapuracao)){
			if(nfs != null){
				if(nfs.getBasecalculoinss() != null){
					aux_apuracaoimposto.setBasecalculo(nfs.getBasecalculoinss());
				}
				if(nfs.getInss() != null){
					aux_apuracaoimposto.setAliquota(nfs.getInss()); 
				}
				if(nfs.getValorInss() != null){
					if(Boolean.TRUE.equals(nfs.getIncideinss())){
						aux_apuracaoimposto.setValorretido(nfs.getValorInss());
					}else{
						aux_apuracaoimposto.setValor(nfs.getValorInss());
					}
				}
			}else if(em != null){
				if(em.getValortotalmaterial() != null && em.getValortotalmaterial() > 0 && em.getValorinss() != null){
					aux_apuracaoimposto.setBasecalculo(new Money(em.getValortotalmaterial()));
					aux_apuracaoimposto.setValor(em.getValorinss());
					aux_apuracaoimposto.setAliquota(em.getValorinss().multiply(new Money(100.0)).divide(aux_apuracaoimposto.getBasecalculo()).getValue().doubleValue()); 
				}
			}
		} else if(Faixaimpostoapuracao.CSLL.equals(faixaimpostoapuracao)){
			if(nfs != null){
				if(nfs.getBasecalculocsll() != null){
					aux_apuracaoimposto.setBasecalculo(nfs.getBasecalculocsll());
				}
				if(nfs.getCsll() != null){
					aux_apuracaoimposto.setAliquota(nfs.getCsll()); 
				}
				if(nfs.getValorCsll() != null){
					if(Boolean.TRUE.equals(nfs.getIncidecsll())){
						aux_apuracaoimposto.setValorretido(nfs.getValorCsll());
					}else{
						aux_apuracaoimposto.setValor(nfs.getValorCsll());
					}
				}
			}else if(em != null){
				if(em.getValortotalmaterial() != null && em.getValortotalmaterial() > 0 && em.getValorcsll() != null){
					aux_apuracaoimposto.setBasecalculo(new Money(em.getValortotalmaterial()));
					aux_apuracaoimposto.setValor(em.getValorcsll());
					aux_apuracaoimposto.setAliquota(em.getValorcsll().multiply(new Money(100.0)).divide(aux_apuracaoimposto.getBasecalculo()).getValue().doubleValue()); 
				}
			}
		} else if(Faixaimpostoapuracao.IR.equals(faixaimpostoapuracao)){
			if(nfs != null){
				if(nfs.getBasecalculoir() != null){
					aux_apuracaoimposto.setBasecalculo(nfs.getBasecalculoir());
				}
				if(nfs.getIr() != null){
					aux_apuracaoimposto.setAliquota(nfs.getIr()); 
				}
				if(nfs.getValorIr() != null){
					if(Boolean.TRUE.equals(nfs.getIncideir())){
						aux_apuracaoimposto.setValorretido(nfs.getValorIr());
					}else{
						aux_apuracaoimposto.setValor(nfs.getValorIr());
					}
				}
			}else if(em != null){
				if(em.getValortotalmaterial() != null && em.getValortotalmaterial() > 0 && em.getValorir() != null){
					aux_apuracaoimposto.setBasecalculo(new Money(em.getValortotalmaterial()));
					aux_apuracaoimposto.setValor(em.getValorir());
					aux_apuracaoimposto.setAliquota(em.getValorir().multiply(new Money(100.0)).divide(aux_apuracaoimposto.getBasecalculo()).getValue().doubleValue()); 
				}
			}
		} else if(Faixaimpostoapuracao.IMPORTACAO.equals(faixaimpostoapuracao)){
			if(em.getValortotalmaterial() != null && em.getValortotalmaterial() > 0 && em.getValorimpostoimportacao() != null){
				aux_apuracaoimposto.setBasecalculo(new Money(em.getValortotalmaterial()));
				aux_apuracaoimposto.setValor(em.getValorimpostoimportacao());
				aux_apuracaoimposto.setAliquota(em.getValorimpostoimportacao().multiply(new Money(100.0)).divide(aux_apuracaoimposto.getBasecalculo()).getValue().doubleValue()); 
			}
		}
	}
	
	public void addAux_apuracaoimposto(List<Aux_apuracaoimposto> lista, Aux_apuracaoimposto aux_apuracaoimposto){
		if(lista == null) lista = new ArrayList<Aux_apuracaoimposto>();
		
		boolean adicionar = true;
		for(Aux_apuracaoimposto bean : lista){
			if(bean.getCdentregadocumento() != null && bean.getCdentregadocumento().equals(aux_apuracaoimposto.getCdentregadocumento())){
				if(aux_apuracaoimposto.getAliquota() != null && aux_apuracaoimposto.getAliquota().equals(aux_apuracaoimposto.getAliquota())){
					bean.setBasecalculo(bean.getBasecalculo().add(aux_apuracaoimposto.getBasecalculo()));
					bean.setValor(bean.getValor().add(aux_apuracaoimposto.getValor()));
					adicionar = false;
					break;
				}
			}
		}
		
		if(adicionar){
			lista.add(aux_apuracaoimposto);
		}
	}
	
	/**
	 * M�todo que gera o csv da apura��o de outros impostos
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarRelatorioApuracaoimpostoCSV(WebRequestContext request, ApuracaoimpostoFiltro filtro) {
		List<NotaFiscalServico> listaNFS = null;
		if(filtro.getSaida() != null && filtro.getSaida()){
			listaNFS = notaFiscalServicoService.findForApuracaoimposto(filtro);
		}
		
		List<Entregadocumento> listaEntregadocumento = null;
		if(filtro.getEntrada() != null && filtro.getEntrada()){
			listaEntregadocumento = entradafiscalService.findForApuracaoimposto(filtro);
		}
		
		List<Aux_apuracaoimposto> listaAux_apuracaoimposto = this.criaApuracao(listaNFS, listaEntregadocumento, filtro);
		
		Empresa empresa = null;
		String empresanome = "";
		String empresacnpj = "";
		String empresaim = "";
		if(filtro.getEmpresa() != null){
			empresa = empresaService.load(filtro.getEmpresa(), "empresa.nome, empresa.razaosocial, empresa.cnpj, empresa.inscricaomunicipal");
			if(empresa != null){
				if(empresa.getRazaosocialOuNome() != null && !"".equals(empresa.getRazaosocialOuNome())){
					empresanome = empresa.getRazaosocialOuNome();
				}
				if(empresa.getCnpj() != null){
					empresacnpj = empresa.getCnpj().toString();
				}
				if(empresa.getInscricaomunicipal() != null && !"".equals(empresa.getInscricaomunicipal())){
					empresaim = empresa.getInscricaomunicipal();
				}
			}
		}
		
		String imposto = "";
		if(filtro.getFaixaimpostoapuracao() != null)
			imposto = filtro.getFaixaimpostoapuracao().getNome();
		
		StringBuilder csv = new StringBuilder();
		csv.append("Registro de Servi�os Prestados;\n");
		csv.append("Empresa:;").append(empresanome).append("\n");
		csv.append("CNPJ:;").append(empresacnpj).append(";");
		csv.append("I.M.:;").append(empresaim).append("\n");
		csv.append("Per�odo:;").append(new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio())).append(" � ")
							   .append(new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim()));
		csv.append("\n\n");
		csv.append("Emiss�o;Nota Fiscal;N NFS-e;CNPJ/CPF;CLIENTE/FORNECEDOR;Valor Bruto (R$);Base de C�lculo (R$);" + imposto + " (%);Valor " + imposto + " (R$);Valor Retido " + imposto + " (R$);"+"Valor L�quido (R$);\n");
																																	 
		
		Money totalvalorbruto = new Money();
		Money totalbciss = new Money();
		Double totaliss = 0.0;
		Money totalvaloriss = new Money();
		Money totalvalorissretido = new Money();
		Money totalvalorliquido = new Money();
		for(Aux_apuracaoimposto auxApuracaoimposto : listaAux_apuracaoimposto){
			csv
				.append(auxApuracaoimposto.getDtemissao() != null ? new SimpleDateFormat("dd/MM/yyyy").format(auxApuracaoimposto.getDtemissao()) : "")
				.append(";")
				.append(auxApuracaoimposto.getNumero() != null ? auxApuracaoimposto.getNumero() : "")
				.append(";")
				.append(auxApuracaoimposto.getNumeronfse() != null && !"".equals(auxApuracaoimposto.getNumeronfse()) ? auxApuracaoimposto.getNumeronfse() : "")
				.append(auxApuracaoimposto.getNumeronfse() != null && !"".equals(auxApuracaoimposto.getNumeronfse()) ? 
						((auxApuracaoimposto.getCodverificacao() != null ? " / " + auxApuracaoimposto.getCodverificacao() : "") ) : auxApuracaoimposto.getCodverificacao() != null ? auxApuracaoimposto.getCodverificacao() : "")
				.append(";")
				.append(auxApuracaoimposto.getCnpj() != null ? auxApuracaoimposto.getCnpj().toString() : "")
				.append(auxApuracaoimposto.getCpf() != null ? auxApuracaoimposto.getCpf().toString() : "")
				.append(";")
				.append(auxApuracaoimposto.getPessoa() != null ? auxApuracaoimposto.getPessoa().getNome() : "")
				.append(";")
				.append(auxApuracaoimposto.getValorbruto())
				.append(";")
				.append(auxApuracaoimposto.getBasecalculo())
				.append(";")
				.append(auxApuracaoimposto.getAliquota())
				.append(";")
				.append(auxApuracaoimposto.getValor())
				.append(";")
				.append(auxApuracaoimposto.getValorretido())
				.append(";")
				.append(auxApuracaoimposto.getValorliquido())
				.append(";\n");
			
				if(auxApuracaoimposto.getValorbruto() != null){
					totalvalorbruto = totalvalorbruto.add(auxApuracaoimposto.getValorbruto());
				}
				if(auxApuracaoimposto.getBasecalculo() != null){
					totalbciss = totalbciss.add(auxApuracaoimposto.getBasecalculo());
				}
				if(auxApuracaoimposto.getAliquota() != null){
					totaliss = totaliss + auxApuracaoimposto.getAliquota();
				}
				if(auxApuracaoimposto.getValor() != null){
					totalvaloriss = totalvaloriss.add(auxApuracaoimposto.getValor());
				}
				if(auxApuracaoimposto.getValorretido() != null){
					totalvalorissretido = totalvalorissretido.add(auxApuracaoimposto.getValorretido());
				}
				if(auxApuracaoimposto.getValorliquido() != null){
					totalvalorliquido = totalvalorliquido.add(auxApuracaoimposto.getValorliquido());
				}			
		}
		
		csv.append(";;;;Total;")
				.append(totalvalorbruto).append(";")
				.append(totalbciss).append(";")
				.append(totaliss).append(";")
				.append(totalvaloriss).append(";")
				.append(totalvalorissretido).append(";")
				.append(totalvalorliquido);
		
		Resource resource = new Resource("text/csv", "apuracao" + imposto + "_" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio()) + "_" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim())  +".csv", csv.toString().getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
}
