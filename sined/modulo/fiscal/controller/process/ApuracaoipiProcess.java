package br.com.linkcom.sined.modulo.fiscal.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoipiFiltro;

@Controller(
		path="/fiscal/process/Apuracaoipi",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ApuracaoipiProcess extends MultiActionController{
	
	@DefaultAction
	public ModelAndView handle(WebRequestContext request){
		ApuracaoipiFiltro filtro = new ApuracaoipiFiltro();
		return new ModelAndView("process/apuracaoipi", "filtro", filtro);
	}
}
