package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0130 {
	
	public final static String REG = "0130";
	
	private String identificacao;
	private String codigo_produto_componente;
	private Double quantidade;
	private String sigla_unidademedida;
	private Double percentual_perda;
	private Date inicio_vigencia;
	private Date fim_vigencia;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigo_produto_componente()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getQuantidade()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getSigla_unidademedida()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getPercentual_perda()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getInicio_vigencia()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDate(getFim_vigencia()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getCodigo_produto_componente() {
		return codigo_produto_componente;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public String getSigla_unidademedida() {
		return sigla_unidademedida;
	}

	public Double getPercentual_perda() {
		return percentual_perda;
	}

	public Date getInicio_vigencia() {
		return inicio_vigencia;
	}

	public Date getFim_vigencia() {
		return fim_vigencia;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_produto_componente(String codigoProdutoComponente) {
		codigo_produto_componente = codigoProdutoComponente;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setSigla_unidademedida(String siglaUnidademedida) {
		sigla_unidademedida = siglaUnidademedida;
	}

	public void setPercentual_perda(Double percentualPerda) {
		percentual_perda = percentualPerda;
	}

	public void setInicio_vigencia(Date inicioVigencia) {
		inicio_vigencia = inicioVigencia;
	}

	public void setFim_vigencia(Date fimVigencia) {
		fim_vigencia = fimVigencia;
	}
}
