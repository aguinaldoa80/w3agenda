package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;


public class Registro3300 {
	
	public final static String REG = "3300";
	
	private String identificacao;
	private Date data_lancamento;
	private Integer contacontabil_debito;
	private Integer contacontabil_credito;
	private Double valor_lancamento;
	private Integer codigo_historico_contabil;
	private String descricao_historico_contabil;
	private String usuario;
	private Integer codigo_filial;
	
	@Override
	public String toString() {
		return  SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Date getData_lancamento() {
		return data_lancamento;
	}

	public Integer getContacontabil_debito() {
		return contacontabil_debito;
	}

	public Integer getContacontabil_credito() {
		return contacontabil_credito;
	}

	public Double getValor_lancamento() {
		return valor_lancamento;
	}

	public Integer getCodigo_historico_contabil() {
		return codigo_historico_contabil;
	}

	public String getDescricao_historico_contabil() {
		return descricao_historico_contabil;
	}

	public String getUsuario() {
		return usuario;
	}

	public Integer getCodigo_filial() {
		return codigo_filial;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setData_lancamento(Date dataLancamento) {
		data_lancamento = dataLancamento;
	}

	public void setContacontabil_debito(Integer contacontabilDebito) {
		contacontabil_debito = contacontabilDebito;
	}

	public void setContacontabil_credito(Integer contacontabilCredito) {
		contacontabil_credito = contacontabilCredito;
	}

	public void setValor_lancamento(Double valorLancamento) {
		valor_lancamento = valorLancamento;
	}

	public void setCodigo_historico_contabil(Integer codigoHistoricoContabil) {
		codigo_historico_contabil = codigoHistoricoContabil;
	}

	public void setDescricao_historico_contabil(String descricaoHistoricoContabil) {
		descricao_historico_contabil = descricaoHistoricoContabil;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setCodigo_filial(Integer codigoFilial) {
		codigo_filial = codigoFilial;
	}
}
