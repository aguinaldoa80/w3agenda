package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0022 {
	
	public final static String REG = "0012";
	
	private String identificacao;
	private String nomedependente;
	private Date datanascimentodependente;
	private Integer dependenteate;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getNomedependente()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getDatanascimentodependente()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getDependenteate()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getNomedependente() {
		return nomedependente;
	}

	public Date getDatanascimentodependente() {
		return datanascimentodependente;
	}

	public Integer getDependenteate() {
		return dependenteate;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setNomedependente(String nomedependente) {
		this.nomedependente = nomedependente;
	}

	public void setDatanascimentodependente(Date datanascimentodependente) {
		this.datanascimentodependente = datanascimentodependente;
	}

	public void setDependenteate(Integer dependenteate) {
		this.dependenteate = dependenteate;
	}
}
