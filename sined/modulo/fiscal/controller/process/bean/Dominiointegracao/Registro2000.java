package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro2000 {
	
	public final static String REG = "2000";
	
	private String identificacao;
	private Integer codigo_especie;
	private String inscricao_cliente;
	private Integer codigo_acumulador;
	private String cfop;
	private Integer codigo_exclusao_dief;
	private String sigla_estado_cliente;
	private Integer seguimento;
	private String numero_documento;
	private String serie;
	private Integer documento_final;
	private Date data_saida;
	private Date data_emissao;
	private Double valor_contabil;
	private Double valor_exclusao_dief;
	private String obsevacao;
	private String codigo_municipio;
	private String modalidade_frete;
	private Integer cfop_estendido_detalhamento;
	private Integer codigo_transferencia_credito;
	private Integer codigo_observacao;
	private Date data_visto_notas_transf_cred_icms;
	private Integer codigo_antecipacao_tributaria;
	private String fato_gerador_crf;
	private String fato_gerador_crfop;
	private String fato_gerador_irrf;
	private Integer tipo_receita;
	private Double valor_frete;
	private Double valor_seguro;
	private Double valor_despesas_acessorias;
	private Double valor_produtos;
	private Double valor_bc_icms_st;
	private Double outras_saidas_isentas;
	private Double saidas_isentas;
	private Double saidas_isentas_cupomfiscal;
	private Double saidas_isentas_nf_mod02;
	private Integer codigo_modelo_doc_fiscal;
	private Integer codigo_fiscal_prestacao_servicos;
	private Integer codigo_situacao_tributaria;
	private Integer sub_serie;
	private Integer tipo_titulo;
	private String identificacao_titulo;
	private String inscricao_estadual_cliente;
	private String inscricao_municipal_cliente;
	private String chave_nfe;
	private String codigo_recolhimento_fethab;
	private String responsavel_recolhimento_fethab;
	private Integer tipo_cte;
	private String cte_referencia;
	private Integer codigo_informacao_complementar;
	private String informacao_complementar;
	private String cst_piscofins;
	private String natureza_receita;
	private Double valor_servico_itens_piscofins;
	private Double base_calculo_piscofins;
	private Double aliquota_pis;
	private Double aliquota_cofins;
	private Integer quantidade_kwh;
	
	private List<Registro2020> listaRegistro2020;
	
	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_especie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_cliente()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_acumulador()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCfop()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_exclusao_dief()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getSigla_estado_cliente()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getSeguimento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNumero_documento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getSerie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getDocumento_final()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_saida()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_emissao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_contabil()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_exclusao_dief()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getObsevacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_municipio()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getModalidade_frete()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCfop_estendido_detalhamento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_transferencia_credito()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_observacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_visto_notas_transf_cred_icms()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_antecipacao_tributaria()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_crf()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_crfop()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_irrf()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getTipo_receita()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_frete()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_seguro()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_despesas_acessorias()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_produtos()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_bc_icms_st()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getOutras_saidas_isentas()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getSaidas_isentas()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getSaidas_isentas_cupomfiscal()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getSaidas_isentas_nf_mod02()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_modelo_doc_fiscal()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_fiscal_prestacao_servicos()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_situacao_tributaria()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getSub_serie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getTipo_titulo()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getIdentificacao_titulo()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_estadual_cliente()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_municipal_cliente()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getChave_nfe()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_recolhimento_fethab()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getResponsavel_recolhimento_fethab()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getTipo_cte()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCte_referencia()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_informacao_complementar()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInformacao_complementar()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCst_piscofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNatureza_receita()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_servico_itens_piscofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getBase_calculo_piscofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getAliquota_pis()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getAliquota_cofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getQuantidade_kwh()) + SPEDUtil.SEPARADOR_CAMPO + SIntegraUtil.SEPARADOR_LINHA +
				
				SPEDUtil.printLista(getListaRegistro2020());
		
		
		
		
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Integer getCodigo_especie() {
		return codigo_especie;
	}

	public String getInscricao_cliente() {
		return inscricao_cliente;
	}

	public Integer getCodigo_acumulador() {
		return codigo_acumulador;
	}

	public String getCfop() {
		return cfop;
	}

	public Integer getCodigo_exclusao_dief() {
		return codigo_exclusao_dief;
	}

	public String getSigla_estado_cliente() {
		return sigla_estado_cliente;
	}

	public Integer getSeguimento() {
		return seguimento;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public String getSerie() {
		return serie;
	}

	public Integer getDocumento_final() {
		return documento_final;
	}

	public Date getData_saida() {
		return data_saida;
	}

	public Date getData_emissao() {
		return data_emissao;
	}

	public Double getValor_contabil() {
		return valor_contabil;
	}

	public Double getValor_exclusao_dief() {
		return valor_exclusao_dief;
	}

	public String getObsevacao() {
		return obsevacao;
	}

	public String getCodigo_municipio() {
		return codigo_municipio;
	}

	public String getModalidade_frete() {
		return modalidade_frete;
	}

	public Integer getCfop_estendido_detalhamento() {
		return cfop_estendido_detalhamento;
	}

	public Integer getCodigo_transferencia_credito() {
		return codigo_transferencia_credito;
	}

	public Integer getCodigo_observacao() {
		return codigo_observacao;
	}

	public Date getData_visto_notas_transf_cred_icms() {
		return data_visto_notas_transf_cred_icms;
	}

	public Integer getCodigo_antecipacao_tributaria() {
		return codigo_antecipacao_tributaria;
	}

	public String getFato_gerador_crf() {
		return fato_gerador_crf;
	}

	public String getFato_gerador_crfop() {
		return fato_gerador_crfop;
	}

	public String getFato_gerador_irrf() {
		return fato_gerador_irrf;
	}

	public Integer getTipo_receita() {
		return tipo_receita;
	}

	public Double getValor_frete() {
		return valor_frete;
	}

	public Double getValor_seguro() {
		return valor_seguro;
	}

	public Double getValor_despesas_acessorias() {
		return valor_despesas_acessorias;
	}

	public Double getValor_produtos() {
		return valor_produtos;
	}

	public Double getValor_bc_icms_st() {
		return valor_bc_icms_st;
	}

	public Double getOutras_saidas_isentas() {
		return outras_saidas_isentas;
	}

	public Double getSaidas_isentas() {
		return saidas_isentas;
	}

	public Double getSaidas_isentas_cupomfiscal() {
		return saidas_isentas_cupomfiscal;
	}

	public Double getSaidas_isentas_nf_mod02() {
		return saidas_isentas_nf_mod02;
	}

	public Integer getCodigo_modelo_doc_fiscal() {
		return codigo_modelo_doc_fiscal;
	}

	public Integer getCodigo_fiscal_prestacao_servicos() {
		return codigo_fiscal_prestacao_servicos;
	}

	public Integer getCodigo_situacao_tributaria() {
		return codigo_situacao_tributaria;
	}

	public Integer getSub_serie() {
		return sub_serie;
	}

	public Integer getTipo_titulo() {
		return tipo_titulo;
	}

	public String getIdentificacao_titulo() {
		return identificacao_titulo;
	}

	public String getInscricao_estadual_cliente() {
		return inscricao_estadual_cliente;
	}

	public String getInscricao_municipal_cliente() {
		return inscricao_municipal_cliente;
	}

	public String getChave_nfe() {
		return chave_nfe;
	}

	public String getCodigo_recolhimento_fethab() {
		return codigo_recolhimento_fethab;
	}

	public String getResponsavel_recolhimento_fethab() {
		return responsavel_recolhimento_fethab;
	}

	public Integer getTipo_cte() {
		return tipo_cte;
	}

	public String getCte_referencia() {
		return cte_referencia;
	}

	public Integer getCodigo_informacao_complementar() {
		return codigo_informacao_complementar;
	}

	public String getInformacao_complementar() {
		return informacao_complementar;
	}

	public String getCst_piscofins() {
		return cst_piscofins;
	}

	public String getNatureza_receita() {
		return natureza_receita;
	}

	public Double getValor_servico_itens_piscofins() {
		return valor_servico_itens_piscofins;
	}

	public Double getBase_calculo_piscofins() {
		return base_calculo_piscofins;
	}

	public Double getAliquota_pis() {
		return aliquota_pis;
	}

	public Double getAliquota_cofins() {
		return aliquota_cofins;
	}

	public Integer getQuantidade_kwh() {
		return quantidade_kwh;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_especie(Integer codigoEspecie) {
		codigo_especie = codigoEspecie;
	}

	public void setInscricao_cliente(String inscricaoCliente) {
		inscricao_cliente = inscricaoCliente;
	}

	public void setCodigo_acumulador(Integer codigoAcumulador) {
		codigo_acumulador = codigoAcumulador;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	public void setCodigo_exclusao_dief(Integer codigoExclusaoDief) {
		codigo_exclusao_dief = codigoExclusaoDief;
	}

	public void setSigla_estado_cliente(String siglaEstadoCliente) {
		sigla_estado_cliente = siglaEstadoCliente;
	}

	public void setSeguimento(Integer seguimento) {
		this.seguimento = seguimento;
	}

	public void setNumero_documento(String numeroDocumento) {
		numero_documento = numeroDocumento;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setDocumento_final(Integer documentoFinal) {
		documento_final = documentoFinal;
	}

	public void setData_saida(Date dataSaida) {
		data_saida = dataSaida;
	}

	public void setData_emissao(Date dataEmissao) {
		data_emissao = dataEmissao;
	}

	public void setValor_contabil(Double valorContabil) {
		valor_contabil = valorContabil;
	}

	public void setValor_exclusao_dief(Double valorExclusaoDief) {
		valor_exclusao_dief = valorExclusaoDief;
	}

	public void setObsevacao(String obsevacao) {
		this.obsevacao = obsevacao;
	}

	public void setCodigo_municipio(String codigoMunicipio) {
		codigo_municipio = codigoMunicipio;
	}

	public void setModalidade_frete(String modalidadeFrete) {
		modalidade_frete = modalidadeFrete;
	}

	public void setCfop_estendido_detalhamento(Integer cfopEstendidoDetalhamento) {
		cfop_estendido_detalhamento = cfopEstendidoDetalhamento;
	}

	public void setCodigo_transferencia_credito(Integer codigoTransferenciaCredito) {
		codigo_transferencia_credito = codigoTransferenciaCredito;
	}

	public void setCodigo_observacao(Integer codigoObservacao) {
		codigo_observacao = codigoObservacao;
	}

	public void setData_visto_notas_transf_cred_icms(
			Date dataVistoNotasTransfCredIcms) {
		data_visto_notas_transf_cred_icms = dataVistoNotasTransfCredIcms;
	}

	public void setCodigo_antecipacao_tributaria(Integer codigoAntecipacaoTributaria) {
		codigo_antecipacao_tributaria = codigoAntecipacaoTributaria;
	}

	public void setFato_gerador_crf(String fatoGeradorCrf) {
		fato_gerador_crf = fatoGeradorCrf;
	}

	public void setFato_gerador_crfop(String fatoGeradorCrfop) {
		fato_gerador_crfop = fatoGeradorCrfop;
	}

	public void setFato_gerador_irrf(String fatoGeradorIrrf) {
		fato_gerador_irrf = fatoGeradorIrrf;
	}

	public void setTipo_receita(Integer tipoReceita) {
		tipo_receita = tipoReceita;
	}

	public void setValor_frete(Double valorFrete) {
		valor_frete = valorFrete;
	}

	public void setValor_seguro(Double valorSeguro) {
		valor_seguro = valorSeguro;
	}

	public void setValor_despesas_acessorias(Double valorDespesasAcessorias) {
		valor_despesas_acessorias = valorDespesasAcessorias;
	}

	public void setValor_produtos(Double valorProdutos) {
		valor_produtos = valorProdutos;
	}

	public void setValor_bc_icms_st(Double valorBcIcmsSt) {
		valor_bc_icms_st = valorBcIcmsSt;
	}

	public void setOutras_saidas_isentas(Double outrasSaidasIsentas) {
		outras_saidas_isentas = outrasSaidasIsentas;
	}

	public void setSaidas_isentas(Double saidasIsentas) {
		saidas_isentas = saidasIsentas;
	}

	public void setSaidas_isentas_cupomfiscal(Double saidasIsentasCupomfiscal) {
		saidas_isentas_cupomfiscal = saidasIsentasCupomfiscal;
	}

	public void setSaidas_isentas_nf_mod02(Double saidasIsentasNfMod02) {
		saidas_isentas_nf_mod02 = saidasIsentasNfMod02;
	}

	public void setCodigo_modelo_doc_fiscal(Integer codigoModeloDocFiscal) {
		codigo_modelo_doc_fiscal = codigoModeloDocFiscal;
	}

	public void setCodigo_fiscal_prestacao_servicos(
			Integer codigoFiscalPrestacaoServicos) {
		codigo_fiscal_prestacao_servicos = codigoFiscalPrestacaoServicos;
	}

	public void setCodigo_situacao_tributaria(Integer codigoSituacaoTributaria) {
		codigo_situacao_tributaria = codigoSituacaoTributaria;
	}

	public void setSub_serie(Integer subSerie) {
		sub_serie = subSerie;
	}

	public void setTipo_titulo(Integer tipoTitulo) {
		tipo_titulo = tipoTitulo;
	}

	public void setIdentificacao_titulo(String identificacaoTitulo) {
		identificacao_titulo = identificacaoTitulo;
	}

	public void setInscricao_estadual_cliente(String inscricaoEstadualCliente) {
		inscricao_estadual_cliente = inscricaoEstadualCliente;
	}

	public void setInscricao_municipal_cliente(String inscricaoMunicipalCliente) {
		inscricao_municipal_cliente = inscricaoMunicipalCliente;
	}

	public void setChave_nfe(String chaveNfe) {
		chave_nfe = chaveNfe;
	}

	public void setCodigo_recolhimento_fethab(String codigoRecolhimentoFethab) {
		codigo_recolhimento_fethab = codigoRecolhimentoFethab;
	}

	public void setResponsavel_recolhimento_fethab(
			String responsavelRecolhimentoFethab) {
		responsavel_recolhimento_fethab = responsavelRecolhimentoFethab;
	}

	public void setTipo_cte(Integer tipoCte) {
		tipo_cte = tipoCte;
	}

	public void setCte_referencia(String cteReferencia) {
		cte_referencia = cteReferencia;
	}

	public void setCodigo_informacao_complementar(
			Integer codigoInformacaoComplementar) {
		codigo_informacao_complementar = codigoInformacaoComplementar;
	}

	public void setInformacao_complementar(String informacaoComplementar) {
		informacao_complementar = informacaoComplementar;
	}

	public void setCst_piscofins(String cstPiscofins) {
		cst_piscofins = cstPiscofins;
	}

	public void setNatureza_receita(String naturezaReceita) {
		natureza_receita = naturezaReceita;
	}

	public void setValor_servico_itens_piscofins(Double valorServicoItensPiscofins) {
		valor_servico_itens_piscofins = valorServicoItensPiscofins;
	}

	public void setBase_calculo_piscofins(Double baseCalculoPiscofins) {
		base_calculo_piscofins = baseCalculoPiscofins;
	}

	public void setAliquota_pis(Double aliquotaPis) {
		aliquota_pis = aliquotaPis;
	}

	public void setAliquota_cofins(Double aliquotaCofins) {
		aliquota_cofins = aliquotaCofins;
	}

	public void setQuantidade_kwh(Integer quantidadeKwh) {
		quantidade_kwh = quantidadeKwh;
	}

	public List<Registro2020> getListaRegistro2020() {
		return listaRegistro2020;
	}

	public void setListaRegistro2020(List<Registro2020> listaRegistro2020) {
		this.listaRegistro2020 = listaRegistro2020;
	}
}
