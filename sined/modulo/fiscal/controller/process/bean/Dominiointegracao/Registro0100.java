package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0100 {
	
	public final static String REG = "0100";
	
	private String identificacao;
	private String codigoproduto;
	private String descricaoproduto;
	private String codigonbm;
	private String codigoncm;
	private Integer codigoncmexterior;
	private String codigobarras;
	private Integer codigoimpostoimportacao;
	private Integer codigogrupoprodutos;
	private String unidademedida;
	private String unidademedida_inventariada_dif_comercializada;
	private String tipoproduto;
	private Integer tipoarmafogo;
	private String descricao_armafogo;
	private Integer tipomedicamento;
	private String servico_tributado_issqn;
	private String codigo_chassiveiculo;
	private Double valorunitario;
	private Double quantidade_inicial_estoque;
	private Double valor_inicial_estoque;
	private Integer codigo_situacaotributaria_icms;
	private Double aliquotaicms;
	private Double aliquotaipi;
	private String periodicidadeipi;
	private String observacao;
	private String exportarprodutodnf;
	private String extipi;
	private Integer dnf_codigoespecie_produto;
	private Integer dnf_unidademedida_padrao;
	private Double dnf_fatorconversao;
	private String dnf_codigoproduto;
	private Double dnf_capacidade_volumetrica;
	private String sedic_codigoean;
	private Integer sedic_codigoprodutorelevante;
	private String scanc_gerascanc;
	private Integer scanc_codigoprodutoscanc;
	private String scanc_contemgasolinaa;
	private String scanc_tipoproduto;
	private String grfctb_geragrfctb;
	private Integer grfctb_codigoproduto;
	private String dief_unidade;
	private Integer dief_tipoprodutoservico;
	private String st88_informaregistro88stsintegra;
	private Integer st88_codigotabelasefaz;
	private String go_informacoes_complementares_ipmpdi;
	private Integer go_codigoprodutoservico_ipmpdi;
	private String go_produto_relacionado;
	private String am_cestabasica;
	private Integer am_codigoprodutodam;
	private String rs_produto_incluido_campo_st;
	private Date rs_data_inicio_st;
	private String rs_produto_precotabelado;
	private Double rs_valor_unitario_st;
	private Double rs_mvast;
	private Integer rs_grupost;
	private String pr_equipamentoecf;
	private String ms_possui_incentivo_fiscal;
	private Integer df_produto_sujeito_regimeespecial;
	private Integer df_item_padrao_regimeespecial;
	private Integer pe_tipoproduto;
	private String sp_controla_ressarcimento_cat1799;
	private Double sp_data_saldo_inicial_controle_cat1799;
	private Double sp_valor_unitario_controle_cat1799;
	private Double sp_quantidade_controle_cat1799;
	private Double sp_valor_final_controle_cat1799;
	private Integer sped_genero;
	private Integer sped_codigoservico;
	private Integer sped_tipoitem;
	private Integer sped_classificacao;
	private Integer sped_conta_contabil_estoque_emseupoder;
	private Integer sped_conta_contabil_estoque_empoderterceiros;
	private Integer sped_conta_contabil_estoque_terceirosemseupoder;
	private String sped_tiporeceita;
	private Integer sped_energiaeletrica_gascanalizado;
	private Date datacadastro;
	private String produto_escrituradolmc;
	private String codigocombustivel_tabeladf;
	private String codigocombustivel_tabelaanp;
	private String produtorelacionado_inciso_art8_mp5402011;
	private String permitir_informar_descricaocomplementar_lancto_nota;
	private Integer codigo_atividade_inssfolha;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigoproduto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDescricaoproduto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigonbm()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigoncm()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigoncmexterior()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigobarras()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigoimpostoimportacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigogrupoprodutos()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getUnidademedida()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getUnidademedida_inventariada_dif_comercializada()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getTipoproduto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getTipoarmafogo()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDescricao_armafogo()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getTipomedicamento()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getServico_tributado_issqn()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigo_chassiveiculo()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getValorunitario()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getQuantidade_inicial_estoque()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getValor_inicial_estoque()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo_situacaotributaria_icms()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getAliquotaicms()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getAliquotaipi()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getPeriodicidadeipi()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getObservacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getExportarprodutodnf()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getExtipi()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getDnf_codigoespecie_produto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getDnf_unidademedida_padrao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getDnf_fatorconversao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDnf_codigoproduto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getDnf_capacidade_volumetrica()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getSedic_codigoean()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSedic_codigoprodutorelevante()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getScanc_gerascanc()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getScanc_codigoprodutoscanc()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getScanc_contemgasolinaa()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getScanc_tipoproduto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getGrfctb_geragrfctb()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getGrfctb_codigoproduto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDief_unidade()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getDief_tipoprodutoservico()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getSt88_informaregistro88stsintegra()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSt88_codigotabelasefaz()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getGo_informacoes_complementares_ipmpdi()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getGo_codigoprodutoservico_ipmpdi()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getGo_produto_relacionado()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getAm_cestabasica()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getAm_codigoprodutodam()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getRs_produto_incluido_campo_st()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getRs_data_inicio_st()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getRs_produto_precotabelado()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getRs_valor_unitario_st()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getRs_mvast()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getRs_grupost()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getPr_equipamentoecf()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getMs_possui_incentivo_fiscal()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getDf_produto_sujeito_regimeespecial()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getDf_item_padrao_regimeespecial()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getPe_tipoproduto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getSp_controla_ressarcimento_cat1799()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getSp_data_saldo_inicial_controle_cat1799()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getSp_valor_unitario_controle_cat1799()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getSp_quantidade_controle_cat1799()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getSp_valor_final_controle_cat1799()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSped_genero()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSped_codigoservico()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSped_tipoitem()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSped_classificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSped_conta_contabil_estoque_emseupoder()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSped_conta_contabil_estoque_empoderterceiros()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getSped_conta_contabil_estoque_terceirosemseupoder()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getSped_tiporeceita()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getSped_energiaeletrica_gascanalizado()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getDatacadastro()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getProduto_escrituradolmc()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigocombustivel_tabeladf()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigocombustivel_tabelaanp()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getProdutorelacionado_inciso_art8_mp5402011()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getPermitir_informar_descricaocomplementar_lancto_nota()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo_atividade_inssfolha()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getCodigoproduto() {
		return codigoproduto;
	}

	public String getDescricaoproduto() {
		return descricaoproduto;
	}

	public String getCodigonbm() {
		return codigonbm;
	}

	public String getCodigoncm() {
		return codigoncm;
	}

	public Integer getCodigoncmexterior() {
		return codigoncmexterior;
	}

	public String getCodigobarras() {
		return codigobarras;
	}

	public Integer getCodigoimpostoimportacao() {
		return codigoimpostoimportacao;
	}

	public Integer getCodigogrupoprodutos() {
		return codigogrupoprodutos;
	}

	public String getUnidademedida() {
		return unidademedida;
	}

	public String getUnidademedida_inventariada_dif_comercializada() {
		return unidademedida_inventariada_dif_comercializada;
	}

	public String getTipoproduto() {
		return tipoproduto;
	}

	public Integer getTipoarmafogo() {
		return tipoarmafogo;
	}

	public String getDescricao_armafogo() {
		return descricao_armafogo;
	}

	public Integer getTipomedicamento() {
		return tipomedicamento;
	}

	public String getServico_tributado_issqn() {
		return servico_tributado_issqn;
	}

	public String getCodigo_chassiveiculo() {
		return codigo_chassiveiculo;
	}

	public Double getValorunitario() {
		return valorunitario;
	}

	public Double getQuantidade_inicial_estoque() {
		return quantidade_inicial_estoque;
	}

	public Double getValor_inicial_estoque() {
		return valor_inicial_estoque;
	}

	public Integer getCodigo_situacaotributaria_icms() {
		return codigo_situacaotributaria_icms;
	}

	public Double getAliquotaicms() {
		return aliquotaicms;
	}

	public Double getAliquotaipi() {
		return aliquotaipi;
	}

	public String getPeriodicidadeipi() {
		return periodicidadeipi;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getExportarprodutodnf() {
		return exportarprodutodnf;
	}

	public String getExtipi() {
		return extipi;
	}

	public Integer getDnf_codigoespecie_produto() {
		return dnf_codigoespecie_produto;
	}

	public Integer getDnf_unidademedida_padrao() {
		return dnf_unidademedida_padrao;
	}

	public Double getDnf_fatorconversao() {
		return dnf_fatorconversao;
	}

	public String getDnf_codigoproduto() {
		return dnf_codigoproduto;
	}

	public Double getDnf_capacidade_volumetrica() {
		return dnf_capacidade_volumetrica;
	}

	public String getSedic_codigoean() {
		return sedic_codigoean;
	}

	public Integer getSedic_codigoprodutorelevante() {
		return sedic_codigoprodutorelevante;
	}

	public String getScanc_gerascanc() {
		return scanc_gerascanc;
	}

	public Integer getScanc_codigoprodutoscanc() {
		return scanc_codigoprodutoscanc;
	}

	public String getScanc_contemgasolinaa() {
		return scanc_contemgasolinaa;
	}

	public String getScanc_tipoproduto() {
		return scanc_tipoproduto;
	}

	public String getGrfctb_geragrfctb() {
		return grfctb_geragrfctb;
	}

	public Integer getGrfctb_codigoproduto() {
		return grfctb_codigoproduto;
	}

	public String getDief_unidade() {
		return dief_unidade;
	}

	public Integer getDief_tipoprodutoservico() {
		return dief_tipoprodutoservico;
	}

	public String getSt88_informaregistro88stsintegra() {
		return st88_informaregistro88stsintegra;
	}

	public Integer getSt88_codigotabelasefaz() {
		return st88_codigotabelasefaz;
	}

	public String getGo_informacoes_complementares_ipmpdi() {
		return go_informacoes_complementares_ipmpdi;
	}

	public Integer getGo_codigoprodutoservico_ipmpdi() {
		return go_codigoprodutoservico_ipmpdi;
	}

	public String getGo_produto_relacionado() {
		return go_produto_relacionado;
	}

	public String getAm_cestabasica() {
		return am_cestabasica;
	}

	public Integer getAm_codigoprodutodam() {
		return am_codigoprodutodam;
	}

	public String getRs_produto_incluido_campo_st() {
		return rs_produto_incluido_campo_st;
	}

	public Date getRs_data_inicio_st() {
		return rs_data_inicio_st;
	}

	public String getRs_produto_precotabelado() {
		return rs_produto_precotabelado;
	}

	public Double getRs_valor_unitario_st() {
		return rs_valor_unitario_st;
	}

	public Double getRs_mvast() {
		return rs_mvast;
	}

	public Integer getRs_grupost() {
		return rs_grupost;
	}

	public String getPr_equipamentoecf() {
		return pr_equipamentoecf;
	}

	public String getMs_possui_incentivo_fiscal() {
		return ms_possui_incentivo_fiscal;
	}

	public Integer getDf_produto_sujeito_regimeespecial() {
		return df_produto_sujeito_regimeespecial;
	}

	public Integer getDf_item_padrao_regimeespecial() {
		return df_item_padrao_regimeespecial;
	}

	public Integer getPe_tipoproduto() {
		return pe_tipoproduto;
	}

	public String getSp_controla_ressarcimento_cat1799() {
		return sp_controla_ressarcimento_cat1799;
	}

	public Double getSp_data_saldo_inicial_controle_cat1799() {
		return sp_data_saldo_inicial_controle_cat1799;
	}

	public Double getSp_valor_unitario_controle_cat1799() {
		return sp_valor_unitario_controle_cat1799;
	}

	public Double getSp_quantidade_controle_cat1799() {
		return sp_quantidade_controle_cat1799;
	}

	public Double getSp_valor_final_controle_cat1799() {
		return sp_valor_final_controle_cat1799;
	}

	public Integer getSped_genero() {
		return sped_genero;
	}

	public Integer getSped_codigoservico() {
		return sped_codigoservico;
	}

	public Integer getSped_tipoitem() {
		return sped_tipoitem;
	}

	public Integer getSped_classificacao() {
		return sped_classificacao;
	}

	public Integer getSped_conta_contabil_estoque_emseupoder() {
		return sped_conta_contabil_estoque_emseupoder;
	}

	public Integer getSped_conta_contabil_estoque_empoderterceiros() {
		return sped_conta_contabil_estoque_empoderterceiros;
	}

	public Integer getSped_conta_contabil_estoque_terceirosemseupoder() {
		return sped_conta_contabil_estoque_terceirosemseupoder;
	}

	public String getSped_tiporeceita() {
		return sped_tiporeceita;
	}

	public Integer getSped_energiaeletrica_gascanalizado() {
		return sped_energiaeletrica_gascanalizado;
	}

	public Date getDatacadastro() {
		return datacadastro;
	}

	public String getProduto_escrituradolmc() {
		return produto_escrituradolmc;
	}

	public String getCodigocombustivel_tabeladf() {
		return codigocombustivel_tabeladf;
	}

	public String getCodigocombustivel_tabelaanp() {
		return codigocombustivel_tabelaanp;
	}

	public String getProdutorelacionado_inciso_art8_mp5402011() {
		return produtorelacionado_inciso_art8_mp5402011;
	}

	public String getPermitir_informar_descricaocomplementar_lancto_nota() {
		return permitir_informar_descricaocomplementar_lancto_nota;
	}

	public Integer getCodigo_atividade_inssfolha() {
		return codigo_atividade_inssfolha;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigoproduto(String codigoproduto) {
		this.codigoproduto = codigoproduto;
	}

	public void setDescricaoproduto(String descricaoproduto) {
		this.descricaoproduto = descricaoproduto;
	}

	public void setCodigonbm(String codigonbm) {
		this.codigonbm = codigonbm;
	}

	public void setCodigoncm(String codigoncm) {
		this.codigoncm = codigoncm;
	}

	public void setCodigoncmexterior(Integer codigoncmexterior) {
		this.codigoncmexterior = codigoncmexterior;
	}

	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}

	public void setCodigoimpostoimportacao(Integer codigoimpostoimportacao) {
		this.codigoimpostoimportacao = codigoimpostoimportacao;
	}

	public void setCodigogrupoprodutos(Integer codigogrupoprodutos) {
		this.codigogrupoprodutos = codigogrupoprodutos;
	}

	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setUnidademedida_inventariada_dif_comercializada(
			String unidademedidaInventariadaDifComercializada) {
		unidademedida_inventariada_dif_comercializada = unidademedidaInventariadaDifComercializada;
	}

	public void setTipoproduto(String tipoproduto) {
		this.tipoproduto = tipoproduto;
	}

	public void setTipoarmafogo(Integer tipoarmafogo) {
		this.tipoarmafogo = tipoarmafogo;
	}

	public void setDescricao_armafogo(String descricaoArmafogo) {
		descricao_armafogo = descricaoArmafogo;
	}

	public void setTipomedicamento(Integer tipomedicamento) {
		this.tipomedicamento = tipomedicamento;
	}

	public void setServico_tributado_issqn(String servicoTributadoIssqn) {
		servico_tributado_issqn = servicoTributadoIssqn;
	}

	public void setCodigo_chassiveiculo(String codigoChassiveiculo) {
		codigo_chassiveiculo = codigoChassiveiculo;
	}

	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public void setQuantidade_inicial_estoque(Double quantidadeInicialEstoque) {
		quantidade_inicial_estoque = quantidadeInicialEstoque;
	}

	public void setValor_inicial_estoque(Double valorInicialEstoque) {
		valor_inicial_estoque = valorInicialEstoque;
	}

	public void setCodigo_situacaotributaria_icms(
			Integer codigoSituacaotributariaIcms) {
		codigo_situacaotributaria_icms = codigoSituacaotributariaIcms;
	}

	public void setAliquotaicms(Double aliquotaicms) {
		this.aliquotaicms = aliquotaicms;
	}

	public void setAliquotaipi(Double aliquotaipi) {
		this.aliquotaipi = aliquotaipi;
	}

	public void setPeriodicidadeipi(String periodicidadeipi) {
		this.periodicidadeipi = periodicidadeipi;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setExportarprodutodnf(String exportarprodutodnf) {
		this.exportarprodutodnf = exportarprodutodnf;
	}

	public void setExtipi(String extipi) {
		this.extipi = extipi;
	}

	public void setDnf_codigoespecie_produto(Integer dnfCodigoespecieProduto) {
		dnf_codigoespecie_produto = dnfCodigoespecieProduto;
	}

	public void setDnf_unidademedida_padrao(Integer dnfUnidademedidaPadrao) {
		dnf_unidademedida_padrao = dnfUnidademedidaPadrao;
	}

	public void setDnf_fatorconversao(Double dnfFatorconversao) {
		dnf_fatorconversao = dnfFatorconversao;
	}

	public void setDnf_codigoproduto(String dnfCodigoproduto) {
		dnf_codigoproduto = dnfCodigoproduto;
	}

	public void setDnf_capacidade_volumetrica(Double dnfCapacidadeVolumetrica) {
		dnf_capacidade_volumetrica = dnfCapacidadeVolumetrica;
	}

	public void setSedic_codigoean(String sedicCodigoean) {
		sedic_codigoean = sedicCodigoean;
	}

	public void setSedic_codigoprodutorelevante(Integer sedicCodigoprodutorelevante) {
		sedic_codigoprodutorelevante = sedicCodigoprodutorelevante;
	}

	public void setScanc_gerascanc(String scancGerascanc) {
		scanc_gerascanc = scancGerascanc;
	}

	public void setScanc_codigoprodutoscanc(Integer scancCodigoprodutoscanc) {
		scanc_codigoprodutoscanc = scancCodigoprodutoscanc;
	}

	public void setScanc_contemgasolinaa(String scancContemgasolinaa) {
		scanc_contemgasolinaa = scancContemgasolinaa;
	}

	public void setScanc_tipoproduto(String scancTipoproduto) {
		scanc_tipoproduto = scancTipoproduto;
	}

	public void setGrfctb_geragrfctb(String grfctbGeragrfctb) {
		grfctb_geragrfctb = grfctbGeragrfctb;
	}

	public void setGrfctb_codigoproduto(Integer grfctbCodigoproduto) {
		grfctb_codigoproduto = grfctbCodigoproduto;
	}

	public void setDief_unidade(String diefUnidade) {
		dief_unidade = diefUnidade;
	}

	public void setDief_tipoprodutoservico(Integer diefTipoprodutoservico) {
		dief_tipoprodutoservico = diefTipoprodutoservico;
	}

	public void setSt88_informaregistro88stsintegra(
			String st88Informaregistro88stsintegra) {
		st88_informaregistro88stsintegra = st88Informaregistro88stsintegra;
	}

	public void setSt88_codigotabelasefaz(Integer st88Codigotabelasefaz) {
		st88_codigotabelasefaz = st88Codigotabelasefaz;
	}

	public void setGo_informacoes_complementares_ipmpdi(
			String goInformacoesComplementaresIpmpdi) {
		go_informacoes_complementares_ipmpdi = goInformacoesComplementaresIpmpdi;
	}

	public void setGo_codigoprodutoservico_ipmpdi(
			Integer goCodigoprodutoservicoIpmpdi) {
		go_codigoprodutoservico_ipmpdi = goCodigoprodutoservicoIpmpdi;
	}

	public void setGo_produto_relacionado(String goProdutoRelacionado) {
		go_produto_relacionado = goProdutoRelacionado;
	}

	public void setAm_cestabasica(String amCestabasica) {
		am_cestabasica = amCestabasica;
	}

	public void setAm_codigoprodutodam(Integer amCodigoprodutodam) {
		am_codigoprodutodam = amCodigoprodutodam;
	}

	public void setRs_produto_incluido_campo_st(String rsProdutoIncluidoCampoSt) {
		rs_produto_incluido_campo_st = rsProdutoIncluidoCampoSt;
	}

	public void setRs_data_inicio_st(Date rsDataInicioSt) {
		rs_data_inicio_st = rsDataInicioSt;
	}

	public void setRs_produto_precotabelado(String rsProdutoPrecotabelado) {
		rs_produto_precotabelado = rsProdutoPrecotabelado;
	}

	public void setRs_valor_unitario_st(Double rsValorUnitarioSt) {
		rs_valor_unitario_st = rsValorUnitarioSt;
	}

	public void setRs_mvast(Double rsMvast) {
		rs_mvast = rsMvast;
	}

	public void setRs_grupost(Integer rsGrupost) {
		rs_grupost = rsGrupost;
	}

	public void setPr_equipamentoecf(String prEquipamentoecf) {
		pr_equipamentoecf = prEquipamentoecf;
	}

	public void setMs_possui_incentivo_fiscal(String msPossuiIncentivoFiscal) {
		ms_possui_incentivo_fiscal = msPossuiIncentivoFiscal;
	}

	public void setDf_produto_sujeito_regimeespecial(
			Integer dfProdutoSujeitoRegimeespecial) {
		df_produto_sujeito_regimeespecial = dfProdutoSujeitoRegimeespecial;
	}

	public void setDf_item_padrao_regimeespecial(Integer dfItemPadraoRegimeespecial) {
		df_item_padrao_regimeespecial = dfItemPadraoRegimeespecial;
	}

	public void setPe_tipoproduto(Integer peTipoproduto) {
		pe_tipoproduto = peTipoproduto;
	}

	public void setSp_controla_ressarcimento_cat1799(
			String spControlaRessarcimentoCat1799) {
		sp_controla_ressarcimento_cat1799 = spControlaRessarcimentoCat1799;
	}

	public void setSp_data_saldo_inicial_controle_cat1799(
			Double spDataSaldoInicialControleCat1799) {
		sp_data_saldo_inicial_controle_cat1799 = spDataSaldoInicialControleCat1799;
	}

	public void setSp_valor_unitario_controle_cat1799(
			Double spValorUnitarioControleCat1799) {
		sp_valor_unitario_controle_cat1799 = spValorUnitarioControleCat1799;
	}

	public void setSp_quantidade_controle_cat1799(Double spQuantidadeControleCat1799) {
		sp_quantidade_controle_cat1799 = spQuantidadeControleCat1799;
	}

	public void setSp_valor_final_controle_cat1799(
			Double spValorFinalControleCat1799) {
		sp_valor_final_controle_cat1799 = spValorFinalControleCat1799;
	}

	public void setSped_genero(Integer spedGenero) {
		sped_genero = spedGenero;
	}

	public void setSped_codigoservico(Integer spedCodigoservico) {
		sped_codigoservico = spedCodigoservico;
	}

	public void setSped_tipoitem(Integer spedTipoitem) {
		sped_tipoitem = spedTipoitem;
	}

	public void setSped_classificacao(Integer spedClassificacao) {
		sped_classificacao = spedClassificacao;
	}

	public void setSped_conta_contabil_estoque_emseupoder(
			Integer spedContaContabilEstoqueEmseupoder) {
		sped_conta_contabil_estoque_emseupoder = spedContaContabilEstoqueEmseupoder;
	}

	public void setSped_conta_contabil_estoque_empoderterceiros(
			Integer spedContaContabilEstoqueEmpoderterceiros) {
		sped_conta_contabil_estoque_empoderterceiros = spedContaContabilEstoqueEmpoderterceiros;
	}

	public void setSped_conta_contabil_estoque_terceirosemseupoder(
			Integer spedContaContabilEstoqueTerceirosemseupoder) {
		sped_conta_contabil_estoque_terceirosemseupoder = spedContaContabilEstoqueTerceirosemseupoder;
	}

	public void setSped_tiporeceita(String spedTiporeceita) {
		sped_tiporeceita = spedTiporeceita;
	}

	public void setSped_energiaeletrica_gascanalizado(
			Integer spedEnergiaeletricaGascanalizado) {
		sped_energiaeletrica_gascanalizado = spedEnergiaeletricaGascanalizado;
	}

	public void setDatacadastro(Date datacadastro) {
		this.datacadastro = datacadastro;
	}

	public void setProduto_escrituradolmc(String produtoEscrituradolmc) {
		produto_escrituradolmc = produtoEscrituradolmc;
	}

	public void setCodigocombustivel_tabeladf(String codigocombustivelTabeladf) {
		codigocombustivel_tabeladf = codigocombustivelTabeladf;
	}

	public void setCodigocombustivel_tabelaanp(String codigocombustivelTabelaanp) {
		codigocombustivel_tabelaanp = codigocombustivelTabelaanp;
	}

	public void setProdutorelacionado_inciso_art8_mp5402011(
			String produtorelacionadoIncisoArt8Mp5402011) {
		produtorelacionado_inciso_art8_mp5402011 = produtorelacionadoIncisoArt8Mp5402011;
	}

	public void setPermitir_informar_descricaocomplementar_lancto_nota(
			String permitirInformarDescricaocomplementarLanctoNota) {
		permitir_informar_descricaocomplementar_lancto_nota = permitirInformarDescricaocomplementarLanctoNota;
	}

	public void setCodigo_atividade_inssfolha(Integer codigoAtividadeInssfolha) {
		codigo_atividade_inssfolha = codigoAtividadeInssfolha;
	}
}
