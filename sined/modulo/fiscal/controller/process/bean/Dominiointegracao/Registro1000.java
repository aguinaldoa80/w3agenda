package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro1000 {
	
	public final static String REG = "1000";
	
	private String identificacao;
	private Integer codigo_especie;
	private String inscricao_fornecedor;
	private Integer codigo_exclusao_dief;
	private Integer codigo_acumulador;
	private String cfop;
	private Integer seguimento;
	private String numero_documento;
	private String serie;
	private String numero_documento_final;
	private Date data_entrada;
	private Date data_emissao;
	private Double valor_contabil;
	private Double valor_exclusao_dief;
	private String obsevacao;
	private String modalidade_frete;
	private String emitente_notafiscal;
	private Integer cfop_estendido_detalhamento;
	private Integer codigo_transferencia_credito;
	private String codigo_recolhimento_iss_retido;
	private String codigo_recolhimento_irrf;
	private Integer codigo_observacao;
	private Date data_visto_notas_transf_cred_icms;
	private String fato_gerador_crf;
	private String fato_gerador_irrf;
	private Double valor_frete;
	private Double valor_seguro;
	private Double valor_despesas_acessorias;
	private Double valor_pis;
	private Integer codigo_iden_tipo_antecipacao_tributaria;
	private Double valor_cofins;
	private Double valor_calc_ref_tare_nota;
	private Double aliquota_valor_calc_ref_tarf_nota;
	private Integer valor_bc_icms_st;
	private Double entradasaida_isenta;
	private Double outras_entradas_isentas;
	private Double valor_transporte_incluido_base;
	private Integer codigo_ressarcimento;
	private Double valor_produtos;
	private String codigo_municipio;
	private String codigo_modelo_doc_fiscal;
	private Integer codigo_situacao_tributaria;
	private Integer sub_serie;
	private String inscricao_estadual_fornecedor;
	private String inscricao_municipal_fornecedor;
	private String codigo_operacao_prestacao;
	private Double valor_deduzido_receita_tributavel;
	private Date competencia;
	private Integer operacao;
	private String numero_parecer_fiscal;
	private Date data_parecer_fiscal;
	private String numero_declaracao_importacao;
	private String possui_benficio_fiscal;
	private String chave_notafiscal_eletronica;
	private String codigo_recolhimento_fethab;
	private String responsavel_recolhimento_fethab;
	private Integer cfop_doc_fiscal;
	private Integer tipo_cte;
	private String cte_referencia;
	private Integer modalidade_importacao;
	private Integer codigo_informacao_complementar;
	private String informacao_complementar;
	private Integer classe_consumo;
	private Integer tipo_ligacao;
	private Integer grupo_tensao;
	private Integer tipo_assinante;
	private Integer kwh_consumido;
	private Double valor_fornecido_consumido_gas_energiaeletrica;
	private Double valor_cobrado_terceiros;
	private Integer tipo_doc_importacao;
	private String numero_ato_concessorio_regime_drawback;
	private Integer natureza_frete_piscofins;
	private String cst_piscofins;
	private String base_credito_piscofins;
	private Double valor_servico_itens_piscofins;
	private Double base_calculo_piscofins;
	private Double aliquota_pis;
	private Double aliquota_cofins;
	private String chave_nfse;
	private String numero_processo_ato_concessorio;
	private String origem_processo;
	private Date data_escrituracao;
	private Integer cfps;
	private Integer natureza_receita_piscofins;
	
	private List<Registro1020> listaRegistro1020;
	
	@Override
	public String toString() {
		return 	SPEDUtil.printString(getIdentificacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_especie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_fornecedor()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_exclusao_dief()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_acumulador()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCfop()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getSeguimento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNumero_documento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getSerie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNumero_documento_final()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_entrada()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_emissao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_contabil()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_exclusao_dief()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getObsevacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getModalidade_frete()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getEmitente_notafiscal()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCfop_estendido_detalhamento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_transferencia_credito()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_recolhimento_iss_retido()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_recolhimento_irrf()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_observacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_visto_notas_transf_cred_icms()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_crf()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_irrf()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_frete()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_seguro()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_despesas_acessorias()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_pis()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_iden_tipo_antecipacao_tributaria()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_cofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_calc_ref_tare_nota()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getAliquota_valor_calc_ref_tarf_nota()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getValor_bc_icms_st()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getEntradasaida_isenta()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getOutras_entradas_isentas()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_transporte_incluido_base()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_ressarcimento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_produtos()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_municipio()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_modelo_doc_fiscal()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_situacao_tributaria()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getSub_serie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_estadual_fornecedor()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_municipal_fornecedor()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_operacao_prestacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_deduzido_receita_tributavel()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getCompetencia()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getOperacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNumero_parecer_fiscal()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDate(getData_parecer_fiscal()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNumero_declaracao_importacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getPossui_benficio_fiscal()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getChave_notafiscal_eletronica()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_recolhimento_fethab()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getResponsavel_recolhimento_fethab()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCfop_doc_fiscal()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getTipo_cte()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCte_referencia()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getModalidade_importacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_informacao_complementar()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInformacao_complementar()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getClasse_consumo()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getTipo_ligacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getGrupo_tensao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getTipo_assinante()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getKwh_consumido()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_fornecido_consumido_gas_energiaeletrica()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_cobrado_terceiros()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getTipo_doc_importacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNumero_ato_concessorio_regime_drawback()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getNatureza_frete_piscofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCst_piscofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getBase_credito_piscofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_servico_itens_piscofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getBase_calculo_piscofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getAliquota_pis()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getAliquota_cofins()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getChave_nfse()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNumero_processo_ato_concessorio()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getOrigem_processo()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_escrituracao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCfps()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getNatureza_receita_piscofins()) + SPEDUtil.SEPARADOR_CAMPO + SIntegraUtil.SEPARADOR_LINHA +
				
				SPEDUtil.printLista(getListaRegistro1020());
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Integer getCodigo_especie() {
		return codigo_especie;
	}

	public String getInscricao_fornecedor() {
		return inscricao_fornecedor;
	}

	public Integer getCodigo_exclusao_dief() {
		return codigo_exclusao_dief;
	}

	public Integer getCodigo_acumulador() {
		return codigo_acumulador;
	}

	public String getCfop() {
		return cfop;
	}

	public Integer getSeguimento() {
		return seguimento;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public String getSerie() {
		return serie;
	}

	public String getNumero_documento_final() {
		return numero_documento_final;
	}

	public Date getData_entrada() {
		return data_entrada;
	}

	public Date getData_emissao() {
		return data_emissao;
	}

	public Double getValor_contabil() {
		return valor_contabil;
	}

	public Double getValor_exclusao_dief() {
		return valor_exclusao_dief;
	}

	public String getObsevacao() {
		return obsevacao;
	}

	public String getModalidade_frete() {
		return modalidade_frete;
	}

	public String getEmitente_notafiscal() {
		return emitente_notafiscal;
	}

	public Integer getCfop_estendido_detalhamento() {
		return cfop_estendido_detalhamento;
	}

	public Integer getCodigo_transferencia_credito() {
		return codigo_transferencia_credito;
	}

	public String getCodigo_recolhimento_iss_retido() {
		return codigo_recolhimento_iss_retido;
	}

	public String getCodigo_recolhimento_irrf() {
		return codigo_recolhimento_irrf;
	}

	public Integer getCodigo_observacao() {
		return codigo_observacao;
	}

	public Date getData_visto_notas_transf_cred_icms() {
		return data_visto_notas_transf_cred_icms;
	}

	public String getFato_gerador_crf() {
		return fato_gerador_crf;
	}

	public String getFato_gerador_irrf() {
		return fato_gerador_irrf;
	}

	public Double getValor_frete() {
		return valor_frete;
	}

	public Double getValor_seguro() {
		return valor_seguro;
	}

	public Double getValor_despesas_acessorias() {
		return valor_despesas_acessorias;
	}

	public Double getValor_pis() {
		return valor_pis;
	}

	public Integer getCodigo_iden_tipo_antecipacao_tributaria() {
		return codigo_iden_tipo_antecipacao_tributaria;
	}

	public Double getValor_cofins() {
		return valor_cofins;
	}

	public Double getValor_calc_ref_tare_nota() {
		return valor_calc_ref_tare_nota;
	}

	public Double getAliquota_valor_calc_ref_tarf_nota() {
		return aliquota_valor_calc_ref_tarf_nota;
	}

	public Integer getValor_bc_icms_st() {
		return valor_bc_icms_st;
	}

	public Double getEntradasaida_isenta() {
		return entradasaida_isenta;
	}

	public Double getOutras_entradas_isentas() {
		return outras_entradas_isentas;
	}

	public Double getValor_transporte_incluido_base() {
		return valor_transporte_incluido_base;
	}

	public Integer getCodigo_ressarcimento() {
		return codigo_ressarcimento;
	}

	public Double getValor_produtos() {
		return valor_produtos;
	}

	public String getCodigo_municipio() {
		return codigo_municipio;
	}

	public String getCodigo_modelo_doc_fiscal() {
		return codigo_modelo_doc_fiscal;
	}

	public Integer getCodigo_situacao_tributaria() {
		return codigo_situacao_tributaria;
	}

	public Integer getSub_serie() {
		return sub_serie;
	}

	public String getInscricao_estadual_fornecedor() {
		return inscricao_estadual_fornecedor;
	}

	public String getInscricao_municipal_fornecedor() {
		return inscricao_municipal_fornecedor;
	}

	public String getCodigo_operacao_prestacao() {
		return codigo_operacao_prestacao;
	}

	public Double getValor_deduzido_receita_tributavel() {
		return valor_deduzido_receita_tributavel;
	}

	public Date getCompetencia() {
		return competencia;
	}

	public Integer getOperacao() {
		return operacao;
	}

	public String getNumero_parecer_fiscal() {
		return numero_parecer_fiscal;
	}

	public Date getData_parecer_fiscal() {
		return data_parecer_fiscal;
	}

	public String getNumero_declaracao_importacao() {
		return numero_declaracao_importacao;
	}

	public String getPossui_benficio_fiscal() {
		return possui_benficio_fiscal;
	}

	public String getChave_notafiscal_eletronica() {
		return chave_notafiscal_eletronica;
	}

	public String getCodigo_recolhimento_fethab() {
		return codigo_recolhimento_fethab;
	}
	
	public String getResponsavel_recolhimento_fethab() {
		return responsavel_recolhimento_fethab;
	}

	public Integer getCfop_doc_fiscal() {
		return cfop_doc_fiscal;
	}

	public Integer getTipo_cte() {
		return tipo_cte;
	}

	public String getCte_referencia() {
		return cte_referencia;
	}

	public Integer getModalidade_importacao() {
		return modalidade_importacao;
	}

	public Integer getCodigo_informacao_complementar() {
		return codigo_informacao_complementar;
	}

	public String getInformacao_complementar() {
		return informacao_complementar;
	}

	public Integer getClasse_consumo() {
		return classe_consumo;
	}

	public Integer getTipo_ligacao() {
		return tipo_ligacao;
	}

	public Integer getGrupo_tensao() {
		return grupo_tensao;
	}

	public Integer getTipo_assinante() {
		return tipo_assinante;
	}

	public Integer getKwh_consumido() {
		return kwh_consumido;
	}

	public Double getValor_fornecido_consumido_gas_energiaeletrica() {
		return valor_fornecido_consumido_gas_energiaeletrica;
	}

	public Double getValor_cobrado_terceiros() {
		return valor_cobrado_terceiros;
	}

	public Integer getTipo_doc_importacao() {
		return tipo_doc_importacao;
	}

	public String getNumero_ato_concessorio_regime_drawback() {
		return numero_ato_concessorio_regime_drawback;
	}

	public Integer getNatureza_frete_piscofins() {
		return natureza_frete_piscofins;
	}

	public String getCst_piscofins() {
		return cst_piscofins;
	}

	public String getBase_credito_piscofins() {
		return base_credito_piscofins;
	}

	public Double getValor_servico_itens_piscofins() {
		return valor_servico_itens_piscofins;
	}

	public Double getBase_calculo_piscofins() {
		return base_calculo_piscofins;
	}

	public Double getAliquota_pis() {
		return aliquota_pis;
	}

	public Double getAliquota_cofins() {
		return aliquota_cofins;
	}

	public String getChave_nfse() {
		return chave_nfse;
	}

	public String getNumero_processo_ato_concessorio() {
		return numero_processo_ato_concessorio;
	}

	public String getOrigem_processo() {
		return origem_processo;
	}

	public Date getData_escrituracao() {
		return data_escrituracao;
	}

	public Integer getCfps() {
		return cfps;
	}

	public Integer getNatureza_receita_piscofins() {
		return natureza_receita_piscofins;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_especie(Integer codigoEspecie) {
		codigo_especie = codigoEspecie;
	}

	public void setInscricao_fornecedor(String inscricaoFornecedor) {
		inscricao_fornecedor = inscricaoFornecedor;
	}

	public void setCodigo_exclusao_dief(Integer codigoExclusaoDief) {
		codigo_exclusao_dief = codigoExclusaoDief;
	}

	public void setCodigo_acumulador(Integer codigoAcumulador) {
		codigo_acumulador = codigoAcumulador;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	public void setSeguimento(Integer seguimento) {
		this.seguimento = seguimento;
	}

	public void setNumero_documento(String numeroDocumento) {
		numero_documento = numeroDocumento;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setNumero_documento_final(String numeroDocumentoFinal) {
		numero_documento_final = numeroDocumentoFinal;
	}

	public void setData_entrada(Date dataEntrada) {
		data_entrada = dataEntrada;
	}

	public void setData_emissao(Date dataEmissao) {
		data_emissao = dataEmissao;
	}

	public void setValor_contabil(Double valorContabil) {
		valor_contabil = valorContabil;
	}

	public void setValor_exclusao_dief(Double valorExclusaoDief) {
		valor_exclusao_dief = valorExclusaoDief;
	}

	public void setObsevacao(String obsevacao) {
		this.obsevacao = obsevacao;
	}

	public void setModalidade_frete(String modalidadeFrete) {
		modalidade_frete = modalidadeFrete;
	}

	public void setEmitente_notafiscal(String emitenteNotafiscal) {
		emitente_notafiscal = emitenteNotafiscal;
	}

	public void setCfop_estendido_detalhamento(Integer cfopEstendidoDetalhamento) {
		cfop_estendido_detalhamento = cfopEstendidoDetalhamento;
	}

	public void setCodigo_transferencia_credito(Integer codigoTransferenciaCredito) {
		codigo_transferencia_credito = codigoTransferenciaCredito;
	}

	public void setCodigo_recolhimento_iss_retido(String codigoRecolhimentoIssRetido) {
		codigo_recolhimento_iss_retido = codigoRecolhimentoIssRetido;
	}

	public void setCodigo_recolhimento_irrf(String codigoRecolhimentoIrrf) {
		codigo_recolhimento_irrf = codigoRecolhimentoIrrf;
	}

	public void setCodigo_observacao(Integer codigoObservacao) {
		codigo_observacao = codigoObservacao;
	}

	public void setData_visto_notas_transf_cred_icms(
			Date dataVistoNotasTransfCredIcms) {
		data_visto_notas_transf_cred_icms = dataVistoNotasTransfCredIcms;
	}

	public void setFato_gerador_crf(String fatoGeradorCrf) {
		fato_gerador_crf = fatoGeradorCrf;
	}

	public void setFato_gerador_irrf(String fatoGeradorIrrf) {
		fato_gerador_irrf = fatoGeradorIrrf;
	}

	public void setValor_frete(Double valorFrete) {
		valor_frete = valorFrete;
	}

	public void setValor_seguro(Double valorSeguro) {
		valor_seguro = valorSeguro;
	}

	public void setValor_despesas_acessorias(Double valorDespesasAcessorias) {
		valor_despesas_acessorias = valorDespesasAcessorias;
	}

	public void setValor_pis(Double valorPis) {
		valor_pis = valorPis;
	}

	public void setCodigo_iden_tipo_antecipacao_tributaria(
			Integer codigoIdenTipoAntecipacaoTributaria) {
		codigo_iden_tipo_antecipacao_tributaria = codigoIdenTipoAntecipacaoTributaria;
	}

	public void setValor_cofins(Double valorCofins) {
		valor_cofins = valorCofins;
	}

	public void setValor_calc_ref_tare_nota(Double valorCalcRefTareNota) {
		valor_calc_ref_tare_nota = valorCalcRefTareNota;
	}

	public void setAliquota_valor_calc_ref_tarf_nota(
			Double aliquotaValorCalcRefTarfNota) {
		aliquota_valor_calc_ref_tarf_nota = aliquotaValorCalcRefTarfNota;
	}

	public void setValor_bc_icms_st(Integer valorBcIcmsSt) {
		valor_bc_icms_st = valorBcIcmsSt;
	}

	public void setEntradasaida_isenta(Double entradasaidaIsenta) {
		entradasaida_isenta = entradasaidaIsenta;
	}

	public void setOutras_entradas_isentas(Double outrasEntradasIsentas) {
		outras_entradas_isentas = outrasEntradasIsentas;
	}

	public void setValor_transporte_incluido_base(Double valorTransporteIncluidoBase) {
		valor_transporte_incluido_base = valorTransporteIncluidoBase;
	}

	public void setCodigo_ressarcimento(Integer codigoRessarcimento) {
		codigo_ressarcimento = codigoRessarcimento;
	}

	public void setValor_produtos(Double valorProdutos) {
		valor_produtos = valorProdutos;
	}

	public void setCodigo_municipio(String codigoMunicipio) {
		codigo_municipio = codigoMunicipio;
	}

	public void setCodigo_modelo_doc_fiscal(String codigoModeloDocFiscal) {
		codigo_modelo_doc_fiscal = codigoModeloDocFiscal;
	}

	public void setCodigo_situacao_tributaria(Integer codigoSituacaoTributaria) {
		codigo_situacao_tributaria = codigoSituacaoTributaria;
	}

	public void setSub_serie(Integer subSerie) {
		sub_serie = subSerie;
	}

	public void setInscricao_estadual_fornecedor(String inscricaoEstadualFornecedor) {
		inscricao_estadual_fornecedor = inscricaoEstadualFornecedor;
	}

	public void setInscricao_municipal_fornecedor(
			String inscricaoMunicipalFornecedor) {
		inscricao_municipal_fornecedor = inscricaoMunicipalFornecedor;
	}

	public void setCodigo_operacao_prestacao(String codigoOperacaoPrestacao) {
		codigo_operacao_prestacao = codigoOperacaoPrestacao;
	}

	public void setValor_deduzido_receita_tributavel(
			Double valorDeduzidoReceitaTributavel) {
		valor_deduzido_receita_tributavel = valorDeduzidoReceitaTributavel;
	}

	public void setCompetencia(Date competencia) {
		this.competencia = competencia;
	}

	public void setOperacao(Integer operacao) {
		this.operacao = operacao;
	}

	public void setNumero_parecer_fiscal(String numeroParecerFiscal) {
		numero_parecer_fiscal = numeroParecerFiscal;
	}

	public void setData_parecer_fiscal(Date dataParecerFiscal) {
		data_parecer_fiscal = dataParecerFiscal;
	}

	public void setNumero_declaracao_importacao(String numeroDeclaracaoImportacao) {
		numero_declaracao_importacao = numeroDeclaracaoImportacao;
	}

	public void setPossui_benficio_fiscal(String possuiBenficioFiscal) {
		possui_benficio_fiscal = possuiBenficioFiscal;
	}

	public void setChave_notafiscal_eletronica(String chaveNotafiscalEletronica) {
		chave_notafiscal_eletronica = chaveNotafiscalEletronica;
	}

	public void setCodigo_recolhimento_fethab(String codigoRecolhimentoFethab) {
		codigo_recolhimento_fethab = codigoRecolhimentoFethab;
	}
	
	public void setResponsavel_recolhimento_fethab(String responsavelRecolhimentoFethab) {
		responsavel_recolhimento_fethab = responsavelRecolhimentoFethab;
	}

	public void setCfop_doc_fiscal(Integer cfopDocFiscal) {
		cfop_doc_fiscal = cfopDocFiscal;
	}

	public void setTipo_cte(Integer tipoCte) {
		tipo_cte = tipoCte;
	}

	public void setCte_referencia(String cteReferencia) {
		cte_referencia = cteReferencia;
	}

	public void setModalidade_importacao(Integer modalidadeImportacao) {
		modalidade_importacao = modalidadeImportacao;
	}

	public void setCodigo_informacao_complementar(
			Integer codigoInformacaoComplementar) {
		codigo_informacao_complementar = codigoInformacaoComplementar;
	}

	public void setInformacao_complementar(String informacaoComplementar) {
		informacao_complementar = informacaoComplementar;
	}

	public void setClasse_consumo(Integer classeConsumo) {
		classe_consumo = classeConsumo;
	}

	public void setTipo_ligacao(Integer tipoLigacao) {
		tipo_ligacao = tipoLigacao;
	}

	public void setGrupo_tensao(Integer grupoTensao) {
		grupo_tensao = grupoTensao;
	}

	public void setTipo_assinante(Integer tipoAssinante) {
		tipo_assinante = tipoAssinante;
	}

	public void setKwh_consumido(Integer kwhConsumido) {
		kwh_consumido = kwhConsumido;
	}

	public void setValor_fornecido_consumido_gas_energiaeletrica(
			Double valorFornecidoConsumidoGasEnergiaeletrica) {
		valor_fornecido_consumido_gas_energiaeletrica = valorFornecidoConsumidoGasEnergiaeletrica;
	}

	public void setValor_cobrado_terceiros(Double valorCobradoTerceiros) {
		valor_cobrado_terceiros = valorCobradoTerceiros;
	}

	public void setTipo_doc_importacao(Integer tipoDocImportacao) {
		tipo_doc_importacao = tipoDocImportacao;
	}

	public void setNumero_ato_concessorio_regime_drawback(
			String numeroAtoConcessorioRegimeDrawback) {
		numero_ato_concessorio_regime_drawback = numeroAtoConcessorioRegimeDrawback;
	}

	public void setNatureza_frete_piscofins(Integer naturezaFretePiscofins) {
		natureza_frete_piscofins = naturezaFretePiscofins;
	}

	public void setCst_piscofins(String cstPiscofins) {
		cst_piscofins = cstPiscofins;
	}

	public void setBase_credito_piscofins(String baseCreditoPiscofins) {
		base_credito_piscofins = baseCreditoPiscofins;
	}

	public void setValor_servico_itens_piscofins(Double valorServicoItensPiscofins) {
		valor_servico_itens_piscofins = valorServicoItensPiscofins;
	}

	public void setBase_calculo_piscofins(Double baseCalculoPiscofins) {
		base_calculo_piscofins = baseCalculoPiscofins;
	}

	public void setAliquota_pis(Double aliquotaPis) {
		aliquota_pis = aliquotaPis;
	}

	public void setAliquota_cofins(Double aliquotaCofins) {
		aliquota_cofins = aliquotaCofins;
	}

	public void setChave_nfse(String chaveNfse) {
		chave_nfse = chaveNfse;
	}

	public void setNumero_processo_ato_concessorio(
			String numeroProcessoAtoConcessorio) {
		numero_processo_ato_concessorio = numeroProcessoAtoConcessorio;
	}

	public void setOrigem_processo(String origemProcesso) {
		origem_processo = origemProcesso;
	}

	public void setData_escrituracao(Date dataEscrituracao) {
		data_escrituracao = dataEscrituracao;
	}

	public void setCfps(Integer cfps) {
		this.cfps = cfps;
	}

	public void setNatureza_receita_piscofins(Integer naturezaReceitaPiscofins) {
		natureza_receita_piscofins = naturezaReceitaPiscofins;
	}

	public List<Registro1020> getListaRegistro1020() {
		return listaRegistro1020;
	}

	public void setListaRegistro1020(List<Registro1020> listaRegistro1020) {
		this.listaRegistro1020 = listaRegistro1020;
	}
}
