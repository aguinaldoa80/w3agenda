package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;


public class Registro2310 {
	
	public final static String REG = "2310";
	
	private String identificacao;
	private Integer codigo_centrocusto_debito;
	private Integer codigo_centrocusto_credito;
	private Double valor_rateio;
	
	@Override
	public String toString() {
		return  SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Integer getCodigo_centrocusto_debito() {
		return codigo_centrocusto_debito;
	}

	public Integer getCodigo_centrocusto_credito() {
		return codigo_centrocusto_credito;
	}

	public Double getValor_rateio() {
		return valor_rateio;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_centrocusto_debito(Integer codigoCentrocustoDebito) {
		codigo_centrocusto_debito = codigoCentrocustoDebito;
	}

	public void setCodigo_centrocusto_credito(Integer codigoCentrocustoCredito) {
		codigo_centrocusto_credito = codigoCentrocustoCredito;
	}

	public void setValor_rateio(Double valorRateio) {
		valor_rateio = valorRateio;
	}
}
