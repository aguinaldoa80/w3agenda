package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0300 {
	
	public final static String REG = "0300";
	
	private String identificacao;
	private String numero_serie_equipamento;
	private String descricao;
	private Integer tipo;
	private String modelo;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getNumero_serie_equipamento()) +  SPEDUtil.SEPARADOR_CAMPO +  
				SPEDUtil.printString(getDescricao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getTipo()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getModelo()) +  SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getNumero_serie_equipamento() {
		return numero_serie_equipamento;
	}

	public String getDescricao() {
		return descricao;
	}

	public Integer getTipo() {
		return tipo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setNumero_serie_equipamento(String numeroSerieEquipamento) {
		numero_serie_equipamento = numeroSerieEquipamento;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
}
