package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0240 {
	
	public final static String REG = "0240";
	
	private String identificacao;
	private Integer codigo;
	private String descricao;
	private Integer codigo_departamento;
	private Date data_cadastro;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo()) +  SPEDUtil.SEPARADOR_CAMPO +  
				SPEDUtil.printString(getDescricao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo_departamento()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDate(getData_cadastro()) +  SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public Integer getCodigo_departamento() {
		return codigo_departamento;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setCodigo_departamento(Integer codigoDepartamento) {
		codigo_departamento = codigoDepartamento;
	}

	public void setData_cadastro(Date dataCadastro) {
		data_cadastro = dataCadastro;
	}
}
