package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0150 {
	
	public final static String REG = "0150";
	
	private String identificacao;
	private String silga;
	private String descricao;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getSilga()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDescricao()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getSilga() {
		return silga;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setSilga(String silga) {
		this.silga = silga;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
