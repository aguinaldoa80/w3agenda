package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro3000 {
	
	public final static String REG = "3000";
	
	private String identificacao;
	private Integer codigo_especie;
	private String inscricao_cliente;
	private String sigla_estado_cliente;
	private Integer codigo_acumulador;
	private String serie;
	private String numero_documento;
	private Integer seguimento;
	private Integer documento_final;
	private Date data_servico;
	private Date data_emissao;
	private Double valor_contabil;
	private String obsevacao;
	private Integer codigo_observacao;
	private String fato_gerador_crf;
	private String fato_gerador_irrf;
	private String fato_gerador_crfop;
	private String fato_gerador_irrfp;
	private String codigo_municipio;
	private Integer codigo_modelo_doc;
	private Integer codigo_fiscal_prestacao_servicos;
	private Integer sub_serie;
	private String inscricao_estadual_cliente;
	private String inscricao_municipal_cliente;
	
	private List<Registro3020> listaRegistro3020;
	
	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_especie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_cliente()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getSigla_estado_cliente()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_acumulador()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getSerie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getNumero_documento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getSeguimento()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getDocumento_final()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_servico()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDateDominio(getData_emissao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_contabil()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getObsevacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_observacao()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_crf()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_irrf()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_crfop()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getFato_gerador_irrfp()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_municipio()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_modelo_doc()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getCodigo_fiscal_prestacao_servicos()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printInteger(getSub_serie()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_estadual_cliente()) + SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getInscricao_municipal_cliente()) + SPEDUtil.SEPARADOR_CAMPO + SIntegraUtil.SEPARADOR_LINHA +
				
				SPEDUtil.printLista(getListaRegistro3020());
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Integer getCodigo_especie() {
		return codigo_especie;
	}

	public String getInscricao_cliente() {
		return inscricao_cliente;
	}

	public String getSigla_estado_cliente() {
		return sigla_estado_cliente;
	}

	public Integer getCodigo_acumulador() {
		return codigo_acumulador;
	}

	public String getSerie() {
		return serie;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public Integer getSeguimento() {
		return seguimento;
	}

	public Integer getDocumento_final() {
		return documento_final;
	}

	public Date getData_servico() {
		return data_servico;
	}

	public Date getData_emissao() {
		return data_emissao;
	}

	public Double getValor_contabil() {
		return valor_contabil;
	}

	public String getObsevacao() {
		return obsevacao;
	}

	public Integer getCodigo_observacao() {
		return codigo_observacao;
	}

	public String getFato_gerador_crf() {
		return fato_gerador_crf;
	}

	public String getFato_gerador_irrf() {
		return fato_gerador_irrf;
	}

	public String getFato_gerador_crfop() {
		return fato_gerador_crfop;
	}

	public String getFato_gerador_irrfp() {
		return fato_gerador_irrfp;
	}

	public String getCodigo_municipio() {
		return codigo_municipio;
	}

	public Integer getCodigo_modelo_doc() {
		return codigo_modelo_doc;
	}

	public Integer getCodigo_fiscal_prestacao_servicos() {
		return codigo_fiscal_prestacao_servicos;
	}
	
	public Integer getSub_serie() {
		return sub_serie;
	}

	public String getInscricao_estadual_cliente() {
		return inscricao_estadual_cliente;
	}

	public String getInscricao_municipal_cliente() {
		return inscricao_municipal_cliente;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_especie(Integer codigoEspecie) {
		codigo_especie = codigoEspecie;
	}

	public void setInscricao_cliente(String inscricaoCliente) {
		inscricao_cliente = inscricaoCliente;
	}

	public void setSigla_estado_cliente(String siglaEstadoCliente) {
		sigla_estado_cliente = siglaEstadoCliente;
	}

	public void setCodigo_acumulador(Integer codigoAcumulador) {
		codigo_acumulador = codigoAcumulador;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setNumero_documento(String numeroDocumento) {
		numero_documento = numeroDocumento;
	}

	public void setSeguimento(Integer seguimento) {
		this.seguimento = seguimento;
	}

	public void setDocumento_final(Integer documentoFinal) {
		documento_final = documentoFinal;
	}

	public void setData_servico(Date dataServico) {
		data_servico = dataServico;
	}

	public void setData_emissao(Date dataEmissao) {
		data_emissao = dataEmissao;
	}

	public void setValor_contabil(Double valorContabil) {
		valor_contabil = valorContabil;
	}

	public void setObsevacao(String obsevacao) {
		this.obsevacao = obsevacao;
	}

	public void setCodigo_observacao(Integer codigoObservacao) {
		codigo_observacao = codigoObservacao;
	}

	public void setFato_gerador_crf(String fatoGeradorCrf) {
		fato_gerador_crf = fatoGeradorCrf;
	}

	public void setFato_gerador_irrf(String fatoGeradorIrrf) {
		fato_gerador_irrf = fatoGeradorIrrf;
	}

	public void setFato_gerador_crfop(String fatoGeradorCrfop) {
		fato_gerador_crfop = fatoGeradorCrfop;
	}

	public void setFato_gerador_irrfp(String fatoGeradorIrrfp) {
		fato_gerador_irrfp = fatoGeradorIrrfp;
	}

	public void setCodigo_municipio(String codigoMunicipio) {
		codigo_municipio = codigoMunicipio;
	}

	public void setCodigo_modelo_doc(Integer codigoModeloDoc) {
		codigo_modelo_doc = codigoModeloDoc;
	}

	public void setCodigo_fiscal_prestacao_servicos(
			Integer codigoFiscalPrestacaoServicos) {
		codigo_fiscal_prestacao_servicos = codigoFiscalPrestacaoServicos;
	}
	
	public void setSub_serie(Integer subSerie) {
		sub_serie = subSerie;
	}

	public void setInscricao_estadual_cliente(String inscricaoEstadualCliente) {
		inscricao_estadual_cliente = inscricaoEstadualCliente;
	}

	public void setInscricao_municipal_cliente(String inscricaoMunicipalCliente) {
		inscricao_municipal_cliente = inscricaoMunicipalCliente;
	}

	public List<Registro3020> getListaRegistro3020() {
		return listaRegistro3020;
	}

	public void setListaRegistro3020(List<Registro3020> listaRegistro3020) {
		this.listaRegistro3020 = listaRegistro3020;
	}
}
