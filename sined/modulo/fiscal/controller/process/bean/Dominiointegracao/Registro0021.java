package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0021 {
	
	public final static String REG = "0021";
	
	private String identificacao;
	private Date dataalteracao;
	private Integer campoalterado;
	private String valoranterioralteracao;
	private String valorposterioralteracao;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getDataalteracao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCampoalterado()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getValoranterioralteracao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getValorposterioralteracao()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Date getDataalteracao() {
		return dataalteracao;
	}

	public Integer getCampoalterado() {
		return campoalterado;
	}

	public String getValoranterioralteracao() {
		return valoranterioralteracao;
	}

	public String getValorposterioralteracao() {
		return valorposterioralteracao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setDataalteracao(Date dataalteracao) {
		this.dataalteracao = dataalteracao;
	}

	public void setCampoalterado(Integer campoalterado) {
		this.campoalterado = campoalterado;
	}

	public void setValoranterioralteracao(String valoranterioralteracao) {
		this.valoranterioralteracao = valoranterioralteracao;
	}

	public void setValorposterioralteracao(String valorposterioralteracao) {
		this.valorposterioralteracao = valorposterioralteracao;
	}
}
