package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro3020 {
	
	public final static String REG = "3020";
	
	private String identificacao;
	private String codigo_imposto;
	private Double percentual_reducao_bc;
	private Double base_calculo;
	private Double aliquota;
	private Double valor_imposto;
	private Double valor_isentas;
	private Double valor_outras;
	private Double valor_contabil;
	
	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigo_imposto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getPercentual_reducao_bc()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getBase_calculo()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getAliquota()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_imposto()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_isentas()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_outras()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_contabil()) +  SPEDUtil.SEPARADOR_CAMPO + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getCodigo_imposto() {
		return codigo_imposto;
	}

	public Double getPercentual_reducao_bc() {
		return percentual_reducao_bc;
	}

	public Double getBase_calculo() {
		return base_calculo;
	}

	public Double getAliquota() {
		return aliquota;
	}

	public Double getValor_imposto() {
		return valor_imposto;
	}

	public Double getValor_isentas() {
		return valor_isentas;
	}

	public Double getValor_outras() {
		return valor_outras;
	}

	public Double getValor_contabil() {
		return valor_contabil;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_imposto(String codigoImposto) {
		codigo_imposto = codigoImposto;
	}

	public void setPercentual_reducao_bc(Double percentualReducaoBc) {
		percentual_reducao_bc = percentualReducaoBc;
	}

	public void setBase_calculo(Double baseCalculo) {
		base_calculo = baseCalculo;
	}

	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}

	public void setValor_imposto(Double valorImposto) {
		valor_imposto = valorImposto;
	}

	public void setValor_isentas(Double valorIsentas) {
		valor_isentas = valorIsentas;
	}

	public void setValor_outras(Double valorOutras) {
		valor_outras = valorOutras;
	}

	public void setValor_contabil(Double valorContabil) {
		valor_contabil = valorContabil;
	}
}
