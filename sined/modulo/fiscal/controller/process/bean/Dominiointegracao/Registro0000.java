package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0000 {
	
	public final static String REG = "0000";
	
	private String identificacao;		//Fixo 0000 - Identificador do registro.
	private String inscricaoempresa;	//CNPJ/CPF/CEI da empresa. Informar apenas n�meros.

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getInscricaoempresa())+ SPEDUtil.SEPARADOR_CAMPO + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}
	public String getInscricaoempresa() {
		return inscricaoempresa;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	public void setInscricaoempresa(String inscricaoempresa) {
		this.inscricaoempresa = inscricaoempresa;
	}
}
