package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro2020 {
	
	public final static String REG = "1020";
	
	private String identificacao;
	private String codigo_imposto;
	private Double percentual_reducao_bc;
	private Double base_calculo;
	private Double aliquota;
	private Double valor_imposto;
	private Double valor_isentas;
	private Double valor_outras;
	private Double valor_ipi;
	private Double valor_st;
	private Double valor_contabil;
	private String codigo_recolhimento_imposto;
	private Double valor_nao_tributado;
	private Double valor_parcela_reduzida;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigo_imposto()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getPercentual_reducao_bc()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getBase_calculo()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getAliquota()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_imposto()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_isentas()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_outras()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_ipi()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_st()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_contabil()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getCodigo_recolhimento_imposto()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_nao_tributado()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getValor_parcela_reduzida()) +  SPEDUtil.SEPARADOR_CAMPO + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getCodigo_imposto() {
		return codigo_imposto;
	}

	public Double getPercentual_reducao_bc() {
		return percentual_reducao_bc;
	}

	public Double getBase_calculo() {
		return base_calculo;
	}

	public Double getAliquota() {
		return aliquota;
	}

	public Double getValor_imposto() {
		return valor_imposto;
	}

	public Double getValor_isentas() {
		return valor_isentas;
	}

	public Double getValor_outras() {
		return valor_outras;
	}

	public Double getValor_ipi() {
		return valor_ipi;
	}

	public Double getValor_st() {
		return valor_st;
	}

	public Double getValor_contabil() {
		return valor_contabil;
	}

	public String getCodigo_recolhimento_imposto() {
		return codigo_recolhimento_imposto;
	}

	public Double getValor_nao_tributado() {
		return valor_nao_tributado;
	}

	public Double getValor_parcela_reduzida() {
		return valor_parcela_reduzida;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_imposto(String codigoImposto) {
		codigo_imposto = codigoImposto;
	}

	public void setPercentual_reducao_bc(Double percentualReducaoBc) {
		percentual_reducao_bc = percentualReducaoBc;
	}

	public void setBase_calculo(Double baseCalculo) {
		base_calculo = baseCalculo;
	}

	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}

	public void setValor_imposto(Double valorImposto) {
		valor_imposto = valorImposto;
	}

	public void setValor_isentas(Double valorIsentas) {
		valor_isentas = valorIsentas;
	}

	public void setValor_outras(Double valorOutras) {
		valor_outras = valorOutras;
	}

	public void setValor_ipi(Double valorIpi) {
		valor_ipi = valorIpi;
	}

	public void setValor_st(Double valorSt) {
		valor_st = valorSt;
	}

	public void setValor_contabil(Double valorContabil) {
		valor_contabil = valorContabil;
	}

	public void setCodigo_recolhimento_imposto(String codigoRecolhimentoImposto) {
		codigo_recolhimento_imposto = codigoRecolhimentoImposto;
	}

	public void setValor_nao_tributado(Double valorNaoTributado) {
		valor_nao_tributado = valorNaoTributado;
	}

	public void setValor_parcela_reduzida(Double valorParcelaReduzida) {
		valor_parcela_reduzida = valorParcelaReduzida;
	}
}
