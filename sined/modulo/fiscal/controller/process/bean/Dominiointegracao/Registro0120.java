package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0120 {
	
	public final static String REG = "0120";
	
	private String identificacao;
	private String sigla_unidade_comercializada;
	private Double fator_conversao;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getSigla_unidade_comercializada()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getFator_conversao()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getSigla_unidade_comercializada() {
		return sigla_unidade_comercializada;
	}

	public Double getFator_conversao() {
		return fator_conversao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setSigla_unidade_comercializada(String siglaUnidadeComercializada) {
		sigla_unidade_comercializada = siglaUnidadeComercializada;
	}

	public void setFator_conversao(Double fatorConversao) {
		fator_conversao = fatorConversao;
	}
}
