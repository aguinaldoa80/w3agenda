package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0010 {
	
	public final static String REG = "0010";
	
	private String identificacao;
	private String inscricao;
	private String razaosocial;
	private String apelido;
	private String endereco;
	private String numeroendereco;
	private String complemento;
	private String bairro;
	private String codigomunicipio;
	private String uf;
	private Integer codigopais;
	private String cep;
	private String inscricaoestadual;
	private String inscricaomunicipal;
	private String inscricaosuframa;
	private Date ddd;
	private String telefone;
	private String fax;
	private Date datacadastro;
	private Integer contacontabil;
	private Integer contacontabilfornecedor;
	private String agropecuario;
	private String naturezajuridica;
	private String regimeapuracao;
	private String contribuinteicms;
	private Double aliquotaicms;
	private String categoriaestabelecimento;
	private String interdependenciacomempresa;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getInscricao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getRazaosocial()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getApelido()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getEndereco()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getNumeroendereco()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getComplemento()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getBairro()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigomunicipio()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getUf()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigopais()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCep()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getInscricaoestadual()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getInscricaomunicipal()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getInscricaosuframa()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getDdd()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getTelefone()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getFax()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getDatacadastro()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getContacontabil()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getContacontabilfornecedor()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getAgropecuario()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getNaturezajuridica()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getRegimeapuracao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getContribuinteicms()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getAliquotaicms()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCategoriaestabelecimento()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getInterdependenciacomempresa()) + SPEDUtil.SEPARADOR_CAMPO + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getInscricao() {
		return inscricao;
	}

	public String getRazaosocial() {
		return razaosocial;
	}

	public String getApelido() {
		return apelido;
	}

	public String getEndereco() {
		return endereco;
	}

	public String getNumeroendereco() {
		return numeroendereco;
	}

	public String getComplemento() {
		return complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCodigomunicipio() {
		return codigomunicipio;
	}

	public String getUf() {
		return uf;
	}

	public Integer getCodigopais() {
		return codigopais;
	}

	public String getCep() {
		return cep;
	}

	public String getInscricaoestadual() {
		return inscricaoestadual;
	}

	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}

	public String getInscricaosuframa() {
		return inscricaosuframa;
	}

	public Date getDdd() {
		return ddd;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getFax() {
		return fax;
	}

	public Date getDatacadastro() {
		return datacadastro;
	}

	public Integer getContacontabil() {
		return contacontabil;
	}

	public Integer getContacontabilfornecedor() {
		return contacontabilfornecedor;
	}

	public String getAgropecuario() {
		return agropecuario;
	}

	public String getNaturezajuridica() {
		return naturezajuridica;
	}

	public String getRegimeapuracao() {
		return regimeapuracao;
	}

	public String getContribuinteicms() {
		return contribuinteicms;
	}

	public Double getAliquotaicms() {
		return aliquotaicms;
	}

	public String getCategoriaestabelecimento() {
		return categoriaestabelecimento;
	}

	public String getInterdependenciacomempresa() {
		return interdependenciacomempresa;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setInscricao(String inscricao) {
		this.inscricao = inscricao;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setNumeroendereco(String numeroendereco) {
		this.numeroendereco = numeroendereco;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCodigomunicipio(String codigomunicipio) {
		this.codigomunicipio = codigomunicipio;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public void setCodigopais(Integer codigopais) {
		this.codigopais = codigopais;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}

	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}

	public void setInscricaosuframa(String inscricaosuframa) {
		this.inscricaosuframa = inscricaosuframa;
	}

	public void setDdd(Date ddd) {
		this.ddd = ddd;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setDatacadastro(Date datacadastro) {
		this.datacadastro = datacadastro;
	}

	public void setContacontabil(Integer contacontabil) {
		this.contacontabil = contacontabil;
	}

	public void setContacontabilfornecedor(Integer contacontabilfornecedor) {
		this.contacontabilfornecedor = contacontabilfornecedor;
	}

	public void setAgropecuario(String agropecuario) {
		this.agropecuario = agropecuario;
	}

	public void setNaturezajuridica(String naturezajuridica) {
		this.naturezajuridica = naturezajuridica;
	}

	public void setRegimeapuracao(String regimeapuracao) {
		this.regimeapuracao = regimeapuracao;
	}

	public void setContribuinteicms(String contribuinteicms) {
		this.contribuinteicms = contribuinteicms;
	}

	public void setAliquotaicms(Double aliquotaicms) {
		this.aliquotaicms = aliquotaicms;
	}

	public void setCategoriaestabelecimento(String categoriaestabelecimento) {
		this.categoriaestabelecimento = categoriaestabelecimento;
	}

	public void setInterdependenciacomempresa(String interdependenciacomempresa) {
		this.interdependenciacomempresa = interdependenciacomempresa;
	}
}
