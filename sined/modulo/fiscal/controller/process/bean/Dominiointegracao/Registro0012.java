package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0012 {
	
	public final static String REG = "0012";
	
	private String identificacao;
	private String nomebeneficiario;
	private String cpfbeneficiario;
	private Date datanascimentobeneficiario;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getNomebeneficiario()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCpfbeneficiario()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getDatanascimentobeneficiario()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getNomebeneficiario() {
		return nomebeneficiario;
	}

	public String getCpfbeneficiario() {
		return cpfbeneficiario;
	}

	public Date getDatanascimentobeneficiario() {
		return datanascimentobeneficiario;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setNomebeneficiario(String nomebeneficiario) {
		this.nomebeneficiario = nomebeneficiario;
	}

	public void setCpfbeneficiario(String cpfbeneficiario) {
		this.cpfbeneficiario = cpfbeneficiario;
	}

	public void setDatanascimentobeneficiario(Date datanascimentobeneficiario) {
		this.datanascimentobeneficiario = datanascimentobeneficiario;
	}
}
