package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0110 {
	
	public final static String REG = "0110";
	
	private String identificacao;
	private String descricao;
	private Integer cst_entradas;
	private Integer vinculo_credito;
	private Integer base_credito;
	private String aproveitar_credito_proporcional_rec_naocumulativa;
	private String credito_aliquota_diferenciada_entradas;
	private Double aliquota_pis_entradas;
	private Double aliquota_cofins_entradas;
	private String credito_unidademedida_entradas;
	private String unidade_tribuada_dif_inventariada_entradas;
	private String unidade_tributavel_entradas;
	private Double fator_conversao_entradas;
	private Double valor_pis_entradas;
	private Double valor_cofins_entradas;
	private Integer cst_saidas;
	private String tipo_contribuicao;
	private Integer natureza_receita;
	private String codigo_recolhimento_pis;
	private String codigo_recolhimento_cofins;
	private String debito_aliquota_diferenciada_saidas;
	private Double aliquota_pis_saidas;
	private Double aliquota_cofins_saidas;
	private String debito_unidademedida_saida;
	private String unidade_tributada_dif_inventariada_saidas;
	private String unidade_tributada_saidas;
	private Double fator_conversao_saidas;
	private Double valor_pis_saidas;
	private Double valor_cofins_saidas;
	private Integer tabela_sped;
	private Integer marca_gruposped;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDescricao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCst_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getVinculo_credito()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getBase_credito()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getAproveitar_credito_proporcional_rec_naocumulativa()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCredito_aliquota_diferenciada_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getAliquota_pis_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getAliquota_cofins_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCredito_unidademedida_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getUnidade_tribuada_dif_inventariada_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getUnidade_tributavel_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getFator_conversao_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getValor_pis_entradas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCst_saidas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getTipo_contribuicao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getNatureza_receita()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigo_recolhimento_pis()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getCodigo_recolhimento_cofins()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDebito_aliquota_diferenciada_saidas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getAliquota_pis_saidas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getAliquota_cofins_saidas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDebito_unidademedida_saida()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getUnidade_tributada_dif_inventariada_saidas()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printString(getUnidade_tributada_saidas()) +  SPEDUtil.SEPARADOR_CAMPO +
				SPEDUtil.printDouble(getFator_conversao_saidas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getValor_pis_saidas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getValor_cofins_saidas()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getTabela_sped()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getMarca_gruposped()) +  SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public Integer getCst_entradas() {
		return cst_entradas;
	}

	public Integer getVinculo_credito() {
		return vinculo_credito;
	}

	public Integer getBase_credito() {
		return base_credito;
	}

	public String getAproveitar_credito_proporcional_rec_naocumulativa() {
		return aproveitar_credito_proporcional_rec_naocumulativa;
	}

	public String getCredito_aliquota_diferenciada_entradas() {
		return credito_aliquota_diferenciada_entradas;
	}

	public Double getAliquota_pis_entradas() {
		return aliquota_pis_entradas;
	}

	public Double getAliquota_cofins_entradas() {
		return aliquota_cofins_entradas;
	}

	public String getCredito_unidademedida_entradas() {
		return credito_unidademedida_entradas;
	}

	public String getUnidade_tribuada_dif_inventariada_entradas() {
		return unidade_tribuada_dif_inventariada_entradas;
	}

	public String getUnidade_tributavel_entradas() {
		return unidade_tributavel_entradas;
	}

	public Double getFator_conversao_entradas() {
		return fator_conversao_entradas;
	}

	public Double getValor_pis_entradas() {
		return valor_pis_entradas;
	}

	public Double getValor_cofins_entradas() {
		return valor_cofins_entradas;
	}

	public Integer getCst_saidas() {
		return cst_saidas;
	}

	public String getTipo_contribuicao() {
		return tipo_contribuicao;
	}

	public Integer getNatureza_receita() {
		return natureza_receita;
	}

	public String getCodigo_recolhimento_pis() {
		return codigo_recolhimento_pis;
	}

	public String getCodigo_recolhimento_cofins() {
		return codigo_recolhimento_cofins;
	}

	public String getDebito_aliquota_diferenciada_saidas() {
		return debito_aliquota_diferenciada_saidas;
	}

	public Double getAliquota_pis_saidas() {
		return aliquota_pis_saidas;
	}

	public Double getAliquota_cofins_saidas() {
		return aliquota_cofins_saidas;
	}

	public String getDebito_unidademedida_saida() {
		return debito_unidademedida_saida;
	}

	public String getUnidade_tributada_dif_inventariada_saidas() {
		return unidade_tributada_dif_inventariada_saidas;
	}

	public String getUnidade_tributada_saidas() {
		return unidade_tributada_saidas;
	}

	public Double getFator_conversao_saidas() {
		return fator_conversao_saidas;
	}

	public Double getValor_pis_saidas() {
		return valor_pis_saidas;
	}

	public Double getValor_cofins_saidas() {
		return valor_cofins_saidas;
	}

	public Integer getTabela_sped() {
		return tabela_sped;
	}

	public Integer getMarca_gruposped() {
		return marca_gruposped;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setCst_entradas(Integer cstEntradas) {
		cst_entradas = cstEntradas;
	}

	public void setVinculo_credito(Integer vinculoCredito) {
		vinculo_credito = vinculoCredito;
	}

	public void setBase_credito(Integer baseCredito) {
		base_credito = baseCredito;
	}

	public void setAproveitar_credito_proporcional_rec_naocumulativa(
			String aproveitarCreditoProporcionalRecNaocumulativa) {
		aproveitar_credito_proporcional_rec_naocumulativa = aproveitarCreditoProporcionalRecNaocumulativa;
	}

	public void setCredito_aliquota_diferenciada_entradas(
			String creditoAliquotaDiferenciadaEntradas) {
		credito_aliquota_diferenciada_entradas = creditoAliquotaDiferenciadaEntradas;
	}

	public void setAliquota_pis_entradas(Double aliquotaPisEntradas) {
		aliquota_pis_entradas = aliquotaPisEntradas;
	}

	public void setAliquota_cofins_entradas(Double aliquotaCofinsEntradas) {
		aliquota_cofins_entradas = aliquotaCofinsEntradas;
	}

	public void setCredito_unidademedida_entradas(
			String creditoUnidademedidaEntradas) {
		credito_unidademedida_entradas = creditoUnidademedidaEntradas;
	}

	public void setUnidade_tribuada_dif_inventariada_entradas(
			String unidadeTribuadaDifInventariadaEntradas) {
		unidade_tribuada_dif_inventariada_entradas = unidadeTribuadaDifInventariadaEntradas;
	}

	public void setUnidade_tributavel_entradas(String unidadeTributavelEntradas) {
		unidade_tributavel_entradas = unidadeTributavelEntradas;
	}

	public void setFator_conversao_entradas(Double fatorConversaoEntradas) {
		fator_conversao_entradas = fatorConversaoEntradas;
	}

	public void setValor_pis_entradas(Double valorPisEntradas) {
		valor_pis_entradas = valorPisEntradas;
	}

	public void setValor_cofins_entradas(Double valorCofinsEntradas) {
		valor_cofins_entradas = valorCofinsEntradas;
	}

	public void setCst_saidas(Integer cstSaidas) {
		cst_saidas = cstSaidas;
	}

	public void setTipo_contribuicao(String tipoContribuicao) {
		tipo_contribuicao = tipoContribuicao;
	}

	public void setNatureza_receita(Integer naturezaReceita) {
		natureza_receita = naturezaReceita;
	}

	public void setCodigo_recolhimento_pis(String codigoRecolhimentoPis) {
		codigo_recolhimento_pis = codigoRecolhimentoPis;
	}

	public void setCodigo_recolhimento_cofins(String codigoRecolhimentoCofins) {
		codigo_recolhimento_cofins = codigoRecolhimentoCofins;
	}

	public void setDebito_aliquota_diferenciada_saidas(
			String debitoAliquotaDiferenciadaSaidas) {
		debito_aliquota_diferenciada_saidas = debitoAliquotaDiferenciadaSaidas;
	}

	public void setAliquota_pis_saidas(Double aliquotaPisSaidas) {
		aliquota_pis_saidas = aliquotaPisSaidas;
	}

	public void setAliquota_cofins_saidas(Double aliquotaCofinsSaidas) {
		aliquota_cofins_saidas = aliquotaCofinsSaidas;
	}

	public void setDebito_unidademedida_saida(String debitoUnidademedidaSaida) {
		debito_unidademedida_saida = debitoUnidademedidaSaida;
	}

	public void setUnidade_tributada_dif_inventariada_saidas(
			String unidadeTributadaDifInventariadaSaidas) {
		unidade_tributada_dif_inventariada_saidas = unidadeTributadaDifInventariadaSaidas;
	}

	public void setUnidade_tributada_saidas(String unidadeTributadaSaidas) {
		unidade_tributada_saidas = unidadeTributadaSaidas;
	}

	public void setFator_conversao_saidas(Double fatorConversaoSaidas) {
		fator_conversao_saidas = fatorConversaoSaidas;
	}

	public void setValor_pis_saidas(Double valorPisSaidas) {
		valor_pis_saidas = valorPisSaidas;
	}

	public void setValor_cofins_saidas(Double valorCofinsSaidas) {
		valor_cofins_saidas = valorCofinsSaidas;
	}

	public void setTabela_sped(Integer tabelaSped) {
		tabela_sped = tabelaSped;
	}

	public void setMarca_gruposped(Integer marcaGruposped) {
		marca_gruposped = marcaGruposped;
	}
}
