package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0200 {
	
	public final static String REG = "0200";
	
	private String identificacao;
	private Integer codigo_reduzido;
	private String classificacao_contabil;
	private String tipo;
	private String descricao;
	private Date data_cadastro;
	private String situacao;
	private Date data_inativacao;
	private Integer codigo_relacionamento_dlpa;
	private Integer codigo_relacionamento_doar;
	private Integer codigo_relacionamento_dre;
	private Integer codigo_relacionamento_dmpl;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo_reduzido()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getClassificacao_contabil()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getTipo()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDescricao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getData_cadastro()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getSituacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getData_inativacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo_relacionamento_dlpa()) +  SPEDUtil.SEPARADOR_CAMPO +  
				SPEDUtil.printInteger(getCodigo_relacionamento_doar()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo_relacionamento_dre()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo_relacionamento_dmpl()) +  SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Integer getCodigo_reduzido() {
		return codigo_reduzido;
	}

	public String getClassificacao_contabil() {
		return classificacao_contabil;
	}

	public String getTipo() {
		return tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public String getSituacao() {
		return situacao;
	}

	public Date getData_inativacao() {
		return data_inativacao;
	}

	public Integer getCodigo_relacionamento_dlpa() {
		return codigo_relacionamento_dlpa;
	}

	public Integer getCodigo_relacionamento_doar() {
		return codigo_relacionamento_doar;
	}

	public Integer getCodigo_relacionamento_dre() {
		return codigo_relacionamento_dre;
	}

	public Integer getCodigo_relacionamento_dmpl() {
		return codigo_relacionamento_dmpl;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_reduzido(Integer codigoReduzido) {
		codigo_reduzido = codigoReduzido;
	}

	public void setClassificacao_contabil(String classificacaoContabil) {
		classificacao_contabil = classificacaoContabil;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setData_cadastro(Date dataCadastro) {
		data_cadastro = dataCadastro;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public void setData_inativacao(Date dataInativacao) {
		data_inativacao = dataInativacao;
	}

	public void setCodigo_relacionamento_dlpa(Integer codigoRelacionamentoDlpa) {
		codigo_relacionamento_dlpa = codigoRelacionamentoDlpa;
	}

	public void setCodigo_relacionamento_doar(Integer codigoRelacionamentoDoar) {
		codigo_relacionamento_doar = codigoRelacionamentoDoar;
	}

	public void setCodigo_relacionamento_dre(Integer codigoRelacionamentoDre) {
		codigo_relacionamento_dre = codigoRelacionamentoDre;
	}

	public void setCodigo_relacionamento_dmpl(Integer codigoRelacionamentoDmpl) {
		codigo_relacionamento_dmpl = codigoRelacionamentoDmpl;
	}
}
