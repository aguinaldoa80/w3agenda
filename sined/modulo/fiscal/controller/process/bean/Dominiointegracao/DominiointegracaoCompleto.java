package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.util.List;

import br.com.linkcom.utils.SPEDUtil;


public class DominiointegracaoCompleto {
	
	private List<Registro0000> listaRegistro0000;
	private List<Registro0010> listaRegistro0010;
	private List<Registro0020> listaRegistro0020;
	private List<Registro1000> listaRegistro1000;
	private List<Registro2000> listaRegistro2000;
	private List<Registro3000> listaRegistro3000;

	@Override
	public String toString() {
		return  SPEDUtil.printLista(getListaRegistro0000()) +
				SPEDUtil.printLista(getListaRegistro0010()) +
				SPEDUtil.printLista(getListaRegistro0020()) +
				SPEDUtil.printLista(getListaRegistro1000()) +
				SPEDUtil.printLista(getListaRegistro2000()) +
				SPEDUtil.printLista(getListaRegistro3000());
	}

	public List<Registro0000> getListaRegistro0000() {
		return listaRegistro0000;
	}

	public List<Registro0010> getListaRegistro0010() {
		return listaRegistro0010;
	}

	public List<Registro0020> getListaRegistro0020() {
		return listaRegistro0020;
	}

	public List<Registro1000> getListaRegistro1000() {
		return listaRegistro1000;
	}

	public List<Registro2000> getListaRegistro2000() {
		return listaRegistro2000;
	}

	public List<Registro3000> getListaRegistro3000() {
		return listaRegistro3000;
	}

	public void setListaRegistro0000(List<Registro0000> listaRegistro0000) {
		this.listaRegistro0000 = listaRegistro0000;
	}

	public void setListaRegistro0010(List<Registro0010> listaRegistro0010) {
		this.listaRegistro0010 = listaRegistro0010;
	}

	public void setListaRegistro0020(List<Registro0020> listaRegistro0020) {
		this.listaRegistro0020 = listaRegistro0020;
	}

	public void setListaRegistro1000(List<Registro1000> listaRegistro1000) {
		this.listaRegistro1000 = listaRegistro1000;
	}

	public void setListaRegistro2000(List<Registro2000> listaRegistro2000) {
		this.listaRegistro2000 = listaRegistro2000;
	}

	public void setListaRegistro3000(List<Registro3000> listaRegistro3000) {
		this.listaRegistro3000 = listaRegistro3000;
	}
}
