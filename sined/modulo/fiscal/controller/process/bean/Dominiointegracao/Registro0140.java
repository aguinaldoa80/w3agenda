package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0140 {
	
	public final static String REG = "0140";
	
	private String identificacao;
	private Date data_saldo;
	private Double quantidade_final;
	private Double valor_unitario;
	private Double valor_final;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDate(getData_saldo()) +  SPEDUtil.SEPARADOR_CAMPO +  
				SPEDUtil.printDouble(getQuantidade_final()) +  SPEDUtil.SEPARADOR_CAMPO +  
				SPEDUtil.printDouble(getValor_unitario()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printDouble(getValor_final()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Date getData_saldo() {
		return data_saldo;
	}

	public Double getQuantidade_final() {
		return quantidade_final;
	}

	public Double getValor_unitario() {
		return valor_unitario;
	}

	public Double getValor_final() {
		return valor_final;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setData_saldo(Date dataSaldo) {
		data_saldo = dataSaldo;
	}

	public void setQuantidade_final(Double quantidadeFinal) {
		quantidade_final = quantidadeFinal;
	}

	public void setValor_unitario(Double valorUnitario) {
		valor_unitario = valorUnitario;
	}

	public void setValor_final(Double valorFinal) {
		valor_final = valorFinal;
	}
}
