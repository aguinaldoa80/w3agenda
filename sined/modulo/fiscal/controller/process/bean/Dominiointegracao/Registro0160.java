package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import br.com.linkcom.utils.SIntegraUtil;
import br.com.linkcom.utils.SPEDUtil;


public class Registro0160 {
	
	public final static String REG = "0160";
	
	private String identificacao;
	private Integer codigo_grupo;
	private String descricao_grupo;

	@Override
	public String toString() {
		return  SPEDUtil.printString(getIdentificacao()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printInteger(getCodigo_grupo()) +  SPEDUtil.SEPARADOR_CAMPO + 
				SPEDUtil.printString(getDescricao_grupo()) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public Integer getCodigo_grupo() {
		return codigo_grupo;
	}

	public String getDescricao_grupo() {
		return descricao_grupo;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_grupo(Integer codigoGrupo) {
		codigo_grupo = codigoGrupo;
	}

	public void setDescricao_grupo(String descricaoGrupo) {
		descricao_grupo = descricaoGrupo;
	}
}
