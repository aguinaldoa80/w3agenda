package br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Dominiointegracao;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;


public class Registro3030 {
	
	public final static String REG = "3030";
	
	private String identificacao;
	private String codigo_produto;
	private Date data_movimentacao;
	private Double quantidade;
	private Double valor_produto;
	private Double valor_desconto;
	private Double valor_unitario;
	private Integer cst_issqn;
	private Double base_calculo_issqn;
	private Double aliquota_issqn;
	private Double valor_issqn;
	private Double isentas_issqn;
	private Double nao_incidencia_issqn;
	private Double iss_retido;
	private Double quantidade_cancelada;
	private Double valor_cancelamento;
	private Integer cst_pis;
	private Double base_calculo_pis;
	private Double aliquota_pis;
	private Double valor_pis;
	private Integer cst_cofins;
	private Double base_calculo_cofins;
	private Double aliquota_cofins;
	private Double valor_cofins;
	private String descricao_complementar;
	private Integer natureza_receita_pis;
	private Integer natureza_receita_cofins;
	private Double exclusao_cooperativa;
	
	@Override
	public String toString() {
		return  SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public String getCodigo_produto() {
		return codigo_produto;
	}

	public Date getData_movimentacao() {
		return data_movimentacao;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public Double getValor_produto() {
		return valor_produto;
	}

	public Double getValor_desconto() {
		return valor_desconto;
	}

	public Double getValor_unitario() {
		return valor_unitario;
	}

	public Integer getCst_issqn() {
		return cst_issqn;
	}

	public Double getBase_calculo_issqn() {
		return base_calculo_issqn;
	}

	public Double getAliquota_issqn() {
		return aliquota_issqn;
	}

	public Double getValor_issqn() {
		return valor_issqn;
	}

	public Double getIsentas_issqn() {
		return isentas_issqn;
	}

	public Double getNao_incidencia_issqn() {
		return nao_incidencia_issqn;
	}

	public Double getIss_retido() {
		return iss_retido;
	}

	public Double getQuantidade_cancelada() {
		return quantidade_cancelada;
	}

	public Double getValor_cancelamento() {
		return valor_cancelamento;
	}

	public Integer getCst_pis() {
		return cst_pis;
	}

	public Double getBase_calculo_pis() {
		return base_calculo_pis;
	}

	public Double getAliquota_pis() {
		return aliquota_pis;
	}

	public Double getValor_pis() {
		return valor_pis;
	}

	public Integer getCst_cofins() {
		return cst_cofins;
	}

	public Double getBase_calculo_cofins() {
		return base_calculo_cofins;
	}

	public Double getAliquota_cofins() {
		return aliquota_cofins;
	}

	public Double getValor_cofins() {
		return valor_cofins;
	}

	public String getDescricao_complementar() {
		return descricao_complementar;
	}

	public Integer getNatureza_receita_pis() {
		return natureza_receita_pis;
	}

	public Integer getNatureza_receita_cofins() {
		return natureza_receita_cofins;
	}

	public Double getExclusao_cooperativa() {
		return exclusao_cooperativa;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setCodigo_produto(String codigoProduto) {
		codigo_produto = codigoProduto;
	}

	public void setData_movimentacao(Date dataMovimentacao) {
		data_movimentacao = dataMovimentacao;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setValor_produto(Double valorProduto) {
		valor_produto = valorProduto;
	}

	public void setValor_desconto(Double valorDesconto) {
		valor_desconto = valorDesconto;
	}

	public void setValor_unitario(Double valorUnitario) {
		valor_unitario = valorUnitario;
	}

	public void setCst_issqn(Integer cstIssqn) {
		cst_issqn = cstIssqn;
	}

	public void setBase_calculo_issqn(Double baseCalculoIssqn) {
		base_calculo_issqn = baseCalculoIssqn;
	}

	public void setAliquota_issqn(Double aliquotaIssqn) {
		aliquota_issqn = aliquotaIssqn;
	}

	public void setValor_issqn(Double valorIssqn) {
		valor_issqn = valorIssqn;
	}

	public void setIsentas_issqn(Double isentasIssqn) {
		isentas_issqn = isentasIssqn;
	}

	public void setNao_incidencia_issqn(Double naoIncidenciaIssqn) {
		nao_incidencia_issqn = naoIncidenciaIssqn;
	}

	public void setIss_retido(Double issRetido) {
		iss_retido = issRetido;
	}

	public void setQuantidade_cancelada(Double quantidadeCancelada) {
		quantidade_cancelada = quantidadeCancelada;
	}

	public void setValor_cancelamento(Double valorCancelamento) {
		valor_cancelamento = valorCancelamento;
	}

	public void setCst_pis(Integer cstPis) {
		cst_pis = cstPis;
	}

	public void setBase_calculo_pis(Double baseCalculoPis) {
		base_calculo_pis = baseCalculoPis;
	}

	public void setAliquota_pis(Double aliquotaPis) {
		aliquota_pis = aliquotaPis;
	}

	public void setValor_pis(Double valorPis) {
		valor_pis = valorPis;
	}

	public void setCst_cofins(Integer cstCofins) {
		cst_cofins = cstCofins;
	}

	public void setBase_calculo_cofins(Double baseCalculoCofins) {
		base_calculo_cofins = baseCalculoCofins;
	}

	public void setAliquota_cofins(Double aliquotaCofins) {
		aliquota_cofins = aliquotaCofins;
	}

	public void setValor_cofins(Double valorCofins) {
		valor_cofins = valorCofins;
	}

	public void setDescricao_complementar(String descricaoComplementar) {
		descricao_complementar = descricaoComplementar;
	}

	public void setNatureza_receita_pis(Integer naturezaReceitaPis) {
		natureza_receita_pis = naturezaReceitaPis;
	}

	public void setNatureza_receita_cofins(Integer naturezaReceitaCofins) {
		natureza_receita_cofins = naturezaReceitaCofins;
	}

	public void setExclusao_cooperativa(Double exclusaoCooperativa) {
		exclusao_cooperativa = exclusaoCooperativa;
	}
}
