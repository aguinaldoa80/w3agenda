package br.com.linkcom.sined.modulo.fiscal.controller.process.bean;

import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;

public class EventoBean {

	private Uf uf;
	private Municipio municipio;
	private Boolean exterior;
	private Empresa empresa;
	private Configuracaonfe configuracaonfe;
	private ArquivoMdfe arquivoMdfe;
	private Mdfe mdfe;
	private String justificativa;
	private String nome;
	private Cpf cpf;

	public Uf getUf() {
		return uf;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}

	public ArquivoMdfe getArquivoMdfe() {
		return arquivoMdfe;
	}

	public Mdfe getMdfe() {
		return mdfe;
	}
	
	@MinLength(15)
	@MaxLength(255)
	public String getJustificativa() {
		return justificativa;
	}
	
	public Boolean getExterior() {
		return exterior;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Cpf getCpf() {
		return cpf;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}

	public void setArquivoMdfe(ArquivoMdfe arquivoMdfe) {
		this.arquivoMdfe = arquivoMdfe;
	}

	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
	
	public void setExterior(Boolean exterior) {
		this.exterior = exterior;
	}
}