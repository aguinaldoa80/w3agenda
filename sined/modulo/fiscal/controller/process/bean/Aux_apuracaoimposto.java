package br.com.linkcom.sined.modulo.fiscal.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Pessoa;

public class Aux_apuracaoimposto {

	protected Integer cdnota;
	protected Integer cdentregadocumento;
	protected Boolean entrada;
	protected Date dtemissao;
	protected String numero;
	protected String numeronfse;
	protected String codverificacao;
	protected Cpf cpf;
	protected Cnpj cnpj;
	protected Pessoa pessoa;
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected Money valorbruto = new Money();
	protected Money basecalculo = new Money();
	protected Double aliquota = 0.0;
	protected Money valor = new Money();
	protected Money valorliquido = new Money();
	protected Money valorretido = new Money();
	
	public Integer getCdnota() {
		return cdnota;
	}
	public Integer getCdentregadocumento() {
		return cdentregadocumento;
	}
	@DisplayName("Data de Emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	@DisplayName("N�mero Nfs-e")
	public String getNumeronfse() {
		return numeronfse;
	}
	@DisplayName("C�d. Verifica��o")
	public String getCodverificacao() {
		return codverificacao;
	}
	public Cpf getCpf() {
		return cpf;
	}
	public Cnpj getCnpj() {
		return cnpj;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Valor Bruto (R$)")
	public Money getValorbruto() {
		return valorbruto;
	}
	@DisplayName("Cliente/Fornecedor")
	public Pessoa getPessoa() {
		return pessoa;
	}
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Base de C�lculo (R$)")
	public Money getBasecalculo() {
		return basecalculo;
	}
	@DisplayName("Al�quota (%)")
	public Double getAliquota() {
		return aliquota;
	}
	@DisplayName("Valor (R$)")
	public Money getValor() {
		return valor;
	}
	@DisplayName("Valor L�quido (R$)")
	public Money getValorliquido() {
		return valorliquido;
	}
	public Boolean getEntrada() {
		return entrada;
	}
	public Money getValorretido() {
		return valorretido;
	}
	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}
	public void setCdnota(Integer cdnota) {
		this.cdnota = cdnota;
	}
	public void setCdentregadocumento(Integer cdentregadocumento) {
		this.cdentregadocumento = cdentregadocumento;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setNumeronfse(String numeronfse) {
		this.numeronfse = numeronfse;
	}
	public void setCodverificacao(String codverificacao) {
		this.codverificacao = codverificacao;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setValorbruto(Money valorbruto) {
		this.valorbruto = valorbruto;
	}
	public void setValorliquido(Money valorliquido) {
		this.valorliquido = valorliquido;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setBasecalculo(Money basecalculo) {
		this.basecalculo = basecalculo;
	}
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setValorretido(Money valorretido) {
		this.valorretido = valorretido;
	}
}
