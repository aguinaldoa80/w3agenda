package br.com.linkcom.sined.modulo.fiscal.controller.process.bean;

import br.com.linkcom.neo.types.Money;

public class Aux_apuracaopiscofins {

	protected String mes;
	protected String ano;
	
	protected Money receitafaturamento = new Money();	
	protected Money bcpis = new Money();
	protected Money pis = new Money();
	protected Money piscredito = new Money();
	protected Money pisretido = new Money();
	protected Money pisdevido = new Money();
	
	protected Money bccofins = new Money();
	protected Money cofins = new Money();
	protected Money cofinscredito = new Money();
	protected Money cofinsretido = new Money();
	protected Money cofinsdevido = new Money();
	
	protected Money aquisicaobensrevenda = new Money();
	protected Money aquisicaobensinsumos = new Money();
	protected Money aquisicaoservicosutilizado = new Money();
	protected Money energiaeletrica = new Money();
	protected Money bccreditos = new Money();
	
	protected Money saldopis = new Money();
	protected Money saldocofins = new Money();
	
	
	
	public Aux_apuracaopiscofins() {}
	
	public Aux_apuracaopiscofins(String mes, String ano) {
		this.mes = mes;
		this.ano = ano;
	}
	
	public String getMes() {
		return mes;
	}
	public String getAno() {
		return ano;
	}
	public Money getReceitafaturamento() {
		return receitafaturamento;
	}
	public Money getBcpis() {
		return bcpis;
	}
	public Money getPis() {
		return pis;
	}
	public Money getPiscredito() {
		return piscredito;
	}
	public Money getPisretido() {
		return pisretido;
	}
	public Money getPisdevido() {
		return pisdevido;
	}
	public Money getBccofins() {
		return bccofins;
	}
	public Money getCofins() {
		return cofins;
	}
	public Money getCofinscredito() {
		return cofinscredito;
	}
	public Money getCofinsretido() {
		return cofinsretido;
	}
	public Money getCofinsdevido() {
		return cofinsdevido;
	}
	public Money getAquisicaobensrevenda() {
		return aquisicaobensrevenda;
	}
	public Money getAquisicaobensinsumos() {
		return aquisicaobensinsumos;
	}
	public Money getAquisicaoservicosutilizado() {
		return aquisicaoservicosutilizado;
	}
	public Money getEnergiaeletrica() {
		return energiaeletrica;
	}
	public Money getBccreditos() {
		return bccreditos;
	}
	public Money getSaldopis() {
		return saldopis;
	}
	public Money getSaldocofins() {
		return saldocofins;
	}
	
	
	public void setMes(String mes) {
		this.mes = mes;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public void setReceitafaturamento(Money receitafaturamento) {
		this.receitafaturamento = receitafaturamento;
	}
	public void setBcpis(Money bcpis) {
		this.bcpis = bcpis;
	}
	public void setPis(Money pis) {
		this.pis = pis;
	}
	public void setPiscredito(Money piscredito) {
		this.piscredito = piscredito;
	}
	public void setPisretido(Money pisretido) {
		this.pisretido = pisretido;
	}
	public void setPisdevido(Money pisdevido) {
		this.pisdevido = pisdevido;
	}
	public void setBccofins(Money bccofins) {
		this.bccofins = bccofins;
	}
	public void setCofins(Money cofins) {
		this.cofins = cofins;
	}
	public void setCofinscredito(Money cofinscredito) {
		this.cofinscredito = cofinscredito;
	}
	public void setCofinsretido(Money cofinsretido) {
		this.cofinsretido = cofinsretido;
	}
	public void setCofinsdevido(Money cofinsdevido) {
		this.cofinsdevido = cofinsdevido;
	}
	public void setAquisicaobensrevenda(Money aquisicaobensrevenda) {
		this.aquisicaobensrevenda = aquisicaobensrevenda;
	}
	public void setAquisicaobensinsumos(Money aquisicaobensinsumos) {
		this.aquisicaobensinsumos = aquisicaobensinsumos;
	}
	public void setAquisicaoservicosutilizado(Money aquisicaoservicosutilizado) {
		this.aquisicaoservicosutilizado = aquisicaoservicosutilizado;
	}
	public void setEnergiaeletrica(Money energiaeletrica) {
		this.energiaeletrica = energiaeletrica;
	}
	public void setBccreditos(Money bccreditos) {
		this.bccreditos = bccreditos;
	}
	public void setSaldopis(Money saldopis) {
		this.saldopis = saldopis;
	}
	public void setSaldocofins(Money saldocofins) {
		this.saldocofins = saldocofins;
	}
}
