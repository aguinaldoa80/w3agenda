package br.com.linkcom.sined.modulo.fiscal.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Projeto;

public class GerarLancamentoContabilCentrocustoProjetoBean {
	
	private Centrocusto centrocusto;
	private Projeto projeto;
	private Money valor;
	private Double percentual = 100D;
	
	public GerarLancamentoContabilCentrocustoProjetoBean(){
	}
	
	public GerarLancamentoContabilCentrocustoProjetoBean(Centrocusto centrocusto, Projeto projeto){
		this.centrocusto = centrocusto;
		this.projeto = projeto;
	}
	
	public GerarLancamentoContabilCentrocustoProjetoBean(Centrocusto centrocusto, Projeto projeto, Money valor){
		this.centrocusto = centrocusto;
		this.projeto = projeto;
		this.valor = valor;
	}
	
	public GerarLancamentoContabilCentrocustoProjetoBean(Centrocusto centrocusto, Projeto projeto, Double percentual){
		this.centrocusto = centrocusto;
		this.projeto = projeto;
		this.percentual = percentual;
	}
	
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	
	public Money getValor() {
		return valor;
	}
	
	public Double getPercentual() {
		return percentual;
	}	
}