package br.com.linkcom.sined.modulo.fiscal.controller.process;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.enumeration.AlterdataModelointegracao;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.AlterdataintegracaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(path="/fiscal/process/Alterdataintegracao", authorizationModule=ProcessAuthorizationModule.class)
public class AlterdataintegracaoProcess extends ResourceSenderController<AlterdataintegracaoFiltro>{
	
	private MovimentacaocontabilService movimentacaocontabilService;
	
	public void setMovimentacaocontabilService(MovimentacaocontabilService movimentacaocontabilService) {
		this.movimentacaocontabilService = movimentacaocontabilService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,	AlterdataintegracaoFiltro filtro) throws Exception {
		return gerarArquivointegracao(request, filtro);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, AlterdataintegracaoFiltro filtro) throws Exception {
		List<AlterdataModelointegracao> lista = new ArrayList<AlterdataModelointegracao>();
		lista.add(AlterdataModelointegracao.MODELO1);
		lista.add(AlterdataModelointegracao.MODELO2);
		lista.add(AlterdataModelointegracao.MODELO3);
		request.setAttribute("listaAlteradataTipointegracao", lista);
		
		if(filtro.getEmpresa() == null && request.getSession().getAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase()) == null){
			request.getSession().setAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase(), Boolean.TRUE);
			Empresa empresa = (Empresa)request.getSession().getAttribute("empresaSelecionada");
			if(empresa != null){
				Method methodSetEmpresa = Util.beans.getSetterMethod(filtro.getClass(), "empresa");
				if(methodSetEmpresa != null && methodSetEmpresa.getParameterTypes()[0].equals(Empresa.class)){
					methodSetEmpresa.invoke(filtro, empresa);
				}
			}
		}
		
		return new ModelAndView("process/alterdataintegracao").addObject("filtro", filtro);
	}
	
	/**
	 * Gera o arquivo de integra��o de acordo com o filtro
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public Resource gerarArquivointegracao(WebRequestContext request, AlterdataintegracaoFiltro filtro) throws Exception {		
		String arquivoString = new String();				
		StringBuilder s = new StringBuilder();		
		
		if(filtro.getAlterdataModelointegracao() != null){
			List<Movimentacaocontabil> listaMovimentacaocontabil = movimentacaocontabilService.findForALteradataintegracaoLancamentocontabil(filtro.getEmpresa(), filtro.getDtinicio(), filtro.getDtfim());
			if(filtro.getAlterdataModelointegracao().equals(AlterdataModelointegracao.MODELO1)){
				this.gerarArquivoModelo1(s, filtro, listaMovimentacaocontabil);
			}else if(filtro.getAlterdataModelointegracao().equals(AlterdataModelointegracao.MODELO2)){
				this.gerarArquivoModelo2(s, filtro, listaMovimentacaocontabil);
			}else if(filtro.getAlterdataModelointegracao().equals(AlterdataModelointegracao.MODELO3)){
				this.gerarArquivoModelo3(s, filtro, listaMovimentacaocontabil);
			}
			arquivoString = s != null ? s.toString() : "";
		}	
		
		if(s != null && "".equals(s.toString()))
			throw new SinedException("N�o existem dados a serem exportados neste per�odo.");
		
		Resource resource = new Resource("text/plain", "alterdata_integracao_" + 
										 new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio()) + 
										 "_" + 
										 new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim()) +
										 "_" +
										 filtro.getAlterdataModelointegracao().getNome() +
										 ".txt", arquivoString.getBytes());
		return resource;
	}
	
	private void gerarArquivoModelo1(StringBuilder arquivo, AlterdataintegracaoFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil){
		if(listaMovimentacaocontabil != null && !listaMovimentacaocontabil.isEmpty()){			
			for(Movimentacaocontabil movimentacaocontabil : listaMovimentacaocontabil){
				this.criaLinhaModelo1(arquivo, filtro, movimentacaocontabil);		
			}		
		}			
	}

	private void criaLinhaModelo1(StringBuilder arquivo, AlterdataintegracaoFiltro filtro, Movimentacaocontabil movimentacaocontabil) {
		if(movimentacaocontabil != null){
			if(!arquivo.toString().equals(""))
				arquivo.append("\r\n");
			
			arquivo.append("\"\"").append(",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getDebito() != null && movimentacaocontabil.getDebito().getCodigoalternativo() != null){
				arquivo.append(movimentacaocontabil.getDebito().getCodigoalternativo());
			}else if(SinedUtil.isListNotEmpty(movimentacaocontabil.getListaDebito()) && movimentacaocontabil.getListaDebito().get(0).getContaContabil() != null &&
					movimentacaocontabil.getListaDebito().get(0).getContaContabil().getCodigoalternativo() != null){
				arquivo.append(movimentacaocontabil.getListaDebito().get(0).getContaContabil().getCodigoalternativo());
			} 
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getCredito() != null && movimentacaocontabil.getCredito().getCodigoalternativo() != null){
				arquivo.append(movimentacaocontabil.getCredito().getCodigoalternativo());
			}else if(SinedUtil.isListNotEmpty(movimentacaocontabil.getListaCredito()) && movimentacaocontabil.getListaCredito().get(0).getContaContabil() != null &&
					movimentacaocontabil.getListaCredito().get(0).getContaContabil().getCodigoalternativo() != null){
				arquivo.append(movimentacaocontabil.getListaCredito().get(0).getContaContabil().getCodigoalternativo());
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getDtlancamento() != null){
				arquivo.append(new SimpleDateFormat("dd/MM/yyyy").format(movimentacaocontabil.getDtlancamento()));
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getValor() != null){
				arquivo.append(movimentacaocontabil.getValor().toString().replace(",", ""));
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getCentrocustocredito() != null || movimentacaocontabil.getCentrocustodebito() != null){
				if(movimentacaocontabil.getCentrocustocredito() != null && movimentacaocontabil.getCentrocustocredito().getCodigoalternativo() != null){
					arquivo.append(movimentacaocontabil.getCentrocustocredito().getCodigoalternativo());
				}else if(movimentacaocontabil.getCentrocustodebito() != null && movimentacaocontabil.getCentrocustodebito().getCodigoalternativo() != null) {
					arquivo.append(movimentacaocontabil.getCentrocustodebito().getCodigoalternativo());
				}
			}
			arquivo.append("\"").append(",");
			
			arquivo.append("\"0\"").append(",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getHistorico() != null){
				arquivo.append(movimentacaocontabil.getHistorico());
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getNumero() != null){
				arquivo.append(movimentacaocontabil.getNumero());
			}
			arquivo.append("\"");
		}
	}

	private void gerarArquivoModelo2(StringBuilder arquivo, AlterdataintegracaoFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil){
		if(listaMovimentacaocontabil != null && !listaMovimentacaocontabil.isEmpty()){			
			for(Movimentacaocontabil movimentacaocontabil : listaMovimentacaocontabil){
				this.criaLinhaModelo2(arquivo, filtro, movimentacaocontabil);
			}		
		}			
	}
	
	private void criaLinhaModelo2(StringBuilder arquivo, AlterdataintegracaoFiltro filtro, Movimentacaocontabil movimentacaocontabil) {
		if(movimentacaocontabil != null){
			if(!arquivo.toString().equals(""))
				arquivo.append("\r\n");
			
			arquivo.append("\"\"").append(",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getDebito() != null && movimentacaocontabil.getDebito().getCodigoalternativo() != null){
				arquivo.append(movimentacaocontabil.getDebito().getCodigoalternativo());
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getCredito() != null && movimentacaocontabil.getCredito().getCodigoalternativo() != null){
				arquivo.append(movimentacaocontabil.getCredito().getCodigoalternativo());
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getDtlancamento() != null){
				arquivo.append(new SimpleDateFormat("dd/MM/yyyy").format(movimentacaocontabil.getDtlancamento()));
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getValor() != null){
				arquivo.append(movimentacaocontabil.getValor().toString().replace(",", ""));
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getCentrocustodebito() != null && movimentacaocontabil.getCentrocustodebito().getCodigoalternativo() != null){
				arquivo.append(movimentacaocontabil.getCentrocustodebito().getCodigoalternativo());
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getCentrocustocredito() != null && movimentacaocontabil.getCentrocustocredito().getCodigoalternativo() != null){
				arquivo.append(movimentacaocontabil.getCentrocustocredito().getCodigoalternativo());
			}
			arquivo.append("\",");
			
			arquivo.append("\"0\"").append(",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getHistorico() != null){
				arquivo.append(movimentacaocontabil.getHistorico());
			}
			arquivo.append("\",");
			
			arquivo.append("\"");
			if(movimentacaocontabil.getNumero() != null){
				arquivo.append(movimentacaocontabil.getNumero());
			}
			arquivo.append("\"");
			
		}
	}
	
	private void gerarArquivoModelo3(StringBuilder arquivo, AlterdataintegracaoFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil){
		if(listaMovimentacaocontabil != null && !listaMovimentacaocontabil.isEmpty()){			
			for(Movimentacaocontabil movimentacaocontabil : listaMovimentacaocontabil){
				this.criaLinhaModelo3(arquivo, filtro, movimentacaocontabil);		
			}		
		}			
	}
	
	private void criaLinhaModelo3(StringBuilder arquivo, AlterdataintegracaoFiltro filtro, Movimentacaocontabil movimentacaocontabil) {
		if(movimentacaocontabil != null){
			if(!arquivo.toString().equals(""))
				arquivo.append("\r\n");
			
			if(movimentacaocontabil.getDtlancamento() != null){
				arquivo.append(new SimpleDateFormat("ddMMyyyy").format(movimentacaocontabil.getDtlancamento()));
			}
			arquivo.append(" ");
			
			if(movimentacaocontabil.getDebito() != null && movimentacaocontabil.getDebito().getVcontagerencial() != null && 
					movimentacaocontabil.getDebito().getVcontagerencial().getIdentificador() != null){
				arquivo.append(this.printString(movimentacaocontabil.getDebito().getVcontagerencial().getIdentificador(), 15, true));
			}
			
			arquivo.append(" ");
			
			if(movimentacaocontabil.getCredito() != null && movimentacaocontabil.getCredito().getVcontagerencial() != null && 
					movimentacaocontabil.getCredito().getVcontagerencial().getIdentificador() != null){
				arquivo.append(this.printString(movimentacaocontabil.getCredito().getVcontagerencial().getIdentificador(), 15, true));
			}
			
			arquivo.append(" ");
			
			if(movimentacaocontabil.getValor() != null){
				arquivo.append(this.printNumero(movimentacaocontabil.getValor().toString(), 14));
			}
			
			arquivo.append(" ");
			
			if(movimentacaocontabil.getHistorico() != null){
				arquivo.append(this.printString(movimentacaocontabil.getHistorico(), 48, false));
			}			
		}
	}
	
	private String printString(String campo, Integer qtdePosicoes, boolean incluiraesqueda){
		StringBuilder novoCampo = new StringBuilder();
		if(qtdePosicoes != null && campo != null){
			if(campo.length() > qtdePosicoes){
				return campo.substring(0, qtdePosicoes );
			}
			Integer qtde = qtdePosicoes - campo.length();
			if(incluiraesqueda){
				for(int i = 0; i < qtde; i++)
					novoCampo.append(" ");
			}
			novoCampo.append(campo);
			if(!incluiraesqueda){
				for(int i = 0; i < qtde; i++)
					novoCampo.append(" ");
			}
				novoCampo.append(" ");
			
		}else if(qtdePosicoes != null){
			for(int i = 0; i < qtdePosicoes; i++)
				novoCampo.append(" ");
		}
		
		return novoCampo.toString();
	}
	
	private String printNumero(String campo, Integer qtdePosicoes){
		StringBuilder novoCampo = new StringBuilder();
		
		if(qtdePosicoes != null && campo != null){
			campo = campo.replace(".", "").replace(",", ".");
			if(campo.length() > qtdePosicoes){
				return campo.substring(0, qtdePosicoes );
			}
			Integer qtde = qtdePosicoes - campo.length();
			for(int i = 0; i < qtde; i++)
				novoCampo.append("0");
			novoCampo.append(campo);
		}else if(qtdePosicoes != null){
			for(int i = 0; i < qtdePosicoes; i++)
				novoCampo.append("0");
		}
		
		return novoCampo.toString();
	}
}
