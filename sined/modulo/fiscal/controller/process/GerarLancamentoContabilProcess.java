package br.com.linkcom.sined.modulo.fiscal.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesaitem;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.DocumentoApropriacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EntregaDocumentoApropriacao;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.geral.bean.HistoricoAntecipacao;
import br.com.linkcom.sined.geral.bean.LoteContabil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.OperacaoContabilEventoPagamento;
import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Operacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.Parcela;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Provisao;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabildebitocreditoControleEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabildebitocreditoTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.service.ColaboradordespesaService;
import br.com.linkcom.sined.geral.service.ContacontabilService;
import br.com.linkcom.sined.geral.service.DocumentoApropriacaoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntregaDocumentoApropriacaoService;
import br.com.linkcom.sined.geral.service.EntregadocumentoService;
import br.com.linkcom.sined.geral.service.EntregamaterialService;
import br.com.linkcom.sined.geral.service.FechamentoContabilService;
import br.com.linkcom.sined.geral.service.LoteContabilService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilorigemService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.OperacaoContabilEventoPagamentoService;
import br.com.linkcom.sined.geral.service.OperacaoContabilTipoLancamentoService;
import br.com.linkcom.sined.geral.service.OperacaocontabilService;
import br.com.linkcom.sined.geral.service.ProvisaoService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.GerarLancamentoContabilCentrocustoProjetoBean;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;

@Bean
@Controller(path="/contabil/process/GerarLancamentoContabil", authorizationModule=ProcessAuthorizationModule.class)
public class GerarLancamentoContabilProcess extends MultiActionController {
	
	private static final String NOME_LISTA_MOVIMENTACAOCONTABIL_SESSAO = "NOME_LISTA_MOVIMENTACAOCONTABIL_SESSAO" + GerarLancamentoContabilProcess.class.getName();
	private OperacaocontabilService operacaocontabilService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private EmpresaService empresaService;
	private MovimentacaocontabilService movimentacaocontabilService; 
	private EntregadocumentoService entregadocumentoService;
	private EntregamaterialService entregamaterialService;
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private VendaService vendaService;
	private MovimentacaocontabilorigemService movimentacaocontabilorigemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private OperacaoContabilTipoLancamentoService operacaoContabilTipoLancamentoService;
	private LoteContabilService loteContabilService;
	private DocumentoApropriacaoService documentoApropriacaoService;
	private EntregaDocumentoApropriacaoService entregaDocumentoApropriacaoService;
	private ColaboradordespesaService colaboradordespesaService;
	private ProvisaoService provisaoService;
	private OperacaoContabilEventoPagamentoService operacaoContabilEventoPagamentoService;
	private FechamentoContabilService fechamentoContabilService;
	private ContacontabilService contacontabilService;
	
	public void setOperacaocontabilService(OperacaocontabilService operacaocontabilService) {this.operacaocontabilService = operacaocontabilService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setMovimentacaocontabilService(MovimentacaocontabilService movimentacaocontabilService) {this.movimentacaocontabilService = movimentacaocontabilService;}
	public void setEntregadocumentoService(EntregadocumentoService entregadocumentoService) {this.entregadocumentoService = entregadocumentoService;}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {this.entregamaterialService = entregamaterialService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setMovimentacaocontabilorigemService(MovimentacaocontabilorigemService movimentacaocontabilorigemService) {this.movimentacaocontabilorigemService = movimentacaocontabilorigemService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setOperacaoContabilTipoLancamentoService(OperacaoContabilTipoLancamentoService operacaoContabilTipoLancamentoService) {this.operacaoContabilTipoLancamentoService = operacaoContabilTipoLancamentoService;}
	public void setLoteContabilService(LoteContabilService loteContabilService) {this.loteContabilService = loteContabilService;}
	public void setDocumentoApropriacaoService(DocumentoApropriacaoService documentoApropriacaoService) {this.documentoApropriacaoService = documentoApropriacaoService;}
	public void setEntregaDocumentoApropriacaoService(EntregaDocumentoApropriacaoService entregaDocumentoApropriacaoService) {this.entregaDocumentoApropriacaoService = entregaDocumentoApropriacaoService;}
	public void setColaboradordespesaService(ColaboradordespesaService colaboradordespesaService) {this.colaboradordespesaService = colaboradordespesaService;}
	public void setProvisaoService(ProvisaoService provisaoService) {this.provisaoService = provisaoService;}
	public void setOperacaoContabilEventoPagamentoService(OperacaoContabilEventoPagamentoService operacaoContabilEventoPagamentoService) {this.operacaoContabilEventoPagamentoService = operacaoContabilEventoPagamentoService;}
	public void setFechamentoContabilService(FechamentoContabilService fechamentoContabilService) {this.fechamentoContabilService = fechamentoContabilService;}
	public void setContacontabilService(ContacontabilService contacontabilService) {this.contacontabilService = contacontabilService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, GerarLancamentoContabilFiltro filtro) throws Exception {
		if (filtro.getMillis()==null){
			filtro.setMillis(System.currentTimeMillis());
		}
		
		if (filtro.getDtPeriodoInicio()==null && filtro.getDtPeriodoFim()==null){
			Date dataMesAnterior = SinedDateUtils.incrementDate(SinedDateUtils.currentDate(), -1, Calendar.MONTH);
			filtro.setDtPeriodoInicio(SinedDateUtils.firstDateOfMonth(dataMesAnterior));
			filtro.setDtPeriodoFim(SinedDateUtils.lastDateOfMonth(dataMesAnterior));
		}
		
		request.setAttribute("listaTipoLancamento", OperacaoContabilTipoLancamento.getLista());
		
		return new ModelAndView("process/gerarLancamentoContabil", "filtro", filtro);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView filtrar(WebRequestContext request, GerarLancamentoContabilFiltro filtro) throws Exception {
		Date ultimoFechamentoContabil = fechamentoContabilService.carregarUltimaDataFechamento(filtro.getEmpresa());
		if(ultimoFechamentoContabil != null && 
				((ultimoFechamentoContabil.equals(filtro.getDtPeriodoInicio()) || ultimoFechamentoContabil.equals(filtro.getDtPeriodoFim()))
					|| (ultimoFechamentoContabil.after(filtro.getDtPeriodoInicio()) && ultimoFechamentoContabil.before(filtro.getDtPeriodoFim()))
					|| (ultimoFechamentoContabil.after(filtro.getDtPeriodoInicio()) && ultimoFechamentoContabil.after(filtro.getDtPeriodoFim())))) {
			filtro.setDtFechamentoContabil(SinedDateUtils.toString(ultimoFechamentoContabil));
		} else {
			filtro.setEmpresa(empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.nomefantasia, empresa.razaosocial"));
			filtro.setMapaTipoLancamentoOperacaoContabil(getMapaTipoLancamentoOperacaoContabil(filtro));
			filtro.setCentrocustoEmpresa(empresaService.loadComContagerencialCentrocusto(filtro.getEmpresa()).getCentrocusto());
			
			List<Movimentacaocontabil> listaMovimentacaocontabil = new ArrayList<Movimentacaocontabil>();
			if(SinedUtil.isListNotEmpty(filtro.getListaTipoLancamento())){
				addListaMovimentacaocontabil(filtro, listaMovimentacaocontabil);
				validarListaMovimentacaocontabil(listaMovimentacaocontabil);
			}
		
//			Collections.sort(listaMovimentacaocontabil, new Comparator<Movimentacaocontabil>() {
//				@Override
//				public int compare(Movimentacaocontabil o1, Movimentacaocontabil o2) {
//					String tipoLancamentoStr1 = o1.getTipoLancamento()!=null ? o1.getTipoLancamento().getNome() : "";
//					String tipoLancamentoStr2 = o2.getTipoLancamento()!=null ? o2.getTipoLancamento().getNome() : "";
//					int compare1 = tipoLancamentoStr1.compareToIgnoreCase(tipoLancamentoStr2);
//					int compare2 = o1.getOperacaocontabil().getNome().compareToIgnoreCase(o2.getOperacaocontabil().getNome());
//					int compare3 = SinedDateUtils.diferencaDias(o1.getDtlancamento(), o2.getDtlancamento());
//					
//					if (compare1==0){
//						if (compare2==0){
//							return compare3;						
//						}else {
//							return compare2;
//						}
//					}else {
//						return compare1;
//					}
//				}
//			});  
		
			request.getSession().setAttribute(NOME_LISTA_MOVIMENTACAOCONTABIL_SESSAO + filtro.getMillis(), listaMovimentacaocontabil);
			
			filtro.setListaMovimentacaocontabil((List<Movimentacaocontabil>) SinedUtil.getListPaginacao(request, filtro, listaMovimentacaocontabil));
		}
		
		return new ModelAndView("direct:process/gerarLancamentoContabilListagem", "filtro", filtro);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView paginar(WebRequestContext request, GerarLancamentoContabilFiltro filtro) throws Exception {
		List<Movimentacaocontabil> listaMovimentacaocontabil = getlistaMovimentacaocontabilSessao(request, filtro.getMillis());
		filtro.setListaMovimentacaocontabil((List<Movimentacaocontabil>) SinedUtil.getListPaginacao(request, filtro, listaMovimentacaocontabil));
		return new ModelAndView("direct:process/gerarLancamentoContabilListagem", "filtro", filtro);
	}
	
	public ModelAndView gerarLancamentosContabeis(WebRequestContext request, GerarLancamentoContabilFiltro filtro) throws Exception {
		List<Movimentacaocontabil> listaMovimentacaocontabil = getlistaMovimentacaocontabilSessao(request, filtro.getMillis());
		
		LoteContabil lote = new LoteContabil();
		loteContabilService.saveOrUpdate(lote);
		
		for(Movimentacaocontabil movimentacao : listaMovimentacaocontabil) {
			movimentacao.setLoteContabil(lote);
		}
		
		movimentacaocontabilService.transactionGerarLancamentoContabeis(listaMovimentacaocontabil);
		
		filtro.setMillis(null);
		filtro.setListaMovimentacaocontabil(null);
		filtro.setMapaTipoLancamentoOperacaoContabil(null);
		filtro.setCentrocustoEmpresa(null);
		filtro.setProjeto(null);
		
		request.getSession().removeAttribute(NOME_LISTA_MOVIMENTACAOCONTABIL_SESSAO + filtro.getMillis());
		request.addMessage("Lan�amentos cont�beis gerados com sucesso.", MessageType.INFO);
		
		return index(request, filtro);
	}
	
	@SuppressWarnings("unchecked")
	private List<Movimentacaocontabil> getlistaMovimentacaocontabilSessao(WebRequestContext request, Long millis) throws Exception {
		return (List<Movimentacaocontabil>) request.getSession().getAttribute(NOME_LISTA_MOVIMENTACAOCONTABIL_SESSAO + millis);
	}
	
	private Map<OperacaoContabilTipoLancamento, List<Operacaocontabil>> getMapaTipoLancamentoOperacaoContabil(GerarLancamentoContabilFiltro filtro) throws Exception {
		//Mapa que � utilizado para auxiliar na cria��o das movimenta��es cont�beis. A chave � o Tipo e seu valor a Opera��o Cont�bil cadastrada 
		Map<OperacaoContabilTipoLancamento, List<Operacaocontabil>> mapaTipoLancamentoOperacaoContabil = new HashMap<OperacaoContabilTipoLancamento, List<Operacaocontabil>>();
		List<Operacaocontabil> listaOperacaocontabil = operacaocontabilService.findForGerarLancamentoContabil(filtro);
		
		for (Operacaocontabil operacaocontabil: listaOperacaocontabil){
			if (!OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO.equals(operacaocontabil.getTipoLancamento())){
				if(mapaTipoLancamentoOperacaoContabil.get(operacaocontabil.getTipoLancamento()) == null){
					mapaTipoLancamentoOperacaoContabil.put(operacaocontabil.getTipoLancamento(), new ArrayList<Operacaocontabil>());
				}
				
				mapaTipoLancamentoOperacaoContabil.get(operacaocontabil.getTipoLancamento()).add(operacaocontabil);
				
			}
		}
		
		return mapaTipoLancamentoOperacaoContabil;
	}

	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Nota nota) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.CONTA_CONTABIL, nota.getCliente().getContaContabil());
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.COLABORADOR, nota.getCliente().getContaContabil());
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, nota.getCliente().getContaContabil());
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Entregadocumento entregadocumento) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		if (entregadocumento.getFornecedor()!=null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.FORNECEDOR, entregadocumento.getFornecedor().getContaContabil());
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, entregadocumento.getFornecedor().getContaContabil());
		}
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Entregamaterial entregamaterial, Empresa empresa) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		if (entregamaterial.getMaterial() !=null){
			ContaContabil contaContabil = this.getContaContabilByMaterial(entregamaterial.getMaterial(), empresa);
			if(Util.objects.isPersistent(contaContabil)){
				mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.MATERIAL, contaContabil);
			}
		}
		return mapaMapaContaContabil;
	}
	
	private ContaContabil getContaContabilByMaterial(Material material, Empresa empresa){
		return Util.objects.isPersistent(material.getContaContabil())? material.getContaContabil(): contacontabilService.loadForLancamentoContabil(material, empresa);
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Notafiscalprodutoitem notafiscalprodutoitem, Empresa empresa) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		if (notafiscalprodutoitem.getMaterial() !=null){
			ContaContabil contaContabil = this.getContaContabilByMaterial(notafiscalprodutoitem.getMaterial(), empresa);
			if(Util.objects.isPersistent(contaContabil)){
				mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.MATERIAL, contaContabil);
			}
		}
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Documento documento) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		Pessoa pessoa = documento.getPessoa();
		Movimentacao movimentacao = null;
		
		if(Hibernate.isInitialized(documento.getListaMovimentacaoOrigem()) && SinedUtil.isListNotEmpty(documento.getListaMovimentacaoOrigem())){
			Movimentacaoorigem movimentacaoorigem = new ListSet<Movimentacaoorigem>(Movimentacaoorigem.class, documento.getListaMovimentacaoOrigem()).get(0);
			movimentacao = movimentacaoorigem.getMovimentacao();
		}
		
		if (pessoa!=null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, documento.getContaContabilForPessoa());
			if(pessoa.getCliente()!=null){
				mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.CLIENTE, pessoa.getCliente().getContaContabil());
			}
			if (pessoa.getFornecedorBean()!=null){
				mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.FORNECEDOR, pessoa.getFornecedorBean().getContaContabil());
			}
		}
		
		
		if (movimentacao!=null && movimentacao.getConta()!=null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.VINCULO_CONTA_BANCARIA_CAIXA, movimentacao.getConta().getContaContabil());			
		}
		
		if(movimentacao!=null && Hibernate.isInitialized(movimentacao.getListaMovimentacaoorigemcartaocredito()) && SinedUtil.isListNotEmpty(movimentacao.getListaMovimentacaoorigemcartaocredito())){
			Movimentacaoorigem movimentacaoorigemcartaocredito = movimentacao.getListaMovimentacaoorigemcartaocredito().get(0);
			if (movimentacaoorigemcartaocredito!=null && movimentacaoorigemcartaocredito.getConta()!=null){
				mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.VINCULO_ORIGEM, movimentacaoorigemcartaocredito.getConta().getContaContabil());			
			}
		}
		
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Provisao provisao) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		Colaborador pessoa = provisao.getColaborador();
		if (pessoa!=null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, pessoa.getContaContabil());
		}
		
		
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Colaboradordespesa colaboradorDespesa) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		Colaborador pessoa = colaboradorDespesa.getColaborador();
		if (pessoa!=null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, pessoa.getContaContabil());
		}
		
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Colaboradordespesa colaboradorDespesa, Colaboradordespesaitem colaboradorDespesaItem) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		Colaborador pessoa = colaboradorDespesa.getColaborador();
		if (pessoa!=null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, pessoa.getContaContabil());
		}
		if(colaboradorDespesaItem.getEvento() != null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.EVENTO, colaboradorDespesaItem.getEvento().getContaContabil());
		}
		
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabilAdiantamento(Documento documento) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		Pessoa pessoa = documento.getPessoa();
		
		if (pessoa!=null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, documento.getContaContabilForPessoa());
			if (pessoa.getCliente()!=null){
				mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.CLIENTE, pessoa.getCliente().getContaContabil());
			}
			if (pessoa.getFornecedorBean()!=null){
				mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.FORNECEDOR, pessoa.getFornecedorBean().getContaContabil());
			}
		}

		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabilTransferencia(Movimentacao movimentacaoCredito) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		Movimentacaoorigem movimentacaoorigem = new ListSet<Movimentacaoorigem>(Movimentacaoorigem.class, movimentacaoCredito.getListaMovimentacaoorigem()).get(0);
		
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.VINCULO_CONTA_BANCARIA_CAIXA, movimentacaoCredito.getConta().getContaContabil());			
		
		if (movimentacaoorigem.getConta()!=null){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.VINCULO_ORIGEM, movimentacaoorigem.getConta().getContaContabil());			
		}
		
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Movimentacao movimentacao) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.VINCULO_CONTA_BANCARIA_CAIXA, movimentacao.getConta().getContaContabil());
		
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Movimentacao movimentacao, Documento documentoOrigem) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, documentoOrigem.getContaContabilForPessoa());	
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.VINCULO_CONTA_BANCARIA_CAIXA, movimentacao.getConta() != null ? movimentacao.getConta().getContaContabil() : null);
		
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Movimentacaoestoque movimentacaoestoque, Empresa empresa) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		ContaContabil contaContabil = this.getContaContabilByMaterial(movimentacaoestoque.getMaterial(), empresa);
		if(Util.objects.isPersistent(contaContabil)){
			mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.MATERIAL, contaContabil);
		}
		
		return mapaMapaContaContabil;
	}
	
	private Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> getMapaContaContabil(Venda venda) throws Exception {
		Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaMapaContaContabil = new HashMap<OperacaocontabildebitocreditoTipoEnum, ContaContabil>();
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.CLIENTE, venda.getCliente().getContaContabil());
		mapaMapaContaContabil.put(OperacaocontabildebitocreditoTipoEnum.PESSOA, venda.getCliente().getContaContabil());
		return mapaMapaContaContabil;
	}

	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(NotaFiscalServico nfs, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamentoEnum) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		
		if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)
				|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)){
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_BRUTO_NOTA, SinedUtil.zeroIfNull(nfs.getValorTotalServicos()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_LIQUIDO_NOTA, SinedUtil.zeroIfNull(nfs.getValorNota()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_OUTRAS_DESPESAS, SinedUtil.zeroIfNull(nfs.getOutrasretencoes()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESCONTO, SinedUtil.getSoma(Arrays.asList(new Money[]{nfs.getDescontoincondicionado(), nfs.getDescontocondicionado()})));
		}
		
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS.equals(movimentacaocontabilTipoLancamentoEnum) 
                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS, SinedUtil.zeroIfNull(nfs.getValoriss()));
        	mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS_RETIDO, SinedUtil.zeroIfNull(nfs.getValoriss()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IR.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IR, SinedUtil.zeroIfNull(nfs.getValorIr()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_INSS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_INSS_RETIDO, SinedUtil.zeroIfNull(nfs.getValorInss()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_CSLL.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_CSLL, SinedUtil.zeroIfNull(nfs.getValorCsll()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ICMS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMS, SinedUtil.zeroIfNull(nfs.getValorIcms()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_PIS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS, SinedUtil.zeroIfNull(nfs.getValorPis()));
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS_RETIDO, SinedUtil.zeroIfNull(nfs.getValorPisRetido()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_COFINS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_COFINS, SinedUtil.zeroIfNull(nfs.getValorCofins()));
        }
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Notafiscalproduto nfp, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamentoEnum) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();

		if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)
				|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)){
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_BRUTO_NOTA, SinedUtil.zeroIfNull(nfp.getValorprodutos()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_LIQUIDO_NOTA, SinedUtil.zeroIfNull(nfp.getValor()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_OUTRAS_DESPESAS, SinedUtil.zeroIfNull(nfp.getOutrasdespesas()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_FRETE, SinedUtil.zeroIfNull(nfp.getValorfrete()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_SEGURO, SinedUtil.zeroIfNull(nfp.getValorseguro()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESCONTO, SinedUtil.zeroIfNull(nfp.getValordesconto()));
		}
        
		if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS.equals(movimentacaocontabilTipoLancamentoEnum) 
				|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS, SinedUtil.zeroIfNull(nfp.getValoriss()));
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS_RETIDO, SinedUtil.zeroIfNull(nfp.getValorissretido()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IR.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IR, SinedUtil.zeroIfNull(nfp.getValorirrf()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_INSS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_INSS_RETIDO, SinedUtil.zeroIfNull(nfp.getValorprevsocial()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_CSLL.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_CSLL, SinedUtil.zeroIfNull(nfp.getValorcsll()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_PIS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS, SinedUtil.zeroIfNull(nfp.getValorpis()));
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS_RETIDO, SinedUtil.zeroIfNull(nfp.getValorpisretido()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_COFINS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_COFINS, SinedUtil.zeroIfNull(nfp.getValorcofins()));
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_COFINS_RETIDO, SinedUtil.zeroIfNull(nfp.getValorcofinsretido()));
        }
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ICMS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMS, SinedUtil.zeroIfNull(nfp.getValoricms()));
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMSST, SinedUtil.zeroIfNull(nfp.getValoricmsst()));
        }
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_II.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_II, SinedUtil.zeroIfNull(nfp.getValorii()));
        }
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IPI.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IPI, SinedUtil.zeroIfNull(nfp.getValoripi()));
        }
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Entregadocumento entregadocumento) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_BRUTO_NOTA, SinedUtil.zeroIfNull(entregadocumento.getValormercadoria()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_LIQUIDO_NOTA, SinedUtil.zeroIfNull(entregadocumento.getValor()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_OUTRAS_DESPESAS, SinedUtil.zeroIfNull(entregadocumento.getValoroutrasdespesas()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_FRETE, SinedUtil.zeroIfNull(entregadocumento.getValorfrete()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_SEGURO, SinedUtil.zeroIfNull(entregadocumento.getValorseguro()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IR, entregamaterialService.getValorIR(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_INSS_RETIDO, entregamaterialService.getValorINSS(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_CSLL, entregamaterialService.getValorCSLL(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_II, entregamaterialService.getValorII(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS, entregamaterialService.getValorISS(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS_RETIDO, entregamaterialService.getValorISS(entregadocumento.getListaEntregamaterial(), true));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMS, entregamaterialService.getValorICMS(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMSST, entregamaterialService.getValorICMSST(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IPI, entregamaterialService.getValorIPI(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS, entregamaterialService.getValorPIS(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS_RETIDO, entregamaterialService.getValorPISRetido(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_COFINS, entregamaterialService.getValorCOFINS(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_COFINS_RETIDO, entregamaterialService.getValorCOFINSRetido(entregadocumento.getListaEntregamaterial()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESCONTO, SinedUtil.zeroIfNull(entregadocumento.getValordesconto()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IMPORTACAO, SinedUtil.zeroIfNull(entregadocumento.getValorImportacaoCalculado()));
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Entregadocumento entregadocumento, EntregaDocumentoApropriacao apropriacao) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_LIQUIDO_NOTA, SinedUtil.zeroIfNull(apropriacao.getValor()));
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Entregamaterial entregamaterial) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_OUTRAS_DESPESAS, SinedUtil.zeroIfNull(entregamaterial.getValoroutrasdespesas()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_FRETE, SinedUtil.zeroIfNull(entregamaterial.getValorfrete()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_SEGURO, SinedUtil.zeroIfNull(entregamaterial.getValorseguro()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IR, SinedUtil.zeroIfNull(entregamaterial.getValorir()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_INSS_RETIDO, SinedUtil.zeroIfNull(entregamaterial.getValorinss()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_CSLL, SinedUtil.zeroIfNull(entregamaterial.getValorcsll()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_II, SinedUtil.zeroIfNull(entregamaterial.getValorimpostoimportacao()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS, SinedUtil.zeroIfNull(entregamaterial.getValoriss()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS_RETIDO, SinedUtil.zeroIfNull(Tipotributacaoiss.RETIDA.equals(entregamaterial.getTipotributacaoiss()) ? entregamaterial.getValoriss() : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMS, SinedUtil.zeroIfNull(entregamaterial.getValoricms()));
		if(!Boolean.TRUE.equals(entregamaterial.getNaoconsideraricmsst())){
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMSST, SinedUtil.zeroIfNull(entregamaterial.getValoricmsst()));
		}else{
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMSST, new Money(0));
		}
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IPI, SinedUtil.zeroIfNull(entregamaterial.getValoripi()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS, SinedUtil.zeroIfNull(entregamaterial.getValorpis()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS_RETIDO, SinedUtil.zeroIfNull(entregamaterial.getValorpisretido()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_COFINS, SinedUtil.zeroIfNull(entregamaterial.getValorcofins()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_COFINS_RETIDO, SinedUtil.zeroIfNull(entregamaterial.getValorcofinsretido()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESCONTO, SinedUtil.zeroIfNull(entregamaterial.getValordesconto()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MATERIAL, entregamaterial.getValorTotalItem() != null ? new Money(entregamaterial.getValorTotalItem()) : new Money());

		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IMPORTACAO, entregamaterial.getValorImportacaoCalculado());
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Entregamaterial entregamaterial, EntregaDocumentoApropriacao apropriacao) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MATERIAL, entregamaterial.getValorProporcionalApropriacao() != null? new Money(entregamaterial.getValorProporcionalApropriacao()): new Money());
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Notafiscalprodutoitem notafiscalprodutoitem, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamentoEnum) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		
		if(!MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_II.equals(movimentacaocontabilTipoLancamentoEnum) &&
			!MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IPI.equals(movimentacaocontabilTipoLancamentoEnum)){			
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_OUTRAS_DESPESAS, SinedUtil.zeroIfNull(notafiscalprodutoitem.getOutrasdespesas()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_FRETE, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorfrete()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_SEGURO, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorseguro()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESCONTO, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValordesconto()));
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MATERIAL, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorbruto()));
		}
        
		if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS.equals(movimentacaocontabilTipoLancamentoEnum) 
				|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValoriss()));
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ISS_RETIDO, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorissretido()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IR.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IR, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorir()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_INSS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_INSS_RETIDO, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorinss()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_CSLL.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_CSLL, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorcsll()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_PIS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PIS, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorpis()));
        }
        
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_COFINS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_COFINS, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorcofins()));
        }
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ICMS.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMS, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValoricms()));
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ICMSST, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValoricmsst()));
        }
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_II.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_II, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorii()));
        }
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IPI.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_IPI, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValoripi()));
        }
        if(movimentacaocontabilTipoLancamentoEnum == null || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_II.equals(movimentacaocontabilTipoLancamentoEnum) 
        		|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamentoEnum)
        		|| MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamentoEnum)){
            mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_II, SinedUtil.zeroIfNull(notafiscalprodutoitem.getValorii()));
        }
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Documento documento, Movimentacao movimentacao, Money valorMovimentacao) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		
		boolean considerarTaxas = movimentacao == null 
				|| movimentacao.getValor() == null 
				|| documento.getValor() == null
				|| documento.getValor().compareTo(movimentacao.getValor()) != 0
				;
				
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_JUROS, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? (considerarTaxas ? documento.getAux_documento().getValorjurosOuJurosMes() : null) : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MULTA, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? (considerarTaxas ? documento.getAux_documento().getValormulta() : null) : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESCONTO, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? (considerarTaxas ? documento.getAux_documento().getValordesconto() : null) : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESAGIO, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? (considerarTaxas ? documento.getAux_documento().getValordesagio() : null) : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_TAXA_BOLETO, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? (considerarTaxas ? documento.getAux_documento().getValortaxaboleto() : null) : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_TERMO, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? (considerarTaxas ? documento.getAux_documento().getValortermo() : null) : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MOVIMENTO, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? (considerarTaxas ? documento.getAux_documento().getValortaxamovimento() : null) : null));
		//mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MOVIMENTACAO, SinedUtil.zeroIfNull(movimentacao != null ? movimentacao.getValor() : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MOVIMENTACAO, SinedUtil.zeroIfNull(valorMovimentacao != null ? valorMovimentacao : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ATUAL_CONTA, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? documento.getAux_documento().getValoratual() : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ORIGINAL_CONTA, SinedUtil.zeroIfNull(documento.getValor()));
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Documento documento) throws Exception {
		return getMapaValor(documento, null, null);
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(HistoricoAntecipacao ha, Documento documento) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_UTILIZADO, SinedUtil.zeroIfNull(ha.getValor()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ATUAL_CONTA, SinedUtil.zeroIfNull(documento.getAux_documento() != null ? documento.getAux_documento().getValoratual() : null));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ORIGINAL_CONTA, SinedUtil.zeroIfNull(documento.getValor()));
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Provisao provisao) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_PROVISAO, SinedUtil.zeroIfNull(provisao.getValor()));
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Colaboradordespesa colaboradorDespesa) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESPESA_COLABORADOR, SinedUtil.zeroIfNull(colaboradorDespesa.getValorRateio()));
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Colaboradordespesa colaboradorDespesa, Colaboradordespesaitem colaboradorDespesaItem) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_DESPESA_COLABORADOR, SinedUtil.zeroIfNull(colaboradorDespesa.getValorRateio()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_EVENTO_DESPESA_COLABORADOR, SinedUtil.zeroIfNull(colaboradorDespesaItem.getValor()));
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Movimentacao movimentacao) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MOVIMENTACAO, SinedUtil.zeroIfNull(movimentacao.getValor()));
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Movimentacao movimentacao, Documento documentoOrigem, Integer countDocumentos) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		if(countDocumentos > 1){
			if(documentoOrigem != null && documentoOrigem.getAux_documento() != null){
				mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MOVIMENTACAO, SinedUtil.zeroIfNull(documentoOrigem.getAux_documento() != null ? documentoOrigem.getAux_documento().getValoratual() : null));
			}
		}else{
			mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MOVIMENTACAO, SinedUtil.zeroIfNull(movimentacao.getValor()));
		}
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Movimentacaoestoque movimentacaoestoque) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MOVIMENTACAO_ESTOQUE, movimentacaoestoque.getValor() != null ? new Money(movimentacaoestoque.getValor()) : new Money()); //valor movimentacao
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_CUSTO, movimentacaoestoque.getValorcusto() != null ? new Money(movimentacaoestoque.getValorcusto()) : new Money()); //valor de custo
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_CUSTO_MEDIO, movimentacaoestoque.getMaterial() != null ? movimentacaoestoque.getMaterial().getValorcusto() != null ? new Money(movimentacaoestoque.getMaterial().getValorcusto()) : new Money() : null); //valor custo medio

		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Rateioitem rateioitem) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ITEM_RATEIO, SinedUtil.zeroIfNull(rateioitem.getValor()));
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Rateioitem rateioitem, Double indice) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_ITEM_RATEIO, SinedUtil.zeroIfNull(rateioitem.getValor() !=null ? new Money(rateioitem.getValor().getValue().doubleValue()*indice) : null));
		
		return mapaValor;
	}
	
	private Map<OperacaocontabildebitocreditoControleEnum, Money> getMapaValor(Venda venda) throws Exception {
		Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = new HashMap<OperacaocontabildebitocreditoControleEnum, Money>();
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_VENDA, SinedUtil.zeroIfNull(venda.getTotalvenda()));
		mapaValor.put(OperacaocontabildebitocreditoControleEnum.VALOR_MATERIAL, SinedUtil.zeroIfNull(venda.getTotalproduto()));
		
		return mapaValor;
	}

	private Map<String, String> getMapaHistorico(Nota nota, Operacaocontabil operacaocontabil) throws Exception {
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, nota.getNumero());
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CLIENTE, nota.getCliente().getRazaosocial());
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, NeoFormater.getInstance().format(nota.getDtEmissao()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, operacaocontabil != null && operacaocontabil.getCodigointegracao() != null ? operacaocontabil.getCodigointegracao().toString() : null);
		
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistorico(Movimentacaoestoque movimentacaoestoque, Operacaocontabil operacaocontabil) throws Exception {
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		if(movimentacaoestoque.getMovimentacaoestoqueorigem() != null) {
			if(movimentacaoestoque.getMovimentacaoestoqueorigem().getEntregamaterial() != null){
				if(movimentacaoestoque.getMovimentacaoestoqueorigem().getEntregamaterial().getEntregadocumento() != null){
					mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_NUMERO_NOTA, movimentacaoestoque.getMovimentacaoestoqueorigem().getEntregamaterial().getEntregadocumento().getNumero());
				}
			} else if(movimentacaoestoque.getMovimentacaoestoqueorigem().getNotafiscalproduto() != null){
				mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_NUMERO_NOTA, movimentacaoestoque.getMovimentacaoestoqueorigem().getNotafiscalproduto().getNumero());
			}
		}	
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistorico(Entregadocumento entregadocumento, Operacaocontabil operacaocontabil) throws Exception {
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, entregadocumento.getNumero());
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_FORNECEDOR, entregadocumento.getFornecedor()!=null ? entregadocumento.getFornecedor().getRazaosocial() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, NeoFormater.getInstance().format(entregadocumento.getDtemissao()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, operacaocontabil != null && operacaocontabil.getCodigointegracao() != null ? operacaocontabil.getCodigointegracao().toString() : null);
		
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistorico(Documento documento, Operacaocontabil operacaocontabil) throws Exception {
		Movimentacao movimentacao = null;
		
		if (Hibernate.isInitialized(documento.getListaMovimentacaoOrigem()) && SinedUtil.isListNotEmpty(documento.getListaMovimentacaoOrigem())){
			movimentacao = new ListSet<Movimentacaoorigem>(Movimentacaoorigem.class, documento.getListaMovimentacaoOrigem()).get(0).getMovimentacao();
		}
		
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, Util.strings.emptyIfNull(documento.getNumero()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO, Util.strings.emptyIfNull(documento.getDescricao()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_PAGAMENTOA, Util.strings.emptyIfNull(documento.getPagamentoa()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CLIENTE, Tipopagamento.CLIENTE.equals(documento.getTipopagamento()) && documento.getPessoa()!=null && documento.getPessoa().getCliente()!=null ? documento.getPessoa().getCliente().getRazaosocial() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_FORNECEDOR, Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento()) && documento.getPessoa()!=null && documento.getPessoa().getFornecedorBean()!=null ? documento.getPessoa().getFornecedorBean().getRazaosocial() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_PARCELA, documento.getListaParcela()!=null && !documento.getListaParcela().isEmpty() ? new ListSet<Parcela>(Parcela.class, documento.getListaParcela()).get(0).getOrdem().toString() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, movimentacao!=null ? NeoFormater.getInstance().format(movimentacao.getDataMovimentacao()) : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CHEQUE, movimentacao!=null && movimentacao.getCheque()!=null ? movimentacao.getCheque().getDescricaoCompleta() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, operacaocontabil != null && operacaocontabil.getCodigointegracao() != null ? operacaocontabil.getCodigointegracao().toString() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_VALOR_UTILIZADO_ADIANTAMENTO, SinedUtil.isListNotEmpty(documento.getListaHistoricoAntecipacao()) ? documento.getValorAntecipacao().toString() : null);
		
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistorico(Provisao provisao, Operacaocontabil operacaocontabil) throws Exception {
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, NeoFormater.getInstance().format(provisao.getDtProvisao()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_COLABORADOR, operacaocontabil != null && provisao.getColaborador() != null? provisao.getColaborador().getNome(): null);
		
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistorico(Colaboradordespesa colaboradorDespesa, Colaboradordespesaitem colaboradorDespesaItem, Operacaocontabil operacaocontabil) throws Exception {
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, NeoFormater.getInstance().format(colaboradorDespesa.getDtdeposito()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_COLABORADOR, operacaocontabil != null && colaboradorDespesa.getColaborador() != null? colaboradorDespesa.getColaborador().getNome(): null);
		if(colaboradorDespesaItem != null){
			mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_EVENTO, operacaocontabil != null && colaboradorDespesaItem.getEvento() != null? colaboradorDespesaItem.getEvento().getNome(): null);
		}
		
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistorico(HistoricoAntecipacao ha, Documento documento, Operacaocontabil operacaocontabil) throws Exception {
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, Util.strings.emptyIfNull(documento.getNumero()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO, Util.strings.emptyIfNull(documento.getDescricao()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_PAGAMENTOA, Util.strings.emptyIfNull(documento.getPagamentoa()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CLIENTE, Tipopagamento.CLIENTE.equals(documento.getTipopagamento()) && documento.getPessoa()!=null && documento.getPessoa().getCliente()!=null ? documento.getPessoa().getCliente().getRazaosocial() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_FORNECEDOR, Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento()) && documento.getPessoa()!=null && documento.getPessoa().getFornecedorBean()!=null ? documento.getPessoa().getFornecedorBean().getRazaosocial() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_PARCELA, documento.getListaParcela()!=null && !documento.getListaParcela().isEmpty() ? new ListSet<Parcela>(Parcela.class, documento.getListaParcela()).get(0).getOrdem().toString() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, ha!=null ? NeoFormater.getInstance().format(ha.getDataHora()) : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, operacaocontabil != null && operacaocontabil.getCodigointegracao() != null ? operacaocontabil.getCodigointegracao().toString() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_VALOR_UTILIZADO_ADIANTAMENTO, ha != null && ha.getValor() != null ? ha.getValor().toString() : null);
		
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistoricoTransferencia(Movimentacao movimentacaoOrigem, Operacaocontabil operacaocontabil) throws Exception {
		List<Movimentacaoorigem> listaMovimentacaoorigemOrigem = movimentacaoOrigem.getListaMovimentacaoorigem();
		Movimentacao movimentacaoDestino = listaMovimentacaoorigemOrigem.get(0).getMovimentacaorelacionada();
		String chequeStr = "";
		
		for (Movimentacaoorigem movimentacaoorigemOrigem: listaMovimentacaoorigemOrigem){
			Cheque cheque = movimentacaoorigemOrigem.getMovimentacaorelacionada().getCheque();
			if (cheque!=null){
				chequeStr += chequeStr.trim().equals("") ? "" : " / "; 
				chequeStr += cheque.getDescricaoCompleta();
			}
		}
		
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, movimentacaoOrigem.getChecknum());
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CONTAORIGEM, movimentacaoOrigem.getConta()!=null && movimentacaoOrigem.getConta().getContaContabil()!=null ? movimentacaoOrigem.getConta().getContaContabil().getNome() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CONTADESTINO, movimentacaoDestino.getConta()!=null && movimentacaoDestino.getConta().getContaContabil()!=null ? movimentacaoDestino.getConta().getContaContabil().getNome() : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, NeoFormater.getInstance().format(movimentacaoOrigem.getDataMovimentacao()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CHEQUE, chequeStr);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO, Util.strings.emptyIfNull(movimentacaoOrigem.getHistorico()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, operacaocontabil != null && operacaocontabil.getCodigointegracao() != null ? operacaocontabil.getCodigointegracao().toString() : null);
		
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistorico(Movimentacao movimentacao, Operacaocontabil operacaocontabil) throws Exception {
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, movimentacao.getChecknum());
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, NeoFormater.getInstance().format(movimentacao.getDataMovimentacao()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO, Util.strings.emptyIfNull(movimentacao.getHistorico()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, operacaocontabil != null && operacaocontabil.getCodigointegracao() != null ? operacaocontabil.getCodigointegracao().toString() : null);
		
		return mapaHistorico;
	}
	
	private Map<String, String> getMapaHistorico(Venda venda, Operacaocontabil operacaocontabil) throws Exception {
		Map<String, String> mapaHistorico = new HashMap<String, String>();
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CLIENTE, venda.getCliente().getNome());
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, NeoFormater.getInstance().format(venda.getDtvenda()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_VENDA, venda.getCdvenda().toString());
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_IDENTIFICADORVENDA, Util.strings.emptyIfNull(venda.getIdentificadorexterno()));
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO, SinedUtil.isListNotEmpty(venda.getListavendapagamento()) ? CollectionsUtil.listAndConcatenate(venda.getListavendapagamento(), "documento.descricao", ", ") : null);
		mapaHistorico.put(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, operacaocontabil != null && operacaocontabil.getCodigointegracao() != null ? operacaocontabil.getCodigointegracao().toString() : null);
		
		return mapaHistorico;
	}
	
	private ContaContabil getContaContabil(Operacaocontabildebitocredito operacaocontabildebitocredito, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil){
		ContaContabil contaContabil = null;
		if (mapaContaContabil.containsKey(operacaocontabildebitocredito.getTipo())){
			contaContabil = mapaContaContabil.get(operacaocontabildebitocredito.getTipo());
		}
		
		if (contaContabil == null && operacaocontabildebitocredito.getContaContabil()!=null){
			contaContabil =  operacaocontabildebitocredito.getContaContabil();
		}
		return contaContabil;
	}
	
	private List<Movimentacaocontabildebitocredito> getListaDebitoCredito(List<Operacaocontabildebitocredito> listaOperacaocontabildebitocredito, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, Boolean agruparvalormesmaconta, Map<String, String> mapaHistorico, MovimentacaocontabilTipoLancamentoEnum tipoLancamento) throws Exception {
		return getListaDebitoCredito(listaOperacaocontabildebitocredito, mapaContaContabil, mapaValor, listaCentrocustoProjetoBean, null, agruparvalormesmaconta, null, mapaHistorico, tipoLancamento);
	}
	
	private List<Movimentacaocontabildebitocredito> getListaDebitoCredito(List<Operacaocontabildebitocredito> listaOperacaocontabildebitocredito, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, List<Movimentacaocontabildebitocredito> listaDebitoCreditoItens, Boolean agruparvalormesmaconta, OperacaocontabildebitocreditoTipoEnum tipo, Map<String, String> mapaHistorico, MovimentacaocontabilTipoLancamentoEnum tipoLancamento) throws Exception {
		return getListaDebitoCredito(listaOperacaocontabildebitocredito, mapaContaContabil, mapaValor, listaCentrocustoProjetoBean, listaDebitoCreditoItens, agruparvalormesmaconta, tipo, mapaHistorico, null, tipoLancamento);
	}
	
	private List<Movimentacaocontabildebitocredito> getListaDebitoCredito(List<Operacaocontabildebitocredito> listaOperacaocontabildebitocredito, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, List<Movimentacaocontabildebitocredito> listaDebitoCreditoItens, Boolean agruparvalormesmaconta, OperacaocontabildebitocreditoTipoEnum tipo, Map<String, String> mapaHistorico, Rateioitem rateioitem, MovimentacaocontabilTipoLancamentoEnum tipoLancamento) throws Exception {
		List<Movimentacaocontabildebitocredito> listaDebitoCredito = new ArrayList<Movimentacaocontabildebitocredito>();
		
		if(SinedUtil.isListNotEmpty(listaDebitoCreditoItens)){
			listaDebitoCredito.addAll(listaDebitoCreditoItens);
		}
		
		if (listaOperacaocontabildebitocredito!=null
				&& (SinedUtil.isListEmpty(listaDebitoCreditoItens) || MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL.equals(tipoLancamento) || MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO.equals(tipoLancamento))){
			for (Operacaocontabildebitocredito operacaocontabildebitocredito: listaOperacaocontabildebitocredito){
				if(tipo != null && operacaocontabildebitocredito.getTipo() != null && !tipo.equals(operacaocontabildebitocredito.getTipo())){
					continue;
				}
				
				ContaContabil contaContabil = getContaContabil(operacaocontabildebitocredito, mapaContaContabil);
				Money valor = getValorOperacaoContabil(operacaocontabildebitocredito.getControle(), operacaocontabildebitocredito.getFormula(), mapaValor);
				if (contaContabil!=null && valor!=null && valor.toLong()>0L){
					//Caso a opera��o cont�bil possuir o check "Centro de custo" marcado � criado um item na movimenta��o para cada centro de custo do rateio de origem
					if (!OperacaocontabildebitocreditoControleEnum.VALOR_ITEM_RATEIO.equals(operacaocontabildebitocredito.getControle()) && (Boolean.TRUE.equals(operacaocontabildebitocredito.getCentrocusto()) || Boolean.TRUE.equals(operacaocontabildebitocredito.getProjeto()))
							&& listaCentrocustoProjetoBean!=null && !listaCentrocustoProjetoBean.isEmpty()){
						for (GerarLancamentoContabilCentrocustoProjetoBean centrocustoProjetoBean: listaCentrocustoProjetoBean){
							Money valorRateio;
							if (centrocustoProjetoBean.getValor()!=null){
								valorRateio = centrocustoProjetoBean.getValor();
							}else {
								valorRateio = valor.multiply(new Money(centrocustoProjetoBean.getPercentual())).divide(new Money(100));
							}
							
							Centrocusto centrocusto = null;
							Projeto projeto = null;
							
							if (Boolean.TRUE.equals(operacaocontabildebitocredito.getCentrocusto())){
								centrocusto = centrocustoProjetoBean.getCentrocusto();
							}
							
							if (Boolean.TRUE.equals(operacaocontabildebitocredito.getProjeto())){
								projeto = centrocustoProjetoBean.getProjeto();
							}
							
							listaDebitoCredito.add(new Movimentacaocontabildebitocredito(contaContabil, centrocusto, projeto, valorRateio, getHistoricoItem(operacaocontabildebitocredito, mapaHistorico), operacaocontabildebitocredito));
						}
					}else {
						// validar se as flags centrocusto e projeto est�o true e se o rateio item est� diferente de null
						if(rateioitem != null) {
							Centrocusto centrocusto = Boolean.TRUE.equals(operacaocontabildebitocredito.getCentrocusto()) && rateioitem.getCentrocusto() != null ? rateioitem.getCentrocusto() : null;
							Projeto projeto = Boolean.TRUE.equals(operacaocontabildebitocredito.getProjeto()) && rateioitem.getProjeto() != null ? rateioitem.getProjeto() : null;
							
							listaDebitoCredito.add(new Movimentacaocontabildebitocredito(contaContabil, centrocusto, projeto, valor, getHistoricoItem(operacaocontabildebitocredito, mapaHistorico), operacaocontabildebitocredito));
						} else {
							listaDebitoCredito.add(new Movimentacaocontabildebitocredito(contaContabil, null, null, valor, getHistoricoItem(operacaocontabildebitocredito, mapaHistorico), operacaocontabildebitocredito));
						}
					}
				}
			}
		}
		
		if (Boolean.TRUE.equals(agruparvalormesmaconta) && listaDebitoCredito.size()>1){
			Map<String, List<Movimentacaocontabildebitocredito>> mapa = new HashMap<String, List<Movimentacaocontabildebitocredito>>();
			
			for (Movimentacaocontabildebitocredito movimentacaocontabildebitocredito: listaDebitoCredito){
				String key = movimentacaocontabildebitocredito.getContaContabil().getCdcontacontabil() + "-";
				key += movimentacaocontabildebitocredito.getCentrocusto()!=null ? movimentacaocontabildebitocredito.getCentrocusto().getCdcentrocusto() : "";
				key += movimentacaocontabildebitocredito.getProjeto()!=null ? movimentacaocontabildebitocredito.getProjeto().getCdprojeto() : "";
				
				List<Movimentacaocontabildebitocredito> listaAux = mapa.get(key);
				if (listaAux==null){
					listaAux = new ArrayList<Movimentacaocontabildebitocredito>();
				}
				listaAux.add(movimentacaocontabildebitocredito);
				mapa.put(key, listaAux);
			}
			
			List<Movimentacaocontabildebitocredito> listaDebitoCreditoNova = new ArrayList<Movimentacaocontabildebitocredito>();
			
			for (String key: mapa.keySet()){
				List<Movimentacaocontabildebitocredito> listaAux = mapa.get(key);
				Money valor = new Money();
				for (Iterator<Movimentacaocontabildebitocredito> iterator = listaAux.iterator(); iterator.hasNext();) {
					Movimentacaocontabildebitocredito movimentacaocontabildebitocredito = iterator.next();
					valor = valor.add(movimentacaocontabildebitocredito.getValor());
					
					if (!iterator.hasNext()){
						Movimentacaocontabildebitocredito movimentacaocontabildebitocreditoNova = movimentacaocontabildebitocredito.getClone();
						movimentacaocontabildebitocreditoNova.setValor(valor);
						listaDebitoCreditoNova.add(movimentacaocontabildebitocreditoNova);
					}
				}
			}
			
			return listaDebitoCreditoNova;
		}else {
			return listaDebitoCredito;
		}		
	}
	
	private Money getValorOperacaoContabil(OperacaocontabildebitocreditoControleEnum controle, String formula, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor) {
		if(OperacaocontabildebitocreditoControleEnum.VALOR_CALCULADO.equals(controle) && StringUtils.isNotBlank(formula)){
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setMapVariaveisFormula(engine, mapaValor);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(SinedUtil.getFuncoesSinedJs() + formula.replaceAll("\\$s\\.", ""));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new Money(resultado != null && !resultado.isNaN() && !resultado.isInfinite() ? resultado : 0d);
		}else {
			return mapaValor.get(controle);
		}
	}
	
	public void setMapVariaveisFormula(ScriptEngine engine, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor) {
		Money valor;
		for(OperacaocontabildebitocreditoControleEnum value: Arrays.asList(OperacaocontabildebitocreditoControleEnum.values())){
			if(!value.equals(OperacaocontabildebitocreditoControleEnum.VALOR_CALCULADO)){
				valor = mapaValor.get(value);
				engine.put(value.name(), valor != null ? valor.getValue().doubleValue() : 0d);
			}
		}
	}
	
	private List<GerarLancamentoContabilCentrocustoProjetoBean> getlistaCentrocustoProjetoBean(Rateio rateio, boolean incluirValor){
		List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean = new ArrayList<GerarLancamentoContabilCentrocustoProjetoBean>();
		
		if (rateio!=null && rateio.getListaRateioitem()!=null && !rateio.getListaRateioitem().isEmpty()){
			for (Rateioitem rateioitem: rateio.getListaRateioitem()){
				if (incluirValor){
					listaCentrocustoProjetoBean.add(new GerarLancamentoContabilCentrocustoProjetoBean(rateioitem.getCentrocusto(), rateioitem.getProjeto(), rateioitem.getValor()));
				}else {
					listaCentrocustoProjetoBean.add(new GerarLancamentoContabilCentrocustoProjetoBean(rateioitem.getCentrocusto(), rateioitem.getProjeto(), rateioitem.getPercentual()));
				}
			}
		}else {
			listaCentrocustoProjetoBean.add(new GerarLancamentoContabilCentrocustoProjetoBean());
		}
		
		return listaCentrocustoProjetoBean;
	}
	
	private String getHistoricoItem(Operacaocontabildebitocredito operacaocontabildebitocredito, Map<String, String> mapaHistorico) {
		if (operacaocontabildebitocredito.getHistorico()!=null && mapaHistorico!=null){
			String historico = operacaocontabildebitocredito.getHistorico();
			
			for (Entry<String, String> entry: mapaHistorico.entrySet()){
				//If para quando for alterar o c�digo de integra��o da lista de cr�dito e d�bito
				if (entry.getKey().equals(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO)) {
					historico = historico.replace(entry.getKey(), Util.strings.emptyIfNull(operacaocontabildebitocredito.getCodigointegracao() != null ? 
							operacaocontabildebitocredito.getCodigointegracao().toString() : null));
				} else {
					historico = historico.replace(entry.getKey(), Util.strings.emptyIfNull(entry.getValue()));
				}
			}
			
			//Cada tipo de lan�amento possui suas vari�veis para substitui��o. Caso cadastre alguma que n�o seja pertencente ao seu tipo, eu coloco como vazio.
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_PAGAMENTOA, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CLIENTE, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_FORNECEDOR, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_PARCELA, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CONTAORIGEM, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CONTADESTINO, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CHEQUE, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_VALOR_UTILIZADO_ADIANTAMENTO, "");
			
			return historico;
		}
		
		return null;
	}
	
	private List<GerarLancamentoContabilCentrocustoProjetoBean> getlistaCentrocustoProjetoBean(Rateio rateio){
		return getlistaCentrocustoProjetoBean(rateio, false);
	}
	
	private Movimentacaocontabil getMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, Date dtlancamento, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum movimentacaocontabilTipo, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String numero, Money valor, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, Map<String, String> mapaHistorico, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, Rateioitem rateioitem) throws Exception {
		return getMovimentacaocontabil(filtro, dtlancamento, operacaocontabil, movimentacaocontabilTipo, movimentacaocontabilTipoLancamento, numero, valor, mapaContaContabil, mapaValor, mapaHistorico, listaCentrocustoProjetoBean, null, null, null, dtlancamento, rateioitem);	
	}
	
	private Movimentacaocontabil getMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, Date dtlancamento, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum movimentacaocontabilTipo, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String numero, Money valor, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, Map<String, String> mapaHistorico, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean) throws Exception {
		return getMovimentacaocontabil(filtro, dtlancamento, operacaocontabil, movimentacaocontabilTipo, movimentacaocontabilTipoLancamento, numero, valor, mapaContaContabil, mapaValor, mapaHistorico, listaCentrocustoProjetoBean, null, null, null);
	}
	
	private Movimentacaocontabil getMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, Date dtlancamento, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum movimentacaocontabilTipo, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String numero, Money valor, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, Map<String, String> mapaHistorico, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, List<Movimentacaocontabildebitocredito> listaCredito, List<Movimentacaocontabildebitocredito> listaDebito) throws Exception {
		return getMovimentacaocontabil(filtro, dtlancamento, operacaocontabil, movimentacaocontabilTipo, movimentacaocontabilTipoLancamento, numero, valor, mapaContaContabil, mapaValor, mapaHistorico, listaCentrocustoProjetoBean, listaCredito, listaDebito, null);
	}
	
	private Movimentacaocontabil getMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, Date dtlancamento, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum movimentacaocontabilTipo, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String numero, Money valor, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, Map<String, String> mapaHistorico, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, Date dtReferencia) throws Exception {
		return getMovimentacaocontabil(filtro, dtlancamento, operacaocontabil, movimentacaocontabilTipo, movimentacaocontabilTipoLancamento, numero, valor, mapaContaContabil, mapaValor, mapaHistorico, listaCentrocustoProjetoBean, null, null, null, dtReferencia);
	}
	
	private Movimentacaocontabil getMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, Date dtlancamento, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum movimentacaocontabilTipo, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String numero, Money valor, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, Map<String, String> mapaHistorico, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, OperacaocontabildebitocreditoTipoEnum tipo) throws Exception {
		return getMovimentacaocontabil(filtro, dtlancamento, operacaocontabil, movimentacaocontabilTipo, movimentacaocontabilTipoLancamento, numero, valor, mapaContaContabil, mapaValor, mapaHistorico, listaCentrocustoProjetoBean, null, null, tipo);
	}
	
	private Movimentacaocontabil getMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, Date dtlancamento, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum movimentacaocontabilTipo, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String numero, Money valor, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, Map<String, String> mapaHistorico, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, List<Movimentacaocontabildebitocredito> listaCredito, List<Movimentacaocontabildebitocredito> listaDebito, OperacaocontabildebitocreditoTipoEnum tipo, Date dtReferencia) throws Exception {
		return getMovimentacaocontabil(filtro, dtlancamento, operacaocontabil, movimentacaocontabilTipo, movimentacaocontabilTipoLancamento, numero, valor, mapaContaContabil, mapaValor, mapaHistorico, listaCentrocustoProjetoBean, listaCredito, listaDebito, tipo, dtReferencia, null);
	}
	
	private Movimentacaocontabil getMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, Date dtlancamento, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum movimentacaocontabilTipo, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String numero, Money valor, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, Map<String, String> mapaHistorico, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, List<Movimentacaocontabildebitocredito> listaCredito, List<Movimentacaocontabildebitocredito> listaDebito, OperacaocontabildebitocreditoTipoEnum tipo, Date dtReferencia, Rateioitem rateioitem) throws Exception {
		Movimentacaocontabil movimentacaocontabil = new Movimentacaocontabil(dtlancamento, filtro.getEmpresa(), operacaocontabil, movimentacaocontabilTipo, movimentacaocontabilTipoLancamento, numero, valor, operacaocontabil.getHistorico());
		movimentacaocontabil.setDtReferencia(dtReferencia);
		if (listaCentrocustoProjetoBean==null || listaCentrocustoProjetoBean.isEmpty()){
			addListaMovimentacaocontabildebitocredito(movimentacaocontabil, operacaocontabil, mapaContaContabil, mapaValor, filtro.getListaCentrocustoProjetoBean(), listaCredito, listaDebito, tipo, mapaHistorico, rateioitem);
		}else {
			addListaMovimentacaocontabildebitocredito(movimentacaocontabil, operacaocontabil, mapaContaContabil, mapaValor, listaCentrocustoProjetoBean, listaCredito, listaDebito, tipo, mapaHistorico, rateioitem);			
		}
		setValorMovimentacaocontabil(movimentacaocontabil);
		addHistorico(movimentacaocontabil, operacaocontabil, mapaHistorico);
		
		return movimentacaocontabil;	
	}
	
	private Movimentacaocontabil getMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, Date dtlancamento, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum movimentacaocontabilTipo, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, String numero, Money valor, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, Map<String, String> mapaHistorico, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, List<Movimentacaocontabildebitocredito> listaCredito, List<Movimentacaocontabildebitocredito> listaDebito, OperacaocontabildebitocreditoTipoEnum tipo) throws Exception {
		return getMovimentacaocontabil(filtro, dtlancamento, operacaocontabil, movimentacaocontabilTipo, movimentacaocontabilTipoLancamento, numero, valor, mapaContaContabil, mapaValor, mapaHistorico, listaCentrocustoProjetoBean, listaCredito, listaDebito, tipo, dtlancamento);		
	}
	
	private void setValorMovimentacaocontabil(Movimentacaocontabil movimentacaocontabil) {
		if(movimentacaocontabil != null){
			Money valor = new Money();
			if(SinedUtil.isListNotEmpty(movimentacaocontabil.getListaCredito())){
				for(Movimentacaocontabildebitocredito item : movimentacaocontabil.getListaCredito()){
					if(item.getValor() != null){
						valor = valor.add(item.getValor());
					}
				}
			}
			if(valor.getValue().doubleValue() == 0 && SinedUtil.isListNotEmpty(movimentacaocontabil.getListaDebito())){
				for(Movimentacaocontabildebitocredito item : movimentacaocontabil.getListaDebito()){
					if(item.getValor() != null){
						valor = valor.add(item.getValor());
					}
				}
			}
			movimentacaocontabil.setValor(valor);
		}
	}
	
	private void addListaMovimentacaocontabildebitocredito(Movimentacaocontabil movimentacaocontabil, Operacaocontabil operacaocontabil, Map<OperacaocontabildebitocreditoTipoEnum, ContaContabil> mapaContaContabil, Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor, List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean, List<Movimentacaocontabildebitocredito> listaCredito, List<Movimentacaocontabildebitocredito> listaDebito, OperacaocontabildebitocreditoTipoEnum tipo, Map<String, String> mapaHistorico, Rateioitem rateioitem) throws Exception {
		movimentacaocontabil.setListaDebito(getListaDebitoCredito(operacaocontabil.getListaDebito(), mapaContaContabil, mapaValor, listaCentrocustoProjetoBean, listaDebito, operacaocontabil.getAgruparvalormesmaconta(), tipo, mapaHistorico, rateioitem, movimentacaocontabil.getTipoLancamento()));
		movimentacaocontabil.setListaCredito(getListaDebitoCredito(operacaocontabil.getListaCredito(), mapaContaContabil, mapaValor, listaCentrocustoProjetoBean, listaCredito, operacaocontabil.getAgruparvalormesmaconta(), tipo, mapaHistorico, rateioitem, movimentacaocontabil.getTipoLancamento()));
	}
	
	private void addHistorico(Movimentacaocontabil movimentacaocontabil, Operacaocontabil operacaocontabil, Map<String, String> mapaHistorico) throws Exception {
		if (operacaocontabil.getHistorico()!=null && mapaHistorico!=null){
			String historico = operacaocontabil.getHistorico();
			
			for (Entry<String, String> entry: mapaHistorico.entrySet()){
				historico = historico.replace(entry.getKey(), Util.strings.emptyIfNull(entry.getValue()));
			}
			
			//Cada tipo de lan�amento possui suas vari�veis para substitui��o. Caso cadastre alguma que n�o seja pertencente ao seu tipo, eu coloco como vazio.
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_PAGAMENTOA, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CLIENTE, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_FORNECEDOR, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_PARCELA, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CONTAORIGEM, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CONTADESTINO, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CHEQUE, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_NUMERO_NOTA, "");
			historico = historico.replace(OperacaocontabilService.VARIAVEL_HISTORICO_VALOR_UTILIZADO_ADIANTAMENTO, "");
			
			movimentacaocontabil.setHistorico(historico);
		}
	}
	
	private void addListaMovimentacaocontabil(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil) throws Exception {
		List<Documento> listaDocumentoWhereNotIn = new ArrayList<Documento>();
		List<Movimentacao> listaMovimentacaoWhereNotIn = new ArrayList<Movimentacao>();
		List<Movimentacaoestoque> listaMovimentacaoEstoqueWhereNotIn = new ArrayList<Movimentacaoestoque>();
		List<Rateioitem> listaRateioItemWhereNotIn = new ArrayList<Rateioitem>();
		HashMap<OperacaoContabilTipoLancamento, StringBuilder> mapWhereNotInNF = new HashMap<OperacaoContabilTipoLancamento, StringBuilder>();
		
		String whereIn = SinedUtil.listAndConcatenate(filtro.getListaTipoLancamento(), "cdOperacaoContabilTipoLancamento", ",");
		List<OperacaoContabilTipoLancamento> listaOperacaoContabilTipoLancamento = operacaoContabilTipoLancamentoService.buscarOrdenados(whereIn);
		
		if(SinedUtil.isListNotEmpty(listaOperacaoContabilTipoLancamento)) {
			boolean movimentacoesContabeisContaReceberGeradas = false;
			
			for(OperacaoContabilTipoLancamento op : listaOperacaoContabilTipoLancamento) {
				if(op.equals(OperacaoContabilTipoLancamento.PROVISIONAMENTO)){
					addListaMovimentacaocontabilProvisionamento(filtro, listaMovimentacaocontabil, mapWhereNotInNF, op);
				} else if(op.equals(OperacaoContabilTipoLancamento.DESPESA_COLABORADOR)){
					addListaMovimentacaocontabilColaboradorDespesa(filtro, listaMovimentacaocontabil, mapWhereNotInNF, op);
				} else if(op.equals(OperacaoContabilTipoLancamento.EMISSAO_NF) || op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_ISS_CLIENTE)
                		|| op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_ISS)
                		|| op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_IR)
                        || op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_CSLL)
                        || op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_INSS)
                        || op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_PIS)
                        || op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_COFINS)
                        || op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_ICMS)
                        || op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_II)
                        || op.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_IPI)){
					addListaMovimentacaocontabilNF(filtro, listaMovimentacaocontabil, mapWhereNotInNF, op);
				} else if(op.equals(OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL)) {
					addListaMovimentacaocontabilEntradaFiscal(filtro, listaMovimentacaocontabil, op);
				} else if(op.equals(OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO)) {
					addListaMovimentacaocontabilEntradaFiscalComApropriacao(filtro, listaMovimentacaocontabil, op);
				} else if(op.equals(OperacaoContabilTipoLancamento.CREDITO_GERADO_RETORNO_BANCARIO)) {
					addListaMovimentacaocontabilRetornoBancario(filtro, listaMovimentacaocontabil, listaMovimentacaoWhereNotIn);
				} else if(op.equals(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO)
						|| op.equals(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER)
						|| op.equals(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE)) {
					addListaMovimentacaocontabilContaGerencial(filtro, listaMovimentacaocontabil, listaMovimentacaoWhereNotIn, listaDocumentoWhereNotIn, listaRateioItemWhereNotIn, listaMovimentacaoEstoqueWhereNotIn);
				} else if(op.equals(OperacaoContabilTipoLancamento.VENDA)) {
					addListaMovimentacaocontabilVenda(filtro, listaMovimentacaocontabil);
				} else if(op.equals(OperacaoContabilTipoLancamento.MOVIMENTACAO_ESTOQUE_ORIGEM)) {
					addListaMovimentacaocontabilEstoqueOrigem(filtro, listaMovimentacaocontabil, listaMovimentacaoWhereNotIn, listaRateioItemWhereNotIn, listaMovimentacaoEstoqueWhereNotIn);
				} else if(op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO)) {
					addListaMovimentacaocontabilContaPagarReceberPorFormaPagamento(filtro, listaMovimentacaocontabil, listaDocumentoWhereNotIn);
				} else if(op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_PARCIAL) 
							|| op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_DINHEIRO)
							|| op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CHEQUE)
							|| op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CARTAO)
							|| op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO)
							|| op.equals(OperacaoContabilTipoLancamento.CONTA_PAGAR_BAIXADA_FORNECEDOR)
							|| op.equals(OperacaoContabilTipoLancamento.JUROS_RECEBIDOS)
							|| op.equals(OperacaoContabilTipoLancamento.DESCONTOS_CONCEDIDOS)
							|| op.equals(OperacaoContabilTipoLancamento.JUROS_PAGOS)
							|| op.equals(OperacaoContabilTipoLancamento.DESCONTOS_OBTIDOS)
							|| op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE)
							|| op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO)) {
					addListaMovimentacaocontabilContaPagarReceber(filtro, listaMovimentacaocontabil, listaDocumentoWhereNotIn);
				} else if(op.equals(OperacaoContabilTipoLancamento.DEPOSITO)
							|| op.equals(OperacaoContabilTipoLancamento.DEPOSITO_CHEQUE)
							|| op.equals(OperacaoContabilTipoLancamento.SAQUE)
							|| op.equals(OperacaoContabilTipoLancamento.SAQUE_CHEQUE)
							|| op.equals(OperacaoContabilTipoLancamento.TRANSFERENCIA_ENTRE_CONTAS)
							|| op.equals(OperacaoContabilTipoLancamento.TRANSFERENCIA_ENTRE_CAIXAS)) {
					addListaMovimentacaocontabilTransferencia(filtro, listaMovimentacaocontabil, listaMovimentacaoWhereNotIn);
				} else if(op.equals(OperacaoContabilTipoLancamento.CONCILIACAO_CARTAO)) { 
					if(!movimentacoesContabeisContaReceberGeradas) {
						addListaMovimentacaocontabilContaPagarReceber(filtro, listaMovimentacaocontabil, listaDocumentoWhereNotIn);
						movimentacoesContabeisContaReceberGeradas = true;
					} else {
						addListaMovimentacaocontabilTransferencia(filtro, listaMovimentacaocontabil, listaMovimentacaoWhereNotIn);
					}
				} else if(op.equals(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_ADIANTAMENTO)
							|| op.equals(OperacaoContabilTipoLancamento.CONTA_PAGAR_BAIXADA_ADIANTAMENTO)) {
					addListaMovimentacaocontabilContaPagarReceberAdiantamento(filtro, listaMovimentacaocontabil);
				}
			}
		}
	}
	
	private void addListaMovimentacaocontabilNF(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, HashMap<OperacaoContabilTipoLancamento, StringBuilder> map, OperacaoContabilTipoLancamento operacaoContabilTipoLancamento) throws Exception {
		if (operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.EMISSAO_NF) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_COFINS) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_CSLL) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_ICMS) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_II) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_INSS) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_IPI) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_IR) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_ISS) 
				|| operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_PIS)){
			addListaMovimentacaocontabilNFS(filtro, listaMovimentacaocontabil, operacaoContabilTipoLancamento.getMovimentacaocontabilTipoLancamento(), map);
			addListaMovimentacaocontabilNFP(filtro, listaMovimentacaocontabil, operacaoContabilTipoLancamento.getMovimentacaocontabilTipoLancamento(), map);
		}
		if (operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.RECOLHIMENTO_ISS_CLIENTE)){
			addListaMovimentacaocontabilNFS(filtro, listaMovimentacaocontabil, operacaoContabilTipoLancamento.getMovimentacaocontabilTipoLancamento(), map);
			addListaMovimentacaocontabilNFP(filtro, listaMovimentacaocontabil, operacaoContabilTipoLancamento.getMovimentacaocontabilTipoLancamento(), map);
		}
	}
	
	private void addListaMovimentacaocontabilProvisionamento(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, HashMap<OperacaoContabilTipoLancamento, StringBuilder> map, OperacaoContabilTipoLancamento operacaoContabilTipoLancamento) throws Exception {
		if (operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.PROVISIONAMENTO)){
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(operacaoContabilTipoLancamento);
			if(operacaocontabil == null || operacaocontabil.isEmpty()){
				return;
			}
			List<Provisao> listaProvisao = provisaoService.findForGerarLancamentoContabil(filtro);
			if(SinedUtil.isListNotEmpty(listaProvisao)){
				for(Operacaocontabil op : operacaocontabil){
					for (Provisao provisao: listaProvisao){
						Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, provisao.getDtProvisao(), op, op.getTipo().getMovimentacaocontabilTipo(), operacaoContabilTipoLancamento.getMovimentacaocontabilTipoLancamento(), null, provisao.getValor(), getMapaContaContabil(provisao), getMapaValor(provisao), getMapaHistorico(provisao, op), null);
						
						movimentacaocontabil.addOrigem(provisao);
						listaMovimentacaocontabil.add(movimentacaocontabil);
					}
				}
			}
		}
	}
	
	private void addListaMovimentacaocontabilColaboradorDespesa(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, HashMap<OperacaoContabilTipoLancamento, StringBuilder> map, OperacaoContabilTipoLancamento operacaoContabilTipoLancamento) throws Exception {
		if (operacaoContabilTipoLancamento.equals(OperacaoContabilTipoLancamento.DESPESA_COLABORADOR)){
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(operacaoContabilTipoLancamento);
			if(operacaocontabil == null || operacaocontabil.isEmpty()){
				return;
			}
			boolean considerarEvento = false;
			for(Operacaocontabil op : operacaocontabil){
				for(Operacaocontabildebitocredito operacaoContabilCredito : op.getListaCredito()){
					if(OperacaocontabildebitocreditoControleEnum.VALOR_EVENTO_DESPESA_COLABORADOR.equals(operacaoContabilCredito.getControle())){
						considerarEvento = true;
						break;
					}
				}
				List<Movimentacaocontabildebitocredito> listaCredito = null;
				List<Movimentacaocontabildebitocredito> listaDebito = null;
				String whereInEventos = null;
				if(considerarEvento){
					List<OperacaoContabilEventoPagamento> listaRelacaoOperacaoEvento = operacaoContabilEventoPagamentoService.findByOperacaoContabil(op);
					List<EventoPagamento> listaEvento = new ArrayList<EventoPagamento>();
					for(OperacaoContabilEventoPagamento relacao: listaRelacaoOperacaoEvento){
						listaEvento.add(relacao.getEvento());
					}
					whereInEventos = CollectionsUtil.listAndConcatenate(listaEvento, "cdEventoPagamento", ",");
				}
				List<Colaboradordespesa> listaColaboradordespesa = colaboradordespesaService.findForGerarLancamentoContabil(filtro, considerarEvento, whereInEventos);
				for(Colaboradordespesa colaboradorDespesa: listaColaboradordespesa){
					listaCredito = new ArrayList<Movimentacaocontabildebitocredito>();
					listaDebito = new ArrayList<Movimentacaocontabildebitocredito>();
					Movimentacaocontabil movimentacaocontabil = null;
					if(considerarEvento){
						
						for(Colaboradordespesaitem item: colaboradorDespesa.getListaColaboradordespesaitem()){
							Movimentacaocontabil movimentacaocontabilItem = getMovimentacaocontabil(filtro, colaboradorDespesa.getDtinsercao(), op, op.getTipo().getMovimentacaocontabilTipo(), operacaoContabilTipoLancamento.getMovimentacaocontabilTipoLancamento(), null, colaboradorDespesa.getValor(), getMapaContaContabil(colaboradorDespesa, item), getMapaValor(colaboradorDespesa, item), getMapaHistorico(colaboradorDespesa, item, op), null);
							
							if(movimentacaocontabilItem != null){
								movimentacaocontabilItem.addOrigem(item, item.getEvento());
								if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaCredito())){
									listaCredito.addAll(movimentacaocontabilItem.getListaCredito());
								}
								if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaDebito())){
									listaDebito.addAll(movimentacaocontabilItem.getListaDebito());
								}
							}
						}
					}
					movimentacaocontabil = getMovimentacaocontabil(filtro, colaboradorDespesa.getDtinsercao(), op, op.getTipo().getMovimentacaocontabilTipo(), operacaoContabilTipoLancamento.getMovimentacaocontabilTipoLancamento(), null, colaboradorDespesa.getValor(), getMapaContaContabil(colaboradorDespesa), getMapaValor(colaboradorDespesa), getMapaHistorico(colaboradorDespesa, null, op), null, listaCredito, listaDebito);	
						
					movimentacaocontabil.addOrigem(colaboradorDespesa);
					listaMovimentacaocontabil.add(movimentacaocontabil);
				}
			}
		}
	}
	
	private void addListaMovimentacaocontabilNFS(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, HashMap<OperacaoContabilTipoLancamento, StringBuilder> map) throws Exception {
		if(map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()) == null){
			map.put(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento(), new StringBuilder());
		}
		List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento());
		if (operacaocontabil==null || operacaocontabil.isEmpty()){
			return;
		}
		for(Operacaocontabil op : operacaocontabil){
			String whereInNaturezaOperacao = op.getWhereInNaturezaOperacao();
			if(StringUtils.isBlank(whereInNaturezaOperacao)){
				continue;
			}
			List<NotaFiscalServico> listaNFS = notaFiscalServicoService.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento, map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()).toString(), whereInNaturezaOperacao);
			
			for (NotaFiscalServico nfs: listaNFS){
				Movimentacaocontabil movimentacaocontabil = null;
				
				
				if (MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamento) 
						|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS.equals(movimentacaocontabilTipoLancamento) 
		                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IR.equals(movimentacaocontabilTipoLancamento)
		                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_CSLL.equals(movimentacaocontabilTipoLancamento)
		                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_INSS.equals(movimentacaocontabilTipoLancamento)
		                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_PIS.equals(movimentacaocontabilTipoLancamento)
		                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_COFINS.equals(movimentacaocontabilTipoLancamento)
		                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ICMS.equals(movimentacaocontabilTipoLancamento)
		                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ICMS.equals(movimentacaocontabilTipoLancamento)
		                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_II.equals(movimentacaocontabilTipoLancamento)){
					movimentacaocontabil = getMovimentacaocontabil(filtro, nfs.getDtEmissao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, nfs.getNumero(), nfs.getValorNota(), getMapaContaContabil(nfs), getMapaValor(nfs, null), getMapaHistorico(nfs, op), null);
				}else if (MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamento)){
					movimentacaocontabil = getMovimentacaocontabil(filtro, nfs.getDtEmissao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, nfs.getNumero(), SinedUtil.zeroIfNull(nfs.getValoriss()), getMapaContaContabil(nfs), getMapaValor(nfs, movimentacaocontabilTipoLancamento), getMapaHistorico(nfs, op), null);
				}else {
					continue;
				}
				
				if (movimentacaocontabil.getListaDebito()==null 
						|| movimentacaocontabil.getListaDebito().isEmpty()
						|| movimentacaocontabil.getListaCredito()==null 
						|| movimentacaocontabil.getListaCredito().isEmpty()){
					continue;
				}
				
				movimentacaocontabil.addOrigem(nfs);
				if(map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()).length() > 0) map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()).append(",");
				map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()).append(nfs.getCdNota());
				listaMovimentacaocontabil.add(movimentacaocontabil);
			}
		}
	}
	
	private void addListaMovimentacaocontabilNFP(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento, HashMap<OperacaoContabilTipoLancamento, StringBuilder> map) throws Exception {
		if(map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()) == null){
			map.put(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento(), new StringBuilder());
		}
		List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento());
		
		if(operacaocontabil == null) return;
		for(Operacaocontabil op : operacaocontabil){
			String whereInNaturezaOperacao = op.getWhereInNaturezaOperacao();
			if(StringUtils.isBlank(whereInNaturezaOperacao)){
				continue;
			}
			List<Notafiscalproduto> listaNFP = notafiscalprodutoService.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento, map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()).toString(), whereInNaturezaOperacao);
			
			for (Notafiscalproduto nfp: listaNFP){
				Movimentacaocontabil movimentacaocontabil = null;
				
				List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean = new ArrayList<GerarLancamentoContabilCentrocustoProjetoBean>();
				
				if (nfp.getListaItens()!=null){
					for (Notafiscalprodutoitem notafiscalprodutoitem: nfp.getListaItens()){
						if (op.isPossuiCentroCustoDebitoCredito() 
								&& notafiscalprodutoitem.getMaterial()!=null 
								&& notafiscalprodutoitem.getMaterial().getCentrocustovenda()!=null){
							listaCentrocustoProjetoBean.add(new GerarLancamentoContabilCentrocustoProjetoBean(notafiscalprodutoitem.getMaterial().getCentrocustovenda(), null, notafiscalprodutoitem.getValorbruto()));
						}
					}
				}
				
				List<Movimentacaocontabildebitocredito> listaCredito = new ArrayList<Movimentacaocontabildebitocredito>();
				List<Movimentacaocontabildebitocredito> listaDebito = new ArrayList<Movimentacaocontabildebitocredito>();
				
				if(SinedUtil.isListNotEmpty(nfp.getListaItens())){
					for(Notafiscalprodutoitem item : nfp.getListaItens()){
						Movimentacaocontabil movimentacaocontabilItem = getMovimentacaocontabil(filtro, nfp.getDtEmissao(), op, op.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL, nfp.getNumero(), item.getValorbruto(), getMapaContaContabil(item, filtro.getEmpresa()), getMapaValor(item, movimentacaocontabilTipoLancamento), getMapaHistorico(nfp, op), listaCentrocustoProjetoBean, OperacaocontabildebitocreditoTipoEnum.MATERIAL);
						if(movimentacaocontabilItem != null){
							if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaCredito())){
								listaCredito.addAll(movimentacaocontabilItem.getListaCredito());
							}
							if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaDebito())){
								listaDebito.addAll(movimentacaocontabilItem.getListaDebito());
							}
						}
					}
				}
				
				if (MovimentacaocontabilTipoLancamentoEnum.EMISSAO_NF.equals(movimentacaocontabilTipoLancamento) 
					|| MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS.equals(movimentacaocontabilTipoLancamento) 
	                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IR.equals(movimentacaocontabilTipoLancamento)
	                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_CSLL.equals(movimentacaocontabilTipoLancamento)
	                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_INSS.equals(movimentacaocontabilTipoLancamento)
	                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_PIS.equals(movimentacaocontabilTipoLancamento)
	                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_COFINS.equals(movimentacaocontabilTipoLancamento)
	                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_IPI.equals(movimentacaocontabilTipoLancamento)
	                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ICMS.equals(movimentacaocontabilTipoLancamento)
	                || MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_II.equals(movimentacaocontabilTipoLancamento)){
					movimentacaocontabil = getMovimentacaocontabil(filtro, nfp.getDtEmissao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, nfp.getNumero(), nfp.getValor(), getMapaContaContabil(nfp), getMapaValor(nfp, movimentacaocontabilTipoLancamento), getMapaHistorico(nfp, op), listaCentrocustoProjetoBean, listaCredito, listaDebito, null);
				}else if (MovimentacaocontabilTipoLancamentoEnum.RECOLHIMENTO_ISS_CLIENTE.equals(movimentacaocontabilTipoLancamento)){
					movimentacaocontabil = getMovimentacaocontabil(filtro, nfp.getDtEmissao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, nfp.getNumero(), SinedUtil.zeroIfNull(nfp.getValoriss()), getMapaContaContabil(nfp), getMapaValor(nfp, movimentacaocontabilTipoLancamento), getMapaHistorico(nfp, op), listaCentrocustoProjetoBean, listaCredito, listaDebito, null);
				}
				
				if (movimentacaocontabil.getListaDebito()==null 
						|| movimentacaocontabil.getListaDebito().isEmpty()
						|| movimentacaocontabil.getListaCredito()==null 
						|| movimentacaocontabil.getListaCredito().isEmpty()){
					continue;
				}
				
				movimentacaocontabil.addOrigem(nfp);
				if(map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()).length() > 0) map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()).append(",");
				map.get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento()).append(nfp.getCdNota());
				listaMovimentacaocontabil.add(movimentacaocontabil);
			}
		}
	}
	
	private void addListaMovimentacaocontabilEntradaFiscal(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, OperacaoContabilTipoLancamento op) throws Exception {
		if (filtro.isTipoLancamento(OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL)){
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(op);
			
			if(operacaocontabil == null) return;
			List<Integer> listaEntregaDocumentoWhereNotIn = new ArrayList<Integer>();
			for(Operacaocontabil opco : operacaocontabil){
				String whereInNaturezaOperacao = opco.getWhereInNaturezaOperacao();
				if(StringUtils.isBlank(whereInNaturezaOperacao)){
					continue;
				}
				List<Entregadocumento> listaEntregadocumento = entregadocumentoService.findForGerarLancamentoContabil(filtro, whereInNaturezaOperacao);
				for (Entregadocumento entregadocumento: listaEntregadocumento){
					if (listaEntregaDocumentoWhereNotIn.contains(entregadocumento.getCdentregadocumento())){
						continue;
					}
					List<Movimentacaocontabildebitocredito> listaCredito = new ArrayList<Movimentacaocontabildebitocredito>();
					List<Movimentacaocontabildebitocredito> listaDebito = new ArrayList<Movimentacaocontabildebitocredito>();
	
					if(SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())){
						for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
							Double valortotaloperacao = entregamaterial.getValorTotalItem();
							if(valortotaloperacao != null){
								Movimentacaocontabil movimentacaocontabilItem = getMovimentacaocontabil(filtro, entregadocumento.getDtentrada(), opco, opco.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL, entregadocumento.getNumero(), new Money(valortotaloperacao), getMapaContaContabil(entregamaterial, filtro.getEmpresa()), getMapaValor(entregamaterial), getMapaHistorico(entregadocumento, opco), getlistaCentrocustoProjetoBean(entregadocumento.getRateio()), OperacaocontabildebitocreditoTipoEnum.MATERIAL);
								if(movimentacaocontabilItem != null){
									if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaCredito())){
										listaCredito.addAll(movimentacaocontabilItem.getListaCredito());
									}
									if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaDebito())){
										listaDebito.addAll(movimentacaocontabilItem.getListaDebito());
									}
								}
							}
						}
					}
					
					Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, entregadocumento.getDataForGeracaoContabil(), opco, opco.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL, entregadocumento.getNumero(), entregadocumento.getValor(), getMapaContaContabil(entregadocumento), getMapaValor(entregadocumento), getMapaHistorico(entregadocumento, opco), getlistaCentrocustoProjetoBean(entregadocumento.getRateio()), listaCredito, listaDebito, null);
					movimentacaocontabil.addOrigem(entregadocumento);
					listaMovimentacaocontabil.add(movimentacaocontabil);
					
					if (!this.movimentacaoContabilInvalida(movimentacaocontabil)){
						listaEntregaDocumentoWhereNotIn.add(entregadocumento.getCdentregadocumento());
					}
					
					System.out.println("Fim Cdentregadocumento: " + entregadocumento.getCdentregadocumento() + " - operacao: " + opco.getCdoperacaocontabil());
				}
			}
		}
	}
	
	private boolean movimentacaoContabilInvalida(Movimentacaocontabil movimentacaocontabil){
		return (movimentacaocontabil.getListaDebito()==null 
				|| movimentacaocontabil.getListaDebito().isEmpty()
				|| movimentacaocontabil.getListaCredito()==null 
				|| movimentacaocontabil.getListaCredito().isEmpty());
	}
	
	private void addListaMovimentacaocontabilEntradaFiscalComApropriacao(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, OperacaoContabilTipoLancamento op) throws Exception {
		if (filtro.isTipoLancamento(OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO)){
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(op);
			
			if(operacaocontabil == null || operacaocontabil.isEmpty()){
				return;
			}	
			List<Integer> listaEntregaDocumentoWhereNotIn = new ArrayList<Integer>();
			for(Operacaocontabil opco : operacaocontabil){
				String whereInNaturezaOperacao = opco.getWhereInNaturezaOperacao();
				if(StringUtils.isBlank(whereInNaturezaOperacao)){
					continue;
				}
				List<Entregadocumento> listaEntregadocumento = entregadocumentoService.findForGerarLancamentoContabilWithApropriacao(filtro, whereInNaturezaOperacao);
				
				for (Entregadocumento entregadocumento: listaEntregadocumento){
					
					List<Movimentacaocontabildebitocredito> listaCredito = new ArrayList<Movimentacaocontabildebitocredito>();
					List<Movimentacaocontabildebitocredito> listaDebito = new ArrayList<Movimentacaocontabildebitocredito>();
					
					List<EntregaDocumentoApropriacao> listaApropriacao = entregaDocumentoApropriacaoService.findByEntregaDocumento(entregadocumento);
					
					if(SinedUtil.isListNotEmpty(listaApropriacao)){
						Double valortotalitensoperacao = 0.0; 
						for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
							if(entregamaterial.getValorTotalItem() != null){
								valortotalitensoperacao += entregamaterial.getValorTotalItem();
							}
						}
						Double indiceProporcional = valortotalitensoperacao / entregadocumento.getValor().getValue().doubleValue();
						for(EntregaDocumentoApropriacao apropriacao: listaApropriacao){
							listaCredito = new ArrayList<Movimentacaocontabildebitocredito>();
							listaDebito = new ArrayList<Movimentacaocontabildebitocredito>();
							EntregaDocumentoApropriacao apropriacaoAux = new EntregaDocumentoApropriacao();
							if(SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())){
								
								apropriacaoAux.setValor(new Money(apropriacao.getValor().getValue().doubleValue() * indiceProporcional));
								//Double indiceProporcionalItem = apropriacaoAux.getValor().getValue().doubleValue()/valortotalitensoperacao;
								
								for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
									Double indiceProporcionalItem = entregamaterial.getValorTotalItem()/valortotalitensoperacao;
									//Double valorProporcionalItem = (entregamaterial.getValorTotalItem() * indiceProporcional);
									Double valortotaloperacao = apropriacaoAux.getValor().getValue().doubleValue() * indiceProporcionalItem;//valorProporcionalItem  * indiceProporcionalItem;
									if(valortotaloperacao != null){
										entregamaterial.setValorProporcionalApropriacao(valortotaloperacao);
										Movimentacaocontabil movimentacaocontabilItem = getMovimentacaocontabil(filtro, entregadocumento.getDtentrada(), opco, opco.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO, entregadocumento.getNumero(), new Money(valortotaloperacao), getMapaContaContabil(entregamaterial, filtro.getEmpresa()), getMapaValor(entregamaterial, apropriacaoAux), getMapaHistorico(entregadocumento, opco), getlistaCentrocustoProjetoBean(entregadocumento.getRateio()), OperacaocontabildebitocreditoTipoEnum.MATERIAL);
										if(movimentacaocontabilItem != null){
											if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaCredito())){
												listaCredito.addAll(movimentacaocontabilItem.getListaCredito());
											}
											if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaDebito())){
												listaDebito.addAll(movimentacaocontabilItem.getListaDebito());
											}
										}
									}
								}
							}
							
							
							Date dtMovimentacao = SinedDateUtils.addDiasData(apropriacao.getMesAno(), SinedDateUtils.getDia(entregadocumento.getDataForGeracaoContabil())-1);
							if(SinedDateUtils.getDia(dtMovimentacao) != SinedDateUtils.getDia(entregadocumento.getDataForGeracaoContabil())){
								dtMovimentacao = SinedDateUtils.lastDateOfMonth(apropriacao.getMesAno());
							}
							
							Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, dtMovimentacao, opco, opco.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO, entregadocumento.getNumero(), apropriacao.getValor(), getMapaContaContabil(entregadocumento), getMapaValor(entregadocumento, apropriacaoAux), getMapaHistorico(entregadocumento, opco), getlistaCentrocustoProjetoBean(entregadocumento.getRateio()), listaCredito, listaDebito, null, entregadocumento.getDataForGeracaoContabil());
							movimentacaocontabil.addOrigem(entregadocumento);
							listaMovimentacaocontabil.add(movimentacaocontabil);
							if (!this.movimentacaoContabilInvalida(movimentacaocontabil)){
								listaEntregaDocumentoWhereNotIn.add(entregadocumento.getCdentregadocumento());
							}
						}
					}
					System.out.println("Fim Cdentregadocumento: " + entregadocumento.getCdentregadocumento() + " - operacao: " + opco.getCdoperacaocontabil());
				}
			}
		}
	}

	
	private void addListaMovimentacaocontabilContaPagarReceber(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, List<Documento> listaDocumentoWhereNotIn) throws Exception {
		List<MovimentacaocontabilTipoLancamentoEnum> listaTipoLancamento = Arrays.asList(new MovimentacaocontabilTipoLancamentoEnum[]{MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_DINHEIRO, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CHEQUE,
																			MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CARTAO, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO,
																			MovimentacaocontabilTipoLancamentoEnum.CONTA_PAGAR_BAIXADA_FORNECEDOR, MovimentacaocontabilTipoLancamentoEnum.JUROS_RECEBIDOS, MovimentacaocontabilTipoLancamentoEnum.DESCONTOS_CONCEDIDOS,
																			MovimentacaocontabilTipoLancamentoEnum.JUROS_PAGOS, MovimentacaocontabilTipoLancamentoEnum.DESCONTOS_OBTIDOS, MovimentacaocontabilTipoLancamentoEnum.CONCILIACAO_CARTAO,
																			MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE, MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_PARCIAL});
		
		for (MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento: listaTipoLancamento){
			if (!filtro.isTipoLancamento(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento())){
				continue;
			}
			
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento());
			
			if (operacaocontabil==null || operacaocontabil.isEmpty()){
				continue;
			}
			
			List<Documento> listaDocumento = documentoService.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento);
			
			for (Documento documento: listaDocumento){
				if (listaDocumentoWhereNotIn.contains(documento)){
					continue;
				}
				
				Money valor = new Money();
				
				switch (movimentacaocontabilTipoLancamento) {
					case CONTA_RECEBER_BAIXADA_DINHEIRO:
						valor = documento.getAux_documento().getValoratual();
						break;
					case CONTA_RECEBER_BAIXADA_CHEQUE:
						valor = documento.getAux_documento().getValoratual();
						break;
					case CONTA_RECEBER_BAIXADA_CARTAO:
						valor = documento.getAux_documento().getValoratual();
						break;
					case CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO:
						valor = documento.getAux_documento().getValoratual();
						break;
					case CONTA_PAGAR_BAIXADA_FORNECEDOR:
						valor = documento.getAux_documento().getValoratual();
						break;
					case JUROS_RECEBIDOS:
						valor = SinedUtil.zeroIfNull(documento.getAux_documento().getValorjurosmes());
						break;
					case DESCONTOS_CONCEDIDOS:
						valor = SinedUtil.zeroIfNull(documento.getAux_documento().getValordesconto());
						break;
					case JUROS_PAGOS:
						valor = SinedUtil.zeroIfNull(documento.getAux_documento().getValorjurosOuJurosMes());
						break;
					case DESCONTOS_OBTIDOS:
						valor = SinedUtil.zeroIfNull(documento.getAux_documento().getValordesconto());
						break;	
					case CONCILIACAO_CARTAO:
						valor = documento.getAux_documento().getValoratual();
						break;
					case CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE:
						valor = documento.getAux_documento().getValoratual();
						break;
					case CONTA_RECEBER_BAIXADA_PARCIAL:
						valor = documento.getAux_documento().getValoratual();
						break;
					default:
						break;
				}
				
				
				for(Operacaocontabil op : operacaocontabil){
					if(MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_PARCIAL.equals(movimentacaocontabilTipoLancamento) && SinedUtil.isListNotEmpty(documento.getListaMovimentacaoOrigem())){
						for(Movimentacaoorigem mo : documento.getListaMovimentacaoOrigem()){
							Movimentacao movimentacao = mo.getMovimentacao();
							
							if(!Movimentacaoacao.CANCELADA.equals(movimentacao.getMovimentacaoacao()) && !movimentacaocontabilorigemService.existeLancamentoContabilMovimentacao(movimentacao, movimentacaocontabilTipoLancamento)){
								// Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, documento.getNumero(), movimentacao.getValor(), getMapaContaContabil(documento), getMapaValor(movimentacao), getMapaHistorico(documento, operacaocontabil), getlistaCentrocustoProjetoBean(movimentacao.getRateio()));
								Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, documento.getNumero(), movimentacao.getValor(), getMapaContaContabil(documento), getMapaValor(movimentacao), getMapaHistorico(documento, op), getlistaCentrocustoProjetoBean(movimentacao.getRateio(), true));
								
								movimentacaocontabil.addOrigem(movimentacao);
								listaMovimentacaocontabil.add(movimentacaocontabil);
							}
						}
					}else {
						Movimentacao movimentacao = new ListSet<Movimentacaoorigem>(Movimentacaoorigem.class, documento.getListaMovimentacaoOrigem()).get(0).getMovimentacao();
						
						// Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, documento.getNumero(), valor, getMapaContaContabil(documento), getMapaValor(documento, movimentacao), getMapaHistorico(documento, operacaocontabil), getlistaCentrocustoProjetoBean(movimentacao.getRateio()));
						Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, documento.getNumero(), valor, getMapaContaContabil(documento), getMapaValor(documento, movimentacao, movimentacao.getValor()), getMapaHistorico(documento, op), getlistaCentrocustoProjetoBean(movimentacao.getRateio(), true));
	
						movimentacaocontabil.addOrigem(documento);
						listaMovimentacaocontabil.add(movimentacaocontabil);
					}
					listaDocumentoWhereNotIn.add(documento);
				}
			}
		}
	}
	
	private void addListaMovimentacaocontabilContaPagarReceberPorFormaPagamento(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, List<Documento> listaDocumentoWhereNotIn) throws Exception {
		List<MovimentacaocontabilTipoLancamentoEnum> listaTipoLancamento = Arrays.asList(new MovimentacaocontabilTipoLancamentoEnum[]{MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO});
		
		for (MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento: listaTipoLancamento){
			if (!filtro.isTipoLancamento(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento())){
				continue;
			}
			
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento());
			
			if (operacaocontabil==null || operacaocontabil.isEmpty()){
				continue;
			}
			Map<Integer, List<Integer>> mapJaAdicionados = new HashMap<Integer, List<Integer>>();
			for(Operacaocontabil op : operacaocontabil){
				List<Documento> listaDocumento = documentoService.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento, op.getFormaPagamento());
			
				for (Documento documento: listaDocumento){
					if(!mapJaAdicionados.containsKey(documento.getCddocumento())){
						mapJaAdicionados.put(documento.getCddocumento(), new ArrayList<Integer>());
					}
					if(SinedUtil.isListNotEmpty(documento.getListaMovimentacaoOrigem())){
						Money totalMovimentacoes = movimentacaoService.calcularMovimentacoesByWhereInDocumento(documento.getCddocumento().toString());
						boolean possuiMovimentacaoVinculadaAMaisDeUmDocumento = movimentacaoService.isMovimentacaoVinculadaAMaisDeUmDocumento(documento);
						if(possuiMovimentacaoVinculadaAMaisDeUmDocumento){
							for(Movimentacaoorigem mo : documento.getListaMovimentacaoOrigem()){
								Movimentacao movimentacao = mo.getMovimentacao();
								if(mapJaAdicionados.get(documento.getCddocumento()).contains(movimentacao.getCdmovimentacao())){
									continue;
								}
								if(!Movimentacaoacao.CANCELADA.equals(movimentacao.getMovimentacaoacao()) && !movimentacaocontabilorigemService.existeLancamentoContabilMovimentacao(movimentacao, movimentacaocontabilTipoLancamento)){
									Double indicePercentual = movimentacao.getValor().divide(totalMovimentacoes).getValue().doubleValue();
									Money valor = new Money(indicePercentual * documento.getAux_documento().getValoratual().getValue().doubleValue());
									Double indicePercentualDocumento = documento.getAux_documento().getValoratual().divide(totalMovimentacoes).getValue().doubleValue();
									
									Rateio rateioProporcional = new Rateio();
									rateioProporcional.setListaRateioitem(new ArrayList<Rateioitem>());
									for(Rateioitem ri: movimentacao.getRateio().getListaRateioitem()){
										Money valorProporcional = new Money(indicePercentualDocumento * ri.getValor().getValue().doubleValue());
										Rateioitem riProporcional = new Rateioitem(ri.getContagerencial(), ri.getCentrocusto(), ri.getProjeto(), valorProporcional);
										
										rateioProporcional.getListaRateioitem().add(riProporcional);
									}
									Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, documento.getNumero(), valor, getMapaContaContabil(documento), getMapaValor(documento, movimentacao, valor), getMapaHistorico(documento, op), getlistaCentrocustoProjetoBean(rateioProporcional, true));
									
									movimentacaocontabil.addOrigem(documento);
									movimentacaocontabil.addOrigem(movimentacao);
									listaMovimentacaocontabil.add(movimentacaocontabil);
									mapJaAdicionados.get(documento.getCddocumento()).add(movimentacao.getCdmovimentacao());
								}
							}
						}else{
							for(Movimentacaoorigem mo : documento.getListaMovimentacaoOrigem()){
								Movimentacao movimentacao = mo.getMovimentacao();
								if(mapJaAdicionados.get(documento.getCddocumento()).contains(movimentacao.getCdmovimentacao())){
									continue;
								}
								if(!Movimentacaoacao.CANCELADA.equals(movimentacao.getMovimentacaoacao()) && !movimentacaocontabilorigemService.existeLancamentoContabilMovimentacao(movimentacao, movimentacaocontabilTipoLancamento)){
									Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, documento.getNumero(), movimentacao.getValor(), getMapaContaContabil(documento), getMapaValor(documento, movimentacao, movimentacao.getValor()), getMapaHistorico(documento, op), getlistaCentrocustoProjetoBean(movimentacao.getRateio(), true));
									
									movimentacaocontabil.addOrigem(documento);
									movimentacaocontabil.addOrigem(movimentacao);
									listaMovimentacaocontabil.add(movimentacaocontabil);
									
									mapJaAdicionados.get(documento.getCddocumento()).add(movimentacao.getCdmovimentacao());
								}
							}
						}
						listaDocumentoWhereNotIn.add(documento);
					}
				}
			}
			

		}
	}
	
	private void addListaMovimentacaocontabilContaPagarReceberAdiantamento(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil) throws Exception {
		List<MovimentacaocontabilTipoLancamentoEnum> listaTipoLancamento = Arrays.asList(new MovimentacaocontabilTipoLancamentoEnum[]{MovimentacaocontabilTipoLancamentoEnum.CONTA_RECEBER_BAIXADA_ADIANTAMENTO, MovimentacaocontabilTipoLancamentoEnum.CONTA_PAGAR_BAIXADA_ADIANTAMENTO});
		
		for (MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento: listaTipoLancamento){
			if (!filtro.isTipoLancamento(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento())){
				continue;
			}
			
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento());
			if (operacaocontabil==null || operacaocontabil.isEmpty()){
				continue;
			}
			for(Operacaocontabil op : operacaocontabil){

				List<Documento> listaDocumento = documentoService.findForGerarLancamentoContabilAntecipacao(filtro, movimentacaocontabilTipoLancamento);
				
				for (Documento documento: listaDocumento){
					
					for(HistoricoAntecipacao ha: documento.getListaHistoricoAntecipacaoDestino()){
						Money valor = new Money();
						
						switch (movimentacaocontabilTipoLancamento) {
							case CONTA_RECEBER_BAIXADA_ADIANTAMENTO:
								valor = valor.add(ha.getValor());
								break;
							case CONTA_PAGAR_BAIXADA_ADIANTAMENTO:
								valor = valor.add(ha.getValor());
								break;
							default:
								break;
						}
						Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, new Date(ha.getDataHora().getTime()), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, documento.getNumero(), valor, getMapaContaContabilAdiantamento(documento), getMapaValor(ha, documento), getMapaHistorico(ha, documento, op), getlistaCentrocustoProjetoBean(documento.getRateio(), true));
	
						movimentacaocontabil.addOrigem(documento);
						listaMovimentacaocontabil.add(movimentacaocontabil);
					}
				}
			}
		}	
	}
	
	private void addListaMovimentacaocontabilTransferencia(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, List<Movimentacao> listaMovimentacaoWhereNotIn) throws Exception {
		List<MovimentacaocontabilTipoLancamentoEnum> listaTipoLancamento = Arrays.asList(new MovimentacaocontabilTipoLancamentoEnum[]{MovimentacaocontabilTipoLancamentoEnum.DEPOSITO, MovimentacaocontabilTipoLancamentoEnum.DEPOSITO_CHEQUE, MovimentacaocontabilTipoLancamentoEnum.SAQUE, MovimentacaocontabilTipoLancamentoEnum.SAQUE_CHEQUE, MovimentacaocontabilTipoLancamentoEnum.TRANSFERENCIA_ENTRE_CONTAS, MovimentacaocontabilTipoLancamentoEnum.CONCILIACAO_CARTAO, MovimentacaocontabilTipoLancamentoEnum.TRANSFERENCIA_ENTRE_CAIXAS});
		
		for (MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento: listaTipoLancamento){
			if (!filtro.isTipoLancamento(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento())){
				continue;
			}
				
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento());
			if (operacaocontabil==null || operacaocontabil.isEmpty()){
				continue;
			}
			
			List<Movimentacao> listaMovimentacaoTransferencia = movimentacaoService.findForGerarLancamentoContabilTransferencia(filtro, movimentacaocontabilTipoLancamento);
			
			for(Operacaocontabil op : operacaocontabil){
				for (Movimentacao movimentacaoCredito: listaMovimentacaoTransferencia){
					if(listaMovimentacaoWhereNotIn.contains(movimentacaoCredito)){
						continue;
					}
					List<Movimentacaoorigem> listaMovimentacaoorigemOrigem = movimentacaoCredito.getListaMovimentacaoorigem();
					Movimentacao movimentacaoDebito = listaMovimentacaoorigemOrigem.get(0).getMovimentacaorelacionada();
					
					Map<OperacaocontabildebitocreditoControleEnum, Money> mapaValor = getMapaValor(movimentacaoCredito);
					List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBeanDebito = getlistaCentrocustoProjetoBean(movimentacaoDebito.getRateio());
					List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBeanCredito = getlistaCentrocustoProjetoBean(movimentacaoCredito.getRateio());
					
					Map<String, String> mapaHistorico = getMapaHistoricoTransferencia(movimentacaoCredito, op);
					Movimentacaocontabil movimentacaocontabil = new Movimentacaocontabil(movimentacaoCredito.getDataMovimentacao(), filtro.getEmpresa(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, movimentacaoCredito.getChecknum(), movimentacaoCredito.getValor(), op.getHistorico());
					movimentacaocontabil.setListaDebito(getListaDebitoCredito(op.getListaDebito(), getMapaContaContabilTransferencia(movimentacaoCredito), mapaValor, listaCentrocustoProjetoBeanDebito, op.getAgruparvalormesmaconta(), mapaHistorico, movimentacaocontabil.getTipoLancamento()));
					movimentacaocontabil.setListaCredito(getListaDebitoCredito(op.getListaCredito(), getMapaContaContabilTransferencia(movimentacaoCredito), mapaValor, listaCentrocustoProjetoBeanCredito, op.getAgruparvalormesmaconta(), mapaHistorico, movimentacaocontabil.getTipoLancamento()));
					
					addHistorico(movimentacaocontabil, op, mapaHistorico);
					
					movimentacaocontabil.addOrigem(movimentacaoCredito);
					listaMovimentacaoWhereNotIn.add(movimentacaoCredito);
					
					for (Movimentacaoorigem movimentacaoorigem: movimentacaoCredito.getListaMovimentacaoorigem()){
						movimentacaocontabil.addOrigem(movimentacaoorigem.getMovimentacaorelacionada());
						listaMovimentacaoWhereNotIn.add(movimentacaoorigem.getMovimentacaorelacionada());
					}
					
					listaMovimentacaocontabil.add(movimentacaocontabil);
				}
			}
		}
	}
	
	private void addListaMovimentacaocontabilRetornoBancario(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, List<Movimentacao> listaMovimentacaoWhereNotIn) throws Exception {
		List<MovimentacaocontabilTipoLancamentoEnum> listaTipoLancamento = Arrays.asList(new MovimentacaocontabilTipoLancamentoEnum[]{MovimentacaocontabilTipoLancamentoEnum.CREDITO_GERADO_RETORNO_BANCARIO});
		
		for (MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento: listaTipoLancamento){
			if (!filtro.isTipoLancamento(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento())){
				continue;
			}
			
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento());
			if (operacaocontabil==null || operacaocontabil.isEmpty()){
				continue;
			}
			
			List<Movimentacao> listaMovimentacaoRetornoBancario = movimentacaoService.findForGerarLancamentoContabilRetornoBancario(filtro, movimentacaocontabilTipoLancamento);
			
			for(Operacaocontabil op : operacaocontabil){
				for (Movimentacao movimentacao: listaMovimentacaoRetornoBancario){
					Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, movimentacao.getChecknum(), movimentacao.getValor(), getMapaContaContabil(movimentacao), getMapaValor(movimentacao), getMapaHistorico(movimentacao, op), getlistaCentrocustoProjetoBean(movimentacao.getRateio()));
					movimentacaocontabil.addOrigem(movimentacao);
					listaMovimentacaoWhereNotIn.add(movimentacao);
					listaMovimentacaocontabil.add(movimentacaocontabil);
				}
			}
		}
	}
	
	private void addListaMovimentacaocontabilContaGerencial(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, List<Movimentacao> listaMovimentacaoWhereNotIn, List<Documento> listaDocumentoWhereNotIn, List<Rateioitem> listaRateioItemWhereNotIn, List<Movimentacaoestoque> listaMovimentacaoEstoqueWhereNotIn) throws Exception {
		if (filtro.isTipoLancamento(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO) 
				|| filtro.isTipoLancamento(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER)
				|| filtro.isTipoLancamento(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE)){
			List<Operacaocontabil> listaOperacaocontabil = operacaocontabilService.findForGerarLancamentoContabil(filtro);
			for (Operacaocontabil operacaocontabil: listaOperacaocontabil){
				if (operacaocontabil.getContagerencial()!=null){
					Money valorConsiderado;
					boolean considerarRateio = false;
					boolean considerarValorCustoMedio = false;
					boolean considerarValorCusto = false;
					boolean considerarValorMovimentacaoEstoque = false;
					boolean agruparPorPessoa = false;
					
					for(Operacaocontabildebitocredito operacaoContabilCredito : operacaocontabil.getListaCredito()){
						if(OperacaocontabildebitocreditoControleEnum.VALOR_ITEM_RATEIO.equals(operacaoContabilCredito.getControle())){
							if(!agruparPorPessoa && OperacaocontabildebitocreditoTipoEnum.PESSOA.equals(operacaoContabilCredito.getTipo())) {
								agruparPorPessoa = true;
							}
							considerarRateio = true;
							break;
						}else if (OperacaocontabildebitocreditoControleEnum.VALOR_CUSTO.equals(operacaoContabilCredito.getControle())){
							considerarValorCusto = true;
							break;
						} else if (OperacaocontabildebitocreditoControleEnum.VALOR_CUSTO_MEDIO.equals(operacaoContabilCredito.getControle())){
							considerarValorCustoMedio = true;
							break;
						} else if(OperacaocontabildebitocreditoControleEnum.VALOR_CUSTO_MEDIO.equals(operacaoContabilCredito.getControle())){
							considerarValorMovimentacaoEstoque = true;
							break;
						}
						
					}
					
					if (OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO.equals(operacaocontabil.getTipoLancamento())){		
						//Movimenta��es que j� entraram nos outros tipos n�o podem entrar outra vez.
						List<Movimentacao> listaMovimentacaoContaGerencial = new ArrayList<Movimentacao>();
						if(considerarRateio){
							String whereNotInRatioitem = CollectionsUtil.listAndConcatenate(listaRateioItemWhereNotIn, "cdrateioitem", ", ");
							listaMovimentacaoContaGerencial = movimentacaoService.findForGerarLancamentoContabilContaGerencial(filtro, operacaocontabil, "", whereNotInRatioitem, considerarRateio);
						} else {
							String whereNotIn = CollectionsUtil.listAndConcatenate(listaMovimentacaoWhereNotIn, "cdmovimentacao", ", ");
							listaMovimentacaoContaGerencial = movimentacaoService.findForGerarLancamentoContabilContaGerencial(filtro, operacaocontabil, whereNotIn, "", false);							
						}
						
						for (Movimentacao movimentacao: listaMovimentacaoContaGerencial){
							Integer countOrigens = SinedUtil.isListNotEmpty(movimentacao.getListaMovimentacaoorigem())? movimentacao.getListaMovimentacaoorigem().size(): 1;
							if(considerarRateio){
								for(Rateioitem ri : movimentacao.getRateio().getListaRateioitem()){
									List<Movimentacaocontabildebitocredito> listaDebito = new ArrayList<Movimentacaocontabildebitocredito>();
									List<Movimentacaocontabildebitocredito> listaCredito = new ArrayList<Movimentacaocontabildebitocredito>();
									
									if(SinedUtil.isListNotEmpty(movimentacao.getListaMovimentacaoorigem())){
										if(agruparPorPessoa) {
											Money totalRateioItem = new Money();
											
											for(Movimentacaoorigem mo : movimentacao.getListaMovimentacaoorigem()) {
												if(mo.getDocumento() != null) {
													for(Rateioitem riDocumentoOrigem : mo.getDocumento().getRateio().getListaRateioitem()) {
														if(ri.getContagerencial().equals(riDocumentoOrigem.getContagerencial())
																&& ri.getCentrocusto().equals(riDocumentoOrigem.getCentrocusto())
																&& ((ri.getProjeto() == null && riDocumentoOrigem.getProjeto() == null) 
																		|| (ri.getProjeto() != null && riDocumentoOrigem.getProjeto() != null
																				&& ri.getProjeto().equals(riDocumentoOrigem.getProjeto())))) {
															totalRateioItem = totalRateioItem.add(riDocumentoOrigem.getValor());
														}
													}
												}
											}
											
											for(Movimentacaoorigem mo : movimentacao.getListaMovimentacaoorigem()) {
												if(mo.getDocumento() != null) {
													for(Rateioitem riDocumentoOrigem : mo.getDocumento().getRateio().getListaRateioitem()) {
														Rateio rateioAux = new Rateio();
														rateioAux.setListaRateioitem(new ArrayList<Rateioitem>());
														if(ri.getContagerencial().equals(riDocumentoOrigem.getContagerencial())
																&& ri.getCentrocusto().equals(riDocumentoOrigem.getCentrocusto())
																&& ((ri.getProjeto() == null && riDocumentoOrigem.getProjeto() == null) 
																		|| (ri.getProjeto() != null && riDocumentoOrigem.getProjeto() != null
																				&& ri.getProjeto().equals(riDocumentoOrigem.getProjeto())))) {
															Money percentual = riDocumentoOrigem.getValor().divide(totalRateioItem);
															Money valorProporcionalRateio = percentual.multiply(ri.getValor());
															
															Rateioitem riAux = new Rateioitem();
															riAux.setValor(valorProporcionalRateio);
															riAux.setProjeto(riDocumentoOrigem.getProjeto());
															riAux.setCentrocusto(riDocumentoOrigem.getCentrocusto());
															rateioAux.getListaRateioitem().add(riAux);
															
															Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO, movimentacao.getChecknum(), riAux.getValor(), getMapaContaContabil(movimentacao, mo.getDocumento()), getMapaValor(riAux), getMapaHistorico(movimentacao, operacaocontabil), getlistaCentrocustoProjetoBean(rateioAux, true), ri);
															
//															if(movimentacaocontabilItem != null){
//																if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaCredito())){
//																	listaCredito.addAll(movimentacaocontabilItem.getListaCredito());
//																}
//																if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaDebito())){
//																	listaDebito.addAll(movimentacaocontabilItem.getListaDebito());
//																}
//															}
//															
//															Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO, movimentacao.getChecknum(), ri.getValor(), getMapaContaContabil(movimentacao, mo.getDocumento()), getMapaValor(ri), getMapaHistorico(movimentacao, operacaocontabil), getlistaCentrocustoProjetoBean(rateioAux, true), listaCredito, listaDebito);
															
															movimentacaocontabil.addOrigem(riDocumentoOrigem);
															movimentacaocontabil.addOrigem(movimentacao);
															listaMovimentacaocontabil.add(movimentacaocontabil);
															listaRateioItemWhereNotIn.add(ri);
														}
													}
												}
											}
										} else {
											for(Movimentacaoorigem mo: movimentacao.getListaMovimentacaoorigem()){
												if(mo.getDocumento() != null){
													Double indice = countOrigens > 1? mo.getDocumento().getAux_documento().getValoratual().divide(movimentacao.getValor()).getValue().doubleValue(): 1;
													Rateio rateioAux = new Rateio();
													rateioAux.setListaRateioitem(new ArrayList<Rateioitem>());
													for(Rateioitem ri2: movimentacao.getRateio().getListaRateioitem()){
														Rateioitem riAux = new Rateioitem();
														riAux.setValor(new Money(ri2.getValor().getValue().doubleValue() * indice));
														riAux.setProjeto(ri2.getProjeto());
														riAux.setCentrocusto(ri2.getCentrocusto());
														rateioAux.getListaRateioitem().add(riAux);
													}
													
													Movimentacaocontabil movimentacaocontabilItem = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO, movimentacao.getChecknum(), ri.getValor(), getMapaContaContabil(movimentacao, mo.getDocumento()), getMapaValor(ri, indice), getMapaHistorico(movimentacao, operacaocontabil), getlistaCentrocustoProjetoBean(rateioAux, true), ri);
													
													if(movimentacaocontabilItem != null){
														if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaCredito())){
															listaCredito.addAll(movimentacaocontabilItem.getListaCredito());
														}
														if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaDebito())){
															listaDebito.addAll(movimentacaocontabilItem.getListaDebito());
														}
													}
												}
											}
											Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO, movimentacao.getChecknum(), ri.getValor(), getMapaContaContabil(movimentacao), getMapaValor(ri), getMapaHistorico(movimentacao, operacaocontabil), getlistaCentrocustoProjetoBean(movimentacao.getRateio(), true), listaCredito, listaDebito);
											movimentacaocontabil.addOrigem(ri);
											movimentacaocontabil.addOrigem(movimentacao);
											listaMovimentacaocontabil.add(movimentacaocontabil);
											listaRateioItemWhereNotIn.add(ri);
										}
									}
								}
							}else {
								List<Movimentacaocontabildebitocredito> listaDebito = new ArrayList<Movimentacaocontabildebitocredito>();
								List<Movimentacaocontabildebitocredito> listaCredito = new ArrayList<Movimentacaocontabildebitocredito>();
								if(SinedUtil.isListNotEmpty(movimentacao.getListaMovimentacaoorigem())){
									for(Movimentacaoorigem mo: movimentacao.getListaMovimentacaoorigem()){
										if(mo.getDocumento() != null){
											Double indice = countOrigens > 1? mo.getDocumento().getAux_documento().getValoratual().divide(movimentacao.getValor()).getValue().doubleValue(): 1;
											Rateio rateioAux = new Rateio();
											rateioAux.setListaRateioitem(new ArrayList<Rateioitem>());
											for(Rateioitem ri2: movimentacao.getRateio().getListaRateioitem()){
												Rateioitem riAux = new Rateioitem();
												riAux.setValor(new Money(ri2.getValor().getValue().doubleValue() * indice));
												riAux.setProjeto(ri2.getProjeto());
												riAux.setCentrocusto(ri2.getCentrocusto());
												rateioAux.getListaRateioitem().add(riAux);
											}
											//Movimentacaocontabil movimentacaocontabilItem = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO, movimentacao.getChecknum(), mo.getDocumento().getAux_documento().getValoratual(), getMapaContaContabil(movimentacao, mo.getDocumento()), getMapaValor(movimentacao, mo.getDocumento(), countOrigens), getMapaHistorico(movimentacao, operacaocontabil), getlistaCentrocustoProjetoBean(rateioAux, true));
											Movimentacaocontabil movimentacaocontabilItem = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO, movimentacao.getChecknum(), mo.getDocumento().getAux_documento().getValoratual(), getMapaContaContabil(movimentacao, mo.getDocumento()), getMapaValor(movimentacao, mo.getDocumento(), countOrigens), getMapaHistorico(movimentacao, operacaocontabil), getlistaCentrocustoProjetoBean(rateioAux, true));
											if(movimentacaocontabilItem != null){
												if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaCredito())){
													listaCredito.addAll(movimentacaocontabilItem.getListaCredito());
												}
												if(SinedUtil.isListNotEmpty(movimentacaocontabilItem.getListaDebito())){
													listaDebito.addAll(movimentacaocontabilItem.getListaDebito());
												}
											}
										}
									}
								}
								Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacao.getDataMovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO, movimentacao.getChecknum(), movimentacao.getValor(), getMapaContaContabil(movimentacao), getMapaValor(movimentacao), getMapaHistorico(movimentacao, operacaocontabil), getlistaCentrocustoProjetoBean(movimentacao.getRateio(), true), listaCredito, listaDebito);
								
								movimentacaocontabil.addOrigem(movimentacao);
								listaMovimentacaocontabil.add(movimentacaocontabil);
								listaMovimentacaoWhereNotIn.add(movimentacao);
							}
						}		
					}else if (OperacaoContabilTipoLancamento.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER.equals(operacaocontabil.getTipoLancamento())){
						//Documentos que j� entraram nos outros tipos n�o podem entrar outra vez.
						List<Documento> listaDocumentoContaGerencial = new ArrayList<Documento>();
						if(considerarRateio){
							String whereNotInRateioitem = CollectionsUtil.listAndConcatenate(listaRateioItemWhereNotIn, "cdrateioitem", ", ");
							listaDocumentoContaGerencial = documentoService.findForGerarLancamentoContabilContaGerencial(filtro, operacaocontabil.getContagerencial(), "", whereNotInRateioitem);
						} else {											
							String whereNotIn = CollectionsUtil.listAndConcatenate(listaDocumentoWhereNotIn, "cddocumento", ", ");
							listaDocumentoContaGerencial = documentoService.findForGerarLancamentoContabilContaGerencial(filtro, operacaocontabil.getContagerencial(), whereNotIn, "");
						}
						
						for (Documento documento: listaDocumentoContaGerencial){
							List<DocumentoApropriacao> listaApropriacao = documentoApropriacaoService.findByDocumento(documento);
							if(SinedUtil.isListNotEmpty(listaApropriacao)){
								for(DocumentoApropriacao apropriacao: listaApropriacao){
									Date dtMovimentacao = SinedDateUtils.addDiasData(apropriacao.getMesAno(), SinedDateUtils.getDia(documento.getDataForGeracaoContabil())-1);
									if(SinedDateUtils.getDia(dtMovimentacao) != SinedDateUtils.getDia(documento.getDataForGeracaoContabil())){
										dtMovimentacao = SinedDateUtils.lastDateOfMonth(apropriacao.getMesAno());
									}
									if(considerarRateio){
										for(Rateioitem ri: documento.getRateio().getListaRateioitem()){
											Money valorRateado = new Money((apropriacao.getValor().getValue().doubleValue()/100) * ri.getPercentual());
											ri.setValor(valorRateado);
											Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, dtMovimentacao, operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER, documento.getNumero(), valorRateado, getMapaContaContabil(documento), getMapaValor(ri), getMapaHistorico(documento, operacaocontabil), getlistaCentrocustoProjetoBean(documento.getRateio(), true), documento.getDataForGeracaoContabil());
											movimentacaocontabil.addOrigem(ri);
											movimentacaocontabil.addOrigem(documento);
											listaMovimentacaocontabil.add(movimentacaocontabil);
											listaRateioItemWhereNotIn.add(ri);
										}
										
									} else{
										documento.setValor(apropriacao.getValor());
										Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, dtMovimentacao, operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER, documento.getNumero(), apropriacao.getValor(), getMapaContaContabil(documento), getMapaValor(documento), getMapaHistorico(documento, operacaocontabil), getlistaCentrocustoProjetoBean(documento.getRateio(), true), documento.getDataForGeracaoContabil());
										movimentacaocontabil.addOrigem(documento);
										listaMovimentacaocontabil.add(movimentacaocontabil);
										listaDocumentoWhereNotIn.add(documento);
									}
								}
							}else{
								if(considerarRateio){
									for(Rateioitem ri: documento.getRateio().getListaRateioitem()){
										Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, documento.getDataForGeracaoContabil(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER, documento.getNumero(), ri.getValor(), getMapaContaContabil(documento), getMapaValor(ri), getMapaHistorico(documento, operacaocontabil), getlistaCentrocustoProjetoBean(documento.getRateio(), true), ri);
										movimentacaocontabil.addOrigem(ri);
										movimentacaocontabil.addOrigem(documento);
										listaMovimentacaocontabil.add(movimentacaocontabil);
										listaRateioItemWhereNotIn.add(ri);
									}
									
								} else{
									Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, documento.getDataForGeracaoContabil(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER, documento.getNumero(), documento.getValor(), getMapaContaContabil(documento), getMapaValor(documento), getMapaHistorico(documento, operacaocontabil), getlistaCentrocustoProjetoBean(documento.getRateio(), true));
									movimentacaocontabil.addOrigem(documento);
									listaMovimentacaocontabil.add(movimentacaocontabil);
									listaDocumentoWhereNotIn.add(documento);
								}
							}
						}		
					} else if (OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE.equals(operacaocontabil.getTipoLancamento())){
						//Movimenta��es de Estoque que j� entraram nos outros tipos n�o podem entrar outra vez.
						List<Movimentacaoestoque> listaMovimentacaoEstoqueGerencial = new ArrayList<Movimentacaoestoque>();
						if(considerarRateio){
							String whereNotInRateioitem = CollectionsUtil.listAndConcatenate(listaRateioItemWhereNotIn, "cdrateioitem", ", ");
							listaMovimentacaoEstoqueGerencial = movimentacaoestoqueService.findForGerarLancamentoContabilContaGerencial(filtro, operacaocontabil.getContagerencial(), "", whereNotInRateioitem, null, true);
						} else {
							String whereNotIn = CollectionsUtil.listAndConcatenate(listaMovimentacaoEstoqueWhereNotIn, "cdmovimentacaoestoque", ", ");
							listaMovimentacaoEstoqueGerencial = movimentacaoestoqueService.findForGerarLancamentoContabilContaGerencial(filtro, operacaocontabil.getContagerencial(), whereNotIn, "", null, false);							
						}
						
						for(Movimentacaoestoque movimentacaoEstoque : listaMovimentacaoEstoqueGerencial) {
							if(considerarRateio){
								for(Rateioitem ri: movimentacaoEstoque.getRateio().getListaRateioitem()){
									Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacaoEstoque.getDtmovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE, movimentacaoEstoque.getCdmovimentacaoestoque().toString(), ri.getValor(), getMapaContaContabil(movimentacaoEstoque, filtro.getEmpresa()), getMapaValor(ri), getMapaHistorico(movimentacaoEstoque, operacaocontabil), getlistaCentrocustoProjetoBean(movimentacaoEstoque.getRateio(), true));
									movimentacaocontabil.addOrigem(ri);
									movimentacaocontabil.addOrigem(movimentacaoEstoque);
									listaMovimentacaocontabil.add(movimentacaocontabil);
									listaMovimentacaoEstoqueWhereNotIn.add(movimentacaoEstoque);
								}
							} else {
								valorConsiderado = null;
								if(considerarValorCustoMedio){
									valorConsiderado = new Money(movimentacaoEstoque.getMaterial().getValorcusto() != null ? movimentacaoEstoque.getMaterial().getValorcusto() : 0d);								}
								if(considerarValorCusto){
									valorConsiderado = new Money(movimentacaoEstoque.getValorcusto() != null ? movimentacaoEstoque.getValorcusto() : 0d);
								}
								if(considerarValorMovimentacaoEstoque){
									valorConsiderado = new Money(movimentacaoEstoque.getValor() != null ? movimentacaoEstoque.getValor() : 0d);
								}
								Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacaoEstoque.getDtmovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE, movimentacaoEstoque.getCdmovimentacaoestoque().toString(), valorConsiderado, getMapaContaContabil(movimentacaoEstoque, filtro.getEmpresa()), getMapaValor(movimentacaoEstoque), getMapaHistorico(movimentacaoEstoque, operacaocontabil), getlistaCentrocustoProjetoBean(movimentacaoEstoque.getRateio(), true));
								movimentacaocontabil.addOrigem(movimentacaoEstoque);
								listaMovimentacaocontabil.add(movimentacaocontabil);
								listaMovimentacaoEstoqueWhereNotIn.add(movimentacaoEstoque);
							}
						}	
					}
				}
			}			
		}
	}
	
	private void addListaMovimentacaocontabilEstoqueOrigem(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil, List<Movimentacao> listaMovimentacaoWhereNotIn, List<Rateioitem> listaRateioItemWhereNotIn, List<Movimentacaoestoque> listaMovimentacaoEstoqueWhereNotIn) throws Exception{
		if (filtro.isTipoLancamento(OperacaoContabilTipoLancamento.MOVIMENTACAO_ESTOQUE_ORIGEM)){
			List<Operacaocontabil> listaOperacaocontabil = operacaocontabilService.findForGerarLancamentoContabil(filtro);
			for (Operacaocontabil operacaocontabil: listaOperacaocontabil){
				if (operacaocontabil.getOrigemMovimentacaoEstoque()!=null){
					Money valorConsiderado;
					boolean considerarRateio = false;
					boolean considerarValorCustoMedio = false;
					boolean considerarValorCusto = false;
					for(Operacaocontabildebitocredito operacaoContabilCredito : operacaocontabil.getListaCredito()){
						if(OperacaocontabildebitocreditoControleEnum.VALOR_ITEM_RATEIO.equals(operacaoContabilCredito.getControle())){
							considerarRateio = true;
							break;
						}else if (OperacaocontabildebitocreditoControleEnum.VALOR_CUSTO.equals(operacaoContabilCredito.getControle())){
							considerarValorCusto = true;
							break;
						} else if (OperacaocontabildebitocreditoControleEnum.VALOR_CUSTO_MEDIO.equals(operacaoContabilCredito.getControle())){
							considerarValorCustoMedio = true;
							break;
						}
					}
					List<Movimentacaoestoque> listaMovimentacaoEstoqueGerencial = new ArrayList<Movimentacaoestoque>();
					if(considerarRateio){
						String whereNotInRateioitem = CollectionsUtil.listAndConcatenate(listaRateioItemWhereNotIn, "cdrateioitem", ", ");
						listaMovimentacaoEstoqueGerencial = movimentacaoestoqueService.findForGerarLancamentoContabilContaGerencial(filtro, operacaocontabil.getContagerencial(), "", whereNotInRateioitem, operacaocontabil.getOrigemMovimentacaoEstoque(), true);
					} else {
						String whereNotIn = CollectionsUtil.listAndConcatenate(listaMovimentacaoEstoqueWhereNotIn, "cdmovimentacaoestoque", ", ");
						listaMovimentacaoEstoqueGerencial = movimentacaoestoqueService.findForGerarLancamentoContabilContaGerencial(filtro, operacaocontabil.getContagerencial(), whereNotIn, "", operacaocontabil.getOrigemMovimentacaoEstoque(), false);							
					}
					
					for(Movimentacaoestoque movimentacaoEstoque : listaMovimentacaoEstoqueGerencial) {
						if(considerarRateio && (movimentacaoEstoque.getRateio() != null &&  movimentacaoEstoque.getRateio().getListaRateioitem() != null)){
							for(Rateioitem ri: movimentacaoEstoque.getRateio().getListaRateioitem()){
								Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacaoEstoque.getDtmovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.MOVIMENTACAO_ESTOQUE_ORIGEM, movimentacaoEstoque.getCdmovimentacaoestoque().toString(), ri.getValor(), getMapaContaContabil(movimentacaoEstoque, filtro.getEmpresa()), getMapaValor(ri), getMapaHistorico(movimentacaoEstoque, operacaocontabil), getlistaCentrocustoProjetoBean(movimentacaoEstoque.getRateio(), true));
								movimentacaocontabil.addOrigem(ri, movimentacaoEstoque);
								listaMovimentacaocontabil.add(movimentacaocontabil);
								listaMovimentacaoEstoqueWhereNotIn.add(movimentacaoEstoque);
							}
						} else {
							valorConsiderado = new Money(movimentacaoEstoque.getValor() != null ? movimentacaoEstoque.getValor() : 0d);
							if(considerarValorCustoMedio){
								valorConsiderado = new Money(movimentacaoEstoque.getMaterial().getValorcusto() != null ? movimentacaoEstoque.getMaterial().getValorcusto() : 0d);								
							} else if(considerarValorCusto){
								valorConsiderado = new Money(movimentacaoEstoque.getValorcusto() != null ? movimentacaoEstoque.getValorcusto() : 0d);
							}
								
							Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, movimentacaoEstoque.getDtmovimentacao(), operacaocontabil, operacaocontabil.getTipo().getMovimentacaocontabilTipo(), MovimentacaocontabilTipoLancamentoEnum.MOVIMENTACAO_ESTOQUE_ORIGEM, movimentacaoEstoque.getCdmovimentacaoestoque().toString(), valorConsiderado, getMapaContaContabil(movimentacaoEstoque, filtro.getEmpresa()), getMapaValor(movimentacaoEstoque), getMapaHistorico(movimentacaoEstoque, operacaocontabil), getlistaCentrocustoProjetoBean(movimentacaoEstoque.getRateio(), true));
							if(movimentacaocontabil != null){
								movimentacaocontabil.addOrigem(movimentacaoEstoque);
								listaMovimentacaocontabil.add(movimentacaocontabil);
								listaMovimentacaoEstoqueWhereNotIn.add(movimentacaoEstoque);
							}
						}
					}		
				}
			}
		}
	}
	
	private void addListaMovimentacaocontabilVenda(GerarLancamentoContabilFiltro filtro, List<Movimentacaocontabil> listaMovimentacaocontabil) throws Exception {
		List<MovimentacaocontabilTipoLancamentoEnum> listaTipoLancamento = Arrays.asList(new MovimentacaocontabilTipoLancamentoEnum[]{MovimentacaocontabilTipoLancamentoEnum.VENDA});
		
		for (MovimentacaocontabilTipoLancamentoEnum movimentacaocontabilTipoLancamento: listaTipoLancamento){
			if (!filtro.isTipoLancamento(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento())){
				continue;
			}
			
			List<Operacaocontabil> operacaocontabil = filtro.getMapaTipoLancamentoOperacaoContabil().get(movimentacaocontabilTipoLancamento.getOperacaocontabilTipoLancamento());
			if (operacaocontabil==null || operacaocontabil.isEmpty()){
				continue;
			}
			
			List<Venda> listaVenda = vendaService.findForGerarLancamentoContabil(filtro, movimentacaocontabilTipoLancamento);
			
			for(Operacaocontabil op : operacaocontabil){
				for (Venda venda: listaVenda){
					Money valor = venda.getTotalvenda();
					
					List<GerarLancamentoContabilCentrocustoProjetoBean> listaCentrocustoProjetoBean = new ArrayList<GerarLancamentoContabilCentrocustoProjetoBean>();
					if (venda.getCentrocusto()!=null || venda.getProjeto()!=null){
						listaCentrocustoProjetoBean.add(new GerarLancamentoContabilCentrocustoProjetoBean(venda.getCentrocusto(), venda.getProjeto(), valor));
					}
					
					Movimentacaocontabil movimentacaocontabil = getMovimentacaocontabil(filtro, new Date(venda.getDtvenda().getTime()), op, op.getTipo().getMovimentacaocontabilTipo(), movimentacaocontabilTipoLancamento, venda.getCdvenda().toString(), valor, getMapaContaContabil(venda), getMapaValor(venda), getMapaHistorico(venda, op), listaCentrocustoProjetoBean);
					movimentacaocontabil.addOrigem(venda);
					listaMovimentacaocontabil.add(movimentacaocontabil);
				}
			}
		}
	}
	
	private void validarListaMovimentacaocontabil(List<Movimentacaocontabil> listaMovimentacaocontabil){
		for (Iterator<Movimentacaocontabil> iterator = listaMovimentacaocontabil.iterator(); iterator.hasNext();) {
			Movimentacaocontabil movimentacaocontabil = iterator.next();
			if(this.movimentacaoContabilInvalida(movimentacaocontabil)){
				iterator.remove();
			}			
		}
		
//		Collections.sort(listaMovimentacaocontabil,new Comparator<Movimentacaocontabil>(){
//			public int compare(Movimentacaocontabil o1, Movimentacaocontabil o2) {
//				int x1 = o1.getDtlancamento() == null || o2.getDtlancamento() == null? 0:
//							o1.getDtlancamento().compareTo(o2.getDtlancamento());
//				if(x1 != 0){
//					return x1;
//				}
//				return o1.getDtReferencia().compareTo(o2.getDtReferencia());
//			}
//		});
	}
	public ModelAndView ajaxComboValor(WebRequestContext request, OperacaoContabilTipoLancamento tipoLancamento) throws Exception{
		Map<OperacaocontabildebitocreditoControleEnum, Money> map = null;
		if(tipoLancamento != null && tipoLancamento.getCdOperacaoContabilTipoLancamento() != null){
			tipoLancamento = operacaoContabilTipoLancamentoService.load(tipoLancamento);
			
			if(OperacaoContabilTipoLancamento.PROVISIONAMENTO.equals(tipoLancamento)){
				map = getMapaValor(new Provisao());
			}
			
			if(OperacaoContabilTipoLancamento.DESPESA_COLABORADOR.equals(tipoLancamento)){
				map = getMapaValor(new Colaboradordespesa(), new Colaboradordespesaitem());
			}
			
			if(OperacaoContabilTipoLancamento.DEPOSITO.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.DEPOSITO_CHEQUE.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.SAQUE.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.SAQUE_CHEQUE.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.TRANSFERENCIA_ENTRE_CAIXAS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.TRANSFERENCIA_ENTRE_CONTAS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONCILIACAO_CARTAO.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CREDITO_GERADO_RETORNO_BANCARIO.equals(tipoLancamento)){
				map = getMapaValor(new Movimentacao());
			}			
			if(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.MOVIMENTACAO_ESTOQUE_ORIGEM.equals(tipoLancamento)){
				map = getMapaValor(new Rateioitem());
				map.putAll(getMapaValor(new Movimentacao()));
				map.putAll(getMapaValor(new Documento()));
				map.putAll(getMapaValor(new Movimentacaoestoque()));
				map.putAll(getMapaValor(new Movimentacao(), new Documento(), 10));
			}
			if(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO.equals(tipoLancamento)) {
				map = getMapaValor(new Rateioitem());
				map.putAll(getMapaValor(new Movimentacao()));
				map.putAll(getMapaValor(new Movimentacao(), new Documento(), 10));
			}
			if(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO.equals(tipoLancamento)) {
				map = getMapaValor(new Documento(), new Movimentacao(), new Money());
				map.putAll(getMapaValor(new Movimentacao()));
				map.remove(OperacaocontabildebitocreditoControleEnum.VALOR_JUROS);				
			}
			if(OperacaoContabilTipoLancamento.CONTA_PAGAR_BAIXADA_ADIANTAMENTO.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_ADIANTAMENTO.equals(tipoLancamento)){
				map = getMapaValor(new HistoricoAntecipacao(), new Documento());
			}
			if(OperacaoContabilTipoLancamento.CONTA_PAGAR_BAIXADA_FORNECEDOR.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CARTAO.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CHEQUE.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_DINHEIRO.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_PARCIAL.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.JUROS_PAGOS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.JUROS_RECEBIDOS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.DESCONTOS_CONCEDIDOS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.DESCONTOS_OBTIDOS.equals(tipoLancamento)){
				map = getMapaValor(new Documento(), new Movimentacao(), new Money());
				map.putAll(getMapaValor(new Movimentacao()));
				if(OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_PARCIAL.equals(tipoLancamento)){
					map.remove(OperacaocontabildebitocreditoControleEnum.VALOR_JUROS);				
				}
			}
			
			if(OperacaoContabilTipoLancamento.RECOLHIMENTO_ISS_CLIENTE.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.RECOLHIMENTO_ISS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.RECOLHIMENTO_IR.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.RECOLHIMENTO_CSLL.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.RECOLHIMENTO_INSS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.RECOLHIMENTO_PIS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.RECOLHIMENTO_COFINS.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.RECOLHIMENTO_ICMS.equals(tipoLancamento)){
				
				map = getMapaValor(new NotaFiscalServico(), tipoLancamento != null ? tipoLancamento.getMovimentacaocontabilTipoLancamento() : null);
			}
			
			if(OperacaoContabilTipoLancamento.RECOLHIMENTO_II.equals(tipoLancamento) ||
				OperacaoContabilTipoLancamento.RECOLHIMENTO_IPI.equals(tipoLancamento)){
				map = getMapaValor(new Notafiscalprodutoitem(), tipoLancamento != null ? tipoLancamento.getMovimentacaocontabilTipoLancamento() : null);
			}
			
			if(OperacaoContabilTipoLancamento.EMISSAO_NF.equals(tipoLancamento)){
				
				map = getMapaValor(new NotaFiscalServico(), tipoLancamento != null ? tipoLancamento.getMovimentacaocontabilTipoLancamento() : null);
				map.putAll(getMapaValor(new Notafiscalproduto(), tipoLancamento != null ? tipoLancamento.getMovimentacaocontabilTipoLancamento() : null));
				map.putAll(getMapaValor(new Notafiscalprodutoitem(), tipoLancamento != null ? tipoLancamento.getMovimentacaocontabilTipoLancamento() : null));
			}
			
			if(OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL.equals(tipoLancamento)){
				map = getMapaValor(new Entregamaterial());
				map.putAll(getMapaValor(new Entregadocumento()));
			}
			
			if(OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO.equals(tipoLancamento)){
				map = getMapaValor(new Entregamaterial(),new EntregaDocumentoApropriacao());
				map.putAll(getMapaValor(new Entregadocumento(), new EntregaDocumentoApropriacao()));
			}
			if(OperacaoContabilTipoLancamento.VENDA.equals(tipoLancamento)){
				map = getMapaValor(new Venda());
			}
			if(OperacaoContabilTipoLancamento.MOVIMENTACAO_ESTOQUE_ORIGEM.equals(tipoLancamento)) {
				map = getMapaValor(new Movimentacaoestoque());
			}
		}
		ArrayList<GenericBean> listaControle = new ArrayList<GenericBean>();
		if(map != null){
			GenericBean genericBean;
			for(OperacaocontabildebitocreditoControleEnum op : map.keySet()){
				genericBean = new GenericBean(op.name(), op.getNome());
				if(!listaControle.contains(genericBean)){					
					listaControle.add(genericBean);
				}
			}
		}
		return new JsonModelAndView().addObject("listaControle", listaControle);
	}
}