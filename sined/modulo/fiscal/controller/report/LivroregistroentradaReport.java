package br.com.linkcom.sined.modulo.fiscal.controller.report;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.geral.service.EntregamaterialService;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.Entradafiscal;
import br.com.linkcom.sined.modulo.fiscal.controller.report.filtro.LivroregistroentradaFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.filtro.ModeloRegistroEntrada;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/fiscal/relatorio/Livroregistroentrada", authorizationModule=ReportAuthorizationModule.class)
public class LivroregistroentradaReport extends SinedReport<LivroregistroentradaFiltro>{

	private EntregamaterialService entregamaterialService;
	
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {
		this.entregamaterialService = entregamaterialService;
	}
	
	@DefaultAction
	public ModelAndView handle(WebRequestContext request){
		LivroregistroentradaFiltro filtro = new LivroregistroentradaFiltro();
		return new ModelAndView("relatorio/livroregistroentrada", "filtro", filtro);
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, LivroregistroentradaFiltro filtro) throws Exception {
		return createReportLivroRegistroEntrada(request, filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "livro_registro_entrada";
	}

	@Override
	public String getTitulo(LivroregistroentradaFiltro filtro) {
		return "LIVRO REGISTRO DE ENTRADAS � RE � " + filtro.getModelo().getDescricao().toUpperCase();
	}

	private IReport createReportLivroRegistroEntrada(WebRequestContext request, LivroregistroentradaFiltro filtro) throws IllegalAccessException, InvocationTargetException {
		List<Entregamaterial> listaEntregamaterial = entregamaterialService.findForLivroEntrada(filtro);
		Map<String, Entradafiscal> mapa = new HashMap<String, Entradafiscal>();
		
		for (Entregamaterial entregamaterial: listaEntregamaterial){
			
			Entregadocumento entregadocumento = entregamaterial.getEntregadocumento();
			Fornecedor fornecedor = entregadocumento.getFornecedor();
			
			Entradafiscal bean = new Entradafiscal();
			bean.setCdentregadocumento(entregadocumento.getCdentregadocumento());
			bean.setDtentrada(entregadocumento.getDtentrada());
			bean.setEspecie(entregadocumento.getModelodocumentofiscal() != null && entregadocumento.getModelodocumentofiscal().getNome() != null ? entregadocumento.getModelodocumentofiscal().getNome() : null);
			bean.setSerie(entregadocumento.getSerie() != null ? entregadocumento.getSerie() : "");
			bean.setNumero(entregadocumento.getNumero());
			bean.setCodigoEmitente(fornecedor.getIdentificador());
			
			if (fornecedor.getListaEndereco()!=null && !fornecedor.getListaEndereco().isEmpty()){
				List<Endereco> listaEndereco = new ListSet<Endereco>(Endereco.class, fornecedor.getListaEndereco());
				if (listaEndereco.get(0).getMunicipio()!=null && listaEndereco.get(0).getMunicipio().getUf()!=null){
					bean.setUfOrigem(listaEndereco.get(0).getMunicipio().getUf().getSigla());
				}
			}
			
			Money valorContabil = new Money();
			if (entregamaterial.getValorunitario() != null){
				valorContabil = new Money(entregamaterial.getValorunitario() * (entregamaterial.getQtde() != null ? entregamaterial.getQtde() : 1.0));
				
				if(entregamaterial.getValoricmsst() != null){
					valorContabil = valorContabil.add(entregamaterial.getValoricmsst());
				}
				if(entregamaterial.getValoripi() != null){
					valorContabil = valorContabil.add(entregamaterial.getValoripi());
				}
				if(entregamaterial.getValoroutrasdespesas() != null){
					valorContabil = valorContabil.add(entregamaterial.getValoroutrasdespesas());
				}
				if(entregamaterial.getValordesconto() != null){
					valorContabil = valorContabil.subtract(entregamaterial.getValordesconto());
				}
				if(entregamaterial.getValorfrete() != null){
					valorContabil = valorContabil.add(entregamaterial.getValorfrete());
				}
				if(entregamaterial.getValorseguro() != null){
					valorContabil = valorContabil.add(entregamaterial.getValorseguro());
				}
			}
			bean.setValorContabil(valorContabil);
			
			
			String fornecedorStr = fornecedor.getNome();
			if (fornecedor.getRazaosocial()!=null && !fornecedor.getRazaosocial().trim().equals("")){
				fornecedorStr = fornecedor.getRazaosocial();
			}
			if (fornecedor.getCnpj()!=null && fornecedor.getCnpj().getValue()!=null){
				fornecedorStr += " - ";
				fornecedorStr += fornecedor.getCnpj().toString();
			}
			if (fornecedor.getInscricaoestadual()!=null && !fornecedor.getInscricaoestadual().trim().equals("")){
				fornecedorStr += " - ";
				fornecedorStr += fornecedor.getInscricaoestadual();
			}
			
			bean.setFornecedorStr(fornecedorStr);
			bean.setFiscal(entregamaterial.getCfop()!=null ? entregamaterial.getCfop().getCodigo() : null);
			bean.setValorBCIcms(entregamaterial.getValorbcicms()!=null ? entregamaterial.getValorbcicms() : new Money(0));
			bean.setValorBCIcmsst(entregamaterial.getValorbcicmsst()!=null ? entregamaterial.getValorbcicmsst() : new Money(0));
			bean.setAliquotaIcms(entregamaterial.getIcms()!=null ? entregamaterial.getIcms() : 0D);
			bean.setValorCreditadoIcms(entregamaterial.getValoricms()!=null ? entregamaterial.getValoricms() : new Money(0));
			bean.setValorCreditadoIcmsst(entregamaterial.getValoricmsst()!=null ? entregamaterial.getValoricmsst(): new Money(0));
			bean.setValorBCIpi(entregamaterial.getValorbcipi()!=null ? entregamaterial.getValorbcipi() : new Money(0));
			bean.setAliquotaIpi(entregamaterial.getIpi()!=null ? entregamaterial.getIpi() : 0d);
			bean.setValorCreditadoIpi(entregamaterial.getValoripi()!=null ? entregamaterial.getValoripi(): new Money(0));

			if (entregamaterial.getValorFiscalIcms()!=null){
				bean.setCodigoValorFiscalIcms( entregamaterial.getValorFiscalIcms().getCodigo() );
			} else {
				if(Tipocobrancaicms.NAO_TRIBUTADA.equals(entregamaterial.getCsticms()) || Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(entregamaterial.getCsticms()) || 
						Tipocobrancaicms.ISENTA.equals(entregamaterial.getCsticms()) || Tipocobrancaicms.IMUNE.equals(entregamaterial.getCsticms()) || 
						Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(entregamaterial.getCsticms())){
					bean.setCodigoValorFiscalIcms(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.getCodigo());
				} else {
					bean.setCodigoValorFiscalIcms(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.getCodigo());
				}
			}
			if (entregamaterial.getValorFiscalIpi()!=null){
				bean.setCodigoValorFiscalIpi( entregamaterial.getValorFiscalIpi().getCodigo() );
			} else {
				if(Tipocobrancaipi.SAIDA_NAO_TRIBUTADA.equals(entregamaterial.getCstipi()) ||  
						Tipocobrancaipi.SAIDA_ISENTA.equals(entregamaterial.getCstipi()) || Tipocobrancaipi.SAIDA_IMUNE.equals(entregamaterial.getCstipi())){
					bean.setCodigoValorFiscalIpi(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.getCodigo());
				} else {
					bean.setCodigoValorFiscalIpi(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.getCodigo());
				}
			}
			
			//Se o relat�rio for P1/A devo duplicar o objeto para representar o IPI e o ICMS
			if (filtro.getModelo().equals(ModeloRegistroEntrada.MODELO_P1_A)){
				if (bean.getValorBCIcms().getValue().compareTo(BigDecimal.ZERO) <= 0 && bean.getValorBCIpi().getValue().compareTo(BigDecimal.ZERO) <= 0){
					bean.setImposto("ICMS");//imposto padr�o quando a base de c�lculo � ZERO para os dois
				} else if (bean.getValorBCIcms().getValue().compareTo(BigDecimal.ZERO) > 0 && bean.getValorBCIpi().getValue().compareTo(BigDecimal.ZERO) <= 0){
					bean.setImposto("ICMS");
				} else if (bean.getValorBCIcms().getValue().compareTo(BigDecimal.ZERO) <= 0 && bean.getValorBCIpi().getValue().compareTo(BigDecimal.ZERO) > 0){
					bean.setImposto("IPI");
				} else {
//					Entradafiscal beanIpi = new Entradafiscal();
//					BeanUtils.copyProperties(beanIpi, bean);
					Entradafiscal beanIpi = bean.copiaEntradafiscal();
					beanIpi.setValorBCIcms(new Money(0.0));
					beanIpi.setValorBCIcmsst(new Money(0.0));
					beanIpi.setValorCreditadoIcms(new Money(0.0));
					beanIpi.setValorCreditadoIcmsst(new Money(0.0));
					beanIpi.setAliquotaIcms(0.0);
					beanIpi.setImposto("IPI");
					beanIpi.setValorContabil(new Money(0));
					
					if (entregamaterial.getValorFiscalIpi()!=null){
						beanIpi.setCodigoValorFiscalIpi( entregamaterial.getValorFiscalIpi().getCodigo() );
					} else {
						if(Tipocobrancaipi.SAIDA_NAO_TRIBUTADA.equals(entregamaterial.getCstipi()) ||  
								Tipocobrancaipi.SAIDA_ISENTA.equals(entregamaterial.getCstipi()) || Tipocobrancaipi.SAIDA_IMUNE.equals(entregamaterial.getCstipi())){
							beanIpi.setCodigoValorFiscalIpi(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.getCodigo());
						} else {
							beanIpi.setCodigoValorFiscalIpi(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.getCodigo());
						}
					}
					bean.setValorBCIpi(new Money(0.0));
					bean.setValorCreditadoIpi(new Money(0.0));
					bean.setAliquotaIpi(0.0);
					bean.setImposto("ICMS");
					
					Entradafiscal beanAux = mapa.get(beanIpi.getKey());
					if (beanAux==null){
						mapa.put(beanIpi.getKey(), beanIpi);
					}else {
						beanAux.somarCampos(beanIpi);
						mapa.put(beanIpi.getKey(), beanAux);	
					}	
				}
			}
			
			if (filtro.getDiscriminarST() != null && filtro.getDiscriminarST() && 
					BigDecimal.ZERO.compareTo(bean.getValorCreditadoIcmsst().getValue()) < 0){
				bean.setObservacao((bean.getObservacao() != null ? bean.getObservacao() : "") + " ICMS ST: " + bean.getValorCreditadoIcmsst());
			}
			
			Entradafiscal beanAux = mapa.get(bean.getKey());
			if (beanAux==null){
				mapa.put(bean.getKey(), bean);
			}else {
				beanAux.somarCampos(bean);
				mapa.put(bean.getKey(), beanAux);	
			}			
		}
		
		List<Entradafiscal> listaRelatorio = new ArrayList<Entradafiscal>(mapa.values());
		
		Collections.sort(listaRelatorio, new Comparator<Entradafiscal>() {
			@Override
			public int compare(Entradafiscal o1, Entradafiscal o2) {
				
				if (o1.getDtentrada() != null && o2.getDtentrada() == null)
					return -1;
				else if (o1.getDtentrada() == null && o2.getDtentrada() != null)
					return 1;
				else if (o1.getDtentrada() != null && o2.getDtentrada() != null){
					if (o1.getDtentrada().compareTo(o2.getDtentrada()) != 0)
						return o1.getDtentrada().compareTo(o2.getDtentrada());
	
					if (o1.getDtentrada().compareTo(o2.getDtentrada()) != 0)
						return o1.getDtentrada().compareTo(o2.getDtentrada());
				}

				if (o1.getCdentregadocumento().compareTo(o2.getCdentregadocumento()) == 0){
					if (o1.getImposto() != null && o2.getImposto() == null)
						return -1;
					else if (o1.getImposto() == null && o2.getImposto() != null)
						return 1;
					else if (o1.getImposto() != null && o2.getImposto() != null){
						if (o1.getImposto().compareTo(o2.getImposto()) != 0)
							return o1.getImposto().compareTo(o2.getImposto());
						
						if (o1.getImposto().compareTo(o2.getImposto()) != 0)
							return o1.getImposto().compareTo(o2.getImposto());
					}
				}
				
				return o1.getCdentregadocumento().compareTo(o2.getCdentregadocumento());
			}
		});
		
		Report report;
		
		if (filtro.getModelo().equals(ModeloRegistroEntrada.MODELO_P1))
			report = new Report("/fiscal/livroregistroentrada_p1");
		else
			report = new Report("/fiscal/livroregistroentrada_p1_a");
		
		configurarParametros(request, filtro, report);
		Empresa empresa = this.getEmpresa(filtro);
		
		report.addParameter("CNPJ", empresa.getCnpj() != null ? empresa.getCnpj().toString() : "");
		report.addParameter("INSCRICAOESTADUAL", empresa.getInscricaoestadual());
		report.addParameter("PERIODOENTRADA", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
		report.setDataSource(listaRelatorio);
		
		return report;
	}
	

}
