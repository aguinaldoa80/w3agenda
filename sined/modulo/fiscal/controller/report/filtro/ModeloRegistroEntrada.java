package br.com.linkcom.sined.modulo.fiscal.controller.report.filtro;

public enum ModeloRegistroEntrada {

	MODELO_P1("Modelo P1"), MODELO_P1_A("Modelo P1/A");
	
	private String descricao;
	
	private ModeloRegistroEntrada(String descricao){

		this.descricao = descricao;
	}
	
	@Override
	public String toString() {

		return descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
