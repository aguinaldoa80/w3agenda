package br.com.linkcom.sined.modulo.fiscal.controller.report.filtro;

public enum ModeloRegistroSaida {

	MODELO_P2("Modelo P2"), MODELO_P2_A("Modelo P2/A");
	
	private String descricao;
	
	private ModeloRegistroSaida(String descricao){

		this.descricao = descricao;
	}
	
	@Override
	public String toString() {

		return descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
