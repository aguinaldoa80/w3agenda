package br.com.linkcom.sined.modulo.fiscal.controller.report.filtro;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.SinedDateUtils;

public class LivroregistrosaidaFiltro {

	protected Empresa empresa;
	protected Date dtinicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtfim = SinedDateUtils.lastDateOfMonth();
	protected Boolean discriminarST = Boolean.TRUE;
	protected ModeloRegistroSaida modelo = ModeloRegistroSaida.MODELO_P2_A;

	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("Data In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}

	@Required
	@DisplayName("Data Fim")
	public Date getDtfim() {
		return dtfim;
	}
	
	@Required
	@DisplayName("Modelo")
	public ModeloRegistroSaida getModelo() {
		return modelo;
	}

	@DisplayName("Discriminar ST no campo Observa��o")
	public Boolean getDiscriminarST() {
		return discriminarST;
	}

	public void setDiscriminarST(Boolean entrada) {
		this.discriminarST = entrada;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	public void setModelo(ModeloRegistroSaida modelo) {
		this.modelo = modelo;
	}

}
