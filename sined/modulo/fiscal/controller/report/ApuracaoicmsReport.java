package br.com.linkcom.sined.modulo.fiscal.controller.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Ajusteicms;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.geral.service.AjusteicmsService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoicmsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.Aux_apuracaoicms;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/fiscal/relatorio/ApuracaoicmsReport", authorizationModule=ReportAuthorizationModule.class)
public class ApuracaoicmsReport extends SinedReport<ApuracaoicmsFiltro>{
	
	private EntradafiscalService entradafiscalService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private AjusteicmsService ajusteicmsService;
	
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setAjusteicmsService(AjusteicmsService ajusteicmsService) {
		this.ajusteicmsService = ajusteicmsService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	ApuracaoicmsFiltro filtro) throws Exception {
		return this.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "apuracaoicms";
	}
	
	@Override
	public String getTitulo(ApuracaoicmsFiltro filtro) {
		return "APURA��O DE ICMS";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ApuracaoicmsFiltro filtro) {
		Empresa empresa = getEmpresaService().load(filtro.getEmpresa(), "empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.inscricaoestadual, empresa.cpf, empresa.cnpj");
		report.addParameter("EMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(empresa));
		report.addParameter("CNPJEMPRESA", empresa.getCnpj() != null ?  empresa.getCnpj().toString() : null);
		report.addParameter("INSCRICAOESTADUAL", empresa.getInscricaoestadual() != null ? empresa.getInscricaoestadual() : "");
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}
	
	/**
	 * M�todo que gera o relat�rio de apura��o de icms
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private IReport gerarRelatorio(ApuracaoicmsFiltro filtro) {		
		List<Aux_apuracaoicms> listaApuracaoicmsEntrada = new ArrayList<Aux_apuracaoicms>();
		List<Aux_apuracaoicms> listaApuracaoicmsSaida = new ArrayList<Aux_apuracaoicms>();
		
		List<Entregadocumento> listaEntregadocumento = null;
		List<Notafiscalproduto> listaNotafiscalprodutoEntrada = null;
		if(filtro.getEntrada() != null && filtro.getEntrada()){
			listaEntregadocumento = entradafiscalService.findForApuracaoicms(filtro);
			listaNotafiscalprodutoEntrada = notafiscalprodutoService.findForApuracaoicms(filtro, Tipooperacaonota.ENTRADA); 
		}
		
		List<Notafiscalproduto> listaNotafiscalprodutoSaida = null;
		if(filtro.getSaida() != null && filtro.getSaida()){
			listaNotafiscalprodutoSaida = notafiscalprodutoService.findForApuracaoicms(filtro, Tipooperacaonota.SAIDA);
		}
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					if(em.getCfop() != null){
						if(em.getValorFiscalIcms() != null){
							Aux_apuracaoicms auxApuracaoicms = new Aux_apuracaoicms(Aux_apuracaoicms.ENTRADAICMS);
							auxApuracaoicms.setCfop(em.getCfop());
							
							ValorFiscal valorFiscalIpi = em.getValorFiscalIpi();
							if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi) && em.getValoripi().getValue().doubleValue() <= 0){
								valorFiscalIpi = ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS;
							}
							
							Double valortotaloperacaoIcms = em.getValortotaloperacao(!ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi));
							if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(em.getValorFiscalIcms())){
								auxApuracaoicms.setBc(em.getValorbcicms() != null ? em.getValorbcicms() : null);
								auxApuracaoicms.setImpostocreditado(em.getValoricms() != null ? em.getValoricms() : null);
								
								Double valorbcicms = em.getValorbcicms() != null ? em.getValorbcicms().getValue().doubleValue() : 0d;
								Double valor = valortotaloperacaoIcms != null ? valortotaloperacaoIcms : 0d;
								double valoroutrasicms = valor - valorbcicms;
								if(valoroutrasicms > 0){
									auxApuracaoicms.setOutras(new Money(valoroutrasicms));
								}
							}
							if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(em.getValorFiscalIcms())){
								auxApuracaoicms.setIsentanaotributada(new Money(valortotaloperacaoIcms));
							}
							if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(em.getValorFiscalIcms())){
								auxApuracaoicms.setOutras(new Money(valortotaloperacaoIcms));
							}
							
							if (em.getValorbcicms() != null && em.getValorbcicms().getValue().doubleValue() > 0) {
								auxApuracaoicms.setValortotal(em.getValorbcicms());
							} else {
								auxApuracaoicms.setValortotal(new Money(em.getValortotaloperacao()));
							}
							
							this.addAuxApuracaoicms(listaApuracaoicmsEntrada, auxApuracaoicms);
						}
						
						if(em.getValoricmsst() != null && em.getValoricmsst().getValue().doubleValue() > 0){
							Aux_apuracaoicms auxApuracaoicmsst = new Aux_apuracaoicms(Aux_apuracaoicms.ENTRADAICMSST);
							auxApuracaoicmsst.setCfop(em.getCfop());
							auxApuracaoicmsst.setBc(em.getValorbcicmsst());
							auxApuracaoicmsst.setImpostocreditado(em.getValoricmsst());
							auxApuracaoicmsst.setValortotal(em.getValorbcicmsst());
							this.addAuxApuracaoicms(listaApuracaoicmsEntrada, auxApuracaoicmsst);
						}
					}
				}
			}
		}
		
		if(listaNotafiscalprodutoEntrada != null && !listaNotafiscalprodutoEntrada.isEmpty()){
			for(Notafiscalproduto nfp : listaNotafiscalprodutoEntrada){
				for(Notafiscalprodutoitem item : nfp.getListaItens()){
					this.makeListaApuracaoByNotafiscalproduto(listaApuracaoicmsEntrada, item, Tipooperacaonota.ENTRADA);
				}
			}
		}
		
		if(listaNotafiscalprodutoSaida != null && !listaNotafiscalprodutoSaida.isEmpty()){
			for(Notafiscalproduto nfp : listaNotafiscalprodutoSaida){
				for(Notafiscalprodutoitem item : nfp.getListaItens()){
					this.makeListaApuracaoByNotafiscalproduto(listaApuracaoicmsSaida, item, Tipooperacaonota.SAIDA);
				}
			}
		}
		
		Collections.sort(listaApuracaoicmsEntrada,new Comparator<Aux_apuracaoicms>(){
			public int compare(Aux_apuracaoicms o1, Aux_apuracaoicms o2) {
				return o1.getCfop().getCodigo().compareTo(o2.getCfop().getCodigo());
			}
		});
		
		Collections.sort(listaApuracaoicmsSaida,new Comparator<Aux_apuracaoicms>(){
			public int compare(Aux_apuracaoicms o1, Aux_apuracaoicms o2) {
				return o1.getCfop().getCodigo().compareTo(o2.getCfop().getCodigo());
			}
		});
		
		Report report = new Report("/fiscal/apuracaoicms");
		Report reportEntrada = new Report("/fiscal/apuracaoicmsentrada_sub");
		Report reportSaida = new Report("/fiscal/apuracaoicmssaida_sub");
		
		Money somaimpostodebitado = new Money();
		Money somaoutrosdebitos = new Money();
		Money subtotaldebitos = new Money();
		Money somaimpostocreditado = new Money();
		Money somaoutroscreditos = new Money();
		Money subtotalcreditos = new Money();
		Money saldoimposto = new Money();
		
		this.criaParametrosicms(reportEntrada, listaApuracaoicmsEntrada, Aux_apuracaoicms.ENTRADA);
		this.criaParametrosicms(reportSaida, listaApuracaoicmsSaida, Aux_apuracaoicms.SAIDA);
		
		for(Aux_apuracaoicms apuracaoicms : listaApuracaoicmsEntrada){
			if (apuracaoicms.getTipo().equals(Aux_apuracaoicms.ENTRADAICMS)){
				somaimpostocreditado = somaimpostocreditado.add(apuracaoicms.getImpostocreditado());
			}
		}
		for(Aux_apuracaoicms apuracaoicms : listaApuracaoicmsSaida){
			if (apuracaoicms.getTipo().equals(Aux_apuracaoicms.SAIDAICMS)){
				somaimpostodebitado = somaimpostodebitado.add(apuracaoicms.getImpostodebitado());
			}
		}
		List<Ajusteicms> listaAjusteicmsDebito = ajusteicmsService.findForApuracaoICMSDebito(filtro);
		if (SinedUtil.isListNotEmpty(listaAjusteicmsDebito)) {
			Double somaoutrosdebitosTotal = new Double(0);
			for (Ajusteicms ajusteicms : listaAjusteicmsDebito) {
				somaoutrosdebitosTotal += ajusteicms.getValor();
			}
			somaoutrosdebitos = new Money(somaoutrosdebitosTotal);
		}
		
		List<Ajusteicms> listaAjusteicmsCredito = ajusteicmsService.findForApuracaoICMSCredito(filtro);
		if (SinedUtil.isListNotEmpty(listaAjusteicmsCredito)) {
			Double somaoutroscreditosTotal = new Double(0);
			for (Ajusteicms ajusteicms : listaAjusteicmsCredito) {
				somaoutroscreditosTotal += ajusteicms.getValor();
			}
			somaoutroscreditos = new Money(somaoutroscreditosTotal);
		}
		
		subtotaldebitos = somaimpostodebitado.add(somaoutrosdebitos);
		subtotalcreditos = somaimpostocreditado.add(somaoutroscreditos);
		saldoimposto = subtotalcreditos.subtract(subtotaldebitos);
		
		report.addSubReport("SUB_APURACAOICMSENTRADA", reportEntrada);
		report.addSubReport("SUB_APURACAOICMSSAIDA", reportSaida);
		
		report.addParameter("LISTAAPURACAOICMSENTRADA", listaApuracaoicmsEntrada);
		report.addParameter("LISTAAPURACAOICMSSAIDA", listaApuracaoicmsSaida);
		
		report.addParameter("SOMAIMPOSTODEBITADO", somaimpostodebitado);
		report.addParameter("SOMAOUTROSDEBITOS", somaoutrosdebitos);
		report.addParameter("SUBTOTALDEBITOS", subtotaldebitos);
		report.addParameter("SOMAIMPOSTOCREDITADO", somaimpostocreditado);
		report.addParameter("SOMAOUTROSCREDITOS", somaoutroscreditos);
		report.addParameter("SUBTOTALCREDITOS", subtotalcreditos);
		report.addParameter("SALDOIMPOSTO", saldoimposto);
		
		return report;
	}
	
	/**
	 * M�todo que faz a inclus��o da nota na apura��o de ICMS
	 *
	 * @param listaApuracaoicms
	 * @param item
	 * @param tipooperacaonota
	 * @author Rodrigo Freitas
	 * @since 04/08/2014
	 */
	private void makeListaApuracaoByNotafiscalproduto(List<Aux_apuracaoicms> listaApuracaoicms, Notafiscalprodutoitem item, Tipooperacaonota tipooperacaonota) {
		String tipoicms = Aux_apuracaoicms.SAIDAICMS;
		String tipoicmsst = Aux_apuracaoicms.SAIDAICMSST;
		boolean entrada = tipooperacaonota.equals(Tipooperacaonota.ENTRADA);
		if(entrada){
			tipoicms = Aux_apuracaoicms.ENTRADAICMS;
			tipoicmsst = Aux_apuracaoicms.ENTRADAICMSST;
		}
		
		if(item.getCfop() != null){
			if(item.getValorFiscalIcms() != null){
				Aux_apuracaoicms auxApuracaoicms = new Aux_apuracaoicms(tipoicms);
				auxApuracaoicms.setCfop(item.getCfop());
				
				if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(item.getValorFiscalIcms())){
					auxApuracaoicms.setBc(item.getValorbcicms() != null ? item.getValorbcicms() : null);
					if(entrada){
						auxApuracaoicms.setImpostocreditado(item.getValoricms() != null ? item.getValoricms() : null);
					} else {
						auxApuracaoicms.setImpostodebitado(item.getValoricms() != null ? item.getValoricms() : null);
					}
					
					Double valorbcicms = item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : 0d;
					Double valor = item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0d;
					double valoroutrasicms = valor - valorbcicms;
					if(valoroutrasicms > 0){
						auxApuracaoicms.setOutras(new Money(valoroutrasicms));
					}
				}
				if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(item.getValorFiscalIcms())){
					auxApuracaoicms.setIsentanaotributada(item.getValorbruto());
				}
				if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(item.getValorFiscalIcms())){
					auxApuracaoicms.setOutras(item.getValorbruto());
				}
				
				if (item.getValorbcicms() != null && item.getValorbcicms().getValue().doubleValue() > 0) {
					auxApuracaoicms.setValortotal(item.getValorbcicms());
				} else {
					auxApuracaoicms.setValortotal(item.getValorbruto());
				}
				
				this.addAuxApuracaoicms(listaApuracaoicms, auxApuracaoicms);
			}
			
			if(item.getValoricmsst() != null && item.getValoricmsst().getValue().doubleValue() > 0){
				Aux_apuracaoicms auxApuracaoicmsst = new Aux_apuracaoicms(tipoicmsst);
				auxApuracaoicmsst.setCfop(item.getCfop());
				auxApuracaoicmsst.setBc(item.getValorbcicmsst());
				auxApuracaoicmsst.setImpostocreditado(item.getValoricmsst());
				auxApuracaoicmsst.setValortotal(item.getValorbcicmsst());
				this.addAuxApuracaoicms(listaApuracaoicms, auxApuracaoicmsst);
			}
		}
	}
	
	/**
	 * M�todo que cria os parametros do sub-relat�rio de entrada e saida 
	 *
	 * @param report
	 * @param listaApuracaoicms
	 * @param tipo
	 * @author Luiz Fernando
	 */
	private void criaParametrosicms(Report report, List<Aux_apuracaoicms> listaApuracaoicms, String tipo) {
		
		Money valortotalcontabilestado = new Money();
		Money valortotalcontabiloutrosestados = new Money();
		Money valortotalcontabilexterior = new Money();
		Money valortotalcontabil = new Money();
		
		Money valortotalbcestado = new Money();
		Money valortotalbcoutrosestados = new Money();
		Money valortotalbcexterior = new Money();
		Money valortotalbc = new Money();
		
		Money valortotalicestado = new Money();
		Money valortotalicoutrosestados = new Money();
		Money valortotalicexterior = new Money();
		Money valortotalic = new Money();
		
		Money valortotalidestado = new Money();
		Money valortotalidoutrosestados = new Money();
		Money valortotalidexterior = new Money();
		Money valortotalid = new Money();
		
		Money valortotalintestado = new Money();
		Money valortotalintoutrosestados = new Money();
		Money valortotalintexterior = new Money();
		Money valortotalint = new Money();
		
		Money valortotaloutrasestado = new Money();
		Money valortotaloutrasoutrosestados = new Money();
		Money valortotaloutrasexterior = new Money();
		Money valortotaloutras = new Money();
		
		Integer qtdeestado = 0;
		Integer qtdeoutrosestados = 0;
		Integer qtdeexterior = 0;
		
		String tipoentradasaida = "";
		for(Aux_apuracaoicms auxApuracaoicms : listaApuracaoicms){
			if(Aux_apuracaoicms.ENTRADAICMSST.equals(auxApuracaoicms.getTipo()) 
					|| Aux_apuracaoicms.SAIDAICMSST.equals(auxApuracaoicms.getTipo())){
				continue;//Ignorando ICMSST
			}

			if(Aux_apuracaoicms.ENTRADAICMS.equals(auxApuracaoicms.getTipo()) ){
				tipoentradasaida = Aux_apuracaoicms.ENTRADA;
			}else if(Aux_apuracaoicms.SAIDAICMS.equals(auxApuracaoicms.getTipo())){
				tipoentradasaida = Aux_apuracaoicms.SAIDA;
			}
			if(tipo.equals(tipoentradasaida)){				
				if(Cfopescopo.ESTADUAL.getCdcfopescopo().equals(auxApuracaoicms.getCfop().getCfopescopo().getCdcfopescopo())){
					valortotalbcestado = valortotalbcestado.add(auxApuracaoicms.getBc());
					if(tipo.equals(Aux_apuracaoicms.ENTRADA)){
						valortotalicestado = valortotalicestado.add(auxApuracaoicms.getImpostocreditado());
					}else if(tipo.equals(Aux_apuracaoicms.SAIDA)){
						valortotalidestado = valortotalidestado.add(auxApuracaoicms.getImpostodebitado());
					}
					valortotalintestado = valortotalintestado.add(auxApuracaoicms.getIsentanaotributada());
					valortotaloutrasestado = valortotaloutrasestado.add(auxApuracaoicms.getOutras());
					valortotalcontabilestado = valortotalcontabilestado.add(auxApuracaoicms.getValortotal());
					qtdeestado++;
				}else if(Cfopescopo.FORA_DO_ESTADO.getCdcfopescopo().equals(auxApuracaoicms.getCfop().getCfopescopo().getCdcfopescopo())){
					valortotalbcoutrosestados = valortotalbcoutrosestados.add(auxApuracaoicms.getBc());
					if(tipo.equals(Aux_apuracaoicms.ENTRADA)){
						valortotalicoutrosestados = valortotalicoutrosestados.add(auxApuracaoicms.getImpostocreditado());
					}else if(tipo.equals(Aux_apuracaoicms.SAIDA)){
						valortotalidoutrosestados = valortotalidoutrosestados.add(auxApuracaoicms.getImpostodebitado());
					}					
					valortotalintoutrosestados = valortotalintoutrosestados.add(auxApuracaoicms.getIsentanaotributada());
					valortotaloutrasoutrosestados = valortotaloutrasoutrosestados.add(auxApuracaoicms.getOutras());
					valortotalcontabiloutrosestados = valortotalcontabiloutrosestados.add(auxApuracaoicms.getValortotal());
					qtdeoutrosestados++;
				}else if(Cfopescopo.INTERNACIONAL.getCdcfopescopo().equals(auxApuracaoicms.getCfop().getCfopescopo().getCdcfopescopo())){
					valortotalbcestado = valortotalbcestado.add(auxApuracaoicms.getBc());
					if(tipo.equals(Aux_apuracaoicms.ENTRADA)){
						valortotalicexterior = valortotalicexterior.add(auxApuracaoicms.getImpostocreditado());
					}else if(tipo.equals(Aux_apuracaoicms.SAIDA)){
						valortotalidexterior = valortotalidexterior.add(auxApuracaoicms.getImpostodebitado());
					}
					valortotalintexterior = valortotalintexterior.add(auxApuracaoicms.getIsentanaotributada());
					valortotaloutrasexterior = valortotaloutrasexterior.add(auxApuracaoicms.getOutras());
					valortotalcontabilexterior = valortotalcontabilexterior.add(auxApuracaoicms.getValortotal());
					qtdeexterior++;
				}
			}
		}

		valortotalbc = valortotalbcestado.add(valortotalbcoutrosestados).add(valortotalbcexterior);
		if(tipo.equals(Aux_apuracaoicms.ENTRADA)){
			valortotalic = valortotalicestado.add(valortotalicoutrosestados).add(valortotalicexterior);
		}else if(tipo.equals(Aux_apuracaoicms.SAIDA)){
			valortotalid = valortotalidestado.add(valortotalidoutrosestados).add(valortotalidexterior);
		}		
		valortotalint = valortotalintestado.add(valortotalintoutrosestados).add(valortotalintexterior);
		valortotaloutras = valortotaloutrasestado.add(valortotaloutrasoutrosestados).add(valortotaloutrasexterior);
		valortotalcontabil = valortotalcontabilestado.add(valortotalcontabiloutrosestados).add(valortotalcontabilexterior);
		
		//VALORES CONTABEIS
		report.addParameter("VALORTOTALCONTABILESTADO" + tipo.toUpperCase(), valortotalcontabilestado);
		report.addParameter("VALORTOTALCONTABILOUTROSESTADOS"+ tipo.toUpperCase(), valortotalcontabiloutrosestados);
		report.addParameter("VALORTOTALCONTABILEXTERIOR"+ tipo.toUpperCase(), valortotalcontabilexterior);
		
		//VALORES BC
		report.addParameter("VALORTOTALBCESTADO"+ tipo.toUpperCase(), valortotalbcestado);
		report.addParameter("VALORTOTALBCOUTROSESTADOS"+ tipo.toUpperCase(), valortotalbcoutrosestados);
		report.addParameter("VALORTOTALBCEXTERIOR"+ tipo.toUpperCase(), valortotalbcexterior);
		
		//VALORES IMPOSTO CREDITADO/DEBITADO
		if(tipo.equals(Aux_apuracaoicms.ENTRADA)){
			report.addParameter("VALORTOTALICESTADO"+ tipo.toUpperCase(), valortotalicestado);
			report.addParameter("VALORTOTALICOUTROSESTADOS"+ tipo.toUpperCase(), valortotalicoutrosestados);
			report.addParameter("VALORTOTALICEXTERIOR"+ tipo.toUpperCase(), valortotalicexterior);
		}else if(tipo.equals(Aux_apuracaoicms.SAIDA)){
			report.addParameter("VALORTOTALIDESTADO"+ tipo.toUpperCase(), valortotalidestado);
			report.addParameter("VALORTOTALIDOUTROSESTADOS"+ tipo.toUpperCase(), valortotalidoutrosestados);
			report.addParameter("VALORTOTALIDEXTERIOR"+ tipo.toUpperCase(), valortotalidexterior);
		}
		
		//VALORES ISENTAS OU N�O TRIBUTADAS
		report.addParameter("VALORTOTALINTESTADO"+ tipo.toUpperCase(), valortotalintestado);
		report.addParameter("VALORTOTALINTOUTROSESTADOS"+ tipo.toUpperCase(), valortotalintoutrosestados);
		report.addParameter("VALORTOTALINTEXTERIOR"+ tipo.toUpperCase(), valortotalintexterior);
		
		//VALORES OUTRAS
		report.addParameter("VALORTOTALOUTRASESTADO"+ tipo.toUpperCase(), valortotaloutrasestado);
		report.addParameter("VALORTOTALOUTRASOUTROSESTADOS"+ tipo.toUpperCase(), valortotaloutrasoutrosestados);
		report.addParameter("VALORTOTALOUTRASEXTERIOR"+ tipo.toUpperCase(), valortotaloutrasexterior);
		
		
		//TOTAIS
		report.addParameter("VALORTOTALCONTABIL"+ tipo.toUpperCase(), valortotalcontabil);
		report.addParameter("VALORTOTALBC"+ tipo.toUpperCase(), valortotalbc);
		if(tipo.equals(Aux_apuracaoicms.ENTRADA)){
			report.addParameter("VALORTOTALIC"+ tipo.toUpperCase(), valortotalic);
		}else if(tipo.equals(Aux_apuracaoicms.SAIDA)){
			report.addParameter("VALORTOTALID"+ tipo.toUpperCase(), valortotalid);
		}
		report.addParameter("VALORTOTALINT"+ tipo.toUpperCase(), valortotalint);
		report.addParameter("VALORTOTALOUTRAS"+ tipo.toUpperCase(), valortotaloutras);
		
		report.addParameter("QTDEESTADOS"+ tipo.toUpperCase(), qtdeestado);
		report.addParameter("QTDEOUTROSESTADOS"+ tipo.toUpperCase(), qtdeoutrosestados);
		report.addParameter("QTDEEXTERIOR"+ tipo.toUpperCase(), qtdeexterior);
	}
	
	/**
	 * M�todo adiciona o cfop a lista - Se j� existe, soma-se os valores,  caso contr�rio, adiciona a lista
	 *
	 * @param listaApuracaoicms
	 * @param auxApuracaoicms
	 * @author Luiz Fernando
	 */
	private void addAuxApuracaoicms(List<Aux_apuracaoicms> listaApuracaoicms, Aux_apuracaoicms auxApuracaoicms) {
		boolean existe = false;
		if(listaApuracaoicms.size() > 0){
			for(Aux_apuracaoicms apuracaoicms : listaApuracaoicms){
				if(auxApuracaoicms.getTipo().equals(apuracaoicms.getTipo())){
					if(auxApuracaoicms.getCfop() != null){
						if(apuracaoicms.getCfop() != null && apuracaoicms.getCfop().equals(auxApuracaoicms.getCfop())){
							existe = true;
						}
					}else {
						if(apuracaoicms.getCfop() == null){
							existe = true;
						}
					}
					if(existe){
						apuracaoicms.setBc(apuracaoicms.getBc().add(auxApuracaoicms.getBc()));
						apuracaoicms.setImpostocreditado(apuracaoicms.getImpostocreditado().add(auxApuracaoicms.getImpostocreditado()));
						apuracaoicms.setImpostodebitado(apuracaoicms.getImpostodebitado().add(auxApuracaoicms.getImpostodebitado()));
						apuracaoicms.setIsentanaotributada(apuracaoicms.getIsentanaotributada().add(auxApuracaoicms.getIsentanaotributada()));
						apuracaoicms.setOutras(apuracaoicms.getOutras().add(auxApuracaoicms.getOutras()));
						apuracaoicms.setValortotal(apuracaoicms.getValortotal().add(auxApuracaoicms.getValortotal()));
						break;
					}
				}
			}
		}		
		if(!existe){
			listaApuracaoicms.add(auxApuracaoicms);
		}
	}	
}
