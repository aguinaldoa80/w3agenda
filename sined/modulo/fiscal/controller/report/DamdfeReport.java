package br.com.linkcom.sined.modulo.fiscal.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.geral.service.MdfeService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path = "/fiscal/relatorio/Damdfe", authorizationModule=ReportAuthorizationModule.class)
public class DamdfeReport extends ResourceSenderController<FiltroListagemSined> {

	private MdfeService mdfeService;
	
	public void setMdfeService(MdfeService mdfeService) {
		this.mdfeService = mdfeService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		if(mdfeService.haveMDFeDiferenteStatus(itensSelecionados, MdfeSituacao.AUTORIZADO, MdfeSituacao.EMITIDO, MdfeSituacao.ENCERRADO, MdfeSituacao.CANCELADO)) {
			request.addError("Para impress�o do DAMDF-e as notas tem que estar com a situa��o 'MDFe Emitida', 'MDFe Autorizada', 'MDFe Encerrado' ou 'MDFe Cancelado'.");
			return null;
		}
			
		return mdfeService.gerarDamdfe(itensSelecionados);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined filtro) throws Exception {
		return new ModelAndView("redirect:/fiscal/crud/Mdfe");
	}
}
