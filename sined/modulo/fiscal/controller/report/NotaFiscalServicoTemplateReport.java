package br.com.linkcom.sined.modulo.fiscal.controller.report;

import java.util.LinkedHashMap;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.NotaFiscalEletronicaReportBean;

@Controller(path="/fiscal/relatorio/NotaFiscalServicoTemplate", authorizationModule=ReportAuthorizationModule.class)
public class NotaFiscalServicoTemplateReport extends ReportTemplateController<ReportTemplateFiltro>{

	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_NFSE;
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(
			WebRequestContext request, ReportTemplateFiltro filtro)
			throws Exception {
		NotaFiscalEletronicaReportBean bean = (NotaFiscalEletronicaReportBean)request.getAttribute("bean");
		LinkedHashMap<String, Object> mapa = new LinkedHashMap<String, Object>();
		
		mapa.put("bean", bean);
		
		return mapa;
	}

	@Override
	protected String getFileName() {
		return "nota_fiscal_servico";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,
			ReportTemplateFiltro filtro) {
		return getReportTemplateService().load(filtro.getTemplate());
	}
}
