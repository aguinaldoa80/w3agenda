package br.com.linkcom.sined.modulo.fiscal.controller.report;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Aux_apuracaopiscofins;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaopiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.ApuracaoimpostosDetalhadoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/fiscal/relatorio/ApuracaopiscofinsCSVReport", authorizationModule=ReportAuthorizationModule.class)
public class ApuracaopiscofinsCSVReport extends ResourceSenderController<ApuracaopiscofinsFiltro>{
	
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private EntradafiscalService entradafiscalService;
	private ArquivonfnotaService arquivonfnotaService;
	
	public void setArquivonfnotaService(
			ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,ApuracaopiscofinsFiltro filtro) throws Exception {
		return this.gerarRelatorioApuracaopiscofinsCSV(filtro);
	}
	
	private Resource gerarRelatorioApuracaopiscofinsCSV(ApuracaopiscofinsFiltro filtro) {
		List<Entregadocumento> listaEntregadocumento = null;
		if(filtro.getEntrada() != null && filtro.getEntrada()){
			listaEntregadocumento = entradafiscalService.findforApuracaopiscofins(filtro);
		}
		
		List<NotaFiscalServico> listaNotafiscalservico = null;
		List<Notafiscalproduto> listanNotafiscalproduto = null;
		if(filtro.getSaida() != null && filtro.getSaida()){
			listaNotafiscalservico = notaFiscalServicoService.findforApuracaopiscofins(filtro);
			listanNotafiscalproduto = notafiscalprodutoService.findforApuracaopiscofins(filtro);
		}
		
		Money totalreceitafaturamento = new Money();
		
		Money totalbcpis = new Money();
		Money totalpis = new Money();
		Money totalpiscredito = new Money();
		Money totalpisretido = new Money();
		Money totalpisdevido = new Money();
		
		Money totalbccofins = new Money();
		Money totalcofins = new Money();
		Money totalcofinscredito = new Money();
		Money totalcofinsretido = new Money();
		Money totalcofinsdevido = new Money();
		
		Money totalaquisicaobensrevenda = new Money();
		Money totalaquisicaobensinsumos = new Money();
		Money totalaquisicaoservicosutilizado = new Money();
		Money totalenergiaeletrica = new Money();
		Money totalbccreditos = new Money();
		
		Money totalsaldopis = new Money();
		Money totalsaldocofins = new Money();
		
		List<Aux_apuracaopiscofins> listaApuracaopiscofins = this.criaListaApuracaoAgrupadaMesAno(filtro, listaNotafiscalservico, listanNotafiscalproduto, listaEntregadocumento);
		
		StringBuilder csv = new StringBuilder();
		csv.append("Descri��o;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getMes()).append("/").append(auxApuracaopiscofins.getAno()).append(";");
		}
		csv.append("Total;\n");
		csv.append("FATURAMENTO;\n");
		
		csv.append("\nReceita de Faturamento;");
		
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getReceitafaturamento()).append(";");
			totalreceitafaturamento = totalreceitafaturamento.add(auxApuracaopiscofins.getReceitafaturamento());
		}
		csv.append(totalreceitafaturamento);
		csv.append(";\nBase de C�lculo PIS;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getBcpis()).append(";");
			totalbcpis = totalbcpis.add(auxApuracaopiscofins.getBcpis());
		}
		csv.append(totalbcpis);
		csv.append(";\nPIS;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getPis()).append(";");
			totalpis = totalpis.add(auxApuracaopiscofins.getPis());
		}
		csv.append(totalpis);
		csv.append(";\n(-) PIS Retido;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getPisretido()).append(";");
			totalpisretido = totalpisretido.add(auxApuracaopiscofins.getPisretido());
		}
		csv.append(totalpisretido);
		csv.append(";\nPIS DEVIDO;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){	
			csv.append(auxApuracaopiscofins.getPisdevido()).append(";");
			totalpisdevido = totalpisdevido.add(auxApuracaopiscofins.getPisdevido());
		}
		csv.append(totalpisdevido);
		csv.append(";\nBase de C�lculo COFINS;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getBccofins()).append(";");
			totalbccofins = totalbccofins.add(auxApuracaopiscofins.getBccofins());
		}
		csv.append(totalbccofins);
		csv.append(";\nCOFINS;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getCofins()).append(";");
			totalcofins = totalcofins.add(auxApuracaopiscofins.getCofins());
		}
		csv.append(totalcofins);
		csv.append(";\n(-) COFINS Retido;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getCofinsretido()).append(";");
			totalcofinsretido = totalcofinsretido.add(auxApuracaopiscofins.getCofinsretido());
		}
		csv.append(totalcofinsretido);
		csv.append(";\nCOFINS DEVIDO;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getCofinsdevido()).append(";");
			totalcofinsdevido = totalcofinsdevido.add(auxApuracaopiscofins.getCofinsdevido());
		}
		csv.append(totalcofinsdevido);
			
		csv.append(";\nCR�DITO DE PIS/COFINS;\n");
		csv.append("\nAquisi��o de bens para revenda;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getAquisicaobensrevenda()).append(";");
			totalaquisicaobensrevenda = totalaquisicaobensrevenda.add(auxApuracaopiscofins.getAquisicaobensrevenda());
		}
		csv.append(totalaquisicaobensrevenda);
		csv.append(";\nAquisi��o de bens utilizados como insumos;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getAquisicaobensinsumos()).append(";");
			totalaquisicaobensinsumos = totalaquisicaobensinsumos.add(auxApuracaopiscofins.getAquisicaobensinsumos());
		}
		csv.append(totalaquisicaobensinsumos);
		csv.append(";\nAquisi��o de servi�os utilizados;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getAquisicaoservicosutilizado()).append(";");
			totalaquisicaoservicosutilizado = totalaquisicaoservicosutilizado.add(auxApuracaopiscofins.getAquisicaoservicosutilizado());
		}
		csv.append(totalaquisicaoservicosutilizado);
		csv.append(";\nEnergia el�trica;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getEnergiaeletrica()).append(";");
			totalenergiaeletrica = totalenergiaeletrica.add(auxApuracaopiscofins.getEnergiaeletrica());
		}
		csv.append(totalenergiaeletrica);
		csv.append(";\nBase de C�lculo dos Cr�ditos;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getBccreditos()).append(";");
			totalbccreditos = totalbccreditos.add(auxApuracaopiscofins.getBccreditos());
		}
		csv.append(totalbccreditos);
		csv.append(";\nCR�DITO PIS;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getPiscredito()).append(";");
			totalpiscredito = totalpiscredito.add(auxApuracaopiscofins.getPiscredito());
		}
		csv.append(totalpiscredito);
		csv.append(";\nCR�DITO COFINS;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getCofinscredito()).append(";");
			totalcofinscredito = totalcofinscredito.add(auxApuracaopiscofins.getCofinscredito());
		}
		csv.append(totalcofinscredito);
		csv.append(";\nSALDO PIS;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getSaldopis()).append(";");
			totalsaldopis = totalsaldopis.add(auxApuracaopiscofins.getSaldopis());
		}
		csv.append(totalsaldopis);
		csv.append(";\nSALDO COFINS;");
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			csv.append(auxApuracaopiscofins.getSaldocofins()).append(";");
			totalsaldocofins = totalsaldocofins.add(auxApuracaopiscofins.getSaldocofins());
		}
		csv.append(totalsaldocofins);
		Resource resource = new Resource("text/csv", "apuracaocreditopiscofins_" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio()) + "_" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim())  +".csv", csv.toString().getBytes());
		return resource;
	}
	
	private List<Aux_apuracaopiscofins> criaListaApuracaoAgrupadaMesAno(ApuracaopiscofinsFiltro filtro, List<NotaFiscalServico> listaNotafiscalservico, List<Notafiscalproduto> listanNotafiscalproduto, List<Entregadocumento> listaEntregadocumento) {
		String mesinicio = new SimpleDateFormat("MM").format(filtro.getDtinicio());
		String mesfim = new SimpleDateFormat("MM").format(filtro.getDtfim());
		String ano = new SimpleDateFormat("yyyy").format(filtro.getDtfim());
		
		List<Aux_apuracaopiscofins> listaApuracaopiscofins = new ArrayList<Aux_apuracaopiscofins>();
		listaApuracaopiscofins.add(new Aux_apuracaopiscofins(mesinicio, ano));
		
		if(!mesinicio.equals(mesfim)){
			Date dataproximomes = SinedDateUtils.addMesData(filtro.getDtinicio(), 1);
			String proximomes = new SimpleDateFormat("MM").format(dataproximomes);
			ano = new SimpleDateFormat("yyyy").format(dataproximomes);
			while(!proximomes.equals(mesfim)){
				listaApuracaopiscofins.add(new Aux_apuracaopiscofins(proximomes, ano));
				dataproximomes = SinedDateUtils.addMesData(dataproximomes, 1);
				proximomes = new SimpleDateFormat("MM").format(dataproximomes);		
				ano = new SimpleDateFormat("yyyy").format(dataproximomes);
			}
			if(!proximomes.equals(mesinicio)){
				listaApuracaopiscofins.add(new Aux_apuracaopiscofins(proximomes, ano));
			}
			
		}	
		
		String mesatual = "";
		if(listaNotafiscalservico != null && !listaNotafiscalservico.isEmpty()){
			for(NotaFiscalServico nfs : listaNotafiscalservico){
				mesatual = new SimpleDateFormat("MM").format(nfs.getDtEmissao());
				for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
					if(mesatual.equals(auxApuracaopiscofins.getMes())){
						if(nfs.getValorTotalServicos() != null){
							auxApuracaopiscofins.setReceitafaturamento(auxApuracaopiscofins.getReceitafaturamento().add(nfs.getValorNota()));
						}
						
						if(nfs.getBasecalculopis() != null){
							auxApuracaopiscofins.setBcpis(auxApuracaopiscofins.getBcpis().add(nfs.getBasecalculopis())); 
						}else if(nfs.getBasecalculo() != null){
							auxApuracaopiscofins.setBcpis(auxApuracaopiscofins.getBcpis().add(nfs.getBasecalculo()));
						}
						if(nfs.getValorPis() != null){
							auxApuracaopiscofins.setPis(auxApuracaopiscofins.getPis().add(nfs.getValorPis())); 
						}
						if(nfs.getIncidepis() != null && nfs.getIncidepis()){
							auxApuracaopiscofins.setPisretido(auxApuracaopiscofins.getPisretido().add(nfs.getValorPis()));
						}
						
						if(nfs.getBasecalculocofins() != null){
							auxApuracaopiscofins.setBccofins(auxApuracaopiscofins.getBccofins().add(nfs.getBasecalculocofins())); 
						} else if(nfs.getBasecalculo() != null){
							auxApuracaopiscofins.setBccofins(auxApuracaopiscofins.getBccofins().add(nfs.getBasecalculo()));
						}
						
						if(nfs.getValorCofins() != null){
							auxApuracaopiscofins.setCofins(auxApuracaopiscofins.getCofins().add(nfs.getValorCofins())); 
						}
						if(nfs.getIncidecofins() != null && nfs.getIncidecofins()){
							auxApuracaopiscofins.setCofinsretido(auxApuracaopiscofins.getCofinsretido().add(nfs.getValorCofins()));
						}
						break;
					}
				}
			}
		}
		
		if(listanNotafiscalproduto != null && !listanNotafiscalproduto.isEmpty()){
			for(Notafiscalproduto nfp : listanNotafiscalproduto){
				mesatual = new SimpleDateFormat("MM").format(nfp.getDtEmissao());
				for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
					if(mesatual.equals(auxApuracaopiscofins.getMes())){
						if(nfp.getValorprodutos() != null){
							auxApuracaopiscofins.setReceitafaturamento(auxApuracaopiscofins.getReceitafaturamento().add(nfp.getValorprodutos()));
						}
						if(nfp.getListaItens() != null && !nfp.getListaItens().isEmpty()){
							for(Notafiscalprodutoitem item : nfp.getListaItens()){
								if (!Tipocobrancapis.OPERACAO_70.equals(item.getTipocobrancapis())){
									if(item.getValorbcpis() != null){
										auxApuracaopiscofins.setBcpis(auxApuracaopiscofins.getBcpis().add(item.getValorbcpis())); 
									}
								}
								if(item.getValorpis() != null){
									auxApuracaopiscofins.setPis(auxApuracaopiscofins.getPis().add(item.getValorpis())); 
								}
								if (!Tipocobrancacofins.OPERACAO_70.equals(item.getTipocobrancacofins())){
									if(item.getValorbccofins() != null){
										auxApuracaopiscofins.setBccofins(auxApuracaopiscofins.getBccofins().add(item.getValorbccofins())); 
									}
								}
								if(item.getValorcofins() != null){
									auxApuracaopiscofins.setCofins(auxApuracaopiscofins.getCofins().add(item.getValorcofins())); 
								}
							}
						}
						break;
					}
				}
			}
		}
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){				
				mesatual = new SimpleDateFormat("MM").format(ed.getDtemissao());
				for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
					if(mesatual.equals(auxApuracaopiscofins.getMes())){
						if(ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty()){
							for(Entregamaterial em : ed.getListaEntregamaterial()){
								if(em.getValordesconto() == null) em.setValordesconto(new Money());
								if(em.getValorfrete() == null) em.setValorfrete(new Money());
								
								if(em.getNaturezabcc() != null){
									if(Naturezabasecalculocredito.AQUISICAO_BENS_REVENDA.equals(em.getNaturezabcc())){
										auxApuracaopiscofins.setAquisicaobensrevenda(auxApuracaopiscofins.getAquisicaobensrevenda().add(new Money(em.getQtde()*em.getValorunitario())).add(em.getValorfrete()).subtract(em.getValordesconto()));									
									}else if(Naturezabasecalculocredito.AQUISICAO_BENS_INSUMO.equals(em.getNaturezabcc())){
										auxApuracaopiscofins.setAquisicaobensinsumos(auxApuracaopiscofins.getAquisicaobensinsumos().add(new Money(em.getQtde()*em.getValorunitario())).add(em.getValorfrete()).subtract(em.getValordesconto()));
									}else if(Naturezabasecalculocredito.AQUISICAO_SERVICOS_INSUMO.equals(em.getNaturezabcc())){
										auxApuracaopiscofins.setAquisicaoservicosutilizado(auxApuracaopiscofins.getAquisicaoservicosutilizado().add(new Money(em.getQtde()*em.getValorunitario())).add(em.getValorfrete()).subtract(em.getValordesconto()));
									}else if(Naturezabasecalculocredito.ENERGIA_ELETRICA_ESTABELECIMENTO_PESSOA_JURIDICA.equals(em.getNaturezabcc())){
										auxApuracaopiscofins.setEnergiaeletrica(auxApuracaopiscofins.getEnergiaeletrica().add(new Money(em.getQtde()*em.getValorunitario())).add(em.getValorfrete()).subtract(em.getValordesconto()));
									}
								}
								if(em.getCstpis() != null && em.getValorpis() != null){
									if(Tipocobrancapis.OPERACAO_50.equals(em.getCstpis()) || Tipocobrancapis.OPERACAO_51.equals(em.getCstpis()) || 
										Tipocobrancapis.OPERACAO_51.equals(em.getCstpis()) || Tipocobrancapis.OPERACAO_53.equals(em.getCstpis()) || 
										Tipocobrancapis.OPERACAO_54.equals(em.getCstpis()) || Tipocobrancapis.OPERACAO_55.equals(em.getCstpis()) || 
										Tipocobrancapis.OPERACAO_56.equals(em.getCstpis())){
										auxApuracaopiscofins.setPiscredito(auxApuracaopiscofins.getPiscredito().add(em.getValorpis()));
									}
								}
								if(em.getCstcofins() != null && em.getValorcofins() != null){
									if(Tipocobrancacofins.OPERACAO_50.equals(em.getCstcofins()) || Tipocobrancacofins.OPERACAO_51.equals(em.getCstcofins()) || 
										Tipocobrancacofins.OPERACAO_51.equals(em.getCstcofins()) || Tipocobrancacofins.OPERACAO_53.equals(em.getCstcofins()) || 
										Tipocobrancacofins.OPERACAO_54.equals(em.getCstcofins()) || Tipocobrancacofins.OPERACAO_55.equals(em.getCstcofins()) || 
										Tipocobrancacofins.OPERACAO_56.equals(em.getCstcofins())){
										auxApuracaopiscofins.setCofinscredito(auxApuracaopiscofins.getCofinscredito().add(em.getValorcofins()));
									}
								}
							}
						}
						break;
					}
				}
			}
		}
		
		for(Aux_apuracaopiscofins auxApuracaopiscofins : listaApuracaopiscofins){
			auxApuracaopiscofins.setPisdevido(auxApuracaopiscofins.getPis().subtract(auxApuracaopiscofins.getPisretido()));
			auxApuracaopiscofins.setCofinsdevido(auxApuracaopiscofins.getCofins().subtract(auxApuracaopiscofins.getCofinsretido()));
			auxApuracaopiscofins.setBccreditos(auxApuracaopiscofins.getAquisicaobensinsumos().add(auxApuracaopiscofins.getAquisicaobensrevenda()).add(auxApuracaopiscofins.getAquisicaoservicosutilizado()).add(auxApuracaopiscofins.getEnergiaeletrica()));
			auxApuracaopiscofins.setSaldopis(auxApuracaopiscofins.getPisdevido().subtract(auxApuracaopiscofins.getPiscredito()));
			auxApuracaopiscofins.setSaldocofins(auxApuracaopiscofins.getCofinsdevido().subtract(auxApuracaopiscofins.getCofinscredito()));
		}
		
		return listaApuracaopiscofins;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ApuracaopiscofinsFiltro filtro) throws Exception {
		return null;
	}
	
	public ModelAndView gerarDetalhado(WebRequestContext request, ApuracaopiscofinsFiltro filtro) throws Exception {
		List<Entregadocumento> listaEntregadocumento = null;
		if(filtro.getEntrada() != null && filtro.getEntrada()){
			listaEntregadocumento = entradafiscalService.findforApuracaopiscofins(filtro);
		}
		
		List<NotaFiscalServico> listaNotafiscalservico = null;
		List<Notafiscalproduto> listanNotafiscalproduto = null;
		if(filtro.getSaida() != null && filtro.getSaida()){
			listaNotafiscalservico = notaFiscalServicoService.findforApuracaopiscofins(filtro);
			listanNotafiscalproduto = notafiscalprodutoService.findforApuracaopiscofins(filtro);
		}
		
		StringBuilder csv = new StringBuilder();
		csv.append("Data fiscal;");
		csv.append("Modelo NF;");
		csv.append("N�mero;");
		csv.append("N�mero NFS-e;");
		csv.append("Chave de acesso / C�digo de verifica��o;");
		csv.append("CPF / CNPJ;");
		csv.append("Cliente / Fornecedor;");
		csv.append("Valor Bruto (R$);");
		csv.append("Valor Liquido NF-e (R$);");
		csv.append("CST PIS;");
		csv.append("Base de calculo PIS (R$);");
		csv.append("Aliquota PIS (%);");
		csv.append("Valor do PIS (R$);");
		csv.append("Valor do PIS Retido (R$);");
		csv.append("Valor PIS Cr�dito (R$);");
		csv.append("CST COFINS;");
		csv.append("Base de calculo COFINS (R$);");
		csv.append("Aliquota COFINS (%);");
		csv.append("Valor do COFINS (R$);");
		csv.append("Valor do COFINS Retido (R$);");
		csv.append("Valor COFINS Cr�dito (R$);");
		csv.append("Natureza B.C.;");
		csv.append("\n");
		
		Money totalBruto = new Money();
		Money totalLiquido = new Money();
		Money totalBasePis = new Money();
		Money totalPis = new Money();
		Money totalPisRetido = new Money();
		Money totalPisCredito = new Money();
		Money totalBaseCofins = new Money();
		Money totalCofins = new Money();
		Money totalCofinsRetido = new Money();
		Money totalCofinsCredito = new Money();
		
		if(listaNotafiscalservico != null && !listaNotafiscalservico.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(listaNotafiscalservico, "cdNota", ",");
			List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(whereIn);
			
			for(NotaFiscalServico nfs : listaNotafiscalservico){
				boolean isEletronica = false;
				for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
					if(arquivonfnota.getNota() != null && 
							arquivonfnota.getNota().getCdNota() != null &&
							arquivonfnota.getNota().getCdNota().equals(nfs.getCdNota()) &&
							arquivonfnota.getArquivonf() != null && 
							arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
							arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO) &&
							arquivonfnota.getNumeronfse() != null &&
							arquivonfnota.getCodigoverificacao() != null){
						nfs.setNumeroNfse(arquivonfnota.getNumeronfse());
						nfs.setCodVerificacao(arquivonfnota.getCodigoverificacao());
						isEletronica = true;
						break;
					}
				}
				
//				DATA FISCAL
				csv.append(SinedDateUtils.toString(nfs.getDtEmissao())).append(";");
				
//				MODELO NF
				if(isEletronica){
					csv.append("99;");
				} else {
					csv.append("98;");
				}
				
//				N�MERO
				csv.append(nfs.getNumero() != null ? nfs.getNumero() : "").append(";");
				
//				N�MERO NFS-e
				csv.append(nfs.getNumeroNfse() != null ? nfs.getNumeroNfse() : "").append(";");
				
//				CHAVE DE ACESSO / C�DIGO DE VERIFICA��O
				csv.append(nfs.getCodVerificacao() != null ? nfs.getCodVerificacao() : "").append(";");
				
//				CPF / CNPJ
				if(nfs.getCliente() != null){
					if(nfs.getCliente().getCpf() != null){
						csv.append(nfs.getCliente().getCpf());
					}else if(nfs.getCliente().getCnpj() != null){
						csv.append(nfs.getCliente().getCnpj());
					}
				}
				csv.append(";");
				
//				CLIENTE / FORNECEDOR
				if(nfs.getCliente() != null){
					if(nfs.getCliente().getRazaosocial() != null && nfs.getCliente().getRazaosocial().trim().equals("")){
						csv.append(nfs.getCliente().getRazaosocial());
					} else {
						csv.append(nfs.getCliente().getNome());
					}
				}
				csv.append(";");
				
//				VALOR BRUTO
				Money valorbruto = nfs.getValorTotalServicos();
				if(valorbruto != null){
					csv.append(valorbruto);
					totalBruto = totalBruto.add(valorbruto);
				}
				csv.append(";");
				
//				VALOR LIQUIDO NF-E
				Money valorNota = nfs.getValorNota();
				if(valorNota != null){
					csv.append(valorNota);
					totalLiquido = totalLiquido.add(valorNota);
				}
				csv.append(";");
				
				Money basecalculo = nfs.getBasecalculo();
				
//				CST PIS
				Tipocobrancapis cstpis = nfs.getCstpis();
				if(cstpis != null){
					csv.append(cstpis.getCdnfe());
				}
				csv.append(";");
				
//				BASE DE CALCULO PIS
				Money basecalculopis = nfs.getBasecalculopis();
				if(basecalculopis != null){
					csv.append(basecalculopis);
					totalBasePis = totalBasePis.add(basecalculopis);
				} else if(basecalculo != null){
					csv.append(basecalculo);
					totalBasePis = totalBasePis.add(basecalculo);
				}
				csv.append(";");
				
//				ALIQUOTA PIS
				Double pis = nfs.getPis();
				if(pis != null){
					csv.append(SinedUtil.descriptionDecimal(pis));
				}
				csv.append(";");
				
//				VALOR PIS
				Money valorPis = nfs.getValorPis();
				if(valorPis != null){
					csv.append(valorPis);
					totalPis = totalPis.add(valorPis);
				}
				csv.append(";");
				
//				VALOR PIS RETIDO
				Boolean incidepis = nfs.getIncidepis();
				if(valorPis != null && incidepis != null && incidepis){
					csv.append(valorPis);
					totalPisRetido = totalPisRetido.add(valorPis);
				}
				csv.append(";");
				
//				VALOR PIS CREDITO
				csv.append(";");

//				CST COFINS
				Tipocobrancacofins cstcofins = nfs.getCstcofins();
				if(cstcofins != null){
					csv.append(cstcofins.getCdnfe());
				}
				csv.append(";");
				
//				BASE DE CALCULO COFINS
				Money basecalculocofins = nfs.getBasecalculocofins();
				if(basecalculocofins != null){
					csv.append(basecalculocofins);
					totalBaseCofins = totalBaseCofins.add(basecalculocofins);
				} else if(basecalculo != null){
					csv.append(basecalculo);
					totalBaseCofins = totalBaseCofins.add(basecalculo);
				}
				csv.append(";");
				
//				ALIQUOTA COFINS
				Double cofins = nfs.getCofins();
				if(cofins != null){
					csv.append(SinedUtil.descriptionDecimal(cofins));
				}
				csv.append(";");
				
//				VALOR COFINS
				Money valorCofins = nfs.getValorCofins();
				if(valorCofins != null){
					csv.append(valorCofins);
					totalCofins = totalCofins.add(valorCofins);
				}
				csv.append(";");
				
//				VALOR COFINS RETIDO
				Boolean incidecofins = nfs.getIncidecofins();
				if(valorCofins != null && incidecofins != null && incidecofins){
					csv.append(valorCofins);
					totalCofinsRetido = totalCofins.add(totalCofinsRetido);
				}
				csv.append(";");
				
//				VALOR COFINS CREDITO
				csv.append(";");
				
//				NATUREZA B.C.
				csv.append(";");
				
				csv.append("\n");
			}
		}
		
		if(listanNotafiscalproduto != null && !listanNotafiscalproduto.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(listanNotafiscalproduto, "cdNota", ",");
			List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNotaNotCancelada(whereIn);
			
			for(Notafiscalproduto nfp : listanNotafiscalproduto){
				boolean entrada = Tipooperacaonota.ENTRADA.equals(nfp.getTipooperacaonota());
				boolean isEletronica = false;
				for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
					if(arquivonfnota.getNota() != null && 
							arquivonfnota.getNota().getCdNota() != null &&
							arquivonfnota.getNota().getCdNota().equals(nfp.getCdNota()) &&
							arquivonfnota.getChaveacesso() != null){
						nfp.setChaveAcesso(arquivonfnota.getChaveacesso());
						isEletronica = true;
						break;
					}
				}
				
//				DATA FISCAL
				if(nfp.getDatahorasaidaentrada() != null){
					csv.append(SinedDateUtils.toString(nfp.getDatahorasaidaentrada())).append(";");
				} else {
					csv.append(SinedDateUtils.toString(nfp.getDtEmissao())).append(";");
				}
				
//				MODELO NF
				if(isEletronica){
					csv.append("55;");
				} else {
					csv.append("01;");
				}
				
//				N�MERO
				csv.append(nfp.getNumero() != null ? nfp.getNumero() : "").append(";");
				
//				N�MERO NFS-e
				csv.append(";");
				
//				CHAVE DE ACESSO / C�DIGO DE VERIFICA��O
				csv.append(nfp.getChaveAcesso() != null ? ("'"  + nfp.getChaveAcesso() + "'") : "").append(";");
				
//				CPF / CNPJ
				if(nfp.getCliente() != null){
					if(nfp.getCliente().getCpf() != null){
						csv.append(nfp.getCliente().getCpf());
					}else if(nfp.getCliente().getCnpj() != null){
						csv.append(nfp.getCliente().getCnpj());
					}
				}
				csv.append(";");
				
//				CLIENTE / FORNECEDOR
				if(nfp.getCliente() != null){
					if(nfp.getCliente().getRazaosocial() != null && nfp.getCliente().getRazaosocial().trim().equals("")){
						csv.append(nfp.getCliente().getRazaosocial());
					} else {
						csv.append(nfp.getCliente().getNome());
					}
				}
				csv.append(";");
				
//				VALOR BRUTO
				Money valorbruto = nfp.getValorprodutos();
				if(valorbruto != null){
					csv.append(valorbruto);
					totalBruto = totalBruto.add(valorbruto);
				}
				csv.append(";");
				
//				VALOR LIQUIDO NF-E
				Money valorliquido = nfp.getValorNota();
				if(valorliquido != null){
					csv.append(valorliquido);
					totalLiquido = totalLiquido.add(valorliquido);
				}
				csv.append(";");
				
				List<ApuracaoimpostosDetalhadoBean> lista = new ArrayList<ApuracaoimpostosDetalhadoBean>();
				List<Notafiscalprodutoitem> listaItens = nfp.getListaItens();
				
				for (Notafiscalprodutoitem it : listaItens) {
					Tipocobrancacofins cstcofins = it.getTipocobrancacofins();
					Tipocobrancapis cstpis = it.getTipocobrancapis();
					double cofins = it.getCofins() != null ? it.getCofins() : 0d;
					double pis = it.getPis() != null ? it.getPis() : 0d;
					
					Money basecalculocofins = it.getValorbccofins();
					Money valorcofins = it.getValorcofins();
					
					Money basecalculopis = it.getValorbcpis();
					Money valorpis = it.getValorpis();
					
					ApuracaoimpostosDetalhadoBean bean = new ApuracaoimpostosDetalhadoBean(cstcofins, cofins, cstpis, pis, null);
					if(lista.contains(bean)){
						bean = lista.get(lista.indexOf(bean));
					}
					
					if (!entrada || Tipocobrancacofins.isCreditoCofins(cstcofins)){
						bean.addBasecalculocofins(basecalculocofins);
						bean.addValorcofins(valorcofins);
						totalBaseCofins = totalBaseCofins.add(basecalculocofins);
						totalCofins = totalCofins.add(valorcofins);
						
						if(entrada && Tipocobrancacofins.isCreditoCofins(cstcofins)){
							bean.addValorcofinscredito(valorcofins);
							totalCofinsCredito = totalCofinsCredito.add(valorcofins);
						}
					}
					
					if (!entrada || Tipocobrancapis.isCreditoPis(cstpis)){
						bean.addBasecalculopis(basecalculopis);
						bean.addValorpis(valorpis);
						totalBasePis = totalBasePis.add(basecalculopis);
						totalPis = totalPis.add(valorpis);
						
						if(entrada && Tipocobrancacofins.isCreditoCofins(cstcofins)){
							bean.addValorpiscredito(valorpis);
							totalPisCredito = totalPisCredito.add(valorpis);
						}
					}
					
					if(lista.contains(bean)){
						bean = lista.set(lista.indexOf(bean), bean);
					} else {
						lista.add(bean);
					}
				}
				
				this.putDetalheCSv(csv, lista);
				
				csv.append("\n");
			}
		}
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){		
//				DATA FISCAL
				if(ed.getDtentrada() != null){
					csv.append(SinedDateUtils.toString(ed.getDtentrada())).append(";");
				} else {
					csv.append(SinedDateUtils.toString(ed.getDtemissao())).append(";");
				}
				
//				MODELO NF
				if(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null){
					csv.append(ed.getModelodocumentofiscal().getCodigo());
				}
				csv.append(";");
				
//				N�MERO
				csv.append(ed.getNumero()).append(";");
				
//				N�MERO NFS-e
				csv.append(";");
				
//				CHAVE DE ACESSO / C�DIGO DE VERIFICA��O
				if(ed.getChaveacesso() != null && !ed.getChaveacesso().equals("")){
					csv.append("'").append(ed.getChaveacesso());
				}
				csv.append(";");
				
//				CPF / CNPJ
				if(ed.getFornecedor() != null){
					if(ed.getFornecedor().getCpf() != null){
						csv.append(ed.getFornecedor().getCpf());
					}else if(ed.getFornecedor().getCnpj() != null){
						csv.append(ed.getFornecedor().getCnpj());
					}
				}
				csv.append(";");
				
//				CLIENTE / FORNECEDOR
				if(ed.getFornecedor() != null){
					if(ed.getFornecedor().getRazaosocial() != null && ed.getFornecedor().getRazaosocial().trim().equals("")){
						csv.append(ed.getFornecedor().getRazaosocial());
					} else {
						csv.append(ed.getFornecedor().getNome());
					}
				}
				csv.append(";");
				
//				VALOR BRUTO
				Money valorbruto = ed.getValormercadoriaSPEDPiscofins();
				if(valorbruto != null){
					csv.append(valorbruto);
					totalBruto = totalBruto.add(valorbruto);
				}
				csv.append(";");
				
//				VALOR LIQUIDO NF-E
				Money valorliquido = ed.getValor();
				if(valorliquido != null){
					csv.append(valorliquido);
					totalLiquido = totalLiquido.add(valorliquido);
				}
				csv.append(";");
				
				List<ApuracaoimpostosDetalhadoBean> lista = new ArrayList<ApuracaoimpostosDetalhadoBean>();
				Set<Entregamaterial> listaEntregamaterial = ed.getListaEntregamaterial();
				
				for (Entregamaterial it : listaEntregamaterial) {
					Tipocobrancacofins cstcofins = it.getCstcofins();
					Tipocobrancapis cstpis = it.getCstpis();
					double cofins = it.getCofins() != null ? it.getCofins() : 0d;
					double pis = it.getPis() != null ? it.getPis() : 0d;
					Naturezabasecalculocredito naturezabcc = it.getNaturezabcc();
					
					Money basecalculocofins = it.getValorbccofins();
					Money valorcofins = it.getValorcofins();
					Money valorcofinsretido = it.getValorcofinsretido();
					
					Money basecalculopis = it.getValorbcpis();
					Money valorpis = it.getValorpis();
					Money valorpisretido = it.getValorpisretido();
					
					ApuracaoimpostosDetalhadoBean bean = new ApuracaoimpostosDetalhadoBean(cstcofins, cofins, cstpis, pis, naturezabcc);
					
					boolean contains = lista.contains(bean);
					int idx = lista.indexOf(bean);
					if(contains){
						bean = lista.get(idx);
					}
					
					if(Tipocobrancapis.isCreditoPis(cstpis)){
						bean.addBasecalculopis(basecalculopis);
						bean.addValorpis(valorpis);
						bean.addValorpisretido(valorpisretido);
						bean.addValorpiscredito(valorpis);
						
						totalBasePis = totalBasePis.add(basecalculopis != null ? basecalculopis : new Money());
						totalPis = totalPis.add(valorpis != null ? valorpis : new Money());
						totalPisRetido = totalPisRetido.add(valorpisretido != null ? valorpisretido : new Money());
						totalPisCredito = totalPisCredito.add(valorpis != null ? valorpis : new Money());
					}
					
					if(Tipocobrancacofins.isCreditoCofins(cstcofins)){
						bean.addBasecalculocofins(basecalculocofins);
						bean.addValorcofins(valorcofins);
						bean.addValorcofinsretido(valorcofinsretido);
						bean.addValorcofinscredito(valorcofins);
						
						totalBaseCofins = totalBaseCofins.add(basecalculocofins != null ? basecalculocofins : new Money());
						totalCofins = totalCofins.add(valorcofins != null ? valorcofins : new Money());
						totalCofinsRetido = totalCofinsRetido.add(valorcofinsretido != null ? valorcofinsretido : new Money());
						totalCofinsCredito = totalCofinsCredito.add(valorcofins != null ? valorcofins : new Money());
					}
					
					if(contains){
						bean = lista.set(idx, bean);
					} else {
						lista.add(bean);
					}
				}
				
				this.putDetalheCSv(csv, lista);
				
				csv.append("\n");
			}
		}
		
		csv.append(";");
		csv.append(";");
		csv.append(";");
		csv.append(";");
		csv.append(";");
		csv.append(";");
		csv.append(";");
		csv.append(totalBruto.toString()).append(";");
		csv.append(totalLiquido.toString()).append(";");
		csv.append(";");
		csv.append(totalBasePis.toString()).append(";");
		csv.append(";");
		csv.append(totalPis.toString()).append(";");
		csv.append(totalPisRetido.toString()).append(";");
		csv.append(totalPisCredito.toString()).append(";");
		csv.append(";");
		csv.append(totalBaseCofins.toString()).append(";");
		csv.append(";");
		csv.append(totalCofins.toString()).append(";");
		csv.append(totalCofinsRetido.toString()).append(";");
		csv.append(totalCofinsCredito.toString()).append(";");
		csv.append(";");
		csv.append("\n");
		
		Resource resource = new Resource("text/csv","apuracaocreditopiscofins_detalhado_" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtinicio()) + "_" + new SimpleDateFormat("dd/MM/yyyy").format(filtro.getDtfim()) + ".csv", csv.toString().replaceAll(";null;", ";;").replaceAll(";null;", ";;").getBytes());
		return new ResourceModelAndView(resource);
	}
	
	private void putDetalheCSv(StringBuilder csv,
			List<ApuracaoimpostosDetalhadoBean> lista) {
		boolean primeiraIteracao = true;
		
		for (ApuracaoimpostosDetalhadoBean bean : lista) {
			if(!primeiraIteracao){
				csv.append("\n");
				csv.append(";"); // DATA FISCAL
				csv.append(";"); // MODELO NF
				csv.append(";"); // NUMERO
				csv.append(";"); // NUMERO NFS-E
				csv.append(";"); // CHAVE DE ACESSO / C�DIGO DE VERIFICA��O
				csv.append(";"); // CPF / CNPJ
				csv.append(";"); // CLIENTE / FORNECEDOR
				csv.append(";"); // VALOR BRUTO
				csv.append(";"); // VALOR LIQUIDO
			}
			primeiraIteracao = false;
			
//			CST PIS
			Tipocobrancapis cstpis = bean.getCstpis();
			if(cstpis != null){
				csv.append(cstpis.getCdnfe());
			}
			csv.append(";");
			
//			BASE DE CALCULO PIS
			csv.append(bean.getBasecalculopis()).append(";");
			
//			ALIQUOTA PIS
			csv.append(SinedUtil.descriptionDecimal(bean.getPis())).append(";");
			
//			VALOR PIS
			csv.append(bean.getValorpis()).append(";");
			
//			VALOR PIS RETIDO
			csv.append(bean.getValorpisretido()).append(";");
			
//			VALOR PIS CREDITO
			csv.append(bean.getValorpiscredito()).append(";");

//			CST COFINS
			Tipocobrancacofins cstcofins = bean.getCstcofins();
			if(cstcofins != null){
				csv.append(cstcofins.getCdnfe());
			}
			csv.append(";");
			
//			BASE DE CALCULO COFINS
			csv.append(bean.getBasecalculocofins()).append(";");
			
//			ALIQUOTA COFINS
			csv.append(SinedUtil.descriptionDecimal(bean.getCofins())).append(";");
			
//			VALOR COFINS
			csv.append(bean.getValorcofins()).append(";");
			
//			VALOR COFINS RETIDO
			csv.append(bean.getValorcofinsretido()).append(";");
			
//			VALOR COFINS CREDITO
			csv.append(bean.getValorcofinscredito()).append(";");
			
//			NATUREZA B.C.
			Naturezabasecalculocredito naturezabc = bean.getNaturezabc();
			if(naturezabc != null){
				csv.append(naturezabc.getDescricao());
			}
			csv.append(";");
		}
	}

}
