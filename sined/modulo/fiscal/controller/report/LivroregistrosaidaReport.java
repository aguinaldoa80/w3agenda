package br.com.linkcom.sined.modulo.fiscal.controller.report;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.Saidafiscal;
import br.com.linkcom.sined.modulo.fiscal.controller.report.filtro.LivroregistrosaidaFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.filtro.ModeloRegistroSaida;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/fiscal/relatorio/Livroregistrosaida", authorizationModule=ReportAuthorizationModule.class)
public class LivroregistrosaidaReport  extends SinedReport<LivroregistrosaidaFiltro>{

	private NotafiscalprodutoitemService notafiscalprodutoitemService;
	private ArquivonfnotaService arquivonfnotaService;
	
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {
		this.notafiscalprodutoitemService = notafiscalprodutoitemService;
	}
	
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	
	@DefaultAction
	public ModelAndView handle(WebRequestContext request){
		return new ModelAndView("relatorio/livroregistrosaida", "filtro", new LivroregistrosaidaFiltro());
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, LivroregistrosaidaFiltro filtro) throws Exception {
		return createReportLivroRegistroEntrada(request, filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "livro_registro_saida";
	}

	@Override
	public String getTitulo(LivroregistrosaidaFiltro filtro) {
		return "LIVRO REGISTRO DE SA�DAS � RE � " + filtro.getModelo().getDescricao().toUpperCase();
	}

	private IReport createReportLivroRegistroEntrada(WebRequestContext request, LivroregistrosaidaFiltro filtro) {
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = notafiscalprodutoitemService.findForLivroSaida(filtro);
		Map<String, Saidafiscal> mapa = new HashMap<String, Saidafiscal>();
		
		for (Notafiscalprodutoitem notafiscalprodutoitem: listaNotafiscalprodutoitem){
			
			Notafiscalproduto notafiscalproduto = notafiscalprodutoitem.getNotafiscalproduto();
			Cliente cliente = notafiscalproduto.getCliente();
			
			String serie = null;
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProdutoAndCancelada(notafiscalproduto);
			if (arquivonfnota != null && 
					arquivonfnota.getArquivonf() != null && 
					arquivonfnota.getArquivonf().getConfiguracaonfe() != null){
				if(arquivonfnota.getNota().getSerienfe() != null){
					serie = arquivonfnota.getNota().getSerienfe().toString();
				} else if(arquivonfnota.getArquivonf().getConfiguracaonfe().getSerienfse() != null){
					serie = arquivonfnota.getArquivonf().getConfiguracaonfe().getSerienfse();
				}
			}
			
			Saidafiscal bean = new Saidafiscal();
			bean.setCdnota(notafiscalproduto.getCdNota());
			bean.setSerie(serie);
			bean.setNumero(notafiscalproduto.getNumero());
			
			if (notafiscalproduto.getDtEmissao()!=null){
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(notafiscalproduto.getDtEmissao());
				bean.setDia(calendar.get(Calendar.DAY_OF_MONTH));
			}
			
			if (cliente.getListaEndereco()!=null && !cliente.getListaEndereco().isEmpty()){
				List<Endereco> listaEndereco = new ListSet<Endereco>(Endereco.class, cliente.getListaEndereco());
				if (listaEndereco.get(0).getMunicipio()!=null && listaEndereco.get(0).getMunicipio().getUf()!=null){
					bean.setUfDestino(listaEndereco.get(0).getMunicipio().getUf().getSigla());
				}
			}
			
			Money valorcontabil = new Money(0);
			if(notafiscalprodutoitem.getValorunitario() != null && notafiscalprodutoitem.getValorunitario() > 0){
				valorcontabil = new Money(notafiscalprodutoitem.getValorunitario() * (notafiscalprodutoitem.getQtde() != null ? notafiscalprodutoitem.getQtde() : 1.0));
				if(notafiscalprodutoitem.getValoricmsst() != null){
					valorcontabil = valorcontabil.add(notafiscalprodutoitem.getValoricmsst());
				}
				if(notafiscalprodutoitem.getValoripi() != null){
					valorcontabil = valorcontabil.add(notafiscalprodutoitem.getValoripi());
				}
				if(notafiscalprodutoitem.getOutrasdespesas() != null){
					valorcontabil = valorcontabil.add(notafiscalprodutoitem.getOutrasdespesas());
				}
				if(notafiscalprodutoitem.getValordesconto() != null){
					valorcontabil = valorcontabil.subtract(notafiscalprodutoitem.getValordesconto());
				}
				if(notafiscalprodutoitem.getValorfrete() != null){
					valorcontabil = valorcontabil.add(notafiscalprodutoitem.getValorfrete());
				}
				if(notafiscalprodutoitem.getValorseguro() != null){
					valorcontabil = valorcontabil.add(notafiscalprodutoitem.getValorseguro());
				}
			}
			bean.setValorContabil(valorcontabil);
			
			bean.setFiscal(notafiscalprodutoitem.getCfop()!=null ? notafiscalprodutoitem.getCfop().getCodigo() : null);
			
			boolean icms = false;
			boolean ipi = false;
			
			if ((notafiscalprodutoitem.getValorbcicms()!=null 
					&& notafiscalprodutoitem.getIcms()!=null
					&& notafiscalprodutoitem.getValoricms()!=null) ||
				 (notafiscalprodutoitem.getValorbcicmsst()!=null 
							&& notafiscalprodutoitem.getValoricmsst()!=null)){
				icms = true;
			}
			
			if (notafiscalprodutoitem.getValorbcipi()!=null 
					&& notafiscalprodutoitem.getIpi()!=null
					&& notafiscalprodutoitem.getValoripi()!=null){
				ipi = true;
			}
			
			if (!icms && !ipi){
				Saidafiscal beanClone = bean.getClone();
				beanClone.setIcmsIpi("");
				preencherMapaSaidaFiscal(beanClone, mapa);
			}else {
				if (icms){
					Saidafiscal beanClone = bean.getClone();
					beanClone.setIcmsIpi("ICMS");
					if (ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(notafiscalprodutoitem.getValorFiscalIcms())){
						beanClone.setValorBC(notafiscalprodutoitem.getValorbcicms());
						beanClone.setAliquota(notafiscalprodutoitem.getIcms());
						beanClone.setValorDebitado(notafiscalprodutoitem.getValoricms());
					}else if (ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(notafiscalprodutoitem.getValorFiscalIcms())){
						beanClone.setValorIsentasNaoTributadas(notafiscalprodutoitem.getValorbruto());
					}else if (ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(notafiscalprodutoitem.getValorFiscalIcms())){
						beanClone.setValorOutras(notafiscalprodutoitem.getValorbruto());
					}
					
					if (filtro.getDiscriminarST() != null && filtro.getDiscriminarST() && 
							BigDecimal.ZERO.compareTo(notafiscalprodutoitem.getValoricmsst().getValue()) < 0){
						beanClone.setValorIcmsSt(notafiscalprodutoitem.getValoricmsst());
					}

					preencherMapaSaidaFiscal(beanClone, mapa);
				}
				if (ipi && filtro.getModelo().equals(ModeloRegistroSaida.MODELO_P2)){
					Saidafiscal beanClone = bean.getClone();
					beanClone.setIcmsIpi("IPI");
					if (ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(notafiscalprodutoitem.getValorFiscalIpi())){
						beanClone.setValorBC(notafiscalprodutoitem.getValorbcipi());
						beanClone.setAliquota(notafiscalprodutoitem.getIpi());
						beanClone.setValorDebitado(notafiscalprodutoitem.getValoripi());
					}else if (ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(notafiscalprodutoitem.getValorFiscalIpi())){
						beanClone.setValorIsentasNaoTributadas(notafiscalprodutoitem.getValorbruto());
					}else if (ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(notafiscalprodutoitem.getValorFiscalIpi())){
						beanClone.setValorOutras(notafiscalprodutoitem.getValorbruto());
					}
					if(icms){
						beanClone.setValorContabil(new Money());
						beanClone.setValorIcmsSt(new Money());
					}
					
					preencherMapaSaidaFiscal(beanClone, mapa);
				}
			}
		}		
		
		List<Saidafiscal> listaRelatorio = new ArrayList<Saidafiscal>(mapa.values());
		for(Saidafiscal saidaFiscal: listaRelatorio){
			if(saidaFiscal.getValorIcmsSt() != null && BigDecimal.ZERO.compareTo(saidaFiscal.getValorIcmsSt().getValue()) < 0){
				saidaFiscal.setObservacao((saidaFiscal.getObservacao() != null ? saidaFiscal.getObservacao() : "") + " ICMS ST: " + saidaFiscal.getValorIcmsSt());
			}
		}
		
		Collections.sort(listaRelatorio, new Comparator<Saidafiscal>() {
			@Override
			public int compare(Saidafiscal o1, Saidafiscal o2) {
				String numero1 = o1.getNumero();
				String numero2 = o2.getNumero();
				int compCdnota = o1.getCdnota().compareTo(o2.getCdnota());
				int compIcmsipi = o1.getIcmsIpi().compareTo(o2.getIcmsIpi());
				if (numero1==null && numero2==null){
					return compCdnota;
				}else if (numero1!=null && numero2==null){
					return -1;
				}else if (numero1==null && numero2!=null){
					return 1;
				}else {
					int compNumero = numero1.compareTo(numero2);
					if (compNumero==0){
						if (compCdnota==0){
							return compIcmsipi;
						}else {
							return compCdnota;
						}
					}else {
						return compNumero;
					}
				}
			}
		});
		
		Report report;
		if (filtro.getModelo().equals(ModeloRegistroSaida.MODELO_P2))
			report = new Report("/fiscal/livroregistrosaida_p2");
		else
			report = new Report("/fiscal/livroregistrosaida_p2_a");
		
		configurarParametros(request, filtro, report);
		Empresa empresa = this.getEmpresa(filtro);
		
		report.addParameter("CNPJ", empresa.getCnpj() != null ? empresa.getCnpj().toString() : "");
		report.addParameter("INSCRICAOESTADUAL", empresa.getInscricaoestadual());
		report.addParameter("PERIODOSAIDA", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
		report.setDataSource(listaRelatorio);
		
		return report;
	}
	
	private void preencherMapaSaidaFiscal(Saidafiscal bean, Map<String, Saidafiscal> mapa){
		Saidafiscal beanAux = mapa.get(bean.getKey());
		if (beanAux==null){
			mapa.put(bean.getKey(), bean);
		}else {
			beanAux.somarCampos(bean);
			mapa.put(bean.getKey(), beanAux);	
		}
	}

}
