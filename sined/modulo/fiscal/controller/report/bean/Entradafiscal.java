package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import java.sql.Date;

import javax.persistence.Transient;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;

public class Entradafiscal implements Cloneable {
	
	private Integer cdentregadocumento;
	private Date dtentrada;
	private String especie;
	private String serie;
	private String numero;
	private Date dtdocumento;
	private String codigoEmitente;
	private String ufOrigem;
	private Money valorContabil;
	private String fornecedorStr;
	private String fiscal;
	private Integer codigoValorFiscalIcms;
	private Money valorBCIcms;
	private Money valorBCIcmsst;
	private Double aliquotaIcms;
	private Money valorCreditadoIcms;
	private Money valorCreditadoIcmsst;
	private Integer codigoValorFiscalIpi;
	private Money valorBCIpi;
	private Double aliquotaIpi;
	private Money valorCreditadoIpi;
	private String observacao;
	private String imposto;
	
	// TRANSIENT
	private String identificadortela;
	
	public Integer getCdentregadocumento() {
		return cdentregadocumento;
	}
	public Date getDtentrada() {
		return dtentrada;
	}
	public String getEspecie() {
		return especie;
	}
	public String getSerie() {
		return serie;
	}
	public String getNumero() {
		return numero;
	}
	public Date getDtdocumento() {
		return dtdocumento;
	}
	public String getCodigoEmitente() {
		return codigoEmitente;
	}
	public String getUfOrigem() {
		return ufOrigem;
	}
	public Money getValorContabil() {
		return valorContabil;
	}
	public String getFornecedorStr() {
		return fornecedorStr;
	}
	public String getFiscal() {
		return fiscal;
	}
	public Integer getCodigoValorFiscalIcms() {
		return codigoValorFiscalIcms;
	}
	public Money getValorBCIcms() {
		return valorBCIcms;
	}
	public Money getValorBCIcmsst() {
		return valorBCIcmsst;
	}
	public Double getAliquotaIcms() {
		return aliquotaIcms;
	}
	public Money getValorCreditadoIcms() {
		return valorCreditadoIcms;
	}
	public Money getValorCreditadoIcmsst() {
		return valorCreditadoIcmsst;
	}
	public Integer getCodigoValorFiscalIpi() {
		return codigoValorFiscalIpi;
	}
	public Money getValorBCIpi() {
		return valorBCIpi;
	}
	public Double getAliquotaIpi() {
		return aliquotaIpi;
	}
	public Money getValorCreditadoIpi() {
		return valorCreditadoIpi;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getImposto() {
		return imposto;
	}
		
	public String getKey(){
		
		StringBuilder key = new StringBuilder();
		
		key.append(getCdentregadocumento()).append("-");
		key.append(Util.strings.emptyIfNull(getFiscal())).append("-");
		key.append(getCodigoValorFiscalIcms()!=null ? getCodigoValorFiscalIcms() : "").append("-");
		key.append(getAliquotaIcms()!=null ? getAliquotaIcms() : "").append("-");
		key.append(getCodigoValorFiscalIpi()!=null ? getCodigoValorFiscalIpi() : "").append("-");
		key.append(getAliquotaIpi()!=null ? getAliquotaIpi() : "").append("-");
		key.append(getImposto()!=null ? getImposto() : "");
		
		return key.toString();
	}
	
	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}
	
	public void setCdentregadocumento(Integer cdentregadocumento) {
		this.cdentregadocumento = cdentregadocumento;
	}
	public void setDtentrada(Date dtentrada) {
		this.dtentrada = dtentrada;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDtdocumento(Date dtdocumento) {
		this.dtdocumento = dtdocumento;
	}
	public void setCodigoEmitente(String codigoEmitente) {
		this.codigoEmitente = codigoEmitente;
	}
	public void setUfOrigem(String ufOrigem) {
		this.ufOrigem = ufOrigem;
	}
	public void setValorContabil(Money valorContabil) {
		this.valorContabil = valorContabil;
	}
	public void setFornecedorStr(String fornecedorStr) {
		this.fornecedorStr = fornecedorStr;
	}
	public void setFiscal(String fiscal) {
		this.fiscal = fiscal;
	}
	public void setCodigoValorFiscalIcms(Integer codigoValorFiscalIcms) {
		this.codigoValorFiscalIcms = codigoValorFiscalIcms;
	}
	public void setValorBCIcms(Money valorBCIcms) {
		this.valorBCIcms = valorBCIcms;
	}
	public void setValorBCIcmsst(Money valorBCIcmsst) {
		this.valorBCIcmsst = valorBCIcmsst;
	}
	public void setAliquotaIcms(Double aliquotaIcms) {
		this.aliquotaIcms = aliquotaIcms;
	}
	public void setValorCreditadoIcms(Money valorCreditadoIcms) {
		this.valorCreditadoIcms = valorCreditadoIcms;
	}
	public void setValorCreditadoIcmsst(Money valorCreditadoIcmsst) {
		this.valorCreditadoIcmsst = valorCreditadoIcmsst;
	}
	public void setCodigoValorFiscalIpi(Integer codigoValorFiscalIpi) {
		this.codigoValorFiscalIpi = codigoValorFiscalIpi;
	}
	public void setValorBCIpi(Money valorBCIpi) {
		this.valorBCIpi = valorBCIpi;
	}
	public void setAliquotaIpi(Double aliquotaIpi) {
		this.aliquotaIpi = aliquotaIpi;
	}
	public void setValorCreditadoIpi(Money valorCreditadoIpi) {
		this.valorCreditadoIpi = valorCreditadoIpi;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}	
	public void setImposto(String imposto) {
		this.imposto = imposto;
	}
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	
	public void somarCampos(Entradafiscal bean){
		valorBCIcms = valorBCIcms.add(bean.getValorBCIcms());
		valorCreditadoIcms = valorCreditadoIcms.add(bean.getValorCreditadoIcms());
		valorBCIpi = valorBCIpi.add(bean.getValorBCIpi());
		valorCreditadoIpi = valorCreditadoIpi.add(bean.getValorCreditadoIpi());
		valorContabil = valorContabil.add(bean.getValorContabil());
	}
	
	public Entradafiscal copiaEntradafiscal(){
		Entradafiscal entradafiscal = new Entradafiscal();
		try {
			entradafiscal = (Entradafiscal) this.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return entradafiscal;
	}
}