package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

public class DamdfeCondutor {

	private String cpf;
	private String nome;
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
