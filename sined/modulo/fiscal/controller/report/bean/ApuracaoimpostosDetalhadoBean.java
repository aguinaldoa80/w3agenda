package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;

public class ApuracaoimpostosDetalhadoBean {

	private Tipocobrancacofins cstcofins = null;
	private double cofins = 0d;
	private Tipocobrancapis cstpis = null;
	private double pis = 0d;
	private Naturezabasecalculocredito naturezabc = null;
	
	private Money basecalculocofins = new Money();
	private Money valorcofins = new Money();
	private Money valorcofinsretido = new Money();
	private Money valorcofinscredito = new Money();
	private Money basecalculopis = new Money();
	private Money valorpis = new Money();
	private Money valorpisretido = new Money();
	private Money valorpiscredito = new Money();
	
	
	public ApuracaoimpostosDetalhadoBean(Tipocobrancacofins cstcofins,
			double cofins, Tipocobrancapis cstpis, double pis,
			Naturezabasecalculocredito naturezabc) {
		this.cstcofins = cstcofins;
		this.cofins = cofins;
		this.cstpis = cstpis;
		this.pis = pis;
		this.naturezabc = naturezabc;
	}
	
	public Tipocobrancacofins getCstcofins() {
		return cstcofins;
	}
	public double getCofins() {
		return cofins;
	}
	public Tipocobrancapis getCstpis() {
		return cstpis;
	}
	public double getPis() {
		return pis;
	}
	public Naturezabasecalculocredito getNaturezabc() {
		return naturezabc;
	}
	public void setCstcofins(Tipocobrancacofins cstcofins) {
		this.cstcofins = cstcofins;
	}
	public void setCofins(double cofins) {
		this.cofins = cofins;
	}
	public void setCstpis(Tipocobrancapis cstpis) {
		this.cstpis = cstpis;
	}
	public void setPis(double pis) {
		this.pis = pis;
	}
	public void setNaturezabc(Naturezabasecalculocredito naturezabc) {
		this.naturezabc = naturezabc;
	}
	
	public void addBasecalculocofins(Money valor){
		if(valor != null){
			basecalculocofins = basecalculocofins.add(valor);
		}
	}
	public void addValorcofins(Money valor){
		if(valor != null){
			valorcofins = valorcofins.add(valor);
		}
	}
	public void addValorcofinsretido(Money valor){
		if(valor != null){
			valorcofinsretido = valorcofinsretido.add(valor);
		}
	}
	public void addValorcofinscredito(Money valor){
		if(valor != null){
			valorcofinscredito = valorcofinscredito.add(valor);
		}
	}
	public void addBasecalculopis(Money valor){
		if(valor != null){
			basecalculopis = basecalculopis.add(valor);
		}
	}
	public void addValorpis(Money valor){
		if(valor != null){
			valorpis = valorpis.add(valor);
		}
	}
	public void addValorpisretido(Money valor){
		if(valor != null){
			valorpisretido = valorpisretido.add(valor);
		}
	}
	public void addValorpiscredito(Money valor){
		if(valor != null){
			valorpiscredito = valorpiscredito.add(valor);
		}
	}
	
	public Money getBasecalculocofins() {
		return basecalculocofins;
	}

	public Money getValorcofins() {
		return valorcofins;
	}

	public Money getValorcofinsretido() {
		return valorcofinsretido;
	}

	public Money getValorcofinscredito() {
		return valorcofinscredito;
	}

	public Money getBasecalculopis() {
		return basecalculopis;
	}

	public Money getValorpis() {
		return valorpis;
	}

	public Money getValorpisretido() {
		return valorpisretido;
	}

	public Money getValorpiscredito() {
		return valorpiscredito;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(cofins);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((cstcofins == null) ? 0 : cstcofins.hashCode());
		result = prime * result + ((cstpis == null) ? 0 : cstpis.hashCode());
		result = prime * result
				+ ((naturezabc == null) ? 0 : naturezabc.hashCode());
		temp = Double.doubleToLongBits(pis);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApuracaoimpostosDetalhadoBean other = (ApuracaoimpostosDetalhadoBean) obj;
		if (Double.doubleToLongBits(cofins) != Double
				.doubleToLongBits(other.cofins))
			return false;
		if (cstcofins == null) {
			if (other.cstcofins != null)
				return false;
		} else if (!cstcofins.equals(other.cstcofins))
			return false;
		if (cstpis == null) {
			if (other.cstpis != null)
				return false;
		} else if (!cstpis.equals(other.cstpis))
			return false;
		if (naturezabc == null) {
			if (other.naturezabc != null)
				return false;
		} else if (!naturezabc.equals(other.naturezabc))
			return false;
		if (Double.doubleToLongBits(pis) != Double.doubleToLongBits(other.pis))
			return false;
		return true;
	}
	
}
