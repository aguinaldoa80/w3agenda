package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;


public class NotaItemBean {

	private Double aliquotaCreditoIcms;
	private Double valorBruto;
	private Double quantidade;
	private String cfop;
	private String tipoTributacaoIcms;
	private String tipoCobrancaIcms;
	private Integer cdmaterial;
	private Integer cdmaterialgrupo;
	private String ncmcompleto;
	
	public Double getAliquotaCreditoIcms() {
		return aliquotaCreditoIcms;
	}
	public void setAliquotaCreditoIcms(Double aliquotaCreditoIcms) {
		this.aliquotaCreditoIcms = aliquotaCreditoIcms;
	}
	
	public Double getValorBruto() {
		return valorBruto;
	}
	public void setValorBruto(Double preco) {
		this.valorBruto = preco;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	public String getCfop() {
		return cfop;
	}
	public void setCfop(String cfop) {
		this.cfop = cfop;
	}
	
	public String getTipoTributacaoIcms() {
		return tipoTributacaoIcms;
	}
	public void setTipoTributacaoIcms(String tipoTributacaoIcms) {
		this.tipoTributacaoIcms = tipoTributacaoIcms!=null? tipoTributacaoIcms.toUpperCase(): "";
	}
	
	public String getTipoCobrancaIcms() {
		return tipoCobrancaIcms;
	}
	public void setTipoCobrancaIcms(String tipoCobrancaIcms) {
		this.tipoCobrancaIcms = tipoCobrancaIcms;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	
	
}
