package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfop;

public class Aux_apuracaoicms {

	public final static String ENTRADA = "entrada";
	public final static String SAIDA = "saida";
	
	public final static String ENTRADAICMS = "entradaicms";
	public final static String ENTRADAICMSST = "entradaicmsst";
	public final static String SAIDAICMS = "saidaicms";
	public final static String SAIDAICMSST = "saidaicmsst";
	
	protected String tipo; 
	protected Cfop cfop;
	protected Money valortotal = new Money();
	protected Money bc = new Money();
	protected Money impostocreditado = new Money();
	protected Money impostodebitado = new Money();
	protected Money isentanaotributada = new Money();
	protected Money outras = new Money();
	
	public Aux_apuracaoicms(){}
	
	public Aux_apuracaoicms(String tipo){
		this.tipo = tipo;
	}
	
	public String getTipo() {
		return tipo;
	}
	public Cfop getCfop() {
		return cfop;
	}
	public Money getValortotal() {
		return valortotal;
	}
	public Money getBc() {
		return bc;
	}
	public Money getImpostocreditado() {
		return impostocreditado;
	}
	public Money getImpostodebitado() {
		return impostodebitado;
	}
	public Money getIsentanaotributada() {
		return isentanaotributada;
	}
	public Money getOutras() {
		return outras;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setBc(Money bc) {
		this.bc = bc;
	}
	public void setImpostocreditado(Money impostocreditado) {
		this.impostocreditado = impostocreditado;
	}
	public void setImpostodebitado(Money impostodebitado) {
		this.impostodebitado = impostodebitado;
	}
	public void setIsentanaotributada(Money isentanaotributada) {
		this.isentanaotributada = isentanaotributada;
	}
	public void setOutras(Money outras) {
		this.outras = outras;
	}
}
