package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.enumeration.FormaemissaoMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModalidadefreteMdfe;

public class DamdfeBean {

	private Image logo;
	private String nomeEmpresa;
	private String cnpjEmpresa;
	private String ieEmpresa;
	private String logradouroEmpresa;
	private String numeroEmpresa;
	private String complementoEmpresa;
	private String bairroEmpresa;
	private String ufEmpresa;
	private String municipioEmpresa;
	private String cepEmpresa;
	
	private Image codigoBarra;
	private String chaveAcesso;
	private String protocoloAutorizacao;
	
	private String modelo;
	private String serie;
	private String numero;
	private String fl;
	private String dtEmissao;
	private String ufCarreg;
	private String ufDescar;
	
	private String qtdCte;
	private String qtdNfe;
	private String pesoTotal;
	private String tipoPeso;
	
	private String placa;
	private String rntrc;
	
	private List<DamdfeValePedagio> damdfeValePedagioList = new ArrayList<DamdfeValePedagio>();
	private List<DamdfeCondutor> damdfeCondutorList = new ArrayList<DamdfeCondutor>();
	
	private String observacao;
	
	private String marcaNacionalidadeAeronave;
	private String marcaMatriculaAeronave;
	private String numeroVoo;
	private String dataVoo;
	private String aerodromoEmbarque;
	private String aerodromoDestino;
	
	private String codigoEmbarcacao;
	private String nomeEmbarcacao;
	
	private String prefixoTrem;
	private String dtTrem;
	private String origemTrem;
	private String destinoTrem;
	private String qtdeVagoesCarregados;
	
	private List<DamdfeVagao> damdfeVagoesList;
	
	private List<DamdfeDadosTerminal> damdfeDadosTerminalCarregList;
	private List<DamdfeDadosTerminal> damdfeDadosTerminalDescarregList;
	private String qtdMdfe;
	
	private List<DamdfeUnidadeTranspCarga> damdfeUnidadeTranspCargaList;
	private List<DamdfeDocFiscTranspCarga> damdfeDocFiscTranspCargaList;
	
	private String situacao;
	
	private Image qrCode;
	
	public Image getLogo() {
		return logo;
	}

	public void setLogo(Image logo) {
		this.logo = logo;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getCnpjEmpresa() {
		return cnpjEmpresa;
	}

	public void setCnpjEmpresa(String cnpjEmpresa) {
		this.cnpjEmpresa = cnpjEmpresa;
	}
	
	public String getIeEmpresa() {
		return ieEmpresa;
	}
	
	public void setIeEmpresa(String ieEmpresa) {
		this.ieEmpresa = ieEmpresa;
	}
	
	public String getLogradouroEmpresa() {
		return logradouroEmpresa;
	}
	
	public void setLogradouroEmpresa(String logradouroEmpresa) {
		this.logradouroEmpresa = logradouroEmpresa;
	}

	public String getNumeroEmpresa() {
		return numeroEmpresa;
	}

	public void setNumeroEmpresa(String numeroEmpresa) {
		this.numeroEmpresa = numeroEmpresa;
	}

	public String getComplementoEmpresa() {
		return complementoEmpresa;
	}

	public void setComplementoEmpresa(String complementoEmpresa) {
		this.complementoEmpresa = complementoEmpresa;
	}

	public String getBairroEmpresa() {
		return bairroEmpresa;
	}

	public void setBairroEmpresa(String bairroEmpresa) {
		this.bairroEmpresa = bairroEmpresa;
	}

	public String getUfEmpresa() {
		return ufEmpresa;
	}

	public void setUfEmpresa(String ufEmpresa) {
		this.ufEmpresa = ufEmpresa;
	}

	public String getMunicipioEmpresa() {
		return municipioEmpresa;
	}

	public void setMunicipioEmpresa(String municipioEmpresa) {
		this.municipioEmpresa = municipioEmpresa;
	}

	public String getCepEmpresa() {
		return cepEmpresa;
	}
	
	public void setCepEmpresa(String cepEmpresa) {
		this.cepEmpresa = cepEmpresa;
	}
	
	public String getChaveAcesso() {
		return chaveAcesso;
	}
	
	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	
	public String getProtocoloAutorizacao() {
		return protocoloAutorizacao;
	}
	
	public void setProtocoloAutorizacao(String protocoloAutorizacao) {
		this.protocoloAutorizacao = protocoloAutorizacao;
	}
	
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getFl() {
		return fl;
	}

	public void setFl(String fl) {
		this.fl = fl;
	}

	public String getDtEmissao() {
		return dtEmissao;
	}

	public void setDtEmissao(String dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public String getUfCarreg() {
		return ufCarreg;
	}

	public void setUfCarreg(String ufCarreg) {
		this.ufCarreg = ufCarreg;
	}

	public String getUfDescar() {
		return ufDescar;
	}

	public void setUfDescar(String ufDescar) {
		this.ufDescar = ufDescar;
	}

	public String getQtdCte() {
		return qtdCte;
	}

	public void setQtdCte(String qtdCte) {
		this.qtdCte = qtdCte;
	}

	public String getQtdNfe() {
		return qtdNfe;
	}

	public void setQtdNfe(String qtdNfe) {
		this.qtdNfe = qtdNfe;
	}

	public String getPesoTotal() {
		return pesoTotal;
	}

	public void setPesoTotal(String pesoTotal) {
		this.pesoTotal = pesoTotal;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRntrc() {
		return rntrc;
	}

	public void setRntrc(String rntrc) {
		this.rntrc = rntrc;
	}

	public List<DamdfeCondutor> getDamdfeCondutorList() {
		return damdfeCondutorList;
	}
	
	public void setDamdfeCondutorList(List<DamdfeCondutor> damdfeCondutorList) {
		this.damdfeCondutorList = damdfeCondutorList;
	}
	
	public List<DamdfeValePedagio> getDamdfeValePedagioList() {
		return damdfeValePedagioList;
	}
	
	public void setDamdfeValePedagioList(List<DamdfeValePedagio> damdfeValePedagioList) {
		this.damdfeValePedagioList = damdfeValePedagioList;
	}

	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public Image getCodigoBarra() {
		return codigoBarra;
	}
	
	public void setCodigoBarra(Image codigoBarra) {
		this.codigoBarra = codigoBarra;
	}
	
	public String getMarcaNacionalidadeAeronave() {
		return marcaNacionalidadeAeronave;
	}

	public void setMarcaNacionalidadeAeronave(String marcaNacionalidadeAeronave) {
		this.marcaNacionalidadeAeronave = marcaNacionalidadeAeronave;
	}

	public String getMarcaMatriculaAeronave() {
		return marcaMatriculaAeronave;
	}

	public void setMarcaMatriculaAeronave(String marcaMatriculaAeronave) {
		this.marcaMatriculaAeronave = marcaMatriculaAeronave;
	}

	public String getNumeroVoo() {
		return numeroVoo;
	}

	public void setNumeroVoo(String numeroVoo) {
		this.numeroVoo = numeroVoo;
	}

	public String getDataVoo() {
		return dataVoo;
	}

	public void setDataVoo(String dataVoo) {
		this.dataVoo = dataVoo;
	}

	public String getAerodromoEmbarque() {
		return aerodromoEmbarque;
	}

	public void setAerodromoEmbarque(String aerodromoEmbarque) {
		this.aerodromoEmbarque = aerodromoEmbarque;
	}

	public String getAerodromoDestino() {
		return aerodromoDestino;
	}

	public void setAerodromoDestino(String aerodromoDestino) {
		this.aerodromoDestino = aerodromoDestino;
	}
	
	public String getCodigoEmbarcacao() {
		return codigoEmbarcacao;
	}
	
	public void setCodigoEmbarcacao(String codigoEmbarcacao) {
		this.codigoEmbarcacao = codigoEmbarcacao;
	}
	
	public String getNomeEmbarcacao() {
		return nomeEmbarcacao;
	}
	
	public void setNomeEmbarcacao(String nomeEmbarcacao) {
		this.nomeEmbarcacao = nomeEmbarcacao;
	}

	public String getPrefixoTrem() {
		return prefixoTrem;
	}

	public void setPrefixoTrem(String prefixoTrem) {
		this.prefixoTrem = prefixoTrem;
	}

	public String getDtTrem() {
		return dtTrem;
	}

	public void setDtTrem(String dtTrem) {
		this.dtTrem = dtTrem;
	}

	public String getOrigemTrem() {
		return origemTrem;
	}

	public void setOrigemTrem(String origemTrem) {
		this.origemTrem = origemTrem;
	}

	public String getDestinoTrem() {
		return destinoTrem;
	}

	public void setDestinoTrem(String destinoTrem) {
		this.destinoTrem = destinoTrem;
	}

	public String getQtdeVagoesCarregados() {
		return qtdeVagoesCarregados;
	}

	public void setQtdeVagoesCarregados(String qtdeVagoesCarregados) {
		this.qtdeVagoesCarregados = qtdeVagoesCarregados;
	}
	
	public List<DamdfeVagao> getDamdfeVagoesList() {
		return damdfeVagoesList;
	}
	
	public void setDamdfeVagoesList(List<DamdfeVagao> damdfeVagoesList) {
		this.damdfeVagoesList = damdfeVagoesList;
	}

	public String getQtdMdfe() {
		return qtdMdfe;
	}

	public void setQtdMdfe(String qtdMdfe) {
		this.qtdMdfe = qtdMdfe;
	}

	public List<DamdfeDadosTerminal> getDamdfeDadosTerminalCarregList() {
		return damdfeDadosTerminalCarregList;
	}

	public void setDamdfeDadosTerminalCarregList(List<DamdfeDadosTerminal> damdfeDadosTerminalCarregList) {
		this.damdfeDadosTerminalCarregList = damdfeDadosTerminalCarregList;
	}

	public List<DamdfeDadosTerminal> getDamdfeDadosTerminalDescarregList() {
		return damdfeDadosTerminalDescarregList;
	}

	public void setDamdfeDadosTerminalDescarregList(List<DamdfeDadosTerminal> damdfeDadosTerminalDescarregList) {
		this.damdfeDadosTerminalDescarregList = damdfeDadosTerminalDescarregList;
	}
	
	public List<DamdfeUnidadeTranspCarga> getDamdfeUnidadeTranspCargaList() {
		return damdfeUnidadeTranspCargaList;
	}
	
	public void setDamdfeUnidadeTranspCargaList(List<DamdfeUnidadeTranspCarga> damdfeUnidadeTranspCargaList) {
		this.damdfeUnidadeTranspCargaList = damdfeUnidadeTranspCargaList;
	}

	public String getSituacao() {
		return situacao;
	}
	
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	public List<DamdfeDocFiscTranspCarga> getDamdfeDocFiscTranspCargaList() {
		return damdfeDocFiscTranspCargaList;
	}
	
	public void setDamdfeDocFiscTranspCargaList(List<DamdfeDocFiscTranspCarga> damdfeDocFiscTranspCargaList) {
		this.damdfeDocFiscTranspCargaList = damdfeDocFiscTranspCargaList;
	}
	
	public String getTipoPeso() {
		return tipoPeso;
	}
	
	public void setTipoPeso(String tipoPeso) {
		this.tipoPeso = tipoPeso;
	}
	
	public Image getQrCode() {
		return qrCode;
	}
	
	public void setQrCode(Image qrCode) {
		this.qrCode = qrCode;
	}
	
	public Report makeReport(ModalidadefreteMdfe modalidadefreteMdfe, FormaemissaoMdfe formaEmissaoMdfe) {
		Report report = null;
		
		if (ModalidadefreteMdfe.RODOVIARIO.equals(modalidadefreteMdfe)) {
			if (FormaemissaoMdfe.CONTINGENCIA.equals(formaEmissaoMdfe)) {
				report = new Report("/fiscal/damdfe_rodo_contigencia");
				
				Report sub_valePedagio = new Report("/fiscal/damdfe_valePedagio_contigencia");
				Report sub_condutor = new Report("/fiscal/damdfe_condutor_contigencia");
				
				report.addSubReport("SUB_DAMDFE_VALEPEDAGIO", sub_valePedagio);
				report.addSubReport("SUB_DAMDFE_CONDUTOR", sub_condutor);
				
				if (damdfeDocFiscTranspCargaList != null && damdfeDocFiscTranspCargaList.size() == 0) {
					damdfeDocFiscTranspCargaList.add(new DamdfeDocFiscTranspCarga("", "", ""));
				}
				
				report.setDataSource(damdfeDocFiscTranspCargaList);
			} else {
				report = new Report("/fiscal/damdfe_rodo");
				
				Report sub_valePedagio = new Report("/fiscal/damdfe_valePedagio");
				Report sub_condutor = new Report("/fiscal/damdfe_condutor");
				
				report.addSubReport("SUB_DAMDFE_VALEPEDAGIO", sub_valePedagio);
				report.addSubReport("SUB_DAMDFE_CONDUTOR", sub_condutor);
			}
		} else if (ModalidadefreteMdfe.AEREO.equals(modalidadefreteMdfe)) {
			report = new Report("/fiscal/damdfe_aereo");
		} else if (ModalidadefreteMdfe.FERROVIARIO.equals(modalidadefreteMdfe)) {
			report = new Report("/fiscal/damdfe_ferro");
			report.setDataSource(damdfeVagoesList);
		} else if (ModalidadefreteMdfe.AQUAVIARIO.equals(modalidadefreteMdfe)) {
			report = new Report("/fiscal/damdfe_aqua");
			Report sub_dados_terminal = new Report("/fiscal/damdfe_dados_terminal");
			
			report.addSubReport("SUB_DAMDFE_DADOS_TERMINAL", sub_dados_terminal);
			
			if (damdfeUnidadeTranspCargaList != null && damdfeUnidadeTranspCargaList.size() == 0) {
				damdfeUnidadeTranspCargaList.add(new DamdfeUnidadeTranspCarga("", ""));
			}
			
			report.setDataSource(damdfeUnidadeTranspCargaList);
		}
		
		report.addParameter("logo", logo);
		report.addParameter("nomeEmpresa", nomeEmpresa);
		report.addParameter("cnpjEmpresa", cnpjEmpresa);
		report.addParameter("ieEmpresa", ieEmpresa);
		report.addParameter("logradouroEmpresa", logradouroEmpresa);
		report.addParameter("numeroEmpresa", numeroEmpresa);
		report.addParameter("complementoEmpresa", complementoEmpresa);
		report.addParameter("bairroEmpresa", bairroEmpresa);
		report.addParameter("ufEmpresa", ufEmpresa);
		report.addParameter("municipioEmpresa", municipioEmpresa);
		report.addParameter("cepEmpresa", cepEmpresa);
		
		report.addParameter("codigoBarra", codigoBarra);
		report.addParameter("chaveAcesso", chaveAcesso);
		report.addParameter("protocoloAutorizacao", protocoloAutorizacao);
		
		report.addParameter("modelo", modelo);
		report.addParameter("serie", serie);
		report.addParameter("numero", numero);
		report.addParameter("fl", fl);
		report.addParameter("dtEmissao", dtEmissao);
		report.addParameter("ufCarreg", ufCarreg);
		report.addParameter("ufDescar", ufDescar);
		
		report.addParameter("qtdCte", qtdCte);
		report.addParameter("qtdNfe", qtdNfe);
		report.addParameter("pesoTotal", pesoTotal);
		report.addParameter("tipoPeso", tipoPeso);
		
		report.addParameter("placa", placa);
		report.addParameter("rntrc", rntrc);
		
		report.addParameter("damdfeValePedagioList", damdfeValePedagioList);
		report.addParameter("damdfeCondutorList", damdfeCondutorList);
		
		report.addParameter("observacao", observacao);
		
		report.addParameter("marcaNacionalidadeAeronave", marcaNacionalidadeAeronave);
		report.addParameter("marcaMatriculaAeronave", marcaMatriculaAeronave);
		report.addParameter("numeroVoo", numeroVoo);
		report.addParameter("dataVoo", dataVoo);
		report.addParameter("aerodromoEmbarque", aerodromoEmbarque);
		report.addParameter("aerodromoDestino", aerodromoDestino);
		
		report.addParameter("prefixoTrem", prefixoTrem);
		report.addParameter("dtTrem", dtTrem);
		report.addParameter("origemTrem", origemTrem);
		report.addParameter("destinoTrem", destinoTrem);
		report.addParameter("qtdeVagoesCarregados", qtdeVagoesCarregados);
		
		report.addParameter("codigoEmbarcacao", codigoEmbarcacao);
		report.addParameter("nomeEmbarcacao", nomeEmbarcacao);
		report.addParameter("damdfeDadosTerminalCarregList", damdfeDadosTerminalCarregList);
		report.addParameter("damdfeDadosTerminalDescarregList", damdfeDadosTerminalDescarregList);
		report.addParameter("qtdMdfe", qtdMdfe);
		
		report.addParameter("situacao", situacao);
		
		report.addParameter("qrCode", qrCode);
		
		return report;
	}
}
