package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;

public class Saidafiscal implements Cloneable {
	
	private Integer cdnota;
	private String especie = "NF-e";
	private String serie;
	private String numero;
	private Integer dia;
	private String ufDestino;
	private Money valorContabil = new Money(0);
	private String fiscal;
	private String icmsIpi;
	private Money valorBC = new Money(0);
	private Double aliquota = 0D;
	private Money valorDebitado = new Money(0);
	private Money valorIsentasNaoTributadas = new Money(0);
	private Money valorOutras = new Money(0);
	private Money valorIcmsSt = new Money(0);
	private String observacao;
	
	public Integer getCdnota() {
		return cdnota;
	}
	public String getEspecie() {
		return especie;
	}
	public String getSerie() {
		return serie;
	}
	public String getNumero() {
		return numero;
	}
	public Integer getDia() {
		return dia;
	}
	public String getUfDestino() {
		return ufDestino;
	}
	public Money getValorContabil() {
		return valorContabil;
	}
	public String getFiscal() {
		return fiscal;
	}
	public String getIcmsIpi() {
		return icmsIpi;
	}
	public Money getValorBC() {
		return valorBC;
	}
	public Double getAliquota() {
		return aliquota;
	}
	public Money getValorDebitado() {
		return valorDebitado;
	}
	public Money getValorIsentasNaoTributadas() {
		return valorIsentasNaoTributadas;
	}
	public Money getValorOutras() {
		return valorOutras;
	}
	public Money getValorIcmsSt() {
		return valorIcmsSt;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getKey(){
		StringBuilder key = new StringBuilder();
		
		key.append(getCdnota()).append("-");
		key.append(Util.strings.emptyIfNull(getFiscal())).append("-");
		key.append(getIcmsIpi()!=null ? getIcmsIpi() : "").append("-");
		key.append(getAliquota()!=null ? getAliquota() : "");
		
		return key.toString();
	}
	public Saidafiscal getClone(){

		Saidafiscal clone = new Saidafiscal();
		clone.setCdnota(cdnota);
		clone.setEspecie(especie);
		clone.setSerie(serie);
		clone.setNumero(numero);
		clone.setDia(dia);
		clone.setUfDestino(ufDestino);
		clone.setValorContabil(valorContabil);
		clone.setFiscal(fiscal);
		clone.setIcmsIpi(icmsIpi);
		clone.setValorBC(valorBC);
		clone.setAliquota(aliquota);
		clone.setValorDebitado(valorDebitado);
		clone.setValorIsentasNaoTributadas(valorIsentasNaoTributadas);
		clone.setValorOutras(valorOutras);
		clone.setObservacao(observacao);
		
		return clone;
	}

	public void setCdnota(Integer cdnota) {
		this.cdnota = cdnota;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDia(Integer dia) {
		this.dia = dia;
	}
	public void setUfDestino(String ufDestino) {
		this.ufDestino = ufDestino;
	}
	public void setValorContabil(Money valorContabil) {
		this.valorContabil = valorContabil;
	}
	public void setFiscal(String fiscal) {
		this.fiscal = fiscal;
	}
	public void setIcmsIpi(String icmsIpi) {
		this.icmsIpi = icmsIpi;
	}
	public void setValorBC(Money valorBC) {
		this.valorBC = valorBC;
	}
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}
	public void setValorDebitado(Money valorDebitado) {
		this.valorDebitado = valorDebitado;
	}
	public void setValorIsentasNaoTributadas(Money valorIsentasNaoTributadas) {
		this.valorIsentasNaoTributadas = valorIsentasNaoTributadas;
	}
	public void setValorOutras(Money valorOutras) {
		this.valorOutras = valorOutras;
	}
	public void setValorIcmsSt(Money valorIcmsSt) {
		this.valorIcmsSt = valorIcmsSt;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void somarCampos(Saidafiscal bean){
		valorContabil = valorContabil.add(bean.getValorContabil());
		valorBC = valorBC.add(bean.getValorBC());
		valorDebitado = valorDebitado.add(bean.getValorDebitado());
		valorIsentasNaoTributadas = valorIsentasNaoTributadas.add(bean.getValorIsentasNaoTributadas());
		valorOutras = valorOutras.add(bean.getValorOutras());
		valorIcmsSt = valorIcmsSt.add(bean.getValorIcmsSt());
	}
}