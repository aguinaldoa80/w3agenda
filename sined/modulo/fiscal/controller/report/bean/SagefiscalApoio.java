package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.sagefiscal.registros.RegistroE022;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.utils.SagefiscalUtil;

public class SagefiscalApoio {
	
	private List<Integer> listaCliente;
	private List<SageClienteEndereco> listaClienteEndereco;
	private List<Integer> listaFornecedor;
	private List<Integer> listaProdutoServico;
	private List<Integer> listaServico;
	private List<String> listaUnidadeComercial;
	private List<Integer> listaMaterialgrupo;
	private List<Integer> listaNaturezaOperacao;
	private List<RegistroE022> listaRegistroE022;
	
	private Map<Integer, Tipocobrancapis> mapMaterialTipocobrancapis;
	private Map<Integer, Tipocobrancacofins> mapMaterialTipocobrancacofins;
	
	public SagefiscalApoio() {
		listaCliente = new ArrayList<Integer>();
		listaClienteEndereco = new ArrayList<SageClienteEndereco>();
		listaFornecedor = new ArrayList<Integer>();
		listaProdutoServico = new ArrayList<Integer>();
		listaServico = new ArrayList<Integer>();
		listaUnidadeComercial = new ArrayList<String>();
		listaMaterialgrupo = new ArrayList<Integer>();
		listaNaturezaOperacao = new ArrayList<Integer>();
		listaRegistroE022 = new ArrayList<RegistroE022>();
		
		mapMaterialTipocobrancapis = new HashMap<Integer, Tipocobrancapis>();
		mapMaterialTipocobrancacofins = new HashMap<Integer, Tipocobrancacofins>();
	}
	
	public String addCliente(Integer codigo, String cpfcnpj){
		if(!listaCliente.contains(codigo)){
			listaCliente.add(codigo);
		}
		return cpfcnpj != null ? cpfcnpj : codigo.toString();
	}
	
	public void addClienteEndereco(Integer codigoCliente, Integer codigoEndereco, Date dtalteracao){
		SageClienteEndereco sageClienteEndereco = new SageClienteEndereco(codigoCliente, codigoEndereco, dtalteracao);
		if(!listaCliente.contains(codigoCliente)){
			listaCliente.add(codigoCliente);
		}
		if(!listaClienteEndereco.contains(sageClienteEndereco)){
			listaClienteEndereco.add(sageClienteEndereco);
		} else {
			int idx = listaClienteEndereco.indexOf(sageClienteEndereco);
			Date dtalteracao_aux = listaClienteEndereco.get(idx).getDtalteracao();
			if(SinedDateUtils.afterIgnoreHour(dtalteracao, dtalteracao_aux)){
				listaClienteEndereco.get(idx).setDtalteracao(dtalteracao);
			}
		}
	}
	
	public String addFornecedor(Integer codigo, String cpfcnpj){
		if(!listaFornecedor.contains(codigo)){
			listaFornecedor.add(codigo);
		}
		return cpfcnpj != null ? cpfcnpj : codigo.toString();
	}
	
	public String addProdutoServico(Integer codigo, Tipocobrancapis tipocobrancapis, Tipocobrancacofins tipocobrancacofins){
		if(!listaProdutoServico.contains(codigo)){
			listaProdutoServico.add(codigo);
		}
		if(tipocobrancapis != null && mapMaterialTipocobrancapis.get(codigo) == null){
			mapMaterialTipocobrancapis.put(codigo, tipocobrancapis);
		}
		if(tipocobrancacofins != null && mapMaterialTipocobrancacofins.get(codigo) == null){
			mapMaterialTipocobrancacofins.put(codigo, tipocobrancacofins);
		}
		return codigo.toString();
	}
	
	public String addServico(Integer codigo, Tipocobrancapis tipocobrancapis, Tipocobrancacofins tipocobrancacofins){
		if(!listaServico.contains(codigo)){
			listaServico.add(codigo);
		}
		
		if(tipocobrancapis != null && mapMaterialTipocobrancapis.get(codigo) == null){
			mapMaterialTipocobrancapis.put(codigo, tipocobrancapis);
		}
		if(tipocobrancacofins != null && mapMaterialTipocobrancacofins.get(codigo) == null){
			mapMaterialTipocobrancacofins.put(codigo, tipocobrancacofins);
		}
		return "1" + SagefiscalUtil.printInteger(codigo, 19);
	}
	
	public String addUnidadeComercial(String codigo){
		if(!listaUnidadeComercial.contains(codigo)){
			listaUnidadeComercial.add(codigo);
		}
		return codigo.toString();
	}
	
	public String addMaterialgrupo(Integer codigo){
		if(!listaMaterialgrupo.contains(codigo)){
			listaMaterialgrupo.add(codigo);
		}
		return codigo.toString();
	}
	
	public String addNaturezaOperacao(Integer codigo){
		if(!listaNaturezaOperacao.contains(codigo)){
			listaNaturezaOperacao.add(codigo);
		}
		return codigo.toString();
	}
	
	public void addRegistroE022(RegistroE022 registroE022){
		listaRegistroE022.add(registroE022);
	}

	public List<Integer> getListaCliente() {
		return listaCliente;
	}

	public List<Integer> getListaFornecedor() {
		return listaFornecedor;
	}

	public List<Integer> getListaProdutoServico() {
		return listaProdutoServico;
	}

	public List<String> getListaUnidadeComercial() {
		return listaUnidadeComercial;
	}

	public List<Integer> getListaNaturezaOperacao() {
		return listaNaturezaOperacao;
	}
	
	public List<Integer> getListaServico() {
		return listaServico;
	}
	
	public List<RegistroE022> getListaRegistroE022() {
		return listaRegistroE022;
	}
	
	public void setListaRegistroE022(List<RegistroE022> listaRegistroE022) {
		this.listaRegistroE022 = listaRegistroE022;
	}
	
	public void setListaServico(List<Integer> listaServico) {
		this.listaServico = listaServico;
	}

	public void setListaCliente(List<Integer> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public void setListaFornecedor(List<Integer> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}

	public void setListaProdutoServico(List<Integer> listaProdutoServico) {
		this.listaProdutoServico = listaProdutoServico;
	}

	public void setListaUnidadeComercial(List<String> listaUnidadeComercial) {
		this.listaUnidadeComercial = listaUnidadeComercial;
	}

	public void setListaNaturezaOperacao(List<Integer> listaNaturezaOperacao) {
		this.listaNaturezaOperacao = listaNaturezaOperacao;
	}

	public List<Integer> getListaMaterialgrupo() {
		return listaMaterialgrupo;
	}
	
	public List<SageClienteEndereco> getListaClienteEndereco() {
		return listaClienteEndereco;
	}
	
	public void setListaClienteEndereco(
			List<SageClienteEndereco> listaClienteEndereco) {
		this.listaClienteEndereco = listaClienteEndereco;
	}

	public void setListaMaterialgrupo(List<Integer> listaMaterialgrupo) {
		this.listaMaterialgrupo = listaMaterialgrupo;
	}

	public Map<Integer, Tipocobrancapis> getMapMaterialTipocobrancapis() {
		return mapMaterialTipocobrancapis;
	}

	public Map<Integer, Tipocobrancacofins> getMapMaterialTipocobrancacofins() {
		return mapMaterialTipocobrancacofins;
	}

	public void setMapMaterialTipocobrancapis(Map<Integer, Tipocobrancapis> mapMaterialTipocobrancapis) {
		this.mapMaterialTipocobrancapis = mapMaterialTipocobrancapis;
	}

	public void setMapMaterialTipocobrancacofins(Map<Integer, Tipocobrancacofins> mapMaterialTipocobrancacofins) {
		this.mapMaterialTipocobrancacofins = mapMaterialTipocobrancacofins;
	}
}
