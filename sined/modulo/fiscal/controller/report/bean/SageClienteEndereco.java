package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import java.sql.Date;

public class SageClienteEndereco {
	
	private Integer codigoCliente;
	private Integer codigoEndereco;
	private Date dtalteracao;
	
	public SageClienteEndereco(Integer codigoCliente, Integer codigoEndereco, Date dtalteracao) {
		this.codigoCliente = codigoCliente;
		this.codigoEndereco = codigoEndereco;
		this.dtalteracao = dtalteracao;
	}
	
	public Integer getCodigoCliente() {
		return codigoCliente;
	}
	public Integer getCodigoEndereco() {
		return codigoEndereco;
	}
	public Date getDtalteracao() {
		return dtalteracao;
	}
	public void setDtalteracao(Date dtalteracao) {
		this.dtalteracao = dtalteracao;
	}
	public void setCodigoCliente(Integer codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public void setCodigoEndereco(Integer codigoEndereco) {
		this.codigoEndereco = codigoEndereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoCliente == null) ? 0 : codigoCliente.hashCode());
		result = prime * result
				+ ((codigoEndereco == null) ? 0 : codigoEndereco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SageClienteEndereco other = (SageClienteEndereco) obj;
		if (codigoCliente == null) {
			if (other.codigoCliente != null)
				return false;
		} else if (!codigoCliente.equals(other.codigoCliente))
			return false;
		if (codigoEndereco == null) {
			if (other.codigoEndereco != null)
				return false;
		} else if (!codigoEndereco.equals(other.codigoEndereco))
			return false;
		return true;
	}
	
}
