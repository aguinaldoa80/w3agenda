package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaInfoContribuinteBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.EmpresaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GrupoTributacaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.NaturezaOperacaoBean;

public class InformacaoContribuinteNfseBean {

	private EmpresaBean empresa;
	private ClienteBean cliente;
	private GrupoTributacaoBean grupoTributacao;
	private NaturezaOperacaoBean naturezaoperacao;
	private LinkedList<VendaInfoContribuinteBean> vendas;
	
	public EmpresaBean getEmpresa() {
		return empresa;
	}
	public void setEmpresa(EmpresaBean empresa) {
		this.empresa = empresa;
	}
	
	public ClienteBean getCliente() {
		return cliente;
	}
	public void setCliente(ClienteBean cliente) {
		this.cliente = cliente;
	}
	
	public GrupoTributacaoBean getGrupoTributacao() {
		return grupoTributacao;
	}
	public void setGrupoTributacao(GrupoTributacaoBean grupoTributacao) {
		this.grupoTributacao = grupoTributacao;
	}
	
	public NaturezaOperacaoBean getNaturezaoperacao() {
		return naturezaoperacao;
	}
	public void setNaturezaoperacao(NaturezaOperacaoBean naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	
	public LinkedList<VendaInfoContribuinteBean> getVendas() {
		return vendas;
	}
	public void setVendas(LinkedList<VendaInfoContribuinteBean> vendas) {
		this.vendas = vendas;
	}
}
