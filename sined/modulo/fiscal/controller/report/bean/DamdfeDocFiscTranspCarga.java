package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

public class DamdfeDocFiscTranspCarga {

	private String docFisc;
	private String unidTransp;
	private String unidCarga;

	public DamdfeDocFiscTranspCarga() {}
	
	public DamdfeDocFiscTranspCarga(String docFisc, String unidTransp, String unidCarga) {
		this.docFisc = docFisc;
		this.unidTransp = unidTransp;
		this.unidCarga = unidCarga;
	}
	
	public String getDocFisc() {
		return docFisc;
	}

	public void setDocFisc(String docFisc) {
		this.docFisc = docFisc;
	}

	public String getUnidTransp() {
		return unidTransp;
	}

	public void setUnidTransp(String unidTransp) {
		this.unidTransp = unidTransp;
	}

	public String getUnidCarga() {
		return unidCarga;
	}

	public void setUnidCarga(String unidCarga) {
		this.unidCarga = unidCarga;
	}
}
