package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaInfoContribuinteBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ColetaInfoContribuinteBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.EmpresaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GrupoTributacaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.NaturezaOperacaoBean;

public class InformacaoContribuinteNfeBean {

	private EmpresaBean empresa;
	private ClienteBean cliente;
	private GrupoTributacaoBean grupoTributacao;
	private NaturezaOperacaoBean naturezaoperacao;
	private LinkedList<VendaInfoContribuinteBean> vendas;
	private LinkedList<ColetaInfoContribuinteBean> coletas;
	private LinkedList<NotaItemBean> itensnota;
	private Money valorTotalDesoneracao;
	
	public EmpresaBean getEmpresa() {
		return empresa;
	}
	public void setEmpresa(EmpresaBean empresa) {
		this.empresa = empresa;
	}
	
	public ClienteBean getCliente() {
		return cliente;
	}
	public void setCliente(ClienteBean cliente) {
		this.cliente = cliente;
	}
	
	public GrupoTributacaoBean getGrupoTributacao() {
		return grupoTributacao;
	}
	public void setGrupoTributacao(GrupoTributacaoBean grupoTributacao) {
		this.grupoTributacao = grupoTributacao;
	}
	
	public NaturezaOperacaoBean getNaturezaoperacao() {
		return naturezaoperacao;
	}
	public void setNaturezaoperacao(NaturezaOperacaoBean naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public Money getValorTotalDesoneracao() {
		return valorTotalDesoneracao;
	}
	public void setValorTotalDesoneracao(Money valorTotalDesoneracao) {
		this.valorTotalDesoneracao = valorTotalDesoneracao;
	}
	public LinkedList<VendaInfoContribuinteBean> getVendas() {
		return vendas;
	}
	public void setVendas(LinkedList<VendaInfoContribuinteBean> vendas) {
		this.vendas = vendas;
	}
	
	public LinkedList<ColetaInfoContribuinteBean> getColetas() {
		return coletas;
	}
	public void setColetas(LinkedList<ColetaInfoContribuinteBean> coletas) {
		this.coletas = coletas;
	}
	
	public LinkedList<NotaItemBean> getItensnota() {
		return itensnota;
	}
	public void setItensnota(LinkedList<NotaItemBean> itensnota) {
		this.itensnota = itensnota;
	}
}
