package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

public class DamdfeVagao {

	private String serieIdent;
	private String numeroIdent;
	private String seq;
	private String tonUtil;

	public String getSerieIdent() {
		return serieIdent;
	}

	public void setSerieIdent(String serieIdent) {
		this.serieIdent = serieIdent;
	}

	public String getNumeroIdent() {
		return numeroIdent;
	}

	public void setNumeroIdent(String numeroIdent) {
		this.numeroIdent = numeroIdent;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getTonUtil() {
		return tonUtil;
	}

	public void setTonUtil(String tonUtil) {
		this.tonUtil = tonUtil;
	}
}
