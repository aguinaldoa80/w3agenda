package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

public class DamdfeValePedagio {

	private String responsavelCnpj;
	private String fornecedoraCnpj;
	private String nComprovante;
	
	public String getResponsavelCnpj() {
		return responsavelCnpj;
	}
	
	public void setResponsavelCnpj(String responsavelCnpj) {
		this.responsavelCnpj = responsavelCnpj;
	}
	
	public String getFornecedoraCnpj() {
		return fornecedoraCnpj;
	}
	
	public void setFornecedoraCnpj(String fornecedoraCnpj) {
		this.fornecedoraCnpj = fornecedoraCnpj;
	}
	
	public String getnComprovante() {
		return nComprovante;
	}
	
	public void setnComprovante(String nComprovante) {
		this.nComprovante = nComprovante;
	}
}
