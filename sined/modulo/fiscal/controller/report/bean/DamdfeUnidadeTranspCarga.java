package br.com.linkcom.sined.modulo.fiscal.controller.report.bean;

public class DamdfeUnidadeTranspCarga {

	private String unidadeTransp;
	private String unidadeCarga;
	
	public DamdfeUnidadeTranspCarga() {
		
	}
	
	public DamdfeUnidadeTranspCarga(String unidadeTransp, String unidadeCarga) {
		this.unidadeTransp = unidadeTransp;
		this.unidadeCarga = unidadeCarga;
	}
	
	public String getUnidadeCarga() {
		return unidadeCarga;
	}
	
	public void setUnidadeCarga(String unidadeCarga) {
		this.unidadeCarga = unidadeCarga;
	}
	
	public String getUnidadeTransp() {
		return unidadeTransp;
	}
	
	public void setUnidadeTransp(String unidadeTransp) {
		this.unidadeTransp = unidadeTransp;
	}
}
