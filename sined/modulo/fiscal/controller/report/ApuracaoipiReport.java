package br.com.linkcom.sined.modulo.fiscal.controller.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoipiFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.Aux_apuracaoipi;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/fiscal/relatorio/ApuracaoipiReport", authorizationModule=ReportAuthorizationModule.class)
public class ApuracaoipiReport extends SinedReport<ApuracaoipiFiltro>{
	
	private EntradafiscalService entradafiscalService;
	private NotafiscalprodutoService notafiscalprodutoService;
	
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}


	@Override
	public IReport createReportSined(WebRequestContext request,	ApuracaoipiFiltro filtro) throws Exception {
		return this.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "apuracaoipi";
	}
	
	@Override
	public String getTitulo(ApuracaoipiFiltro filtro) {
		return "APURA��O DE IPI";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ApuracaoipiFiltro filtro) {
		Empresa empresa = getEmpresaService().load(filtro.getEmpresa(), "empresa.nome, empresa.razaosocial, empresa.nomefantasia, empresa.inscricaoestadual, empresa.cpf, empresa.cnpj");
		report.addParameter("EMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(empresa));
		report.addParameter("CNPJEMPRESA", empresa.getCnpj() != null ?  empresa.getCnpj().toString() : null);
		report.addParameter("INSCRICAOESTADUAL", empresa.getInscricaoestadual() != null ? empresa.getInscricaoestadual() : "");
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}
	
	/**
	 * M�todo que gera o relat�rio de apura��o de IPI
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	private IReport gerarRelatorio(ApuracaoipiFiltro filtro) {		
		List<Aux_apuracaoipi> listaApuracaoipiEntrada = new ArrayList<Aux_apuracaoipi>();
		List<Aux_apuracaoipi> listaApuracaoipiSaida = new ArrayList<Aux_apuracaoipi>();
		
		List<Entregadocumento> listaEntregadocumento = null;
		if(filtro.getEntrada() != null && filtro.getEntrada()){
			listaEntregadocumento = entradafiscalService.findForApuracaoipi(filtro);
		}
		
		List<Notafiscalproduto> listaNotafiscalproduto = null;
		if((filtro.getSaida() != null && filtro.getSaida()) || (filtro.getEntrada() != null && filtro.getEntrada())){
			listaNotafiscalproduto = notafiscalprodutoService.findForApuracaoipi(filtro);
		}
		
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				for(Entregamaterial em : ed.getListaEntregamaterial()){
					if(em.getCfop() != null){
						ValorFiscal valorFiscalIpi = em.getValorFiscalIpi();
						if(valorFiscalIpi != null){
							if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi) && em.getValoripi().getValue().doubleValue() <= 0){
								valorFiscalIpi = ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS;
							}
							
							Aux_apuracaoipi auxApuracaoipi = new Aux_apuracaoipi(Aux_apuracaoipi.ENTRADA);					
							auxApuracaoipi.setCfop(em.getCfop());
							
							if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(valorFiscalIpi)){
								auxApuracaoipi.setBc(em.getValorbcipi() != null ? em.getValorbcipi() : null);
								auxApuracaoipi.setImpostocreditado(em.getValoripi() != null ? em.getValoripi() : null);
								
								Double valorbcipi = em.getValorbcipi() != null ? em.getValorbcipi().getValue().doubleValue() : 0d;
								Double valor = em.getValortotaloperacao(false) != null ? em.getValortotaloperacao(false) : 0d;
								double valoroutrasipi = valor - valorbcipi;
								if(valoroutrasipi > 0){
									auxApuracaoipi.setOutras(new Money(valoroutrasipi));
								}
							}
							if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(valorFiscalIpi)){
								auxApuracaoipi.setIsentanaotributada(new Money(em.getValortotaloperacao()));
							}
							if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(valorFiscalIpi)){
								auxApuracaoipi.setOutras(new Money(em.getValortotaloperacao()));
							}
							auxApuracaoipi.setValortotal(new Money(em.getValortotaloperacao()));
							
							this.addAuxApuracaoipi(listaApuracaoipiEntrada, auxApuracaoipi);
						}
					}
				}
			}
		}
		
		if(listaNotafiscalproduto != null && !listaNotafiscalproduto.isEmpty()){
			for(Notafiscalproduto nfp : listaNotafiscalproduto){
				for(Notafiscalprodutoitem item : nfp.getListaItens()){
					if(item.getCfop() != null){
						if(item.getValorFiscalIpi() != null){
							boolean entrada = Tipooperacaonota.ENTRADA.equals(nfp.getTipooperacaonota());
							
							Aux_apuracaoipi auxApuracaoipi = new Aux_apuracaoipi(entrada ? Aux_apuracaoipi.ENTRADA : Aux_apuracaoipi.SAIDA);					
							auxApuracaoipi.setCfop(item.getCfop());
							
							if(ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(item.getValorFiscalIpi())){
								auxApuracaoipi.setBc(item.getValorbcipi() != null ? item.getValorbcipi() : null);
								if(entrada){
									auxApuracaoipi.setImpostocreditado(item.getValoripi() != null ? item.getValoripi() : null);
								} else {
									auxApuracaoipi.setImpostodebitado(item.getValoripi() != null ? item.getValoripi() : null);
								}
								
								Double valorbcipi = item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0d;
								Double valor = item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : 0d;
								double valoroutrasipi = valor - valorbcipi;
								if(valoroutrasipi > 0){
									auxApuracaoipi.setOutras(new Money(valoroutrasipi));
								}
							}
							if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(item.getValorFiscalIpi())){
								auxApuracaoipi.setIsentanaotributada(item.getValorbruto());
							}
							if(ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(item.getValorFiscalIpi())){
								auxApuracaoipi.setOutras(item.getValorbruto());
							}
							auxApuracaoipi.setValortotal(item.getValorbruto());
							
							if(entrada){
								this.addAuxApuracaoipi(listaApuracaoipiEntrada, auxApuracaoipi);
							}else {
								this.addAuxApuracaoipi(listaApuracaoipiSaida, auxApuracaoipi);
							}
						}
					}
				}
			}
		}
		
		Collections.sort(listaApuracaoipiEntrada,new Comparator<Aux_apuracaoipi>(){
			public int compare(Aux_apuracaoipi o1, Aux_apuracaoipi o2) {
				return o1.getCfop().getCodigo().compareTo(o2.getCfop().getCodigo());
			}
		});
		
		Collections.sort(listaApuracaoipiSaida,new Comparator<Aux_apuracaoipi>(){
			public int compare(Aux_apuracaoipi o1, Aux_apuracaoipi o2) {
				return o1.getCfop().getCodigo().compareTo(o2.getCfop().getCodigo());
			}
		});
		
		Report report = new Report("/fiscal/apuracaoipi");
		Report reportEntrada = new Report("/fiscal/apuracaoipientrada_sub");
		Report reportSaida = new Report("/fiscal/apuracaoipisaida_sub");
		
		Money somaimpostodebitado = new Money();
		Money subtotaldebitos = new Money();
		Money somaimpostocreditado = new Money();
		Money subtotalcreditos = new Money();
		Money saldoimposto = new Money();
		
		this.criaParametrosipi(reportEntrada, listaApuracaoipiEntrada, Aux_apuracaoipi.ENTRADA);
		this.criaParametrosipi(reportSaida, listaApuracaoipiSaida, Aux_apuracaoipi.SAIDA);
		
		for(Aux_apuracaoipi apuracaoipi : listaApuracaoipiEntrada){
			somaimpostocreditado = somaimpostocreditado.add(apuracaoipi.getImpostocreditado());
		}
		for(Aux_apuracaoipi apuracaoipi : listaApuracaoipiSaida){
			somaimpostodebitado = somaimpostodebitado.add(apuracaoipi.getImpostodebitado());
		}
		
		saldoimposto = somaimpostocreditado.subtract(somaimpostodebitado);
		
		report.addSubReport("SUB_APURACAOIPIENTRADA", reportEntrada);
		report.addSubReport("SUB_APURACAOIPISAIDA", reportSaida);
		
		report.addParameter("LISTAAPURACAOIPIENTRADA", listaApuracaoipiEntrada);
		report.addParameter("LISTAAPURACAOIPISAIDA", listaApuracaoipiSaida);
		
		report.addParameter("SOMAIMPOSTODEBITADO", somaimpostodebitado);
		report.addParameter("SUBTOTALDEBITOS", subtotaldebitos);
		report.addParameter("SOMAIMPOSTOCREDITADO", somaimpostocreditado);
		report.addParameter("SUBTOTALCREDITOS", subtotalcreditos);
		report.addParameter("SALDOIMPOSTO", saldoimposto);
		
		return report;
	}
	
	/**
	 * M�todo que cria os parametros do sub-relat�rio de entrada e saida 
	 *
	 * @param report
	 * @param listaApuracaoipi
	 * @param tipo
	 * @author Luiz Fernando
	 */
	private void criaParametrosipi(Report report, List<Aux_apuracaoipi> listaApuracaoipi, String tipo) {
		
		Money valortotalcontabilestado = new Money();
		Money valortotalcontabiloutrosestados = new Money();
		Money valortotalcontabilexterior = new Money();
		Money valortotalcontabil = new Money();
		
		Money valortotalbcestado = new Money();
		Money valortotalbcoutrosestados = new Money();
		Money valortotalbcexterior = new Money();
		Money valortotalbc = new Money();
		
		Money valortotalicestado = new Money();
		Money valortotalicoutrosestados = new Money();
		Money valortotalicexterior = new Money();
		Money valortotalic = new Money();
		
		Money valortotalidestado = new Money();
		Money valortotalidoutrosestados = new Money();
		Money valortotalidexterior = new Money();
		Money valortotalid = new Money();
		
		Money valortotalintestado = new Money();
		Money valortotalintoutrosestados = new Money();
		Money valortotalintexterior = new Money();
		Money valortotalint = new Money();
		
		Money valortotaloutrasestado = new Money();
		Money valortotaloutrasoutrosestados = new Money();
		Money valortotaloutrasexterior = new Money();
		Money valortotaloutras = new Money();
		
		Integer qtdeestado = 0;
		Integer qtdeoutrosestados = 0;
		Integer qtdeexterior = 0;
		
		for(Aux_apuracaoipi auxApuracaoipi : listaApuracaoipi){
			if(tipo.equals(auxApuracaoipi.getTipo())){
				if(Cfopescopo.ESTADUAL.getCdcfopescopo().equals(auxApuracaoipi.getCfop().getCfopescopo().getCdcfopescopo())){
					valortotalbcestado = valortotalbcestado.add(auxApuracaoipi.getBc());
					if(tipo.equals(Aux_apuracaoipi.ENTRADA)){
						valortotalicestado = valortotalicestado.add(auxApuracaoipi.getImpostocreditado());
					}else if(tipo.equals(Aux_apuracaoipi.SAIDA)){
						valortotalidestado = valortotalidestado.add(auxApuracaoipi.getImpostodebitado());
					}
					valortotalintestado = valortotalintestado.add(auxApuracaoipi.getIsentanaotributada());
					valortotaloutrasestado = valortotaloutrasestado.add(auxApuracaoipi.getOutras());
					valortotalcontabilestado = valortotalcontabilestado.add(auxApuracaoipi.getValortotal());
					qtdeestado++;
				}else if(Cfopescopo.FORA_DO_ESTADO.getCdcfopescopo().equals(auxApuracaoipi.getCfop().getCfopescopo().getCdcfopescopo())){
					valortotalbcoutrosestados = valortotalbcoutrosestados.add(auxApuracaoipi.getBc());
					if(tipo.equals(Aux_apuracaoipi.ENTRADA)){
						valortotalicoutrosestados = valortotalicoutrosestados.add(auxApuracaoipi.getImpostocreditado());
					}else if(tipo.equals(Aux_apuracaoipi.SAIDA)){
						valortotalidoutrosestados = valortotalidoutrosestados.add(auxApuracaoipi.getImpostodebitado());
					}					
					valortotalintoutrosestados = valortotalintoutrosestados.add(auxApuracaoipi.getIsentanaotributada());
					valortotaloutrasoutrosestados = valortotaloutrasoutrosestados.add(auxApuracaoipi.getOutras());
					valortotalcontabiloutrosestados = valortotalcontabiloutrosestados.add(auxApuracaoipi.getValortotal());
					qtdeoutrosestados++;
				}else if(Cfopescopo.INTERNACIONAL.getCdcfopescopo().equals(auxApuracaoipi.getCfop().getCfopescopo().getCdcfopescopo())){
					valortotalbcestado = valortotalbcestado.add(auxApuracaoipi.getBc());
					if(tipo.equals(Aux_apuracaoipi.ENTRADA)){
						valortotalicexterior = valortotalicexterior.add(auxApuracaoipi.getImpostocreditado());
					}else if(tipo.equals(Aux_apuracaoipi.SAIDA)){
						valortotalidexterior = valortotalidexterior.add(auxApuracaoipi.getImpostodebitado());
					}
					valortotalintexterior = valortotalintexterior.add(auxApuracaoipi.getIsentanaotributada());
					valortotaloutrasexterior = valortotaloutrasexterior.add(auxApuracaoipi.getOutras());
					valortotalcontabilexterior = valortotalcontabilexterior.add(auxApuracaoipi.getValortotal());
					qtdeexterior++;
				}
			}
		}

		valortotalbc = valortotalbcestado.add(valortotalbcoutrosestados).add(valortotalbcexterior);
		if(tipo.equals(Aux_apuracaoipi.ENTRADA)){
			valortotalic = valortotalicestado.add(valortotalicoutrosestados).add(valortotalicexterior);
		}else if(tipo.equals(Aux_apuracaoipi.SAIDA)){
			valortotalid = valortotalidestado.add(valortotalidoutrosestados).add(valortotalidexterior);
		}		
		valortotalint = valortotalintestado.add(valortotalintoutrosestados).add(valortotalintexterior);
		valortotaloutras = valortotaloutrasestado.add(valortotaloutrasoutrosestados).add(valortotaloutrasexterior);
		valortotalcontabil = valortotalcontabilestado.add(valortotalcontabiloutrosestados).add(valortotalcontabilexterior);
		
		//VALORES CONTABEIS
		report.addParameter("VALORTOTALCONTABILESTADO" + tipo.toUpperCase(), valortotalcontabilestado);
		report.addParameter("VALORTOTALCONTABILOUTROSESTADOS"+ tipo.toUpperCase(), valortotalcontabiloutrosestados);
		report.addParameter("VALORTOTALCONTABILEXTERIOR"+ tipo.toUpperCase(), valortotalcontabilexterior);
		
		//VALORES BC
		report.addParameter("VALORTOTALBCESTADO"+ tipo.toUpperCase(), valortotalbcestado);
		report.addParameter("VALORTOTALBCOUTROSESTADOS"+ tipo.toUpperCase(), valortotalbcoutrosestados);
		report.addParameter("VALORTOTALBCEXTERIOR"+ tipo.toUpperCase(), valortotalbcexterior);
		
		//VALORES IMPOSTO CREDITADO/DEBITADO
		if(tipo.equals(Aux_apuracaoipi.ENTRADA)){
			report.addParameter("VALORTOTALICESTADO"+ tipo.toUpperCase(), valortotalicestado);
			report.addParameter("VALORTOTALICOUTROSESTADOS"+ tipo.toUpperCase(), valortotalicoutrosestados);
			report.addParameter("VALORTOTALICEXTERIOR"+ tipo.toUpperCase(), valortotalicexterior);
		}else if(tipo.equals(Aux_apuracaoipi.SAIDA)){
			report.addParameter("VALORTOTALIDESTADO"+ tipo.toUpperCase(), valortotalidestado);
			report.addParameter("VALORTOTALIDOUTROSESTADOS"+ tipo.toUpperCase(), valortotalidoutrosestados);
			report.addParameter("VALORTOTALIDEXTERIOR"+ tipo.toUpperCase(), valortotalidexterior);
		}
		
		//VALORES ISENTAS OU N�O TRIBUTADAS
		report.addParameter("VALORTOTALINTESTADO"+ tipo.toUpperCase(), valortotalintestado);
		report.addParameter("VALORTOTALINTOUTROSESTADOS"+ tipo.toUpperCase(), valortotalintoutrosestados);
		report.addParameter("VALORTOTALINTEXTERIOR"+ tipo.toUpperCase(), valortotalintexterior);
		
		//VALORES OUTRAS
		report.addParameter("VALORTOTALOUTRASESTADO"+ tipo.toUpperCase(), valortotaloutrasestado);
		report.addParameter("VALORTOTALOUTRASOUTROSESTADOS"+ tipo.toUpperCase(), valortotaloutrasoutrosestados);
		report.addParameter("VALORTOTALOUTRASEXTERIOR"+ tipo.toUpperCase(), valortotaloutrasexterior);
		
		
		//TOTAIS
		report.addParameter("VALORTOTALCONTABIL"+ tipo.toUpperCase(), valortotalcontabil);
		report.addParameter("VALORTOTALBC"+ tipo.toUpperCase(), valortotalbc);
		if(tipo.equals(Aux_apuracaoipi.ENTRADA)){
			report.addParameter("VALORTOTALIC"+ tipo.toUpperCase(), valortotalic);
		}else if(tipo.equals(Aux_apuracaoipi.SAIDA)){
			report.addParameter("VALORTOTALID"+ tipo.toUpperCase(), valortotalid);
		}
		report.addParameter("VALORTOTALINT"+ tipo.toUpperCase(), valortotalint);
		report.addParameter("VALORTOTALOUTRAS"+ tipo.toUpperCase(), valortotaloutras);
		
		report.addParameter("QTDEESTADOS"+ tipo.toUpperCase(), qtdeestado);
		report.addParameter("QTDEOUTROSESTADOS"+ tipo.toUpperCase(), qtdeoutrosestados);
		report.addParameter("QTDEEXTERIOR"+ tipo.toUpperCase(), qtdeexterior);
	}
	
	/**
	 * M�todo adiciona o cfop a lista - Se j� existe, soma-se os valores,  caso contr�rio, adiciona a lista
	 *
	 * @param listaApuracaoipi
	 * @param auxApuracaoipi
	 * @author Luiz Fernando
	 */
	private void addAuxApuracaoipi(List<Aux_apuracaoipi> listaApuracaoipi, Aux_apuracaoipi auxApuracaoipi) {
		boolean existe = false;
		if(listaApuracaoipi.size() > 0){
			for(Aux_apuracaoipi apuracaoipi : listaApuracaoipi){
				if(auxApuracaoipi.getTipo().equals(apuracaoipi.getTipo())){
					if(auxApuracaoipi.getCfop() != null){
						if(apuracaoipi.getCfop() != null && apuracaoipi.getCfop().equals(auxApuracaoipi.getCfop())){
							existe = true;
						}
					}else {
						if(apuracaoipi.getCfop() == null){
							existe = true;
						}
					}
					if(existe){
						apuracaoipi.setBc(apuracaoipi.getBc().add(auxApuracaoipi.getBc()));
						apuracaoipi.setImpostocreditado(apuracaoipi.getImpostocreditado().add(auxApuracaoipi.getImpostocreditado()));
						apuracaoipi.setImpostodebitado(apuracaoipi.getImpostodebitado().add(auxApuracaoipi.getImpostodebitado()));
						apuracaoipi.setIsentanaotributada(apuracaoipi.getIsentanaotributada().add(auxApuracaoipi.getIsentanaotributada()));
						apuracaoipi.setOutras(apuracaoipi.getOutras().add(auxApuracaoipi.getOutras()));
						apuracaoipi.setValortotal(apuracaoipi.getValortotal().add(auxApuracaoipi.getValortotal()));
						break;
					}
				}
			}
		}		
		if(!existe){
			listaApuracaoipi.add(auxApuracaoipi);
		}
	}	
}
