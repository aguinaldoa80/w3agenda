package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LoteConsultaDfeService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.SelecionarConfiguracaoBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeItem;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.TipoLoteConsultaDfeEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LoteConsultaDfeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/LoteConsultaDfe", authorizationModule=CrudAuthorizationModule.class)
public class LoteConsultaDfeCrud extends CrudControllerSined<LoteConsultaDfeFiltro, LoteConsultaDfe, LoteConsultaDfe> {

	private LoteConsultaDfeService loteConsultaDfeService;
	private ArquivoService arquivoService;
	private EmpresaService empresaService;
	private ConfiguracaonfeService configuracaoNfeService;
	
	public void setLoteConsultaDfeService(LoteConsultaDfeService loteConsultaDfeService) {
		this.loteConsultaDfeService = loteConsultaDfeService;
	}
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setConfiguracaoNfeService(ConfiguracaonfeService configuracaoNfeService) {
		this.configuracaoNfeService = configuracaoNfeService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, LoteConsultaDfeFiltro filtro) throws Exception {
		request.setAttribute("listaSituacaoEnum", SituacaoEmissorEnum.getTodos());
	}
	
	@Override
	protected void entrada(WebRequestContext request, LoteConsultaDfe form)	throws Exception {
		request.setAttribute("listaEmpresa", empresaService.findEmpresasConfiguradasNfe(form.getEmpresa()));
		if (form.getCdLoteConsultaDfe() != null) {
			List<LoteConsultaDfeHistorico> listaLoteConsultaDfeHistorico = loteConsultaDfeService.procurarHistoricoPorLoteConsultaDfe(form);
			
			if (listaLoteConsultaDfeHistorico != null && listaLoteConsultaDfeHistorico.size() > 0) {
				form.setListaLoteConsultaDfeHistorico(listaLoteConsultaDfeHistorico);
			} else {
				form.setListaLoteConsultaDfeHistorico(null);
			}
			
			List<LoteConsultaDfeItem> listaLoteConsultaDfeItem = loteConsultaDfeService.procurarItemPorLoteConsultaDfe(form);
			
			if (listaLoteConsultaDfeItem != null && listaLoteConsultaDfeItem.size() > 0) {
				List<LoteConsultaDfeItem> listaLoteConsultaDfeItemNfe = new ArrayList<LoteConsultaDfeItem>();
				List<LoteConsultaDfeItem> listaLoteConsultaDfeItemEvento = new ArrayList<LoteConsultaDfeItem>();
				
				for (LoteConsultaDfeItem loteConsultaDfeItem : listaLoteConsultaDfeItem) {
					if (StringUtils.isNotEmpty(loteConsultaDfeItem.getEvento())) {
						listaLoteConsultaDfeItemEvento.add(loteConsultaDfeItem);
					} else {
						listaLoteConsultaDfeItemNfe.add(loteConsultaDfeItem);
					}
				}
				
				if (listaLoteConsultaDfeItemNfe.size() > 0) {
					form.setListaLoteConsultaDfeItemNfe(listaLoteConsultaDfeItemNfe);
				} else {
					form.setListaLoteConsultaDfeItemNfe(null);
				}
				
				if (listaLoteConsultaDfeItemEvento.size() > 0) {
					form.setListaLoteConsultaDfeItemEvento(listaLoteConsultaDfeItemEvento);
				} else {
					form.setListaLoteConsultaDfeItemEvento(null);
				}
			}
		} else {
			form.setTipoLoteConsultaDfeEnum(TipoLoteConsultaDfeEnum.CHAVE_ACESSO);
		}
		
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, LoteConsultaDfe form) throws CrudException {
		String acao = request.getParameter("ACAO");
		LoteConsultaDfe loteConsultaDfe = loteConsultaDfeService.procurarSituacao(form);
		
		if (acao != null && "editar".equals(acao) && loteConsultaDfe != null && loteConsultaDfe.getSituacaoEnum() != null && !SituacaoEmissorEnum.EMITIDO.equals(loteConsultaDfe.getSituacaoEnum())) {
			request.addError("N�o � poss�vel realizar a altera��o, pois a situa��o do arquivo de lote � diferente de 'Emitido'.");
			SinedUtil.redirecionamento(request, "/fiscal/crud/LoteConsultaDfe");
			return null;
		} else if (acao != null && "consultar".equals(acao) && loteConsultaDfe != null && loteConsultaDfe.getSituacaoEnum() != null && !SituacaoEmissorEnum.EMITIDO.equals(loteConsultaDfe.getSituacaoEnum())) {
			request.setAttribute(CONSULTAR, true);
		}
		
		return super.doEditar(request, form);
	}
	
	@Override
	protected void excluir(WebRequestContext request, LoteConsultaDfe bean)	throws Exception {
		Boolean excluir = Boolean.TRUE;
		String itens = request.getParameter("itenstodelete");
		
		if (StringUtils.isNotEmpty(itens)) {
			if (loteConsultaDfeService.temLoteConsultaDfeDiferenteDe(itens, SituacaoEmissorEnum.EMITIDO)) {
				excluir = Boolean.FALSE;
				request.addError("N�o � poss�vel realizar a exclus�o, pois existe situa��o do arquivo de lote � diferente de 'Emitido'.");
			}
		} else {
			LoteConsultaDfe loteConsultaDfe = loteConsultaDfeService.procurarSituacao(bean);
			
			if (loteConsultaDfe.getSituacaoEnum() != null && !SituacaoEmissorEnum.EMITIDO.equals(loteConsultaDfe.getSituacaoEnum())) {
				excluir = Boolean.FALSE;
				request.addError("N�o � poss�vel realizar a exclus�o, pois a situa��o do arquivo de lote � diferente de 'Emitido'.");
			}
		}
		
		if (excluir) {
			super.excluir(request, bean);
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, LoteConsultaDfe bean) throws Exception {
		Boolean isCriado;
		
		if (bean.getSituacaoEnum() == null) {
			bean.setSituacaoEnum(SituacaoEmissorEnum.EMITIDO);
			bean.setDhEnvio(SinedDateUtils.currentTimestamp());
		}
		
		if (bean.getCdLoteConsultaDfe() == null) {
			isCriado = Boolean.TRUE;
		} else {
			isCriado = Boolean.FALSE;
		}
		
		super.salvar(request, bean);
		
		LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
		Usuario usuario = SinedUtil.getUsuarioLogado();
		loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
		
		if (isCriado) {
			loteConsultaDfeHistorico.setLoteConsultaDfe(bean);
			loteConsultaDfeHistorico.setObservacao("Lote de Consulta de DF-e criado.");
			loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.EMITIDO);
		} else {
			loteConsultaDfeHistorico.setLoteConsultaDfe(bean);
			loteConsultaDfeHistorico.setObservacao("Lote de Consulta de DF-e alterado.");
			loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.EMITIDO);
		}
		
		loteConsultaDfeService.salvarLoteConsultaDfeHistorico(loteConsultaDfeHistorico);
	}
	
	public ModelAndView selecionarConfiguracaoLoteConsultaDfe(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if (loteConsultaDfeService.temLoteConsultaDfeDiferenteDe(whereIn, SituacaoEmissorEnum.EMITIDO)) {
			request.addError("O Lote de Consulta de DF-e n�o pode ter situa��o diferente de Emitido.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if (loteConsultaDfeService.procurarEmpresasDoLoteConsultaDfe(whereIn)) {
			request.addError("Selecione apenas regitros de Lote de Consulta de DF-e de uma empresa.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		try {
			String[] cdLoteConsultaDfe = whereIn.split(",");
			
			LoteConsultaDfe loteConsultaDfe = loteConsultaDfeService.procurarPorId(Integer.parseInt(cdLoteConsultaDfe[0]));
			Empresa empresa = loteConsultaDfe.getEmpresa();

			boolean havePadrao = configuracaoNfeService.havePadrao(null, Tipoconfiguracaonfe.DDFE, empresa);
			
			SelecionarConfiguracaoBean bean = new SelecionarConfiguracaoBean();
			bean.setWhereIn(whereIn);
			bean.setEmpresa(empresa);
			
			if(!havePadrao){
				List<Configuracaonfe> listaConfiguracao = configuracaoNfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.DDFE, empresa);
				request.setAttribute("listaConfiguracao", listaConfiguracao);
				
				return new ModelAndView("direct:/crud/popup/selecionarConfiguracaoDde", "bean", bean);
			}
			
			Configuracaonfe configuracaonfePadrao = configuracaoNfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.DDFE, empresa);
			bean.setConfiguracaonfe(configuracaonfePadrao);
			
			return continueOnAction("criarLoteConsultaDfe", bean);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	public void criarLoteConsultaDfe(WebRequestContext request, SelecionarConfiguracaoBean bean) {
		String whereIn = bean.getWhereIn();
		bean.setEmpresa(empresaService.carregaEmpresa(bean.getEmpresa()));
		bean.setConfiguracaonfe(configuracaoNfeService.carregaConfiguracao(bean.getConfiguracaonfe()));
		
		if (StringUtils.isEmpty(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/fiscal/crud/LoteConsultaDfe");
		}
		
		String[] ids = whereIn.split(",");
		
		for (String id : ids) {
			LoteConsultaDfe loteConsultaDfe = loteConsultaDfeService.procurarPorId(Integer.parseInt(id));
			
			if (loteConsultaDfe != null) {
				String xml = loteConsultaDfeService.criarXmlLoteConsultaDfe(loteConsultaDfe, bean.getEmpresa(), bean.getConfiguracaonfe());
				
				Arquivo arquivoXml = new Arquivo(xml.getBytes(), "loteConsultaDfe_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				
				loteConsultaDfe.setArquivoXml(arquivoXml);
				loteConsultaDfe.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
				loteConsultaDfe.setConfiguracaoNfe(bean.getConfiguracaonfe());
				
				arquivoService.saveFile(loteConsultaDfe, "arquivoXml");
				
				arquivoService.saveOrUpdate(arquivoXml);
				
				loteConsultaDfeService.atualizarArquivoXml(loteConsultaDfe);
				loteConsultaDfeService.atualizarSituacaoEnum(loteConsultaDfe);
				loteConsultaDfeService.atualizarConfiguracaoNfe(loteConsultaDfe);
				
				LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
				
				Usuario usuario = SinedUtil.getUsuarioLogado();
				loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				loteConsultaDfeHistorico.setLoteConsultaDfe(loteConsultaDfe);
				loteConsultaDfeHistorico.setObservacao("Gerada a Distrubui��o de DF-e");
				loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
				
				loteConsultaDfeService.salvarLoteConsultaDfeHistorico(loteConsultaDfeHistorico);
			}
		}
		
		request.addMessage("Arquivo(s) de lote de consulta de DF-e criado(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/fiscal/crud/LoteConsultaDfe");
	}
}
