package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Operacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoFinanceiraVinculoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabilTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabildebitocreditoControleEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabildebitocreditoTipoEnum;

import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;

import br.com.linkcom.sined.geral.service.FormapagamentoService;

import br.com.linkcom.sined.geral.service.OperacaoContabilEventoPagamentoService;
import br.com.linkcom.sined.geral.service.OperacaocontabilService;
import br.com.linkcom.sined.geral.service.OperacaocontabildebitocreditoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.OperacaocontabilFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/contabil/crud/Operacaocontabil", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "tipo", "tipoLancamento", "contagerencial"})
public class OperacaocontabilCrud extends CrudControllerSined<OperacaocontabilFiltro, Operacaocontabil, Operacaocontabil> {	
	
	private OperacaocontabildebitocreditoService operacaocontabildebitocreditoService;
	private OperacaocontabilService operacaocontabilService;
	private ParametrogeralService parametrogeralService;
	private OperacaoContabilEventoPagamentoService operacaoContabilEventoPagamentoService;
	private NaturezaoperacaoService naturezaOperacaoService;
	private FormapagamentoService formapagamentoService;

	
	public void setNaturezaOperacaoService(
			NaturezaoperacaoService naturezaOperacaoService) {
		this.naturezaOperacaoService = naturezaOperacaoService;
	}
	public void setOperacaocontabildebitocreditoService(OperacaocontabildebitocreditoService operacaocontabildebitocreditoService) {
		this.operacaocontabildebitocreditoService = operacaocontabildebitocreditoService;
	}
	public void setOperacaocontabilService(OperacaocontabilService operacaocontabilService) {
		this.operacaocontabilService = operacaocontabilService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setOperacaoContabilEventoPagamentoService(
			OperacaoContabilEventoPagamentoService operacaoContabilEventoPagamentoService) {
		this.operacaoContabilEventoPagamentoService = operacaoContabilEventoPagamentoService;
	}
	public void setFormapagamentoService(
			FormapagamentoService formapagamentoService) {
		this.formapagamentoService = formapagamentoService;
	}

	@Override
	protected void entrada(WebRequestContext request, Operacaocontabil form) throws Exception {
		if (form.getCdoperacaocontabil()==null){
			if (form.getTipo()==null){
				form.setTipo(OperacaocontabilTipoEnum.UM_DEBITO_UM_CREDITO);
			}
			if (form.getTipoLancamento()==null){
				form.setTipoLancamento(OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO);
			}
		}else {
			form.setListaDebito(operacaocontabildebitocreditoService.findByOperacaocontabil(form, "operacaocontabildebito"));
			form.setListaCredito(operacaocontabildebitocreditoService.findByOperacaocontabil(form, "operacaocontabilcredito"));
			form.setListaEvento(operacaoContabilEventoPagamentoService.findByOperacaoContabil(form));
		}
		
		/*List<OperacaocontabildebitocreditoControleEnum> listaControle = new ArrayList<OperacaocontabildebitocreditoControleEnum>();
		
		if (OperacaocontabilTipoLancamentoEnum.APROPRIACAO.equals(form.getTipoLancamento())){
			listaControle = OperacaocontabildebitocreditoControleEnum.getListaControleApropriacao();
		}else if (OperacaocontabilTipoLancamentoEnum.PAGAMENTO.equals(form.getTipoLancamento())){
			listaControle = OperacaocontabildebitocreditoControleEnum.getListaControlePagamento();
		}
		
		request.setAttribute("listaControle", listaControle);*/
		request.setAttribute("PLANOCONTASCONTABILPOREMPRESA", parametrogeralService.getBoolean("PLANO_CONTAS_CONTABIL_POR_EMPRESA"));
//		request.setAttribute("listaTipoLancamento", OperacaoContabilTipoLancamento.getLista());
		request.setAttribute("listaTipoDebitoCredito", OperacaocontabildebitocreditoTipoEnum.getListaOrdenada());
		request.setAttribute("listaControleDebitoCredito", OperacaocontabildebitocreditoControleEnum.getListaOrdenada());
		request.setAttribute("descricaoVariaveisFormula", operacaocontabilService.getVariaveisFormula());
		request.setAttribute("listaFormaPagamento", formapagamentoService.findCredito());
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(Operacaocontabil bean, BindException errors) {
		if(SinedUtil.isListNotEmpty(bean.getListaCredito())){
			validaFormula(bean.getListaCredito(), errors);
		}else{
			errors.reject("001", "� obrigat�rio preencher o se��o D�bito.");
		}
		if(SinedUtil.isListNotEmpty(bean.getListaDebito())){
			validaFormula(bean.getListaDebito(), errors);
		}else{
			errors.reject("001", "� obrigat�rio preencher o se��o Cr�dito.");
		}
			validaCheckboxSped(errors, bean);			
	}
	
	private void validaCheckboxSped(BindException errors, Operacaocontabil bean) {
		int countCheck = 0;
		if(SinedUtil.isListNotEmpty(bean.getListaDebito())&& SinedUtil.isListNotEmpty(bean.getListaCredito())){
			for(Operacaocontabildebitocredito item : bean.getListaDebito()){
				if(Boolean.TRUE.equals(item.getSped())) countCheck++;
			}
			for(Operacaocontabildebitocredito item : bean.getListaCredito()){
				if(Boolean.TRUE.equals(item.getSped())) countCheck++;
			}
		}
		
		if(countCheck > 1){
			errors.reject("001", "N�o pode haver mais de um item com a marca��o SPED.");
		}
		
	}
	
	private void validaFormula(List<Operacaocontabildebitocredito> lista, BindException errors) {
		String erro = "";
		for(Operacaocontabildebitocredito item : lista){
			if(StringUtils.isNotBlank(item.getFormula())){
				try {
					Object resultado = null;
					ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
					
					String startsWithJs = 	"if (typeof String.prototype.startsWith != 'function') { " +
											" 	String.prototype.startsWith = function (str){ " +
											"    	return this.indexOf(str) == 0; " +
											"  	}; " +
											" } ";
					
					operacaocontabilService.setMapVariaveisFormula(engine);
					
					resultado = engine.eval(startsWithJs + item.getFormula());
					if (resultado != null && SinedException.class.equals(resultado.getClass()))
						throw (SinedException) resultado;
				} catch (ScriptException e) {
					if(!erro.contains("Express�o inv�lida: '" + item.getFormula() + "'.")){
						erro += "Express�o inv�lida: '" + item.getFormula() + "'.";
					}
				} catch (SinedException e) {
					errors.reject("001", e.getMessage());
				}
			}
		}
		if(erro.length() > 0){
			errors.reject("001", erro);
		}
	}
	
	@Override
	protected void listagem(WebRequestContext request, OperacaocontabilFiltro filtro) throws Exception {
		request.setAttribute("listaTipoLancamento", OperacaoContabilTipoLancamento.getLista());
		super.listagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<Operacaocontabil> getLista(WebRequestContext request, OperacaocontabilFiltro filtro) {
		ListagemResult<Operacaocontabil> listagemResult = super.getLista(request, filtro);
		List<Operacaocontabil> listaOperacaocontabil = listagemResult.list();
		
		for (Operacaocontabil operacaocontabil: listaOperacaocontabil){
			operacaocontabil.setListaDebito(operacaocontabildebitocreditoService.findByOperacaocontabil(operacaocontabil, "operacaocontabildebito"));
			operacaocontabil.setListaCredito(operacaocontabildebitocreditoService.findByOperacaocontabil(operacaocontabil, "operacaocontabilcredito"));
		}
		
		//Enuns
		if ("operacaocontabil.tipo".equals(request.getParameter("orderBy"))){
			final boolean ordenacaoTipoAsc = !"asc".equals(request.getSession().getAttribute(OperacaocontabilCrud.class.getName() + "ULTIMA_ORDENACAO_TIPO"));
			Collections.sort(listaOperacaocontabil, new Comparator<Operacaocontabil>() {
				@Override
				public int compare(Operacaocontabil o1, Operacaocontabil o2) {
					if (ordenacaoTipoAsc){
						return o1.getTipo().getNome().compareToIgnoreCase(o2.getTipo().getNome());
					}else {
						return o2.getTipo().getNome().compareToIgnoreCase(o1.getTipo().getNome());						
					}
				}
			});
			request.getSession().setAttribute(OperacaocontabilCrud.class.getName() + "ULTIMA_ORDENACAO_TIPO", ordenacaoTipoAsc ? "asc" : "desc");
		}
		
		if ("operacaocontabil.tipoLancamento".equals(request.getParameter("orderBy"))){
			final boolean ordenacaoTipoLancamentoAsc = !"asc".equals(request.getSession().getAttribute(OperacaocontabilCrud.class.getName() + "ULTIMA_ORDENACAO_TIPO_LANCAMENTO"));
			Collections.sort(listaOperacaocontabil, new Comparator<Operacaocontabil>() {
				@Override
				public int compare(Operacaocontabil o1, Operacaocontabil o2) {
					String nome1 = o1.getTipoLancamento()!=null ? o1.getTipoLancamento().getNome() : "";
					String nome2 = o2.getTipoLancamento()!=null ? o2.getTipoLancamento().getNome() : "";
					if (ordenacaoTipoLancamentoAsc){
						return nome1.compareToIgnoreCase(nome2);
					}else {
						return nome2.compareToIgnoreCase(nome1);						
					}
				}
			});
			request.getSession().setAttribute(OperacaocontabilCrud.class.getName() + "ULTIMA_ORDENACAO_TIPO_LANCAMENTO", ordenacaoTipoLancamentoAsc ? "asc" : "desc");
		}
		
		return listagemResult;
	}
	
	public ModelAndView popUpVariaveisBase(WebRequestContext request) {
		List<GenericBean> listaVariavel = new ArrayList<GenericBean>();
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO, "N�mero do Documento (" + OperacaocontabilService.VARIAVEL_HISTORICO_NUMDOCUMENTO + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO, " Descri��o da conta (" + OperacaocontabilService.VARIAVEL_HISTORICO_DESCRICAO + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_PAGAMENTOA, "Pagamento a (" + OperacaocontabilService.VARIAVEL_HISTORICO_PAGAMENTOA + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_CLIENTE, "Cliente (" + OperacaocontabilService.VARIAVEL_HISTORICO_CLIENTE + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_FORNECEDOR, "Fornecedor (" + OperacaocontabilService.VARIAVEL_HISTORICO_FORNECEDOR + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_PARCELA, "Parcela (" + OperacaocontabilService.VARIAVEL_HISTORICO_PARCELA + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_CONTAORIGEM, "C�digo da conta de origem (" + OperacaocontabilService.VARIAVEL_HISTORICO_CONTAORIGEM + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_CONTADESTINO, "C�digo da conta de destino (" + OperacaocontabilService.VARIAVEL_HISTORICO_CONTADESTINO + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_DATA, "Data (" + OperacaocontabilService.VARIAVEL_HISTORICO_DATA + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_CHEQUE, "Cheque (" + OperacaocontabilService.VARIAVEL_HISTORICO_CHEQUE + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_VENDA, "N�mero da venda (" + OperacaocontabilService.VARIAVEL_HISTORICO_VENDA + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_IDENTIFICADORVENDA, "Identificador externo da venda (" + OperacaocontabilService.VARIAVEL_HISTORICO_IDENTIFICADORVENDA + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO, "C�digo da Integra��o (" + OperacaocontabilService.VARIAVEL_HISTORICO_CODIGO_INTEGRACAO + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_NUMERO_NOTA, "N�mero Nota (" + OperacaocontabilService.VARIAVEL_HISTORICO_NUMERO_NOTA + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_VALOR_UTILIZADO_ADIANTAMENTO, "Valor Utilizado (" + OperacaocontabilService.VARIAVEL_HISTORICO_VALOR_UTILIZADO_ADIANTAMENTO + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_EVENTO, "Evento (" + OperacaocontabilService.VARIAVEL_HISTORICO_EVENTO + ")"));
		listaVariavel.add(new GenericBean(OperacaocontabilService.VARIAVEL_HISTORICO_COLABORADOR, "Colaborador (" + OperacaocontabilService.VARIAVEL_HISTORICO_COLABORADOR + ")"));
		
		Collections.sort(listaVariavel, new Comparator<GenericBean>() {
			@Override
			public int compare(GenericBean o1, GenericBean o2) {
				return o1.getValue().toString().compareToIgnoreCase(o2.getValue().toString());
			}
		});
		
		request.setAttribute("index", request.getParameter("index"));
		request.setAttribute("detalhe", request.getParameter("detalhe"));
		return new ModelAndView("direct:crud/popup/popUpVariavelHistoricoBase", "variavelshistoricobase", listaVariavel);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Operacaocontabil bean) throws Exception {
		validarSalvar(bean);
		super.salvar(request, bean);
	}
	
	private void validarSalvar(Operacaocontabil bean) throws Exception {
//		if (!OperacaocontabilTipoLancamentoEnum.CONTA_GERENCIAL.equals(bean.getTipoLancamento()) && operacaocontabilService.isPossuiTipoLancamento(bean)){
//			throw new Exception("� permitido apenas um registro por 'Tipo de lan�amento', exce��o ao tipo 'Conta Gerencial'.");
//		}
	}
	
	@Override
	public ModelAndView doExportar(WebRequestContext request, OperacaocontabilFiltro filtro) throws CrudException, IOException {
		List<Operacaocontabil> lista = operacaocontabilService.findForCSV(filtro);
		
		StringBuilder rel = new StringBuilder();
		rel.append("Nome;");
		rel.append("Tipo;");
		rel.append("Tipo de lan�amento;");
		rel.append("Conta Gerencial;");
		rel.append("Contas cont�beis d�bito;");
		rel.append("Contas cont�beis credito;\n");
		
		if(SinedUtil.isListNotEmpty(lista)){
			for(Operacaocontabil operacaocontabil : lista){
				rel.append(operacaocontabil.getNome()).append(";");
				rel.append(operacaocontabil.getTipo() != null ? operacaocontabil.getTipo().getNome() : "").append(";");
				rel.append(operacaocontabil.getTipoLancamento() != null ? operacaocontabil.getTipoLancamento().getNome() : "").append(";");
				rel.append(operacaocontabil.getContagerencial() != null ? operacaocontabil.getContagerencial().getNome() : "").append(";");
				rel.append(operacaocontabil.getContascontabeisDebito()).append(";");
				rel.append(operacaocontabil.getContascontabeisCredito()).append(";");
				rel.append("\n");
			}
		}
		
		Resource resource = new Resource();
		resource.setContentType("text/csv");
		resource.setFileName("operacaocontabil.csv");
		resource.setContents(rel.toString().replaceAll("null;", ";").getBytes());
		
		return new ResourceModelAndView(resource);
	}
	
	@Override
	protected Operacaocontabil criar(WebRequestContext request,
			Operacaocontabil form) throws Exception {
		Operacaocontabil novo = super.criar(request, form);
		novo.setMovimentacaoFinanceiraVinculo(MovimentacaoFinanceiraVinculoEnum.TODAS);
		return novo;
	}
	
	public ModelAndView ajaxAtualizaComboNatureza (WebRequestContext request){
		String cdTipoLacamento = request.getParameter("cdTipoLancamento");
		List<Naturezaoperacao> natureza = new ArrayList<Naturezaoperacao>();
		if((OperacaoContabilTipoLancamento.EMISSAO_NF.getCdOperacaoContabilTipoLancamento())==(Integer.parseInt(cdTipoLacamento))){
			natureza = naturezaOperacaoService.findByNotaTipoNome("Nota Fiscal");
		}
		if((OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL.getCdOperacaoContabilTipoLancamento())==(Integer.parseInt(cdTipoLacamento)) || (OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO.getCdOperacaoContabilTipoLancamento())==(Integer.parseInt(cdTipoLacamento))){
			natureza = naturezaOperacaoService.findByNotaTipoNome("Entrada Fiscal");
		}
		
		return new JsonModelAndView().addObject("naturezaOperacao", natureza);
		
		
		
	}
	
}