package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.io.ByteArrayOutputStream;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Operacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabilTipoEnum;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FechamentoContabilService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabildebitocreditoService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilorigemService;
import br.com.linkcom.sined.geral.service.OperacaocontabilService;
import br.com.linkcom.sined.geral.service.OperacaocontabildebitocreditoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.MovimentacaocontabilFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/contabil/crud/Movimentacaocontabil", authorizationModule=CrudAuthorizationModule.class)
public class MovimentacaocontabilCrud extends CrudControllerSined<MovimentacaocontabilFiltro, Movimentacaocontabil, Movimentacaocontabil>{

	private MovimentacaocontabilService movimentacaocontabilService;
	private OperacaocontabilService operacaocontabilService;
	private MovimentacaocontabildebitocreditoService movimentacaocontabildebitocreditoService;
	private OperacaocontabildebitocreditoService operacaocontabildebitocreditoService;
	private MovimentacaocontabilorigemService movimentacaocontabilorigemService;
	private ClienteService clienteService;
	private FornecedorService fornecedorService;
	private EmpresaService empresaService;
	private FechamentoContabilService fechamentoContabilService;

	public void setOperacaocontabilService(OperacaocontabilService operacaocontabilService) {
		this.operacaocontabilService = operacaocontabilService;
	}
	public void setMovimentacaocontabilService(MovimentacaocontabilService movimentacaocontabilService) {
		this.movimentacaocontabilService = movimentacaocontabilService;
	}
	public void setMovimentacaocontabildebitocreditoService(MovimentacaocontabildebitocreditoService movimentacaocontabildebitocreditoService) {
		this.movimentacaocontabildebitocreditoService = movimentacaocontabildebitocreditoService;
	}
	public void setOperacaocontabildebitocreditoService(OperacaocontabildebitocreditoService operacaocontabildebitocreditoService) {
		this.operacaocontabildebitocreditoService = operacaocontabildebitocreditoService;
	}
	public void setMovimentacaocontabilorigemService(MovimentacaocontabilorigemService movimentacaocontabilorigemService) {
		this.movimentacaocontabilorigemService = movimentacaocontabilorigemService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setFechamentoContabilService(FechamentoContabilService fechamentoContabilService) {
		this.fechamentoContabilService = fechamentoContabilService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Movimentacaocontabil form) throws Exception {
		if (form.getCdmovimentacaocontabil()==null){
			if (form.getTipo()==null){
				form.setTipo(MovimentacaocontabilTipoEnum.UM_DEBITO_UM_CREDITO);
			}
		}else {
			form.setListaDebito(movimentacaocontabildebitocreditoService.findByMovimentacaocontabil(form, "movimentacaocontabildebito"));
			form.setListaCredito(movimentacaocontabildebitocreditoService.findByMovimentacaocontabil(form, "movimentacaocontabilcredito"));
			request.setAttribute("linksOrigem", movimentacaocontabilorigemService.getLinksOrigem(movimentacaocontabilorigemService.findByMovimentacaocontabil(form), false));
		}
		
		request.setAttribute("listaTipoLancamento", MovimentacaocontabilTipoLancamentoEnum.getListaOrdenada());
		
		super.entrada(request, form);
	}	
	
	@Override
	protected void salvar(WebRequestContext request, Movimentacaocontabil bean) throws Exception {
		bean.setSalvarMovimentacaocontabilorigem(false);
		super.salvar(request, bean);
	}
	
	@Override
	protected void listagem(WebRequestContext request, MovimentacaocontabilFiltro filtro) throws Exception {
		request.setAttribute("listaTipoLancamento", MovimentacaocontabilTipoLancamentoEnum.getListaOrdenada());
		
		String cdLoteContabil = request.getParameter("cdLoteContabil");
		if(StringUtils.isNotBlank(cdLoteContabil)) {
			filtro.setCdLoteContabil(Integer.parseInt(cdLoteContabil));
		}
		
		super.listagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<Movimentacaocontabil> getLista(WebRequestContext request, MovimentacaocontabilFiltro filtro) {
		ListagemResult<Movimentacaocontabil> list = super.getLista(request, filtro);
		List<Movimentacaocontabil> lista = list.list();
		if(SinedUtil.isListNotEmpty(lista)){
			List<Movimentacaocontabil> listaMovimentacaocontabil = movimentacaocontabilService.findForListagem(CollectionsUtil.listAndConcatenate(lista, "cdmovimentacaocontabil", ","));
			if(SinedUtil.isListNotEmpty(listaMovimentacaocontabil)){
				Iterator<Movimentacaocontabil> iter = listaMovimentacaocontabil.iterator();
				Movimentacaocontabil movimentacaocontabil;
				while(iter.hasNext()){
					movimentacaocontabil = iter.next();
					if(movimentacaocontabil.getCdmovimentacaocontabil() != null){
						for(Movimentacaocontabil beanListResult : lista){
							if(beanListResult.getCdmovimentacaocontabil() != null && beanListResult.getCdmovimentacaocontabil().equals(movimentacaocontabil.getCdmovimentacaocontabil())){
								beanListResult.setListaCredito(movimentacaocontabil.getListaCredito());
								beanListResult.setListaDebito(movimentacaocontabil.getListaDebito());
							}
						}
					}
					iter.remove();
				}
			}
		}
		
		return list;
	}
	
	@Override
	protected void validateBean(Movimentacaocontabil bean, BindException errors) {
		boolean msg001 = false;
		Money valorDebito = new Money();
		Money valorCredito = new Money();
		
		if (bean.getListaDebito()!=null){
			for (Movimentacaocontabildebitocredito debito: bean.getListaDebito()){
				if (debito.getValor()!=null){
					valorDebito = valorDebito.add(debito.getValor());
				}
			}
		}
		
		if (bean.getListaCredito()!=null){
			for (Movimentacaocontabildebitocredito credito: bean.getListaCredito()){
				if (credito.getValor()!=null){
					valorCredito = valorCredito.add(credito.getValor());
				}
			}
		}
		
		if (valorDebito.toLong()==valorCredito.toLong()){
			bean.setValor(valorDebito);
		}else {
			msg001 = true;			
		}
		
		if (msg001){
			errors.reject("001", "O total de d�bitos deve ser igual ao total de cr�ditos.");
		}
		
		Date dtUltimoFechamentoContabil = fechamentoContabilService.carregarUltimaDataFechamento(bean.getEmpresa());
		if(dtUltimoFechamentoContabil != null 
				&& (bean.getDtlancamento().equals(dtUltimoFechamentoContabil) || bean.getDtlancamento().before(dtUltimoFechamentoContabil))) {
			errors.reject("001", "N�o � possivel criar ou editar um registro de lan�amento cont�bil com data igual ou anterior ao �ltimo fechamento cont�bil.");
		}
		
		super.validateBean(bean, errors);
	}
	
	public ModelAndView gerarCsv(WebRequestContext request, MovimentacaocontabilFiltro filtro){
		List<Movimentacaocontabil> listaMovimentacaocontabil = movimentacaocontabilService.findForCsv(filtro);
		
		StringBuilder sb = new StringBuilder();
		sb.append("Data;");
		sb.append("Opera��o cont�bil;");
		sb.append("Tipo de lan�amento;");
		sb.append("Valor;");
		sb.append("Hist�rico;");
		sb.append("\n");
				
		for (Movimentacaocontabil movimentacaocontabil: listaMovimentacaocontabil) {
			//Data
			sb.append(SinedDateUtils.toString(movimentacaocontabil.getDtlancamento()));
			sb.append(";");
			
			//Opera��o cont�bil
			if (movimentacaocontabil.getOperacaocontabil()!=null){
				sb.append(movimentacaocontabil.getOperacaocontabil().getNome());
			}
			sb.append(";");
			
			//Tipo de lan�amento;"
			if (movimentacaocontabil.getTipoLancamento()!=null){
				sb.append(movimentacaocontabil.getTipoLancamento().getNome());
			}
			sb.append(";");	
			
			//Valor
			Money valor = movimentacaocontabil.getValor();
			if (valor==null){
				sb.append(0);
			}else {
				sb.append(valor.toString());				
			}
			sb.append(";");
			
			//Hist�rico
			if (movimentacaocontabil.getHistorico()!=null){
				sb.append(SinedUtil.escapeCsv(movimentacaocontabil.getHistorico()));
			}
			sb.append(";");
			
			sb.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "lancamentos_contabeis_" + SinedUtil.datePatternForReport() + ".csv", sb.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		return resourceModelAndView;
	}
	
	public ModelAndView buscarContas(WebRequestContext request, Movimentacaocontabil form) throws Exception {
		Operacaocontabil operacaocontabil = operacaocontabilService.loadForEntrada(form.getOperacaocontabil());
		List<Operacaocontabildebitocredito> listaOperacaocontabildebitocreditoDebito = operacaocontabildebitocreditoService.findByOperacaocontabil(operacaocontabil, "operacaocontabildebito");
		List<Operacaocontabildebitocredito> listaOperacaocontabildebitocreditoCredito = operacaocontabildebitocreditoService.findByOperacaocontabil(operacaocontabil, "operacaocontabilcredito");
		
		form.setTipo(null);
		form.setHistorico(null);
		form.setListaDebito(new ArrayList<Movimentacaocontabildebitocredito>());
		form.setListaCredito(new ArrayList<Movimentacaocontabildebitocredito>());
		
		if (OperacaocontabilTipoEnum.UM_DEBITO_UM_CREDITO.equals(operacaocontabil.getTipo())){
			form.setTipo(MovimentacaocontabilTipoEnum.UM_DEBITO_UM_CREDITO);
		}else if (OperacaocontabilTipoEnum.N_DEBITOS_UM_CREDITO.equals(operacaocontabil.getTipo())){
			form.setTipo(MovimentacaocontabilTipoEnum.N_DEBITOS_UM_CREDITO);
		}else if (OperacaocontabilTipoEnum.UM_DEBITO_N_CREDITOS.equals(operacaocontabil.getTipo())){
			form.setTipo(MovimentacaocontabilTipoEnum.UM_DEBITO_N_CREDITOS);
		}else if (OperacaocontabilTipoEnum.N_DEBITOS_N_CREDITOS.equals(operacaocontabil.getTipo())){
			form.setTipo(MovimentacaocontabilTipoEnum.N_DEBITOS_N_CREDITOS);
		}

		if (operacaocontabil.getHistorico()!=null && !operacaocontabil.getHistorico().trim().equals("")){
			form.setHistorico(operacaocontabil.getHistorico());
		}
		
		/*if (OperacaocontabilTipoLancamentoEnum.APROPRIACAO.equals(operacaocontabil.getTipoLancamento())){
			form.setTipoLancamento(MovimentacaocontabilTipoLancamentoEnum.APROPRIACAO);
		}else if (OperacaocontabilTipoLancamentoEnum.PAGAMENTO.equals(operacaocontabil.getTipoLancamento())){
			form.setTipoLancamento(MovimentacaocontabilTipoLancamentoEnum.PAGAMENTO);
		}*/
		
		for (Operacaocontabildebitocredito operacaocontabildebitocredito: listaOperacaocontabildebitocreditoDebito){
			Movimentacaocontabildebitocredito movimentacaocontabildebitocredito = new Movimentacaocontabildebitocredito();
			movimentacaocontabildebitocredito.setContaContabil(operacaocontabildebitocredito.getContaContabil());
			movimentacaocontabildebitocredito.setOperacaocontabildebitocredito(operacaocontabildebitocredito);
			form.getListaDebito().add(movimentacaocontabildebitocredito);
		}
		
		for (Operacaocontabildebitocredito operacaocontabildebitocredito: listaOperacaocontabildebitocreditoCredito){
			Movimentacaocontabildebitocredito movimentacaocontabildebitocredito = new Movimentacaocontabildebitocredito();
			movimentacaocontabildebitocredito.setContaContabil(operacaocontabildebitocredito.getContaContabil());
			movimentacaocontabildebitocredito.setOperacaocontabildebitocredito(operacaocontabildebitocredito);
			form.getListaCredito().add(movimentacaocontabildebitocredito);
		}	
		
		return doEntrada(request, form);
	}
	
	public ModelAndView gerarArquivoSAGE(WebRequestContext request, MovimentacaocontabilFiltro filtro) throws Exception {
		Empresa empresa = filtro.getEmpresa();
		Date dtlancamentoinicio = filtro.getDtlancamentoinicio();
		Date dtlancamentofim = filtro.getDtlancamentofim();
		
		if (empresa==null || dtlancamentoinicio==null || dtlancamentofim==null){
			request.addError("O campo Empresa e o per�odo de Data � obrigat�rio.");
		}else {
			DateFormat formatadorMesAno = new SimpleDateFormat("MMyyyy");
			DateFormat formatadorMes = new SimpleDateFormat("MM");
			DateFormat formatadorAno = new SimpleDateFormat("yyyy");
			
			if (!formatadorMesAno.format(dtlancamentoinicio).equals(formatadorMesAno.format(dtlancamentofim))){
				request.addError("A integra��o SAGE pode ser gerada apenas para per�odo dentro do mesmo m�s.");				
			}else {
				empresa = empresaService.loadForEntrada(empresa);
				final String movimentos = gerarLinhasSAGEMovimentos(movimentacaocontabilService.findForSAGE(filtro));
				final String contas = gerarLinhasSAGEContas(clienteService.findForSAGE(dtlancamentoinicio), fornecedorService.findForSAGE(dtlancamentoinicio));
				final String nomeArquivoMovimentos = "FI" + SinedUtil.formataTamanho(empresa.getCdpessoa().toString(), 4, '0', false) + SinedUtil.formataTamanho(empresa.getIntegracaocontabil(), 4, '0', false) + "." + formatadorMes.format(dtlancamentoinicio);
				final String nomeArquivoContas = "PLANO" + formatadorAno.format(dtlancamentoinicio) + ".txt";
				
				if (!movimentos.trim().equals("") && !contas.trim().equals("")){
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
					
					ZipEntry zipEntryMovimentos = new ZipEntry(nomeArquivoMovimentos);
					zipOutputStream.putNextEntry(zipEntryMovimentos);
					zipOutputStream.write(movimentos.getBytes());
					
					ZipEntry zipEntryContas = new ZipEntry(nomeArquivoContas);
					zipOutputStream.putNextEntry(zipEntryContas);
					zipOutputStream.write(contas.getBytes());
					
					zipOutputStream.closeEntry();
					zipOutputStream.close();
					
					Resource resource = new Resource("application/zip", "SAGE_" + formatadorMesAno.format(dtlancamentoinicio) + ".zip", byteArrayOutputStream.toByteArray());
					return new ResourceModelAndView(resource);
				}else if (!movimentos.equals("")){
					Resource resource = new Resource("text/plain", nomeArquivoMovimentos, movimentos.getBytes());
					return new ResourceModelAndView(resource);
				}else if (!contas.equals("")){
					Resource resource = new Resource("text/plain", nomeArquivoContas, contas.getBytes());
					return new ResourceModelAndView(resource);
				}else {
					request.addError("Nenhum registro encontrado.");
				}
			}
		}
		
		return doListagem(request, filtro);
	}
	
	private String gerarLinhasSAGEMovimentos(List<Movimentacaocontabil> listaMovimentacaocontabil){
		StringBuilder linhas = new StringBuilder();
		
		for (Movimentacaocontabil movimentacaocontabil: listaMovimentacaocontabil){
			List<Movimentacaocontabildebitocredito> listaDebito = movimentacaocontabil.getListaDebito();
			List<Movimentacaocontabildebitocredito> listaCredito = movimentacaocontabil.getListaCredito();
			
				String tipo = MovimentacaocontabilTipoEnum.UM_DEBITO_UM_CREDITO.equals(movimentacaocontabil.getTipo()) 
								&& listaDebito != null 
								&& listaDebito.size() == 1
								&& listaCredito != null 
								&& listaCredito.size() == 1 ? "N" : "M";
				
				if (listaDebito != null && listaDebito.size() == 1 && listaCredito != null && listaCredito.size() == 1) {
					linhas.append(gerarLinhaSAGEMovimentos(movimentacaocontabil.getDtlancamento(), movimentacaocontabil.getHistorico(), "N", movimentacaocontabil.getCdmovimentacaocontabil(), listaDebito.get(0), listaCredito.get(0)));
				} else {
					if (listaDebito!=null){
						for (Movimentacaocontabildebitocredito debito: listaDebito){
							linhas.append(gerarLinhaSAGEMovimentos(movimentacaocontabil.getDtlancamento(), movimentacaocontabil.getHistorico(), tipo, movimentacaocontabil.getCdmovimentacaocontabil(), debito, null));
						}
					}
					if (listaCredito!=null){
						for (Movimentacaocontabildebitocredito credito: listaCredito){
							linhas.append(gerarLinhaSAGEMovimentos(movimentacaocontabil.getDtlancamento(), movimentacaocontabil.getHistorico(), tipo, movimentacaocontabil.getCdmovimentacaocontabil(), null, credito));
						}
					}
				}
		}
		
		return linhas.toString();
	}
	
	private String gerarLinhaSAGEMovimentos(Date dtlancamento, String historico, String tipo, Integer cdmovimentacaocontabil, Movimentacaocontabildebitocredito debito, Movimentacaocontabildebitocredito credito){
		String codigoIntegracao = "";
		
		if (debito != null) {
			codigoIntegracao = debito.getOperacaocontabildebitocredito() != null && debito.getOperacaocontabildebitocredito().getCodigointegracao() != null ? debito.getOperacaocontabildebitocredito().getCodigointegracao().toString() : 
				(debito.getMovimentacaocontabildebito() != null && debito.getMovimentacaocontabildebito().getOperacaocontabil() != null && debito.getMovimentacaocontabildebito().getOperacaocontabil().getCodigointegracao() != null ? 
						debito.getMovimentacaocontabildebito().getOperacaocontabil().getCodigointegracao().toString() : "");
		} else if (credito != null) {
			codigoIntegracao = credito.getOperacaocontabildebitocredito() != null && credito.getOperacaocontabildebitocredito().getCodigointegracao() != null ? credito.getOperacaocontabildebitocredito().getCodigointegracao().toString() : 
				(credito.getMovimentacaocontabilcredito() != null && credito.getMovimentacaocontabilcredito().getOperacaocontabil() != null && credito.getMovimentacaocontabilcredito().getOperacaocontabil().getCodigointegracao() != null ? 
						credito.getMovimentacaocontabilcredito().getOperacaocontabil().getCodigointegracao().toString() : "");
		}
		
		StringBuilder linha = new StringBuilder();
		linha.append(SinedUtil.formataTamanho("", 5, ' ', true));
		linha.append(SinedUtil.formataTamanho(debito!=null ? debito.getContaContabil().getVcontacontabil().getIdentificador() : "", 18, ' ', true));
		linha.append(SinedUtil.formataTamanho(credito!=null ? credito.getContaContabil().getVcontacontabil().getIdentificador() : "", 18, ' ', true));
		linha.append(SinedUtil.formataTamanho(StringUtils.isNotEmpty(codigoIntegracao) ? codigoIntegracao : "", 4, ' ', true));
		linha.append(SinedUtil.formataTamanho("", 1, ' ', true));
		linha.append(SinedUtil.formataTamanho(debito!=null ? String.valueOf(debito.getValor().toLong()) : String.valueOf(credito.getValor().toLong()), 12, '0', false));
		linha.append(SinedUtil.formataTamanho(NeoFormater.getInstance().format(dtlancamento), 10, ' ', true));
		linha.append(SinedUtil.formataTamanho("", 6, ' ', true));
		linha.append(SinedUtil.formataTamanho(Util.strings.tiraAcento(historico), 143, ' ', true));
		linha.append(SinedUtil.formataTamanho(Util.strings.tiraAcento(SinedUtil.getUsuarioLogado().getNome()), 20, ' ', true));
		linha.append(SinedUtil.formataTamanho("8", 20, ' ', true));
		linha.append(SinedUtil.formataTamanho(debito!=null && debito.getCentrocusto()!=null && debito.getCentrocusto().getCodigoalternativo()!=null ? debito.getCentrocusto().getCodigoalternativo() : "", 20, ' ', true));
		linha.append(debito!=null && debito.getCentrocusto()!=null && debito.getCentrocusto().getCodigoalternativo()!=null ? SinedUtil.formataTamanho(String.valueOf(debito.getValor().toLong()), 15, '0', false) : SinedUtil.formataTamanho("", 15, ' ', true));
		linha.append(SinedUtil.formataTamanho(credito!=null && credito.getCentrocusto()!=null && credito.getCentrocusto().getCodigoalternativo()!=null ? credito.getCentrocusto().getCodigoalternativo() : "", 20, ' ', true));
		linha.append(credito!=null && credito.getCentrocusto()!=null && credito.getCentrocusto().getCodigoalternativo()!=null ? SinedUtil.formataTamanho(String.valueOf(credito.getValor().toLong()), 15, '0', false) : SinedUtil.formataTamanho("", 15, ' ', true));
		linha.append(SinedUtil.formataTamanho(tipo, 1, ' ', true));
		linha.append(SinedUtil.formataTamanho("", 4, ' ', true));
		linha.append(SinedUtil.formataTamanho(cdmovimentacaocontabil.toString(), 10, ' ', true));		
		linha.append("\r\n");
		
		return linha.toString();
	}
	
	private String gerarLinhasSAGEContas(List<Cliente> listaCliente, List<Fornecedor> listaForncedor){
		StringBuilder linhas = new StringBuilder();
		
		for (Cliente cliente: listaCliente){
			linhas.append(gerarLinhaSAGEContas(cliente.getContaContabil()));
		}				
		
		for (Fornecedor fornecedor: listaForncedor){
			linhas.append(gerarLinhaSAGEContas(fornecedor.getContaContabil()));
		}
		
		return linhas.toString();
	}
	
	private String gerarLinhaSAGEContas(ContaContabil ContaContabil){
		StringBuilder linha = new StringBuilder();
		linha.append(SinedUtil.formataTamanho("", 5, ' ', true));
		linha.append(SinedUtil.formataTamanho(ContaContabil.getVcontacontabil().getIdentificador(), 36, ' ', true));
		linha.append(SinedUtil.formataTamanho(ContaContabil.getCodigoalternativo()!=null ? ContaContabil.getCodigoalternativo().toString() : "", 10, ' ', true));
		linha.append(SinedUtil.formataTamanho(ContaContabil.getNome(), 40, ' ', true));
		linha.append(SinedUtil.formataTamanho(ContaContabil.getVcontacontabil().getNivel().toString(), 2, '0', false));
		linha.append(SinedUtil.formataTamanho("", 2, ' ', true));
		linha.append(SinedUtil.formataTamanho("", 40, ' ', true));
		linha.append(SinedUtil.formataTamanho("", 18, ' ', true));
		linha.append("\r\n");
		
		return linha.toString();
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, MovimentacaocontabilFiltro filtro) {
		Money valorTotal = movimentacaocontabilService.findForTotaisValor(filtro);
		filtro.setTotalValor(valorTotal);
		
		return super.getListagemModelAndView(request, filtro);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Movimentacaocontabil bean) throws Exception {
		String itens = request.getParameter("itenstodelete");
		if(StringUtils.isNotBlank(itens)) {
			Map<Integer, Date> mapaFechamentosContabeis = fechamentoContabilService.montarMapaEmpresaDataFechamento(new SinedUtil().getListaEmpresa());
			List<Movimentacaocontabil> listaMovimentacoesContabeis = movimentacaocontabilService.carregarMovimentacoesContabeisParaValidarExclusao(itens);
			
			for(Movimentacaocontabil movimentacao : listaMovimentacoesContabeis) {
				Date ultimoFechamentoContabil = mapaFechamentosContabeis.get(movimentacao.getEmpresa().getCdpessoa());
				if(ultimoFechamentoContabil != null 
						&& (movimentacao.getDtlancamento().equals(ultimoFechamentoContabil) || movimentacao.getDtlancamento().before(ultimoFechamentoContabil))) {
					throw new SinedException("N�o � possivel excluir um registro de lan�amento cont�bil com data igual ou anterior ao �ltimo fechamento cont�bil.");
				}
			}
			
		} else {
			bean = movimentacaocontabilService.load(bean, "movimentacaocontabil.cdmovimentacaocontabil, movimentacaocontabil.dtlancamento");
			Date ultimoFechamentoContabil = fechamentoContabilService.carregarUltimaDataFechamento(bean.getEmpresa());
			
			if(ultimoFechamentoContabil != null 
					&& (bean.getDtlancamento().equals(ultimoFechamentoContabil) || bean.getDtlancamento().before(ultimoFechamentoContabil))) {
				throw new SinedException("N�o � possivel excluir um registro de lan�amento cont�bil com data igual ou anterior ao �ltimo fechamento cont�bil.");
			}
		}
		
		super.excluir(request, bean);
	}

}