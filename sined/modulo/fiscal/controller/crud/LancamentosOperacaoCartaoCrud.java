package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.LancamentosOperacaoCartao;
import br.com.linkcom.sined.geral.service.LancamentosOperacaoCartaoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LancamentosOperacaoCartaoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/fiscal/crud/LancamentosOperacaoCartao", authorizationModule=CrudAuthorizationModule.class)
public class LancamentosOperacaoCartaoCrud extends CrudControllerSined<LancamentosOperacaoCartaoFiltro, LancamentosOperacaoCartao, LancamentosOperacaoCartao>{
	
	private LancamentosOperacaoCartaoService lancamentosOperacaoCartaoService;
	
	public void setLancamentosOperacaoCartaoService(LancamentosOperacaoCartaoService lancamentosOperacaoCartaoService) {this.lancamentosOperacaoCartaoService = lancamentosOperacaoCartaoService;}
	
	@Override
	protected void validateBean(LancamentosOperacaoCartao bean, BindException errors) {
		if(lancamentosOperacaoCartaoService.existeOperacaoPeriodoCredenciadora(bean)){
			errors.reject("001", "J� existe registro com essas informa��es.");
		}
	}
	
}
