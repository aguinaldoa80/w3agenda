package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.view.Vwsaidafiscal;
import br.com.linkcom.sined.geral.bean.view.Vwsaidafiscalimposto;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;
import br.com.linkcom.sined.geral.service.VwsaidafiscalService;
import br.com.linkcom.sined.geral.service.VwsaidafiscalimpostoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.VwsaidafiscalFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/Saidafiscal", authorizationModule=CrudAuthorizationModule.class)
public class VwsaidafiscalCrud extends CrudControllerSined<VwsaidafiscalFiltro, Vwsaidafiscal, Vwsaidafiscal>{
	
	private VwsaidafiscalService vwsaidafiscalService;
	private VwsaidafiscalimpostoService vwsaidafiscalimpostoService;
	private EmpresaService empresaService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService;;
	
	public void setVwsaidafiscalService(VwsaidafiscalService vwsaidafiscalService) {
		this.vwsaidafiscalService = vwsaidafiscalService;
	}
	public void setVwsaidafiscalimpostoService(VwsaidafiscalimpostoService vwsaidafiscalimpostoService) {
		this.vwsaidafiscalimpostoService = vwsaidafiscalimpostoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {
		this.notafiscalprodutoitemService = notafiscalprodutoitemService;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Vwsaidafiscal form) throws CrudException {
		if(request.getParameter("ACAO") == null || !"consultar".equals(request.getParameter("ACAO")))
			throw new SinedException("A��o inv�lida!");
		
		return super.doEditar(request, form);
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Vwsaidafiscal form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Vwsaidafiscal form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Vwsaidafiscal form) throws Exception {
		if(form.getCdnota() != null){
			form.setListaImposto(vwsaidafiscalimpostoService.getListaImpsoto(form.getCdnota()));
		}
		super.entrada(request, form);
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return false;
	}
	
	@Override
	protected void listagem(WebRequestContext request, VwsaidafiscalFiltro filtro) throws Exception {
		request.setAttribute("listanotastatus", NotaStatus.getTodosSaidaFiscal());
		super.listagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<Vwsaidafiscal> getLista(WebRequestContext request,	VwsaidafiscalFiltro filtro) {
		ListagemResult<Vwsaidafiscal> lista = super.getLista(request, filtro);
		List<Vwsaidafiscal> list = lista.list();
		
		if(list != null && !list.isEmpty()){
			for(Vwsaidafiscal vwsaidafiscal : list){
				if(vwsaidafiscal.getCdnota() != null){
					vwsaidafiscal.setListaImposto(vwsaidafiscalimpostoService.getListaImpsoto(vwsaidafiscal.getCdnota()));
				}
			}
		}
		
		return lista;
	}
	
	/**
	 * 
	 * @param request
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView gerarPdf(WebRequestContext request, VwsaidafiscalFiltro filtro) throws Exception {
		
		Report report = vwsaidafiscalService.createReportSaidafiscalListagem(filtro);
		
		if (report==null){
			request.addMessage("N�o existem registros suficientes para gerar o relat�rio", MessageType.ERROR);
			return doListagem(request, filtro);
		}
		
		Resource resource = new Resource();
        resource.setContentType("application/pdf");
        resource.setFileName("saidafiscal.pdf");
        resource.setContents(Neo.getApplicationContext().getReportGenerator().toPdf(report));
		
		return new ResourceModelAndView(resource);
	}
	
	public ModelAndView gerarCsv(WebRequestContext request, VwsaidafiscalFiltro filtro){
		StringBuilder sbLinhas = new StringBuilder();
		
		List<Vwsaidafiscal> listaVwsaidafiscal = vwsaidafiscalService.findForCsv(filtro);
		
		int colunasImpostos = 1;
		
		if (!listaVwsaidafiscal.isEmpty()){
			String whereInCdnota = CollectionsUtil.listAndConcatenate(listaVwsaidafiscal, "cdnota", ",");
			
			Map<Integer, List<Contagerencial>> mapaCdnotaListaContagerencial = new HashMap<Integer, List<Contagerencial>>();
			Map<Integer, Contagerencial> mapaCdempresaContagerencial = new HashMap<Integer, Contagerencial>();
			
			for (Notafiscalprodutoitem notafiscalprodutoitem: notafiscalprodutoitemService.findByWhereInCdnota(whereInCdnota)){
				if (notafiscalprodutoitem.getMaterial().getContagerencialvenda()!=null){
					List<Contagerencial> listaContagerencial = mapaCdnotaListaContagerencial.get(notafiscalprodutoitem.getNotafiscalproduto().getCdNota());
					if (listaContagerencial==null){
						listaContagerencial = new ArrayList<Contagerencial>();
					}
					listaContagerencial.add(notafiscalprodutoitem.getMaterial().getContagerencialvenda());
					mapaCdnotaListaContagerencial.put(notafiscalprodutoitem.getNotafiscalproduto().getCdNota(), listaContagerencial);
				}
			}
			
			for (Empresa empresa: empresaService.findAllContagerencialVenda()){
				mapaCdempresaContagerencial.put(empresa.getCdpessoa(), empresa.getContagerencial());
			}
			
			List<Vwsaidafiscalimposto> listaVwsaidafiscalimposto = vwsaidafiscalimpostoService.findVwsaidafiscalimpostoByCdnota(whereInCdnota);
			
			for (Vwsaidafiscal vwsaidafiscal: listaVwsaidafiscal) {
				List<Vwsaidafiscalimposto> listaVwsaidafiscalimpostoIteracao = new ArrayList<Vwsaidafiscalimposto>();
				for (Vwsaidafiscalimposto vwsaidafiscalimposto: listaVwsaidafiscalimposto){
					if (vwsaidafiscalimposto.getCdnota().equals(vwsaidafiscal.getCdnota())){
						listaVwsaidafiscalimpostoIteracao.add(vwsaidafiscalimposto);
					}
				}
				
				vwsaidafiscal.setListaImposto(listaVwsaidafiscalimpostoIteracao);
				
				sbLinhas.append(Util.strings.emptyIfNull(vwsaidafiscal.getEmpresanome()) + ";");
				sbLinhas.append(Util.strings.emptyIfNull(vwsaidafiscal.getClientenome()) + ";");
				sbLinhas.append(NeoFormater.getInstance().format(vwsaidafiscal.getDtemissao()) + ";");
				sbLinhas.append(Util.strings.emptyIfNull(vwsaidafiscal.getNumero()) + ";");
				sbLinhas.append(Util.strings.emptyIfNull(vwsaidafiscal.getModelo()) + ";");
				sbLinhas.append((vwsaidafiscal.getValorTotalNota()!=null ? vwsaidafiscal.getValorTotalNota() : "") + ";");
				sbLinhas.append(Util.strings.emptyIfNull(vwsaidafiscal.getNotastatusnome()) + ";");
				
				
				colunasImpostos = Math.max(colunasImpostos, listaVwsaidafiscalimpostoIteracao.size());
				
				for (Vwsaidafiscalimposto vwsaidafiscalimposto: listaVwsaidafiscalimpostoIteracao){
					sbLinhas.append(Util.strings.emptyIfNull(vwsaidafiscalimposto.getCfopstr()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBcicms()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getIcms()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValoricms()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBcicmsst()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getIcmsst()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValoricmsst()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBcipi()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getIpi()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValoripi()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBciss()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getIss()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValoriss()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBccsll()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getCsll()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValorcsll()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBcpis()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getPis()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValorpis()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBccofins()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getCofins()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValorcofins()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBcinss()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getInss()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValorinss()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getBcir()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getIr()) + ";");
					sbLinhas.append(SinedUtil.zeroIfNull(vwsaidafiscalimposto.getValorir()) + ";");
				}
				
				if (mapaCdnotaListaContagerencial.containsKey(vwsaidafiscal.getCdnota())){
					sbLinhas.append(CollectionsUtil.listAndConcatenate(mapaCdnotaListaContagerencial.get(vwsaidafiscal.getCdnota()), "nome", ", "));
				}else if (mapaCdempresaContagerencial.containsKey(vwsaidafiscal.getCdempresa())){
					sbLinhas.append(mapaCdempresaContagerencial.get(vwsaidafiscal.getCdempresa()).getNome());					
				}
				sbLinhas.append(";");	
				
				sbLinhas.append("\n");
			}
		}				
		
		StringBuilder sbCSV = new StringBuilder();
		sbCSV.append("Empresa;");
		sbCSV.append("Cliente;");
		sbCSV.append("Data de emiss�o;");
		sbCSV.append("N�mero;");
		sbCSV.append("Modelo;");
		sbCSV.append("Valor do documento;");
		sbCSV.append("Situa��o;");
		
		for (int i=1; i<=colunasImpostos; i++){
			sbCSV.append("CFOP;");
			sbCSV.append("Base de C�lculo de ICMS;");
			sbCSV.append("Al�quota ICMS (%);");
			sbCSV.append("Valor ICMS;");
			sbCSV.append("Base de C�lculo de ICMSST;");
			sbCSV.append("Al�quota ICMSST (%);");
			sbCSV.append("Valor ICMSST;");
			sbCSV.append("Base de C�lculo de IPI;");
			sbCSV.append("Al�quota IPI (%);");
			sbCSV.append("Valor IPI;");
			sbCSV.append("Base de C�lculo de ISS;");
			sbCSV.append("Al�quota ISS (%);");
			sbCSV.append("Valor ISS;");
			sbCSV.append("Base de C�lculo de CSLL;");
			sbCSV.append("Al�quota CSLL (%);");
			sbCSV.append("Valor CSLL;");
			sbCSV.append("Base de C�lculo de PIS;");
			sbCSV.append("Al�quota PIS (%);");
			sbCSV.append("Valor PIS;");
			sbCSV.append("Base de C�lculo de COFINS;");
			sbCSV.append("Al�quota COFINS (%);");
			sbCSV.append("Valor COFINS;");
			sbCSV.append("Base de C�lculo de INSS;");
			sbCSV.append("Al�quota INSS (%);");
			sbCSV.append("Valor INSS;");
			sbCSV.append("Base de C�lculo de IR;");
			sbCSV.append("Al�quota IR (%);");
			sbCSV.append("Valor IR;");
		}
		
		sbCSV.append("Conta gerencial;");
		
		sbCSV.append("\n");
		sbCSV.append(sbLinhas);
		
		Resource resource = new Resource("text/csv", "saida_fiscal_" + SinedUtil.datePatternForReport() + ".csv", sbCSV.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		return resourceModelAndView;
	}
}