package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.LcdprArquivo;
import br.com.linkcom.sined.geral.bean.Spedarquivo;
import br.com.linkcom.sined.geral.service.EmpresaproprietarioService;
import br.com.linkcom.sined.geral.service.LcdprArquivoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/fiscal/crud/LcdprArquivo", authorizationModule=CrudAuthorizationModule.class)
public class LcdprArquivoCrud extends CrudControllerSined<LcdprArquivoFiltro, LcdprArquivo, LcdprArquivo>{
	
	EmpresaproprietarioService empresaproprietarioService;
	LcdprArquivoService lcdprArquivoService;
	
	public void setEmpresaproprietarioService(EmpresaproprietarioService empresaproprietarioService) {
		this.empresaproprietarioService = empresaproprietarioService;
	}
	public void setLcdprArquivoService(LcdprArquivoService lcdprArquivoService) {
		this.lcdprArquivoService = lcdprArquivoService;
	}
	
	public ModelAndView gerar(WebRequestContext request, LcdprArquivoFiltro filtro) {
		try {
			return new ResourceModelAndView(lcdprArquivoService.geraArquivo(filtro, request));
		} catch (SinedException e) {
			request.addError(e.getMessage());
			return sendRedirectToAction("listagem");
		} /*catch (Exception e) {
			request.addError("Houve um erro ao gerar o arquivo.");
			return sendRedirectToAction("listagem");
		} */
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected void setListagemInfo(WebRequestContext request, LcdprArquivoFiltro filtro) throws Exception {
		if (!listagemVaziaPrimeiraVez() || filtro.isNotFirstTime()) {
			if(request.getParameter("resetCurrentPage") != null) {
				filtro.setCurrentPage(0);
			}
		
			ListagemResult<LcdprArquivo> listagemResult = getLista(request, filtro);
			List<LcdprArquivo> list = listagemResult.list();

			for (LcdprArquivo lcdprArquivo : list) {
				if (lcdprArquivo.getArquivo() != null) {
					DownloadFileServlet.addCdfile(request.getSession(), new Long(lcdprArquivo.getArquivo().getCdarquivo()));
				}
			}

			request.setAttribute("lista", list);
			request.setAttribute("currentPage", filtro.getCurrentPage());
			request.setAttribute("numberOfPages", filtro.getNumberOfPages());
			request.setAttribute("filtro", filtro);
		} else {
			request.setAttribute("lista", new ArrayList<Spedarquivo>());
			request.setAttribute("currentPage", 0);
			request.setAttribute("numberOfPages", 0);
			request.setAttribute("filtro", filtro);
		}

		super.setListagemInfo(request, filtro);
	}

}
