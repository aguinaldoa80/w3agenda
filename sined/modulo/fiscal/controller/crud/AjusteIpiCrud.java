package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.AjusteIpi;
import br.com.linkcom.sined.geral.bean.AjusteIpiDocumento;
import br.com.linkcom.sined.geral.bean.enumeration.OrigemDocumentoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.AjusteIpiFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/AjusteIpi", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"empresa", "dtFatoGerador", "tipoAjuste", "codigoAjuste", "valor"})
public class AjusteIpiCrud extends CrudControllerSined<AjusteIpiFiltro, AjusteIpi, AjusteIpi> {
	@Override
	protected void salvar(WebRequestContext request, AjusteIpi bean) throws Exception {
		if (!OrigemDocumentoEnum.DOCUMENTO_FISCAL.equals(bean.getOrigemDocumento())) {
			bean.setAjusteIpiDocumentoList(new ArrayList<AjusteIpiDocumento>());
		}
		
		super.salvar(request, bean);
	}
}
