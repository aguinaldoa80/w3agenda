package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.MaterialSelecaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/fiscal/crud/MaterialSelecao", "/faturamento/crud/MaterialSelecao"})
public class MaterialSelecaoCrud extends CrudControllerSined<MaterialSelecaoFiltro, Material, Material>{
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Material form) throws CrudException {
		throw new SinedException("A��o n�o permitida.");
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Material form) throws CrudException {
		throw new SinedException("A��o n�o permitida.");
	}
	
	@Override
	public ModelAndView doConsultar(WebRequestContext request, Material form) throws CrudException {
		throw new SinedException("A��o n�o permitida.");
	}
	
	protected ModelAndView getListagemModelAndView(WebRequestContext request, MaterialSelecaoFiltro filtro) {
		return new ModelAndView("crud/popup/materialSelecao");
	}
}