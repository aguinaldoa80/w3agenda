package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeHistorico;
import br.com.linkcom.sined.geral.bean.enumeration.FormaemissaoMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Mdfetipoproprietarioveiculo;
import br.com.linkcom.sined.geral.bean.enumeration.ModalidadefreteMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEmitenteMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.UnidadeMedidaPesoBrutoCarga;
import br.com.linkcom.sined.geral.service.ArquivoMdfeService;
import br.com.linkcom.sined.geral.service.MdfeHistoricoService;
import br.com.linkcom.sined.geral.service.MdfeService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.MdfeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/Mdfe", authorizationModule=CrudAuthorizationModule.class)
public class MdfeCrud extends CrudControllerSined<MdfeFiltro, Mdfe, Mdfe>{

	private MdfeHistoricoService mdfeHistoricoService;
	private MdfeService mdfeService;
	private ArquivoMdfeService arquivoMdfeService;
	
	public void setMdfeHistoricoService(MdfeHistoricoService mdfeHistoricoService) {
		this.mdfeHistoricoService = mdfeHistoricoService;
	}
	public void setMdfeService(MdfeService mdfeService) {
		this.mdfeService = mdfeService;
	}
	public void setArquivoMdfeService(ArquivoMdfeService arquivoMdfeService) {
		this.arquivoMdfeService = arquivoMdfeService;
	}
	
	@Override
	protected Mdfe criar(WebRequestContext request, Mdfe form) throws Exception {
		form.setDataHoraEmissao(SinedDateUtils.currentTimestamp());
		form.setDataHoraInicioViagem(SinedDateUtils.currentTimestamp());
		form.setTipoEmitenteMdfe(TipoEmitenteMdfe.NAO_PRESTADOR_SERVICO_TRANSPORTE);
		form.setModalidadeFrete(ModalidadefreteMdfe.RODOVIARIO);
		form.setFormaEmissao(FormaemissaoMdfe.NORMAL);
		form.setUnidadeMedidaPesoBrutoCarga(UnidadeMedidaPesoBrutoCarga.KG);
	//	form.setTipoProprietarioVeiculoRodoviario(Mdfetipoproprietarioveiculo.OUTROS);
		
		return form;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Mdfe form)
			throws Exception {
		if(form.getCdmdfe() != null){
			if(EDITAR.equals(request.getParameter("ACAO"))){
				if(!(MdfeSituacao.REJEITADO.equals(form.getMdfeSituacao()) || MdfeSituacao.EMITIDO.equals(form.getMdfeSituacao()))){
					throw new SinedException("Somente � permitido a altera��o de MDFe's com as situa��es \"MDFe Emitida\" ou \"MDFe Rejeitada\"");
				}
			}
			mdfeService.carregarListas(form);
		}
		super.entrada(request, form);
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Mdfe form)
			throws CrudException {
		throw new SinedException("A��o n�o permitida.");
	}

	
	@Override
	protected void salvar(WebRequestContext request, Mdfe bean)	throws Exception {
		MdfeHistorico historico = new MdfeHistorico();
		if(bean.getCdmdfe() == null){
			bean.setMdfeSituacao(MdfeSituacao.EMITIDO);
			historico.setMdfeSituacao(MdfeSituacao.EMITIDO);
		}else{
			historico.setMdfeSituacao(MdfeSituacao.ALTERADO);
		}
		historico.setDtaltera(SinedDateUtils.currentTimestamp());
		historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		
		super.salvar(request, bean);
		
		if(bean.getUsarSequencial() != null && bean.getUsarSequencial()){
			mdfeService.updateNumero(bean);
		}
		
		historico.setMdfe(bean);
		mdfeHistoricoService.saveOrUpdate(historico);
	}
	
	@Override
	protected void listagem(WebRequestContext request, MdfeFiltro filtro) throws Exception {
		super.listagem(request, filtro);
		request.setAttribute("listaMdfeSituacao", mdfeService.getListaSituacoesNaoTransients());
	}
	
	@Override
	protected ListagemResult<Mdfe> getLista(WebRequestContext request, MdfeFiltro filtro) {
		ListagemResult<Mdfe> lista = super.getLista(request, filtro);
		List<Mdfe> list = lista.list();
		
		if(SinedUtil.isListNotEmpty(list)){
			String whereIn = CollectionsUtil.listAndConcatenate(list, "cdmdfe", ",");
			
			List<ArquivoMdfe> listaArquivoMdfe = arquivoMdfeService.findByMdfe(whereIn);
			for (ArquivoMdfe arquivoMdfe : listaArquivoMdfe) {
				for (Mdfe mdfe : list) {
					if(arquivoMdfe.getMdfe() != null && mdfe.getCdmdfe().equals(arquivoMdfe.getMdfe().getCdmdfe())){
						mdfe.setHaveArquivoMdfe(Boolean.TRUE);
						break;
					}
				}
			}
		}
		
		return lista;
	}

	public ModelAndView abrePopupCancelarInternamente(WebRequestContext request){
		MdfeHistorico bean = new MdfeHistorico();
		String whereIn = request.getParameter("selectedItens");
		if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.EMITIDO, MdfeSituacao.SOLICITADO, MdfeSituacao.REJEITADO, MdfeSituacao.EM_ENCERRAMENTO)){
			request.addError("Apenas os MDFe's com a situa��o igual a \"MDFe Emitido\", \"MDFe Solicitado\", \"MDFe Enviado\", \"MDFe Rejeitado\" e \"Encerrando MDFe\" podem ser cancelados internamente.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		request.setAttribute("msg", "Justificativa de Cancelamento");
		request.setAttribute("acaoSalvar", "cancelarInternamente");
		bean.setIds(whereIn);
		return new ModelAndView("direct:crud/popup/cancelamentoMdfeJustificativa").addObject("bean", bean);
	}
	
	public ModelAndView cancelarInternamente(WebRequestContext request, MdfeHistorico bean){
		mdfeService.cancelarInternamente(bean.getIds(), bean.getObservacao());
		request.addMessage("MDFe(s) cancelada(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/fiscal/crud/Mdfe");
		return null;
	}
	
	public ModelAndView abrePopupEstornar(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		if(mdfeService.haveMDFeDiferenteStatus(whereIn, MdfeSituacao.SOLICITADO, MdfeSituacao.AUTORIZADO)){
			request.addError("Apenas os MDFe's com a situa��o igual a \"MDFe Solicitado\" e \"MDFe Autorizado\" podem ser estornados.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		request.setAttribute("msg", "Justificativa de Estorno");
		request.setAttribute("acaoSalvar", "estornar");
		MdfeHistorico bean = new MdfeHistorico();
		bean.setIds(whereIn);
		return new ModelAndView("direct:crud/popup/cancelamentoMdfeJustificativa").addObject("bean", bean);
	}
	
	public ModelAndView estornar(WebRequestContext request, MdfeHistorico bean){
		if(mdfeService.haveMDFeDiferenteStatus(bean.getIds(), MdfeSituacao.SOLICITADO, MdfeSituacao.REJEITADO)){
			request.addError("� permitido estornar somente MDFe na situa��o 'Solicitado'.");
		}else {
			mdfeService.estornar(bean.getIds(), bean.getObservacao());
			Integer count = bean.getIds().split(",").length;
			request.addMessage(count + " MDFe(s) foi(ram) estornado(s) com sucesso.", MessageType.INFO);
		}
		SinedUtil.redirecionamento(request, "/fiscal/crud/Mdfe");
		return null;
	}
	
	public ModelAndView downloadMdfe(WebRequestContext request, Mdfe mdfe) {
		try{
			Arquivo arquivoxml = arquivoMdfeService.getArquivoxmlDistribuicao(mdfe);
			Resource resource = new Resource(arquivoxml.getContenttype(), arquivoxml.getName(), arquivoxml.getContent());
			return new ResourceModelAndView(resource);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("N�o foi poss�vel obter o arquivo XML na MDF-e.");
			return new ModelAndView("redirect:/fiscal/crud/Mdfe");
		}
	}
	
	@Override
	protected void validateBean(Mdfe bean, BindException errors) {
		if(SinedUtil.isListEmpty(bean.getListaLocalCarregamento()) && !Boolean.TRUE.equals(bean.getCarregamentoNoExterior())){
			errors.reject("001", "Munic�pio de carregamento � obrigat�rio.");			
		}
		if(ModalidadefreteMdfe.RODOVIARIO.equals(bean.getModalidadeFrete()) &&
			SinedUtil.isListEmpty(bean.getListaCondutor())){
			errors.reject("001", "Informa��es sobre os condutores � obrigat�rio.");	
		}
		if(SinedUtil.isListEmpty(bean.getListaLacre()) &&
			!(ModalidadefreteMdfe.RODOVIARIO.equals(bean.getModalidadeFrete()) || ModalidadefreteMdfe.FERROVIARIO.equals(bean.getModalidadeFrete()))){
			errors.reject("001", "\"Lacres\" � obrigat�rio para a modalidade de frete"+bean.getModalidadeFrete().getNome()+".");
		}
//		super.validateBean(bean, errors);
	}
}
