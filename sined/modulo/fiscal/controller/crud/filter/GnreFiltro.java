package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.GnreCodigoReceitaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnreSituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GnreFiltro extends FiltroListagemSined {
	
	private Uf uf;
	private GnreCodigoReceitaEnum codigoreceita;
	private Date dtvencimento1;
	private Date dtvencimento2;
	private Date dtpagamento1;
	private Date dtpagamento2;
	private Empresa empresa;
	private Cliente cliente;
	private String numerodocumento;
	
	private List<GnreSituacao> listaSituacao;
	
	public GnreFiltro() {
		if(listaSituacao == null){
			listaSituacao = new ArrayList<GnreSituacao>();
			listaSituacao.add(GnreSituacao.EM_ABERTA);
			listaSituacao.add(GnreSituacao.GERADA);
		}
	}
	
	@DisplayName("UF favorecida")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("C�digo da receita")
	public GnreCodigoReceitaEnum getCodigoreceita() {
		return codigoreceita;
	}
	public Date getDtvencimento1() {
		return dtvencimento1;
	}
	public Date getDtvencimento2() {
		return dtvencimento2;
	}
	public Date getDtpagamento1() {
		return dtpagamento1;
	}
	public Date getDtpagamento2() {
		return dtpagamento2;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("N�mero do documento")
	public String getNumerodocumento() {
		return numerodocumento;
	}
	@DisplayName("Situa��es")
	public List<GnreSituacao> getListaSituacao() {
		return listaSituacao;
	}
	public void setListaSituacao(List<GnreSituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setCodigoreceita(GnreCodigoReceitaEnum codigoreceita) {
		this.codigoreceita = codigoreceita;
	}
	public void setDtvencimento1(Date dtvencimento1) {
		this.dtvencimento1 = dtvencimento1;
	}
	public void setDtvencimento2(Date dtvencimento2) {
		this.dtvencimento2 = dtvencimento2;
	}
	public void setDtpagamento1(Date dtpagamento1) {
		this.dtpagamento1 = dtpagamento1;
	}
	public void setDtpagamento2(Date dtpagamento2) {
		this.dtpagamento2 = dtpagamento2;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}
	
}
