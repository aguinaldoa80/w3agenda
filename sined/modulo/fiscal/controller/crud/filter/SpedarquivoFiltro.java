package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Inventario;

import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.SPEDFiscalApoio;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE200;
import br.com.linkcom.spedfiscal.spedicmsipi.sped.VersaoFiscal;

public class SpedarquivoFiltro extends FiltroListagemSined {
	
	protected VersaoFiscal versaoFiscal;
	protected Date mesano;
	protected String mesanoAux;
	protected Empresa empresa;
	protected Boolean gerarinventario = Boolean.FALSE;
	protected Boolean gerarregistroc410 = Boolean.FALSE;
	protected Boolean gerarRegistroK280 = Boolean.FALSE;
	protected Boolean gerarRegistro1100 = Boolean.FALSE;
	protected List<Inventario> inventario;
	protected List<Inventario> inventarioEstoque;
	protected List<Inventario> inventarioCorrecao;
	protected Boolean buscarentradas = Boolean.TRUE;
	protected Boolean buscarsaidas = Boolean.TRUE;
	protected Boolean gerarblocok = Boolean.FALSE;
	protected Integer mes;
	protected Integer ano;
	private SPEDFiscalApoio apoio;
	private List<String> listaErro;
	protected List<RegistroE200> listaRegistroE200;
	private String ufSigla;
	private List<DeclaracaoExportacao> listaDeclaracaoExportacao;
	private List<Entregamaterial> listaEntregamaterialKit;
	
	////////////////////////////|
	/// GET		           /////|
	////////////////////////////|
	public Date getMesano() {
		return mesano;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Gerar registro 1100")
	public Boolean getGerarRegistro1100() {
		return gerarRegistro1100;
	}
	public SPEDFiscalApoio getApoio() {
		return apoio;
	}
	
	public List<String> getListaErro() {
		return listaErro;
	}
	
	@DisplayName("Gerar invent�rio")
	public Boolean getGerarinventario() {
		return gerarinventario;
	}
	@DisplayName("Invent�rio")
	public List<Inventario> getInventario() {
		return inventario;
	}
	@DisplayName("Gerar registro C410")
	public Boolean getGerarregistroc410() {
		return gerarregistroc410;
	}
	@DisplayName("Entradas")
	public Boolean getBuscarentradas() {
		return buscarentradas;
	}
	@DisplayName("Sa�das")
	public Boolean getBuscarsaidas() {
		return buscarsaidas;
	}	
	@DisplayName("Gerar Bloco K")
	public Boolean getGerarblocok() {
		return gerarblocok;
	}
	public Date getDtinicio(){
		if(this.mesano != null){
			return SinedDateUtils.firstDateOfMonth(this.mesano);
		}
		return null;
	}
	public Date getDtfim(){
		if(this.mesano != null){
			return SinedDateUtils.lastDateOfMonth(this.mesano);
		}
		return null;
	}
	public List<RegistroE200> getListaRegistroE200() {
		return listaRegistroE200;
	}
	public VersaoFiscal getVersaoFiscal() {
		return versaoFiscal;
	}
	public String getUfSigla() {
		return ufSigla;
	}	
	public Integer getMes() {
		return mes;
	}
	public Integer getAno() {
		return ano;
	}
	@DisplayName("Gerar registro K280")
	public Boolean getGerarRegistroK280() {
		return gerarRegistroK280;
	}
	public List<Inventario> getInventarioCorrecao() {
		return inventarioCorrecao;
	}
	public List<DeclaracaoExportacao> getListaDeclaracaoExportacao() {
		return listaDeclaracaoExportacao;
	}
	public List<Entregamaterial> getListaEntregamaterialKit() {
		return listaEntregamaterialKit;
	}
	////////////////////////////|
	/// SET		           /////|
	////////////////////////////|
	public void setGerarRegistroK280(Boolean gerarRegistroK280) {
		this.gerarRegistroK280 = gerarRegistroK280;
	}
	
	public void setInventarioCorrecao(List<Inventario> inventarioCorrecao) {
		this.inventarioCorrecao = inventarioCorrecao;
	}
	public List<Inventario> getInventarioEstoque() {
		return inventarioEstoque;
	}
	public void setListaRegistroE200(List<RegistroE200> listaRegistroE200) {
		this.listaRegistroE200 = listaRegistroE200;
	}
	public void setBuscarentradas(Boolean buscarentradas) {
		this.buscarentradas = buscarentradas;
	}
	public void setBuscarsaidas(Boolean buscarsaidas) {
		this.buscarsaidas = buscarsaidas;
	}
	public void setGerarregistroc410(Boolean gerarregistroc410) {
		this.gerarregistroc410 = gerarregistroc410;
	}
	
	public void setGerarinventario(Boolean gerarinventario) {
		this.gerarinventario = gerarinventario;
	}	
	public void setInventario(List<Inventario> inventario) {
		this.inventario = inventario;
	}
	public void setListaErro(List<String> listaErro) {
		this.listaErro = listaErro;
	}
	public void setApoio(SPEDFiscalApoio apoio) {
		this.apoio = apoio;
	}
	public void setMesano(Date mesano) {
		this.mesano = mesano;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setGerarblocok(Boolean gerarblocok) {
		this.gerarblocok = gerarblocok;
	}
	public void setInventarioEstoque(List<Inventario> inventarioEstoque) {
		this.inventarioEstoque = inventarioEstoque;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public void setVersaoFiscal(VersaoFiscal versaoFiscal) {
		this.versaoFiscal = versaoFiscal;
	}
	public void setUfSigla(String ufSigla) {
		this.ufSigla = ufSigla;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public void setGerarRegistro1100(Boolean gerarRegistro1100) {
		this.gerarRegistro1100 = gerarRegistro1100;
	}

	public void setMesanoAux(String mesanoAux) {
		try {
			if(mesanoAux != null){
				this.mesano = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
			} else {
				this.mesano = null;
			}
		} catch (ParseException e) {
			this.mesano = null;
		};
	}
	public void setListaDeclaracaoExportacao(
			List<DeclaracaoExportacao> listaDeclaracaoExportacao) {
		this.listaDeclaracaoExportacao = listaDeclaracaoExportacao;
	}
	public void setListaEntregamaterialKit(List<Entregamaterial> listaEntregamaterialKit) {
		this.listaEntregamaterialKit = listaEntregamaterialKit;
	}
	////////////////////////////|
	/// transient e outros /////|
	////////////////////////////|
	@DisplayName("M�s/ano")
	@Transient
	public String getMesanoAux() {
		if(this.mesano != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesano);
		return null;
	}
	public void addE200(RegistroE200 registroE200){
		if(this.listaRegistroE200 == null) this.setListaRegistroE200(new ArrayList<RegistroE200>());
		boolean adicionar = true;
		if(registroE200 != null){
			for(RegistroE200 bean : this.listaRegistroE200){
				if(bean.getUf() != null && bean.getUf().equals(registroE200.getUf())){
					adicionar = false;
					break;
				}
			}
		}	
		if(adicionar)
			listaRegistroE200.add(registroE200);
	}
	
	public void addError(String erro){
		if(listaErro == null){
			listaErro = new ArrayList<String>();
		}
		listaErro.add(erro);
	}
	
}
