package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodocumento;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EntradafiscalFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Fornecedor fornecedor;
	protected String numero;
	protected Date dtentrada1;
	protected Date dtentrada2;
	protected Date dtemissao1 = SinedDateUtils.firstDateOfMonth();
	protected Date dtemissao2;
	protected Situacaodocumento situacaodocumento;
	protected Modelodocumentofiscal modelodocumentofiscal;
	protected Money valordocumento1;
	protected Money valordocumento2;
	protected List<Entregadocumentosituacao> listaSituacao;
	protected Documentotipo documentotipo;
	protected Projeto projeto;
	protected Loteestoque loteestoque;
	protected boolean popupCTRC;
	protected Cfop cfop;
	protected Boolean exibirTotais = false;
	protected Money totalgeral;
	protected Money totalpagamento;
	protected Material material;
	
	public EntradafiscalFiltro(){
		if(this.getListaSituacao() == null){
			this.setListaSituacao(new ArrayList<Entregadocumentosituacao>());
			this.getListaSituacao().add(Entregadocumentosituacao.REGISTRADA);
			this.getListaSituacao().add(Entregadocumentosituacao.FATURADA);
		}
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	@DisplayName("Data de Entrada In�cio")
	public Date getDtentrada1() {
		return dtentrada1;
	}
	@DisplayName("Data de Entrada Fim")
	public Date getDtentrada2() {
		return dtentrada2;
	}
	@DisplayName("Data de Emiss�o In�cio")
	public Date getDtemissao1() {
		return dtemissao1;
	}
	@DisplayName("Data de Emiss�o Fim")
	public Date getDtemissao2() {
		return dtemissao2;
	}
	@DisplayName("Situa��o Fiscal do Documento")
	public Situacaodocumento getSituacaodocumento() {
		return situacaodocumento;
	}
	@DisplayName("Modelo da NF-e")
	public Modelodocumentofiscal getModelodocumentofiscal() {
		return modelodocumentofiscal;
	}	
	@DisplayName("Valor do Documento")
	public Money getValordocumento1() {
		return valordocumento1;
	}
	@DisplayName("Valor do Documento")
	public Money getValordocumento2() {
		return valordocumento2;
	}
	@DisplayName("Situa��o")
	public List<Entregadocumentosituacao> getListaSituacao() {
		return listaSituacao;
	}
	@DisplayName("Tipo de Documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Boolean getExibirTotais() {
		return exibirTotais;
	}
	@DisplayName("Total dos Documentos")
	public Money getTotalgeral() {
		return totalgeral;
	}
	@DisplayName("Total dos Pagamentos")
	public Money getTotalpagamento() {
		return totalpagamento;
	}
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}
	public void setTotalgeral(Money totalgeral) {
		this.totalgeral = totalgeral;
	}
	public void setTotalpagamento(Money totalpagamento) {
		this.totalpagamento = totalpagamento;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDtentrada1(Date dtentrada1) {
		this.dtentrada1 = dtentrada1;
	}
	public void setDtentrada2(Date dtentrada2) {
		this.dtentrada2 = dtentrada2;
	}
	public void setDtemissao1(Date dtemissao1) {
		this.dtemissao1 = dtemissao1;
	}
	public void setDtemissao2(Date dtemissao2) {
		this.dtemissao2 = dtemissao2;
	}
	public void setSituacaodocumento(Situacaodocumento situacaodocumento) {
		this.situacaodocumento = situacaodocumento;
	}
	public void setModelodocumentofiscal(
			Modelodocumentofiscal modelodocumentofiscal) {
		this.modelodocumentofiscal = modelodocumentofiscal;
	}
	public void setValordocumento1(Money valordocumento1) {
		this.valordocumento1 = valordocumento1;
	}
	public void setValordocumento2(Money valordocumento2) {
		this.valordocumento2 = valordocumento2;
	}
	public void setListaSituacao(List<Entregadocumentosituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	@DisplayName("Lote")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}

	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	public boolean isPopupCTRC() {
		return popupCTRC;
	}
	public void setPopupCTRC(boolean popupCTRC) {
		this.popupCTRC = popupCTRC;
	}

	public Cfop getCfop() {
		return cfop;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
}
