package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.TipoEventoNfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.OperacaoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.TipoLoteConsultaDfeEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LoteConsultaDfeFiltro extends FiltroListagemSined {

	private TipoLoteConsultaDfeEnum tipoLoteConsultaDfeEnum;
	private Empresa empresa;
	private Integer nsu;
	private String chaveAcesso;
	private TipoEventoNfe tipoEventoNfe;
	private Date dhRespInicio;
	private Date dhRespFim;
	private List<SituacaoEmissorEnum> listaSituacaoEnum;
	private Integer nsuInicio;
	private Integer nsuFim;
	
	@DisplayName("Tipo do Lote DF-e")
	public TipoLoteConsultaDfeEnum getTipoLoteConsultaDfeEnum() {
		return tipoLoteConsultaDfeEnum;
	}

	public void setTipoLoteConsultaDfeEnum(TipoLoteConsultaDfeEnum tipoLoteConsultaDfeEnum) {
		this.tipoLoteConsultaDfeEnum = tipoLoteConsultaDfeEnum;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@DisplayName("NSU")
	public Integer getNsu() {
		return nsu;
	}

	public void setNsu(Integer nsu) {
		this.nsu = nsu;
	}

	public String getChaveAcesso() {
		return chaveAcesso;
	}

	@DisplayName("Chave de Acesso")
	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	@DisplayName("")
	public Date getDhRespInicio() {
		return dhRespInicio;
	}
	
	public void setDhRespInicio(Date dhRespInicio) {
		this.dhRespInicio = dhRespInicio;
	}
	
	@DisplayName("")
	public Date getDhRespFim() {
		return dhRespFim;
	}
	
	public void setDhRespFim(Date dhRespFim) {
		this.dhRespFim = dhRespFim;
	}

	@DisplayName("Situa��o")
	public List<SituacaoEmissorEnum> getListaSituacaoEnum() {
		return listaSituacaoEnum;
	}
	
	public void setListaSituacaoEnum(List<SituacaoEmissorEnum> listaSituacaoEnum) {
		this.listaSituacaoEnum = listaSituacaoEnum;
	}

	@DisplayName("")
	public Integer getNsuInicio() {
		return nsuInicio;
	}

	public void setNsuInicio(Integer nsuInicio) {
		this.nsuInicio = nsuInicio;
	}

	@DisplayName("")
	public Integer getNsuFim() {
		return nsuFim;
	}

	public void setNsuFim(Integer nsuFim) {
		this.nsuFim = nsuFim;
	}

	@DisplayName("Opera��o")
	public TipoEventoNfe getTipoEventoNfe() {
		return tipoEventoNfe;
	}

	public void setTipoEventoNfe(TipoEventoNfe tipoEventoNfe) {
		this.tipoEventoNfe = tipoEventoNfe;
	}
	
	
}