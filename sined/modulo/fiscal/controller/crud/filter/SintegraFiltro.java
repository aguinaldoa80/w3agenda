package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraConvenio;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraFinalidadearquivo;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraNaturezaoperacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sintegrafiscal.registros.Registro50;
import br.com.linkcom.sintegrafiscal.registros.Registro51;
import br.com.linkcom.sintegrafiscal.registros.Registro53;
import br.com.linkcom.sintegrafiscal.registros.Registro54;
import br.com.linkcom.sintegrafiscal.registros.Registro55;
import br.com.linkcom.sintegrafiscal.registros.Registro60A;
import br.com.linkcom.sintegrafiscal.registros.Registro60D;
import br.com.linkcom.sintegrafiscal.registros.Registro60I;
import br.com.linkcom.sintegrafiscal.registros.Registro60M;
import br.com.linkcom.sintegrafiscal.registros.Registro61;
import br.com.linkcom.sintegrafiscal.registros.Registro61R;
import br.com.linkcom.sintegrafiscal.registros.Registro70;
import br.com.linkcom.sintegrafiscal.registros.Registro71;
import br.com.linkcom.sintegrafiscal.registros.Registro74;
import br.com.linkcom.sintegrafiscal.registros.Registro75;
import br.com.linkcom.sintegrafiscal.registros.Registro76;
import br.com.linkcom.sintegrafiscal.registros.Registro77;

public class SintegraFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected SIntegraConvenio convenio;
	protected SIntegraNaturezaoperacao naturezaoperacao;
	protected SIntegraFinalidadearquivo finalidadearquivo;
	
	protected Boolean buscarentradas = Boolean.TRUE;
	protected Boolean buscarsaidasProduto = Boolean.TRUE;
	protected Boolean buscarsaidasServico = Boolean.FALSE;
	protected Boolean gerardadoscupomfiscal = Boolean.FALSE;
	protected Boolean gerarRegistro61;
	protected Boolean gerarRegistro61R;
	protected Boolean gerarRegisro71;
	protected Boolean gerarRegisro74= Boolean.FALSE;
	protected Inventario inventario;
	protected Boolean considerarnotasnaoeletronicas;
	protected String modelo;
	
	private List<String> listaErro;
	private Map<String, Integer> mapTotalizadorRegistros = new HashMap<String, Integer>();
	
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Data In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	
	@DisplayName("Data Fim")
	public Date getDtfim() {
		return dtfim;
	}
	
	@DisplayName("Conv�nio")
	public SIntegraConvenio getConvenio() {
		return convenio;
	}

	@DisplayName("Natureza das opera��es identificadas")
	public SIntegraNaturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	@DisplayName("Finalidade do arquivo")
	public SIntegraFinalidadearquivo getFinalidadearquivo() {
		return finalidadearquivo;
	}
	
	@DisplayName("Entradas")
	public Boolean getBuscarentradas() {
		return buscarentradas;
	}
	
	@DisplayName("Sa�das (Produto)")
	public Boolean getBuscarsaidasProduto() {
		return buscarsaidasProduto;
	}
	@DisplayName("Sa�das (Servi�o)")
	public Boolean getBuscarsaidasServico() {
		return buscarsaidasServico;
	}
	
	@DisplayName("Gerar Registro 61")
	public Boolean getGerarRegistro61() {
		return gerarRegistro61;
	}
	
	@DisplayName("Gerar Registro 61R")
	public Boolean getGerarRegistro61R() {
		return gerarRegistro61R;
	}
	
	@DisplayName("Gerar Registro 71")
	public Boolean getGerarRegisro71() {
		return gerarRegisro71;
	}
	
	@DisplayName("Gerar dados de cupom fiscal")
	public Boolean getGerardadoscupomfiscal() {
		return gerardadoscupomfiscal;
	}
	
	public void setGerardadoscupomfiscal(Boolean gerardadoscupomfiscal) {
		this.gerardadoscupomfiscal = gerardadoscupomfiscal;
	}
	
	public void setBuscarentradas(Boolean buscarentradas) {
		this.buscarentradas = buscarentradas;
	}

	public void setBuscarsaidasProduto(Boolean buscarsaidasProduto) {
		this.buscarsaidasProduto = buscarsaidasProduto;
	}
	public void setBuscarsaidasServico(Boolean buscarsaidasServico) {
		this.buscarsaidasServico = buscarsaidasServico;
	}
	public void setGerarRegisro71(Boolean gerarRegisro71) {
		this.gerarRegisro71 = gerarRegisro71;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setConvenio(SIntegraConvenio convenio) {
		this.convenio = convenio;
	}
	public void setNaturezaoperacao(SIntegraNaturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setFinalidadearquivo(SIntegraFinalidadearquivo finalidadearquivo) {
		this.finalidadearquivo = finalidadearquivo;
	}
	public void setGerarRegistro61(Boolean gerarRegistro61) {
		this.gerarRegistro61 = gerarRegistro61;
	}
	public void setGerarRegistro61R(Boolean gerarRegistro61R) {
		this.gerarRegistro61R = gerarRegistro61R;
	}
	
	
	public List<String> getListaErro() {
		return listaErro;
	}
	public void setListaErro(List<String> listaErro) {
		this.listaErro = listaErro;
	}
	
	public void addError(String erro){
		if(listaErro == null){
			listaErro = new ArrayList<String>();
		}
		listaErro.add(erro);
	}
	
	public Map<String, Integer> getMapTotalizadorRegistros() {
		return mapTotalizadorRegistros;
	}
	public void setMapTotalizadorRegistros(Map<String, Integer> mapTotalizadorRegistros) {
		this.mapTotalizadorRegistros = mapTotalizadorRegistros;
	}
	
	public void addRegistro(String registro){
		if(mapTotalizadorRegistros.get(registro) == null){
			mapTotalizadorRegistros.put(registro, 1);
		}else
			mapTotalizadorRegistros.put(registro, mapTotalizadorRegistros.get(registro)+1);
	}
	@DisplayName("Gerar Registro 74")
	public Boolean getGerarRegisro74() {
		return gerarRegisro74;
	}

	public void setGerarRegisro74(Boolean gerarRegisro74) {
		this.gerarRegisro74 = gerarRegisro74;
	}

	public Inventario getInventario() {
		return inventario;
	}

	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}
	
	@DisplayName("Considerar notas n�o eletr�nicas")
	public Boolean getConsiderarnotasnaoeletronicas() {
		return considerarnotasnaoeletronicas;
	}

	@MaxLength(2)
	public String getModelo() {
		return modelo;
	}

	public void setConsiderarnotasnaoeletronicas(
			Boolean considerarnotasnaoeletronicas) {
		this.considerarnotasnaoeletronicas = considerarnotasnaoeletronicas;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public void inicializarRegistros(){
		mapTotalizadorRegistros.put(Registro50.TIPO, 0);
		mapTotalizadorRegistros.put(Registro51.TIPO, 0);
		mapTotalizadorRegistros.put(Registro53.TIPO, 0);
		mapTotalizadorRegistros.put(Registro54.TIPO, 0);
		mapTotalizadorRegistros.put(Registro55.TIPO, 0);
		mapTotalizadorRegistros.put(Registro61.TIPO, 0);
		mapTotalizadorRegistros.put(Registro61R.TIPO, 0);
		mapTotalizadorRegistros.put(Registro60M.TIPO_TOTALIZADOR, 0);
		mapTotalizadorRegistros.put(Registro60A.TIPO_TOTALIZADOR, 0);
		mapTotalizadorRegistros.put(Registro60D.TIPO_TOTALIZADOR, 0);
		mapTotalizadorRegistros.put(Registro60I.TIPO_TOTALIZADOR, 0);
		mapTotalizadorRegistros.put(Registro70.TIPO, 0);
		mapTotalizadorRegistros.put(Registro71.TIPO, 0);
		mapTotalizadorRegistros.put(Registro74.TIPO, 0);
		mapTotalizadorRegistros.put(Registro75.TIPO, 0);
		mapTotalizadorRegistros.put(Registro76.TIPO, 0);
		mapTotalizadorRegistros.put(Registro77.TIPO, 0);
	}
}
