package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadorincidenciatributaria;
import br.com.linkcom.sined.geral.bean.enumeration.Metodoapropriacaocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.SPEDPiscofinsApoio;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM410;
import br.com.linkcom.spedfiscal.spedpiscofins.registros.RegistroM810;

public class SpedpiscofinsFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected Indicadorincidenciatributaria indicadorincidenciatributaria;
	protected Metodoapropriacaocredito metodoapropriacaocredito;
	protected Boolean gerarC170Saida;
	protected Boolean gerarC500 = Boolean.TRUE;
	protected Boolean gerarD500 = Boolean.TRUE;
	protected Boolean buscardadosfilias;
	protected Boolean buscarentradas = Boolean.TRUE;
	protected Boolean buscarsaidas = Boolean.TRUE;
	
	private SPEDPiscofinsApoio apoio;
	private List<String> listaErro;
	
	protected String cnpjEmpresa;
	protected List<Empresa> listaFiliais;
	protected List<Empresa> listaEmpresaFiliais;
	
	private List<Naturezabasecalculocredito> listaNaturezabccredito;
	private List<RegistroM410> listaAuxRegistroM410;
	private List<RegistroM810> listaAuxRegistroM810;

	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Data de In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}

	@DisplayName("Data Final")
	public Date getDtfim() {
		return dtfim;
	}
	@Required
	@DisplayName("Indicador da incid�ncia tribut�ria no per�odo")
	public Indicadorincidenciatributaria getIndicadorincidenciatributaria() {
		return indicadorincidenciatributaria;
	}
	
	@DisplayName("M�todo de apropria��o de cr�ditos")
	public Metodoapropriacaocredito getMetodoapropriacaocredito() {
		return metodoapropriacaocredito;
	}
	
	@DisplayName("Gerar C170 das Sa�das")
	public Boolean getGerarC170Saida() {
		return gerarC170Saida;
	}
	
	@DisplayName("Entradas")
	public Boolean getBuscarentradas() {
		return buscarentradas;
	}
	
	@DisplayName("Sa�das")
	public Boolean getBuscarsaidas() {
		return buscarsaidas;
	}

	public void setBuscarentradas(Boolean buscarentradas) {
		this.buscarentradas = buscarentradas;
	}
	public void setBuscarsaidas(Boolean buscarsaidas) {
		this.buscarsaidas = buscarsaidas;
	}
	public void setGerarC170Saida(Boolean gerarC170Saida) {
		this.gerarC170Saida = gerarC170Saida;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setIndicadorincidenciatributaria(
			Indicadorincidenciatributaria indicadorincidenciatributaria) {
		this.indicadorincidenciatributaria = indicadorincidenciatributaria;
	}
	public void setMetodoapropriacaocredito(
			Metodoapropriacaocredito metodoapropriacaocredito) {
		this.metodoapropriacaocredito = metodoapropriacaocredito;
	}
	
	public SPEDPiscofinsApoio getApoio() {
		return apoio;
	}
	public List<String> getListaErro() {
		return listaErro;
	}
	public void setApoio(SPEDPiscofinsApoio apoio) {
		this.apoio = apoio;
	}
	public void setListaErro(List<String> listaErro) {
		this.listaErro = listaErro;
	}
	
	public void addError(String erro){
		if(listaErro == null){
			listaErro = new ArrayList<String>();
		}
		listaErro.add(erro);
	}
	public String getCnpjEmpresa() {
		return cnpjEmpresa;
	}
	public void setCnpjEmpresa(String cnpjEmpresa) {
		this.cnpjEmpresa = cnpjEmpresa;
	}

	public List<Naturezabasecalculocredito> getListaNaturezabccredito() {
		return listaNaturezabccredito;
	}
	public void setListaNaturezabccredito(List<Naturezabasecalculocredito> listaNaturezabccredito) {
		this.listaNaturezabccredito = listaNaturezabccredito;
	}

	@DisplayName("Buscar dados das filiais")
	public Boolean getBuscardadosfilias() {
		return buscardadosfilias;
	}
	public void setBuscardadosfilias(Boolean buscardadosfilias) {
		this.buscardadosfilias = buscardadosfilias;
	}

	public List<Empresa> getListaFiliais() {
		return listaFiliais;
	}
	public void setListaFiliais(List<Empresa> listaFiliais) {
		this.listaFiliais = listaFiliais;
	}
	
	public String getWhereInEmpresaFiliais(){
		StringBuilder ids = new StringBuilder();
		if(this.getEmpresa() != null && this.getEmpresa().getCdpessoa() != null){
			ids.append(this.getEmpresa().getCdpessoa());
		}
		String idsFiliais = this.getWhereInFiliais();
		if(idsFiliais != null && !"".equals(idsFiliais)){
			ids.append(!"".equals(ids.toString()) ? "," : "").append(idsFiliais);
		}
		return ids.toString();
	}
	
	public String getWhereInFiliais(){
		StringBuilder ids = new StringBuilder();
		if(this.listaFiliais != null && !this.listaFiliais.isEmpty()){
			for(Empresa e : this.listaFiliais){
				if(e.getCdpessoa() != null){
					ids.append(!"".equals(ids.toString()) ? "," : "").append(e.getCdpessoa());
				}
			}
		}
		return ids.toString();
	}

	public List<Empresa> getListaEmpresaFiliais() {
		return listaEmpresaFiliais;
	}
	public void setListaEmpresaFiliais(List<Empresa> listaEmpresaFiliais) {
		this.listaEmpresaFiliais = listaEmpresaFiliais;
	}

	public List<RegistroM410> getListaAuxRegistroM410() {
		return listaAuxRegistroM410;
	}
	public List<RegistroM810> getListaAuxRegistroM810() {
		return listaAuxRegistroM810;
	}

	public void setListaAuxRegistroM810(List<RegistroM810> listaAuxRegistroM810) {
		this.listaAuxRegistroM810 = listaAuxRegistroM810;
	}
	public void setListaAuxRegistroM410(List<RegistroM410> listaAuxRegistroM410) {
		this.listaAuxRegistroM410 = listaAuxRegistroM410;
	}

	@DisplayName("Gerar C500")
	public Boolean getGerarC500() {
		return gerarC500;
	}
	@DisplayName("Gerar D500")
	public Boolean getGerarD500() {
		return gerarD500;
	}

	public void setGerarC500(Boolean gerarC500) {
		this.gerarC500 = gerarC500;
	}
	public void setGerarD500(Boolean gerarD500) {
		this.gerarD500 = gerarD500;
	}
	
	public boolean isCnpjEmpresaFiltro(){
		return false;
	}
}
