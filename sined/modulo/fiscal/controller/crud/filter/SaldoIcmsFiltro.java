package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class SaldoIcmsFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private Money valor;
	private String tipoUtilizacaoCredito;
	protected Date mesAno;
	protected String mesAnoAux;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Money getValor() {
		return valor;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@MaxLength(8)
	@DisplayName("Tipo de Utiliza��o de Cr�dito")
	public String getTipoUtilizacaoCredito() {
		return tipoUtilizacaoCredito;
	}
	
	public void setTipoUtilizacaoCredito(String tipoUtilizacaoCredito) {
		this.tipoUtilizacaoCredito = tipoUtilizacaoCredito;
	}
	
	public Date getMesAno() {
		return mesAno;
	}
	
	public void setMesAno(Date mesAno) {
		this.mesAno = mesAno;
	}
	
	@DisplayName("M�s/ano")
	@Transient
	public String getMesAnoAux() {
		if(this.mesAno != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesAno);
		return null;
	}
	
	public void setMesAnoAux(String mesAnoAux) {
		try {
			if(mesAnoAux != null){
				this.mesAno = new Date(new SimpleDateFormat("MM/yyyy").parse(mesAnoAux).getTime());
			} else {
				this.mesAno = null;
			}
		} catch (ParseException e) {
			this.mesAno = null;
		};
	}
}
