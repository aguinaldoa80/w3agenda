package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.Faixaimpostoapuracao;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Aux_apuracaoimposto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ApuracaoimpostoFiltro extends FiltroListagemSined{

	protected Empresa empresa;
	protected Date dtinicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtfim = SinedDateUtils.lastDateOfMonth();
	protected Faixaimpostoapuracao faixaimpostoapuracao;
	protected Boolean entrada = Boolean.TRUE;
	protected Boolean saida = Boolean.TRUE;
	protected Boolean impostoretido = Boolean.FALSE;
	
	protected List<Aux_apuracaoimposto> listaApuracaoimposto;
	
	protected Money totalvalorbruto = new Money();
	protected Money totalbasecalculo = new Money();
	protected Double totalaliquota = 0.0;
	protected Money totalvalor = new Money();
	protected Money totalentrada = new Money();
	protected Money totalsaida = new Money();
	protected Money totalvalorliquido = new Money();
	protected Money totalretencoes = new Money();
	
	public ApuracaoimpostoFiltro() {
		dtinicio = SinedDateUtils.firstDateOfMonth();
		dtfim = SinedDateUtils.lastDateOfMonth();
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Data In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data Fim")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Servi�os Prestados")
	public List<Aux_apuracaoimposto> getListaApuracaoimposto() {
		return listaApuracaoimposto;
	}
	@DisplayName("Imposto")
	public Faixaimpostoapuracao getFaixaimpostoapuracao() {
		return faixaimpostoapuracao;
	}

	public Boolean getEntrada() {
		return entrada;
	}

	@DisplayName("S�ida")
	public Boolean getSaida() {
		return saida;
	}

	@DisplayName("Exibir somente notas com reten��o")
	public Boolean getImpostoretido() {
		return impostoretido;
	}
	
	public void setImpostoretido(Boolean impostoretido) {
		this.impostoretido = impostoretido;
	}
	
	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}

	public void setSaida(Boolean saida) {
		this.saida = saida;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setListaApuracaoimposto(List<Aux_apuracaoimposto> listaApuracaoimposto) {
		this.listaApuracaoimposto = listaApuracaoimposto;
	}
	public void setFaixaimpostoapuracao(Faixaimpostoapuracao faixaimpostoapuracao) {
		this.faixaimpostoapuracao = faixaimpostoapuracao;
	}

	@DisplayName("Total Bruto (R$)")
	public Money getTotalvalorbruto() {
		return totalvalorbruto;
	}
	@DisplayName("Total Base de C�lculo (R$)")
	public Money getTotalbasecalculo() {
		return totalbasecalculo;
	}
	@DisplayName("Total Al�quota(%)")
	public Double getTotalaliquota() {
		return totalaliquota;
	}
	@DisplayName("Imposto Total (R$)")
	public Money getTotalvalor() {
		return totalvalor;
	}
	@DisplayName("Total Valor L�quido (R$)")
	public Money getTotalvalorliquido() {
		return totalvalorliquido;
	}
	@DisplayName("Imposto Entrada (R$)")
	public Money getTotalentrada() {
		return totalentrada;
	}
	@DisplayName("Imposto Sa�da (R$)")
	public Money getTotalsaida() {
		return totalsaida;
	}
	@DisplayName("Imposto Retido (R$)")
	public Money getTotalretencoes() {
		return totalretencoes;
	}
	
	public void setTotalsaida(Money totalsaida) {
		this.totalsaida = totalsaida;
	}
	public void setTotalentrada(Money totalentrada) {
		this.totalentrada = totalentrada;
	}
	public void setTotalvalorbruto(Money totalvalorbruto) {
		this.totalvalorbruto = totalvalorbruto;
	}
	public void setTotalbasecalculo(Money totalbciss) {
		this.totalbasecalculo = totalbciss;
	}
	public void setTotalaliquota(Double totalaliquota) {
		this.totalaliquota = totalaliquota;
	}
	public void setTotalvalor(Money totalvalor) {
		this.totalvalor = totalvalor;
	}
	public void setTotalvalorliquido(Money totalvalorliquido) {
		this.totalvalorliquido = totalvalorliquido;
	}
	public void setTotalretencoes(Money totalretencoes) {
		this.totalretencoes = totalretencoes;
	}
}
