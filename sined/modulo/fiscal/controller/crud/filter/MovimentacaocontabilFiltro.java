package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MovimentacaocontabilFiltro extends FiltroListagemSined {
	
	private Date dtlancamentoinicio;
	private Date dtlancamentofim;
	private Empresa empresa;
	private Boolean buscarDadosFiliais;
	private Operacaocontabil operacaocontabil;
	private MovimentacaocontabilTipoLancamentoEnum tipoLancamento;
	private String numero;
	private Centrocusto centrocustodebito;
	private Centrocusto centrocustocredito;
	private Money valorde;
	private Money valorate;
	private ContaContabil contaContabilDebito;
	private ContaContabil contaContabilCredito;
	private Integer cdmovimentacaocontabilde;
	private Integer cdmovimentacaocontabilate;
	private Projeto projetodebito;
	private Projeto projetocredito;
	private Money totalValor;
	private Integer cdLoteContabil;
	
	public Money getTotalValor() {
		return totalValor;
	}
	public Date getDtlancamentoinicio() {
		return dtlancamentoinicio;
	}
	public Date getDtlancamentofim() {
		return dtlancamentofim;
	}
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Buscar dados das filiais")
	public Boolean getBuscarDadosFiliais() {
		return buscarDadosFiliais;
	}
	@DisplayName("Opera��o cont�bil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	@DisplayName("Tipo de lan�amento")
	public MovimentacaocontabilTipoLancamentoEnum getTipoLancamento() {
		return tipoLancamento;
	}
	@MaxLength(10)
	@DisplayName("Documento")
	public String getNumero() {
		return numero;
	}
	@DisplayName("Centro de custo d�bito")
	public Centrocusto getCentrocustodebito() {
		return centrocustodebito;
	}
	@DisplayName("Centro de custo cr�dito")
	public Centrocusto getCentrocustocredito() {
		return centrocustocredito;
	}
	public Money getValorde() {
		return valorde;
	}
	public Money getValorate() {
		return valorate;
	}
	@DisplayName("Conta cont�bil d�bito")
	public ContaContabil getContaContabilDebito() {
		return contaContabilDebito;
	}
	@DisplayName("Conta cont�bil cr�dito")
	public ContaContabil getContaContabilCredito() {
		return contaContabilCredito;
	}
	@DisplayName("ID")
	@MaxLength(9)
	public Integer getCdmovimentacaocontabilde() {
		return cdmovimentacaocontabilde;
	}
	@MaxLength(9)
	public Integer getCdmovimentacaocontabilate() {
		return cdmovimentacaocontabilate;
	}
	@DisplayName("Projeto d�bito")
	public Projeto getProjetodebito() {
		return projetodebito;
	}
	@DisplayName("Projeto cr�dito")
	public Projeto getProjetocredito() {
		return projetocredito;
	}
	@DisplayName("Lote Cont�bil")
	public Integer getCdLoteContabil() {
		return cdLoteContabil;
	}
	
	public void setDtlancamentoinicio(Date dtlancamentoinicio) {
		this.dtlancamentoinicio = dtlancamentoinicio;
	}
	public void setDtlancamentofim(Date dtlancamentofim) {
		this.dtlancamentofim = dtlancamentofim;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setBuscarDadosFiliais(Boolean buscarDadosFiliais) {
		this.buscarDadosFiliais = buscarDadosFiliais;
	}
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	public void setTipoLancamento(MovimentacaocontabilTipoLancamentoEnum tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setCentrocustodebito(Centrocusto centrocustodebito) {
		this.centrocustodebito = centrocustodebito;
	}
	public void setCentrocustocredito(Centrocusto centrocustocredito) {
		this.centrocustocredito = centrocustocredito;
	}
	public void setValorde(Money valorde) {
		this.valorde = valorde;
	}
	public void setValorate(Money valorate) {
		this.valorate = valorate;
	}
	public void setContaContabilDebito(ContaContabil contaContabilDebito) {
		this.contaContabilDebito = contaContabilDebito;
	}
	public void setContaContabilCredito(ContaContabil contaContabilCredito) {
		this.contaContabilCredito = contaContabilCredito;
	}
	public void setCdmovimentacaocontabilde(Integer cdmovimentacaocontabilde) {
		this.cdmovimentacaocontabilde = cdmovimentacaocontabilde;
	}
	public void setCdmovimentacaocontabilate(Integer cdmovimentacaocontabilate) {
		this.cdmovimentacaocontabilate = cdmovimentacaocontabilate;
	}
	public void setProjetodebito(Projeto projetodebito) {
		this.projetodebito = projetodebito;
	}
	public void setProjetocredito(Projeto projetocredito) {
		this.projetocredito = projetocredito;
	}
	public void setTotalValor(Money totalValor) {
		this.totalValor = totalValor;
	}
	public void setCdLoteContabil(Integer cdLoteContabil) {
		this.cdLoteContabil = cdLoteContabil;
	}
}