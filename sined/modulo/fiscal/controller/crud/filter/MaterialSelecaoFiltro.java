package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MaterialSelecaoFiltro extends FiltroListagemSined {
	
	protected String nome;
	protected String codigo;
	protected String identificacao;

	public String getNome() {
		return nome;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getIdentificacao() {
		return identificacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
}
