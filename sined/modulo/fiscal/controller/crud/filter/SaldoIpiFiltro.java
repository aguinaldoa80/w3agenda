package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class SaldoIpiFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private Money valor;
	private CreditoDebitoEnum tipoUtilizacao;
	protected Date mesAno;
	protected String mesAnoAux;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Money getValor() {
		return valor;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@DisplayName("Tipo de Utiliza��o")
	public CreditoDebitoEnum getTipoUtilizacao() {
		return tipoUtilizacao;
	}
	
	public void setTipoUtilizacao(CreditoDebitoEnum tipoUtilizacao) {
		this.tipoUtilizacao = tipoUtilizacao;
	}
	
	public Date getMesAno() {
		return mesAno;
	}
	
	public void setMesAno(Date mesAno) {
		this.mesAno = mesAno;
	}
	
	@DisplayName("M�s/ano")
	@Transient
	public String getMesAnoAux() {
		if(this.mesAno != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesAno);
		return null;
	}
	
	public void setMesAnoAux(String mesAnoAux) {
		try {
			if(mesAnoAux != null){
				this.mesAno = new Date(new SimpleDateFormat("MM/yyyy").parse(mesAnoAux).getTime());
			} else {
				this.mesAno = null;
			}
		} catch (ParseException e) {
			this.mesAno = null;
		};
	}
}
