package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProcessoReferenciadoFiltro extends FiltroListagemSined {
	
	protected Empresa empresa;
	protected Integer numeroProcessoJudicial;
	protected Date dataSentenca;
	protected Boolean ativo;
	
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Integer getNumeroProcessoJudicial() {
		return numeroProcessoJudicial;
	}
	public void setNumeroProcessoJudicial(Integer numeroProcessoJudicial) {
		this.numeroProcessoJudicial = numeroProcessoJudicial;
	}
	public Date getDataSentenca() {
		return dataSentenca;
	}
	public void setDataSentenca(Date dataSentenca) {
		this.dataSentenca = dataSentenca;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	
}
