package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Codobrigacaoicmsrecolher;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ObrigacaoicmsrecolherFiltro extends FiltroListagemSined {
	
	protected Empresa empresa;
	protected Uf uf;
	protected Codobrigacaoicmsrecolher codobrigacaoicmsrecolher;
	protected Money valorobrigacaoInicio;
	protected Money valorobrigacaoFim;
	protected Date dtvencimentoInicio;
	protected Date dtvencimentoFim;
	protected String codigoreceita;
	protected String numeroprocesso;
	protected String mesreferencia;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Uf getUf() {
		return uf;
	}
	@DisplayName("C�digo do ICMS/ ICMS ST recolhido ou a recolher")
	public Codobrigacaoicmsrecolher getCodobrigacaoicmsrecolher() {
		return codobrigacaoicmsrecolher;
	}
	@DisplayName("C�digo de receita")
	public String getCodigoreceita() {
		return codigoreceita;
	}
	@MaxLength(15)
	@DisplayName("N�mero de processo")
	public String getNumeroprocesso() {
		return numeroprocesso;
	}
	@MaxLength(255)
	@DisplayName("M�s de refer�ncia")
	public String getMesreferencia() {
		return mesreferencia;
	}
	public Money getValorobrigacaoInicio() {
		return valorobrigacaoInicio;
	}
	public Money getValorobrigacaoFim() {
		return valorobrigacaoFim;
	}
	public Date getDtvencimentoInicio() {
		return dtvencimentoInicio;
	}
	public Date getDtvencimentoFim() {
		return dtvencimentoFim;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setCodobrigacaoicmsrecolher(
			Codobrigacaoicmsrecolher codobrigacaoicmsrecolher) {
		this.codobrigacaoicmsrecolher = codobrigacaoicmsrecolher;
	}
	public void setCodigoreceita(String codigoreceita) {
		this.codigoreceita = codigoreceita;
	}
	public void setNumeroprocesso(String numeroprocesso) {
		this.numeroprocesso = numeroprocesso;
	}
	public void setMesreferencia(String mesreferencia) {
		this.mesreferencia = mesreferencia;
	}
	public void setValorobrigacaoInicio(Money valorobrigacaoInicio) {
		this.valorobrigacaoInicio = valorobrigacaoInicio;
	}
	public void setValorobrigacaoFim(Money valorobrigacaoFim) {
		this.valorobrigacaoFim = valorobrigacaoFim;
	}
	public void setDtvencimentoInicio(Date dtvencimentoInicio) {
		this.dtvencimentoInicio = dtvencimentoInicio;
	}
	public void setDtvencimentoFim(Date dtvencimentoFim) {
		this.dtvencimentoFim = dtvencimentoFim;
	}
}
