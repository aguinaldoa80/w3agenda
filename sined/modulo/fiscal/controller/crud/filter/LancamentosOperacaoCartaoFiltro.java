package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LancamentosOperacaoCartaoFiltro  extends FiltroListagemSined  {
	private Integer cdLancamentoOperacaoCredito;
	protected Date dtLancamentoInicio;
	protected Date dtLancamentoFim;
	private Empresa empresa;
	private Fornecedor credenciadora;
	private Money valorCreditoDe;
	private Money valorCreditoA;
	private Money valorDebitoDe;
	private Money valorDebitoA;
	
	
	@DisplayName("C�digo")
	public Integer getCdLancamentoOperacaoCredito() {
		return cdLancamentoOperacaoCredito;
	}
	public Date getDtLancamentoInicio() {
		return dtLancamentoInicio;
	}
	public Date getDtLancamentoFim() {
		return dtLancamentoFim;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Fornecedor getCredenciadora() {
		return credenciadora;
	}
	

	public Money getValorCreditoDe() {
		return valorCreditoDe;
	}
	
	public Money getValorCreditoA() {
		return valorCreditoA;
	}
	
	public Money getValorDebitoDe() {
		return valorDebitoDe;
	}
	
	public Money getValorDebitoA() {
		return valorDebitoA;
	}
	
	public void setCdLancamentoOperacaoCredito(Integer cdLancamentoOperacaoCredito) {
		this.cdLancamentoOperacaoCredito = cdLancamentoOperacaoCredito;
	}
	
	public void setDtLancamentoInicio(Date dtLancamentoInicio) {
		this.dtLancamentoInicio = dtLancamentoInicio;
	}
	
	public void setDtLancamentoFim(Date dtLancamentoFim) {
		this.dtLancamentoFim = dtLancamentoFim;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setCredenciadora(Fornecedor credenciadora) {
		this.credenciadora = credenciadora;
	}
	
	public void setValorCreditoDe(Money valorCreditoDe) {
		this.valorCreditoDe = valorCreditoDe;
	}
	
	public void setValorCreditoA(Money valorCreditoA) {
		this.valorCreditoA = valorCreditoA;
	}
	
	public void setValorDebitoDe(Money valorDebitoDe) {
		this.valorDebitoDe = valorDebitoDe;
	}
	
	public void setValorDebitoA(Money valorDebitoA) {
		this.valorDebitoA = valorDebitoA;
	}
	
	///Auxiliares
	@DisplayName("Inicio do Exercicio")
	@Transient
	public String getDtInicio() {
		if(this.dtLancamentoInicio != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtLancamentoInicio);
		return null;
	}
	public void setDtInicio(String dtInicio) {
		if(dtInicio!=null){
			try {
				this.dtLancamentoInicio = new Date(new SimpleDateFormat("MM/yyyy").parse(dtInicio).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};
		}else{
			setDtLancamentoInicio(null);
		}
	}
	@DisplayName("Fim do Exercicio")
	@Transient
	public String getDtFim() {
		if(this.dtLancamentoFim != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtLancamentoFim);
		return null;
	}
	public void setDtFim(String dtFim) {
		if(dtFim!=null){
			try {
				this.dtLancamentoFim = new Date(new SimpleDateFormat("MM/yyyy").parse(dtFim).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};
		}else{
			setDtLancamentoFim(null);
		}
	}
}
