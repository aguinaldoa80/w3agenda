package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.enumeration.FormaApuracao;
import br.com.linkcom.sined.geral.bean.enumeration.IndicadorInicioDoPeriodo;
import br.com.linkcom.sined.geral.bean.enumeration.IndicadorSituacaoEspecial;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LcdprArquivoFiltro extends FiltroListagemSined {
	private Colaborador colaborador;
	private IndicadorInicioDoPeriodo indicadorInicioDoPeriodo;
	private IndicadorSituacaoEspecial indicadorSituacaoEspecial;
	private FormaApuracao formaApuracao;
	private Date dtinicio;
	private Date dtfim;
	private Date dtFalecimento;
	
	@DisplayName("Propriet�rio")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@DisplayName("Inidicador do In�cio do Per�odo")
	public IndicadorInicioDoPeriodo getIndicadorInicioDoPeriodo() {
		return indicadorInicioDoPeriodo;
	}
	
	@DisplayName("Indicador de Situa��o Especial e Outros Eventos")
	public IndicadorSituacaoEspecial getIndicadorSituacaoEspecial() {
		return indicadorSituacaoEspecial;
	}
	
	@DisplayName("Forma de Apura��o")
	public FormaApuracao getFormaApuracao() {
		return formaApuracao;
	}
	
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	
	@DisplayName("Data de Falecimento")
	public Date getDtFalecimento() {
		return dtFalecimento;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setIndicadorInicioDoPeriodo(
			IndicadorInicioDoPeriodo indicadorInicioDoPeriodo) {
		this.indicadorInicioDoPeriodo = indicadorInicioDoPeriodo;
	}
	public void setIndicadorSituacaoEspecial(
			IndicadorSituacaoEspecial indicadorSituacaoEspecial) {
		this.indicadorSituacaoEspecial = indicadorSituacaoEspecial;
	}
	public void setFormaApuracao(FormaApuracao formaApuracao) {
		this.formaApuracao = formaApuracao;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setDtFalecimento(Date dtFalecimento) {
		this.dtFalecimento = dtFalecimento;
	}
}
