package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration.Documentoenvioboletosituacaoagendamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentoenvioboletoFiltro extends FiltroListagemSined{

	private Empresa empresa;
	private Cliente cliente;
	private Date dtinicio;
	private Date dtfim;
	private Documentoenvioboletosituacaoagendamento situacaoagendamento;
	private List<Documentoacao> listaAcao;
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@DisplayName("Vencimento do agendamento")
	public Date getDtinicio() {
		return dtinicio;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	@DisplayName("Situa��o do agendamento")
	public Documentoenvioboletosituacaoagendamento getSituacaoagendamento() {
		return situacaoagendamento;
	}
	public void setSituacaoagendamento(
			Documentoenvioboletosituacaoagendamento situacaoagendamento) {
		this.situacaoagendamento = situacaoagendamento;
	}
	
	@DisplayName("Situa��o da conta")
	public List<Documentoacao> getListaAcao() {
		return listaAcao;
	}
	public void setListaAcao(List<Documentoacao> listaAcao) {
		this.listaAcao = listaAcao;
	}
}
