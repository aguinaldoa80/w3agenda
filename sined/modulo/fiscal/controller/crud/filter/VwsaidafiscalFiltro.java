package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VwsaidafiscalFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Date dtemissaoinicio = SinedDateUtils.firstDateOfMonth();
	protected Date dtemissaofim = SinedDateUtils.lastDateOfMonth();
	protected Fornecedor fornecedor;
	protected Cliente cliente;
	protected String numero;
	protected List<NotaStatus> listanotastatus;
	
	{
		this.listanotastatus = NotaStatus.getTodosSaidaFiscal();
		
		this.listanotastatus.remove(NotaStatus.CANCELADA);
		this.listanotastatus.remove(NotaStatus.EM_ESPERA);
		this.listanotastatus.remove(NotaStatus.NFSE_CANCELANDO);
		this.listanotastatus.remove(NotaStatus.NFE_CANCELANDO);
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Data In�cio")
	public Date getDtemissaoinicio() {
		return dtemissaoinicio;
	}
	@DisplayName("Data Fim")
	public Date getDtemissaofim() {
		return dtemissaofim;
	}
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	@DisplayName("Situa��o")
	public List<NotaStatus> getListanotastatus() {
		return listanotastatus;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}	
	public void setListanotastatus(List<NotaStatus> listanotastatus) {
		this.listanotastatus = listanotastatus;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtemissaoinicio(Date dtemissaoinicio) {
		this.dtemissaoinicio = dtemissaoinicio;
	}
	public void setDtemissaofim(Date dtemissaofim) {
		this.dtemissaofim = dtemissaofim;
	}
}
