package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OperacaocontabilFiltro extends FiltroListagemSined{
	
	private String nome;
	private OperacaoContabilTipoLancamento tipoLancamento;
	private ContaContabil contacontabildebito;
	private ContaContabil contacontabilcredito;
	private Contagerencial contagerencial;

	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@DisplayName("Tipo de Lan�amento")
	public OperacaoContabilTipoLancamento getTipoLancamento() {
		return tipoLancamento;
	}
	@DisplayName("Conta D�bito")
	public ContaContabil getContacontabildebito() {
		return contacontabildebito;
	}
	@DisplayName("Conta Cr�dito")
	public ContaContabil getContacontabilcredito() {
		return contacontabilcredito;
	}
	@DisplayName("Conta Gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setTipoLancamento(OperacaoContabilTipoLancamento tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	public void setContacontabildebito(ContaContabil contacontabildebito) {
		this.contacontabildebito = contacontabildebito;
	}
	public void setContacontabilcredito(ContaContabil contacontabilcredito) {
		this.contacontabilcredito = contacontabilcredito;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
}