package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.ArquivoMdfeSituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivoMdfeFiltro extends FiltroListagemSined {
	
	private Configuracaonfe configuracaonfe;
	private Date dtenvio1;
	private Date dtenvio2;
	private String numeroNota;
	private Empresa empresa;
	private String ids;
	
	private List<ArquivoMdfeSituacao> listaSituacao;
	
	public ArquivoMdfeFiltro() {
		if(listaSituacao == null){
			listaSituacao = new ArrayList<ArquivoMdfeSituacao>();
		}
	}
	
	public List<ArquivoMdfeSituacao> getListaSituacao() {
		return listaSituacao;
	}
	
	@DisplayName("Configura��o")
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}

	public Date getDtenvio1() {
		return dtenvio1;
	}

	public Date getDtenvio2() {
		return dtenvio2;
	}
	
	@DisplayName("N�mero da nota")
	public String getNumeroNota() {
		return numeroNota;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Lote(s)")
	public String getIds() {
		return ids;
	}
	
	public void setIds(String ids) {
		this.ids = ids;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}

	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}

	public void setDtenvio1(Date dtenvio1) {
		this.dtenvio1 = dtenvio1;
	}

	public void setDtenvio2(Date dtenvio2) {
		this.dtenvio2 = dtenvio2;
	}

	public void setListaSituacao(List<ArquivoMdfeSituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	
}
