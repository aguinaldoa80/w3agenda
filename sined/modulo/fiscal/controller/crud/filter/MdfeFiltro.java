package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.FormaemissaoMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModalidadefreteMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEmitenteMdfe;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTransportadorMdfe;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MdfeFiltro extends FiltroListagemSined{
	
	protected String serie;
	protected String numero;
	protected Empresa empresa;
	protected Timestamp dataHoraEmissaoInicio;
	protected Timestamp dataHoraEmissaoFim;
	protected Timestamp dataHoraInicioViagemInicio;
	protected Timestamp dataHoraInicioViagemFim;
	protected TipoEmitenteMdfe tipoEmitenteMdfe;
	protected TipoTransportadorMdfe tipoTransportador;
	protected ModalidadefreteMdfe modalidadeFrete;
	protected FormaemissaoMdfe formaEmissao;
	protected List<MdfeSituacao> listaMdfeSituacao;
	
	@DisplayName("S�rie")
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}

	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@DisplayName("Data e hora de emiss�o")
	public Timestamp getDataHoraEmissaoInicio() {
		return dataHoraEmissaoInicio;
	}
	public void setDataHoraEmissaoInicio(Timestamp datahoraemissao) {
		this.dataHoraEmissaoInicio = datahoraemissao;
	}
	
	public Timestamp getDataHoraEmissaoFim() {
		return dataHoraEmissaoFim;
	}
	public void setDataHoraEmissaoFim(Timestamp dataHoraEmissaoFim) {
		this.dataHoraEmissaoFim = dataHoraEmissaoFim;
	}

	@DisplayName("Data e hora de in�cio de viagem")
	public Timestamp getDataHoraInicioViagemInicio() {
		return dataHoraInicioViagemInicio;
	}
	public void setDataHoraInicioViagemInicio(Timestamp datahorainicioviagem) {
		this.dataHoraInicioViagemInicio = datahorainicioviagem;
	}
	
	public Timestamp getDataHoraInicioViagemFim() {
		return dataHoraInicioViagemFim;
	}
	public void setDataHoraInicioViagemFim(Timestamp dataHoraInicioViagemFim) {
		this.dataHoraInicioViagemFim = dataHoraInicioViagemFim;
	}
	
	@DisplayName("Tipo de emitente")
	public TipoEmitenteMdfe getTipoEmitenteMdfe() {
		return tipoEmitenteMdfe;
	}
	public void setTipoEmitenteMdfe(TipoEmitenteMdfe tipoemitente) {
		this.tipoEmitenteMdfe = tipoemitente;
	}

	@DisplayName("Tipo de transportador")
	public TipoTransportadorMdfe getTipoTransportador() {
		return tipoTransportador;
	}
	public void setTipoTransportador(TipoTransportadorMdfe tipotransportador) {
		this.tipoTransportador = tipotransportador;
	}

	@DisplayName("Modalidade de frete")
	public ModalidadefreteMdfe getModalidadeFrete() {
		return modalidadeFrete;
	}
	public void setModalidadeFrete(ModalidadefreteMdfe modalidadefrete) {
		this.modalidadeFrete = modalidadefrete;
	}
	
	@DisplayName("Forma de emiss�o")
	public FormaemissaoMdfe getFormaEmissao() {
		return formaEmissao;
	}
	public void setFormaEmissao(FormaemissaoMdfe formaemissao) {
		this.formaEmissao = formaemissao;
	}

	public List<MdfeSituacao> getListaMdfeSituacao() {
		return listaMdfeSituacao;
	}
	public void setListaMdfeSituacao(List<MdfeSituacao> listaMdfeSituacao) {
		this.listaMdfeSituacao = listaMdfeSituacao;
	}
}
