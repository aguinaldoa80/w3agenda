package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.CodigoAjusteIpi;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AjusteIpiFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private Date dtFatoGerador;
	private CreditoDebitoEnum tipoAjuste;
	private CodigoAjusteIpi codigoAjuste;

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@DisplayName("Data do Fato Gerador")
	public Date getDtFatoGerador() {
		return dtFatoGerador;
	}

	public void setDtFatoGerador(Date dtFatoGerador) {
		this.dtFatoGerador = dtFatoGerador;
	}

	@DisplayName("Tipo de Ajuste")
	public CreditoDebitoEnum getTipoAjuste() {
		return tipoAjuste;
	}

	public void setTipoAjuste(CreditoDebitoEnum tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}

	@DisplayName("C�digo de Ajuste")
	public CodigoAjusteIpi getCodigoAjuste() {
		return codigoAjuste;
	}

	public void setCodigoAjuste(CodigoAjusteIpi codigoAjuste) {
		this.codigoAjuste = codigoAjuste;
	}
}
