package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.ManifestoDfeSituacaoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ManifestoDfeFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private String chaveAcesso;
	private Tipopessoa tipopessoa;
	private Cnpj cnpj;
	private Cpf cpf;
	private Date dhEmissaoInicio;
	private Date dhEmissaoFim;
	private Date dhRecbtoInicio;
	private Date dhRecbtoFim;
	private String nProt;
	private List<LoteConsultaDfeItemStatusEnum> listaStatusEnum;
	private List<ManifestoDfeSituacaoEnum> listaManifestoDfeSituacaoEnum;
	private String nome;
	private Boolean outros;
	private String numeroNf;

	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	public Cpf getCpf() {
		return cpf;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}

	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}

	public Cnpj getCnpj() {
		return cnpj;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	
	@DisplayName("")
	public Date getDhEmissaoInicio() {
		return dhEmissaoInicio;
	}
	
	public void setDhEmissaoInicio(Date dhEmissaoInicio) {
		this.dhEmissaoInicio = dhEmissaoInicio;
	}
	
	@DisplayName("")
	public Date getDhEmissaoFim() {
		return dhEmissaoFim;
	}
	
	public void setDhEmissaoFim(Date dhEmissaoFim) {
		this.dhEmissaoFim = dhEmissaoFim;
	}

	@DisplayName("")
	public Date getDhRecbtoInicio() {
		return dhRecbtoInicio;
	}
	
	public void setDhRecbtoInicio(Date dhRecbtoInicio) {
		this.dhRecbtoInicio = dhRecbtoInicio;
	}
	
	@DisplayName("")
	public Date getDhRecbtoFim() {
		return dhRecbtoFim;
	}
	
	public void setDhRecbtoFim(Date dhRecbtoFim) {
		this.dhRecbtoFim = dhRecbtoFim;
	}

	@DisplayName("Protocolo")
	public String getnProt() {
		return nProt;
	}

	public void setnProt(String nProt) {
		this.nProt = nProt;
	}
	
	@DisplayName("Status NF-e")
	public List<LoteConsultaDfeItemStatusEnum> getListaStatusEnum() {
		return listaStatusEnum;
	}
	
	public void setListaStatusEnum(List<LoteConsultaDfeItemStatusEnum> listaStatusEnum) {
		this.listaStatusEnum = listaStatusEnum;
	}
	
	@DisplayName("Situa��o Manifesto")
	public List<ManifestoDfeSituacaoEnum> getListaManifestoDfeSituacaoEnum() {
		return listaManifestoDfeSituacaoEnum;
	}
	
	public void setListaManifestoDfeSituacaoEnum(List<ManifestoDfeSituacaoEnum> listaManifestoDfeSituacaoEnum) {
		this.listaManifestoDfeSituacaoEnum = listaManifestoDfeSituacaoEnum;
	}
	
	@DisplayName("Emitente")
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Boolean getOutros() {
		return outros;
	}
	
	public void setOutros(Boolean outros) {
		this.outros = outros;
	}
	
	@DisplayName("N�mero")
	public String getNumeroNf() {
		return numeroNf;
	}
	
	public void setNumeroNf(String numeroNf) {
		this.numeroNf = numeroNf;
	}

}