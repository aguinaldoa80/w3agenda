package br.com.linkcom.sined.modulo.fiscal.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Ajusteicmstipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.TipoAjuste;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AjusteicmsFiltro extends FiltroListagemSined {
	
	protected Empresa empresa;
	protected Date dtfatogerador;
	protected TipoAjuste tipoajuste;
	protected Ajusteicmstipo ajusteicmstipo;
	protected Uf uf;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Data do fato gerador")
	public Date getDtfatogerador() {
		return dtfatogerador;
	}
	@DisplayName("Tipo de ajuste")
	public TipoAjuste getTipoajuste() {
		return tipoajuste;
	}
	@DisplayName("Ajuste")
	public Ajusteicmstipo getAjusteicmstipo() {
		return ajusteicmstipo;
	}
	public Uf getUf() {
		return uf;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtfatogerador(Date dtfatogerador) {
		this.dtfatogerador = dtfatogerador;
	}
	public void setTipoajuste(TipoAjuste tipoajuste) {
		this.tipoajuste = tipoajuste;
	}
	public void setAjusteicmstipo(Ajusteicmstipo ajusteicmstipo) {
		this.ajusteicmstipo = ajusteicmstipo;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
}
