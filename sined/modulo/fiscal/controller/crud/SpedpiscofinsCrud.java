package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Spedpiscofins;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.SpedpiscofinsService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/fiscal/crud/Spedpiscofins", authorizationModule=CrudAuthorizationModule.class)
public class SpedpiscofinsCrud extends CrudControllerSined<SpedpiscofinsFiltro, Spedpiscofins, Spedpiscofins>{

	private SpedpiscofinsService spedpiscofinsService;
	private EmpresaService empresaService;
	
	public void setSpedpiscofinsService(SpedpiscofinsService spedpiscofinsService) {
		this.spedpiscofinsService = spedpiscofinsService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}


	@Override
	public ModelAndView doEditar(WebRequestContext request, Spedpiscofins form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Spedpiscofins form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Spedpiscofins form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@SuppressWarnings("unchecked")
	protected void setListagemInfo(WebRequestContext request, SpedpiscofinsFiltro filtro) throws Exception {
		if (!listagemVaziaPrimeiraVez() || filtro.isNotFirstTime()) {
			if(request.getParameter("resetCurrentPage") != null) {
				filtro.setCurrentPage(0);
			}
			
			ListagemResult<Spedpiscofins> listagemResult = getLista(request, filtro);
			List<Spedpiscofins> list = listagemResult.list();
			
			// CHAVES DOS ARQUIVOS PARA DOWNLOAD
			for (Spedpiscofins spedpiscofins : list) {
				if(spedpiscofins.getArquivo() != null)
					DownloadFileServlet.addCdfile(request.getSession(), new Long(spedpiscofins.getArquivo().getCdarquivo()));
			}
			
			request.setAttribute("lista", list);
			request.setAttribute("currentPage", filtro.getCurrentPage());
			request.setAttribute("numberOfPages", filtro.getNumberOfPages());
			request.setAttribute("filtro", filtro);			
		} else {
			request.setAttribute("lista", new ArrayList());
			request.setAttribute("currentPage", 0);
			request.setAttribute("numberOfPages", 0);
			request.setAttribute("filtro", filtro);
		}
	}
	
	public ModelAndView gerar(WebRequestContext request, SpedpiscofinsFiltro filtro){
		try{
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			listaEmpresa.add(empresaService.carregaEmpresa(filtro.getEmpresa()));
			if(filtro.getBuscardadosfilias() != null && filtro.getBuscardadosfilias()){
				filtro.setListaFiliais(empresaService.findFiliais(filtro.getEmpresa()));
				if(filtro.getListaFiliais() != null && !filtro.getListaFiliais().isEmpty())
					listaEmpresa.addAll(filtro.getListaFiliais());
			}
			filtro.setListaEmpresaFiliais(listaEmpresa);
			
			return new ResourceModelAndView(spedpiscofinsService.gerarArquivo(filtro, request));
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return sendRedirectToAction("listagem");	
		}
		 
	}
}
