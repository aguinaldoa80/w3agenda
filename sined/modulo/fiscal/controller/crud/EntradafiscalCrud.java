package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Ajustefiscal;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.EntregadocumentoColeta;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.geral.bean.Entregadocumentofreterateio;
import br.com.linkcom.sined.geral.bean.Entregadocumentohistorico;
import br.com.linkcom.sined.geral.bean.Entregadocumentoreferenciado;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialdevolucao;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.service.AjustefiscalService;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.ColaboradorFornecedorService;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.EntregaDocumentoApropriacaoService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.EntregadocumentoColetaService;
import br.com.linkcom.sined.geral.service.EntregadocumentofreteService;
import br.com.linkcom.sined.geral.service.EntregadocumentofreterateioService;
import br.com.linkcom.sined.geral.service.EntregadocumentohistoricoService;
import br.com.linkcom.sined.geral.service.EntregamaterialService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.FornecimentoService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.ManifestoDfeService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialdevolucaoService;
import br.com.linkcom.sined.geral.service.MaterialfornecedorService;
import br.com.linkcom.sined.geral.service.ModelodocumentofiscalService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NcmcapituloService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.PedidovendamaterialService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.MaterialgradeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeAssociadaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeAtualizacaoMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.EntradafiscalFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/Entradafiscal", authorizationModule=CrudAuthorizationModule.class)
public class EntradafiscalCrud extends CrudControllerSined<EntradafiscalFiltro, Entregadocumento, Entregadocumento>{
	
	private RateioService rateioService;
	private EntradafiscalService entradafiscalService;
	private MaterialService materialService;
	private CfopService cfopService;
	private EntregaService entregaService;
	private ProdutoService produtoService;
	private EntregadocumentohistoricoService entregadocumentohistoricoService;
	private MaterialdevolucaoService materialdevolucaoService;
	private FornecimentoService fornecimentoService;
	private LocalarmazenagemService localarmazenagemService;
	private DocumentoService documentoService;
	private EmpresaService empresaService;
	private ContagerencialService contagerencialService;
	private ContapagarService contapagarService;
	private DocumentohistoricoService documentohistoricoService;
	private FornecedorService fornecedorService;
	private PessoaService pessoaService;
	private MaterialfornecedorService materialfornecedorService;
	private UnidademedidaService unidademedidaService;
	private NcmcapituloService ncmcapituloService;
	private DocumentoorigemService documentoorigemService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private EntregadocumentofreteService entregadocumentofreteService;
	private EntregamaterialService entregamaterialService;
	private EntregadocumentofreterateioService entregadocumentofreterateioService;
	private EntregadocumentoColetaService entregadocumentoColetaService;
	private ColetaService coletaService;
	private ColaboradorFornecedorService colaboradorFornecedorService;
	private ModelodocumentofiscalService modelodocumentofiscalService;
	private AjustefiscalService ajustefiscalService;
	private PedidovendamaterialService pedidovendamaterialService;
	private PedidovendaService pedidovendaService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private ParametrogeralService parametrogeralService;
	private PrazopagamentoService prazopagamentoService;
	private DocumentotipoService documentotipoService;
	private ArquivoService arquivoService;
	private ManifestoDfeService manifestoDfeService;
	private EntregaDocumentoApropriacaoService entregaDocumentoApropriacaoService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private LoteestoqueService loteestoqueService;
	
	public void setModelodocumentofiscalService(ModelodocumentofiscalService modelodocumentofiscalService) {this.modelodocumentofiscalService = modelodocumentofiscalService;}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService){this.pedidovendahistoricoService = pedidovendahistoricoService;	}
	public void setEmpresaService(EmpresaService empresaService){this.empresaService = empresaService;	}
	public void setMaterialdevolucaoService(MaterialdevolucaoService materialdevolucaoService){this.materialdevolucaoService = materialdevolucaoService;	}
	public void setRateioService(RateioService rateioService){this.rateioService = rateioService;	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService){this.entradafiscalService = entradafiscalService;	}
	public void setMaterialService(MaterialService materialService){this.materialService = materialService;	}
	public void setCfopService(CfopService cfopService){this.cfopService = cfopService;	}	
	public void setEntregaService(EntregaService entregaService){this.entregaService = entregaService;	}	
	public void setProdutoService(ProdutoService produtoService){this.produtoService = produtoService;	}
	public void setEntregadocumentohistoricoService(EntregadocumentohistoricoService entregadocumentohistoricoService){this.entregadocumentohistoricoService = entregadocumentohistoricoService;}
	public void setFornecimentoService(FornecimentoService fornecimentoService){this.fornecimentoService = fornecimentoService;	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService){this.localarmazenagemService = localarmazenagemService;	}
	public void setDocumentoService(DocumentoService documentoService){this.documentoService = documentoService;	}
	public void setContagerencialService(ContagerencialService contagerencialService){this.contagerencialService = contagerencialService;	}
	public void setContapagarService(ContapagarService contapagarService){this.contapagarService = contapagarService;	}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService){this.documentohistoricoService = documentohistoricoService;	}
	public void setFornecedorService(FornecedorService fornecedorService){this.fornecedorService = fornecedorService;	}
	public void setPessoaService(PessoaService pessoaService){this.pessoaService = pessoaService;}
	public void setMaterialfornecedorService(MaterialfornecedorService materialfornecedorService){this.materialfornecedorService = materialfornecedorService;	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService){this.unidademedidaService = unidademedidaService;	}
	public void setNcmcapituloService(NcmcapituloService ncmcapituloService){this.ncmcapituloService = ncmcapituloService;	}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService){this.documentoorigemService = documentoorigemService;	}
	public void setEntregadocumentofreteService(EntregadocumentofreteService entregadocumentofreteService){this.entregadocumentofreteService = entregadocumentofreteService;	}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService){this.entregamaterialService = entregamaterialService;	}
	public void setEntregadocumentofreterateioService(EntregadocumentofreterateioService entregadocumentofreterateioService) {		this.entregadocumentofreterateioService = entregadocumentofreterateioService;	}
	public void setEntregadocumentoColetaService(EntregadocumentoColetaService entregadocumentoColetaService) {this.entregadocumentoColetaService = entregadocumentoColetaService;	}
	public void setColetaService(ColetaService coletaService){this.coletaService = coletaService;}
	public void setColaboradorFornecedorService(ColaboradorFornecedorService colaboradorFornecedorService){this.colaboradorFornecedorService = colaboradorFornecedorService;	}
	public void setAjustefiscalService(AjustefiscalService ajustefiscalService) {this.ajustefiscalService = ajustefiscalService;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setManifestoDfeService(ManifestoDfeService manifestoDfeService) {this.manifestoDfeService = manifestoDfeService;}
	public void setEntregaDocumentoApropriacaoService(EntregaDocumentoApropriacaoService entregaDocumentoApropriacaoService) {this.entregaDocumentoApropriacaoService = entregaDocumentoApropriacaoService;}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {this.unidademedidaconversaoService = unidademedidaconversaoService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	
	@Override
	protected Entregadocumento criar(WebRequestContext request, Entregadocumento form) throws Exception {
		Entregadocumento bean = super.criar(request, form);
		List<Materialdevolucao> listaDevolucao = null;
		String paramWhereInDevolucao = request.getParameter("whereInDevolucoes");
		if(paramWhereInDevolucao != null && !paramWhereInDevolucao.equals("")){
			listaDevolucao = materialdevolucaoService.findForEntrega(paramWhereInDevolucao);
			bean = materialdevolucaoService.preecheEntregadocumento(listaDevolucao);
			bean.setWhereInDevolucoes(paramWhereInDevolucao);
		}
		String fromRegistrarentradafiscal = request.getParameter("fromRegistrarentradafiscal");
		String whereIn = request.getParameter("cddocumento");

		if(fromRegistrarentradafiscal != null && !fromRegistrarentradafiscal.equals("") && "true".equals(fromRegistrarentradafiscal) && 
				whereIn != null && !"".equals(whereIn)){
			List<Documento> listaDocumento = contapagarService.findContas(whereIn);
			bean = entradafiscalService.criaEntradafiscalByDocumento(listaDocumento);
			bean.setIsFaturar("TRUE");
			request.setAttribute("fromRegistrarentradafiscal", true);
			request.setAttribute("registrarentradafiscalcddocumento", whereIn);
		}
		Boolean recebimentoEntradaDataCorrente = parametrogeralService.getBoolean(Parametrogeral.RECEBIMENTO_ENTRADA_DATA_CORRENTE);
		request.setAttribute("disableDtentrada", recebimentoEntradaDataCorrente);
		if(recebimentoEntradaDataCorrente){
			bean.setDtentrada(SinedDateUtils.currentDate());
		}
		
		return bean;
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Entregadocumento form) throws CrudException {
		String idFornecimento = request.getParameter("idFornecimento");
		if(idFornecimento != null && !idFornecimento.equals("")){
			Fornecimento fornecimento = fornecimentoService.findForRegistrarEntradafiscal(new Fornecimento(Integer.parseInt(idFornecimento)));
			if(fornecimento != null && fornecimento.getCdentregadocumento() != null){
				if(entradafiscalService.existEntradafiscalNotCancelada(fornecimento.getCdentregadocumento())){
					request.addError("Existe uma Entrada Fiscal j� registrada para o fornecimento selecionado.");
					return new ModelAndView("redirect:/suprimento/crud/Fornecimento");
				}
			}
		}
		return super.doCriar(request, form);
	}
	
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, EntradafiscalFiltro filtro) {
		return new ModelAndView("crud/entradafiscalListagem");
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Entregadocumento form) {
		return new ModelAndView("crud/entradafiscalEntrada");
	}

	public ModelAndView criarEntradafiscalDoRecebimento(WebRequestContext request, Entregadocumento form) throws CrudException {
		if("true".equals(request.getParameter("forcarConsulta"))){
			request.setAttribute(CONSULTAR, true);
		}
		try {
			if(Boolean.parseBoolean(request.getParameter("fromrecebimento"))) {
				form = entradafiscalService.carregarEntregadocumentoFromRecebimento(request);
				if(request.getParameter("existeErrosEntradaFiscaoFromRecebimento") !=null && Boolean.parseBoolean(request.getParameter("existeErrosEntradaFiscaoFromRecebimento"))){
					validateBean(form, request.getBindException());
					this.validateBeanCamposObrigatorios(form, request.getBindException());
				}
			}
			setInfoForTemplate(request, form);
			setEntradaDefaultInfo(request, form);
			entrada(request, form);
		} catch (Exception e) {
			throw new CrudException(ENTRADA, e);
		}
		return getEntradaModelAndView(request, form);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, EntradafiscalFiltro filtro) throws CrudException {
		filtro.setPopupCTRC("true".equalsIgnoreCase(request.getParameter("popupCTRC")));
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request,	EntradafiscalFiltro filtro) throws Exception {
		if(filtro.getCfop() != null && filtro.getCfop().getCdcfop() != null){
			filtro.setCfop(cfopService.load(filtro.getCfop(), "cfop.cdcfop, cfop.codigo, cfop.descricaoresumida"));
		}
		List<Entregadocumentosituacao> listaSituacao = new ArrayList<Entregadocumentosituacao>();
		listaSituacao.add(Entregadocumentosituacao.REGISTRADA);
		listaSituacao.add(Entregadocumentosituacao.FATURADA);
		listaSituacao.add(Entregadocumentosituacao.CANCELADA);
	
		request.setAttribute("listaSituacao", listaSituacao);
		request.setAttribute("listaDocumentotipo", documentotipoService.findForCompra(null));
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Entregadocumento bean) {
		if(bean.getProducaoautomatica() != null && 
				bean.getProducaoautomatica() &&
				bean.getWhereInPedido() != null &&
				!"".equals(bean.getWhereInPedido()) &&
				bean.getWhereInPedido().split(",").length == 1){
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda?ACAO=abrirGerarProducaoConferencia&selectedItens=" + bean.getWhereInPedido());
		} else if(Boolean.TRUE.equals(bean.getFromRecebimento())){
			return getModelAndViewOrigemEntrega(request, bean.getIdentificadortelaRecebimento(), "editar", bean.getCdentrega());
		} else return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Entregadocumento form) throws Exception {
		boolean fromRecebimento = Boolean.parseBoolean(request.getParameter("fromrecebimento")) || Boolean.TRUE.equals(form.getFromRecebimento());
		String acao = request.getParameter(CrudController.ACTION_PARAMETER);
		
		if(!fromRecebimento || (fromRecebimento && acao != null && !acao.equals(CRIAR))) {
			entradafiscalService.carregarEntregadocumento(request, form, fromRecebimento);
			if(!Boolean.TRUE.equals(form.getFromRecebimento())){
				form.setFromRecebimento(fromRecebimento);
			}
			
			if(fromRecebimento){
				String cdentrega = request.getParameter("cdentrega");
				if(SinedUtil.validaNumeros(cdentrega)){
					form.setCdentrega(cdentrega);
				}
			}
		}
	}
	
	@Command(validate=false)
	public ModelAndView calculaRateio(WebRequestContext request, Entregadocumento bean) throws CrudException{
		Entregamaterial entregamaterial = null;
		String identificadortela = bean.getIdentificadortela();
		
		if(StringUtils.isNotBlank(identificadortela)){
			Object attr = request.getSession().getAttribute("listaEntregamaterial" + request.getParameter("identificadortela"));
			if(attr != null) {
				bean.setListaEntregamaterial((Set<Entregamaterial>) attr);
			}
		} 
		
		entradafiscalService.addInfAtributo(request, bean, entregamaterial);
		
		if(bean.getListaEntregamaterial() != null && bean.getListaEntregamaterial().size() > 0)
			entradafiscalService.calculaRateio(bean);
				
		return continueOnAction("entrada", bean);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void validateBean(Entregadocumento bean, BindException errors) {
		try {
			if(bean.getSimplesremessa() == null || !bean.getSimplesremessa()){
				rateioService.validateRateio(bean.getRateio(), bean.getValor());
			}
		} catch (SinedException e) {
			errors.reject("001",e.getMessage());
		}
		
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaEntregamaterial" + bean.getIdentificadortela());
		if (attribute != null) {
			bean.setListaEntregamaterial((Set<Entregamaterial>) attribute);
		}
		
		if(bean.getListaEntregamaterial() != null && !bean.getListaEntregamaterial().isEmpty()){
			String msgErroCfopescopo = "";
			String nomeMaterial;
			for(Entregamaterial em : bean.getListaEntregamaterial()){
				if(Util.objects.isPersistent(em.getLoteestoque())){
					materialService.validateLoteSemValidade(em, errors);
				}else if(SinedUtil.isListNotEmpty(em.getListaEntregamateriallote())){
					for(Entregamateriallote eml: em.getListaEntregamateriallote()){
						Entregamaterial aux = new Entregamaterial();
						aux.setMaterial(em.getMaterial());
						aux.setLoteestoque(eml.getLoteestoque());
						materialService.validateLoteSemValidade(aux, errors);
					}
				}
				if(em.getCfop() != null && em.getCfop().getCdcfop() != null){
						em.setCfop(cfopService.carregarCfop(em.getCfop()));
						if(em.getCfop().getCodigo() != null){
//							if("5".equals(em.getCfop().getCodigo().subSequence(0, 1)) || "6".equals(em.getCfop().getCodigo().subSequence(0, 1)) || 
//									"7".equals(em.getCfop().getCodigo().subSequence(0, 1))){
//								errors.reject("001","CFOP informado deve ser do tipo Entrada. " + (em.getMaterial() != null ? "Material " + materialService.load(em.getMaterial(), "material.nome").getNome() : "") + "\n");
//							}
						}
						msgErroCfopescopo = cfopService.verificaCfopescopo(bean.getFornecedor(), bean.getEmpresa(), em.getCfop());
						if(msgErroCfopescopo != null && !"".equals(msgErroCfopescopo)){
							errors.reject("001",msgErroCfopescopo + (em.getMaterial() != null ? " Material " + materialService.load(em.getMaterial(), "material.nome").getNome() : "") + "\n");
						}
				}	
				if(SinedUtil.isRateioMovimentacaoEstoque() && bean.getEntrega() != null && bean.getEntrega().getCdentrega() != null && em.getCentrocusto() == null){
					nomeMaterial = null;
					if(em.getMaterial() != null){
						if(em.getMaterial().getNome() != null){
							nomeMaterial = em.getMaterial().getNome();
						}else {
							nomeMaterial = materialService.loadSined(em.getMaterial(), "material.nome").getNome();
						}
					}
					errors.reject("001", "O campo Centro de custo � obrigat�rio. " + (nomeMaterial != null ? " Material " +  nomeMaterial : "") + "\n");
				}
			}
		}
		if(entregaService.validaFornecedornumero(null, bean)){
			errors.reject("001", "Existe fornecedor e n�mero j� cadastrado no sistema.");
		}
		if (!entradafiscalService.validaPagamentoAntecipado(bean)){
			errors.reject("001","O valor da parcela deve ser menor ou igual ao saldo do pagamento adiantado escolhido.");
		}
		
		if(bean.getListaEntregadocumentofrete() != null && bean.getListaEntregadocumentofrete().size() > 0){
			Money valorDocumento = bean.getValor();
			Money valorRateio = new Money(0);
			if (bean.getListaEntregadocumentofreterateio()!=null && !bean.getListaEntregadocumentofreterateio().isEmpty()){
				for (Entregadocumentofreterateio entregadocumentofreterateio: bean.getListaEntregadocumentofreterateio()){
					valorRateio = valorRateio.add(entregadocumentofreterateio.getValor());
				}
			}
			if (valorDocumento.toLong()!=valorRateio.toLong()){
				errors.reject("001", "O valor do rateio n�o pode diferente do valor total da nota.");
			}
		}
			
		if(bean.getFornecedor() != null){
			try {
				colaboradorFornecedorService.validaPermissao(bean.getFornecedor());
			} catch (SinedException e) {
				errors.reject("001",e.getMessage());
			}
		}
		
		super.validateBean(bean, errors);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request,	Entregadocumento form) throws CrudException {
		if(form.getIdFornecimento() != null && !"".equals(form.getIdFornecimento())){
			String idFornecimento = form.getIdFornecimento();
			super.doSalvar(request, form);
			fornecimentoService.updateCdentregadocumento(idFornecimento, form.getCdentregadocumento());
			request.clearMessages();
			request.addMessage("Entrada Fiscal registrada com sucesso.");
			return new ModelAndView("redirect:/suprimento/crud/Fornecimento?ACAO=listagem");
		}
		if(form.getIdDocumentoRegistrarEntrada() != null && !"".equals(form.getIdDocumentoRegistrarEntrada())){
			String idDocumento = form.getIdDocumentoRegistrarEntrada();
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Entrada Fiscal registrada com sucesso.");
			return new ModelAndView("redirect:/financeiro/crud/Contapagar?ACAO=consultar&cddocumento=" + idDocumento);
		}
		return super.doSalvar(request, form);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void salvar(WebRequestContext request, Entregadocumento bean)	throws Exception {
		Boolean isCriar = bean.getCdentregadocumento() != null ? Boolean.FALSE : Boolean.TRUE;
		
		Object attribute = request.getSession().getAttribute("listaEntregamaterial" + bean.getIdentificadortela());
		if (attribute != null) {
			bean.setListaEntregamaterial((Set<Entregamaterial>) attribute);
		}
		
//		Rateio rateioOriginal = null;
//		if(!isCriar && bean.getRateio() != null && bean.getRateio().getCdrateio() != null){
//			rateioOriginal = rateioService.findRateio(bean.getRateio());
//		}
		String whereInPedidovenda = bean.getWhereInPedido();
		String whereInColeta = bean.getWhereInColeta();
		if(whereInPedidovenda != null && !whereInPedidovenda.isEmpty()){
			List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPedidovenda);
			if(listaColeta != null && !listaColeta.isEmpty()){
				whereInColeta = SinedUtil.listAndConcatenateIDs(listaColeta);
				bean.setWhereInColeta(whereInColeta);
			}
		} else if(whereInColeta != null && !whereInColeta.isEmpty()){
			List<Coleta> listaColeta = coletaService.loadForGerarNotafiscalproduto(whereInColeta);
			if(listaColeta != null && !listaColeta.isEmpty()){
				whereInPedidovenda = SinedUtil.listAndConcatenate(listaColeta, "pedidovenda.cdpedidovenda", ",");
				bean.setWhereInPedido(whereInPedidovenda);
			}
		}
		
		Set<Entregadocumentohistorico> listaHistorico = new ListSet<Entregadocumentohistorico>(Entregadocumentohistorico.class);
		if(isCriar){
			bean.setEntregadocumentosituacao(Entregadocumentosituacao.REGISTRADA);			
			Entregadocumentohistorico edh = entradafiscalService.criaEntregadocumentohistorico(bean, Entregadocumentosituacao.REGISTRADA);
			if(edh != null){	
				listaHistorico.add(edh);
			}	
		} else{
			List<Entregadocumentohistorico> listaEdh = entregadocumentohistoricoService.findByEntregadocumento(bean);
			Entregadocumentohistorico edh = entradafiscalService.criaEntregadocumentohistorico(bean, Entregadocumentosituacao.ALTERADA);
			listaEdh.add(edh);
			listaHistorico = entregadocumentohistoricoService.getLista(listaEdh);
		}
		bean.setListaEntregadocumentohistorico(listaHistorico);
		
		// Adicionar a lista transiente, caso exista, � lista original para ser salva.
		if(bean.getListaEntregadocumentoreferenciadoTrans() != null){
			if(bean.getListaEntregadocumentoreferenciado() == null){
				bean.setListaEntregadocumentoreferenciado(bean.getListaEntregadocumentoreferenciadoTrans());
			} else{
				bean.getListaEntregadocumentoreferenciado().addAll(bean.getListaEntregadocumentoreferenciadoTrans());				
			}
		}
		
		Set<Ajustefiscal> listaAjusteFiscal = null;
		if(bean.getCdentregadocumento() != null){
			listaAjusteFiscal = SinedUtil.listToSet(ajustefiscalService.buscarItemComAjusteFiscal(bean), Ajustefiscal.class);  
		}
		try {
			if(bean.getIsFaturar() !=null && bean.getIsFaturar().equals("TRUE")){
				bean.setIsFaturar(null);
				bean.setEntregadocumentosituacao(Entregadocumentosituacao.FATURADA);
			}
			validaFechamentoFinanceiro(bean);
			super.salvar(request, bean);
			entradafiscalService.salvarVinculo(bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_ENTREGADOCUMENTO_CDFORNECEDORNUMEROTITULO")) {
				throw new SinedException("Fornecedor e N�mero do t�tulo j� cadastrado no sistema.");
			} else
				e.printStackTrace();		
		}
		
		entradafiscalService.verificaExclusaoAjusteFiscal(listaAjusteFiscal, bean);
		
		if(whereInPedidovenda != null && !whereInPedidovenda.equals("")){
			String[] idsPedidovenda = whereInPedidovenda.split(",");
			for (String idPedidovenda : idsPedidovenda) {
				Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
				pedidovendahistorico.setAcao("Entrada fiscal criada");
				pedidovendahistorico.setObservacao("Entrada fiscal de coleta criada: <a href=\"javascript:visualizarEntradafiscal(" + bean.getCdentregadocumento() + ")\">" + bean.getCdentregadocumento() + "</a>");
				pedidovendahistorico.setPedidovenda(new Pedidovenda(Integer.parseInt(idPedidovenda)));
				
				pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
			}
		}
		
		if(whereInColeta != null && !whereInColeta.isEmpty()){
			String[] idsColeta = whereInColeta.split(",");
			for (String idColeta : idsColeta) {
				EntregadocumentoColeta edc = new EntregadocumentoColeta(Integer.parseInt(idColeta), bean.getCdentregadocumento());
				entregadocumentoColetaService.saveOrUpdate(edc);
			}
		}
		
		if(bean.getEntrega() != null){
			Localarmazenagem localprincipal = null;
			Entrega entrega = entregaService.load(bean.getEntrega(), "entrega.cdentrega, entrega.romaneio, entrega.localarmazenagem");
			if(entrega != null && entrega.getLocalarmazenagem() != null){
				localprincipal = entrega.getLocalarmazenagem();
			}
			
			if(entrega.getRomaneio() == null || !entrega.getRomaneio()){
				if(entregaService.verificaEntregadocumentoRomaneio(bean, localprincipal)){
					entregaService.updateRomaneio(bean.getEntrega(), Boolean.FALSE);
				} else {
					entregaService.updateRomaneio(bean.getEntrega(), null);
				}
				entregaService.callProcedureAtualizaEntrega(bean.getEntrega());
			}
		}
		
		if(bean.getIdDocumentoRegistrarEntrada() != null && !"".equals(bean.getIdDocumentoRegistrarEntrada())){
			if(bean.getListadocumento() != null && !bean.getListadocumento().isEmpty()){
				Documentohistorico documentohistorico;
				for(Entregapagamento entregapagamento : bean.getListadocumento()){
					if(entregapagamento.getDocumentoorigem() != null && entregapagamento.getDocumentoorigem().getCddocumento() != null){
						documentohistorico = new Documentohistorico(entregapagamento.getDocumentoorigem());
						documentohistorico.setDocumento(entregapagamento.getDocumentoorigem());
						documentohistorico.setObservacao("Registro de entrada fiscal: <a href=\"/w3erp/fiscal/crud/Entradafiscal?ACAO=consultar&cdentregadocumento=" + bean.getCdentregadocumento() +
								"\" >" + bean.getCdentregadocumento() + "</a>");
						documentohistoricoService.saveOrUpdate(documentohistorico);
					}
				}
			}
		}
		
		String paramWhereInDevolucao = bean.getWhereInDevolucoes();
		if(paramWhereInDevolucao != null && !paramWhereInDevolucao.equals("")){			
			List<Materialdevolucao> listaDevolucao = materialdevolucaoService.findForEntrega(paramWhereInDevolucao);
			for (Materialdevolucao materialdevolucao : listaDevolucao) {
				materialdevolucao.setEntregadocumento(bean);
				materialdevolucaoService.saveOrUpdateNoUseTransaction(materialdevolucao);
			}
		}
		// Salva os documentos referenciados na entregadocumento
		entradafiscalService.saveDocumentosReferenciados(request, bean);
		
//		if(rateioOriginal != null && rateioService.existAlteracaorateio(rateioOriginal, bean.getRateio())){
//			request.setAttribute("existAlteracaoRateio", "true");
//		}
		
		entradafiscalService.verificarMaterialLote(bean);
		
		try {
			entradafiscalService.calcularCustoEntradaMaterialByEntradafiscal(bean);
		} catch (Exception e) {
			request.addError("Erro ao calcular o valor da entrada no estoque: " + e.getMessage());
		}
		
		try{
			materialService.calculaValorvendaByEntregadocumento(bean);
		} catch (Exception e) {
			request.addError("Erro ao calcular o valor de venda dos materiais: " + e.getMessage());
		}
		
		request.getSession().removeAttribute("listaItensNotaFiscalProduto" + bean.getIdentificadortela());
		request.getSession().removeAttribute("listaEntregamaterial" + bean.getIdentificadortela());
	}
	
	public void ajaxAtualizaRateioObjetosRelacionados(WebRequestContext request){
		String id = request.getParameter("cdentregadocumento");
		Boolean sucesso = false; 
		if(id != null && !"".equals(id)){
			
			Entregadocumento ed = entradafiscalService.findRateioByEntregadocumento(new Entregadocumento(Integer.parseInt(id)));
			
			if(ed != null && ed.getRateio() != null){
				entradafiscalService.atualizaRateioReferencias(ed);
				sucesso = true;
			}
		}
		
		View.getCurrent().println("sucesso = '" + sucesso + "';");
	}
	
	/**
	 * M�todo ajax que informa se tem que exibir o campo comprimento
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxInfMaterialComprimento(WebRequestContext request){
		Boolean showcomprimento = false;
		String cdmaterialstr = request.getParameter("cdmaterial") != null && !"".equals(request.getParameter("cdmaterial")) ? 
								request.getParameter("cdmaterial") : null;
		String index = request.getParameter("index") != null && !"".equals(request.getParameter("index")) ? 
				request.getParameter("index") : null;
								
		if(cdmaterialstr != null && !"".equals(cdmaterialstr)){
			Produto produto = produtoService.carregaProduto(new Material(Integer.parseInt(cdmaterialstr)));
			if(produto != null){
				if(produto.getComprimento() != null || produto.getLargura() != null || produto.getAltura() != null){
					showcomprimento = Boolean.TRUE;
				}
			}
		}
		
		View.getCurrent()
			.println("var showcomprimento = '" + showcomprimento + "';")
			.println("var index = '" + index +"';");		
	}
	
	/**
	 * M�todo para gerar CSV da Entrada Fiscal
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarCsv(WebRequestContext request, EntradafiscalFiltro filtro){
		
		List<Entregadocumento> listaId = entradafiscalService.findForCsv(filtro);
		StringBuilder rel = new StringBuilder();
		
		String whereIn = CollectionsUtil.listAndConcatenate(listaId, "cdentregadocumento", ",");
		List<Entregadocumento> lista = entradafiscalService.loadWithLista(whereIn, "", false);
		
		rel.append("Empresa;");
		rel.append("Fornecedor;");
		rel.append("Data de Entrada;");
		rel.append("Data de Emiss�o;");
		rel.append("N�mero;");
		rel.append("S�rie;");
		rel.append("Situa��o;");
		rel.append("Modelo;");
		rel.append("Valor;");
		rel.append("Outras despesas;");
		rel.append("Desconto;");
		rel.append("Valor seguro;");
		rel.append("Frete;");
		rel.append("Respons�vel pelo frete;");
		rel.append("Base de c�lculo ICMS;");
		rel.append("Valor ICMS;");
		rel.append("Base de c�lculo ICMS/ST;");
		rel.append("Valor ICMS/ST;");
		rel.append("Base de c�lculo IPI;");
		rel.append("Valor IPI;");
		rel.append("Base de c�lculo ISS;");
		rel.append("Valor ISS;");
		rel.append("Base de c�lculo PIS;");
		rel.append("Valor PIS;");
		rel.append("Base de c�lculo CONFINS;");
		rel.append("Valor COFINS;");
		rel.append("Tipo de t�tulo;");
		rel.append("Descri��o do t�tulo;");
		rel.append("N�mero do t�tulo;");
		rel.append("Conta gerencial;");
		rel.append("\n");
		
		for(Entregadocumento ed : lista){
			ed.criaTotaisimposto();
			
			if(ed.getEmpresa() != null) rel.append(ed.getEmpresa().getNomefantasia());
			rel.append(";");
			
			if(ed.getFornecedor() != null) rel.append(ed.getFornecedor().getNome());
			rel.append(";");
			
			if(ed.getDtentrada() != null) rel.append(SinedDateUtils.toString(ed.getDtentrada()));
			rel.append(";");
			
			if(ed.getDtemissao() != null) rel.append(SinedDateUtils.toString(ed.getDtemissao()));
			rel.append(";");
			
			if(ed.getNumero() != null) rel.append(ed.getNumero());
			rel.append(";");

			if(ed.getSerie() != null) rel.append(ed.getSerie());
			rel.append(";");
			
			if(ed.getSituacaodocumento() != null) rel.append(ed.getSituacaodocumento().getDesricao());
			rel.append(";");

			if(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getNome() != null) rel.append(ed.getModelodocumentofiscal().getNome());
			rel.append(";");
			
			if(ed.getValor() != null) rel.append(ed.getValor());
			rel.append(";");
			
//			if(ed.getValoroutrasdespesas() != null) rel.append(ed.getValoroutrasdespesas());
			rel.append(SinedUtil.zeroIfNull(ed.getValoroutrasdespesas()));
			rel.append(";");
			
			if(ed.getValordesconto() != null) rel.append(ed.getValordesconto());
			rel.append(";");
			
			if(ed.getValorseguro() != null) rel.append(ed.getValorseguro());
			rel.append(";");

			if(ed.getValorfrete() != null) rel.append(ed.getValorfrete());
			rel.append(";");
			
			if(ed.getResponsavelfrete() != null) rel.append(ed.getResponsavelfrete().getNome());
			rel.append(";");
			
			if(ed.getValortotalbcicms() != null) rel.append(ed.getValortotalbcicms());
			rel.append(";");
			
			if(ed.getValortotalicms() != null) rel.append(ed.getValortotalicms());
			rel.append(";");
			
			if(ed.getValortotalbcicmsst() != null) rel.append(ed.getValortotalbcicmsst());
			rel.append(";");
			
			if(ed.getValortotalicmsst() != null) rel.append(ed.getValortotalicmsst());
			rel.append(";");
			
			if(ed.getValortotalbcipi() != null) rel.append(ed.getValortotalbcipi());
			rel.append(";");
			
			if(ed.getValortotalipi() != null) rel.append(ed.getValortotalipi());
			rel.append(";");

//			if(ed.getValortotalbciss() != null) rel.append(ed.getValortotalbciss());
			rel.append(SinedUtil.zeroIfNull(ed.getValortotalbciss()));
			rel.append(";");
			
			if(ed.getValortotaliss() != null) rel.append(ed.getValortotaliss());
			rel.append(";");			
			
			if(ed.getValortotalbcpis() != null) rel.append(ed.getValortotalbcpis());
			rel.append(";");
			
			if(ed.getValortotalpis() != null) rel.append(ed.getValortotalpis());
			rel.append(";");
			
			if(ed.getValortotalbccofins() != null) rel.append(ed.getValortotalbccofins());
			rel.append(";");
			
			if(ed.getValortotalcofins() != null) rel.append(ed.getValortotalcofins());
			rel.append(";");
			
			if(ed.getTipotitulo() != null) rel.append(ed.getTipotitulo().getDescricao());
			rel.append(";");
			
			if(ed.getDescricaotitulo() != null) rel.append(ed.getDescricaotitulo());
			rel.append(";");
			
			if(ed.getNumerotitulo() != null) rel.append(ed.getNumerotitulo());
			rel.append(";");

			if (ed.getRateio()!=null && ed.getRateio().getListaRateioitem()!=null && !ed.getRateio().getListaRateioitem().isEmpty()){
				rel.append(CollectionsUtil.listAndConcatenate(ed.getRateio().getListaRateioitem(), "contagerencial.nome", ", "));
			}
			rel.append(";");
			
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","entradafiscal_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	/**
	 * M�todo ajax que busca os documentos de antecipa��o do fornecedor
	 *
	 * @param request
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxDocumentoAntecipacao(WebRequestContext request, Entregadocumento entregadocumento){
		if (entregadocumento == null || entregadocumento.getFornecedor() == null) {
			throw new SinedException("Fornecedor n�o pode ser nulo.");
		}
		
		List<Documento> listaDocumento = new ArrayList<Documento>();
		listaDocumento = documentoService.findForAntecipacaoEntrega(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
		
		return new JsonModelAndView().addObject("lista", listaDocumento);
	}
	
	/**
	 * M�todo para faturar a entrada fiscal
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView faturar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));
		}
		
		List<Documentoorigem> listadocumentoOrigem = documentoorigemService.getListaDocumentosDaEntradaFiscal(whereIn);
		if (listadocumentoOrigem != null && !listadocumentoOrigem.isEmpty()){
			request.addError("N�o � possivel faturar contas n�o canceladas vinculadas a recebimento.");
			return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));	    	
		}else if(entregaService.haveEntregaSituacao(whereIn, Situacaosuprimentos.EM_ABERTO)){
			request.addError("N�o � possivel faturar entrada fiscal vinculada a um recebimento 'Em Aberto'. O faturamento deve ser realizado pela tela de recebimento.");
			return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));
		}else if(entradafiscalService.haveEntradafiscalSituacao(whereIn, true, Entregadocumentosituacao.REGISTRADA)){
			request.addError("S� � poss�vel faturar entradas fiscais que estejam com status Registrada.");
			return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));
		}else if(documentoService.existeContaPagarEntradaFiscal(whereIn)){
			request.addError("N�o � poss�vel faturar entradas fiscais que j� tenham gerado contas a pagar.");
			return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));
		}
				
//		if(entradafiscalService.isOrigemDocumentoOrEntrega(whereIn)){
//			request.addError("S� � permitido faturar entrada fiscal avulsa.");
//			return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));
//		}
		
		List<Entregadocumento> listaEntregadocumento = entradafiscalService.findForFaturar(whereIn);
		
		HashMap<Fornecedor, List<Entregadocumento>> mapFornecedor = new HashMap<Fornecedor, List<Entregadocumento>>();
		if(SinedUtil.isListNotEmpty(listaEntregadocumento)){
			for(Entregadocumento entregadocumento : listaEntregadocumento){
				if(Boolean.TRUE.equals(entregadocumento.getSimplesremessa())){
					request.addError("N�o � poss�vel faturar entrada fiscal de simples remessa.");
					return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));
				}
			}
			
			List<Entregadocumento> listaFornecedor;
			for(Entregadocumento entregadocumento : listaEntregadocumento){
				listaFornecedor = mapFornecedor.get(entregadocumento.getFornecedor());
				if(listaFornecedor == null){
					listaFornecedor = new ArrayList<Entregadocumento>();
				}
				listaFornecedor.add(entregadocumento);
				mapFornecedor.put(entregadocumento.getFornecedor(), listaFornecedor);
			}
			boolean abrirTelaContaPagar = parametrogeralService.getBoolean(Parametrogeral.ABRIR_TELA_CONTAPAGAR_NA_ENTREGA);
			if(abrirTelaContaPagar && mapFornecedor.keySet() != null && mapFornecedor.keySet().size() > 1){
				request.addError("O sistema est� parametrizado para abrir a tela de conta a pagar ao faturar. Nesse caso n�o ser� poss�vel faturar entrada fiscal de mais de um fornecedor ao mesmo tempo.");
				return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));
			}
			for(Fornecedor fornecedor : mapFornecedor.keySet()){
				List<Entregadocumento> listaFaturar = mapFornecedor.get(fornecedor);
				HashMap<Boolean, Documento> mapaDocumentoCriado = entradafiscalService.faturar(listaFaturar, abrirTelaContaPagar);
				Boolean existsDocumentoreferenciado = mapaDocumentoCriado.containsKey(Boolean.TRUE);
				Documento documentoCriado = mapaDocumentoCriado.get(existsDocumentoreferenciado);
				
				if(documentoCriado != null){
					if(abrirTelaContaPagar){
						request.getSession().setAttribute("documentoCriadoByEntradafiscal", documentoCriado);
						return new ModelAndView("redirect:/financeiro/crud/Contapagar?ACAO=criaContaByEntradafiscal");
					}else{
						request.addMessage("Entrada fiscal faturada com sucesso. " + SinedUtil.listAndConcatenate(listaFaturar, "numero", ","));
					}
				}else{
					if(existsDocumentoreferenciado){
						request.addMessage("Entrada fiscal faturada com sucesso. " + SinedUtil.listAndConcatenate(listaFaturar, "numero", ","));
					}else{
						request.addError("N�o foi poss�vel faturar a entrada fiscal. Entrada fiscal j� faturada." + SinedUtil.listAndConcatenate(listaFaturar, "numero", ","));						
					}
				}
			}
			
		}
		
		return new ModelAndView("redirect:/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+whereIn : ""));
	}
	
	/**
	 * M�todo que abre a popup para importar nota. (xml ou chave de acesso)
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopUpEntradafiscal(WebRequestContext request){
		String criaEntradaColeta = request.getParameter("criaEntradaColeta");
		if("true".equalsIgnoreCase(criaEntradaColeta)){
			request.setAttribute("parametrosAdicionais", "&criaEntradaColeta=true&selectedItens="+request.getParameter("selectedItens"));
			request.setAttribute("criaEntradaColeta", "true");
		}
		return new ModelAndView("direct:/crud/popup/receberEntradafiscal");
	}
	
	/**
	 * M�todo que recebe a Entrada fiscal atrrav�s do xml
	 *
	 * @param request
	 * @param entregadocumento
	 * @author Luiz Fernando
	 */
	public void receberEntradafiscal(WebRequestContext request, Entregadocumento entregadocumento){
		request.getSession().removeAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL");
		request.getSession().removeAttribute("CHAVE_ACESSO");
		request.getSession().removeAttribute("HTML_CHAVE_ACESSO");
		
		Boolean criaEntradaDevolucao = Boolean.valueOf(request.getParameter("receberxmlchaveacesso") != null ? request.getParameter("receberxmlchaveacesso") : "false");
		Boolean criaEntradaColeta = Boolean.valueOf(request.getParameter("criaEntradaColeta") != null ? request.getParameter("criaEntradaColeta") : "false");
		Boolean criaEntradaColetaFromPV = Boolean.valueOf(request.getParameter("receberxmlcoleta") != null ? request.getParameter("receberxmlcoleta") : "false");
		String param = "";
		if(criaEntradaDevolucao){
			if(request.getSession().getAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL_DEVOLUCAO") != null){
				request.getSession().setAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL", request.getSession().getAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL_DEVOLUCAO"));
				request.getSession().removeAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL_DEVOLUCAO");
			}
		} else if(criaEntradaColeta){
			if(entregadocumento.getArquivoxmlnfe() != null){
				request.getSession().setAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL", entregadocumento.getArquivoxmlnfe());
			}
			if (entregadocumento.getChaveacesso() != null && !entregadocumento.getChaveacesso().equals("")){
				request.getSession().setAttribute("CHAVE_ACESSO", entregadocumento.getChaveacesso());
				request.getSession().setAttribute("HTML_CHAVE_ACESSO", entregadocumento.getHtmlChaveacesso());
			}
			String whereInColeta = SinedUtil.getItensSelecionados(request);
			if(whereInColeta != null && !whereInColeta.equals(""))
				param += "&criaEntradaColeta=true&whereInColeta=" + whereInColeta;
		} else if(criaEntradaColetaFromPV){
			if(request.getSession().getAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL_COLETA") != null ||
					request.getSession().getAttribute("CHAVE_ACESSO_RECEBER_ENTRADAFISCAL_COLETA") != null){
				
				request.getSession().setAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL", request.getSession().getAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL_COLETA"));
				request.getSession().setAttribute("CHAVE_ACESSO", request.getSession().getAttribute("CHAVE_ACESSO_RECEBER_ENTRADAFISCAL_COLETA"));
				request.getSession().setAttribute("HTML_CHAVE_ACESSO", request.getSession().getAttribute("HTML_CHAVE_ACESSO_RECEBER_ENTRADAFISCAL_COLETA"));
				
				request.getSession().removeAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL_COLETA");
				request.getSession().removeAttribute("CHAVE_ACESSO_RECEBER_ENTRADAFISCAL_COLETA");
				request.getSession().removeAttribute("HTML_CHAVE_ACESSO_RECEBER_ENTRADAFISCAL_COLETA");
				
				String whereInPedidovenda = request.getParameter("whereInPedidovenda");
				param += "&receberxmlcoleta=true&fromPedidoColeta=true";
				if(whereInPedidovenda != null && !whereInPedidovenda.equals("")) param += "&whereInPedidovenda=" + whereInPedidovenda;
			}
		} else {
			if(entregadocumento.getArquivoxmlnfe() != null){
				request.getSession().setAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL", entregadocumento.getArquivoxmlnfe());
			}
			if (StringUtils.isNotEmpty(entregadocumento.getChaveacesso())) {
				request.getSession().setAttribute("CHAVE_ACESSO", entregadocumento.getChaveacesso());
				request.getSession().setAttribute("HTML_CHAVE_ACESSO", entregadocumento.getHtmlChaveacesso());
			}
		}
		
		param += "&producaoautomatica=" + "true".equals(request.getParameter("producaoautomatica"));
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>parent.location = '"+request.getServletRequest().getContextPath()+"/fiscal/crud/Entradafiscal?ACAO=conferenciaDados" 
								+ param + "';" +
				"parent.$.akModalRemove(true);</script>");
		
		if(request.getParameter("whereInDevolucoes")!= null && !request.getParameter("whereInDevolucoes").equals("")){			
			request.getSession().removeAttribute("InDevolucoes");
			request.getSession().setAttribute("InDevolucoes", request.getParameter("whereInDevolucoes"));
		}
	}
	
	public ModelAndView ajaxValidacaoChaveAcesso(WebRequestContext request, Entregadocumento entregadocumento) {
		return entregaService.ajaxValidacaoChaveAcesso(request, entregadocumento.getChaveacesso());
	}
	
	public ModelAndView conferenciaDados(WebRequestContext request, Entregadocumento entregadocumento) throws JDOMException, IOException, ParseException{
		request.getSession().removeAttribute("LISTA_ASSOCIADO_RECEBER_ENTRADAFISCAL");
		request.getSession().removeAttribute("IMPORTADOR_BEAN_RECEBER_ENTRADAFISCAL");
		request.getSession().removeAttribute("LISTA_ASSOCIADO_PEDIDOVENDAMATERIAL_RECEBER_ENTREGA");
		
		Boolean criaEntradaColeta = Boolean.valueOf(request.getParameter("criaEntradaColeta") != null ? request.getParameter("criaEntradaColeta") : "false");
		if(criaEntradaColeta){
			entregadocumento.setWhereInColeta(request.getParameter("whereInColeta"));
			entregadocumento.setFromColeta(Boolean.TRUE);
		}
		Boolean fromPedidoColeta = Boolean.valueOf(request.getParameter("fromPedidoColeta") != null ? request.getParameter("fromPedidoColeta") : "false");
		if(fromPedidoColeta){
			entregadocumento.setWhereInPedido(request.getParameter("whereInPedidovenda"));
			entregadocumento.setFromPedidoColeta(Boolean.TRUE);
			if(StringUtils.isNotEmpty(entregadocumento.getWhereInPedido())){
				entregadocumento.setListaPedidovendamaterial(pedidovendamaterialService.findForConferenciaByPedidovenda(entregadocumento.getWhereInPedido()));
			}
		}else if(criaEntradaColeta && StringUtils.isNotEmpty(entregadocumento.getWhereInColeta())){
			entregadocumento.setListaPedidovendamaterial(pedidovendamaterialService.findForConferenciaByColeta(entregadocumento.getWhereInColeta()));
		}
		entregadocumento.setProducaoautomatica("true".equals(request.getParameter("producaoautomatica")));
			
		Arquivo arquivo = null;
		Object object = request.getSession().getAttribute("ARQUIVO_XML_RECEBER_ENTRADAFISCAL");
		Object chaveacesso = request.getSession().getAttribute("CHAVE_ACESSO");
		Map<Integer, List<Unidademedida>> mapaUnidades = new HashMap<Integer, List<Unidademedida>>();
		
		if((object != null && object instanceof Arquivo) || (chaveacesso != null && !chaveacesso.equals(""))){
			ImportacaoXmlNfeBean bean = new ImportacaoXmlNfeBean();
			if (object != null && object instanceof Arquivo){
				arquivo = (Arquivo)object;
				try {
					bean = entregaService.getListaImportacao(arquivo);
				} catch (SinedException e) {
					request.addError("Arquivo XML inv�lido. (" + e.getMessage() + ")");
					SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal");
					return null;
				}
			}
			if (chaveacesso != null && StringUtils.isNotEmpty(chaveacesso.toString())) {
				try {
					ManifestoDfe manifestoDfe = manifestoDfeService.procurarPorChaveAcesso(chaveacesso.toString());
					Arquivo arquivoNfeCompletoXml = arquivoService.loadWithContents(manifestoDfe.getArquivoNfeCompletoXml());
					bean = entregaService.getListaImportacao(arquivoNfeCompletoXml);
					
					if (bean.getRazaosocial() == null && (bean.getListaItens() == null || bean.getListaItens().isEmpty())){
						throw new SinedException("N�o foi poss�vel encontrar os dados da entrega no site da receita. Verifique se a chave de acesso est� correta e digite novamente!");
					}
				} catch (Exception e) {
					request.addError(e.getMessage());
				}
				bean.setChaveacesso(chaveacesso.toString());
			}
			
			Fornecedor fornecedor = null;
			if(bean.getCnpj() != null && !bean.getCnpj().equals("")){
				fornecedor = fornecedorService.getFornecedorByCpfCnpj(bean.getCnpj(), null);
			} else if(bean.getCpf() != null && !bean.getCpf().equals("")){
				fornecedor = fornecedorService.getFornecedorByCpfCnpj(null, bean.getCpf());
			}
			if(fornecedor == null){
				Pessoa pessoa = null;
				if(bean.getCnpj() != null && !bean.getCnpj().equals("")){
					pessoa = pessoaService.findPessoaByCnpj(bean.getCnpj());
				} else if(bean.getCpf() != null && !bean.getCpf().equals("")){
					pessoa = pessoaService.findPessoaByCpf(bean.getCpf());
				}
				if (pessoa != null){						
					fornecedor = new Fornecedor();
					fornecedor.setCdpessoa(pessoa.getCdpessoa());
					fornecedorService.insertSomenteFornecedor(fornecedor);
				}
			}
			for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
				mapaUnidades.put(it.getSequencial(), new ArrayList<Unidademedida>());
			}
			if(fornecedor != null){
				for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
					String cprod = it.getCprod();
					if(cprod != null && !cprod.equals("")){
						List<Material> listaMaterial = materialService.findByFornecedorCodigo(fornecedor, cprod);
						if(listaMaterial != null && listaMaterial.size() == 1){
							Material material = listaMaterial.get(0);
							it.setMaterial(material);
							it.setNomeMaterial(material.getAutocompleteDescription());
							if(Util.strings.isNotEmpty(it.getUnidademedidacomercial())){
								List<Unidademedida> listaUnidade = unidademedidaService.getUnidademedidaByMaterial(material);
								if(SinedUtil.isListNotEmpty(listaUnidade)){
									for(Unidademedida un: listaUnidade){
										if(it.getUnidademedidacomercial().equalsIgnoreCase(un.getSimbolo())){
											it.setUnidademedidaAssociada(un);
											it.setUnidademedidaAssociada_simbolo(un.getSimbolo());
											break;
										}
									}
								}
								mapaUnidades.get(it.getSequencial()).addAll(listaUnidade);
							}
							if(SinedUtil.isListNotEmpty(it.getListaRastro())){
								try {
									entradafiscalService.preencheLotesW3(it.getListaRastro(), material, fornecedor);
								} catch (Exception e2) {
									request.addError(e2.getMessage());
									SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal");
									return null;
								}
								
								/*List<Loteestoque> listaLoteestoque = loteestoqueService.findByMaterialAndFornecedor(material, fornecedor);
								for(Rastro rastro: it.getListaRastro()){
									for(Loteestoque le: listaLoteestoque){
										if(rastro.getNumeroLote().equalsIgnoreCase(le.getNumeroLoteSoNumeroAndLetra())){
											if(le.getValidade() == null)
												throw new SinedException("A data de validade do(s) lote(s) do XML � diferente da data de validade do(s) registro(s) de lote(s).");
											if(le.getValidade().compareTo(rastro.getdVal()) != 0){
												throw new SinedException("A data de validade do(s) lote(s) do XML � diferente da data de validade do(s) registro(s) de lote(s).");
											}
										}
									}
									
									rastro.getListaLotesW3().addAll(listaLoteestoque);
								}*/
							}
							
							if(SinedUtil.isListNotEmpty(entregadocumento.getListaPedidovendamaterial())){
								Pedidovendamaterial itPVM = null;
								for(Pedidovendamaterial pedidovendamaterial : entregadocumento.getListaPedidovendamaterial()){
									if(pedidovendamaterial.getMaterial() != null && 
											pedidovendamaterial.getMaterial().equals(material)){
										if(itPVM == null){
											itPVM = pedidovendamaterial;
										}else {
											itPVM = null;
											break;
										}
									}
								}
								it.setPedidovendamaterial(itPVM);
							}
						}
					}
				}
			}
			
			request.setAttribute("achouFornecedor", fornecedor != null);
			bean.setConferenciaDadosFornecedorBean(entradafiscalService.setaDadosConferenciaFornecedor(fornecedor, bean));
			request.getSession().setAttribute("IMPORTADOR_BEAN_RECEBER_ENTRADAFISCAL", bean);
			
			List<Pedidovendamaterial> listaAssociadosPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
			for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
				if(it.getPedidovendamaterial() != null){
					listaAssociadosPedidovendamaterial.add(it.getPedidovendamaterial());
				}
			}
			request.getSession().setAttribute("LISTA_ASSOCIADO_PEDIDOVENDAMATERIAL_RECEBER_ENTREGA", listaAssociadosPedidovendamaterial);
			
			bean.setFromPedidoColeta(entregadocumento.getFromPedidoColeta());
			bean.setWhereInPedido(entregadocumento.getWhereInPedido());
			bean.setFromColeta(entregadocumento.getFromColeta());
			bean.setWhereInColeta(entregadocumento.getWhereInColeta());
			bean.setProducaoautomatica(entregadocumento.getProducaoautomatica());
			bean.setListaPedidovendamaterial(entregadocumento.getListaPedidovendamaterial());
			request.setAttribute("permissaoListagemMaterial", Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/suprimento/crud/Material", "listagem", SinedUtil.getUsuarioLogado()));
			request.setAttribute("mapaUnidades", mapaUnidades);
			return new ModelAndView("crud/popup/conferenciaInserirEntradafiscal", "bean", bean); 
		} else if(criaEntradaColeta){
			SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal?ACAO=criar&fromColeta="+entregadocumento.getFromColeta()+"&whereInColeta="+entregadocumento.getWhereInColeta());
			return null;
		} else{
			request.addError("Arquivo XML ou chave de acesso n�o encontrado.");
			SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal");
			return null;
		}
	}
	
	public ModelAndView confimarNovaConferencia(WebRequestContext request, ImportacaoXmlNfeBean bean){
		Entregadocumento entregadocumento = new Entregadocumento();
		if(bean.getFromPedidoColeta() != null && bean.getFromPedidoColeta()){
			entregadocumento.setFromPedidoColeta(bean.getFromPedidoColeta());
			entregadocumento.setWhereInPedido(bean.getWhereInPedido());
		}
		entregadocumento.setProducaoautomatica(bean.getProducaoautomatica());
		if(bean.getFromColeta() != null && bean.getFromColeta()){
			entregadocumento.setFromColeta(bean.getFromColeta());
			entregadocumento.setWhereInColeta(bean.getWhereInColeta());
		}
		if(request.getSession().getAttribute("InDevolucoes") != null){			
			request.setAttribute("whereInDevolucoes", request.getSession().getAttribute("InDevolucoes"));
			entregadocumento.setWhereInDevolucoes((String) request.getAttribute("whereInDevolucoes"));
			request.getSession().removeAttribute("InDevolucoes");
		}
//		Preencher os dados da entrega com os dados do conferenciaDadosFornecedorBean
		if (bean.getConferenciaDadosFornecedorBean() != null){
			entradafiscalService.preencheFornecedorFromImportacaoXml(entregadocumento, bean);
			
			Fornecedor fornecedor = Util.objects.isPersistent(entregadocumento.getFornecedor()) ? 
										fornecedorService.loadForTributacao(entregadocumento.getFornecedor()) : null;
					
			if(entregadocumento.getNaturezaoperacao() == null && fornecedor != null && fornecedor.getNaturezaoperacao() != null){
				entregadocumento.setNaturezaoperacao(fornecedor.getNaturezaoperacao());
			}
			if(entregadocumento.getNaturezaoperacao() == null){
				entregadocumento.setNaturezaoperacao(naturezaoperacaoService.loadPadrao(NotaTipo.ENTRADA_FISCAL));
			}
			
			Material material = null;
			Ncmcapitulo ncmcapitulo = null;
			ImportacaoXmlNfeAtualizacaoMaterialBean atualizaBean;
			Object object = request.getSession().getAttribute("IMPORTADOR_BEAN_RECEBER_ENTRADAFISCAL");
			if(object != null && object instanceof ImportacaoXmlNfeBean){
				ImportacaoXmlNfeBean bean_session = (ImportacaoXmlNfeBean)object;
				
				Entregamaterial entregamaterial;
				Set<Entregamaterial> listaEntregamaterial = new ListSet<Entregamaterial>(Entregamaterial.class);
				
				Integer controle = 0;
				for (ImportacaoXmlNfeItemBean it_session : bean_session.getListaItens()) {
					for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
						if(it.getSequencial() != null && it.getSequencial().equals(it_session.getSequencial())){
							it_session.setListaRastro(it.getListaRastro());
							it_session.setMaterial(materialService.load(it.getMaterial(), "material.cdmaterial, material.nome, material.identificacao"));
							
							if(it.getMaterial() != null){
								material = materialService.loadForNFProduto(it.getMaterial());
								
								atualizaBean = new ImportacaoXmlNfeAtualizacaoMaterialBean();
								atualizaBean.setTipoitemsped(material.getTipoitemsped());
								
								atualizaBean.setNcmcapitulo_bd(material.getNcmcapitulo());
								atualizaBean.setCodigobarras_bd(material.getCodigobarras());
								atualizaBean.setNcmcompleto_bd(material.getNcmcompleto());
								atualizaBean.setExtipi_bd(material.getExtipi());
								atualizaBean.setCodlistaservico_bd(material.getCodlistaservico());
								
								if(it_session.getNcm() != null && it_session.getNcm().trim().length() > 1){
									String ncm = it_session.getNcm().trim();
									if(ncm.length() > 2){
										atualizaBean.setNcmcompleto_xml(ncm);
									}
									Integer id_ncm = Integer.parseInt(ncm.substring(0, 2));
									ncmcapitulo = ncmcapituloService.load(new Ncmcapitulo(id_ncm));
									atualizaBean.setNcmcapitulo_xml(ncmcapitulo);
								}
								
								atualizaBean.setCodigobarras_xml(it_session.getCean());
								atualizaBean.setExtipi_xml(it_session.getExtipi());
								atualizaBean.setCodlistaservico_xml(it_session.getCListServ());
								
								materialService.updateInfByConferenciaEntradafical(material, atualizaBean);
								if(it.getPedidovendamaterial() != null){
									it_session.setPedidovendamaterial(it.getPedidovendamaterial());
								}
							}
							it_session.setUnidademedidaAssociada(it.getUnidademedidaAssociada());
						}
					}
					
					if(entregadocumento.getFornecedor() != null && entregadocumento.getNaturezaoperacao() != null && !Boolean.TRUE.equals(entregadocumento.getNaturezaoperacao().getNaoVincularMaterialFornecedor())){
						materialfornecedorService.saveIfNotExists(it_session.getMaterial(), entregadocumento.getFornecedor(), it_session.getCprod());
					}
					
					Unidademedida unidademedida = it_session.getUnidademedidaAssociada();
					
					entregamaterial = new Entregamaterial();
					entradafiscalService.setIdentificadorinterno(entregamaterial, controle);
					controle++;
					
					entregamaterial.setUnidademedidacomercial(unidademedida);
					entregamaterial.setCprod(it_session.getCprod());
					entregamaterial.setXprod(Util.strings.truncate(it_session.getXprod(), 120));
					entregamaterial.setQtde(it_session.getQtde());
					entregamaterial.setSequencial(it_session.getSequencial());
					entregamaterial.setPedidovendamaterial(it_session.getPedidovendamaterial());
					entregamaterial.setOrigemProduto(it_session.getOrigemProduto());
					/*if(SinedUtil.isListNotEmpty(it_session.getListaRastro())){
						if(it_session.getListaRastro().size() == 1){
							entregamaterial.setTipolote(true);
							Loteestoque loteestoque = it_session.getListaRastro().get(0).getLoteestoque();
							entregamaterial.setLoteestoque(loteestoqueService.loadWithNumeroValidade(loteestoque, material));
						}else{
							entregamaterial.setTipolote(true);
							List<Entregamateriallote> listaLotes = new ArrayList<Entregamateriallote>();
							for(Rastro rastro: it_session.getListaRastro()){
								Entregamateriallote lote = new Entregamateriallote();
								
								lote.setQtde(rastro.getqLote());
								lote.setLoteestoque(loteestoqueService.loadWithNumeroValidade(rastro.getLoteestoque(), material));
								
								listaLotes.add(lote);
							}
							entregamaterial.setListaEntregamateriallote(CollectionsUtil.listToSet(Entregamateriallote.class, listaLotes));
						}
					}*/
					
					listaEntregamaterial.add(entregamaterial);
				}
				entregadocumento.setListaEntregamaterial(listaEntregamaterial);
				entradafiscalService.setaValoresImportacao(entregadocumento, bean_session);
			}
			
			if(bean.getModelodocumentofiscal() != null){
				bean.setModelodocumentofiscal(modelodocumentofiscalService.load(bean.getModelodocumentofiscal()));
			}
			
			if(bean.getListaNfeAssociadas() != null && !bean.getListaNfeAssociadas().isEmpty()){
				if(entregadocumento.getListaEntregadocumentofrete() == null){
					entregadocumento.setListaEntregadocumentofrete(new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class));
				}
				if(entregadocumento.getListaEntregadocumentofreterateio() == null){
					entregadocumento.setListaEntregadocumentofreterateio(new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class));
				}
				for(ImportacaoXmlNfeAssociadaBean nfeAssociada : bean.getListaNfeAssociadas()){
					if(nfeAssociada.getChaveacesso() != null && nfeAssociada.getListaNfe() != null){
						String whereIn = SinedUtil.listAndConcatenate(nfeAssociada.getListaNfe(), "cdentregadocumento", ",");
						List<Entregadocumento> listaAssociadas = entradafiscalService.loadWithLista(whereIn, null, false);
						if(listaAssociadas != null && !listaAssociadas.isEmpty()){
							for(Entregadocumento edvinculo : listaAssociadas){
								if(edvinculo != null && edvinculo.getCdentregadocumento() != null){
									Entregadocumentofrete edf = new Entregadocumentofrete(entregadocumento, edvinculo);
									entregadocumento.getListaEntregadocumentofrete().add(edf);
									if(edvinculo.getListaEntregamaterial() != null && !edvinculo.getListaEntregamaterial().isEmpty()){
										for(Entregamaterial em : edvinculo.getListaEntregamaterial()){
											entregadocumento.getListaEntregadocumentofreterateio().add(new Entregadocumentofreterateio(entregadocumento, em));
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return continueOnAction("entrada", entregadocumento);
	}
	
	public ModelAndView ajaxVerificaFornecedorForDevolucao(WebRequestContext request){
		String itens = request.getParameter("itens");
		Boolean fornecedordiferente = false;
		if(itens != null && !"".equals(itens)){
			if(entradafiscalService.isFornecedorDiferente(itens)){
				fornecedordiferente = true;
			}
		}
		return new JsonModelAndView().addObject("fornecedordiferente", fornecedordiferente);
	}
	
	/**
	 * 
	 * @param request
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView gerarPdf(WebRequestContext request, EntradafiscalFiltro filtro) throws Exception {
		
		Report report = entradafiscalService.createReportEntradafiscalListagem(filtro);
		
		if (report==null){
			request.addMessage("N�o existem registros suficientes para gerar o relat�rio", MessageType.ERROR);
			return doListagem(request, filtro);
		}
		
		Resource resource = new Resource();
        resource.setContentType("application/pdf");
        resource.setFileName("entradafiscal_" + SinedUtil.datePatternForReport() + ".pdf");
        resource.setContents(Neo.getApplicationContext().getReportGenerator().toPdf(report));
		
		return new ResourceModelAndView(resource);
	}
	
	@Override
	protected ListagemResult<Entregadocumento> getLista(WebRequestContext request, EntradafiscalFiltro filtro) {
		if(filtro.getExibirTotais() != null && filtro.getExibirTotais()){
			entradafiscalService.setTotalListagem(filtro);
		}
		
		return super.getLista(request, filtro);
	}
	
	/**
	 * Ajax para carregar infos complementares da empresa
	 *
	 * @param request
	 * @param entregadocumento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/11/2013
	 */
	public ModelAndView ajaxCarregaInfosEmpresa(WebRequestContext request, Entregadocumento entregadocumento){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(entregadocumento.getEmpresa() != null && entregadocumento.getEmpresa().getCdpessoa() != null){
			Empresa empresa = empresaService.load(entregadocumento.getEmpresa(), "empresa.geracaospedpiscofins, empresa.geracaospedicmsipi");
			jsonModelAndView.addObject("geracaospedicmsipi", empresa.getGeracaospedicmsipi() != null ? empresa.getGeracaospedicmsipi() : false);
			jsonModelAndView.addObject("geracaospedpiscofins", empresa.getGeracaospedpiscofins() != null ? empresa.getGeracaospedpiscofins() : false);
		}
		return jsonModelAndView;
	}
	
	public ModelAndView montarGrade(WebRequestContext request, Material material){
		return materialService.montarGrade(request, material);
	}
	
	public ModelAndView limparGrade(WebRequestContext request, Entregamaterial entregamaterial){
		return materialService.limparGrade(request, entregamaterial);
	}
	
	public ModelAndView salvarGrade(WebRequestContext request, Material material){
		return materialService.salvarGrade(request, material);
	}
	
	public ModelAndView buscarInfMaterialGradeAJAX(WebRequestContext request, Material material){
		return materialService.buscarInfMaterialGradeAJAX(request, material);
	}
	
	public ModelAndView buscarInfLoteestoqueGradeAJAX(WebRequestContext request, Loteestoque loteestoque){
		return materialService.buscarInfLoteestoqueGradeAJAX(request, loteestoque);
	}
	
	public ModelAndView verificaButtonGrade(WebRequestContext request){
		return entradafiscalService.verificaButtonGrade(request);
	}
	
	public ModelAndView buscarMaterialmestregrade(WebRequestContext request){
		return entradafiscalService.buscarMaterialmestregrade(request);
	}
	
	public ModelAndView selecionarItensGrade(WebRequestContext request){
		return new ModelAndView("direct:/process/popup/popUpSelecionarItensGradeRecebimento", "bean", entradafiscalService.selecionarItensGrade(request));
	}
	
	public ModelAndView atualizarItensGrade(WebRequestContext request){
		return new ModelAndView("direct:process/popup/popUpAtualizarItensGradeRecebimento", "bean", entradafiscalService.atualizarItensGrade(request));
	}
	
	public ModelAndView salvarAtualizacaoItensGrade(WebRequestContext request, MaterialgradeBean materialgradeBean){
		return entradafiscalService.salvarAtualizacaoItensGrade(request, materialgradeBean);
	}
	
	public ModelAndView trocaEmpresaOuClienteOuNaturezaoperacao(WebRequestContext request, Entregadocumento entregadocumento){
		entradafiscalService.trocaEmpresaOuClienteOuNaturezaoperacao(request, entregadocumento);
		return continueOnAction("entrada", entregadocumento);
	}
	
	public void ajaxOnChangeGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao){
		entradafiscalService.ajaxOnChangeGrupotributacao(request, grupotributacao);
	}
	
	public void ajaxCarregaGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao){
		entradafiscalService.ajaxCarregaGrupotributacao(request, grupotributacao);
	}
	
	public ModelAndView comboBox(WebRequestContext request, Entregamaterial bean) {
		ModelAndView mv = new JsonModelAndView().addObject("listaClasse", materialService.findClasses(bean.getMaterial()));
		return mv;
	}
	
	@Command(validate=false)
	public ModelAndView ajustarListaAposIncluirItensGrade(WebRequestContext request, Entregadocumento bean) throws CrudException{
		String identificadortela = request.getParameter("identificadortela");
		Object attr = request.getSession().getAttribute("listaEntregamaterial" + identificadortela);
			
		Set<Entregamaterial> set = attr != null ? (Set<Entregamaterial>) attr : new ListSet<Entregamaterial>(Entregamaterial.class);
		bean.setListaEntregamaterial(set);
		entradafiscalService.ajustarListaAposIncluirItensGrade(bean, null);
		request.getSession().setAttribute("listaEntregamaterial" + identificadortela, bean.getListaEntregamaterial());
		
		entradafiscalService.addInfAtributo(request, bean, null);
		
		return continueOnAction("entrada", bean);
	}
	
	private void adicionarVinculoCTRC(WebRequestContext request, Entregadocumento form) throws Exception {
		
		String whereNotInCdentregadocumento = null; 
		
		if (form.getListaEntregadocumentofrete()!=null && !form.getListaEntregadocumentofrete().isEmpty()){
			whereNotInCdentregadocumento = CollectionsUtil.listAndConcatenate(form.getListaEntregadocumentofrete(), "entregadocumentovinculo.cdentregadocumento", ",");
		}
		
		List<Entregadocumento> listaEntregadocumento = entradafiscalService.findForCTRC(form.getCdsentregadocumento(), whereNotInCdentregadocumento);
		
		if (!listaEntregadocumento.isEmpty()){
			//Vinculo
			Set<Entregadocumentofrete> listaEntregadocumentofreteAdd = new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class);
			for (Entregadocumento entregadocumento: listaEntregadocumento){
				listaEntregadocumentofreteAdd.add(new Entregadocumentofrete(entregadocumento));
			}
			if (form.getListaEntregadocumentofrete()==null){
				form.setListaEntregadocumentofrete(listaEntregadocumentofreteAdd);
			}else {
				form.getListaEntregadocumentofrete().addAll(listaEntregadocumentofreteAdd);
			}
			
			//Materiais
			String whereInCdentregadocumento = CollectionsUtil.listAndConcatenate(listaEntregadocumento, "cdentregadocumento", ",");
			String whereNotInCdentregamaterial = null; 
			if (form.getListaEntregadocumentofreterateio()!=null && !form.getListaEntregadocumentofreterateio().isEmpty()){
				whereNotInCdentregamaterial = CollectionsUtil.listAndConcatenate(form.getListaEntregadocumentofreterateio(), "entregamaterial.cdentregamaterial", ",");
			}
			List<Entregamaterial> listaEntregamaterial = entregamaterialService.findForCTRC(whereInCdentregadocumento, whereNotInCdentregamaterial);
			if (!listaEntregamaterial.isEmpty()){
				Set<Entregadocumentofreterateio> listaEntregadocumentofreterateioAdd = new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class);
				for (Entregamaterial entregamaterial: listaEntregamaterial){
					listaEntregadocumentofreterateioAdd.add(new Entregadocumentofreterateio(entregamaterial));
				}
				if (form.getListaEntregadocumentofreterateio()==null){
					form.setListaEntregadocumentofreterateio(listaEntregadocumentofreterateioAdd);
				}else {
					form.getListaEntregadocumentofreterateio().addAll(listaEntregadocumentofreterateioAdd);
				}
			}
		}
	}
	
	private void removerVinculoCTRC(WebRequestContext request, Entregadocumento form) throws Exception {
		if (form.getListaEntregadocumentofrete()!=null && !form.getListaEntregadocumentofrete().isEmpty()){
			for (Iterator<Entregadocumentofrete> iterator1 = form.getListaEntregadocumentofrete().iterator(); iterator1.hasNext();) {
				Entregadocumentofrete entregadocumentofrete = (Entregadocumentofrete) iterator1.next();
				if (form.getCdentregadocumentofreteRemover().equals(entregadocumentofrete.getEntregadocumentovinculo().getCdentregadocumento().toString())){
					if (form.getListaEntregadocumentofreterateio()!=null){
						for (Iterator<Entregadocumentofreterateio> iterator2 = form.getListaEntregadocumentofreterateio().iterator(); iterator2.hasNext();) {
							Entregadocumentofreterateio entregadocumentofreterateio = (Entregadocumentofreterateio) iterator2.next();
							if (entregadocumentofreterateio.getEntregamaterial().getEntregadocumento().getCdentregadocumento().equals(entregadocumentofrete.getEntregadocumentovinculo().getCdentregadocumento())){
								iterator2.remove();
								
							}
						}
					}
					iterator1.remove();
				}
			}
		}
	}
	
	private void processarMateriaisCTRC(WebRequestContext request, Entregadocumento form) throws Exception {
		
		Set<Entregadocumentofreterateio> listaEntregadocumentofreterateio = form.getListaEntregadocumentofreterateio();
		Money valorTotalDocumentoCTRC = form.getValor()==null ?  new Money(0) : form.getValor();
		Money valorTotalRateioCTRC = new Money(0);
		
		if (listaEntregadocumentofreterateio!=null && !listaEntregadocumentofreterateio.isEmpty()){
			for (Entregadocumentofreterateio entregadocumentofreterateio: listaEntregadocumentofreterateio){
				if (entregadocumentofreterateio.getEntregamaterial().getQtde()==null){
					entregadocumentofreterateio.getEntregamaterial().setQtde(0D);
				}
				if (entregadocumentofreterateio.getEntregamaterial().getValorunitario()==null){
					entregadocumentofreterateio.getEntregamaterial().setValorunitario(0D);
				}
				Double qtde = entregadocumentofreterateio.getEntregamaterial().getQtde();
				Double valorunitario = entregadocumentofreterateio.getEntregamaterial().getValorunitario();
				double valortotal = SinedUtil.round(valorunitario * qtde, 2);
				entregadocumentofreterateio.getEntregamaterial().setValortotal(valortotal);
				
				if (entregadocumentofreterateio.getValor()!=null){
					valorTotalRateioCTRC = valorTotalRateioCTRC.add(entregadocumentofreterateio.getValor());
				}
			}
		}
		
		form.setValorTotalDocumentoCTRC(valorTotalDocumentoCTRC);
		form.setValorTotalRateioCTRC(valorTotalRateioCTRC);
		form.setValorRestanteCTRC(valorTotalDocumentoCTRC.subtract(valorTotalRateioCTRC));
	}
	
	/**
	 * Ajax que carrega as infos de modelodocumentofiscal para a tela 
	 *
	 * @param request
	 * @param entregadocumento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/11/2014
	 */
	public ModelAndView ajaxCarregaInfoModelodocumentofiscal(WebRequestContext request, Entregadocumento entregadocumento){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		Modelodocumentofiscal modelodocumentofiscal = entregadocumento.getModelodocumentofiscal();
		if(modelodocumentofiscal != null && modelodocumentofiscal.getCdmodelodocumentofiscal() != null){
			modelodocumentofiscal = modelodocumentofiscalService.load(modelodocumentofiscal);
			jsonModelAndView.addObject("modelodocumentofiscal", modelodocumentofiscal);
		}
		return jsonModelAndView;
	}
	
	public ModelAndView abrirPopUpApuracaoicms(WebRequestContext request, Entregamaterial entregamaterial){
		return entradafiscalService.abrirPopUpApuracaoicms(request, entregamaterial, "crud");
	}
	
	public ModelAndView carregarNfeAssociada(WebRequestContext request){
		List<Entregadocumento> lista = null;
		String whereIn = request.getParameter("whereIn");
		if(whereIn != null && !whereIn.trim().isEmpty()){
			lista = entradafiscalService.findAllForConferenciaCte(whereIn);
			
		}
		
		return new JsonModelAndView().addObject("listaEntregadocumento", lista);
	}
	
	public ModelAndView registrarAjuste(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado");
			return new ModelAndView("/fiscal/crud/Entradafiscal");
		}
		
		if(whereIn.split(",").length > 1){
			request.addError("� permitido selecionar apenas um item por vez para esta a��o.");
			return new ModelAndView("/fiscal/crud/Entradafiscal");
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		Entregadocumento entregadocumento = entradafiscalService.carregarDocumentoParaAjusteFiscal(new Entregadocumento(Integer.parseInt(whereIn)));
		
		Ajustefiscal entregadocumentoajustefiscal = new Ajustefiscal();
		entregadocumentoajustefiscal.setEntregadocumento(entregadocumento);
		List<Material> listaMaterial = new ArrayList<Material>();
		if(SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())){
			for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
				if(entregamaterial.getMaterial() != null && !listaMaterial.contains(entregamaterial.getMaterial())){
					listaMaterial.add(entregamaterial.getMaterial());
				}
			}
		}
		request.getSession().setAttribute("listaMaterialregistrarAjusteEntradaFiscal", listaMaterial);
		request.setAttribute("entrada", entrada);
		return new ModelAndView("direct:/crud/popup/registrarajuste", "bean", entregadocumentoajustefiscal);
	}
	
	public void saveRegistrarAjuste(WebRequestContext request, Ajustefiscal ajustefiscal){
		request.getSession().removeAttribute("listaMaterialregistrarAjusteEntradaFiscal");
		
		if(ajustefiscal.getEntregadocumento() == null){
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal");
		}
		ajustefiscalService.saveOrUpdate(ajustefiscal);
		
		request.addMessage("Ajuste registrado com sucesso.");
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal" + (entrada ? "?ACAO=consultar&cdentregadocumento="+
				ajustefiscal.getEntregadocumento().getCdentregadocumento():""));
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView abrePopUpAssociacaoPedidovendamaterial(WebRequestContext request, ImportacaoXmlNfeItemBean item){
		boolean fecharPopUp = false;
		Object object = request.getSession().getAttribute("IMPORTADOR_BEAN_RECEBER_ENTRADAFISCAL");
		if(object != null && object instanceof ImportacaoXmlNfeBean){
			ImportacaoXmlNfeBean bean = (ImportacaoXmlNfeBean)object;
			
			List<Pedidovendamaterial> listaPedidovendamaterialEntrega = new ArrayList<Pedidovendamaterial>();
			if(SinedUtil.isListNotEmpty(bean.getListaPedidovendamaterial())){
				listaPedidovendamaterialEntrega.addAll(bean.getListaPedidovendamaterial());
				
				List<Pedidovendamaterial> listaAssociados = null;
				Object objLista = request.getSession().getAttribute("LISTA_ASSOCIADO_PEDIDOVENDAMATERIAL_RECEBER_ENTREGA");
				if(objLista != null && objLista instanceof List){
					listaAssociados = (List<Pedidovendamaterial>) objLista;
					
					for (Iterator<Pedidovendamaterial> iterator = listaPedidovendamaterialEntrega.iterator(); iterator.hasNext();) {
						Pedidovendamaterial pedidovendamaterial = iterator.next();
						if(listaAssociados.contains(pedidovendamaterial)){
							iterator.remove();
						}
					}
				}
				
				for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterialEntrega) {
					pedidovendamaterial.getMaterial().setNomescape(pedidovendamaterial.getMaterial().getNome().replaceAll("'", "\\\\'").replaceAll("\"", ""));
				}
				
				List<ImportacaoXmlNfeItemBean> listaItens = bean.getListaItens();
				for (ImportacaoXmlNfeItemBean itBean : listaItens) {
					if(itBean.getSequencial() != null && itBean.getSequencial().equals(item.getSequencial())){
						itBean.setIndex(item.getIndex());
						request.setAttribute("itBean", itBean);
						break;
					}
				}
				
				request.setAttribute("listaPedidovendamaterialEntrega", listaPedidovendamaterialEntrega);
			}else {
				fecharPopUp = true;
			}
		}
		if(fecharPopUp){
			SinedUtil.fechaPopUp(request);
			return null;
		}
				
		return new ModelAndView("direct:/process/popup/popupAssociacaoPedidovendamaterial");
	}
	
	@SuppressWarnings("unchecked")
	public void associarPedidovendamaterialConferencia(WebRequestContext request, Pedidovendamaterial pedidovendamaterial){
		List<Pedidovendamaterial> listaAssociados = null;
		Object objLista = request.getSession().getAttribute("LISTA_ASSOCIADO_PEDIDOVENDAMATERIAL_RECEBER_ENTREGA");
		if(objLista != null && objLista instanceof List){
			listaAssociados = (List<Pedidovendamaterial>) objLista;
		} else {
			listaAssociados = new ArrayList<Pedidovendamaterial>();
		}
		
		listaAssociados.add(pedidovendamaterial);
		request.getSession().setAttribute("LISTA_ASSOCIADO_PEDIDOVENDAMATERIAL_RECEBER_ENTREGA", listaAssociados);
	}
	
	@SuppressWarnings("unchecked")
	public void deassociarPedidovendamaterialConferencia(WebRequestContext request, Pedidovendamaterial pedidovendamaterial){
		List<Integer> listaAssociados = null;
		Object objLista = request.getSession().getAttribute("LISTA_ASSOCIADO_PEDIDOVENDAMATERIAL_RECEBER_ENTREGA");
		if(objLista != null && objLista instanceof List){
			listaAssociados = (List<Integer>) objLista;
			listaAssociados.remove(pedidovendamaterial);
			request.getSession().setAttribute("LISTA_ASSOCIADO_PEDIDOVENDAMATERIAL_RECEBER_ENTREGA", listaAssociados);
		}
	}
	
	public ModelAndView ajaxInfoFornecedor(WebRequestContext request, Entregadocumento doc){
		return entradafiscalService.ajaxInfoFornecedor(request, doc);
	}
	
	public ModelAndView ajaxInfoNaturezaOperacao(WebRequestContext request, Entregadocumento doc){
		return entradafiscalService.ajaxInfoNaturezaOperacao(doc);
	}
	
	/**
	* M�todo que preenche o array de fun��o de icmsst
	*
	* @see br.com.linkcom.sined.geral.service.EntradafiscalService#preencherArrayFuncaoICMSSTAjax(WebRequestContext request, Entregadocumento entregadocumento)
	*
	* @param request
	* @param entregadocumento
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public ModelAndView preencherArrayFuncaoICMSSTAjax(WebRequestContext request, Entregadocumento entregadocumento){
		return entradafiscalService.preencherArrayFuncaoICMSSTAjax(request, entregadocumento);
	}
	
	/**
	* M�todo que converte a quantidade restante ao trocar a unidade de medida
	*
	* @param request
	* @param entregamaterial
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView converteUnidademedida(WebRequestContext request, Entregamaterial entregamaterial){
		return entradafiscalService.converteUnidademedida(request, entregamaterial);
	}
	
	/**
	* M�todo que abre uma popup com lotes do item da entrada fiscal
	*
	* @param request
	* @param entregamaterial
	* @return
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public ModelAndView gerenciarLote(WebRequestContext request, Entregamaterial entregamaterial){
		return entradafiscalService.gerenciarLote(request, entregamaterial, "crud");
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#ajaxVerificaObrigatoriedadeLote(WebRequestContext request)
	*
	* @param request
	* @return
	* @since 07/12/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificaObrigatoriedadeLote(WebRequestContext request){
		return materialService.ajaxVerificaObrigatoriedadeLote(request);
	}
	
	private void validaFechamentoFinanceiro(Entregadocumento bean){
		if (bean.getListaEntregadocumentoreferenciadoTrans() != null){
			for(Entregadocumentoreferenciado edr : bean.getListaEntregadocumentoreferenciadoTrans()){
				if(Boolean.TRUE.equals(edr.getContapaga())){
					if (fechamentofinanceiroService.haveFechamentoPeriodo(bean.getEmpresa(), edr.getDtpagamento(), edr.getDtpagamento(), edr.getVinculo())){
						throw new SinedException("J� existe um fechamento financeiro com os mesmos dados cadastrados no Documento referenciado.");
					}
				}
			}
		}
	}
	
	public ModelAndView ajaxHabilitaCamposDeACordoComGrupoMaterial(WebRequestContext request, Entregamaterial bean){
		ModelAndView retorno = new JsonModelAndView();
		if(bean.getMaterial() == null){
			return retorno.addObject("registrarPesoMedio", false);
		}
		Material material = materialService.load(bean.getMaterial(), "material.materialgrupo");
		
		if(material != null && material.getMaterialgrupo() != null && Boolean.TRUE.equals(material.getMaterialgrupo().getRegistrarpesomedio())){
			retorno.addObject("registrarPesoMedio", true);
		}else{
			retorno.addObject("registrarPesoMedio", false);
		}
		return retorno;
	}
	
	public ModelAndView ajaxBuscarDocumentoreferenciado(WebRequestContext request, Entregamaterial entregamaterial) {
		return entradafiscalService.ajaxBuscarDocumentoreferenciado(request, entregamaterial);
	}
	
	public ModelAndView ajaxPrazopagamento(WebRequestContext request, Entregadocumento entregadocumento){
		return prazopagamentoService.ajaxPrazopagamentoParcelasiguaisjuros(entregadocumento.getPrazopagamento());
	}
	
	public ModelAndView buscaInformacoesModelo(WebRequestContext request, Entregadocumento entregadocumento){
		return modelodocumentofiscalService.buscaInformacoesModelo(entregadocumento.getModelodocumentofiscal());
	}
	
	public ModelAndView ajaxBuscaUnidadeForConferencia(WebRequestContext request, Material material){
		ModelAndView retorno = new JsonModelAndView();
		String unidadeXml = request.getParameter("unidadeXml");
		if(material != null && material.getCdmaterial() != null && Util.strings.isNotEmpty(unidadeXml)){
			List<Unidademedida> listaUnidade = unidademedidaService.getUnidademedidaByMaterial(material);
			if(SinedUtil.isListNotEmpty(listaUnidade)){
				for(Unidademedida un: listaUnidade){
					if(unidadeXml.equals(un.getSimbolo())){
						retorno.addObject("cdunidademedida", un.getCdunidademedida())
								.addObject("simbolo", un.getSimbolo());
						break;
					}
				}
			}
		}
		return retorno;
	}
	
	public ModelAndView cancelar(WebRequestContext request){
		String erro = "N�o � poss�vel cancelar a(s) entrada(s) fiscal(ais): ";
		StringBuilder error = new StringBuilder();
		Boolean existeEntrega = false;
		Boolean isCancelada = false;
		Boolean documentoEmAberto = false;
		Entregadocumento entregadocumento = new Entregadocumento();
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		if (itensSelecionados == null || itensSelecionados.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		String[] cdentregas = itensSelecionados.split(",");
		int cont = 0;
		for (String cdentrega : cdentregas) {
			entregadocumento.setCdentregadocumento(Integer.parseInt(cdentrega));
			entregadocumento = entradafiscalService.loadForCancelamento(entregadocumento);
			if (Entregadocumentosituacao.CANCELADA.equals(entregadocumento.getEntregadocumentosituacao())) {
				isCancelada = true;
				if (cont == 0) {
					error.append(entregadocumento.getNumero());
					cont++;
				} else {
					error.append(", " + entregadocumento.getNumero());
					cont++;
				}
			} else if (entregadocumento.getEntrega() != null) {
				existeEntrega = true;
				if (cont == 0) {
					error.append(entregadocumento.getNumero());
					cont++;
				} else {
					error.append(", " + entregadocumento.getNumero());
					cont++;
				}

			} else if (entregadocumento.getListadocumento() != null) {
				for(Entregapagamento doc : entregadocumento.getListadocumento()){
					if(doc.getDocumentoantecipacao()!=null && !Documentoacao.CANCELADA.equals(doc.getDocumentoantecipacao().getDocumentoacao())){	
						documentoEmAberto = true;
						if (cont == 0) {
							error.append(entregadocumento.getNumero());
							cont++;
						} else {
							error.append(", " + entregadocumento.getNumero());
							cont++;
						}													
						
					}
				}					
			}			
		}
		List<Documentoorigem> listadocumentoOrigem = documentoorigemService.getListaDocumentosDaEntradaFiscalCancelar(itensSelecionados);
		if(listadocumentoOrigem != null) {
			for(Documentoorigem doc : listadocumentoOrigem){
				if(doc!=null && !Documentoacao.CANCELADA.equals(doc.getDocumento().getDocumentoacao())){	
					documentoEmAberto = true;
					if (cont == 0) {
						error.append(entregadocumento.getNumero());
						cont++;
					} else {
						error.append(", " + entregadocumento.getNumero());
						cont++;
					}													
					
				}
			}					
		}
		if (cont == 0) {
			entregadocumento.setWhereIn(itensSelecionados);
			request.setAttribute("titulo", "CANCELAR ENTRADA FISCAL");
			request.setAttribute("acaoSave", "saveCancelarEntradaFiscal");
			return new ModelAndView(
					"direct:/crud/popup/popupCancelamentoEntradaFiscal",
					"entregadocumento", entregadocumento);
		}
		request.addError(erro + error);
		if(existeEntrega){			
			request.addError("\n Por favor cancelar o(s) recebimentos(s).");
		}
		else if(documentoEmAberto){		
			request.addError("\n Por favor cancelar a(s) conta(s) a pagar vinculadas � entrada fiscal.");			
		}
		else if(isCancelada){
			request.addError("\n Entrada fiscal j� cancelada.");
		}
		SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal");
		return null;
    }

	public void saveCancelar(WebRequestContext request, Entregadocumento bean){
		
		String itensSelecionados = bean.getWhereIn();
    	String[] itens = itensSelecionados.split(",");
		for (String item : itens) {	
			bean.setCdentregadocumento(Integer.parseInt(item));
			entradafiscalService.updateSituacaoEntradafiscal(bean, Entregadocumentosituacao.CANCELADA);
			Entregadocumentohistorico edh = entradafiscalService.criaEntregadocumentohistorico(bean, Entregadocumentosituacao.CANCELADA);
			edh.setObservacao(bean.getObservacao());
			entregadocumentohistoricoService.saveOrUpdate(edh);
		}
		SinedUtil.redirecionamento(request, "/fiscal/crud/Entradafiscal");

	}
	
	@Override
	protected void excluir(WebRequestContext request, Entregadocumento bean)
			throws Exception {
		throw new SinedException("A��o n�o permitida");
	}
	
	public ModelAndView ajaxIsApropriacao(WebRequestContext request, Entregadocumento entregaDocumento){
		return entradafiscalService.ajaxIsApropriacao(request, entregaDocumento);
	}
	
	public void ajaxMontaApropriacoes(WebRequestContext request, Entregadocumento entregaDocumento){
		entradafiscalService.ajaxMontaApropriacoes(request, entregaDocumento);
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView abrirPopupEntregaMaterial(WebRequestContext request, Entregadocumento entregadocumento) {
		Entregamaterial entregamaterial = null;
		String indexStr = request.getParameter("index");
		String identificadortela = request.getParameter("identificadortela");
		
		if(StringUtils.isNotBlank(identificadortela) && StringUtils.isNotBlank(indexStr)) {
			Integer index = Integer.parseInt(indexStr);
			
			Object attr = request.getSession().getAttribute("listaEntregamaterial" + request.getParameter("identificadortela"));
			if(attr != null) {
				Set<Entregamaterial> set = (Set<Entregamaterial>) attr;
				entregamaterial = set.toArray(new Entregamaterial[set.size()])[index];
				entregamaterial.setIdentificadortela(identificadortela);
				entregamaterial.setIndexLista(index);
				
				if(Util.objects.isPersistent(entregadocumento) && entregamaterial.getEntregadocumento() == null){
					entregamaterial.setEntregadocumento(entregadocumento);
				}
			}
		} else {
			entregamaterial = new Entregamaterial();
			entregamaterial.setIdentificadortela(identificadortela);
			entregamaterial.setIndexLista(null);
			entregamaterial.setListaEntregamateriallote(new ListSet<Entregamateriallote>(Entregamateriallote.class));
		}
		
		String consultar = request.getParameter("consultar");
		request.setAttribute("consultar", consultar != null && consultar.equals("true") ? true : false);
		
		String haveOrdemCompra = request.getParameter("haveOrdemCompra");
		if(StringUtils.isNotBlank(haveOrdemCompra)) { 
			request.setAttribute("haveOrdemCompra", haveOrdemCompra); 
		}
		if(Util.objects.isPersistent(entregadocumento.getEntrega())){
			request.setAttribute("entregaJaBaixada", !entregaService.entregaNotBaixada(entregadocumento.getEntrega().getCdentrega().toString()));
		}
		entradafiscalService.addInfAtributo(request, entregadocumento, entregamaterial);
		return new ModelAndView("direct:crud/popup/criaEntregamaterial", "entregamaterial", entregamaterial).addObject("tipoLoteRadio", Boolean.TRUE.equals(entregamaterial.getTipolote()));
	}
	
	@SuppressWarnings("unchecked")
	public void salvarPopupEntregaMaterial(WebRequestContext request, Entregamaterial entregamaterial) {
		Object attribute = request.getSession().getAttribute("listaEntregamaterial" + entregamaterial.getIdentificadortela());
		Set<Entregamaterial> listaEntregamaterial = null;
		StringBuilder functionBuilder = new StringBuilder();
		
		if (attribute == null) {
			listaEntregamaterial = new ListSet<Entregamaterial>(Entregamaterial.class);
			listaEntregamaterial.add(entregamaterial);
		} else {
			listaEntregamaterial = (Set<Entregamaterial>) attribute;
			
			if (entregamaterial.getIndexLista() != null) {
				List<Entregamaterial> listaAux = Arrays.asList(listaEntregamaterial.toArray(new Entregamaterial[listaEntregamaterial.size()]));
				listaAux.set(entregamaterial.getIndexLista(), entregamaterial);
				listaEntregamaterial = (Set<Entregamaterial>) new LinkedHashSet<Entregamaterial>(listaAux);
			} else {
				listaEntregamaterial.add(entregamaterial);
			} 
		}
		
		this.ajustaNullParaSalvarPopupItem(entregamaterial);
		
		request.getSession().setAttribute("listaEntregamaterial" + entregamaterial.getIdentificadortela(), listaEntregamaterial);
		request.getServletResponse().setContentType("text/html");
		
		if(entregamaterial.getCfop() != null){
			entregamaterial.setCfop(cfopService.load(entregamaterial.getCfop()));
		}
		
		String materialIdentificacao = entregamaterial.getMaterial() != null ? entregamaterial.getMaterial().getAutocompleteDescription() : "";
		functionBuilder.append("parent.montaListaItens("+ entregamaterial.getIndexLista()
				+", " + (entregamaterial.getMaterial() != null ? entregamaterial.getMaterial().getCdmaterial() : "null") 
				+", '" + SinedUtil.escapeSingleQuotes(materialIdentificacao) + "'"
				+", " + (entregamaterial.getCfop() != null ? entregamaterial.getCfop().getCdcfop() : "''") 
				+", " + (entregamaterial.getCfop() != null ? entregamaterial.getCfop().getNaoconsiderarreceita() : "''")
				+", '" + (entregamaterial.getCfop() != null ? entregamaterial.getCfop().getCodigo() : "") + "'"
				+", '" + (entregamaterial.getValoricms() != null ? entregamaterial.getValoricms().toString() : "0,00") + "'"
				+", '" + (entregamaterial.getValoriss() != null ? entregamaterial.getValoriss().toString() : "0,00") + "'"
				+", '" + (entregamaterial.getValoripi() != null ? entregamaterial.getValoripi().toString() : "0,00") + "'"
				+", '" + (entregamaterial.getValorpis() != null ? entregamaterial.getValorpis().toString() : "0,00") + "'"
				+", '" + (entregamaterial.getValorcofins() != null ? entregamaterial.getValorcofins().toString() : "0,00") + "'"
				+", '" + (entregamaterial.getValorfrete() != null ? entregamaterial.getValorfrete().toString() : "0,00") + "'"
				+", '" + (entregamaterial.getValordesconto() != null ? entregamaterial.getValordesconto().toString() : "0,00") + "'"
				+", '" + (entregamaterial.getValorseguro() != null ? entregamaterial.getValorseguro().toString() : "0,00") + "'"
				+", '" + (entregamaterial.getQtde() != null ? SinedUtil.descriptionDecimal(entregamaterial.getQtde()) : "0") + "'"
				+", " + (entregamaterial.getUnidademedidacomercial() != null ? entregamaterial.getUnidademedidacomercial().getCdunidademedida() : "''")
				+", '" + (entregamaterial.getUnidademedidacomercial() != null ? escapeSingleQuotes(unidademedidaService.load(entregamaterial.getUnidademedidacomercial()).getNome()) : "") + "'"
				+", '" + (entregamaterial.getValorunitario() != null ? new Money(entregamaterial.getValorunitario()) : "0,00") + "'"
				+", '" + (entregamaterial.getValortotaloperacao() != null ? new Money(entregamaterial.getValortotaloperacao()) : "0,00") + "'"
				+", '" + (entregamaterial.getValortotal() != null ? new Money(entregamaterial.getValortotal()) : "0,00") + "'"
				+", " + (entregamaterial.getCentrocusto() != null ? entregamaterial.getCentrocusto().getCdcentrocusto() : "''")
				+"); ");
		
		functionBuilder.append(montarJavascriptRecalcularTotais(listaEntregamaterial));
		functionBuilder.append("parent.$.akModalRemove();");
		View.getCurrent().println("<script>" + functionBuilder.toString() + "</script>");
	}
	
	private void ajustaNullParaSalvarPopupItem(Entregamaterial entregamaterial){
		if(entregamaterial.getValoricms() == null){
			entregamaterial.setValoricms(new Money());
		}
		if(entregamaterial.getValoriss() == null){
			entregamaterial.setValoriss(new Money());			
		}
		if(entregamaterial.getValoripi() == null){
			entregamaterial.setValoripi(new Money());			
		}
		if(entregamaterial.getValorpis() == null){
			entregamaterial.setValorpis(new Money());			
		}
		if(entregamaterial.getValorcofins() == null){
			entregamaterial.setValorcofins(new Money());			
		}
		if(entregamaterial.getValorfrete() == null){
			entregamaterial.setValorfrete(new Money());			
		}
		if(entregamaterial.getValordesconto() == null){
			entregamaterial.setValordesconto(new Money());			
		}
		if(entregamaterial.getValorseguro() == null){
			entregamaterial.setValorseguro(new Money());			
		}
		if(entregamaterial.getValorunitario() == null){
			entregamaterial.setValorunitario(0d);			
		}
	}
	
	// a fun��o receber� todos os totais e setar� nos seus devidos campos
	private String montarJavascriptRecalcularTotais(Set<Entregamaterial> listaEntregamaterial) {
		if(SinedUtil.isListEmpty(listaEntregamaterial)) {
			return "";
		}
		Money valorMercadoria = new Money();
		Money valorMercadoriaReceita = new Money();
		Money valorImportacao = new Money();
		Money icmsDesonerado = new Money();
		Money icmsst = new Money();
		Money fcpst = new Money();
		Money icmsstTotalDocumento = new Money();
		Money fcpstTotalDocumento = new Money();
		Money ipi = new Money();
		Money frete = new Money();
		Money outrasDespesas = new Money();
		Money seguro = new Money();
		Money desconto = new Money();
		Money csll = new Money();
		Money inss = new Money();
		Money impostoImportacao = new Money();
		Money totalIssRetido = new Money();
		
		for(Entregamaterial item : listaEntregamaterial) {
			valorMercadoria = valorMercadoria.add(getValorMercadoriaItem(item));
			valorImportacao = valorImportacao.add(SinedUtil.zeroIfNull(item.getValorDespesasAduaneiras())).add(SinedUtil.zeroIfNull(item.getValoriiItem())).add(SinedUtil.zeroIfNull(item.getIof()));
			icmsDesonerado = icmsDesonerado.add(SinedUtil.zeroIfNull(item.getValorIcmsDesonerado()));
			icmsst = icmsst.add(SinedUtil.zeroIfNull(item.getValoricmsst()));
			fcpst = fcpst.add(SinedUtil.zeroIfNull(item.getValorfcpst()));
			ipi = ipi.add(SinedUtil.zeroIfNull(item.getValoripi()));
			frete = frete.add(SinedUtil.zeroIfNull(item.getValorfrete()));
			outrasDespesas = outrasDespesas.add(SinedUtil.zeroIfNull(item.getValoroutrasdespesas()));
			seguro = seguro.add(SinedUtil.zeroIfNull(item.getValorseguro()));
			desconto = desconto.add(SinedUtil.zeroIfNull(item.getValordesconto()));
			csll = csll.add(SinedUtil.zeroIfNull(item.getValorcsll()));
			inss = inss.add(SinedUtil.zeroIfNull(item.getValorinss()));
			impostoImportacao = impostoImportacao.add(SinedUtil.zeroIfNull(item.getValorimpostoimportacao()));
			
			if(!Boolean.TRUE.equals(item.getNaoconsideraricmsst())) {
				icmsstTotalDocumento = icmsstTotalDocumento.add(SinedUtil.zeroIfNull(item.getValoricmsst()));
				fcpstTotalDocumento = fcpstTotalDocumento.add(SinedUtil.zeroIfNull(item.getValorfcpst()));
			}
			
			boolean considerarReceita = item.getCfop() == null || item.getCfop().getNaoconsiderarreceita() == null || !item.getCfop().getNaoconsiderarreceita();
			if(considerarReceita) {
				valorMercadoriaReceita = valorMercadoriaReceita.add(getValorMercadoriaItem(item));
			}
			
			if(item.getTipotributacaoiss() != null && item.getTipotributacaoiss().equals(Tipotributacaoiss.RETIDA)) {
				totalIssRetido = totalIssRetido.add(SinedUtil.zeroIfNull(item.getValoriss()));
			}
		}
		
		Money totalOutrosValores = icmsstTotalDocumento.add(fcpstTotalDocumento).add(ipi).add(valorImportacao).add(frete).add(outrasDespesas).add(seguro).subtract(icmsDesonerado).subtract(desconto).subtract(totalIssRetido);
		
		return "parent.recalcularTotais('"+ valorMercadoria +"', " +
				"'"+ valorMercadoriaReceita +"', " +
				"'"+ totalOutrosValores +"', " +
				"'"+ valorImportacao +"', " +
				"'"+ icmsst +"', " +
				"'"+ icmsstTotalDocumento +"', " +
				"'"+ icmsDesonerado +"', " +
				"'"+ fcpst +"', " +
				"'"+ fcpstTotalDocumento +"', " +
				"'"+ ipi +"', " +
				"'"+ frete +"', " +
				"'"+ outrasDespesas +"', " +
				"'"+ seguro +"', " +
				"'"+ desconto +"', " +
				"'"+ csll +"', " +
				"'"+ inss +"', " +
				"'"+ impostoImportacao +"', " +
				"'"+ totalIssRetido +"'" +
				");";
	}
	
	private Money getValorMercadoriaItem(Entregamaterial entregamaterial) {
		if(entregamaterial.getValortotal() != null && entregamaterial.getValortotal().doubleValue() == 0d) {
			return new Money(0d);
		}
		
		Double qtde = entregamaterial.getQtde() != null ? entregamaterial.getQtde() : 0d;
		Double valorUnitario = entregamaterial.getValorunitario() != null ? entregamaterial.getValorunitario() : 0d;
		
		Money valor = new Money(entregamaterial.getValortotal() != null ? entregamaterial.getValortotal() : (qtde * valorUnitario));
		Money valorii = SinedUtil.zeroIfNull(entregamaterial.getValorimpostoimportacao());
		Money valorir = SinedUtil.zeroIfNull(entregamaterial.getValorir());
		Money valorinss = SinedUtil.zeroIfNull(entregamaterial.getValorinss());
		Money valorcsll = SinedUtil.zeroIfNull(entregamaterial.getValorcsll());
		Money valorpisretido = SinedUtil.zeroIfNull(entregamaterial.getValorpisretido());
		Money valorcofinsretido = SinedUtil.zeroIfNull(entregamaterial.getValorcofinsretido());
		
		Money total = valor.add(valorii);
		Money impostos = valorcofinsretido.add(valorpisretido).add(valorir).add(valorinss).add(valorcsll);
		
		return total.subtract(impostos);
	}
	
	private String escapeSingleQuotes(String opValue) {
		if(opValue==null) return null;
		return opValue.replaceAll("\\\\", "\\\\\\\\").replaceAll("\'", "\\\\'");
	}
	
	@SuppressWarnings("unchecked")
	public void excluirEntregamaterial(WebRequestContext request) {
		try {
			Set<Entregamaterial> listaEntregamaterial = null;
			Object object = request.getParameter("index");
			
			if (object != null) {
				String parameter = object.toString();
				Integer index = Integer.parseInt(parameter) - 1;
				Object attr = request.getSession().getAttribute("listaEntregamaterial" + request.getParameter("identificadortela"));
				
				if (attr != null) {
					listaEntregamaterial = (Set<Entregamaterial>) attr;
					List<Entregamaterial> listaAux = new LinkedList<Entregamaterial>(Arrays.asList(listaEntregamaterial.toArray(new Entregamaterial[listaEntregamaterial.size()])));
					listaAux.remove(listaAux.get(index));
					listaEntregamaterial = new LinkedHashSet<Entregamaterial>(listaAux);
				}
			}
			
			request.getSession().setAttribute("listaEntregamaterial" + request.getParameter("identificadortela"), listaEntregamaterial);
			
			String function = montarJavascriptRecalcularTotais(listaEntregamaterial);
			View.getCurrent().println("var sucesso = true; " + function);
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void ajaxSelecionarCfopTodos(WebRequestContext request) {
		try {
			String identificadortela = request.getParameter("identificadortela");
			Object attribute = request.getSession().getAttribute("listaEntregamaterial" + identificadortela);
	
			String cdcfop = request.getParameter("cdcfop");
			Cfop cfop = null;
			if(StringUtils.isNotBlank(cdcfop)) {
				cfop = cfopService.load(new Cfop(Integer.parseInt(cdcfop)), "cfop.cdcfop, cfop.codigo, cfop.descricaoresumida");
			}
			
			if (attribute != null && cfop != null) {
				Set<Entregamaterial> listaEntregamaterial = (Set<Entregamaterial>) attribute;
				
				if(SinedUtil.isListNotEmpty(listaEntregamaterial)) {
					for(Entregamaterial item : listaEntregamaterial) {
						item.setCfop(cfop);
					}
				}
				
				View.getCurrent().println("var sucesso = true; " 
						+"var codigoCfop = '" + cfop.getCodigo() + "'; "
						+"var cdcfop = '" + cfop.getCdcfop() + "'; ");
			} else {
				View.getCurrent().println("var sucesso = false; ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false; ");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void ajaxSelecionarNaturezabccTodos(WebRequestContext request) {
		try {
			String identificadortela = request.getParameter("identificadortela");
			Object attribute = request.getSession().getAttribute("listaEntregamaterial" + identificadortela);
			
			String naturezabccStr = request.getParameter("naturezabcc");
			Naturezabasecalculocredito naturezabcc = Naturezabasecalculocredito.valueOf(naturezabccStr);
			
			if (attribute != null && naturezabcc != null) {
				Set<Entregamaterial> listaEntregamaterial = (Set<Entregamaterial>) attribute;
				if(SinedUtil.isListNotEmpty(listaEntregamaterial)) {
					for(Entregamaterial item : listaEntregamaterial) {
						item.setNaturezabcc(naturezabcc);
					}
				}
				View.getCurrent().println("var sucesso = true; ");
			} else {
				View.getCurrent().println("var sucesso = false; ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false; ");
		}
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView salvarEntradafiscalFromRecebimento(WebRequestContext request, Entregadocumento entregadocumento) throws CrudException {
		String identificadortelaRecebimento = entregadocumento.getIdentificadortelaRecebimento();
		String identificadortela = entregadocumento.getIdentificadortela();
		StringBuilder urlBuilder = new StringBuilder("/suprimento/crud/Entrega");
		
		if(StringUtils.isNotBlank(identificadortelaRecebimento) && StringUtils.isNotBlank(identificadortela)) {
			urlBuilder.append("?identificadortela=" + identificadortelaRecebimento);
			
			Object attrEntregamaterial = request.getSession().getAttribute("listaEntregamaterial" + identificadortela);
			if(attrEntregamaterial != null) {
				Set<Entregamaterial> listaEntregamaterial = new ListSet<Entregamaterial>(Entregamaterial.class);
				listaEntregamaterial.addAll((Set<Entregamaterial>) attrEntregamaterial);
				entregadocumento.setListaEntregamaterial(listaEntregamaterial);
			}
			
			Object attrEntregadocumento = request.getSession().getAttribute("listaEntregadocumento" + identificadortelaRecebimento);
			if(attrEntregadocumento != null) {
				List<Entregadocumento> listaEntregadocumento = (List<Entregadocumento>) attrEntregadocumento;
				
				if(entregadocumento.getIndexlista() != null) {
					listaEntregadocumento.set(entregadocumento.getIndexlista(), entregadocumento);
				} else if(SinedUtil.isListNotEmpty(listaEntregadocumento)) {
					listaEntregadocumento.add(entregadocumento);
				} else {
					entregadocumento.setIndexlista(0);
					listaEntregadocumento = new ArrayList<Entregadocumento>();
					listaEntregadocumento.add(entregadocumento);
					request.getSession().setAttribute("listaEntregadocumento" + identificadortelaRecebimento , listaEntregadocumento);
				}
				
				if(entregadocumento.getCdentregadocumento() != null) {
					doSalvar(request, entregadocumento);
				} else {
					validateBean(entregadocumento, request.getBindException());
					this.validateBeanCamposObrigatorios(entregadocumento, request.getBindException());
				}
	
				if(request.getBindException().hasErrors()){
					StringBuilder url = new StringBuilder("/fiscal/crud/Entradafiscal?ACAO=criarEntradafiscalDoRecebimento&fromrecebimento=true&existeErrosEntradaFiscaoFromRecebimento=true" +
									"&cdentrega=" + (entregadocumento.getCdentrega() == null ? "" : entregadocumento.getCdentrega()) +
									"&cdentregadocumento=" + (entregadocumento.getCdentregadocumento() == null ? "": entregadocumento.getCdentregadocumento()) +
									"&identificadortela=" + identificadortelaRecebimento +
									"&indexLista=" + (entregadocumento.getIndexlista()));
					request.getSession().setAttribute("entregadocumentoTrans"  + identificadortela, entregadocumento);
					request.getSession().setAttribute("indexListaEntregadocumentoTrans"  + identificadortela, entregadocumento.getIndexlista());
					//request.setAttribute("existeErrosEntradaFiscaoFromRecebimento", Boolean.TRUE);
					return new ModelAndView("redirect:" + url.toString());
					
			//		return continueOnAction("criarEntradafiscalDoRecebimento", entregadocumento);
				}
				
				request.getSession().setAttribute("listaEntregadocumento" + identificadortelaRecebimento, listaEntregadocumento);
			} else {
				request.addError("Houve um problema ao salvar a entrada fiscal.");
			}
		} else {
			request.addError("Houve um problema ao salvar a entrada fiscal.");
		}
		
		return getModelAndViewOrigemEntrega(request, identificadortelaRecebimento, null, null);
	}
	
	private ModelAndView getModelAndViewOrigemEntrega(WebRequestContext request, String identificadortelaRecebimento, String acaoAnterior, String cdentregaOrigem) {
		StringBuilder urlBuilder = new StringBuilder("/suprimento/crud/Entrega");
		
		if(StringUtils.isNotBlank(identificadortelaRecebimento)) {
			urlBuilder.append("?identificadortela=" + identificadortelaRecebimento);
		}
		String acao = acaoAnterior != null ? acaoAnterior : request.getParameter("acaoAnterior");
		String cdentrega = cdentregaOrigem != null ? cdentregaOrigem : request.getParameter("cdentrega");
		
		if(!SinedUtil.validaNumeros(cdentrega) && "editar".equalsIgnoreCase(acao)){
			cdentrega = "";
			acao = "criar";
		}
		urlBuilder.append(StringUtils.isNotBlank(acao) ? "&ACAO=" + acao : "");
		urlBuilder.append(SinedUtil.validaNumeros(cdentrega) ? "&cdentrega=" + cdentrega : "");
		
		return new ModelAndView("redirect:" + urlBuilder.toString());
	}
	
	public ModelAndView ajaxBuscaLotes(WebRequestContext request, Entregamaterial bean){
		return loteestoqueService.ajaxBuscaLotes(bean);
	}
}