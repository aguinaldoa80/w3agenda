package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Sintegra;
import br.com.linkcom.sined.geral.service.SintegraService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/fiscal/crud/Sintegra", authorizationModule=CrudAuthorizationModule.class)
public class SintegraCrud extends CrudControllerSined<SintegraFiltro, Sintegra, Sintegra>{

	private SintegraService sintegraService;
		
	public void setSintegraService(SintegraService sintegraService) {this.sintegraService = sintegraService;}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Sintegra form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Sintegra form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Sintegra form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}	
	
	@SuppressWarnings("unchecked")
	protected void setListagemInfo(WebRequestContext request, SintegraFiltro filtro) throws Exception {
		if (!listagemVaziaPrimeiraVez() || filtro.isNotFirstTime()) {
			if(request.getParameter("resetCurrentPage") != null) {
				filtro.setCurrentPage(0);
			}
			
			ListagemResult<Sintegra> listagemResult = getLista(request, filtro);
			List<Sintegra> list = listagemResult.list();
			
			// CHAVES DOS ARQUIVOS PARA DOWNLOAD
			for (Sintegra sintegra : list) {
				if(sintegra.getArquivo() != null)
					DownloadFileServlet.addCdfile(request.getSession(), new Long(sintegra.getArquivo().getCdarquivo()));
			}
			
			request.setAttribute("lista", list);
			request.setAttribute("currentPage", filtro.getCurrentPage());
			request.setAttribute("numberOfPages", filtro.getNumberOfPages());
			request.setAttribute("filtro", filtro);			
		} else {
			request.setAttribute("lista", new ArrayList());
			request.setAttribute("currentPage", 0);
			request.setAttribute("numberOfPages", 0);
			request.setAttribute("filtro", filtro);
		}
	}
	
	public ModelAndView gerar(WebRequestContext request, SintegraFiltro filtro){
		try{
			return new ResourceModelAndView(sintegraService.gerarArquivo(filtro, request));
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return sendRedirectToAction("listagem");	
		}
		 
	}

}
