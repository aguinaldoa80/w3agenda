package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OperacaoEnum {

	CIENCIA_OPERACAO		 (0, "Ci�ncia da opera��o"), 
	DESCONHECIMENTO_OPERACAO (1, "Desconhecimento da opera��o"), 
	CONFIRMACAO_OPERACAO	 (2, "Confirma��o da opera��o"),
	OPERACAO_NAO_REALIZADA	 (3, "Opera��o n�o realizada");
	
	private Integer value;
	private String descricao;

	private OperacaoEnum(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public Integer getValue() {
		return value;
	}

	@Override
	public String toString() {
		return descricao;
	}
}
