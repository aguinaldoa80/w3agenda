package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoLoteConsultaDfeEnum {

	NSU			  (0, "Consulta por NSU"), 
	NSU_SEQUENCIA (1, "Consulta por Sequ�ncia NSU"),
	CHAVE_ACESSO  (2, "Consulta por Chave de Acesso");

	private Integer value;
	private String descricao;

	private TipoLoteConsultaDfeEnum(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public Integer getValue() {
		return value;
	}

	@Override
	public String toString() {
		return descricao;
	}
}