package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ManifestoDfeSituacaoEnum {

	SEM_MANIFESTO   (0, "Sem manifesto"),
	CONFIRMACAO     (210200, "Confirma��o de Opera��o"),
	CIENCIA         (210210, "Ci�ncia da Opera��o"),
	DESCONHECIMENTO (210220, "Desconhecimento da Opera��o"),
	NAO_REALIZADA   (210240, "Opera��o n�o Realizada");
	
	private Integer value;
	private String descricao;

	private ManifestoDfeSituacaoEnum(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public Integer getValue() {
		return value;
	}

	@Override
	public String toString() {
		return descricao;
	}
	
	public static String listAndConcatenate(List<ManifestoDfeSituacaoEnum> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (ManifestoDfeSituacaoEnum bean : ManifestoDfeSituacaoEnum.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof ManifestoDfeSituacaoEnum){
					if((situacoes.get(i)).equals(bean)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(bean.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(bean.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		
		String cods = codigos.toString();
		
		cods = cods.replace("210200", "1");
		cods = cods.replace("210210", "2");
		cods = cods.replace("210220", "3");
		cods = cods.replace("210240", "4");
		
		return cods;
	}

	public static List<ManifestoDfeSituacaoEnum> getTodos() {
		List<ManifestoDfeSituacaoEnum> lista = new ArrayList<ManifestoDfeSituacaoEnum>();
		
		lista.add(SEM_MANIFESTO);
		lista.add(CONFIRMACAO);
		lista.add(CIENCIA);
		lista.add(DESCONHECIMENTO);
		lista.add(NAO_REALIZADA);
		
		return lista;
	}
}
