package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum LoteConsultaDfeItemStatusEnum {
	
	DFE_AUTORIZADA (1, "Uso autorizado"),
	DFE_DENEGADA   (2, "Uso denegado"),
	DFE_CANCELADA  (3, "NF-e cancelada");
	
	private Integer value;
	private String descricao;

	private LoteConsultaDfeItemStatusEnum(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public Integer getValue() {
		return value;
	}

	@Override
	public String toString() {
		return descricao;
	}
	
	public static String listAndConcatenate(List<LoteConsultaDfeItemStatusEnum> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (LoteConsultaDfeItemStatusEnum bean : LoteConsultaDfeItemStatusEnum.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof LoteConsultaDfeItemStatusEnum){
					if((situacoes.get(i)).equals(bean)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(bean.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(bean.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		
		String cods = codigos.toString();
		
		cods = cods.replace("1", "0");
		cods = cods.replace("2", "1");
		cods = cods.replace("3", "2");
		
		return cods;
	}

	public static List<LoteConsultaDfeItemStatusEnum> getTodos() {
		List<LoteConsultaDfeItemStatusEnum> lista = new ArrayList<LoteConsultaDfeItemStatusEnum>();
		
		lista.add(DFE_AUTORIZADA);
		lista.add(DFE_DENEGADA);
		lista.add(DFE_CANCELADA);
		
		return lista;
	}
}
