package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoEmissorEnum {

	EMITIDO		       (0, "Emitido"), 
	GERADO		       (1, "Gerado"), 
	PROCESSADO_SUCESSO (2, "Processado com sucesso"),
	PROCESSADO_ERRO	   (3, "Processado com erro");
	
	private Integer value;
	private String descricao;

	private SituacaoEmissorEnum(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public Integer getValue() {
		return value;
	}
	
	public static String listAndConcatenate(List<SituacaoEmissorEnum> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoEmissorEnum bean : SituacaoEmissorEnum.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoEmissorEnum){
					if((situacoes.get(i)).equals(bean)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(bean.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(bean.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}

	public static List<SituacaoEmissorEnum> getTodos() {
		List<SituacaoEmissorEnum> lista = new ArrayList<SituacaoEmissorEnum>();
		
		lista.add(EMITIDO);
		lista.add(GERADO);
		lista.add(PROCESSADO_SUCESSO);
		lista.add(PROCESSADO_ERRO);
		
		return lista;
	}
}
