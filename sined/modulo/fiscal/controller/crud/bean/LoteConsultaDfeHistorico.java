package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_loteconsultadfehistorico", sequenceName = "sq_loteconsultadfehistorico")
@DisplayName("Hist�rico")
public class LoteConsultaDfeHistorico {

	private Integer cdLoteConsultaDfeHistorico;
	private LoteConsultaDfe loteConsultaDfe;
	private SituacaoEmissorEnum situacaoEnum;
	private String observacao;

	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_loteconsultadfehistorico")
	public Integer getCdLoteConsultaDfeHistorico() {
		return cdLoteConsultaDfeHistorico;
	}

	public void setCdLoteConsultaDfeHistorico(Integer cdLoteConsultaDfeHistorico) {
		this.cdLoteConsultaDfeHistorico = cdLoteConsultaDfeHistorico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteconsultadfe")
	public LoteConsultaDfe getLoteConsultaDfe() {
		return loteConsultaDfe;
	}

	public void setLoteConsultaDfe(LoteConsultaDfe loteConsultaDfe) {
		this.loteConsultaDfe = loteConsultaDfe;
	}

	@DisplayName("Situa��o")
	public SituacaoEmissorEnum getSituacaoEnum() {
		return situacaoEnum;
	}
	
	public void setSituacaoEnum(SituacaoEmissorEnum situacaoEnum) {
		this.situacaoEnum = situacaoEnum;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
}