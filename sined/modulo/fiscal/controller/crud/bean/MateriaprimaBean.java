package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Material;

public class MateriaprimaBean {

	private Material material;
	private Double qtde;

	public Material getMaterial() {
		return material;
	}
	public Double getQtde() {
		return qtde;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
}
