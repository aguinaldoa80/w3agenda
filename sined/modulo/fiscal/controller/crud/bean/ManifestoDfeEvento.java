package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_manifestodfeevento", sequenceName = "sq_manifestodfeevento")
@DisplayName("Manifesto DF-e Evento")
public class ManifestoDfeEvento implements Log {

	private Integer cdManifestoDfeEvento;
	private ManifestoDfe manifestoDfe;
	private String chaveAcesso;
	private Cpf cpf;
	private Cnpj cnpj;
	private String coStat;
	private String motivo;
	private String nuProt;
	private TipoEventoNfe tpEvento;
	private String evento;
	private Timestamp dhEvento;
	private Timestamp dhRegEvento;
	private Arquivo arquivoXml;
	private Arquivo arquivoRetornoXml;
	private Configuracaonfe configuracaoNfe;
	private SituacaoEmissorEnum situacaoEnum;
	private Boolean enviado;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	private String hash;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_manifestodfeevento")
	public Integer getCdManifestoDfeEvento() {
		return cdManifestoDfeEvento;
	}

	public void setCdManifestoDfeEvento(Integer cdManifestoDfeEvento) {
		this.cdManifestoDfeEvento = cdManifestoDfeEvento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmanifestodfe")
	public ManifestoDfe getManifestoDfe() {
		return manifestoDfe;
	}

	public void setManifestoDfe(ManifestoDfe manifestoDfe) {
		this.manifestoDfe = manifestoDfe;
	}

	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	public Cpf getCpf() {
		return cpf;
	}
	
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public Cnpj getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	
	@DisplayName("C�digo")
	public String getCoStat() {
		return coStat;
	}
	
	public void setCoStat(String coStat) {
		this.coStat = coStat;
	}
	
	@DisplayName("Motivo")
	public String getMotivo() {
		return motivo;
	}
	
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	public String getNuProt() {
		return nuProt;
	}

	public void setNuProt(String nuProt) {
		this.nuProt = nuProt;
	}

	@DisplayName("Tipo do Evento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tpevento")
	public TipoEventoNfe getTpEvento() {
		return tpEvento;
	}

	public void setTpEvento(TipoEventoNfe tpEvento) {
		this.tpEvento = tpEvento;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public Timestamp getDhEvento() {
		return dhEvento;
	}

	public void setDhEvento(Timestamp dhEvento) {
		this.dhEvento = dhEvento;
	}

	public Timestamp getDhRegEvento() {
		return dhRegEvento;
	}

	public void setDhRegEvento(Timestamp dhRegEvento) {
		this.dhRegEvento = dhRegEvento;
	}

	@DisplayName("Xml Envio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoXml() {
		return arquivoXml;
	}

	public void setArquivoXml(Arquivo arquivoXml) {
		this.arquivoXml = arquivoXml;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaonfe")
	public Configuracaonfe getConfiguracaoNfe() {
		return configuracaoNfe;
	}

	public void setConfiguracaoNfe(Configuracaonfe configuracaoNfe) {
		this.configuracaoNfe = configuracaoNfe;
	}

	public SituacaoEmissorEnum getSituacaoEnum() {
		return situacaoEnum;
	}

	public void setSituacaoEnum(SituacaoEmissorEnum situacaoEnum) {
		this.situacaoEnum = situacaoEnum;
	}

	@DisplayName("Xml Retorno")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornoxml")
	public Arquivo getArquivoRetornoXml() {
		return arquivoRetornoXml;
	}

	public void setArquivoRetornoXml(Arquivo arquivoRetornoXml) {
		this.arquivoRetornoXml = arquivoRetornoXml;
	}

	public Boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
}
