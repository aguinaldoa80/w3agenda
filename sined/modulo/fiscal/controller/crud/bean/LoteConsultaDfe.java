package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.OperacaoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.TipoLoteConsultaDfeEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_loteconsultadfe", sequenceName = "sq_loteconsultadfe")
@DisplayName("Lote Consulta DF-e")
public class LoteConsultaDfe implements Log {

	private Integer cdLoteConsultaDfe;
	private TipoLoteConsultaDfeEnum tipoLoteConsultaDfeEnum;
	private Empresa empresa;
	private Configuracaonfe configuracaoNfe;
	private Integer nsu;
	private String chaveAcesso;
	private OperacaoEnum operacaoEnum;
	private Timestamp dhResp;
	private String coStat;
	private String motivo;
	private SituacaoEmissorEnum situacaoEnum;
	private List<LoteConsultaDfeItem> listaLoteConsultaDfeItem;
	private Timestamp dhEnvio;
	
	private Arquivo arquivoXml;
	private Arquivo arquivoRetornoXml;
	
	private Boolean enviado;
	
	private ManifestoDfe manifestoDfe;
	
	private List<LoteConsultaDfeHistorico> listaLoteConsultaDfeHistorico;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	private String hash;
	private List<LoteConsultaDfeItem> listaLoteConsultaDfeItemNfe;
	private List<LoteConsultaDfeItem> listaLoteConsultaDfeItemEvento;

	public LoteConsultaDfe() {
	}
	
	public LoteConsultaDfe(Integer cdLoteConsultaDfe) {
		this.cdLoteConsultaDfe = cdLoteConsultaDfe;
	}
	
	public LoteConsultaDfe(String chaveAcesso, TipoLoteConsultaDfeEnum tipoLoteConsultaDfeEnum) {
		this.chaveAcesso = chaveAcesso;
		this.tipoLoteConsultaDfeEnum = tipoLoteConsultaDfeEnum;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_loteconsultadfe")
	public Integer getCdLoteConsultaDfe() {
		return cdLoteConsultaDfe;
	}

	public void setCdLoteConsultaDfe(Integer cdLoteConsultaDfe) {
		this.cdLoteConsultaDfe = cdLoteConsultaDfe;
	}

	@Required
	@DisplayName("Tipo do Lote")
	public TipoLoteConsultaDfeEnum getTipoLoteConsultaDfeEnum() {
		return tipoLoteConsultaDfeEnum;
	}

	public void setTipoLoteConsultaDfeEnum(TipoLoteConsultaDfeEnum tipoLoteConsultaDfeEnum) {
		this.tipoLoteConsultaDfeEnum = tipoLoteConsultaDfeEnum;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaonfe")
	public Configuracaonfe getConfiguracaoNfe() {
		return configuracaoNfe;
	}
	
	public void setConfiguracaoNfe(Configuracaonfe configuracaoNfe) {
		this.configuracaoNfe = configuracaoNfe;
	}

	@MaxLength(15)
	@DisplayName("NSU")
	public Integer getNsu() {
		return nsu;
	}

	public void setNsu(Integer nsu) {
		this.nsu = nsu;
	}

	@MaxLength(44)
	@DisplayName("Chave de Acesso")
	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	@DisplayName("Opera��o")
	public OperacaoEnum getOperacaoEnum() {
		return operacaoEnum;
	}

	public void setOperacaoEnum(OperacaoEnum operacaoEnum) {
		this.operacaoEnum = operacaoEnum;
	}

	@DisplayName("Data e Hora de Resposta")
	public Timestamp getDhResp() {
		return dhResp;
	}
	
	public void setDhResp(Timestamp dhResp) {
		this.dhResp = dhResp;
	}

	@DisplayName("Motivo")
	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	@DisplayName("C�digo")
	public String getCoStat() {
		return coStat;
	}
	
	public void setCoStat(String coStat) {
		this.coStat = coStat;
	}

	@DisplayName("Situa��o")
	public SituacaoEmissorEnum getSituacaoEnum() {
		return situacaoEnum;
	}

	public void setSituacaoEnum(SituacaoEmissorEnum situacaoEnum) {
		this.situacaoEnum = situacaoEnum;
	}
	
	@DisplayName("NF-e(s)")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="loteConsultaDfe")
	public List<LoteConsultaDfeItem> getListaLoteConsultaDfeItem() {
		return listaLoteConsultaDfeItem;
	}
	
	public void setListaLoteConsultaDfeItem(List<LoteConsultaDfeItem> listaLoteConsultaDfeItem) {
		this.listaLoteConsultaDfeItem = listaLoteConsultaDfeItem;
	}
	
	@DisplayName("Xml Consulta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoXml() {
		return arquivoXml;
	}
	
	public void setArquivoXml(Arquivo arquivoXml) {
		this.arquivoXml = arquivoXml;
	}
	
	@DisplayName("Xml Retorno")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornoxml")
	public Arquivo getArquivoRetornoXml() {
		return arquivoRetornoXml;
	}
	
	public void setArquivoRetornoXml(Arquivo arquivoRetornoXml) {
		this.arquivoRetornoXml = arquivoRetornoXml;
	}
	
	public Boolean getEnviado() {
		return enviado;
	}
	
	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}
	
	@DisplayName("Manifesto DF-e")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmanifestodfe")
	public ManifestoDfe getManifestoDfe() {
		return manifestoDfe;
	}
	
	public void setManifestoDfe(ManifestoDfe manifestoDfe) {
		this.manifestoDfe = manifestoDfe;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="loteConsultaDfe")
	public List<LoteConsultaDfeHistorico> getListaLoteConsultaDfeHistorico() {
		return listaLoteConsultaDfeHistorico;
	}
	
	public void setListaLoteConsultaDfeHistorico(List<LoteConsultaDfeHistorico> listaLoteConsultaDfeHistorico) {
		this.listaLoteConsultaDfeHistorico = listaLoteConsultaDfeHistorico;
	}
	
	public Timestamp getDhEnvio() {
		return dhEnvio;
	}
	
	public void setDhEnvio(Timestamp dhEnvio) {
		this.dhEnvio = dhEnvio;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	@Transient
	@DisplayName("NF-e")
	public List<LoteConsultaDfeItem> getListaLoteConsultaDfeItemNfe() {
		return listaLoteConsultaDfeItemNfe;
	}
	
	public void setListaLoteConsultaDfeItemNfe(List<LoteConsultaDfeItem> listaLoteConsultaDfeItemNfe) {
		this.listaLoteConsultaDfeItemNfe = listaLoteConsultaDfeItemNfe;
	}
	
	@Transient
	@DisplayName("Evento")
	public List<LoteConsultaDfeItem> getListaLoteConsultaDfeItemEvento() {
		return listaLoteConsultaDfeItemEvento;
	}
	
	public void setListaLoteConsultaDfeItemEvento(List<LoteConsultaDfeItem> listaLoteConsultaDfeItemEvento) {
		this.listaLoteConsultaDfeItemEvento = listaLoteConsultaDfeItemEvento;
	}
}
