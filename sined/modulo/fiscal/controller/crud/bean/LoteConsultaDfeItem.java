package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_loteconsultadfeitem", sequenceName = "sq_loteconsultadfeitem")
@DisplayName("Lote Consulta DF-e Item")
public class LoteConsultaDfeItem implements Log{

	private Integer cdLoteConsultaDfeItem;
	private LoteConsultaDfe loteConsultaDfe;
	private String chaveAcesso;
	private Cpf cpf;
	private Cnpj cnpj;
	private String razaoSocial;
	private String ie;
	private Tipooperacaonota tipoOperacaoNota;
	private Money valorNf;
	private Timestamp dhEmissao;
	private Timestamp dhRecbto;
	private String nuProt;
	private LoteConsultaDfeItemStatusEnum statusEnum;
	private String coStat;
	private String motivo;
	private Arquivo arquivoXml;
	private List<ManifestoDfe> listaManifestoDfe;
	
	private TipoEventoNfe tpEvento;
	private String evento;
	private Timestamp dhEvento;
	private Timestamp dhRegEvento;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	// transient 
	private String linkManifestoDfe;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_loteconsultadfeitem")
	public Integer getCdLoteConsultaDfeItem() {
		return cdLoteConsultaDfeItem;
	}

	public void setCdLoteConsultaDfeItem(Integer cdLoteConsultaDfeItem) {
		this.cdLoteConsultaDfeItem = cdLoteConsultaDfeItem;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteconsultadfe")
	public LoteConsultaDfe getLoteConsultaDfe() {
		return loteConsultaDfe;
	}

	public void setLoteConsultaDfe(LoteConsultaDfe loteConsultaDfe) {
		this.loteConsultaDfe = loteConsultaDfe;
	}

	@DisplayName("Chave de Acesso")
	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	@DisplayName("Raz�o Social")
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	@DisplayName("Inscri��o Estadual")
	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	@DisplayName("Tipo de Opera��o")
	public Tipooperacaonota getTipoOperacaoNota() {
		return tipoOperacaoNota;
	}

	public void setTipoOperacaoNota(Tipooperacaonota tipoOperacaoNota) {
		this.tipoOperacaoNota = tipoOperacaoNota;
	}

	@DisplayName("Valor")
	public Money getValorNf() {
		return valorNf;
	}

	public void setValorNf(Money valorNf) {
		this.valorNf = valorNf;
	}

	@DisplayName("Data e Hora de Emiss�o")
	public Timestamp getDhEmissao() {
		return dhEmissao;
	}
	
	public void setDhEmissao(Timestamp dhEmissao) {
		this.dhEmissao = dhEmissao;
	}
	
	@DisplayName("Data e Hora de Recebimento")
	public Timestamp getDhRecbto() {
		return dhRecbto;
	}

	public void setDhRecbto(Timestamp dhRecbto) {
		this.dhRecbto = dhRecbto;
	}

	@DisplayName("N�mero de Protocolo")
	public String getNuProt() {
		return nuProt;
	}

	public void setNuProt(String nuProt) {
		this.nuProt = nuProt;
	}

	@DisplayName("Status da NF-e")
	public LoteConsultaDfeItemStatusEnum getStatusEnum() {
		return statusEnum;
	}
	
	public void setStatusEnum(LoteConsultaDfeItemStatusEnum statusEnum) {
		this.statusEnum = statusEnum;
	}
	
	public String getCoStat() {
		return coStat;
	}
	
	public void setCoStat(String coStat) {
		this.coStat = coStat;
	}
	
	public String getMotivo() {
		return motivo;
	}
	
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	@DisplayName("Xml")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoXml() {
		return arquivoXml;
	}

	public void setArquivoXml(Arquivo arquivoXml) {
		this.arquivoXml = arquivoXml;
	}
	
	@DisplayName("Data e Hora do Evento")
	public Timestamp getDhEvento() {
		return dhEvento;
	}
	
	public void setDhEvento(Timestamp dhEvento) {
		this.dhEvento = dhEvento;
	}
	
	@DisplayName("Data e Hora de Registro do Evento")
	public Timestamp getDhRegEvento() {
		return dhRegEvento;
	}
	
	public void setDhRegEvento(Timestamp dhRegEvento) {
		this.dhRegEvento = dhRegEvento;
	}
	
	@DisplayName("Evento")
	public String getEvento() {
		return evento;
	}
	
	public void setEvento(String evento) {
		this.evento = evento;
	}
	
	@DisplayName("Tipo do Evento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tpevento")
	public TipoEventoNfe getTpEvento() {
		return tpEvento;
	}
	
	public void setTpEvento(TipoEventoNfe tpEvento) {
		this.tpEvento = tpEvento;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="loteConsultaDfeItem")
	public List<ManifestoDfe> getListaManifestoDfe() {
		return listaManifestoDfe;
	}
	
	public void setListaManifestoDfe(List<ManifestoDfe> listaManifestoDfe) {
		this.listaManifestoDfe = listaManifestoDfe;
	}

	@Transient
	public String getLinkManifestoDfe() {
		if(listaManifestoDfe != null && !listaManifestoDfe.isEmpty()){
			return "<a href=\"javascript:irParaRegistroManifestoDfe(" + listaManifestoDfe.get(0).getCdManifestoDfe() + ");\">" + listaManifestoDfe.get(0).getCdManifestoDfe() + "</a>";
		} else {
			return "";
		}
	}
}