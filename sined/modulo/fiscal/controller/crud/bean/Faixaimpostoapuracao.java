package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Faixaimpostoapuracao {
	
	ISS(0,"ISS"),
	CSLL(5,"CSLL"),
	IR(6,"IR"),
	IMPORTACAO(7,"Importação"),
	INSS(8,"INSS");
	
	private Integer value;
	private String nome;
	
	private Faixaimpostoapuracao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Faixaimpostoapuracao> situacoes){
		StringBuilder codigos = new StringBuilder();
		if(situacoes == null){
			return null;
		}
		for (int i = 0; i < situacoes.size(); i++)
			for (Faixaimpostoapuracao s : Faixaimpostoapuracao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Faixaimpostoapuracao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
