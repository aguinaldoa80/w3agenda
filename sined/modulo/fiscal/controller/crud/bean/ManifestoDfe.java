package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.ManifestoDfeSituacaoEnum;

@Entity
@SequenceGenerator(name = "sq_manifestodfe", sequenceName = "sq_manifestodfe")
@DisplayName("Documento Fiscal")
public class ManifestoDfe {

	private Integer cdManifestoDfe;
	private LoteConsultaDfeItem loteConsultaDfeItem;
	private String chaveAcesso;
	private Cpf cpf;
	private Cnpj cnpj;
	private String razaoSocial;
	private String ie;
	private Tipooperacaonota tipoOperacaoNota;
	private Money valorNf;
	private Timestamp dhEmissao;
	private Timestamp dhRecbto;
	private String nuProt;
	private LoteConsultaDfeItemStatusEnum statusEnum;
	private String coStat;
	private String motivo;
	private List<ManifestoDfeEvento> listaManifestoDfeEvento;
	private List<ManifestoDfeHistorico> listaManifestoDfeHistorico;

	private ManifestoDfeSituacaoEnum manifestoDfeSituacaoEnum;
	private Arquivo arquivoXml;
	private Arquivo arquivoNfeCompletoXml;

	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	private Boolean ultimoEventoPendente;
	private Boolean outros;
	private String numeroNfe;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_manifestodfe")
	public Integer getCdManifestoDfe() {
		return cdManifestoDfe;
	}

	public void setCdManifestoDfe(Integer cdManifestoDfe) {
		this.cdManifestoDfe = cdManifestoDfe;
	}

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteconsultadfeitem")
	@DisplayName("Lote Consulta DF-e Item")
	public LoteConsultaDfeItem getLoteConsultaDfeItem() {
		return loteConsultaDfeItem;
	}

	public void setLoteConsultaDfeItem(LoteConsultaDfeItem loteConsultaDfeItem) {
		this.loteConsultaDfeItem = loteConsultaDfeItem;
	}

	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	public Cpf getCpf() {
		return cpf;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public Cnpj getCnpj() {
		return cnpj;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public Tipooperacaonota getTipoOperacaoNota() {
		return tipoOperacaoNota;
	}

	public void setTipoOperacaoNota(Tipooperacaonota tipoOperacaoNota) {
		this.tipoOperacaoNota = tipoOperacaoNota;
	}

	public Money getValorNf() {
		return valorNf;
	}

	public void setValorNf(Money valorNf) {
		this.valorNf = valorNf;
	}

	@DisplayName("Emiss�o")
	public Timestamp getDhEmissao() {
		return dhEmissao;
	}

	public void setDhEmissao(Timestamp dhEmissao) {
		this.dhEmissao = dhEmissao;
	}

	@DisplayName("Recebimento")
	public Timestamp getDhRecbto() {
		return dhRecbto;
	}

	public void setDhRecbto(Timestamp dhRecbto) {
		this.dhRecbto = dhRecbto;
	}

	public String getNuProt() {
		return nuProt;
	}

	public void setNuProt(String nuProt) {
		this.nuProt = nuProt;
	}

	public LoteConsultaDfeItemStatusEnum getStatusEnum() {
		return statusEnum;
	}

	public void setStatusEnum(LoteConsultaDfeItemStatusEnum statusEnum) {
		this.statusEnum = statusEnum;
	}

	public String getCoStat() {
		return coStat;
	}

	public void setCoStat(String coStat) {
		this.coStat = coStat;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	@DisplayName("Eventos")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="manifestoDfe")
	public List<ManifestoDfeEvento> getListaManifestoDfeEvento() {
		return listaManifestoDfeEvento;
	}
	
	public void setListaManifestoDfeEvento(List<ManifestoDfeEvento> listaManifestoDfeEvento) {
		this.listaManifestoDfeEvento = listaManifestoDfeEvento;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="manifestoDfe")
	public List<ManifestoDfeHistorico> getListaManifestoDfeHistorico() {
		return listaManifestoDfeHistorico;
	}

	public void setListaManifestoDfeHistorico(List<ManifestoDfeHistorico> listaManifestoDfeHistorico) {
		this.listaManifestoDfeHistorico = listaManifestoDfeHistorico;
	}
	
	public ManifestoDfeSituacaoEnum getManifestoDfeSituacaoEnum() {
		return manifestoDfeSituacaoEnum;
	}
	
	public void setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum manifestoDfeSituacaoEnum) {
		this.manifestoDfeSituacaoEnum = manifestoDfeSituacaoEnum;
	}

	@DisplayName("Xml")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoXml() {
		return arquivoXml;
	}

	public void setArquivoXml(Arquivo arquivoXml) {
		this.arquivoXml = arquivoXml;
	}
	
	@DisplayName("Xml Completo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivonfecompletoxml")
	public Arquivo getArquivoNfeCompletoXml() {
		return arquivoNfeCompletoXml;
	}
	
	public void setArquivoNfeCompletoXml(Arquivo arquivoNfeCompletoXml) {
		this.arquivoNfeCompletoXml = arquivoNfeCompletoXml;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Boolean getUltimoEventoPendente() {
		return ultimoEventoPendente;
	}
	
	public void setUltimoEventoPendente(Boolean ultimoEventoPendente) {
		this.ultimoEventoPendente = ultimoEventoPendente;
	}
	
	@Transient
	public Boolean getOutros() {
		return outros;
	}
	
	public void setOutros(Boolean outros) {
		this.outros = outros;
	}
	
	@Transient
	public String getNumeroNfe() {
		return numeroNfe;
	}
	
	public void setNumeroNfe(String numeroNfe) {
		this.numeroNfe = numeroNfe;
	}
	
	@Transient
	@DisplayName("Empresa")
	public Empresa getEmpresaManifesto(){
		if(getLoteConsultaDfeItem() != null && getLoteConsultaDfeItem().getLoteConsultaDfe() != null){
			return getLoteConsultaDfeItem().getLoteConsultaDfe().getEmpresa();
		}
		return null;
	}
}
