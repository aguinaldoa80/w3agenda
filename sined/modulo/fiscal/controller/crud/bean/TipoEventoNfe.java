package br.com.linkcom.sined.modulo.fiscal.controller.crud.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@DisplayName("Tipo de Evento da NF-e")
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name = "sq_tipoeventonfe", sequenceName = "sq_tipoeventonfe")
public class TipoEventoNfe {
	
	public static final TipoEventoNfe CONFIRMACAO_OPERACAO = new TipoEventoNfe(1);
	public static final TipoEventoNfe CIENCIA_OPERACAO = new TipoEventoNfe(2);
	public static final TipoEventoNfe DESCONHECIMENTO_OPERACAO = new TipoEventoNfe(3);
	public static final TipoEventoNfe OPERACAO_NAO_REALIZADA = new TipoEventoNfe(4);
	
	private Integer cdTipoEventoNfe;
	private String codigo;
	private String descricao;

	public TipoEventoNfe() {}
	
	public TipoEventoNfe(Integer cdTipoEventoNfe) {
		this.cdTipoEventoNfe = cdTipoEventoNfe;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipoeventonfe")
	public Integer getCdTipoEventoNfe() {
		return cdTipoEventoNfe;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public void setCdTipoEventoNfe(Integer cdTipoEventoNfe) {
		this.cdTipoEventoNfe = cdTipoEventoNfe;
	}
	
	@Override
	public String toString() {
		return descricao;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoEventoNfe other = (TipoEventoNfe) obj;
		if (cdTipoEventoNfe == null) {
			if (other.cdTipoEventoNfe != null)
				return false;
		} else if (!cdTipoEventoNfe.equals(other.cdTipoEventoNfe))
			return false;
		return true;
	}
}
