package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.SaldoIpi;
import br.com.linkcom.sined.geral.service.SaldoIpiService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SaldoIpiFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/SaldoIpi", authorizationModule=CrudAuthorizationModule.class)
public class SaldoIpiCrud extends CrudControllerSined<SaldoIpiFiltro, SaldoIpi, SaldoIpi> {

private SaldoIpiService saldoIpiService;
	
	public void setSaldoIpiService(SaldoIpiService saldoIpiService) {
		this.saldoIpiService = saldoIpiService;
	}

	protected void validateBean(SaldoIpi bean, BindException errors) {
		SaldoIpi saldoIpiFound = saldoIpiService.getSaldoIpiByMesAnoEmpresaTipoUtilizacao(bean);
		
		if (saldoIpiFound != null) {
			errors.reject("001", "J� existe registro com essa mesma empresa e m�s/ano.");
		}
	}
}
