package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.SaldoIcms;
import br.com.linkcom.sined.geral.service.SaldoIcmsService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SaldoIcmsFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/SaldoIcms", authorizationModule=CrudAuthorizationModule.class)
public class SaldoIcmsCrud extends CrudControllerSined<SaldoIcmsFiltro, SaldoIcms, SaldoIcms> {
	
	private SaldoIcmsService saldoIcmsService;
	
	public void setSaldoIcmsService(SaldoIcmsService saldoIcmsService) {
		this.saldoIcmsService = saldoIcmsService;
	}

	@Override
	protected void validateBean(SaldoIcms bean, BindException errors) {
		SaldoIcms saldoIcmsFound = saldoIcmsService.getSaldoIcmsByMesAnoEmpresaTipoUtilizacaoCredito(bean);
		
		if (saldoIcmsFound != null) {
			errors.reject("001", "J� existe registro com essa mesma empresa e m�s/ano.");
		}
	}
}
