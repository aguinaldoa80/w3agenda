package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Obrigacaoicmsrecolher;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ObrigacaoicmsrecolherFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/Obrigacaoicmsrecolher", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"empresa", "uf", "codobrigacaoicmsrecolher", "valorobrigacao", "dtvencimento", "mesreferencia"})
public class ObrigacaoicmsrecolherCrud extends CrudControllerSined<ObrigacaoicmsrecolherFiltro, Obrigacaoicmsrecolher, Obrigacaoicmsrecolher> {
	
}