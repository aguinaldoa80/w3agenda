package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Spedarquivo;
import br.com.linkcom.sined.geral.bean.enumeration.Indiceatividadesped;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.geral.service.DeclaracaoExportacaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.SpedarquivoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/fiscal/crud/Spedarquivo", authorizationModule=CrudAuthorizationModule.class)
public class SpedarquivoCrud extends CrudControllerSined<SpedarquivoFiltro, Spedarquivo, Spedarquivo> {
	
	private SpedarquivoService spedarquivoService;
	private InventarioService inventarioService;
	private EmpresaService empresaService;
	private DeclaracaoExportacaoService declaracaoExportacaoService;
	
	public void setDeclaracaoExportacaoService(DeclaracaoExportacaoService declaracaoExportacaoService) {
		this.declaracaoExportacaoService = declaracaoExportacaoService;
	}
	public void setSpedarquivoService(SpedarquivoService spedarquivoService) {
		this.spedarquivoService = spedarquivoService;
	}
	public void setInventarioService (InventarioService inventarioService){
		this.inventarioService = inventarioService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Spedarquivo form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Spedarquivo form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Spedarquivo form) throws CrudException {
		throw new SinedException("A��o inv�lida!");
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	
	

	protected void setListagemInfo(WebRequestContext request, SpedarquivoFiltro filtro) throws Exception {
		if (!listagemVaziaPrimeiraVez() || filtro.isNotFirstTime()) {
			if(request.getParameter("resetCurrentPage") != null) {
				filtro.setCurrentPage(0);
			}
			
			ListagemResult<Spedarquivo> listagemResult = getLista(request, filtro);
			List<Spedarquivo> list = listagemResult.list();
			
			// CHAVES DOS ARQUIVOS PARA DOWNLOAD
			for (Spedarquivo spedarquivo : list) {
				if(spedarquivo.getArquivo() != null)
					DownloadFileServlet.addCdfile(request.getSession(), new Long(spedarquivo.getArquivo().getCdarquivo()));
			}
			
			request.setAttribute("lista", list);
			request.setAttribute("currentPage", filtro.getCurrentPage());
			request.setAttribute("numberOfPages", filtro.getNumberOfPages());
			request.setAttribute("filtro", filtro);			
		} else {
			request.setAttribute("lista", new ArrayList<Spedarquivo>());
			request.setAttribute("currentPage", 0);
			request.setAttribute("numberOfPages", 0);
			request.setAttribute("filtro", filtro);
		}
	}
	
	
	
	/**
	 * Action que faz a gera��o do arquivo SPED.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#geraArquivo
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerar(WebRequestContext request, SpedarquivoFiltro filtro){
		try{
			if(Boolean.TRUE.equals(filtro.getGerarRegistroK280())){
				for (Inventario inv1: filtro.getInventarioCorrecao()) {
					inv1 =inventarioService.load(inv1);
					for (Inventario inv2: filtro.getInventarioCorrecao()) {
						if(!inv1.equals(inv2)){
							inv2 =inventarioService.load(inv2);
							if(inv1.getDtinventario().equals(inv2.getDtinventario())){
								request.addError("Existe per�odo duplicado no ajuste de estoque!");
								return continueOnAction("listagem", filtro);
							}
						}
					}
				}
			}
			return new ResourceModelAndView(spedarquivoService.geraArquivo(filtro, request));
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			filtro.setApoio(null);
			filtro.setListaRegistroE200(null);
			return continueOnAction("listagem", filtro);	
		}
		 
	}

	public void ajaxGetListaDeclaracaoExportacao(WebRequestContext request){
		Empresa empresa;
		String cdpessoa = request.getParameter("cdpessoa");
		if (cdpessoa==null || cdpessoa.isEmpty()){
			empresa = null;
		}
		else{
			empresa = new Empresa(Integer.parseInt(cdpessoa));
		}	
		List<DeclaracaoExportacao> listaDeclaracaoExportacao = new ArrayList<DeclaracaoExportacao>();
		listaDeclaracaoExportacao = declaracaoExportacaoService.findByEmpresa(empresa);
		if(listaDeclaracaoExportacao !=null){
			View.getCurrent().println(SinedUtil.convertToJavaScript(listaDeclaracaoExportacao, "listaDeclaracaoExportacao", ""));
		}
	}
	
	public void ajaxGetListaInventario(WebRequestContext request){
		Empresa empresa;
		String cdpessoa = request.getParameter("cdpessoa");

		if (cdpessoa==null || cdpessoa.isEmpty()){
			empresa = null;
		}
		else{
			empresa = new Empresa(Integer.parseInt(cdpessoa));
		}
		
		List<Inventario> todosInventarios = inventarioService.findByEmpresa(empresa);
		List<Inventario> listaInventario = new ArrayList<Inventario>();
		List<Inventario> listaInventarioCorrecao = new ArrayList<Inventario>();
		List<Inventario> listaInventarioEstoque = new ArrayList<Inventario>();	
		
		for (Inventario inventario : todosInventarios) {
			if(inventario.getInventarioTipo().equals(InventarioTipo.ESTOQUEESTRUTURADO)){
				listaInventarioEstoque.add(inventario);
			} else if(inventario.getInventarioTipo().equals(InventarioTipo.CORRECAO)){
				listaInventarioCorrecao.add(inventario);
			} else {
				listaInventario.add(inventario);
			}
		}

		View.getCurrent().println(SinedUtil.convertToJavaScript(listaInventario, "listaInventario", ""));
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaInventarioEstoque, "listaInventarioEstoque", ""));
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaInventarioCorrecao, "listaInventarioCorrecao", ""));
		
	}
	
	public void ajaxCheckUncheckGerarBlocoK(WebRequestContext request){
		Empresa empresa;
		String isAtividadeSpedIndustrial = "false";
		String cdpessoa = request.getParameter("cdpessoa");

		if (cdpessoa==null || cdpessoa.isEmpty()){
			empresa = null;
		}else{
			empresa = new Empresa(Integer.parseInt(cdpessoa));
		}
		
		empresa = empresaService.load(empresa, "empresa.cdpessoa, empresa.indiceatividadesped");
		
		if (empresa.getIndiceatividadesped() != null && empresa.getIndiceatividadesped().equals(Indiceatividadesped.INDUSTRIAL)) {
			isAtividadeSpedIndustrial = "true";
		}
		View.getCurrent().println("var isAtividadeSpedIndustrial = " + isAtividadeSpedIndustrial + ";");
	}
}
