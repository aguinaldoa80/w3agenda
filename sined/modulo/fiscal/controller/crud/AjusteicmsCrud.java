package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Ajusteicms;
import br.com.linkcom.sined.geral.bean.Ajusteicmstipo;
import br.com.linkcom.sined.geral.service.AjusteicmstipoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.AjusteicmsFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/Ajusteicms", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"empresa", "dtfatogerador", "ajusteicmstipo.tipoajuste", "ajusteicmstipo", "uf.sigla", "valor"})
public class AjusteicmsCrud extends CrudControllerSined<AjusteicmsFiltro, Ajusteicms, Ajusteicms> {
	
	private AjusteicmstipoService ajusteicmstipoService;
	
	public void setAjusteicmstipoService(AjusteicmstipoService ajusteicmstipoService) {
		this.ajusteicmstipoService = ajusteicmstipoService;
	}

	public ModelAndView ajaxOnChangeAjusteicmstipo(WebRequestContext request, Ajusteicmstipo ajusteicmstipo){
		if(ajusteicmstipo.getCdajusteicmstipo() != null){
			ajusteicmstipo = ajusteicmstipoService.load(ajusteicmstipo, "ajusteicmstipo.cdajusteicmstipo, ajusteicmstipo.apuracao");
		}
		return new JsonModelAndView().addObject("ajusteicmstipo", ajusteicmstipo);
	}
}