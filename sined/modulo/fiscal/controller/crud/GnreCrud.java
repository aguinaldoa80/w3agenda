package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Gnre;
import br.com.linkcom.sined.geral.bean.Gnrehistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.enumeration.GnreAcaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnreSituacao;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.GnreService;
import br.com.linkcom.sined.geral.service.GnrehistoricoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.GnreFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/Gnre", authorizationModule=CrudAuthorizationModule.class)
public class GnreCrud extends CrudControllerSined<GnreFiltro, Gnre, Gnre>{

	private EmpresaService empresaService;
	private GnreService gnreService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private GnrehistoricoService gnrehistoricoService;
	
	public void setGnrehistoricoService(
			GnrehistoricoService gnrehistoricoService) {
		this.gnrehistoricoService = gnrehistoricoService;
	}
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setGnreService(GnreService gnreService) {
		this.gnreService = gnreService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Gnre form) throws Exception {
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}
	
	@Override
	protected void listagem(WebRequestContext request, GnreFiltro filtro) throws Exception {
		request.setAttribute("listaEmpresa", empresaService.findAtivos());
		
		List<GnreSituacao> listaSituacao = new ArrayList<GnreSituacao>();
		for (int i = 0; i < GnreSituacao.values().length; i++) {
			listaSituacao.add(GnreSituacao.values()[i]);
		}
		request.setAttribute("listaSituacao", listaSituacao);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Gnre bean) throws Exception {
		boolean isCriar = bean.getCdgnre() == null;
		
		super.salvar(request, bean);
		
		Gnrehistorico gnrehistorico = new Gnrehistorico();
		gnrehistorico.setAcao(isCriar ? GnreAcaoEnum.CRIADA : GnreAcaoEnum.ALTERADA);
		gnrehistorico.setGnre(bean);
		gnrehistoricoService.saveOrUpdate(gnrehistorico);
	}
	
	public ModelAndView gerarContapagar(WebRequestContext request){
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item encontrado.");
			return redirectMoreAction(whereIn, entrada);
		}
		
		List<Gnre> listaGnre = gnreService.findForGerarContaAPagar(whereIn);
		for (Gnre gnre : listaGnre) {
			boolean erro = false;
			if(!gnre.getSituacao().equals(GnreSituacao.GERADA)){
				erro = true;
				request.addError("� permitido gerar conta a pagar somente de GNRE(s) na situa��o de Gerada.");
			}
			if(erro){
				return redirectMoreAction(whereIn, entrada);
			}
		}
		
		try {
			gnreService.gerarContapagar(listaGnre);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o de XML da GNRE: " + e.getMessage());
		}
		
		return redirectMoreAction(whereIn, entrada);
	}
	
	private ModelAndView redirectMoreAction(String whereIn, Boolean entrada) {
		if(entrada) return sendRedirectToAction("consultar", "cdgnre=" + whereIn);
		else return sendRedirectToAction("listagem");
	}
	
	public ModelAndView gerarXML(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item encontrado.");
			return redirectMoreAction(whereIn, entrada);
		}
		
		List<Gnre> listaGnre = gnreService.findForXML(whereIn);
		for (Gnre gnre : listaGnre) {
			boolean erro = false;
			if(!gnre.getSituacao().equals(GnreSituacao.EM_ABERTA)){
				erro = true;
				request.addError("� permitido gerar XML somente de GNRE(s) na situa��o de Em aberto.");
			}
			if(erro){
				return redirectMoreAction(whereIn, entrada);
			}
		}
		
		try {
			gnreService.gerarXML(listaGnre);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o de XML da GNRE: " + e.getMessage());
		}
		return redirectMoreAction(whereIn, entrada);
	}
	
	
	public ModelAndView gerarByNota(WebRequestContext request){
		String whereInNota = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereInNota)){
			request.addError("Nenhum item encontrado.");
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto");
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		List<Notafiscalproduto> listaNotafiscalproduto = notafiscalprodutoService.findForGnre(whereInNota);
		boolean erro = false;
		for(Notafiscalproduto notafiscalproduto : listaNotafiscalproduto){
			if(!notafiscalproduto.getNotaStatus().equals(NotaStatus.NFE_EMITIDA) && 
					!notafiscalproduto.getNotaStatus().equals(NotaStatus.NFE_LIQUIDADA)){
				erro = true;
				request.addError("� permitido gerar GNRE somente de nota(s) na situa��o de NF-e Emitida ou NF-e Liquidada. Nota: " + notafiscalproduto.getNumero());
			}
		}
		
		if(erro){
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto" + (entrada ? "?ACAO=consultar&cdNota=" + listaNotafiscalproduto.get(0).getCdNota() : ""));
		}
		
		try {
			int success = gnreService.gerarByNota(listaNotafiscalproduto);
			if(success > 0){
				request.addMessage("Foram geradas " + success + " GNRE(s).");
			} else {
				request.addMessage("N�o foi gerada nenhuma GNRE.", MessageType.WARN);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o de GNRE: " + e.getMessage());
		}
		
		return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto" + (entrada ? "?ACAO=consultar&cdNota=" + listaNotafiscalproduto.get(0).getCdNota() : ""));
	}
	
	public ModelAndView cancelar(WebRequestContext request){
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item encontrado.");
			return redirectMoreAction(whereIn, entrada);
		}
		
		List<Gnre> listaGnre = gnreService.findForCancelar(whereIn);
		for (Gnre gnre : listaGnre) {
			boolean erro = false;
			if(!gnre.getSituacao().equals(GnreSituacao.EM_ABERTA) && !gnre.getSituacao().equals(GnreSituacao.GERADA)){
				erro = true;
				request.addError("� permitido cancelar somente de GNRE(s) na situa��o de Em aberto ou Gerada.");
			}
			if(erro){
				return redirectMoreAction(whereIn, entrada);
			}
		}
		
		try {
			gnreService.cancelar(listaGnre);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro no cancelamento da GNRE: " + e.getMessage());
		}
		return redirectMoreAction(whereIn, entrada);
	}
	
	public ModelAndView estornar(WebRequestContext request){
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item encontrado.");
			return redirectMoreAction(whereIn, entrada);
		}
		
		List<Gnre> listaGnre = gnreService.findForCancelar(whereIn);
		for (Gnre gnre : listaGnre) {
			boolean erro = false;
			if(!gnre.getSituacao().equals(GnreSituacao.CANCELADA) && !gnre.getSituacao().equals(GnreSituacao.GERADA)){
				erro = true;
				request.addError("� permitido estornar somente de GNRE(s) na situa��o de Cancelada ou Gerada.");
			}
			if(erro){
				return redirectMoreAction(whereIn, entrada);
			}
		}
		
		try {
			gnreService.estornar(listaGnre);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro no estorno da GNRE: " + e.getMessage());
		}
		return redirectMoreAction(whereIn, entrada);
	}
	
}
