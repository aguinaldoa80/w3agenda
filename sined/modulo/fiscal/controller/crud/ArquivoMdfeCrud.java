package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.ArquivoMdfeErro;
import br.com.linkcom.sined.geral.bean.enumeration.ArquivoMdfeSituacao;
import br.com.linkcom.sined.geral.service.ArquivoMdfeErroService;
import br.com.linkcom.sined.geral.service.ArquivoMdfeService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ArquivoMdfeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/fiscal/crud/Arquivomdfe", authorizationModule=CrudAuthorizationModule.class)
public class ArquivoMdfeCrud extends CrudControllerSined<ArquivoMdfeFiltro, ArquivoMdfe, ArquivoMdfe> {
	
	private ArquivoMdfeErroService arquivoMdfeErroService;
	private ConfiguracaonfeService configuracaonfeService;
	private ArquivoMdfeService arquivoMdfeService;
	
	public void setArquivoMdfeErroService(ArquivoMdfeErroService arquivoMdfeErroService) {
		this.arquivoMdfeErroService = arquivoMdfeErroService;
	}
	
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	
	public void setArquivoMdfeService(ArquivoMdfeService arquivoMdfeService) {
		this.arquivoMdfeService = arquivoMdfeService;
	}

	@Override
	protected void salvar(WebRequestContext request, ArquivoMdfe bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, ArquivoMdfe bean) throws Exception {
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
		} else super.excluir(request, bean);
	}
	
	@Override
	protected ArquivoMdfe criar(WebRequestContext request, ArquivoMdfe form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void listagem(WebRequestContext request, ArquivoMdfeFiltro filtro) throws Exception {
		List<ArquivoMdfeSituacao> listaSituacao = new ArrayList<ArquivoMdfeSituacao>();
		for (int i = 0; i < ArquivoMdfeSituacao.values().length; i++) {
			listaSituacao.add(ArquivoMdfeSituacao.values()[i]);
		}
		request.setAttribute("listaSituacao", listaSituacao);
		request.setAttribute("listaConfiguracaonfe", configuracaonfeService.findForComboMDFe());
	}
	
	@Override
	protected void entrada(WebRequestContext request, ArquivoMdfe form) throws Exception {
		if(form.getCdArquivoMdfe() != null){
			List<ArquivoMdfeErro> listaErro = arquivoMdfeErroService.findByArquivoMdfe(form);
			if(listaErro != null && listaErro.size() > 0){
				request.setAttribute("listaErro", Boolean.TRUE);
				form.setListaArquivoMdfeErro(listaErro);
			}
		}
	}
	
	@Override
	protected ListagemResult<ArquivoMdfe> getLista(WebRequestContext request, ArquivoMdfeFiltro filtro) {
		ListagemResult<ArquivoMdfe> listagemResult = super.getLista(request, filtro);
		List<ArquivoMdfe> lista = listagemResult.list();
		
		for (ArquivoMdfe ArquivoMdfe : lista) {
			if(ArquivoMdfe != null && ArquivoMdfe.getArquivoxml() != null && ArquivoMdfe.getArquivoxml().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), ArquivoMdfe.getArquivoxml().getCdarquivo());
			}
			if(ArquivoMdfe != null && ArquivoMdfe.getArquivoxmlassinado() != null && ArquivoMdfe.getArquivoxmlassinado().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), ArquivoMdfe.getArquivoxmlassinado().getCdarquivo());
			}
			if(ArquivoMdfe != null && ArquivoMdfe.getArquivoretornoenvio() != null && ArquivoMdfe.getArquivoretornoenvio().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), ArquivoMdfe.getArquivoretornoenvio().getCdarquivo());
			}
			if(ArquivoMdfe != null && ArquivoMdfe.getArquivoretornoconsulta() != null && ArquivoMdfe.getArquivoretornoconsulta().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), ArquivoMdfe.getArquivoretornoconsulta().getCdarquivo());
			}
		}
		
		return listagemResult;
	}
	
	public ModelAndView removerFlag(WebRequestContext request, ArquivoMdfe arquivoMdfe) throws Exception {
		if(arquivoMdfe == null || 
				arquivoMdfe.getCdArquivoMdfe() == null || 
				arquivoMdfe.getFlag() == null || 
				arquivoMdfe.getFlag().equals("")) {
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		if ("consultando".equals(arquivoMdfe.getFlag())) {
			ArquivoMdfe arquivoMdfeFound = arquivoMdfeService.loadForEntrada(arquivoMdfe);
			
			if (arquivoMdfeFound.getArquivoretornoenvio() != null) {
				if (arquivoMdfeFound.getArquivoxmlconsultalote() == null) {
					try {
						arquivoMdfeService.createArquivoXmlConsultaLote(arquivoMdfeFound);
					} catch (Exception e) {
						request.addError("Erro ao tentar gerar xml de consulta.");
					}
				}
				
				arquivoMdfeService.updateSituacao(arquivoMdfeFound, ArquivoMdfeSituacao.ENVIADO);
				arquivoMdfeService.desmarcarArquivoMdfe(arquivoMdfe.getCdArquivoMdfe(), arquivoMdfe.getFlag());
			} else {
				request.addError("Fa�a o envio da MDF-e primeiro.");
			}
		} else {
			request.addError("N�o � poss�vel remover essa flag.");
		}
		
		return new ModelAndView("redirect:/fiscal/crud/Arquivomdfe?ACAO=consultar&cdArquivoMdfe=" + arquivoMdfe.getCdArquivoMdfe());
	}
}
