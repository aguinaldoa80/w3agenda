package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.ProcessoReferenciado;
import br.com.linkcom.sined.geral.service.AcaoJudicialNaturezaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntregadocumentoService;
import br.com.linkcom.sined.geral.service.EntregamaterialService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoitemService;

import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ProcessoReferenciadoFiltro;

import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/Processoreferenciado", authorizationModule=CrudAuthorizationModule.class)
public class ProcessoReferenciadoCrud extends CrudControllerSined<ProcessoReferenciadoFiltro, ProcessoReferenciado, ProcessoReferenciado>{
	
	private EmpresaService empresaService;
	private AcaoJudicialNaturezaService acaoJudicialNaturezaService;
	private NotafiscalprodutoService notaFiscalProdutoService;
	private EntregadocumentoService entregaDocumentoService;
	private MaterialService materialService;
	private NotafiscalprodutoitemService notafiscalProdutoItemService;
	private EntregamaterialService entregaMaterialService;
	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setAcaoJudicialNaturezaService(AcaoJudicialNaturezaService acaoJudicialNaturezaService) {this.acaoJudicialNaturezaService = acaoJudicialNaturezaService;}
	public void setNotaFiscalProdutoService(NotafiscalprodutoService notaFiscalProdutoService) {this.notaFiscalProdutoService = notaFiscalProdutoService;}
	public void setEntregaDocumentoService(EntregadocumentoService entregaDocumentoService) {this.entregaDocumentoService = entregaDocumentoService;} 
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setNotafiscalProdutoItemService(NotafiscalprodutoitemService notafiscalProdutoItemService) {this.notafiscalProdutoItemService = notafiscalProdutoItemService;}
	public void setEntregaMaterialService(EntregamaterialService entregaMaterialService) {this.entregaMaterialService = entregaMaterialService;}
	
	
	@Override
	protected void entrada(WebRequestContext request, ProcessoReferenciado form)
			throws Exception {
		request.setAttribute("listaempresa",empresaService.findAtivos());
		request.setAttribute("listaAcaoJudicialNatureza", acaoJudicialNaturezaService.findAll());

	}
	
	
	public ModelAndView ajaxPreencheDataValorOperacao(WebRequestContext request){
		JsonModelAndView json = new JsonModelAndView(); 
		Notafiscalproduto nota;
		String cdNota=request.getParameter("cdNotaFiscal");
		if (cdNota==null || cdNota.isEmpty()){
			nota = null;
		}
		else{
			nota = new Notafiscalproduto(Integer.parseInt(cdNota));
		}
		Entregadocumento entregaDocumento;
		String cdEntradaFiscal = request.getParameter("cdEntradaFiscal");
		if (cdEntradaFiscal==null || cdEntradaFiscal.isEmpty()){
			entregaDocumento = null;
		}
		else{
			entregaDocumento = new Entregadocumento(Integer.parseInt(cdEntradaFiscal));
		}
		if(nota != null){
			Notafiscalproduto notaFiscalProduto = notaFiscalProdutoService.load(nota);
			System.out.println(notaFiscalProduto.getDtEmissao());
			json.addObject("data", notaFiscalProduto.getDtEmissao());
			json.addObject("valor", notaFiscalProduto.getValor());
		}
		if(entregaDocumento !=null){
			Entregadocumento ed = entregaDocumentoService.load(entregaDocumento);
			json.addObject("dataEntradaFiscal", ed.getDtemissao());
			json.addObject("valorEntradaFiscal", ed.getValor());
		}
		return json;	
	}
	public ModelAndView ajaxPreencheBcVlPisCofins(WebRequestContext request){
		JsonModelAndView json = new JsonModelAndView(); 
		Notafiscalproduto nota;
		String cdNota=request.getParameter("cdNotaFiscal");
		if (cdNota==null || cdNota.isEmpty()){
			nota = null;
		}
		else{
			nota = new Notafiscalproduto(Integer.parseInt(cdNota));
		}
		Entregadocumento entregaDocumento;
		String cdEntradaFiscal = request.getParameter("cdEntradaFiscal");
		if (cdEntradaFiscal==null || cdEntradaFiscal.isEmpty()){
			entregaDocumento = null;
		}
		else{
			entregaDocumento = new Entregadocumento(Integer.parseInt(cdEntradaFiscal));
		}
		
		Material material;
		String cdMaterial = request.getParameter("material");
		if (cdMaterial==null || cdMaterial.isEmpty()){
			material = null;
		}
		else{
			material = new Material(Integer.parseInt(cdMaterial));
		}
		if(nota != null && material != null){
			Notafiscalprodutoitem nfpi = notafiscalProdutoItemService.findByMaterialNotaFiscal(nota, material);
		//	System.out.println(notaFiscalProduto.getDtEmissao());
			json.addObject("vlBcPis", nfpi.getValorbcpis());
			json.addObject("valorPis", nfpi.getPis());
			json.addObject("vlBcCofins", nfpi.getValorbccofins());
			json.addObject("valorCofins", nfpi.getCofins());
			json.addObject("qtde", nfpi.getQtde());
		}
		if(entregaDocumento !=null && material != null){
			Entregamaterial em = entregaMaterialService.findByMaterialNotaFiscal(entregaDocumento, material);
			json.addObject("vlBcPisEntrada", em.getValorbcpis());
			json.addObject("valorPisEntrada", em.getPis());
			json.addObject("vlBcCofinsEntrada", em.getValorbccofins());
			json.addObject("valorCofinsEntrada", em.getCofins());
			json.addObject("qtdeEntrada", em.getQtde());
		}
		return json;	
	}
}
