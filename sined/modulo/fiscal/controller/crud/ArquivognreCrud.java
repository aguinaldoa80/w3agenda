package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Arquivognre;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/fiscal/crud/Arquivognre", authorizationModule=CrudAuthorizationModule.class)
public class ArquivognreCrud extends CrudControllerSined<FiltroListagemSined, Arquivognre, Arquivognre> {
	
	@Override
	protected void salvar(WebRequestContext request, Arquivognre bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Arquivognre bean) throws Exception {
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
		} else super.excluir(request, bean);
	}
	
	@Override
	protected Arquivognre criar(WebRequestContext request, Arquivognre form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected ListagemResult<Arquivognre> getLista(WebRequestContext request, FiltroListagemSined filtro) {
		ListagemResult<Arquivognre> listagemResult = super.getLista(request, filtro);
		List<Arquivognre> lista = listagemResult.list();
		
		for (Arquivognre arquivognre : lista) {
			if(arquivognre != null && arquivognre.getArquivoxml() != null && arquivognre.getArquivoxml().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivognre.getArquivoxml().getCdarquivo());
			}
		}
		
		return listagemResult;
	}
	
}
