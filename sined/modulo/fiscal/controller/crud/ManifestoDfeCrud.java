package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.ManifestoDfeEventoService;
import br.com.linkcom.sined.geral.service.ManifestoDfeHistoricoService;
import br.com.linkcom.sined.geral.service.ManifestoDfeService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.SelecionarConfiguracaoBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeEvento;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.TipoEventoNfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.ManifestoDfeSituacaoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ManifestoDfeFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/ManifestoDfe", authorizationModule=CrudAuthorizationModule.class)
public class ManifestoDfeCrud extends CrudControllerSined<ManifestoDfeFiltro, ManifestoDfe, ManifestoDfe> {

	private ConfiguracaonfeService configuracaoNfeService;
	private ManifestoDfeService manifestoDfeService;
	private ManifestoDfeEventoService manifestoDfeEventoService;
	private ManifestoDfeHistoricoService manifestoDfeHistoricoService;
	private EntradafiscalService entradaFiscalService;
	
	public void setConfiguracaoNfeService(ConfiguracaonfeService configuracaoNfeService) {
		this.configuracaoNfeService = configuracaoNfeService;
	}
	public void setManifestoDfeService(ManifestoDfeService manifestoDfeService) {
		this.manifestoDfeService = manifestoDfeService;
	}
	public void setManifestoDfeEventoService(ManifestoDfeEventoService manifestoDfeEventoService) {
		this.manifestoDfeEventoService = manifestoDfeEventoService;
	}
	public void setManifestoDfeHistoricoService(ManifestoDfeHistoricoService manifestoDfeHistoricoService) {
		this.manifestoDfeHistoricoService = manifestoDfeHistoricoService;
	}
	public void setEntradaFiscalService(EntradafiscalService entradaFiscalService) {
		this.entradaFiscalService = entradaFiscalService;
	}
	
	@Override
	protected ListagemResult<ManifestoDfe> getLista(WebRequestContext request,	ManifestoDfeFiltro filtro) {
		ListagemResult<ManifestoDfe> lista = super.getLista(request, filtro);
		List<ManifestoDfe> list = lista.list();
		
		if (list != null && list.size() > 0) {
			for (ManifestoDfe manifestoDfe : list) {
				List<ManifestoDfeEvento> listaManifestoDfeEvento = manifestoDfeEventoService.procurarPorManifestoDfe(manifestoDfe);
				
				if (listaManifestoDfeEvento != null && listaManifestoDfeEvento.size() > 0 && listaManifestoDfeEvento.get(0) != null && 
						(!SituacaoEmissorEnum.PROCESSADO_SUCESSO.equals(listaManifestoDfeEvento.get(0).getSituacaoEnum()) && 
						!SituacaoEmissorEnum.PROCESSADO_ERRO.equals(listaManifestoDfeEvento.get(0).getSituacaoEnum())) &&
						(TipoEventoNfe.CIENCIA_OPERACAO.equals(listaManifestoDfeEvento.get(0).getTpEvento()) ||
						TipoEventoNfe.CONFIRMACAO_OPERACAO.equals(listaManifestoDfeEvento.get(0).getTpEvento()) ||
						TipoEventoNfe.DESCONHECIMENTO_OPERACAO.equals(listaManifestoDfeEvento.get(0).getTpEvento()) ||
						TipoEventoNfe.OPERACAO_NAO_REALIZADA.equals(listaManifestoDfeEvento.get(0).getTpEvento()))) {
					manifestoDfe.setUltimoEventoPendente(Boolean.TRUE);
				}
				
				List<Entregadocumento> listaEntregaDocumento = entradaFiscalService.findByChaveacesso(manifestoDfe.getChaveAcesso());
				Entregadocumento entregaDocumento = null;
				
				if (listaEntregaDocumento != null && listaEntregaDocumento.size() > 0) {
					entregaDocumento = listaEntregaDocumento.get(0);
				}
				
				if (LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA.equals(manifestoDfe.getStatusEnum()) && 
						entregaDocumento != null && !Entregadocumentosituacao.CANCELADA.equals(entregaDocumento.getEntregadocumentosituacao())) {
					manifestoDfe.setOutros(Boolean.TRUE);
				}
				
				if (LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA.equals(manifestoDfe.getStatusEnum()) && 
						(ManifestoDfeSituacaoEnum.CIENCIA.equals(manifestoDfe.getManifestoDfeSituacaoEnum()) || ManifestoDfeSituacaoEnum.CONFIRMACAO.equals(manifestoDfe.getManifestoDfeSituacaoEnum())) &&
						(entregaDocumento == null || Entregadocumentosituacao.CANCELADA.equals(entregaDocumento.getEntregadocumentosituacao()))) {
					manifestoDfe.setOutros(Boolean.FALSE);
				}
				
				if (StringUtils.isNotEmpty(manifestoDfe.getChaveAcesso())) {
					manifestoDfe.setNumeroNfe(manifestoDfe.getChaveAcesso().substring(25, 34));
				}
			}
		}
		
		return lista;
	}
	
	@Override
	protected void listagem(WebRequestContext request, ManifestoDfeFiltro filtro) throws Exception {
		request.setAttribute("listaStatusEnum", LoteConsultaDfeItemStatusEnum.getTodos());
		request.setAttribute("listaManifestoDfeSituacaoEnum", ManifestoDfeSituacaoEnum.getTodos());
	}
	
	@Override
	protected void entrada(WebRequestContext request, ManifestoDfe form) throws Exception {
		if (form.getCdManifestoDfe() != null) {
			List<ManifestoDfeHistorico> listaManifestoDfeHistorico = manifestoDfeHistoricoService.procurarPorManifestoDfe(form);
			
			if (listaManifestoDfeHistorico != null && listaManifestoDfeHistorico.size() > 0) {
				form.setListaManifestoDfeHistorico(listaManifestoDfeHistorico);
			} else {
				form.setListaManifestoDfeHistorico(null);
			}
			
			List<ManifestoDfeEvento> listaManifestoDfeEvento = manifestoDfeEventoService.procurarPorManifestoDfe(form);
			Boolean ultimoEventoPendente = Boolean.FALSE;
			
			if (listaManifestoDfeEvento != null && listaManifestoDfeEvento.size() > 0) {
				form.setListaManifestoDfeEvento(listaManifestoDfeEvento);
				
				if (listaManifestoDfeEvento.get(0) != null && 
						(!SituacaoEmissorEnum.PROCESSADO_SUCESSO.equals(listaManifestoDfeEvento.get(0).getSituacaoEnum()) &&
						!SituacaoEmissorEnum.PROCESSADO_ERRO.equals(listaManifestoDfeEvento.get(0).getSituacaoEnum())) && 
						(TipoEventoNfe.CIENCIA_OPERACAO.equals(listaManifestoDfeEvento.get(0).getTpEvento()) ||
						TipoEventoNfe.CONFIRMACAO_OPERACAO.equals(listaManifestoDfeEvento.get(0).getTpEvento()) ||
						TipoEventoNfe.DESCONHECIMENTO_OPERACAO.equals(listaManifestoDfeEvento.get(0).getTpEvento()) ||
						TipoEventoNfe.OPERACAO_NAO_REALIZADA.equals(listaManifestoDfeEvento.get(0).getTpEvento()))) {
					ultimoEventoPendente = Boolean.TRUE;
				}
			} else {
				form.setListaManifestoDfeEvento(null);
			}
			
			form.setUltimoEventoPendente(ultimoEventoPendente);
		}
		
		super.entrada(request, form);
	}
	
	public ModelAndView selecionarConfiguracaoManifestoDfe(WebRequestContext request){
		String selectedItens = SinedUtil.getItensSelecionados(request);
		
		if (manifestoDfeService.existeMaisDeUmaEmpresaDosManifestoDfe(selectedItens)) {
			request.addError("Selecione apenas manifesto de DF-e de uma empresa.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<ManifestoDfeSituacaoEnum> listaManifestoDfeSituacaoEnum = new ArrayList<ManifestoDfeSituacaoEnum>();
		listaManifestoDfeSituacaoEnum.add(ManifestoDfeSituacaoEnum.SEM_MANIFESTO);
		listaManifestoDfeSituacaoEnum.add(ManifestoDfeSituacaoEnum.DESCONHECIMENTO);
		
		if (!manifestoDfeService.existeManifestoDfeSituacaoEnumDiferenteDe(selectedItens, listaManifestoDfeSituacaoEnum)) {
			request.addError("Selecione apenas manifesto concluídos.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		try {
			String[] cdManifestoDfe = selectedItens.split(",");
			
			ManifestoDfe manifestoDfe = manifestoDfeService.procurarEmpresaPorId(Integer.parseInt(cdManifestoDfe[0]));
			Empresa empresa = manifestoDfe.getLoteConsultaDfeItem().getLoteConsultaDfe().getEmpresa();

			boolean havePadrao = configuracaoNfeService.havePadrao(null, Tipoconfiguracaonfe.DDFE, empresa);
			
			SelecionarConfiguracaoBean bean = new SelecionarConfiguracaoBean();
			bean.setWhereIn(selectedItens);
			bean.setEmpresa(empresa);
			
			if(!havePadrao){
				List<Configuracaonfe> listaConfiguracao = configuracaoNfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.DDFE, empresa);
				request.setAttribute("listaConfiguracao", listaConfiguracao);
				
				return new ModelAndView("direct:/crud/popup/selecionarConfiguracaoDde", "bean", bean);
			}
			
			Configuracaonfe configuracaonfePadrao = configuracaoNfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.DDFE, empresa);
			bean.setConfiguracaonfe(configuracaonfePadrao);
			
			return continueOnAction("criarLoteConsultaDfe", bean);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	public void criarLoteConsultaDfe(WebRequestContext request, SelecionarConfiguracaoBean bean) {
		String selectedItens = bean.getWhereIn();
		
		if (StringUtils.isEmpty(selectedItens)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/fiscal/crud/ManifestoDfe");
		}
		
		try {
			manifestoDfeService.criarLoteConsultaDfe(bean.getEmpresa(), bean.getConfiguracaonfe(), selectedItens, false);
			
			request.addMessage("Arquivo(s) de lote de consulta de DF-e criado(s) com sucesso.");
		} catch (Exception e) {
			request.addMessage("Erro ao criar arquivo(s) de lote de consulta de DF-e. " + e.getMessage());
		}
		SinedUtil.redirecionamento(request, "/fiscal/crud/LoteConsultaDfe");
	}
	
	public ModelAndView downloadXmlCompleto(WebRequestContext request) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/fiscal/crud/ManifestoDfe");
			return null;
		}
		
		try {
			return manifestoDfeService.downloadDfeXmlCompleto(request, whereIn);
		} catch (Exception e) {
			request.addError("Erro ao fazer download de Dfe: " + e.getMessage());
			SinedUtil.redirecionamento(request, "/fiscal/crud/ManifestoDfe");
			return null;
		}
	}
}
