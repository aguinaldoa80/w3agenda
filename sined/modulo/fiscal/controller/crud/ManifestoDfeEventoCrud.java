package br.com.linkcom.sined.modulo.fiscal.controller.crud;

import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ManifestoDfeEventoService;
import br.com.linkcom.sined.geral.service.ManifestoDfeHistoricoService;
import br.com.linkcom.sined.geral.service.TipoEventoNfeService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.SelecionarConfiguracaoBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeEvento;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.TipoEventoNfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ManifestoDfeEventoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/fiscal/crud/ManifestoDfeEvento")
public class ManifestoDfeEventoCrud extends CrudControllerSined<ManifestoDfeEventoFiltro, ManifestoDfeEvento, ManifestoDfeEvento> {

	private ManifestoDfeEventoService manifestoDfeEventoService;
	private ManifestoDfeHistoricoService manifestoDfeHistoricoService;
	private EmpresaService empresaService;
	private ConfiguracaonfeService configuracaoNfeService;
	private ArquivoService arquivoService;
	private TipoEventoNfeService tipoEventoNfeService;
	
	public void setManifestoDfeEventoService(ManifestoDfeEventoService manifestoDfeEventoService) {
		this.manifestoDfeEventoService = manifestoDfeEventoService;
	}
	
	public void setManifestoDfeHistoricoService(ManifestoDfeHistoricoService manifestoDfeHistoricoService) {
		this.manifestoDfeHistoricoService = manifestoDfeHistoricoService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setConfiguracaoNfeService(ConfiguracaonfeService configuracaoNfeService) {
		this.configuracaoNfeService = configuracaoNfeService;
	}
	
	public void setTipoEventoNfeService(TipoEventoNfeService tipoEventoNfeService) {
		this.tipoEventoNfeService = tipoEventoNfeService;
	}
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public ModelAndView selecionarConfiguracaoManifestoDfeEvento(WebRequestContext request) {
		String selectedItens = SinedUtil.getItensSelecionados(request);
		String operacao = request.getParameter("operacao");
		
		String erro = manifestoDfeEventoService.validarManifestoDfe(selectedItens, operacao);
		if (StringUtils.isNotEmpty(erro)) {
			request.addError(erro);
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		try {
			String[] cdManifestoDfeEvento = selectedItens.split(",");
			
			ManifestoDfe manifestoDfe = manifestoDfeEventoService.procurarEmpresaPorIdManifestoDfe(Integer.parseInt(cdManifestoDfeEvento[0]));
			Empresa empresa = manifestoDfe.getLoteConsultaDfeItem().getLoteConsultaDfe().getEmpresa();

			boolean havePadrao = configuracaoNfeService.havePadrao(null, Tipoconfiguracaonfe.MD, empresa);
			
			SelecionarConfiguracaoBean bean = new SelecionarConfiguracaoBean();
			bean.setWhereIn(selectedItens);
			bean.setEmpresa(empresa);
			bean.setOperacao(operacao);
			
			if(!havePadrao){
				List<Configuracaonfe> listaConfiguracao = configuracaoNfeService.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.MD, empresa);
				request.setAttribute("listaConfiguracao", listaConfiguracao);
				
				return new ModelAndView("direct:/crud/popup/selecionarConfiguracaoNfe", "bean", bean);
			}
			
			Configuracaonfe configuracaonfePadrao = configuracaoNfeService.getConfiguracaoPadrao(Tipoconfiguracaonfe.MD, empresa);
			bean.setConfiguracaonfe(configuracaonfePadrao);
			
			return continueOnAction("criarManifestoDfe", bean);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
	
	public void criarManifestoDfe(WebRequestContext request, SelecionarConfiguracaoBean bean) {
		String selectedItens = bean.getWhereIn();
		bean.setEmpresa(empresaService.carregaEmpresa(bean.getEmpresa()));
		bean.setConfiguracaonfe(configuracaoNfeService.carregaConfiguracao(bean.getConfiguracaonfe()));
		
		if (StringUtils.isEmpty(selectedItens)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/fiscal/crud/ManifestoDfe");
		}
		
		if (bean.getOperacao() == null) {
			request.addError("Opera��o n�o selecionada.");
			SinedUtil.redirecionamento(request, "/fiscal/crud/ManifestoDfe");
		}
		
		String[] ids = selectedItens.split(",");
		
		for (String id : ids) {
			ManifestoDfe manifestoDfe = manifestoDfeEventoService.procurarManifestoDfePorId(Integer.parseInt(id));
			
			if (manifestoDfe != null) {
				ManifestoDfeEvento manifestoDfeEvento = new ManifestoDfeEvento();
				manifestoDfeEvento.setManifestoDfe(manifestoDfe);
				manifestoDfeEvento.setCnpj(manifestoDfe.getCnpj());
				manifestoDfeEvento.setCpf(manifestoDfe.getCpf());
				manifestoDfeEvento.setChaveAcesso(manifestoDfe.getChaveAcesso());
				
				TipoEventoNfe tipoEventoNfe = tipoEventoNfeService.procurarPorCodigo(bean.getOperacao());
				manifestoDfeEvento.setTpEvento(tipoEventoNfe);
				
				String xml = manifestoDfeEventoService.criarEventoManifestacaoDfe(manifestoDfeEvento, bean.getEmpresa(), bean.getConfiguracaonfe());
				Arquivo arquivoXml = new Arquivo(xml.getBytes(), "manifestoDfe_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				
				manifestoDfeEvento.setArquivoXml(arquivoXml);
				manifestoDfeEvento.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
				manifestoDfeEvento.setConfiguracaoNfe(bean.getConfiguracaonfe());
				
				arquivoService.saveFile(manifestoDfeEvento, "arquivoXml");
				arquivoService.saveOrUpdate(arquivoXml);
				
				manifestoDfeEventoService.saveOrUpdate(manifestoDfeEvento);
				
				ManifestoDfeHistorico manifestoDfeHistorico = new ManifestoDfeHistorico();
				Usuario usuario = SinedUtil.getUsuarioLogado();
	            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
	            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				manifestoDfeHistorico.setManifestoDfe(manifestoDfeEvento.getManifestoDfe());
				manifestoDfeHistorico.setTipoEventoNfe(manifestoDfeEvento.getTpEvento());
				manifestoDfeHistorico.setObservacao("Gerado o lote de " + manifestoDfeEvento.getTpEvento().getDescricao() + " da Manifesto DF-e.");
				manifestoDfeHistorico.setSituacaoEnum(manifestoDfeEvento.getSituacaoEnum());
				
				manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistorico);
			}
		}
		
		request.addMessage("Arquivo do evento do manifesto DF-e criado(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/fiscal/crud/ManifestoDfe");
	}
}
