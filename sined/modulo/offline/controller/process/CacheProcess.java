package br.com.linkcom.sined.modulo.offline.controller.process;

import java.util.Date;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cacheusuariooffline;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.CacheusuarioofflineService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;

@Controller(path="/offline/process/Cache")
public class CacheProcess extends MultiActionController{
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		return new ModelAndView("direct:/process/cache");
	}
	
	public ModelAndView forcarAtualizacaoCache(WebRequestContext request){
		ParametrogeralService.getInstance().updateValorPorNome(Parametrogeral.OFFLINE_DT_MODIFICACAO_ARQUIVOS, new Date().getTime()+"");
		return new JsonModelAndView().addObject("error", false)
		.addObject("msg", "O cache ser� atualizado na pr�xima vez que o usu�rio acessar o W3ERP.");
	}
	
	public void forcarAtualizacaoEntidadeOffline(WebRequestContext request){
		ParametrogeralService.getInstance().updateValorPorNome(Parametrogeral.OFFLINE_DT_MODIFICACAO_ENTIDADES, new Date().getTime()+"");
	}
	
	public ModelAndView atualizarCache(WebRequestContext request){
		CacheusuarioofflineService.getInstance().saveOrUpdate(new Cacheusuariooffline());
		return new JsonModelAndView().addObject("error", false)
			.addObject("msg", "N�o foi poss�vel atualizar o cache.");
	}
}
