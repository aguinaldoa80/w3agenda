package br.com.linkcom.sined.modulo.offline.controller.crud;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendaoffline;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoPendencia;
import br.com.linkcom.sined.geral.service.CacheusuarioofflineService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.PedidovendamaterialService;
import br.com.linkcom.sined.geral.service.PedidovendaofflineService;
import br.com.linkcom.sined.geral.service.PedidovendapagamentoService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoitemService;
import br.com.linkcom.sined.geral.service.RestricaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoItemBean;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.OfflineUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.offline.ClienteOfflineJSON;
import br.com.linkcom.sined.util.offline.EnderecoOfflineJSON;
import br.com.linkcom.sined.wms.WmsWebServiceStub;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ConfirmarExclusaoItemPedido;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ConfirmarExclusaoItemPedidoResponse;
import br.com.linkcom.sined.wms.WmsWebServiceStub.MarcarItemPedido;
import br.com.linkcom.sined.wms.WmsWebServiceStub.MarcarItemPedidoResponse;

@Controller(path="/offline/crud/Pedidovenda")
public class PedidovendaModuloOfflineCrud extends CrudControllerSined<PedidovendaFiltro, Pedidovenda, Pedidovenda>{

	private EmpresaService empresaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private ParametrogeralService parametrogeralService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private PedidovendaService pedidovendaService;
	private MaterialService materialService;
	private PedidovendapagamentoService pedidovendapagamentoService;
	private FornecedorService fornecedorService;
	private PrazopagamentoService prazopagamentoService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private EnderecoService enderecoService;
	private ColaboradorService colaboradorService;
	private PedidovendaofflineService pedidovendaofflineService;
	private PedidovendatipoService pedidovendatipoService;
	private RestricaoService restricaoService;
	private ClienteService clienteService;
	
	public void setPedidovendapagamentoService(PedidovendapagamentoService pedidovendapagamentoService) {this.pedidovendapagamentoService = pedidovendapagamentoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {this.pedidovendahistoricoService = pedidovendahistoricoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setPedidovendaofflineService(PedidovendaofflineService pedidovendaofflineService) {this.pedidovendaofflineService = pedidovendaofflineService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setRestricaoService(RestricaoService restricaoService) {this.restricaoService = restricaoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	
	@Override
	public GenericService<Pedidovenda> getGenericService() {
		return pedidovendaService;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Pedidovenda form) throws Exception {		
		if(form.getCdpedidovenda() == null){
			form.setEmpresa(empresaService.loadPrincipal());
			form.setColaborador(SinedUtil.getUsuarioComoColaborador());
			form.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
			
			Boolean isWms = empresaService.isIntegracaoWms(form.getEmpresa());
			if(isWms != null && isWms)
				request.setAttribute("WMS_INTEGRACAO", "TRUE");
			else
				request.setAttribute("WMS_INTEGRACAO", "FALSE");
			request.setAttribute("bonificacaoPedidovenda", Boolean.FALSE);
		}else {
		
			if (form.getEmpresa() == null){
				Empresa empresa = empresaService.loadPrincipal();
				form.setEmpresa(empresa);
			} 
			
			boolean showEditarLink = false;
			if (form.getCdpedidovenda() != null) {
				if(form.getPedidovendasituacao() != null){
					showEditarLink = form.getPedidovendasituacao().equals(Pedidovendasituacao.PREVISTA);
				}
				form.setListaPedidovendahistorico(pedidovendahistoricoService.findByPedidovenda(form));
				form.setListaPedidovendapagamento(pedidovendapagamentoService.findByPedidovenda(form));
			}
			request.setAttribute("showEditarLink", showEditarLink);
			
			Boolean isWms = empresaService.isIntegracaoWms(form.getEmpresa());
			if(isWms != null && isWms)
				request.getSession().setAttribute("integracao", true);
			else
				request.getSession().setAttribute("integracao", false);
			
			if(isWms != null && isWms)
				request.setAttribute("WMS_INTEGRACAO", "TRUE");
			else
				request.setAttribute("WMS_INTEGRACAO", "FALSE");
			
			
			if(form.getPedidovendatipo() != null && 
					form.getPedidovendatipo().getBonificacao() != null && 
					form.getPedidovendatipo().getBonificacao()){
				request.setAttribute("bonificacaoPedidovenda", Boolean.TRUE);
			} else {
				request.setAttribute("bonificacaoPedidovenda", Boolean.FALSE);
			}
		}
		
		List<Endereco> listaEnderecoByCliente = new ArrayList<Endereco>();
		if(form.getCliente() != null && form.getCliente().getCdpessoa() != null)
			listaEnderecoByCliente = enderecoService.findByCliente(form.getCliente());
		request.setAttribute("listaEnderecoByCliente", listaEnderecoByCliente);
		Boolean prazomedio = Boolean.FALSE;
		if(form.getPrazomedio() != null && form.getPrazomedio()){
			prazomedio = Boolean.TRUE;
		}
		List<Prazopagamento> listaPrazopagamento = prazopagamentoService.findByPrazomedio(prazomedio);
		request.setAttribute("listaPrazopagamento", listaPrazopagamento);
			
			
		request.setAttribute("listaFornecedorTransportadora", fornecedorService.findFornecedoresTransportadora(null));
		request.setAttribute("VENDASALDOPORMINMAX", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX));
		request.setAttribute("VENDASALDOPRODUTO", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTO));
		request.setAttribute("VENDASALDOPRODUTONEGATIVO", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTONEGATIVO));
		request.setAttribute("DESCONSIDERARDESCONTOSALDOPRODUTO", parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO));
		request.setAttribute("TABELA_VENDA_CLIENTE", parametrogeralService.getValorPorNome(Parametrogeral.TABELA_VENDA_CLIENTE));
	}
	
	@Override
	protected Pedidovenda criar(WebRequestContext request, Pedidovenda form) throws Exception {
		request.setAttribute("REALIZARPEDIDOVENDA", Boolean.TRUE);
		request.setAttribute("listaArquivosEntities" , OfflineUtil.getOfflineFiles());
		return super.criar(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Pedidovenda bean) throws Exception {
		List<Pedidovendamaterial> listaVendamaterialForCriarTabela = pedidovendaService.getListaPedidovendamaterialForCriarTabela(bean);
		
		//se der erro salvar agora o pedidovendaoffline
		boolean bonificacao = false;
		boolean sincronizarComWMS = false;
		boolean restricaoCliente = false;
		boolean requeraprovacaopedido = false;
		boolean prazomaiorprazotabela = pedidovendaService.isPrazomaiorprazotabela(bean);
		Presencacompradornfe presencacompradornfe = null;
		
		bean.setDtsincronizacaooffline(new Timestamp(System.currentTimeMillis())); 
		try{			
			Pedidovendatipo pedidovendatipo = bean.getPedidovendatipo();
			if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
				pedidovendatipo = pedidovendatipoService.load(pedidovendatipo);
				if(pedidovendatipo.getBonificacao() != null && pedidovendatipo.getBonificacao()){
					bonificacao = true;
				}
				if(pedidovendatipo.getSincronizarComWMS() != null && pedidovendatipo.getSincronizarComWMS()){
					sincronizarComWMS = true;
				}
				if(pedidovendatipo.getRequeraprovacaopedido() != null){
					requeraprovacaopedido = pedidovendatipo.getRequeraprovacaopedido();
				}
				presencacompradornfe = pedidovendatipo.getPresencacompradornfe();
			}
			
			if(presencacompradornfe == null && bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
				Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.presencacompradornfe");
				if(empresa != null){
					presencacompradornfe = empresa.getPresencacompradornfe();
				}
			}
			if(presencacompradornfe == null){
				presencacompradornfe = Presencacompradornfe.PRESENCIAL;
			}
			bean.setPresencacompradornfe(presencacompradornfe);
			
			if(!bonificacao){
				restricaoCliente = restricaoService.verificaRestricaoSemDtLiberacao(bean.getCliente());
			}
			
			if(bonificacao || restricaoCliente || requeraprovacaopedido || prazomaiorprazotabela){
				bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
			}
			
			pedidovendaService.saveOrUpdate(bean);			
			
		} catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_PEDIDOVENDA_PROTOCOLOOFFLINE")){
				return;//se tiver alguma chave igual no banco, n�o vai retornar nenhum msg ao usuario, apenas verifica se a chave � igual para n�o haver duplicidade				
			}else{
				throw e;
			}
		}catch(Exception e){
//			System.out.println("Erro ao sincronizar pedido de venda offline.");
			e.printStackTrace();
			Pedidovendaoffline pedidovendaoffline = new Pedidovendaoffline();
			pedidovendaoffline.setJson(View.convertToJson(bean).toString());
			pedidovendaoffline.setMsgErro(e.getMessage());
			pedidovendaoffline.setDtcriacaooffline(bean.getDtsincronizacaooffline());
			pedidovendaoffline.setProtocolooffline(bean.getProtocolooffline());
			
			pedidovendaofflineService.saveOrUpdate(pedidovendaoffline);
		}
		
		try {
			
			if(prazomaiorprazotabela){
				bean.setObservacao("Aguardando aprova��o devido a mudan�a no prazo de pagamento. " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
			}
			
			this.salvaPedidoHistorico(bean);
			
			if(sincronizarComWMS){
				if(empresaService.isIntegracaoWms(bean.getEmpresa())){
					pedidovendaService.integracaoWMS(bean, "Cria��o do pedido de venda offline. Qtde de itens = " + bean.getListaPedidovendamaterial().size(), false);
				}
			}
			
			pedidovendaService.verificarAndCriarTabelaprecoCliente(request, bean, listaVendamaterialForCriarTabela);
		} catch (Exception e) {}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Pedidovenda form) throws CrudException {
		throw new SinedException("N�o suportado");
	}
	
	@Override
	protected void listagem(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		List<Pedidovendasituacao> listaPedidoVendasituacao = new ArrayList<Pedidovendasituacao>();
		
		Pedidovendasituacao[] situacoes = Pedidovendasituacao.values();
		for (int i = 0; i < situacoes.length; i++) {
			listaPedidoVendasituacao.add(situacoes[i]);
		}
		request.setAttribute("listaPedidoVendasituacao", listaPedidoVendasituacao);
		
		request.setAttribute("listaPedidoVendasituacao", listaPedidoVendasituacao);
		
		
		Boolean isWms = empresaService.isIntegracaoWms(empresaService.loadPrincipal());
		if(isWms != null && isWms)
			request.getSession().setAttribute("integracao", true);
		else
			request.getSession().setAttribute("integracao", false);
		request.setAttribute("listaArquivosEntities" , OfflineUtil.getOfflineFiles());
	}
	
	@Override
	protected ListagemResult<Pedidovenda> getLista(WebRequestContext request, PedidovendaFiltro filtro) {
		return new ListagemResult<Pedidovenda>(null, filtro);
	}
	
	@Override
	protected void setListagemInfo(WebRequestContext request, PedidovendaFiltro filtro) throws Exception {
		super.setListagemInfo(request, filtro);
		List<Pedidovenda> pvs = new ArrayList<Pedidovenda>();
		pvs.add(new Pedidovenda());
		request.setAttribute("lista", pvs);
	}

	
	/**
	 *	Action que faz a confirma��o do pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView confirmar(WebRequestContext request){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		if(pedidovendaService.havePedidovendaSituacao(itens, Pedidovendasituacao.CANCELADO, Pedidovendasituacao.CONFIRMADO)){
			request.addError("Pedido(s) de venda com situa��o(�es) diferente de 'PREVISTA' e 'CONFIRMADO PARCIALMENTE'.");
			return sendRedirectToAction("listagem");
		}
		
		return new ModelAndView("redirect:/faturamento/process/RealizarVenda?orcamento=false&cdpedidovenda=" + itens);
	}
	
	/**
	 *	Action que faz o cancelamento do pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cancelar(WebRequestContext request){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		boolean erro = false;
		try{
			List<Pedidovendamaterial> lista = pedidovendamaterialService.findByPedidoVenda(itens);
			List<Pedidovendamaterial> listaIntegracao = pedidovendaService.getListaIntegracaoWms(lista);
			if (listaIntegracao != null && !listaIntegracao.isEmpty()){
				WmsWebServiceStub stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(empresaService.loadPrincipal()) + "/webservices/WmsWebService");
				
				String itensmaterial = CollectionsUtil.listAndConcatenate(listaIntegracao, "cdpedidovendamaterial", ",");
				
				MarcarItemPedido marcarItemPedido = new MarcarItemPedido();
				marcarItemPedido.setIn0(itensmaterial);
				
				MarcarItemPedidoResponse marcarItemPedidoResponse = stub.marcarItemPedido(marcarItemPedido);
				if(!marcarItemPedidoResponse.getOut()){
					erro = true;
				} else {
					ConfirmarExclusaoItemPedido confirmarExclusaoItemPedido = new ConfirmarExclusaoItemPedido();
					confirmarExclusaoItemPedido.setIn0(itensmaterial);
					
					ConfirmarExclusaoItemPedidoResponse confirmarExclusaoItemPedidoResponse = stub.confirmarExclusaoItemPedido(confirmarExclusaoItemPedido);
					if(!confirmarExclusaoItemPedidoResponse.getOut()){
						erro = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(erro){
			request.addError("N�o � poss�vel cancelar este(s) registro(s).");
			return sendRedirectToAction("listagem");
		}
		
		
		List<Pedidovenda> listaPedidovenda = pedidovendaService.findForCancelamento(itens);
		
		for (Pedidovenda pedidovenda : listaPedidovenda) {
			if(!pedidovenda.getPedidovendasituacao().equals(Pedidovendasituacao.PREVISTA)){
				request.addError("Pedido de venda com situa��o diferente de 'PREVISTA'.");
				return sendRedirectToAction("listagem");
			}
		}
		
		pedidovendaService.updateCancelamento(itens);
		Pedidovendahistorico pedidovendahistorico;
		for (Pedidovenda pedidovenda : listaPedidovenda) {
			pedidovendahistorico = new Pedidovendahistorico();
			pedidovendahistorico.setPedidovenda(pedidovenda);
			pedidovendahistorico.setAcao("Cancelado");
			pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		}
		
		request.addMessage("Pedido(s) de venda cancelado(s) com sucesso.");
		return sendRedirectToAction("listagem");
	}
	
	public void ajaxMaterial(WebRequestContext request, Material material ){
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		try {
			material = materialService.getUnidademedidaValorvenda(material);
			
			if (material.getUnidademedida() != null)
				view.println("var unidademedida = '" + material.getUnidademedida().getNome() + "';");
			else
				view.println("var unidademedida = '';");
			
			if (material.getValorvenda() != null)
				view.println("var valorvenda = '" + material.getValorvenda() + "';");
			else
				view.println("var valorvenda = '';");
			
			view.println("var sucesso = true;");						
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}
	
	
	/************ M�todos do Process ***************/
	
	/** 
	 * M�todo que salva o Pedido de Venda.
	 * 
	 * @author Thiago Augusto
	 * @param request
	 * @param pedidoVenda
	 * @return
	 */
	public ModelAndView comprar(WebRequestContext request, Pedidovenda pedidoVenda){
		Colaborador colaborador = colaboradorService.findColaboradorusuario(SinedUtil.getUsuarioLogado().getCdpessoa());
		if(colaborador == null || colaborador.getCdpessoa() == null){
			request.addError("Aten��o! Este usu�rio n�o � colaborador.");
			return new ModelAndView("redirect:/faturamento/crud/Pedidovenda");
		}
		if(validaContagerencialCentrocusto(request, pedidoVenda) && validaListaPedidovendamaterial(request, pedidoVenda)){
			if (pedidoVenda.getListaPedidovendamaterial().size() > 0 && !pedidoVenda.getListaPedidovendamaterial().isEmpty()) {
				pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
				
				pedidovendaService.setOrdemItem(pedidoVenda);
				
				pedidovendaService.saveOrUpdate(pedidoVenda);
				this.salvaPedidoHistorico(pedidoVenda);
				
				Boolean isWms = empresaService.isIntegracaoWms(pedidoVenda.getEmpresa());
				if(isWms != null && isWms)
					pedidovendaService.integracaoWMS(pedidoVenda, "Cria��o do pedido de venda offline. Qtde de itens = " + pedidoVenda.getListaPedidovendamaterial().size(), false);
				
				request.addMessage("Pedido de Venda cadastrado com sucesso.");
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println(
						"<html><body>" +
						"<script>window.open('../../faturamento/relatorio/Pedidovenda?ACAO=gerar&cdpedidovenda="+pedidoVenda.getCdpedidovenda() + "');" +
						"window.location = 'Pedidovenda?ACAO=criar';" +
						"</script>" +
						"</body></html>"
						);
			} else {
				request.addError("� necess�rio ter pelo menos um produto para efetuar o pedido.");
				return continueOnAction("entrada", pedidoVenda);
			}
			return null;
		}else {
			return continueOnAction("entrada", pedidoVenda);
		}
	}
	
	private Boolean validaListaPedidovendamaterial(WebRequestContext request, Pedidovenda pedidoVenda) {
		if(pedidoVenda.getListaPedidovendamaterial() == null || pedidoVenda.getListaPedidovendamaterial().isEmpty()){
			request.addError("Nenhum produto encontrado no pedido de venda.");
			return Boolean.FALSE;
		}
		
		for(Pedidovendamaterial pedidovendamaterial : pedidoVenda.getListaPedidovendamaterial()){
			if(pedidovendamaterial.getDtprazoentrega() == null){
				request.addError("O campo prazo de entrega da lista de produtos � obrigat�rio.");
				return Boolean.FALSE;
			}
		}
		
		return Boolean.TRUE;
	}
	/**
	 * M�todo para verificar a conta gerencial e centro custo no material
	 * 
	 * @param request
	 * @param pedidovenda
	 * @return
	 */
	private Boolean validaContagerencialCentrocusto(WebRequestContext request, Pedidovenda pedidovenda){
		List<Pedidovendamaterial> listapedidovendamaterial = pedidovenda.getListaPedidovendamaterial();
		Empresa empresa = empresaService.loadComContagerencialCentrocusto(pedidovenda.getEmpresa());
		List<String> listError = new ArrayList<String>();
		
		for (Pedidovendamaterial pedidovendamaterial : listapedidovendamaterial) {
			Material material = materialService.load(pedidovendamaterial.getMaterial());
			
			Contagerencial  cg = pedidovendamaterial.getMaterial().getContagerencialvenda() == null ?  empresa.getContagerencial() : pedidovendamaterial.getMaterial().getContagerencialvenda();
			Centrocusto cc = pedidovendamaterial.getMaterial().getCentrocustovenda() == null ?  empresa.getCentrocusto() : pedidovendamaterial.getMaterial().getCentrocustovenda();
			
			if(cc == null){
				listError.add("Informar o centro de custo de venda no material " + material.getCdmaterial() + " ou informar no cadastro de empresa o centro de custo de venda.");
			}
			
			if(cg == null){
				listError.add("Informar a conta gerencial de venda no material " + material.getCdmaterial() + " ou informar no cadastro de empresa a conta gerencial de venda.");
			}	
	
			if(listError.size() > 0){
				for (String string : listError) {
					request.addError(string);
				}
				return false;
			}
		}
		return true;
	}
	
	/**
	 * M�todo que salva o PedidoHistorico
	 * 
	 * @author Thiago Augusto
	 * @param pedidoVenda
	 */
	public void salvaPedidoHistorico(Pedidovenda pedidovenda){
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setObservacao(pedidovenda.getObservacao());
		pedidovendahistorico.setAcao("Criado (Offline)");
		
		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
	}
	
	public ModelAndView pagamento(WebRequestContext request, Pedidovenda pedidovenda){	
		
		List<Prazopagamentoitem> listaPagItem = prazopagamentoitemService.findByPrazo(pedidovenda.getPrazopagamento());
		Double juros = prazopagamentoService.loadAll(pedidovenda.getPrazopagamento()).getJuros();
		
		Prazopagamento pg = prazopagamentoService.load(pedidovenda.getPrazopagamento());
		Boolean isValorminimo = Boolean.TRUE;
		if(pg.getPrazomedio() != null && pg.getPrazomedio() && pg.getValorminimo() != null){
			if(pedidovenda.getValor() < pg.getValorminimo().getValue().doubleValue())
				isValorminimo = Boolean.FALSE;			
		}
		
		AjaxRealizarVendaPagamentoBean bean = new AjaxRealizarVendaPagamentoBean();
		if(pg.getPrazomedio() != null && pg.getPrazomedio())
			bean.setParcela(pedidovenda.getQtdeParcelas());
		else
			bean.setParcela(listaPagItem.size());
		
		AjaxRealizarVendaPagamentoItemBean item;
		Money valor;
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Date dtparcela;
		
		List<AjaxRealizarVendaPagamentoItemBean> lista = new ArrayList<AjaxRealizarVendaPagamentoItemBean>();
		
		if(pg.getPrazomedio() != null && pg.getPrazomedio()){
			
			Prazopagamentoitem it = listaPagItem.get(0);
			Date dtvencimentoAnterior = pedidovenda.getDtpedidovenda();
			String dtPrimeiraEntrega = request.getParameter("dtprazoentrega");
			if(dtPrimeiraEntrega != null && !"".equals(dtPrimeiraEntrega)){
				try {
					dtvencimentoAnterior = SinedDateUtils.stringToDate(dtPrimeiraEntrega);
				} catch (ParseException e) {}
				
			}
			
			if(it.getDias() != null){
				if(bean.getParcela() % 2 != 0){
					Integer meio = (bean.getParcela()/2) + (bean.getParcela() % 2);
					Integer dias = it.getDias();
					Date dtParcelaMeio = SinedDateUtils.incrementDate(new java.sql.Date(pedidovenda.getDtpedidovenda().getTime()), dias, Calendar.DAY_OF_MONTH); 
					Integer intervaloParcealas = SinedDateUtils.diferencaDias(dtParcelaMeio, pedidovenda.getDtpedidovenda()) / (bean.getParcela() / 2);
					for(int i = 0; i < bean.getParcela(); i++){
						item = new AjaxRealizarVendaPagamentoItemBean();
						
						if(juros == null || juros.equals(0d)){			  
							valor = new Money(pedidovenda.getValor()).divide(new Money(bean.getParcela())); 
						}else{
							valor = calculaJuros(new Money(pedidovenda.getValor()), bean.getParcela(), juros);
						}
						item.setValor(valor.toString());
						
						if( (meio-1) == i){
							dtparcela = dtParcelaMeio;
						}else if(i == 0){
							dtparcela = dtvencimentoAnterior;
						}else if(i < (meio - 1)){
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), -intervaloParcealas, Calendar.DAY_OF_MONTH);
						}else {
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), intervaloParcealas, Calendar.DAY_OF_MONTH);
						}
						
						item.setDtparcela(formatador.format(dtparcela));
						if(i != 0)
							dtvencimentoAnterior = dtparcela;
						else
							dtvencimentoAnterior = dtParcelaMeio;
						lista.add(item);
					}
				}else {					
					Integer intervaloParcealas = it.getDias() * 2 / bean.getParcela();
					for(int i = 0; i < bean.getParcela(); i++){
						item = new AjaxRealizarVendaPagamentoItemBean();
						
						if(juros == null || juros.equals(0d)){			  
							valor = new Money(pedidovenda.getValor()).divide(new Money(bean.getParcela())); 
						}else{
							valor = calculaJuros(new Money(pedidovenda.getValor()), bean.getParcela(), juros);
						}
						item.setValor(valor.toString());
						
						if(i == 0)
							dtparcela = dtvencimentoAnterior;
						else
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dtvencimentoAnterior.getTime()), intervaloParcealas, Calendar.DAY_OF_MONTH);
						
						item.setDtparcela(formatador.format(dtparcela));
						dtvencimentoAnterior = dtparcela;						
						lista.add(item);
					}
				}
			}			
		}else{
			for (Prazopagamentoitem it : listaPagItem) {
				item = new AjaxRealizarVendaPagamentoItemBean();
				
				if(juros == null || juros.equals(0d)){			  
					valor = new Money(pedidovenda.getValor()).divide(new Money(bean.getParcela())); 
				}else{
					valor = calculaJuros(new Money(pedidovenda.getValor()), bean.getParcela(), juros);
				}
				item.setValor(valor.toString());
				
				
				if(it.getDias() != null && it.getDias() > 0){
					dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(pedidovenda.getDtpedidovenda().getTime()), it.getDias().intValue(), Calendar.DAY_OF_MONTH);
				} else if(it.getMeses() != null && it.getMeses() > 0){
					dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(pedidovenda.getDtpedidovenda().getTime()), it.getMeses().intValue(), Calendar.MONTH);
				} else {
					dtparcela = pedidovenda.getDtpedidovenda();
				}
				item.setDtparcela(formatador.format(dtparcela));
				
				lista.add(item);
			}
		}
		
		bean.setLista(lista);
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", bean);
		json.addObject("isValorminimo", isValorminimo);
		
		return json;
	}
	
	private Money calculaJuros(Money totalvenda, Integer parcela, Double juros){
		Double total = totalvenda.getValue().doubleValue();
		
		juros = juros/100.00; 
		Double valor = total*juros*Math.pow((1+juros),parcela)/(Math.pow((1+juros),parcela)-1);
		
		Money resultado = new Money(valor);
		return resultado;
	}
	
	/**
	 * M�todo ajax para buscar prazopagamento de acordo com o par�metro
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaPrazopagamento(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		Boolean prazomedio = Boolean.valueOf( request.getParameter("prazomedio") != null ? request.getParameter("prazomedio") : "false");
		
		List<Prazopagamento> listaPrazopagamento = prazopagamentoService.findByPrazomedio(prazomedio);
		
		view.println(SinedUtil.convertToJavaScript(listaPrazopagamento, "listaprazopagamento", ""));		
	}
	
	public void buscarMaterialAJAX(WebRequestContext request, Venda venda){	
		Material material = materialService.loadWithCodFlex(Integer.parseInt(venda.getCodigo()));
		venda.setMaterial(material);
		Double entrada = 0d;
		if (venda.getMaterial() != null){
			entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(venda.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), null);
			if (entrada == null){
				entrada = 0d;
			}
		}
		Double saida = 0d;
		if (venda.getMaterial() != null){
			saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(venda.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), null);
			if (saida == null){
				saida = 0d;
			}
		}
		Double qtddisponivel = SinedUtil.getQtdeEntradaSubtractSaida(entrada,saida);
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		if(venda.getMaterial() != null){			
			Double valorvenda = venda.getMaterial().getValorvenda();
//			String qtde = request.getParameter("qtde");
//			if (qtde != null){
//				//Double qtdevendida = Double.parseDouble(qtde);
//				Double qtdevendida = Double.parseDouble(qtde);
//				if (venda.getMaterial().getProducao() != null && venda.getMaterial().getProducao()){ 
//					//valorvenda = materialService.calculaPrecoVenda(material, qtdevendida);
//				}
//			}
			
			view.println("var valorproduto = '" + valorvenda + "';");
			view.println("var valorminimo = '" + venda.getMaterial().getValorvendaminimo() + "';");
			view.println("var valormaximo = '" + venda.getMaterial().getValorvendamaximo() + "';");
			view.println("var qtddisponivel = '" + qtddisponivel + "';");
			Boolean validaestoque = !((venda.getMaterial().getServico() != null && venda.getMaterial().getServico()) || (venda.getMaterial().getProducao() != null && venda.getMaterial().getProducao()));
			view.println("var validaestoque ='" + validaestoque + "';");
			view.println("var producao ='" + (venda.getMaterial().getProducao() == null ? false : venda.getMaterial().getProducao()) + "';");
		}
	}
	
	public boolean validaTotalPedidoVenda(Pedidovenda pedidovenda, int size){	
		
		Double juros = prazopagamentoService.loadAll(pedidovenda.getPrazopagamento()).getJuros();		
		
		Money frete = pedidovenda.getValorfrete() != null ? pedidovenda.getValorfrete() : new Money();
		Money valor;
		Money bean;
		Double vpValores = 0.0;
		Double vmValores = 0.0;
		Money vp_valores_money = new Money();
		
		if(juros == null || juros.equals(0d)){			  
			valor = new Money(pedidovenda.getTotalvenda()).divide(new Money(size));
			
			for(Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()){
				vp_valores_money = new Money(vp_valores_money.add(pedidovendapagamento.getValororiginal()));				
			}
			
			if((""+vp_valores_money).equals(""+(pedidovenda.getTotalvenda().add(frete)))){
				return true;
			}else return false;
		}else{
			valor = calculaJuros(new Money(pedidovenda.getTotalvenda()), size, juros);
			valor = new Money(valor.getValue().doubleValue() * size);
		}
		
		for(Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()){
			vpValores += pedidovendapagamento.getValororiginal().getValue().doubleValue();
		}
		for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
			Double descontoMat = pedidovendamaterial.getDesconto() != null ? pedidovendamaterial.getDesconto().getValue().doubleValue() : 0.0;
			vmValores += (pedidovendamaterial.getPreco() * pedidovendamaterial.getQuantidade()) - descontoMat;
		}
		bean = new Money( vmValores + (pedidovenda.getValorfrete() != null ? pedidovenda.getValorfrete().getValue().doubleValue() : 0));
		
				
		if(!(bean.getValue().doubleValue() == pedidovenda.getTotalvenda().getValue().doubleValue())){
			return false;		
		}else {
			if((""+vpValores).equals(""+valor.getValue())){
				return false;
			}
		}
		
		return true;
	}
	
	public ModelAndView popupGridMaterial(WebRequestContext request){
		return new ModelAndView("direct:crud/popupGridMaterial");
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Pedidovenda form) throws CrudException {
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("error", false)
		.addObject("msg", "Pedido de venda sincronizado com sucesso.");
		
		List<ClienteOfflineJSON> listaClienteOfflineJSON = new ArrayList<ClienteOfflineJSON>();
		List<EnderecoOfflineJSON> listaEnderecoOfflineJSON = new ArrayList<EnderecoOfflineJSON>();
		
		Pedidovenda bean = null;
		try {
			bean = formToBean(form);
			//salvando e tratando os dados de cliente
			if(bean.getListaClienteOffline()!=null && !bean.getListaClienteOffline().isEmpty()){
				for(Cliente c : bean.getListaClienteOffline()){
					//salvando os enderecos
					if(c.getListaEndereco() != null){
						for(Endereco end : c.getListaEndereco()){
							Integer cdenderecoOld = end.getCdendereco();
							if(cdenderecoOld>1000000){//Se � transiente (convencionado que qualquer valor a partir de 1000000 � de transiente)
								end.setCdendereco(null);
								if(bean.getEndereco().getCdendereco().equals(cdenderecoOld)){//se o endere�o definido na tela � um dos endere�os transientes
									enderecoService.saveOrUpdate(end);
									bean.setEndereco(end);
									
									listaEnderecoOfflineJSON.add(new EnderecoOfflineJSON(cdenderecoOld, end.getCdendereco()));
								}
							}else{
								enderecoService.saveOrUpdate(end);
							}
						}
					}
					
					if(c.getCdpessoa()>1000000){
						Integer cdpessoaOld = c.getCdpessoa();
						c.setCdpessoa(null);
						try {
							ClienteService.getInstance().saveOrUpdate(c);//FIXME salvar sem updateManaged
							listaClienteOfflineJSON.add(new ClienteOfflineJSON(c.getCdpessoa(), cdpessoaOld));
						} catch(DataIntegrityViolationException e){
							if(DatabaseError.isKeyPresent(e, "IDX_PESSOA_CNPJ")){
								c = clienteService.findByCnpj(c.getCnpj());
								if(c == null) throw e;
								listaClienteOfflineJSON.add(new ClienteOfflineJSON(c.getCdpessoa(), cdpessoaOld));
							}else if(DatabaseError.isKeyPresent(e, "IDX_PESSOA_CPF")){
								c = clienteService.findByCpf(c.getCpf());
								if(c == null) throw e;
								listaClienteOfflineJSON.add(new ClienteOfflineJSON(c.getCdpessoa(), cdpessoaOld));
							}else {
								throw e;
							}
						}
						if(bean.getCliente().getCdpessoa().equals(cdpessoaOld))
							bean.setCliente(c);
					}
						
				}
			}
			salvar(request, bean);
		} catch (Exception e) {
			//pedidovendaService.updateMsgErro(form, e.getMessage());
			json.addObject("error", true)
			.addObject("msg", e.getMessage());
			e.printStackTrace();
		}
		
		if(SinedUtil.isListNotEmpty(listaEnderecoOfflineJSON)){
			json.addObject("listaEnderecoUpdate", listaEnderecoOfflineJSON);
		}
		if(SinedUtil.isListNotEmpty(listaClienteOfflineJSON)){
			json.addObject("listaClienteUpdate", listaClienteOfflineJSON);
		}
		return json;
	}
	
	public ModelAndView buscaUltimaAlteracaoEntidades(WebRequestContext request){
		Long ultimaAtualizacaoEntidadeOffline = ParametrogeralService.getInstance().getLong(Parametrogeral.OFFLINE_DT_MODIFICACAO_ARQUIVOS);
		Long ultimoCacheEntidadeOffline = ParametrogeralService.getInstance().getLong(Parametrogeral.OFFLINE_DT_MODIFICACAO_ENTIDADES) ;
		Timestamp timestampUsuario = CacheusuarioofflineService.getInstance().getDtalteraByUsuario(SinedUtil.getUsuarioLogado().getCdpessoa());
		
		if(timestampUsuario != null)
			ultimaAtualizacaoEntidadeOffline = timestampUsuario.getTime();
		
//		System.out.println("Data ultima atualiza��o do usuario: " + new Timestamp(ultimaAtualizacaoEntidadeOffline));
//		System.out.println("Data ultimo cache quartz: " + new Timestamp(ultimoCacheEntidadeOffline));
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("ultimaAtualizacaoEntidadeOffline", (ultimaAtualizacaoEntidadeOffline != null ? ultimaAtualizacaoEntidadeOffline : ""));
		json.addObject("ultimoCacheEntidadeOffline", (ultimoCacheEntidadeOffline != null ? ultimoCacheEntidadeOffline : ""));
		return json;
	}
	
	public void printInfPvs(WebRequestContext request){
		String pvsoffline = request.getParameter("pvsOfflineUsuario");
//		System.out.println("pvs offline: " + pvsoffline != null ? pvsoffline : "nenhum pedido informado.");
		
		if(pvsoffline != null && request.getParameter("enviarEmail") != null &&
				"true".equals(request.getParameter("enviarEmail"))){
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				email.setFrom("w3erp@linkcom.com.br");
				email.setSubject("Dados do Offline");
				email.setTo("luiz.silva@linkcom.com.br");
				email.addHtmlText("Base: " + SinedUtil.getUrlWithContext() + " <br> " + pvsoffline);
				email.sendMessage();
			} catch (Exception e) {}
		}
	}
}
