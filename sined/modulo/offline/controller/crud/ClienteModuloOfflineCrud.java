package br.com.linkcom.sined.modulo.offline.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.util.OfflineUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/offline/crud/Cliente")
public class ClienteModuloOfflineCrud extends CrudControllerSined<FiltroListagem, Cliente, Cliente> {

	private ClienteService clienteService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@Override
	public GenericService<Cliente> getGenericService() {
		return clienteService;
	}

	@Override
	public ModelAndView doEditar(WebRequestContext request, Cliente form) throws CrudException {
		throw new SinedException("Opera��o n�o permitida");
	}

	@Override
	public ModelAndView doExcluir(WebRequestContext request, Cliente form) throws CrudException {
		throw new SinedException("Opera��o n�o permitida");
	}

	@Override
	public ModelAndView doSalvar(WebRequestContext request, Cliente form)	throws CrudException {
		throw new SinedException("Opera��o n�o permitida");
	}

	@Override
	protected Cliente criar(WebRequestContext request, Cliente form) throws Exception {
		request.setAttribute("listaArquivosEntities" , OfflineUtil.getOfflineFiles());
		return super.criar(request, form);
	}
	
	
	
}
