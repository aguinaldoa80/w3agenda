package br.com.linkcom.sined.modulo.help.controller.process;

import java.io.IOException;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
		path="/sistema/process/Suporte"
)
public class SuporteProcess extends MultiActionController {
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, FiltroListagemSined filtro) {
		
		request.getServletResponse().setContentType("text/html");
		StringBuilder sb = new StringBuilder();
		sb.append("<script language=\"Javascript\">");
		if (request.getSession().getAttribute("SUPORTE_CHAT") != null && request.getSession().getAttribute("SUPORTE_CHAT").toString().toUpperCase().equals("TRUE")) {
			sb.append("javascript:void(window.open('http://suporte.linkcom.com.br/livezilla.php','','width=600,height=600,left=0,top=0,resizable=yes,menubar=no,location=yes,status=yes,scrollbars=yes'));");							
		}
		else {
			sb.append("javascript:alert('N�o dispon�vel');");							
		}
		sb.append("window.close();");
		sb.append("</script>");
		
		try {
			request.getServletResponse().getWriter().println(sb.toString());
		} 
		catch (IOException e) {
			e.printStackTrace();
		}			

		return null;		
	}
}
