package br.com.linkcom.sined.modulo.juridico.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.PartecontrariaService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.PartecontrariaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/juridico/relatorio/Partecontraria", authorizationModule=ReportAuthorizationModule.class)
public class PartecontrariaReport extends SinedReport<PartecontrariaFiltro> {
	
	private PartecontrariaService partecontrariaService;
	
	public void setPartecontrariaService(PartecontrariaService partecontrariaService) {
		this.partecontrariaService = partecontrariaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, PartecontrariaFiltro filtro) throws Exception {
		return partecontrariaService.gerarRelatorioListagem(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "ParteContraria";
	}

	@Override
	public String getTitulo(PartecontrariaFiltro filtro) {
		return "Parte Contrária";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, PartecontrariaFiltro filtro) {
	}
}