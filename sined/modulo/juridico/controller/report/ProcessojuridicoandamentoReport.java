package br.com.linkcom.sined.modulo.juridico.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.ProcessojuridicoandamentoService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoandamentoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/juridico/relatorio/Processojuridicoandamento", authorizationModule=ReportAuthorizationModule.class)
public class ProcessojuridicoandamentoReport extends SinedReport<ProcessojuridicoandamentoFiltro> {
	
	private ProcessojuridicoandamentoService processojuridicoandamentoService;
	
	public void setProcessojuridicoandamentoService(ProcessojuridicoandamentoService processojuridicoandamentoService) {
		this.processojuridicoandamentoService = processojuridicoandamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ProcessojuridicoandamentoFiltro filtro) throws Exception {
		return processojuridicoandamentoService.gerarRelatorioListagem(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "Andamento";
	}

	@Override
	public String getTitulo(ProcessojuridicoandamentoFiltro filtro) {
		return "Andamento";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ProcessojuridicoandamentoFiltro filtro) {
	}
}