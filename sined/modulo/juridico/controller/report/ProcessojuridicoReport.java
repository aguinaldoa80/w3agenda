package br.com.linkcom.sined.modulo.juridico.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.ProcessojuridicoService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/juridico/relatorio/Processojuridico", authorizationModule=ReportAuthorizationModule.class)
public class ProcessojuridicoReport extends SinedReport<ProcessojuridicoFiltro> {
	
	private ProcessojuridicoService processojuridicoService;
	
	public void setProcessojuridicoService(ProcessojuridicoService processojuridicoService) {
		this.processojuridicoService = processojuridicoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ProcessojuridicoFiltro filtro) throws Exception {
		return processojuridicoService.gerarRelatorioListagem(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "ProcessoJuridico";
	}

	@Override
	public String getTitulo(ProcessojuridicoFiltro filtro) {
		return "Processo Jur�dico";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ProcessojuridicoFiltro filtro) {
	}
}