package br.com.linkcom.sined.modulo.juridico.controller.report.bean;

import java.util.List;

public class QuantitativoProcessojuridicoBean implements Comparable<QuantitativoProcessojuridicoBean> {
	
	private String nome;
	private List<QuantitativoProcessojuridicoSituacaoBean> listaSituacaoBean;
	
	public String getNome() {
		return nome;
	}
	public List<QuantitativoProcessojuridicoSituacaoBean> getListaSituacaoBean() {
		return listaSituacaoBean;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setListaSituacaoBean(List<QuantitativoProcessojuridicoSituacaoBean> listaSituacaoBean) {
		this.listaSituacaoBean = listaSituacaoBean;
	}
	
	public int compareTo(QuantitativoProcessojuridicoBean o) {
		return this.nome.compareToIgnoreCase(o.getNome());
	}	
}