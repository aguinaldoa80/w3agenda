package br.com.linkcom.sined.modulo.juridico.controller.report.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Areatipo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Processojuridicoquestionario;

public class FichaProcessojuridicoBean implements Comparable<FichaProcessojuridicoBean> {
	
	private Integer cdprocessojuridico;
	private Area area;
	private Areatipo areatipo;
	private Colaborador advogado;
	private String sintese;
	private List<FichaProcessojuridicoClienteBean> listaClienteBean; 
	private List<FichaProcessojuridicoParteContrariaBean> listaParteContrariaBean; 
	private List<Processojuridicoquestionario> listaProcessojuridicoquestionario;
	
	public Integer getCdprocessojuridico() {
		return cdprocessojuridico;
	}
	public Area getArea() {
		return area;
	}
	public Areatipo getAreatipo() {
		return areatipo;
	}
	public Colaborador getAdvogado() {
		return advogado;
	}
	public String getSintese() {
		return sintese;
	}
	public List<FichaProcessojuridicoClienteBean> getListaClienteBean() {
		return listaClienteBean;
	}
	public List<FichaProcessojuridicoParteContrariaBean> getListaParteContrariaBean() {
		return listaParteContrariaBean;
	}
	public List<Processojuridicoquestionario> getListaProcessojuridicoquestionario() {
		return listaProcessojuridicoquestionario;
	}
	
	public void setCdprocessojuridico(Integer cdprocessojuridico) {
		this.cdprocessojuridico = cdprocessojuridico;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public void setAreatipo(Areatipo areatipo) {
		this.areatipo = areatipo;
	}
	public void setAdvogado(Colaborador advogado) {
		this.advogado = advogado;
	}
	public void setSintese(String sintese) {
		this.sintese = sintese;
	}
	public void setListaClienteBean(List<FichaProcessojuridicoClienteBean> listaClienteBean) {
		this.listaClienteBean = listaClienteBean;
	}
	public void setListaParteContrariaBean(List<FichaProcessojuridicoParteContrariaBean> listaParteContrariaBean) {
		this.listaParteContrariaBean = listaParteContrariaBean;
	}
	public void setListaProcessojuridicoquestionario(List<Processojuridicoquestionario> listaProcessojuridicoquestionario) {
		this.listaProcessojuridicoquestionario = listaProcessojuridicoquestionario;
	}

	public int compareTo(FichaProcessojuridicoBean o) {
		return 0;
	}	
}