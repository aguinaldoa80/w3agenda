package br.com.linkcom.sined.modulo.juridico.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Processojuridicosituacao;

public class QuantitativoProcessojuridicoSituacaoBean {
	
	private Processojuridicosituacao processojuridicosituacao;
	private Integer total;
	
	public Processojuridicosituacao getProcessojuridicosituacao() {
		return processojuridicosituacao;
	}
	public Integer getTotal() {
		return total;
	}
	
	public void setProcessojuridicosituacao(Processojuridicosituacao processojuridicosituacao) {
		this.processojuridicosituacao = processojuridicosituacao;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
}