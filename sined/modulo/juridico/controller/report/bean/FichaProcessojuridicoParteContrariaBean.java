package br.com.linkcom.sined.modulo.juridico.controller.report.bean;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Partecontraria;
import br.com.linkcom.sined.geral.bean.Processojuridicocliente;

public class FichaProcessojuridicoParteContrariaBean implements Comparable<FichaProcessojuridicoParteContrariaBean> {
	
	private Partecontraria partecontraria;
	
	public FichaProcessojuridicoParteContrariaBean(){
	}
	
	public FichaProcessojuridicoParteContrariaBean(Processojuridicocliente processojuridicocliente){
		this.partecontraria = processojuridicocliente.getPartecontraria();
	}
	
	public Partecontraria getPartecontraria() {
		return partecontraria;
	}
	
	public String getEnderecoCompleto() {
		if (partecontraria!=null){
			StringBuilder enderecoCompleto = new StringBuilder();
			enderecoCompleto.append(!Util.strings.isEmpty(partecontraria.getLogradouro()) ? partecontraria.getLogradouro() : "");
			enderecoCompleto.append(!Util.strings.isEmpty(partecontraria.getNumero()) ? ", " + partecontraria.getNumero() : "");
			enderecoCompleto.append(!Util.strings.isEmpty(partecontraria.getComplemento()) ? "/" + partecontraria.getComplemento() : "");
			enderecoCompleto.append(!Util.strings.isEmpty(partecontraria.getBairro()) ? " - " + partecontraria.getBairro() : "");
			enderecoCompleto.append(partecontraria.getMunicipio()!=null ? " - " + partecontraria.getMunicipio().getNome() : "");
			enderecoCompleto.append(partecontraria.getMunicipio()!=null && partecontraria.getMunicipio().getUf()!=null ? " - " + partecontraria.getMunicipio().getUf().getSigla() : "");
			return enderecoCompleto.toString();
		}else {
			return null;
		}
	}
	
	public int compareTo(FichaProcessojuridicoParteContrariaBean o) {
		return partecontraria.getNome().compareToIgnoreCase(o.getPartecontraria().getNome());
	}	
}