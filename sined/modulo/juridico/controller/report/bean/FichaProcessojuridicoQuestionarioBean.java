package br.com.linkcom.sined.modulo.juridico.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Processojuridicoquestionario;

public class FichaProcessojuridicoQuestionarioBean implements Comparable<FichaProcessojuridicoQuestionarioBean> {
	
	private Processojuridicoquestionario processojuridicoquestionario;
	
	public Processojuridicoquestionario getProcessojuridicoquestionario() {
		return processojuridicoquestionario;
	}

	public void setProcessojuridicoquestionario(Processojuridicoquestionario processojuridicoquestionario) {
		this.processojuridicoquestionario = processojuridicoquestionario;
	}

	public int compareTo(FichaProcessojuridicoQuestionarioBean o) {
		return processojuridicoquestionario.getOrdem().compareTo(o.getProcessojuridicoquestionario().getOrdem());
	}	
}