package br.com.linkcom.sined.modulo.juridico.controller.report.bean;

import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Processojuridicocliente;

public class FichaProcessojuridicoClienteBean implements Comparable<FichaProcessojuridicoClienteBean> {
	
	private Cliente cliente;
	private Set<Endereco> listaEndereco;
	
	public FichaProcessojuridicoClienteBean(){		
	}
	
	public FichaProcessojuridicoClienteBean(Processojuridicocliente processojuridicocliente){
		this.cliente = processojuridicocliente.getCliente();
		this.listaEndereco = processojuridicocliente.getCliente().getListaEndereco();
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public String getEnderecoCompleto() {
		
		if (listaEndereco!=null && !listaEndereco.isEmpty()){
			Endereco endereco = new ListSet<Endereco>(Endereco.class, this.listaEndereco).get(0);
			StringBuilder enderecoCompleto = new StringBuilder();
			enderecoCompleto.append(!Util.strings.isEmpty(endereco.getLogradouro()) ? endereco.getLogradouro() : "");
			enderecoCompleto.append(!Util.strings.isEmpty(endereco.getNumero()) ? ", " + endereco.getNumero() : "");
			enderecoCompleto.append(!Util.strings.isEmpty(endereco.getComplemento()) ? "/" + endereco.getComplemento() : "");
			enderecoCompleto.append(!Util.strings.isEmpty(endereco.getBairro()) ? " - " + endereco.getBairro() : "");
			enderecoCompleto.append(endereco.getMunicipio()!=null ? " - " + endereco.getMunicipio().getNome() : "");
			enderecoCompleto.append(endereco.getMunicipio()!=null && endereco.getMunicipio().getUf()!=null ? " - " + endereco.getMunicipio().getUf().getSigla() : "");
			return enderecoCompleto.toString();
		}
		
		return null;
	}
	
	public int compareTo(FichaProcessojuridicoClienteBean o) {
		return cliente.getNome().compareToIgnoreCase(o.getCliente().getNome());
	}
}