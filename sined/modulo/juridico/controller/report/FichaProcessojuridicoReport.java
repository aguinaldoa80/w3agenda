package br.com.linkcom.sined.modulo.juridico.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ProcessojuridicoService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path="/juridico/relatorio/Fichaprocessojuridico", authorizationModule=ReportAuthorizationModule.class)
public class FichaProcessojuridicoReport extends SinedReport<ProcessojuridicoFiltro> {
	
	private ProcessojuridicoService processojuridicoService;
	
	public void setProcessojuridicoService(ProcessojuridicoService processojuridicoService) {
		this.processojuridicoService = processojuridicoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ProcessojuridicoFiltro filtro) throws Exception {
		return processojuridicoService.gerarRelatorioFichaProcessoJuridico(request.getParameter("selectedItens"));		
	}

	@Override
	public String getNomeArquivo() {
		return "FichaProcessoJuridico";
	}

	@Override
	public String getTitulo(ProcessojuridicoFiltro filtro) {
		return "Ficha de Processo Jur�dico";
	}
}