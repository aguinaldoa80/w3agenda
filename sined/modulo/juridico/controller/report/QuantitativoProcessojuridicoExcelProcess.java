package br.com.linkcom.sined.modulo.juridico.controller.report;

import java.sql.Date;
import java.util.Calendar;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ProcessojuridicoService;
import br.com.linkcom.sined.modulo.juridico.controller.report.filter.QuantitativoProcessojuridicoFiltro;

@Controller(path="/juridico/process/QuantitativoprocessojuridicoExcel", authorizationModule=ProcessAuthorizationModule.class)
public class QuantitativoProcessojuridicoExcelProcess extends ResourceSenderController<QuantitativoProcessojuridicoFiltro> {
	
	private ProcessojuridicoService processojuridicoService;
	
	public void setProcessojuridicoService(ProcessojuridicoService processojuridicoService) {
		this.processojuridicoService = processojuridicoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, QuantitativoProcessojuridicoFiltro filtro) throws Exception {
		return processojuridicoService.getResourceQuantitativoProcessojuridico(filtro);
	}
	

	@Override
	public ModelAndView doFiltro(WebRequestContext request, QuantitativoProcessojuridicoFiltro filtro) throws Exception {
		
		if (filtro.getDtinicio()==null && filtro.getDtfim()==null){
			Calendar calendarPrimeiroDiaMes = Calendar.getInstance();
			calendarPrimeiroDiaMes.setTimeInMillis(System.currentTimeMillis());
			calendarPrimeiroDiaMes.set(Calendar.DAY_OF_MONTH, 1);
			filtro.setDtinicio(new Date(calendarPrimeiroDiaMes.getTimeInMillis()));
			filtro.setDtfim(new Date(System.currentTimeMillis()));
		}
		
		return new ModelAndView("relatorio/quantitativoProcessojuridico", "filtro", filtro);
	}
}