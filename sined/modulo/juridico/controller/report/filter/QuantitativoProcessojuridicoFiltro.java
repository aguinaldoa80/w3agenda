package br.com.linkcom.sined.modulo.juridico.controller.report.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Areatipo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Colaborador;

public class QuantitativoProcessojuridicoFiltro {
	
	private Date dtinicio;
	private Date dtfim;
	private Colaborador advogado;
	private Area area;
	private Areatipo areatipo;
	private Categoria categoria;
	private AgrupamentoEnum agrupamento;
	
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Advogado")
	public Colaborador getAdvogado() {
		return advogado;
	}
	@DisplayName("�rea")
	public Area getArea() {
		return area;
	}
	@DisplayName("Tipo de A��o")
	public Areatipo getAreatipo() {
		return areatipo;
	}
	@DisplayName("Categoria de Cliente")
	public Categoria getCategoria() {
		return categoria;
	}
	@Required
	@DisplayName("Agrupar por")
	public AgrupamentoEnum getAgrupamento() {
		return agrupamento;
	}
	
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setAdvogado(Colaborador advogado) {
		this.advogado = advogado;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public void setAreatipo(Areatipo areatipo) {
		this.areatipo = areatipo;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setAgrupamento(AgrupamentoEnum agrupamento) {
		this.agrupamento = agrupamento;
	}	
}