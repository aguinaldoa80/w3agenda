package br.com.linkcom.sined.modulo.juridico.controller.report.filter;

public enum AgrupamentoEnum {
		
	ADVOGADO("Advogado"), 
	AREA("�rea"), 
	CLIENTE("Cliente"), 
	PARTE_CONTRARIA("Parte Contr�ria");
	
	private String nome;
		
	private AgrupamentoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}	
}