package br.com.linkcom.sined.modulo.juridico.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratojuridico;
import br.com.linkcom.sined.geral.bean.Contratojuridicohistorico;
import br.com.linkcom.sined.geral.bean.Empresamodelocontrato;
import br.com.linkcom.sined.geral.bean.Processojuridico;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contratojuridicoacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContratojuridico;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContratojuridicoService;
import br.com.linkcom.sined.geral.service.ContratojuridicohistoricoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EmpresamodelocontratoService;
import br.com.linkcom.sined.geral.service.ProcessojuridicoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.bean.ProcessojuridicoComboBean;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ContratojuridicoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/juridico/crud/Contratojuridico", authorizationModule=CrudAuthorizationModule.class)
public class ContratojuridicoCrud extends CrudControllerSined<ContratojuridicoFiltro, Contratojuridico, Contratojuridico>{
	
	private ProjetoService projetoService;
	private RateioitemService rateioitemService;
	private RateioService rateioService;
	private TaxaService taxaService;
	private CentrocustoService centrocustoService;
	private ContratojuridicoService contratojuridicoService;
	private ContratojuridicohistoricoService contratojuridicohistoricoService;
	private EmpresaService empresaService;
	private ProcessojuridicoService processojuridicoService;
	private ClienteService clienteService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setContratojuridicohistoricoService(
			ContratojuridicohistoricoService contratojuridicohistoricoService) {
		this.contratojuridicohistoricoService = contratojuridicohistoricoService;
	}
	public void setContratojuridicoService(
			ContratojuridicoService contratojuridicoService) {
		this.contratojuridicoService = contratojuridicoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setProcessojuridicoService(ProcessojuridicoService processojuridicoService) {
		this.processojuridicoService = processojuridicoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, ContratojuridicoFiltro filtro) throws Exception {
		ArrayList<SituacaoContratojuridico> listaSituacao =  new ArrayList<SituacaoContratojuridico>();
		for (SituacaoContratojuridico situacao : SituacaoContratojuridico.values()) {
			listaSituacao.add(situacao);
		}
		
		request.setAttribute("listaSituacao", listaSituacao);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Contratojuridico bean) throws Exception {
		boolean isCriar = bean.getCdcontratojuridico() == null;		
		
		super.salvar(request, bean);
		
		Contratojuridicohistorico contratojuridicohistorico = new Contratojuridicohistorico();
		contratojuridicohistorico.setContratojuridico(bean);
		if(isCriar){
			contratojuridicohistorico.setAcao(Contratojuridicoacao.CRIADO);
		} else {
			contratojuridicohistorico.setAcao(Contratojuridicoacao.ALTERADO);
		}
		contratojuridicohistoricoService.saveOrUpdate(contratojuridicohistorico);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Contratojuridico bean)throws Exception {
		try {			
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException e) {
			throw new SinedException("Contrato n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Contratojuridico form) throws Exception {
		
		List<Centrocusto> listaCentrocusto = null;		
		if(form.getCdcontratojuridico() != null){
			if(form.getCliente() != null){
				form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome, cliente.identificador"));
			}
			if(form.getRateio() != null && form.getRateio().getCdrateio() != null){
				form.setRateio(rateioService.findRateio(form.getRateio()));
				listaCentrocusto = (List<Centrocusto>) CollectionsUtil.getListProperty(form.getRateio().getListaRateioitem(), "centrocusto");
			}
			
			if(form.getTaxa() != null && form.getTaxa().getCdtaxa() != null){
				form.setTaxa(taxaService.findTaxa(form.getTaxa()));
			}
		}
		
		//Parametro geral que define se pode ser exibido nova linha para rateio
		boolean showNewLineButtonRateio = SinedUtil.showNewLineButtonRateio();
		request.setAttribute("exibirNovaLinhaRateio", showNewLineButtonRateio);
		
		if(form.getCdcontratojuridico() == null && !showNewLineButtonRateio){
			List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
			listaRateioitem.add(new Rateioitem());
			Rateio rateio = new Rateio();
			rateio.setListaRateioitem(listaRateioitem);
			form.setRateio(rateio);
		}
		
		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getCdcontratojuridico() != null && form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
			List<Projeto> projetos = rateioitemService.getProjetos(form.getRateio().getListaRateioitem());
			listaProjeto = projetoService.findProjetosAbertosSemPermissao(projetos);
		} else {
			listaProjeto = projetoService.findProjetosAbertos(null);
		}
		request.setAttribute("listaProjeto", listaProjeto);
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_CREDITO);
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(listaCentrocusto));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		
		if(form.getCdcontratojuridico() != null){
			form.setListaContratojuridicohistorico(contratojuridicohistoricoService.findByContratojuridico(form));
		}
		request.setAttribute("processojuridicoEscolhido", form.getProcessojuridico() != null && form.getProcessojuridico().getCdprocessojuridico() != null ? 
				"br.com.linkcom.sined.geral.bean.Processojuridico[cdprocessojuridico=" + form.getProcessojuridico().getCdprocessojuridico() + "]" : "<null>");
	}
	
	public void ajaxGerarParcelas(WebRequestContext request, Contratojuridico contratojuridico){
		List<String> listaDatas = contratojuridicoService.geraParcelamento(contratojuridico);
		View.getCurrent().convertObjectToJs(listaDatas, "listaDocDatas");
	}
	
	public ModelAndView gerarReceita(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Contratojuridico> listaContratojuridico = contratojuridicoService.findForReceita(whereIn);
		for (Contratojuridico contratojuridico : listaContratojuridico) {
			if(!contratojuridico.getAux_contratojuridico().getSituacao().equals(SituacaoContratojuridico.A_CONSOLIDAR) && 
					!contratojuridico.getAux_contratojuridico().getSituacao().equals(SituacaoContratojuridico.ATENCAO)){
				request.addError("S� � poss�vel gerar receita de contratos nas situa��es 'A CONSOLIDAR' e 'ATEN��O'.");
				if("consultar".equals(request.getParameter("action"))){
					return new ModelAndView("redirect:/juridico/crud/Contratojuridico?ACAO=consultar&cdcontratojuridico=" + whereIn);
				} else {
					return sendRedirectToAction(request.getParameter("action"));
				}
			}
			contratojuridicoService.gerarReceita(contratojuridico);
		}
		
		request.addMessage("Receita(s) gerada(s) com sucesso.");
		if("consultar".equals(request.getParameter("action"))){
			return new ModelAndView("redirect:/juridico/crud/Contratojuridico?ACAO=consultar&cdcontratojuridico=" + whereIn);
		} else {
			return sendRedirectToAction(request.getParameter("action"));
		}
	}
	
	public ModelAndView executarSentenca(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Contratojuridico> listaContratojuridico = contratojuridicoService.findForReceita(whereIn);
		for (Contratojuridico contratojuridico : listaContratojuridico) {
			if(!contratojuridico.getAux_contratojuridico().getSituacao().equals(SituacaoContratojuridico.AGUARDANDO_SENTENCA)){
				request.addError("S� � poss�vel executar senten�a de contratos na situa��o 'AGUARDANDO SENTEN�A'.");
				if("consultar".equals(request.getParameter("action"))){
					return new ModelAndView("redirect:/juridico/crud/Contratojuridico?ACAO=consultar&cdcontratojuridico=" + whereIn);
				} else {
					return sendRedirectToAction(request.getParameter("action"));
				}
			}
			contratojuridicoService.executarSentenca(contratojuridico);
		}
		
		request.addMessage("Senten�a(s) executada(s) com sucesso.");
		if("consultar".equals(request.getParameter("action"))){
			return new ModelAndView("redirect:/juridico/crud/Contratojuridico?ACAO=consultar&cdcontratojuridico=" + whereIn);
		} else {
			return sendRedirectToAction(request.getParameter("action"));
		}
	}
	
	public ModelAndView cancelar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		contratojuridicoService.cancelar(whereIn);
		
		String[] array = whereIn.split(",");
		Contratojuridico cj;
		Contratojuridicohistorico cjh;
		for (int i = 0; i < array.length; i++) {
			try{
				cj = new Contratojuridico();
				cj.setCdcontratojuridico(Integer.parseInt(array[i]));
				
				cjh = new Contratojuridicohistorico();
				cjh.setContratojuridico(cj);
				cjh.setAcao(Contratojuridicoacao.CANCELADO);
				contratojuridicohistoricoService.saveOrUpdate(cjh);
				
				contratojuridicoService.updateAux(cj.getCdcontratojuridico());
			} catch (Exception e) {}
		}
		
		request.addMessage("Contrato(s) cancelado(s) com sucesso.");
		if("consultar".equals(request.getParameter("action"))){
			return new ModelAndView("redirect:/juridico/crud/Contratojuridico?ACAO=consultar&cdcontratojuridico=" + whereIn);
		} else {
			return sendRedirectToAction(request.getParameter("action"));
		}
	}

    /**
	 * Verifica se existe um modelo de contrato associado ao Material/Servi�os,
	 * retorna o modelo se existir apenas um, caso contr�rio dever� ser exibida
	 * uma tela para que o usu�rio selecione o contrato.
	 * 
	 * @param request
	 * @author Giovane Freitas
	 * @since 21/01/2012
	 */
    public ModelAndView selecionarContrato(WebRequestContext request, Contratojuridico contratojuridico){
    	
    	List<Empresamodelocontrato> empresamodelocontrato = EmpresamodelocontratoService.getInstance().findByContratojuridico(contratojuridico);
		
		if (empresamodelocontrato != null && empresamodelocontrato.size() == 1){
			return new JsonModelAndView()
						.addObject("cdempresamodelocontrato", empresamodelocontrato.get(0).getCdempresamodelocontrato());
		}else{
			return new JsonModelAndView().addObject("cdempresamodelocontrato", "<null>");
		}
	}

	/**
	 * Abre uma tela para que o usu�rio selecione o modelo de contrato RTF a ser usado para a emiss�o.
	 * 
	 * @author Giovane Freitas
	 * @param request
	 * @return
	 */
	public ModelAndView abrirSelecaoModeloContrato(WebRequestContext request, Contratojuridico contratojuridico) {
		
		if(contratojuridico == null || contratojuridico.getCdcontratojuridico() == null){
			request.addError("Selecione um contrato para realizar a emiss�o.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		request.setAttribute("contratojuridico", contratojuridico);
		request.setAttribute("modelos", EmpresamodelocontratoService.getInstance().findByContratojuridico(contratojuridico));
		
		return new ModelAndView("direct:/crud/popup/selecaoModeloContrato");
	}

	/**
	* M�todo que carrega o combo de processo juridico de acordo com o cliente
	*
	* @param request
	* @param cliente
	* @return
	* @since 04/11/2016
	* @author Luiz Fernando
	*/
	public ModelAndView comboBoxProcessojuridico(WebRequestContext request, Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		List<Processojuridico> lista = processojuridicoService.findByCliente(cliente);
		List<ProcessojuridicoComboBean> listaComboBean = new ArrayList<ProcessojuridicoComboBean>();
		if(SinedUtil.isListNotEmpty(lista)){
			for(Processojuridico processojuridico : lista){
				listaComboBean.add(new ProcessojuridicoComboBean(processojuridico));
			}
		}
		return new JsonModelAndView().addObject("lista", listaComboBean);
	}
}