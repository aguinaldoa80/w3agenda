package br.com.linkcom.sined.modulo.juridico.controller.crud;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Partecontraria;
import br.com.linkcom.sined.geral.service.ProcessojuridicoclienteService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.PartecontrariaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/juridico/crud/Partecontraria", authorizationModule=CrudAuthorizationModule.class)
public class PartecontrariaCrud extends CrudControllerSined<PartecontrariaFiltro, Partecontraria, Partecontraria>{
	
	private ProcessojuridicoclienteService processojuridicoclienteService;
	
	public void setProcessojuridicoclienteService(
			ProcessojuridicoclienteService processojuridicoclienteService) {
		this.processojuridicoclienteService = processojuridicoclienteService;
	}
	
	@Override
	protected void excluir(WebRequestContext request, Partecontraria bean)throws Exception {
		try {			
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException e) {
			throw new SinedException("Parte contr�ria n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@Override
	protected ListagemResult<Partecontraria> getLista(WebRequestContext request, PartecontrariaFiltro filtro) {
		ListagemResult<Partecontraria> lista = super.getLista(request, filtro);
		List<Partecontraria> list = lista.list();
		
		for (Partecontraria partecontraria : list) {
			partecontraria.setQtdeProcessos(processojuridicoclienteService.countByParteContraria(partecontraria));
		}
		
		return lista;
	}

}
