package br.com.linkcom.sined.modulo.juridico.controller.crud;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Processojuridicoandamento;
import br.com.linkcom.sined.geral.bean.Processojuridicoinstancia;
import br.com.linkcom.sined.geral.service.ProcessojuridicoService;
import br.com.linkcom.sined.geral.service.ProcessojuridicoinstanciaService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoandamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/juridico/crud/ProcessoJuridicoAndamento", authorizationModule=CrudAuthorizationModule.class)
public class ProcessojuridicoandamentoCrud extends CrudControllerSined<ProcessojuridicoandamentoFiltro, Processojuridicoandamento, Processojuridicoandamento>{
	
	private ProcessojuridicoService processojuridicoService;
	private ProcessojuridicoinstanciaService processojuridicoinstanciaService;
	
	public void setProcessojuridicoService(ProcessojuridicoService processojuridicoService) {
		this.processojuridicoService = processojuridicoService;
	}
	public void setProcessojuridicoinstanciaService(ProcessojuridicoinstanciaService processojuridicoinstanciaService) {
		this.processojuridicoinstanciaService = processojuridicoinstanciaService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, ProcessojuridicoandamentoFiltro filtro) throws Exception {
		
		if (filtro.getProcessojuridico()!=null && filtro.getProcessojuridico().getCdprocessojuridico()!=null){
			filtro.setProcessojuridico(processojuridicoService.load(filtro.getProcessojuridico(), "processojuridico.cdprocessojuridico, processojuridico.vprocessojuridico"));
		}
		
		if(filtro.getProcessojuridicoinstancia() != null && filtro.getProcessojuridicoinstancia().getCdprocessojuridicoinstancia() != null){
			request.setAttribute("instanciaEscolhida", "br.com.linkcom.sined.geral.bean.Processojuridicoinstancia[cdprocessojuridicoinstancia=" + filtro.getProcessojuridicoinstancia().getCdprocessojuridicoinstancia() + "]");
		} else {
			request.setAttribute("instanciaEscolhida", "<null>");
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Processojuridicoandamento bean)throws Exception {
		try {	
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			request.setAttribute("clearBase", request.getParameter("clearBase"));
			if (DatabaseError.isKeyPresent(e, "idx_procjuridicoandamento_instancia")) throw new SinedException("Andamento j� cadastrado nesta data para esta inst�ncia.");
			else throw e;
		}
	}
	@Override
	protected Processojuridicoandamento criar(WebRequestContext request, Processojuridicoandamento form) throws Exception {
		request.setAttribute("clearBase", request.getParameter("clearBase"));
		return super.criar(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Processojuridicoandamento form) throws Exception {
		if(form.getCdprocessojuridicoandamento() == null && request.getParameter("cdprocessojuridicoinstancia") != null){
			Integer cdprocessojuridicoinstancia = Integer.parseInt(request.getParameter("cdprocessojuridicoinstancia"));
			Processojuridicoinstancia processojuridicoinstancia = processojuridicoinstanciaService.carregaInstancia(cdprocessojuridicoinstancia);
			form.setProcessojuridicoinstancia(processojuridicoinstancia);
		}
		
		if(form.getProcessojuridicoinstancia() != null && form.getProcessojuridicoinstancia().getCdprocessojuridicoinstancia() != null){
			request.setAttribute("instanciaEscolhida", "br.com.linkcom.sined.geral.bean.Processojuridicoinstancia[cdprocessojuridicoinstancia=" + form.getProcessojuridicoinstancia().getCdprocessojuridicoinstancia() + "]");
		} else {
			request.setAttribute("instanciaEscolhida", "<null>");
		}
	}
	
	
	@Override
	protected void excluir(WebRequestContext request, Processojuridicoandamento bean)throws Exception {
		try {			
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException e) {
			throw new SinedException("Andamento n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	/**
	 * Ajax que retorna para a tela de andamento a �ltima inst�ncia do processo selecionado.
	 *
	 * @see br.com.linkcom.sined.geral.service.ProcessojuridicoinstanciaService#listaDeInstancias
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxUltimaInstancia(WebRequestContext request, ProcessojuridicoandamentoFiltro bean){
		if(bean.getProcessojuridico() == null || bean.getProcessojuridico().getCdprocessojuridico() == null){
			View.getCurrent().println("var instancia = '<null>'; alert('Erro no carregamento da �ltima inst�ncia.');");
		} else {
			List<Processojuridicoinstancia> listaDeInstancias = processojuridicoinstanciaService.listaDeInstancias(bean.getProcessojuridico().getCdprocessojuridico());
			String processo = "<null>";
			if(listaDeInstancias != null && listaDeInstancias.size() > 0){
				processo = "br.com.linkcom.sined.geral.bean.Processojuridicoinstancia[cdprocessojuridicoinstancia=" + listaDeInstancias.get(0).getCdprocessojuridicoinstancia() + "]";
			}
			View.getCurrent().println("var instancia = '" + processo + "';");
		}
	}
	
	public ModelAndView comboBoxInstancia(WebRequestContext request, Processojuridicoinstancia instancia) {
		List<Processojuridicoinstancia> lista = processojuridicoinstanciaService.listaDeInstancias(instancia.getProcessojuridico().getCdprocessojuridico());
		return new JsonModelAndView().addObject("listaProcessojuridicoinstancia", lista);
	}

}
