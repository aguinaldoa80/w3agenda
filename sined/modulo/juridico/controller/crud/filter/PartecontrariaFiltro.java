package br.com.linkcom.sined.modulo.juridico.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PartecontrariaFiltro extends FiltroListagemSined {
	
	protected String nome;
	protected Tipopessoa tipopessoa;
	protected Cpf cpf;
	protected Cnpj cnpj;

	@MaxLength(100)
	public String getNome() {
		return nome;
	}

	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}

	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}

	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	
}
