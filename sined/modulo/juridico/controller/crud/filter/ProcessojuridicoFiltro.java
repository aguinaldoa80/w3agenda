package br.com.linkcom.sined.modulo.juridico.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Areatipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Partecontraria;
import br.com.linkcom.sined.geral.bean.Processojuridicoinstanciatipo;
import br.com.linkcom.sined.geral.bean.Processojuridicosituacao;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.ProcessojuridicosituacaoService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProcessojuridicoFiltro extends FiltroListagemSined {
	
	protected Date datainicio;
	protected Date datafim;
	protected Area area;
	protected Areatipo areatipo;
	protected Colaborador advogado;
	protected Cliente cliente;
	protected Partecontraria partecontraria;
	protected Processojuridicoinstanciatipo processojuridicoinstanciatipo;
	protected String numeroinstancia;
	protected Projeto projeto;

	protected List<Processojuridicosituacao> listaProcessojuridicosituacao;
	
	public ProcessojuridicoFiltro() {
		listaProcessojuridicosituacao = ProcessojuridicosituacaoService.getInstance().findAll();
	}
	
	@DisplayName("Advogado")
	public Colaborador getAdvogado() {
		return advogado;
	}
	
	public Date getDatainicio() {
		return datainicio;
	}
	
	public Date getDatafim() {
		return datafim;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Situa��o")
	public List<Processojuridicosituacao> getListaProcessojuridicosituacao() {
		return listaProcessojuridicosituacao;
	}
	
	@DisplayName("�rea")
	public Area getArea() {
		return area;
	}

	@DisplayName("Tipo")
	public Areatipo getAreatipo() {
		return areatipo;
	}

	@DisplayName("Parte contr�ria")
	public Partecontraria getPartecontraria() {
		return partecontraria;
	}

	@DisplayName("Tipo de inst�ncia")
	public Processojuridicoinstanciatipo getProcessojuridicoinstanciatipo() {
		return processojuridicoinstanciatipo;
	}

	@MaxLength(50)
	@DisplayName("N�mero da inst�ncia")
	public String getNumeroinstancia() {
		return numeroinstancia;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	public void setAreatipo(Areatipo areatipo) {
		this.areatipo = areatipo;
	}
	public void setPartecontraria(Partecontraria partecontraria) {
		this.partecontraria = partecontraria;
	}
	public void setProcessojuridicoinstanciatipo(
			Processojuridicoinstanciatipo processojuridicoinstanciatipo) {
		this.processojuridicoinstanciatipo = processojuridicoinstanciatipo;
	}
	public void setNumeroinstancia(String numeroinstancia) {
		this.numeroinstancia = numeroinstancia;
	}
	public void setListaProcessojuridicosituacao(
			List<Processojuridicosituacao> listaProcessojuridicosituacao) {
		this.listaProcessojuridicosituacao = listaProcessojuridicosituacao;
	}
	public void setAdvogado(Colaborador advogado) {
		this.advogado = advogado;
	}
	public void setDatainicio(Date datainicio) {
		this.datainicio = datainicio;
	}
	public void setDatafim(Date datafim) {
		this.datafim = datafim;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
}
