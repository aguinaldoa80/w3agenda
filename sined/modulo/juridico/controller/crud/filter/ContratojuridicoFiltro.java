package br.com.linkcom.sined.modulo.juridico.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContratojuridico;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContratojuridicoFiltro extends FiltroListagemSined {
	
	protected Boolean tipopesquisacliente;
	protected Cliente clienterazaosocial;
	protected Cliente clientenome;
	protected String descricao;
	protected Date dtinicioDe;
	protected Date dtinicioAte;
	protected Empresa empresa;
	protected List<SituacaoContratojuridico> listaSituacao;
	protected Contratotipo contratotipo;
	
	public ContratojuridicoFiltro() {
	}
	
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	public Date getDtinicioDe() {
		return dtinicioDe;
	}
	public Date getDtinicioAte() {
		return dtinicioAte;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public List<SituacaoContratojuridico> getListaSituacao() {
		return listaSituacao;
	}
	@DisplayName("Tipo")
	public Boolean getTipopesquisacliente() {
		return tipopesquisacliente;
	}
	@DisplayName("Raz�o social")
	public Cliente getClienterazaosocial() {
		return clienterazaosocial;
	}
	@DisplayName("Nome / Identificador")
	public Cliente getClientenome() {
		return clientenome;
	}
	public void setTipopesquisacliente(Boolean tipopesquisacliente) {
		this.tipopesquisacliente = tipopesquisacliente;
	}
	public void setClienterazaosocial(Cliente clienterazaosocial) {
		this.clienterazaosocial = clienterazaosocial;
	}
	public void setClientenome(Cliente clientenome) {
		this.clientenome = clientenome;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setDtinicioDe(Date dtinicioDe) {
		this.dtinicioDe = dtinicioDe;
	}
	public void setDtinicioAte(Date dtinicioAte) {
		this.dtinicioAte = dtinicioAte;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setListaSituacao(List<SituacaoContratojuridico> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	@DisplayName("Tipo de contrato")
	public Contratotipo getContratotipo() {
		return contratotipo;
	}
	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}
	
	
}
