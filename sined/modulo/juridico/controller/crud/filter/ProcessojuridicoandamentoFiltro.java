package br.com.linkcom.sined.modulo.juridico.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Partecontraria;
import br.com.linkcom.sined.geral.bean.Processojuridico;
import br.com.linkcom.sined.geral.bean.Processojuridicoinstancia;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoenvolvido;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProcessojuridicoandamentoFiltro extends FiltroListagemSined {
	
	protected Tipoenvolvido tipoenvolvido;
	protected Cliente cliente;
	protected Partecontraria partecontraria;
	protected Processojuridicoinstancia processojuridicoinstancia;
	protected Processojuridico processojuridico;
	protected Date dataDe;
	protected Date dataAte;
	
	@DisplayName("Processo Jur�dico")
	public Processojuridico getProcessojuridico() {
		return processojuridico;
	}
	
	@DisplayName("Inst�ncia")
	public Processojuridicoinstancia getProcessojuridicoinstancia() {
		return processojuridicoinstancia;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Envolvido")
	public Tipoenvolvido getTipoenvolvido() {
		return tipoenvolvido;
	}

	@DisplayName("Parte contr�ria")
	public Partecontraria getPartecontraria() {
		return partecontraria;
	}
	
	public Date getDataDe() {
		return dataDe;
	}

	public Date getDataAte() {
		return dataAte;
	}

	public void setDataDe(Date dataDe) {
		this.dataDe = dataDe;
	}

	public void setDataAte(Date dataAte) {
		this.dataAte = dataAte;
	}

	public void setTipoenvolvido(Tipoenvolvido tipoenvolvido) {
		this.tipoenvolvido = tipoenvolvido;
	}

	public void setPartecontraria(Partecontraria partecontraria) {
		this.partecontraria = partecontraria;
	}

	public void setProcessojuridicoinstancia(
			Processojuridicoinstancia processojuridicoinstancia) {
		this.processojuridicoinstancia = processojuridicoinstancia;
	}
	
	public void setProcessojuridico(Processojuridico processojuridico) {
		this.processojuridico = processojuridico;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
