package br.com.linkcom.sined.modulo.juridico.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Areaquestionario;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Processojuridico;
import br.com.linkcom.sined.geral.bean.Processojuridicocliente;
import br.com.linkcom.sined.geral.bean.Processojuridicoinstancia;
import br.com.linkcom.sined.geral.bean.Processojuridicosituacao;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Vara;
import br.com.linkcom.sined.geral.service.AreaquestionarioService;
import br.com.linkcom.sined.geral.service.ContratojuridicoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.ProcessojuridicoService;
import br.com.linkcom.sined.geral.service.ProcessojuridicosituacaoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.VaraService;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.ProcessojuridicoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path = "/juridico/crud/ProcessoJuridico", authorizationModule = CrudAuthorizationModule.class)
public class ProcessojuridicoCrud extends CrudControllerSined<ProcessojuridicoFiltro, Processojuridico, Processojuridico> {

	private AreaquestionarioService areaquestionarioService;
	private ProcessojuridicoService processojuridicoService;
	private VaraService varaService;
	private MunicipioService municipioService;
	private EmpresaService empresaService;
	private ContratojuridicoService contratojuridicoService;
	private ProjetoService projetoService;
	
	public void setContratojuridicoService(ContratojuridicoService contratojuridicoService) {this.contratojuridicoService = contratojuridicoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setAreaquestionarioService(AreaquestionarioService areaquestionarioService) {this.areaquestionarioService = areaquestionarioService;}
	public void setProcessojuridicoService(ProcessojuridicoService processojuridicoService) {this.processojuridicoService = processojuridicoService;}
	public void setVaraService(VaraService varaService) {this.varaService = varaService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setProjetoService(ProjetoService projetoService) { this.projetoService = projetoService; }
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Processojuridico form) throws Exception {

		if (form.getCdprocessojuridico() == null) {
			form.setData(new Date(System.currentTimeMillis()));
			form.setProcessojuridicosituacao(new Processojuridicosituacao(1, "EMANDAMENTO"));
			form.setAdvogado(SinedUtil.getUsuarioComoColaborador());
		}
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		List<Vara> listaVara = new ArrayList<Vara>();
		
		for (Processojuridicoinstancia itens : form.getListaProcessojuridicoinstancia()) {
			Vara vara = varaService.loadForEntrada(itens.getVara());
			listaMunicipio.add(vara.getMunicipio());
			listaVara.add(vara);
		}
		
		
		
		request.setAttribute("listaMunicipio", listaMunicipio);
		request.setAttribute("listaVara", listaVara);
		request.setAttribute("comboprocessorelacionado", processojuridicoService.populaCombo(form));
		request.setAttribute("listaProjeto", projetoService.findNaoCancelados());
	}

	@Override
	protected ListagemResult<Processojuridico> getLista(WebRequestContext request, ProcessojuridicoFiltro filtro) {

		ListagemResult<Processojuridico> listagemResult = super.getLista(request, filtro);
		List<Processojuridico> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdprocessojuridico", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(processojuridicoService.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}
		
		return listagemResult;
	}
	
	@Override
	protected void listagem(WebRequestContext request, ProcessojuridicoFiltro filtro) throws Exception {
		request.setAttribute("listaSituacao", ProcessojuridicosituacaoService.getInstance().findAll());
		request.setAttribute("listaProjeto", projetoService.findNaoCancelados());
	}

	@Override
	protected void salvar(WebRequestContext request, Processojuridico bean) throws Exception {
		try {	
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_procjuridico_instanciatipo")) throw new SinedException("N�o � permitido gravar inst�ncias com o mesmo tipo.");
			else throw e;
		}
	}
	
	/**
	 * Carrega a combo de munic�pio de acordo com a uf selecionada
	 * @author Taidson Santos
	 * @since 16/03/2010
	 * @param request
	 */
	public void ajaxComboMunicipio(WebRequestContext request){
		
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		Uf uf = null;
		if(!"<null>".equals(request.getParameter("cduf"))){
			uf = new Uf(Integer.valueOf(request.getParameter("cduf")));
		}
			listaMunicipio = municipioService.findByUfProcJuridico(uf);			
				
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaMunicipio, "listaMunicipio", ""));
	}
	
	/**
	 * Carrega a combo de vara incluindo somente as que n�o possuem uf.
	 * @author Taidson Santos
	 * @since 16/03/2010
	 * @param request
	 */
	public void ajaxCarregaFederais(WebRequestContext request){
		List<Vara> listaVaraFederal = new ArrayList<Vara>();
		Uf uf = null;
		Municipio municipio = null;
		listaVaraFederal = varaService.findByVaraUf(uf, municipio);
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaVaraFederal, "listaVaraFederal", ""));
	}
	
	/**
	 * Carrega a combo de vara de acordo com a uf e a comarca selecionadas.
	 * @author Taidson Santos
	 * @since 16/03/2010
	 * @param request
	 */
	public void ajaxComboVara(WebRequestContext request){
		
		List<Vara> listaVara = new ArrayList<Vara>();
		
		Uf uf = null;
		Municipio municipio = null;
		
		if("<null>".equals(request.getParameter("cduf"))){
			listaVara = varaService.findByVaraUf(uf , municipio);
		}else if(!"<null>".equals(request.getParameter("cduf")) && "<null>".equals(request.getParameter("cdmunicipio"))){
			uf = new Uf(Integer.valueOf(request.getParameter("cduf")));
			listaVara = varaService.findByVaraUf(uf, municipio);
		}else{
			municipio = new Municipio(Integer.valueOf(request.getParameter("cdmunicipio")));
			listaVara = varaService.findByMunicipio(municipio);
		}
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaVara, "listaVara", ""));
	}
	
	/**
	 * Retorna o objeto Vara com UF e Munic�pio correspondentes, 
	 * para sele��o das combos em inst�ncia.
	 * @param request
	 * @param bean
	 * @author Taidson
	 * @since 22/06/2010
	 */
	public void ajaxComarcaUf(WebRequestContext request, Processojuridicoinstancia bean){
		
		Vara vara = new Vara();
		
		vara = varaService.findByVaraComarcaUf(bean.getVara());
		
		if(vara.getMunicipio() != null){
			View.getCurrent().println("var ufSelecionado = '" + Util.strings.toStringIdStyled(vara.getUf(), false)+"';");
		}else{
			View.getCurrent().println("var ufSelecionado = '<null>';");
		}
		if(vara.getUf() != null){
			View.getCurrent().println("var municipioSelecionado = '" + Util.strings.toStringIdStyled(vara.getMunicipio(), false)+"';");
		}else{
			View.getCurrent().println("var municipioSelecionado = '<null>';");
		}
		View.getCurrent().println("var varaSelecionado = '" + Util.strings.toStringIdStyled(vara, false)+"';");
	}
	
	public ModelAndView ajaxQuestionario(WebRequestContext request, Processojuridico bean){
		List<Areaquestionario> lista = new ArrayList<Areaquestionario>();
		if(bean.getArea() != null){
			lista = areaquestionarioService.findByArea(bean.getArea());
		} 
		return new JsonModelAndView().addObject("listaQuestionario", lista);
	}
	
	public ModelAndView gerarContrato(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Processojuridico> lista = processojuridicoService.findForGeracaoContrato(whereIn);
		Empresa empresa = empresaService.loadPrincipal();
		
		boolean erro = false;
		boolean haveCliente;
		for (Processojuridico pj : lista) {
			if(contratojuridicoService.haveContratoNaoCancelado(pj)){
				erro = true;
				request.addError("J� existe um contrato n�o cancelado para o processo jur�dico " + pj.getCdprocessojuridico() + ".");
			}
			
			if(pj.getPrazopagamento() == null || pj.getPrazopagamento().getListaPagamentoItem() == null){
				erro = true;
				request.addError("N�o existe condi��o de pagamento cadastrado para o processo jur�dico " + pj.getCdprocessojuridico() + ".");
			}
			
			haveCliente = false;
			for (Processojuridicocliente pjc : pj.getListaProcessojuridicocliente()) {
				if(pjc.getCliente() != null && pjc.getCliente().getCdpessoa() != null){
					haveCliente = true;
				}
			}
			
			if(!haveCliente){
				erro = true;
				request.addError("N�o existe cliente cadastrado para o processo jur�dico " + pj.getCdprocessojuridico() + ".");
			}
			
			if(pj.getArea() != null){
				if(pj.getArea().getCentrocusto() == null || pj.getArea().getCentrocusto().getCdcentrocusto() == null){
					erro = true;
					request.addError("N�o existe centro de custo cadastrado para a �rea " + pj.getArea().getDescricao() + ".");
				}
				if(pj.getArea().getContagerencial() == null || pj.getArea().getContagerencial().getCdcontagerencial() == null){
					erro = true;
					request.addError("N�o existe conta gerencial cadastrada para a �rea " + pj.getArea().getDescricao() + ".");
				}
			} else {
				erro = true;
				request.addError("N�o existe �rea cadastrada para o processo jur�dico " + pj.getCdprocessojuridico() + ".");
			}
		}
		
		if(erro) {
			if("true".equals(request.getParameter("entrada"))){
				return new ModelAndView("redirect:/juridico/crud/ProcessoJuridico?ACAO=consultar&cdprocessojuridico=" + whereIn);
			} else {
				return sendRedirectToAction("listagem");
			}
		}
		
		for (Processojuridico processojuridico : lista) {
			processojuridicoService.gerarContrato(processojuridico, empresa);
		}
		
		request.addMessage("Contrato(s) gerado(s) com sucesso.");
		if("true".equals(request.getParameter("entrada"))){
			return new ModelAndView("redirect:/juridico/crud/ProcessoJuridico?ACAO=consultar&cdprocessojuridico=" + whereIn);
		} else {
			return sendRedirectToAction("listagem");
		}
	}
	
}
