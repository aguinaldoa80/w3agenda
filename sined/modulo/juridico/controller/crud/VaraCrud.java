package br.com.linkcom.sined.modulo.juridico.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Vara;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VaraFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Vara","/juridico/crud/Vara"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "municipio", "uf"})
public class VaraCrud extends CrudControllerSined<VaraFiltro, Vara, Vara>{
	
	@Override
	protected void salvar(WebRequestContext request, Vara bean)throws Exception {
		try {	
			
			super.salvar(request, bean);
		
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_vara_nome_municipio_uf")) throw new SinedException("Vara j� cadastrada no sistema.");
			else throw e;
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Vara bean)throws Exception {
		try {			
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException e) {
			throw new SinedException("Vara n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}

}
