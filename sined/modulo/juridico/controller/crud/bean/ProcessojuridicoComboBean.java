package br.com.linkcom.sined.modulo.juridico.controller.crud.bean;

import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Processojuridico;

public class ProcessojuridicoComboBean {

	private Integer cdprocessojuridico;
	private String descricaoCombo;
	
	public ProcessojuridicoComboBean(Processojuridico processojuridico) {
		if(processojuridico != null){
			this.cdprocessojuridico = processojuridico.getCdprocessojuridico();
			String descricao = processojuridico.getDescricaoCombo();
			this.descricaoCombo = descricao != null ? new StringUtils().addScapesToDescription(processojuridico.getDescricaoCombo()) : "";
		}
	}
	
	public Integer getCdprocessojuridico() {
		return cdprocessojuridico;
	}
	public String getDescricaoCombo() {
		return descricaoCombo;
	}
	public void setCdprocessojuridico(Integer cdprocessojuridico) {
		this.cdprocessojuridico = cdprocessojuridico;
	}
	public void setDescricaoCombo(String descricaoCombo) {
		this.descricaoCombo = descricaoCombo;
	}
}
