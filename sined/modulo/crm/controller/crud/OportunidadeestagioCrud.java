package br.com.linkcom.sined.modulo.crm.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Oportunidadeestagio;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadeestagioFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/crm/crud/Oportunidadeestagio",
		authorizationModule=CrudAuthorizationModule.class
)
public class OportunidadeestagioCrud extends CrudControllerSined<OportunidadeestagioFiltro, Oportunidadeestagio, Oportunidadeestagio>{

}
