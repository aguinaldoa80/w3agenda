package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ContatotipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/crm/crud/Contatotipo",
		authorizationModule=CrudAuthorizationModule.class
)
public class ContatotipoCrud extends CrudControllerSined<ContatotipoFiltro, Contatotipo, Contatotipo>{

	@Override
	protected void salvar(WebRequestContext request, Contatotipo bean)	throws Exception {
		try {
			bean.setNome(bean.getNome().trim());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CONTATOTIPO_NOME")) {
				throw new SinedException("Tipo de contato j� cadastrado no sistema.");
			}
		}
	}
	
}
