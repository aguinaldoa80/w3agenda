package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteDocumentoTipo;
import br.com.linkcom.sined.geral.bean.ClienteEmpresa;
import br.com.linkcom.sined.geral.bean.ClienteLicencaSipeagro;
import br.com.linkcom.sined.geral.bean.ClienteSegmento;
import br.com.linkcom.sined.geral.bean.Clientearquivo;
import br.com.linkcom.sined.geral.bean.Clienteespecialidade;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatoemail;
import br.com.linkcom.sined.geral.bean.Contacrmcontatofone;
import br.com.linkcom.sined.geral.bean.Contacrmsegmento;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Especialidade;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leademail;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Leadtelefone;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Tipopessoacrm;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ClienteDocumentoTipoService;
import br.com.linkcom.sined.geral.service.ClienteEmpresaService;
import br.com.linkcom.sined.geral.service.ClientePrazoPagamentoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ClienteespecialidadeService;
import br.com.linkcom.sined.geral.service.ClientehistoricoService;
import br.com.linkcom.sined.geral.service.ClientevendedorService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.ContacontabilService;
import br.com.linkcom.sined.geral.service.ContacrmcontatoemailService;
import br.com.linkcom.sined.geral.service.ContacrmcontatofoneService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EspecialidadeService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OportunidadesituacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.PessoaContatoService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PessoaquestionarioService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.ProspeccaoService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.geral.service.TipooperacaoService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;
import br.com.linkcom.sined.modulo.crm.controller.process.ContatoProcess;
import br.com.linkcom.sined.modulo.faturamento.controller.process.ImportarXmlDeclaracaoImportacaoProcess;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.DeclaracaoImportacaoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.ValidacaoCampoObrigatorio;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(
		path={	
				"/crm/crud/Cliente",
				"/financeiro/crud/Cliente",
				"/faturamento/crud/Cliente",
				"/juridico/crud/Cliente",
				"/producao/crud/Cliente"
			},
		authorizationModule=CrudAuthorizationModule.class
)
public class ClienteCrud extends CrudControllerSined<ClienteFiltro, Cliente, Cliente>{

	private PropostaService propostaService;
	private ProspeccaoService prospccaoService;
	private MunicipioService municipioService;
	private EnderecoService enderecoService;
	private TelefoneService telefoneService;
	private PessoaService pessoaService;
	private ClienteService clienteService;
	private ClientehistoricoService clientehistoricoService;
	private OportunidadeService oportunidadeService;
	private ContacrmcontatoemailService contacrmcontatoemailService;
	private ContacrmcontatofoneService contacrmcontatofoneService;
	private ArquivoService arquivoService;
	private ParametrogeralService parametrogeralService;
	private OportunidadesituacaoService oportunidadesituacaoService;
	private UsuarioService usuarioService;
	private PedidovendaService pedidovendaService;
	private LeadService leadService;
	private LeadhistoricoService leadhistoricoService;
	private ClientevendedorService clientevendedorService;
	private ContratoService contratoService;
	private EmpresaService empresaService;
	private ConfiguracaonfeService configuracaonfeService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private ClienteDocumentoTipoService clienteDocumentoTipoService;
	private ClientePrazoPagamentoService clientePrazoPagamentoService;
	private ValecompraService valecompraService;
	private ReportTemplateService reportTemplateService; 
	private ClienteEmpresaService clienteEmpresaService;
	private ContacontabilService contacontabilService;
	private TipooperacaoService tipooperacaoService;
	private PessoaquestionarioService pessoaquestionarioService;
	private DocumentotipoService documentotipoService;
	private ClienteespecialidadeService clienteespecialidadeService;
	private EspecialidadeService especialidadeService;
	private PessoaContatoService pessoaContatoService;
	private UfService ufService;
	
	public void setPropostaService(PropostaService propostaService) {this.propostaService = propostaService;}
	public void setProspccaoService(ProspeccaoService prospccaoService) {this.prospccaoService = prospccaoService;}
	public void setUfService(UfService ufService) {this.ufService = ufService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setClientevendedorService(ClientevendedorService clientevendedorService) {this.clientevendedorService = clientevendedorService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}		
	public void setContacrmcontatofoneService(ContacrmcontatofoneService contacrmcontatofoneService) {this.contacrmcontatofoneService = contacrmcontatofoneService;}
	public void setContacrmcontatoemailService(ContacrmcontatoemailService contacrmcontatoemailService) {this.contacrmcontatoemailService = contacrmcontatoemailService;}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setClientehistoricoService(ClientehistoricoService clientehistoricoService) {this.clientehistoricoService = clientehistoricoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}	
	public void setOportunidadesituacaoService(OportunidadesituacaoService oportunidadesituacaoService) {this.oportunidadesituacaoService = oportunidadesituacaoService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setLeadService(LeadService leadService) {this.leadService = leadService;}
	public void setLeadhistoricoService(LeadhistoricoService leadhistoricoService) {this.leadhistoricoService = leadhistoricoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {this.pedidovendahistoricoService = pedidovendahistoricoService;}
	public void setClienteDocumentoTipoService(ClienteDocumentoTipoService clienteDocumentoTipoService) {this.clienteDocumentoTipoService = clienteDocumentoTipoService;	}
	public void setClientePrazoPagamentoService(ClientePrazoPagamentoService clientePrazoPagamentoService) {this.clientePrazoPagamentoService = clientePrazoPagamentoService;	}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setClienteEmpresaService(ClienteEmpresaService clienteEmpresaService) {this.clienteEmpresaService = clienteEmpresaService;}
	public void setContacontabilService(ContacontabilService contacontabilService) {this.contacontabilService = contacontabilService;}
	public void setTipooperacaoService(TipooperacaoService tipooperacaoService) {this.tipooperacaoService = tipooperacaoService;}
	public void setPessoaquestionarioService(PessoaquestionarioService pessoaquestionarioService) {this.pessoaquestionarioService = pessoaquestionarioService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setClienteespecialidadeService(ClienteespecialidadeService clienteespecialidadeService) {this.clienteespecialidadeService = clienteespecialidadeService;}
	public void setEspecialidadeService(EspecialidadeService especialidadeService) {this.especialidadeService = especialidadeService;}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {this.pessoaContatoService = pessoaContatoService;}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
		
	@Override
	protected Cliente criar(WebRequestContext request, Cliente form) throws Exception {
		form.setCdpessoa(null);
		request.setAttribute("isRecuperarPessoa", "true");
		super.criar(request, form);
		String param = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORCLIENTE);
		if(param != null && param.toUpperCase().equals("TRUE")){
			Integer identificador = Integer.parseInt(clienteService.geraIdentificadorSemDV());
			form.setIdentificador(identificador.toString());
			form.setIsIdentificadorGerado(true);
			request.setAttribute("geracaoIdentificador", true);
		} else
			form.setIsIdentificadorGerado(false);
		
		if("true".equals(request.getParameter("importacaoXMLDI"))){
			DeclaracaoImportacaoBean declaracaoImportacaoBean = (DeclaracaoImportacaoBean) request.getSession().getAttribute(ImportarXmlDeclaracaoImportacaoProcess.ATTR_BEAN_DI_SESSAO);
			if(declaracaoImportacaoBean != null && declaracaoImportacaoBean.getBeanXml() != null){
				form.setNome(declaracaoImportacaoBean.getBeanXml().getFornecedorNome());
				
				Endereco endereco = new Endereco();
				endereco.setNumero(declaracaoImportacaoBean.getBeanXml().getFornecedorNumero());
				endereco.setBairro(declaracaoImportacaoBean.getBeanXml().getFornecedorCidade());
				endereco.setComplemento(declaracaoImportacaoBean.getBeanXml().getFornecedorComplemento());
				endereco.setLogradouro(declaracaoImportacaoBean.getBeanXml().getFornecedorLogradouro());
				endereco.setPais(declaracaoImportacaoBean.getBeanXml().getFornecedorEstado());
				
				form.setListaEndereco(new ListSet<Endereco>(Endereco.class));
				form.getListaEndereco().add(endereco);
			}
		}
		return form;
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Cliente form)throws CrudException {
		String acao = request.getParameter("ACAO");
		if("criar".equalsIgnoreCase(acao) && "true".equalsIgnoreCase(request.getParameter("copiar"))){
			request.addError("N�o existe a funcionalidade de copiar cliente.");
			return sendRedirectToAction("listagem");
		}
		
		if (form.getCdpessoa() == null && !request.getBindException().hasErrors()) {
			form.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
		}
		if(form.getCdpessoa()==null && !request.getBindException().hasErrors()){
			form.setTipopessoa("F".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.PADRAO_TIPO_CLIENTE))?
								  Tipopessoa.PESSOA_FISICA: Tipopessoa.PESSOA_JURIDICA);
		}
		return super.doEntrada(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, ClienteFiltro filtro) throws Exception {
		request.setAttribute("TIPO_RESPONSAVEL_COLABORADOR", Tiporesponsavel.COLABORADOR);
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Cliente form) throws CrudException {
		String acao = request.getParameter("ACAO");
		Usuario usuario = SinedUtil.getUsuarioLogado();
		Boolean limitaracessoclientevendedor = ("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.LIMITAR_ACESSO_CLIENTEVENDEDOR)) ? true : false);
		if(usuarioService.isRestricaoClienteVendedor(usuario) && !clienteService.isUsuarioPertenceRestricaoclientevendedorForCliente(form, usuario, limitaracessoclientevendedor)){
			if(acao != null && "editar".equals(acao) && !limitaracessoclientevendedor){
				request.addError("Usu�rio n�o tem permiss�o para editar este cliente!");
				SinedUtil.redirecionamento(request, "/crm/crud/Cliente?ACAO=consultar&cdpessoa=" + form.getCdpessoa());
				return null;
			}else if(acao != null && "consultar".equals(acao) && limitaracessoclientevendedor){
				request.addError("Usu�rio n�o tem permiss�o para consultar este cliente!");
				SinedUtil.redirecionamento(request, "/crm/crud/Cliente");
				return null;
			}
		}
		if(request.getAttribute("isRecuperarPessoa")!=null && !request.getAttribute("isRecuperarPessoa").equals(true))
			request.setAttribute("isRecuperarPessoa",false);

		return super.doEditar(request, form);
	}
	
	@Override
	protected Cliente carregar(WebRequestContext request, Cliente cliente) throws Exception {
		cliente = super.carregar(request, cliente);
		
		// LOAD DA FOTO
		try {
			arquivoService.loadAsImage(cliente.getLogocliente());
			if(cliente.getLogocliente().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), cliente.getLogocliente().getCdarquivo());
				request.setAttribute("exibeFoto", true);
			}
			if(cliente.getListaClientearquivo()!=null && !cliente.getListaClientearquivo().isEmpty()){
				for (Clientearquivo clientearquivo : cliente.getListaClientearquivo()) {
					Arquivo arquivo = clientearquivo.getArquivo();
					if(arquivo!=null && arquivo.getCdarquivo()!=null){
						DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());	
					}
				}
			}
		} catch (Exception e) {
			request.setAttribute("exibeFoto", false);
		}
		
		return cliente;
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Cliente form) throws CrudException {
		return super.doExcluir(request, form);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Cliente bean) throws Exception {
		String itens = request.getParameter("itenstodelete");
		if(itens == null || itens.equals("")){
			itens = bean.getCdpessoa() + "";
		}
		
		if(clienteService.verificaOutrosRegistros(itens)){
			throw new SinedException("Cliente n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
		
		super.excluir(request, bean);
	}

	
	@Override
	@SuppressWarnings("unchecked")
	protected void salvar(WebRequestContext request, Cliente bean)throws Exception {
		if(bean.getIdentificador() != null && bean.getIdentificador().trim().equals(""))
			bean.setIdentificador(null);
		
		boolean isCriar = bean.getCdpessoa() == null;
		boolean updateIdentificador = false;
		Integer valorIdentificador = null;
		
		if(bean.getListaEndereco() == null){
			Set<Endereco> end = new HashSet<Endereco>();
			bean.setListaEndereco(end);
		}
		
		String param = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORCLIENTE);
		if(param != null && param.toUpperCase().equals("TRUE") && bean.getIdentificador() != null){
			if(clienteService.existeIdentificadorCliente(bean.getIdentificador(), bean.getCdpessoa())){
				bean.setIdentificador(clienteService.geraIdentificadorSemDV());
			}
			
			String identificador = parametrogeralService.getValorPorNome("ProximoIdentificadorCliente");
			if (identificador.equals(bean.getIdentificador())){
				valorIdentificador = Integer.parseInt(identificador);
				valorIdentificador++;
				updateIdentificador = true;
				//parametrogeralService.updateValorPorNome("ProximoIdentificadorCliente", valorIdentificador.toString());
			}
		}
		
		pessoaService.ajustaPessoaToSave(bean);
		
		if (isCriar){
			bean.setDtinsercao(new Timestamp(System.currentTimeMillis()));
			
			String bloquearvendanovocliente = parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEAR_VENDANOVOCLIENTE);
			if("TRUE".equalsIgnoreCase(bloquearvendanovocliente)){
				Restricao restricao = new Restricao();
				restricao.setDtrestricao(new Date(System.currentTimeMillis()));
				restricao.setMotivorestricao("NOVO CLIENTE");
				Set<Restricao> listaRestricao = new ListSet<Restricao>(Restricao.class); 
				if(bean.getListaRestricao() != null && !bean.getListaRestricao().isEmpty())
					listaRestricao = bean.getListaRestricao();
				listaRestricao.add(restricao);
				bean.setListaRestricao(listaRestricao);
			}
		} 
//		else {
//			Empresa empresa = empresaService.load(new Empresa(bean.getCdpessoa()));
//			if(empresa != null){
//				if(bean.getListaEndereco() != null && bean.getListaEndereco().size() > 1){
//					throw new SinedException("N�o � permitido salvar mais de um endere�o para o cliente, pois ele tamb�m � uma empresa.");
//				}
//			}
//		}
		//Verifica data de libera��o para acrescentar informa��es ao historico de pedido venda
		if(bean.getCdpessoa() != null){
			List<Restricao> listRestricao = clienteService.carregaRestricoesByCliente(bean.getCdpessoa());
			Set<Restricao> listaRestricaoBean =  bean.getListaRestricao();
			if(bean.getListaRestricao() != null && !bean.getListaRestricao().isEmpty() && !listRestricao.isEmpty()){		
				boolean dataDiferente = false;
				for (Restricao restricao : listaRestricaoBean){
					Restricao r = listRestricao.get(getCacheSeconds()+1);
					if (r.getDtliberacao() != null && restricao.getDtliberacao() != null){
						if (restricao.getDtliberacao().after(r.getDtliberacao())){
							dataDiferente = true;
						}					
					}else if (restricao.getDtliberacao() != null && r.getDtliberacao() == null){
						dataDiferente = true;
					}
				}
				if (dataDiferente){
					List<Pedidovenda> listaPedidoVenda = pedidovendaService.findByCliente(new Cliente(bean.getCdpessoa()));				
					if (!listaPedidoVenda.isEmpty()){
						for (Pedidovenda pedidos : listaPedidoVenda){
							if (pedidos.getPedidovendasituacao().getValue() == 4){													
								Pedidovendahistorico pedidovendaHistorico = new Pedidovendahistorico();
								pedidovendaHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));							
								pedidovendaHistorico.setAcao("Cliente liberado para realiza��o do pedido de venda");
								pedidovendaHistorico.setCdusuarioaltera(bean.getCdusuarioaltera());
								pedidovendaHistorico.setPedidovenda(new Pedidovenda(pedidos.getCdpedidovenda()));
								pedidovendahistoricoService.saveOrUpdate(pedidovendaHistorico);							
							}
						}				
					}
				}
			}
		}
		
		if(bean.getOportunidade() != null && bean.getOportunidade().getCdoportunidade() != null){
			bean.setOportunidade(oportunidadeService.loadForEntrada(bean.getOportunidade()));
			bean.setHistorico((bean.getHistorico() != null ? bean.getHistorico() + " " : "") + "Importa��o da Conta <a href=\"javascript:visualizarContacrm("+bean.getOportunidade().getContacrm().getCdcontacrm()+")\">"+bean.getOportunidade().getContacrm().getCdcontacrm()+"</a>.");
		}
		
		if (bean.getHistoricoLead() != null && !bean.getHistoricoLead().equals("")){
			bean.setHistorico((bean.getHistorico() != null ? bean.getHistorico() + " " : "") + bean.getHistoricoLead());
		}
		
		if(bean.getListaEndereco() != null){
			for (Endereco en : bean.getListaEndereco()) {
				if(en.getMunicipio() != null){
					en.getMunicipio().setUf(en.getUf());
				}
			}
		}
		
		Object attribute = request.getSession().getAttribute("listaContato" + (bean.getCdpessoa() != null ? bean.getCdpessoa() : ""));
		if (attribute != null) {
			bean.setListaContato((List<PessoaContato>) attribute);
		}
		
		if(bean.getLogin() != null && "".equals(bean.getLogin())){
			bean.setLogin(null);
		}
		
		String observacaoAlteracaoLimiteValidadeCredito =  clienteService.criaObservacaoAlteracaoLimiteValidadeCredito(bean);
		
		try{
			if(bean.getListaClientearquivo()!=null && !bean.getListaClientearquivo().isEmpty()){
				for (Clientearquivo clientearquivo : bean.getListaClientearquivo()) {
					if(clientearquivo.getArquivo()!=null){
						arquivoService.saveFile(clientearquivo, "arquivo");
					}
				}
			}
			
			//Preenchimento do IE e IE estado
			if(Tipopessoa.PESSOA_FISICA.equals(bean.getTipopessoa())){
				if(bean.getInscricaoestadual() == null){
					bean.setInscricaoestadual("ISENTO");
				}
				if(bean.getUfinscricaoestadual() == null && bean.getListaEndereco() != null){
					Set<Endereco> listaEnderecos = bean.getListaEndereco();
					for (Endereco endereco : listaEnderecos) {
						if((Enderecotipo.FATURAMENTO.equals(endereco.getEnderecotipo()) || Enderecotipo.UNICO.equals(endereco.getEnderecotipo())) &&
							endereco.getMunicipio() != null){
							Municipio municipio = municipioService.carregaMunicipio(endereco.getMunicipio());
							bean.setUfinscricaoestadual(municipio.getUf());
							break;
						}
					}
				}
			}else {
				bean.setListaClienteespecialidade(null);
			}
			
			super.salvar(request, bean);

			if(updateIdentificador && valorIdentificador != null){
				parametrogeralService.updateValorPorNome("ProximoIdentificadorCliente", valorIdentificador.toString());
			}
			//Atualiza as filiais de acordo com o check marcado neste cliente
			if (Tipopessoa.PESSOA_JURIDICA.equals(bean.getTipopessoa()) && !isCriar)
				clienteService.updateContabilidadeCentralizada(bean);
			
			if (bean.getHistoricoLead() != null && !bean.getHistoricoLead().equals("")){
				String[] string = bean.getHistoricoLead().split(">");
				String[] string2 = string[1].split("<");
//				SALVA O LEAD COM O CLIENTE
				Lead lead = leadService.carregaLead(string2[0]);
				lead.setCliente(bean);
				lead.setDtconversao(new Date(System.currentTimeMillis()));
				leadService.updateClienteLead(lead);
//				SALVA O HIST�TICO DO LEAD
				Leadhistorico lh = new Leadhistorico();
				
				lh.setLead(lead);
				lh.setObservacao("Exporta��o para cliente <a href=\"javascript:visualizarCliente("+bean.getCdpessoa()+")\">"+bean.getCdpessoa()+"</a>");
				leadhistoricoService.saveOrUpdate(lh);
			}
			
			if(observacaoAlteracaoLimiteValidadeCredito != null){
				bean.setHistorico(observacaoAlteracaoLimiteValidadeCredito + (bean.getHistorico() != null ? " " + bean.getHistorico() : ""));
			}
			
			if(bean != null && ((bean.getHistorico() != null && !bean.getHistorico().trim().equals("")) || isCriar)){
				Clientehistorico clientehistorico = new Clientehistorico();
				
				clientehistorico.setCliente(bean);
				if(isCriar){
					clientehistorico.setObservacao(bean.getHistorico() != null && !bean.getHistorico().equals("") ?  "Cria��o do cliente - " + bean.getHistorico() : "Cria��o do cliente.");
				} else {
					clientehistorico.setObservacao(bean.getHistorico());
				}
				clientehistorico.setDtaltera(bean.getDtaltera());
				clientehistorico.setCdusuarioaltera(bean.getCdusuarioaltera());
				
				clientehistoricoService.saveOrUpdate(clientehistorico);
			}
			
			if("true".equals(request.getParameter("closeOnSave"))){
				request.getServletResponse().setContentType("text/html");
				View.getCurrent()
				.eval("<script type='text/javascript'>")
					.eval("window.opener.$s.showNoticeMessage('Cliente salvo com sucesso.');")
					.eval("window.opener.changeCliente('" + bean.getNome() + "');")
					.eval("window.top.close();")
				.eval("</script>").flush();
			}
			
			pedidovendaService.verificaPedidovendaComRestricaocliente(bean);
			
			if (isCriar && bean.getContaContabil()==null){
				criarContaContabil(bean);
			}
		}catch(DataIntegrityViolationException e){
			if(updateIdentificador){
				bean.setIdentificador(null);
			}
			if(DatabaseError.isKeyPresent(e, "IDX_PESSOA_EMAIL")){
				throw new SinedException("E-mail j� cadastrado no sistema.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_CLIENTE_IDENTIFICADOR")){
				throw new SinedException("Identificador j� cadastrado no sistema.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_PESSOA_CNPJ")){
				throw new SinedException("CNPJ j� cadastrado no sistema. Tente utilizar a op��o 'Recuperar pelo CNPJ', dispon�vel no �cone ao lado do campo.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_PESSOA_CPF")){
				throw new SinedException("CPF j� cadastrado no sistema. Tente utilizar a op��o 'Recuperar pelo CPF', dispon�vel no �cone ao lado do campo.");
			}
			if(DatabaseError.isKeyPresent(e, "endereco")){
				request.setAttribute("ErroAoDeletarEnd", "true");
				throw new SinedException("Endere�o n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_CLIENTE_24")){
				throw new SinedException("Login j� cadastrado no sistema.");
			}
			if(DatabaseError.isKeyPresent(e, "idx_ClienteDocumentoTipo")){
				throw new SinedException("Forma de Pagamento j� cadastrado.");
			}
			if(DatabaseError.isKeyPresent(e, "idx_ClientePrazoPagamento")){
				throw new SinedException("Condi��o de Pagamento j� cadastrado.");
			}
			if(DatabaseError.isKeyPresent(e, "idx_uq_clienteempresa_0")){
				throw new SinedException("Empresa j� cadastrada.");
			}
			throw e;
		}catch(Exception e){
			e.printStackTrace();
			throw new SinedException(e.getMessage());
		}
		

		if(!isCriar && StringUtils.isNotBlank(parametrogeralService.buscaValorPorNome(Parametrogeral.URL_SINC_CONTRATO_ARVOREACESSO))){
			clienteService.sincronizarClienteArvoreAcesso(bean);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected ListagemResult<Cliente> getLista(WebRequestContext request,ClienteFiltro filtro) {
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoClienteVendedor(usuario)){
			filtro.setLimitaracessoclientevendedor("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.LIMITAR_ACESSO_CLIENTEVENDEDOR)) ? Boolean.TRUE : Boolean.FALSE);
		}else {
			filtro.setLimitaracessoclientevendedor(false);
		}
		
		ListagemResult<Cliente> listagemResult = super.getLista(request, filtro);
		List<Cliente> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdpessoa", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(clienteService.findListagem(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}		
		
		for (Cliente c : list) {
			c.setListaTelefone(SinedUtil.listToSet(telefoneService.findByPessoa(c), Telefone.class));
			
			if(c.getCpf() != null){
				c.setCpfcnpj(c.getCpf().toString());
			}else if(c.getCnpj() != null){
				c.setCpfcnpj(c.getCnpj().toString());
			}
			
			if(c.getListaTelefone()!= null){
				c.setListaTelefones(telefoneService.createListaTelefone(c.getListaTelefone(), "<br>"));
			}
			
		}
		return listagemResult;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Cliente form)throws Exception {
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		Boolean limitaracessoclientevendedor = ("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.LIMITAR_ACESSO_CLIENTEVENDEDOR)) ? true : false);
		Boolean restricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(usuarioLogado);
		boolean usuarioPertenceRestricaoclientevendedor = form.getCdpessoa() != null ? clienteService.isUsuarioPertenceRestricaoclientevendedorForCliente(form, usuarioLogado, limitaracessoclientevendedor) : true;
		boolean obrigaCamposArvoreAcesso = StringUtils.isNotBlank(parametrogeralService.buscaValorPorNome(Parametrogeral.URL_SINC_CONTRATO_ARVOREACESSO));
		boolean restricao_cliente_empresa = SinedUtil.isRestricaoEmpresaClienteUsuarioLogado();
		
		request.getBindException();
		
		
		if(request.getAttribute("ErroAoDeletarEnd") !=null && request.getAttribute("ErroAoDeletarEnd").equals("true")){
			pessoaService.setListaEnderecoForEntrada(form);
		}
		
		if(request.getParameter(ACTION_PARAMETER).equals(ENTRADA) && restricaoClienteVendedor
				&& !usuarioPertenceRestricaoclientevendedor){
			request.addError("Voc� n�o tem permiss�o para editar esse cliente.");
			goToAction("listagem");
		}
		
		request.setAttribute("exibirAbaVendedores", !restricaoClienteVendedor || (restricaoClienteVendedor && usuarioPertenceRestricaoclientevendedor));
		
		List<ReportTemplateBean> reportTemplateBean = null;
		try {
			reportTemplateBean = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.DISCRIMINACAO_DOS_SERVICOS);
		} catch (Exception e) {}
		request.setAttribute("listaCategoriaTemplate", reportTemplateBean != null ? reportTemplateBean : new ListSet<ReportTemplateBean>(ReportTemplateBean.class));		
		
		if (request.getSession().getAttribute("cdLead") != null){
			Lead lead = leadService.carregaLead(request.getSession().getAttribute("cdLead").toString());
			request.getSession().removeAttribute("cdLead");
			form.setNome(lead.getNome());
			form.setAtivo(Boolean.TRUE);
			form.setEmail(lead.getEmail());

			ListSet<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class);
			List<PessoaContato> listaPessoaContato = new ArrayList<PessoaContato>();
			PessoaContato pessoaContato = null;
			
			//Campos abaixo s� ser�o preenchidos caso parametrogeral.SUGERIR_CAMPOS_LEAD_CLIENTE == TRUE, SET no LeadCrud.verificarParametroLead
			if(request.getSession().getAttribute("sugerirCamposLeadCliente").equals(Boolean.TRUE)){
								
				for (Leadtelefone lt : lead.getListLeadtelefone()) {
					Telefone telefone = new Telefone();
					telefone.setTelefone(lt.getTelefone());
					telefone.setTelefonetipo(lt.getTelefonetipo());
					listaTelefone.add(telefone);
				}
				form.setListaTelefone(listaTelefone);
				
				Endereco endereco = new Endereco();
				endereco.setEnderecotipo(Enderecotipo.UNICO);
				endereco.setLogradouro(lead.getLogradouro());
				endereco.setNumero(lead.getNumero());
				endereco.setBairro(lead.getBairro());
				endereco.setMunicipio(lead.getMunicipio());
				endereco.setUf(lead.getUf());
				endereco.setCep(lead.getCep());
				if(lead.getMunicipio() != null && lead.getMunicipio().getCdmunicipio() != null){
					endereco.setCdmunicipioaux(lead.getMunicipio().getCdmunicipio());
				}
				ListSet<Endereco> listaEndereco = new ListSet<Endereco>(Endereco.class);
				listaEndereco.add(endereco);
				form.setListaEndereco(listaEndereco);
			}
		
			form.setHistoricoLead("Importado do Lead <a href=\"javascript:visualizarLead(" + 
						lead.getCdlead() + ");\">" + lead.getCdlead() + "</a>");
	
			Contato contato;
			if(SinedUtil.isListNotEmpty(lead.getListleademail())){
				for(Leademail leademail : lead.getListleademail()){
					contato = new Contato();
					contato.setNome(leademail.getContato());
					contato.setEmailcontato(leademail.getEmail());
					
					pessoaContato = new PessoaContato();
					pessoaContato.setPessoa(form);
					pessoaContato.setContato(contato);
					
					listaPessoaContato.add(pessoaContato);
				}
			}
			if(SinedUtil.isListNotEmpty(listaPessoaContato)){
				form.setListaContato(listaPessoaContato);
			}
		}
		
		pessoaService.ajustaPessoaToLoad(form);
		if(form.getCdpessoa() != null) request.setAttribute("cdpessoaParam", form.getCdpessoa());
		
		if (form.getCdpessoa() == null && !request.getBindException().hasErrors()) {
			form.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
		}
		
		if(form.getCpf() != null && form.getCpf().getValue() != null){
			form.setTipopessoa(Tipopessoa.PESSOA_FISICA);
		}else if(form.getCnpj() != null && form.getCnpj().getValue() != null){
			form.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
		}
		
		if (!request.getBindException().hasErrors() && form.getCdpessoa() != null) {
			List<PessoaContato> listaPessoaContato = pessoaContatoService.findByPessoa(form);
			form.setListaContato(listaPessoaContato);
		}
		if(request.getBindException().hasErrors()){
			Object attribute = request.getSession().getAttribute("listaContato" + (form.getCdpessoa() != null ? form.getCdpessoa() : ""));
			if (attribute != null) {
				form.setListaContato((List<PessoaContato>)attribute);		
			}
		}
		request.getSession().setAttribute("listaContato" + (form.getCdpessoa() != null ? form.getCdpessoa() : ""), form.getListaContato());

		if(form.getCdpessoa() != null){
			form.setListClientehistorico(clientehistoricoService.findByCliente(form));
			form.setListaClientevendedor(SinedUtil.listToSet(clientevendedorService.findByClienteForAgenciaColaborador(form), Clientevendedor.class));
			form.setListDocumentoTipo(clienteDocumentoTipoService.findByCliente(form));
			form.setListPrazoPagamento(clientePrazoPagamentoService.findByCliente(form));
			
			form.setListaQuestionario(SinedUtil.listToSet(pessoaquestionarioService.carregarPessoaQuestionario(form), Pessoaquestionario.class));
			form.setListaQuestionarioWithAtividadetipo(SinedUtil.listToSet(pessoaquestionarioService.carregarPessoaquestionarioByInteracao(form), Pessoaquestionario.class));
			
			form.setListaValecompra(valecompraService.findByCliente(form));
			form.setSaldoValecompra(valecompraService.getSaldoByCliente(form.getListaValecompra()));
			form.setListaClienteespecialidade(clienteespecialidadeService.findByCliente(form));
		}

		Map<Integer, List<Municipio>> mapMunicipio = new HashMap<Integer, List<Municipio>>();
		if(SinedUtil.isListNotEmpty(form.getListaEndereco())){
			if(form.getListaEndereco() != null){
				for (Endereco endereco : form.getListaEndereco()) {
					if(endereco != null && 
							endereco.getMunicipio() != null && 
							endereco.getMunicipio().getUf() != null &&
							endereco.getMunicipio().getUf().getCduf() != null &&
							!mapMunicipio.containsKey(endereco.getMunicipio().getUf().getCduf())){
						mapMunicipio.put(endereco.getMunicipio().getUf().getCduf(), municipioService.findByUf(endereco.getMunicipio().getUf()));
					}
				}
			}
		}
		request.setAttribute("mapMunicipio", mapMunicipio);
		
		String param = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORCLIENTE);
		if(param != null && param.toUpperCase().equals("TRUE")){
			request.setAttribute("geracaoIdentificador", true);
			form.setIsIdentificadorGerado(true);
		} else
			form.setIsIdentificadorGerado(false);	
		
		if(request.getParameter("isClienteVendaEdicao") != null && request.getParameter("isClienteVendaEdicao").equals("true"))
			form.setIsClienteVendaEdicao(Boolean.TRUE);
		
		String paramObrigar = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CATEGORIA_CLIENTE);
		if(paramObrigar != null && "TRUE".equals(paramObrigar.toUpperCase())){
			request.setAttribute("obrigarCategoriaCliente", true);
		}else{
			request.setAttribute("obrigarCategoriaCliente", false);
		}
		
		Usuario usuario = usuarioLogado;
		if(usuarioService.isRestricaoClienteVendedor(usuario) && form.getCdpessoa() != null &&
				!clienteService.isUsuarioPertenceRestricaoclientevendedorForCliente(form, usuario, limitaracessoclientevendedor)){
			request.setAttribute("restricaoclientevendedor", true);
		}else {
			request.setAttribute("restricaoclientevendedor", false);
		}
		
		if(usuarioService.isRestricaoClienteVendedor(usuario) && form.getCdpessoa() != null && !clienteService.isUsuarioPertenceRestricaoclientevendedorForCliente(form, usuario,limitaracessoclientevendedor)){
			request.setAttribute("editarvendedores", true);
		}else {
			request.setAttribute("editarvendedores", false);
		}
		
		if (form.getContaContabil() != null){
			form.setContaContabil(new ContaContabil(form.getContaContabil().getCdcontacontabil()));
		}
		
		Boolean existCdpessoaTabelaEmpresa = false;
		if(form.getCdpessoa() != null){
			existCdpessoaTabelaEmpresa = empresaService.existCdpessoaTabelaEmpresa(form.getCdpessoa());
		}
		if(obrigaCamposArvoreAcesso){
			if(form.getListaClienteSegmento()==null || form.getListaClienteSegmento().size()==0){
				if(form.getListaClienteSegmento()==null) form.setListaClienteSegmento(new ListSet<ClienteSegmento>(ClienteSegmento.class));
				form.getListaClienteSegmento().add(new ClienteSegmento(true));
			}
			
			if(form.getListaTelefone()==null || form.getListaTelefone().size()==0){
				if(form.getListaTelefone()==null) form.setListaTelefone(new HashSet<Telefone>());
				form.getListaTelefone().add(new Telefone());
			}
		}
		
		form.setListaClienteEmpresa(clienteEmpresaService.findByCliente(form));
		request.setAttribute("listaEmpresa", empresaService.findByUsuario());
		
		if(form.getCdpessoa()==null && !request.getBindException().hasErrors()){
			form.setTipopessoa("F".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.PADRAO_TIPO_CLIENTE))?
								  Tipopessoa.PESSOA_FISICA: Tipopessoa.PESSOA_JURIDICA);
		}
		request.setAttribute("existCdpessoaTabelaEmpresa", existCdpessoaTabelaEmpresa);
		request.setAttribute("isNfeServicoBH", configuracaonfeService.havePrefixoativo(Prefixowebservice.BHISS_PROD));
		request.setAttribute(ContatoProcess.NOME_CLASSE_PAI_CONTATO, "Cliente");
		request.setAttribute("ObrigaCPF", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGARCPFCLIENTE))? "TRUE": "FALSE");
		request.setAttribute("ObrigaCNPJ", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CNPJ_CLIENTE))? "TRUE": "FALSE");
		request.setAttribute("OBRIGAR_RAZAOSOCIAL_CLIENTE", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_RAZAOSOCIAL_CLIENTE));
		request.setAttribute("ObrigaCamposArvoreAcesso", obrigaCamposArvoreAcesso );
		request.setAttribute("RESTRICAO_CLIENTE_EMPRESA", restricao_cliente_empresa);
		request.setAttribute("listaTipooperacao", tipooperacaoService.findDebitoCredito());
		String whereInCddocumentotipo = null;
		if(form.getCdpessoa() != null && SinedUtil.isListNotEmpty(form.getListDocumentoTipo())){
			List<String> listaWhereinDocumentotipo = new ArrayList<String>();
			for(ClienteDocumentoTipo cdt: form.getListDocumentoTipo()){
				if(cdt.getDocumentotipo() != null){
					listaWhereinDocumentotipo.add(cdt.getDocumentotipo().getCddocumentotipo().toString());
				}
			}
			whereInCddocumentotipo = CollectionsUtil.concatenate(listaWhereinDocumentotipo, ",");
		}
		request.setAttribute("listaDocumentotipo", documentotipoService.getListaDocumentoTipoUsuarioForVenda(whereInCddocumentotipo));
		request.setAttribute("obrigaEmailTelefone", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_EMAIL_TELEFONE_CLIENTE));
		
		if(Hibernate.isInitialized(form.getListaClienteEmpresa()) && SinedUtil.isListNotEmpty(form.getListaClienteEmpresa())){
			Collections.sort(form.getListaClienteEmpresa(), new Comparator<ClienteEmpresa>(){
				public int compare(ClienteEmpresa a1, ClienteEmpresa a2){
					return new SinedUtil().getUsuarioPermissaoEmpresa(a1.getEmpresa()) ? 1 : 0; 
				}
			});
		}
		
		request.setAttribute("CLIENTE_EDICAO_VALE_COMPRA", Boolean.TRUE.equals(SinedUtil.isUserHasAction("CLIENTE_EDICAO_VALE_COMPRA")));
		request.setAttribute("CLIENTE_EDICAO_RESTRICAO", Boolean.TRUE.equals(SinedUtil.isUserHasAction("CLIENTE_EDICAO_RESTRICAO")));
		request.setAttribute("CLIENTE_EDICAO_FORMAPAGAMENTO", Boolean.TRUE.equals(SinedUtil.isUserHasAction("CLIENTE_EDICAO_FORMAPAGAMENTO")));
		request.setAttribute("CLIENTE_EDICAO_CONDICAOPAGAMENTO", Boolean.TRUE.equals(SinedUtil.isUserHasAction("CLIENTE_EDICAO_CONDICAOPAGAMENTO")));
		request.setAttribute("OBRIGAR_VENDEDORPRINCIPAL_VENDA", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_VENDEDORPRINCIPAL_VENDA));
		request.setAttribute("PRODUTO_CONTROLADO", parametrogeralService.getBoolean(Parametrogeral.PRODUTO_CONTROLADO));
		request.setAttribute("CLIENTE", Boolean.TRUE);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void validateBean(Cliente bean, BindException errors) {
		enderecoService.validateListaEndereco(errors, bean.getListaEndereco());		
		
		if (bean.getListaRestricao() != null) {
			Set<Restricao> listaRestricao = bean.getListaRestricao();
			for (Restricao restricao : listaRestricao) {
				if (restricao.getDtliberacao() != null && restricao.getDtrestricao().getTime() > restricao.getDtliberacao().getTime()) {
					errors.reject("002","Data de restri��o n�o pode ser maior que a data de libera��o.");
					break;
				}
			}
		}
		
		String paramObrigar = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CATEGORIA_CLIENTE);
		if(paramObrigar != null && "TRUE".equals(paramObrigar.toUpperCase()) && (bean.getListaPessoacategoria() == null || 
				bean.getListaPessoacategoria().isEmpty())){
			errors.reject("001","O cliente deve ter pelo menos uma categoria.");
		}
		
		
		if (!bean.getAtivo()){		
			if (clienteService.load(bean, "cliente.ativo").getAtivo()){
				List<Contrato> listaContrato = contratoService.findBycliente(bean);
				for (Contrato c : listaContrato) {
					if (contratoService.haveContratoSituacao(c.getCdcontrato().toString(), SituacaoContrato.EM_ESPERA, SituacaoContrato.NORMAL, SituacaoContrato.A_CONSOLIDAR, SituacaoContrato.ATENCAO, SituacaoContrato.ATRASADO, SituacaoContrato.ISENTO)){
						errors.reject("003","Existem contratos vigentes para este cliente");
						break;
					}
				}
			}
		}
		boolean obrigaVendedorPrincipal = parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_VENDEDORPRINCIPAL_VENDA);
		if(obrigaVendedorPrincipal){
			boolean possuiVendedorPrincipal = false;
			for(Clientevendedor clientevendedor: bean.getListaClientevendedor()){
				if(Boolean.TRUE.equals(clientevendedor.getPrincipal())){
					possuiVendedorPrincipal = true;
					break;
				}
			}
			if(!possuiVendedorPrincipal){
				errors.reject("004", "Para salvar o cadastro de cliente deve existir um vendedor marcado como 'vendedor principal' na aba Vendedor.");
			}
		}
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.GERAR_CONTACONTABIL_RAZAOSOCIAL_CLIENTE))
				&& Tipopessoa.PESSOA_JURIDICA.equals(bean.getTipopessoa())
				&& (bean.getRazaosocial()==null || bean.getRazaosocial().trim().equals(""))){
			errors.reject("005", "O campo raz�o social � obrigat�rio. Motivo: O sistema cria a conta cont�bil do cliente considerando a raz�o social do cliente.");
		}
		
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaContato" + (bean.getCdpessoa() != null ? bean.getCdpessoa() : ""));
		if (attribute != null) {
			bean.setListaContato((List<PessoaContato>) attribute);
		}else{
			errors.reject("001", "Sess�o expirada. Favor fazer o processo novamente recarregando a p�gina.");
		}
		
		if(bean.getCdpessoa() != null){
			List<PessoaContato> listaContato = pessoaContatoService.findByPessoa(bean);
			boolean verificaContato;
			int i=0;
			if(SinedUtil.isListNotEmpty(listaContato)){
				for(PessoaContato contato : listaContato){
					verificaContato = false;
					for(PessoaContato c : bean.getListaContato()){
						if(contato.equals(c)){
							verificaContato = true;
							break;
						}
						
					}
					if(!verificaContato){
						if(contato.getContato() != null){
							if(prospccaoService.verificaProspeccaoPorContato(contato.getContato()) || propostaService.verificaPropostaPorContato(contato.getContato()) ){
								bean.getListaContato().add(i, contato);
								errors.reject("006","Contato "+contato.getContato().getNome()+ " n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema");
							
							}
						}
					}
					i++;
				}
			}
		}
		
		if (bean.getListaClienteLicencaSipeagro() != null) {
			for (ClienteLicencaSipeagro licenca : bean.getListaClienteLicencaSipeagro()) {
				if (licenca.getDtinicio() != null && licenca.getDtinicio().getTime() > licenca.getDtfim().getTime()) {
					errors.reject("002","Data de inicio da licen�a n�o pode ser maior que a data de fim.");
					break;
				}
			}
		}
	}
	
	/**
	 * M�todo para filtrar os municipios por uf.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MunicipioService#findByUf(Uf)
	 * @param request
	 * @param departamento
	 * @author Fl�vio
	 */
	public ModelAndView makeAjaxMunicipio(WebRequestContext request, Uf uf){
		List<Municipio> listaMunicipio = municipioService.findByUf(uf);
		return new JsonModelAndView().addObject("listaMunicipio", listaMunicipio);
	}
	
	/**
	 * Veirifica se existe alguma pessoa cadastrada no banco com o mesmo cpf ou cnpj.
	 *
	 * @param request
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	public void verificaPessoa(WebRequestContext request, ClienteFiltro filtro){
		Pessoa pessoa;
		if(filtro.getCpf() != null){
			pessoa = pessoaService.findPessoaByCpf(filtro.getCpf());
		} else if(filtro.getCnpj() != null){
			pessoa = pessoaService.findPessoaByCnpj(filtro.getCnpj());
		} else {
			throw new SinedException("Erro na obten��o da pessoa.");
		}
		
		String string = "";
		if(pessoa != null){
			string += "var achou = true;";
			
			Cliente cliente = clienteService.load(pessoa.getCdpessoa());
			if(cliente != null){
				string += "var clienteAchou = true;";
			} else {
				string += "var clienteAchou = false;";
				string += "var nomeCliente = '" +  SinedUtil.escapeSingleQuotes(pessoa.getNome()) + "';";
				string += "var cdpessoaCliente = " + pessoa.getCdpessoa() + ";";
			}
		} else {
			string += "var achou = false;";
		}
		
		View.getCurrent().println(string);
	}
	
	public void verificaEmpresa(WebRequestContext request, ClienteFiltro filtro){
		Pessoa pessoa;
		if(filtro.getCpf() != null){
			pessoa = pessoaService.findPessoaByCpf(filtro.getCpf());
		} else if(filtro.getCnpj() != null){
			pessoa = pessoaService.findPessoaByCnpj(filtro.getCnpj());
		} else {
			throw new SinedException("Erro na obten��o da pessoa.");
		}
		String string = "";

		if(pessoa != null){
			string += "var achou = true;";
			
			Empresa empresa = empresaService.load(new Empresa(pessoa.getCdpessoa()));
			if(empresa != null){
				string += "var clienteAchou = true;";
			} else {
				string += "var clienteAchou = false;";
				string += "var nomeCliente = '" +  SinedUtil.escapeSingleQuotes(pessoa.getNome()) + "';";
				string += "var cdpessoaCliente = " + pessoa.getCdpessoa() + ";";
			}
		} else {
			string += "var achou = false;";
		}
		
		View.getCurrent().println(string);
	}
	
	public void verificaNomeCPF(WebRequestContext request, Pessoa pessoa){
		if(pessoa.getCpf() != null){
			pessoaService.verificaNomeCPFCNPJ(pessoa.getCpf().getValue());
		} else {
			View.getCurrent().println("alert('N�o foi encontrado o CPF.');");
		}
	}
	
	public void verificaNomeCNPJ(WebRequestContext request, Pessoa pessoa){
		if(pessoa.getCnpj() != null){
			pessoaService.verificaNomeCPFCNPJ(pessoa.getCnpj().getValue());
		} else {
			View.getCurrent().println("alert('N�o foi encontrado o CNPJ.');");
		}
	}
	
	public void verificaSimples(WebRequestContext request, Pessoa pessoa){
		if(pessoa.getCnpj() != null){
			pessoaService.verificaSimples(pessoa.getCnpj().getValue());
		} else {
			View.getCurrent().println("alert('N�o foi encontrado o CNPJ.');");
		}
	}
	
	
	/**
	 * Recupera a pessoa e manda para edi��o do cliente.
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @throws CrudException
	 * @author Rodrigo Freitas
	 */
	public ModelAndView recuperarPessoa(WebRequestContext request, Cliente cliente) throws CrudException{
		String param = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORCLIENTE);
		Integer identificador = 0;
		if(param != null && param.toUpperCase().equals("TRUE")){
			identificador = Integer.parseInt(clienteService.geraIdentificadorSemDV());
			cliente.setIdentificador(identificador.toString());
			cliente.setIsIdentificadorGerado(true);
			request.setAttribute("geracaoIdentificador", true);
			identificador++;
			parametrogeralService.updateValorPorNome("ProximoIdentificadorCliente", identificador.toString());
		} else
			cliente.setIsIdentificadorGerado(false);
		clienteService.insertSomenteCliente(cliente);
		request.setAttribute("isRecuperarPessoa", true);
		return doEditar(request, cliente);
	}
	
	
	/**
	 * M�todo para filtrar os mun�cipios do UF
	 * 
	 * @see MunicipioService#findByUf(br.com.linkcom.sined.geral.bean.Uf)
	 * @see SinedUtil#convertToJavaScript(List, String, String)
	 * @param request
	 * @param uf
	 * @author Taidson
	 * @since 25/04/2010
     * Estrutura copia do Sindis
	 */

	public void ajaxComboMunicipio(WebRequestContext request, Uf uf){
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		String municipioAux = request.getParameter("municipioRF");
		Municipio municipio = null;
		final Collator instance = Collator.getInstance();
		 instance.setStrength(Collator.NO_DECOMPOSITION);
		if (uf!=null && uf.getCduf()!=null){
			listaMunicipio = municipioService.findByUf(uf);			
		}
		if(municipioAux != null){
			for(Municipio m : listaMunicipio){
				if(instance.compare(m.getNome(), municipioAux) == 0){
					municipio = m;
					break;
				}
			}
		}
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaMunicipio, "listaMunicipio", ""));		
		View.getCurrent().println(municipio != null ?"var municipio = '" + municipio.getNome()+"';":"");
	}
	
	public void ajaxRetornarUfPorNome(WebRequestContext request){
		String nomeUf = request.getParameter("nomeUf");
		Uf uf = ufService.findBySigla(nomeUf);
		ajaxComboMunicipio(request, uf);
		List<Uf> listaUf = new ArrayList<Uf>();
		listaUf.add(uf);
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaUf, "listaUf", ""));
	}
	
	/**
	 * M�todo chamado via Ajax que gera um identificador
	 * 
	 * @param request
	 * @author Tom�s Rabelo
	 */
	public void ajaxGerarIdentificador(WebRequestContext request){
		String identificador = clienteService.geraIdentificadorSemDV();
		View.getCurrent().println("var identificador = "+identificador);
	}
	
	/**
	 * M�todo para carregar os mun�cipios do UF no carregamento das telas, pois sen�o carrega todos os munic�pios,
	 * assim, sobrecarregando os m�todos
	 * 
	 * @see MunicipioService#findByUf(br.com.linkcom.sined.geral.bean.Uf)
	 * @author Taidson
	 * @since 25/04/2010
     * Estrutura copia do Sindis
	 */
	public ModelAndView ajaxPreencheComboMunicipioCarregarTela(WebRequestContext request){
		Municipio municipio = null;
		String paramCdendereco = request.getParameter("cdendereco");
		String paramCdmunicipioaux = request.getParameter("cdmunicipioaux");
		
		if(paramCdendereco != null && !paramCdendereco.equals("")){
			Endereco endereco = enderecoService.loadEndereco(new Endereco(Integer.valueOf(paramCdendereco)));
			municipio = endereco.getMunicipio();
		} else if(paramCdmunicipioaux != null && !paramCdmunicipioaux.equals("")){
			municipio = new Municipio(Integer.parseInt(paramCdmunicipioaux));
		}
		
		Uf uf = new Uf(Integer.valueOf(request.getParameter("cduf")));
		List<Municipio> listaMunicipio = municipioService.findByUf(uf);
	
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("listaMunicipio", listaMunicipio);
		json.addObject("municipioSelecionado", municipio);
	
		return json;
	}
	
	 @SuppressWarnings("unchecked")
	public ModelAndView criarClienteOportunidade(WebRequestContext request, Oportunidade oportunidade) throws Exception{
		oportunidade = oportunidadeService.loadForCliente(oportunidade);
		
		Oportunidadesituacao situacaoantesfinal = oportunidadesituacaoService.findSituacaoantesfinal();
		Oportunidadesituacao situacaofinal = oportunidadesituacaoService.findSituacaofinal();
		
		if(situacaoantesfinal == null && situacaofinal == null){
			request.addError("N�o existe nenhuma situa��o da oportunidade cadastrada como antes da final e final.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}else if(!oportunidade.getOportunidadesituacao().equals(situacaoantesfinal) && !oportunidade.getOportunidadesituacao().equals(situacaofinal)){
			request.addError("Para gerar cliente a oportunidade tem que estar na situa��o '" + 
					(situacaoantesfinal != null && situacaoantesfinal.getNome() != null ? situacaoantesfinal.getNome().toUpperCase():"") +
					(situacaofinal.getNome() != null ? (situacaoantesfinal != null ? "' ou '" : "") + situacaofinal.getNome().toUpperCase():"") +"'.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}
		
		Contacrm contacrm = oportunidade.getContacrm();
		
		Cliente cliente = null;
		if(contacrm != null){
			if(contacrm.getTipo().equals(Tipopessoa.PESSOA_JURIDICA) && contacrm.getCnpj() != null){
				cliente = clienteService.findByCnpj(contacrm.getCnpj());
			} else if(contacrm.getTipo().equals(Tipopessoa.PESSOA_FISICA) && contacrm.getCpf() != null){
				cliente = clienteService.findByCpf(contacrm.getCpf());
			}
		}
		
		if(cliente != null){
			request.addError("J� existe o cadastro de cliente com este CNPJ/CPF.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}
		//Valida��o para evitar criar um cliente que j� esteja cadastrado no sistema
		if(oportunidade.getTipopessoacrm().equals(Tipopessoacrm.CLIENTE)){
			request.addError("J� existe o cadastro de cliente com este CNPJ/CPF.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}
		
		cliente = criar(request, new Cliente());
		
		if(contacrm != null){
			if(contacrm.getLead() != null && contacrm.getLead().getCdlead() != null){
				Lead lead = leadService.carregaLead(contacrm.getLead().getCdlead().toString());
				
				//Campos abaixo s� ser�o preenchidos caso parametrogeral.SUGERIR_CAMPOS_LEAD_CLIENTE == TRUE e Lead preenchido
				if(lead != null && parametrogeralService.getBoolean(Parametrogeral.SUGERIR_CAMPOS_LEAD_CLIENTE)){
				
					ListSet<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class);
					if(lead.getListLeadtelefone() != null){
						for (Leadtelefone lt : lead.getListLeadtelefone()) {
							Telefone telefone = new Telefone();
							telefone.setTelefone(lt.getTelefone());
							telefone.setTelefonetipo(lt.getTelefonetipo());
							listaTelefone.add(telefone);
						}
						cliente.setListaTelefone(listaTelefone);
					}
				
					Endereco endereco = new Endereco();
					endereco.setEnderecotipo(Enderecotipo.UNICO);
					endereco.setLogradouro(lead.getLogradouro());
					endereco.setNumero(lead.getNumero());
					endereco.setBairro(lead.getBairro());
					endereco.setMunicipio(lead.getMunicipio());
					endereco.setUf(lead.getUf());
					endereco.setCep(lead.getCep());
					if(lead.getMunicipio() != null && lead.getMunicipio().getCdmunicipio() != null){
						endereco.setCdmunicipioaux(lead.getMunicipio().getCdmunicipio());
					}
					ListSet<Endereco> listaEndereco = new ListSet<Endereco>(Endereco.class);
					listaEndereco.add(endereco);
					cliente.setListaEndereco(listaEndereco);	
					
					cliente.setEmail(lead.getEmail());
				}
			}
			
			//Unindo os contatos do Lead e ContaCrm
			List<Contacrmcontato> listaContato = contacrm.getListcontacrmcontato();	
			if(listaContato != null){
				PessoaContato pessoaContato = null;
				List<PessoaContato> listaPessoaContatoNova = new ArrayList<PessoaContato>();
				List<Contacrmcontatoemail> listcontacrmcontatoemail;
				for (Contacrmcontato cc : listaContato) {
					cc.setListcontacrmcontatofone(SinedUtil.listToSet(contacrmcontatofoneService.findByContacrmcontato(cc), Contacrmcontatofone.class));
					listcontacrmcontatoemail = contacrmcontatoemailService.findListEmailFromContacrmcontato(cc);
					if(listcontacrmcontatoemail != null){
						for (Contacrmcontatoemail cce : listcontacrmcontatoemail) {
							Contato contato = createContato(cc, cce.getEmail());
							
							pessoaContato = new PessoaContato();
							pessoaContato.setPessoa(cliente);
							pessoaContato.setContato(contato);
							
							listaPessoaContatoNova.add(pessoaContato);
						}
					} else {
						Contato contato = createContato(cc, "");
						
						pessoaContato = new PessoaContato();
						pessoaContato.setPessoa(cliente);
						pessoaContato.setContato(contato);
						
						listaPessoaContatoNova.add(pessoaContato);
					}
				}
				cliente.setListaContato(listaPessoaContatoNova);
			}		
			cliente.setNome(contacrm.getNome());
			cliente.setTipopessoa(contacrm.getTipo());
			cliente.setCnpj(contacrm.getCnpj());
			cliente.setCpf(contacrm.getCpf());

			for(Contacrmsegmento ccs : contacrm.getListcontacrmsegmento()){
				if(cliente.getListaClienteSegmento()==null){
					cliente.setListaClienteSegmento(new ListSet<ClienteSegmento>(ClienteSegmento.class));
				}
				cliente.getListaClienteSegmento().add( new ClienteSegmento(ccs) );
			}
		}
	
		if (oportunidade != null && oportunidade.getTiporesponsavel() != null && oportunidade.getResponsavel() != null) {
			ListSet<Clientevendedor> listaClientevendedor = new ListSet<Clientevendedor>(Clientevendedor.class);
			if (oportunidade.getTiporesponsavel().equals(Tiporesponsavel.COLABORADOR)) {
				Colaborador colaborador = new Colaborador();
				colaborador.setCdpessoa(oportunidade.getResponsavel().getCdpessoa());
				colaborador.setNome(StringUtils.isNotBlank(oportunidade.getResponsavel().getNome()) ? oportunidade.getResponsavel().getNome() : "");
				colaborador.setCpf(oportunidade.getResponsavel().getCpf() != null ? oportunidade.getResponsavel().getCpf() : null);
				colaborador.setCnpj(oportunidade.getResponsavel().getCnpj() != null ? oportunidade.getResponsavel().getCnpj() : null);
				if (colaborador != null) {
					Clientevendedor clientevendedor = new Clientevendedor();
					clientevendedor.setColaborador(colaborador);
					clientevendedor.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
					listaClientevendedor.add(clientevendedor);
				}
				
			} else if (oportunidade.getTiporesponsavel().equals(Tiporesponsavel.AGENCIA)) {
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setCdpessoa(oportunidade.getResponsavel().getCdpessoa());
				fornecedor.setNome(StringUtils.isNotBlank(oportunidade.getResponsavel().getNome()) ? oportunidade.getResponsavel().getNome() : "");
				fornecedor.setCpf(oportunidade.getResponsavel().getCpf() != null ? oportunidade.getResponsavel().getCpf() : null);
				fornecedor.setCnpj(oportunidade.getResponsavel().getCnpj() != null ? oportunidade.getResponsavel().getCnpj() : null);
				if (fornecedor != null) {
					Clientevendedor clientevendedor = new Clientevendedor();
					clientevendedor.setAgencia(fornecedor);
					clientevendedor.setTiporesponsavel(Tiporesponsavel.AGENCIA);
					listaClientevendedor.add(clientevendedor);
				}
			}
			cliente.setListaClientevendedor(listaClientevendedor);
		}
		
		cliente.setOportunidade(oportunidade);
		
		return getCriarModelAndView(request, cliente);	
	}
	 
	private Contato createContato(Contacrmcontato cc, String email) {
		Contato contato = new Contato();
		
		contato.setNome(cc.getNome());
		contato.setEmailcontato(email);
		
		Set<Endereco> listaEnd = new ListSet<Endereco>(Endereco.class);
		Endereco end = new Endereco();
		end.setLogradouro(cc.getLogradouro());
		end.setNumero(cc.getNumero());
		end.setComplemento(cc.getComplemento());
		end.setBairro(cc.getBairro());
		end.setCep(cc.getCep());
		end.setMunicipio(cc.getMunicipio());
		end.setEnderecotipo(Enderecotipo.UNICO);
		listaEnd.add(end);
		contato.setListaEndereco(listaEnd);
		
		Set<Contacrmcontatofone> list = cc.getListcontacrmcontatofone();
		if(list != null){
			Set<Telefone> listaTelefone = new ListSet<Telefone>(Telefone.class);
			Telefone tel;
			for (Contacrmcontatofone fone : list) {
				tel = new Telefone();
				tel.setTelefone(fone.getTelefone());
				tel.setTelefonetipo(fone.getTelefonetipo());
				listaTelefone.add(tel);
			}
			contato.setListaTelefone(listaTelefone);
		}
		
		return contato;
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Cliente bean) {
		if(bean.getOportunidade() != null){
			request.clearMessages();
			request.addMessage("Cliente gerado com sucesso.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Cliente form) throws CrudException {

		if(form.getIsClienteVendaEdicao() != null && form.getIsClienteVendaEdicao()){
			super.doSalvar(request, form);
			request.clearMessages();			
			View.getCurrent().println("<script>window.opener.atualizaComboEndereco();window.close();</script>");
			return null;
		}
		return super.doSalvar(request, form);
	}
	
	/**
	 * M�todo para incluir o cdpessoa na tabela empresa
	 *
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#incluirPessoaComoEmpresa(Pessoa pessoa)
	 *
	 * @param request
	 * @param pessoa
	 * @return
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	public ModelAndView incluirClienteComoEmpresa(WebRequestContext request, Pessoa pessoa){
		return empresaService.incluirPessoaComoEmpresa(pessoa);
	}
	
	/**
	 * M�todo que em ajax faz a exclus�o do hist�rico
	 *
	 * @param request
	 * @param clientehistorico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/12/2013
	 */
	public ModelAndView ajaxExclusaoClienteHistorico(WebRequestContext request, Clientehistorico clientehistorico){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		try{
			if(clientehistorico != null && clientehistorico.getCdclientehistorico() != null){
				clientehistoricoService.delete(clientehistorico);
				jsonModelAndView.addObject("sucesso", Boolean.TRUE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonModelAndView.addObject("error", e.getMessage());
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * M�todo para abrir a view de edi��o de hist�rico
	 *
	 * @param request
	 * @param clientehistorico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/12/2013
	 */
	public ModelAndView editarHistorico(WebRequestContext request, Clientehistorico clientehistorico){
		clientehistorico = clientehistoricoService.loadForEntrada(clientehistorico);
		return new ModelAndView("direct:/crud/popup/editarHistoricoCliente", "bean", clientehistorico);
	}
	
	/**
	 * M�todo que salva o hist�rico editado e fecha o popup
	 *
	 * @param request
	 * @param clientehistorico
	 * @author Rodrigo Freitas
	 * @since 19/12/2013
	 */
	public void saveHistorico(WebRequestContext request, Clientehistorico clientehistorico){
		clientehistoricoService.saveOrUpdateWithoutLog(clientehistorico);
		request.addMessage("Hist�rico editado com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * Met�do que carrega a popup com o mapa do endere�o do cliente
	 * @param request
	 * @param endereco
	 */
	public ModelAndView abrirMapaEndereco(WebRequestContext request, Endereco endereco){
		
		endereco = enderecoService.loadEndereco(endereco);
		
		request.setAttribute("endereco", br.com.linkcom.utils.StringUtils.tiraAcento(endereco.getLogradouroCompletoComBairro()));
		return new ModelAndView("direct:crud/popup/popUpmapaEndereco");
	}
	
	/**
	* M�todo que abre uma popup para o usu�rio definir qual etiqueta imprimir
	*
	* @param request
	* @return
	* @since 25/11/2014
	* @author Jo�o Vitor ETIQUETA_9X115
	*/
	public ModelAndView abrirEmitirEtiquetaIdentificacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request,"/crm/crud/Cliente");
			return null;
		}
		List<Tipoetiqueta> listaTipoEtiqueta = new ArrayList<Tipoetiqueta>();
		listaTipoEtiqueta.add(Tipoetiqueta.ETIQUETA_9X115);
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("listaTipoEtiqueta", listaTipoEtiqueta);
		request.setAttribute("isSelectedItensCdCliente", "TRUE");
		return new ModelAndView("direct:/crud/popup/emitirEtiquetaIdentificacao");
	}
	
	private void criarContaContabil(Cliente bean){
		String nome;
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.GERAR_CONTACONTABIL_RAZAOSOCIAL_CLIENTE))){
			if (Tipopessoa.PESSOA_FISICA.equals(bean.getTipopessoa())){
				if (bean.getRazaosocial()!=null && !bean.getRazaosocial().trim().equals("")){
					nome = bean.getRazaosocial();
				}else {
					nome = bean.getNome();
				}
			}else {
				nome = bean.getRazaosocial();
			}
		}else {
			nome = bean.getNome();
		}
				
		ContaContabil contacontabil = contacontabilService.criarContaContabil(bean.getCdpessoa(), nome, Parametrogeral.CONTA_CONTABIL_CLIENTE);
		if (contacontabil!=null){
			clienteService.updateContaContabil(bean, contacontabil);
		}
	}
	
	public ModelAndView ajaxTemplatesValeCompraDisponiveis(WebRequestContext request){
		List<ReportTemplateBean> templates = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COMPROVANTE_VALE_COMPRA);;
		ModelAndView retorno = new JsonModelAndView().addObject("qtdeTemplates", templates.size());
		if(templates.size()==1){
			retorno.addObject("cdtemplate", templates.get(0).getCdreporttemplate());
		}
		return retorno;
	}
	
	public ModelAndView abrirSelecaoTemplateComprovanteValeCompra(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request,"/crm/crud/Cliente");
			return null;
		}
		
		List<ReportTemplateBean> templates = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_COMPROVANTE_VALE_COMPRA);;
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("listaTemplatesValeCompra", templates);
		return new ModelAndView("direct:/crud/popup/emitirComprovanteValeCompra");
	}
	
	@Override
	protected List<ValidacaoCampoObrigatorio> getCamposObrigatoriosAdicionais() {
		List<ValidacaoCampoObrigatorio> lista = new ArrayList<ValidacaoCampoObrigatorio>();
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGARCPFCLIENTE))){
			ValidacaoCampoObrigatorio bean = new ValidacaoCampoObrigatorio() {
				@Override
				public String getPrefixo() {
					return "";
				}
				
				@Override
				public Boolean getPelomenosumregistro() {
					return false;
				}
				
				@Override
				public String getCondicao() {
					return "tipopessoa == 'PESSOA_FISICA' && !exterior";
				}
				
				@Override
				public String getCampo() {
					return "cpf";
				}
				
				@Override
				public String getDisplayNameCampo() {
					return "cpf";
				}
				
				@Override
				public String getDisplayNameLista() {
					return null;
				}

				@Override
				public void setDisplayNameCampo(String string) {
				}

				@Override
				public void setDisplayNameLista(String string) {
				}
			};
			
			lista.add(bean);
		}
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CNPJ_CLIENTE))){
			ValidacaoCampoObrigatorio bean = new ValidacaoCampoObrigatorio() {
				@Override
				public String getPrefixo() {
					return "";
				}
				
				@Override
				public Boolean getPelomenosumregistro() {
					return false;
				}
				
				@Override
				public String getCondicao() {
					return "tipopessoa == 'PESSOA_JURIDICA' && !exterior";
				}
				
				@Override
				public String getCampo() {
					return "cnpj";
				}
				
				@Override
				public String getDisplayNameCampo() {
					return "CNPJ";
				}
				
				@Override
				public String getDisplayNameLista() {
					return null;
				}

				@Override
				public void setDisplayNameCampo(String string) {
				}

				@Override
				public void setDisplayNameLista(String string) {
				}
			};
			
			lista.add(bean);
		}
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_RAZAOSOCIAL_CLIENTE))){
			ValidacaoCampoObrigatorio bean = new ValidacaoCampoObrigatorio() {
				@Override
				public String getPrefixo() {
					return "";
				}
				
				@Override
				public Boolean getPelomenosumregistro() {
					return false;
				}
				
				@Override
				public String getCondicao() {
					return "tipopessoa == 'PESSOA_JURIDICA' && !exterior";
				}
				
				@Override
				public String getCampo() {
					return "razaosocial";
				}
				
				@Override
				public String getDisplayNameCampo() {
					return "Raz�o Social";
				}
				
				@Override
				public String getDisplayNameLista() {
					return null;
				}

				@Override
				public void setDisplayNameCampo(String string) {
				}

				@Override
				public void setDisplayNameLista(String string) {
				}
			};
			
			lista.add(bean);
		}
		
		return lista;
	}
	
	public ModelAndView ajaxBuscarConselhoclasseprofissional(WebRequestContext request, Clienteespecialidade clienteespecialidade){
		Especialidade especialidade = especialidadeService.loadForEntrada(clienteespecialidade.getEspecialidade());
		return new JsonModelAndView().addObject("conselhoclasseprofissionalStr", especialidade.getConselhoclasseprofissional()!=null ? especialidade.getConselhoclasseprofissional().getDescricao() : "");
	}
}