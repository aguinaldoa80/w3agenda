package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.view.Vwemailcontatocliente;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaillingService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/crm/crud/Mailling",
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = {"cliente", "contatonome", "emailcontatocliente"})
public class MaillingCrud extends CrudControllerSined<MaillingFiltro, Vwemailcontatocliente, Vwemailcontatocliente>{

	private MaillingService maillingService;
	
	public void setMaillingService(MaillingService maillingService) {
		this.maillingService = maillingService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Vwemailcontatocliente form) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Vwemailcontatocliente bean) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Vwemailcontatocliente bean) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, MaillingFiltro filtro) {
		if(filtro.getEmail() == null || filtro.getEmail().equals("")){
			filtro.setEmail(EmpresaService.getInstance().loadPrincipal().getEmail());
		}
		return new ModelAndView("crud/maillingListagem");
	}
	
	@Action("enviarMailling")
	public ModelAndView enviarMailling(WebRequestContext request, MaillingFiltro filtro){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn != null && !"".equals(whereIn)){
			whereIn = SinedUtil.montaWhereInString(whereIn);
			
			List<Vwemailcontatocliente> listaClientesContatos = maillingService.findClientesContatosByWhereIn(whereIn);
			maillingService.enviaEmail(listaClientesContatos, filtro, request);
		
			//salvar hist�rico do cliente
			maillingService.salvarHistoricosCliente(listaClientesContatos, request.getParameter("assunto"));
		}
	
		return continueOnAction("listagem", filtro);
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
}
