package br.com.linkcom.sined.modulo.crm.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Tipoidentidade;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.TipoidentidadeFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean		  
@Controller(path="/crm/crud/Tipoidentidade", authorizationModule=CrudAuthorizationModule.class)
public class TipoidentidadeCrud extends CrudControllerSined<TipoidentidadeFiltro, Tipoidentidade, Tipoidentidade> {

	
	
}
