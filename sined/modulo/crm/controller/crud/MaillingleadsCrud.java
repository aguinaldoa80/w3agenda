package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leademail;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.service.CampanhaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.EnvioemailitemService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.MaillingleadsService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.SegmentoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingleadsFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/crm/crud/MaillingLeads",
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = {"nomeComEmpresa", "listaEmails"})
public class MaillingleadsCrud extends CrudControllerSined<MaillingleadsFiltro, Lead, Lead>{

	private LeadService leadService;
	private SegmentoService segmentoService;
	private LeadhistoricoService leadhistoricoService;
	private CampanhaService campanhaService;
	private EnvioemailService envioemailService;
	private EnvioemailitemService envioemailitemService;
	private MaillingleadsService maillingleadsService;
	
	public void setMaillingleadsService(
			MaillingleadsService maillingleadsService) {
		this.maillingleadsService = maillingleadsService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setSegmentoService(SegmentoService segmentoService) {
		this.segmentoService = segmentoService;
	}
	public void setLeadhistoricoService(LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setCampanhaService(CampanhaService campanhaService) {
		this.campanhaService = campanhaService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setEnvioemailitemService(EnvioemailitemService envioemailitemService) {
		this.envioemailitemService = envioemailitemService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Lead form) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Lead bean) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Lead bean) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, MaillingleadsFiltro filtro) {
		if(filtro.getEmail() == null || filtro.getEmail().equals("")){
			filtro.setEmail(EmpresaService.getInstance().loadPrincipal().getEmail());
		}
		return new ModelAndView("crud/maillingleadsListagem");
	}
	
	@Override
	protected ListagemResult<Lead> getLista(WebRequestContext request, MaillingleadsFiltro filtro) {
		
		ListagemResult<Lead> listresult = super.getLista(request, filtro); 
		List<Lead> list = listresult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdlead", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(maillingleadsService.findListagem(whereIn, filtro.getOrderBy(), filtro.isAsc()));
			
			for(Lead lead : list){
				if(lead.getListleademail()!= null){
					lead.setListaEmails(CollectionsUtil.listAndConcatenate(lead.getListleademail(), "email", "<br>"));
					if(StringUtils.isNotEmpty(lead.getEmail())){
						if(StringUtils.isNotEmpty(lead.getListaEmails())){
							lead.setListaEmails(lead.getEmail() + "<br>" + lead.getListaEmails());
						}else {
							lead.setListaEmails(lead.getEmail());
						}
					}
				}
			}
		}
		
		return listresult;
	}
	
	@Override
	protected void listagem(WebRequestContext request, MaillingleadsFiltro filtro) throws Exception {
		
		List<Segmento> listaSegmento = segmentoService.findForCombo();
		request.setAttribute("listaSegmento", listaSegmento);
		
		super.listagem(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, MaillingleadsFiltro filtro) throws CrudException {
		
		String whereInCampanha = request.getParameter("selectedItens");
		
		if(whereInCampanha != null && !whereInCampanha.equals("")){
			filtro.setNotFirstTime(true);
			Campanha campanha = campanhaService.carregaCampanha(whereInCampanha);
			filtro.setCampanha(campanha);
			request.setAttribute("selectedItens", whereInCampanha);
			
		}
		
		return super.doListagem(request, filtro);
	}
	
	@Action("enviarMailling")
	public ModelAndView enviarMailling(WebRequestContext request, MaillingleadsFiltro filtro){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Lead> listaLead = leadService.findLead(whereIn);
		List<Leadhistorico> listleadhistorico = new ArrayList<Leadhistorico>();
		
		boolean enviaMsg = false;
		
		Envioemail envioemail = envioemailService.registrarEnvio(filtro.getEmail(), filtro.getAssunto(), filtro.getMensagem(), Envioemailtipo.MAILING_LEAD);
		
		for (Lead lead : listaLead) {
			boolean enviaMsgLead = false;
			
			if(lead.getEmail() != null && !lead.getEmail().trim().equals("")){
				enviaMsgLead = true;
				try {
					enviaMsg = this.enviaEmailLead(filtro, envioemail, lead, lead.getEmail());
					listleadhistorico.add(this.addHistoricoEnvioEmail(filtro, lead));
				} catch (Exception e) {
					e.printStackTrace();
					request.addError("N�o foi poss�vel enviar o mailling para " + lead.getEmail());
				}
			}
			
			if(lead.getListleademail() != null && lead.getListleademail().size() > 0){
				enviaMsgLead = true;
				for(Leademail leademail : lead.getListleademail()){
					try {
						enviaMsg = this.enviaEmailLead(filtro, envioemail, lead, leademail.getEmail());
						listleadhistorico.add(this.addHistoricoEnvioEmail(filtro, lead));
					} catch (Exception e) {
						e.printStackTrace();
						request.addError("N�o foi poss�vel enviar o mailling para " + leademail.getEmail());
					}
				}
			}
			
			if(!enviaMsgLead){
				request.addError("N�o foram enviados o mailling para o contato " + lead.getNome() + " pois n�o possui e-mail cadastrado.");
			}
		}
		
		//enviar c�pia do email para o usu�rio logado atualmente
		if (filtro.getEnviaCopia() != null && filtro.getEnviaCopia()){
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
											.setFrom(filtro.getEmail())
											.setTo(SinedUtil.getUsuarioLogado().getEmail())
											.setSubject(filtro.getAssunto());
				
				//anexar arquivo
				if(filtro.getAnexo() != null && filtro.getAnexo().getContent() != null && filtro.getAnexo().getName() != null && filtro.getAnexo().getContenttype() != null ){
					try {
						email.attachFileUsingByteArray(filtro.getAnexo().getContent(), filtro.getAnexo().getName(), filtro.getAnexo().getContenttype(), "");
					} catch (Exception e) {
						throw new SinedException("Erro ao anexar arquivo para "+SinedUtil.getUsuarioLogado().getEmail());
					}							
				}
				
				email
					.addHtmlText(filtro.getMensagem())
					.sendMessage();
			} catch (Exception e) {
				e.printStackTrace();
				throw new SinedException("N�o foi poss�vel enviar para voc� a c�pia do mailling ");
			}
		}
		
		if(enviaMsg){
			request.addMessage("Mailling enviado com sucesso!");
		}
		
		if(listleadhistorico != null){
			for (Leadhistorico leadhistorico : listleadhistorico) {
				leadhistoricoService.saveOrUpdate(leadhistorico);
			}
		}
		
		return continueOnAction("listagem", filtro);
	}
	
	private Leadhistorico addHistoricoEnvioEmail(MaillingleadsFiltro filtro, Lead lead) {
		Leadhistorico leadhistorico = new Leadhistorico();
		
		leadhistorico.setLead(lead);
		leadhistorico.setObservacao("Envio de e-mail " + filtro.getAssunto());
		leadhistorico.setDtaltera(new Timestamp (Calendar.getInstance().getTimeInMillis()));
		leadhistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		
		return leadhistorico;
	}
	
	private boolean enviaEmailLead(MaillingleadsFiltro filtro, Envioemail envioemail, Lead lead, String email) throws Exception {
		EmailManager emailManager = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
											.setFrom(filtro.getEmail())
											.setTo(email)
											.setSubject(filtro.getAssunto());
		
		//anexar arquivo
		if(filtro.getAnexo() != null && filtro.getAnexo().getContent() != null && filtro.getAnexo().getName() != null && filtro.getAnexo().getContenttype() != null ){
			try {
				emailManager.attachFileUsingByteArray(filtro.getAnexo().getContent(), filtro.getAnexo().getName(), filtro.getAnexo().getContenttype(), "");
			} catch (Exception e) {
				throw new SinedException("Erro ao anexar arquivo para " + email);
			}							
		}
		
		Envioemailitem envioemailitem = new Envioemailitem();
		envioemailitem.setEnvioemail(envioemail);
		envioemailitem.setEmail(email);
		envioemailitem.setNomecontato(lead.getNome());
		envioemailitem.setPessoa(lead.getResponsavel());
		envioemailitemService.saveOrUpdate(envioemailitem);
		emailManager.setEmailItemId(envioemailitem.getCdenvioemailitem());
		
		String msg = filtro.getMensagem().replace("&lt; NOMECONTATO &gt;", lead.getNome()) + EmailUtil.getHtmlConfirmacaoEmail(envioemail, email);
		emailManager.addHtmlText(msg).sendMessage();
		
		return true;
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
}
