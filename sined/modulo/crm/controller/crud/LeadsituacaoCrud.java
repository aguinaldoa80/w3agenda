	package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.LeadsituacaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Leadsituacao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class LeadsituacaoCrud extends CrudControllerSined<LeadsituacaoFiltro, Leadsituacao, Leadsituacao>{
	
	@Override
	protected void salvar(WebRequestContext request, Leadsituacao bean) throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_leadsituacao_nome")) 
				throw new SinedException("Segmento j� cadastrado no sistema.");	
		}
	}
	
}
