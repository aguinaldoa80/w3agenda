package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendainteracaohistorico;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.bean.Requisicaoprioridade;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaorelacionado;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoagendainteracao;
import br.com.linkcom.sined.geral.service.AgendainteracaoService;
import br.com.linkcom.sined.geral.service.AgendainteracaohistoricoService;
import br.com.linkcom.sined.geral.service.AnimalService;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.PessoaquestionarioService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.RequisicaohistoricoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.AgendainteracaoFiltro;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.PropostaFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/crm/crud/Agendainteracao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "")
public class AgendainteracaoCrud extends CrudControllerSined<AgendainteracaoFiltro, Agendainteracao, Agendainteracao>{

	private ContratoService contratoService;
	private ColaboradorService colaboradorService;
	private AgendainteracaoService agendainteracaoService;
	private RequisicaoService requisicaoService;
	private RequisicaohistoricoService requisicaohistoricoService;
	private ReportTemplateService reportTemplateService;
	private AnimalService animalService;
	private AgendainteracaohistoricoService agendainteracaohistoricoService;
	private AtividadetipoService atividadetipoService;
	private ClienteService clienteService;
	private EmpresaService empresaService;
	private ContacrmService contacrmService;
	private PessoaquestionarioService pessoaquestionarioService;
	
	public void setAgendainteracaohistoricoService(AgendainteracaohistoricoService agendainteracaohistoricoService) {this.agendainteracaohistoricoService = agendainteracaohistoricoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setAgendainteracaoService(AgendainteracaoService agendainteracaoService) {this.agendainteracaoService = agendainteracaoService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {	this.requisicaoService = requisicaoService;}
	public void setRequisicaohistoricoService(RequisicaohistoricoService requisicaohistoricoService) {this.requisicaohistoricoService = requisicaohistoricoService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setAnimalService(AnimalService animalService) { this.animalService = animalService; }
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {this.atividadetipoService = atividadetipoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setContacrmService(ContacrmService contacrmService) {this.contacrmService = contacrmService;}
	public void setPessoaquestionarioService(PessoaquestionarioService pessoaquestionarioService) {this.pessoaquestionarioService = pessoaquestionarioService;}
	
	@Override
	protected void listagem(WebRequestContext request, AgendainteracaoFiltro filtro) throws Exception {
		List<Agendainteracaosituacao> listaSituacao = new ArrayList<Agendainteracaosituacao>();
		for (Agendainteracaosituacao situacao : Agendainteracaosituacao.values()) {
			listaSituacao.add(situacao);
		}
		request.setAttribute("listaSituacao", listaSituacao);
		
		super.listagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<Agendainteracao> getLista(WebRequestContext request, AgendainteracaoFiltro filtro) {
		if (!filtro.isNotFirstTime()) {
			Colaborador colaborador = new Colaborador(((Usuario)request.getUser()).getCdpessoa());
			colaborador = colaboradorService.carregaColaboradorresponsavel(colaborador.getCdpessoa());
			if(colaborador != null && colaborador.getCdpessoa() != null)
				filtro.setResponsavel(colaborador);
		}
		
		ListagemResult<Agendainteracao> listagemResult = super.getLista(request, filtro);
		List<Agendainteracao> listaAgendainteracao = listagemResult.list();
		
		if(SinedUtil.isListNotEmpty(listaAgendainteracao)){
			String whereIn = CollectionsUtil.listAndConcatenate(listaAgendainteracao, "cdagendainteracao", ",");
			listaAgendainteracao.removeAll(listaAgendainteracao);
			listaAgendainteracao.addAll(agendainteracaoService.findListagem(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}
		
		return listagemResult;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Agendainteracao form)	throws Exception {
		if(form.getCdagendainteracao() != null){
			if(form.getCliente() != null){
				form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome"));
			}
		}
		
		request.setAttribute("listaEmpresasUsuario", empresaService.findByUsuario());
		
		Boolean abrirTelaCriacaoAgendaInteracao = request.getAttribute("abrirTelaCriacaoAgendaInteracao") != null ? (Boolean) request.getAttribute("abrirTelaCriacaoAgendaInteracao") : Boolean.FALSE; 
		if(form.getAgendainteracaorelacionado() == null || abrirTelaCriacaoAgendaInteracao){
			form.setEmpresahistorico(empresaService.loadPrincipal());
			form.setResponsavel(SinedUtil.getUsuarioComoColaborador());
			form.setDtproximainteracao(new java.sql.Date(System.currentTimeMillis()));
		}
		if(form.getCliente() != null){
			request.setAttribute("listAnimais", animalService.findExcetoObitoByCliente(form.getCliente()));
		}
		
		if(form.getCdagendainteracao() != null){
			form.setListaAgendainteracaohistorico(agendainteracaohistoricoService.findByAgendainteracao(form));
			form.setListaQuestionarioServicos(SinedUtil.listToSet(pessoaquestionarioService.carregarPessoaquestionarioByAgendaInteracao(form), Pessoaquestionario.class));
		}
		
		request.setAttribute("possuiPermissaoAlteraData", SinedUtil.isUserHasAction("ALTERARDATAAGENDAINTERACAO"));
		request.setAttribute("listaAtividadetipo", atividadetipoService.findForCrm());
		
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(Agendainteracao bean, BindException errors) {
		if(bean.getCliente() != null && bean.getResponsavel() != null && bean.getCliente().getCdpessoa() != null && 
				bean.getResponsavel().getCdpessoa() != null && bean.getCliente().getCdpessoa().equals(bean.getResponsavel().getCdpessoa())){
			errors.reject("001", "O cliente e o respons�vel n�o podem ter o mesmo id");
		}
		super.validateBean(bean, errors);
	}
	
	/**
	 * Carrega a combo contrato com os contratos do cliente selecionado.
	 * @author Taidson Santos
	 * @since 24/09/2010
	 */
	public void ajaxComboContrato(WebRequestContext request, Agendainteracao agendainteracao){
		
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		
		if(agendainteracao.getCliente() != null){
			listaContrato = contratoService.findByContrato(agendainteracao.getCliente());			
		}
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaContrato, "listaContrato", ""));
	}
	
	/**
	 * Carrega popup para conclus�o de Agendas de intera��o.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/07/2015
	 */
	public ModelAndView concluir(WebRequestContext request){
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		if(itensSelecionados == null || itensSelecionados.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		request.setAttribute("selectedItens", itensSelecionados);
		
		request.setAttribute("titulo", "CONCLUIR AGENDA DE INTERA��O");
		request.setAttribute("acaoSave", "saveConcluirAgenda");
		
		return new ModelAndView("direct:/crud/popup/cancelaAgendainteracao").addObject("clientehistorico", new Clientehistorico());
	}
	
	/**
	 * Salva os dados de conclus�o no hist�rico de cliente e a data atual em agenda de intera��o.
	 *
	 * @param request
	 * @param clientehistorico
	 * @author Rodrigo Freitas
	 * @since 02/07/2015
	 */
	public void saveConcluirAgenda(WebRequestContext request, Clientehistorico clientehistorico){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}
    	
    	String observacao = "Conclu�do.";
    	if(clientehistorico.getObservacao() != null && !clientehistorico.getObservacao().trim().equals("")){
    		observacao += " Motivo: " + clientehistorico.getObservacao();
    	}
    	
    	String[] ids = itensSelecionados.split(",");
    	for (int i = 0; i < ids.length; i++) {
    		Agendainteracaohistorico agendainteracaohistorico = new Agendainteracaohistorico();
    		agendainteracaohistorico.setAgendainteracao(new Agendainteracao(Integer.parseInt(ids[i])));
			agendainteracaohistorico.setObservacao(observacao);
			agendainteracaohistoricoService.saveOrUpdate(agendainteracaohistorico);
			
			List<Requisicao> listarequisicao = requisicaoService.findForAgendaInteracaoHistorico(new Agendainteracao(Integer.parseInt(ids[i])));
			for (Requisicao requisicao : listarequisicao) {
				requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, "(Agenda conclu�da)" + (clientehistorico.getObservacao() != null ? "Motivo : " + clientehistorico.getObservacao() : ""), requisicao));		
			}
		}
    	
    	agendainteracaoService.updateDataConclusao(itensSelecionados);

    	request.clearMessages();
    	request.addMessage("Agenda(s) conclu�da(s) com sucesso.");
    	SinedUtil.fechaPopUp(request);
    }
	
	/**
	 * Carrega popup para cancelamento de Agendas de intera��o.
	 * @param request
	 * @return
	 * @author Taidson
	 * @since 28/09/2010
	 */
	public ModelAndView cancelar(WebRequestContext request){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}
		request.setAttribute("selectedItens", itensSelecionados);
		
		request.setAttribute("titulo", "CANCELAR AGENDA DE INTERA��O");
		request.setAttribute("acaoSave", "saveCancelarAgenda");
    	
    	return new ModelAndView("direct:/crud/popup/cancelaAgendainteracao").addObject("clientehistorico", new Clientehistorico());
    }
	
	/**
	 * Salva os dados de cancelamento no hist�rico de cliente e a data atual em agenda de intera��o.
	 * @param request
	 * @param clientehistorico
	 * @author Taidson
	 * @since 28/09/2010
	 */
	public void saveCancelarAgenda(WebRequestContext request, Clientehistorico clientehistorico){
    	String itensSelecionados = SinedUtil.getItensSelecionados(request);
    	if(itensSelecionados == null || itensSelecionados.equals("")){
    		throw new SinedException("Nenhum item selecionado.");
    	}
    	
    	String observacao = "Cancelado.";
    	if(clientehistorico.getObservacao() != null && !clientehistorico.getObservacao().trim().equals("")){
    		observacao += " Motivo: " + clientehistorico.getObservacao();
    	}
    	
    	String[] ids = itensSelecionados.split(",");
    	for (int i = 0; i < ids.length; i++) {
    		Agendainteracaohistorico agendainteracaohistorico = new Agendainteracaohistorico();
    		agendainteracaohistorico.setAgendainteracao(new Agendainteracao(Integer.parseInt(ids[i])));
			agendainteracaohistorico.setObservacao(observacao);
			agendainteracaohistoricoService.saveOrUpdate(agendainteracaohistorico);
    		
    		Agendainteracao agendainteracao = new Agendainteracao(Integer.parseInt(ids[i]));
    		agendainteracao = agendainteracaoService.loadForEntrada(agendainteracao);
    		
    		agendainteracaoService.registrarHistoricoClienteConta(agendainteracao, clientehistorico.getObservacao());
        	
			List<Requisicao> listarequisicao = requisicaoService.findForAgendaInteracaoHistorico(agendainteracao);
			for (Requisicao requisicao : listarequisicao) {
				requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, "(Agenda Cancelada) " + (clientehistorico.getObservacao() != null ? clientehistorico.getObservacao() : ""), requisicao));		
			}
		}
    	
    	agendainteracaoService.updateDataCancelamento(itensSelecionados);

    	request.clearMessages();
    	request.addMessage("Agenda(s) cancelada(s) com sucesso.");
    	SinedUtil.fechaPopUp(request);
    }
    
    
	/**
	 * Carrega popup para lan�ar hist�rico de Agendas de intera��o.
	 * @param request
	 * @return
	 * @author Taidson
	 * @since 28/09/2010
	 */
	public ModelAndView lancarHistorico(WebRequestContext request){
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		if(itensSelecionados == null || itensSelecionados.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		request.setAttribute("selectedItens", itensSelecionados);
		
		request.setAttribute("titulo", "LAN�AR HIST�RICO");
		request.setAttribute("acaoSave", "saveLancarHistorico");
		
		return new ModelAndView("direct:/crud/popup/historicoAgendainteracao").addObject("clientehistorico", new Clientehistorico());
	}
	
	/**
	 * Salva os dados no hist�rico de cliente ou conta.
	 * @param request
	 * @param clientehistorico
	 * @author Taidson
	 * @since 28/09/2010
	 */
	public void saveLancarHistorico(WebRequestContext request, Clientehistorico clientehistorico){
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		if(itensSelecionados == null || itensSelecionados.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		String[] ids = itensSelecionados.split(",");
		for (int i = 0; i < ids.length; i++) {
			Agendainteracao agendainteracao = new Agendainteracao(Integer.parseInt(ids[i]));
			agendainteracao = agendainteracaoService.loadForEntrada(agendainteracao);
			
			agendainteracaoService.registrarHistoricoClienteConta(agendainteracao, "(Intera��o com origem na agenda de intera��o) " + (StringUtils.isNotBlank(clientehistorico.getObservacao()) ? clientehistorico.getObservacao() : ""));
			
			//salva os dados no hist�rico de Agendainteracao
			Agendainteracaohistorico agendainteracaohistorico = new Agendainteracaohistorico();
			agendainteracaohistorico.setAgendainteracao(agendainteracao);
			agendainteracaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			agendainteracaohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
			agendainteracaohistorico.setObservacao(clientehistorico.getObservacao());
			
			agendainteracaohistoricoService.saveOrUpdate(agendainteracaohistorico);
			
			if(clientehistorico.getAgendarProxInteracao()){
				Date dtproximainteracao = SinedDateUtils.incrementDateFrequencia(
						new java.sql.Date(System.currentTimeMillis()), 
						agendainteracao.getPeriodicidade(),(agendainteracao.getQtdefrequencia() != null && agendainteracao.getQtdefrequencia() > 0 ? agendainteracao.getQtdefrequencia() : 1));
				agendainteracaoService.updateDataProxInteracao(agendainteracao.getCdagendainteracao(), dtproximainteracao);
			}
			
			List<Requisicao> listarequisicao = requisicaoService.findForAgendaInteracaoHistorico(agendainteracao);
			for (Requisicao requisicao : listarequisicao) {
				String observacaoHistorico ="(Hist�rico Agenda) " + clientehistorico.getObservacao() ;
				requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, observacaoHistorico, requisicao));		
			}
		}
		
		request.clearMessages();
		request.addMessage("Hist�rico(s) lan�ado(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * Carrega popup para gerar nova OS.
	 * @param request
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView gerarOS(WebRequestContext request){
		String id = SinedUtil.getItensSelecionados(request);
		if(id == null || id.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		request.setAttribute("selectedItens", id);
		request.setAttribute("acaoSave", "saveGerarOS");
		
		Agendainteracao agendainteracao = agendainteracaoService.loadForGerarOS(new Agendainteracao(Integer.parseInt(id)));
		Requisicao requisicao = new Requisicao();
		requisicao.setAgendainteracao(agendainteracao);
		
		requisicao.setCliente(agendainteracao.getCliente());
		requisicao.setColaboradorresponsavel(agendainteracao.getResponsavel());
		if(agendainteracao.getContrato() != null && agendainteracao.getContrato().getDescricao() != null)
			requisicao.setDescricao(agendainteracao.getContrato().getDescricao());
		else if(agendainteracao.getMaterial() != null && agendainteracao.getMaterial().getNome() != null)
			requisicao.setDescricao(agendainteracao.getMaterial().getNome());
		requisicao.setRequisicaoestado(new Requisicaoestado(Requisicaoestado.EMESPERA));
		requisicao.setRequisicaoprioridade(new Requisicaoprioridade(Requisicaoprioridade.MEDIA));
		
		return new ModelAndView("direct:/crud/popup/gerarOS").addObject("requisicao", requisicao);
	}
	
	public void saveGerarOS(WebRequestContext request, Requisicao requisicao){
		String id = SinedUtil.getItensSelecionados(request);
		if(id == null || id.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		requisicaoService.saveOrUpdate(requisicao);
		
		String observacaoHistorico = " ";
		if(requisicao.getAgendainteracao() != null){
			observacaoHistorico = "Cria��o a partir da Agenda Intera��o : <a href=\"javascript:visualizarAgendainteracao(" +
					requisicao.getAgendainteracao().getCdagendainteracao() + ");\">" + 
					requisicao.getAgendainteracao().getCdagendainteracao() + "</a>";
		}
		
		requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, observacaoHistorico, requisicao));
		
		Agendainteracaohistorico agendainteracaohistorico = new Agendainteracaohistorico();
		if(requisicao.getAgendainteracao() != null){
			agendainteracaohistorico.setAgendainteracao(requisicao.getAgendainteracao());
			agendainteracaohistorico.setObservacao("Cria��o da O.S. : <a href=\"javascript:visualizarOS(" +
					requisicao.getCdrequisicao() + ");\">" + 
					requisicao.getCdrequisicao() + "</a>");
		}
		agendainteracaohistoricoService.saveOrUpdate(agendainteracaohistorico);
		
		Agendainteracao agendainteracao = new Agendainteracao(Integer.parseInt(id));
		agendainteracao = agendainteracaoService.loadForEntrada(agendainteracao);
		Date dtproximainteracao = SinedDateUtils.incrementDateFrequencia(
				new java.sql.Date(System.currentTimeMillis()), 
				agendainteracao.getPeriodicidade(), (agendainteracao.getQtdefrequencia() != null && agendainteracao.getQtdefrequencia() > 0 ? agendainteracao.getQtdefrequencia() : 1));
		agendainteracaoService.updateDataProxInteracao(agendainteracao.getCdagendainteracao(), dtproximainteracao);

		request.clearMessages();
		request.addMessage("Hist�rico(s) lan�ado(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarModelos(WebRequestContext request) throws Exception {
		
		List<ReportTemplateBean> listaReportTemplateBean = reportTemplateService.loadTemplatesByCategoria(EnumCategoriaReportTemplate.AGENDA_INTERACAO);
		List<Integer> listaCdreporttemplate = new ArrayList<Integer>();
		
		for (ReportTemplateBean reportTemplateBean: listaReportTemplateBean){
			listaCdreporttemplate.add(reportTemplateBean.getCdreporttemplate());
		}
		
		return new JsonModelAndView().addObject("listaCdreporttemplate", listaCdreporttemplate);
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView escolherModelo(WebRequestContext request) throws Exception {
		
		String selectedItens = SinedUtil.getItensSelecionados(request);
		
		if (selectedItens==null || selectedItens.trim().equals("")){
			return null;
		}
		
		String cdsreporttemplate = request.getParameter("cdsreporttemplate");
		List<ReportTemplateBean> listaReportTemplateBean = new ArrayList<ReportTemplateBean>();
		
		for (String cdreporttemplate: cdsreporttemplate.split("\\,")){
			listaReportTemplateBean.add(reportTemplateService.load(new ReportTemplateBean(Integer.valueOf(cdreporttemplate))));
		}
		
		Collections.sort(listaReportTemplateBean, new Comparator<ReportTemplateBean>() {
			@Override
			public int compare(ReportTemplateBean o1, ReportTemplateBean o2) {
				return o1.getNome().compareToIgnoreCase(o2.getNome());
			}
		});
		
		request.setAttribute("titulo", "Escolher Modelo");
		request.setAttribute("listaReportTemplateBean", listaReportTemplateBean);
		request.setAttribute("selectedItens", selectedItens);
		request.setAttribute("apuracaoPeriodoInicio", Util.strings.emptyIfNull(request.getParameter("apuracaoPeriodoInicio")));
		request.setAttribute("apuracaoPeriodoFim", Util.strings.emptyIfNull(request.getParameter("apuracaoPeriodoFim")));
		
		return new ModelAndView("direct:/crud/popup/escolherModeloAgendaInteracao");
	}
	
	/**
	 * Busca os Animais de acordo com o Cliente selecionado
	 * 
	 * @param request
	 * @param bean
	 * @author Marcos Lisboa
	 */
	public ModelAndView ajaxLoadAnimalByCliente(WebRequestContext request, Cliente bean){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		try {
			jsonModelAndView.addObject("listAnimais", View.convertToJson(animalService.findExcetoObitoByCliente(bean)));
		} catch (Exception e) {
			e.printStackTrace();
			jsonModelAndView.addObject("erro", e.getMessage());
		}
		return jsonModelAndView;
	}
	
	/**
	* M�todo que redireciona para a tela de entrada da agenda de intera��o
	*
	* @param request
	* @param form
	* @return
	* @throws CrudException
	* @since 23/08/2016
	* @author Luiz Fernando
	*/
	public ModelAndView abrirTelaCriacaoAgendaInteracao(WebRequestContext request, Agendainteracao form) throws CrudException {
		form.setAgendainteracaorelacionado(Agendainteracaorelacionado.AVULSO);
		form.setPeriodicidade(new Frequencia(Frequencia.UNICA));
		request.setAttribute("abrirTelaCriacaoAgendaInteracao", true);
		return getCriarModelAndView(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Agendainteracao bean)
			throws Exception {
		String observacaoHistorico = bean.getCdagendainteracao() == null? "Criada uma agenda de intera��o": "Alterado";
		try {
			boolean isCriar = bean.getCdagendainteracao() == null;
			super.salvar(request, bean);
			
    		Agendainteracaohistorico agendainteracaohistorico = new Agendainteracaohistorico();
    		agendainteracaohistorico.setAgendainteracao(bean);
			agendainteracaohistorico.setObservacao(observacaoHistorico);
			agendainteracaohistoricoService.saveOrUpdate(agendainteracaohistorico);
			
			if(isCriar){
				agendainteracaoService.registrarHistoricoClienteConta(bean, observacaoHistorico);
			}
			
			
		}catch(Exception e){
			throw new SinedException(e.getMessage());
		}
	}
	
	public ModelAndView ajaxCarregaDadosContato(WebRequestContext request, AgendainteracaoFiltro filtro){
		String contato = "";
		if(filtro.getTipoagendainteracao() == Tipoagendainteracao.CLIENTE && filtro.getCliente()!=null){
			Cliente cliente = clienteService.findForContato(filtro.getCliente());
			if(cliente.getContatoResponsavel()!=null){
				contato = cliente.getContatoResponsavel().getNome();
				String telefone = cliente.getContatoResponsavel().getTelefonesSeparadosPorBarra();
				if(telefone!=null){
					contato += " - "+cliente.getContatoResponsavel().getTelefonesSeparadosPorBarra();
				}
			}
		}else if(filtro.getTipoagendainteracao()==Tipoagendainteracao.CONTA && filtro.getContacrm()!=null){
			Contacrm conta = contacrmService.findForContato(filtro.getContacrm());
			for(Contacrmcontato contatocrm: conta.getListcontacrmcontato()){
				String telefones = contatocrm.getTelefonesSeparadosPorBarra();
				if(contatocrm.getNome()!=null && telefones!=null){
					contato = contatocrm.getNome()+" - "+telefones;
					break;
				}
			}
			//O primeiro contato encontrado com n�mero de telefone preenchido ser� retornado.
			//Caso nenhum possua telefone preenchido, retorna o primeiro contato da lista.
			if(contato == "" &&
				conta.getListcontacrmcontato() != null && conta.getListcontacrmcontato().size()>0){
				contato = conta.getListcontacrmcontato().get(0).getNome();
			}
		}
		return new JsonModelAndView().addObject("contato", contato);
	}

	@Override
	public ModelAndView doExportar(WebRequestContext request,
			AgendainteracaoFiltro filtro) throws CrudException, IOException {
		Resource resource = agendainteracaoService.gerarListagemCSV(filtro);
		return new ResourceModelAndView(resource);
	}
}