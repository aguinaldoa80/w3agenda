package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Campanhacontato;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatoemail;
import br.com.linkcom.sined.geral.bean.Contacrmcontatofone;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Contacrmsegmento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leademail;
import br.com.linkcom.sined.geral.bean.Leadsegmento;
import br.com.linkcom.sined.geral.bean.Leadtelefone;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadehistorico;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Tipopessoacrm;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.service.CampanhacontatoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContacrmcontatoService;
import br.com.linkcom.sined.geral.service.ContacrmcontatoemailService;
import br.com.linkcom.sined.geral.service.ContacrmcontatofoneService;
import br.com.linkcom.sined.geral.service.ContacrmhistoricoService;
import br.com.linkcom.sined.geral.service.ContacrmsegmentoService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.LeademailService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.LeadsegmentoService;
import br.com.linkcom.sined.geral.service.LeadtelefoneService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OportunidadeestagioService;
import br.com.linkcom.sined.geral.service.OportunidadehistoricoService;
import br.com.linkcom.sined.geral.service.OportunidadematerialService;
import br.com.linkcom.sined.geral.service.OportunidadesituacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ContacrmFiltro;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.LeadFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path = "/crm/crud/Contacrm", authorizationModule = CrudAuthorizationModule.class)
@ExportCSV(fields = "")
public class ContacrmCrud extends CrudControllerSined<ContacrmFiltro, Contacrm, Contacrm> {

	protected OportunidadeService oportunidadeService;
	protected ContacrmService contacrmService;
	protected ContacrmcontatoService contacrmcontatoService;
	protected ContacrmsegmentoService contacrmsegmentoService;
	protected ContacrmhistoricoService contacrmhistoricoService;
	protected OportunidadehistoricoService oportunidadehistoricoService;
	protected OportunidadesituacaoService oportunidadesituacaoService;
	protected OportunidadeestagioService oportunidadeestagioService;
	protected ColaboradorService colaboradorService;
	protected LeadService leadService;
	protected LeademailService leademailService;
	protected LeadtelefoneService leadtelefoneService;
	protected LeadsegmentoService leadsegmentoService;
	protected LeadhistoricoService leadhistoricoService;
	protected ContacrmcontatoemailService contacrmcontatoemailService;
	protected ContacrmcontatofoneService contacrmcontatofoneService;	
	protected MaterialService materialService;
	protected CampanhacontatoService campanhacontatoService;
	protected OportunidadematerialService oportunidadematerialService;
	protected ParametrogeralService parametrogeralService;
	
	public void setCampanhacontatoService(CampanhacontatoService campanhacontatoService) {
		this.campanhacontatoService = campanhacontatoService;
	}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	public void setContacrmcontatoService(
			ContacrmcontatoService contacrmcontatoService) {
		this.contacrmcontatoService = contacrmcontatoService;
	}
	public void setContacrmsegmentoService(
			ContacrmsegmentoService contacrmsegmentoService) {
		this.contacrmsegmentoService = contacrmsegmentoService;
	}
	public void setContacrmhistoricoService(
			ContacrmhistoricoService contacrmhistoricoService) {
		this.contacrmhistoricoService = contacrmhistoricoService;
	}
	public void setOportunidadehistoricoService(
			OportunidadehistoricoService oportunidadehistoricoService) {
		this.oportunidadehistoricoService = oportunidadehistoricoService;
	}
	public void setOportunidadesituacaoService(
			OportunidadesituacaoService oportunidadesituacaoService) {
		this.oportunidadesituacaoService = oportunidadesituacaoService;
	}
	public void setOportunidadeestagioService(
			OportunidadeestagioService oportunidadeestagioService) {
		this.oportunidadeestagioService = oportunidadeestagioService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setLeademailService(LeademailService leademailService) {
		this.leademailService = leademailService;
	}
	public void setLeadtelefoneService(LeadtelefoneService leadtelefoneService) {
		this.leadtelefoneService = leadtelefoneService;
	}
	public void setLeadsegmentoService(LeadsegmentoService leadsegmentoService) {
		this.leadsegmentoService = leadsegmentoService;
	}
	public void setLeadhistoricoService(
			LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setContacrmcontatoemailService(
			ContacrmcontatoemailService contacrmcontatoemailService) {
		this.contacrmcontatoemailService = contacrmcontatoemailService;
	}
	public void setContacrmcontatofoneService(
			ContacrmcontatofoneService contacrmcontatofoneService) {
		this.contacrmcontatofoneService = contacrmcontatofoneService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setOportunidadematerialService(OportunidadematerialService oportunidadematerialService) {
		this.oportunidadematerialService = oportunidadematerialService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	protected Contacrm criar(WebRequestContext request, Contacrm form)throws Exception {
		Boolean conerterLead= Boolean.valueOf(request.getParameter("converterLead") != null ? request.getParameter("converterLead") : "false");
		if(request.getSession().getAttribute("contacrmConverterLead") != null && conerterLead){
			form = (Contacrm) request.getSession().getAttribute("contacrmConverterLead");
			request.getSession().removeAttribute("contacrmConverterLead");
			return form;
		}
		return super.criar(request, form);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void entrada(WebRequestContext request, Contacrm form) throws Exception {
		if(form.getIdentificadortela() == null) form.setIdentificadortela(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
		request.setAttribute("modulo", "crm");
				
		if(form.getCdcontacrm() != null){
			form.setListoportunidade(oportunidadeService.findByContacrm(form));
			form.setListcontacrmcontato(contacrmcontatoService.findByContacrm(form));
			form.setListcontacrmsegmento(contacrmsegmentoService.findByContacrm(form));
			boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
			form.setPermitidoEdicao(!isRestricaoClienteVendedor ||
											SinedUtil.getUsuarioLogado().equals(form.getResponsavel()));
		}

		if (request.getBindException().hasErrors()) {
			Object attrContacrm = request.getSession().getAttribute("listcontacrmcontato");
			if (attrContacrm != null) {
				form.setListcontacrmcontato((List<Contacrmcontato>) attrContacrm);
			}
			
			Object attrOP = request.getSession().getAttribute("listoportunidade"+form.getIdentificadortela());
			if (attrOP != null) {
				form.setListoportunidade((List<Oportunidade>) attrOP);
			}
		}
		
		if(form.getListcontacrmhistorico() != null){
			for(Contacrmhistorico historico : form.getListcontacrmhistorico()){
				if(historico.getObservacao() != null)
					historico.setObservacao(historico.getObservacao().replaceAll("\n", "<br>"));
			}
		}
		
		//verifica parametro no sistema para tornar obrigatorio campo observacao
		request.setAttribute("OBRIGAR_OBS_CONTA", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_OBS_CONTA));
		request.getSession().setAttribute("listcontacrmcontato", form.getListcontacrmcontato());
		request.getSession().setAttribute("listoportunidade"+form.getIdentificadortela(), form.getListoportunidade());
		super.entrada(request, form);
	}

	@Action("gerarconta")
	@SuppressWarnings("unchecked")
	public ModelAndView gerarConta(WebRequestContext request, LeadFiltro filtro)
			throws CrudException {

		String whereIn = request.getParameter("selectedItens");

		if (whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}

		Lead lead = leadService.findForContacrm(Integer.parseInt(whereIn));
		Contacrm contacrm = new Contacrm();

		contacrm.setIsLead(true);
		contacrm.setLead(lead);
		contacrm.setNome(lead.getNome());
		contacrm.setResponsavel(lead.getResponsavel());

		Contacrmcontato contato = new Contacrmcontato();

		List<Contacrmsegmento> listasegmento = new ArrayList<Contacrmsegmento>();
		lead.setListLeadsegmento(leadsegmentoService.findByLead(lead));
		if (lead.getListLeadsegmento() != null
				|| (!lead.getListLeadsegmento().isEmpty())) {
			for (Leadsegmento leadsegmento : lead.getListLeadsegmento()) {
				Contacrmsegmento seg = new Contacrmsegmento();
				seg.setSegmento(leadsegmento.getSegmento());
				listasegmento.add(seg);
			}
			contacrm.setListcontacrmsegmento(listasegmento);
		}

		contato.setLead(lead);
		contato.setNome(lead.getNome() != null ? lead.getNome() : "");
		contato.setSexo(lead.getSexo());

		contato.setUf(lead.getMunicipio() != null ? lead.getMunicipio().getUf()
				: null);
		contato.setMunicipio(lead.getMunicipio());
		contato.setLogradouro(lead.getLogradouro());
		contato.setNumero(lead.getNumero());
		contato.setComplemento(lead.getComplemento());
		contato.setBairro(lead.getBairro());
		contato.setCep(lead.getCep());

		Set<Contacrmcontatoemail> listaemail = new ListSet<Contacrmcontatoemail>(
				Contacrmcontatoemail.class);
		lead.setListleademail(SinedUtil.listToSet(leademailService
				.findByLead(lead), Leademail.class));
		if (lead.getListleademail() != null
				|| !lead.getListleademail().isEmpty()) {
			for (Leademail leademail : lead.getListleademail()) {
				Contacrmcontatoemail email = new Contacrmcontatoemail();
				email.setEmail(leademail.getEmail());
				listaemail.add(email);
			}
			contato.setListcontacrmcontatoemail(listaemail);
		}

		Set<Contacrmcontatofone> listafone = new ListSet<Contacrmcontatofone>(
				Contacrmcontatofone.class);
		lead.setListLeadtelefone(SinedUtil.listToSet(leadtelefoneService
				.findByLead(lead), Leademail.class));
		if (lead.getListLeadtelefone() != null
				|| !lead.getListLeadtelefone().isEmpty()) {
			for (Leadtelefone leadtelefone : lead.getListLeadtelefone()) {
				Contacrmcontatofone fone = new Contacrmcontatofone();
				fone.setTelefone(leadtelefone.getTelefone());
				fone.setTelefonetipo(leadtelefone.getTelefonetipo());
				listafone.add(fone);
			}
			contato.setListcontacrmcontatofone(listafone);
		}

		List<Contacrmcontato> listacontato = new ArrayList<Contacrmcontato>();
		listacontato.add(contato);
		contacrm.setListcontacrmcontato(listacontato);

		StringBuilder sb = new StringBuilder();

		sb.append("Importa��o do Lead "
				+ "<a href=\"/w3erp/crm/crud/Lead?ACAO=consultar&cdlead="
				+ lead.getCdlead() + "\" >" + lead.getCdlead() + "</a>");

		contacrm.setObservacaoGerarconta(sb.toString());
		contacrm.setObservacao("Importa��o do Lead " + lead.getCdlead());

		return getCriarModelAndView(request, contacrm);
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void salvar(WebRequestContext request, Contacrm bean)	throws Exception {
		
		Object attrContatocrm = request.getSession().getAttribute("listcontacrmcontato");
		Object attrOportunidade = request.getSession().getAttribute("listoportunidade"+bean.getIdentificadortela());
		List<String> listaNome = new ArrayList<String>();
		if (attrContatocrm != null) {
			bean.setListcontacrmcontato((List<Contacrmcontato>)attrContatocrm);
						
			for(Contacrmcontato contato : bean.getListcontacrmcontato()){
				if(contato.getCdcontacrmcontato()== null){
					listaNome.add(contato.getNome());
				}
			}
		}
		if (attrOportunidade != null) {
			bean.setListoportunidade((List<Oportunidade>)attrOportunidade);
		}
		
		List<Oportunidadehistorico> listOpHistorico = new ArrayList<Oportunidadehistorico>();
		
		try{
			
			Boolean isLead = bean.getIsLead();
			String observacaoGerarconta = bean.getObservacaoGerarconta();
			String observacaoConverterlead = bean.getObservacaoConverterlead();
			boolean converterLead = bean.getConverterlead() != null ? bean.getConverterlead() : false;
			bean.setConverterlead(false);
			super.salvar(request, bean);
			
			if(isLead != null && isLead && bean.getListoportunidade() != null){
				
				for(Oportunidade oportunidade : bean.getListoportunidade()){
					if (converterLead){
						oportunidade.setContacrm(bean);
						oportunidade.setTipopessoacrm(Tipopessoacrm.CONTA);
						if(oportunidade.getListaOportunidadematerial() != null && !oportunidade.getListaOportunidadematerial().isEmpty()){
							Money valortotal = new Money();
							for(Oportunidadematerial oportunidadematerial : oportunidade.getListaOportunidadematerial()){
								if(oportunidadematerial.getTotal() != null){
									valortotal = valortotal.add(new Money(oportunidadematerial.getTotal()));
								}
							}
							oportunidade.setValortotal(valortotal);
						}
						oportunidadeService.saveOrUpdate(oportunidade);
						
						// atualiza a data de conversao do lead
						String cdLead = request.getSession().getAttribute("cdLead").toString();
						if(cdLead != null && !cdLead.equals("")) {
							Lead lead = new Lead(Integer.parseInt(cdLead));
							lead.setDtconversao(new Date(System.currentTimeMillis()));
							leadService.updateDataConversaoLead(lead);
						}
						
					}
					if(oportunidade.getListoportunidadehistorico() != null){	
						for(Oportunidadehistorico oportunidadehistorico : oportunidade.getListoportunidadehistorico()){	
						
							oportunidadehistorico.setOportunidade(oportunidade);
							oportunidadehistorico.setObservacao("Converter LEAD");
							oportunidadehistorico.setDtaltera(new Timestamp(Calendar.getInstance().getTimeInMillis()));
							oportunidadehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
							oportunidadehistorico.setOportunidadesituacao(oportunidade.getOportunidadesituacao());
							oportunidadehistorico.setProbabilidade(oportunidade.getProbabilidade());
							oportunidadehistoricoService.saveOrUpdate(oportunidadehistorico);
							
							listOpHistorico.add(oportunidadehistorico);
							oportunidade.setListoportunidadehistorico(listOpHistorico);	
						
							listOpHistorico.add(oportunidadehistorico);
						}						
					} else{
						Oportunidadehistorico oportunidadehistorico = new Oportunidadehistorico();
						
						oportunidadehistorico.setOportunidade(oportunidade);
						oportunidadehistorico.setObservacao("Converter LEAD");
						oportunidadehistorico.setDtaltera(new Timestamp(Calendar.getInstance().getTimeInMillis()));
						oportunidadehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
						oportunidadehistorico.setOportunidadesituacao(oportunidade.getOportunidadesituacao());
						oportunidadehistorico.setProbabilidade(oportunidade.getProbabilidade());
						oportunidadehistoricoService.saveOrUpdate(oportunidadehistorico);
					}
				}			
							
			}
			
			if(listaNome != null && listaNome.size() > 0){
				for (String string : listaNome) {
					Contacrmhistorico contacrmhistorico = new Contacrmhistorico();
					contacrmhistorico.setContacrm(bean);
					contacrmhistorico.setObservacao("Novo Contato: " + string);
					contacrmhistorico.setDtaltera(bean.getDtaltera());
					contacrmhistorico.setCdusuarioaltera(bean.getCdusuarioaltera());
					contacrmhistoricoService.saveOrUpdate(contacrmhistorico);
				}	
			}
			if(bean != null && bean.getObservacao() != null && !bean.getObservacao().trim().equals("")){
				Contacrmhistorico contacrmhistorico = new Contacrmhistorico();
								
				contacrmhistorico.setContacrm(bean);
				if(observacaoGerarconta != null && !"".equals(observacaoGerarconta))
					contacrmhistorico.setObservacao(observacaoGerarconta);
				else if(observacaoConverterlead != null && !"".equals(observacaoConverterlead))
					contacrmhistorico.setObservacao(observacaoConverterlead);
				else
					contacrmhistorico.setObservacao(bean.getObservacao());
				contacrmhistorico.setDtaltera(bean.getDtaltera());
				contacrmhistorico.setCdusuarioaltera(bean.getCdusuarioaltera());
				
				contacrmhistoricoService.saveOrUpdate(contacrmhistorico);
			}
			
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_CONTACRM_NOME")){
				throw new SinedException("Conta j� cadastrado no sistema.");
			}
		}
	}
	
	/**
	 * Action para incluir os contatos das contas na campanha selecionada no filtro.
	 *
	 * @see br.com.linkcom.sined.geral.service.ContacrmcontatoService#findByContacrm(String whereIn)
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @since 05/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView incluirCampanha(WebRequestContext request, ContacrmFiltro filtro){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(!contacrmService.usuarioPodeEditarContacrm(whereIn)){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pelo lead.");
		}else if(filtro != null && filtro.getCampanha() != null){
			List<Contacrmcontato> listaContacrmcontato = contacrmcontatoService.findByContacrm(whereIn);
			
			Campanhacontato campanhacontato;
			for (Contacrmcontato contacrmcontato : listaContacrmcontato) {
				campanhacontato = new Campanhacontato();
				campanhacontato.setCampanha(filtro.getCampanha());
				campanhacontato.setContacrmcontato(contacrmcontato);
				
				campanhacontatoService.saveOrUpdate(campanhacontato);
			}
			
			request.addMessage("Conta(s) inclu�das na campanha com sucesso."); 
		} else {
			request.addError("N�o foi poss�vel encontrar a campanha.");
		}
		
		return sendRedirectToAction("listagem");
	}
	
	@Override
	protected void listagem(WebRequestContext request, ContacrmFiltro filtro) throws Exception {
		request.setAttribute("TIPO_RESPONSAVEL_COLABORADOR", Tiporesponsavel.COLABORADOR);
	}

	@Override
	protected ListagemResult<Contacrm> getLista(WebRequestContext request, ContacrmFiltro filtro) {
		if (!filtro.isNotFirstTime()) {
			Colaborador usuarioLogado = SinedUtil.getUsuarioComoColaborador();
			if(usuarioLogado!=null){
				if(ColaboradorService.getInstance().isColaborador(usuarioLogado.getCdpessoa())){
					filtro.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
					filtro.setResponsavel(usuarioLogado);
				}else{
					filtro.setTiporesponsavel(Tiporesponsavel.AGENCIA);
					filtro.setAgencia(new Fornecedor(usuarioLogado.getCdpessoa()));
				}
			}
		}
		
		ListagemResult<Contacrm> listagemResult = super.getLista(request,filtro);
		List<Contacrm> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcontacrm",",");
		if (whereIn != null && !whereIn.equals("")) {
			list.removeAll(list);
			list.addAll(contacrmService.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}

		boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
		for (Contacrm conta : list) {
			if (conta.getListcontacrmcontato() != null) {
				String email = "";
				for (Contacrmcontato contato : conta.getListcontacrmcontato()) {
					if (contato.getListcontacrmcontatoemail() != null) {
						email = email + CollectionsUtil.listAndConcatenate(contato.getListcontacrmcontatoemail(),"email", "<br>") + "<br>";
					}

					conta.setListaEmails(email);

					if (contato.getListcontacrmcontatofone() != null) {
						String strFone = "";
						for (Contacrmcontatofone fone : contato.getListcontacrmcontatofone()) {
							strFone = strFone + fone.getTelefone() + " ("
									+ fone.getTelefonetipo().getNome() + ")<br>";
						}
						if (!strFone.equals("")) {
							strFone = strFone.substring(0, (strFone.length() - 4));
						}
						conta.setListaFones(strFone);
					}
				}
			}
			conta.setPermitidoEdicao(!isRestricaoClienteVendedor ||
					(conta.getResponsavel() != null && conta.getResponsavel().getCdpessoa() != null &&  SinedUtil.getUsuarioLogado().equals(conta.getResponsavel())));
		}

		return listagemResult;
	}

	@Action("criaoportunidade")
	public ModelAndView criaOportunidade(WebRequestContext request,Contacrm contacrm) {
		Integer cdcontacrm = Integer.parseInt(request.getParameter("cdcontacrm"));

		Contacrm ccrm = new Contacrm();
		ccrm.setCdcontacrm(cdcontacrm);

		Oportunidade op = new Oportunidade();

		if (ccrm != null) {
			ccrm = contacrmService.load(ccrm);
			op.setContacrm(ccrm);
			op.setResponsavel(ccrm.getResponsavel());
		}
		op.setDtInicio(new Date(System.currentTimeMillis()));

		return new ModelAndView("direct:crud/popup/criaOportunidade", "oportunidade", op);
	}


	public ModelAndView editarHistorico(WebRequestContext request, Contacrmhistorico contacrmhistorico) {
		//Verificando a obrigatoriedade da observa��o
		request.setAttribute("OBRIGAR_OBS_CONTA", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_OBS_CONTA));
		contacrmhistorico = contacrmhistoricoService.loadForEntrada(contacrmhistorico);
		return new ModelAndView("direct:/crud/popup/editarHistoricoContacrm","bean", contacrmhistorico);
	}

	public void saveHistorico(WebRequestContext request,
			Contacrmhistorico contacrmhistorico) {
		contacrmhistoricoService.saveOrUpdateWithoutLog(contacrmhistorico);
		request.addMessage("Hist�rico editado com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	@Action("converterlead")
	@SuppressWarnings("unchecked")
	public ModelAndView converterLead(WebRequestContext request, Oportunidade oportunidade) throws CrudException, IOException {

		String whereIn = oportunidade.getWhereIn();

		if (whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}

		Lead lead = leadService.findForContacrm(Integer.parseInt(whereIn));
		Contacrm contacrm = new Contacrm();

		contacrm.setIsLead(true);
		contacrm.setLead(lead);
		
		if(lead!=null && lead.getEmpresa()!=null && !lead.getEmpresa().isEmpty())
			contacrm.setNome(lead.getEmpresa());
		else
			contacrm.setNome(lead.getNome());
		
		if (lead != null) {
			contacrm.setWebsite(lead.getWebsite());
			contacrm.setConcorrente(lead.getConcorrente());
			
			if (lead.getTiporesponsavel() != null && lead.getTiporesponsavel().equals(Tiporesponsavel.COLABORADOR) && lead.getResponsavel() != null) {
				contacrm.setResponsavel(lead.getResponsavel());
				contacrm.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
				oportunidade.setResponsavel(lead.getResponsavel());
				oportunidade.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
			} else if (lead.getTiporesponsavel() != null && lead.getTiporesponsavel().equals(Tiporesponsavel.AGENCIA) && lead.getAgencia() != null){
				contacrm.setAgencia(lead.getAgencia());
				contacrm.setTiporesponsavel(Tiporesponsavel.AGENCIA);
				oportunidade.setResponsavel(lead.getAgencia());	
				oportunidade.setTiporesponsavel(Tiporesponsavel.AGENCIA);
			}
			
			// armazena o lead na sess�o para atualizar a data de convers�o ap�s salvar
			request.getSession().setAttribute("cdLead", lead.getCdlead());
		}
		
		Contacrmcontato contato = new Contacrmcontato();

		List<Contacrmsegmento> listasegmento = new ArrayList<Contacrmsegmento>();
		lead.setListLeadsegmento(leadsegmentoService.findByLead(lead));
		if (lead.getListLeadsegmento() != null
				|| (!lead.getListLeadsegmento().isEmpty())) {
			for (Leadsegmento leadsegmento : lead.getListLeadsegmento()) {
				Contacrmsegmento seg = new Contacrmsegmento();
				seg.setSegmento(leadsegmento.getSegmento());
				listasegmento.add(seg);
			}
			contacrm.setListcontacrmsegmento(listasegmento);
		}

		contato.setLead(lead);
		contato.setNome(lead.getNome() != null ? lead.getNome() : "");
		contato.setSexo(lead.getSexo());

		contato.setUf(lead.getMunicipio() != null ? lead.getMunicipio().getUf()
				: null);
		contato.setMunicipio(lead.getMunicipio());
		contato.setLogradouro(lead.getLogradouro());
		contato.setNumero(lead.getNumero());
		contato.setComplemento(lead.getComplemento());
		contato.setBairro(lead.getBairro());
		contato.setCep(lead.getCep());

		Set<Contacrmcontatoemail> listaemail = new ListSet<Contacrmcontatoemail>(
				Contacrmcontatoemail.class);
		if(StringUtils.isNotEmpty(lead.getEmail())){
			Contacrmcontatoemail email = new Contacrmcontatoemail();
			email.setEmail(lead.getEmail());
			listaemail.add(email);
			contato.setListcontacrmcontatoemail(listaemail);
		}
		
		Set<Contacrmcontatofone> listafone = new ListSet<Contacrmcontatofone>(
				Contacrmcontatofone.class);
		lead.setListLeadtelefone(SinedUtil.listToSet(leadtelefoneService
				.findByLead(lead), Leademail.class));
		if (lead.getListLeadtelefone() != null
				|| !lead.getListLeadtelefone().isEmpty()) {
			for (Leadtelefone leadtelefone : lead.getListLeadtelefone()) {
				Contacrmcontatofone fone = new Contacrmcontatofone();
				fone.setTelefone(leadtelefone.getTelefone());
				fone.setTelefonetipo(leadtelefone.getTelefonetipo());
				listafone.add(fone);
			}
			contato.setListcontacrmcontatofone(listafone);
		}		
		
		List<Contacrmcontato> listacontato = new ArrayList<Contacrmcontato>();
		listacontato.add(contato);
		
		lead.setListleademail(SinedUtil.listToSet(leademailService
				.findByLead(lead), Leademail.class));
		
		if (SinedUtil.isListNotEmpty(lead.getListleademail())){
			for (Leademail leademail : lead.getListleademail()) {
				if(StringUtils.isNotEmpty(leademail.getEmail())){
					Contacrmcontato contatoEmail = new Contacrmcontato();
					contatoEmail.setNome(leademail.getContato());
					
					Set<Contacrmcontatoemail> listaContacrmcontatoemail = new ListSet<Contacrmcontatoemail>(
							Contacrmcontatoemail.class);
					Contacrmcontatoemail email = new Contacrmcontatoemail();
					email.setEmail(leademail.getEmail());
					listaContacrmcontatoemail.add(email);
					
					contatoEmail.setListcontacrmcontatoemail(listaContacrmcontatoemail);
					listacontato.add(contatoEmail);
				}
			}
		}
		
		contacrm.setListcontacrmcontato(listacontato);

		List<Oportunidade> listOp = new ArrayList<Oportunidade>();		
		if(oportunidade.getNome() != null && oportunidade.getDtInicio() != null &&
				oportunidade.getResponsavel() != null && oportunidade.getOportunidadesituacao() != null){			
			contacrm.setOportunidadenome(oportunidade.getNome());
			Money valortotal = new Money();
			if(oportunidade.getListaOportunidadematerial() != null && !oportunidade.getListaOportunidadematerial().isEmpty()){
				for(Oportunidadematerial op : oportunidade.getListaOportunidadematerial()){
					if(op.getMaterial() != null && op.getMaterial().getNome() == null){
						op.setMaterial(materialService.load(op.getMaterial(), "material.cdmaterial, material.nome"));
					}
					if(op.getTotal() != null){
						valortotal = valortotal.add(new Money(op.getTotal()));
					}
				}
			}
			listOp.add(oportunidade);
			contacrm.setListoportunidade(listOp);
		}		
		
		contacrm.setConverterlead(true);

		contacrm.setObservacao("Importa��o do Lead " + lead.getCdlead());
		contacrm.setObservacaoConverterlead("Importa��o do Lead <a href=\"/w3erp/crm/crud/Lead?ACAO=consultar&cdlead=" + lead.getCdlead() +
												"\" >" + lead.getCdlead() + "</a>");
		
		
		request.getSession().setAttribute("contacrmConverterLead", contacrm);
		SinedUtil.redirecionamento(request, "/crm/crud/Contacrm?ACAO=criar&converterLead=true");
		return null;
//		return getCriarModelAndView(request, contacrm);
	}
	
	/**
	 * Action para desassociar os leads da campanha selecionada no filtro.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView desassociarCampanha(WebRequestContext request, ContacrmFiltro filtro){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(!contacrmService.usuarioPodeEditarContacrm(whereIn)){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pelo lead.");
		}else if(filtro != null && filtro.getCampanha() != null){

			StringBuilder registrosInvalidos = new StringBuilder();
			StringBuilder registrosAlterados = new StringBuilder();
			
			try {
				List<Contacrm> lista = contacrmService.findAllForDesassociarCampanha(whereIn);
				for(Contacrm contacrm : lista){
					if(contacrm.getListcontacrmcontato() == null || contacrm.getListcontacrmcontato().isEmpty()){
						registrosInvalidos.append("<a href=\"javascript:visualizarContacrm("+contacrm.getCdcontacrm()+");\">"+contacrm.getCdcontacrm()+",</a> ");
					}
					else{
						for(Contacrmcontato contacrmcontato : contacrm.getListcontacrmcontato()){
							if(contacrmcontato.getListcampanhacontato() == null || contacrmcontato.getListcampanhacontato().isEmpty()){
								registrosInvalidos.append("<a href=\"javascript:visualizarContacrm("+contacrm.getCdcontacrm()+");\">"+contacrm.getCdcontacrm()+",</a> ");
							}
							else{
								Boolean hasCampanha = Boolean.FALSE;
								for(Campanhacontato cc : contacrmcontato.getListcampanhacontato()){
									if(cc.getCampanha() != null && cc.getCampanha().getCdcampanha().equals(filtro.getCampanha().getCdcampanha())){
										campanhacontatoService.delete(cc);
										hasCampanha = Boolean.TRUE;
									}
								}
								if(hasCampanha){
									registrosAlterados.append("<a href=\"javascript:visualizarContacrm("+contacrm.getCdcontacrm()+");\">"+contacrm.getCdcontacrm()+",</a>, ");
								}
								else{
									registrosInvalidos.append("<a href=\"javascript:visualizarContacrm("+contacrm.getCdcontacrm()+");\">"+contacrm.getCdcontacrm()+",</a> ");
								}
							}
						}
					}
				}
				if(registrosAlterados.length() > 0){
					registrosAlterados.replace(registrosAlterados.lastIndexOf(","), registrosAlterados.length()-1, ".");
					request.addMessage("Contas desassociadas com sucesso: "+registrosAlterados.toString());
				}
				if(registrosInvalidos.length() > 0){
					registrosInvalidos.replace(registrosInvalidos.lastIndexOf(","), registrosInvalidos.length()-1, ".");
					request.addError("As seguintes contas n�o possuem v�nculo com a campanha selecionada: " + registrosInvalidos.toString());
				}
			} catch (Exception e) {
//				System.out.println("Erro ao desassociar a campanha das contas selecionados.");
				e.printStackTrace();
			}
		} else{
			request.addError("N�o foi poss�vel encontrar a campanha.");
		}
		
		return sendRedirectToAction("listagem");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Contacrm bean)
			throws Exception {
		String itens = request.getParameter("itenstodelete");

		if(Util.strings.isNotEmpty(itens) && !contacrmService.usuarioPodeEditarContacrm(itens)){
			request.addError("Exclus�o n�o permitida. Usu�rio logado n�o � o respons�vel pela conta.");
		}else if(bean != null && bean.getCdcontacrm() != null && !contacrmService.usuarioPodeEditarContacrm(bean.getCdcontacrm().toString())){
			request.addError("Exclus�o n�o permitida. Usu�rio logado n�o � o respons�vel pela conta.");
		}else{
			super.excluir(request, bean);
		}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Contacrm form)
			throws CrudException {
		boolean editando = "editar".equalsIgnoreCase(request.getParameter("ACAO"));
		if(editando && form.getCdcontacrm() != null){
			Contacrm bean = contacrmService.loadWithResponsavel(form.getCdcontacrm());
			boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
			boolean permitidaEdicao = !isRestricaoClienteVendedor || SinedUtil.getUsuarioLogado().equals(bean.getResponsavel());
			if(!permitidaEdicao){
				throw new SinedException("Edi��o n�o permitida. Usu�rio logado n�o � o respons�vel pela Conta.");
			}
		}
		
		return super.doEditar(request, form);
	}

	@Override
	public ModelAndView doExportar(WebRequestContext request,
			ContacrmFiltro filtro) throws CrudException, IOException {
		Resource resource = contacrmService.gerarListagemCSV(filtro);
		
		return new ResourceModelAndView(resource);
	}
}