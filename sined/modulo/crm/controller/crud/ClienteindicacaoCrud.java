package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Clienteindicacao;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteindicacaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(
		path="/crm/crud/Clienteindicacao",
		authorizationModule=CrudAuthorizationModule.class
)
public class ClienteindicacaoCrud extends CrudControllerSined<ClienteindicacaoFiltro, Clienteindicacao, Clienteindicacao>{
	
	@Override
	protected void salvar(WebRequestContext request, Clienteindicacao bean) throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_clienteindicacao_nome")) 
				throw new SinedException("Indica��o j� cadastrada no sistema.");	
		}
	}

}
