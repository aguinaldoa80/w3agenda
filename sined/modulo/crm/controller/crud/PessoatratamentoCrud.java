package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Pessoatratamento;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.PessoatratamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(
		path="/crm/crud/Pessoatratamento",
		authorizationModule=CrudAuthorizationModule.class
)
public class PessoatratamentoCrud extends CrudControllerSined<PessoatratamentoFiltro, Pessoatratamento, Pessoatratamento>{
	
	@Override
	protected void salvar(WebRequestContext request, Pessoatratamento bean) throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_pessoatratamento_nome")) 
				throw new SinedException("Tratamento para pessoa j� cadastrada no sistema.");	
		}
	}

}
