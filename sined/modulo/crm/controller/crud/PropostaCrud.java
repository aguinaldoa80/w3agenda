package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.geral.bean.Interacaoitem;
import br.com.linkcom.sined.geral.bean.Interacaotipo;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.PropostaArquivo;
import br.com.linkcom.sined.geral.bean.PropostaCaixaItem;
import br.com.linkcom.sined.geral.bean.PropostaItem;
import br.com.linkcom.sined.geral.bean.Propostarevisao;
import br.com.linkcom.sined.geral.bean.Propostasituacao;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.InteracaoitemService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.PropostaArquivoService;
import br.com.linkcom.sined.geral.service.PropostaCaixaItemService;
import br.com.linkcom.sined.geral.service.PropostaItemService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.PropostacaixaService;
import br.com.linkcom.sined.geral.service.PropostarevisaoService;
import br.com.linkcom.sined.geral.service.PropostasituacaoService;
import br.com.linkcom.sined.geral.service.ProspeccaoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.PropostaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path = "/crm/crud/Proposta", authorizationModule = CrudAuthorizationModule.class)
@ExportCSV(fields = {""})
public class PropostaCrud extends
		CrudControllerSined<PropostaFiltro, Proposta, Proposta> {

	private PropostasituacaoService propostasituacaoService;
	private PropostaService propostaService;
	private InteracaoitemService interacaoitemService;
	private PropostarevisaoService propostarevisaoService;
	private ProspeccaoService prospeccaoService;
	private ContatoService contatoService;
	private MunicipioService municipioService;
	private PropostaCaixaItemService propostaCaixaItemService;
	private PropostaItemService propostaItemService;
	private PropostaArquivoService propostaArquivoService;
	private PropostacaixaService propostacaixaService;
	private EmpresaService empresaService;
	private ClienteService clienteService;

	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setPropostaItemService(PropostaItemService propostaItemService) {this.propostaItemService = propostaItemService;}
	public void setMunicipioService(MunicipioService municipioService) {this.municipioService = municipioService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setProspeccaoService(ProspeccaoService prospeccaoService) {this.prospeccaoService = prospeccaoService;}
	public void setPropostarevisaoService(PropostarevisaoService propostarevisaoService) {this.propostarevisaoService = propostarevisaoService;}
	public void setInteracaoitemService(InteracaoitemService interacaoitemService) {this.interacaoitemService = interacaoitemService;}
	public void setPropostaService(PropostaService propostaService) {this.propostaService = propostaService;}
	public void setPropostasituacaoService(PropostasituacaoService propostasituacaoService) {this.propostasituacaoService = propostasituacaoService;}
	public void setPropostaCaixaItemService(PropostaCaixaItemService propostaCaixaItemService) {this.propostaCaixaItemService = propostaCaixaItemService;}
	public void setPropostaArquivoService(PropostaArquivoService propostaArquivoService) {this.propostaArquivoService = propostaArquivoService;}
	public void setPropostacaixaService(PropostacaixaService propostacaixaService) {this.propostacaixaService = propostacaixaService;}

	@Override
	protected Proposta criar(WebRequestContext request, Proposta form)
			throws Exception {
		Proposta bean = super.criar(request, form);

		String descricao = request.getParameter("descricao");
		String cdpessoa = request.getParameter("cdpessoa");
		String cdmunicipio = request.getParameter("cdmunicipio");
		String cdorcamento = request.getParameter("cdorcamento");

		if (descricao != null && !descricao.equals("")) {
			bean.setDescricao(descricao);
		}
		if (cdpessoa != null && !cdpessoa.equals("")) {
			bean.setCliente(new Cliente(Integer.parseInt(cdpessoa)));
		}
		if (cdmunicipio != null && !cdmunicipio.equals("")) {
			bean.setMunicipio(municipioService.load(new Municipio(Integer
					.parseInt(cdmunicipio))));
		}
		if (cdorcamento != null && !cdorcamento.equals("")) {
			Orcamento orcamento = new Orcamento(Integer.parseInt(cdorcamento));

			Money valorMDO = propostaService.getValorMDO(orcamento);
			Money valorMateriais = propostaService.getValorMateriais(orcamento);
			Money valorMobilizacao = propostaService
					.getValorMobilizacao(orcamento);

			Propostarevisao propostarevisao = new Propostarevisao();
			propostarevisao.setMdo(valorMDO);
			propostarevisao.setMateriais(valorMateriais);
			propostarevisao.setMobilizacao(valorMobilizacao);

			List<Propostarevisao> listaPropostarevisao = new ArrayList<Propostarevisao>();
			listaPropostarevisao.add(propostarevisao);

			bean.setListaPropostarevisao(listaPropostarevisao);
		}

		return bean;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Proposta form)
			throws Exception {

		if (form.getCdproposta() == null) {
			if (request.getParameter("cdprospeccao") != null) {
				Prospeccao prospeccao = new Prospeccao();
				prospeccao.setCdprospeccao(Integer.parseInt(request
						.getParameter("cdprospeccao")));
				prospeccao = prospeccaoService.loadForNew(prospeccao);
				form.setDescricao(prospeccao.getDescricao());
				form.setCliente(prospeccao.getCliente());
				form.setContato(prospeccao.getContato());
				form.setProspeccao(prospeccao);
				form.setFromprospeccao(Boolean.TRUE);
			}
			form.setPropostasituacao(Propostasituacao.EM_ELABORACAO);

			String sufixo = new SimpleDateFormat("yy").format(new Date(System
					.currentTimeMillis()));
			form.setSufixo(sufixo);

			Integer ultimoNumero = propostaService.findUltimoNumero(sufixo);
			if (ultimoNumero != null) {
				form.setNumero(ultimoNumero + 1);
			} else {
				form.setNumero(1);
			}
		} else {
			if (form.getInteracao() != null
					&& form.getInteracao().getCdinteracao() != null) {
				Interacao interacao = form.getInteracao();
				interacao.setListaInteracaoitem(interacaoitemService
						.findByInteracao(interacao));
				form.setInteracao(interacao);
			}
			if(form.getCliente() != null && form.getCliente().getCdpessoa() != null){
				form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome"));
			}
			
			form.setListaItem(SinedUtil.listToSet(propostaItemService.findByProposta(form), PropostaItem.class));
			form.setListaArquivos(SinedUtil.listToSet(propostaArquivoService.findListByProposta(form), PropostaArquivo.class));
		}

		request.getSession().setAttribute("interacao", form.getInteracao());
		
		request.setAttribute("tamanhoLista",form.getListaPropostarevisao() != null ? form.getListaPropostarevisao().size() : 0);
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}

	@Override
	protected void salvar(WebRequestContext request, Proposta bean)
			throws Exception {

		bean.setInteracao((Interacao) request.getSession().getAttribute(
				"interacao"));

		if (bean != null && bean.getInteracao() != null
				&& bean.getInteracao().getListaInteracaoitem() != null) {
			List<Interacaoitem> lista = bean.getInteracao()
					.getListaInteracaoitem();
			for (Interacaoitem interacaoitem : lista) {
				if (interacaoitem.getArquivo() != null
						&& interacaoitem.getArquivo().getCdarquivo() == null) {
					interacaoitem.setArquivo(null);
				}
			}
			
			if(bean.getCdproposta() != null){
				Interacaoitem interacaoitem = new Interacaoitem();
				interacaoitem.setDe(SinedUtil.getUsuarioLogado().getNome());
				interacaoitem.setData(new java.sql.Date(System.currentTimeMillis()));
				Interacaotipo interacaotipo = new Interacaotipo();
				interacaotipo.setCdinteracaotipo(6);
				interacaoitem.setInteracaoTipo(interacaotipo);
				interacaoitem.setAssunto("Proposta alterada");
				
				lista.add(interacaoitem);
			}
		}

		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PROPOSTA_NUMERO")) {
				throw new SinedException("Proposta j� cadastrada no sistema.");
			}
		}

		if (bean.getFromprospeccao() != null && bean.getFromprospeccao()) {
			bean.setFromprospeccao(false);
		}
	}
	
	@Override
	protected ListagemResult<Proposta> getLista(WebRequestContext request, PropostaFiltro filtro) {
		ListagemResult<Proposta> listagemResult = super.getLista(request, filtro);
		List<Proposta> lista = listagemResult.list();
		
		java.sql.Date dtatual = SinedDateUtils.currentDate();
		java.sql.Date dtreferenciarAvencer = SinedDateUtils.addDiasData(dtatual, 5);
		for (Proposta proposta : lista) {
			
			boolean corvermelho = proposta.getAux_proposta() != null &&
								 proposta.getAux_proposta().getDtvalidade() != null &&
								 (Propostasituacao.EM_ELABORACAO.equals(proposta.getPropostasituacao()) ||
								 Propostasituacao.AGUARDANDO_POSICAO_CLIENTE.equals(proposta.getPropostasituacao())) &&		
								 (SinedDateUtils.beforeIgnoreHour(proposta.getAux_proposta().getDtvalidade(), dtatual) || 
								  (SinedDateUtils.afterOrEqualsIgnoreHour(proposta.getAux_proposta().getDtvalidade(), dtatual) && 
								  SinedDateUtils.beforeOrEqualIgnoreHour(proposta.getAux_proposta().getDtvalidade(), dtreferenciarAvencer)));
			
			proposta.setCorvermelho(corvermelho);
		}
		
		return listagemResult;
	}

	@Override
	public ModelAndView doListagem(WebRequestContext request,
			PropostaFiltro filtro) throws CrudException {

		String clienteParam = request.getParameter("cliente_filtro");
		if (clienteParam != null && !clienteParam.equals("")) {
			try {
				String id = clienteParam.substring(
						clienteParam.indexOf("=") + 1, clienteParam
								.indexOf(","));
				Cliente cliente = new Cliente(Integer.parseInt(id));
				filtro.setCliente(cliente);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return super.doListagem(request, filtro);
	}

	@Override
	protected void listagem(WebRequestContext request, PropostaFiltro filtro) throws Exception {
		request.setAttribute("listaSituacaoCompleta", propostasituacaoService.findAll());
		request.setAttribute("listaPropostaCaixa", propostacaixaService.findAll());
	}

	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request,
			Proposta form) {
		if (form.getFromprospeccao() != null && !form.getFromprospeccao()) {
			request.getServletResponse().setContentType("text/html");
			View
					.getCurrent()
					.println(
							"<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		} else {
			return super.getSalvarModelAndView(request, form);
		}
	}

	/**
	 * M�todo que carrega as informa��es para serem mostradas e o formul�rio
	 * para nova revis�o em popup.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PropostaService#loadWithView
	 * @param request
	 * @param proposta
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("criarevisao")
	public ModelAndView criaRevisao(WebRequestContext request, Proposta proposta) {
		Propostarevisao propostarevisao = new Propostarevisao();
		propostarevisao.setProposta(proposta);

		if (proposta.getCdorcamento() != null) {
			Orcamento orcamento = new Orcamento(proposta.getCdorcamento());

			propostarevisao.setCdorcamento(proposta.getCdorcamento());
			propostarevisao.setMateriais(propostaService
					.getValorMateriais(orcamento));
			propostarevisao.setMdo(propostaService.getValorMDO(orcamento));
			propostarevisao.setMobilizacao(propostaService
					.getValorMobilizacao(orcamento));
		} else {
			proposta = propostaService.loadWithView(proposta);

			propostarevisao
					.setMateriais(proposta.getAux_proposta() != null ? proposta
							.getAux_proposta().getMateriais() : null);
			propostarevisao
					.setMdo(proposta.getAux_proposta() != null ? proposta
							.getAux_proposta().getMdo() : null);
			propostarevisao
					.setMobilizacao(proposta.getAux_proposta() != null ? proposta
							.getAux_proposta().getMobilizacao()
							: null);
		}

		return new ModelAndView("direct:crud/popup/criaRevisao",
				"propostarevisao", propostarevisao);
	}

	/**
	 * Salva a revis�o no popup na listagem de proposta.
	 * 
	 * @param request
	 * @param propostarevisao
	 * @author Rodrigo Freitas
	 */
	@Action("salvarevisao")
	public void salvaRevisao(WebRequestContext request,
			Propostarevisao propostarevisao) {

		if (propostarevisao.getArquivoPCM() != null
				&& propostarevisao.getArquivoPCM().getNome() != null
				&& propostarevisao.getArquivoPCM().getNome().equals("")) {
			propostarevisao.setArquivoPCM(null);
		}
		if (propostarevisao.getArquivoPTM() != null
				&& propostarevisao.getArquivoPTM().getNome() != null
				&& propostarevisao.getArquivoPTM().getNome().equals("")) {
			propostarevisao.setArquivoPTM(null);
		}

		propostarevisaoService.saveOrUpdate(propostarevisao);

		if (propostarevisao.getCdorcamento() == null) {
			request.addMessage("Revis�o salva com sucesso.");
			request.getServletResponse().setContentType("text/html");
			View
					.getCurrent()
					.println(
							"<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
		} else {
			request.getServletResponse().setContentType("text/html");
			View
					.getCurrent()
					.println(
							"<script>setTimeout('window.close()', 500);window.opener.alert('Revis�o criada com sucesso.');</script>");
		}
	}

	public ModelAndView ajaxContato(WebRequestContext request, Proposta proposta) {
		if (proposta == null || proposta.getContato() == null
				|| proposta.getContato().getCdpessoa() == null) {
			throw new SinedException("Contato n�o pode ser nulo.");
		}

		Contato contato = contatoService.carregaContato(proposta.getContato());

		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("email", contato.getEmailcontato());
		jsonModelAndView.addObject("telefones", contato.getTelefones());
		jsonModelAndView.addObject("unidade", contato.getUnidade());

		return jsonModelAndView;
	}

	/**
	 * M�todo respons�vel por selecionar como default o mesmo cliente
	 * selecionado em Painel de intera��o.
	 * 
	 * @param request
	 * @param form
	 * @return
	 * @throws CrudException
	 * @author Taidson
	 * @since 17/09/2010
	 */
	public ModelAndView propostaPainelInteracao(WebRequestContext request,
			Proposta form) throws CrudException {
		if(form.getCliente() != null && form.getCliente().getCdpessoa() != null){
			Cliente cliente = clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome");
			if(cliente != null){
				form.getCliente().setNome(cliente.getNome());
			}
		}
		return getCriarModelAndView(request, form);
	}

	public ModelAndView ajaxCamposAdicionais(WebRequestContext request,
			Proposta bean) {
		if (bean.getPropostacaixa() == null
				|| bean.getPropostacaixa()
						.getCdpropostacaixa() == null) {
			throw new SinedException(
					"O item da caixa de proposta n�o pode se nulo.");
		}

		List<PropostaCaixaItem> lista = propostaCaixaItemService.findByItem(bean.getPropostacaixa());
		return new JsonModelAndView().addObject("size", lista.size())
				.addObject("lista", lista);
	}
	
	public ModelAndView jsonEmitirRTF(WebRequestContext request,
			Proposta bean){
		String cdProposta = request.getParameter("listaCdProposta");
		String [] listaString = null;
		if(cdProposta != null && !cdProposta.equals("")){
			listaString = cdProposta.split(",");
		}
		Proposta proposta;
		for (String string : listaString) {
			proposta = new Proposta();
			proposta.setCdproposta(Integer.parseInt(string));
			proposta = propostaService.carregarProposta(proposta);
			propostaService.makePropostaRTF(proposta);
		}
		return new JsonModelAndView();
	}
	
	/**
	 * Action em ajax para buscar os contatos de um cliente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContatoService#findByPessoa(Pessoa form)
	 *
	 * @param request
	 * @param proposta
	 * @return
	 * @since 09/07/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxBuscaContato(WebRequestContext request, Proposta proposta){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		List<Contato> listaContato = new ArrayList<Contato>();
		if(proposta.getCliente() != null && proposta.getCliente().getCdpessoa() != null){
			listaContato = contatoService.findByPessoa(proposta.getCliente());
		}
		return jsonModelAndView.addObject("listaContato", listaContato);
	}

	@Override
	public ModelAndView doExportar(WebRequestContext request,
			PropostaFiltro filtro) throws CrudException, IOException {
		Resource resource = propostaService.gerarListagemCSV(filtro);
		
		return new ResourceModelAndView(resource);
	}
}	