package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Especialidade;
import br.com.linkcom.sined.geral.service.ConselhoclasseprofissionalService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.EspecialidadeFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Especialidade"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdespecialidade", "descricao", "conselhoclasseprofissional", "ativo"})
public class EspecialidadeCrud extends CrudControllerSined<EspecialidadeFiltro, Especialidade, Especialidade>{
	
	private ConselhoclasseprofissionalService conselhoclasseprofissionalService;
	
	public void setConselhoclasseprofissionalService(ConselhoclasseprofissionalService conselhoclasseprofissionalService) {this.conselhoclasseprofissionalService = conselhoclasseprofissionalService;}
	
	@Override
	protected void listagem(WebRequestContext request, EspecialidadeFiltro filtro) throws Exception {
		request.setAttribute("listaConselhoclasseprofissional", conselhoclasseprofissionalService.findAtivos());
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Especialidade form) throws Exception {
		request.setAttribute("listaConselhoclasseprofissional", conselhoclasseprofissionalService.findAtivos());
		super.entrada(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Especialidade bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch(DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_ESPECIALIDADE_DESCRICAO")){
				throw new SinedException("Especialidade j� cadastrada no sistema.");
			}
		}
	}	
}