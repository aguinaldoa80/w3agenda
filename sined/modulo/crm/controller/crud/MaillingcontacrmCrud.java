package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatoemail;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.dao.MaillingcontacrmDAO;
import br.com.linkcom.sined.geral.service.CampanhaService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContacrmhistoricoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.EnvioemailitemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.SegmentoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingcontacrmFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/crm/crud/MaillingContascrm",
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = {"nome", "listaEmails"})
public class MaillingcontacrmCrud extends CrudControllerSined<MaillingcontacrmFiltro, Contacrm, Contacrm>{

	private ContacrmService contacrmService;
	private MaillingcontacrmDAO maillingcontacrmDAO;
	private SegmentoService segmentoService;
	private ContacrmhistoricoService contacrmhistoricoService;
	private CampanhaService campanhaService;
	private EnvioemailService envioemailService;

	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	public void setSegmentoService(SegmentoService segmentoService) {
		this.segmentoService = segmentoService;
	}
	public void setMaillingcontacrmDAO(MaillingcontacrmDAO maillingcontacrmDAO) {
		this.maillingcontacrmDAO = maillingcontacrmDAO;
	}
	public void setContacrmhistoricoService(ContacrmhistoricoService contacrmhistoricoService) {
		this.contacrmhistoricoService = contacrmhistoricoService;
	}
	public void setCampanhaService(CampanhaService campanhaService) {
		this.campanhaService = campanhaService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Contacrm form) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Contacrm bean) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Contacrm bean) throws Exception {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, MaillingcontacrmFiltro filtro) {
		if(filtro.getEmail() == null || filtro.getEmail().equals("")){
			filtro.setEmail(EmpresaService.getInstance().loadPrincipal().getEmail());
		}
		return new ModelAndView("crud/maillingcontacrmListagem");
	}
	
	@Override
	protected ListagemResult<Contacrm> getLista(WebRequestContext request, MaillingcontacrmFiltro filtro) {
		
		ListagemResult<Contacrm> listagemResult = super.getLista(request, filtro);
		List<Contacrm> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcontacrm", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(maillingcontacrmDAO.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}
		
		for (Contacrm conta : list) {
			if(conta.getListcontacrmcontato()!= null){
				String email = "";
				for(Contacrmcontato contato : conta.getListcontacrmcontato()){
					if(contato.getListcontacrmcontatoemail() != null){
						email =  email + CollectionsUtil.listAndConcatenate(contato.getListcontacrmcontatoemail(), "email", "<br>") + "<br>";
					}
				}
				conta.setListaEmails(email);
			}
		}
		
		return listagemResult;
	}
	
	@Override
	protected void listagem(WebRequestContext request, MaillingcontacrmFiltro filtro) throws Exception {
		
		List<Segmento> listaSegmento = segmentoService.findForCombo();
		
		request.setAttribute("listaSegmento", listaSegmento);
		super.listagem(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, MaillingcontacrmFiltro filtro) throws CrudException {
		
		String whereInCampanha = request.getParameter("selectedItens");
		
		if(whereInCampanha != null && !whereInCampanha.equals("")){
			filtro.setNotFirstTime(true);
			Campanha campanha = campanhaService.carregaCampanha(whereInCampanha);
			filtro.setCampanha(campanha);
			request.setAttribute("selectedItens", whereInCampanha);
			
		}
		return super.doListagem(request, filtro);
	}
	
	
	@Action("enviarMailling")
	public ModelAndView enviarMailling(WebRequestContext request, MaillingcontacrmFiltro filtro){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Contacrm> listaContacrm = contacrmService.findContacrmcontato(whereIn);
		List<Contacrmhistorico> listcontacrmhistorico = new ArrayList<Contacrmhistorico>();
		
		EmailManager email;
		boolean enviaMsg = false;
		
		Envioemail envioemail = envioemailService.registrarEnvio(filtro.getEmail(), filtro.getAssunto(), 
				filtro.getMensagem(), Envioemailtipo.MAILING_CONTA_CRM);
		
		for (Contacrm contacrm : listaContacrm) {
			
			if(contacrm.getListcontacrmcontato() != null){
				for(Contacrmcontato contato : contacrm.getListcontacrmcontato()){
					if(contato.getListcontacrmcontatoemail() != null){
						for (Contacrmcontatoemail contatoemail : contato.getListcontacrmcontatoemail()) {
							try {
								email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
												.setFrom(filtro.getEmail())
												.setTo(contatoemail.getEmail())
												.setSubject(filtro.getAssunto());
								
								//anexar arquivo
								if(filtro.getAnexo() != null && filtro.getAnexo().getContent() != null && filtro.getAnexo().getName() != null && filtro.getAnexo().getContenttype() != null ){
									try {
										email.attachFileUsingByteArray(filtro.getAnexo().getContent(), filtro.getAnexo().getName(), filtro.getAnexo().getContenttype(), "");
									} catch (Exception e) {
										throw new SinedException("Erro ao anexar arquivo para "+contatoemail.getEmail());
									}									
								}
								
								Envioemailitem envioemailitem = new Envioemailitem();
								envioemailitem.setEnvioemail(envioemail);
								envioemailitem.setPessoa(contacrm.getResponsavel());
								envioemailitem.setEmail(contatoemail.getEmail());
								envioemailitem.setNomecontato(contato.getNome());
								
								EnvioemailitemService.getInstance().saveOrUpdate(envioemailitem);
								
								email.setEmailItemId(envioemailitem.getCdenvioemailitem());
								
								email.addHtmlText(filtro.getMensagem() + EmailUtil.getHtmlConfirmacaoEmail(envioemail, contatoemail.getEmail()))
										.sendMessage();
								enviaMsg = true;

								Contacrmhistorico contacrmhistorico = new Contacrmhistorico();
								
								contacrmhistorico.setContacrm(contacrm);
								contacrmhistorico.setObservacao("Envio de e-mail " + filtro.getAssunto());
								contacrmhistorico.setDtaltera(new Timestamp (Calendar.getInstance().getTimeInMillis()));
								contacrmhistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
								
								listcontacrmhistorico.add(contacrmhistorico);
								
							} catch (Exception e) {
								e.printStackTrace();
								request.addError("N�o foi poss�vel enviar o mailling para " + contatoemail.getEmail());
							}
						}
					}
				}
				
			} else {
				request.addError("N�o foram enviados o mailling para o contato " + contacrm.getNome() + " pois n�o possui e-mail cadastrado.");
			}
			
		}
		if(enviaMsg){
			request.addMessage("Mailling enviado com sucesso!");
		}
		
		if(listcontacrmhistorico != null){
			for (Contacrmhistorico contacrmhistorico : listcontacrmhistorico) {
				contacrmhistoricoService.saveOrUpdate(contacrmhistorico);
			}
		}
		
		if(filtro.getEnviaCopia() != null && filtro.getEnviaCopia()){
			try {
				if(!SinedUtil.getUsuarioLogado().getEmail().equals(filtro.getEmail())){
					email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
								.setFrom(filtro.getEmail())
								.setTo(SinedUtil.getUsuarioLogado().getEmail())
								.setSubject(filtro.getAssunto());
					
					//anexar arquivo
					if(filtro.getAnexo() != null && filtro.getAnexo().getContent() != null && filtro.getAnexo().getName() != null && filtro.getAnexo().getContenttype() != null ){
						try {
							email.attachFileUsingByteArray(filtro.getAnexo().getContent(), filtro.getAnexo().getName(), filtro.getAnexo().getContenttype(), "");
						} catch (Exception e) {
							throw new SinedException("Erro ao anexar arquivo para " + SinedUtil.getUsuarioLogado().getNome());
						}									
					}				
					email
						.setBcc(SinedUtil.getUsuarioLogado().getEmail())
						.addHtmlText(filtro.getMensagem())
						.sendMessage();
				}
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("N�o foi poss�vel enviar o mailling para " + SinedUtil.getUsuarioLogado().getNome());
			}
			
		}
		
		return continueOnAction("listagem", filtro);
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
}
