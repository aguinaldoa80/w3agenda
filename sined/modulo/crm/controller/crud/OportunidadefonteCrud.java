package br.com.linkcom.sined.modulo.crm.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Oportunidadefonte;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadefonteFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean		  
@Controller(path="/crm/crud/Oportunidadefonte", authorizationModule=CrudAuthorizationModule.class)
public class OportunidadefonteCrud extends CrudControllerSined<OportunidadefonteFiltro, Oportunidadefonte, Oportunidadefonte> {

}
