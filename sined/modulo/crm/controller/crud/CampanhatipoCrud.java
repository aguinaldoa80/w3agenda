package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Campanhatipo;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.CampanhatipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(
		path="/crm/crud/Campanhatipo",
		authorizationModule=CrudAuthorizationModule.class
)
public class CampanhatipoCrud extends CrudControllerSined<CampanhatipoFiltro, Campanhatipo, Campanhatipo>{
	
	@Override
	protected void salvar(WebRequestContext request, Campanhatipo bean) throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_campanhatipo_nome")) 
				throw new SinedException("Tipo de Campanha j� cadastrado no sistema.");	
		}
	}
	
}
