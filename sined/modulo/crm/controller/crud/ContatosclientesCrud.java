package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;

@Controller(path="/crm/crud/Contatosclientes", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "categorias"})
public class ContatosclientesCrud extends CrudController<ClienteFiltro, Cliente, Cliente>{
	
	private ClienteService clienteService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request,ClienteFiltro filtro) {
		return new ModelAndView("crud/contatosclientesListagem");
	}
	
	@Override
	protected ListagemResult<Cliente> getLista(WebRequestContext request,ClienteFiltro filtro) {
		ListagemResult<Cliente> listagemResult = super.getLista(request, filtro);
		List<Cliente> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdpessoa", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(clienteService.findListagem(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}		
		
		return listagemResult;
	}

}
