package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Documentodigital;
import br.com.linkcom.sined.geral.bean.Documentodigitaldestinatario;
import br.com.linkcom.sined.geral.bean.Documentodigitalobservador;
import br.com.linkcom.sined.geral.bean.Documentodigitalusuario;
import br.com.linkcom.sined.geral.bean.enumeration.Documentodigitalsituacao;
import br.com.linkcom.sined.geral.service.DocumentodigitalService;
import br.com.linkcom.sined.geral.service.DocumentodigitaldestinatarioService;
import br.com.linkcom.sined.geral.service.DocumentodigitalusuarioService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.DocumentodigitalFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/crm/crud/Documentodigital", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cddocumentodigital", "empresa", "documentodigitalmodelo", "arquivo", "tipo", "descricaotipo", "situacao"})
public class DocumentodigitalCrud extends CrudControllerSined<DocumentodigitalFiltro, Documentodigital, Documentodigital>{
	
	private DocumentodigitalService documentodigitalService;
	private DocumentodigitalusuarioService documentodigitalusuarioService;
	private DocumentodigitaldestinatarioService documentodigitaldestinatarioService;
	
	public void setDocumentodigitaldestinatarioService(
			DocumentodigitaldestinatarioService documentodigitaldestinatarioService) {
		this.documentodigitaldestinatarioService = documentodigitaldestinatarioService;
	}
	public void setDocumentodigitalusuarioService(
			DocumentodigitalusuarioService documentodigitalusuarioService) {
		this.documentodigitalusuarioService = documentodigitalusuarioService;
	}
	public void setDocumentodigitalService(
			DocumentodigitalService documentodigitalService) {
		this.documentodigitalService = documentodigitalService;
	}
	
	@Override
	protected Documentodigital criar(WebRequestContext request, Documentodigital form) throws Exception {
		Documentodigital bean = super.criar(request, form);
		
		Documentodigitalobservador documentodigitalobservador = new Documentodigitalobservador();
		documentodigitalobservador.setEmail(SinedUtil.getUsuarioLogado().getEmail());
		Set<Documentodigitalobservador> listaDocumentodigitalobservador = new ListSet<Documentodigitalobservador>(Documentodigitalobservador.class);
		listaDocumentodigitalobservador.add(documentodigitalobservador);
		bean.setListaDocumentodigitalobservador(listaDocumentodigitalobservador);
		
		return bean;
	}
	
	@Override
	protected void listagem(WebRequestContext request, DocumentodigitalFiltro filtro) throws Exception {
		List<Documentodigitalsituacao> listaSituacao = new ArrayList<Documentodigitalsituacao>();
		for (int i = 0; i < Documentodigitalsituacao.values().length; i++) {
			listaSituacao.add(Documentodigitalsituacao.values()[i]);
		}
		request.setAttribute("listaSituacao", listaSituacao);
	}
	
	/**
	 * Action em ajax para adicionar um destinat�rio na tela de entrada.
	 * Caso n�o tenha usu�rio criado com o e-mail informado ele cria automaticamente.
	 *
	 * @param request
	 * @param documentodigital
	 * @return
	 * @since 08/03/2019
	 * @author Rodrigo Freitas
	 */
	public ModelAndView adicionarDestinatario(WebRequestContext request, Documentodigital documentodigital){
		String email = documentodigital.getEmail();
		if(StringUtils.isBlank(email)){
			throw new SinedException("E-mail � obrigat�rio.");
		}
		
		Documentodigitalusuario documentodigitalusuario = documentodigitalusuarioService.loadByEmail(email);
		if(documentodigitalusuario == null){
			documentodigitalusuario = new Documentodigitalusuario();
			documentodigitalusuario.setEmail(email);
			documentodigitalusuarioService.saveOrUpdate(documentodigitalusuario);
		}
		
		return new JsonModelAndView().addObject("documentodigitalusuario", documentodigitalusuario);
	}
	
	/**
	 * Action para reenviar e-mail de aceite para determinado destinat�rio.
	 *
	 * @param request
	 * @param documentodigitaldestinatario
	 * @return
	 * @since 26/04/2019
	 * @author Rodrigo Freitas
	 */
	public ModelAndView reenviarAceiteDigital(WebRequestContext request, Documentodigitaldestinatario documentodigitaldestinatario){
		if(documentodigitaldestinatario == null || documentodigitaldestinatario.getCddocumentodigitaldestinatario() == null){
			throw new SinedException("Destinat�rio � obrigat�rio.");
		}
		
		documentodigitaldestinatario = documentodigitaldestinatarioService.loadForReenvio(documentodigitaldestinatario);
		if(documentodigitaldestinatario == null || documentodigitaldestinatario.getCddocumentodigitaldestinatario() == null){
			throw new SinedException("Destinat�rio n�o encontrado.");
		}
		
		try {
			documentodigitalService.reenviarAceiteDigital(documentodigitaldestinatario);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao reenviar o documento digital: " + e.getMessage());
		}
		
		return sendRedirectToAction("consultar", "cddocumentodigital=" + SinedUtil.getItensSelecionados(request));
	}
	
	/**
	 * Action que faz o envio de e-mail do documento digital
	 *
	 * @param request
	 * @return
	 * @since 08/03/2019
	 * @author Rodrigo Freitas
	 */
	public ModelAndView enviarAceiteDigital(WebRequestContext request){
		ModelAndView returnModelAndView = sendRedirectToAction("listagem");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado.");
			return returnModelAndView; 
		}
		
		String entradaParam = request.getParameter("entrada");
		if(entradaParam != null && entradaParam.equalsIgnoreCase("true")){
			returnModelAndView = sendRedirectToAction("consultar", "cddocumentodigital=" + whereIn);
		}
		
		List<Documentodigital> listaDocumentodigital = documentodigitalService.findForEnvioAceiteDigital(whereIn, null);
		for (Documentodigital documentodigital : listaDocumentodigital) {
			if(!documentodigital.getSituacao().equals(Documentodigitalsituacao.EM_ESPERA)){
				request.addError("O aceite digital s� pode ser enviado para documento com a situa��o 'EM ESPERA'.");
				return returnModelAndView; 
			}
		}
		
		try {
			documentodigitalService.enviarAceiteDigital(listaDocumentodigital, false);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao enviar o(s) documento(s) digital(is): " + e.getMessage());
		}
		
		return returnModelAndView;
	}
	
}
