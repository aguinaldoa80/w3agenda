package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.sql.Date;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.geral.bean.Interacaoitem;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.ProspeccaoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ProspeccaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/crm/crud/Prospeccao",
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = {"data", "cliente", "descricao", "contato", "colaborador", "propostas"})
public class ProspeccaoCrud extends CrudControllerSined<ProspeccaoFiltro, Prospeccao, Prospeccao>{
	
	private ProspeccaoService prospeccaoService;
	private PropostaService propostaService;
	private ClienteService clienteService;
	private ColaboradorService colaboradorService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setPropostaService(PropostaService propostaService) {
		this.propostaService = propostaService;
	}
	public void setProspeccaoService(ProspeccaoService prospeccaoService) {
		this.prospeccaoService = prospeccaoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Prospeccao form) throws Exception {
		if (form.getCdprospeccao() != null) {
//			FEITO ISSO POIS TEM UM PROBLEMA DE QUANDO CARREGAR DUAS LISTAS H� UMA DUPLICA��O DE REGISTROS
			form.setListaProposta(propostaService.findByProspeccao(form));
			
			if(form.getColaborador() != null && form.getColaborador().getCdpessoa() != null){
				form.setColaborador(colaboradorService.load(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
			}
			
			if(form.getCliente() != null && form.getCliente().getCdpessoa() != null)
				form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa,cliente.nome"));
		}
		
		
		request.setAttribute("listaContatoByCliente", form.getCliente()!=null ? prospeccaoService.retornarListaContato(form.getCliente()) : null);
		
		request.getSession().setAttribute("interacao", form.getInteracao());
	}
	
	@Override
	protected void listagem(WebRequestContext request, ProspeccaoFiltro filtro) throws Exception {
		super.listagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Prospeccao bean)throws Exception {
		
		bean.setInteracao((Interacao)request.getSession().getAttribute("interacao"));
		
		if (bean != null && bean.getInteracao() != null && bean.getInteracao().getListaInteracaoitem() != null) {
			List<Interacaoitem> lista = bean.getInteracao().getListaInteracaoitem();
			for (Interacaoitem interacaoitem : lista) {
				if (interacaoitem.getArquivo() != null && interacaoitem.getArquivo().getCdarquivo() == null) {
					interacaoitem.setArquivo(null);
				}
			}
		}
		
		super.salvar(request, bean);
	}
	
	@Override
	protected void validateBean(Prospeccao bean, BindException errors) {		
		if (SinedDateUtils.javaSqlDateToCalendar(new Date(System.currentTimeMillis())).getTimeInMillis() < bean.getData().getTime()) {
			errors.reject("001","Data da prospec��o inv�lida.");
		}
	}
	
	@Override
	protected ListagemResult<Prospeccao> getLista(WebRequestContext request, ProspeccaoFiltro filtro) {
		
		ListagemResult<Prospeccao> lista = super.getLista(request, filtro);
		List<Prospeccao> list = lista.list();

		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdprospeccao", ",");
		list.removeAll(list);
		list.addAll(prospeccaoService.loadWithListaProposta(whereIn));
		
		return lista;
	}

	public ModelAndView comboBoxContato(WebRequestContext request, Cliente cliente) {
		return prospeccaoService.comboBoxContato(request, cliente);
	}
}
