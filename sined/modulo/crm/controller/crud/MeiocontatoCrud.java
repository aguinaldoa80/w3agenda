	package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.geral.service.MeiocontatoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MeiocontatoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Meiocontato"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "padrao", "ativo"})
public class MeiocontatoCrud extends CrudControllerSined<MeiocontatoFiltro, Meiocontato, Meiocontato>{
	
	protected MeiocontatoService meiocontatoService;
	
	public void setMeiocontatoService(MeiocontatoService meiocontatoService) {this.meiocontatoService = meiocontatoService;}
	
	@Override
	protected void entrada(WebRequestContext request, Meiocontato form)throws Exception {
		if(form.getCdmeiocontato() == null){
			form.setAtivo(Boolean.TRUE);
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Meiocontato bean)throws Exception {
		try {
			bean.setNome(bean.getNome().trim());
			
			if(bean.getCdmeiocontato() == null){
				if(meiocontatoService.existeMeiocontatoPadrao() && bean.getPadrao() != null && Boolean.TRUE.equals(bean.getPadrao())){
					throw new SinedException("J� existe um meio de contato marcado como padr�o.");
				}
			}else{
				if(!meiocontatoService.isMeiocontatoPadrao(bean.getCdmeiocontato()) && meiocontatoService.existeMeiocontatoPadrao() && bean.getPadrao() != null && Boolean.TRUE.equals(bean.getPadrao())){
					throw new SinedException("J� existe um meio de contato marcado como padr�o.");
				}
			}
			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_MEIOCONTATO_NOME")) {
				throw new SinedException("Meio de contato j� cadastrado no sistema.");
			}
		}
	}
}
