package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Campanhalead;
import br.com.linkcom.sined.geral.bean.CampoExtraLead;
import br.com.linkcom.sined.geral.bean.CampoExtraSegmento;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leademail;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Leadqualificacao;
import br.com.linkcom.sined.geral.bean.Leadsegmento;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.geral.bean.Leadtelefone;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.service.CampanhaleadService;
import br.com.linkcom.sined.geral.service.CampoExtraSegmentoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ConcorrenteService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.LeademailService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.LeadqualificacaoService;
import br.com.linkcom.sined.geral.service.LeadsegmentoService;
import br.com.linkcom.sined.geral.service.LeadsituacaoService;
import br.com.linkcom.sined.geral.service.LeadtelefoneService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OportunidadesituacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ContacrmFiltro;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.LeadFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/crm/crud/Lead",
		authorizationModule=CrudAuthorizationModule.class
)
public class LeadCrud extends CrudControllerSined<LeadFiltro, Lead, Lead>{
	
	protected LeadService leadService;
	protected LeadhistoricoService leadhistoricoService;
	protected LeadtelefoneService leadtelefoneService;
	protected LeademailService leademailService;
	protected LeadsegmentoService leadsegmentoService;
	protected ColaboradorService colaboradorService;
	protected OportunidadesituacaoService oportunidadesituacaoService;
	protected LeadsituacaoService leadsituacaoService;
	protected LeadqualificacaoService leadqualificacaoService;
	protected ParametrogeralService parametrogeralService;
	protected CampanhaleadService campanhaleadService;
	protected OportunidadeService oportunidadeService;
	private CampoExtraSegmentoService campoExtraSegmentoService;
	private ConcorrenteService concorrenteService;
	
	public void setCampanhaleadService(CampanhaleadService campanhaleadService) {
		this.campanhaleadService = campanhaleadService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setLeadhistoricoService(
			LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setLeadtelefoneService(LeadtelefoneService leadtelefoneService) {
		this.leadtelefoneService = leadtelefoneService;
	}
	public void setLeademailService(LeademailService leademailService) {
		this.leademailService = leademailService;
	}
	public void setLeadsegmentoService(LeadsegmentoService leadsegmentoService) {
		this.leadsegmentoService = leadsegmentoService;
	}
	public void setOportunidadesituacaoService(
			OportunidadesituacaoService oportunidadesituacaoService) {
		this.oportunidadesituacaoService = oportunidadesituacaoService;
	}
	public void setLeadsituacaoService(LeadsituacaoService leadsituacaoService) {
		this.leadsituacaoService = leadsituacaoService;
	}
	public void setLeadqualificacaoService(
			LeadqualificacaoService leadqualificacaoService) {
		this.leadqualificacaoService = leadqualificacaoService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setCampoExtraSegmentoService(CampoExtraSegmentoService campoExtraSegmentoService) {
		this.campoExtraSegmentoService = campoExtraSegmentoService;
	}
	public void setConcorrenteService(ConcorrenteService concorrenteService) {
		this.concorrenteService = concorrenteService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Lead bean)	throws Exception {
		try {
			for (CampoExtraLead l : bean.getCampoExtraLeadList()) {
				if (l.getValor() == null || l.getValor().equals("")) {
					l.setValor("");
				}
			}
			
			super.salvar(request, bean);
			if((bean != null && bean.getObservacao() != null && !bean.getObservacao().trim().equals("")) || bean.getAtividadetipotrans() != null){
				Leadhistorico leadhistorico = new Leadhistorico();
				
				leadhistorico.setLead(bean);
				leadhistorico.setObservacao(bean.getObservacao());
				leadhistorico.setAtividadetipo(bean.getAtividadetipotrans());
				leadhistorico.setDtaltera(bean.getDtaltera());
				leadhistorico.setCdusuarioaltera(bean.getCdusuarioaltera());
				
				leadhistoricoService.saveOrUpdate(leadhistorico);
			}
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "lead_idx")){
				throw new SinedException("Empresa j� cadastrada no sistema.");
			}
		}
	}
	
	@Override
	protected ListagemResult<Lead> getLista(WebRequestContext request, LeadFiltro filtro) {
		//Filtrar somente Lead do usu�rio logado
		if (!filtro.isNotFirstTime()) {
			Colaborador colaboradorLogado = SinedUtil.getUsuarioComoColaborador();
			if(colaboradorLogado!=null){
				if(ColaboradorService.getInstance().isColaborador(colaboradorLogado.getCdpessoa())){
					filtro.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
					filtro.setResponsavel(colaboradorLogado);
				}else{
					filtro.setTiporesponsavel(Tiporesponsavel.AGENCIA);
					filtro.setAgencia(new Fornecedor(colaboradorLogado.getCdpessoa()));
				}
			}
		}
		ListagemResult<Lead> listresult = super.getLista(request, filtro); 
		List<Lead> list = listresult.list();

		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdlead", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(leadService.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}
		
		leadService.verificaLeadsConvertidos(list);
		
		boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
		for(Lead lead : list){
			leadService.addInfListagem(lead, filtro, false);
			lead.setPermitidoEdicao(!isRestricaoClienteVendedor ||
										SinedUtil.getUsuarioLogado().equals(lead.getResponsavel()));
		}
		
		if(whereIn != null && !"".equals(whereIn)){
			filtro.setListLeadhistoricoTotais(leadService.getTotaisAtividadetipoByLead(whereIn));
		}else {
			filtro.setListLeadhistoricoTotais(null);
		}
		
		return listresult;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	protected void entrada(WebRequestContext request, Lead form) throws Exception {
		
		if(form.getListLeadtelefone() != null && form.getCdlead() != null){
			form.setListLeadtelefone(SinedUtil.listToSet(leadtelefoneService.findByLead(form), Leadtelefone.class));
		}
		if(form.getListleademail() != null && form.getCdlead() != null){
			form.setListleademail(SinedUtil.listToSet(leademailService.findByLead(form), Leademail.class));
		}
		if(form.getListLeadsegmento() != null && form.getCdlead() != null){
			form.setListLeadsegmento(leadsegmentoService.findByLead(form));
		}
		if(form.getListLeadhistorico() != null){
			if(form.getCdlead() != null){
				//feito isto porque estava duplicando os registro do hist�rico na visualiza��o
				form.setListLeadhistorico(leadhistoricoService.findByLead(form));
			}
			for(Leadhistorico historico : form.getListLeadhistorico()){
				if(historico.getObservacao() != null)
					historico.setObservacao(historico.getObservacao().replaceAll("\n", "<br>"));
			
			}
		}
		if(form.getCdlead() != null){
			boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
			form.setPermitidoEdicao(!isRestricaoClienteVendedor ||
											SinedUtil.getUsuarioLogado().equals(form.getResponsavel()));
		}
		
		List<Leadsegmento> leadSegmentoListFound = form.getListLeadsegmento();
		
		if (leadSegmentoListFound != null && leadSegmentoListFound.size() > 0) {
			form.setCampoExtraLeadList(leadService.carregaCamposExtraLead(leadSegmentoListFound));
		} else {
			form.setCampoExtraLeadList(new ListSet<CampoExtraLead>(CampoExtraLead.class));
		}
		
		//verifica parametro no sistema para tornar obrigatorio campo observacao
		request.setAttribute("OBRIGAR_OBS_LEAD", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_OBS_LEAD));
		
		super.entrada(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, LeadFiltro filtro) throws Exception {
		request.setAttribute("listaLeadsituacaoTodas", leadsituacaoService.findAll());
		request.setAttribute("TIPO_RESPONSAVEL_COLABORADOR", Tiporesponsavel.COLABORADOR);
	}
	
	public ModelAndView editarHistorico(WebRequestContext request, Leadhistorico leadhistorico){
		//Verificando a obrigatoriedade da observa��o
		request.setAttribute("OBRIGAR_OBS_LEAD", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_OBS_LEAD));
		leadhistorico = leadhistoricoService.loadForEntrada(leadhistorico);
		return new ModelAndView("direct:/crud/popup/editarHistoricoLead", "bean", leadhistorico);
	}
	
	public void saveHistorico(WebRequestContext request, Leadhistorico leadhistorico){
		leadhistoricoService.saveOrUpdateWithoutLog(leadhistorico);
		request.addMessage("Hist�rico editado com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	@Action("abrirnomeoportunidade")
	public ModelAndView abrirNomeOportunidade(WebRequestContext request){
		Oportunidade oportunidade = new Oportunidade();
		
		Lead lead = leadService.findLeadForOportunidade(SinedUtil.getItensSelecionados(request));
		
		if (lead != null) {
			if (lead.getTiporesponsavel() != null && lead.getTiporesponsavel().equals(Tiporesponsavel.COLABORADOR) && lead.getResponsavel() != null) {
				oportunidade.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
				oportunidade.setResponsavel(lead.getResponsavel());
			} else if (lead.getTiporesponsavel() != null && lead.getTiporesponsavel().equals(Tiporesponsavel.AGENCIA) && lead.getAgencia() != null){
				oportunidade.setTiporesponsavel(Tiporesponsavel.AGENCIA);
				oportunidade.setAgencia(lead.getAgencia());
			}
			
			if(parametrogeralService.getBoolean(Parametrogeral.LEAD_NOME_OPORTUNIDADE) && StringUtils.isNotBlank(lead.getEmpresa())){
				oportunidade.setNome(lead.getEmpresa() + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
			}
			
			if(lead.getListLeadcampanha() != null && lead.getListLeadcampanha().size() == 1){
				for(Campanhalead campanhaLead: lead.getListLeadcampanha()){
					oportunidade.setCampanha(campanhaLead.getCampanha());
				}
			}
			
			if (lead.getConcorrente() != null) {
				oportunidade.setConcorrente(lead.getConcorrente());
				oportunidade.setSite(lead.getSite());
				oportunidade.setEstrategiaMarketing(lead.getEstrategiaMarketing());
				oportunidade.setForcas(lead.getForcas());
				oportunidade.setFraquezas(lead.getFraquezas());
			}
		} 
		
		oportunidade.setWhereIn(SinedUtil.getItensSelecionados(request));
		oportunidade.setDtInicio(new Date(System.currentTimeMillis()));
		
		Oportunidadesituacao situacaoinicial = oportunidadesituacaoService.findSituacaoinicial();
		if(situacaoinicial != null){
			oportunidade.setOportunidadesituacao(situacaoinicial);	
		}else{
			request.addError("N�o existe nenhuma 'Situa��o de oportunidade' cadastrada como inicial.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
				
		
		request.setAttribute("titulo", "OPORTUNIDADE");
		request.setAttribute("action", "converterlead");
		request.setAttribute("labelCampo", "Nome da oportunidade");
		
		return new ModelAndView("direct:/crud/popup/nomeOportunidade", "bean", oportunidade);
	}

	public void getComboSituacao(WebRequestContext request, Lead lead){
		lead = leadService.load(lead);
		Integer situacao = lead.getLeadsituacao() != null ? lead.getLeadsituacao().getCdleadsituacao() : null;
		View.getCurrent().println(getOptionComboSituacao(situacao));
	}
	
	public String getOptionComboSituacao(Integer cdpendenciastatus) {
		StringBuilder sb = new StringBuilder();
		List<Leadsituacao> lista = leadsituacaoService.findAll();
		
		for (Leadsituacao status : lista) {
			sb.append("<option ");
			
			if(cdpendenciastatus != null && cdpendenciastatus.equals(status.getCdleadsituacao())){
				sb.append("selected");
			}
			
			sb.append(" value=\"br.com.linkcom.sined.geral.bean.Leadsituacao[cdleadsituacao=")
				.append(status.getCdleadsituacao())
				.append("]\">")
				.append(status.getNome())
				.append("</option>");
		}
		
		return sb.toString();
	}
	
	public void saveSituacao(WebRequestContext context, Lead lead){
		leadService.updateLeadSituacao(lead);
	}
	
	public void getComboQualificacao(WebRequestContext request, Lead lead){
		lead = leadService.load(lead);
		Integer qualificacao = lead.getLeadqualificacao() != null ? lead.getLeadqualificacao().getCdleadqualificacao() : null;
		View.getCurrent().println(getOptionComboQualificacao(qualificacao));
	}
	
	public String getOptionComboQualificacao(Integer cdpendenciastatus) {
		StringBuilder sb = new StringBuilder();
		List<Leadqualificacao> lista = leadqualificacaoService.findAll();
		
		for (Leadqualificacao status : lista) {
			sb.append("<option ");
			
			if(cdpendenciastatus != null && cdpendenciastatus.equals(status.getCdleadqualificacao())){
				sb.append("selected");
			}
			
			sb.append(" value=\"br.com.linkcom.sined.geral.bean.Leadqualificacao[cdleadqualificacao=")
				.append(status.getCdleadqualificacao())
				.append("]\">")
				.append(status.getNome())
				.append("</option>");
		}
		
		return sb.toString();
	}
	
	public void saveQualificacao(WebRequestContext context, Lead lead){
		leadService.updateLeadQualificacao(lead);
	}

	public void saveDtRetorno(WebRequestContext context, Lead lead){
		leadService.updateLeadDtRetorno(lead);
	}
	
	public void saveProximoPasso(WebRequestContext context, Lead lead){
		leadService.updateLeadProximoPasso(lead);
	}
	
	public ModelAndView verificarParametroLead(WebRequestContext request){
		JsonModelAndView json = new JsonModelAndView();
		String retorno = "false";
		String valorParametroLead = parametrogeralService.getValorPorNome(Parametrogeral.LEAD_PARA_CLIENTE);
		Boolean sugerirCamposLeadCliente = parametrogeralService.getBoolean(Parametrogeral.SUGERIR_CAMPOS_LEAD_CLIENTE);
		if (valorParametroLead.equals("TRUE")){
			request.getSession().setAttribute("cdLead", request.getParameter("cdLead"));
			request.getSession().setAttribute("sugerirCamposLeadCliente", sugerirCamposLeadCliente);
			retorno = "true";
		}
		json.addObject("retorno", retorno);
		return json;
	}
	
	/**
	 * Action para desassociar os leads da campanha selecionada no filtro.
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView desassociarCampanha(WebRequestContext request, ContacrmFiltro filtro){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(!leadService.usuarioPodeEditarLead(whereIn)){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela lead.");
		}else if(filtro != null && filtro.getCampanha() != null){
			
			StringBuilder leadsInvalidos = new StringBuilder();
			StringBuilder leadsAlterados = new StringBuilder();
			try {
				List<Lead> lista = leadService.findAllForDesassociarCampanha(whereIn);
				for(Lead lead : lista){
					if(lead.getListLeadcampanha() == null || lead.getListLeadcampanha().isEmpty()){
						leadsInvalidos.append("<a href=\"javascript:visualizarLead("+lead.getCdlead()+");\">"+lead.getCdlead()+",</a> ");
					}
					else{
						Boolean hasCampanha = Boolean.FALSE;
						for(Campanhalead cl : lead.getListLeadcampanha()){
							if(cl.getCampanha() != null && cl.getCampanha().getCdcampanha().equals(filtro.getCampanha().getCdcampanha())){
								campanhaleadService.delete(cl);
								hasCampanha = Boolean.TRUE;
							}
						}
						if(hasCampanha){
							leadsAlterados.append("<a href=\"javascript:visualizarLead("+lead.getCdlead()+");\">"+lead.getCdlead()+",</a> ");
						}
						else{
							leadsInvalidos.append("<a href=\"javascript:visualizarLead("+lead.getCdlead()+");\">"+lead.getCdlead()+",</a> ");
						}
					}
				}
				if(leadsAlterados.length() > 0){
					leadsAlterados.replace(leadsAlterados.lastIndexOf(","), leadsAlterados.length()-1, ".");
					request.addMessage("Lead's desassociados com sucesso: "+leadsAlterados.toString());
				}
				if(leadsInvalidos.length() > 0){
					leadsInvalidos.replace(leadsInvalidos.lastIndexOf(","), leadsInvalidos.length()-1, ".");
					request.addError("Os seguintes lead's n�o possuem v�nculo com a campanha selecionada: " + leadsInvalidos.toString());
				}
			} catch (Exception e) {
//				System.out.println("Erro ao desassociar a campanha dos Lead's selecionados.");
				e.printStackTrace();
			}
			
		} else{
			request.addError("N�o foi poss�vel encontrar a campanha.");
		}
		
		return sendRedirectToAction("listagem");
	}

	public ModelAndView buscarEmpresa(WebRequestContext request){
		return new ModelAndView("direct:/crud/popup/buscarEmpresaLead", "listaLead", leadService.findForBuscarEmpresa(request.getParameter("empresa")));
	}
	
	public ModelAndView gerarCSV(WebRequestContext request, LeadFiltro filtro){
		
		filtro.setPageSize(Integer.MAX_VALUE);
		List<Lead> listaLead = leadService.findForCSV(filtro);
		
		if(listaLead != null && !listaLead.isEmpty()){
			for(Lead lead : listaLead){
				leadService.addInfListagem(lead, filtro, true);			
			}
		}
		
		StringBuilder csv = new StringBuilder();
		csv.append("Nome;");
		csv.append("Empresa;");
		csv.append("Campanhas;");
		csv.append("Respons�vel;");
		csv.append("Situa��o;");
		csv.append("Qualifica��o;");
		csv.append("E-mails;");
		csv.append("Telefones;");
		csv.append("Data de Retorno;");
		csv.append("Pr�ximo Passo;");
		csv.append("Ciclo;");
		csv.append("Totais por Tipo de Atividade;");
		csv.append("�lt. Contato;");
		csv.append("Int.;");
		csv.append("Segmento;");
		
		for (Lead lead: listaLead){
			
			StringBuilder csvAux = new StringBuilder();
			csvAux.append(Util.strings.emptyIfNull(lead.getNome()) + ";");
			csvAux.append(Util.strings.emptyIfNull(lead.getEmpresa()) + ";");
			csvAux.append(Util.strings.emptyIfNull(lead.getListaCampanhas()) + ";");
			csvAux.append((lead.getResponsavel()!=null ? lead.getResponsavel().getNome() : "") + ";");
			csvAux.append((lead.getLeadsituacao()!=null ? lead.getLeadsituacao().getNome() : "") + ";");
			csvAux.append((lead.getLeadqualificacao()!=null ? lead.getLeadqualificacao().getNome() : "") + ";");
			csvAux.append(Util.strings.emptyIfNull(lead.getListaEmails()) + ";");
			csvAux.append(Util.strings.emptyIfNull(lead.getListaTelefones()) + ";");
			csvAux.append(Util.strings.emptyIfNull(lead.getDtRetornoString()) + ";");
			csvAux.append(Util.strings.emptyIfNull(lead.getProximopasso()) + ";");
			csvAux.append((lead.getCiclo()!=null ? lead.getCiclo() : "") + ";");
			csvAux.append((lead.getTotaisatividadetipo()!=null ? lead.getTotaisatividadetipo() : "") + ";");
			csvAux.append((lead.getDtultimohistorico()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(lead.getDtultimohistorico()) : "") + ";");
			csvAux.append((lead.getQtdehistorico()!=null ? lead.getQtdehistorico() : "") + ";");
			if(lead.getListLeadsegmento() != null)
				csvAux.append(CollectionsUtil.listAndConcatenate(lead.getListLeadsegmento(), "segmento.nome", " / "));
			csvAux.append(";");
			
			String csvAuxString = csvAux.toString();
			csvAuxString = csvAuxString.replace("<br>", "");
			csvAuxString = csvAuxString.replaceAll("\n", "");
			
			csv.append("\n");
			csv.append(csvAuxString);
		}		
		
		Resource resource = new Resource();
		resource.setContentType("text/csv");
		resource.setFileName("lead.csv");
		resource.setContents(csv.toString().getBytes());
		
		return new ResourceModelAndView(resource);
	}
	
	public ModelAndView incluirCampanhaPopup(WebRequestContext request, Lead lead){
		String whereIn = request.getParameter("selectedItens");
		if(!leadService.usuarioPodeEditarLead(whereIn)){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela lead.");
			SinedUtil.fechaPopUp(request);
			
			return null;
		}
		lead.setWhereIn(whereIn);
		return new ModelAndView("direct:/crud/popup/incluirCampanhaLead", "lead", lead);
	}
	
	public ModelAndView alterarResponsavelPopup(WebRequestContext request, Lead lead){
		String whereIn = request.getParameter("selectedItens");
		if(!leadService.usuarioPodeEditarLead(whereIn)){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela lead.");
			SinedUtil.fechaPopUp(request);
			
			return null;
		}
		lead.setWhereIn(whereIn);
		return new ModelAndView("direct:/crud/popup/alterarResponsavelLead", "lead", lead);
	}
	
	public ModelAndView incluirCampanha(WebRequestContext request, Lead lead){
		
		String whereIn = lead.getWhereIn();
		String[] ids = whereIn.split(",");
		Campanhalead campanhalead;
		
		for (String id: ids) {
			campanhalead = new Campanhalead();
			campanhalead.setCampanha(lead.getCampanhaPopup());
			campanhalead.setLead(new Lead(Integer.parseInt(id)));
			campanhaleadService.saveOrUpdate(campanhalead);
		}			
		
		request.addMessage("Campanha(s) inclu�da(s) com sucesso.", MessageType.INFO);
		
		SinedUtil.fechaPopUp(request);
		
		return null;
	}
	
	public ModelAndView alterarResponsavel(WebRequestContext request, Lead lead){
			
		String whereIn = lead.getWhereIn();
		String[] ids = whereIn.split(",");
		
		for (String id: ids) {
			leadService.updateResponsavelLeads(id, lead.getColaboradorPopup());
		}
		
		request.addMessage("Respons�vel(eis) alterado(s) com sucesso.", MessageType.INFO);
		
		SinedUtil.fechaPopUp(request);
		
		return null;
	}
	
	public ModelAndView getInfoMaterialUnidademedida(WebRequestContext request, Oportunidadematerial bean){
		return oportunidadeService.getInfMaterialUnidademedida(request, bean);
	}
	
	/**
	 * Met�do que carrega a popup com o mapa do endere�o do lead 
	 * @param request
	 * @param bean
	 */
	public ModelAndView abrirMapaEndereco(WebRequestContext request, Lead bean){
		bean = leadService.carregaLead(bean.getCdlead().toString());
		
		request.setAttribute("endereco", bean.getEnderecoCompleto());
		return new ModelAndView("direct:crud/popup/popUpmapaEndereco");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Lead bean)
			throws Exception {
		String itens = request.getParameter("itenstodelete");

		if(Util.strings.isNotEmpty(itens) && !leadService.usuarioPodeEditarLead(itens)){
			request.addError("Exclus�o n�o permitida. Usu�rio logado n�o � o respons�vel pela Lead.");
		}else if(bean != null && bean.getCdlead() != null && !leadService.usuarioPodeEditarLead(bean.getCdlead().toString())){
			request.addError("Exclus�o n�o permitida. Usu�rio logado n�o � o respons�vel pela Lead.");
		}else{
			super.excluir(request, bean);
		}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Lead form)
			throws CrudException {
		boolean editando = "editar".equalsIgnoreCase(request.getParameter("ACAO"));
		if(editando && form.getCdlead() != null){
			boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
			Lead bean = leadService.loadWithResponsavel(form.getCdlead());
			boolean permitidaEdicao = !isRestricaoClienteVendedor || SinedUtil.getUsuarioLogado().equals(bean.getResponsavel());
			if(!permitidaEdicao){
				throw new SinedException("Edi��o n�o permitida. Usu�rio logado n�o � o respons�vel pela Lead.");
			}
		}
		
		return super.doEditar(request, form);
	}
	
	public ModelAndView carregarCamposAdicionaisLeadAjax(WebRequestContext request, Segmento segmento) {
		JsonModelAndView json = new JsonModelAndView();
		
		List<CampoExtraSegmento> campoExtraSegmentosList = null;
		
		if (segmento.getCdsegmento() != null) {
			campoExtraSegmentosList = campoExtraSegmentoService.findBySegmento(segmento);

			if (campoExtraSegmentosList != null && campoExtraSegmentosList.size() > 0) {
				json.addObject("list", campoExtraSegmentosList);
			}
		}
		
		return json;
	}
	
	public ModelAndView carregaCamposConcorrente(WebRequestContext request, Lead lead) {
		JsonModelAndView json = new JsonModelAndView();
		
		json.addObject("concorrente", concorrenteService.carregaConcorrente(new Concorrente(lead.getCdconcorrente())));
		
		return json;
	}
}