package br.com.linkcom.sined.modulo.crm.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhacontato;
import br.com.linkcom.sined.geral.bean.Campanhahistorico;
import br.com.linkcom.sined.geral.bean.Campanhalead;
import br.com.linkcom.sined.geral.bean.Campanhasituacao;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.service.CampanhaService;
import br.com.linkcom.sined.geral.service.CampanhacontatoService;
import br.com.linkcom.sined.geral.service.CampanhahistoricoService;
import br.com.linkcom.sined.geral.service.CampanhaleadService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContacrmcontatoService;
import br.com.linkcom.sined.geral.service.ContacrmcontatoemailService;
import br.com.linkcom.sined.geral.service.ContacrmhistoricoService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.LeademailService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.OportunidadehistoricoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.CampanhaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/crm/crud/Campanha",
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = {"nome", "responsavel", "dtcriacao", "dtprevisaofechamento", "projeto", "campanhasituacao", "campanhatipo"})
public class CampanhaCrud extends CrudControllerSined<CampanhaFiltro, Campanha, Campanha>{
	
	protected CampanhaService campanhaService;
	protected CampanhahistoricoService campanhahistoricoService;
	protected CampanhaleadService campanhaleadService;
	protected CampanhacontatoService campanhacontatoService;
	protected LeadService leadService;
	protected ContacrmService contacrmService;
	protected ContacrmcontatoemailService contacrmcontatoemailService;
	protected ContacrmcontatoService contacrmcontatoService;
	protected LeademailService leademailService;
	protected OportunidadehistoricoService oportunidadehistoricoService;
	protected ContacrmhistoricoService contacrmhistoricoService;
	protected LeadhistoricoService leadhistoricoService;	
	
	public void setCampanhaService(CampanhaService campanhaService) {
		this.campanhaService = campanhaService;
	}
	public void setCampanhahistoricoService(CampanhahistoricoService campanhahistoricoService) {
		this.campanhahistoricoService = campanhahistoricoService;
	}
	public void setCampanhaleadService(CampanhaleadService campanhaleadService) {
		this.campanhaleadService = campanhaleadService;
	}
	public void setCampanhacontatoService(CampanhacontatoService campanhacontatoService) {
		this.campanhacontatoService = campanhacontatoService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	public void setContacrmcontatoemailService(ContacrmcontatoemailService contacrmcontatoemailService) {
		this.contacrmcontatoemailService = contacrmcontatoemailService;
	}
	public void setContacrmcontatoService(ContacrmcontatoService contacrmcontatoService) {
		this.contacrmcontatoService = contacrmcontatoService;
	}
	public void setLeademailService(LeademailService leademailService) {
		this.leademailService = leademailService;
	}
	public void setOportunidadehistoricoService(OportunidadehistoricoService oportunidadehistoricoService) {
		this.oportunidadehistoricoService = oportunidadehistoricoService;
	}
	public void setContacrmhistoricoService(ContacrmhistoricoService contacrmhistoricoService) {
		this.contacrmhistoricoService = contacrmhistoricoService;
	}
	public void setLeadhistoricoService(LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	
	@Override
	protected Campanha criar(WebRequestContext request, Campanha form) throws Exception {
		Campanha bean = super.criar(request, form);
		
		bean.setDtcriacao(new Date(System.currentTimeMillis()));
		bean.setCampanhasituacao(new Campanhasituacao(1));
		bean.setResponsavel(SinedUtil.getUsuarioComoColaborador());
		
		return bean;
	}
	
	@Override
	protected void listagem(WebRequestContext request, CampanhaFiltro filtro) throws Exception {
		
		List<Campanhasituacao> listaCampanhasituacao = new ArrayList<Campanhasituacao>();
		listaCampanhasituacao.add(Campanhasituacao.EM_ABERTO);
		listaCampanhasituacao.add(Campanhasituacao.FINALIZADA);
		listaCampanhasituacao.add(Campanhasituacao.CANCELADA);
		request.setAttribute("listaCampanhasituacao", listaCampanhasituacao);
		
		super.listagem(request, filtro);
	}
	
	
	
	
	@Override
	@SuppressWarnings("unchecked")
	protected void salvar(WebRequestContext request, Campanha bean) throws Exception {
		
		Object attrContatocrm = request.getSession().getAttribute("listCampanhacontato");
		if (attrContatocrm != null) {
			bean.setListCampanhacontato((List<Campanhacontato>)attrContatocrm);
		}

		Object attrOP = request.getSession().getAttribute("listCampanhalead");
		if (attrOP != null) {
			bean.setListCampanhalead((ListSet<Campanhalead>)attrOP);
		}
		
		super.salvar(request, bean);

		if(bean != null && bean.getObservacao()!=null && !bean.getObservacao().trim().equals("")){
			Campanhahistorico campanhahistorico = new Campanhahistorico();
			
			campanhahistorico.setCampanha(bean);
			campanhahistorico.setObservacao(bean.getObservacao());
			campanhahistorico.setDtaltera(bean.getDtaltera());
			campanhahistorico.setCdusuarioaltera(bean.getCdusuarioaltera());
			
			campanhahistoricoService.saveOrUpdate(campanhahistorico);
		}
	}
	
	public void ajaxMontaLead(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		String cdlead = request.getParameter("cdlead");
			if (StringUtils.isNotEmpty(cdlead)){
				Lead lead = new Lead();
				lead = leadService.carregaLead(cdlead);
				if(lead != null){
					view.println("var existe = true");
					
					if(lead.getEmpresa() != null){
						view.println("var leadEmpresa = '" + lead.getEmpresa() + "';");
					}
					if(lead.getResponsavel() != null){
						view.println("var leadResponsavel = '" + lead.getResponsavel().getNome() + "';");
					}
	//				if(lead.getListleademail() != null){
	//					lead.setListaEmails(CollectionsUtil.listAndConcatenate(lead.getListleademail(), "email", "<br>"));
	//					view.println("var leadListaEmails = '" + lead.getListaEmails() + "';");
	//				}
					if(lead.getLeadsituacao() != null){
						view.println("var leadSituacao = '" + lead.getLeadsituacao().getNome() + "';");
					}
					if(lead.getUltimoHistorico() != null){
						view.println("var leadUltimoHistorico = '" + lead.getUltimoHistorico() + "';");
					}
				}else{
					view.println("var existe = false");
				}
				
			}
	}
	
	public void ajaxMontaContato(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		String cdcontato = request.getParameter("cdcontato");
		String cdcampanha = request.getParameter("cdcampanha");
		
			if (StringUtils.isNotEmpty(cdcontato)){
				Contacrm conta = new Contacrm();
				conta = contacrmService.carregaContacrmcontatoOportunidadeSituacao(cdcontato);
				
				if(conta.getResponsavel() != null){
					view.println("var contatoResponsavel = '" + conta.getResponsavel().getNome() + "';");
				}
				
				view.println("var oportunidade = false;");
				if(conta.getListoportunidade() != null && !conta.getListoportunidade().isEmpty()){					
					for(Oportunidade op : conta.getListoportunidade()){
						if(op.getCampanha() != null && op.getCampanha().getCdcampanha() != null){
							if(cdcampanha.equals(op.getCampanha().getCdcampanha().toString())){
								if(conta.getOportunidadesituacao() != null){
									view.println("oportunidadesituacao = '" + op.getOportunidadesituacao().getNome() + "';");
									view.println("oportunidade = true;");
									break;
								}
							}
						}
					}
				}
				
				if(conta.getUltimaInteracaoVenda() != null){
					view.println("var ultimaInteracaoVenda = '" + conta.getUltimaInteracaoVenda() + "';");
				}
				
				if(conta.getListcontacrmcontato() != null){
					View.getCurrent().convertObjectToJs(conta.getListcontacrmcontato(), "listaContatos");
				}
				
			}
	}
	
//	public ModelAndView ajaxMontaListaEmail(WebRequestContext request, Contacrmcontato contacrmcontato){
//		List<Contacrmcontatoemail> listaEmail = new ArrayList<Contacrmcontatoemail>();
//		listaEmail = contacrmcontatoemailService.carregaContacrmcontatoemail(contacrmcontato.getCdcontacrmcontato().toString());
//		String email = CollectionsUtil.listAndConcatenate(listaEmail, "email", ", ");
//		
//		return new JsonModelAndView().addObject("contatoListaEmails", email);
//		
//	}
	
	/**
	* M�todo que faz o redirecionamento para a intera��o de venda 
	*
	* @param request
	* @return
	* @since Aug 2, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView visualizarInteracaoVenda(WebRequestContext request){
		String cdContacrm = request.getParameter("cdContacrm");
		String cdCampanha = request.getParameter("cdCampanha");
		if(cdContacrm != null){			
			Contacrm contacrm = contacrmService.loadForOportunidade(Integer.parseInt(cdContacrm));
			
			if(contacrm != null && contacrm.getListoportunidade() != null && !contacrm.getListoportunidade().isEmpty()){
				for(Oportunidade oportunidade : contacrm.getListoportunidade()){
					if(oportunidade.getCampanha() != null && oportunidade.getCampanha().getCdcampanha() != null &&
							oportunidade.getCampanha().getCdcampanha().toString().equals(cdCampanha)){
						return new ModelAndView("redirect:/crm/process/PainelInteracaoVenda?ACAO=interacaoOportunidade&cdoportunidade=" + oportunidade.getCdoportunidade());
								
					}
				}
				
				return new ModelAndView("redirect:/crm/process/PainelInteracaoVenda?ACAO=handle&cdContacrm=" + cdContacrm + "&nomeContacrm=" + contacrm.getNome());
			}else {
				
				return new ModelAndView("redirect:/crm/process/PainelInteracaoVenda?ACAO=handle&cdContacrm=" + cdContacrm + "&nomeContacrm=" + contacrm.getNome());
			}
				
		}
		
		request.addError("C�digo inv�lido");
		return new ModelAndView("redirect:/crm/crud/Campanha");
	}
	
}
