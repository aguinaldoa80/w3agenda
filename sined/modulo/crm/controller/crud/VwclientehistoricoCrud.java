package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.view.Vwclientehistorico;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclientehistoricoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/crm/crud/Vwclientehistorico", authorizationModule=CrudAuthorizationModule.class)
public class VwclientehistoricoCrud extends CrudControllerSined<VwclientehistoricoFiltro, Vwclientehistorico, Vwclientehistorico>{
	
	private AtividadetipoService atividadetipoService;
	private EmpresaService empresaService;
	
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Vwclientehistorico form) throws CrudException {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Vwclientehistorico bean) throws Exception {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Vwclientehistorico bean) throws Exception {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	public void exibirSituacao(WebRequestContext request, VwclientehistoricoFiltro filtro) throws Exception {
		
		boolean exibir;
		
		if (filtro.getAtividadetipo()!=null){
			exibir = Boolean.TRUE.equals(atividadetipoService.load(filtro.getAtividadetipo()).getReclamacao());
		}else {
			exibir = false;
		}
		
		View.getCurrent().print("var exibir = " + exibir + ";");		
	}
	
	@Override
	protected void entrada(WebRequestContext request, Vwclientehistorico form)
			throws Exception {
		
		request.setAttribute("listaEmpresasPorUsuario", empresaService.findByUsuario());
		super.entrada(request, form);
	}
}
 