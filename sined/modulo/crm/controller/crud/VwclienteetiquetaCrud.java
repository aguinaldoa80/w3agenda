package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.view.Vwclienteetiqueta;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclienteetiquetaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/crm/crud/Vwclienteetiqueta", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"pessoa_nome", "enderecotipo_nome", "logradouro", "municipio_nome", "uf_sigla", "cdpessoa"})
public class VwclienteetiquetaCrud extends CrudControllerSined<VwclienteetiquetaFiltro, Vwclienteetiqueta, Vwclienteetiqueta>{
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Vwclienteetiqueta form) throws CrudException {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Vwclienteetiqueta bean) throws Exception {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Vwclienteetiqueta bean) throws Exception {
		throw new SinedException("Opera��o n�o permitida.");
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}

}
