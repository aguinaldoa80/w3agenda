package br.com.linkcom.sined.modulo.crm.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Campanhasituacao;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.CampanhasituacaoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(
		path="/crm/crud/Campanhasituacao",
		authorizationModule=CrudAuthorizationModule.class
)
public class CampanhasituacaoCrud extends CrudControllerSined<CampanhasituacaoFiltro, Campanhasituacao, Campanhasituacao>{
	
//	@Override
//	protected void salvar(WebRequestContext request, Campanhasituacao bean) throws Exception {
//		try {			
//			super.salvar(request, bean);
//		} catch (DataIntegrityViolationException e) {
//			if (DatabaseError.isKeyPresent(e, "idx_leadsituacao_nome")) 
//				throw new SinedException("Segmento j� cadastrado no sistema.");	
//		}
//	}
	
}
