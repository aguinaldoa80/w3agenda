package br.com.linkcom.sined.modulo.crm.controller.crud;


import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campoentrada;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadehistorico;
import br.com.linkcom.sined.geral.bean.Oportunidadematerial;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PropostaCaixaItem;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.geral.bean.Propostacaixatemplate;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.Tipopessoacrm;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalizadorOportunidade;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.CampanhaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContacrmcontatoService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialformulavalorvendaService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OportunidadefonteService;
import br.com.linkcom.sined.geral.service.OportunidadehistoricoService;
import br.com.linkcom.sined.geral.service.OportunidadematerialService;
import br.com.linkcom.sined.geral.service.OportunidadesituacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PropostaCaixaItemService;
import br.com.linkcom.sined.geral.service.PropostacaixaService;
import br.com.linkcom.sined.geral.service.RestricaoService;
import br.com.linkcom.sined.geral.service.TelaService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadeFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Controller(path="/crm/crud/Oportunidade",authorizationModule=CrudAuthorizationModule.class)
public class OportunidadeCrud extends CrudControllerSined<OportunidadeFiltro, Oportunidade, Oportunidade>{

	protected ContacrmService contacrmService;
	protected OportunidadeService oportunidadeService;
	protected OportunidadehistoricoService oportunidadehistoricoService;
	protected OportunidadesituacaoService oportunidadesituacaoService;
	protected ColaboradorService colaboradorService;
	protected ContacrmcontatoService contacrmcontatoService;
	protected CampanhaService campanhaService;
	protected ArquivoService arquivoService; 
	protected UfService ufService;
	protected MunicipioService municipioService;
	protected MaterialService materialService;	
	protected TelaService telaService;
	protected LeadhistoricoService leadhistoricoService;
	protected OportunidadefonteService oportunidadefonteService;
	protected ContatoService contatoService;
	protected PropostaCaixaItemService propostaCaixaItemService;
	protected PropostacaixaService propostacaixaService;
	protected RestricaoService restricaoService;
	protected OportunidadematerialService oportunidadematerialService;
	protected ClienteService clienteService;
	protected FornecedorService fornecedorService;
	protected MaterialformulavalorvendaService materialformulavalorvendaService;
	protected ParametrogeralService parametrogeralService;
	
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setOportunidadematerialService(OportunidadematerialService oportunidadematerialService) {this.oportunidadematerialService = oportunidadematerialService;}
	public void setPropostacaixaService(
			PropostacaixaService propostacaixaService) {
		this.propostacaixaService = propostacaixaService;
	}
	public void setPropostaCaixaItemService(
			PropostaCaixaItemService propostaCaixaItemService) {
		this.propostaCaixaItemService = propostaCaixaItemService;
	}
	public void setLeadhistoricoService(LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setCampanhaService(CampanhaService campanhaService) {
		this.campanhaService = campanhaService;
	}
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setOportunidadehistoricoService(
			OportunidadehistoricoService oportunidadehistoricoService) {
		this.oportunidadehistoricoService = oportunidadehistoricoService;
	}
	public void setOportunidadesituacaoService(OportunidadesituacaoService oportunidadesituacaoService) {
		this.oportunidadesituacaoService = oportunidadesituacaoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setContacrmcontatoService(ContacrmcontatoService contacrmcontatoService) {
		this.contacrmcontatoService = contacrmcontatoService;
	}	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}	
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setTelaService(TelaService telaService) {
		this.telaService = telaService;
	}
	public void setOportunidadefonteService(OportunidadefonteService oportunidadefonteService) {
		this.oportunidadefonteService = oportunidadefonteService;
	}	
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setRestricaoService(RestricaoService restricaoService) {
		this.restricaoService = restricaoService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {
		this.materialformulavalorvendaService = materialformulavalorvendaService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Oportunidade form) throws Exception {
		request.setAttribute("ignoreHackAndroidDownload", true);

		if(form.getCdoportunidade() == null){
			
			form.setDtInicio(new Date(System.currentTimeMillis()));
			form.setCiclo(0);
			form.setOportunidadesituacao(new Oportunidadesituacao(1));
			request.getServletResponse().setContentType("text/html");
			try{
				String paramConta = request.getParameter("contacrminsert");
				if(paramConta != null && !paramConta.equals("")){
					Contacrm contacrm = new Contacrm();
					contacrm.setCdcontacrm(Integer.parseInt(paramConta));
					contacrm = contacrmService.load(contacrm);
					form.setContacrm(contacrm);
					form.setTipopessoacrm(Tipopessoacrm.CONTA);
					if (contacrm != null) {
						if (contacrm.getTiporesponsavel() != null && contacrm.getTiporesponsavel().equals(Tiporesponsavel.COLABORADOR) && contacrm.getResponsavel() != null) {
							form.setResponsavel(colaboradorService.load(contacrm.getResponsavel(), "colaborador.cdpessoa, colaborador.nome"));
							form.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
						} else if (contacrm.getTiporesponsavel() != null && contacrm.getTiporesponsavel().equals(Tiporesponsavel.AGENCIA) && contacrm.getAgencia() != null){
							form.setAgencia(fornecedorService.load(contacrm.getAgencia(), "fornecedor.cdpessoa, fornecedor.nome"));
							form.setTiporesponsavel(Tiporesponsavel.AGENCIA);
						}
					} else {
//						String fullName = "'br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=";
						Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
						if(form.getResponsavel() == null){				
							form.setColaborador(colaborador);
						}
						
//						if(colaborador != null && colaborador.getCdpessoa() != null){
//							request.setAttribute("cdresponsavel", fullName + colaborador.getCdpessoa()+"]'");
//						}else{
//							request.setAttribute("cdresponsavel", 0);
//						}
					}
				}
			}catch (Exception e) {
			}
		}else{
			if(form.getCdoportunidade() != null){
				boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
				form.setPermitidoEdicao(!isRestricaoClienteVendedor ||
												SinedUtil.getUsuarioLogado().equals(form.getResponsavel()));
			}
			if(form.getListoportunidadehistorico() != null){
				for(Oportunidadehistorico historico : form.getListoportunidadehistorico()){
					if(historico.getObservacao() != null)
						historico.setObservacao(historico.getObservacao().replaceAll("\n", "<br>"));
				
				}
			}
		
			if(form.getContacrmcontato() != null && form.getContacrmcontato().getCdcontacrmcontato() != null){
				request.setAttribute("contacrmcontatoSelecionado", "br.com.linkcom.sined.geral.bean.Contacrmcontato[cdcontacrmcontato=" + form.getContacrmcontato().getCdcontacrmcontato() + "]");
			} else {
				request.setAttribute("contacrmcontatoSelecionado", "<null>");
			}
			
			if(form.getContato() != null && form.getContato().getCdpessoa() != null){
				request.setAttribute("contatoSelecionado", "br.com.linkcom.sined.geral.bean.Contato[cdpessoa=" + form.getContato().getCdpessoa() + "]");
			} else {
				request.setAttribute("contatoSelecionado", "<null>");
			}
			
		}
		request.setAttribute("combooportunidadesituacao", oportunidadesituacaoService.populaComboSituacao(form));
		
		if(form.getResponsavel() != null && form.getResponsavel().getCdpessoa() != null &&
				form.getTiporesponsavel() != null && form.getTiporesponsavel().equals(Tiporesponsavel.AGENCIA)){
			Fornecedor f = new Fornecedor();
			f.setCdpessoa(form.getResponsavel().getCdpessoa());
			f.setNome(form.getResponsavel().getNome());
			form.setAgencia(f);
		}else if(form.getResponsavel() != null && form.getResponsavel().getCdpessoa() != null &&
				form.getTiporesponsavel() != null && form.getTiporesponsavel().equals(Tiporesponsavel.COLABORADOR)){
			Colaborador c = new Colaborador();
			c.setCdpessoa(form.getResponsavel().getCdpessoa());
			c.setNome(form.getResponsavel().getNome());
			form.setColaborador(c);
		}else if(form.getTiporesponsavel() == null && form.getResponsavel() != null && form.getResponsavel().getCdpessoa() != null){
			Colaborador c = new Colaborador();
			c.setCdpessoa(form.getResponsavel().getCdpessoa());
			c.setNome(form.getResponsavel().getNome());
			form.setColaborador(c);
		}
		
		Tela tela = telaService.findForCampoentradaByPath(TagFunctions.getPartialURL());
		StringBuilder campos = new StringBuilder();
		if(tela != null && tela.getListaCampoentrada() != null && !tela.getListaCampoentrada().isEmpty()){
			for(Campoentrada campoentrada : tela.getListaCampoentrada()){
				if((campoentrada.getExibir() == null || !campoentrada.getExibir()) && (campoentrada.getCampo() != null && 
						!"".equals(campoentrada.getCampo()))){
					campos.append(campos.toString().equals("") ? campoentrada.getCampo().toLowerCase().replace(" ", "") : "," + campoentrada.getCampo().toLowerCase().replace(" ", ""));
				}
			}
		}		
		
		//listLeadHistorico
		if(form.getContacrm() != null && form.getContacrm().getLead() != null && form.getContacrm().getLead().getCdlead() != null ){
			List<Leadhistorico> listLeadHistorico = leadhistoricoService.findByLead(form.getContacrm().getLead());
			if(SinedUtil.isListNotEmpty(listLeadHistorico)){
				for(Leadhistorico historico : listLeadHistorico){
					if(historico.getObservacao() != null)
						historico.setObservacao(historico.getObservacao().replaceAll("\n", "<br>"));
				
				}
			}
			request.setAttribute("listLeadHistorico", listLeadHistorico);
		}
		request.setAttribute("registarEdicao", parametrogeralService.getBoolean(Parametrogeral.REGISTRAR_EDICAO_OPORTUNIDADE));
		request.setAttribute("listaOportunidadefonte", oportunidadefonteService.findAll("nome"));
		//verifica parametro no sistema para tornar obrigatorio campo observacao
		request.setAttribute("OBRIGAR_OBS_OPORTUNIDADE", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_OBS_OPORTUNIDADE));
		request.setAttribute("camposHide", campos.toString() != null && !campos.toString().equals("") ? Util.strings.tiraAcento(campos.toString()) : "nenhum");
		oportunidadeService.criaMapaAtributoUnidademedida(request, form);
		oportunidadeService.calcularMargemcontribuicaoByFormula(form);
		request.setAttribute("CALCULO_MARGEMCONTRIB_OPORTUNIDADE", parametrogeralService.getBoolean(Parametrogeral.CALCULO_MARGEMCONTRIB_OPORTUNIDADE));
		super.entrada(request, form);
	}
	
	@Override
	protected Oportunidade criar(WebRequestContext request, Oportunidade form)throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdoportunidade() != null){
			return oportunidadeService.criarCopia(form);
		}
		return super.criar(request, form);
	}
	
	/**
	 * Valida se a situa��o escolhida � anterior � situa��o atual.
	 * A combo tamb�m n�o permite selecionar uma situa��o anterior � situa��o atual.
	 *
	 * Valida se o campo probalidade possui valor maior que 100.
	 * 
	 * @author Taidson Santos
	 * @since 30/03/2010
	 * @param filtro
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void validateBean(Oportunidade bean, BindException errors) {
		if(bean.getProbabilidade() != null && bean.getProbabilidade() > 100){
			errors.reject("001", "O campo Prob. Sucesso n�o pode ser maior que 100.");
		}
		super.validateBean(bean, errors);
	}
	
	@Override
	protected ListagemResult<Oportunidade> getLista(WebRequestContext request, OportunidadeFiltro filtro) {
		ListagemResult<Oportunidade> listResult = super.getLista(request, filtro);
		
		boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
		for(Oportunidade op : listResult.list()){
			if(op.getProbabilidade() != null){
				op.setShowProbabilidade(op.getProbabilidade().toString() + "%"); 
			}
				
			if(op.getOportunidadesituacao() != null){
				Oportunidadesituacao os = oportunidadesituacaoService.carregaImagemarquivo(op.getOportunidadesituacao());
				if(os != null && os.getImagem() != null && os.getImagem().getCdarquivo() != null){
					try {					
						arquivoService.loadAsImage(os.getImagem());
						if(os.getImagem().getCdarquivo() != null){
							DownloadFileServlet.addCdfile(request.getSession(),os.getImagem().getCdarquivo());
							op.setOportunidadesituacao(os);
						}
					} catch (Exception e) {
						//e.printStackTrace();
					}
				}
			}
			
			op.setPermitidoEdicao(!isRestricaoClienteVendedor ||
									SinedUtil.getUsuarioLogado().equals(op.getResponsavel()));
		}
		
		String whereIn = SinedUtil.listAndConcatenateIDs(listResult.list());
		if(whereIn != null && !whereIn.equals("")){
			List<Oportunidadematerial> list = oportunidadematerialService.findByOportunidade(whereIn);
			String whereInOportunidadeFiltro = Boolean.TRUE.equals(filtro.getExibirTotais()) ? oportunidadeService.getWhereInOportunidadeFiltro(filtro) : whereIn;
			
			for (Oportunidade oportunidade : listResult.list()) {
				oportunidade.setListaOportunidadematerial(new HashSet<Oportunidadematerial>(oportunidadematerialService.getListaOportunidadematerialByOportunidade(list, oportunidade)));
			}
			
			if(Boolean.TRUE.equals(filtro.getExibirTotais())){
				filtro.setListaTotalizadorOportunidadeProjetoMaterial(oportunidadeService.calculaTotaisMaterialProjetoListagemOportunidade(filtro, whereInOportunidadeFiltro));
				filtro.setListaTotalizadorOportunidadesituacao(oportunidadeService.calculaTotaisOportunidadesituacaoListagemOportunidade(filtro, whereInOportunidadeFiltro));
			}
			
			if(filtro.getListaTotalizadorOportunidadeProjetoMaterial() != null && !filtro.getListaTotalizadorOportunidadeProjetoMaterial().isEmpty()){
				Double totalgeralMaterialprojeto = 0.0;
				Integer totalqtde = 0;
				for(TotalizadorOportunidade to : filtro.getListaTotalizadorOportunidadeProjetoMaterial()){
					totalgeralMaterialprojeto += to.getValor().getValue().doubleValue();
					totalqtde += to.getQtde();
				}
				filtro.getListaTotalizadorOportunidadeProjetoMaterial().add(new TotalizadorOportunidade("TOTAL: ", totalgeralMaterialprojeto*100, totalqtde));
				request.setAttribute("exibirTotalizadoresProjetoMaterial", Boolean.TRUE);
			}else
				request.setAttribute("exibirTotalizadoresProjetoMaterial", Boolean.FALSE);
			
			if(filtro.getListaTotalizadorOportunidadesituacao() != null && !filtro.getListaTotalizadorOportunidadesituacao().isEmpty()){
				Double totalgeralOportunidadesituacao = 0.0;
				Integer totalqtde = 0;
				for(TotalizadorOportunidade to : filtro.getListaTotalizadorOportunidadesituacao()){
					totalgeralOportunidadesituacao += to.getValor().getValue().doubleValue();
					totalqtde += to.getQtde();
				}
				filtro.getListaTotalizadorOportunidadesituacao().add(new TotalizadorOportunidade("TOTAL: ", totalgeralOportunidadesituacao*100, totalqtde));
				request.setAttribute("exibirTotalizadoresOportunidadesituacao", Boolean.TRUE);
			}else
				request.setAttribute("exibirTotalizadoresOportunidadesituacao", Boolean.FALSE);
			
						
		}
		
		
		return listResult;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Oportunidade bean) throws Exception {
		boolean isCriar = bean.getCdoportunidade() == null;
		
		// GERAR O PR�XIMO N�MERO DE FATURA DE LOCA��O A PARTIR DA EMPRESA
		if(isCriar && bean.getUsarSequencial() != null && bean.getUsarSequencial()){
			oportunidadeService.gerarNumeroSequencial(bean);
		}
		
		if(Tiporesponsavel.AGENCIA.equals(bean.getTiporesponsavel()) && bean.getAgencia() != null){			
			bean.setResponsavel(new Pessoa(bean.getAgencia().getCdpessoa()));
		} else if(Tiporesponsavel.COLABORADOR.equals(bean.getTiporesponsavel()) && bean.getColaborador() != null){			
			bean.setResponsavel(new Pessoa(bean.getColaborador().getCdpessoa()));
		}
		
		if(bean.getDtInicio() != null){
			Date dataAtual = new Date(System.currentTimeMillis());			
			if(bean.getOportunidadesituacao() != null){
				Oportunidadesituacao situacaocancelada = oportunidadesituacaoService.findSituacaocancelada();
				Oportunidadesituacao situacaofinal = oportunidadesituacaoService.findSituacaofinal();				
				if(situacaocancelada != null && !bean.getOportunidadesituacao().equals(situacaocancelada) &&
						situacaofinal != null && !bean.getOportunidadesituacao().equals(situacaofinal)){
					bean.setCiclo(SinedDateUtils.diferencaDias(dataAtual, bean.getDtInicio()));
				}
			}
		}
		
		try {			
			super.salvar(request, bean);
			
			boolean haveObservacaoHistorico = bean.getObservacao() != null && !bean.getObservacao().trim().equals("");
			boolean haveArquivoHistorico = bean.getArquivohistorico() != null && bean.getArquivohistorico().getNome() != null;
			if(bean != null && (haveObservacaoHistorico || haveArquivoHistorico || !Boolean.TRUE.equals(bean.getAlterado()))){
				Oportunidadehistorico oportunidadehistorico = new Oportunidadehistorico();
				
				oportunidadehistorico.setOportunidade(bean);
				if(!Boolean.TRUE.equals(bean.getAlterado())){
					String msg = "";
					if(bean.getObservacao()!=null){
						msg = bean.getObservacao().concat(". ");
					}
					msg = msg.concat("A situa��o e/ou probabilidade n�o foram alteradas pelo usuario "+SinedUtil.getUsuarioLogado().getNome());
					oportunidadehistorico.setObservacao(msg);
				}else{
					oportunidadehistorico.setObservacao(bean.getObservacao());
				}
				oportunidadehistorico.setArquivo(bean.getArquivohistorico());
				oportunidadehistorico.setDtaltera(bean.getDtaltera());
				oportunidadehistorico.setCdusuarioaltera(bean.getCdusuarioaltera());
				oportunidadehistorico.setOportunidadesituacao(bean.getOportunidadesituacao());
				oportunidadehistorico.setProbabilidade(bean.getProbabilidade());
				
				oportunidadehistoricoService.saveOrUpdate(oportunidadehistorico);
			}
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_oportunidade_nome")) {
				throw new SinedException("Oportunidade j� cadastrada no sistema.");
			} else {
				e.printStackTrace();
				throw e;
			}
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Oportunidade form) throws CrudException {
		ModelAndView doSalvar = super.doSalvar(request, form);
		
		if(form.getRedirecionaTela()){
			request.clearMessages();
			request.addMessage("Oportunidade salva com sucesso.");
			return new ModelAndView("redirect:/servicointerno/crud/Solicitacaoservico?ACAO=criarAgendamentoOportunidade&cdoportunidade="+form.getCdoportunidade());
		}
		return doSalvar;
	}
	
	@Override
	protected void listagem(WebRequestContext request, OportunidadeFiltro filtro) throws Exception {
		if(telaService.isCampoentrada(TagFunctions.getPartialURL()))
			request.setAttribute("showNewLinkPopup", Boolean.TRUE);
		
		List<Oportunidadesituacao> listaOportunidadesituacao = oportunidadesituacaoService.findAll();
		
		if(filtro.getMaterial() != null)
			filtro.setMaterial(materialService.load(filtro.getMaterial(), "material.cdmaterial, material.nome, material.identificacao"));
		if(filtro.getMunicipio() != null && filtro.getMunicipio().getCdmunicipio() != null)
			request.setAttribute("municipiofiltro", "br.com.linkcom.sined.geral.bean.Municipio[cdmunicipio=" +filtro.getMunicipio().getCdmunicipio() + "]");
		if(filtro.getUf() != null && filtro.getUf().getCduf() != null)
			request.setAttribute("uffiltro", "br.com.linkcom.sined.geral.bean.Uf[cduf=" +filtro.getUf().getCduf() + "]");
		
		request.setAttribute("listaOportunidadesituacao", listaOportunidadesituacao);
		request.setAttribute("listaOportunidadefonte", oportunidadefonteService.findAll("nome"));
		super.listagem(request, filtro);
	}
	
	/**
	 * M�todo chamado via Ajax para sugerir o responsavel da conta
	 * 
	 * @param request
	 * @param contacrm
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView sugereResponsavelAjax(WebRequestContext request, OportunidadeFiltro filtro){
		Contacrm contAux = filtro.getContacrm();
		if(contAux != null && contAux.getCdcontacrm() != null){
			contAux = contacrmService.loadContaWithResponsavel(contAux);
			if (contAux != null)
				return new JsonModelAndView().addObject("responsavel", contAux.getResponsavel());
		}
		return new JsonModelAndView().addObject("responsavel", null);
	}
	
	public ModelAndView editarHistorico(WebRequestContext request, Oportunidadehistorico oportunidadehistorico){
		//verificando obrigatoriedade do campo observacao
		request.setAttribute("OBRIGAR_OBS_OPORTUNIDADE", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_OBS_OPORTUNIDADE));
		oportunidadehistorico = oportunidadehistoricoService.loadForEntrada(oportunidadehistorico);
		return new ModelAndView("direct:/crud/popup/editarHistoricoOportunidade", "bean", oportunidadehistorico);
	}
	
	public void saveHistorico(WebRequestContext request, Oportunidadehistorico oportunidadehistorico){
		oportunidadehistoricoService.saveOrUpdateWithoutLog(oportunidadehistorico);
		request.addMessage("Hist�rico editado com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	 /**
     * Verifica se a oportunidade est� na situa��o Cancelada.
     * @param request
     * @author Taidson
     * @since 03/08/2010
     */
    public void ajaxSituacaoCancelada(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		
		Boolean cancelada = null;
		cancelada = oportunidadeService.haveSituacaoCancelada(whereIn);
		
		View.getCurrent().println("var cancelada = '" + Util.strings.toStringIdStyled(cancelada, false)+"';");
	}
    
    /**
     * Carrega a combo de contacrmcontato conforme a conta selecionada.
     * @param request
     * @param agendainteracao
     * @author Taidson
     * @since 06/10/2010
     */
    public void ajaxComboContato(WebRequestContext request, Oportunidade oportunidade){
		
		List<Contacrmcontato> listaContacrmcontato = new ArrayList<Contacrmcontato>();
		List<Contato> listaContato = new ArrayList<Contato>();
		
		if(oportunidade.getContacrm() != null){
			listaContacrmcontato = contacrmcontatoService.findByContacrmForCombo(oportunidade.getContacrm());			
		}
		if(oportunidade.getResponsavel() != null){
			listaContato = contatoService.findByPessoa(oportunidade.getResponsavel());			
		}
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaContacrmcontato, "listaContacrmcontato", ""));
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaContato, "listaContato", ""));
	}
    
    public void ajaxComboCampanha(WebRequestContext request, Oportunidade oportunidade){
    	
    	List<Campanha> listaCampanha = new ArrayList<Campanha>();
    	
   		listaCampanha = campanhaService.findAll();
    	View.getCurrent().println(SinedUtil.convertToJavaScript(listaCampanha, "listaCampanha", ""));
    }
    
    /**
    * M�todo para preencher o UF/MUNICIPIO de acordo com o responsavel escolhido no filtro,
    * sen�o tiver respons�vel faz a busca dos UF/MUNICIPIO normal
    *
    * @param request
    * @since Oct 6, 2011
    * @author Luiz Fernando F Silva
    */
    public void ajaxPreencheComboUfMunicipioOportunidade(WebRequestContext request){
    	
    	View view = View.getCurrent();
    	request.getServletResponse().setContentType("text/html");
    	    	
    	List<Contacrm> listaContacrm = null;
    	List<Municipio> listaMunicipio = new ArrayList<Municipio>();
    	List<Uf> listaUf = new ArrayList<Uf>();
    	Uf uf = null;
    	Colaborador colaborador = null;
    	String cdcolaborador = request.getParameter("cdcolaborador");
    	String cduf = request.getParameter("cduf");
    	
    	if(cdcolaborador != null && !cdcolaborador.equals(""))
    		 colaborador = new Colaborador(Integer.parseInt(cdcolaborador));
    	
    	if(cduf != null && !cduf.equals("")){
    		uf = new Uf();
    		uf.setCduf(Integer.parseInt(cduf));
    	}
    	
    	if( (cduf == null || cduf.equals("")) && (cdcolaborador == null || cdcolaborador.equals("")) ){
    		listaUf = ufService.findAll(); 
    		String listauf = SinedUtil.convertToJavaScript(listaUf, "listauf", null);
    		view.println(listauf);
    		return;
    	}else if(cduf != null && !cduf.equals("") && (cdcolaborador == null || cdcolaborador.equals("")) ){
    		listaMunicipio = municipioService.find(uf); 
    		String listamunicipio = SinedUtil.convertToJavaScript(listaMunicipio, "listamunicipio", null);
    		view.println(listamunicipio);
    		return;
    	}
    	
    	StringBuilder s = new StringBuilder();
    	Boolean sucesso = false;
    	Boolean contem = false;
    	
    	
    	listaContacrm = contacrmService.buscaEnderecoColaborador(colaborador, uf);
    	
    	
    	if(listaContacrm != null && !listaContacrm.isEmpty()){
    		for(Contacrm contacrm : listaContacrm){
    			if(contacrm.getListcontacrmcontato() != null && !contacrm.getListcontacrmcontato().isEmpty()){
    				for(Contacrmcontato contacrmcontato : contacrm.getListcontacrmcontato()){
    					if(contacrmcontato.getMunicipio() != null && contacrmcontato.getMunicipio().getCdmunicipio() != null){
    						contem = false;
    						if(listaMunicipio != null && !listaMunicipio.isEmpty()){
    							for(Municipio m : listaMunicipio){
    								if(m.equals(contacrmcontato.getMunicipio())){
    									contem = true;
    									break;
    								}
    							}
    							if(!contem)
    								listaMunicipio.add(contacrmcontato.getMunicipio());
    						}else
    							listaMunicipio.add(contacrmcontato.getMunicipio());
    						
    						if(contacrmcontato.getMunicipio().getUf() != null && contacrmcontato.getMunicipio().getUf().getCduf() != null){
    							contem = false;
    							if(listaUf != null && !listaUf.isEmpty()){
        							for(Uf u : listaUf){
        								if(u.equals(contacrmcontato.getMunicipio().getUf())){
        									contem = true;
        									break;
        								}
        							}
        							if(!contem)
        								listaUf.add(contacrmcontato.getMunicipio().getUf());
        						}else
        							listaUf.add(contacrmcontato.getMunicipio().getUf());
    						}
    					}
    				}
    			}
    		}
    	}
    	
    	if(listaMunicipio != null && !listaMunicipio.isEmpty()){
    		String listamunicipio = SinedUtil.convertToJavaScript(listaMunicipio, "listamunicipio", null);
    		String listauf = new String();
    		if(listaUf != null && !listaUf.isEmpty())
    			listauf = SinedUtil.convertToJavaScript(listaUf, "listauf", null);
    		s.append(listamunicipio);    		
    		s.append(listauf);
    		sucesso = Boolean.TRUE;
    	}   	
		
    	if(sucesso){
    		s.append("var sucesso = true;");
    		view.println(s.toString());
    	}else{
    		s.append("var sucesso = false;");
    		view.println(s.toString());
    	}
    }
    
    /**
     * M�todo ajax para sugerir o valor do material selecionado para a oportunidade
     *
     * @param request
     * @author Luiz Fernando
     */
    public void ajaxSugerirValorVendaMaterial(WebRequestContext request){
    	View view = View.getCurrent();
    	request.getServletResponse().setContentType("text/html");
    	
    	String cdmaterial = request.getParameter("cdmaterial");
    	Double valorvendamaterial = null;
    	Double valorcustomaterial = null;
    	String unidademedida = null;
    	if(cdmaterial != null && !"".equals(cdmaterial)){
    		Material material = materialService.loadForCalcularValorvenda(new Material(Integer.parseInt(cdmaterial)));
    		if(material != null){
    			material.setRevendedor(Boolean.valueOf(request.getParameter("revendedor") != null ? request.getParameter("revendedor") : "false"));
    			try {
    				material.setQuantidadeOportunidade(Double.parseDouble(StringUtils.isNotEmpty(request.getParameter("quantidade")) ? request.getParameter("quantidade") : "0"));
				} catch (Exception e) {}
    			materialformulavalorvendaService.setValorvendaByFormula(material, null, null, null, null, null, false);
    			valorvendamaterial = material.getValorvenda();
    			valorcustomaterial = material.getValorcusto();
    			if(material.getUnidademedida() != null){
    				unidademedida = "br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + material.getUnidademedida().getCdunidademedida() + "]";
    			}
    		}
    	}
    	
    	view.println("var unidademedida = '" + unidademedida + "';");
    	view.println("var valorvendamaterial = '" + (valorvendamaterial != null ? new Money(valorvendamaterial) : "<null>") + "';");
    	view.println("var valorcustomaterial = '" + (valorcustomaterial != null ? new Money(valorcustomaterial) : "") + "';");
    }
    
    /**
     *
     *
     * @param request
     * @author Luiz Fernando
     */
    public void ajaxDadoscontato(WebRequestContext request){    	
    	View view = View.getCurrent();
    	request.getServletResponse().setContentType("text/html");
    	    	
    	String listaemails = null;
    	String listatelefones = null;    	    	
    	String cdcontacrmcontato = request.getParameter("cdcontacrmcontato");
    	String cdpessoa = request.getParameter("cdpessoa");
    	
    	if(cdcontacrmcontato != null && !"".equals(cdcontacrmcontato)){
    		Contacrmcontato contacrmcontato = new Contacrmcontato();
    		contacrmcontato.setCdcontacrmcontato(Integer.parseInt(cdcontacrmcontato));
    		contacrmcontato = contacrmcontatoService.buscaEmailsTelefones(contacrmcontato);
    		
    		if(contacrmcontato != null){
    			listaemails = contacrmcontato.getListaEmail();
    			listatelefones = contacrmcontato.getTelefones();
    		}
    	}else if(cdpessoa != null && !"".equals(cdpessoa)){
    		Pessoa pessoa = new Pessoa();
    		pessoa.setCdpessoa(Integer.parseInt(cdpessoa));
    		Contato contato = contatoService.buscaEmailsTelefones(pessoa);
    		
    		if(contato != null){
    			listaemails = contato.getEmailcontato() + "<BR>";
    			listatelefones = contato.getTelefones();
    		}
    	}    	
    	
    	if(listaemails != null && !"".equals(listaemails)){
    		view.println("var lista = '" + listaemails + listatelefones + "';");
    	}else if(listaemails != null && !"".equals(listaemails)){
    		view.println("var lista = '" + listaemails + "';");
    	}else if(listatelefones != null && !"".equals(listatelefones)){
    		view.println("var lista = '" + listatelefones + "';");
    	}else
    		view.println("var lista = '<null>';");
    }
    
    /**
     * 
     * M�todo Para carregar a oportunidade que foi inserida na tela de contacrm
     *
     *@author Thiago Augusto
     *@date 13/01/2012
     * @param request
     * @param filtro
     * @return
     */
    public ModelAndView carregarOportunidade(WebRequestContext request, OportunidadeFiltro filtro){
    	Oportunidade oportunidade = new Oportunidade();
    	if(filtro.getOportunidade() != null && filtro.getOportunidade().getCdoportunidade() != null){
    		oportunidade = filtro.getOportunidade();
    	} else {
    		oportunidade.setCdoportunidade(filtro.getCdoportunidade());
    	}
    	
    	if(oportunidade != null && oportunidade.getCdoportunidade() != null){
    		oportunidade = oportunidadeService.carregarOportunidadeCompleta(oportunidade);
    	}
    	
    	return new JsonModelAndView().addObject("oportunidade", oportunidade);
	}
    
    /**
     * 
     * M�todo que exclui as oportunidades da tela Contacrm.
     *
     *@author Thiago Augusto
     *@date 16/01/2012
     * @param request
     */
    public void ajaxExcluirOportunidade(WebRequestContext request){
    	String cdOportunidade = request.getParameter("cdoportunidade");
    	Oportunidade bean = new Oportunidade();
    	bean.setCdoportunidade(Integer.parseInt(cdOportunidade));
    	oportunidadeService.delete(bean);
    }
    
    /**
	 * Verifica se existe um modelo de contrato associado ao Material/Servi�os,
	 * retorna o modelo se existir apenas um, caso contr�rio dever� ser exibida
	 * uma tela para que o usu�rio selecione o contrato.
	 * 
	 * @param request
	 * @author Giovane Freitas
	 * @since 21/01/2012
	 */
    public ModelAndView selecionarModeloOportunidade(WebRequestContext request, Oportunidade oportunidade){
    	oportunidade = oportunidadeService.load(oportunidade, "oportunidade.propostacaixa");
    	String erro = "";
    	
    	JsonModelAndView jsonModelAndView = new JsonModelAndView();
    	
    	boolean geracaoRTF = false;
    	Integer cdreporttemplate = null;
    	
		if(oportunidade != null && oportunidade.getPropostacaixa() != null && oportunidade.getPropostacaixa() != null){
			Propostacaixa propostacaixa = propostacaixaService.loadForEntrada(oportunidade.getPropostacaixa());
	    	if(propostacaixa.getArquivo() != null &&
	    			propostacaixa.getArquivo().getCdarquivo() != null){
	    		geracaoRTF = true;
	    	} else {
	    		Set<Propostacaixatemplate> listaPropostacaixatemplate = propostacaixa.getListaPropostacaixatemplate();
	    		if(listaPropostacaixatemplate != null && listaPropostacaixatemplate.size() > 0){
	    			if(listaPropostacaixatemplate.size() == 1){
	    				try{
	    					cdreporttemplate = listaPropostacaixatemplate.iterator().next().getReporttemplate().getCdreporttemplate();
	    				} catch (Exception e) {
	    					e.printStackTrace();
						}
	    			}
	    		} else {
	    			erro += "N�o foi encontrado nenhum RTF e nenhum template na caixa de proposta da oportunidade.\n";
	    		}
	    	}
    	} else {
    		erro += "N�o foi encontrada a caixa de proposta na oportunidade selecionada.\n";
    	}
    	
		return jsonModelAndView
						.addObject("erro", erro)
						.addObject("geracaoRTF", geracaoRTF)
						.addObject("cdreporttemplate", cdreporttemplate);
	}

	/**
	 * Abre uma tela para que o usu�rio selecione o modelo de contrato RTF a ser usado para a emiss�o.
	 * 
	 * @author Giovane Freitas
	 * @param request
	 * @return
	 */
	public ModelAndView abrirSelecaoModeloOportunidade(WebRequestContext request, Oportunidade oportunidade) {
		if(oportunidade == null || oportunidade.getCdoportunidade() == null){
			request.addError("Selecione uma oportunidade para realizar a emiss�o.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(!oportunidadeService.isUsuarioPodeEditarOportunidades(oportunidade.getCdoportunidade().toString())){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		oportunidade = oportunidadeService.load(oportunidade, "oportunidade.cdoportunidade, oportunidade.propostacaixa");
		
		if(oportunidade != null && oportunidade.getPropostacaixa() != null && oportunidade.getPropostacaixa() != null){
			Propostacaixa propostacaixa = propostacaixaService.loadForEntrada(oportunidade.getPropostacaixa());
			Set<Propostacaixatemplate> listaPropostacaixatemplate = propostacaixa.getListaPropostacaixatemplate();
			List<ReportTemplateBean> listaReporttemplate = new ArrayList<ReportTemplateBean>();
			for (Propostacaixatemplate propostacaixatemplate : listaPropostacaixatemplate) {
				listaReporttemplate.add(propostacaixatemplate.getReporttemplate());
			}
			request.setAttribute("listaReporttemplate", listaReporttemplate);
		}
		
		request.setAttribute("oportunidade", oportunidade);
		return new ModelAndView("direct:/crud/popup/selecaoModeloOportunidade");
	}
	
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, Oportunidade bean) {
		if (bean.getPropostacaixa() == null || 
				bean.getPropostacaixa().getCdpropostacaixa() == null) {
			throw new SinedException("A caixa de proposta n�o pode se nula.");
		}
		List<PropostaCaixaItem> lista = propostaCaixaItemService.findByItem(bean.getPropostacaixa());
		return new JsonModelAndView()
					.addObject("size", lista.size())
					.addObject("lista", lista);
	}
	
	/**
	 * M�todo ajax para validar a cria��o da venda a partir das oportunidades
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public ModelAndView ajaxCriarvendaportunidade(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		boolean sucesso = false;
		boolean clientediferente = false;
		boolean clienterestricao = false;
		if(whereIn != null && !"".equals(whereIn)){
			sucesso = true;
			if(oportunidadeService.isClienteDiferenteOportunidade(whereIn)){
				clientediferente = true;
			}else if(restricaoService.isClienteWithRestricaoByOportunidade(whereIn)){
				clienterestricao = true;
			}
		}
		return new JsonModelAndView()
					.addObject("sucesso", sucesso)
					.addObject("clienterestricao", clienterestricao)
					.addObject("clientediferente", clientediferente);
	}
	
	public ModelAndView getInfoMaterialUnidademedida(WebRequestContext request, Oportunidadematerial bean){
		return oportunidadeService.getInfMaterialUnidademedida(request, bean);
	}
	
	/**
	* M�todo ajax para calcular a margem de contribuicao do material da oportunidade
	*
	* @param request
	* @param oportunidadematerial
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public void calcularMargemcontribuicaoByFormulaAJAX(WebRequestContext request, Oportunidadematerial oportunidadematerial){
		Boolean revendedor = Boolean.valueOf(request.getParameter("revendedor") != null ? request.getParameter("revendedor") : "false"); 
		oportunidadeService.calcularMargemcontribuicaoByFormula(oportunidadematerial, revendedor);
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		view.println("var margemcontribuicaovalorFormula = '" + (oportunidadematerial.getMargcontribvalor() != null ? SinedUtil.descriptionDecimal(SinedUtil.round(oportunidadematerial.getMargcontribvalor(), 2)) : "") + "';");
		view.println("var margemcontribuicaopercentualFormula = '" + (oportunidadematerial.getMargcontribpercentual() != null ? SinedUtil.descriptionDecimal(SinedUtil.round(oportunidadematerial.getMargcontribpercentual(), 2)) : "") + "';");
	}
	
	/**
	 * M�todo respons�vel por retornar view
	 * @param request
	 * @param form
	 * @return
	 * @throws CrudException
	 * @since 13/07/2016
	 * @author C�sar 
	 */
	public ModelAndView oportunidadePainelInteracao(WebRequestContext request, Oportunidade form) throws CrudException{
		if(form != null && form.getCliente() != null){
			form.setCliente(clienteService.findForOportunidade(form.getCliente()));
			return getCriarModelAndView(request, form);			
		}else{
			throw new SinedException("N�o � poss�vel realizar pesquisa sem cliente.");
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Oportunidade bean)
			throws Exception {
		String itens = request.getParameter("itenstodelete");

		if(Util.strings.isNotEmpty(itens) && !oportunidadeService.isUsuarioPodeEditarOportunidades(itens)){
			request.addError("Exclus�o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.");
		}else if(bean != null && bean.getCdoportunidade() != null && !oportunidadeService.isUsuarioPodeEditarOportunidades(bean.getCdoportunidade().toString())){
			request.addError("Exclus�o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.");
		}else{
			super.excluir(request, bean);
		}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Oportunidade form)
			throws CrudException {
		boolean editando = "editar".equalsIgnoreCase(request.getParameter("ACAO"));
		if(editando && form.getCdoportunidade() != null){
			boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
			Oportunidade bean = oportunidadeService.loadWithResponsavel(form.getCdoportunidade());
			boolean permitidaEdicao = !isRestricaoClienteVendedor || SinedUtil.getUsuarioLogado().equals(bean.getResponsavel());
			if(!permitidaEdicao){
				throw new SinedException("Edi��o n�o permitida. Usu�rio logado n�o � o respons�vel pela Oportunidade.");
			}
		}
		
		return super.doEditar(request, form);
	}
}
