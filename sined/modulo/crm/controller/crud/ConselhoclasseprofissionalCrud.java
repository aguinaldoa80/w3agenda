package br.com.linkcom.sined.modulo.crm.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Conselhoclasseprofissional;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ConselhoclasseprofissionalFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Conselhoclasseprofissional"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "sigla", "ativo"})
public class ConselhoclasseprofissionalCrud extends CrudControllerSined<ConselhoclasseprofissionalFiltro, Conselhoclasseprofissional, Conselhoclasseprofissional>{

	@Override
	protected void salvar(WebRequestContext request, Conselhoclasseprofissional bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch(DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CONSELHOCLASSEPROFISSIONAL_DESCRICAO")) {
				throw new SinedException("Conselho de Classe Profissional j� cadastrado no sistema.");
			}
		}
	}	
}