package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.sql.Date;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProspeccaoFiltro extends FiltroListagemSined {
	
	protected Cliente cliente;
	protected Date dtInicio;
	protected Date dtFim;
	protected Contato contato;
	protected Colaborador colaborador;
	protected String descricao;
	protected Integer numero;
	protected String ano;
	
	
	public Date getDtInicio() {
		return dtInicio;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public Date getDtFim() {
		return dtFim;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	//@Required
	@DisplayName("Cliente")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	//@Required
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@MaxLength(9)
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	@MaxLength(2)
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public Contato getContato() {
		return contato;
	}
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	
	
}
