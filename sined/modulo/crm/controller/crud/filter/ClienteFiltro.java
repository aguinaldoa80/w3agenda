package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clienteprofissao;
import br.com.linkcom.sined.geral.bean.Clienterelacaotipo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Conselhoclasseprofissional;
import br.com.linkcom.sined.geral.bean.Especialidade;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ClienteFiltro extends FiltroListagemSined {

	protected Integer codigo;
	protected Tipopessoa tipopessoa;
	protected Cnpj cnpj;
	protected Cpf cpf;
	protected Categoria categoria;
	protected String nomefantasia;
	protected String razaosocial;
	protected Boolean ativo = Boolean.TRUE;
	
	protected String logradouro;
	protected String bairro;
	protected String numero;
	protected Uf uf;
	protected Municipio municipio;
	protected Pais pais;


	protected Mes nascimentoDe;
	protected Mes nascimentoAte;
	protected Clienteprofissao clienteprofissao;
	protected String contato;
	protected String telefone;
	protected String identificador;
	
	protected Date dtinicioinsercao;
	protected Date dtfiminsercao;

	protected Date dtiniciovalidade;
	protected Date dtfimvalidade;
	
	protected Tiporesponsavel tiporesponsavel;
	protected Cliente cliente;
	protected Colaborador clientevendedor;
	protected Fornecedor agencia;
	
	protected Boolean matriz;
	protected Boolean limitaracessoclientevendedor;
	protected Clienterelacaotipo clienterelacaotipo;
	protected Cliente clienteoutro;
	protected Segmento segmento;
	
	protected Fornecedor associacao;
	private Especialidade especialidade;
	private Conselhoclasseprofissional conselhoclasseprofissional;
	private String registro;
	
	@DisplayName("C�digo")
	public Integer getCodigo() {
		return codigo;
	}
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}
	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}
	
	@DisplayName("Categoria")
	public Categoria getCategoria() {
		return categoria;
	}
	@DisplayName("Nome")
	@MaxLength(100)
	public String getNomefantasia() {
		return nomefantasia;
	}
	
	@DisplayName("Raz�o social")
	@MaxLength(50)
	public String getRazaosocial() {
		return razaosocial;
	}
	@DisplayName("Situa��o")
	public Boolean getAtivo() {
		return ativo;
	}
	@MaxLength(100)
	@DisplayName("Logradouro")
	public String getLogradouro() {
		return logradouro;
	}
	@MaxLength(50)
	@DisplayName("Bairro")
	public String getBairro() {
		return bairro;
	}
	@DisplayName("Uf")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("Pa�s")
	public Pais getPais() {
		return pais;
	}
	public Mes getNascimentoDe() {
		return nascimentoDe;
	}
	public Mes getNascimentoAte() {
		return nascimentoAte;
	}
	@DisplayName("Profiss�o")
	public Clienteprofissao getClienteprofissao() {
		return clienteprofissao;
	}
	public String getContato() {
		return contato;
	}
	public String getTelefone() {
		return telefone;
	}
	
	@DisplayName("N�mero")
	@MaxLength(5)
	public String getNumero() {
		return numero;
	}
	@MaxLength(11)
	public String getIdentificador() {
		return identificador;
	}
	
	@DisplayName("")
	public Date getDtinicioinsercao() {
		return dtinicioinsercao;
	}
	
	@DisplayName("")
	public Date getDtfiminsercao() {
		return dtfiminsercao;
	}
	@DisplayName("Vendedor")
	public Colaborador getClientevendedor() {
		return clientevendedor;
	}
	public Date getDtiniciovalidade() {
		return dtiniciovalidade;
	}
	public Date getDtfimvalidade() {
		return dtfimvalidade;
	}
	
	@DisplayName("Tipo Respons�vel")
	public Tiporesponsavel getTiporesponsavel() {
		return tiporesponsavel;
	}

	@DisplayName("Ag�ncia")
	public Fornecedor getAgencia() {
		return agencia;
	}
	
	@DisplayName("Segmento")
	public Segmento getSegmento() {
		return segmento;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}
	public void setTiporesponsavel(Tiporesponsavel tiporesponsavel) {
		this.tiporesponsavel = tiporesponsavel;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setClienteprofissao(Clienteprofissao clienteprofissao) {
		this.clienteprofissao = clienteprofissao;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	public void setRazaosocial(String nome) {
		this.razaosocial = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public void setNascimentoDe(Mes nascimentoDe) {
		this.nascimentoDe = nascimentoDe;
	}
	public void setNascimentoAte(Mes nascimentoAte) {
		this.nascimentoAte = nascimentoAte;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setClientevendedor(Colaborador clientevendedor) {
		this.clientevendedor = clientevendedor;
	}
	public void setDtiniciovalidade(Date dtiniciovalidade) {
		this.dtiniciovalidade = dtiniciovalidade;
	}
	public void setDtfimvalidade(Date dtfimvalidade) {
		this.dtfimvalidade = dtfimvalidade;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setDtinicioinsercao(Date dtinicioinsercao) {
		this.dtinicioinsercao = dtinicioinsercao;
	}
	public void setDtfiminsercao(Date dtfiminsercao) {
		this.dtfiminsercao = dtfiminsercao;
	}

	@DisplayName("Considerar Matriz")
	public Boolean getMatriz() {
		return matriz;
	}
	public void setMatriz(Boolean matriz) {
		this.matriz = matriz;
	}
	
	public String getNumeromatriz() {
		if (this.getMatriz() != null && this.getMatriz() && getCnpj() != null && getCnpj().toString().length() >= 7){
			return this.getCnpj().getValue().toString().substring(0, 7);
		} else 
			return null;
	}

	public Boolean getLimitaracessoclientevendedor() {
		return limitaracessoclientevendedor;
	}
	public void setLimitaracessoclientevendedor(Boolean limitaracessoclientevendedor) {
		this.limitaracessoclientevendedor = limitaracessoclientevendedor;
	}
	
	@DisplayName("Refer�ncia")
	public Clienterelacaotipo getClienterelacaotipo() {
		return clienterelacaotipo;
	}
	@DisplayName("Pessoa Refer�ncia")
	public Cliente getClienteoutro() {
		return clienteoutro;
	}
	
	public void setClienterelacaotipo(Clienterelacaotipo clienterelacaotipo) {
		this.clienterelacaotipo = clienterelacaotipo;
	}
	public void setClienteoutro(Cliente clienteoutro) {
		this.clienteoutro = clienteoutro;
	}
	
	@DisplayName("Associa��o")
	public Fornecedor getAssociacao() {
		return associacao;
	}
	@DisplayName("Especialidade")
	public Especialidade getEspecialidade() {
		return especialidade;
	}
	@DisplayName("Conselho de Classe Profissional")
	public Conselhoclasseprofissional getConselhoclasseprofissional() {
		return conselhoclasseprofissional;
	}
	@DisplayName("Registro")
	@MaxLength(50)
	public String getRegistro() {
		return registro;
	}
	
	public void setAssociacao(Fornecedor associacao) {
		this.associacao = associacao;
	}
	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}
	public void setConselhoclasseprofissional(Conselhoclasseprofissional conselhoclasseprofissional) {
		this.conselhoclasseprofissional = conselhoclasseprofissional;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}	
}