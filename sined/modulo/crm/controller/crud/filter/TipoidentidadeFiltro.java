package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TipoidentidadeFiltro extends FiltroListagemSined {

	protected String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
