package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhatipo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Leadqualificacao;
import br.com.linkcom.sined.geral.bean.Leadsegmento;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.LeadsituacaoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LeadFiltro extends FiltroListagemSined {
	
	protected String nome;
	protected String empresa;
	protected Tiporesponsavel tiporesponsavel;
	protected Fornecedor agencia;
	protected Colaborador responsavel;
	
	protected Uf uf;
	protected Municipio municipio;
	protected Leadqualificacao leadqualificacao;
	protected Leadsituacao leadsituacao;
	protected Leadsegmento leadSegmento;
	protected Campanha campanha;
	protected Concorrente concorrente;
	protected Date dtiniciohistorico;
	protected Date dtfimhistorico;
	protected Date dtinicioretorno;
	protected Date dtfimretorno;
	protected Boolean convertidos;
	protected Atividadetipo atividadetipo;
	protected Campanhatipo campanhatipo;
	
	protected Date dtinicioultimocontato;
	protected Date dtfimultimocontato;
	
	protected Integer iniciointeracao;
	protected Integer fiminteracao;
	
	protected List<Leadsituacao> listaLeadsituacao = new ArrayList<Leadsituacao>();
	protected List<Leadhistorico> listLeadhistoricoTotais;
	
	private String valorCampoAdicional;
	
	public LeadFiltro() {
		this.listaLeadsituacao = LeadsituacaoService.getInstance().findForListagem();
		this.convertidos = Boolean.FALSE;
	
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if (ColaboradorService.getInstance().isColaborador(usuario.getCdpessoa())) {
			this.responsavel = ColaboradorService.getInstance().loadWithoutPermissao(new Colaborador(usuario.getCdpessoa()), "colaborador.cdpessoa, colaborador.nome");
			this.tiporesponsavel = Tiporesponsavel.COLABORADOR;
		} else if (FornecedorService.getInstance().isFornecedor(usuario.getCdpessoa())){
			this.tiporesponsavel = Tiporesponsavel.AGENCIA;
			this.agencia = FornecedorService.getInstance().loadWithoutPermissao(new Fornecedor(usuario.getCdpessoa()), "fornecedor.cdpessoa, fornecedor.nome");
		}
	}
	
	public Date getDtinicioultimocontato() {
		return dtinicioultimocontato;
	}

	public Date getDtfimultimocontato() {
		return dtfimultimocontato;
	}

	public Integer getIniciointeracao() {
		return iniciointeracao;
	}

	public Integer getFiminteracao() {
		return fiminteracao;
	}
	
	@MaxLength(80)
	@DisplayName("Contato")
	public String getNome() {
		return nome;
	}
	@MaxLength(80)
	public String getEmpresa() {
		return empresa;
	}
	@DisplayName("Respons�vel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("Qualifica��o")
	public Leadqualificacao getLeadqualificacao() {
		return leadqualificacao;
	}
	@DisplayName("Situa��o")
	public Leadsituacao getLeadsituacao() {
		return leadsituacao;
	}
	@DisplayName("Segmento")
	public Leadsegmento getLeadSegmento() {
		return leadSegmento;
	}
	@DisplayName("Campanha")	
	public Campanha getCampanha() {
		return campanha;
	}
	@DisplayName("Mostrar Leads Convertidos")
	public Boolean getConvertidos() {
		return convertidos;
	}
	@DisplayName("Situa��o")
	public List<Leadsituacao> getListaLeadsituacao() {
		return listaLeadsituacao;
	}
	
	@DisplayName("Tipo de Atividade")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	
	public List<Leadhistorico> getListLeadhistoricoTotais() {
		return listLeadhistoricoTotais;
	}
	
	@DisplayName("Tipo de Campanha")
	public Campanhatipo getCampanhatipo() {
		return campanhatipo;
	}
	
	@DisplayName("Valor do Campo Adicional")
	public String getValorCampoAdicional() {
		return valorCampoAdicional;
	}
	
	public void setValorCampoAdicional(String valorCampoAdicional) {
		this.valorCampoAdicional = valorCampoAdicional;
	}
	
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setCampanhatipo(Campanhatipo campanhatipo) {
		this.campanhatipo = campanhatipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setLeadqualificacao(Leadqualificacao leadqualificacao) {
		this.leadqualificacao = leadqualificacao;
	}
	public void setLeadsituacao(Leadsituacao leadsituacao) {
		this.leadsituacao = leadsituacao;
	}
	public void setLeadSegmento(Leadsegmento leadSegmento) {
		this.leadSegmento = leadSegmento;
	}
	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}
	public Concorrente getConcorrente() {
		return concorrente;
	}
	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}
	public Date getDtiniciohistorico() {
		return dtiniciohistorico;
	}
	public void setDtiniciohistorico(Date dtiniciohistorico) {
		this.dtiniciohistorico = dtiniciohistorico;
	}
	public Date getDtfimhistorico() {
		return dtfimhistorico;
	}
	public void setDtfimhistorico(Date dtfimhistorico) {
		this.dtfimhistorico = dtfimhistorico;
	}
	public Date getDtinicioretorno() {
		return dtinicioretorno;
	}
	public void setDtinicioretorno(Date dtinicioretorno) {
		this.dtinicioretorno = dtinicioretorno;
	}
	public Date getDtfimretorno() {
		return dtfimretorno;
	}
	public void setDtfimretorno(Date dtfimretorno) {
		this.dtfimretorno = dtfimretorno;
	}
	public void setListaLeadsituacao(List<Leadsituacao> listaLeadsituacao) {
		this.listaLeadsituacao = listaLeadsituacao;
	}
	public void setConvertidos(Boolean convertidos) {
		this.convertidos = convertidos;
	}
	public void setListLeadhistoricoTotais(List<Leadhistorico> listLeadhistoricoTotais) {
		this.listLeadhistoricoTotais = listLeadhistoricoTotais;
	}
	public void setDtinicioultimocontato(Date dtinicioultimocontato) {
		this.dtinicioultimocontato = dtinicioultimocontato;
	}
	public void setDtfimultimocontato(Date dtfimultimocontato) {
		this.dtfimultimocontato = dtfimultimocontato;
	}
	public void setIniciointeracao(Integer iniciointeracao) {
		this.iniciointeracao = iniciointeracao;
	}
	public void setFiminteracao(Integer fiminteracao) {
		this.fiminteracao = fiminteracao;
	}
	
	@DisplayName("Tipo Respons�vel")
	public Tiporesponsavel getTiporesponsavel() {
		return tiporesponsavel;
	}

	@DisplayName("Ag�ncia")
	public Fornecedor getAgencia() {
		return agencia;
	}

	public void setTiporesponsavel(Tiporesponsavel tiporesponsavel) {
		this.tiporesponsavel = tiporesponsavel;
	}

	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}
}
