package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.util.Date;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipobuscaconta;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VwclienteetiquetaFiltro extends FiltroListagemSined {

	private Categoria categoria;
	private Boolean ativo;
	private Enderecotipo enderecotipo;
	private Tipoetiqueta tipoetiqueta;
	private Integer apartirlinha;
	private Integer apartircoluna;
	private Date dtde;
	private Date dtate;
	private Contatotipo contatotipo;	
	private Tipobuscaconta tipobuscaconta;
	private Boolean isSelectedItensCdCliente;
	
	public VwclienteetiquetaFiltro() {
		this.tipobuscaconta = Tipobuscaconta.VENCIMENTO;
	}

	@DisplayName ("Contato Tipo")
	@ManyToOne (fetch=FetchType.LAZY)
	@JoinColumn (name="cdcontatotipo")
	public Contatotipo getContatotipo() {
		return contatotipo;
	}

	public void setContatotipo(Contatotipo contatotipo) {
		this.contatotipo = contatotipo;
	}
	
	public Tipobuscaconta getTipobuscaconta() {		
		return tipobuscaconta;
	}

	public void setTipobuscaconta(Tipobuscaconta tipobuscaconta) {
		this.tipobuscaconta = tipobuscaconta;
	}

	@Required
	public Categoria getCategoria() {
		return categoria;
	}
	
	@DisplayName("Situa��o")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Tipo de endere�o")
	public Enderecotipo getEnderecotipo() {
		return enderecotipo;
	}
	@DisplayName("Modelo de etiqueta")
	public Tipoetiqueta getTipoetiqueta() {
		return tipoetiqueta;
	}
	@DisplayName("Imprimir a partir da linha")
	public Integer getApartirlinha() {
		return apartirlinha;
	}
	@DisplayName("Imprimir a partir da coluna")
	public Integer getApartircoluna() {
		return apartircoluna;
	}
	public Date getDtde() {
		return dtde;
	}
	public Date getDtate() {
		return dtate;
	}
	public Boolean getIsSelectedItensCdCliente() {
		return isSelectedItensCdCliente;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setEnderecotipo(Enderecotipo enderecotipo) {
		this.enderecotipo = enderecotipo;
	}
	public void setTipoetiqueta(Tipoetiqueta tipoetiqueta) {
		this.tipoetiqueta = tipoetiqueta;
	}
	public void setApartirlinha(Integer apartirlinha) {
		this.apartirlinha = apartirlinha;
	}
	public void setApartircoluna(Integer apartircoluna) {
		this.apartircoluna = apartircoluna;
	}
	public void setDtde(Date dtde) {
		this.dtde = dtde;
	}
	public void setDtate(Date dtate) {
		this.dtate = dtate;
	}
	public void setIsSelectedItensCdCliente(Boolean isSelectedItensCdCliente) {
		this.isSelectedItensCdCliente = isSelectedItensCdCliente;
	}
}
