package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LeadsituacaoFiltro extends FiltroListagemSined{
	
	protected String nome;
	
	@MaxLength(80)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
