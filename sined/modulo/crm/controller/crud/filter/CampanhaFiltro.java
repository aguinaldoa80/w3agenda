package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Campanhasituacao;
import br.com.linkcom.sined.geral.bean.Campanhatipo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CampanhaFiltro extends FiltroListagemSined{
	
	protected String nome;
	protected Date dtcriacaoinicio;
	protected Date dtcriacaofim;
	protected Colaborador responsavel;
	protected Campanhatipo campanhatipo;
	protected Projeto projeto;
	protected List<Campanhasituacao> listaCampanhasituacao = new ArrayList<Campanhasituacao>();
	
	
	
	@MaxLength(80)
	public String getNome() {
		return nome;
	}
	public Date getDtcriacaoinicio() {
		return dtcriacaoinicio;
	}
	public Date getDtcriacaofim() {
		return dtcriacaofim;
	}
	@DisplayName("Respons�vel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	@DisplayName("Tipo da Campanha")
	public Campanhatipo getCampanhatipo() {
		return campanhatipo;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Situa��o")
	public List<Campanhasituacao> getListaCampanhasituacao() {
		return listaCampanhasituacao;
	}	
	
	
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDtcriacaoinicio(Date dtcriacaoinicio) {
		this.dtcriacaoinicio = dtcriacaoinicio;
	}
	public void setDtcriacaofim(Date dtcriacaofim) {
		this.dtcriacaofim = dtcriacaofim;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setCampanhatipo(Campanhatipo campanhatipo) {
		this.campanhatipo = campanhatipo;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setListaCampanhasituacao(List<Campanhasituacao> listaCampanhasituacao) {
		this.listaCampanhasituacao = listaCampanhasituacao;
	}
	
	
	

}
