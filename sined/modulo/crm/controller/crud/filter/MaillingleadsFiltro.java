package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Leadqualificacao;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MaillingleadsFiltro extends FiltroListagemSined {

	protected List<Segmento> listasegmento;
	protected Colaborador responsavel;
	protected Leadqualificacao leadqualificacao;
	protected Leadsituacao leadsituacao;
	protected Campanha campanha;
	
	protected String email;
	protected String assunto;
	protected String mensagem;
	protected Arquivo anexo;
	
	protected Boolean enviaCopia;
	
	public MaillingleadsFiltro() {
		mensagem = "&lt; NOMECONTATO &gt;";
	}
	
	@DisplayName("Segmento")
	public List<Segmento> getListasegmento() {
		return listasegmento;
	}
	@DisplayName("Respons�vel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	@DisplayName("Qualifica��o")
	public Leadqualificacao getLeadqualificacao() {
		return leadqualificacao;
	}
	@DisplayName("Situa��o")
	public Leadsituacao getLeadsituacao() {
		return leadsituacao;
	}
	public Campanha getCampanha() {
		return campanha;
	}
	@DisplayName("Enviar c�pia da mensagem para o meu email")
	public Boolean getEnviaCopia() {
		return enviaCopia;
	}
	
	@DisplayName("Remetente")
	@MaxLength(100)
	public String getEmail() {
		return email;
	}
	@DisplayName("Assunto")
	@MaxLength(1000)
	public String getAssunto() {
		return assunto;
	}
	@DisplayName("Mensagem")
	@MaxLength(5000)
	public String getMensagem() {
		return mensagem;
	}
	@DisplayName("Anexo")
	public Arquivo getAnexo() {
		return anexo;
	}
	public void setEnviaCopia(Boolean enviaCopia) {
		this.enviaCopia = enviaCopia;
	}	
	
	public void setListasegmento(List<Segmento> listasegmento) {
		this.listasegmento = listasegmento;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setLeadqualificacao(Leadqualificacao leadqualificacao) {
		this.leadqualificacao = leadqualificacao;
	}
	public void setLeadsituacao(Leadsituacao leadsituacao) {
		this.leadsituacao = leadsituacao;
	}
	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}
	
	
	public void setEmail(String email) {
		this.email = email;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public void setAnexo(Arquivo anexo) {
		this.anexo = anexo;
	}
}
