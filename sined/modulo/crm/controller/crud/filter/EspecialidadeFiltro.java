package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Conselhoclasseprofissional;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EspecialidadeFiltro extends FiltroListagemSined {

	private Integer cdespecialidade;
	private String descricao;
	private Conselhoclasseprofissional conselhoclasseprofissional;
	private Boolean ativo = Boolean.TRUE;
	
	@DisplayName("C�digo")
	public Integer getCdespecialidade() {
		return cdespecialidade;
	}
	@DisplayName("Especialidade")
	@MaxLength(200)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Conselho de Classe Profissional")
	public Conselhoclasseprofissional getConselhoclasseprofissional() {
		return conselhoclasseprofissional;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setCdespecialidade(Integer cdespecialidade) {
		this.cdespecialidade = cdespecialidade;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setConselhoclasseprofissional(Conselhoclasseprofissional conselhoclasseprofissional) {
		this.conselhoclasseprofissional = conselhoclasseprofissional;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}	
}