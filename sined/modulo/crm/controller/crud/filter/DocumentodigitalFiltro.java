package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documentodigitalmodelo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.enumeration.Documentodigitalsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Documentodigitaltipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentodigitalFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private Documentodigitaltipo tipo;
	private Oportunidade oportunidade;
	private Cliente cliente;
	private String descricaotipo;
	private Date dtenvio1;
	private Date dtenvio2;
	private Documentodigitalmodelo documentodigitalmodelo;
	private List<Documentodigitalsituacao> listaSituacao;
	
	public DocumentodigitalFiltro() {
		listaSituacao = new ArrayList<Documentodigitalsituacao>();
		listaSituacao.add(Documentodigitalsituacao.EM_ESPERA);
		listaSituacao.add(Documentodigitalsituacao.ENVIADO);
		listaSituacao.add(Documentodigitalsituacao.ACEITE_PARCIALMENTE);
	}
	
	public Documentodigitaltipo getTipo() {
		return tipo;
	}
	public Oportunidade getOportunidade() {
		return oportunidade;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Date getDtenvio1() {
		return dtenvio1;
	}
	public Date getDtenvio2() {
		return dtenvio2;
	}
	public List<Documentodigitalsituacao> getListaSituacao() {
		return listaSituacao;
	}
	@DisplayName("CLiente / Oportunidade")
	public String getDescricaotipo() {
		return descricaotipo;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Modelo")
	public Documentodigitalmodelo getDocumentodigitalmodelo() {
		return documentodigitalmodelo;
	}
	public void setDocumentodigitalmodelo(
			Documentodigitalmodelo documentodigitalmodelo) {
		this.documentodigitalmodelo = documentodigitalmodelo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDescricaotipo(String descricaotipo) {
		this.descricaotipo = descricaotipo;
	}
	public void setTipo(Documentodigitaltipo tipo) {
		this.tipo = tipo;
	}
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setDtenvio1(Date dtenvio1) {
		this.dtenvio1 = dtenvio1;
	}
	public void setDtenvio2(Date dtenvio2) {
		this.dtenvio2 = dtenvio2;
	}
	public void setListaSituacao(List<Documentodigitalsituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	
}
