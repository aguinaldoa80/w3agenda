package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaorelacionado;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoagendainteracao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AgendainteracaoFiltro extends FiltroListagemSined {
	
	protected Tipoagendainteracao tipoagendainteracao;
	protected Cliente cliente;
	protected Contacrm contacrm;
	protected Material material;
	protected Contrato contrato;
	protected Date dtproximainteracaode;
	protected Date dtproximainteracaoate;
	protected Colaborador responsavel;
	protected Agendainteracaorelacionado agendainteracaorelacionado;  
	protected Agendainteracaosituacao agendainteracaosituacao;
	protected List<Agendainteracaosituacao> listaSituacao;
	protected Date apuracaoPeriodoInicio;
	protected Date apuracaoPeriodoFim;
	protected Empresa empresahistorico;
	protected String contato;
	protected Boolean ativo = null;
	
	public AgendainteracaoFiltro() {
		List<Agendainteracaosituacao> listaSituacao = new ArrayList<Agendainteracaosituacao>();
		listaSituacao.add(Agendainteracaosituacao.ATENCAO);
		listaSituacao.add(Agendainteracaosituacao.ATRASADO);
		
		this.listaSituacao = listaSituacao;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	public Contrato getContrato() {
		return contrato;
	}
	public Date getDtproximainteracaode() {
		return dtproximainteracaode;
	}
	public Date getDtproximainteracaoate() {
		return dtproximainteracaoate;
	}
	@DisplayName("Respons�vel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	
	@DisplayName("Relacionado a")
	public Agendainteracaorelacionado getAgendainteracaorelacionado() {
		return agendainteracaorelacionado;
	}
	public Agendainteracaosituacao getAgendainteracaosituacao() {
		return agendainteracaosituacao;
	}
	public List<Agendainteracaosituacao> getListaSituacao() {
		return listaSituacao;
	}
	public Empresa getEmpresahistorico() {
		return empresahistorico;
	}
	
	@DisplayName("Tipo de Pessoa")
	public Tipoagendainteracao getTipoagendainteracao() {
		return tipoagendainteracao;
	}

	public void setTipoagendainteracao(Tipoagendainteracao tipoagendainteracao) {
		this.tipoagendainteracao = tipoagendainteracao;
	}

	@DisplayName("Conta")
	public Contacrm getContacrm() {
		return contacrm;
	}
	
	@DisplayName("Contato")
	public String getContato() {
		return contato;
	}

	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setDtproximainteracaode(Date dtproximainteracaode) {
		this.dtproximainteracaode = dtproximainteracaode;
	}
	public void setDtproximainteracaoate(Date dtproximainteracaoate) {
		this.dtproximainteracaoate = dtproximainteracaoate;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setAgendainteracaorelacionado(
			Agendainteracaorelacionado agendainteracaorelacionado) {
		this.agendainteracaorelacionado = agendainteracaorelacionado;
	}
	public void setAgendainteracaosituacao(
			Agendainteracaosituacao agendainteracaosituacao) {
		this.agendainteracaosituacao = agendainteracaosituacao;
	}
	public void setListaSituacao(List<Agendainteracaosituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public Date getApuracaoPeriodoInicio() {
		return apuracaoPeriodoInicio;
	}
	public Date getApuracaoPeriodoFim() {
		return apuracaoPeriodoFim;
	}
	public void setApuracaoPeriodoInicio(Date apuracaoPeriodoInicio) {
		this.apuracaoPeriodoInicio = apuracaoPeriodoInicio;
	}
	public void setApuracaoPeriodoFim(Date apuracaoPeriodoFim) {
		this.apuracaoPeriodoFim = apuracaoPeriodoFim;
	}
	public void setEmpresahistorico(Empresa empresahistorico) {
		this.empresahistorico = empresahistorico;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}