package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.PropostaCaixaItem;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.geral.bean.Propostasituacao;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PropostaFiltro extends FiltroListagemSined {
	
	protected Integer numero;
	protected String sufixo;
	protected Cliente cliente;
	protected Money valorde;
	protected Money valorate;
	protected Date dtentregade;
	protected Date dtentregaate;
	protected Date dtvalidadede;
	protected Date dtvalidadeate;
	protected Uf uf;
	protected Municipio municipio;
	protected List<Propostasituacao> listaSituacao;
	protected Boolean simples;
	protected Projeto projeto;
	protected Propostacaixa propostaCaixa;
	protected PropostaCaixaItem propostaCaixaItem;
	protected String nomeItem;
	
	@MaxLength(9)
	public Integer getNumero() {
		return numero;
	}
	@MaxLength(2)
	public String getSufixo() {
		return sufixo;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Money getValorde() {
		return valorde;
	}
	public Money getValorate() {
		return valorate;
	}
	public Date getDtentregade() {
		return dtentregade;
	}
	public Date getDtentregaate() {
		return dtentregaate;
	}
	public Date getDtvalidadede() {
		return dtvalidadede;
	}
	public Date getDtvalidadeate() {
		return dtvalidadeate;
	}
	public Uf getUf() {
		return uf;
	}
	public Municipio getMunicipio() {
		return municipio;
	}
	public List<Propostasituacao> getListaSituacao() {
		return listaSituacao;
	}
	public Boolean getSimples() {
		return simples;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	
	
	
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setSimples(Boolean simples) {
		this.simples = simples;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public void setSufixo(String sufixo) {
		this.sufixo = sufixo;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setValorde(Money valorde) {
		this.valorde = valorde;
	}
	public void setValorate(Money valorate) {
		this.valorate = valorate;
	}
	public void setDtentregade(Date dtentregade) {
		this.dtentregade = dtentregade;
	}
	public void setDtentregaate(Date dtentregaate) {
		this.dtentregaate = dtentregaate;
	}
	public void setDtvalidadede(Date dtvalidadede) {
		this.dtvalidadede = dtvalidadede;
	}
	public void setDtvalidadeate(Date dtvalidadeate) {
		this.dtvalidadeate = dtvalidadeate;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setListaSituacao(List<Propostasituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	
	public Propostacaixa getPropostaCaixa() {
		return propostaCaixa;
	}
	public void setPropostaCaixa(Propostacaixa propostaCaixa) {
		this.propostaCaixa = propostaCaixa;
	}
	public PropostaCaixaItem getPropostaCaixaItem() {
		return propostaCaixaItem;
	}
	public void setPropostaCaixaItem(PropostaCaixaItem propostaCaixaItem) {
		this.propostaCaixaItem = propostaCaixaItem;
	}
	public String getNomeItem() {
		return nomeItem;
	}
	public void setNomeItem(String nomeItem) {
		this.nomeItem = nomeItem;
	}
}
