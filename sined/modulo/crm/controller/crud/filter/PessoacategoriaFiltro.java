package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PessoacategoriaFiltro extends FiltroListagemSined  {

	protected Boolean ativo = Boolean.TRUE;
	protected Categoria categoria;
	
	public Boolean getAtivo() {
		return ativo;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
}
