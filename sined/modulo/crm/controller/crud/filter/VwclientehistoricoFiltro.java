package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.AtividadetipoSituacaoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VwclientehistoricoFiltro extends FiltroListagemSined {
	private Categoria categoria;
	private Boolean ativo;
	private Cliente cliente;
	private Atividadetipo atividadetipo;
	private AtividadetipoSituacaoEnum situacao;
	private Boolean exibirInteracao = Boolean.TRUE;
	private Boolean exibirRequisicao = Boolean.TRUE;
	private Boolean exibirInteracaoVenda = Boolean.TRUE;
	private Date dtinteracaoinicio;
	private Date dtinteracaofim;
	private Empresa empresahistorico;
	
	public VwclientehistoricoFiltro(){
		this.setExibirInteracao(Boolean.TRUE);
		this.setExibirInteracaoVenda(Boolean.TRUE);
		this.setExibirRequisicao(Boolean.TRUE);
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	@DisplayName("Atividade")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	
	@DisplayName("Situa��o")
	public AtividadetipoSituacaoEnum getSituacao() {
		return situacao;
	}
	public void setSituacao(AtividadetipoSituacaoEnum situacao) {
		this.situacao = situacao;
	}

	@DisplayName("Intera��es")
	public Boolean getExibirInteracao() {
		verificaExibicao();
		return exibirInteracao;
	}
	public void setExibirInteracao(Boolean exibirInteracao) {
		this.exibirInteracao = exibirInteracao;
	}
	
	@DisplayName("Ordens de servi�o")
	public Boolean getExibirRequisicao() {
		verificaExibicao();
		return exibirRequisicao;
	}
	public void setExibirRequisicao(Boolean exibirRequisicao) {
		this.exibirRequisicao = exibirRequisicao;
	}
	
	@DisplayName("Intera��es de venda")
	public Boolean getExibirInteracaoVenda() {
		verificaExibicao();
		return exibirInteracaoVenda;
	}
	public void setExibirInteracaoVenda(Boolean exibirInteracaoVenda) {
		this.exibirInteracaoVenda = exibirInteracaoVenda;
	}

	public Date getDtinteracaoinicio() {
		return dtinteracaoinicio;
	}
	public Date getDtinteracaofim() {
		return dtinteracaofim;
	}

	public void setDtinteracaoinicio(Date dtinteracaoinicio) {
		this.dtinteracaoinicio = dtinteracaoinicio;
	}
	public void setDtinteracaofim(Date dtinteracaofim) {
		this.dtinteracaofim = dtinteracaofim;
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresahistorico() {
		return empresahistorico;
	}
	public void setEmpresahistorico(Empresa empresahistorico) {
		this.empresahistorico = empresahistorico;
	}
	
	private void verificaExibicao(){
		if((exibirInteracao == null || !exibirInteracao) && (exibirInteracaoVenda == null || !exibirInteracaoVenda) && (exibirRequisicao == null || !exibirRequisicao)){
			exibirInteracao = Boolean.TRUE;
			exibirInteracaoVenda = Boolean.TRUE;
			exibirRequisicao = Boolean.TRUE;
		}
	}
}
