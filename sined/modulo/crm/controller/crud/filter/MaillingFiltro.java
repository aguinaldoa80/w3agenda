package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MaillingFiltro extends FiltroListagemSined {

	protected Categoria categoria;
	protected Boolean ativo = Boolean.TRUE;
	protected String cliente;
	protected Boolean enviaUsuarioSenha;
	protected Mes aniversariantesMes;
	protected Integer aniversariantesDe;
	protected Integer aniversariantesAte;
	
	protected Colaborador colaborador;
	protected Colaborador colaboradorPrincipal;
	protected List<Segmento> listaSegmento;
	
	protected String email;
	protected String assunto;
	protected String mensagem;
	protected Arquivo anexo;
	
	protected Boolean enviaCopia;
	protected Boolean combinarSegmentos;
	protected Contatotipo contatotipo;
	
	@DisplayName("Categoria")
	public Categoria getCategoria() {
		return categoria;
	}
	@DisplayName("Situa��o")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Aniversariantes M�s")
	public Mes getAniversariantesMes() {
		return aniversariantesMes;
	}
	@DisplayName("Aniversariantes de")
	public Integer getAniversariantesDe() {
		return aniversariantesDe;
	}
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Vendedor Principal")
	public Colaborador getColaboradorPrincipal() {
		return colaboradorPrincipal;
	}
	@DisplayName("Segmento")
	public List<Segmento> getListaSegmento() {
		return listaSegmento;
	}
	@DisplayName("Aniversariantes at�")
	public Integer getAniversariantesAte() {
		return aniversariantesAte;
	}
	@DisplayName("Remetente")
	@MaxLength(100)
	public String getEmail() {
		return email;
	}
	@DisplayName("Assunto")
	@MaxLength(1000)
	public String getAssunto() {
		return assunto;
	}
	@DisplayName("Mensagem")
	@MaxLength(5000)
	public String getMensagem() {
		return mensagem;
	}
	@DisplayName("Anexo")
	public Arquivo getAnexo() {
		return anexo;
	}
	@MaxLength(100)
	@DisplayName("Cliente")
	public String getCliente() {
		return cliente;
	}
	@DisplayName("Envia usu�rio e senha")
	public Boolean getEnviaUsuarioSenha() {
		return enviaUsuarioSenha;
	}
	
	@DisplayName("Enviar c�pia da mensagem para o meu email")
	public Boolean getEnviaCopia() {
		return enviaCopia;
	}
	
	@DisplayName("Tipo de Contato")
	public Contatotipo getContatotipo() {
		return contatotipo;
	}
	@DisplayName("Combinar Segmentos")
	public Boolean getCombinarSegmentos() {
		return combinarSegmentos;
	}
	public void setContatotipo(Contatotipo contatotipo) {
		this.contatotipo = contatotipo;
	}
	public void setEnviaCopia(Boolean enviaCopia) {
		this.enviaCopia = enviaCopia;
	}
	public void setEnviaUsuarioSenha(Boolean enviaUsuarioSenha) {
		this.enviaUsuarioSenha = enviaUsuarioSenha;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public void setAnexo(Arquivo anexo) {
		this.anexo = anexo;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setAniversariantesMes(Mes aniversariantesMes) {
		this.aniversariantesMes = aniversariantesMes;
	}
	public void setAniversariantesDe(Integer aniversariantesDe) {
		this.aniversariantesDe = aniversariantesDe;
	}
	public void setAniversariantesAte(Integer aniversariantesAte) {
		this.aniversariantesAte = aniversariantesAte;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setColaboradorPrincipal(Colaborador colaboradorPrincipal) {
		this.colaboradorPrincipal = colaboradorPrincipal;
	}
	public void setListaSegmento(List<Segmento> listaSegmento) {
		this.listaSegmento = listaSegmento;
	}
	public void setCombinarSegmentos(Boolean combinarSegmentos) {
		this.combinarSegmentos = combinarSegmentos;
	}
}
