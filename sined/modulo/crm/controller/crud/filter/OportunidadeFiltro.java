package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadefonte;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Tipopessoacrm;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalizadorOportunidade;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.OportunidadesituacaoService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OportunidadeFiltro extends FiltroListagemSined{
	
	protected String nome;
	protected Date dtInicioinicio;
	protected Date dtIniciofim;
	//protected Integer ciclo;
	protected Colaborador colaborador;
	protected Fornecedor agencia;
	protected Empresa empresa;
	protected Tipopessoacrm tipopessoacrm;
	protected Cliente cliente;
	protected Contato contato;
	protected Propostacaixa propostacaixa;
	protected String selectedItens;
	protected Boolean exibirTotais = false;
	protected Segmento segmento;
	protected Concorrente concorrente;
	
	protected Oportunidade oportunidade;
	protected Integer cdoportunidade;
	
	protected List<Oportunidadesituacao> listaOportunidadesituacao = new ArrayList<Oportunidadesituacao>();
		
	protected Contacrm contacrm;
	protected Contacrmcontato contacrmcontato;
	
	protected Campanha campanha;
	protected String proximopasso;
	
	protected Integer probabilidadeinicio;
	protected Integer probabilidadefim;
	protected Date dtretornoinicio;
	protected Date dtretornofim;
	
	protected Oportunidadefonte oportunidadefonte;
	
	protected Integer iniciociclo;
	protected Integer fimciclo;
	
	protected Integer iniciointeracao;
	protected Integer fiminteracao;
	
	protected Uf uf;
	protected Municipio municipio;
	protected Material material;
	protected List<TotalizadorOportunidade> listaTotalizadorOportunidadeProjetoMaterial = new ArrayList<TotalizadorOportunidade>();	
	protected List<TotalizadorOportunidade> listaTotalizadorOportunidadesituacao = new ArrayList<TotalizadorOportunidade>();
	
	protected Date dtiniciohistorico;
	protected Date dtfimhistorico;
	
	public OportunidadeFiltro() {
		this.listaOportunidadesituacao = OportunidadesituacaoService.getInstance().findDefaultlistagem();
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if (ColaboradorService.getInstance().isColaborador(usuario.getCdpessoa())) {
			this.colaborador = ColaboradorService.getInstance().loadWithoutPermissao(new Colaborador(usuario.getCdpessoa()), "colaborador.cdpessoa, colaborador.nome");
		} else if (FornecedorService.getInstance().isFornecedor(usuario.getCdpessoa())){
			this.agencia = FornecedorService.getInstance().loadWithoutPermissao(new Fornecedor(usuario.getCdpessoa()), "fornecedor.cdpessoa, fornecedor.nome");
		}
	}
	
	@MaxLength(80)
	public String getNome() {
		return nome;
	}	
	public Date getDtInicioinicio() {
		return dtInicioinicio;
	}	
	public Date getDtIniciofim() {
		return dtIniciofim;
	}
//	@DisplayName("Ciclo")
//	public Integer getCiclo() {
//		return ciclo;
//	}
	@DisplayName("Respons�vel (Colaborador)")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Situa��o")
	public List<Oportunidadesituacao> getListaOportunidadesituacao() {
		return listaOportunidadesituacao;
	}
	
	@DisplayName("Conta")
	public Contacrm getContacrm() {
		return contacrm;
	}
	@DisplayName("Contato da Conta")
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}
	@DisplayName("Campanha")
	public Campanha getCampanha(){
		return campanha;
	}
	@DisplayName("Pr�ximo Passo")
	public String getProximopasso() {
		return proximopasso;
	}	
	@MaxLength(9)
	public Integer getProbabilidadeinicio() {
		return probabilidadeinicio;
	}
	@MaxLength(9)
	public Integer getProbabilidadefim() {
		return probabilidadefim;
	}
	public Date getDtretornoinicio() {
		return dtretornoinicio;
	}
	public Date getDtretornofim() {
		return dtretornofim;
	}
	@DisplayName("Fonte")
	public Oportunidadefonte getOportunidadefonte() {
		return oportunidadefonte;
	}
	public Integer getIniciociclo() {
		return iniciociclo;
	}
	public Integer getFimciclo() {
		return fimciclo;
	}
	@MaxLength(5)
	public Integer getIniciointeracao() {
		return iniciointeracao;
	}
	@MaxLength(5)
	public Integer getFiminteracao() {
		return fiminteracao;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Tipo Pessoa")
	public Tipopessoacrm getTipopessoacrm() {
		return tipopessoacrm;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Contato do cliente")
	public Contato getContato() {
		return contato;
	}
	public Oportunidade getOportunidade() {
		return oportunidade;
	}	
	@DisplayName("Caixa de Proposta")
	public Propostacaixa getPropostacaixa() {
		return propostacaixa;
	}
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDtInicioinicio(Date dtInicioinicio) {
		this.dtInicioinicio = dtInicioinicio;
	}
	public void setDtIniciofim(Date dtIniciofim) {
		this.dtIniciofim = dtIniciofim;
	}
//	public void setCiclo(Integer ciclo) {
//		this.ciclo = ciclo;
//	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setListaOportunidadesituacao(List<Oportunidadesituacao> listaOportunidadesituacao) {
		this.listaOportunidadesituacao = listaOportunidadesituacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}
	public void setCampanha(Campanha campanha){
		this.campanha = campanha;
	}
	public void setProximopasso(String proximopasso) {
		this.proximopasso = proximopasso;
	}
	public void setProbabilidadeinicio(Integer probabilidadeinicio) {
		this.probabilidadeinicio = probabilidadeinicio;
	}
	public void setProbabilidadefim(Integer probabilidadefim) {
		this.probabilidadefim = probabilidadefim;
	}
	public void setDtretornoinicio(Date dtretornoinicio) {
		this.dtretornoinicio = dtretornoinicio;
	}
	public void setDtretornofim(Date dtretornofim) {
		this.dtretornofim = dtretornofim;
	}
	public void setOportunidadefonte(Oportunidadefonte fonte) {
		this.oportunidadefonte = fonte;
	}
	public void setIniciociclo(Integer iniciociclo) {
		this.iniciociclo = iniciociclo;
	}
	public void setFimciclo(Integer fimciclo) {
		this.fimciclo = fimciclo;
	}
	public void setIniciointeracao(Integer iniciointeracao) {
		this.iniciointeracao = iniciointeracao;
	}
	public void setFiminteracao(Integer fiminteracao) {
		this.fiminteracao = fiminteracao;
	}
	public void setTipopessoacrm(Tipopessoacrm tipopessoacrm) {
		this.tipopessoacrm = tipopessoacrm;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}	
	public void setPropostacaixa(Propostacaixa propostacaixa) {
		this.propostacaixa = propostacaixa;
	}

	@DisplayName("Uf")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	@DisplayName("Produto/Servi�o")
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@DisplayName("Respons�vel (Ag�ncia)")
	public Fornecedor getAgencia() {
		return agencia;
	}

	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}


	public List<TotalizadorOportunidade> getListaTotalizadorOportunidadeProjetoMaterial() {
		return listaTotalizadorOportunidadeProjetoMaterial;
	}


	public List<TotalizadorOportunidade> getListaTotalizadorOportunidadesituacao() {
		return listaTotalizadorOportunidadesituacao;
	}


	public void setListaTotalizadorOportunidadeProjetoMaterial(
			List<TotalizadorOportunidade> listaTotalizadorOportunidadeProjetoMaterial) {
		this.listaTotalizadorOportunidadeProjetoMaterial = listaTotalizadorOportunidadeProjetoMaterial;
	}


	public void setListaTotalizadorOportunidadesituacao(
			List<TotalizadorOportunidade> listaTotalizadorOportunidadesituacao) {
		this.listaTotalizadorOportunidadesituacao = listaTotalizadorOportunidadesituacao;
	}

	@DisplayName("C�digo")
	public Integer getCdoportunidade() {
		return cdoportunidade;
	}


	public void setCdoportunidade(Integer cdoportunidade) {
		this.cdoportunidade = cdoportunidade;
	}

	public Date getDtiniciohistorico() {
		return dtiniciohistorico;
	}

	public Date getDtfimhistorico() {
		return dtfimhistorico;
	}

	public void setDtiniciohistorico(Date dtiniciohistorico) {
		this.dtiniciohistorico = dtiniciohistorico;
	}

	public void setDtfimhistorico(Date dtfimhistorico) {
		this.dtfimhistorico = dtfimhistorico;
	}

	public String getSelectedItens() {
		return selectedItens;
	}

	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}

	public Boolean getExibirTotais() {
		return exibirTotais;
	}

	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}
	
	public Segmento getSegmento() {
		return segmento;
	}
	
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	
	public Concorrente getConcorrente() {
		return concorrente;
	}
	
	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}
}
