package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContacrmFiltro extends FiltroListagemSined{
	
	protected String nome;
	protected Colaborador responsavel;
	protected Tiporesponsavel tiporesponsavel;
	protected Fornecedor agencia;
	protected Uf uf;
	protected Municipio municipio;
	protected Segmento segmento;
	protected Concorrente concorrente;
	protected Campanha campanha;
	
	public ContacrmFiltro(){
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if (ColaboradorService.getInstance().isColaborador(usuario.getCdpessoa())) {
			this.responsavel = ColaboradorService.getInstance().loadWithoutPermissao(new Colaborador(usuario.getCdpessoa()), "colaborador.cdpessoa, colaborador.nome");
			this.tiporesponsavel = Tiporesponsavel.COLABORADOR;
		} else if (FornecedorService.getInstance().isFornecedor(usuario.getCdpessoa())){
			this.tiporesponsavel = Tiporesponsavel.AGENCIA;
			this.agencia = FornecedorService.getInstance().loadWithoutPermissao(new Fornecedor(usuario.getCdpessoa()), "fornecedor.cdpessoa, fornecedor.nome");
		}
	}
	
	@MaxLength(80)
	public String getNome() {
		return nome;
	}
	@DisplayName("Respons�vel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public Segmento getSegmento() {
		return segmento;
	}
	public Campanha getCampanha() {
		return campanha;
	}
	
	@DisplayName("Tipo Respons�vel")
	public Tiporesponsavel getTiporesponsavel() {
		return tiporesponsavel;
	}

	@DisplayName("Ag�ncia")
	public Fornecedor getAgencia() {
		return agencia;
	}

	public void setTiporesponsavel(Tiporesponsavel tiporesponsavel) {
		this.tiporesponsavel = tiporesponsavel;
	}

	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}
	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}

	public Concorrente getConcorrente() {
		return concorrente;
	}

	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}
}
