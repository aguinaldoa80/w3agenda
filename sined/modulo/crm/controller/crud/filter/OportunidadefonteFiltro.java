package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OportunidadefonteFiltro extends FiltroListagemSined{
	
	protected String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
