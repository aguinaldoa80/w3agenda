package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ConselhoclasseprofissionalFiltro extends FiltroListagemSined {

	private String descricao;
	private String sigla;
	private Boolean ativo = Boolean.TRUE;
	
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Sigla")
	@MaxLength(20)
	public String getSigla() {
		return sigla;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}