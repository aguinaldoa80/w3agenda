package br.com.linkcom.sined.modulo.crm.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OportunidadesituacaoFiltro extends FiltroListagemSined{

	private String nome;

	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
