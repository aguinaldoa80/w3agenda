package br.com.linkcom.sined.modulo.crm.controller.report;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclienteetiquetaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/crm/relatorio/Etiqueta", authorizationModule=ReportAuthorizationModule.class)
public class EtiquetaReport extends SinedReport<VwclienteetiquetaFiltro>{

	private ClienteService clienteService;
	
	public void setClienteServic(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, VwclienteetiquetaFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		String isSelectedItensCdCliente = StringUtils.isNotBlank(request.getParameter("isSelectedItensCdCliente")) ? request.getParameter("isSelectedItensCdCliente") : "FALSE";
		
		if ("TRUE".equalsIgnoreCase(isSelectedItensCdCliente)) {
			filtro.setIsSelectedItensCdCliente(Boolean.TRUE);
		} else {
			filtro.setIsSelectedItensCdCliente(Boolean.FALSE);
		}
		
		return clienteService.createRelatorioEtiqueta(filtro, whereIn);
	}

	@Override
	public String getNomeArquivo() {
		return "etiqueta";
	}

	@Override
	public String getTitulo(VwclienteetiquetaFiltro filtro) {
		return "";
	}

}
