package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class AgendaInteracaoBean {
	
	private Integer codigo_cliente;
	private String nome_cliente;
	private String endereco_cliente;
	private String relacionado;
	private String material_servico;
	private String contrato;
	private Integer codigo_responsavel;
	private String nome_responsavel;
	private String proxima_interacao;
	private String periodicidade;
	private Integer frequencia;
	private String situacao;
	private String dia_mes_semana;
	
	public Integer getCodigo_cliente() {
		return codigo_cliente;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public String getEndereco_cliente() {
		return endereco_cliente;
	}
	public String getRelacionado() {
		return relacionado;
	}
	public String getMaterial_servico() {
		return material_servico;
	}
	public String getContrato() {
		return contrato;
	}
	public Integer getCodigo_responsavel() {
		return codigo_responsavel;
	}
	public String getNome_responsavel() {
		return nome_responsavel;
	}
	public String getProxima_interacao() {
		return proxima_interacao;
	}
	public String getPeriodicidade() {
		return periodicidade;
	}
	public Integer getFrequencia() {
		return frequencia;
	}
	public String getSituacao() {
		return situacao;
	}
	public String getDia_mes_semana() {
		return dia_mes_semana;
	}
	
	public void setCodigo_cliente(Integer codigo_cliente) {
		this.codigo_cliente = codigo_cliente;
	}
	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}
	public void setEndereco_cliente(String endereco_cliente) {
		this.endereco_cliente = endereco_cliente;
	}
	public void setRelacionado(String relacionado) {
		this.relacionado = relacionado;
	}
	public void setMaterial_servico(String material_servico) {
		this.material_servico = material_servico;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public void setCodigo_responsavel(Integer codigo_responsavel) {
		this.codigo_responsavel = codigo_responsavel;
	}
	public void setNome_responsavel(String nome_responsavel) {
		this.nome_responsavel = nome_responsavel;
	}
	public void setProxima_interacao(String proxima_interacao) {
		this.proxima_interacao = proxima_interacao;
	}
	public void setPeriodicidade(String periodicidade) {
		this.periodicidade = periodicidade;
	}
	public void setFrequencia(Integer frequencia) {
		this.frequencia = frequencia;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public void setDia_mes_semana(String dia_mes_semana) {
		this.dia_mes_semana = dia_mes_semana;
	}	
}