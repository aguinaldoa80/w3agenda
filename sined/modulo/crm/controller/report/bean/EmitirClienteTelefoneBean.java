package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class EmitirClienteTelefoneBean {

	private String telefone;
	private String telefonetipo;
	
	public EmitirClienteTelefoneBean() {}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getTelefonetipo() {
		return telefonetipo;
	}

	public void setTelefonetipo(String telefonetipo) {
		this.telefonetipo = telefonetipo;
	}
	
}
