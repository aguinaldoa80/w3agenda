package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class EmitirClienteVendedorBean {
	
	private String vendedor;

	public EmitirClienteVendedorBean() {}

	public String getVendedor() {
		return vendedor;
	}

	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}

}
