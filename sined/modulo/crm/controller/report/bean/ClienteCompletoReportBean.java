package br.com.linkcom.sined.modulo.crm.controller.report.bean;

import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Tipopessoa;

public class ClienteCompletoReportBean {
	
	private Integer cdcliente;
	private String nome_cliente;
	private String razaosocial_cliente;
	private Tipopessoa tipopessoa_cliente;
	private Cpf cpf_cliente;
	private Cnpj cnpj_cliente;
	private String email_cliente;
	private String telefones_cliente;
	private String categorias_cliente;
	
	private String nome_contato;
	private String municipio_contato;
	private String uf_contato;
	private String telefones_contato;
	private String email_contato;
	private String observacao_contato;
	
	public Integer getCdcliente() {
		return cdcliente;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public String getRazaosocial_cliente() {
		return razaosocial_cliente;
	}
	public Tipopessoa getTipopessoa_cliente() {
		return tipopessoa_cliente;
	}
	public Cpf getCpf_cliente() {
		return cpf_cliente;
	}
	public Cnpj getCnpj_cliente() {
		return cnpj_cliente;
	}
	public String getEmail_cliente() {
		return email_cliente;
	}
	public String getTelefones_cliente() {
		return telefones_cliente;
	}
	public String getCategorias_cliente() {
		return categorias_cliente;
	}
	public String getNome_contato() {
		return nome_contato;
	}
	public String getMunicipio_contato() {
		return municipio_contato;
	}
	public String getUf_contato() {
		return uf_contato;
	}
	public String getTelefones_contato() {
		return telefones_contato;
	}
	public String getEmail_contato() {
		return email_contato;
	}
	public String getObservacao_contato() {
		return observacao_contato;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setNome_cliente(String nomeCliente) {
		nome_cliente = nomeCliente;
	}
	public void setRazaosocial_cliente(String razaosocialCliente) {
		razaosocial_cliente = razaosocialCliente;
	}
	public void setTipopessoa_cliente(Tipopessoa tipopessoaCliente) {
		tipopessoa_cliente = tipopessoaCliente;
	}
	public void setCpf_cliente(Cpf cpfCliente) {
		cpf_cliente = cpfCliente;
	}
	public void setCnpj_cliente(Cnpj cnpjCliente) {
		cnpj_cliente = cnpjCliente;
	}
	public void setEmail_cliente(String emailCliente) {
		email_cliente = emailCliente;
	}
	public void setTelefones_cliente(String telefonesCliente) {
		telefones_cliente = telefonesCliente;
	}
	public void setCategorias_cliente(String categoriasCliente) {
		categorias_cliente = categoriasCliente;
	}
	public void setNome_contato(String nomeContato) {
		nome_contato = nomeContato;
	}
	public void setMunicipio_contato(String municipioContato) {
		municipio_contato = municipioContato;
	}
	public void setUf_contato(String ufContato) {
		uf_contato = ufContato;
	}
	public void setTelefones_contato(String telefonesContato) {
		telefones_contato = telefonesContato;
	}
	public void setEmail_contato(String emailContato) {
		email_contato = emailContato;
	}
	public void setObservacao_contato(String observacaoContato) {
		observacao_contato = observacaoContato;
	}
	
}
