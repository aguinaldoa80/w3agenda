package br.com.linkcom.sined.modulo.crm.controller.report.bean;

import java.util.LinkedList;

public class AgendaInteracaoClienteBean {
	
	private Integer codigo_cliente;
	private String nome_cliente;
	private LinkedList<AgendaInteracaoBean> agendas = new LinkedList<AgendaInteracaoBean>();
	
	public AgendaInteracaoClienteBean(Integer codigo_cliente, String nome_cliente){
		this.codigo_cliente = codigo_cliente;
		this.nome_cliente = nome_cliente;
	}
	public Integer getCodigo_cliente() {
		return codigo_cliente;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public LinkedList<AgendaInteracaoBean> getAgendas() {
		return agendas;
	}
	
	public void setCodigo_cliente(Integer codigo_cliente) {
		this.codigo_cliente = codigo_cliente;
	}
	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}
	public void setAgendas(LinkedList<AgendaInteracaoBean> agendas) {
		this.agendas = agendas;
	}
	
	@Override
	public boolean equals(Object obj) {
		return getCodigo_cliente().equals(((AgendaInteracaoClienteBean) obj).getCodigo_cliente());
	}
}