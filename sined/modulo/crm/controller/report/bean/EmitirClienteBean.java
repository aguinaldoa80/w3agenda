package br.com.linkcom.sined.modulo.crm.controller.report.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.LinkedList;

import br.com.linkcom.neo.types.Money;


public class EmitirClienteBean {

	private String identificador;
	private String nome;
	private String nacionalidade;
	private String pessoatratamento;
	private String email;
	private Boolean naoconsiderartaxaboleto;
	private Boolean discriminartaxaboleto;
	private Boolean discriminardescontonota;
	private Money taxapedidovenda;
	private Double creditolimitecompra;
	private String site;
	private String estadocivil;
	private String conjuge;
	private String profissaoconjuge;
	private String pai; 
	private String profissaopai;
	private String mae;
	private String profissaomae;
	private String observacao;
	private String infoadicionalcontrib;
	private String infoadicionalfisco;
	private String observacaoVenda;
	private Boolean ativo;
	private String tipopessoa;
	private String cnpj;
	private String razaosocial;
	private String inscricaoestadual;
	private String ufinscricaoestadual;
	private String cpf;
	private String inscricaomunicipal;
	private String crt;
	private Boolean incidiriss;
	private Boolean contabilidadecentralizada;
	private String tipoidentidade;
	private String rg;
	private String orgaoemissorrg;
	private Timestamp dtemissaorg;
	private String passaporte;
	private Date validadepassaporte;
	private String sexo;
	private Date dtnascimento;
	private String municipionaturalidadeuf;
	private String municipionaturalidade;
	private String grauinstrucao;
	private String clienteprofissao;
	private String responsavelFinanceiro;
	private String clienteindicacao;
	private String contagerencial;
	private String operacaocontabil;
	private String codigotributacao;
	private String ramoatividade;
	private String objetosocial;
	private String grupotributacao;
	private String banco;
	private String agencia;
	private String dvagencia;
	private String conta;
	private String dvconta;
	private String operacao;
	
	private LinkedList<EmitirClienteTelefoneBean> listaClienteTelefone = new LinkedList<EmitirClienteTelefoneBean>();
	private LinkedList<EmitirClienteContatoBean> listaClienteContato = new LinkedList<EmitirClienteContatoBean>();
	private LinkedList<EmitirClienteEnderecoBean> listaClienteEndereco = new LinkedList<EmitirClienteEnderecoBean>();
	private LinkedList<EmitirClienteRestricaoBean> listaClienteRestricao = new LinkedList<EmitirClienteRestricaoBean>();
	private LinkedList<EmitirClienteCategoriaBean> listaClienteCategoria = new LinkedList<EmitirClienteCategoriaBean>();
	private LinkedList<EmitirClienteVendedorBean> listaClienteVendedor = new LinkedList<EmitirClienteVendedorBean>();
	private LinkedList<EmitirClienteReferenciaBean> listaClienteReferencia = new LinkedList<EmitirClienteReferenciaBean>();
	
	public EmitirClienteBean() {}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getPessoatratamento() {
		return pessoatratamento;
	}

	public void setPessoatratamento(String pessoatratamento) {
		this.pessoatratamento = pessoatratamento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getNaoconsiderartaxaboleto() {
		return naoconsiderartaxaboleto;
	}

	public void setNaoconsiderartaxaboleto(Boolean naoconsiderartaxaboleto) {
		this.naoconsiderartaxaboleto = naoconsiderartaxaboleto;
	}

	public Boolean getDiscriminartaxaboleto() {
		return discriminartaxaboleto;
	}

	public void setDiscriminartaxaboleto(Boolean discriminartaxaboleto) {
		this.discriminartaxaboleto = discriminartaxaboleto;
	}

	public Boolean getDiscriminardescontonota() {
		return discriminardescontonota;
	}

	public void setDiscriminardescontonota(Boolean discriminardescontonota) {
		this.discriminardescontonota = discriminardescontonota;
	}

	public Money getTaxapedidovenda() {
		return taxapedidovenda;
	}

	public void setTaxapedidovenda(Money taxapedidovenda) {
		this.taxapedidovenda = taxapedidovenda;
	}

	public Double getCreditolimitecompra() {
		return creditolimitecompra;
	}

	public void setCreditolimitecompra(Double creditolimitecompra) {
		this.creditolimitecompra = creditolimitecompra;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getEstadocivil() {
		return estadocivil;
	}

	public void setEstadocivil(String estadocivil) {
		this.estadocivil = estadocivil;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getProfissaoconjuge() {
		return profissaoconjuge;
	}

	public void setProfissaoconjuge(String profissaoconjuge) {
		this.profissaoconjuge = profissaoconjuge;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getProfissaopai() {
		return profissaopai;
	}

	public void setProfissaopai(String profissaopai) {
		this.profissaopai = profissaopai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getProfissaomae() {
		return profissaomae;
	}

	public void setProfissaomae(String profissaomae) {
		this.profissaomae = profissaomae;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getInfoadicionalcontrib() {
		return infoadicionalcontrib;
	}

	public void setInfoadicionalcontrib(String infoadicionalcontrib) {
		this.infoadicionalcontrib = infoadicionalcontrib;
	}

	public String getInfoadicionalfisco() {
		return infoadicionalfisco;
	}

	public void setInfoadicionalfisco(String infoadicionalfisco) {
		this.infoadicionalfisco = infoadicionalfisco;
	}

	public String getObservacaoVenda() {
		return observacaoVenda;
	}

	public void setObservacaoVenda(String observacaoVenda) {
		this.observacaoVenda = observacaoVenda;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getTipopessoa() {
		return tipopessoa;
	}

	public void setTipopessoa(String tipopessoa) {
		this.tipopessoa = tipopessoa;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaosocial() {
		return razaosocial;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	public String getInscricaoestadual() {
		return inscricaoestadual;
	}

	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}

	public String getUfinscricaoestadual() {
		return ufinscricaoestadual;
	}

	public void setUfinscricaoestadual(String ufinscricaoestadual) {
		this.ufinscricaoestadual = ufinscricaoestadual;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}

	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}

	public String getCrt() {
		return crt;
	}

	public void setCrt(String crt) {
		this.crt = crt;
	}

	public Boolean getIncidiriss() {
		return incidiriss;
	}

	public void setIncidiriss(Boolean incidiriss) {
		this.incidiriss = incidiriss;
	}

	public Boolean getContabilidadecentralizada() {
		return contabilidadecentralizada;
	}

	public void setContabilidadecentralizada(Boolean contabilidadecentralizada) {
		this.contabilidadecentralizada = contabilidadecentralizada;
	}

	public String getTipoidentidade() {
		return tipoidentidade;
	}

	public void setTipoidentidade(String tipoidentidade) {
		this.tipoidentidade = tipoidentidade;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getOrgaoemissorrg() {
		return orgaoemissorrg;
	}

	public void setOrgaoemissorrg(String orgaoemissorrg) {
		this.orgaoemissorrg = orgaoemissorrg;
	}

	public Timestamp getDtemissaorg() {
		return dtemissaorg;
	}

	public void setDtemissaorg(Timestamp dtemissaorg) {
		this.dtemissaorg = dtemissaorg;
	}

	public String getPassaporte() {
		return passaporte;
	}

	public void setPassaporte(String passaporte) {
		this.passaporte = passaporte;
	}

	public Date getValidadepassaporte() {
		return validadepassaporte;
	}

	public void setValidadepassaporte(Date validadepassaporte) {
		this.validadepassaporte = validadepassaporte;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Date getDtnascimento() {
		return dtnascimento;
	}

	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}

	public String getMunicipionaturalidadeuf() {
		return municipionaturalidadeuf;
	}

	public void setMunicipionaturalidadeuf(String municipionaturalidadeuf) {
		this.municipionaturalidadeuf = municipionaturalidadeuf;
	}

	public String getMunicipionaturalidade() {
		return municipionaturalidade;
	}

	public void setMunicipionaturalidade(String municipionaturalidade) {
		this.municipionaturalidade = municipionaturalidade;
	}

	public String getGrauinstrucao() {
		return grauinstrucao;
	}

	public void setGrauinstrucao(String grauinstrucao) {
		this.grauinstrucao = grauinstrucao;
	}

	public String getClienteprofissao() {
		return clienteprofissao;
	}

	public void setClienteprofissao(String clienteprofissao) {
		this.clienteprofissao = clienteprofissao;
	}

	public String getResponsavelFinanceiro() {
		return responsavelFinanceiro;
	}

	public void setResponsavelFinanceiro(String responsavelFinanceiro) {
		this.responsavelFinanceiro = responsavelFinanceiro;
	}

	public String getClienteindicacao() {
		return clienteindicacao;
	}

	public void setClienteindicacao(String clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}

	public String getContagerencial() {
		return contagerencial;
	}

	public void setContagerencial(String contagerencial) {
		this.contagerencial = contagerencial;
	}

	public String getOperacaocontabil() {
		return operacaocontabil;
	}

	public void setOperacaocontabil(String operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}

	public String getCodigotributacao() {
		return codigotributacao;
	}

	public void setCodigotributacao(String codigotributacao) {
		this.codigotributacao = codigotributacao;
	}

	public String getRamoatividade() {
		return ramoatividade;
	}

	public void setRamoatividade(String ramoatividade) {
		this.ramoatividade = ramoatividade;
	}

	public String getObjetosocial() {
		return objetosocial;
	}

	public void setObjetosocial(String objetosocial) {
		this.objetosocial = objetosocial;
	}

	public String getGrupotributacao() {
		return grupotributacao;
	}

	public void setGrupotributacao(String grupotributacao) {
		this.grupotributacao = grupotributacao;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getDvagencia() {
		return dvagencia;
	}

	public void setDvagencia(String dvagencia) {
		this.dvagencia = dvagencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getDvconta() {
		return dvconta;
	}

	public void setDvconta(String dvconta) {
		this.dvconta = dvconta;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public LinkedList<EmitirClienteTelefoneBean> getListaClienteTelefone() {
		return listaClienteTelefone;
	}

	public void setListaClienteTelefone(
			LinkedList<EmitirClienteTelefoneBean> listaClienteTelefone) {
		this.listaClienteTelefone = listaClienteTelefone;
	}

	public LinkedList<EmitirClienteContatoBean> getListaClienteContato() {
		return listaClienteContato;
	}

	public void setListaClienteContato(
			LinkedList<EmitirClienteContatoBean> listaClienteContato) {
		this.listaClienteContato = listaClienteContato;
	}
	
	public LinkedList<EmitirClienteEnderecoBean> getListaClienteEndereco() {
		return listaClienteEndereco;
	}

	public void setListaClienteEndereco(
			LinkedList<EmitirClienteEnderecoBean> listaClienteEndereco) {
		this.listaClienteEndereco = listaClienteEndereco;
	}

	public LinkedList<EmitirClienteRestricaoBean> getListaClienteRestricao() {
		return listaClienteRestricao;
	}

	public void setListaClienteRestricao(
			LinkedList<EmitirClienteRestricaoBean> listaClienteRestricao) {
		this.listaClienteRestricao = listaClienteRestricao;
	}

	public LinkedList<EmitirClienteCategoriaBean> getListaClienteCategoria() {
		return listaClienteCategoria;
	}

	public void setListaClienteCategoria(
			LinkedList<EmitirClienteCategoriaBean> listaClienteCategoria) {
		this.listaClienteCategoria = listaClienteCategoria;
	}

	public LinkedList<EmitirClienteVendedorBean> getListaClienteVendedor() {
		return listaClienteVendedor;
	}

	public void setListaClienteVendedor(
			LinkedList<EmitirClienteVendedorBean> listaClienteVendedor) {
		this.listaClienteVendedor = listaClienteVendedor;
	}

	public LinkedList<EmitirClienteReferenciaBean> getListaClienteReferencia() {
		return listaClienteReferencia;
	}

	public void setListaClienteReferencia(
			LinkedList<EmitirClienteReferenciaBean> listaClienteReferencia) {
		this.listaClienteReferencia = listaClienteReferencia;
	}
	
}
