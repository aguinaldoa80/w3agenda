package br.com.linkcom.sined.modulo.crm.controller.report.bean;

import java.util.LinkedList;

public class AgendaInteracaoResponsavelBean {
	
	private Integer codigo_responsavel;
	private String responsavel;
	private LinkedList<AgendaInteracaoBean> agendas = new LinkedList<AgendaInteracaoBean>();
	
	public AgendaInteracaoResponsavelBean(Integer codigo_responsavel, String responsavel){
		this.codigo_responsavel = codigo_responsavel;
		this.responsavel = responsavel;
	}
	
	public Integer getCodigo_responsavel() {
		return codigo_responsavel;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public LinkedList<AgendaInteracaoBean> getAgendas() {
		return agendas;
	}
	
	public void setCodigo_responsavel(Integer codigo_responsavel) {
		this.codigo_responsavel = codigo_responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setAgendas(LinkedList<AgendaInteracaoBean> agendas) {
		this.agendas = agendas;
	}
	
	@Override
	public boolean equals(Object obj) {
		return getCodigo_responsavel().equals(((AgendaInteracaoResponsavelBean) obj).getCodigo_responsavel());
	}
}
