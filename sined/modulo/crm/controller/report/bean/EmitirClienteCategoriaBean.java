package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class EmitirClienteCategoriaBean {
	
	private String categoria;

	public EmitirClienteCategoriaBean() {}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	
}
