package br.com.linkcom.sined.modulo.crm.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Telefone;

public class TelefoneBean {

	private String telefone;
	private String tipo_telefone;
	
	public TelefoneBean() {}
	
	public TelefoneBean(Telefone telefone) {
		this.telefone = telefone != null ? telefone.getTelefone() : "";
		this.tipo_telefone = telefone != null && telefone.getTelefonetipo() != null ? telefone.getTelefonetipo().getNome() : ""; 
	}

	public String getTelefone() {
		return telefone;
	}

	public String getTipo_telefone() {
		return tipo_telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setTipo_telefone(String tipoTelefone) {
		tipo_telefone = tipoTelefone;
	}
}
