package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class EmitirClienteEnderecoBean {

	private String enderecotipo;
	private String cep;
	private String logradouro;
	private String numero;
	private String bairro;
	private String complemento;
	private String municipiouf;
	private String municipio;
	private String caixapostal;
	private String pais;
	private String pontoreferencia;
	private Integer distancia;
	
	public EmitirClienteEnderecoBean() {}

	public String getEnderecotipo() {
		return enderecotipo;
	}

	public void setEnderecotipo(String enderecotipo) {
		this.enderecotipo = enderecotipo;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getMunicipiouf() {
		return municipiouf;
	}

	public void setMunicipiouf(String municipiouf) {
		this.municipiouf = municipiouf;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCaixapostal() {
		return caixapostal;
	}

	public void setCaixapostal(String caixapostal) {
		this.caixapostal = caixapostal;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getPontoreferencia() {
		return pontoreferencia;
	}

	public void setPontoreferencia(String pontoreferencia) {
		this.pontoreferencia = pontoreferencia;
	}

	public Integer getDistancia() {
		return distancia;
	}

	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}
	
	

}
