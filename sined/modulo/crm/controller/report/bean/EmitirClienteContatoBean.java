package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class EmitirClienteContatoBean {

	private String nome;
	private String telefones;
	private String contatotipo;
	private String email;
	private Boolean receberboleto;
	private Boolean responsavelos;
	
	public EmitirClienteContatoBean() {}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefones() {
		return telefones;
	}

	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}

	public String getContatotipo() {
		return contatotipo;
	}

	public void setContatotipo(String contatotipo) {
		this.contatotipo = contatotipo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getReceberboleto() {
		return receberboleto;
	}

	public void setReceberboleto(Boolean receberboleto) {
		this.receberboleto = receberboleto;
	}

	public Boolean getResponsavelos() {
		return responsavelos;
	}

	public void setResponsavelos(Boolean responsavelos) {
		this.responsavelos = responsavelos;
	}

}
