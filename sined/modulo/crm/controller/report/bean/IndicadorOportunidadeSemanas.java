package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class IndicadorOportunidadeSemanas {
	
	protected Integer numeroSemana;
	protected Integer numeroInteracoes;
	protected String whereInCdOportunidadeHistorico;
	protected String mesCorrente;
	protected String anoCorrente;
	
	public Integer getNumeroSemana() {
		return numeroSemana;
	}
	public void setNumeroSemana(Integer numeroSemana) {
		this.numeroSemana = numeroSemana;
	}
	public Integer getNumeroInteracoes() {
		return numeroInteracoes;
	}
	public void setNumeroInteracoes(Integer numeroInteracoes) {
		this.numeroInteracoes = numeroInteracoes;
	}
	public String getWhereInCdOportunidadeHistorico() {
		return whereInCdOportunidadeHistorico;
	}
	public void setWhereInCdOportunidadeHistorico(
			String whereInCdOportunidadeHistorico) {
		this.whereInCdOportunidadeHistorico = whereInCdOportunidadeHistorico;
	}
	public String getMesCorrente() {
		return mesCorrente;
	}
	public void setMesCorrente(String mesCorrente) {
		this.mesCorrente = mesCorrente;
	}
	public String getAnoCorrente() {
		return anoCorrente;
	}
	public void setAnoCorrente(String anoCorrente) {
		this.anoCorrente = anoCorrente;
	}
}
