package br.com.linkcom.sined.modulo.crm.controller.report.bean;

import java.sql.Date;

public class EmitirClienteRestricaoBean {

	private Date dtrestricao;
	private String motivorestricao;
	private Date dtliberacao;
	private String motivoliberacao;
	
	public EmitirClienteRestricaoBean() {}

	public Date getDtrestricao() {
		return dtrestricao;
	}

	public void setDtrestricao(Date dtrestricao) {
		this.dtrestricao = dtrestricao;
	}

	public String getMotivorestricao() {
		return motivorestricao;
	}

	public void setMotivorestricao(String motivorestricao) {
		this.motivorestricao = motivorestricao;
	}

	public Date getDtliberacao() {
		return dtliberacao;
	}

	public void setDtliberacao(Date dtliberacao) {
		this.dtliberacao = dtliberacao;
	}

	public String getMotivoliberacao() {
		return motivoliberacao;
	}

	public void setMotivoliberacao(String motivoliberacao) {
		this.motivoliberacao = motivoliberacao;
	}
}
