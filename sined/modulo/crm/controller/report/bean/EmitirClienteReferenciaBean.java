package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class EmitirClienteReferenciaBean {

	private String clienterelacaotipo;
	private String clienteoutro;
	
	public EmitirClienteReferenciaBean() {}

	public String getClienterelacaotipo() {
		return clienterelacaotipo;
	}

	public void setClienterelacaotipo(String clienterelacaotipo) {
		this.clienterelacaotipo = clienterelacaotipo;
	}

	public String getClienteoutro() {
		return clienteoutro;
	}

	public void setClienteoutro(String clienteoutro) {
		this.clienteoutro = clienteoutro;
	}
}
