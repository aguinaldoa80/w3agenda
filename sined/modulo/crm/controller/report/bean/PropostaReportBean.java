package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class PropostaReportBean {
	
	private String cliente;
	private Integer ganhas;
	private Integer elaboracao;
	private Integer perdidas;
	private Integer aguardando;
	private Integer suspensa;
	private Integer total;
	
	public PropostaReportBean() {
		ganhas = 0;
		elaboracao = 0;
		perdidas = 0;
		aguardando = 0;
		suspensa = 0;
		total = 0;
	}
	
	public String getCliente() {
		return cliente;
	}
	public Integer getGanhas() {
		return ganhas;
	}
	public Integer getElaboracao() {
		return elaboracao;
	}
	public Integer getPerdidas() {
		return perdidas;
	}
	public Integer getAguardando() {
		return aguardando;
	}
	public Integer getSuspensa() {
		return suspensa;
	}
	public Integer getTotal() {
		return total;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public void setGanhas(Integer ganhas) {
		this.ganhas = ganhas;
	}
	public void setElaboracao(Integer elaboracao) {
		this.elaboracao = elaboracao;
	}
	public void setPerdidas(Integer perdidas) {
		this.perdidas = perdidas;
	}
	public void setAguardando(Integer aguardando) {
		this.aguardando = aguardando;
	}
	public void setSuspensa(Integer suspensa) {
		this.suspensa = suspensa;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	
	public void adicionaGanhas(){
		ganhas++;
	}
	public void adicionaElaboracao(){
		elaboracao++;
	}
	public void adicionaAguardando(){
		aguardando++;
	}
	public void adicionaSuspensa(){
		suspensa++;
	}
	public void adicionaPerdidas(){
		perdidas++;
	}
	public void adicionaTotal(){
		total++;
	}
}
