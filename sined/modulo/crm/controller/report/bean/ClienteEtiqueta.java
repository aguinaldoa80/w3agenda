package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class ClienteEtiqueta {

	protected String nomea;
	protected String endereco;
	protected String cep;
	protected String cidade;
	protected String bairro;
	protected String uf;
	protected String pontoreferencia;
	protected String nomecontato;
	protected String cpf;
	protected String cnpj;
	protected String data_de_nascimento;
	protected String estado_civil;
	protected String cliente_profissao;
	protected String sexo;
	protected String identificador;
	protected String listaTelefone;
	protected String nomeEmpresa;
	protected String razao_social;
	protected String logomarca_principal_sistema;

	//|..GET..|
	public String getNomea() {
		return nomea;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getCep() {
		return cep;
	}
	public String getCidade() {
		return cidade;
	}
	public String getBairro() {
		return bairro;
	}
	public String getUf() {
		return uf;
	}
	public String getPontoreferencia() {
		return pontoreferencia;
	}
	public String getNomecontato() {
		return nomecontato;
	}
	public String getCpf() {
		return cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getData_de_nascimento() {
		return data_de_nascimento;
	}
	public String getEstado_civil() {
		return estado_civil;
	}
	public String getCliente_profissao() {
		return cliente_profissao;
	}
	public String getSexo() {
		return sexo;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getListaTelefone() {
		return listaTelefone;
	}
	public String getNomeEmpresa() {
		return nomeEmpresa;
	}
	public String getRazao_social() {
		return razao_social;
	}
	public String getLogomarca_principal_sistema() {
		return logomarca_principal_sistema;
	}
	//|..SET..|
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setData_de_nascimento(String dataDeNascimento) {
		data_de_nascimento = dataDeNascimento;
	}
	public void setEstado_civil(String estadoCivil) {
		estado_civil = estadoCivil;
	}
	public void setCliente_profissao(String clienteProfissao) {
		cliente_profissao = clienteProfissao;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setListaTelefone(String listaTelefone) {
		this.listaTelefone = listaTelefone;
	}
	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}
	public void setRazao_social(String razaoSocial) {
		razao_social = razaoSocial;
	}
	public void setLogomarca_principal_sistema(String logomarcaPrincipalSistema) {
		logomarca_principal_sistema = logomarcaPrincipalSistema;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setNomea(String nomea) {
		this.nomea = nomea;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setPontoreferencia(String pontoreferencia) {
		this.pontoreferencia = pontoreferencia;
	}
	public void setNomecontato(String nomecontato) {
		this.nomecontato = nomecontato;
	}
	
}