package br.com.linkcom.sined.modulo.crm.controller.report.bean;

public class IndicadorOportunidadeMes {
	
	protected String mesAnoCorrente;
	protected Integer total;
	protected String whereInCdOportunidadeHistorico;

	public String getMesAnoCorrente() {
		return mesAnoCorrente;
	}
	public void setMesAnoCorrente(String mesAnoCorrente) {
		this.mesAnoCorrente = mesAnoCorrente;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getWhereInCdOportunidadeHistorico() {
		return whereInCdOportunidadeHistorico;
	}
	public void setWhereInCdOportunidadeHistorico(
			String whereInCdOportunidadeHistorico) {
		this.whereInCdOportunidadeHistorico = whereInCdOportunidadeHistorico;
	}
}
