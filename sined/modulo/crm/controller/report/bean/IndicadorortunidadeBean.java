package br.com.linkcom.sined.modulo.crm.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;



public class IndicadorortunidadeBean {
	
	protected Integer cdOportunidadeHistorico;
	protected String nome;
	protected Money valor;
	protected String nomeResponsavel;
	protected Date data;
	protected Date inicio;
	protected String status;
	protected String diferencaDias;
	protected String dataFormatada;
	
	public IndicadorortunidadeBean() {
	}
	
	public IndicadorortunidadeBean(String _nome, Money _valor) {
		this.nome = _nome;
		this.valor = _valor;
	}

	public IndicadorortunidadeBean(Integer _cdOportunidadeHistorico, String _nome, Double _valor, String _nomeResponsavel, 
			Date _data, Date _inicio, String _status) {
		
		this.cdOportunidadeHistorico = _cdOportunidadeHistorico;
		this.nome = _nome;
		this.valor = new Money(_valor, true);
		this.nomeResponsavel = _nomeResponsavel;
		this.data = _data;
		this.inicio = _inicio;
		this.status = _status;
	}
	
	public Integer getCdOportunidadeHistorico() {
		return cdOportunidadeHistorico;
	}

	public void setCdOportunidadeHistorico(Integer cdOportunidadeHistorico) {
		this.cdOportunidadeHistorico = cdOportunidadeHistorico;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public String getNomeResponsavel() {
		return nomeResponsavel;
	}

	public void setNomeResponsavel(String nomeResponsavel) {
		this.nomeResponsavel = nomeResponsavel;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDiferencaDias() {
		return diferencaDias;
	}

	public void setDiferencaDias(String diferencaDias) {
		this.diferencaDias = diferencaDias;
	}

	public String getDataFormatada() {
		return dataFormatada;
	}

	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}
}
