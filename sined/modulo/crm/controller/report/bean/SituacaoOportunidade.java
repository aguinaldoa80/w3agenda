package br.com.linkcom.sined.modulo.crm.controller.report.bean;

import java.util.List;

public class SituacaoOportunidade {

	protected String whereInCdOportunidadeHistorico;
	protected String descricao;
	protected List<IndicadorOportunidadeSemanas> listaSemanas;
	protected Integer total;
	protected List<IndicadorOportunidadeMes> listaMes;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<IndicadorOportunidadeSemanas> getListaSemanas() {
		return listaSemanas;
	}
	public void setListaSemanas(List<IndicadorOportunidadeSemanas> listaSemanas) {
		this.listaSemanas = listaSemanas;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getWhereInCdOportunidadeHistorico() {
		return whereInCdOportunidadeHistorico;
	}
	public void setWhereInCdOportunidadeHistorico(
			String whereInCdOportunidadeHistorico) {
		this.whereInCdOportunidadeHistorico = whereInCdOportunidadeHistorico;
	}
	public List<IndicadorOportunidadeMes> getListaMes() {
		return listaMes;
	}
	public void setListaMes(List<IndicadorOportunidadeMes> listaMes) {
		this.listaMes = listaMes;
	}
}
