package br.com.linkcom.sined.modulo.crm.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
		path = "/crm/relatorio/Cliente",
		authorizationModule=ReportAuthorizationModule.class
	)
public class ClienteReport extends SinedReport<ClienteFiltro> {
	
	@Override
	public IReport createReportSined(WebRequestContext request,	ClienteFiltro filtro) throws Exception {
		return getClienteService().createRelatorioCliente(filtro);
	}
	
	@Override
	public String getTitulo(ClienteFiltro filtro) {
		return "RELATÓRIO DE CLIENTES";
	}
	
	@Override
	public String getNomeArquivo() {
		return "clientes";
	}

}
