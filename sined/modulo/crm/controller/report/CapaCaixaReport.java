package br.com.linkcom.sined.modulo.crm.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.PropostacaixaService;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/crm/relatorio/CapaCaixa", authorizationModule=ReportAuthorizationModule.class)
public class CapaCaixaReport extends SinedReport<Object> {
	private PropostacaixaService propostacaixaService;
	
	public void setPropostacaixaService(PropostacaixaService propostacaixaService) {
		this.propostacaixaService = propostacaixaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, Object filtro) throws Exception {
		String caixasSelecionadas = request.getParameter("selectedItens");
		return propostacaixaService.gerarRelatorioCapaCaixa(caixasSelecionadas);
	}
	
	@Override
	public String getTitulo(Object filtro) {
		return null;
	}
	
	@Override
	public String getNomeArquivo() {
		return "capacaixa";
	}
}
