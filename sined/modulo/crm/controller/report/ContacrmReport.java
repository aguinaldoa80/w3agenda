package br.com.linkcom.sined.modulo.crm.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ContacrmFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
		path = "/crm/relatorio/Contacrm",
		authorizationModule=ReportAuthorizationModule.class
	)
public class ContacrmReport extends SinedReport<ContacrmFiltro> {
	
	protected ContacrmService contacrmService;
	
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	
	
	
	@Override
	public IReport createReportSined(WebRequestContext request,	ContacrmFiltro filtro) throws Exception {
		return contacrmService.createRelatorioContascrm(filtro);
	}
	
	@Override
	public String getTitulo(ContacrmFiltro filtro) {
		return "RELATÓRIO DE CONTAS";
	}
	
	@Override
	public String getNomeArquivo() {
		return "contacrm";
	}

}
