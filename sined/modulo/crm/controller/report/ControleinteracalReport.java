package br.com.linkcom.sined.modulo.crm.controller.report;

import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.InteracaoService;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.ControleInteracaoBean;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
		path = "/crm/relatorio/Controleinteracao",
		authorizationModule=ReportAuthorizationModule.class
	)
public class ControleinteracalReport extends SinedReport<ControleInteracaoBean> {
	private InteracaoService interacaoService;
	
	public void setInteracaoService(InteracaoService interacaoService) {
		this.interacaoService = interacaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	ControleInteracaoBean filtro) throws Exception {
			Report report = new Report("/crm/controleinteracao");
			
			List<ControleInteracaoBean> lista = interacaoService.getListaControleInteracao(request, filtro);
			
			report.setDataSource(lista);
			return report;
	}
	
	@Override
	public String getTitulo(ControleInteracaoBean filtro) {
		return "RELAT�RIO DE CONTROLE DE INTERA��O";
	}
	
	@Override
	public String getNomeArquivo() {
		return "controle de intera��o";
	}

}
