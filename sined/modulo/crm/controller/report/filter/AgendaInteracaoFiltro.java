package br.com.linkcom.sined.modulo.crm.controller.report.filter;

import java.util.Date;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;

public class AgendaInteracaoFiltro extends ReportTemplateFiltro {
	
	private String selectedItens;
	private ReportTemplateBean reportTemplate;
	private Date apuracaoPeriodoInicio;
	private Date apuracaoPeriodoFim;
	
	public String getSelectedItens() {
		return selectedItens;
	}
	public ReportTemplateBean getReportTemplate() {
		return reportTemplate;
	}
	public Date getApuracaoPeriodoInicio() {
		return apuracaoPeriodoInicio;
	}
	public Date getApuracaoPeriodoFim() {
		return apuracaoPeriodoFim;
	}
	
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
	public void setReportTemplate(ReportTemplateBean reportTemplate) {
		this.reportTemplate = reportTemplate;
	}
	public void setApuracaoPeriodoInicio(Date apuracaoPeriodoInicio) {
		this.apuracaoPeriodoInicio = apuracaoPeriodoInicio;
	}
	public void setApuracaoPeriodoFim(Date apuracaoPeriodoFim) {
		this.apuracaoPeriodoFim = apuracaoPeriodoFim;
	}		
}