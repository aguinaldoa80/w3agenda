package br.com.linkcom.sined.modulo.crm.controller.report.filter;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class IndicadoroportunidadeFiltro extends FiltroListagemSined{
	
	protected Projeto projeto;
	protected String dtPeriodoDe;
	protected String dtPeriodoAte;
	protected Date dtPeriodo;
	protected Material material;
	protected Colaborador colaborador;
	
	public IndicadoroportunidadeFiltro() {
		DateFormat format = new SimpleDateFormat("MM/yyyy");
		this.dtPeriodoDe = format.format(new Timestamp(System.currentTimeMillis()));
		this.dtPeriodoAte = format.format(new Timestamp(System.currentTimeMillis()));
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	public String getDtPeriodoDe() {
		return dtPeriodoDe;
	}
	@Required
	public String getDtPeriodoAte() {
		return dtPeriodoAte;
	}
	
	public Date getDtref1(){
		if(dtPeriodoDe != null){
			try {
				Date data = new Date(new SimpleDateFormat("MM/yyyy").parse(dtPeriodoDe).getTime());
				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(data);
				calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
				
				return new Date(calendar.getTimeInMillis());
			} catch (ParseException e) {
				return null;
			}
		} else return null;
	}
	
	public Date getDtref2(){
		if(dtPeriodoAte != null){
			try {
				Date data = new Date(new SimpleDateFormat("MM/yyyy").parse(dtPeriodoAte).getTime());
				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(data);
				calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				
				return new Date(calendar.getTimeInMillis());
			} catch (ParseException e) {
				return null;
			}
		} else return null;
	}
	
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDtPeriodoDe(String dtPeriodoDe) {
		this.dtPeriodoDe = dtPeriodoDe;
	}
	public void setDtPeriodoAte(String dtPeriodoAte) {
		this.dtPeriodoAte = dtPeriodoAte;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

}
