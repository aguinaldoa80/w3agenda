package br.com.linkcom.sined.modulo.crm.controller.report.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Material;

public class EmitirfunilvendasFiltro {
	protected Date dtinicio;
	protected Date dtfim;
	protected Colaborador colaborador;
	protected Material material;
	protected Boolean produto;
	protected Boolean servico;
	
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Material getMaterial() {
		return material;
	}
	public Boolean getProduto() {
		return produto;
	}
	@DisplayName("Servi�o")
	public Boolean getServico() {
		return servico;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setProduto(Boolean produto) {
		this.produto = produto;
	}
	public void setServico(Boolean servico) {
		this.servico = servico;
	}
}
