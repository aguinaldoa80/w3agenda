package br.com.linkcom.sined.modulo.crm.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.LeadFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
		path = "/crm/relatorio/Lead",
		authorizationModule=ReportAuthorizationModule.class
	)
public class LeadReport extends SinedReport<LeadFiltro> {
	
	protected LeadService leadService;
	
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	
	
	@Override
	public IReport createReportSined(WebRequestContext request,	LeadFiltro filtro) throws Exception {
		return leadService.createRelatorioLead(filtro);
	}
	
	@Override
	public String getTitulo(LeadFiltro filtro) {
		return "RELATÓRIO DE LEADS";
	}
	
	@Override
	public String getNomeArquivo() {
		return "leads";
	}

}
