package br.com.linkcom.sined.modulo.crm.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.PropostaFiltro;

@Controller(path = "/crm/relatorio/Proposta", authorizationModule=ReportAuthorizationModule.class)
public class PropostaReport extends ResourceSenderController<PropostaFiltro>{

	private PropostaService propostaService;

	public void setPropostaService(PropostaService propostaService) {
		this.propostaService = propostaService;
	}

	@Override
	public Resource generateResource(WebRequestContext request, PropostaFiltro filtro) throws Exception {
		return  propostaService.createRelatorioProposta(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, PropostaFiltro filtro) throws Exception {
		return null;
	}

}
