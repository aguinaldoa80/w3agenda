package br.com.linkcom.sined.modulo.crm.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.LeadFiltro;

@Controller(path = "/crm/relatorio/LeadCSV", authorizationModule=ReportAuthorizationModule.class)
public class LeadCSVReport extends ResourceSenderController<LeadFiltro>{

	private LeadService leadService;
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,
			LeadFiltro filtro) throws Exception {
		return leadService.gerarRelatorioCSVListagem(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, LeadFiltro filtro)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
