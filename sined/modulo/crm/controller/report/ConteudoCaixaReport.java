package br.com.linkcom.sined.modulo.crm.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PropostacaixaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Controller(path = "/crm/relatorio/ConteudoCaixa",authorizationModule=ReportAuthorizationModule.class)
public class ConteudoCaixaReport extends SinedReport<PropostacaixaFiltro>{
	private PropostaService propostaService;
	
	public void setPropostaService(PropostaService propostaService) {
		this.propostaService = propostaService;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	PropostacaixaFiltro filtro) throws ResourceGenerationException {		
		return new ModelAndView("redirect:/sistema/crud/PropostaCaixa");
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	PropostacaixaFiltro filtro) throws Exception {
		return propostaService.createConteudoCaixaReport(request.getParameter("selectedItens"));
	}

	@Override
	public String getTitulo(PropostacaixaFiltro filtro) {
		return "RELAT�RIO DE CONTE�DO DE CAIXA";
	}
	
	@Override
	public String getNomeArquivo() {
		return "conteudocaixa";
	}

}
