package br.com.linkcom.sined.modulo.crm.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/crm/relatorio/ClienteCompleto", authorizationModule=ReportAuthorizationModule.class)
public class ClienteCompletoReport extends SinedReport<ClienteFiltro>{
	
	private ClienteService clienteService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, ClienteFiltro filtro) throws Exception {
		return clienteService.createRelatorioClienteCompleto(request);
	}

	@Override
	public String getNomeArquivo() {
		return "clientecompleto";
	}

	@Override
	public String getTitulo(ClienteFiltro filtro) {
		return "RELATÓRIO COMPLETO DE CLIENTE";
	}
	
	public ModelAndView ajaxContatosSelecionados(WebRequestContext request){
		JsonModelAndView json = new JsonModelAndView();
		String s = request.getParameter("selectedItens");
		request.getSession().setAttribute("lista_contatos_selecionados_pdf", s);
		return json;
	}

}
