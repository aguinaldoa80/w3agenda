package br.com.linkcom.sined.modulo.crm.controller.report;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.EmitirfunilvendasFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/crm/relatorio/EmitirfunilvendasExcel", authorizationModule=ProcessAuthorizationModule.class)
public class EmitirfunilvendasExcel extends ResourceSenderController<EmitirfunilvendasFiltro>{
	
	private OportunidadeService oportunidadeService;
	
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, EmitirfunilvendasFiltro filtro) throws Exception {
		SinedUtil.setCookieBlockButton(request);
		return oportunidadeService.gerarExcelEmitirfunilvendas(filtro); 
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,	EmitirfunilvendasFiltro filtro) throws Exception {
		return  new ModelAndView("relatorio/emitirfunilvendas").addObject("filtro", filtro);
	}
	
	public Resource generateResourceSintetico(WebRequestContext request, EmitirfunilvendasFiltro filtro) throws Exception {
		SinedUtil.setCookieBlockButton(request);
		return oportunidadeService.gerarExcelEmitirfunilvendasSintetico(filtro); 
	}
	
	@Action("gerarSintetico")
	public ModelAndView doGerarSintetico(WebRequestContext request, EmitirfunilvendasFiltro filtro) throws Exception {
		HttpServletResponse response = request.getServletResponse();
		Resource recurso = generateResourceSintetico(request, filtro);
		if (recurso == null) {
			return goToAction(FILTRO);
		}
		response.setContentType(recurso.getContentType());
		
		response.addHeader("Content-Disposition", "attachment; filename=\"" + recurso.getFileName() + "\";");
		response.getOutputStream().write(recurso.getContents());
		return null;
	}
	
}
