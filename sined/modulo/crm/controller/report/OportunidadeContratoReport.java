package br.com.linkcom.sined.modulo.crm.controller.report;

import java.util.LinkedHashMap;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.rtf.bean.OportunidadeBean;

@Controller(path = "/crm/relatorio/OportunidadeContrato")
public class OportunidadeContratoReport extends ReportTemplateController<ReportTemplateFiltro> {

	private OportunidadeService oportunidadeService;

	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}


	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.OPORTUNIDADE;
	}

	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {	
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		OportunidadeBean oportunidadeRTF = oportunidadeService.makeOportunidadeRTF(new Oportunidade(Integer.parseInt(request.getParameter("cdoportunidade"))));
		map.put("oportunidade", oportunidadeRTF);
		return map;
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return getReportTemplateService().load(new ReportTemplateBean(Integer.parseInt(request.getParameter("cdreporttemplate"))));
	}

	@Override
	protected String getFileName() {
		return "oportunidadecontrato";
	}

	@Override
	public ModelAndView doGerar(WebRequestContext request,
			ReportTemplateFiltro filtro) throws Exception {
		String cdoportunidade = request.getParameter("cdoportunidade");
		if(Util.strings.isNotEmpty(cdoportunidade) && !oportunidadeService.isUsuarioPodeEditarOportunidades(cdoportunidade)){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.");
			String urlRedirecionamento = "redirect:/crm/crud/Oportunidade";
			if("entrada".equals(request.getParameter("controller"))){
				urlRedirecionamento +=  "?ACAO=consultar&cdoportunidade="+cdoportunidade;
			}
			return new ModelAndView(urlRedirecionamento);
		}
		return super.doGerar(request, filtro);
	}
}
