package br.com.linkcom.sined.modulo.crm.controller.report;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.service.AgendainteracaoService;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.AgendaInteracaoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.AgendaInteracaoClienteBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.AgendaInteracaoResponsavelBean;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.AgendaInteracaoFiltro;

@Controller(path="/crm/relatorio/Agendainteracao")
public class AgendaInteracaoReport extends ReportTemplateController<AgendaInteracaoFiltro> {
	
	private AgendainteracaoService agendainteracaoService;
	
	public void setAgendainteracaoService(AgendainteracaoService agendainteracaoService) {
		this.agendainteracaoService = agendainteracaoService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.AGENDA_INTERACAO;
	}

	@Override
	protected String getFileName() {
		return "agenda_interacao";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	AgendaInteracaoFiltro filtro) {
		return getReportTemplateService().load(filtro.getReportTemplate());
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, AgendaInteracaoFiltro filtro) throws Exception {
		
		List<Agendainteracao> listaAgendainteracao = agendainteracaoService.findForRelatorio(filtro.getSelectedItens());
		LinkedList<AgendaInteracaoBean> listaBean = new LinkedList<AgendaInteracaoBean>();
		Map<Integer, AgendaInteracaoClienteBean> mapaCliente = new HashMap<Integer, AgendaInteracaoClienteBean>();
		Map<Integer, AgendaInteracaoResponsavelBean> mapaResponsavel = new HashMap<Integer, AgendaInteracaoResponsavelBean>();
		
		SimpleDateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");
		
		for (Agendainteracao agendainteracao: listaAgendainteracao){
			
			Cliente cliente = agendainteracao.getCliente();
			Colaborador responsavel = agendainteracao.getResponsavel();
			String enderecoCliente = "";
			
			AgendaInteracaoBean bean = new AgendaInteracaoBean();
			
			if (cliente!=null){
				bean.setCodigo_cliente(cliente.getCdpessoa());
				bean.setNome_cliente(cliente.getNome());
				if (cliente.getListaEndereco()!=null && !cliente.getListaEndereco().isEmpty()){
					for (Endereco endereco: cliente.getListaEndereco()){
						enderecoCliente += endereco.getLogradouroCompletoComCep();
						enderecoCliente += " / ";
					}
					enderecoCliente = enderecoCliente.substring(0, enderecoCliente.length()-3);
				}
				bean.setEndereco_cliente(enderecoCliente);
			}
			
			bean.setRelacionado(agendainteracao.getAgendainteracaorelacionado()!=null ? agendainteracao.getAgendainteracaorelacionado().getNome() : "");
			bean.setMaterial_servico(agendainteracao.getMaterial()!=null ? agendainteracao.getMaterial().getNome() : "");
			bean.setContrato(agendainteracao.getContrato()!=null ? agendainteracao.getContrato().getDescricao() : "");
			
			if (responsavel!=null){
				bean.setCodigo_responsavel(responsavel.getCdpessoa());
				bean.setNome_responsavel(responsavel.getNome());
			}
			
			bean.setProxima_interacao(agendainteracao.getDtproximainteracao()!=null ? formatadorData.format(agendainteracao.getDtproximainteracao()) : "");
			bean.setPeriodicidade(agendainteracao.getPeriodicidade()!=null ? agendainteracao.getPeriodicidade().getNome() : "");
			bean.setFrequencia(agendainteracao.getQtdefrequencia());
			bean.setSituacao(agendainteracao.getAgendainteracaosituacao()!=null ? agendainteracao.getAgendainteracaosituacao().getNome() : "");
			bean.setDia_mes_semana(agendainteracaoService.getEscala(agendainteracao, filtro));
			
			listaBean.add(bean);
		}
		
		for (AgendaInteracaoBean bean: listaBean){
			if (bean.getCodigo_cliente()!=null){
				AgendaInteracaoClienteBean clienteBean = mapaCliente.get(bean.getCodigo_cliente());
				if (clienteBean==null){
					clienteBean = new AgendaInteracaoClienteBean(bean.getCodigo_cliente(), bean.getNome_cliente());
				}
				clienteBean.getAgendas().add(bean);
				mapaCliente.put(bean.getCodigo_cliente(), clienteBean);
			}
			if (bean.getCodigo_responsavel()!=null){
				AgendaInteracaoResponsavelBean responsavelBean = mapaResponsavel.get(bean.getCodigo_responsavel());
				if (responsavelBean==null){
					responsavelBean = new AgendaInteracaoResponsavelBean(bean.getCodigo_responsavel(), bean.getNome_responsavel());
				}
				responsavelBean.getAgendas().add(bean);
				mapaResponsavel.put(bean.getCodigo_responsavel(), responsavelBean);
			}
		}
		
		LinkedHashMap<String, Object> dados = new LinkedHashMap<String, Object>();
		dados.put("AgendaInteracaoBean", listaBean);
		dados.put("AgendaInteracaoClienteBean", mapaCliente.values());
		dados.put("AgendaInteracaoResponsavelBean", mapaResponsavel.values());
		
		return dados;
	}	
}