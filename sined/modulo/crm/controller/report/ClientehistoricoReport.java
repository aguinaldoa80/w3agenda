package br.com.linkcom.sined.modulo.crm.controller.report;


import java.sql.Date;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclientehistoricoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/crm/relatorio/Clientehistorico",  authorizationModule=ReportAuthorizationModule.class)
public class ClientehistoricoReport extends SinedReport<VwclientehistoricoFiltro>{

	@Override
	public IReport createReportSined(WebRequestContext request, VwclientehistoricoFiltro filtro) throws Exception {
		String selectedItens = request.getParameter("selectedItens");
		filtro.setExibirInteracao(Boolean.valueOf(request.getParameter("exibirInteracao")));
		filtro.setExibirInteracaoVenda(Boolean.valueOf(request.getParameter("exibirInteracaoVenda")));
		filtro.setExibirRequisicao(Boolean.valueOf(request.getParameter("exibirRequisicao")));
		
		if(request.getParameter("cdatividadetipo") != null){
			try {
				Atividadetipo atividadetipo = new Atividadetipo();
				atividadetipo.setCdatividadetipo(Integer.parseInt(request.getParameter("cdatividadetipo")));
				filtro.setAtividadetipo(atividadetipo);
			} catch (Exception e) {}
		}
		if(request.getParameter("dtinteracaoinicio") != null){
			try {
				Date dtinicio = SinedDateUtils.stringToDate(request.getParameter("dtinteracaoinicio"));
				filtro.setDtinteracaoinicio(dtinicio);
			} catch (Exception e) {}
		}
		if(request.getParameter("dtinteracaofim") != null){
			try {
				Date dtfim = SinedDateUtils.stringToDate(request.getParameter("dtinteracaodtfim"));
				filtro.setDtinteracaofim(dtfim);
			} catch (Exception e) {}
		}
		String virgula = ",";
		String[] ids = selectedItens.split(",");
		String whereIn = "";
		for(int i = 0; i < ids.length; i++){
			if(i == (ids.length - 1)) virgula = "";
			whereIn += StringUtils.substringBetween(ids[i], "", ".") + virgula;
		}
		return getClienteService().createRelatorioHistoricoCliente(whereIn, filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "historicoCliente";
	}

	@Override
	public String getTitulo(VwclientehistoricoFiltro filtro) {
		return "";
	}

}
