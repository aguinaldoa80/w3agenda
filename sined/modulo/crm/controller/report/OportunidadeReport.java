package br.com.linkcom.sined.modulo.crm.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadeFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
		path = "/crm/relatorio/Oportunidade",
		authorizationModule=ReportAuthorizationModule.class
	)
public class OportunidadeReport extends SinedReport<OportunidadeFiltro> {
	
	protected OportunidadeService oportunidadeService;
	
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	
	
	
	@Override
	public IReport createReportSined(WebRequestContext request,	OportunidadeFiltro filtro) throws Exception {
		return oportunidadeService.createRelatorioOportunidades(filtro);
	}
	
	@Override
	public String getTitulo(OportunidadeFiltro filtro) {
		return "RELATÓRIO DE OPORTUNIDADES";
	}
	
	@Override
	public String getNomeArquivo() {
		return "oportunidades";
	}

}
