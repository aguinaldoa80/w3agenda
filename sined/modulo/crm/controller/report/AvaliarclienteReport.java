package br.com.linkcom.sined.modulo.crm.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;

@Controller(path = "/crm/relatorio/AvaliarCliente")
public class AvaliarclienteReport extends ResourceSenderController<ClienteFiltro>{
	
	private ClienteService clienteService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,
			ClienteFiltro filtro) throws Exception {
		
		String whereIn = request.getParameter("selectedItens");
		
		return clienteService.createAvaliarCliente(whereIn);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, ClienteFiltro filtro)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
