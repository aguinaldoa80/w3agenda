package br.com.linkcom.sined.modulo.crm.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.service.CampanhaService;

@Controller(
		path="/crm/process/CampanhaCSV",
		authorizationModule=ProcessAuthorizationModule.class
)
public class CampanhaCSVProcess extends ResourceSenderController<Tarefa>{

	private CampanhaService campanhaService;
	
	public void setCampanhaService(CampanhaService campanhaService) {
		this.campanhaService = campanhaService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	Tarefa tarefa) throws Exception {
		return campanhaService.prepararArquivoCampanhaCSV(request.getParameter("selectedItens"));
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Tarefa tarefa) throws Exception {
		return null;
	}
	
}
