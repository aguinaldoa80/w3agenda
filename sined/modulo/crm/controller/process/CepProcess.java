package br.com.linkcom.sined.modulo.crm.controller.process;

import static br.com.linkcom.sined.util.SinedUtil.returnIdWithParameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.wscep.service.CepSearch;
import br.com.linkcom.sined.wscep.service.CepSearchServiceLocator;

@Controller(path={"/financeiro/process/Cep","/crm/process/Cep","/suprimento/process/Cep","/rh/process/Cep","/juridico/process/Cep","/faturamento/process/Cep","/sistema/process/Cep", "/pub/process/Cep", "/patrimonio/process/Cep"})
public class CepProcess extends MultiActionController {

	MunicipioService municipioService;
	
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}

	@DefaultAction
	@Action("entrada")
	public ModelAndView doGetAction(WebRequestContext context){
//		System.out.println("passou por aki");
		return null;
	}

	public void verifySendTo(WebRequestContext request){
		View view = View.getCurrent();
		view.println("var cepNaoEncontrado = false;");
		
		String endereco = "";
		String logradouro    = "";
		String complemento   = "";
		String bairro        = "";
		String nomeMunicipio = "";
		String siglaUf       = "";
		String cdmunicipio   = "";
		String cduf          = "";		

		try {
			String cep = request.getParameter("cep") != null && !request.getParameter("cep").equals("") ? request.getParameter("cep") : "";

//			CepClient cepClient = new CepClient();
//			CepPortType cepHttpPort = cepClient.getCepHttpPort();
//			endereco = cepHttpPort.retornaEndereco(cep);
			
			CepSearchServiceLocator loc = new CepSearchServiceLocator();
			CepSearch cepSearch = loc.getCepSearch();
			endereco = cepSearch.findAddressByCep(cep);
			
			if (endereco!=null){
				String[] campos = endereco.split("#",5);
				logradouro    = (campos.length > 0 ? campos[0] : "");
				complemento   = (campos.length > 1 ? campos[1] : "");
				bairro        = (campos.length > 2 ? campos[2] : "");
				nomeMunicipio = (campos.length > 3 ? campos[3] : "");
				siglaUf       = (campos.length > 4 ? campos[4] : "");
				cdmunicipio   = "";
				cduf          = "";
	
				if (!nomeMunicipio.equals("") && !siglaUf.equals("")) {
					Municipio municipio = municipioService.getByNomeAndUf( nomeMunicipio, siglaUf);
					if (municipio != null) {
						cdmunicipio = municipio.getCdmunicipio().toString();
						cduf = municipio.getUf().getCduf().toString();
					}
				}
			}else{
				view.println("cepNaoEncontrado = true;");
				view.println("alert('Cep n�o encontrado.');");
				view.println("$s.hideAlertMessage();");
			}
		}catch (Exception e) {
			throw new SinedException("javascript:alert('" + e.getMessage() + "');");
		}
		view.println("var logradouro  = '" + SinedUtil.escapeSingleQuotes(logradouro) + "';");
		view.println("var complemento = '" + SinedUtil.escapeSingleQuotes(complemento) + "';");
		view.println("var bairro      = '" + SinedUtil.escapeSingleQuotes(bairro) + "';");
		view.println("var cdmunicipio = 'br.com.linkcom.sined.geral.bean.Municipio[cdmunicipio=" + cdmunicipio + "]';");
		view.println("var cduf        = 'br.com.linkcom.sined.geral.bean.Uf[cduf=" + cduf + "]';");
	}
	
	public ModelAndView verifyUf(WebRequestContext request) throws IOException{
		View view = View.getCurrent();
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		Integer cdmunicipioSel = 0;
		try {
			
			Integer cduf = returnIdWithParameters(request.getParameter("cduf"));
			cdmunicipioSel = returnIdWithParameters(request.getParameter("cdmunicipio"));
			
			Uf uf = new Uf();
			uf.setCduf(cduf);			
			listaMunicipio = municipioService.find(uf);						
		} 
		catch (Exception e) {
			throw new SinedException("javascript:alert('" + e.getMessage() + "');");
		}
		printVar(listaMunicipio, cdmunicipioSel.intValue() != 0 ? cdmunicipioSel.toString() : "", view) ;
		return null;
	}

	
	private void printVar( List<Municipio> lista, String cdmunicipioSel, View view) throws IOException {
		StringBuffer sbVar = new StringBuffer();
		sbVar.append("var cdmunicipioSel = 'br.com.linkcom.sined.geral.bean.Municipio[cdmunicipio=" + cdmunicipioSel + "]';");
		sbVar.append("var mapa = [");
		sbVar.append("[\"<null>\",\"\"],");
		
		String cdmunicipio = "";
		String nome        = "";
		
		for (Municipio municipio : lista) {
			cdmunicipio = municipio.getCdmunicipio().toString();
			nome        = municipio.getNome();
			sbVar.append("[\"br.com.linkcom.sined.geral.bean.Municipio[cdmunicipio=" + cdmunicipio + "]\",\"" + nome + "\"],");		
		}
		sbVar.replace(sbVar.length() - 1, sbVar.length(), "");
		sbVar.append("];");
		view.println(sbVar.toString());
	}	
	
}
