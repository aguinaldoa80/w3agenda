package br.com.linkcom.sined.modulo.crm.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadeFiltro;

@Controller(
		path="/crm/process/OportunidadeCSV",
		authorizationModule=ProcessAuthorizationModule.class
)
public class OportunidadeCSVProcess extends ResourceSenderController<OportunidadeFiltro>{
	
	private OportunidadeService oportunidadeService;

	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,	OportunidadeFiltro filtro) throws Exception {		
		return oportunidadeService.prepararArquivoOportunidadeCSV(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	OportunidadeFiltro filtro) throws Exception {
		return null;
	}
	
	
}
