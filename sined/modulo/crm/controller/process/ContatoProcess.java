package br.com.linkcom.sined.modulo.crm.controller.process;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.ContatotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaContatoService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.TelefonetipoService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path={
				"/crm/process/Contato", 
				"/suprimento/process/Contato", 
				"/financeiro/process/Contato",
				"/juridico/process/Contato",
				"/producao/process/Contato",
				"/faturamento/process/Contato"
			},
		authorizationModule=ProcessAuthorizationModule.class
)
public class ContatoProcess extends MultiActionController{
	
	public static final String NOME_CLASSE_PAI_CONTATO = "nomeClassePaiContato";
	private EnderecoService enderecoService;
	private MunicipioService municipioService;
	private TelefonetipoService telefonetipoService;
	private ContatotipoService contatotipoService;
	private EmpresaService empresaService;
	private ConfiguracaonfeService configuracaonfeService;
	private PessoaService pessoaService;
	private ContatoService contatoService;
	private ParametrogeralService parametrogeralService;
	private PessoaContatoService pessoaContatoService;
	
	public void setTelefonetipoService(TelefonetipoService telefonetipoService) {
		this.telefonetipoService = telefonetipoService;
	}	
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setContatotipoService(ContatotipoService contatotipoService) {
		this.contatotipoService = contatotipoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {
		this.pessoaContatoService = pessoaContatoService;
	}
	
	/**
	 * Carrega a popup de cria��o de um contato.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, PessoaContato pessoaContato){
		pessoaContato.setContato(new Contato());
		pessoaContato.getContato().setCdpessoaParam(request.getParameter("cdpessoaParam"));
		request.setAttribute("listaEmpresa", empresaService.findAtivos());
		request.setAttribute("isNfeServicoBH", configuracaonfeService.havePrefixoativo(Prefixowebservice.BHISS_PROD));
		
		boolean obrigaCamposArvoreAcesso = StringUtils.isNotBlank(parametrogeralService.buscaValorPorNome(Parametrogeral.URL_SINC_CONTRATO_ARVOREACESSO));
		request.setAttribute("ObrigaCamposArvoreAcesso", obrigaCamposArvoreAcesso );		
		request.setAttribute(NOME_CLASSE_PAI_CONTATO, request.getParameter(ContatoProcess.NOME_CLASSE_PAI_CONTATO));
		return new ModelAndView("direct:process/popup/criaContato","pessoaContato", pessoaContato);
	}
	
	/**
	 * Exclui o contato da lista no escopo de sess�o.
	 * 
	 * @param request
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	@Action("excluir")
	public void excluir(WebRequestContext request){
		try{
			List<PessoaContato> listaContato = null;
			Object object = request.getParameter("posicaoLista");
			String cdpessoaParam = request.getParameter("cdpessoaParam");
			if(cdpessoaParam == null) cdpessoaParam = "";
			
			if (object != null) {
				String parameter = object.toString();
				parameter = parameter.substring(parameter.indexOf("indice=")+7, parameter.length()-1);
				Integer index = Integer.parseInt(parameter)-1;
				Object attr = request.getSession().getAttribute("listaContato" + cdpessoaParam);
				if (attr != null) {
					listaContato = (List<PessoaContato>) attr;
					listaContato.remove(index.intValue());
				}
			}
			request.getSession().setAttribute("listaContato" + cdpessoaParam, listaContato);
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("var sucesso = true;");
		} catch (Exception e) {
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	
	
	/**
	 * Salva o item de intera��o no objeto interacao e logo depois joga o objeto para a sess�o.
	 * 
	 * @param request
	 * @param interacaoitem
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	@Command(validate=true)
	@Input("criar")
	@Action("salvainteracao")
	public void salvaInteracao(WebRequestContext request, PessoaContato pessoaContato){
		Contato contato = pessoaContato.getContato();
		
		if(contato.getContatotipo() != null && contato.getContatotipo().getCdcontatotipo() != null){
			contato.setContatotipo(contatotipoService.load(contato.getContatotipo()));
		}else{
			contato.setContatotipo(null);
		}
		String cdpessoaParam = request.getParameter("cdpessoaParam");
		if(cdpessoaParam == null) cdpessoaParam = contato.getCdpessoaParam();
		if(cdpessoaParam == null) cdpessoaParam = "";
		
		Object attribute = request.getSession().getAttribute("listaContato" + cdpessoaParam);
		List<PessoaContato> listaContato = null;
		
		Set<Telefone> listaTelefone = contato.getListaTelefone();
		if (listaTelefone != null) {
			for (Telefone telefone : listaTelefone) {
				telefone.setTelefonetipo(telefonetipoService.load(telefone.getTelefonetipo()));
			}
		}
		
		if (attribute == null) {
			listaContato = new ArrayList<PessoaContato>();
			listaContato.add(pessoaContato);
			
			if (contato.getMunicipio() != null && contato.getMunicipio().getCdmunicipio() != null) {
				Municipio municipio = contato.getMunicipio();
				contato.setMunicipio(municipioService.carregaMunicipio(municipio));
			}
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>" +
										"parent.montaListaContato('"+new br.com.linkcom.neo.util.StringUtils().escape(contato.getNome())+
										"','"+contato.getTelefones()+
										"','"+(contato.getContatotipo() != null ? contato.getContatotipo().getNome() : "")+
										"','"+(contato.getEmailcontato() != null ? contato.getEmailcontato() : "") +
										"','"+ (contato.getReceberboleto() != null && contato.getReceberboleto() ? "Sim" : "N�o") +
										"','"+ (contato.getRecebernf() != null && contato.getRecebernf() ? "Sim" : "N�o") +
										"','"+ (contato.getResponsavelos() != null && contato.getResponsavelos() ? "Sim" : "N�o") +
										"');" +
										"parent.$.akModalRemove(true);" +
										"</script>");
		} else {
			listaContato = (List<PessoaContato>) attribute;
			
			if (contato.getMunicipio() != null && contato.getMunicipio().getCdmunicipio() != null) {
				Municipio municipio = contato.getMunicipio();
				contato.setMunicipio(municipioService.carregaMunicipio(municipio));
			}
			
			if(contato.getListaEndereco() != null && contato.getListaEndereco().size() > 0){
				for (Endereco endereco : contato.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						endereco.setMunicipio(municipioService.carregaMunicipio(endereco.getMunicipio()));
					}
				}
			}
			
			if (contato.getIndexlista() != null) {
				listaContato.set(contato.getIndexlista(), pessoaContato);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"parent.editaItem('"+contato.getIndexlista()+
											"','"+new br.com.linkcom.neo.util.StringUtils().escape(contato.getNome())+
											"','"+contato.getTelefones()+
											"','"+(contato.getContatotipo() != null ? contato.getContatotipo().getNome() : "")+
											"','"+(contato.getEmailcontato() != null ? contato.getEmailcontato() : "") +
											"','"+ (contato.getReceberboleto() != null && contato.getReceberboleto() ? "Sim" : "N�o") +
											"','"+ (contato.getRecebernf() != null && contato.getRecebernf() ? "Sim" : "N�o") +
											"','"+ (contato.getResponsavelos() != null && contato.getResponsavelos() ? "Sim" : "N�o") +
											"');" +
											"parent.$.akModalRemove(true);" +
											"</script>");
			} else {
				listaContato.add(pessoaContato);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"parent.montaListaContato('"+new br.com.linkcom.neo.util.StringUtils().escape(contato.getNome())+
											"','"+contato.getTelefones()+
											"','"+(contato.getContatotipo() != null ? contato.getContatotipo().getNome() : "")+
											"','"+(contato.getEmailcontato() != null ? contato.getEmailcontato() : "") +
											"','"+ (contato.getReceberboleto() != null && contato.getReceberboleto() ? "Sim" : "N�o") +
											"','"+ (contato.getRecebernf() != null && contato.getRecebernf() ? "Sim" : "N�o") +
											"','"+ (contato.getResponsavelos() != null && contato.getResponsavelos() ? "Sim" : "N�o") +
											"');" +
											"parent.$.akModalRemove(true);" +
											"</script>");
			}
		}
		
		
		request.getSession().setAttribute("listaContato" + cdpessoaParam, listaContato);
	}
	
	@SuppressWarnings("unchecked")
	public boolean existVariosResponsavelOS(PessoaContato pessoaContato){
		WebRequestContext request = NeoWeb.getRequestContext();
		Object attribute = request.getSession().getAttribute("listaContato" + pessoaContato.getContato().getCdpessoaParam());
		if (attribute != null) {
			List<PessoaContato> listaContato = (List<PessoaContato>) attribute;
			if(listaContato != null && listaContato.size() > 0 && pessoaContato.getContato().getResponsavelos() != null && pessoaContato.getContato().getResponsavelos()){
				for (int i = 0; i < listaContato.size(); i++) {
					Contato c = listaContato.get(i).getContato();
					if(pessoaContato.getContato().getIndexlista() != null && pessoaContato.getContato().getIndexlista().equals(i)) 
						continue;
					
					if(c.getResponsavelos() != null && c.getResponsavelos())
						return true;
				}
			}
		}
		return false;
	}
	
	
	
	
	/**
	 * Edita a intera��o da lista na entrada de prospec��o; 
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#carregaIndexLista
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	@Action("editar")
	public ModelAndView editar(WebRequestContext request){
		PessoaContato pessoaContato = null;
		Object object = request.getParameter("posicaoLista");
		
		if (object != null) {
			String parameter = object.toString();
			parameter = parameter.substring(parameter.indexOf("indice=")+7, parameter.length()-1);
			Integer index = Integer.parseInt(parameter)-1;
			
			String cdpessoaParam = request.getParameter("cdpessoaParam");
			if(cdpessoaParam == null) cdpessoaParam = "";
			
			Object attr = request.getSession().getAttribute("listaContato" + cdpessoaParam);
			List<PessoaContato> listaContato = null;
			
			if (attr != null) {
				listaContato = (List<PessoaContato>) attr;
				pessoaContato = listaContato.get(index);
				Contato contato = pessoaContato.getContato();
				
				contato.setIndexlista(index);
				contato.setCdpessoaParam(cdpessoaParam);
				if(contato.getMunicipio() != null){
					contato.setUf(contato.getMunicipio().getUf());
				}
				
				List<Municipio> listaMunicipio = new ArrayList<Municipio>();
				
				if(contato.getListaEndereco() != null){
					for (Endereco endereco : contato.getListaEndereco()) {
						if(endereco != null && endereco.getMunicipio() != null){
							listaMunicipio.add(endereco.getMunicipio());
							endereco.setUf(endereco.getMunicipio().getUf());
						}
					}
				}
				
				request.setAttribute("listaEmpresa", empresaService.findAtivos(contato.getEmpresa()));
				request.setAttribute("listaMunicipioEndereco", listaMunicipio);	
			}
		}
		
		boolean obrigaCamposArvoreAcesso = StringUtils.isNotBlank(parametrogeralService.buscaValorPorNome(Parametrogeral.URL_SINC_CONTRATO_ARVOREACESSO));
		request.setAttribute("ObrigaCamposArvoreAcesso", obrigaCamposArvoreAcesso );		
		request.setAttribute(NOME_CLASSE_PAI_CONTATO, request.getParameter(ContatoProcess.NOME_CLASSE_PAI_CONTATO));
		return new ModelAndView("direct:process/popup/criaContato", "pessoaContato", pessoaContato);
	}
	
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		PessoaContato pessoaContato = (PessoaContato) obj;
		enderecoService.validateListaEndereco(errors, pessoaContato.getContato().getListaEndereco());
		
		if(existVariosResponsavelOS(pessoaContato)){
			errors.reject("001","S� � permitido um respons�vel por cliente.");
		}
	}
	

	/**
	 * M�todo para filtrar os mun�cipios do UF
	 * 
	 * @see MunicipioService#findByUf(br.com.linkcom.sined.geral.bean.Uf)
	 * @see SinedUtil#convertToJavaScript(List, String, String)
	 * @param request
	 * @param uf
	 * @author Taidson
	 * @since 25/04/2010
     * Estrutura copiada do Sindis
	 */

	public void ajaxComboMunicipio(WebRequestContext request, Uf uf){
		
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		if (uf!=null && uf.getCduf()!=null){
			listaMunicipio = municipioService.findByUf(uf);			
		}
				
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaMunicipio, "listaMunicipio", ""));
	}
	
	public void ajaxVerificarContatotipoSocio(WebRequestContext request, Contato contato){
		boolean socio = false;
		if (contato!=null && contato.getContatotipo()!=null && contato.getContatotipo().getCdcontatotipo()!=null){
			socio = Boolean.TRUE.equals(contatotipoService.load(contato.getContatotipo()).getSocio());
		}
		View.getCurrent().println("socio = " + socio);
	}
	
	/**
	 * Verifica se a pessoa/contato existe
	 * @param request
	 */
	public void verificaPessoa(WebRequestContext request){
		Pessoa pessoa;
		Cpf cpf = new Cpf(request.getParameter("cpf"));
		String cdPessoaParam = request.getParameter("cdpessoaParam");
		if(cpf != null){
			pessoa = pessoaService.findPessoaByCpf(cpf);
		} else {
			throw new SinedException("Erro na obten��o da pessoa.");
		}
		
		String string = "";
		if(pessoa != null && cdPessoaParam != null){
			string += "var achou = true;";
			
			List<PessoaContato> pessoaContatoList = pessoaContatoService.findContatoByPessoaAndContato(Integer.parseInt(cdPessoaParam), pessoa.getCdpessoa());
			if(pessoaContatoList != null && pessoaContatoList.size() > 0){
				string += "var contatoAchou = true;";
			} else {
				string += "var contatoAchou = false;";
				string += "var nomeContato = '" + SinedUtil.escapeSingleQuotes(pessoa.getNome()) + "';";
				string += "var cdpessoaContato = " + pessoa.getCdpessoa() + ";";
			}
		} else {
			string += "var achou = false;";
		}
		
		View.getCurrent().println(string);
	}
	
	/**
	 * M�todo que busca e seta os dados da pessoa/contato
	 * @param request
	 * @param bean
	 */
	public void recuperarPessoa(WebRequestContext request, PessoaContato bean){
		if(request.getParameter("cdpessoaParam")!=null && request.getParameter("cdpessoaParam")!="") {
			Integer cdpessoaligacao = Integer.parseInt(request.getParameter("cdpessoaParam"));
			Contato contato = contatoService.load(bean.getContato());
			
			if (contato == null) {
				contatoService.insertSomenteContato(bean.getContato(), cdpessoaligacao);
			}
			
			PessoaContato pessoaContato = new PessoaContato();
			pessoaContato.setContato(bean.getContato());
			pessoaContato.setPessoa(new Pessoa(cdpessoaligacao));
			
			pessoaContatoService.saveOrUpdate(pessoaContato);
			
			if (BooleanUtils.isTrue(bean.getCliente())) {
				SinedUtil.redirecionamento(request, "/crm/crud/Cliente?ACAO=consultar&cdpessoa=" + cdpessoaligacao);
			} else {
				SinedUtil.redirecionamento(request, "/suprimento/crud/Fornecedor?ACAO=consultar&cdpessoa=" + cdpessoaligacao);
			}
		} else {
			Pessoa pessoa = new Pessoa(bean.getPessoa().getCdpessoa());
			pessoa = pessoaService.carregaPessoaForContato(pessoa);
			Contato contato = new Contato();
			contato.setNome(pessoa.getNome());
			contato.setCpf(pessoa.getCpf());
			contato.setEmail(pessoa.getEmail());
			contato.setListaEndereco(pessoa.getListaEndereco());
			contato.setDtnascimento(pessoa.getDtnascimento());
			contato.setListaTelefone(pessoa.getListaTelefone());
			contato.setCdpessoaParam(request.getParameter("cdpessoaParam"));
			salvaInteracao(request, bean);			
		}
	}
}