package br.com.linkcom.sined.modulo.crm.controller.process;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendainteracaohistorico;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaorelacionado;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoagendainteracao;
import br.com.linkcom.sined.geral.service.AgendainteracaoService;
import br.com.linkcom.sined.geral.service.AgendainteracaohistoricoService;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ClientehistoricoService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MeiocontatoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.VwclientehistoricoFiltro;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.HistoricoPainelVO;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.VendaBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path={"/crm/process/PainelInteracao","/juridico/process/PainelInteracao"},
		authorizationModule=ProcessAuthorizationModule.class
)
public class PainelInteracaoProcess extends MultiActionController{
	
	private ClientehistoricoService clientehistoricoService;
	private RequisicaoService requisicaoService;
	private ContratoService contratoService;
	private ContatoService contatoService;
	private DocumentoService documentoService;
	private ClienteService clienteService;
	private PropostaService propostaService;
	private ApontamentoService apontamentoService;
	private ParametrogeralService parametrogeralService;
	private AgendainteracaoService agendainteracaoService;
	private MeiocontatoService meiocontatoService;
	private VendaService vendaService;
	private UsuarioService usuarioService;
	private PedidovendaService pedidovendaService;
	private AtividadetipoService atividadetipoService;
	private ValecompraService valecompraService;
	private MovimentacaoService movimentacaoService;
	private VendaorcamentoService vendaorcamentoService;
	private OportunidadeService oportunidadeService;
	private EmpresaService empresaService;
	private AgendainteracaohistoricoService agendainteracaohistoricoService;
	
	public void setAgendainteracaohistoricoService(
			AgendainteracaohistoricoService agendainteracaohistoricoService) {
		this.agendainteracaohistoricoService = agendainteracaohistoricoService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setValecompraService(ValecompraService valecompraService) {
		this.valecompraService = valecompraService;
	}
	public void setPropostaService(PropostaService propostaService) {
		this.propostaService = propostaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setClientehistoricoService(ClientehistoricoService clientehistoricoService) {
		this.clientehistoricoService = clientehistoricoService;
	}
	public void setRequisicaoService(RequisicaoService requisicaoService) {
		this.requisicaoService = requisicaoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setApontamentoService(ApontamentoService apontamentoService) {
		this.apontamentoService = apontamentoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setAgendainteracaoService(AgendainteracaoService agendainteracaoService) {
		this.agendainteracaoService = agendainteracaoService;
	}
	public void setMeiocontatoService(MeiocontatoService meiocontatoService) {
		this.meiocontatoService = meiocontatoService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {
		this.vendaorcamentoService = vendaorcamentoService;
	}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@DefaultAction
	public ModelAndView handle(WebRequestContext request, Prospeccao prospeccao){
		Clientehistorico clientehistorico = new Clientehistorico();
		clientehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		if(request.getAttribute("EMPRESA_AGENDAINTERACAO") != null){
			clientehistorico.setEmpresahistorico((Empresa) request.getAttribute("EMPRESA_AGENDAINTERACAO"));
		}else {
			clientehistorico.setEmpresahistorico(empresaService.loadPrincipal());
		}
		
		String cdCliente = request.getParameter("cdCliente");
		if(cdCliente != null){
			Cliente cliente = new Cliente(Integer.parseInt(cdCliente));
			prospeccao.setCliente(cliente);
		}
		String whereInDocumentoCobranca = request.getParameter("whereInDocumentoCobranca");
		if(StringUtils.isNotEmpty(whereInDocumentoCobranca) && !"undefined".equals(whereInDocumentoCobranca) && 
				!"null".equals(whereInDocumentoCobranca)){
			clientehistorico.setWhereInDocumentoCobranca(whereInDocumentoCobranca);
		}
		
		if(prospeccao.getSitucaoAtencaoAtrasado() != null)
			clientehistorico.setSitucaoAtencaoAtrasado(prospeccao.getSitucaoAtencaoAtrasado());
		
		if(prospeccao.getCliente() != null){
			clientehistorico.setCliente(clienteService.load(prospeccao.getCliente(), "cliente.cdpessoa, cliente.nome"));
			if(clientehistorico.getEmpresahistorico() != null && clientehistorico.getEmpresahistorico().getCdpessoa() != null &&
					clientehistorico.getCliente() != null && !clienteService.isClientePermissaoEmpresa(clientehistorico.getCliente(), clientehistorico.getEmpresahistorico().getCdpessoa().toString())){
				clientehistorico.setEmpresahistorico(null);
			}
		}
		String fechaPopUp = request.getParameter("fechaPopUp");
		request.setAttribute("fechaPopUp", fechaPopUp);
		request.setAttribute("listaMeioContato", meiocontatoService.findAtivos());
		request.setAttribute("listaAtividadetipo", atividadetipoService.findForCrm());
		return new ModelAndView("process/painelinteracao","clientehistorico",clientehistorico);
	}
	
	public void saveHistorico(WebRequestContext requestContext, Clientehistorico clientehistorico){
		if(clientehistorico.getDtproximacobranca() != null && SinedUtil.getUsuarioComoColaborador() != null){
			Agendainteracao agendainteracao = new Agendainteracao();
			agendainteracao.setEmpresahistorico(clientehistorico.getEmpresahistorico());
			agendainteracao.setTipoagendainteracao(Tipoagendainteracao.CLIENTE);
			agendainteracao.setCliente(clientehistorico.getCliente());
			agendainteracao.setAgendainteracaorelacionado(Agendainteracaorelacionado.AVULSO);
			agendainteracao.setAtividadetipo(clientehistorico.getAtividadetipo());
			agendainteracao.setDescricao(clientehistorico.getObservacao());
			agendainteracao.setResponsavel(SinedUtil.getUsuarioComoColaborador());
			agendainteracao.setDtproximainteracao(clientehistorico.getDtproximacobranca());
			agendainteracao.setPeriodicidade(new Frequencia(Frequencia.UNICA));
			agendainteracaoService.saveOrUpdate(agendainteracao);
			
			Agendainteracaohistorico agendainteracaohistorico = new Agendainteracaohistorico();
    		agendainteracaohistorico.setAgendainteracao(agendainteracao);
			agendainteracaohistoricoService.saveOrUpdate(agendainteracaohistorico);
		}
		
		//seta a pessoa
		clientehistorico.setCdusuarioaltera(((Usuario)Neo.getUser()).getCdpessoa());
		clientehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		// FEITO ISSO PARA CORRIGIR O ERRO DE ENCODING
		String obs = clientehistorico.getObservacao();
		if(obs != null && !obs.equals("")){
			try {
				obs = new String(obs.getBytes(), "LATIN1");
			} catch (Exception e) {
				try {
					obs = new String(obs.getBytes("LATIN1"));
				} catch (Exception e2) {}
			}
		}
		
		String obsDocumento = obs;
		obs = "(Origem na agenda de intera��o) " + obs;
		
		clientehistorico.setObservacao(obs);
		
		String fechaPopUp = requestContext.getParameter("fechaPopUp");
		
		try{
			clientehistoricoService.saveOrUpdate(clientehistorico);
			if(fechaPopUp != null && "TRUE".equals(fechaPopUp.toUpperCase())){
				View.getCurrent().println("{erro:false,fechaPopUp:true}");
			} else {
				View.getCurrent().println("{erro:false}");
			}
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("{erro:true}");
		}
		try {
			if(StringUtils.isNotEmpty(clientehistorico.getWhereInDocumentoCobranca())){
				for(String idDocumento : clientehistorico.getWhereInDocumentoCobranca().split(",")){
					if(StringUtils.isNotEmpty(idDocumento)){
						documentoService.criarHistoricoERegistrarUltimaCobranca(null, new Documento(Integer.parseInt(idDocumento)), obsDocumento);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public void loadLists(WebRequestContext webRequestContext, Clientehistorico clientehistorico){
		HistoricoPainelVO vo = new HistoricoPainelVO();
		
		if (clientehistorico.getAtividadetipo() != null && clientehistorico.getListaTodos() != null && !clientehistorico.getListaTodos()) {
			vo.setExibirAlertaHistorico(Boolean.TRUE);
		} else {
			vo.setExibirAlertaHistorico(Boolean.FALSE);
			clientehistorico.setAtividadetipo(null);
		}
		
		vo.setListaClienteHistorico(clientehistoricoService.getClientehistoricoVO((clientehistoricoService.findByCliente(clientehistorico.getCliente(),
																		clientehistorico.getAtividadetipo(),
																		clientehistorico.getEmpresahistorico()))));
		
		vo.setListaRequisicao(requisicaoService.findByCliente(clientehistorico.getCliente(),
															clientehistorico.getAtividadetipo(),
															clientehistorico.getEmpresahistorico()));
		
		View.getCurrent().eval(View.getCurrent().convertToJson(vo).toString());
	}
	
	/**
	 * Carrega a combo contrato com os contratos do cliente selecionado.
	 * @author Taidson Santos
	 * @since 29/06/2010
	 */
	public void ajaxComboContrato(WebRequestContext request, Clientehistorico clientehistorico){
		
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		
		if(clientehistorico.getCliente() != null){
			listaContrato = contratoService.findByContrato(clientehistorico.getCliente());			
		}
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaContrato, "listaContrato", ""));
	}
	/**
	 * Carrega a combo contato com os contatos do cliente selecionado.
	 * @author Taidson Santos
	 * @since 29/06/2010
	 */
	public void ajaxComboContato(WebRequestContext request, Clientehistorico clientehistorico){
		
		List<Contato> listaContato = new ArrayList<Contato>();

		if(clientehistorico.getCliente() != null){
			listaContato = contatoService.findByPessoaCombo(clientehistorico.getCliente(), true);						
		}
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaContato, "listaContato", ""));
	}
	
	
	public void ajaxLinkProposta(WebRequestContext request, Clientehistorico clientehistorico){
		if(clientehistorico.getCliente() != null){
			if(propostaService.havePropostaCliente(clientehistorico.getCliente())){
				View.getCurrent().println("var haveProposta = true;");
			} else {
				View.getCurrent().println("var haveProposta = false;");
			}
		} else {
			View.getCurrent().println("var haveProposta = false;");
		}
	}
	
	
	/**
	 * M�todo respons�vel por verificar se o cliente possiu alguma conta � receber em aberto.
	 * @author Taidson
	 * @since 01/06/2010
	 */
	public void ajaxVerificaPendenciaFinanceira(WebRequestContext request, Clientehistorico clientehistorico){
		
		Boolean existePendenciaFinaneceira = null;
		Integer numDias = null;
		
		if(clientehistorico.getCliente() != null){
			Date dtvencimento = documentoService.getPrimeiroVencimentoDocumentoAtrasado(clientehistorico.getCliente());	
			if(dtvencimento != null){
				existePendenciaFinaneceira = Boolean.TRUE;
				numDias = SinedDateUtils.diferencaDias(SinedDateUtils.currentDateToBeginOfDay(), dtvencimento);
			} else {
				existePendenciaFinaneceira = Boolean.FALSE;
			}
		}
		View.getCurrent().println("var existePendenciaFinaneceira = '" + Util.strings.toStringIdStyled(existePendenciaFinaneceira, false)+"';");
		View.getCurrent().println("var numDias = '" + numDias + "';");
	}
	
	/**
	 * Carrega dados necess�rios para a popup de apontamento em intera��o de cliente.
	 * @param request
	 * @param clientehistorico
	 * @return
	 * @author Taidson
	 * @since 15/09/2010
	 */
	public ModelAndView apontamentoInteracao(WebRequestContext request, Clientehistorico clientehistorico){
		
		Date data = null;
		Apontamento apontamento = new Apontamento();
		if(request.getSession().getAttribute("dtHoraInteracao") != null){
			data = (Date) request.getSession().getAttribute("dtHoraInteracao");
			Long dtlong = new Long(data.getTime());
			apontamento.setHrinicioApontamento((Hora) new Hora(dtlong));
		}
		apontamento.setHrfimApontamento((Hora) new Hora(System.currentTimeMillis())); 
		
		
		Contrato contrato = new Contrato();
		if(clientehistorico.getContrato() != null)
			contrato = contratoService.loadForEntrada(clientehistorico.getContrato());
		apontamento.setCliente(clienteService.load(clientehistorico.getCliente(), "cliente.cdpessoa, cliente.nome, cliente.identificador"));
		apontamento.setContrato(contrato);
		
		Meiocontato meiocontato = meiocontatoService.load(clientehistorico.getMeiocontato());
		apontamento.setDescricao("Intera��o via "+ meiocontato.getNome());
		
		Colaborador colaborador = new Colaborador(((Usuario)request.getUser()).getCdpessoa());
		apontamento.setColaborador(colaborador);
		
		return new ModelAndView("direct:/process/popup/apontamentoInteracao", "apontamento", apontamento);
	}
	
	/**
	 * Verifica se est� setado em par�metro geral incluir apontamento via intera��o de cliente (APONTAMENTO_INTERACAO).
	 * @param request
	 * @param clientehistorico
	 * @author Taidson
	 * @since 15/09/2010
	 */
	public void ajaxIncluirApontamentoInteracao(WebRequestContext request){
		
		try{
			Boolean incluirApontamentoInteracao = false;
			if(!parametrogeralService.getValorPorNome("APONTAMENTO_INTERACAO").equals("FALSE")){
				incluirApontamentoInteracao = true;			
			}
			View.getCurrent().println("var incluirApontamentoInteracao = '" + Util.strings.toStringIdStyled(incluirApontamentoInteracao, false)+"';");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var incluirApontamentoInteracao = 'false';");
		}
	}

	/**
	 * Seta, na sess�o do usu�rio, a data inicial para o apontamento via intera��o de cliente
	 * @param request
	 * @author Taidson
	 * @since 15/09/2010
	 */
	public void setDtHoraInteracao(WebRequestContext request){
		Timestamp data = new Timestamp(System.currentTimeMillis());
		request.getSession().setAttribute("dtHoraInteracao", data);
	}
	
	/**
	 * Salva apontamento em intera��o de cliente.
	 * @param request
	 * @param apontamento
	 * @author Taidson
	 * @since 15/09/2010
	 */
	public void saveApontamentoInteracao(WebRequestContext request, Apontamento apontamento){
		try{
			ApontamentoHoras apontamentoHoras = new ApontamentoHoras();
			apontamentoHoras.setHrinicio(apontamento.getHrinicioApontamento());
			apontamentoHoras.setHrfim(apontamento.getHrfimApontamento());
			List<ApontamentoHoras> horas = new ListSet<ApontamentoHoras>(ApontamentoHoras.class);
			horas.add(apontamentoHoras);
			
			apontamento.setApontamentoTipo(ApontamentoTipo.FUNCAO_HORA);
			apontamento.setDtapontamento(new java.sql.Date(System.currentTimeMillis()));
			apontamento.setListaApontamentoHoras(horas);
			apontamento.setQtdehoras(SinedDateUtils.calculaDiferencaHoras(apontamento.getHrinicioApontamento().toString(), apontamento.getHrfimApontamento().toString()));
		
			apontamentoService.saveOrUpdate(apontamento);
			request.addMessage("Apontamento salvo com sucesso.", MessageType.INFO);
		} catch (Exception e) {
			request.addError("A inclus�o do apontamento n�o foi realizada. " + e.getMessage());
		} finally {
			SinedUtil.fechaPopUp(request);
		}
	}
	
	/**
	 * Monta a lista de propostas para o painel de intera��o.
	 * @param webRequestContext
	 * @param clientehistorico
	 * @author Taidson
	 * @since 16/09/2010
	 */
	@SuppressWarnings("static-access")
	public void loadPropostas(WebRequestContext webRequestContext, Clientehistorico clientehistorico){
		HistoricoPainelVO vo = new HistoricoPainelVO();
		
		vo.setListaProposta(propostaService.findByCliente(clientehistorico.getCliente(), clientehistorico.getEmpresahistorico()));
		View.getCurrent().eval(View.getCurrent().convertToJson(vo).toString());
	}
	
	/**
	 * Monta lista de oportunidades para o painel de intera��o
	 * @param request
	 * @param clientehistorico
	 * @since 12/07/2016
	 * @author C�sar
	 */
	@SuppressWarnings("static-access")
	public void loadOportunidades(WebRequestContext request, Clientehistorico clientehistorico){
		HistoricoPainelVO vo = new HistoricoPainelVO();
		
		if(clientehistorico != null && clientehistorico.getCliente() != null){
			vo.setListaOportunidade(oportunidadeService.findForPainelInteracao(clientehistorico.getCliente(),
																			clientehistorico.getEmpresahistorico()));
			View.getCurrent().eval(View.getCurrent().convertToJson(vo).toString());
		}
	}
	
	/**
	 * Monta a lista de pagamentos para o painel de intera��o.
	 * @param webRequestContext
	 * @param clientehistorico
	 * @author Taidson
	 * @since 17/09/2010
	 */
	@SuppressWarnings("static-access")
	public void loadPagamentos(WebRequestContext webRequestContext, Clientehistorico clientehistorico){
		HistoricoPainelVO vo = new HistoricoPainelVO();
		
		List<Documento> lista = documentoService.findPagamentosByCliente(clientehistorico.getCliente(), clientehistorico.getEmpresahistorico());
		for (Documento documento : lista) {
			if (Documentoacao.BAIXADA_PARCIAL.equals(documento.getDocumentoacao())){
				documento.setValor(movimentacaoService.calcularMovimentacoesByDocumento(documento));
			}
		}
		vo.setListaPagamento(lista);
		
		Money valortotalEmAberto = new Money();
		if(vo.getListaPagamento() != null && !vo.getListaPagamento().isEmpty()){
			for(Documento documento : vo.getListaPagamento()){
				if(documento.getValor() != null){
					valortotalEmAberto = valortotalEmAberto.add(documento.getValor());
				}
				                                                                                                 
			}
		}
		
		vo.setTotalemAberto(valortotalEmAberto);
		vo.setSaldopagamentoantecipado(documentoService.getTotalAntecipacaoByCliente(clientehistorico.getCliente()));
		Cliente cliente = clienteService.load(clientehistorico.getCliente(), "cliente.creditolimitecompra");
		vo.setCreditolimitecompra(cliente.getCreditolimitecompra() != null ? new Money(cliente.getCreditolimitecompra()) : new Money());
		vo.setSaldoValeCompras(valecompraService.getSaldoByCliente(clientehistorico.getCliente()));
		
		View.getCurrent().eval(View.getCurrent().convertToJson(vo).toString());
		
	}
	
	@SuppressWarnings({"static-access","unchecked"})
	public void loadAgendainteracao(WebRequestContext webRequestContext, Clientehistorico clientehistorico){
		HistoricoPainelVO vo = new HistoricoPainelVO();
		
		List<Agendainteracao> listaAgendainteracao = agendainteracaoService.findByEmpresaCliente(clientehistorico.getEmpresahistorico(), clientehistorico.getCliente());
		Collections.sort(listaAgendainteracao, new ReverseComparator(new BeanComparator("dtproximainteracao")));
		
		vo.setListaAgendainteracao(listaAgendainteracao);
		View.getCurrent().eval(View.getCurrent().convertToJson(vo).toString());
	}
	
	@SuppressWarnings({"static-access","unchecked"})
	public void loadVendas(WebRequestContext webRequestContext, Clientehistorico clientehistorico){
		HistoricoPainelVO vo = new HistoricoPainelVO();
		List<Venda> listaVenda = vendaService.findByClienteOrderData(clientehistorico.getCliente(), clientehistorico.getEmpresahistorico());
		List<Pedidovenda> listaPedidoVenda = pedidovendaService.findAllNaoConfirmadosByClienteOrderbyData(clientehistorico.getCliente(),
																										clientehistorico.getEmpresahistorico());
		List<Vendaorcamento> listaVendaorcamento = vendaorcamentoService.findByCliente(clientehistorico.getCliente(), clientehistorico.getEmpresahistorico());
		
		List<VendaBean> lista = new ArrayList<VendaBean>();
		for (Venda venda : listaVenda) {
			String tipo = "";
			if (venda.getPedidovendatipo() != null) {
				tipo = "/ " + venda.getPedidovendatipo().getDescricao();
			}
			VendaBean bean = new VendaBean();
			bean.setCdvenda(venda.getCdvenda());
			bean.setDtvenda(new java.sql.Date(venda.getDtvenda().getTime()));
			bean.setTipo("Venda " + tipo);
			
			String str = "";
			
			for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
				str += vendamaterial.getMaterial().getNome();
				Money total = new Money(vendamaterial.getQuantidade());
				Money desconto = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() : new Money();
				str += " (R$ " + total.multiply(new Money(vendamaterial.getPreco())).subtract(desconto).toString() + ") | ";
			}
			bean.setMateriais(str);
			lista.add(bean);
		}
		for (Pedidovenda pedidovenda : listaPedidoVenda) {
			String tipo = "";
			if (pedidovenda.getPedidovendatipo() != null) {
				tipo = "/ " + pedidovenda.getPedidovendatipo().getDescricao();
			}
			VendaBean bean = new VendaBean();
			bean.setCdvenda(pedidovenda.getCdpedidovenda());
			bean.setDtvenda(new java.sql.Date(pedidovenda.getDtpedidovenda().getTime()));
			bean.setTipo("Pedido de venda " + tipo);
			String str = "";
			
			for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
				str += pedidovendamaterial.getMaterial().getNome();
				Money total = new Money(pedidovendamaterial.getQuantidade());
				Money desconto = pedidovendamaterial.getDesconto() != null ? pedidovendamaterial.getDesconto() : new Money();
				str += " (R$ " + total.multiply(new Money(pedidovendamaterial.getPreco())).subtract(desconto).toString() + ") | ";
			}
			bean.setMateriais(str);
			lista.add(bean);
		}
		
		for (Vendaorcamento vendaorcamento : listaVendaorcamento) {
			if (vendaorcamento.getVendaorcamentosituacao().getCdvendaorcamentosituacao().equals(Vendaorcamentosituacao.CANCELADA.getCdvendaorcamentosituacao())){
				continue;
			}
			String tipo = "";
			if (vendaorcamento.getPedidovendatipo() != null) {
				tipo = "/ " + vendaorcamento.getPedidovendatipo().getDescricao();
			}
			VendaBean bean = new VendaBean();
			bean.setCdvenda(vendaorcamento.getCdvendaorcamento());
			bean.setDtvenda(new java.sql.Date(vendaorcamento.getDtorcamento().getTime()));
			bean.setTipo("Or�amento " + tipo);
			String str = "";
			
			for(Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()){
				str += vendaorcamentomaterial.getMaterial().getNome();
				Money total = new Money(vendaorcamentomaterial.getQuantidade());
				Money desconto = vendaorcamentomaterial.getDesconto() != null ? vendaorcamentomaterial.getDesconto() : new Money();
				str += " (R$ " + total.multiply(new Money(vendaorcamentomaterial.getPreco())).subtract(desconto).toString() + ") | ";
			}
			bean.setMateriais(str);
			lista.add(bean);
		}
		
		Collections.sort(lista, new ReverseComparator(new BeanComparator("dtvenda")));
		
		vo.setListaVenda(lista);
		View.getCurrent().eval(View.getCurrent().convertToJson(vo).toString());
	}

	/**
	 * Redireciona para tela de Intera��o de vendas, com os campos preenchidos,
	 * a partir da tela da listagem de Oportunidade.
	 * @param request
	 * @param oportunidade
	 * @return
	 * @throws Exception
	 * @author Taidson
	 * @since 03/09/2010
	 */
	public ModelAndView interacaoAgendaInteracao(WebRequestContext request, Agendainteracao agendainteracao) throws Exception{
		request.getServletResponse().setContentType("text/html");
		String fullName = "'br.com.linkcom.sined.geral.bean.Agendainteracao[cdagendainteracao=";
		
		Integer situacao = Integer.parseInt(request.getParameter("situacao"));
		
		agendainteracao = agendainteracaoService.loadForInteracao(agendainteracao);

		Prospeccao prospeccao = new Prospeccao();
		
		prospeccao.setCliente(agendainteracao.getCliente());
		
		request.setAttribute("cliente", fullName + agendainteracao.getCliente().getCdpessoa()+",nome="+agendainteracao.getCliente().getNome()+"]'");
		
		Boolean atencaoAtrasado = null;
		if(situacao == 1 || situacao == 2){  
			atencaoAtrasado = true;
		}else{
			atencaoAtrasado = false;
		}
		
		prospeccao.setSitucaoAtencaoAtrasado(atencaoAtrasado);
		
		request.setAttribute("EMPRESA_AGENDAINTERACAO", agendainteracao.getEmpresahistorico());
		return handle(request, prospeccao);	
		
	}
	public ModelAndView habilitaDesabilitaLink(WebRequestContext webRequestContext, Clientehistorico clientehistorico){
		String cdcontrato = webRequestContext.getParameter("contrato");
		String url ="";
		boolean redirecionaUrl = false;
		Usuario usuario = usuarioService.carregarUsuarioByLogin("admin");
		if(!cdcontrato.equals("<null>")){
			String [] string = cdcontrato.split("cdcontrato=");
			Contrato contrato = new Contrato();
			contrato.setCdcontrato(Integer.parseInt(string[1].replace("]", "")));
			url = contratoService.carregaUrlSistema(contrato);
			String parametroW3mpsbr = parametrogeralService.buscaValorPorNome(Parametrogeral.W3MPSBR_INTEGRACAO);
			if (parametroW3mpsbr != null && parametroW3mpsbr.toUpperCase().equals("TRUE") && url != null && !url.equals("")){
				redirecionaUrl = true;
			}
		}
		return new JsonModelAndView().addObject("url",url)
			.addObject("redirecionaUrl", redirecionaUrl)
			.addObject("usuario", usuario.getLogin())
			.addObject("senha", usuario.getSenha());
	}
	
	/**
	 * 
	 * @param webRequestContext
	 * @param clientehistorico
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarContratoCancelado(WebRequestContext webRequestContext, Clientehistorico clientehistorico){
		
		boolean cancelado = false;
		
		if (clientehistorico.getContrato()!=null && clientehistorico.getContrato().getCdcontrato()!=null){
			Contrato contrato = contratoService.load(clientehistorico.getContrato(), "contrato.aux_contrato");
			if (contrato!=null 
					&& contrato.getAux_contrato()!=null 
					&& SituacaoContrato.CANCELADO.equals(contrato.getAux_contrato().getSituacao())){
				cancelado = true;
			}
		}
		
		return new JsonModelAndView().addObject("cancelado", cancelado);		
	}
	
	public void exibirSituacao(WebRequestContext request, VwclientehistoricoFiltro filtro) throws Exception {
		boolean exibir = false;
		boolean exibirProximacobranca = false;
		
		if (filtro.getAtividadetipo()!=null){
			Atividadetipo atividadetipo = atividadetipoService.load(filtro.getAtividadetipo());
			exibir = Boolean.TRUE.equals(atividadetipo.getReclamacao());
			exibirProximacobranca = Boolean.TRUE.equals(atividadetipo.getCobranca());
		}
		
		View.getCurrent().print("var exibir = " + exibir + ";");		
		View.getCurrent().print("var exibirProximacobranca = " + exibirProximacobranca + ";");		
	}
	
	public ModelAndView verificaClienteEmpresa(WebRequestContext request) throws Exception {
		return clienteService.verificaClienteEmpresa(request);
	}
}
