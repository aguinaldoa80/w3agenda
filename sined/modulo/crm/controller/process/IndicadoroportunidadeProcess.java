package br.com.linkcom.sined.modulo.crm.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.IndicadorOportunidadeService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.IndicadorortunidadeBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.SituacaoOportunidade;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.IndicadoroportunidadeFiltro;
import br.com.linkcom.sined.util.Grafico;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/crm/process/Indicadoroportunidade", authorizationModule=ProcessAuthorizationModule.class)
public class IndicadoroportunidadeProcess extends MultiActionController{

	protected OportunidadeService oportunidadeService;
	protected MaterialService materialService;
	protected ColaboradorService colaboradorService;
	protected IndicadorOportunidadeService indicadorOportunidadeService;
	private ArquivoService arquivoService;
	
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setIndicadorOportunidadeService(
			IndicadorOportunidadeService indicadorOportunidadeService) {
		this.indicadorOportunidadeService = indicadorOportunidadeService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	@DefaultAction
	public ModelAndView listar(WebRequestContext request, IndicadoroportunidadeFiltro filtro) throws Exception{
		request.setAttribute("listaMateriais", materialService.findListMaterialOportunidade());
		request.setAttribute("listaColaboradores", colaboradorService.findListColaboradorOportunidade());
		request.setAttribute("filtro", filtro);
		return new ModelAndView("process/indicadoroportunidade");
	}
	
	public ModelAndView listagem(WebRequestContext request, IndicadoroportunidadeFiltro filtro) throws Exception{
		List<SituacaoOportunidade> listaSituacaoOportunidade = indicadorOportunidadeService.setListagemInfoTrue(request, filtro);
		listaSituacaoOportunidade = indicadorOportunidadeService.setListagemInfo(request, filtro, listaSituacaoOportunidade);
		listaSituacaoOportunidade = indicadorOportunidadeService.getlistaCompletaMes(listaSituacaoOportunidade);
		request.setAttribute("listaMeses", indicadorOportunidadeService.getListaMeses(filtro));
		Arquivo arquivo = Grafico.geraHistogramaOportunidadesMes(listaSituacaoOportunidade);
		if (arquivo != null){
			arquivoService.save(arquivo, null);
			arquivoService.loadAsImage(arquivo);
			try {
				if(arquivo.getCdarquivo() != null){
					DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());
					request.setAttribute("exibefoto", true);
					request.setAttribute("cdArquivo", arquivo.getCdarquivo());
				}
				else{
					request.setAttribute("cdArquivo", null);
				}
			} catch (Exception e) {
				request.setAttribute("exibefoto", false);
			}
		}
		
		return new ModelAndView("direct:ajax/indicadoroportunidadelistagem", "listaSituacaoOportunidade", listaSituacaoOportunidade);
	}
	
	/**
	 * 
	 * M�todo que ir� mostrar a listagem detalhada dos Indicadores de Oportunidade.
	 *
	 *@author Thiago Augusto
	 *@date 15/02/2012
	 * @param whereIn
	 * @return
	 */
	public ModelAndView listagemDetalhada(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<IndicadorortunidadeBean> listaRetorno = new ArrayList<IndicadorortunidadeBean>();
		listaRetorno = indicadorOportunidadeService.carregarListaIndicadoresPorCdOportunidadeHistorico(whereIn);
		Money total = new Money(0);
		
		for (IndicadorortunidadeBean io : listaRetorno) {
			io.setDiferencaDias(indicadorOportunidadeService.getDiferencaDias(io) + " dias");
			io.setDataFormatada(indicadorOportunidadeService.formataDataEvento(io.getData()));
			total = total.add(io.getValor());
		}
		
		listaRetorno.add(new IndicadorortunidadeBean("Total: ", total));
		
		return new ModelAndView("direct:ajax/indicadoroportunidadedetalhe", "listagem", listaRetorno);
	}
}
