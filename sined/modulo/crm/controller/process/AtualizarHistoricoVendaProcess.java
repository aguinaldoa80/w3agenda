package br.com.linkcom.sined.modulo.crm.controller.process;


import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.modulo.crm.controller.process.filter.AtualizarHistoricoVendaFiltro;
import br.com.linkcom.sined.util.SinedException;

@Controller(
		path="/crm/process/AtualizarHistoricoVenda",
		authorizationModule=ProcessAuthorizationModule.class
)
public class AtualizarHistoricoVendaProcess extends MultiActionController{
	
	private ContacrmService contacrmService;
	
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, AtualizarHistoricoVendaFiltro filtro){
		return new ModelAndView("process/atualizarhistorico", "filtro", filtro);
	}
	
	@Input("index")
	public ModelAndView atualizar(WebRequestContext request, AtualizarHistoricoVendaFiltro filtro){
		
		if(filtro.getArquivo() == null || filtro.getArquivo().getContent() == null){
			throw new SinedException("Arquivo inv�lido.");
		}
		
		try{
			String string = new String(filtro.getArquivo().getContent());
			int num = contacrmService.processaArquivoMailmarketing(string);
			request.addMessage(num + " hist�rico(s) atualizado(s) com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na atualiza��o dos hist�ricos.");
			request.addError(e.getMessage());
		}
		return sendRedirectToAction("index");
	}
}
