package br.com.linkcom.sined.modulo.crm.controller.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatofone;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.service.ContacrmcontatoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.SexoService;
import br.com.linkcom.sined.geral.service.TelefonetipoService;

@Controller(
		path={"/crm/process/Contacrmcontato"},
		authorizationModule=ProcessAuthorizationModule.class
)
public class ContacrmcontatoProcess extends MultiActionController{
	
	
	protected TelefonetipoService telefonetipoService;
	protected MunicipioService municipioService;
	protected SexoService sexoService;
	protected ContacrmcontatoService contacrmcontatoService;
	
	
	public void setTelefonetipoService(TelefonetipoService telefonetipoService) {
		this.telefonetipoService = telefonetipoService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	
	public void setSexoService(SexoService sexoService) {
		this.sexoService = sexoService;
	}
	public void setContacrmcontatoService(
			ContacrmcontatoService contacrmcontatoService) {
		this.contacrmcontatoService = contacrmcontatoService;
	}
	
	
	
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, Contacrmcontato contacrmcontato){
		return new ModelAndView("direct:process/popup/criaContacrmcontato","contacrmcontato",contacrmcontato);
	}
	
	@Action("criarnovo")
	public ModelAndView criarnovo(WebRequestContext request, Contacrmcontato contacrmcontato){
		return new ModelAndView("direct:process/popup/crianovoContacrmcontato","contacrmcontato",contacrmcontato);
	}
	
	@SuppressWarnings("unchecked")
	@Action("editar")
	public ModelAndView editar(WebRequestContext request){
		
		Contacrmcontato contacrmcontato = null;
		
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			Integer index = Integer.parseInt((String)object);
			Object attr = request.getSession().getAttribute("listcontacrmcontato");
			List<Contacrmcontato> listaContacrmcontato = null;
			if (attr != null) {
				listaContacrmcontato = (List<Contacrmcontato>) attr;
				contacrmcontato = listaContacrmcontato.get(index);
				contacrmcontato.setIndexlista(index);
				
				if(contacrmcontato.getMunicipio() != null){
					contacrmcontato.setUf(contacrmcontato.getMunicipio().getUf());
				}
				
			}
		}
		
		return new ModelAndView("direct:process/popup/criaContacrmcontato","contacrmcontato",contacrmcontato);
	}
	
	
	@SuppressWarnings("unchecked")
	@Action("excluir")
	public void excluir(WebRequestContext request){
		
		List<Contacrmcontato> listaContacrmcontato = null;
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			String parameter = object.toString();
			parameter = parameter.substring(parameter.indexOf("indice=")+7, parameter.length()-1);
			Integer index = Integer.parseInt(parameter)-1;
			Object attr = request.getSession().getAttribute("listcontacrmcontato");
			if (attr != null) {
				listaContacrmcontato = (List<Contacrmcontato>) attr;
				listaContacrmcontato.remove(index.intValue());
			}
		}
		request.getSession().setAttribute("listcontacrmcontato", listaContacrmcontato);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.close();</script>");
	}
	
	
	@Action("salvanovo")
	public void salvaNovo(WebRequestContext request, Contacrmcontato contacrmcontato){
		contacrmcontatoService.saveOrUpdate(contacrmcontato);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.selecionacontato("+contacrmcontato.getCdcontacrmcontato()+",'" + contacrmcontato.getNome() +"'); window.close();</script>");
	}
	
	@SuppressWarnings("unchecked")
	@Input("criar")
	@Action("salvainteracao")
	public void salvaInteracao(WebRequestContext request, Contacrmcontato contacrmcontato){
		
		Object attribute = request.getSession().getAttribute("listcontacrmcontato");
		List<Contacrmcontato> listaContacrmcontato = null;
		
		Set<Contacrmcontatofone> listaTelefone = contacrmcontato.getListcontacrmcontatofone();
		if (listaTelefone != null) {
			for (Contacrmcontatofone telefone : listaTelefone) {
				telefone.setTelefonetipo(telefonetipoService.load(telefone.getTelefonetipo()));
			}
		}
		
		if(contacrmcontato.getSexo() != null){
			contacrmcontato.setSexo(sexoService.load(contacrmcontato.getSexo()));
			contacrmcontato.setSexoNome(contacrmcontato.getSexo().getNome());
		}
		
		if (attribute == null) {
			listaContacrmcontato = new ArrayList<Contacrmcontato>();
			listaContacrmcontato.add(contacrmcontato);
			
			if (contacrmcontato.getMunicipio() != null && contacrmcontato.getMunicipio().getCdmunicipio() != null) {
				Municipio municipio = contacrmcontato.getMunicipio();
				contacrmcontato.setMunicipio(municipioService.carregaMunicipio(municipio));
			}
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>" +
										"window.opener.montaListaContato('"+contacrmcontato.getNome()+
										"','"+(contacrmcontato.getSexoNome() != null ? contacrmcontato.getSexoNome() : "" )+
										"','"+(contacrmcontato.getTelefones() != null ? contacrmcontato.getTelefones() : "")+
										"','"+(!contacrmcontato.getListaEmail().equals("") ? contacrmcontato.getListaEmail() : "")+
										"');" +
										"window.close();" +
										"</script>");
		} else {
			listaContacrmcontato = (List<Contacrmcontato>)attribute;
			
			if (contacrmcontato.getMunicipio() != null && contacrmcontato.getMunicipio().getCdmunicipio() != null) {
				Municipio municipio = contacrmcontato.getMunicipio();
				contacrmcontato.setMunicipio(municipioService.carregaMunicipio(municipio));
			}
			
			if (contacrmcontato.getIndexlista() != null) {
				listaContacrmcontato.set(contacrmcontato.getIndexlista(), contacrmcontato);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"window.opener.editaItem('"+contacrmcontato.getIndexlista()+
											"','"+contacrmcontato.getNome()+
											"','"+(contacrmcontato.getSexoNome() != null ? contacrmcontato.getSexoNome() : "" )+
											"','"+(contacrmcontato.getTelefones() != null ? contacrmcontato.getTelefones() : "")+
											"','"+(!contacrmcontato.getListaEmail().equals("") ? contacrmcontato.getListaEmail() : "")+
											"');" +
											"window.close();" +
											"</script>");
			} else {
				listaContacrmcontato.add(contacrmcontato);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"window.opener.montaListaContato('"+contacrmcontato.getNome()+
											"','"+ (contacrmcontato.getSexoNome() != null ? contacrmcontato.getSexoNome() : "" )+
											"','"+ (contacrmcontato.getTelefones() != null ? contacrmcontato.getTelefones() : "") +
											"','"+ (!contacrmcontato.getListaEmail().equals("") ? contacrmcontato.getListaEmail() : "") +
											"');" +
											"window.close();" +
											"</script>");
			}
		}	
		
		request.getSession().setAttribute("listcontacrmcontato", listaContacrmcontato);
	}
	

}
