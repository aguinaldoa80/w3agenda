package br.com.linkcom.sined.modulo.crm.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Situacaohistorico;
import br.com.linkcom.sined.geral.service.InteracaoService;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.ControleInteracaoBean;

@Controller(path="/crm/process/ControleInteracao", authorizationModule=ProcessAuthorizationModule.class)
public class ControleInteracaoProcess extends MultiActionController{

	private InteracaoService interacaoService;
	
	public void setInteracaoService(InteracaoService interacaoService) {
		this.interacaoService = interacaoService;
	}

	@DefaultAction
	public ModelAndView index(WebRequestContext request, ControleInteracaoBean filtro) {	
		
		Object filtroSessionObject = request.getSession().getAttribute(filtro.getClass().getCanonicalName());
		if(filtroSessionObject != null){
			filtro = (ControleInteracaoBean) filtroSessionObject;
			request.getSession().removeAttribute(filtro.getClass().getCanonicalName());
		}
		
		List<Situacaohistorico> listaSituacaohistorico = new ArrayList<Situacaohistorico>();
		listaSituacaohistorico.add(Situacaohistorico.VENDA);
		listaSituacaohistorico.add(Situacaohistorico.ROTINA);
		listaSituacaohistorico.add(Situacaohistorico.PERDIDA);
		listaSituacaohistorico.add(Situacaohistorico.SEM_CONTATO);
		
		request.setAttribute("listaSituacaohistorico", listaSituacaohistorico);
		
		return new ModelAndView("/process/controleinteracao", "filtro", filtro);
	}
	
	public ModelAndView listar(WebRequestContext request, ControleInteracaoBean filtro) {
		List<ControleInteracaoBean> listaControleInteracao = interacaoService.getListaControleInteracao(request, filtro);
		request.setAttribute("visualizar", filtro.getVisualizarpor());
		request.setAttribute("tamanhoLista", listaControleInteracao.size());
		return new ModelAndView("direct:ajax/controleinteracaolistagem", "listaControleInteracao", listaControleInteracao);
	}
}
