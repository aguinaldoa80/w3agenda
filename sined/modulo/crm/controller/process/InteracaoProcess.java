package br.com.linkcom.sined.modulo.crm.controller.process;


import java.sql.Date;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.geral.bean.Interacaoitem;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.service.InteracaoService;
import br.com.linkcom.sined.geral.service.InteracaoitemService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.ProspeccaoService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(
		path="/crm/process/Interacao",
		authorizationModule=ProcessAuthorizationModule.class
)
public class InteracaoProcess extends MultiActionController{
	
	private ProspeccaoService prospeccaoService;
	private InteracaoService interacaoService;
	private InteracaoitemService interacaoitemService;
	private PropostaService propostaService;

	public void setPropostaService(PropostaService propostaService) {
		this.propostaService = propostaService;
	}
	public void setInteracaoService(InteracaoService interacaoService) {
		this.interacaoService = interacaoService;
	}
	public void setInteracaoitemService(InteracaoitemService interacaoitemService) {
		this.interacaoitemService = interacaoitemService;
	}
	public void setProspeccaoService(ProspeccaoService prospeccaoService) {
		this.prospeccaoService = prospeccaoService;
	}
	
	
	/**
	 * Carrega a popup de cria��o de um item da intera��o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProspeccaoService#loadWithInteracao
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, Prospeccao prospeccao){
		
		Interacaoitem interacaoitem = new Interacaoitem();
		interacaoitem.setDe(SinedUtil.getUsuarioLogado().getNome());
		if (prospeccao != null && prospeccao.getCdprospeccao() != null) {
			prospeccao = prospeccaoService.loadWithInteracao(prospeccao);
			Interacao interacao = prospeccao.getInteracao();
			interacaoitem.setInteracao(interacao);
			interacaoitem.setProspeccao(prospeccao);
			
		}
		interacaoitem.setData(new Date(System.currentTimeMillis()));
		return new ModelAndView("direct:process/popup/criaInteracao","interacaoitem",interacaoitem);
	}
	
	/**
	 * Carrega a popup de cria��o de um item da intera��o a partir da listagem de proposta.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProspeccaoService#loadWithInteracao
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("criarproposta")
	public ModelAndView criarproposta(WebRequestContext request, Proposta proposta){
		
		Interacaoitem interacaoitem = new Interacaoitem();
		interacaoitem.setDe(SinedUtil.getUsuarioLogado().getNome());
		if (proposta != null && proposta.getCdproposta() != null) {
			proposta = propostaService.loadWithInteracao(proposta);
			Interacao interacao = proposta.getInteracao();
			interacaoitem.setInteracao(interacao);
			interacaoitem.setProposta(proposta);
		}
		interacaoitem.setData(new Date(System.currentTimeMillis()));
		return new ModelAndView("direct:process/popup/criaInteracao","interacaoitem",interacaoitem);
	}
	
	/**
	 * M�todo para consultar uma intera��o.
	 * @param request
	 * @param item - Intera��o item a ser consultada.
	 * @author Murilo
	 * @return
	 */
	@Action("consultar")
	public ModelAndView consultarInteracaoitem(WebRequestContext request, Interacaoitem item){
		return new ModelAndView("direct:process/popup/consultarInteracao", "interacaoitem", interacaoitemService.load(item));
	}
	
	/**
	 * Exclui a interacaoitem da lista no escopo de sess�o. Que est� sendo usado na cria��o de prospec��o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#carregaIndexLista
	 * @param request
	 * @author Rodrigo Freitas
	 */
	@Action("excluir")
	public void excluir(WebRequestContext request){
		
		if(request.getParameter("posicaoLista") != null){
			try{
				interacaoService.carregaIndexLista(request,false);
			} catch (Exception e) {
				request.addError("N�o foi poss�vel excluir a intera��o.");
			}
		} else {
			request.addError("N�o foi poss�vel excluir a intera��o.");
		}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.close();</script>");
	}
	
	
	
	/**
	 * Salva o item de intera��o no objeto interacao e logo depois joga o objeto para a sess�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#verificacaoArquivo
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#salvaInteracaoSessao
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#interacaoNova
	 * @param request
	 * @param interacaoitem
	 * @author Rodrigo Freitas
	 */
	@Action("salvainteracao")
	public void salvaInteracao(WebRequestContext request,Interacaoitem interacaoitem){
		
		interacaoService.verificacaoArquivo(interacaoitem);
		
//		VERIFICA SE O USU�RIO PEDIU PARA EXCLUIR O ARQUIVO
		if("true".equals(request.getParameter("anexo_excludeField"))){
			interacaoitem.setArquivo(null);
		}
		
		
		Prospeccao prospeccao = interacaoitem.getProspeccao();
		Proposta proposta = interacaoitem.getProposta();
		
		if ((prospeccao == null || prospeccao.getCdprospeccao() == null) && (proposta == null || proposta.getCdproposta() == null)) {
//			ENTRA NESTA PARTE SE FOR PELA TELA DE CADASTRO
			Interacao interacao = (Interacao)request.getSession().getAttribute("interacao");
			interacao = interacaoService.salvaInteracaoSessao(interacaoitem, interacao);
			request.getSession().setAttribute("interacao", interacao);
			if (interacaoitem.getArquivo() != null) {
				DownloadFileServlet.addCdfile(request.getSession(), interacaoitem.getArquivo().getCdarquivo());
			}
			if (interacaoitem.getIndexlista() != null) {
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"window.opener.editaItem('"+interacaoitem.getIndexlista()+
											"','"+SinedDateUtils.toString(interacaoitem.getData())+
											"','"+interacaoitem.getDe()+
											"','"+(interacaoitem.getPara() != null ? interacaoitem.getPara() : "")+
											"','"+interacaoitem.getAssunto()+
											"','"+(interacaoitem.getArquivo() == null ? "" : interacaoitem.getArquivo().getNome())+
											"','" +(interacaoitem.getArquivo() == null ? "" : interacaoitem.getArquivo().getCdarquivo()) +
											"');" +
											"window.close();" +
											"</script>");
			} else {
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"window.opener.montaListaInteracao('"+SinedDateUtils.toString(interacaoitem.getData())+
											"','"+interacaoitem.getDe()+
											"','"+(interacaoitem.getPara() != null ? interacaoitem.getPara() : "")+
											"','"+interacaoitem.getAssunto()+
											"','"+(interacaoitem.getArquivo() == null ? "" : interacaoitem.getArquivo().getNome())+
											"','" +(interacaoitem.getArquivo() == null ? "" : interacaoitem.getArquivo().getCdarquivo()) +
											"');" +
											"window.close();" +
											"</script>");
			}
		} else {
			interacaoService.interacaoNova(interacaoitem, prospeccao, proposta);
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>" 
										+"window.close();" +
										"</script>");
			
		}
		
	}
	
	
	
	
	/**
	 * Edita a intera��o da lista na entrada de prospec��o; 
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#carregaIndexLista
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("editar")
	public ModelAndView editar(WebRequestContext request){
		
		Interacaoitem interacaoitem = null;
		if(request.getParameter("posicaoLista") != null){
			try{
				interacaoitem = interacaoService.carregaIndexLista(request,true);
				interacaoitem.setAnexo(interacaoitem.getArquivo());
				
			} catch (Exception e) {
				request.addError("N�o foi poss�vel excluir a intera��o.");
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>window.close();</script>");
			}
		} else {
			request.addError("N�o foi poss�vel excluir a intera��o.");
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.close();</script>");
		}
		
		
		return new ModelAndView("direct:process/popup/criaInteracao","interacaoitem",interacaoitem);
	}
	
	

	
}
