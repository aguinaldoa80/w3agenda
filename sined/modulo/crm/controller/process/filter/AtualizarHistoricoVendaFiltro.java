package br.com.linkcom.sined.modulo.crm.controller.process.filter;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;


public class AtualizarHistoricoVendaFiltro {
	
	private Arquivo arquivo;

	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
}
