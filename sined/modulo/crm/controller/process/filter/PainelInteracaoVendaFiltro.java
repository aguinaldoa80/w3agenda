package br.com.linkcom.sined.modulo.crm.controller.process.filter;

import java.sql.Date;
import java.sql.Timestamp;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Situacaohistorico;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.PainelInteracaoVendaInt;

public class PainelInteracaoVendaFiltro {
	
	private PainelInteracaoVendaInt painelInteracaoVendaInt;
	private Contacrm contacrm;
	private Contacrmcontato contacrmcontato;
	private Oportunidade oportunidade;
	private Timestamp dtinteracao = new Timestamp(System.currentTimeMillis());
	private Atividadetipo atividadetipo;
	private Meiocontato meiocontato;
	private String descricao;
	private Date dtretorno;
	private Situacaohistorico situacaohistorico = Situacaohistorico.VENDA;
	private Empresa empresahistorico;
	
	private Boolean listatodos = Boolean.FALSE;
	
	@DisplayName("Conta")
	public Contacrm getContacrm() {
		return contacrm;
	}
	@DisplayName("Contato")
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}
	public Oportunidade getOportunidade() {
		return oportunidade;
	}
	@DisplayName("Data")
	public Timestamp getDtinteracao() {
		return dtinteracao;
	}
	@DisplayName("Atividade")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	@DisplayName("Meio de contato")
	public Meiocontato getMeiocontato() {
		return meiocontato;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Data de Retorno")
	public Date getDtretorno(){
		return dtretorno;
	}
	public Boolean getListatodos() {
		return listatodos;
	}
	@DisplayName("Pesquisar")
	public PainelInteracaoVendaInt getPainelInteracaoVendaInt() {
		return painelInteracaoVendaInt;
	}
	@DisplayName("Situa��o")
	public Situacaohistorico getSituacaohistorico() {
		return situacaohistorico;
	}
	@DisplayName("Empresa")
	public Empresa getEmpresahistorico() {
		return empresahistorico;
	}
	public void setSituacaohistorico(Situacaohistorico situacaohistorico) {
		this.situacaohistorico = situacaohistorico;
	}
	public void setPainelInteracaoVendaInt(
			PainelInteracaoVendaInt painelInteracaoVendaInt) {
		this.painelInteracaoVendaInt = painelInteracaoVendaInt;
	}
	public void setListatodos(Boolean listatodos) {
		this.listatodos = listatodos;
	}
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	public void setDtinteracao(Timestamp dtinteracao) {
		this.dtinteracao = dtinteracao;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setMeiocontato(Meiocontato meiocontato) {
		this.meiocontato = meiocontato;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setDtretorno(Date dtretorno){
		this.dtretorno = dtretorno;
	}
	public void setEmpresahistorico(Empresa empresahistorico) {
		this.empresahistorico = empresahistorico;
	}

}
