package br.com.linkcom.sined.modulo.crm.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.IndicadorOportunidadeService;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.IndicadoroportunidadeFiltro;

@Controller(path="/crm/process/IndicadoroportunidadeCSV", authorizationModule=ProcessAuthorizationModule.class)
public class IndicadoroportunidadeCSVProcess extends ResourceSenderController<IndicadoroportunidadeFiltro>{
	
	private IndicadorOportunidadeService indicadorOportunidadeService;
	
	public void setIndicadorOportunidadeService(
			IndicadorOportunidadeService indicadorOportunidadeService) {
		this.indicadorOportunidadeService = indicadorOportunidadeService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,
			IndicadoroportunidadeFiltro filtro) throws Exception {
		return indicadorOportunidadeService.prepararArquivoIndicadorOportunidadeCSV(request, filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			IndicadoroportunidadeFiltro filtro) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
