package br.com.linkcom.sined.modulo.crm.controller.process.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Situacaohistorico;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoControleInteracao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoagendainteracao;
import br.com.linkcom.sined.geral.bean.enumeration.VisualizarControleInteracao;

public class ControleInteracaoBean {

	protected Colaborador colaborador;
	protected Tipoagendainteracao tipoagendainteracao;
	protected Cliente cliente;
	protected Categoria categoria;
	protected Contacrm contacrm;
	protected Segmento segmento;
	protected VisualizarControleInteracao visualizarpor = VisualizarControleInteracao.SEMANAL;
	protected Date datade;
	protected Date dataate;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Situacaohistorico situacaohistorico;
	protected List<Situacaohistorico> listaSituacaohistorico;
	protected String contato;
	protected String telefones;
	protected String cidadeuf;
	protected String categorias;
	protected String segmentos;
	protected List<Contacrmhistorico> listaHistoricoContacrm;
	protected List<Clientehistorico> listaHistoricoCliente;
	protected Municipio municipio;
	protected Uf uf;
	protected OrdenacaoControleInteracao ordenacao;
	
	protected String nome;
	protected String tipo;
	protected String periodo1;
	protected String periodo2;
	protected String periodo3;
	protected String periodo4;
	protected Integer situacao1;
	protected Integer situacao2;
	protected Integer situacao3;
	protected Integer situacao4;
	
	protected String municipionome;
	protected String ufsigla;
	
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Tipoagendainteracao getTipoagendainteracao() {
		return tipoagendainteracao;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public Contacrm getContacrm() {
		return contacrm;
	}
	public Segmento getSegmento() {
		return segmento;
	}
	public VisualizarControleInteracao getVisualizarpor() {
		return visualizarpor;
	}
	@Required
	public Date getDatade() {
		return datade;
	}
	@Required
	public Date getDataate() {
		return dataate;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Situacaohistorico getSituacaohistorico() {
		return situacaohistorico;
	}
	public List<Situacaohistorico> getListaSituacaohistorico() {
		return listaSituacaohistorico;
	}
	public String getContato() {
		return contato;
	}
	public String getTelefones() {
		return telefones;
	}
	public String getCidadeuf() {
		return cidadeuf;
	}
	public List<Contacrmhistorico> getListaHistoricoContacrm() {
		return listaHistoricoContacrm;
	}
	public List<Clientehistorico> getListaHistoricoCliente() {
		return listaHistoricoCliente;
	}
	public String getCategorias() {
		return categorias;
	}
	public String getSegmentos() {
		return segmentos;
	}
	public Municipio getMunicipio() {
		return municipio;
	}
	public Uf getUf() {
		return uf;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setSegmentos(String segmentos) {
		this.segmentos = segmentos;
	}
	public void setCategorias(String categorias) {
		this.categorias = categorias;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setTipoagendainteracao(Tipoagendainteracao tipoagendainteracao) {
		this.tipoagendainteracao = tipoagendainteracao;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	public void setVisualizarpor(VisualizarControleInteracao visualizarpor) {
		this.visualizarpor = visualizarpor;
	}
	public void setDatade(Date datade) {
		this.datade = datade;
	}
	public void setDataate(Date dataate) {
		this.dataate = dataate;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setSituacaohistorico(Situacaohistorico situacaohistorico) {
		this.situacaohistorico = situacaohistorico;
	}
	public void setListaSituacaohistorico(
			List<Situacaohistorico> listaSituacaohistorico) {
		this.listaSituacaohistorico = listaSituacaohistorico;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public void setContato(Cliente cliente) {
		if(cliente.getListaContato() != null && !cliente.getListaContato().isEmpty()){
			StringBuilder sb = new StringBuilder("");
			for (PessoaContato pessoaContato : cliente.getListaContato()) {
				Contato contato = pessoaContato.getContato();
				
				if(contato.getAtivo() != null && contato.getAtivo()){
					sb.append(contato.getNome());
					if(contato.getContatotipo() != null && contato.getContatotipo().getNome() != null){
						sb.append("(").append(contato.getContatotipo().getNome()).append(")");
					}
					sb.append(", ");
				}
			}
			
			if(sb.length() > 0) this.contato = sb.substring(0, sb.length()-2).toString();
		}
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	public void setCidadeuf(String cidadeuf) {
		this.cidadeuf = cidadeuf;
	}
	public void setListaHistoricoContacrm(
			List<Contacrmhistorico> listaHistoricoContacrm) {
		this.listaHistoricoContacrm = listaHistoricoContacrm;
	}
	public void setListaHistoricoCliente(
			List<Clientehistorico> listaHistoricoCliente) {
		this.listaHistoricoCliente = listaHistoricoCliente;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public OrdenacaoControleInteracao getOrdenacao() {
		return ordenacao;
	}
	public void setOrdenacao(OrdenacaoControleInteracao ordenacao) {
		this.ordenacao = ordenacao;
	}

	public String getNome() {
		if(nome == null){
			if(cliente != null && cliente.getNome() != null){
				nome = cliente.getNome();
			} else if(contacrm != null && contacrm.getNome() != null){
				nome = contacrm.getNome();
			}
		} else{
			nome = "";
		}
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		if(tipo == null && tipoagendainteracao != null){
			tipo = tipoagendainteracao.getNome();
		} else{
			tipo = "";
		}
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getPeriodo1() {
		if(periodo1 == null){
			setPeriodos();
		}
		return periodo1;
	}
	public void setPeriodo1(String periodo1) {
		this.periodo1 = periodo1;
	}
	public String getPeriodo2() {
		if(periodo2 == null){
			setPeriodos();
		}
		return periodo2;
	}
	public void setPeriodo2(String periodo2) {
		this.periodo2 = periodo2;
	}
	public String getPeriodo3() {
		if(periodo3 == null){
			setPeriodos();
		}
		return periodo3;
	}
	public void setPeriodo3(String periodo3) {
		this.periodo3 = periodo3;
	}
	public String getPeriodo4() {
		if(periodo4 == null){
			setPeriodos();
		}
		return periodo4;
	}
	
	public Integer getSituacao1() {
		if(situacao1 == null){
			setPeriodos();
		}
		return situacao1;
	}
	public void setSituacao1(Integer situacao1) {
		this.situacao1 = situacao1;
	}
	
	public Integer getSituacao2() {
		if(situacao2 == null){
			setPeriodos();
		}
		return situacao2;
	}
	public void setSituacao2(Integer situacao2) {
		this.situacao2 = situacao2;
	}
	
	public Integer getSituacao3() {
		if(situacao3 == null){
			setPeriodos();
		}
		return situacao3;
	}
	public void setSituacao3(Integer situacao3) {
		this.situacao3 = situacao3;
	}
	
	public Integer getSituacao4() {
		if(situacao4 == null){
			setPeriodos();
		}
		return situacao4;
	}
	public void setSituacao4(Integer situacao4) {
		this.situacao4 = situacao4;
	}
	
	private void setPeriodos(){
		if(listaHistoricoCliente != null && listaHistoricoCliente.size() == 4){
			Clientehistorico ch1 = listaHistoricoCliente.get(0);
			if(ch1 != null){
				periodo1 = ch1.getDataHistorico();
				situacao1 = ch1.getSituacaohistorico() != null ? ch1.getSituacaohistorico().getCdsituacaohistorico() : 2;
			}
			Clientehistorico ch2 = listaHistoricoCliente.get(1);
			if(ch2 != null){
				periodo2 = ch2.getDataHistorico();
				situacao2 = ch2.getSituacaohistorico() != null ? ch2.getSituacaohistorico().getCdsituacaohistorico() : 2;
			}
			Clientehistorico ch3 = listaHistoricoCliente.get(2);
			if(ch3 != null){
				periodo3 = ch3.getDataHistorico();
				situacao3 = ch3.getSituacaohistorico() != null ? ch3.getSituacaohistorico().getCdsituacaohistorico() : 2;
			}
			Clientehistorico ch4 = listaHistoricoCliente.get(3);
			if(ch4 != null){
				periodo4 = ch4.getDataHistorico();
				situacao4 = ch4.getSituacaohistorico() != null ? ch4.getSituacaohistorico().getCdsituacaohistorico() : 2;
			}
		} else if(listaHistoricoContacrm != null && listaHistoricoContacrm.size() == 4){
			Contacrmhistorico ch1 = listaHistoricoContacrm.get(0);
			if(ch1 != null){
				periodo1 = ch1.getDataHistorico();
				situacao1 = ch1.getSituacaohistorico() != null ? ch1.getSituacaohistorico().getCdsituacaohistorico() : 2;
			}
			Contacrmhistorico ch2 = listaHistoricoContacrm.get(1);
			if(ch2 != null){
				periodo2 = ch2.getDataHistorico();
				situacao2 = ch2.getSituacaohistorico() != null ? ch2.getSituacaohistorico().getCdsituacaohistorico() : 2;
			}
			Contacrmhistorico ch3 = listaHistoricoContacrm.get(2);
			if(ch3 != null){
				periodo3 = ch3.getDataHistorico();
				situacao3 = ch3.getSituacaohistorico() != null ? ch3.getSituacaohistorico().getCdsituacaohistorico() : 2;
			}
			Contacrmhistorico ch4 = listaHistoricoContacrm.get(3);
			if(ch4 != null){
				periodo4 = ch4.getDataHistorico();
				situacao4 = ch4.getSituacaohistorico() != null ? ch4.getSituacaohistorico().getCdsituacaohistorico() : 2;
			}
		}
	}
	public void setPeriodo4(String periodo4) {
		this.periodo4 = periodo4;
	}
	
	public String getMunicipionome() {
		return municipionome;
	}
	public String getUfsigla() {
		return ufsigla;
	}
	public void setMunicipionome(String municipionome) {
		this.municipionome = municipionome;
	}
	public void setUfsigla(String ufsigla) {
		this.ufsigla = ufsigla;
	}
}
