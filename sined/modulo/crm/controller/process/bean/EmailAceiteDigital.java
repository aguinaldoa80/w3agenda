package br.com.linkcom.sined.modulo.crm.controller.process.bean;

public class EmailAceiteDigital {
	
	private String cliente_nome;
	private String empresa_nome;
	private String link;
	
	public String getCliente_nome() {
		return cliente_nome;
	}
	public String getEmpresa_nome() {
		return empresa_nome;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public void setCliente_nome(String cliente_nome) {
		this.cliente_nome = cliente_nome;
	}
	public void setEmpresa_nome(String empresa_nome) {
		this.empresa_nome = empresa_nome;
	}

}
