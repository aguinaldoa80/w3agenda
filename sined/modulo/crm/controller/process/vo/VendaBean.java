package br.com.linkcom.sined.modulo.crm.controller.process.vo;

import java.sql.Date;

public class VendaBean {
	
	private Integer cdvenda;
	private String tipo;
	private Date dtvenda;
	private String materiais;
	
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setDtvenda(Date date) {
		this.dtvenda = date;
	}
	public Integer getCdvenda() {
		return cdvenda;
	}
	public Date getDtvenda() {
		return dtvenda;
	}
	public String getMateriais() {
		return materiais;
	}
	public void setMateriais(String materiais) {
		this.materiais = materiais;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
