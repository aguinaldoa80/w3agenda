package br.com.linkcom.sined.modulo.crm.controller.process.vo;

import java.sql.Timestamp;

public class AtualizarHistoricoVO {
	
	private String email;
	private Timestamp dtenvio;
	
	public String getEmail() {
		return email;
	}
	public Timestamp getDtenvio() {
		return dtenvio;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setDtenvio(Timestamp dtenvio) {
		this.dtenvio = dtenvio;
	}
	
}
