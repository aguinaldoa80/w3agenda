package br.com.linkcom.sined.modulo.crm.controller.process.vo;

import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Requisicao;

public class HistoricoPainelVO {
	private List<Requisicao> listaRequisicao;
	private List<ClientehistoricoVO> listaClienteHistorico;
	private List<Proposta> listaProposta;
	private List<Documento> listaPagamento;
	private List<VendaBean> listaVenda;
	private List<Oportunidade> listaOportunidade;
	private List<Agendainteracao> listaAgendainteracao;
	
	private Money totalemAberto;
	private Money creditolimitecompra;
	private Money saldopagamentoantecipado;
	private Money saldoValeCompras;
	private Boolean exibirAlertaHistorico;
	
	public void setListaClienteHistorico(
			List<ClientehistoricoVO> listaClienteHistorico) {
		this.listaClienteHistorico = listaClienteHistorico;
	}
	
	public void setListaRequisicao(List<Requisicao> listaRequisicao) {
		this.listaRequisicao = listaRequisicao;
	}
	
	public void setListaProposta(List<Proposta> listaProposta) {
		this.listaProposta = listaProposta;
	}
	public void setListaPagamento(List<Documento> listaPagamento) {
		this.listaPagamento = listaPagamento;
	}
	public void setListaVenda(List<VendaBean> listaVenda) {
		this.listaVenda = listaVenda;
	}
	public List<Agendainteracao> getListaAgendainteracao() {
		return listaAgendainteracao;
	}
	public void setListaAgendainteracao(
			List<Agendainteracao> listaAgendainteracao) {
		this.listaAgendainteracao = listaAgendainteracao;
	}
	
	
	public List<ClientehistoricoVO> getListaClienteHistorico() {
		return listaClienteHistorico;
	}
	
	public List<Requisicao> getListaRequisicao() {
		return listaRequisicao;
	}
	
	public List<Proposta> getListaProposta() {
		return listaProposta;
	}
	public List<Documento> getListaPagamento() {
		return listaPagamento;
	}
	public List<VendaBean> getListaVenda() {
		return listaVenda;
	}
	
	public List<Oportunidade> getListaOportunidade() {
		return listaOportunidade;
	}

	public Money getTotalemAberto() {
		return totalemAberto;
	}
	public void setTotalemAberto(Money totalemAberto) {
		this.totalemAberto = totalemAberto;
	}
	public Money getCreditolimitecompra() {
		return creditolimitecompra;
	}
	public void setCreditolimitecompra(Money creditolimitecompra) {
		this.creditolimitecompra = creditolimitecompra;
	}
	public Money getSaldopagamentoantecipado() {
		return saldopagamentoantecipado;
	}
	public void setSaldopagamentoantecipado(Money saldopagamentoantecipado) {
		this.saldopagamentoantecipado = saldopagamentoantecipado;
	}
	public Money getSaldoValeCompras() {
		return saldoValeCompras;
	}
	public void setSaldoValeCompras(Money saldoValeCompras) {
		this.saldoValeCompras = saldoValeCompras;
	}
	public Boolean getExibirAlertaHistorico() {
		return exibirAlertaHistorico;
	}
	public void setExibirAlertaHistorico(Boolean exibirAlertaHistorico) {
		this.exibirAlertaHistorico = exibirAlertaHistorico;
	}
	public void setListaOportunidade(List<Oportunidade> listaOportunidade) {
		this.listaOportunidade = listaOportunidade;
	}
}