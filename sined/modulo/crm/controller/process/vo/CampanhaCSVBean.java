package br.com.linkcom.sined.modulo.crm.controller.process.vo;

public class CampanhaCSVBean {
	
	private String nome;
	private String email;
	
	public CampanhaCSVBean() {
	
	}
	
	public CampanhaCSVBean(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}
	
	public String getNome() {
		return nome;
	}
	public String getEmail() {
		return email;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}
