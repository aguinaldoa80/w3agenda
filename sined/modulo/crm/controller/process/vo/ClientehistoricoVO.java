package br.com.linkcom.sined.modulo.crm.controller.process.vo;

import java.sql.Timestamp;

import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.enumeration.AtividadetipoSituacaoEnum;

public class ClientehistoricoVO {
	
	protected Integer cdclientehistorico;
	protected String observacao;
	protected String cliente_nome;
	protected String contato_nome;
	protected String meiocontato_nome;
	protected String contrato_descricao;
	protected String atividadetipo_nome;
	protected AtividadetipoSituacaoEnum situacao;
	protected String situacaohistorico_descricao;
	protected String empresahistorico_nome;
	protected String  usuarioaltera_nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public ClientehistoricoVO(){}
	
	public ClientehistoricoVO(Clientehistorico clientehistorico){
		if(clientehistorico != null){
			this.cdclientehistorico = clientehistorico.getCdclientehistorico();
			this.observacao = clientehistorico.getObservacao();
			this.cliente_nome = clientehistorico.getCliente() != null ? clientehistorico.getCliente().getNome() : null;
			this.contato_nome = clientehistorico.getContato() != null ? clientehistorico.getContato().getNome() : null;
			this.meiocontato_nome = clientehistorico.getMeiocontato() != null ? clientehistorico.getMeiocontato().getNome() : null;
			this.contrato_descricao = clientehistorico.getContrato() != null ? clientehistorico.getContrato().getDescricao() : null;
			this.atividadetipo_nome = clientehistorico.getAtividadetipo() != null ? clientehistorico.getAtividadetipo().getNome() : null;
			this.situacao = clientehistorico.getSituacao();
			this.situacaohistorico_descricao = clientehistorico.getSituacaohistorico() != null ? clientehistorico.getSituacaohistorico().getDescricao() : null;
			this.empresahistorico_nome = clientehistorico.getEmpresahistorico() != null ? clientehistorico.getEmpresahistorico().getNome() : null;
			this.usuarioaltera_nome = clientehistorico.getUsuarioaltera() != null ? clientehistorico.getUsuarioaltera().getNome() : null;
			this.cdusuarioaltera = clientehistorico.getCdusuarioaltera();
			this.dtaltera = clientehistorico.getDtaltera();
		}
	}
	
	public Integer getCdclientehistorico() {
		return cdclientehistorico;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getCliente_nome() {
		return cliente_nome;
	}
	public String getContato_nome() {
		return contato_nome;
	}
	public String getMeiocontato_nome() {
		return meiocontato_nome;
	}
	public String getContrato_descricao() {
		return contrato_descricao;
	}
	public String getAtividadetipo_nome() {
		return atividadetipo_nome;
	}
	public AtividadetipoSituacaoEnum getSituacao() {
		return situacao;
	}
	public String getSituacaohistorico_descricao() {
		return situacaohistorico_descricao;
	}
	public String getEmpresahistorico_nome() {
		return empresahistorico_nome;
	}
	public String getUsuarioaltera_nome() {
		return usuarioaltera_nome;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdclientehistorico(Integer cdclientehistorico) {
		this.cdclientehistorico = cdclientehistorico;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}
	public void setContato_nome(String contatoNome) {
		contato_nome = contatoNome;
	}
	public void setMeiocontato_nome(String meiocontatoNome) {
		meiocontato_nome = meiocontatoNome;
	}
	public void setContrato_descricao(String contratoDescricao) {
		contrato_descricao = contratoDescricao;
	}
	public void setAtividadetipo_nome(String atividadetipoNome) {
		atividadetipo_nome = atividadetipoNome;
	}
	public void setSituacao(AtividadetipoSituacaoEnum situacao) {
		this.situacao = situacao;
	}
	public void setSituacaohistorico_descricao(String situacaohistoricoDescricao) {
		situacaohistorico_descricao = situacaohistoricoDescricao;
	}
	public void setEmpresahistorico_nome(String empresahistoricoNome) {
		empresahistorico_nome = empresahistoricoNome;
	}
	public void setUsuarioaltera_nome(String usuarioalteraNome) {
		usuarioaltera_nome = usuarioalteraNome;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}