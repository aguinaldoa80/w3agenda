package br.com.linkcom.sined.modulo.crm.controller.process;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadehistorico;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContacrmcontatoService;
import br.com.linkcom.sined.geral.service.ContacrmhistoricoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.MeiocontatoService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OportunidadehistoricoService;
import br.com.linkcom.sined.modulo.crm.controller.process.filter.PainelInteracaoVendaFiltro;

@Controller(
		path="/crm/process/PainelInteracaoVenda",
		authorizationModule=ProcessAuthorizationModule.class
)

public class PainelInteracaoVendaProcess extends MultiActionController{
	
	private OportunidadeService oportunidadeService;
	private ContacrmcontatoService contacrmcontatoService;
	private LeadhistoricoService leadhistoricoService;
	private ContacrmhistoricoService contacrmhistoricoService;
	private OportunidadehistoricoService oportunidadehistoricoService;
	private ContacrmService contacrmService;
	private ClienteService clienteService;
	private MeiocontatoService meiocontatoService;
	private AtividadetipoService atividadetipoService;
	private EmpresaService empresaService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	public void setOportunidadehistoricoService(
			OportunidadehistoricoService oportunidadehistoricoService) {
		this.oportunidadehistoricoService = oportunidadehistoricoService;
	}
	public void setContacrmhistoricoService(
			ContacrmhistoricoService contacrmhistoricoService) {
		this.contacrmhistoricoService = contacrmhistoricoService;
	}
	public void setLeadhistoricoService(
			LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setContacrmcontatoService(
			ContacrmcontatoService contacrmcontatoService) {
		this.contacrmcontatoService = contacrmcontatoService;
	}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setMeiocontatoService(MeiocontatoService meiocontatoService) {
		this.meiocontatoService = meiocontatoService;
	}
	public void setAtividadetipoService(
			AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@DefaultAction
	public ModelAndView handle(WebRequestContext request, PainelInteracaoVendaFiltro filtro){
		if(filtro == null) filtro = new PainelInteracaoVendaFiltro();
		
		String cdContacrm = request.getParameter("cdContacrm");
		if(cdContacrm != null){
			String nomeContacrm =  request.getParameter("nomeContacrm");
			Contacrm contacrm = new Contacrm();			
			contacrm.setCdcontacrm(Integer.parseInt(cdContacrm));
			contacrm.setNome(nomeContacrm);
			filtro.setContacrm(contacrm);
		}
		String fechaPopUp = request.getParameter("fechaPopUp");
		request.setAttribute("fechaPopUp", fechaPopUp);
		request.setAttribute("listaMeioContato", meiocontatoService.findAtivos());
		request.setAttribute("listaAtividadetipo", atividadetipoService.findForCrm());
		request.setAttribute("listaEmpresasByUsuario", empresaService.findByUsuario());
		filtro.setEmpresahistorico(empresaService.loadPrincipal());

		return new ModelAndView("process/painelinteracaovenda", "filtro", filtro);
	}
	
	public void verificaCliente(WebRequestContext requestContext, PainelInteracaoVendaFiltro filtro){
		
		Contacrm contacrm = contacrmService.load(filtro.getContacrm(), "contacrm.cdcontacrm, contacrm.cpf, contacrm.cnpj, contacrm.tipo");
		if(contacrm != null){
			Cliente cliente = null;
			if(contacrm.getTipo().equals(Tipopessoa.PESSOA_FISICA) && contacrm.getCpf() != null){
				cliente = clienteService.findByCpf(contacrm.getCpf());
			} else if(contacrm.getTipo().equals(Tipopessoa.PESSOA_JURIDICA) && contacrm.getCnpj() != null){
				cliente = clienteService.findByCnpj(contacrm.getCnpj());
			}
			
			if(cliente != null && cliente.getCdpessoa() != null){
				View.getCurrent().println("clienteCriado = 'br.com.linkcom.sined.geral.bean.Cliente[cdpessoa=" + cliente.getCdpessoa() + "]';");
			} else {
				View.getCurrent().println("clienteCriado = null;");
			}
		} else {
			View.getCurrent().println("clienteCriado = null;");
		}
	}
	
	/**
	 * Redireciona para tela de Intera��o de vendas, com os campos preenchidos,
	 * a partir da tela da listagem de Oportunidade.
	 * @param request
	 * @param oportunidade
	 * @return
	 * @throws Exception
	 * @author Taidson
	 * @since 03/09/2010
	 */
	public ModelAndView interacaoOportunidade(WebRequestContext request, Oportunidade oportunidade) throws Exception{
		request.getServletResponse().setContentType("text/html");
		String fullName = "'br.com.linkcom.sined.geral.bean.Oportunidade[cdoportunidade=";

		oportunidade = oportunidadeService.loadForInteracao(oportunidade);

		PainelInteracaoVendaFiltro filtro = new PainelInteracaoVendaFiltro();
		Oportunidade op = new Oportunidade();
		op.setCdoportunidade(oportunidade.getCdoportunidade());
		op.setNome(oportunidade.getNome());
		filtro.setOportunidade(op);
		filtro.setContacrm(oportunidade.getContacrm());
		filtro.setContacrmcontato(oportunidade.getContacrmcontato());
		
		request.setAttribute("optde", fullName + op.getCdoportunidade()+",nome="+op.getNome()+"]'");
				
		return handle(request, filtro);	
		
	}
	
	
	
	public void saveHistorico(WebRequestContext requestContext, PainelInteracaoVendaFiltro filtro){
		if(filtro.getOportunidade() != null){
			Oportunidadehistorico oh = new Oportunidadehistorico();
			oh.setAtividadetipo(filtro.getAtividadetipo());
			oh.setCdusuarioaltera(((Usuario)Neo.getUser()).getCdpessoa());
			oh.setContacrmcontato(filtro.getContacrmcontato());
			oh.setDtaltera(new Timestamp(System.currentTimeMillis()));
			oh.setMeiocontato(filtro.getMeiocontato());
			oh.setObservacao(filtro.getDescricao());
			oh.setOportunidade(filtro.getOportunidade());
			oh.setEmpresahistorico(filtro.getEmpresahistorico());
			
			
			Oportunidade op = oportunidadeService.load(filtro.getOportunidade(), "oportunidade.cdoportunidade, oportunidade.oportunidadesituacao, oportunidade.probabilidade");
			
			op.setDtretorno(filtro.getDtretorno());
			
			oportunidadeService.atualizaDataRetornoOportunidade(op);
			
			
			oh.setOportunidadesituacao(op.getOportunidadesituacao());
			oh.setProbabilidade(op.getProbabilidade());
			
			oportunidadehistoricoService.saveOrUpdate(oh);
		} else {
			Contacrmhistorico ch = new Contacrmhistorico();
			ch.setAtividadetipo(filtro.getAtividadetipo());
			ch.setCdusuarioaltera(((Usuario)Neo.getUser()).getCdpessoa());
			ch.setContacrmcontato(filtro.getContacrmcontato());
			ch.setDtaltera(new Timestamp(System.currentTimeMillis()));
			ch.setMeiocontato(filtro.getMeiocontato());
			ch.setObservacao(filtro.getDescricao());
			ch.setContacrm(filtro.getContacrm());
			ch.setSituacaohistorico(filtro.getSituacaohistorico());
			ch.setEmpresahistorico(filtro.getEmpresahistorico());
			
			contacrmhistoricoService.saveOrUpdate(ch);
		}
		
		String fechaPopUp = requestContext.getParameter("fechaPopUp");
		if(fechaPopUp != null && "TRUE".equals(fechaPopUp.toUpperCase())){
			View.getCurrent().println("{fechaPopUp:true}");
		} else {
			View.getCurrent().println("{fechaPopUp:false}");
		}
	}
	
	public ModelAndView loadListaHistoricoConta(WebRequestContext request, PainelInteracaoVendaFiltro filtro){
		List<Leadhistorico> leadHistorico = leadhistoricoService.findByContacrm(filtro.getContacrm(), filtro.getAtividadetipo());
		List<Contacrmhistorico> contaHistorico = contacrmhistoricoService.findByContacrm(filtro.getContacrm(), filtro.getAtividadetipo(), filtro.getEmpresahistorico());
		contaHistorico.addAll(this.convertToContaHistorico(leadHistorico));
		
		Collections.sort(contaHistorico,new Comparator<Contacrmhistorico>(){
			public int compare(Contacrmhistorico o1, Contacrmhistorico o2) {
				return o2.getDtaltera().compareTo(o1.getDtaltera());
			}
		});
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		Boolean exibirAlertaHistoricoConta = Boolean.FALSE;
		
		if (filtro.getAtividadetipo() != null && filtro.getListatodos() != null && !filtro.getListatodos()) {
			exibirAlertaHistoricoConta = Boolean.TRUE;
		}
		
		jsonModelAndView.addObject("exibirAlertaHistoricoConta", exibirAlertaHistoricoConta);
		
		if((filtro.getListatodos() != null && filtro.getListatodos()) || contaHistorico.size() < 20){
			jsonModelAndView.addObject("listaHistoricoConta", contaHistorico);
		} else {
			jsonModelAndView.addObject("listaHistoricoConta", contaHistorico.subList(0, 20));
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView loadListaHistoricoOportunidade(WebRequestContext request, PainelInteracaoVendaFiltro filtro){
		
		List<Oportunidadehistorico> oportunidadeHistorico = oportunidadehistoricoService.findByOportunidade(filtro.getOportunidade(),
																											filtro.getAtividadetipo(),
																											filtro.getEmpresahistorico());
		
		Collections.sort(oportunidadeHistorico,new Comparator<Oportunidadehistorico>(){
			public int compare(Oportunidadehistorico o1, Oportunidadehistorico o2) {
				return o2.getDtaltera().compareTo(o1.getDtaltera());
			}
		});
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		Boolean exibirAlertaHistoricoOportunidade = Boolean.FALSE;
		
		if (filtro.getAtividadetipo() != null && filtro.getListatodos() != null && !filtro.getListatodos()) {
			exibirAlertaHistoricoOportunidade = Boolean.TRUE;
		}
		
		jsonModelAndView.addObject("exibirAlertaHistoricoOportunidade", exibirAlertaHistoricoOportunidade);
		
		if((filtro.getListatodos() != null && filtro.getListatodos()) || oportunidadeHistorico.size() < 20){
			jsonModelAndView.addObject("listaHistoricoOportunidade", oportunidadeHistorico);
		} else {
			jsonModelAndView.addObject("listaHistoricoOportunidade", oportunidadeHistorico.subList(0, 20));
		}
		
		return jsonModelAndView;
	}
	
	private List<Contacrmhistorico> convertToContaHistorico(List<Leadhistorico> leadHistorico) {
		List<Contacrmhistorico> lista = new ArrayList<Contacrmhistorico>();
		Contacrmhistorico ch;
		for (Leadhistorico lh : leadHistorico) {
			ch = new Contacrmhistorico();
			ch.setCdusuarioaltera(lh.getCdusuarioaltera());
			ch.setDtaltera(lh.getDtaltera());
			ch.setObservacao(lh.getObservacao());
			
			lista.add(ch);
		}
		return lista;
	}
	
	public ModelAndView ajaxComboContato(WebRequestContext request, PainelInteracaoVendaFiltro filtro){
		List<Contacrmcontato> lista = contacrmcontatoService.findByContacrmForCombo(filtro.getContacrm());
		return new JsonModelAndView().addObject("listaContato", lista);
	}
	
	public ModelAndView ajaxComboOportunidade(WebRequestContext request, PainelInteracaoVendaFiltro filtro){
		List<Oportunidade> lista = oportunidadeService.findByContacrmForCombo(filtro.getContacrm());
		return new JsonModelAndView().addObject("listaOportunidade", lista);
	}
	
	public ModelAndView loadCampos(WebRequestContext request, PainelInteracaoVendaFiltro filtro){
		JsonModelAndView json = new JsonModelAndView();
		
		if(filtro.getOportunidade() != null){
			Oportunidade oportunidade = oportunidadeService.loadForInteracao(filtro.getOportunidade());
			oportunidade.getContacrm().setNomeEscape(Util.strings.addScapesToDescription(oportunidade.getContacrm().getNome()));
			
			json.addObject("oportunidade", oportunidade);
			json.addObject("contacrm", oportunidade.getContacrm());
		} else if(filtro.getContacrm() != null){
			Contacrm contacrm = contacrmService.load(filtro.getContacrm(), "contacrm.cdcontacrm, contacrm.nome");
			contacrm.setNomeEscape(Util.strings.addScapesToDescription(contacrm.getNome()));
				
			json.addObject("oportunidade", null);
			json.addObject("contacrm", contacrm);
		}
		
		return json;
	}
}
