package br.com.linkcom.sined.modulo.crm.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;

@Controller(
		path="/crm/process/ClienteCSV",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ClienteCSVProcess extends ResourceSenderController<ClienteFiltro>{
	
	private ClienteService clienteService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	ClienteFiltro filtro) throws Exception {
		return clienteService.prepararArquivoClienteCSV(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, ClienteFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/crm/crud/Cliente");
	}
}
