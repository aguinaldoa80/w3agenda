package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/suprimento/process/FluxoStatusCotacao",
		authorizationModule=ProcessAuthorizationModule.class
)
public class FluxoStatusCotacaoProcess extends MultiActionController{
	
	private CotacaoService cotacaoService;
	private SolicitacaocompraService solicitacaocompraService;
	
	public void setSolicitacaocompraService(
			SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	
	@DefaultAction
	public ModelAndView atualizar(WebRequestContext request){
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		if("1".equals(request.getParameter("STATUSACTION"))){
			List<Solicitacaocompra> listaSolicitacoes = new ArrayList<Solicitacaocompra>();
			List<Cotacao> listaCotacao = cotacaoService.findForVerificacao(whereIn);
			if (listaCotacao != null && listaCotacao.size() > 0) {
				for (Cotacao cotacao : listaCotacao) {
					/*if (!cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) && 
							!cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO)) {
						request.addError("Situa��o da(s) cota��o(�es) diferente de 'em aberto' e 'em cota��o'.");
						return getControllerModelAndView(request);
					}*/
					if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.ORDEMCOMPRA_GERADA)) {
						request.addError("Cancelamento n�o permitido. Cota��o est� com ordem de compra gerada.");
						return getControllerModelAndView(request);
					} else if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)) {
						request.addError("Cancelamento n�o permitido. Cota��o est� baixada.");
						return getControllerModelAndView(request);
					} else if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)) {
						request.addError("A cota��o j� est� cancelada.");
						return getControllerModelAndView(request);
					}
					
					listaSolicitacoes.addAll(solicitacaocompraService.findByCotacao(cotacao));
					
				}
			}
			
			cotacaoService.updateStatus(whereIn,Situacaosuprimentos.CANCELADA);
			request.addMessage("Cota��o(�es) canceladas com sucesso.");
			
			if(listaSolicitacoes != null && !listaSolicitacoes.isEmpty()){
				String whereInSolicitacaoCompra = "";
				whereInSolicitacaoCompra = SinedUtil.listAndConcatenate(listaSolicitacoes, "cdsolicitacaocompra", ",");
				if(!"".equals(whereInSolicitacaoCompra)){
					cotacaoService.updateSituacaoSolicitacaoCompra(whereInSolicitacaoCompra);
				}
			}
			
		} else if("2".equals(request.getParameter("STATUSACTION"))){
			
			List<Cotacao> listaCotacao = cotacaoService.findForVerificacao(whereIn);
			if (listaCotacao != null && listaCotacao.size() > 0) {
				for (Cotacao cotacao : listaCotacao) {
					if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)) {
						request.addError("Cota��o j� est� baixada.");
						return getControllerModelAndView(request);
					} else if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)) {
						request.addError("Baixa n�o permitida. A cota��o est� cancelada.");
						return getControllerModelAndView(request);
					}  else if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.ORDEMCOMPRA_GERADA)) {
						request.addError("Cota��o deve aguardar a baixa das suas ordens de compra geradas.");
						return getControllerModelAndView(request);
					} 
				}
			}
			
			cotacaoService.updateStatus(whereIn,Situacaosuprimentos.BAIXADA);
			request.addMessage("Cota��o(�es) baixadas com sucesso.");
			
		} else if("3".equals(request.getParameter("STATUSACTION"))){
			
			
			List<Cotacao> listaCotacao = cotacaoService.findForVerificacao(whereIn);
			List<Solicitacaocompra> lista = null;
			String string = null;
			
			if (listaCotacao != null && listaCotacao.size() > 0) {
				for (Cotacao cotacao : listaCotacao) {
					if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO)) {
						request.addError("Cota��o j� est� estornada.");
						return getControllerModelAndView(request);
					} else if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO)) {
						request.addError("Para estornar uma cota��o \"Em cota��o\", basta ter nenhum pedido enviado.");
						return getControllerModelAndView(request);
					} else if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.ORDEMCOMPRA_GERADA)) {
						request.addError("Estorno n�o permitido. Cota��o est� com ordem de compra gerada.");
						return getControllerModelAndView(request);
					} else if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA) &&
							 	cotacao.getDtbaixa() == null) {
						request.addError("Estorno n�o permitido. A ordem de compra da cota��o est� baixada. (Para o caso de uma baixa n�o manual).");
						return getControllerModelAndView(request);
					} else if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
						lista = solicitacaocompraService.findByCotacao(cotacao);
						string = CollectionsUtil.listAndConcatenate(lista, "cdsolicitacaocompra", ",");
						lista = solicitacaocompraService.findSolicitacoes(string);
						for (Solicitacaocompra solicitacaocompra : lista) {
							if (!solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) && 
									!solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.AUTORIZADA)) {
								request.addError("Estorno n�o permitido. H� uma cota��o em execu��o para a(s) mesma(s) solicita��o(�es) de compra.");
								return getControllerModelAndView(request);
							}
						}
					}
					
					/*if (!cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA) && !cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)) {
						request.addError("Para estornar uma cota��o, a situa��o tem que estar 'cancelada' e com nehuma solicita��o de compra associada diferente de 'cancelado'; e se estiver 'baixada', n�o pode ser por motivo da ordem de compra baixada.");
						return getControllerModelAndView(request);
					}
					
					if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA) && cotacao.getDtbaixa() == null) {
						request.addError("Para estornar uma cota��o, a situa��o tem que estar 'cancelada' e com nehuma solicita��o de compra associada diferente de 'cancelado'; e se estiver 'baixada', n�o pode ser por motivo da ordem de compra baixada.");
						return getControllerModelAndView(request);
					}
					
					
					if (cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
						lista = solicitacaocompraService.findByCotacao(cotacao);
						string = CollectionsUtil.listAndConcatenate(lista, "cdsolicitacaocompra", ",");
						lista = solicitacaocompraService.findSolicitacoes(string);
						for (Solicitacaocompra solicitacaocompra : lista) {
							if (!solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) && 
									!solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.AUTORIZADA)) {
								request.addError("N�o foi poss�vel estornar a(s) cota��o(�es).");
								return getControllerModelAndView(request);
							}
						}
					}*/
				}
			}
			
			
			
			cotacaoService.updateEstornada(whereIn);
			request.addMessage("Cota��o(�es) estornadas com sucesso.");
			
		} else{
			request.addError("STATUSACTION inv�lido.");
			return getControllerModelAndView(request);
		}
		
		
		return getControllerModelAndView(request);
	}

	/**
	 * Retorna para o controller do USC
	 * 
	 * @param request
	 * @return
	 * @author Tom�s Rabelo
	 */
	private ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			action += "&cdcotacao="+request.getParameter("selectedItens");
		}
		return new ModelAndView("redirect:"+action);
	}
	
	/**
	 * Prepara a cota��o enviando para o pop up.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#findCotacaoParaEnviarEmail(String)
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#validaEnviarEmail(Cotacao, org.springframework.validation.BindException)
	 * @param request
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 * @throws IOException 
	 */
	public ModelAndView enviarEmail(WebRequestContext request, Cotacao cotacao) throws IOException{
		cotacao = cotacaoService.findCotacaoParaEnviarEmail(request.getParameter("selectedItens"));
		
		if(!cotacaoService.validaEnviarEmail(cotacao, request.getBindException())){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		cotacaoService.preparaEmail(cotacao);
		
		cotacao.setRemetente(((Usuario)Neo.getUser()).getEmail());
		return new ModelAndView("direct:/process/popup/enviaEmailCotacao").addObject("cotacao", cotacao);
	}

	/**
	 * M�todo que envia e-mail para os contatos dos fornecedores e atualiza o cotacaofornecedor para convite enviado
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#enviaConviteParaEmail(Cotacao)
	 * @param request
	 * @param cotacao
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public ModelAndView enviarConvite(WebRequestContext request, Cotacao cotacao) throws Exception{
		cotacaoService.enviaConviteParaEmail(cotacao);
		
		request.addMessage("Convite(s) enviado(s) com sucesso.", MessageType.INFO);
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
}
