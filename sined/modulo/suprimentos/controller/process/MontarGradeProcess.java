package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.service.MaterialService;

@Controller(
		path={"/suprimento/process/Montargrade"},
		authorizationModule=ProcessAuthorizationModule.class
)
public class MontarGradeProcess extends MultiActionController{
	
	private MaterialService materialService;
	
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}

	@DefaultAction
	@Action("criarGrade")
	public ModelAndView montarGrade(WebRequestContext request, Material material){
		return materialService.montarGrade(request, material);
	}
	
	@Action("salvarGrade")
	public ModelAndView salvarGrade(WebRequestContext request, Material material){
		return materialService.salvarGrade(request, material);
	}
	
	public ModelAndView buscarInfMaterialGradeAJAX(WebRequestContext request, Material material){
		return materialService.buscarInfMaterialGradeAJAX(request, material);
	}
	
	public ModelAndView buscarInfLoteestoqueGradeAJAX(WebRequestContext request, Loteestoque loteestoque){
		return materialService.buscarInfLoteestoqueGradeAJAX(request, loteestoque);
	}
	
}
