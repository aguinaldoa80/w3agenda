package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.GerarOrdemCompraBean;
import br.com.linkcom.sined.util.SinedException;

@Controller(
		path="/suprimento/process/GerarOrdemCompra",
		authorizationModule=ProcessAuthorizationModule.class
)
public class GerarOrdemCompraProcess extends MultiActionController{
	
	/**
	 * Este metodo n�o esta sendo utilizado. Pois ele mandava para uma tela intermediaria onde o usuario via
	 * as ordens de compra quer seriam criadas. Esta guardado de backup: crud, DTO e jsp
	 * 
	 * Este m�todo agrupa as cota��es da seguinte maneira. Fornecedor + local de entrega iguais 
	 * tornam mesma ordem de compra.
	 * 
	 * @param request
	 * @param cotacao
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public ModelAndView preparaCotacoes(WebRequestContext request, Cotacao cotacao){
		Set<Ordemcompra> listaOrdensCompra = new ListSet<Ordemcompra>(Ordemcompra.class);
		for (Cotacaofornecedor cotacaofornecedor: cotacao.getListaCotacaofornecedor()) {
			
			//Agrupa materiais aos locais armazenagem com mesmo fornecedor
			Map<Integer, Set<Cotacaofornecedoritem>> map = new HashMap<Integer, Set<Cotacaofornecedoritem>>();
			for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
				if(cotacaofornecedoritem.getChecado()){
					if(map.containsKey(cotacaofornecedoritem.getLocalarmazenagem().getCdlocalarmazenagem())){
						map.get(cotacaofornecedoritem.getLocalarmazenagem().getCdlocalarmazenagem()).add(cotacaofornecedoritem);
					} else{
						Set<Cotacaofornecedoritem> list = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
						list.add(cotacaofornecedoritem);
						map.put(cotacaofornecedoritem.getLocalarmazenagem().getCdlocalarmazenagem(),list);
					}
				}
			}
			
			//Monta a Ordem de compra com os itens
			if(!map.isEmpty()){
				for (Integer cdlocalArmazenagem : map.keySet()) {
					Ordemcompra ordemcompra = new Ordemcompra();
//					ordemcompra.setDtproximaentrega(cotacao.getDtentrega());
					
					ordemcompra.setListaMaterial(new ListSet<Ordemcompramaterial>(Ordemcompramaterial.class));
					ordemcompra.setFornecedor(cotacaofornecedor.getFornecedor());

					Localarmazenagem localarmazenagem = new Localarmazenagem(cdlocalArmazenagem);
					ordemcompra.setLocalarmazenagem(localarmazenagem);
					
					Prazopagamento prazopagamento = new Prazopagamento(cdlocalArmazenagem);
					ordemcompra.setPrazopagamento(prazopagamento);
					
					ordemcompra.setDesconto(cotacaofornecedor.getDesconto());
					ordemcompra.setFrete(new Money());
					
					Collection<Set<Cotacaofornecedoritem>> listaMatForn = map.values();
					for (Set<Cotacaofornecedoritem> set : listaMatForn) {
						for (Cotacaofornecedoritem cotacaofornecedoritem2 : set) {
							//Verifica se o localarmazenagem bate com a key
							if(cotacaofornecedoritem2.getLocalarmazenagem().getCdlocalarmazenagem().equals(cdlocalArmazenagem)){
								Ordemcompramaterial ordemcompramaterial = new Ordemcompramaterial(
										cotacaofornecedoritem2.getMaterial(), cotacaofornecedoritem2.getQtdecot(), cotacaofornecedoritem2.getValor(), cotacaofornecedoritem2.getIcmsissincluso(),
										cotacaofornecedoritem2.getIcmsiss(), cotacaofornecedoritem2.getIpiincluso(), cotacaofornecedoritem2.getIpi(), 
										cotacaofornecedoritem2.getFrequencia(), cotacaofornecedoritem2.getQtdefrequencia(), cotacaofornecedoritem2.getMaterial().getUnidademedida());
								
								ordemcompra.getListaMaterial().add(ordemcompramaterial);
								ordemcompra.setFrete(ordemcompra.getFrete().add(new Money(cotacaofornecedoritem2.getFrete())));
							}
						}
					}
					
					listaOrdensCompra.add(ordemcompra);
				}
			} 
		}

		if(listaOrdensCompra == null || listaOrdensCompra.size() == 0)
			throw new SinedException("Nenhum item da cota��o foi selecionado.");
		
		request.setAttribute("descricaotela", "Gerar Ordem de Compra");
		GerarOrdemCompraBean gerarOrdemCompraBean = new GerarOrdemCompraBean();
		gerarOrdemCompraBean.setOrdens(listaOrdensCompra);
		gerarOrdemCompraBean.setCdcotacao(cotacao.getCdcotacao());
		
		return new ModelAndView("process/gerarOrdemCompra","gerarOrdemCompraBean", gerarOrdemCompraBean);
	}

}
