package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.OrdemcompraFiltro;

@Controller(
		path="/suprimento/process/OrdemcompraCSV",
		authorizationModule=ProcessAuthorizationModule.class
)
public class OrdemcompraCSVProcess extends ResourceSenderController<OrdemcompraFiltro>{
	private OrdemcompraService ordemcompraService;
	
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {this.ordemcompraService = ordemcompraService;}
	
	@Override
	public Resource generateResource(WebRequestContext request,	OrdemcompraFiltro filtro) throws Exception {
		return ordemcompraService.preparaArquivoOrdemCompraCSV(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, OrdemcompraFiltro filtro) throws Exception {
		return null;
	}
	
}
