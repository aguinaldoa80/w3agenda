package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.service.CotacaoService;

@Controller(
		path="/suprimento/process/CotacaoExcel",
		authorizationModule=ProcessAuthorizationModule.class
)
public class CotacaoExcelProcess extends ResourceSenderController<Cotacao>{

	private CotacaoService cotacaoService;
	
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	Cotacao cotacao) throws Exception {
		return cotacaoService.preparaArquivoCotacaoExcelFormatado(cotacao);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Cotacao cotacao) throws Exception {
		return null;
	}
	
}
