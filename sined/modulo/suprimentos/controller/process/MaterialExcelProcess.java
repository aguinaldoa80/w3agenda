package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltro;

@Controller(
		path="/suprimento/process/MaterialExcel",
		authorizationModule=ProcessAuthorizationModule.class
)
public class MaterialExcelProcess extends ResourceSenderController<MaterialFiltro>{

	private MaterialService materialService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	MaterialFiltro filtro) throws Exception {
		return materialService.preparaArquivoMaterialExcelFormatado(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, MaterialFiltro filtro) throws Exception {
		return null;
	}
	
}
