package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.BAIXADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.ESTORNAR;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.NAO_AUTORIZADA;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritemsolicitacaocompra;
import br.com.linkcom.sined.geral.bean.Cotacaoorigem;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraacao;
import br.com.linkcom.sined.geral.bean.Solicitacaocomprahistorico;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.geral.service.CotacaofornecedoritemsolicitacaocompraService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/suprimento/process/FluxoSituacaoSolicitacao",
		authorizationModule=ProcessAuthorizationModule.class
)
public class FluxoSituacaoSolicitacaoProcess extends MultiActionController{

	public final static int GERARCOTACAO = 1;
	public final static int INSERIRCOTACAO = 2;
	
	private SolicitacaocompraService solicitacaocompraService;
	private CotacaoService cotacaoService;
	private MaterialService materialService;
	private EmpresaService empresaService;
	private CotacaofornecedoritemsolicitacaocompraService cotacaofornecedoritemsolicitacaocompraService;
	private MotivoavisoService motivoavisoService;
	private AvisoService avisoService;
	
	public void setCotacaofornecedoritemsolicitacaocompraService(
			CotacaofornecedoritemsolicitacaocompraService cotacaofornecedoritemsolicitacaocompraService) {
		this.cotacaofornecedoritemsolicitacaocompraService = cotacaofornecedoritemsolicitacaocompraService;
	}
	public void setSolicitacaocompraService(
			SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	
	/**
	 * M�todo default que redireciona para o tipo de altera��o escolhido
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findSolicitacoes(String)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#autorizar(List, String, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#cancelar(List, String, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#estornar(List, String, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#baixar(List, String, org.springframework.validation.BindException)
	 * @see #getControllerModelAndView(WebRequestContext)
	 * @param request
	 * @param acao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView acao(WebRequestContext request, Solicitacaocompra acao){
		if (acao == null || acao.getCdsolicitacaocompra() == null) {
			throw new SinedException("Solicita��o de compra n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.findSolicitacoes(whereIn);
		
		if(acao.getCdsolicitacaocompra().equals(AUTORIZADA.getCodigo())){
			if(solicitacaocompraService.autorizar(solicitacoes, whereIn, request.getBindException(), request)){
				request.addMessage("Registro(s) autorizado(s) com sucesso.", MessageType.INFO);
				
				Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.AUTORIZAR_SOLICITACAOCOMPRA);
				if(motivoAviso != null && Boolean.TRUE.equals(motivoAviso.getEmail())){
					for (String selectedItem : whereIn.split(",")) {
						try {
							Aviso aviso = null;
							List<Aviso> avisoList = new ArrayList<Aviso>();
							
							aviso = new Aviso("Autorizar solicita��o de compra", "C�digo da solicita��o de compra: " + selectedItem, 
									motivoAviso.getTipoaviso(), motivoAviso.getPapel(), motivoAviso.getAvisoorigem(), Integer.parseInt(selectedItem), 
									empresaService.loadPrincipal(), SinedDateUtils.currentDate(), motivoAviso);
							
							avisoList.add(aviso);
							
							avisoService.salvarAvisos(avisoList , true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		} else if(acao.getCdsolicitacaocompra().equals(CANCELADA.getCodigo())){
			if(solicitacaocompraService.cancelar(solicitacoes, request.getBindException())){
				return redirecionaPopUpJustificativa(request, whereIn, Solicitacaocompraacao.CANCELADA);
			}else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
		} else if(acao.getCdsolicitacaocompra().equals(ESTORNAR.getCodigo())){
			if(solicitacaocompraService.estornar(solicitacoes, request.getBindException())){
				return redirecionaPopUpJustificativa(request, whereIn, Solicitacaocompraacao.ESTORNADA);
			}else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
		} else if(acao.getCdsolicitacaocompra().equals(BAIXADA.getCodigo())){
			if(solicitacaocompraService.baixar(solicitacoes,request.getBindException())){
				return redirecionaPopUpJustificativa(request, whereIn, Solicitacaocompraacao.BAIXADA);
			}else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
		} else if(acao.getCdsolicitacaocompra().equals(NAO_AUTORIZADA.getCodigo())){
			if(solicitacaocompraService.naoautorizar(solicitacoes,request.getBindException())){
				return redirecionaPopUpJustificativa(request, whereIn, Solicitacaocompraacao.NAO_AUTORIZADA);
			} else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		if(request.getBindException().hasErrors()){
			request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
		}
		return getControllerModelAndView(request);
	}

	/**
	 * M�todo que redireciona para o pop up de justificativa 
	 * 
	 * @param request
	 * @param whereIn
	 * @param solicitacaocompraacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	private ModelAndView redirecionaPopUpJustificativa(WebRequestContext request, String whereIn, Solicitacaocompraacao solicitacaocompraacao) {
		Solicitacaocomprahistorico solicitacaocomprahistorico = new Solicitacaocomprahistorico();
		solicitacaocomprahistorico.setIds(whereIn);
		solicitacaocomprahistorico.setSolicitacaocompraacao(solicitacaocompraacao);
		
		if(solicitacaocompraacao.equals(Solicitacaocompraacao.CANCELADA)){
			request.setAttribute("descricao", "CANCELAR");
			request.setAttribute("msg", "Justificativa para o cancelamento");
		}else if (solicitacaocompraacao.equals(Solicitacaocompraacao.ESTORNADA)){
			request.setAttribute("descricao", "ESTORNAR");
			request.setAttribute("msg", "Justificativa para o estorno");
		}else if (solicitacaocompraacao.equals(Solicitacaocompraacao.NAO_AUTORIZADA)){
			request.setAttribute("descricao", "N�O AUTORIZAR");
			request.setAttribute("msg", "Justificativa para a n�o autoriza��o");
		}else{
			request.setAttribute("descricao", "BAIXAR");
			request.setAttribute("msg", "Justificativa para dar baixa");
		}
		return new ModelAndView("direct:/process/popup/canceladaestornadaJustificativaSolicitacao")
								.addObject("solicitacaocomprahistorico", solicitacaocomprahistorico);
	}
	
	/**
	 * M�todo chamado ap�s fechar o pop up de justificativa de estorno
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findSolicitacoes(String)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#preparaSolicitacaocomprahistorico(List, Solicitacaocompraacao, Solicitacaocomprahistorico)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#geraAvisoAtualizaSolicitacoes(List, List, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @param request
	 * @param ordemcomprahistorico
	 * @author Tom�s Rabelo
	 */
	@Action("saveJustificativa")
	public void saveJustificativaCancelamento (WebRequestContext request, Solicitacaocomprahistorico solicitacaocomprahistorico){
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.findSolicitacoes(solicitacaocomprahistorico.getIds());
		try {
			if(solicitacaocompraService.cancelar(solicitacoes, request.getBindException())){
				solicitacaocompraService.preparaSolicitacaocomprahistorico(solicitacoes, Solicitacaocompraacao.CANCELADA, solicitacaocomprahistorico);
				solicitacaocompraService.geraAvisoAtualizaSolicitacoes(null, solicitacoes, CANCELADA);
				request.addMessage("Registro(s) cancelado(s) com sucesso.", MessageType.INFO);
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			if(request.getBindException().hasErrors()){
				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			}
			SinedUtil.fechaPopUp(request);
		}
	}
	
	public void saveJustificativaNaoautorizar(WebRequestContext request, Solicitacaocomprahistorico solicitacaocomprahistorico){
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.findSolicitacoes(solicitacaocomprahistorico.getIds());
		try {
			if(solicitacaocompraService.naoautorizar(solicitacoes, request.getBindException())){
				solicitacaocompraService.preparaSolicitacaocomprahistorico(solicitacoes, Solicitacaocompraacao.NAO_AUTORIZADA, solicitacaocomprahistorico);
				solicitacaocompraService.geraAvisoAtualizaSolicitacoes(null, solicitacoes, NAO_AUTORIZADA);
				request.addMessage("Registro(s) n�o autorizado(s) com sucesso.", MessageType.INFO);
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			if(request.getBindException().hasErrors()){
				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			}
			SinedUtil.fechaPopUp(request);
		}
	}
	
	/**
	 * M�todo chamado ap�s fechar o pop up de justificativa de cancelamento
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findSolicitacoes(String)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#estornar(List, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#preparaSolicitacaocomprahistorico(List, Solicitacaocompraacao, Solicitacaocomprahistorico)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#geraAvisoAtualizaSolicitacoes(List, List, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @param request
	 * @param ordemcomprahistorico
	 * @author Tom�s Rabelo
	 */
	@Action("saveJustificativa")
	public void saveJustificativaEstorno (WebRequestContext request, Solicitacaocomprahistorico solicitacaocomprahistorico){
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.findSolicitacoes(solicitacaocomprahistorico.getIds());
		try {
			if(solicitacaocompraService.estornar(solicitacoes, request.getBindException())){
				solicitacaocompraService.preparaSolicitacaocomprahistorico(solicitacoes, Solicitacaocompraacao.ESTORNADA, solicitacaocomprahistorico);
				solicitacaocompraService.geraAvisoAtualizaSolicitacoes(null, solicitacoes, ESTORNAR);
				request.addMessage("Registro(s) estornado(s) com sucesso.", MessageType.INFO);
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			if(request.getBindException().hasErrors()){
				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			}
			SinedUtil.fechaPopUp(request);
		}
	}
	/**
	 * M�todo chamado ap�s fechar o pop up de justificativa de dar baixa
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findSolicitacoes(String)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#estornar(List, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#preparaSolicitacaocomprahistorico(List, Solicitacaocompraacao, Solicitacaocomprahistorico)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#geraAvisoAtualizaSolicitacoes(List, List, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @param request
	 * @param ordemcomprahistorico
	 * @author Nilmar Nascimento
	 */
	@Action("saveJustificativa")
	public void saveJustificativaBaixar (WebRequestContext request, Solicitacaocomprahistorico solicitacaocomprahistorico){
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.findSolicitacoes(solicitacaocomprahistorico.getIds());
		try {
			if(solicitacaocompraService.baixar(solicitacoes, request.getBindException())){
				solicitacaocompraService.preparaSolicitacaocomprahistorico(solicitacoes, Solicitacaocompraacao.BAIXADA, solicitacaocomprahistorico);
				solicitacaocompraService.geraAvisoAtualizaSolicitacoes(null, solicitacoes, BAIXADA);
				request.addMessage("Registro(s) baixado(s) com sucesso.", MessageType.INFO);
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			if(request.getBindException().hasErrors()){
				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			}
			SinedUtil.fechaPopUp(request);
		}
	}
	
	public ModelAndView abrirPopupInserirSolicitacaoCotacao(WebRequestContext request, Solicitacaocompra solicitacaocompra){
		String selectedItens = request.getParameter("selectedItens");
		request.setAttribute("selectedItens", selectedItens);
		request.setAttribute("controller", request.getParameter("controller"));
		request.setAttribute("existeMesmoMaterial", cotacaoService.existeMaterial(selectedItens, solicitacaocompra.getCotacaotrans()));
		
		List<Solicitacaocompra> list = solicitacaocompraService.findSolicitacoes(selectedItens);
		boolean existeRestricaoMaterialempresa = materialService.existeRestricaoMaterialempresa(list);
		boolean existEmpresadiferente = false;
		if(!existeRestricaoMaterialempresa && solicitacaocompra.getCotacaotrans() != null){
			Cotacao cotacao = cotacaoService.loadWithEmpresa(solicitacaocompra.getCotacaotrans());
			if(cotacao != null && cotacao.getEmpresa() != null){
				if(list != null && !list.isEmpty()){
					for(Solicitacaocompra sc : list){
						if(sc.getEmpresa() != null && sc.getEmpresa().getCdpessoa() != null && !sc.getEmpresa().equals(cotacao.getEmpresa())){
							existEmpresadiferente = true;
							break;
						}
					}
				}
			}
		}
		request.setAttribute("existEmpresadiferente", existEmpresadiferente);
		request.setAttribute("listaEmpresa", empresaService.findAtivosByUsuario());
		return new ModelAndView("direct:/process/popup/popUpInserirSolicitacaoCotacao", "bean", solicitacaocompra);
	}
	
	/**
	 * M�todo que inseri solicita��es de compra em uma cota��o
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findSolicitacoesParaInserirCotacao(String)
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#loadForEntrada(Cotacao)
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#preparaSolicitacoesParaInsercaoCotacao(List, Solicitacaocompra, Cotacao)
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#preparaOrigensDaInsercaoCotacao(List, Cotacao)
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#saveSolicitacaoNaCotacao(List, List)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#atualizaSituacaoSolicitacoes(List)
	 * @see #getControllerModelAndView(WebRequestContext)
	 * @param request
	 * @param solicitacaocompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView inserirSolicitacaoCotacao(WebRequestContext request, Solicitacaocompra solicitacaocompra){
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.findSolicitacoesParaInserirCotacao(request.getParameter("selectedItens"));
		Cotacao cotacao = cotacaoService.loadForEntrada(solicitacaocompra.getCotacaotrans());
		if(solicitacaocompra.getLocalunicoentrega() != null){
			cotacao.setConsiderarlocalunicoentrega(true);
		}
		List<Cotacaofornecedoritem> itens = cotacaoService.preparaSolicitacoesParaInsercaoCotacao(solicitacoes, solicitacaocompra, cotacao);
		List<Cotacaoorigem> origens = cotacaoService.preparaOrigensDaInsercaoCotacao(solicitacoes, cotacao);
		
		if(itens != null && itens.size() > 0){
			for (Cotacaofornecedoritem cotacaofornecedoritem : itens) {
				if(cotacaofornecedoritem.getCdcotacaofornecedoritem() != null){
					if(cotacaofornecedoritem.getListaCotacaofornecedoritemsolicitacaocompra() == null){
						cotacaofornecedoritem.setListaCotacaofornecedoritemsolicitacaocompra(new ListSet<Cotacaofornecedoritemsolicitacaocompra>(Cotacaofornecedoritemsolicitacaocompra.class));
					}
					
					List<Cotacaofornecedoritemsolicitacaocompra> listByCotacaofornecedoritem = cotacaofornecedoritemsolicitacaocompraService.findByCotacaofornecedoritem(cotacaofornecedoritem);
					for (Cotacaofornecedoritemsolicitacaocompra cotacaofornecedoritemsolicitacaocompra : listByCotacaofornecedoritem) {
						boolean achouSolicitacaocompra = false;
						for (Cotacaofornecedoritemsolicitacaocompra cotacaofornecedoritemsolicitacaocompra2 : cotacaofornecedoritem.getListaCotacaofornecedoritemsolicitacaocompra()) {
							if(cotacaofornecedoritemsolicitacaocompra2.getSolicitacaocompra() != null && 
									cotacaofornecedoritemsolicitacaocompra2.getSolicitacaocompra().equals(cotacaofornecedoritemsolicitacaocompra.getSolicitacaocompra())){
								achouSolicitacaocompra = true;
								break;
							}
						}
						if(!achouSolicitacaocompra){
							cotacaofornecedoritem.getListaCotacaofornecedoritemsolicitacaocompra().add(cotacaofornecedoritemsolicitacaocompra);
						}
					}
				}
			}
		}
		
		cotacaoService.saveSolicitacaoNaCotacao(origens, itens);
		solicitacaocompraService.atualizaSituacaoSolicitacoes(solicitacoes);
		if(solicitacaocompra.getEmpresaunicaentrega() != null){
			cotacaoService.updateEmpresaUnicaentrega(cotacao, solicitacaocompra.getEmpresaunicaentrega());
		}
		if(SinedUtil.isListNotEmpty(solicitacoes)){
			solicitacaocompraService.registrarHistoricoAposInserirEmCotacao(CollectionsUtil.listAndConcatenate(solicitacoes, "cdsolicitacaocompra", ","), cotacao);
		}
		request.addMessage("Solicita��o(�es) inserida(s) na cota��o "+cotacao.getCdcotacao()+" com sucesso.", MessageType.INFO);
		request.setAttribute("fecharPopUpRedirecionar", true);
		return getControllerModelAndView(request);
	}

	/**
	 * M�todo que v�lida se as solicita��es est�o com situa��o Autorizada, chamado via ajax quando a op��o 
	 * 'inserir em cota��o' � escolhida.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findSolicitacoesParaInserirCotacao(String)
	 * @param request
	 * @param solicitacaocompra
	 * @author Tom�s Rabelo
	 */
	public void verificaSolicitacoesAutorizada(WebRequestContext request, Solicitacaocompra solicitacaocompra){
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.findSolicitacoesParaInserirCotacao(solicitacaocompra.getWhereIn());
		
		String dados = "var valido = ";
		if(solicitacaocompraService.isSolicitacoesDiferenteDeAutorizada(solicitacoes, request.getBindException(), FluxoSituacaoSolicitacaoProcess.INSERIRCOTACAO)){
			if(solicitacaocompra.getController().contains("consultar")){
				solicitacaocompra.setController(solicitacaocompra.getController()+"&cdsolicitacaocompra="+solicitacaocompra.getWhereIn());
			}
			if(request.getBindException().hasErrors()){
				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			}
			dados += "false; window.opener.location = '"+request.getServletRequest().getContextPath()+solicitacaocompra.getController()+"';";
		}else{
			dados += "true;";
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(dados);
	}
	
	/**
	 * Retorna para o controller do USC
	 * 
	 * @param request
	 * @return
	 * @author Tom�s Rabelo
	 */
	private ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			action += "&cdsolicitacaocompra="+request.getParameter("selectedItens");
		}
		Boolean fecharPopUpRedirecionar = (Boolean) request.getAttribute("fecharPopUpRedirecionar");
		if(fecharPopUpRedirecionar != null && fecharPopUpRedirecionar){
			SinedUtil.redirecionamento(request, action);
			return null;
		}
		return new ModelAndView("redirect:"+action);
	}
	
}
