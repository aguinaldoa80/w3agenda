package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/suprimento/process/Mapacotacao", authorizationModule=ProcessAuthorizationModule.class)
public class MapaCotacaoProcess extends MultiActionController{
	
	private CotacaoService cotacaoService;
	private SolicitacaocompraService solicitacaocompraService;
	
	public void setSolicitacaocompraService(
			SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, Cotacao cotacao) {
		cotacao = cotacaoService.loadForEntrada(cotacao);
		boolean podeeditaremprocessocompra = true;
		if(cotacao.getCdcotacao() != null){
			cotacaoService.setInfOrdemcompragerada(cotacao);
			boolean todositensgerados = false;
			for(Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()){
				todositensgerados = cf.isTodosItensGerados();
				break;
			}
			if(todositensgerados) podeeditaremprocessocompra = false;
		}
		
		if(!cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO)){
			if((!Situacaosuprimentos.ORDEMCOMPRA_GERADA.equals(cotacao.getAux_cotacao().getSituacaosuprimentos()) && 
					!podeeditaremprocessocompra) || 
					(Situacaosuprimentos.ORDEMCOMPRA_GERADA.equals(cotacao.getAux_cotacao().getSituacaosuprimentos()) && 
							!podeeditaremprocessocompra)){
				request.clearMessages();
				request.addError("Registro(s) com situa��o diferente de 'em cota��o'.");
				return new ModelAndView("redirect:/suprimento/crud/Cotacao?ACAO=consultar&cdcotacao=" + cotacao.getCdcotacao());
			}
		}
		cotacao.setListaSolicitacao(solicitacaocompraService.findByCotacao(cotacao));
		calculaValorTotal(cotacao.getListaCotacaofornecedor());
		
		String itensSolicitacao = SinedUtil.listAndConcatenate(cotacao.getListaSolicitacao(), "identificador", ", ");
		if(itensSolicitacao.split(",").length > 10) itensSolicitacao = "DIVERSAS";
		request.setAttribute("itensSolicitacao", itensSolicitacao);
		
		request.setAttribute("projetos", SinedUtil.listAndConcatenate(cotacao.getListaSolicitacao(), "projeto.nome", ", "));
		request.setAttribute("tamanhoFornecedor", cotacao.getListaCotacaofornecedor() != null ? cotacao.getListaCotacaofornecedor().size() : 0);
		request.setAttribute("tamanhoFornecedorItem", cotacaoService.getTamanhoListaFornecedorItem(cotacao.getListaCotacaofornecedor()));
		
		return new ModelAndView("process/mapacotacao","cotacao",cotacao);
	}

	public ModelAndView updateMapaCotacao(WebRequestContext request, Cotacao bean){
		try{
			cotacaoService.updateMapaCotacao(bean);
			request.addMessage("Registro salvo com Sucesso!");
		}catch (Exception e) {
			e.printStackTrace();
			request.addError("Registro n�o pode ser salvo.");
		}
		return new ModelAndView("redirect:/suprimento/crud/Cotacao");
	}

	public void calculaValorTotal(List<Cotacaofornecedor> listaCotacao){
		for (Cotacaofornecedor cotacaofornecedor : listaCotacao) {
			Double valorTotal = 0.0;
			for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
				if(cotacaofornecedoritem.getValor() != null){
					Double total = 0d;
					if(cotacaofornecedoritem.getValor() != null) total += cotacaofornecedoritem.getValor();
					if(cotacaofornecedoritem.getIcmsiss() != null && cotacaofornecedoritem.getIcmsiss() > 0 && 
							(cotacaofornecedoritem.getIcmsissincluso() == null || !cotacaofornecedoritem.getIcmsissincluso())){
						total += (cotacaofornecedoritem.getIcmsiss() / 100) * (cotacaofornecedoritem.getValor() != null ? cotacaofornecedoritem.getValor() : 0.0);
					}
					if(cotacaofornecedoritem.getIpi() != null && cotacaofornecedoritem.getIpi() > 0 && 
							(cotacaofornecedoritem.getIpiincluso() == null || !cotacaofornecedoritem.getIpiincluso())){
						total += (cotacaofornecedoritem.getIpi() / 100) * (cotacaofornecedoritem.getValorSemDesconto() != null ? cotacaofornecedoritem.getValorSemDesconto() : 0.0);
					}
					if(cotacaofornecedoritem.getFrete() != null && cotacaofornecedoritem.getFrete() > 0){
						total += cotacaofornecedoritem.getFrete();
					}
					
					valorTotal += total;
				}
			}
			cotacaofornecedor.setValorTotalFornecedorItem(SinedUtil.round(valorTotal, 2));
		}
	}
}
