package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Produto;

public class CotacaoAux {

	protected Integer cdcotacao;
	protected String situacao;
	protected Integer quantidade;
	
	protected List<Produto> listaProdutosSelected;
	protected List<Produto> listaProdutosUnselected ;
	protected List<Fornecedor> listaFornecedoresSelected;
	protected List<Fornecedor> listaFornecedoresUnselected;
	
	public CotacaoAux(){
	}

	public CotacaoAux(Integer cdcotacao){
		this.cdcotacao = cdcotacao;
	}
	
	public Integer getCdcotacao() {
		return cdcotacao;
	}
	public String getSituacao() {
		return situacao;
	}
	public List<Produto> getListaProdutosSelected() {
		return listaProdutosSelected;
	}
	public List<Produto> getListaProdutosUnselected() {
		return listaProdutosUnselected;
	}
	public List<Fornecedor> getListaFornecedoresSelected() {
		return listaFornecedoresSelected;
	}
	public List<Fornecedor> getListaFornecedoresUnselected() {
		return listaFornecedoresUnselected;
	}
	
	public void setCdcotacao(Integer cdcotacao) {
		this.cdcotacao = cdcotacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public void setListaProdutosSelected(List<Produto> listaProdutosSelected) {
		this.listaProdutosSelected = listaProdutosSelected;
	}
	public void setListaProdutosUnselected(List<Produto> listaProdutosUnselected) {
		this.listaProdutosUnselected = listaProdutosUnselected;
	}
	public void setListaFornecedoresSelected(
			List<Fornecedor> listaFornecedoresSelected) {
		this.listaFornecedoresSelected = listaFornecedoresSelected;
	}
	public void setListaFornecedoresUnselected(
			List<Fornecedor> listaFornecedoresUnselected) {
		this.listaFornecedoresUnselected = listaFornecedoresUnselected;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
}
