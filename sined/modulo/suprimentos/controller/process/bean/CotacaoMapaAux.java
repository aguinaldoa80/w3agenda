package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;

public class CotacaoMapaAux {

	protected Integer cdcotacao;
	protected List<Cotacaofornecedoritem> listaProdutos;
	protected List<Cotacaofornecedor> listaFornecedores;
	protected String situacao;
	
	public Integer getCdcotacao() {
		return cdcotacao;
	}
	public List<Cotacaofornecedoritem> getListaProdutos() {
		return listaProdutos;
	}
	public List<Cotacaofornecedor> getListaFornecedores() {
		return listaFornecedores;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public void setListaProdutos(List<Cotacaofornecedoritem> listaProdutos) {
		this.listaProdutos = listaProdutos;
	}
	public void setListaFornecedores(List<Cotacaofornecedor> listaFornecedores) {
		this.listaFornecedores = listaFornecedores;
	}
	public void setCdcotacao(Integer cdcotacao) {
		this.cdcotacao = cdcotacao;
	}
}
