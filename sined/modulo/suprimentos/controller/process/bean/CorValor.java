package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import br.com.linkcom.neo.types.Money;

public class CorValor {

	protected Money valor;
	protected Integer cor;
	protected Boolean editable;
	
	public Money getValor() {
		return valor;
	}
	public Integer getCor() {
		return cor;
	}
	public Boolean getEditable() {
		return editable;
	}
	
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setCor(Integer cor) {
		this.cor = cor;
	}
	
}
