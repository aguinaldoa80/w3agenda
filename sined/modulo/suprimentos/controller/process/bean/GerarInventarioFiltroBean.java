package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadorpropriedade;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.geral.bean.enumeration.Motivoinventario;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopessoainventario;
import br.com.linkcom.sined.util.SinedDateUtils;

public class GerarInventarioFiltroBean {

	private InventarioTipo inventarioTipo;
	private Empresa empresa;
	private Localarmazenagem localarmazenagem;
	private Projeto projeto;
	private Date dtinventario; 
	private Motivoinventario motivoinventario;
	private Indicadorpropriedade indicadorpropriedade;
	private String contacontabil;
	private String mesanoAux;
	protected Tipopessoainventario tipopessoainventario;
	protected Pessoa pessoa;
	
	@DisplayName("Tipo de invent�rio")
	public InventarioTipo getInventarioTipo() {
		return inventarioTipo;
	}

	@Required
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public Date getDtinventario() {
		return dtinventario;
	}

	@DisplayName("Motivo do invent�rio")
	public Motivoinventario getMotivoinventario() {
		return motivoinventario;
	}

	@DisplayName("Indicador de propriedade")
	public Indicadorpropriedade getIndicadorpropriedade() {
		return indicadorpropriedade;
	}

	@DisplayName("Conta cont�bil")
	public String getContacontabil() {
		return contacontabil;
	}
	
	@DisplayName("Tipo de pessoa")
	public Tipopessoainventario getTipopessoainventario() {
		return tipopessoainventario;
	}
	
	@DisplayName("Pessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setInventarioTipo(InventarioTipo inventarioTipo) {
		this.inventarioTipo = inventarioTipo;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setDtinventario(Date dtinventario) {
		this.dtinventario = dtinventario;
	}

	public void setMotivoinventario(Motivoinventario motivoinventario) {
		this.motivoinventario = motivoinventario;
	}

	public void setIndicadorpropriedade(Indicadorpropriedade indicadorpropriedade) {
		this.indicadorpropriedade = indicadorpropriedade;
	}

	public void setContacontabil(String contacontabil) {
		this.contacontabil = contacontabil;
	}

	@Required
	@DisplayName("M�s/ano")
	public String getMesanoAux() {
		if(this.dtinventario != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtinventario);
		return mesanoAux;
	}
	
	public void setMesanoAux(String mesanoAux) {
		try {
			if(mesanoAux != null){
				this.dtinventario = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
				this.dtinventario = SinedDateUtils.lastDateOfMonth(this.dtinventario);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
	
	public void setTipopessoainventario(
			Tipopessoainventario tipopessoainventario) {
		this.tipopessoainventario = tipopessoainventario;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
}
