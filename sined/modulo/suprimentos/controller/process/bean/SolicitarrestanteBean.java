package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Garantia;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipofrete;


public class SolicitarrestanteBean {

	protected Tiposolicitacaorestante tiposolicitacaorestante;
	protected Date dtnecessidade;
	protected Fornecedor fornecedor;
	protected Contato contato;
	protected Localarmazenagem localarmazenagem;
	protected Colaborador colaborador;
	protected Empresa empresa;
	protected Departamento departamento;
	protected Documentotipo documentotipo;
	protected Fornecedor transportador;
	protected Garantia garantia;
	protected Prazopagamento prazopagamento;
	protected Date dtcriacao;
	protected Tipofrete tipofrete;
	protected List<SolicitarrestanteItemBean> listaSolicitarrestanteItemBean;
	protected String pedidoorigem;
	
	public Tiposolicitacaorestante getTiposolicitacaorestante() {
		return tiposolicitacaorestante;
	}
	public Date getDtnecessidade() {
		return dtnecessidade;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Contato getContato() {
		return contato;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public Fornecedor getTransportador() {
		return transportador;
	}
	public Garantia getGarantia() {
		return garantia;
	}
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	public Date getDtcriacao() {
		return dtcriacao;
	}
	public Tipofrete getTipofrete() {
		return tipofrete;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public List<SolicitarrestanteItemBean> getListaSolicitarrestanteItemBean() {
		return listaSolicitarrestanteItemBean;
	}

	public void setTiposolicitacaorestante(Tiposolicitacaorestante tiposolicitacaorestante) {
		this.tiposolicitacaorestante = tiposolicitacaorestante;
	}
	public void setDtnecessidade(Date dtnecessidade) {
		this.dtnecessidade = dtnecessidade;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setTransportador(Fornecedor transportador) {
		this.transportador = transportador;
	}
	public void setGarantia(Garantia garantia) {
		this.garantia = garantia;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setDtcriacao(Date dtcriacao) {
		this.dtcriacao = dtcriacao;
	}
	public void setTipofrete(Tipofrete tipofrete) {
		this.tipofrete = tipofrete;
	}
	public void setListaSolicitarrestanteItemBean(List<SolicitarrestanteItemBean> listaSolicitarrestanteItemBean) {
		this.listaSolicitarrestanteItemBean = listaSolicitarrestanteItemBean;
	}

	public enum Tiposolicitacaorestante {
		SOLICITACAOCOMPRA 	(0, "Solicitação de compra"), 
		ORDEMCOMPRA			(1, "Ordem de compra");
		
		private Integer value;
		private String nome;
		
		private Tiposolicitacaorestante(Integer _value, String _nome) {
			this.value = _value;
			this.nome = _nome;
		}
		
		public Integer getValue() {
			return value;
		}
		@DescriptionProperty
		public String getNome() {
			return nome;
		}
		@Override
		public String toString() {
			return nome;
		}
	}

	public String getPedidoorigem() {
		return pedidoorigem;
	}
	public void setPedidoorigem(String pedidoorigem) {
		this.pedidoorigem = pedidoorigem;
	}
	
}
