package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.util.Set;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Ordemcompra;

@DisplayName("Gerar Ordem Compra")
public class GerarOrdemCompraBean {
	
	protected Set<Ordemcompra> ordens;
	protected Integer cdcotacao;
	
	public Set<Ordemcompra> getOrdens() {
		return ordens;
	}
	public Integer getCdcotacao() {
		return cdcotacao;
	}
	public void setOrdens(Set<Ordemcompra> ordens) {
		this.ordens = ordens;
	}
	public void setCdcotacao(Integer cdcotacao) {
		this.cdcotacao = cdcotacao;
	}
	
}
