package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.util.SinedUtil;


public class SolicitarrestanteItemBean {

	protected Long identificador;
	protected Boolean checado;
	protected Material material;
	protected Materialclasse materialclasse;
	protected Double quantidade;
	protected Double valorunitario;
	protected Double valortotal;
	protected Unidademedida unidademedida;
	protected Localarmazenagem localarmazenagem;
	protected Solicitacaocompra solicitacaocompra;
	protected Date dtnecessidade;
	protected Centrocusto centrocusto;
	protected Contagerencial contagerencial;
	protected String whereInOrdemcompra;
	protected String whereInCotacao;
	protected Projeto projeto;
	protected String observacao;
	protected List<Solicitacaocompra> listaSolicitacaocompra;
	protected Boolean existeOrigem;
	
	public Long getIdentificador() {
		return identificador;
	}
	public Boolean getChecado() {
		return checado;
	}
	public Material getMaterial() {
		return material;
	}
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public Double getValortotal() {
		return valortotal;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Solicitacaocompra getSolicitacaocompra() {
		return solicitacaocompra;
	}
	public Date getDtnecessidade() {
		return dtnecessidade;
	}
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public String getWhereInOrdemcompra() {
		return whereInOrdemcompra;
	}
	
	public void setIdentificador(Long identificador) {
		this.identificador = identificador;
	}
	public void setChecado(Boolean checado) {
		this.checado = checado;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setSolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}
	public void setDtnecessidade(Date dtnecessidade) {
		this.dtnecessidade = dtnecessidade;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setWhereInOrdemcompra(String whereInOrdemcompra) {
		this.whereInOrdemcompra = whereInOrdemcompra;
	}
	
	public List<Solicitacaocompra> getListaSolicitacaocompra() {
		return listaSolicitacaocompra;
	}
	public void setListaSolicitacaocompra(List<Solicitacaocompra> listaSolicitacaocompra) {
		this.listaSolicitacaocompra = listaSolicitacaocompra;
	}
	
	public Boolean getExisteOrigem(){
		return SinedUtil.isListNotEmpty(listaSolicitacaocompra);
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	
}
