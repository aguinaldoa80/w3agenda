package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Questionarioquestao;

@Entity
@SequenceGenerator(name = "sq_pessoaquestionarioquestao", sequenceName = "sq_pessoaquestionarioquestao")
public class Pessoaquestionarioquestao {
	
	protected Integer cdpessoaquestionarioquestao;
	protected Questionarioquestao questionarioquestao;
	protected Boolean sim;
	protected String resposta;
	protected Integer pontuacao;
	protected Pessoaquestionario pessoaquestionario;
	protected Boolean obrigarresposta;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoaquestionarioquestao")
	public Integer getCdpessoaquestionarioquestao() {
		return cdpessoaquestionarioquestao;
	}
	public void setCdpessoaquestionarioquestao(Integer cdpessoaquestionarioquestao) {
		this.cdpessoaquestionarioquestao = cdpessoaquestionarioquestao;
	}
	
	@JoinColumn(name="cdquestionarioquestao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Questionarioquestao getQuestionarioquestao() {
		return questionarioquestao;
	}
	public void setQuestionarioquestao(Questionarioquestao questionarioquestao) {
		this.questionarioquestao = questionarioquestao;
	}
	
	@DisplayName("Sim")
	public Boolean getSim() {
		return sim;
	}
	public void setSim(Boolean sim) {
		this.sim = sim;
	}
	
	@DisplayName("Resposta")
	@MaxLength(200)
	public String getResposta() {
		return resposta;
	}
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
	
	@DisplayName("Pontuacao")
	public Integer getPontuacao() {
		return pontuacao;
	}
	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}
	
	@JoinColumn(name="cdpessoaquestionario")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pessoaquestionario getPessoaquestionario() {
		return pessoaquestionario;
	}
	public void setPessoaquestionario(Pessoaquestionario pessoaquestionario) {
		this.pessoaquestionario = pessoaquestionario;
	}
	
	@Transient
	public Boolean getObrigarresposta() {
		return obrigarresposta;
	}
	public void setObrigarresposta(Boolean obrigarresposta) {
		this.obrigarresposta = obrigarresposta;
	}
	
	@Transient
	public String getSimString(){
		if (Boolean.TRUE.equals(getSim())){
			return "Sim";
		} else {
			return "N�o";
		}
	}
}
