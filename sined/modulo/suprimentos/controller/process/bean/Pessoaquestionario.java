package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Grupoquestionario;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoEnum;
import br.com.linkcom.sined.util.Log;

/**
 * @author Linkcom
 *
 */
@SuppressWarnings("unchecked")
@Entity
@SequenceGenerator(name = "sq_pessoaquestionario", sequenceName = "sq_pessoaquestionario")
public class Pessoaquestionario implements Log{
	
	protected Integer cdpessoaquestionario;
	protected Questionario questionario;
	protected Integer pontuacao;
	protected SituacaoEnum situacao;
	protected Pessoa pessoa;
	protected Agendainteracao agendainteracao;
	List<Pessoaquestionarioquestao> listaPessoaQuestionarioQuestao = new ListSet(Pessoaquestionarioquestao.class);
	
//	TRANSIENT
	protected String situacaoString;
	protected String avaliadorString;
	protected String arquivoString;
	protected List<Grupoquestionario> listaGrupoquestionario = new ListSet(Grupoquestionario.class);
	protected Integer cdagendainteracao;
	
//	CAMPOS DE LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoaquestionario")
	public Integer getCdpessoaquestionario() {
		return cdpessoaquestionario;
	}
	public void setCdpessoaquestionario(Integer cdpessoaquestionario) {
		this.cdpessoaquestionario = cdpessoaquestionario;
	}
	
	@JoinColumn(name="cdquestionario")
	@ManyToOne(fetch=FetchType.LAZY)
	public Questionario getQuestionario() {
		return questionario;
	}
	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}
	
	@DisplayName("Pontua��o")
	public Integer getPontuacao() {
		return pontuacao;
	}
	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}
	
	@DisplayName("Situa��o")
	public SituacaoEnum getSituacao() {
		return situacao;
	}
	public void setSituacao(SituacaoEnum situacao) {
		this.situacao = situacao;
	}

	@JoinColumn(name="cdpessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	@OneToMany(mappedBy="pessoaquestionario")
	public List<Pessoaquestionarioquestao> getListaPessoaQuestionarioQuestao() {
		return listaPessoaQuestionarioQuestao;
	}
	public void setListaPessoaQuestionarioQuestao(
			List<Pessoaquestionarioquestao> listaPessoaQuestionarioQuestao) {
		this.listaPessoaQuestionarioQuestao = listaPessoaQuestionarioQuestao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getDataString(){
		if(getDtaltera() != null){
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy"); 
			return format.format(getDtaltera());
		} return "";
	}
	
	@Transient
	public String getSituacaoString(){
		return getSituacao().getNome();
	}
	
	public void setSituacaoString(String situacaoString) {
		this.situacaoString = situacaoString;
	}

	@Transient
	public String getAvaliadorString() {
		return avaliadorString;
	}
	public void setAvaliadorString(String avaliadorString) {
		this.avaliadorString = avaliadorString;
	}
	
	@Transient
	public String getArquivoString() {
		return arquivoString;
	}
	public void setArquivoString(String arquivoString) {
		this.arquivoString = arquivoString;
	}
	
	@Transient
	public List<Grupoquestionario> getListaGrupoquestionario() {
		return listaGrupoquestionario;
	}
	
	public void setListaGrupoquestionario(List<Grupoquestionario> listaGrupoquestionario) {
		this.listaGrupoquestionario = listaGrupoquestionario;
	}
	
	@DisplayName("Agenda de intera��o")
	@JoinColumn(name="cdagendainteracao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Agendainteracao getAgendainteracao() {
		return agendainteracao;
	}
	public void setAgendainteracao(Agendainteracao agendainteracao) {
		this.agendainteracao = agendainteracao;
	}
	
	@Transient
	public Integer getCdagendainteracao() {
		return cdagendainteracao;
	}
	public void setCdagendainteracao(Integer cdagendainteracao) {
		this.cdagendainteracao = cdagendainteracao;
	}
}
