package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;


public class BaixarequisicaomaterialBean {
	
	protected List<Requisicaomaterial> listaProdutoEpiServico = new ListSet<Requisicaomaterial>(Requisicaomaterial.class);
	protected List<Requisicaomaterial> listaPatrimonio = new ListSet<Requisicaomaterial>(Requisicaomaterial.class);
	protected List<Requisicaomaterial> listaProdutoEpiServicoGrade = new ListSet<Requisicaomaterial>(Requisicaomaterial.class);
	
	protected String controller;
	
	protected Boolean apenasBaixa;
	
	public List<Requisicaomaterial> getListaProdutoEpiServico() {
		return listaProdutoEpiServico;
	}
	public List<Requisicaomaterial> getListaPatrimonio() {
		return listaPatrimonio;
	}
	public List<Requisicaomaterial> getListaProdutoEpiServicoGrade() {
		return listaProdutoEpiServicoGrade;
	}
	public String getController() {
		return controller;
	}
	public String getCdrequisicoes() {
		String cdrequisicoes = "";
		List<Requisicaomaterial> listaRequisicaomaterialAux = new ArrayList<Requisicaomaterial>();
		
		if (listaProdutoEpiServico!=null) listaRequisicaomaterialAux.addAll(listaProdutoEpiServico);
		if (listaPatrimonio!=null) listaRequisicaomaterialAux.addAll(listaPatrimonio);
		if (listaProdutoEpiServicoGrade!=null) listaRequisicaomaterialAux.addAll(listaProdutoEpiServicoGrade);
		
		for (Requisicaomaterial requisicaomaterial: listaRequisicaomaterialAux){
			cdrequisicoes += requisicaomaterial.getCdrequisicaomaterial() + ",";
		}
		
		if (cdrequisicoes.endsWith(",")){
			cdrequisicoes = cdrequisicoes.substring(0, cdrequisicoes.length()-1);
		}
		
		return cdrequisicoes;
	}
	@Transient
	public Boolean getApenasBaixa() {
		return apenasBaixa;
	}
	public void setListaProdutoEpiServico(List<Requisicaomaterial> listaProdutoEpiServico) {
		this.listaProdutoEpiServico = listaProdutoEpiServico;
	}
	public void setListaPatrimonio(List<Requisicaomaterial> listaPatrimonio) {
		this.listaPatrimonio = listaPatrimonio;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	public void setListaProdutoEpiServicoGrade(List<Requisicaomaterial> listaProdutoEpiServicoGrade) {
		this.listaProdutoEpiServicoGrade = listaProdutoEpiServicoGrade;
	}
	public void setApenasBaixa(Boolean apenasBaixa) {
		this.apenasBaixa = apenasBaixa;
	}
}
