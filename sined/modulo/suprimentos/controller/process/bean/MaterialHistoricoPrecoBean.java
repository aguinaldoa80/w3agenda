package br.com.linkcom.sined.modulo.suprimentos.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public class MaterialHistoricoPrecoBean {

	private Date dtVenda;
	private Integer cdVenda;
	private Material material;
	private Unidademedida unidadeMedida;
	private Double quantidade;
	private Double desconto;
	private String observacao;
	private Double valorVenda;
	private Double totalVenda;
	private String nomeCliente;
	private String nomeEmpresa;
	private String descricaoTipoPedidoVenda;
	private Double valorSeguro;
	private Double outrasDespesas;
	

	@DisplayName("Data da venda")
	public Date getDtVenda() {
		return dtVenda;
	}
	@DisplayName("C�digo da venda")
	public Integer getCdVenda() {
		return cdVenda;
	}
	public Material getMaterial() {
		return material;
	}
	@DisplayName("U.M.")
	public Unidademedida getUnidadeMedida() {
		return unidadeMedida;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getDesconto() {
		return desconto;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Valor")
	public Double getValorVenda() {
		return valorVenda;
	}
	public Double getTotalVenda() {
		return totalVenda;
	}
	@DisplayName("Cliente")
	public String getNomeCliente() {
		return nomeCliente;
	}
	@DisplayName("Empresa")
	public String getNomeEmpresa() {
		return nomeEmpresa;
	}
	@DisplayName("Tipo de pedido")
	public String getDescricaoTipoPedidoVenda() {
		return descricaoTipoPedidoVenda;
	}
	public Double getValorSeguro() {
		return valorSeguro;
	}
	public Double getOutrasDespesas() {
		return outrasDespesas;
	}
	
	public void setDtVenda(Date dtVenda) {
		this.dtVenda = dtVenda;
	}
	public void setCdVenda(Integer cdVenda) {
		this.cdVenda = cdVenda;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidadeMedida(Unidademedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}
	public void setTotalVenda(Double totalVenda) {
		this.totalVenda = totalVenda;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}
	public void setDescricaoTipoPedidoVenda(String descricaoTipoPedidoVenda) {
		this.descricaoTipoPedidoVenda = descricaoTipoPedidoVenda;
	}
	public void setValorSeguro(Double valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	public void setOutrasDespesas(Double outrasDespesas) {
		this.outrasDespesas = outrasDespesas;
	}
}
