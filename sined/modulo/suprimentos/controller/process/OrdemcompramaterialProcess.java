package br.com.linkcom.sined.modulo.suprimentos.controller.process;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.FrequenciaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialformulapesoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.util.SinedUtil;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(
		path="/suprimento/process/Ordemcompramaterial",
		authorizationModule=ProcessAuthorizationModule.class
)
public class OrdemcompramaterialProcess extends MultiActionController{
	
	private ContagerencialService contagerencialService;
	private MaterialService materialService;
	private UnidademedidaService unidademedidaService;
	private FrequenciaService frequenciaService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private ProjetoService projetoService;
	private CentrocustoService centrocustoService;
	private LocalarmazenagemService localarmazenagemService;
	private ProdutoService produtoService;
	private MaterialformulapesoService materialformulapesoService;
	private ParametrogeralService parametrogeralService;
	
	public void setMaterialformulapesoService(
			MaterialformulapesoService materialformulapesoService) {
		this.materialformulapesoService = materialformulapesoService;
	}
	public void setProdutoService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}
	public void setUnidademedidaService(
			UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setFrequenciaService(FrequenciaService frequenciaService) {
		this.frequenciaService = frequenciaService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	/**
	 * Carrega a popup de cria��o de um contato.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, Ordemcompramaterial ordemcompramaterial){
		request.setAttribute("listaContagerencial", contagerencialService.findAnaliticas(Tipooperacao.TIPO_DEBITO));
		request.setAttribute("listaProjeto", projetoService.findProjetosAbertos(null));
		request.setAttribute("centrosAtivos", centrocustoService.findAtivos());
		request.setAttribute("mostraQtdRestante", "true".equals(request.getParameter("mostraQtdRestante")));
		request.setAttribute("fornecedor", request.getParameter("fornecedor"));
		ordemcompramaterial.setCdordemcompraParam(request.getParameter("cdordemcompraParam"));
		ordemcompramaterial.setIdentificadortela(request.getParameter("identificadortela"));
		request.setAttribute("localObrigatorio", parametrogeralService.getBoolean(Parametrogeral.LOCALMATERIALOC_OBRIGATORIO));
		request.setAttribute("OC_QTE_SUP_SOLICITACAOCOMPRA", parametrogeralService.getBoolean(Parametrogeral.OC_QTE_SUP_SOLICITACAOCOMPRA));		
		return new ModelAndView("direct:process/popup/criaOrdemcompramaterial","ordemcompramaterial",ordemcompramaterial);
	}
	
	/**
	 * Exclui o contato da lista no escopo de sess�o.
	 * 
	 * @param request
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void excluir(WebRequestContext request){
		try{
			List<Ordemcompramaterial> listaOrdemcompramaterial = null;
			Object object = request.getParameter("posicaoLista");
			if (object != null) {
				String parameter = object.toString();
				Integer index = Integer.parseInt(parameter)-1;
				Object attr = request.getSession().getAttribute("listaOrdemcompramaterial" + request.getParameter("identificadortela"));
				if (attr != null) {
					listaOrdemcompramaterial = (List<Ordemcompramaterial>) attr;
					if (listaOrdemcompramaterial != null && listaOrdemcompramaterial.size() > index)
						listaOrdemcompramaterial.remove(index.intValue());
				}
			}
			request.getSession().setAttribute("listaOrdemcompramaterial" + request.getParameter("identificadortela"), listaOrdemcompramaterial);
			View.getCurrent().println("var sucesso = true;");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	
	
	/**
	 * Salva o item de intera��o no objeto interacao e logo depois joga o objeto para a sess�o.
	 * 
	 * @param request
	 * @param interacaoitem
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void salva(WebRequestContext request, Ordemcompramaterial ordemcompramaterial){
		
		Object attribute = request.getSession().getAttribute("listaOrdemcompramaterial" + ordemcompramaterial.getIdentificadortela());
		String fornecedor = request.getParameter("fornecedor");
		List<Ordemcompramaterial> listaOrdemcompramaterial = null;
		DecimalFormat format = new DecimalFormat("#.##");
		
		if(ordemcompramaterial.getMaterial() != null){
			if(ordemcompramaterial.getMaterial().getIdentificacao() == null || ordemcompramaterial.getMaterial().getNome() == null){
				ordemcompramaterial.setMaterial(materialService.loadForOrdemcompra(ordemcompramaterial.getMaterial()));
			}
		}
		
		if(ordemcompramaterial.getContagerencial() != null){
			if(ordemcompramaterial.getContagerencial().getNome() == null || ordemcompramaterial.getContagerencial().getVcontagerencial() == null || ordemcompramaterial.getContagerencial().getVcontagerencial().getIdentificador() == null){
				ordemcompramaterial.setContagerencial(contagerencialService.loadWithIdentificador(ordemcompramaterial.getContagerencial()));
			} 
		}
		
		if(ordemcompramaterial.getUnidademedida() != null){
			if(ordemcompramaterial.getUnidademedida().getSimbolo() == null){
				ordemcompramaterial.setUnidademedida(unidademedidaService.load(ordemcompramaterial.getUnidademedida()));
			}
		}
		
		if(ordemcompramaterial.getFrequencia() != null){
			if(ordemcompramaterial.getFrequencia().getNome() == null){
				ordemcompramaterial.setFrequencia(frequenciaService.load(ordemcompramaterial.getFrequencia()));
			}
		}
		
		if(ordemcompramaterial.getLocalarmazenagem() != null){
			if(ordemcompramaterial.getLocalarmazenagem().getNome() == null){
				ordemcompramaterial.setLocalarmazenagem(localarmazenagemService.load(ordemcompramaterial.getLocalarmazenagem()));
			}
		}
		
		if(ordemcompramaterial.getCentrocusto() != null){
			if(ordemcompramaterial.getCentrocusto().getNome() == null){
				ordemcompramaterial.setCentrocusto(centrocustoService.load(ordemcompramaterial.getCentrocusto()));
			}
		}
		
		if(ordemcompramaterial.getProjeto() != null){
			if(ordemcompramaterial.getProjeto().getNome() == null){
				ordemcompramaterial.setProjeto(projetoService.load(ordemcompramaterial.getProjeto()));
			}
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Double valor = SinedUtil.round(ordemcompramaterial.getQtdpedida() * ordemcompramaterial.getValorunitario(), 2);
		String codigoAlternativo = null;
		if(ordemcompramaterial.getMaterial() != null){
			if(ordemcompramaterial.getMaterial().getListaFornecedor().size() > 0){
				for(Materialfornecedor matForn: ordemcompramaterial.getMaterial().getListaFornecedor()){
					if(matForn.getFornecedor().getCdpessoa().toString().equals(fornecedor)){
						if(codigoAlternativo == null){
							codigoAlternativo = matForn.getCodigo();
						}else{
							if(!codigoAlternativo.equals(matForn.getCodigo())){
								codigoAlternativo = null;
								break;
							}
						}
						
					}
				}
				ordemcompramaterial.setCodigoalternativo(codigoAlternativo);
			}
		}
		
		if (attribute == null) {
			listaOrdemcompramaterial = new ArrayList<Ordemcompramaterial>();
			listaOrdemcompramaterial.add(ordemcompramaterial);
			
			request.getServletResponse().setContentType("text/html");
			
//			if(ordemcompramaterial.getIpiincluso() != null && ordemcompramaterial.getIpi() != null && !ordemcompramaterial.getIpiincluso()){
//				valor += ordemcompramaterial.getIpi();
//			}
//			if(ordemcompramaterial.getIcmsissincluso() != null && !ordemcompramaterial.getIcmsissincluso() && ordemcompramaterial.getIcmsiss() != null){
//				valor += ordemcompramaterial.getIcmsiss();
//			}
			ordemcompramaterial.setValor(valor);
			
			View.getCurrent().println("<script>" +
										"parent.criarOrdemcompramaterial(" +
										"'"+escapeQuotes(ordemcompramaterial.getMaterial().getAutocompleteDescription())+
										"','"+(ordemcompramaterial.getCodigoalternativo() != null ? escapeQuotes(ordemcompramaterial.getCodigoalternativo()) : "")+
										"','"+escapeQuotes(ordemcompramaterial.getContagerencial().getDescricaoCombo())+
										"','"+format.format(ordemcompramaterial.getQtdpedida())+
										"','"+(ordemcompramaterial.getUnidademedida() != null ? escapeQuotes(ordemcompramaterial.getUnidademedida().getNome()) : "")+
										"','"+escapeQuotes(ordemcompramaterial.getFrequencia().getNome())+
										"','"+(ordemcompramaterial.getQtdrestante() != null ? Util.strings.toStringDescription(ordemcompramaterial.getQtdrestante()) : "")+
										"','"+format.format(ordemcompramaterial.getQtdefrequencia())+
										"','"+(ordemcompramaterial.getValorunitario() != null ? Util.strings.toStringDescription(ordemcompramaterial.getValorunitario()) : "")+
										"','"+(ordemcompramaterial.getValorfrete() != null ? Util.strings.toStringDescription(ordemcompramaterial.getValorfrete()) : "")+
										"','"+(ordemcompramaterial.getValoroutrasdespesas() != null ? Util.strings.toStringDescription(ordemcompramaterial.getValoroutrasdespesas()) : "")+
										"','"+format.format(ordemcompramaterial.getValor())+
										"','"+dateFormat.format(ordemcompramaterial.getDtentrega())+
										"','"+(ordemcompramaterial.getLocalarmazenagem() != null ? escapeQuotes(ordemcompramaterial.getLocalarmazenagem().getNome()) : "") +
										"',\""+escapeQuotes(ordemcompramaterial.getCentrocustoprojeto()) +
										"\",'"+(ordemcompramaterial.getDtentrega() != null ? new SimpleDateFormat("dd/MM/yyyy").format(ordemcompramaterial.getDtentrega()) : "")+
										"','"+(ordemcompramaterial.getIpi() != null ? Util.strings.toStringDescription(ordemcompramaterial.getIpi()) : "")+
										"','"+(ordemcompramaterial.getIcmsiss() != null ? Util.strings.toStringDescription(ordemcompramaterial.getIcmsiss()) : "")+
										"',"+ordemcompramaterial.getMaterial().getCdmaterial().toString()+
										");parent.$.akModalRemove(true);" +
										"</script>");
		} else {
			listaOrdemcompramaterial = (List<Ordemcompramaterial>)attribute;
			
			if (ordemcompramaterial.getIndexlista() != null) {
				
				if(ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial() != null && 
						ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial().size() == 1){
					ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial().iterator().next().setQtde(ordemcompramaterial.getQtdpedida());
				}
				
				listaOrdemcompramaterial.set(ordemcompramaterial.getIndexlista(), ordemcompramaterial);
				
//				if(ordemcompramaterial.getIpiincluso() != null && ordemcompramaterial.getIpi() != null && !ordemcompramaterial.getIpiincluso()){
//					valor += ordemcompramaterial.getIpi();
//				}
//				if(ordemcompramaterial.getIcmsissincluso() != null && !ordemcompramaterial.getIcmsissincluso() && ordemcompramaterial.getIcmsiss() != null){
//					valor += ordemcompramaterial.getIcmsiss();
//				}
				ordemcompramaterial.setValor(valor);
				request.getServletResponse().setContentType("text/html");
				String html = "<script>" +
						"parent.editarOrdemcompramaterial(" +
						"'"+ordemcompramaterial.getIndexlista()+
						"','"+escapeQuotes(ordemcompramaterial.getMaterial().getAutocompleteDescription())+
						"','"+(ordemcompramaterial.getCodigoalternativo() != null ? escapeQuotes(ordemcompramaterial.getCodigoalternativo()) : "")+
						"','"+escapeQuotes(ordemcompramaterial.getContagerencial().getDescricaoCombo())+
						"','"+format.format(ordemcompramaterial.getQtdpedida())+
						"','"+(ordemcompramaterial.getUnidademedida() != null ? escapeQuotes(ordemcompramaterial.getUnidademedida().getNome()) : "")+
						"','"+escapeQuotes(ordemcompramaterial.getFrequencia().getNome())+
						"','"+(ordemcompramaterial.getQtdrestante() != null ? format.format(ordemcompramaterial.getQtdrestante()) : "")+
						"','"+format.format(ordemcompramaterial.getQtdefrequencia())+
						"','"+(ordemcompramaterial.getValorunitario() != null ? format.format(ordemcompramaterial.getValorunitario()) : "")+
						"','"+(ordemcompramaterial.getValorfrete() != null ? Util.strings.toStringDescription(ordemcompramaterial.getValorfrete()) : "")+
						"','"+(ordemcompramaterial.getValoroutrasdespesas() != null ? Util.strings.toStringDescription(ordemcompramaterial.getValoroutrasdespesas()) : "")+
						"','"+format.format(ordemcompramaterial.getValor())+
						"','"+dateFormat.format(ordemcompramaterial.getDtentrega())+
						"','"+(ordemcompramaterial.getLocalarmazenagem() != null ? escapeQuotes(ordemcompramaterial.getLocalarmazenagem().getNome()) : "") +
						"',\""+escapeQuotes(ordemcompramaterial.getCentrocustoprojeto()) +
						"\",'"+(ordemcompramaterial.getDtentrega() != null ? new SimpleDateFormat("dd/MM/yyyy").format(ordemcompramaterial.getDtentrega()) : "")+
						"','"+(ordemcompramaterial.getIpi() != null ? Util.strings.toStringDescription(ordemcompramaterial.getIpi()) : "")+
						"','"+(ordemcompramaterial.getIcmsiss() != null ? Util.strings.toStringDescription(ordemcompramaterial.getIcmsiss()) : "")+
						"',"+ordemcompramaterial.getMaterial().getCdmaterial().toString()+
						"," + (ordemcompramaterial.getFrequencia() != null ? ordemcompramaterial.getFrequencia().getCdfrequencia() : "") +
						"," + (ordemcompramaterial.getQtdefrequencia() != null ? ordemcompramaterial.getQtdefrequencia() : "") +
						");" +
						"parent.$.akModalRemove(true);" +
						"</script>";
				View.getCurrent().println(html);
			} else {
				listaOrdemcompramaterial.add(ordemcompramaterial);
				
//				if(ordemcompramaterial.getIpiincluso() != null && ordemcompramaterial.getIpi() != null && !ordemcompramaterial.getIpiincluso()){
//					valor += ordemcompramaterial.getIpi();
//				}
//				if(ordemcompramaterial.getIcmsissincluso() != null && !ordemcompramaterial.getIcmsissincluso() && ordemcompramaterial.getIcmsiss() != null){
//					valor += ordemcompramaterial.getIcmsiss();
//				}
				ordemcompramaterial.setValor(valor);
				request.getServletResponse().setContentType("text/html");			
				View.getCurrent().println("<script>" +
						"parent.criarOrdemcompramaterial(" +
						"'"+escapeQuotes(ordemcompramaterial.getMaterial().getAutocompleteDescription())+
						"','"+(ordemcompramaterial.getCodigoalternativo() != null ? escapeQuotes(ordemcompramaterial.getCodigoalternativo()) : "")+
						"','"+escapeQuotes(ordemcompramaterial.getContagerencial().getDescricaoCombo())+
						"','"+format.format(ordemcompramaterial.getQtdpedida())+
						"','"+(ordemcompramaterial.getUnidademedida() != null ? escapeQuotes(ordemcompramaterial.getUnidademedida().getNome()) : "")+
						"','"+escapeQuotes(ordemcompramaterial.getFrequencia().getNome())+
						"','"+(ordemcompramaterial.getQtdrestante() != null ? Util.strings.toStringDescription(ordemcompramaterial.getQtdrestante()) : "")+
						"','"+format.format(ordemcompramaterial.getQtdefrequencia())+
						"','"+(ordemcompramaterial.getValorunitario() != null ? Util.strings.toStringDescription(ordemcompramaterial.getValorunitario()) : "")+
						"','"+(ordemcompramaterial.getValorfrete() != null ? Util.strings.toStringDescription(ordemcompramaterial.getValorfrete()) : "")+
						"','"+(ordemcompramaterial.getValoroutrasdespesas() != null ? Util.strings.toStringDescription(ordemcompramaterial.getValoroutrasdespesas()) : "")+
						"','"+format.format(ordemcompramaterial.getValor())+
						"','"+(ordemcompramaterial.getLocalarmazenagem() != null ? escapeQuotes(ordemcompramaterial.getLocalarmazenagem().getNome()) : "") +
						"',\""+escapeQuotes(ordemcompramaterial.getCentrocustoprojeto()) +
						"\",'"+(ordemcompramaterial.getDtentrega() != null ? new SimpleDateFormat("dd/MM/yyyy").format(ordemcompramaterial.getDtentrega()) : "")+
						"','"+(ordemcompramaterial.getIpi() != null ? Util.strings.toStringDescription(ordemcompramaterial.getIpi()) : "")+
						"','"+(ordemcompramaterial.getIcmsiss() != null ? Util.strings.toStringDescription(ordemcompramaterial.getIcmsiss()) : "")+
						"',"+ordemcompramaterial.getMaterial().getCdmaterial().toString()+
						");parent.$.akModalRemove(true);" +
						"</script>");
			}
		}	
		
		request.getSession().setAttribute("listaOrdemcompramaterial" + ordemcompramaterial.getIdentificadortela(), listaOrdemcompramaterial);
	}
	
	private String escapeQuotes(String opValue) {
		if(opValue==null) return null;
		return opValue.replaceAll("\\\\", "\\\\\\\\").replaceAll("\'", "\\\\'").replaceAll("\"", "&quot");
	}
	
	/**
	 * Edita a intera��o da lista na entrada de prospec��o; 
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#carregaIndexLista
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	@Action("editar")
	public ModelAndView editar(WebRequestContext request){
		
		Ordemcompramaterial ordemcompramaterial = null;
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			String parameter = object.toString();
			Integer index = Integer.parseInt(parameter)-1;
			Object attr = request.getSession().getAttribute("listaOrdemcompramaterial" + request.getParameter("identificadortela"));
			List<Ordemcompramaterial> listaOrdemcompramaterial = null;
			if (attr != null) {
				listaOrdemcompramaterial = (List<Ordemcompramaterial>) attr;
				ordemcompramaterial = listaOrdemcompramaterial.get(index);
				ordemcompramaterial.setIndexlista(index);
				ordemcompramaterial.setCdordemcompraParam(request.getParameter("cdordemcompraParam"));
				ordemcompramaterial.setIdentificadortela(request.getParameter("identificadortela"));

				List<Unidademedida> listaUnidademedida= new ArrayList<Unidademedida>();
				if(ordemcompramaterial.getMaterial() != null){
					Material material = ordemcompramaterial.getMaterial();
					Material materialaux = null;
					if(material.getUnidademedida() == null){
						materialaux = materialService.unidadeMedidaMaterial(material);
					}
					if(materialaux != null && materialaux.getUnidademedida() != null){
						listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(materialaux.getUnidademedida(), material);
					}else if(material.getUnidademedida() != null){
						listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(material.getUnidademedida(), material);
					}
				}
				request.setAttribute("listaUnidademedida", listaUnidademedida);					
				request.setAttribute("vendaorcamento", ordemcompramaterial.getVendaorcamento());
				request.setAttribute("pedidocompra", ordemcompramaterial.getPedidovenda());
			}
			
		}	
		
		
		request.setAttribute("locaisAtivosEmpresa", localarmazenagemService.findAtivosByEmpresa(null));
		request.setAttribute("listaContagerencial", contagerencialService.findAnaliticas(Tipooperacao.TIPO_DEBITO));
		request.setAttribute("listaProjeto", projetoService.findProjetosAbertos(null));
		request.setAttribute("centrosAtivos", centrocustoService.findAtivos());
		request.setAttribute("mostraQtdRestante", "true".equals(request.getParameter("mostraQtdRestante")));
		request.setAttribute("localObrigatorio", parametrogeralService.getBoolean(Parametrogeral.LOCALMATERIALOC_OBRIGATORIO));
		request.setAttribute("OC_QTE_SUP_SOLICITACAOCOMPRA", parametrogeralService.getBoolean(Parametrogeral.OC_QTE_SUP_SOLICITACAOCOMPRA));
		return new ModelAndView("direct:process/popup/criaOrdemcompramaterial", "ordemcompramaterial", ordemcompramaterial);
	}
	
	/**
	 * M�todo para consultar o item sem ter que editar
	 * o atributo consultaitemnotafiscalproduto � utilizado para setar os campos como disable
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	@Action("consultar")
	public ModelAndView consultar(WebRequestContext request){
		
		Ordemcompramaterial ordemcompramaterial = null;
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			String parameter = object.toString();
			Integer index = Integer.parseInt(parameter);
			Object attr = request.getSession().getAttribute("listaOrdemcompramaterial" + request.getParameter("identificadortela"));
			List<Ordemcompramaterial> listaOrdemcompramaterial = null;
			if (attr != null) {
				listaOrdemcompramaterial = (List<Ordemcompramaterial>) attr;
				ordemcompramaterial = listaOrdemcompramaterial.get(index);
				ordemcompramaterial.setIndexlista(index);
				ordemcompramaterial.setCdordemcompraParam(request.getParameter("cdordemcompraParam"));
				ordemcompramaterial.setIdentificadortela(request.getParameter("identificadortela"));

				List<Unidademedida> listaUnidademedida= new ArrayList<Unidademedida>();
				if(ordemcompramaterial.getMaterial() != null){
					Material material = ordemcompramaterial.getMaterial();
					Material materialaux = null;
					if(material.getUnidademedida() == null){
						materialaux = materialService.unidadeMedidaMaterial(material);
					}
					if(materialaux != null && materialaux.getUnidademedida() != null){
						listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(materialaux.getUnidademedida(), material);
					}else if(material.getUnidademedida() != null){
						listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(material.getUnidademedida(), material);
					}
				}
				request.setAttribute("listaUnidademedida", listaUnidademedida);					
			}
			
		}	
		
		
		request.setAttribute("locaisAtivosEmpresa", localarmazenagemService.findAtivosByEmpresa(null));
		request.setAttribute("listaContagerencial", contagerencialService.findAnaliticas(Tipooperacao.TIPO_DEBITO));
		request.setAttribute("listaProjeto", projetoService.findProjetosAbertos(null));
		request.setAttribute("centrosAtivos", centrocustoService.findAtivos());
		request.setAttribute("mostraQtdRestante", "true".equals(request.getParameter("mostraQtdRestante")));
		
		request.setAttribute("consultaitemordemcompramaterial", true);
		request.setAttribute("localObrigatorio", parametrogeralService.getBoolean(Parametrogeral.LOCALMATERIALOC_OBRIGATORIO));
		
		return new ModelAndView("direct:process/popup/criaOrdemcompramaterial", "ordemcompramaterial", ordemcompramaterial);
	}
	
	/**
	 * Busca as dimens�es do material.
	 *
	 * @param request
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/09/2013
	 */
	public ModelAndView verificaDimensoesMaterialAjax(WebRequestContext request, Ordemcompramaterial ordemcompramaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(ordemcompramaterial != null && ordemcompramaterial.getMaterial() != null){
			Produto produto = produtoService.load(new Produto(ordemcompramaterial.getMaterial()), "produto.cdmaterial, produto.altura, produto.comprimento, produto.largura");
			
			if(produto != null){
				if(produto.getAltura() != null){
					jsonModelAndView.addObject("altura", produto.getAltura());
				}
				if(produto.getComprimento() != null){
					jsonModelAndView.addObject("comprimento", produto.getComprimento());
				}
				if(produto.getLargura() != null){
					jsonModelAndView.addObject("largura", produto.getLargura());
				}
			}
		}
		return jsonModelAndView;
	}
	
	/**
	 * Realiza o c�lculo de peso do material de acordo com infos do item da ordem de compra
	 *
	 * @param request
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/09/2013
	 */
	public ModelAndView calculaPesoMaterialAjax(WebRequestContext request, Ordemcompramaterial ordemcompramaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(ordemcompramaterial != null && ordemcompramaterial.getMaterial() != null){
			try{
				Double qtde = ordemcompramaterial.getQtdpedida() != null ? ordemcompramaterial.getQtdpedida() : 0d;
				boolean addPesoCampoQtde = false;
				
				Material material = materialService.load(ordemcompramaterial.getMaterial(), "material.cdmaterial, material.materialtipo, material.peso, material.pesobruto");
				if(material == null){
					material = materialService.load(ordemcompramaterial.getMaterial(), "material.cdmaterial, material.peso, material.pesobruto");
				}
				material.setListaMaterialformulapeso(materialformulapesoService.findByMaterial(material));
				
				Double peso = material.getPeso();
				if(material.getListaMaterialformulapeso() != null && material.getListaMaterialformulapeso().size() > 0){
					Double altura = ordemcompramaterial.getAltura() != null ? ordemcompramaterial.getAltura() : 0d;
					Double comprimento = ordemcompramaterial.getComprimento() != null ? ordemcompramaterial.getComprimento() : 0d;
					Double largura = ordemcompramaterial.getLargura() != null ? ordemcompramaterial.getLargura() : 0d;
					Double qtdvolume = ordemcompramaterial.getQtdvolume() != null ? ordemcompramaterial.getQtdvolume() : 0d;
					
					material.setProduto_altura(altura);
					material.setProduto_comprimento(comprimento);
					material.setProduto_largura(largura);
					material.setQtdvolume(qtdvolume);
					
					Material m = materialService.getSimboloUnidadeMedidaETempoEntrega(ordemcompramaterial.getMaterial());
					Unidademedida unidademedida = m != null ? unidademedida = m.getUnidademedida() : null;
					if(ordemcompramaterial.getUnidademedida() != null && ordemcompramaterial.getUnidademedida().getCdunidademedida() != null){
						ordemcompramaterial.setUnidademedida(unidademedidaService.load(ordemcompramaterial.getUnidademedida(), 
								"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo"));
						unidademedida = ordemcompramaterial.getUnidademedida();
					}
					
					peso = materialformulapesoService.calculaPesoMaterial(material);
					
					if(peso != null && 
							peso > 0 && 
							unidademedida != null && 
							unidademedida.getSimbolo() != null &&
							"KG".equalsIgnoreCase(unidademedida.getSimbolo())){
						addPesoCampoQtde = true;
					}
				}
				
				if(peso != null && !addPesoCampoQtde){
					peso = peso * qtde;
				}
				
				jsonModelAndView
					.addObject("peso", peso != null ? SinedUtil.roundByParametro(peso) : 0d)
					.addObject("addPesoCampoQtde", addPesoCampoQtde);
			} catch (Exception e) {
				e.printStackTrace();
				return jsonModelAndView.addObject("erro", e.getMessage());
			}
		}
		return jsonModelAndView;
	}
}
