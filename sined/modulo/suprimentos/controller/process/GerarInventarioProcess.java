package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.InventariomaterialService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.GerarInventarioFiltroBean;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/suprimento/process/GerarInventario", authorizationModule=ProcessAuthorizationModule.class)
public class GerarInventarioProcess extends MultiActionController {
	
	private InventariomaterialService inventariomaterialService;
	private LocalarmazenagemService localarmazenagemService;
	private InventarioService inventarioService;
	
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setInventariomaterialService(
			InventariomaterialService inventariomaterialService) {
		this.inventariomaterialService = inventariomaterialService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, GerarInventarioFiltroBean filtro){
		this.beforeShow(request);
		
		return new ModelAndView("/process/gerarInventario", "filtro", filtro);
	}
	
	public ModelAndView visualizar(WebRequestContext request, GerarInventarioFiltroBean filtro){
		this.beforeShow(request);
		
		if(filtro.getLocalarmazenagem() != null) filtro.setLocalarmazenagem(localarmazenagemService.load(filtro.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
		List<Inventariomaterial> listaInventariomaterial = inventariomaterialService.findForGerarInventario(filtro);
		
		Double somatorioTotal = 0d;
		Double somatorioTotalPesoLiquido = 0d;
		Double somatorioTotalPesoBruto = 0d;
		Double somatorioQtde = 0d;
		Double somatorioValorUnitario = 0d;
		
		for (Inventariomaterial inventariomaterial : listaInventariomaterial) {
			somatorioQtde += inventariomaterial.getQtde();
			somatorioValorUnitario += inventariomaterial.getValorUnitario();
			somatorioTotal += inventariomaterial.getTotal();
			somatorioTotalPesoLiquido += inventariomaterial.getTotalPesoLiquido();
			somatorioTotalPesoBruto += inventariomaterial.getTotalPesoBruto();
		}
		
		request.setAttribute("lista", listaInventariomaterial);
		request.setAttribute("somatorioQtde", SinedUtil.round(somatorioQtde, 4));
		request.setAttribute("somatorioValorUnitario", SinedUtil.round(somatorioValorUnitario, 2));
		request.setAttribute("somatorioTotal", SinedUtil.round(somatorioTotal, 2));
		request.setAttribute("somatorioTotalPesoLiquido", SinedUtil.round(somatorioTotalPesoLiquido, 4));
		request.setAttribute("somatorioTotalPesoBruto", SinedUtil.round(somatorioTotalPesoBruto, 4));
		
		return new ModelAndView("/process/gerarInventario", "filtro", filtro);
	}
	
	public ModelAndView gerar(WebRequestContext request, GerarInventarioFiltroBean filtro){
		try {
			
			if(inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(filtro.getDtinventario(), filtro.getEmpresa(), filtro.getLocalarmazenagem(), filtro.getProjeto(), filtro.getInventarioTipo())){
				request.addError("N�o � poss�vel gerar invent�rio. J� existe um invent�rio para o per�odo informado.");
			}else {
				Inventario inventario = inventariomaterialService.gerarInventario(filtro);
				request.addMessage("Invent�rio <a href=\"" + NeoWeb.getRequestContext().getServletRequest().getContextPath() + 
							"/suprimento/crud/Inventario?ACAO=consultar&cdinventario=" + inventario.getCdinventario() + "\">" + inventario.getCdinventario() + "</a> gerado com sucesso.");
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao gerar o invent�rio: " + e.getMessage());
		}
		return sendRedirectToAction("index");
	}
	
	public ModelAndView gerarCSV(WebRequestContext request, GerarInventarioFiltroBean filtro){
		return new ResourceModelAndView(inventariomaterialService.gerarCSV(filtro));
	}
	
	private void beforeShow(WebRequestContext request) {
		List<InventarioTipo> listaInventarioTipo = new ArrayList<InventarioTipo>();
		listaInventarioTipo.add(InventarioTipo.INVENTARIO);
		listaInventarioTipo.add(InventarioTipo.ESTOQUEESTRUTURADO);
		request.setAttribute("listaInventarioTipo", listaInventarioTipo);
	}
	
}
