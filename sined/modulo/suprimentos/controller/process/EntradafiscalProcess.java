package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.geral.bean.Entregadocumentofreterateio;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Tipopessoaquestionario;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.EntregadocumentofreteService;
import br.com.linkcom.sined.geral.service.EntregadocumentofreterateioService;
import br.com.linkcom.sined.geral.service.EntregamaterialService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.ModelodocumentofiscalService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.QuestionarioService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.MaterialgradeBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path={"/suprimento/process/Entradafiscal"},
		authorizationModule=ProcessAuthorizationModule.class
)
public class EntradafiscalProcess extends MultiActionController{
	
	private EntradafiscalService entradafiscalService;
	private MaterialService materialService;
	private CfopService cfopService;
	private DocumentoService documentoService;
	private EmpresaService empresaService;
	private ContagerencialService contagerencialService;
	private QuestionarioService questionarioService;
	private EntregadocumentofreteService entregadocumentofreteService;
	private EntregamaterialService entregamaterialService;
	private EntregadocumentofreterateioService entregadocumentofreterateioService;
	private LoteestoqueService loteestoqueService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private ParametrogeralService parametrogeralService;
	private PrazopagamentoService prazopagamentoService;
	private DocumentotipoService documentotipoService;
	private ModelodocumentofiscalService modelodocumentofiscalService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setCfopService(CfopService cfopService) {
		this.cfopService = cfopService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setQuestionarioService(QuestionarioService questionarioService) {
		this.questionarioService = questionarioService;
	}
	public void setEntregadocumentofreteService(EntregadocumentofreteService entregadocumentofreteService) {
		this.entregadocumentofreteService = entregadocumentofreteService;
	}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {
		this.entregamaterialService = entregamaterialService;
	}
	public void setEntregadocumentofreterateioService(EntregadocumentofreterateioService entregadocumentofreterateioService) {
		this.entregadocumentofreterateioService = entregadocumentofreterateioService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {
		this.naturezaoperacaoService = naturezaoperacaoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}
	public void setDocumentotipoService(
			DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setModelodocumentofiscalService(ModelodocumentofiscalService modelodocumentofiscalService) {this.modelodocumentofiscalService = modelodocumentofiscalService;}
	
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, Entregadocumento entregadocumento){
		entregadocumento.setAcao("criar");
		entregadocumento.setIdentificadortela(request.getParameter("identificadortela"));
		entregadocumento.setNaturezaoperacao(naturezaoperacaoService.loadPadrao(NotaTipo.ENTRADA_FISCAL));
		
		Object indexlistaforrateio = request.getParameter("posicaoListaForRateio");
		if(entregadocumento == null){
			entregadocumento = new Entregadocumento();
		}else {
			if (entregadocumento.getListaEntregamaterial() != null && 
					!entregadocumento.getListaEntregamaterial().isEmpty()) {
				for (Entregamaterial e1 : entregadocumento.getListaEntregamaterial()) {
					if(e1.getMaterial() != null && e1.getMaterial().getCdmaterial() != null)
						e1.setMaterial(materialService.load(e1.getMaterial(), "material.cdmaterial, material.nome"));
					if(e1.getCfop() != null && e1.getCfop().getCdcfop() != null)
						e1.setCfop(cfopService.carregarCfop(e1.getCfop()));
				}
			}
		}
		if (indexlistaforrateio==null){
			indexlistaforrateio = entregadocumento.getPosicaoListaParam();
		}
		if(indexlistaforrateio != null){
			entregadocumento.setIndexlistaforrateio(Integer.parseInt((String)indexlistaforrateio));
		}
		entradafiscalService.criaAtributoEntregamaterialParaCalculoRateio(request, entregadocumento);
		
		String cddocumentotipo = entregadocumento.getCdentregadocumento() != null && entregadocumento.getDocumentotipo() != null && entregadocumento.getDocumentotipo().getCddocumentotipo() != null? entregadocumento.getDocumentotipo().getCddocumentotipo().toString(): null;
		request.setAttribute("listaDocumentotipo", documentotipoService.findForCompra(cddocumentotipo));
		
		//Cria a lista de documentos antecipados utilizados nos pagamentos
		List<Documento> listaDocumento = new ArrayList<Documento>();
		List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
		if (entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null && 
				entregadocumento.getDocumentotipo() != null && entregadocumento.getDocumentotipo().getCddocumentotipo() != null){
			listaDocumento = documentoService.findForAntecipacaoEntrega(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
			listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
		}
		if(entregadocumento.getListadocumento() != null && !entregadocumento.getListadocumento().isEmpty()){
			for (Entregapagamento ep : entregadocumento.getListadocumento()){
				if (ep.getDocumentoantecipacao() != null && !listaDocumento.contains(ep.getDocumentoantecipacao())){
					if(!listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())){
						listaDocumento.add(ep.getDocumentoantecipacao());
					}
				}
			}
		}		
		request.setAttribute("listaDocumento", listaDocumento);
//		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		
		entradafiscalService.addInfAtributo(request, entregadocumento);
		
		if (entregadocumento.getCdsentregadocumento()!=null && !entregadocumento.getCdsentregadocumento().trim().equals("")){
			adicionarVinculoCTRC(request, entregadocumento, entregadocumento.getCdsentregadocumento());
		}else if (entregadocumento.getCdentregadocumentofreteRemover()!=null && !entregadocumento.getCdentregadocumentofreteRemover().trim().equals("")){
			removerVinculoCTRC(request, entregadocumento, entregadocumento.getCdentregadocumentofreteRemover());
		}
		processarMateriaisCTRC(request, entregadocumento);
		request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
		Boolean recebimentoEntradaDataCorrente = parametrogeralService.getBoolean(Parametrogeral.RECEBIMENTO_ENTRADA_DATA_CORRENTE);
		request.setAttribute("RECEBIMENTO_ENTRADA_DATA_CORRENTE", recebimentoEntradaDataCorrente);
		Boolean disableDtentrada = recebimentoEntradaDataCorrente && (entregadocumento.getCdentregadocumento() == null || entregadocumento.getDtentrada() != null);
		request.setAttribute("disableDtentrada", disableDtentrada);
		if(entregadocumento.getCdentregadocumento() == null && recebimentoEntradaDataCorrente){
			entregadocumento.setDtentrada(SinedDateUtils.currentDate());
		}
		
		return new ModelAndView("direct:process/popup/criaEntradafiscal","entregadocumento", entregadocumento);
	}
	
	@Action("criarnovo")
	public ModelAndView criarnovo(WebRequestContext request, Entregadocumento entregadocumento){
		entregadocumento.setAcao("criarnovo");
		entregadocumento.setIdentificadortela(request.getParameter("identificadortela"));
		
		Object indexlistaforrateio = request.getParameter("posicaoListaForRateio");
		if(entregadocumento == null){
			entregadocumento = new Entregadocumento();
		}else {
			if (entregadocumento.getListaEntregamaterial() != null && 
					!entregadocumento.getListaEntregamaterial().isEmpty()) {
				for (Entregamaterial e1 : entregadocumento.getListaEntregamaterial()) {
					if(e1.getMaterial() != null && e1.getMaterial().getCdmaterial() != null)
						e1.setMaterial(materialService.load(e1.getMaterial(), "material.cdmaterial, material.nome"));
					if(e1.getCfop() != null && e1.getCfop().getCdcfop() != null)
						e1.setCfop(cfopService.carregarCfop(e1.getCfop()));
				}
			}
		}
		if(indexlistaforrateio != null){
			entregadocumento.setIndexlistaforrateio(Integer.parseInt((String)indexlistaforrateio));
		}
		entradafiscalService.criaAtributoEntregamaterialParaCalculoRateio(request, entregadocumento);
		
		//Cria a lista de documentos antecipados utilizados nos pagamentos
		List<Documento> listaDocumento = new ArrayList<Documento>();
		List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
		if (entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null && 
				entregadocumento.getDocumentotipo() != null && entregadocumento.getDocumentotipo().getCddocumentotipo() != null){
			listaDocumento = documentoService.findForAntecipacaoEntrega(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
			listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
		}
		if(entregadocumento.getListadocumento() != null && !entregadocumento.getListadocumento().isEmpty()){
			for (Entregapagamento ep : entregadocumento.getListadocumento()){
				if (ep.getDocumentoantecipacao() != null && !listaDocumento.contains(ep.getDocumentoantecipacao())){
					if(!listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())){
						listaDocumento.add(ep.getDocumentoantecipacao());
					}
				}
			}
		}	
		request.setAttribute("listaDocumento", listaDocumento);
		
		request.setAttribute("listaEmpresa", empresaService.findAtivos(entregadocumento.getEmpresa()));
//		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
		
		return new ModelAndView("direct:process/popup/crianovoEntradafiscal","entregadocumento", entregadocumento);
	}
	
	@SuppressWarnings("unchecked")
	@Action("editar")
	public ModelAndView editar(WebRequestContext request, Entregadocumento entregadocumentoTela){
		
		Entregadocumento entregadocumento = null;
		
		Object object = request.getParameter("posicaoLista");
		if (object==null){
			object = entregadocumentoTela.getIndexlista()!=null ? entregadocumentoTela.getIndexlista().toString() : null;
		}
		String cdentregastr = request.getParameter("cdentrega");
		if (object != null) {
			Integer index = Integer.parseInt((String)object);
			Object attr = request.getSession().getAttribute("listaEntregadocumento" + request.getParameter("identificadortela"));
			List<Entregadocumento> listaEntregadocumento = null;
			if (attr != null) {
				listaEntregadocumento = (List<Entregadocumento>) attr;
				entregadocumento = listaEntregadocumento.get(index);
				entregadocumento.setIndexlista(index);
				entregadocumento.setAcao("editar");
				entregadocumento.setIdentificadortela(request.getParameter("identificadortela"));
				
				request.setAttribute("tamanholista", entregadocumento.getListaEntregamaterial() != null ? entregadocumento.getListaEntregamaterial().size() : 0);
				if (entregadocumento.getCdentregadocumento() != null && entregadocumento.getListaEntregamaterial() != null && 
						!entregadocumento.getListaEntregamaterial().isEmpty()) {
					for (Entregamaterial e1 : entregadocumento.getListaEntregamaterial()) {
						e1.setMaterialclassetrans(e1.getMaterialclasse());
					}
				}
				
				if (entregadocumento.getListaEntregamaterial() != null && 
						!entregadocumento.getListaEntregamaterial().isEmpty()) {
					Loteestoque loteestoque;
					for (Entregamaterial e1 : entregadocumento.getListaEntregamaterial()) {
						e1.setUnidademedidacomercialAnterior(e1.getUnidademedidacomercial());
						if(e1.getMaterial() != null && e1.getMaterial().getCdmaterial() != null)
							e1.setMaterial(materialService.loadWithGrade(e1.getMaterial()));
						if(e1.getCfop() != null && e1.getCfop().getCdcfop() != null)
							e1.setCfop(cfopService.carregarCfop(e1.getCfop()));
						if(e1.getLoteestoque() != null){
							if(e1.getLoteestoque().getNumero() == null){
								loteestoque = loteestoqueService.load(e1.getLoteestoque(), "loteestoque.cdloteestoque, loteestoque.numero");
								if(loteestoque != null) e1.getLoteestoque().setNumero(loteestoque.getNumero());
							}
						}else if(SinedUtil.isListNotEmpty(e1.getListaEntregamateriallote())){
							for(Entregamateriallote entregamateriallote : e1.getListaEntregamateriallote()){
								if(entregamateriallote.getLoteestoque() != null){
									if(entregamateriallote.getLoteestoque().getNumero() == null){
										loteestoque = loteestoqueService.load(entregamateriallote.getLoteestoque(), "loteestoque.cdloteestoque, loteestoque.numero");
										if(loteestoque != null) entregamateriallote.getLoteestoque().setNumero(loteestoque.getNumero());
									}
								}
							}
						}
						e1.setHabilitaPesoMedio(e1.getMaterial()!=null && e1.getMaterial().getMaterialgrupo()!=null &&
								Boolean.TRUE.equals(e1.getMaterial().getMaterialgrupo().getRegistrarpesomedio()));
					}
				}
				
				entradafiscalService.criaAtributoEntregamaterialParaCalculoRateio(request, entregadocumento);
				entradafiscalService.calculaQtdeRestanteEntregadocumento(entregadocumento, false);
				
				
				
			}
		}
		
		if(SinedUtil.validaNumeros(cdentregastr)){
			request.setAttribute("cdentrega",cdentregastr);
			if(entregadocumento != null){
				entregadocumento.setCdentrega(cdentregastr);
			}
		}
		
		//Cria a lista de documentos antecipados utilizados nos pagamentos
		List<Documento> listaDocumento = new ArrayList<Documento>();
		List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
		if (entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null && 
				entregadocumento.getDocumentotipo() != null && entregadocumento.getDocumentotipo().getCddocumentotipo() != null){
			listaDocumento = documentoService.findForAntecipacaoEntrega(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
			listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
		}
		if(entregadocumento.getListadocumento() != null && !entregadocumento.getListadocumento().isEmpty()){
			for (Entregapagamento ep : entregadocumento.getListadocumento()){
				if (ep.getDocumentoantecipacao() != null && !listaDocumento.contains(ep.getDocumentoantecipacao())){
					if(!listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())){
						listaDocumento.add(ep.getDocumentoantecipacao());
					}
				}
			}
		}
		entradafiscalService.addInfAtributo(request, entregadocumento);
		request.setAttribute("listaDocumento", listaDocumento);
		request.setAttribute("listaEmpresa", empresaService.findAtivos(entregadocumento.getEmpresa()));
//		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		
		if (entregadocumento!=null){
			if (entregadocumentoTela.getCdsentregadocumento()!=null && !entregadocumentoTela.getCdsentregadocumento().trim().equals("")){
				entregadocumento.setModelodocumentofiscal(entregadocumentoTela.getModelodocumentofiscal());
				adicionarVinculoCTRC(request, entregadocumento, entregadocumentoTela.getCdsentregadocumento());
			}else if (entregadocumentoTela.getCdentregadocumentofreteRemover()!=null && !entregadocumentoTela.getCdentregadocumentofreteRemover().trim().equals("")){
				entregadocumento.setModelodocumentofiscal(entregadocumentoTela.getModelodocumentofiscal());
				removerVinculoCTRC(request, entregadocumento, entregadocumentoTela.getCdentregadocumentofreteRemover());
			}else {
				if (entregadocumento.getCdentregadocumento()!=null){
					entregadocumento.setListaEntregadocumentofrete(new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class, entregadocumentofreteService.findByEntregadocumento(entregadocumento)));
					entregadocumento.setListaEntregadocumentofreterateio(new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class, entregadocumentofreterateioService.findByEntregadocumento(entregadocumento)));
				}
			}
			processarMateriaisCTRC(request, entregadocumento);
		}
		
		entradafiscalService.setDocumentoreferenciado(entregadocumento);
		
		request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
		
		return new ModelAndView("direct:process/popup/criaEntradafiscal","entregadocumento",entregadocumento);
	}
	
	
	@SuppressWarnings("unchecked")
	@Action("excluir")
	public void excluir(WebRequestContext request){
		
		List<Entregadocumento> listaEntregadocumento = null;
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			String parameter = object.toString();
			parameter = parameter.substring(parameter.indexOf("indice=")+7, parameter.length()-1);
			Integer index = Integer.parseInt(parameter)-1;
			Object attr = request.getSession().getAttribute("listaEntregadocumento" +  request.getParameter("identificadortela"));
			if (attr != null) {
				listaEntregadocumento = (List<Entregadocumento>) attr;
				listaEntregadocumento.remove(index.intValue());
			}
		}
		request.getSession().setAttribute("listaEntregadocumento" + request.getParameter("identificadortela"), listaEntregadocumento);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.close();</script>");
	}
	
	
	@Action("salvanovo")
	public void salvaNovo(WebRequestContext request, Entregadocumento entregadocumento){
		entradafiscalService.saveOrUpdate(entregadocumento);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.close();</script>");
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView verificarProjetoSelecionado(WebRequestContext request, Entregadocumento entregadocumentoTela){
		Object attribute = request.getSession().getAttribute("listaEntregadocumento" + request.getParameter("identificadortela"));
		
		boolean projetoSelecionadoEntregaDocumento = false;
		if (attribute != null) {
			List<Entregadocumento> listaEntregadocumento = (List<Entregadocumento>)attribute;
			if(listaEntregadocumento != null && listaEntregadocumento.size() > 0){
				for (Entregadocumento entregadocumento : listaEntregadocumento) {
					if(entregadocumento.getRateio() != null && entregadocumento.getRateio().getListaRateioitem() != null){
						for (Rateioitem rateioitem : entregadocumento.getRateio().getListaRateioitem()) {
							if(rateioitem != null && rateioitem.getProjeto() != null){
								projetoSelecionadoEntregaDocumento = true;
								break;
							}
						}
						if(projetoSelecionadoEntregaDocumento){
							break;
						}
					}
				}
			}
		}
		return new JsonModelAndView().addObject("projetoSelecionadoEntregaDocumento", projetoSelecionadoEntregaDocumento + "");
	}
	
	@SuppressWarnings("unchecked")
	@Input("criar")
	@Action("salvainteracao")
	public void salvaInteracao(WebRequestContext request, Entregadocumento entregadocumento){
		
		String cdentregastr = request.getParameter("cdentrega");
		Object attribute = request.getSession().getAttribute("listaEntregadocumento" + request.getParameter("identificadortela"));
		List<Entregadocumento> listaEntregadocumento = null;
		
		Boolean avaliarfornecedor = false;
		if((cdentregastr == null || "".equals(cdentregastr) || "<null>".equals(cdentregastr) ) && entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null){
//			Fornecedor fornecedor = fornecedorService.carregaFornecedor(entregadocumento.getFornecedor());
			Questionario questionario = questionarioService.carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario.FORNECEDOR);
			if(questionario != null && questionario.getAvaliarrecebimento() != null && questionario.getAvaliarrecebimento() && entregadocumento.getFornecedor().getPessoaquestionariotrans() == null){
				avaliarfornecedor = true;
			}
		}
		
		if (attribute == null) {
			listaEntregadocumento = new ArrayList<Entregadocumento>();
			listaEntregadocumento.add(entregadocumento);
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(criaScript(entregadocumento, "montaListaEntregadocumento", avaliarfornecedor));
		} else {
			listaEntregadocumento = (List<Entregadocumento>)attribute;
			
			if (entregadocumento.getIndexlista() != null) {
				listaEntregadocumento.set(entregadocumento.getIndexlista(), entregadocumento);				
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println(criaScript(entregadocumento, "editaItem", avaliarfornecedor));
			} else {
				listaEntregadocumento.add(entregadocumento);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println(criaScript(entregadocumento, "montaListaEntregadocumento", avaliarfornecedor));
			}
		}	
		
		if(entregadocumento != null && entregadocumento.getIndexlistaforrateio() != null)
			request.getSession().removeAttribute("listaEntregamaterial" + entregadocumento.getIndexlistaforrateio());
		
		request.getSession().setAttribute("listaEntregadocumento" + request.getParameter("identificadortela"), listaEntregadocumento);
	}
	
	private String criaScript(Entregadocumento entregadocumento, String funcao, Boolean avaliarfornecedor){
		String fornecedor = "";
		if(entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getNome() != null)
			fornecedor = entregadocumento.getFornecedor().getNome();
		String fornecedorcdpessoa = "";
		if(entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null)
			fornecedorcdpessoa = entregadocumento.getFornecedor().getCdpessoa().toString();
		
		String index = "";
		if("editaItem".equals(funcao)){
			index = "'" + entregadocumento.getIndexlista() + "',";
		}
		
		return ("<script>" +
				"window.parent."+funcao+"(" + index + "'"+fornecedor.replace("'", "\\'")+
				"','"+(entregadocumento.getNumero() != null ? entregadocumento.getNumero() : "" )+
				"','"+(entregadocumento.getValor() != null ? entregadocumento.getValor() : "")+
				"','" +(fornecedorcdpessoa != null ? fornecedorcdpessoa : "") +
				"','" +(avaliarfornecedor != null && avaliarfornecedor ? "true" : "false") +
				"');" +
				"parent.$.akModalRemove(true);" +
				"</script>");
	}
	
//	@SuppressWarnings("unchecked")
	@Command(validate=false)
	public ModelAndView calculaRateio(WebRequestContext request, Entregadocumento bean) throws CrudException{
		String acao = "editar";
		if(bean.getAcao() != null && !"".equals(bean.getAcao())){
			acao = bean.getAcao();
		}
		
		request.setAttribute("tamanholista", bean.getListaEntregamaterial() != null ? bean.getListaEntregamaterial().size() : 0);
		
		if(bean.getListaEntregamaterial() != null && bean.getListaEntregamaterial().size() > 0)
			entradafiscalService.calculaRateio(bean);		
		
		if("editar".equals(acao)){
			if(bean.getListaEntregamaterial() != null && bean.getListaEntregamaterial().size() > 0){
				for(Entregamaterial e1 : bean.getListaEntregamaterial()){					
					if(e1.getMaterial() != null && e1.getMaterial().getCdmaterial() != null)
						e1.setMaterial(materialService.load(e1.getMaterial(), "material.cdmaterial, material.nome"));
					if(e1.getCfop() != null && e1.getCfop().getCdcfop() != null)
						e1.setCfop(cfopService.carregarCfop(e1.getCfop()));
				}
			}
			request.setAttribute("listaEmpresa", empresaService.findAtivos(bean.getEmpresa()));
			
			//Cria a lista de documentos antecipados utilizados nos pagamentos
			List<Documento> listaDocumento = new ArrayList<Documento>();
			List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
			if (bean.getFornecedor() != null && bean.getFornecedor().getCdpessoa() != null && 
					bean.getDocumentotipo() != null && bean.getDocumentotipo().getCddocumentotipo() != null){
				listaDocumento = documentoService.findForAntecipacaoEntrega(bean.getFornecedor(), bean.getDocumentotipo());
				listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(bean.getFornecedor(), bean.getDocumentotipo());
			}
			if(bean.getListadocumento() != null && !bean.getListadocumento().isEmpty()){
				for (Entregapagamento ep : bean.getListadocumento()){
					if (ep.getDocumentoantecipacao() != null && !listaDocumento.contains(ep.getDocumentoantecipacao())){
						if(!listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())){
							listaDocumento.add(ep.getDocumentoantecipacao());
						}
					}
				}
			}		
			request.setAttribute("listaDocumento", listaDocumento);
//			request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
			request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
			entradafiscalService.addInfAtributo(request, bean);
			request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
			
			return new ModelAndView("direct:process/popup/criaEntradafiscal","entregadocumento", bean);
		}
		
		entradafiscalService.addInfAtributo(request, bean);
		return continueOnAction(acao, bean);
	}
	
	public ModelAndView verificaButtonGrade(WebRequestContext request){
		return entradafiscalService.verificaButtonGrade(request);
	}
	
	public ModelAndView buscarMaterialmestregrade(WebRequestContext request){
		return entradafiscalService.buscarMaterialmestregrade(request);
	}
	
	public ModelAndView selecionarItensGrade(WebRequestContext request){
		return new ModelAndView("direct:process/popup/popUpSelecionarItensGradeRecebimento", "bean", entradafiscalService.selecionarItensGrade(request));
	}
	
	public ModelAndView atualizarItensGrade(WebRequestContext request){
		return new ModelAndView("direct:process/popup/popUpAtualizarItensGradeRecebimento", "bean", entradafiscalService.atualizarItensGrade(request));
	}
	
	public ModelAndView salvarAtualizacaoItensGrade(WebRequestContext request, MaterialgradeBean materialgradeBean){
		return entradafiscalService.salvarAtualizacaoItensGrade(request, materialgradeBean);
	}
	
	@Command(validate=false)
	@Action("ajustarListaAposIncluirItensGrade")
	public ModelAndView ajustarListaAposIncluirItensGrade(WebRequestContext request, Entregadocumento bean) throws CrudException{
		if(bean.getListaEntregamaterial() != null && bean.getListaEntregamaterial().size() > 0)
			entradafiscalService.ajustarListaAposIncluirItensGrade(bean);
			
		String acao = "editar";
		if(bean.getAcao() != null && !"".equals(bean.getAcao())){
			acao = bean.getAcao();
		}
		if("editar".equals(acao)){
			if(bean.getListaEntregamaterial() != null && bean.getListaEntregamaterial().size() > 0){
				for(Entregamaterial e1 : bean.getListaEntregamaterial()){					
					if(e1.getMaterial() != null && e1.getMaterial().getCdmaterial() != null)
						e1.setMaterial(materialService.load(e1.getMaterial(), "material.cdmaterial, material.nome"));
					if(e1.getCfop() != null && e1.getCfop().getCdcfop() != null)
						e1.setCfop(cfopService.carregarCfop(e1.getCfop()));
				}
			}
			request.setAttribute("listaEmpresa", empresaService.findAtivos(bean.getEmpresa()));
			
			//Cria a lista de documentos antecipados utilizados nos pagamentos
			List<Documento> listaDocumento = new ArrayList<Documento>();
			List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
			if (bean.getFornecedor() != null && bean.getFornecedor().getCdpessoa() != null && 
					bean.getDocumentotipo() != null && bean.getDocumentotipo().getCddocumentotipo() != null){
				listaDocumento = documentoService.findForAntecipacaoEntrega(bean.getFornecedor(), bean.getDocumentotipo());
				listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(bean.getFornecedor(), bean.getDocumentotipo());
			}
			if(bean.getListadocumento() != null && !bean.getListadocumento().isEmpty()){
				for (Entregapagamento ep : bean.getListadocumento()){
					if (ep.getDocumentoantecipacao() != null && !listaDocumento.contains(ep.getDocumentoantecipacao())){
						if(!listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())){
							listaDocumento.add(ep.getDocumentoantecipacao());
						}
					}
				}
			}		
			request.setAttribute("listaDocumento", listaDocumento);
			request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
			entradafiscalService.addInfAtributo(request, bean);
			request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
			
			return new ModelAndView("direct:process/popup/criaEntradafiscal","entregadocumento", bean);
		}
		
		entradafiscalService.addInfAtributo(request, bean);
		return continueOnAction(acao, bean);
	}

	public ModelAndView montarGrade(WebRequestContext request, Material material){
		return materialService.montarGrade(request, material);
	}
	
	public ModelAndView limparGrade(WebRequestContext request, Entregamaterial entregamaterial){
		return materialService.limparGrade(request, entregamaterial);
	}
	
	public ModelAndView salvarGrade(WebRequestContext request, Material material){
		return materialService.salvarGrade(request, material);
	}
	
	public ModelAndView buscarInfMaterialGradeAJAX(WebRequestContext request, Material material){
		return materialService.buscarInfMaterialGradeAJAX(request, material);
	}
	
	public ModelAndView buscarInfLoteestoqueGradeAJAX(WebRequestContext request, Loteestoque loteestoque){
		return materialService.buscarInfLoteestoqueGradeAJAX(request, loteestoque);
	}
	
	public ModelAndView trocaEmpresaOuClienteOuNaturezaoperacao(WebRequestContext request, Entregadocumento entregadocumento){
		String acao = "editar";
		if(entregadocumento.getAcao() != null && !"".equals(entregadocumento.getAcao())){
			acao = entregadocumento.getAcao();
		}
		
		request.setAttribute("tamanholista", entregadocumento.getListaEntregamaterial() != null ? entregadocumento.getListaEntregamaterial().size() : 0);
		
		entradafiscalService.trocaEmpresaOuClienteOuNaturezaoperacao(request, entregadocumento);
		
		if("editar".equals(acao)){
			if(entregadocumento.getListaEntregamaterial() != null && entregadocumento.getListaEntregamaterial().size() > 0){
				for(Entregamaterial e1 : entregadocumento.getListaEntregamaterial()){					
					if(e1.getMaterial() != null && e1.getMaterial().getCdmaterial() != null)
						e1.setMaterial(materialService.load(e1.getMaterial(), "material.cdmaterial, material.nome"));
					if(e1.getCfop() != null && e1.getCfop().getCdcfop() != null)
						e1.setCfop(cfopService.carregarCfop(e1.getCfop()));
				}
			}
			request.setAttribute("listaEmpresa", empresaService.findAtivos(entregadocumento.getEmpresa()));
			
			//Cria a lista de documentos antecipados utilizados nos pagamentos
			List<Documento> listaDocumento = new ArrayList<Documento>();
			List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
			if (entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null && 
					entregadocumento.getDocumentotipo() != null && entregadocumento.getDocumentotipo().getCddocumentotipo() != null){
				listaDocumento = documentoService.findForAntecipacaoEntrega(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
				listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
			}
			if(entregadocumento.getListadocumento() != null && !entregadocumento.getListadocumento().isEmpty()){
				for (Entregapagamento ep : entregadocumento.getListadocumento()){
					if (ep.getDocumentoantecipacao() != null && !listaDocumento.contains(ep.getDocumentoantecipacao())){
						if(!listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())){
							listaDocumento.add(ep.getDocumentoantecipacao());
						}
					}
				}
			}		
			request.setAttribute("listaDocumento", listaDocumento);
//			request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
			request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
			entradafiscalService.addInfAtributo(request, entregadocumento);
			request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
			
			return new ModelAndView("direct:process/popup/criaEntradafiscal","entregadocumento", entregadocumento);
		}
		
		entradafiscalService.addInfAtributo(request, entregadocumento);
		return continueOnAction(acao, entregadocumento);
	}
	
	public void ajaxOnChangeGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao){
		entradafiscalService.ajaxOnChangeGrupotributacao(request, grupotributacao);
	}
	
	public void ajaxCarregaGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao){
		entradafiscalService.ajaxCarregaGrupotributacao(request, grupotributacao);
	}
	
	private void adicionarVinculoCTRC(WebRequestContext request, Entregadocumento form, String cdsentregadocumento){
		
		String whereNotInCdentregadocumento = null; 
		
		if (form.getListaEntregadocumentofrete()!=null && !form.getListaEntregadocumentofrete().isEmpty()){
			whereNotInCdentregadocumento = CollectionsUtil.listAndConcatenate(form.getListaEntregadocumentofrete(), "entregadocumentovinculo.cdentregadocumento", ",");
		}
		
		List<Entregadocumento> listaEntregadocumento = entradafiscalService.findForCTRC(cdsentregadocumento, whereNotInCdentregadocumento);
		
		if (!listaEntregadocumento.isEmpty()){
			//Vinculo
			Set<Entregadocumentofrete> listaEntregadocumentofreteAdd = new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class);
			for (Entregadocumento entregadocumentovinculo: listaEntregadocumento){
				listaEntregadocumentofreteAdd.add(new Entregadocumentofrete(entregadocumentovinculo));
			}
			if (form.getListaEntregadocumentofrete()==null){
				form.setListaEntregadocumentofrete(listaEntregadocumentofreteAdd);
			}else {
				form.getListaEntregadocumentofrete().addAll(listaEntregadocumentofreteAdd);
			}
			
			//Materiais
			String whereInCdentregadocumento = CollectionsUtil.listAndConcatenate(listaEntregadocumento, "cdentregadocumento", ",");
			String whereNotInCdentregamaterial = null; 
			if (form.getListaEntregadocumentofreterateio()!=null && !form.getListaEntregadocumentofreterateio().isEmpty()){
				whereNotInCdentregamaterial = CollectionsUtil.listAndConcatenate(form.getListaEntregadocumentofreterateio(), "entregamaterial.cdentregamaterial", ",");
			}
			List<Entregamaterial> listaEntregamaterial = entregamaterialService.findForCTRC(whereInCdentregadocumento, whereNotInCdentregamaterial);
			if (!listaEntregamaterial.isEmpty()){
				Set<Entregadocumentofreterateio> listaEntregadocumentofreterateioAdd = new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class);
				for (Entregamaterial entregamaterial: listaEntregamaterial){
					listaEntregadocumentofreterateioAdd.add(new Entregadocumentofreterateio(entregamaterial));
				}
				if (form.getListaEntregadocumentofreterateio()==null){
					form.setListaEntregadocumentofreterateio(listaEntregadocumentofreterateioAdd);
				}else {
					form.getListaEntregadocumentofreterateio().addAll(listaEntregadocumentofreterateioAdd);
				}
			}
		}
	}
	
	private void removerVinculoCTRC(WebRequestContext request, Entregadocumento form, String cdentregadocumentofreteRemover){
		if (form.getListaEntregadocumentofrete()!=null && !form.getListaEntregadocumentofrete().isEmpty()){
			for (Iterator<Entregadocumentofrete> iterator1 = form.getListaEntregadocumentofrete().iterator(); iterator1.hasNext();) {
				Entregadocumentofrete entregadocumentofrete = (Entregadocumentofrete) iterator1.next();
				if (cdentregadocumentofreteRemover.equals(entregadocumentofrete.getCdentregadocumentofrete().toString())){
					if (form.getListaEntregadocumentofreterateio()!=null){
						for (Iterator<Entregadocumentofreterateio> iterator2 = form.getListaEntregadocumentofreterateio().iterator(); iterator2.hasNext();) {
							Entregadocumentofreterateio entregadocumentofreterateio = (Entregadocumentofreterateio) iterator2.next();
							if (entregadocumentofreterateio.getEntregamaterial().getEntregadocumento().getCdentregadocumento().equals(entregadocumentofrete.getEntregadocumentovinculo().getCdentregadocumento())){
								iterator2.remove();
								
							}
						}
					}
					iterator1.remove();
				}
			}
		}
	}
	
	private void processarMateriaisCTRC(WebRequestContext request, Entregadocumento form){
		
		Set<Entregadocumentofreterateio> listaEntregadocumentofreterateio = form.getListaEntregadocumentofreterateio();
		Money valorTotalDocumentoCTRC = form.getValor()==null ?  new Money(0) : form.getValor();
		Money valorTotalRateioCTRC = new Money(0);
		
		if (listaEntregadocumentofreterateio!=null && !listaEntregadocumentofreterateio.isEmpty()){
			for (Entregadocumentofreterateio entregadocumentofreterateio: listaEntregadocumentofreterateio){
				if (entregadocumentofreterateio.getEntregamaterial().getQtde()==null){
					entregadocumentofreterateio.getEntregamaterial().setQtde(0D);
				}
				if (entregadocumentofreterateio.getEntregamaterial().getValorunitario()==null){
					entregadocumentofreterateio.getEntregamaterial().setValorunitario(0D);
				}
				Double qtde = entregadocumentofreterateio.getEntregamaterial().getQtde();
				Double valorunitario = entregadocumentofreterateio.getEntregamaterial().getValorunitario();
				double valortotal = valorunitario * qtde;
				entregadocumentofreterateio.getEntregamaterial().setValortotal(valortotal);
				
				if (entregadocumentofreterateio.getValor()!=null){
					valorTotalRateioCTRC = valorTotalRateioCTRC.add(entregadocumentofreterateio.getValor());
				}
			}
		}
		
		form.setValorTotalDocumentoCTRC(valorTotalDocumentoCTRC);
		form.setValorTotalRateioCTRC(valorTotalRateioCTRC);
		form.setValorRestanteCTRC(valorTotalDocumentoCTRC.subtract(valorTotalRateioCTRC));
	}
	
	public ModelAndView abrirPopUpApuracaoicms(WebRequestContext request, Entregamaterial entregamaterial){
		return entradafiscalService.abrirPopUpApuracaoicms(request, entregamaterial, "process");
	}
	
	/**
	* M�todo que preenche o array de fun��o de icmsst
	*
	* @see br.com.linkcom.sined.geral.service.EntradafiscalService#preencherArrayFuncaoICMSSTAjax(WebRequestContext request, Entregadocumento entregadocumento)
	*
	* @param request
	* @param entregadocumento
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public ModelAndView preencherArrayFuncaoICMSSTAjax(WebRequestContext request, Entregadocumento entregadocumento){
		return entradafiscalService.preencherArrayFuncaoICMSSTAjax(request, entregadocumento);
	}
	
	/**
	* M�todo que converte a quantidade restante ao trocar a unidade de medida
	*
	* @param request
	* @param entregamaterial
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView converteUnidademedida(WebRequestContext request, Entregamaterial entregamaterial){
		return entradafiscalService.converteUnidademedida(request, entregamaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.EntradafiscalService#gerenciarLote(WebRequestContext request, Entregamaterial entregamaterial, String modulo)
	*
	* @param request
	* @param entregamaterial
	* @return
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public ModelAndView gerenciarLote(WebRequestContext request, Entregamaterial entregamaterial){
		return entradafiscalService.gerenciarLote(request, entregamaterial, "process");
	}
	
	/** 
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#ajaxVerificaObrigatoriedadeLote(WebRequestContext request)
	*
	* @param request
	* @return
	* @since 07/12/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificaObrigatoriedadeLote(WebRequestContext request){
		return materialService.ajaxVerificaObrigatoriedadeLote(request);
	}
	
	public ModelAndView ajaxInfoFornecedor(WebRequestContext request, Entregadocumento doc){
		return entradafiscalService.ajaxInfoFornecedor(request, doc);
	}
	
	public ModelAndView ajaxBuscarDocumentoreferenciado(WebRequestContext request, Entregamaterial entregamaterial) {
		return entradafiscalService.ajaxBuscarDocumentoreferenciado(request, entregamaterial);
	}
	
	public ModelAndView ajaxPrazopagamento(WebRequestContext request, Entregadocumento entregadocumento){
		return prazopagamentoService.ajaxPrazopagamentoParcelasiguaisjuros(entregadocumento.getPrazopagamento());
	}
	
	public ModelAndView buscaInformacoesModelo(WebRequestContext request, Entregadocumento entregadocumento){
		return modelodocumentofiscalService.buscaInformacoesModelo(entregadocumento.getModelodocumentofiscal());
	}
	
	public ModelAndView ajaxInfoNaturezaOperacao(WebRequestContext request, Entregadocumento doc){
		return entradafiscalService.ajaxInfoNaturezaOperacao(doc);
	}
	
	public ModelAndView ajaxIsApropriacao(WebRequestContext request, Entregadocumento entregaDocumento){
		return entradafiscalService.ajaxIsApropriacao(request, entregaDocumento);
	}
	
	public void ajaxMontaApropriacoes(WebRequestContext request, Entregadocumento entregaDocumento){
		entradafiscalService.ajaxMontaApropriacoes(request, entregaDocumento);
	}
}