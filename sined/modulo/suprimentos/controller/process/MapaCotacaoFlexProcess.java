package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.CotacaoFiltro;

@Controller(path="/suprimento/process/MapacotacaoFlex", authorizationModule=ProcessAuthorizationModule.class)
public class MapaCotacaoFlexProcess extends MultiActionController {
	
	/**
	 * M�todo que redireciona para a tela do Flex. � feito desse jeito pois se o usu�rio der F5 no browser abre um novo carregamento
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	@DefaultAction
	public ModelAndView index(WebRequestContext request, CotacaoFiltro filtro) {
		return new ModelAndView("process/mapacotacaoflex","filtro",filtro);
	}

	/**
	 * M�todo que coloca o codigo do carregamento na sess�o e redireciona para a p�gina de edi��o do flex
	 * 
	 * @param request
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView editar(WebRequestContext request) {
		String cdcotacao = request.getParameter("cdcotacao");
		NeoWeb.getRequestContext().getSession().setAttribute("cdcotacao", cdcotacao);
		return new ModelAndView("redirect:/suprimento/process/MapacotacaoFlex");
	}
}

