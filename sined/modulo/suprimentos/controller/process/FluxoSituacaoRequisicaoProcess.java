package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.BAIXADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.EM_PROCESSO_COMPRA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.ESTORNAR;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.ROMANEIO_GERADO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Situacaorequisicao;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.RequisicaomaterialService;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.geral.service.RomaneioitemService;
import br.com.linkcom.sined.geral.service.RomaneioorigemService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterial;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterialItem;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.BaixarequisicaomaterialBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/suprimento/process/FluxoSituacaoRequisicao", authorizationModule=ProcessAuthorizationModule.class)
public class FluxoSituacaoRequisicaoProcess extends MultiActionController{
	
	private RequisicaomaterialService requisicaomaterialService;
	private SolicitacaocompraService solicitacaocompraService;
	private PatrimonioitemService patrimonioitemService;
	private ParametrogeralService parametrogeralService;
	private RomaneioService romaneioService;
	private RomaneioorigemService romaneioorigemService;
	private RomaneioitemService romaneioitemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private LoteestoqueService loteestoqueService;
	private LocalarmazenagemService localarmazenagemService;
	private MotivoavisoService motivoavisoService;
	private AvisoService avisoService;
	private EmpresaService empresaService;
	
	public void setRomaneioitemService(RomaneioitemService romaneioitemService) {
		this.romaneioitemService = romaneioitemService;
	}
	public void setRomaneioorigemService(
			RomaneioorigemService romaneioorigemService) {
		this.romaneioorigemService = romaneioorigemService;
	}
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {
		this.requisicaomaterialService = requisicaomaterialService;
	}
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	/**
	 * M�todo chamado pelo jsp listagem toma decis�o de qual a��o tomar.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#findRequisicoes(String)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#autorizar(List, String, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#cancelar(List, String, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#estornar(List, String, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#findRequisicoesParaBaixa(String)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#baixar(List, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#gerarCompra(List, org.springframework.validation.BindException)
	 * @see #preparaBaixa(List, WebRequestContext)
	 * @see #salvaCompras(List, WebRequestContext)
	 * @see #getControllerModelAndView(WebRequestContext)
	 * 
	 * @param request
	 * @param acao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView acao(WebRequestContext request, Requisicaomaterial acao){
		if (acao == null || acao.getCdrequisicaomaterial() == null)
			throw new SinedException("Requisi��o de material n�o pode ser nulo.");
		
		String whereIn = request.getParameter("selectedItens");
		
		List<Requisicaomaterial> requisicoes = null;
		if(!acao.getCdrequisicaomaterial().equals(EM_PROCESSO_COMPRA.getCodigo()) && !acao.getCdrequisicaomaterial().equals(BAIXADA.getCodigo()))
			requisicoes = requisicaomaterialService.findRequisicoes(whereIn);
		
		if(acao.getCdrequisicaomaterial().equals(AUTORIZADA.getCodigo())){
			if(requisicaomaterialService.autorizar(requisicoes, whereIn, request.getBindException())){
				request.addMessage("Registro(s) autorizado(s) com sucesso.", MessageType.INFO);
			}
		} else if(acao.getCdrequisicaomaterial().equals(CANCELADA.getCodigo())){
			if(requisicaomaterialService.cancelar(requisicoes, whereIn, request.getBindException())){
				request.addMessage("Registro(s) cancelado(s) com sucesso.", MessageType.INFO);
			}
		} else if(acao.getCdrequisicaomaterial().equals(ESTORNAR.getCodigo())){
			if(requisicaomaterialService.estornar(requisicoes, whereIn, request.getBindException())){
				cancelarRomaneioVinculado(whereIn);
				cancelarMovimentacaoVinculo(whereIn);
				request.addMessage("Registro(s) estornado(s) com sucesso.", MessageType.INFO);
			}
		} else if(acao.getCdrequisicaomaterial().equals(BAIXADA.getCodigo())){
			List<Requisicaomaterial> requisicoes2 = requisicaomaterialService.findRequisicoesParaBaixa(whereIn);
			if(requisicaomaterialService.baixar(requisicoes2, request.getBindException())){
				return preparaBaixa(requisicoes2, request);
			}
		}  else if(acao.getCdrequisicaomaterial().equals(EM_PROCESSO_COMPRA.getCodigo())){
			List<Requisicaomaterial> requisicoesGerarCompra = requisicaomaterialService.findRequisicoesParaGerarCompra(whereIn);
			StringBuilder codigosSC = new StringBuilder();
			if(requisicaomaterialService.gerarCompra(requisicoesGerarCompra, request.getBindException())){
				for(Requisicaomaterial requisicaomaterial : requisicoesGerarCompra){
					List<Requisicaomaterial> requisicoesGerarCompraSalvar = requisicaomaterialService.findRequisicoesParaGerarCompra(requisicaomaterial.getCdrequisicaomaterial().toString());
					if(requisicaomaterialService.gerarCompra(requisicoesGerarCompraSalvar, request.getBindException())){
						String codigoSC = salvaCompras(requisicoesGerarCompraSalvar, request);
						if(StringUtils.isNotBlank(codigoSC)){
							codigosSC.append(codigoSC).append(",");
						}
					}
				}
				if(codigosSC.length() > 0){
					request.addMessage("Solicita��o(�es) de compra "+codigosSC.substring(0, codigosSC.length()-1)+" gerada(s) com sucesso.", MessageType.INFO);
				}
			}
		}  else if(acao.getCdrequisicaomaterial().equals(ROMANEIO_GERADO.getCodigo())){
			List<Requisicaomaterial> requisicoes2 = requisicaomaterialService.findRequisicoesParaBaixa(whereIn);
			if(requisicaomaterialService.gerarRomaneio(requisicoes2, request.getBindException())){
				return preparaRomaneio(request, requisicoes2, whereIn);
			} else {
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		if(request.getBindException().hasErrors()){
			request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
		}
		return getControllerModelAndView(request);
	}
	
	/**
	 * M�todo que cancela os romaneio vinculados.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 23/04/2014
	 */
	@SuppressWarnings("unchecked")
	private void cancelarRomaneioVinculado(String whereIn) {
		List<Romaneioorigem> lista = romaneioorigemService.findByRequisicaomaterial(whereIn);
		if(lista != null && lista.size() > 0){
			romaneioService.cancelaRomaneios((List<Romaneio>) CollectionsUtil.getListProperty(lista, "romaneio"));
		}
	}
	
	/**
	* M�todo que cancela as movimenta��es de estoque da requisi��o de material
	*
	* @param whereIn
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	private void cancelarMovimentacaoVinculo(String whereIn) {
		List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByRequisicaomaterial(whereIn, true);
		if(SinedUtil.isListNotEmpty(listaMovimentacaoestoque)){
			movimentacaoestoqueService.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ","));
		}
	}
	
	/**
	 * M�todo que gera as solicita��es de compra e cria msg para informar ao usu�rio
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#preparaSolicitacoes(List)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#saveSolicitacoes(requisicoes)
	 * @param requisicoesGerarCompra
	 * @param request
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	private String salvaCompras(List<Requisicaomaterial> requisicoesGerarCompra, WebRequestContext request) {
		String codigos = null;
		//Salva solicita��es
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.preparaSolicitacoes(request, requisicoesGerarCompra);
		if(SinedUtil.isListNotEmpty(solicitacoes)){
			if("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEARCOMPRAPROJETO))){
				for (Solicitacaocompra solicitacaocompra : solicitacoes){ 
					solicitacaocompraService.validaQtdeCompraMaiorQuePlanejamento(solicitacaocompra, request.getBindException());
				}
			}
			
			if(!request.getBindException().hasErrors()){
				codigos = solicitacaocompraService.saveSolicitacoes(solicitacoes);
			
				if(codigos != null && !"".equals(codigos)){
					Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.REQUISICAO_MATERIAL_CRIADA);
					if(motivoAviso != null && Boolean.TRUE.equals(motivoAviso.getEmail())){
						for (String selectedItem : codigos.split(",")) {
							try {
								Aviso aviso = null;
								List<Aviso> avisoList = new ArrayList<Aviso>();
								
								aviso = new Aviso("Requisi��o de material criada", "C�digo da requisi��o: " + selectedItem, 
										motivoAviso.getTipoaviso(), motivoAviso.getPapel(), motivoAviso.getAvisoorigem(), Integer.parseInt(selectedItem), 
										empresaService.loadPrincipal(), SinedDateUtils.currentDate(), motivoAviso);
								
								avisoList.add(aviso);
								
								avisoService.salvarAvisos(avisoList , true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		
		if(request.getAttribute("listaRequisicaomaterialQtdeZerada") != null){
			List<Requisicaomaterial> listaRequisicaomaterialQtdeZerada = (List<Requisicaomaterial>) request.getAttribute("listaRequisicaomaterialQtdeZerada");
			if(SinedUtil.isListNotEmpty(listaRequisicaomaterialQtdeZerada)){
				StringBuilder msgQtdeZerada = new StringBuilder("N�o � permitido gerar solicita��o de compra para requisi��es com quantidade restante menor ou igual a zero.<br>");
				for(Requisicaomaterial requisicaomaterial : listaRequisicaomaterialQtdeZerada){
					msgQtdeZerada.append("Requisi��o: " + requisicaomaterial.getIdentificadorOuCdrequisicaomaterial() + " - " + 
							requisicaomaterial.getDescricao()).append(",");
				}
				request.addError(msgQtdeZerada.substring(0, msgQtdeZerada.length()-1));
			}
			
		}
		return codigos;
	}
	
	/**
	 * Retorna para o controller do USC
	 * 
	 * @param request
	 * @return
	 * @author Tom�s Rabelo
	 */
	private ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			if(!action.contains("cdrequisicaomaterial")){
				action += "&cdrequisicaomaterial="+request.getParameter("selectedItens");
			}
		}
		return new ModelAndView("redirect:"+action);
	}
	
	/*===============================================================================================
	 *=============================================================================================== 
	 *================================  BAIXA REQUISI��ES ===========================================			
	 *===============================================================================================		
	 *===============================================================================================*/
	
	/**
	 * Prepara a baixa das requisi��es com situa��o em processo de compra ou baixa parcial
	 * Separa as requisi��es em Produto/EPI/Servi�o e Patrim�nio. E retorna a p�gina de baixa.
	 * 
	 * @param requisicoes
	 * @return
	 * @author Tom�s Rabelo
	 * @param request 
	 */
	private ModelAndView preparaBaixa(List<Requisicaomaterial> requisicoes, WebRequestContext request) {
		BaixarequisicaomaterialBean bean = requisicaomaterialService.preparaBaixaSeparandoRequisicao(requisicoes);
		
		String whereIn = request.getParameter("selectedItens");
		if(Boolean.TRUE.equals(bean.getApenasBaixa())){
			requisicaomaterialService.doUpdateRequisicoes(whereIn, Situacaorequisicao.BAIXADA, false);
			return new ModelAndView("redirect:/suprimento/crud/Requisicaomaterial");
		}
		
		String controller = request.getParameter("controller");
		if(controller.contains("consultar")){
			controller += "&cdrequisicaomaterial="+request.getParameter("selectedItens");
		}
		bean.setController(controller);
		
		setAttributeRequestBaixa(request, bean);
		return new ModelAndView("process/baixarequisicaomaterial", "baixarequisicaomaterialBean", bean);
	}
	
	/**
	* M�todo que seta no atributo da requisi��o o mapa de lote
	*
	* @param request
	* @param bean
	* @since 05/10/2016
	* @author Luiz Fernando
	*/
	private void setAttributeRequestBaixa(WebRequestContext request, BaixarequisicaomaterialBean bean) {
		HashMap<Integer, List<Loteestoque>> mapLoteestoque = new HashMap<Integer, List<Loteestoque>>();
		if(bean != null && SinedUtil.isListNotEmpty(bean.getListaProdutoEpiServico())){
			for(Requisicaomaterial requisicaomaterial : bean.getListaProdutoEpiServico()){
				mapLoteestoque.put(requisicaomaterial.getCdrequisicaomaterial(), 
						loteestoqueService.findQtdeLoteestoque(requisicaomaterial.getMaterial(), requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getEmpresa()));
			}
		}
		request.setAttribute("mapLoteestoque", mapLoteestoque);
	}
	
	/**
	* M�todo ajax que carrega os lotes
	*
	* @param request
	* @param bean
	* @return
	* @since 15/09/2016
	* @author Luiz Fernando
	*/
	public ModelAndView carregarLoteestoque(WebRequestContext request, RomaneioGerenciamentoMaterial bean){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(bean.getLocalarmazenagemorigem() != null){
			Localarmazenagem localarmazenagem = localarmazenagemService.load(bean.getLocalarmazenagemorigem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo");
			boolean considerarQtdeDisponivel = localarmazenagem == null || localarmazenagem.getPermitirestoquenegativo() == null || !localarmazenagem.getPermitirestoquenegativo(); 
			List<RomaneioGerenciamentoMaterialItem> listaMateriais = bean.getListaMateriais();
			for (RomaneioGerenciamentoMaterialItem it : listaMateriais) {
				it.setListaLoteestoqueVO(loteestoqueService.createLoteestoqueVO(loteestoqueService.findQtdeLoteestoque(it.getMaterial(), bean.getLocalarmazenagemorigem(), bean.getEmpresaorigem(), considerarQtdeDisponivel)));
			}
			jsonModelAndView.addObject("listaMateriais", bean.getListaMateriais());
		}
		return jsonModelAndView;
	}
	
	/**
	 * Redireciona a tela para a tela de Romaneio.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @param request 
	 * @param requisicoes2 
	 * @since 17/04/2014
	 */
	private ModelAndView preparaRomaneio(WebRequestContext request, List<Requisicaomaterial> requisicoes, String whereIn) {
		List<GerarRomaneioBean> lista = requisicaomaterialService.preparaRomaneioList(request, requisicoes, whereIn);
		request.setAttribute("fromRequisicaomaterial", Boolean.TRUE);
		return romaneioService.abrirTelaGerarRomaneio(request, lista, null, false, false, whereIn);
	}
	
	public void saveRomaneio(WebRequestContext request, RomaneioGerenciamentoMaterial form){
		Projeto projetoRomaneio = null;
		for (RomaneioGerenciamentoMaterialItem item : form.getListaMateriais()) {
			if(item.getQtdedestino() <= 0d) continue;
			if(item.getProjetodestino() != null){
				projetoRomaneio = item.getProjetodestino();
				break;
			}
		}
		for (RomaneioGerenciamentoMaterialItem item : form.getListaMateriais()) {
			if(item.getQtdedestino() <= 0d) continue;
			
			// CRIA��O DO ROMANEIO
			Romaneio romaneio = new Romaneio();
			romaneio.setDtromaneio(SinedDateUtils.currentDate());
			romaneio.setDescricao("Romaneio gerado a partir da requisi��o de material " + item.getRequisicaomaterial().getCdrequisicaomaterial());
			romaneio.setEmpresa(form.getEmpresaorigem());
			romaneio.setLocalarmazenagemdestino(item.getLocalarmazenagemdestino());
			romaneio.setLocalarmazenagemorigem(form.getLocalarmazenagemorigem());
			romaneio.setRomaneiosituacao(Romaneiosituacao.BAIXADA);
			romaneio.setDtromaneio(form.getDtromaneio());
			romaneio.setProjeto(projetoRomaneio);
			romaneio.setConsiderarProjetoorigem(true);
			
			romaneioService.saveOrUpdate(romaneio);
			
			// CRIA��O DA ORIGEM DO ROMANEIO
			Romaneioorigem romaneioorigem = new Romaneioorigem();
			romaneioorigem.setRomaneio(romaneio);
			romaneioorigem.setRequisicaomaterial(item.getRequisicaomaterial());
			
			romaneioorigemService.saveOrUpdate(romaneioorigem);
			
			// CRIA��O DO ITEM ROMANEIO
			Romaneioitem romaneioitem = new Romaneioitem();
			romaneioitem.setMaterial(item.getMaterial());
			romaneioitem.setQtde(item.getQtdedestino());
			romaneioitem.setMaterialclasse(item.getMaterialclasse());
			romaneioitem.setRomaneio(romaneio);
			romaneioitem.setLoteestoque(item.getLoteestoque());
			romaneioitem.setPatrimonioitem(item.getPatrimonioitem());
			
			romaneioitemService.saveOrUpdate(romaneioitem);
			
			// GERA A ENTRADA E SA�DA DO ROMANEIO
			romaneioService.gerarEntradaSaidaRomaneio(romaneio, romaneioitem, item.getValor(), form.getEmpresaorigem(), item.getProjetodestino(), form.getObservacao(), form.getEmpresaorigem(), form.getProjetoorigem(), false, null, item.getLoteestoque(), item.getCentrocusto(), item.getRequisicaomaterial());
			
			Situacaorequisicao situacao = Situacaorequisicao.ROMANEIO_GERADO;
			if(item.getQtdedestino() >= item.getQtderestante()){
				situacao = Situacaorequisicao.ROMANEIO_COMPLETO;
			} 
			if((item.getQtdedestino() < item.getQtderestante() || item.getQtdedestino() >= item.getQtderestante()) && 
					item.getRequisicaomaterial().getAux_requisicaomaterial() != null &&
					Situacaorequisicao.EM_ABERTO.equals(item.getRequisicaomaterial().getAux_requisicaomaterial().getSituacaorequisicao())){
				requisicaomaterialService.doUpdateRequisicoes(item.getRequisicaomaterial().getCdrequisicaomaterial().toString(), Situacaorequisicao.AUTORIZADA);
			}
			
			// ATUALIZA A SITUA��O DA REQUISI��O
			requisicaomaterialService.doUpdateRequisicoes(item.getRequisicaomaterial().getCdrequisicaomaterial().toString(), situacao);
		}
		
		request.addMessage("Romaneio(s) gerado(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * Monta a Sa�da no caso de Produto/EPI/Servi�o e a movimenta��o no caso de Patrim�nio.
	 * Depois salva ambas. 
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#preparaMovimentacaoEstoque(List, StringBuilder)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#criaListaPlaquetasDigitadas(List, StringBuilder)
	 * @see #verificaPlaquetasIguais(WebRequestContext, List)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#montaMovimenta��esParaSalvar(List)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#saveBaixa(List, List, String)
	 * @param request
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView baixaRequisicoes(WebRequestContext request, BaixarequisicaomaterialBean bean){
		List<Requisicaomaterial> listaRequisicaomaterial = requisicaomaterialService.findRequisicoesParaBaixa(bean.getCdrequisicoes());
		boolean possuiBaixada = false;
		
		for (Requisicaomaterial requisicaomaterial: listaRequisicaomaterial){
			if (requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA)){
				possuiBaixada = true;
				break;
			}
			for(Requisicaomaterial rm: bean.getListaProdutoEpiServico()){
				if(rm.equals(requisicaomaterial)){
					requisicaomaterial.setLoteestoque(rm.getLoteestoque());
					break;
				}
			}
			if(!MaterialService.getInstance().validateObrigarLote(requisicaomaterial, request)){
				setAttributeRequestBaixa(request, bean);
				return new ModelAndView("process/baixarequisicaomaterial", "baixarequisicaomaterialBean", bean);
			}
		}
		
		if (possuiBaixada){
			request.addError("Requisi��o j� est� baixada.");
			setAttributeRequestBaixa(request, bean);
			return new ModelAndView("process/baixarequisicaomaterial", "baixarequisicaomaterialBean", bean);
		}
		
		StringBuilder cdrequisicoes = new StringBuilder();
		
		//Prepara produto/Epi/Servi�o
		List<Movimentacaoestoque> movimentacoesProdutoEpiServico = null;
		if(bean.getListaProdutoEpiServico() != null && bean.getListaProdutoEpiServico().size() > 0)
			movimentacoesProdutoEpiServico = requisicaomaterialService.preparaMovimentacaoEstoque(bean.getListaProdutoEpiServico(), cdrequisicoes);
		
		//Prepara produto/Epi/Servi�o Grade
		List<Movimentacaoestoque> movimentacoesProdutoEpiServicoGrade = null;
		if(bean.getListaProdutoEpiServicoGrade() != null && bean.getListaProdutoEpiServicoGrade().size() > 0)
			movimentacoesProdutoEpiServicoGrade = requisicaomaterialService.preparaMovimentacaoEstoque(bean.getListaProdutoEpiServicoGrade(), cdrequisicoes);
		
		
		//Prepara patrim�nio
		List<Movpatrimonio> movimentacoesPatrimonio = null;
		List<Requisicaomaterial> patrimoniosComPlaquetas = null;
		if(bean.getListaPatrimonio() != null && bean.getListaPatrimonio().size() > 0){
			//Cria uma lista so com as plaquetas que foram digitadas o restante descarta
			patrimoniosComPlaquetas = requisicaomaterialService.criaListaPlaquetasDigitadas(bean.getListaPatrimonio(), cdrequisicoes);
			
			if(verificaPlaquetasIguais(request, patrimoniosComPlaquetas))
				return new ModelAndView("process/baixarequisicaomaterial", "baixarequisicaomaterialBean", bean);
			
			if(patrimoniosComPlaquetas != null && patrimoniosComPlaquetas.size() > 0)
				movimentacoesPatrimonio = requisicaomaterialService.montaMovimenta��esParaSalvar(patrimoniosComPlaquetas);
		}
		
		if(cdrequisicoes != null && cdrequisicoes.length() > 0){
			cdrequisicoes.delete(cdrequisicoes.length()-1, cdrequisicoes.length());
			request.addMessage("Registro(s) baixado(s) com sucesso.", MessageType.INFO);
		}
		
		if(cdrequisicoes != null && cdrequisicoes.toString().equals("")){
			request.addError("Nenhum registro foi baixado.");
			setAttributeRequestBaixa(request, bean);
			return new ModelAndView("process/baixarequisicaomaterial", "baixarequisicaomaterialBean", bean);
		} else{
			requisicaomaterialService.saveBaixa(movimentacoesProdutoEpiServico, movimentacoesProdutoEpiServicoGrade, movimentacoesPatrimonio, cdrequisicoes != null ? cdrequisicoes.toString() : null);
			String requisicoesApenasbaixa = bean.getCdrequisicoes();
			requisicoesApenasbaixa = requisicoesApenasbaixa.replace(cdrequisicoes.toString() + ",", "");
			
			
			
			requisicaomaterialService.doUpdateRequisicoes(requisicoesApenasbaixa, Situacaorequisicao.BAIXADA, false);
			
		}
		return getControllerModelAndView(request);
	}
	
	/**
	 * Verifica se foram digitadas plaquetas iguais
	 * 
	 * @param request
	 * @param list
	 * @return
	 * @author Tom�s Rabelo
	 */
	private boolean verificaPlaquetasIguais(WebRequestContext request, List<Requisicaomaterial> list) {
		for (int i = 0; i < list.size(); i++) {
			int qtd = 0;
			for (int j = 0; j < list.size(); j++) {
				if(list.get(i).getPlaqueta().equals(list.get(j).getPlaqueta())){
					qtd++;
					if(qtd > 1){
						boolean existeMsg = false;
						if(request.getMessages().length > 0)
							for (Message message : request.getMessages()) 
								if(((String)message.getSource()).contains(list.get(i).getPlaqueta())){
									existeMsg = true;
									break;
								}
						if(!existeMsg)
							request.addMessage("A plaqueta "+list.get(i).getPlaqueta()+" n�o pode ser utilizada mais de uma vez.", MessageType.ERROR);
						break;
					}
				}
			}
		}
		
		if(request.getMessages().length > 0)
			return true;
		
		return false;
	}

	/**
	 * Ajax que verifica se a plaqueta do patrim�nio � v�lida
	 * 
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#isPlaquetaValida(Patrimonioitem)
	 * @param request
	 * @param patrimonioitem
	 * @author Tom�s Rabelo
	 */
	public void isPlaquetaValida(WebRequestContext request, Patrimonioitem patrimonioitem){
		Boolean result = patrimonioitemService.isPlaquetaValida(patrimonioitem);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(result == null ? Boolean.FALSE.toString() : result.toString());
	}

}
