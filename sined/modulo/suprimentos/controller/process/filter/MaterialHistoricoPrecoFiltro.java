package br.com.linkcom.sined.modulo.suprimentos.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public class MaterialHistoricoPrecoFiltro {

	private Date dtVendaIni;
	private Date dtVendaFim;
	private Integer cdVenda;
	private Material material;
	private Double quantidade;
	private Pedidovendatipo pedidoVendaTipo;
	private Cliente cliente;
	private Empresa empresa;
	private Unidademedida unidade;
	protected Boolean ativo = Boolean.TRUE;
	
	public Date getDtVendaIni() {
		return dtVendaIni;
	}
	public Date getDtVendaFim() {
		return dtVendaFim;
	}
	public Integer getCdVenda() {
		return cdVenda;
	}
	public Material getMaterial() {
		return material;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Pedidovendatipo getPedidoVendaTipo() {
		return pedidoVendaTipo;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Unidademedida getUnidade() {
		return unidade;
	}
	
	@DisplayName("Situa��o")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setDtVendaIni(Date dtVendaIni) {
		this.dtVendaIni = dtVendaIni;
	}
	public void setDtVendaFim(Date dtVendaFim) {
		this.dtVendaFim = dtVendaFim;
	}
	public void setCdVenda(Integer cdVenda) {
		this.cdVenda = cdVenda;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setPedidoVendaTipo(Pedidovendatipo pedidoVendaTipo) {
		this.pedidoVendaTipo = pedidoVendaTipo;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setUnidade(Unidademedida unidade) {
		this.unidade = unidade;
	}
}
