package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.service.CotacaofornecedorService;

@Controller(
		path="/suprimento/process/CotacaofornecedorExcel",
		authorizationModule=ProcessAuthorizationModule.class
)
public class CotacaofornecedorExcelProcess extends ResourceSenderController<Cotacaofornecedor>{

	private CotacaofornecedorService cotacaofornecedorService;
	
	public void setCotacaofornecedorService(
			CotacaofornecedorService cotacaofornecedorService) {
		this.cotacaofornecedorService = cotacaofornecedorService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	Cotacaofornecedor cotacaofornecedor) throws Exception {
		return cotacaofornecedorService.preparaArquivoCotacaoExcelFormatado(cotacaofornecedor);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Cotacaofornecedor cotacaofornecedor) throws Exception {
		return null;
	}
	
}
