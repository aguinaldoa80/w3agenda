package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendainteracaohistorico;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grupoquestionario;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.Questionariogrupo;
import br.com.linkcom.sined.geral.bean.Questionarioquestao;
import br.com.linkcom.sined.geral.bean.Questionarioresposta;
import br.com.linkcom.sined.geral.bean.Tipopessoaquestionario;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.TipoRespostaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoagendainteracao;
import br.com.linkcom.sined.geral.service.AgendainteracaoService;
import br.com.linkcom.sined.geral.service.AgendainteracaohistoricoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ClientehistoricoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.GrupoquestionarioService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.PessoaquestionarioService;
import br.com.linkcom.sined.geral.service.QuestionarioService;
import br.com.linkcom.sined.geral.service.QuestionarioquestaoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionarioquestao;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path={"/suprimento/process/popup/Pessoaquestionario"})
public class PessoaquestionarioProcess extends MultiActionController{

	private FornecedorService fornecedorService;
	private QuestionarioService questionarioService;
	private QuestionarioquestaoService questionarioquestaoService;
	private PessoaquestionarioService pessoaquestionarioService;
	private ClienteService clienteService;
	private OrdemcompraService ordemcompraService;
	private GrupoquestionarioService grupoquestionarioService;
	private ClientehistoricoService clientehistoricoService;
	private AgendainteracaohistoricoService agendainteracaohistoricoService;
	private AgendainteracaoService agendainteracaoService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setQuestionarioService(QuestionarioService questionarioService) {
		this.questionarioService = questionarioService;
	}
	public void setQuestionarioquestaoService(
			QuestionarioquestaoService questionarioquestaoService) {
		this.questionarioquestaoService = questionarioquestaoService;
	}
	public void setPessoaquestionarioService(
			PessoaquestionarioService pessoaquestionarioService) {
		this.pessoaquestionarioService = pessoaquestionarioService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setGrupoquestionarioService(GrupoquestionarioService grupoquestionarioService) {
		this.grupoquestionarioService = grupoquestionarioService;
	}
	public void setClientehistoricoService(
			ClientehistoricoService clientehistoricoService) {
		this.clientehistoricoService = clientehistoricoService;
	}
	public void setAgendainteracaohistoricoService(
			AgendainteracaohistoricoService agendainteracaohistoricoService) {
		this.agendainteracaohistoricoService = agendainteracaohistoricoService;
	}
	public void setAgendainteracaoService(
			AgendainteracaoService agendainteracaoService) {
		this.agendainteracaoService = agendainteracaoService;
	}
	
	/**
	 * 
	 * M�todo que faz o carregamento das perguntas de avalia��o para cada fornecedor
	 *
	 *@author Thiago Augusto
	 *@date 06/03/2012
	 * @param request
	 * @param bean
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ModelAndView avaliarFornecedor(WebRequestContext request, Fornecedor bean){

		Boolean origementrega = Boolean.valueOf(request.getParameter("origementrega") != null ? request.getParameter("origementrega") : "false");
		Boolean origemOrdemCompra = Boolean.valueOf(request.getParameter("origemOrdemCompra") != null ? request.getParameter("origemOrdemCompra") : "false");
		String identificadortela = request.getParameter("identificadortela");
		String indexentregadocumento = request.getParameter("indexentregadocumento");
		Fornecedor fornecedor = new Fornecedor();
		
		if(origementrega != null && origementrega){
			Object attribute = request.getSession().getAttribute("listaEntregadocumento" + identificadortela);
			if(attribute != null){
				List<Entregadocumento> listaEntregadocumento = (List<Entregadocumento>)attribute;
				if (indexentregadocumento != null && !"".equals(indexentregadocumento) && listaEntregadocumento.get(Integer.parseInt(indexentregadocumento)) != null) {
					Fornecedor aux_fornecedor = listaEntregadocumento.get(Integer.parseInt(indexentregadocumento)).getFornecedor();
					fornecedor.setCdpessoa(aux_fornecedor.getCdpessoa());
				}
			}
		} else {
			fornecedor = bean;
		}
		
		Pessoaquestionario pessoaquestionario = new Pessoaquestionario();
		pessoaquestionario.setPessoa(fornecedorService.carregaFornecedor(fornecedor));
		
		request.setAttribute("origementrega", origementrega);
		request.setAttribute("origemOrdemCompra", origemOrdemCompra);
		request.setAttribute("identificadortela", identificadortela);
		request.setAttribute("indexentregadocumento", indexentregadocumento);
		if(origemOrdemCompra != null && origemOrdemCompra){
			String cdOrdemCompra = request.getParameter("cdOrdemCompra");
			request.setAttribute("cdOrdemCompra", cdOrdemCompra);
		}

		if (origemOrdemCompra != null && origemOrdemCompra) {
			pessoaquestionario.setQuestionario(questionarioService.carregaQuestionarioPorTipoPessoaForOrdemCompra(Tipopessoaquestionario.FORNECEDOR));
		} else {
			pessoaquestionario.setQuestionario(questionarioService.carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario.FORNECEDOR));
		}
		
		if (pessoaquestionario != null && pessoaquestionario.getQuestionario() != null && pessoaquestionario.getQuestionario().getDefinirgrupo() != null && pessoaquestionario.getQuestionario().getDefinirgrupo()) {
			List<Grupoquestionario> listaGrupoquestionario = new ArrayList<Grupoquestionario>();
			if (SinedUtil.isListNotEmpty(pessoaquestionario.getQuestionario().getListaQuestionariogrupo())) {
				for (Questionariogrupo questionariogrupo : pessoaquestionario.getQuestionario().getListaQuestionariogrupo()) {
					if (questionariogrupo.getGrupoquestionario() != null && SinedUtil.isListNotEmpty(questionariogrupo.getGrupoquestionario().getListaquestionarioquestao())) {
						listaGrupoquestionario.add(questionariogrupo.getGrupoquestionario());
					}
				}
			}
			pessoaquestionario.setListaGrupoquestionario(listaGrupoquestionario);
			return new ModelAndView("direct:/process/popup/pessoaquestionariogrupo", "pessoaquestionario", pessoaquestionario);
		} else {
			List<Pessoaquestionarioquestao> listaPessoaQuestionarioQuestao = new ArrayList<Pessoaquestionarioquestao>();
			Pessoaquestionarioquestao pessoaQuestionarioQuestao;
			if (pessoaquestionario.getQuestionario() != null && pessoaquestionario.getQuestionario().getListaquestionarioquestao() != null){
				for (Questionarioquestao qq : pessoaquestionario.getQuestionario().getListaquestionarioquestao()) {
					pessoaQuestionarioQuestao = new Pessoaquestionarioquestao();
					pessoaQuestionarioQuestao.setQuestionarioquestao(qq);
					listaPessoaQuestionarioQuestao.add(pessoaQuestionarioQuestao);
				}
			}
			request.setAttribute("identificadortela", identificadortela);
			pessoaquestionario.setListaPessoaQuestionarioQuestao(listaPessoaQuestionarioQuestao);
			return new ModelAndView("direct:/process/popup/pessoaquestionario", "pessoaquestionario", pessoaquestionario);
		}
	
	}
	
	/**
	 * 
	 * M�todo que salva a avalia��o de cada fornecedor.
	 *
	 *@author Thiago Augusto
	 *@date 06/03/2012
	 * @param request
	 * @param bean
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public void saveAvaliacao(WebRequestContext request, Pessoaquestionario bean){
		Boolean origementrega = Boolean.valueOf(request.getParameter("origementrega") != null ? request.getParameter("origementrega") : "false");
		Boolean origemOrdemCompra = Boolean.valueOf(request.getParameter("origemOrdemCompra") != null ? request.getParameter("origemOrdemCompra") : "false");
		boolean origemAgendainteracaoByCliente = Boolean.TRUE.toString().equalsIgnoreCase(request.getParameter("origemAgendainteracaoByCliente"));
		boolean origemAgendainteracaoByInteracao = Boolean.TRUE.toString().equalsIgnoreCase(request.getParameter("origemAgendainteracaoByInteracao"));
		boolean agendarProximaInteracao = Boolean.TRUE.toString().equalsIgnoreCase(request.getParameter("agendarProximaInteracao"));
		
		if (bean.getQuestionario() != null && bean.getQuestionario().getDefinirgrupo() != null && bean.getQuestionario().getDefinirgrupo()) {
			preencheListaPessoaQuestinarioQuestao(bean);
		} 
		
		if(SinedUtil.isListNotEmpty(bean.getListaPessoaQuestionarioQuestao())){
			avaliaQuestionario(bean);
			
			if((origemAgendainteracaoByCliente || origemAgendainteracaoByInteracao) && bean.getCdagendainteracao() != null){
				Agendainteracao agendainteracao = new Agendainteracao(bean.getCdagendainteracao());
				bean.setAgendainteracao(agendainteracao);
			}
			if((origementrega == null || !origementrega) && (origemOrdemCompra == null || !origemOrdemCompra)){
				pessoaquestionarioService.saveOrUpdate(bean);
				request.addMessage("Avalia��o feita com sucesso!");
			}
		}
		
		if(origementrega != null && origementrega){
			String identificadortela = request.getParameter("identificadortela");
			String indexstr = request.getParameter("indexentregadocumento");
			String script = "<script>";
			if (indexstr != null && !"".equals(indexstr)) {
				Object attribute = request.getSession().getAttribute("listaEntregadocumento" + identificadortela);
				List<Entregadocumento> listaEntregadocumento = null;
				listaEntregadocumento = (List<Entregadocumento>) attribute;
				Integer index = Integer.parseInt(indexstr);
				Fornecedor fornecedor = listaEntregadocumento.get(index).getFornecedor();
				if(fornecedor != null){
					fornecedor.setPessoaquestionariotrans(bean);	
					listaEntregadocumento.get(index).setAvaliado(true);
					listaEntregadocumento.get(index).setFornecedorAvaliado(true);
					listaEntregadocumento.get(index).setFornecedor(fornecedor);
					request.getSession().setAttribute("listaEntregadocumento" + identificadortela, listaEntregadocumento);
					request.getServletResponse().setContentType("text/html");
					script += "window.parent.avaliacaoCriada('" + index + "');" ;
				}
			}
			script += " parent.$.akModalRemove();</script>";
			View.getCurrent().println(script);
		}else if(origemOrdemCompra){
			Ordemcompra ordemcompra = new Ordemcompra();
			ordemcompra.setCdordemcompra(request.getParameter("cdOrdemCompra") != null ? Integer.parseInt(request.getParameter("cdOrdemCompra")) : null);
			if (ordemcompra != null && ordemcompra.getCdordemcompra() != null) {
				
				Ordemcompra ordemcompraAvaliacao = ordemcompraService.findOrdemcompraAvaliacao(ordemcompra); 
				
				if (ordemcompraAvaliacao != null && ordemcompraAvaliacao.getCdordemcompra() != null && ordemcompraAvaliacao.getPessoaquestionario() != null && ordemcompraAvaliacao.getPessoaquestionario().getCdpessoaquestionario() != null) {
					request.addError("J� existe uma avalia��o para esta Ordem de Compra.");
				} else {
					ordemcompra.setPessoaquestionario(bean);
					pessoaquestionarioService.saveOrUpdate(bean);
					ordemcompraService.vinculaPessoaQuestionarioAOrdemCompra(ordemcompra);
					request.addMessage("Avalia��o feita com sucesso!");
				}
			} 
		}
		if(bean.getPessoa() instanceof Cliente){
			Clientehistorico clientehistorico = new Clientehistorico();
			
			clientehistorico.setCliente((Cliente)bean.getPessoa());
			clientehistorico.setObservacao("Avalia��o realizada.");
			Agendainteracao agendainteracao = null;
			if((origemAgendainteracaoByCliente || origemAgendainteracaoByInteracao) && bean.getCdagendainteracao()!=null){
				agendainteracao = agendainteracaoService.load(new Agendainteracao(bean.getCdagendainteracao()));
				clientehistorico.setAtividadetipo(agendainteracao.getAtividadetipo());
				clientehistorico.setEmpresahistorico(agendainteracao.getEmpresahistorico());
			}
			
			clientehistoricoService.saveOrUpdate(clientehistorico);
			
			if(agendainteracao!=null){
				String observacao = "";
				if(origemAgendainteracaoByCliente){
					observacao = "Avalia��o de cliente realizada.";
				}else if(origemAgendainteracaoByInteracao){
					observacao = "Avalia��o do servi�o realizada.";
				}
				Agendainteracaohistorico agendainteracaohistorico = new Agendainteracaohistorico();
				agendainteracaohistorico.setAgendainteracao(agendainteracao);
				agendainteracaohistorico.setObservacao(observacao);
				agendainteracaohistoricoService.saveOrUpdate(agendainteracaohistorico);
				if(agendarProximaInteracao){
					Date dtproximainteracao = SinedDateUtils.incrementDateFrequencia(
							new java.sql.Date(System.currentTimeMillis()), 
							agendainteracao.getPeriodicidade(),(agendainteracao.getQtdefrequencia() != null && agendainteracao.getQtdefrequencia() > 0 ? agendainteracao.getQtdefrequencia() : 1));
					agendainteracaoService.updateDataProxInteracao(agendainteracao.getCdagendainteracao(), dtproximainteracao);
				}
			}
		}
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * 
	 * M�todo respons�vel por realizar a avalia��o do questin�rio.
	 *
	 * @author Jo�o Vitor
	 * @date 18/03/2015
	 * @param bean
	 * @return 
	 */
	private void avaliaQuestionario(Pessoaquestionario bean) {
		List<Questionarioquestao> listQuestionarioquestao = new ArrayList<Questionarioquestao>();
		Map<Integer, Questionarioquestao> mapQuestionarioquestao = new HashMap<Integer, Questionarioquestao>();
		
		Map<Integer, Grupoquestionario> mapGrupoquestionario = new HashMap<Integer, Grupoquestionario>();
		
		for (Pessoaquestionarioquestao pessoaquestionarioquestao : bean.getListaPessoaQuestionarioQuestao()) {
			if (pessoaquestionarioquestao.getQuestionarioquestao() != null) {
				listQuestionarioquestao.add(pessoaquestionarioquestao.getQuestionarioquestao());
			}
		}
		
		if (SinedUtil.isListNotEmpty(bean.getListaGrupoquestionario())) {
			String whereIn = SinedUtil.listAndConcatenate(bean.getListaGrupoquestionario(), "cdgrupoquestionario", ",");
			List<Grupoquestionario> listGrupoquestionario = grupoquestionarioService.carregaGrupoquestionarioForCalculoMediaAprovacao(whereIn);
			if (SinedUtil.isListNotEmpty(listGrupoquestionario)) {
				for (Grupoquestionario grupoquestionario : listGrupoquestionario) {
					mapGrupoquestionario.put(grupoquestionario.getCdgrupoquestionario(), grupoquestionario);
				}
			}
		}

		String whereIn = SinedUtil.listAndConcatenate(listQuestionarioquestao, "cdquestionarioquestao", ",");
		listQuestionarioquestao = questionarioquestaoService.carregarListaQuestionarioQuestao(whereIn);
		if (SinedUtil.isListNotEmpty(listQuestionarioquestao)) {
			for (Questionarioquestao questionarioquestao : listQuestionarioquestao) {
				mapQuestionarioquestao.put(questionarioquestao.getCdquestionarioquestao(), questionarioquestao);
			}
		}
		
		int pontuacao;
		int pontuacaoTotal = 0;
		for (Pessoaquestionarioquestao pqq : bean.getListaPessoaQuestionarioQuestao()) {
			if (pqq != null && pqq.getQuestionarioquestao() != null && mapQuestionarioquestao.containsKey(pqq.getQuestionarioquestao().getCdquestionarioquestao())) {
				Questionarioquestao questionarioquestao = mapQuestionarioquestao.get(pqq.getQuestionarioquestao().getCdquestionarioquestao());
				pontuacao = 0;
				if (questionarioquestao.getTiporesposta().equals(TipoRespostaEnum.SIM_NAO)){
					if (pqq.getSim() != null && pqq.getSim()){
						if(questionarioquestao.getPontuacaosim() != null){
							pontuacao += questionarioquestao.getPontuacaosim();
						}else if(questionarioquestao.getPontuacaoquestao() != null){
							pontuacao += questionarioquestao.getPontuacaoquestao();
						}						
					} else if (pqq.getSim() != null && !pqq.getSim()){
						if(questionarioquestao.getPontuacaonao() != null){
							pontuacao += questionarioquestao.getPontuacaonao();
						}else if(questionarioquestao.getPontuacaoquestao() != null){
							pontuacao += questionarioquestao.getPontuacaoquestao();
						}
					}else if (pqq.getResposta() != null && !"".equals(pqq.getResposta()) && questionarioquestao.getPontuacaoquestao() != null){
						pontuacao += questionarioquestao.getPontuacaoquestao();
					}
				}else {
					if(questionarioquestao.getTiporesposta().equals(TipoRespostaEnum.OUTRAS_RESPOSTAS)
							&& pqq.getResposta() != null && !"".equals(pqq.getResposta())){
						if (SinedUtil.isListNotEmpty(questionarioquestao.getListaquestionarioresposta())) {
							for (Questionarioresposta qr : questionarioquestao.getListaquestionarioresposta()) {
								if (pqq.getResposta().equals(qr.getResposta())) {
									questionarioquestao.setPontuacaoquestao(qr.getPontuacao());
									break;
								}
							}
						}
					}
					if (pqq.getResposta() != null && !"".equals(pqq.getResposta()) && questionarioquestao.getPontuacaoquestao() != null){
						pontuacao += questionarioquestao.getPontuacaoquestao();
					}
				}
				pqq.setPontuacao(pontuacao);
				pontuacaoTotal += pontuacao;
				pqq.setPessoaquestionario(bean);
			}
		}
		bean.setPontuacao(pontuacaoTotal);
		Questionario questionario = new Questionario();
		questionario = questionarioService.carregarQuestionarioPorId(bean.getQuestionario());
		
		if (SinedUtil.isListNotEmpty(bean.getListaGrupoquestionario())) {
			Integer mediaPontuacaoGrupos = 0;
			Integer qtdGrupoquestionario = 0;
			for (Grupoquestionario gq : bean.getListaGrupoquestionario()) {
				if (gq != null && gq.getNaoseaplica() != null && !gq.getNaoseaplica()) {
					Grupoquestionario grupoquestionario = mapGrupoquestionario.get(gq.getCdgrupoquestionario()); 
					if (grupoquestionario != null && grupoquestionario.getMediapontuacao() != null) {
						qtdGrupoquestionario++;
						mediaPontuacaoGrupos += grupoquestionario.getMediapontuacao();
					}
				}
			}
			if(qtdGrupoquestionario > 0){
			if (mediaPontuacaoGrupos > 0 && bean.getPontuacao() != null && bean.getPontuacao() > mediaPontuacaoGrupos / qtdGrupoquestionario){
				bean.setSituacao(SituacaoEnum.APROVADO);
			} else {
				bean.setSituacao(SituacaoEnum.REPROVADO);
			}
			}else{
				bean.setSituacao(SituacaoEnum.APROVADO);
			}
		} else {
			if(questionario != null && questionario.getMediapontuacao() != null){
				if (bean.getPontuacao() > questionario.getMediapontuacao()){
					bean.setSituacao(SituacaoEnum.APROVADO);
				} else {
					bean.setSituacao(SituacaoEnum.REPROVADO);
				}
			}
		}
	}
	
	public ModelAndView avaliarCliente(WebRequestContext request, Cliente bean){
		Pessoaquestionario pessoaquestionario = new Pessoaquestionario();
		pessoaquestionario.setPessoa(clienteService.load(bean));
		Boolean origemAgendainteracaoByCliente = Boolean.TRUE.toString().equals(request.getParameter("origemAgendainteracaoByCliente"));
		String cdagendainteracao = request.getParameter("cdagendainteracao");
		request.setAttribute("origemAgendainteracaoByCliente", origemAgendainteracaoByCliente);
		boolean agendarProximaInteracao = "true".equals(request.getParameter("agendarProximaInteracao"));
		request.setAttribute("agendarProximaInteracao", agendarProximaInteracao);
		if(origemAgendainteracaoByCliente && !StringUtils.isEmpty(cdagendainteracao)){
			pessoaquestionario.setCdagendainteracao(Integer.parseInt(cdagendainteracao));
		}
		pessoaquestionario.setQuestionario(questionarioService.carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario.CLIENTE));
		if (pessoaquestionario != null && pessoaquestionario.getQuestionario() != null && pessoaquestionario.getQuestionario().getDefinirgrupo() != null && pessoaquestionario.getQuestionario().getDefinirgrupo()) {
			List<Grupoquestionario> listaGrupoquestionario = new ArrayList<Grupoquestionario>();
			if (SinedUtil.isListNotEmpty(pessoaquestionario.getQuestionario().getListaQuestionariogrupo())) {
				for (Questionariogrupo questionariogrupo : pessoaquestionario.getQuestionario().getListaQuestionariogrupo()) {
					if (questionariogrupo.getGrupoquestionario() != null && SinedUtil.isListNotEmpty(questionariogrupo.getGrupoquestionario().getListaquestionarioquestao())) {
						listaGrupoquestionario.add(questionariogrupo.getGrupoquestionario());
					}
				}
			}
			pessoaquestionario.setListaGrupoquestionario(listaGrupoquestionario);
			return new ModelAndView("direct:/process/popup/pessoaquestionariogrupo", "pessoaquestionario", pessoaquestionario);
		} else {
			List<Pessoaquestionarioquestao> listaPessoaQuestionarioQuestao = new ArrayList<Pessoaquestionarioquestao>();
			Pessoaquestionarioquestao pessoaQuestionarioQuestao;
			if (pessoaquestionario.getQuestionario() != null && pessoaquestionario.getQuestionario().getListaquestionarioquestao() != null){
				for (Questionarioquestao qq : pessoaquestionario.getQuestionario().getListaquestionarioquestao()) {
					pessoaQuestionarioQuestao = new Pessoaquestionarioquestao();
					pessoaQuestionarioQuestao.setQuestionarioquestao(qq);
					listaPessoaQuestionarioQuestao.add(pessoaQuestionarioQuestao);
				}
			}
			pessoaquestionario.setListaPessoaQuestionarioQuestao(listaPessoaQuestionarioQuestao);
			return new ModelAndView("direct:/process/popup/pessoaquestionario", "pessoaquestionario", pessoaquestionario);
		}
	}
	
	public ModelAndView avaliarClienteByAgendainteracao(WebRequestContext request, Agendainteracao bean){
		if(bean.getCdagendainteracao() == null){
			request.addError("Agenda de intera��o n�o pode ser nula.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		bean = agendainteracaoService.load(bean);
		if(!Tipoagendainteracao.CLIENTE.equals(bean.getTipoagendainteracao())){
			request.addError("S� � poss�vel realizar a avalia��o caso o tipo de pessoa igual 'Cliente'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		
		if(bean.getCliente() == null){
			request.addError("Cliente n�o pode ser nulo.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		return avaliarCliente(request, bean.getCliente());
	}
	
	public ModelAndView avaliarInteracao(WebRequestContext request, Agendainteracao bean){
		if(bean.getCdagendainteracao() == null){
			request.addError("Agenda de intera��o n�o pode ser nula.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		bean = agendainteracaoService.load(bean);
		if(!Tipoagendainteracao.CLIENTE.equals(bean.getTipoagendainteracao())){
			request.addError("S� � poss�vel realizar a avalia��o caso o tipo de pessoa igual 'Cliente'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		
		if(bean.getCliente() == null){
			request.addError("Cliente n�o pode ser nulo.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Pessoaquestionario pessoaquestionario = new Pessoaquestionario();
		bean = agendainteracaoService.load(bean);
		pessoaquestionario.setPessoa(bean.getCliente());
		Boolean origemAgendainteracaoByInteracao = Boolean.TRUE.toString().equals(request.getParameter("origemAgendainteracaoByInteracao"));
		String cdagendainteracao = request.getParameter("cdagendainteracao");
		request.setAttribute("origemAgendainteracaoByInteracao", origemAgendainteracaoByInteracao);
		boolean agendarProximaInteracao = "true".equals(request.getParameter("agendarProximaInteracao"));
		request.setAttribute("agendarProximaInteracao", agendarProximaInteracao);
		if(origemAgendainteracaoByInteracao && !StringUtils.isEmpty(cdagendainteracao)){
			pessoaquestionario.setCdagendainteracao(Integer.parseInt(cdagendainteracao));
		}
		pessoaquestionario.setQuestionario(questionarioService.carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario.SERVICO));
		if (pessoaquestionario != null && pessoaquestionario.getQuestionario() != null && pessoaquestionario.getQuestionario().getDefinirgrupo() != null && pessoaquestionario.getQuestionario().getDefinirgrupo()) {
			List<Grupoquestionario> listaGrupoquestionario = new ArrayList<Grupoquestionario>();
			if (SinedUtil.isListNotEmpty(pessoaquestionario.getQuestionario().getListaQuestionariogrupo())) {
				for (Questionariogrupo questionariogrupo : pessoaquestionario.getQuestionario().getListaQuestionariogrupo()) {
					if (questionariogrupo.getGrupoquestionario() != null && SinedUtil.isListNotEmpty(questionariogrupo.getGrupoquestionario().getListaquestionarioquestao()) &&
						(questionariogrupo.getGrupoquestionario().getAtividadetipo() == null || questionariogrupo.getGrupoquestionario().getAtividadetipo().equals(bean.getAtividadetipo()))) {
						listaGrupoquestionario.add(questionariogrupo.getGrupoquestionario());
					}
				}
			}
			pessoaquestionario.setListaGrupoquestionario(listaGrupoquestionario);
			return new ModelAndView("direct:/process/popup/pessoaquestionariogrupo", "pessoaquestionario", pessoaquestionario);
		} else {
			List<Pessoaquestionarioquestao> listaPessoaQuestionarioQuestao = new ArrayList<Pessoaquestionarioquestao>();
			Pessoaquestionarioquestao pessoaQuestionarioQuestao;
			if (pessoaquestionario.getQuestionario() != null && pessoaquestionario.getQuestionario().getListaquestionarioquestao() != null){
				for (Questionarioquestao qq : pessoaquestionario.getQuestionario().getListaquestionarioquestao()) {
					if(qq.getAtividadetipo() == null || qq.getAtividadetipo().equals(bean.getAtividadetipo())){
						pessoaQuestionarioQuestao = new Pessoaquestionarioquestao();
						pessoaQuestionarioQuestao.setQuestionarioquestao(qq);
						pessoaQuestionarioQuestao.setObrigarresposta(qq.getObrigarresposta());
						listaPessoaQuestionarioQuestao.add(pessoaQuestionarioQuestao);						
					}
				}
			}
			pessoaquestionario.setListaPessoaQuestionarioQuestao(listaPessoaQuestionarioQuestao);
			return new ModelAndView("direct:/process/popup/pessoaquestionario", "pessoaquestionario", pessoaquestionario);
		}
	}
	
	public ModelAndView avaliarFornecedorOcNaoConcluida(WebRequestContext request, Fornecedor bean){
		String identificadortela = request.getParameter("identificadortela");
		Boolean origemOrdemCompra = Boolean.valueOf(request.getParameter("origemOrdemCompra") != null ? request.getParameter("origemOrdemCompra") : "false");
		Fornecedor fornecedor = new Fornecedor();
		fornecedor = bean;
		Pessoaquestionario pessoaquestionario = new Pessoaquestionario();
		pessoaquestionario.setPessoa(fornecedorService.carregaFornecedor(fornecedor));
		request.setAttribute("identificadortela", identificadortela);
		if (origemOrdemCompra != null && origemOrdemCompra) {
			pessoaquestionario.setQuestionario(questionarioService.carregaQuestionarioPorTipoPessoaForOrdemCompra(Tipopessoaquestionario.FORNECEDOR));
		} else {
			pessoaquestionario.setQuestionario(questionarioService.carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario.FORNECEDOR));
		}
		
		if (pessoaquestionario != null && pessoaquestionario.getQuestionario() != null && pessoaquestionario.getQuestionario().getDefinirgrupo() != null && pessoaquestionario.getQuestionario().getDefinirgrupo()) {
			List<Grupoquestionario> listaGrupoquestionario = new ArrayList<Grupoquestionario>();
			if (SinedUtil.isListNotEmpty(pessoaquestionario.getQuestionario().getListaQuestionariogrupo())) {
				for (Questionariogrupo questionariogrupo : pessoaquestionario.getQuestionario().getListaQuestionariogrupo()) {
					if (questionariogrupo.getGrupoquestionario() != null && SinedUtil.isListNotEmpty(questionariogrupo.getGrupoquestionario().getListaquestionarioquestao())) {
						listaGrupoquestionario.add(questionariogrupo.getGrupoquestionario());
					}
				}
			}
			request.setAttribute("identificadortela", identificadortela);
			pessoaquestionario.setListaGrupoquestionario(listaGrupoquestionario);
			return new ModelAndView("direct:/process/popup/pessoaquestionarioocnaoconcluidagrupo", "pessoaquestionario", pessoaquestionario);
		} else {
			List<Pessoaquestionarioquestao> listaPessoaQuestionarioQuestao = new ArrayList<Pessoaquestionarioquestao>();
			Pessoaquestionarioquestao pessoaQuestionarioQuestao;
			if (pessoaquestionario.getQuestionario() != null && pessoaquestionario.getQuestionario().getListaquestionarioquestao() != null){
				for (Questionarioquestao qq : pessoaquestionario.getQuestionario().getListaquestionarioquestao()) {
					pessoaQuestionarioQuestao = new Pessoaquestionarioquestao();
					pessoaQuestionarioQuestao.setQuestionarioquestao(qq);
					listaPessoaQuestionarioQuestao.add(pessoaQuestionarioQuestao);
				}
			}
			request.setAttribute("identificadortela", identificadortela);
			pessoaquestionario.setListaPessoaQuestionarioQuestao(listaPessoaQuestionarioQuestao);
			return new ModelAndView("direct:/process/popup/pessoaquestionarioocnaoconcluida", "pessoaquestionario", pessoaquestionario);
		}
	} 
	
	/**
	 * 
	 * M�todo que guarda em sess�o a avalia��o realizada na entrada da ordem de compra.
	 *
	 *@author Jo�o Vitor
	 *@date 29/01/2015
	 * @param request
	 * @param bean
	 */
	public void saveAvaliacaoOcNaoConcluida(WebRequestContext request, Pessoaquestionario bean){

		if (bean.getQuestionario() != null && bean.getQuestionario().getDefinirgrupo() != null && bean.getQuestionario().getDefinirgrupo()) {
			preencheListaPessoaQuestinarioQuestao(bean);
		} 
		
		if(bean.getListaPessoaQuestionarioQuestao() != null){
			avaliaQuestionario(bean);
			
			String identificadortela = request.getParameter("identificadortela");
			
			request.getSession().setAttribute("pessoaquestionario" + identificadortela, bean);
			request.getSession().setAttribute("isQuestionarioFornecedorRespondido", Boolean.TRUE);
		}
		View.getCurrent().println("<script> " +
				"parent.isQuestionarioFornecedorRespondido = true; " +
				"parent.alert('Avalia��o feita com sucesso!'); " +
				"</script>");
		SinedUtil.fechaPopUpSemRecarregar(request);
	}
	
	/**
	 * 
	 * M�todo respons�vel por preencher a lista de Pessoaquestionarioquestao caso o questipn�rio utilize grupos.
	 *
	 * @author Jo�o Vitor
	 * @date 18/03/2015
	 * @param bean
	 */
	private void preencheListaPessoaQuestinarioQuestao(Pessoaquestionario bean) {
		List<Pessoaquestionarioquestao> listaPessoaQuestionarioQuestao = new ArrayList<Pessoaquestionarioquestao>();
		if (SinedUtil.isListNotEmpty(bean.getListaGrupoquestionario())) {
			for (Grupoquestionario grupoquestionario : bean.getListaGrupoquestionario()) {
				if (grupoquestionario != null && SinedUtil.isListNotEmpty(grupoquestionario.getListaquestionarioquestao()) && grupoquestionario != null && !grupoquestionario.getNaoseaplica()) {
					for (Questionarioquestao questionarioquestao: grupoquestionario.getListaquestionarioquestao()) {
						if (questionarioquestao != null) {
							Pessoaquestionarioquestao pessoaQuestionarioQuestao = new Pessoaquestionarioquestao();
							pessoaQuestionarioQuestao = new Pessoaquestionarioquestao();
							pessoaQuestionarioQuestao.setQuestionarioquestao(questionarioquestao);
							if (questionarioquestao.getQuestionarioresposta() != null) {
								if (questionarioquestao.getQuestionarioresposta().getResposta() != null) {
									pessoaQuestionarioQuestao.setResposta(questionarioquestao.getQuestionarioresposta().getResposta());
								} else if(questionarioquestao.getQuestionarioresposta().getSim() != null) {
									pessoaQuestionarioQuestao.setSim(questionarioquestao.getQuestionarioresposta().getSim());
								}
							}
							listaPessoaQuestionarioQuestao.add(pessoaQuestionarioQuestao);
						}
					}
				}
			}
		}
		bean.setListaPessoaQuestionarioQuestao(listaPessoaQuestionarioQuestao);
	}
	
	public void deleteQuestionarioForOCNaoConcluida(WebRequestContext request){
		String identificadortela = request.getParameter("identificadortela");
		request.getSession().setAttribute("pessoaquestionario" + identificadortela, null);
	}
 
}
