package br.com.linkcom.sined.modulo.suprimentos.controller.process;

import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.APROVADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.BAIXADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.ENTREGA_PARCIAL;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.ESTORNAR;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.PEDIDO_ENVIADO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.mail.SendFailedException;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraacao;
import br.com.linkcom.sined.geral.bean.Ordemcompraarquivo;
import br.com.linkcom.sined.geral.bean.Ordemcomprahistorico;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.SolicitarrestanteBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.SolicitarrestanteItemBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/suprimento/process/FluxoSituacaoOrdem",
		authorizationModule=ProcessAuthorizationModule.class
)
public class FluxoSituacaoOrdemProcess extends MultiActionController{

	private OrdemcompraService ordemcompraService;
	private EmpresaService empresaService;
	private ParametrogeralService parametrogeralService;
	private UsuarioService usuarioService;
	private MaterialService materialService;
	private MotivoavisoService motivoavisoService;
	private AvisoService avisoService;
	private ContatoService contatoService;
	
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	
	/**
	 * M�todo default que redireciona para o tipo de altera��o escolhido
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#findOrdens(String)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#autorizar(String, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#cancelar(List, org.springframework.validation.BindException)
	 * @see #redirecionaPopUpJustificativa(WebRequestContext, String, Ordemcompraacao)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#estornar(List, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#baixar(List, String, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#findOrdemCompraParaEnviarPedido(String)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#enviarPedido(Ordemcompra, org.springframework.validation.BindException)
	 * @see #redirecionaPopUpEnviaEmail(WebRequestContext, Ordemcompra)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#receberEntrega(List, String, org.springframework.validation.BindException)
	 * @param request
	 * @param acao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView acao(WebRequestContext request, Ordemcompra acao){
		if (acao == null || acao.getCdordemcompra() == null) {
			throw new SinedException("Ordem de compra n�o pode ser nulo.");
		}
		
		String whereIn = request.getParameter("selectedItens");
		
		List<Ordemcompra> ordens = null;
		if(!acao.getCdordemcompra().equals(AUTORIZADA.getCodigo()) && !acao.getCdordemcompra().equals(PEDIDO_ENVIADO.getCodigo())){
			ordens = ordemcompraService.findOrdens(whereIn);
		}
		if((acao.getCdordemcompra().equals(AUTORIZADA.getCodigo()) || acao.getCdordemcompra().equals(APROVADA.getCodigo())) &&
				usuarioService.isValorMaiorLimitevaloraprovacaoautorizacao(whereIn, "ordemcompra")){
			request.addError("O valor da ordem de compra � maior do que o limite para autoriza��o.");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
			return null;
		}
		
		if(acao.getCdordemcompra().equals(AUTORIZADA.getCodigo())){
			if(ordemcompraService.autorizar(whereIn, request.getBindException())) {
//				CASO O PAR�METRO EnvioEmailsOrdemDeCompra TENHA UM E-MAIL CADASTRADO E
//				O PAR�METRO Aprovar Ordem de Compra ESTIVER COMO TRUE ENVIA E-MAIL. 
//				String email = parametrogeralService.getValorPorNome(Parametrogeral.ENVIAEMAILORDEMDECOMPRA);
//				String aprovar = parametrogeralService.getValorPorNome(Parametrogeral.APROVAR_ORDEM_DE_COMPRA);
//				
//				if (email != null && !email.equals("") && aprovar != null && aprovar.toUpperCase().equals("FALSE")){
//					enviarEmail(email, whereIn);
//				}
				List<Ordemcompra> ordensAutorizar = ordemcompraService.findOrdensParaAutorizar(whereIn);
				
				request.addMessage("Registro(s) autorizado(s) com sucesso.", MessageType.INFO);
				
				Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.AUTORIZAR_ORDEMCOMPRA);
				
				if(motivoAviso != null){
					if(motivoAviso != null && SinedUtil.isListNotEmpty(ordensAutorizar)){
						List<Aviso> avisoList = new ArrayList<Aviso>();
						
						for(Ordemcompra oc : ordensAutorizar){
							avisoList.add(new Aviso("Ordem de compra autorizada", "C�digo da ordem de compra: " + oc.getCdordemcompra(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
									motivoAviso.getUsuario(), AvisoOrigem.ORDEM_DE_COMPRA, oc.getCdordemcompra(), oc.getEmpresa(), null, motivoAviso));
							List<Usuario> usuarioList = avisoService.salvarAvisos(avisoList, true);
						
							StringBuilder notWhereInCdusuario = new StringBuilder();
							for (Usuario u : usuarioList) {
								if(!"".equals(notWhereInCdusuario.toString())) {
									notWhereInCdusuario.append(",");
								}
								notWhereInCdusuario.append(u.getCdpessoa());
							}
							if(Boolean.TRUE.equals(motivoAviso.getEmail())){
								ordemcompraService.enviarEmailOrdemcompra(whereIn, Situacaosuprimentos.AUTORIZADA, notWhereInCdusuario.toString());
							}
						}
					}
				}
				
			}
		} else if(acao.getCdordemcompra().equals(APROVADA.getCodigo())){
			if(ordemcompraService.aprovar(whereIn, request.getBindException())){
				request.addMessage("Registro(s) aprovado(s) com sucesso.", MessageType.INFO);
				
				Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.APROVAR_ORDEMCOMPRA);
				if(motivoAviso != null && SinedUtil.isListNotEmpty(ordens)){
					for(Ordemcompra oc : ordens){
						List<Aviso> avisoList = new ArrayList<Aviso>();
						
						avisoList.add(new Aviso("Ordem de compra aprovada", "C�digo da ordem de compra: " + oc.getCdordemcompra(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
								motivoAviso.getUsuario(), AvisoOrigem.ORDEM_DE_COMPRA, oc.getCdordemcompra(), oc.getEmpresa(), null, motivoAviso));
						List<Usuario> usuarioList = avisoService.salvarAvisos(avisoList, true);
						
						StringBuilder notWhereInCdusuario = new StringBuilder();
						for (Usuario u : usuarioList) {
							if(!"".equals(notWhereInCdusuario.toString())) {
								notWhereInCdusuario.append(",");
							}
							notWhereInCdusuario.append(u.getCdpessoa());
						}
						
						if(Boolean.TRUE.equals(motivoAviso.getEmail())){
							ordemcompraService.enviarEmailOrdemcompra(oc.getCdordemcompra().toString(), Situacaosuprimentos.APROVADA, notWhereInCdusuario.toString());
						}
					}
				}
			}
		} else if(acao.getCdordemcompra().equals(CANCELADA.getCodigo())){
			if(ordemcompraService.cancelar(ordens, request.getBindException())){
				return redirecionaPopUpJustificativa(request, whereIn, Ordemcompraacao.CANCELADA);
			}else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
		} else if(acao.getCdordemcompra().equals(ESTORNAR.getCodigo())){
			if(ordemcompraService.estornar(ordens, request.getBindException())){
				return redirecionaPopUpJustificativa(request, whereIn, Ordemcompraacao.ESTORNADA);
			}else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
		} else if(acao.getCdordemcompra().equals(BAIXADA.getCodigo())){
			if(ordemcompraService.baixar(ordens, whereIn, request.getBindException()))
				request.addMessage("Registro(s) baixado(s) com sucesso.", MessageType.INFO);
		} else if(acao.getCdordemcompra().equals(PEDIDO_ENVIADO.getCodigo())){
			Ordemcompra ordemcompra = ordemcompraService.findOrdemCompraParaEnviarPedido(whereIn);
			
			if(ordemcompraService.enviarPedido(ordemcompra, request.getBindException())){
				if(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) &&
						usuarioService.isValorMaiorLimitevaloraprovacaoautorizacao(whereIn, "ordemcompra")){
					request.addError("O valor da ordem de compra � maior do que o limite. N�o foi poss�vel enviar pedido.");
					SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
					return null;
				}
				return redirecionaPopUpEnviaEmail(request, ordemcompra);
			} else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
		} else if(acao.getCdordemcompra().equals(ENTREGA_PARCIAL.getCodigo())){
			if(ordemcompraService.receberEntrega(ordens, whereIn, request.getBindException())){
				return redirecionaPopUpReceberEntrega(request, whereIn);
			}else{
				SinedUtil.fechaPopUp(request);
				return null;
			}
				
		}
		
		if(request.getBindException().hasErrors()){
			request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
		}
		
		
		return getControllerModelAndView(request);
	}
	
	public void receberEntregaArquivo(WebRequestContext request, Ordemcompra ordemcompra){
		request.getSession().removeAttribute("ARQUIVO_XML_RECEBER_ENTREGA");
		
		if(ordemcompra.getArquivoxmlnfe() != null){
			request.getSession().setAttribute("ARQUIVO_XML_RECEBER_ENTREGA", ordemcompra.getArquivoxmlnfe());
		}
		
		if (ordemcompra.getChaveacesso() != null){
			request.getSession().setAttribute("CHAVE_ACESSO", ordemcompra.getChaveacesso());
			request.getSession().setAttribute("HTML_CHAVE_ACESSO", ordemcompra.getHtmlChaveacesso());
		}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>parent.location = '" + request.getServletRequest().getContextPath() + 
											"/suprimento/crud/Entrega?ACAO=conferenciaDados&whereIn=" + ordemcompra.getWhereIn() + 
											"&controller=" + request.getParameter("controller") +
											"&receberEntregaByMaterialmestre=" + (ordemcompra.getReceberEntregaByMaterialmestre() != null ?
													ordemcompra.getReceberEntregaByMaterialmestre() : "false" )+ 
											"&agrupamentoMesmoMaterial=" + ordemcompra.getAgrupamentoMesmoMaterial() + "';" +
				"parent.$.akModalRemove(true);</script>");
	}
	
	private ModelAndView redirecionaPopUpReceberEntrega(WebRequestContext request, String whereIn){
		request.getSession().removeAttribute("ARQUIVO_XML_RECEBER_ENTREGA");
		request.getSession().removeAttribute("CHAVE_ACESSO");
		request.setAttribute("controller", request.getParameter("controller"));
		
		Ordemcompra ordemcompra = new Ordemcompra();
		ordemcompra.setWhereIn(whereIn);
		ordemcompra.setExistItemGrade(ordemcompraService.existItemGrade(whereIn));
		
		request.setAttribute("variasOrdens", whereIn.split(",").length > 1);
		
		return new ModelAndView("direct:/process/popup/receberEntrega").addObject("ordemcompra", ordemcompra);
	}
	
	/**
	 * M�todo que prepara email e redireciona para pop up
	 * 
	 * @param request
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	private ModelAndView redirecionaPopUpEnviaEmail(WebRequestContext request, Ordemcompra ordemcompra) {
		List<Contato> contatos = new ArrayList<Contato>();
		Set<Ordemcompraarquivo> arquivos = new ListSet<Ordemcompraarquivo>(Ordemcompraarquivo.class);
		Contato contato = new Contato();
		
		if(ordemcompra.getContato() != null){
			contato = ordemcompra.getContato();
			contato.setContatotrans(ordemcompra.getContato());
			
			contatos.add(ordemcompra.getContato());
		} else{
			contatos.add(contato);
		}
		arquivos.add(new Ordemcompraarquivo());
		
		
		for (Ordemcompraarquivo arquivosordemcompra : ordemcompra.getListaArquivosordemcompra()){
			if(arquivosordemcompra.getEnviarFornecedor() != null && arquivosordemcompra.getEnviarFornecedor()){
				arquivos.add(arquivosordemcompra);
			}
		}
		
		ordemcompra.setListaArquivosordemcompra(arquivos);
		ordemcompra.setListaDestinatarios(contatos);
		ordemcompra.setRemetente(((Usuario)Neo.getUser()).getEmail());
		ordemcompra.setEmail(parametrogeralService.getValorPorNome(Parametrogeral.DESCRICAO_PEDIDO_COMPRA_EMAIL));
		request.setAttribute("listaContato", contatoService.findByPessoaCombo(ordemcompra.getFornecedor(), Boolean.TRUE));
		return new ModelAndView("direct:/process/popup/enviaEmailOrdemCompra").addObject("ordemcompra", ordemcompra);
	}
	
	/**
	 * M�todo que envia email, muda situa��o da ordem de compra para 'pedido enviado' e retorna para a tela que abriu
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContatoService#retiraContatoComEmailDuplicados(List)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#emitirOrdemCompra(String)
	 * @see #enviaPedido(WebRequestContext, Ordemcompra)
	 * @param request
	 * @param ordemcompra
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public ModelAndView enviaPedidoComEmail(WebRequestContext request, Ordemcompra ordemcompra) throws Exception {
		try {
			ordemcompraService.enviaPedidoComEmail(ordemcompra);
		} catch (SendFailedException e) {
			StringBuilder emailserro = new StringBuilder();
			if(e.getInvalidAddresses() != null){
				for(javax.mail.Address address : e.getInvalidAddresses()){
					emailserro.append("\n").append(address.toString());
				}
			}
			e.printStackTrace();
			request.addError("N�o foi poss�vel enviar pedido. " + (!"".equals(emailserro.toString()) ? ("Email inv�lido: " + emailserro.toString()) : ""));
			SinedUtil.fechaPopUp(request);
			return null;
		} catch (Exception e){	
			e.printStackTrace();
			request.addError("N�o foi poss�vel enviar pedido.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		return enviaPedido(request, ordemcompra);
	}

	/**
	 * M�todo que da update na ordem de compra mudando a situa��o para 'Pedido enviado'.
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#getSolicitacoesCompraRelacionadasAOrdemCompra(Ordemcompra)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#saveHistoricoEUpdateSituacao(Ordemcomprahistorico, Ordemcompraacao, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#montaAvisosOrdemCompra(List, List, Ordemcompra, String)
	 * @param request
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView enviaPedido(WebRequestContext request,	Ordemcompra ordemcompra) {
		List<Solicitacaocompra> solicitacoesOrdemCompra = ordemcompraService.getSolicitacoesCompraRelacionadasAOrdemCompra(ordemcompra);
		
		List<Aviso> listaAvisos = new ListSet<Aviso>(Aviso.class);
		ordemcompra.setEmpresa(empresaService.findEmpresaOrdemCompra(ordemcompra));
		ordemcompraService.montaAvisosOrdemCompra(listaAvisos, MotivoavisoEnum.PEDIDO_COMPRA_ENVIADO, ordemcompra, "Aguardando entrega");
		ordemcompraService.saveHistoricoEUpdateSituacao(new Ordemcomprahistorico(ordemcompra.getCdordemcompra().toString()), Ordemcompraacao.PEDIDO_ENVIADO, PEDIDO_ENVIADO, solicitacoesOrdemCompra, listaAvisos);
		
		List<Ordemcompra> listaOrdemcompra = new ArrayList<Ordemcompra>();
		listaOrdemcompra.add(ordemcompra);
		ordemcompraService.gerarFluxoCaixaOrdens(listaOrdemcompra);
		
		request.addMessage("Registro(s) enviado(s) com sucesso.", MessageType.INFO);
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	/**
	 * M�todo chamado ap�s fechar o pop up de justificativa de estorno
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#findOrdens(String)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#cancelar(List, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#saveHistoricoEUpdateSituacao(Ordemcomprahistorico, Ordemcompraacao, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @param request
	 * @param ordemcomprahistorico
	 * @author Tom�s Rabelo
	 */
	@Action("saveJustificativa")
	public void saveJustificativaCancelamento (WebRequestContext request, Ordemcomprahistorico ordemcomprahistorico){
		List<Ordemcompra> ordens = ordemcompraService.findOrdens(ordemcomprahistorico.getIds());
		try {
			if(ordemcompraService.cancelar(ordens, request.getBindException())){
				ordemcompraService.saveHistoricoEUpdateSituacao(ordemcomprahistorico, Ordemcompraacao.CANCELADA, CANCELADA, null, null);
				request.addMessage("Registro(s) cancelado(s) com sucesso.", MessageType.INFO);
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			if(request.getBindException().hasErrors()){
				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			}
			SinedUtil.fechaPopUp(request);
		}
	}
	
	/**
	 * M�todo chamado ap�s fechar o pop up de justificativa de cancelamento
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#findOrdens(String)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#estornar(List, org.springframework.validation.BindException)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#getSolicitacoesCompraRelacionadasAsOrdensCompras(List, String)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#saveHistoricoEUpdateSituacao(Ordemcomprahistorico, Ordemcompraacao, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @param request
	 * @param ordemcomprahistorico
	 * @author Tom�s Rabelo
	 */
	@Action("saveJustificativa")
	public void saveJustificativaEstorno (WebRequestContext request, Ordemcomprahistorico ordemcomprahistorico){
		List<Ordemcompra> ordens = ordemcompraService.findOrdensParaEstorno(ordemcomprahistorico.getIds());
		try {
			if(ordemcompraService.estornar(ordens, request.getBindException())){
				List<Solicitacaocompra> solicitacaoes = ordemcompraService.getSolicitacoesCompraRelacionadasAsOrdensCompras(ordens, ordemcomprahistorico.getIds());
				
				ordemcompraService.saveHistoricoEUpdateSituacao(ordemcomprahistorico, Ordemcompraacao.ESTORNADA, ESTORNAR, solicitacaoes, null);
				request.addMessage("Registro(s) estornado(s) com sucesso.", MessageType.INFO);
			}
		} catch (Exception e) {
			request.addError(e.getMessage());
		} finally {
			if(request.getBindException().hasErrors()){
				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			}
			SinedUtil.fechaPopUp(request);
		}
	}
	
	/**
	 * M�todo chamado quando � escolhida a a��o estornar ou cancelar as ordens de compra. Este m�todo abre um pop up para 
	 * o usu�rio digitar a justificativa do estorno ou baixa.
	 * 
	 * @param request
	 * @param whereIn
	 * @param ordemcompraacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	private ModelAndView redirecionaPopUpJustificativa(WebRequestContext request, String whereIn, Ordemcompraacao ordemcompraacao) {
		Ordemcomprahistorico ordemcomprahistorico = new Ordemcomprahistorico();
		ordemcomprahistorico.setIds(whereIn);
		ordemcomprahistorico.setOrdemcompraacao(ordemcompraacao);
		
		if(ordemcompraacao.equals(Ordemcompraacao.CANCELADA)){
			request.setAttribute("descricao", "CANCELAR");
			request.setAttribute("msg", "Justificativa para o cancelamento");
		}else{
			request.setAttribute("descricao", "ESTORNAR");
			request.setAttribute("msg", "Justificativa para o estorno");
		}
		return new ModelAndView("direct:/process/popup/canceladaestornadaJustificativa")
								.addObject("ordemcomprahistorico", ordemcomprahistorico);
	}

	/**
	 * Retorna para o controller do USC
	 * 
	 * @param request
	 * @return
	 * @author Tom�s Rabelo
	 */
	private ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			action += "&cdordemcompra="+request.getParameter("selectedItens");
		}
		return new ModelAndView("redirect:"+action);
	}
	
	/**
	 * M�todo que gera o contrato de fornecimento da(s) ordem(ns) de compra
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarFornecimento(WebRequestContext request){
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ?  request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("/suprimento/crud/Ordemcompra");
		}
		
		List<Ordemcompra> listaOrdemcompra = ordemcompraService.findForGerarfornecimento(whereIn);
		
		if(listaOrdemcompra == null || listaOrdemcompra.isEmpty()){
			request.addError("N�o existe ordem de compra para gerar fornecimento.");
			return new ModelAndView("redirect:/suprimento/crud/Ordemcompra" + (entrada ? "?ACAO=consultar&cdordemcompra=" + whereIn : ""));
		}
		
		Fornecedor fornecedor = null;
		Empresa empresa = null;
		boolean existFornecedorEmpresaDiferente = false;
		boolean existOCDiferentePedidoenviado = false;
		for(Ordemcompra ordemcompra : listaOrdemcompra){
			if(ordemcompra.getAux_ordemcompra() != null && ordemcompra.getAux_ordemcompra().getSituacaosuprimentos() != null && 
					!Situacaosuprimentos.PEDIDO_ENVIADO.equals(ordemcompra.getAux_ordemcompra().getSituacaosuprimentos())){
				existOCDiferentePedidoenviado = true;
				break;
			}
			if(ordemcompra.getFornecedor() != null){
				if(fornecedor == null){
					fornecedor = ordemcompra.getFornecedor();
				}else if(!fornecedor.equals(ordemcompra.getFornecedor())){
					existFornecedorEmpresaDiferente = true;
					break;
				}
			}
			if(ordemcompra.getEmpresa() != null){
				if(empresa == null){
					empresa = ordemcompra.getEmpresa();
				}else if(!empresa.equals(ordemcompra.getEmpresa())){
					existFornecedorEmpresaDiferente = true;
					break;
				}
			}
		}
		
		if(existOCDiferentePedidoenviado){
			request.addError("S� � poss�vel gerar fornecimento de ordem de compra com situa��o de Pedido Enviado.");
			return new ModelAndView("redirect:/suprimento/crud/Ordemcompra" + (entrada ? "?ACAO=consultar&cdordemcompra=" + whereIn : ""));
		}
		if(existFornecedorEmpresaDiferente){
			request.addError("S� � poss�vel gerar fornecimento de ordem de compra com o mesmo fornecedor e empresa.");
			return new ModelAndView("redirect:/suprimento/crud/Ordemcompra" + (entrada ? "?ACAO=consultar&cdordemcompra=" + whereIn : ""));
		}
		
		return new ModelAndView("redirect:/suprimento/crud/Fornecimentocontrato?ACAO=criar&gerarfornecimento=true&entrada="+entrada+"&selectedItens="+whereIn);
	}
	
	/**
	* M�todo que abre uma popup com os itens com quantidade restante da ordem de compra
	*
	* @param request
	* @return
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView solicitarrestante(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(ordemcompraService.existeSituacaoDiferente(whereIn, Situacaosuprimentos.ENTREGA_PARCIAL)){
			request.addError("� permitido solicitar restante apenas para ordem de compra na situa��o 'Entrega Parcial'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		List<Ordemcompra> listaOrdemcompra = ordemcompraService.findForSolicitarrestante(whereIn);
		SolicitarrestanteBean solicitarrestanteBean = ordemcompraService.criaSolicitarrestanteBean(listaOrdemcompra);
		
		boolean existeItemSemOrigem = false;
		HashMap<Long, List<Solicitacaocompra>> mapSolicitacaocompra = new HashMap<Long, List<Solicitacaocompra>>();
		HashMap<Material, List<Materialclasse>> mapMaterialclasse = new HashMap<Material, List<Materialclasse>>();
		if(SinedUtil.isListNotEmpty(solicitarrestanteBean.getListaSolicitarrestanteItemBean())){
			for(SolicitarrestanteItemBean sci : solicitarrestanteBean.getListaSolicitarrestanteItemBean()){
				if(!existeItemSemOrigem && !sci.getExisteOrigem()){
					existeItemSemOrigem = true;
				}
				mapSolicitacaocompra.put(sci.getIdentificador(), sci.getListaSolicitacaocompra() != null ? sci.getListaSolicitacaocompra() : new ArrayList<Solicitacaocompra>());
				mapMaterialclasse.put(sci.getMaterial(), materialService.findClasses(sci.getMaterial()));
			}
		}
		solicitarrestanteBean.setPedidoorigem(whereIn);
		request.setAttribute("mapSolicitacaocompra", mapSolicitacaocompra);
		request.setAttribute("mapMaterialclasse", mapMaterialclasse);
		request.setAttribute("existeItemSemOrigem", existeItemSemOrigem);
		request.setAttribute("listaEmpresa", empresaService.findAtivos());
		return new ModelAndView("direct:process/popup/solicitarrestante", "bean", solicitarrestanteBean);
	}
	
	/**
	* M�todo que salva as solicita��es de acordo com os itens restantes
	*
	* @param request
	* @param solicitarrestanteBean
	* @return
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView salvaSolicitarrestantes(WebRequestContext request, SolicitarrestanteBean solicitarrestanteBean){
		List<Solicitacaocompra> listaSC = ordemcompraService.criaSolicitacaocompraBySolicitarrestanteBean(solicitarrestanteBean);
		String whereInSolicitacao = ordemcompraService.salvaSolicitacoes(listaSC);
		ordemcompraService.baixarEcriarHistoricoSolicitarrestante(solicitarrestanteBean, whereInSolicitacao);
		
		request.addMessage("Solicita��es criada com sucesso.");
		SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
		return null;
	}
}
