package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.OrdemcompraFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/Ordemcompra", authorizationModule=ReportAuthorizationModule.class)
public class OrdemcompraReport extends SinedReport<OrdemcompraFiltro>{

	private OrdemcompraService ordemcompraService;
	
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	OrdemcompraFiltro filtro) throws Exception {
		return ordemcompraService.gerarRelatorio(filtro);
	}

	@Override
	public String getTitulo(OrdemcompraFiltro filtro) {
		return "ORDEM DE COMPRA";
	}
	
	@Override
	public String getNomeArquivo() {
		return "ordemcompra";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, OrdemcompraFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getProximaentregade(), filtro.getProximaentregaate()));
	}

}
