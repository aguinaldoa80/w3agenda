package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EntregaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/Inspecaoentrega", authorizationModule=ReportAuthorizationModule.class)
public class InspecaoentregaReport extends SinedReport<EntregaFiltro>{

	private EntregaService entregaService;
	
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	EntregaFiltro filtro) throws Exception {
		return entregaService.gerarRelatorioInspecaoEntrega(request.getParameter("selectedItens"));
	}

	@Override
	public String getTitulo(EntregaFiltro filtro) {
		return "INSPE��O DE ENTREGA";
	}
	
	@Override
	public String getNomeArquivo() {
		return "inspecaoentrega";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EntregaFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, EntregaFiltro filtro) throws ResourceGenerationException {
		super.doFiltro(request, filtro);
		return new ModelAndView("redirect:/suprimento/crud/Entrega");
	}

}
