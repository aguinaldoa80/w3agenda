package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.MaterialdevolucaoService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/faturamento/relatorio/Materialdevolucao", authorizationModule=ReportAuthorizationModule.class)
public class ComprovantematerialdevolucaoReport extends ResourceSenderController<Object> {

	private MaterialdevolucaoService materialdevolucaoService;

	public void setMaterialdevolucaoService(MaterialdevolucaoService materialdevolucaoService) {
		this.materialdevolucaoService = materialdevolucaoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, Object filtro) throws Exception {
		return materialdevolucaoService.gerarComprovante(SinedUtil.getItensSelecionados(request));
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Object filtro) throws Exception {
		return null;
	}

}
