package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import java.sql.Date;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.CotacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Cotacaoemissaofornecedor", 
			authorizationModule=ReportAuthorizationModule.class
)
public class CotacaoemissaofornecedorReport extends SinedReport<CotacaoFiltro>{

	private CotacaoService cotacaoService;
	
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	CotacaoFiltro filtro) throws Exception {
		return cotacaoService.emitirCotacaoFornecedor(filtro, request.getParameter("whereIn"));
	}
	
	@Override
	public String getNomeArquivo() {
		return "cotacao";
	}

	@Override
	public String getTitulo(CotacaoFiltro filtro) {
		return "Cota��o";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, CotacaoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}
	
	@Override
	protected void configurarParametros(WebRequestContext request, CotacaoFiltro filtro, Report report) {
		Empresa empresa = this.getEmpresa(filtro);

		if (report != null){
			report.addParameter("LOGO", SinedUtil.getLogo(empresa));
			report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocial());
			report.addParameter("TITULO", this.getTitulo(filtro));
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			
			this.adicionaParametrosFiltro(report, filtro);
		}
			
		super.configurarParametros(request, filtro, report);
	}

}
