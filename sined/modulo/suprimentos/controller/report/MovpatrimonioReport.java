package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovpatrimonioFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Movpatrimonio", 
			authorizationModule=ReportAuthorizationModule.class
)
public class MovpatrimonioReport extends SinedReport<MovpatrimonioFiltro>{

	private MovpatrimonioService movpatrimonioService;
	
	public void setMovpatrimonioService(
			MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	MovpatrimonioFiltro filtro) throws Exception {
		return movpatrimonioService.gerarRelatorio(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "movpatrimonio";
	}

	@Override
	public String getTitulo(MovpatrimonioFiltro filtro) {
		return "MOVIMENTAÇÃO DE PATRIMÔNIO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, MovpatrimonioFiltro filtro) {
		
	}

}
