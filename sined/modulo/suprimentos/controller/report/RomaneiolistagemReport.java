package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RomaneioFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Romaneiolistagem", 
			authorizationModule=ReportAuthorizationModule.class
)
public class RomaneiolistagemReport extends SinedReport<RomaneioFiltro>{

	private RomaneioService romaneioService;
	
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	RomaneioFiltro filtro) throws Exception {
		return romaneioService.createRelatorioRomaneio(request, filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "romaneio";
	}
	
	@Override
	public String getTitulo(RomaneioFiltro filtro) {
		if(filtro.getContrato() != null){
			return "EXTRATO DE LOCA��O";
		} else {
			return "ROMANEIO";
		}
	}
}
