package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.OrdemcompraFiltro;

@Controller(path = "/suprimento/relatorio/Ordemcompraemissao", authorizationModule=ReportAuthorizationModule.class)
public class OrdemcompraemissaoReport extends ResourceSenderController<OrdemcompraFiltro>{

	private OrdemcompraService ordemcompraService;
	
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	OrdemcompraFiltro filtro) throws Exception {
		filtro.setPodeVisualizarReportEmTela(!"true".equals(request.getParameter("imprimirDireto")));
		String whereIn = request.getParameter("selectedItens");
		return ordemcompraService.emitirOrdemCompra(whereIn);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, OrdemcompraFiltro filtro) throws Exception {
		return null;
	}

}
