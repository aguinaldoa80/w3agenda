package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.SolicitacaocompraFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Solicitacaocompra", 
			authorizationModule=ReportAuthorizationModule.class
)
public class SolicitacaocompraReport extends SinedReport<SolicitacaocompraFiltro>{

	private SolicitacaocompraService solicitacaocompraService;
	
	public void setSolicitacaocompraService(
			SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	SolicitacaocompraFiltro filtro) throws Exception {
		return solicitacaocompraService.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "solicitacaocompra";
	}
	
	@Override
	public String getTitulo(SolicitacaocompraFiltro filtro) {
		return "SOLICITAÇÃO DE COMPRA";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, SolicitacaocompraFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getProjeto() != null){
			Projeto projeto = getProjetoService().load(filtro.getProjeto(), "projeto.nome");
			report.addParameter("projeto", projeto.getNome());
		}
		if(filtro.getEmpresa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDatalimitede(), filtro.getDatalimiteate()));
	}

}
