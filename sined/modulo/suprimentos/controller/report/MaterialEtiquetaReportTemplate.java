package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumTipoEmissao;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.MaterialEtiquetaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltroReportTemplate;
import br.com.linkcom.sined.util.SinedUtil;


@Controller(path = "/suprimento/relatorio/MaterialEtiquetaTemplate", authorizationModule=ReportAuthorizationModule.class)
public class MaterialEtiquetaReportTemplate extends ReportTemplateController<MaterialFiltroReportTemplate> {
	
	private MaterialService materialService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.ETIQUETA_MATERIAL;
	}

	@Override
	protected String getFileName() {
		return "etiqueta material";
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, MaterialFiltroReportTemplate filtro) throws Exception {
		try{
			ReportTemplateBean bean = getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_MATERIAL);
			if(request.getParameter("templateSelecionado") != null && !request.getParameter("templateSelecionado").equals("")){
				bean  = getReportTemplateService().loadTemplateByCategoriaAndNome(EnumCategoriaReportTemplate.ETIQUETA_MATERIAL, request.getParameter("templateSelecionado"));
			}
			if(EnumTipoEmissao.W3ERP.equals(bean.getTipoEmissao())){
				try{
					Resource recurso = getResource(request, filtro, "application/remote_printing", "w3erp");
					if (recurso == null) {
						return goToAction(FILTRO);
					}
					
					HttpServletResponse response = request.getServletResponse();
					response.setContentType(recurso.getContentType());
					response.addHeader("Content-Disposition", "attachment; filename=\"" + recurso.getFileName() +"\";");
					response.getOutputStream().write(recurso.getContents());
					
				} catch (Exception e) {
					throw new Exception(e.getMessage(),e.getCause());
				}			
				return null;
			}else {
				ModelAndView model = super.doGerar(request, filtro);
				filtro.getListaetiquetamaterial().clear();
				return model;
			}
		}catch (NullPointerException e) {
			request.addError("Template n�o encontrado");
			return doFiltro(request, filtro);
		}catch (Exception e) {
			request.addError(e.getMessage()+" " +e.getCause());
			return doFiltro(request, filtro);
		}
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	MaterialFiltroReportTemplate filtro) {
		return getReportTemplateService().load(filtro.getTipoetiquetamaterialconfiguravel());
	}
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext  request, MaterialFiltroReportTemplate filtro) throws Exception {
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();
		mapa.put("lista", materialService.geraListaEtiquetaMaterialBean(filtro));
		return mapa;
	}
	
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, MaterialFiltroReportTemplate filtro) throws Exception {
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if(!parametrogeralService.getBoolean(Parametrogeral.VALIDAR_EAN13_ETIQUETAMATERIAL))
			return;
		MaterialFiltroReportTemplate filtro = (MaterialFiltroReportTemplate)obj;
		for(int i = 0; i < filtro.getListaetiquetamaterial().size(); i++){
			MaterialEtiquetaBean materialEtq = filtro.getListaetiquetamaterial().get(i);
			List<Material> listamaterial = materialService.buscarMaterialForEtiqueta(materialEtq.getMaterial().getCdmaterial().toString());
			Material material = listamaterial.get(0);
			if(StringUtils.isBlank(material.getCodigobarras()) ||
				(!material.getCodigobarras().matches("^[0-9]+$")) ||
				material.getCodigobarras().length() != 13){
				
				String cdmaterial = material.getCdmaterial() != null ? material.getCdmaterial().toString() : "";
				errors.reject("001", "N�o foi poss�vel emitir etiqueta EAN13 - o n�mero de c�digo de barras do produto "+cdmaterial+" - " + material.getNome() + " � inv�lido.");
				filtro.getListaetiquetamaterial().clear();
			}
		}
	}
}
