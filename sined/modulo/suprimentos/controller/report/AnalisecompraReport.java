package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.AnalisecompraFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Analisecompra", 
			authorizationModule=ReportAuthorizationModule.class
)
public class AnalisecompraReport extends SinedReport<AnalisecompraFiltro>{

	private OrdemcompraService ordemcompraService;
	private ContagerencialService contagerencialService;
		
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) { this.ordemcompraService = ordemcompraService;	}
	public void setContagerencialService(ContagerencialService contagerencialService) { this.contagerencialService = contagerencialService;	}
		
	@Override
	public IReport createReportSined(WebRequestContext request,	AnalisecompraFiltro filtro) throws Exception {

		if(request.getParameter("sintetico") != null && "true".equals(request.getParameter("sintetico"))){
			return ordemcompraService.gerarAnaliseCompraSinteticoRelatorio(filtro);
		}
		return ordemcompraService.gerarAnaliseCompraRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "analisecompra";
	}
	
	@Override
	public String getTitulo(AnalisecompraFiltro filtro) {
		return "AN�LISE DE COMPRA";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, AnalisecompraFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getProjeto() != null){
			Projeto projeto = getProjetoService().load(filtro.getProjeto(), "projeto.nome");
			report.addParameter("projeto", projeto.getNome());
		}
		if(filtro.getEmpresa() != null){
			report.addParameter("empresafiltro", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
//		if(filtro.getMaterial() != null){
//			Material material = materialService.load(filtro.getMaterial(), "material.nome");
//			report.addParameter("material", material.getNome());
//		}
		if(filtro.getContagerencial() != null){
			Contagerencial contagerencial = contagerencialService.load(filtro.getContagerencial(), "contagerencial.nome");
			report.addParameter("contagerencial", contagerencial.getNome());
		}
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}

}
