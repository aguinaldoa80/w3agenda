package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.InventarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/InventarioCSV", authorizationModule = ReportAuthorizationModule.class)
public class InventarioCSVReport extends SinedReport<InventarioFiltro> {
	
	InventarioService inventarioService;
	
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			InventarioFiltro filtro) throws Exception {
		throw new SinedException("M�todo n�o implementado.");
	}

	@Override
	public Resource generateResource(WebRequestContext request,
			InventarioFiltro filtro) throws Exception {
		return inventarioService.generateCsvReport(filtro, getNomeArquivo());
	}
	
	@Override
	public String getNomeArquivo() {
		return "inventario.csv";
	}

	@Override
	public String getTitulo(InventarioFiltro filtro) {
		return null;
	}
	
}
