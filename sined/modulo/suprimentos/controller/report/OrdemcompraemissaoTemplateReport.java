package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.OrdemcompraemissaoFiltroReportTemplate;
import br.com.linkcom.sined.util.SinedException;

@Controller(path = "/suprimento/relatorio/Ordemcompraemissaotemplate", authorizationModule=ReportAuthorizationModule.class)
public class OrdemcompraemissaoTemplateReport  extends ReportTemplateController<OrdemcompraemissaoFiltroReportTemplate>{

	private OrdemcompraService ordemcompraService;
	
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_ORDEMCOMPRA;
	}

	@Override
	protected String getFileName() {
		return "ordemcompra";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	OrdemcompraemissaoFiltroReportTemplate filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_ORDEMCOMPRA);
	}
	
	@Override
	public Resource getPdfResource(WebRequestContext request, OrdemcompraemissaoFiltroReportTemplate filtro) throws Exception {
		String selectedItens = request.getParameter("selectedItens");
		if (StringUtils.isNotBlank(selectedItens)) {
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			List<Ordemcompra> ordens = ordemcompraService.findOrdemCompraWithEmpresa(selectedItens);
			for (Ordemcompra ordemcompra : ordens) {
				if(ordemcompra.getEmpresa() != null && !listaEmpresa.contains(ordemcompra.getEmpresa())){
					listaEmpresa.add(ordemcompra.getEmpresa());
				}
			}
			
			if(listaEmpresa.size() == 1){
				filtro.setEmpresa(listaEmpresa.get(0));
			}else {
				filtro.setEmpresa(null);
			}
		}
		return super.getPdfResource(request, filtro);
	}

	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext  request, OrdemcompraemissaoFiltroReportTemplate filtro) throws Exception {
		LinkedHashMap<String, Object> mapa =  new LinkedHashMap<String, Object>();

		String selectedItens = request.getParameter("selectedItens");
		if (StringUtils.isBlank(selectedItens)) {
			Object attribute = request.getAttribute("selectedItensEnvioOrdemcompra");
			selectedItens = attribute != null ? attribute.toString() : null;
			
			if (StringUtils.isBlank(selectedItens)) {
				throw new SinedException("Nenhum item selecionado.");
			}
		}
		mapa.put("lista", ordemcompraService.gerarOrdemCompraBean(selectedItens));
		return mapa;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, OrdemcompraemissaoFiltroReportTemplate filtro) throws Exception {
		return new ModelAndView("redirect:/suprimento/crud/Ordemcompra");
	}

}
