package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioBean;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/suprimento/relatorio/RomaneioTemplate", authorizationModule=ReportAuthorizationModule.class)
public class RomaneioTemplateReport extends ReportTemplateController<ReportTemplateFiltro> {

	private RomaneioService romaneioService;
	
	public void setRomaneioService(RomaneioService romaneioService) {this.romaneioService = romaneioService;}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.ROMANEIO;
	}

	@Override
	protected String getFileName() {
		return "romaneio";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		if(filtro != null && filtro.getTemplate() != null && filtro.getTemplate().getCdreporttemplate() != null){
			return getReportTemplateService().load(filtro.getTemplate());
		}
		
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.ROMANEIO);
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {	
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		
		Empresa empresa = null;
		Object empresaFiltro = request.getAttribute("empresaFiltro");
		if(empresaFiltro != null){
			empresa = empresaService.loadComArquivo((Empresa)empresaFiltro);
		} else {
			empresa = empresaService.loadArquivoPrincipal();
		}
		map.put("logo", SinedUtil.getLogoURLForReport(empresa));
		map.put("empresa", empresa);
		
		LinkedList<RomaneioBean> romaneioBeanList = new LinkedList<RomaneioBean>(romaneioService.getRomaneioBeanForTemplate(request.getParameter("selectedItens")));
		
		map.put("lista", romaneioBeanList);
		
		return map;
	}
}
