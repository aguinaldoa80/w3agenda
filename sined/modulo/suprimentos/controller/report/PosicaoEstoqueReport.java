package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import java.awt.Image;
import java.util.List;

import javax.servlet.http.Cookie;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PosicaoEstoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.PosicaoEstoqueBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/PosicaoEstoque", authorizationModule=ReportAuthorizationModule.class)
public class PosicaoEstoqueReport extends SinedReport<PosicaoEstoqueFiltro>{
	
	private LoteestoqueService loteestoqueService;
	
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}

	@Override
	public IReport createReportSined(WebRequestContext request, PosicaoEstoqueFiltro filtro) throws Exception {
		List<PosicaoEstoqueBean> listaRelatorioEstoque = loteestoqueService.findForRelatorioPosicaoEstoque(filtro);
		
		Report report = new Report("/suprimento/posicaoEstoque");
		
		if(filtro.getEmpresa() != null){
			report.addParameter("EMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}else{
			report.addParameter("EMPRESA", null);
		}
		
		if(filtro.getFornecedorLote() != null){
			report.addParameter("FORNECEDOR", filtro.getFornecedorLote().getNome());
		}else{
			report.addParameter("FORNECEDOR", null);
		}
		
		if(filtro.getLocalarmazenagem() != null){
			report.addParameter("LOCALARMAZENAGEM", filtro.getLocalarmazenagem().getNome());
		}else{
			report.addParameter("LOCALARMAZENAGEM", null);
		}
		
		if(filtro.getMaterialgrupo() != null){
			report.addParameter("MATERIALGRUPO", filtro.getMaterialgrupo().getNome());
		}else{
			report.addParameter("MATERIALGRUPO", null);
		}
		
		if(filtro.getMaterialtipo() != null){
			report.addParameter("MATERIALTIPO", filtro.getMaterialtipo().getNome());
		}else{
			report.addParameter("MATERIALTIPO", null);
		}
		
		if(filtro.getMaterialcategoria() != null){
			report.addParameter("MATERIALCATEGORIA", filtro.getMaterialcategoria().getDescricao());
		}else{
			report.addParameter("MATERIALCATEGORIA", null);
		}
		
		if(filtro.getMaterial() != null){
			report.addParameter("MATERIAL", filtro.getMaterial().getNome());
		}else{
			report.addParameter("MATERIAL", null);
		}
		
		if(filtro.getLoteestoque() != null){
			report.addParameter("LOTEESTOQUE", filtro.getLoteestoque().getNumero());
		}else{
			report.addParameter("LOTEESTOQUE", null);
		}
		
		if(filtro.getFabricanteMaterial() != null){
			report.addParameter("FABRICANTE", filtro.getFabricanteMaterial().getNome());
		}else{
			report.addParameter("FABRICANTE", null);
		}
		
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("TITULO", "RELAT�RIO DE POSI��O DE ESTOQUE");
		Image image = SinedUtil.getLogo(filtro.getEmpresa());
		report.addParameter("LOGO", image);

		
		report.setDataSource(listaRelatorioEstoque);
		request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		return report;
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, PosicaoEstoqueFiltro filtro) throws ResourceGenerationException {
		return super.doFiltro(request, new PosicaoEstoqueFiltro());
	}

	@Override
	public String getNomeArquivo() {
		return "posicaoEstoque";
	}

	@Override
	public String getTitulo(PosicaoEstoqueFiltro filtro) {
		return "Relat�rio de Posi��o de Estoque";
	}
	
	public ModelAndView gerarCsv(WebRequestContext request, PosicaoEstoqueFiltro filtro){
		List<PosicaoEstoqueBean> listaRelatorioEstoque = loteestoqueService.findForRelatorioPosicaoEstoque(filtro);
		
		Resource resource = loteestoqueService.gerarRelatorioPosicaoEstoqueCsv(listaRelatorioEstoque, filtro);
		request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		
		return new ResourceModelAndView(resource);
	}

}
