package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.CotacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Cotacao", 
			authorizationModule=ReportAuthorizationModule.class
)
public class CotacaoReport extends SinedReport<CotacaoFiltro>{

	private CotacaoService cotacaoService;
	
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	CotacaoFiltro filtro) throws Exception {
		return cotacaoService.gerarRelatorio(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "cotacao";
	}

	@Override
	public String getTitulo(CotacaoFiltro filtro) {
		return "Cota��o";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, CotacaoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}

}
