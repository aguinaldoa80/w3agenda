package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import java.awt.Image;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RelatorioLoteEstoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.RelatorioLoteEstoqueBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/RelatorioLoteEstoque", authorizationModule=ReportAuthorizationModule.class)
public class RelatorioLoteEstoqueReport extends SinedReport<RelatorioLoteEstoqueFiltro>{
	
	private LoteestoqueService loteestoqueService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	private MaterialcategoriaService materialcategoriaService;
	private MaterialService materialService;
	
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {this.materialgrupoService = materialgrupoService;}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {this.materialtipoService = materialtipoService;}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	
	@Override
	public IReport createReportSined(WebRequestContext request, RelatorioLoteEstoqueFiltro filtro) throws Exception {
		List<RelatorioLoteEstoqueBean> listaRelatorioEstoque = loteestoqueService.findForRelatorioEstoque(filtro);
		
		Report report = new Report("/suprimento/relatorioLoteEstoque");
		
		if(filtro.getEmpresa() != null){
			report.addParameter("EMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}else{
			report.addParameter("EMPRESA", null);
		}
		
		if(filtro.getFornecedorLote() != null){
			report.addParameter("FORNECEDOR", filtro.getFornecedorLote().getNome());
		}else{
			report.addParameter("FORNECEDOR", null);
		}
		
		if(filtro.getLocalarmazenagem() != null){
			report.addParameter("LOCALARMAZENAGEM", filtro.getLocalarmazenagem().getNome());
		}else{
			report.addParameter("LOCALARMAZENAGEM", null);
		}
		
		if(filtro.getMaterialgrupo() != null){
			filtro.setMaterialgrupo(materialgrupoService.load(filtro.getMaterialgrupo(), "materialgrupo.nome"));
			report.addParameter("MATERIALGRUPO", filtro.getMaterialgrupo().getNome());
		}else{
			report.addParameter("MATERIALGRUPO", null);
		}
		
		if(filtro.getMaterialtipo() != null){
			filtro.setMaterialtipo(materialtipoService.load(filtro.getMaterialtipo(), "materialtipo.nome"));
			report.addParameter("MATERIALTIPO", filtro.getMaterialtipo().getNome());
		}else{
			report.addParameter("MATERIALTIPO", null);
		}
		
		if(filtro.getMaterialcategoria() != null){
			filtro.setMaterialcategoria(materialcategoriaService.load(filtro.getMaterialcategoria(), "materialcategoria.cdmaterialcategoria, materialcategoria.descricao"));
			report.addParameter("MATERIALCATEGORIA", filtro.getMaterialcategoria().getDescricao());
		}else{
			report.addParameter("MATERIALCATEGORIA", null);
		}
		
		if(filtro.getMaterial() != null){
			filtro.setMaterial(materialService.load(filtro.getMaterial(), "material.nome"));
			report.addParameter("MATERIAL", filtro.getMaterial().getNome());
		}else{
			report.addParameter("MATERIAL", null);
		}
		
		if(filtro.getLoteestoque() != null){
			report.addParameter("LOTEESTOQUE", filtro.getLoteestoque().getNumero());
		}else{
			report.addParameter("LOTEESTOQUE", null);
		}
		
		if(filtro.getFabricanteMaterial() != null){
			report.addParameter("FABRICANTE", filtro.getFabricanteMaterial().getNome());
		}else{
			report.addParameter("FABRICANTE", null);
		}
		
		List<Loteestoque> listaLotes = new ArrayList<Loteestoque>();
		for (RelatorioLoteEstoqueBean relatorioLoteEstoqueBean : listaRelatorioEstoque) {
			Loteestoque lote = new Loteestoque(relatorioLoteEstoqueBean.getCdlote());
			if(!listaLotes.contains(lote)){
				listaLotes.add(lote);
			}			
		}
		
		report.addParameter("TOTAL_LOTES", listaLotes.size());
		
		Date dataAtual = new Date(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String data = sdf.format(dataAtual);
		
		report.addParameter("DATAATUAL", data);
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("TITULO", "RELATÓRIO DE LOTE DE ESTOQUE");
		Image image = SinedUtil.getLogo(filtro.getEmpresa());
		report.addParameter("LOGO", image);

		report.setDataSource(listaRelatorioEstoque);
		
		request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		return report;
	}

	@Override
	public String getNomeArquivo() {
		return "relatorioLoteEstoque";
	}

	@Override
	public String getTitulo(RelatorioLoteEstoqueFiltro filtro) {
		return "Relatório de Lote de Estoque";
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, RelatorioLoteEstoqueFiltro filtro) throws ResourceGenerationException {
		// TODO Auto-generated method stub
		return super.doFiltro(request, filtro);
	}
	
	public ModelAndView gerarCsv(WebRequestContext request, RelatorioLoteEstoqueFiltro filtro){
		List<RelatorioLoteEstoqueBean> listaRelatorioEstoque = loteestoqueService.findForRelatorioEstoque(filtro);
		
		Resource resource = loteestoqueService.gerarRelatorioLoteEstoqueCsv(listaRelatorioEstoque, filtro);
		
		request.getServletResponse().addCookie(new Cookie("blockbutton", "false"));
		return new ResourceModelAndView(resource);
	}

}
