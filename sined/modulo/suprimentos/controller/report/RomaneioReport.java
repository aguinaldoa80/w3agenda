package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RomaneioFiltro;

@Controller(path = "/suprimento/relatorio/Romaneio", authorizationModule=ReportAuthorizationModule.class)
public class RomaneioReport extends ResourceSenderController<RomaneioFiltro>{

	private RomaneioService romaneioService;
	
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	RomaneioFiltro filtro) throws Exception {
		return romaneioService.gerarRelatorio(request);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, RomaneioFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/suprimento/crud/Romaneio");
	}

}
