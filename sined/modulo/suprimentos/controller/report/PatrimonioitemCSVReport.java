package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PatrimonioitemFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/PatrimonioitemCSV", authorizationModule = ReportAuthorizationModule.class)
public class PatrimonioitemCSVReport extends SinedReport<PatrimonioitemFiltro> {
	PatrimonioitemService patrimonioitemService;
	
	public void setPatrimonioitemService(
			PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
		
	@Override
	public IReport createReportSined(WebRequestContext request,
			PatrimonioitemFiltro filtro) throws Exception {
		throw new SinedException("M�todo n�o implementado.");
	}

	@Override
	public Resource generateResource(WebRequestContext request,
			PatrimonioitemFiltro filtro) throws Exception {
		return patrimonioitemService.generateCsvReport(filtro, getNomeArquivo());
	}
	
	@Override
	public String getNomeArquivo() {
		return "item_de_patrimonio.csv";
	}

	@Override
	public String getTitulo(PatrimonioitemFiltro filtro) {
		return null;
	}

}
