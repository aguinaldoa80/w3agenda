package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.FornecimentoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecimentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/FornecimentoCSV", authorizationModule = ReportAuthorizationModule.class)
public class FornecimentoCSVReport extends SinedReport<FornecimentoFiltro> {
	FornecimentoService fornecimentoService;
	
	public void setFornecimentoService(FornecimentoService fornecimentoService) {
		this.fornecimentoService = fornecimentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			FornecimentoFiltro filtro) throws Exception {
		throw new SinedException("M�todo n�o implementado.");
	}

	@Override
	public Resource generateResource(WebRequestContext request,
			FornecimentoFiltro filtro) throws Exception {
		return fornecimentoService.generateCsvReport(filtro, getNomeArquivo());
	}
	
	@Override
	public String getNomeArquivo() {
		return "fornecimento.csv";
	}

	@Override
	public String getTitulo(FornecimentoFiltro filtro) {
		return null;
	}

}
