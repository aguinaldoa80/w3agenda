package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EntregaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Entrega", 
			authorizationModule=ReportAuthorizationModule.class
)
public class EntregaReport extends SinedReport<EntregaFiltro>{

	private EntregaService entregaService;
	
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	EntregaFiltro filtro) throws Exception {
		return entregaService.gerarRelatorio(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "entrega";
	}

	@Override
	public String getTitulo(EntregaFiltro filtro) {
		return "ENTREGA";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EntregaFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		report.addParameter("periodoentrega", SinedUtil.getDescricaoPeriodo(filtro.getDtentrega1(), filtro.getDtentrega2()));
		report.addParameter("periodoemissao", SinedUtil.getDescricaoPeriodo(filtro.getDtemissao1(), filtro.getDtemissao2()));
	}

}
