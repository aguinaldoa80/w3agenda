package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovpatrimonioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/MovpatrimonioCSV", authorizationModule = ReportAuthorizationModule.class)
public class MovpatrimonioCSVReport extends SinedReport<MovpatrimonioFiltro> {
	MovpatrimonioService movpatrimonioService;
	
	public void setMovpatrimonioService(
			MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			MovpatrimonioFiltro filtro) throws Exception {
		throw new SinedException("M�todo n�o implementado.");
	}

	@Override
	public Resource generateResource(WebRequestContext request,
			MovpatrimonioFiltro filtro) throws Exception {
		return movpatrimonioService.generateCsvReport(filtro, getNomeArquivo());
	}
	
	@Override
	public String getNomeArquivo() {
		return "movimentacao_de_patrimonio.csv";
	}

	@Override
	public String getTitulo(MovpatrimonioFiltro filtro) {
		return "";
	}

}
