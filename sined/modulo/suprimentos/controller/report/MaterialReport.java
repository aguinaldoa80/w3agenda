package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEtiquetaMaterial;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/Material", authorizationModule=ReportAuthorizationModule.class)
public class MaterialReport extends SinedReport<MaterialFiltro>{

	private MaterialService materialService;

	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	MaterialFiltro filtro) throws Exception {
		
		if (filtro.getTipoetiquetamaterialpredifinido() != null && (filtro.getTipoetiquetamaterialpredifinido().equals(TipoEtiquetaMaterial.ETIQUETA_CBEAN13)
			||	filtro.getTipoetiquetamaterialpredifinido().equals(TipoEtiquetaMaterial.ETIQUETA_PIMACO	))) {
			return materialService.gerarEtiqueta(filtro);
		}else {
			return materialService.gerarRelatorio(filtro);
		}
	}
	
	@Override
	public String getTitulo(MaterialFiltro filtro) {
		return "MATERIAIS E SERVI�OS";
	}
	
	@Override
	public String getNomeArquivo() {
		return "material";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, MaterialFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getEmpresa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,MaterialFiltro filtro) throws ResourceGenerationException {
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
}
