package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.CotacaoFiltro;

@Controller(path = "/suprimento/relatorio/Cotacaoemissao", authorizationModule=ReportAuthorizationModule.class)
public class CotacaoemissaoReport extends ResourceSenderController<CotacaoFiltro>{

	private CotacaoService cotacaoService;
	
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,	CotacaoFiltro filtro) throws Exception {
		return cotacaoService.emitirCotacao(request);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, CotacaoFiltro filtro) throws Exception {
		return null;
	}

}
