package br.com.linkcom.sined.modulo.suprimentos.controller.report;


import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.ClassificacaoCurvaAbc;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitircurvaabcFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/Emitircurvaabc", authorizationModule=ReportAuthorizationModule.class)
public class EmitircurvaabcReport extends SinedReport<EmitircurvaabcFiltro>{

	private EntregaService entregaService;
		
	public void setEntregaService(EntregaService entregaService) { this.entregaService = entregaService;	}
		
	@Override
	public IReport createReportSined(WebRequestContext request, EmitircurvaabcFiltro filtro) throws Exception {
		return entregaService.gerarRelatorioCurvaABC(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "emitircurvaabc";
	}

	@Override
	public String getTitulo(EmitircurvaabcFiltro filtro) {
		return "CURVA ABC";
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, EmitircurvaabcFiltro filtro) throws ResourceGenerationException {
		List<ClassificacaoCurvaAbc> listaClassificacao = new ArrayList<ClassificacaoCurvaAbc>();
		listaClassificacao.add(ClassificacaoCurvaAbc.VALOR);
		listaClassificacao.add(ClassificacaoCurvaAbc.QUANTIDADE);
		request.setAttribute("listaClassificacao", listaClassificacao);
		return super.doFiltro(request, filtro);
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EmitircurvaabcFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		if(filtro.getEmpresa() != null){
			report.addParameter("NOMEEMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		
		String nomeprojeto = "";
		if(filtro.getProjeto() != null){
			Projeto projeto = getProjetoService().load(filtro.getProjeto(), "projeto.cdprojeto, projeto.nome");
			nomeprojeto = projeto.getNome();
		}
		report.addParameter("NOMEPROJETO", nomeprojeto);
		
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
		
	}

}

