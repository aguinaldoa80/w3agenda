package br.com.linkcom.sined.modulo.suprimentos.controller.report;


import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitirprazomediocompraFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/suprimento/relatorio/Emitirprazomediocompra", authorizationModule=ReportAuthorizationModule.class)
public class EmitirprazomediocompraReport extends ResourceSenderController<EmitirprazomediocompraFiltro>{

	private SolicitacaocompraService solicitacaocompraService;
		
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, EmitirprazomediocompraFiltro filtro) throws Exception {
		SinedUtil.setCookieBlockButton(request);
		return solicitacaocompraService.gerarRelatorioPrazomediocompra(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, EmitirprazomediocompraFiltro filtro) throws Exception {
		return new ModelAndView("relatorio/emitirprazomediocompra", "filtro", filtro);
	}
	
}

