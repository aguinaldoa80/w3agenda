package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LoteestoqueFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/LoteestoqueCSV", authorizationModule = ReportAuthorizationModule.class)
public class LoteestoqueCSVReport extends SinedReport<LoteestoqueFiltro> {
	LoteestoqueService loteestoqueService;
	
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) { this.loteestoqueService = loteestoqueService; }
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			LoteestoqueFiltro filtro) throws Exception {
		throw new SinedException("M�todo n�o implementado.");
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,
			LoteestoqueFiltro filtro) throws Exception {
		return loteestoqueService.generateCsvReport(filtro, getNomeArquivo());
	}
	
	@Override
	public String getNomeArquivo() {
		return "lote.csv";
	}

	@Override
	public String getTitulo(LoteestoqueFiltro filtro) {
		return "Lote";
	}
	
}
