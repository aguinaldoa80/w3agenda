package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.SolicitacaocompraFiltro;

@Controller(path = "/suprimento/relatorio/Solicitacaocompraemissao", authorizationModule=ReportAuthorizationModule.class)
public class SolicitacaocompraemissaoReport extends ResourceSenderController<SolicitacaocompraFiltro>{

	private SolicitacaocompraService solicitacaocompraService;
	
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	SolicitacaocompraFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		String mesmaSituacao = request.getParameter("mesmaSituacao");
		return solicitacaocompraService.emitirSolicitacaoCompra(whereIn, mesmaSituacao != null && mesmaSituacao.toUpperCase().equals("TRUE") ? Boolean.TRUE : Boolean.FALSE);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, SolicitacaocompraFiltro filtro) throws Exception {
		return null;
	}

}
