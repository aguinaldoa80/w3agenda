package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.LocacaomaterialService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LocacaomaterialFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Locacaomaterial", 
			authorizationModule=ReportAuthorizationModule.class
)
public class LocacaomaterialReport extends SinedReport<LocacaomaterialFiltro>{

	private LocacaomaterialService locacaomaterialService;
	
	public void setLocacaomaterialService(
			LocacaomaterialService locacaomaterialService) {
		this.locacaomaterialService = locacaomaterialService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	LocacaomaterialFiltro filtro) throws Exception {
		return locacaomaterialService.gerarRelatorio(filtro);
	}

	@Override
	public String getTitulo(LocacaomaterialFiltro filtro) {
		return "CONTROLE DE LOCA��ES";
	}
	
	@Override
	public String getNomeArquivo() {
		return "locacaomaterial";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, LocacaomaterialFiltro filtro) {
		
	}

}
