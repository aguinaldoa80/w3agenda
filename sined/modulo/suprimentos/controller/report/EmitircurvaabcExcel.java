package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitircurvaabcFiltro;

@Controller(path = "/suprimento/relatorio/EmitircurvaabcExcel", authorizationModule=ProcessAuthorizationModule.class)
public class EmitircurvaabcExcel extends ResourceSenderController<EmitircurvaabcFiltro>{
	private EntregaService entregaService;
	
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	EmitircurvaabcFiltro filtro) throws Exception {
		return entregaService.gerarExcelEmitircurvaabc(filtro, request);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	EmitircurvaabcFiltro filtro) throws Exception {
		return  new ModelAndView("relatorio/emitircurvaabc").addObject("filtro", filtro);
	}

}
