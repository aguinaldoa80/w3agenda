package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.RequisicaomaterialService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RequisicaomaterialFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;



@Controller(
			path = "/suprimento/relatorio/Requisicaomaterial", 
			authorizationModule=ReportAuthorizationModule.class
)
public class RequisicaomaterialReport extends SinedReport<RequisicaomaterialFiltro>{
	private RequisicaomaterialService requisicaomaterialService;
		
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {
		this.requisicaomaterialService = requisicaomaterialService;
	}

	
	@Override
	public IReport createReportSined(WebRequestContext request,	RequisicaomaterialFiltro filtro) throws Exception {
		return requisicaomaterialService.gerarRelatorio(filtro);
	}

	@Override
	public String getTitulo(RequisicaomaterialFiltro filtro) {
		return "Requisição de material";
	}
	
	@Override
	public String getNomeArquivo() {
		return "requisicaomaterial";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, RequisicaomaterialFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getProjeto() != null){
			Projeto projeto = getProjetoService().load(filtro.getProjeto(), "projeto.nome");
			report.addParameter("projeto", projeto.getNome());
		}
		if(filtro.getEmpresa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDatalimitede(), filtro.getDatalimiteate()));
	}
}
