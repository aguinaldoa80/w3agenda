package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.GerenciamentomaterialService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.GerenciamentomaterialFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/Gerenciamentomaterial", authorizationModule=ReportAuthorizationModule.class)
public class GerenciamentomaterialReport extends SinedReport<GerenciamentomaterialFiltro>{

	private GerenciamentomaterialService gerenciamentomaterialService;
	
	public void setGerenciamentomaterialService(GerenciamentomaterialService gerenciamentomaterialService) {
		this.gerenciamentomaterialService = gerenciamentomaterialService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	GerenciamentomaterialFiltro filtro) throws Exception {
		if(request.getParameter("porperiodo") != null && "true".equals(request.getParameter("porperiodo"))){
			return gerenciamentomaterialService.gerarRelatorioPorPeriodo(filtro);
		}
		return gerenciamentomaterialService.gerarRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "gerenciamentomaterial";
	}
	
	@Override
	public String getTitulo(GerenciamentomaterialFiltro filtro) {
		return "GERENCIAMENTO DE MATERIAL";
	}
	
}
