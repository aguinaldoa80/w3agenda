package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovimentacaoestoqueFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/Movimentacaoestoque",authorizationModule=ReportAuthorizationModule.class
)
public class MovimentacaoestoqueReport extends SinedReport<MovimentacaoestoqueFiltro>{

	private MovimentacaoestoqueService movimentacaoestoqueService;
	
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	MovimentacaoestoqueFiltro filtro) throws Exception {
		return movimentacaoestoqueService.gerarRelatorio(filtro);
	}

	@Override
	public String getTitulo(MovimentacaoestoqueFiltro filtro) {
		return "ENTRADAS E SA�DAS";
	}
	
	@Override
	public String getNomeArquivo() {
		return "entradasaida";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, MovimentacaoestoqueFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getEmpresa() != null){
			report.addParameter("empresa", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDatade(), filtro.getDataate()));
	}

}
