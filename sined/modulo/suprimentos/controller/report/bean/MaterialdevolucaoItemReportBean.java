package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;

public class MaterialdevolucaoItemReportBean {

	private Integer material_codigo;
	private String material_nome;
	private Double quantidade_devolvida;
	private Double preco;
	
	public MaterialdevolucaoItemReportBean() {
	}
	
	public MaterialdevolucaoItemReportBean(MaterialdevolucaoQueryBean itQuery) {
		this.material_codigo = itQuery.getMaterial_codigo();
		this.material_nome = itQuery.getMaterial_nome();
		this.quantidade_devolvida = itQuery.getQuantidade_devolvida();
		this.preco = itQuery.getPreco();
	}

	public Integer getMaterial_codigo() {
		return material_codigo;
	}
	public String getMaterial_nome() {
		return material_nome;
	}
	public Double getQuantidade_devolvida() {
		return quantidade_devolvida;
	}
	public Double getPreco() {
		return preco;
	}
	public void setMaterial_codigo(Integer material_codigo) {
		this.material_codigo = material_codigo;
	}
	public void setMaterial_nome(String material_nome) {
		this.material_nome = material_nome;
	}
	public void setQuantidade_devolvida(Double quantidade_devolvida) {
		this.quantidade_devolvida = quantidade_devolvida;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	public Double getValor_devolvido(){
		return (quantidade_devolvida != null ? quantidade_devolvida : 0d) * (preco != null ? preco : 0d);
	}
	
}
