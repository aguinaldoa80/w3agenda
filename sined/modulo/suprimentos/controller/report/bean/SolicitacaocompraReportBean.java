package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;

public class SolicitacaocompraReportBean {

	private String ident;
	private String materialservico;
	private String qtde;
	private String projdep;
	private String local;
	private String solicitante;
	private String datasolicitacao;
	private String dataautorizacao;
	private String datanecessidade;
	private String observacao;
	private String situacao;
	
	public String getIdent() {
		return ident;
	}
	public String getMaterialservico() {
		return materialservico;
	}
	public String getQtde() {
		return qtde;
	}
	public String getProjdep() {
		return projdep;
	}
	public String getLocal() {
		return local;
	}
	public String getSolicitante() {
		return solicitante;
	}
	public String getDatasolicitacao() {
		return datasolicitacao;
	}
	public String getDataautorizacao() {
		return dataautorizacao;
	}
	public String getDatanecessidade() {
		return datanecessidade;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setIdent(String ident) {
		this.ident = ident;
	}
	public void setMaterialservico(String materialservico) {
		this.materialservico = materialservico;
	}
	public void setQtde(String qtde) {
		this.qtde = qtde;
	}
	public void setProjdep(String projdep) {
		this.projdep = projdep;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}
	public void setDatasolicitacao(String datasolicitacao) {
		this.datasolicitacao = datasolicitacao;
	}
	public void setDataautorizacao(String dataautorizacao) {
		this.dataautorizacao = dataautorizacao;
	}
	public void setDatanecessidade(String datanecessidade) {
		this.datanecessidade = datanecessidade;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
}
