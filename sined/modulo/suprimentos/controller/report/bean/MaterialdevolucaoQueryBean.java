package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;


public class MaterialdevolucaoQueryBean {
	
	private Integer cdempresa;
	private String empresa_nome;
	private String cliente_nome;
	private Integer venda_numero;
	private Integer material_codigo;
	private String material_nome;
	private Double quantidade_devolvida;
	private Double preco;
	
	public MaterialdevolucaoQueryBean(Integer cdempresa, String empresa_nome, String cliente_nome,
			Integer venda_numero, Integer material_codigo, String material_nome, 
			Double quantidade_devolvida, Double preco) {
		this.cdempresa = cdempresa;
		this.empresa_nome = empresa_nome;
		this.cliente_nome = cliente_nome;
		this.venda_numero = venda_numero;
		this.material_codigo = material_codigo;
		this.material_nome = material_nome;
		this.quantidade_devolvida = quantidade_devolvida;
		this.preco = preco;
	}
	
	public String getEmpresa_nome() {
		return empresa_nome;
	}
	public String getCliente_nome() {
		return cliente_nome;
	}
	public Integer getVenda_numero() {
		return venda_numero;
	}
	public Integer getMaterial_codigo() {
		return material_codigo;
	}
	public String getMaterial_nome() {
		return material_nome;
	}
	public Double getQuantidade_devolvida() {
		return quantidade_devolvida;
	}
	public Double getPreco() {
		return preco;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setMaterial_codigo(Integer material_codigo) {
		this.material_codigo = material_codigo;
	}
	public void setMaterial_nome(String material_nome) {
		this.material_nome = material_nome;
	}
	public void setQuantidade_devolvida(Double quantidade_devolvida) {
		this.quantidade_devolvida = quantidade_devolvida;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setEmpresa_nome(String empresa_nome) {
		this.empresa_nome = empresa_nome;
	}
	public void setCliente_nome(String cliente_nome) {
		this.cliente_nome = cliente_nome;
	}
	public void setVenda_numero(Integer venda_numero) {
		this.venda_numero = venda_numero;
	}
	
	
}
