package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MaterialdevolucaoReportBean {
	
	private Integer cdempresa;
	private String empresa_nome;
	private String cliente_nome;
	private Integer venda_numero;
	private Date data_devolucao;
	private List<MaterialdevolucaoItemReportBean> listaItens;
	
	public MaterialdevolucaoReportBean(MaterialdevolucaoQueryBean itQuery, MaterialdevolucaoItemReportBean itBean) {
		this.cdempresa = itQuery.getCdempresa();
		this.empresa_nome = itQuery.getEmpresa_nome();
		this.cliente_nome = itQuery.getCliente_nome();
		this.venda_numero = itQuery.getVenda_numero();
		this.data_devolucao = new Date(System.currentTimeMillis());
		
		this.listaItens = new ArrayList<MaterialdevolucaoItemReportBean>();
		this.listaItens.add(itBean);
	}
	
	public String getEmpresa_nome() {
		return empresa_nome;
	}
	public String getCliente_nome() {
		return cliente_nome;
	}
	public Integer getVenda_numero() {
		return venda_numero;
	}
	public Date getData_devolucao() {
		return data_devolucao;
	}
	public List<MaterialdevolucaoItemReportBean> getListaItens() {
		return listaItens;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setEmpresa_nome(String empresa_nome) {
		this.empresa_nome = empresa_nome;
	}
	public void setCliente_nome(String cliente_nome) {
		this.cliente_nome = cliente_nome;
	}
	public void setVenda_numero(Integer venda_numero) {
		this.venda_numero = venda_numero;
	}
	public void setData_devolucao(Date data_devolucao) {
		this.data_devolucao = data_devolucao;
	}
	public void setListaItens(List<MaterialdevolucaoItemReportBean> listaItens) {
		this.listaItens = listaItens;
	}
	
	public Double getValor_total_devolvido() {
		Double valor_total_devolvido = 0d;
		if(listaItens != null && listaItens.size() > 0){
			for (MaterialdevolucaoItemReportBean it : listaItens) {
				valor_total_devolvido += it.getValor_devolvido(); 
			}
		}
		return valor_total_devolvido;
	}
	
}
