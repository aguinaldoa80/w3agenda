package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.util.DoubleUtils;

public class RelatorioLoteEstoqueBean {
	private Integer cdmaterial;
	private String identificadorMaterial;
	private String nomeMaterial;
	private String unidadeMedida;
	private Integer cdlote;
	private String numeroLote;
	private Date dtValidade;
	private String fornecedorLote;
	private String materialTipo;
	private String localarmazenagem;
	private Double qtde;
	private Double qtdeReservada;
	private Double qtdeDisponivel;
	private String empresa;
	private String fabricanteMaterial;

	public RelatorioLoteEstoqueBean(Integer cdmaterial,
			String identificadorMaterial, String nomeMaterial,
			String unidadeMedida, Integer cdlote, String numeroLote,
			Date dtValidade, String fornecedorLote, String materialTipo,
			String localarmazenagem, Double qtde, Double qtdeReservada,
			String empresa, String fabricanteMaterial) {
		super();
		this.cdmaterial = cdmaterial;
		this.identificadorMaterial = identificadorMaterial;
		this.nomeMaterial = nomeMaterial;
		this.unidadeMedida = unidadeMedida;
		this.cdlote = cdlote;
		this.numeroLote = numeroLote;
		this.dtValidade = dtValidade;
		this.fornecedorLote = fornecedorLote;
		this.materialTipo = materialTipo;
		this.localarmazenagem = localarmazenagem;
		this.qtde = qtde;
		this.qtdeReservada = qtdeReservada;
		this.empresa = empresa;
		this.fabricanteMaterial = fabricanteMaterial;
	}
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public String getIdentificadorMaterial() {
		return identificadorMaterial;
	}
	public String getNomeMaterial() {
		return nomeMaterial;
	}
	public String getUnidadeMedida() {
		return unidadeMedida;
	}
	public Integer getCdlote() {
		return cdlote;
	}
	public String getNumeroLote() {
		return numeroLote;
	}
	public Date getDtValidade() {
		return dtValidade;
	}
	public String getFornecedorLote() {
		return fornecedorLote;
	}
	public String getMaterialTipo() {
		return materialTipo;
	}
	public String getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getQtdeReservada() {
		return qtdeReservada;
	}
	public Double getQtdeDisponivel() {
		qtdeDisponivel = DoubleUtils.zeroWhenNull(getQtde()) - DoubleUtils.zeroWhenNull(getQtdeReservada());
		return qtdeDisponivel;
	}
	public String getEmpresa() {
		return empresa;
	}
	public String getFabricanteMaterial() {
		return fabricanteMaterial;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setIdentificadorMaterial(String identificadorMaterial) {
		this.identificadorMaterial = identificadorMaterial;
	}
	public void setNomeMaterial(String nomeMaterial) {
		this.nomeMaterial = nomeMaterial;
	}
	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public void setCdlote(Integer cdlote) {
		this.cdlote = cdlote;
	}
	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}
	public void setDtValidade(Date dtValidade) {
		this.dtValidade = dtValidade;
	}
	public void setFornecedorLote(String fornecedorLote) {
		this.fornecedorLote = fornecedorLote;
	}
	public void setMaterialTipo(String materialTipo) {
		this.materialTipo = materialTipo;
	}
	public void setLocalarmazenagem(String localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setQtdeReservada(Double qtdeReservada) {
		this.qtdeReservada = qtdeReservada;
	}
	public void setQtdeDisponivel(Double qtdeDisponivel) {
		this.qtdeDisponivel = qtdeDisponivel;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setFabricanteMaterial(String fabricanteMaterial) {
		this.fabricanteMaterial = fabricanteMaterial;
	}

}
