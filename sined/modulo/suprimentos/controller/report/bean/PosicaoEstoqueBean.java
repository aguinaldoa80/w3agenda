package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.Util;

public class PosicaoEstoqueBean {
	private Integer cdmaterial;
	private String identificadorMaterial;
	private String nomeMaterial;
	private String unidadeMedida;
	private String materialCategoria;
	private String materialGrupo;
	private String codigoBarra;
	private Double custoUnitario;
	private Double custoTotal;
	private Integer cdlote;
	private String numeroLote;
	private Date dtValidade;
	private String fornecedor;
	private String materialTipo;
	private String localarmazenagem;
	private Double qtde;
	private Double qtdeDisponivel;
	private Double qtdeReservada;
	private String empresa;
	private String fabricanteMaterial;
	private String codigoFabricante;

	public PosicaoEstoqueBean(Integer cdmaterial,
			String identificadorMaterial, String nomeMaterial,
			String unidadeMedida, Integer cdlote, String numeroLote,
			Date dtValidade, String fornecedorLote, String materialTipo,
			String localarmazenagem, Double qtde, Double qtdeReservada,
			String empresa, String fabricanteMaterial, String materialCategoria,
			String materialGrupo, String codigoBarra, Double custoUnitario,
			Double custoTotal, String codigoFabricante) {
		super();
		this.cdmaterial = cdmaterial;
		this.identificadorMaterial = identificadorMaterial;
		this.nomeMaterial = nomeMaterial;
		this.unidadeMedida = unidadeMedida;
		this.cdlote = cdlote;
		this.numeroLote = numeroLote;
		this.dtValidade = dtValidade;
		this.fornecedor = fornecedorLote;
		this.materialTipo = materialTipo;
		this.localarmazenagem = localarmazenagem;
		this.qtde = qtde;
		this.qtdeReservada = qtdeReservada;
		this.empresa = empresa;
		this.fabricanteMaterial = fabricanteMaterial;
		this.materialCategoria = materialCategoria;
		this.materialGrupo = materialGrupo;
		this.codigoBarra = codigoBarra;
		this.custoUnitario = custoUnitario;
		this.custoTotal = custoTotal;
		this.codigoFabricante = codigoFabricante;
	}
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public String getIdentificadorMaterial() {
		return Util.strings.emptyIfNull(identificadorMaterial);
	}
	public String getNomeMaterial() {
		return Util.strings.emptyIfNull(nomeMaterial);
	}
	public String getUnidadeMedida() {
		return Util.strings.emptyIfNull(unidadeMedida);
	}
	public Integer getCdlote() {
		return cdlote;
	}
	public String getNumeroLote() {
		return Util.strings.emptyIfNull(numeroLote);
	}
	public Date getDtValidade() {
		return dtValidade;
	}
	public String getFornecedor() {
		return Util.strings.emptyIfNull(fornecedor);
	}
	public String getMaterialTipo() {
		return materialTipo;
	}
	public String getLocalarmazenagem() {
		return Util.strings.emptyIfNull(localarmazenagem);
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getQtdeDisponivel() {
		qtdeDisponivel = DoubleUtils.zeroWhenNull(getQtde()) - DoubleUtils.zeroWhenNull(getQtdeReservada());
		return qtdeDisponivel;
	}
	public Double getQtdeReservada() {
		return qtdeReservada;
	}
	public String getEmpresa() {
		return Util.strings.emptyIfNull(empresa);
	}
	public String getFabricanteMaterial() {
		return Util.strings.emptyIfNull(fabricanteMaterial);
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setIdentificadorMaterial(String identificadorMaterial) {
		this.identificadorMaterial = identificadorMaterial;
	}
	public void setNomeMaterial(String nomeMaterial) {
		this.nomeMaterial = nomeMaterial;
	}
	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public void setCdlote(Integer cdlote) {
		this.cdlote = cdlote;
	}
	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}
	public void setDtValidade(Date dtValidade) {
		this.dtValidade = dtValidade;
	}
	public void setFornecedor(String fornecedorLote) {
		this.fornecedor = fornecedorLote;
	}
	public void setMaterialTipo(String materialTipo) {
		this.materialTipo = materialTipo;
	}
	public void setLocalarmazenagem(String localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setQtdeDisponivel(Double qtdeDisponivel) {
		this.qtdeDisponivel = qtdeDisponivel;
	}
	public void setQtdeReservada(Double qtdeReservada) {
		this.qtdeReservada = qtdeReservada;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setFabricanteMaterial(String fabricanteMaterial) {
		this.fabricanteMaterial = fabricanteMaterial;
	}

	public String getMaterialCategoria() {
		return Util.strings.emptyIfNull(materialCategoria);
	}

	public void setMaterialCategoria(String materialCategoria) {
		this.materialCategoria = materialCategoria;
	}

	public String getMaterialGrupo() {
		return Util.strings.emptyIfNull(materialGrupo);
	}

	public void setMaterialGrupo(String materialGrupo) {
		this.materialGrupo = materialGrupo;
	}

	public String getCodigoBarra() {
		return Util.strings.emptyIfNull(codigoBarra);
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public Double getCustoUnitario() {
		return custoUnitario;
	}

	public void setCustoUnitario(Double custoUnitario) {
		this.custoUnitario = custoUnitario;
	}

	public Double getCustoTotal() {
		return custoTotal;
	}

	public void setCustoTotal(Double custoTotal) {
		this.custoTotal = custoTotal;
	}
	
	public String getCodigoFabricante() {
		return Util.strings.emptyIfNull(codigoFabricante);
	}
	public void setCodigoFabricante(String codigoFabricante) {
		this.codigoFabricante = codigoFabricante;
	}
}
