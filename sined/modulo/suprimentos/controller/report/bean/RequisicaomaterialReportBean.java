package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;

public class RequisicaomaterialReportBean {
	
	
	private String ident;
	private String descricao;
	private String materialservico;
	private String unidademedida;
	private String qtde;
	private String localarmazenagem;
	private String origem;
	private String datalimite;
	private String situacao;
	private String observacao;
	private String datacriacao;
	
	
	public String getIdent() {
		return ident;
	}
	public void setIdent(String ident) {
		this.ident = ident;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getMaterialservico() {
		return materialservico;
	}
	public void setMaterialservico(String materialservico) {
		this.materialservico = materialservico;
	}
	public String getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	public String getQtde() {
		return qtde;
	}
	public void setQtde(String qtde) {
		this.qtde = qtde;
	}
	public String getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(String localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDatalimite() {
		return datalimite;
	}
	public void setDatalimite(String datalimite) {
		this.datalimite = datalimite;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getDatacriacao() {
		return datacriacao;
	}
	public void setDatacriacao(String datacriacao) {
		this.datacriacao = datacriacao;
	}
}