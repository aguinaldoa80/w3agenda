package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;

public class PrazomediocompraReportBean {
	
	public static String META_ALCANCADA = "Meta Alcan�ada";
	public static String META_NAO_ALCANCADA = "Meta N�o Alcan�ada";
	public static String META_DENTRO_TOLERANCIA = "Meta dentro da Toler�ncia";
	
	public static String AUTORIZADA_DENTRO_PRAZO = "Requisi��o Autorizada Dentro do Prazo Limite para Compra";
	public static String AUTORIZADA_FORA_PRAZO = "Requisi��o Autorizada Fora do Prazo Limite";
	
	private Empresa empresa;
	private Material material;
	private Date dtsolicitacao;
	private String nomepessoasolicitante;
	private Date dtautorizacao;
	private String nomepessoaautorizacao;
	private Date dtnecessidadesolicitacao;
	private Date dtbaixaordemcompra;
	private String nomepessoabaixouordemcompra;
	private Date dtentrada;
	private Integer tempoentrega;
	private Date prazolimiteautorizacao;
	private Integer valorsituacaosolicitacao;
	private Integer valorcomprarecebidaposprazo;
	private Integer indiceprazomediocompra;
	private Double toleranciaprazocompra;
	private Double valorsituacaoordemcompra;
	private Double metaprazomediocompra;
	
	private String situacaosolicitacao;
	private String situacaocompra;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Material getMaterial() {
		return material;
	}
	public Date getDtsolicitacao() {
		return dtsolicitacao;
	}
	public String getNomepessoasolicitante() {
		return nomepessoasolicitante;
	}
	public Date getDtautorizacao() {
		return dtautorizacao;
	}
	public String getNomepessoaautorizacao() {
		return nomepessoaautorizacao;
	}
	public Date getDtnecessidadesolicitacao() {
		return dtnecessidadesolicitacao;
	}
	public Date getDtbaixaordemcompra() {
		return dtbaixaordemcompra;
	}
	public String getNomepessoabaixouordemcompra() {
		return nomepessoabaixouordemcompra;
	}
	public Date getDtentrada() {
		return dtentrada;
	}
	public Integer getTempoentrega() {
		return tempoentrega;
	}
	public Date getPrazolimiteautorizacao() {
		return prazolimiteautorizacao;
	}
	public Integer getValorsituacaosolicitacao() {
		return valorsituacaosolicitacao;
	}
	public Integer getValorcomprarecebidaposprazo() {
		return valorcomprarecebidaposprazo;
	}
	public Integer getIndiceprazomediocompra() {
		return indiceprazomediocompra;
	}
	public Double getToleranciaprazocompra() {
		return toleranciaprazocompra;
	}
	public Double getValorsituacaoordemcompra() {
		return valorsituacaoordemcompra;
	}
	public Double getMetaprazomediocompra() {
		return metaprazomediocompra;
	}
	

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setDtsolicitacao(Date dtsolicitacao) {
		this.dtsolicitacao = dtsolicitacao;
	}
	public void setNomepessoasolicitante(String nomepessoasolicitante) {
		this.nomepessoasolicitante = nomepessoasolicitante;
	}
	public void setDtautorizacao(Date dtautorizacao) {
		this.dtautorizacao = dtautorizacao;
	}
	public void setNomepessoaautorizacao(String nomepessoaautorizacao) {
		this.nomepessoaautorizacao = nomepessoaautorizacao;
	}
	public void setDtnecessidadesolicitacao(Date dtnecessidadesolicitacao) {
		this.dtnecessidadesolicitacao = dtnecessidadesolicitacao;
	}
	public void setDtbaixaordemcompra(Date dtbaixaordemcompra) {
		this.dtbaixaordemcompra = dtbaixaordemcompra;
	}
	public void setNomepessoabaixouordemcompra(String nomepessoabaixouordemcompra) {
		this.nomepessoabaixouordemcompra = nomepessoabaixouordemcompra;
	}
	public void setDtentrada(Date dtentrada) {
		this.dtentrada = dtentrada;
	}
	public void setTempoentrega(Integer tempoentrega) {
		this.tempoentrega = tempoentrega;
	}
	public void setPrazolimiteautorizacao(Date prazolimiteautorizacao) {
		this.prazolimiteautorizacao = prazolimiteautorizacao;
	}
	public void setValorsituacaosolicitacao(Integer valorsituacaosolicitacao) {
		this.valorsituacaosolicitacao = valorsituacaosolicitacao;
	}
	public void setValorcomprarecebidaposprazo(Integer valorcomprarecebidaposprazo) {
		this.valorcomprarecebidaposprazo = valorcomprarecebidaposprazo;
	}
	public void setIndiceprazomediocompra(Integer indiceprazomediocompra) {
		this.indiceprazomediocompra = indiceprazomediocompra;
	}
	public void setToleranciaprazocompra(Double toleranciaprazocompra) {
		this.toleranciaprazocompra = toleranciaprazocompra;
	}
	public void setValorsituacaoordemcompra(Double valorsituacaoordemcompra) {
		this.valorsituacaoordemcompra = valorsituacaoordemcompra;
	}
	public void setMetaprazomediocompra(Double metaprazomediocompra) {
		this.metaprazomediocompra = metaprazomediocompra;
	}
	
	public String getSituacaosolicitacao() {
		if(this.valorsituacaosolicitacao != null){
			if(this.valorsituacaosolicitacao >= 0) return AUTORIZADA_DENTRO_PRAZO;
			else return AUTORIZADA_FORA_PRAZO;
		}
		return situacaosolicitacao;
	}
	public String getSituacaocompra() {
		Double meta = 0.0;
		Double tolerancia = 0.0;
		Integer indice = 0;
		
		Double valor = 0.0;
		if(this.metaprazomediocompra != null) meta = this.metaprazomediocompra;
		if(this.toleranciaprazocompra != null) tolerancia = this.toleranciaprazocompra;
		if(this.indiceprazomediocompra != null) indice = this.indiceprazomediocompra;
		
		valor = meta + tolerancia - indice;
		if(valor > 0) return META_ALCANCADA;
		else if(valor < 0) return META_NAO_ALCANCADA;
		else if(valor == 0) return META_DENTRO_TOLERANCIA;
		
		return situacaocompra;
	}
	
	
	public void setSituacaosolicitacao(String situacaosolicitacao) {
		this.situacaosolicitacao = situacaosolicitacao;
	}
	public void setSituacaocompra(String situacaocompra) {
		this.situacaocompra = situacaocompra;
	}
}