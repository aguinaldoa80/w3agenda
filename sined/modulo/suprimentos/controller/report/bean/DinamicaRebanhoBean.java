package br.com.linkcom.sined.modulo.suprimentos.controller.report.bean;

import java.math.BigDecimal;

import com.ibm.icu.math.MathContext;

public class DinamicaRebanhoBean {

	private String material;
	private BigDecimal qtdeestoqueinicial;
	private BigDecimal qtdenascimento;
	private BigDecimal qtdemorte;
	private BigDecimal qtdeentradatransferencia;
	private BigDecimal qtdesaidatransferencia;
	private BigDecimal qtdevenda;
	private BigDecimal qtdecompra;
	private BigDecimal qtdeentradamudancacategoria;
	private BigDecimal qtdesaidamudancacategoria;
	private BigDecimal qtdeestoquefinal;
	
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	
	public BigDecimal getQtdeestoqueinicial() {
		return this.format(qtdeestoqueinicial);
	}
	public void setQtdeestoqueinicial(BigDecimal qtdeestoqueinicial) {
		this.qtdeestoqueinicial = qtdeestoqueinicial;
	}
	
	public BigDecimal getQtdenascimento() {
		return this.format(qtdenascimento);
	}
	public void setQtdenascimento(BigDecimal qtdenascimento) {
		this.qtdenascimento = qtdenascimento;
	}
	
	public BigDecimal getQtdemorte() {
		return this.format(qtdemorte);
	}
	public void setQtdemorte(BigDecimal qtdemorte) {
		this.qtdemorte = qtdemorte;
	}
	
	public BigDecimal getQtdeentradatransferencia() {
		return this.format(qtdeentradatransferencia);
	}
	public void setQtdeentradatransferencia(BigDecimal qtdeentradatransferencia) {
		this.qtdeentradatransferencia = qtdeentradatransferencia;
	}
	
	public BigDecimal getQtdesaidatransferencia() {
		return this.format(qtdesaidatransferencia);
	}
	public void setQtdesaidatransferencia(BigDecimal qtdesaidatransferencia) {
		this.qtdesaidatransferencia = qtdesaidatransferencia;
	}
	
	public BigDecimal getQtdevenda() {
		return this.format(qtdevenda);
	}
	public void setQtdevenda(BigDecimal qtdevenda) {
		this.qtdevenda = qtdevenda;
	}
	
	public BigDecimal getQtdecompra() {
		return this.format(qtdecompra);
	}
	public void setQtdecompra(BigDecimal qtdecompra) {
		this.qtdecompra = qtdecompra;
	}
	
	public BigDecimal getQtdeentradamudancacategoria() {
		return this.format(qtdeentradamudancacategoria);
	}
	public void setQtdeentradamudancacategoria(BigDecimal qtdeentradamudancacategoria) {
		this.qtdeentradamudancacategoria = qtdeentradamudancacategoria;
	}
	
	public BigDecimal getQtdesaidamudancacategoria() {
		return this.format(qtdesaidamudancacategoria);
	}
	public void setQtdesaidamudancacategoria(BigDecimal qtdesaidamudancacategoria) {
		this.qtdesaidamudancacategoria = qtdesaidamudancacategoria;
	}
	
	public BigDecimal getQtdeestoquefinal() {
		return this.format(qtdeestoquefinal);
	}
	public void setQtdeestoquefinal(BigDecimal qtdeestoquefinal) {
		this.qtdeestoquefinal = qtdeestoquefinal;
	}
	
	private BigDecimal format(BigDecimal valor){
		BigDecimal valorAux = valor != null? new BigDecimal(valor.intValue()): null;
		return valor != null && valor.compareTo(valorAux) != 0 ? valor.setScale(2, MathContext.ROUND_HALF_UP): valor;
	}
}
