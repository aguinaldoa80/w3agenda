package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.LocacaomaterialService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LocacaomaterialFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/LocacaomaterialCSV", authorizationModule = ReportAuthorizationModule.class)
public class LocacaomaterialCSVReport extends SinedReport<LocacaomaterialFiltro> {
	LocacaomaterialService locacaomaterialService;
	
	public void setLocacaomaterialService(
			LocacaomaterialService locacaomaterialService) {
		this.locacaomaterialService = locacaomaterialService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			LocacaomaterialFiltro filtro) throws Exception {
		throw new SinedException("M�todo n�o implementado.");
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,
			LocacaomaterialFiltro filtro) throws Exception {
		return locacaomaterialService.generateCsvReport(filtro, getNomeArquivo());
	}

	@Override
	public String getNomeArquivo() {
		return "locacao_de_material.csv";
	}

	@Override
	public String getTitulo(LocacaomaterialFiltro filtro) {
		return null;
	}
	
}
