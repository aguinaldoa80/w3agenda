package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.DinamicaRebanhoFiltro;

@Controller(path = "/suprimento/relatorio/Emitirdinamicarebanho", authorizationModule=ReportAuthorizationModule.class)
public class DinamicaRebanhoReport extends ResourceSenderController<DinamicaRebanhoFiltro>{
	
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MaterialcategoriaService materialcategoriaService;
	private MaterialgrupoService materialgrupoService;
	private MaterialService materialService;
	
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setMaterialcategoriaService(MaterialcategoriaService materialcategoriaService) {this.materialcategoriaService = materialcategoriaService;}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {this.materialgrupoService = materialgrupoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}

	@Override
	public Resource generateResource(WebRequestContext request,
			DinamicaRebanhoFiltro filtro) throws Exception {
		if(filtro.getMaterialcategoria() != null && filtro.getMaterialcategoria().getCdmaterialcategoria() != null){
			materialcategoriaService.loadIdentificador(filtro.getMaterialcategoria());
		}
		return movimentacaoestoqueService.gerarDinamicaRebanho(request, filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			DinamicaRebanhoFiltro filtro) throws Exception {
		List<Materialgrupo> listaMaterialGrupo = materialgrupoService.findForAnimal();
		List<Material> listaMaterial = materialService.findMateriaisWithMaterialGrupoAnimal();
		request.setAttribute("listaMaterialGrupo", listaMaterialGrupo);
		request.setAttribute("listaMaterial", listaMaterial);
		return new ModelAndView("relatorio/emitirdinamicarebanho")
					.addObject("filtro", new DinamicaRebanhoFiltro());
	}

}

