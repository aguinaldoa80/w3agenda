package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RomaneioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/suprimento/relatorio/RomaneioCSV", authorizationModule = ReportAuthorizationModule.class)
public class RomaneioCSVReport extends SinedReport<RomaneioFiltro> {

	RomaneioService romaneioService;
	
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			RomaneioFiltro filtro) throws Exception {
		throw new SinedException("M�todo n�o implementado");
	}

	@Override
	public Resource generateResource(WebRequestContext request,
			RomaneioFiltro filtro) throws Exception {
		return romaneioService.generateCsvReport(filtro, getNomeArquivo());
	}
	
	@Override
	public String getNomeArquivo() {
		return "romaneio.csv";
	}

	@Override
	public String getTitulo(RomaneioFiltro filtro) {
		return null;
	}
}
