package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.SugestaocompraFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(
			path = "/suprimento/relatorio/Sugestaocompra", 
			authorizationModule=ReportAuthorizationModule.class
)
public class SugestaocompraReport extends SinedReport<SugestaocompraFiltro>{

	private MaterialService materialService;
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	SugestaocompraFiltro filtro) throws Exception {
		return materialService.gerarSugestaocompraRelatorio(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "sugestaocompra";
	}
	
	@Override
	public String getTitulo(SugestaocompraFiltro filtro) {
		return "SUGEST�O DE COMPRA";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, SugestaocompraFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);

		if(filtro.getEmpresa() != null){
			report.addParameter("empresafiltro", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}
		report.addParameter("periodo", SinedUtil.getDescricaoPeriodo(filtro.getDtinicio(), filtro.getDtfim()));
	}
	
	public ModelAndView gerarCSV(WebRequestContext request, SugestaocompraFiltro filtro) throws ResourceGenerationException{
		List<Material> lista = materialService.ajustaListaSugestaocompra(materialService.findForSugestaocompra(filtro), filtro);
		
		if(SinedUtil.isListEmpty(lista)){
			request.addError("N�o existe sugest�o de compra de acordo com o filtro selecionado.");
			return doFiltro(request, filtro);
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("Cod. Material;");
		sb.append("Material;");
		sb.append("U.M.;");
		sb.append("Estoque m�nimo;");
		sb.append("Estoque atual;");
		sb.append("Vendas;");
		sb.append("Sugest�o de compra;");
		sb.append("Peso l�quido;");
		sb.append("Peso bruto;");
		sb.append("\n");
				
		for (Material material: lista) {
			if(material.getIdentificacaoOuCdmaterial() !=null){
				sb.append(material.getIdentificacaoOuCdmaterial());
			}else{
				sb.append(material.getCdmaterial());
			}
			sb.append(";");
			sb.append(material.getNome());
			sb.append(";");
			
			sb.append(Util.strings.emptyIfNull(material.getUnidademedida().getSimbolo()));
			sb.append(";");
			
			sb.append(material.getSugestaocompraestoqueminimo()!=null ? SinedUtil.roundByUnidademedida(material.getSugestaocompraestoqueminimo(), material.getUnidademedida()).toString().replace(".", ",") : "");
			sb.append(";");
			
			sb.append(material.getSugestaocompraestoqueatual()!=null ? SinedUtil.roundByUnidademedida(material.getSugestaocompraestoqueatual(), material.getUnidademedida()).toString().replace(".", ",") : "");
			sb.append(";");
			
			sb.append(material.getSugestaocompravendas()!=null ? SinedUtil.roundByUnidademedida(material.getSugestaocompravendas(), material.getUnidademedida()).toString().replace(".", ",") : "");
			sb.append(";");
			
			sb.append(material.getSugestaocomprasugestao()!=null ? SinedUtil.roundByUnidademedida(material.getSugestaocomprasugestao(), material.getUnidademedida()).toString().replace(".", ",") : "");
			sb.append(";");
			
			sb.append(material.getSugestaocompratotalpeso()!=null ? material.getSugestaocompratotalpeso() : "");
			sb.append(";");
			
			sb.append(material.getSugestaocompratotalpesobruto()!=null ? material.getSugestaocompratotalpesobruto() : "");
			sb.append(";");
			
			sb.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "sugestao_compra_" + SinedUtil.datePatternForReport() + ".csv", sb.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		
		desbloquearTela(request);
		
		return resourceModelAndView;
	}
}