package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecedorFiltro;

@Controller(path = "/suprimento/relatorio/AvaliarFornecedor")
public class AvaliarfornecedorReport extends ResourceSenderController<FornecedorFiltro>{

	private FornecedorService fornecedorService;
	
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,
			FornecedorFiltro filtro) throws Exception {
		
		String whereIn = request.getParameter("selectedItens");
		
		return fornecedorService.createAvaliarFornecedor(whereIn);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			FornecedorFiltro filtro) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

}
