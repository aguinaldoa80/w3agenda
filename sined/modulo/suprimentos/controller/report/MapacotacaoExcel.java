package br.com.linkcom.sined.modulo.suprimentos.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.CotacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/suprimento/relatorio/MapacotacaoExcel", authorizationModule=ProcessAuthorizationModule.class)
public class MapacotacaoExcel extends ResourceSenderController<CotacaoFiltro>{
	private CotacaoService cotacaoService;
	
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,	CotacaoFiltro filtro) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request);
		return cotacaoService.gerarExcelMapacotacao(whereIn);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	CotacaoFiltro filtro) throws Exception {
		return  new ModelAndView("${ctx}/suprimento/crud/Cotacao cotacao");
	}

}
