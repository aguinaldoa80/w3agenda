package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedorempresa;
import br.com.linkcom.sined.geral.bean.Fornecedorhistorico;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Pessoamaterial;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.ColaboradorFornecedorService;
import br.com.linkcom.sined.geral.service.ContacontabilService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.FornecedorempresaService;
import br.com.linkcom.sined.geral.service.FornecedorhistoricoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PessoaContatoService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PessoadapService;
import br.com.linkcom.sined.geral.service.PessoamaterialService;
import br.com.linkcom.sined.geral.service.PessoaquestionarioService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.crm.controller.process.ContatoProcess;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecedorFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.ValidacaoCampoObrigatorio;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path={"/suprimento/crud/Fornecedor","/financeiro/crud/Fornecedor"},
		authorizationModule=CrudAuthorizationModule.class
)
public class FornecedorCrud extends CrudControllerSined<FornecedorFiltro, Fornecedor, Fornecedor>{

	private TelefoneService telefoneService;
	private MunicipioService municipioService;
	private EnderecoService enderecoService;
	private PessoaContatoService pessoaContatoService;
	private UsuarioService usuarioService;
	private PessoaService pessoaService;
	private FornecedorService fornecedorService;
	private ParametrogeralService parametrogeralService;
	private PessoaquestionarioService pessoaquestionarioService;
	private FornecedorhistoricoService fornecedorHistoricoService;
	private ColaboradorFornecedorService colaboradorFornecedorService;
	private PessoadapService pessoadapService;
	private ContacontabilService contacontabilService;
	private PessoamaterialService pessoamaterialService;
	private MaterialService materialService;
	private EmpresaService empresaService;
	private FornecedorempresaService fornecedorempresaService;
	private PedidovendatipoService pedidovendatipoService;
	
	public void setPessoadapService(PessoadapService pessoadapService) {
		this.pessoadapService = pessoadapService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {
		this.pessoaContatoService = pessoaContatoService;
	}
	public void setTelefoneService(TelefoneService telefoneService) {
		this.telefoneService = telefoneService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPessoaquestionarioService(
			PessoaquestionarioService pessoaquestionarioService) {
		this.pessoaquestionarioService = pessoaquestionarioService;
	}
	public void setFornecedorHistoricoService(FornecedorhistoricoService fornecedorHistoricoService) {
		this.fornecedorHistoricoService = fornecedorHistoricoService;
	}
	public void setColaboradorFornecedorService (ColaboradorFornecedorService colaboradorFornecedorService){
		this.colaboradorFornecedorService = colaboradorFornecedorService;
	}
	public void setContacontabilService(ContacontabilService contacontabilService) {
		this.contacontabilService = contacontabilService;
	}
	
	
	public void setPessoamaterialService(
			PessoamaterialService pessoamaterialService) {
		this.pessoamaterialService = pessoamaterialService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setFornecedorempresaService(FornecedorempresaService fornecedorempresaService) {
		this.fornecedorempresaService = fornecedorempresaService;
	}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {
		this.pedidovendatipoService = pedidovendatipoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected Fornecedor criar(WebRequestContext request, Fornecedor form)throws Exception {
		Fornecedor fornecedor = super.criar(request, form);
		fornecedor.setAtivo(Boolean.TRUE);
		fornecedor.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
		Usuario usuario = SinedUtil.getUsuarioLogado();
		request.setAttribute("RESTRICAO_FORNECEDOR_PRODUTO", usuario != null && usuario.getRestricaofornecedorproduto() 
				!= null && usuario.getRestricaofornecedorproduto());
		String param = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORFORNECEDOR);
		if(param != null && param.toUpperCase().equals("TRUE")){
			Integer identificador = Integer.parseInt(fornecedorService.geraIdentificadorSemDV());
			fornecedor.setIdentificador(identificador.toString());
			fornecedor.setIsIdentificadorGerado(true);
			request.setAttribute("geracaoIdentificador", true);
		} else
			fornecedor.setIsIdentificadorGerado(false);
		
		if("true".equals(request.getParameter("importacaoXMLDI"))){
			fornecedor.setTransportador(Boolean.TRUE);
		}
		
		return fornecedor;
	}
	
	public void verificaNomeCPF(WebRequestContext request, Pessoa pessoa){
		if(pessoa.getCpf() != null){
			pessoaService.verificaNomeCPFCNPJ(pessoa.getCpf().getValue());
		} else {
			View.getCurrent().println("alert('N�o foi encontrado o CPF.');");
		}
	}
	
	public void verificaNomeCNPJ(WebRequestContext request, Pessoa pessoa){
		if(pessoa.getCnpj() != null){
			pessoaService.verificaNomeCPFCNPJ(pessoa.getCnpj().getValue());
		} else {
			View.getCurrent().println("alert('N�o foi encontrado o CNPJ.');");
		}
	}
	
	public void verificaSimples(WebRequestContext request, Pessoa pessoa){
		if(pessoa.getCnpj() != null){
			pessoaService.verificaSimples(pessoa.getCnpj().getValue());
		} else {
			View.getCurrent().println("alert('N�o foi encontrado o CNPJ.');");
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void salvar(WebRequestContext request, Fornecedor bean)throws Exception {
		pessoaService.ajustaPessoaToSave(bean);
		
		String param = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORFORNECEDOR);
		if(param != null && param.toUpperCase().equals("TRUE") && bean.getIdentificador() != null){
			if(fornecedorService.existeIdentificadorFornecedor(bean.getIdentificador(), bean.getCdpessoa())){
				bean.setIdentificador(fornecedorService.geraIdentificadorSemDV());
			}
			
			String identificador = parametrogeralService.getValorPorNome("ProximoIdentificadorFornecedor");
			if (identificador.equals(bean.getIdentificador())){
				Integer valorIdentificador = Integer.parseInt(identificador);
				valorIdentificador++;
				parametrogeralService.updateValorPorNome("ProximoIdentificadorFornecedor", valorIdentificador.toString());
			}
		}
		
		if(bean.getListaEndereco() == null){
			Set<Endereco> end = new HashSet<Endereco>();
			bean.setListaEndereco(end);
		}
		
		if(bean.getListaEndereco() != null){
			for (Endereco en : bean.getListaEndereco()) {
				if(en.getMunicipio() != null && en.getMunicipio().getCdmunicipio() != null){
					en.getMunicipio().setUf(en.getUf());
				}
			}
		}
		
		Object attribute = request.getSession().getAttribute("listaContato" + (bean.getCdpessoa() != null ? bean.getCdpessoa() : ""));
		if (attribute != null) {
			bean.setListaContato((List<PessoaContato>)attribute);
		}
		
		if (bean.getAcesso() == null) {
			bean.setAcesso(false);
		}
		
//		SALVA A LISTA DE QUESITONARIO
		if (bean.getListaQuestionario() != null){
			String whereIn = CollectionsUtil.listAndConcatenate(bean.getListaQuestionario(), "cdpessoaquestionario", ",");
			pessoaquestionarioService.excluirDaListaPessoa(whereIn, bean);
		} else {
			if (bean.getCdpessoa() != null){
				pessoaquestionarioService.excluirListaCompletaPessoa(bean);
			}
		}
		
		//Preenchimento do IE e IE estado
		if(Tipopessoa.PESSOA_FISICA.equals(bean.getTipopessoa())){
			if(bean.getInscricaoestadual() == null){
				bean.setInscricaoestadual("ISENTO");
			}
			if(bean.getUfinscricaoestadual() == null && bean.getListaEndereco() != null){
				Set<Endereco> listaEnderecos = bean.getListaEndereco();
				for (Endereco endereco : listaEnderecos) {
					if((Enderecotipo.FATURAMENTO.equals(endereco.getEnderecotipo()) || Enderecotipo.UNICO.equals(endereco.getEnderecotipo())) &&
						endereco.getMunicipio() != null){
						Municipio municipio = municipioService.carregaMunicipio(endereco.getMunicipio());
						bean.setUfinscricaoestadual(municipio.getUf());
						break;
					}
				}
			}
		}
		
		try{
			boolean isCriar = bean.getCdpessoa() == null;
			Fornecedor oldFornecedor =  fornecedorService.carregaTiketMedio(bean);
			StringBuilder observacao = new StringBuilder();
			
			String ticketMedio = oldFornecedor.getValorTicketMedio() != null ? "R$" + new Money(oldFornecedor.getValorTicketMedio()).toString() : "campo vazio";
			String novoTicketMedio = bean.getValorTicketMedio() != null ?  "R$" + new Money(bean.getValorTicketMedio()).toString() : "campo vazio";

			super.salvar(request, bean);
			
			if(ticketMedio.compareTo(novoTicketMedio)!=0){
				observacao.append("O valor do ticket m�dio por KG modificado de " + ticketMedio + " para " + novoTicketMedio);
			}
			

			super.salvar(request, bean);
			salvarHistoricoFornecedor(bean, isCriar, observacao.toString());
			
			if (isCriar && bean.getContaContabil()==null){
				criarContaContabil(bean);
			}
			
			if("true".equals(request.getParameter("closeOnSave"))){
				request.getServletResponse().setContentType("text/html");
				View.getCurrent()
				.eval("<script type='text/javascript'>")
					.eval("window.opener.$s.showNoticeMessage('Cliente salvo com sucesso.');")
					.eval("window.opener.changeFornecedor('" + bean.getNome() + "');")
					.eval("window.top.close();")
				.eval("</script>").flush();
			}
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_PESSOA_EMAIL")){
				throw new SinedException("E-mail j� cadastrado no sistema.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_PESSOA_CNPJ")){
				throw new SinedException("CNPJ j� cadastrado no sistema. Tente utilizar a op��o 'Recuperar pelo CNPJ', dispon�vel no �cone ao lado do campo.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_PESSOA_CPF")){
				throw new SinedException("CPF j� cadastrado no sistema. Tente utilizar a op��o 'Recuperar pelo CPF', dispon�vel no �cone ao lado do campo.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_FORNECEDOR_LOGIN")){
				throw new SinedException("Login j� cadastrado para outro fornecedor.");
			}
			if(DatabaseError.isKeyPresent(e, "IDX_FORNECEDOR_IDENTIFICADOR")){
				throw new SinedException("Identificador j� cadastrado para outro fornecedor.");
			}
			if(DatabaseError.isKeyPresent(e, "idx_uq_fornecedorempresa_0")){
				throw new SinedException("Empresa j� cadastrada.");
			}
			if(DatabaseError.isKeyPresent(e, "endereco")){
				request.setAttribute("ErroAoDeletarEnd", "true");
				throw new SinedException("Endere�o n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
			}
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected ListagemResult<Fornecedor> getLista(WebRequestContext request,FornecedorFiltro filtro) {
		
		ListagemResult<Fornecedor> listagemResult = super.getLista(request, filtro);
		List<Fornecedor> list = listagemResult.list();
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdpessoa", ",");
		List<Usuario> listaUsuario = usuarioService.findForGerenciaPlanejamento(whereIn);
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(fornecedorService.findListagem(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}	
		
		for (Fornecedor f : list) {
			f.setListaTelefone(SinedUtil.listToSet(telefoneService.findByPessoa(f), Telefone.class));
			
			if(f.getCpf() != null){
				f.setCpfcnpj(f.getCpf().toString());
			}else if(f.getCnpj() != null){
				f.setCpfcnpj(f.getCnpj().toString());
			}
			
			if(f.getListaTelefone()!= null){
				f.setListaTelefones(telefoneService.createListaTelefone(f.getListaTelefone(), "<br>"));
			}
			
			for (Usuario u : listaUsuario) {
				if (f.getCdpessoa().equals(u.getCdpessoa())){
					f.setPossuiUsuario(true);
				}
			}
			
		}
		return listagemResult;
	}
	
	@Override
	protected void excluir(WebRequestContext request, Fornecedor bean)throws Exception {
		String itens = request.getParameter("itenstodelete");
		if(itens == null || itens.equals("")){
			itens = bean.getCdpessoa() + "";
		}
		
		if(fornecedorService.verificaOutrosRegistros(itens)){
			throw new SinedException("Fornecedor n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
		
		try {
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException da) {
			throw new SinedException("Fornecedor n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Fornecedor form)throws Exception {
		colaboradorFornecedorService.validaPermissao(form);
		
		pessoaService.ajustaPessoaToLoad(form);
		if(form.getCdpessoa() != null) request.setAttribute("cdpessoaParam", form.getCdpessoa());
		
		if(request.getAttribute("ErroAoDeletarEnd") !=null && request.getAttribute("ErroAoDeletarEnd").equals("true")){
			pessoaService.setListaEnderecoForEntrada(form);
		}
		
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
		request.setAttribute("tipooperacaocredito", Tipooperacao.TIPO_CREDITO);
		
		
		if(form.getCpf() != null && form.getCpf().getValue() != null){
			form.setTipopessoa(Tipopessoa.PESSOA_FISICA);
		}else
		if(form.getCnpj() != null && form.getCnpj().getValue() != null){
			form.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
		}
		
		if (!request.getBindException().hasErrors() && form.getCdpessoa() != null) {
			List<PessoaContato> listaPessoaContato = pessoaContatoService.findByPessoa(form);
			form.setListaContato(listaPessoaContato);
		}
		if(request.getBindException().hasErrors()){
			Object attribute = request.getSession().getAttribute("listaContato" + (form.getCdpessoa() != null ? form.getCdpessoa() : ""));
			if (attribute != null) {
				form.setListaContato((List<PessoaContato>) attribute);		
			}
		}
		request.getSession().setAttribute("listaContato" + (form.getCdpessoa() != null ? form.getCdpessoa() : ""), form.getListaContato());
		
		if(form.getCdpessoa() != null){
			if(request.getBindException() != null && !request.getBindException().hasErrors()){
				form.setListaPessoadap(pessoadapService.findByPessoa(form));
				form.setListaPessoamaterial(pessoamaterialService.findByPessoa(form));
				form.setListaFornecedorempresa(fornecedorempresaService.findByFornecedor(form));
			}
			form.setHistorico(fornecedorHistoricoService.findByFornecedor(form));
			form.setListaColaboradorFornecedor(colaboradorFornecedorService.findByFornecedor(form));
			
			List<Municipio> listaMunicipio = new ArrayList<Municipio>();
			if(form.getListaEndereco() != null){
				for (Endereco endereco : form.getListaEndereco()) {
					if(endereco != null && endereco.getMunicipio() != null){
						listaMunicipio.add(endereco.getMunicipio());
						endereco.setUf(endereco.getMunicipio().getUf());
					}
				}
			}
			
			request.setAttribute("listaMunicipioEndereco", listaMunicipio);
		}
		
		if (form.getContagerencial() != null){
			form.setContagerencial(new Contagerencial(form.getContagerencial().getCdcontagerencial()));
		}
		if (form.getContagerencialcredito() != null){
			form.setContagerencialcredito(new Contagerencial(form.getContagerencialcredito().getCdcontagerencial()));
		}
		if (form.getContaContabil() != null){
			form.setContaContabil(new ContaContabil(form.getContaContabil().getCdcontacontabil()));
		}
		
		String param = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORFORNECEDOR);
		if(param != null && param.toUpperCase().equals("TRUE")){
			request.setAttribute("geracaoIdentificador", true);
			form.setIsIdentificadorGerado(true);
		} else
			form.setIsIdentificadorGerado(false);	
		
		if(form.getCdpessoa() != null)
			form.setListaQuestionario(SinedUtil.listToSet(pessoaquestionarioService.carregarPessoaQuestinario(form), Pessoaquestionario.class));
		
		request.setAttribute("listaEmpresa", empresaService.findForCombo(SinedUtil.isListNotEmpty(form.getListaFornecedorempresa()) ? (List<Empresa>) CollectionsUtil.getListProperty(form.getListaFornecedorempresa(), "empresa") : null, true));
		request.setAttribute(ContatoProcess.NOME_CLASSE_PAI_CONTATO, "Fornecedor");
		request.setAttribute("ignoreHackAndroidDownload", Boolean.TRUE);
		request.setAttribute("COOPERATIVAS", parametrogeralService.getBoolean(Parametrogeral.COOPERATIVAS));
		request.setAttribute("OBRIGAR_CPF_FORNECEDOR", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_CPF_FORNECEDOR));
		request.setAttribute("codigoPaisBrasil", Pais.BRASIL.getCdpais());
		request.setAttribute("OBRIGAR_CNPJ_FORNECEDOR", parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_CNPJ_FORNECEDOR));
		request.setAttribute("FORNECEDOR", Boolean.TRUE);
		request.setAttribute("isTrayCorp", parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE));
		request.setAttribute("INTEGRACAO_ECOMMERCE", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE));
		request.setAttribute("INTEGRACAO_CORREIOS", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_CORREIOS));
		request.setAttribute("utilizaTicketMedio", pedidovendatipoService.existsPedidovendaWithTicketMedio());
		request.setAttribute("ALTERACAO_TICKET_MEDIO", SinedUtil.isUserHasAction("ALTERACAO_TICKET_MEDIO"));
		Usuario usuario = SinedUtil.getUsuarioLogado();
		request.setAttribute("RESTRICAO_FORNECEDOR_PRODUTO", usuario != null && usuario.getRestricaofornecedorproduto() 
				!= null && usuario.getRestricaofornecedorproduto());
		
		if(Hibernate.isInitialized(form.getListaFornecedorempresa()) && SinedUtil.isListNotEmpty(form.getListaFornecedorempresa())){
			Collections.sort(form.getListaFornecedorempresa(), new Comparator<Fornecedorempresa>(){
				public int compare(Fornecedorempresa a1, Fornecedorempresa a2){
					return new SinedUtil().getUsuarioPermissaoEmpresa(a1.getEmpresa()) ? 1 : 0; 
				}
			});
		}
	}
	
	@Override
	protected void validateBean(Fornecedor bean, BindException errors) {
		enderecoService.validateListaEndereco(errors, bean.getListaEndereco());
		
		if (bean.getAcesso() != null && bean.getAcesso() && bean.getLogin() != null && usuarioService.exiteLoginUsuario(bean)) {
			errors.reject("001","Login j� cadastrado para algum usu�rio.");
		}
		
		if(bean.getListaPessoamaterial() != null && !bean.getListaPessoamaterial().isEmpty()){

			Boolean mesEntrePeriodoJaExistente = false;
			Map<Material, List<Pessoamaterial>> mesesPorMaterial = new HashMap<Material, List<Pessoamaterial>>();
			for(Pessoamaterial pessoamaterial: bean.getListaPessoamaterial()){
				if(pessoamaterial.getMesinicial().compareTo(pessoamaterial.getMesfinal()) > 0){
					errors.reject("003", "M�s inicial de disponibilidade do material n�o pode ser maior que m�s final.");
					break;
				}
				if(mesesPorMaterial.containsKey(pessoamaterial.getMaterial())){
					for(Pessoamaterial pes: mesesPorMaterial.get(pessoamaterial.getMaterial())){
						mesEntrePeriodoJaExistente = ((pessoamaterial.getMesinicial().compareTo(pes.getMesinicial()) >= 0) &&
															(pessoamaterial.getMesinicial().compareTo(pes.getMesfinal()) <= 0)) ||
													((pessoamaterial.getMesfinal().compareTo(pes.getMesinicial()) >= 0) &&
															(pessoamaterial.getMesfinal().compareTo(pes.getMesfinal()) <= 0));
						if(mesEntrePeriodoJaExistente){
							errors.reject("002", "O material '"+pessoamaterial.getMaterial().getNome()+"' possui disponibilidade registrada mais de uma vez para o mesmo per�odo.");
							break;							
						}
					}
					if(mesEntrePeriodoJaExistente){
						break;
					}
					
					mesesPorMaterial.get(pessoamaterial.getMaterial()).add(pessoamaterial);
				}else{
					mesesPorMaterial.put(pessoamaterial.getMaterial(), new ArrayList<Pessoamaterial>());
					mesesPorMaterial.get(pessoamaterial.getMaterial()).add(pessoamaterial);
				}
			}
		}
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.GERAR_CONTACONTABIL_RAZAOSOCIAL_FORNECEDOR))
				&& Tipopessoa.PESSOA_JURIDICA.equals(bean.getTipopessoa())
				&& (bean.getRazaosocial()==null || bean.getRazaosocial().trim().equals(""))){
			errors.reject("005", "O campo raz�o social � obrigat�rio. Motivo: O sistema cria a conta cont�bil do fornecedor considerando a raz�o social.");
		}
		
		if(bean.getCdpessoa() != null){
			if(!Boolean.TRUE.equals(bean.getFabricante())){
				List<Material> listaMateriais = materialService.findMateriaisFabricante(bean);
				if(SinedUtil.isListNotEmpty(listaMateriais)){
					String materiais = "";
					for (Material material : listaMateriais) {
						materiais = materiais + material.getNome() + ", ";
					}
					
					materiais = materiais.substring(0, materiais.length() - 2);
					materiais = materiais + ".";
					errors.reject("007", "N�o � poss�vel desmarcar o fabricante do fornecedor, pois o mesmo est� vinculado aos seguintes materiais como fabricante: " + materiais);
				}
			} else {
				List<Material> listaMateriais = materialService.findMateriaisFornecedor(bean);
				if(SinedUtil.isListNotEmpty(listaMateriais)){
					String materiais = "";
					for (Material material : listaMateriais) {
						materiais = materiais + material.getNome() + ", ";
					}
					
					materiais = materiais.substring(0, materiais.length() - 2);
					materiais = materiais + ".";
					errors.reject("008", "N�o � poss�vel marcar o fabricante do fornecedor, pois o mesmo est� vinculado aos seguintes materiais como fornecedor: " + materiais);
				}
			}
		}
		
		if(bean.getCodigoParceiro() != null){
			Fornecedor fornecedor = null;
			if(bean.getCdpessoa()!=null){
				fornecedor = fornecedorService.load(bean);
			
				if(fornecedor !=null && fornecedor.getCodigoParceiro() !=null && !fornecedor.getCodigoParceiro().equals(bean.getCodigoParceiro()) && fornecedorService.existeCodigoParceiro(bean.getCodigoParceiro())){
					errors.reject("008","C�digo parceiro j� cadastrado.");
				}
			}else if(fornecedorService.existeCodigoParceiro(bean.getCodigoParceiro())){
				errors.reject("008","C�digo parceiro j� cadastrado.");
			}
		}
	}
	
	/**
	 * M�todo para filtrar os municipios por uf.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MunicipioService#findByUf(Uf)
	 * @param request
	 * @param departamento
	 * @author Fl�vio
	 */
	public void makeAjaxMunicipio(WebRequestContext request, Uf uf){
		List<Municipio> listaMunicipio = municipioService.findByUf(uf);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaMunicipio, "listaMunicipios", ""));
	}
	
	/**
	 * Veirifica se existe alguma pessoa cadastrada no banco com o mesmo cpf ou cnpj.
	 *
	 * @param request
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	public void verificaPessoa(WebRequestContext request, FornecedorFiltro filtro){
		Pessoa pessoa;
		if(filtro.getCpf() != null){
			pessoa = pessoaService.findPessoaByCpf(filtro.getCpf());
		} else if(filtro.getCnpj() != null){
			pessoa = pessoaService.findPessoaByCnpj(filtro.getCnpj());
		} else {
			throw new SinedException("Erro na obten��o da pessoa.");
		}
		
		String string = "";
		if(pessoa != null){
			string += "var achou = true;";
			
			Fornecedor fornecedor = fornecedorService.load(pessoa.getCdpessoa());
			if(fornecedor != null){
				string += "var fornecedorAchou = true;";
			} else {
				string += "var fornecedorAchou = false;";
				string += "var nomeFornecedor = '" + SinedUtil.escapeSingleQuotes(pessoa.getNome()) + "';";
				string += "var cdpessoaFornecedor = " + pessoa.getCdpessoa() + ";";
			}
		} else {
			string += "var achou = false;";
		}
		
		View.getCurrent().println(string);
	}
	
	/**
	 * Recupera a pessoa e manda para edi��o do cliente.
	 *
	 * @param request
	 * @param cliente
	 * @return
	 * @throws CrudException
	 * @author Rodrigo Freitas
	 */
	public ModelAndView recuperarPessoa(WebRequestContext request, Fornecedor fornecedor) throws CrudException{
		String param = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORCLIENTE);
		Integer identificador = 0;
		if(param != null && param.toUpperCase().equals("TRUE")){
			identificador = Integer.parseInt(fornecedorService.geraIdentificadorSemDV());
			while(fornecedorService.existeIdentificadorFornecedor(identificador.toString(), null)){
				identificador++;
			} 
			
			fornecedor.setIdentificador(identificador.toString());
			fornecedor.setIsIdentificadorGerado(true);
			request.setAttribute("geracaoIdentificador", true);
		} else
			fornecedor.setIsIdentificadorGerado(false);
		fornecedorService.insertSomenteFornecedor(fornecedor);
		identificador++;
		parametrogeralService.updateValorPorNome("ProximoIdentificadorFornecedor", identificador.toString());
		return doEditar(request, fornecedor);
	}
	
	/**
	 * M�todo para filtrar os mun�cipios do UF
	 * 
	 * @see MunicipioService#findByUf(br.com.linkcom.sined.geral.bean.Uf)
	 * @see SinedUtil#convertToJavaScript(List, String, String)
	 * @param request
	 * @param uf
	 * @author Taidson
	 * @since 28/04/2010
     * Estrutura copiada do Sindis
	 */

	public void ajaxComboMunicipio(WebRequestContext request, Uf uf){
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		if (uf!=null && uf.getCduf()!=null){
			listaMunicipio = municipioService.findByUf(uf);			
		}
				
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaMunicipio, "listaMunicipio", ""));
	}
	
	/**
	 * M�todo para carregar os mun�cipios do UF no carregamento das telas, pois sen�o carrega todos os munic�pios,
	 * assim, cobrecarregando os m�todos
	 * 
	 * @see MunicipioService#findByUf(br.com.linkcom.sined.geral.bean.Uf)
	 * @author Taidson
	 * @since 28/04/2010
     * Estrutura copiada do Sindis
	 */
	public ModelAndView ajaxPreencheComboMunicipioCarregarTela(WebRequestContext request){
		Endereco endereco = enderecoService.loadEndereco(new Endereco(Integer.valueOf(request.getParameter("cdendereco"))));
		Uf uf = new Uf(Integer.valueOf(request.getParameter("cduf")));
		List<Municipio> listaMunicipio = municipioService.findByUf(uf);
		
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("listaMunicipio", listaMunicipio);
		json.addObject("municipioSelecionado", endereco.getMunicipio());
		return json;
	}
	
	/**
	 * M�todo para carregar a conta gerencial associada ao fornecedor.
	 * 
	 * @see MunicipioService#findByUf(br.com.linkcom.sined.geral.bean.Uf)
	 * @author Taidson
	 * @since 28/04/2010
     * Estrutura copiada do Sindis
	 */
	public ModelAndView ajaxCarregaContaGerencial(WebRequestContext request){
		String cdfornecedor = request.getParameter("cdfornecedor");
		
		JsonModelAndView json = new JsonModelAndView();

		if (!StringUtils.isEmpty(cdfornecedor) && StringUtils.isNumeric(cdfornecedor)){		
			Contagerencial contagerencial = fornecedorService.findContaGerencial(new Fornecedor(Integer.valueOf(cdfornecedor)));
			
			if (contagerencial != null){
				json.addObject("sucesso", true);
				json.addObject("value", Util.strings.toStringIdStyled(contagerencial, false));
				json.addObject("label", contagerencial.getVcontagerencial().getIdentificador() + " - " + 
						contagerencial.getDescricaoCombo());
			}
		}
		
		return json;
	}
	
	/**
	 * M�todo chamado via Ajax que gera um identificador
	 * 
	 * @param request
	 * @author Giovane Freitas
	 */
	public void ajaxGerarIdentificador(WebRequestContext request){
		String identificador = fornecedorService.geraIdentificadorSemDV();
		View.getCurrent().println("var identificador = "+identificador);
	}
	
	/**
	 * M�todo para salvar o Historico do Fornecedor
	 * @param bean
	 * @author Dhiego Morais
	 * @since 03/02/2014
	 */
	public void salvarHistoricoFornecedor(Fornecedor bean, boolean isCriar, String observacao){
		if (isCriar){
			String texto = "Cria��o do fornecedor.";
			if (bean.getObservacaohistorico()==null || bean.getObservacaohistorico().trim().equals("")){
				bean.setObservacaohistorico(texto);
			}else {
				bean.setObservacaohistorico(texto + " " + bean.getObservacaohistorico());
			}
		}
		
		StringBuilder obs = new StringBuilder();
		if(bean.getObservacaohistorico() != null)
			obs.append(bean.getObservacaohistorico());
		if(observacao != null && !observacao.isEmpty()){
			if(bean.getObservacaohistorico() != null)
				obs.append(". ");
			obs.append(observacao);
		}
		
		Fornecedorhistorico fornecedorHistorico = new Fornecedorhistorico();
		fornecedorHistorico.setObservacao(obs.toString());
		Usuario responsavel = usuarioService.carregarUsuarioByPK(bean.getCdusuarioaltera());
		fornecedorHistorico.setResponsavel(responsavel);
		fornecedorHistorico.setDtaltera(bean.getDtaltera());
		fornecedorHistorico.setFornecedor(bean);
		fornecedorHistoricoService.saveOrUpdate(fornecedorHistorico);
	}
	
	
	/**
	 * Met�do que carrega a popup com o mapa do endere�o do fornecedor
	 * @param request
	 * @param endereco
	 */
	public ModelAndView abrirMapaEndereco(WebRequestContext request, Endereco endereco){
		
		endereco = enderecoService.loadEndereco(endereco);
		
		request.setAttribute("endereco", endereco.getLogradouroCompletoComBairro());
		return new ModelAndView("direct:crud/popup/popUpmapaEndereco");
	}
	
	private void criarContaContabil(Fornecedor bean){
		String nome;
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.GERAR_CONTACONTABIL_RAZAOSOCIAL_FORNECEDOR))){
			nome = bean.getRazaosocial();
		}else {
			nome = bean.getNome();
		}
		
		ContaContabil contacontabil = contacontabilService.criarContaContabil(bean.getCdpessoa(), nome, Parametrogeral.CONTA_CONTABIL_FORNECEDOR);
		if (contacontabil!=null){
			fornecedorService.updateContaContabil(bean, contacontabil);
		}
	}
	
	public ModelAndView ajaxUnidademedidaMaterial(WebRequestContext request) {
		Material material = new Material(Integer.valueOf(request.getParameter("cdmaterial")));
		material = materialService.loadMaterialunidademedida(material);
		return new JsonModelAndView()
									 .addObject("unidademedida", material.getUnidademedida());
	}
	
	@Override
	protected List<ValidacaoCampoObrigatorio> getCamposObrigatoriosAdicionais() {
		List<ValidacaoCampoObrigatorio> lista = new ArrayList<ValidacaoCampoObrigatorio>();
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CPF_FORNECEDOR))){
			ValidacaoCampoObrigatorio bean = new ValidacaoCampoObrigatorio() {
				@Override
				public String getPrefixo() {
					return "";
				}
				
				@Override
				public Boolean getPelomenosumregistro() {
					return false;
				}
				
				@Override
				public String getCondicao() {
					return "tipopessoa == 'PESSOA_FISICA' && !exterior";
				}
				
				@Override
				public String getCampo() {
					return "cpf";
				}
				
				@Override
				public String getDisplayNameCampo() {
					return "CPF";
				}
				
				@Override
				public String getDisplayNameLista() {
					return null;
				}

				@Override
				public void setDisplayNameCampo(String string) {
				}

				@Override
				public void setDisplayNameLista(String string) {
				}
			};
			
			lista.add(bean);
		}
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CNPJ_FORNECEDOR))){
			ValidacaoCampoObrigatorio bean = new ValidacaoCampoObrigatorio() {
				@Override
				public String getPrefixo() {
					return "";
				}
				
				@Override
				public Boolean getPelomenosumregistro() {
					return false;
				}
				
				@Override
				public String getCondicao() {
					return "tipopessoa == 'PESSOA_JURIDICA' && !exterior";
				}
				
				@Override
				public String getCampo() {
					return "cnpj";
				}
				
				@Override
				public String getDisplayNameCampo() {
					return "CNPJ";
				}
				
				@Override
				public String getDisplayNameLista() {
					return null;
				}

				@Override
				public void setDisplayNameCampo(String string) {
				}

				@Override
				public void setDisplayNameLista(String string) {
				}
			};
			
			lista.add(bean);
		}
		
		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_RAZAOSOCIAL_FORNECEDOR))){
			ValidacaoCampoObrigatorio bean = new ValidacaoCampoObrigatorio() {
				@Override
				public String getPrefixo() {
					return "";
				}
				
				@Override
				public Boolean getPelomenosumregistro() {
					return false;
				}
				
				@Override
				public String getCondicao() {
					return "";
				}
				
				@Override
				public String getCampo() {
					return "razaosocial";
				}
				
				@Override
				public String getDisplayNameCampo() {
					return "Raz�o Social";
				}
				
				@Override
				public String getDisplayNameLista() {
					return null;
				}

				@Override
				public void setDisplayNameCampo(String string) {
				}

				@Override
				public void setDisplayNameLista(String string) {
				}
			};
			
			lista.add(bean);
		}
		
		return lista;
	}

}
