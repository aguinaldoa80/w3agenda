package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.InventarioMaterialHistorico;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.service.InventarioMaterialHistoricoService;
import br.com.linkcom.sined.geral.service.InventariomaterialService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.InventariomaterialFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/suprimento/crud/Inventariomaterial", authorizationModule=CrudAuthorizationModule.class)
public class InventariomaterialCrud extends CrudControllerSined<InventariomaterialFiltro, Inventariomaterial, Inventariomaterial>{
	InventarioMaterialHistoricoService inventarioMaterialHistoricoService;
	InventariomaterialService inventarioMaterialService;
	
	public void setInventarioMaterialHistoricoService(
			InventarioMaterialHistoricoService inventarioMaterialHistoricoService) {
		this.inventarioMaterialHistoricoService = inventarioMaterialHistoricoService;
	}
	
	public void setInventarioMaterialService(
			InventariomaterialService inventarioMaterialService) {
		this.inventarioMaterialService = inventarioMaterialService;
	}



	@Override
	public ModelAndView doCriar(WebRequestContext request, Inventariomaterial form)throws CrudException {
		throw new SinedException("Não é permitido criar item de inventário.");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Inventariomaterial form)	throws CrudException {
		throw new SinedException("Não é permitido excluir item de inventário.");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Inventariomaterial form) throws Exception {
		if(form != null && form.getCdinventariomaterial()!=null){
			form.setListaInventarioMaterialHistorico(inventarioMaterialHistoricoService.findByInventarioMaterialHistorico(form));
		}
		form.ajustaBeanToLoad();
	}
	
	@Override
	protected void salvar(WebRequestContext request, Inventariomaterial bean) throws Exception {
		Boolean criar = bean.getCdinventariomaterial() == null;
		if(!criar){
			Inventariomaterial inventarioMaterialAntigo = inventarioMaterialService.loadForEntrada(bean);
			if(!inventarioMaterialAntigo.getGerarBlocoH().equals(bean.getGerarBlocoH())){
				inventarioMaterialHistoricoService.preencherHistorico(bean, null, bean.getGerarBlocoH());
			}
			if(!inventarioMaterialAntigo.getGerarBlocoK().equals(bean.getGerarBlocoK())){
				inventarioMaterialHistoricoService.preencherHistorico(bean, bean.getGerarBlocoK(), null);
			}
		}else{
			inventarioMaterialHistoricoService.preencherHistorico(bean, null, null);
		}
		
		bean.ajustaBeanToSave();
		super.salvar(request, bean);
	}
	
	
	public ModelAndView ajaxVerificaBloco(WebRequestContext request ) {
		String ids = request.getParameter("ids").replace(" ", "");
		Boolean blocoH = false;
		Boolean blocoK = false;
		for (String id: ids.split(",")){
			Inventariomaterial inventarioMaterial = inventarioMaterialService.findGerarBloco(id);
			if(inventarioMaterial!=null){
				if(inventarioMaterial.getGerarBlocoH()!=null){
					if(inventarioMaterial.getGerarBlocoH().equals(Boolean.TRUE)){
						blocoH = true;
					}
				}
				if(inventarioMaterial.getGerarBlocoK()!=null){
					if(inventarioMaterial.getGerarBlocoK().equals(Boolean.TRUE)){
						blocoK = true;
					}
				}
			}
		}
		return new JsonModelAndView().addObject("blocoH", blocoH).addObject("blocoK", blocoK);
	}
	
	public ModelAndView gerarNaoGerarBlocos (WebRequestContext request) throws Exception{
		String bloco = request.getParameter("Blocoacao");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.trim().equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		for (String id: whereIn.split(",")){
			Inventariomaterial inventarioMaterial = inventarioMaterialService.loadForEntrada(inventarioMaterialService.findGerarBloco(id));
			if(bloco.equals("GERARBLOCOK")){
				inventarioMaterial.setGerarBlocoK(Boolean.TRUE);
			}
			if(bloco.equals("NAOGERARBLOCOK")){
				inventarioMaterial.setGerarBlocoK(Boolean.FALSE);
			}
			if(bloco.equals("GERARBLOCOH")){
				inventarioMaterial.setGerarBlocoH(Boolean.TRUE);
			}
			if(bloco.equals("NAOGERARBLOCOH")){
				inventarioMaterial.setGerarBlocoH(Boolean.FALSE);
			}
			salvar(request, inventarioMaterial);
		}
		return sendRedirectToAction("listagem");
	}
}
