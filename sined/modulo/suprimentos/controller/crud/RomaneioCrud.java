package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialnumeroserieService;
import br.com.linkcom.sined.geral.service.MaterialrelacionadoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.geral.service.RomaneioorigemService;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioItemBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RomaneioFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/suprimento/crud/Romaneio", authorizationModule=CrudAuthorizationModule.class)
public class RomaneioCrud extends CrudControllerSined<RomaneioFiltro, Romaneio, Romaneio> {

	private MaterialService materialService;
	private RomaneioService romaneioService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ContratoService contratoService;
	private PatrimonioitemService patrimonioitemService;
	private MovpatrimonioService movpatrimonioService;
	private LocalarmazenagemService localarmazenagemService;
	private EnderecoService enderecoService;
	private RomaneioorigemService romaneioorigemService;
	private MaterialnumeroserieService materialnumeroserieService;
	private ProjetoService projetoService;
	private ReportTemplateService reportTemplateService;
	private ParametrogeralService parametroGeralService;
	private InventarioService inventarioService;
	private MaterialrelacionadoService materialrelacionadoService;
	
	public void setParametroGeralService(ParametrogeralService parametroGeralService) {this.parametroGeralService = parametroGeralService;}
	public void setRomaneioorigemService(
			RomaneioorigemService romaneioorigemService) {
		this.romaneioorigemService = romaneioorigemService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setMovpatrimonioService(
			MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}
	public void setPatrimonioitemService(
			PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setMaterialnumeroserieService(MaterialnumeroserieService materialnumeroserieService) {
		this.materialnumeroserieService = materialnumeroserieService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}
	
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, RomaneioFiltro filtro) throws Exception {
		List<Romaneiosituacao> listaRomaneio = new ArrayList<Romaneiosituacao>();
		listaRomaneio.add(Romaneiosituacao.EM_ABERTO);
		listaRomaneio.add(Romaneiosituacao.BAIXADA);
		listaRomaneio.add(Romaneiosituacao.CANCELADA);
				
		List<String> listaMostrar = new ArrayList<String>();
		listaMostrar.add("Simples Remessa Gerada");
		listaMostrar.add("Devolu��o de Contrato");
		listaMostrar.add("Remessa de Contrato");
		listaMostrar.add("Sem Simples Remessa");
		listaMostrar.add("Outros");
		
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null){
			listaContrato = contratoService.findBycliente(filtro.getCliente());	
			if(listaContrato == null) listaContrato = new ArrayList<Contrato>();
		}
		
		if (filtro.getMaterial()!=null){
			filtro.setMaterial(materialService.load(filtro.getMaterial()));
			filtro.getMaterial().setListaMaterialnumeroserie(new ListSet<Materialnumeroserie>(Materialnumeroserie.class, materialnumeroserieService.findByCdsmaterial(filtro.getMaterial().getCdmaterial().toString())));
		}
		
		request.setAttribute("listaContrato", listaContrato);		
		request.setAttribute("listasituacoesromaneio", listaRomaneio);
		request.setAttribute("listaMostrar", listaMostrar);
	}
	
	@Override
	protected Romaneio criar(WebRequestContext request, Romaneio form) throws Exception {		
		Romaneio bean = super.criar(request, form);
		
		if (parametroGeralService.getBoolean(Parametrogeral.VALIDAR_ITENSKIT_TODOSLOCAIS) && request.getParameter("popupMatDisponivel") != null) {
			
			Localarmazenagem localOrigem = localarmazenagemService.load(new Localarmazenagem(Integer.parseInt(request.getParameter("localOrigem")), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
			bean.setLocalarmazenagemorigem(localOrigem);
			
			Localarmazenagem localDestino = localarmazenagemService.load(new Localarmazenagem(Integer.parseInt(request.getParameter("localDestino")), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
			bean.setLocalarmazenagemdestino(localDestino);
			
			Material material = materialService.unidadeMedidaMaterial(new Material(Integer.parseInt(request.getParameter("cdmaterial"))));
			Romaneioitem item = new Romaneioitem();
			item.setMaterial(material);
			bean.getListaRomaneioitem().add(item);
					
		}
		
		bean.setRomaneiosituacao(Romaneiosituacao.EM_ABERTO);
		bean.setDtromaneio(SinedDateUtils.currentDate());
		
		String gerarRomaneioMov = request.getParameter("gerarRomaneioMov");
		if(gerarRomaneioMov != null && "true".equals(gerarRomaneioMov)){
			romaneioService.criaRomaneioByMovimentacao(request, bean);
		}		
		
		String tarefas = request.getParameter("tarefas");
		if(tarefas != null && !tarefas.trim().equals("")){
			romaneioService.criaRomaneioByTarefa(tarefas, bean);
		}
		
		if("true".equals(request.getParameter("copiar")) && form.getCdromaneio() != null){
			form = romaneioService.criarCopia(form);
			return form;
		}
		
		return bean;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Romaneio bean) throws Exception {
		if(bean.getRomaneiosituacao() == null && bean.getCdromaneio() == null){
			bean.setRomaneiosituacao(Romaneiosituacao.EM_ABERTO);
		}
		
		String whereInTarefa = bean.getWhereInTarefa();
		
		super.salvar(request, bean);
		
		if(!StringUtils.isEmpty(whereInTarefa)){
			String[] ids = whereInTarefa.split(",");
			for (String cdtarefa : ids) {
				Romaneioorigem romaneioorigem = new Romaneioorigem();
				romaneioorigem.setRomaneio(bean);
				romaneioorigem.setTarefa(new Tarefa(Integer.parseInt(cdtarefa)));
				romaneioorigemService.saveOrUpdate(romaneioorigem);
			}
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Romaneio form) throws Exception {
		if(form.getCdromaneio() != null){
			List<OrigemsuprimentosBean> listaOrigem = romaneioService.montaOrigem(form);
			if(SinedUtil.isListNotEmpty(listaOrigem)){
				request.setAttribute("origem", listaOrigem.get(0));
			}
			request.setAttribute("listaOrigem", listaOrigem);
			request.setAttribute("listaDestino", romaneioService.montaDestino(form));
		}
		
		Boolean isDescricaoObrigatorio = true;		
		
		if(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.OBRIGAR_DESCRICAO_ROMANEIO).toUpperCase().equals("FALSE")){
			isDescricaoObrigatorio = false;
		}
		Boolean permissaoProjeto = false;
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			permissaoProjeto = StringUtils.isNotBlank(SinedUtil.getListaProjeto());
		}
		request.setAttribute("listaProjetos", projetoService.findProjetosAtivos(form.getProjeto(), permissaoProjeto));
		
		HashMap<Integer, List<Materialclasse>> mapMaterialClasse = new HashMap<Integer, List<Materialclasse>>();
		if (form.getListaRomaneioitem()!=null){
			for (Romaneioitem romaneioitem: form.getListaRomaneioitem()){
				if (romaneioitem.getMaterial()!=null && mapMaterialClasse.get(romaneioitem.getMaterial().getCdmaterial()) == null){
					mapMaterialClasse.put(romaneioitem.getMaterial().getCdmaterial(), materialService.findClasses(romaneioitem.getMaterial()));
				}
			}
		}
		request.setAttribute("RATEIO_MOVIMENTACAO_ESTOQUE", parametroGeralService.getBoolean(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE));
		request.setAttribute("mapMaterialClasse", mapMaterialClasse);
		request.setAttribute("descricaoObrigatorio", isDescricaoObrigatorio);
	}
	
	@Override
	protected void validateBean(Romaneio bean, BindException errors){
		if(bean.getListaRomaneioitem() == null || bean.getListaRomaneioitem().size() == 0){
			errors.reject("001", "N�o h� itens para gerar o romaneio.");
		} else{
			StringBuilder itensComEstoqueInsuficiente = new StringBuilder();
			Material materialAux = null;
			Empresa empresa = romaneioService.getEmpresaOrigem(bean.getEmpresa(), bean.getLocalarmazenagemorigem());
			for (Romaneioitem it : bean.getListaRomaneioitem()){
				if(it.getQtde() == null || it.getQtde() == 0){
					errors.reject("002", "Existe(m) item(ns) com valor da Qtde. vazio ou igual a 0.");
					break;
				} else{
//					String paramEstoquenegativo = parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO);
					if(!localarmazenagemService.getPermitirestoquenegativo(bean.getLocalarmazenagemorigem())){
						if(it.getMaterialclasse() == null || !it.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
							Double qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServicoFromConferenciaComReserva(it.getMaterial(), it.getMaterialclasse(), bean.getLocalarmazenagemorigem(), empresa, bean.getProjeto(), it.getLoteestoque());
							Double qtde = it.getQtde();
							
							if(qtde > qtdeDisponivel){
								materialAux = materialService.load(it.getMaterial(), "material.cdmaterial, material.identificacao, material.nome");
								if(materialAux != null){
									itensComEstoqueInsuficiente.append("<BR>".concat(materialAux.getIdentificacao()
																					.concat(" - ").concat(materialAux.getNome())));
								}
							}
						}
					}
				}
			}
 
			if(itensComEstoqueInsuficiente.length()>0){
				errors.reject("003", "Os materiais abaixo possuem estoque insuficiente:".
									concat(itensComEstoqueInsuficiente.toString()));
			}
		}
		if(bean.getDtromaneio().getTime() > System.currentTimeMillis()){
			errors.reject("004", "A data do romaneio n�o pode ser maior que a data atual.");
		}
	}
	
	/**
	 * M�todo que monta a URL de retorno para as a��es da tela.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			String id = request.getParameter("selectedItens");
			if(id != null && !id.equals(""))
				action += "&cdromaneio="+id;
		}
		return new ModelAndView("redirect:"+action);
	}
	
	/**
	 * M�todo que faz a valida��o para a emiss�o de nota dos romaneios.
	 * Depois redireciona para o m�todo de cria��o da nota de produto.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public ModelAndView emitirNota(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Localarmazenagem localarmazenagem = null;
		List<Romaneio> listaRomaneio = romaneioService.findForEmissaoNotaValidacao(whereIn);
		for (Romaneio romaneio : listaRomaneio) {
			if(localarmazenagem == null){
				localarmazenagem = romaneio.getLocalarmazenagemdestino();
			} else if(!localarmazenagem.equals(romaneio.getLocalarmazenagemdestino())){
				request.addError("Para emitir a nota todos os romaneios tem que ter o mesmo local de destino.");
				return getControllerModelAndView(request);
			}
		}
		
		return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=criar&romaneios=" + whereIn);
	}
	
	/**
	 * Action que realiza a baixa do(s) romaneio(s) selecionado(s).
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public ModelAndView baixar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Romaneio> listaRomaneio = romaneioService.findForBaixar(whereIn);
		boolean erro = false;
		
		for (Romaneio romaneio : listaRomaneio) {
			if(romaneio.getRomaneiosituacao() != null && !romaneio.getRomaneiosituacao().equals(Romaneiosituacao.EM_ABERTO)){
				request.addError("Para realizar a baixa de romaneio(s), este(s) tem que estar na situa��o 'EM ABERTO'.");
				return getControllerModelAndView(request);
			}
			
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				if(SinedUtil.isListNotEmpty(romaneio.getListaRomaneioitem())){
					for (Romaneioitem item : romaneio.getListaRomaneioitem()) {
						if(item.getMaterial() == null || item.getMaterial().getMaterialRateioEstoque() == null || item.getMaterial().getMaterialRateioEstoque().getContaGerencialRomaneioEntrada() == null || item.getMaterial().getMaterialRateioEstoque().getContaGerencialRomaneioEntrada() == null){
							request.addError("� obrigat�rio informar a conta gerencial de romaneio (entrada) e conta gerencial  de romaneio (sa�da) no cadastro de material.");
							return getControllerModelAndView(request);
						}
					}
				}else{
					request.addError("� obrigat�rio informar a conta gerencial de romaneio (entrada) e conta gerencial  de romaneio (sa�da) no cadastro de material.");
					return getControllerModelAndView(request);
				}
			}
			
			if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), romaneio.getLocalarmazenagemorigem(), romaneio.getProjeto())) {
				request.addError("N�o � poss�vel realizar a baixa,  pois o registro est� vinculado � um registro de invent�rio. Para baixar, � necess�rio cancelar o invent�rio.");
				return getControllerModelAndView(request);
			}
			
			if (inventarioService.hasEstoqueEscrituradoNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), romaneio.getEmpresa(), romaneio.getLocalarmazenagemdestino(), romaneio.getProjeto())) {
				request.addError("N�o � poss�vel realizar a baixa,  pois o registro est� vinculado � um registro de invent�rio. Para baixar, � necess�rio cancelar o invent�rio.");
				return getControllerModelAndView(request);
			}
		}
		
//		if("FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO))){
			for (Romaneio romaneio : listaRomaneio) {
				List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
				if(listaRomaneioitem != null && listaRomaneioitem.size() > 0){
					Empresa empresa = romaneioService.getEmpresaOrigem(romaneio.getEmpresa(), romaneio.getLocalarmazenagemorigem());
					for (Romaneioitem romaneioitem : listaRomaneioitem) {
						if(!materialService.validateObrigarLote(romaneioitem, request)){
							erro = true;
						}
						if(!localarmazenagemService.getPermitirestoquenegativo(romaneio.getLocalarmazenagemorigem())){
							if(romaneioitem.getMaterialclasse() == null || !romaneioitem.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
								Double qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServicoFromConferenciaComReserva(romaneioitem.getMaterial(), romaneioitem.getMaterialclasse(), romaneio.getLocalarmazenagemorigem(), empresa, romaneio.getProjeto(), romaneioitem.getLoteestoque());
								Double qtde = romaneioitem.getQtde();
								
								if(qtde > qtdeDisponivel){
									erro = true;
									request.addError("O material " + romaneioitem.getMaterial().getNome() + " n�o tem quantidade suficiente para a baixa do romaneio " + romaneio.getCdromaneio() + ".");
								}
							}
						}
					}
				}
			}
//		}
		
		for (Romaneio romaneio : listaRomaneio) {
			List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
			if(listaRomaneioitem != null && listaRomaneioitem.size() > 0){
				for (Romaneioitem romaneioitem : listaRomaneioitem) {
					if(romaneioitem.getMaterialclasse() != null && 
							romaneioitem.getMaterialclasse().equals(Materialclasse.PATRIMONIO) &&
							romaneioitem.getPatrimonioitem() != null){
						Double qtdeDisponivel = movpatrimonioService.qtdeDisponivelPatrimonio(null, romaneioitem.getPatrimonioitem(), romaneio.getLocalarmazenagemorigem());
						Double qtde = romaneioitem.getQtde();
						
						if(qtde > qtdeDisponivel){
							erro = true;
							request.addError("O material " + romaneioitem.getMaterial().getNome() + " n�o tem quantidade suficiente para a baixa do romaneio " + romaneio.getCdromaneio() + ".");
						}
					}
				}
			}
		}
		
		if(erro){
			return getControllerModelAndView(request);
		}
		
		for (Romaneio romaneio : listaRomaneio) {
			Localarmazenagem localarmazenagemorigem = romaneio.getLocalarmazenagemorigem();
			Empresa empresaorigem = romaneioService.getEmpresaOrigem(romaneio.getEmpresa(), localarmazenagemorigem);
			Empresa empresadestino = romaneio.getEmpresa();
			Cliente clientedestino = romaneio.getCliente();
			
			Localarmazenagem localarmazenagemdestino = romaneio.getLocalarmazenagemdestino();
			if(localarmazenagemdestino != null && localarmazenagemdestino.getCdlocalarmazenagem() != null){
				Empresa empresa = romaneioService.getEmpresaUnicaLocalarmazenagem(localarmazenagemdestino);
				if(empresa != null) empresadestino = empresa;
			}
			
			List<Romaneioitem> listaRomaneioitem = romaneio.getListaRomaneioitem();
			if(listaRomaneioitem != null && listaRomaneioitem.size() > 0){
				for (Romaneioitem romaneioitem : listaRomaneioitem) {
					if(romaneioitem.getQtde() > 0){
						romaneioService.gerarEntradaSaidaRomaneio(romaneio, romaneioitem, romaneioitem.getMaterial().getValorcusto(), empresadestino, null, null, empresaorigem, null, true, clientedestino, romaneioitem.getLoteestoque(), null, null);
					}
				}
			}
		}
		romaneioService.doUpdateSituacaoRomaneio(whereIn, Romaneiosituacao.BAIXADA);
		
		request.addMessage("Romaneio(s) baixado(s) com sucesso.");
		return getControllerModelAndView(request);
	}
	
	/**
	 * M�todo que busca informa��es do material via ajax.
	 *
	 * @param request
	 * @param romaneioitem
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public ModelAndView buscarInfMaterialAjax(WebRequestContext request, Romaneioitem romaneioitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(romaneioitem.getMaterial() != null){
			Material material = materialService.load(romaneioitem.getMaterial(), "material.cdmaterial, material.unidademedida, material.patrimonio, " +
																					"material.produto, material.epi, material.servico, material.codigobarras");
			List<Materialclasse> listaMaterialclasse = null;
			if(material != null){
				listaMaterialclasse = materialService.findClassesWithoutLoad(material);
				
				if(material.getUnidademedida() != null && material.getUnidademedida().getSimbolo() != null){
					jsonModelAndView.addObject("unidademedidasimbolo", material.getUnidademedida().getSimbolo());
				}
				if(material.getCodigobarras() != null){
					jsonModelAndView.addObject("codigobarras", material.getCodigobarras());
				}
			} else {
				listaMaterialclasse = materialService.findClasses(material);
			}
			
			if(listaMaterialclasse != null && listaMaterialclasse.size() > 0){
				jsonModelAndView.addObject("listaMaterialclasse", listaMaterialclasse);
			}
		}
		return jsonModelAndView;
	}
	
	/**
	 * M�todo para cancelar romaneio
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView cancelar(WebRequestContext request){
		romaneioService.cancelar(request, Boolean.FALSE);
		return getControllerModelAndView(request);
	}
	
	public void ajaxComboContato(WebRequestContext request, RomaneioFiltro filtro){
		
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		
		if (filtro.getCliente()!=null){
			listaContrato = contratoService.findBycliente(filtro.getCliente());			
		}
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaContrato, "listaContrato", "descricaoIdentificador"));
	}
	
	/**
	 * A��o em ajax para buscar os itens de patrim�nio de um material.
	 *
	 * @param request
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2013
	 */
	public ModelAndView buscarPatrimonioitemAjax(WebRequestContext request, Romaneioitem romaneioitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(romaneioitem != null && romaneioitem.getMaterial() != null){
			Patrimonioitemsituacao patrimonioitemsituacao = Patrimonioitemsituacao.DISPONIVEL;
			if("TRUE".equalsIgnoreCase(request.getParameter("fromFechamento"))){
				patrimonioitemsituacao = Patrimonioitemsituacao.ALOCADO;
			}
			Integer cdpatrimonioitem = romaneioitem.getPatrimonioitem() != null ? romaneioitem.getPatrimonioitem().getCdpatrimonioitem() : null;
			List<Patrimonioitem> listaPatrimonioitem = patrimonioitemService.findByMaterial(romaneioitem.getMaterial(), cdpatrimonioitem, patrimonioitemsituacao);
			jsonModelAndView.addObject("listaPatrimonioitem", listaPatrimonioitem);
		}
		
		return jsonModelAndView;
	}
	
	/**
	 * Action em ajax para carregar os endere�os do cliente.
	 *
	 * @param request
	 * @param contratofaturalocacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/12/2013
	 */
	public ModelAndView carregaEnderecoByClienteAjax(WebRequestContext request, Contratofaturalocacao contratofaturalocacao){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(contratofaturalocacao == null || 
				contratofaturalocacao.getCliente() == null ||
				contratofaturalocacao.getCliente().getCdpessoa() == null){
			return jsonModelAndView;
		}
		Cliente cliente = contratofaturalocacao.getCliente();
		
		List<Endereco> listaEndereco = enderecoService.findByCliente(cliente);
		jsonModelAndView.addObject("listaEndereco", listaEndereco);
		
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxEstoqueMaterialLocal(WebRequestContext request, Localarmazenagem localarmazenagem){
		request.getSession().setAttribute("localarmazenagemForAutocompleteMaterial", localarmazenagem);
		request.getSession().setAttribute("serieForAutocompleteMaterial", Boolean.TRUE);
		return new JsonModelAndView();
	}

	/**
	 * M�todo que faz a valida��o para a emiss�o de nota deseervi�o dos romaneios.
	 * Depois redireciona para o m�todo de cria��o da nota de servi�o.
	 *
	 * @param request
	 * @return
	 * @author Lucas Costa
	 * @since 17/04/2014
	 */
	public ModelAndView emitirNotaServico(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		Localarmazenagem localarmazenagem = null;
		List<Romaneio> listaRomaneio = romaneioService.findForEmissaoNotaValidacao(whereIn);
		for (Romaneio romaneio : listaRomaneio) {
			if(localarmazenagem == null){
				localarmazenagem = romaneio.getLocalarmazenagemdestino();
			} else if(!localarmazenagem.equals(romaneio.getLocalarmazenagemdestino())){
				request.addError("Para emitir a nota todos os romaneios tem que ter o mesmo local de destino.");
				return getControllerModelAndView(request);
			}
		}
		
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico?ACAO=criar&romaneios=" + whereIn);
	}
	
	public void ajaxVerificaContratoFinalizado(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		List<Romaneio> listaRomaneio = romaneioService.findForCancelar(whereIn);
		String string = "var achou = false;";
		StringBuilder msgUltimo = new StringBuilder();
		for (Romaneio romaneio : listaRomaneio) {
			if(romaneio.getListaRomaneioorigem() != null && !romaneio.getListaRomaneioorigem().isEmpty()){
				StringBuilder whereInContrato = new StringBuilder();
				for(Romaneioorigem romaneioorigem : romaneio.getListaRomaneioorigem()){
					if(romaneioorigem.getContrato() != null && romaneioorigem.getContrato().getCdcontrato() != null){
						if(!whereInContrato.toString().contains(romaneioorigem.getContrato().getCdcontrato().toString())){
							if(!whereInContrato.toString().equals(""))
								whereInContrato.append(",");
							whereInContrato.append(romaneioorigem.getContrato().getCdcontrato());
						}
					}else if(romaneioorigem.getContratofechamento() != null && romaneioorigem.getContratofechamento().getCdcontrato() != null){
						if(!whereInContrato.toString().contains(romaneioorigem.getContratofechamento().getCdcontrato().toString())){
							if(!whereInContrato.toString().equals(""))
								whereInContrato.append(",");
							whereInContrato.append(romaneioorigem.getContratofechamento().getCdcontrato());
						}
					}
				}
				
				if(whereInContrato != null && !whereInContrato.toString().equals("") && 
						!romaneioService.isUltimoRomaneioGeradoByContrato(romaneio, whereInContrato.toString())){
					msgUltimo.append("N�o � permitido cancelar o romaneio " + romaneio.getCdromaneio() + ". Este, n�o � o �ltimo romaneio gerado. \\n");
				}
				if(whereInContrato != null && !whereInContrato.toString().equals("")){
					if(contratoService.existeContratoFinalizado(whereInContrato.toString())){
						string = "var achou = true;";
					}
				}
			}
		}
		View.getCurrent().println(string);
		View.getCurrent().println("var msgUltimo = '" + msgUltimo.toString() + "';");
	}

	public ModelAndView plaquetaSerieAjax(WebRequestContext request, Romaneioitem romaneioitem){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		String plaquetaSerie = "";
		
		if (romaneioitem.getMaterial()!=null){
			List<Patrimonioitem> listaPatrimonioitem = patrimonioitemService.findByMaterial(romaneioitem.getMaterial());
			if (!listaPatrimonioitem.isEmpty()){
				String plaqueta = "Plaqueta: ";
				String serie = "S�rie: ";
				
				for (Patrimonioitem patrimonioitem: listaPatrimonioitem){
					if (patrimonioitem.getPlaqueta()!=null && !patrimonioitem.getPlaqueta().trim().equals("")){
						plaqueta += patrimonioitem.getPlaqueta() + ", "; 
					}
					if (patrimonioitem.getBempatrimonio()!=null && patrimonioitem.getBempatrimonio().getListaMaterialnumeroserie()!=null){
						serie += CollectionsUtil.listAndConcatenate(patrimonioitem.getBempatrimonio().getListaMaterialnumeroserie(), "numero", ", "); 
						serie += ", "; 
					}
				}
				
				if (plaqueta.endsWith(", ")){
					plaqueta = plaqueta.substring(0, plaqueta.length()-2);
				}
				
				if (serie.endsWith(", ")){
					serie = serie.substring(0, serie.length()-2);
				}
				
				plaquetaSerie = plaqueta + " / " + serie + "<br/>";
			}
		}
		
		jsonModelAndView.addObject("plaquetaSerie", plaquetaSerie);
		
		return jsonModelAndView;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#ajaxVerificaObrigatoriedadeLote(WebRequestContext request)
	*
	* @param request
	* @return
	* @since 07/12/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificaObrigatoriedadeLote(WebRequestContext request){
		return materialService.ajaxVerificaObrigatoriedadeLote(request);
	}
	
	public void ajaxFindMaterialForCodigobarras(WebRequestContext request, Romaneio romaneio){
		String codigobarras = request.getParameter("codigobarras");
		List<Material> lista = materialService.findMaterialEstoqueByCodigobarras(codigobarras, romaneio.getLocalarmazenagemorigem());
		String bean = "";
		String beannome = "";
		if(lista != null && !lista.isEmpty()){
			if(lista.size() == 1){
				Material material = lista.get(0);
				bean = "br.com.linkcom.sined.geral.bean.Material[cdmaterial=" + material.getCdmaterial() + ",nome=" + new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(material.getNome()) + "]";
				beannome = material.getNome();
			}
		}
		
		if("".equals(bean)){
			View.getCurrent().println("var unico = false;");
		}else {
			View.getCurrent().println("var unico = true;");
			View.getCurrent().println("var bean = \"" + bean + "\";");
			View.getCurrent().println("var beannome = \"" + beannome + "\";");
		}		
	}
	
	/**
	* M�todo que abre uma popup para o usu�rio definir qual template de romaneio ele deseja imprimir
	*
	* @param request
	* @return ModelAndView
	* @since 13/03/2018
	* @authorFabricio Berg
	*/
	public ModelAndView abrirImprimirRomaneio(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request,"/suprimento/crud/Romaneio");
			return null;
		}
		
		List<ReportTemplateBean> templates = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.ROMANEIO, "nome");
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("romaneioTemplates", templates);
		
		return new ModelAndView("direct:/crud/popup/romaneioTemplate");
	}
	
	/**
	* Buscar a lista de template de romaneio
	*
	* @param request
	* @return ModelAndView
	* @since 13/03/2018
	* @author Fabricio Berg
	*/
	public ModelAndView ajaxListaTemplatesRomaneio(WebRequestContext request) {
		JsonModelAndView json = new JsonModelAndView();
		
		List<ReportTemplateBean> reportTemplateBean = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.ROMANEIO);
		json.addObject("romaneioTemplatesSize", reportTemplateBean.size());
		
		if(reportTemplateBean.size() == 1){
			json.addObject("cdtemplate", reportTemplateBean.get(0).getCdreporttemplate());
		}
		
		return json;
	}
}