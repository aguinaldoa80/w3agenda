package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ColetaService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.EntregadocumentoService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialCustoEmpresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MovimentacaoEstoqueHistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueorigemService;
import br.com.linkcom.sined.geral.service.NotaVendaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorEntradaSaida;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovimentacaoestoqueFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/suprimento/crud/Movimentacaoestoque", authorizationModule=CrudAuthorizationModule.class)
public class MovimentacaoestoqueCrud extends CrudControllerSined<MovimentacaoestoqueFiltro, Movimentacaoestoque, Movimentacaoestoque>{

	private MaterialService materialService;
	private EmpresaService empresaService;
	private LocalarmazenagemService localarmazenagemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MaterialtipoService materialtipoService;
	private MaterialgrupoService materialgrupoService;
	private UnidademedidaService unidademedidaService;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private EntregaService entregaService;
	private ProjetoService projetoService;
	private NotaVendaService notaVendaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private EntregadocumentoService entregadocumentoService;
	private ColaboradorService colaboradorService;
	private LoteestoqueService loteestoqueService;
	private ParametrogeralService parametrogeralService;
	private ColetaService coletaService;
	private CentrocustoService centroCustoService;
	private ContagerencialService contaGerencialService;
	private RateioService rateioService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	private MaterialCustoEmpresaService materialCustoEmpresaService;
	private InventarioService inventarioService;
	
	public void setInventarioService(InventarioService inventarioService) {this.inventarioService = inventarioService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setContaGerencialService(ContagerencialService contaGerencialService) {this.contaGerencialService = contaGerencialService;}
	public void setCentroCustoService(CentrocustoService centroCustoService) {this.centroCustoService = centroCustoService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}	
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMovimentacaoestoqueorigemService(
			MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {
		this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;
	}
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setNotaVendaService(NotaVendaService notaVendaService) {
		this.notaVendaService = notaVendaService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setEntregadocumentoService(EntregadocumentoService entregadocumentoService) {
		this.entregadocumentoService = entregadocumentoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setColetaService(ColetaService coletaService) {
		this.coletaService = coletaService;
	}
	public void setMovimentacaoEstoqueHistoricoService(
			MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {
		this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;
	}
	public void setMaterialCustoEmpresaService(MaterialCustoEmpresaService materialCustoEmpresaService) {this.materialCustoEmpresaService = materialCustoEmpresaService;}	
	
	@Override
	protected void listagem(WebRequestContext request,	MovimentacaoestoqueFiltro filtro) throws Exception {
		this.getRegistros(request);
		
		List<Movimentacaoestoquetipo> listatipos = new ArrayList<Movimentacaoestoquetipo>();
		listatipos.add(Movimentacaoestoquetipo.ENTRADA);
		listatipos.add(Movimentacaoestoquetipo.SAIDA);
		
		if (filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null)
			filtro.setMaterial(materialService.carregaMaterial(filtro.getMaterial()));
		
		request.setAttribute("listatipos", listatipos);
		
		request.setAttribute("listaTipo", materialtipoService.findAtivos());
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		
		String whereInEmpresaUsuarioLogado = new SinedUtil().getListaEmpresa();
		request.setAttribute("listaLocalarmazenagem", localarmazenagemService.findByEmpresa(null, whereInEmpresaUsuarioLogado));
		request.setAttribute("utilizaControleGado", parametrogeralService.getBoolean("CONTROLE_GADO"));
		super.listagem(request, filtro);
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, MovimentacaoestoqueFiltro filtro) {
		List<TotalizadorEntradaSaida> listaTotais = movimentacaoestoqueService.getTotalValoresEntradaSaida(filtro);
		if(listaTotais != null && !listaTotais.isEmpty()){
			for(TotalizadorEntradaSaida totalizadorEntradaSaida : listaTotais){
				if(totalizadorEntradaSaida.getMovimentacaoestoquetipo() != null){
					if(Movimentacaoestoquetipo.ENTRADA.equals(totalizadorEntradaSaida.getMovimentacaoestoquetipo())){
						filtro.setTotalqtdeentradas(totalizadorEntradaSaida.getQtde());
						filtro.setTotalvalorentradas(SinedUtil.roundByParametro(totalizadorEntradaSaida.getValor()));
						filtro.setTotalpesoentradas(SinedUtil.roundByParametro(totalizadorEntradaSaida.getPesototal()));
						filtro.setTotalpesobrutoentradas(SinedUtil.roundByParametro(totalizadorEntradaSaida.getPesobrutototal()));
					}else {
						filtro.setTotalqtdesaidas(totalizadorEntradaSaida.getQtde());
						filtro.setTotalvalorsaidas(SinedUtil.roundByParametro(totalizadorEntradaSaida.getValor()));
						filtro.setTotalpesosaidas(SinedUtil.roundByParametro(totalizadorEntradaSaida.getPesototal()));
						filtro.setTotalpesobrutosaidas(SinedUtil.roundByParametro(totalizadorEntradaSaida.getPesobrutototal()));
					}
				}
			}
		}
		
		return super.getListagemModelAndView(request, filtro);
	}
	
	@Override
	protected ListagemResult<Movimentacaoestoque> getLista(WebRequestContext request, MovimentacaoestoqueFiltro filtro) {
		ListagemResult<Movimentacaoestoque> list = super.getLista(request, filtro);
		List<Movimentacaoestoque> lista = list.list();
		if(lista != null && lista.size() > 0){
			for (Movimentacaoestoque movimentacaoestoque : lista) {
				if(movimentacaoestoque.getLoteestoque() != null){
					movimentacaoestoque.getLoteestoque().montaDescriptionAutocomplete(movimentacaoestoque.getMaterial());
				}
			}
		}
		return list;
	}
	
	private void getRegistros(WebRequestContext request) {
		List<Materialclasse> listaclasses = new ArrayList<Materialclasse>();
		listaclasses.add(Materialclasse.EPI);
		listaclasses.add(Materialclasse.PRODUTO);
		listaclasses.add(Materialclasse.SERVICO);
		
		request.setAttribute("listaclasses", listaclasses);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Movimentacaoestoque bean) {
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		return super.getSalvarModelAndView(request, bean);
	}

	@Override
	protected void entrada(WebRequestContext request, Movimentacaoestoque form)	throws Exception {
		if(SinedUtil.isRateioMovimentacaoEstoque()){
			if(form.getQtde() != null && (form.getValor() != null || form.getValorcusto() != null)){
				form.setValorTotal(new Money(form.getQtde()*(form.getValor() != null ? form.getValor() : form.getValorcusto())));
			}
			if(form.getCdmovimentacaoestoque() == null){
				if(form.getRateio() == null) form.setRateio(new Rateio());
				if(SinedUtil.isListEmpty(form.getRateio().getListaRateioitem())){
					form.getRateio().setListaRateioitem(new ArrayList<Rateioitem>());
					form.getRateio().getListaRateioitem().add(new Rateioitem(null, new Money(), 100d));
				}
			}
		}
		if (form.getCdmovimentacaoestoque() != null) {
			form.setListaMovimentacaoEstoqueHistorico(movimentacaoEstoqueHistoricoService.findByMovimentacaoEstoque(form));
			form.setMovimentacaoAvulsa(form.getMovimentacaoestoqueorigem() == null ||
									(form.getMovimentacaoestoqueorigem().getEntrega() == null && form.getMovimentacaoestoqueorigem().getEntregamaterial() == null
									&& form.getMovimentacaoestoqueorigem().getExpedicao() == null && form.getMovimentacaoestoqueorigem().getExpedicaoproducao() == null
									&& form.getMovimentacaoestoqueorigem().getInventario() == null && form.getMovimentacaoestoqueorigem().getNotafiscalproduto() == null
									&& form.getMovimentacaoestoqueorigem().getNotaFiscalProdutoItem() == null && form.getMovimentacaoestoqueorigem().getOrdemservicoveterinaria() == null
									&& form.getMovimentacaoestoqueorigem().getOrdemservicoveterinaria() == null && form.getMovimentacaoestoqueorigem().getPedidovenda() == null
									&& form.getMovimentacaoestoqueorigem().getProducaoagenda() == null && form.getMovimentacaoestoqueorigem().getProducaoAgendaMaterial() == null
									&& form.getMovimentacaoestoqueorigem().getProducaoordem() == null && form.getMovimentacaoestoqueorigem().getProducaoOrdemMaterial() == null
									&& form.getMovimentacaoestoqueorigem().getRequisicao() == null && form.getMovimentacaoestoqueorigem().getRequisicaomaterial() == null
									&& form.getMovimentacaoestoqueorigem().getRomaneio() == null && form.getMovimentacaoestoqueorigem().getVeiculodespesa() == null
									&& form.getMovimentacaoestoqueorigem().getVenda() == null));
		}
		if(form.getUnidademedidatrans() != null && form.getUnidademedidatrans().getCdunidademedida() != null){
			request.setAttribute("unidademedidamaterialTrans", form.getUnidademedidatrans() != null ? 
					"br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + form.getUnidademedidatrans().getCdunidademedida() + "]": "");
		}
		Boolean visualizarValor = SinedUtil.isUserHasAction(Acao.VISUALIZAR_VALOR_CUSTO_ESTOQUE);
		request.setAttribute("visualizarValor", visualizarValor);
		
		if(form.getLoteestoque() != null && form.getMaterial() != null){
			form.getLoteestoque().setValidade(loteestoqueService.getValidadeByMaterialLote(form.getLoteestoque(), form.getMaterial()));
		}
		
		this.getRegistros(request);
		if(form.getCdmovimentacaoestoque() == null){
			Movimentacaoestoqueorigem origem = new Movimentacaoestoqueorigem();
			origem.setUsuario((Usuario)Neo.getUser());
			form.setDtmovimentacao(new Date(System.currentTimeMillis()));
			form.setMovimentacaoestoqueorigem(origem);
		}else if(form.getMovimentacaoestoqueorigem() != null && form.getMovimentacaoestoqueorigem().getColaborador() != null){
			form.getMovimentacaoestoqueorigem().setColaborador(colaboradorService.load(form.getMovimentacaoestoqueorigem().getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
		}else if(form.getMovimentacaoestoqueorigem() != null && form.getMovimentacaoestoqueorigem().getColeta() != null){
			form.getMovimentacaoestoqueorigem().setColeta(coletaService.load(form.getMovimentacaoestoqueorigem().getColeta(), "coleta.cdcoleta, coleta.dtconfirmacaodevolucao"));
		}

		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		
		if(form.getMaterialclasse() != null && form.getMaterialclasse().getCdmaterialclasse() != null){
			request.setAttribute("materialclasseid", form.getMaterialclasse().getCdmaterialclasse());
		}
		
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		if(form.getMaterial() != null){
			form.setUnidademedidatrans(form.getMaterial().getUnidademedida());
			request.setAttribute("unidademedidamaterial", form.getMaterial().getUnidademedida() != null ? 
					"br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + form.getMaterial().getUnidademedida().getCdunidademedida() + "]": "");
		}
		
		request.setAttribute("listaUnidademedida", listaUnidademedida);
		request.setAttribute("listaOrigem", movimentacaoestoqueService.montaOrigem(form));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));

		if(request.getBindException().hasErrors()){
			form.setMaterial(materialService.load(form.getMaterial()));
		}
		
		List<Projeto> projeto = new ArrayList<Projeto>();
		if(form.getProjeto() != null){
			projeto.add(form.getProjeto());
		}
		
		List<Projeto> listaProjeto = projetoService.findForComboByUsuariologado(projeto, true);
		request.setAttribute("listaProjeto", listaProjeto);
		
		request.getSession().setAttribute("MATERIAL_LOTE", form.getMaterial());
		
		List<String> listaCamposAutocompleteMaterial = new ArrayList<String>();
		listaCamposAutocompleteMaterial.add(MovimentacaoestoqueFiltro.TODOS);
		listaCamposAutocompleteMaterial.add(MovimentacaoestoqueFiltro.CODIGO_BARRAS);
		listaCamposAutocompleteMaterial.add(MovimentacaoestoqueFiltro.IDENTIFICACAO);
		listaCamposAutocompleteMaterial.add(MovimentacaoestoqueFiltro.NOME);
		
		if(form.getTipobuscamaterial() == null || form.getTipobuscamaterial().equals("")){
			form.setTipobuscamaterial(MovimentacaoestoqueFiltro.TODOS);
		}
		
		form.setEmpresaAnterior(form.getEmpresa());
		
		request.setAttribute("listaCentrocusto", centroCustoService.findAtivos());
		request.setAttribute("listaCentrocusto", centroCustoService.findAtivos());
		request.setAttribute("listaContasGerencial", contaGerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		request.setAttribute("listaCamposAutocompleteMaterial", listaCamposAutocompleteMaterial);
		request.setAttribute("utilizaControleGado", parametrogeralService.getBoolean("CONTROLE_GADO"));
	}
	
	@Override
	protected void validateBean(Movimentacaoestoque bean, BindException errors) {
//		String paramEstoquenegativo = parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO);
		if(!localarmazenagemService.getPermitirestoquenegativo(bean.getLocalarmazenagem())){
			materialService.validateObrigarLote(bean, errors);
			Double qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico( bean.getMaterial(), bean.getMaterialclasse(), bean.getLocalarmazenagem(), bean.getEmpresa(), bean.getProjeto(), null, bean.getLoteestoque());
			Movimentacaoestoque me = new Movimentacaoestoque(bean.getMaterial(), 
					bean.getMovimentacaoestoquetipo(), 
					bean.getQtde(), 
					bean.getLocalarmazenagem(), 
					bean.getEmpresa(), 
					bean.getMovimentacaoestoqueorigem(), 
					bean.getMaterialclasse(), 
					bean.getProjeto());
			me.setUnidademedidatrans(bean.getUnidademedidatrans());
			if(me.getMaterial() != null){
				verificaUnidademedidaQtde(me);	
			}
			if(bean.getCdmovimentacaoestoque() != null){
				Movimentacaoestoque aux_mov = movimentacaoestoqueService.loadForEntrada(bean);
				if(aux_mov != null && aux_mov.getMovimentacaoestoquetipo() != null && aux_mov.getQtde() != null){
					if(Movimentacaoestoquetipo.ENTRADA.equals(aux_mov.getMovimentacaoestoquetipo())){
						qtdeDisponivel -= aux_mov.getQtde();
						if((qtdeDisponivel + me.getQtde()) < 0){
							errors.reject("001", "A Quantidade de entrada insuficiente para deixar estoque positivo.");
						}
					} else {
						Localarmazenagem localarmazenagemAntigo = aux_mov.getLocalarmazenagem();
						Localarmazenagem localarmazenagemNovo = bean.getLocalarmazenagem();
						
						if((localarmazenagemAntigo != null && localarmazenagemNovo != null && localarmazenagemAntigo.equals(localarmazenagemNovo)) || 
								(localarmazenagemAntigo == null && localarmazenagemNovo == null)){
							qtdeDisponivel += aux_mov.getQtde();
						}
						if(me.getQtde() > qtdeDisponivel){
							errors.reject("001", "A Quantidade desejada n�o esta dispon�vel.");
						}
					}
					
				}
			} else {
				if(me.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.SAIDA)){
					if(me.getQtde() > qtdeDisponivel){
						errors.reject("001", "A Quantidade desejada n�o esta dispon�vel.");
					}
				}
			}
		}
		
		if(SinedUtil.isRateioMovimentacaoEstoque()){
			try {
				rateioService.validateRateio(bean.getRateio(), bean.getValorTotal());
			} catch (SinedException e) {
				errors.reject("001",e.getMessage());
			}
		}
		
		if(bean.getLocalarmazenagem() != null && bean.getEmpresa() != null && !localarmazenagemService.isLocalEmpresa(bean.getLocalarmazenagem(), bean.getEmpresa())){
			errors.reject("001", "A empresa n�o pertence ao local de armazenagem.");
		}
	}

	@Override
	protected void salvar(WebRequestContext request, Movimentacaoestoque bean) throws Exception {
		boolean criar = bean.getCdmovimentacaoestoque() == null;
		
		boolean alteracaoLote = false;
		Movimentacaoestoque movimentacaoAntiga = null;
		StringBuilder lotesAlterados = new StringBuilder();
		if(!criar){
			movimentacaoAntiga = movimentacaoestoqueService.findWithLoteestoque(bean);
			if(bean.getLoteestoque() != null && !bean.getLoteestoque().equals(movimentacaoAntiga.getLoteestoque())){
				Loteestoque loteNovo = loteestoqueService.load(bean.getLoteestoque(), "loteestoque.numero");
				alteracaoLote = true;
				lotesAlterados.append("Lote anterior: ")
				.append(movimentacaoAntiga != null && movimentacaoAntiga.getLoteestoque() != null ? movimentacaoAntiga.getLoteestoque().getNumero() : "Sem lote")
				.append("  -  Lote atual: ")
				.append(loteNovo.getNumero() + ".");
			}else if(bean.getLoteestoque() == null && movimentacaoAntiga.getLoteestoque() != null){
				alteracaoLote = true;
				lotesAlterados.append("Lote anterior: ")
				.append(movimentacaoAntiga.getLoteestoque().getNumero())
				.append("  -  Lote atual: Sem lote.");
			}
		}
		
		
		if(inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(bean)){
			throw new SinedException("N�o � poss�vel realizar inclus�o ou altera��o, pois o registro compreende a um per�odo com invent�rio registrado.");
		}
		
		if(bean.getMaterial() != null){
			verificaUnidademedidaQtde(bean);	
		}
		
		if (bean.getMovimentacaoestoqueorigem() != null && bean.getMovimentacaoestoqueorigem().getCdmovimentacaoestoqueorigem() != null){
			bean.setMovimentacaoestoqueorigem(movimentacaoestoqueorigemService.load(bean.getMovimentacaoestoqueorigem()));
		}
		
		super.salvar(request, bean);
		
		if(criar){
			movimentacaoEstoqueHistoricoService.preencherHistorico(bean, MovimentacaoEstoqueAcao.CRIAR, "", true);
		}else if(alteracaoLote){
			movimentacaoEstoqueHistoricoService.preencherHistorico(bean, MovimentacaoEstoqueAcao.ALTERAR, lotesAlterados.toString(), true);
		}else {
			movimentacaoEstoqueHistoricoService.preencherHistorico(bean, MovimentacaoEstoqueAcao.ALTERAR, "", true);
		}
		
		if(bean.getMaterial() != null && bean.getMaterial().getCdmaterial() != null){
			List<MaterialCustoEmpresa> listaMaterialCustoEmpresa = materialCustoEmpresaService.findByMaterial(bean.getMaterial(), null);
			
			if(parametrogeralService.getBoolean(Parametrogeral.ATUALIZA_MATERIALCUSTO_MOVIMENTACAOENTREGA)){
				Movimentacaoestoque movimentacaoestoque = movimentacaoestoqueService.loadUltimaMovimentacaoestoque(bean.getMaterial(), null);
				
				if(SinedUtil.isListEmpty(listaMaterialCustoEmpresa)) {
					materialService.preencherMaterialCustoPorEmpresa(movimentacaoestoque.getMaterial(), movimentacaoestoque, null);
					listaMaterialCustoEmpresa = materialCustoEmpresaService.findByMaterial(bean.getMaterial(), null);
				}
				
				if(SinedUtil.isListNotEmpty(listaMaterialCustoEmpresa)) {
					for(MaterialCustoEmpresa materialCustoEmpresa : listaMaterialCustoEmpresa) {
						Movimentacaoestoque movimentacaoestoquePorEmpresa = movimentacaoestoqueService.loadUltimaMovimentacaoestoque(bean.getMaterial(), materialCustoEmpresa.getEmpresa());
						
						if(bean.getEmpresa() != null 
								&& bean.getEmpresa().equals(materialCustoEmpresa.getEmpresa())
								&& movimentacaoestoquePorEmpresa != null 
								&& movimentacaoestoquePorEmpresa.getValor() != null 
								&& movimentacaoestoquePorEmpresa.getMaterial() != null) {
							
							materialCustoEmpresaService.updateValorUltimaCompra(materialCustoEmpresa, SinedUtil.roundByParametro(movimentacaoestoquePorEmpresa.getValor()));
							materialCustoEmpresaService.updateValorCusto(materialCustoEmpresa, SinedUtil.roundByParametro(movimentacaoestoquePorEmpresa.getValor()));
						}
					}
				}

				if(movimentacaoestoque != null && movimentacaoestoque.getValor() != null && movimentacaoestoque.getMaterial() != null){
					materialService.updateValorCusto(movimentacaoestoque.getMaterial(), SinedUtil.roundByParametro(movimentacaoestoque.getValor()));
					materialService.recalcularValorvenda(movimentacaoestoque.getMaterial().getCdmaterial().toString());
					materialService.recalcularMinioMaximo(movimentacaoestoque.getMaterial().getCdmaterial().toString());
				}
			} else {
				materialService.recalcularCustoMedio(bean.getMaterial().getCdmaterial().toString(), bean);
				
				if(SinedUtil.isListNotEmpty(listaMaterialCustoEmpresa)) {
					for(MaterialCustoEmpresa materialCustoEmpresa : listaMaterialCustoEmpresa) {
						if(bean.getEmpresa() != null && bean.getEmpresa().equals(materialCustoEmpresa.getEmpresa())) {
							materialCustoEmpresaService.updateValorUltimaCompra(materialCustoEmpresa, bean.getValor());
						}
					}
				}
			}
		}
		
		if(bean.getValor() == null && parametrogeralService.getBoolean(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE)){
			movimentacaoestoqueService.atualizaValorRateioPorCustoMedeio(bean.getCdmovimentacaoestoque());
		}
	}
	
	public void colocarTipobuscamaterialSessao(WebRequestContext request){
		request.getSession().setAttribute("MOVIMENTACAOESTOQUE_TIPOBUSCAMATERIAL", request.getParameter("tipobuscamaterial"));	
	}
	
	/**
	 * M�todo que verifica a unidademedida do material e converte a qtde caso necess�rio 
	 *
	 * @param movimentacaoestoque
	 * @author Luiz Fernando
	 */
	public void verificaUnidademedidaQtde(Movimentacaoestoque movimentacaoestoque) {
		if(movimentacaoestoque != null && movimentacaoestoque.getMaterial() != null && movimentacaoestoque.getUnidademedidatrans() != null && 
				movimentacaoestoque.getUnidademedidatrans().getCdunidademedida() != null){
			Material material = materialService.unidadeMedidaMaterial(movimentacaoestoque.getMaterial());
			if(material != null && material.getUnidademedida() != null && material.getUnidademedida().getCdunidademedida() != null && 
					!movimentacaoestoque.getUnidademedidatrans().getCdunidademedida().equals(material.getUnidademedida().getCdunidademedida())){
				movimentacaoestoque.setQtde(unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), 
												movimentacaoestoque.getQtde(), 
												movimentacaoestoque.getUnidademedidatrans(), 
												material,null,1.0));
				if(movimentacaoestoque.getValor() != null){
					Double fracao = unidademedidaService.getFracaoconversaoUnidademedida(material.getUnidademedida(), movimentacaoestoque.getQtde(), movimentacaoestoque.getUnidademedidatrans(), material, null);
					if(fracao != null){
						movimentacaoestoque.setValor(movimentacaoestoque.getValor()*fracao);
					}
				}
			}
		}
		
		Material aux_material = materialService.load(movimentacaoestoque.getMaterial(), "material.cdmaterial, material.unidademedida");
		if(aux_material != null && aux_material.getUnidademedida() != null){
			movimentacaoestoque.setQtde(SinedUtil.roundByUnidademedida(movimentacaoestoque.getQtde(), aux_material.getUnidademedida()));
		}
	}
	
	/** Fun��o ajax que retorna o s�mbolo da unidade de medida do material
	 * @param request
	 * @param material
	 * @author Tom�s Rabelo
	 */
	public void getSimboloUnidadeMedidaAjax(WebRequestContext request, Material material){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		Unidademedida unidadeMedidaSelecionada = new Unidademedida();
		String simbolo = materialService.getSimboloUnidadeMedida(material);
		material = materialService.unidadeMedidaMaterial(material);
		
		if (material != null){
			List<Unidademedida> lista = unidademedidaService.unidadeAndUnidadeRelacionada(material.getUnidademedida());
			Material bean = materialService.findListaMaterialunidademedida(material);
			
			if(bean != null){
				List<Unidademedida> unidadesMedidaAll = new ArrayList<Unidademedida>();
				unidadesMedidaAll.add(bean.getUnidademedida());
				
				if(bean.getListaMaterialunidademedida() != null && !bean.getListaMaterialunidademedida().isEmpty()){
					for(Materialunidademedida materialunidademedida : bean.getListaMaterialunidademedida()){
						if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().getCdunidademedida() != null){
							lista.add(materialunidademedida.getUnidademedida());
						}
					}
				}
				
				if(lista != null && lista.size() > 0){
					for (Unidademedida item : lista) {
						unidadesMedidaAll.add(item);
						if(item.getListaUnidademedidaconversao() != null && item.getListaUnidademedidaconversao().size() > 0){
							for (Unidademedidaconversao item2 : item.getListaUnidademedidaconversao()) {
								if(item2.getUnidademedida() != null)
									unidadesMedidaAll.add(item2.getUnidademedida());
								if(item2.getUnidademedidarelacionada() != null)
									unidadesMedidaAll.add(item2.getUnidademedidarelacionada());
							}
						}
					}
				}			
				
				if(unidadesMedidaAll != null && unidadesMedidaAll.size() > 0){
					for (Unidademedida unidademedida : unidadesMedidaAll) {
						if(listaUnidademedida == null || listaUnidademedida.size() == 0 || !listaUnidademedida.contains(unidademedida)){
							listaUnidademedida.add(unidademedida);
						}
					}
				}		
				
			}else {
				if(lista!=null && !lista.isEmpty()){
					listaUnidademedida = unidademedidaService.setListUnidademedidaWithConversao(lista);
				}else{
					listaUnidademedida.add(material.getUnidademedida());	
				}				
			}
			unidadeMedidaSelecionada = material.getUnidademedida();
		}
		
		Boolean exibircomprimento = materialService.isMaterialComAlturaLargura(material);
		request.getServletResponse().setContentType("text/html");

		view.println("var simbolo = '" + simbolo + "';");
		view.println("var exibircomprimento = '" + (exibircomprimento != null && exibircomprimento ? "true" : "false") + "';");
		view.println(SinedUtil.convertToJavaScript(listaUnidademedida, "listaUnidademedida", ""));
		view.println("var unidadeMedidaSelecionada = '" + (unidadeMedidaSelecionada != null ? 
				"br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + unidadeMedidaSelecionada.getCdunidademedida() + "]" : "") + "';");
	}
	
	/**
	 * M�todo ajax que converte a qtde de acordo com a unidademedida escolhida
	 *
	 * @param request
	 * @param material
	 * @author Luiz Fernando
	 */
	public void ajaxConverteQtdeUnidademedida(WebRequestContext request, Material material){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		String cdunidademedidatrans = request.getParameter("cdunidademedidatrans");
		String qtdestr = request.getParameter("qtde");
		Boolean efetuarconversao = Boolean.FALSE;
		Double qtde = 0.0;
		if(cdunidademedidatrans != null && !"".equals(cdunidademedidatrans) && qtdestr != null && !"".equals(qtdestr)){
			Unidademedida unidademedidarelacionada = new Unidademedida();
			unidademedidarelacionada.setCdunidademedida(Integer.parseInt(cdunidademedidatrans));
			material = materialService.unidadeMedidaMaterial(material);
			if(material != null && material.getUnidademedida() != null && material.getUnidademedida().getCdunidademedida() != null && 
					!material.getUnidademedida().getCdunidademedida().equals(unidademedidarelacionada.getCdunidademedida())){
				qtde = unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), Double.parseDouble(qtdestr), unidademedidarelacionada, material,null,1.0);
				efetuarconversao = Boolean.TRUE;
			}
		}
		
		request.getServletResponse().setContentType("text/html");
		view.println("var efetuarconversao = '" + efetuarconversao + "';");
		view.println("var qtde = '" + qtde + "';");
	}
	
	/**
	 * M�todo que retorna os tipos de classes poss�veis para o material
	 * 
	 * @param request
	 * @param material
	 * @author Tom�s Rabelo
	 */
	@Action("materialClassesAjax")
	public ModelAndView materialClassesAjax(WebRequestContext request, Material material){
		List<Materialclasse> listaClasses = materialService.findClasses(material);
		listaClasses.remove(Materialclasse.PATRIMONIO);
		
		return new JsonModelAndView().addObject("lista", listaClasses);
	}
	
	/**
	 * M�todo que cancela as movimenta��es
	 * 
	 * @param request
	 * @param movimentacaoestoque
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView cancelar(WebRequestContext request, Movimentacaoestoque movimentacaoestoque) throws CrudException{
		String whereIn = request.getParameter("selectedItens");
		
		List<Movimentacaoestoque> movimentacoes = movimentacaoestoqueService.findMovimentacoes(whereIn);
		boolean erro = false;
		for (Movimentacaoestoque movimentacaoestoque2 : movimentacoes) {
            if(movimentacaoestoque2.getDtcancelamento() != null){
            	request.addError("N�o � poss�vel cancelar movimenta��o que j� est� cancelada. Movimenta��o "+ movimentacaoestoque2.getCdmovimentacaoestoque());
				erro = true;
				break;
            }
			if(movimentacaoestoque2.getMovimentacaoestoqueorigem() != null && movimentacaoestoque2.getMovimentacaoestoqueorigem().getVenda() != null){
				Venda venda = movimentacaoestoque2.getMovimentacaoestoqueorigem().getVenda();
				if(!venda.getVendasituacao().equals(Vendasituacao.CANCELADA)){
					request.addError("S� � possivel cancelar registro que a situa��o da venda � CANCELADA. Movimenta��o "+ movimentacaoestoque2.getCdmovimentacaoestoque());
					erro = true;
					break;
				}
			}
			if(movimentacaoestoque2.getMovimentacaoestoqueorigem() != null && movimentacaoestoque2.getMovimentacaoestoqueorigem().getInventario() != null){
				Inventario inventario = movimentacaoestoque2.getMovimentacaoestoqueorigem().getInventario();
				if(inventario.getInventarioSituacao() != null && !inventario.getInventarioSituacao().equals(InventarioSituacao.CANCELADO)){
					request.addError("N�o � poss�vel realizar o cancelamento, pois o registro est� vinculado � um registro de invent�rio. Para cancelar, � necess�rio cancelar o invent�rio. Movimenta��o "+ movimentacaoestoque2.getCdmovimentacaoestoque());
					erro = true;
					break;
				}
			}
			if(inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(movimentacaoestoque2)){
				request.addError("N�o � poss�vel cancelar o registro de movimenta��o, pois compreende a um per�odo com invent�rio registrado. Movimenta��o "+ movimentacaoestoque2.getCdmovimentacaoestoque());
				erro = true;
				break;
			}
		}
		if(!erro){
			movimentacoes = movimentacaoestoqueService.findMovimentacoesForQtdedisponivel(whereIn);
			Double qtdeDisponivel;
			for (Movimentacaoestoque movimentacaoestoque2 : movimentacoes) {
				if(movimentacaoestoque2.getDtcancelamento() == null){
					if(!localarmazenagemService.getPermitirestoquenegativo(movimentacaoestoque2.getLocalarmazenagem())){
						if(movimentacaoestoque2.getMovimentacaoestoquetipo() != null && 
								movimentacaoestoque2.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.ENTRADA)){
							qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico( movimentacaoestoque2.getMaterial(), movimentacaoestoque2.getMaterialclasse(), movimentacaoestoque2.getLocalarmazenagem(), movimentacaoestoque2.getEmpresa(), movimentacaoestoque2.getProjeto(), false, movimentacaoestoque2.getLoteestoque());
							if(qtdeDisponivel != null && (qtdeDisponivel - movimentacaoestoque2.getQtde()) < 0){
								request.addError("Cancelamento n�o permitido. O estoque ficar� negativo. Movimenta��o "+ movimentacaoestoque2.getCdmovimentacaoestoque());
								erro = true;
								break;
							}
						}
					}
				}	
			}
		}
		if(!erro){
			List<Material> listaMaterial = new ArrayList<Material>();
			List<Movimentacaoestoque> listaMovimentacao = movimentacaoestoqueService.findMovimentacoesWithMaterial(whereIn);
			if(SinedUtil.isListNotEmpty(listaMovimentacao)){
				for (Movimentacaoestoque me : listaMovimentacao) {
					if(me.getMaterial() != null && me.getMaterial().getCdmaterial() != null){
						listaMaterial.add(me.getMaterial());
					}
				}
			}
			movimentacaoestoqueService.doUpdateMovimentacoes(whereIn);
			for (Movimentacaoestoque movimentacaoestoqueCancelar : listaMovimentacao) {
				movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoqueCancelar, MovimentacaoEstoqueAcao.CANCELAR, "", true);
			}
			List<Movimentacaoestoque> movimentacoesOrigemEntrega = movimentacaoestoqueService.findMovimentacoesEntrega(whereIn);
			StringBuilder whereInEntrega = new StringBuilder();
			List<Entrega> listaEntregaComMovNotCancelada = new ArrayList<Entrega>();
			for(Movimentacaoestoque m : movimentacoesOrigemEntrega){
				if(m.getMovimentacaoestoqueorigem() != null && m.getMovimentacaoestoqueorigem().getEntrega() != null && 
						m.getMovimentacaoestoqueorigem().getEntrega().getCdentrega() != null && !listaEntregaComMovNotCancelada.contains(m.getMovimentacaoestoqueorigem().getEntrega())){
					if(movimentacaoestoqueService.existeMovimentacaoestoqueByEntrega(m.getMovimentacaoestoqueorigem().getEntrega(), true)){
						listaEntregaComMovNotCancelada.add(m.getMovimentacaoestoqueorigem().getEntrega());
						continue;
					}
					if(!"".equals(whereInEntrega.toString())) whereInEntrega.append(",");
					whereInEntrega.append(m.getMovimentacaoestoqueorigem().getEntrega().getCdentrega());
				}
			}
			if(!"".equals(whereInEntrega.toString())){
				entregaService.updateDtBaixa(null, whereInEntrega.toString());
			}
			if(SinedUtil.isListNotEmpty(listaMaterial)){
				ListIterator<Material> iterator = listaMaterial.listIterator(); 		  
		        while (iterator.hasNext()){ 
		        	Material mat = iterator.next();
		        	Movimentacaoestoque mov = movimentacaoestoqueService.loadUltimaMovimentacaoestoque(mat, null);
					if(parametrogeralService.getBoolean(Parametrogeral.ATUALIZA_MATERIALCUSTO_MOVIMENTACAOENTREGA)){
						List<MaterialCustoEmpresa> listaMaterialCustoEmpresa = materialCustoEmpresaService.findByMaterial(mat, null);
						if(SinedUtil.isListEmpty(listaMaterialCustoEmpresa)) {
							materialService.preencherMaterialCustoPorEmpresa(mat, null, null);
							listaMaterialCustoEmpresa = materialCustoEmpresaService.findByMaterial(mat, null);
						}						
						if(SinedUtil.isListNotEmpty(listaMaterialCustoEmpresa)) {
							for(MaterialCustoEmpresa materialCustoEmpresa : listaMaterialCustoEmpresa) {
								Movimentacaoestoque movstoq = movimentacaoestoqueService.loadUltimaMovimentacaoestoque(mat, materialCustoEmpresa.getEmpresa());
								if(movstoq != null && movstoq.getValor() != null && movstoq.getMaterial() != null){
									materialCustoEmpresaService.updateValorCusto(materialCustoEmpresa, movstoq.getValor());
								}
							}
						} 
						if(movimentacaoestoque.getMaterial()!=null){
							Movimentacaoestoque movstoq = movimentacaoestoqueService.loadUltimaMovimentacaoestoque(mat, null);	
							if(movstoq != null && movstoq.getValor() != null && movstoq.getMaterial() != null){
								materialService.updateValorCusto(movstoq.getMaterial(), movstoq.getValor());
								materialService.recalcularValorvenda(movstoq.getMaterial().getCdmaterial().toString());
								materialService.recalcularMinioMaximo(movstoq.getMaterial().getCdmaterial().toString());
							}					
						}
					}
					else if(parametrogeralService.getBoolean(Parametrogeral.CALCULAR_CUSTO_MEDIO)){
						materialService.recalcularCustoMedio(CollectionsUtil.listAndConcatenate(listaMaterial, "cdmaterial", ","));
					}
		        }
			}
			request.addMessage("Registro(s) cancelado(s) com sucesso.", MessageType.INFO);
		}
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * M�todo chamado a partir da tela gerenciamento material. Inst�ncia um novo filtro com novos valores
	 * e coloca na se��o
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @throws CrudException
	 * @author Tom�s Rabelo
	 * @throws IOException 
	 */
	public ModelAndView preparaFiltroFromGerenciamentoMaterial(WebRequestContext request, MovimentacaoestoqueFiltro filtro) throws CrudException, IOException{
		SinedUtil.permissaoAcaoTela(request, "listagem");
		
		String cdmaterialGerenciamentoestoque = request.getParameter("cdmaterialGerenciamentoestoque");
		String cdmaterialclasseGerenciamentoestoque = request.getParameter("cdmaterialclasseGerenciamentoestoque");
		String cdempresaGerenciamentoestoque = request.getParameter("cdempresaGerenciamentoestoque");
		String cdlocalarmazenagemGerenciamentoestoque = request.getParameter("cdlocalarmazenagemGerenciamentoestoque");
		String cdprojetoGerenciamentoestoque = request.getParameter("cdprojetoGerenciamentoestoque");
		String cdloteestoqueGerenciamentoestoque = request.getParameter("cdloteestoqueGerenciamentoestoque");
		
		if(StringUtils.isNotBlank(cdmaterialGerenciamentoestoque)) filtro.setMaterial(new Material(Integer.parseInt(cdmaterialGerenciamentoestoque)));
		if(StringUtils.isNotBlank(cdmaterialclasseGerenciamentoestoque)) filtro.setMaterialclasse(new Materialclasse(Integer.parseInt(cdmaterialclasseGerenciamentoestoque)));
		if(StringUtils.isNotBlank(cdempresaGerenciamentoestoque)) filtro.setEmpresa(new Empresa(Integer.parseInt(cdempresaGerenciamentoestoque)));
		if(StringUtils.isNotBlank(cdlocalarmazenagemGerenciamentoestoque)) filtro.setLocalarmazenagem(new Localarmazenagem(Integer.parseInt(cdlocalarmazenagemGerenciamentoestoque)));
		if(StringUtils.isNotBlank(cdprojetoGerenciamentoestoque)) filtro.setProjeto(new Projeto(Integer.parseInt(cdprojetoGerenciamentoestoque)));
		if(StringUtils.isNotBlank(cdloteestoqueGerenciamentoestoque)) filtro.setLoteestoque(new Loteestoque(Integer.parseInt(cdloteestoqueGerenciamentoestoque)));
		
		if(filtro.getMaterialclasse() != null){
			if(filtro.getMaterialclasse().equals(Materialclasse.EPI)){
				filtro.setEpi(true);
			} else if (filtro.getMaterialclasse().equals(Materialclasse.PRODUTO)) {
				filtro.setProduto(true);
			} else if (filtro.getMaterialclasse().equals(Materialclasse.SERVICO)) {
				filtro.setServico(true);
			}
		}
		
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			filtro.setWhereInItemgrade(materialService.getWhereInItensgradeOrMaterialmestregrade(filtro.getMaterial()));
		}
		if(filtro.getLocalarmazenagem() != null && StringUtils.isEmpty(filtro.getLocalarmazenagem().getNome())){
			Localarmazenagem localarmazenagem = localarmazenagemService.load(filtro.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome");
			if(localarmazenagem != null){
				filtro.getLocalarmazenagem().setNome(localarmazenagem.getNome());
			}
		}
		if(filtro.getLoteestoque() != null && filtro.getLoteestoque().getCdloteestoque() != null){
			Loteestoque loteestoque = loteestoqueService.carregaLoteestoque(filtro.getLoteestoque());
			if(loteestoque != null){
				filtro.setLoteestoque(loteestoque);
			}
		}
		
		filtro.setDatade(null);
		filtro.setDataate(null);
		
		request.getSession().setAttribute(this.getClass().getSimpleName()+"CONTROLLER"+filtro.getClass().getName(), filtro);
		return continueOnAction("listagem", filtro);
	}
	
	
	public ModelAndView gerarCsv(WebRequestContext request, MovimentacaoestoqueFiltro filtro){

		List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findForCsv(filtro);
		
		StringBuilder rel = new StringBuilder();
		
		rel.append("Movimenta��o;");
		rel.append("Tipo;");
		rel.append("Data;");
		rel.append("Identificador;");
		rel.append("Produto/EPI/Servi�o;");
		rel.append("Unidade de Medida;");
		rel.append("Quantidade;");
		rel.append("Valor;");
		rel.append("Local;");
		rel.append("Origem;"); 
		rel.append("N�mero da Nota;");
		rel.append("Canceladas;");
		rel.append("\n");
				
		for (Movimentacaoestoque movestq : listaMovimentacaoestoque) {
			
			Integer movimentacao = movestq.getCdmovimentacaoestoque();
			if(movimentacao != null) rel.append(new Integer(movimentacao));
			rel.append(";");
			
			if(movestq.getMovimentacaoestoquetipo() != null) rel.append(SinedUtil.escapeCsv(movestq.getMovimentacaoestoquetipo().getNome()));
			rel.append(";"); 

			if(movestq.getDtmovimentacao() != null) rel.append(SinedDateUtils.toString(movestq.getDtmovimentacao()));
			rel.append(";");
			
			if(movestq.getMaterial() != null && movestq.getMaterial().getIdentificacaoListagem() != null) 
				rel.append(SinedUtil.escapeCsv("'"+movestq.getMaterial().getIdentificacaoListagem()+"'"));
			rel.append(";");
			
			if(movestq.getMaterial() != null) rel.append(SinedUtil.escapeCsv(movestq.getMaterial().getNome()));
			rel.append(";");
			
			if(movestq.getMaterial() != null && movestq.getMaterial().getUnidademedida() != null) rel.append(SinedUtil.escapeCsv(movestq.getMaterial().getUnidademedida().getNome()));
			rel.append(";");
			
			Double qtde = movestq.getQtde();
			if(qtde != null) rel.append(SinedUtil.descriptionDecimal(SinedUtil.round(qtde, 4)));
			rel.append(";");
			
			Double valor = movestq.getValor();
			if(valor != null) rel.append(SinedUtil.roundByParametro(valor).toString().replace(".", ","));
			rel.append(";");
			
			if(movestq.getLocalArmazenagemDescricao() != null) rel.append(SinedUtil.escapeCsv(movestq.getLocalArmazenagemDescricao()));
			rel.append(";");
			
			if(movestq.getOrigemreport() != null  && !movestq.getLocalArmazenagemDescricao().equals("")) 
				rel.append(SinedUtil.escapeCsv(movestq.getOrigemreport()));
			rel.append(";");
			
			if(movestq.getMovimentacaoestoqueorigem() != null){
				if(movestq.getMovimentacaoestoqueorigem().getNotafiscalproduto() != null){ 
					rel.append(SinedUtil.escapeCsv(movestq.getMovimentacaoestoqueorigem().getNotafiscalproduto().getNumero()));
				}else{
					if (movestq.getMovimentacaoestoqueorigem().getVenda() != null){
						List<NotaVenda> vendaNota = notaVendaService.findByVenda(movestq.getMovimentacaoestoqueorigem().getVenda(), false);
						for (int i=0; i < vendaNota.size(); i++){
							Notafiscalproduto notafiscalproduto = new Notafiscalproduto();
							notafiscalproduto.setCdNota(vendaNota.get(i).getNota().getCdNota());
							notafiscalproduto = notafiscalprodutoService.load(notafiscalproduto);
							if (notafiscalproduto !=null){
								rel.append(SinedUtil.escapeCsv(notafiscalproduto.getNumero()));
								break;
							}
						}
					}
					else if (movestq.getMovimentacaoestoqueorigem().getEntrega() != null){
						List<Entregadocumento> listaEntregadocumento = entregadocumentoService.findListEntregaDocumentoByEntrega(movestq.getMovimentacaoestoqueorigem().getEntrega());
						Notafiscalproduto notafiscalproduto = new Notafiscalproduto();
						StringBuilder sb = new StringBuilder ();
						for (Entregadocumento entregadocumento : listaEntregadocumento) {
							
							sb.append(entregadocumentoService.load(entregadocumento, "numero")+", ");
						}
						notafiscalproduto.setNumero(sb.toString());
						rel.append(SinedUtil.escapeCsv(notafiscalproduto.getNumero()));
						
					}
				}
			}
			rel.append(";");
			
			if(movestq.getDtcancelamento() != null) rel.append(SinedDateUtils.toString(movestq.getDtcancelamento()));
			rel.append(";");
				
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","entrada_saida_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#ajaxVerificaObrigatoriedadeLote(WebRequestContext request)
	*
	* @param request
	* @return
	* @since 07/12/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificaObrigatoriedadeLote(WebRequestContext request){
		return materialService.ajaxVerificaObrigatoriedadeLote(request);
	}
	
	public ModelAndView comboBoxEmpresa(WebRequestContext request, Movimentacaoestoque movimentacaoestoque) {
		return new JsonModelAndView().addObject("listaEmpresa", empresaService.findAtivoByLocalarmazenagem(movimentacaoestoque.getLocalarmazenagem(), null));
	}
	
	public ModelAndView ajaxHabilitaCamposDeACordoComGrupoMaterial(WebRequestContext request, Movimentacaoestoque bean){
		ModelAndView retorno = new JsonModelAndView();
		if(bean.getCdmaterial() == null){
			return retorno.addObject("habilitaMotivoAnimal", false)
						.addObject("registrarPesoMedio", false);
		}
		Material material = materialService.load(new Material(bean.getCdmaterial()), "material.pesobruto, material.materialgrupo");
		
		if(material != null && material.getMaterialgrupo() != null){
			retorno.addObject("habilitaMotivoAnimal", Boolean.TRUE.equals(material.getMaterialgrupo().getAnimal()))
				.addObject("registrarPesoMedio", Boolean.TRUE.equals(material.getMaterialgrupo().getRegistrarpesomedio()))
				.addObject("pesoBruto", material.getPesobruto());
		}else{
			retorno.addObject("habilitaMotivoAnimal", false)
				.addObject("registrarPesoMedio", false);
		}
		return retorno;
	}
	
	public ModelAndView abrirRateio(WebRequestContext request){
		return rateioService.abrirRateio(request);
	}
	
	/**
	* Cria processo para que quando o campo tiver apenas uma op��o, j� traz esta op��o selecionada. Campos: Local, empresa e projeto
	*/
	@Override
	protected Movimentacaoestoque criar(WebRequestContext request, Movimentacaoestoque form) throws Exception {
		form = super.criar(request, form);		
		
		List<Localarmazenagem> listaLocaisArmazenagem = localarmazenagemService.findAtivos();
		 if(listaLocaisArmazenagem.size() == 1){
			 form.setLocalarmazenagem(listaLocaisArmazenagem.get(0));
		 }
		 
		 List<Empresa> listaEmpresas = empresaService.findAtivos();
		 if(listaEmpresas.size() == 1){
			 form.setEmpresa(listaEmpresas.get(0));
		 }
		 
		 List<Projeto> listaProjetos =  new ArrayList<Projeto>();
		 listaProjetos = projetoService.findProjetosAtivos(listaProjetos, false);
		 if(listaProjetos.size() == 1) {
			 form.setProjeto(listaProjetos.get(0));
		 }
		 
		return form;
	}
}
