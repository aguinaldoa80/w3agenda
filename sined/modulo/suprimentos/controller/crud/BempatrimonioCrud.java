package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/suprimento/crud/Bempatrimonio",
		authorizationModule=CrudAuthorizationModule.class
)
public class BempatrimonioCrud extends CrudControllerSined<MaterialFiltro, Bempatrimonio, Bempatrimonio>{

	private EmpresaService empresaService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;

	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, MaterialFiltro filtro) throws Exception {
		List<String> listaCodigos = new ArrayList<String>();
		listaCodigos.add(MaterialFiltro.CODIGO);
		listaCodigos.add(MaterialFiltro.CODIGO_ALTERNATIVO);
		listaCodigos.add(MaterialFiltro.CODIGO_BARRAS);
		listaCodigos.add(MaterialFiltro.CODIGO_FABRICANTE);
		listaCodigos.add(MaterialFiltro.IDENTIFICACAO);
		listaCodigos.add(MaterialFiltro.REFERENCIA);
		
		if(filtro.getTipo() == null || filtro.getTipo().equals(""))
			filtro.setTipo(MaterialFiltro.IDENTIFICACAO);
		
		request.setAttribute("listaCodigos", listaCodigos);
		request.setAttribute("listaEmpresa", empresaService.findAtivos());
		request.setAttribute("listaTipo", materialtipoService.findAtivos());
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
	}
	
	@Override
	protected void entrada(WebRequestContext request, Bempatrimonio form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade no sistema.");
	}
	
}
