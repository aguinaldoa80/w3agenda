package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.script.ScriptException;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Inspecaoitem;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialAtributosMaterial;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.bean.MaterialEcomerceExcluido;
import br.com.linkcom.sined.geral.bean.MaterialFaixaMarkup;
import br.com.linkcom.sined.geral.bean.MaterialProducaoEtapaItem;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialcolaborador;
import br.com.linkcom.sined.geral.bean.Materialempresa;
import br.com.linkcom.sined.geral.bean.Materialformulapeso;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialhistorico;
import br.com.linkcom.sined.geral.bean.Materialimposto;
import br.com.linkcom.sined.geral.bean.Materialkitflexivel;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.FaturamentoVeterinaria;
import br.com.linkcom.sined.geral.bean.enumeration.Materialacao;
import br.com.linkcom.sined.geral.bean.enumeration.StatusUsoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.FaixaMarkupNomeService;
import br.com.linkcom.sined.geral.service.FormulacustoService;
import br.com.linkcom.sined.geral.service.InspecaoitemService;
import br.com.linkcom.sined.geral.service.LotematerialService;
import br.com.linkcom.sined.geral.service.MaterialCustoEmpresaService;
import br.com.linkcom.sined.geral.service.MaterialEcomerceExcluidoService;
import br.com.linkcom.sined.geral.service.MaterialFaixaMarkupService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialformulacustoService;
import br.com.linkcom.sined.geral.service.MaterialformulamargemcontribuicaoService;
import br.com.linkcom.sined.geral.service.MaterialformulapesoService;
import br.com.linkcom.sined.geral.service.MaterialformulavalorvendaService;
import br.com.linkcom.sined.geral.service.MaterialformulavendamaximoService;
import br.com.linkcom.sined.geral.service.MaterialformulavendaminimoService;
import br.com.linkcom.sined.geral.service.MaterialfornecedorService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialhistoricoService;
import br.com.linkcom.sined.geral.service.MaterialimpostoService;
import br.com.linkcom.sined.geral.service.MaterialkitflexivelService;
import br.com.linkcom.sined.geral.service.MateriallocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialpneumodeloService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MaterialrelacionadoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MaterialunidademedidaService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.OrdemcompramaterialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PneumodeloService;
import br.com.linkcom.sined.geral.service.ProducaoetapaitemService;
import br.com.linkcom.sined.geral.service.ReservaService;
import br.com.linkcom.sined.geral.service.TabelaimpostoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.MaterialEtiquetaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.Materialajustarpreco;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.MaterialproducaoAjaxBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.MaterialHistoricoPrecoBean;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(path="/suprimento/crud/Material", authorizationModule=CrudAuthorizationModule.class)
public class MaterialCrud extends CrudControllerSined<MaterialFiltro, Material, Material>{

	private EmpresaService empresaService;
	private ContagerencialService contagerencialService;
	private MaterialgrupoService materialgrupoService;
	private MaterialService materialService;
	private MaterialtipoService materialtipoService;
	private UnidademedidaService unidademedidaService;
	private InspecaoitemService inspecaoitemService;
	private ArquivoService arquivoService;
	private MaterialproducaoService materialproducaoService;
	private MaterialhistoricoService materialhistoricoService;
	private MaterialunidademedidaService materialunidademedidaService;
	private MaterialformulapesoService materialformulapesoService;
	private MaterialrelacionadoService materialrelacionadoService;
	private OrdemcompramaterialService ordemcompramaterialService;
	private MaterialformulavalorvendaService materialformulavalorvendaService;
	private ProducaoetapaitemService producaoetapaitemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ParametrogeralService parametrogeralService;
	private FormulacustoService formulacustoService;
	private ReportTemplateService reportTemplateService;
	private LotematerialService lotematerialService;
	private MaterialkitflexivelService materialkitflexivelService;
	private MaterialformulacustoService materialformulacustoService;
	private MaterialformulamargemcontribuicaoService materialformulamargemcontribuicaoService;
	private TabelaimpostoService tabelaimpostoService;
	private MaterialimpostoService materialimpostoService;
	private CentrocustoService centrocustoService;
	private EntregaService entregaService;
	private MateriallocalarmazenagemService materiallocalarmazenagemService;
	private MaterialpneumodeloService materialpneumodeloService;
	private PneumodeloService pneumodeloService;
	private MaterialfornecedorService materialfornecedorService;
	private OrdemcompraService ordemcompraService;
	private MaterialformulavendaminimoService materialformulavendaminimoService;
	private MaterialformulavendamaximoService materialformulavendamaximoService;
	private ReservaService reservaService;
	private PedidovendaService pedidoVendaService;
	private PedidovendatipoService pedidovendatipoService;
	private MaterialCustoEmpresaService materialCustoEmpresaService;
	private MaterialFaixaMarkupService materialFaixaMarkupService;
	private FaixaMarkupNomeService faixaMarkupNomeService;
	private MovpatrimonioService movpatrimonioService;
	private MaterialEcomerceExcluidoService materialEcomerceExcluidoService;

	public void setPedidoVendaService(PedidovendaService pedidoVendaService) {this.pedidoVendaService = pedidoVendaService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setReservaService(ReservaService reservaService) {this.reservaService = reservaService;}
	public void setPneumodeloService(PneumodeloService pneumodeloService) {this.pneumodeloService = pneumodeloService;}
	public void setMaterialpneumodeloService(MaterialpneumodeloService materialpneumodeloService) {this.materialpneumodeloService = materialpneumodeloService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setMaterialimpostoService(MaterialimpostoService materialimpostoService) {this.materialimpostoService = materialimpostoService;}
	public void setTabelaimpostoService(TabelaimpostoService tabelaimpostoService) {this.tabelaimpostoService = tabelaimpostoService;}
	public void setOrdemcompramaterialService(OrdemcompramaterialService ordemcompramaterialService) {this.ordemcompramaterialService = ordemcompramaterialService;}
	public void setMaterialformulapesoService(MaterialformulapesoService materialformulapesoService) {this.materialformulapesoService = materialformulapesoService;}
	public void setMaterialhistoricoService(MaterialhistoricoService materialhistoricoService) {this.materialhistoricoService = materialhistoricoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {this.materialtipoService = materialtipoService;}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {this.materialgrupoService = materialgrupoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setInspecaoitemService(InspecaoitemService inspecaoitemService) {this.inspecaoitemService = inspecaoitemService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}	
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {this.materialproducaoService = materialproducaoService;}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {this.materialunidademedidaService = materialunidademedidaService;}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {this.materialrelacionadoService = materialrelacionadoService;}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {this.materialformulavalorvendaService = materialformulavalorvendaService;}
	public void setProducaoetapaitemService(ProducaoetapaitemService producaoetapaitemService) {this.producaoetapaitemService = producaoetapaitemService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setFormulacustoService(FormulacustoService formulacustoService) {this.formulacustoService = formulacustoService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setLotematerialService(LotematerialService lotematerialService) {this.lotematerialService = lotematerialService;}
	public void setMaterialkitflexivelService(MaterialkitflexivelService materialkitflexivelService) {this.materialkitflexivelService = materialkitflexivelService;}
	public void setMaterialformulacustoService(MaterialformulacustoService materialformulacustoService) {this.materialformulacustoService = materialformulacustoService;}
	public void setMaterialformulamargemcontribuicaoService(MaterialformulamargemcontribuicaoService materialformulamargemcontribuicaoService) {this.materialformulamargemcontribuicaoService = materialformulamargemcontribuicaoService;}
	public void setEntregaService(EntregaService entregaService) {this.entregaService = entregaService;	}
	public void setMateriallocalarmazenagemService(MateriallocalarmazenagemService materiallocalarmazenagemService) {this.materiallocalarmazenagemService = materiallocalarmazenagemService;}
	public void setMaterialfornecedorService(MaterialfornecedorService materialfornecedorService) {this.materialfornecedorService = materialfornecedorService;}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService){this.ordemcompraService = ordemcompraService;}
	public void setMaterialformulavendaminimoService(MaterialformulavendaminimoService materialformulavendaminimoService) {this.materialformulavendaminimoService = materialformulavendaminimoService;}
	public void setMaterialformulavendamaximoService(MaterialformulavendamaximoService materialformulavendamaximoService) {this.materialformulavendamaximoService = materialformulavendamaximoService;}
	public void setMaterialCustoEmpresaService(MaterialCustoEmpresaService materialCustoEmpresaService) {this.materialCustoEmpresaService = materialCustoEmpresaService;}
	public void setMaterialFaixaMarkupService(MaterialFaixaMarkupService materialFaixaMarkupService) {this.materialFaixaMarkupService = materialFaixaMarkupService;}
	public void setFaixaMarkupNomeService(FaixaMarkupNomeService faixaMarkupNomeService) {this.faixaMarkupNomeService = faixaMarkupNomeService;}
	public void setMovpatrimonioService(MovpatrimonioService movpatrimonioService) {this.movpatrimonioService = movpatrimonioService;}
	public void setMaterialEcomerceExcluidoService(MaterialEcomerceExcluidoService materialEcomerceExcluidoService) {this.materialEcomerceExcluidoService = materialEcomerceExcluidoService;}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, MaterialFiltro filtro) throws CrudException {
		String cdcodigo = request.getParameter("cdcodigo");
		if("true".equals(request.getParameter("filtrarSomenteCodigobarra"))){
			String codigo = filtro.getCodigo();
			filtro = new MaterialFiltro();
			filtro.setTipo("C�digo de barras");
			filtro.setCodigo(codigo);
		}
		if(cdcodigo != null && !cdcodigo.equals("")){
			if(filtro.getTipo() == null || filtro.getTipo().equals(""))
				filtro.setTipo(MaterialFiltro.TODOS);
			filtro.setCodigo(cdcodigo);
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request, MaterialFiltro filtro) throws Exception {
		if("true".equals(request.getParameter("FROMGERENCIAMATERIAL"))){
			filtro.setBempatrimonio(false);
			filtro.setProduto(true);
			filtro.setServico(true);

		}
		
		try{
			String listarQantidade = NeoWeb.getRequestContext().getParameter("listarQantidade");
			if(listarQantidade !=null && listarQantidade.toUpperCase().equals("TRUE")){
				request.setAttribute("listarQantidade", true);
			}
		}catch (Exception e) {
		}
		
		List<String> listaCodigos = new ArrayList<String>();
		listaCodigos.add(MaterialFiltro.TODOS);
		listaCodigos.add(MaterialFiltro.CODIGO);
		listaCodigos.add(MaterialFiltro.CODIGO_ALTERNATIVO);
		listaCodigos.add(MaterialFiltro.CODIGO_BARRAS);
		listaCodigos.add(MaterialFiltro.CODIGO_FABRICANTE);
		listaCodigos.add(MaterialFiltro.IDENTIFICACAO);
		listaCodigos.add(MaterialFiltro.REFERENCIA);
		
		if(filtro.getTipo() == null || filtro.getTipo().equals(""))
			filtro.setTipo(MaterialFiltro.TODOS);
		
		request.setAttribute("listaCodigos", listaCodigos);
		request.setAttribute("listaTipo", materialtipoService.findAtivos());
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		request.setAttribute("itensInspecaoAtivos", inspecaoitemService.findAtivos());
		request.setAttribute("listaConta", contagerencialService.findAnaliticas(Tipooperacao.TIPO_DEBITO));
		request.setAttribute("integracaoEmporium", IntegracaoEmporiumUtil.util.haveIntegracaoEcf());
		request.setAttribute("QTDE_DISPONIVEL_CONSIDERAR_RESERVADO", parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO));
		request.setAttribute("GERAR_VENDA_AJUSTE_FATURAMENTO", parametrogeralService.getBoolean(Parametrogeral.GERAR_VENDA_AJUSTE_FATURAMENTO));
	}
	
	@Override
	protected ListagemResult<Material> getLista(WebRequestContext request,	MaterialFiltro filtro) {
		ListagemResult<Material> listagemResult = super.getLista(request, filtro);
		List<Material> lista = listagemResult.list();
		
		
		
		Empresa empresaVenda = null;
		Localarmazenagem localVenda = null;
		Projeto projeto = null;
		try{
			String paramEmpresa = NeoWeb.getRequestContext().getParameter("cdempresaFiltroVenda");
			String paramLocal = NeoWeb.getRequestContext().getParameter("cdLocalFiltro");
			String projetoPedido = NeoWeb.getRequestContext().getParameter("cdProjeto");
			if(!StringUtils.isEmpty(paramEmpresa)){
				empresaVenda = new Empresa(Integer.parseInt(paramEmpresa));
			}
			if(!StringUtils.isEmpty(paramLocal)){
				localVenda = new Localarmazenagem(Integer.parseInt(paramLocal));
			}
			if(!StringUtils.isEmpty(projetoPedido)){
				projeto = new Projeto(Integer.parseInt(projetoPedido));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if(lista != null && !lista.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdmaterial", ",");
			lista.removeAll(lista);
			lista.addAll(materialService.findListagem(whereIn,filtro.getOrderBy(),filtro.isAsc(),null));
		}
		Double entrada = 0d;
		Double saida = 0d;
		Double qtddisponivel = 0d;

		
		for (Material material : lista) {
			if(material.getVgerenciarmaterial() == null || material.getVgerenciarmaterial().getQtdedisponivel() == null){
				if(material.getVgerenciarmaterial() == null){
					Vgerenciarmaterial v = new Vgerenciarmaterial();
					v.setQtdedisponivel(0.0);
					material.setVgerenciarmaterial(v);
				}else{
					material.getVgerenciarmaterial().setQtdedisponivel(0.0);
				}
			}
			
			entrada = pedidoVendaService.retornaEntradaMaterial(material, localVenda,  empresaVenda, projeto);
			saida = pedidoVendaService.retornaSaidaMaterial(material, localVenda,  empresaVenda, projeto);
			
			qtddisponivel = pedidoVendaService.retornarQtdDisponivel(null,entrada,saida);
			material.getVgerenciarmaterial().setQtdedisponivel(qtddisponivel);
			
			Double reserva = reservaService.getQtdeReservada(material, empresaVenda, localVenda);
			
			material.setQtdeemreserva(reserva);
		}
		request.setAttribute("visualizarValor",SinedUtil.isUserHasAction(Acao.VISUALIZAR_CUSTO_MATERIAL));
		return listagemResult;
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Material form)	throws CrudException {
		String cdmaterial = request.getParameter("cdmaterial");
		String stCopiar = request.getParameter("copiar");
		request.setAttribute("visualizarValor",SinedUtil.isUserHasAction(Acao.VISUALIZAR_CUSTO_MATERIAL));	
		request.setAttribute("isMestreGrade", StringUtils.isNotEmpty(cdmaterial) ? materialService.isMestreGrade(form) : Boolean.FALSE);
		boolean isEcompleto = "ECOMPLETO".equals(parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE));
		boolean isTrayCorp = "TRAYCORP".equals(parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE));
		request.setAttribute("isEcompleto", isEcompleto);
		request.setAttribute("isTrayCorp", isTrayCorp);		
		
		List<StatusUsoEnum> listStatusUso = new ArrayList<StatusUsoEnum>();
		listStatusUso.add(StatusUsoEnum.NOVO);					
		listStatusUso.add(StatusUsoEnum.USADO);
		listStatusUso.add(StatusUsoEnum.RECONDICIONADO);
		listStatusUso.add(StatusUsoEnum.REMANUFATURADO);
		if (isTrayCorp) listStatusUso.add(StatusUsoEnum.DANIFICADO);
		if(isEcompleto) listStatusUso.add(StatusUsoEnum.MOSTRUARIO);
		request.setAttribute("statusUsoList", listStatusUso);
		
		if (StringUtils.isNotEmpty(cdmaterial) && StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
			form.setCdmaterial(Integer.parseInt(cdmaterial));
			form = materialService.loadForEntrada(form);
			form.setListaMaterialformulapeso(materialformulapesoService.findByMaterial(form));
			form.setListaMaterialformulavalorvenda(materialformulavalorvendaService.findByMaterial(form));
			form.setListaMaterialformulamargemcontribuicao(materialformulamargemcontribuicaoService.findByMaterial(form));
			form.setCdmaterialtrans(form.getCdmaterial());
			form.setCdmaterial(null);
            form.setIdEcommerce(null);
			materialService.limparReferenciasForCopiar(form);
		}		
		
		return super.doEntrada(request, form);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Material bean) {
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}else if (Boolean.TRUE.equals(bean.getFromVeiculo())){
			return new ModelAndView("redirect:/suprimento/crud/Patrimonioitem?ACAO=criar&fromVeiculo=true&clearBase=true&cdmaterialFromVeiculo="+bean.getCdmaterial());
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void entrada(WebRequestContext request, Material form) throws Exception {
		String stCopiar = request.getParameter("copiar");
		
		//Novo Material
		if (form.getCdmaterial() == null) {
			form.setValorcustoinicial(0.0);
			form.setDtcustoinicial(SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), -1));
		}
		
		request.setAttribute("haveIntegracaoEcf", IntegracaoEmporiumUtil.util.haveIntegracaoEcf());
		
		if(form.getIdDocumentoRegistrarEntrada() != null && !"".equals(form.getIdDocumentoRegistrarEntrada())){
			request.setAttribute("fromRegistrarentradafiscal", true);
		}
		
		form.setFromVeiculo("true".equals(request.getParameter("fromVeiculo")));
		
		List<Inspecaoitem> listaInspecaoitemAtivo = null;
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		
		if (form.getCdmaterial() != null || (StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true"))) {
			if(StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
				form.setCdmaterial(form.getCdmaterialtrans());
			} 
			
			if (!request.getBindException().hasErrors()) {			
				materialService.setaProduto(form);
				materialService.setaPatrimonio(form);
				materialService.setaEpi(form);
				materialService.setaServico(form);
				
				if(form.getListaProducao() != null && !form.getListaProducao().isEmpty()){
					for(Materialproducao mp : form.getListaProducao()){
						if(mp.getMaterial() != null && mp.getMaterial().getCdmaterial() != null)
							materialService.setaProduto(mp.getMaterial());
					}
				}
				
				carregarImagem(request, form.getArquivo(), "exibefoto");
				
			}
			
			if(StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
				form.setCdmaterial(null);
				form.setValorcusto(null);
				form.setQtdecustoinicial(null);
				form.setIdEcommerce(null);
				form.setIdPaiExterno(null);
				form.setIdentificacao(null);
			}
		} else {
			if(request.getParameter("novoMaterial") != null){
				String unidadeMedida = request.getParameter("unidadeMedida");
				form.setNome(request.getParameter("nome"));
				if(unidadeMedida != null && !unidadeMedida.equals("") && !unidadeMedida.equals("undefined")){
					form.setUnidademedida(unidademedidaService.findBySimbolo(unidadeMedida));
				}
			}
			
			form.setProduto_fatorconversao(1000.0);
			form.setProduto_fatorconversaoproducao(1d);
		}
		
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos(form.getMaterialgrupo()));
		request.setAttribute("listaTipo", materialtipoService.findAtivos(form.getMaterialtipo()));
		request.setAttribute("listaUnidade", unidademedidaService.findAtivos(form.getUnidademedida()));
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(form.getCentrocustovenda()));
		
		List<Empresa> listaEmpresaAtiva = null;
		if(form.getCdmaterial() != null){
			List<Empresa> listaEmpresaSelecionada = (List<Empresa>) CollectionsUtil.getListProperty(form.getListaMaterialempresa(), "empresa");
			listaEmpresaAtiva = empresaService.findAtivos(listaEmpresaSelecionada);
		} else {
			listaEmpresaAtiva = empresaService.findAtivos();
		}
		request.setAttribute("listaEmpresa", listaEmpresaAtiva);
		
		if(form.getCdmaterial() != null){
			List<Materialclasse> listaClasses = materialService.findClassesWithoutLoad(form);
			request.getSession().setAttribute(MaterialService.NOME_PARAM_MATERIALCLASSE_SIMILAR, listaClasses);
		}
		
		if(form != null)
			if(form.getListaMaterialinspecaoitem() != null && form.getListaMaterialinspecaoitem().size() > 0){
				listaInspecaoitemAtivo = (List<Inspecaoitem>) CollectionsUtil.getListProperty(form.getListaMaterialinspecaoitem(), "inspecaoitem");
				request.setAttribute("itensInspecaoAtivos", inspecaoitemService.findAtivos(listaInspecaoitemAtivo));
			}
		
		request.setAttribute("whereInNaturezaContagerencialMaterial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		
		if(form.getCdmaterial() != null){
			materialService.setaAtributosMestreTrayCorp(form);
			form.setListaMaterialhistorico(materialhistoricoService.findByMaterial(form));
			
			form.setIdEcommerceTrans(form.getIdEcommerce());
			if(form.getIdEcommerce() != null){
				MaterialEcomerceExcluido materialEcomerceExcluido = materialEcomerceExcluidoService.loadByIdEcommerce(form.getIdEcommerce());
				if(materialEcomerceExcluido != null){
					form.setIdEcommerceTrans(null);
				}
			}
			
			if(!request.getBindException().hasErrors()){
				form.setListaMaterialformulapeso(materialformulapesoService.findByMaterial(form));
				form.setListaMaterialformulavalorvenda(materialformulavalorvendaService.findByMaterial(form));
				form.setListaMaterialformulacusto(materialformulacustoService.findByMaterial(form));
				form.setListaMaterialformulavendaminimo(materialformulavendaminimoService.findByMaterial(form));
				form.setListaMaterialformulavendamaximo(materialformulavendamaximoService.findByMaterial(form));
				form.setListaMaterialformulamargemcontribuicao(materialformulamargemcontribuicaoService.findByMaterial(form));
				form.setListaMateriallocalarmazenagem(materiallocalarmazenagemService.findByMaterial(form));
				form.setListaMaterialpneumodelo(materialpneumodeloService.findByMaterial(form));
				form.setListaFornecedor(SinedUtil.listToSet(materialfornecedorService.findByMaterial(form), Materialfornecedor.class));
				form.setListaMaterialFaixaMarkup(materialFaixaMarkupService.findByMaterial(form));
			}
		}
		
		form.setListaProducao(materialproducaoService.ordernarListaProducao(form.getListaProducao(), false));
		request.setAttribute("integracaoEmporium", IntegracaoEmporiumUtil.util.haveIntegracaoEcf());
		boolean existeMovimentacaoEstoqueNaoCancelada = false;
		if(EDITAR.equals(request.getParameter(MultiActionController.ACTION_PARAMETER)) && form.getProduto() != null &&
				form.getProduto()){
			existeMovimentacaoEstoqueNaoCancelada = movimentacaoestoqueService.existeMovimentacaoEstoqueNaoCancelada(form);
		}
		request.setAttribute("existeMovimentacaoEstoqueNaoCancelada", existeMovimentacaoEstoqueNaoCancelada);
		request.setAttribute("listaUnidadeMedidaMaterial", unidademedidaService.findByConversaoUnidade(form, stCopiar));
		
		//valida��o para o tipo de item sped
		List<String> listaTipoitemsped = new ArrayList<String>();
		listaTipoitemsped.add(Tipoitemsped.USO_CONSUMO.getDescricao());
		listaTipoitemsped.add(Tipoitemsped.SERVICOS.getDescricao());
		listaTipoitemsped.add(Tipoitemsped.OUTROS_INSUMOS.getDescricao());
		listaTipoitemsped.add(Tipoitemsped.OUTRAS.getDescricao());

		request.setAttribute("listaTipoitemsped", listaTipoitemsped);
		
		request.setAttribute("ENTREGA_NAOATUALIZACUSTO", parametrogeralService.getValorPorNome(Parametrogeral.ENTREGA_NAOATUALIZACUSTO));
		request.setAttribute("EXIST_FORMULACUSTO", formulacustoService.existsFormulaAtiva());
		request.setAttribute("CALCULO_MARGEMCONTRIB_OPORTUNIDADE", parametrogeralService.getBoolean(Parametrogeral.CALCULO_MARGEMCONTRIB_OPORTUNIDADE));
		request.setAttribute("CONSIDERAR_CUSTO_POR_EMPRESA", parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_CUSTO_POR_EMPRESA));
		request.setAttribute("haveTabelaimposto", tabelaimpostoService.haveTabelaimposto());
		request.setAttribute("EXIBIR_IPI_NA_VENDA", parametrogeralService.getBoolean(Parametrogeral.EXIBIR_IPI_NA_VENDA));
        request.setAttribute("GARANTIA_MATERIAL_MESES", parametrogeralService.getBoolean(Parametrogeral.GARANTIA_MATERIAL_MESES));
        request.setAttribute("INTEGRACAO_ECOMMERCE", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE));
		materialService.setAtributeDimensaomaterial(request);
		
		if(Hibernate.isInitialized(form.getListaMaterialempresa()) && SinedUtil.isListNotEmpty(form.getListaMaterialempresa())){
			Collections.sort((List<Materialempresa>) form.getListaMaterialempresa(), new Comparator<Materialempresa>(){
				public int compare(Materialempresa a1, Materialempresa a2){
					return new SinedUtil().getUsuarioPermissaoEmpresa(a1.getEmpresa()) ? 1 : 0; 
				}
			});
		}
		if(Hibernate.isInitialized(form.getListaFornecedor()) && SinedUtil.isListNotEmpty(form.getListaFornecedor())){
			Collections.sort((List<Materialfornecedor>) form.getListaFornecedor(), new Comparator<Materialfornecedor>(){
				public int compare(Materialfornecedor a1, Materialfornecedor a2){
					return new SinedUtil().getUsuarioPermissaoFornecedor(a1.getFornecedor()) ? 1 : 0; 
				}
			});
		}
		materialService.addInformacoesAtributoRequisicao(request, form);
		
		boolean possuiPermissaoVeterinaria = SinedUtil.getPermissaoModulo("/veterinaria/");
		
		if (possuiPermissaoVeterinaria){
			if (form.getFaturamentoVeterinaria()==null){
				form.setFaturamentoVeterinaria(FaturamentoVeterinaria.UNIDADE_INTEGRAL);
			}
			if (form.getQtdeunidadeveterinaria()==null){
				form.setQtdeunidadeveterinaria(1D);
			}
		}
		request.setAttribute("PLANOCONTASCONTABILPOREMPRESA", parametrogeralService.getBoolean("PLANO_CONTAS_CONTABIL_POR_EMPRESA"));
		request.setAttribute("RATEIO_MOVIMENTACAO_ESTOQUE", parametrogeralService.getBoolean(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE));
		request.setAttribute("possuiPermissaoVeterinaria", possuiPermissaoVeterinaria);
		request.setAttribute("MARCAMODELO_OPCIONAL_MODELOSVINCULADOS", parametrogeralService.getBoolean("MARCAMODELO_OPCIONAL_MODELOSVINCULADOS"));
		request.setAttribute("TIPO_ATUALIZACAO_PRECO_VENDA", parametrogeralService.getValorPorNome(Parametrogeral.TIPO_ATUALIZACAO_PRECO_VENDA));
		request.setAttribute("HABILITAR_EDICAO_FAIXA_MARKUP", parametrogeralService.getBoolean("HABILITAR_EDICAO_FAIXA_MARKUP"));
		request.setAttribute("GERAR_ESTOQUE_COMPOSICAO_KIT", parametrogeralService.getBoolean("GERAR_ESTOQUE_COMPOSICAO_KIT"));
		request.setAttribute("GERAR_VENDA_AJUSTE_FATURAMENTO", parametrogeralService.getBoolean(Parametrogeral.GERAR_VENDA_AJUSTE_FATURAMENTO));
		request.setAttribute("GARANTIA_MATERIAL_MESES", parametrogeralService.getBoolean(Parametrogeral.GARANTIA_MATERIAL_MESES));
		request.setAttribute("INTEGRACAO_ECOMMERCE", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE));
		request.setAttribute("SEPARACAO_EXPEDICAO", parametrogeralService.getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO));
		boolean eCommerceEnabled = parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE) != null; 
		request.setAttribute("ECOMMERCE_ENABLED", eCommerceEnabled);
		request.setAttribute("utilizaTicketMedio", pedidovendatipoService.existsPedidovendaWithTicketMedio());
		request.setAttribute("RESTRICAO_FORNECEDOR_PRODUTO", usuario != null && Boolean.TRUE.equals(usuario.getRestricaofornecedorproduto()));
	}
	
	/**
	 * M�todo respons�vel por carregar as imagens que s�o mostradas na tela
	 * @param request
	 * @param arquivo
	 * @param atributo
	 * @author Thiago Gon�alves
	 */
	private void carregarImagem(WebRequestContext request, Arquivo arquivo, String atributo){
		try {
			arquivoService.loadAsImage(arquivo);
			if(arquivo.getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());
				request.setAttribute(atributo, true);
			}
		} catch (Exception e) {
			request.setAttribute(atributo, false);
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Material bean)throws Exception {
		if(bean.getListaFornecedor() != null && bean.getListaFornecedor().size() > 0){
			for (Iterator<Materialfornecedor> iterator = bean.getListaFornecedor().iterator(); iterator.hasNext();) {
				Materialfornecedor mf = (Materialfornecedor) iterator.next();
				if(mf.getCdmaterialfornecedor() == null && mf.getFornecedor() == null){
					iterator.remove();
				}
			}
		}
		
		if(!Boolean.TRUE.equals(bean.getCadastrarFaixaMarkup()) && SinedUtil.isListNotEmpty(bean.getListaMaterialFaixaMarkup())){
			bean.setListaMaterialFaixaMarkup(null);
		}
		
		if(bean.getCdmaterial() != null){
//			List<Materialnumeroserie> listaMaterialnumeroserie = materialnumeroserieService.findListaMaterialnumeroserieByMaterial(bean);
//			if(listaMaterialnumeroserie != null && !listaMaterialnumeroserie.isEmpty())
//				bean.setListaMaterialnumeroserie(SinedUtil.listToSet(listaMaterialnumeroserie, Materialnumeroserie.class));
		}
		
		boolean isCriar = bean.getCdmaterial() == null;
		try {
			Set<Materialimposto> listaMaterialimposto = bean.getListaMaterialimposto();
			
			bean.setNome(bean.getNome().replaceAll("\"", "''"));
			
			Material antigo = null;
			String historicoAlteracaoFaixaMarkup = "";
			if(!isCriar){
				antigo = materialService.loadForHistoricoAlteracao(bean);
				if(Boolean.TRUE.equals(bean.getCadastrarFaixaMarkup())){
					List<MaterialFaixaMarkup> listaFaixaAntiga = materialFaixaMarkupService.findByMaterial(bean);
					if(SinedUtil.isListNotEmpty(listaFaixaAntiga)){
						historicoAlteracaoFaixaMarkup = criaHistoricoAlteracaoFaixasMarkup(bean.getListaMaterialFaixaMarkup(), listaFaixaAntiga);
					}
				}
			}
			
			if(Boolean.TRUE.equals(bean.getEcommerce())){
				if(bean.getDescontoAVista() == null){
					bean.setDescontoAVista(0);				
				}
				if(bean.getEcommerce_altura() == null){
					bean.setEcommerce_altura(0.0d);
				}
				if(bean.getEcommerce_comprimento() == null){
					bean.setEcommerce_comprimento(0.0d);
				}
				if(bean.getEcommerce_largura() == null){
					bean.setEcommerce_largura(0.0d);
				}
			}
			if(SinedUtil.isIntegracaoTrayCorp() && Boolean.TRUE.equals(bean.getEcommerce()) && bean.getIdEcommerce() == null){
				bean.setIdVinculoExterno(materialService.getNextIdVinculoExterno());
			}
			
			super.salvar(request, bean);
			
			//Se usar o e-commerce TrayCorp, o material for mestre de grade e tiver atualizando o mesmo, tem que for�ar a atualiza��o dos itens filhos
			if("TRAYCORP".equals(parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE)) && Util.objects.isNotPersistent(bean.getMaterialmestregrade())
				&& Util.objects.isPersistent(bean.getMaterialgrupo())){
				if(materialgrupoService.isGrupoGrade(bean.getMaterialgrupo())){
					materialService.updateToForceTriggers(bean);
				}
			}
			if("TRAYCORP".equals(parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE))){
				if(Util.objects.isPersistent(bean.getMaterialmestregrade()) && !isCriar){
					materialService.updateIdPaiExternoItensDeGrade(bean.getMaterialmestregrade());
				}
			}
			
			Materialhistorico materialhistorico = new Materialhistorico();
			materialhistorico.setMaterial(bean);
			if(isCriar){
				materialhistorico.setAcao(Materialacao.CRIADO);
			} else {
				materialhistorico.setAcao(Materialacao.ALTERADO);
				if(antigo != null){
					materialhistoricoService.setValoresAntigos(materialhistorico, antigo);
				}
			}
			materialhistoricoService.saveOrUpdate(materialhistorico);
			
			if(StringUtils.isNotBlank(historicoAlteracaoFaixaMarkup)){
				materialhistorico.setObservacaoFaixaMarkup(historicoAlteracaoFaixaMarkup);
				materialhistoricoService.saveOrUpdate(materialhistorico);
			}
			
			if(bean.getIdentificacao() == null || bean.getIdentificacao().equals("")){
				materialService.updateIdentificacao(bean, bean.getCdmaterial().toString());
			}
			
			if(IntegracaoEmporiumUtil.util.haveIntegracaoEcf()){
				if(tabelaimpostoService.haveTabelaimposto()){
					materialimpostoService.calculaByMaterialInTabelaimposto(bean);
				} else {
					if(listaMaterialimposto != null && listaMaterialimposto.size() > 0){
						List<Integer> listaIdsMaterialimposto = new ArrayList<Integer>();
						for (Materialimposto materialimposto : listaMaterialimposto) {
							if(materialimposto.getCdmaterialimposto() != null){
								listaIdsMaterialimposto.add(materialimposto.getCdmaterialimposto());
							}
						}
						
						if(listaIdsMaterialimposto.size() > 0){
							String whereIn = CollectionsUtil.concatenate(listaIdsMaterialimposto, ",");
							materialimpostoService.deleteByMaterialWhereNotIn(bean, whereIn);
						}
						
						for (Materialimposto materialimposto : listaMaterialimposto) {
							materialimposto.setMaterial(bean);
							materialimpostoService.saveOrUpdate(materialimposto);
						}
					} else {
						materialimpostoService.deleteByMaterial(bean);
					}
				}
			}
			
			materialService.atualizarAbaMaterialCustoEmpresa(bean);			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_MATERIAL_NOME")) {
				throw new SinedException("Material j� cadastrado no sistema.");
			} else if (DatabaseError.isKeyPresent(e, "material_identificacao_key") || DatabaseError.isKeyPresent(e, "material_idx")) {
				throw new SinedException("Identificador j� cadastrado no sistema.");
			} else if (DatabaseError.isKeyPresent(e, "IDX_UNIQUE_CTE")) {
				throw new SinedException("J� existe material para importa��o de cte cadastrado no sistema.");				
			} else if (DatabaseError.isKeyPresent(e, "IDX_SERVICO_NOMEREDUZIDO")) {
				throw new SinedException("Servi�o j� cadastrado no sistema.");				
			} else if(DatabaseError.isKeyPresent(e, "fk_patrimonioitem_4")) {
				throw new SinedException("Existem n�meros de s�rie que possuem movimenta��o relacionada. N�o � poss�vel exclu�-las.");
			} else {
				throw new SinedException(e.getMessage());
			}
		}
		
		try {
			if(isCriar){
				Double valorvendaformula = materialService.getValorvendaByCadastroMaterial(bean, true);
				if(valorvendaformula != null && valorvendaformula > 0 && bean.getCdmaterial() != null){
					materialService.updateValorVenda(bean, valorvendaformula);
				}
			}
		} catch (Exception e) {}
		
		try {
			materialService.recalcularConsumoAposAtualizacaoCusto(bean.getCdmaterial().toString());
			materialService.recalcularCustoKitAposAtualizacaoCusto(bean.getCdmaterial().toString());
			materialService.recalcularMinioMaximo(bean.getCdmaterial().toString());
		} catch (Exception e) {
			request.addError("Erro ao atualizar consumo ap�s atualiza��o de custo: " +  e.getMessage());
			e.printStackTrace();
		}
	}	
	
	private String criaHistoricoAlteracaoFaixasMarkup(List<MaterialFaixaMarkup> listaNova, List<MaterialFaixaMarkup> listaAntiga){
		StringBuilder historico = new StringBuilder();
		for(MaterialFaixaMarkup beanNovo: listaNova){
			beanNovo.setFaixaMarkupNome(faixaMarkupNomeService.load(beanNovo.getFaixaMarkupNome(), "faixaMarkupNome.cdFaixaMarkupNome, faixaMarkupNome.nome"));
			if(Util.objects.isPersistent(beanNovo.getEmpresa())){
				beanNovo.setEmpresa(empresaService.load(beanNovo.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.nomefantasia"));
			}
			if(Util.objects.isPersistent(beanNovo.getUnidadeMedida())){
				beanNovo.setUnidadeMedida(unidademedidaService.load(beanNovo.getUnidadeMedida(), "unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo"));
			}
			if(beanNovo.getCdMaterialFaixaMarkup() == null){
				historico.append("Inclus�o de faixa: ").append("\n");
				historico.append("Nome da faixa: "+beanNovo.getFaixaMarkupNome().getNome()).append("\n");
				if(beanNovo.getEmpresa() != null){
					historico.append("Empresa: "+beanNovo.getEmpresa().getNome()).append("\n");
				}
				historico.append("% Markup: "+beanNovo.getMarkup()).append("\n");
				historico.append("Valor de venda: "+beanNovo.getValorVenda()).append("\n");
				historico.append("\n").append("-----------------------------------------------------------------------------------------").append("\n").append("\n");
			}else{
				MaterialFaixaMarkup beanAntigo = listaAntiga.get(listaAntiga.indexOf(beanNovo));
				StringBuilder alteracao = new StringBuilder();
				if(!beanNovo.getFaixaMarkupNome().getNome().equals(beanAntigo.getFaixaMarkupNome().getNome())){
					alteracao.append("De faixa ")
							.append(beanAntigo.getFaixaMarkupNome().getNome())
							.append(" para faixa ")
							.append(beanNovo.getFaixaMarkupNome().getNome()).append("\n");
				}
				
				if(beanNovo.getEmpresa() == null && beanAntigo.getEmpresa() != null){
					alteracao.append("Removida Empresa: ")
							.append(beanAntigo.getEmpresa().getNome()).append("\n");
				}
				if(beanNovo.getEmpresa() != null && beanAntigo.getEmpresa() == null){
					alteracao.append("Preenchimento do campo Empresa: ")
							.append(beanNovo.getEmpresa().getNome()).append("\n");
				}
				if(beanNovo.getEmpresa() != null && beanAntigo.getEmpresa() != null
					&& !beanNovo.getEmpresa().getNome().equals(beanAntigo.getEmpresa().getNome())){
					alteracao.append("De empresa ")
							.append(beanAntigo.getEmpresa().getNome())
							.append(" para empresa ")
							.append(beanNovo.getEmpresa().getNome()).append("\n");
				}
				
				if(beanNovo.getUnidadeMedida() != null && beanAntigo.getUnidadeMedida() != null
					&& !beanNovo.getUnidadeMedida().equals(beanAntigo.getUnidadeMedida())){
					alteracao.append("De unidade de medida ")
							.append(beanAntigo.getUnidadeMedida().getNome())
							.append(" para unidade de medida ")
							.append(beanNovo.getUnidadeMedida().getNome()).append("\n");
				}
				System.out.println(SinedUtil.round(beanNovo.getMarkup(), 10));
				System.out.println(SinedUtil.round(beanAntigo.getMarkup(), 10));
				if(beanNovo.getMarkup() != null && beanAntigo.getMarkup() != null && SinedUtil.round(beanNovo.getMarkup(), 10).compareTo(SinedUtil.round(beanAntigo.getMarkup(), 10)) != 0){
					alteracao.append("De % Markup ")
							//.append(new DecimalFormat("#,##0.00").format(beanAntigo.getMarkup()))
							.append(beanAntigo.getMarkup())
							.append(" para % Markup ")
							.append(beanNovo.getMarkup()).append("\n");
							//.append(new DecimalFormat("#,##0.00").format(beanNovo.getMarkup())).append("\n");
				}
				if(Boolean.TRUE.equals(beanNovo.getIsValorIdeal()) != Boolean.TRUE.equals(beanAntigo.getIsValorIdeal())){
					alteracao.append("De Valor Ideal ")
							.append(SinedUtil.booleanToSimNao(beanAntigo.getIsValorIdeal()))
							.append(" para empresa ")
							.append(SinedUtil.booleanToSimNao(beanNovo.getIsValorIdeal())).append("\n");
				}
				if(Boolean.TRUE.equals(beanNovo.getIsValorMinimo()) != Boolean.TRUE.equals(beanAntigo.getIsValorMinimo())){
					alteracao.append("De Valor M�nimo ")
							.append(SinedUtil.booleanToSimNao(beanAntigo.getIsValorMinimo()))
							.append(" para Valor M�nimo ")
							.append(SinedUtil.booleanToSimNao(beanNovo.getIsValorMinimo())).append("\n");
				}
				if(alteracao.length() > 0){
					historico.append("Altera��o da faixa ").append(beanAntigo.getFaixaMarkupNome().getNome()).append("\n");
					historico.append(alteracao.toString());
					historico.append("\n").append("-----------------------------------------------------------------------------------------").append("\n").append("\n");
				}
			}
		}
		
		for(MaterialFaixaMarkup beanAntigo: listaAntiga){
			if(!listaNova.contains(beanAntigo)){
				historico.append("Exclus�o da faixa: "+beanAntigo.getFaixaMarkupNome().getNome());
				if(beanAntigo.getEmpresa() != null){
					historico.append(" - Empresa: "+beanAntigo.getEmpresa().getNome());
				}
				historico.append("\n");
				historico.append("% Markup: "+beanAntigo.getMarkup()).append("\n");;
				historico.append("Valor de venda: "+beanAntigo.getValorVenda()).append("\n");;
				historico.append("\n").append("-----------------------------------------------------------------------------------------").append("\n").append("\n");
			}
		}
		return historico.toString();
	}
	
	@Override
	protected void validateBean(Material bean, BindException errors) {

		if(SinedUtil.isIntegracaoTrayCorp() && Boolean.TRUE.equals(bean.getEcommerce())
				&& (Util.objects.isPersistent(bean.getMaterialmestregrade()) || !materialgrupoService.isGrupoGrade(bean.getMaterialgrupo()))){
			if(StringUtils.isBlank(bean.getIdentificacao())){
				errors.reject("001","O campo Identifica��o deve ser informado quando se utiliza a integra��o com o Tray Corp. Essa informa��o vai para o SKU do produto do e-commerce.");
			}else{
				Material material = materialService.findByIdentificacao(bean.getIdentificacao(), true, bean.getCdmaterial());
				if(material != null){
					errors.reject("001","J� existe um material com esse identificador. Quando se utiliza a integra��o com o Tray Corp n�o � permitido mais de um produto com mesma identifica��o, pois essa informa��o vai para o SKU do produto do e-commerce.");
				}
			}
		}
		
		if (bean.getServico() && (bean.getProduto() || bean.getEpi() || bean.getPatrimonio())) {
			errors.reject("001","Quando o material � servi�o n�o pode ser produto, patrim�nio ou epi.");
		}
		if(bean.getListaMaterialinspecaoitem() != null && bean.getListaMaterialinspecaoitem().size() > 0)
			materialService.verificaItemInspecaoIgual(bean.getListaMaterialinspecaoitem(), errors);
		
		if (!validaImagem(bean.getArquivo())) 
			errors.reject("001", "A imagem selecionada n�o � v�lida.");
		
		if(bean.getListaMaterialcolaborador() != null && bean.getListaMaterialcolaborador().size() > 1){
			if(duplicidadeColaboradorDocumantotipo(bean.getListaMaterialcolaborador()))
				errors.reject("001", "Existe duplicidade no cadastro de Colaboradores/Comiss�o (Colaborador e Forma de pagamento iguais).");
		}
		if(bean.getReferencia() != null && !"".equals(bean.getReferencia())){
			if(materialService.isReferenciaDiferenteNomenf(bean)){
				errors.reject("001", "A refer�ncia informada j� existe no cadastro de outro material/servi�o. O nome para NF deve ser igual para todos os materiais/servi�os que possuem a mesma refer�ncia.");
			}
		}
		boolean mudouFlagObrigaLote = Boolean.TRUE.equals(bean.getObrigarlote()) != materialService.obrigarLote(bean);
		if(mudouFlagObrigaLote){
			if(movimentacaoestoqueService.existeMovimentacaoEstoqueNaoCancelada(bean)){
				String auxTexto = Boolean.TRUE.equals(bean.getObrigarlote())? "marcar": "desmarcar";
				bean.setObrigarlote(!Boolean.TRUE.equals(bean.getObrigarlote()));
				errors.reject("001", "N�o � permitido "+auxTexto+" a flag Obrigar Lote, porque o material j� possui movimenta��o de estoque.");
			}
		}
		
		if(bean.getListaMaterialunidademedida() != null && !bean.getListaMaterialunidademedida().isEmpty()){
			Boolean erroFracao = Boolean.FALSE;
			Boolean erroQtderef = Boolean.FALSE;
			for(Materialunidademedida mud : bean.getListaMaterialunidademedida()){
				if(mud.getFracao() != null && !(mud.getFracao() > 0d) && !erroFracao){
					errors.reject("001", "A convers�o de unidades n�o pode conter fra��es com valor 0.");
					erroFracao = Boolean.TRUE;
				}
				if(mud.getQtdereferencia() != null && !(mud.getQtdereferencia() > 0d) && !erroQtderef){
					errors.reject("001", "A convers�o de unidades n�o pode conter qtdes. de refer�ncia com valor 0.");
					erroQtderef = Boolean.TRUE;
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaMaterialunidademedida())){
			List<Unidademedida> listaUnidade = new ArrayList<Unidademedida>();
			if(bean.getUnidademedida() != null){
				listaUnidade.add(bean.getUnidademedida());
			}
			
			for(Materialunidademedida materialunidademedida : bean.getListaMaterialunidademedida()){
				if(listaUnidade.contains(materialunidademedida.getUnidademedida())){
					errors.reject("001", "N�o � permitido cadastrar a mesma unidade de medida na convers�o de unidade");
				}
				listaUnidade.add(materialunidademedida.getUnidademedida());
			}
		}
		
		if(bean.getCdmaterial() != null && SinedUtil.isListNotEmpty(bean.getListaProducao())){
			for(Materialproducao mp : bean.getListaProducao()){
				if(mp.getMaterial() != null && mp.getMaterial().equals(bean)){
					errors.reject("001", "N�o � permitido vincular o mesmo material de produ��o como mat�ria-prima.");
					break;
				}
			}
		}
		
		if (bean.getListaProducao()!=null){
			int cont = 0;
			for (Materialproducao materialproducao: bean.getListaProducao()){
				if (Boolean.TRUE.equals(materialproducao.getAcompanhabanda())){
					cont++;
				}
			}
			
			if (cont>1){
				errors.reject("001", "N�o � permitido que um Material/Servi�o de Mat�rias-primas e Equipamentos possuir mais de uma marca��o de Acompanhar banda.");
			}
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaMaterialsimilar())){
			String whereIn = CollectionsUtil.listAndConcatenate(bean.getListaMaterialsimilar(), "materialsimilaritem.cdmaterial", ",");
			if(StringUtils.isNotBlank(whereIn)){
				Boolean servico = Boolean.TRUE.equals(bean.getServico());
				Boolean produto = Boolean.TRUE.equals(bean.getProduto());
				Boolean epi = Boolean.TRUE.equals(bean.getEpi());
				Boolean patrimonio = Boolean.TRUE.equals(bean.getPatrimonio());
				
				List<Material> listaMaterial = materialService.findWithClasse(whereIn);
				if(SinedUtil.isListNotEmpty(listaMaterial)){
					for(Material material : listaMaterial){
						if(!((produto && produto.equals(material.getProduto()))
							|| (servico && servico.equals(material.getServico()))
							|| (patrimonio && patrimonio.equals(material.getPatrimonio()))
							|| (epi && epi.equals(material.getEpi())))){
							errors.reject("001", "N�o � permitido vincular um material similar de classe diferente. (Material: " + material.getNome() + ")");
						}
					}
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaMaterialCustoEmpresa())) {
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			
			for(MaterialCustoEmpresa m : bean.getListaMaterialCustoEmpresa()) {
				if(!listaEmpresa.contains(m.getEmpresa())) {
					listaEmpresa.add(m.getEmpresa());
				} else {
					errors.reject("001", "N�o � permitido cadastrar empresas repetidas na aba custo por empresa.");
					break;
				}
			}
		}
		
		if(!errors.hasErrors()){
			materialunidademedidaService.validaConversoesUnidade(bean, errors);
		}
		
		if(Boolean.TRUE.equals(bean.getCadastrarFaixaMarkup())){
			if(Boolean.TRUE.equals(bean.getVendapromocional())){
				errors.reject("001", "Material do tipo Kit n�o pode ter o campo Faixa Markup marcado.");
			}
			if(Boolean.TRUE.equals(bean.getKitflexivel())){
				errors.reject("001", "Material do tipo Kit Flex�vel n�o pode ter o campo Faixa Markup marcado.");			
			}
			if(Boolean.TRUE.equals(bean.getProducao())){
				errors.reject("001", "Material de produ��o n�o pode ter o campo Faixa Markup marcado.");
			}
			if(bean.getMaterialmestregrade() != null && bean.getMaterialmestregrade().getCdmaterial() != null){
				errors.reject("001", "Material Item de Grade n�o pode ter o campo Faixa Markup marcado.");
			}
		}
		
		if(parametrogeralService.getBoolean(Parametrogeral.GERAR_ESTOQUE_COMPOSICAO_KIT)){
			Double porcentagem = 0.0d;
			boolean desmontarKit = false;
			boolean custoGeralNulo = false;
			if(SinedUtil.isListNotEmpty(bean.getListaMaterialrelacionado())){
				for (Materialrelacionado materialrelacionado : bean.getListaMaterialrelacionado()) {
					if(Boolean.TRUE.equals(materialrelacionado.getDesmontarKitRecebimento())){
						desmontarKit = true;
					}
					if(materialrelacionado.getPorcentagemCustoGeral() != null){
						porcentagem = porcentagem + materialrelacionado.getPorcentagemCustoGeral();					
					}
					
					if(Boolean.TRUE.equals(materialrelacionado.getDesmontarKitRecebimento()) && materialrelacionado.getPorcentagemCustoGeral() == null){
						custoGeralNulo = true;
					}
				}
				
				if(porcentagem != 100.0d && desmontarKit){
					errors.reject("001", "A soma do campo % custo geral dos itens que comp�em o kit deve ser 100%.");
				}
				if(custoGeralNulo){
					errors.reject("001", "O valor do campo % Custo Geral n�o pode ser nulo, caso a flag Desmontar o kit no recebimento esteja marcada, nos materiais da Composi��o do Kit");
				}
			}
		}

		if(Boolean.TRUE.equals(bean.getItemGradePadraoMaterialMestre() && bean.getMaterialmestregrade() != null && materialService.existMaterialPadraoParaMestreGrade(bean.getMaterialmestregrade(), bean))){
			errors.reject("001", "J� existe Material de Grade padr�o para o Material Mestre selecionado.");
		}
		
		//valida��o e-commerce
		if(SinedUtil.isListNotEmpty(bean.getListaFornecedor()) && Boolean.TRUE.equals(bean.getEcommerce())){
			
			boolean hasPrincipal = false;
			boolean hasFabricante = false;
			
			for (Materialfornecedor materialfornecedor : bean.getListaFornecedor()) {
				if(Boolean.TRUE.equals(materialfornecedor.getPrincipal())){
					
					if(hasPrincipal){
						errors.reject("001", "Deve haver somente um fabricante marcado como principal.");
					}else{
						hasPrincipal = true;						
					}
				}
				if(Boolean.TRUE.equals(materialfornecedor.getFabricante())){
					hasFabricante = true;
				}
			}
			
			if(!hasPrincipal || !hasFabricante){
				errors.reject("001", "Deve haver ao menos um fabricante marcado como principal.");
			}
			
		}else if(SinedUtil.isListEmpty(bean.getListaFornecedor()) && Boolean.TRUE.equals(bean.getEcommerce())){
			errors.reject("001", "Deve haver ao menos um fabricante marcado como principal.");
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaMaterialAtributosMaterial())){
			boolean erro = false;
			List<MaterialAtributosMaterial> aux2 = bean.getListaMaterialAtributosMaterial();

			for(MaterialAtributosMaterial materialAtributoMaterial : bean.getListaMaterialAtributosMaterial()){
				if(materialAtributoMaterial.getCdmaterialatributosmaterial()!=null && bean.getCdmaterial()!=null){
					if(materialAtributoMaterial.getAtributosMaterial()!= null){
						Material material = materialService.buscaAtributosMaterial(bean);
						if(material != null && SinedUtil.isListNotEmpty(material.getListaMaterialAtributosMaterial())){
							for(MaterialAtributosMaterial mam : material.getListaMaterialAtributosMaterial()){
								if(mam.getAtributosMaterial() != null && mam.getAtributosMaterial().getCdatributosmaterial().equals(materialAtributoMaterial.getAtributosMaterial().getCdatributosmaterial()) && !mam.getCdmaterialatributosmaterial().equals(materialAtributoMaterial.getCdmaterialatributosmaterial())){
									erro = true;
								}
							}	
						}
					}		
				}
				int count = 0;
				for(MaterialAtributosMaterial mam2: aux2){
					if(materialAtributoMaterial.getAtributosMaterial().getCdatributosmaterial() == mam2.getAtributosMaterial().getCdatributosmaterial()){
						if(count != 0){
							erro=true;
							break;
						}
						count++;
					}
				}
			}
			if(erro){
				errors.reject("008", "Esse atributo j� existe para esse material.");
			}
		}
		if("TRAYCORP".equals(parametrogeralService.buscaValorPorNome(Parametrogeral.NOME_ECOMMERCE))){
			if(bean.getMaterialmestregrade() != null){
				Material materialMestre =  materialService.loadWithMatrialgrupo(bean.getMaterialmestregrade());
				if(materialMestre.getMaterialgrupo() != null && materialMestre.getMaterialgrupo().getGradeestoquetipo() == null){
					errors.reject("001", "Material mestre escolhido n�o possui configura��o de grade.");
				}
			}
		}
		materialService.existDuplicidadenumeroserie(bean, errors);
		
//		if(bean.getPesobruto() == null && !Boolean.TRUE.equals(bean.getServico()) && pedidovendatipoService.existsPedidovendaWithTicketMedio()){
//			errors.reject("001", "Quando se utiliza o c�lculo de ticket m�dio o campo Peso Bruto do material � obrigat�rio.");
//		}
	}
	
	/**
	 * M�todo para verificar se existe duplicidade em Materialcolaborador (Colaborador e Documentotipo iguais)
	 *
	 * @param listaMaterialcolaborador
	 * @return
	 * @author Luiz Fernando
	 */
	private Boolean duplicidadeColaboradorDocumantotipo(Set<Materialcolaborador> listaMaterialcolaborador){
		List<MultiKey> listaColaboradorDocumentotipo = new ArrayList<MultiKey>();
		for (Materialcolaborador materialcolaborador : listaMaterialcolaborador) {
			MultiKey multiKey = new MultiKey(materialcolaborador.getColaborador(), materialcolaborador.getDocumentotipo());
			if(listaColaboradorDocumentotipo.contains(multiKey)){
				return true;
			} else listaColaboradorDocumentotipo.add(multiKey);
		}
		
//		String c1 = null;
//		Documentotipo d1 = null;
//		String c2 = null;
//		Documentotipo d2 = null;
//		for(int i = 0; i < listaMaterialcolaborador.size(); i++){
//			c1 = listaMaterialcolaborador.get(i).getColaborador() != null && listaMaterialcolaborador.get(i).getColaborador().getCdpessoa() != null ? 
//					listaMaterialcolaborador.get(i).getColaborador().getCdpessoa().toString() : "";
//			d1 = listaMaterialcolaborador.get(i).getDocumentotipo();
//			if(d1 != null){
//				for(int j = i+1; j < listaMaterialcolaborador.size(); j++){
//					c2 = listaMaterialcolaborador.get(j).getColaborador() != null && listaMaterialcolaborador.get(j).getColaborador().getCdpessoa() != null ? 
//							listaMaterialcolaborador.get(j).getColaborador().getCdpessoa().toString() : "";
//					d2 = listaMaterialcolaborador.get(j).getDocumentotipo();
//					if(d2 != null && c1.equals(c2) && d1.equals(d2)){
//						return Boolean.TRUE;
//					}
//				}
//			}
//		}
		
		return false;
	}
	
	/**
	 * Verifica se o arquivo � uma imagem v�lida
	 * @param imagem
	 * @return
	 * @author Thiago Gon�alves
	 */
	private Boolean validaImagem(Arquivo imagem){
		if (imagem != null && imagem.getTamanho() != 0 && !SinedUtil.isImage(imagem))
			return false;
		else
			return true;
	}
	
	public void ajaxMaterialProducao(WebRequestContext request, Material material){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		try {
			material = materialService.loadForEntrada(material);
			materialService.setaProduto(material);
			int cont = 0;
			if(material.getProduto_materialcorte() != null && material.getProduto_materialcorte())
				view.println("var materialcorte = true;");
			else
				view.println("var materialcorte = false;");
			
			if (material.getProduto_largura() != null){
				view.println("var largura = '" + material.getProduto_largura() + "';");
				cont++;
			}else
				view.println("var largura = '';");

			if (material.getProduto_comprimento() != null){
				view.println("var comprimento = '" + material.getProduto_comprimento() + "';");
				cont++;
			}else
				view.println("var comprimento = '';");
			
			if (material.getProduto_altura() != null){
				view.println("var altura = '" + material.getProduto_altura() + "';");
				cont++;
			}else
				view.println("var altura = '';");
			
			if (material.getValorvenda() != null)
				view.println("var preco = '" + material.getValorcusto() + "';");
			else
				view.println("var preco = '';");
			
			view.println("var cont = " + cont + ";");
			view.println("var sucesso = true;");						
		} catch (Exception e) {
			view.println("var sucesso = false;");
		}
	}
	
	public void ajaxValorVendaMaterial(WebRequestContext request, Materialrelacionado rel){
		request.getServletResponse().setContentType("text/html");
		Material material = materialService.load(rel.getMaterial(), "material.cdmaterial, material.valorvenda, material.valorcusto");
		
		String valorvenda = "0,00";
		if(material != null && material.getValorvenda() != null){
			valorvenda = SinedUtil.descriptionDecimal(material.getValorvenda());
		}
		String valorcusto = "0,00";
		if(material != null && material.getValorcusto() != null){
			valorcusto = SinedUtil.descriptionDecimal(material.getValorcusto());
		}
			
		View.getCurrent().println("var valorvenda = '" + valorvenda + "';");
		View.getCurrent().println("var valorcusto = '" + valorcusto + "';");
	}
	
	
	public void ajaxQuantidadeEstoqueMaterial(WebRequestContext request, Materialrelacionado rel){
		request.getServletResponse().setContentType("text/html");
		
		List<Empresa> listaMaterialEmprsa = rel.getListaMaterialEmpresas();
		String whereInEmpresas = SinedUtil.listAndConcatenate(listaMaterialEmprsa, "cdpessoa", ",");
		
		if (StringUtils.isBlank(whereInEmpresas)) {
			whereInEmpresas = new SinedUtil().getListaEmpresa();
		}
		
		Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntradaWhereInEmpresa(rel.getMaterial(), null, whereInEmpresas, null);
		if (entrada == null){
			entrada = 0d;
		}
		Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaidaWhereInEmpresa(rel.getMaterial(), null, whereInEmpresas, null);
		if (saida == null){
			saida = 0d;
		}
		Double qtddisponivel = entrada - saida;
		
		View.getCurrent().println("var quantidadeEstoque = '" + SinedUtil.descriptionDecimal(qtddisponivel) + "';");
	}
	
	
	@Override
	protected Material carregar(WebRequestContext request, Material bean) throws Exception {
		bean = super.carregar(request, bean);		
		return bean;
	}
	
	/**
	* M�todo para verificar a sele��o dos materiais
	*  
	*
	* @param whereIn
	* @return
	* @since Jun 17, 2011
	* @author Luiz Fernando F Silva
	*/
	public boolean verificaSelecaomaterialproducao(String whereIn){
		List<Material> listamaterial = materialService.findForAtualizarmaterial(whereIn);
		List<Material> listamaterialmestre;
		
		if(listamaterial != null && !listamaterial.isEmpty()){
			for(Material material: listamaterial){
				if(material.getProducao() != null && material.getProducao()){
					listamaterialmestre = materialService.findForAtualizarmaterialmestre(whereIn);		
					
					if(listamaterialmestre == null || listamaterialmestre.isEmpty()){					
								return false;							
					}
				}
			}
		}else {			
			listamaterialmestre = materialService.findForAtualizarmaterialmestre(whereIn);		
			
			if(listamaterialmestre == null || listamaterialmestre.isEmpty()){					
						return false;							
			}
		}		
		
		return true;
	}
	
	/**
	* Action que faz a valida��o e abre uma pop-up para atualizar valores do material.
	*
	*
	* @param request
	* @return
	* @since Jun 15, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView abrirAlterarvalores(WebRequestContext request){
		
		Materialajustarpreco materialajustarpreco = new Materialajustarpreco();
		String whereIn = SinedUtil.getItensSelecionados(request);
		materialajustarpreco.setWhereIn(whereIn);
		
		//verifica a sele��o dos materiais de produ��o com as mat�rias primas
		if(!verificaSelecaomaterialproducao(whereIn)){
			SinedUtil.fechaPopUp(request);
			request.addError("N�o � poss�vel atualizar somente produtos de produ��o. Selecione as mat�rias primas.");			
			SinedUtil.redirecionamento(request, "/suprimento/crud/Material?ACAO=consultar&cdmaterial=" + whereIn);
			return new ModelAndView();
		}
		
		request.setAttribute("acaoSalvar", "ajustarpreco");		
		
		return new ModelAndView("direct:/crud/popup/ajustarprecosMaterial", "bean", materialajustarpreco);
	}
	
	/**
	* M�todo para ajustar valores do material
	*
	* @param request
	* @param bean
	* @since Jun 15, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajustarpreco(WebRequestContext request, Materialajustarpreco bean){
				
		String whereIn = bean.getWhereIn();		
		
		List<Material> listamaterial = materialService.findForAtualizarmaterial(whereIn);		
		List<Materialproducao> listamaterialproducao = materialproducaoService.findForAtualizar(whereIn);
		List<Materialunidademedida> listaMaterialunidademedida = materialunidademedidaService.findForAtualizarmaterial(whereIn);
		
		// atualiza os materiais
		if(listamaterial != null && !listamaterial.isEmpty()){
			for(Material material : listamaterial){
				if(material.getProducao() == null || !material.getProducao())
					materialService.updateMaterialvalor(material, bean);
					
			}	
		}
		listamaterial = materialService.findForAtualizarmaterial(whereIn);
		//atualiza os materiaisproducao
		if(listamaterialproducao != null && !listamaterialproducao.isEmpty()){
			for(Materialproducao materialproducao : listamaterialproducao){
				if(materialproducao.getMaterialmestre() != null && materialproducao.getCdmaterialproducao() != null){
					for(Material material : listamaterial){
						if(material.getCdmaterial().equals(materialproducao.getMaterial().getCdmaterial())){
							materialproducao.setPreco(material.getValorcusto());
							materialproducaoService.updatePreco(materialproducao, bean);
							break;
						}
					}
										
				}
			}
		}
		
		try {
			materialService.recalcularConsumoAposAtualizacaoCusto(whereIn);
			materialService.recalcularCustoKitAposAtualizacaoCusto(whereIn);
		} catch (Exception e) {
			request.addError("Erro ao atualizar consumo ap�s atualiza��o de custo: " +  e.getMessage());
			e.printStackTrace();
			
			SinedUtil.fechaPopUp(request);
			SinedUtil.recarregarPaginaWithClose(request);
			return;
		}
		
		List<Material> listamaterialmestre = materialService.findForAtualizarmaterialmestre(whereIn);		
		if(listamaterialmestre != null && !listamaterialmestre.isEmpty()){
			for(Material materialmestre : listamaterialmestre){		
				if(materialmestre.getListaProducao() != null && !materialmestre.getListaProducao().isEmpty()){
					listamaterialproducao = materialproducaoService.findListaProducao(materialmestre.getCdmaterial());					
					if(listamaterialproducao != null && !listamaterialproducao.isEmpty()){
						materialService.updateMaterialmestrevalor(materialmestre, bean);
					}
				}
			}
		}
		
		//atualiza Materialunidademedida
		for (Materialunidademedida materialunidademedida: listaMaterialunidademedida){
			Double valorunitario = materialunidademedida.getValorunitario();
			valorunitario += (valorunitario * bean.getPercentual() / 100);
			materialunidademedida.setValorunitario(valorunitario);
			materialunidademedidaService.updateValorunitario(materialunidademedida);
		}
		
		//Atualiza os demais campos: NCM, Peso Liquido, Peso Bruto, Material Mestre da Grade
		listamaterial = materialService.findListagem(whereIn, "", true,null);
		if(listamaterial != null && !listamaterial.isEmpty()){
			for(Material material : listamaterial){
				materialService.updateMaterialAtualizarLote(material, bean);
			}
		}
		
		request.addMessage("Material/Servi�o atualizado(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
		SinedUtil.recarregarPaginaWithClose(request);
	}
	

	/**
	* M�todo ajax para buscar a unidademedida do material
	*
	* @param request
	* @since Aug 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxCarregaUnidadeMaterial(WebRequestContext request){
		String cdmaterial = request.getParameter("cdmaterial");
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		view.println("var sucesso = false;");
		
		if(cdmaterial != null){
			Material material = new Material();
			material.setCdmaterial(Integer.parseInt(cdmaterial));
			material = materialService.unidadeMedidaMaterial(material);
			
			if(material != null && material.getUnidademedida() != null){
				view.println("var unidademedida = '" + material.getUnidademedida().getNome() + "';");
				view.println("sucesso = true;");
			}
		}
	}
	
	/**
	 * 
	 * M�todo que carrega e retorna um boolean dizendo se o material � um servi�o ou n�o.
	 *
	 *@author Thiago Augusto
	 *@date 28/03/2012
	 * @param request
	 * @return
	 */
	public ModelAndView verificarServico(WebRequestContext request, Entregamaterial bean){
		JsonModelAndView json = new JsonModelAndView();
		if (bean.getMaterial() != null && bean.getMaterial().getCdmaterial() != null){
			boolean retorno = false;
			Material material = materialService.carregaMaterialServico(bean.getMaterial());
			if (material.getServico() != null && material.getServico())
				retorno = true;
			json.addObject("retorno", retorno);
		}
		return json;
	}
	
	/**
	 * 
	 * M�todo para inserir material � partir da tela de CONFER�NCIA DE DADOS DA NF-E.
	 *
	 * @name cadastraMaterial
	 * @param request
	 * @return void
	 * @author Thiago Augusto
	 * @date 03/04/2012
	 *
	 */
	public void cadastraMaterial(WebRequestContext request){
		String nome = request.getParameter("nome");
		String unidadeMedida = request.getParameter("unidadeMedida");
		
		Material material = new Material();
		material.setNome(nome);
		material.setUnidademedida(unidademedidaService.findBySimbolo(unidadeMedida));
		request.getSession().setAttribute("conferenciaNFe", true);
		
//		REDIRECIONAR PARA O ENTRADA E DEPOIS MUDAR O �CONE DA TELA DE CONFER�NCIA
		
	}
	
	@Override
	protected Material criar(WebRequestContext request, Material form)
			throws Exception {
		
		if(request.getParameter("novoMaterial") != null){
			String unidadeMedida = request.getParameter("unidadeMedida");
			form.setNome(request.getParameter("nome"));
			if(unidadeMedida != null && !unidadeMedida.equals("") && !unidadeMedida.equals("undefined")){
				form.setUnidademedida(unidademedidaService.findBySimbolo(unidadeMedida));
			}
			form.setAtivo(Boolean.TRUE);
		}
		Usuario usuario = SinedUtil.getUsuarioLogado();
		request.setAttribute("RESTRICAO_FORNECEDOR_PRODUTO", usuario != null && usuario.getRestricaofornecedorproduto() 
				!= null && usuario.getRestricaofornecedorproduto());
		
		String fromRegistrarentradafiscal = request.getParameter("fromRegistrarentradafiscal");
		String cddocumento = request.getParameter("cddocumento");
		String cdfornecedor = request.getParameter("cdfornecedor");
		String nomefornecedor = request.getParameter("nomefornecedor");
		String whereIn = request.getParameter("whereIn");
		
		if(fromRegistrarentradafiscal != null && !fromRegistrarentradafiscal.equals("") && "true".equals(fromRegistrarentradafiscal) && 
				cddocumento != null && !"".equals(cddocumento) && cdfornecedor != null && !"".equals(cdfornecedor) && !"".equals(whereIn) ){
			request.setAttribute("fromRegistrarentradafiscal", true);
			request.setAttribute("registrarentradafiscalcddocumento", whereIn);
			form.setIdDocumentoRegistrarEntrada(whereIn);
			form.setIdFornecedorRegistrarEntrada(cdfornecedor);
			Set<Materialfornecedor> listaMaterialfornecedor = new ListSet<Materialfornecedor>(Materialfornecedor.class);
			Materialfornecedor materialfornecedor = new Materialfornecedor();
			materialfornecedor.setFornecedor(new Fornecedor(Integer.parseInt(cdfornecedor),nomefornecedor));
			listaMaterialfornecedor.add(materialfornecedor);
			form.setListaFornecedor(listaMaterialfornecedor);
			form.setNome("Servi�o - " + nomefornecedor);
			form.setServico(true);
			return form;
		}
		
		return super.criar(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Material form) throws CrudException {
		if(form.getIdDocumentoRegistrarEntrada() != null && !"".equals(form.getIdDocumentoRegistrarEntrada()) && 
				form.getIdFornecedorRegistrarEntrada() != null && !"".equals(form.getIdFornecedorRegistrarEntrada())){
			String idDocumento = form.getIdDocumentoRegistrarEntrada();
			String cdFornecedor = form.getIdFornecedorRegistrarEntrada();
			super.doSalvar(request, form);
			
			boolean existefornecedor = false;
			if(form.getListaFornecedor() != null && !form.getListaFornecedor().isEmpty()){
				for(Materialfornecedor mf : form.getListaFornecedor()){
					if(mf.getFornecedor() != null && mf.getFornecedor().getCdpessoa() != null && mf.getFornecedor().getCdpessoa().toString().equals(cdFornecedor)){
						existefornecedor = true;
						break;
					}
				}
			}
			request.clearMessages();
			request.addMessage("Material cadastrado com sucesso.");
			if(existefornecedor)
				return new ModelAndView("redirect:/fiscal/crud/Entradafiscal?ACAO=criar&fromRegistrarentradafiscal=true&cddocumento=" + idDocumento);
			else {
				request.addError("N�o foi poss�vel registrar a entrada fiscal. Fornecedor do material � diferente do fornecedor da conta a pagar.");
				return new ModelAndView("redirect:/financeiro/crud/Contapagar?ACAO=consultar&cddocumento=" + idDocumento);
			}
		}
		return super.doSalvar(request, form);
	}
	
	/**
	 * M�todo ajax que busca o tipo de material de acordo com a altura e comprimento
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaTipomaterial(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		String alturastr = request.getParameter("altura");
		String largurastr = request.getParameter("largura");		
		Double altura = null;
		Double largura = null;	
		String materialtipostr = "";
		if(alturastr != null && !"".equals(alturastr)){
			altura = Double.parseDouble(alturastr.replace(",", "."));			
		}
		if(largurastr != null && !"".equals(largurastr)){
			largura = Double.parseDouble(largurastr.replace(",", "."));
		}
		if(altura != null && largura != null){
			List<Materialtipo> listaMaterialtipo = materialtipoService.buscaMaterialtipoIntervaloAlturaLargura(altura, largura);
			if(listaMaterialtipo != null && !listaMaterialtipo.isEmpty()){
				Integer id = listaMaterialtipo.get(0).getCdmaterialtipo();				
				if(id != null){
					materialtipostr = "br.com.linkcom.sined.geral.bean.Materialtipo[cdmaterialtipo=" + id + "]";
				}
			}
		}
		
		view.println("var materialtipo = '" + materialtipostr + "';");
	}
	
	/**
	 * M�todo ajax que seta o atributo MaterialService.NOME_PARAM_MATERIALCLASSE_SIMILAR com a classe do material
	 *
	 * @param request
	 * @author Luiz Fernando, Rodrigo Freitas
	 */
	public void ajaxSetAtributoClasseprodutoForMaterialsimilar(WebRequestContext request, Material material){
		request.getSession().setAttribute(MaterialService.NOME_PARAM_MATERIALCLASSE_SIMILAR, materialService.findClassesWithoutLoad(material));
	}
	
	/**
	 * Ajax para buscar a unidade de medida do item de produ��o.
	 *
	 * @param request
	 * @param materialproducao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/03/2013
	 */
	public ModelAndView ajaxBuscaUnidademedidaProducao(WebRequestContext request, Materialproducao materialproducao){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(materialproducao.getMaterial() != null){
			Material material = materialService.getSimboloUnidadeMedidaETempoEntrega(materialproducao.getMaterial());
			if(material != null){
				if(material.getUnidademedida() != null && material.getUnidademedida().getSimbolo() != null){
					jsonModelAndView.addObject("unidademedida", material.getUnidademedida().getSimbolo());
				}
				if(material.getValorcusto() != null){
					jsonModelAndView.addObject("valorcusto", material.getValorcusto());
				}
			}
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxBuscaTipoBanda (WebRequestContext request, Materialproducao materialproducao){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(parametrogeralService.getBoolean("RECAPAGEM")){
			if(materialproducao.getMaterial() != null){
				Material material = materialService.getSimboloUnidadeMedidaETempoEntrega(materialproducao.getMaterial());
				if(material.getMaterialgrupo()!=null){
					Materialgrupo grupo = material.getMaterialgrupo();
					if(Boolean.TRUE.equals(grupo.getBanda())){
						jsonModelAndView.addObject("desabilitarBandas", false);
						jsonModelAndView.addObject("bandaAtiva", true);
					}else if(Boolean.TRUE.equals(grupo.getAcompanhaBanda())){
						jsonModelAndView.addObject("desabilitarBandas", false);
						jsonModelAndView.addObject("acompanhaBandaAtiva", true);
					}else{
						jsonModelAndView.addObject("desabilitarBandas", true);
					}
				}else{
					jsonModelAndView.addObject("desabilitarBandas", true);
				}
			}
			jsonModelAndView.addObject("parametroInativo", false);
		}else{
			jsonModelAndView.addObject("parametroInativo", true);
		}
		return jsonModelAndView;
	}
	
	public ModelAndView visualizarMaterialhistorico(WebRequestContext request, Materialhistorico materialhistorico){
		materialhistorico = materialhistoricoService.loadForEntrada(materialhistorico);
		
		request.setAttribute("TEMPLATE_beanName", materialhistorico);
		request.setAttribute("TEMPLATE_beanClass", Materialhistorico.class);
		request.setAttribute("descricao", "Material");
		request.setAttribute("consultar", true);

		return new ModelAndView("direct:crud/popup/visualizacaoMaterial").addObject("materialhistorico", materialhistorico);
	}
	
	/**
	 * M�todo para copiar as formulas da produ��o de acordo com o tipo de material
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 17/01/2014
	 */
	public ModelAndView copiarFormulaproducao(WebRequestContext request, Material material){
		if(material.getCdmaterial() != null && material.getListaProducao() != null && 
				!material.getListaProducao().isEmpty()){
			Set<Materialproducao> listaProducao = new ListSet<Materialproducao>(Materialproducao.class);
			Material bean = materialService.loadWithMaterialtipo(material);
			if(bean != null && bean.getMaterialtipo() != null){
				for(Materialproducao materialproducao : material.getListaProducao()){
					if(materialproducao.getMaterial() != null && materialproducao.getMaterial().getCdmaterial() != null &&
						!materialproducao.getMaterial().equals(material)){
						Material item = materialService.loadWithMaterialtipo(materialproducao.getMaterial());
						if(item != null && item.getMaterialtipo() != null && item.getMaterialtipo().getCdmaterialtipo() != null &&
								item.getMaterialtipo().equals(bean.getMaterialtipo())){
							listaProducao.add(materialproducao);
						}
					}
				}
			}
			material.setListaProducao(listaProducao);
		}
		
		return new JsonModelAndView().addObject("bean", material);
	}
	
	public ModelAndView copiarFormulakit(WebRequestContext request, Material material){
		if(material.getCdmaterial() != null && material.getListaMaterialrelacionado() != null && 
				!material.getListaMaterialrelacionado().isEmpty()){
			Set<Materialrelacionado> listaMaterialrelacionado = new ListSet<Materialrelacionado>(Materialrelacionado.class);
			Material bean = materialService.loadWithMaterialtipo(material);
			if(bean != null && bean.getMaterialtipo() != null){
				for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
					if(materialrelacionado.getMaterialpromocao() != null && materialrelacionado.getMaterialpromocao().getCdmaterial() != null &&
						!materialrelacionado.getMaterialpromocao().equals(material)){
						Material item = materialService.loadWithMaterialtipo(materialrelacionado.getMaterialpromocao());
						if(item != null && item.getMaterialtipo() != null && item.getMaterialtipo().getCdmaterialtipo() != null &&
								item.getMaterialtipo().equals(bean.getMaterialtipo())){
							listaMaterialrelacionado.add(materialrelacionado);
						}
					}
				}
			}
			material.setListaMaterialrelacionado(listaMaterialrelacionado);
		}
		
		return new JsonModelAndView().addObject("bean", material);
	}
	
	public ModelAndView copiarFormulakitflexivel(WebRequestContext request, Material material){
		if(material.getCdmaterial() != null && material.getListaMaterialkitflexivel() != null && 
				!material.getListaMaterialkitflexivel().isEmpty()){
			Set<Materialkitflexivel> listaMaterialkitflexivel = new ListSet<Materialkitflexivel>(Materialkitflexivel.class);
			Material bean = materialService.loadWithMaterialtipo(material);
			if(bean != null && bean.getMaterialtipo() != null){
				for(Materialkitflexivel mkf : material.getListaMaterialkitflexivel()){
					if(mkf.getMaterialkit() != null && mkf.getMaterialkit().getCdmaterial() != null &&
						!mkf.getMaterialkit().equals(material)){
						Material item = materialService.loadWithMaterialtipo(mkf.getMaterialkit());
						if(item != null && item.getMaterialtipo() != null && item.getMaterialtipo().getCdmaterialtipo() != null &&
								item.getMaterialtipo().equals(bean.getMaterialtipo())){
							listaMaterialkitflexivel.add(mkf);
						}
					}
				}
			}
			material.setListaMaterialkitflexivel(listaMaterialkitflexivel);
		}
		
		return new JsonModelAndView().addObject("bean", material);
	}
	
	public ModelAndView openPopUpFormulaProducao(WebRequestContext request, Materialproducao materialproducao){
		return new ModelAndView("direct:crud/popup/formulaProducao", "materialproducao", materialproducao);
	}
	
	public ModelAndView calculaValorConsumoFormula(WebRequestContext request, Material material) throws ScriptException{
		try{
			materialproducaoService.getValorvendaproducao(material);
			
			if(material.getListaProducao() != null){
				for (Materialproducao materialproducao : material.getListaProducao()) {
					boolean producaoIgual = (materialproducao.getCdmaterialproducao() != null && 
							material.getCdmaterialproducao() != null &&
							materialproducao.getCdmaterialproducao().equals(material.getCdmaterialproducao())) || 
							(materialproducao.getCdmaterialproducao() == null && material.getCdmaterialproducao() == null);
					
					boolean materialIgual = materialproducao.getMaterial() != null && 
												materialproducao.getMaterial().getCdmaterial() != null &&
												materialproducao.getMaterial().getCdmaterial().equals(material.getCdmaterialtrans());
					
					if(producaoIgual && materialIgual){
						return new JsonModelAndView().addObject("consumo", materialproducao.getConsumo());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonModelAndView().addObject("erro", e.getMessage());
		}
		return new JsonModelAndView();
	}
	
	/**
	 * M�todo que abre a popup de formula para o kit
	 *
	 * @param request
	 * @param materialrelacionado
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public ModelAndView openPopUpFormulaKit(WebRequestContext request, Materialrelacionado materialrelacionado){
		return new ModelAndView("direct:crud/popup/formulaKit", "materialrelacionado", materialrelacionado);
	}
	
	public ModelAndView openPopUpFormulaKitFlexivel(WebRequestContext request, Materialkitflexivel materialkitflexivel){
		return new ModelAndView("direct:crud/popup/formulaKitflexivel", "materialkitflexivel", materialkitflexivel);
	}
	
	public ModelAndView openPopUpFormulaValorvenda(WebRequestContext request, Material material){
		return new ModelAndView("direct:crud/popup/formulaValorvenda", "material", material);
	}
	
	/**
	* M�todo que abre a popup para cadastro de formula de margem de contribui��o
	*
	* @param request
	* @param material
	* @return
	* @since 24/06/2015
	* @author Luiz Fernando
	*/
	public ModelAndView openPopUpFormulaMargemcontribuicao(WebRequestContext request, Material material){
		return new ModelAndView("direct:crud/popup/formulaMargemcontribuicao", "material", material);
	}
	
	public void calcularValorVendaByFormulaAJAX(WebRequestContext request, Material material){
		Double valorvendaformula = null;
		try {
			valorvendaformula = materialService.getValorvendaByCadastroMaterial(material, false);
		} catch (Exception e) {}
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		view.println("var valorvendaformula = '" + (valorvendaformula != null ? SinedUtil.descriptionDecimal(valorvendaformula) : "") + "';");
	}
	
	/**
	* M�todo que abre uma popup para cadastro da f�rmula de custo
	*
	* @param request
	* @param material
	* @return
	* @since 27/05/2015
	* @author Luiz Fernando
	*/
	public ModelAndView openPopUpFormulaCusto(WebRequestContext request, Material material){
		return new ModelAndView("direct:crud/popup/formulaCusto", "material", material);
	}
	
	public ModelAndView openPopUpFormulaVendaminimo(WebRequestContext request, Material material){
		return new ModelAndView("direct:crud/popup/formulaVendaminimo", "material", material);
	}
	
	public ModelAndView openPopUpFormulaVendamaximo(WebRequestContext request, Material material){
		return new ModelAndView("direct:crud/popup/formulaVendamaximo", "material", material);
	}
	
	/**
	* M�todo que calcula o custo do material de acordo com a f�rmula cadastrada
	*
	* @see br.com.linkcom.sined.geral.service.MaterialformulacustoService#calculaCustoMaterial(Material material) throws ScriptException
	*
	* @param request
	* @param material
	* @return
	* @throws ScriptException
	* @since 27/05/2015
	* @author Luiz Fernando
	*/
	public ModelAndView calculaFormulacustoAjax(WebRequestContext request, Material material) throws ScriptException{
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		try {
			Double custo = materialformulacustoService.calculaCustoMaterial(material);
			if(Double.isNaN(custo)){
				throw new ScriptException("O resultado � NaN.");
			}
			
			jsonModelAndView.addObject("custo", custo);
		} catch (ScriptException e) {
			e.printStackTrace();
			return jsonModelAndView.addObject("erroScript", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return jsonModelAndView.addObject("erro", e.getMessage());
		}
		return jsonModelAndView;
	}
	
	public ModelAndView calculaFormulavendaminimoAjax(WebRequestContext request, Material material) throws ScriptException{
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		try {
			Double vendaminimo = materialformulavendaminimoService.calculavendaminimoMaterial(material);
			if(Double.isNaN(vendaminimo)){
				throw new ScriptException("O resultado � NaN.");
			}
			
			jsonModelAndView.addObject("vendaminimo", vendaminimo);
		} catch (ScriptException e) {
			e.printStackTrace();
			return jsonModelAndView.addObject("erroScript", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return jsonModelAndView.addObject("erro", e.getMessage());
		}
		return jsonModelAndView;
	}
	
	public ModelAndView calculaFormulavendamaximoAjax(WebRequestContext request, Material material) throws ScriptException{
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		try {
			Double vendamaximo = materialformulavendamaximoService.calculavendamaximoMaterial(material);
			if(Double.isNaN(vendamaximo)){
				throw new ScriptException("O resultado � NaN.");
			}
			
			jsonModelAndView.addObject("vendamaximo", vendamaximo);
		} catch (ScriptException e) {
			e.printStackTrace();
			return jsonModelAndView.addObject("erroScript", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return jsonModelAndView.addObject("erro", e.getMessage());
		}
		return jsonModelAndView;
	}
	
	
	
	/**
	 * M�todo que calcula a quantidade do kit
	 *
	 * @param request
	 * @param material
	 * @return
	 * @throws ScriptException
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public ModelAndView calculaQuantidadeKitFormula(WebRequestContext request, Material material) throws ScriptException{
		try{
			materialrelacionadoService.calculaQuantidadeKit(material);
			
			if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
				for (Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()) {
					boolean producaoIgual = (materialrelacionado.getCdmaterialrelacionado() != null && 
							material.getCdmaterialrelacionado() != null &&
							materialrelacionado.getCdmaterialrelacionado().equals(material.getCdmaterialrelacionado())) || 
							(materialrelacionado.getCdmaterialrelacionado() == null && material.getCdmaterialrelacionado() == null);
					
					boolean materialIgual = materialrelacionado.getMaterialpromocao() != null && 
												materialrelacionado.getMaterialpromocao().getCdmaterial() != null &&
												materialrelacionado.getMaterialpromocao().getCdmaterial().equals(material.getCdmaterialtrans());
					
					if(producaoIgual && materialIgual){
						return new JsonModelAndView().addObject("quantidade", materialrelacionado.getQuantidade());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonModelAndView().addObject("erro", e.getMessage());
		}
		return new JsonModelAndView();
	}
	
	public ModelAndView calculaQuantidadeKitFlexivelFormula(WebRequestContext request, Material material) throws ScriptException{
		try{
			materialkitflexivelService.calculaQuantidadeKitflexivel(material);
			
			if(material.getListaMaterialkitflexivel() != null && !material.getListaMaterialkitflexivel().isEmpty()){
				for (Materialkitflexivel materialkitflexivel : material.getListaMaterialkitflexivel()) {
					boolean producaoIgual = (materialkitflexivel.getCdmaterialkitflexivel() != null && 
							material.getCdmaterialkitflexivel() != null &&
							materialkitflexivel.getCdmaterialkitflexivel().equals(material.getCdmaterialkitflexivel())) || 
							(materialkitflexivel.getCdmaterialkitflexivel() == null && material.getCdmaterialkitflexivel() == null);
					
					boolean materialIgual = materialkitflexivel.getMaterialkit() != null && 
												materialkitflexivel.getMaterialkit().getCdmaterial() != null &&
												materialkitflexivel.getMaterialkit().getCdmaterial().equals(material.getCdmaterialtrans());
					
					if(producaoIgual && materialIgual){
						return new JsonModelAndView().addObject("quantidade", materialkitflexivel.getQuantidade());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonModelAndView().addObject("erro", e.getMessage());
		}
		return new JsonModelAndView();
	}
	
	public ModelAndView calculaFormulaPesoAjax(WebRequestContext request, Material material) throws ScriptException{
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		try {
			Double peso = materialformulapesoService.calculaPesoMaterial(material);
			if(Double.isNaN(peso)){
				throw new ScriptException("O resultado � NaN.");
			}
			
			jsonModelAndView.addObject("peso", peso);
		} catch (ScriptException e) {
			e.printStackTrace();
			return jsonModelAndView.addObject("erroScript", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return jsonModelAndView.addObject("erro", e.getMessage());
		}
		return jsonModelAndView;
	}
	
	/**
	 * M�todo ajax para copiar a f�rmula do material para os materiais do mesmo tipo
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 * @since 10/02/2014
	 */
	public ModelAndView copiarFormulaPesoByMaterialtipo(WebRequestContext request, Material bean){
		boolean sucesso = false;
		if(bean.getMaterialtipo() != null && bean.getMaterialtipo().getCdmaterialtipo() != null && 
				bean.getMaterialformulapesoTrans() != null && 
				bean.getMaterialformulapesoTrans().getFormula() != null &&
				!"".equals(bean.getMaterialformulapesoTrans().getFormula())){
			List<Material> lista = materialService.findForCopiarFormulapeso(bean.getMaterialtipo(), bean.getCdmaterial() != null ? bean : null);
			if(lista != null && !lista.isEmpty()){
				for(Material material : lista){
					Integer ordem = 1;
					boolean existFormula = false;
					if(material.getListaMaterialformulapeso() != null && !material.getListaMaterialformulapeso().isEmpty()){
						for(Materialformulapeso materialformulapeso : material.getListaMaterialformulapeso()){
							if(materialformulapeso.getFormula() != null && 
									(materialformulapeso.getFormula().equals(bean.getMaterialformulapesoTrans().getFormula()) ||
									(materialformulapeso.getFormula().equals("\\\r\\\n"+bean.getMaterialformulapesoTrans().getFormula()+"\\\r\\\n")))){
								existFormula = true;
								
								if(bean.getMaterialformulapesoTrans().getOrdem() != null)
									materialformulapeso.setOrdem(bean.getMaterialformulapesoTrans().getOrdem());
								if(bean.getMaterialformulapesoTrans().getIdentificador() != null &&
										!"".equals(bean.getMaterialformulapesoTrans().getIdentificador()))
									materialformulapeso.setIdentificador(bean.getMaterialformulapesoTrans().getIdentificador());
								
								materialformulapeso.setFormula(bean.getMaterialformulapesoTrans().getFormula());
								materialformulapeso.setMaterial(material);
								
								materialformulapesoService.saveOrUpdate(materialformulapeso);
								break;
							}
								
							if(materialformulapeso.getOrdem() != null && materialformulapeso.getOrdem() > ordem){
								ordem = materialformulapeso.getOrdem();
							}
						}
					}
					
					if(!existFormula){
						Materialformulapeso materialformulapeso = new Materialformulapeso();
						materialformulapeso.setOrdem(bean.getMaterialformulapesoTrans().getOrdem());
						materialformulapeso.setIdentificador(bean.getMaterialformulapesoTrans().getIdentificador());
						materialformulapeso.setFormula(bean.getMaterialformulapesoTrans().getFormula());
						materialformulapeso.setMaterial(material);
						
						materialformulapesoService.saveOrUpdate(materialformulapeso);
					}
				}
				sucesso = true;
			}
		}
		
		return new JsonModelAndView().addObject("sucesso", sucesso);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Material bean)
			throws Exception {
		if (IntegracaoEmporiumUtil.util.haveIntegracaoEcf())
			request.addError("N�o � possivel excluir material quando a integra��o com ECF estiver ativada.");
		else super.excluir(request, bean);
	}
	
	/**
	 * Action que em ajax busca os materiais e inclui na aba de mat�rias-primas.
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/04/2014
	 */
	public ModelAndView ajaxAdicionaMateriasprimasByOrdemcompra(WebRequestContext request, Material material){
		JsonModelAndView json = new JsonModelAndView();
		if(material.getOrdemcompratrans() != null){
			List<MaterialproducaoAjaxBean> listaMaterial = new ArrayList<MaterialproducaoAjaxBean>();
			List<Ordemcompramaterial> listaOC = ordemcompramaterialService.findByOrdemcompra(material.getOrdemcompratrans());
			for (Ordemcompramaterial ordemcompramaterial : listaOC) {
				if(ordemcompramaterial.getMaterial() != null && 
						ordemcompramaterial.getQtdpedida() != null && 
						ordemcompramaterial.getQtdpedida() > 0){
					MaterialproducaoAjaxBean bean = new MaterialproducaoAjaxBean();
					
					bean.setCdmaterial(ordemcompramaterial.getMaterial().getCdmaterial());
					bean.setNome(ordemcompramaterial.getMaterial().getAutocompleteDescription());
					bean.setQtde(ordemcompramaterial.getQtdpedida());
					
					Double valorunitario = 0d;
					if(ordemcompramaterial.getQtdpedida() != null && 
							ordemcompramaterial.getQtdefrequencia() != null && 
							ordemcompramaterial.getValor() != null){
						Double qtdepedida = ordemcompramaterial.getQtdpedida() * ordemcompramaterial.getQtdefrequencia();
						valorunitario = ordemcompramaterial.getValor() / qtdepedida;
					}
					bean.setValorunitario(valorunitario);
					
					listaMaterial.add(bean);
				}
			}
			json.addObject("listaMaterial", listaMaterial);
		}
		return json;
		
	}
	
	public ModelAndView abrirReplicarFormulaVendaminimo(WebRequestContext request){
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		return new ModelAndView("direct:/crud/popup/popupReplicarFormulaVendaminimo", "bean", new Material());
	}
	
	public ModelAndView saveReplicarFormulaVendaminimo(WebRequestContext request, Material bean){
		boolean sucessoFormulaVendaminimo = false;
		String msgFormulaVendaminimo = "";
		if(bean.getMaterialgrupo() != null && bean.getMaterialgrupo().getCdmaterialgrupo() != null){
			List<Material> lista = materialService.findForCopiarFormulavalorvendaminimo(bean.getMaterialgrupo(), bean.getCdmaterial() != null ? bean : null);
			try {
				if(bean.getListaMaterialformulavendaminimo() != null && !bean.getListaMaterialformulavendaminimo().isEmpty()){
					if(lista != null && !lista.isEmpty()){
						for(Material material : lista){
							materialformulavendaminimoService.saveReplicarFormulavendaminimo(bean.getListaMaterialformulavendaminimo(), material);
							boolean isValorFormula = materialformulavendaminimoService.setVendaminimoByFormula(material, null, true);
							if(isValorFormula && material.getValorvendaminimo() != null && material.getValorvendaminimo() > 0){
								materialService.updateValorVendaminimo(material, material.getValorvendaminimo());
							}
						}
					}
					sucessoFormulaVendaminimo = true;
				}
			} catch (Exception e) {
				msgFormulaVendaminimo = e.getMessage();
			}
		}
		return new JsonModelAndView().addObject("sucessoFormulaVendaminimo", sucessoFormulaVendaminimo).addObject("msgFormulaVendaminimo", msgFormulaVendaminimo);
	}
	
	public ModelAndView abrirReplicarFormulaVendamaximo(WebRequestContext request){
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		return new ModelAndView("direct:/crud/popup/popupReplicarFormulaVendamaximo", "bean", new Material());
	}
	
	public ModelAndView saveReplicarFormulaVendamaximo(WebRequestContext request, Material bean){
		boolean sucessoFormulaVendamaximo = false;
		String msgFormulaVendamaximo = "";
		if(bean.getMaterialgrupo() != null && bean.getMaterialgrupo().getCdmaterialgrupo() != null){
			List<Material> lista = materialService.findForCopiarFormulavalorvendamaximo(bean.getMaterialgrupo(), bean.getCdmaterial() != null ? bean : null);
			try {
				if(bean.getListaMaterialformulavendamaximo() != null && !bean.getListaMaterialformulavendamaximo().isEmpty()){
					if(lista != null && !lista.isEmpty()){
						for(Material material : lista){
							materialformulavendamaximoService.saveReplicarFormulavendamaximo(bean.getListaMaterialformulavendamaximo(), material);
							boolean isValorFormula = materialformulavendamaximoService.setVendamaximoByFormula(material, null, true);
							if(isValorFormula && material.getValorvendamaximo() != null && material.getValorvendamaximo() > 0){
								materialService.updateValorVendamaximo(material, material.getValorvendamaximo());
							}
						}
					}
					sucessoFormulaVendamaximo = true;
				}
			} catch (Exception e) {
				msgFormulaVendamaximo = e.getMessage();
			}
		}
		return new JsonModelAndView().addObject("sucessoFormulaVendamaximo", sucessoFormulaVendamaximo).addObject("msgFormulaVendamaximo", msgFormulaVendamaximo);
	}
	
	public ModelAndView abrirReplicarFormulaCusto(WebRequestContext request){
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		return new ModelAndView("direct:/crud/popup/popupReplicarFormulaCusto", "bean", new Material());
	}
	
	public ModelAndView saveReplicarFormulaCusto(WebRequestContext request, Material bean){
		boolean sucessoFormulaCusto = false;
		String msgFormulaCusto = "";
		if(bean.getMaterialgrupo() != null && bean.getMaterialgrupo().getCdmaterialgrupo() != null){
			List<Material> lista = materialService.findForCopiarFormulavalorvenda(bean.getMaterialgrupo(), bean.getCdmaterial() != null ? bean : null);
			try {
				if(bean.getListaMaterialformulacusto() != null && !bean.getListaMaterialformulacusto().isEmpty()){
					if(lista != null && !lista.isEmpty()){
						for(Material material : lista){
							materialformulacustoService.saveReplicarFormulacusto(bean.getListaMaterialformulacusto(), material);
							materialService.updateValorCusto(material, materialformulacustoService.calculaCustoMaterial(material));
						}
					}
					sucessoFormulaCusto = true;
				}
			} catch (Exception e) {
				msgFormulaCusto = e.getMessage();
			}
		}
		return new JsonModelAndView().addObject("sucessoFormulaCusto", sucessoFormulaCusto).addObject("msgFormulaCusto", msgFormulaCusto);
	}
	
	public ModelAndView abrirReplicarFormulaValorvenda(WebRequestContext request){
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		return new ModelAndView("direct:/crud/popup/popupReplicarFormulaValorvenda", "bean", new Material());
	}
	
	public ModelAndView saveReplicarFormulaValorvenda(WebRequestContext request, Material bean){
		boolean sucessoFormulaValorvenda = false;
		boolean sucessoFormulaMargemcontribuicao = false;
		String msgFormulavalorvenda = "";
		String msgFormulaMargemcontribuicao = "";
		if(bean.getMaterialgrupo() != null && bean.getMaterialgrupo().getCdmaterialgrupo() != null){
			List<Material> lista = materialService.findForCopiarFormulavalorvenda(bean.getMaterialgrupo(), bean.getCdmaterial() != null ? bean : null);
			try {
				if(bean.getListaMaterialformulavalorvenda() != null && !bean.getListaMaterialformulavalorvenda().isEmpty()){
					if(lista != null && !lista.isEmpty()){
						for(Material material : lista){
							materialformulavalorvendaService.saveReplicarFormulavalorvenda(bean.getListaMaterialformulavalorvenda(), material);
							try {
								Double valorvendaformula = materialService.getValorvendaByCadastroMaterial(material, true);
								if(valorvendaformula != null && valorvendaformula > 0){
									materialService.updateValorVenda(material, valorvendaformula);
								}
							} catch (Exception e) {}
						}
					}
					sucessoFormulaValorvenda = true;
				}
			} catch (Exception e) {
				msgFormulavalorvenda = e.getMessage();
			}
			try {
				if(bean.getListaMaterialformulamargemcontribuicao() != null && !bean.getListaMaterialformulamargemcontribuicao().isEmpty()){
					if(lista != null && !lista.isEmpty()){
						for(Material material : lista){
							materialformulamargemcontribuicaoService.saveReplicarFormulamargemcontribuicao(bean.getListaMaterialformulamargemcontribuicao(), material);
						}
					}
					sucessoFormulaMargemcontribuicao = true;
				}
			} catch (Exception e) {
				msgFormulaMargemcontribuicao = e.getMessage();
			}
		}
		return new JsonModelAndView().addObject("sucessoFormulaValorvenda", sucessoFormulaValorvenda)
									 .addObject("msgFormulaValorvenda", msgFormulavalorvenda)
									 .addObject("sucessoFormulaMargemcontribuicao", sucessoFormulaMargemcontribuicao)
									 .addObject("msgFormulaMargemcontribuicao", msgFormulaMargemcontribuicao);
	}

	/**
	 * Ajax para buscar  a Lista de MaterialProducaoEtapaItem de acordo com a Producaoetapa.
	 *
	 * @param request
	 * @param producaoetapa
	 * @author Lucas Costa
	 * @since 20/06/2014
	 */
	public ModelAndView ajaxGetListaMaterialProducaoEtapaItem(WebRequestContext request, Producaoetapa producaoetapa){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(producaoetapa.getCdproducaoetapa()!=null){
			List <Producaoetapaitem> listProducaoetapaitem;
			listProducaoetapaitem = producaoetapaitemService.findByProducaoetapa(producaoetapa);
			List<MaterialProducaoEtapaItem> listMaterialProducaoEtapaItem = new ListSet<MaterialProducaoEtapaItem>(MaterialProducaoEtapaItem.class);
			for(Producaoetapaitem producaoetapaitem : listProducaoetapaitem){
				listMaterialProducaoEtapaItem.add(new MaterialProducaoEtapaItem(producaoetapaitem, ""));
			}
			jsonModelAndView.addObject("listMaterialProducaoEtapaItem", listMaterialProducaoEtapaItem);
		}	
		return jsonModelAndView;
	}

	/**
	* M�todo que abre uma popUp para o usu�rio definir os valores refente ao custo inicial
	*
	* @param request
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public ModelAndView abrirDefinircustoinicial(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
			return null;
		}
		
		request.setAttribute("acaoSalvar", "saveDefinircustoinicial");
		Material material = new Material();
		material.setWhereIn(whereIn);
		
		return new ModelAndView("direct:/crud/popup/definircustoinicialMaterial", "bean", material);
	}
	
	/**
	* M�todo que abre uma popup para o usu�rio definir qual etiqueta imprimir
	* @param request
	* @return
	* @since 25/11/2014
	* @author Jo�o Vitor
	*/
	public ModelAndView abrirEmitirEtiqueta(WebRequestContext request){
		
		List<ReportTemplateBean> reportTemplateBean = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_MATERIAL);
		MaterialFiltro filtro= new MaterialFiltro();
		
		String origem = request.getParameter("origem");
		String selectedItens = request.getParameter("selectedItens");
		
		if(StringUtils.isEmpty(selectedItens) && "material".equals(origem)){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
			return null;
		}else if(StringUtils.isEmpty(selectedItens) && "entrega".equals(origem)){
			request.addError("Nenhum entrega selecionada.");			
			SinedUtil.redirecionamento(request, "/suprimento/crud/Entrega");
			return null;
		}
		
		String tipoetiqueta = request.getParameter("tipoetiqueta");
		if(StringUtils.isBlank(tipoetiqueta) && "entrega".equals(origem) && reportTemplateBean != null && reportTemplateBean.size() == 1)
			tipoetiqueta = "configuravel";
		
		List<MaterialEtiquetaBean> lst = null;
		if(!StringUtils.isBlank(tipoetiqueta) && "entrega".equals(origem))
			lst = gerarMaterialEtiquetaEntrega(selectedItens, tipoetiqueta);
		else if("material".equals(origem))
			lst = gerarMaterialEtiquetaMaterial(selectedItens);
		else
			lst = new ArrayList<MaterialEtiquetaBean>();
		
		
		StringBuilder whereIn = new StringBuilder("");
		for(int i = 0; i < lst.size(); i++){
			MaterialEtiquetaBean et = lst.get(i);
			whereIn.append(et.getMaterial().getCdmaterial());
			if(i + 1 < lst.size()) whereIn.append(",");
		}
		
		filtro.setListaetiquetamaterial(lst);
		filtro.setWhereIn(whereIn.toString());
		request.setAttribute("tipoetiqueta", tipoetiqueta);
		request.setAttribute("selectedItens", selectedItens);
		request.setAttribute("origem", origem);
		request.setAttribute("listaCategoriaTemplate", reportTemplateBean != null ? reportTemplateBean : new ListSet<ReportTemplateBean>(ReportTemplateBean.class));
		return new ModelAndView("direct:/crud/popup/emitiretiqueta", "filtro", filtro);
	}
	
	private List<MaterialEtiquetaBean> gerarMaterialEtiquetaMaterial(String cdmaterial) {
		List<MaterialEtiquetaBean> lst = new ArrayList<MaterialEtiquetaBean>();
		List<Material> materiais = materialService.buscarMaterialForPopUpEtiqueta(cdmaterial);
		for(Material material: materiais){
			MaterialEtiquetaBean materialEtq = new MaterialEtiquetaBean();
			materialEtq.setLote("");
			materialEtq.setMaterial(material);
			lst.add(materialEtq);
		}
		validaImpressaoUnica(lst);
		return lst;
	}
	
	private List<MaterialEtiquetaBean> gerarMaterialEtiquetaEntrega(String cdentrega, String tipoetiqueta) {
		List<MaterialEtiquetaBean> lst = new ArrayList<MaterialEtiquetaBean>();
		List<Entrega> listaentrega = entregaService.buscarMaterialForPopUpEtiquetaEntrega(cdentrega);
		if(listaentrega == null  || listaentrega.size() == 0) return lst;
		
		for (Entrega entrega : listaentrega) {
			if(entrega.getListaEntregadocumento() == null || entrega.getListaEntregadocumento().size() == 0) continue;
			for (Entregadocumento entregadocumento : entrega.getListaEntregadocumento()) {
				if(entregadocumento.getListaEntregamaterial() == null  || entregadocumento.getListaEntregamaterial().size() == 0) continue;
				for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
					if(entregamaterial.getMaterial().getCdmaterial() == null) continue;
					if(!Boolean.TRUE.equals(entregamaterial.getTipolote()) && !SinedUtil.isListEmpty(entregamaterial.getListaEntregamateriallote())){//M�ltiplos lotes
						for(Entregamateriallote entMatLote: entregamaterial.getListaEntregamateriallote())
							criarMaterialEtiqueta(entrega, entregamaterial, entregadocumento, entMatLote, lst, tipoetiqueta);
					}else
						criarMaterialEtiqueta(entrega, entregamaterial, entregadocumento, null, lst, tipoetiqueta);
				}
			}
		}
		return lst;
	}
	
	private void criarMaterialEtiqueta(Entrega entrega, Entregamaterial entregamaterial, Entregadocumento entregadocumento, Entregamateriallote entMatLote, List<MaterialEtiquetaBean> listamaterialetiqueta, String tipoetiqueta){
		MaterialEtiquetaBean materialEtq = new MaterialEtiquetaBean();
		materialEtq.setMaterial(entregamaterial.getMaterial());
		materialEtq.setCdEntrega(entrega.getCdentrega());
		materialEtq.setCdentregadocumento(entregadocumento.getCdentregadocumento());
		materialEtq.setCdEntregaMaterial(entregamaterial.getCdentregamaterial());
		materialEtq.setNumerocopiasimpressaoetiqueta(entMatLote != null ? entMatLote.getQtde().intValue() : entregamaterial.getQtde().intValue());
		materialEtq.setTipoetiqueta(tipoetiqueta);
		
		Empresa empresa = entrega.getEmpresa();
		Loteestoque lote = entMatLote != null ? entMatLote.getLoteestoque() : entregamaterial.getLoteestoque();
		materialEtq.setEmpresaRecebimento(empresa != null && empresa.getNome()!=null? empresa.getNome(): "");

		if(lote != null && lote.getCdloteestoque() != null){
			materialEtq.setLote(lote.getNumero().toString());
			List<Lotematerial> listaLotematerial = lotematerialService.findAllByLoteestoqueMaterial(lote, materialEtq.getMaterial());
			if(listaLotematerial!=null && listaLotematerial.size() > 0 && listaLotematerial.get(0).getValidade()!=null){
				materialEtq.setValidadeLote(new SimpleDateFormat("dd/MM/yyyy").format(listaLotematerial.get(0).getValidade()));												
			}
		}
		List<MaterialEtiquetaBean> etiquetas;
		if("configuravel".equals(tipoetiqueta))
			etiquetas = expandirMaterialEtiqueta(materialEtq, ordemcompraService.getOrdemcompraPorEntrega(entrega), movimentacaoestoqueService.findMovimentacoesNaoCanceladasPorEntrega(entregamaterial));
		else
			etiquetas = Arrays.asList(materialEtq);
		
		for(MaterialEtiquetaBean et: etiquetas){
			if(incluiBeanEtiquetaNaLista(listamaterialetiqueta, et)) listamaterialetiqueta.add(et);
		}
	}
	
	private List<MaterialEtiquetaBean> expandirMaterialEtiqueta(MaterialEtiquetaBean materialEtq, List<Ordemcompra> lstOc, List<Movimentacaoestoque> lstMov) {
		List<MaterialEtiquetaBean> etiquetas = new ArrayList<MaterialEtiquetaBean>();
		if((lstMov != null && lstMov.size() > 0 && lstOc != null && lstOc.size() > 0)){
			for(Ordemcompra oc: lstOc){
				for(Movimentacaoestoque me: lstMov)
					etiquetas.add(new MaterialEtiquetaBean(materialEtq, oc, me));
			}
		}else if(lstMov != null && lstMov.size() > 0){
			for(Movimentacaoestoque me: lstMov)
				etiquetas.add(new MaterialEtiquetaBean(materialEtq, me));
		}else if(lstOc != null && lstOc.size() > 0){
			for(Ordemcompra oc: lstOc){
				etiquetas.add(new MaterialEtiquetaBean(materialEtq, oc));
			}
		}else{
			etiquetas.add(materialEtq);
		}
		return etiquetas;
	}
	
	private boolean incluiBeanEtiquetaNaLista(List<MaterialEtiquetaBean> listaJaPreenchida, MaterialEtiquetaBean materialAdd){
		int index = listaJaPreenchida.indexOf(materialAdd);
		if(index == -1) return true;
		for(int j = index; j < listaJaPreenchida.size(); j++)
			listaJaPreenchida.get(j).addNumerocopiasimpressaoetiqueta(materialAdd.getNumerocopiasimpressaoetiqueta());
		return false;
	}
	
	/**
	 * Metodo respons�vel por validar se etiqueta ser� impressa somente uma vez de acordo com sua unidade de medida
	 * @param listamaterialetiqueta
	 * @author C�sar
	 */
	private void validaImpressaoUnica(List<MaterialEtiquetaBean> listamaterialetiqueta) {
		for (MaterialEtiquetaBean material : listamaterialetiqueta) {
			if(material.getMaterial().getUnidademedida() != null && material.getMaterial().getUnidademedida().getEtiquetaunica() != null){
				if(material.getMaterial().getUnidademedida().getEtiquetaunica().equals(true))
					material.setNumerocopiasimpressaoetiqueta(1);
			}else if(material.getMaterial().getUnidademedida() != null && material.getMaterial().getUnidademedida().getEtiquetaunica() == null){
				material.getMaterial().getUnidademedida().setEtiquetaunica(Boolean.FALSE);
			}
		}
	}
	
	/**
	* M�todo que salva os valores dos custos iniciais e fecha a popup
	*
	* @see br.com.linkcom.sined.geral.service.MaterialService#saveDefinircustoinicial(Material material, String whereIn)
	*
	* @param request
	* @param material
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public void saveDefinircustoinicial(WebRequestContext request, Material material){
		String whereIn = material.getWhereIn();
		
		if(!StringUtils.isEmpty(whereIn)){
			materialService.saveDefinircustoinicial(material, whereIn);
			
			request.addMessage("O custo inicial dos materiais selecionados foram definidos");
		}else {
			request.addError("Nenhum item selecionado.");			
		}
		
		SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
	}
	
	/**
	* M�todo que recalcula o custo dos materiais selecionados
	*
	* @param request
	* @since 29/08/2014
	* @author Luiz Fernando
	*/
	public void recalcularCustoMedio(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
			return;
		}
		
		List<Material> listaMaterialInvalido = materialService.buscarMaterialValidacaoCustomedio(whereIn);
		if(SinedUtil.isListNotEmpty(listaMaterialInvalido)){
			for(Material material : listaMaterialInvalido){
				Boolean materialSemCustoInicial = material.getValorcustoinicial() == null;
				
				Boolean materialCustoEmpresaSemCustoInicial = false;
				if(SinedUtil.isListNotEmpty(material.getListaMaterialCustoEmpresa())) {
					for(MaterialCustoEmpresa m : material.getListaMaterialCustoEmpresa()) {
						if(m.getCustoInicial() == null || m.getDtInicio() == null) {
							materialCustoEmpresaSemCustoInicial = true;
							break;
						}
					}
				}
				
//				if(material.getValorcustoinicial() == null || material.getValorcustoinicial() == 0){
//					materialSemCustoInicial = true;
//				}
				
				if(materialSemCustoInicial || materialCustoEmpresaSemCustoInicial) {
					request.addError("Para recalcular o custo do material " + material.getNome() +" � necess�rio primeiramente " + 
						"definir o custo inicial na aba material e na aba custo por empresa quando houver.");
					SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
					return;
				}
			}
		}
		
		materialService.recalcularCustoMedio(whereIn);
		materialService.criarHistoricoRecalculoCustoMedio(whereIn);
		request.addMessage("O valor de custo dos materiais selecionados foram atualizados");
		
		SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
	}
	
	public ModelAndView verificaLoteMaterial(WebRequestContext request, Material material){
		boolean existeLoteMaterial = false;
		if(material != null && material.getCdmaterial() != null){
			existeLoteMaterial = lotematerialService.existeLotematerial(material);				
		}
		return new JsonModelAndView().addObject("existeLoteMaterial", existeLoteMaterial);
	}
	
	public ModelAndView ajaxCalcularDvEan13(WebRequestContext request){
		String codigobarras = request.getParameter("codigobarras");
		Boolean erro = Boolean.TRUE;
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if (StringUtils.isNotBlank(codigobarras) && codigobarras.matches("^[0-9]+$")){
			StringBuilder codigobarrasComDV = new StringBuilder();
			String digitoVerificador = materialService.calculoDigitoVerfifcadorEAN13(codigobarras);
			if (StringUtils.isNotBlank(digitoVerificador)) {
				codigobarrasComDV.append(codigobarras).append(digitoVerificador);
				codigobarras = codigobarrasComDV.toString();
				erro = Boolean.FALSE;
			}
		}
		jsonModelAndView.addObject("erro", erro);
		jsonModelAndView.addObject("codigobarras", codigobarras);
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxPneumodelo(WebRequestContext request, Pneumodelo pneumodelo){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(pneumodelo.getPneumarca() != null){
			jsonModelAndView.addObject("listaPneumodelo", pneumodeloService.findBy(pneumodelo.getPneumarca()));
		}
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxHabilitacarcaca(WebRequestContext request, Material material){
		ModelAndView retorno = new JsonModelAndView();
		if(material.getCdmaterial() == null){
			retorno = new JsonModelAndView()
					.addObject("isGrupoColeta", Boolean.FALSE);
		}else{
			material = materialService.loadMaterialWithMaterialgrupo(material);
			retorno = new JsonModelAndView()
							.addObject("isGrupoColeta", material!=null && material.getMaterialgrupo()!=null && Boolean.TRUE.equals(material.getMaterialgrupo().getColeta()));
		}
		
		return retorno;
	}
	
	public ModelAndView ajaxHabilitaMedicamento(WebRequestContext request, Material material) {
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if (material != null && material.getMaterialgrupo() != null && material.getMaterialgrupo().getCdmaterialgrupo() != null) {
			Materialgrupo mg = materialgrupoService.load(material.getMaterialgrupo(), "materialgrupo.medicamento");
			jsonModelAndView.addObject("medicamento", mg.getMedicamento());
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxBuscaValorCusto(WebRequestContext request, MaterialFaixaMarkup bean){
		Double valorCusto = materialService.getValorCustoAtual(bean.getMaterial(), bean.getUnidadeMedida(), bean.getEmpresa());
		return new JsonModelAndView()
					.addObject("valorCusto", valorCusto);
	}
	
	public ModelAndView abrePopupHistoricoAlteracaoFaixaMarkup(WebRequestContext request, Materialhistorico materialHistorico){
		materialHistorico = materialhistoricoService.load(materialHistorico, "materialhistorico.cdmaterialhistorico, materialhistorico.observacaoFaixaMarkup");
		return new ModelAndView("direct:crud/popup/popupHistoricoAlteracaoFaixaMarkup", "bean", materialHistorico);
	}
	
	public ModelAndView selecionarHistoricoVendas(WebRequestContext request, Material material){
		MaterialHistoricoPrecoBean bean = null;//materialhistoricoService.load(materialHistorico, "materialhistorico.cdmaterialhistorico, materialhistorico.observacaoFaixaMarkup");
		return new ModelAndView("direct:crud/popup/popupHistoricoPrecos", "bean", bean);
	}
	
	public ModelAndView ajaxValidaExclusaoSerie(WebRequestContext request, Materialnumeroserie materialNumeroSerie){
		Boolean possuiMovimentacao = false;
		if(materialNumeroSerie != null && materialNumeroSerie.getCdmaterialnumeroserie() != null){
			possuiMovimentacao = SinedUtil.isListNotEmpty(movpatrimonioService.findBySerie(materialNumeroSerie));
		}
		return new JsonModelAndView().addObject("possuiMovimentacao", possuiMovimentacao);
	}
	
	public ModelAndView ajaxCalcularValorVendaForConversaoUnidade(WebRequestContext request, Material material){
		Double precoCusto = material.getValorcusto();//materialService.getValorCustoAtual(material, material.getEmpresatrans());
		if(precoCusto == null){
			precoCusto = 0d;
		}
		Material bean = materialService.loadMaterialWithUnidadeMedida(material);
		boolean isUnidadePrincipal = false;
		if(bean != null && bean.getUnidademedida() != null && bean.getUnidademedida().equals(material.getUnidademedida())){
			isUnidadePrincipal = true;
		}
		if(!isUnidadePrincipal){
			Materialunidademedida materialUnidadeMedida = materialunidademedidaService.getMaterialunidademedida(material, material.getUnidademedida());
			Double fracao = 1D;
			if(materialUnidadeMedida != null && DoubleUtils.isMaiorQueZero(materialUnidadeMedida.getFracaoQtdereferencia())){
				fracao = materialUnidadeMedida.getFracaoQtdereferencia();
			}
			precoCusto = precoCusto / fracao;
		}
		return new JsonModelAndView()
					.addObject("precoCusto", precoCusto);
	}
	
	public ModelAndView abrirConsultarEcommerce(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
			return null;
		}
		
		Material material = new Material();
		material.setWhereIn(whereIn);
		material.setValorVendaEcommerce(new Money());
		material.setValorCustoEcommerce(new Money());
		material.setQtdeEstoqueEcommerce(0.0d);
		//colocar a logica aqui!
		
		return new ModelAndView("direct:/crud/popup/consultaProdutoEcommerce", "bean", material);
	}
	
	public ModelAndView atualizarProdutoEcommerce(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");			
			SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
			return null;
		}
		
		List<Material> listaMateriais = materialService.loadWithIdEcommerce(whereIn);
		
		for (Material material : listaMateriais) {
			if(material.getIdEcommerce() == null){
				request.addError("Existem materiais ainda n�o cadastrados no E-commerce.");			
				SinedUtil.redirecionamento(request, "/suprimento/crud/Material");
				return null;
			}
		}
		
		for (Material material2 : listaMateriais) {
			
			MaterialEcomerceExcluido materialExcluido = new MaterialEcomerceExcluido();
			
			materialExcluido.setIdEcommerce(material2.getIdEcommerce());
			materialExcluido.setMaterial(material2);
			
			materialEcomerceExcluidoService.saveOrUpdate(materialExcluido);
			materialService.insertMaterialTabelaSincronizacaoEcommerce(material2);		
		}
		
		request.addMessage("O material ser� atualizado no E-commerce. Isso pode levar alguns minutos.");			
		SinedUtil.redirecionamento(request, "/suprimento/crud/Material?ACAO=consultar&cdmaterial=" + whereIn);
		return null;
	}
	
	public ModelAndView ajaxCarregaDadosMestreTrayCorp(WebRequestContext request, Material materialMestre){
		Material material = new Material();
		material.setMaterialmestregrade(materialMestre);
		materialService.setaAtributosMestreTrayCorp(material);
		
		return new JsonModelAndView()
					.addObject("idPaiExterno", material.getIdPaiExterno())
					.addObject("urlVideo", material.getUrlVideo())
					.addObject("marketplace", material.getMarketplace())
					.addObject("contraProposta", Boolean.TRUE.equals(material.getContraProposta()))
					.addObject("somenteParceiros", material.getSomenteParceiros())
					.addObject("exibirMatrizAtributos", material.getExibirMatrizAtributos() != null? material.getExibirMatrizAtributos().name(): "")
					.addObject("maisInfromacoes", material.getMaisInformacoes());
	}
	
	public ModelAndView ajaxGrupoIsGrade(WebRequestContext request, Materialgrupo materialGrupo){
		materialGrupo = materialgrupoService.load(materialGrupo, "materialgrupo.cdmaterialgrupo, materialgrupo.gradeestoquetipo");
		
		return new JsonModelAndView()
					.addObject("isUtilizaGrade", materialGrupo != null && materialGrupo.getGradeestoquetipo() != null);
	}
}
