package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraacao;
import br.com.linkcom.sined.geral.bean.Ordemcompraarquivo;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.bean.Ordemcomprahistorico;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialitem;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentohistorico;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ColaboradorFornecedorService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.EntregamaterialService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.OrdemcompraentregaService;
import br.com.linkcom.sined.geral.service.OrdemcomprahistoricoService;
import br.com.linkcom.sined.geral.service.OrdemcompramaterialService;
import br.com.linkcom.sined.geral.service.OrdemcompraorigemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.PessoaquestionarioService;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProducaoagendahistoricoService;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialService;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialitemService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.geral.service.VendaorcamentohistoricoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.OrdemcompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.SolicitarrestanteBean;
import br.com.linkcom.sined.util.DateUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/suprimento/crud/Ordemcompra", authorizationModule=CrudAuthorizationModule.class)
public class OrdemcompraCrud extends CrudControllerSined<OrdemcompraFiltro, Ordemcompra, Ordemcompra>{
	
	private OrdemcompraService ordemcompraService;
	private CentrocustoService centrocustoService;
	private EmpresaService empresaService;
	private PrazopagamentoService prazopagamentoService;
	private MaterialService materialService;
	private EntregamaterialService entregamaterialService;
	private OrdemcomprahistoricoService ordemcomprahistoricoService;
	private ContatoService contatoService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	private OrdemcompramaterialService ordemcompramaterialService;
	private ProjetoService projetoService;
	private DocumentotipoService documentotipoService;
	private ParametrogeralService parametrogeralService;
	private PedidovendaService pedidovendaService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private FornecedorService fornecedorService;
	private OrdemcompraorigemService ordemcompraorigemService;
	private ProdutoService produtoService;
	private ContagerencialService contagerencialService;
	private OrdemcompraentregaService ordemcompraentregaService;
	private ArquivoService arquivoService;
	private EntregaService entregaService;
	private VendaorcamentoService vendaorcamentoService;
	private VendaorcamentohistoricoService vendaorcamentohistoricoService;
	private ColaboradorFornecedorService colaboradorFornecedorService;
	private ProducaoagendamaterialitemService producaoagendamaterialitemService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private MaterialproducaoService materialproducaoService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private PessoaquestionarioService pessoaquestionarioService;
	private SolicitacaocompraService solicitacaocompraService;
	private UnidademedidaService unidademedidaService;
	private PlanejamentoService planejamentoService;
	private EnderecoService enderecoService;
	private DocumentoorigemService documentoorigemService;
	private MotivoavisoService motivoavisoService;
	private AvisoService avisoService;
	
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {this.solicitacaocompraService = solicitacaocompraService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setOrdemcompraentregaService(OrdemcompraentregaService ordemcompraentregaService) {this.ordemcompraentregaService = ordemcompraentregaService;}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {this.pedidovendahistoricoService = pedidovendahistoricoService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setOrdemcompramaterialService(OrdemcompramaterialService ordemcompramaterialService) {this.ordemcompramaterialService = ordemcompramaterialService;}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {this.materialgrupoService = materialgrupoService;}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {this.materialtipoService = materialtipoService;}
	public void setOrdemcomprahistoricoService(OrdemcomprahistoricoService ordemcomprahistoricoService) {this.ordemcomprahistoricoService = ordemcomprahistoricoService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {this.ordemcompraService = ordemcompraService;}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {this.entregamaterialService = entregamaterialService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {this.unidademedidaconversaoService = unidademedidaconversaoService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setOrdemcompraorigemService(OrdemcompraorigemService ordemcompraorigemService) {this.ordemcompraorigemService = ordemcompraorigemService;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setEntregaService(EntregaService entregaService) {this.entregaService = entregaService;}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {this.vendaorcamentoService = vendaorcamentoService;}
	public void setVendaorcamentohistoricoService(VendaorcamentohistoricoService vendaorcamentohistoricoService) {this.vendaorcamentohistoricoService = vendaorcamentohistoricoService;}
	public void setColaboradorFornecedorService (ColaboradorFornecedorService colaboradorFornecedorService){this.colaboradorFornecedorService = colaboradorFornecedorService;}
	public void setProducaoagendamaterialitemService(ProducaoagendamaterialitemService producaoagendamaterialitemService) {this.producaoagendamaterialitemService = producaoagendamaterialitemService;}
	public void setProducaoagendamaterialService(ProducaoagendamaterialService producaoagendamaterialService) {this.producaoagendamaterialService = producaoagendamaterialService;}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {this.materialproducaoService = materialproducaoService;}
	public void setProducaoagendahistoricoService(ProducaoagendahistoricoService producaoagendahistoricoService) {this.producaoagendahistoricoService = producaoagendahistoricoService;}
	public void setPessoaquestionarioService(PessoaquestionarioService pessoaquestionarioService) {this.pessoaquestionarioService = pessoaquestionarioService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {this.planejamentoService = planejamentoService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	@Override
	protected Ordemcompra criar(WebRequestContext request, Ordemcompra form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdordemcompra() != null){
			return ordemcompraService.criarCopia(form);
		}
		request.setAttribute("consultar", false);
		if("true".equals(request.getParameter("importarOrdemcompra"))){
			if(request.getSession().getAttribute("ORDEMCOMPRA_PROCESSAR_IMPORTARARQUIVOCSV") != null){
				Ordemcompra oc = (Ordemcompra) request.getSession().getAttribute("ORDEMCOMPRA_PROCESSAR_IMPORTARARQUIVOCSV");
//				request.getSession().removeAttribute("ORDEMCOMPRA_PROCESSAR_IMPORTARARQUIVOCSV");
				return oc;
			}
		}
		if("true".equals(request.getParameter("criarOrdemcompraSolicitarrestantes"))){
			String identificadorControle = request.getParameter("identificadorControle");
			Ordemcompra oc = (Ordemcompra) request.getSession().getAttribute("formOrdemcompraSolicitarrestantes" + identificadorControle);
			request.getSession().removeAttribute("formOrdemcompraSolicitarrestantes" + identificadorControle);
			return oc;
		}
		return super.criar(request, form);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, OrdemcompraFiltro filtro) throws CrudException {

		String param = request.getParameter("FROMENTREGACRIAR");
		if (param != null && param.equals("true")) {
			filtro.setFromentregacriar(Boolean.TRUE);		
		} else {
			filtro.setFromentregacriar(Boolean.FALSE);
		}
		
		return super.doListagem(request, filtro);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected ListagemResult<Ordemcompra> getLista(WebRequestContext request, OrdemcompraFiltro filtro) {

		ListagemResult<Ordemcompra> listagemResult = super.getLista(request, filtro);
		List<Ordemcompra> lista = listagemResult.list();
		
		Money total = new Money();
		
		for (Ordemcompra o : lista) {
			o.setListaMaterial(ordemcompramaterialService.findByOrdemcompra(o));
			o.setListaOrdemcompraentrega(SinedUtil.listToSet(ordemcompraentregaService.findByOrdemcompra(o), Ordemcompraentrega.class));
			total = total.add(o.getValorTotalOrdemCompraMoney());
		}
		
		filtro.setValorTotal(total);
		
		return listagemResult;
	}
	
	@Override
	protected void listagem(WebRequestContext request, OrdemcompraFiltro filtro) throws Exception {
		this.getRegistrosAtivo(request, null);
		
		List<Situacaosuprimentos> list = new ArrayList<Situacaosuprimentos>();
		if (filtro.getFromentregacriar() == null || !filtro.getFromentregacriar()) {
			list.add(Situacaosuprimentos.EM_ABERTO);
			list.add(Situacaosuprimentos.AUTORIZADA);
			if(new SinedUtil().needAprovacao()){
				list.add(Situacaosuprimentos.APROVADA);
			}
		}
		list.add(Situacaosuprimentos.PEDIDO_ENVIADO);
		list.add(Situacaosuprimentos.ENTREGA_PARCIAL);
		if (filtro.getFromentregacriar() == null || !filtro.getFromentregacriar()) {
			list.add(Situacaosuprimentos.BAIXADA);
			list.add(Situacaosuprimentos.CANCELADA);
			filtro.setFromentregacriar(Boolean.FALSE);
		}
		
		String modelo = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.LAYOUT_EMISSAO_ORDEMCOMPRA);
		
		request.setAttribute("listasituacoessolicitacaocompra", list);
		request.setAttribute("gruposAtivos", materialgrupoService.findAtivos(filtro != null ? filtro.getMaterialgrupo() : null));
		request.setAttribute("tiposAtivos", materialtipoService.findAtivos(filtro != null ? filtro.getMaterialtipo() : null));
		request.setAttribute("RELATORIO_CONFIGURAVEL", "configuravel".equalsIgnoreCase(modelo));
		
		super.listagem(request, filtro);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void salvar(WebRequestContext request, Ordemcompra bean) throws Exception {
		Boolean isCriar = bean.getCdordemcompra() != null ? Boolean.FALSE : Boolean.TRUE;
		Pessoaquestionario pessoaquestionario = null;
		
		if (isCriar) {
			if (request.getSession().getAttribute("pessoaquestionario"+bean.getIdentificadortela()) != null) {
				pessoaquestionario = (Pessoaquestionario) request.getSession().getAttribute("pessoaquestionario"+bean.getIdentificadortela());
				if (pessoaquestionario != null) {
					bean.setPessoaquestionario(pessoaquestionario);
					pessoaquestionarioService.saveOrUpdate(pessoaquestionario);
					ordemcompraService.vinculaPessoaQuestionarioAOrdemCompra(bean);
				}
			}
		}
		
		if (!isCriar) {
			Ordemcompra ordemcompra = ordemcompraService.findFornecedorForOrdemCompra(bean);
			if (bean.getFornecedor() != null
					&& bean.getFornecedor().getCdpessoa() != null 
					&& ordemcompra != null 
					&& ordemcompra.getFornecedor() != null 
					&& ordemcompra.getFornecedor().getCdpessoa() != null
					&& !bean.getFornecedor().getCdpessoa().equals(ordemcompra.getFornecedor().getCdpessoa())) {
				
				Ordemcompra ordemcompraAvaliacao = ordemcompraService.findOrdemcompraAvaliacao(bean);
				
				if (ordemcompraAvaliacao != null && ordemcompraAvaliacao.getCdordemcompra() != null && ordemcompraAvaliacao.getPessoaquestionario() != null && ordemcompraAvaliacao.getPessoaquestionario().getCdpessoaquestionario() != null) {
					ordemcompraService.deleteVinculoPessoaQuestionarioAOrdemCompra(bean);
					Pessoaquestionario pessoaquestionarioForDelete = new Pessoaquestionario();
					bean.setPessoaquestionario(null);
					pessoaquestionarioForDelete.setCdpessoaquestionario(ordemcompraAvaliacao.getPessoaquestionario().getCdpessoaquestionario());
					pessoaquestionarioService.delete(pessoaquestionarioForDelete);
				}
			}
		}
		
		Object attribute = request.getSession().getAttribute("listaOrdemcompramaterial" + bean.getIdentificadortela());
		if (attribute != null) {
			bean.setListaMaterial((List<Ordemcompramaterial>)attribute);
		}
		
		if(bean.getListaMaterial() == null || bean.getListaMaterial().size() == 0){
			request.addError("Houve um problema na hora de salvar os itens da ordem de compra. Favor refazer a criao ou edio da ordem de compra.");
			bean.setErroSalvar(Boolean.TRUE);
			return;
		}
		
		Integer cdpedidovenda = bean.getCdpedidovenda();
		String whereInSolicitacaocompra = null;
		String whereInPedidovenda = request.getParameter("whereInPedidovenda");
		String whereInCdvendaorcamento = request.getParameter("whereInCdvendaorcamento");
		String whereInProducaoagenda = bean.getWhereInProducaoagenda();
		String whereInOCSolicitarrestante = null;
		String whereInPlanejamento = bean.getWhereInPlanejamento();
		
		if(bean.getCdordemcompra() != null){
			//Carregar a verso anterior
			Ordemcompra ordemcompra = ordemcompraService.loadForEntrada(bean);
			bean.setOrdemcomprahistorico(new Ordemcomprahistorico(bean, Ordemcompraacao.ALTERADA, ordemcompra));
		} else{
			bean.setOrdemcomprahistorico(new Ordemcomprahistorico(bean, Ordemcompraacao.CRIADA));
			if(cdpedidovenda != null){
				bean.getOrdemcomprahistorico().setObservacao("Origem pedido de venda <a href=\"javascript:visualizarPedidovenda(" + cdpedidovenda + ")\">" + cdpedidovenda + "</a>.");
			}else if(bean.getCdordemcompra() == null && bean.getWhereInSolicitacaocompra() != null && !"".equals(bean.getWhereInSolicitacaocompra())){
				whereInSolicitacaocompra = bean.getWhereInSolicitacaocompra();
				bean.getOrdemcomprahistorico().setObservacao("Origem solicitao(es) de compra " + whereInSolicitacaocompra);
			} else if(whereInPedidovenda != null && !"<null>".equalsIgnoreCase(whereInPedidovenda)) {
				String cdpedido[] = whereInPedidovenda.split(",");
				String observacao = "";
				for (String string : cdpedido) {
					observacao += "Origem pedido de venda <a href=\"javascript:visualizarPedidovenda(" + string + ")\">" + string +"</a><br />";
				}
				bean.getOrdemcomprahistorico().setObservacao(observacao);
			} else if(whereInCdvendaorcamento != null && !"<null>".equalsIgnoreCase(whereInCdvendaorcamento)) {
				bean.getOrdemcomprahistorico().setObservacao("Origem oramento <a href=\"javascript:visualizarOrcamentovenda(" + whereInCdvendaorcamento + ")\">" + whereInCdvendaorcamento +"<a>");
			} else if(whereInProducaoagenda != null && !"<null>".equalsIgnoreCase(whereInProducaoagenda)) {
				String cdpedido[] = whereInProducaoagenda.split(",");
				StringBuilder observacao = new StringBuilder("");
				for (String string : cdpedido) {
					observacao.append("Origem agenda de produo <a href=\"javascript:visualizarProducaoagenda(" + string + ")\">" + string +"</a><br />");
				}
				bean.getOrdemcomprahistorico().setObservacao(observacao.toString());
			 }else if(bean.getCdordemcompra() == null && StringUtils.isNotEmpty(bean.getWhereInOCSolicitarrestante())) {
				whereInOCSolicitarrestante = bean.getWhereInOCSolicitarrestante();
			}
			Set<Ordemcompraarquivo> listaArquivosordemcompra = bean.getListaArquivosordemcompra();
			if(listaArquivosordemcompra != null && listaArquivosordemcompra.size() > 0){
				for (Ordemcompraarquivo ordemcompraarquivo : listaArquivosordemcompra) {
					if(ordemcompraarquivo.getArquivo() != null && ordemcompraarquivo.getArquivo().getCdarquivo() != null){
						Arquivo arquivocarregado = arquivoService.loadWithContents(ordemcompraarquivo.getArquivo());
						arquivocarregado.setCdarquivo(null);
						
						ordemcompraarquivo.setArquivo(arquivocarregado);
					}
				}
			}
		}
		
		if(bean.getObservacao() != null){
			bean.setObservacao(bean.getObservacao().trim());
		}
		
//		Integer situacao = bean.getCdordemcompra();
		
		super.salvar(request, bean);
		
		if(cdpedidovenda != null){
			Pedidovenda pedidovenda = new Pedidovenda(cdpedidovenda);
			pedidovendahistoricoService.createByOrdemcompra(pedidovenda, bean);
			pedidovendaService.updateOrdemcompra(pedidovenda);
			request.setAttribute("fromPedidovenda", Boolean.TRUE);
		}
		
		if(whereInSolicitacaocompra != null){
			ordemcompraorigemService.criaSalvaOrdemcompraorigemBySolicitacoes(bean, whereInSolicitacaocompra);
		}
		if(whereInPedidovenda != null && !"<null>".equalsIgnoreCase(whereInPedidovenda)) {
			salvaPedidovendaHistorico(bean, whereInPedidovenda);
		}
		if(whereInCdvendaorcamento != null && !"<null>".equalsIgnoreCase(whereInCdvendaorcamento)) {
			salvaVendaorcamentoHistorico(bean, whereInCdvendaorcamento);
		}
		if(StringUtils.isNotEmpty(whereInOCSolicitarrestante)){
			ordemcompraService.baixarEcriarHistoricoSolicitarrestante(bean, whereInOCSolicitarrestante);
		}
		if(whereInProducaoagenda != null && !"<null>".equalsIgnoreCase(whereInProducaoagenda)){
			salvaProducaoagendaHistorico(bean, whereInProducaoagenda);
		}
		if(StringUtils.isNotBlank(whereInPlanejamento)){
			ordemcompraorigemService.saveOrdemcompraorigemByPlanejamentorecursogeral(bean, whereInPlanejamento);
		}
		
//		CASO O PARMETRO EnvioEmailsOrdemDeCompra TENHA UM E-MAIL CADASTRADO E
//		O PARMETRO Aprovar Ordem de Compra ESTIVER COMO FALSE ENVIA E-MAIL.
//		if (situacao == null){
//			String email = parametrogeralService.getValorPorNome(Parametrogeral.ENVIAEMAILORDEMDECOMPRA);
//			String aprovar = parametrogeralService.getValorPorNome(Parametrogeral.APROVAR_ORDEM_DE_COMPRA);
//			
//			if (email != null && !email.equals("") && aprovar != null && aprovar.toUpperCase().equals("FALSE")){
//				enviarEmail(email, bean.getCdordemcompra());
//			}
//		}
		Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.ORDEMCOMPRA_CRIADA);
		if(motivoAviso != null){
			List<Aviso> avisoList = new ArrayList<Aviso>();
		
			avisoList.add(new Aviso("Ordem de compra criada", "Cdigo da ordem de compra: " + bean.getCdordemcompra(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
					motivoAviso.getUsuario(), AvisoOrigem.ORDEM_DE_COMPRA, bean.getCdordemcompra(), bean.getEmpresa(), null, motivoAviso));
			List<Usuario> usuarioList = avisoService.salvarAvisos(avisoList, true);
			
			StringBuilder notWhereInCdusuario = new StringBuilder();
			for (Usuario u : usuarioList) {
				if(!"".equals(notWhereInCdusuario.toString())) {
					notWhereInCdusuario.append(",");
				}
				notWhereInCdusuario.append(u.getCdpessoa());
			}
		
			if(isCriar && bean.getCdordemcompra() != null){
				if(Boolean.TRUE.equals(motivoAviso.getEmail())){
					try {
						ordemcompraService.enviarEmailOrdemcompra(bean.getCdordemcompra().toString(), Situacaosuprimentos.EM_ABERTO, notWhereInCdusuario.toString());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		if(bean.getRateio() != null && bean.getRateio().getListaRateioitem() != null && bean.getRateio().getListaRateioitem().size() > 0){
			String projetosPermissao = SinedUtil.getListaProjeto();
			if(projetosPermissao != null && !projetosPermissao.equals("")){
				boolean achou = false;
				Boolean rateioverificado = bean.getRateioverificado();
				
				List<String> listaProjetosPermissao = Arrays.asList(projetosPermissao.split(","));
				List<?> projetos = CollectionsUtil.getListProperty(bean.getRateio().getListaRateioitem(), "projeto.cdprojeto");
				
				for (Object id : projetos) {
					
					boolean boolAchou = id != null && listaProjetosPermissao.contains(id.toString());
					boolean boolNaoSalvo = (!rateioverificado || rateioverificado == null) && id == null;
					
					if(boolAchou || boolNaoSalvo){
						achou = true;
						break;
					}
				}
				
				if(!achou){
					bean.setNaoTemPermissao(Boolean.TRUE);
				}
			}
		}
		
//		if (isCriar){
//			request.getSession().setAttribute("ordemCompraSessaoCriar", null);
//		}
	}
	
	private void salvaVendaorcamentoHistorico(Ordemcompra bean, String whereInCdvendaorcamento) {
		for (String cdvendaorcamento : whereInCdvendaorcamento.split(",")) {
			Vendaorcamento vendaorcamento = new Vendaorcamento(Integer.parseInt(cdvendaorcamento));
			
			Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
			vendaorcamentohistorico.setObservacao("Ordem de compra gerada <a href=\"javascript:visualizarOrdemcompra(" + bean.getCdordemcompra() +")\">" +bean.getCdordemcompra() + "</a>");
			vendaorcamentohistorico.setVendaorcamento(vendaorcamento);
			vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);
		}
	}
	private void salvaPedidovendaHistorico(Ordemcompra bean, String whereInPedidovenda) {
		for (String cdpedidovenda : whereInPedidovenda.split(",")) {
			Pedidovenda pedidovenda = new Pedidovenda(Integer.parseInt(cdpedidovenda));
			
			Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
			pedidovendahistorico.setObservacao("Ordem de compra gerada <a href=\"javascript:visualizarOrdemcompra(" + bean.getCdordemcompra() + ")\">"+bean.getCdordemcompra() + "</a>");
			pedidovendahistorico.setPedidovenda(pedidovenda);
			pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		}
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Ordemcompra bean) {
		if(bean.getNaoTemPermissao() != null && bean.getNaoTemPermissao()){
			request.addMessage("Este usurio no tem permisso para visualizar a Ordem de compra salva.", MessageType.WARN);
			return sendRedirectToAction("listagem");
		}
		
		if(bean.getErroSalvar() != null && bean.getErroSalvar()){
			return sendRedirectToAction("listagem");
		}
		
		if(request.getAttribute("fromPedidovenda") != null && (Boolean)request.getAttribute("fromPedidovenda")){
		 	return new ModelAndView("redirect:/faturamento/crud/Pedidovenda");
		} else return super.getSalvarModelAndView(request, bean);
	}
	
	
	/** Mtodo que busca registros ativos que sero usados tanto 
	 *  na listagem quanto na entrada
	 * 
	 * @param request
	 * @author Toms Rabelo
	 * @param ordemcompra 
	 */
	private void getRegistrosAtivo(WebRequestContext request, Ordemcompra ordemcompra) {
//		request.setAttribute("centrosAtivos", centrocustoService.findAtivos(ordemcompra != null ? ordemcompra.getCentrocusto() : null));
		request.setAttribute("tiposDocumentos", documentotipoService.findDocumentoTipoOrdemCompra(ordemcompra != null ? ordemcompra.getDocumentotipo() : null));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void validateBean(Ordemcompra bean, BindException errors) {
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaOrdemcompramaterial" + bean.getIdentificadortela());
		if (attribute != null) {
			bean.setListaMaterial((List<Ordemcompramaterial>)attribute);
			
			for(Ordemcompramaterial ordemcompramaterial : bean.getListaMaterial()){
				if(ordemcompramaterial.getValor() == null){
					errors.reject("001", "O material "+ ordemcompramaterial.getMaterial().getNome()+ " est sem valor.");
				}
			}
		}
		
		if (!SinedUtil.isListNotEmpty(bean.getListaMaterial())) {
			errors.reject("001", "Deve haver pelo menos um produto na Ordem de compra.");
		}
		
		
//		List<Material> materiaisRepetidos = new ArrayList<Material>();
		//Verifica materiais iguais
//		for (Ordemcompramaterial ordemcompramaterial : bean.getListaMaterial()) {
//			int qtd = 0;
//			if(ordemcompramaterial.getMaterial() != null){
//				Material material = materialService.carregaMaterial(ordemcompramaterial.getMaterial());
//				if((material.getServico() == null || !material.getServico())){
//					for (Ordemcompramaterial ordemcompramaterial2 : bean.getListaMaterial())
//						if(ordemcompramaterial2.getMaterial() != null)
//							if (ordemcompramaterial.getMaterial().getCdmaterial().equals(ordemcompramaterial2.getMaterial().getCdmaterial()) &&
//								ordemcompramaterial.getContagerencial().getCdcontagerencial().equals(ordemcompramaterial2.getContagerencial().getCdcontagerencial()) &&
//								ordemcompramaterial.getFrequencia().equals(ordemcompramaterial2.getFrequencia()) &&
//								ordemcompramaterial.getQtdefrequencia().equals(ordemcompramaterial2.getQtdefrequencia())) {
//								qtd++;
//								if(qtd > 1){
//									if(!materiaisRepetidos.contains(ordemcompramaterial.getMaterial()))
//										materiaisRepetidos.add(ordemcompramaterial.getMaterial());
//									break;
//								}
//							}
//				}
//			}
//		}
//		
//		if(materiaisRepetidos != null && materiaisRepetidos.size() > 0){
//			List<Material> materiais = materialService.findMateriais(materiaisRepetidos);
//			for (Material material : materiais) {
//				errors.reject("001", "O material "+material.getNome()+" s pode aparecer uma vez.");
//			}
//		}
		
		if(!errors.hasErrors())
			if(bean.getRateioverificado() != null && bean.getRateioverificado() && 
			  (bean.getAux_ordemcompra().getSituacaosuprimentos() == null || bean.getAux_ordemcompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO)))
				ordemcompraService.validaRateio(bean, errors);
		
		if(bean.getFornecedor()!=null){
			try {
				colaboradorFornecedorService.validaPermissao(bean.getFornecedor());
			} catch (SinedException e) {
				errors.reject("001",e.getMessage());
			}
		}
		
		ordemcompraService.validaQtdeCompraMaiorQuePlanejamento(bean, errors);
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Ordemcompra form) throws CrudException {
		if(EDITAR.equals(request.getParameter("ACAO")) && form.getCdordemcompra() != null && !ordemcompraService.podeEditar(form)){
			throw new SinedException("Ordem de compra no pode ser editada.");
		}
		return super.doEditar(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Ordemcompra form) throws CrudException {
		if(form.getCdordemcompra() != null && !ordemcompraService.podeEditar(form)){
			request.clearMessages();
			request.addError("Ordem de compra no pode ser editada.");
			return new ModelAndView("redirect:/suprimento/crud/Ordemcompra?ACAO=consultar&cdordemcompra="+form.getCdordemcompra());
		}
		return super.doSalvar(request, form);
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Ordemcompra form) throws CrudException {
		if(form != null && form.getCdordemcompra() != null && !isEmpresaValida(form)){
			request.addError("No  possvel editar a ordem de compra #"+form.getCdordemcompra());
			return this.doListagem(request, new OrdemcompraFiltro());
		}
		return super.doEntrada(request, form);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Ordemcompra form)	throws Exception {
		if(form.getIdentificadortela() == null) {
			form.setIdentificadortela(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
		}
		if(form.getCdordemcompra() != null)	{
			if(!request.getBindException().hasErrors()){
				ordemcompraService.carregaListaSolicitacaocompraordemcompramaterial(form);
			}
			
			if(form.getFornecedor()!=null){
				colaboradorFornecedorService.validaPermissao(form.getFornecedor());
			}
			request.setAttribute("cdordemcompraParam", form.getCdordemcompra());
		}
		
		ordemcompraService.calculaQtdeRestanteOrdemcompra(form);
		
		if (parametrogeralService.getBoolean(Parametrogeral.ENVIAR_CODIGO_FORNECEDOR_MATERIAL_OC)) {
			ordemcompramaterialService.getCodigoAlternativo(form.getListaMaterial(), form.getFornecedor());
		}
		
		request.setAttribute("ENVIAR_CODIGO_FORNECEDOR_MATERIAL_OC", parametrogeralService.getBoolean(Parametrogeral.ENVIAR_CODIGO_FORNECEDOR_MATERIAL_OC));
		
		
		if (form.getPessoaquestionario() != null && form.getPessoaquestionario().getCdpessoaquestionario() != null) {
			Pessoaquestionario pessoaquestionario = pessoaquestionarioService.findPessoaQuestinario(form.getPessoaquestionario()); 
			form.setPessoaquestionario(pessoaquestionario);
		}
		if(form.getCdordemcompra() == null && request.getParameter("cdpedidovenda") != null && 
				!"".equals(request.getParameter("cdpedidovenda")) && !"<null>".equals(request.getParameter("cdpedidovenda"))){
			Integer cdpedidovenda = Integer.parseInt(request.getParameter("cdpedidovenda"));
			form.setCdpedidovenda(cdpedidovenda);
			
			Pedidovenda pedidovenda = pedidovendaService.loadForConfirmacao(new Pedidovenda(cdpedidovenda));
			
			Money desconto = new Money();
			for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
				if(pedidovendamaterial.getDesconto() != null){
					desconto = desconto.add(pedidovendamaterial.getDesconto());
				}
			}
			form.setDesconto(desconto);
			
			form.setListaMaterial(ordemcompramaterialService.getListaMaterialByPedidovenda(pedidovenda));
			
		}
		
		if(form.getCdordemcompra() == null && request.getParameter("gerarocbysolicitacao") != null && 
				"true".equals(request.getParameter("gerarocbysolicitacao"))){
			String whereInSolicitacoes = request.getParameter("selectedItensSolicitacao");
			if(whereInSolicitacoes != null && !"".equals(whereInSolicitacoes)){
				ordemcompraService.criaOrdemcompraBySolicitacao(form, whereInSolicitacoes);
			}
		}
		
		String paramPlanejamento = request.getParameter("planejamentos");
		if(form.getCdordemcompra() == null && StringUtils.isNotBlank(paramPlanejamento)){
			form = planejamentoService.gerarOrdemcompra(form, paramPlanejamento);
		}

		String idsproducaoagenda = request.getParameter("idsproducaoagenda");
		if(form.getCdordemcompra() == null && idsproducaoagenda != null && 
				!"".equals(idsproducaoagenda) && !"<null>".equals(idsproducaoagenda)){
			
			List<Ordemcompramaterial> listaOrdemcompramaterial = new ArrayList<Ordemcompramaterial>();
			List<Producaoagendamaterialitem> listaProducaoagendamaterialitem = producaoagendamaterialitemService.findByProducaoagenda(idsproducaoagenda);
			StringBuilder whereInProducaoagenda = new StringBuilder("");
			if(listaProducaoagendamaterialitem != null && listaProducaoagendamaterialitem.size() > 0){
				for (Producaoagendamaterialitem producaoagendamaterialitem : listaProducaoagendamaterialitem) {
					Material material = producaoagendamaterialitem.getMaterial();
					Double qtde = producaoagendamaterialitem.getQtde();
					
					Ordemcompramaterial ordemcompramaterial = new Ordemcompramaterial();
					ordemcompramaterial.setMaterial(material);					
					ordemcompramaterial.setQtdpedida(new Double(qtde));
					if(producaoagendamaterialitem.getProducaoagenda() != null && producaoagendamaterialitem.getProducaoagenda().getCdproducaoagenda() != null){
						if (!whereInProducaoagenda.toString().contains(producaoagendamaterialitem.getProducaoagenda().getCdproducaoagenda()+",")){
							whereInProducaoagenda.append(producaoagendamaterialitem.getProducaoagenda().getCdproducaoagenda()+",");	
						}
						
					}
					ordemcompraService.addMateriaisAgendaproducao(listaOrdemcompramaterial, ordemcompramaterial);

				}
			} else {
				List<Producaoagendamaterial> listaProducaoagendamaterial = producaoagendamaterialService.findByProducaoagenda(idsproducaoagenda, null);
				for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
					Material material = producaoagendamaterial.getMaterial();
					Double qtdeproduzir = producaoagendamaterial.getQtde();
					Double qtdereferencia = material.getQtdereferencia() != null ? material.getQtdereferencia() : 1d;
					
					if(SinedUtil.isListNotEmpty(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima())){
						for (Producaoagendamaterialmateriaprima materiaprima : producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()) {
							Ordemcompramaterial ordemcompramaterial = new Ordemcompramaterial();
							ordemcompramaterial.setMaterial(materiaprima.getMaterial());
							ordemcompramaterial.setQtdpedida(materiaprima.getQtdeprevista() / qtdereferencia);
							if(producaoagendamaterial.getProducaoagenda() != null && producaoagendamaterial.getProducaoagenda().getCdproducaoagenda() != null){
								if (!whereInProducaoagenda.toString().contains(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda()+",")){
									whereInProducaoagenda.append(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda()+",");
								}
							}
							ordemcompraService.addMateriaisAgendaproducao(listaOrdemcompramaterial, ordemcompramaterial);
						}
					}else {
					
						try{
							material.setProduto_altura(producaoagendamaterial.getAltura());
							material.setProduto_largura(producaoagendamaterial.getLargura());
							material.setQuantidade(qtdeproduzir != null ? qtdeproduzir.doubleValue() : 1.0);
							materialproducaoService.getValorvendaproducao(material);			
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						Material auxMaterial = materialService.loadMaterialComMaterialproducao(material);
						if(auxMaterial != null && auxMaterial.getListaProducao() != null){
							material = auxMaterial;
							material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
							
							for (Materialproducao materialproducao : material.getListaProducao()) {
								Ordemcompramaterial ordemcompramaterial = new Ordemcompramaterial();
								ordemcompramaterial.setMaterial(materialproducao.getMaterial());
								ordemcompramaterial.setQtdpedida(qtdeproduzir * (materialproducao.getConsumo() / qtdereferencia));
								if(producaoagendamaterial.getProducaoagenda() != null && producaoagendamaterial.getProducaoagenda().getCdproducaoagenda() != null){
									if (!whereInProducaoagenda.toString().contains(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda()+",")){
										whereInProducaoagenda.append(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda()+",");
									}
								}
								ordemcompraService.addMateriaisAgendaproducao(listaOrdemcompramaterial, ordemcompramaterial);
							}
						}
						else{
							Ordemcompramaterial ordemcompramaterial = new Ordemcompramaterial();
							ordemcompramaterial.setMaterial(material);
							ordemcompramaterial.setQtdpedida(material.getQuantidade());
							if(producaoagendamaterial.getProducaoagenda() != null && producaoagendamaterial.getProducaoagenda().getCdproducaoagenda() != null){
								if (!whereInProducaoagenda.toString().contains(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda()+",")){
									whereInProducaoagenda.append(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda()+",");
								}
							}
							ordemcompraService.addMateriaisAgendaproducao(listaOrdemcompramaterial, ordemcompramaterial);
						}
					}
				}
			}
			form.setWhereInProducaoagenda(whereInProducaoagenda.toString());
			form.setListaMaterial(listaOrdemcompramaterial);
		}
		
//		List<Ordemcompramaterial> lista = new ArrayList<Ordemcompramaterial>();
//		lista.addAll(form.getListaMaterial());
//		
//		Collections.sort(lista,new Comparator<Ordemcompramaterial>(){
//			public int compare(Ordemcompramaterial o1, Ordemcompramaterial o2) {
//				try{
//					Integer o1Id = Integer.parseInt(o1.getMaterial().getIdentificacao());
//					Integer o2Id = Integer.parseInt(o2.getMaterial().getIdentificacao());
//					return o1Id.compareTo(o2Id);
//				} catch (Exception e) {
//					return o1.getMaterial().getAutocompleteDescription().compareTo(o2.getMaterial().getAutocompleteDescription());
//				}
//			}
//		});
		
//		form.setListaMaterialTrans(lista);
		form.setListaMaterialTrans(form.getListaMaterial());
		
//		ordemCompraSessaoCriar(request, form);
		
		this.getRegistrosAtivo(request, form);
		//request.setAttribute("prazosAtivos", prazopagamentoService.findAtivos(form != null ? form.getPrazopagamento() : null));
		List<Prazopagamento> listaPrazopagamentoAtivos = prazopagamentoService.findForProcessoCompra();
		request.setAttribute("prazosAtivos", listaPrazopagamentoAtivos);
		request.setAttribute("centrosAtivos", centrocustoService.findAtivos(form.getRateio() != null ? form.getRateio().getCentrocusto() : null));
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
		
		if (form != null && form.getCdordemcompra() != null) {
			List<OrigemsuprimentosBean> listaBeanDestino = ordemcompraService.montaDestino(form);
			request.setAttribute("listaDestino", listaBeanDestino);
			
//			//Tenta calcular rateio
			if(request.getAttribute("rateiocalculado") == null || !"true".equals(request.getAttribute("rateiocalculado"))){
				ordemcompraService.calculaRateioAPartirDaCotacao(form);
			}else {
				request.setAttribute("rateiocalculado", null);
			}
			
			if(form.getAux_ordemcompra().getSituacaosuprimentos().equals(Situacaosuprimentos.PEDIDO_ENVIADO) || 
					form.getAux_ordemcompra().getSituacaosuprimentos().equals(Situacaosuprimentos.ENTREGA_PARCIAL) ||
					form.getAux_ordemcompra().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)){

					Double qtdeEntregue;
					for (Ordemcompramaterial ordemcompramaterial : form.getListaMaterialTrans()) {
						qtdeEntregue = entregamaterialService.getQtdEntregueDoMaterialNaOrdemCompra(ordemcompramaterial);
						if(ordemcompramaterial.getUnidademedida() != null && ordemcompramaterial.getMaterial() != null && 
								ordemcompramaterial.getMaterial().getUnidademedida() != null && 
								!ordemcompramaterial.getMaterial().getUnidademedida().equals(ordemcompramaterial.getUnidademedida())){
							Double fracao = unidademedidaService.getFatorconversao(ordemcompramaterial.getMaterial().getUnidademedida(), qtdeEntregue, ordemcompramaterial.getUnidademedida(), ordemcompramaterial.getMaterial(), null, 1d);
							if(fracao != null){
								qtdeEntregue = qtdeEntregue * fracao;
							}
						}
						ordemcompramaterial.setQtdrestante((ordemcompramaterial.getQtdpedida() * ordemcompramaterial.getQtdefrequencia()) - (qtdeEntregue != null ? qtdeEntregue : 0.0));
					}
			}
			
			Double valorunitario;
			Double qtdepedida;
			for (Ordemcompramaterial ordemcompramaterial : form.getListaMaterialTrans()) {
				qtdepedida = ordemcompramaterial.getQtdpedida() * ordemcompramaterial.getQtdefrequencia();
				valorunitario = qtdepedida > 0? ordemcompramaterial.getValor() / qtdepedida: 0;
				ordemcompramaterial.setValorunitario(SinedUtil.round(valorunitario, 10));
			}
			
			if(form.getContato() != null && form.getContato().getCdpessoa() != null){
				request.setAttribute("contatoid", form.getContato().getCdpessoa());
			}
		}
		
		Object attribute = request.getSession().getAttribute("listaOrdemcompramaterial" + form.getIdentificadortela());
		if (attribute != null) {
			form.setListaMaterialTrans((List<Ordemcompramaterial>)attribute);		
		}
		
		if(form.getListaMaterialTrans() == null) form.setListaMaterialTrans(new ArrayList<Ordemcompramaterial>());
		request.getSession().setAttribute("listaOrdemcompramaterial"  + form.getIdentificadortela(), form.getListaMaterialTrans());
		request.getSession().setAttribute("fornecedor"  + form.getIdentificadortela(), form.getFornecedor());
		
		String whereInPedidovenda = request.getParameter("whereInPedidovenda");
		if(form.getCdordemcompra() == null && whereInPedidovenda != null && 
				!"".equals(whereInPedidovenda) && !"<null>".equalsIgnoreCase(whereInPedidovenda)){
			form.setWhereInPedidovenda(whereInPedidovenda);
			List<Ordemcompramaterial> listaMateriais = new ArrayList<Ordemcompramaterial>();
			List<Pedidovenda> listaPedidovenda = pedidovendaService.findForOrdemcompra(form.getWhereInPedidovenda());
			Ordemcompramaterial ordemcompramaterial;			
			
			if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
				Money desconto = new Money();
				for(Pedidovenda pedidovenda : listaPedidovenda){
					for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
						if(pedidovendamaterial.getDesconto() != null){
							desconto = desconto.add(pedidovendamaterial.getDesconto());
						}
						ordemcompramaterial = new Ordemcompramaterial();
						ordemcompramaterial.setMaterial(pedidovendamaterial.getMaterial());
						ordemcompramaterial.setUnidademedida(pedidovendamaterial.getUnidademedida());
						ordemcompramaterial.setOrdemcompra(form);
						ordemcompramaterial.setCentrocusto(pedidovendamaterial.getMaterial().getCentrocustovenda());
						ordemcompramaterial.setContagerencial(pedidovendamaterial.getMaterial().getContagerencial());
						ordemcompramaterial.setQtdpedida(pedidovendamaterial.getQuantidade());
						ordemcompramaterial.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
						ordemcompramaterial.setDtentrega(pedidovendamaterial.getDtprazoentrega());
						ordemcompramaterial.setObservacao(pedidovendamaterial.getObservacao() != null && pedidovendamaterial.getObservacao().length() > 500?
															pedidovendamaterial.getObservacao().substring(0, 500):
																pedidovendamaterial.getObservacao());

						ordemcompramaterial.setFrequencia(new Frequencia(Frequencia.UNICA, "nica"));
						ordemcompramaterial.setQtdefrequencia(1d);
						ordemcompramaterial.setPedidovenda(pedidovenda);
						ordemcompramaterial.setIcmsissincluso(Boolean.FALSE);
						ordemcompramaterial.setIpiincluso(Boolean.FALSE);						
						listaMateriais.add(ordemcompramaterial);
					}
					if(form.getFornecedor() == null)
						form.setFornecedor(pedidovenda.getFornecedor());
					if(form.getEmpresa() == null)
						form.setEmpresa(pedidovenda.getEmpresa());
					if(form.getLocalarmazenagem() == null)
						form.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
					if(form.getListaMaterial() == null || form.getListaMaterial().isEmpty()) {
						form.setListaMaterialTrans(listaMateriais);
						if (attribute != null) {
							form.setListaMaterialTrans((List<Ordemcompramaterial>)attribute);		
						}
						form.setListaMaterial(listaMateriais);
						request.getSession().setAttribute("listaOrdemcompramaterial"  + form.getIdentificadortela(), form.getListaMaterialTrans());
						request.getSession().setAttribute("fornecedor"  + form.getIdentificadortela(), form.getFornecedor());
					}
					if(form.getContato() != null)
						form.setContato(pedidovenda.getContato());
				}
			}
		}
		
		String whereInCdvendaorcamento = request.getParameter("whereInCdvendaorcamento");
		if(form.getCdordemcompra() == null && whereInCdvendaorcamento != null && 
				!"".equals(whereInCdvendaorcamento) && !"<null>".equalsIgnoreCase(whereInCdvendaorcamento)){
			form.setWhereInCdvendaorcamento(whereInCdvendaorcamento);
			List<Ordemcompramaterial> listaMateriais = new ArrayList<Ordemcompramaterial>();
			List<Vendaorcamento> listaVendaorcamento = vendaorcamentoService.findForOrdemcompra(form.getWhereInCdvendaorcamento());
			Ordemcompramaterial ordemcompramaterial;			
			
			if(listaVendaorcamento != null && !listaVendaorcamento.isEmpty()){
				Money desconto = new Money();
				for(Vendaorcamento vendaorcamento : listaVendaorcamento){
					for (Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()) {
						if(vendaorcamentomaterial.getDesconto() != null){
							desconto = desconto.add(vendaorcamentomaterial.getDesconto());
						}
						ordemcompramaterial = new Ordemcompramaterial();
						ordemcompramaterial.setMaterial(vendaorcamentomaterial.getMaterial());
						ordemcompramaterial.setUnidademedida(vendaorcamentomaterial.getUnidademedida());
						ordemcompramaterial.setOrdemcompra(form);
						ordemcompramaterial.setCentrocusto(vendaorcamentomaterial.getMaterial().getCentrocustovenda());
						ordemcompramaterial.setContagerencial(vendaorcamentomaterial.getMaterial().getContagerencial());
						ordemcompramaterial.setQtdpedida(vendaorcamentomaterial.getQuantidade());
//						ordemcompramaterial.setValorunitario(vendaorcamentomaterial.getValorvendaRelatorio());
						ordemcompramaterial.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
						ordemcompramaterial.setObservacao(vendaorcamentomaterial.getObservacao() != null && vendaorcamentomaterial.getObservacao().length() > 500?
															vendaorcamentomaterial.getObservacao().substring(0, 500):
																vendaorcamentomaterial.getObservacao());
						ordemcompramaterial.setFrequencia(new Frequencia(Frequencia.UNICA, "nica"));
						ordemcompramaterial.setQtdefrequencia(1d);
//						ordemcompramaterial.setDtentrega(vendaorcamentomaterial.getDtprazoentrega());
						ordemcompramaterial.setVendaorcamento(vendaorcamento);
						ordemcompramaterial.setIcmsissincluso(Boolean.FALSE);
						ordemcompramaterial.setIpiincluso(Boolean.FALSE);
						listaMateriais.add(ordemcompramaterial);
					}
					if(form.getFornecedor() == null)
						form.setFornecedor(vendaorcamento.getFornecedor());
					if(form.getEmpresa() == null)
						form.setEmpresa(vendaorcamento.getEmpresa());
					if(form.getLocalarmazenagem() == null)
						form.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
					if(form.getListaMaterial() == null || form.getListaMaterial().isEmpty()) {
						form.setListaMaterialTrans(listaMateriais);
						if (attribute != null) {
							form.setListaMaterialTrans((List<Ordemcompramaterial>)attribute);		
						}
						form.setListaMaterial(listaMateriais);
						request.getSession().setAttribute("listaOrdemcompramaterial"  + form.getIdentificadortela(), form.getListaMaterialTrans());
						request.getSession().setAttribute("fornecedor"  + form.getIdentificadortela(), form.getFornecedor());
					}
					if(form.getContato() != null)
						form.setContato(vendaorcamento.getContato());
				}
			}
		}
		
		List<Projeto> projeto = new ArrayList<Projeto>();
		if(form.getRateio() != null && form.getRateio().getListaRateioitem() != null){
			for(Rateioitem rateioitem : form.getRateio().getListaRateioitem()){
				if(rateioitem.getProjeto() != null && rateioitem.getProjeto().getCdprojeto() != null){
					projeto.add(rateioitem.getProjeto());
				}
			}
		}
		List<Projeto> listaProjeto = projetoService.findProjetosAbertos(projeto, true, null);
		request.setAttribute("listaProjeto", listaProjeto);

		List<Contato> listaContato = new ArrayList<Contato>(); 
		if (form.getFornecedor() != null && form.getFornecedor().getCdpessoa() != null){
			listaContato = contatoService.findByPessoaCombo(form.getFornecedor(), Boolean.TRUE);
		}
		request.setAttribute("listaContato", listaContato);
		request.setAttribute("listaOrigem", ordemcompraService.montaOrigem(form));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
//		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		empresaService.adicionarEmpresaSessaoForFluxocompra(request, form.getEmpresa(), "ORDEMCOMPRA");
		
		request.setAttribute("PERMISSAO_PROJETO", SinedUtil.getListaProjeto());
		request.setAttribute("inspecaoordemcompra", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.INSPECAOORDEMCOMPRA)));
		request.setAttribute("ignoreHackAndroidDownload", true);
		request.setAttribute("whereInPedidovenda", whereInPedidovenda);
		request.setAttribute("whereInCdvendaorcamento", whereInCdvendaorcamento);
		
		String modelo = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.LAYOUT_EMISSAO_ORDEMCOMPRA);

		request.setAttribute("RELATORIO_CONFIGURAVEL", "configuravel".equalsIgnoreCase(modelo));
		request.setAttribute("enderecoentrega", Arrays.asList(new Endereco[]{form.getEnderecoentrega() != null ? enderecoService.loadEndereco(form.getEnderecoentrega()) : null}));
	}
	
	/**
	 * Mtodo com referncia no Service
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#geraOrdemCompra(Cotacao)
	 * @see br.com.linkcom.sined.geral.service.PapelService#carregaPapeisPermissaoAcao(String)
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#salvaOrdensCompra(Set, List)
	 * @param request
	 * @param form
	 * @return
	 * @throws Exception
	 * @author Toms T. Rabelo
	 */
	public ModelAndView gerarOrdemCompra(WebRequestContext request, Cotacao cotacao) throws Exception {
		String ids = null;
		try{
		 ids = ordemcompraService.gerarOrdemCompra(cotacao);
		} catch (SinedException e) {
			request.clearMessages();
			request.addError(e.getMessage());
			return new ModelAndView("redirect:/suprimento/crud/Cotacao");
		}
		request.clearMessages();
		request.addMessage("Ordem(ns) de compra(s) "+ ids +" gerada(s) com sucesso.", MessageType.INFO);
		
		if(ids != null && !"".equals(ids)) {
			for (String id : ids.split(",")) {
				Ordemcompra ordemCompra = ordemcompraService.load(new Ordemcompra(Integer.parseInt(id)));
				Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.ORDEMCOMPRA_CRIADA);
				if(motivoAviso != null){
					List<Aviso> avisoList = new ArrayList<Aviso>();
					
					avisoList.add(new Aviso("Ordem de compra criada", "Cdigo da ordem de compra: " + ordemCompra.getCdordemcompra(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
							motivoAviso.getUsuario(), AvisoOrigem.ORDEM_DE_COMPRA, ordemCompra.getCdordemcompra(), ordemCompra.getEmpresa(), null, motivoAviso));
					List<Usuario> usuarioList = avisoService.salvarAvisos(avisoList, true);
					
					StringBuilder notWhereInCdusuario = new StringBuilder();
					for (Usuario u : usuarioList) {
						if(!"".equals(notWhereInCdusuario.toString())) {
							notWhereInCdusuario.append(",");
						}
						notWhereInCdusuario.append(u.getCdpessoa());
					}
				
					if(Boolean.TRUE.equals(motivoAviso.getEmail())){
						try {
							ordemcompraService.enviarEmailOrdemcompra(ordemCompra.getCdordemcompra().toString(), Situacaosuprimentos.EM_ABERTO, notWhereInCdusuario.toString());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		List<Ordemcompra> listaOrdemCompra = ordemcompraService.findForEdicaoCotacao(cotacao);
		List<Material> listaMateriaisOrdemcompra = new ArrayList<Material>();
		if(listaOrdemCompra != null && !listaOrdemCompra.isEmpty()){
			for(Ordemcompra ordemcompra : listaOrdemCompra){
				if(ordemcompra.getListaMaterial() != null && !ordemcompra.getListaMaterial().isEmpty()){
					for(Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()){
						if(ordemcompramaterial.getMaterial() != null && !listaMateriaisOrdemcompra.contains(ordemcompramaterial.getMaterial())){
								listaMateriaisOrdemcompra.add(ordemcompramaterial.getMaterial());
						}
					}
				}
			}
		}
		String whereInCdSolicitacao = "";
		String whereInCdMaterial = "";
		if(listaMateriaisOrdemcompra != null && !listaMateriaisOrdemcompra.isEmpty()){
			List<Solicitacaocompra> listaSolicitacao = solicitacaocompraService.findByCotacao(cotacao); 
			if(listaSolicitacao != null && !listaSolicitacao.isEmpty()){
				for(Solicitacaocompra solicitacao : listaSolicitacao){
					if(solicitacao.getMaterial() != null){
						if(!listaMateriaisOrdemcompra.contains(solicitacao.getMaterial())){
							whereInCdSolicitacao += solicitacao.getCdsolicitacaocompra().toString() + ",";
							whereInCdMaterial += solicitacao.getMaterial().getCdmaterial().toString() + ",";
						}
					}
				}
			}
		}
		if(!"".equals(whereInCdSolicitacao) && !"".equals(whereInCdMaterial)){
			whereInCdSolicitacao = whereInCdSolicitacao.substring(0, whereInCdSolicitacao.length() - 1);
			whereInCdMaterial = whereInCdMaterial.substring(0, whereInCdMaterial.length() - 1);
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println(
					"<html><body>" +
					"<script>if(confirm('Existem materiais na cotao que no geraram Ordem de Compra. Deseja retira-los desta cotao para realizar nova cotao?')){ " +
					" parent.location = '" + request.getServletRequest().getContextPath() + "/suprimento/crud/Cotacao?ACAO=atualizaSituacaoSolicitacaoCompra&cdcotacao="+ cotacao.getCdcotacao() + 
					"&whereInCdSolicitacao=" + whereInCdSolicitacao + "&whereInCdMaterial=" + whereInCdMaterial + "&fromOrdemCompra=true';" +
					"}else{" +
					" parent.location = '" + request.getServletRequest().getContextPath() + "/suprimento/crud/Cotacao?ACAO=consultar&cdcotacao="+ cotacao.getCdcotacao() + "';" +
					"}</script>" +
					"</body></html>"
					);
			return null;
			
		}
		
		return new ModelAndView("redirect:/suprimento/crud/Cotacao?ACAO=consultar&cdcotacao="+cotacao.getCdcotacao());
	}
	
	/**
	 * Mtodo Ajax que verifica se o material pertence  classe servio.
	 * Serve para desabilitar campos no JSP
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialService#load(br.com.linkcom.sined.geral.bean.Material)
	 * @see br.com.linkcom.sined.geral.service.EntregamaterialService#getQtdEntregueDoMaterialNaOrdemCompra(Ordemcompra)
	 * @param request
	 * @param material
	 * @autor Toms Rabelo
	 */
	public void isMaterialServicoAjax(WebRequestContext request, Ordemcompramaterial ordemcompra){
//		Boolean result = materialclasseService.isMaterialServico(material);
		Boolean result = materialService.load(ordemcompra.getMaterial(), "material.servico").getServico();
		String script = "var isServico = ";
		script +=result == null ? Boolean.FALSE.toString() : result.toString()+";";
		
		//Caso a ordem de compra tenha sido salva, verifica se houve alguma entrega para o material.
		Double qtdEntregue = 0.0;
		if(ordemcompra.getCdordemcompramaterial() != null && ordemcompra.getVerificarQtderestante()){
			qtdEntregue = entregamaterialService.getQtdEntregueDoMaterialNaOrdemCompra(ordemcompra);
			script += "var qtdEntregue = "+qtdEntregue+";";
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(script);
//		View.getCurrent().eval(result == null ? Boolean.FALSE.toString() : result.toString());
	}
	
	
	/**
	 * Mtodo que gera entrega para a ordem de compra
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public ModelAndView gerarEntrega(WebRequestContext request, Ordemcompra bean) throws Exception {
		salvar(request,bean);	
		return new ModelAndView("forward:/suprimento/crud/Entrega?ACAO=gerarEntrega", "ordemcompra", bean);
	}
	
	/**
	 * Mtodo calcula rateio quando o usurio clica em calcular rateio. Obs: s  possivel se a
	 * ordem de compra for gerada manualmente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#calculaRateio(Ordemcompra)
	 * @param bean
	 * @author Toms Rabelo
	 */
	@SuppressWarnings("unchecked")
	@Command(validate=false)
	public ModelAndView calculaRateio(WebRequestContext request, Ordemcompra bean) throws CrudException{
		Object attribute = request.getSession().getAttribute("listaOrdemcompramaterial" + bean.getIdentificadortela());
		if (attribute != null) {
			bean.setListaMaterial((List<Ordemcompramaterial>)attribute);
		}
		
		if(bean.getListaMaterial() != null && bean.getListaMaterial().size() > 0)
			ordemcompraService.calculaRateio(bean);
		
		List<Ordemcomprahistorico> listaOrdemcomprahistorico = bean.getListaOrdemcomprahistorico();
		if(listaOrdemcomprahistorico != null && listaOrdemcomprahistorico.size() > 0){
			for (Ordemcomprahistorico historico : listaOrdemcomprahistorico) {
				if(historico.getOrdemcompraacao() != null){
					historico.setOrdemcompraacao(Ordemcompraacao.getDescricaoAcao(historico.getOrdemcompraacao().getCdordemcompraacao()));
				}
			}
		}
		
		request.setAttribute("rateiocalculado", "true");
		if(bean.getFornecedor() != null && bean.getFornecedor().getCdpessoa() != null){
			if(bean.getFornecedor().getNome() == null || "".equals(bean.getFornecedor().getNome())){
				bean.setFornecedor(fornecedorService.load(bean.getFornecedor(), "fornecedor.cdpessoa, fornecedor.nome"));
			}
			if(bean.getEnderecoentrega() != null && bean.getEnderecoentrega().getCdendereco() != null){
				bean.setEnderecoentrega(enderecoService.loadEndereco(bean.getEnderecoentrega()));
			}
		}
		
//		bean.setVerificarOrdemCompraCriar(false);
		
		return continueOnAction("entrada", bean);
	}
	
	/**
	 * Mtodo que visualiza ordem de compra no histrico
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcomprahistoricoService#findForVisualizacao(Ordemcomprahistorico)
	 * @param request
	 * @param ordemcomprahistorico
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Action("visualizaOrdem")
	public ModelAndView visualizaOrdem(WebRequestContext request, Ordemcomprahistorico ordemcomprahistorico){
		if(ordemcomprahistorico == null)
			throw new SinedException("No foi possvel encontrar a ordem de compra em nosso sistema.");

		request.setAttribute("TEMPLATE_beanName", ordemcomprahistorico);
		request.setAttribute("TEMPLATE_beanClass", Ordemcomprahistorico.class);
		request.setAttribute("descricao", "Ordem de compra");
		request.setAttribute("consultar", true);

		ordemcomprahistorico = ordemcomprahistoricoService.findForVisualizacao(ordemcomprahistorico);

		return new ModelAndView("direct:crud/popup/visualizacaoOrdem").addObject("ordemcomprahistorico", ordemcomprahistorico);
	}
	
	/**
	 * Popula o combo de box de contato, via AJAX.
	 * 
	 * @param request
	 * @return
	 * @author Toms Rabelo
	 */
	public ModelAndView comboBox(WebRequestContext request, Cotacaofornecedor cotacaofornecedor) {
		Fornecedor fornecedor = cotacaofornecedor.getFornecedor();
		List<Contato> listaContato = contatoService.findByPessoaCombo(fornecedor, Boolean.TRUE);
		String listaCodigos = CollectionsUtil.listAndConcatenate(listaContato, "cdpessoa",",");
		String listaNomes = CollectionsUtil.listAndConcatenate(listaContato, "nome",",");
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("codigos", listaCodigos);
		json.addObject("nomes", listaNomes);
		return json;
	}
	
	/**
	 * Mtodo que carrega o email de um determinado contato
	 * 
	 * @param request
	 * @param contato
	 * @return
	 * @author Toms Rabelo
	 */
	public void atualizaEmailContato(WebRequestContext request, Contato contato) {
		contato = contatoService.getEmailContato(contato);
		String emailcontato = "var emailcontato = ";
		if(contato != null){
			emailcontato += contato.getEmailcontato() == null ? "'';" : "'"+contato.getEmailcontato()+"';";
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(emailcontato);
	}
	
	public ModelAndView getInfoMaterial(WebRequestContext request, Solicitacaocompra bean){
		Contagerencial contagerencial = null;
		String labelContagerencial = "";
		Material material = materialService.loadWithContagerencial(bean.getMaterial());
		if(material != null && material.getContagerencial() != null){
			contagerencial = material.getContagerencial();
			if(contagerencial.getVcontagerencial() != null && contagerencial.getVcontagerencial().getIdentificador() != null){
				labelContagerencial = contagerencial.getVcontagerencial().getIdentificador() + " - ";
			}
			labelContagerencial += contagerencial.getDescricaoCombo();
		}
		Boolean isLocacao = materialService.isLocacaoByMaterial(bean.getMaterial());
		
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		Material unidadeMaterial = new Material();
		unidadeMaterial = materialService.unidadeMedidaMaterial(bean.getMaterial());
		
		listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(unidadeMaterial.getUnidademedida(), unidadeMaterial);
		
		Unidademedida unidademedidaPrioritaria = null;
		for (Unidademedida unidademedida : listaUnidademedida) {
			if(unidademedida.getPrioritariacompra() != null && unidademedida.getPrioritariacompra()){
				unidademedidaPrioritaria = unidademedida;
			}
		}
		
		List<Materialclasse> listaClasse = materialService.findClasses(bean.getMaterial());
		Integer qtdeminima = 0;
		if(listaClasse != null && !listaClasse.isEmpty()){
			for(Materialclasse materialclasse : listaClasse){
				if(Materialclasse.PRODUTO.equals(materialclasse)){
					Produto produto = produtoService.carregaProduto(bean.getMaterial());
					if(produto != null && produto.getQtdeminima() != null){
						qtdeminima = produto.getQtdeminima();
						
					}
					break;
				}
			}
		}
		
		return new JsonModelAndView().addObject("islocacao", isLocacao)
									 .addObject("unidadeMedidaSelecionada", unidademedidaPrioritaria != null ? unidademedidaPrioritaria : unidadeMaterial.getUnidademedida())
									 .addObject("listaUnidademedida", listaUnidademedida)
									 .addObject("cdcontagerencial", contagerencial != null ? contagerencial.getCdcontagerencial() : "")
									 .addObject("labelContagerencial", labelContagerencial)
									 .addObject("qtdeminima", qtdeminima);
	}
	
	public ModelAndView comboBoxContato(WebRequestContext request, Fornecedor fornecedor) {
		if (fornecedor == null || fornecedor.getCdpessoa() == null) {
			throw new SinedException("Fornecedor no pode ser nulo.");
		}
		List<Contato> lista = contatoService.findByPessoaCombo(fornecedor, Boolean.TRUE);
		
		return new JsonModelAndView().addObject("lista", lista);
	}
	
//	/**Mtodo que  chamado para o envio de e-mail.
//	 * @author Thiago Augusto
//	 * @param mensagem
//	 */
//	private void enviarEmail(String enderecoEmail, Integer cdOrdemCompra) {
//		String[] emails = enderecoEmail.split(";");
//		List<String> listTo = new ArrayList<String>();
//		String string;
//		
//		HttpServletRequest servletRequest = NeoWeb.getRequestContext().getServletRequest();
//		String url = servletRequest.getRequestURL().toString();
//		String[] urlDividida = url.split("/");
//		String servidor = urlDividida[2];
//		
//		TemplateManager templateAux = null;
//		String template = null;
//		try{
//			templateAux = new TemplateManager("/WEB-INF/template/templateEnvioEmailIncluirOrdemCompra.tpl");
//			template = templateAux.assign("cdOrdemCompra", cdOrdemCompra.toString())
//									.assign("link", servidor)
//									.getTemplate();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		for (int i = 0; i < emails.length; i++) {
//			string = emails[i];
//			if(string != null && !string.trim().equals("")){
//				listTo.add(string.trim());
//			}
//		}
//
//		try {
//			EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
//			
//			email
//				.setFrom("w3erp@w3erp.com.br")
//				.setListTo(listTo)
//				.setSubject("Assunto: Autorizao da OC "+cdOrdemCompra)
//				.addHtmlText(template)
//				.sendMessage();
//		} catch (Exception e) {
//			throw new SinedException("Erro ao enviar e-mail.", e);
//		}
//	}
	
	/**
	 * Mtodo que verifica se o usurio tem permisso para visualizar determinada ordem de compra de acordo com a empresa selecionada.
	 * @param ordemcompra
	 * @return
	 */
	protected Boolean isEmpresaValida(Ordemcompra ordemcompra){
		if(ordemcompra.getEmpresa()!=null && !empresaService.findByUsuario().contains(ordemcompra.getEmpresa())){
			return false;
		}
		return true;
	}
	
	/**
	 * Mtodo que armazena a dtentrega modificada na lista da sesso.
	 * @param request
	 * @author Rafael Salvio
	 */
	@SuppressWarnings("unchecked")
	public void saveDtentregaListaSessao(WebRequestContext request){
		String dateString = request.getParameter("dtentrega");
		Integer cdordemcompramaterial = null;
		if(request.getParameter("cdordemcompramaterial") != null && !"".equals(request.getParameter("cdordemcompramaterial"))){
			cdordemcompramaterial = Integer.parseInt(request.getParameter("cdordemcompramaterial"));
		}
		Integer index = null;
		if(request.getParameter("index") != null && !"".equals(request.getParameter("index"))){
			index = Integer.parseInt(request.getParameter("index"));
		}
		String identificadortela = request.getParameter("identificadortela");
		if(dateString != null && !"".equals(dateString) && (cdordemcompramaterial != null || index != null)){
			Date dtentrega = DateUtils.stringToDate(dateString);
			Object attribute = request.getSession().getAttribute("listaOrdemcompramaterial" + (identificadortela != null ? identificadortela : ""));
			if (attribute != null) {
				List<Ordemcompramaterial> listaItens = (List<Ordemcompramaterial>) attribute;
				if(cdordemcompramaterial != null){
					for(Ordemcompramaterial ordemcompramaterial : listaItens){
							if(ordemcompramaterial.getCdordemcompramaterial() != null && 
									ordemcompramaterial.getCdordemcompramaterial().equals(cdordemcompramaterial)){
								ordemcompramaterial.setDtentrega(dtentrega);
							}
					}
				}else {
					listaItens.get(index).setDtentrega(dtentrega);
				}
				Set<Ordemcompramaterial> lista = SinedUtil.listToSet(listaItens, Ordemcompramaterial.class);
				request.getSession().setAttribute("listaOrdemcompramaterial"  + (identificadortela != null ? identificadortela : ""), lista);
			}
			View.getCurrent().println("var erroDtentrega = 'false';");
		}
		else{
			View.getCurrent().println("var erroDtentrega = 'true';");
		}
	}
	
	/**
	 * Mtodo ajax que busca o ltimo valor de compra
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaValorAnterior(WebRequestContext request){
		Integer cdmaterial = Integer.parseInt(request.getParameter("cdmaterial"));
		Integer cdfornecedor = Integer.parseInt(request.getParameter("cdfornecedor"));
		
		Double valor = ordemcompramaterialService.getUltimoValor(cdmaterial, cdfornecedor);
		
		if(valor != null){
			View.getCurrent().println("var sucesso = true;");
			View.getCurrent().println("var valorunitario = '" + valor.toString().replaceAll("\\.", ",") + "'");
		} else {
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	public ModelAndView abrirPopupUltimascompras(WebRequestContext request){
		return entregaService.abrirPopupUltimascompras(request);	
	}
	
	/**
	 * Mtodo ajax para buscar email/telefones do fornecedor
	 *
	 * @param request
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxInfoFornecedor(WebRequestContext request, Ordemcompra ordemcompra){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(ordemcompra.getFornecedor() != null && ordemcompra.getFornecedor().getCdpessoa() != null){
			Fornecedor fornecedor = fornecedorService.loadForOrdemcompraInfo(ordemcompra.getFornecedor());
				
			String telefones = fornecedor.getTelefonesHtml();
			String categorias = fornecedor.getCategorias();
			String email = fornecedor.getEmail();
			String cpfcnpj = fornecedor.getCpfOuCnpj();						
			
			jsonModelAndView.addObject("telefones", telefones);
			jsonModelAndView.addObject("categorias", categorias);
			jsonModelAndView.addObject("email", email);
			jsonModelAndView.addObject("cpfcnpj", cpfcnpj );
			
		}
		return jsonModelAndView;
	}
	
	/**
	 * Mtodo ajax para buscar email/telefones do contato do fornecedor
	 *
	 * @param request
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxInfoContatoFornecedor(WebRequestContext request, Ordemcompra ordemcompra){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(ordemcompra.getContato() != null && ordemcompra.getContato().getCdpessoa() != null){
			Contato contato = contatoService.loadForOrdemcompraInfo(ordemcompra.getContato());
			
			String telefones = contato.getTelefonesHtml();
			String categorias = contato.getCategorias();
			String email = contato.getEmailcontato();
			
			jsonModelAndView.addObject("telefones", telefones);
			jsonModelAndView.addObject("categorias", categorias);
			jsonModelAndView.addObject("email", email);
		}
		return jsonModelAndView;
	}
	
	/**
	 * 	 * Mtodo ajax para adiconar a empresa na sesso (fluxo compra)
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void colocarEmpresaSessao(WebRequestContext request){
		empresaService.adicionarEmpresaSessaoForFluxocompra(request, null, "ORDEMCOMPRA");
	}
	
	/**
	 * Mtodo que abre popup para escolha do fornecedor e arquivo para importar OC
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopUpImportarOC(WebRequestContext request){
		return new ModelAndView("direct:/process/popup/importarOrdemcompra");
	}
	
	/**
	 * Mtodo que importa a ordem de compra
	 *
	 * @param request
	 * @param ordemcompra
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView importarOrdemcompraArquivo(WebRequestContext request, Ordemcompra ordemcompra) {
		if(ordemcompra.getArquivoimportacao() == null){
			request.addError("Arquivo no informado para a importao da ordem de compra.");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
			return null;
		}
		Ordemcompra oc = ordemcompraService.criaOrdemcompraByImportacao(ordemcompra.getArquivoimportacao(), ordemcompra.getFornecedor());
		
		if(oc.getListaErros() != null && !oc.getListaErros().isEmpty()){
			request.addError(oc.getListaErros());
		}
		
		if(oc.getListaMaterial() != null && !oc.getListaMaterial().isEmpty()){
			request.getSession().setAttribute("ORDEMCOMPRA_PROCESSAR_IMPORTARARQUIVOCSV", oc);
			SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra?ACAO=criar&importarOrdemcompra=true");
		}else {
			request.getSession().removeAttribute("ORDEMCOMPRA_PROCESSAR_IMPORTARARQUIVOCSV");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
		}
		return null;
	}
	
	/**
	 * Mtodo que gera a antecipao da ordem de compra
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	public ModelAndView gerarAntecipacao(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
		String entrada = request.getParameter("entrada");
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("/suprimento/crud/Ordemcompra");
		}
		
		Integer cdordemcompra = Integer.parseInt(whereIn);
		Ordemcompra ordemcompra = new Ordemcompra(cdordemcompra);
		ordemcompra = ordemcompraService.load(ordemcompra);
		
		List<Documentoorigem> listaDocOrigem = documentoorigemService.findByOrdemcompra(ordemcompra.getCdordemcompra().toString());
		if((ordemcompra.getOrdemcompraantecipada()!= null && ordemcompra.getOrdemcompraantecipada() && SinedUtil.isListEmpty(listaDocOrigem)) ||
				documentoorigemService.existeDocumentosNaoCancelado(listaDocOrigem)){
			request.addError("A Ordem de compra j foi antecipada.");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
		}
		
		SinedUtil.redirecionamento(request, "/financeiro/crud/Contapagar?ACAO=criar&gerarAntecipacaoOrdemcompra=true&entrada="+entrada+"&whereInOrdemcompra="+whereIn);
		return null;
	}
	
//	private void ordemCompraSessaoCriar(WebRequestContext request, Ordemcompra ordemcompra){
//		if (ordemcompra.getCdordemcompra()==null && ordemcompra.isVerificarOrdemCompraCriar()){
//			Object confirmarNovo = (Object) request.getSession().getAttribute("confirmarNovo");
//			if (confirmarNovo!=null){
//				request.getSession().setAttribute("confirmarNovo", null);
//				return;
//			}
//			Object ordemCompraSessaoCriar = (Object) request.getSession().getAttribute("ordemCompraSessaoCriar");
//			if (ordemCompraSessaoCriar==null){
//				request.getSession().setAttribute("ordemCompraSessaoCriar", new Object());
//			}else {
//				request.setAttribute("possuiOrdemCompraSessaoCriar", true);
//			}
//		}		
//	}
//	
//	public void confirmarNovo(WebRequestContext request){
//		request.getSession().setAttribute("confirmarNovo", new Object());
//	}
	
	public ModelAndView comboBoxEnderecoEntrega(WebRequestContext request, Fornecedor fornecedor) {
		if (fornecedor == null || fornecedor.getCdpessoa() == null) {
			throw new SinedException("Fornecedor no pode ser nulo.");
		}
		List<Endereco> lista = enderecoService.carregarListaEndereco(fornecedor);
		Integer selected = 0;
		if(lista != null){
			for(Endereco endereco : lista){
				if(endereco.getEnderecotipo().equals(Enderecotipo.UNICO)){
					selected = endereco.getCdendereco();
					break;
				}else if(endereco.getEnderecotipo().equals(Enderecotipo.ENTREGA)){
					selected = endereco.getCdendereco();
					break;
				}
			}
		}
		return new JsonModelAndView().addObject("lista", lista).addObject("selected", selected);
	}
	
	private void salvaProducaoagendaHistorico(Ordemcompra bean, String whereInProducaoagenda) {
		for (String cdproducaoagenda : whereInProducaoagenda.split(",")) {
			Producaoagenda producaoagenda = new Producaoagenda(Integer.parseInt(cdproducaoagenda));
			
			Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
			producaoagendahistorico.setProducaoagenda(producaoagenda);
			producaoagendahistorico.setObservacao("Ordem de compra gerada <a href=/w3erp/suprimento/crud/Ordemcompra?ACAO=consultar&cdordemcompra=" + bean.getCdordemcompra() +">"+bean.getCdordemcompra()+"</a>");
			producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
		}
	}
	
	public void criarFluxoCaixaOrdemCompraSemRegistro(WebRequestContext request){
		String dataInicio = request.getParameter("dtCompraInicio");
		String dataFim = request.getParameter("dtCompraFim");
		String whereInOC = request.getParameter("whereInOC");
		
		try {
			Date dtInicio = StringUtils.isNotEmpty(dataInicio) ? SinedDateUtils.stringToDate(dataInicio) : null;
			Date dtFim = StringUtils.isNotEmpty(dataFim) ? SinedDateUtils.stringToDate(dataFim) : null;
			
			if((dtInicio != null && dtFim != null && SinedDateUtils.afterOrEqualsIgnoreHour(dtFim, dtInicio)) ||
					StringUtils.isNotEmpty(whereInOC)){
				List<Ordemcompra> lista = ordemcompraService.buscarOrdemcompraCriarFluxocaixa(dtInicio, dtFim, whereInOC);
				if(SinedUtil.isListNotEmpty(lista)){
					ordemcompraService.gerarFluxoCaixaOrdens(lista);
				}
			}
		} catch (Exception e) {e.printStackTrace();}
	}
	
	/**
	* Mtodo ajax para verificar se existe itens restantes na ordem de compra
	*
	* @param request
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxVerificarBaixaComItensRestantes(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		boolean existeItensRestantes = false;
		if(StringUtils.isNotEmpty(whereIn)){
			existeItensRestantes = ordemcompraService.existeItensRestantes(whereIn);
		}
		return new JsonModelAndView().addObject("existeItensRestantes", existeItensRestantes);
	}
	
	/**
	* Mtodo que abre uma popup para confirmao de baixa da ordem de compra
	*
	* @param request
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopupBaixaComItensRestantes(WebRequestContext request){
		request.setAttribute("acao_value", request.getParameter("acao_value"));
		return new ModelAndView("direct:crud/popup/confirmacaoBaixaOrdemcompra");
	}
	
	/**
	* Mtodo que cria a ordem de compra de acordo com os itens restantes e redireciona para a tela de entrada
	*
	* @param request
	* @param solicitarrestanteBean
	* @throws Exception
	* @since 02/09/2015
	* @author Luiz Fernando
	*/
	public void criarOrdemcompraSolicitarrestantes(WebRequestContext request, SolicitarrestanteBean solicitarrestanteBean) throws Exception{
		Ordemcompra form = ordemcompraService.criaOrdemcompraBySolicitarrestanteBean(solicitarrestanteBean);
		Long identificadorControle = System.currentTimeMillis();
		request.getSession().setAttribute("formOrdemcompraSolicitarrestantes" + identificadorControle, form);
		SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra?ACAO=criar&criarOrdemcompraSolicitarrestantes=true&identificadorControle="+identificadorControle);
	}
	
	@SuppressWarnings("unchecked")
	public void ajaxValorTotal(WebRequestContext request){
		Object attr = request.getSession().getAttribute("listaOrdemcompramaterial" + request.getParameter("identificadortela"));
		List<Ordemcompramaterial> listaOrdemcompramaterial = null;
		Double valortotal = 0d;
		Money valortotalfrete = new Money();
		Money valortotaloutrasdespesas = new Money();
		Double valortotalipi = 0d;
		Double valortotalicms = 0d;
		Date dtProxEntrega = null;
		if (attr != null) {
			listaOrdemcompramaterial = (List<Ordemcompramaterial>) attr;
			for (Ordemcompramaterial ocm : listaOrdemcompramaterial) {
				valortotal += (ocm.getValor() != null ? ocm.getValor() : 0d);
				
				valortotalicms += ((ocm.getIcmsissincluso() == null || !ocm.getIcmsissincluso()) && ocm.getIcmsiss() != null ? ocm.getIcmsiss() : 0d);
				valortotalipi += ((ocm.getIpiincluso() == null || !ocm.getIpiincluso()) && ocm.getIpi() != null ? ocm.getIpi() : 0d);
				
				if(ocm.getDtentrega() != null && (dtProxEntrega == null || dtProxEntrega.after(ocm.getDtentrega())))
					dtProxEntrega = ocm.getDtentrega();
				if(ocm.getValorfrete() != null){
					valortotalfrete = valortotalfrete.add(ocm.getValorfrete());
				}
				if(ocm.getValoroutrasdespesas() != null){
					valortotaloutrasdespesas = valortotaloutrasdespesas.add(ocm.getValoroutrasdespesas());
				}
			}
		}
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
		View.getCurrent().println("var valorTotalFrete = '" + valortotalfrete + "';");
		View.getCurrent().println("var valorTotalOutrasDespesas = '" + valortotaloutrasdespesas + "';");
		View.getCurrent().println("var valorTotalIpi = '" + new Money(valortotalipi) + "';");
		View.getCurrent().println("var valorTotalIcms = '" + new Money(valortotalicms) + "';");
		View.getCurrent().println("var valorTotal = " + SinedUtil.round(valortotal, 2) + ";");
		View.getCurrent().println("var dtProxEntrega = '" + (dtProxEntrega != null ? format.format(dtProxEntrega) : "") + "';");
	}
	
	/**
	* Mtodo ajax para validar a quantidade solicitada com a quantidade comprada
	* OBS: a quantidade comprada no pode ser maior que a quantidade solicitada
	*
	* @param request
	* @param ordemcompramaterial
	* @return
	* @since 25/10/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxValidaQtdeSolicitacaocompra(WebRequestContext request, Ordemcompramaterial ordemcompramaterial){
		boolean qtdeMaior = false;
		StringBuilder mensagem = new StringBuilder();
		if(ordemcompramaterial.getMaterial() != null && SinedUtil.isListNotEmpty(ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial()) && 
				ordemcompramaterial.getUnidademedida() != null){
			Material material = materialService.load(ordemcompramaterial.getMaterial(), "material.cdmaterial, material.nome, material.identificacao, material.unidademedida");
			
			String whereInSolicitacao = CollectionsUtil.listAndConcatenate(ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial(), "solicitacaocompra.cdsolicitacaocompra", ",");
			List<Solicitacaocompra> listaSolicitacaocompra = solicitacaocompraService.findWithUnidademedida(whereInSolicitacao);
			
			Solicitacaocompra solicitacaocompra = null;
			for(Solicitacaocompraordemcompramaterial scocm : ordemcompramaterial.getListaSolicitacaocompraordemcompramaterial()){
				solicitacaocompra = solicitacaocompraService.getSolicitacaoByLista(scocm.getSolicitacaocompra(), listaSolicitacaocompra);
				if(solicitacaocompra != null && solicitacaocompra.getUnidademedida() != null && solicitacaocompra.getQtde() != null && 
						material != null && material.getUnidademedida() != null &&
						scocm.getQtde() != null){
					Double qtdeSol = solicitacaocompra.getQtde();
					Double qtde = scocm.getQtde();
					if(!solicitacaocompra.getUnidademedida().equals(ordemcompramaterial.getUnidademedida())){
						if(!solicitacaocompra.getUnidademedida().equals(material.getUnidademedida())){
							qtdeSol = unidademedidaService.getQtdeUnidadePrincipal(material, solicitacaocompra.getUnidademedida(), qtdeSol);
						}
						if(!material.getUnidademedida().equals(ordemcompramaterial.getUnidademedida())){
							qtde = unidademedidaService.getQtdeUnidadePrincipal(material, ordemcompramaterial.getUnidademedida(), qtde);
						}
					}
					
					if(qtdeSol != null && qtde != null && qtde > qtdeSol){
						qtdeMaior = true;
						mensagem
							.append("A quantidade de material informada extrapola a quantidade da solicitao.")
							.append(solicitacaocompra.getIdentificadorOuCdsolicitacaocompra())
							.append("\n");
					}
				}
			}
		}
		
		return new JsonModelAndView()
			.addObject("qtdeMaior", qtdeMaior)
			.addObject("mensagem", mensagem.toString());
	}
	@SuppressWarnings("unchecked")
	public ModelAndView ajaxBuscaCodigoAlternativo(WebRequestContext request, Ordemcompra ordemcompra){
			
		List<Ordemcompramaterial> listaOrdemcompramaterial = (List<Ordemcompramaterial>) request.getSession().getAttribute("listaOrdemcompramaterial" + request.getParameter("identificadortela"));
		
		if (parametrogeralService.getBoolean(Parametrogeral.ENVIAR_CODIGO_FORNECEDOR_MATERIAL_OC)) {
			ordemcompramaterialService.getCodigoAlternativo(listaOrdemcompramaterial, ordemcompra.getFornecedor());				
		}
				
		if (listaOrdemcompramaterial != null) {								
			int i = 0;
			for (Ordemcompramaterial lista : listaOrdemcompramaterial) {
				
				String codigoalternativo_value = lista.getCodigoalternativo() != null ? lista.getCodigoalternativo().toString() : "";
				
				View.getCurrent().println("$('#listaMaterialTrans\\\\[" + i + "\\\\]\\\\.codigoalternativo_value').text('" +codigoalternativo_value+ "');");				
				i++;
			}						
		}				
		return null;		
	}	
}
