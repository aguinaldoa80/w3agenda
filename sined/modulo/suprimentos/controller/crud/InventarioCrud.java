package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.InventarioHistorico;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialRateioEstoque;
import br.com.linkcom.sined.geral.bean.MovimentacaoEstoqueHistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioAcao;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.service.InventarioHistoricoService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.InventariomaterialService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoEstoqueHistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.InventarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/suprimento/crud/Inventario", authorizationModule=CrudAuthorizationModule.class)
public class InventarioCrud extends CrudControllerSined<InventarioFiltro, Inventario, Inventario>{
	
	private InventarioService inventarioService;
	private InventariomaterialService inventariomaterialService;
	private InventarioHistoricoService inventarioHistoricoService;
	private LocalarmazenagemService localarmazenagemService;	
	private MaterialService materialService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	
	public void setMovimentacaoEstoqueHistoricoService(
			MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {
		this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setInventarioHistoricoService(
			InventarioHistoricoService inventarioHistoricoService) {
		this.inventarioHistoricoService = inventarioHistoricoService;
	}
	public void setInventariomaterialService(
			InventariomaterialService inventariomaterialService) {
		this.inventariomaterialService = inventariomaterialService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Inventario form)throws CrudException {
		throw new SinedException("Não é permitido criar inventário.");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Inventario form)	throws CrudException {
		throw new SinedException("Não é permitido excluir inventário.");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Inventario form)throws Exception {
		if(form.getCdinventario() != null){
			form.setListaInventarioHistorico(inventarioHistoricoService.findByInventario(form));
			form.setListaInventariomaterial(inventariomaterialService.findByInventario(form));
		}
		form.ajustaBeanToLoad();
	}
	
	@Override
	protected void listagem(WebRequestContext request, InventarioFiltro filtro) throws Exception {
		List<InventarioSituacao> listaInventarioSituacao = new ArrayList<InventarioSituacao>();
		for (InventarioSituacao situacao : InventarioSituacao.values()) {
			listaInventarioSituacao.add(situacao);
		}
		request.setAttribute("listaInventarioSituacao", listaInventarioSituacao);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Inventario bean) throws Exception {
		super.salvar(request, bean);
		inventarioHistoricoService.createHistoricoByInventario(bean, InventarioAcao.ALTERADO);
	}
	
	/**
	 * Método que gera o csv de inventário
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarCSV(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn.indexOf(",") != -1){
			request.addError("Só é permitido gerar CSV de um inventário por vez.");
			return sendRedirectToAction("listagem");
		}
		
		String rel = inventarioService.gerarCSVInventario(inventarioService.loadForEntrada(new Inventario(Integer.parseInt(whereIn))));
		return new ResourceModelAndView(new Resource("text/csv","inventario_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll("null;", ";").getBytes()));
	}
	
	/**
	 * Ação de estornar inventário.
	 *
	 * @param request
	 * @return
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public ModelAndView estornar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(inventarioService.hasNotAtSituacao(whereIn, InventarioSituacao.CANCELADO, InventarioSituacao.AUTORIZADO)){
			request.addError("O estorno é permitido apenas para o(s) inventário(s) na situação de autorizado ou cancelado.");
			return sendRedirectToAction("listagem");
		}
		
		inventarioService.atualizaSituacao(whereIn, InventarioSituacao.EM_ABERTO);
		inventarioHistoricoService.createHistoricoByInventario(whereIn, InventarioAcao.ESTORNADO);
		request.addMessage("Inventário(s) estornado(s) com sucesso.");
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * Ação de autorizar inventário.
	 *
	 * @param request
	 * @return
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public ModelAndView autorizar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(inventarioService.hasNotAtSituacao(whereIn, InventarioSituacao.EM_ABERTO)){
			throw new SinedException("Só é possível autorizar inventário com em situação 'Em aberto'.");
		}
		
		inventarioService.atualizaSituacao(whereIn, InventarioSituacao.AUTORIZADO);
		inventarioHistoricoService.createHistoricoByInventario(whereIn, InventarioAcao.AUTORIZADO);
		request.addMessage("Inventário(s) autorizado(s) com sucesso.");
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * Ação de cancelar inventário.
	 *
	 * @param request
	 * @return
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cancelar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(inventarioService.hasNotAtSituacao(whereIn, InventarioSituacao.EM_ABERTO, InventarioSituacao.AUTORIZADO)){
			request.addError("Só é possível cancelar inventário com em situação 'Em aberto' ou 'Autorizado'.");
			SinedUtil.fechaPopUp(request);
			
			return null;
		}
		
		String erroInventarioSubsequente = inventarioService.getErrosInventarioSubsequente(whereIn);
		
		if (StringUtils.isNotEmpty(erroInventarioSubsequente)) {
			request.addError(erroInventarioSubsequente);
			SinedUtil.fechaPopUp(request);
			
			return null;
		}
		
		request.setAttribute("titulo", "Cancelar inventário");
		request.setAttribute("action", "saveCancelar");
		
		InventarioHistorico bean = new InventarioHistorico();
		bean.setWhereIn(whereIn);
		
		return new ModelAndView("direct:/crud/popup/confirmacaoCancelarInventario", "bean", bean);
	}
	
	/**
	 * Ação que salva e fecha a popup de cancelar inventário.
	 *
	 * @param request
	 * @param inventarioHistorico
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public void saveCancelar(WebRequestContext request, InventarioHistorico inventarioHistorico){
		String whereIn = inventarioHistorico.getWhereIn();
		if(StringUtils.isBlank(whereIn)){
			request.addError("Erro na passagem de parâmetros.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		List<Movimentacaoestoque> listaMovimentacoesByInventarios = movimentacaoestoqueService.findByInventario(whereIn);
		if(listaMovimentacoesByInventarios != null && listaMovimentacoesByInventarios.size() > 0){
			String whereInMovimentacoes = CollectionsUtil.listAndConcatenate(listaMovimentacoesByInventarios, "cdmovimentacaoestoque", ",");
			
			boolean erro = false;
			List<Movimentacaoestoque> movimentacoes = movimentacaoestoqueService.findMovimentacoesForQtdedisponivel(whereInMovimentacoes);
			for (Movimentacaoestoque movimentacaoestoque : movimentacoes) {
				if(movimentacaoestoque.getDtcancelamento() ==null){
					if(!localarmazenagemService.getPermitirestoquenegativo(movimentacaoestoque.getLocalarmazenagem())){
						if(movimentacaoestoque.getMovimentacaoestoquetipo() != null && 
								movimentacaoestoque.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.ENTRADA)){
							Double qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico( movimentacaoestoque.getMaterial(), movimentacaoestoque.getMaterialclasse(), movimentacaoestoque.getLocalarmazenagem(), movimentacaoestoque.getEmpresa(), movimentacaoestoque.getProjeto(), false, movimentacaoestoque.getLoteestoque());
							if(qtdeDisponivel != null && (qtdeDisponivel - movimentacaoestoque.getQtde()) < 0){
								request.addError("Cancelamento não permitido. O estoque ficará negativo.");
								erro = true;
								break;
							}
						}
					}
				}	
			}
			if(erro) {
				SinedUtil.fechaPopUp(request);
				return;
			}
			
			for (Movimentacaoestoque movimentacaoestoque : movimentacoes) {
				MovimentacaoEstoqueHistorico movimentacaoEstoqueHistorico = new MovimentacaoEstoqueHistorico();
				movimentacaoEstoqueHistorico.setMovimentacaoEstoque(movimentacaoestoque);
				movimentacaoEstoqueHistorico.setMovimentacaoEstoqueAcao(MovimentacaoEstoqueAcao.CANCELAR);
				movimentacaoEstoqueHistorico.setObservacao("Cancelamento do inventário");
				movimentacaoEstoqueHistoricoService.saveOrUpdate(movimentacaoEstoqueHistorico);
			}
			
			movimentacaoestoqueService.doUpdateMovimentacoes(whereInMovimentacoes);
		}
		
		inventarioService.atualizaSituacao(whereIn, InventarioSituacao.CANCELADO);
		inventarioHistoricoService.createHistoricoByInventario(whereIn, InventarioAcao.CANCELADO, inventarioHistorico.getObservacao());
		request.addMessage("Inventário(s) cancelado(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * Action que faz o registro da correção de estoque escriturado
	 *
	 * @param request
	 * @return
	 * @since 08/10/2019
	 * @author Rodrigo Freitas
	 */
	public ModelAndView registrarCorrecao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);			
		
		if(inventarioService.hasNotAtSituacao(whereIn, InventarioSituacao.AUTORIZADO)){
			request.addError("Só é possível registrar ajuste de inventário em situação 'Autorizado'.");
			return sendRedirectToAction("listagem");
		}
		if(whereIn.indexOf(",") != -1){
			request.addError("Só é permitido gerar um ajuste de um inventário por vez.");
			return sendRedirectToAction("listagem");
		}
		
		Inventario inventarioOrigem = new Inventario(Integer.parseInt(whereIn));
		List<Inventario> listaInventarioAjustesAnteriores = inventarioService.findAjustesByOrigem(inventarioOrigem);
		
		Inventario inventario = inventarioService.loadForEntrada(inventarioOrigem);
		if(!InventarioTipo.ESTOQUEESTRUTURADO.equals(inventario.getInventarioTipo())){
			request.addError("Só é possível registrar ajuste de inventário escriturado.");
			return sendRedirectToAction("listagem");
		}
		
		List<Inventariomaterial> listaInventariomaterial = inventariomaterialService.findByInventario(inventario);
		for (Inventario inventarioAjuste : listaInventarioAjustesAnteriores) {
			List<Inventariomaterial> listaInventariomaterialAjuste = inventarioAjuste.getListaInventariomaterial();
			for (Inventariomaterial inventariomaterialAjuste : listaInventariomaterialAjuste) {
				for (Inventariomaterial inventariomaterial : listaInventariomaterial) {
					boolean equalsMaterial = inventariomaterial.getMaterial().equals(inventariomaterialAjuste.getMaterial());
					boolean equalsLoteestoque = (inventariomaterial.getLoteestoque() == null && inventariomaterialAjuste.getLoteestoque() == null) ||
												(inventariomaterial.getLoteestoque() != null && inventariomaterial.getLoteestoque().equals(inventariomaterialAjuste.getLoteestoque()));
												
					
					if(equalsMaterial && equalsLoteestoque){
						if(inventariomaterial.getHasAjuste()){
							break;
						}
						inventariomaterial.setHasAjuste(Boolean.TRUE);
						inventariomaterial.setQtde(inventariomaterialAjuste.getQtde());
					}
				}
			}
		}
		
		for (Inventariomaterial inventariomaterial : listaInventariomaterial) {
			inventariomaterial.setQtdeOrigem(inventariomaterial.getQtde());
		}
		
		inventario.setListaInventariomaterial(listaInventariomaterial);
		inventario.setInventarioOrigem(inventarioOrigem);
		
		return new ModelAndView("/process/registrarInventarioCorrecao", "inventario", inventario); 
	}
	
	/**
	 * Action que salva a correção do inventário.
	 *
	 * @param request
	 * @param inventario
	 * @return
	 * @since 08/10/2019
	 * @author Rodrigo Freitas
	 */
	public ModelAndView saveRegistrarCorrecao(WebRequestContext request, Inventario inventario){
		List<Inventariomaterial> listaInventariomaterialForSave = new ArrayList<Inventariomaterial>();
		List<Inventariomaterial> listaInventariomaterial = inventario.getListaInventariomaterial();
		
		boolean hasError = false;
		
		if(SinedUtil.isListNotEmpty(listaInventariomaterial)){
			for (Inventariomaterial inventariomaterial : listaInventariomaterial) {
				if(!inventariomaterial.getQtde().equals(inventariomaterial.getQtdeOrigem())){
					listaInventariomaterialForSave.add(inventariomaterial);
				}
			}
		}
		
		if(listaInventariomaterialForSave.size() == 0) {
			request.addError("Nenhum item identificado para ser corrigido.");
			return new ModelAndView("/process/registrarInventarioCorrecao", "inventario", inventario); 
		}
		for (Inventariomaterial inventariomaterial : listaInventariomaterial) {
			if(!materialService.validateObrigarLote(inventariomaterial, request)){
				return new ModelAndView("/process/registrarInventarioCorrecao", "inventario", inventario);
			}
		}
		Localarmazenagem localarmazenagem = null;
		Localarmazenagem localarmazenagemTemp = null;
		if(inventario.getLocalarmazenagem() != null){
			localarmazenagem = localarmazenagemService.loadWithCentrocusto(inventario.getLocalarmazenagem());
		} else {
			localarmazenagemTemp = localarmazenagemService.loadPrincipalByEmpresa(inventario.getEmpresa());
			if(localarmazenagemTemp != null){
				localarmazenagem = localarmazenagemService.loadWithCentrocusto(localarmazenagemTemp);				
			}
		}
		inventario.setLocalarmazenagem(localarmazenagem);
		
		if(inventario.getLocalarmazenagem() == null){
			request.addError("Não é possível registrar ajuste. Não existe nenhum local de armazenagem marcado como principal.");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Inventario?ACAO=registrarCorrecao&selectedItens=" + inventario.getCdinventario());
			return null;
		}
		
		if(SinedUtil.isRateioMovimentacaoEstoque()){
			if(localarmazenagem.getCentrocusto() == null){
				request.addError("O centro de custo do local de armazenagem '" + localarmazenagem.getNome() + "' não foi encontrado.");
				SinedUtil.redirecionamento(request, "/suprimento/crud/Inventario?ACAO=registrarCorrecao&selectedItens=" + inventario.getCdinventario());
				hasError = true;
			}
			
			for (Inventariomaterial inventariomaterial : listaInventariomaterialForSave) {
				Material material = materialService.loadForMaterialRateioEstoque(inventariomaterial.getMaterial());
				MaterialRateioEstoque materialRateioEstoque = material.getMaterialRateioEstoque();
				if(materialRateioEstoque == null){
					request.addError("Os dados de conta gerencial para o rateio da movimentação de estoque do material '" + material.getNome() + "' não foram encontrados.");
					SinedUtil.redirecionamento(request, "/suprimento/crud/Inventario?ACAO=registrarCorrecao&selectedItens=" + inventario.getCdinventario());
					hasError = true;
				}
				if(materialRateioEstoque != null && (materialRateioEstoque.getContaGerencialAjusteEntrada() == null || materialRateioEstoque.getContaGerencialAjusteSaida() == null)){
					request.addError("A conta gerencial para o rateio da movimentação de estoque do material '" + localarmazenagem.getNome() + "' não foi encontrada.");
					SinedUtil.redirecionamento(request, "/suprimento/crud/Inventario?ACAO=registrarCorrecao&selectedItens=" + inventario.getCdinventario());
					hasError = true;
				}
			}
		}
		
		if (hasError){
			return null;
		}
		
		inventario.setInventarioTipo(InventarioTipo.CORRECAO);
		inventario.setInventarioSituacao(InventarioSituacao.EM_ABERTO);
		inventario.setCdinventario(null);
		inventarioService.saveOrUpdate(inventario);
		
		for (Inventariomaterial inventariomaterial : listaInventariomaterialForSave) {
			inventariomaterial.setInventario(inventario);
			inventariomaterialService.saveOrUpdate(inventariomaterial);
		}
		
		List<Movimentacaoestoque> listaMovimentacaoestoque = inventarioService.registraMovimentacaoestoqueForAjuste(inventario, listaInventariomaterial);
		String linkMovimentacoes = movimentacaoestoqueService.makeLinkHistoricoMovimentacaoestoque(CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ","));
		String linkInventarioOrigem = inventarioService.makeLinkHistorico(inventario);
		String linkInventarioNovo = inventarioService.makeLinkHistorico(inventario.getInventarioOrigem());
		
		inventarioHistoricoService.createHistoricoByInventario(inventario, InventarioAcao.CRIADO, "Registro criado a partir do ajuste do inventário " + linkInventarioNovo + ". Gerado as seguintes movimentações de estoque de ajuste " + linkMovimentacoes + ".");
		inventarioHistoricoService.createHistoricoByInventario(inventario.getInventarioOrigem(), InventarioAcao.AJUSTADO, "Novo registro de ajuste " + linkInventarioOrigem);
		
		request.addMessage("Correção de inventário realizada com sucesso.");
		
		return sendRedirectToAction("listagem");
	}
	
}
