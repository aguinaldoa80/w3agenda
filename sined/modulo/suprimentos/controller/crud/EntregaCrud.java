package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Ajustefiscal;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.geral.bean.Entregadocumentofreterateio;
import br.com.linkcom.sined.geral.bean.Entregadocumentoreferenciado;
import br.com.linkcom.sined.geral.bean.Entregadocumentosincronizacao;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamaterialinspecao;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedorhistorico;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialinspecaoitem;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Tipopessoaquestionario;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.Entregaacao;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.RetornoWMSEnum;
import br.com.linkcom.sined.geral.service.AjustefiscalService;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ColaboradorFornecedorService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.EntregadocumentoService;
import br.com.linkcom.sined.geral.service.EntregadocumentosincronizacaoService;
import br.com.linkcom.sined.geral.service.Entregahistoricoservice;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.FornecedorhistoricoService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.ManifestoDfeService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialfornecedorService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialinspecaoitemService;
import br.com.linkcom.sined.geral.service.MaterialnumeroserieService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.ModelodocumentofiscalService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.geral.service.NcmcapituloService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.OrdemcompraentregaService;
import br.com.linkcom.sined.geral.service.OrdemcompramaterialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.QuestionarioService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RequisicaomaterialService;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.geral.service.RomaneioitemService;
import br.com.linkcom.sined.geral.service.RomaneioorigemService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ConferenciaDadosFornecedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeAssociadaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeAtualizacaoMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemAssociadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.BaixarEntregaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.BaixarEntregaItemBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.FaturamentoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarPlaquetaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBaixarEntregaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBaixarEntregaItemBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBaixarEntregaRomaneio;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorEntrega;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EntregaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/suprimento/crud/Entrega", authorizationModule=CrudAuthorizationModule.class)
public class EntregaCrud extends CrudControllerSined<EntregaFiltro, Entrega, Entrega>{
	
	private EntregaService entregaService;
	private MaterialService materialService;
	private OrdemcompraService ordemcompraService;
	private PatrimonioitemService patrimonioitemService;
	private LocalarmazenagemService localarmazenagemService;
	private EmpresaService empresaService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	private PrazopagamentoService prazopagamentoService;
	private FornecedorService fornecedorService;
	private NcmcapituloService ncmcapituloService;
	private PessoaService pessoaService;
	private UnidademedidaService unidademedidaService;
	private UfService ufService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private OrdemcompramaterialService ordemcompramaterialService;
	private EntradafiscalService entradafiscalService;	
	private CentrocustoService centrocustoService;
	private DocumentotipoService documentotipoService;
	private MaterialnumeroserieService materialnumeroserieService;
	private EntregadocumentosincronizacaoService entregadocumentosincronizacaoService;
	private MaterialinspecaoitemService materialinspecaoitemService;
	private RateioService rateioService;
	private EntregadocumentoService entregadocumentoService;
	private RequisicaomaterialService requisicaomaterialService;
	private ParametrogeralService parametrogeralService;
	private OrdemcompraentregaService ordemcompraentregaService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private DocumentoorigemService documentoorigemService;
	private MovpatrimonioService movpatrimonioService;
	private RomaneioService romaneioService;
	private RomaneioorigemService romaneioorigemService;
	private RomaneioitemService romaneioitemService;
	private UsuarioService usuarioService;
	private MaterialfornecedorService materialfornecedorService;
	private QuestionarioService questionarioService;
	private ColaboradorFornecedorService colaboradorFornecedorService;
	private ModelodocumentofiscalService modelodocumentofiscalService;
	private AjustefiscalService ajustefiscalService;
	private DocumentoService documentoService;
	private Entregahistoricoservice entregahistoricoService;
	private MotivoavisoService motivoavisoService;
	private AvisoService avisoService;
	private FornecedorhistoricoService fornecedorhistoricoService;
	private ManifestoDfeService manifestoDfeService;
	private ArquivoService arquivoService;
	private InventarioService inventarioService;
	private LoteestoqueService loteestoqueService;
	
	public void setMaterialfornecedorService(
			MaterialfornecedorService materialfornecedorService) {
		this.materialfornecedorService = materialfornecedorService;
	}
	public void setRomaneioitemService(RomaneioitemService romaneioitemService) {
		this.romaneioitemService = romaneioitemService;
	}
	public void setRomaneioorigemService(
			RomaneioorigemService romaneioorigemService) {
		this.romaneioorigemService = romaneioorigemService;
	}
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	public void setOrdemcompraentregaService(
			OrdemcompraentregaService ordemcompraentregaService) {
		this.ordemcompraentregaService = ordemcompraentregaService;
	}
	public void setNcmcapituloService(NcmcapituloService ncmcapituloService) {
		this.ncmcapituloService = ncmcapituloService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setPrazopagamentoService(
			PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setUnidademedidaService(
			UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setOrdemcompramaterialService(OrdemcompramaterialService ordemcompramaterialService) {
		this.ordemcompramaterialService = ordemcompramaterialService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setMaterialnumeroserieService(MaterialnumeroserieService materialnumeroserieService) {
		this.materialnumeroserieService = materialnumeroserieService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setEntregadocumentosincronizacaoService(EntregadocumentosincronizacaoService entregadocumentosincronizacaoService) {
		this.entregadocumentosincronizacaoService = entregadocumentosincronizacaoService;
	}
	public void setMaterialinspecaoitemService(MaterialinspecaoitemService materialinspecaoitemService) {
		this.materialinspecaoitemService = materialinspecaoitemService;
	}	
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}		
	public void setEntregadocumentoService(EntregadocumentoService entregadocumentoService) {
		this.entregadocumentoService = entregadocumentoService;
	}
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {
		this.requisicaomaterialService = requisicaomaterialService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setMovpatrimonioService(MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setQuestionarioService(QuestionarioService questionarioService) {
		this.questionarioService = questionarioService;
	}
	public void setColaboradorFornecedorService (ColaboradorFornecedorService colaboradorFornecedorService){
		this.colaboradorFornecedorService = colaboradorFornecedorService;
	}
	public void setModelodocumentofiscalService(ModelodocumentofiscalService modelodocumentofiscalService) {
		this.modelodocumentofiscalService = modelodocumentofiscalService;
	}
	public void setAjustefiscalService(AjustefiscalService ajustefiscalService) {
		this.ajustefiscalService = ajustefiscalService;
	}	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setEntregahistoricoService(
			Entregahistoricoservice entregahistoricoService) {
		this.entregahistoricoService = entregahistoricoService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setFornecedorhistoricoService(FornecedorhistoricoService fornecedorhistoricoService) {
		this.fornecedorhistoricoService = fornecedorhistoricoService;
	}
	public void setManifestoDfeService(ManifestoDfeService manifestoDfeService) {
		this.manifestoDfeService = manifestoDfeService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Entrega bean) {
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void salvar(WebRequestContext request, Entrega bean)	throws Exception {
		Object attrEntregadocumento = request.getSession().getAttribute("listaEntregadocumento" + bean.getIdentificadortela());
		if (attrEntregadocumento != null) {
			bean.setListaEntregadocumento((List<Entregadocumento>)attrEntregadocumento);			
		}
		if(bean.getHaveOrdemcompra()){
			entregaService.preparaAtualizacaoDasSolicitacoes(bean);
			entregaService.verificaEntregaRomaneio(bean);
		}
		
		if(bean.getCdentrega() == null){
			entregaService.preparaListaInspecao(bean);
			
			if(bean.getHaveOrdemcompra()){
				List<Entregadocumento> listaEntregadocumento = bean.getListaEntregadocumento();
				Set<Entregamaterial> listaEntregamaterial = new ListSet<Entregamaterial>(Entregamaterial.class);
				
				for (Entregadocumento entregadocumento : listaEntregadocumento) {
					listaEntregamaterial.addAll(entregadocumento.getListaEntregamaterial());
				}
				
				List<Integer> listaOrdemcompramaterialID = new ArrayList<Integer>();
				
				for (Entregamaterial entregamaterial : listaEntregamaterial) {
					Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = entregamaterial.getListaOrdemcompraentregamaterial();
					if(listaOrdemcompraentregamaterial != null){
						for (Ordemcompraentregamaterial ordemcompraentregamaterial : listaOrdemcompraentregamaterial) {
							if(ordemcompraentregamaterial.getOrdemcompramaterial() != null && ordemcompraentregamaterial.getOrdemcompramaterial().getCdordemcompramaterial() != null)
								listaOrdemcompramaterialID.add(ordemcompraentregamaterial.getOrdemcompramaterial().getCdordemcompramaterial());
						}
					}
				}
				
				String whereIn = CollectionsUtil.concatenate(listaOrdemcompramaterialID, ",");
				if(StringUtils.isNotBlank(whereIn)){
					List<Ordemcompra> listaOrdemcompra = ordemcompraService.findByItens(whereIn);
					
					if(listaOrdemcompra != null && listaOrdemcompra.size() > 0){
						Set<Ordemcompraentrega> listaOrdemcompraentrega = new ListSet<Ordemcompraentrega>(Ordemcompraentrega.class);
						
						for (Ordemcompra ordemcompra : listaOrdemcompra) {
							Ordemcompraentrega ordemcompraentrega = new Ordemcompraentrega();
							ordemcompraentrega.setOrdemcompra(ordemcompra);
							
							listaOrdemcompraentrega.add(ordemcompraentrega);
						}
						
						bean.setListaOrdemcompraentrega(listaOrdemcompraentrega);
					}
				}
			}
		}
		
		boolean clearMessage = false;
		if(bean.getHaveOrdemcompra() && bean.getCdentrega() == null && bean.getController() != null)
			clearMessage = true;
		
		if(bean.isFromImportacaoArquivo()){
			Object object = request.getSession().getAttribute("ARQUIVO_XML_RECEBER_ENTREGA");
			if(object != null && object instanceof Arquivo){
				Arquivo arquivo = (Arquivo)object;
				bean.setArquivoxml(arquivo);
			}
		}
		boolean verificaCriar = true;
		if (bean.getCdentrega() != null){
			verificaCriar = false;
		}
		
		Arquivo arquivoxml = bean.getArquivoxml();
		bean.setArquivoxml(null);
		
		List<Entregadocumento> listaEntregadocumentoAjusteFiscal = null;
		String whereInEntregadocumento = entregaService.getWhereInEntregadocumento(bean);
		if(bean.getCdentrega() != null && StringUtils.isNotEmpty(whereInEntregadocumento)){
			List<Ajustefiscal> listaAjusteFiscal = ajustefiscalService.buscarItemComAjusteFiscal(whereInEntregadocumento);
			if(SinedUtil.isListNotEmpty(listaAjusteFiscal)){
				for(Entregadocumento entregadocumento : bean.getListaEntregadocumento()){
					if(entregadocumento.getCdentregadocumento() != null){
						Entregadocumento ajusteFiscalED = new Entregadocumento(entregadocumento.getCdentregadocumento());
						for(Ajustefiscal ajusteFiscal : listaAjusteFiscal){
							if(ajusteFiscal.getEntregadocumento() != null && ajusteFiscal.getEntregadocumento().getCdentregadocumento() != null &&
									ajusteFiscalED.getCdentregadocumento().equals(ajusteFiscal.getEntregadocumento().getCdentregadocumento())){
								if(ajusteFiscalED.getListaAjustefiscal() == null){
									ajusteFiscalED.setListaAjustefiscal(new ListSet<Ajustefiscal>(Ajustefiscal.class));
								}
								ajusteFiscalED.getListaAjustefiscal().add(ajusteFiscal);
							}
						}
						if(listaEntregadocumentoAjusteFiscal == null){
							listaEntregadocumentoAjusteFiscal = new ArrayList<Entregadocumento>(); 
						}
						listaEntregadocumentoAjusteFiscal.add(ajusteFiscalED);
					}
				}
			}
		}
		try {		
			super.salvar(request, bean);
			
			if(arquivoxml != null){
				entregaService.salvaArquivo(bean, arquivoxml);
			}
		} catch (Exception e) {	
			if(arquivoxml != null){
				bean.setArquivoxml(arquivoxml);
			}
			request.setAttribute("erroSalvar", "true");
			e.printStackTrace();
			throw new SinedException(e.getMessage());			
		}
		
		if(bean.getListaEntregadocumento() != null){
			for(Entregadocumento entregadocumento : bean.getListaEntregadocumento()){				
				entradafiscalService.saveDocumentosReferenciados(request, entregadocumento);
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaEntregadocumentoAjusteFiscal)){
			for(Entregadocumento entregadocumentoAjusteFiscal : listaEntregadocumentoAjusteFiscal){
				if(SinedUtil.isListNotEmpty(entregadocumentoAjusteFiscal.getListaAjustefiscal())){
					entradafiscalService.verificaExclusaoAjusteFiscal(entregadocumentoAjusteFiscal.getListaAjustefiscal(), entregadocumentoAjusteFiscal);
				}
			}
		}
		
		if (verificaCriar){
			try{
				Boolean isWms = empresaService.isIntegracaoWms(bean.getEmpresa());
				
				if(isWms != null && isWms){
					entradafiscalService.ajustaEntregamaterialForSincronizar(bean);
					List<Entregadocumentosincronizacao> listaEntregadocumentosincronizacao = entregadocumentosincronizacaoService.criarEntregadocumentoSincronizacao(bean, Boolean.FALSE);
					if(listaEntregadocumentosincronizacao != null && !listaEntregadocumentosincronizacao.isEmpty()){
						for(Entregadocumentosincronizacao eds : listaEntregadocumentosincronizacao){
							entregadocumentosincronizacaoService.saveOrUpdate(eds);
						}
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
				request.addError(e.getMessage());
			}
		}
		
		if(bean.getListaEntregadocumento() != null){
			for(Entregadocumento entregadocumento : bean.getListaEntregadocumento()){
				entradafiscalService.verificarMaterialLote(entregadocumento);
			}
		}
		
		if(clearMessage)
			request.clearMessages();
		
		try{
			if(bean.getListaEntregadocumento() != null){
				for(Entregadocumento entregadocumento : bean.getListaEntregadocumento()){
					if(SinedUtil.isListNotEmpty(entregadocumento.getListaEntregadocumentofreterateio())){
						entradafiscalService.calcularCustoEntradaMaterialByEntradafiscal(entregadocumento);
					}
				}
			}
		} catch (Exception e) {
			request.addError("Erro ao calcular o valor da entrada no estoque: " + e.getMessage());
		}
		
		try{
			if(bean.getListaEntregadocumento() != null){
				for(Entregadocumento entregadocumento : bean.getListaEntregadocumento()){
					materialService.calculaValorvendaByEntregadocumento(entregadocumento);
				}
			}
		} catch (Exception e) {
			request.addError("Erro ao calcular o valor de venda dos materiais: " + e.getMessage());
		}
		
		if(verificaCriar){
			entregahistoricoService.criaHistoricoEntrega(bean, Entregaacao.RECEBIMENTO_CRIADO);
			if(bean.getHaveOrdemcompra()){
				entregahistoricoService.criaHistoricoEntrega(bean, Entregaacao.ORDEM_COMPRA_ASSOCIADA);				
			}
		}
		
		if(StringUtils.isNotBlank(bean.getIdentificadortela())) {
			request.getSession().removeAttribute("entrada" + bean.getIdentificadortela());
			request.getSession().removeAttribute("listaEntregadocumento" + bean.getIdentificadortela());
		}
	}
	
	@Override
	protected Entrega criar(WebRequestContext request, Entrega form) throws Exception {
		if(SinedUtil.isAmbienteDesenvolvimento() && "true".equals(request.getParameter("copiar")) && form.getCdentrega() != null){
			form = entregaService.criarCopia(form);
			return form;
		}
		return super.criar(request, form);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Entrega form) throws CrudException {
		if(!CONSULTAR.equals(request.getParameter(CrudController.ACTION_PARAMETER))){
			// identificador tela est� preenchido quando vem da tela de Entrada Fiscal (edi��o do item da listaEntregadocumento)
			String identificadortela = request.getParameter("identificadortela");
			if(StringUtils.isNotBlank(identificadortela)){
				Object attr = request.getSession().getAttribute("entrega" + identificadortela);
				if(attr != null) {
					form = (Entrega) attr;
					Object attrLista = request.getSession().getAttribute("listaEntregadocumento" + identificadortela);
					if(attrLista != null) {
						List<Entregadocumento> listaEntregadocumento = (List<Entregadocumento>) attrLista;
						form.setListaEntregadocumento(listaEntregadocumento);
						
						if("true".equalsIgnoreCase(request.getParameter("recuperarEntregadocumentoTrans"))){
							try {
								Entregadocumento entregadocumento = (Entregadocumento) request.getSession().getAttribute("entregadocumentoTrans"  + form.getIdentificadortela());
								String indexLista = (String) request.getSession().getAttribute("indexListaEntregadocumentoTrans"  + form.getIdentificadortela());
								
								if(entregadocumento != null && StringUtils.isNotBlank(indexLista)){
									form.getListaEntregadocumento().set(Integer.parseInt(indexLista), entregadocumento);
									request.getSession().setAttribute("listaEntregadocumento" + identificadortela, form.getListaEntregadocumento());
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		return super.doEntrada(request, form);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void entrada(WebRequestContext request, Entrega form) throws Exception {
		if(form.getIdentificadortela() == null) {
			if(StringUtils.isNotBlank(request.getParameter("identificadortela"))) {
				form.setIdentificadortela(request.getParameter("identificadortela"));
			} else {
				String identificadortela = new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis());
				form.setIdentificadortela(identificadortela);
			}
		}
		
		request.setAttribute("modulo", "suprimento");
		
		if(request.getParameter("ACAO") != null && "consultar".equalsIgnoreCase(request.getParameter("ACAO"))){
			if(form == null || form.getListaEntregadocumento() == null || form.getListaEntregadocumento().isEmpty()){
				throw new SinedException("Entrega n�o possui documentos.");
			}
		}
		if(form.getArquivoxml() != null && form.getArquivoxml().getCdarquivo() != null ){
			DownloadFileServlet.addCdfile(request.getSession(), form.getArquivoxml().getCdarquivo());
			if(StringUtils.isBlank(form.getArquivoxml().getNome())) {
				try {
					form.setArquivoxml(arquivoService.loadWithContents(form.getArquivoxml()));
				} catch (Exception e) {}
			}
		}
		this.getRegistrosAtivo(request, form);
		request.setAttribute("empresasAtivos", empresaService.findAtivos(form.getEmpresa()));

		if(form.getCdentrega() == null && form.getDtentrega() == null){
			form.setDtentrega(new Date(System.currentTimeMillis()));
		}
		
		if(form.getCdentrega() != null){
			List<Entregadocumento> listaEntregadocumento = entradafiscalService.carregaListaEntregadocumentoForEntrega(form);
			if(request.getAttribute("erroSalvar") != null && "true".equals(request.getAttribute("erroSalvar"))){
				Object attrEntregadocumento = request.getSession().getAttribute("listaEntregadocumento" + form.getIdentificadortela());
				if (attrEntregadocumento != null) {
					List<Entregadocumento> listaEntregadocumentoSessao = (List<Entregadocumento>) attrEntregadocumento;
					for(Entregadocumento itemSessao : listaEntregadocumentoSessao) {
						if(!listaEntregadocumento.contains(itemSessao)) {
							listaEntregadocumento.add(itemSessao);
						}
					}
					form.setListaEntregadocumento(listaEntregadocumento);
				}
			}
			
			if(request.getAttribute("erroSalvar") != null && "true".equals(request.getAttribute("erroSalvar"))){
				Object attrEntregadocumento = request.getSession().getAttribute("listaEntregadocumento" + form.getIdentificadortela());
				if (attrEntregadocumento != null) {
					form.setListaEntregadocumento((List<Entregadocumento>) attrEntregadocumento);					
				}
			} else if(!form.existeEntregadocumentoEntregamaterial()) {
				form.setListaEntregadocumento(entradafiscalService.carregaListaEntregadocumentoForEntrega(form));
			}
			
			if(form.getListaEntregadocumento() != null){
				for(Entregadocumento entregadocumento : form.getListaEntregadocumento()){
					if(entregadocumento.getListaEntregadocumentoreferenciado() != null && !entregadocumento.getListaEntregadocumentoreferenciado().isEmpty()){
						String whereInEdr = SinedUtil.listAndConcatenateIDs(entregadocumento.getListaEntregadocumentoreferenciado());
						List<Documentoorigem> listaDocOrigem = documentoorigemService.getlistaDocumentosReferenciados(whereInEdr);
						if(listaDocOrigem != null && !listaDocOrigem.isEmpty()){
							Map<Integer, Documento> mapaDocumentos = new HashMap<Integer, Documento>();
							for(Documentoorigem docOrigem : listaDocOrigem){
								mapaDocumentos.put(docOrigem.getEntregadocumentoreferenciado().getCdentregadocumentoreferenciado(), docOrigem.getDocumento());
							}
							for(Entregadocumentoreferenciado edr : entregadocumento.getListaEntregadocumentoreferenciado()){
								edr.setDocumento(mapaDocumentos.get(edr.getCdentregadocumentoreferenciado()));
							}
						}
						
					}
				}
			}
			
			form.setListaOrdemcompraentrega(SinedUtil.listToSet(ordemcompraentregaService.findByEntrega(form), Ordemcompraentrega.class));
		} else {
			if(request.getAttribute("erroSalvar") != null && "true".equals(request.getAttribute("erroSalvar"))){
				Object attrEntregadocumento = request.getSession().getAttribute("listaEntregadocumento" + form.getIdentificadortela());
				if (attrEntregadocumento != null) {
					form.setListaEntregadocumento((List<Entregadocumento>) attrEntregadocumento);
				}
			}
		}
		
		if (request.getBindException().hasErrors()) {
			Object attrEntregadocumento = request.getSession().getAttribute("listaEntregadocumento" + form.getIdentificadortela());
			if (attrEntregadocumento != null) {
				form.setListaEntregadocumento((List<Entregadocumento>) attrEntregadocumento);
			}
		}
		
		if (form != null && form.getCdentrega() != null) {
			List<OrigemsuprimentosBean> listaBeanDestino = entregaService.montaDestino(form);
			request.setAttribute("listaDestino", listaBeanDestino);
		}
	
//		request.setAttribute("tamanholista", form.getListaEntregamaterial() != null ? form.getListaEntregamaterial().size() : 0);
//		if (form.getCdentrega() != null && form.getListaEntregamaterial() != null) {
//			for (Entregamaterial e1 : form.getListaEntregamaterial()) {
//				e1.setMaterialclassetrans(e1.getMaterialclasse());
//			}
//		}
		
		List<OrigemsuprimentosBean> listaBeanOrigem = entregaService.montaOrigem(form);
		request.setAttribute("listaOrigem", listaBeanOrigem);
		
		if(form.getHaveOrdemcompra())
			calculaQtdeEntrega(form, false);
		
		if(form.getListaEntregadocumento() == null) form.setListaEntregadocumento(new ArrayList<Entregadocumento>());
		request.getSession().setAttribute("listaEntregadocumento" + form.getIdentificadortela(), form.getListaEntregadocumento());
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		request.setAttribute("ignoreHackAndroidDownload", true);
		
		
		request.setAttribute("abrirTelaContaPagarNaEntrega", parametrogeralService.getBoolean(Parametrogeral.ABRIR_TELA_CONTAPAGAR_NA_ENTREGA));
		entradafiscalService.setDocumentoreferenciado(form);
	}
	
	@Override
	protected Entrega carregar(WebRequestContext request, Entrega bean) throws Exception {
		Entrega entrega = super.carregar(request, bean);
		
		String acao = request.getParameter(MultiActionController.ACTION_PARAMETER);
		if("editar".equals(acao) && entrega.getAux_entrega() !=  null && !entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO)){
			throw new SinedException("N�o � poss�vel editar uma entrega com situa��o diferente de 'EM ABERTO'.");
		}
		
		return entrega;
	}
	
	@Override
	protected void listagem(WebRequestContext request, EntregaFiltro filtro) throws Exception {
		this.getRegistrosAtivo(request, null);
		
		List<Situacaosuprimentos> list = new ArrayList<Situacaosuprimentos>();
			list.add(Situacaosuprimentos.EM_ABERTO);
			list.add(Situacaosuprimentos.FATURADA);
			list.add(Situacaosuprimentos.BAIXADA);
			list.add(Situacaosuprimentos.CANCELADA);
			
		request.setAttribute("listaSituacao", list);
		request.setAttribute("gruposAtivos", materialgrupoService.findAtivos(filtro != null ? filtro.getMaterialgrupo() : null));
		request.setAttribute("tiposAtivos", materialtipoService.findAtivos(filtro != null ? filtro.getMaterialtipo() : null));
		
		TotalizadorEntrega totalizadorEntrega = entregaService.findForListagemTotalizador(filtro);
		request.setAttribute("totalAberto", totalizadorEntrega.getTotalAberto());
		request.setAttribute("totalFaturado", totalizadorEntrega.getTotalFaturado());
		request.setAttribute("totalBaixado", totalizadorEntrega.getTotalBaixado());
		
		Money totalGeral = totalizadorEntrega.getTotalAberto()
							.add(totalizadorEntrega.getTotalFaturado())
							.add(totalizadorEntrega.getTotalBaixado())
							.add(totalizadorEntrega.getTotalCancelado());
		
		request.setAttribute("totalGeral", totalGeral);
	}
	
	@Override
	protected ListagemResult<Entrega> getLista(WebRequestContext request, EntregaFiltro filtro) {
		
		ListagemResult<Entrega> listagemResult = super.getLista(request, filtro);
		List<Entrega> lista = listagemResult.list();
		
		if(lista != null && !lista.isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdentrega", ",");
			lista.removeAll(lista);
			lista.addAll(entregaService.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}
		
		return listagemResult;
	}
	
	/** M�todo que busca registros ativos que ser�o usados tanto 
	 *  na listagem quanto na entrada
	 * 
	 * @param request
	 * @author Tom�s Rabelo
	 * @param entrega 
	 */
	private void getRegistrosAtivo(WebRequestContext request, Entrega entrega) {
		request.setAttribute("locaisAtivos", localarmazenagemService.findAtivos(entrega != null ? entrega.getLocalarmazenagem() : null));
		request.setAttribute("centrosAtivos", centrocustoService.findAtivos());
		request.setAttribute("tiposDocumentos", documentotipoService.findDocumentoTipoOrdemCompra(null));
	}
	
	/**
	 * M�todo que faz o cancelamento das entregas.
	 * 
	 * @see #getItensSelecionados(WebRequestContext)
	 * @see br.com.linkcom.sined.geral.service.EntregaService#loadWithSituacao
	 * @see br.com.linkcom.sined.geral.service.EntregaService#getSolicitacoesCompraRelacionadasAsEntregas(List)
	 * @see br.com.linkcom.sined.geral.service.EntregaService#cancelaEstornaEntregaAtualizaSolicitacoes(List, String, Situacaosuprimentos)
	 * @see #getControllerModelAndView(WebRequestContext)
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@OnErrors(LISTAGEM)
	public ModelAndView cancelar(WebRequestContext request){
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		
		List<Entrega> listaEntrega = entregaService.loadWithSituacao(itensSelecionados);
		for (Entrega entrega : listaEntrega) {
			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.FATURADA)) {
				request.addMessage("Cancelamento n�o permitido. A entrega est� faturada.", MessageType.ERROR);
				return getControllerModelAndView(request);
			}
			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA) 
					&& movimentacaoestoqueService.existeMovimentacaoestoqueByEntrega(entrega, null)) {
				request.addMessage("Cancelamento n�o permitido. A entrega est� baixada.", MessageType.ERROR);
				return getControllerModelAndView(request);
			}
			else if(!entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)
					&& documentoService.existeContaPagarByEntrega(entrega)){
				request.addMessage("Cancelamento n�o permitido. � necess�rio cancelar " +
						"primeiramente as contas a pagar vinculadas ao recebimento.", MessageType.ERROR);
				return getControllerModelAndView(request);
			}		
				
			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)) {
				request.addMessage("Entrega j� est� cancelada.", MessageType.ERROR);
				return getControllerModelAndView(request);
			}			
		}
		
		List<Solicitacaocompra> solicitacaoes = entregaService.getSolicitacoesCompraRelacionadasAsEntregas(listaEntrega);
		entregaService.cancelaEstornaEntregaAtualizaSolicitacoes(solicitacaoes, itensSelecionados, Situacaosuprimentos.CANCELADA);
		request.addMessage("Entrega(s) cancelada(s) com sucesso.");
		this.entregahistoricoService.saveHistoricos(listaEntrega, Entregaacao.RECEBIMENTO_CANCELADO);
		
		return getControllerModelAndView(request);
	}
	
	/**
	 * Retorna para o controller do USC
	 * 
	 * @param request
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			if(!action.contains("cdordemcompra")){
				String id = request.getParameter("selectedItens");
				if(id != null && !id.equals(""))
					action += "&cdentrega="+id;
			}
		}
		
		String gerarPlaquetaAposBaixa = "";
		String itensPlaqueta = "";
		if(request.getAttribute("gerarPlaquetaAposBaixa") != null){
			if(action.contains("/suprimento/crud/Entrega") && !action.contains("consultar")){
				action += "?ACAO=listagem";
			}
			gerarPlaquetaAposBaixa = "&gerarPlaquetaAposBaixa=" + (String) request.getAttribute("gerarPlaquetaAposBaixa");
			itensPlaqueta = "&itensPlaqueta=" + (String) request.getAttribute("itensPlaqueta");
			if(itensPlaqueta != null && !"".equals(itensPlaqueta))
				gerarPlaquetaAposBaixa += itensPlaqueta;
		}
		return new ModelAndView("redirect:"+action + gerarPlaquetaAposBaixa);
	}
	
//	/**
//	 * M�todo que faz o estorno das entregas.
//	 * 
//	 * @see br.com.linkcom.sined.geral.service.EntregaService#loadWithSituacao
//	 * @see br.com.linkcom.sined.geral.service.EntregaService#updateEstorno
//	 * @see #getItensSelecionados(WebRequestContext)
//	 * @see #getControllerModelAndView(WebRequestContext)
//	 * @param request
//	 * @return
//	 * @throws Exception
//	 * @author Rodrigo Freitas
//	 * @author Tom�s Rabelo
//	 */
//	@OnErrors(LISTAGEM)
//	public ModelAndView estornar(WebRequestContext request){
//		
//		String itensSelecionados = SinedUtil.getItensSelecionados(request);
//		
//		List<Entrega> listaEntrega = entregaService.loadWithSituacao(itensSelecionados);
//		for (Entrega entrega : listaEntrega) {
//			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO)) {
//				request.addError("Entrega j� est� estornada.");
//				break;
//			}
//			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.FATURADA)) {
//				request.addError("Estorno n�o permitido. A entrega est� faturada.");
//				break;
//			}
//			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)) {
//				request.addError("Estorno n�o permitido. A entrega est� baixada.");
//				break;
//			}
//			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)) {
//				request.addError("Estorno n�o permitido. H� outra entrega efetuando o processo.");
//				break;
//			}
//		}
//		
//		if(request.getMessages().length == 0){
//			for (Entrega entrega : listaEntrega) {
//				BindException errors = new BindException(entrega, "entrega");
//				validateBean(entrega, errors);
//				if(errors.hasErrors()){
//					request.addError("A quantidade total estornada do material � maior que a quantidade pedida na ordem de compra.");
//				}
//			}
//		}
//		
//		if(request.getMessages().length == 0){
//			Entrega entrega = listaEntrega.get(0);
//			entregaService.preparaAtualizacaoDasSolicitacoes(entrega);
//			entregaService.cancelaEstornaEntregaAtualizaSolicitacoes(entrega.getListaSolicitacao(), itensSelecionados, Situacaosuprimentos.ESTORNAR);
//			//			entregaService.updateEstorno(itensSelecionados);
//			request.addMessage("Entrega(s) estornada(s) com sucesso.");
//		}
//		return getControllerModelAndView(request);
//	}
	
	public ModelAndView estornar(WebRequestContext request){
		
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		
		List<Entrega> listaEntrega = entregaService.loadWithSituacao(itensSelecionados);
		List<Entrega> listaEntregaBaixa = new ArrayList<Entrega>();
		List<Entrega> listaEntregaFaturada = new ArrayList<Entrega>();
		List<Entrega> listaEntregaEmaberto = new ArrayList<Entrega>();
		for (Entrega entrega : listaEntrega) {
			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO)) {
				request.addError("Entrega j� est� estornada.");
				break;
			}
			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.FATURADA)) {
				if(documentoorigemService.existDocumentosByEntrega(entrega)){
					request.addError("Estorno n�o permitido. A entrega est� faturada e possui documentos n�o cancelados.");
					break;
				}else {
					listaEntregaFaturada.add(entrega);
				}
			}
			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)) {
				if(movimentacaoestoqueService.existEntradaByEntrega(entrega) || movpatrimonioService.existMovPatrimonioDaEntrega(entrega)){
					request.addError("Estorno n�o permitido. A entrega est� baixada e possui movimenta��o de estoque/patrim�nio.");
					break;
				}else {
					listaEntregaBaixa.add(entrega);
				}
			}
			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)) {
				listaEntregaEmaberto.add(entrega);
			}
		}
		
		if(request.getMessages().length == 0){
			for (Entrega entrega : listaEntregaBaixa) {
				BindException errors = new BindException(entrega, "entrega");
				entregaService.validateQtdeEntregue(entrega, errors);
				if(errors.hasErrors()){
					request.addError("A quantidade total estornada do material � maior que a quantidade pedida na ordem de compra.");
				}
			}
			for (Entrega entrega : listaEntregaEmaberto) {
				Entrega e = entregaService.loadForEntrada(entrega);
				e.setListaEntregadocumento(entradafiscalService.carregaListaEntregadocumentoForEntrega(e));
				BindException errors = new BindException(entrega, "entrega");
				entregaService.validateQtdeEntregue(entrega, errors);
				if(errors.hasErrors()){
					request.addError("A quantidade total estornada do material � maior que a quantidade pedida na ordem de compra.");
				}
			}
		}
		
		if(request.getMessages().length == 0){
			for(Entrega entrega : listaEntregaBaixa){
				entregaService.updateDtBaixa(null, entrega.getCdentrega().toString());
			}
			for(Entrega entrega : listaEntregaFaturada){
				entregaService.callProcedureAtualizaEntrega(entrega);
			}
			for(Entrega entrega : listaEntregaEmaberto){
				entregaService.updateEstorno(entrega.getCdentrega().toString());
				entregaService.callProcedureAtualizaEntrega(entrega);
			}
			List<Entrega> entregas = new ArrayList<Entrega>();
			entregas.addAll(listaEntregaBaixa);
			entregas.addAll(listaEntregaEmaberto);
			entregas.addAll(listaEntregaFaturada);
			this.entregahistoricoService.saveHistoricos(entregas, Entregaacao.RECEBIMENTO_ESTORNADO);
			
			request.addMessage("Entrega(s) estornada(s) com sucesso.");
		}
		return getControllerModelAndView(request);
	}
	
	/**
	 * Carrega a pop-up para a escolha do tipo dos documentos para o faturamento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#loadWithSituacao
	 * @see #getControllerModelAndView(WebRequestContext)
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView doFaturar(WebRequestContext request){
		
		String itensSelecionados = SinedUtil.getItensSelecionados(request);	
		
		List<Entrega> listaEntrega = entregaService.loadWithSituacao(itensSelecionados);
		Questionario questionario = questionarioService.carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario.FORNECEDOR);
		
		for (Entrega entrega : listaEntrega) {
			if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)) {
				request.addError("N�o � poss�vel faturar entrega cancelada.");
				return getControllerModelAndView(request);
			}else if (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) && (entrega.getInspecaocompleta() != null && !entrega.getInspecaocompleta())) {
				request.addError("Para faturar, a inspe��o deve estar conclu�da.");
				return getControllerModelAndView(request);
			}else if(entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) &&
					  entrega.getFaturamentocliente()){
				request.addError("N�o � poss�vel faturar entrega com faturamento no cliente.");
				return getControllerModelAndView(request);
			}else if(entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.FATURADA)){
				request.addError("Entrega j� est� faturada.");
				return getControllerModelAndView(request);
			}else if(entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)){
				request.addError("Entrega j� foi faturada.");
				return getControllerModelAndView(request);
				
			}else if(questionario != null 
					&& questionario.getAvaliarrecebimento() != null
					&& questionario.getObrigaavaliacaoreceber() != null
					&& questionario.getObrigaavaliacaoreceber()
					&& questionario.getAvaliarrecebimento()){
				StringBuilder fornecedores = new StringBuilder();
				for (int i = 0; i < entrega.getListaEntregadocumento().size(); i++) {
					if (entrega.getListaEntregadocumento().get(i).getFornecedorAvaliado() == null || !entrega.getListaEntregadocumento().get(i).getFornecedorAvaliado()) {
						if (fornecedores.length() > 0) {
							fornecedores.append(", " + entrega.getListaEntregadocumento().get(i).getFornecedor().getNome());
						} else {
							fornecedores.append(entrega.getListaEntregadocumento().get(i).getFornecedor().getNome());
						}
					}
				}
				if (fornecedores.length() > 0) {
					request.addError("Aten��o! � obrigat�rio a avalia��o do(s) fornecedor(es) " + fornecedores.toString() + " para faturar e baixar e dar entrada do material  no estoque.");
					return getControllerModelAndView(request);
				}
			}
			
			if(entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
				Boolean simplesremessa = Boolean.TRUE;
				for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
					if(entregadocumento.getSimplesremessa() == null || !entregadocumento.getSimplesremessa()){
						simplesremessa = Boolean.FALSE;
					}
				}
				if(simplesremessa){
					request.addError("N�o � poss�vel faturar entrega do tipo simples remessa.");
					return getControllerModelAndView(request);
				}
			}
		}
		
		FaturamentoBean bean = new FaturamentoBean();
		bean.setIds(itensSelecionados);
		
		return continueOnAction("faturarEntrega", bean);
	}
	
	/**
	 * M�todo que fecha o pop up e redireciona o usu�rio para a tela de listagem
	 * 
	 * @param request
	 * @author Tom�s Rabelo
	 */
	/*private void recarregarPaginaListagem(WebRequestContext request) {
		request.getServletResponse().setContentType("text/html");
//		View.getCurrent().println("<script>window.opener.location.href = '"+request.getServletRequest().getContextPath()+"/suprimento/crud/Entrega?ACAO=listagem'; window.close();</script>");
		View.getCurrent().println("<script>window.opener.location = window.opener.location; window.close();</script>");
	}*/
	
	/**
	 * Faz o faturamento das entregas selecionadas na tela.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#doFaturar
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public ModelAndView faturarEntrega(WebRequestContext request, FaturamentoBean bean){
		String whereIn = request.getParameter("selectedItens");
		List<Entrega> listaEntrega = entregaService.findByEntrega(whereIn);
		HashMap<Integer, String> retornowms = new HashMap<Integer, String>();
		StringBuilder whereInEd;
		for (Entrega entrega : listaEntrega) {
			if(empresaService.isIntegracaoWms(entrega.getEmpresa())){
				whereInEd = new StringBuilder();
				for(Entregadocumento ed : entrega.getListaEntregadocumento()){
					if(ed.existeItemSincronizarWms()){
						if (ed.getRetornowms() != null){
							if (!ed.getRetornowms().equals(RetornoWMSEnum.CONFERIDA_OK) 
									&& !ed.getRetornowms().equals(RetornoWMSEnum.CONFERIDA_DIVERGENCIA)){
								whereInEd.append(!"".equals(whereIn.toString()) ? "," : "").append(ed.getCdentregadocumento());
								request.addError("Para faturar � preciso que a Entrega " + entrega.getCdentrega() + " seja conferida!");
							}
						} else{		
							whereInEd.append(!"".equals(whereIn.toString()) ? "," : "").append(ed.getCdentregadocumento());
							request.addError("Para faturar � preciso que a Entrega " + entrega.getCdentrega() + " seja conferida!");
						}
					}
				}
				if(!"".equals(whereInEd.toString())){
					retornowms.put(entrega.getCdentrega(), whereInEd.toString());
				}
			}
		}
		
		if(entregaService.verificaQtdeItens(bean.getIds()) > 1){
			return new ModelAndView("redirect:/financeiro/crud/Contapagar?ACAO=criarFaturamentoEntrega&selectedEntregasFaturar=" + bean.getIds()+"&entregaUnica=false");
		}else {
			if(parametrogeralService.getBoolean(Parametrogeral.ABRIR_TELA_CONTAPAGAR_NA_ENTREGA)){
				Entrega entrega = entregaService.findForFaturar(bean.getIds());
				StringBuilder whereInEntregadocumento = new StringBuilder();
				int countEntradafiscal = 0;
				for(Entregadocumento ed : entrega.getListaEntregadocumento()){
					if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
						if(!whereInEntregadocumento.toString().contains(ed.getCdentregadocumento().toString())){
							whereInEntregadocumento.append(ed.getCdentregadocumento());
							countEntradafiscal++;
						}
					}
				}
				
				if(countEntradafiscal > 1){
					request.addError("O recebimento selecionado possui mais de um documento vinculado e n�o poder� ser faturado.");
				}else if(countEntradafiscal == 1){
					boolean existsSomenteDocumentoantecipado = true;
					boolean existsEntregapagamento = false;
					for(Entregadocumento ed : entrega.getListaEntregadocumento()){
						if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){

							Set<Entregapagamento> listaEntregaPagamento = ed.getListadocumento();	
							for(Entregapagamento entregapagamento : listaEntregaPagamento){
								existsEntregapagamento = true;
								if(entregapagamento.getDocumentoantecipacao() == null){
									existsSomenteDocumentoantecipado = false;
								}
							}
						}
					}

					if(existsEntregapagamento && existsSomenteDocumentoantecipado){
						if(entregaService.doFaturar(bean.getIds(), bean.getDocumentotipo(), retornowms)){
							request.addMessage("Faturamento gerado com sucesso.");
						}
					}else{
						return new ModelAndView("redirect:/financeiro/crud/Contapagar?ACAO=criarFaturamentoEntrega&selectedEntregasFaturar=" + bean.getIds()+"&entregaUnica=true");
					}
				}
				
			}else if(entregaService.doFaturar(bean.getIds(), bean.getDocumentotipo(), retornowms)){
				request.addMessage("Faturamento gerado com sucesso.");
			}
		}
		
		String action = request.getParameter("controller");
		if(action.contains("consultar"))
			action += "&cdentrega="+ request.getParameter("selectedItens");
		
		return new ModelAndView("redirect:" + action);		
	}
	
	
	
	/**
	 * M�todo para fazer o carregamento da ordem de compra selecionada.
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrdemcompraService#loadForEntrada(Ordemcompra)
	 * @see #gerarEntrega(WebRequestContext, Ordemcompra)
	 * @param request
	 * @param ordemcompra
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView carregaOrdem(WebRequestContext request, Ordemcompra ordemcompra){
		
		Boolean agrupamento = ordemcompra.getAgrupamentoMesmoMaterial();
		Boolean receberEntregaByMaterialmestre = ordemcompra.getReceberEntregaByMaterialmestre();
		String whereIn = ordemcompra.getWhereIn();
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Erro ao carregar ordem de compra. Par�metro n�o pode ser nulo");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
			return null;
		}
			
		String[] ids = whereIn.split(",");

		// CARREGA OS DADOS DA PRIMEIRA ORDEM DE COMPRA
		ordemcompra = ordemcompraService.loadForEntrada(new Ordemcompra(Integer.parseInt(ids[0])));
		
		Integer cdordemcompra = ordemcompra.getCdordemcompra();
		
		List<Ordemcompramaterial> listaMateriais = new ArrayList<Ordemcompramaterial>();
		for (int i = 0; i < ids.length; i++) {
			listaMateriais.addAll(ordemcompramaterialService.findByOrdemcompra(new Ordemcompra(Integer.parseInt(ids[i]))));
		}
		ordemcompra.setListaMaterial(listaMateriais);
		ordemcompra.setReceberEntregaByMaterialmestre(receberEntregaByMaterialmestre);
		if(receberEntregaByMaterialmestre != null && receberEntregaByMaterialmestre){
			listaMateriais = ordemcompraService.createListaByGrade(listaMateriais);
		}
		
		Double valor = 0.0;		
		for (Ordemcompramaterial o1 : ordemcompra.getListaMaterial()) {
			valor = valor + o1.getValor();
		}
		
		ordemcompra.setValorprodutos(new Money(valor));
		ordemcompra.setWhereIn(whereIn);
		ordemcompra.setCdordemcompra(cdordemcompra);
		ordemcompra.setAgrupamentoMesmoMaterial(agrupamento);
		ordemcompra.setController("/suprimento/crud/Ordemcompra");
		
		return continueOnAction("gerarEntrega", ordemcompra);
	}
	
	public ModelAndView confimarConferencia(WebRequestContext request, ImportacaoXmlNfeBean bean){
		
		String whereIn = bean.getWhereInOrdemcompra();
		String[] ids = whereIn.split(",");
		
		Ordemcompra ordemcompra = ordemcompraService.loadForEntrada(new Ordemcompra(Integer.parseInt(ids[0])));
		ordemcompra.setReceberEntregaByMaterialmestre(bean.getReceberEntregaByMaterialmestre());
		
		Double valor = 0.0;		
		for (Ordemcompramaterial o1 : ordemcompra.getListaMaterial()) {
			valor = valor + o1.getValor();
		}
		
		ordemcompra.setValorprodutos(new Money(valor));
		ordemcompra.setWhereIn(whereIn);
		ordemcompra.setController(bean.getController());
		ordemcompra.setAgrupamentoMesmoMaterial(bean.getAgrupamentoMaterial());
		ordemcompra.setFromConferenciaXml(Boolean.TRUE);
		
		Material material = null;
		Ncmcapitulo ncmcapitulo = null;
		ImportacaoXmlNfeAtualizacaoMaterialBean atualizaBean;
		Object object = request.getSession().getAttribute("IMPORTADOR_BEAN_RECEBER_ENTREGA");
		if(object != null && object instanceof ImportacaoXmlNfeBean){
			ImportacaoXmlNfeBean bean_session = (ImportacaoXmlNfeBean)object;
			for (ImportacaoXmlNfeItemBean it_session : bean_session.getListaItens()) {
				it_session.setListaItemAssociado(null);
				for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
					if(it.getSequencial().equals(it_session.getSequencial())){
						it_session.setMaterial(it.getMaterial());
						it_session.setWhereInOrdemcompramaterial(it.getWhereInOrdemcompramaterial());
						it_session.setListaRastro(it.getListaRastro());
						if(SinedUtil.isListNotEmpty(it.getListaItemAssociado())){
							if(it_session.getListaItemAssociado() == null){
								it_session.setListaItemAssociado(it.getListaItemAssociado());
							}else {
								it_session.getListaItemAssociado().addAll(it.getListaItemAssociado());
							}
						}
						
						if(it.getMaterial() != null){
							material = materialService.loadForNFProduto(it.getMaterial());
							
							atualizaBean = new ImportacaoXmlNfeAtualizacaoMaterialBean();
							atualizaBean.setTipoitemsped(material.getTipoitemsped());
							
							atualizaBean.setCodigobarras_bd(material.getCodigobarras());
							atualizaBean.setExtipi_bd(material.getExtipi());
							atualizaBean.setCodlistaservico_bd(material.getCodlistaservico());
							
							if(it_session.getNcm() != null && it_session.getNcm().trim().length() > 1){
								String ncm = it_session.getNcm().trim();
								if(ncm.length() > 2){
									atualizaBean.setNcmcompleto_xml(ncm);
								}
								Integer id_ncm = Integer.parseInt(ncm.substring(0, 2));
								ncmcapitulo = ncmcapituloService.load(new Ncmcapitulo(id_ncm));
								atualizaBean.setNcmcapitulo_xml(ncmcapitulo);
							}
							
							atualizaBean.setCodigobarras_xml(it_session.getCean());
							atualizaBean.setExtipi_xml(it_session.getExtipi());
							atualizaBean.setCodlistaservico_xml(it_session.getCListServ());
							
							materialService.updateInfByConferenciaEntradafical(material, atualizaBean);
						}
						it_session.setUnidademedidaAssociada(it.getUnidademedidaAssociada());
					}
					
				}
				
				if(ordemcompra.getFornecedor() != null){
					if(it_session.getMaterial()!=null && it_session.getMaterial().getCdmaterial()!=null){
						materialfornecedorService.saveIfNotExists(material, ordemcompra.getFornecedor(), it_session.getCprod());
					}
				}
			}
			
			ordemcompra.setImportacaoXmlNfeBean(bean_session);
		}
		
		return continueOnAction("gerarEntrega", ordemcompra);
	}
	
	public ModelAndView conferenciaDados(WebRequestContext request, Ordemcompra ordemcompra) throws JDOMException, IOException, ParseException{
		Map<Integer, List<Unidademedida>> mapaUnidades = new HashMap<Integer, List<Unidademedida>>();
		request.getSession().removeAttribute("LISTA_ITEM_ASSOCIADO_BEAN_RECEBER_ENTREGA");
		request.getSession().removeAttribute("IMPORTADOR_BEAN_RECEBER_ENTREGA");
		boolean novaEntrega = false;
		if (request.getSession().getAttribute("novaEntrega") != null){
			novaEntrega = Boolean.parseBoolean(request.getSession().getAttribute("novaEntrega").toString());
			request.getSession().removeAttribute("novaEntrega");
		}
		boolean receberEntregaByMaterialmestre = Boolean.valueOf(request.getParameter("receberEntregaByMaterialmestre") != null ?
				request.getParameter("receberEntregaByMaterialmestre") : "false");
		ordemcompra.setReceberEntregaByMaterialmestre(receberEntregaByMaterialmestre);
		
		Arquivo arquivo = null;
		Object object = request.getSession().getAttribute("ARQUIVO_XML_RECEBER_ENTREGA");
		Object chaveacesso = request.getSession().getAttribute("CHAVE_ACESSO");
		Object html_chaveacesso = request.getSession().getAttribute("HTML_CHAVE_ACESSO");
		
		if((object != null && object instanceof Arquivo) || (chaveacesso != null && !chaveacesso.equals(""))){
			ImportacaoXmlNfeBean bean = new ImportacaoXmlNfeBean();
			if (object != null && object instanceof Arquivo){
				arquivo = (Arquivo)object;
				try {
					bean = entregaService.getListaImportacao(arquivo);
				} catch (SinedException e) {
					request.addError("Arquivo XML inv�lido. (" + e.getMessage() + ")");
					SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
					return null;
				}
			}
			if (chaveacesso != null && !chaveacesso.equals("")){
				try {
					ManifestoDfe manifestoDfe = manifestoDfeService.procurarPorChaveAcesso(chaveacesso.toString());
					Arquivo arquivoNfeCompletoXml = arquivoService.loadWithContents(manifestoDfe.getArquivoNfeCompletoXml());
					bean = entregaService.getListaImportacao(arquivoNfeCompletoXml);
					
					if (bean.getRazaosocial() == null && (bean.getListaItens() == null || bean.getListaItens().isEmpty())){
						throw new SinedException("N�o foi poss�vel encontrar os dados da entrega no site da receita. Verifique se a chave de acesso est� correta e digite novamente!");
					}
				} catch (Exception e) {
					request.addError(e.getMessage());
				}
				bean.setChaveacesso(chaveacesso.toString());
			}
			
			for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
				mapaUnidades.put(it.getSequencial(), new ArrayList<Unidademedida>());
			}
			
//			REDIRECIONA PARA O M�TODO DE PREENCHIMENTO PARA A ENTRADA
			if (novaEntrega){
				Fornecedor fornecedor = new Fornecedor();
				if(bean.getCnpj() != null && !bean.getCnpj().equals("")){
					fornecedor = fornecedorService.getFornecedorByCpfCnpj(bean.getCnpj(), null);
				} else {
					fornecedor = fornecedorService.getFornecedorByCpfCnpj(null, bean.getCpf());
				}
				if(fornecedor == null){
					Pessoa pessoa = new Pessoa();
					if(bean.getCnpj() != null && !bean.getCnpj().equals("")){
						pessoa = pessoaService.findPessoaByCnpj(bean.getCnpj());
					} else {
						pessoa = pessoaService.findPessoaByCpf(bean.getCpf());
					}
					if (pessoa != null){						
						fornecedor = new Fornecedor();
						fornecedor.setCdpessoa(pessoa.getCdpessoa());
						fornecedorService.insertSomenteFornecedor(fornecedor);
					}
				}
				
				if(fornecedor != null){
					for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
						String cprod = it.getCprod();
						if(cprod != null && !cprod.equals("")){
							List<Material> listaMaterial = materialService.findByFornecedorCodigo(fornecedor, cprod);
							if(listaMaterial != null && listaMaterial.size() == 1){
								Material material = listaMaterial.get(0);
								it.setMaterial(material);
								it.setNomeMaterial(material.getAutocompleteDescription());
								if(Util.strings.isNotEmpty(it.getUnidademedidacomercial())){
									List<Unidademedida> listaUnidade = unidademedidaService.getUnidademedidaByMaterial(material);
									if(SinedUtil.isListNotEmpty(listaUnidade)){
										for(Unidademedida un: listaUnidade){
											if(it.getUnidademedidacomercial().equalsIgnoreCase(un.getSimbolo())){
												it.setUnidademedidaAssociada(un);
												it.setUnidademedidaAssociada_simbolo(un.getSimbolo());
												break;
											}
										}
									}
									mapaUnidades.get(it.getSequencial()).addAll(listaUnidade);
								}
								if(SinedUtil.isListNotEmpty(it.getListaRastro())){
									try{
										entradafiscalService.preencheLotesW3(it.getListaRastro(), material, fornecedor);
									} catch (Exception e2) {
										request.addError(e2.getMessage());
										SinedUtil.redirecionamento(request, "/suprimento/crud/Entrega");
										return null;
									}
								}
							}
						}
						//loteestoqueService.findByNumeroAndFornecedor(it.get, fornecedor);
					}
				}
				
				request.setAttribute("achouFornecedor", fornecedor != null);
				bean.setConferenciaDadosFornecedorBean(this.setaDadosConferenciaFornecedor(fornecedor, bean));
				request.setAttribute("mapaUnidades", mapaUnidades);
				request.getSession().setAttribute("IMPORTADOR_BEAN_RECEBER_ENTREGA", bean);
				request.setAttribute("permissaoListagemMaterial", Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/suprimento/crud/Material", "listagem", SinedUtil.getUsuarioLogado()));
				return new ModelAndView("process/conferenciaInserirEntrega", "bean", bean); 
			} 
//			CONTINUA FAZENDO O RECEBIMENTO NORMALMENTE
			else{ 
			
				try{
					bean.setReceberEntregaByMaterialmestre(ordemcompra.getReceberEntregaByMaterialmestre());
					List<Ordemcompra> listaOc = ordemcompraService.findOrdens(ordemcompra.getWhereIn());
					
					Money frete = new Money();
					for (Ordemcompra oc2 : listaOc) {
						if(oc2.getFrete() != null){
							frete = frete.add(oc2.getFrete());
						}
					}
					
					Ordemcompra oc = listaOc.get(0);
					oc.setWhereIn(ordemcompra.getWhereIn());
					oc.setFrete(frete);
					oc.setReceberEntregaByMaterialmestre(ordemcompra.getReceberEntregaByMaterialmestre());
					
					List<Entregamaterial> listaMaterialEntrega = new ArrayList<Entregamaterial>(entregaService.setaListaMaterial(oc, ordemcompra.getAgrupamentoMesmoMaterial()));
					
					Collections.sort(listaMaterialEntrega, new Comparator<Entregamaterial>(){
						public int compare(Entregamaterial o1, Entregamaterial o2) {
							return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
						}
					});
					
					bean.setAgrupamentoMaterial(ordemcompra.getAgrupamentoMesmoMaterial());
					bean.setWhereInOrdemcompra(ordemcompra.getWhereIn());
					bean.setListaMaterialEntrega(listaMaterialEntrega);
					
					String controller = request.getParameter("controller");
					if(controller != null && controller.contains("consultar")){
						bean.setController(controller+"&cdordemcompra="+ordemcompra.getCdordemcompra());
					} else{
						bean.setController(controller);
					}
					
					if(bean.getListaItens() == null){
						if(chaveacesso != null && !chaveacesso.equals("")){
							request.addError("N�o foi poss�vel obter informa��es relacionado ao material com a chave de acesso informada.");
							SinedUtil.redirecionamento(request, "/suprimento/crud/Ordemcompra");
							return null;
						}
					}else if(bean.getListaItens().size() != listaMaterialEntrega.size()){
						request.setAttribute("msgAlert", "A quantidade de itens da ordem de compra � diferente da quantidade de itens do XML da Nota Fiscal Eletr�nica.");
					}
					
					Fornecedor fornecedor = fornecedorService.loadForEntrada(oc.getFornecedor());
					bean.setConferenciaDadosFornecedorBean(this.setaDadosConferenciaFornecedor(fornecedor, bean));
					
					if(fornecedor != null){
						int posicao = 0;
						for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
							String cprod = it.getCprod();
							if(cprod != null && !cprod.equals("")){
								List<Material> listaMaterial = materialService.findByFornecedorCodigo(fornecedor, cprod);
								if(listaMaterial != null && listaMaterial.size() == 1){
									Material material = listaMaterial.get(0);
									
									List<Ordemcompramaterial> listaOrdemcompramaterialAssociada = new ArrayList<Ordemcompramaterial>();
									List<Ordemcompramaterial> listaOrdemcompramaterial = ordemcompraService.getListaOrdemcompramaterial(listaOc);
									Unidademedida unidademedidaOC = null;
									Ordemcompramaterial ordemcompramaterialAssociada = null;
									for (Ordemcompramaterial ordemcompramaterial : listaOrdemcompramaterial) {
										if(ordemcompramaterial.getMaterial() != null && 
												ordemcompramaterial.getMaterial().equals(material)){
											unidademedidaOC = ordemcompramaterial.getUnidademedida();
											ordemcompramaterialAssociada = ordemcompramaterial;
											listaOrdemcompramaterialAssociada.add(ordemcompramaterial);
										}
									}
									
//									if(listaOrdemcompramaterialAssociada != null && listaOrdemcompramaterialAssociada.size() > 0){
									if(listaOrdemcompramaterialAssociada != null && listaOrdemcompramaterialAssociada.size() == 1){
										boolean associar = true;
										for(int i = 0; i < bean.getListaItens().size(); i++){
											if(posicao != i){
												if(bean.getListaItens().get(i).getCprod() != null && 
														bean.getListaItens().get(i).getCprod().equals(cprod)){
													associar = false;
													break;
												}
											}
										}
										if(associar){
											if(Util.strings.isNotEmpty(it.getUnidademedidacomercial())){
												List<Unidademedida> listaUnidade = unidademedidaService.getUnidademedidaByMaterial(material);
												if(SinedUtil.isListNotEmpty(listaUnidade)){
													associar = false;
													for(Unidademedida un: listaUnidade){
														if(it.getUnidademedidacomercial().equalsIgnoreCase(un.getSimbolo())){
															it.setUnidademedidaAssociada(un);
															it.setUnidademedidaAssociada_simbolo(un.getSimbolo());
															associar = true;
															break;
														}
													}
												}
												mapaUnidades.get(it.getSequencial()).addAll(listaUnidade);
											}
											
											if(associar){
												it.setMaterial(material);
												it.setNomeMaterial(material.getAutocompleteDescription());
												it.setWhereInOrdemcompramaterial(CollectionsUtil.listAndConcatenate(listaOrdemcompramaterialAssociada, "cdordemcompramaterial", ","));
												
												ImportacaoXmlNfeItemAssociadoBean beanXmlNfeItem = new ImportacaoXmlNfeItemAssociadoBean(material, it.getWhereInOrdemcompramaterial(), it.getQtde());
												it.setListaItemAssociado(new ArrayList<ImportacaoXmlNfeItemAssociadoBean>());
												
												if(it.getQtde() != null && it.getQtde() > 0 && it.getUnidademedidaAssociada() != null && unidademedidaOC != null){
													Entregamaterial emConversao = new Entregamaterial();
													emConversao.setMaterial(new Material(material.getCdmaterial()));
													emConversao.setUnidademedidacomercial(unidademedidaOC);
													emConversao.setUnidademedidacomercialAnterior(it.getUnidademedidaAssociada());
													emConversao.setXmlqtdeinformada(it.getQtde());
													
													Double qtdesol = entregaService.getQtdeUnidadeOrdemcompra(emConversao);
													Double qtdPedida = ordemcompramaterialAssociada != null ? entregaService.getQtdPedidaRestanteOCM(ordemcompramaterialAssociada) : qtdesol;
													
													if(qtdesol != null){
														if(qtdesol >= qtdPedida){
															beanXmlNfeItem.setQtdeassociada(qtdPedida);
														}else {
															beanXmlNfeItem.setQtdeassociada(qtdesol);
														}
														
														emConversao.setUnidademedidacomercial(it.getUnidademedidaAssociada());
														emConversao.setUnidademedidacomercialAnterior(unidademedidaOC);
														emConversao.setXmlqtdeinformada(beanXmlNfeItem.getQtdeassociada());
														beanXmlNfeItem.setQtdeassociada(SinedUtil.round(entregaService.getQtdeUnidadeOrdemcompra(emConversao), 20));
													}else {
														beanXmlNfeItem.setQtdeassociada(it.getQtde());
													}
												}
												it.getListaItemAssociado().add(beanXmlNfeItem);
											}
										}
									}
								}
							}
							posicao++;
						}
					}
					
					List<ImportacaoXmlNfeItemAssociadoBean> listaItemAssociadoBean = new ArrayList<ImportacaoXmlNfeItemAssociadoBean>();
					for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
						if(SinedUtil.isListNotEmpty(it.getListaItemAssociado())){
							listaItemAssociadoBean.addAll(it.getListaItemAssociado());
						}
					}
					request.getSession().setAttribute("LISTA_ITEM_ASSOCIADO_BEAN_RECEBER_ENTREGA", listaItemAssociadoBean);
					
					request.getSession().setAttribute("IMPORTADOR_BEAN_RECEBER_ENTREGA", bean);
					request.setAttribute("permissaoListagemMaterial", Neo.getApplicationContext().getAuthorizationManager().isAuthorized("/suprimento/crud/Material", "listagem", SinedUtil.getUsuarioLogado()));
					request.setAttribute("mapaUnidades", mapaUnidades);
					if(ordemcompra != null){
						bean.setCdordemcompra(ordemcompra.getCdordemcompra());
					}
					return new ModelAndView("process/conferenciaReceberEntrega", "bean", bean);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} 
		
		return continueOnAction("carregaOrdem", ordemcompra);
	}
	
	private ConferenciaDadosFornecedorBean setaDadosConferenciaFornecedor(Fornecedor f, ImportacaoXmlNfeBean x) {
		ConferenciaDadosFornecedorBean bean = new ConferenciaDadosFornecedorBean();
		
		bean.setTipopessoa_xml(x.getCnpj() != null ? Tipopessoa.PESSOA_JURIDICA : Tipopessoa.PESSOA_FISICA);
		bean.setCnpj_xml(x.getCnpj());
		bean.setCpf_xml(x.getCpf());
		bean.setRazaosocial_xml(x.getRazaosocial());
		bean.setNomefantasia_xml(x.getNomefantasia());
		bean.setIe_xml(x.getIe());
		bean.setIm_xml(x.getIm());
		if (x.getCuf() != null)
			bean.setUf_xml(ufService.findForCodigo(x.getCuf()));
		
		if(f != null){
			bean.setCdpessoa(f.getCdpessoa());
			bean.setTipopessoa_bd(f.getTipopessoa());
			bean.setCnpj_bd(f.getCnpj());
			bean.setCpf_bd(f.getCpf());
			bean.setRazaosocial_bd(f.getRazaosocial());
			bean.setNomefantasia_bd(f.getNome());
			bean.setIe_bd(f.getInscricaoestadual());
			bean.setIm_bd(f.getInscricaomunicipal());
		}
		
//		bean.setLogradouro_xml(String logradouro_xml);
//		bean.setLogradouro_bd(String logradouro_bd);
//		bean.setNumero_xml(String numero_xml);
//		bean.setNumero_bd(String numero_bd);
//		bean.setComplemento_xml(String complemento_xml);
//		bean.setComplemento_bd(String complemento_bd);
//		bean.setBairro_xml(String bairro_xml);
//		bean.setBairro_bd(String bairro_bd);
//		bean.setMunicipio_xml(Municipio municipio_xml);
//		bean.setMunicipio_bd(Municipio municipio_bd);
//		bean.setUf_xml(Uf uf_xml);
//		bean.setUf_bd(Uf uf_bd);
//		bean.setCep_xml(Cep cep_xml);
//		bean.setCep_bd(Cep cep_bd);
		
		return bean;
	}
	
	public ModelAndView abrePopUpAtualizacaoMaterial(WebRequestContext request, ImportacaoXmlNfeItemBean item){
		
		Object object = request.getSession().getAttribute("IMPORTADOR_BEAN_RECEBER_ENTREGA");
		if(object != null && object instanceof ImportacaoXmlNfeBean){
			ImportacaoXmlNfeBean bean = (ImportacaoXmlNfeBean)object;
			
			ImportacaoXmlNfeAtualizacaoMaterialBean atualizaBean = new ImportacaoXmlNfeAtualizacaoMaterialBean();
			atualizaBean.setIndex(item.getIndex());
			atualizaBean.setMaterial(item.getMaterial());
			
			Material material = materialService.loadForNFProduto(item.getMaterial());
			
			ImportacaoXmlNfeItemBean it = null;
			List<ImportacaoXmlNfeItemBean> listaItens = bean.getListaItens();
			for (ImportacaoXmlNfeItemBean itBean : listaItens) {
				if(itBean.getSequencial() != null && itBean.getSequencial().equals(item.getSequencial())){
					it = itBean;
					break;
				}
			}
			
			if(it == null){
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			atualizaBean.setTipoitemsped(material.getTipoitemsped());
			
			atualizaBean.setNcmcapitulo_bd(material.getNcmcapitulo());
			atualizaBean.setCodigobarras_bd(material.getCodigobarras());
			atualizaBean.setNcmcompleto_bd(material.getNcmcompleto());
			atualizaBean.setExtipi_bd(material.getExtipi());
			atualizaBean.setCodlistaservico_bd(material.getCodlistaservico());
			
			if(it.getNcm() != null && it.getNcm().trim().length() > 1){
				String ncm = it.getNcm().trim();
				if(ncm.length() > 2){
					atualizaBean.setNcmcompleto_xml(ncm);
				}
				Integer id_ncm = Integer.parseInt(ncm.substring(0, 2));
				Ncmcapitulo ncmcapitulo = ncmcapituloService.load(new Ncmcapitulo(id_ncm));
				atualizaBean.setNcmcapitulo_xml(ncmcapitulo);
			}
			
			atualizaBean.setCodigobarras_xml(it.getCean());
			atualizaBean.setExtipi_xml(it.getExtipi());
			atualizaBean.setCodlistaservico_xml(it.getCListServ());
			
			request.setAttribute("atualizaBean", atualizaBean);
		} else {
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		return new ModelAndView("direct:/process/popup/popupAtualizacaoMaterial");
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView abrePopUpAssociacao(WebRequestContext request, ImportacaoXmlNfeItemBean item){
		
		Object object = request.getSession().getAttribute("IMPORTADOR_BEAN_RECEBER_ENTREGA");
		if(object != null && object instanceof ImportacaoXmlNfeBean){
			ImportacaoXmlNfeBean bean = (ImportacaoXmlNfeBean)object;
			
			List<Entregamaterial> listaMaterialEntrega = new ArrayList<Entregamaterial>();
			listaMaterialEntrega.addAll(bean.getListaMaterialEntrega());
			
			List<ImportacaoXmlNfeItemBean> listaItens = bean.getListaItens();
			for (ImportacaoXmlNfeItemBean itBean : listaItens) {
				if(itBean.getSequencial() != null && itBean.getSequencial().equals(item.getSequencial())){
					itBean.setIndex(item.getIndex());
					request.setAttribute("itBean", itBean);
					break;
				}
			}
			
			List<ImportacaoXmlNfeItemAssociadoBean> listaItemAssociadoBean = null;
			Object objListaItemAssociadoBean = request.getSession().getAttribute("LISTA_ITEM_ASSOCIADO_BEAN_RECEBER_ENTREGA");
			if(objListaItemAssociadoBean != null && objListaItemAssociadoBean instanceof List){
				listaItemAssociadoBean = (List<ImportacaoXmlNfeItemAssociadoBean>) objListaItemAssociadoBean;
				
				for (Iterator<Entregamaterial> iterator = listaMaterialEntrega.iterator(); iterator.hasNext();) {
					Entregamaterial entregamaterial = iterator.next();
					entregamaterial.setQtdeanteriorassociada(0d);
					Double qtdeUtilizada = 0d;
					for(ImportacaoXmlNfeItemAssociadoBean itemAssociadoBean : listaItemAssociadoBean){
						if(StringUtils.isNotEmpty(entregamaterial.getWhereInOrdemcompramaterial()) && 
								StringUtils.isNotEmpty(itemAssociadoBean.getWhereInOrdemcompramaterial()) &&
								entregamaterial.getWhereInOrdemcompramaterial().equals(itemAssociadoBean.getWhereInOrdemcompramaterial())){
							if(itemAssociadoBean.getQtdeassociada() != null && itemAssociadoBean.getQtdeassociada() > 0){
								qtdeUtilizada += itemAssociadoBean.getQtdeassociada();
							}
						}
					}
					if(qtdeUtilizada >= entregamaterial.getQtde()){
						iterator.remove();
					}
					entregamaterial.setQtdeanteriorassociada(qtdeUtilizada);
				}
			}
			
			for (Entregamaterial entregamaterial : listaMaterialEntrega) {
				entregamaterial.getMaterial().setNomescape(entregamaterial.getMaterial().getNome().replaceAll("'", "\\\\'").replaceAll("\"", ""));
			}
			
			
			request.setAttribute("listaMaterialEntrega", listaMaterialEntrega);
		} else {
			SinedUtil.fechaPopUp(request);
			return null;
		}
				
		return new ModelAndView("direct:/process/popup/popupAssociacaoMaterial");
	}
	
	@SuppressWarnings("unchecked")
	public void associarMaterial(WebRequestContext request, ImportacaoXmlNfeItemBean bean){
		List<ImportacaoXmlNfeItemAssociadoBean> listaItemAssociadoBean = null;
		Object objListaItemAssociadoBean = request.getSession().getAttribute("LISTA_ITEM_ASSOCIADO_BEAN_RECEBER_ENTREGA");
		if(objListaItemAssociadoBean != null && objListaItemAssociadoBean instanceof List){
			listaItemAssociadoBean = (List<ImportacaoXmlNfeItemAssociadoBean>) objListaItemAssociadoBean;
		} else {
			listaItemAssociadoBean = new ArrayList<ImportacaoXmlNfeItemAssociadoBean>();
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaItemAssociado())){
			for(ImportacaoXmlNfeItemAssociadoBean item : bean.getListaItemAssociado()){
				if(item.getXmlqtdeinformada() != null){
					item.setQtdeassociada(item.getXmlqtdeinformada());
				}
			}
		}
		listaItemAssociadoBean.addAll(bean.getListaItemAssociado());
		
		request.getSession().setAttribute("LISTA_ITEM_ASSOCIADO_BEAN_RECEBER_ENTREGA", listaItemAssociadoBean);
	}
	
	@SuppressWarnings("unchecked")
	public void deassociarMaterial(WebRequestContext request, ImportacaoXmlNfeItemBean bean){
		List<ImportacaoXmlNfeItemAssociadoBean> listaItemAssociadoBean = null;
		Object objListaItemAssociadoBean = request.getSession().getAttribute("LISTA_ITEM_ASSOCIADO_BEAN_RECEBER_ENTREGA");
		if(objListaItemAssociadoBean != null && objListaItemAssociadoBean instanceof List){
			listaItemAssociadoBean = (List<ImportacaoXmlNfeItemAssociadoBean>) objListaItemAssociadoBean;
			if(SinedUtil.isListNotEmpty(bean.getListaItemAssociado())){
				for(ImportacaoXmlNfeItemAssociadoBean item : bean.getListaItemAssociado()){
					listaItemAssociadoBean.remove(item);
				}
			}
			request.getSession().setAttribute("LISTA_ITEM_ASSOCIADO_BEAN_RECEBER_ENTREGA", listaItemAssociadoBean);
		}
		
	}
	
	
	/**
	 * Action para a gera��o da entrega a partir do crud de ordem de compra.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#setaValoresEntrega
	 * @see br.com.linkcom.sined.geral.service.EntregaService#setaListaMaterial
	 * @see br.com.linkcom.sined.geral.service.EntregaService#setaPagamentos
	 * @see br.com.linkcom.sined.geral.service.LocalarmazenagemService#findAtivos(br.com.linkcom.sined.geral.bean.Localarmazenagem)
	 * @see #getEntradaModelAndView(WebRequestContext, Entrega)
	 * @param request
	 * @param ordemcompra
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarEntrega(WebRequestContext request, Ordemcompra ordemcompra) throws Exception{
		Entrega entrega = new Entrega();
		Entregadocumento entregadocumento = new Entregadocumento();
		
		entregaService.setaValoresEntregadocumento(ordemcompra, entregadocumento);
		entregadocumento.setListaEntregamaterial(entregaService.setaListaMaterial(ordemcompra, ordemcompra.getAgrupamentoMesmoMaterial()));
		if(ordemcompra.getImportacaoXmlNfeBean() == null){
			entregadocumento.setValor(entregadocumento.getValortotaldocumento());
		}
		
		entrega.setFaturamentocliente(entregadocumento.getSimplesremessa());
		
		entradafiscalService.addInfAtributo(request, entregadocumento);
		
		List<Entregadocumento> listaDocumento = new ListSet<Entregadocumento>(Entregadocumento.class);
		listaDocumento.add(entregadocumento);
		entrega.setListaEntregadocumento(listaDocumento);
		entrega.setLocalarmazenagem(ordemcompra.getLocalarmazenagem());
		
		setInfoForTemplate(request, entrega);
		setEntradaDefaultInfo(request, entrega);
		request.setAttribute("tamanholista", entregadocumento.getListaEntregamaterial() != null ? entregadocumento.getListaEntregamaterial().size() : 0);
		request.setAttribute("locaisAtivos", localarmazenagemService.findAtivos(entrega != null ? entrega.getLocalarmazenagem() : null));
		
		String controller = request.getParameter("controller");
		if(controller == null || controller.equals("")){
			controller = ordemcompra.getController();
		}
		
		controller = controller.replaceAll("&cdordemcompra=null", "");
		if(controller != null && controller.contains("consultar") && !controller.contains("&cdordemcompra=")){
			entrega.setController(controller+"&cdordemcompra="+ordemcompra.getCdordemcompra());
		} else{
			entrega.setController(controller);
		}
		
		this.calculaQtdeEntrega(entrega, true);
		boolean gerarPagamentosByXml = ordemcompra.getPrazopagamento() == null || !parametrogeralService.getBoolean(Parametrogeral.CONSIDERARPRAZO_ORDEMCOMPRA);
		if(ordemcompra.getImportacaoXmlNfeBean() != null){
			entradafiscalService.setaValoresImportacao(entregadocumento, ordemcompra.getImportacaoXmlNfeBean(), gerarPagamentosByXml);
			entrega.setFromImportacaoArquivo(true);
		}
		
		if ((!gerarPagamentosByXml) || (ordemcompra.getImportacaoXmlNfeBean() == null ||
				ordemcompra.getImportacaoXmlNfeBean().getListaDuplicatas() == null ||
				ordemcompra.getImportacaoXmlNfeBean().getListaDuplicatas().size() == 0)) {		
			entregadocumento.setListadocumento(entregaService.setaPagamentos(ordemcompra,entregadocumento));
		}
		
		if(ordemcompra != null && ordemcompra.getWhereIn() != null){
			String whereIn = ordemcompra.getWhereIn();
			String[] ids = whereIn.split(",");
			if(ordemcompra.getImportacaoXmlNfeBean() == null){
				this.setaFreteDescontoIcmsst(entregadocumento, whereIn);
			}
			Set<Ordemcompraentrega> listaOrdemcompraentrega = new ListSet<Ordemcompraentrega>(Ordemcompraentrega.class);
			Ordemcompraentrega ordemcompraentrega;
			
			for (int i = 0; i < ids.length; i++) {
				ordemcompraentrega = new Ordemcompraentrega();
				ordemcompraentrega.setOrdemcompra(new Ordemcompra(Integer.parseInt(ids[i])));
				
				listaOrdemcompraentrega.add(ordemcompraentrega);
			}
			entrega.setListaOrdemcompraentrega(listaOrdemcompraentrega);
		}
		
		if(entrega.getIdentificadortela() == null) {
			entrega.setIdentificadortela(new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis()));
		}
		request.getSession().setAttribute("listaEntregadocumento" + entrega.getIdentificadortela(), listaDocumento);
		request.setAttribute("modulo", "suprimento");
		
		if(entregadocumento.getValorfrete() == null || entregadocumento.getValorfrete().getValue().doubleValue() == 0)
			entregadocumento.setValorfrete(entregadocumento.getValorTotalFrete());

		entrega.setEmpresa(ordemcompra.getEmpresa());
		request.setAttribute("empresasAtivos", empresaService.findAtivos(entrega.getEmpresa()));
		
		return getEntradaModelAndView(request, entrega);
	}
	
	private void setaFreteDescontoIcmsst(Entregadocumento entregadocumento, String whereIn) {
		Money frete = new Money(), desconto = new Money(), icmsst = new Money();
		List<Ordemcompra> listaOrdemcompra = ordemcompraService.findOrdens(whereIn);
		
		for (Ordemcompra oc : listaOrdemcompra) {
			frete = frete.add(oc.getFrete());
			desconto = desconto.add(oc.getDesconto());
			icmsst = icmsst.add(oc.getValoricmsst());
		}
		
		entregadocumento.setValorfrete(frete);
		entregadocumento.setValordesconto(desconto);
		entregadocumento.setValor(entregadocumento.getValor().add(frete).subtract(desconto).add(icmsst));
		
		if(frete.getValue().doubleValue() > 0 ||
				icmsst.getValue().doubleValue() > 0){
			Set<Entregamaterial> listaEntregamaterial = entregadocumento.getListaEntregamaterial();
			
			Double qtdetotal = 0d;
			for (Entregamaterial entregamaterial : listaEntregamaterial) {
				qtdetotal += entregamaterial.getQtde();
			}
			
			Double fretetotal = 0.0;
			Double descontototal = 0.0;
			for (Entregamaterial entregamaterial : listaEntregamaterial) {
				if(qtdetotal > 0 && entregamaterial.getQtde() != null){
					if(frete != null && frete.getValue().doubleValue() > 0){
						BigDecimal roundFrete = SinedUtil.roundFloor(new BigDecimal(entregamaterial.getQtde() * frete.getValue().doubleValue() / qtdetotal), 2);
						entregamaterial.setValorfrete(new Money(roundFrete));
					}
					if(desconto != null && desconto.getValue().doubleValue() > 0){
						BigDecimal roundDesconto = SinedUtil.roundFloor(new BigDecimal(entregamaterial.getQtde() * desconto.getValue().doubleValue() / qtdetotal), 2);
						entregamaterial.setValordesconto(new Money(roundDesconto));
					}
					if(icmsst != null && icmsst.getValue().doubleValue() > 0){
						entregamaterial.setValoricmsst(new Money(entregamaterial.getQtde() * icmsst.getValue().doubleValue() / qtdetotal));
					}
				}
				
				fretetotal += entregamaterial.getValorfrete() != null ? entregamaterial.getValorfrete().getValue().doubleValue() : 0d;
				descontototal += entregamaterial.getValordesconto() != null ? entregamaterial.getValordesconto().getValue().doubleValue() : 0d;
			}
			
			if(fretetotal.doubleValue() != frete.getValue().doubleValue()){
				Entregamaterial entreg = listaEntregamaterial.iterator().next();
				if(entreg != null){
					Money valorfrete = entreg.getValorfrete();
					entreg.setValorfrete(valorfrete.add(new Money(frete.getValue().doubleValue() - fretetotal)));
				}
			}
			
			if(descontototal.doubleValue() != desconto.getValue().doubleValue() && desconto.getValue().doubleValue() > 0){
				Entregamaterial entreg = listaEntregamaterial.iterator().next();
				if(entreg != null){
					Money valordesconto = entreg.getValordesconto();
					entreg.setValordesconto(valordesconto.add(new Money(desconto.getValue().doubleValue() - descontototal)));
				}
			}
			if(descontototal != null && descontototal > 0){
				entregadocumento.setValordesconto(new Money(descontototal));
				entregadocumento.setValor(entregadocumento.getValortotaldocumento());
			}
			
		}
	}
	
	/**
	 * Carrega a lista de materiais a serem baixados e se encontrar alguma 
	 * irregularidade redireciona para a p�gina de pend�ncias, se n�o baixar as entregas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#loadWithSituacao
	 * @see br.com.linkcom.sined.geral.service.EntregaService#findForBaixa
	 * @see br.com.linkcom.sined.geral.service.EntregaService#setarItemDefault
	 * @see br.com.linkcom.sined.geral.service.EntregaService#calculoQuantidades
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView doBaixar(WebRequestContext request){
		
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		List<Entrega> listaEntrega = entregaService.loadWithSituacao(itensSelecionados);
		
		Questionario questionario = questionarioService.carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario.FORNECEDOR);
		for (Entrega entrega : listaEntrega) {
			Boolean simplesremessa = Boolean.TRUE;
			if(questionario != null 
					&& questionario.getAvaliarrecebimento() != null
					&& questionario.getObrigaavaliacaoreceber() != null
					&& questionario.getObrigaavaliacaoreceber()
					&& questionario.getAvaliarrecebimento()){
				StringBuilder fornecedores = new StringBuilder();
				for (int i = 0; i < entrega.getListaEntregadocumento().size(); i++) {
					if (entrega.getListaEntregadocumento().get(i).getFornecedorAvaliado() == null || !entrega.getListaEntregadocumento().get(i).getFornecedorAvaliado()) {
						if (fornecedores.length() > 0) {
							fornecedores.append(", " + entrega.getListaEntregadocumento().get(i).getFornecedor().getNome());
						} else {
							fornecedores.append(entrega.getListaEntregadocumento().get(i).getFornecedor().getNome());
						}
					}
				}
				if (fornecedores.length() > 0) {
					request.addError("Aten��o! � obrigat�rio a avalia��o do(s) fornecedor(es) " + fornecedores.toString() + " para faturar e baixar.");
					return getControllerModelAndView(request);
				}
			} else if(entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
				for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
					if(entregadocumento.getSimplesremessa() == null || !entregadocumento.getSimplesremessa()){
						simplesremessa = Boolean.FALSE;
					}
				}
			}
			if(!simplesremessa && (entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) &&
				  !entrega.getFaturamentocliente())){
				request.addError("Para baixar, a entrega deve estar faturada ou caso 'Em aberto' possuir faturamento no cliente.");
				return getControllerModelAndView(request);
			}
				  
			if(entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA) || 
					entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)){
				request.addError("Para baixar, a entrega deve estar faturada ou caso 'Em aberto' possuir faturamento no cliente.");
				return getControllerModelAndView(request);
			}
				
			if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto("DTBAIXA".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.RECEBIMENTO_MOVIMENTACAO_DATA)) ? SinedDateUtils.currentDate() : entrega.getDtentrega(), entrega.getEmpresa(), entrega.getLocalarmazenagem(), null)) {
				request.addError("N�o � poss�vel realizar a baixa, pois j� existe invent�rio compreendendo a data do registro de movimenta��o a ser criado.");
				return getControllerModelAndView(request);
			}
		}
		
		BaixarEntregaBean bean = new BaixarEntregaBean();
		List<BaixarEntregaItemBean> listaPendentes = new ArrayList<BaixarEntregaItemBean>();
		List<BaixarEntregaItemBean> listaEncontrados = new ArrayList<BaixarEntregaItemBean>();

		List<Entrega> lista = entregaService.findForBaixa(itensSelecionados);
		
		boolean existMaterialitemByMaterialgrademestreEntrega = false;
		for (Entrega entrega : lista) {
			for(Entregadocumento ed : entrega.getListaEntregadocumento()){
				if (ed.getListaEntregamaterial() != null) {
					for (Entregamaterial entregamaterial : ed.getListaEntregamaterial()) {
						BaixarEntregaItemBean baixarEntregaItemBean = new BaixarEntregaItemBean();				
						if(entregamaterial.getMaterial() != null && entregamaterial.getMaterial().getMaterialgrupo() != null && 
								entregamaterial.getMaterial().getMaterialmestregrade() == null &&
								entregamaterial.getMaterial().getMaterialgrupo().getGradeestoquetipo() != null){
							if(StringUtils.isBlank(entregamaterial.getWhereInMaterialItenGrade())){
								request.addError("Nenhum item de grade foi selecionado para o material "+entregamaterial.getMaterial().getNome()+".");
								return getControllerModelAndView(request);
							}
						}
						boolean iObrigaLote = materialService.obrigarLote(entregamaterial.getMaterial());
						if(iObrigaLote && Util.objects.isNotPersistent(entregamaterial.getLoteestoque()) && SinedUtil.isListEmpty(entregamaterial.getListaEntregamateriallote())){
							request.addError("O material "+entregamaterial.getMaterial().getNome()+" n�o possui lote informado.");
							return getControllerModelAndView(request);
						}
						if(SinedUtil.isListNotEmpty(entregamaterial.getListaEntregamateriallote())){
							for(Entregamateriallote entregamateriallote : entregamaterial.getListaEntregamateriallote()){
								entregamaterial.setLoteestoqueAux(entregamateriallote.getLoteestoque());
								entregamaterial.setQtdeAux(entregamateriallote.getQtde());	
								baixarEntregaItemBean = criaBaixarEntregaItemBean(entrega, ed, entregamaterial, existMaterialitemByMaterialgrademestreEntrega, listaPendentes, listaEncontrados, false);
							}
						}else if(entregamaterial.getMaterial() != null && Boolean.TRUE.equals(entregamaterial.getMaterial().getVendapromocional()) && SinedUtil.isListNotEmpty(entregamaterial.getMaterial().getListaMaterialrelacionado())){
							Boolean desmontarKit = false;
							baixarEntregaItemBean = criaBaixarEntregaItemBean(entrega, ed, entregamaterial, existMaterialitemByMaterialgrademestreEntrega, listaPendentes, listaEncontrados, true);
							for (Materialrelacionado materialrelacionado : entregamaterial.getMaterial().getListaMaterialrelacionado()) {
								if(Boolean.TRUE.equals(materialrelacionado.getDesmontarKitRecebimento())){
									desmontarKit = true;
									BaixarEntregaItemBean baixarEntregaItemBeanAux = new BaixarEntregaItemBean(baixarEntregaItemBean);
									baixarEntregaItemBeanAux.setMaterial(materialrelacionado.getMaterialpromocao());
									baixarEntregaItemBeanAux.setQuantidade(entregamaterial.getQtde() * materialrelacionado.getQuantidade());
									baixarEntregaItemBeanAux.setMaterialOrigem(entregamaterial.getMaterial());
									baixarEntregaItemBeanAux.setQuantidadePrincipal(entregamaterial.getQtde());								
									Money valorUnitario = new Money(entregamaterial.getValorunitario());
									
									if(materialrelacionado.getPorcentagemCustoUnitario() == null){										
										request.addError("O campo de % Unitario de Custo do  material "+entregamaterial.getMaterial().getCdmaterial()+"  "+
												entregamaterial.getMaterial().getNome()+" n�o pode ser vazio.");					
										return getControllerModelAndView(request);
									}else {
										baixarEntregaItemBeanAux.setValorunitario(valorUnitario.multiply(materialrelacionado.getPorcentagemCustoUnitario() / 100).getValue().doubleValue());
									}
									
									baixarEntregaItemBeanAux.setUnidademedidacomercial(materialrelacionado.getMaterialpromocao().getUnidademedida());
									
									listaEncontrados.add(baixarEntregaItemBeanAux);
								}
							}	
							if(!desmontarKit){
								listaEncontrados.add(baixarEntregaItemBean);
							}
						}else {
							baixarEntregaItemBean = criaBaixarEntregaItemBean(entrega, ed, entregamaterial, existMaterialitemByMaterialgrademestreEntrega, listaPendentes, listaEncontrados, false);
						}
						if(SinedUtil.isRateioMovimentacaoEstoque() && entregamaterial.getMaterial() != null && (entregamaterial.getMaterial().getMaterialRateioEstoque() == null || 
								entregamaterial.getMaterial().getMaterialRateioEstoque().getContaGerencialEntrada() == null || 
								entregamaterial.getCentrocusto() == null)){
							request.addError("Para realizar a baixa e dar entrada do material no estoque � obrigat�rio o preenchimento da conta gerencial de entrada no cadastro de material e o preenchimento do centro de custo nos itens da entrada fiscal.");
							return getControllerModelAndView(request);
						}
					}
				}				
			}
		}	
		
		bean.setListaPendentes(entregaService.getListaCorrigida(listaPendentes));
		bean.setListaResolvidos(entregaService.getListaCorrigida(listaEncontrados));	
		bean.setIds(itensSelecionados);		
		
		Boolean isQtdeCorretaItensGrade = null;
		if(existMaterialitemByMaterialgrademestreEntrega){
			if(bean.getListaResolvidos() != null && !bean.getListaResolvidos().isEmpty()){
				for (BaixarEntregaItemBean baixarEntregaItemBean : bean.getListaResolvidos()) {
					if(baixarEntregaItemBean.getQuantidade() != null && 
							baixarEntregaItemBean.getListaMaterialitemmestregrade() != null && 
							!baixarEntregaItemBean.getListaMaterialitemmestregrade().isEmpty()){
						Double qtdeTotal = baixarEntregaItemBean.getQtdeTotalItens();
						if(baixarEntregaItemBean.getUnidademedidacomercial() != null && baixarEntregaItemBean.getUnidademedidacomercial().getCasasdecimaisestoque() != null){
							qtdeTotal = SinedUtil.roundByUnidademedida(qtdeTotal, baixarEntregaItemBean.getUnidademedidacomercial());
						}else {
							qtdeTotal = SinedUtil.roundByParametro(qtdeTotal);
						}
						if(qtdeTotal != null && qtdeTotal.compareTo(baixarEntregaItemBean.getQuantidade()) == 0){
							if(isQtdeCorretaItensGrade == null){
								isQtdeCorretaItensGrade = true;
							}
						}else {
							isQtdeCorretaItensGrade = false;
						}
					}
				}
			}
			if(bean.getListaPendentes() != null && !bean.getListaPendentes().isEmpty()){
				for (BaixarEntregaItemBean baixarEntregaItemBean : bean.getListaPendentes()) {
					if(baixarEntregaItemBean.getQuantidade() != null && 
							baixarEntregaItemBean.getListaMaterialitemmestregrade() != null && 
							!baixarEntregaItemBean.getListaMaterialitemmestregrade().isEmpty()){
						Double qtdeTotal = baixarEntregaItemBean.getQtdeTotalItens();
						if(baixarEntregaItemBean.getUnidademedidacomercial() != null && baixarEntregaItemBean.getUnidademedidacomercial().getCasasdecimaisestoque() != null){
							qtdeTotal = SinedUtil.roundByUnidademedida(qtdeTotal, baixarEntregaItemBean.getUnidademedidacomercial());
						}else {
							qtdeTotal = SinedUtil.roundByParametro(qtdeTotal);
						}
						if(qtdeTotal != null && qtdeTotal.compareTo(baixarEntregaItemBean.getQuantidade()) == 0){
							if(isQtdeCorretaItensGrade == null){
								isQtdeCorretaItensGrade = true;
							}
						}else {
							isQtdeCorretaItensGrade = false;
						}
					}
				}
			}
		}
			
		String controller = request.getParameter("controller");
		if(controller.contains("consultar")){
			bean.setController(controller+"&cdentrega="+bean.getIds());
		} else{
			bean.setController(controller);
		}
		if (bean.getListaPendentes() != null && bean.getListaPendentes().size() > 0) {
			return new ModelAndView("process/baixarEntrega","bean",bean);
		} else if(existMaterialitemByMaterialgrademestreEntrega && (isQtdeCorretaItensGrade == null || !isQtdeCorretaItensGrade)){
			return new ModelAndView("process/baixarEntregaItemmestregrade", "bean", bean);
		} else {
			return continueOnAction("saveBaixar", bean);
		}

	}
	
	/**
	* M�todo que cria o bean BaixarEntregaItemBean
	*
	* @param entrega
	* @param ed
	* @param entregamaterial
	* @param existMaterialitemByMaterialgrademestreEntrega
	* @param listaPendentes
	* @param listaEncontrados
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	private BaixarEntregaItemBean criaBaixarEntregaItemBean(Entrega entrega, Entregadocumento ed, Entregamaterial entregamaterial, Boolean existMaterialitemByMaterialgrademestreEntrega, 
			List<BaixarEntregaItemBean> listaPendentes, List<BaixarEntregaItemBean> listaEncontrados, boolean desmontarKit) {
		BaixarEntregaItemBean itemBean = new BaixarEntregaItemBean();
		
		entregaService.setarItemDefault(itemBean, entrega, entregamaterial);
		
		if(entregamaterial.getUnidademedidacomercial() != null && entregamaterial.getMaterial() != null && 
				entregamaterial.getMaterial().getUnidademedida() != null &&
				!entregamaterial.getMaterial().getUnidademedida().equals(entregamaterial.getUnidademedidacomercial())){
			Double qtde = unidademedidaService.converteQtdeUnidademedida(entregamaterial.getMaterial().getUnidademedida(), 
								entregamaterial.getQtde(),
								entregamaterial.getUnidademedidacomercial(),
								entregamaterial.getMaterial(),null,1.0);
			itemBean.setQuantidadePrincipal(qtde);
		}
		
		if(entregamaterial.getMaterial() != null && entregamaterial.getMaterial().getMaterialgrupo() != null && 
				entregamaterial.getMaterial().getMaterialmestregrade() == null &&
				entregamaterial.getMaterial().getMaterialgrupo().getGradeestoquetipo() != null){
			boolean existMaterialitemByMaterialgrademestre = materialService.existMaterialitemByMaterialgrademestre(entregamaterial.getMaterial());
			if(existMaterialitemByMaterialgrademestre){
				existMaterialitemByMaterialgrademestreEntrega = true;
				itemBean.setListaMaterialitemmestregrade(entregaService.montaMaterialitemmestregrade(materialService.findMaterialitemByMaterialmestregrade(entregamaterial.getMaterial(), entregamaterial.getWhereInMaterialItenGrade())));
				itemBean.setWhereInMaterialItenGrade(entregamaterial.getWhereInMaterialItenGrade());
			}
			
		}
		
		itemBean.setEntregadocumento(ed);
		if ((entregamaterial.getMaterial().getProduto() && entregamaterial.getMaterial().getPatrimonio()) || 
				(entregamaterial.getMaterial().getProduto() && entregamaterial.getMaterial().getEpi()) ||
				(entregamaterial.getMaterial().getEpi() && entregamaterial.getMaterial().getPatrimonio())) {
			if(!desmontarKit){
				entregaService.calculoQuantidades(listaPendentes, listaEncontrados, itemBean, entrega, entregamaterial);				
			}
		} else {
			if(!desmontarKit){
				listaEncontrados.add(itemBean);				
			}
		}
		
		return itemBean;
	}
	
	public ModelAndView doSaveBaixar(WebRequestContext request, BaixarEntregaBean bean){
		
		boolean existMaterialitemByMaterialgrademestreEntrega = false;
		for (BaixarEntregaItemBean itemBean : bean.getListaResolvidos()) {
			if(itemBean.getMaterial() != null && itemBean.getMaterial().getMaterialgrupo() != null && 
					itemBean.getMaterial().getMaterialmestregrade() == null &&
					itemBean.getMaterial().getMaterialgrupo().getGradeestoquetipo() != null){
				boolean existMaterialitemByMaterialgrademestre = materialService.existMaterialitemByMaterialgrademestre(itemBean.getMaterial());
				if(existMaterialitemByMaterialgrademestre){
					existMaterialitemByMaterialgrademestreEntrega = true;
					itemBean.setListaMaterialitemmestregrade(entregaService.montaMaterialitemmestregrade(materialService.findMaterialitemByMaterialmestregrade(itemBean.getMaterial())));
				}				
			}
		}
		for (BaixarEntregaItemBean itemBean : bean.getListaPendentes()) {
			if(itemBean.getMaterial() != null && itemBean.getMaterial().getMaterialgrupo() != null && 
					itemBean.getMaterial().getMaterialmestregrade() == null &&
					itemBean.getMaterial().getMaterialgrupo().getGradeestoquetipo() != null){
				boolean existMaterialitemByMaterialgrademestre = materialService.existMaterialitemByMaterialgrademestre(itemBean.getMaterial());
				if(existMaterialitemByMaterialgrademestre){
					existMaterialitemByMaterialgrademestreEntrega = true;
					itemBean.setListaMaterialitemmestregrade(entregaService.montaMaterialitemmestregrade(materialService.findMaterialitemByMaterialmestregrade(itemBean.getMaterial())));
				}				
			}
		}
		
		if(existMaterialitemByMaterialgrademestreEntrega){
			return new ModelAndView("process/baixarEntregaItemmestregrade", "bean", bean);
		}else {
			return continueOnAction("saveBaixar", bean);
		}
	}
	
	/**
	 * Executa a baixa das entregas que foram calculadas anteriormente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#validaBaixaEntrega(BaixarEntregaBean, BindException)
	 * @see br.com.linkcom.sined.geral.service.MaterialService#findClasses
	 * @see br.com.linkcom.sined.geral.service.EntregaService#lancarPatrimonio
	 * @see br.com.linkcom.sined.geral.service.EntregaService#lancarProduto
	 * @see br.com.linkcom.sined.geral.service.EntregaService#lancarEpi
	 * @see br.com.linkcom.sined.geral.service.EntregaService#lancarEstoque
	 * @see br.com.linkcom.sined.geral.service.EntregaService#updateBaixa
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView saveBaixar(WebRequestContext request, BaixarEntregaBean bean){
		
//		String valorParametro = parametrogeralService.buscaValorPorNome(Parametrogeral.WMS_INTEGRACAO);
		
		List<Entrega> listaEntrega = entregaService.findByEntrega(bean.getIds());
//		if (valorParametro.toUpperCase().equals("TRUE")){
		
		Empresa empresa = null;
		for (Entrega entrega : listaEntrega) {
			if(empresa == null){
				empresa = entrega.getEmpresa();
			} else if(entrega.getEmpresa() != null && !entrega.getEmpresa().equals(empresa)){
				empresa = null;
				break;
			}
		}
		
		boolean retornowms = false;
		for (Entrega entrega : listaEntrega) {
			
			if(empresaService.isIntegracaoWms(entrega.getEmpresa())){
				retornowms = false;
				if (entrega.getRetornowms() == null || (!entrega.getRetornowms().equals(RetornoWMSEnum.CONFERIDA_OK) 
						&& !entrega.getRetornowms().equals(RetornoWMSEnum.CONFERIDA_DIVERGENCIA))){
					retornowms = false;
					if(entrega.getListaEntregadocumento() != null && !entrega.getListaEntregadocumento().isEmpty()){
						retornowms = true;
						for (Entregadocumento entregadocumento : entrega.getListaEntregadocumento()) {
							if (entregadocumento.existeItemSincronizarWms() && (entregadocumento.getRetornowms() == null || (!entregadocumento.getRetornowms().equals(RetornoWMSEnum.CONFERIDA_OK) 
									&& !entregadocumento.getRetornowms().equals(RetornoWMSEnum.CONFERIDA_DIVERGENCIA)))){
								retornowms = false;
								break;
							}
						}
					}
				}else {
					retornowms = true;
				}
				if(!retornowms){
					request.addError("Para baixar � preciso que a Entrega seja conferida!");
					return sendRedirectToAction("listagem");
				}
			}
		}
		entregaService.validaBaixaEntrega(bean, request.getBindException());
		
		if(request.getBindException().hasErrors()){
			return getControllerModelAndView(request);
		}
		
		List<BaixarEntregaItemBean> listaPendentes = bean.getListaPendentes();
		List<BaixarEntregaItemBean> listaResolvidos = bean.getListaResolvidos();
		
		listaPendentes = entregaService.getListaCorrigida(bean.getListaPendentes());
		
		List<Patrimonioitem> listaPatrimonio = new ArrayList<Patrimonioitem>();
		
//		List<Entrega> listaEntregaOrigemOrdemcompra = entregaService.findRateioByEntrega(bean.getIds());
//		List<BaixarEntregaItemBean> listaPendentesNova = entregaService.ajustarQtdeByRateioOrdemcompraVariosProjetos(listaEntregaOrigemOrdemcompra, listaPendentes, estoquePorProjeto);
//		List<BaixarEntregaItemBean> listaResolvidosNova = entregaService.ajustarQtdeByRateioOrdemcompraVariosProjetos(listaEntregaOrigemOrdemcompra, listaResolvidos, estoquePorProjeto);
//		if(listaPendentesNova != null && !listaPendentesNova.isEmpty()){
//			listaPendentes = listaPendentesNova;
//		}
//		if(listaResolvidosNova != null && !listaResolvidosNova.isEmpty()){
//			listaResolvidos = listaResolvidosNova;
//		}
		
		
		//adicionar informa��es da requisi��o para a baixa no estoque
		entregaService.adicionarInfRequisicao(listaPendentes);
		entregaService.adicionarInfRequisicao(listaResolvidos);
		
		if (listaPendentes != null) {
			for (BaixarEntregaItemBean item : listaPendentes) {
				item.setEmpresa(empresa);
				
				if(item.getListaMaterialitemmestregrade() != null && !item.getListaMaterialitemmestregrade().isEmpty()){
					for(BaixarEntregaItemBean itemGrade : item.getListaMaterialitemmestregrade()){
						BaixarEntregaItemBean itemGradeForBaixa = new BaixarEntregaItemBean(item);
						itemGradeForBaixa.setMaterial(itemGrade.getMaterial());
						itemGradeForBaixa.setMaterialmestregrade(item.getMaterial());
						itemGradeForBaixa.setQuantidade(itemGrade.getQuantidade());
						itemGradeForBaixa.setValorunitario(item.getValorunitario());
						processaBaixaEntregaItemBeanPendentes(itemGrade, listaPatrimonio);
					}
				}else {
					processaBaixaEntregaItemBeanPendentes(item, listaPatrimonio);
				}
			}
		}
		
		boolean sucessoProcessoBaixa = false;
		if (listaResolvidos != null) {
			for (BaixarEntregaItemBean item : listaResolvidos) {
				if(item.getListaMaterialitemmestregrade() != null && !item.getListaMaterialitemmestregrade().isEmpty()){
					for(BaixarEntregaItemBean itemGrade : item.getListaMaterialitemmestregrade()){
						BaixarEntregaItemBean itemGradeForBaixa = new BaixarEntregaItemBean(item);
						itemGradeForBaixa.setMaterial(itemGrade.getMaterial());
						itemGradeForBaixa.setMaterialmestregrade(item.getMaterial());
						itemGradeForBaixa.setMaterialOrigem(item.getMaterialOrigem());
						itemGradeForBaixa.setQuantidade(itemGrade.getQuantidade());
						itemGradeForBaixa.setValorunitario(item.getValorunitario());
						sucessoProcessoBaixa = processaBaixaEntregaItemBeanResolvidos(request, itemGradeForBaixa, listaPatrimonio);
						if(!sucessoProcessoBaixa){
							return getControllerModelAndView(request);
						}
					}
				}else {
					sucessoProcessoBaixa = processaBaixaEntregaItemBeanResolvidos(request, item, listaPatrimonio);
					if(!sucessoProcessoBaixa){
						return getControllerModelAndView(request);
					}
				}
			}
		}
		
		entregaService.updateBaixa(bean.getIds());
		
		if(bean.getIds() != null && !"".equals(bean.getIds())){
			Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.RECEBIMENTO_BAIXADO);
			if(motivoAviso != null && Boolean.TRUE.equals(motivoAviso.getEmail())){
				List<Requisicaomaterial> listarRequisicaomaterial = requisicaomaterialService.findRequisitanteMaterialByEntregaBaixada(bean.getIds());
				
				if(listarRequisicaomaterial != null && !listarRequisicaomaterial.isEmpty()){
					List<Aviso> avisoList = new ArrayList<Aviso>();
					
					for(Requisicaomaterial requisicaomaterial : listarRequisicaomaterial){
						if(requisicaomaterial.getCdentrega() != null && requisicaomaterial.getColaborador() != null){
							avisoList.add(new Aviso("Recebimento baixado", "C�digo do recebimento: " + requisicaomaterial.getCdentrega() + " relacionado com a requisi��o de material " + requisicaomaterial.getCdrequisicaomaterial(), 
									motivoAviso.getTipoaviso(), motivoAviso.getPapel(), motivoAviso.getUsuario(), AvisoOrigem.ENTREGA, requisicaomaterial.getCdentrega(), 
									empresaService.loadPrincipal(), null, motivoAviso));
						}
					}
					
					avisoService.salvarAvisos(avisoList, true);
				}
			}
		}
		
		request.addMessage("Entrega(s) baixada(s) com sucesso.");
		
		for(Entrega entrega : listaEntrega){
			try {
				entradafiscalService.calcularCustoEntradaMaterial(entrega, null, null);
			} catch (Exception e) {
				e.printStackTrace();
				request.addError("Erro ao atualizar o valor da entrada: " + e.getMessage());
			}
		}
		
		entregahistoricoService.saveHistoricos(listaEntrega, Entregaacao.RECEBIMENTO_BAIXADO);
		
		List<Entrega> listaEntregaRomaneio = entregaService.findEntregasNecessitamRomaneio(bean.getIds());
		if(listaEntregaRomaneio != null && listaEntregaRomaneio.size() > 0){
			bean.setListaEntregaRomaneio(listaEntregaRomaneio);
			request.addMessage("Existe(m) entrega(s) que necessita(m) de romaneio.", MessageType.WARN); 
			return continueOnAction("gerarRomaneioBaixarEntrega", bean);
		}
		
		if(bean.getIds() != null && !"".equals(bean.getIds())){
			List<Patrimonioitem> listaItem = patrimonioitemService.findByEntregaWithoutPlaqueta(bean.getIds());
			if(listaItem != null && !listaItem.isEmpty()){
				request.setAttribute("gerarPlaquetaAposBaixa", "true");
				request.setAttribute("itensPlaqueta", bean.getIds());
			}
		}
		
		return getControllerModelAndView(request);
	}
	
	private Boolean processaBaixaEntregaItemBeanResolvidos(WebRequestContext request, BaixarEntregaItemBean item, List<Patrimonioitem> listaPatrimonio) {
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = null;
		
		if (item.getMaterialclasse() == null){
			request.addError("No recebimento " +item.getEntrega().getCdentrega()+ " � necess�rio selecionar a necessidade do material "+item.getMaterial().getNome());
			return false;
		} else if (item.getMaterialclasse().equals(Materialclasse.PATRIMONIO)) {
			
			for (int i = 0; i < item.getQuantidade(); i++) entregaService.lancarPatrimonio(listaPatrimonio, item);
			
		} else  if(!Materialclasse.SERVICO.equals(item.getMaterialclasse())){
			if (!materialService.validateObrigarLote(item.getEntregamaterial(), null, request, item.getEntregamaterial().getListaEntregamateriallote())){
				return false;
			}
			movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			if (item.getEntrega().getEmpresa() == null){
				List<Entregadocumento> listEntregaDocumento = entregadocumentoService.findListEntregaDocumentoByEntrega(item.getEntrega());
				if (listEntregaDocumento != null && listEntregaDocumento.size() > 0 && listEntregaDocumento.get(0).getEmpresa() != null){
					item.setEmpresa(listEntregaDocumento.get(0).getEmpresa());
				}
				
			}
			movimentacaoestoqueorigem.setEntrega(item.getEntrega());
			movimentacaoestoqueorigem.setEntregamaterial(item.getEntregamaterial());
			
			entregaService.lancarEstoque(movimentacaoestoqueorigem, item);
			
		}	
		return true;
	}
	
	private void processaBaixaEntregaItemBeanPendentes(BaixarEntregaItemBean item, List<Patrimonioitem> listaPatrimonio) {
		List<Materialclasse> listaClasse = materialService.findClasses(item.getMaterial());
		for (Materialclasse materialclasse : listaClasse) {
			if (materialclasse.equals(Materialclasse.PATRIMONIO)) {
				
				if (item.getQtdpatrimonio() != null && item.getQtdpatrimonio() > 0) 
					for (int i = 0; i < item.getQtdpatrimonio(); i++) 
						entregaService.lancarPatrimonio(listaPatrimonio, item);
				
			} else if (materialclasse.equals(Materialclasse.PRODUTO)) {
				materialService.validateObrigarLote(item.getEntregamaterial(), null, null, item.getEntregamaterial().getListaEntregamateriallote());
				if (item.getQtdproduto() != null && item.getQtdproduto() > 0) entregaService.lancarProduto(item);
				
			} else if (materialclasse.equals(Materialclasse.EPI)) {
				
				if (item.getQtdepi() != null && item.getQtdepi() > 0) entregaService.lancarEpi(item);
				
			} else if (!materialclasse.equals(Materialclasse.SERVICO)) {
				throw new SinedException("Erro ao tentar obter a classe do material.");
			}
		}
		
	}
	/**
	 * Action que realiza a abertura do jsp para gera��o do romaneio.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public ModelAndView gerarRomaneioBaixarEntrega(WebRequestContext request, BaixarEntregaBean bean){
		String ids = CollectionsUtil.listAndConcatenate(bean.getListaEntregaRomaneio(), "cdentrega", ",");
		
		List<GerarRomaneioBaixarEntregaItemBean> lista = entregaService.criarListaGerarRomaneio(bean.getListaEntregaRomaneio());
		GerarRomaneioBaixarEntregaBean gerarRomaneioBaixarEntregaBean = new GerarRomaneioBaixarEntregaBean();
		gerarRomaneioBaixarEntregaBean.setIds(ids);
		gerarRomaneioBaixarEntregaBean.setListaEntregaRomaneio(lista);
		
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			String id = request.getParameter("selectedItens");
			if(id != null && !id.equals(""))
				action += "&cdentrega="+id;
		}
		gerarRomaneioBaixarEntregaBean.setController(action);
		
		for (GerarRomaneioBaixarEntregaItemBean gerarRomaneioBaixarEntregaItemBean : lista) {
			if(!entregaService.isDisponivelRomaneio(gerarRomaneioBaixarEntregaItemBean, false, false)){
				request.addError("Recebimento " + gerarRomaneioBaixarEntregaItemBean.getCdentrega() + " j� possui romaneio para todos seus materiais.");
				return getControllerModelAndView(request);
			}
		}
			
//		if("FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.PERMITIR_ESTOQUE_NEGATIVO))){
			boolean existeItemDisponivel = false;
			List<String> listaErro = new ArrayList<String>(); 
			for (GerarRomaneioBaixarEntregaItemBean gerarRomaneioBaixarEntregaItemBean : lista) {
				List<GerarRomaneioBaixarEntregaRomaneio> listaRomaneio = gerarRomaneioBaixarEntregaItemBean.getListaRomaneio();
				
				if(listaRomaneio != null && listaRomaneio.size() > 0){
					for (GerarRomaneioBaixarEntregaRomaneio it : listaRomaneio) {
						if(!localarmazenagemService.getPermitirestoquenegativo(it.getLocalarmazenagemorigem()) && (it.getMaterialclasse() == null || !it.getMaterialclasse().equals(Materialclasse.PATRIMONIO))){
							Double qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(it.getMaterial(), it.getMaterialclasse(), it.getLocalarmazenagemorigem());
							Double qtde = it.getQtdeSolicitada() != null ? it.getQtdeSolicitada() : it.getQtde(); 
							
							if(qtde > qtdeDisponivel){
								if(it.getQtde() != null && it.getQtde() > qtdeDisponivel){
									it.setDisponivel(Boolean.FALSE);
									it.setQtde(0d);
									it.setMarcado(Boolean.FALSE);
									listaErro.add("O material " + it.getMaterial().getNome() + " n�o tem quantidade suficiente para a realiza��o do romaneio do recebimento " + gerarRomaneioBaixarEntregaItemBean.getCdentrega() + ".");
								}else {
									existeItemDisponivel = true;
								}
							}else {
								existeItemDisponivel = true;
							}
						}else {
							existeItemDisponivel = true;
						}
					}
				}
			}
			
			if(!existeItemDisponivel){
				for(String msgErro : listaErro){
					request.addError(msgErro);
				}
				return getControllerModelAndView(request);
			}
//		}
		
		return new ModelAndView("process/gerarRomaneioBaixarEntrega", "bean", gerarRomaneioBaixarEntregaBean);
	}
	
	/**
	 * Action que abre a tela de gera��o de romaneio do recebimento.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public ModelAndView gerarRomaneioAcao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			throw new SinedException("Nenhum item selecionado");
		}
		
		boolean erro = false;
		List<Entrega> listaEntrega = entregaService.findForValidacaoGerarRomaneio(whereIn);
		for (Entrega entrega : listaEntrega) {
			if(entrega.getRomaneio() == null){
				erro = true;
				request.addError("Recebimento " + entrega.getCdentrega() + " n�o necessita de romaneio.");
			}
			if(entrega.getAux_entrega() == null || 
					entrega.getAux_entrega().getSituacaosuprimentos() == null ||
					!entrega.getAux_entrega().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)){
				erro = true;
				request.addError("Recebimento " + entrega.getCdentrega() + " para gerar o romaneio a situa��o tem que estar 'BAIXADA'.");
			}
		}
		
		if(erro){
			return getControllerModelAndView(request);
		}
		
		BaixarEntregaBean bean = new BaixarEntregaBean();
		bean.setIds(whereIn);
		bean.setListaEntregaRomaneio(listaEntrega);
		
		String action = request.getParameter("controller");
		if(action.contains("consultar")){
			String id = request.getParameter("selectedItens");
			if(id != null && !id.equals(""))
				action += "&cdentrega="+id;
		}
		bean.setController(action);
		
		request.setAttribute("fromAcaoEntrega", Boolean.TRUE);
		
		return continueOnAction("gerarRomaneioBaixarEntrega", bean);
	}
	
	/**
	 * Action que salva os romaneios vindo do submit da tela de gerar romaneio na baixa da entrega.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public ModelAndView saveGerarRomaneio(WebRequestContext request, GerarRomaneioBaixarEntregaBean bean) {
		for (GerarRomaneioBaixarEntregaItemBean gerarRomaneioBaixarEntregaItemBean : bean.getListaEntregaRomaneio()) {
			if(!entregaService.isDisponivelRomaneio(gerarRomaneioBaixarEntregaItemBean, true, false)){
				request.addError("Recebimento " + gerarRomaneioBaixarEntregaItemBean.getCdentrega() + " j� possui romaneio para todos seus materiais.");
				return getControllerModelAndView(request);
			}
		}
		
		List<String> listaErro = new ArrayList<String>(); 
		for (GerarRomaneioBaixarEntregaItemBean gerarRomaneioBaixarEntregaItemBean : bean.getListaEntregaRomaneio()) {
			List<GerarRomaneioBaixarEntregaRomaneio> listaRomaneio = gerarRomaneioBaixarEntregaItemBean.getListaRomaneio();
			
			if(listaRomaneio != null && listaRomaneio.size() > 0){
				for (GerarRomaneioBaixarEntregaRomaneio it : listaRomaneio) {
					if(!localarmazenagemService.getPermitirestoquenegativo(it.getLocalarmazenagemorigem()) && (it.getMaterialclasse() == null || !it.getMaterialclasse().equals(Materialclasse.PATRIMONIO))){
						Double qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(it.getMaterial(), it.getMaterialclasse(), it.getLocalarmazenagemorigem());
						Double qtde = it.getQtde(); 
						
						if(qtde > qtdeDisponivel){
							Material material = materialService.load(it.getMaterial(), "material.cdmaterial, material.nome");
							listaErro.add("O material " + material.getNome() + " n�o tem quantidade suficiente para a realiza��o do romaneio do recebimento " + gerarRomaneioBaixarEntregaItemBean.getCdentrega() + ".");
						}
					}
				}
			}
		}
		
		if(listaErro.size() > 0){
			for(String msgErro : listaErro){
				request.addError(msgErro);
			}
			return getControllerModelAndView(request);
		}
		
		Map<MultiKey, List<GerarRomaneioBaixarEntregaRomaneio>> map = new HashMap<MultiKey, List<GerarRomaneioBaixarEntregaRomaneio>>();
		
		List<GerarRomaneioBaixarEntregaItemBean> listaEntregaRomaneio = bean.getListaEntregaRomaneio();
		for (GerarRomaneioBaixarEntregaItemBean itBean : listaEntregaRomaneio) {
			
			Integer cdentrega = itBean.getCdentrega();
			List<GerarRomaneioBaixarEntregaRomaneio> listaGerarRomaneio = itBean.getListaRomaneio();
			if(listaGerarRomaneio != null){
				for (GerarRomaneioBaixarEntregaRomaneio itRomaneio : listaGerarRomaneio) {
					if(itRomaneio.getMarcado() != null && itRomaneio.getMarcado() && itRomaneio.getQtde() != null && itRomaneio.getQtde() > 0){
						itRomaneio.setCdentrega(cdentrega);
						List<GerarRomaneioBaixarEntregaRomaneio> lista;
						
						MultiKey multiKey = new MultiKey(new Object[]{itRomaneio.getCdentrega(), 
																		itRomaneio.getLocalarmazenagemorigem(), 
																		itRomaneio.getLocalarmazenagemdestino()});
						if(map.containsKey(multiKey)){
							lista = map.get(multiKey);
						} else {
							lista = new ArrayList<GerarRomaneioBaixarEntregaRomaneio>();
						}
						
						lista.add(itRomaneio);
						map.put(multiKey, lista);
					}
				}
			} else {
				entregaService.updateRomaneio(new Entrega(cdentrega), Boolean.TRUE);
			}
		}
		
		List<String> listaMsg = new ArrayList<String>();
		List<Entrega> entregas = new ArrayList<Entrega>();
		Set<Entry<MultiKey, List<GerarRomaneioBaixarEntregaRomaneio>>> entrySet = map.entrySet();
		for (Entry<MultiKey, List<GerarRomaneioBaixarEntregaRomaneio>> entry : entrySet) {
			
			MultiKey key = entry.getKey();
			List<GerarRomaneioBaixarEntregaRomaneio> lista = entry.getValue();
			
			Integer cdentrega = (Integer)key.getKey(0);
			Localarmazenagem localarmazenagemorigem = (Localarmazenagem)key.getKey(1);
			Localarmazenagem localarmazenagemdestino = (Localarmazenagem)key.getKey(2);
			
			Entrega entrega = entregaService.loadWithEmpresa(new Entrega(cdentrega)) ;
			
			Empresa empresa = null;
			if(entrega.getEmpresa() != null){
				empresa = entrega.getEmpresa();
			} else if(entrega.getListaOrdemcompraentrega() != null){
				for (Ordemcompraentrega oce : entrega.getListaOrdemcompraentrega()) {
					if(oce.getOrdemcompra() != null && oce.getOrdemcompra().getEmpresa() != null){
						empresa = oce.getOrdemcompra().getEmpresa();
						break;
					}
				}
			}
			
			Romaneio romaneio = new Romaneio();
			romaneio.setDtromaneio(SinedDateUtils.currentDate());
			romaneio.setEmpresa(empresa);
			romaneio.setDescricao("Romaneio referente ao recebimento " + cdentrega);
			romaneio.setLocalarmazenagemorigem(localarmazenagemorigem);
			romaneio.setLocalarmazenagemdestino(localarmazenagemdestino);
			romaneio.setRomaneiosituacao(Romaneiosituacao.BAIXADA);
			
			romaneioService.saveOrUpdate(romaneio);
			
			for (GerarRomaneioBaixarEntregaRomaneio gerarRomaneioBaixarEntregaRomaneio : lista) {
				Romaneioitem romaneioitem = new Romaneioitem();
				romaneioitem.setRomaneio(romaneio);
				romaneioitem.setMaterial(gerarRomaneioBaixarEntregaRomaneio.getMaterial());
				romaneioitem.setQtde(gerarRomaneioBaixarEntregaRomaneio.getQtde());
				romaneioitem.setMaterialclasse(gerarRomaneioBaixarEntregaRomaneio.getMaterialclasse());
				
				romaneioitemService.saveOrUpdate(romaneioitem);
				
				// GERA A ENTRADA E SA�DA DO ROMANEIO
				romaneioService.gerarEntradaSaidaRomaneio(romaneio, romaneioitem, null, empresa, null, null, null, null, false, null, null, null, null);
			}
			
			Romaneioorigem romaneioorigem = new Romaneioorigem();
			romaneioorigem.setRomaneio(romaneio);
			romaneioorigem.setEntrega(entrega);
			
			romaneioorigemService.saveOrUpdate(romaneioorigem);
			
			if(entregaService.isRomaneioCompleto(cdentrega)){
				// ATUALIZA A FLAG DO RECEBIMENTO
				entregaService.updateRomaneio(entrega, Boolean.TRUE);
			}
			
			entregas.add(entrega);
			
			listaMsg.add("Romaneio " + romaneio.getCdromaneio() + " gerado com sucesso.");
		}
		
		if(listaMsg.size() > 0){
			for (String msg : listaMsg) {
				request.addMessage(msg);
			}
			this.entregahistoricoService.saveHistoricos(entregas, Entregaacao.ROMANEIO_CRIADO);
		} else {
			request.addError("Nenhum romaneio gerado.");
		}
		
		return getControllerModelAndView(request);
	}
	
	/**
	 * Popula o combo de box de materialclasse, via AJAX.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView comboBox(WebRequestContext request, Entregamaterial bean) {
		List<Materialclasse> listaClasse = materialService.findClasses(bean.getMaterial());
			
		return new JsonModelAndView().addObject("listaClasse", listaClasse);
	}
	
	/**
	 * Action para que puxe a listagem dos patrim�nios que n�o tem plaqueta.
	 *
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#findByEntregaWithoutPlaqueta
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView popUpPlaqueta(WebRequestContext request, GerarPlaquetaBean bean){

		if (bean.getLista() == null || bean.getLista().size() == 0) {
			String whereIn = SinedUtil.getItensSelecionados(request);
			
			if(entregaService.entregaNotBaixada(whereIn)){
				request.addError("Para gerar plaqueta, a entrega deve estar baixada. ");
				SinedUtil.redirecionamento(request, "/suprimento/crud/Entrega");
				 return null;
			}
			
			List<Patrimonioitem> listaItem = patrimonioitemService.findByEntregaWithoutPlaqueta(whereIn);
			bean.setLista(listaItem);
			
			if(listaItem == null || listaItem.size() <= 0){
//				request.addError("N�o h� item de patrim�nio sem plaqueta.");
				request.addError("N�o h� item de patrim�nio sem " + patrimonioitemService.getTextoPlaquetaSerie() + ".");
				SinedUtil.redirecionamento(request, "/suprimento/crud/Entrega");
				return null;
			}
		}
		
		request.setAttribute("validarPlaquetas", true);
		return new ModelAndView("direct:/crud/popup/plaqueta").addObject("bean", bean);
	}
	
	public ModelAndView validaPlaqueta(WebRequestContext request, GerarPlaquetaBean bean) throws Exception {
		StringBuilder erros = new StringBuilder();
		if(SinedUtil.isListNotEmpty(bean.getLista())){
			for (Patrimonioitem patrimonioitem : bean.getLista()) {
				if(StringUtils.isNotBlank(patrimonioitem.getPlaqueta()) && patrimonioitemService.existePlaqueta(patrimonioitem.getPlaqueta())){
					erros.append("Plaqueta " + patrimonioitem.getPlaqueta() + " j� cadastrada no sistema.\n");
				}
			}
		}
		return new JsonModelAndView().addObject("msgErro", erros.toString());
	}
	
	/**
	 * M�todo para salvar as plaquetas dos itens de patrim�nio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#updateListaPlaquetas
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	@Input("popUpPlaqueta")
	public void savePlaqueta(WebRequestContext request, GerarPlaquetaBean bean) throws Exception {
		
		if (bean == null || bean.getLista() == null) {
			throw new SinedException("Lista de itens de patrim�nio n�o pode ser nulo.");
		}
		
		try {
			patrimonioitemService.updateListaPlaquetas(bean.getLista());
		} catch (Exception e) {
			request.addError(e.getMessage());
			SinedUtil.redirecionamento(request, "/suprimento/crud/Entrega");
			return;
		}
		
		request.addMessage("Plaqueta(s) gerada(s) com sucesso.");
		SinedUtil.redirecionamento(request, "/suprimento/crud/Entrega");
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected void validateBean(Entrega bean, BindException errors) {
		Object attrEntregadocumento = NeoWeb.getRequestContext().getSession().getAttribute("listaEntregadocumento" + bean.getIdentificadortela());
		if (attrEntregadocumento != null) {
			bean.setListaEntregadocumento((List<Entregadocumento>)attrEntregadocumento);	
		}
		
		if(bean.getListaEntregadocumento() == null || bean.getListaEntregadocumento().isEmpty()){
			errors.reject("001", "A entrega tem que ter pelo menos 1 item.");
		} else {
			for(Entregadocumento ed : bean.getListaEntregadocumento()){
				try {
					if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
						rateioService.validateRateio(ed.getRateio(), ed.getValor());
						rateioService.validaRateioitem(ed.getRateio());
					}
				} catch (SinedException e) {
					errors.reject("001",e.getMessage());
				}
				try {
					entradafiscalService.validaEntradafiscal(ed);
				} catch (SinedException e) {
					errors.reject("001",e.getMessage());
				}	
				if(bean.getCdentrega() == null && ed.getFornecedor() != null && ed.getFornecedor().getCdpessoa() != null){
//					Fornecedor fornecedor = fornecedorService.carregaFornecedor(ed.getFornecedor());
					Questionario questionario = questionarioService.carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario.FORNECEDOR);
					if(questionario != null 
							&& questionario.getAvaliarrecebimento() != null 
							&& questionario.getObrigaavaliacaoreceber() != null 
							&& questionario.getAvaliarrecebimento() 
							&& !questionario.getObrigaavaliacaoreceber()
							&& ed.getFornecedor().getPessoaquestionariotrans() == null){
						errors.reject("001", "Fornecedor " + ed.getFornecedor().getNome()  + " n�o foi avaliado.");
					}
					try {
						colaboradorFornecedorService.validaPermissao(ed.getFornecedor());
					} catch (SinedException e) {
						errors.reject("001",e.getMessage());
					}
				}
				
			}
			if(entregaService.validaFornecedornumero(bean, null)){
				errors.reject("001", "Existe fornecedor e n�mero j� cadastrado no sistema.");
			}
		}
		entregaService.validateQtdeEntregue(bean, errors);

		if(errors.hasErrors()) {
			NeoWeb.getRequestContext().getSession().setAttribute("entrega" + bean.getIdentificadortela(), bean);
		} else {
			NeoWeb.getRequestContext().getSession().removeAttribute("entrega" + bean.getIdentificadortela());
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Entrega form) throws CrudException {
		if(form.getHaveOrdemcompra() && form.getCdentrega() == null && form.getController() != null){
			String controller = form.getController();
			super.doSalvar(request, form);
			return preparaRedirecionamentoOrdemCompra(form, request, controller);
		}
		return super.doSalvar(request, form);
	}
	
	/**
	 * M�todo que prepara request para redirecionar para tela que chamou a entrega da Ordem de compra
	 * 
	 * @param form
	 * @param request
	 * @param controller
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	private ModelAndView preparaRedirecionamentoOrdemCompra(Entrega entrega, WebRequestContext request, String controller) {
		request.addMessage("Entrega " + entrega.getCdentrega() + " gerada com sucesso.", MessageType.INFO);
		return getControllerModelAndView(request);
	}
	
	/**
	 * M�todo que gera parcelas para a entrega
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#geraParcelamento(Entregapagamento)
	 * @param request
	 * @param entregapagamento
	 * @author Tom�s T. Rabelo
	 */
	public void geraParcelas(WebRequestContext request, Entregapagamento entregapagamento){
		entregapagamento.setPrazo(prazopagamentoService.findForCdPrazoPagamento(entregapagamento.getPrazo().getCdprazopagamento()));
		List<Entregapagamento> listaDocumento = entregaService.geraParcelamento(entregapagamento);
		View.getCurrent().convertObjectToJs(listaDocumento, "listaDocFinanciado");
		if(entregapagamento.getPrazo().getParcelasiguaisjuros()!=null && entregapagamento.getPrazo().getParcelasiguaisjuros()){
			Double valorJuros = 0.0;
			for (Entregapagamento e : listaDocumento){
				valorJuros += SinedUtil.round(e.getValor().getValue().doubleValue(), 2);
			}
			valorJuros -= entregapagamento.getValor().getValue().doubleValue();
			Entregapagamento tmp = new Entregapagamento();
			tmp.setValor(new Money(valorJuros));
			View.getCurrent().convertObjectToJs(tmp, "valorJuros");
		}
	}
	
	/**
	 * M�todo que carrega a inspe��o da entrega caso haja
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#carregaEntregaParaInspecao(String)
	 * @see #getControllerModelAndView(WebRequestContext)
	 * @param request
	 * @param entrega
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public ModelAndView inspecionarEntrega(WebRequestContext request, Entrega entrega) throws Exception {
		entrega = entregaService.carregaEntregaParaInspecao(request.getParameter("selectedItens"));
		if(entrega == null){
			request.addError("A entrega "+request.getParameter("selectedItens")+" n�o h� itens para inspecionar.");
			return getControllerModelAndView(request);
		}
		
		boolean listaInspecao = Boolean.FALSE;
		for(Entregadocumento ed : entrega.getListaEntregadocumento()){
			for (Entregamaterial em : ed.getListaEntregamaterial()) {
				List<Entregamaterialinspecao> listaNovaInspecao = new ArrayList<Entregamaterialinspecao>();
				if (em.getListaInspecao() == null || em.getListaInspecao().size() == 0){
					List<Materialinspecaoitem> listaMaterialInspecaoItem = materialinspecaoitemService.findListaMaterialInspecaoByMaterial(em.getMaterial());
					if (!listaMaterialInspecaoItem.isEmpty() && listaMaterialInspecaoItem.size() > 0){
						listaInspecao = Boolean.TRUE;
					}
					for (Materialinspecaoitem materialinspecaoitem : listaMaterialInspecaoItem) {
						Entregamaterialinspecao emiBean = new Entregamaterialinspecao();
						emiBean.setInspecaoitem(materialinspecaoitem.getInspecaoitem());
						emiBean.setEntregamaterial(em);
						listaNovaInspecao.add(emiBean);
					}
					em.setListaInspecao(listaNovaInspecao);
				}  else {
					listaInspecao = true;
				}
			}
		}
		if(!listaInspecao){
			request.addError("A entrega "+request.getParameter("selectedItens")+" n�o h� itens para inspecionar.");
			return getControllerModelAndView(request);
		}
		
		String controller = request.getParameter("controller");
		if(controller.contains("consultar"))
			controller += "&cdentrega="+request.getParameter("selectedItens");
		
		entrega.setController(controller);
		
		setInfoForTemplate(request, entrega);
		setEntradaDefaultInfo(request, entrega);
		
		if(entrega.getInspecaocompleta() != null && entrega.getInspecaocompleta() && request.getParameter("editMode") == null)
			request.setAttribute(CONSULTAR, true);
		return new ModelAndView("/process/inspecaoEntrega", "entrega", entrega);
	}
	
	/**
	 * M�todo que atualiza os itens da inspe��o e a entrega caso todos radios tenham sido preenchidos.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#verificaInspecaoCompleta(Entrega)
	 * @see br.com.linkcom.sined.geral.service.EntregaService#salvaInspecao(Entrega)
	 * @param request
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView salvaInspecao(WebRequestContext request, Entrega entrega){
		if(!entrega.getInspecaocompleta()){
			entregaService.verificaInspecaoCompleta(entrega);
		}
		
		entregaService.salvaInspecao(entrega);
		
		if(entrega.getInspecaocompleta())
			request.addMessage("Inspe��o completa para a entrega "+entrega.getCdentrega()+".", MessageType.INFO);
	
		request.addMessage("Registro salvo com sucesso.", MessageType.INFO);
		return new ModelAndView("redirect:"+entrega.getController());
	}
	
	public void ajaxCalculaVencimentos(WebRequestContext request, Entregadocumento bean) {
		request.getServletResponse().setContentType("text/html");
		
		Prazopagamento prazopagamento = prazopagamentoService.loadForEntrada(bean.getPrazopagamento());
		
		List<Date> listaData = new ArrayList<Date>();
		
		Calendar ultimaData = Calendar.getInstance();
		ultimaData.setTimeInMillis(bean.getDtemissao().getTime());
		
		List<Prazopagamentoitem> lista = prazopagamento.getListaPagamentoItem();
		for (int i = 0; i < bean.getRepeticoes(); i++) {
			for (Prazopagamentoitem item : lista) {
				ultimaData.add(Calendar.DAY_OF_MONTH, item.getDias() != null ? item.getDias() : 0);
				ultimaData.add(Calendar.MONTH, item.getMeses() != null ? item.getMeses() : 0);
				
				listaData.add(new Date(ultimaData.getTimeInMillis()));
			}
		}
		
		String html = "";
		
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		for (int i = 0; i < listaData.size(); i++) {
			html += "form['listadocumento["+i+"].dtvencimento'].value = '" + format.format(listaData.get(i)) + "'; ";
		}
		
		View.getCurrent().eval(html);
	}
	
	/**
	 * M�todo que faz o c�lculo do resto para a tela de entrega.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregamaterialService#getQtdeByEntregaMaterial
	 *
	 * @param entrega
	 * @author Rodrigo Freitas
	 */
	public void calculaQtdeEntrega(Entrega entrega, boolean verificarConversaoQtde){
		for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
			entradafiscalService.calculaQtdeRestanteEntregadocumento(entregadocumento, verificarConversaoQtde);
		}			
	}
	
	
	/**
	 * Action que via ajax atualiza os campos do Fornecedor.
	 * 
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#atualizaTipopessoa
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#atualizaCnpj
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#atualizaCpf
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#atualizaRazaosocial
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#atualizaNomefantasia
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#atualizaInscricaoestadual
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#atualizaInscricaomunicipal
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void atualizaCampoFornecedor(WebRequestContext request, ConferenciaDadosFornecedorBean bean){
		if(bean != null && bean.getCdpessoa() != null){
			Fornecedor fornecedor = fornecedorService.loadForAtualizacaoCampos(new Fornecedor(bean.getCdpessoa()));
			
			StringBuilder sbHistoricoAlteracao = new StringBuilder();
			if(bean.getTipopessoa_bd() != null){
				if(fornecedor != null){
					sbHistoricoAlteracao.append("Altera��o do tipo de pessoa ");
					sbHistoricoAlteracao.append(fornecedor.getTipopessoa() != null ? " de " + fornecedor.getTipopessoa().getTipo() : "");
					sbHistoricoAlteracao.append(" para " + bean.getTipopessoa_bd().getTipo());
				}
				fornecedorService.atualizaTipopessoa(bean.getTipopessoa_bd(), bean.getCdpessoa());
			}
			if(bean.getCnpj_bd() != null){
				if(fornecedor != null){
					sbHistoricoAlteracao.append("Altera��o do cnpj ");
					sbHistoricoAlteracao.append(fornecedor.getCnpj() != null ? " de " + fornecedor.getCnpj().toString() : "");
					sbHistoricoAlteracao.append(" para " + bean.getCnpj_bd().toString());
				}
				fornecedorService.atualizaCnpj(bean.getCnpj_bd(), bean.getCdpessoa());
			}
			if(bean.getCpf_bd() != null){
				if(fornecedor != null){
					sbHistoricoAlteracao.append("Altera��o do cpf ");
					sbHistoricoAlteracao.append(fornecedor.getCpf() != null ? " de " + fornecedor.getCpf().toString() : "");
					sbHistoricoAlteracao.append(" para " + bean.getCpf_bd().toString());
				}
				fornecedorService.atualizaCpf(bean.getCpf_bd(), bean.getCdpessoa());
			}
			if(bean.getRazaosocial_bd() != null && !bean.getRazaosocial_bd().equals("")){
				if(fornecedor != null){
					sbHistoricoAlteracao.append("Altera��o da raz�o social ");
					sbHistoricoAlteracao.append(fornecedor.getRazaosocial() != null ? " de " + fornecedor.getRazaosocial() : "");
					sbHistoricoAlteracao.append(" para " + bean.getRazaosocial_bd());
				}
				fornecedorService.atualizaRazaosocial(bean.getRazaosocial_bd(), bean.getCdpessoa());
			}
			if(bean.getNomefantasia_bd() != null && !bean.getNomefantasia_bd().equals("")){
				if(fornecedor != null){
					sbHistoricoAlteracao.append("Altera��o do nome fantasia ");
					sbHistoricoAlteracao.append(fornecedor.getNome() != null ? " de " + fornecedor.getNome() : "");
					sbHistoricoAlteracao.append(" para " + bean.getNomefantasia_bd());
				}
				fornecedorService.atualizaNomefantasia(bean.getNomefantasia_bd(), bean.getCdpessoa());
			}
			if(bean.getIe_bd() != null && !bean.getIe_bd().equals("")){
				if(fornecedor != null){
					sbHistoricoAlteracao.append("Altera��o da inscri��o estadual ");
					sbHistoricoAlteracao.append(fornecedor.getInscricaoestadual() != null ? " de " + fornecedor.getInscricaoestadual() : "");
					sbHistoricoAlteracao.append(" para " + bean.getIe_bd());
				}
				fornecedorService.atualizaInscricaoestadual(bean.getIe_bd(), bean.getCdpessoa());
			}
			if(bean.getIm_bd() != null && !bean.getIm_bd().equals("")){
				if(fornecedor != null){
					sbHistoricoAlteracao.append("Altera��o da inscri��o municipal ");
					sbHistoricoAlteracao.append(fornecedor.getInscricaomunicipal() != null ? " de " + fornecedor.getInscricaomunicipal() : "");
					sbHistoricoAlteracao.append(" para " + bean.getIm_bd());
				}
				fornecedorService.atualizaInscricaomunicipal(bean.getIm_bd(), bean.getCdpessoa());
			}
			
			if(fornecedor != null && sbHistoricoAlteracao.length() > 0){
				try {
					Fornecedorhistorico fornecedorhistorico = new Fornecedorhistorico();
					fornecedorhistorico.setFornecedor(fornecedor);
					fornecedorhistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					fornecedorhistorico.setResponsavel(SinedUtil.getUsuarioLogado());
					fornecedorhistorico.setObservacao(sbHistoricoAlteracao.toString());
					fornecedorhistoricoService.saveOrUpdate(fornecedorhistorico);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * M�todo que via ajax atualiza os campos do material.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialService#updateTipoitemsped
	 * @see br.com.linkcom.sined.geral.service.MaterialService#updateNcmcapitulo
	 * @see br.com.linkcom.sined.geral.service.MaterialService#updateCodigobarras
	 * @see br.com.linkcom.sined.geral.service.MaterialService#updateNcmcompleto
	 * @see br.com.linkcom.sined.geral.service.MaterialService#updateExtipi
	 * @see br.com.linkcom.sined.geral.service.MaterialService#updateCodlistaservico
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void atualizaCampoMaterial(WebRequestContext request, ImportacaoXmlNfeAtualizacaoMaterialBean bean){
		if(bean != null && bean.getMaterial() != null && bean.getMaterial().getCdmaterial() != null){
			if(bean.getTipoitemsped() != null){
				materialService.updateTipoitemsped(bean.getTipoitemsped(), bean.getMaterial());
			}
			if(bean.getNcmcapitulo_bd() != null){
				materialService.updateNcmcapitulo(bean.getNcmcapitulo_bd(), bean.getMaterial());
			}
			if(bean.getCodigobarras_bd() != null){
				materialService.updateCodigobarras(bean.getCodigobarras_bd(), bean.getMaterial());
			}
			if(bean.getNcmcompleto_bd() != null){
				materialService.updateNcmcompleto(bean.getNcmcompleto_bd(), bean.getMaterial());
			}
			if(bean.getExtipi_bd() != null){
				materialService.updateExtipi(bean.getExtipi_bd(), bean.getMaterial());
			}
			if(bean.getCodlistaservico_bd() != null){
				materialService.updateCodlistaservico(bean.getCodlistaservico_bd(), bean.getMaterial());
			}
		}
	}
	
	/**
	 * Action que cria o CSV da listagem de Solicita��o de compra.
	 * 
	 * @see r.com.linkcom.sined.geral.service.SolicitacaocompraService#findForCsv
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarCsv(WebRequestContext request, EntregaFiltro filtro){
		
		List<Entrega> listaId = entregaService.findForCsv(filtro);
		StringBuilder rel = new StringBuilder();
		
		String whereIn = CollectionsUtil.listAndConcatenate(listaId, "cdentrega", ",");
		List<Entrega> lista = entregaService.loadWithLista(whereIn, "", false);
		
		rel.append("Entrega;");
		rel.append("N�mero;");
		rel.append("Fornecedor;");
		rel.append("Local;");
		rel.append("Data de entrega;");
		rel.append("Data de emiss�o;");
		rel.append("Valor;");
		rel.append("Situa��o;");
		rel.append("Desconto;");
		rel.append("Base de c�lculo ICMS;");
		rel.append("Valor ICMS;");
		rel.append("Base de c�lculo ICMS/ST;");
		rel.append("Valor ICMS/ST;");
		rel.append("Outras despesas;");
		rel.append("Valor seguro;");
		rel.append("Frete;");
		rel.append("Tipo de t�tulo;");
		rel.append("Descri��o do t�tulo;");
		rel.append("N�mero do t�tulo;");
		rel.append("Valor IPI;");
		rel.append("Valor PIS;");
		rel.append("Valor COFINS;");
		rel.append("Modelo;");
		rel.append("S�rie;");
		rel.append("Data de entrada;");
		rel.append("Respons�vel pelo frete;");
		rel.append("\n");
		
		for (Entrega e : lista) {
			rel.append(e.getCdentrega());
			rel.append(";");
			
			//numero e fornecedor
			rel.append(";;");
						
			if(e.getLocalarmazenagem() != null)
				rel.append(e.getLocalarmazenagem().getNome());
			rel.append(";");
			
			if(e.getDtentrega() != null)
				rel.append(SinedDateUtils.toString(e.getDtentrega()));
			rel.append(";");
			
			//dtemissao
			rel.append(";");
			
			rel.append(e.getValorTotalEntrega());
			rel.append(";");
			
			if(e.getAux_entrega() != null && e.getAux_entrega().getSituacaosuprimentos() != null)
				rel.append(e.getAux_entrega().getSituacaosuprimentos().getDescricao());
			rel.append(";");
			
			rel.append("\n");
			
			for(Entregadocumento ed : e.getListaEntregadocumento()){
				ed.criaTotaisimposto();
				
				//empresa
				rel.append(";");				

				if(ed.getNumero() != null) rel.append(ed.getNumero());
				rel.append(";");
				
				if(ed.getFornecedor() != null)
					rel.append(ed.getFornecedor().getNome());
				rel.append(";;;");
				
				if(ed.getDtemissao() != null)
					rel.append(SinedDateUtils.toString(ed.getDtemissao()));
				rel.append(";;;");
				
				if(ed.getValordesconto() != null) rel.append(ed.getValordesconto());
				rel.append(";");
				
				if(ed.getValortotalbcicms() != null) rel.append(ed.getValortotalbcicms());
				rel.append(";");
				
				if(ed.getValortotalicms() != null) rel.append(ed.getValortotalicms());
				rel.append(";");
				
				if(ed.getValortotalbcicmsst() != null) rel.append(ed.getValortotalbcicmsst());
				rel.append(";");
				
				if(ed.getValortotalicmsst() != null) rel.append(ed.getValortotalicmsst());
				rel.append(";");
				
				if(ed.getValoroutrasdespesas() != null) rel.append(ed.getValoroutrasdespesas());
				rel.append(";");
				
				if(ed.getValorseguro() != null) rel.append(ed.getValorseguro());
				rel.append(";");
				
				if(ed.getValorfrete() != null) rel.append(ed.getValorfrete());
				rel.append(";");
				
				if(ed.getTipotitulo() != null) rel.append(ed.getTipotitulo().getDescricao());
				rel.append(";");
				
				if(ed.getDescricaotitulo() != null) rel.append(ed.getDescricaotitulo());
				rel.append(";");
				
				if(ed.getNumerotitulo() != null) rel.append(ed.getNumerotitulo());
				rel.append(";");

				if(ed.getValortotalipi() != null) rel.append(ed.getValortotalipi());
				rel.append(";");
				
				if(ed.getValortotalpis() != null) rel.append(ed.getValortotalpis());
				rel.append(";");
				
				if(ed.getValortotalcofins() != null) rel.append(ed.getValortotalcofins());
				rel.append(";");
				
				if(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getNome() != null) rel.append(ed.getModelodocumentofiscal().getNome());
				rel.append(";");
				
				if(ed.getSerie() != null) rel.append(ed.getSerie());
				rel.append(";");
								
				if(ed.getDtentrada() != null) rel.append(SinedDateUtils.toString(ed.getDtentrada()));
				rel.append(";");
				
				if(ed.getResponsavelfrete() != null) rel.append(ed.getResponsavelfrete().getNome());
				rel.append(";");
				
				rel.append("\n");
			}			
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","entrega_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	public ModelAndView verificaFornecedorAjax(WebRequestContext request){
		
		JsonModelAndView json = new JsonModelAndView();
		
		String whereIn = request.getParameter("whereIn");
		List<Entrega> listaEntrega = entregaService.findByEntrega(whereIn);
		
		if(listaEntrega != null && !listaEntrega.isEmpty()){
			for(Entrega entrega : listaEntrega){
				for(Entregadocumento ed1 : entrega.getListaEntregadocumento()){
					for(Entrega entrega2 : listaEntrega){
						for(Entregadocumento ed2 : entrega2.getListaEntregadocumento()){
							if(!ed1.getFornecedor().getNome().equals(ed2.getFornecedor().getNome())){
								json.addObject("fornecedordiferente", true);
								return json;
							}
						}
					}					
				}
			}
		}
		json.addObject("fornecedordiferente", false);
		return json;
		
	}
	
	/**
	 * 
	 * M�todo que redireciona para a tela de receber entrega
	 *
	 * @name abrirPopUpEntrega
	 * @param request
	 * @return
	 * @return ModelAndView
	 * @author Thiago Augusto
	 * @date 03/04/2012
	 *
	 */
	public ModelAndView abrirPopUpEntrega(WebRequestContext request){
		request.setAttribute("novaEntrega", "true");
		return new ModelAndView("direct:/process/popup/receberEntrega");
	}
	
	public ModelAndView ajaxValidacaoChaveAcesso(WebRequestContext request, Entregadocumento entregadocumento) {
		return entregaService.ajaxValidacaoChaveAcesso(request, entregadocumento.getChaveacesso());
	}
	
	/**
	 * 
	 * M�todo para criar uma entrega a partir de uma chave de acesso ou um xml de NF-e
	 *
	 * @name receberEntregaArquivo
	 * @param request
	 * @param ordemcompra
	 * @return void
	 * @author Thiago Augusto
	 * @date 03/04/2012
	 *
	 */
	public void novoRecebimento(WebRequestContext request, Ordemcompra ordemcompra){
		request.getSession().removeAttribute("ARQUIVO_XML_RECEBER_ENTREGA");
		request.getSession().removeAttribute("CHAVE_ACESSO");
		request.getSession().removeAttribute("HTML_CHAVE_ACESSO");
		
		if(ordemcompra.getArquivoxmlnfe() != null){
			request.getSession().setAttribute("ARQUIVO_XML_RECEBER_ENTREGA", ordemcompra.getArquivoxmlnfe());
		}
		if (ordemcompra.getChaveacesso() != null && !ordemcompra.getChaveacesso().equals("")){
			request.getSession().setAttribute("CHAVE_ACESSO", ordemcompra.getChaveacesso());
			request.getSession().setAttribute("HTML_CHAVE_ACESSO", ordemcompra.getHtmlChaveacesso());
		}
		
		request.getSession().setAttribute("novaEntrega", true);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>parent.location = '"+request.getServletRequest().getContextPath()+"/suprimento/crud/Entrega?ACAO=conferenciaDados&novaEntrega="+true+"';" +
				"parent.$.akModalRemove(true);</script>");
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView associarMaterialConferencia(WebRequestContext request, Material material){
		ModelAndView retorno = new JsonModelAndView();
		List<Integer> listaAssociados = null;
		Object objLista = request.getSession().getAttribute("LISTA_ASSOCIADO_CONFERENCIA");
		if(objLista != null && objLista instanceof List){
			listaAssociados = (List<Integer>) objLista;
		} else {
			listaAssociados = new ArrayList<Integer>();
		}
		
		listaAssociados.add(material.getCdmaterial());
		request.getSession().setAttribute("LISTA_ASSOCIADO_CONFERENCIA", listaAssociados);
		
		
		String unidadeXml = request.getParameter("unidadeXml");
		if(material != null && material.getCdmaterial() != null && Util.strings.isNotEmpty(unidadeXml)){
			List<Unidademedida> listaUnidade = unidademedidaService.getUnidademedidaByMaterial(material);
			List<Unidademedida> listaUnidadeRetorno = new ArrayList<Unidademedida>();
			for(Unidademedida un: listaUnidade){
				Unidademedida unMed = new Unidademedida();
				unMed.setCdunidademedida(un.getCdunidademedida());
				unMed.setNome(un.getNome());
				listaUnidadeRetorno.add(un);
			}
			retorno.addObject("listaUnidademedida", listaUnidadeRetorno);
			material = materialService.load(material, "material.cdmaterial, material.nome, material.identificacao");
			retorno.addObject("nomematerial", material.getNome());
		}
		return retorno;
	}
	
	@SuppressWarnings("unchecked")
	public void deassociarMaterialConferencia(WebRequestContext request, Material material){
		List<Integer> listaAssociados = null;
		Object objLista = request.getSession().getAttribute("LISTA_ASSOCIADO_CONFERENCIA");
		if(objLista != null && objLista instanceof List){
			listaAssociados = (List<Integer>) objLista;
			listaAssociados.remove(material.getCdmaterial());
			request.getSession().setAttribute("LISTA_ASSOCIADO_CONFERENCIA", listaAssociados);
		}
	}
	
	/**
	 * 
	 * M�todo para inserir a entrega � partir de uma confer�ncia feita por um xml ou uma chave de acesso.
	 *
	 * @name confimarNovaConferencia
	 * @param request
	 * @param bean
	 * @return
	 * @return ModelAndView
	 * @author Thiago Augusto
	 * @date 05/04/2012
	 *
	 */
	public ModelAndView confimarNovaConferencia(WebRequestContext request, ImportacaoXmlNfeBean bean){
		
		Entrega entrega = new Entrega();
		Entregadocumento entregadocumento = new Entregadocumento();
		List<Entregadocumento> listaEntregadocumento= new ArrayList<Entregadocumento>();
		
		Object objectArquivoXml = request.getSession().getAttribute("ARQUIVO_XML_RECEBER_ENTREGA");
		if(objectArquivoXml != null && objectArquivoXml instanceof Arquivo){
			entrega.setFromImportacaoArquivo(true);
		}
		
//		Preencher os dados da entrega com os dados do conferenciaDadosFornecedorBean
		if (bean.getConferenciaDadosFornecedorBean() != null){
			entradafiscalService.preencheFornecedorFromImportacaoXml(entregadocumento, bean);
			
			Material material = null;
			Ncmcapitulo ncmcapitulo = null;
			ImportacaoXmlNfeAtualizacaoMaterialBean atualizaBean;
			Object object = request.getSession().getAttribute("IMPORTADOR_BEAN_RECEBER_ENTREGA");
			if(object != null && object instanceof ImportacaoXmlNfeBean){
				ImportacaoXmlNfeBean bean_session = (ImportacaoXmlNfeBean)object;
				
				Entregamaterial entregamaterial;
				Set<Entregamaterial> listaEntregamaterial = new ListSet<Entregamaterial>(Entregamaterial.class);
				
				Integer controle = 0;
				for (ImportacaoXmlNfeItemBean it_session : bean_session.getListaItens()) {
					for (ImportacaoXmlNfeItemBean it : bean.getListaItens()) {
						if(it.getSequencial().equals(it_session.getSequencial())){
							it_session.setListaRastro(it.getListaRastro());
							it_session.setMaterial(materialService.load(it.getMaterial(), "material.cdmaterial, material.nome, material.identificacao"));
							
							if(it.getMaterial() != null){
								material = materialService.loadForNFProduto(it.getMaterial());
								
								atualizaBean = new ImportacaoXmlNfeAtualizacaoMaterialBean();
								atualizaBean.setTipoitemsped(material.getTipoitemsped());
								
								
								atualizaBean.setCodigobarras_bd(material.getCodigobarras());
								atualizaBean.setExtipi_bd(material.getExtipi());
								atualizaBean.setCodlistaservico_bd(material.getCodlistaservico());
								
								if(it_session.getNcm() != null && it_session.getNcm().trim().length() > 1){
									String ncm = it_session.getNcm().trim();
									if(ncm.length() > 2){
										atualizaBean.setNcmcompleto_xml(ncm);
									}
									Integer id_ncm = Integer.parseInt(ncm.substring(0, 2));
									ncmcapitulo = ncmcapituloService.load(new Ncmcapitulo(id_ncm));
									atualizaBean.setNcmcapitulo_xml(ncmcapitulo);
								}
								
								atualizaBean.setCodigobarras_xml(it_session.getCean());
								atualizaBean.setExtipi_xml(it_session.getExtipi());
								atualizaBean.setCodlistaservico_xml(it_session.getCListServ());
								
								materialService.updateInfByConferenciaEntradafical(material, atualizaBean);
							}
							it_session.setUnidademedidaAssociada(it.getUnidademedidaAssociada());
						}
					}
					
					if(entregadocumento.getFornecedor() != null){
						materialfornecedorService.saveIfNotExists(it_session.getMaterial(), entregadocumento.getFornecedor(), it_session.getCprod());
					}
					
					entregamaterial = new Entregamaterial();
					entradafiscalService.setIdentificadorinterno(entregamaterial, controle);
					controle++;
					
					entregamaterial.setUnidademedidacomercial(it_session.getUnidademedidaAssociada());
					
					entregamaterial.setCprod(it_session.getCprod());
					entregamaterial.setXprod(Util.strings.truncate(it_session.getXprod(), 120));
					entregamaterial.setQtde(it_session.getQtde());
					entregamaterial.setSequencial(it_session.getSequencial());
					entregamaterial.setValorunitario(it_session.getValorunitario());
					
					listaEntregamaterial.add(entregamaterial);
				}
				entregadocumento.setListaEntregamaterial(listaEntregamaterial);
				entradafiscalService.setaValoresImportacao(entregadocumento, bean_session);
				listaEntregadocumento.add(entregadocumento);				
			}
			
			if(bean.getModelodocumentofiscal() != null){
				bean.setModelodocumentofiscal(modelodocumentofiscalService.load(bean.getModelodocumentofiscal()));
			}
			
			if(bean.getListaNfeAssociadas() != null && !bean.getListaNfeAssociadas().isEmpty()){
				if(entregadocumento.getListaEntregadocumentofrete() == null){
					entregadocumento.setListaEntregadocumentofrete(new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class));
				}
				if(entregadocumento.getListaEntregadocumentofreterateio() == null){
					entregadocumento.setListaEntregadocumentofreterateio(new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class));
				}
				for(ImportacaoXmlNfeAssociadaBean nfeAssociada : bean.getListaNfeAssociadas()){
					if(nfeAssociada.getChaveacesso() != null && nfeAssociada.getListaNfe() != null){
						String whereIn = SinedUtil.listAndConcatenate(nfeAssociada.getListaNfe(), "cdentregadocumento", ",");
						List<Entregadocumento> listaAssociadas = entradafiscalService.loadWithLista(whereIn, null, false);
						if(listaAssociadas != null && !listaAssociadas.isEmpty()){
							for(Entregadocumento edvinculo : listaAssociadas){
								if(edvinculo != null && edvinculo.getCdentregadocumento() != null){
									Entregadocumentofrete edf = new Entregadocumentofrete(entregadocumento, edvinculo);
									entregadocumento.getListaEntregadocumentofrete().add(edf);
									if(edvinculo.getListaEntregamaterial() != null && !edvinculo.getListaEntregamaterial().isEmpty()){
										for(Entregamaterial em : edvinculo.getListaEntregamaterial()){
											entregadocumento.getListaEntregadocumentofreterateio().add(new Entregadocumentofreterateio(entregadocumento, em));
										}
									}
								}
							}
						}
					}
				}
			}

		}
		
		entrega.setListaEntregadocumento(listaEntregadocumento);
		return continueOnAction("entrada", entrega);
	}
	
	/**
	 * M�todo que busca a unidade de medida do material e unidade de medida relacionada (selecionada)
	 * 
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView getInfoMaterialUnidademedida(WebRequestContext request, Solicitacaocompra bean){
		String index = request.getParameter("index");
		String cdordemcompra = request.getParameter("cdordemcompra");
		Ordemcompramaterial ordemcompramaterial = null;
		Unidademedida unidademedidaselecionado = null;
		if(cdordemcompra != null && !"".equals(cdordemcompra)){
			ordemcompramaterial = ordemcompramaterialService.findUnidademedidaMaterial(new Ordemcompra(Integer.parseInt(cdordemcompra)), bean.getMaterial());
			if(ordemcompramaterial != null && ordemcompramaterial.getUnidademedida() != null)
				unidademedidaselecionado = ordemcompramaterial.getUnidademedida();
		}
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		Material unidadeMaterial = new Material();
		
		unidadeMaterial = materialService.unidadeMedidaMaterial(bean.getMaterial());
		
		if(bean.getUnidademedida()!=null && bean.getUnidademedida().getCdunidademedida()!=null)
			unidademedidaselecionado = bean.getUnidademedida();
		else if(unidademedidaselecionado == null)
			unidademedidaselecionado = unidadeMaterial.getUnidademedida();
		
		listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(unidadeMaterial.getUnidademedida(), unidadeMaterial);
		
		return new JsonModelAndView().addObject("index", index)
									 .addObject("unidadeMedidaSelecionada", unidademedidaselecionado)
									 .addObject("listaUnidademedida", listaUnidademedida);
	}
	
	/**
	 * M�todo que abre uma popup para registrar n�meros de s�rie do material
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirRegistrarnumeroserie(WebRequestContext request){
		String cdmaterial = request.getParameter("cdmaterial");
		String cdentrega = request.getParameter("cdentrega");
		String qtdestr = request.getParameter("qtde");
		String index = request.getParameter("index");
		
		if(cdmaterial == null || "".equals(cdmaterial) || cdentrega == null || "".equals(cdentrega)){
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if(qtdestr != null){
			
		}
		
		
		Entrega entrega = new Entrega(Integer.parseInt(cdentrega));
		Material material = materialService.findForRegistrarnumeroserie(Integer.parseInt(cdmaterial));
		
		if(material == null){
			request.addError("N�o foi poss�vel carregar o material.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Integer qtde = 1;
		Materialnumeroserie materialnumeroserie = new Materialnumeroserie();
		materialnumeroserie.setEntrega(entrega);
		if(qtdestr != null && !"".equals(qtdestr)){
			try {
				qtde = Integer.parseInt(qtdestr);
			} catch (NumberFormatException e) {
				request.addError("S� � permitido escolher n�mero de s�rie para quantidade inteira.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		Set<Materialnumeroserie> listaMaterialnumeroserie = new ListSet<Materialnumeroserie>(Materialnumeroserie.class);
		for(int i = 0; i < qtde; i++){
			listaMaterialnumeroserie.add(materialnumeroserie);
		}
		material.setListaMaterialnumeroserie(listaMaterialnumeroserie);
		material.setIndex(index);
		return new ModelAndView("direct:/crud/popup/registrarnumeroserie", "bean", material);
			
	}
	
	/**
	 * M�todo que registra os n�meros de s�rie do material relacionado a entrega
	 *
	 * @param request
	 * @param material
	 * @author Luiz Fernando
	 */
	public void registrarNumeroserie(WebRequestContext request, Material material){
		String index = null;
		boolean registrado = false;
		if(material != null && material.getCdmaterial() != null && material.getListaMaterialnumeroserie() != null && 
				!material.getListaMaterialnumeroserie().isEmpty()){
			Set<Materialnumeroserie> listaMaterialnumeroserie = material.getListaMaterialnumeroserie();
			index = material.getIndex();
			material = materialService.findForRegistrarnumeroserie(material.getCdmaterial());
			if(material != null){
				if(material.getListaMaterialnumeroserie() != null && !material.getListaMaterialnumeroserie().isEmpty()){
					for(Materialnumeroserie materialnumeroserie : listaMaterialnumeroserie){
						material.getListaMaterialnumeroserie().add(materialnumeroserie);
					}
				} else {
					material.setListaMaterialnumeroserie(listaMaterialnumeroserie);
				}
				if(!materialService.existDuplicidadenumeroserie(material, null)){
					for(Materialnumeroserie materialnumeroserie : listaMaterialnumeroserie){
						if(materialnumeroserie.getNumero() != null && !"".equals(materialnumeroserie.getNumero())){
							materialnumeroserie.setMaterial(material);
							materialnumeroserieService.saveOrUpdate(materialnumeroserie);
							registrado = true;
						}
					}					
				}
			}
		}
		
		request.getServletResponse().setContentType("text/html");
		if(index != null && !"".equals(index) && registrado){
			View.getCurrent().println("<script>parent.$('#numeroregistrado"+index+"').show();</script>");
		}
		View.getCurrent().println("<script>parent.$.akModalRemove(true);</script>");
	}
	
	/**
	 * M�todo ajax para adiconar a empresa na sess�o (fluxo compra)
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void colocarEmpresaSessao(WebRequestContext request){
		empresaService.adicionarEmpresaSessaoForFluxocompra(request, null, "ENTREGA");
	}

	public ModelAndView ajaxBuscaUnidademedida(WebRequestContext request, Unidademedida unidademedida){
		ModelAndView retorno = new JsonModelAndView();
		if(unidademedida.getCdunidademedida() != null){
			unidademedida = unidademedidaService.load(unidademedida, "unidademedida.cdunidademedida,unidademedida.nome,unidademedida.simbolo");
			retorno.addObject("unidademedida", unidademedida);
		}
		
		return retorno;
	}
	
	public ModelAndView ajaxBuscaQuantidadeconvertida(WebRequestContext request, Entregamaterial entregamaterial){
		String cdunidademedidaassociada = request.getParameter("cdunidademedidaAssociada");
		if(Util.strings.isEmpty(cdunidademedidaassociada)){
			return new ModelAndView();
		}
		Double quantidade = unidademedidaService.getQtdeUnidadePrincipal(entregamaterial.getMaterial(), new Unidademedida(Integer.parseInt(cdunidademedidaassociada)), entregamaterial.getXmlqtdeinformada());
		return new JsonModelAndView().addObject("quantidadeconvertida", quantidade);
	}
	
	public ModelAndView ajaxListaUnidademedida(WebRequestContext request, Material material){
		ModelAndView retorno = new JsonModelAndView();
		if(material.getCdmaterial() != null){
			List<Unidademedida> listaUnidade = unidademedidaService.getUnidademedidaByMaterial(material);
			retorno.addObject("lista", listaUnidade);
		}
		
		return retorno;
	}
	
	public ModelAndView ajaxQtdeUnidadeOrdemcompra(WebRequestContext request, Entregamaterial entregamaterial){
		Double qtdesol = entregaService.getQtdeUnidadeOrdemcompra(entregamaterial);
		return new JsonModelAndView().addObject("qtde", SinedUtil.descriptionDecimal(qtdesol));
	}
	
	@SuppressWarnings("unchecked")
	public ModelAndView abrirEntregadocumento(WebRequestContext request, Entrega entrega) throws CrudException {
		if(StringUtils.isNotBlank(entrega.getIdentificadortela())) {
			request.getSession().setAttribute("entrega"  + entrega.getIdentificadortela(), entrega);
			request.getSession().setAttribute("entregadocumentoTrans"  + entrega.getIdentificadortela(), null);
			
			StringBuilder urlBuilder = new StringBuilder("/fiscal/crud/Entradafiscal");
			urlBuilder.append("?fromrecebimento=true");
			urlBuilder.append("&identificadortela=" + (entrega.getIdentificadortela() != null ? entrega.getIdentificadortela() : ""));
			urlBuilder.append("&cdempresaentrega=" + (entrega.getEmpresa() != null ? entrega.getEmpresa().getCdpessoa() : ""));
			
			Entregadocumento entregadocumento = null;
			String indexLista = request.getParameter("indexLista");
			if(StringUtils.isNotBlank(indexLista)) {
				Object attrListaEntregadocumento = request.getSession().getAttribute("listaEntregadocumento" + entrega.getIdentificadortela());
				if(attrListaEntregadocumento != null) {
					urlBuilder.append("&indexLista=" + indexLista);
					
					List<Entregadocumento> lista = (List<Entregadocumento>) attrListaEntregadocumento;
					entregadocumento = lista.get(Integer.parseInt(indexLista));
					
					
					if(entregadocumento != null && entregadocumento.getCdentregadocumento() != null) {
						urlBuilder.append("&cdentregadocumento=" + entregadocumento.getCdentregadocumento());
					}
				}
			}
			urlBuilder.append("&ACAO=criarEntradafiscalDoRecebimento");
			
			if(entrega.getCdentrega() != null) {
				urlBuilder.append("&cdentrega=" + entrega.getCdentrega());
			}
			request.getSession().setAttribute("entregadocumentoTrans"  + entrega.getIdentificadortela(), entregadocumento);
			request.getSession().setAttribute("indexListaEntregadocumentoTrans"  + entrega.getIdentificadortela(), indexLista);
			return new ModelAndView("redirect:" + urlBuilder.toString());
		}
		
		return doEntrada(request, entrega);
	}
	
	@SuppressWarnings("unchecked")
	public void excluirEntregadocumento(WebRequestContext request) {
		List<Entregadocumento> listaEntregadocumento = null;
		String index = request.getParameter("indexLista");
		
		if(index != null) {
			Object attr = request.getSession().getAttribute("listaEntregadocumento" +  request.getParameter("identificadortela"));
			if(attr != null) {
				listaEntregadocumento = (List<Entregadocumento>) attr;
				listaEntregadocumento.remove(Integer.parseInt(index));
			}
		}
		request.getSession().setAttribute("listaEntregadocumento" + request.getParameter("identificadortela"), listaEntregadocumento);
		View.getCurrent().println("var sucesso = true");
	}
	
	public ModelAndView ajaxBuscaLotes(WebRequestContext request, Entregamaterial bean){
		return loteestoqueService.ajaxBuscaLotes(bean);
	}
}
