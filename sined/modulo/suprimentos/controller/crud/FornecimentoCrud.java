package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Fornecimentocontratoitem;
import br.com.linkcom.sined.geral.bean.Fornecimentoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Fornecimentocontratoitemtipo;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FornecimentoService;
import br.com.linkcom.sined.geral.service.FornecimentocontratoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecimentoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/suprimento/crud/Fornecimento", authorizationModule=CrudAuthorizationModule.class)
public class FornecimentoCrud extends CrudControllerSined<FornecimentoFiltro, Fornecimento, Fornecimento>{

	private FornecimentocontratoService fornecimentocontratoService;
	private FornecimentoService fornecimentoService;
	private EmpresaService empresaService;
	
	public void setFornecimentocontratoService(FornecimentocontratoService fornecimentocontratoService) {
		this.fornecimentocontratoService = fornecimentocontratoService;
	}
	public void setFornecimentoService(FornecimentoService fornecimentoService) {
		this.fornecimentoService = fornecimentoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	@Override
	protected void listagem(WebRequestContext request, FornecimentoFiltro filtro)	throws Exception {
		request.setAttribute("listacontrato", fornecimentocontratoService.findAtivos());
	}
	
	/**
	 * M�todo respons�vel por gerar o fornecimento a partir de um contrato de fornecimento.
	 * @param request
	 * @param bean
	 * @return
	 * @throws CrudException
	 * @author Taidson
	 * @since 24/06/2010
	 */
	public ModelAndView gerarFornecimento(WebRequestContext request, Fornecimento bean) throws CrudException{
		String itens[] = request.getParameter("selectedItens").split(",");
		Integer cdfornecimentocontrato = Integer.valueOf(itens[0]);
		
		bean.setFromFornecimentocontrato(true);
		
		if(itens.length == 1){
			
			Fornecimentocontrato fornecimentocontrato = new Fornecimentocontrato();
			
			fornecimentocontrato.setCdfornecimentocontrato(cdfornecimentocontrato);
			fornecimentocontrato = fornecimentocontratoService.loadForEntrada(fornecimentocontrato);
			
			bean.setEmpresa(fornecimentocontrato.getEmpresa());
			bean.setFornecimentocontrato(fornecimentocontrato);
			bean.setValor(fornecimentocontrato.getValorFinal());
			bean.setObservacao(fornecimentocontrato.getObservacao());
			
			if(fornecimentocontrato.getListaFornecimentocontratoitem() != null 
					&& fornecimentocontrato.getListaFornecimentocontratoitem().size() > 0){
				for (Fornecimentocontratoitem item : fornecimentocontrato.getListaFornecimentocontratoitem()) {
					Fornecimentoitem fornecimentoitem = new Fornecimentoitem();
					fornecimentoitem.setQtdeprevista(item.getQtde());
					fornecimentoitem.setDescricao(item.getDescricao());
					
					if(item.getFornecimentocontratoitemtipo() != null && item.getFornecimentocontratoitemtipo().equals(Fornecimentocontratoitemtipo.MATERIAL) 
						&& item.getMaterial() != null && item.getMaterial().getNome() != null && 
						!"".equals(item.getMaterial().getNome())){
						fornecimentoitem.setDescricao(item.getMaterial().getNome());
					}
					bean.getListaFornecimentoitem().add(fornecimentoitem);
				}
			}
		}else{
			request.addError("� poss�vel gerar fornecimento somente para um contrato por vez.");
			return new ModelAndView("redirect:/suprimento/crud/Fornecimentocontrato?ACAO=listagem");
		}
		return doEntrada(request, bean);
	}

	@Override
	protected void entrada(WebRequestContext request, Fornecimento form) throws Exception {
		if(form.getCdfornecimento() ==  null){
			form.setDtcontrato(new Date(System.currentTimeMillis()));	
			form.setBaixado(Boolean.FALSE);
		}
		if (form != null && form.getCdfornecimento() != null) {
			List<OrigemsuprimentosBean> listaBeanDestino = fornecimentoService.montaDestino(form);
			request.setAttribute("listaDestino", listaBeanDestino);
		}
		request.setAttribute("listacontrato", fornecimentocontratoService.findByEmpresa(form.getEmpresa()));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		
	}
	
	@Override
	protected void salvar(WebRequestContext request, Fornecimento bean)	throws Exception {
		if(bean.getHistorico() != null) bean.setHistorico(bean.getHistorico().trim());
		if(bean.getObservacao() != null) bean.setObservacao(bean.getObservacao().trim());
		super.salvar(request, bean);
	}
	
	/**
	 * M�todo chamando quando o usu�rio escolhe um contrato na tela de entrada. Carrega os itens do contrato ou retira e 
	 * retorna para a mesma p�gina.
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @throws CrudException
	 * @author Tom�s Rabelo
	 */
	public ModelAndView recuperaFornecimentoContrato(WebRequestContext request, Fornecimento bean) throws CrudException{
		if(bean.getFornecimentocontrato() != null && bean.getFornecimentocontrato().getCdfornecimentocontrato() != null){
			Fornecimentocontrato fornecimentocontrato = fornecimentocontratoService.carregaFornecimentocontrato(bean.getFornecimentocontrato());
			
			bean.setValor(fornecimentocontrato.getValor());
			if(fornecimentocontrato.getListaFornecimentocontratoitem() != null && fornecimentocontrato.getListaFornecimentocontratoitem().size() > 0){
				Set<Fornecimentoitem> list = new ListSet<Fornecimentoitem>(Fornecimentoitem.class);
				for (Fornecimentocontratoitem item : fornecimentocontrato.getListaFornecimentocontratoitem()) {
					Fornecimentoitem fornecimentoitem = new Fornecimentoitem();
					fornecimentoitem.setQtdeprevista(item.getQtde());
					fornecimentoitem.setDescricao(item.getDescricao());
					
					if(item.getFornecimentocontratoitemtipo() != null && item.getFornecimentocontratoitemtipo().equals(Fornecimentocontratoitemtipo.MATERIAL) 
						&& item.getMaterial() != null && item.getMaterial().getNome() != null && 
						!"".equals(item.getMaterial().getNome())){
						fornecimentoitem.setDescricao(item.getMaterial().getNome());
					}
					
					list.add(fornecimentoitem);
				}
				bean.setListaFornecimentoitem(list);
			} else{
				bean.setListaFornecimentoitem(null);
			}
		} else{
			bean.setListaFornecimentoitem(null);
			bean.setValor(null);
		}
		
		return continueOnAction("entrada", bean);
	}
	
	public ModelAndView updateListaContrato (WebRequestContext request, Empresa empresa){
		try{
			List<Fornecimentocontrato> l = fornecimentocontratoService.findByEmpresa(empresa);
			request.setAttribute("listacontrato", l);
			return new JsonModelAndView().addObject("success", true);
		}catch (Exception ex){
			return new JsonModelAndView().addObject("success", false);
		}
	}
	
}
