package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class InventarioFiltro extends FiltroListagemSined {

	protected Date dtinventario;
	protected Integer indicadorpropriedade;
	protected Material material;
	protected Empresa empresa;
	protected Materialcategoria materialcategoria;
	protected InventarioTipo inventarioTipo;
	protected String mesanoAux;
	protected List<InventarioSituacao> listaInventarioSituacao;
	
	public InventarioFiltro() {
		listaInventarioSituacao = new ArrayList<InventarioSituacao>();
		listaInventarioSituacao.add(InventarioSituacao.EM_ABERTO);
		listaInventarioSituacao.add(InventarioSituacao.AUTORIZADO);
	}
	
	@DisplayName("Data do Inventário")
	public Date getDtinventario() {
		return dtinventario;
	}
	@DisplayName("Indicador de propriedade")
	public Integer getIndicadorpropriedade() {
		return indicadorpropriedade;
	}
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}
	public void setDtinventario(Date dtinventario) {
		this.dtinventario = dtinventario;
	}
	public void setIndicadorpropriedade(Integer indicadorpropriedade) {
		this.indicadorpropriedade = indicadorpropriedade;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	
	@DisplayName("Data do Inventário")
	public String getMesanoAux() {
		if(this.dtinventario != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtinventario);
		return mesanoAux;
	}
	
	@DisplayName("Situações")
	public List<InventarioSituacao> getListaInventarioSituacao() {
		return listaInventarioSituacao;
	}
	
	public void setListaInventarioSituacao(
			List<InventarioSituacao> listaInventarioSituacao) {
		this.listaInventarioSituacao = listaInventarioSituacao;
	}
	
	public void setMesanoAux(String mesanoAux) {
		try {
			if(mesanoAux != null)
				this.dtinventario = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	@DisplayName("Tipo de Inventário")
	public InventarioTipo getInventarioTipo() {
		return inventarioTipo;
	}
	public void setInventarioTipo(InventarioTipo inventarioTipo) {
		this.inventarioTipo = inventarioTipo;
	}
	
}
