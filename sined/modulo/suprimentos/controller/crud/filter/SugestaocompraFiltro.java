package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.Calendar;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class SugestaocompraFiltro extends FiltroListagemSined{

	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected Localarmazenagem localarmazenagem;
	protected Materialgrupo materialgrupo;
	protected Materialcategoria materialcategoria;
	protected Fornecedor fornecedor;
	protected Double margemcompra = 50.00;
	
	public SugestaocompraFiltro(){
		Calendar dataAux = Calendar.getInstance();
		dataAux.set(Calendar.DAY_OF_MONTH, 1);
		
		this.dtinicio = new Date(dataAux.getTimeInMillis());
		
		dataAux.set(Calendar.MONTH, dataAux.get(Calendar.MONTH)+1);
		dataAux.set(Calendar.DAY_OF_MONTH, dataAux.get(Calendar.DAY_OF_MONTH)-1);
		
		this.dtfim = new Date(dataAux.getTimeInMillis());
	}
	
	@Required
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Grupo de Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Margem de compra %")
	public Double getMargemcompra() {
		return margemcompra;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setMargemcompra(Double margemcompra) {
		this.margemcompra = margemcompra;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
}
