package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.TipoExploracao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTerceiros;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PropriedadeRuralFiltro extends FiltroListagemSined {
	protected String nome;
	protected Integer codigoImovel;
	protected String cafir;
	protected String caepf;
	protected Empresa empresa;
	protected TipoExploracao tipoExploracao;
	protected TipoTerceiros tipoTerceiros;
	protected Boolean ativo;
	
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	@DisplayName("C�digo do Im�vel")
	public Integer getCodigoImovel() {
		return codigoImovel;
	}
	@DisplayName("CAFIR")
	public String getCafir() {
		return cafir;
	}
	@DisplayName("CAEPF")
	public String getCaepf() {
		return caepf;
	}
	@DisplayName("Fazenda")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Tipo de Explora��o")
	public TipoExploracao getTipoExploracao() {
		return tipoExploracao;
	}
	@DisplayName("Tipo de Terceiros")
	public TipoTerceiros getTipoTerceiros() {
		return tipoTerceiros;
	}
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}
	public void setCafir(String cafir) {
		this.cafir = cafir;
	}
	public void setCaepf(String caepf) {
		this.caepf = caepf;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setTipoExploracao(TipoExploracao tipoExploracao) {
		this.tipoExploracao = tipoExploracao;
	}
	public void setTipoTerceiros(TipoTerceiros tipoTerceiros) {
		this.tipoTerceiros = tipoTerceiros;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
