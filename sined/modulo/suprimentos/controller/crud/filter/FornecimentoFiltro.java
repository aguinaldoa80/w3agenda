package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FornecimentoFiltro extends FiltroListagemSined{

	protected String historico;
	protected Fornecimentocontrato fornecimentocontrato;
	protected Boolean baixado;
	protected Date dtinicio;
	protected Date dtfim;
	protected Empresa empresa;	
	public FornecimentoFiltro(){
		if (!isNotFirstTime()) {
			this.baixado = false;
		}
	}
	
	@DisplayName("Histórico")
	@MaxLength(150)
	public String getHistorico() {
		return historico;
	}
	@DisplayName("Contrato")
	public Fornecimentocontrato getFornecimentocontrato() {
		return fornecimentocontrato;
	}
	public Boolean getBaixado() {
		return baixado;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setFornecimentocontrato(
			Fornecimentocontrato fornecimentocontrato) {
		this.fornecimentocontrato = fornecimentocontrato;
	}
	public void setBaixado(Boolean baixado) {
		this.baixado = baixado;
	}
	
}
