package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localizacaoestoque;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadorpropriedade;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GerenciamentomaterialFiltro extends FiltroListagemSined{

	protected Material material;
	protected Materialgrupo materialgrupo;
	protected Localarmazenagem localarmazenagem;
	protected Loteestoque loteestoque;
	protected Boolean semplaquetas;
	protected Empresa empresa;
	protected Inventario inventario;
	protected Indicadorpropriedade indicadorpropriedade;
	protected Projeto projeto;
	protected Date dtmovimentacaoInicio;
	protected Date dtmovimentacaoFim;
	protected Projetotipo projetotipo;
	protected Materialcategoria materialcategoria;
	
	protected Boolean orderClasse = false;
	protected Money totalgeral;
	protected Double qtdetotal;
	protected Double qtdetotalreservada;
	protected Double pesototal;
	protected Double pesobrutototal;
	protected List<Materialclasse> listaclasses;
	protected Double minimo = null;
	protected Double maximo = null;
	protected Boolean abaixominimo = false;
	protected Boolean somentematerialinativo = false;
	protected Boolean somentematerialativo = false;
	protected Boolean materialreserva = false;
	protected Contagerencial contagerencial;
	protected String caracteristica;
	protected Materialtipo materialtipo;
	protected Fornecedor fornecedor;	
	protected String plaqueta;
	protected String whereInMaterialInventario;
	protected Localizacaoestoque localizacaoestoque;
	protected Boolean exibirTotais = false;
	protected Boolean filtroItemGrade = false;
	protected String whereInMaterialItemGrade;
	protected Boolean fromSelecionarLoteestoque = false;
	protected Boolean agruparporlote = Boolean.FALSE;
	
	protected Double larguraDe;
	protected Double comprimentoDe;
	protected Double alturaDe;
	protected Double larguraAte;
	protected Double comprimentoAte;
	protected Double alturaAte;
	
	protected String whereInLocalRestricaoUsuario;
	protected String whereInEmpresaRestricaoUsuario;
	
	@DisplayName("Localiza��o de estoque")
	public Localizacaoestoque getLocalizacaoestoque() {
		return localizacaoestoque;
	}
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Classe")
	public List<Materialclasse> getListaclasses() {
		return listaclasses;
	}
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Lote")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public Boolean getSemplaquetas() {
		return semplaquetas;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Boolean getOrderClasse() {
		return orderClasse;
	}
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Invent�rio")
	public Inventario getInventario() {
		return inventario;
	}
	@DisplayName("Indicador de Propriedade")
	public Indicadorpropriedade getIndicadorpropriedade() {
		return indicadorpropriedade;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Plaqueta")
	public String getPlaqueta() {
		return plaqueta;
	}
	@DisplayName("Exibir Totais")
	public Boolean getExibirTotais() {
		return exibirTotais;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	public Double getLarguraDe() {
		return larguraDe;
	}
	public Double getComprimentoDe() {
		return comprimentoDe;
	}
	public Double getAlturaDe() {
		return alturaDe;
	}
	public Double getLarguraAte() {
		return larguraAte;
	}
	public Double getComprimentoAte() {
		return comprimentoAte;
	}
	public Double getAlturaAte() {
		return alturaAte;
	}
	public void setLarguraDe(Double larguraDe) {
		this.larguraDe = larguraDe;
	}
	public void setComprimentoDe(Double comprimentoDe) {
		this.comprimentoDe = comprimentoDe;
	}
	public void setAlturaDe(Double alturaDe) {
		this.alturaDe = alturaDe;
	}
	public void setLarguraAte(Double larguraAte) {
		this.larguraAte = larguraAte;
	}
	public void setComprimentoAte(Double comprimentoAte) {
		this.comprimentoAte = comprimentoAte;
	}
	public void setAlturaAte(Double alturaAte) {
		this.alturaAte = alturaAte;
	}
	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}
	public void setPlaqueta(String plaqueta) {
		this.plaqueta = plaqueta;
	}
	
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setOrderClasse(Boolean orderClasse) {
		this.orderClasse = orderClasse;
	}
	public void setSemplaquetas(Boolean semplaquetas) {
		this.semplaquetas = semplaquetas;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setListaclasses(List<Materialclasse> listaclasses) {
		this.listaclasses = listaclasses;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}
	public void setIndicadorpropriedade(Indicadorpropriedade indicadorpropriedade) {
		this.indicadorpropriedade = indicadorpropriedade;
	}
	
	public Money getTotalgeral() {
		return totalgeral;
	}
	public void setTotalgeral(Money totalgeral) {
		this.totalgeral = totalgeral;
	}
	
	public Double getQtdetotal() {
		return qtdetotal;
	}
	public void setQtdetotal(Double qtdetotal) {
		this.qtdetotal = qtdetotal;
	}
	
	@DisplayName("De")
	public Double getMinimo() {
		return minimo;
	}
	public void setMinimo(Double minimo) {
		this.minimo = minimo;
	}
	@DisplayName("At�")
	public Double getMaximo() {
		return maximo;
	}
	public void setMaximo(Double maximo) {
		this.maximo = maximo;
	}
	@DisplayName("Abaixo do m�nimo")
	public Boolean getAbaixominimo() {
		return abaixominimo;
	}
	
	public void setAbaixominimo(Boolean abaixominimo) {
		this.abaixominimo = abaixominimo;
	}
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	@DisplayName("Caracter�stica")
	public String getCaracteristica() {
		return caracteristica;
	}
	@DisplayName("Tipo de Material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Somente materiais inativos")
	public Boolean getSomentematerialinativo() {
		return somentematerialinativo;
	}
	@DisplayName("Somente materiais ativos")
	public Boolean getSomentematerialativo() {
		return somentematerialativo;
	}
	@DisplayName("Somente materiais ativos")
	public Boolean getMaterialreserva() {
		return materialreserva;
	}
	
	public void setLocalizacaoestoque(Localizacaoestoque localizacaoestoque) {
		this.localizacaoestoque = localizacaoestoque;
	}
	
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Date getDtmovimentacaoInicio() {
		return dtmovimentacaoInicio;
	}
	public Date getDtmovimentacaoFim() {
		return dtmovimentacaoFim;
	}
	public void setDtmovimentacaoInicio(Date dtmovimentacaoInicio) {
		this.dtmovimentacaoInicio = dtmovimentacaoInicio;
	}
	public void setDtmovimentacaoFim(Date dtmovimentacaoFim) {
		this.dtmovimentacaoFim = dtmovimentacaoFim;
	}
	public void setSomentematerialinativo(Boolean somentematerialinativo) {
		this.somentematerialinativo = somentematerialinativo;
	}
	public void setSomentematerialativo(Boolean somentematerialativo) {
		this.somentematerialativo = somentematerialativo;
	}
	public void setMaterialreserva(Boolean materialreserva) {
		this.materialreserva = materialreserva;
	}
	
	
	public String getWhereInMaterialInventario() {
		return whereInMaterialInventario;
	}
	public void setWhereInMaterialInventario(String whereInMaterialInventario) {
		this.whereInMaterialInventario = whereInMaterialInventario;
	}
	public Double getQtdetotalreservada() {
		return qtdetotalreservada;
	}
	public void setQtdetotalreservada(Double qtdetotalreservada) {
		this.qtdetotalreservada = qtdetotalreservada;
	}
	public Double getPesototal() {
		return pesototal;
	}
	public Double getPesobrutototal() {
		return pesobrutototal;
	}
	public void setPesototal(Double pesototal) {
		this.pesototal = pesototal;
	}
	public void setPesobrutototal(Double pesobrutototal) {
		this.pesobrutototal = pesobrutototal;
	}
	public Boolean getFiltroItemGrade() {
		return filtroItemGrade;
	}
	public void setFiltroItemGrade(Boolean filtroItemGrade) {
		this.filtroItemGrade = filtroItemGrade;
	}
	public String getWhereInMaterialItemGrade() {
		return whereInMaterialItemGrade;
	}
	public void setWhereInMaterialItemGrade(String whereInMaterialItemGrade) {
		this.whereInMaterialItemGrade = whereInMaterialItemGrade;
	}
	public Boolean getFromSelecionarLoteestoque() {
		return fromSelecionarLoteestoque;
	}
	public void setFromSelecionarLoteestoque(Boolean fromSelecionarLoteestoque) {
		this.fromSelecionarLoteestoque = fromSelecionarLoteestoque;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public Boolean getAgruparporlote() {
		return agruparporlote;
	}
	
	@DisplayName("Tipo de Projeto")
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}
	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}
	
	public String getWhereInLocalRestricaoUsuario() {
		return whereInLocalRestricaoUsuario;
	}
	public String getWhereInEmpresaRestricaoUsuario() {
		return whereInEmpresaRestricaoUsuario;
	}
	
	public void setWhereInLocalRestricaoUsuario(String whereInLocalRestricaoUsuario) {
		this.whereInLocalRestricaoUsuario = whereInLocalRestricaoUsuario;
	}
	public void setWhereInEmpresaRestricaoUsuario(String whereInEmpresaRestricaoUsuario) {
		this.whereInEmpresaRestricaoUsuario = whereInEmpresaRestricaoUsuario;
	}
	public void setAgruparporlote(Boolean agruparporlote) {
		this.agruparporlote = agruparporlote;
	}
}
