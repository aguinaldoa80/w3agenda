package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;

public class DinamicaRebanhoFiltro {

	private Empresa empresa;
	private Date dtinicio;
	private Date dtfim;
	private Localarmazenagem localarmazenagem;
	private Materialcategoria materialcategoria;
	private Materialgrupo materialgrupo;
	private Material material;
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	@DisplayName("Local de armazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	@DisplayName("Categoria do material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	
	@DisplayName("Grupo do material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
}
