package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmitirprazomediocompraFiltro extends FiltroListagemSined{
	
	private Date dtInicio;
	private Date dtFim;
	private Material material;
	private Materialcategoria materialcategoria;
	private Fornecedor fornecedor;
	private Localarmazenagem localarmazenagem;
	
	private Boolean metaalcancada = false;
	private Boolean metatolerancia = false;
	private Boolean metanaoalcancada = false;
	
	private Boolean solautorizadadentroprazo = false;
	private Boolean solautorizadaforaprazo = false;

	@Required
	@DisplayName("Data In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	@Required
	@DisplayName("Data Fim")
	public Date getDtFim() {
		return dtFim;
	}
	public Material getMaterial() {
		return material;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Meta alcan�ada")
	public Boolean getMetaalcancada() {
		return metaalcancada;
	}
	@DisplayName("Meta dentro da toler�ncia")
	public Boolean getMetatolerancia() {
		return metatolerancia;
	}
	@DisplayName("Meta n�o alcan�ada")
	public Boolean getMetanaoalcancada() {
		return metanaoalcancada;
	}
	@DisplayName("Solicita��o autorizada dentro do prazo limite para compra do material")
	public Boolean getSolautorizadadentroprazo() {
		return solautorizadadentroprazo;
	}
	@DisplayName("Solicita��o autorizada fora do prazo limite para compra do material")
	public Boolean getSolautorizadaforaprazo() {
		return solautorizadaforaprazo;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setMetaalcancada(Boolean metaalcancada) {
		this.metaalcancada = metaalcancada;
	}
	public void setMetatolerancia(Boolean metatolerancia) {
		this.metatolerancia = metatolerancia;
	}
	public void setMetanaoalcancada(Boolean metanaoalcancada) {
		this.metanaoalcancada = metanaoalcancada;
	}
	public void setSolautorizadadentroprazo(Boolean solautorizadadentroprazo) {
		this.solautorizadadentroprazo = solautorizadadentroprazo;
	}
	public void setSolautorizadaforaprazo(Boolean solautorizadaforaprazo) {
		this.solautorizadaforaprazo = solautorizadaforaprazo;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
}
