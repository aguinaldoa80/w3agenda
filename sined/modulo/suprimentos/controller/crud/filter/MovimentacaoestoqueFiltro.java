package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.List;

import com.ibm.icu.math.BigDecimal;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.util.DateUtils;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueOrigemEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Movimentacaoanimalmotivo;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MovimentacaoestoqueFiltro extends FiltroListagemSined{

	public final static String TODOS = "Todos";
	public final static String NOME = "Nome";
	public final static String IDENTIFICACAO = "Identificador";
	public final static String CODIGO_BARRAS = "C�digo de barras";
	
	protected Integer cdmoviemtacaestoque;
	protected Boolean canceladas;
	protected Empresa empresa;
	protected Localarmazenagem localarmazenagem;
	protected Date datade = new Date(DateUtils.primeiroDiaMes().getTime());
	protected Date dataate;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Material material;
	protected Double valorinicial;
	protected Double valorfinal;
	protected Projeto projeto;
	protected Loteestoque loteestoque;
	protected MovimentacaoEstoqueOrigemEnum origem;
	protected Integer idorigem;
	protected Projetotipo projetotipo;
	protected Materialcategoria materialcategoria;
	
	protected Boolean epi = false;
	protected Boolean produto = false;
	protected Boolean servico = false;
	
	protected List<Movimentacaoestoquetipo> listatipos;
	
	protected Materialclasse materialclasse;
	protected String whereInItemgrade;
	
	protected Double totalqtdeentradas;
	protected Double totalvalorentradas;
	protected Double totalpesoentradas;
	protected Double totalpesobrutoentradas;
	protected Double totalqtdesaidas;
	protected Double totalvalorsaidas;
	protected Double totalpesosaidas;
	protected Double totalpesobrutosaidas;
	protected Double totalqtdeentradasaidas;
	protected Double totalvalorentradasaidas;
	protected Double totalpesoentradasaida;
	protected Double totalpesobrutoentradasaida;
	
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	
	protected Movimentacaoanimalmotivo movimentacaoanimalmotivo;
	
	public MovimentacaoestoqueFiltro(){
	}

	public MovimentacaoestoqueFiltro(Empresa empresa,
			Localarmazenagem localarmazenagem, Material material, Boolean epi,
			Boolean produto, Boolean servico) {
		this.empresa = empresa;
		this.localarmazenagem = localarmazenagem;
		this.material = material;
		this.epi = epi;
		this.produto = produto;
		this.servico = servico;
	}

	@DisplayName("Movimenta��o")
	@MaxLength(9)
	public Integer getCdmoviemtacaestoque() {
		return cdmoviemtacaestoque;
	}
	@DisplayName("Mostrar canceladas")
	public Boolean getCanceladas() {
		return canceladas;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Date getDatade() {
		return datade;
	}
	public Date getDataate() {
		return dataate;
	}
	@DisplayName("Tipo")
	public List<Movimentacaoestoquetipo> getListatipos() {
		return listatipos;
	}
	@DisplayName("Grupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Produto/EPI/Servi�o")	
	public Material getMaterial() {
		return material;
	}
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	public Boolean getEpi() {
		return epi;
	}
	public Boolean getProduto() {
		return produto;
	}
	public Boolean getServico() {
		return servico;
	}
	public Double getValorinicial() {
		return valorinicial;
	}
	public Double getValorfinal() {
		return valorfinal;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Lote")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public void setEpi(Boolean epi) {
		this.epi = epi;
	}
	public void setProduto(Boolean produto) {
		this.produto = produto;
	}
	public void setServico(Boolean servico) {
		this.servico = servico;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCdmoviemtacaestoque(Integer cdmoviemtacaestoque) {
		this.cdmoviemtacaestoque = cdmoviemtacaestoque;
	}
	public void setCanceladas(Boolean canceladas) {
		this.canceladas = canceladas;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setDatade(Date datade) {
		this.datade = datade;
	}
	public void setDataate(Date dataate) {
		this.dataate = dataate;
	}
	public void setListatipos(List<Movimentacaoestoquetipo> listatipos) {
		this.listatipos = listatipos;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setValorinicial(Double valorinicial) {
		this.valorinicial = valorinicial;
	}
	public void setValorfinal(Double valorfinal) {
		this.valorfinal = valorfinal;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}

	public Double getTotalqtdeentradas() {
		return totalqtdeentradas;
	}

	public Double getTotalvalorentradas() {
		return totalvalorentradas;
	}

	public Double getTotalqtdesaidas() {
		return totalqtdesaidas;
	}

	public Double getTotalvalorsaidas() {
		return totalvalorsaidas;
	}

	public Double getTotalqtdeentradasaidas() {
		Double qtde = 0.0;
		if(this.getTotalqtdeentradas() != null){
			qtde = this.getTotalqtdeentradas();
		}
		if(this.getTotalqtdesaidas() != null){
			qtde = new BigDecimal(qtde.toString()).subtract(new BigDecimal(this.getTotalqtdesaidas().toString())).doubleValue();
		}		
		return qtde;
	}

	public Double getTotalvalorentradasaidas() {
		Double valor = 0.0;
		if(this.getTotalvalorentradas() != null){
			valor = this.getTotalvalorentradas();
		}
		if(this.getTotalvalorsaidas() != null){
			valor = valor - this.getTotalvalorsaidas();
		}		
		return SinedUtil.round(valor, 4);
	}

	public void setTotalqtdeentradas(Double totalqtdeentradas) {
		this.totalqtdeentradas = totalqtdeentradas;
	}

	public void setTotalvalorentradas(Double totalvalorentradas) {
		this.totalvalorentradas = totalvalorentradas;
	}

	public void setTotalqtdesaidas(Double totalqtdesaidas) {
		this.totalqtdesaidas = totalqtdesaidas;
	}

	public void setTotalvalorsaidas(Double totalvalorsaidas) {
		this.totalvalorsaidas = totalvalorsaidas;
	}

	public void setTotalqtdeentradasaidas(Double totalqtdeentradasaidas) {
		this.totalqtdeentradasaidas = totalqtdeentradasaidas;
	}

	public void setTotalvalorentradasaidas(Double totalvalorentradasaidas) {
		this.totalvalorentradasaidas = totalvalorentradasaidas;
	}

	@DisplayName("Origem")
	public MovimentacaoEstoqueOrigemEnum getOrigem() {
		return origem;
	}

	public void setOrigem(MovimentacaoEstoqueOrigemEnum origem) {
		this.origem = origem;
	}

	@DisplayName("Id Origem")
	public Integer getIdorigem() {
		return idorigem;
	}
	public void setIdorigem(Integer idorigem) {
		this.idorigem = idorigem;
	}

	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}

	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Double getTotalpesoentradas() {
		return totalpesoentradas;
	}

	public Double getTotalpesobrutoentradas() {
		return totalpesobrutoentradas;
	}

	public Double getTotalpesosaidas() {
		return totalpesosaidas;
	}

	public Double getTotalpesobrutosaidas() {
		return totalpesobrutosaidas;
	}

	public Double getTotalpesoentradasaida() {
		Double qtde = 0.0;
		if(this.getTotalpesoentradas() != null){
			qtde = this.getTotalpesoentradas();
		}
		if(this.getTotalpesosaidas() != null){
			qtde = qtde - this.getTotalpesosaidas();
		}		
		return qtde;
	}

	public Double getTotalpesobrutoentradasaida() {
		Double qtde = 0.0;
		if(this.getTotalpesobrutoentradas() != null){
			qtde = this.getTotalpesobrutoentradas();
		}
		if(this.getTotalpesobrutosaidas() != null){
			qtde = qtde - this.getTotalpesobrutosaidas();
		}		
		return qtde;
	}

	public void setTotalpesoentradas(Double totalpesoentradas) {
		this.totalpesoentradas = totalpesoentradas;
	}

	public void setTotalpesobrutoentradas(Double totalpesobrutoentradas) {
		this.totalpesobrutoentradas = totalpesobrutoentradas;
	}

	public void setTotalpesosaidas(Double totalpesosaidas) {
		this.totalpesosaidas = totalpesosaidas;
	}

	public void setTotalpesobrutosaidas(Double totalpesobrutosaidas) {
		this.totalpesobrutosaidas = totalpesobrutosaidas;
	}

	public void setTotalpesoentradasaida(Double totalpesoentradasaida) {
		this.totalpesoentradasaida = totalpesoentradasaida;
	}

	public void setTotalpesobrutoentradasaida(Double totalpesobrutoentradasaida) {
		this.totalpesobrutoentradasaida = totalpesobrutoentradasaida;
	}

	public String getWhereInItemgrade() {
		return whereInItemgrade;
	}

	public void setWhereInItemgrade(String whereInItemgrade) {
		this.whereInItemgrade = whereInItemgrade;
	}
	
	@DisplayName("Tipo de Projeto")
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}
	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}
	
	@DisplayName("Motivo")
	public Movimentacaoanimalmotivo getMovimentacaoanimalmotivo() {
		return movimentacaoanimalmotivo;
	}
	public void setMovimentacaoanimalmotivo(
			Movimentacaoanimalmotivo movimentacaoanimalmotivo) {
		this.movimentacaoanimalmotivo = movimentacaoanimalmotivo;
	}
}