package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PatrimonioitemFiltro extends FiltroListagemSined {

	protected Boolean booleanplaqueta;
	protected Boolean ativo = true;
	protected String plaqueta;
	protected Materialtipo materialtipo;
	protected Materialgrupo materialgrupo;
	protected Bempatrimonio bempatrimonio;
	protected Date dtaquisicaode;
	protected Date dtaquisicaoate;
	protected Localarmazenagem localarmazenagem;
	protected Materialcategoria materialcategoria;
	protected Materialnumeroserie materialnumeroserie;
	protected Cliente cliente;

	private List<Patrimonioitemsituacao> listaSituacao = new ArrayList<Patrimonioitemsituacao>();

	protected Boolean orderbymaterial = true;
	protected Boolean orderbyplaqueta;

	public Boolean getBooleanplaqueta() {
		return booleanplaqueta;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	@MaxLength(50)
	public String getPlaqueta() {
		return plaqueta;
	}

	@DisplayName("Material (Patrim�nio)")
	public Bempatrimonio getBempatrimonio() {
		return bempatrimonio;
	}

	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}

	@DisplayName("Tipo de material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}

	public Boolean getOrderbymaterial() {
		return orderbymaterial;
	}

	public Boolean getOrderbyplaqueta() {
		return orderbyplaqueta;
	}

	public List<Patrimonioitemsituacao> getListaSituacao() {
		return listaSituacao;
	}

	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public void setListaSituacao(List<Patrimonioitemsituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}

	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}

	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}

	public void setBooleanplaqueta(Boolean booleanplaqueta) {
		this.booleanplaqueta = booleanplaqueta;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setPlaqueta(String plaqueta) {
		this.plaqueta = plaqueta;
	}

	public void setBempatrimonio(Bempatrimonio bempatrimonio) {
		this.bempatrimonio = bempatrimonio;
	}

	public Date getDtaquisicaode() {
		return dtaquisicaode;
	}

	public Date getDtaquisicaoate() {
		return dtaquisicaoate;
	}

	public void setDtaquisicaode(Date dtaquisicaode) {
		this.dtaquisicaode = dtaquisicaode;
	}

	public void setDtaquisicaoate(Date dtaquisicaoate) {
		this.dtaquisicaoate = dtaquisicaoate;
	}

	public void setOrderbymaterial(Boolean orderbymaterial) {
		this.orderbymaterial = orderbymaterial;
	}

	public void setOrderbyplaqueta(Boolean orderbyplaqueta) {
		this.orderbyplaqueta = orderbyplaqueta;
	}
	
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	
	@DisplayName("S�rie")
	public Materialnumeroserie getMaterialnumeroserie() {
		return materialnumeroserie;
	}
	
	public void setMaterialnumeroserie(Materialnumeroserie materialnumeroserie) {
		this.materialnumeroserie = materialnumeroserie;
	}
	
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
