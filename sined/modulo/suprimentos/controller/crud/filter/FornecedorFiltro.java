package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.FornecedorTipoEnum;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FornecedorFiltro extends FiltroListagemSined{

	protected Tipopessoa tipopessoa;
	protected Cnpj cnpj;
	protected Cpf cpf;
	protected Categoria categoria;
	protected String nomefantasia;
	protected String razaosocial;
	protected Boolean ativo = Boolean.TRUE;
	protected Date dtAvaliacaoInicio;
	protected Date dtAvaliacaoFim;
	
	
	protected String contato;
	protected String telefone;
	protected Municipio municipio;
	protected Uf uf;
	protected String logradouro;
	protected String bairro;
	protected Boolean exibirNotaMediaCsv = Boolean.TRUE;
	
	protected Boolean matriz;
	
	protected Boolean associacao;
	protected Boolean captador;
	protected Fornecedor captadorassociacao;
	protected Sexo sexo;
	protected Date dtnascimento;
	protected FornecedorTipoEnum fornecedorTipoEnum;
	
	
	@DisplayName("Contato")
	public String getContato() {
		return contato;
	}
	
	@DisplayName("Telefone")
	public String getTelefone() {
		return telefone;
	}
	
	@DisplayName("Municipio")
	public Municipio getMunicipio() {
		return municipio;
	}

	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	
	@DisplayName("Logradouro")
	public String getLogradouro() {
		return logradouro;
	}
	
	@DisplayName("Bairro")
	public String getBairro() {
		return bairro;
	}
	
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}
	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}
	
	@DisplayName("Categoria")
	public Categoria getCategoria() {
		return categoria;
	}
	@DisplayName("Nome fantasia")
	@MaxLength(50)
	public String getNomefantasia() {
		return nomefantasia;
	}
	
	@DisplayName("Raz�o social")
	@MaxLength(50)
	public String getRazaosocial() {
		return razaosocial;
	}
	@DisplayName("Situa��o")
	public Boolean getAtivo() {
		return ativo;
	}
	public Date getDtAvaliacaoInicio() {
		return dtAvaliacaoInicio;
	}
	public Date getDtAvaliacaoFim() {
		return dtAvaliacaoFim;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	public void setRazaosocial(String nome) {
		this.razaosocial = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	@DisplayName("Considerar Matriz")
	public Boolean getMatriz() {
		return matriz;
	}
	public void setMatriz(Boolean matriz) {
		this.matriz = matriz;
	}
	public void setDtAvaliacaoInicio(Date dtAvaliacaoInicio) {
		this.dtAvaliacaoInicio = dtAvaliacaoInicio;
	}
	public void setDtAvaliacaoFim(Date dtAvaliacaoFim) {
		this.dtAvaliacaoFim = dtAvaliacaoFim;
	}
	
	public void setContato(String contato){
		this.contato = contato;
	}
	
	public String getNumeromatriz() {
		if (this.getMatriz() != null && this.getMatriz() && this.getCnpj() != null){
			return this.getCnpj().getValue().toString().substring(0, 7);
		} else 
			return null;
	}

	@DisplayName("Exibir Nota M�dia CSV")
	public Boolean getExibirNotaMediaCsv() {
		return exibirNotaMediaCsv;
	}
	public void setExibirNotaMediaCsv(Boolean exibirNotaMediaCsv) {
		this.exibirNotaMediaCsv = exibirNotaMediaCsv;
	}
	
	@DisplayName("Associa��o")
	public Boolean getAssociacao() {
		return associacao;
	}
	@DisplayName("Captador")
	public Boolean getCaptador() {
		return captador;
	}
	@DisplayName("Captador da Associa��o")
	public Fornecedor getCaptadorassociacao() {
		return captadorassociacao;
	}
	@DisplayName("Sexo")
	public Sexo getSexo() {
		return sexo;
	}
	@DisplayName("Data de Nascimento")
	public Date getDtnascimento() {
		return dtnascimento;
	}
	
	public void setAssociacao(Boolean associacao) {
		this.associacao = associacao;
	}
	public void setCaptador(Boolean captador) {
		this.captador = captador;
	}
	public void setCaptadorassociacao(Fornecedor captadorassociacao) {
		this.captadorassociacao = captadorassociacao;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	@DisplayName("Tipo de fornecedor")
	public FornecedorTipoEnum getFornecedorTipoEnum() {
		return fornecedorTipoEnum;
	}
	public void setFornecedorTipoEnum(FornecedorTipoEnum fornecedorTipoEnum) {
		this.fornecedorTipoEnum = fornecedorTipoEnum;
	}
}
