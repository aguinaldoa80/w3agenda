package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LoteestoqueFiltro extends FiltroListagemSined{

	protected String numero;
	protected Date dtinicioFab;
	protected Date dtfimFab;
	protected Date dtinicio;
	protected Date dtfim;
	protected Fornecedor fornecedor;
	protected Material material;
	protected Materialcategoria materialcategoria;
	
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	public Date getDtfimFab() {
		return dtfimFab;
	}
	public Date getDtinicioFab() {
		return dtinicioFab;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	
	public void setDtfimFab(Date dtfimFab) {
		this.dtfimFab = dtfimFab;
	}
	public void setDtinicioFab(Date dtinicioFab) {
		this.dtinicioFab = dtinicioFab;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
}
