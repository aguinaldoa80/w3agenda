package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Situacaorequisicao;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.enumeration.Mostrarromaneio;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RequisicaomaterialFiltro extends FiltroListagemSined{

	protected String identificador;
	protected String descricao;
	
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;

	protected Material material;
	protected Localarmazenagem localarmazenagem;
	protected Date datalimitede;
	protected Date datalimiteate;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Empresa empresa;
	protected Departamento departamento;
	protected String colaborador;
	protected Mostrarromaneio mostrarromaneio;
	protected Date dtcriacaoinicio;
	protected Date dtcriacaofim;
	protected Integer cdpedidovenda;
	protected Boolean baixa;
	protected Materialcategoria materialcategoria;
	
	protected List<Situacaorequisicao> listasituacoes;
	protected List<Materialclasse> listaclasses;
	protected List<String> listaorigens;
	
	public RequisicaomaterialFiltro(){
		if(this.getListasituacoes() == null){
			String paramBloqueioBaixa = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.BLOQUEAR_BAIXA_REQUISICAOMATERIAL);
			
			this.setListasituacoes(new ArrayList<Situacaorequisicao>());
			this.getListasituacoes().add(Situacaorequisicao.EM_ABERTO);
			this.getListasituacoes().add(Situacaorequisicao.AUTORIZADA);
			this.getListasituacoes().add(Situacaorequisicao.EM_PROCESSO_COMPRA);
			if(paramBloqueioBaixa != null && !paramBloqueioBaixa.equals("TRUE")){
				this.getListasituacoes().add(Situacaorequisicao.ROMANEIO_COMPLETO);
				this.getListasituacoes().add(Situacaorequisicao.COMPRA_FINALIZADA);
			}
			this.getListasituacoes().add(Situacaorequisicao.COMPRA_NAO_AUTORIZADA);
		}
	}
	
	@DisplayName("Identificador")
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo de material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@MaxLength(100)
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("De")
	public Date getDatalimitede() {
		return datalimitede;
	}
	@DisplayName("At�")
	public Date getDatalimiteate() {
		return datalimiteate;
	}
	@DisplayName("Centro de Custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	@MaxLength(100)
	@DisplayName("Requisitante")
	public String getColaborador() {
		return colaborador;
	}
	@DisplayName("Situa��es")
	public List<Situacaorequisicao> getListasituacoes() {
		return listasituacoes;
	}
	@DisplayName("Classe")
	public List<Materialclasse> getListaclasses() {
		return listaclasses;
	}
	@DisplayName("Origem")
	public List<String> getListaorigens() {
		return listaorigens;
	}
	@DisplayName("Romaneio")
	public Mostrarromaneio getMostrarromaneio() {
		return mostrarromaneio;
	}
	@DisplayName("Data Cria��o Inicio")
	public Date getDtcriacaoinicio() {
		return dtcriacaoinicio;
	}
	@DisplayName("Data Cria��o Fim")
	public Date getDtcriacaofim() {
		return dtcriacaofim;
	}
	@DisplayName("Pedido de venda")
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}

	public void setDtcriacaoinicio(Date dtcriacaoinicio) {
		this.dtcriacaoinicio = dtcriacaoinicio;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setDtcriacaofim(Date dtcriacaofim) {
		this.dtcriacaofim = dtcriacaofim;
	}
	public Boolean getBaixa() {
		return baixa;
	}

	public void setMostrarromaneio(Mostrarromaneio mostrarromaneio) {
		this.mostrarromaneio = mostrarromaneio;
	}
	public void setListasituacoes(List<Situacaorequisicao> listasituacoes) {
		this.listasituacoes = listasituacoes;
	}
	public void setListaclasses(List<Materialclasse> listaclasses) {
		this.listaclasses = listaclasses;
	}
	public void setListaorigens(List<String> listaorigens) {
		this.listaorigens = listaorigens;
	}
	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setDatalimiteate(Date datalimiteate) {
		this.datalimiteate = datalimiteate;
	}
	public void setDatalimitede(Date datalimitede) {
		this.datalimitede = datalimitede;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setBaixa(Boolean baixa) {
		this.baixa = baixa;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
}
