package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RomaneioFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected String descricao;
	protected Localarmazenagem localorigem;
	protected Localarmazenagem localdestino;
	protected Cliente cliente;
	protected Contrato contrato;
	protected String placa;
	protected List<Romaneiosituacao> listasituacoes;
	protected String mostrar;
	protected Date dtRomaneioInicio;
	protected Date dtRomaneioFim;
	protected Money valorAFaturar;
	protected Material material;
	protected String codigo;
	protected Materialcategoria materialcategoria;
	protected String plaquetaSerie;
	
	public RomaneioFiltro(){
		if(this.getListasituacoes() == null){
			this.setListasituacoes(new ArrayList<Romaneiosituacao>());
			this.getListasituacoes().add(Romaneiosituacao.EM_ABERTO);
			this.getListasituacoes().add(Romaneiosituacao.BAIXADA);
		}
		this.dtRomaneioInicio = SinedDateUtils.currentDate();
	}
	
	@DisplayName("Origem")
	public Localarmazenagem getLocalorigem() {
		return localorigem;
	}
	@DisplayName("Destino")
	public Localarmazenagem getLocaldestino() {
		return localdestino;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Situa��o")
	public List<Romaneiosituacao> getListasituacoes() {
		return listasituacoes;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Contrato getContrato() {
		return contrato;
	}
	@MaxLength(8)
	public String getPlaca() {
		return placa;
	}
	public String getMostrar() {
		return mostrar;
	}

	public Date getDtRomaneioInicio() {
		return dtRomaneioInicio;
	}

	public Date getDtRomaneioFim() {
		return dtRomaneioFim;
	}
	
	public Money getValorAFaturar() {
		return valorAFaturar;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	@DisplayName("Plaqueta/S�rie")
	public String getPlaquetaSerie() {
		return plaquetaSerie;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setValorAFaturar(Money valorAFaturar) {
		this.valorAFaturar = valorAFaturar;
	}
	
	public void setDtRomaneioInicio(Date dtRomaneioInicio) {
		this.dtRomaneioInicio = dtRomaneioInicio;
	}

	public void setDtRomaneioFim(Date dtRomaneioFim) {
		this.dtRomaneioFim = dtRomaneioFim;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setLocalorigem(Localarmazenagem localorigem) {
		this.localorigem = localorigem;
	}
	public void setLocaldestino(Localarmazenagem localdestino) {
		this.localdestino = localdestino;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setListasituacoes(List<Romaneiosituacao> listasituacoes) {
		this.listasituacoes = listasituacoes;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public void setPlaquetaSerie(String plaquetaSerie) {
		this.plaquetaSerie = plaquetaSerie;
	}
}
