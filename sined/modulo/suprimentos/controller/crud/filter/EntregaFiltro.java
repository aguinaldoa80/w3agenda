package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EntregaFiltro extends FiltroListagemSined{

	protected Integer identrega;
	protected Fornecedor fornecedor;
//	protected String fornecedor;
	protected Localarmazenagem localarmazenagem;
	protected Date dtentrega1 = SinedDateUtils.firstDateOfMonth();
	protected Date dtentrega2;
	protected Date dtemissao1;
	protected Date dtemissao2;
	protected Integer cdordemcompra;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Material material;
//	protected String material;
	protected Centrocusto centrocusto;
	protected Empresa empresa;
	protected List<Situacaosuprimentos> listaSituacao;
	protected String numerodocumento;
	protected Projeto projeto;
	protected Loteestoque loteestoque;
	protected Materialcategoria materialcategoria;
	
	public EntregaFiltro(){
		if(this.getListaSituacao() == null){
			this.setListaSituacao(new ArrayList<Situacaosuprimentos>());
			this.getListaSituacao().add(Situacaosuprimentos.EM_ABERTO);
			this.getListaSituacao().add(Situacaosuprimentos.FATURADA);
		}
	}
	
	@MaxLength(9)
	@DisplayName("Recebimento")
	public Integer getIdentrega() {
		return identrega;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Local entrega")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Date getDtentrega1() {
		return dtentrega1;
	}
	public Date getDtentrega2() {
		return dtentrega2;
	}
	public Date getDtemissao1() {
		return dtemissao1;
	}
	public Date getDtemissao2() {
		return dtemissao2;
	}
	@MaxLength(9)
	@DisplayName("Ordem compra")
	public Integer getCdordemcompra() {
		return cdordemcompra;
	}
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo de material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public List<Situacaosuprimentos> getListaSituacao() {
		return listaSituacao;
	}
	@DisplayName("N�mero do Documento")
	public String getNumerodocumento() {
		return numerodocumento;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setListaSituacao(List<Situacaosuprimentos> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setIdentrega(Integer identrega) {
		this.identrega = identrega;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setDtentrega1(Date dtentrega1) {
		this.dtentrega1 = dtentrega1;
	}
	public void setDtentrega2(Date dtentrega2) {
		this.dtentrega2 = dtentrega2;
	}
	public void setDtemissao1(Date dtemissao1) {
		this.dtemissao1 = dtemissao1;
	}
	public void setDtemissao2(Date dtemissao2) {
		this.dtemissao2 = dtemissao2;
	}
	public void setCdordemcompra(Integer cdordemcompra) {
		this.cdordemcompra = cdordemcompra;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}

	@DisplayName("Lote")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}

	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
}
