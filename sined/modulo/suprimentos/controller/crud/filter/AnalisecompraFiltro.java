package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.Calendar;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AnalisecompraFiltro extends FiltroListagemSined{

	protected Empresa empresa;
	protected Date dtinicio;
	protected Date dtfim;
	protected Material material;
	protected Contagerencial contagerencial;
	protected Projeto projeto;
	protected Fornecedor fornecedor;
	protected Boolean somentemenorpreco;
	protected Materialgrupo materialgrupo;
	protected Indicecorrecao indicecorrecao;
	protected String indice;
	protected Materialcategoria materialcategoria;
	
	public AnalisecompraFiltro(){
		Calendar dataAux = Calendar.getInstance();
		dataAux.set(Calendar.DAY_OF_MONTH, 1);
		
		this.dtinicio = new Date(dataAux.getTimeInMillis());
		
		dataAux.set(Calendar.MONTH, dataAux.get(Calendar.MONTH)+1);
		dataAux.set(Calendar.DAY_OF_MONTH, dataAux.get(Calendar.DAY_OF_MONTH)-1);
		
		this.dtfim = new Date(dataAux.getTimeInMillis());
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Somente menor pre�o")
	public Boolean getSomentemenorpreco() {
		return somentemenorpreco;
	}
	@DisplayName("Grupo de Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Atualizar �ltimo pre�o conforme")
	public Indicecorrecao getIndicecorrecao() {
		return indicecorrecao;
	}
	@DisplayName("�ndice")
	public String getIndice() {
		return indice;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}

	public void setIndicecorrecao(Indicecorrecao indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}
	public void setSomentemenorpreco(Boolean somentemenorpreco) {
		this.somentemenorpreco = somentemenorpreco;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setIndice(String indice) {
		this.indice = indice;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
}
