package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.sined.geral.bean.Empresa;

public class OrdemcompraemissaoFiltroReportTemplate extends ReportTemplateFiltro {
	
	protected Empresa empresa;

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
