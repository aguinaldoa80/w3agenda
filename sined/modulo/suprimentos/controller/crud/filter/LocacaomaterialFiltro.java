package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Locacaomaterialtipo;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LocacaomaterialFiltro extends FiltroListagemSined{

	protected Fornecedor fornecedor;
	protected Materialtipo materialtipo;
	protected Materialgrupo materialgrupo;
	protected Bempatrimonio bempatrimonio;
	protected Money valorlocacao1;
	protected Money valorlocacao2;
	protected Locacaomaterialtipo locacaomaterialtipo;
	protected Date dtinicio;
	protected Date dtfim;
	protected Materialcategoria materialcategoria;
	
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Material/servi�o")
	public Bempatrimonio getBempatrimonio() {
		return bempatrimonio;
	}
	@DisplayName("Grupo de Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo de Material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	public Money getValorlocacao1() {
		return valorlocacao1;
	}
	public Money getValorlocacao2() {
		return valorlocacao2;
	}
	@DisplayName("Tipo de loca��o")
	public Locacaomaterialtipo getLocacaomaterialtipo() {
		return locacaomaterialtipo;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	public void setBempatrimonio(Bempatrimonio bempatrimonio) {
		this.bempatrimonio = bempatrimonio;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setValorlocacao2(Money valorlocacao2) {
		this.valorlocacao2 = valorlocacao2;
	}
	public void setValorlocacao1(Money valorlocacao1) {
		this.valorlocacao1 = valorlocacao1;
	}
	public void setLocacaomaterialtipo(Locacaomaterialtipo locacaomaterialtipo) {
		this.locacaomaterialtipo = locacaomaterialtipo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
}
