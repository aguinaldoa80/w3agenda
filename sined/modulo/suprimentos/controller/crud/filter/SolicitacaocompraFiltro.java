package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.enumeration.EtapaProcessoCompra;
import br.com.linkcom.sined.geral.bean.enumeration.SolicitacaocompraFiltroMostrar;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class SolicitacaocompraFiltro extends FiltroListagemSined{

	protected String identificador;
	protected String descricao;
	
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Material material;
	protected Localarmazenagem localarmazenagem;
	protected Date datalimitede;
	protected Date datalimiteate;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Empresa empresa;
	protected Departamento departamento;
	protected String colaborador;
	protected Cotacao cotacaotrans;
	protected Boolean mostrargrupomaterial = Boolean.TRUE;
	protected Boolean mostrarpendenciaqtde;
	protected SolicitacaocompraFiltroMostrar solicitacaocompraFiltroMostrar;
	protected Boolean mostrarvinculoordemcompra;
	protected Boolean ordemcompragerada;
	protected EtapaProcessoCompra etapaProcessoCompra;
	protected Materialcategoria materialcategoria;
	
	protected List<Situacaosuprimentos> listasituacoes;
	protected List<Materialclasse> listaclasses;
	protected List<String> listaorigens;
	protected List<GenericBean> listaResumoSolicitacaocompra;
	
	protected Double pesototal = 0d;
	
	public SolicitacaocompraFiltro(){
		if(this.getListasituacoes() == null){
			this.setListasituacoes(new ArrayList<Situacaosuprimentos>());
			this.getListasituacoes().add(Situacaosuprimentos.EM_ABERTO);
			this.getListasituacoes().add(Situacaosuprimentos.AUTORIZADA);
//			this.getListasituacoes().add(Situacaosuprimentos.EM_PROCESSOCOMPRA);
		}
	}
	
	protected String materialnomecodigo; 
	
	@MaxLength(50)
	@DisplayName("Identificador")
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo de material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@MaxLength(100)
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("De")
	public Date getDatalimitede() {
		return datalimitede;
	}
	@DisplayName("At�")
	public Date getDatalimiteate() {
		return datalimiteate;
	}
	@DisplayName("Centro de Custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	@DisplayName("Situa��o")
	public List<Situacaosuprimentos> getListasituacoes() {
		return listasituacoes;
	}
	@MaxLength(100)
	@DisplayName("Solicitante")
	public String getColaborador() {
		return colaborador;
	}
	@DisplayName("Classe")
	public List<Materialclasse> getListaclasses() {
		return listaclasses;
	}
	@DisplayName("Origem")
	public List<String> getListaorigens() {
		return listaorigens;
	}
	public Cotacao getCotacaotrans() {
		return cotacaotrans;
	}
	@DisplayName("Mostrar")
	public Boolean getMostrargrupomaterial() {
		return mostrargrupomaterial;
	}
	@DisplayName("Material")	
	public String getMaterialnomecodigo() {
		return materialnomecodigo;
	}
	@DisplayName("Mostrar")
	public Boolean getMostrarpendenciaqtde() {
		return mostrarpendenciaqtde;
	}
	@DisplayName("Mostrar")
	public SolicitacaocompraFiltroMostrar getSolicitacaocompraFiltroMostrar() {
		return solicitacaocompraFiltroMostrar;
	}
	public Boolean getOrdemcompragerada() {
		return ordemcompragerada;
	}
	@DisplayName("Etapa")
	public EtapaProcessoCompra getEtapaProcessoCompra() {
		return etapaProcessoCompra;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	public void setEtapaProcessoCompra(EtapaProcessoCompra etapaProcessoCompra) {
		this.etapaProcessoCompra = etapaProcessoCompra;
	}
	public void setSolicitacaocompraFiltroMostrar(SolicitacaocompraFiltroMostrar solicitacaocompraFiltroMostrar) {
		this.solicitacaocompraFiltroMostrar = solicitacaocompraFiltroMostrar;
	}
	public void setMostrarpendenciaqtde(Boolean mostrarpendenciaqtde) {
		this.mostrarpendenciaqtde = mostrarpendenciaqtde;
	}
	public void setMostrargrupomaterial(Boolean mostrargrupomaterial) {
		this.mostrargrupomaterial = mostrargrupomaterial;
	}
	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setDatalimiteate(Date datalimiteate) {
		this.datalimiteate = datalimiteate;
	}
	public void setDatalimitede(Date datalimitede) {
		this.datalimitede = datalimitede;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setListasituacoes(List<Situacaosuprimentos> listasituacoes) {
		this.listasituacoes = listasituacoes;
	}
	public void setListaclasses(List<Materialclasse> listaclasses) {
		this.listaclasses = listaclasses;
	}
	public void setListaorigens(List<String> listaorigens) {
		this.listaorigens = listaorigens;
	}
	public void setCotacaotrans(Cotacao cotacaotrans) {
		this.cotacaotrans = cotacaotrans;
	}
	public void setMaterialnomecodigo(String materialnomecodigo) {
		this.materialnomecodigo = materialnomecodigo;
	}	
	public void setOrdemcompragerada(Boolean ordemcompragerada) {
		this.ordemcompragerada = ordemcompragerada;
	}
	public List<GenericBean> getListaResumoSolicitacaocompra() {
		return listaResumoSolicitacaocompra;
	}
	public void setListaResumoSolicitacaocompra(List<GenericBean> listaResumoSolicitacaocompra) {
		this.listaResumoSolicitacaocompra = listaResumoSolicitacaocompra;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	@DisplayName("Peso Total")
	public Double getPesototal() {
		return pesototal;
	}
	public void setPesototal(Double pesototal) {
		this.pesototal = pesototal;
	}
	@DisplayName("Mostrar")
	public Boolean getMostrarvinculoordemcompra() {
		return mostrarvinculoordemcompra;
	}
	public void setMostrarvinculoordemcompra(Boolean mostrarvinculoordemcompra) {
		this.mostrarvinculoordemcompra = mostrarvinculoordemcompra;
	}
}
