package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FornecimentotipoFiltro extends FiltroListagemSined{

	protected String nome;
	protected Boolean ativo = true;
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
