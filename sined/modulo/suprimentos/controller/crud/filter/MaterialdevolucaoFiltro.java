package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MaterialdevolucaoFiltro extends FiltroListagemSined{

	protected Material material;
	protected String cdvenda;
	protected String cdpedidovenda;
	protected Empresa empresa;
	protected Colaborador colaborador;
	protected Date dtiniciovenda;
	protected Date dtfimvenda;
	protected Date dtiniciodevolucao;
	protected Date dtfimdevolucao;
	protected Boolean canceladas;
	protected Materialcategoria materialcategoria;
	protected Loteestoque loteestoque;
	
	protected String whereIn;
	protected Double qtdetotaldevolvida;
	
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("C�digo da Venda")
	public String getCdvenda() {
		return cdvenda;
	}
	@DisplayName("C�digo do Pedido de Venda")
	public String getCdpedidovenda() {
		return cdpedidovenda;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Date getDtiniciovenda() {
		return dtiniciovenda;
	}
	public Date getDtfimvenda() {
		return dtfimvenda;
	}
	public Date getDtiniciodevolucao() {
		return dtiniciodevolucao;
	}
	public Date getDtfimdevolucao() {
		return dtfimdevolucao;
	}
	public Boolean getCanceladas() {
		return canceladas;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCdvenda(String cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setCdpedidovenda(String cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtiniciovenda(Date dtiniciovenda) {
		this.dtiniciovenda = dtiniciovenda;
	}
	public void setDtfimvenda(Date dtfimvenda) {
		this.dtfimvenda = dtfimvenda;
	}
	public void setDtiniciodevolucao(Date dtiniciodevolucao) {
		this.dtiniciodevolucao = dtiniciodevolucao;
	}
	public void setDtfimdevolucao(Date dtfimdevolucao) {
		this.dtfimdevolucao = dtfimdevolucao;
	}
	public void setCanceladas(Boolean canceladas) {
		this.canceladas = canceladas;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	
	public String getWhereInCdVenda(){
		if(cdvenda != null && !"".equals(cdvenda)){
			StringBuilder s = new StringBuilder();
			String cds[] = cdvenda.split(",");
			Integer aux = 0;
			if(cds != null && cds.length > 0){
				for(int i = 0; i < cds.length; i++){
					try {
						aux = Integer.parseInt(cds[i]);
						if(!"".equals(s.toString()))
							s.append(",");
						s.append(aux);
					} catch (NumberFormatException e) {}
				}
				if(s != null && !"".equals(s.toString()))
					return s.toString();
			}
		}
		
		return null;
	}
	
	public String getWhereInCdPedidoVenda(){
		if(cdpedidovenda != null && !"".equals(cdpedidovenda)){
			StringBuilder s = new StringBuilder();
			String cds[] = cdpedidovenda.split(",");
			Integer aux = 0;
			if(cds != null && cds.length > 0){
				for(int i = 0; i < cds.length; i++){
					try {
						aux = Integer.parseInt(cds[i]);
						if(!"".equals(s.toString()))
							s.append(",");
						s.append(aux);
					} catch (NumberFormatException e) {}
				}
				if(s != null && !"".equals(s.toString()))
					return s.toString();
			}
		}
		
		return null;
	}
	
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	@DisplayName("Qtde Total Devolvida")
	public Double getQtdetotaldevolvida() {
		return qtdetotaldevolvida;
	}
	public void setQtdetotaldevolvida(Double qtdetotaldevolvida) {
		this.qtdetotaldevolvida = qtdetotaldevolvida;
	}
	@DisplayName("Lote")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
}
