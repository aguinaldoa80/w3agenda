package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inspecaoitem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialcor;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEtiquetaMaterial;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.MaterialEtiquetaBean;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MaterialFiltro extends FiltroListagemSined{

	public final static String TODOS = "Todos";
	public final static String CODIGO = "C�digo";
	public final static String CODIGO_FABRICANTE = "C�digo fabricante";
	public final static String REFERENCIA = "Refer�ncia";
	public final static String IDENTIFICACAO = "Identifica��o";
	public final static String CODIGO_ALTERNATIVO = "C�digo alternativo";
	public final static String CODIGO_BARRAS = "C�digo de barras";
	
	
	protected String nome;
	protected String codigo;
	protected Contagerencial contagerencial;
	protected Material materialmestregrade;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Empresa empresa;
	protected String fornecedor;
	protected Boolean ativo = true;
	protected Boolean produto = true;
	protected Boolean epi = true;
	protected Boolean bempatrimonio = true;
	protected Boolean servico = true;
	protected Inspecaoitem inspecaoitem;
	protected String caracteristica;
	protected Materialcor materialcor;
	protected String tipo;
	protected Unidademedida unidademedida;
	protected Double valorcustode;
	protected Double valorcustoate;
	protected Double valorvendade;
	protected Double valorvendaate;
	protected Boolean abaixominimo;
	protected Boolean producao;
	protected Double qtdede;
	protected Double qtdeate;
	protected String codigofabricante; 
	protected Boolean grade;
	protected Boolean kit;
	protected Materialcategoria materialcategoria;
	protected String whereIn;
	protected TipoEtiquetaMaterial tipoetiquetamaterialpredifinido;
	protected ReportTemplateBean tipoetiquetamaterialconfiguravel;
	protected List<MaterialEtiquetaBean> listaetiquetamaterial;
	protected String ncmcompleto;
	protected String codlistaservico;
	protected String codigonbs;
//	protected Boolean listarQantidade;
	protected Boolean itemGradePadrao;
	
	@DisplayName("C�digo do fabricante")
	@MaxLength(25)
	public String getCodigofabricante() {
		return codigofabricante;
	}
	
	@MaxLength(200)
	public String getNome() {
		return nome;
	}
	@DisplayName("C�digo")
	@MaxLength(20)
	public String getCodigo() {
		return codigo;
	}
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Grupo do material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo do material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Item de inspe��o")
	public Inspecaoitem getInspecaoitem() {
		return inspecaoitem;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@MaxLength(100)
	public String getFornecedor() {
		return fornecedor;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Boolean getProduto() {
		return produto;
	}
	public Boolean getEpi() {
		return epi;
	}
	public Boolean getBempatrimonio() {
		return bempatrimonio;
	}
	public Boolean getServico() {
		return servico;
	}
	@DisplayName("Caracter�stica")
	public String getCaracteristica() {
		return caracteristica;
	}
	@DisplayName("Cor")
	public Materialcor getMaterialcor() {
		return materialcor;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	public String getWhereIn() {
		return whereIn;
	}
	@DisplayName("Modelo de etiqueta")
	public TipoEtiquetaMaterial getTipoetiquetamaterialpredifinido() {
		return tipoetiquetamaterialpredifinido;
	}
	@DisplayName("Modelo de etiqueta")
	public ReportTemplateBean getTipoetiquetamaterialconfiguravel() {
		return tipoetiquetamaterialconfiguravel;
	}
	public List<MaterialEtiquetaBean> getListaetiquetamaterial() {
		return listaetiquetamaterial;
	}
	@MaxLength(9)
	@DisplayName("NCM Completo")
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	@MaxLength(9)
	@DisplayName("Item da lista de servi�o")
	public String getCodlistaservico() {
		return codlistaservico;
	}
	@MaxLength(9)
	@DisplayName("C�digo NBS")
	public String getCodigonbs() {
		return codigonbs;
	}
	
//	public Boolean getListarQantidade() {
//		return listarQantidade;
//	}
	
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	public void setCodlistaservico(String codlistaservico) {
		this.codlistaservico = codlistaservico;
	}
	public void setCodigonbs(String codigonbs) {
		this.codigonbs = codigonbs;
	}
	public void setTipoetiquetamaterialpredifinido(
			TipoEtiquetaMaterial tipoetiquetamaterialpredifinido) {
		this.tipoetiquetamaterialpredifinido = tipoetiquetamaterialpredifinido;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public void setMaterialcor(Materialcor materialcor) {
		this.materialcor = materialcor;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	public void setProduto(Boolean produto) {
		this.produto = produto;
	}
	public void setEpi(Boolean epi) {
		this.epi = epi;
	}
	public void setBempatrimonio(Boolean bempatrimonio) {
		this.bempatrimonio = bempatrimonio;
	}
	public void setServico(Boolean servico) {
		this.servico = servico;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setInspecaoitem(Inspecaoitem inspecaoitem) {
		this.inspecaoitem = inspecaoitem;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setCodigofabricante(String codigofabricante) {
		this.codigofabricante = codigofabricante;
	}
	public void setListaetiquetamaterial(List<MaterialEtiquetaBean> listaetiquetamaterial) {
		this.listaetiquetamaterial = listaetiquetamaterial;
	}
	public void setTipoetiquetamaterialconfiguravel(ReportTemplateBean tipoetiquetamaterialconfiguravel) {
		this.tipoetiquetamaterialconfiguravel = tipoetiquetamaterialconfiguravel;
	}
	@DisplayName("Unidade medida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Double getValorcustode() {
		return valorcustode;
	}
	public Double getValorcustoate() {
		return valorcustoate;
	}
	public Double getValorvendade() {
		return valorvendade;
	}
	public Double getValorvendaate() {
		return valorvendaate;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setValorcustode(Double valorcustode) {
		this.valorcustode = valorcustode;
	}
	public void setValorcustoate(Double valorcustoate) {
		this.valorcustoate = valorcustoate;
	}
	public void setValorvendade(Double valorvendade) {
		this.valorvendade = valorvendade;
	}
	public void setValorvendaate(Double valorvendaate) {
		this.valorvendaate = valorvendaate;
	}
	@DisplayName("Abaixo do m�nimo")
	public Boolean getAbaixominimo() {
		return abaixominimo;
	}
	public void setAbaixominimo(Boolean abaixominimo) {
		this.abaixominimo = abaixominimo;
	}
	public Double getQtdede() {
		return qtdede;
	}
	public Double getQtdeate() {
		return qtdeate;
	}
	public void setQtdede(Double qtdede) {
		this.qtdede = qtdede;
	}
	public void setQtdeate(Double qtdeate) {
		this.qtdeate = qtdeate;
	}
	@DisplayName("Produ��o")
	public Boolean getProducao() {
		return producao;
	}
	public void setProducao(Boolean producao) {
		this.producao = producao;
	}
	@DisplayName("Materiais de Grade")
	public Boolean getGrade() {
		return grade;
	}
	public void setGrade(Boolean grade) {
		this.grade = grade;
	}
	@DisplayName("Materiais de Kit")
	public Boolean getKit() {
		return kit;
	}
	public void setKit(Boolean kit) {
		this.kit = kit;
	}
//	public void setListarQantidade(Boolean listarQantidade) {
//		this.listarQantidade = listarQantidade;
//	}
	@DisplayName("Item de grade padr�o")
	public Boolean getItemGradePadrao() {
		return itemGradePadrao;
	}
	public void setItemGradePadrao(Boolean itemGradePadrao) {
		this.itemGradePadrao = itemGradePadrao;
	}

	public Material getMaterialmestregrade() {
		return materialmestregrade;
	}

	public void setMaterialmestregrade(Material materialmestregrade) {
		this.materialmestregrade = materialmestregrade;
	}

	
	
}
