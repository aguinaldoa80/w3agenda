package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadorpropriedade;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class InventariomaterialFiltro extends FiltroListagemSined{

	protected Indicadorpropriedade indicadorpropriedade;
	protected Material material;
	protected Materialcategoria materialcategoria;
	protected Inventario inventario;
	protected Boolean gerarBlocoK;
	protected Boolean gerarBlocoH;
	
	@DisplayName("Indicador de propriedade")
	public Indicadorpropriedade getIndicadorpropriedade() {
		return indicadorpropriedade;
	}
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	@DisplayName("Inventário")
	public Inventario getInventario() {
		return inventario;
	}
	public void setIndicadorpropriedade(Indicadorpropriedade indicadorpropriedade) {
		this.indicadorpropriedade = indicadorpropriedade;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}
	public Boolean getGerarBlocoK() {
		return gerarBlocoK;
	}
	public void setGerarBlocoK(Boolean gerarBlocoK) {
		this.gerarBlocoK = gerarBlocoK;
	}
	public Boolean getGerarBlocoH() {
		return gerarBlocoH;
	}
	public void setGerarBlocoH(Boolean gerarBlocoH) {
		this.gerarBlocoH = gerarBlocoH;
	}
	
	
	
}
