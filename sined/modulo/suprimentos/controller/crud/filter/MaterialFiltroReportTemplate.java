package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEtiquetaMaterial;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.MaterialEtiquetaBean;

public class MaterialFiltroReportTemplate extends ReportTemplateFiltro {
	
	protected String whereIn;
	protected TipoEtiquetaMaterial tipoetiquetamaterialpredifinido;
	protected ReportTemplateBean tipoetiquetamaterialconfiguravel;
	protected List<MaterialEtiquetaBean> listaetiquetamaterial;
	
	
	public String getWhereIn() {
		return whereIn;
	}
	
	public TipoEtiquetaMaterial getTipoetiquetamaterialpredifinido() {
		return tipoetiquetamaterialpredifinido;
	}
	
	public ReportTemplateBean getTipoetiquetamaterialconfiguravel() {
		return tipoetiquetamaterialconfiguravel;
	}
	
	public List<MaterialEtiquetaBean> getListaetiquetamaterial() {
		return listaetiquetamaterial;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	public void setTipoetiquetamaterialpredifinido(TipoEtiquetaMaterial tipoetiquetamaterialpredifinido) {
		this.tipoetiquetamaterialpredifinido = tipoetiquetamaterialpredifinido;
	}
	
	public void setTipoetiquetamaterialconfiguravel(ReportTemplateBean tipoetiquetamaterialconfiguravel) {
		this.tipoetiquetamaterialconfiguravel = tipoetiquetamaterialconfiguravel;
	}
	
	public void setListaetiquetamaterial(List<MaterialEtiquetaBean> listaetiquetamaterial) {
		this.listaetiquetamaterial = listaetiquetamaterial;
	}
}
