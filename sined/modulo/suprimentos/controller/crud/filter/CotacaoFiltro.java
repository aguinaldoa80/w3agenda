package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CotacaoFiltro extends FiltroListagemSined{

	protected Integer idcotacao;
	protected Fornecedor fornecedor;
	protected Date dtinicio;
	protected Date dtfim;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Material material;
	protected Boolean origemsolicitacao;
	protected Boolean origemprojeto;
	protected List<Situacaosuprimentos> listaSituacao;
	protected Boolean fromsolicitacao;
	protected Projeto projeto;
	protected Materialcategoria materialcategoria;
	
	protected Empresa empresa;
	
	public CotacaoFiltro(){
		if(this.getListaSituacao() == null){
			this.setListaSituacao(new ArrayList<Situacaosuprimentos>());
			this.getListaSituacao().add(Situacaosuprimentos.EM_ABERTO);
			this.getListaSituacao().add(Situacaosuprimentos.EM_COTACAO);
//			this.getListaSituacao().add(Situacaosuprimentos.ORDEMCOMPRA_GERADA);
		}
	}
	
	@MaxLength(9)
	@DisplayName("Cota��o")
	public Integer getIdcotacao() {
		return idcotacao;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo de material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	public Boolean getOrigemsolicitacao() {
		return origemsolicitacao;
	}
	public Boolean getOrigemprojeto() {
		return origemprojeto;
	}
	public List<Situacaosuprimentos> getListaSituacao() {
		return listaSituacao;
	}
	public Boolean getFromsolicitacao() {
		return fromsolicitacao;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setListaSituacao(List<Situacaosuprimentos> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setIdcotacao(Integer idcotacao) {
		this.idcotacao = idcotacao;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setOrigemsolicitacao(Boolean origemsolicitacao) {
		this.origemsolicitacao = origemsolicitacao;
	}
	public void setOrigemprojeto(Boolean origemprojeto) {
		this.origemprojeto = origemprojeto;
	}
	public void setFromsolicitacao(Boolean fromsolicitacao) {
		this.fromsolicitacao = fromsolicitacao;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
