package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.Calendar;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.FornecedorTipoEnum;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.ClassificacaoCurvaAbc;
import br.com.linkcom.sined.geral.bean.enumeration.Emitircurvaabcranking;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmitircurvaabcFiltro extends FiltroListagemSined{
	
	public static final String SEM_RANKING = "SEM RANKING";
	
	private Empresa empresa;
	private Date dtInicio;
	private Date dtFim;
	private Projeto projeto;
	private Fornecedor fornecedor;
	private Cliente cliente;
	private Colaborador colaborador;
	private Material material;
	private Materialtipo materialtipo;
	private Categoria categoria;
	private Uf uf;
	private Municipio municipio;
	private Regiao regiao;
	private Materialcategoria materialcategoria;
	
	private Materialgrupo materialgrupo;
	
	private ClassificacaoCurvaAbc classificacao = ClassificacaoCurvaAbc.VALOR;
	
	private Boolean produto;
	private Boolean patrimonio;
	private Boolean epi;
	private Boolean servico;
	
	private Boolean exibirpeso;
	private Emitircurvaabcranking emitircurvaabcranking;
	protected Boolean isRestricaoClienteVendedor;
	protected Boolean isRestricaoVendaVendedor;
	protected Boolean considerarkitpromocional;
	
	private Pedidovendatipo pedidovendatipo;
	
	protected Boolean isVendedorPrincipal;
	
	public EmitircurvaabcFiltro() {
		
		Calendar dataAux = Calendar.getInstance();
		dataAux.set(Calendar.DAY_OF_MONTH, 1);
		
		this.dtInicio = new Date(dataAux.getTimeInMillis());
		
		dataAux.set(Calendar.MONTH, dataAux.get(Calendar.MONTH)+1);
		dataAux.set(Calendar.DAY_OF_MONTH, dataAux.get(Calendar.DAY_OF_MONTH)-1);
		
		this.dtFim = new Date(dataAux.getTimeInMillis());
		
		this.emitircurvaabcranking = Emitircurvaabcranking.PRODUTO;
		
	}
	
	
	  //*************//
	 //**Get & Set**//
	//*************//
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Data In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	
	@DisplayName("Data Fim")
	public Date getDtFim() {
		return dtFim;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Vendedor")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Grupo do Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Produto")
	public Boolean getProduto() {
		return produto;
	}
	@DisplayName("Patrim�nio")
	public Boolean getPatrimonio() {
		return patrimonio;
	}
	@DisplayName("EPI")
	public Boolean getEpi() {
		return epi;
	}
	@DisplayName("Servi�o")
	public Boolean getServico() {
		return servico;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Classifica��o")
	public ClassificacaoCurvaAbc getClassificacao() {
		return classificacao;
	}
	@DisplayName("Ranking")
	public Emitircurvaabcranking getEmitircurvaabcranking() {
		return emitircurvaabcranking;
	}
	public Material getMaterial() {
		return material;
	}	
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Exibir Peso")
	public Boolean getExibirpeso() {
		return exibirpeso;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}

	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setEmitircurvaabcranking(Emitircurvaabcranking emitircurvaabcranking) {
		this.emitircurvaabcranking = emitircurvaabcranking;
	}
	public void setClassificacao(ClassificacaoCurvaAbc classificacao) {
		this.classificacao = classificacao;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setProduto(Boolean produto) {
		this.produto = produto;
	}
	public void setPatrimonio(Boolean patrimonio) {
		this.patrimonio = patrimonio;
	}
	public void setEpi(Boolean epi) {
		this.epi = epi;
	}
	public void setServico(Boolean servico) {
		this.servico = servico;
	}
	public void setExibirpeso(Boolean exibirpeso) {
		this.exibirpeso = exibirpeso;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public Boolean getIsRestricaoClienteVendedor() {
		return isRestricaoClienteVendedor;
	}
	public Boolean getIsRestricaoVendaVendedor() {
		return isRestricaoVendaVendedor;
	}
	public void setIsRestricaoClienteVendedor(Boolean isRestricaoClienteVendedor) {
		this.isRestricaoClienteVendedor = isRestricaoClienteVendedor;
	}
	public void setIsRestricaoVendaVendedor(Boolean isRestricaoVendaVendedor) {
		this.isRestricaoVendaVendedor = isRestricaoVendaVendedor;
	}
	
	@DisplayName("Tipo de Pedido de Venda")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}

	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}

	@DisplayName("Considerar Kit Promocional")
	public Boolean getConsiderarkitpromocional() {
		return considerarkitpromocional;
	}

	public void setConsiderarkitpromocional(Boolean considerarkitpromocional) {
		this.considerarkitpromocional = considerarkitpromocional;
	}
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	@DisplayName("Uf")
	public Uf getUf() {
		return uf;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	@DisplayName("Regi�o")
	public Regiao getRegiao() {
		return regiao;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	
	@DisplayName("Vendedor principal")
	public Boolean getIsVendedorPrincipal() {
		return isVendedorPrincipal;
	}
	
	public void setIsVendedorPrincipal(Boolean isVendedorPrincipal) {
		this.isVendedorPrincipal = isVendedorPrincipal;
	}

}