package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RelatorioLoteEstoqueFiltro extends FiltroListagemSined {

	private Empresa empresa;
	private Fornecedor fornecedorLote;
	private Fornecedor fabricanteMaterial;
	private Date dtInicio;
	private Date dtFim;
	private Localarmazenagem localarmazenagem;
	private Materialgrupo materialgrupo;
	private Materialtipo materialtipo;
	private Materialcategoria materialcategoria;
	private Material material;
	private Loteestoque loteestoque;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedorLote() {
		return fornecedorLote;
	}
	@DisplayName("Fabricante")
	public Fornecedor getFabricanteMaterial() {
		return fabricanteMaterial;
	}
	public Date getDtInicio() {
		return dtInicio;
	}
	public Date getDtFim() {
		return dtFim;
	}
	@DisplayName("Local de armazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Grupo do material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo do material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Categoria do material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Lote")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setFornecedorLote(Fornecedor fornecedorLote) {
		this.fornecedorLote = fornecedorLote;
	}
	public void setFabricanteMaterial(Fornecedor fabricanteMaterial) {
		this.fabricanteMaterial = fabricanteMaterial;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}

}
