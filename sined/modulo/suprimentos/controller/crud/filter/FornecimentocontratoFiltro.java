package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecimentotipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FornecimentocontratoFiltro extends FiltroListagemSined{

	protected String descricao;
	protected Fornecedor fornecedor;
	protected Boolean ativo = true;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Date dtinicio;
	protected Date dtfim;
	protected Fornecimentotipo fornecimentotipo;
	protected Integer cdfornecimentocontrato;
	protected Empresa empresa;
	
	
	@MaxLength(150)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Tipo de fornecimento")
	public Fornecimentotipo getFornecimentotipo() {
		return fornecimentotipo;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Integer getCdfornecimentocontrato() {
		return cdfornecimentocontrato;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCdfornecimentocontrato(Integer cdfornecimentocontrato) {
		this.cdfornecimentocontrato = cdfornecimentocontrato;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setFornecimentotipo(Fornecimentotipo fornecimentotipo) {
		this.fornecimentotipo = fornecimentotipo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
