package br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.enumeration.OrigemOrdemCompra;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OrdemcompraFiltro extends FiltroListagemSined{

	protected String whereInCdcompra;
	protected Integer cdcompra;
	protected Fornecedor fornecedor;
	protected Localarmazenagem localarmazenagem;
	protected Date proximaentregade;
	protected Date proximaentregaate;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Material material;
	protected String identificaomaterial;
//	protected Centrocusto centrocusto;
	protected Empresa empresa;
	protected Double valor1;
	protected Double valor2;
	protected Projeto projeto;
	protected Date dtcriacao1;
	protected Date dtcriacao2;
	protected Usuario usuario;
	protected Boolean criadorordemcompra;
	protected String observacao;
	protected Boolean fromentregacriar;
	protected Boolean rateioverificado;
	protected List<Situacaosuprimentos> listasituacoes;
	protected Materialcategoria materialcategoria;
	
	protected OrigemOrdemCompra origemOrdemCompra;
	protected Integer idOrigem;
	
	protected Money valorTotal;
	
	public OrdemcompraFiltro(){
		if(this.getListasituacoes() == null){
			this.setListasituacoes(new ArrayList<Situacaosuprimentos>());
			this.getListasituacoes().add(Situacaosuprimentos.EM_ABERTO);
			this.getListasituacoes().add(Situacaosuprimentos.AUTORIZADA);
			if(new SinedUtil().needAprovacao()){
				this.getListasituacoes().add(Situacaosuprimentos.APROVADA);
			}
			this.getListasituacoes().add(Situacaosuprimentos.PEDIDO_ENVIADO);
			this.getListasituacoes().add(Situacaosuprimentos.ENTREGA_PARCIAL);
		}
	}

	public OrdemcompraFiltro(Integer cdcompra){
		this.cdcompra = cdcompra;
	}
	
	@DisplayName("Compra")
	@MaxLength(9)
	public Integer getCdcompra() {
		return cdcompra;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Date getProximaentregade() {
		return proximaentregade;
	}
	public Date getProximaentregaate() {
		return proximaentregaate;
	}
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Tipo de material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@MaxLength(100)
	@DisplayName("Material/Servi�o")
	public Material getMaterial() {
		return material;
	}
	/*@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}*/
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Situa��o")
	public List<Situacaosuprimentos> getListasituacoes() {
		return listasituacoes;
	}
	public Boolean getFromentregacriar() {
		return fromentregacriar;
	}
	public Boolean getRateioverificado() {
		return rateioverificado;
	}
	public Double getValor1() {
		return valor1;
	}

	public Double getValor2() {
		return valor2;
	}
	

	public Money getValorTotal() {
		return valorTotal;
	}
	
	@DisplayName("Compra(s)")
	@MaxLength(300)
	public String getWhereInCdcompra() {
		return whereInCdcompra;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@MaxLength(20)
	@DisplayName("Identificador")
	public String getIdentificaomaterial() {
		return identificaomaterial;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	@DisplayName("Categoria Material")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setIdentificaomaterial(String identificaomaterial) {
		this.identificaomaterial = identificaomaterial;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public Date getDtcriacao1() {
		return dtcriacao1;
	}

	public Date getDtcriacao2() {
		return dtcriacao2;
	}

	public void setDtcriacao1(Date dtcriacao1) {
		this.dtcriacao1 = dtcriacao1;
	}

	public void setDtcriacao2(Date dtcriacao2) {
		this.dtcriacao2 = dtcriacao2;
	}

	public void setWhereInCdcompra(String whereInCdcompra) {
		this.whereInCdcompra = StringUtils.trimToNull(whereInCdcompra);
	}

	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}

	public void setValor1(Double valor1) {
		this.valor1 = valor1;
	}

	public void setValor2(Double valor2) {
		this.valor2 = valor2;
	}

	public void setFromentregacriar(Boolean fromentregacriar) {
		this.fromentregacriar = fromentregacriar;
	}
	public void setCdcompra(Integer cdcompra) {
		this.cdcompra = cdcompra;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setProximaentregade(Date proximaentregade) {
		this.proximaentregade = proximaentregade;
	}
	public void setProximaentregaate(Date proximaentregaate) {
		this.proximaentregaate = proximaentregaate;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	/*public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}*/
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setListasituacoes(List<Situacaosuprimentos> listasituacoes) {
		this.listasituacoes = listasituacoes;
	}
	public void setRateioverificado(Boolean rateioverificado) {
		this.rateioverificado = rateioverificado;
	}

	@DisplayName("Criador da OC")
	public Boolean getCriadorordemcompra() {
		return criadorordemcompra;
	}
	public void setCriadorordemcompra(Boolean criadorordemcompra) {
		this.criadorordemcompra = criadorordemcompra;
	}
	
	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}
	
	@DisplayName("Origem")
	public OrigemOrdemCompra getOrigemOrdemCompra() {
		return origemOrdemCompra;
	}
	
	@MaxLength(9)
	@DisplayName("ID Origem")
	public Integer getIdOrigem() {
		return idOrigem;
	}
	
	public void setOrigemOrdemCompra(OrigemOrdemCompra origemOrdemCompra) {
		this.origemOrdemCompra = origemOrdemCompra;
	}
	public void setIdOrigem(Integer idOrigem) {
		this.idOrigem = idOrigem;
	}
}