package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Reserva;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Romaneioitem;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Romaneiosituacao;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_MovimentacaoEstoque;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Movimentacaoanimalmotivo;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.GerenciamentomaterialService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.ReservaService;
import br.com.linkcom.sined.geral.service.RomaneioService;
import br.com.linkcom.sined.geral.service.RomaneioitemService;
import br.com.linkcom.sined.geral.service.RomaneioorigemService;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.RegistroMovimentacaoAnimalBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.RegistroMovimentacaoAnimalMaterialBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarPlaquetaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RegistrarConsumoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterial;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.RomaneioGerenciamentoMaterialItem;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorGerenciamentoMaterial;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.GerenciamentomaterialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.persistence.QueryBuilderSined;

import com.ibm.icu.text.DecimalFormat;

@Controller(path="/suprimento/crud/Gerenciamentomaterial", authorizationModule=CrudAuthorizationModule.class)
public class GerenciamentomaterialCrud extends CrudControllerSined<GerenciamentomaterialFiltro, Vgerenciarmaterial, Vgerenciarmaterial>{

	private PatrimonioitemService patrimonioitemService;
	private GerenciamentomaterialService gerenciamentomaterialService;
	private MaterialService materialService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private RomaneioService romaneioService;
	private RomaneioitemService romaneioitemService;
	private RomaneioorigemService romaneioorigemService;
	private InventarioService inventarioService;
	private LocalarmazenagemService localarmazenagemService;
	private LoteestoqueService loteestoqueService;
	private EmpresaService empresaService;
	private MaterialtipoService materialtipoService;
	private PedidovendatipoService pedidovendatipoService;
	private ParametrogeralService parametrogeralService;
	private CentrocustoService centrocustoService ;
	private ProjetoService projetoService;
	private RateioService rateioService;
	private ReservaService reservaService;
		
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setRomaneioorigemService(RomaneioorigemService romaneioorigemService) {
		this.romaneioorigemService = romaneioorigemService;
	}
	public void setRomaneioitemService(RomaneioitemService romaneioitemService) {
		this.romaneioitemService = romaneioitemService;
	}
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setGerenciamentomaterialService(
			GerenciamentomaterialService gerenciamentomaterialService) {
		this.gerenciamentomaterialService = gerenciamentomaterialService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {
		this.pedidovendatipoService = pedidovendatipoService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	
	public void setReservaService(ReservaService reservaService) {
		this.reservaService = reservaService;
	}
	@Override
	public void updateQueryCountTotalregistrosPaginacaoAjax(QueryBuilderSined<Long> query, GerenciamentomaterialFiltro filtro) {
		boolean exibirQtdereservada = filtro.getEmpresa() != null;
		if(exibirQtdereservada){
			query.select("count(distinct vgerenciarmaterial.concatedempresa)");
		} else {
			query.select("count(distinct vgerenciarmaterial.concated)");
		}
	}
	
	
	@Override
	protected boolean isPageSizeLimited() {
		return false;
	}
	
	@Override
	public void calcularTotalRegistrosPaginacaoAJax(WebRequestContext request, GerenciamentomaterialFiltro filtro) {
		Boolean fromSelecionarLoteestoque = filtro.getFromSelecionarLoteestoque();
		doFilter(request, filtro);
		filtro.setFromSelecionarLoteestoque(fromSelecionarLoteestoque);
		
		if(filtro.getMinimo() != null || filtro.getMaximo() != null || filtro.getLoteestoque() != null || (filtro.getAbaixominimo() != null && filtro.getAbaixominimo()) || 
				(filtro.getAgruparporlote() == null || !filtro.getAgruparporlote())){
			filtro.setPageSize(Integer.MAX_VALUE);
			ListagemResult<Vgerenciarmaterial> listagemResult = super.getLista(request, filtro);
			List<Vgerenciarmaterial> list = listagemResult.list();
			
			Long totalRegistrosPaginacao = list != null ? new Long(list.size()) : 0; 
			View view = View.getCurrent();
			view.println("var totalRegistrosPaginacao = " + totalRegistrosPaginacao + ";");
		}else {
			super.calcularTotalRegistrosPaginacaoAJax(request, filtro);
		}
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, GerenciamentomaterialFiltro filtro) throws CrudException {
		doFilter(request, filtro);
		return super.doListagem(request, filtro);
	}
	

	
	private void doFilter(WebRequestContext request, GerenciamentomaterialFiltro filtro) {
		filtro.setFiltroItemGrade(false);
		if(filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null && 
				materialService.isControleMaterialitemgrade(filtro.getMaterial())){
			List<Material> listaItemGrade = materialService.findMaterialitemByMaterialmestregrade(filtro.getMaterial());
			if(listaItemGrade != null && !listaItemGrade.isEmpty()){
				filtro.setWhereInMaterialItemGrade(CollectionsUtil.listAndConcatenate(listaItemGrade, "cdmaterial", ","));
				filtro.setFiltroItemGrade(true);
			}
		}
		String cdempresa = request.getParameter("cdempresaFiltro");
		if(cdempresa != null && !cdempresa.trim().isEmpty()){
			filtro.setEmpresa(empresaService.load(new Empresa(Integer.parseInt(cdempresa)), "empresa.cdpessoa, empresa.nome, empresa.nomefantasia"));
		}
		String cdlocalarmazenagem = request.getParameter("cdlocalarmazenagemFiltro");
		if(cdlocalarmazenagem != null && !cdlocalarmazenagem.trim().isEmpty()){
			filtro.setLocalarmazenagem(localarmazenagemService.load(new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem)), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
		}
		String cdmaterial = request.getParameter("cdmaterialFiltro");
		if(cdmaterial != null && !cdmaterial.trim().isEmpty()){
			filtro.setMaterial(new Material(Integer.parseInt(cdmaterial)));
		}
		String fromSelecionarLoteestoque = request.getParameter("fromSelecionarLoteestoque");
		filtro.setFromSelecionarLoteestoque("true".equalsIgnoreCase(fromSelecionarLoteestoque));

		request.setAttribute("fromSelecionarLoteestoque", filtro.getFromSelecionarLoteestoque());
		
		filtro.setWhereInEmpresaRestricaoUsuario(null);
		filtro.setWhereInLocalRestricaoUsuario(null);
		if(filtro.getLocalarmazenagem() == null){
			filtro.setWhereInLocalRestricaoUsuario(localarmazenagemService.getWhereInLocalRestricaoUsuarioLogado());
		}
		if(filtro.getEmpresa() == null){
			filtro.setWhereInEmpresaRestricaoUsuario(new SinedUtil().getListaEmpresa());
		}
		
		if("vgerenciarmaterial.materialclassenome".equals(request.getParameter("orderBy")) || "vgerenciarmaterial.materialclassenome".equals(filtro.getOrderBy())){
			filtro.setOrderBy("");
			filtro.setOrderClasse(true);
		} else{
			filtro.setOrderClasse(false);
		}
	}
	
	@Override
	protected ListagemResult<Vgerenciarmaterial> getLista(WebRequestContext request, GerenciamentomaterialFiltro filtro) {
		ListagemResult<Vgerenciarmaterial> listagemResult = super.getLista(request, filtro);
		List<Vgerenciarmaterial> list = listagemResult.list();
		if(list != null){
			for (Vgerenciarmaterial item : list) {
				Double qtdeminima = materialService.getQtdeMinimaMaterial(item.getCdmaterial(), item.getLocalarmazenagem());
				if(qtdeminima != null){
					item.setQtdeMininaProduto(qtdeminima);
				}
				if(item.getLoteestoque() != null){
					item.getLoteestoque().montaDescriptionAutocomplete(new Material(item.getCdmaterial()));
					item.getLoteestoque().setValidade(loteestoqueService.getValidadeByMaterialLote(item.getLoteestoque(), new Material(item.getCdmaterial())));
				}
			}
		}
		if(filtro.getOrderClasse()){
			if (filtro.isAsc()) {
				Collections.sort(list,new Comparator<Vgerenciarmaterial>(){
					public int compare(Vgerenciarmaterial o1, Vgerenciarmaterial o2) {
						return o1.getMaterialclassenome().compareTo(o2.getMaterialclassenome());
					}
				});
			} else {
				Collections.sort(list,new Comparator<Vgerenciarmaterial>(){
					public int compare(Vgerenciarmaterial o1, Vgerenciarmaterial o2) {
						return o2.getMaterialclassenome().compareTo(o1.getMaterialclassenome());
					}
				});
			}
			filtro.setOrderBy("vgerenciarmaterial.materialclassenome");
			filtro.setOrderClasse(true);
		}
		
		if(filtro.getExibirTotais() != null && filtro.getExibirTotais()){
			TotalizadorGerenciamentoMaterial total = gerenciamentomaterialService.findForTotalGeral(filtro);
			Double qtdetotalreservada = gerenciamentomaterialService.getQtdeTotalReservada(filtro);
			if(qtdetotalreservada == null) qtdetotalreservada = 0.0;
			
			filtro.setQtdetotal(total.getQtdetotal() != null ? total.getQtdetotal().setScale(2, BigDecimal.ROUND_DOWN).doubleValue() : 0.0);
			filtro.setTotalgeral(total.getValottotal());
			filtro.setQtdetotalreservada(qtdetotalreservada);
			filtro.setPesototal(total.getPesototal() != null ? total.getPesototal().doubleValue() : 0.0);
			filtro.setPesobrutototal(total.getPesobrutototal() != null ? total.getPesobrutototal().doubleValue() : 0.0);
		}
		
		if(list !=null && SinedUtil.isListNotEmpty(list)){
			if(!parametrogeralService.getValorPorNome(Parametrogeral.ORDEM_DIMENSAOVENDA).equals("")){
				for (Vgerenciarmaterial v : list) {
					v.setDimensao(gerenciamentomaterialService.criarDimentacao(v.getLargura(),v.getAltura(),v.getComprimento()));
				}
			}
		}
		
		return listagemResult;
	}
	
	@Override
	protected void listagem(WebRequestContext request, GerenciamentomaterialFiltro filtro)throws Exception {
		List<Materialclasse> listaclasses = new ArrayList<Materialclasse>();
		listaclasses.add(Materialclasse.PRODUTO);
		listaclasses.add(Materialclasse.PATRIMONIO);
		listaclasses.add(Materialclasse.EPI);
		listaclasses.add(Materialclasse.SERVICO);
		if (filtro.getMaterial() != null && filtro.getMaterial().getCdmaterial() != null){
			filtro.setMaterial(materialService.carregaMaterial(filtro.getMaterial()));
		}
		request.setAttribute("exibirmaterialreserva", filtro.getMaterialreserva() != null ? filtro.getMaterialreserva() : false);
		request.setAttribute("listaclasses", listaclasses);	
		request.setAttribute("exibirQtdereservada", filtro.getEmpresa() != null);
		request.setAttribute("listaEmpresa", empresaService.findByUsuario());
		request.setAttribute("listaMaterialtipo", materialtipoService.findAtivos());
		request.setAttribute("utilizaControleGado", parametrogeralService.getBoolean(Parametrogeral.CONTROLE_GADO));
		request.setAttribute("OCULTAR_CUSTO_ESTOQUE", parametrogeralService.getBoolean(Parametrogeral.OCULTAR_CUSTO_ESTOQUE));
		request.setAttribute("visualizarValor", SinedUtil.isUserHasAction(Acao.VISUALIZAR_VALOR_CUSTO_ESTOQUE));
		request.setAttribute("QTDE_DISPONIVEL_CONSIDERAR_RESERVADO", parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO));
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, GerenciamentomaterialFiltro filtro) {
		return new ModelAndView("crud/gerenciamentomaterialListagem");
	}
	
	/**
	 * M�todo que busca patrimonios sem plaquetas e redireciona para pop up para serem inseridas
	 * 
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#findMaterialWithoutPlaqueta(String)
	 * @param request
	 * @param bean
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public ModelAndView popUpPlaqueta(WebRequestContext request, GerarPlaquetaBean bean){
		String itensSelecionados = request.getParameter("selectedItens");
		
		if (bean.getLista() == null || bean.getLista().size() == 0) {
			String[] split = itensSelecionados.split(",");
			if(split != null && split.length != 3){
				request.addError("N�o existe plaqueta(s) dispon�vel(is) para este material.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			if(split != null && !split[2].equals("2")){
				request.addError("O material deve ser um patrim�nio.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			List<Patrimonioitem> lista = patrimonioitemService.findMaterialSemPlaquetaNoLocal(itensSelecionados);
			if(lista == null || lista.size() == 0){
				request.addError("N�o existe plaqueta(s) dispon�vel(is) para este material.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			bean.setLista(lista);
		}
		
		return new ModelAndView("direct:/crud/popup/plaqueta").addObject("bean", bean);
	}
	
	/**
	 * M�todo para salvar as plaquetas dos itens de patrim�nio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#updateListaPlaquetas
	 * @see br.com.linkcom.sined.geral.service.GerenciamentomaterialService#existePlaquetaPreenchida(List)
	 * @param request
	 * @param bean
	 * @author Tom�s T. Rabelo
	 */
	@Input("popUpPlaqueta")
	public void savePlaqueta(WebRequestContext request, GerarPlaquetaBean bean) throws Exception {
		
		if (bean == null || bean.getLista() == null) {
			throw new SinedException("Lista de itens de patrim�nio n�o pode ser nulo.");
		}
		
		if(bean.getLista().size() > 0){
			patrimonioitemService.updateListaPlaquetas(bean.getLista());
			if(gerenciamentomaterialService.existePlaquetaPreenchida(bean.getLista())){
				request.addMessage("Plaqueta(s) gerada(s) com sucesso.", MessageType.INFO);
			}
		}
		request.getServletResponse().setContentType("text/html");
		SinedUtil.redirecionamento(request, "/suprimento/crud/Gerenciamentomaterial");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Vgerenciarmaterial bean) throws Exception {
		throw new SinedException("A��o inv�lida");
	}
	
	@Override
	protected Vgerenciarmaterial criar(WebRequestContext request, Vgerenciarmaterial form) throws Exception {
		throw new SinedException("A��o inv�lida");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Vgerenciarmaterial form) throws Exception {
		throw new SinedException("A��o inv�lida");
	}
	
	
	/**
	 * M�todo que abre a popup para o Gerar Romaneio
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public ModelAndView abrirRomaneio(WebRequestContext request){		
		String itensSelecionados = request.getParameter("selectedItens");
		String[] itens = itensSelecionados.split(",");
		
		Empresa empresa = request.getParameter("cdempresa") != null ? new Empresa(Integer.parseInt(request.getParameter("cdempresa"))) : null;
		Loteestoque loteestoqueFiltro = request.getParameter("cdloteestoque") != null ? loteestoqueService.carregaLoteestoque(new Loteestoque(Integer.parseInt(request.getParameter("cdloteestoque")))) : null;
		
		List<GerarRomaneioBean> lista = new ArrayList<GerarRomaneioBean>();
		HashMap<Localarmazenagem, Boolean> mapLocal = new HashMap<Localarmazenagem, Boolean>();
		Map<String, List<Loteestoque>> mapaLoteestoque = new HashMap<String, List<Loteestoque>>();
		for (int i = 0; i < itens.length; i++) {
			String[] campos = itens[i].split("\\|");
			
			Material material = new Material(Integer.parseInt(campos[0]));
			Localarmazenagem localarmazenagem = campos[1] != null && !"".equals(campos[1]) ? new Localarmazenagem(Integer.parseInt(campos[1])) : null;
			if(localarmazenagem != null && mapLocal.get(localarmazenagem) == null){
				mapLocal.put(localarmazenagem, localarmazenagemService.getPermitirestoquenegativo(localarmazenagem));
			}
			Materialclasse materialclasse = new Materialclasse(Integer.parseInt(campos[2]));
			Loteestoque loteestoque = null;
			Boolean permiteestoquenegativo = false; 
			try {
				if(campos.length > 3 && campos[3] != null && !"".equals(campos[3])){
					loteestoque = loteestoqueService.carregaLoteestoque(new Loteestoque(Integer.parseInt(campos[3])));
				}else if(loteestoqueFiltro != null){
					loteestoque = loteestoqueFiltro;
				}
				if(loteestoque != null){
					if(mapaLoteestoque.get(material.getCdmaterial()) == null){
						permiteestoquenegativo = mapLocal.get(localarmazenagem);
						List<Loteestoque> listaLoteestoque = loteestoqueService.findQtdeLoteestoque(material, localarmazenagem, null, (permiteestoquenegativo == null || !permiteestoquenegativo)) ;
						mapaLoteestoque.put(material.getCdmaterial().toString() + "-" + 
								(localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() != null ? localarmazenagem.getCdlocalarmazenagem().toString() : "null"), 
								listaLoteestoque != null ? listaLoteestoque : new ArrayList<Loteestoque>());
					}
				}
			} catch (Exception e) {}
			
			GerarRomaneioBean bean = new GerarRomaneioBean();
			bean.setMaterial(material);
			bean.setLocalarmazenagem(localarmazenagem);
			bean.setMaterialclasse(materialclasse);
			bean.setLoteestoque(loteestoque);
			bean.setEmpresa(empresa);
			
			lista.add(bean);
		}
		
		request.setAttribute("mapaLoteestoque", mapaLoteestoque);
		request.setAttribute("fromGerenciamentoestoque", Boolean.TRUE);
		
		return romaneioService.abrirTelaGerarRomaneio(request, lista, null, true, false, null);	
	}
	
	/**
	 * Action que salva a popup de Gerar Romaneio.
	 *
	 * @param request
	 * @param form
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public void saveRomaneio(WebRequestContext request, RomaneioGerenciamentoMaterial form){
		
		Map<String, List<RomaneioGerenciamentoMaterialItem>> mapLocalOrigemItens = new HashMap<String, List<RomaneioGerenciamentoMaterialItem>>();
		List<RomaneioGerenciamentoMaterialItem> listaMateriais = form.getListaMateriais();
		
		// PREECHE UM MAP PAR SEPARAR OS LOCAIS DE ORIGEM
		for (RomaneioGerenciamentoMaterialItem item : listaMateriais) {
			List<RomaneioGerenciamentoMaterialItem> lista;
			
			if(mapLocalOrigemItens.containsKey(getKeyHashMapRomaneioGerenciamentoMaterialItem(item))){
				lista = mapLocalOrigemItens.get(getKeyHashMapRomaneioGerenciamentoMaterialItem(item));
			} else {
				lista = new ArrayList<RomaneioGerenciamentoMaterialItem>();
			}
			
			lista.add(item);
			mapLocalOrigemItens.put(getKeyHashMapRomaneioGerenciamentoMaterialItem(item), lista);
		}
		
		// PARA CADA LOCAL DE ORIGEM SER� CRIADO UM REGISTRO DE ROMANEIO
		Set<Entry<String, List<RomaneioGerenciamentoMaterialItem>>> entrySet = mapLocalOrigemItens.entrySet();
		for (Entry<String, List<RomaneioGerenciamentoMaterialItem>> entry : entrySet) {
			String[] splitAux = entry.getKey().split("\\|");
			Projeto projetoorigem = null;
			Localarmazenagem localarmazenagemorigem = null;
			if(splitAux.length > 0 && splitAux[0] != null && StringUtils.isNotBlank(splitAux[0])){
				localarmazenagemorigem = new Localarmazenagem(Integer.parseInt(splitAux[0]));
			}
			if(splitAux.length > 1 && splitAux[1] != null && StringUtils.isNotBlank(splitAux[1])){
				projetoorigem = new Projeto(Integer.parseInt(splitAux[1]));
			}else {
				projetoorigem = form.getProjetodestino();
			}
			
			// CRIA��O DO ROMANEIO
			Romaneio romaneio = new Romaneio();
			romaneio.setDtromaneio(SinedDateUtils.currentDate());
			romaneio.setDescricao("Romaneio manual");
			romaneio.setEmpresa(form.getEmpresadestino());
			romaneio.setLocalarmazenagemdestino(form.getLocalarmazenagemdestino());
			romaneio.setLocalarmazenagemorigem(localarmazenagemorigem);
			romaneio.setProjeto(form.getProjetodestino() != null ? form.getProjetodestino() : projetoorigem);
			romaneio.setConsiderarProjetoorigem(true);
			romaneio.setRomaneiosituacao(Romaneiosituacao.BAIXADA);
			romaneio.setDtromaneio(form.getDtromaneio());
			romaneio.setCliente(form.getClientedestino());
			romaneio.setPlaca(form.getPlaca());
			romaneio.setMotorista(form.getMotorista());
			
			romaneioService.saveOrUpdate(romaneio);
			
			// CRIA��O DA ORIGEM DO ROMANEIO
			Romaneioorigem romaneioorigem = new Romaneioorigem();
			romaneioorigem.setEstoque(true);
			romaneioorigem.setRomaneio(romaneio);
			
			romaneioorigemService.saveOrUpdate(romaneioorigem);
			
			List<RomaneioGerenciamentoMaterialItem> lista = entry.getValue();
			for (RomaneioGerenciamentoMaterialItem item : lista) {
				// CRIA��O DO ITEM ROMANEIO
				Romaneioitem romaneioitem = new Romaneioitem();
				romaneioitem.setMaterial(item.getMaterial());
				romaneioitem.setQtde(item.getQtdedestino());
				romaneioitem.setMaterialclasse(item.getMaterialclasse());
				romaneioitem.setRomaneio(romaneio);
				romaneioitem.setLoteestoque(item.getLoteestoque());
				
				romaneioitemService.saveOrUpdate(romaneioitem);
				
				// GERA A ENTRADA E SA�DA DO ROMANEIO
				romaneioService.gerarEntradaSaidaRomaneio(romaneio, romaneioitem, item.getValor(), form.getEmpresadestino(), form.getProjetodestino(), form.getObservacao(), form.getEmpresadestino(), projetoorigem, false, form.getClientedestino(), null, item.getCentrocusto(), null);
			}
		}
		
		request.addMessage("Romaneio gerado com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	* M�todo que cria o id com o cdlocalarmazenagem concatenacom com pipe '|' e o cdprojeto
	*
	* @param item
	* @return
	* @since 23/10/2015
	* @author Luiz Fernando
	*/
	private String getKeyHashMapRomaneioGerenciamentoMaterialItem(RomaneioGerenciamentoMaterialItem item){
		String cdlocalarmazenagemorigem = item != null && item.getLocalarmazenagemorigem() != null && item.getLocalarmazenagemorigem().getCdlocalarmazenagem() != null ? item.getLocalarmazenagemorigem().getCdlocalarmazenagem().toString() : "";
		String cdprojetoorigem = item != null && item.getProjetoorigem() != null && item.getProjetoorigem().getCdprojeto() != null ? item.getProjetoorigem().getCdprojeto().toString() : "";
		return cdlocalarmazenagemorigem + "|" + cdprojetoorigem;
	}
	
	/**
	 * M�todo que gera o resumo do estoque
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 20/09/2013
	 */
	public ModelAndView gerarCSVResumo(WebRequestContext request, GerenciamentomaterialFiltro filtro){
		Boolean porperiodo = Boolean.valueOf(request.getParameter("porperiodo") != null ? request.getParameter("porperiodo") : "false");
		List<Vgerenciarmaterial> listaMaterial = new ArrayList<Vgerenciarmaterial>();
		if(porperiodo){
			listaMaterial = gerenciamentomaterialService.findForReportPeriodo(filtro, false);
		}else {
			listaMaterial = gerenciamentomaterialService.findForCsv(filtro);
		}
		
		Inventario inventario = new Inventario();
		List<Inventariomaterial> listaInventariomaterial = new ArrayList<Inventariomaterial>();
		inventario.setLocalarmazenagem(filtro.getLocalarmazenagem() != null ? localarmazenagemService.load(filtro.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome") : null);
		
		boolean adicionar = true;
		Inventariomaterial inventariomaterial;
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (Vgerenciarmaterial vgm : listaMaterial) {
				if(vgm.getCdmaterial() == null) continue;
				
				adicionar = true;
				inventariomaterial = new Inventariomaterial();
				inventariomaterial.setMaterial(new Material(vgm.getCdmaterial()));
				inventariomaterial.setQtde(vgm.getQtdedisponivel() != null ? vgm.getQtdedisponivel() : 0.);
				inventariomaterial.setValorUnitario(vgm.getValorcustomaterialTrans().getValue().doubleValue());
				
				if(!listaInventariomaterial.isEmpty()){
					for(Inventariomaterial im : listaInventariomaterial){
						if(im.getMaterial() != null && inventariomaterial.getMaterial() != null &&
								im.getMaterial().equals(inventariomaterial.getMaterial())){
							adicionar = false;
							im.setQtde(im.getQtde()+inventariomaterial.getQtde());
						}
					}
				}
				
				if(adicionar){
					listaInventariomaterial.add(inventariomaterial);
				}
			}
			inventario.setListaInventariomaterial(listaInventariomaterial);
		}
		
		String rel = inventarioService.gerarCSVInventario(inventario);
		
		Resource resource = new Resource("text/csv","gerenciamentomaterial_resumo" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll("null;", ";").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	public ModelAndView gerarCSV(WebRequestContext request, GerenciamentomaterialFiltro filtro){
		Boolean porperiodo = Boolean.valueOf(request.getParameter("porperiodo") != null ? request.getParameter("porperiodo") : "false");
		Boolean qtdeDisponivelConsiderarReservado = parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO);
		
		List<Vgerenciarmaterial> lista = new ArrayList<Vgerenciarmaterial>();
		
		if(porperiodo){
			lista = gerenciamentomaterialService.findForReportPeriodo(filtro, false);
		}else {
			lista = gerenciamentomaterialService.findForCsv(filtro);
		}
		boolean controle_gado = parametrogeralService.getBoolean(Parametrogeral.CONTROLE_GADO);
		boolean ocultarCusto = parametrogeralService.getBoolean(Parametrogeral.OCULTAR_CUSTO_ESTOQUE);
		Boolean permissao = SinedUtil.isUserHasAction(Acao.VISUALIZAR_VALOR_CUSTO_ESTOQUE);
				
		StringBuilder rel = new StringBuilder();
		
		if(lista != null && lista.size() > 0){
			boolean origemtelavenda = false; 
			if(request.getParameter("telavenda") != null && "true".equals(request.getParameter("telavenda"))){
				origemtelavenda = true;
			}
			
			boolean vendacomexpedicao = false;
			Pedidovendatipo pedidovendatipo = request.getParameter("cdpedidovendatipo") != null ? new Pedidovendatipo(Integer.parseInt(request.getParameter("cdpedidovendatipo"))) : null;
			if(pedidovendatipo != null){
				vendacomexpedicao = pedidovendatipoService.gerarExpedicaoVenda(pedidovendatipo, true);
			}
			
			String whereInMat = CollectionsUtil.listAndConcatenate(lista, "cdmaterial", ",");
			List<Material> listaMaterial = materialService.findForCsvGerenciamentoMaterial(whereInMat);
			List<Material> listaMaterialEmExpedicaoAndEmCompra = new ArrayList<Material>(); 
			
			if(whereInMat != null && !"".equals(whereInMat)){
				listaMaterialEmExpedicaoAndEmCompra = materialService.findEmExpedicaoAndEmCompra(whereInMat);
			}
			
			rel.append("Local;");
			if(Boolean.TRUE.equals(filtro.getAgruparporlote())){
				rel.append("Lote;");
			}
			rel.append("Refer�ncia;");
			rel.append("C�digo do Material;");
			rel.append("Identificador;");
			rel.append("Material;");
			rel.append("Grupo de Material;");
			
			if(porperiodo){
				rel.append("Data;");
			}
			if(controle_gado){
				rel.append("Peso bruto;");
			}
			if(qtdeDisponivelConsiderarReservado){
				rel.append("Qtd. Atual;");
				rel.append("Qtd. Atual Unidade Secund�ria;");
				rel.append("Qtd. Dispon�vel;");
			}else {
				rel.append("Qtd. Dispon�vel;");
				rel.append("Qtd. Dispon�vel Unidade Secund�ria;");				
			}
			rel.append("Unidade Medida;");
			
			if(!ocultarCusto && !origemtelavenda && permissao){
				rel.append("Valor Unit�rio;");
				rel.append("Total;");
				rel.append("Valor Unit�rio Unidade Secund�ria;");
				rel.append("Total Unidade Secund�ria;");
			}
			if(origemtelavenda){
				if(vendacomexpedicao){
					rel.append("Em expedi��o;");
				}
				rel.append("Em compra;");
			}
			rel.append("Qtde Reservada;");
			rel.append("Total Peso L�quido;");
			rel.append("Total Peso Bruto;");
			rel.append("Localiza��o de Estoque;");
			rel.append("Dimens�o;");
			rel.append("\n");
			
			DecimalFormat formataNumero = new DecimalFormat("#,###.00");
			
			Material mat = null;
			Material matEmExpedicaoEmcompra = null;
			String qtdeString = "";
			Double qtdeunidadesecundaria = 0.0;
			Double valorunidadesecundaria = 0.0;
			if(listaMaterial != null && listaMaterial.size() > 0){
				for (Vgerenciarmaterial vgm : lista) {
					mat = new Material(vgm.getCdmaterial());
					
					if(!listaMaterial.contains(mat))
						continue;
					
					mat = listaMaterial.get(listaMaterial.indexOf(mat));
					
					rel.append(vgm.getLocalarmazenagem().getNome()).append(";");
					if(Boolean.TRUE.equals(filtro.getAgruparporlote())){
						if(vgm.getLoteestoque() != null){
							rel.append(vgm.getLoteestoque().getNumerovalidade()).append(";");
						}else {
							rel.append(";");
						}
					}
					rel.append(mat.getReferencia()).append(";");
					rel.append((vgm.getCdmaterial() != null ? "'" + vgm.getCdmaterial() + "'" : "")).append(";");
					rel.append((vgm.getIdentificacao() != null ? "'" + vgm.getIdentificacao() + "'" : "")).append(";");
					rel.append("\"").append(mat.getNome().replaceAll("\"", "")).append("\"").append(";");
					
					if(mat.getMaterialgrupo() != null && mat.getMaterialgrupo().getNome() != null)
						rel.append(mat.getMaterialgrupo().getNome());
					rel.append(";");
					
					if(porperiodo){
						rel.append(vgm.getDtmovimentacao()).append(";");
					}
					
					if(controle_gado){
						String pesobruto = vgm.getPesobruto() != null? vgm.getPesobruto().toString()+";": ";";
						rel.append(pesobruto.replace(".", ","));
					}
					
					if(vgm.getQtdedisponivel() != null){
						qtdeString = vgm.getQtdedisponivel().toString();
						rel.append(qtdeString.replace(".", ","));
					}
					rel.append(";;");
					
					if(qtdeDisponivelConsiderarReservado){
						Double qtdeReservada = 0.0;
						if(vgm.getQtdereservada() != null){
							qtdeReservada = vgm.getQtdereservada();
						}
						Double qtdeDisponivel = vgm.getQtdedisponivel() - qtdeReservada;
						String qntdDisponivelString = qtdeDisponivel.toString();
						rel.append(qntdDisponivelString.replace(".", ",") + ";");			
					}
		
					if(mat.getUnidademedida() != null && mat.getUnidademedida().getNome() != null){
						rel.append(mat.getUnidademedida().getNome());
					}
					if(!ocultarCusto && !origemtelavenda){
						rel.append(";");
						rel.append(vgm.getValorcustomaterial() != null ? formataNumero.format(new Double(SinedUtil.roundByParametro(vgm.getValorcustomaterial())/100)): "").append(";");
						rel.append(vgm.getTotal() != null ? formataNumero.format(vgm.getTotal().getValue().doubleValue()).toString(): "");
					}
					
					if(!ocultarCusto && !origemtelavenda){
						rel.append(";");
						rel.append(";");
					}
					
					if(origemtelavenda && listaMaterialEmExpedicaoAndEmCompra.contains(mat)){
						rel.append(";");
						matEmExpedicaoEmcompra = listaMaterialEmExpedicaoAndEmCompra.get(listaMaterialEmExpedicaoAndEmCompra.indexOf(mat));
						if(matEmExpedicaoEmcompra != null){
							if(vendacomexpedicao){
								rel.append(matEmExpedicaoEmcompra.getQtdeemexpedicao()).append(";");
							}
							rel.append(matEmExpedicaoEmcompra.getQtdeemcompra());
						}
					}
					
					rel.append(";");
					rel.append(SinedUtil.descriptionDecimal(vgm.getQtdereservada()));
					
					rel.append(";");
					if(mat.getPeso() != null){
						if(mat.getUnidademedida()!=null && mat.getUnidademedida().getCasasdecimaisestoque()!=null)
							rel.append(vgm.getQtdedisponivel() != null ? SinedUtil.roundByUnidademedida(vgm.getQtdedisponivel()*mat.getPeso(),mat.getUnidademedida()).toString().replace(".", ","): "");
						else
							rel.append(vgm.getQtdedisponivel() != null ? SinedUtil.roundByParametro(vgm.getQtdedisponivel()*mat.getPeso()).toString().replace(".", ","): "");
					}
					
					rel.append(";");
					if(mat.getPesobruto() != null){
						if(mat.getUnidademedida()!=null && mat.getUnidademedida().getCasasdecimaisestoque()!=null)
							rel.append(vgm.getQtdedisponivel() != null ? SinedUtil.roundByUnidademedida(vgm.getQtdedisponivel()*mat.getPesobruto(),mat.getUnidademedida()).toString().replace(".", ","): "");
						else
							rel.append(vgm.getQtdedisponivel() != null ? SinedUtil.roundByParametro(vgm.getQtdedisponivel()*mat.getPesobruto()).toString().replace(".", ","): "");
					}
					rel.append(";");
					
					if(mat.getListaMaterialunidademedida() != null && !mat.getListaMaterialunidademedida().isEmpty()){				
						for(Materialunidademedida materialunidademedida : mat.getListaMaterialunidademedida()){
							if(materialunidademedida.getMostrarconversao() != null && materialunidademedida.getMostrarconversao() && 
									materialunidademedida.getFracao() != null && materialunidademedida.getUnidademedida() != null){	
								qtdeunidadesecundaria = materialunidademedida.getFracaoQtdereferencia() * vgm.getQtdedisponivel();
								qtdeString = qtdeunidadesecundaria.toString();
								rel.append("\n;;;;;;" + (Boolean.TRUE.equals(filtro.getAgruparporlote()) ? ";" : "") + (porperiodo ? ";" : "")).append(qtdeString.replace(".", ","));
								rel.append(";").append(materialunidademedida.getUnidademedida().getNome()).append(";;;");
								if(!origemtelavenda){
									if(vgm.getValorcustomaterial() != null && vgm.getValorcustomaterial() > 0){
										valorunidadesecundaria = materialunidademedida.getFracaoQtdereferencia() / vgm.getValorcustomaterial();
										if(materialunidademedida.getUnidademedida()!=null && materialunidademedida.getUnidademedida().getCasasdecimaisestoque()!=null)
											rel.append(SinedUtil.roundByUnidademedida(valorunidadesecundaria, materialunidademedida.getUnidademedida()).toString().replace(".", ","));
										else
											rel.append(SinedUtil.roundByParametro(valorunidadesecundaria).toString().replace(".", ","));
									}
									rel.append(";");
									if(qtdeunidadesecundaria != null && valorunidadesecundaria != null){
										valorunidadesecundaria = qtdeunidadesecundaria * valorunidadesecundaria;
										if(materialunidademedida.getUnidademedida()!=null && materialunidademedida.getUnidademedida().getCasasdecimaisestoque()!=null)
											rel.append(SinedUtil.roundByUnidademedida(valorunidadesecundaria,materialunidademedida.getUnidademedida()).toString().replace(".", ","));
										else
											rel.append(SinedUtil.roundByParametro(valorunidadesecundaria).toString().replace(".", ","));
									}
									rel.append(";");
								}
							}
						}
					}
					
					rel.append(mat.getLocalizacaoestoque() != null ? mat.getLocalizacaoestoque().getDescricao() : "");
					rel.append(";");
					rel.append(gerenciamentomaterialService.criarDimentacao(vgm.getLargura(),vgm.getAltura(),vgm.getComprimento()));
					
					rel.append("\n");
				}
			}
		}
		
		Resource resource = new Resource("text/csv","gerenciamentomaterial_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll("null;", ";").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	/**
	 * M�todo para gerar CSV a partir da tela de entrada da venda/pedido de venda
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarCSVLocal(WebRequestContext request){
		String cdlocalarmazenagem = request.getParameter("cdlocalarmazenagem");
		if(cdlocalarmazenagem == null || "".equals(cdlocalarmazenagem)){
			throw new SinedException("Par�metro inv�lido.");
		}		
		
		GerenciamentomaterialFiltro filtro = new GerenciamentomaterialFiltro();
		Localarmazenagem localarmazenagem = new Localarmazenagem();
		localarmazenagem.setCdlocalarmazenagem(Integer.parseInt(cdlocalarmazenagem));
		filtro.setLocalarmazenagem(localarmazenagem);
		
		return gerarCSV(request, filtro);
	}
	
	/**
	 * 
	 * M�todo que faz a chamada da popUp de Registro do consumo dos materiais..
	 *
	 * @name abrePopUpRegistrarConsumo
	 * @param request
	 * @return
	 * @return ModelAndView
	 * @author Thiago Augusto
	 * @date 03/07/2012
	 *
	 */
	public ModelAndView abrePopUpRegistrarConsumo(WebRequestContext request){		
		String itensSelecionados = request.getParameter("selectedItens");
		String[] split = itensSelecionados.split(",");
		Empresa empresa = null;
		try {
			empresa = new Empresa(Integer.parseInt(request.getParameter("idempresafiltro")));
		} catch (Exception e) {}
		
		List<Movimentacaoestoque> lista = this.makeListaRegistrarConsumo(split, empresa);
		if(lista != null && !lista.isEmpty()){
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				for(Movimentacaoestoque movimentacaoestoque : lista){
					if(movimentacaoestoque.getMaterial() != null && (movimentacaoestoque.getMaterial().getMaterialRateioEstoque() == null || movimentacaoestoque.getMaterial().getMaterialRateioEstoque().getContaGerencialConsumo() == null)){
						request.addError("Para realizar o consumo � obrigat�rio ter a conta gerencial de consumo preenchida na aba Rateio de Estoque do cadastro do material. (" + movimentacaoestoque.getMaterialDescricao() + ")");
						SinedUtil.fechaPopUp(request);
						return null;
					}
				}
			}
			
			Aux_MovimentacaoEstoque bean = new Aux_MovimentacaoEstoque();
			bean.setListaMovimentacaoEstoque(lista);
			
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			if(empresa != null){
				listaEmpresa.add(empresaService.load(empresa, "empresa.cdpessoa, empresa.nome, empresa.nomefantasia"));
			}else {
				listaEmpresa.addAll(empresaService.findByUsuario());
			}
			
			//Verifica se s� existe 1 local, empresa e projeto cadastrados no sistema. Caso sim, j� traz preenchido na tela
			List<Localarmazenagem> listaLocaisArmazenagem = localarmazenagemService.findAtivos();
			 if(listaLocaisArmazenagem.size() == 1){
				 for (Movimentacaoestoque movimentacaoestoque : bean.getListaMovimentacaoEstoque()) {
					 movimentacaoestoque.setLocalarmazenagem(listaLocaisArmazenagem.get(0));	
				}
			 }
			 
			 List<Empresa> listaEmpresas = empresaService.findAtivos();
			 if(listaEmpresas.size() == 1){
				 for (Movimentacaoestoque movimentacaoestoque : bean.getListaMovimentacaoEstoque()) {
					 movimentacaoestoque.setEmpresa(listaEmpresas.get(0));
				 }
			 }
			 
			 List<Projeto> listaProjetos =  new ArrayList<Projeto>();
			 listaProjetos = projetoService.findProjetosAtivos(listaProjetos, false);
			 if(listaProjetos.size() == 1) {
				 for (Movimentacaoestoque movimentacaoestoque : bean.getListaMovimentacaoEstoque()){
					 movimentacaoestoque.setProjeto(listaProjetos.get(0));					 
				 }
			 }
			
			request.setAttribute("listaEmpresa", listaEmpresa);
			request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
			request.setAttribute("listaProjeto", projetoService.findForComboByUsuariologadoNotCancelado());
			return new ModelAndView("direct:/crud/popup/registrarconsumo").addObject("bean", bean);	
		} else {
			request.addError("N�o foi encontrado nenhum produto ou servi�o com o(s) item(ns) selecionado(s).");
			SinedUtil.fechaPopUp(request);
			return null;
		}	
	}
	
	/**
	 * Monta a lista para exibir na tela de registrar consumo
	 *
	 * @param split
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/11/2013
	 */
	private List<Movimentacaoestoque> makeListaRegistrarConsumo(String[] split, Empresa empresa) {
		List<Movimentacaoestoque> lista = new ArrayList<Movimentacaoestoque>();
		List<RegistrarConsumoBean> listaAux = new ArrayList<RegistrarConsumoBean>();
		for (int i = 0; i < split.length; i++) {
			String[] splitAux = split[i].split("\\|");
			
			Integer cdmaterial = splitAux[0] != null && !splitAux[0].equals("") ? Integer.parseInt(splitAux[0]) : null;
			Integer cdlocalarmazenagem = splitAux[1] != null && !splitAux[1].equals("") ? Integer.parseInt(splitAux[1]) : null;
			Integer cdmaterialclasse = splitAux[2] != null && !splitAux[2].equals("") ? Integer.parseInt(splitAux[2]) : null;
			Integer cdloteestoque = splitAux.length > 3 && splitAux[3] != null && !splitAux[3].equals("") ? Integer.parseInt(splitAux[3]) : null;
			
			RegistrarConsumoBean it = new RegistrarConsumoBean();
			it.setCdmaterial(cdmaterial);
			it.setCdlocalarmazenagem(cdlocalarmazenagem);
			it.setCdmaterialclasse(cdmaterialclasse);
			it.setCdloteestoque(cdloteestoque);
			listaAux.add(it);
		}
		
		List<Material> listaMaterial = materialService.findForRegistrarConsumno(CollectionsUtil.listAndConcatenate(listaAux, "cdmaterial", ","));
		List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadWithEndereco(CollectionsUtil.listAndConcatenate(listaAux, "cdlocalarmazenagem", ","));
		
		for (RegistrarConsumoBean aux : listaAux) {
			Material material = aux.getCdmaterial() != null ? new Material(aux.getCdmaterial()) : null;
			Localarmazenagem localarmazenagem = aux.getCdlocalarmazenagem() != null ? new Localarmazenagem(aux.getCdlocalarmazenagem()) : null;
			Materialclasse materialclasse = aux.getCdmaterialclasse() != null ? new Materialclasse(aux.getCdmaterialclasse()) : null;
			Loteestoque loteestoque = aux.getCdloteestoque() != null ? new Loteestoque(aux.getCdloteestoque()) : null;
			
			if(material != null && listaMaterial.indexOf(material) != -1)
				material = listaMaterial.get(listaMaterial.indexOf(material));
			if(localarmazenagem != null && listaLocalarmazenagem.indexOf(localarmazenagem) != -1)
				localarmazenagem = listaLocalarmazenagem.get(listaLocalarmazenagem.indexOf(localarmazenagem));
			
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setMaterial(material);
			movimentacaoestoque.setLoteestoque(loteestoque);
			movimentacaoestoque.setLocalarmazenagem(localarmazenagem);
			movimentacaoestoque.setEmpresa(empresa);
			movimentacaoestoque.setMaterialclasse(materialclasse);
			movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
			movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
			movimentacaoestoque.setValor(movimentacaoestoque.getMaterial().getValorcusto());
			movimentacaoestoque.setQtdeTotal(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServicoFromConferencia(material, materialclasse, localarmazenagem, loteestoque, empresa));
			movimentacaoestoque.setQtde(0.0);
			
			lista.add(movimentacaoestoque);
		}
		return lista;
	}
	
	/**
	 * 
	 * M�todo que salva a lista de Movimenta��oEstoque.
	 *
	 * @name registrarConsumo
	 * @param request
	 * @param bean
	 * @return void
	 * @author Thiago Augusto
	 * @date 04/07/2012
	 *
	 */
	public void registrarConsumo(WebRequestContext request, Aux_MovimentacaoEstoque bean){
		for (Movimentacaoestoque me : bean.getListaMovimentacaoEstoque()) {
			if (me.getQtde() > 0) {
				
				Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
				movimentacaoestoqueorigem.setConsumo(Boolean.TRUE);
				movimentacaoestoqueorigem.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				movimentacaoestoqueorigem.setDtaltera(new Timestamp(System.currentTimeMillis()));	
				
				me.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
				if(SinedUtil.isRateioMovimentacaoEstoque()){
					me.setContaGerencial(me.getMaterial().getMaterialRateioEstoque() != null ? me.getMaterial().getMaterialRateioEstoque().getContaGerencialConsumo() : null);
					if(me.getProjetoRateio() == null){
						me.setProjetoRateio(me.getProjeto() != null ? me.getProjeto() : me.getMaterial().getMaterialRateioEstoque() != null ? me.getMaterial().getMaterialRateioEstoque().getProjeto() : null);
					}
					me.setRateio(rateioService.criarRateio(me));
				}
				me.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
				movimentacaoestoqueService.saveOrUpdate(me);
				movimentacaoestoqueService.salvarHisotirico(me, "Criado a partir do registro de consumo", MovimentacaoEstoqueAcao.CRIAR);
			}
		}
		request.getServletResponse().setContentType("text/html");
		request.addMessage("Registro de consumo realizado com sucesso!");
		SinedUtil.fechaPopUp(request);
		SinedUtil.recarregarPaginaWithClose(request);
	}
	
	public ModelAndView verificarProdutosEstoque(WebRequestContext request){
		JsonModelAndView json = new JsonModelAndView();
		String itensSelecionados = request.getParameter("selectedItens");
		String[] split = itensSelecionados.split(",");
		Empresa empresa = null;
		try {
			empresa = new Empresa(Integer.parseInt(request.getParameter("idempresafiltro")));
		} catch (Exception e) {}
		
		List<Movimentacaoestoque> lista = makeListaRegistrarConsumo(split, empresa);
		
		if (lista == null || lista.isEmpty()){
			json.addObject("retorno", true);
			return json;
		}
		boolean retorno = true;
		for (Movimentacaoestoque me : lista) {
			if(me.getQtdeTotal() <= 0)
				retorno = false;
		}
		json.addObject("retorno", retorno);
		return json;
	}
	
	public ModelAndView abrePopupRegistrarMorteAnimal(WebRequestContext request){
		List<RegistroMovimentacaoAnimalMaterialBean> lista = new ArrayList<RegistroMovimentacaoAnimalMaterialBean>();
		String motivo = request.getParameter("motivoMovimento");
		Movimentacaoanimalmotivo motivoEnum = Util.strings.emptyIfNull(motivo).equals("MORTE")? Movimentacaoanimalmotivo.MORTE: Movimentacaoanimalmotivo.NASCIMENTO;
		String itensSelecionados = SinedUtil.getItensSelecionados(request);
		String[] split = itensSelecionados.split(",");
		
		
		for (int i = 0; i < split.length; i++) {
			String[] splitAux = split[i].split("\\|");
			
			String cdmaterial = splitAux[0];
			Material material = materialService.findForRegitrarMovimentacaoAnimal(cdmaterial).get(0);
			if(material != null){
				if(materialService.obrigarLote(material)){
					request.addError("N�o � poss�vel realizar a a��o para material que possui lote.");
					SinedUtil.redirecionamento(request, "/suprimento/crud/Gerenciamentomaterial");
					return null;
				}
				if(SinedUtil.isRateioMovimentacaoEstoque() && (material.getMaterialRateioEstoque() == null || (motivoEnum.equals(Movimentacaoanimalmotivo.MORTE) && material.getMaterialRateioEstoque().getContaGerencialPerda() == null)
						|| (motivoEnum.equals(Movimentacaoanimalmotivo.NASCIMENTO) && material.getMaterialRateioEstoque().getContaGerencialEntrada() == null))){
					if(motivoEnum.equals(Movimentacaoanimalmotivo.MORTE)){
						request.addError("Para realizar o registro de morte � obrigat�rio ter a conta gerencial de perda preenchida na aba Rateio de Estoque do cadastro do material. Material " + material.getNome());
					}else if(motivoEnum.equals(Movimentacaoanimalmotivo.NASCIMENTO)){
						request.addError("Para realizar o registro de nascimento � obrigat�rio ter a conta gerencial de entrada preenchida na aba Rateio de Estoque do cadastro do material. Material " + material.getNome());
					}
					SinedUtil.redirecionamento(request, "/suprimento/crud/Gerenciamentomaterial");
					return null;
				}
				
				String cdlocalStr = splitAux[1];
				if(cdlocalStr != null && !cdlocalStr.equals("")){
					Integer cdlocalarmazenagem = Integer.parseInt(splitAux[1]);
					Localarmazenagem local = localarmazenagemService.load(new Localarmazenagem(cdlocalarmazenagem));
					material.setLocalarmazenagem(local);					
				}
				RegistroMovimentacaoAnimalMaterialBean bean = new RegistroMovimentacaoAnimalMaterialBean();
				bean.setMaterial(material);
				bean.setLocalarmazenagem(material.getLocalarmazenagem());
				bean.setMotivo(motivoEnum);
				bean.setRegistrapesomedio(material.getMaterialgrupo() != null && Boolean.TRUE.equals(material.getMaterialgrupo().getRegistrarpesomedio()));
				Double estoqueAtual = materialService.findEstoqueAtual(bean.getMaterial().getCdmaterial().toString(), bean.getLocalarmazenagem(), bean.getEmpresa());
				bean.setQuantidadedisponivel(estoqueAtual);
				lista.add(bean);
			}
		}

		RegistroMovimentacaoAnimalBean bean = new RegistroMovimentacaoAnimalBean();
		bean.setListaMateriais(lista);
		request.setAttribute("tituloMovimento", motivoEnum.equals(Movimentacaoanimalmotivo.MORTE)? "REGISTRAR MORTE DE ANIMAL": "REGISTRAR NASCIMENTO DE ANIMAL");
		request.setAttribute("registrandoSaida", motivoEnum.equals(Movimentacaoanimalmotivo.MORTE));
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		request.setAttribute("listaProjeto", projetoService.findForComboByUsuariologadoNotCancelado());
		return new ModelAndView("direct:/crud/popup/registroMovimentacaoAnimal", "bean", bean);
	}
	
	public void saveRegistroMovimentacaoAnimal(WebRequestContext request, RegistroMovimentacaoAnimalBean bean){
		String registrandoMorte = request.getParameter("registrandoSaida");

		if("true".equals(registrandoMorte)){
			movimentacaoestoqueService.registrarMorteAnimal(bean.getListaMateriais());
			request.addMessage("Morte de animal(is) registrada com sucesso.");
		}else{
			movimentacaoestoqueService.registrarNascimentoAnimal(bean.getListaMateriais());
			request.addMessage("Nascimento de animal(is) registrado com sucesso.");
		}

		SinedUtil.redirecionamento(request, "/suprimento/crud/Gerenciamentomaterial");
	}
	
	public ModelAndView ajaxValidaGruposMarcadosComoAnimal(WebRequestContext request, String whereIn){
		String itensSelecionados = request.getParameter("whereIn");
		String materiais = "";
		String[] split = itensSelecionados.split(",");
		for(String str: split){
			materiais += str.substring(0, str.indexOf("|"))+",";
		}
		ModelAndView retorno = new JsonModelAndView();
		if(materiais != null && !materiais.equals("")){
			materiais = materiais.substring(0, materiais.length() - 1);
			List<Material> lista = materialService.findMateriaisWithMaterialGrupoNotAnimal(materiais);
			
			if(lista != null && !lista.isEmpty()){
				retorno.addObject("msgErro", "Esta a��o s� � poss�vel para grupos de material marcados como 'Animal'.");
			}			
		}
		return retorno;
	}
	
	public ModelAndView ajaxValidaMovimentacaoAnimal(WebRequestContext request, RegistroMovimentacaoAnimalBean bean){
		request.clearMessages();
		movimentacaoestoqueService.validaRegistrarMorte(request, bean.getListaMateriais());
		Boolean erros = Boolean.FALSE;
		String mensagemErro = "";
		for(Message msg: request.getMessages()){
			if(msg.getType().equals(MessageType.ERROR)){
				erros = Boolean.TRUE;
				mensagemErro += msg.getSource().toString()+"\n";
			}
		}
		request.clearMessages();
		ModelAndView retorno =  new JsonModelAndView()
								.addObject("naoPodeRegistrarMorte", erros)
								.addObject("mensagem", mensagemErro);
		
		return retorno;
	}
	
	public ModelAndView comboBoxInventario(WebRequestContext request, Inventario inventario) {
		List<Inventario> lista = new ArrayList<Inventario>();
		if(inventario.getEmpresa() != null){
			lista = inventarioService.findByEmpresa(inventario.getEmpresa(), inventario.getDtinventario(),inventario.getInventarioTipo());
		}
		return new JsonModelAndView().addObject("lista", lista);
	}
	
	public ModelAndView openPopupReserva(WebRequestContext request){
		Integer cdMaterial = Integer.parseInt(request.getParameter("cdmaterial"));
		Integer cdEmpresa = Integer.parseInt(request.getParameter("cdempresa"));
		List<Reserva> listaReserva = reservaService.recuperarReservaPorMaterial(cdMaterial, cdEmpresa, null);
		
		return new ModelAndView("direct:crud/popup/popUpReserva", "listaReserva", listaReserva);
		
	}
}
