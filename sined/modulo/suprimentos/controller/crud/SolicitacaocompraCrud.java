package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraorigem;
import br.com.linkcom.sined.geral.bean.OrigemSuprimentos;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialitem;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.GerenciamentomaterialService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemempresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialformulapesoService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.PapelService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialService;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialitemService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaorcamentoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.SolicitacaocompraFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/suprimento/crud/Solicitacaocompra", authorizationModule=CrudAuthorizationModule.class)
public class SolicitacaocompraCrud extends CrudControllerSined<SolicitacaocompraFiltro, Solicitacaocompra, Solicitacaocompra>{

	private MaterialService materialService;
	private SolicitacaocompraService solicitacaocompraService;
	private CentrocustoService centrocustoService;
	private PatrimonioitemService patrimonioitemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	private PapelService papelService;
	private PessoaService pessoaService;
	private PlanejamentoService planejamentoService;
	private TarefaService tarefaService;
	private ParametrogeralService parametrogeralService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private EmpresaService empresaService;
	private PedidovendaService pedidovendaService;
	private ProdutoService produtoService;
	private EntregaService entregaService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private MaterialproducaoService materialproducaoService;
	private UsuarioService usuarioService;
	private ProjetoService projetoService;
	private ProducaoagendamaterialitemService producaoagendamaterialitemService;
	private VendaorcamentoService vendaorcamentoService;
	private MaterialformulapesoService materialformulapesoService;
	private UnidademedidaService unidademedidaService;
	private GerenciamentomaterialService gerenciamentomaterialService;
	private LocalarmazenagemempresaService localarmazenagemempresaService;
	private OrdemcompraService ordemcompraService;
	private LocalarmazenagemService localarmazenagemService;
	private CotacaoService cotacaoService;
	private AvisoService avisoService;
	
	public void setUnidademedidaService(
			UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setGerenciamentomaterialService(
			GerenciamentomaterialService gerenciamentomaterialService) {
		this.gerenciamentomaterialService = gerenciamentomaterialService;
	}
	public void setMaterialformulapesoService(
			MaterialformulapesoService materialformulapesoService) {
		this.materialformulapesoService = materialformulapesoService;
	}
	public void setProducaoagendamaterialitemService(
			ProducaoagendamaterialitemService producaoagendamaterialitemService) {
		this.producaoagendamaterialitemService = producaoagendamaterialitemService;
	}
	public void setMaterialproducaoService(
			MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	public void setProducaoagendamaterialService(
			ProducaoagendamaterialService producaoagendamaterialService) {
		this.producaoagendamaterialService = producaoagendamaterialService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setProdutoService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {
		this.vendaorcamentoService = vendaorcamentoService;
	}
	public void setLocalarmazenagemempresaService(LocalarmazenagemempresaService localarmazenagemempresaService) {
		this.localarmazenagemempresaService = localarmazenagemempresaService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	/**
	 * M�todo que salva dados em massa.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#preparaSaveEmMassa(Solicitacaocompra)
	 * @see br.com.linkcom.sined.geral.service.PapelService#carregaPapeisPermissao(String)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#salvaRequisicaoEmMassa(List)
	 * @param request
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	@Command(validate=true)
	@Input("doEntrada")
	public ModelAndView salvaSolicitacaoEmMassa(WebRequestContext request, Solicitacaocompra bean){
		List<Solicitacaocompra> solicitacoes = solicitacaocompraService.preparaSaveEmMassa(bean);
		
		List<Papel> listaPapeis = papelService.carregaPapeisPermissaoAcao("AUTORIZAR_SOLICITACAOCOMPRA");		
		solicitacaocompraService.salvaSolicitacaoEmMassa(solicitacoes, listaPapeis, bean.getUrgente());
		
		request.clearMessages();
		request.addMessage("Registro(s) salvo(s) com sucesso.", MessageType.INFO);
		return new ModelAndView("redirect:/suprimento/crud/Solicitacaocompra?ACAO=listagem");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Solicitacaocompra bean) throws Exception {
		String whereIn = request.getParameter("itenstodelete");
		if(StringUtils.isBlank(whereIn) && bean.getCdsolicitacaocompra() != null){
			whereIn = bean.getCdsolicitacaocompra().toString();
		}
		
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhum item selecionado");
		}
		
		if(solicitacaocompraService.existSolicitacaoDiferenteEmAberto(whereIn)){
			throw new RuntimeException("N�o � permitido excluir solicita��o de compra com situa��o diferente de Em Aberto.");
		}
		super.excluir(request, bean);
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Solicitacaocompra form) throws CrudException {
//		Solicitacaocompra solicitacaocompra = solicitacaocompraService.findForExcluir(form);
//		if(solicitacaocompra != null && solicitacaocompra.getAux_solicitacaocompra() != null && 
//				!Situacaosuprimentos.EM_ABERTO.equals(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos())){
//			throw new RuntimeException("N�o � permitido excluir solicita��o de compra com situa��o diferente e Em Aberto.");
//		}
		return super.doExcluir(request, form);
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Solicitacaocompra form) {
		if((form == null || form.getCdsolicitacaocompra() == null) && StringUtils.isEmpty(request.getParameter("copiar")) && (form.getCopiar() == null || !form.getCopiar()))
			return new ModelAndView("crud/solicitacaocompraEntrada1");
		else
			return new ModelAndView("crud/solicitacaocompraEntrada");
	}
	
	@Override
	protected ListagemResult<Solicitacaocompra> getLista(WebRequestContext request, SolicitacaocompraFiltro filtro) {
		ListagemResult<Solicitacaocompra> listagemResult = super.getLista(request, filtro);
		List<Solicitacaocompra> lista = listagemResult.list();
		
		String prazodiasStr = parametrogeralService.getValorPorNome(Parametrogeral.PRAZO_DIAS_INICIO_COMPRA);
		Integer prazodias = 0;
		try{
			prazodias = Integer.parseInt(prazodiasStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String[] cotacoes = new String[]{};
        for (Solicitacaocompra solicitacaocompra : lista) {
        	String cotacao = solicitacaocompra.getAux_solicitacaocompra().getCotacao() != null ? solicitacaocompra.getAux_solicitacaocompra().getCotacao() : "";
        	if(StringUtils.isNotBlank(cotacao)){
        		cotacoes = (String[]) ArrayUtils.addAll(cotacoes, cotacao.split(","));
        	}
        }
        String whereInCdcotacao = CollectionsUtil.concatenate(Arrays.asList(cotacoes), ",");
        List<Cotacao> listaCotacao = new ArrayList<Cotacao>();
        if(StringUtils.isNotBlank(whereInCdcotacao.toString())){
        	listaCotacao = cotacaoService.findCotacoesEmAberto(whereInCdcotacao);
        }
        
		for (Solicitacaocompra solicitacaocompra : lista) {
			
			String dataentregaStr = solicitacaocompra.getDtentrega();
			java.sql.Date dtentrega = null;
			try {
				dtentrega = dataentregaStr != null ? SinedDateUtils.stringToDate(dataentregaStr) : null;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			java.sql.Date dtsolicitacao = solicitacaocompra.getDtsolicitacao();
			
			boolean corAmarelo = dtentrega != null &&
									solicitacaocompra.getAux_solicitacaocompra() != null &&
									solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos() != null &&
									!solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA) &&
									SinedDateUtils.beforeIgnoreHour(dtentrega, SinedDateUtils.currentDate());
			
			boolean corVermelho = dtsolicitacao != null &&
									prazodias > 0 &&
									solicitacaocompra.getAux_solicitacaocompra() != null &&
									solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos() != null &&
									(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) ||
									solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.AUTORIZADA)) &&
									
									SinedDateUtils.diferencaDias(SinedDateUtils.currentDate(), dtsolicitacao) > prazodias;
			
			solicitacaocompra.setCorAmarelo(corAmarelo);
			solicitacaocompra.setCorVermelho(corVermelho);
			
			
			String cotacao = solicitacaocompra.getAux_solicitacaocompra().getCotacao();
			String[] array = StringUtils.isNotBlank(cotacao) ? cotacao.split(",") : null;
            String whereInNovo = "";
            if(array != null) {
	            for(String cdcotacaoStr : array){
                    if(listaCotacao.contains(new Cotacao(Integer.parseInt(cdcotacaoStr)))){
                    	whereInNovo += cdcotacaoStr + ","; 
                    }
	            }
            }
			whereInNovo = !whereInNovo.isEmpty() ? whereInNovo.substring(0, whereInNovo.length()-1) : null;
			if(solicitacaocompra.getAux_solicitacaocompra() != null){
				solicitacaocompra.getAux_solicitacaocompra().setCotacao(solicitacaocompraService.makeLinkSuprimentos(whereInNovo, "Cotacao", "cdcotacao"));
				if(!Situacaosuprimentos.BAIXADA.equals(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos())){
					if(StringUtils.isNotEmpty(solicitacaocompra.getAux_solicitacaocompra().getOrdemcompra())){
						List<Ordemcompra> listaOrdemcompra = ordemcompraService.findOrdemcompraEmAberto(solicitacaocompra.getAux_solicitacaocompra().getOrdemcompra(), solicitacaocompra.getMaterial());
						if(SinedUtil.isListNotEmpty(listaOrdemcompra)){
							solicitacaocompra.getAux_solicitacaocompra().setOrdemcompra(solicitacaocompraService.makeLinkSuprimentos(CollectionsUtil.listAndConcatenate(listaOrdemcompra, "cdordemcompra", ","), "Ordemcompra", "cdordemcompra"));
						}else {
							solicitacaocompra.getAux_solicitacaocompra().setOrdemcompra(null);
						}
					}
				}else {
					solicitacaocompra.getAux_solicitacaocompra().setOrdemcompra(null);
				}
				if(StringUtils.isNotEmpty(solicitacaocompra.getAux_solicitacaocompra().getEntrega())){
					List<Entrega> listaEntrega = entregaService.findEntregaDifernteCancelada(solicitacaocompra.getAux_solicitacaocompra().getEntrega(), solicitacaocompra.getMaterial());
					if(SinedUtil.isListNotEmpty(listaEntrega)){
						solicitacaocompra.getAux_solicitacaocompra().setEntrega(solicitacaocompraService.makeLinkSuprimentos(CollectionsUtil.listAndConcatenate(listaEntrega, "cdentrega", ","), "Entrega", "cdentrega"));
					}else {
						solicitacaocompra.getAux_solicitacaocompra().setEntrega(null);
					}
				}
			}
		}
		
		solicitacaocompraService.createResumoSolicitacaocompra(filtro);
		return listagemResult;
	}
	
	@Override
	protected void listagem(WebRequestContext request,	SolicitacaocompraFiltro filtro) throws Exception {
		this.getRegistrosAtivos(request, null);
		
		List<Situacaosuprimentos> listSituacaoSuprimentos = new ArrayList<Situacaosuprimentos>();
		listSituacaoSuprimentos.add(Situacaosuprimentos.EM_ABERTO);
		listSituacaoSuprimentos.add(Situacaosuprimentos.AUTORIZADA);
		listSituacaoSuprimentos.add(Situacaosuprimentos.NAO_AUTORIZADA);
		listSituacaoSuprimentos.add(Situacaosuprimentos.EM_PROCESSOCOMPRA);
		listSituacaoSuprimentos.add(Situacaosuprimentos.BAIXADA);
		listSituacaoSuprimentos.add(Situacaosuprimentos.CANCELADA);
		
		List<Materialclasse> listaMaterialClasse = new ArrayList<Materialclasse>();
		listaMaterialClasse.add(Materialclasse.PRODUTO);
		listaMaterialClasse.add(Materialclasse.PATRIMONIO);
		listaMaterialClasse.add(Materialclasse.EPI);
		listaMaterialClasse.add(Materialclasse.SERVICO);
		
		List<OrigemSuprimentos> listaOrigemSuprimentos = new ArrayList<OrigemSuprimentos>();
		listaOrigemSuprimentos.add(OrigemSuprimentos.REQUISICAO);
		listaOrigemSuprimentos.add(OrigemSuprimentos.REPOSICAO);
		listaOrigemSuprimentos.add(OrigemSuprimentos.USUARIO);
		listaOrigemSuprimentos.add(OrigemSuprimentos.PROJETO);
		listaOrigemSuprimentos.add(OrigemSuprimentos.LISTAMATERIAL);
		
		request.setAttribute("listasituacoessolicitacaocompra", listSituacaoSuprimentos);
		request.setAttribute("listaclassessolicitacaocompra", listaMaterialClasse);
		request.setAttribute("listaorigenssolicitacaocompra", listaOrigemSuprimentos);
		
		super.listagem(request, filtro);
	}
	
	/** M�todo que busca registros ativos que ser�o usados tanto 
	 *  na listagem quanto na entrada
	 * 
	 * @param request
	 * @author Tom�s Rabelo
	 * @param solicitacaoCompra 
	 */
	private void getRegistrosAtivos(WebRequestContext request, Solicitacaocompra solicitacaoCompra) {
		request.setAttribute("centroCustosAtivos", centrocustoService.findAtivos(solicitacaoCompra != null ? solicitacaoCompra.getCentrocusto() : null));
		request.setAttribute("gruposAtivos", materialgrupoService.findAtivos(solicitacaoCompra != null ? solicitacaoCompra.getMaterialgrupo() : null));
		request.setAttribute("tiposAtivos", materialtipoService.findAtivos(solicitacaoCompra != null ? solicitacaoCompra.getMaterialtipo() : null));
	}
	
	@Override
	protected void validateBean(Solicitacaocompra bean, BindException errors) {
		if("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEARCOMPRAPROJETO))){
			if(SinedUtil.isListNotEmpty(bean.getSolicitacoes())){
				solicitacaocompraService.validaQtdeCompraMaiorQuePlanejamento(bean.getSolicitacoes(), bean.getProjeto(), errors);
			}else {
				solicitacaocompraService.validaQtdeCompraMaiorQuePlanejamento(bean, errors);
			}
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Solicitacaocompra bean) throws Exception {
		Integer cdsolicitacaocompraQuantidadeRestante = bean.getCdsolicitacaocompraQuantidadeRestante();
		
		boolean criar = bean.getCdsolicitacaocompra() == null;
		super.salvar(request, bean);
		
		if(criar){
			List<Solicitacaocompra> lista = new ArrayList<Solicitacaocompra>();
			lista.add(bean);
			avisoService.salvarAvisos(solicitacaocompraService.montaAvisosSolicitacaoCompra(MotivoavisoEnum.AUTORIZAR_SOLICITACAOCOMPRA, lista, "Aguardando autoriza��o"), false);
		}
		
		if(cdsolicitacaocompraQuantidadeRestante != null){
			solicitacaocompraService.baixarEregistrarHistoricoQuantidadeRestante(bean, cdsolicitacaocompraQuantidadeRestante);
		}
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Solicitacaocompra form) throws CrudException {
		String cdsolicitacaocompra = request.getParameter("cdsolicitacaocompra");
		String stCopiar = request.getParameter("copiar");
		String quantidadeRestanteParam = request.getParameter("fromQuantidadeRestante");
		
		if (StringUtils.isNotEmpty(cdsolicitacaocompra) && StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
			Integer cdsolicitacaocompraIn = Integer.parseInt(cdsolicitacaocompra);
			
			form.setCdsolicitacaocompra(cdsolicitacaocompraIn);
			form = solicitacaocompraService.loadForEntrada(form);
			form.setCdsolicitacaocompra(null);
			form.setDtbaixa(null);
			form.setCopiar(Boolean.TRUE);
			
			if (StringUtils.isNotEmpty(quantidadeRestanteParam) && quantidadeRestanteParam.equals("true")){
				if(form.getAux_solicitacaocompra() == null || 
						form.getAux_solicitacaocompra().getPendenciaqtde() == null ||
						!form.getAux_solicitacaocompra().getPendenciaqtde() ||
						form.getAux_solicitacaocompra().getSituacaosuprimentos() == null ||
						form.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
					request.addError("S� pode solicitar restante de uma solicita��o de compra que tenha pend�ncia na quantidade restante e esteja na situa��o 'EM PROCESSO DE COMPRA'.");
					return sendRedirectToAction("listagem");
				}
				
				cdsolicitacaocompraIn = Integer.parseInt(cdsolicitacaocompra);
				
				form.setQtde(solicitacaocompraService.getQtdeRestanteSolicitacaoOrdemcompra(cdsolicitacaocompraIn));
				form.setCdsolicitacaocompraQuantidadeRestante(cdsolicitacaocompraIn);
			}else {
				Integer identificador = solicitacaocompraService.getUltimoIdentificador();
				form.setIdentificador(new Integer(identificador != null ? identificador+1 : 0));
			}
			
			form.setAux_solicitacaocompra(null);
			
			
			SimpleDateFormat format = new SimpleDateFormat("yy");
			form.setDescricao(form.getIdentificador()+"/"+format.format(new Date()));
		}
		
		return super.doEntrada(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Solicitacaocompra form)throws Exception {
		request.setAttribute("hasErrorsValidate", request.getBindException() != null && request.getBindException().hasErrors());
		if(form != null && form.getCdsolicitacaocompra() != null){
			List<OrigemsuprimentosBean> listaBeanDestino = solicitacaocompraService.montaDestino(form);			
			request.setAttribute("listaDestino", listaBeanDestino);
			
			List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
			if(form.getMaterial() != null && form.getMaterial().getUnidademedida() != null){
				listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(form.getMaterial().getUnidademedida(), form.getMaterial());
			}
			request.setAttribute("listaUnidademedida", listaUnidademedida);
		} else if(request.getBindException() == null || !request.getBindException().hasErrors()){
			
			solicitacaocompraService.preparaCriacaoSolicitacao(form);
			
			String paramPlanejamento = request.getParameter("planejamentos");
			if(paramPlanejamento != null && !paramPlanejamento.equals("")){
				form = planejamentoService.gerarSolicitacaocompra(form, paramPlanejamento);
			}
			
			String paramTarefa = request.getParameter("tarefas");
			if(paramTarefa != null && !paramTarefa.equals("")){
				form = tarefaService.gerarSolicitacaocompra(form, paramTarefa);
			}
			
			String paramProducaoagenda = request.getParameter("idsproducaoagenda");
			if(paramProducaoagenda != null && !paramProducaoagenda.equals("")){
				List<Solicitacaocompra> solicitacoes = new ArrayList<Solicitacaocompra>();
				
				List<Producaoagendamaterialitem> listaProducaoagendamaterialitem = producaoagendamaterialitemService.findByProducaoagenda(paramProducaoagenda);
				if(listaProducaoagendamaterialitem != null && listaProducaoagendamaterialitem.size() > 0){
					for (Producaoagendamaterialitem producaoagendamaterialitem : listaProducaoagendamaterialitem) {
						Material material = producaoagendamaterialitem.getMaterial();
						Double qtde = producaoagendamaterialitem.getQtde();
						
						Solicitacaocompra solicitacaocompra = new Solicitacaocompra();
						solicitacaocompra.setMaterial(material);
						solicitacaocompra.setQtde(new Double(qtde));
						solicitacaocompra.setQtdvolume(producaoagendamaterialitem.getVolume());
						solicitacaocompra.setProducaoagenda(producaoagendamaterialitem.getProducaoagenda());
						
						solicitacaocompraService.addMateriaisAgendaproducao(solicitacoes, solicitacaocompra);
					}
				} else {
					List<Producaoagendamaterial> listaProducaoagendamaterial = producaoagendamaterialService.findByProducaoagenda(paramProducaoagenda, null);
					for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
						Material material = producaoagendamaterial.getMaterial();
						Double qtdeproduzir = producaoagendamaterial.getQtde();
						Double qtdereferencia = material.getQtdereferencia() != null ? material.getQtdereferencia() : 1d;
						
						if(SinedUtil.isListNotEmpty(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima())){
							for (Producaoagendamaterialmateriaprima materiaprima : producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()) {
								Solicitacaocompra solicitacaocompra = new Solicitacaocompra();
								solicitacaocompra.setMaterial(materiaprima.getMaterial());
								solicitacaocompra.setQtde(materiaprima.getQtdeprevista() / qtdereferencia);
								solicitacaocompra.setProducaoagenda(producaoagendamaterial.getProducaoagenda());
								
								if(solicitacaocompra.getQtde() > 0)
									solicitacaocompraService.addMateriaisAgendaproducao(solicitacoes, solicitacaocompra);
							}
						}else {
							try{
								material.setProduto_altura(producaoagendamaterial.getAltura());
								material.setProduto_largura(producaoagendamaterial.getLargura());
								material.setQuantidade(qtdeproduzir != null ? qtdeproduzir.doubleValue() : 1.0);
								materialproducaoService.getValorvendaproducao(material);			
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							Material auxMaterial = materialService.loadMaterialComMaterialproducao(material);
							if(auxMaterial != null && auxMaterial.getListaProducao() != null){
								material = auxMaterial;
								material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
		
								for (Materialproducao materialproducao : material.getListaProducao()) {
									Solicitacaocompra solicitacaocompra = new Solicitacaocompra();
									solicitacaocompra.setMaterial(materialproducao.getMaterial());
									solicitacaocompra.setQtde(qtdeproduzir * (materialproducao.getConsumo() / qtdereferencia));
									solicitacaocompra.setProducaoagenda(producaoagendamaterial.getProducaoagenda());
									
									if(solicitacaocompra.getQtde() > 0)
										solicitacaocompraService.addMateriaisAgendaproducao(solicitacoes, solicitacaocompra);
								}
							}
							else{
								Solicitacaocompra solicitacaocompra = new Solicitacaocompra();;
								solicitacaocompra.setMaterial(material);
								solicitacaocompra.setQtde(material.getQuantidade());
								if(producaoagendamaterial.getProducaoagenda() != null && producaoagendamaterial.getProducaoagenda().getCdproducaoagenda() != null){
									solicitacaocompra.setProducaoagenda(producaoagendamaterial.getProducaoagenda());
								}
								if(solicitacaocompra.getQtde() > 0)
									solicitacaocompraService.addMateriaisAgendaproducao(solicitacoes, solicitacaocompra);
							}
						}
					}
				}
				
				form.setSolicitacoes(solicitacoes);
			}
		}
		this.getRegistrosAtivos(request, form);
		
		if(form.getColaborador() == null){
			form.setColaborador(SinedUtil.getUsuarioComoColaborador());
		}
		
		List<OrigemsuprimentosBean> listaBeanOrigem = solicitacaocompraService.montaOrigem(form);
		request.setAttribute("listaOrigem", listaBeanOrigem);
		
		form.setObrigar_projeto_conta_grupo(parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_PROJETO_CONTA_GRUPO));		
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		
		if(form.getCdsolicitacaocompra() == null && request.getParameter("whereInCdpedidovenda") != null && 
				!"".equals(request.getParameter("whereInCdpedidovenda")) && !"<null>".equals(request.getParameter("whereInCdpedidovenda"))){
			form.setWhereInCdpedidovenda(request.getParameter("whereInCdpedidovenda"));
			
			List<Pedidovenda> listaPedidovenda = pedidovendaService.findForSolicitacaocompra(form.getWhereInCdpedidovenda());
			form.setSolicitacoes(new ArrayList<Solicitacaocompra>());
			if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
				Money desconto = new Money();
				for(Pedidovenda pedidovenda : listaPedidovenda){
					for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
						if(pedidovendamaterial.getDesconto() != null){
							desconto = desconto.add(pedidovendamaterial.getDesconto());
						}
					}
					if(form.getProjeto() == null)
						form.setProjeto(pedidovenda.getProjeto());
					if(form.getEmpresa() == null)
						form.setEmpresa(pedidovenda.getEmpresa());
					if(form.getLocalarmazenagem() == null)
						form.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
					form.getSolicitacoes().addAll(solicitacaocompraService.getListaMaterialByPedidovenda(pedidovenda));
				}
			}
			
			if(form.getSolicitacoes() != null && form.getSolicitacoes().size() > 0){
				List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
				for (Solicitacaocompra solicitacaocompra : form.getSolicitacoes()) {
					listaUnidademedida.add(solicitacaocompra.getUnidademedida());
				}
				request.setAttribute("listaUnidademedida", listaUnidademedida);
			}
		}
		if(form.getCdsolicitacaocompra() == null && request.getParameter("whereInCdvendaorcamento") != null && 
				!"".equals(request.getParameter("whereInCdvendaorcamento")) && !"<null>".equals(request.getParameter("whereInCdvendaorcamento"))){
			form.setWhereInCdvendaorcamento(request.getParameter("whereInCdvendaorcamento"));
			
			List<Vendaorcamento> listaVendaorcamento = vendaorcamentoService.findForSolicitacaocompra(form.getWhereInCdvendaorcamento());
			form.setSolicitacoes(new ArrayList<Solicitacaocompra>());
			if(listaVendaorcamento != null && !listaVendaorcamento.isEmpty()){
				Money desconto = new Money();
				for(Vendaorcamento vendaorcamento : listaVendaorcamento){
					for (Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()) {
						if(vendaorcamentomaterial.getDesconto() != null){
							desconto = desconto.add(vendaorcamentomaterial.getDesconto());
						}
					}
					if(form.getProjeto() == null)
						form.setProjeto(vendaorcamento.getProjeto());
					if(form.getEmpresa() == null)
						form.setEmpresa(vendaorcamento.getEmpresa());
					if(form.getLocalarmazenagem() == null)
						form.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
					form.getSolicitacoes().addAll(solicitacaocompraService.getListaMaterialByOrcamento(vendaorcamento));
				}
			}
		}
		
		String whereIngerenciamentomaterial = request.getParameter("whereIngerenciamentomaterial");
		if(StringUtils.isNotBlank(whereIngerenciamentomaterial)) {
			String[] itens = whereIngerenciamentomaterial.split(",");
			
			List<Material> listaMaterial = new ArrayList<Material>();
			List<Localarmazenagem> listaLocal = new ArrayList<Localarmazenagem>();
			for (int i = 0; i < itens.length; i++) {
				String[] campos = itens[i].split("\\|");
				
				Material material = new Material(Integer.parseInt(campos[0]));
				Localarmazenagem localarmazenagem = campos[1] != null && !"".equals(campos[1]) ? new Localarmazenagem(Integer.parseInt(campos[1])) : null;
				
				if(!listaMaterial.contains(material)){
					listaMaterial.add(material);
				}
				if(!listaLocal.contains(localarmazenagem)){
					listaLocal.add(localarmazenagem);
				}
			}
			
			List<Vgerenciarmaterial> listaVgerenciarmaterial = gerenciamentomaterialService.findForSolicitacaocompra(CollectionsUtil.listAndConcatenate(listaMaterial, "cdmaterial", ","));
			form.setSolicitacoes(new ArrayList<Solicitacaocompra>());
			if(listaVgerenciarmaterial != null && !listaVgerenciarmaterial.isEmpty()) {
				for (Vgerenciarmaterial vgerenciarmaterial : listaVgerenciarmaterial) {
					if((listaLocal == null || listaLocal.size() == 1) && form.getLocalarmazenagem() == null){
						if(vgerenciarmaterial.getLocalarmazenagem() != null){
							form.setLocalarmazenagem(localarmazenagemService.load(vgerenciarmaterial.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
						}
					}
					if(form.getEmpresa() == null)
						form.setEmpresa(vgerenciarmaterial.getEmpresa());
					if(form.getProjeto() == null)
						form.setProjeto(vgerenciarmaterial.getProjeto());
				}
				form.getSolicitacoes().addAll(solicitacaocompraService.getListaMaterialByVgerenciarmaterial(listaVgerenciarmaterial));
			}
		}
		
		List<Projeto> projetos = new ArrayList<Projeto>();
		if(form.getProjeto() != null){
			projetos.add(form.getProjeto());
		}
		List<Projeto> listaProjeto = projetoService.findForSolicitacaoCompra(projetos, true);
		request.setAttribute("listaProjeto", listaProjeto);
		
		request.setAttribute("ignoreHackAndroidDownload", true);
		
		Object cdtarefa = request.getParameter("tarefas");
		if(!"".equals(cdtarefa) && cdtarefa != null ){
			Tarefa tarefa = new Tarefa();
			tarefa.setCdtarefa(Integer.parseInt(String.valueOf(cdtarefa)));
			tarefa = tarefaService.load(tarefa, "tarefa.cdtarefa, tarefa.planejamento");
			Planejamento planejamento = planejamentoService.load(tarefa.getPlanejamento(), "planejamento.cdplanejamento, planejamento.projeto");
			Projeto projeto = projetoService.load(planejamento.getProjeto(), "projeto.cdprojeto, projeto.localarmazenagem, projeto.centrocusto");
			form.setProjeto(planejamento.getProjeto());
			if(projeto!= null){
				form.setLocalarmazenagem(projeto.getLocalarmazenagem());
				form.setCentrocusto(projeto.getCentrocusto());
			}
		}
	}
	
	@Override
	protected Solicitacaocompra carregar(WebRequestContext request, Solicitacaocompra bean) throws Exception {
		Solicitacaocompra solicitacaocompra = super.carregar(request, bean); 
		if(solicitacaocompra == null){
			throw new SinedException("Registro n�o encontrado no sistema");
		}
		return solicitacaocompra;
	}
	
	/** Fun��o ajax que retorna o s�mbolo da unidade de medida do material + a qtde disponivel do produto
	 * @param request
	 * @param material
	 * @author Tom�s Rabelo
	 */
	public void getComplementoMaterialAjax(WebRequestContext request, Material material){
		material = materialService.getSimboloUnidadeMedidaETempoEntrega(material);
		
		String data = "var tempoentrega = ";
		data += material.getTempoentrega() != null ? "'"+material.getTempoentrega()+"';" : "'';";	
		data += "var unidademedida = ";
		data += material.getUnidademedida() != null ? "'"+material.getUnidademedida().getSimbolo()+"';" : "'';";
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(data);
	}
	
	/**
	 * Fun��o ajax que retorna quantidade disponivel do material
	 * @param request
	 * @param solicitacaocompra
	 * @author Tom�s Rabelo
	 */
	public void getQtdDisponivelMaterialAjax(WebRequestContext request, Solicitacaocompra solicitacaocompra){
		Double disponivel = null;
		if(solicitacaocompra.getMaterialclasse().getCdmaterialclasse().equals(Materialclasse.PATRIMONIO.getCdmaterialclasse())){
//			Long qtdPatrimonioDisponivel = patrimonioitemService.getQtdDisponivelPatrimonio(solicitacaocompra.getMaterial());
			Long qtdPatrimonioDisponivel = patrimonioitemService.getQtdDisponivelPatrimonio(solicitacaocompra.getMaterial(), solicitacaocompra.getLocalarmazenagem());
			if(qtdPatrimonioDisponivel != null)
				disponivel = qtdPatrimonioDisponivel.doubleValue();
		} else{
			disponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(solicitacaocompra.getMaterial(), solicitacaocompra.getMaterialclasse(), solicitacaocompra.getLocalarmazenagem(), null, null, null, true);
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(disponivel == null ? "" : disponivel.toString());
	}
	
	/**
	 * M�todo que busca as classes do material e a unidade de medida
	 * 
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView getInfoMaterial(WebRequestContext request, Solicitacaocompra bean){
		List<Materialclasse> listaClasse = materialService.findClasses(bean.getMaterial());
		
		Boolean isLocacao = materialService.isLocacaoByMaterial(bean.getMaterial());
		Material material = materialService.getSimboloUnidadeMedidaETempoEntrega(bean.getMaterial());
		
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		Material unidadeMaterial = new Material();
		unidadeMaterial = materialService.unidadeMedidaMaterial(bean.getMaterial());
		
		listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(unidadeMaterial.getUnidademedida(), unidadeMaterial);
		
		Unidademedida unidademedidaPrioritaria = null;
		for (Unidademedida unidademedida : listaUnidademedida) {
			if(unidademedida.getPrioritariacompra() != null && unidademedida.getPrioritariacompra()){
				unidademedidaPrioritaria = unidademedida;
			}
		}
		
		Material contagerencialMaterial = new Material();		
		contagerencialMaterial = materialService.findWithContagerencial(bean.getMaterial());
		
		Material materialGrupo = new Material();
		materialGrupo = materialService.findWithMaterialgrupo(bean.getMaterial());
		
		Integer qtdeminima = 0;
		if(listaClasse != null && !listaClasse.isEmpty()){
			for(Materialclasse materialclasse : listaClasse){
				if(Materialclasse.PRODUTO.equals(materialclasse)){
					Produto produto = produtoService.carregaProduto(bean.getMaterial());
					if(produto != null && produto.getQtdeminima() != null){
						qtdeminima = produto.getQtdeminima();
						
					}
					break;
				}
			}
		}
		
		Unidademedida unidadeselecionada = bean.getUnidademedida();
		if(bean.getUnidademedida() != null)
			unidadeselecionada = bean.getUnidademedida();
		
		return new JsonModelAndView().addObject("listaClasse", listaClasse)
									 .addObject("islocacao", isLocacao)
									 .addObject("unidadeMedidaSelecionada", unidadeselecionada != null ? unidadeselecionada : (unidademedidaPrioritaria != null ? unidademedidaPrioritaria : unidadeMaterial.getUnidademedida()))
									 .addObject("listaUnidademedida", listaUnidademedida)
									 .addObject("tempoentrega", material.getTempoentrega() != null ? material.getTempoentrega() : "")
									 .addObject("contaGerencial", (contagerencialMaterial.getContagerencial() != null ? contagerencialMaterial.getContagerencial().getCdcontagerencial() : "" ) + "")
									 .addObject("materialGrupo", materialGrupo.getMaterialgrupo().getCdmaterialgrupo() + "")
									 .addObject("qtdeminima", qtdeminima);
	}
	
	/**
	 * M�todo que busca materiais a partir do grupo e tipo escolhidos
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView getMateriais(WebRequestContext request, Solicitacaocompra bean){
		List<Material> listaMaterial = materialService.findByTipoGrupo(bean.getMaterial().getMaterialgrupo(), bean.getMaterial().getMaterialtipo());
		return new JsonModelAndView().addObject("listaMaterial", listaMaterial);
	}

	/**
	 * M�todo chamado via AJAX que verifica se a msg de emitir solicita��es com a mesma situa��o ser� mostrada 
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView verificaMsgSituacao(WebRequestContext request, Solicitacaocompra bean){
		Boolean mostrarMsg = solicitacaocompraService.existeSolicitacaoComSituacaoDiferente(bean);
		return new JsonModelAndView().addObject("mostrarMsg", mostrarMsg);
	}
	
	/**
	 * Action que cria o CSV da listagem de Solicita��o de compra.
	 * 
	 * @see r.com.linkcom.sined.geral.service.SolicitacaocompraService#findForCsv
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarCsv(WebRequestContext request, SolicitacaocompraFiltro filtro){
		List<Solicitacaocompra> lista = solicitacaocompraService.findForCsv(filtro);
		StringBuilder rel = new StringBuilder();
		
		rel.append("Solicita��o compra;");
		rel.append("Ident.;");
		rel.append("Material/servi�o;");
		rel.append("Unidade de medida;");
		rel.append("Qtde.;");
		rel.append("Qtde. comprada;");
		rel.append("Grupo material;");
		rel.append("Centro de custo;");
		rel.append("Comprador;");
		rel.append("Projeto;");
		rel.append("Solicitante;");
		rel.append("Data emiss�o;");
		rel.append("Data aprova��o;");
		rel.append("Data da necessidade;");
		rel.append("Observa��o;");
		rel.append("Situa��o;");
		rel.append("OC Gerada;");
		rel.append("\n");
		
		for (Solicitacaocompra sc : lista) {
			rel.append("(");
			rel.append(sc.getDescricao());
			rel.append(");");
			
			rel.append(sc.getMaterial().getIdentificacao() != null ? sc.getMaterial().getIdentificacao() : "");
			rel.append(";");
			rel.append(sc.getMaterial().getNome());
			rel.append(";");
			if (sc.getUnidademedida() != null && sc.getUnidademedida().getSimbolo() != null)
				rel.append(sc.getUnidademedida().getSimbolo());
			rel.append(";");
			
			if(sc.getQtde() != null)
				rel.append(SinedUtil.descriptionDecimal(sc.getQtde()));
			rel.append(";");
			
			Double qtdecomprada = 0d;
			if(sc.getAux_solicitacaocompra() != null && 
					(sc.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_PROCESSOCOMPRA) ||
					 sc.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA))){
				qtdecomprada = solicitacaocompraService.getQtdeComprada(sc);
			}
			rel.append(SinedUtil.descriptionDecimal(qtdecomprada));
			rel.append(";");
			
			rel.append(sc.getMaterial().getMaterialgrupo().getNome());
			rel.append(";");
			if(sc.getCentrocusto() != null)
				rel.append(sc.getCentrocusto().getNome());
			rel.append(";");
			
			boolean verificarComprador = false;
			
			if (sc.getListaCotacaoorigem() != null && !sc.getListaCotacaoorigem().isEmpty()){
				for (Cotacaoorigem cotacaoorigem : sc.getListaCotacaoorigem()) {
					if(cotacaoorigem.getCotacao().getDtcancelamento() == null){
						rel.append(pessoaService.load(new Pessoa(cotacaoorigem.getCotacao().getCdusuarioaltera())).getNome());
						verificarComprador = true;
						break;
					}
				}
			}
			if (!verificarComprador && sc.getMaterial().getMaterialgrupo() != null && sc.getMaterial().getMaterialgrupo().getListaMaterialgrupousuario() != null){
				String nomes = "";
				nomes = CollectionsUtil.listAndConcatenate(sc.getMaterial().getMaterialgrupo().getListaMaterialgrupousuario(), "usuario.nome", ", ");
				rel.append(nomes);
			}
			rel.append(";");
			
			if(sc.getProjeto() != null)
				rel.append(sc.getProjeto().getNome());
			rel.append(";");
			
			rel.append(sc.getColaborador().getNome());
			rel.append(";");
			
			if(sc.getDtsolicitacao() != null)
				rel.append(SinedDateUtils.toString(sc.getDtsolicitacao()));
			rel.append(";");
			
			if(sc.getDtautorizacao() != null) 
				rel.append(SinedDateUtils.toString(sc.getDtautorizacao()));
			rel.append(";");
			
			if(sc.getDtlimite() != null) 
				rel.append(SinedDateUtils.toString(sc.getDtlimite()));
			rel.append(";");
			
			if(sc.getObservacao() != null)
				rel.append(sc.getObservacao());
			rel.append(";");
			
			if(sc.getAux_solicitacaocompra() != null && sc.getAux_solicitacaocompra().getSituacaosuprimentos() != null) 
				rel.append(sc.getAux_solicitacaocompra().getSituacaosuprimentos().getDescricao());
			rel.append(";");
			
			String destinos = "";
			
			if(sc.getListaCotacaoorigem() != null && sc.getListaCotacaoorigem().size() > 0){
				for (Cotacaoorigem cotacaoorigem : sc.getListaCotacaoorigem()) {
					if(!cotacaoorigem.getCotacao().getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
						if(cotacaoorigem.getCotacao().getListaOrdemcompra() != null && 
								!cotacaoorigem.getCotacao().getListaOrdemcompra().isEmpty()){
							for(Ordemcompra ordemcompra : cotacaoorigem.getCotacao().getListaOrdemcompra()){
								if(ordemcompra.getAux_ordemcompra() != null && 
										ordemcompra.getAux_ordemcompra().getSituacaosuprimentos() != null &&
										!ordemcompra.getAux_ordemcompra().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
									if(ordemcompra.getCdordemcompra() != null && 
											!destinos.contains(ordemcompra.getCdordemcompra().toString())){
										destinos +=  ordemcompra.getCdordemcompra() +", ";
									}
								}
							}
						}
					}
				}
			}
			if(!destinos.equals("")){
				if (destinos.endsWith(", ")){
					destinos = destinos.substring(0, destinos.length()-2);
				}
				destinos = "Cota��o (" + destinos + ")";
			}
			
			if(sc.getListaOrdemcompraorigem() != null && sc.getListaOrdemcompraorigem().size() > 0){
				for (Ordemcompraorigem ordemcompraorigem : sc.getListaOrdemcompraorigem()) {
					if(ordemcompraorigem.getOrdemcompra() != null && 
							ordemcompraorigem.getOrdemcompra().getAux_ordemcompra() != null && 
							ordemcompraorigem.getOrdemcompra().getAux_ordemcompra().getSituacaosuprimentos() != null &&
							!ordemcompraorigem.getOrdemcompra().getAux_ordemcompra().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
						destinos += "Ordem de Compra (" + ordemcompraorigem.getOrdemcompra().getCdordemcompra().toString() + ", ";
					}
				}
			}
			
			if (destinos.endsWith(", ")){
				destinos = destinos.substring(0, destinos.length()-2) + ")";
			}
			
			rel.append(destinos);
			rel.append(";");
			
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","solicitacaocompra_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	/**
	 * M�todo que redireciona para a cria��o da ordem de compra
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarOrdemcompra(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String getcontroller = request.getParameter("controller");
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/suprimento/crud/Solicitacaocompra");
		}else {
			List<Solicitacaocompra> solicitacoes = solicitacaocompraService.findSolicitacoes(whereIn);
			Boolean permiteAutorizarSolicitacaocompra = usuarioService.permiteAcao(SinedUtil.getUsuarioLogado(), "AUTORIZAR_SOLICITACAOCOMPRA");
			
			Boolean erro = Boolean.FALSE;
			for (Solicitacaocompra solicitacaocompra : solicitacoes) {
				if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) && !permiteAutorizarSolicitacaocompra){
					request.addError("Para gerar a ordem de compra, a solicita��o de compra deve estar autorizada.");
					erro = true;
				} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_PROCESSOCOMPRA)){
					request.addError("Solicita��o de compra j� est� em processo de compra.");
					erro = true;
				} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)){
					request.addError("Solicita��o de compra j� foi realizada.");
					erro = true;
				} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
					request.addError("Gera��o de cota��o n�o permitida. A solicita��o de compra est� cancelada.");
					erro = true;
				} 
				if(erro) {
					return new ModelAndView("redirect:/suprimento/crud/Solicitacaocompra");
				}
			}
		} 
		
		return new ModelAndView("redirect:/suprimento/crud/Ordemcompra?ACAO=criar&gerarocbysolicitacao=true&controller=" + getcontroller +"&selectedItensSolicitacao="+ whereIn);
	}
	
	public ModelAndView abrirPopupUltimascompras(WebRequestContext request){
		return entregaService.abrirPopupUltimascompras(request);	
	}
	
	/**
	 * M�todo que busca os materias de acordo com tipo, grupo, empresa
	 *
	 * @param request
	 * @param solicitacaocompra
	 * @author Luiz Fernando
	 */
	public void buscaMateriais(WebRequestContext request, Solicitacaocompra solicitacaocompra){
		List<Material> listaMaterial = materialService.findByTipoGrupo(solicitacaocompra.getMaterialgrupo(), 
																	solicitacaocompra.getMaterialtipo(), 
																	solicitacaocompra.getEmpresa());
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaMaterial, "listaMaterial", ""));
	}
	
	/**
	 * M�todo ajax para adiconar a empresa na sess�o (fluxo compra)
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void colocarEmpresaSessao(WebRequestContext request){
		empresaService.adicionarEmpresaSessaoForFluxocompra(request, null, "SOLICITACAOCOMPRA");
	}
	
	/**
	 * Busca as dimens�es do material.
	 *
	 * @param request
	 * @param solicitacaocompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/09/2013
	 */
	public ModelAndView verificaDimensoesMaterialAjax(WebRequestContext request, Solicitacaocompra solicitacaocompra){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(solicitacaocompra != null && solicitacaocompra.getMaterial() != null){
			Produto produto = produtoService.load(new Produto(solicitacaocompra.getMaterial()), "produto.cdmaterial, produto.altura, produto.comprimento, produto.largura");
			
			if(produto != null){
				if(produto.getAltura() != null){
					jsonModelAndView.addObject("altura", produto.getAltura());
				}
				if(produto.getComprimento() != null){
					jsonModelAndView.addObject("comprimento", produto.getComprimento());
				}
				if(produto.getLargura() != null){
					jsonModelAndView.addObject("largura", produto.getLargura());
				}
			}
		}
		return jsonModelAndView;
	}
	
	/**
	 * Realiza o c�lculo de peso do material de acordo com infos da solilcita��o de compra.
	 *
	 * @param request
	 * @param solicitacaocompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/09/2013
	 */
	public ModelAndView calculaPesoMaterialAjax(WebRequestContext request, Solicitacaocompra solicitacaocompra){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(solicitacaocompra != null && solicitacaocompra.getMaterial() != null){
			try{
				Double qtde = solicitacaocompra.getQtde() != null ? solicitacaocompra.getQtde() : 0d;
				boolean addPesoCampoQtde = false;
				
				Material material = materialService.load(solicitacaocompra.getMaterial(), "material.cdmaterial, material.materialtipo, material.peso, material.pesobruto");
				if(material == null){
					material = materialService.load(solicitacaocompra.getMaterial(), "material.cdmaterial, material.peso, material.pesobruto");
				}
				material.setListaMaterialformulapeso(materialformulapesoService.findByMaterial(material));
				
				Double peso = material.getPeso() != null ? material.getPeso() : material.getPesobruto();
				if(material.getListaMaterialformulapeso() != null && material.getListaMaterialformulapeso().size() > 0){
				
					Double altura = solicitacaocompra.getAltura() != null ? solicitacaocompra.getAltura() : 0d;
					Double comprimento = solicitacaocompra.getComprimento() != null ? solicitacaocompra.getComprimento() : 0d;
					Double largura = solicitacaocompra.getLargura() != null ? solicitacaocompra.getLargura() : 0d;
					Double qtdvolume = solicitacaocompra.getQtdvolume() != null ? solicitacaocompra.getQtdvolume() : 0d;
					
					material.setProduto_altura(altura);
					material.setProduto_comprimento(comprimento);
					material.setProduto_largura(largura);
					material.setQtdvolume(qtdvolume);
					
					Material m = materialService.getSimboloUnidadeMedidaETempoEntrega(solicitacaocompra.getMaterial());
					Unidademedida unidademedida = m != null ? unidademedida = m.getUnidademedida() : null;
					if(solicitacaocompra.getUnidademedida() != null && solicitacaocompra.getUnidademedida().getCdunidademedida() != null){
						solicitacaocompra.setUnidademedida(unidademedidaService.load(solicitacaocompra.getUnidademedida(), 
								"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo"));
						unidademedida = solicitacaocompra.getUnidademedida();
					}
					
					peso = materialformulapesoService.calculaPesoMaterial(material);
					
					if(peso != null && 
							peso > 0 && 
							unidademedida != null && 
							unidademedida.getSimbolo() != null &&
							"KG".equalsIgnoreCase(unidademedida.getSimbolo())){
						addPesoCampoQtde = true;
					}
				}
				
				if(peso != null && !addPesoCampoQtde){
					peso = peso * qtde;
				}
				
				jsonModelAndView
					.addObject("peso", peso != null ? SinedUtil.roundByParametro(peso) : 0d)
					.addObject("addPesoCampoQtde", addPesoCampoQtde);
			} catch (Exception e) {
				e.printStackTrace();
				return jsonModelAndView.addObject("erro", e.getMessage());
			}
		}
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxCarregarEmpresa(WebRequestContext request, Solicitacaocompra solicitacaocompra){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		if(solicitacaocompra.getLocalarmazenagem() != null && 
				solicitacaocompra.getLocalarmazenagem().getCdlocalarmazenagem() != null){
			List<Localarmazenagemempresa> lista = localarmazenagemempresaService.findByLocalarmazenagem(solicitacaocompra.getLocalarmazenagem());
			if(SinedUtil.isListNotEmpty(lista)){
				for(Localarmazenagemempresa localarmazenagemempresa : lista){
					if(localarmazenagemempresa.getEmpresa() != null){
						listaEmpresa.add(localarmazenagemempresa.getEmpresa());
					}
				}
			}
		}
		return jsonModelAndView.addObject("listaEmpresa", listaEmpresa);
	}
	
	/**
	* M�todo ajax que busca o local e o centro de custo do projeto
	*
	* @param request
	* @param projeto
	* @since 26/08/2015
	* @author Luiz Fernando
	*/
	public void ajaxCarregaLocalarmazenagemCentrocusto(WebRequestContext request, Projeto projeto){
		Localarmazenagem localarmazenagem = null;
		Centrocusto centrocusto = null;
		
		if(projeto != null && projeto.getCdprojeto() != null){
			Projeto p = projetoService.carregaLocalarmazenagemCentroCusto(projeto);
			if(p != null){				
				if(p.getLocalarmazenagem() != null && p.getLocalarmazenagem().getCdlocalarmazenagem() != null){
					localarmazenagem = p.getLocalarmazenagem();
				}
				if(p.getCentrocusto() != null && p.getCentrocusto().getCdcentrocusto() != null){
					centrocusto = p.getCentrocusto();
				}
			}
		}
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		view.println("var localarmazenagem = '" + (localarmazenagem != null ? "br.com.linkcom.sined.geral.bean.Localarmazenagem[cdlocalarmazenagem="+localarmazenagem.getCdlocalarmazenagem()+"]" : "<null>") + "';");
		view.println("var localarmazenagem_label = '" +(localarmazenagem != null ? localarmazenagem.getNome() : "") + "';");
		view.println("var centrocusto = '" +(centrocusto != null ? "br.com.linkcom.sined.geral.bean.Centrocusto[cdcentrocusto="+centrocusto.getCdcentrocusto()+"]" : "<null>") + "';");
	}
}