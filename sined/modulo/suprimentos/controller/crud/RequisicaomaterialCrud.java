package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.OrigemSuprimentos;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialitem;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneioorigem;
import br.com.linkcom.sined.geral.bean.Situacaorequisicao;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialformulapesoService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialproducaoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueorigemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialService;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialitemService;
import br.com.linkcom.sined.geral.service.ProdutoService;
import br.com.linkcom.sined.geral.service.RequisicaomaterialService;
import br.com.linkcom.sined.geral.service.RomaneioorigemService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.UnidademedidaVO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RequisicaomaterialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/suprimento/crud/Requisicaomaterial",
		authorizationModule=CrudAuthorizationModule.class
)
public class RequisicaomaterialCrud extends CrudControllerSined<RequisicaomaterialFiltro, Requisicaomaterial, Requisicaomaterial>{

	private MaterialService materialService;
	private RequisicaomaterialService requisicaomaterialService;
	private EmpresaService empresaService;
	private CentrocustoService centrocustoService;
	private PatrimonioitemService patrimonioitemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	private ParametrogeralService parametrogeralService;
	private UnidademedidaService unidademedidaService;
	private PedidovendaService pedidovendaService;
	private RomaneioorigemService romaneioorigemService;
	private ProducaoagendamaterialitemService producaoagendamaterialitemService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private MaterialproducaoService materialproducaoService;
	private MaterialformulapesoService materialformulapesoService;
	private ProdutoService produtoService;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private PlanejamentoService planejamentoService;
	private AvisoService avisoService;
	private MotivoavisoService motivoavisoService;
		
	public void setProdutoService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}
	public void setMaterialformulapesoService(
			MaterialformulapesoService materialformulapesoService) {
		this.materialformulapesoService = materialformulapesoService;
	}
	public void setRomaneioorigemService(
			RomaneioorigemService romaneioorigemService) {
		this.romaneioorigemService = romaneioorigemService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {
		this.requisicaomaterialService = requisicaomaterialService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setPatrimonioitemService(
			PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setProducaoagendamaterialitemService(
			ProducaoagendamaterialitemService producaoagendamaterialitemService) {
		this.producaoagendamaterialitemService = producaoagendamaterialitemService;
	}
	public void setProducaoagendamaterialService(
			ProducaoagendamaterialService producaoagendamaterialService) {
		this.producaoagendamaterialService = producaoagendamaterialService;
	}
	public void setMaterialproducaoService(
			MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	public void setMovimentacaoestoqueorigemService(MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {
		this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected ListagemResult<Requisicaomaterial> getLista(WebRequestContext request, RequisicaomaterialFiltro filtro) {
		ListagemResult<Requisicaomaterial> listagemResult = super.getLista(request, filtro);
		List<Requisicaomaterial> lista = listagemResult.list();
		if(lista != null && lista.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdrequisicaomaterial", ",");
			lista.removeAll(lista);
			lista.addAll(requisicaomaterialService.findWithList(whereIn,filtro.getOrderBy(),filtro.isAsc()));
			
			List<Romaneioorigem> listaRomaneioorigem = romaneioorigemService.findByRequisicaomaterial(whereIn);
			for (Requisicaomaterial requisicaomaterial : lista) {
				for (Romaneioorigem romaneioorigem : listaRomaneioorigem) {
					if(romaneioorigem.getRequisicaomaterial() != null && 
							romaneioorigem.getRequisicaomaterial().equals(requisicaomaterial)){
						requisicaomaterial.setRomaneioGerado(Boolean.TRUE);
					}
				}
				
				if(requisicaomaterial.getDtbaixa() != null && requisicaomaterial.getVrequisicaomaterialmovimentacaoestoque()  != null && 
						 requisicaomaterial.getVrequisicaomaterialmovimentacaoestoque().getQtde()  != null){
					requisicaomaterial.setQtderestante(requisicaomaterial.getQtde() - 
							requisicaomaterial.getVrequisicaomaterialmovimentacaoestoque().getQtde());
				}
			}
		}
		
		return listagemResult;
	}
	
	@Override
	protected void validateBean(Requisicaomaterial bean, BindException errors) {
		if("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEARREQUISICAOPROJETO))){
			if(SinedUtil.isListNotEmpty(bean.getRequisicoes())){
				requisicaomaterialService.validaQtdeCompraMaiorQuePlanejamento(bean.getRequisicoes(), bean.getProjeto(), errors);
			}else {
				requisicaomaterialService.validaQtdeCompraMaiorQuePlanejamento(bean, errors);
			}
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Requisicaomaterial bean) throws Exception {
		if(bean.getMaterial() != null){
			requisicaomaterialService.verificaUnidademedidaQtde(bean);	
		}
		
		super.salvar(request, bean);
		
		if(bean.getCdrequisicaomaterial() != null){
			try {
				Aviso aviso = null;
				List<Aviso> avisoList = new ArrayList<Aviso>();
				Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.REQUISICAO_MATERIAL_CRIADA);
				
				if(motivoAviso != null){
					aviso = new Aviso("Requisi��o de material criada", "C�digo da requisi��o: " + bean.getCdrequisicaomaterial(), 
							motivoAviso.getTipoaviso(), motivoAviso.getPapel(), motivoAviso.getAvisoorigem(), bean.getCdrequisicaomaterial(), 
							empresaService.loadPrincipal(), SinedDateUtils.currentDate(), motivoAviso);
					
					avisoList.add(aviso);
					
					avisoService.salvarAvisos(avisoList , true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * M�todo que salva dados em massa.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#preparaSaveEmMassa(Requisicaomaterial)
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#salvaRequisicaoEmMassa(List)
	 * @param request
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	@Command(validate=true)
	@Input("doEntrada")
	public ModelAndView salvaRequisicaoEmMassa(WebRequestContext request, Requisicaomaterial bean){
		requisicaomaterialService.preparaCriacaoRequisicao(bean);
		List<Requisicaomaterial> requisicoes = requisicaomaterialService.preparaSaveEmMassa(bean);
		requisicaomaterialService.salvaRequisicaoEmMassa(requisicoes);
	
		for(Requisicaomaterial requisicaomaterial : requisicoes){
			if(requisicaomaterial.getCdrequisicaomaterial() != null){
				try {
					Aviso aviso = null;
					List<Aviso> avisoList = new ArrayList<Aviso>();
					Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.REQUISICAO_MATERIAL_CRIADA);
					
					if(motivoAviso != null){
						aviso = new Aviso("Requisi��o de material criada", "C�digo da requisi��o: " + requisicaomaterial.getCdrequisicaomaterial(), 
								motivoAviso.getTipoaviso(), motivoAviso.getPapel(), motivoAviso.getAvisoorigem(), requisicaomaterial.getCdrequisicaomaterial(), 
								empresaService.loadPrincipal(), SinedDateUtils.currentDate(), motivoAviso);
						
						avisoList.add(aviso);
						
						avisoService.salvarAvisos(avisoList , true);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		request.addMessage("Registro(s) salvo(s) com sucesso.", MessageType.INFO);
		return new ModelAndView("redirect:/suprimento/crud/Requisicaomaterial?ACAO=listagem");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Requisicaomaterial bean) throws Exception {
		String whereIn = request.getParameter("itenstodelete");
		if(StringUtils.isBlank(whereIn) && bean.getCdrequisicaomaterial() != null){
			whereIn = bean.getCdrequisicaomaterial().toString();
		}
		
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhum item selecionado");
		}
		
		List<Requisicaomaterial> lista = requisicaomaterialService.findProcessoCompra(whereIn);
		
		if( lista.isEmpty()){
			super.excluir(request, bean);
		}else{
			StringBuilder sb = new StringBuilder();
			for (Requisicaomaterial requisicaomaterial : lista) {
				sb.append(requisicaomaterial.getIdentificador()).append(",");
			}
			
			throw new SinedException("As Requisi��es de Material com Identificadores " + sb.toString().substring(0,sb.toString().length()-1)+" n�o podem ser exclu�das pois se encontram com status de Processo de Compra");
		}
		
	}
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Requisicaomaterial form) {
		if((form == null || form.getCdrequisicaomaterial() == null) && StringUtils.isEmpty(request.getParameter("copiar")) && (form.getCopiar() == null || !form.getCopiar()))
			return new ModelAndView("crud/requisicaomaterialEntrada1");
		else
			return new ModelAndView("crud/requisicaomaterialEntrada");	
	}

	@Override
	protected void listagem(WebRequestContext request,	RequisicaomaterialFiltro filtro) throws Exception {
		this.getRegistrosAtivos(request, null);
		
		List<Situacaorequisicao> list = new ArrayList<Situacaorequisicao>();
		list.add(Situacaorequisicao.EM_ABERTO);
		list.add(Situacaorequisicao.AUTORIZADA);
		list.add(Situacaorequisicao.EM_PROCESSO_COMPRA);
		list.add(Situacaorequisicao.COMPRA_NAO_AUTORIZADA);
		list.add(Situacaorequisicao.COMPRA_FINALIZADA);
		list.add(Situacaorequisicao.ROMANEIO_COMPLETO);
		list.add(Situacaorequisicao.BAIXADA);
		list.add(Situacaorequisicao.CANCELADA);
		
		List<Materialclasse> listaclasses = new ArrayList<Materialclasse>();
		listaclasses.add(Materialclasse.PRODUTO);
		listaclasses.add(Materialclasse.PATRIMONIO);
		listaclasses.add(Materialclasse.EPI);
		listaclasses.add(Materialclasse.SERVICO);
		
		List<OrigemSuprimentos> listaorigens = new ArrayList<OrigemSuprimentos>();
		listaorigens.add(OrigemSuprimentos.PROJETO);
		listaorigens.add(OrigemSuprimentos.PEDIDOVENDA);
		listaorigens.add(OrigemSuprimentos.USUARIO);
		
		request.setAttribute("listasituacoesrequisicaomaterial", list);
		request.setAttribute("listaclassesrequisicaomaterial", listaclasses);
		request.setAttribute("listaorigensrequisicaomaterial", listaorigens);
		request.setAttribute("BLOQUEAR_BAIXA_REQUISICAOMATERIAL", parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEAR_BAIXA_REQUISICAOMATERIAL));
	}
	
	/** M�todo que busca registros ativos que ser�o usados tanto 
	 *  na listagem quanto na entrada
	 * 
	 * @param request
	 * @author Tom�s Rabelo
	 * @param requisicaomaterial 
	 */
	private void getRegistrosAtivos(WebRequestContext request, Requisicaomaterial reqMaterial) {
		request.setAttribute("centroCustosAtivos", centrocustoService.findAtivos(reqMaterial != null ? reqMaterial.getCentrocusto() : null));
		request.setAttribute("gruposAtivos", materialgrupoService.findAtivos(reqMaterial != null ? reqMaterial.getMaterialgrupo() : null));
		request.setAttribute("tiposAtivos", materialtipoService.findAtivos(reqMaterial != null ? reqMaterial.getMaterialtipo() : null));
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Requisicaomaterial form) throws CrudException {
		String cdrequisicao = request.getParameter("cdrequisicaomaterial");
		String stCopiar = request.getParameter("copiar");
		if (StringUtils.isNotEmpty(cdrequisicao) && StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
			form.setCdrequisicaomaterial(Integer.parseInt(cdrequisicao));
			form = requisicaomaterialService.loadForEntrada(form);
			form.setCdrequisicaomaterial(null);
			form.setAux_requisicaomaterial(null);
			form.setCopiar(Boolean.TRUE);
			form.setIdentificador(requisicaomaterialService.getUltimoIdentificador()+1);
		}
		
		return super.doEntrada(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Requisicaomaterial form)throws Exception {
		if(request.getAttribute("CONSULTAR") == null && 
			request.getParameter("ACAO") != null && 
			request.getParameter("ACAO").equals("editar") &&	
			form.getCdrequisicaomaterial() != null &&
			form.getAux_requisicaomaterial() != null && 
			form.getAux_requisicaomaterial().getSituacaorequisicao() != null &&
			!form.getAux_requisicaomaterial().getSituacaorequisicao().equals(Situacaorequisicao.EM_ABERTO)){
			throw new SinedException("N�o � poss�vel editar uma requisi��o de material na situa��o diferente de 'EM ABERTO'.");
		}
		
		if(form.getCdrequisicaomaterial() == null){
			if(request.getBindException() == null || !request.getBindException().hasErrors()){
				requisicaomaterialService.preparaCriacaoRequisicao(form);
				List<Requisicaomaterial> requisicoes = new ListSet<Requisicaomaterial>(Requisicaomaterial.class);
				requisicoes.add(new Requisicaomaterial());
				form.setRequisicoes(requisicoes);
				form.setColaborador(SinedUtil.getUsuarioComoColaborador());
				form.setDtcriacao(SinedDateUtils.currentDate());
				
				String paramPlanejamento = request.getParameter("planejamentos");
				if(StringUtils.isNotBlank(paramPlanejamento)){
					form = planejamentoService.gerarRequisicaomaterial(form, paramPlanejamento);
				}
			}
		} else{
			if(form.getMaterial() != null){
				form.setUnidademedidatrans(form.getMaterial().getUnidademedida());
				request.setAttribute("unidademedidamaterial", form.getMaterial().getUnidademedida() != null ? 
						"br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + form.getMaterial().getUnidademedida().getCdunidademedida() + "]": "");
				
			}
			List<OrigemsuprimentosBean> listaBeanDestino = requisicaomaterialService.montaDestino(form);
			request.setAttribute("listaDestino", listaBeanDestino);
		}
		
		if(form.getCdrequisicaomaterial() == null && request.getParameter("whereInCdpedidovenda") != null && 
				!"".equals(request.getParameter("whereInCdpedidovenda")) && 
				!"<null>".equals(request.getParameter("whereInCdpedidovenda"))){
			
			form.setWhereInCdpedidovenda(request.getParameter("whereInCdpedidovenda"));
			
			List<Pedidovenda> listaPedidovenda = pedidovendaService.findForSolicitacaocompra(form.getWhereInCdpedidovenda());
			form.setRequisicoes(new ArrayList<Requisicaomaterial>());
			if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
				Money desconto = new Money();
				for(Pedidovenda pedidovenda : listaPedidovenda){
					for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
						if(pedidovendamaterial.getDesconto() != null){
							desconto = desconto.add(pedidovendamaterial.getDesconto());
						}
					}
					if(form.getProjeto() == null)
						form.setProjeto(pedidovenda.getProjeto());
					if(form.getEmpresa() == null)
						form.setEmpresa(pedidovenda.getEmpresa());
					if(form.getLocalarmazenagem() == null)
						form.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
					form.getRequisicoes().addAll(requisicaomaterialService.getListaMaterialByPedidovenda(pedidovenda));
				}
			}
		}
		
		String paramProducaoagenda = request.getParameter("idsproducaoagenda");
		if(form.getCdrequisicaomaterial() == null && paramProducaoagenda != null && 
				!"".equals(paramProducaoagenda) && !"<null>".equals(paramProducaoagenda)){
			
			List<Requisicaomaterial> listaRequisicaomaterial = new ArrayList<Requisicaomaterial>();
			List<Producaoagendamaterialitem> listaProducaoagendamaterialitem = producaoagendamaterialitemService.findByProducaoagenda(paramProducaoagenda);
			if(listaProducaoagendamaterialitem != null && listaProducaoagendamaterialitem.size() > 0){
				for (Producaoagendamaterialitem producaoagendamaterialitem : listaProducaoagendamaterialitem) {
					Material material = producaoagendamaterialitem.getMaterial();
					Double qtde = producaoagendamaterialitem.getQtde();
					Double volume = producaoagendamaterialitem.getVolume();
					
					Requisicaomaterial requisicaomaterial = new Requisicaomaterial();
					requisicaomaterial.setMaterial(material);
					requisicaomaterial.setQtde(qtde);
					requisicaomaterial.setQtdvolume(volume);
					if(producaoagendamaterialitem.getProducaoagenda() != null && producaoagendamaterialitem.getProducaoagenda().getCdproducaoagenda() != null){
						requisicaomaterial.setCdProducaoagenda(producaoagendamaterialitem.getProducaoagenda().getCdproducaoagenda());
					}
					
					requisicaomaterialService.addMateriaisAgendaproducao(listaRequisicaomaterial, requisicaomaterial);

				}
			} else {
				List<Producaoagendamaterial> listaProducaoagendamaterial = producaoagendamaterialService.findByProducaoagenda(paramProducaoagenda, null);
				for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
					Material material = producaoagendamaterial.getMaterial();
					Double qtdeproduzir = producaoagendamaterial.getQtde();
					Double qtdereferencia = material.getQtdereferencia() != null ? material.getQtdereferencia() : 1d;
					
					if(SinedUtil.isListNotEmpty(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima())){
						for (Producaoagendamaterialmateriaprima materiaprima : producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()) {
							Requisicaomaterial requisicaomaterial = new Requisicaomaterial();
							requisicaomaterial.setMaterial(materiaprima.getMaterial());
							requisicaomaterial.setQtde(materiaprima.getQtdeprevista() / qtdereferencia);
							if(producaoagendamaterial.getProducaoagenda() != null && producaoagendamaterial.getProducaoagenda().getCdproducaoagenda() != null){
								requisicaomaterial.setCdProducaoagenda(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda());
							}
		
							requisicaomaterialService.addMateriaisAgendaproducao(listaRequisicaomaterial, requisicaomaterial);
						}
					}else {
						try{
							material.setProduto_altura(producaoagendamaterial.getAltura());
							material.setProduto_largura(producaoagendamaterial.getLargura());
							material.setQuantidade(qtdeproduzir != null ? qtdeproduzir.doubleValue() : 1.0);
							materialproducaoService.getValorvendaproducao(material);			
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						Material auxMaterial = materialService.loadMaterialComMaterialproducao(material);
						if(auxMaterial != null && auxMaterial.getListaProducao() != null){
							material = auxMaterial;
							material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
	
							for (Materialproducao materialproducao : material.getListaProducao()) {
								Requisicaomaterial requisicaomaterial = new Requisicaomaterial();
								requisicaomaterial.setMaterial(materialproducao.getMaterial());
								requisicaomaterial.setQtde(qtdeproduzir * (materialproducao.getConsumo() / qtdereferencia));
								if(producaoagendamaterial.getProducaoagenda() != null && producaoagendamaterial.getProducaoagenda().getCdproducaoagenda() != null){
									requisicaomaterial.setCdProducaoagenda(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda());
								}
			
								requisicaomaterialService.addMateriaisAgendaproducao(listaRequisicaomaterial, requisicaomaterial);
							}
						}
						else{
							Requisicaomaterial requisicaomaterial = new Requisicaomaterial();
							requisicaomaterial.setMaterial(material);
							requisicaomaterial.setQtde(material.getQuantidade());
							if(producaoagendamaterial.getProducaoagenda() != null && producaoagendamaterial.getProducaoagenda().getCdproducaoagenda() != null){
								requisicaomaterial.setCdProducaoagenda(producaoagendamaterial.getProducaoagenda().getCdproducaoagenda());
							}
							requisicaomaterialService.addMateriaisAgendaproducao(listaRequisicaomaterial, requisicaomaterial);
						}
					}
				}
			}
			form.setRequisicoes(listaRequisicaomaterial);
		}
		
		this.getRegistrosAtivos(request, form);
		
		request.setAttribute("listaOrigem", requisicaomaterialService.montaOrigem(form));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		empresaService.adicionarEmpresaSessaoForFluxocompra(request, form.getEmpresa(), "REQUISICAOMATERIAL");
		request.setAttribute("ignoreHackAndroidDownload", true);
		request.setAttribute("BLOQUEAR_BAIXA_REQUISICAOMATERIAL", parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEAR_BAIXA_REQUISICAOMATERIAL));
	}
	
	/** Fun��o ajax que retorna o s�mbolo da unidade de medida do material + a qtde disponivel do produto
	 * @param request
	 * @param material
	 * @author Tom�s Rabelo
	 */
	public void getComplementoMaterialAjax(WebRequestContext request, Material material){
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(materialService.getSimboloUnidadeMedida(material));
	}
	
	/**
	 * Fun��o ajax que retorna quantidade disponivel do material
	 * @param request
	 * @param requisicaomaterial
	 * @author Tom�s Rabelo
	 */
	public void getQtdDisponivelMaterialAjax(WebRequestContext request, Requisicaomaterial requisicaomaterial){
		Double disponivel = null;
		Boolean naoconsiderarlote = Boolean.valueOf(request.getParameter("naoconsiderarlote") != null ? request.getParameter("naoconsiderarlote") : "false");
		if(requisicaomaterial.getMaterialclasse().getCdmaterialclasse().equals(Materialclasse.PATRIMONIO.getCdmaterialclasse())){
//			Long qtdPatrimonioDisponivel = patrimonioitemService.getQtdDisponivelPatrimonio(requisicaomaterial.getMaterial());
			Long qtdPatrimonioDisponivel = patrimonioitemService.getQtdDisponivelPatrimonio(requisicaomaterial.getMaterial(), requisicaomaterial.getLocalarmazenagem());
			if(qtdPatrimonioDisponivel != null)
				disponivel = qtdPatrimonioDisponivel.doubleValue();
		} else{
			disponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(requisicaomaterial.getMaterial(), requisicaomaterial.getMaterialclasse(), requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getEmpresa(), null, requisicaomaterial.getLoteestoque(), naoconsiderarlote);
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(disponivel == null ? "" : disponivel.toString());
	}
	
	/**
	 * M�todo que busca as classes do material e a unidade de medida
	 * 
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView getInfoMaterial(WebRequestContext request, Requisicaomaterial bean){
		List<Materialclasse> listaClasse = materialService.findClasses(bean.getMaterial());
		Boolean isLocacao = materialService.isLocacaoByMaterial(bean.getMaterial());
		
		List<UnidademedidaVO> listaUnidademedida = new ArrayList<UnidademedidaVO>();
		Unidademedida unidadeMedidaSelecionada = new Unidademedida();
		String simbolo = materialService.getSimboloUnidadeMedida(bean.getMaterial());
		Material material = materialService.unidadeMedidaMaterial(bean.getMaterial());
		
		if (material != null){
			List<Unidademedida> lista = unidademedidaService.unidadeAndUnidadeRelacionada(material.getUnidademedida());
			Material m = materialService.findListaMaterialunidademedida(material);
			
			if(m != null){
				List<Unidademedida> unidadesMedidaAll = new ArrayList<Unidademedida>();
				unidadesMedidaAll.add(m.getUnidademedida());
				
				if(m.getListaMaterialunidademedida() != null && !m.getListaMaterialunidademedida().isEmpty()){
					for(Materialunidademedida materialunidademedida : m.getListaMaterialunidademedida()){
						if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().getCdunidademedida() != null){
							lista.add(materialunidademedida.getUnidademedida());
						}
					}
				}
				
				if(lista != null && lista.size() > 0){
					for (Unidademedida item : lista) {
						unidadesMedidaAll.add(item);
						if(item.getListaUnidademedidaconversao() != null && item.getListaUnidademedidaconversao().size() > 0){
							for (Unidademedidaconversao item2 : item.getListaUnidademedidaconversao()) {
								if(item2.getUnidademedida() != null)
									unidadesMedidaAll.add(item2.getUnidademedida());
								if(item2.getUnidademedidarelacionada() != null)
									unidadesMedidaAll.add(item2.getUnidademedidarelacionada());
							}
						}
					}
				}
				if(unidadesMedidaAll != null && unidadesMedidaAll.size() > 0){
					for (Unidademedida unidademedida : unidadesMedidaAll) {
						if(listaUnidademedida == null) listaUnidademedida = new ArrayList<UnidademedidaVO>();
						UnidademedidaVO unidademedidaVO = new UnidademedidaVO(unidademedida);
						if(!listaUnidademedida.contains(unidademedidaVO)){
							listaUnidademedida.add(unidademedidaVO);
						}
					}
				}				
			}else {
				listaUnidademedida.add(new UnidademedidaVO(material.getUnidademedida()));
			}
			unidadeMedidaSelecionada = material.getUnidademedida();
		}
		
		return new JsonModelAndView().addObject("listaClasse", listaClasse)
									 .addObject("islocacao", isLocacao)
									 .addObject("simbolo", simbolo)
									 .addObject("listaUnidademedida", listaUnidademedida)
									 .addObject("unidademedidamaterial", (material != null && material.getUnidademedida() != null ?  "br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + material.getUnidademedida().getCdunidademedida() + "]" : "<null>"))
									 .addObject("unidadeMedidaSelecionada", (unidadeMedidaSelecionada != null ?  "br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + unidadeMedidaSelecionada.getCdunidademedida() + "]" : "<null>"))
									 ;
	}
	
	/**
	 * M�todo que busca materiais a partir do grupo e tipo escolhidos
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView getMateriais(WebRequestContext request, Requisicaomaterial bean){
		List<Material> listaMaterial = materialService.findByTipoGrupo(bean.getMaterial().getMaterialgrupo(), bean.getMaterial().getMaterialtipo());
		return new JsonModelAndView().addObject("listaMaterial", listaMaterial);
	}
	
	/**
	 * Action que cria o CSV da listagem de Solicita��o de compra.
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView gerarCsv(WebRequestContext request, RequisicaomaterialFiltro filtro){
		List<Requisicaomaterial> lista = requisicaomaterialService.findForCsv(filtro);
		StringBuilder rel = new StringBuilder();
		
		rel.append("Ident.;");
		rel.append("Descri��o;");
		rel.append("Classe;");
		rel.append("Material/servi�o;");
		rel.append("U.M.;");
		rel.append("Qtde.;");
		rel.append("Frequ�ncia;");
		rel.append("Qtde. freq.;");
		rel.append("Centro de custo;");
		rel.append("Empresa;");
		rel.append("Departamento;");
		rel.append("Projeto;");
		rel.append("Local armazenagem;");
		rel.append("Requisitante;");
		rel.append("Projeto origem;");
		rel.append("Usu�rio;");
		rel.append("Material origem;");
		rel.append("Cria��o;");
		rel.append("Data limite;");
		rel.append("Data autoriza��o;");
		rel.append("Data cancelamento;");
		rel.append("Data baixa;");
		rel.append("Observa��o;");
		rel.append("Situa��o;");
		rel.append("\n");
		
		for (Requisicaomaterial r : lista) {
			if(r.getIdentificador() != null)
				rel.append(r.getIdentificador().toString());
			rel.append(";");
			if(r.getDescricao() != null)
				rel.append("("+r.getDescricao()+")");
			rel.append(";");
			if(r.getMaterialclasse() != null && r.getMaterialclasse().getNome() != null)
				rel.append(r.getMaterialclasse().getNome());
			rel.append(";");
			if(r.getMaterial() != null && r.getMaterial().getNome() != null)
				rel.append(r.getMaterial().getNome());
			rel.append(";");
			if(r.getMaterial() != null && r.getMaterial().getUnidademedida() != null && r.getMaterial().getUnidademedida().getSimbolo() != null)
				rel.append(r.getMaterial().getUnidademedida().getSimbolo());
			rel.append(";");
			if(r.getQtde() != null)
				rel.append(r.getQtde());
			rel.append(";");
			if(r.getFrequencia() != null && r.getFrequencia().getNome() != null)
				rel.append(r.getFrequencia().getNome());
			rel.append(";");
			if(r.getQtdefrequencia() != null)
				rel.append(r.getQtdefrequencia());
			rel.append(";");
			if(r.getCentrocusto() != null && r.getCentrocusto().getNome() != null)
				rel.append(r.getCentrocusto().getNome());
			rel.append(";");
			if(r.getEmpresa() != null && r.getEmpresa().getRazaosocialOuNome() != null)
				rel.append(r.getEmpresa().getRazaosocialOuNome());
			rel.append(";");
			if(r.getDepartamento() != null && r.getDepartamento().getNome() != null)
				rel.append(r.getDepartamento().getNome());
			rel.append(";");
			if(r.getProjeto() != null && r.getProjeto().getNome() != null)
				rel.append(r.getProjeto().getNome());
			rel.append(";");
			if(r.getLocalarmazenagem() != null && r.getLocalarmazenagem().getNome() != null)
				rel.append(r.getLocalarmazenagem().getNome());
			rel.append(";");
			if(r.getColaborador() != null && r.getColaborador().getNome() != null)
				rel.append(r.getColaborador().getNome());
			rel.append(";");
			if(r.getRequisicaoorigem() != null && r.getRequisicaoorigem().getProjeto() != null && r.getRequisicaoorigem().getProjeto().getNome() != null)
				rel.append(r.getRequisicaoorigem().getProjeto().getNome());
			rel.append(";");
			if(r.getRequisicaoorigem() != null && r.getRequisicaoorigem().getUsuario() != null && r.getRequisicaoorigem().getUsuario().getNome() != null)
				rel.append(r.getRequisicaoorigem().getUsuario().getNome());
			rel.append(";");
			if(r.getRequisicaoorigem() != null && r.getRequisicaoorigem().getMaterial() != null && r.getRequisicaoorigem().getMaterial().getNome() != null)
				rel.append(r.getRequisicaoorigem().getMaterial().getNome());
			rel.append(";");
			if(r.getDtcriacao() != null)
				rel.append(SinedDateUtils.toString(r.getDtcriacao()));
			rel.append(";");
			if(r.getDtlimite() != null)
				rel.append(SinedDateUtils.toString(r.getDtlimite()));
			rel.append(";");
			if(r.getDtautorizacao() != null)
				rel.append(SinedDateUtils.toString(r.getDtautorizacao()));
			rel.append(";");
			if(r.getDtcancelamento() != null)
				rel.append(SinedDateUtils.toString(r.getDtcancelamento()));
			rel.append(";");
			if(r.getDtbaixa() != null)
				rel.append(SinedDateUtils.toString(r.getDtbaixa()));
			rel.append(";");
			if(r.getObservacao() != null)
				rel.append(r.getObservacao());
			rel.append(";");
			if(r.getAux_requisicaomaterial() != null && r.getAux_requisicaomaterial().getSituacaorequisicao() != null 
				&& r.getAux_requisicaomaterial().getSituacaorequisicao().getDescricao() != null){
				
				rel.append(r.getAux_requisicaomaterial().getSituacaorequisicao().getDescricao());
			}
			rel.append(";\n");
			
		}
		
		Resource resource = new Resource("text/csv","requisicaomaterial_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	/**
	 * 	 * M�todo ajax para adiconar a empresa na sess�o (fluxo compra)
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void colocarEmpresaSessao(WebRequestContext request){
		empresaService.adicionarEmpresaSessaoForFluxocompra(request, null, "REQUISICAOMATERIAL");
	}
	
	/**
	 * Action em ajax para verificar a existencia de romaneio
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/04/2014
	 */
	public ModelAndView ajaxVerificaRomaneioGerado(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn != null && !whereIn.equals("")){
			List<Romaneioorigem> lista = romaneioorigemService.findByRequisicaomaterial(whereIn);
			jsonModelAndView.addObject("haveRomaneio", lista != null && lista.size() > 0);
			jsonModelAndView.addObject("haveMovimentacaoestoque", movimentacaoestoqueorigemService.existeMovEstoqueComOrigemRequisicaomaterial(whereIn, true));
		}
		return jsonModelAndView;
	}
	
	/**
	 * Realiza o c�lculo de peso do material de acordo com infos da requisi��o de material
	 *
	 * @param request
	 * @param requisicaomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/01/2015
	 */
	public ModelAndView calculaPesoMaterialAjax(WebRequestContext request, Requisicaomaterial requisicaomaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(requisicaomaterial != null && requisicaomaterial.getMaterial() != null){
			try{
				Double qtde = requisicaomaterial.getQtde() != null ? requisicaomaterial.getQtde() : 0d;
				boolean addPesoCampoQtde = false;
				
				Material material = materialService.load(requisicaomaterial.getMaterial(), "material.cdmaterial, material.materialtipo, material.peso, material.pesobruto");
				if(material == null){
					material = materialService.load(requisicaomaterial.getMaterial(), "material.cdmaterial, material.peso, material.pesobruto");
				}
				material.setListaMaterialformulapeso(materialformulapesoService.findByMaterial(material));
				
				Double peso = material.getPeso() != null ? material.getPeso() : material.getPesobruto();
				if(material.getListaMaterialformulapeso() != null && material.getListaMaterialformulapeso().size() > 0){
				
					Double altura = requisicaomaterial.getAltura() != null ? requisicaomaterial.getAltura() : 0d;
					Double comprimento = requisicaomaterial.getComprimento() != null ? requisicaomaterial.getComprimento() : 0d;
					Double largura = requisicaomaterial.getLargura() != null ? requisicaomaterial.getLargura() : 0d;
					Double qtdvolume = requisicaomaterial.getQtdvolume() != null ? requisicaomaterial.getQtdvolume() : 0d;
					
					material.setProduto_altura(altura);
					material.setProduto_comprimento(comprimento);
					material.setProduto_largura(largura);
					material.setQtdvolume(qtdvolume);
					
					Material m = materialService.getSimboloUnidadeMedidaETempoEntrega(requisicaomaterial.getMaterial());
					Unidademedida unidademedida = m != null ? unidademedida = m.getUnidademedida() : null;
					if(requisicaomaterial.getUnidademedidatrans() != null && requisicaomaterial.getUnidademedidatrans().getCdunidademedida() != null){
						requisicaomaterial.setUnidademedidatrans(unidademedidaService.load(requisicaomaterial.getUnidademedidatrans(), 
								"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo"));
						unidademedida = requisicaomaterial.getUnidademedidatrans();
					}
					
					peso = materialformulapesoService.calculaPesoMaterial(material);
					
					if(peso != null && 
							peso > 0 && 
							unidademedida != null && 
							unidademedida.getSimbolo() != null &&
							"KG".equalsIgnoreCase(unidademedida.getSimbolo())){
						addPesoCampoQtde = true;
					}
				}
				
				if(peso != null && !addPesoCampoQtde){
					peso = peso * qtde;
				}
				
				jsonModelAndView
					.addObject("peso", peso != null ? SinedUtil.roundByParametro(peso) : 0d)
					.addObject("addPesoCampoQtde", addPesoCampoQtde);
			} catch (Exception e) {
				e.printStackTrace();
				return jsonModelAndView.addObject("erro", e.getMessage());
			}
		}
		return jsonModelAndView;
	}
	
	/**
	 * Busca as dimens�es do material.
	 *
	 * @param request
	 * @param requisicaomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/01/2015
	 */
	public ModelAndView verificaDimensoesMaterialAjax(WebRequestContext request, Requisicaomaterial requisicaomaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(requisicaomaterial != null && requisicaomaterial.getMaterial() != null){
			Produto produto = produtoService.load(new Produto(requisicaomaterial.getMaterial()), "produto.cdmaterial, produto.altura, produto.comprimento, produto.largura");
			
			if(produto != null){
				if(produto.getAltura() != null){
					jsonModelAndView.addObject("altura", produto.getAltura());
				}
				if(produto.getComprimento() != null){
					jsonModelAndView.addObject("comprimento", produto.getComprimento());
				}
				if(produto.getLargura() != null){
					jsonModelAndView.addObject("largura", produto.getLargura());
				}
			}
		}
		return jsonModelAndView;
	}
	
	
	public ModelAndView abrirPopUpImportarRM(WebRequestContext request ){
		Requisicaomaterial requisicaomaterial= new Requisicaomaterial();
		requisicaomaterialService.preparaCriacaoRequisicao(requisicaomaterial);
		requisicaomaterial.setDescricao(null);
		return new ModelAndView("direct:/crud/popup/importarRequisicaomaterial", "requisicaomaterial",requisicaomaterial);
	}
	
	
	public ModelAndView importarRequisicaomaterialArquivo(WebRequestContext request, Requisicaomaterial requisicaomaterial) {
		if(requisicaomaterial.getArquivo() == null){
			request.addError("Arquivo n�o informado para a importa��o da ordem de compra.");
			SinedUtil.redirecionamento(request, "/suprimento/crud/Requisicaomaterial");
			return null;
		}
		Requisicaomaterial rm = requisicaomaterialService.criaRequisicaomaterialByImportacao(requisicaomaterial.getArquivo(), requisicaomaterial.getIdentificador(), requisicaomaterial.getDescricao(), requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getDtlimite(), requisicaomaterial.getCentrocusto(), requisicaomaterial.getProjeto(), requisicaomaterial.getEmpresa(), requisicaomaterial.getColaborador(), requisicaomaterial.getObservacao());
		
		if(rm.getListaErros() != null && !rm.getListaErros().isEmpty()){
			request.addError(rm.getListaErros());
		}else{
			for(Requisicaomaterial requisicao : rm.getRequisicoes()){
			requisicaomaterialService.preparaCriacaoRequisicao(requisicao);
			}
			requisicaomaterialService.salvaRequisicaoEmMassa(rm.getRequisicoes());
			SinedUtil.redirecionamento(request, "/suprimento/crud/Requisicaomaterial");
		}

		return null;
	}
}
