package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Materialdevolucao;
import br.com.linkcom.sined.geral.bean.Materialdevolucaohistorico;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.MaterialdevolucaoService;
import br.com.linkcom.sined.geral.service.MaterialdevolucaohistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MaterialdevolucaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/suprimento/crud/Materialdevolucao",
		authorizationModule=CrudAuthorizationModule.class
)
public class MaterialdevolucaoCrud extends CrudControllerSined<MaterialdevolucaoFiltro, Materialdevolucao, Materialdevolucao>{
	
	private MaterialdevolucaoService materialdevolucaoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MaterialdevolucaohistoricoService materialdevolucaohistoricoService;
	private ValecompraService valeCompraService;
	private InventarioService inventarioService;
	
	public void setValeCompraService(ValecompraService valeCompraService) {
		this.valeCompraService = valeCompraService;
	}
	public void setMaterialdevolucaoService(MaterialdevolucaoService materialdevolucaoService) {
		this.materialdevolucaoService = materialdevolucaoService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setMaterialdevolucaohistoricoService(MaterialdevolucaohistoricoService materialdevolucaohistoricoService) {
		this.materialdevolucaohistoricoService = materialdevolucaohistoricoService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Materialdevolucao form) throws CrudException {
		String acao = request.getParameter("ACAO");
		if(acao != null && !"consultar".equals(acao)){
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade no sistema.");
		}
		
		return super.doEditar(request, form);
	}
	
	@Override
	protected ListagemResult<Materialdevolucao> getLista(WebRequestContext request, MaterialdevolucaoFiltro filtro) {
		filtro.setQtdetotaldevolvida(materialdevolucaoService.getTotalQtdeDevolvidaListagem(filtro));
		return super.getLista(request, filtro);
	}
	
	@Override
	protected Materialdevolucao criar(WebRequestContext request, Materialdevolucao form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade no sistema.");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Materialdevolucao form) throws CrudException {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade no sistema.");
	}

	/**
	 * M�todo que gera o CSV de Devolu��o de material
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarCsv(WebRequestContext request, MaterialdevolucaoFiltro filtro){
		List<Materialdevolucao> listaMaterial = materialdevolucaoService.findForCsv(filtro);
		
		StringBuilder rel = new StringBuilder();
		
		rel.append("C�digo de venda;Data de devolu��o;Vendedor;Data da venda;Descri��o da Devolu��o;");
		rel.append("Valor total da devolu��o;Material;Quantidade devolvida;Quantidade vendida;");
		rel.append("\n");
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		if(listaMaterial != null && !listaMaterial.isEmpty()){
			for (Materialdevolucao materialdevolucao : listaMaterial) {
				if(materialdevolucao.getVenda() != null)
					rel.append(materialdevolucao.getVenda().getCdvenda());
				rel.append(";");
				
				if(materialdevolucao.getDtaltera() != null)
					rel.append(format.format(new Date(materialdevolucao.getDtaltera().getTime())));
				rel.append(";");
				
				if(materialdevolucao.getVenda() != null && materialdevolucao.getVenda().getColaborador() != null)
					rel.append(materialdevolucao.getVenda().getColaborador().getNome());
				rel.append(";");
				
				if(materialdevolucao.getVenda() != null && materialdevolucao.getVenda().getDtvenda() != null)
					rel.append(format.format(materialdevolucao.getVenda().getDtvenda()));
				rel.append(";");
				
//				rel.append(materialdevolucao.getObsHistoricoTrans());
				rel.append(";");
	
				if(materialdevolucao.getPreco() != null && materialdevolucao.getQtdedevolvida() != null)
					rel.append(new Money(materialdevolucao.getPreco()*materialdevolucao.getQtdedevolvida()));
				rel.append(";");
				
				if(materialdevolucao.getMaterial() != null)
					rel.append(materialdevolucao.getMaterial().getNome());
				rel.append(";");
				
				if(materialdevolucao.getQtdedevolvida() != null)
					rel.append(materialdevolucao.getQtdedevolvida());
				rel.append(";");
				
				if(materialdevolucao.getQtdevendida() != null)
					rel.append(materialdevolucao.getQtdevendida());
				rel.append(";");
				
				
				rel.append("\n");
			}
		}
		
		Resource resource = new Resource("text/csv","devolucao_material_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	@Action("emitirDevolucao")
	public ModelAndView emitirDevolucao(WebRequestContext request) throws Exception {
		return new ResourceModelAndView(materialdevolucaoService.gerarComprovante(SinedUtil.getItensSelecionados(request)));
	}
	
	/**
	* M�todo que cancela a movimenta��o de estoque e a devolu��o de material
	*
	* @param request
	* @return
	* @since 20/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView cancelar(WebRequestContext request) {
		String urlretorno = "/suprimento/crud/Materialdevolucao";
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:" + urlretorno);
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		if(entrada) urlretorno += "?ACAO=consultar&cdmaterialdevolucao=" + whereIn;
		List<Materialdevolucao> lista = materialdevolucaoService.findForCancelamento(whereIn);
		
		if(SinedUtil.isListNotEmpty(lista)){
			StringBuilder whereInMovimentacaoestoque = new StringBuilder();
			boolean erro = false;
			String erroFiscal = null;
			
			for(Materialdevolucao materialdevolucao : lista){
				if(materialdevolucao.getDtcancelamento() != null){
					erro = true;
					break;
				}
				if(materialdevolucao.getEntregadocumento() != null && !Entregadocumentosituacao.CANCELADA.equals(materialdevolucao.getEntregadocumento().getEntregadocumentosituacao())){
					erroFiscal = materialdevolucao.getEntregadocumento().getNumero();
					break;
				}
				
				if (materialdevolucao.getMovimentacaoestoque() != null && inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(
						materialdevolucao.getMovimentacaoestoque().getDtmovimentacao(), materialdevolucao.getMovimentacaoestoque().getEmpresa(), 
						materialdevolucao.getMovimentacaoestoque().getLocalarmazenagem(), materialdevolucao.getMovimentacaoestoque().getProjeto())) {
					request.addError("N�o � poss�vel realizar o cancelamento, pois o registro est� vinculado � um registro de invent�rio. Para cancelar, � necess�rio cancelar o invent�rio.");
					return new ModelAndView("redirect:" + urlretorno);
				}
				
				if(materialdevolucao.getMovimentacaoestoque() != null && materialdevolucao.getMovimentacaoestoque().getCdmovimentacaoestoque() != null){
					whereInMovimentacaoestoque.append(materialdevolucao.getMovimentacaoestoque().getCdmovimentacaoestoque()).append(",");
				}else if(StringUtils.isNotBlank(materialdevolucao.getNumeronotadevolucao())){
					request.addMessage("A devolu��o " + materialdevolucao.getCdmaterialdevolucao() + (materialdevolucao.getVenda() != null ? " (C�digo da Venda = " + materialdevolucao.getVenda().getCdvenda() + ")" : "" ) + 
					" est� associada � nota " + materialdevolucao.getNumeronotadevolucao() + ", que dever� ser cancelada manualmente.");
				}else {
					request.addMessage("A devolu��o " + materialdevolucao.getCdmaterialdevolucao() + (materialdevolucao.getVenda() != null ? " (C�digo da Venda = " + materialdevolucao.getVenda().getCdvenda() + ")" : "" ) + 
							" est� associada a uma movimenta��o de estoque que dever� ser cancelada manualmente.");
				}
			}
			if(erroFiscal != null){
				request.addError("A devolu��o est� associada � uma entrada fiscal "+erroFiscal+" e por isso n�o poder� ser cancelada.");
				return new ModelAndView("redirect:" + urlretorno);	
			}
			if(erro){
				request.addError("N�o � poss�vel cancelar devolu��o que j� est� cancelada.");
				return new ModelAndView("redirect:" + urlretorno);
			}
			
			
			if(!validarValeCompra(lista)){
				request.addError("O cliente n�o possui saldo suficiente em vale-compras para realizar o estorno referente ao cancelamento desta devolu��o.");
				return new ModelAndView("redirect:" + urlretorno);
			}
			
			materialdevolucaoService.updateDtcancelamento(whereIn);
			
			if(StringUtils.isNotBlank(whereInMovimentacaoestoque.toString())){
				movimentacaoestoqueService.doUpdateMovimentacoes(whereInMovimentacaoestoque.substring(0, whereInMovimentacaoestoque.length()-1));
			}
			
			Usuario usuario = SinedUtil.getUsuarioLogado();
			Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
			
			for(Materialdevolucao materialdevolucao : lista){
				estonarSaldoValeCompra(materialdevolucao);
				Materialdevolucaohistorico materialdevolucaohistorico = new Materialdevolucaohistorico();
				materialdevolucaohistorico.setMaterialdevolucao(materialdevolucao);
				materialdevolucaohistorico.setAcao("Cancelado");
				materialdevolucaohistorico.setUsuario(usuario);
				materialdevolucaohistorico.setDtaltera(dtaltera);
				materialdevolucaohistoricoService.saveOrUpdate(materialdevolucaohistorico);
			}	
			request.addMessage("Opera��o realizada com sucesso.");
		}
		return new ModelAndView("redirect:" + urlretorno);
		
	}
	private void estonarSaldoValeCompra(Materialdevolucao materialdevolucao) {
		if(Boolean.TRUE.equals(materialdevolucao.isValeCompra())){
			Money valor = new Money(materialdevolucao.getPreco());
			if(materialdevolucao.getQtdedevolvida() != null){
				Money quantidade = new Money(materialdevolucao.getQtdedevolvida());
				valor = valor.multiply(quantidade);
			}
			Valecompra vale = new Valecompra();
			
			Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
			Valecompraorigem valecompraorigem = new Valecompraorigem();
			valecompraorigem.setMaterialDevolucao(materialdevolucao);
			listaValecompraorigem.add(valecompraorigem);
			
			vale.setCliente(materialdevolucao.getVenda().getCliente());
			vale.setData(SinedDateUtils.currentDate());
			vale.setIdentificacao("Cancelamento da devolu��o "+materialdevolucao.getCdmaterialdevolucao().toString());
			vale.setValor(valor);
			vale.setListaValecompraorigem(listaValecompraorigem);
			vale.setTipooperacao(Tipooperacao.TIPO_DEBITO);
			valeCompraService.saveOrUpdate(vale);
		}	
	}
	private boolean validarValeCompra(List<Materialdevolucao> lista) {
		Money valorDevolvido = new Money(0.0);
		Money valorValeCompra = new Money(0.0);
		HashMap<Integer, Cliente> mapClientes = new HashMap<Integer, Cliente>();
		for (Materialdevolucao materialdevolucao : lista) {
			if(Boolean.TRUE.equals(materialdevolucao.isValeCompra())){
				if(materialdevolucao.getPreco() != null){
					Money preco = new Money(materialdevolucao.getPreco());
					if(materialdevolucao.getQtdedevolvida() !=null){
						Money quantidade = new Money(materialdevolucao.getQtdedevolvida());
						preco = preco.multiply(quantidade);
					}
					valorDevolvido.add(preco);		
					Money valorValeCliente = valeCompraService.getSaldoByCliente(materialdevolucao.getVenda().getCliente(), null);
					if(preco.compareTo(valorValeCliente)==-1 ){
						return false;
					}else{
						valorValeCompra.add(valorValeCliente);
					}
					Integer cdCliente = materialdevolucao.getVenda().getCliente().getCdpessoa();
					mapClientes.put(cdCliente, materialdevolucao.getVenda().getCliente());
				}
			}
		}
		for (Map.Entry<Integer, Cliente> map : mapClientes.entrySet()) {
			Cliente cli = map.getValue();
			valorValeCompra.add(valeCompraService.getSaldoByCliente(cli, null));
		}
		if(valorDevolvido.compareTo(valorValeCompra)!= -1){
			return true;
		}
		return false;
	}
	
}
