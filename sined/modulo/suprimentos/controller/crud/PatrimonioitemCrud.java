package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Patrimonioitemhistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;
import br.com.linkcom.sined.geral.service.BempatrimonioService;
import br.com.linkcom.sined.geral.service.FornecimentocontratoService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialnumeroserieService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.geral.service.MovpatrimonioorigemService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.PatrimonioitemhistoricoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarMovimentacaoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarPlaquetaBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PatrimonioitemFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path={"/suprimento/crud/Patrimonioitem", "/patrimonio/crud/Patrimonioitem"}, authorizationModule=CrudAuthorizationModule.class)
public class PatrimonioitemCrud extends CrudControllerSined<PatrimonioitemFiltro, Patrimonioitem, Patrimonioitem>{

	private PatrimonioitemService patrimonioitemService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	private MovpatrimonioorigemService movpatrimonioorigemService;
	private MovpatrimonioService movpatrimonioService;
	private PatrimonioitemhistoricoService patrimonioitemhistoricoService;
	private BempatrimonioService bempatrimonioService;
	private ProjetoService projetoService;
	private FornecimentocontratoService fornecimentocontratoService;
	private MaterialnumeroserieService materialnumeroserieService;
	
	public void setPatrimonioitemhistoricoService(
			PatrimonioitemhistoricoService patrimonioitemhistoricoService) {
		this.patrimonioitemhistoricoService = patrimonioitemhistoricoService;
	}
	public void setMovpatrimonioService(
			MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}
	public void setMovpatrimonioorigemService(
			MovpatrimonioorigemService movpatrimonioorigemService) {
		this.movpatrimonioorigemService = movpatrimonioorigemService;
	}
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setBempatrimonioService(BempatrimonioService bempatrimonioService) {
		this.bempatrimonioService = bempatrimonioService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setFornecimentocontratoService(FornecimentocontratoService fornecimentocontratoService) {
		this.fornecimentocontratoService = fornecimentocontratoService;
	}
	public void setMaterialnumeroserieService(MaterialnumeroserieService materialnumeroserieService) {
		this.materialnumeroserieService = materialnumeroserieService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, PatrimonioitemFiltro filtro) throws Exception {
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		request.setAttribute("listaTipo", materialtipoService.findAtivos());
		request.setAttribute("listaSituacao", Arrays.asList(Patrimonioitemsituacao.values()));
		request.setAttribute("textoPlaquetaSerie", patrimonioitemService.getTextoPlaquetaSerie());
	}
	
	@Override
	protected Patrimonioitem criar(WebRequestContext request, Patrimonioitem form) throws Exception {
		Patrimonioitem item = super.criar(request, form);
		item.setAtivo(true);
		
		if(request.getParameter("fromFornecimentocontrato") != null && "true".equals(request.getParameter("fromFornecimentocontrato")) &&
				request.getParameter("cdmaterial") != null && !"".equals(request.getParameter("cdmaterial"))){
			item.setFornecimentocontrato(new Fornecimentocontrato(Integer.parseInt(request.getParameter("cdfornecimentocontrato"))));
			item.setBempatrimonio(bempatrimonioService.carregaPatrimonio(new Material(Integer.parseInt(request.getParameter("cdmaterial")))));
		}
		return item;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Patrimonioitem bean) throws Exception {
		try {
			String obs = bean.getObservacao();
			Double horimetro = bean.getHorimetro();
			
			if (bean.getPlaqueta() != null && "".equals(bean.getPlaqueta().trim()))
				bean.setPlaqueta(null);
			
			super.salvar(request, bean);
			
			if((obs != null && !obs.trim().equals("")) || horimetro != null){
				Patrimonioitemhistorico historico = new Patrimonioitemhistorico();
				historico.setHorimetro(horimetro);
				historico.setObservacao(obs);
				historico.setPatrimonioitem(bean);
				
				patrimonioitemhistoricoService.saveOrUpdate(historico);
			}
			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PATRIMONIOITEM_PLAQUETA")) {
				throw new SinedException("Plaqueta j� cadastrada no sistema.");
			}
		}
	}
	
	
	/**
	 * Carrega a pop-up para preenchimento das plaquetas.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView popUpPlaqueta(WebRequestContext request, GerarPlaquetaBean bean){
		
		if (bean.getLista() == null || bean.getLista().size() == 0) {
			String itensSelecionados = request.getParameter("selectedItens");
			if (itensSelecionados == null || itensSelecionados.equals("")) {
				throw new SinedException("Nenhum item selecionado.");
			}
			
			List<Patrimonioitem> lista = patrimonioitemService.findWithoutPlaqueta(itensSelecionados);
			
			if(lista == null || lista.size() == 0){
				request.addError("Patrim�nio(s) escolhido(s) j� possui(em) plaqueta.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
			
			bean.setLista(lista);
		}

		return new ModelAndView("direct:/crud/popup/plaqueta").addObject("bean", bean);
	}
	
	/**
	 * M�todo para salvar as plaquetas dos itens de patrim�nio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#updateListaPlaquetas
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	@Input("popUpPlaqueta")
	public void savePlaqueta(WebRequestContext request, GerarPlaquetaBean bean) throws Exception {
		
		if (bean == null || bean.getLista() == null) {
			throw new SinedException("Lista de itens de patrim�nio n�o pode ser nulo.");
		}
		
		patrimonioitemService.updateListaPlaquetas(bean.getLista());
		
		request.getServletResponse().setContentType("text/html");
		SinedUtil.fechaPopUp(request);
	}
	
	public ModelAndView gerarMovimentacao(WebRequestContext request){
		
		String itensSelecionados = request.getParameter("selectedItens");
		if (itensSelecionados == null || itensSelecionados.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		GerarMovimentacaoBean bean = new GerarMovimentacaoBean();
		bean.setItensSelecionados(itensSelecionados);
		
		request.setAttribute("listaProjetos", projetoService.findForComboByUsuariologadoNotCancelado());
		return new ModelAndView("direct:/crud/popup/gerarMovimentacao").addObject("bean", bean);
	}
	
	public void saveGerarMovimentacao(WebRequestContext request, GerarMovimentacaoBean bean){
		String itensSelecionados = bean.getItensSelecionados();
		String[] ids = itensSelecionados.split(",");
		
		Movpatrimonio mov;
		Movpatrimonioorigem ori;
		Timestamp today = new Timestamp(System.currentTimeMillis());
		StringBuilder whereInPatrimonioitem = new StringBuilder();
		for (int i = 0; i < ids.length; i++) {
			ori = new Movpatrimonioorigem();
			ori.setUsuario(SinedUtil.getUsuarioLogado());
			
			movpatrimonioorigemService.saveOrUpdate(ori);
			
			mov = new Movpatrimonio();
			mov.setLocalarmazenagem(bean.getLocalarmazenagem());
			mov.setDepartamento(bean.getDepartamento());
			mov.setEmpresa(bean.getEmpresa());
			mov.setColaborador(bean.getColaborador());
			mov.setFornecedor(bean.getFornecedor());
			mov.setCliente(bean.getCliente());
			mov.setProjeto(bean.getProjeto());
			mov.setMovpatrimonioorigem(ori);
			mov.setPatrimonioitem(new Patrimonioitem(Integer.parseInt(ids[i])));
			mov.setDtmovimentacao(today);
			
			movpatrimonioService.saveOrUpdate(mov);
			
			whereInPatrimonioitem.append(ids[i]).append(",");
		}
		
		request.addMessage("Movimenta��o de patrim�nio realizada.");
		request.getServletResponse().setContentType("text/html");
		
		if(bean.getGerarRomaneio() != null && bean.getGerarRomaneio()){
			String param = "";
			if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
				param += "&cdempresa="+bean.getEmpresa().getCdpessoa();
			}
			if(bean.getLocalarmazenagem() != null && bean.getLocalarmazenagem().getCdlocalarmazenagem() != null){
				param += "&cdlocal="+bean.getLocalarmazenagem().getCdlocalarmazenagem();
			}
			if(!"".equals(whereInPatrimonioitem.toString())){
				param += "&whereInPatrimonioitem=" + whereInPatrimonioitem.toString().substring(0,whereInPatrimonioitem.length()-1);
			}
			SinedUtil.redirecionamento(request, "/suprimento/crud/Romaneio?ACAO=criar&gerarRomaneioMov=true" + param);
		}else {
			SinedUtil.fechaPopUp(request);
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Patrimonioitem form)throws Exception {
		form.setFromVeiculo("true".equals(request.getParameter("fromVeiculo")));
		if (NumberUtils.isNumber(request.getParameter("cdmaterialFromVeiculo"))){
			form.setBempatrimonio(bempatrimonioService.carregaPatrimonio(new Material(Integer.valueOf(request.getParameter("cdmaterialFromVeiculo")))));
		}
		
		List<Fornecimentocontrato> listaFornecimentocontrato = new ArrayList<Fornecimentocontrato>();
		if(form.getFornecimentocontrato() != null){
			listaFornecimentocontrato.add(form.getFornecimentocontrato());
			listaFornecimentocontrato = fornecimentocontratoService.findForCombo(listaFornecimentocontrato, true);
		}
		request.setAttribute("listaFornecimentocontrato", listaFornecimentocontrato);
		
		if(form.getCdpatrimonioitem() != null){
			if(form.getFoto() != null && form.getFoto().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), form.getFoto().getCdarquivo());
			}
		}
		
//		if ("TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM))){
//			if (form.getBempatrimonio()!=null){
//				form.getBempatrimonio().setListaMaterialnumeroserie(new ListSet<Materialnumeroserie>(Materialnumeroserie.class, materialnumeroserieService.findByCdsmaterial(form.getBempatrimonio().getCdmaterial().toString())));
//			}
//		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Patrimonioitem form) throws CrudException {
		if (Boolean.TRUE.equals(form.getFromVeiculo())){
			try {
				salvar(request, form);
			}catch (Exception e) {
				e.printStackTrace();
			}
			SinedUtil.recarregarPaginaWithClose(request);
			return null;
		}else {
			return super.doSalvar(request, form);
		}		
	}
	
	@Override
	protected ListagemResult<Patrimonioitem> getLista(WebRequestContext request, PatrimonioitemFiltro filtro) {
		ListagemResult<Patrimonioitem> listagemResult = super.getLista(request, filtro);
		List<Patrimonioitem> listaPatrimonioitem = listagemResult.list(); 
		Map<Integer, List<Materialnumeroserie>> mapa = new HashMap<Integer, List<Materialnumeroserie>>();
//		String whereIn = "-1";
		StringBuilder whereIn = new StringBuilder("-1");
		
		for (Patrimonioitem patrimonioitem: listaPatrimonioitem){
			if (patrimonioitem.getBempatrimonio()!=null){
//				whereIn += ", " + patrimonioitem.getBempatrimonio().getCdmaterial();
				whereIn.append(", " + patrimonioitem.getBempatrimonio().getCdmaterial());
			}
		}
		
		for (Materialnumeroserie materialnumeroserie: materialnumeroserieService.findByCdsmaterial(whereIn.toString())){
			List<Materialnumeroserie> listaMaterialnumeroserie = mapa.get(materialnumeroserie.getMaterial().getCdmaterial());
			if (listaMaterialnumeroserie==null){
				listaMaterialnumeroserie = new ArrayList<Materialnumeroserie>();
			}
			listaMaterialnumeroserie.add(materialnumeroserie);
			mapa.put(materialnumeroserie.getMaterial().getCdmaterial(), listaMaterialnumeroserie);
		}			
		
		for (Patrimonioitem patrimonioitem: listaPatrimonioitem){
			if (patrimonioitem.getBempatrimonio()!=null){
				List<Materialnumeroserie> listaMaterialnumeroserie = mapa.get(patrimonioitem.getBempatrimonio().getCdmaterial());
				if (listaMaterialnumeroserie!=null){
					patrimonioitem.getBempatrimonio().setListaMaterialnumeroserie(new ListSet<Materialnumeroserie>(Materialnumeroserie.class, listaMaterialnumeroserie));
				}
			}
		}		
		
		return listagemResult;
	}
	
	public ModelAndView ajaxSerie(WebRequestContext request) throws Exception {
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		List<Materialnumeroserie> listaMaterialnumeroserie = new ArrayList<Materialnumeroserie>();
		
		try {
			Integer cdmaterial = Integer.valueOf(request.getParameter("cdmaterial"));
			listaMaterialnumeroserie = materialnumeroserieService.findListaMaterialnumeroserieByMaterial(new Material(cdmaterial));
		}catch (Exception e) {
		}
		
		jsonModelAndView.addObject("lista", listaMaterialnumeroserie);
		
		return jsonModelAndView;
	}
	
	public ModelAndView buscarPatrimonioItemPlaqueta(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		List<Patrimonioitem> lista = patrimonioitemService.findByPlaqueta(request.getParameter("codigo"));
		String bean = "";
		String plaqueta = "";
		if(lista != null && !lista.isEmpty()){
			if(lista.size() == 1){
				if(Patrimonioitemsituacao.ALOCADO.equals(lista.get(0).getVpatrimonioitem().getSituacao())){
					jsonModelAndView.addObject("alocado", true);
				}if(Patrimonioitemsituacao.PERDIDO.equals(lista.get(0).getVpatrimonioitem().getSituacao())){
					jsonModelAndView.addObject("perdido", true);
				}if(Patrimonioitemsituacao.VENDIDO.equals(lista.get(0).getVpatrimonioitem().getSituacao())){
					jsonModelAndView.addObject("vendido", true);
				}else{
					Patrimonioitem patrimonioitem = lista.get(0);
					bean = "br.com.linkcom.sined.geral.bean.Patrimonioitem[cdpatrimonioitem=" + patrimonioitem.getCdpatrimonioitem() + ",descricao=" + patrimonioitem.getDescricao() + "]";
					plaqueta = patrimonioitem.getPlaqueta();					
				}
			}
		}
		
		if(SinedUtil.isListEmpty(lista)){
			jsonModelAndView.addObject("naoEncontrado", true);
		}else if(lista.size() > 1){
			jsonModelAndView.addObject("unico", false);
		}else {
			jsonModelAndView.addObject("unico", true);
			jsonModelAndView.addObject("bean", bean);
			jsonModelAndView.addObject("plaqueta", plaqueta);
		}
		
		return jsonModelAndView;
	}

}