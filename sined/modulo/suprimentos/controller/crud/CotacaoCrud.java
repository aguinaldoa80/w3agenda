package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritemsolicitacaocompra;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraacao;
import br.com.linkcom.sined.geral.bean.Solicitacaocomprahistorico;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_cotacao;
import br.com.linkcom.sined.geral.service.ColaboradorFornecedorService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.CotacaoService;
import br.com.linkcom.sined.geral.service.CotacaofornecedorService;
import br.com.linkcom.sined.geral.service.CotacaofornecedoritemService;
import br.com.linkcom.sined.geral.service.CotacaofornecedoritemsolicitacaocompraService;
import br.com.linkcom.sined.geral.service.CotacaoorigemService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntregaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.SolicitacaocompraService;
import br.com.linkcom.sined.geral.service.SolicitacaocomprahistoricoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.CotacaoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.CotacaoFornecedorBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.CotacaoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.FluxoSituacaoSolicitacaoProcess;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/suprimento/crud/Cotacao", authorizationModule=CrudAuthorizationModule.class)
public class CotacaoCrud extends CrudControllerSined<CotacaoFiltro, Cotacao, Cotacao>{
	
	private static final String PARAMETRO_MINIMO_FORNECEDORES = "CotacaoNumeroMinimoFornecedor";
	
	private SolicitacaocompraService solicitacaocompraService;
	private ContatoService contatoService;
	private CotacaoService cotacaoService;
	private MaterialService materialService;
	private CotacaofornecedoritemService cotacaofornecedoritemService;
	private ParametrogeralService parametrogeralService;
	private CotacaofornecedorService cotacaofornecedorService;
	private EntregaService entregaService;
	private UsuarioService usuarioService;
	private ColaboradorFornecedorService colaboradorFornecedorService;
	private LocalarmazenagemService localarmazenagemService;
	private SolicitacaocomprahistoricoService solicitacaocomprahistoricoService;
	private CotacaoorigemService cotacaoorigemService;
	private EmpresaService empresaService;
	private CotacaofornecedoritemsolicitacaocompraService cotacaofornecedoritemsolicitacaocompraService;
	private UnidademedidaService unidademedidaService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
		
	public void setCotacaofornecedoritemsolicitacaocompraService(
			CotacaofornecedoritemsolicitacaocompraService cotacaofornecedoritemsolicitacaocompraService) {
		this.cotacaofornecedoritemsolicitacaocompraService = cotacaofornecedoritemsolicitacaocompraService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setCotacaofornecedoritemService(
			CotacaofornecedoritemService cotacaofornecedoritemService) {
		this.cotacaofornecedoritemService = cotacaofornecedoritemService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setSolicitacaocompraService(SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	
	public void setCotacaofornecedorService(CotacaofornecedorService cotacaofornecedorService) {
		this.cotacaofornecedorService = cotacaofornecedorService;
	}
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void setColaboradorFornecedorService (ColaboradorFornecedorService colaboradorFornecedorService){
		this.colaboradorFornecedorService = colaboradorFornecedorService;
	}
	
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setSolicitacaocomprahistoricoService(
			SolicitacaocomprahistoricoService solicitacaocomprahistoricoService) {
		this.solicitacaocomprahistoricoService = solicitacaocomprahistoricoService;
	}
	public void setCotacaoorigemService(CotacaoorigemService cotacaoorigemService) {
		this.cotacaoorigemService = cotacaoorigemService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Cotacao form) throws CrudException {
		if(request.getParameter("flex") != null && request.getParameter("flex").toUpperCase().equals("TRUE"))
			return new ModelAndView("redirect:/suprimento/process/CotacaoFlex?ACAO=editar&cdcotacao="+form.getCdcotacao());
		
		if("true".equals(request.getParameter("isNovoFornecedor"))){
			try {
				return this.novoFornecedor(request, form);
			} catch (Exception e) {
				e.printStackTrace();
				throw new CrudException(e.getMessage(), e);
			}
		}
		return super.doEditar(request, form);
	}
	@Override
	public ModelAndView doListagem(WebRequestContext request, CotacaoFiltro filtro) throws CrudException {
		
		String param = request.getParameter("FROMSOLICITACAO");
		if (param != null && param.equals("true")) {
			filtro.setFromsolicitacao(Boolean.TRUE);		
		} else {
			filtro.setFromsolicitacao(Boolean.FALSE);
		}
		
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request, CotacaoFiltro filtro) throws Exception {
		
		List<Situacaosuprimentos> list = new ArrayList<Situacaosuprimentos>();
			list.add(Situacaosuprimentos.EM_ABERTO);
		
		if (filtro.getFromsolicitacao() == null || !filtro.getFromsolicitacao()) {
			list.add(Situacaosuprimentos.EM_COTACAO);
			list.add(Situacaosuprimentos.ORDEMCOMPRA_GERADA);
			list.add(Situacaosuprimentos.BAIXADA);
			list.add(Situacaosuprimentos.CANCELADA);
		}
		
		request.setAttribute("listaSituacao", list);
	}
	
	@Override
	protected Cotacao criar(WebRequestContext request, Cotacao form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar a a��o criar de Cota��o.");
	}
	
	@Override
	protected ListagemResult<Cotacao> getLista(WebRequestContext request, CotacaoFiltro filtro) {
		
		ListagemResult<Cotacao> listagemResult = super.getLista(request, filtro);
		List<Cotacao> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcotacao", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(cotacaoService.loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));
			
			try{
				String param = parametrogeralService.getValorPorNome(PARAMETRO_MINIMO_FORNECEDORES);
				if(param != null && !param.equals("") && !param.equals("1")){
					Integer paramNum = Integer.parseInt(param);
					for (Cotacao cotacao : list) {
						cotacao.setIsFornecedorMinimo(paramNum > cotacao.getListaCotacaofornecedor().size());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		cotacaoService.formataStringFornecedores(list);
		return listagemResult;
	}

	@Override
	protected Cotacao carregar(WebRequestContext request, Cotacao bean) throws Exception {
		bean = super.carregar(request, bean);
		boolean podeeditar = false;
		boolean podeeditaremprocessocompra = true;
		if(bean.getCdcotacao() != null){
			cotacaoService.setInfOrdemcompragerada(bean);
			boolean todositensgerados = false;
			for(Cotacaofornecedor cf : bean.getListaCotacaofornecedor()){
				todositensgerados = cf.isTodosItensGerados();
				break;
			}
			if(todositensgerados) podeeditaremprocessocompra = false;
		}
		String acao = request.getParameter(MultiActionController.ACTION_PARAMETER);
		if("editar".equals(acao) &&
				!bean.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) && 
				!bean.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO)
		){
			if((!Situacaosuprimentos.ORDEMCOMPRA_GERADA.equals(bean.getAux_cotacao().getSituacaosuprimentos()) && 
					!podeeditaremprocessocompra) || 
					(Situacaosuprimentos.ORDEMCOMPRA_GERADA.equals(bean.getAux_cotacao().getSituacaosuprimentos()) && 
							!podeeditaremprocessocompra))
				throw new SinedException("N�o � poss�vel editar uma cota��o com situa��o diferente de 'EM ABERTO' ou 'EM COTA��O'.");
		}
		
		if(bean.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) || 
		   bean.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO)||
		   ((Situacaosuprimentos.ORDEMCOMPRA_GERADA.equals(bean.getAux_cotacao().getSituacaosuprimentos())) &&
				 podeeditaremprocessocompra)){
			podeeditar = true;
		}
		request.setAttribute("podeeditar", podeeditar);
		request.setAttribute("CASAS_DECIMAIS_ARREDONDAMENTO", SinedUtil.getParametroCasasDecimaisArredondamento());
		return bean;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Cotacao form) throws Exception {

		if (request.getBindException().hasErrors()) {
			if (form.getCdcotacao() == null) {
				request.setAttribute(MultiActionController.ACTION_PARAMETER, CrudControllerSined.CRIAR);
			} else {
				request.setAttribute(MultiActionController.ACTION_PARAMETER, CrudControllerSined.EDITAR);
			}
		}
		
		cotacaoService.setInfo(request, form);
		request.setAttribute("ignoreHackAndroidDownload", true);
	}
	
	/**
	 * Seta as informa��es necess�rias para carregar a tela de entrada.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#setInfo
	 * @param request
	 * @param form
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public void setInfoForCotacao(WebRequestContext request, Cotacao form) throws Exception {
		setInfoForTemplate(request, form);
		setEntradaDefaultInfo(request, form);
		
		cotacaoService.setInfo(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Cotacao form)	throws CrudException {
		if(form.getFromSolicitacao() != null && form.getFromSolicitacao()) {
			super.doSalvar(request, form);
			
			request.clearMessages();
			request.addMessage("Cota��o "+form.getCdcotacao()+" gerada com sucesso.", MessageType.INFO);
			SinedUtil.fechaPopUp(request);
			return null;
		}
		return super.doSalvar(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Cotacao bean) throws Exception {
		super.salvar(request, bean);
	}
	
	/**
	 * Salva a cota��o e verifica se sua situa��o est� 'em cota��o', se estiver � redirecionado
	 * para o crud ordem de compra que gera novas ordens.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#validaQtdCotada(List)
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public ModelAndView gerarOrdemCompra(WebRequestContext request, Cotacao bean) throws Exception {
		Cotacao cotacaoAux = new Cotacao();
		cotacaoAux.setCdcotacao(bean.getCdcotacao());
		cotacaoAux.setListaCotacaofornecedor(bean.getListaCotacaofornecedor());
		
		boolean podeeditaremprocessocompra = true;
		if(bean.getCdcotacao() != null){
			boolean todositensgerados = false;
			for(Cotacaofornecedor cf : bean.getListaCotacaofornecedor()){
				todositensgerados = cf.isTodosItensGerados();
				break;
			}
			if(todositensgerados) podeeditaremprocessocompra = false;
		}
		
		String origemTelamapacotacao = request.getParameter("origemTelamapacotacao");
		if(origemTelamapacotacao != null && "true".equals(origemTelamapacotacao)){
			Cotacao c = cotacaoService.loadWithSituacao(bean);
			if(!c.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO) && 
					(!c.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.ORDEMCOMPRA_GERADA) || 
							!podeeditaremprocessocompra)){
					request.clearMessages();
					request.addError("Registro(s) com situa��o diferente de 'em cota��o'.");
					return new ModelAndView("redirect:/suprimento/process/Mapacotacao?cdcotacao=" + bean.getCdcotacao());
			}
		}
		
		boolean precomaximocompraInvalido = cotacaoService.validaPrecoMaximoCompra(request, cotacaoAux.getListaCotacaofornecedor());
		
		salvar(request, bean);
		bean = cotacaoService.loadForEntrada(bean);
		
		
		if(!bean.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO) && 
				(!bean.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.ORDEMCOMPRA_GERADA) || 
						!podeeditaremprocessocompra)){
			request.clearMessages();
			request.addError("Registro(s) com situa��o diferente de 'em cota��o'.");
		}else if(cotacaoService.validaQtdCotada(cotacaoAux.getListaCotacaofornecedor())){
			request.clearMessages();
			request.addError("Informe a quantidade cotada e o valor cotado (maior que zero).");
		}else if(!precomaximocompraInvalido){
			return new ModelAndView("forward:/suprimento/crud/Ordemcompra?ACAO=gerarOrdemCompra&origemTelamapacotacao=" + (origemTelamapacotacao != null ? origemTelamapacotacao : "false") , "cotacao", cotacaoAux);
		}
		
		if(origemTelamapacotacao != null && "true".equals(origemTelamapacotacao)){
			return new ModelAndView("redirect:/suprimento/process/Mapacotacao?cdcotacao=" + bean.getCdcotacao());
		}
		return doEditar(request, bean);
	}
	
	/**
	 * Carrega a pop-up para a escolha dos Fornecedores
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#getWhereInRequest
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findSolicitacoes(String)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#isSolicitacoesDiferenteDeAutorizada(String)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findForVerificacao
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas, Tom�s Rabelo
	 */
	public ModelAndView escolheFornecedor(WebRequestContext request, CotacaoBean bean){
		if (!request.getBindException().hasErrors()) {
			cotacaoService.getWhereInRequest(request, bean);
			
			//Inst�ncia 3 objetos para j� aparecer 3 campos.
			for (int i = 0; i < 3; i++) {
				bean.getListaFornecedor().add(new CotacaoFornecedorBean());
			}
		}
		
		List<Solicitacaocompra> list = solicitacaocompraService.findSolicitacoes(bean.getIds());
		if(solicitacaocompraService.isSolicitacoesDiferenteDeAutorizada(list, request.getBindException(), FluxoSituacaoSolicitacaoProcess.GERARCOTACAO)){
			SinedUtil.fechaPopUp(request);
			return null;
		} 
		
		boolean existeRestricaoMaterialempresa = materialService.existeRestricaoMaterialempresa(list);
		boolean existEmpresadiferente = solicitacaocompraService.existEmpresadiferente(list); 
		if(existeRestricaoMaterialempresa){
			if(existEmpresadiferente){
				request.addError("N�o � poss�vel gerar a cota��o. As solicita��es de compra possui empresas distintas e os materiais selecionados tem restri��o de empresa.");
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		Empresa empresa = null;
		if(!existEmpresadiferente){
			if(list != null && !list.isEmpty()){
				for(Solicitacaocompra sc : list){
					if(sc.getEmpresa() != null && sc.getEmpresa().getCdpessoa() != null){
						empresa = sc.getEmpresa();
						bean.setEmpresaunicaentrega(empresa);
						break;
					}
				}
			}
		}
		
		if(!SinedUtil.isUsuarioLogadoAdministrador() && SinedUtil.haveGrupoMaterialUsuario()){
			List<Materialgrupo> listaUsuarioMaterialgrupo = SinedUtil.getListaUsuarioMaterialgrupo();
			for (Solicitacaocompra sol : list) {
				if(sol != null && sol.getMaterial() != null && sol.getMaterial().getMaterialgrupo() != null){
					if(!listaUsuarioMaterialgrupo.contains(sol.getMaterial().getMaterialgrupo())){
						request.addError("Este usu�rio logado n�o tem permiss�o para gerar cota��o deste(s) material(is).");
						SinedUtil.fechaPopUp(request);
						return null;
					}
				}
			}	
		}
		
		StringBuilder whereInMaterial = new StringBuilder("");
		for(Solicitacaocompra sol : list){
			if(sol.getMaterial() != null && sol.getMaterial().getCdmaterial() != null){
				if(!whereInMaterial.toString().equals(""))
					whereInMaterial.append(",").append(sol.getMaterial().getCdmaterial());
				else
					whereInMaterial.append(sol.getMaterial().getCdmaterial());
			}
		}
		
		List<Fornecedor> lista3ultimosFornecedores = cotacaofornecedorService.find3FornecedoresMaisUsadosByMaterial(whereInMaterial.toString());
		
		if(lista3ultimosFornecedores != null && !lista3ultimosFornecedores.isEmpty()){
			int i = 0;
			for(Fornecedor fornecedor : lista3ultimosFornecedores){
				if(fornecedor.getCdpessoa() != null && fornecedor.getNome() != null)
					bean.getListaFornecedor().get(i).setFornecedor(fornecedor);
				i++;
			}
		}
		
		request.setAttribute("existEmpresadiferente", existEmpresadiferente);
		request.setAttribute("existeRestricaoMaterialempresa", existeRestricaoMaterialempresa);
		request.setAttribute("bloquearComboEmpresa", empresa != null && list.size() > 1);
		request.setAttribute("listaEmpresa", empresaService.findAtivosByUsuario());
		return new ModelAndView("direct:/crud/popup/escolheFornecedor","cotacaobean",bean);
	}
	
	/**
	 * Gera a cota��o a partir das solicita��es de compra selecionadas na listagem.
	 * Obs: este m�todo foi modificado, agora ele n�o redireciona mais o usu�rio para a tela intermedi�ria de cota��o.
	 * O sistema salva as cota��es direito e redireciona para a tela de solicita��o com os c�digos gerados.
	 * As partes que existiam foram comentadas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findForCotacao
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#verificaMatLocalSoma
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#preencheListaCotacaofornecedor
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#preencheListaCotacaofornecedoritem
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	@Command(validate=true)
	@Input("escolheFornecedor")
	public ModelAndView geraCotacao(WebRequestContext request, CotacaoBean bean) throws Exception{
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			for (CotacaoFornecedorBean cotacaofornecedor : bean.getListaFornecedor()){
				try {
					colaboradorFornecedorService.validaPermissao(cotacaofornecedor.getFornecedor());
				} catch (SinedException e) {
					request.addError(e.getMessage());
					SinedUtil.fechaPopUp(request);
					return null;
				}
			}
		}
		
		List<CotacaoFornecedorBean> listaFornecedor = bean.getListaFornecedor();
				
		List<Solicitacaocompra> listaSolicitacao = solicitacaocompraService.findForCotacao(bean.getIds());
		
		solicitacaocompraService.ajustaQtdeUnidademedida(listaSolicitacao);
		
//		request.getSession().setAttribute("solicitacoesSalvar", listaSolicitacao);
		
		List<Cotacaofornecedoritem> lista = new ArrayList<Cotacaofornecedoritem>();
		Cotacaofornecedoritem mat = null;
		Boolean find = null;
		
		for (Solicitacaocompra solicitacaocompra : listaSolicitacao) {
			mat = new Cotacaofornecedoritem();
			find = false;
			
			if (!parametrogeralService.getBoolean("DESAGRUPAR_ITENS_COTACAO_ORDEMCOMPRA")) {
				find = cotacaoService.verificaMatLocalSoma(lista, find, solicitacaocompra, bean.getLocalunicoentrega(), false);
			}
			
			if (!find) {
				mat.setMaterial(solicitacaocompra.getMaterial());
				mat.setIsServico(materialService.load(solicitacaocompra.getMaterial(),"material.servico").getServico());
				mat.setMaterialclasse(solicitacaocompra.getMaterialclasse());
				mat.setQtdesol(solicitacaocompra.getQtde());
				mat.setLocalarmazenagem(bean.getLocalunicoentrega() != null ? bean.getLocalunicoentrega() : solicitacaocompra.getLocalarmazenagem());
				mat.setLocalarmazenagemsolicitacao(solicitacaocompra.getLocalarmazenagem());
				mat.setFrequencia(solicitacaocompra.getFrequencia());
				mat.setQtdefrequencia(solicitacaocompra.getQtdefrequencia());
				mat.setUnidademedida(solicitacaocompra.getUnidademedida());
				mat.setObservacao(solicitacaocompra.getObservacao());
				
				Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra = new ListSet<Cotacaofornecedoritemsolicitacaocompra>(Cotacaofornecedoritemsolicitacaocompra.class);
				Cotacaofornecedoritemsolicitacaocompra cotacaofornecedoritemsolicitacaocompra = new Cotacaofornecedoritemsolicitacaocompra();
				cotacaofornecedoritemsolicitacaocompra.setSolicitacaocompra(solicitacaocompra);
				listaCotacaofornecedoritemsolicitacaocompra.add(cotacaofornecedoritemsolicitacaocompra);
				
				mat.setListaCotacaofornecedoritemsolicitacaocompra(listaCotacaofornecedoritemsolicitacaocompra);
				
				lista.add(mat);
			}
			
			solicitacaocompra.setSolicitacaocomprahistorico(new Solicitacaocomprahistorico(solicitacaocompra, Solicitacaocompraacao.EM_PROCESSO_COMPRA));
		}
		
		List<Cotacaofornecedor> listaForn = cotacaoService.preencheListaCotacaofornecedor(listaFornecedor);
		
		//Esta parte foi modificada para voltar para tela de solicita��o
		Cotacao cotacao = new Cotacao();
		cotacao.setListaCotacaofornecedor(listaForn);
		cotacao.setListaSolicitacao(listaSolicitacao);
		cotacao.setFromSolicitacao(Boolean.TRUE);
		if(bean.getLocalunicoentrega() != null){
			cotacao.setConsiderarlocalunicoentrega(true);
		}
		cotacao.setEmpresa(bean.getEmpresaunicaentrega());
		
		cotacaoService.preencheListaCotacaofornecedoritem(cotacao, lista);
		/*
		request.getSession().setAttribute("cotacaoSolicitacao", listaForn);
		request.getSession().setAttribute("listaMaterialSessao", lista);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.location = \""+request.getServletRequest().getContextPath()+"/suprimento/crud/Cotacao?ACAO=exibirCotacao\"; " +
				"window.close();</script>");
		*/
		return doSalvar(request, cotacao);
	}
	
	

	
	/**
	 * Este m�todo esta comentado pois, o fluxo agora n�o vai mais para uma tela intermediaria.
	 * Ap�s gerar cota��o na tela de solicita��o o sistema salva e retorna para a tela de solicita��o de compra
	 * 
	 * Carrega a tela de entrada vindo da a��o de escolher os fornecedores para a cota��o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#zeraAtributosSessao
	 * @see br.com.linkcom.sined.modulo.suprimentos.controller.crud.CotacaoCrud#setInfoForCotacao
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
/*	@SuppressWarnings("unchecked")
	public ModelAndView exibirCotacao(WebRequestContext request) throws Exception{
		Cotacao form = new Cotacao();
		
		form.setListaCotacaofornecedor((List<Cotacaofornecedor>)request.getSession().getAttribute("cotacaoSolicitacao"));
		request.setAttribute("listaMaterial", request.getSession().getAttribute("listaMaterialSessao"));
		cotacaoService.zeraAtributosSessao(request);
		
		form.setFromSolicitacao(true);
		
		this.setInfoForCotacao(request, form);		
		
		return getEntradaModelAndView(request, form);
	}
	*/
	
	
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		if (acao != null && acao.equals("geraCotacao")) {
			CotacaoBean bean = (CotacaoBean) obj;
			List<CotacaoFornecedorBean> lista = bean.getListaFornecedor();
			
			cotacaoService.verificaUmFornecedor(errors, lista);
			cotacaoService.verificaFornecedorIgual(errors, lista);
		}
		
		super.validate(obj, errors, acao);
	}
	
	
	/**
	 * Popula o combo de box de contato, via AJAX.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @author Tom�s Rabelo
	 */
	public ModelAndView comboBox(WebRequestContext request, Cotacaofornecedor cotacaofornecedor) {
		Fornecedor fornecedor = cotacaofornecedor.getFornecedor();
		List<Contato> listaContato = contatoService.findByPessoaCombo(fornecedor);
		String listaCodigos = CollectionsUtil.listAndConcatenate(listaContato, "cdpessoa",",");
		String listaNomes = CollectionsUtil.listAndConcatenate(listaContato, "nome",",");
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("codigos", listaCodigos);
		json.addObject("nomes", listaNomes);
		return json;
	}
	
	/**
	 * Simula a a��o de excluir uma linha da lista de cota��es.
	 * 
	 * @see br.com.linkcom.sined.modulo.suprimentos.controller.crud.CotacaoCrud#setInfoForCotacao 
	 * @param request
	 * @param cotacao
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public ModelAndView excluiFornecedor(WebRequestContext request, Cotacao cotacao) throws Exception {
		
		Integer integer = Integer.parseInt(request.getParameter("indexFornecedor"));
		List<Cotacaofornecedor> lista = cotacao.getListaCotacaofornecedor();
		lista.remove(integer.intValue());
		cotacao.setListaCotacaofornecedor(lista);
		
		this.setInfoForCotacao(request, cotacao);
		
		return getEntradaModelAndView(request, cotacao);
	}
	
	/**
	 * M�todo que simula a a��o ded nova linha com um novo fornecedor na lista de cota��es.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#getListaDefault
	 * @see br.com.linkcom.sined.modulo.suprimentos.controller#crud.CotacaoCrud.setInfoForCotacao
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#setaListaParaDefault
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#adicionaFornecedor
	 * @param request
	 * @param cotacao
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	//adiciona fornecedor
	public ModelAndView novoFornecedor(WebRequestContext request, Cotacao cotacao) throws Exception{
		
		List<Cotacaofornecedor> lista = cotacao.getListaCotacaofornecedor();
		List<Cotacaofornecedoritem> listaDefault = cotacaoService.getListaDefault(lista);
		
		for (Cotacaofornecedor cotacaofornecedor : lista) {
			if (cotacaofornecedor.getFornecedor().getCdpessoa().equals(cotacao.getFornecedor().getCdpessoa())) {
				request.addError("Fornecedor j� presente na cota��o.");
				this.setInfoForCotacao(request, cotacao);
				cotacao.setFornecedor(null);
				return getEntradaModelAndView(request, cotacao);
			}
		}
		
		cotacaoService.adicionaFornecedor(cotacao, lista, listaDefault);
		
		this.setInfoForCotacao(request, cotacao);
		cotacao.setFornecedor(null);
		
		return getEntradaModelAndView(request, cotacao);
	}
	
	
	
	/**
	 * M�todo que simula a a��o de incluir um novo material em todas as 
	 * listas de materiais sendo cotados na tela.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#adicionaFornMat
	 * @see br.com.linkcom.sined.modulo.suprimentos.controller.crud.CotacaoCrud#setInfoForCotacao
	 * @param request
	 * @param cotacao
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public ModelAndView novoMaterial(WebRequestContext request, Cotacao cotacao) throws Exception{
		
		Cotacaofornecedoritem cotacaofornecedoritem = new Cotacaofornecedoritem();
		cotacaofornecedoritem.setMaterial(cotacao.getMaterial());
		cotacaofornecedoritem.setQtdesol(cotacao.getQtdesol());
		cotacaofornecedoritem.setLocalarmazenagem(cotacao.getLocalarmazenagem());
	
		cotacaoService.adicionaFornMat(cotacao, cotacaofornecedoritem);
		this.setInfoForCotacao(request, cotacao);
		
		cotacao.setMaterial(null);
		cotacao.setQtdesol(null);
		cotacao.setLocalarmazenagem(null);
		
		return getEntradaModelAndView(request, cotacao);
	}
	
	
	
	@Override
	protected void validateBean(Cotacao bean, BindException errors) {
		
		List<Cotacaofornecedor> listaCotacaofornecedor = bean.getListaCotacaofornecedor();
		List<Fornecedor> listaFornecedor = new ListSet<Fornecedor>(Fornecedor.class);
		for (Cotacaofornecedor cotacaofornecedor : listaCotacaofornecedor) {
			listaFornecedor.add(cotacaofornecedor.getFornecedor());
		}
		cotacaoService.verificaFornecedores(errors, listaFornecedor);
	}
	
	/**
	 * M�todo que carrega o email de um determinado contato
	 * 
	 * @param request
	 * @param contato
	 * @return
	 * @author Tom�s Rabelo
	 */
	public void atualizaEmailContato(WebRequestContext request, Contato contato) {
		contato = contatoService.getEmailContato(contato);
		String emailcontato = "var emailcontato = ";
		if(contato != null){
			emailcontato += contato.getEmailcontato() == null ? "'';" : "'"+contato.getEmailcontato()+"';";
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(emailcontato);
	}
	
	public void ajaxBuscaValorAnterior(WebRequestContext request){
		Integer cdmaterial = Integer.parseInt(request.getParameter("cdmaterial"));
		Integer cdfornecedor = Integer.parseInt(request.getParameter("cdfornecedor"));
		
		Double valor = cotacaofornecedoritemService.getUltimoValor(cdmaterial, cdfornecedor);
		
		if(valor != null){
			View.getCurrent().println("var sucesso = true;");
			View.getCurrent().println("var valorunitario = '" + valor.toString().replaceAll("\\.", ",") + "'");
		} else {
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	/**
	 * Action que cria o CSV da listagem de Solicita��o de compra.
	 * 
	 * @see r.com.linkcom.sined.geral.service.SolicitacaocompraService#findForCsv
	 *
	 * @param request
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView gerarCsv(WebRequestContext request, CotacaoFiltro filtro){
		
		List<Cotacao> listaId = cotacaoService.findForCsv(filtro);
		StringBuilder rel = new StringBuilder();
		
		String whereIn = CollectionsUtil.listAndConcatenate(listaId, "cdcotacao", ",");
		List<Cotacao> lista = cotacaoService.loadWithLista(whereIn, "", false);
		
		rel.append("Cota��o;");
		rel.append("Fornecedores;");
		rel.append("Materiais e servi�os;");
		rel.append("Locais de entrega;");
		rel.append("Data limite;");
		rel.append("Situa��o;");
		rel.append("\n");
		
		cotacaoService.formataStringFornecedores(lista);
		
		for (Cotacao c : lista) {
			rel.append(c.getCdcotacao());
			rel.append(";");
			
			if(c.getAux_cotacao() != null)
				rel.append(c.getAux_cotacao().getFornecedoresForCsv());
			rel.append(";");
			
			rel.append(c.getMateriaisForCsv());
			rel.append(";");
			
			rel.append(c.getLocaisForCsv());
			rel.append(";");
			
			if(c.getAux_cotacao() != null && c.getAux_cotacao().getDtlimite() != null)
				rel.append(SinedDateUtils.toString(c.getAux_cotacao().getDtlimite()));
			rel.append(";");
			
			if(c.getAux_cotacao() != null)
				rel.append(c.getAux_cotacao().getSituacaosuprimentos().getDescricao());
			rel.append(";");
			
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","cotacao_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	public ModelAndView popUpOutrosDados(WebRequestContext request, Cotacaofornecedoritem cotacaofornecedoritem){
		
		String habilitado = request.getParameter("habilitado");
		String consultar = request.getParameter("consultar");
		boolean consultarBool = false;
		if("false".equals(habilitado) || "true".equals(consultar)){
			consultarBool = true;
		}
		request.setAttribute("consultar", consultarBool);
		
		if(cotacaofornecedoritem.getLocalarmazenagem() != null){
			Localarmazenagem localarmazenagem = localarmazenagemService.load(cotacaofornecedoritem.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome");
			if(localarmazenagem != null){
				cotacaofornecedoritem.getLocalarmazenagem().setNome(localarmazenagem.getNome());
			}
		}
		
		return new ModelAndView("direct:/crud/popup/outrosDadosCotacao", "cotacaofornecedoritem", cotacaofornecedoritem);
	}
	
	public ModelAndView abrirPopupUltimascompras(WebRequestContext request){
		return entregaService.abrirPopupUltimascompras(request);	
	}
	
	/**
	 * M�toque que abre uma popup para exclus�o de materiais de uma cota��o
	 * @param request
	 * @param cotacao
	 * @return
	 */
	public ModelAndView openPopupExcluirMaterial(WebRequestContext request, Cotacao cotacao){
		String cdcotacao = request.getParameter("selectedItens");
		if (!StringUtils.isBlank(cdcotacao)) {
			cotacao = new Cotacao(Integer.parseInt(cdcotacao));
		}
		Aux_cotacao aux_cotacao = cotacaoService.loadWithSituacao(cotacao).getAux_cotacao();
		
		if(!aux_cotacao.getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) &&
		   !aux_cotacao.getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO)){
			request.addError("Cota��o com situa��o diferente de \"Em aberto\" e \"Em cota��o\".");
			SinedUtil.fechaPopUp(request);
			return null;
		} else {
			Cotacao c = cotacaoService.loadWithListaCotacaoFornecedor(cotacao);
			ListSet<Material> listaMaterial = new ListSet<Material>(Material.class);
			if(c.getListaCotacaofornecedor() != null && !c.getListaCotacaofornecedor().isEmpty() && c.getListaCotacaofornecedor().get(0).getListaCotacaofornecedoritem() != null){
				for(Cotacaofornecedoritem cfi : c.getListaCotacaofornecedor().get(0).getListaCotacaofornecedoritem()){
					if (cfi.getMaterial() != null) {
						List<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra = cotacaofornecedoritemsolicitacaocompraService.findByCotacaofornecedoritem(cfi);
						String whereInSolicitacaocompra = SinedUtil.listAndConcatenateSort(listaCotacaofornecedoritemsolicitacaocompra, "solicitacaocompra.cdsolicitacaocompra", ",");
						
						Material material = new Material();
						material.setCdmaterial(cfi.getMaterial().getCdmaterial());
						material.setIdentificacao(cfi.getMaterial().getIdentificacao());
						material.setNome(cfi.getMaterial().getNome());
						material.setWhereInSolicitacaocompra(whereInSolicitacaocompra);
						
						listaMaterial.add(material);
					}
				}
			}
			
			Collections.sort(listaMaterial,new Comparator<Material>(){
				public int compare(Material o1, Material o2) {
					return o1.getCdmaterial().compareTo(o2.getCdmaterial());
				}
			});
			
			c.setListaMateriais(listaMaterial);
			return new ModelAndView("direct:/crud/popup/popupRetirarMaterialCotacao", "cotacao", c);
		}
	}
	
	/**
	 * M�todo que atualiza a situa��o de compra quando algum material � exclu�do de uma cota��o
	 * @param request
	 * @param cotacao
	 * @return
	 */
	public ModelAndView atualizaSituacaoSolicitacaoCompra(WebRequestContext request, Cotacao cotacao){
		String whereInCdMaterial = request.getParameter("whereInCdMaterial");
		String whereInCdSolicitacao = request.getParameter("whereInCdSolicitacao");
		List<Material> listaMaterial = materialService.findForSolicitacaoCompraFromGerenciamentomaterial(whereInCdMaterial);
		
		cotacao = cotacaoService.loadWithListaCotacaoFornecedor(cotacao);
		
		if(cotacao.getListaCotacaofornecedor() != null && !cotacao.getListaCotacaofornecedor().isEmpty()){
			for(Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()){
				if(cf.getListaCotacaofornecedoritem() != null && !cf.getListaCotacaofornecedoritem().isEmpty()){
					for(Cotacaofornecedoritem ci : cf.getListaCotacaofornecedoritem()){
						if(listaMaterial.contains(ci.getMaterial())){
							cotacaoService.deleteListaMaterialNaoCotado(ci.getCdcotacaofornecedoritem().toString());
						}
					}
				}
			}
		}
		
		cotacaoService.updateSituacaoSolicitacaoCompra(whereInCdSolicitacao);
		cotacaoorigemService.deleteCotacaoOrigem(whereInCdSolicitacao,cotacao.getCdcotacao());

		for(Integer cdsolicitacaocompra: SinedUtil.getListaByIds(whereInCdSolicitacao)){
			Solicitacaocomprahistorico solicitacaocomprahistorico = new Solicitacaocomprahistorico();
			solicitacaocomprahistorico.setSolicitacaocompra(new Solicitacaocompra(cdsolicitacaocompra));
			solicitacaocomprahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			solicitacaocomprahistorico.setDtaltera(SinedDateUtils.currentTimestamp());
			solicitacaocomprahistorico.setSolicitacaocompraacao(Solicitacaocompraacao.EXCLUSAO_COTACAO);			
			solicitacaocomprahistorico.setObservacao("Item removido da cota��o <a href='javascript:redirecionaCotacao(" + cotacao.getCdcotacao() + ");'>" + cotacao.getCdcotacao() + "</a>");
			solicitacaocomprahistoricoService.saveOrUpdate(solicitacaocomprahistorico);
		}
		
		return new ModelAndView("redirect:/suprimento/crud/Cotacao?ACAO=consultar&cdcotacao="+cotacao.getCdcotacao());
	}
	
	/**
	 * M�todo que atualiza a situa��o de compra quando algum material � exclu�do de uma cota��o, atrav�s de um popup
	 * @param request
	 * @param cotacao
	 * @return
	 */
	public void atualizaSituacaoSolicitacaoCompraFromPopup(WebRequestContext request, Cotacao cotacao){
		List<Material> listaMaterial = new ArrayList<Material>();
		for(Material item : cotacao.getListaMateriais()){
			if(item.getChecado() != null && item.getChecado()){
				listaMaterial.add(item);
			}
		}
		
		if(listaMaterial.size() >= cotacao.getListaMateriais().size()){
			request.addMessage("N�o � permitido a exclus�o de todos os itens de uma cota��o.", MessageType.ERROR);
		} else {
			String whereInCdSolicitacao = SinedUtil.listAndConcatenateSort(listaMaterial, "whereInSolicitacaocompra", ",");
			
			cotacao = cotacaoService.loadWithListaCotacaoFornecedor(cotacao);
			
			if(cotacao.getListaCotacaofornecedor() != null && !cotacao.getListaCotacaofornecedor().isEmpty()){
				for(Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()){
					if(cf.getListaCotacaofornecedoritem() != null && !cf.getListaCotacaofornecedoritem().isEmpty()){
						for(Cotacaofornecedoritem cfi : cf.getListaCotacaofornecedoritem()){
							List<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra = cotacaofornecedoritemsolicitacaocompraService.findByCotacaofornecedoritem(cfi);
							String whereInSolicitacaocompra = SinedUtil.listAndConcatenateSort(listaCotacaofornecedoritemsolicitacaocompra, "solicitacaocompra.cdsolicitacaocompra", ",");
							for(Material mat : listaMaterial){
								if(mat.equals(cfi.getMaterial()) && mat.getWhereInSolicitacaocompra().equals(whereInSolicitacaocompra)){
									cotacaoService.deleteListaMaterialNaoCotado(cfi.getCdcotacaofornecedoritem().toString());
								}
							}
						}
					}	
				}
			}
			
			cotacaoService.updateSituacaoSolicitacaoCompra(whereInCdSolicitacao);
			cotacaoorigemService.deleteCotacaoOrigem(whereInCdSolicitacao, cotacao.getCdcotacao());
			
			for(Integer cdsolicitacaocompra: SinedUtil.getListaByIds(whereInCdSolicitacao)){
				Solicitacaocomprahistorico solicitacaocomprahistorico = new Solicitacaocomprahistorico();
				solicitacaocomprahistorico.setSolicitacaocompra(new Solicitacaocompra(cdsolicitacaocompra));
				solicitacaocomprahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				solicitacaocomprahistorico.setDtaltera(SinedDateUtils.currentTimestamp());
				solicitacaocomprahistorico.setSolicitacaocompraacao(Solicitacaocompraacao.EXCLUSAO_COTACAO);			
				solicitacaocomprahistorico.setObservacao("Item removido da cota��o <a href='javascript:redirecionaCotacao(" + cotacao.getCdcotacao() + ");'>" + cotacao.getCdcotacao() + "</a>");
				solicitacaocomprahistoricoService.saveOrUpdate(solicitacaocomprahistorico);
			}
			
			request.addMessage("Iten(s) removido(s) com sucesso.");
		}

		SinedUtil.fechaPopUp(request);
	}
	
	public ModelAndView converteUnidademedida(WebRequestContext request, Cotacaofornecedoritem cotacaofornecedoritem){
		Double qtdesol = cotacaofornecedoritem.getQtdesol();
		Unidademedida unidademedida = cotacaofornecedoritem.getUnidademedida();
		Unidademedida unidademedidaAntiga = cotacaofornecedoritem.getUnidademedidatrans();
		
		Material material = materialService.findListaMaterialunidademedida(cotacaofornecedoritem.getMaterial());
		
		Double fracao1 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedidaAntiga);
		Double fracao2 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedida);
		
		if(fracao1 != null && fracao2 != null){
			qtdesol = (qtdesol * fracao1) / fracao2;
		} else {
			Double fracao = null;
			
			List<Unidademedidaconversao> listaUnidademedidaconversao = unidademedidaconversaoService.conversoesByUnidademedida(unidademedida);		
			if(listaUnidademedidaconversao != null && listaUnidademedidaconversao.size() > 0){
				for (Unidademedidaconversao item : listaUnidademedidaconversao) {
					if(item.getUnidademedida() != null && 
						item.getUnidademedida().getCdunidademedida() != null && 
						item.getFracao() != null){
						
						if(item.getUnidademedidarelacionada().equals(unidademedida)){
							fracao = item.getFracaoQtdereferencia();
							break;
						}
					}
				}
			}
		
			if(fracao != null && fracao > 0){
				qtdesol = qtdesol / fracao;
			}
		}
		
		return new JsonModelAndView().addObject("qtdesol", SinedUtil.descriptionDecimal(qtdesol));
	}
}
