package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MovpatrimonioService;
import br.com.linkcom.sined.geral.service.PatrimonioitemService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovpatrimonioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/suprimento/crud/Movpatrimonio", "/patrimonio/crud/Movpatrimonio"}, authorizationModule=CrudAuthorizationModule.class)
public class MovpatrimonioCrud extends CrudControllerSined<MovpatrimonioFiltro, Movpatrimonio, Movpatrimonio>{
	
	private PatrimonioitemService patrimonioitemService;
	private LocalarmazenagemService localarmazenagemService;
	private MovpatrimonioService movpatrimonioService;
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	private EmpresaService empresaService;
	private MaterialService materialService;
	private ProjetoService projetoService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setMovpatrimonioService(MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}	
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setPatrimonioitemService(PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	@Override
	protected Movpatrimonio criar(WebRequestContext request, Movpatrimonio form) throws Exception {
		Movpatrimonio mov = super.criar(request, form);
		mov.setDtmovimentacao(new Timestamp(System.currentTimeMillis()));
		return mov;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, MovpatrimonioFiltro filtro) throws CrudException {
		String cdpatrimonioitem = request.getParameter("cdpatrimonioitem");
		if(cdpatrimonioitem != null){
			Patrimonioitem patrimonioitem = new Patrimonioitem();
			patrimonioitem.setCdpatrimonioitem(Integer.parseInt(cdpatrimonioitem));
			patrimonioitem = patrimonioitemService.loadWithPlaqueta(patrimonioitem);
			filtro.setPatrimonioitem(patrimonioitem);
			filtro.setPlaqueta(patrimonioitem.getPlaqueta());
		}else if(filtro.getPatrimonioitem() != null && filtro.getPatrimonioitem().getCdpatrimonioitem() != null){
			filtro.setPatrimonioitem(patrimonioitemService.loadWithPlaqueta(filtro.getPatrimonioitem()));
			filtro.setPlaqueta(filtro.getPatrimonioitem().getPlaqueta());
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request, MovpatrimonioFiltro filtro) throws Exception {
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		request.setAttribute("listaTipo", materialtipoService.findAtivos());
		request.setAttribute("textoPlaquetaSerie", patrimonioitemService.getTextoPlaquetaSerie());
	}
	
	@Override
	protected void entrada(WebRequestContext request, Movpatrimonio form) throws Exception {
		if(form.getCdmovpatrimonio() == null){
			Movpatrimonioorigem movpatrimonioorigem = new Movpatrimonioorigem();
			movpatrimonioorigem.setUsuario((Usuario)Neo.getUser());
			form.setMovpatrimonioorigem(movpatrimonioorigem);	
		}
		
		List<Projeto> listaProjeto = projetoService.findForComboByUsuariologadoNotCancelado();
		if(form.getProjeto() != null){
			if(listaProjeto == null)
				listaProjeto = new ArrayList<Projeto>();
			
			if(!listaProjeto.contains(form.getProjeto())){
				listaProjeto.add(form.getProjeto());
			}
		}
		
//		if ("TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM))){
//			if (form.getPatrimonioitem()!=null && form.getPatrimonioitem().getBempatrimonio()!=null){
//				form.getPatrimonioitem().getBempatrimonio().setListaMaterialnumeroserie(new ListSet<Materialnumeroserie>(Materialnumeroserie.class, materialnumeroserieService.findByCdsmaterial(form.getPatrimonioitem().getBempatrimonio().getCdmaterial().toString())));
//			}
//		}
		
		request.setAttribute("listaProjetos", listaProjeto);
		request.setAttribute("listaOrigem", movpatrimonioService.montaOrigem(form));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("textoPlaquetaSerie", patrimonioitemService.getTextoPlaquetaSerie());
	}
	
	@Override
	protected void salvar(WebRequestContext request, Movpatrimonio bean) throws Exception {
		super.salvar(request, bean);
	}
	
	@Override
	protected void validateBean(Movpatrimonio bean, BindException errors) {
		Movpatrimonio movpatrimonio = movpatrimonioService.findLastMov(bean);
		if(movpatrimonio != null){
			if(bean.getCdmovpatrimonio() == null){
				bean.getPatrimonioitem().setBempatrimonio(movpatrimonio.getPatrimonioitem().getBempatrimonio());
			}
			if(bean.getDtmovimentacao().before(movpatrimonio.getDtmovimentacao()))
				errors.reject("001", "A data da movimenta��o n�o pode ser menor que a data da �ltima movimenta��o deste patrim�nio.");
		}
	}
	
	@Override
	protected Movpatrimonio carregar(WebRequestContext request, Movpatrimonio bean) throws Exception {
		
		Movpatrimonio carregar = super.carregar(request, bean);
		
		if(carregar == null){
			throw new SinedException("Registro n�o encontrado no sistema");
		}
		
		String acao = request.getParameter(MultiActionController.ACTION_PARAMETER);
		if("consultar".equals(acao) && !movpatrimonioService.isLastMov(carregar)){
			request.setAttribute("isLastMov", Boolean.FALSE);
		} else if("editar".equals(acao) && !movpatrimonioService.isLastMov(carregar)){
			throw new SinedException("S� � poss�vel editar a �ltima movimenta��o de um item de patrim�nio.");
		} else{
			request.setAttribute("isLastMov", Boolean.TRUE);
		}
		
		return carregar;
	}
	
	
	/**
	 * M�todo para carregar a plaqueta via AJAX para exibi��o na tela de cadastro da movimenta��o de patrim�nio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#loadWithPlaqueta
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxPlaqueta(WebRequestContext request, Movpatrimonio bean){
		
		if (bean == null || bean.getPatrimonioitem() == null || bean.getPatrimonioitem().getCdpatrimonioitem() == null) {
			throw new SinedException("Patrim�nio item n�o pode ser nulo.");
		}		
		
		Patrimonioitem patrimonioitem = patrimonioitemService.loadWithPlaqueta(bean.getPatrimonioitem());
		request.getServletResponse().setContentType("text/html");
		if (patrimonioitem == null || patrimonioitem.getPlaquetaSerie() == null) {
			View.getCurrent().eval("");
		} else {
			View.getCurrent().eval(patrimonioitem.getPlaquetaSerie());
		}
	}
	
	
	/**
	 * Carrega a �ltima movimenta��o do item de patrim�nio, para o carregamento da tela.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioService#findLastMov
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxOrigem(WebRequestContext request, Movpatrimonio bean){
		
		if (bean == null || bean.getPatrimonioitem() == null || bean.getPatrimonioitem().getCdpatrimonioitem() == null) {
			throw new SinedException("Patrim�nio item n�o pode ser nulo.");
		}
		
		Movpatrimonio movpatrimonio = movpatrimonioService.findLastMov(bean);
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("fornecedor", movpatrimonio != null && movpatrimonio.getFornecedor() != null ? movpatrimonio.getFornecedor().getCdpessoa() : null);
		json.addObject("departamento", movpatrimonio != null && movpatrimonio.getDepartamento() != null ? movpatrimonio.getDepartamento().getCddepartamento() : null);
		json.addObject("empresa", movpatrimonio != null && movpatrimonio.getEmpresa() != null ? movpatrimonio.getEmpresa().getCdpessoa() : null);
		json.addObject("localarmazenagem", movpatrimonio != null && movpatrimonio.getLocalarmazenagem() != null ? movpatrimonio.getLocalarmazenagem().getCdlocalarmazenagem() : null);
		json.addObject("colaborador", movpatrimonio != null && movpatrimonio.getColaborador() != null ? movpatrimonio.getColaborador().getCdpessoa() : null);
		return json;
	}
	
	/**
	 * M�todo que faz um cancelamento de uma movimenta��o de patrim�nio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioService#isLastMov
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioService#updateCancelada
	 * @param request
	 * @param movpatrimonio
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Input(LISTAGEM)
	public ModelAndView cancelar(WebRequestContext request,Movpatrimonio movpatrimonio){
		
		if (movpatrimonio == null || movpatrimonio.getCdmovpatrimonio() == null) {
			throw new SinedException("Movimenta��o n�o pode ser nulo.");
		}
		if(movpatrimonioService.existMovPatrimonioRomaneio(movpatrimonio)){
			request.addError("N�o � poss�vel cancelar uma movimenta��o vinculada a um romaneio.");
			return new ModelAndView("redirect:/suprimento/crud/Movpatrimonio");
		}
		Boolean islast = movpatrimonioService.isLastMov(movpatrimonio);
		
		if (!islast) {
			request.addError("Cancelamento n�o permitido. Somente a �ltima movimenta��o do patrim�nio pode ser cancelado.");
			return new ModelAndView("redirect:/suprimento/crud/Movpatrimonio");	
		}
		movpatrimonioService.updateCancelada(movpatrimonio);
		request.addMessage("Movimenta��o cancelada com sucesso.");
		
		return new ModelAndView("redirect:/suprimento/crud/Movpatrimonio");		
	}
	
	/**
	 * M�todo chamado a partir da tela gerenciamento material. Inst�ncia um novo filtro com novos valores
	 * e coloca na se��o
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @throws CrudException
	 * @author Tom�s Rabelo
	 */
	public ModelAndView preparaFiltroFromGerenciamentoMaterial(WebRequestContext request, MovpatrimonioFiltro filtro) throws CrudException{		
		filtro.setLocalarmazenagem(Integer.parseInt(request.getParameter("cdlocal")));
		if(request.getParameter("bempatrimonio.cdmaterial") != null){
			filtro.setBempatrimonio(materialService.load(new Material(Integer.parseInt(request.getParameter("bempatrimonio.cdmaterial"))), "material.nome").getNome());
		}
		filtro.setDestino(localarmazenagemService.load(new Localarmazenagem(filtro.getLocalarmazenagem()),"localarmazenagem.nome").getNome());
		request.getSession().setAttribute(this.getClass().getSimpleName()+"CONTROLLER"+filtro.getClass().getName(), filtro);
		return continueOnAction("listagem", filtro);
	}

	/**
	 * Carrega a �ltima movimenta��o do item de patrim�nio, para o carregamento da tela.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxOrigemMov(WebRequestContext request, Movpatrimonio bean){
		
		if (bean == null || bean.getPatrimonioitem() == null || bean.getPatrimonioitem().getCdpatrimonioitem() == null) {
			throw new SinedException("Patrim�nio item n�o pode ser nulo.");
		}
		
		Movpatrimonio movpatrimonio = movpatrimonioService.findLastMov(bean);
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("fornecedor", movpatrimonio != null && movpatrimonio.getFornecedor() != null ? 
				"br.com.linkcom.sined.geral.bean.Fornecedor[cdpessoa=" + movpatrimonio.getFornecedor().getCdpessoa() + ",nome=" + movpatrimonio.getFornecedor().getNome() + "]" : null);
		json.addObject("cliente", movpatrimonio != null && movpatrimonio.getCliente() != null ? 
				"br.com.linkcom.sined.geral.bean.Cliente[cdpessoa=" + movpatrimonio.getCliente().getCdpessoa() + ",nome=" + movpatrimonio.getCliente().getNome() + "]" : null);
		json.addObject("departamento", movpatrimonio != null && movpatrimonio.getDepartamento() != null ? 
				"br.com.linkcom.sined.geral.bean.Departamento[cddepartamento=" + movpatrimonio.getDepartamento().getCddepartamento() + ",nome=" + movpatrimonio.getDepartamento().getNome() + "]" : null);
		json.addObject("empresa", movpatrimonio != null && movpatrimonio.getEmpresa() != null ? movpatrimonio.getEmpresa().getCdpessoa() : null);
		json.addObject("localarmazenagem", movpatrimonio != null && movpatrimonio.getLocalarmazenagem() != null ? movpatrimonio.getLocalarmazenagem().getCdlocalarmazenagem() : null);
		json.addObject("colaborador", movpatrimonio != null && movpatrimonio.getColaborador() != null ? 
				"br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=" + movpatrimonio.getColaborador().getCdpessoa() + ",nome=" + movpatrimonio.getColaborador().getNome() + "]" : null);
		json.addObject("projeto", movpatrimonio != null && movpatrimonio.getProjeto() != null ? movpatrimonio.getProjeto().getCdprojeto() : null);
		return json;
	}
	
	public ModelAndView ajaxInfoPatrimonio(WebRequestContext request, Movpatrimonio mov){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(mov.getPatrimonioitem() != null && mov.getPatrimonioitem().getCdpatrimonioitem() != null){
			Patrimonioitem patrimonioitem = patrimonioitemService.load(mov.getPatrimonioitem(), "patrimonioitem.cdpatrimonioitem,patrimonioitem.plaqueta");
			jsonModelAndView.addObject("patrimonioitem",  patrimonioitem);
		}
		return jsonModelAndView;
	}
	
//	@Override
//	protected ListagemResult<Movpatrimonio> getLista(WebRequestContext request, MovpatrimonioFiltro filtro) {
//		ListagemResult<Movpatrimonio> listagemResult = super.getLista(request, filtro);
//		
//		if ("TRUE".equalsIgnoreCase(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.RECAPAGEM))){
//			List<Movpatrimonio> listaPatrimonioitem = listagemResult.list(); 
//			Map<Integer, List<Materialnumeroserie>> mapa = new HashMap<Integer, List<Materialnumeroserie>>();
//			String whereIn = "-1";
//			
//			for (Movpatrimonio movpatrimonio: listaPatrimonioitem){
//				if (movpatrimonio.getPatrimonioitem()!=null &&  movpatrimonio.getPatrimonioitem().getBempatrimonio()!=null){
//					whereIn += ", " +movpatrimonio.getPatrimonioitem().getBempatrimonio().getCdmaterial();
//				}
//			}
//			
//			for (Materialnumeroserie materialnumeroserie: materialnumeroserieService.findByCdsmaterial(whereIn)){
//				List<Materialnumeroserie> listaMaterialnumeroserie = mapa.get(materialnumeroserie.getMaterial().getCdmaterial());
//				if (listaMaterialnumeroserie==null){
//					listaMaterialnumeroserie = new ArrayList<Materialnumeroserie>();
//				}
//				listaMaterialnumeroserie.add(materialnumeroserie);
//				mapa.put(materialnumeroserie.getMaterial().getCdmaterial(), listaMaterialnumeroserie);
//			}			
//			
//			for (Movpatrimonio movpatrimonio: listaPatrimonioitem){
//				if (movpatrimonio.getPatrimonioitem()!=null &&  movpatrimonio.getPatrimonioitem().getBempatrimonio()!=null){
//					List<Materialnumeroserie> listaMaterialnumeroserie = mapa.get(movpatrimonio.getPatrimonioitem().getBempatrimonio().getCdmaterial());
//					if (listaMaterialnumeroserie!=null){
//						movpatrimonio.getPatrimonioitem().getBempatrimonio().setListaMaterialnumeroserie(new ListSet<Materialnumeroserie>(Materialnumeroserie.class, listaMaterialnumeroserie));
//					}
//				}
//			}		
//		}
//		
//		return listagemResult;
//	}
}