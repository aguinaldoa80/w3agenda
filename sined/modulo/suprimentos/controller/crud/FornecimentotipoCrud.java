package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Fornecimentotipo;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecimentotipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/suprimento/crud/Fornecimentotipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class FornecimentotipoCrud extends CrudControllerSined<FornecimentotipoFiltro, Fornecimentotipo, Fornecimentotipo>{

	
	@Override
	protected void salvar(WebRequestContext request, Fornecimentotipo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_FORNECIMENTOTIPO_NOME")) {
				throw new SinedException("Tipo de fornecimento j� cadastrado no sistema.");
			}
		}
	}
	
}
