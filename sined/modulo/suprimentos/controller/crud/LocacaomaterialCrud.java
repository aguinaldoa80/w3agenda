package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.sql.Date;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Locacaomaterial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.service.BempatrimonioService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LocacaomaterialFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/suprimento/crud/Locacaomaterial", "/patrimonio/crud/Locacaomaterial"}, authorizationModule=CrudAuthorizationModule.class)
public class LocacaomaterialCrud extends CrudControllerSined<LocacaomaterialFiltro, Locacaomaterial, Locacaomaterial>{
	
	private MaterialgrupoService materialgrupoService;
	private MaterialtipoService materialtipoService;
	private MaterialService materialService;
	private BempatrimonioService bempatrimonioService;
	
	public void setBempatrimonioService(BempatrimonioService bempatrimonioService){
		this.bempatrimonioService = bempatrimonioService;
	}
	
	public void setMaterialService(MaterialService materialService){
		this.materialService = materialService;
	}
	
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Locacaomaterial form) throws Exception {
		if(form.getCdlocacaomaterial() == null)
			form.setDtinicio(new Date(System.currentTimeMillis()));
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos(form.getBempatrimonio() != null ? form.getBempatrimonio().getMaterialgrupo() : null));
		request.setAttribute("listaTipo", materialtipoService.findAtivos(form.getBempatrimonio() != null ? form.getBempatrimonio().getMaterialtipo() : null));
		String parameter = request.getParameter("copiar");
		if ("true".equals(parameter)) {
			form.setCdlocacaomaterial(null);
		}
	}
	
	@Override
	protected void validateBean(Locacaomaterial bean, BindException errors) {
		if(bean.getQtde() < 1)
			errors.reject("001", "O campo Qtde. deve ser maior que zero.");
	}
	
	@Override
	protected void listagem(WebRequestContext request, LocacaomaterialFiltro filtro) throws Exception {
		request.setAttribute("listaGrupo", materialgrupoService.findAtivos());
		request.setAttribute("listaTipo", materialtipoService.findAtivos());
		if(filtro.getBempatrimonio() != null)
			filtro.setBempatrimonio(bempatrimonioService.load(filtro.getBempatrimonio(), "bempatrimonio.cdmaterial, bempatrimonio.nome"));
	}
	
	public ModelAndView ajaxMaterialgrupoDoBempatrimonio(WebRequestContext request, Locacaomaterial localMaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(localMaterial.getBempatrimonio() == null || localMaterial.getBempatrimonio().getCdmaterial() == null)
			return jsonModelAndView;
		Material material = materialService.loadMaterialWithMaterialgrupo(localMaterial.getBempatrimonio());
		if(material != null && material.getMaterialgrupo() != null)
			jsonModelAndView.addObject("materialgrupo",  material.getMaterialgrupo().getCdmaterialgrupo());
		else
			jsonModelAndView.addObject("materialgrupo",  null);
		return jsonModelAndView;
	}
	
	
	public ModelAndView ajaxMaterialtipoDoBempatrimonio(WebRequestContext request, Locacaomaterial localMaterial){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(localMaterial.getBempatrimonio() == null || localMaterial.getBempatrimonio().getCdmaterial() == null)
			return jsonModelAndView;
		
		Material m = materialService.loadMaterialWithMaterialtipo(localMaterial.getBempatrimonio());
		if(m != null && m.getMaterialtipo() != null)
			jsonModelAndView.addObject("materialtipo",  m.getMaterialgrupo().getCdmaterialgrupo());
		else
			jsonModelAndView.addObject("materialtipo",  null);
		return jsonModelAndView;
	}
	
}
