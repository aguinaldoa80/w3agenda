package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Fornecimentocontratoitem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.FornecimentocontratoService;
import br.com.linkcom.sined.geral.service.FornecimentotipoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecimentocontratoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/suprimento/crud/Fornecimentocontrato", authorizationModule=CrudAuthorizationModule.class)
public class FornecimentocontratoCrud extends CrudControllerSined<FornecimentocontratoFiltro, Fornecimentocontrato, Fornecimentocontrato>{
	
	private CentrocustoService centrocustoService;
	private FornecedorService fornecedorService;
	private FornecimentotipoService fornecimentotipoService;
	private ContagerencialService contagerencialService;
	private FornecimentocontratoService fornecimentocontratoService;
	private OrdemcompraService ordemcompraService;
	private ProjetoService projetoService;
	private MaterialService materialService;
	private EmpresaService empresaService;
	
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setFornecimentotipoService(FornecimentotipoService fornecimentotipoService) {
		this.fornecimentotipoService = fornecimentotipoService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setFornecimentocontratoService(FornecimentocontratoService fornecimentocontratoService) {
		this.fornecimentocontratoService = fornecimentocontratoService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	protected Fornecimentocontrato criar(WebRequestContext request,	Fornecimentocontrato form) throws Exception {
		if(request.getParameter("gerarfornecimento") != null && "true".equals(request.getParameter("gerarfornecimento"))){
			String whereInOC = SinedUtil.getItensSelecionados(request);
			return fornecimentocontratoService.gerarFornecimento(whereInOC);
		}
		return super.criar(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request,	Fornecimentocontrato form) throws CrudException {
		if(form.getWhereInOrdemcompra() != null && !"".equals(form.getWhereInOrdemcompra())){
			super.doSalvar(request, form);
			request.clearMessages();
			request.addMessage("Fornecimento " + form.getCdfornecimentocontrato() + " gerada com sucesso.", MessageType.INFO);
			return new ModelAndView("redirect:/suprimento/crud/Ordemcompra" + ( form.getWhereInOrdemcompra().split(",").length > 1 ? "" : "?ACAO=consultar&cdordemcompra=" + form.getWhereInOrdemcompra()) );
		}
		return super.doSalvar(request, form);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Fornecimentocontrato bean) {
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	protected void listagem(WebRequestContext request, FornecimentocontratoFiltro filtro) throws Exception {
		
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		request.setAttribute("listaFornecimentotipo", fornecimentotipoService.findAtivos());	
//		request.setAttribute("listaFornecedor", fornecedorService.findAtivos());	
		
	}
	
	@Override
	protected void entrada(WebRequestContext request, Fornecimentocontrato form) throws Exception {
		if(form.getCdfornecimentocontrato() == null){
			form.setColaborador(SinedUtil.getUsuarioComoColaborador());
		}
		
		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getCdfornecimentocontrato() != null && form.getProjeto() != null){
			listaProjeto.add(form.getProjeto());
			listaProjeto = projetoService.findProjetosAbertosSemPermissao(listaProjeto, true, null);
		} else {
			listaProjeto = projetoService.findProjetosAbertos(null, true, null);
		}
		List<Centrocusto> listaCentrocusto = new ListSet<Centrocusto>(Centrocusto.class);
		if(form.getCdfornecimentocontrato() != null && form.getCentrocusto() != null){
			listaCentrocusto.add(form.getCentrocusto());
			listaCentrocusto = centrocustoService.findAtivos(listaCentrocusto);
		} else {
			listaCentrocusto = centrocustoService.findAtivos();
		}
		
		request.setAttribute("listaProjeto", listaProjeto);
		request.setAttribute("listaCentrocusto", listaCentrocusto);
		request.setAttribute("listaFornecimentotipo", fornecimentotipoService.findAtivos(form.getFornecimentotipo()));	
		request.setAttribute("listaFornecedor", fornecedorService.findAtivos(form.getFornecedor()));
		request.setAttribute("listaContagerencial", contagerencialService.findAnaliticas(Tipooperacao.TIPO_DEBITO));
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		
		if (form != null && form.getCdfornecimentocontrato() != null) {
			List<OrigemsuprimentosBean> listaBeanDestino = fornecimentocontratoService.montaOrigem(form);
			request.setAttribute("listaOrigem", listaBeanDestino);
		}
		
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}
	
	@Override
	protected ListagemResult<Fornecimentocontrato> getLista(WebRequestContext request, FornecimentocontratoFiltro filtro) {
		ListagemResult<Fornecimentocontrato> listagemResult = super.getLista(request, filtro);
		List<Fornecimentocontrato> lista = listagemResult.list();
		
		java.sql.Date dtatual = SinedDateUtils.currentDate();
		java.sql.Date dtreferenciarAvencer = SinedDateUtils.addDiasData(dtatual, 7);
		for (Fornecimentocontrato fornecimentocontrato : lista) {
			
			boolean corvermelho = fornecimentocontrato.getDtfim() != null &&
								 (SinedDateUtils.beforeIgnoreHour(fornecimentocontrato.getDtfim(), dtatual) || 
								  (SinedDateUtils.afterOrEqualsIgnoreHour(fornecimentocontrato.getDtfim(), dtatual) && 
								  SinedDateUtils.beforeOrEqualIgnoreHour(fornecimentocontrato.getDtfim(), dtreferenciarAvencer)));
			
			fornecimentocontrato.setCorvermelho(corvermelho);
		}
		
		return listagemResult;
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, Fornecimentocontrato bean) throws Exception {
		Boolean criado = bean.getCdfornecimentocontrato() == null;
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_FORNECCONTRATO_DESCRICAO")) {
				throw new SinedException("Contrato de fornecimento j� cadastrado no sistema para este fornecedor.");
			}
		}
		if(criado){
			if(bean.getWhereInOrdemcompra() != null && !bean.getWhereInOrdemcompra().equals("")){
				ordemcompraService.baixarAposGerarfornecimento(bean.getWhereInOrdemcompra(), bean);
			}
		}
	}
	
	public ModelAndView verificaFornecimentoDuplicado(WebRequestContext request, Fornecimentocontrato fornecimentocontrato){
		return new JsonModelAndView().addObject("exibirMsgContratoDuplicado", fornecimentocontratoService.exibirMsgContratoDuplicado(fornecimentocontrato));
	}
	
	public ModelAndView gerarCSV(WebRequestContext request, FornecimentocontratoFiltro filtro){
		List<Fornecimentocontrato> lista = fornecimentocontratoService.findForCSV();
		StringBuilder rel = new StringBuilder();
		
		//String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdfornecimentocontrato", ",");
		//List<Fornecimentocontratoitem> listaItem = fornecimentocontratoitemService.findForCSV(whereIn);
		
		rel.append("Descri��o;");
		rel.append("Fornecedor;");
		rel.append("Centro de Custo;");
		rel.append("Projeto;");
		rel.append("Conta Gerencial;");
		rel.append("Valor;");
		rel.append("Prazo Pagamento;");
		rel.append("Dt Prox. Vencimento;");
		rel.append("Dt In�cio;");
		rel.append("Dt Fim;");
		rel.append("Observa��o;");
		rel.append("Tipo Fornecimento;");
		rel.append("Item;");
		rel.append("Qt. Item;");
		rel.append("Observa��o Item;");
		rel.append("Fluxo Caixa;");
		
		rel.append("Al�quita ISS;");
		rel.append("Valor ISS;");
		rel.append("Al�quita INSS;");
		rel.append("Valor INSS;");
		rel.append("Al�quita IR;");
		rel.append("Valor IR;");
		rel.append("Al�quita PIS;");
		rel.append("Valor PIS;");
		rel.append("Al�quita COFINS;");
		rel.append("Valor COFINS;");
		rel.append("Al�quita CSLL;");
		rel.append("Valor CSLL;");
		rel.append("Al�quita ICMS;");
		rel.append("Valor ICMS;");
		rel.append("Valor Final;");
		rel.append("C�digo GPS;");
		rel.append("\n");
		
		for (Fornecimentocontrato contrato : lista) {
			
			rel.append(contrato.getDescricao()).append(";");
			rel.append(contrato.getFornecedor() != null ? contrato.getFornecedor().getNome() : "").append(";");
			rel.append(contrato.getCentrocusto() != null ? contrato.getCentrocusto().getNome() : "").append(";");
			rel.append(contrato.getProjeto() != null ? contrato.getProjeto().getNome() : "").append(";");
			rel.append(contrato.getContagerencial().getNome()).append(";");
			rel.append(contrato.getValor()).append(";");
			rel.append(contrato.getPrazopagamento().getNome()).append(";");
			rel.append(contrato.getDtproximovencimento()).append(";");
			rel.append(contrato.getDtinicio()).append(";");
			rel.append(contrato.getDtfim()).append(";");
			rel.append(contrato.getObservacao()).append(";");
			rel.append(contrato.getFornecimentotipo() != null ? contrato.getFornecimentotipo().getNome() : "").append(";");
			rel.append(contrato.getListaFornecimentocontratoitem().iterator().next().getDescricao()).append(";");
			rel.append(contrato.getListaFornecimentocontratoitem().iterator().next().getQtde()).append(";");
			rel.append(contrato.getListaFornecimentocontratoitem().iterator().next().getObservacao()).append(";");
			rel.append(contrato.getFluxocaixa().getDescricao()).append(";");
			rel.append(contrato.getIss()).append(";");
			rel.append(contrato.getValorIss()).append(";");
			rel.append(contrato.getInss()).append(";");
			rel.append(contrato.getValorInss()).append(";");
			rel.append(contrato.getIr()).append(";");
			rel.append(contrato.getValorIr()).append(";");
			rel.append(contrato.getPis()).append(";");
			rel.append(contrato.getValorPis()).append(";");
			rel.append(contrato.getCofins()).append(";");
			rel.append(contrato.getValorCofins()).append(";");
			rel.append(contrato.getCsll()).append(";");
			rel.append(contrato.getValorCsll()).append(";");
			rel.append(contrato.getIcms()).append(";");
			rel.append(contrato.getValorIcms()).append(";");
			rel.append(contrato.getValorFinal());

			if(contrato.getListaFornecimentocontratoitem() != null && !contrato.getListaFornecimentocontratoitem().isEmpty() && contrato.getListaFornecimentocontratoitem().size() > 1){				
				boolean primeiro = true;
				for(Fornecimentocontratoitem fornecimentocontratoitem : contrato.getListaFornecimentocontratoitem()){
					if(!primeiro){
						if(fornecimentocontratoitem.getDescricao() != null){						
							rel.append("\n;;;;;;;;;;;;").append(fornecimentocontratoitem.getDescricao()).append(";");
							rel.append(fornecimentocontratoitem.getQtde()).append(";");
							rel.append(fornecimentocontratoitem.getObservacao()).append(";");
						}
					}else{
						primeiro = false;
					}
				}
			}
			rel.append("\n");
		}
		
		Resource resource = new Resource("text/csv","fornecimentocontrato_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll("null;", ";").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource); 
		return resourceModelAndView;
	}
	
	public ModelAndView buscaInfMaterialAjax(WebRequestContext request){
		String cdmaterial = request.getParameter("cdmaterial");
		Material material = materialService.load(new Material(Integer.parseInt(cdmaterial)), "material.cdmaterial, material.patrimonio");
		boolean geristrarpatrimonioitem = false;
		if(material != null && material.getPatrimonio() != null){
			geristrarpatrimonioitem = material.getPatrimonio();
		}
		return new JsonModelAndView().addObject("geristrarpatrimonioitem", geristrarpatrimonioitem);
	}
}
