package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.LotematerialService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LoteestoqueFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/suprimento/crud/Loteestoque",
		authorizationModule=CrudAuthorizationModule.class
)
public class LoteestoqueCrud extends CrudControllerSined<LoteestoqueFiltro, Loteestoque, Loteestoque>{
	private LotematerialService lotematerialService;
	private LoteestoqueService loteestoqueService;
	private FornecedorService fornecedorService;
	private MaterialService materialService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ParametrogeralService parametrogeralService;
	
	public void setLotematerialService(LotematerialService lotematerialService) {this.lotematerialService = lotematerialService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@Override
	protected ListagemResult<Loteestoque> getLista(WebRequestContext request, LoteestoqueFiltro filtro) {
		ListagemResult<Loteestoque> lista = super.getLista(request, filtro);
		List<Loteestoque> list = lista.list();

		if(list != null && list.size() > 0){
			String whereInLoteestoque = CollectionsUtil.listAndConcatenate(list, "cdloteestoque", ",");
			List<Loteestoque> listaLoteestoque = loteestoqueService.findWithLotematerial(whereInLoteestoque);
			if(SinedUtil.isListNotEmpty(listaLoteestoque)){
				for(Loteestoque loteestoque : listaLoteestoque){
					for(Loteestoque loteestoqueListagem : list){
						if(loteestoqueListagem.equals(loteestoque)){
							loteestoqueListagem.setListaLotematerial(loteestoque.getListaLotematerial());
							break;
						}
					}
				}
			}
		}
		
		return lista;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Loteestoque form)	throws Exception {
		if(form != null){
			if(form.getCdloteestoque() != null){		
				form.setListaLotematerial(lotematerialService.findAllByLoteestoque(form));
			}

			if(form.getFornecedor() != null && form.getFornecedor().getCdpessoa() != null){
				form.setFornecedorTrans(form.getFornecedor());
				request.getSession().setAttribute("fornecedorForAutocompleteMaterial", form.getFornecedor());
			} else{
				request.getSession().removeAttribute("fornecedorForAutocompleteMaterial");
			}
			form.setNumero(request.getParameter("numero"));
		}
		
		String fromInsertOne = request.getParameter("fromInsertOne");
		if("true".equalsIgnoreCase(fromInsertOne)){
			String cdfornecedor = request.getParameter("cdfornecedor");
			if(cdfornecedor != null && !cdfornecedor.trim().isEmpty() && StringUtils.isNumeric(cdfornecedor)){
				form.setFornecedor(fornecedorService.load(Integer.parseInt(cdfornecedor)));
				form.setFornecedorTrans(form.getFornecedor());
				request.getSession().setAttribute("fornecedorForAutocompleteMaterial", form.getFornecedor());				
			}
			
			String cdmaterial = request.getParameter("cdmaterial");
			Lotematerial lotematerial = null;
			if(cdmaterial != null && !cdmaterial.trim().isEmpty() && StringUtils.isNumeric(cdmaterial)){
				try {
					Material material = materialService.loadByCodigoMaterial(Integer.parseInt(cdmaterial));
					if(material != null && material.getCdmaterial() != null){
						lotematerial = new Lotematerial();
						lotematerial.setMaterial(material);
						lotematerial.setMaterialTrans(material);
						lotematerial.setBloqueado(Boolean.TRUE);
						if(form.getListaLotematerial() == null){
							form.setListaLotematerial(new ListSet<Lotematerial>(Lotematerial.class));
						}
						form.getListaLotematerial().add(lotematerial);
					}
				} catch (Exception e) {}
			}
		}
		form.setFromImportacaoXml("true".equals(request.getParameter("fromImportacaoXml")));
	}

	@Override
	protected void salvar(WebRequestContext request, Loteestoque bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
//			if (DatabaseError.isKeyPresent(e, "IDX_LOTEESTOQUE_NUMEROFORNECEDOR")) {
//				throw new SinedException("Lote j� cadastrado no sistema.");
//			} else {
				e.printStackTrace();
//			}
		}			
	}
	
	public ModelAndView definirFornecedorLoteestoque(WebRequestContext request, Fornecedor fornecedor){
		try {			
			request.getSession().removeAttribute("fornecedorForAutocompleteMaterial");
			if(fornecedor != null && fornecedor.getCdpessoa() != null){
				request.getSession().setAttribute("fornecedorForAutocompleteMaterial", fornecedor);
			}
		} catch (Exception e) {
			return new JsonModelAndView().addObject("error", true).addObject("msg", e.getMessage());
		}
		
		return new JsonModelAndView().addObject("error", false);
	}
	
	public ModelAndView setMaterialForLoteSessao(WebRequestContext request){
		String cdmaterial = request.getParameter("cdmaterial");
		request.getSession().removeAttribute("MATERIAL_LOTE");
		if(cdmaterial != null && StringUtils.isNotBlank(cdmaterial) && StringUtils.isNumeric(cdmaterial)){
			request.getSession().setAttribute("MATERIAL_LOTE",MaterialService.getInstance().loadByCodigoMaterial(Integer.parseInt(cdmaterial.toString())));
		}
		
		return new JsonModelAndView();
	}
	
	
	@Override
	protected void validateBean(Loteestoque bean, BindException errors) {
		if(bean.getCdloteestoque() != null){
			Loteestoque loteestoqueAux = loteestoqueService.findWithLotematerial(bean);
			if(loteestoqueAux != null && SinedUtil.isListNotEmpty(loteestoqueAux.getListaLotematerial()) && SinedUtil.isListNotEmpty(bean.getListaLotematerial())){
				if(loteestoqueAux.getListaLotematerial().size() > bean.getListaLotematerial().size()){
					for (Lotematerial lotematerial : loteestoqueAux.getListaLotematerial()){
						boolean existeMaterial = false;
						for (Lotematerial lotematerial2 : bean.getListaLotematerial()) {
							if(lotematerial.getMaterial().getCdmaterial().equals(lotematerial2.getMaterial().getCdmaterial())){
								existeMaterial = true;
							}
							if(!existeMaterial){
								if(movimentacaoestoqueService.existeMovimentacaoEstoqueMaterialLote(bean, lotematerial.getMaterial())){
									errors.reject("001", "N�o � poss�vel excluir o material " + lotematerial.getMaterial().getNome() + " pois existe movimenta��o de estoque desse material nesse lote.");
								}
							}
						}
					}				
				}				
			}else if(loteestoqueAux != null && SinedUtil.isListNotEmpty(loteestoqueAux.getListaLotematerial()) && SinedUtil.isListEmpty(bean.getListaLotematerial())){
				for (Lotematerial lotematerial : loteestoqueAux.getListaLotematerial()){
					if(movimentacaoestoqueService.existeMovimentacaoEstoqueMaterialLote(bean, lotematerial.getMaterial())){
						errors.reject("001", "N�o � poss�vel excluir o material " + lotematerial.getMaterial().getNome() + " pois existe movimenta��o de estoque desse material nesse lote.");
					}
				}
			}
		}

		if(SinedUtil.isListEmpty(bean.getListaLotematerial()) && Boolean.TRUE.equals(bean.getFromImportacaoXml())){
			errors.reject("001", "Informe no lote o material correspondente ao material do xml.");
		}
		if(SinedUtil.isListNotEmpty(bean.getListaLotematerial()) && Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.SELECAO_AUTOMATICA_LOTE_FATURAMENTO))){
			for (Lotematerial lotematerial : bean.getListaLotematerial()) {
				if(materialService.isLoteObrigatorio(lotematerial.getMaterial()) && lotematerial.getValidade() ==  null){
					errors.reject("001", "A data de validade � obrigat�ria para o material " + lotematerial.getMaterial().getNome() + ".");
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaLotematerial())){
			for (Lotematerial lotematerial : bean.getListaLotematerial()) {
				if(lotematerialService.existeNumeroFornecedorMaterial(bean, lotematerial.getMaterial())){
					errors.reject("001", "Existe Fornecedor/N�mero j� cadastrado para o material " + lotematerial.getMaterial().getNome() + ".");
				}
			}
		}
	}
	
}
