package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;


public class RegistrarConsumoBean {

	private Integer cdmaterial;
	private Integer cdlocalarmazenagem;
	private Integer cdmaterialclasse;
	private Integer cdloteestoque;
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	public Integer getCdmaterialclasse() {
		return cdmaterialclasse;
	}
	public Integer getCdloteestoque() {
		return cdloteestoque;
	}
	public void setCdloteestoque(Integer cdloteestoque) {
		this.cdloteestoque = cdloteestoque;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	public void setCdmaterialclasse(Integer cdmaterialclasse) {
		this.cdmaterialclasse = cdmaterialclasse;
	}
	
}
