package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrigemOrdemCompra {
	COTACAO		(0, "Cota��o"), 
	SOLICITACAO_COMPRA	(1, "Solicita��o de compra"),
	USUARIO	(2, "Usu�rio"); 
	
	private Integer id;
	private String nome;
	
	private OrigemOrdemCompra(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
