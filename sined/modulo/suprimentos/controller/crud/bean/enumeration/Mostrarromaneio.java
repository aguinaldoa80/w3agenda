package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Mostrarromaneio {

	GERADO		(0, "Romaneio gerados"), 
	SEMROMANEIO	(1, "Sem romaneio"),
	PENDENTE	(2, "Romaneio pendente"); 
	
	private Integer id;
	private String nome;
		
	private Mostrarromaneio(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
