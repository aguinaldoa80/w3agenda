package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FormaTributacaoEnum {

	ACONDICIONAMENTO("Acondicionamento"),
	TRIBUTACAO_PROPRIA("Tributa��o pr�pria");
	
	private String descricao;
	
	private FormaTributacaoEnum(String descricao){
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
