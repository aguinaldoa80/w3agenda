package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entrega;

public class BaixarEntregaBean {

	protected String ids;
	protected String controller;
	protected List<BaixarEntregaItemBean> listaPendentes;
	protected List<BaixarEntregaItemBean> listaResolvidos;
	protected List<Entrega> listaEntregaRomaneio;
	
	public List<BaixarEntregaItemBean> getListaPendentes() {
		return listaPendentes;
	}
	public List<BaixarEntregaItemBean> getListaResolvidos() {
		return listaResolvidos;
	}
	public String getIds() {
		return ids;
	}
	public String getController() {
		return controller;
	}
	public List<Entrega> getListaEntregaRomaneio() {
		return listaEntregaRomaneio;
	}
	public void setListaEntregaRomaneio(List<Entrega> listaEntregaRomaneio) {
		this.listaEntregaRomaneio = listaEntregaRomaneio;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public void setListaPendentes(List<BaixarEntregaItemBean> listaPendentes) {
		this.listaPendentes = listaPendentes;
	}
	public void setListaResolvidos(List<BaixarEntregaItemBean> listaResolvidos) {
		this.listaResolvidos = listaResolvidos;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
}
