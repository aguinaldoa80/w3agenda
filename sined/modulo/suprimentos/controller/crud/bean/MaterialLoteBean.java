package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

public class MaterialLoteBean {

	private Integer cdmaterial;
	private Boolean obrigarlote;
	
	public MaterialLoteBean(Integer cdmaterial, Boolean obrigarlote){
		this.cdmaterial = cdmaterial;
		this.obrigarlote = obrigarlote;
	}
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Boolean getObrigarlote() {
		return obrigarlote;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setObrigarlote(Boolean obrigarlote) {
		this.obrigarlote = obrigarlote;
	}
}
