package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public class BaixarEntregaItemBean {

	protected Material material;
	protected Material materialmestregrade;
	protected Material materialOrigem;
	protected Materialclasse materialclasse;
	protected Double quantidade;
	protected Double quantidadePrincipal;
	protected Double comprimento;
	protected Localarmazenagem localarmazenagem;
	protected Loteestoque loteestoque;
	protected Empresa empresa;
	protected Fornecedor fornecedor;
	protected Entrega entrega;
	protected Entregamaterial entregamaterial;
	protected Double qtdproduto;
	protected Double qtdpatrimonio;
	protected Double qtdepi;
	protected Double valorunitario;
	protected Projeto projeto;
	protected Centrocusto centrocusto;
	protected Entregadocumento entregadocumento;
	protected Departamento departamento;
	protected Colaborador colaborador;
	protected Unidademedida unidademedidacomercial;
	protected Cfop cfop;
	
	protected String whereInMaterialItenGrade;
	
	protected List<BaixarEntregaItemBean> listaMaterialitemmestregrade;
	
	public BaixarEntregaItemBean() {
	}
	
	public BaixarEntregaItemBean(BaixarEntregaItemBean bean) {
		this.entregamaterial = bean.getEntregamaterial();
		this.material = bean.getMaterial();
		this.materialclasse = bean.getMaterialclasse();
		this.quantidade = bean.getQuantidade();
		this.comprimento = bean.getComprimento();
		this.localarmazenagem = bean.getLocalarmazenagem();
		this.empresa = bean.getEmpresa();
		this.qtdproduto = bean.getQtdproduto();
		this.qtdpatrimonio = bean.getQtdpatrimonio();
		this.qtdepi = bean.getQtdepi();
		this.fornecedor = bean.getFornecedor();
		this.entrega = bean.getEntrega();
		this.entregadocumento = bean.getEntregadocumento();
		this.projeto = bean.getProjeto();
		this.centrocusto = bean.getCentrocusto();
		this.loteestoque = bean.getLoteestoque();
		this.cfop = bean.getCfop();
	}

	public Material getMaterial() {
		return material;
	}
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}	
	public Double getQtdproduto() {
		return qtdproduto;
	}
	public Double getQtdpatrimonio() {
		return qtdpatrimonio;
	}
	public Double getQtdepi() {
		return qtdepi;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Entrega getEntrega() {
		return entrega;
	}
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}

	public Double getValorunitario() {
		return valorunitario;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setQtdproduto(Double qtdproduto) {
		this.qtdproduto = qtdproduto;
	}
	public void setQtdpatrimonio(Double qtdpatrimonio) {
		this.qtdpatrimonio = qtdpatrimonio;
	}
	public void setQtdepi(Double qtdepi) {
		this.qtdepi = qtdepi;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public List<BaixarEntregaItemBean> getListaMaterialitemmestregrade() {
		return listaMaterialitemmestregrade;
	}

	public void setListaMaterialitemmestregrade(List<BaixarEntregaItemBean> listaMaterialitemmestregrade) {
		this.listaMaterialitemmestregrade = listaMaterialitemmestregrade;
	}
	
	public String getMaterialDescricao(){
		return getMaterial() != null ? getMaterial().getNome() : "";
	}

	public Material getMaterialmestregrade() {
		return materialmestregrade;
	}

	public void setMaterialmestregrade(Material materialmestregrade) {
		this.materialmestregrade = materialmestregrade;
	}
	
	public Unidademedida getUnidademedidacomercial() {
		return unidademedidacomercial;
	}

	public void setUnidademedidacomercial(Unidademedida unidademedidacomercial) {
		this.unidademedidacomercial = unidademedidacomercial;
	}

	public Double getQuantidadePrincipal() {
		return quantidadePrincipal;
	}

	public void setQuantidadePrincipal(Double quantidadePrincipal) {
		this.quantidadePrincipal = quantidadePrincipal;
	}
	
	public String getWhereInMaterialItenGrade() {
		return whereInMaterialItenGrade;
	}

	public void setWhereInMaterialItenGrade(String whereInMaterialItenGrade) {
		this.whereInMaterialItenGrade = whereInMaterialItenGrade;
	}

	public String getDescricaoQtde(){
		String descricaoqtde = "Quantidade";
		if(getUnidademedidacomercial() != null && getUnidademedidacomercial().getSimbolo() != null &&
				!"".equals(getUnidademedidacomercial().getSimbolo())){
			descricaoqtde += " (" + getUnidademedidacomercial().getSimbolo() + ")";
		}
		descricaoqtde += ": " + (getQuantidade() != null ? getQuantidade() : "");
		return descricaoqtde;
	}
	
	public Double getQtdeTotalItens(){
		Double qtdeTotal = 0d;
		if(listaMaterialitemmestregrade != null && !listaMaterialitemmestregrade.isEmpty()){
			for(BaixarEntregaItemBean baixarEntregaItemBean : listaMaterialitemmestregrade){
				if(baixarEntregaItemBean.getQuantidade() != null){
					qtdeTotal += baixarEntregaItemBean.getQuantidade();
				}
			}
		}
		return qtdeTotal;
	}

	public Cfop getCfop() {
		return cfop;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public Material getMaterialOrigem() {
		return materialOrigem;
	}
	
	public void setMaterialOrigem(Material materialOrigem) {
		this.materialOrigem = materialOrigem;
	}
}
