package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;

public class GerarRomaneioBaixarEntregaSolicitacao {

	private Material material;
	private Materialclasse materialclasse;
	private Double qtde;
	private Localarmazenagem localarmazenagem;
	private String origem;
	
	public Material getMaterial() {
		return material;
	}
	public Double getQtde() {
		return qtde;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public String getOrigem() {
		return origem;
	}
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((localarmazenagem == null) ? 0 : localarmazenagem.hashCode());
		result = prime * result
				+ ((material == null) ? 0 : material.hashCode());
		result = prime * result
				+ ((materialclasse == null) ? 0 : materialclasse.hashCode());
		result = prime * result + ((origem == null) ? 0 : origem.hashCode());
		result = prime * result + ((qtde == null) ? 0 : qtde.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GerarRomaneioBaixarEntregaSolicitacao other = (GerarRomaneioBaixarEntregaSolicitacao) obj;
		if (localarmazenagem == null) {
			if (other.localarmazenagem != null)
				return false;
		} else if (!localarmazenagem.equals(other.localarmazenagem))
			return false;
		if (material == null) {
			if (other.material != null)
				return false;
		} else if (!material.equals(other.material))
			return false;
		if (materialclasse == null) {
			if (other.materialclasse != null)
				return false;
		} else if (!materialclasse.equals(other.materialclasse))
			return false;
		if (origem == null) {
			if (other.origem != null)
				return false;
		} else if (!origem.equals(other.origem))
			return false;
		if (qtde == null) {
			if (other.qtde != null)
				return false;
		} else if (!qtde.equals(other.qtde))
			return false;
		return true;
	}
	
}
