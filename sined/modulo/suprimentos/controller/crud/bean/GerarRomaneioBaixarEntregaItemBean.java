package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.util.List;

public class GerarRomaneioBaixarEntregaItemBean {

	private Integer cdentrega;
	private List<GerarRomaneioBaixarEntregaSolicitacao> listaSolicitacoes;
	private List<GerarRomaneioBaixarEntregaEntradaEstoque> listaEntradaestoque;
	private List<GerarRomaneioBaixarEntregaRomaneio> listaRomaneio;
	
	public Integer getCdentrega() {
		return cdentrega;
	}
	public List<GerarRomaneioBaixarEntregaSolicitacao> getListaSolicitacoes() {
		return listaSolicitacoes;
	}
	public List<GerarRomaneioBaixarEntregaEntradaEstoque> getListaEntradaestoque() {
		return listaEntradaestoque;
	}
	public List<GerarRomaneioBaixarEntregaRomaneio> getListaRomaneio() {
		return listaRomaneio;
	}
	public void setCdentrega(Integer cdentrega) {
		this.cdentrega = cdentrega;
	}
	public void setListaSolicitacoes(
			List<GerarRomaneioBaixarEntregaSolicitacao> listaSolicitacoes) {
		this.listaSolicitacoes = listaSolicitacoes;
	}
	public void setListaEntradaestoque(
			List<GerarRomaneioBaixarEntregaEntradaEstoque> listaEntradaestoque) {
		this.listaEntradaestoque = listaEntradaestoque;
	}
	public void setListaRomaneio(
			List<GerarRomaneioBaixarEntregaRomaneio> listaRomaneio) {
		this.listaRomaneio = listaRomaneio;
	}
	
}
