package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;



public class MaterialComboBean {

	protected Integer cdmaterial;
	protected String nome;
	protected Integer cdcontagerencialvenda;
	protected Integer cdcentrocustovenda;
	protected String descricaoInputPersonalizadoContagerencialvenda;
	protected Boolean servico;
	protected Boolean produto;
	protected Boolean tributacaoestadual;
	protected Double valorVenda;
	protected Double quantidade;
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public String getNome() {
		return nome;
	}
	
	public Double getValorVenda() {
		return valorVenda;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}

	public Integer getCdcontagerencialvenda() {
		return cdcontagerencialvenda;
	}

	public Integer getCdcentrocustovenda() {
		return cdcentrocustovenda;
	}

	public Boolean getServico() {
		return servico;
	}

	public Boolean getProduto() {
		return produto;
	}

	public Boolean getTributacaoestadual() {
		return tributacaoestadual;
	}
	
	public String getDescricaoInputPersonalizadoContagerencialvenda() {
		return descricaoInputPersonalizadoContagerencialvenda;
	}

	public void setDescricaoInputPersonalizadoContagerencialvenda(
			String descricaoInputPersonalizadoContagerencialvenda) {
		this.descricaoInputPersonalizadoContagerencialvenda = descricaoInputPersonalizadoContagerencialvenda;
	}

	public void setCdcontagerencialvenda(Integer cdcontagerencialvenda) {
		this.cdcontagerencialvenda = cdcontagerencialvenda;
	}

	public void setCdcentrocustovenda(Integer cdcentrocustovenda) {
		this.cdcentrocustovenda = cdcentrocustovenda;
	}

	public void setServico(Boolean servico) {
		this.servico = servico;
	}

	public void setProduto(Boolean produto) {
		this.produto = produto;
	}

	public void setTributacaoestadual(Boolean tributacaoestadual) {
		this.tributacaoestadual = tributacaoestadual;
	}

	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
