package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;


public class AnaliseCompraBean {

	protected Integer cdmaterial;
	protected String material;
	protected Integer cdpessoa;
	protected String fornecedor;
	protected Date datacompra;
	protected Date dataentrega;
	protected Money valorunitario;
	protected Double qtde;
	protected Integer qtdeDias;
	protected Money total;
	protected Money valorultimacompra;
	protected Money valormelhorpreco;
	protected Money valormelhorprecofornecedor;
	protected Date datacompramelhorpreco;
	protected Double indicepreco;
	
	protected Integer qtdeocmenorultimovalor;
	protected Integer qtdetotaloc;
	
	public AnaliseCompraBean(){
	}

	public AnaliseCompraBean(Integer cdmaterial, String material, Integer cdpessoa, String fornecedor, Date datacompra, Date dataentrega, Double valorunitario,
							 Double qtde, Integer qtdeDias, Double _total, Double _frete, Double _desconto){
		this.cdmaterial = cdmaterial;
		this.material= material;
		this.cdpessoa = cdpessoa;
		this.fornecedor = fornecedor;
		this.datacompra = datacompra;
		this.dataentrega = dataentrega;
		this.valorunitario = new Money(valorunitario);
		this.qtde = qtde;
		this.qtdeDias = qtdeDias;
		this.total = new Money(_total);
		if (_frete != null){
			this.total = this.total.add(new Money(_frete));
		} 
		if (_desconto != null){
			this.total = this.total.subtract(new Money(_desconto));
		}
	}
	
	public AnaliseCompraBean(Integer cdmaterial, String material, Integer cdpessoa, String fornecedor, Date datacompra, Date dataentrega, Double valorunitario,
			 Double qtde, Integer qtdeDias, Double _total, Double _frete, Double _desconto, Double _valorultimacompra, Double _valormehorpreco, 
			 Double _valormelhorprecofornecedor){
		this.cdmaterial = cdmaterial;
		this.material= material;
		this.cdpessoa = cdpessoa;
		this.fornecedor = fornecedor;
		this.datacompra = datacompra;
		this.dataentrega = dataentrega;
		this.valorunitario = new Money(valorunitario);
		this.qtde = qtde;
		this.qtdeDias = qtdeDias;
		this.total = new Money(_total);
		if (_frete != null){
			this.total = this.total.add(new Money(_frete));
		} 
		if (_desconto != null){
			this.total = this.total.subtract(new Money(_desconto));
		}
		if(_valorultimacompra != null){
			this.valorultimacompra = new Money(_valorultimacompra);
		}
		if(_valormehorpreco != null){
			this.valormelhorpreco = new Money(_valormehorpreco);
		}
		if(_valormelhorprecofornecedor != null){
			this.valormelhorprecofornecedor = new Money(_valormelhorprecofornecedor);
		}
	}
	
	public AnaliseCompraBean(Integer cdmaterial, String material, Integer cdordemcompra, Date datacompra, Double valorunitario, Double _valorultimacompra){
		this.cdmaterial = cdmaterial;
		this.material= material;
		this.datacompra = datacompra;
		this.valorunitario = new Money(valorunitario);
		if(_valorultimacompra != null){
			this.valorultimacompra = new Money(_valorultimacompra);
		}
	}
	
	public String getMaterial() {
		return material;
	}
	public String getFornecedor() {
		return fornecedor;
	}
	public Date getDatacompra() {
		return datacompra;
	}
	public Date getDataentrega() {
		return dataentrega;
	}
	public Money getValorunitario() {
		return valorunitario;
	}
	public Double getQtde() {
		return qtde;
	}
	public Money getValorultimacompra() {
		return valorultimacompra;
	}
	public Money getValormelhorpreco() {
		return valormelhorpreco;
	}
	public Integer getQtdeDias() {
		return qtdeDias;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public Double getIndicepreco() {
		return indicepreco;
	}

	public void setIndicepreco(Double indicepreco) {
		this.indicepreco = indicepreco;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setDatacompra(Date datacompra) {
		this.datacompra = datacompra;
	}
	public void setDataentrega(Date dataentrega) {
		this.dataentrega = dataentrega;
	}
	public void setValorunitario(Money valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setQtdeDias(Integer qtdeDias) {
		this.qtdeDias = qtdeDias;
	}
	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	public void setValorultimacompra(Money valorultimacompra) {
		this.valorultimacompra = valorultimacompra;
	}
	public void setValormelhorpreco(Money valormelhorpreco) {
		this.valormelhorpreco = valormelhorpreco;
	}

	public Integer getQtdeocmenorultimovalor() {
		return qtdeocmenorultimovalor;
	}
	public Integer getQtdetotaloc() {
		return qtdetotaloc;
	}

	public void setQtdeocmenorultimovalor(Integer qtdeocmenorultimovalor) {
		this.qtdeocmenorultimovalor = qtdeocmenorultimovalor;
	}
	public void setQtdetotaloc(Integer qtdetotaloc) {
		this.qtdetotaloc = qtdetotaloc;
	}

	public Money getValormelhorprecofornecedor() {
		return valormelhorprecofornecedor;
	}
	public void setValormelhorprecofornecedor(Money valormelhorprecofornecedor) {
		this.valormelhorprecofornecedor = valormelhorprecofornecedor;
	}

	public Date getDatacompramelhorpreco() {
		return datacompramelhorpreco;
	}
	public void setDatacompramelhorpreco(Date datacompramelhorpreco) {
		this.datacompramelhorpreco = datacompramelhorpreco;
	}
}
