package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;

public class TotalizadorEntradaSaida {
	
	protected Movimentacaoestoquetipo movimentacaoestoquetipo;
	protected Double qtde;
	protected Double valor;
	protected Double pesototal;
	protected Double pesobrutototal;
	
	public TotalizadorEntradaSaida(){}
	
	public TotalizadorEntradaSaida(Integer tipo, Double qtde, Double valor, Double pesototal, Double pesobrutototal){
		if(tipo != null){
			this.movimentacaoestoquetipo = Movimentacaoestoquetipo.ENTRADA.getCdmovimentacaoestoquetipo().equals(tipo) ? Movimentacaoestoquetipo.ENTRADA : Movimentacaoestoquetipo.SAIDA;
		}
		this.qtde = qtde != null ? qtde : 0.0;;
		this.valor = valor != null ? valor : 0.0;
		this.pesototal = pesototal != null ? pesototal : 0.0;
		this.pesobrutototal = pesobrutototal != null ? pesobrutototal : 0.0;
	}

	public Movimentacaoestoquetipo getMovimentacaoestoquetipo() {
		return movimentacaoestoquetipo;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getValor() {
		return valor;
	}
	public Double getPesototal() {
		return pesototal;
	}
	public Double getPesobrutototal() {
		return pesobrutototal;
	}

	public void setMovimentacaoestoquetipo(Movimentacaoestoquetipo movimentacaoestoquetipo) {
		this.movimentacaoestoquetipo = movimentacaoestoquetipo;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setPesototal(Double pesototal) {
		this.pesototal = pesototal;
	}
	public void setPesobrutototal(Double pesobrutototal) {
		this.pesobrutototal = pesobrutototal;
	}
}