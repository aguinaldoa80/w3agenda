package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.util.List;

public class GerarRomaneioBaixarEntregaBean {
	
	private String controller; 
	private String ids;
	private List<GerarRomaneioBaixarEntregaItemBean> listaEntregaRomaneio;
	
	public String getIds() {
		return ids;
	}
	public String getController() {
		return controller;
	}
	public List<GerarRomaneioBaixarEntregaItemBean> getListaEntregaRomaneio() {
		return listaEntregaRomaneio;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public void setListaEntregaRomaneio(
			List<GerarRomaneioBaixarEntregaItemBean> listaEntregaRomaneio) {
		this.listaEntregaRomaneio = listaEntregaRomaneio;
	}
	
}
