package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

public class MaterialproducaoAjaxBean {

	private Integer cdmaterial;
	private String nome;
	private Double qtde;
	private Double valorunitario;
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public String getNome() {
		return nome;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	
}
