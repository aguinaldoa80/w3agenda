package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Projeto;

public class GerarMovimentacaoBean {
	
	private Localarmazenagem localarmazenagem;
	private Departamento departamento;
	private Empresa empresa;
	private Colaborador colaborador;
	private Fornecedor fornecedor;
	private Cliente cliente;
	private Projeto projeto;
	
	private String itensSelecionados;
	private Boolean gerarRomaneio;
	
	@DisplayName("Local")
	@Required
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	
	
	public String getItensSelecionados() {
		return itensSelecionados;
	}
	public Boolean getGerarRomaneio() {
		return gerarRomaneio;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setItensSelecionados(String itensSelecionados) {
		this.itensSelecionados = itensSelecionados;
	}
	public void setGerarRomaneio(Boolean gerarRomaneio) {
		this.gerarRomaneio = gerarRomaneio;
	}
}
