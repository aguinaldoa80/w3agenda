package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;

public class GerarRomaneioBaixarEntregaEntradaEstoque {

	private Material material;
	private Double qtde;
	private Localarmazenagem localarmazenagem;
	private Integer cdmovimentacaoestoque;
	private Boolean adicionado;
	
	public Material getMaterial() {
		return material;
	}
	public Double getQtde() {
		return qtde;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Integer getCdmovimentacaoestoque() {
		return cdmovimentacaoestoque;
	}
	public Boolean getAdicionado() {
		return adicionado;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setCdmovimentacaoestoque(Integer cdmovimentacaoestoque) {
		this.cdmovimentacaoestoque = cdmovimentacaoestoque;
	}
	public void setAdicionado(Boolean adicionado) {
		this.adicionado = adicionado;
	}
}
