package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.math.BigDecimal;

import br.com.linkcom.neo.types.Money;


public class TotalizadorGerenciamentoMaterial {
	
	private BigDecimal qtdetotal = new BigDecimal(0);
	private Money valottotal = new Money();
	private BigDecimal pesototal = new BigDecimal(0);
	private BigDecimal pesobrutototal = new BigDecimal(0);
	
	public BigDecimal getQtdetotal() {
		return qtdetotal;
	}
	public Money getValottotal() {
		return valottotal;
	}
	public BigDecimal getPesototal() {
		return pesototal;
	}
	public BigDecimal getPesobrutototal() {
		return pesobrutototal;
	}
	
	
	public void setQtdetotal(BigDecimal qtdetotal) {
		this.qtdetotal = qtdetotal;
	}
	public void setValottotal(Money valottotal) {
		this.valottotal = valottotal;
	}
	public void setPesototal(BigDecimal pesototal) {
		this.pesototal = pesototal;
	}
	public void setPesobrutototal(BigDecimal pesobrutototal) {
		this.pesobrutototal = pesobrutototal;
	}
}