package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;

@DisplayName("Gera��o de plaqueta")
public class GerarPlaquetaBean {

	protected List<Patrimonioitem> lista = new ListSet<Patrimonioitem>(Patrimonioitem.class);
	protected String ids;
	
	public List<Patrimonioitem> getLista() {
		return lista;
	}
	public String getIds() {
		return ids;
	}
	public void setLista(List<Patrimonioitem> lista) {
		this.lista = lista;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	
}
