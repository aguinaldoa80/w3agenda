package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.sined.geral.bean.Material;

public class Materialajustarpreco {
	
	protected Double percentual;
	protected Boolean calculo = Boolean.TRUE;
	protected String ncm;
	protected String pesoLiquido;
	protected String pesoBruto;
	protected Material mestreGrade;
	
	// TRANSIENTES
	protected String whereIn;
	protected Boolean entrada;

	
	@MinValue (0)
	@DisplayName ("Percentual")
	public Double getPercentual() {
		return percentual;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	
	public String getWhereIn() {
		return whereIn;
	}

	public Boolean getEntrada() {
		return entrada;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}
	
	@DisplayName("Op��o de C�lculo")
	public Boolean getCalculo() {
		return calculo;
	}
	
	public void setCalculo(Boolean calculo) {
		this.calculo = calculo;
	}

	@DisplayName ("NCM")
	public String getNcm() {
		return ncm;
	}

	public void setNcm(String ncm) {
		this.ncm = ncm;
	}

	public String getPesoLiquido() {
		return pesoLiquido;
	}

	public void setPesoLiquido(String pesoLiquido) {
		this.pesoLiquido = pesoLiquido;
	}

	public String getPesoBruto() {
		return pesoBruto;
	}

	public void setPesoBruto(String pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	@DisplayName ("Material Mestre da Grade")
	public Material getMestreGrade() {
		return mestreGrade;
	}

	public void setMestreGrade(Material mestreGrade) {
		this.mestreGrade = mestreGrade;
	}

}