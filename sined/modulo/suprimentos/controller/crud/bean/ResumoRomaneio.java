package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.util.ArrayList;
import java.util.List;

public class ResumoRomaneio {

	private List<ResumoMaterialRomaneio> listaResumoMaterialRomaneio = new ArrayList<ResumoMaterialRomaneio>();
	private Double qtdetotalenviada = 0.0;
	private Double qtdetotaldevolvida = 0.0;
	
	public List<ResumoMaterialRomaneio> getListaResumoMaterialRomaneio() {
		return listaResumoMaterialRomaneio;
	}
	public Double getQtdetotalenviada() {
		return qtdetotalenviada;
	}
	public Double getQtdetotaldevolvida() {
		return qtdetotaldevolvida;
	}
	public void setListaResumoMaterialRomaneio(
			List<ResumoMaterialRomaneio> listaResumoMaterialRomaneio) {
		this.listaResumoMaterialRomaneio = listaResumoMaterialRomaneio;
	}
	public void setQtdetotalenviada(Double qtdetotalenviada) {
		this.qtdetotalenviada = qtdetotalenviada;
	}
	public void setQtdetotaldevolvida(Double qtdetotaldevolvida) {
		this.qtdetotaldevolvida = qtdetotaldevolvida;
	}
	
}
