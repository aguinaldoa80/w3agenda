package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;


public class SubstituirLocacaoItemBean {
	
	private Contratomaterial contratomaterial;
	private Material material_substituido;
	private Patrimonioitem patrimonioitem_substituido;
	private Double qtde_substituido;
	private Localarmazenagem localarmazenagem_origem_substituido;
	private Localarmazenagem localarmazenagem_destino_substituido;
	private Material material_substituto;
	private Patrimonioitem patrimonioitem_substituto;
	private Double qtde_disponivel_substituto;
	private Localarmazenagem localarmazenagem_origem_substituto;
	private Localarmazenagem localarmazenagem_destino_substituto;
	private Boolean patrimonio;
	
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	public Material getMaterial_substituido() {
		return material_substituido;
	}
	public Patrimonioitem getPatrimonioitem_substituido() {
		return patrimonioitem_substituido;
	}
	public Double getQtde_substituido() {
		return qtde_substituido;
	}
	public Localarmazenagem getLocalarmazenagem_origem_substituido() {
		return localarmazenagem_origem_substituido;
	}
	public Localarmazenagem getLocalarmazenagem_destino_substituido() {
		return localarmazenagem_destino_substituido;
	}
	public Material getMaterial_substituto() {
		return material_substituto;
	}
	public Patrimonioitem getPatrimonioitem_substituto() {
		return patrimonioitem_substituto;
	}
	public Double getQtde_disponivel_substituto() {
		return qtde_disponivel_substituto;
	}
	public Localarmazenagem getLocalarmazenagem_origem_substituto() {
		return localarmazenagem_origem_substituto;
	}
	public Localarmazenagem getLocalarmazenagem_destino_substituto() {
		return localarmazenagem_destino_substituto;
	}
	public Boolean getPatrimonio() {
		return patrimonio;
	}
	public void setPatrimonio(Boolean patrimonio) {
		this.patrimonio = patrimonio;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
	public void setMaterial_substituido(Material materialSubstituido) {
		material_substituido = materialSubstituido;
	}
	public void setPatrimonioitem_substituido(
			Patrimonioitem patrimonioitemSubstituido) {
		patrimonioitem_substituido = patrimonioitemSubstituido;
	}
	public void setQtde_substituido(Double qtdeSubstituido) {
		qtde_substituido = qtdeSubstituido;
	}
	public void setLocalarmazenagem_origem_substituido(
			Localarmazenagem localarmazenagemOrigemSubstituido) {
		localarmazenagem_origem_substituido = localarmazenagemOrigemSubstituido;
	}
	public void setLocalarmazenagem_destino_substituido(
			Localarmazenagem localarmazenagemDestinoSubstituido) {
		localarmazenagem_destino_substituido = localarmazenagemDestinoSubstituido;
	}
	public void setMaterial_substituto(Material materialSubstituto) {
		material_substituto = materialSubstituto;
	}
	public void setPatrimonioitem_substituto(Patrimonioitem patrimonioitemSubstituto) {
		patrimonioitem_substituto = patrimonioitemSubstituto;
	}
	public void setQtde_disponivel_substituto(Double qtdeDisponivelSubstituto) {
		qtde_disponivel_substituto = qtdeDisponivelSubstituto;
	}
	public void setLocalarmazenagem_origem_substituto(
			Localarmazenagem localarmazenagemOrigemSubstituto) {
		localarmazenagem_origem_substituto = localarmazenagemOrigemSubstituto;
	}
	public void setLocalarmazenagem_destino_substituto(
			Localarmazenagem localarmazenagemDestinoSubstituto) {
		localarmazenagem_destino_substituto = localarmazenagemDestinoSubstituto;
	}
	
}
