package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;


public class SubstituirLocacaoBean {
	
	private String placa;
	private String motorista;
	private String observacao;
	private Date dtromaneio;
	private List<SubstituirLocacaoItemBean> listaSubstituirLocacaoItemBean = new ListSet<SubstituirLocacaoItemBean>(SubstituirLocacaoItemBean.class);
	
	public String getPlaca() {
		return placa;
	}
	public String getMotorista() {
		return motorista;
	}
	public String getObservacao() {
		return observacao;
	}
	public Date getDtromaneio() {
		return dtromaneio;
	}
	public List<SubstituirLocacaoItemBean> getListaSubstituirLocacaoItemBean() {
		return listaSubstituirLocacaoItemBean;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDtromaneio(Date dtromaneio) {
		this.dtromaneio = dtromaneio;
	}
	public void setListaSubstituirLocacaoItemBean(
			List<SubstituirLocacaoItemBean> listaSubstituirLocacaoItemBean) {
		this.listaSubstituirLocacaoItemBean = listaSubstituirLocacaoItemBean;
	}
	
}
