package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Documentotipo;

@DisplayName("Tipo de documento")
public class FaturamentoBean {

	protected Documentotipo documentotipo;
	protected String ids;	
	
	@Required
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	
	public String getIds() {
		return ids;
	}
	
	public void setIds(String ids) {
		this.ids = ids;
	}
	
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
}
