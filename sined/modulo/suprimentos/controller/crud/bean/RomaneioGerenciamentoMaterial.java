package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

public class RomaneioGerenciamentoMaterial {
	
	private Localarmazenagem localarmazenagemorigem;
	private Empresa empresaorigem;
	private Projeto projetoorigem;
	
	private Localarmazenagem localarmazenagemdestino;
	private Empresa empresadestino;
	private Projeto projetodestino;
	private Cliente clientedestino;
	
	private String whereInRequisicaomaterial;
	private String whereInContrato;
	private String observacao;
	private List<RomaneioGerenciamentoMaterialItem> listaMateriais;
	
	protected String placa;
	protected String motorista;
	protected Date dtromaneio = SinedDateUtils.currentDate();
	protected Boolean fromFechamento;
	protected Boolean atualizarPeriodolocacao;
	protected Boolean exibirAlertaAtualizacaoPeriodolocacao;
	protected Money valorreposicao;
	
	//transient
	protected Boolean isindenizacao;
	
	@Required
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagemdestino() {
		return localarmazenagemdestino;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public List<RomaneioGerenciamentoMaterialItem> getListaMateriais() {
		return listaMateriais;
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresadestino() {
		return empresadestino;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjetodestino() {
		return projetodestino;
	}
	
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagemorigem() {
		return localarmazenagemorigem;
	}

	@DisplayName("Empresa")
	public Empresa getEmpresaorigem() {
		return empresaorigem;
	}

	@DisplayName("Projeto")
	public Projeto getProjetoorigem() {
		return projetoorigem;
	}
	
	@DisplayName("Cliente")
	public Cliente getClientedestino() {
		return clientedestino;
	}
	
	public String getWhereInContrato() {
		return whereInContrato;
	}
	
	public Boolean getFromFechamento() {
		return fromFechamento;
	}
	
	public String getWhereInRequisicaomaterial() {
		return whereInRequisicaomaterial;
	}
	
	@DisplayName("Valor Reposi��o")
	public Money getValorreposicao() {
		return valorreposicao;
	}

	public void setValorreposicao(Money valorreposicao) {
		this.valorreposicao = valorreposicao;
	}

	public void setWhereInRequisicaomaterial(String whereInRequisicaomaterial) {
		this.whereInRequisicaomaterial = whereInRequisicaomaterial;
	}
	
	public void setFromFechamento(Boolean fromFechamento) {
		this.fromFechamento = fromFechamento;
	}
	
	public void setWhereInContrato(String whereInContrato) {
		this.whereInContrato = whereInContrato;
	}

	public void setLocalarmazenagemorigem(Localarmazenagem localarmazenagemorigem) {
		this.localarmazenagemorigem = localarmazenagemorigem;
	}

	public void setEmpresaorigem(Empresa empresaorigem) {
		this.empresaorigem = empresaorigem;
	}

	public void setProjetoorigem(Projeto projetoorigem) {
		this.projetoorigem = projetoorigem;
	}
	
	public void setClientedestino(Cliente clientedestino) {
		this.clientedestino = clientedestino;
	}

	public void setEmpresadestino(Empresa empresadestino) {
		this.empresadestino = empresadestino;
	}

	public void setProjetodestino(Projeto projetodestino) {
		this.projetodestino = projetodestino;
	}

	public void setListaMateriais(
			List<RomaneioGerenciamentoMaterialItem> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}
	
	public void setLocalarmazenagemdestino(Localarmazenagem localarmazenagemdestino) {
		this.localarmazenagemdestino = localarmazenagemdestino;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@MaxLength(8)
	@DisplayName("Placa")
	public String getPlaca() {
		return placa;
	}
	@MaxLength(100)
	@DisplayName("Motorista")
	public String getMotorista() {
		return motorista;
	}
	@DisplayName("Data do Romaneio")
	public Date getDtromaneio() {
		return dtromaneio;
	}
	public void setDtromaneio(Date dtromaneio) {
		this.dtromaneio = dtromaneio;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}
	
	public Boolean getAtualizarPeriodolocacao() {
		return atualizarPeriodolocacao;
	}
	public Boolean getExibirAlertaAtualizacaoPeriodolocacao() {
		return exibirAlertaAtualizacaoPeriodolocacao;
	}
	
	public void setExibirAlertaAtualizacaoPeriodolocacao(
			Boolean exibirAlertaAtualizacaoPeriodolocacao) {
		this.exibirAlertaAtualizacaoPeriodolocacao = exibirAlertaAtualizacaoPeriodolocacao;
	}
	public void setAtualizarPeriodolocacao(Boolean atualizarPeriodolocacao) {
		this.atualizarPeriodolocacao = atualizarPeriodolocacao;
	}
	
	@Transient
	public Boolean getIsindenizacao() {
		return isindenizacao;
	}
	
	public void setIsindenizacao(Boolean isindenizacao) {
		this.isindenizacao = isindenizacao;
	}
	
	public Boolean existeClassePatrimionio(){
		if(SinedUtil.isListNotEmpty(getListaMateriais())){
			for(RomaneioGerenciamentoMaterialItem item : getListaMateriais()){
				if(item.getMaterialclasse() != null && Materialclasse.PATRIMONIO.equals(item.getMaterialclasse())){
					return Boolean.TRUE;
				}
			}
		}
		return Boolean.FALSE;	
	}
}
