package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;

public class GerarRomaneioBaixarEntregaRomaneio {

	private Boolean marcado;
	private Material material;
	private Materialclasse materialclasse;
	private Double qtde;
	private Double qtdeOriginal;
	private Localarmazenagem localarmazenagemorigem;
	private Localarmazenagem localarmazenagemdestino;
	private Integer cdentrega;
	private Double qtdeEntregue;
	private Double qtdeRecebida;
	private Boolean zerarQtde;
	
	private Double qtdeSolicitada;
	private Boolean disponivel = Boolean.TRUE;
	
	public Boolean getMarcado() {
		return marcado;
	}
	public Material getMaterial() {
		return material;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getQtdeOriginal() {
		return qtdeOriginal;
	}
	public Localarmazenagem getLocalarmazenagemorigem() {
		return localarmazenagemorigem;
	}
	public Localarmazenagem getLocalarmazenagemdestino() {
		return localarmazenagemdestino;
	}
	public Integer getCdentrega() {
		return cdentrega;
	}
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setCdentrega(Integer cdentrega) {
		this.cdentrega = cdentrega;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setQtdeOriginal(Double qtdeOriginal) {
		this.qtdeOriginal = qtdeOriginal;
	}
	public void setLocalarmazenagemorigem(Localarmazenagem localarmazenagemorigem) {
		this.localarmazenagemorigem = localarmazenagemorigem;
	}
	public void setLocalarmazenagemdestino(Localarmazenagem localarmazenagemdestino) {
		this.localarmazenagemdestino = localarmazenagemdestino;
	}
	public Double getQtdeEntregue() {
		return qtdeEntregue;
	}
	public void setQtdeEntregue(Double qtdeEntregue) {
		this.qtdeEntregue = qtdeEntregue;
	}
	public Double getQtdeRecebida() {
		return qtdeRecebida;
	}
	public void setQtdeRecebida(Double qtdeRecebida) {
		this.qtdeRecebida = qtdeRecebida;
	}
	public Boolean getZerarQtde() {
		return zerarQtde;
	}
	public void setZerarQtde(Boolean zerarQtde) {
		this.zerarQtde = zerarQtde;
	}
	public Double getQtdeSolicitada() {
		return qtdeSolicitada;
	}
	public void setQtdeSolicitada(Double qtdeSolicitada) {
		this.qtdeSolicitada = qtdeSolicitada;
	}
	public Boolean getDisponivel() {
		return disponivel;
	}
	public void setDisponivel(Boolean disponivel) {
		this.disponivel = disponivel;
	}
}
