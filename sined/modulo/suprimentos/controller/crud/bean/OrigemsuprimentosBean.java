package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

public class OrigemsuprimentosBean {
	
	public static final Integer MATERIAL = 1;
	public static final Integer PROJETO = 2;
	public static final Integer USUARIO = 3;
	public static final Integer REQUISICAO = 4;
	public static final Integer ENTREGA = 5;
	public static final Integer CADASTRO = 6;
	public static final Integer COTACAO = 7;
	public static final Integer ORDEMCOMPRA = 8;
	public static final Integer DOCUMENTO = 9;
	public static final Integer MOVPATRIMONIO = 10;
	public static final Integer ENTRADASAIDA = 11;
	public static final Integer LISTAMATERIAL = 12;
	public static final Integer ROMANEIO = 13;
	public static final Integer NOTA = 14;
	public static final Integer VENDA = 15;
	public static final Integer PLANEJAMENTO = 16;
	public static final Integer TAREFA = 17;
	public static final Integer SOLICITACAOCOMPRA = 18;
	public static final Integer ORDEMSERVICO = 19;
	public static final Integer GERENCIAMENTOESTOQUE = 20;
	public static final Integer CONTRATO = 21;
	public static final Integer PRODUCAOAGENDA = 22;
	public static final Integer PRODUCAOORDEM = 23;
	public static final Integer EXPEDICAOPRODUCAO = 24;
	public static final Integer PATRIMONIOITEM = 25;
	public static final Integer PEDIDOVENDA = 26;
	public static final Integer ORDEMSERVICOVETERINARIA = 27;
	public static final Integer COLETA = 28;
	public static final Integer COLABORADOR = 29;
	public static final Integer EXPEDICAO = 30;
	public static final Integer MOVIMENTACAOESTOQUE = 31;
	public static final Integer DESPESAVEICULO = 32;
	public static final Integer INVENTARIO = 33;
	public static final Integer PEDIDOVENDA_OTR = 34;
	public static final Integer VENDA_OTR = 35;
	
	protected Integer typeorigem;
	protected String tipo;
	protected String descricao;
	protected String historico;
	protected String id;
	protected String identificador;
	protected String produto;
	protected String lote;
	protected Double qtde;
	
	public OrigemsuprimentosBean(){
	}
	
	public OrigemsuprimentosBean(Integer typeorigem, String descricao){
		this.typeorigem = typeorigem;
		this.descricao = descricao;
	}
	
	public OrigemsuprimentosBean(Integer typeorigem, String descricao, String id, String identificador){
		this.typeorigem = typeorigem;
		this.descricao = descricao;
		this.id = id;
		this.identificador = identificador;
	}
	
	public OrigemsuprimentosBean(Integer typeorigem, String tipo, String descricao, String id, String identificador){
		this.typeorigem = typeorigem;
		this.tipo = tipo;
		this.descricao = descricao;
		this.id = id;
		this.identificador = identificador;
	}
	
	public OrigemsuprimentosBean(String descricao, String id){
		this.descricao = descricao;
		this.id = id;
	}
	
	public OrigemsuprimentosBean(String descricao, String id, String historico){
		this.descricao = descricao;
		this.id = id;
		this.historico = historico;
	}
	
	public Integer getTypeorigem() {
		return typeorigem;
	}
	public String getTipo() {
		return tipo;
	}
	public String getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getHistorico() {
		return historico;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	public void setTypeorigem(Integer typeorigem) {
		this.typeorigem = typeorigem;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getIdentificador() {
		if(identificador == null || "".equals(identificador)){
			return id;
		}
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Double getQtde() {
		return qtde;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
}
