package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class EmitircurvaabcBean {
	
	private Integer classificacao; 			//sequencial por material
	private String material; 				//descricao do material
	private Double valorUnitario; 			//valor unit�rio do material
	private Double mediaUnitaria;			//total de venda dividido pela quantidade total vendida
	private Double qtde;					//quantidade total
	private Double valorConsumo;			//valor total
	private Double porcentagemAcumulado;	//%acumulado em rela�ao ao total de valor de consumo na listagem
	private Double porcentagemTotal;
	private Date dtinicio;
	private Integer cdEmpresa;
	private Integer cdProjeto;
	private Double peso;
	private Double percentualMarkup;
	private Double valorMarkup;
	private Double custoTotal;
	private Double totalValorBruto;
	private Double lucro;
	private Double totalDescontoItem;
	private Double totalValecompra;

	
	private String nomeMaterialPessoa;
	
	public EmitircurvaabcBean() {}
	
	public EmitircurvaabcBean(Integer classificacao, String material, Double valorUnitario, Double qtde, Double valorConsumo, Date dtinicio, Integer cdEmpresa, Integer cdProjeto, Double peso){
		this.classificacao = classificacao;
		this.nomeMaterialPessoa = material;
		this.material = material;
		this.valorUnitario = valorUnitario;
		this.qtde = qtde;
		this.valorConsumo = valorConsumo;
		this.mediaUnitaria = valorConsumo / qtde;
		this.dtinicio = dtinicio;
		this.cdEmpresa = cdEmpresa;
		this.cdProjeto = cdProjeto;
		this.peso = peso;
	}
	
	public EmitircurvaabcBean(Integer classificacao, String nomeMaterialPessoa, Double valorUnitario, Double qtde, Double valorConsumo){
		if(classificacao != null){
			this.classificacao = classificacao;
			this.material = nomeMaterialPessoa;
		}
		this.nomeMaterialPessoa = nomeMaterialPessoa;
		this.valorUnitario = valorUnitario;
		this.qtde = qtde;
		this.valorConsumo = valorConsumo;		
		this.mediaUnitaria = valorConsumo / qtde;
	}
	
	public EmitircurvaabcBean(Integer classificacao, String nomeMaterialPessoa, Double valorUnitario, Double qtde, Double valorConsumo, Double peso){
		if(classificacao != null){
			this.classificacao = classificacao;
			this.material = nomeMaterialPessoa;
		}
		this.nomeMaterialPessoa = nomeMaterialPessoa;
		this.valorUnitario = valorUnitario;
		this.qtde = qtde;
		this.valorConsumo = valorConsumo;		
		this.mediaUnitaria = valorConsumo / qtde;
		this.peso = peso;
	}
	
	  //*************//
	 //**Get & Set**//
	//*************//
	
	
	public Integer getClassificacao() {
		return classificacao;
	}
	public String getMaterial() {
		return material;
	}
	public Double getValorUnitario() {
		return valorUnitario;
	}
	public Double getMediaUnitaria() {
		if(mediaUnitaria == null && getValorConsumo() != null && getQtde() != null){
			return getValorConsumo() / getQtde();
		}
		return mediaUnitaria;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getValorConsumo() {
		return valorConsumo;
	}
	public Double getPorcentagemAcumulado() {
		return porcentagemAcumulado;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Integer getCdEmpresa() {
		return cdEmpresa;
	}
	public Integer getCdProjeto() {
		return cdProjeto;
	}
	public Double getPorcentagemTotal() {
		return porcentagemTotal;
	}
	public String getNomeMaterialPessoa() {
		return nomeMaterialPessoa;
	}
	public Double getPeso() {
		return peso;
	}
	public Double getPercentualMarkup() {
		if(percentualMarkup == null){
			percentualMarkup = 0d;
		}
		return percentualMarkup;
	}
	public Double getValorMarkup() {
		if(valorMarkup == null){
			valorMarkup = 0d;
		}
		return valorMarkup;
	}
	public Double getCustoTotal() {
		if(custoTotal == null){
			custoTotal = 0d;
		}
		return custoTotal;
	}
	public Double getTotalValorBruto() {
		if(totalValorBruto == null){
			totalValorBruto = 0d;
		}
		return totalValorBruto;
	}
	public Double getLucro() {
		if(lucro == null){
			lucro = 0d;
		}
		return lucro;
	}
	public Double getTotalDescontoItem() {
		if(totalDescontoItem == null){
			totalDescontoItem = 0d;
		}
		return totalDescontoItem;
	}
	public Double getTotalValecompra() {
		if(totalValecompra == null){
			totalValecompra = 0d;
		}
		return totalValecompra;
	}
	
	public void setPorcentagemTotal(Double porcentagemTotal) {
		this.porcentagemTotal = porcentagemTotal;
	}
	public void setClassificacao(Integer classificacao) {
		this.classificacao = classificacao;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	public void setMediaUnitaria(Double mediaUnitaria) {
		this.mediaUnitaria = mediaUnitaria;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setValorConsumo(Double valorConsumo) {
		this.valorConsumo = valorConsumo;
	}
	public void setPorcentagemAcumulado(Double porcentagemAcumulado) {
		this.porcentagemAcumulado = porcentagemAcumulado;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setCdEmpresa(Integer cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	public void setCdProjeto(Integer cdProjeto) {
		this.cdProjeto = cdProjeto;
	}
	public void setNomeMaterialPessoa(String nomeMaterialPessoa) {
		this.nomeMaterialPessoa = nomeMaterialPessoa;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public void setPercentualMarkup(Double percentualMarkup) {
		this.percentualMarkup = percentualMarkup;
	}
	public void setCustoTotal(Double custoTotal) {
		this.custoTotal = custoTotal;
	}
	public void setTotalValorBruto(Double totalValorBruto) {
		this.totalValorBruto = totalValorBruto;
	}
	public void setLucro(Double lucro) {
		this.lucro = lucro;
	}
	public void setTotalDescontoItem(Double totalDescontoItem) {
		this.totalDescontoItem = totalDescontoItem;
	}
	public void setTotalValecompra(Double totalValecompra) {
		this.totalValecompra = totalValecompra;
	}
	public void setValorMarkup(Double valorMarkup) {
		this.valorMarkup = valorMarkup;
	}

	public void addQtde(Double quantidade) {
		if(quantidade != null){
			setQtde((getQtde() != null ? getQtde() : 0d) + quantidade);
		}		
	}
	public void addValorConsumo(Double valor) {
		if(valor != null){
			setValorConsumo((getValorConsumo() != null ? getValorConsumo() : 0d) + valor);
		}
	}
	public void addPeso(Double peso){
		if(peso != null){
			setPeso((getPeso() != null ? getPeso() : 0d) + peso);
		}
	}
	
	public void addTotalValorBruto(Double valorBruto){
		if(valorBruto != null){
			setTotalValorBruto(getTotalValorBruto() + valorBruto);
		}
	}
	
	public void addCustoTotal(Double custoTotal){
		if(custoTotal != null){
			setCustoTotal(getCustoTotal() + custoTotal);
		}
	}
	
	public void addTotalDescontoItem(Money descontoItem){
		if(descontoItem != null){
			setTotalDescontoItem(getTotalDescontoItem() + descontoItem.getValue().doubleValue());
		}
	}
	
	public void addLucro(Double lucro){
		if(lucro != null){
			setLucro(getLucro() + lucro);
		}
	}
	
	public void addTotalValecompra(Money valorvalecompra){
		if(valorvalecompra != null){
			setTotalValecompra(getTotalValecompra() + valorvalecompra.getValue().doubleValue());
		}
	}
}
