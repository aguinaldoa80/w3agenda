package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;

@DisplayName("Escolha de fornecedores")
public class CotacaoBean {

	protected List<CotacaoFornecedorBean> listaFornecedor = new ListSet<CotacaoFornecedorBean>(CotacaoFornecedorBean.class);
	protected String ids;
	protected String controller;
	protected Localarmazenagem localunicoentrega;
	protected Empresa empresaunicaentrega;
	
	public List<CotacaoFornecedorBean> getListaFornecedor() {
		return listaFornecedor;
	}
	
	public String getIds() {
		return ids;
	}
	
	public String getController() {
		return controller;
	}
	
	public void setIds(String ids) {
		this.ids = ids;
	}
	
	public void setListaFornecedor(List<CotacaoFornecedorBean> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}
	
	public void setController(String controller) {
		this.controller = controller;
	}

	public Localarmazenagem getLocalunicoentrega() {
		return localunicoentrega;
	}

	public void setLocalunicoentrega(Localarmazenagem localunicoentrega) {
		this.localunicoentrega = localunicoentrega;
	}

	public Empresa getEmpresaunicaentrega() {
		return empresaunicaentrega;
	}

	public void setEmpresaunicaentrega(Empresa empresaunicaentrega) {
		this.empresaunicaentrega = empresaunicaentrega;
	}
}
