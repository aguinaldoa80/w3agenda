package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

public class ImportarDadosRequisicaomaterialBean {

	private String identificador;
	private String quantidadeStr;
	private String unidademedidaStr;
	private String qtdeVolume;
	
	private Integer linha;
	
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getQuantidadeStr() {
		return quantidadeStr;
	}
	public void setQuantidadeStr(String quantidadeStr) {
		this.quantidadeStr = quantidadeStr;
	}
	public String getUnidademedidaStr() {
		return unidademedidaStr;
	}
	public void setUnidademedidaStr(String unidademedidaStr) {
		this.unidademedidaStr = unidademedidaStr;
	}
	public String getQtdeVolume() {
		return qtdeVolume;
	}
	public void setQtdeVolume(String qtdeVolume) {
		this.qtdeVolume = qtdeVolume;
	}
	public Integer getLinha() {
		return linha;
	}
	public void setLinha(Integer linha) {
		this.linha = linha;
	}
	
	
	
}
