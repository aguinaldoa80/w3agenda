package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;



public class ImportacaoDadosOrdemcompraBean {

	private String identificador;
	private String quantidadeStr;
	private String unidademedidaStr;
	private String valorunitarioStr;
	private String percentualIcmsissStr;
	private String icmsissStr;
	private String percentualIpiStr;
	private String ipiStr;
	
	private Integer linha;
	private String erro;
	
	
	public String getIdentificador() {
		return identificador;
	}
	public String getQuantidadeStr() {
		return quantidadeStr;
	}
	public String getUnidademedidaStr() {
		return unidademedidaStr;
	}
	public String getValorunitarioStr() {
		return valorunitarioStr;
	}
	public String getPercentualIcmsissStr() {
		return percentualIcmsissStr;
	}
	public String getIcmsissStr() {
		return icmsissStr;
	}
	public String getPercentualIpiStr() {
		return percentualIpiStr;
	}
	public String getIpiStr() {
		return ipiStr;
	}
	public Integer getLinha() {
		return linha;
	}
	public String getErro() {
		return erro;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setQuantidadeStr(String quantidadeStr) {
		this.quantidadeStr = quantidadeStr;
	}
	public void setUnidademedidaStr(String unidademedidaStr) {
		this.unidademedidaStr = unidademedidaStr;
	}
	public void setValorunitarioStr(String valorunitarioStr) {
		this.valorunitarioStr = valorunitarioStr;
	}
	public void setPercentualIcmsissStr(String percentualIcmsissStr) {
		this.percentualIcmsissStr = percentualIcmsissStr;
	}
	public void setIcmsissStr(String icmsissStr) {
		this.icmsissStr = icmsissStr;
	}
	public void setPercentualIpiStr(String percentualIpiStr) {
		this.percentualIpiStr = percentualIpiStr;
	}
	public void setIpiStr(String ipiStr) {
		this.ipiStr = ipiStr;
	}
	public void setLinha(Integer linha) {
		this.linha = linha;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
}
