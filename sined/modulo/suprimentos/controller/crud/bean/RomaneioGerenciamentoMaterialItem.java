package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import java.util.List;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.LoteestoqueVO;

public class RomaneioGerenciamentoMaterialItem {
	
	private Localarmazenagem localarmazenagemorigem;
	private Localarmazenagem localarmazenagemdestino;
	private Projeto projetoorigem;
	private Projeto projetodestino;
	private Materialclasse materialclasse;
	private Material material;
	private Patrimonioitem patrimonioitem;
	private Double qtdeorigem;
	private Double qtdedestino;
	private Double valor;
	private Double qtderestante;
	private Double percentualdanificado = 100.00;
	private Loteestoque loteestoque;
	private Centrocusto centrocusto;
	
	private Contratomaterial contratomaterial;	
	private Requisicaomaterial requisicaomaterial;
	
	private Double qtdedestinoOriginal;
	protected List<LoteestoqueVO> listaLoteestoqueVO;
	
	@DisplayName("Local Origem")
	public Localarmazenagem getLocalarmazenagemorigem() {
		return localarmazenagemorigem;
	}
	
	@DisplayName("Local Destino")
	public Localarmazenagem getLocalarmazenagemdestino() {
		return localarmazenagemdestino;
	}
	
	public Projeto getProjetoorigem() {
		return projetoorigem;
	}

	@DisplayName("Projeto Destino")
	public Projeto getProjetodestino() {
		return projetodestino;
	}

	@DisplayName("Qtde. Origem")
	public Double getQtdeorigem() {
		return qtdeorigem;
	}
	
	@Required
	@DisplayName("Qtde. Destino")
	public Double getQtdedestino() {
		return qtdedestino;
	}
	
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	public Double getValor() {
		return valor;
	}
	
	@DisplayName("Qtde. Restante")
	public Double getQtderestante() {
		return qtderestante;
	}
	
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	
	public Requisicaomaterial getRequisicaomaterial() {
		return requisicaomaterial;
	}
	
	@DisplayName("Percentual Danificado")
	public Double getPercentualdanificado() {
		return percentualdanificado;
	}

	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	public void setPercentualdanificado(Double percentualdanificado) {
		this.percentualdanificado = percentualdanificado;
	}

	public void setRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		this.requisicaomaterial = requisicaomaterial;
	}
	
	public void setLocalarmazenagemdestino(
			Localarmazenagem localarmazenagemdestino) {
		this.localarmazenagemdestino = localarmazenagemdestino;
	}
	
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	
	public void setQtderestante(Double qtderestante) {
		this.qtderestante = qtderestante;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setLocalarmazenagemorigem(Localarmazenagem localarmazenagemorigem) {
		this.localarmazenagemorigem = localarmazenagemorigem;
	}
	public void setProjetoorigem(Projeto projetoorigem) {
		this.projetoorigem = projetoorigem;
	}
	public void setProjetodestino(Projeto projetodestino) {
		this.projetodestino = projetodestino;
	}
	public void setQtdeorigem(Double qtdeorigem) {
		this.qtdeorigem = qtdeorigem;
	}
	public void setQtdedestino(Double qtdedestino) {
		this.qtdedestino = qtdedestino;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public Double getQtdedestinoOriginal() {
		return qtdedestinoOriginal;
	}
	public void setQtdedestinoOriginal(Double qtdedestinoOriginal) {
		this.qtdedestinoOriginal = qtdedestinoOriginal;
	}

	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	@Transient
	public List<LoteestoqueVO> getListaLoteestoqueVO() {
		return listaLoteestoqueVO;
	}
	public void setListaLoteestoqueVO(List<LoteestoqueVO> listaLoteestoqueVO) {
		this.listaLoteestoqueVO = listaLoteestoqueVO;
	}


	public String getCdMaterialCdlocal(){
		return (getMaterial() != null && getMaterial().getCdmaterial() != null ? getMaterial().getCdmaterial().toString() : "null") +
				"-" +
				(getLocalarmazenagemorigem() != null && getLocalarmazenagemorigem().getCdlocalarmazenagem() != null ? getLocalarmazenagemorigem().getCdlocalarmazenagem().toString() : "null");
	}
}
