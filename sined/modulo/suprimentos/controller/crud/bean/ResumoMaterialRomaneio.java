package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;

public class ResumoMaterialRomaneio {

	private Material material;
	private Patrimonioitem patrimonioitem;
	private Money valor = new Money();
	private Double qtderemessa = 0.0;
	private Double qtdedevolvida = 0.0;
	private Double qtdesaldo = 0.0;
	private Money valortotal;
	
	public Material getMaterial() {
		return material;
	}
	public Money getValor() {
		return valor;
	}
	public Double getQtderemessa() {
		return qtderemessa;
	}
	public Double getQtdedevolvida() {
		return qtdedevolvida;
	}
	public Double getQtdesaldo() {
		Double saldo = 0.0;
		if(this.getQtderemessa() != null){
			saldo = this.getQtderemessa();
		}
		if(this.getQtdedevolvida() != null){
			saldo = saldo - this.getQtdedevolvida();
		}
		
		if(saldo != null) return saldo;	
		
		return qtdesaldo;
	}
	public Money getValortotal() {
		if(this.valor != null && this.getQtdesaldo() != null){
			return new Money(this.getValor().getValue().doubleValue()*this.getQtdesaldo());
		}
		return valortotal;
	}
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setQtderemessa(Double qtderemessa) {
		this.qtderemessa = qtderemessa;
	}
	public void setQtdedevolvida(Double qtdedevolvida) {
		this.qtdedevolvida = qtdedevolvida;
	}
	public void setQtdesaldo(Double qtdesaldo) {
		this.qtdesaldo = qtdesaldo;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	
	public boolean equals(ResumoMaterialRomaneio obj) {
		if (obj instanceof ResumoMaterialRomaneio) {
			ResumoMaterialRomaneio resumoMaterialRomaneio = (ResumoMaterialRomaneio) obj;
			return resumoMaterialRomaneio.getMaterial().equals(this.getMaterial());
		}
		return super.equals(obj);
	}
}
