package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;

public class GerarRomaneioBean {

	private Contratomaterial contratomaterial;
	private Requisicaomaterial requisicaomaterial;
	private Material material;
	private Localarmazenagem localarmazenagem;
	private Materialclasse materialclasse;
	private Patrimonioitem patrimonioitem;
	private Double qtde;
	private Double qtdeRestante;
	private Loteestoque loteestoque;
	private Projeto projeto;
	private Empresa empresa;
	
	private Boolean exibirAlertaAtualizacaoPeriodolocacao;
	private Double qtdedestinoOriginal;
	
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	public Material getMaterial() {
		return material;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getQtdeRestante() {
		return qtdeRestante;
	}
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	public Requisicaomaterial getRequisicaomaterial() {
		return requisicaomaterial;
	}
	public void setRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		this.requisicaomaterial = requisicaomaterial;
	}
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	public void setQtdeRestante(Double qtdeRestante) {
		this.qtdeRestante = qtdeRestante;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}	
	
	public Boolean getExibirAlertaAtualizacaoPeriodolocacao() {
		return exibirAlertaAtualizacaoPeriodolocacao;
	}
	public void setExibirAlertaAtualizacaoPeriodolocacao(
			Boolean exibirAlertaAtualizacaoPeriodolocacao) {
		this.exibirAlertaAtualizacaoPeriodolocacao = exibirAlertaAtualizacaoPeriodolocacao;
	}
	
	public Double getQtdedestinoOriginal() {
		return qtdedestinoOriginal;
	}
	public void setQtdedestinoOriginal(Double qtdedestinoOriginal) {
		this.qtdedestinoOriginal = qtdedestinoOriginal;
	}
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
