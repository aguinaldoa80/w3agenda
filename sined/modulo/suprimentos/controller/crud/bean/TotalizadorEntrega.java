package br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean;

import br.com.linkcom.neo.types.Money;

public class TotalizadorEntrega {

	private Money totalAberto = new Money(0);	
	private Money totalFaturado = new Money(0);	
	private Money totalBaixado = new Money(0);	
	private Money totalCancelado = new Money(0);
	
	public Money getTotalAberto() {
		return totalAberto;
	}
	public Money getTotalFaturado() {
		return totalFaturado;
	}
	public Money getTotalBaixado() {
		return totalBaixado;
	}
	public Money getTotalCancelado() {
		return totalCancelado;
	}
	public void setTotalAberto(Money totalAberto) {
		this.totalAberto = totalAberto;
	}
	public void setTotalFaturado(Money totalFaturado) {
		this.totalFaturado = totalFaturado;
	}
	public void setTotalBaixado(Money totalBaixado) {
		this.totalBaixado = totalBaixado;
	}
	public void setTotalCancelado(Money totalCancelado) {
		this.totalCancelado = totalCancelado;
	}	
	
}