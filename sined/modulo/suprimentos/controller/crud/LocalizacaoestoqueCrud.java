package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Localizacaoestoque;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path="/sistema/crud/Localizacaoestoque", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class LocalizacaoestoqueCrud extends CrudControllerSined<FiltroListagemSined, Localizacaoestoque, Localizacaoestoque>{
	
	@Override
	protected void salvar(WebRequestContext request, Localizacaoestoque bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
//			if (DatabaseError.isKeyPresent(e, "IDX_UNIDADEMEDIDA_NOME")) {
//				throw new SinedException("Cor j� cadastrada no sistema.");
//			}
		}
	}
	
}
