package br.com.linkcom.sined.modulo.suprimentos.controller.crud;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PessoaContatoService;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path={"/suprimento/crud/Contato", "/crm/crud/Contato", "/servicointerno/crud/Contato"}, authorizationModule=CrudAuthorizationModule.class)
public class ContatoCrud extends CrudControllerSined<FiltroListagemSined, Contato, Contato>{
	
	private ParametrogeralService parametrogeralService;
	private PessoaContatoService pessoaContatoService;
	private ContatoService contatoService;
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {this.pessoaContatoService = pessoaContatoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	
	@Override
	protected void entrada(WebRequestContext request, Contato form)	throws Exception {
		//Pega o codigo do fornecedor que � passado como parametro no request
		String fornecedor = request.getParameter("fornecedor.cdpessoa");
		String cliente = request.getParameter("cliente.cdpessoa");
		if(fornecedor != null){
			form.setPessoa(new Pessoa(new Integer(fornecedor)));
		}else if (cliente != null){
			try{
				try {
					form.setPessoa(new Pessoa(new Integer(cliente)));
				} catch (Exception e) {
					String cdcliente = StringUtils.substringBetween(cliente, "=", ",n");
					form.setPessoa(new Pessoa(new Integer(cdcliente)));
				}
			} catch (Exception e) {
				String cdcliente = StringUtils.substringBetween(cliente, "=", "]");
				form.setPessoa(new Pessoa(new Integer(cdcliente)));
			}
		}
		boolean obrigaCamposArvoreAcesso = StringUtils.isNotBlank(parametrogeralService.buscaValorPorNome(Parametrogeral.URL_SINC_CONTRATO_ARVOREACESSO));
		request.setAttribute("ObrigaCamposArvoreAcesso", obrigaCamposArvoreAcesso );		
		super.entrada(request, form);
	}
	@Override
	protected void salvar(WebRequestContext request, Contato bean)throws Exception {
		if(bean.getListaEndereco()!= null && bean.getListaEndereco().size() > 0){
			for (Endereco item : bean.getListaEndereco()) {
				if(item.getMunicipio() != null && item.getMunicipio().getNome() == null && item.getMunicipio().getUf() == null){
					item.setMunicipio(null);
				}
			}
		}
		
		bean.setListaContato(null);
		bean.setListaPessoa(null);
		
		if (bean.getPessoa() != null) {
			contatoService.saveOrUpdate(bean);
			
			PessoaContato pessoaContato = new PessoaContato();
			pessoaContato.setContato(bean);
			pessoaContato.setPessoa(bean.getPessoa());
			
			pessoaContatoService.saveOrUpdate(pessoaContato);
		} else {
			super.salvar(request, bean);
		}
	}
}
