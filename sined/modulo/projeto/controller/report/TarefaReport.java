package br.com.linkcom.sined.modulo.projeto.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.TarefaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/projeto/relatorio/Tarefa", authorizationModule=ReportAuthorizationModule.class)
public class TarefaReport extends SinedReport<TarefaFiltro> {
	
	private TarefaService tarefaService;
	
	public void setTarefaService(TarefaService tarefaService) {this.tarefaService = tarefaService;}
	
	@Override
	public IReport createReportSined(WebRequestContext request,TarefaFiltro filtro) throws Exception {
		return tarefaService.createTarefaReport(filtro);
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, TarefaFiltro filtro) throws ResourceGenerationException {
		return new ModelAndView("redirect:/projeto/crud/Tarefa?ACAO=" + AbstractCrudController.LISTAGEM);
	}
	@Override
	public String getNomeArquivo() {
		return "tarefa";
	}

	@Override
	public String getTitulo(TarefaFiltro filtro) {
		return "TAREFA";
	}

}
