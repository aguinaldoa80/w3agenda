package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.AcompanhamentoProjetoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = {"/projeto/relatorio/AcompanhamentoProjeto", "/agro/relatorio/AcompanhamentoProjeto"},
	authorizationModule=ReportAuthorizationModule.class
)
public class AcompanhamentoProjetoReport extends SinedReport<AcompanhamentoProjetoFiltro> {

	private PlanejamentoService planejamentoService;
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, AcompanhamentoProjetoFiltro filtro) throws Exception {
		return planejamentoService.createAcompanhamentoProjetoReport(filtro);
	}
	
	@Override
	public String getTitulo(AcompanhamentoProjetoFiltro filtro) {
		return "RELATÓRIO DE ACOMPANHAMENTO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, AcompanhamentoProjetoFiltro filtro) {
		
	}
	
	@Override
	public String getNomeArquivo() {
		return "acompanhamento";
	}
}
