package br.com.linkcom.sined.modulo.projeto.controller.report.filter;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;

public class ComparativoTarefaReportFiltro {
	
	protected Projeto projeto;
	protected Planejamento planejamento;
	
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}

}
