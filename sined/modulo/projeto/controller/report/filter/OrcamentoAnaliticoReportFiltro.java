package br.com.linkcom.sined.modulo.projeto.controller.report.filter;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Fornecimentotipo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;

public class OrcamentoAnaliticoReportFiltro {
	
	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Boolean material = true;
	protected Boolean funcao = true;
	protected Boolean despesa = true;
	protected List<Material> listaMaterial = new ArrayList<Material>();
	protected List<Cargo> listaCargo = new ArrayList<Cargo>();
	protected List<Fornecimentotipo> listaDespesa = new ArrayList<Fornecimentotipo>();
	protected Boolean sintetico = true;
		
	public Projeto getProjeto() {
		return projeto;
	}
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public Boolean getMaterial() {
		return material;
	}
	public Boolean getFuncao() {
		return funcao;
	}
	public List<Material> getListaMaterial() {
		return listaMaterial;
	}
	public List<Cargo> getListaCargo() {
		return listaCargo;
	}
	public Boolean getDespesa() {
		return despesa;
	}
	public List<Fornecimentotipo> getListaDespesa() {
		return listaDespesa;
	}
	public Boolean getSintetico() {
		return sintetico;
	}
	public void setSintetico(Boolean sintetico) {
		this.sintetico = sintetico;
	}
	public void setDespesa(Boolean despesa) {
		this.despesa = despesa;
	}
	public void setListaDespesa(List<Fornecimentotipo> listaDespesa) {
		this.listaDespesa = listaDespesa;
	}
	public void setListaMaterial(List<Material> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	public void setListaCargo(List<Cargo> listaCargo) {
		this.listaCargo = listaCargo;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setMaterial(Boolean material) {
		this.material = material;
	}
	public void setFuncao(Boolean funcao) {
		this.funcao = funcao;
	}
}
