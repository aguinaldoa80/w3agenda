package br.com.linkcom.sined.modulo.projeto.controller.report.filter;

import br.com.linkcom.sined.geral.bean.Orcamento;

public class TarefaOrcamentoReportFiltro {
	
	protected Orcamento orcamento;
	protected Boolean precovenda;
	protected Boolean precocusto;
	protected Boolean incluirrg;
	protected Boolean incluirrh;
	
	
	public Orcamento getOrcamento() {
		return orcamento;
	}
	public Boolean getPrecocusto() {
		return precocusto;
	}
	public Boolean getIncluirrg() {
		return incluirrg;
	}
	public Boolean getIncluirrh() {
		return incluirrh;
	}
	public Boolean getPrecovenda() {
		return precovenda;
	}
	public void setPrecovenda(Boolean precovenda) {
		this.precovenda = precovenda;
	}
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	public void setPrecocusto(Boolean precocusto) {
		this.precocusto = precocusto;
	}
	public void setIncluirrg(Boolean incluirrg) {
		this.incluirrg = incluirrg;
	}
	public void setIncluirrh(Boolean incluirrh) {
		this.incluirrh = incluirrh;
	}
	
}
