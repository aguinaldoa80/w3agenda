package br.com.linkcom.sined.modulo.projeto.controller.report.filter;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Indice;

public class IndiceReportFiltro {
	
	protected Indice indice;

	@Required
	public Indice getIndice() {
		return indice;
	}

	public void setIndice(Indice indice) {
		this.indice = indice;
	}
	
	
}
