package br.com.linkcom.sined.modulo.projeto.controller.report.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.SinedDateUtils;

public class AcompanhamentoEconomicoReportFiltro {

	private Date dtFim = SinedDateUtils.currentDate();
	private Projeto projeto;
	private Boolean considerarFrequenciaOC;
	
	private Money valorTotalOrcamento;
	private Money valorTotalRealizado;
	
	@Required
	@DisplayName("Considerar at�")
	public Date getDtFim() {
		return dtFim;
	}
	
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	
	public Money getValorTotalOrcamento() {
		return valorTotalOrcamento;
	}
	
	public Money getValorTotalRealizado() {
		return valorTotalRealizado;
	}
	
	public Boolean getConsiderarFrequenciaOC() {
		return considerarFrequenciaOC;
	}
	
	public void setConsiderarFrequenciaOC(Boolean considerarFrequenciaOC) {
		this.considerarFrequenciaOC = considerarFrequenciaOC;
	}
	
	public void setValorTotalOrcamento(Money valorTotalOrcamento) {
		this.valorTotalOrcamento = valorTotalOrcamento;
	}
	
	public void setValorTotalRealizado(Money valorTotalRealizado) {
		this.valorTotalRealizado = valorTotalRealizado;
	}
	
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
}
