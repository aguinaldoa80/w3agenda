package br.com.linkcom.sined.modulo.projeto.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoTarefaFiltro;


@Bean
@Controller(
	path = {"/projeto/relatorio/AcompanhamentoTarefas","/agro/relatorio/AcompanhamentoTarefas"},
	authorizationModule=ReportAuthorizationModule.class
)
public class AcompanhamentoTarefasExcelProcess extends ResourceSenderController<AcompanhamentoTarefaFiltro> {

	private TarefaService tarefaService;
	
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	AcompanhamentoTarefaFiltro filtro) throws Exception {
		return tarefaService.preparaArquivoAcompanhamentoTarefaExcel(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, AcompanhamentoTarefaFiltro filtro) throws Exception {
		return new ModelAndView("relatorio/acompanhamentoTarefas").addObject("filtro", filtro);
	}
	
}