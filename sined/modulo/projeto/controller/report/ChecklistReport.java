package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.TarefaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/projeto/relatorio/Checklist", authorizationModule=ReportAuthorizationModule.class)
public class ChecklistReport extends SinedReport<TarefaFiltro>{

	private TarefaService tarefaService;
	
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,
			TarefaFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		return tarefaService.createReportTarefaChecklist(whereIn);
	}

	@Override
	public String getNomeArquivo() {
		return "ckecklist";
	}

	@Override
	public String getTitulo(TarefaFiltro filtro) {
		return "RELATÓRIO CHECKLIST DE TAREFAS";
	}
	

}
