package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.service.BdiService;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/projeto/relatorio/Resumoorcamento",
	authorizationModule=ReportAuthorizationModule.class
)
public class ResumoOrcamentoReport extends SinedReport<Orcamento> {

	private BdiService bdiService;
	
	public void setBdiService(BdiService bdiService) {
		this.bdiService = bdiService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, Orcamento orcamento) throws Exception {
		return bdiService.createReportResumoGeralOrcamento(orcamento);
	}
	
	@Override
	public String getTitulo(Orcamento orcamento) {
		return "Resumo Geral do Or�amento";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, Orcamento orcamento) {
		
	}
	
	@Override
	public String getNomeArquivo() {
		return "resumoorcamento";
	}
}
