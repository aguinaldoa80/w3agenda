package br.com.linkcom.sined.modulo.projeto.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = {"/projeto/relatorio/OrcamentoRel"},
	authorizationModule=ReportAuthorizationModule.class
)
public class OrcamentoAnaliticoReport extends SinedReport<OrcamentoAnaliticoReportFiltro> {

	private PlanejamentoService planejamentoService;
	private ProjetoService projetoService;
	
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, OrcamentoAnaliticoReportFiltro filtro) throws Exception {
		return planejamentoService.createReportOrcamentoAnalitico(filtro);
	}
	
	@Override
	public String getTitulo(OrcamentoAnaliticoReportFiltro filtro) {
		return "RELAT�RIO DE OR�AMENTO ANAL�TICO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, OrcamentoAnaliticoReportFiltro filtro) {
		report.addParameter("PLANEJAMENTO", planejamentoService.load(filtro.getPlanejamento(), "planejamento.descricao").getDescricao());
		report.addParameter("PROJETO", projetoService.load(filtro.getProjeto(), "projeto.nome").getNome());
	}
	
	@Override
	protected void filtro(WebRequestContext request, OrcamentoAnaliticoReportFiltro filtro) throws Exception {
		if(filtro.getPlanejamento() != null){
			request.setAttribute("listaCargo", planejamentoService.findCargos(filtro.getPlanejamento()));
			request.setAttribute("listaMaterial", planejamentoService.findMateriais(filtro.getPlanejamento()));
			request.setAttribute("listaDespesa", planejamentoService.findDespesa(filtro.getPlanejamento()));
		}
		super.filtro(request, filtro);
	}
	
	@Action("listar")
	public ModelAndView getListas(WebRequestContext request,OrcamentoAnaliticoReportFiltro filtro) throws Exception {
		request.setAttribute("listaCargo", planejamentoService.findCargos(filtro.getPlanejamento()));
		request.setAttribute("listaMaterial", planejamentoService.findMateriais(filtro.getPlanejamento()));
		request.setAttribute("listaDespesa", planejamentoService.findDespesa(filtro.getPlanejamento()));
		request.setAttribute("filtro", filtro);
		return getFiltroModelAndView(request, filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "orcamentoanalitico";
	}
}
