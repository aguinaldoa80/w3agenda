package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.service.BdiService;
import br.com.linkcom.sined.geral.service.OrcamentoService;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/projeto/relatorio/Bdi",
	authorizationModule=ReportAuthorizationModule.class
)
public class BdiReport extends SinedReport<Orcamento> {

	private BdiService bdiService;
	private OrcamentoService orcamentoService;
	
	public void setBdiService(BdiService bdiService) {
		this.bdiService = bdiService;
	}
	
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, Orcamento orcamento) throws Exception {
		return bdiService.createReportBdi(orcamento);
	}
	
	@Override
	public String getTitulo(Orcamento orcamento) {
		orcamento = orcamentoService.loadWithCliente(orcamento);
		return "BDI - " + orcamento.getNome() + (orcamento.getCliente() != null ? " / " + orcamento.getCliente().getNome() : "");
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, Orcamento orcamento) {
		
	}
	
	@Override
	public String getNomeArquivo() {
		return "bdi";
	}
}
