package br.com.linkcom.sined.modulo.projeto.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.AtividadeService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.AtividadeFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/projeto/relatorio/Atividade", authorizationModule=ReportAuthorizationModule.class)
public class AtividadeReport extends SinedReport<AtividadeFiltro>{

	private AtividadeService atividadeService;
	public void setAtividadeService(AtividadeService atividadeService) {
		this.atividadeService = atividadeService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	AtividadeFiltro filtro) throws Exception {
		return atividadeService.geraReportAtividade(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, AtividadeFiltro filtro) throws ResourceGenerationException {
		return new ModelAndView("redirect:/projeto/crud/Atividade?ACAO=" + AbstractCrudController.LISTAGEM);
	}
	
	@Override
	public String getNomeArquivo() {
		return "atividade";
	}

	@Override
	public String getTitulo(AtividadeFiltro filtro) {
		return "Atividade";
	}
	
}
