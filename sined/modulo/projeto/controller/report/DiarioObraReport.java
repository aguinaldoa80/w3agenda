package br.com.linkcom.sined.modulo.projeto.controller.report;

import java.util.LinkedList;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.DiarioobraService;

@Bean
@Controller(path = "/projeto/relatorio/Diarioobra", authorizationModule=ReportAuthorizationModule.class)
public class DiarioObraReport extends ReportTemplateController<ReportTemplateFiltro>{

	private DiarioobraService diarioobraService;
	
	public void setDiarioobraService(DiarioobraService diarioobraService) {
		this.diarioobraService = diarioobraService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_DIARIO_OBRA;
	}

	@Override
	protected String getFileName() {
		return "diarioobra";
	}
	
	@Override
	protected LinkedList<ReportTemplateBean> carregaListaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		return diarioobraService.createReportDiarioObraConfiguravel(request);
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request, ReportTemplateFiltro filtro) {
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_DIARIO_OBRA);
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/projeto/crud/Diarioobra");
	}

}
