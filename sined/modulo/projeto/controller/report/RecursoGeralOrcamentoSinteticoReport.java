package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.RecursocomposicaoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoGeralFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Controller(path = "/projeto/relatorio/RecursoGeralOrcamentoSintetico",	authorizationModule=ReportAuthorizationModule.class)
public class RecursoGeralOrcamentoSinteticoReport extends SinedReport<OrcamentoRecursoGeralFiltro> {

	private RecursocomposicaoService recursocomposicaoService;
	
	public void setRecursocomposicaoService(
			RecursocomposicaoService recursocomposicaoService) {
		this.recursocomposicaoService = recursocomposicaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, OrcamentoRecursoGeralFiltro filtro) throws Exception {
		filtro = (OrcamentoRecursoGeralFiltro)NeoWeb.getRequestContext().getSession().getAttribute(RecursocomposicaoService.SESSION_FILTRO_RELATORIO_RECGERAL_SINTETICO);
		NeoWeb.getRequestContext().getSession().removeAttribute(RecursocomposicaoService.SESSION_FILTRO_RELATORIO_RECGERAL_SINTETICO);
		
		return recursocomposicaoService.gerarRelatorioRecursoGeralOrcamentoSintetico(filtro);
	}
	
	@Override
	public String getTitulo(OrcamentoRecursoGeralFiltro filtro) {
		return "RELAT�RIO DE RECURSO GERAL DO OR�AMENTO";
	}
	
	@Override
	public String getNomeArquivo() {
		return "recursogeralorcamento";
	}
}
