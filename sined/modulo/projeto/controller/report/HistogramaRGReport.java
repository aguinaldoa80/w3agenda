package br.com.linkcom.sined.modulo.projeto.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.HistogramaRGFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/projeto/relatorio/HistogramaRG",
	authorizationModule=ReportAuthorizationModule.class
)
public class HistogramaRGReport extends SinedReport<HistogramaRGFiltro> {

	private PlanejamentoService planejamentoService;
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, HistogramaRGFiltro filtro) throws Exception {
		if ("pdf".equals(request.getParameter("tipo"))){
			return super.doGerar(request, filtro);
		}else {
			return planejamentoService.createHistogramaCSV(request, filtro, "rg");
		}
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, HistogramaRGFiltro filtro) throws Exception {
		if(!planejamentoService.materialProjeto(filtro.getMaterial(), filtro.getPlanejamento())){
			throw new SinedException("O material n�o pertence ao planejamento escolhido.");
		}
		return planejamentoService.createHistogramaRGReport(filtro);
	}
	
	@Override
	public String getTitulo(HistogramaRGFiltro filtro) {
		return "HISTOGRAMA DE RECURSOS GERAIS";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, HistogramaRGFiltro filtro) {
		
	}
	
	@Override
	public String getNomeArquivo() {
		return "histogramarg";
	}
}
