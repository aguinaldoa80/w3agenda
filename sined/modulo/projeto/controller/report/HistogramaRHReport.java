package br.com.linkcom.sined.modulo.projeto.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.HistogramaRHFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/projeto/relatorio/HistogramaRH",
	authorizationModule=ReportAuthorizationModule.class
)
public class HistogramaRHReport extends SinedReport<HistogramaRHFiltro> {

	private PlanejamentoService planejamentoService;
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, HistogramaRHFiltro filtro) throws Exception {
		if ("pdf".equals(request.getParameter("tipo"))){
			return super.doGerar(request, filtro);
		}else {
			return planejamentoService.createHistogramaCSV(request, filtro, "rh");
		}
	}
	
	@Override
	protected void filtro(WebRequestContext request, HistogramaRHFiltro filtro) throws Exception {
		if(filtro.getPlanejamento() != null){
			request.setAttribute("listaCargo", planejamentoService.findCargos(filtro.getPlanejamento()));
		}
		super.filtro(request, filtro);
	}
	
	@Action("listar")
	public ModelAndView getListaCargos(WebRequestContext request,HistogramaRHFiltro filtro) throws Exception {
		request.setAttribute("listaCargo", planejamentoService.findCargos(filtro.getPlanejamento()));
		request.setAttribute("filtro", filtro);
		return getFiltroModelAndView(request, filtro);
	}
	@Override
	public IReport createReportSined(WebRequestContext request, HistogramaRHFiltro filtro) throws Exception {
		return planejamentoService.createHistogramaRHReport(filtro);
	}
	
	@Override
	public String getTitulo(HistogramaRHFiltro filtro) {
		return "HISTOGRAMA DE RECURSOS HUMANOS";
	}
	
	@Override
	public String getNomeArquivo() {
		return "histogramarh";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, HistogramaRHFiltro filtro) {
		
	}
}
