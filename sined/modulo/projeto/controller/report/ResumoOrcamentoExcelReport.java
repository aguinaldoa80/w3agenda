package br.com.linkcom.sined.modulo.projeto.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.service.BdiService;

@Controller(path = "/projeto/relatorio/ResumoorcamentoExcel", authorizationModule=ReportAuthorizationModule.class)
public class ResumoOrcamentoExcelReport extends ResourceSenderController<Orcamento>{

	private BdiService bdiService;

	public void setBdiService(BdiService bdiService) {
		this.bdiService = bdiService;
	}

	@Override
	public Resource generateResource(WebRequestContext request,	Orcamento orcamento) throws Exception {
		return bdiService.createReportResumoGeralExcelOrcamento(orcamento);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Orcamento filtro) throws Exception {
		return null;
	}
}
