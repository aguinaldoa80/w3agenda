package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.service.OrcamentoService;
import br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoHumanoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/projeto/relatorio/Gastomdo",
	authorizationModule=ReportAuthorizationModule.class
)
public class GastoMDOReport extends SinedReport<OrcamentoRecursoHumanoFiltro> {

	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private OrcamentoService orcamentoService;
	
	public void setOrcamentorecursohumanoService(OrcamentorecursohumanoService orcamentorecursohumanoService) {
		this.orcamentorecursohumanoService = orcamentorecursohumanoService;
	}
	
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, OrcamentoRecursoHumanoFiltro filtro) throws Exception {		
		filtro = (OrcamentoRecursoHumanoFiltro) NeoWeb.getRequestContext().getSession().getAttribute(OrcamentorecursohumanoService.SESSION_FILTRO_RELATORIO_RH);
		return orcamentorecursohumanoService.createReportGastoMDO(filtro);
	}
	
	@Override
	public String getTitulo(OrcamentoRecursoHumanoFiltro filtro) {
		filtro = (OrcamentoRecursoHumanoFiltro) NeoWeb.getRequestContext().getSession().getAttribute(OrcamentorecursohumanoService.SESSION_FILTRO_RELATORIO_RH);
		NeoWeb.getRequestContext().getSession().removeAttribute(OrcamentorecursohumanoService.SESSION_FILTRO_RELATORIO_RH);
		Orcamento orcamento = orcamentoService.loadForEntrada(filtro.getOrcamento());
		return "Gasto Com M�o-de-Obra - " + orcamento.getNome() + (orcamento.getCliente() != null ? " / " + orcamento.getCliente().getNome() : "");
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, OrcamentoRecursoHumanoFiltro filtro) {
		
	}
	
	@Override
	public String getNomeArquivo() {
		return "gastomdo";
	}
}
