package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/projeto/relatorio/OrcamentoSintetico",
	authorizationModule=ReportAuthorizationModule.class
)
public class OrcamentoSinteticoReport extends SinedReport<OrcamentoAnaliticoReportFiltro> {

	private PlanejamentoService planejamentoService;
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, OrcamentoAnaliticoReportFiltro filtro) throws Exception {
		return planejamentoService.createReport(filtro);
	}
	
	@Override
	public String getTitulo(OrcamentoAnaliticoReportFiltro filtro) {
		return "RELAT�RIO DE OR�AMENTO SINT�TICO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, OrcamentoAnaliticoReportFiltro filtro) {
	}
	
	@Override
	public String getNomeArquivo() {
		return "orcamentosintetico";
	}
}
