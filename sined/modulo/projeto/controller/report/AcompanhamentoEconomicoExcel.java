package br.com.linkcom.sined.modulo.projeto.controller.report;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetodespesa;
import br.com.linkcom.sined.geral.dao.ApontamentoDAO;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.ProjetodespesaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ApontamentoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoEconomicoListagemBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoEconomicoListagemDetalheBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoEconomicoReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;


@Controller(path = {"/projeto/relatorio/AcompanhamentoEconomico","/agro/relatorio/AcompanhamentoEconomico"}, authorizationModule=ReportAuthorizationModule.class)
public class AcompanhamentoEconomicoExcel extends ResourceSenderController<AcompanhamentoEconomicoReportFiltro>{

	private ProjetoService projetoService;
	private ProjetodespesaService projetodespesaService;
	private ApontamentoDAO apontamentoDAO;
	private ContagerencialService contagerencialService;
	
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setApontamentoDAO(ApontamentoDAO apontamentoDAO) {
		this.apontamentoDAO = apontamentoDAO;
	}
	public void setProjetodespesaService(
			ProjetodespesaService projetodespesaService) {
		this.projetodespesaService = projetodespesaService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	AcompanhamentoEconomicoReportFiltro filtro) throws Exception {
		return projetoService.gerarAcompanhamentoEconomicoExcel(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	AcompanhamentoEconomicoReportFiltro filtro) throws Exception {
		return new ModelAndView("relatorio/acompanhamentoEconomico").addObject("filtro", filtro);
	}
	
	/**
	 * Action que busca a lista de despesa do projeto selecionado no filtro e exibe na tela.
	 *
	 * @see br.com.linkcom.sined.geral.service.ProjetodespesaService#findByProjeto
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#getTotalRhMinutos
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#getTotalRealizado
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @since 13/10/2011
	 * @author Rodrigo Freitas
	 */
	public ModelAndView visualizarDespesasTela(WebRequestContext request, AcompanhamentoEconomicoReportFiltro filtro){
		List<Projetodespesa> lista = projetodespesaService.findByProjetoData(filtro.getProjeto(), filtro.getDtFim());
		
		if (lista != null){
			
			List<AcompanhamentoEconomicoListagemBean> listaBean = new ArrayList<AcompanhamentoEconomicoListagemBean>();
			AcompanhamentoEconomicoListagemBean bean;
			
			ApontamentoFiltro apontamentoFiltro = new ApontamentoFiltro();
			apontamentoFiltro.setProjeto(filtro.getProjeto());
			apontamentoFiltro.setDtapontamentoAte(filtro.getDtFim());
			apontamentoFiltro.setApontamentoTipo(ApontamentoTipo.FUNCAO_HORA);
			long totalRh = apontamentoDAO.getTotalRhMinutos(apontamentoFiltro )/60;
			
			Money realizado = new Money();
			Money orcamento = new Money();
			
			for (Projetodespesa despesa : lista){
				bean = new AcompanhamentoEconomicoListagemBean();
				
				bean.setContagerencial(despesa.getContagerencial());
				bean.setValorTotalOrcamento(despesa.getValortotal());
				bean.setValorHoraOrcamento(despesa.getValorhora());
				
				Money totalRealizado = projetoService.getTotalRealizado(filtro, despesa.getContagerencial());
				bean.setValorTotalRealizado(totalRealizado);
				bean.setValorHoraRelizado(totalRh > 0 ? new Money(totalRealizado.getValue().doubleValue()/totalRh) : new Money());
				
				
				realizado = realizado.add(bean.getValorTotalRealizado());
				orcamento = orcamento.add(bean.getValorTotalOrcamento());
				
				listaBean.add(bean);
			}
			
			filtro.setValorTotalOrcamento(orcamento);
			filtro.setValorTotalRealizado(realizado);
			request.setAttribute("listaBean", listaBean);
			request.setAttribute("dtFimFormatado", SinedDateUtils.toString(filtro.getDtFim()));
		}
		
		return new ModelAndView("relatorio/acompanhamentoEconomico").addObject("filtro", filtro);
	}
	
	public ModelAndView visualizarDetalhe(WebRequestContext request) throws ParseException{
		
		Date dtfim = SinedDateUtils.stringToDate(request.getParameter("dtFim"));
		Projeto projeto = new Projeto(Integer.parseInt(request.getParameter("cdprojeto")));
		Contagerencial contagerencial = contagerencialService.loadWithIdentificador(new Contagerencial(Integer.parseInt(request.getParameter("cdcontagerencial"))));
		
		List<AcompanhamentoEconomicoListagemDetalheBean> lista = projetoService.buscaDetalheAcompanhamentoEconomico(projeto, dtfim, contagerencial);
		request.setAttribute("lista", lista);
		
		return new ModelAndView("direct:/relatorio/popup/detalheAcompanhamentoEconomico");
	}
	
	
}
