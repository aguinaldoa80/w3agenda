package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.OrcamentoService;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoFinanceiroFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path ={ "/projeto/relatorio/AcompanhamentoFinanceiro", "/agro/relatorio/AcompanhamentoFinanceiro"},
	authorizationModule=ReportAuthorizationModule.class
)
public class AcompanhamentoFinanceiroReport extends SinedReport<AcompanhamentoFinanceiroFiltro> {

	private OrcamentoService orcamentoService;
	private PlanejamentoService planejamentoService;
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, AcompanhamentoFinanceiroFiltro filtro) throws Exception {
		return orcamentoService.createReportAcompanhamentoFinanceiro(filtro);
	}
	
	@Override
	public String getTitulo(AcompanhamentoFinanceiroFiltro filtro) {
		return "RELATÓRIO DE ACOMPANHAMENTO FINANCEIRO";
	}
	
	@Override
	public String getNomeArquivo() {
		return "acompanhamentofinanceiro";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, AcompanhamentoFinanceiroFiltro filtro) {
		report.addParameter("PROJETO", projetoService.load(filtro.getProjeto(), "projeto.cdprojeto, projeto.nome").getNome());
		report.addParameter("PLANEJAMENTO", planejamentoService.load(filtro.getPlanejamento(), "planejamento.cdplanejamento, planejamento.descricao").getDescricao());
	}
	
}