package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ValorreferenciaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ValorreferenciaFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Controller(path = "/projeto/relatorio/ValoresReferencia",	authorizationModule=ReportAuthorizationModule.class)
public class ValoresReferenciaReport extends SinedReport<ValorreferenciaFiltro> {

	private ValorreferenciaService valorreferenciaService;
	
	public void setValorreferenciaService(ValorreferenciaService valorreferenciaService) {
		this.valorreferenciaService = valorreferenciaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ValorreferenciaFiltro filtro) throws Exception {
		filtro.setOrderBy(request.getSession().getAttribute("order") != null ? (String) request.getSession().getAttribute("order") : null);
		request.getSession().removeAttribute("order");
		return valorreferenciaService.gerarRelarorioValoresReferencia(filtro);
	}
	
	@Override
	public String getTitulo(ValorreferenciaFiltro filtro) {
		return "RELAT�RIO DE VALORES DE REFER�NCIA";
	}
	
	@Override
	public String getNomeArquivo() {
		return "valorreferencia";
	}
}
