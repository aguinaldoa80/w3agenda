package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.TarefaorcamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.TarefaOrcamentoReportFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


@Controller(path = "/projeto/relatorio/TarefaOrcamento",	authorizationModule=ReportAuthorizationModule.class)
public class TarefaOrcamentoReport extends SinedReport<TarefaOrcamentoReportFiltro> {

	private TarefaorcamentoService tarefaorcamentoService;
	
	public void setTarefaorcamentoService(
			TarefaorcamentoService tarefaorcamentoService) {
		this.tarefaorcamentoService = tarefaorcamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, TarefaOrcamentoReportFiltro filtro) throws Exception {
		filtro = (TarefaOrcamentoReportFiltro)NeoWeb.getRequestContext().getSession().getAttribute(TarefaorcamentoService.SESSION_FILTRO_RELATORIO_TAREFA);
		NeoWeb.getRequestContext().getSession().removeAttribute(TarefaorcamentoService.SESSION_FILTRO_RELATORIO_TAREFA);
		
		return tarefaorcamentoService.gerarRelatorioTarefaOrcamento(filtro);
	}
	
	@Override
	public String getTitulo(TarefaOrcamentoReportFiltro filtro) {
		return "RELAT�RIO DAS TAREFAS DO OR�AMENTO";
	}
	
	@Override
	public String getNomeArquivo() {
		return "tarefaorcamento";
	}
	
}
