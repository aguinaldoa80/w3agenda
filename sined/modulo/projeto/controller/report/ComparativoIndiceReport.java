package br.com.linkcom.sined.modulo.projeto.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.ComparativoIndiceReportFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/projeto/relatorio/ComparativoIndice",
	authorizationModule=ReportAuthorizationModule.class
)
public class ComparativoIndiceReport extends SinedReport<ComparativoIndiceReportFiltro> {

	private TarefaService tarefaService;
	private ProjetoService projetoService;
	private PlanejamentoService planejamentoService;

	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	@Override
	protected void filtro(WebRequestContext request, ComparativoIndiceReportFiltro filtro) throws Exception {
		request.setAttribute("listaProjeto", projetoService.findAll("projeto.nome"));
		super.filtro(request, filtro);
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ComparativoIndiceReportFiltro filtro) throws Exception {
		return tarefaService.createReport(filtro);
	}
	
	@Override
	public String getTitulo(ComparativoIndiceReportFiltro filtro) {
		return "RELAT�RIO COMPARATIVO DE COMPOSI��O";
	}
	
	@Override
	public String getNomeArquivo() {
		return "comparativoindice";
	}
	
	public ModelAndView comboBoxPlanejamento(WebRequestContext request, Projeto projeo) {
		if (projeo == null || projeo.getCdprojeto() == null) {
			throw new SinedException("Projeto n�o pode ser nulo.");
		}
		
		List<Planejamento> lista = planejamentoService.findByProjeto(projeo);
		
		return new JsonModelAndView().addObject("lista", lista);
	}
}
