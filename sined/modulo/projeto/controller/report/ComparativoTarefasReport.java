package br.com.linkcom.sined.modulo.projeto.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.ComparativoTarefaReportFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;


/**
 * 
 * Relatório desempenho de tarefas
 * @author Andre Brunelli
 */

@Bean
@Controller(
	path = {"/projeto/relatorio/ComparativoTarefas","/agro/relatorio/ComparativoTarefas"},
	authorizationModule=ReportAuthorizationModule.class
)
public class ComparativoTarefasReport extends SinedReport<ComparativoTarefaReportFiltro> {

	private TarefaService tarefaService;
	private ProjetoService projetoService;
	private PlanejamentoService planejamentoService;
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ComparativoTarefaReportFiltro filtro)	throws Exception {
		return tarefaService.createReportTarefas(filtro);
	}

	@Override
	public String getTitulo(ComparativoTarefaReportFiltro filtro) {
		return "RELATÓRIO DE DESEMPENHO DAS TAREFAS";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, ComparativoTarefaReportFiltro filtro) {
		report.addParameter("PLANEJAMENTO", planejamentoService.load(filtro.getPlanejamento(), "planejamento.descricao").getDescricao());
		report.addParameter("PROJETO", projetoService.load(filtro.getProjeto(), "projeto.nome").getNome());
	}
	
	@Override
	public String getNomeArquivo() {
		return "desempenhotarefas";
	}

}
