package br.com.linkcom.sined.modulo.projeto.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ApontamentoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/projeto/relatorio/Apontamento", authorizationModule=ReportAuthorizationModule.class)
public class ApontamentoReport extends SinedReport<ApontamentoFiltro>{

	private ApontamentoService apontamentoService;
	
	public void setApontamentoService(ApontamentoService apontamentoService) {
		this.apontamentoService = apontamentoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	ApontamentoFiltro filtro) throws Exception {
		return apontamentoService.createReportListagem(filtro);
	}
	
	public ModelAndView gerarCsv(WebRequestContext request,	ApontamentoFiltro filtro) throws Exception {
		return new ResourceModelAndView(apontamentoService.gerarCSVListagem(filtro));
	}
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ApontamentoFiltro filtro) throws ResourceGenerationException {
		return new ModelAndView("redirect:/projeto/crud/Apontamento?ACAO=" + AbstractCrudController.LISTAGEM);
	}
	
	@Override
	public String getNomeArquivo() {
		return "apontamento";
	}

	@Override
	public String getTitulo(ApontamentoFiltro filtro) {
		return "Apontamento";
	}
	
}
