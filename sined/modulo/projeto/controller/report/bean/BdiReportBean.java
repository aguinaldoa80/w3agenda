package br.com.linkcom.sined.modulo.projeto.controller.report.bean;


public class BdiReportBean {
	private String numero;
	private String item;
	private String valor;
	private String valorHora;
	private Boolean negrito;
	
	public String getNumero() {
		return numero;
	}
	public String getItem() {
		return item;
	}
	public String getValor() {
		return valor;
	}
	public String getValorHora() {
		return valorHora;
	}
	public Boolean getNegrito() {
		return negrito;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public void setValorHora(String valorHora) {
		this.valorHora = valorHora;
	}
	public void setNegrito(Boolean negrito) {
		this.negrito = negrito;
	}
	
	
}
