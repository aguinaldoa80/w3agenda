package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import br.com.linkcom.neo.types.Money;

public class RecursosReportBean {
	
	private String tipo;
	private String item;
	private String unidade;
	private Integer qtde;
	private Money unitario;
	private Money total;
	
	public RecursosReportBean() {}
	
	

	public RecursosReportBean(String tipo, String item, String unidade,
			Integer qtde, Money unitario, Money total) {
		this.tipo = tipo;
		this.item = item;
		this.unidade = unidade;
		this.qtde = qtde;
		this.unitario = unitario;
		this.total = total;
	}



	public String getTipo() {
		return tipo;
	}

	public String getItem() {
		return item;
	}

	public String getUnidade() {
		return unidade;
	}

	public Integer getQtde() {
		return qtde;
	}

	public Money getUnitario() {
		return unitario;
	}

	public Money getTotal() {
		return total;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}

	public void setUnitario(Money unitario) {
		this.unitario = unitario;
	}

	public void setTotal(Money total) {
		this.total = total;
	}

}
