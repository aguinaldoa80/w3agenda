package br.com.linkcom.sined.modulo.projeto.controller.report.bean;


public class Histograma {
	
	String dia;
	Double realizado;
	Double planejado;
	
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public Double getRealizado() {
		return realizado;
	}
	public void setRealizado(Double realizado) {
		this.realizado = realizado;
	}
	public Double getPlanejado() {
		return planejado;
	}
	public void setPlanejado(Double planejado) {
		this.planejado = planejado;
	}


}
