package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

public class DiarioObraSubReportBean {
	
	protected String nome;
	protected Integer qtde;
	
	public String getNome() {
		return nome;
	}
	public Integer getQtde() {
		return qtde;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}
}
