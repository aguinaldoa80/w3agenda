package br.com.linkcom.sined.modulo.projeto.controller.report.bean;




public class DiarioObraAtividadesReportBean {
	
	private String nome;
	
	public DiarioObraAtividadesReportBean() {
	}
	
	public DiarioObraAtividadesReportBean(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
