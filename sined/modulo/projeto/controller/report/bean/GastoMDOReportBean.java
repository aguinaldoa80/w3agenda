package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import java.util.ArrayList;
import java.util.List;


public class GastoMDOReportBean {
	private String totalHoraMOD;
	private String totalHoraMOI;
	private String totalValorMOD;
	private String totalValorMOI;
	private String totalValorVendaMOD;
	private String totalValorVendaMOI;
	private String totalHoraGeral;
	private String totalValorGeral;
	private String totalValorVendaGeral;
	private Boolean incluirMOD;
	private Boolean incluirMOI;
	private Boolean incluirPrecoVenda;
	private List<GastoMDOItemReportBean> listaMOD = new ArrayList<GastoMDOItemReportBean>();
	private List<GastoMDOItemReportBean> listaMOI = new ArrayList<GastoMDOItemReportBean>();
	
	public String getTotalHoraMOD() {
		return totalHoraMOD;
	}
	public String getTotalHoraMOI() {
		return totalHoraMOI;
	}
	public String getTotalValorMOD() {
		return totalValorMOD;
	}
	public String getTotalValorMOI() {
		return totalValorMOI;
	}
	public String getTotalValorVendaMOD() {
		return totalValorVendaMOD;
	}
	public String getTotalValorVendaMOI() {
		return totalValorVendaMOI;
	}
	public String getTotalHoraGeral() {
		return totalHoraGeral;
	}
	public String getTotalValorGeral() {
		return totalValorGeral;
	}
	public String getTotalValorVendaGeral() {
		return totalValorVendaGeral;
	}
	public Boolean getIncluirMOD() {
		return incluirMOD;
	}
	public Boolean getIncluirMOI() {
		return incluirMOI;
	}
	public Boolean getIncluirPrecoVenda() {
		return incluirPrecoVenda;
	}
	public List<GastoMDOItemReportBean> getListaMOD() {
		return listaMOD;
	}
	public List<GastoMDOItemReportBean> getListaMOI() {
		return listaMOI;
	}
	public void setTotalHoraMOD(String totalHoraMOD) {
		this.totalHoraMOD = totalHoraMOD;
	}
	public void setTotalHoraMOI(String totalHoraMOI) {
		this.totalHoraMOI = totalHoraMOI;
	}
	public void setTotalValorMOD(String totalValorMOD) {
		this.totalValorMOD = totalValorMOD;
	}
	public void setTotalValorMOI(String totalValorMOI) {
		this.totalValorMOI = totalValorMOI;
	}
	public void setTotalValorVendaMOD(String totalValorVendaMOD) {
		this.totalValorVendaMOD = totalValorVendaMOD;
	}
	public void setTotalValorVendaMOI(String totalValorVendaMOI) {
		this.totalValorVendaMOI = totalValorVendaMOI;
	}
	public void setTotalHoraGeral(String totalHoraGeral) {
		this.totalHoraGeral = totalHoraGeral;
	}
	public void setTotalValorGeral(String totalValorGeral) {
		this.totalValorGeral = totalValorGeral;
	}
	public void setTotalValorVendaGeral(String totalValorVendaGeral) {
		this.totalValorVendaGeral = totalValorVendaGeral;
	}
	public void setIncluirMOD(Boolean incluirMOD) {
		this.incluirMOD = incluirMOD;
	}
	public void setIncluirMOI(Boolean incluirMOI) {
		this.incluirMOI = incluirMOI;
	}
	public void setIncluirPrecoVenda(Boolean incluirPrecoVenda) {
		this.incluirPrecoVenda = incluirPrecoVenda;
	}
	public void setListaMOD(List<GastoMDOItemReportBean> listaMOD) {
		this.listaMOD = listaMOD;
	}
	public void setListaMOI(List<GastoMDOItemReportBean> listaMOI) {
		this.listaMOI = listaMOI;
	}
}
