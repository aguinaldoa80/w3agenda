package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import java.text.DecimalFormat;

import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.Enumeration.RelComparativoIndiceSituacao;

public class RelComparativoIndice {
	
	protected Integer cdindice;
	protected String indice;
	protected String item;
	protected Integer qtdeItemPrevista;
	protected Integer qtdeItemPrevistaAnterior;
	protected String unidadeItemPrevista;
	
	protected Integer qtdeItemRealizada;
	protected String unidadeItemRealizada;
	
	protected Integer qtdeIndice;
	protected Integer qtdeIndiceAnterior;
	protected String unidadeIndice;
	
	protected Integer qtdeTarefa;
	protected Integer qtdeTarefaAnterior;
	protected Integer qtdeRealizada;
	protected Integer qtdeRealizadaAnterior;
	
	protected Double qtdeItemRazao;
	protected String qtdeItemRazaoFormatado;
	
	protected RelComparativoIndiceSituacao situacao;
	
	protected Boolean encontrado;

	public Integer getCdindice() {
		return cdindice;
	}

	public String getIndice() {
		return indice;
	}

	public String getItem() {
		return item;
	}

	public Integer getQtdeItemPrevista() {
		return qtdeItemPrevista;
	}

	public String getUnidadeItemPrevista() {
		return unidadeItemPrevista;
	}

	public Integer getQtdeItemRealizada() {
		return qtdeItemRealizada;
	}

	public String getUnidadeItemRealizada() {
		return unidadeItemRealizada;
	}

	public Integer getQtdeIndice() {
		return qtdeIndice;
	}

	public String getUnidadeIndice() {
		return unidadeIndice;
	}

	public Double getQtdeItemRazao() {
		return qtdeItemRazao;
	}
	
	public Integer getQtdeTarefa() {
		return qtdeTarefa;
	}

	public Integer getQtdeRealizada() {
		return qtdeRealizada;
	}

	public String getQtdeItemRazaoFormatado() {
		return new DecimalFormat("###0.00").format(getQtdeItemRazao());
	}

	public void setQtdeItemRazaoFormatado(String qtdeItemRazaoFormatado) {
		this.qtdeItemRazaoFormatado = qtdeItemRazaoFormatado;
	}

	public void setQtdeTarefa(Integer qtdeTarefa) {
		this.qtdeTarefa = qtdeTarefa;
	}

	public void setQtdeRealizada(Integer qtdeRealizada) {
		this.qtdeRealizada = qtdeRealizada;
	}

	public void setCdindice(Integer cdindice) {
		this.cdindice = cdindice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void setQtdeItemPrevista(Integer qtdeItemPrevista) {
		this.qtdeItemPrevista = qtdeItemPrevista;
	}

	public void setUnidadeItemPrevista(String unidadeItemPrevista) {
		this.unidadeItemPrevista = unidadeItemPrevista;
	}

	public void setQtdeItemRealizada(Integer qtdeItemRealizada) {
		this.qtdeItemRealizada = qtdeItemRealizada;
	}

	public void setUnidadeItemRealizada(String unidadeItemRealizada) {
		this.unidadeItemRealizada = unidadeItemRealizada;
	}

	public void setQtdeIndice(Integer qtdeIndice) {
		this.qtdeIndice = qtdeIndice;
	}

	public void setUnidadeIndice(String unidadeIndice) {
		this.unidadeIndice = unidadeIndice;
	}

	public void setQtdeItemRazao(Double qtdeItemRazao) {
		this.qtdeItemRazao = qtdeItemRazao;
	}

	public Integer getQtdeItemPrevistaAnterior() {
		return qtdeItemPrevistaAnterior;
	}

	public Integer getQtdeTarefaAnterior() {
		return qtdeTarefaAnterior;
	}
	
	public RelComparativoIndiceSituacao getSituacao() {
		return situacao;
	}

	public void setQtdeItemPrevistaAnterior(Integer qtdeItemPrevistaAnterior) {
		this.qtdeItemPrevistaAnterior = qtdeItemPrevistaAnterior;
	}

	public void setQtdeTarefaAnterior(Integer qtdeTarefaAnterior) {
		this.qtdeTarefaAnterior = qtdeTarefaAnterior;
	}

	public void setSituacao(RelComparativoIndiceSituacao situacao) {
		this.situacao = situacao;
	}

	public Boolean getEncontrado() {
		return encontrado;
	}
	public void setEncontrado(Boolean encontrado) {
		this.encontrado = encontrado;
	}

	public Integer getQtdeIndiceAnterior() {
		return qtdeIndiceAnterior;
	}

	public Integer getQtdeRealizadaAnterior() {
		return qtdeRealizadaAnterior;
	}

	public void setQtdeIndiceAnterior(Integer qtdeIndiceAnterior) {
		this.qtdeIndiceAnterior = qtdeIndiceAnterior;
	}

	public void setQtdeRealizadaAnterior(Integer qtdeRealizadaAnterior) {
		this.qtdeRealizadaAnterior = qtdeRealizadaAnterior;
	}
}
