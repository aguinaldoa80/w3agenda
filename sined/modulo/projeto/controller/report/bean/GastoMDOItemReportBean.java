package br.com.linkcom.sined.modulo.projeto.controller.report.bean;


public class GastoMDOItemReportBean {
	private String nomeCargo;
	private String totalHora;
	private String custoHoraSemEncargoSemFator;
	private String salario;
	private String fatorEncargo;
	private String custoHoraComEncargoComFator;
	private String custoTotal;
	private String custoHoraVenda;
	private String custoTotalVenda;
	
	public String getNomeCargo() {
		return nomeCargo;
	}
	public String getTotalHora() {
		return totalHora;
	}
	public String getCustoHoraSemEncargoSemFator() {
		return custoHoraSemEncargoSemFator;
	}
	public String getSalario() {
		return salario;
	}
	public String getFatorEncargo() {
		return fatorEncargo;
	}
	public String getCustoHoraComEncargoComFator() {
		return custoHoraComEncargoComFator;
	}
	public String getCustoTotal() {
		return custoTotal;
	}
	public String getCustoHoraVenda() {
		return custoHoraVenda;
	}
	public String getCustoTotalVenda() {
		return custoTotalVenda;
	}
	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}
	public void setTotalHora(String totalHora) {
		this.totalHora = totalHora;
	}
	public void setCustoHoraSemEncargoSemFator(String custoHoraSemEncargoSemFator) {
		this.custoHoraSemEncargoSemFator = custoHoraSemEncargoSemFator;
	}
	public void setSalario(String salario) {
		this.salario = salario;
	}
	public void setFatorEncargo(String fatorEncargo) {
		this.fatorEncargo = fatorEncargo;
	}
	public void setCustoHoraComEncargoComFator(String custoHoraComEncargoComFator) {
		this.custoHoraComEncargoComFator = custoHoraComEncargoComFator;
	}
	public void setCustoTotal(String custoTotal) {
		this.custoTotal = custoTotal;
	}
	public void setCustoHoraVenda(String custoHoraVenda) {
		this.custoHoraVenda = custoHoraVenda;
	}
	public void setCustoTotalVenda(String custoTotalVenda) {
		this.custoTotalVenda = custoTotalVenda;
	}
}
