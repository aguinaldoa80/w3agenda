package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

/**
 * 
 * Bean do relatório de comparativo de tarefas
 * @author Andre Brunelli
 */
public class RelComparativoTarefas {
	
	protected Integer cdtarefa;
	protected String tarefa;
	
	protected Integer qtdeTarefa;
	protected String unidadeTarefa;
	
	protected String item;
	protected String unidadeItemPrevista;
	protected String unidadeItemRealizada;
	protected Integer qtdeItemPrevista;
	protected Integer qtdeItemRealizada;
	
	protected Double qtdeItemRazao;

	public Integer getCdtarefa() {
		return cdtarefa;
	}

	public String getTarefa() {
		return tarefa;
	}

	public Integer getQtdeTarefa() {
		return qtdeTarefa;
	}

	public String getUnidadeTarefa() {
		return unidadeTarefa;
	}

	public String getItem() {
		return item;
	}

	public String getUnidadeItemPrevista() {
		return unidadeItemPrevista;
	}

	public String getUnidadeItemRealizada() {
		return unidadeItemRealizada;
	}

	public Integer getQtdeItemPrevista() {
		return qtdeItemPrevista;
	}

	public Integer getQtdeItemRealizada() {
		return qtdeItemRealizada;
	}

	public Double getQtdeItemRazao() {
		return qtdeItemRazao;
	}

	public void setCdtarefa(Integer cdtarefa) {
		this.cdtarefa = cdtarefa;
	}

	public void setTarefa(String tarefa) {
		this.tarefa = tarefa;
	}

	public void setQtdeTarefa(Integer qtdeTarefa) {
		this.qtdeTarefa = qtdeTarefa;
	}

	public void setUnidadeTarefa(String unidadeTarefa) {
		this.unidadeTarefa = unidadeTarefa;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void setUnidadeItemPrevista(String unidadeItemPrevista) {
		this.unidadeItemPrevista = unidadeItemPrevista;
	}

	public void setUnidadeItemRealizada(String unidadeItemRealizada) {
		this.unidadeItemRealizada = unidadeItemRealizada;
	}

	public void setQtdeItemPrevista(Integer qtdeItemPrevista) {
		this.qtdeItemPrevista = qtdeItemPrevista;
	}

	public void setQtdeItemRealizada(Integer qtdeItemRealizada) {
		this.qtdeItemRealizada = qtdeItemRealizada;
	}

	public void setQtdeItemRazao(Double qtdeItemRazao) {
		this.qtdeItemRazao = qtdeItemRazao;
	}

}
