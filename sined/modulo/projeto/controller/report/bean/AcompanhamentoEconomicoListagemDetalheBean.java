package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.TipoRegistroDetalheAcompanhamentoEconomico;

public class AcompanhamentoEconomicoListagemDetalheBean {

	private TipoRegistroDetalheAcompanhamentoEconomico tipoRegistroDetalheAcompanhamentoEconomico;
	private Integer id;
	private String descricao;
	private Date data;
	private Money valor;
	
	public AcompanhamentoEconomicoListagemDetalheBean(
			TipoRegistroDetalheAcompanhamentoEconomico tipoRegistroDetalheAcompanhamentoEconomico, Integer id,
			String descricao, Date data, Money valor) {
		this.tipoRegistroDetalheAcompanhamentoEconomico = tipoRegistroDetalheAcompanhamentoEconomico;
		this.id = id;
		this.descricao = descricao;
		this.data = data;
		this.valor = valor;
	}
	public String getDescricao() {
		return descricao;
	}
	public Date getData() {
		return data;
	}
	public Money getValor() {
		return valor;
	}
	public TipoRegistroDetalheAcompanhamentoEconomico getTipoRegistroDetalheAcompanhamentoEconomico() {
		return tipoRegistroDetalheAcompanhamentoEconomico;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setTipoRegistroDetalheAcompanhamentoEconomico(
			TipoRegistroDetalheAcompanhamentoEconomico tipoRegistroDetalheAcompanhamentoEconomico) {
		this.tipoRegistroDetalheAcompanhamentoEconomico = tipoRegistroDetalheAcompanhamentoEconomico;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
}
