package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import java.sql.Timestamp;

import br.com.linkcom.sined.util.SinedUtil;



public class DiarioObraRecursosReportBean {
	
	private String categoria;
	private Double qte;
	private Double faltas;
	private Timestamp inicio;
	private Timestamp termino;
	private Double hs;
	
	public DiarioObraRecursosReportBean() {
	}
	
	public DiarioObraRecursosReportBean(String categoria, Double qte, Timestamp inicio, Timestamp termino) {
		this.categoria = categoria;
		this.qte = qte;
		this.inicio = inicio;
		this.termino = termino;
		if(inicio != null && termino != null){
			long time = termino.getTime() - inicio.getTime();
			double horas =  new Double(time) / (1000.0*60.0*60.0);
			this.hs = horas;
		}
	}

	public String getCategoria() {
		return categoria;
	}
	public Double getQte() {
		return qte;
	}
	public Double getFaltas() {
		return faltas;
	}
	public Timestamp getInicio() {
		return inicio;
	}
	public Timestamp getTermino() {
		return termino;
	}
	public Double getHs() {
		return hs;
	}
	public String getQteStr() {
		return SinedUtil.retiraZeroDireita(getQte());
	}
	public String getFaltasStr() {
		return SinedUtil.retiraZeroDireita(getFaltas());
	}
	public String getHsStr() {
		return SinedUtil.retiraZeroDireita(getHs());
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public void setQte(Double qte) {
		this.qte = qte;
	}
	public void setFaltas(Double faltas) {
		this.faltas = faltas;
	}
	public void setInicio(Timestamp inicio) {
		this.inicio = inicio;
	}
	public void setTermino(Timestamp termino) {
		this.termino = termino;
	}
	public void setHs(Double hs) {
		this.hs = hs;
	}
	
}
