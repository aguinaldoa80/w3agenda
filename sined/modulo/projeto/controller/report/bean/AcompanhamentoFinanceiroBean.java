package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Contagerencial;


public class AcompanhamentoFinanceiroBean {
	
	private Contagerencial contagerencial;
	private Double orcado;
	private Double realizado;
	private Double saldo;
	private Double acumulado;
	
	public AcompanhamentoFinanceiroBean(Contagerencial contagerencial,Double orcado, Double realizado) {
		this.contagerencial = contagerencial;
		this.orcado = orcado;
		this.realizado = realizado;
	}
	
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public Double getOrcado() {
		return orcado;
	}
	public Double getRealizado() {
		return realizado;
	}
	public Double getAcumulado() {
		return acumulado;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public void setAcumulado(Double acumulado) {
		this.acumulado = acumulado;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setOrcado(Double orcado) {
		this.orcado = orcado;
	}
	public void setRealizado(Double realizado) {
		this.realizado = realizado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((contagerencial == null) ? 0 : contagerencial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AcompanhamentoFinanceiroBean other = (AcompanhamentoFinanceiroBean) obj;
		if (contagerencial == null) {
			if (other.contagerencial != null)
				return false;
		} else if (!contagerencial.equals(other.contagerencial))
			return false;
		return true;
	}
	
	
	
}
