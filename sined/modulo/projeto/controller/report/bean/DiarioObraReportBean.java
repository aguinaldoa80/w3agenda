package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;



public class DiarioObraReportBean {
	
	private LinkedList<DiarioObraTipoAtividadesReportBean> listaAtividade = new LinkedList<DiarioObraTipoAtividadesReportBean>();
	private LinkedList<DiarioObraRecursosReportBean> listaMod = new LinkedList<DiarioObraRecursosReportBean>();
	private LinkedList<DiarioObraRecursosReportBean> listaMoi = new LinkedList<DiarioObraRecursosReportBean>();
	private LinkedList<DiarioObraRecursosReportBean> listaMos = new LinkedList<DiarioObraRecursosReportBean>();
	private LinkedList<DiarioObraRecursosReportBean> listaEquip = new LinkedList<DiarioObraRecursosReportBean>();
	private Boolean solManha;
	private Boolean chuvaManha;
	private Boolean solTarde;
	private Boolean chuvaTarde;
	private Boolean solNoite;
	private Boolean chuvaNoite;
	private Timestamp hrInicioManha;
	private Timestamp hrInicioTarde;
	private Timestamp hrInicioNoite;
	private Timestamp hrTerminoManha;
	private Timestamp hrTerminoTarde;
	private Timestamp hrTerminoNoite;
	private String duracaoManha;
	private String duracaoTarde;
	private String duracaoNoite;
	private String numContrato;
	private Date dtInicioContrato;
	private Date dtTerminoContrato;
	private Date prazoContratual;
	private Date dtMobilizacao;
	private String diaSemana;
	private Integer diasDecorridos;
	private Integer efetivoTotalObra;
	private Date dtDia;
	private Integer numeroDocumento;
	private String obra;
	private String local;
	private LinkedList<String> listaFotos = new LinkedList<String>();
	
	public LinkedList<DiarioObraTipoAtividadesReportBean> getListaAtividade() {
		return listaAtividade;
	}
	public LinkedList<DiarioObraRecursosReportBean> getListaMod() {
		return listaMod;
	}
	public Boolean getSolManha() {
		return solManha;
	}
	public Boolean getChuvaManha() {
		return chuvaManha;
	}
	public Boolean getSolTarde() {
		return solTarde;
	}
	public Boolean getChuvaTarde() {
		return chuvaTarde;
	}
	public Boolean getSolNoite() {
		return solNoite;
	}
	public Boolean getChuvaNoite() {
		return chuvaNoite;
	}
	public Timestamp getHrInicioManha() {
		return hrInicioManha;
	}
	public Timestamp getHrInicioTarde() {
		return hrInicioTarde;
	}
	public Timestamp getHrInicioNoite() {
		return hrInicioNoite;
	}
	public Timestamp getHrTerminoManha() {
		return hrTerminoManha;
	}
	public Timestamp getHrTerminoTarde() {
		return hrTerminoTarde;
	}
	public Timestamp getHrTerminoNoite() {
		return hrTerminoNoite;
	}
	public String getDuracaoManha() {
		return duracaoManha;
	}
	public String getDuracaoTarde() {
		return duracaoTarde;
	}
	public String getDuracaoNoite() {
		return duracaoNoite;
	}
	public String getNumContrato() {
		return numContrato;
	}
	public Date getDtInicioContrato() {
		return dtInicioContrato;
	}
	public Date getDtTerminoContrato() {
		return dtTerminoContrato;
	}
	public Date getPrazoContratual() {
		return prazoContratual;
	}
	public Date getDtMobilizacao() {
		return dtMobilizacao;
	}
	public String getDiaSemana() {
		return diaSemana;
	}
	public Integer getDiasDecorridos() {
		return diasDecorridos;
	}
	public Integer getEfetivoTotalObra() {
		return efetivoTotalObra;
	}
	public LinkedList<DiarioObraRecursosReportBean> getListaMoi() {
		return listaMoi;
	}
	public LinkedList<DiarioObraRecursosReportBean> getListaMos() {
		return listaMos;
	}
	public LinkedList<DiarioObraRecursosReportBean> getListaEquip() {
		return listaEquip;
	}
	public Date getDtDia() {
		return dtDia;
	}
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}
	public String getObra() {
		return obra;
	}
	public String getLocal() {
		return local;
	}
	public LinkedList<String> getListaFotos() {
		return listaFotos;
	}
	public void setListaFotos(LinkedList<String> listaFotos) {
		this.listaFotos = listaFotos;
	}
	public void setDtDia(Date dtDia) {
		this.dtDia = dtDia;
	}
	public void setNumeroDocumento(Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public void setObra(String obra) {
		this.obra = obra;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public void setListaMos(LinkedList<DiarioObraRecursosReportBean> listaMos) {
		this.listaMos = listaMos;
	}
	public void setListaEquip(LinkedList<DiarioObraRecursosReportBean> listaEquip) {
		this.listaEquip = listaEquip;
	}
	public void setListaMoi(LinkedList<DiarioObraRecursosReportBean> listaMoi) {
		this.listaMoi = listaMoi;
	}
	public void setListaAtividade(LinkedList<DiarioObraTipoAtividadesReportBean> listaAtividade) {
		this.listaAtividade = listaAtividade;
	}
	public void setListaMod(LinkedList<DiarioObraRecursosReportBean> listaMod) {
		this.listaMod = listaMod;
	}
	public void setSolManha(Boolean solManha) {
		this.solManha = solManha;
	}
	public void setChuvaManha(Boolean chuvaManha) {
		this.chuvaManha = chuvaManha;
	}
	public void setSolTarde(Boolean solTarde) {
		this.solTarde = solTarde;
	}
	public void setChuvaTarde(Boolean chuvaTarde) {
		this.chuvaTarde = chuvaTarde;
	}
	public void setSolNoite(Boolean solNoite) {
		this.solNoite = solNoite;
	}
	public void setChuvaNoite(Boolean chuvaNoite) {
		this.chuvaNoite = chuvaNoite;
	}
	public void setHrInicioManha(Timestamp hrInicioManha) {
		this.hrInicioManha = hrInicioManha;
	}
	public void setHrInicioTarde(Timestamp hrInicioTarde) {
		this.hrInicioTarde = hrInicioTarde;
	}
	public void setHrInicioNoite(Timestamp hrInicioNoite) {
		this.hrInicioNoite = hrInicioNoite;
	}
	public void setHrTerminoManha(Timestamp hrTerminoManha) {
		this.hrTerminoManha = hrTerminoManha;
	}
	public void setHrTerminoTarde(Timestamp hrTerminoTarde) {
		this.hrTerminoTarde = hrTerminoTarde;
	}
	public void setHrTerminoNoite(Timestamp hrTerminoNoite) {
		this.hrTerminoNoite = hrTerminoNoite;
	}
	public void setDuracaoManha(String duracaoManha) {
		this.duracaoManha = duracaoManha;
	}
	public void setDuracaoTarde(String duracaoTarde) {
		this.duracaoTarde = duracaoTarde;
	}
	public void setDuracaoNoite(String duracaoNoite) {
		this.duracaoNoite = duracaoNoite;
	}
	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}
	public void setDtInicioContrato(Date dtInicioContrato) {
		this.dtInicioContrato = dtInicioContrato;
	}
	public void setDtTerminoContrato(Date dtTerminoContrato) {
		this.dtTerminoContrato = dtTerminoContrato;
	}
	public void setPrazoContratual(Date prazoContratual) {
		this.prazoContratual = prazoContratual;
	}
	public void setDtMobilizacao(Date dtMobilizacao) {
		this.dtMobilizacao = dtMobilizacao;
	}
	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}
	public void setDiasDecorridos(Integer diasDecorridos) {
		this.diasDecorridos = diasDecorridos;
	}
	public void setEfetivoTotalObra(Integer efetivoTotalObra) {
		this.efetivoTotalObra = efetivoTotalObra;
	}
	
	
	public Integer getTamanhoMod() {
		return getListaMod() != null ? getListaMod().size() : 0;
	}
	public Integer getTamanhoMoi() {
		return getListaMoi() != null ? getListaMoi().size() : 0;
	}
	public Integer getTamanhoMos() {
		return getListaMos() != null ? getListaMos().size() : 0;
	}
	public Integer getTamanhoEquip() {
		return getListaEquip() != null ? getListaEquip().size() : 0;
	}

}
