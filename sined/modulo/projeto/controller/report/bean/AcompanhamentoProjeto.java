package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import java.util.Date;

public class AcompanhamentoProjeto {
	
	Date data;
	Double custo;
	Double planejamento;
	Double andamento;
	Boolean diaUtil;
	
	public Date getData() {
		return data;
	}
	public Double getCusto() {
		return custo;
	}
	public Double getPlanejamento() {
		return planejamento;
	}
	public Double getAndamento() {
		return andamento;
	}
	public Boolean getDiaUtil() {
		return diaUtil;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setCusto(Double custo) {
		this.custo = custo;
	}
	public void setPlanejamento(Double planejamento) {
		this.planejamento = planejamento;
	}
	public void setAndamento(Double andamento) {
		this.andamento = andamento;
	}
	public void setDiaUtil(Boolean diaUtil) {
		this.diaUtil = diaUtil;
	}
}
