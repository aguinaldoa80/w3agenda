package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contagerencial;

public class AcompanhamentoEconomicoListagemBean {
	
	private Contagerencial contagerencial;
	private Money valorTotalOrcamento;
	private Money valorHoraOrcamento;
	private Money valorTotalRealizado;
	private Money valorHoraRelizado;
	
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Total R$ (Or�amento)")
	public Money getValorTotalOrcamento() {
		return valorTotalOrcamento;
	}
	@DisplayName("R$/h (Or�amento)")
	public Money getValorHoraOrcamento() {
		return valorHoraOrcamento;
	}
	@DisplayName("Total R$ (Realizado)")
	public Money getValorTotalRealizado() {
		return valorTotalRealizado;
	}
	@DisplayName("R$/h (Realizado)")
	public Money getValorHoraRelizado() {
		return valorHoraRelizado;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setValorTotalOrcamento(Money valorTotalOrcamento) {
		this.valorTotalOrcamento = valorTotalOrcamento;
	}
	public void setValorHoraOrcamento(Money valorHoraOrcamento) {
		this.valorHoraOrcamento = valorHoraOrcamento;
	}
	public void setValorTotalRealizado(Money valorTotalRealizado) {
		this.valorTotalRealizado = valorTotalRealizado;
	}
	public void setValorHoraRelizado(Money valorHoraRelizado) {
		this.valorHoraRelizado = valorHoraRelizado;
	}

}
