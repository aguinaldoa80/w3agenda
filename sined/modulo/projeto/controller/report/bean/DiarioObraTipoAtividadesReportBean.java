package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import java.util.LinkedList;



public class DiarioObraTipoAtividadesReportBean {
	
	private String tipoAtividade;
	private LinkedList<DiarioObraAtividadesReportBean> lista = new LinkedList<DiarioObraAtividadesReportBean>();
	
	public DiarioObraTipoAtividadesReportBean() {
	}
	
	public DiarioObraTipoAtividadesReportBean(String tipoAtividade) {
		this.tipoAtividade = tipoAtividade;
	}
	
	public String getTipoAtividade() {
		return tipoAtividade;
	}
	public LinkedList<DiarioObraAtividadesReportBean> getLista() {
		return lista;
	}
	public void setTipoAtividade(String tipoAtividade) {
		this.tipoAtividade = tipoAtividade;
	}
	public void setLista(LinkedList<DiarioObraAtividadesReportBean> lista) {
		this.lista = lista;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((tipoAtividade == null) ? 0 : tipoAtividade.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final DiarioObraTipoAtividadesReportBean other = (DiarioObraTipoAtividadesReportBean) obj;
		if (tipoAtividade == null) {
			if (other.tipoAtividade != null)
				return false;
		} else if (!tipoAtividade.equals(other.tipoAtividade))
			return false;
		return true;
	}
	
	
	
}
