package br.com.linkcom.sined.modulo.projeto.controller.report.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividade;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.enumeration.Tempo;

/**
 * Bean do relat�rio de Di�rio de obras.
 * @author Andre Brunelli
 */

public class RelDiarioObras {
	
	protected Integer cdatividade;
	protected String descricao;
	protected Atividadetipo atividadetipo;
	protected List<Atividade> itensatividades;
	protected List<Cargo> itenscargo;
	protected Tempo tempomanha;
	protected Tempo tempotarde;
	protected Tempo temponoite;
	protected String dtdia;
	
	public String getDtdia() {
		return dtdia;
	}
	
	public void setDtdia(String dtdia) {
		this.dtdia = dtdia;
	}
	
	public Tempo getTempomanha() {
		return tempomanha;
	}

	public void setTempomanha(Tempo tempomanha) {
		this.tempomanha = tempomanha;
	}

	public Tempo getTempotarde() {
		return tempotarde;
	}

	public void setTempotarde(Tempo tempotarde) {
		this.tempotarde = tempotarde;
	}

	public Tempo getTemponoite() {
		return temponoite;
	}

	public void setTemponoite(Tempo temponoite) {
		this.temponoite = temponoite;
	}

	public List<Cargo> getItenscargo() {
		return itenscargo;
	}
	
	public void setItenscargo(List<Cargo> itenscargo) {
		this.itenscargo = itenscargo;
	}
	
	public List<Atividade> getItensatividades() {
		return itensatividades;
	}
	
	public void setItensatividades(List<Atividade> itensatividades) {
		this.itensatividades = itensatividades;
	}
	
	public Integer getCdatividade() {
		return cdatividade;
	}
	public void setCdatividade(Integer cdatividade) {
		this.cdatividade = cdatividade;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	
	

}
