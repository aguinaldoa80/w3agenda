package br.com.linkcom.sined.modulo.projeto.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoProducaodiaria;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.ProducaodiariaService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ProducaodiariaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/projeto/crud/Producaodiaria","/agro/crud/Producaodiaria"},authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"tarefa.planejamento.projeto", "tarefa.planejamento", "tarefa", "faturar", "situacaoProducaodiaria"})
public class ProducaodiariaCrud extends CrudControllerSined<ProducaodiariaFiltro, Producaodiaria, Producaodiaria>{

	private ProducaodiariaService producaodiariaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private TarefaService tarefaService;
	private UnidademedidaService unidademedidaService;
	
	public void setProducaodiariaService(ProducaodiariaService producaodiariaService) {
		this.producaodiariaService = producaodiariaService;
	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	
	@Override
	protected void listagem(WebRequestContext request,ProducaodiariaFiltro filtro) throws Exception {
		List<SituacaoProducaodiaria> listaFaturada = new ArrayList<SituacaoProducaodiaria>();
		listaFaturada.add(SituacaoProducaodiaria.A_FATURAR);
		listaFaturada.add(SituacaoProducaodiaria.FATURADA);
		request.setAttribute("listaFaturada", listaFaturada);
	}
		
	@Override
	protected void validate(Object obj, BindException errors, String acao) {
		super.validate(obj, errors, acao);
		
		if("associarMedicoesNotafiscal".equals(acao)){
			ProducaodiariaFiltro filtro = (ProducaodiariaFiltro) obj;
			Nota nota = filtro.getNota();
			
			if(nota == null || nota.getCdNota() == null || StringUtils.isBlank(nota.getCodProducoesDiaria())){
				errors.reject("001","A requisi��o n�o p�de ser processada.");
			}
			
		}
	}
	
	/*
	 * In�cio dos m�todos dos subfluxos.
	 */
		
	/**
	 * Action para faturar medi��es. Os c�digos devem ser informados pelo par�metro <code>itens</code>.
	 * A tela de cadastro de Nota fiscal de servi�o � aberta e, ao ser salva, o sistema associa esta nova nota
	 * �s medi��es informadas.
	 * 
	 * @see #validateAndLoadItens(WebRequestContext)
	 * @see #validateItensDoFaturar(List)
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#carregaAssociaNotaServico(NotaFiscalServico, List)
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#criaNotaServico(List)
	 * @see br.com.linkcom.sined.geral.service.ProducaodiariaService#associaNotaProducoesdiarias(br.com.linkcom.sined.geral.bean.Nota, List)
	 * 
	 * @param request
	 * @return
	 * @author Fl�vio Tavares
	 */
	@OnErrors(LISTAGEM)
	@Action("doFaturar")
	public ModelAndView doFaturar(WebRequestContext request, Nota nota){
		if(!SinedUtil.isUserHasAction("FATURAR_PRODUCAODIARIA")){
			throw new SinedException("O usu�rio n�o tem permiss�o para executar esta a��o. Para mais informa��es entre em contato " +
					"com o administrador do sistema.");
		}
		
		List<Producaodiaria> listaMedicao = this.validateAndLoadItens(request);
		this.validateItensDoFaturar(listaMedicao);
		
		NotaFiscalServico nfs = null;
		if(SinedUtil.isObjectValid(nota)){ // Se o ID do par�metro nota foi informado
			nfs = notaFiscalServicoService.carregaAssociaNotaServico(new NotaFiscalServico(nota), listaMedicao);
		}else{
			nfs = notaFiscalServicoService.criaNotaServico(listaMedicao);
		}
		
		nfs.setFromProducaodiaria(Boolean.TRUE);
		String modelName = Util.strings.uncaptalize(NotaFiscalServico.class.getSimpleName());
		request.getSession().setAttribute(modelName, nfs);
		
		return new ModelAndView("redirect:/faturamento/crud/NotaFiscalServico")
					.addObject("ACAO", "criarNotaFiscalServico")					
					.addObject("clearBase", Boolean.TRUE)					
					.addObject("showListagemLink", Boolean.FALSE)					
		;
	}
	
	/**
	 * Valida a lista de Medi��o.
	 * 
	 * @param listaMedicao
	 * @throws SinedException - Se a lista for vazia ou nula.
	 * @throws SinedException - Se as produ��es di�rias possuem projetos diferentes.
	 * @throws SinedException - Se as produ��es di�rias n�o puderem ser faturadas.
	 * @throws SinedException - Se as produ��es di�rias j� estiverem faturadas.
	 * @author Fl�vio Tavares
	 */
	private void validateItensDoFaturar(List<Producaodiaria> listaMedicao){
		if(SinedUtil.isListEmpty(listaMedicao)){
			throw new SinedException("Nenhuma medi��o selecionada.");
		}
		
		Projeto base = listaMedicao.iterator().next().getTarefa().getPlanejamento().getProjeto();
		
		for (Producaodiaria pd : listaMedicao) {
			/*
			 * Verifica se a medi��o faz parte do mesmo projeto
			 */
			if(!base.equals(pd.getTarefa().getPlanejamento().getProjeto())){
				throw new SinedException("Devem ser selecionadas medi��es do mesmo projeto.");
			}
			
			/*
			 * Verifica se a medi��o pode ser faturada.
			 */
			if(Util.booleans.isFalse(pd.getFaturar())){
				throw new SinedException("Existem medi��es que n�o podem ser faturadas.");
			}
			
			/*
			 * Verifica se a medi��o ja foi faturada.
			 */
			if(pd.getDtfatura() != null){
				throw new SinedException("Existem medi��es j� faturadas.");
			}
		}
	}
	
	/**
	 * Valida o par�metro <code>itens</code> e carrega as informa��es das medi��es cujo c�digo
	 * � indicado no par�metro. 
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProducaodiariaService#findByWhereIn(String)
	 * 
	 * @param request
	 * @throws SinedException - Se o par�metro itens n�o foi informado.
	 * @return Lista de Produ��o di�ria se o par�metro itens for v�lido.
	 * 
	 * @author Fl�vio Tavares
	 */
	private List<Producaodiaria> validateAndLoadItens(WebRequestContext request){
		String itens = request.getParameter("itens");
		if(StringUtils.isBlank(itens)){
			throw new SinedException("Nenhuma medi��o informada.");
		}
		return producaodiariaService.findByWhereIn(itens);
	}
	
	/**
	 * Action para estornar medi��es faturadas. Os c�digos devem ser informados pelo par�metro <code>itens</code>.
	 * 
	 * @see #validateAndLoadItens(WebRequestContext)
	 * @see #validateItensDoEstornar(List)
	 * @see br.com.linkcom.sined.geral.service.ProducaodiariaService#doEstornarProducoesDiarias(List)
	 * 
	 * @param request
	 * @return
	 * @author Fl�vio Tavares
	 */
	@OnErrors(LISTAGEM)
	@Action("doEstornar")
	public ModelAndView doEstornar(WebRequestContext request){
		if(!SinedUtil.isUserHasAction("ESTORNAR_PRODUCAODIARIA")){
			throw new SinedException("O usu�rio n�o tem permiss�o para executar esta a��o. Para mais informa��es entre em contato " +
					"com o administrador do sistema.");
		}
		
		List<Producaodiaria> listaMedicao = this.validateAndLoadItens(request);
		this.validateItensDoEstornar(listaMedicao);
		
		int total = listaMedicao.size();
		int estornados = producaodiariaService.doEstornarProducoesDiarias(listaMedicao);
		int naoEstornados = total - estornados;
		
		if(estornados > 0){
			if(estornados == 1)
				request.addMessage("Medi��o estornada com sucesso.");
			else
				request.addMessage(estornados + " medi��es foram estornadas com sucesso.");
		}
		if(naoEstornados > 0){
			if(naoEstornados == 1)
				request.addMessage("Medi��o n�o p�de ser estornada.", MessageType.WARN);
			else
				request.addMessage(estornados + " medi��es n�o puderam ser estornadas.", MessageType.WARN);
		}
		
		return sendRedirectToAction(LISTAGEM);
	}
	
	/**
	 * Valida a lista de produ��o di�ria para o fluxo estornar.
	 * 
	 * @param listaMedicao
	 * @author Fl�vio Tavares
	 */
	private void validateItensDoEstornar(List<Producaodiaria> listaMedicao){
		if(SinedUtil.isListEmpty(listaMedicao)){
			throw new SinedException("Nenhuma medi��o selecionada.");
		}
		
		for (Producaodiaria pd : listaMedicao) {
			
			/*
			 * Verifica se a medi��o foi faturada.
			 */
			if(pd.getDtfatura() == null){
				throw new SinedException("Existem medi��es n�o faturadas.");
			}
		}
	}
	
	
	public ModelAndView doSelecionaNota(WebRequestContext request, Producaodiaria producaodiaria){
		
		String whereIn = producaodiariaService.loadWithProjeto(SinedUtil.getItensSelecionados(request));
		if(whereIn.split(",").length > 1){
			request.addError("Devem ser selecionadas medi��es do mesmo projeto.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		request.setAttribute("listaNota", notaFiscalServicoService.findForSelect(whereIn));
		request.setAttribute("clearBase", true);
		return new ModelAndView("direct:crud/popup/selecionaNota");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Producaodiaria form) throws Exception {
		
		if(form.getTarefa() != null && form.getTarefa().getCdtarefa() != null){
			Double qtdetarefa = tarefaService.getQtdePrevistaTarefa(form.getTarefa());
			Double qtderealizada = producaodiariaService.getQtdeRealizadaTarefa(form.getTarefa());
			
			form.setQtderealizada(qtderealizada);
			form.setQtderestante(qtdetarefa - qtderealizada);
		}
		
	}
	
	/**
	 * M�todo chamado via ajax que carrega a qtde da tarefa
	 * 
	 * @param request
	 * @param tarefa
	 * @author Tom�s Rabelo
	 */
	public ModelAndView getQtdTarefaAjax(WebRequestContext request, Producaodiaria producaodiaria){
		Double qtdetarefa = tarefaService.getQtdePrevistaTarefa(producaodiaria.getTarefa());
		Double qtderealizada = producaodiariaService.getQtdeRealizadaTarefa(producaodiaria.getTarefa());
		Unidademedida unidademedida = unidademedidaService.findByTarefa(producaodiaria.getTarefa());
		
		return new JsonModelAndView()
							.addObject("qtdetarefa", SinedUtil.descriptionDecimal(qtdetarefa))
							.addObject("qtderealizada", SinedUtil.descriptionDecimal(qtderealizada))
							.addObject("qtderestante", SinedUtil.descriptionDecimal(qtdetarefa - qtderealizada))
							.addObject("unidademedidanome", unidademedida!=null ? unidademedida.getNome() : "")
							;
	}
	
}
