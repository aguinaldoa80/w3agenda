package br.com.linkcom.sined.modulo.projeto.controller.crud;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Customaoobraitem;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.service.CustomaoobraitemService;
import br.com.linkcom.sined.geral.service.CustomaoobraitemgrupoService;
import br.com.linkcom.sined.geral.service.OrcamentoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.CustomaoobraitemFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/projeto/crud/Customaoobraitem", authorizationModule=CrudAuthorizationModule.class)
public class CustomaoobraitemCrud extends CrudControllerSined<CustomaoobraitemFiltro, Customaoobraitem, Customaoobraitem> {
	
	protected UnidademedidaService unidademedidaService;
	protected CustomaoobraitemgrupoService customaoobraitemgrupoService;
	protected CustomaoobraitemService customaoobraitemService;
	protected OrcamentoService orcamentoService;
	
	public void setCustomaoobraitemService(
			CustomaoobraitemService customaoobraitemService) {
		this.customaoobraitemService = customaoobraitemService;
	}
	public void setCustomaoobraitemgrupoService(
			CustomaoobraitemgrupoService customaoobraitemgrupoService) {
		this.customaoobraitemgrupoService = customaoobraitemgrupoService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	
	@Override
	protected Customaoobraitem criar(WebRequestContext request,	Customaoobraitem form) throws Exception {
		Customaoobraitem bean = super.criar(request, form);
		String copiar = request.getParameter("copiar");
		if(copiar != null && "true".equals(copiar)){
			bean = customaoobraitemService.criaCopia(form);
		}else {
			String cdorcamento = request.getParameter("cdorcamento");
			if(StringUtils.isNotEmpty(cdorcamento)){
				bean.setOrcamento(orcamentoService.load(new Orcamento(Integer.parseInt(cdorcamento)), "orcamento.cdorcamento, orcamento.nome"));
			}
		}
		return bean;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Customaoobraitem form)throws Exception {
		request.setAttribute("listaUnidademedida", unidademedidaService.findAtivos(form.getUnidademedida()));		
		request.setAttribute("listaCustomaoobraitemgrupo", customaoobraitemgrupoService.findAtivos(form.getCustomaoobraitemgrupo()));		
	}
	
	@Override
	protected void salvar(WebRequestContext request, Customaoobraitem bean)throws Exception {
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_customaoobraitem_descricao")) {throw new SinedException("Item de Custo de M�o de Obra j� cadastrado no sistema.");}	
		}
	}
	
	/**
	 * Action que abre a popup para a altera��o do percentual de venda
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/01/2015
	 */
	public ModelAndView abrirAlteraPercentualVenda(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.trim().equals("")){
			throw new SinedException("Nenhum item selecionado");
		}
		
		Customaoobraitem bean = new Customaoobraitem();
		bean.setWhereIn(whereIn);
		return new ModelAndView("direct:/crud/popup/alterarPercentualVenda", "bean", bean);
	}
	
	/**
	 * Action que faz a atualiza��o do custo de venda
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 12/01/2015
	 */
	public void saveAlteraPercentualVenda(WebRequestContext request, Customaoobraitem bean){
		String whereIn = bean.getWhereIn();
		Double percentualvenda = bean.getPercentualvenda();
		int sucesso = 0;
		
		List<Customaoobraitem> lista = customaoobraitemService.findForAlteracaoPercentual(whereIn);
		for (Customaoobraitem customaoobraitem : lista) {
			try{
				Money custounitario = customaoobraitem.getCustounitario();
				Double quantidade = customaoobraitem.getQuantidade();
				Money custovenda = new Money(quantidade).multiply(custounitario).multiply(new Money(percentualvenda).divide(new Money(100d)));
				customaoobraitemService.updatePercentualVenda(customaoobraitem, percentualvenda);
				customaoobraitemService.updateCustoVenda(customaoobraitem, custovenda);
				sucesso++;
			} catch (Exception e) {
				request.addError("Erro ao alterar o percentual do item " + customaoobraitem.getDescricao() + ": " + e.getMessage());
			}
		}
		
		if(sucesso > 0)	request.addMessage(sucesso + " item(ns) alterado(s) com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
}
