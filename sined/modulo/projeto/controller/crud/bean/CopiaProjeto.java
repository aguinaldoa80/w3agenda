package br.com.linkcom.sined.modulo.projeto.controller.crud.bean;

import java.sql.Timestamp;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.Log;

public class CopiaProjeto implements Log{

	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Boolean copiarRecursosHumanos;
	protected Boolean copiarRecursosGerais;
	protected Boolean copiarPlanejamentoNovoProjeto;
	protected String nome;
	protected Boolean tarefas;
	protected Boolean atividades;
	protected Boolean medicoes;
	protected Boolean apontamentos;
	
	{
		this.copiarRecursosGerais = Boolean.TRUE;
		this.copiarRecursosHumanos = Boolean.TRUE;
	}
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	@DisplayName("Copiar recursos humanos")
	public Boolean getCopiarRecursosHumanos() {
		return copiarRecursosHumanos;
	}
	@DisplayName("Copiar recursos gerais")
	public Boolean getCopiarRecursosGerais() {
		return copiarRecursosGerais;
	}
	@DisplayName("Copiar planejamento em novo projeto")
	public Boolean getCopiarPlanejamentoNovoProjeto() {
		return copiarPlanejamentoNovoProjeto;
	}
	@Required
	@MaxLength(50)
	@DisplayName("Nome do novo projeto")
	public String getNome() {
		return nome;
	}
	public Boolean getTarefas() {
		return tarefas;
	}
	public Boolean getAtividades() {
		return atividades;
	}
	@DisplayName("Medi��es")
	public Boolean getMedicoes() {
		return medicoes;
	}
	public Boolean getApontamentos() {
		return apontamentos;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setTarefas(Boolean tarefas) {
		this.tarefas = tarefas;
	}
	public void setAtividades(Boolean atividades) {
		this.atividades = atividades;
	}
	public void setMedicoes(Boolean medicoes) {
		this.medicoes = medicoes;
	}
	public void setApontamentos(Boolean apontamentos) {
		this.apontamentos = apontamentos;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setCopiarRecursosHumanos(Boolean copiarRecursosHumanos) {
		this.copiarRecursosHumanos = copiarRecursosHumanos;
	}
	public void setCopiarRecursosGerais(Boolean copiarRecursosGerais) {
		this.copiarRecursosGerais = copiarRecursosGerais;
	}
	public void setCopiarPlanejamentoNovoProjeto(
			Boolean copiarPlanejamentoNovoProjeto) {
		this.copiarPlanejamentoNovoProjeto = copiarPlanejamentoNovoProjeto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	/*API*/
	public Integer getCdusuarioaltera() {
		return null;
	}
	public Timestamp getDtaltera() {
		return null;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
	}
	public void setDtaltera(Timestamp dtaltera) {
	}
	
}
