package br.com.linkcom.sined.modulo.projeto.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.Enumeration.ApuracaoResultadoProjetoListagemTipo;

public class ApuracaoResultadoProjetoListagemBean {

	private Integer id;
	private Date data;
	private String descricao;
	private Double valor;
	private ApuracaoResultadoProjetoListagemTipo tipo;
	
	private Integer idItem;
	private String centrocustoNome;
	private String pessoaMaterial;
	private String numeroDocumento;
	private Double quantidade;
	private String unidademedidaSimbolo;
	
	public ApuracaoResultadoProjetoListagemBean() {
	}
	
	public ApuracaoResultadoProjetoListagemBean(Integer id, Date data,
			String descricao, Double valor,
			ApuracaoResultadoProjetoListagemTipo tipo, Integer idItem,
			String centrocustoNome, String pessoaMaterial,
			String numeroDocumento, Double quantidade,
			String unidademedidaSimbolo) {
		super();
		this.id = id;
		this.data = data;
		this.descricao = descricao;
		this.valor = valor;
		this.tipo = tipo;
		this.idItem = idItem;
		this.centrocustoNome = centrocustoNome;
		this.pessoaMaterial = pessoaMaterial;
		this.numeroDocumento = numeroDocumento;
		this.quantidade = quantidade;
		this.unidademedidaSimbolo = unidademedidaSimbolo;
	}

	public Integer getId() {
		return id;
	}
	public Date getData() {
		return data;
	}
	public String getDescricao() {
		return descricao;
	}
	public Double getValor() {
		return valor;
	}
	public ApuracaoResultadoProjetoListagemTipo getTipo() {
		return tipo;
	}
	public Integer getIdItem() {
		return idItem;
	}

	public String getCentrocustoNome() {
		return centrocustoNome;
	}

	public String getPessoaMaterial() {
		return pessoaMaterial;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public String getUnidademedidaSimbolo() {
		return unidademedidaSimbolo;
	}

	public void setIdItem(Integer idItem) {
		this.idItem = idItem;
	}

	public void setCentrocustoNome(String centrocustoNome) {
		this.centrocustoNome = centrocustoNome;
	}

	public void setPessoaMaterial(String pessoaMaterial) {
		this.pessoaMaterial = pessoaMaterial;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setUnidademedidaSimbolo(String unidademedidaSimbolo) {
		this.unidademedidaSimbolo = unidademedidaSimbolo;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setTipo(ApuracaoResultadoProjetoListagemTipo tipo) {
		this.tipo = tipo;
	}
	
	
}
