package br.com.linkcom.sined.modulo.projeto.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Valorreferencia;

public class AtualizarValorreferenciaBean {

	protected List<Valorreferencia> listaReferencia = new ListSet<Valorreferencia>(Valorreferencia.class);
	protected List<Valorreferencia> listaSemValorMaterial = new ListSet<Valorreferencia>(Valorreferencia.class);
	protected List<Valorreferencia> listaSemValorCargo = new ListSet<Valorreferencia>(Valorreferencia.class);
	protected Boolean fromOrcamento;
	protected Planejamento planejamento;
	
	public AtualizarValorreferenciaBean() {
	}
	
	public AtualizarValorreferenciaBean(List<Valorreferencia> listaReferencia,
			List<Valorreferencia> listaSemValorMaterial,
			List<Valorreferencia> listaSemValorCargo, Boolean fromOrcamento, Planejamento planejamento) {
		this.listaReferencia = listaReferencia;
		this.listaSemValorMaterial = listaSemValorMaterial;
		this.listaSemValorCargo = listaSemValorCargo;
		this.fromOrcamento = fromOrcamento;
		this.planejamento = planejamento;
	}

	public List<Valorreferencia> getListaReferencia() {
		return listaReferencia;
	}
	public List<Valorreferencia> getListaSemValorMaterial() {
		return listaSemValorMaterial;
	}

	public List<Valorreferencia> getListaSemValorCargo() {
		return listaSemValorCargo;
	}
	
	public Boolean getFromOrcamento() {
		return fromOrcamento;
	}
	
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	
	public void setFromOrcamento(Boolean fromOrcamento) {
		this.fromOrcamento = fromOrcamento;
	}

	public void setListaSemValorMaterial(List<Valorreferencia> listaSemValorMaterial) {
		this.listaSemValorMaterial = listaSemValorMaterial;
	}

	public void setListaSemValorCargo(List<Valorreferencia> listaSemValorCargo) {
		this.listaSemValorCargo = listaSemValorCargo;
	}

	public void setListaReferencia(List<Valorreferencia> listaReferencia) {
		this.listaReferencia = listaReferencia;
	}
	
}
