package br.com.linkcom.sined.modulo.projeto.controller.crud.bean.Enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum RelComparativoIndiceSituacao {
	
	ALTERADO				(0, "Alterado"),
	MANTIDO					(1, "Mantido"),
	EXCLUIDO				(2, "Exclu�do");
	
	private Integer value;
	private String nome;
	
	private RelComparativoIndiceSituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
