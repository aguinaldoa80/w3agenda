package br.com.linkcom.sined.modulo.projeto.controller.crud.bean.Enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ApuracaoResultadoProjetoListagemTipo {
	
	CONTA_PAGAR				(0, "Conta a pagar"),
	CONTA_RECEBER			(1, "Conta a receber"),
	MOVIMENTACAO_FINANCEIRA	(2, "Movimentação financeira"),
	MOVIMENTACAO_ESTOQUE	(3, "Movimentação de estoque"),
	;
	
	private Integer value;
	private String nome;
	
	private ApuracaoResultadoProjetoListagemTipo(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
