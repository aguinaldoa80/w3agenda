package br.com.linkcom.sined.modulo.projeto.controller.crud.bean;

import java.util.HashMap;
import java.util.Map;

public class ComposicaomaoobrafomulaVO {
	
	private double salario = 0d;
	private Map<String, Double> mapCustomaoobraitem = new HashMap<String, Double>();
	
	public double getSalario() {
		return salario;
	}
	
	public Map<String, Double> getMapCustomaoobraitem() {
		return mapCustomaoobraitem;
	}
	
	public void setMapCustomaoobraitem(Map<String, Double> mapCustomaoobraitem) {
		this.mapCustomaoobraitem = mapCustomaoobraitem;
	}
	
	public void setSalario(double salario) {
		this.salario = salario;
	}
	
	public double item(String identificador){
		Double valor = mapCustomaoobraitem.get(identificador);
		if(valor == null) valor = 0d;
		return valor;
	}

}
