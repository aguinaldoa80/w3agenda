package br.com.linkcom.sined.modulo.projeto.controller.crud.bean;



public class ApuracaoResultadoProjetoBean {
	
	private Integer codigo;
	private String identificador;
	private String nome;
	private Double valor;
	
	private Integer codigoPai;
	private Boolean aberto = Boolean.FALSE;
	private Boolean grafico = Boolean.TRUE;
	private Boolean retirarGrafico = Boolean.FALSE;
	private Boolean haveFilhos = Boolean.FALSE;
	
	public ApuracaoResultadoProjetoBean() {
	}
	
	public ApuracaoResultadoProjetoBean(Integer codigo, String identificador,
			String nome, Double valor, Integer codigoPai, Boolean aberto,
			Boolean grafico, Boolean haveFilhos) {
		super();
		this.codigo = codigo;
		this.identificador = identificador;
		this.nome = nome;
		this.valor = valor;
		this.codigoPai = codigoPai;
		this.aberto = aberto;
		this.grafico = grafico;
		this.haveFilhos = haveFilhos;
	}

	public Integer getCodigo() {
		return codigo;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getNome() {
		return nome;
	}
	public Double getValor() {
		return valor;
	}
	public Boolean getAberto() {
		return aberto;
	}
	public Boolean getGrafico() {
		return grafico;
	}
	public Integer getCodigoPai() {
		return codigoPai;
	}
	public Boolean getHaveFilhos() {
		return haveFilhos;
	}
	public Boolean getRetirarGrafico() {
		return retirarGrafico;
	}
	public void setRetirarGrafico(Boolean retirarGrafico) {
		this.retirarGrafico = retirarGrafico;
	}
	public void setHaveFilhos(Boolean haveFilhos) {
		this.haveFilhos = haveFilhos;
	}
	public void setCodigoPai(Integer codigoPai) {
		this.codigoPai = codigoPai;
	}
	public void setGrafico(Boolean grafico) {
		this.grafico = grafico;
	}
	public void setAberto(Boolean aberto) {
		this.aberto = aberto;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
