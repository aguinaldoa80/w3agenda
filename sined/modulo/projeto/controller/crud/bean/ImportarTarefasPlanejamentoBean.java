package br.com.linkcom.sined.modulo.projeto.controller.crud.bean;

import java.io.UnsupportedEncodingException;

import br.com.linkcom.neo.util.Util;


public class ImportarTarefasPlanejamentoBean {
	
	private Integer id;
	private Integer idPai;
	private String descricao;
	private String unidade;
	private Double qtde;
	private String composicao;
	
	public Integer getId() {
		return id;
	}
	public Integer getIdPai() {
		return idPai;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getUnidade() {
		return unidade;
	}
	public Double getQtde() {
		return qtde;
	}
	public String getComposicao() {
		return composicao;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public void setIdPai(Integer idPai) {
		this.idPai = idPai;
	}
	public void setDescricao(String descricao) {
		try {
			byte[] stringArray = new String(Util.strings.emptyIfNull(descricao).getBytes()).getBytes();
			this.descricao = new String(stringArray, "LATIN1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			this.descricao = descricao;
		}
	}
	public void setUnidade(String unidade) {
		try {
			byte[] stringArray = new String(Util.strings.emptyIfNull(unidade).getBytes()).getBytes();
			this.unidade = new String(stringArray, "LATIN1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			this.unidade = unidade;
		}
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setComposicao(String composicao) {
		try {
			byte[] stringArray = new String(Util.strings.emptyIfNull(composicao).getBytes()).getBytes();
			this.composicao = new String(stringArray,"LATIN1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			this.composicao = composicao;
		}
	}	
}