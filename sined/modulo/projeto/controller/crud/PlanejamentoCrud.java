package br.com.linkcom.sined.modulo.projeto.controller.crud;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentodespesa;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.CopiaProjeto;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.ImportarTarefasPlanejamentoBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.PlanejamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path={"/projeto/crud/Planejamento",
				"/agro/crud/Planejamento",
		},
		authorizationModule=CrudAuthorizationModule.class
)
public class PlanejamentoCrud extends CrudControllerSined<PlanejamentoFiltro, Planejamento, Planejamento>{
	
	private ProjetoService projetoService;
	private PlanejamentoService planejamentoService;
	private AvisoService avisoService;
	private TarefaService tarefaService;
	private ParametrogeralService parametrogeralService;
	private MotivoavisoService motivoavisoService;
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, PlanejamentoFiltro filtro) throws Exception {
		request.setAttribute("listaSituacao", Planejamentosituacao.findAll());
		request.setAttribute("SOLICITARCOMPRAPROJETO", parametrogeralService.buscaValorPorNome(Parametrogeral.SOLICITARCOMPRAPROJETO));
	}
	
	@Override
	protected void salvar(WebRequestContext request, Planejamento bean)throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PLANEJAMENTO_DESCRICAO")) {
				throw new SinedException("Planejamento j� cadastrado no sistema.");
			}	
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Planejamento form) throws Exception {		
		String copiar = request.getParameter("copiar");
		String cdprojeto = request.getParameter("cdplanejamento");
		if (copiar != null && cdprojeto != null) {
			form.setPlanejamentosituacao(Planejamentosituacao.EM_ABERTO);
			form.setCdusuarioaltera(null);
			form.setDtaltera(null);
			form.setCdplanejamento(null);
		}
//		request.setAttribute("listaContagerencial", contagerencialService.findAnaliticas(Tipooperacao.TIPO_DEBITO));
		request.setAttribute("SOLICITARCOMPRAPROJETO", parametrogeralService.buscaValorPorNome(Parametrogeral.SOLICITARCOMPRAPROJETO));
	}
	
	
	/**
	 * M�todo para carregar via AJAX a data do projeto.
	 *
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#load
	 * @param request
	 * @param planejamento
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public void ajaxDtprojeto(WebRequestContext request, Planejamento planejamento) throws Exception {
		if (planejamento == null || planejamento.getProjeto() == null || planejamento.getProjeto().getCdprojeto() == null) {
			throw new SinedException("Projeto n�o pode ser nulo.");
		}
		Projeto projeto = projetoService.load(planejamento.getProjeto(), "projeto.dtprojeto");
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(new SimpleDateFormat("dd/MM/yyyy").format(projeto.getDtprojeto()));
	}
	
	@Override
	protected void validateBean(Planejamento bean, BindException errors) {
		List<Planejamentosituacao> lista = new ArrayList<Planejamentosituacao>();
		lista.add(Planejamentosituacao.EM_ABERTO);
		lista.add(Planejamentosituacao.AUTORIZADA);
		if (planejamentoService.isOutroProjetoComSituacao(bean, lista)) {
			errors.reject("001","J� existe um planejamento deste projeto com situa��o 'EM ABERTO' ou 'AUTORIZADA'.");
		}
		
		List<Planejamentodespesa> listaPlanejamentodespesa = new ArrayList<Planejamentodespesa>(bean.getListaPlanejamentodespesa());
		Boolean erro = false;
		if(listaPlanejamentodespesa != null){
			for (int i = 0; i < listaPlanejamentodespesa.size(); i++) {
				for (int j = 0; j < listaPlanejamentodespesa.size(); j++) {
					if(i != j){
						if(listaPlanejamentodespesa.get(i).getFornecimentotipo().equals(listaPlanejamentodespesa.get(j).getFornecimentotipo())){
							errors.reject("001","N�o se pode cadastrar duas despesas com o mesmo tipo de despesa.");
							erro = true;
							break;
						}
					}
				}
				if (erro) {
					break;
				}
			}
		}
	}
	
	/**
	 * Action para autorizar os planejamentos selecionados na tela.
	 *
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public ModelAndView autorizar(WebRequestContext request) throws Exception{
		String whereIn = SinedUtil.getItensSelecionados(request);
		String comunicado = request.getParameter("comunicado");
		
		List<Planejamento> lista = planejamentoService.loadWithSituacao(whereIn);
		for (Planejamento planejamento : lista) {
			if (!planejamento.getPlanejamentosituacao().equals(Planejamentosituacao.EM_ABERTO)) {
				request.addError("Para autorizar um planejamento, o mesmo deve estar na situa��o 'EM ABERTO'.");
				return new ModelAndView("redirect:/projeto/crud/Planejamento");
			}
		}
		
		planejamentoService.updateSituacao(whereIn,Planejamentosituacao.AUTORIZADA);		
		request.addMessage("Planejamento(s) autorizado(s) com sucesso.");
		
		if(comunicado != null && !comunicado.equals("")){
			List<Aviso> listaAviso = montaAvisosPlanejamento(MotivoavisoEnum.PLANEJAMENTO_AUTORIZADO, lista, "Projeto autorizado");
			avisoService.salvarAvisos(listaAviso, true);
		}
		
		return new ModelAndView("redirect:/projeto/crud/Planejamento");
	}
	
	public List<Aviso> montaAvisosPlanejamento(MotivoavisoEnum motivo, List<Planejamento> planejamentos, String assunto) {
		List<Aviso> avisos = new ListSet<Aviso>(Aviso.class);
		Motivoaviso motivoAviso = motivoavisoService.findByMotivo(motivo);
		if(motivoAviso != null){
			for (Planejamento planejamento : planejamentos)
				if(motivoAviso.getTipoaviso() != null){
					avisos.add(new Aviso("Projeto autorizado", "Nome do projeto: "+planejamento.getProjeto().getNome(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
							motivoAviso.getUsuario(), AvisoOrigem.PLANEJAMENTO, planejamento.getCdplanejamento(), planejamento.getProjeto().getEmpresa(), 
							planejamento.getProjeto().getCdprojeto().toString(), motivoAviso));
			}
		}
		
		return avisos;
	}
	
	/**
	 * Action para cancelar os planjamento selecionados na tela.
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cancelar(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String[] ids = whereIn.split(",");
		
		List<Planejamento> lista = planejamentoService.loadWithSituacao(whereIn);
		for (Planejamento planejamento : lista) {
			if (planejamento.getPlanejamentosituacao().equals(Planejamentosituacao.CANCELADA)) {
				request.addError("Para cancelar um planejamento, o mesmo deve estar na situa��o 'EM ABERTO' ou 'AUTORIZADA'.");
				return new ModelAndView("redirect:/projeto/crud/Planejamento");
			}
		}
		
		for (int i = 0; i < ids.length; i++) {
			if(planejamentoService.haveSolicitacaocompra(new Planejamento(Integer.parseInt(ids[i])))){
				request.addError("N�o � poss�vel cancelar planejamento. Existe(m) solicita��o(�es) de compras 'Em aberto', 'Autorizada' ou 'Em processo de compra'.");
				return new ModelAndView("redirect:/projeto/crud/Planejamento"); 
			}			
		}
		
		planejamentoService.updateSituacao(whereIn,Planejamentosituacao.CANCELADA);		
		request.addMessage("Planejamento(s) cancelado(s) com sucesso.");
		
		return new ModelAndView("redirect:/projeto/crud/Planejamento"); 
	}
	
	/**
	 * Action para a enrada de dados ao copiar projeto.
	 * 
	 * @param request
	 * @param copiaProjeto
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("copiaPlanejamento")
	public ModelAndView doCopiaPlanejamento(WebRequestContext request, CopiaProjeto copiaProjeto){
		copiaProjeto.setProjeto(planejamentoService.load(copiaProjeto.getPlanejamento()).getProjeto());
		request.setAttribute("copiaProjeto", copiaProjeto);
		request.setAttribute("clearBase", Boolean.TRUE);
		request.setAttribute("TEMPLATE_beanName", "copiaProjeto");
		request.setAttribute("hidden", Boolean.TRUE);
		return new ModelAndView("crud/popup/copiaProjeto");
	}
	
	/**
	 * Action para salvar a c�pia de projetos.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#saveCopiaProjeto(CopiaProjeto)
	 * @param request
	 * @param copiaProjeto
	 * @return
	 * @author Fl�vio Tavares
	 */
	@OnErrors("copiaPlanejamento")
	@Action("salvarCopia")
	public ModelAndView salvarCopiaPlanejamento(WebRequestContext request, CopiaProjeto copiaProjeto){
		
		try {
			projetoService.saveCopiaProjeto(copiaProjeto);
			request.addMessage("Planejamento copiado com sucesso.");
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PROJETO_NOME")) {
				throw new SinedException("Projeto j� cadastrado no sistema.");
			}else if (DatabaseError.isKeyPresent(e, "PROJETO_CNPJ_KEY")) {
				throw new SinedException("Cnpj j� cadastrado.");
			}else if (DatabaseError.isKeyPresent(e, "PROJETO_CEI_KEY")){
				throw new SinedException("Cei j� cadastrado.");
			}else {
				throw new SinedException(e.getMessage());
			}
		}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent()
			.println("<script type='text/javascript'>" +
						"window.top.$.akModalHideAndRedirect(window.location.href);" +
						"window.location.href=window.location.href" +
					"</script>");
		
		return null;
	}
	
	/**
	 * 
	 * @param request
	 * @param filtro
	 * @author Thiago Clemente
	 * @throws Exception
	 * 
	 */
	public ModelAndView importarTarefas(WebRequestContext request, PlanejamentoFiltro filtro) throws Exception {
		return new ModelAndView("direct:/crud/popup/importarTarefas").addObject("filtro", filtro);
	}
	
	/**
	 * 
	 * @param request
	 * @param filtro
	 * @author Thiago Clemente
	 * @throws Exception
	 * 
	 */
	public ModelAndView processarImportarTarefas(WebRequestContext request, PlanejamentoFiltro filtro) throws Exception {
		
		try {
			//Como � arquivo CSV, pode vir com qualquer formato texto, obedecendo o formato de ponto e v�rgula
			Arquivo arquivo = filtro.getArquivo();
			
			List<ImportarTarefasPlanejamentoBean> listaBean = tarefaService.validarImportarTarefas(arquivo.getContent());
			tarefaService.validarArquivoImportarTarefas(listaBean);
			List<Tarefa> listaTarefa = tarefaService.getListaTarefa(listaBean, filtro.getPlanejamento());
			tarefaService.saveListaTarefa(listaTarefa);
			
			request.addMessage("Tarefas importadas com sucesso.", MessageType.INFO);
		} catch(SinedException s){
			request.addError(s.getMessage());	
			s.printStackTrace();
		} catch (Exception e) {
			request.addError("Erro no processamento do arquivo.");
			e.printStackTrace();
		} finally {
			filtro.setArquivo(null);
			filtro.setPlanejamento(null);
		}
		
		request.getServletResponse().setContentType("text/html");
		
		View.getCurrent().println("<script>" +
								  "window.parent.location = window.parent.location;" +
								  "window.parent.$.akModalRemove(true);" +
								  "</script>");
		
		return null;
	}
}