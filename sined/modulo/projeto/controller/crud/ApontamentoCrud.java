package br.com.linkcom.sined.modulo.projeto.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.enumeration.ApontamentoSituacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoProducaodiaria;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.CargoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProducaodiariaService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ApontamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path={"/projeto/crud/Apontamento","/agro/crud/Apontamento"}, authorizationModule=CrudAuthorizationModule.class)
public class ApontamentoCrud extends CrudControllerSined<ApontamentoFiltro, Apontamento, Apontamento>{

	private CargoService cargoService;
	private MaterialService materialService;
	private ApontamentoService apontamentoService;
	private ProducaodiariaService producaodiariaService;
	private AtividadetipoService atividadetipoService;
	private ColaboradorService colaboradorService;
	private ContratoService contratoService;
	private ParametrogeralService parametrogeralService;
	private ProjetoService projetoService;
	
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {this.atividadetipoService = atividadetipoService;}	
	public void setCargoService(CargoService cargoService) {this.cargoService = cargoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setApontamentoService(ApontamentoService apontamentoService) {this.apontamentoService = apontamentoService;}
	public void setProducaodiariaService(ProducaodiariaService producaodiariaService) {this.producaodiariaService = producaodiariaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	
	@Override
	protected ListagemResult<Apontamento> getLista(WebRequestContext request, ApontamentoFiltro filtro) {
		ListagemResult<Apontamento> listagemResult = super.getLista(request, filtro);
		List<Apontamento> list = listagemResult.list();
		
//		for (Apontamento apontamento : list) {
//			if(apontamento.getApontamentoTipo().equals(ApontamentoTipo.FUNCAO)){
//				apontamento.setListaApontamentoHoras(apontamentoHorasService.findByApontamento(apontamento));
//			}
//		}
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdapontamento", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(apontamentoService.findListagem(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}
		
		List<GenericBean> listaSituacao = new ArrayList<GenericBean>();
		listaSituacao.add(new GenericBean(ApontamentoSituacaoEnum.REGISTRADO.name(), ApontamentoSituacaoEnum.REGISTRADO.getNome()));
		listaSituacao.add(new GenericBean(ApontamentoSituacaoEnum.AUTORIZADO.name(), ApontamentoSituacaoEnum.AUTORIZADO.getNome()));
		listaSituacao.add(new GenericBean(ApontamentoSituacaoEnum.PAGO.name(), ApontamentoSituacaoEnum.PAGO.getNome()));
		
		request.setAttribute("listaSituacao", listaSituacao);
		
		return listagemResult;
	}
	
	@Override
	protected Apontamento criar(WebRequestContext request, Apontamento form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdapontamento() != null){
			form = apontamentoService.loadForEntrada(form);
			
			form.setCdapontamento(null);
			form.setDtaltera(null);
			form.setCdusuarioaltera(null);
			
			if(form.getListaApontamentoHoras() != null){
				for (ApontamentoHoras apontamentoHoras : form.getListaApontamentoHoras()) {
					apontamentoHoras.setCdapontamentohoras(null);
				}
			}
			
			form.setDtapontamento(SinedDateUtils.currentDate());
			
			return form;
		} else {
			Apontamento bean = super.criar(request, form);
			
//			Colaborador colaborador = new Colaborador(((Usuario)request.getUser()).getCdpessoa());
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			bean.setColaborador(colaborador);
			
			
			
			
			return bean;
		}
	}
	
	@Override
	protected void listagem(WebRequestContext request, ApontamentoFiltro filtro) throws Exception {
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		if(filtro.getCliente() != null && filtro.getCliente().getCdpessoa() != null)
			listaContrato = contratoService.findByClienteAbertosApontamento(filtro.getCliente());
		if(filtro.getContrato() != null && filtro.getContrato().getCdcontrato() != null)
			request.setAttribute("contratoFiltro", "br.com.linkcom.sined.geral.bean.Contrato[cdcontrato=" +filtro.getContrato().getCdcontrato() + "]");
		
		request.setAttribute("listaContrato", listaContrato);
		
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Apontamento form) throws Exception {
		Boolean obrigar_contrato = Boolean.FALSE;
		try{
			String param = parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CONTRATO_APONTAMENTO);
			if(param != null && param.trim().equals("TRUE")){
				obrigar_contrato = Boolean.TRUE;
			}
		} catch (Exception e) {
		}
		if(obrigar_contrato){
			request.setAttribute("style_obrigatorio_contrato", "required");
		}
		
		//if(request.getParameter("ACAO").equals("criar")){
		if (form.getCdapontamento()==null){
			String tipoapontamentopadrao = parametrogeralService.getValorPorNome(Parametrogeral.TIPOAPONTAMENTOPADRAO);
			if (Parametrogeral.TIPOAPONTAMENTOPADRAO_ATIVIDADE.equalsIgnoreCase(tipoapontamentopadrao)){
				form.setApontamentoTipo(ApontamentoTipo.FUNCAO_ATIVIDADE);
			}else if (Parametrogeral.TIPOAPONTAMENTOPADRAO_HORA.equalsIgnoreCase(tipoapontamentopadrao)){
				form.setApontamentoTipo(ApontamentoTipo.FUNCAO_HORA);
			}else if (Parametrogeral.TIPOAPONTAMENTOPADRAO_MATERIAL.equalsIgnoreCase(tipoapontamentopadrao)){
				form.setApontamentoTipo(ApontamentoTipo.MATERIAL);
			}else {
				form.setApontamentoTipo(ApontamentoTipo.FUNCAO_HORA);
			}
			form.setSituacao(ApontamentoSituacaoEnum.REGISTRADO);
			form.setDtapontamento(new Date(System.currentTimeMillis()));
		}
		
		List<Contrato> listaContrato = new ArrayList<Contrato>();
			if(form.getCliente() != null){
				if(form.getCdapontamento() != null)
					listaContrato = contratoService.findByClienteAbertosApontamento(form.getCliente(), form.getCdapontamento());
				else
				listaContrato = contratoService.findByClienteAbertosApontamento(form.getCliente());
			}
		
		request.setAttribute("listaContrato", listaContrato);
		request.setAttribute("listaAtividadetipo", atividadetipoService.findAtivos(form.getAtividadetipo()));
		if(form.getRequisicao()!= null) {
			request.setAttribute("cdrequisicao", form.getRequisicao().getCdrequisicao());
		}
	}
	
	@Override
	protected void validateBean(Apontamento bean, BindException errors) {
		if(bean.getContrato() != null){
			List<Projeto> lista = projetoService.findByContrato(bean.getContrato());
			
			if(lista != null && lista.size() == 1 && !lista.get(0).equals(bean.getProjeto())){
				errors.reject("001","Projeto selecionado n�o pertence ao Contrato.");	
			}
		}
		if(bean.getApontamentoTipo() != null){
			Boolean achou = Boolean.FALSE;
			if(bean.getApontamentoTipo().equals(ApontamentoTipo.MATERIAL)){
				if(bean.getMaterial() != null && bean.getMaterial().getCdmaterial() != null && bean.getTarefa() != null && bean.getTarefa().getCdtarefa() != null){
					achou = materialService.isMaterialTarefa(bean.getTarefa(), bean.getMaterial());
					if(achou == null || !achou){
						errors.reject("001","Material selecionado n�o pertence a Tarefa.");					
						bean.setMaterial(materialService.load(bean.getMaterial(), "material.cdmaterial, material.nome"));					
					}
				}
				
			}else if(bean.getApontamentoTipo().equals(ApontamentoTipo.FUNCAO_ATIVIDADE) || bean.getApontamentoTipo().equals(ApontamentoTipo.FUNCAO_HORA)){
				if(bean.getColaborador() != null && bean.getCargo() != null){
					if(bean.getTarefa() != null && bean.getTarefa().getCdtarefa() != null){
						achou = colaboradorService.isColaboradorTarefa(bean.getTarefa(), bean.getColaborador());
					}
					if(bean.getCargo() != null && bean.getCargo().getCdcargo() != null && (achou == null || !achou)){
						achou = colaboradorService.isColaboradorCargo(bean.getCargo(), bean.getColaborador());
					}
					if(achou == null || !achou)
						errors.reject("001", "Colaborador n�o est� alocado na Tarefa/Cargo selecionado.");
				}
			}
		}
		super.validateBean(bean, errors);
	}
	
	/**
	 * Action para buscar, via ajax, os cargos de acordo com a sele��o dos combos na tela de entrada.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CargoService#findByApontamentoFiltro(Object)
	 * @param request
	 * @param filtro
	 * @author Fl�vio Tavares
	 */
	public void ajaxCargo(WebRequestContext request, ApontamentoFiltro filtro){
		List<Cargo> listaCargo = cargoService.findByApontamentoFiltro(filtro);
		String lista = SinedUtil.convertToJavaScript(listaCargo, "listaCargo", null);
		View.getCurrent().println(lista);
	}
	
	/**
	 * Action para buscar, via ajax, os materiais de acordo com a sele��o dos combos na tela de entrada.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialService#findByForApontamento(ApontamentoFiltro)
	 * @param request
	 * @param filtro
	 * @author Fl�vio Tavares
	 */
	public void ajaxRecursogeral(WebRequestContext request, ApontamentoFiltro filtro){
		List<Material> listaMaterial = materialService.findByForApontamento(filtro);
		String lista = SinedUtil.convertToJavaScript(listaMaterial, "listaMaterial", null);
		View.getCurrent().println(lista);
	}
	
	/**
	 * Action para faturar medi��es. Os c�digos devem ser informados pelo par�metro <code>itens</code>.
	 * A tela de cadastro de Nota fiscal de servi�o � aberta e, ao ser salva, o sistema associa esta nova nota
	 * �s medi��es informadas.
	 * 
	 * @see #validateAndLoadItens(WebRequestContext)
	 * @see #validateItensDoFaturar(List)
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#carregaAssociaNotaServico(NotaFiscalServico, List)
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#criaNotaServico(List)
	 * @see br.com.linkcom.sined.geral.service.ProducaodiariaService#associaNotaProducoesdiarias(br.com.linkcom.sined.geral.bean.Nota, List)
	 * 
	 * @param request
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	@OnErrors(LISTAGEM)
	@Action("doGerarProducao")
	public ModelAndView doGerarProducao(WebRequestContext request, Nota nota){
		if(!SinedUtil.isUserHasAction("GERAR_PRODUCAODIARIA")){
			throw new SinedException("O usu�rio n�o tem permiss�o para executar esta a��o. Para mais informa��es entre em contato " +
					"com o administrador do sistema.");
		}
		
		List<Apontamento> listaApontamento = apontamentoService.findByWhereIn(request.getParameter("itens"));
		Producaodiaria producaodiaria = null;
		if (listaApontamento.size() == 1) {
			if (listaApontamento.get(0).getTarefa()==null) {
				throw new SinedException("N�o � poss�vel gerar a produ��o para um apontamento sem atividade.");
			}else {
				producaodiaria = new Producaodiaria();
				producaodiaria.setSituacaoProducaodiaria(SituacaoProducaodiaria.A_FATURAR);
				producaodiaria.setTarefa(listaApontamento.get(0).getTarefa());
				producaodiaria.setDtproducao(new Date(System.currentTimeMillis()));
				producaodiaria.setQtde(listaApontamento.get(0).getQtdehoras());
				try {				
					producaodiariaService.saveOrUpdate(producaodiaria);
					request.addMessage("Produ��o di�ria gerada com sucesso!");
				} catch (Exception e) {
					throw new SinedException("Erro ao salvar produ��o di�ria.");
				}
			}
		}else {			
			producaodiaria = new Producaodiaria();
			for (int i = 0; i < listaApontamento.size(); i++) {
				if (i==0) {
					producaodiaria.setSituacaoProducaodiaria(SituacaoProducaodiaria.A_FATURAR);
					producaodiaria.setTarefa(listaApontamento.get(i).getTarefa());
					producaodiaria.setDtproducao(new Date(System.currentTimeMillis()));
					producaodiaria.setQtde(listaApontamento.get(i).getQtdehoras());
					producaodiaria.setDataApontamento(listaApontamento.get(i).getDtapontamento());
				}else {
					if ((producaodiaria.getTarefa() != null && listaApontamento.get(i).getTarefa() != null && !producaodiaria.getTarefa().equals(listaApontamento.get(i).getTarefa())) 
							|| producaodiaria.getDataApontamento().compareTo(listaApontamento.get(i).getDtapontamento())!=0) {
						throw new SinedException("A produ��o di�ria s� pode ser criada para apontamentos de mesma data, tarefa e unidade.");
					}else {						
						producaodiaria.setQtde(producaodiaria.getQtde()+listaApontamento.get(i).getQtdehoras());
					}
				}
			}
			try {				
				producaodiariaService.saveOrUpdate(producaodiaria);
				request.addMessage("Produ��o di�ria gerada com sucesso!");
			} catch (Exception e) {
				throw new SinedException("Erro ao salvar produ��o di�ria.");
			}
		}
			
		return new ModelAndView("redirect:/projeto/crud/Producaodiaria")
					.addObject("ACAO", "listagem")
		;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,ApontamentoFiltro filtro) throws CrudException {
		if (!filtro.isNotFirstTime()) {
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			filtro.setColaborador(colaborador);
			filtro.setDtapontamentoDe(SinedDateUtils.firstDateOfMonth());
		}
		
		apontamentoService.preencheTotais(filtro);
		
		return super.doListagem(request, filtro);
	}
	
	/**
	* M�todo que lista colaborador
	* se a tarefa tiver colaborador alocado, retorna a lista de colaborador da tarefa, sen�o retorno todos colaboradores que dtfim n�o � nulo
	*
	* @param request
	* @param tarefa
	* @since Aug 12, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxColaboradorByTarefa(WebRequestContext request, Tarefa tarefa){
		tarefa.setCdtarefa(request.getParameter("cdtarefa") != null ? Integer.parseInt(request.getParameter("cdtarefa")) : null );
		List<Colaborador> listaColaborador = colaboradorService.findByColaboradorTarefa(tarefa);
		String lista = new String();
		if(listaColaborador != null && !listaColaborador.isEmpty()){
			lista = SinedUtil.convertToJavaScript(listaColaborador, "listaColaborador", null);
		}else{			
			listaColaborador = colaboradorService.findByCargo(null);
			lista = SinedUtil.convertToJavaScript(listaColaborador, "listaColaborador", null);
		}		
		View.getCurrent().println(lista);
	}
	
	/**
	 * M�todo ajax para carregar os contratos
	 * 
	 * @param request
	 * @return
	 * @since 02/02/2012
	 * @author Luiz Silva
	 */
	public void ajaxPreencheComboContratoByCliente(WebRequestContext request){
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		
		String cdcliente = request.getParameter("cdcliente");
		String cdapontamento = request.getParameter("cdapontamento");
		if(cdcliente != null && !"".equals(cdcliente)){
			if(cdapontamento != null && !"".equals(cdapontamento))
				listaContrato = contratoService.findByClienteAbertosApontamento(new Cliente(Integer.parseInt(cdcliente)), Integer.parseInt(cdapontamento));
			else
				listaContrato = contratoService.findByClienteAbertosApontamento(new Cliente(Integer.parseInt(cdcliente)));
		}
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaContrato, "listaContrato", ""));
	}
	
	/**
	 * M�todo ajax para verificar o Material ou Colaborador selecionado. 
	 * Material deve pertencer a tarefa
	 * Colaborador deve pertencer a tarefa ou ao cargo
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialService#isMaterialTarefa(Tarefa tarefa, Material material)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#isColaboradorTarefa(Tarefa tarefa, Colaborador colaborador)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#isColaboradorCargo(Cargo cargo, Colaborador colaborador)
	 * 
	 * @param request
	 * @return
	 * @since 02/02/2012
	 * @author Luiz Silva
	 */
	public void ajaxVerificaColaboradorTarefaOuCargo(WebRequestContext request){
		Boolean achou = Boolean.FALSE;
		String cdcargo = request.getParameter("cdcargo");
		String cdtarefa = request.getParameter("cdtarefa");
		String cdcolaborador = request.getParameter("cdcolaborador");
		String cdmaterial = request.getParameter("cdmaterial");
		
		if(cdmaterial != null && !"".equals(cdmaterial)){
			if(cdtarefa != null && !"".equals(cdtarefa)){
				achou = materialService.isMaterialTarefa(new Tarefa(Integer.parseInt(cdtarefa)), new Material(Integer.parseInt(cdmaterial)));				
			}
		}else if(cdcolaborador != null && !"".equals(cdcolaborador) && cdcargo != null && !"".equals(cdcargo)){			
			if(cdtarefa != null && !"".equals(cdtarefa)){
				achou = colaboradorService.isColaboradorTarefa(new Tarefa(Integer.parseInt(cdtarefa)), new Colaborador(Integer.parseInt(cdcolaborador)));
			}
			if(cdcargo != null && !"".equals(cdcargo) && (achou == null || !achou)){
				achou = colaboradorService.isColaboradorCargo(new Cargo(Integer.parseInt(cdcargo)), new Colaborador(Integer.parseInt(cdcolaborador)));
			}
		}
		View.getCurrent().println("var achou = " + achou + ";");
	}
	
	/**
	 * Action em ajax para sugerir o projeto a partir do contrato selecionado.
	 *
	 * @param request
	 * @param apontamento
	 * @since 05/06/2012
	 * @author Rodrigo Freitas
	 */
	public void ajaxSugereProjeto(WebRequestContext request, Apontamento apontamento){
		if(apontamento == null || apontamento.getContrato() == null || apontamento.getContrato().getCdcontrato() == null){
			throw new SinedException("O contrato n�o pode ser nulo.");
		}
		List<Projeto> lista = projetoService.findByContrato(apontamento.getContrato());
		
		if(lista != null && lista.size() == 1){
			View.getCurrent().println("var projeto = 'br.com.linkcom.sined.geral.bean.Projeto[cdprojeto=" + lista.get(0).getCdprojeto() + "]';");
		} else {
			View.getCurrent().println("var projeto = '<null>';"); 
		}
	}
	
	public ModelAndView autorizar(WebRequestContext request, ApontamentoFiltro filtro) throws Exception {
		
		String itens = request.getParameter("itens");
		boolean ok = apontamentoService.autorizar(itens);
		
		if (ok){
			request.addMessage("Autoriza��o realizada com sucesso.", MessageType.INFO);
		}else {
			request.addMessage("Autoriza��o � permitida somente pelo respons�vel pelo projeto", MessageType.ERROR);
		}
		
		return doListagem(request, filtro);
	}	
}