package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RelacaoCargoFiltro extends FiltroListagemSined {

	protected Orcamento orcamento;
	protected String nomeCargo;
	
	public RelacaoCargoFiltro() {
	}
	
	public RelacaoCargoFiltro(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	@DisplayName("Or�amento")
	@Required
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	@MaxLength(50)
	@DisplayName("Cargo")
	public String getNomeCargo() {
		return nomeCargo;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}
}
