package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ComposicaoOrcamentoFiltro extends FiltroListagemSined {

	protected Orcamento orcamento;
	protected String nome;
	
	@DisplayName("Or�amento")
	@Required
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	@MaxLength(30)
	@DisplayName("Recurso")
	public String getNome() {
		return nome;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
