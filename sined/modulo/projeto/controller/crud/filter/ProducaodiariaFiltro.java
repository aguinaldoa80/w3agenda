package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoProducaodiaria;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProducaodiariaFiltro extends FiltroListagemSined{

	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Tarefa tarefa;
	protected Boolean faturar;
	protected Boolean faturada;
	protected Boolean emaberto;
	protected List<SituacaoProducaodiaria> listaSituacao;
	
	protected Nota nota;
	
	public Projeto getProjeto() {
		return projeto;
	}
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public Tarefa getTarefa() {
		return tarefa;
	}
	@DisplayName("Mostrar")
	public Boolean getFaturar() {
		return faturar;
	}
	@DisplayName("Em aberto")
	public Boolean getEmaberto() {
		return emaberto;
	}
	@DisplayName("Faturada")
	public Boolean getFaturada() {
		return faturada;
	}
	@DisplayName("Situa��o")
	public List<SituacaoProducaodiaria> getListaSituacao() {
		return listaSituacao;
	}
	public Nota getNota() {
		return nota;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setFaturar(Boolean faturar) {
		this.faturar = faturar;
	}
	public void setEmaberto(Boolean emaberto) {
		this.emaberto = emaberto;
	}
	public void setFaturada(Boolean situacao) {
		this.faturada = situacao;
	}
	public void setListaFaturada(List<SituacaoProducaodiaria> listaFaturada) {
		this.listaSituacao = listaFaturada;
	}
	public void setNota(Nota nota) {
		this.nota = nota;
	}
}
