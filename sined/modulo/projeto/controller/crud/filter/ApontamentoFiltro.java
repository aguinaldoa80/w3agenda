package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Atividade;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ApontamentoFiltro extends FiltroListagemSined{

	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Tarefa tarefa;
	protected Atividade atividade;
	protected Cargo cargo;
	protected Colaborador colaborador;
	protected Date dtapontamentoDe;
	protected Date dtapontamentoAte;
	protected Cliente cliente;
	protected Contrato contrato;
	protected Atividadetipo atividadetipo;
	protected ApontamentoTipo apontamentoTipo;
	protected List<GenericBean> listaSituacao = new ArrayList<GenericBean>();
	protected Requisicao requisicao;
	
	// Auxiliares para ajax
	protected Materialtipo materialtipo;
	protected Materialgrupo materialgrupo;
	
	// TOTAIS
	protected Double totalRecursogeral;
	protected long totalRecursohumano;
	protected Double totalRecursohumanoQtde;
	
	public Projeto getProjeto() {
		return projeto;
	}
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public Tarefa getTarefa() {
		return tarefa;
	}
	public Atividade getAtividade() {
		return atividade;
	}
	@DisplayName("Fun��o")
	public Cargo getCargo() {
		return cargo;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Date getDtapontamentoDe() {
		return dtapontamentoDe;
	}
	public Date getDtapontamentoAte() {
		return dtapontamentoAte;
	}
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Total de recursos gerais")
	public Double getTotalRecursogeral() {
		return totalRecursogeral;
	}
	public long getTotalRecursohumano() {
		return totalRecursohumano;
	}
	@DisplayName("Total de recursos humanos (Horas)")
	public String getTotalRecursoshumanoStr() {
		long minutos = this.totalRecursohumano % 60;
		long horas = this.totalRecursohumano / 60;
		
		return horas + ":" + (minutos < 10 ? "0" : "") + minutos;
	}
	@DisplayName("Total de recursos humanos (Qtde.)")
	public Double getTotalRecursohumanoQtde() {
		return totalRecursohumanoQtde;
	}
	@DisplayName("O.S.")
	public Requisicao getRequisicao() {
		return requisicao;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	public void setTotalRecursohumanoQtde(Double totalRecursohumanoQtde) {
		this.totalRecursohumanoQtde = totalRecursohumanoQtde;
	}
	public void setTotalRecursogeral(Double totalRecursogeral) {
		this.totalRecursogeral = totalRecursogeral;
	}
	public void setTotalRecursohumano(long totalRecursohumano) {
		this.totalRecursohumano = totalRecursohumano;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtapontamentoDe(Date dtapontamento) {
		this.dtapontamentoDe = dtapontamento;
	}
	public void setDtapontamentoAte(Date dtapontamentoAte) {
		this.dtapontamentoAte = dtapontamentoAte;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Contrato")
	public Contrato getContrato() {
		return contrato;
	}
	@DisplayName("Tipo de Atividade")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	@DisplayName("Tipo de apontamento")
	public ApontamentoTipo getApontamentoTipo() {
		return apontamentoTipo;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setApontamentoTipo(ApontamentoTipo apontamentoTipo) {
		this.apontamentoTipo = apontamentoTipo;
	}
	@DisplayName("Situa��o")
	public List<GenericBean> getListaSituacao() {
		return listaSituacao;
	}
	public void setListaSituacao(List<GenericBean> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}	
}