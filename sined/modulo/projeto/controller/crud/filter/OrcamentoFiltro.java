package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Orcamentosituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OrcamentoFiltro extends FiltroListagemSined {

	protected String nome;
	protected Cliente cliente;
	protected List<Orcamentosituacao> listaSituacao;
	
	
	public OrcamentoFiltro(){
		List<Orcamentosituacao> lista = new ArrayList<Orcamentosituacao>();
		lista.add(Orcamentosituacao.EM_ABERTO);		
		this.listaSituacao = lista;
	}
	
	@MaxLength(50)
	@DisplayName("Descri��o")
	public String getNome() {
		return nome;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Orcamentosituacao> getListaSituacao() {
		return listaSituacao;
	}

	public void setListaSituacao(List<Orcamentosituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
}
