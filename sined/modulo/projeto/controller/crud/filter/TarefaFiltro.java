package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Tarefasituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@DisplayName("Filtro de Tarefa")
public class TarefaFiltro extends FiltroListagemSined{

	protected Projeto projeto;
	protected Planejamento planejamento;
	protected String descricao;
	protected Date dtinicioDe;
	protected Date dtinicioAte;
	protected Integer cdtarefa;
	protected Colaborador colaborador;
	protected List<Tarefasituacao> listaSituacao;
	
	private TarefaFiltro() {
		listaSituacao = new ArrayList<Tarefasituacao>();
		listaSituacao.add(Tarefasituacao.ATRASADA);
		listaSituacao.add(Tarefasituacao.CONCLUIDA);
		listaSituacao.add(Tarefasituacao.EM_ANDAMENTO);
		listaSituacao.add(Tarefasituacao.EM_ESPERA);		
	}
	
	public Integer getCdtarefa() {
		return cdtarefa;
	}
	public void setCdtarefa(Integer cdtarefa) {
		this.cdtarefa = cdtarefa;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public Planejamento getPlanejamento() {
		return planejamento;
	}
	@DisplayName("Descri��o")
	@MaxLength(300)
	public String getDescricao() {
		return descricao;
	}
	public Date getDtinicioDe() {
		return dtinicioDe;
	}
	public Date getDtinicioAte() {
		return dtinicioAte;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public List<Tarefasituacao> getListaSituacao() {
		return listaSituacao;
	}
	
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setDtinicioDe(Date dtinicioDe) {
		this.dtinicioDe = dtinicioDe;
	}
	public void setDtinicioAte(Date dtinicioAte) {
		this.dtinicioAte = dtinicioAte;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}	
	public void setListaSituacao(List<Tarefasituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	
}
