package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class HistogramaRGFiltro extends FiltroListagemSined {
	
	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Material material;
	protected Date dtinicio; 
	protected Date dtfim;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	@Required
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Grupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Periodo")
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Tipo")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}

}
