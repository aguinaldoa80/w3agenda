package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OrcamentoRecursoGeralFiltro extends FiltroListagemSined {

	protected Orcamento orcamento;
	protected Composicaoorcamento composicaoOrcamento;
	protected Boolean recursoDireto;
	protected Boolean recursoComposicao;
	protected String nomeRecurso;
	
	protected Boolean incluirValores;
	
	public OrcamentoRecursoGeralFiltro() {
	}
	
	public OrcamentoRecursoGeralFiltro(Orcamento orcamento) {
		this.orcamento = orcamento;
		this.recursoDireto = Boolean.TRUE;
		this.recursoComposicao = Boolean.TRUE;
	}

	@DisplayName("Or�amento")
	@Required
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	@DisplayName("Composi��o do or�amento")
	public Composicaoorcamento getComposicaoOrcamento() {
		return composicaoOrcamento;
	}
	
	@DisplayName("Recurso direto")
	public Boolean getRecursoDireto() {
		return recursoDireto;
	}
	
	@DisplayName("Recurso da composi��o")	
	public Boolean getRecursoComposicao() {
		return recursoComposicao;
	}

	@DisplayName("Recurso")
	@MaxLength(100)
	public String getNomeRecurso() {
		return nomeRecurso;
	}
	
	public Boolean getIncluirValores() {
		return incluirValores;
	}
	
	public void setIncluirValores(Boolean incluirValores) {
		this.incluirValores = incluirValores;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setComposicaoOrcamento(Composicaoorcamento composicaoorcamento) {
		this.composicaoOrcamento = composicaoorcamento;
	}

	public void setRecursoDireto(Boolean recursoDireto) {
		this.recursoDireto = recursoDireto;
	}
	
	public void setRecursoComposicao(Boolean recursoComposicao) {
		this.recursoComposicao = recursoComposicao;
	}

	public void setNomeRecurso(String nomeRecurso) {
		this.nomeRecurso = nomeRecurso;
	}	
}
