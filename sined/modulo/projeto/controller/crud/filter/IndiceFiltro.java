package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class IndiceFiltro extends FiltroListagemSined {
	
	protected String descricao;
	protected String identificador;

	@DisplayName("Descri��o")
	@MaxLength(300)
	public String getDescricao() {
		return descricao;
	}
	
	@MaxLength(10)
	public String getIdentificador() {
		return identificador;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
