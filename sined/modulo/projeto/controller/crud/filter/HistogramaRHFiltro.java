package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class HistogramaRHFiltro extends FiltroListagemSined {
	
	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Cargo cargo;
	protected Date dtinicio; 
	protected Date dtfim;
	protected Boolean funcao;
	protected List<Cargo> listacargo;
	{
		this.funcao = Boolean.FALSE;
	}
	
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	@DisplayName("Periodo")
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public Boolean getFuncao() {
		return funcao;
	}
	@DisplayName("Fun��o")
	public List<Cargo> getListacargo() {
		return listacargo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setFuncao(Boolean funcao) {
		this.funcao = funcao;
	}
	public void setListacargo(List<Cargo> listacargo) {
		this.listacargo = listacargo;
	}

}
