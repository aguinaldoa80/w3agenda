package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Customaoobraitemgrupo;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CustomaoobraitemFiltro extends FiltroListagemSined {

	protected String identificador;
	protected String descricao;
	protected Customaoobraitemgrupo customaoobraitemgrupo;
	protected Cargo cargo;
	protected Orcamento orcamento;
	
	@MaxLength(200)
	@DisplayName("Item de Custo MO")
	public String getDescricao() {
		return descricao;
	}
	
	@MaxLength(50)
	public String getIdentificador() {
		return identificador;
	}
	
	@DisplayName("Grupo de Item de Custo MO")
	public Customaoobraitemgrupo getCustomaoobraitemgrupo() {
		return customaoobraitemgrupo;
	}
	
	public Cargo getCargo() {
		return cargo;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setCustomaoobraitemgrupo(Customaoobraitemgrupo customaoobraitemgrupo) {
		this.customaoobraitemgrupo = customaoobraitemgrupo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@DisplayName("Or�amento")
	public Orcamento getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
}
