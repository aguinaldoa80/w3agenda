package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DiarioObraFiltro extends FiltroListagemSined {
	
	protected Projeto projeto;
	protected Date dtdia;
	
	@DisplayName("Dia")
	public Date getDtdia() {
		return dtdia;
	}

	public void setDtdia(Date dtdia) {
		this.dtdia = dtdia;
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
}
