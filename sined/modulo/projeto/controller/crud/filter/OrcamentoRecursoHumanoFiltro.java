package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OrcamentoRecursoHumanoFiltro extends FiltroListagemSined {

	protected Orcamento orcamento;
	protected Tipocargo tipoCargo;
	protected String nomeCargo;
	protected Boolean incluirPrecoVenda;
	
	public OrcamentoRecursoHumanoFiltro() {
	}
	
	public OrcamentoRecursoHumanoFiltro(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	@DisplayName("Or�amento")
	@Required
	public Orcamento getOrcamento() {
		return orcamento;
	}

	@DisplayName("Tipo de cargo")
	public Tipocargo getTipoCargo() {
		return tipoCargo;
	}
	
	@MaxLength(50)
	@DisplayName("Cargo")
	public String getNomeCargo() {
		return nomeCargo;
	}
	
	public Boolean getIncluirPrecoVenda() {
		return incluirPrecoVenda;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setTipoCargo(Tipocargo tipoCargo) {
		this.tipoCargo = tipoCargo;
	}
	
	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}
	
	public void setIncluirPrecoVenda(Boolean incluirPrecoVenda) {
		this.incluirPrecoVenda = incluirPrecoVenda;
	}
}
