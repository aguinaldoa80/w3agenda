package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@DisplayName("Filtro")
public class AtividadeFiltro extends FiltroListagemSined{

	protected String descricao;
	protected Date dtinicio;
	protected Date dtfim;
	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Tarefa tarefa;
	protected Colaborador colaborador;
	
	@DisplayName("Nome")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Data início")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public Tarefa getTarefa() {
		return tarefa;
	}
	@DisplayName("Responsável")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
}
