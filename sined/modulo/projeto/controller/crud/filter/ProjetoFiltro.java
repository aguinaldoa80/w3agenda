package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.Projetotipoitem;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProjetoFiltro extends FiltroListagemSined {

	protected Integer cdprojeto;
	protected String nome;
	protected Cliente cliente;
	protected Empresa empresa;
	protected Municipio municipio;
	protected Uf uf;
	protected String colaborador;
	protected String sigla;
	protected List<Situacaoprojeto> listaSituacaoprojeto;
	protected Projetotipo projetotipo;
	protected Projetotipoitem projetotipoitem;
	protected String valorprojetoitem;
	
	public ProjetoFiltro(){
		List<Situacaoprojeto> listaSituacaoprojeto = new ArrayList<Situacaoprojeto>();
		listaSituacaoprojeto.add(Situacaoprojeto.EM_ESPERA);
		listaSituacaoprojeto.add(Situacaoprojeto.EM_ANDAMENTO);
		listaSituacaoprojeto.add(Situacaoprojeto.CONCLUIDO);
		
		this.listaSituacaoprojeto = listaSituacaoprojeto;
	}
	@MaxLength(100)
	@DisplayName("Descrição")
	public String getNome() {
		return nome;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Município")
	public Municipio getMunicipio() {
		return municipio;
	}

	@DisplayName("Uf")
	public Uf getUf() {
		return uf;
	}
	
	@DisplayName("Responsável")
	@MaxLength(100)
	public String getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@DisplayName("Sigla")
	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	public List<Situacaoprojeto> getListaSituacaoprojeto() {
		return listaSituacaoprojeto;
	}
	public void setListaSituacaoprojeto(List<Situacaoprojeto> listaSituacaoprojeto) {
		this.listaSituacaoprojeto = listaSituacaoprojeto;
	}
	
	@DisplayName("Tipo")
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}
	@DisplayName("Campo adicional")
	public Projetotipoitem getProjetotipoitem() {
		return projetotipoitem;
	}
	@DisplayName("Valor")
	public String getValorprojetoitem() {
		return valorprojetoitem;
	}
	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}
	public void setProjetotipoitem(Projetotipoitem projetotipoitem) {
		this.projetotipoitem = projetotipoitem;
	}
	public void setValorprojetoitem(String valorprojetoitem) {
		this.valorprojetoitem = valorprojetoitem;
	}
	@DisplayName("Id")
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	
	
}
