package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ComposicaomaoobraFiltro extends FiltroListagemSined {

	protected Orcamento orcamento;
	protected Cargo cargo;
	
	@DisplayName("Or�amento")
	public Orcamento getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public Cargo getCargo() {
		return cargo;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}
