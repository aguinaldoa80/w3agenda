package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CustomaoobraitemgrupoFiltro extends FiltroListagemSined {

	protected String descricao;
	protected Boolean ativo = true;
	
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
