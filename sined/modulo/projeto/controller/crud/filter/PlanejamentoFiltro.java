package br.com.linkcom.sined.modulo.projeto.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PlanejamentoFiltro extends FiltroListagemSined{

	protected Projeto projeto;
	protected String descricao;
	protected List<Planejamentosituacao> listaSituacao;
	
	//importar tarefas
	private Arquivo arquivo;
	private Planejamento planejamento;
	
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Situa��o")
	public List<Planejamentosituacao> getListaSituacao() {
		return listaSituacao;
	}
	@Required
	@DisplayName("Arquivo CSV")
	public Arquivo getArquivo() {
		return arquivo;
	}
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	
	public void setListaSituacao(List<Planejamentosituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
}