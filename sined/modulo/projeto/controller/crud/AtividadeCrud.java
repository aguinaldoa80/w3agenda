package br.com.linkcom.sined.modulo.projeto.controller.crud;

import java.sql.Date;
import java.util.List;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Atividade;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.AtividadeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/projeto/crud/Atividade",
				  "/agro/crud/Atividade"	
			}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "dtinicio", "dtfim", "planejamento", "tarefa", "colaborador"})
public class AtividadeCrud extends CrudControllerSined<AtividadeFiltro, Atividade, Atividade>{

	private TarefaService tarefaService;
	private ColaboradorService colaboradorService;
	private AtividadetipoService atividadetipoService;
	
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Atividade form)throws Exception {
		request.setAttribute("tiposAtividade", atividadetipoService.findAtivos());
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected void validateBean(Atividade bean, BindException errors) {
		if(bean.getDtinicio() != null &&  bean.getDtfim() != null
				&& SinedDateUtils.afterIgnoreHour(bean.getDtinicio(), bean.getDtfim())){
			errors.reject("001","A data in�cio da atividade deve ser anterior � data fim.");
		}
		
		Projeto projeto = bean.getProjeto();
		if(projeto.getDtprojeto() != null && bean.getDtinicio() != null &&
				SinedDateUtils.beforeIgnoreHour(bean.getDtinicio(), projeto.getDtprojeto())){
			errors.reject("001", "A data in�cio da atividade n�o pode ser anterior a data do projeto.");
		}
		
		
		Tarefa tarefa = bean.getTarefa();
		if(tarefa != null){
			tarefa = tarefaService.load(bean.getTarefa(),"tarefa.dtinicio, tarefa.dtfim, tarefa.duracao");
			Date dtinicio = tarefa.getDtinicio();
			Date dtfim = tarefa.getDtfim();
			
			if(bean.getDtinicio() != null && dtinicio != null
					&& SinedDateUtils.beforeIgnoreHour(bean.getDtinicio(), dtinicio)){
				errors.reject("001","A data in�cio da atividade deve ser maior ou igual � data in�cio da tarefa.");
			}
			
			if(bean.getDtfim() != null && dtfim != null 
					&& SinedDateUtils.afterIgnoreHour(bean.getDtfim(), dtfim)){
				errors.reject("001","A data fim da atividade deve ser menor ou igual � data fim da tarefa.");
			}
		}
	}
	
	@Override
	protected void validateFilter(AtividadeFiltro filtro, BindException errors) {
		if(filtro.getDtinicio() != null && filtro.getDtfim() != null
				&& filtro.getDtfim().before(filtro.getDtinicio())){
			errors.reject("001","Per�odo de atividade inv�lido.");
		}
	}
	
	/**
	 * M�todo para filtrar os colaboradores por cargo e projeto.
	 * 
	 * @param request
	 * @author Fl�vio Tavares
	 */
	public void makeAjaxColaborador(WebRequestContext request){
		Cargo cargo = new Cargo(Integer.parseInt(request.getParameter("cdcargo")));
		Projeto projeto = new Projeto(Integer.parseInt(request.getParameter("cdprojeto")));
		Colaborador colaborador = null;
		if(!"null".equals(request.getParameter("cdcolaborador"))){
			colaborador = new Colaborador(Integer.parseInt(request.getParameter("cdcolaborador")));
		}
		
		List<Colaborador> listaColaboradores = colaboradorService.findByCargoProjeto(cargo, projeto, colaborador);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaColaboradores, "listaColaborador", ""));
	}
}
