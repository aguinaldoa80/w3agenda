package br.com.linkcom.sined.modulo.projeto.controller.crud;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipoitem;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioprojeto;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.ProjetodespesaService;
import br.com.linkcom.sined.geral.service.ProjetotipoitemService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.UsuarioprojetoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.CopiaProjeto;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ProjetoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/projeto/crud/Projeto", "/agro/crud/Projeto"}, authorizationModule=CrudAuthorizationModule.class)
public class ProjetoCrud extends CrudControllerSined<ProjetoFiltro, Projeto, Projeto> {
	
	private ProjetoService projetoService;
	private UsuarioprojetoService usuarioprojetoService;
	private PropostaService propostaService;
	private EmpresaService empresaService;
	private ProjetotipoitemService projetotipoitemService;
	private ColaboradorService colaboradorService;
	private ReportTemplateService reportTemplateService;
	private ParametrogeralService parametrogeralService;
	private ProjetodespesaService projetodespesaService;
	private LocalarmazenagemService localarmazenagemService;
	
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setProjetotipoitemService(ProjetotipoitemService projetotipoitemService) {this.projetotipoitemService = projetotipoitemService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setUsuarioprojetoService(UsuarioprojetoService usuarioprojetoService) {this.usuarioprojetoService = usuarioprojetoService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}	
	public void setPropostaService(PropostaService propostaService) {this.propostaService = propostaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setProjetodespesaService(ProjetodespesaService projetodespesaService) {this.projetodespesaService = projetodespesaService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	
	@Override
	protected void listagem(WebRequestContext request, ProjetoFiltro filtro)throws Exception {
		List<Situacaoprojeto> listaSituacaoprojeto = new ArrayList<Situacaoprojeto>();
		listaSituacaoprojeto.add(Situacaoprojeto.EM_ESPERA);
		listaSituacaoprojeto.add(Situacaoprojeto.EM_ANDAMENTO);
		listaSituacaoprojeto.add(Situacaoprojeto.CONCLUIDO);
		listaSituacaoprojeto.add(Situacaoprojeto.CANCELADO);
		request.setAttribute("listaSituacaoprojeto", listaSituacaoprojeto);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Projeto bean) throws Exception {

		super.excluir(request, bean);
	}
	
	@SuppressWarnings("static-access")
	@Override
	protected void salvar(WebRequestContext request, Projeto bean)throws Exception {
		try {	
			boolean criar = false;
			if(bean.getCdprojeto() == null){
				criar = true;
			}
			if(bean != null && bean.getCdprojeto() != null)
				bean = projetoService.gerarProjetohistorico(bean);
			if(bean.getCei() != null && "".equals(bean.getCei())){
				bean.setCei(null);
			}
			if(!Situacaoprojeto.CANCELADO.equals(bean.getSituacao()) && bean.getDtfimprojeto() != null && SinedDateUtils.afterIgnoreHour(new Date(System.currentTimeMillis()), bean.getDtfimprojeto())){
				bean.setSituacao(Situacaoprojeto.CONCLUIDO);
			}
			
			if(SinedUtil.isListNotEmpty(bean.getListaProjetodespesa())){
				projetodespesaService.agruparListaPD(bean.getListaProjetodespesa());
			}
			
			super.salvar(request, bean);
			
			Usuario user = (Usuario)NeoWeb.getUser();
			if(criar && !user.getTodosprojetos()){
				Usuarioprojeto usuarioprojeto = new Usuarioprojeto();
				usuarioprojeto.setProjeto(bean);
				usuarioprojeto.setUsuario(user);
				
				usuarioprojetoService.saveOrUpdate(usuarioprojeto);
				new SinedUtil().zeraListaProjeto();
			}
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_projeto_nome"))
				throw new SinedException("Projeto j� cadastrado no sistema.");
			else if (DatabaseError.isKeyPresent(e, "projeto_cnpj_key"))
				throw new SinedException("Cnpj j� cadastrado.");
			else if (DatabaseError.isKeyPresent(e, "projeto_cei_key"))
				throw new SinedException("Cei j� cadastrado.");
			else throw e;
		}
	}
	
	/**
	 * Action para a enrada de dados ao copiar projeto.
	 * 
	 * @param request
	 * @param copiaProjeto
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Action("copiaProjeto")
	public ModelAndView doCopiaProjeto(WebRequestContext request, CopiaProjeto copiaProjeto){
		request.setAttribute("copiaProjeto", copiaProjeto);
		request.setAttribute("clearBase", Boolean.TRUE);
		request.setAttribute("TEMPLATE_beanName", "copiaProjeto");
		return new ModelAndView("crud/popup/copiaProjeto");
	}
	
	/**
	 * Action para salvar a c�pia de projetos.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#saveCopiaProjeto(CopiaProjeto)
	 * @param request
	 * @param copiaProjeto
	 * @return
	 * @author Fl�vio Tavares
	 */
	@OnErrors("copiaProjeto")
	@Action("salvarCopia")
	public ModelAndView salvarCopiaProjeto(WebRequestContext request, CopiaProjeto copiaProjeto){
		
		projetoService.saveCopiaProjeto(copiaProjeto);
		request.addMessage("Projeto copiado com sucesso.");
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent()
			.println("<script type='text/javascript'>" +
						"window.top.$.akModalHideAndRedirect(window.location.href);" +
						"window.location.href=window.location.href" +
					"</script>");
		
		return null;
	}	
	
	@Override
	protected void entrada(WebRequestContext request, Projeto form)	throws Exception {		
		
		if(form.getCdprojeto() != null && form.getSigla() == null){
			Projeto projeto = projetoService.load(form);
			if(projeto.getSigla() != null){
				form.setSigla(projeto.getSigla());
			}
		}
		
		if(form.getColaborador()!=null && form.getColaborador().getCdpessoa()!=null){
			form.setColaborador(colaboradorService.load(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
		} else {
			form.setColaborador(SinedUtil.getUsuarioComoColaborador());
		}
		
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("listaProposta", propostaService.findNumeroAnoProposta());
		request.setAttribute("listaTemplateDiarioObra", reportTemplateService.loadTemplatesByCategoria(EnumCategoriaReportTemplate.EMITIR_DIARIO_OBRA));
		
		String labelTotalsemana = "Total Semana(s)";
		String paramHistogramasemanal = parametrogeralService.getValorPorNome(Parametrogeral.CALCULO_HISTOGRAMA_SEMANAL);
		if(paramHistogramasemanal != null && paramHistogramasemanal.trim().toLowerCase().equals("false")){
			labelTotalsemana = "Total M�s(es)";
		}
		request.setAttribute("labelTotalsemana", labelTotalsemana);
		request.setAttribute("OBRIGAR_TIPOPROJETO", "TRUE".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.OBRIGAR_TIPOPROJETO)));
		
		super.entrada(request, form);
	}
	
	/**
	 * 
	 * M�todo que calcula o total de semanas de diferen�as entre a data final e a inicial.
	 *
	 * @name calcularTotalSemanas
	 * @return
	 * @return ModelAndView
	 * @author Thiago Augusto
	 * @throws ParseException 
	 * @date 29/06/2012
	 *
	 */
	public ModelAndView calcularTotalSemanas(WebRequestContext request) throws ParseException{
		JsonModelAndView json = new JsonModelAndView();
		int retorno = 0;
		String dataFinal = request.getParameter("dtFinal");
		String dataInicial = request.getParameter("dtInicial");
		if (!dataFinal.equals("") && !dataInicial.equals("")){
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			String paramCalculoHistograma = parametrogeralService.getValorPorNome(Parametrogeral.CALCULO_HISTOGRAMA_SEMANAL);
			if("TRUE".equalsIgnoreCase(paramCalculoHistograma)){
				retorno = SinedDateUtils.diferencaSemanas(format.parse(dataFinal), format.parse(dataInicial));
			}else {
				retorno = SinedDateUtils.diferencaMeses(format.parse(dataFinal), format.parse(dataInicial));
			}
		}
		json.addObject("retorno", retorno);
		return json;
	}
	
	/**
	 * Busca a lista de campos adicionais a partir do tipo selecionado na tela.
	 *
	 * @param request
	 * @param projeto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/06/2013
	 */
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, Projeto projeto) {
		if (projeto.getProjetotipo() == null || projeto.getProjetotipo().getCdprojetotipo() == null) {
			throw new SinedException("O tipo de projeto n�o pode se nulo.");
		}
		List<Projetotipoitem> lista = projetotipoitemService.findByProjetotipo(projeto.getProjetotipo());
		return new JsonModelAndView()
					.addObject("size", lista.size())
					.addObject("lista", lista);
	}

	/**
	 * M�todo que abre a popup para concluir projeto
	 *
	 * @param request
	 * @param whereIn
	 * @return
	 * @author Lucas Costa
	 */
	public ModelAndView abrirConcluirProjeto(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		request.setAttribute("selectedItens", whereIn);
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		request.setAttribute("entrada", entrada);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String dtAtual = format.format(new Date(System.currentTimeMillis()));
		request.setAttribute("dataAtual", dtAtual);
		return new ModelAndView("direct:/crud/popup/popupConcluirProjeto");
	}
	
	/**
	 * M�todo conclui o projeto
	 *
	 * @param request
	 * @return
	 * @author Lucas Costa
	 */
	public ModelAndView concluirProjeto(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		
		try {
			projetoService.updateDataTerminio(whereIn, SinedDateUtils.stringToSqlDate(request.getParameter("dtfimprojeto")));
			request.addMessage("Projeto <a href=\"Projeto?ACAO=consultar&cdprojeto=" + whereIn + "\">" + whereIn + "</a> conclu�do com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na conclus�o do projeto: " + e.getMessage());
		}
		
		return new ModelAndView("redirect:/projeto/crud/Projeto"+ (entrada ? "?ACAO=consultar&cdprojeto="+whereIn : ""));
			
	}
	
	/**
	* M�todo ajax para carregar o municipio e uf do localarmazenagem
	*
	* @param request
	* @param localarmazenagem
	* @since Sep 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxCarregaUfMunicipio(WebRequestContext request, Localarmazenagem localarmazenagem){
		localarmazenagemService.ajaxCarregaUfMunicipio(request, localarmazenagem);
	}
}
