package br.com.linkcom.sined.modulo.projeto.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Indice;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.IndiceFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/projeto/crud/Indice", authorizationModule=CrudAuthorizationModule.class)
public class IndiceCrud extends CrudControllerSined<IndiceFiltro, Indice, Indice> {
	
	protected UnidademedidaService unidademedidaService;
	
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Indice form)throws Exception {
		request.setAttribute("listaUnidademedida", unidademedidaService.findAtivos(form.getUnidademedida()));		
	}
	
	@Override
	protected void salvar(WebRequestContext request, Indice bean)throws Exception {
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_INDICE_IDENTIFICADOR")) {throw new SinedException("Identificador da composi��o j� cadastrado no sistema.");}	
		}
	}
	
	/**
	 * M�todo chamado via ajax para carregar unidade de medida do material
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView getUnidadeMedidaAjax(WebRequestContext request, Material bean){
		return new JsonModelAndView().addObject("unidademedida", unidademedidaService.findByMaterial(bean));
	}
	
}
