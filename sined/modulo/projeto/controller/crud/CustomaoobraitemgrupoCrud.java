package br.com.linkcom.sined.modulo.projeto.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Customaoobraitemgrupo;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.CustomaoobraitemgrupoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/projeto/crud/Customaoobraitemgrupo", authorizationModule=CrudAuthorizationModule.class)
public class CustomaoobraitemgrupoCrud extends CrudControllerSined<CustomaoobraitemgrupoFiltro, Customaoobraitemgrupo, Customaoobraitemgrupo> {
	
	@Override
	protected void salvar(WebRequestContext request, Customaoobraitemgrupo bean)throws Exception {
		try{
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_customaoobraitemgrupo_descricao")) {throw new SinedException("Grupo de Item de Custo com M�o de Obra j� cadastrado no sistema.");}	
		}
	}
	
}
