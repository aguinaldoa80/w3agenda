package br.com.linkcom.sined.modulo.projeto.controller.crud;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Atividade;
import br.com.linkcom.sined.geral.bean.Diarioobra;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.service.AtividadeService;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.DiarioObraFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

/**
 * @author Andre Brunelli
 */

@CrudBean
@Controller(path={"/projeto/crud/Diarioobra","/agro/crud/Diarioobra"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"dtdia", "projeto", "dtprazocontratual", "diasdecorridos", "dtprorrogacao", "diasrestantes", "diasatraso"})
public class DiarioobraCrud extends CrudControllerSined<DiarioObraFiltro, Diarioobra, Diarioobra> {	

	private PlanejamentoService planejamentoService;
	private ProjetoService projetoService;
	private AtividadeService atividadeService;
	private TarefaService tarefaService;
	private AtividadetipoService atividadetipoService;
	private UnidademedidaService unidademedidaService;
	
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {this.atividadetipoService = atividadetipoService;}
	public void setTarefaService(TarefaService tarefaService) {this.tarefaService = tarefaService;}
	public void setAtividadeService(AtividadeService atividadeService) {this.atividadeService = atividadeService;}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {this.planejamentoService = planejamentoService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}	
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	
	@Override
	protected void entrada(WebRequestContext request, Diarioobra form) throws Exception {
		List<Planejamento> listaPlanejamento = null;
		if(form.getCddiarioobra() == null){
			form.setDtdia(SinedDateUtils.currentDate());
			listaPlanejamento = new ArrayList<Planejamento>();
		} else {
			listaPlanejamento = planejamentoService.findByProjetoForFlex(form.getProjeto());
			
			List<Atividade> listaAtividade = atividadeService.findByData(form.getDtdia(), form.getProjeto());
			form.setListaAtividade(listaAtividade);
			
		}
		request.setAttribute("listaPlanejamento", listaPlanejamento);
	}
	
	/**
	 * N�o permite salvar dois di�rios de obras para o mesmo dia 
	 */
	@Override
	protected void salvar(WebRequestContext request, Diarioobra bean)throws Exception {
		try {	
			List<Atividade> listaAtividade = bean.getListaAtividade();
			
			super.salvar(request, bean);
			
			if(listaAtividade != null){
				for (Atividade atividade : listaAtividade) {
					atividadeService.saveOrUpdate(atividade);
				}
			}

		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_DIARIOOBRA_DTDIA")) {
				throw new SinedException("Di�rio de obra j� cadastrado no sistema.");
			}	
		}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Diarioobra form) throws CrudException {
//		request.setAttribute("disableDia", true);
		return super.doEditar(request, form);
	}
	
	/**
	 * M�todo que via ajax carrega as informa��es do projeto e planejamento para o di�rio de obra.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxProjeto(WebRequestContext request, Diarioobra bean) {
		if (bean == null || bean.getProjeto() == null ||  bean.getProjeto().getCdprojeto() == null) {
			throw new SinedException("Projeto n�o pode ser nulo.");
		}
		
		
		Planejamento planejamento = null;
		List<Planejamento> findAutorizadosByProjeto = planejamentoService.findAutorizadosByProjeto(bean.getProjeto());
		if(findAutorizadosByProjeto != null && findAutorizadosByProjeto.size() > 0)
			planejamento = findAutorizadosByProjeto.get(0);
		
		Projeto projeto = projetoService.load(bean.getProjeto(), "projeto.dtprojeto, projeto.dtfimprojeto");
		
		Date dtfimplanej = planejamento != null ? planejamento.getDtfim() : null;
		Date dtprojeto = projeto != null ? projeto.getDtprojeto() : null;
		Date dtfimprojeto = projeto != null ? projeto.getDtfimprojeto() : null;
		
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		jsonModelAndView.addObject("dtprorrogacao", dtfimplanej != null ? format.format(dtfimplanej) : "");
		jsonModelAndView.addObject("dtprazocontratual", dtfimprojeto != null ? format.format(dtfimprojeto) : "");
		
		Integer decorridos = 0;
		Integer restantes = 0;
		Integer diasatraso = 0;
		
		if(bean.getDtdia() != null){
			if(dtprojeto != null){
				decorridos = new Long(SinedDateUtils.calculaDiferencaDias(dtprojeto, bean.getDtdia())).intValue();
			}
			
			if(dtfimplanej != null){
				restantes = new Long(SinedDateUtils.calculaDiferencaDias(bean.getDtdia(), dtfimplanej)).intValue();
			}
			
			if(dtfimprojeto != null){
				diasatraso = new Long(SinedDateUtils.calculaDiferencaDias(dtfimprojeto, bean.getDtdia())).intValue();
			}
			
			List<Planejamento> listaPlanejamento = planejamentoService.findByProjetoForFlex(bean.getProjeto());
			jsonModelAndView.addObject("listaPlanejamento", listaPlanejamento);
			jsonModelAndView.addObject("sizePlanejamento", listaPlanejamento.size());
			
			List<Atividade> listaAtividade = atividadeService.findByData(bean.getDtdia(), bean.getProjeto());
			jsonModelAndView.addObject("listaAtividade", listaAtividade);
			jsonModelAndView.addObject("sizeAtividade", listaAtividade.size());
			
		}
		
		jsonModelAndView.addObject("diasdecorridos", decorridos < 0 ? 0 : decorridos);
		jsonModelAndView.addObject("diasrestantes", restantes < 0 ? 0 : restantes );
		jsonModelAndView.addObject("diasatraso", diasatraso < 0 ? 0 : diasatraso);
		
		return jsonModelAndView;
	}
	
	
	public void ajaxExcluirAtividade(WebRequestContext request, Atividade bean) {
		try{
			atividadeService.delete(bean);
			View.getCurrent().println("var sucesso = true;");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	public ModelAndView ajaxPlanejamento(WebRequestContext request, Atividade bean) {
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(bean.getPlanejamento() != null){
			List<Tarefa> listaTarefa = tarefaService.findBy(bean.getPlanejamento(), true);
			jsonModelAndView.addObject("listaTarefa", listaTarefa);
			jsonModelAndView.addObject("sizeTarefa", listaTarefa.size());
		} else {
			jsonModelAndView.addObject("listaTarefa", new ArrayList<Tarefa>());
			jsonModelAndView.addObject("sizeTarefa", 0);
		}
		return jsonModelAndView;
	}
	
	public ModelAndView criarAtividade(WebRequestContext request, Atividade atividade){
		
		Projeto projeto = atividade.getProjeto();
		
		atividade.setDtinicio(atividade.getDtdia());
		atividade.setDtfim(atividade.getDtdia());
				
		request.setAttribute("listaPlanejamento", planejamentoService.findByProjetoForFlex(projeto));
		request.setAttribute("titulo", "CRIAR ATIVIDADE");
		request.setAttribute("actionSalvar", "saveCriarAtividade");
		
		return new ModelAndView("direct:/crud/popup/editarAtividadeDiarioobra", "bean", atividade);
	}
	
	public void saveCriarAtividade(WebRequestContext request, Atividade atividade){
		atividade.setAtividadetipo(atividadetipoService.load(atividade.getAtividadetipo()));
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>" +
					"parent.novaLinhaAtividade('" + atividade.getNome() + "', '" + atividade.getDescricao() + "', '" + SinedDateUtils.toString(atividade.getDtinicio()) + "', '" + SinedDateUtils.toString(atividade.getDtfim()) + "', " + atividade.getAtividadetipo().getCdatividadetipo() + ", " + atividade.getColaborador().getCdpessoa() + ", " + (atividade.getPlanejamento() != null ? atividade.getPlanejamento().getCdplanejamento() : "null" ) + ", " + (atividade.getTarefa() != null ? atividade.getTarefa().getCdtarefa() : "null")  + ", '" + atividade.getAtividadetipo().getNome() + "', " + atividade.getProjeto().getCdprojeto() + ");" +
					"parent.$.akModalRemove(true);" +
				"</script>");
	}
	
	public ModelAndView editarAtividade(WebRequestContext request, Atividade atividade){
		
		Projeto projeto = atividade.getProjeto();
		request.setAttribute("listaPlanejamento", planejamentoService.findByProjetoForFlex(projeto));
		request.setAttribute("titulo", "EDITAR ATIVIDADE");
		request.setAttribute("actionSalvar", "saveEditarAtividade");
		
		return new ModelAndView("direct:/crud/popup/editarAtividadeDiarioobra", "bean", atividade);
	}
	
	public void saveEditarAtividade(WebRequestContext request, Atividade atividade){
		atividade.setAtividadetipo(atividadetipoService.load(atividade.getAtividadetipo()));
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/jquery.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimmer.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/dimensions.pack.js\"></script>" +
				"<script type=\"text/javascript\" src=\""+request.getServletRequest().getContextPath()+"/js/akModal.js\"></script>" +
				"<script>" +
				"parent.editaLinhaAtividade(" + atividade.getIndice() + ", '" + atividade.getNome() + "', '" + atividade.getDescricao() + "', '" + SinedDateUtils.toString(atividade.getDtinicio()) + "', '" + SinedDateUtils.toString(atividade.getDtfim()) + "', " + atividade.getAtividadetipo().getCdatividadetipo() + ", " + atividade.getColaborador().getCdpessoa() + ", " + (atividade.getPlanejamento() != null ? atividade.getPlanejamento().getCdplanejamento() : "null" ) + ", " + (atividade.getTarefa() != null ? atividade.getTarefa().getCdtarefa() : "null")  + ", '" + atividade.getAtividadetipo().getNome() + "', " + atividade.getProjeto().getCdprojeto() + ");" +
				"parent.$.akModalRemove(true);" +
		"</script>");
	}
	
	public ModelAndView ajaxCarregaUnidadeMedidaMaterial(WebRequestContext request, Material material){
		Unidademedida unidademedida = unidademedidaService.findByMaterial(material);
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("unidademedida", unidademedida);
		return json;
	}
}
