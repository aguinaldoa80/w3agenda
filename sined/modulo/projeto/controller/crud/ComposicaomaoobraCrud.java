package br.com.linkcom.sined.modulo.projeto.controller.crud;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Composicaomaoobra;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.service.ComposicaomaoobraService;
import br.com.linkcom.sined.geral.service.OrcamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ComposicaomaoobraFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/projeto/crud/Composicaomaoobra", authorizationModule=CrudAuthorizationModule.class)
public class ComposicaomaoobraCrud extends CrudControllerSined<ComposicaomaoobraFiltro, Composicaomaoobra, Composicaomaoobra> {
	
	private ComposicaomaoobraService composicaomaoobraService;
	private OrcamentoService orcamentoService;
	
	public void setComposicaomaoobraService(
			ComposicaomaoobraService composicaomaoobraService) {
		this.composicaomaoobraService = composicaomaoobraService;
	}
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}


	@Override
	protected Composicaomaoobra criar(WebRequestContext request, Composicaomaoobra form) throws Exception {
		Composicaomaoobra bean = super.criar(request, form);
		String copiar = request.getParameter("copiar");
		if(copiar != null && "true".equals(copiar)){
			bean = composicaomaoobraService.criaCopia(form);
		}else {
			String cdorcamento = request.getParameter("cdorcamento");
			if(StringUtils.isNotEmpty(cdorcamento)){
				bean.setOrcamento(orcamentoService.load(new Orcamento(Integer.parseInt(cdorcamento)), "orcamento.cdorcamento, orcamento.nome"));
			}
		}
		return bean;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Composicaomaoobra form) throws Exception {
		if(form.getCdcomposicaomaoobra() != null){
			try{
				Money custoporhora = composicaomaoobraService.calculaFormulaCustoporhora(form);
				form.setCustoporhora(custoporhora);
			} catch (Exception e) {
				request.addError(e.getMessage());
			}
		}
	}
	
}
