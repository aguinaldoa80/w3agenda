package br.com.linkcom.sined.modulo.projeto.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/projeto/crud/Orcamento", authorizationModule=CrudAuthorizationModule.class)
public class OrcamentoCrud extends CrudControllerSined<OrcamentoFiltro, Orcamento, Orcamento> {

	@Override
	protected void entrada(WebRequestContext request, Orcamento form) throws Exception {
//		SETAR A UF POIS ELA � OBRIGAT�RIA EM MUNIC�PIO
		form.setUf(form.getMunicipio() != null ? form.getMunicipio().getUf() : null);
	}
	
}
