package br.com.linkcom.sined.modulo.projeto.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Indice;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefacolaborador;
import br.com.linkcom.sined.geral.bean.Tarefarecursohumano;
import br.com.linkcom.sined.geral.bean.Tarefarequisicao;
import br.com.linkcom.sined.geral.bean.enumeration.Tarefasituacao;
import br.com.linkcom.sined.geral.service.ColaboradorcargoService;
import br.com.linkcom.sined.geral.service.IndiceService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.geral.service.TarefacolaboradorService;
import br.com.linkcom.sined.geral.service.TarefarecursohumanoService;
import br.com.linkcom.sined.geral.service.TarefarequisicaoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.TarefaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/projeto/crud/Tarefa","/agro/crud/Tarefa"},authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "dtinicio", "dtfim", "tarefasituacao"})
public class TarefaCrud extends CrudControllerSined<TarefaFiltro, Tarefa, Tarefa>{

	private IndiceService indiceService;
	private TarefaService tarefaService;
	private TarefacolaboradorService tarefacolaboradorService;		
	private TarefarecursohumanoService tarefarecursohumanoService;
	private ColaboradorcargoService colaboradorcargoService;	
	private TarefarequisicaoService tarefarequisicaoService;
	
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setIndiceService(IndiceService indiceService) {
		this.indiceService = indiceService;
	}	
	public void setTarefacolaboradorService(TarefacolaboradorService tarefacolaboradorService) {
		this.tarefacolaboradorService = tarefacolaboradorService;
	}
	public void setTarefarecursohumanoService(TarefarecursohumanoService tarefarecursohumanoService) {
		this.tarefarecursohumanoService = tarefarecursohumanoService;
	}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {
		this.colaboradorcargoService = colaboradorcargoService;
	}	
	public void setTarefarequisicaoService(TarefarequisicaoService tarefarequisicaoService) {
		this.tarefarequisicaoService = tarefarequisicaoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Tarefa form) throws Exception {
//		String acao = request.getParameter(MultiActionController.ACTION_PARAMETER);
//		if("editar".equals(acao)){
//			throw new SinedException("N�o � poss�vel realizar esta a��o.");
//		}
		
		if("criar".equals(request.getParameter("ACAO"))){
			form.setTarefasituacao(Tarefasituacao.EM_ESPERA);
		}
		
		if("consultar".equals(request.getParameter("ACAO")) || "editar".equals(request.getParameter("ACAO"))){
			tarefaService.updateSituacao(tarefaService.verificaSitucaotarefa(form), form);
		}
		request.setAttribute("listaTarefa", tarefaService.findAllMenosTarefa(form));
		
		if(form.getCdtarefa() != null){
			List<Tarefarequisicao> listaTarefarequisicao = tarefarequisicaoService.findByTarefa(form);
			form.setListaTarefarequisicao(listaTarefarequisicao);
		}
		tarefaService.verificaOrigemRecursogeral(form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, TarefaFiltro filtro)	throws Exception {
		 request.setAttribute("listaSituacao", new ArrayList<Tarefasituacao>(Arrays.asList(Tarefasituacao.values())));	 
	}
	
	@Override
	protected void salvar(WebRequestContext request, Tarefa bean) throws Exception {
		bean.setTarefasituacao(tarefaService.verificaSitucaotarefa(bean));
		super.salvar(request, bean);
	}
	
	@Override
	protected ListagemResult<Tarefa> getLista(WebRequestContext request,TarefaFiltro filtro) {
		
		ListagemResult<Tarefa> listagemResult = super.getLista(request, filtro);
		List<Tarefa> list = listagemResult.list();
		
		if(list != null && list.size() > 0){
			for (Tarefa tarefa : list) {
				tarefaService.updateSituacao(tarefaService.verificaSitucaotarefa(tarefa), tarefa);
			}
		}
		
		return listagemResult;
	}
	
	
//	@Override
//	protected Tarefa criar(WebRequestContext request, Tarefa form) throws Exception {
//		throw new SinedException("N�o � poss�vel realizar esta a��o.");
//	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	/**
	 * M�todo para carregar via AJAX a unidade de medida do indice.
	 *
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#load
	 * @param request
	 * @param planejamento
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public void ajaxUnidade(WebRequestContext request, Tarefa tarefa) throws Exception {
		if (tarefa == null || tarefa.getIndice() == null || tarefa.getIndice().getCdindice() == null) {
			throw new SinedException("Indice n�o pode ser nulo.");
		}
		Indice indice = indiceService.loadWithUnidade(tarefa.getIndice());
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval(indice.getUnidademedida().getClass().getName()+"[cdunidademedida="+indice.getUnidademedida().getCdunidademedida()+"]");
	}
	

	/**
	 * M�todo para carregar via AJAX a dtfim calculada para a tarefa.
	 *
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#load
	 * @param request
	 * @param planejamento
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public void ajaxDtfim(WebRequestContext request, Tarefa tarefa) throws Exception {
		if (tarefa == null || tarefa.getDtinicio() == null || tarefa.getDuracao() == null) {
			throw new SinedException("Campos dtinicio e duracao n�o podem ser nulos.");
		}
		request.getServletResponse().setContentType("text/html");
		Date dtfimCalculada = tarefa.getDtfimCalculada();
		if(dtfimCalculada == null){
			dtfimCalculada = tarefa.getDtinicio();
		}
		View.getCurrent().eval(new SimpleDateFormat("dd/MM/yyyy").format(dtfimCalculada));
	}
	
	/**
	* M�todo que abrir� a tela para que seja selecionado os colaboradores 
	*
	* @param request
	* @return
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView abrirSelecionarColaborador(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn != null && !whereIn.equals("")){			
			return new ModelAndView("redirect:/crud/popup/selecionaColaborador?idstarefa=" + whereIn);
		}
		
		request.addError("Nenhum item selecionado");
		return new ModelAndView("redirect: /projeto/crud/Tarefa");
	}
	
	/**
	* M�todo para alocar o(s) colaborador(es) para uma tarefa
	*
	* @param request
	* @return
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView alocarColaborador(WebRequestContext request){
		
		String whereIn = request.getParameter("cdtarefa");
    	String whereInColaborador = request.getParameter("selectedItensColaborador");
    	String[] idsColaborador = whereInColaborador.split(",");
    	String[] ids = whereIn.split(",");				
		
		if(whereIn == null || whereInColaborador == null){			
			request.addError("Nenhum item selecionado");
			return new ModelAndView("redirect: /projeto/crud/Tarefa");
		}
		
		List<Tarefacolaborador> listaNaoIncluir = new ArrayList<Tarefacolaborador>();
    	for (String tarefa : ids) {
    		List<Tarefacolaborador> lista = tarefacolaboradorService.findColaboradorNaTarefa(tarefa, whereInColaborador);
    		
    		if(lista != null && !lista.isEmpty()){    			
    			request.addError("Colaborador j� est� alocado para esta tarefa.");
    			return sendRedirectToAction("listagem");
    		}else{    			
    			listaNaoIncluir.addAll(lista);
    		}    		
		}    	
    	
    	List<Tarefarecursohumano> listaTarefarecursohumano = new ArrayList<Tarefarecursohumano>();
    	List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
    	
    	for(int i = 0; i < idsColaborador.length; i++){
    		Colaborador c = new Colaborador();
    		c.setCdpessoa(Integer.parseInt(idsColaborador[i].trim()));
    		Colaboradorcargo colaboradorcargo = colaboradorcargoService.findCargoAtual(c);
    		if(colaboradorcargo != null && colaboradorcargo.getCargo() != null ){
    			c.setCargoColaboradorcargo(colaboradorcargo.getCargo());
    		}    		
    		listaColaborador.add(c);    		
    	}
    	
    	StringBuilder idsTarefaTrh = new StringBuilder("");
    	for(int i = 0; i < ids.length; i++){
    		if( i+1 == ids.length)
    			idsTarefaTrh.append(ids[i]);
    		else
    			idsTarefaTrh.append(ids[i]).append(",");
    	}    	
    	if(!idsTarefaTrh.toString().equals("")){
    		listaTarefarecursohumano = tarefarecursohumanoService.carregaCargoTarefa(idsTarefaTrh.toString());
    	}
    	
    	Tarefa tarefaProjeto = new Tarefa();
    	StringBuilder nomesProjetos = new StringBuilder("");
    	boolean save = false;
    	boolean possuiCargo = false;
    	
		for (int y = 0; y < ids.length; y++) {
			for(int i = 0; i < idsColaborador.length; i++){
				boolean naoSalvar = false;
				for (Tarefacolaborador tarefacolaboradorAux : listaNaoIncluir) {
					if(tarefacolaboradorAux.getTarefa().getCdtarefa().equals(Integer.valueOf(ids[y].trim())) && tarefacolaboradorAux.getColaborador().getCdpessoa().equals(Integer.valueOf(idsColaborador[i].trim()))){
						naoSalvar = true;
						break;
					}
				}
				if(naoSalvar)
					continue;
				
				//Verifica se um colaborador selecionado n�o tenha como cargo atual qualquer cargo definido em Recursos Humanos
				possuiCargo = false;
				if(listaTarefarecursohumano != null && !listaTarefarecursohumano.isEmpty()){
					for(Tarefarecursohumano trh : listaTarefarecursohumano){
						if(trh.getCargo() != null && trh.getCargo().getCdcargo() != null && 
								trh.getTarefa() != null && trh.getTarefa().getCdtarefa() != null){
							
							if(trh.getTarefa().getCdtarefa().toString().equals(ids[y].trim()) && listaColaborador != null && !listaColaborador.isEmpty()){
								for(Colaborador colaborador : listaColaborador){
									if(colaborador.getCargoColaboradorcargo() != null && colaborador.getCargoColaboradorcargo().getCdcargo() != null &&
											colaborador.getCargoColaboradorcargo().equals(trh.getCargo())){
										possuiCargo = true;
									}
								}
							}
						}
					}
				}
				
				//VERIFICA SE UM COLABORADOR EST� ALOCADO EM MAIS DE UM PROJETO NO MESMO PER�ODO DE TEMPO
				tarefaProjeto.setCdtarefa(Integer.parseInt(ids[y].trim()));
				tarefaProjeto = tarefaService.load(tarefaProjeto);
				if(tarefaProjeto != null && tarefaProjeto.getDtinicio() != null && tarefaProjeto.getDuracao() != null && tarefaProjeto.getDtfim() != null){
					List<Tarefa> listTarefasConflitantes = tarefaService.findTarefasConflitantesDataColaborador(new Colaborador(Integer.parseInt(idsColaborador[i].trim())), tarefaProjeto.getDtinicio());
					if(listTarefasConflitantes != null && !listTarefasConflitantes.isEmpty()){						
						for(Tarefa tarefa : listTarefasConflitantes){
							if(tarefaProjeto.getDtinicio() != null && tarefa.getDtinicio() != null && tarefa.getDtfim() != null){
								if(SinedDateUtils.afterOrEqualsIgnoreHour(tarefaProjeto.getDtinicio(), tarefa.getDtinicio()) && 
										SinedDateUtils.beforeOrEqualIgnoreHour(tarefaProjeto.getDtinicio(), tarefa.getDtfim())){
									if(tarefa.getPlanejamento() != null && tarefa.getPlanejamento().getProjeto() != null &&	
											!nomesProjetos.toString().contains(tarefa.getPlanejamento().getProjeto().getNome())){
										nomesProjetos.append(tarefa.getPlanejamento().getProjeto().getNome() + ", ");
									}						
								}
							}
						}
					}
				}
				
				if(!possuiCargo && listaTarefarecursohumano != null && !listaTarefarecursohumano.isEmpty()){
					StringBuilder nomeColaborador = new StringBuilder("");
					StringBuilder tarefaDescricao = new StringBuilder("");
					for(Colaborador colaborador : listaColaborador){
						if(colaborador.getCdpessoa().toString().equals(idsColaborador[i])){
							nomeColaborador.append(colaborador.getNome() != null ? colaborador.getNome() : "");
							break;
						}
					}
					for(Tarefarecursohumano tarefarecursohumano : listaTarefarecursohumano){
						if(tarefarecursohumano.getTarefa().getCdtarefa().toString().equals(ids[y])){
							tarefaDescricao.append(tarefarecursohumano.getTarefa().getDescricao() != null ? tarefarecursohumano.getTarefa().getDescricao() : "");
							break;
						}
					}
					request.addMessage("O Colaborador " + nomeColaborador +" n�o tem o cargo indicado para a tarefa " + tarefaDescricao);
				}
				
				Tarefacolaborador tc = new Tarefacolaborador();
				Tarefa t = new Tarefa();
				t.setCdtarefa(Integer.parseInt(ids[y].trim()));
				tc.setTarefa(t);
				tc.setColaborador(new Colaborador(Integer.parseInt(idsColaborador[i].trim())));
				tc.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				tc.setDtaltera(new Timestamp(System.currentTimeMillis()));
				
				tarefacolaboradorService.saveOrUpdate(tc);
				save = true;
			}
		}
		
		
		if(save) {
			if(!nomesProjetos.toString().equals("")){
				request.addMessage("Colaborador alocado no projeto " + nomesProjetos + " neste per�odo", MessageType.WARN);
			}
			request.addMessage("Colaborador(es) inclu�do(s) na(s) tarefa(s) com sucesso.");			
		}
		
		return new ModelAndView("redirect:/projeto/crud/Tarefa");
		
	}
	
	/**
	* M�todo que altera a situa��o da Tarefa para conclu�da
	*
	* @param request
	* @return
	* @since Aug 8, 2011
	* @author Luiz Fernando F Silva
	*/
	public ModelAndView concluirTarefa(WebRequestContext request){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		String entrada = request.getParameter("entrada");
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView();
		}
		
		List<Tarefa> listaTarefa = tarefaService.findSituacaoForTarefa(whereIn);
		
		if(listaTarefa != null && !listaTarefa.isEmpty()){
			for(Tarefa tarefa : listaTarefa){
				if(tarefa.getTarefasituacao() != null && tarefa.getTarefasituacao().equals(Tarefasituacao.CONCLUIDA)){
					request.addError("A tarefa j� est� com a situa��o como conclu�da.");
					if(entrada.equals("true")){						
						return new ModelAndView("redirect:/projeto/crud/Tarefa?ACAO=consultar&cdtarefa=" + whereIn);
					}else{
						return sendRedirectToAction("listagem");
					}					
				}
			}
			
			for(Tarefa tarefa : listaTarefa){
				tarefa.setTarefasituacao(Tarefasituacao.CONCLUIDA);
				tarefaService.updateSituacao(Tarefasituacao.CONCLUIDA, tarefa);
			}
		}
		request.addMessage("Tarefa(s) conclu�da(s) com sucesso.");
		if(entrada.equals("true")){						
			return new ModelAndView("redirect:/projeto/crud/Tarefa?ACAO=consultar&cdtarefa=" + whereIn);
		}else{
			return sendRedirectToAction("listagem");
		}				
	}
	
	/**
	 * Action que reabre uma tarefa concluida
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/07/2014
	 */
	public ModelAndView reabrirTarefa(WebRequestContext request){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		String entrada = request.getParameter("entrada");
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView();
		}
		
		List<Tarefa> listaTarefa = tarefaService.findSituacaoForTarefa(whereIn);
		
		if(listaTarefa != null && !listaTarefa.isEmpty()){
			for(Tarefa tarefa : listaTarefa){
				if(tarefa.getTarefasituacao() != null && !tarefa.getTarefasituacao().equals(Tarefasituacao.CONCLUIDA)){
					request.addError("A(s) tarefa(s) tem que ter a situa��o como conclu�da.");
					if(entrada.equals("true")){						
						return new ModelAndView("redirect:/projeto/crud/Tarefa?ACAO=consultar&cdtarefa=" + whereIn);
					}else{
						return sendRedirectToAction("listagem");
					}					
				}
			}
			
			for(Tarefa tarefa : listaTarefa){
				tarefa.setTarefasituacao(null);
				tarefaService.updateSituacao(tarefaService.verificaSitucaotarefa(tarefa), tarefa);
			}
		}
		request.addMessage("Tarefa(s) reaberta(s) com sucesso.");
		if(entrada.equals("true")){						
			return new ModelAndView("redirect:/projeto/crud/Tarefa?ACAO=consultar&cdtarefa=" + whereIn);
		}else{
			return sendRedirectToAction("listagem");
		}				
	}
	
	/**
	* M�todo ajax para verificar se existe conflito de hor�rio ao alocar um colaborador para a tarefa 
	*
	* @param request
	* @param colaborador
	* @since Aug 11, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxVerificaConflitoHorarioColaborador(WebRequestContext request){
		View view = View.getCurrent();		
		request.getServletResponse().setContentType("text/html");
		
		boolean sucesso = false;
		
		try{
			Date dtinicio = SinedDateUtils.stringToDate(request.getParameter("dtinicio"));
			Colaborador colaborador = new Colaborador(Integer.parseInt(request.getParameter("cdcolaborador")));
			
			List<Tarefa> listTarefasConflitantes = tarefaService.findTarefasConflitantesDataColaborador(colaborador, dtinicio);
			List<String> listaNomesProjetos = new ArrayList<String>();
			
			if(listTarefasConflitantes != null && !listTarefasConflitantes.isEmpty()){						
				for(Tarefa tarefa : listTarefasConflitantes){
					if(SinedDateUtils.afterOrEqualsIgnoreHour(dtinicio, tarefa.getDtinicio()) && 
							SinedDateUtils.beforeOrEqualIgnoreHour(dtinicio, tarefa.getDtfim())){
						if(tarefa.getPlanejamento() != null && 
								tarefa.getPlanejamento().getProjeto() != null &&	
								!listaNomesProjetos.contains(tarefa.getPlanejamento().getProjeto().getNome())){
							listaNomesProjetos.add(tarefa.getPlanejamento().getProjeto().getNome());
						}						
					}									
				}
				
				if(listaNomesProjetos.size() > 0){
					view.println("var nomesProjetos = '" + CollectionsUtil.concatenate(listaNomesProjetos, ", ") + "';");
					view.println("var conflito = true;");
				} else {
					view.println("var conflito = false;");
				}
				
				sucesso = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		view.println("var sucesso = " + sucesso + ";");
	}
	
	public ModelAndView atualizarSituacaoAposGerarRequisicao(WebRequestContext request, Tarefa tarefa){
		if(tarefa == null || tarefa.getCdstarefa() == null || tarefa.getCdstarefa().trim().equals("")){
			request.addError("N�o foi poss�vel obter o c�digo da tarefa.");
			return sendRedirectToAction("listagem");
		}		
		
		List<Integer> listaCdtarefa = SinedUtil.getListaByIds(tarefa.getCdstarefa());
		for (Integer cdtarefa: listaCdtarefa){
			Tarefa tarefa2 = new Tarefa(cdtarefa);
			tarefa2 = tarefaService.load(tarefa2, "tarefa.cdtarefa, tarefa.tarefasituacao, tarefa.dtinicio, tarefa.dtfim, tarefa.duracao");
			if(tarefa2.getDtinicio() == null){
				tarefaService.updateDatainicio(SinedDateUtils.currentDate(), tarefa2);
			} 
			tarefaService.atualizaSituacaoTarefas(tarefa2.getCdtarefa().toString());
		}		
		
		request.addMessage("Situa��o(�es) da(s) tarefa(s) atualizada(s) com sucesso.");
		return sendRedirectToAction("listagem");
	}	
	
	public ModelAndView verificaColaboradorExclusivo(WebRequestContext request, Tarefa bean){
		boolean existeColaboradorExclusivoAlocado = false;
		StringBuilder mensagem = new StringBuilder();
		
		Boolean acaoAlocarColaborador = Boolean.valueOf(request.getParameter("isAlocarColaborador") != null ? request.getParameter("isAlocarColaborador"): "false");
		List<Tarefa> listaTarefa = new ArrayList<Tarefa>();
		if(acaoAlocarColaborador){
			String whereInTarefas = request.getParameter("whereInTarefas");
			if(StringUtils.isNotEmpty(whereInTarefas)){
				listaTarefa = tarefaService.findForVerificarAlocacaoTarefa(whereInTarefas);
				if(SinedUtil.isListNotEmpty(listaTarefa)){
					for(Tarefa tarefa : listaTarefa){
						tarefa.setListaTarefacolaborador(bean.getListaTarefacolaborador());
					}
				}
			}
			
		}else {
			listaTarefa.add(bean);
		}
		
		if(SinedUtil.isListNotEmpty(listaTarefa)){
			for(Tarefa tarefa : listaTarefa){
				if(tarefa.getDtinicio() != null && SinedUtil.isListNotEmpty(tarefa.getListaTarefacolaborador())){
					for(Tarefacolaborador tarefacolaborador : tarefa.getListaTarefacolaborador()){
						if(tarefacolaborador.getColaborador() != null){
							if(tarefacolaboradorService.existeColaboradorExclusivoAlocadoPeriodo(
																tarefa, 
																tarefa.getDtinicio(),
																tarefacolaborador.getColaborador(),
																tarefacolaborador.getExclusivo() != null && tarefacolaborador.getExclusivo())){
								existeColaboradorExclusivoAlocado = true;
								if(StringUtils.isEmpty(mensagem.toString())){
									mensagem.append("Existe(m) colaborador(es) exclusivo(s) alocado(s) em mais de uma tarefa em um mesmo per�odo. Deseja continuar?\n\n");
								}
								mensagem.append(tarefacolaborador.getColaborador().getNome() + "\n");
							}
						}
					}
				}
			}
		}
		
		return new JsonModelAndView()
			.addObject("existeColaboradorExclusivoAlocado", existeColaboradorExclusivoAlocado)
			.addObject("mensagem", mensagem.toString());
	}
	
	/**
	 * M�todo que verifica se existe tarefa conclu�da a partir de uma lista de tarefas
	 * @param request
	 * @return
	 * @author Danilo Guimar�es
	 */
	public ModelAndView ajaxExisteTarefaConcluida(WebRequestContext request){
		String cdstarefa = request.getParameter("cdstarefa");
		
		Boolean existeConcluida = false;
		List<Tarefa> listaTarefa = tarefaService.findSituacaoTarefas(cdstarefa);
		if(listaTarefa != null && !listaTarefa.isEmpty()){
			for(Tarefa item : listaTarefa){
				if(item != null && item.getTarefasituacao() != null && item.getTarefasituacao().equals(Tarefasituacao.CONCLUIDA)){
					existeConcluida = true;
					break;
				}
			}
		}
		
		return new JsonModelAndView().addObject("existeConcluida", existeConcluida);
	}
}
