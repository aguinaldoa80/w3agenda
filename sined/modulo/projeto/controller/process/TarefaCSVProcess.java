package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.service.TarefaService;

@Controller(
		path="/projeto/process/TarefaCSV",
		authorizationModule=ProcessAuthorizationModule.class
)
public class TarefaCSVProcess extends ResourceSenderController<Tarefa>{

	private TarefaService tarefaService;
	
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	Tarefa tarefa) throws Exception {
		return tarefaService.preparaArquivoTarefaCSV(request.getParameter("selectedItens"));
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Tarefa tarefa) throws Exception {
		return null;
	}
	
}
