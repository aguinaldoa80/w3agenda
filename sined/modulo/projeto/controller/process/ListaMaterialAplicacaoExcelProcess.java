package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.OrcamentomaterialService;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
		path="/projeto/process/ListaMaterialAplicacaoExcel",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ListaMaterialAplicacaoExcelProcess extends ResourceSenderController<FiltroListagemSined>{

	private OrcamentomaterialService orcamentomaterialService;
	
	public void setOrcamentomaterialService(
			OrcamentomaterialService orcamentomaterialService) {
		this.orcamentomaterialService = orcamentomaterialService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	FiltroListagemSined filtro) throws Exception {
		return orcamentomaterialService.preparaArquivoExcelFormatado(request);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, FiltroListagemSined cotacao) throws Exception {
		return null;
	}
	
}
