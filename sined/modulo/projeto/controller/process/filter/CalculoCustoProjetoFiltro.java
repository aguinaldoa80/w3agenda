package br.com.linkcom.sined.modulo.projeto.controller.process.filter;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Periodo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;

public class CalculoCustoProjetoFiltro {
	
	protected Projeto projeto;
	protected Planejamento planejamento;
	protected List<Periodo> listaCalculoCustoProjetoPeriodoFiltro;
		
	public Projeto getProjeto() {
		return projeto;
	}
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public List<Periodo> getListaCalculoCustoProjetoPeriodoFiltro() {
		return listaCalculoCustoProjetoPeriodoFiltro;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setListaCalculoCustoProjetoPeriodoFiltro(
			List<Periodo> listaCalculoCustoProjetoPeriodoFiltro) {
		this.listaCalculoCustoProjetoPeriodoFiltro = listaCalculoCustoProjetoPeriodoFiltro;
	}	
	
}
