package br.com.linkcom.sined.modulo.projeto.controller.process.filter;

import br.com.linkcom.sined.geral.bean.Orcamento;

public class TarefaorcamentodetalhadoCSVFiltro {
	
	protected Orcamento orcamento;
	protected Boolean exibirrecursosdiretos;
	protected Boolean exibirrecursosindiretos;
	
	public Orcamento getOrcamento() {
		return orcamento;
	}
	public Boolean getExibirrecursosdiretos() {
		return exibirrecursosdiretos;
	}
	public void setExibirrecursosdiretos(Boolean exibirrecursosdiretos) {
		this.exibirrecursosdiretos = exibirrecursosdiretos;
	}
	public Boolean getExibirrecursosindiretos() {
		return exibirrecursosindiretos;
	}
	public void setExibirrecursosindiretos(Boolean exibirrecursosindiretos) {
		this.exibirrecursosindiretos = exibirrecursosindiretos;
	}
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
}
