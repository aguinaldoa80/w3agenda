package br.com.linkcom.sined.modulo.projeto.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.SinedDateUtils;

public class ApuracaoResultadoProjetoFiltro {
	
	private Projeto projeto;
	private Date dtinicio = SinedDateUtils.firstDateOfMonth();
	private Date dtfim = SinedDateUtils.lastDateOfMonth();
	private Contagerencial contagerencial;
	
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("Data final")
	public Date getDtfim() {
		return dtfim;
	}
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
}
