package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService;

@Controller(
		path="/projeto/process/HistogramaExcel",
		authorizationModule=ProcessAuthorizationModule.class
)
public class HistogramaExcelProcess extends ResourceSenderController<Orcamento>{

	private PeriodoorcamentocargoService periodoorcamentocargoService;
	
	public void setPeriodoorcamentocargoService(
			PeriodoorcamentocargoService periodoorcamentocargoService) {
		this.periodoorcamentocargoService = periodoorcamentocargoService;
	}
	

	
	@Override
	public Resource generateResource(WebRequestContext request,	Orcamento orcamento) throws Exception {
		return periodoorcamentocargoService.preparaArquivoExcelFormatado(orcamento);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Orcamento orcamento) throws Exception {
		return null;
	}
	
}
