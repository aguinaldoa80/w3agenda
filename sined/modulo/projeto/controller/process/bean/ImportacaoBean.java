package br.com.linkcom.sined.modulo.projeto.controller.process.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentomaterial;
import br.com.linkcom.sined.geral.bean.Orcamentomaterialitem;


public class ImportacaoBean{

	protected Arquivo arquivo;
	protected Orcamento orcamento;
	
	protected Boolean haveTarefa = false;
	protected List<Orcamentomaterialitem> listaOrcamentomaterial;	
	protected Orcamentomaterial orcamentomaterial;
	
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@DisplayName("Or�amento")
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	public Boolean getHaveTarefa() {
		return haveTarefa;
	}
	
	public List<Orcamentomaterialitem> getListaOrcamentomaterial() {
		return listaOrcamentomaterial;
	}
	
	public Orcamentomaterial getOrcamentomaterial() {
		return orcamentomaterial;
	}
	
	public void setOrcamentomaterial(Orcamentomaterial orcamentomaterial) {
		this.orcamentomaterial = orcamentomaterial;
	}
	
	public void setListaOrcamentomaterial(
			List<Orcamentomaterialitem> listaOrcamentomaterial) {
		this.listaOrcamentomaterial = listaOrcamentomaterial;
	}
	
	public void setHaveTarefa(Boolean haveTarefa) {
		this.haveTarefa = haveTarefa;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

}
