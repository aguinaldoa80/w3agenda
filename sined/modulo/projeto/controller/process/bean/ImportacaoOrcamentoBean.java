package br.com.linkcom.sined.modulo.projeto.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Projetotipo;

/**
 * Classe usada na parte em flex para a importa��o do <code>Orcamento</code> para o <code>Projeto</code>.
 * 
 * @author Rodrigo Freitas
 */
public class ImportacaoOrcamentoBean {
	
	/**********************
	 ******* PROJETO ******
	 **********************/
	protected String descricaoProjeto;
	protected Projetotipo projetotipoProjeto;
	protected String siglaProjeto;
	protected Date dtProjeto;
	protected Date dtfimProjeto;
	protected String enderecoProjeto;
	protected Municipio municipioProjeto;
	protected Colaborador responsavelProjeto;
	protected Cliente clienteProjeto;
	protected Empresa empresaProjeto;
	protected Centrocusto centrocustoProjeto;
	
	/***************************
	 ******* PLANEJAMENTO ******
	 ***************************/
	protected String descricaoPlanej;
	protected String versaoPlanej;
	protected String tamanhotempoPlanej;
	protected Date dtinicioPlanej;
	protected Date dtfimPlanej;
	protected String custoPlanej;
	
	
	/*********************
	 ******* TAREFA ******
	 *********************/
	protected Boolean eapTarefa;
	protected Boolean modTarefa;
	protected Boolean matTarefa;
	
	/**************************
	 ******* M�O DE OBRA ******
	 **************************/
	protected Boolean mod;
	protected Boolean moi;
	
	/**************************
	 ******* DESPESAS *********
	 **************************/
	protected Boolean despesasMateriais;
	protected Boolean despesasMod;
	protected Boolean despesasMoi;
	
	/*************************
	 ******* COMPOSI��O ******
	 *************************/
	protected List<Composicaoorcamento> listaComposicao;
	
	protected Integer cdorcamento;

	public String getDescricaoProjeto() {
		return descricaoProjeto;
	}

	public Date getDtProjeto() {
		return dtProjeto;
	}

	public Municipio getMunicipioProjeto() {
		return municipioProjeto;
	}

	public Colaborador getResponsavelProjeto() {
		return responsavelProjeto;
	}

	public Cliente getClienteProjeto() {
		return clienteProjeto;
	}

	public String getDescricaoPlanej() {
		return descricaoPlanej;
	}

	public Date getDtinicioPlanej() {
		return dtinicioPlanej;
	}

	public Date getDtfimPlanej() {
		return dtfimPlanej;
	}

	public Boolean getEapTarefa() {
		return eapTarefa;
	}

	public Boolean getModTarefa() {
		return modTarefa;
	}

	public Boolean getMatTarefa() {
		return matTarefa;
	}

	public Boolean getMod() {
		return mod;
	}

	public Boolean getMoi() {
		return moi;
	}

	public List<Composicaoorcamento> getListaComposicao() {
		return listaComposicao;
	}
	
	public String getVersaoPlanej() {
		return versaoPlanej;
	}

	public String getTamanhotempoPlanej() {
		return tamanhotempoPlanej;
	}

	public String getCustoPlanej() {
		return custoPlanej;
	}
	
	public Integer getCdorcamento() {
		return cdorcamento;
	}
	
	public Date getDtfimProjeto() {
		return dtfimProjeto;
	}
	
	public Boolean getDespesasMateriais() {
		return despesasMateriais;
	}

	public Boolean getDespesasMod() {
		return despesasMod;
	}

	public Boolean getDespesasMoi() {
		return despesasMoi;
	}

	public Empresa getEmpresaProjeto() {
		return empresaProjeto;
	}

	public Centrocusto getCentrocustoProjeto() {
		return centrocustoProjeto;
	}

	public String getSiglaProjeto() {
		return siglaProjeto;
	}

	public void setSiglaProjeto(String siglaProjeto) {
		this.siglaProjeto = siglaProjeto;
	}

	public void setEmpresaProjeto(Empresa empresaProjeto) {
		this.empresaProjeto = empresaProjeto;
	}

	public void setCentrocustoProjeto(Centrocusto centrocustoProjeto) {
		this.centrocustoProjeto = centrocustoProjeto;
	}

	public void setDespesasMateriais(Boolean despesasMateriais) {
		this.despesasMateriais = despesasMateriais;
	}

	public void setDespesasMod(Boolean despesasMod) {
		this.despesasMod = despesasMod;
	}

	public void setDespesasMoi(Boolean despesasMoi) {
		this.despesasMoi = despesasMoi;
	}

	public void setDtfimProjeto(Date dtfimProjeto) {
		this.dtfimProjeto = dtfimProjeto;
	}
	
	public void setCdorcamento(Integer cdorcamento) {
		this.cdorcamento = cdorcamento;
	}

	public void setVersaoPlanej(String versaoPlanej) {
		this.versaoPlanej = versaoPlanej;
	}

	public void setTamanhotempoPlanej(String tamanhotempoPlanej) {
		this.tamanhotempoPlanej = tamanhotempoPlanej;
	}

	public void setCustoPlanej(String custoPlanej) {
		this.custoPlanej = custoPlanej;
	}

	public void setDescricaoProjeto(String descricaoProjeto) {
		this.descricaoProjeto = descricaoProjeto;
	}

	public void setDtProjeto(Date dtProjeto) {
		this.dtProjeto = dtProjeto;
	}

	public void setMunicipioProjeto(Municipio municipioProjeto) {
		this.municipioProjeto = municipioProjeto;
	}

	public void setResponsavelProjeto(Colaborador responsavelProjeto) {
		this.responsavelProjeto = responsavelProjeto;
	}

	public void setClienteProjeto(Cliente clienteProjeto) {
		this.clienteProjeto = clienteProjeto;
	}

	public void setDescricaoPlanej(String descricaoPlanej) {
		this.descricaoPlanej = descricaoPlanej;
	}

	public void setDtinicioPlanej(Date dtinicioPlanej) {
		this.dtinicioPlanej = dtinicioPlanej;
	}

	public void setDtfimPlanej(Date dtfimPlanej) {
		this.dtfimPlanej = dtfimPlanej;
	}

	public void setEapTarefa(Boolean eapTarefa) {
		this.eapTarefa = eapTarefa;
	}

	public void setModTarefa(Boolean modTarefa) {
		this.modTarefa = modTarefa;
	}

	public void setMatTarefa(Boolean matTarefa) {
		this.matTarefa = matTarefa;
	}

	public void setMod(Boolean mod) {
		this.mod = mod;
	}

	public void setMoi(Boolean moi) {
		this.moi = moi;
	}

	public void setListaComposicao(List<Composicaoorcamento> listaComposicao) {
		this.listaComposicao = listaComposicao;
	}

	public Projetotipo getProjetotipoProjeto() {
		return projetotipoProjeto;
	}

	public String getEnderecoProjeto() {
		return enderecoProjeto;
	}

	public void setProjetotipoProjeto(Projetotipo projetotipoProjeto) {
		this.projetotipoProjeto = projetotipoProjeto;
	}

	public void setEnderecoProjeto(String enderecoProjeto) {
		this.enderecoProjeto = enderecoProjeto;
	}
}
