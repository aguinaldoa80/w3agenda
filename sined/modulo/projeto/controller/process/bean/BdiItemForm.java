package br.com.linkcom.sined.modulo.projeto.controller.process.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Contagerencial;

public class BdiItemForm {

	protected Integer cdBdiItem;
	protected BdiItemForm bdiItemPai;	
	protected String nome;
	protected Double valor;
	protected Contagerencial contagerencial;
	protected List<BdiItemForm> listaBdiItemFormFilho = new ArrayList<BdiItemForm>();
	
	public Integer getCdBdiItem() {
		return cdBdiItem;
	}
	public BdiItemForm getBdiItemPai() {
		return bdiItemPai;
	}
	public String getNome() {
		return nome;
	}
	public Double getValor() {
		return valor;
	}
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	public List<BdiItemForm> getListaBdiItemFormFilho() {
		return listaBdiItemFormFilho;
	}
	public void setCdBdiItem(Integer cdBdiItem) {
		this.cdBdiItem = cdBdiItem;
	}
	public void setBdiItemPai(BdiItemForm bdiItemPai) {
		this.bdiItemPai = bdiItemPai;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setListaBdiItemFormFilho(List<BdiItemForm> listaBdiItemFormFilho) {
		this.listaBdiItemFormFilho = listaBdiItemFormFilho;
	}
	
	/***********************/
	/** Equals e HashCode **/
	/***********************/
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BdiItemForm) {
			BdiItemForm that = (BdiItemForm) obj;
			return this.getCdBdiItem().equals(that.getCdBdiItem());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdBdiItem != null) {
			return cdBdiItem.hashCode();
		}
		return super.hashCode();		
	}	
	
}
