package br.com.linkcom.sined.modulo.projeto.controller.process.bean;

import java.io.UnsupportedEncodingException;

public class ImportarListaMaterialBean {
	
	protected String descricao;
	protected String unidade;
	protected Double qtde;
	protected Double cdmaterial;
	
	public String getDescricao() {
		return descricao;
	}
	public String getUnidade() {
		return unidade;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getCdmaterial() {
		return cdmaterial;
	}
	public void setDescricao(String descricao) {
		try {
			byte[] stringArray = new String(descricao.getBytes()).getBytes();
			this.descricao = new String(stringArray,"LATIN1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			this.descricao = descricao;
		}
	}
	public void setUnidade(String unidade) {
		try {
			byte[] stringArray = new String(unidade.getBytes()).getBytes();
			this.unidade = new String(stringArray,"LATIN1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			this.unidade = unidade;
		}
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setCdmaterial(Double cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	
}
