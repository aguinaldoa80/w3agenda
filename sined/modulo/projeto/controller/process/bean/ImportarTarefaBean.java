package br.com.linkcom.sined.modulo.projeto.controller.process.bean;

import java.io.UnsupportedEncodingException;

public class ImportarTarefaBean {
	
	protected Double id;
	protected String descricao;
	protected String unidade;
	protected Double qtde;
	protected String indice;
	protected Double idtarefapai;
	
	public Double getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getUnidade() {
		return unidade;
	}
	public Double getQtde() {
		return qtde;
	}
	public String getIndice() {
		return indice;
	}
	public Double getIdtarefapai() {
		return idtarefapai;
	}
	public void setIdtarefapai(Double idtarefapai) {
		this.idtarefapai = idtarefapai;
	}
	public void setId(Double id) {
		this.id = id;
	}
	public void setDescricao(String descricao) {
		try {
			byte[] stringArray = new String(descricao.getBytes()).getBytes();
			this.descricao = new String(stringArray,"LATIN1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			this.descricao = descricao;
		}
	}
	public void setUnidade(String unidade) {
		try {
			byte[] stringArray = new String(unidade.getBytes()).getBytes();
			this.unidade = new String(stringArray,"LATIN1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			this.unidade = unidade;
		}
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setIndice(String indice) {
		try {
			byte[] stringArray = new String(indice.getBytes()).getBytes();
			this.indice = new String(stringArray,"LATIN1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			this.indice = indice;
		}
	}
	
}
