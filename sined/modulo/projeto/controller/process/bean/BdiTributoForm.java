package br.com.linkcom.sined.modulo.projeto.controller.process.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Contagerencial;

public class BdiTributoForm {

	protected Integer cdBdiTributo;
	protected BdiTributoForm bdiTributoPai;	
	protected String nome;
	protected Double valor;
	protected Contagerencial contagerencial;
	protected List<BdiTributoForm> listaBdiTributoFormFilho = new ArrayList<BdiTributoForm>();
	
	public Integer getCdBdiTributo() {
		return cdBdiTributo;
	}
	public BdiTributoForm getBdiTributoPai() {
		return bdiTributoPai;
	}
	public String getNome() {
		return nome;
	}
	public Double getValor() {
		return valor;
	}
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	public List<BdiTributoForm> getListaBdiTributoFormFilho() {
		return listaBdiTributoFormFilho;
	}
	public void setCdBdiTributo(Integer cdBdiTributo) {
		this.cdBdiTributo = cdBdiTributo;
	}
	public void setBdiTributoPai(BdiTributoForm bdiTributoPai) {
		this.bdiTributoPai = bdiTributoPai;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setListaBdiTributoFormFilho(List<BdiTributoForm> listaBdiTributoFormFilho) {
		this.listaBdiTributoFormFilho = listaBdiTributoFormFilho;
	}
	
	/***********************/
	/** Equals e HashCode **/
	/***********************/
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BdiTributoForm) {
			BdiTributoForm that = (BdiTributoForm) obj;
			return this.getCdBdiTributo().equals(that.getCdBdiTributo());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdBdiTributo != null) {
			return cdBdiTributo.hashCode();
		}
		return super.hashCode();		
	}	
	
}
