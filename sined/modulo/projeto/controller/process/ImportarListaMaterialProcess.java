package br.com.linkcom.sined.modulo.projeto.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Orcamentomaterial;
import br.com.linkcom.sined.geral.bean.Orcamentomaterialitem;
import br.com.linkcom.sined.geral.service.OrcamentomaterialService;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.ImportacaoBean;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.ImportarListaMaterialBean;
import br.com.linkcom.sined.util.SinedException;

@Controller(
		path="/projeto/process/ImportarListaMaterial",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ImportarListaMaterialProcess extends MultiActionController{
	
	private OrcamentomaterialService orcamentomaterialService;
	
	public void setOrcamentomaterialService(
			OrcamentomaterialService orcamentomaterialService) {
		this.orcamentomaterialService = orcamentomaterialService;
	}

	/**
	 * M�todo padr�o para carregamento da p�gina e processamento do arquivo.
	 *
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#processarArquivoImportacao
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#processaListaMateriais
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, ImportacaoBean bean) throws Exception{
		
		if (bean != null && bean.getArquivo() != null) {
			
			if(!bean.getArquivo().getContenttype().equals("application/msexcel") && !bean.getArquivo().getNome().endsWith(".xls")){
				bean.setArquivo(null);
				request.addError("Arquivo de formato inv�lido.");
				return new ModelAndView("/process/importacaoListamaterial","filtro",bean);
			}
			
			try{
				List<ImportarListaMaterialBean> lista = orcamentomaterialService.processarArquivoImportacao(bean.getArquivo().getContent());
				
				List<Orcamentomaterialitem> listaTarefa = orcamentomaterialService.processaListaMateriais(lista);
				
				bean.setListaOrcamentomaterial(listaTarefa);
				
			} catch(SinedException s){
				request.addError(s.getMessage());	
				s.printStackTrace();
				bean.setArquivo(null);
			} catch (Exception e) {
				request.addError("Erro no processamento do arquivo.");
				e.printStackTrace();
				bean.setArquivo(null);
			}
			
		} 

		return new ModelAndView("/process/importacaoListamaterial","filtro",bean);
	}
	
	/**
	 * M�todo para salvar a lista de material.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView salvaLista(WebRequestContext request, ImportacaoBean bean){
		
		Orcamentomaterial om = bean.getOrcamentomaterial();
		om.setListaOrcamentomaterialitem(bean.getListaOrcamentomaterial());
		om.setOrcamento(bean.getOrcamento());
		
		Money money = new Money();
		for (Orcamentomaterialitem item : om.getListaOrcamentomaterialitem()) {
			item.setFaturamentodireto(false);
			item.setIcms(0.0);
			item.setIpi(0.0);
			item.setIpiincluso(false);
			item.setTotalcompra(money);
			item.setTotalvenda(money);
			item.setValorunitariocompra(money);
			item.setValorunitariovenda(money);
		}
		
		orcamentomaterialService.saveOrUpdate(om);
		
		request.addMessage("Lista de Material(is) importada com sucesso.");
		
		return new ModelAndView("redirect:/projeto/process/ImportarListaMaterial");
	}
	
}
