package br.com.linkcom.sined.modulo.projeto.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.ApuracaoResultadoProjetoBean;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.ApuracaoResultadoProjetoListagemBean;
import br.com.linkcom.sined.modulo.projeto.controller.process.filter.ApuracaoResultadoProjetoFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path={"/projeto/process/ApuracaoResultadoProjeto", "/agro/process/ApuracaoResultadoProjeto"}, authorizationModule=ProcessAuthorizationModule.class)
public class ApuracaoResultadoProjetoProcess extends MultiActionController {
	
	private ContagerencialService contagerencialService;
	private ProjetoService projetoService;
	
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ApuracaoResultadoProjetoFiltro filtro){
		return new ModelAndView("process/apuracaoResultadoProjeto", "filtro", filtro);
	}
	
	public ModelAndView gerar(WebRequestContext request, ApuracaoResultadoProjetoFiltro filtro){
		List<ApuracaoResultadoProjetoBean> listaProvisoria = contagerencialService.findByApuracaoResultadoProjeto(filtro);
		for (ApuracaoResultadoProjetoBean bean : listaProvisoria) {
			if(bean.getHaveFilhos() != null && bean.getHaveFilhos()){
				Double valor = 0d;
				for (ApuracaoResultadoProjetoBean bean2 : listaProvisoria) {
					if(!bean.getCodigo().equals(bean2.getCodigo()) &&
							bean2.getIdentificador().startsWith(bean.getIdentificador() + ".")){
						valor += bean2.getValor();
					}
				}
				bean.setValor(SinedUtil.round(valor, 2));
			}
		}
		
		List<ApuracaoResultadoProjetoBean> lista = new ArrayList<ApuracaoResultadoProjetoBean>();
		for (ApuracaoResultadoProjetoBean bean : listaProvisoria) {
			if(bean.getValor() > 0){
				lista.add(bean);
			}
		}
		
		if(lista.size() == 0){
			request.addError("Nenhum resultado encontrado.");
			return continueOnAction("index", filtro);
		}
		
		request.setAttribute("lista", lista);
		request.setAttribute("projeto", projetoService.load(filtro.getProjeto(), "projeto.cdprojeto, projeto.area"));
		return new ModelAndView("process/apuracaoResultadoProjetoApresentacao", "filtro", filtro);
	}
	
	public ModelAndView listar(WebRequestContext request, ApuracaoResultadoProjetoFiltro filtro){
		List<ApuracaoResultadoProjetoListagemBean> lista = contagerencialService.findByApuracaoResultadoProjetoListagem(filtro);
		Double total = 0d;
		for (ApuracaoResultadoProjetoListagemBean apuracaoResultadoProjetoListagemBean : lista) {
			total += apuracaoResultadoProjetoListagemBean.getValor();
		}
		request.setAttribute("total", total);
		request.setAttribute("lista", lista);
		request.setAttribute("contagerencial", contagerencialService.load(filtro.getContagerencial(), "contagerencial.cdcontagerencial, contagerencial.nome, contagerencial.vcontagerencial"));
		return new ModelAndView("direct:process/apuracaoResultadoProjetoListagem");
	}

}
