package br.com.linkcom.sined.modulo.projeto.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Tarefaorcamento;
import br.com.linkcom.sined.geral.service.OrcamentoService;
import br.com.linkcom.sined.geral.service.OrcamentorecursohumanoService;
import br.com.linkcom.sined.geral.service.PeriodoorcamentocargoService;
import br.com.linkcom.sined.geral.service.RecursocomposicaoService;
import br.com.linkcom.sined.geral.service.TarefaorcamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.ImportacaoBean;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.ImportarTarefaBean;
import br.com.linkcom.sined.util.SinedException;

@Controller(
		path="/projeto/process/ImportarTarefa",
		authorizationModule=ProcessAuthorizationModule.class
)
public class ImportarTarefaProcess extends MultiActionController{
	
	private TarefaorcamentoService tarefaorcamentoService;
	private PeriodoorcamentocargoService periodoorcamentocargoService;
	private OrcamentoService orcamentoService;
	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private RecursocomposicaoService recursocomposicaoService;
	
	public void setRecursocomposicaoService(
			RecursocomposicaoService recursocomposicaoService) {
		this.recursocomposicaoService = recursocomposicaoService;
	}
	public void setOrcamentorecursohumanoService(
			OrcamentorecursohumanoService orcamentorecursohumanoService) {
		this.orcamentorecursohumanoService = orcamentorecursohumanoService;
	}
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	public void setPeriodoorcamentocargoService(
			PeriodoorcamentocargoService periodoorcamentocargoService) {
		this.periodoorcamentocargoService = periodoorcamentocargoService;
	}
	public void setTarefaorcamentoService(
			TarefaorcamentoService tarefaorcamentoService) {
		this.tarefaorcamentoService = tarefaorcamentoService;
	}

	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, ImportacaoBean bean) throws Exception{
		
		if (bean != null && bean.getArquivo() != null) {
			
			if(!bean.getArquivo().getContenttype().equals("application/msexcel") && !bean.getArquivo().getNome().endsWith(".xls")){
				bean.setArquivo(null);
				request.addError("Arquivo de formato inv�lido.");
				return new ModelAndView("/process/importacaotarefa","filtro",bean);
			}
			
			try{
				List<ImportarTarefaBean> lista = tarefaorcamentoService.processarArquivoImportacao(bean.getArquivo().getContent());
				
				tarefaorcamentoService.validaArquivo(lista);
				
				List<Tarefaorcamento> listaTarefa = tarefaorcamentoService.processaListaTarefas(lista, bean.getOrcamento());
				
				if(bean.getHaveTarefa() != null && bean.getHaveTarefa()){
					tarefaorcamentoService.deleteByOrcamento(bean.getOrcamento());
				}
				
				tarefaorcamentoService.saveListaTarefa(listaTarefa);
				
				Orcamento orcamento = orcamentoService.loadForEntrada(bean.getOrcamento());
				if(orcamento.getCalcularhistograma() != null && orcamento.getCalcularhistograma()){
					if(periodoorcamentocargoService.existeHistograma(orcamento)){
						periodoorcamentocargoService.atualizaValoresHistograma(orcamento);
					}
				} else {
					periodoorcamentocargoService.deleteByOrcamento(orcamento);
					orcamentorecursohumanoService.atualizaOrcamentoRecursoHumanoSemHistogramaFlex(orcamento);
					recursocomposicaoService.atualizaOrcamentoRecursoGeralSemHistogramaFlex(orcamento);
				}
				
				request.addMessage("Tarefa(s) importada(s) com sucesso.");
			} catch(SinedException s){
				request.addError(s.getMessage());	
				s.printStackTrace();
			} catch (Exception e) {
				request.addError("Erro no processamento do arquivo.");
				e.printStackTrace();
			} finally {
				bean.setArquivo(null);
				bean.setOrcamento(null);
			}
			
		} 

		return new ModelAndView("/process/importacaotarefa","filtro",bean);
	}
	
	public void ajaxVerificaTarefa(WebRequestContext request, ImportacaoBean bean) {
		if(tarefaorcamentoService.haveWithOrcamento(bean.getOrcamento())){
			View.getCurrent().eval("true");
		} else {
			View.getCurrent().eval("false");
		}
	}
	
}
