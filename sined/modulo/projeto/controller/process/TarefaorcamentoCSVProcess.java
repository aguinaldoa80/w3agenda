package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.TarefaorcamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.TarefaOrcamentoReportFiltro;

@Controller(
		path="/projeto/process/TarefaOrcamentoCSV",
		authorizationModule=ProcessAuthorizationModule.class
)
public class TarefaorcamentoCSVProcess extends ResourceSenderController<TarefaOrcamentoReportFiltro>{

	private TarefaorcamentoService tarefaorcamentoService;
	
	public void setTarefaorcamentoService(
			TarefaorcamentoService tarefaorcamentoService) {
		this.tarefaorcamentoService = tarefaorcamentoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	TarefaOrcamentoReportFiltro filtro) throws Exception {
		filtro = (TarefaOrcamentoReportFiltro)NeoWeb.getRequestContext().getSession().getAttribute(TarefaorcamentoService.SESSION_FILTRO_RELATORIO_TAREFA);
		NeoWeb.getRequestContext().getSession().removeAttribute(TarefaorcamentoService.SESSION_FILTRO_RELATORIO_TAREFA);
		
		return tarefaorcamentoService.preparaArquivoTarefaOrcamentoCSV(filtro);
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, TarefaOrcamentoReportFiltro filtro) throws Exception {
		return null;
	}
	
}
