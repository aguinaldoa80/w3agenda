package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.modulo.projeto.controller.process.filter.CalculoCustoProjetoFiltro;

@Bean
@Controller(
		path={"/projeto/process/GerenciaTarefa", "/agro/process/GerenciaTarefa"},
		authorizationModule=ProcessAuthorizationModule.class
)
public class GerenciaTarefaProcess extends MultiActionController {
		
//	private PlanejamentoService planejamentoService;
//	private UsuarioService usuarioService;
//	
//	public void setUsuarioService(UsuarioService usuarioService) {
//		this.usuarioService = usuarioService;
//	}
//	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
//		this.planejamentoService = planejamentoService;
//	}
//	
//	public static final String SESSION_PLANEJAMENTO_FLEX = "planejamento_flex_session";
	

	@DefaultAction
	public ModelAndView carrega(WebRequestContext request, CalculoCustoProjetoFiltro filtro) throws Exception {
		return new ModelAndView("process/gerenciaTarefa", "filtro", filtro);
	}
	
//	@Input("carrega")
//	@Action("abrePopUp")
//	public ModelAndView abrePopUp(WebRequestContext request, CalculoCustoProjetoFiltro filtro){
//		Planejamento plan = planejamentoService.loadForEntrada(filtro.getPlanejamento());
//		request.getSession().setAttribute(SESSION_PLANEJAMENTO_FLEX, plan);
//		if(TarefaService.mapPlanejamento.containsValue(plan)){
//			List<Usuario> lista = getKeysOfValueMap(TarefaService.mapPlanejamento, plan);		
//			lista = usuarioService.findForGerenciaPlanejamento(CollectionsUtil.listAndConcatenate(lista, "cdpessoa", ","));
//			for (Usuario usuario : lista) {
//				request.addError("N�o � poss�vel acessar esta funcionalidade o usu�rio "+usuario.getNome()+" j� est� com o planejamento aberto.");
//			}
//			request.getServletResponse().setContentType("text/html");
//			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
//			return null;
//		} else {
//			TarefaService.mapPlanejamento.put(((Usuario)Neo.getUser()).getCdpessoa(), plan);
//			return new ModelAndView("direct:/process/telaFlex");
//		}
//	}
//	
//	/**
//	 * M�todo para pegar as chaves do hashmap a partir do valor.
//	 *
//	 * @param map
//	 * @param value
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	public List<Usuario> getKeysOfValueMap(HashMap<Integer, Planejamento> map, Planejamento value){
//		Set<Entry<Integer, Planejamento>> entrySet = map.entrySet();
//		List<Usuario> lista = new ArrayList<Usuario>();
//		
//		for (Entry<Integer, Planejamento> entry : entrySet) {
//			if(entry.getValue().equals(value)){
//				lista.add(new Usuario(entry.getKey()));
//			}
//		}
//		
//		return lista;
//	}

}
