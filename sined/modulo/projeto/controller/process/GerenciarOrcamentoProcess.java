package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(
		path="/projeto/process/GerenciarOrcamento",
		authorizationModule=ProcessAuthorizationModule.class
)
public class GerenciarOrcamentoProcess extends MultiActionController{
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, FiltroListagemSined filtro){
		return new ModelAndView("process/gerenciarorcamento", "filtro", filtro);
	}
	
}
