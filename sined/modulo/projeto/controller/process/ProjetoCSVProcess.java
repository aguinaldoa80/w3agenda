package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ProjetoFiltro;

@Controller(path = "/projeto/process/ProjetoCSV", authorizationModule=ReportAuthorizationModule.class)
public class ProjetoCSVProcess extends ResourceSenderController<ProjetoFiltro>{

	private ProjetoService projetoService;
	
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,
			ProjetoFiltro filtro) throws Exception {
		return projetoService.prepararProjetoCSV(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, ProjetoFiltro filtro)
			throws Exception {
		return null;
	}

}
