package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.service.OrcamentoService;
import br.com.linkcom.sined.util.SinedException;

@Controller(
		path="/projeto/process/OrcamentoCSV",
		authorizationModule=ProcessAuthorizationModule.class
)
public class OrcamentoCSVProcess extends ResourceSenderController<Orcamento>{

	private OrcamentoService orcamentoService;
	
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	Orcamento orcamento) throws Exception {
		String cdorcamento = request.getParameter("cdorcamento");
		if(cdorcamento == null || cdorcamento.equals(""))
			throw new SinedException("Codigo do or�amento n�o pode ser nulo");
		return orcamentoService.preparaArquivoTarefaCSV(new Orcamento(Integer.valueOf(cdorcamento)));
		
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, Orcamento orcamento) throws Exception {
		return null;
	}
	
}
