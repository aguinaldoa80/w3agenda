package br.com.linkcom.sined.modulo.projeto.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.TarefaorcamentoService;
import br.com.linkcom.sined.modulo.projeto.controller.process.filter.TarefaorcamentodetalhadoCSVFiltro;

@Controller(path="/projeto/process/TarefaorcamentodetalhadoCSV", authorizationModule=ProcessAuthorizationModule.class)
public class TarefaorcamentodetalhadoCSVProcess extends ResourceSenderController<TarefaorcamentodetalhadoCSVFiltro>{

	private TarefaorcamentoService tarefaorcamentoService;
	
	public void setTarefaorcamentoService(
			TarefaorcamentoService tarefaorcamentoService) {
		this.tarefaorcamentoService = tarefaorcamentoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	TarefaorcamentodetalhadoCSVFiltro filtro) throws Exception {
		filtro = (TarefaorcamentodetalhadoCSVFiltro)NeoWeb.getRequestContext().getSession().getAttribute(TarefaorcamentoService.SESSION_FILTRO_RELATORIO_ORCAMENTO_DETALHADO);
		NeoWeb.getRequestContext().getSession().removeAttribute(TarefaorcamentoService.SESSION_FILTRO_RELATORIO_ORCAMENTO_DETALHADO);
		
		return tarefaorcamentoService.preparaArquivoTarefaorcamentodetalhadoCSV(filtro);
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, TarefaorcamentodetalhadoCSVFiltro filtro) throws Exception {
		return null;
	}
	
}
