package br.com.linkcom.sined.modulo.servicointerno.controller.report.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipo;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EventoservicointernoFiltro extends FiltroListagemSined{
	
	protected Date inicio = SinedDateUtils.firstDateOfMonth();
	protected Date fim = SinedDateUtils.lastDateOfMonth();
	protected Boolean comunicado=true;
	protected Boolean protocolo=true;
	protected Boolean solicitacao=true;
	protected String assunto;
	protected String remetente;
	protected String destinatario;
	protected Solicitacaoservicotipo tipo;
	
	
	public Date getInicio() {
		return inicio;
	}
	public Date getFim() {
		return fim;
	}
	
	public Boolean getComunicado() {
		return comunicado;
	}
	public Boolean getProtocolo() {
		return protocolo;
	}
	@DisplayName("Solicitação")
	public Boolean getSolicitacao() {
		return solicitacao;
	}
	
	@MaxLength(100)
	public String getAssunto() {
		return assunto;
	}
	
	@DisplayName("De")
	@MaxLength(100)
	public String getRemetente() {
		return remetente;
	}
	@DisplayName("Para")
	@MaxLength(100)
	public String getDestinatario() {
		return destinatario;
	}
	
	public Solicitacaoservicotipo getTipo() {
		return tipo;
	}
	
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public void setFim(Date fim) {
		this.fim = fim;
	}
	public void setComunicado(Boolean comunicado) {
		this.comunicado = comunicado;
	}
	
	public void setProtocolo(Boolean protocolo) {
		this.protocolo = protocolo;
	}
	
	public void setSolicitacao(Boolean solicitacao) {
		this.solicitacao = solicitacao;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}
	
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	
	public void setTipo(Solicitacaoservicotipo tipo) {
		this.tipo = tipo;
	}

}
