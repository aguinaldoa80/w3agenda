package br.com.linkcom.sined.modulo.servicointerno.controller.report.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.enumeration.RelatorioColaboradorAgendamentoOrderByEnum;

public class ColaboradoragendaservicoReportFiltro {
	
	protected Empresa empresa;
	protected Cliente cliente;
	protected Requisicao requisicao;
	protected Categoria categoria;
	protected Colaborador colaborador;
	protected Material material;
	protected Date dtinicio;
	protected Date dtfim;
	protected RelatorioColaboradorAgendamentoOrderByEnum orderby;
	protected Boolean asc;
	
	@DisplayName("Colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@DisplayName("Servi�o")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("Fim")
	public Date getDtfim() {
		return dtfim;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Requisicao getRequisicao() {
		return requisicao;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public RelatorioColaboradorAgendamentoOrderByEnum getOrderby() {
		return orderby;
	}
	public Boolean getAsc() {
		return asc;
	}
	public void setOrderby(RelatorioColaboradorAgendamentoOrderByEnum orderby) {
		this.orderby = orderby;
	}
	public void setAsc(Boolean asc) {
		this.asc = asc;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
}
