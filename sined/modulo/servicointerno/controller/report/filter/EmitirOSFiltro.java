package br.com.linkcom.sined.modulo.servicointerno.controller.report.filter;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EscolherModeloOSBean;

public class EmitirOSFiltro extends ReportTemplateFiltro {
	
	private String selectedItens;
	private ReportTemplateBean reportTemplate;
	private List<EscolherModeloOSBean> listaEscolherModeloOSBean;
	
	public String getSelectedItens() {
		return selectedItens;
	}
	public ReportTemplateBean getReportTemplate() {
		return reportTemplate;
	}
	public List<EscolherModeloOSBean> getListaEscolherModeloOSBean() {
		return listaEscolherModeloOSBean;
	}
	
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
	public void setReportTemplate(ReportTemplateBean reportTemplate) {
		this.reportTemplate = reportTemplate;
	}
	public void setListaEscolherModeloOSBean(List<EscolherModeloOSBean> listaEscolherModeloOSBean) {
		this.listaEscolherModeloOSBean = listaEscolherModeloOSBean;
	}
}
