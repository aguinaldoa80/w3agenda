package br.com.linkcom.sined.modulo.servicointerno.controller.report;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.bean.Requisicaoitem;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSAtividadeBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSHistoricoBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSItemBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSMaterialBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EscolherModeloOSBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.EmitirOSFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/servicointerno/relatorio/EmitirOS")
public class EmitirOSReport extends ReportTemplateController<EmitirOSFiltro> {
	
	private RequisicaoService requisicaoService;
	private EnderecoService enderecoService;
	private TelefoneService telefoneService;
	private ApontamentoService apontamentoService;
	
	public void setRequisicaoService(RequisicaoService requisicaoService) {
		this.requisicaoService = requisicaoService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setTelefoneService(TelefoneService telefoneService) {
		this.telefoneService = telefoneService;
	}
	public void setApontamentoService(ApontamentoService apontamentoService) {
		this.apontamentoService = apontamentoService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.EMITIR_ORDEM_SERVICO;
	}

	@Override
	protected String getFileName() {
		return "ordem_servico";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	EmitirOSFiltro filtro) {
		return getReportTemplateService().load(getReportTemplateBean(request, filtro));
	}
	
	private ReportTemplateBean getReportTemplateBean(WebRequestContext request, EmitirOSFiltro filtro) {
		if(request.getParameter("cdreporttemplate") != null) return new ReportTemplateBean(Integer.parseInt(request.getParameter("cdreporttemplate")));
		if(request.getParameter("reportTemplate.cdreporttemplate") != null) return new ReportTemplateBean(Integer.parseInt(request.getParameter("reportTemplate.cdreporttemplate")));
		
		if(SinedUtil.isListNotEmpty(filtro.getListaEscolherModeloOSBean())){
			for(EscolherModeloOSBean bean : filtro.getListaEscolherModeloOSBean()){
				if(bean.getReportTemplate() != null){
					return new ReportTemplateBean(bean.getReportTemplate().getCdreporttemplate());
				}
			}
		}
		
		return new ReportTemplateBean(filtro.getTemplate().getCdreporttemplate());
	}
	@Override
	protected LinkedList<ReportTemplateBean> carregaListaDados(WebRequestContext request, EmitirOSFiltro filtro) throws Exception {
		
		LinkedList<ReportTemplateBean> listaReportTemplateBean = new LinkedList<ReportTemplateBean>();
		List<Requisicao> listaRequisicao = requisicaoService.findForEmitirOS(filtro.getSelectedItens());
		Map<Integer, ReportTemplateBean> mapaReportTemplateBean = new HashMap<Integer, ReportTemplateBean>();
		ReportTemplateBean reportTemplateBeanFiltro = null;
		SimpleDateFormat formatadorData = new SimpleDateFormat("dd/MM/yyyy");
		
		if (filtro.getReportTemplate()!=null && filtro.getReportTemplate().getCdreporttemplate()!=null){
			reportTemplateBeanFiltro = getReportTemplateService().load(filtro.getReportTemplate());
		}else if (filtro.getListaEscolherModeloOSBean()!=null){
			for (EscolherModeloOSBean escolherModeloOSBean: filtro.getListaEscolherModeloOSBean()){
				mapaReportTemplateBean.put(escolherModeloOSBean.getAtividadetipo().getCdatividadetipo(), getReportTemplateService().load(escolherModeloOSBean.getReportTemplate()));
			}
		}
		
		Empresa empresa = null;
		for (int i=0; i<listaRequisicao.size(); i++){
			
			Requisicao requisicao = listaRequisicao.get(i);
			ReportTemplateBean reportTemplateBean = null;
			
			if (reportTemplateBeanFiltro!=null){
				reportTemplateBean = new ReportTemplateBean(reportTemplateBeanFiltro.getCdreporttemplate());
				reportTemplateBean.setLeiaute(reportTemplateBeanFiltro.getLeiaute());
			}else {
				reportTemplateBean = mapaReportTemplateBean.get(requisicao.getAtividadetipo().getCdatividadetipo());
			}
			
			if (reportTemplateBean==null){
				continue;
			}
			
			empresa = requisicao.getEmpresa() != null ? empresaService.loadWithProprietario(requisicao.getEmpresa()) : requisicao.getEmpresa();
			
			EmitirOSBean bean = new EmitirOSBean();
			bean.setUltimoRegistro((i+1)==listaRequisicao.size());
			bean.setCodigo(requisicao.getCdrequisicao());
			bean.setData(requisicao.getDtrequisicao()!=null ? formatadorData.format(requisicao.getDtrequisicao()) : "");
			bean.setPrevisao(requisicao.getDtprevisao()!=null ? formatadorData.format(requisicao.getDtprevisao()) : "");
			bean.setConclusao(requisicao.getDtconclusao()!=null ? formatadorData.format(requisicao.getDtconclusao()) : "");
			bean.setOrdemservico(requisicao.getOrdemservico());
			bean.setTipo(requisicao.getAtividadetipo()!=null ? requisicao.getAtividadetipo().getNome() : "");
			bean.setCliente(requisicao.getCliente()!=null ? requisicao.getCliente().getNome() : "");
			bean.setEndereco(requisicao.getEndereco()!=null ? requisicao.getEndereco().getLogradouroCompletoComCep() : "");
			bean.setTelefones_cliente(requisicao.getCliente()!=null && requisicao.getCliente().getListaTelefone()!=null && !requisicao.getCliente().getListaTelefone().isEmpty() ? CollectionsUtil.listAndConcatenate(requisicao.getCliente().getListaTelefone(), "telefone", " / ") : "");
			bean.setEmpresa(empresa!=null ? empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa() : "");
			bean.setContrato(requisicao.getContrato()!=null ? requisicao.getContrato().getDescricao() : "");
			bean.setProjeto(requisicao.getProjeto()!=null ? requisicao.getProjeto().getNome() : "");
			bean.setResponsavel(requisicao.getColaboradorresponsavel()!=null ? requisicao.getColaboradorresponsavel().getNome() : "");
			bean.setContato(requisicao.getContato()!=null ? requisicao.getContato().getNome() : "");
			bean.setSituacao(requisicao.getRequisicaoestado()!=null ? requisicao.getRequisicaoestado().getDescricao() : "");
			bean.setPrioridade(requisicao.getRequisicaoprioridade()!=null ? requisicao.getRequisicaoprioridade().getDescricao() : "");
			bean.setDescricao(requisicao.getDescricao());
			bean.setLogo_relatorio_empresa(SinedUtil.getLogoURLForReport(requisicao.getEmpresa()));
			Endereco enderecoEmpresa = null;
			if (requisicao.getEmpresa()!=null){
				enderecoEmpresa= enderecoService.carregaEnderecoEmpresa(requisicao.getEmpresa());
			}
			
			bean.setEndereco_empresa(enderecoEmpresa!=null ? enderecoEmpresa.getLogradouroCompletoComCep() : "");
			
			bean.setCnpj_empresa(empresa!=null ? empresa.getCpfCnpjProprietarioPrincipalOuEmpresa() : "");
			bean.setEmail_empresa(requisicao.getEmpresa()!=null ? requisicao.getEmpresa().getEmail() : "");
			bean.setTelefones_empresa(requisicao.getEmpresa()!=null && requisicao.getEmpresa().getListaTelefone()!=null && !requisicao.getEmpresa().getListaTelefone().isEmpty() ? CollectionsUtil.listAndConcatenate(requisicao.getEmpresa().getListaTelefone(), "telefone", " / ") : "");
			bean.setCpf_cnpj_cliente(requisicao.getCliente()!=null ? requisicao.getCliente().getCpfcnpj() : "");
			
			Telefone celularCliente= null;
			if (requisicao.getCliente()!=null)
				celularCliente = telefoneService.getTelefoneCelular(requisicao.getCliente());
			bean.setCelular_cliente(celularCliente !=null ? celularCliente.getTelefone() : "");
			
			bean.setRua_cliente(requisicao.getEndereco()!=null ? requisicao.getEndereco().getLogradouro()+" "+requisicao.getEndereco().getNumero() : "");
			bean.setComplemento_cliente(requisicao.getEndereco()!=null ? requisicao.getEndereco().getComplemento() : "");
			bean.setBairro_cliente(requisicao.getEndereco()!=null ? requisicao.getEndereco().getBairro() : "");
			bean.setCidade_cliente(requisicao.getEndereco()!=null && requisicao.getEndereco().getMunicipio()!=null? requisicao.getEndereco().getMunicipio().getNome() : "");
			bean.setUf_cliente(requisicao.getEndereco()!=null && requisicao.getEndereco().getMunicipio()!=null && requisicao.getEndereco().getMunicipio().getUf()!=null? requisicao.getEndereco().getMunicipio().getUf().getSigla() : "");
			bean.setCep_cliente(requisicao.getEndereco()!=null && requisicao.getEndereco().getCep()!=null? requisicao.getEndereco().getCep().toString() : "");
			bean.setClassificacao(requisicao.getClassificacao() != null ? requisicao.getClassificacao().getNome() : "");
			
			if (requisicao.getListaMateriaisrequisicao()!=null){
				LinkedList<EmitirOSMaterialBean> listaMaterialBean = new LinkedList<EmitirOSMaterialBean>();
				for (Materialrequisicao materialrequisicao: requisicao.getListaMateriaisrequisicao()){
					Material material = materialrequisicao.getMaterial();
					EmitirOSMaterialBean materialBean = new EmitirOSMaterialBean();
					materialBean.setItem(materialrequisicao.getItem());
					materialBean.setAutorizacao_faturamento(materialrequisicao.getAutorizacaoFaturamento());
					materialBean.setMaterial_servico(material.getNome());
					materialBean.setData_disponibilidade(materialrequisicao.getDtdisponibilizacao()!=null ? formatadorData.format(materialrequisicao.getDtdisponibilizacao()) : "");
					materialBean.setData_retirada(materialrequisicao.getDtretirada()!=null ? formatadorData.format(materialrequisicao.getDtretirada()) : "");
					materialBean.setUnidade_medida(material!=null && material.getUnidademedida()!=null ? material.getUnidademedida().getNome() : "");
					materialBean.setQuantidade(materialrequisicao.getQuantidade());
					materialBean.setValor_unitario(materialrequisicao.getValorunitario());
					materialBean.setValor_total(materialrequisicao.getValortotal());
					materialBean.setObservacao(materialrequisicao.getObservacao());
					listaMaterialBean.add(materialBean);
				}
				bean.setMateriais(listaMaterialBean);
			}
			requisicao.setListaApontamento(apontamentoService.getApontamentosByRequisicao(requisicao));
			if (requisicao.getListaApontamento()!=null){
				LinkedList<EmitirOSAtividadeBean> listaAtividade = new LinkedList<EmitirOSAtividadeBean>();
				for (Apontamento apontamento: requisicao.getListaApontamento()){
					if (apontamento.getListaApontamentoHoras()!=null){
						for (ApontamentoHoras apontamentoHoras: apontamento.getListaApontamentoHoras()){
							EmitirOSAtividadeBean atividadeBean = new EmitirOSAtividadeBean();
							atividadeBean.setData(formatadorData.format(apontamento.getDtapontamento()));
							atividadeBean.setInicio(apontamentoHoras.getHrinicio().toString());
							atividadeBean.setTermino(apontamentoHoras.getHrfim().toString());
							atividadeBean.setDescricao(apontamento.getDescricao());
							atividadeBean.setTipo_atividade(apontamento.getAtividadetipo()!=null ? apontamento.getAtividadetipo().getNome() : "");
							listaAtividade.add(atividadeBean);
						}
					}
				}
				bean.setAtividades(listaAtividade);
			}
			
			if (requisicao.getListaRequisicaohistorico()!=null){
				LinkedList<EmitirOSHistoricoBean> listaHistoricoBean = new LinkedList<EmitirOSHistoricoBean>();
				for (Requisicaohistorico requisicaohistorico: requisicao.getListaRequisicaohistorico()){
					EmitirOSHistoricoBean historicoBean = new EmitirOSHistoricoBean();
					historicoBean.setData(requisicaohistorico.getDtaltera()!=null ? formatadorData.format(requisicaohistorico.getDtaltera()) : "");
					historicoBean.setResponsavel(requisicaohistorico.getUsuarioAltera()!=null ? requisicaohistorico.getUsuarioAltera().getNome() : "");
					historicoBean.setObservacao(requisicaohistorico.getObservacao());
					historicoBean.setNomeArquivo(requisicaohistorico.getArquivo() != null ? SinedUtil.getNomeArquivoSemExtensao(requisicaohistorico.getArquivo().getNome()) : "");
					historicoBean.setImagem(requisicaohistorico.getArquivo() != null ? SinedUtil.getImgURLForGeradorReport(requisicaohistorico.getArquivo().getCdarquivo()) : "");
					
					listaHistoricoBean.add(historicoBean);
				}
				bean.setHistoricos(listaHistoricoBean);
			}
			
			if (requisicao.getListaItem()!=null){
				LinkedList<EmitirOSItemBean> listaCampoAdicionalBean = new LinkedList<EmitirOSItemBean>();
				for (Requisicaoitem requisicaoitem: requisicao.getListaItem()){
					EmitirOSItemBean campoAdicionalBean = new EmitirOSItemBean();
					campoAdicionalBean.setCampo(requisicaoitem.getAtividadetipoitem().getNome());
					campoAdicionalBean.setValor(requisicaoitem.getValor());
					listaCampoAdicionalBean.add(campoAdicionalBean);
				}
				bean.setCamposAdicionais(listaCampoAdicionalBean);
			}
			
			LinkedHashMap<String, Object> datasource = new LinkedHashMap<String, Object>();
			datasource.put("EmitirOSBean", bean);
			
			reportTemplateBean.setDatasource(datasource);
			
			listaReportTemplateBean.add(reportTemplateBean);
		}
		
		filtro.setSelectedItens(null);
		filtro.setReportTemplate(null);
		filtro.setListaEscolherModeloOSBean(null);
		
		return listaReportTemplateBean;
	}

	
	
}