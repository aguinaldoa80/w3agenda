package br.com.linkcom.sined.modulo.servicointerno.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.RequisicaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/servicointerno/relatorio/Requisicaoficha", authorizationModule=ReportAuthorizationModule.class)
public class RequisicaofichaReport extends SinedReport<RequisicaoFiltro>{

	private RequisicaoService requisicaoService;
	
	public void setRequisicaoService(RequisicaoService requisicaoService) {
		this.requisicaoService = requisicaoService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, RequisicaoFiltro filtro) throws Exception {
		String whereIn = request.getParameter("selectedItens");
		return requisicaoService.gerarRelatorioFichas(whereIn);
	}

	@Override
	public String getNomeArquivo() {
		return "fichaordemservico";
	}

	@Override
	public String getTitulo(RequisicaoFiltro filtro) {
		return "";
	}

	
	

}
