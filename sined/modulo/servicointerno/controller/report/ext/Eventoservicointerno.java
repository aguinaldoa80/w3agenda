package br.com.linkcom.sined.modulo.servicointerno.controller.report.ext;

import java.sql.Date;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Comunicado;
import br.com.linkcom.sined.geral.bean.Protocolo;
import br.com.linkcom.sined.geral.bean.Solicitacaoservico;
/**
 * Estrutura criada para ser usuada
 * no relat�rio de eventos
 * @author C�ntia Nogueira
 *
 */
public class Eventoservicointerno {
	
	protected Integer codigo;
	protected Date emissao;
	protected String evento;
	protected String descricao;
	protected String remetente;
	protected String destinatario;
	protected String situacao;
	
	public Integer getCodigo() {
		return codigo;
	}
	public Date getEmissao() {
		return emissao;
	}
	public String getEvento() {
		return evento;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getRemetente() {
		return remetente;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public String getSituacao() {
		return situacao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setEmissao(Date emissao) {
		this.emissao = emissao;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	//Classes para criar eventoservicointerno
	/**
	 * Cria um evento apartir de Um Comunicado
	 * @param comunicadp
	 * @return Eventoservicointerno
	 * @author C�ntia Nogueira
	 * @see #getDestinatario()
	 */
	public static Eventoservicointerno criaEventoComunicado(Comunicado comunicado){
		Eventoservicointerno evento = new Eventoservicointerno();
		evento.setCodigo(comunicado.getCdcomunicado());
		evento.setDescricao(comunicado.getAssunto());
		evento.setDestinatario("V�rios destinat�rios");
		evento.setEmissao(new Date(comunicado.getDtenvio().getTime()));
		evento.setEvento("Comunicado");
		evento.setRemetente(comunicado.getRemetente().getNome());
		evento.setSituacao(" ");
		return evento;		
	}
	
	/**
	 * Cria um evento a partir de uma solcita��o
	 * @param solicitacao
	 * @return
	 * @author C�ntia Nogueira
	 */
	public static Eventoservicointerno criaEventoSolicitacao(Solicitacaoservico solicitacao){
		Eventoservicointerno evento = new Eventoservicointerno();
		evento.setCodigo(solicitacao.getCdsolicitacaoservico());
		evento.setDescricao(solicitacao.getSolicitacaoservicotipo().getNome());
		evento.setDestinatario("V�rios destinat�rios");
		evento.setEmissao(new Date(solicitacao.getDtenvio().getTime()));
		evento.setEvento("Solicita��o");
		evento.setRemetente(solicitacao.getRequisitante().getNome());
		evento.setSituacao(solicitacao.getSolicitacaoservicosituacao().getNome());
		return evento;
		
	}
	
	/**
	 * Cria um evento a partir de um protocolo
	 * @param protocolo
	 * @return
	 * @author C�ntia Nogueira
	 */
	public static Eventoservicointerno criaEventoProtocolo(Protocolo protocolo){
		Eventoservicointerno evento = new Eventoservicointerno();
		evento.setCodigo(protocolo.getCdprotocolo());
		evento.setDescricao(protocolo.getAssunto());
		evento.setDestinatario(protocolo.getDestinatario().getNome());
		evento.setEmissao(new Date(protocolo.getDtenvio().getTime()));
		evento.setEvento("Protolocolo");
		evento.setRemetente(protocolo.getRemetente().getNome());
		evento.setSituacao(protocolo.getDtrecebimento()==null ? "N�o lido" :"Lido");
		return evento;		
	}
	
	
	/**
	 * Ordena a lista de eventos de acordo
	 * com a data de emiss�o
	 * @param listaEventos
	 */
	public static void ordenaListaEmisao(List<Eventoservicointerno> listaEventos){
		Collections.sort(listaEventos, new Comparator<Eventoservicointerno>(){

			public int compare(Eventoservicointerno o1, Eventoservicointerno o2) {
				if(o1.getEmissao()==null && o2.getEmissao()==null){
					return 0;
				}
				if(o1.getEmissao()==null){
					return 1;
				}
				if(o2.getEmissao()==null){
					return -1;
				}				
				return o1.getEmissao().compareTo(o2.getEmissao());
			}
			
		});
	}
	

}
