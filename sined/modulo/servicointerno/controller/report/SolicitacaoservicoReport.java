package br.com.linkcom.sined.modulo.servicointerno.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicosituacao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.SolicitacaoservicoService;
import br.com.linkcom.sined.geral.service.SolicitacaoservicotipoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.SolicitacaoservicoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Bean
@Controller(path = "/servicointerno/relatorio/Solicitacaoservico",
		      authorizationModule=ReportAuthorizationModule.class)
	
public class SolicitacaoservicoReport extends SinedReport<SolicitacaoservicoFiltro>{

	private SolicitacaoservicoService solicitacaoservicoService;
	private ColaboradorService colaboradorService;
	private SolicitacaoservicotipoService solicitacaoservicotipoService;
	private UsuarioService usuarioService;
	
	public void setSolicitacaoservicotipoService(
			SolicitacaoservicotipoService solicitacaoservicotipoService) {
		this.solicitacaoservicotipoService = solicitacaoservicotipoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setSolicitacaoservicoService(
			SolicitacaoservicoService solicitacaoservicoService) {
		this.solicitacaoservicoService = solicitacaoservicoService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, SolicitacaoservicoFiltro filtro) throws Exception {
		filtro.setOrderBy(request.getSession().getAttribute("order_Solicitacaoservico") != null ? (String) request.getSession().getAttribute("order_Solicitacaoservico") : null);
		return solicitacaoservicoService.gerarRelatorioListagem(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "solicitacaoservico";
	}
	
	@Override
	public String getTitulo(SolicitacaoservicoFiltro filtro) {
		return "SOLICITA��O DE SERVI�O";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, SolicitacaoservicoFiltro filtro) {
		
		report.addParameter("REQUISITANTE", filtro.getRequisitante() != null ? usuarioService.load(filtro.getRequisitante(), "usuario.nome") : null);
		report.addParameter("REQUISITADO", filtro.getRequisitado() != null ? colaboradorService.load(filtro.getRequisitado(), "colaborador.nome") : null);
		report.addParameter("DTENVIO", SinedUtil.getDescricaoPeriodoTimestamp(filtro.getDtenviode(), filtro.getDtenvioate()));
		report.addParameter("DTINICIO", SinedUtil.getDescricaoPeriodoTimestamp(filtro.getDtiniciode(), filtro.getDtinicioate()));
		report.addParameter("DTFIM", SinedUtil.getDescricaoPeriodoTimestamp(filtro.getDtfimde(), filtro.getDtfimate()));
		report.addParameter("PROJETO", filtro.getProjeto() != null ? projetoService.load(filtro.getProjeto(), "projeto.nome") : null);
		report.addParameter("TIPO", filtro.getSolicitacaoservicotipo() != null ? solicitacaoservicotipoService.load(filtro.getSolicitacaoservicotipo(), "solicitacaoservicotipo.nome") : null);
		report.addParameter("PRIORIDADE", filtro.getSolicitacaoservicoprioridade() != null ? filtro.getSolicitacaoservicoprioridade().getNome() : null);
		report.addParameter("SITUACOES", filtro.getListaSituacao() != null && filtro.getListaSituacao().size() > 0 ? Solicitacaoservicosituacao.listAndConcatenateDescription(filtro.getListaSituacao()) : null);
		
	}

}
