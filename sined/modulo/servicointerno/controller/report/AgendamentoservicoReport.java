package br.com.linkcom.sined.modulo.servicointerno.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.AgendamentoservicoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoFiltro;

@Controller(path = "/servicointerno/relatorio/Agendamentoservico", authorizationModule=ReportAuthorizationModule.class)
public class AgendamentoservicoReport extends ResourceSenderController<AgendamentoservicoFiltro>{

	private AgendamentoservicoService agendamentoservicoService;
	
	public void setAgendamentoservicoService(
			AgendamentoservicoService agendamentoservicoService) {
		this.agendamentoservicoService = agendamentoservicoService;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request,	AgendamentoservicoFiltro filtro) throws Exception {
		Resource resource = null;
		try {
			resource = agendamentoservicoService.gerarRelatorio(filtro);
		} catch (Exception e) {
			request.addError(e.getMessage());
		}
		return resource;
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request,	AgendamentoservicoFiltro filtro) throws Exception {
		return new ModelAndView("redirect:/servicointerno/crud/Agendamentoservico");
	}


}
