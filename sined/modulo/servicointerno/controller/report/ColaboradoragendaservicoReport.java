package br.com.linkcom.sined.modulo.servicointerno.controller.report;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.ColaboradoragendaservicoReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.SinedReport;


@Bean
@Controller(
	path = "/servicointerno/relatorio/Colaboradoragendaservico",
	authorizationModule=ReportAuthorizationModule.class
)
public class ColaboradoragendaservicoReport extends SinedReport<ColaboradoragendaservicoReportFiltro>{

	private ColaboradorService colaboradorService;
	private MaterialService materialService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, ColaboradoragendaservicoReportFiltro filtro) throws Exception {
		return colaboradorService.gerarRelatorioAgendaServico(filtro);
	}
	
	@Override
	public String getTitulo(ColaboradoragendaservicoReportFiltro filtro) {
		return "RELAT�RIO DE AGENDAMENTOS DE SERVI�OS";
	}
	
	@Override
	public String getNomeArquivo() {
		return "colaborador";
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, ColaboradoragendaservicoReportFiltro filtro) throws ResourceGenerationException {
		List<Material> materiaisServico = materialService.findByTipoServico();
		request.setAttribute("materiaisServico", materiaisServico);
		
		filtro.setDtinicio(SinedDateUtils.firstDateOfMonth());
		filtro.setDtfim(SinedDateUtils.lastDateOfMonth());
		
		return super.doFiltro(request, filtro);
	}
	
	public ModelAndView listar(WebRequestContext request, ColaboradoragendaservicoReportFiltro filtro) {
		return new ModelAndView("direct:ajax/colaboradoragendaservico_listagem", "lista", colaboradorService.buscarListagemColaboradorAgendamento(filtro, false));
	}
}
