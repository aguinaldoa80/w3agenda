package br.com.linkcom.sined.modulo.servicointerno.controller.report.bean;


public class EmitirOSMaterialBean {
	
	private String item;
	private String autorizacao_faturamento;
	private String material_servico;
	private String data_disponibilidade;
	private String data_retirada;
	private String unidade_medida;
	private Double quantidade;
	private Double valor_unitario;
	private Double valor_total;
	private String observacao;
	
	public String getItem() {
		return item;
	}
	public String getAutorizacao_faturamento() {
		return autorizacao_faturamento;
	}
	public String getMaterial_servico() {
		return material_servico;
	}
	public String getData_disponibilidade() {
		return data_disponibilidade;
	}
	public String getData_retirada() {
		return data_retirada;
	}
	public String getUnidade_medida() {
		return unidade_medida;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getValor_unitario() {
		return valor_unitario;
	}
	public Double getValor_total() {
		return valor_total;
	}
	public String getObservacao() {
		return observacao;
	}
	
	public void setItem(String item) {
		this.item = item;
	}
	public void setAutorizacao_faturamento(String autorizacaoFaturamento) {
		autorizacao_faturamento = autorizacaoFaturamento;
	}
	public void setMaterial_servico(String materialServico) {
		material_servico = materialServico;
	}
	public void setData_disponibilidade(String dataDisponibilidade) {
		data_disponibilidade = dataDisponibilidade;
	}
	public void setData_retirada(String dataRetirada) {
		data_retirada = dataRetirada;
	}
	public void setUnidade_medida(String unidadeMedida) {
		unidade_medida = unidadeMedida;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setValor_unitario(Double valorUnitario) {
		valor_unitario = valorUnitario;
	}
	public void setValor_total(Double valorTotal) {
		valor_total = valorTotal;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}	
}