package br.com.linkcom.sined.modulo.servicointerno.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Material;

public class ColaboradoragendaservicoReportBean {

	private Integer cdagendamentoservicodocumento;
	private Date dtagendamento;
	private Colaborador colaborador;
	private Cliente cliente;
	private Material servico;
	private Integer quantidade;
	private Integer cancelados;
	private Money previsto;
	private Money devido;
	private Money recebido;
	private Money saldo;
	
	public Integer getCdagendamentoservicodocumento() {
		return cdagendamentoservicodocumento;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Material getServico() {
		return servico;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public Integer getCancelados() {
		return cancelados;
	}
	public Money getPrevisto() {
		return previsto;
	}
	public Money getDevido() {
		return devido;
	}
	public Money getRecebido() {
		return recebido;
	}
	public Money getSaldo() {
		return saldo;
	}
	public Date getDtagendamento() {
		return dtagendamento;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtagendamento(Date dtagendamento) {
		this.dtagendamento = dtagendamento;
	}
	public void setCdagendamentoservicodocumento(
			Integer cdagendamentoservicodocumento) {
		this.cdagendamentoservicodocumento = cdagendamentoservicodocumento;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setServico(Material servico) {
		this.servico = servico;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public void setCancelados(Integer cancelados) {
		this.cancelados = cancelados;
	}
	public void setPrevisto(Money previsto) {
		this.previsto = previsto;
	}
	public void setDevido(Money devido) {
		this.devido = devido;
	}
	public void setRecebido(Money recebido) {
		this.recebido = recebido;
	}
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdagendamentoservicodocumento == null) ? 0
						: cdagendamentoservicodocumento.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ColaboradoragendaservicoReportBean other = (ColaboradoragendaservicoReportBean) obj;
		if (cdagendamentoservicodocumento == null) {
			if (other.cdagendamentoservicodocumento != null)
				return false;
		} else if (!cdagendamentoservicodocumento
				.equals(other.cdagendamentoservicodocumento))
			return false;
		return true;
	}
	
}
