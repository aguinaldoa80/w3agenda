package br.com.linkcom.sined.modulo.servicointerno.controller.report.bean;

import java.util.LinkedList;

public class EmitirOSBean {
	
	private Boolean ultimoRegistro = Boolean.FALSE;
	private Integer codigo;
	private String data;
	private String previsao;
	private String conclusao;
	private String ordemservico;
	private String tipo;
	private String cliente;
	private String endereco;
	private String telefones_cliente;
	private String empresa;
	private String contrato;
	private String projeto;
	private String responsavel;
	private String contato;
	private String situacao;
	private String prioridade;
	private String descricao;
	private LinkedList<EmitirOSMaterialBean> materiais = new LinkedList<EmitirOSMaterialBean>();
	private LinkedList<EmitirOSAtividadeBean> atividades = new LinkedList<EmitirOSAtividadeBean>();
	private LinkedList<EmitirOSHistoricoBean> historicos = new LinkedList<EmitirOSHistoricoBean>();
	private LinkedList<EmitirOSItemBean> camposAdicionais = new LinkedList<EmitirOSItemBean>();
	private String endereco_empresa;
	private String cnpj_empresa;
	private String email_empresa;
	private String telefones_empresa;
	private String cpf_cnpj_cliente;
	private String celular_cliente;
	private String rua_cliente;
	private String complemento_cliente;
	private String bairro_cliente;
	private String cidade_cliente;
	private String uf_cliente;
	private String cep_cliente;
	private String logo_relatorio_empresa;
	private String classificacao;
	
	public Boolean getUltimoRegistro() {
		return ultimoRegistro;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public String getData() {
		return data;
	}
	public String getPrevisao() {
		return previsao;
	}
	public String getConclusao() {
		return conclusao;
	}
	public String getOrdemservico() {
		return ordemservico;
	}
	public String getTipo() {
		return tipo;
	}
	public String getCliente() {
		return cliente;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getTelefones_cliente() {
		return telefones_cliente;
	}
	public String getEmpresa() {
		return empresa;
	}
	public String getContrato() {
		return contrato;
	}
	public String getProjeto() {
		return projeto;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public String getContato() {
		return contato;
	}
	public String getSituacao() {
		return situacao;
	}
	public String getPrioridade() {
		return prioridade;
	}
	public String getDescricao() {
		return descricao;
	}
	public LinkedList<EmitirOSMaterialBean> getMateriais() {
		return materiais;
	}
	public LinkedList<EmitirOSAtividadeBean> getAtividades() {
		return atividades;
	}
	public LinkedList<EmitirOSHistoricoBean> getHistoricos() {
		return historicos;
	}
	public LinkedList<EmitirOSItemBean> getCamposAdicionais() {
		return camposAdicionais;
	}
	public String getEndereco_empresa() {
		return endereco_empresa;
	}
	public String getCnpj_empresa() {
		return cnpj_empresa;
	}
	public String getEmail_empresa() {
		return email_empresa;
	}
	public String getTelefones_empresa() {
		return telefones_empresa;
	}
	public String getCpf_cnpj_cliente() {
		return cpf_cnpj_cliente;
	}
	public String getCelular_cliente() {
		return celular_cliente;
	}
	public String getRua_cliente() {
		return rua_cliente;
	}
	public String getComplemento_cliente() {
		return complemento_cliente;
	}
	public String getBairro_cliente() {
		return bairro_cliente;
	}
	public String getCidade_cliente() {
		return cidade_cliente;
	}
	public String getUf_cliente() {
		return uf_cliente;
	}
	public String getCep_cliente() {
		return cep_cliente;
	}
	public String getLogo_relatorio_empresa() {
		return logo_relatorio_empresa;
	}

	public void setUltimoRegistro(Boolean ultimoRegistro) {
		this.ultimoRegistro = ultimoRegistro;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setData(String data) {
		this.data = data;
	}
	public void setPrevisao(String previsao) {
		this.previsao = previsao;
	}
	public void setConclusao(String conclusao) {
		this.conclusao = conclusao;
	}
	public void setOrdemservico(String ordemservico) {
		this.ordemservico = ordemservico;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setTelefones_cliente(String telefonesCliente) {
		telefones_cliente = telefonesCliente;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setMateriais(LinkedList<EmitirOSMaterialBean> materiais) {
		this.materiais = materiais;
	}
	public void setAtividades(LinkedList<EmitirOSAtividadeBean> atividades) {
		this.atividades = atividades;
	}
	public void setHistoricos(LinkedList<EmitirOSHistoricoBean> historicos) {
		this.historicos = historicos;
	}
	public void setCamposAdicionais(LinkedList<EmitirOSItemBean> camposAdicionais) {
		this.camposAdicionais = camposAdicionais;
	}
	public void setEndereco_empresa(String enderecoEmpresa) {
		endereco_empresa = enderecoEmpresa;
	}
	public void setCnpj_empresa(String cnpjEmpresa) {
		cnpj_empresa = cnpjEmpresa;
	}
	public void setEmail_empresa(String emailEmpresa) {
		email_empresa = emailEmpresa;
	}
	public void setTelefones_empresa(String telefonesEmpresa) {
		telefones_empresa = telefonesEmpresa;
	}
	public void setCpf_cnpj_cliente(String cpfCnpjCliente) {
		cpf_cnpj_cliente = cpfCnpjCliente;
	}
	public void setCelular_cliente(String celularCliente) {
		celular_cliente = celularCliente;
	}
	public void setRua_cliente(String ruaCliente) {
		rua_cliente = ruaCliente;
	}
	public void setComplemento_cliente(String complementoCliente) {
		complemento_cliente = complementoCliente;
	}
	public void setBairro_cliente(String bairroCliente) {
		bairro_cliente = bairroCliente;
	}
	public void setCidade_cliente(String cidadeCliente) {
		cidade_cliente = cidadeCliente;
	}
	public void setUf_cliente(String uFCliente) {
		uf_cliente = uFCliente;
	}
	public void setCep_cliente(String cepCliente) {
		cep_cliente = cepCliente;
	}	
	public void setLogo_relatorio_empresa(String logoRelatorioEmpresa) {
		logo_relatorio_empresa = logoRelatorioEmpresa;
	}
	public String getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
}