package br.com.linkcom.sined.modulo.servicointerno.controller.report.bean;

public class EmitirOSHistoricoBean {
	
	private String data;
	private String responsavel;
	private String observacao;
	private String nomeArquivo;
	private String imagem; 
	
	public String getData() {
		return data;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public String getObservacao() {
		return observacao;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
}
