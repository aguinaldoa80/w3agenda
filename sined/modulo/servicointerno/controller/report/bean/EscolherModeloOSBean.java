package br.com.linkcom.sined.modulo.servicointerno.controller.report.bean;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Atividadetipo;

public class EscolherModeloOSBean {
	
	private Atividadetipo atividadetipo;
	private ReportTemplateBean reportTemplate;
	
	public EscolherModeloOSBean(){
	}
	
	public EscolherModeloOSBean(Atividadetipo atividadetipo){
		this.atividadetipo = atividadetipo;
	}
	
	public EscolherModeloOSBean(Atividadetipo atividadetipo, ReportTemplateBean reportTemplate){
		this.atividadetipo = atividadetipo;
		this.reportTemplate = reportTemplate;
	}
	
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	@Required
	public ReportTemplateBean getReportTemplate() {
		return reportTemplate;
	}
	
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setReportTemplate(ReportTemplateBean reportTemplate) {
		this.reportTemplate = reportTemplate;
	}	
}