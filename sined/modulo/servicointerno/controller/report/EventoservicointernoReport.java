package br.com.linkcom.sined.modulo.servicointerno.controller.report;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Comunicado;
import br.com.linkcom.sined.geral.bean.Protocolo;
import br.com.linkcom.sined.geral.bean.Solicitacaoservico;
import br.com.linkcom.sined.geral.service.ComunicadoService;
import br.com.linkcom.sined.geral.service.ProtocoloService;
import br.com.linkcom.sined.geral.service.SolicitacaoservicoService;
import br.com.linkcom.sined.geral.service.SolicitacaoservicotipoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.ext.Eventoservicointerno;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.EventoservicointernoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Bean
@Controller(path = "/servicointerno/relatorio/Eventoservicointerno",
		      authorizationModule=ReportAuthorizationModule.class)
	
public class EventoservicointernoReport extends SinedReport<EventoservicointernoFiltro>{

	private ComunicadoService comunicadoService;
	private ProtocoloService protocoloService;
	private SolicitacaoservicoService solicitacaoservicoService;
	private SolicitacaoservicotipoService solicitacaoservicotipoService;
	
	public void setSolicitacaoservicotipoService(
			SolicitacaoservicotipoService solicitacaoservicotipoService) {
		this.solicitacaoservicotipoService = solicitacaoservicotipoService;
	}
	public void setProtocoloService(ProtocoloService protocoloService) {
		this.protocoloService = protocoloService;
	}
	public void setComunicadoService(ComunicadoService comunicadoService) {
		this.comunicadoService = comunicadoService;
	}
	public void setSolicitacaoservicoService(
			SolicitacaoservicoService solicitacaoservicoService) {
		this.solicitacaoservicoService = solicitacaoservicoService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,EventoservicointernoFiltro filtro) throws Exception {
		List<Comunicado> listaComunicado = null;
		List<Protocolo> listaProtocolo = null;
		List<Solicitacaoservico> listaSolicitacao = null;
		List<Eventoservicointerno> listaEventos = new ArrayList<Eventoservicointerno>();
		Report report = new Report("/servicointerno/eventoservicointerno");
		
		if(filtro.getComunicado() != null && filtro.getComunicado()){
			listaComunicado = comunicadoService.findForFiltroEvento(filtro);
		}
		if(filtro.getProtocolo() != null && filtro.getProtocolo()){
			listaProtocolo = protocoloService.findForRelatorioEvento(filtro);
		}
		
		if(filtro.getSolicitacao() != null && filtro.getSolicitacao()){
			listaSolicitacao = solicitacaoservicoService.findForRelatorioEvento(filtro);
		}
		
		if(listaComunicado != null && !listaComunicado.isEmpty()){
			for(Comunicado comunicado: listaComunicado){
				listaEventos.add(Eventoservicointerno.criaEventoComunicado(comunicado));
			}
		}
		if(listaProtocolo != null && !listaProtocolo.isEmpty()){
			for(Protocolo protocolo: listaProtocolo){
				listaEventos.add(Eventoservicointerno.criaEventoProtocolo(protocolo));
			}
		}
		if(listaSolicitacao != null && !listaSolicitacao.isEmpty()){
			for(Solicitacaoservico solicitacao: listaSolicitacao){
				listaEventos.add(Eventoservicointerno.criaEventoSolicitacao(solicitacao));
			}
		}
		
		if(listaEventos != null && !listaEventos.isEmpty()){
			Eventoservicointerno.ordenaListaEmisao(listaEventos);
			report.setDataSource(listaEventos);
		}
		return report;
	}
	
	@Override
	public String getNomeArquivo() {
		return "eventos";
	}
	
	@Override
	public String getTitulo(EventoservicointernoFiltro filtro) {
		return "EVENTO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EventoservicointernoFiltro filtro) {
		
		String string = "";
		if (filtro.getComunicado() != null && filtro.getComunicado()) 
			string += "Comunicado, ";
		if (filtro.getProtocolo() != null && filtro.getProtocolo()) 
			string += "Protocolo, ";
		if (filtro.getSolicitacao() != null && filtro.getSolicitacao())
			string += "Solicita��o de servi�o, ";
		report.addParameter("TIPOEVENTO", string.equals("") ? null : string.substring(0, string.length() - 2));
		
		report.addParameter("ASSUNTO", filtro.getAssunto());
		report.addParameter("EMISSAO", SinedUtil.getDescricaoPeriodo(filtro.getInicio(), filtro.getFim()));
		
		report.addParameter("DE", (filtro.getRemetente() != null  && !filtro.getRemetente().equals(""))? filtro.getRemetente().toUpperCase() : null);
		report.addParameter("PARA", (filtro.getDestinatario() != null  && !filtro.getDestinatario().equals(""))? filtro.getDestinatario().toUpperCase() : null);
		
		report.addParameter("TIPOSOLICITACAO", filtro.getTipo() != null ? solicitacaoservicotipoService.load(filtro.getTipo(),"solicitacaoservicotipo.nome").getNome() : null);
	}


}
