package br.com.linkcom.sined.modulo.servicointerno.controller.report;

import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Documentoprocessosituacao;
import br.com.linkcom.sined.geral.service.DocumentoprocessoService;
import br.com.linkcom.sined.geral.service.DocumentoprocessotipoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.DocumentoprocessoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/servicointerno/relatorio/Documentoprocesso", authorizationModule=ReportAuthorizationModule.class)
public class DocumentoprocessoReport extends SinedReport<DocumentoprocessoFiltro>{

	private DocumentoprocessoService documentoprocessoService;
	private DocumentoprocessotipoService documentoprocessotipoService;
	
	public void setDocumentoprocessotipoService(
			DocumentoprocessotipoService documentoprocessotipoService) {
		this.documentoprocessotipoService = documentoprocessotipoService;
	}
	public void setDocumentoprocessoService(DocumentoprocessoService documentoprocessoService) {
		this.documentoprocessoService = documentoprocessoService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request,	DocumentoprocessoFiltro filtro) throws Exception {
		return documentoprocessoService.gerarRelatorio(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "documentoprocesso";
	}

	@Override
	public String getTitulo(DocumentoprocessoFiltro filtro) {		
		return "DOCUMENTO/PROCESSO";
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, DocumentoprocessoFiltro filtro) {
		report.addParameter("DOCPROCESSO", filtro.getNumDocumento() != null ? filtro.getNumDocumento().toString() :  null);
		report.addParameter("TITULOFILTRO", filtro.getTitulo() == null || filtro.getTitulo().equals("") ? null : filtro.getTitulo());
		report.addParameter("APROVADO", filtro.getAprovadopor() == null || filtro.getAprovadopor().equals("") ? null : filtro.getAprovadopor());
		report.addParameter("RESPONSAVEL",  filtro.getResponsavel() != null ? filtro.getResponsavel().getNome() : null);
		report.addParameter("TIPO", filtro.getTipo() != null ? documentoprocessotipoService.load(filtro.getTipo(),"documentoprocessotipo.nome").getNome() : null);
		report.addParameter("DISPONIBILIZADO", SinedUtil.getDescricaoPeriodo(filtro.getInicio(), filtro.getFim()));
		
		String string = "";
		List<Documentoprocessosituacao> listaSituacao = filtro.getListasituacao();
		if (listaSituacao != null) {
			for (Documentoprocessosituacao tipo : listaSituacao) {
				if (tipo.equals(Documentoprocessosituacao.DISPONIVEL)) {
					string += Documentoprocessosituacao.DISPONIVEL.getNome() + "; ";
				} else if (tipo.equals(Documentoprocessosituacao.EM_APROVACAO)) {
					string += Documentoprocessosituacao.EM_APROVACAO.getNome() + "; ";
				} else if (tipo.equals(Documentoprocessosituacao.ARQUIVO_MORTO)) {
					string += Documentoprocessosituacao.ARQUIVO_MORTO.getNome() + "; ";
				}
			}
			
			report.addParameter("SITUACOES", string.substring(0, string.length() - 2)+".");
		}
		

		
	}

	

}
