package br.com.linkcom.sined.modulo.servicointerno.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadehistorico;
import br.com.linkcom.sined.geral.bean.Solicitacaoservico;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicohistorico;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicorequisitado;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipoitem;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.SolicitacaoServicoPessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OportunidadehistoricoService;
import br.com.linkcom.sined.geral.service.SolicitacaoservicoService;
import br.com.linkcom.sined.geral.service.SolicitacaoservicohistoricoService;
import br.com.linkcom.sined.geral.service.SolicitacaoservicotipoitemService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean.IncluirDepartamentoBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.SolicitacaoservicoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/servicointerno/crud/Solicitacaoservico", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"solicitacaoservicosituacao", "solicitacaoservicotipo", "descricao", "solicitacaoservicoprioridade", "dtiniciostr", "dtfimstr", "arquivo"})
public class SolicitacaoservicoCrud extends CrudControllerSined<SolicitacaoservicoFiltro, Solicitacaoservico, Solicitacaoservico>{

	private SolicitacaoservicohistoricoService solicitacaoservicohistoricoService;
	private ColaboradorService colaboradorService;
	private SolicitacaoservicoService solicitacaoservicoService;
	private SolicitacaoservicotipoitemService solicitacaoservicotipoitemService;
	private ClienteService clienteService;
	private OportunidadeService oportunidadeService;
	private OportunidadehistoricoService oportunidadehistoricoService;
	private EnderecoService enderecoService;
	private ArquivoService arquivoService;
	
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setSolicitacaoservicotipoitemService(SolicitacaoservicotipoitemService solicitacaoservicotipoitemService) {this.solicitacaoservicotipoitemService = solicitacaoservicotipoitemService;}
	public void setSolicitacaoservicoService(SolicitacaoservicoService solicitacaoservicoService) {this.solicitacaoservicoService = solicitacaoservicoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setSolicitacaoservicohistoricoService(SolicitacaoservicohistoricoService solicitacaoservicohistoricoService) {this.solicitacaoservicohistoricoService = solicitacaoservicohistoricoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setOportunidadehistoricoService(
			OportunidadehistoricoService oportunidadehistoricoService) {this.oportunidadehistoricoService = oportunidadehistoricoService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	
	@Override
	protected void listagem(WebRequestContext request, SolicitacaoservicoFiltro filtro) throws Exception {
		request.setAttribute("listaSituacao", Solicitacaoservicosituacao.listaSituacaoFiltro());
		request.setAttribute("listaTipoPessoa", SolicitacaoServicoPessoa.listaTipoPessoa());
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, SolicitacaoservicoFiltro filtro) throws CrudException {
		if (filtro != null && filtro.getOrderBy() != null && !filtro.getOrderBy().isEmpty()){
			String isasc =  filtro.isAsc() ? " asc " : " desc ";
			request.getSession().setAttribute("order_Solicitacaoservico", filtro.getOrderBy() + isasc);
		}	
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Solicitacaoservico bean) throws Exception {
		boolean isCriar = bean.getCdsolicitacaoservico() == null;
		Oportunidade oportunidade = bean.getOportunidade();
		
		if(bean.getArquivo() != null){
			arquivoService.saveOrUpdate(bean.getArquivo()) ;
			ArquivoDAO.getInstance().saveFile(bean, "arquivo");
		}
		
		super.salvar(request, bean);
		
		if(isCriar){
			Solicitacaoservicohistorico historico = new Solicitacaoservicohistorico();
			historico.setSolicitacaoservico(bean);
			historico.setSolicitacaoservicosituacao(Solicitacaoservicosituacao.PENDENTE);
			
			if(bean.getHistorico() != null && !"".equals(bean.getHistorico())){
				historico.setObservacao("(Criado) " + bean.getHistorico());
			} else {
				historico.setObservacao("Solicita��o de servi�o criada.");
			}

			if(oportunidade != null && oportunidade.getCdoportunidade() != null){
				historico.setObservacao(historico.getObservacao() + " Oportunidade <a href=\"javascript:visualizarOportunidade("+oportunidade.getCdoportunidade()+")\">"+oportunidade.getCdoportunidade()+"</a>.");
				//Conforme solicitadodo pelo Cristiano, em 13/09/2010, a data de retorno n�o ser� modificada
				//para data atual, conforme linha de c�digo abaixo.
				//Taidson - 13/09/2010.
				//	oportunidadeService.atualizaDataRetorno(oportunidade);
			
				bean.setOportunidade(oportunidade);
				
				Oportunidadehistorico oportunidadehistorico = new Oportunidadehistorico();
				Oportunidade op = oportunidadeService.loadForEntrada(oportunidade);
				oportunidadehistorico.setOportunidadesituacao(op.getOportunidadesituacao());
				oportunidadehistorico.setCdusuarioaltera(bean.getCdusuarioaltera());
				oportunidadehistorico.setDtaltera(bean.getDtaltera());
				oportunidadehistorico.setOportunidade(op);


				oportunidadehistorico.setObservacao("Agendamento  criado <a href=\"javascript:visualizarAgendamento("+bean.getCdsolicitacaoservico()+")\">"+bean.getCdsolicitacaoservico()+"</a>.");
				oportunidadehistoricoService.insertHistoricoForAgendamento(oportunidadehistorico);
			}
			solicitacaoservicohistoricoService.saveOrUpdate(historico);
			
			solicitacaoservicoService.salvarAvisoServico("Criada", bean);
			solicitacaoservicoService.enviaEmailCriar(bean);
		} else {
			if(bean.getHistorico() != null && !"".equals(bean.getHistorico())){
				Solicitacaoservicohistorico historico = new Solicitacaoservicohistorico();
				historico.setSolicitacaoservico(bean);
				historico.setSolicitacaoservicosituacao(bean.getSolicitacaoservicosituacao());
				historico.setObservacao("(Alterada) " + bean.getHistorico());
				solicitacaoservicohistoricoService.saveOrUpdate(historico);
			}
		}
	}	
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Solicitacaoservico bean) {
		if(bean.getOportunidade() != null) {
			request.clearMessages();
			request.addMessage("Agendamento salvo com sucesso.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	public ModelAndView solicitacaoServPainelInteracao(WebRequestContext request, Solicitacaoservico form) throws CrudException{
		Usuario requisitante = new Usuario();
		requisitante = SinedUtil.getUsuarioLogado();
		
		form.setRequisitante(requisitante);
			
		if(form.getCliente() != null && form.getCliente().getCdpessoa() != null){
			Integer cdcliente = form.getCliente().getCdpessoa();
			Cliente cliente = new Cliente();
			cliente.setCdpessoa(cdcliente);
			cliente = clienteService.load(cliente);
			form.setCliente(cliente);
		}
		return doEntrada(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Solicitacaoservico form) throws Exception {
		request.setAttribute("listaColaborador", colaboradorService.findForSolicitacaoservico());
		
		List<Endereco> listaEndereco = new ArrayList<Endereco>();
		if(form.getCliente() != null && form.getCliente().getCdpessoa() != null){
			listaEndereco = enderecoService.findAtivosByCliente(form.getCliente());
		}else if(form.getFornecedor() != null && form.getFornecedor().getCdpessoa() != null){
			listaEndereco = enderecoService.findAtivosByFornecedor(form.getFornecedor());
		}
		request.setAttribute("listaEnderecoCliente", listaEndereco);
		
		if(form.getCdsolicitacaoservico() != null){
			form.setListaHistorico(solicitacaoservicohistoricoService.findBySolicitacaoservico(form));
		}
	}
	
	/**
	 * A��o em ajax para ao escolher o colaborador ele retorna 
	 * o e-mail deste colaborador.
	 *
	 * @param request
	 * @param solicitacaoservicorequisitado
	 * @author Rodrigo Freitas
	 */
	public void incluirEmail(WebRequestContext request, Solicitacaoservicorequisitado solicitacaoservicorequisitado){
		Colaborador colaborador = solicitacaoservicorequisitado.getColaborador();
		if(colaborador == null) throw new SinedException("O colaborador n�o pode ser nulo.");
		
		colaborador = colaboradorService.load(colaborador, "colaborador.email");
		if(colaborador.getEmail() != null){
			View.getCurrent().println("var email_colaborador = '" + colaborador.getEmail() + "';");
		}else{
			View.getCurrent().println("var email_colaborador = ' ';");
		}
	}
	
	/**
	 * A��o em ajax que busca os colaboradores que tem usu�rios 
	 * desbloquedos para preenchimento do detalhe na tela.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#findForSolicitacaoservico
	 * 
	 * @param request
	 * @param solicitacaoservico
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView incluirDepartamento(WebRequestContext request, Solicitacaoservico solicitacaoservico){
		if(solicitacaoservico.getDepartamento() == null) throw new SinedException("O departamento n�o pode ser nulo.");
		
		List<Colaborador> listaColaborador = colaboradorService.findForSolicitacaoservico(solicitacaoservico.getDepartamento());
		
		List<IncluirDepartamentoBean> lista = new ArrayList<IncluirDepartamentoBean>();
		IncluirDepartamentoBean sr = null;
		
		for (Colaborador colaborador : listaColaborador) {
			sr = new IncluirDepartamentoBean();
			sr.setColaborador("br.com.linkcom.sined.geral.bean.Colaborador[cdpessoa=" + colaborador.getCdpessoa() + ",nome=" + colaborador.getNome() + "]");
			sr.setEmail(colaborador.getEmail());
			lista.add(sr);
		}
		
		return new JsonModelAndView()
					.addObject("size", lista.size())
					.addObject("lista", lista);		
	}
	
	/**
	 * A��o em ajax que busca os campos adicionais do tipo de solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicotipoitemService#findByTipo
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, Solicitacaoservico bean){
		if(bean.getSolicitacaoservicotipo() == null || bean.getSolicitacaoservicotipo().getCdsolicitacaoservicotipo() == null){
			throw new SinedException("O tipo de solicita��o de servi�o n�o pode se nulo.");
		}
		
		List<Solicitacaoservicotipoitem> lista = solicitacaoservicotipoitemService.findByTipo(bean.getSolicitacaoservicotipo());
		return new JsonModelAndView()
					.addObject("size", lista.size())
					.addObject("lista", lista);	
	}
	
	/**
	 * A��o de excutar as solicita��es de servi�o.
	 *
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#haveSolicitacaoSituacaoDiferente
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#updateSituacao
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView executarSolicitacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(solicitacaoservicoService.haveSolicitacaoSituacaoDiferente(whereIn, Solicitacaoservicosituacao.PENDENTE)){
			request.addError("A(s) solicita��o(�es) de servi�o tem que estar com a situa��o 'PENDENTE'.");
			return sendRedirectToAction("listagem");
		}
		
		solicitacaoservicoService.updateSituacao(whereIn, Solicitacaoservicosituacao.EM_ANDAMENTO);
		
		String[] ids = whereIn.split(",");
		for (int i = 0; i < ids.length; i++) {
			Solicitacaoservicohistorico historico = new Solicitacaoservicohistorico();
			historico.setSolicitacaoservico(new Solicitacaoservico(Integer.parseInt(ids[i])));
			historico.setSolicitacaoservicosituacao(Solicitacaoservicosituacao.EM_ANDAMENTO);
			
			solicitacaoservicohistoricoService.saveOrUpdate(historico);
		}
		
		request.addMessage("A(s) solicita��o(�es) de servi�o foram executadas com sucesso."); 
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * A��o de concluir as solicita��es de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#haveSolicitacaoSituacaoDiferente
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#updateSituacao
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#enviaEmailConcluir
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView concluirSolicitacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(solicitacaoservicoService.haveSolicitacaoSituacaoDiferente(whereIn, Solicitacaoservicosituacao.EM_ANDAMENTO, Solicitacaoservicosituacao.PENDENTE)){
			request.addError("A(s) solicita��o(�es) de servi�o tem que estar com a situa��o 'EM ANDAMENTO' ou 'PENDENTE'.");
			return sendRedirectToAction("listagem");
		}
		
		solicitacaoservicoService.updateSituacao(whereIn, Solicitacaoservicosituacao.CONCLUIDA);
		
		String[] ids = whereIn.split(",");
		Solicitacaoservico bean;
		for (int i = 0; i < ids.length; i++) {
			bean = new Solicitacaoservico(Integer.parseInt(ids[i]));
			
			Solicitacaoservicohistorico historico = new Solicitacaoservicohistorico();
			historico.setSolicitacaoservico(bean);
			historico.setSolicitacaoservicosituacao(Solicitacaoservicosituacao.CONCLUIDA);
			
			solicitacaoservicohistoricoService.saveOrUpdate(historico);
			
			solicitacaoservicoService.salvarAvisoServico("Conclu�da", bean);
		}
		
		request.addMessage("A(s) solicita��o(�es) de servi�o foram conclu�das com sucesso."); 
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * Abre a pop-up de cancelamento de solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService.haveSolicitacaoSituacaoDiferente
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView cancelarSolicitacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(solicitacaoservicoService.haveSolicitacaoSituacaoDiferente(whereIn, Solicitacaoservicosituacao.PENDENTE, Solicitacaoservicosituacao.EM_ANDAMENTO)){
			request.addError("A(s) solicita��o(�es) de servi�o tem que estar com a situa��o 'PENDENTE' ou 'EM ANDAMENTO'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Solicitacaoservicohistorico bean = new Solicitacaoservicohistorico();
		bean.setWhereIn(whereIn);
		
		request.setAttribute("acao", "saveCancelamentoSolicitacao");
		
		return new ModelAndView("direct:/crud/popup/descreveMotivo", "bean", bean);
	}
	
	/**
	 * Salva o cancelamento da solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#updateSituacao
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#enviaEmailCancelar
	 *
	 * @param request
	 * @param solicitacaoservicohistorico
	 * @author Rodrigo Freitas
	 */
	public void saveCancelamentoSolicitacao(WebRequestContext request, Solicitacaoservicohistorico solicitacaoservicohistorico){
		solicitacaoservicoService.updateSituacao(solicitacaoservicohistorico.getWhereIn(), Solicitacaoservicosituacao.CANCELADA);
		
		String[] ids = solicitacaoservicohistorico.getWhereIn().split(",");
		Solicitacaoservico bean;
		for (int i = 0; i < ids.length; i++) {
			bean = new Solicitacaoservico(Integer.parseInt(ids[i]));
			
			Solicitacaoservicohistorico historico = new Solicitacaoservicohistorico();
			historico.setSolicitacaoservico(bean);
			historico.setSolicitacaoservicosituacao(Solicitacaoservicosituacao.CANCELADA);
			historico.setObservacao(solicitacaoservicohistorico.getObservacao());
			
			solicitacaoservicohistoricoService.saveOrUpdate(historico);
			
			solicitacaoservicoService.salvarAvisoServico("Cancelada", bean);
		}
		
		request.addMessage("A(s) solicita��o(�es) de servi�o foram canceladas com sucesso."); 
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * Abre a pop-up de estorno da solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#haveSolicitacaoSituacaoDiferente
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView estornarSolicitacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(solicitacaoservicoService.haveSolicitacaoSituacaoDiferente(whereIn, Solicitacaoservicosituacao.EM_ANDAMENTO, Solicitacaoservicosituacao.CONCLUIDA, Solicitacaoservicosituacao.CANCELADA)){
			request.addError("A(s) solicita��o(�es) de servi�o tem que estar com a situa��o 'EM ANDAMENTO', 'CONCLU�DA' ou 'CANCELADA'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Solicitacaoservicohistorico bean = new Solicitacaoservicohistorico();
		bean.setWhereIn(whereIn);
		
		request.setAttribute("acao", "saveEstornoSolicitacao");
		
		return new ModelAndView("direct:/crud/popup/descreveMotivo", "bean", bean);
	}
	
	/**
	 * Salva o estorno da solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService.updateSituacao
	 *
	 * @param request
	 * @param solicitacaoservicohistorico
	 * @author Rodrigo Freitas
	 */
	public void saveEstornoSolicitacao(WebRequestContext request, Solicitacaoservicohistorico solicitacaoservicohistorico){
		solicitacaoservicoService.updateSituacao(solicitacaoservicohistorico.getWhereIn(), Solicitacaoservicosituacao.PENDENTE);
		
		String[] ids = solicitacaoservicohistorico.getWhereIn().split(",");
		Solicitacaoservico bean;
		for (int i = 0; i < ids.length; i++) {
			bean = new Solicitacaoservico(Integer.parseInt(ids[i]));
			
			Solicitacaoservicohistorico historico = new Solicitacaoservicohistorico();
			historico.setSolicitacaoservico(bean);
			historico.setSolicitacaoservicosituacao(Solicitacaoservicosituacao.PENDENTE);
			historico.setObservacao(solicitacaoservicohistorico.getObservacao());
			
			solicitacaoservicohistoricoService.saveOrUpdate(historico);
			
		}
		
		request.addMessage("A(s) solicita��o(�es) de servi�o foram estornadas com sucesso."); 
		SinedUtil.fechaPopUp(request);
	}
	
	/**
	 * Envia ou reenvia a solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#enviaEmailCriar
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView enviarSolicitacao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String[] ids = whereIn.split(",");
		Solicitacaoservico bean;
		for (int i = 0; i < ids.length; i++) {
			bean = new Solicitacaoservico(Integer.parseInt(ids[i]));
			bean = solicitacaoservicoService.loadForEnviarSolicitacao(bean);
			solicitacaoservicoService.enviaEmailCriar(bean);
		}
		
		request.addMessage("A(s) solicita��o(�es) de servi�o foram enviada/reenviada com sucesso."); 
		return sendRedirectToAction("listagem");
	}
	
	/**
	 * A��o para cria��o de um agendamento da oportunidade.
	 *
	 * @param request
	 * @param oportunidade
	 * @return
	 * @throws CrudException
	 * @author Rodrigo Freitas
	 */
	public ModelAndView criarAgendamentoOportunidade(WebRequestContext request, Oportunidade oportunidade) throws CrudException{
		oportunidade = oportunidadeService.loadForAgendamento(oportunidade);
		if(!oportunidadeService.isUsuarioPodeEditarOportunidades(oportunidade.getCdoportunidade().toString())){
			request.addError("A��o n�o permitida. Usu�rio logado n�o � o respons�vel pela oportunidade.");
			return new ModelAndView("redirect:/crm/crud/Oportunidade");
		}
		
		Solicitacaoservico form = new Solicitacaoservico();
		if(oportunidade != null && oportunidade.getDtretorno() != null){
			form.setDtinicio(new Timestamp(oportunidade.getDtretorno().getTime()));
			form.setDtfim(new Timestamp(oportunidade.getDtretorno().getTime()));
		}
		form.setOportunidade(oportunidade);
		
		if(oportunidade.getResponsavel() != null){
			form.setRequisitante(new Usuario(oportunidade.getResponsavel().getCdpessoa()));
			Set<Solicitacaoservicorequisitado> listaRequisitado = new ListSet<Solicitacaoservicorequisitado>(Solicitacaoservicorequisitado.class);
			Solicitacaoservicorequisitado solicitacaoservicorequisitado = new Solicitacaoservicorequisitado();
			if(oportunidade != null && oportunidade.getTiporesponsavel() != null &&	oportunidade.getTiporesponsavel().equals(Vendedortipo.COLABORADOR))
				solicitacaoservicorequisitado.setColaborador(new Colaborador(oportunidade.getResponsavel().getCdpessoa()));
			solicitacaoservicorequisitado.setEmail(oportunidade.getResponsavel().getEmail());
			listaRequisitado.add(solicitacaoservicorequisitado);
			form.setListaRequisitado(listaRequisitado);
		}
		
//		if(oportunidade.getProjeto() != null){
//			form.setProjeto(oportunidade.getProjeto());
//		}
		if(form.getRequisitante() != null){
			form.setRequisitante(null);
		}
		return getCriarModelAndView(request, form);	
	}
	
	/**
	 * M�todo para carregar a lista de endereco do cliente
	 * 
	 * @param request
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView carregaEnderecosCliente(WebRequestContext request, Cliente bean){
		return new JsonModelAndView().addObject("lista", enderecoService.findAtivosByCliente(bean));
	}
	
	/**
	 * M�todo para carregar a lista de endereco do forncedor
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView carregaEnderecosFornecedor(WebRequestContext request, Fornecedor bean){
		return new JsonModelAndView().addObject("lista", enderecoService.carregarListaEndereco(bean));
	}
}
