package br.com.linkcom.sined.modulo.servicointerno.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Comunicado;
import br.com.linkcom.sined.geral.bean.Comunicadodestinatario;
import br.com.linkcom.sined.geral.bean.GrupoemailColaborador;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ComunicadoService;
import br.com.linkcom.sined.geral.service.GrupoemailColaboradorService;
import br.com.linkcom.sined.geral.service.GrupoemailService;
import br.com.linkcom.sined.geral.service.PlanejamentoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.ComunicadoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/servicointerno/crud/Comunicado", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdcomunicado", "dtenvio", "assunto", "remetente"})
public class ComunicadoCrud extends CrudControllerSined<ComunicadoFiltro, Comunicado, Comunicado>{

	private GrupoemailService grupoemailService;
	private GrupoemailColaboradorService grupoemailcolaboradorService;	
	private ComunicadoService comunicadoService;
	private PlanejamentoService planejamentoService;
	private ColaboradorService colaboradorService;
	
	public void setComunicadoService(ComunicadoService comunicadoService) {
		this.comunicadoService = comunicadoService;
	}

	public void setGrupoemailcolaboradorService(GrupoemailColaboradorService grupoemailcolaboradorService) {
		this.grupoemailcolaboradorService = grupoemailcolaboradorService;
	}

	public void setGrupoemailService(GrupoemailService grupoemailService) {
		this.grupoemailService = grupoemailService;
	}
	
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}

	@Override
	protected void listagem(WebRequestContext request, ComunicadoFiltro filtro)throws Exception {
		request.setAttribute("listaDestinatario", colaboradorService.findAllDestinatarios());
		super.listagem(request, filtro);
	}
	
	@Override
	protected Comunicado criar(WebRequestContext request, Comunicado form)throws Exception {
		request.setAttribute("listaGrupo", grupoemailService.findAtivos());
		Comunicado bean = super.criar(request, form);
		bean.setRemetente(SinedUtil.getUsuarioComoColaborador());
		return bean;
	}
	 
	@Override
	protected void validateBean(Comunicado bean, BindException errors) {
		if(bean.getCdcomunicado()==null){
			
			if(bean.getListadestinatario()==null || bean.getListadestinatario().isEmpty()){
				errors.reject("001","O Comunicado deve ter pelo menos um destinat�rio.");
			} else{
				for (Comunicadodestinatario comunicadodestinatario : bean.getListadestinatario()) {
					if(!SinedUtil.verificaEmail(comunicadodestinatario.getEmail()))
						errors.reject("002","O E-mail "+comunicadodestinatario.getEmail()+" � inv�lido.");
				}
			}
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Comunicado form) throws CrudException {
		if(form.getWhereInPlanejamento() != null && !form.getWhereInPlanejamento().equals("")){
			String whereInPlanejamento = form.getWhereInPlanejamento();
			super.doSalvar(request, form);
			return new ModelAndView("redirect:/projeto/crud/Planejamento?ACAO=autorizar&selectedItens="+whereInPlanejamento+"&comunicado="+form.getCdcomunicado());
		}
		
		return super.doSalvar(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Comunicado bean) throws Exception {				
		bean.setDtenvio(new Timestamp(System.currentTimeMillis()));
		super.salvar(request, bean);
		try{
			comunicadoService.enviaEmail(bean);
			request.addMessage("E-mail enviado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao enviar email: " + e.getMessage());
		}
	}
		
	/**
	 * A��o executada quando se clica em enviar/reenviar
	 * @param request
	 * @param form
	 * @return
	 * @throws Exception
	 * @author C�ntia
	 */
	@Action("enviar")	
	public ModelAndView enviar(WebRequestContext request, Comunicado form) throws Exception{
		String id = request.getParameter("id");
		Comunicado comunicado= new Comunicado();
		 try{
			 comunicado.setCdcomunicado(Integer.valueOf(id));
			 comunicado = comunicadoService.loadForEntrada(comunicado);
		 }catch (Exception e) {
			 comunicado = null;
		 }
			
		if(comunicado == null){
			request.addError("Comunicado com n�mero inv�lido");
			return doListagem(request, new ComunicadoFiltro());
		}

		return doSalvar(request, comunicado);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Comunicado bean)	throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	/**
	 * Verifica se existe colaborador
	 * @param lista
	 * @param colaborador
	 * @return
	 * @author C�ntia Nogueira
	 */
	private boolean existeColaborador(List<Comunicadodestinatario> lista, Colaborador colaborador){
		for(Comunicadodestinatario grupoColaborador : lista){
			if(grupoColaborador.getColaborador().getCdpessoa().equals(colaborador.getCdpessoa())){
				return true;
			}
		}		
		return false;
	}
	
	
	@Override
	protected void entrada(WebRequestContext request, Comunicado form)throws Exception {
		if("editar".equals(request.getParameter("ACAO"))){
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
		}
		request.setAttribute("listaGrupo", grupoemailService.findAtivos());
		if(form.getCdcomunicado()==null && (form.getListagrupo()==null)){
			form.setListadestinatario(null);
		}

		List<Colaborador> listaColaborador = colaboradorService.carregaColaboradorAtivo();
		request.setAttribute("listaColaborador", listaColaborador);
		
			if(form.getCdcomunicado()== null && form.getListagrupo()!=null&& !form.getListagrupo().isEmpty()){ 
				List<GrupoemailColaborador> listaEmail = grupoemailcolaboradorService.find(form.getListagrupo());
				List<Comunicadodestinatario> listaDestinatarios= new ArrayList<Comunicadodestinatario>();
				for(GrupoemailColaborador emailcolaborador: listaEmail){
					if(!existeColaborador(listaDestinatarios, emailcolaborador.getColaborador())){
						Comunicadodestinatario destinatario = new Comunicadodestinatario();
						destinatario.setColaborador(emailcolaborador.getColaborador());
						listaDestinatarios.add(destinatario);
					}
					
				}
				Collections.sort(listaDestinatarios, new Comparator<Comunicadodestinatario>(){

					public int compare(Comunicadodestinatario o1, Comunicadodestinatario o2) {
						return o1.getColaborador().getNome().compareTo(o2.getColaborador().getNome());
					}
					
				});
				form.setListadestinatario(listaDestinatarios);
		}
			
		super.entrada(request, form);
	
	}
	
	/**
	 * Autoriza planejamento enviando email para usu�rios
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public ModelAndView autorizarPlanejamento(WebRequestContext request) throws Exception{
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		List<Planejamento> lista = planejamentoService.loadWithSituacao(whereIn);
		for (Planejamento planejamento : lista) {
			if (!planejamento.getPlanejamentosituacao().equals(Planejamentosituacao.EM_ABERTO)) {
				request.addError("Para autorizar um planejamento, o mesmo deve estar na situa��o 'EM ABERTO'.");
				return new ModelAndView("redirect:/projeto/crud/Planejamento");
			}
		}
		
		Comunicado comunicado = new Comunicado("Projeto "+lista.get(0).getProjeto().getNome()+" autorizado", whereIn);
		return continueOnAction("entrada", comunicado);
	}
	
	/**
	 * M�todo via Ajax que carrega email do destinatario quando selecionado no combobox.
	 * 
	 * @param request
	 * @param colaborador
	 * @author Tom�s Rabelo
	 */
	public void atualizaEmailDestinatario(WebRequestContext request, Colaborador colaborador) {
		colaborador = colaboradorService.carregaEmailColaborador(colaborador);
		String emailDestinatario = "var emailDestinatario = ";
		if(colaborador != null){
			emailDestinatario += colaborador.getEmail() == null ? "'';" : "'"+colaborador.getEmail()+"';";
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(emailDestinatario);
	}
	
}
