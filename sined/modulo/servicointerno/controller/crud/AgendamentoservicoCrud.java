package br.com.linkcom.sined.modulo.servicointerno.controller.crud;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Agendamentoservicoacao;
import br.com.linkcom.sined.geral.bean.Agendamentoservicohistorico;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamentoServico;
import br.com.linkcom.sined.geral.service.AgendamentoservicoService;
import br.com.linkcom.sined.geral.service.AgendamentoservicoacaoService;
import br.com.linkcom.sined.geral.service.AgendamentoservicocancelaService;
import br.com.linkcom.sined.geral.service.AgendamentoservicohistoricoService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EscalahorarioService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.RequisicaohistoricoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/servicointerno/crud/Agendamentoservico", authorizationModule=CrudAuthorizationModule.class)
public class AgendamentoservicoCrud extends CrudControllerSined<AgendamentoservicoFiltro, Agendamentoservico, Agendamentoservico>{
	
	private EmpresaService empresaService;
	private MaterialtipoService materialtipoService;
	private AgendamentoservicoService agendamentoservicoService;
	private AgendamentoservicohistoricoService agendamentoservicohistoricoService;
	private MaterialService materialService;
	private EscalahorarioService escalahorarioService;
	private ColaboradorService colaboradorService;
	private AgendamentoservicocancelaService agendamentoservicocancelaService;
	private ParametrogeralService parametrogeralService;
	private RequisicaohistoricoService requisicaohistoricoService;
	private AgendamentoservicoacaoService agendamentoservicoacaoService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setAgendamentoservicocancelaService(
			AgendamentoservicocancelaService agendamentoservicocancelaService) {
		this.agendamentoservicocancelaService = agendamentoservicocancelaService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setAgendamentoservicohistoricoService(
			AgendamentoservicohistoricoService agendamentoservicohistoricoService) {
		this.agendamentoservicohistoricoService = agendamentoservicohistoricoService;
	}
	public void setAgendamentoservicoService(
			AgendamentoservicoService agendamentoservicoService) {
		this.agendamentoservicoService = agendamentoservicoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setEscalahorarioService(
			EscalahorarioService escalahorarioService) {
		this.escalahorarioService = escalahorarioService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setRequisicaohistoricoService(RequisicaohistoricoService requisicaohistoricoService) {
		this.requisicaohistoricoService = requisicaohistoricoService;
	}
	public void setAgendamentoservicoacaoService(AgendamentoservicoacaoService agendamentoservicoacaoService) {
		this.agendamentoservicoacaoService = agendamentoservicoacaoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected void listagem(WebRequestContext request, AgendamentoservicoFiltro filtro) throws Exception {
		filtro.setPaginacaoSimples(Boolean.TRUE);
		carregaCombos(request, null);
		
		List<SituacaoAgendamentoServico> listaSituacoes = new ArrayList<SituacaoAgendamentoServico>();
		listaSituacoes.add(SituacaoAgendamentoServico.AGENDADO);
		listaSituacoes.add(SituacaoAgendamentoServico.ATENCAO);
		listaSituacoes.add(SituacaoAgendamentoServico.CANCELADO);
		listaSituacoes.add(SituacaoAgendamentoServico.EM_ANDAMENTO);
		listaSituacoes.add(SituacaoAgendamentoServico.REALIZADO);
		request.setAttribute("listaSituacoes", listaSituacoes);
		
		List<String> listaTipoVisaoReport = new ArrayList<String>();
		listaTipoVisaoReport.add(AgendamentoservicoFiltro.CLIENTE);
		listaTipoVisaoReport.add(AgendamentoservicoFiltro.COLABORADOR);
		listaTipoVisaoReport.add(AgendamentoservicoFiltro.COLABORADOR_COMPLETO);
		request.setAttribute("listaTipoVisaoReport", listaTipoVisaoReport);
		
		List <Colaborador> listColaborador = new ArrayList<Colaborador>();
		listColaborador.add(SinedUtil.getUsuarioComoColaborador());

		request.setAttribute("usuarioLogado", listColaborador);
		request.setAttribute("bloqueiaColaborador", SinedUtil.getUsuarioLogado().getRestricaoclientevendedor());
		
		String paramAgendarServicoPor = parametrogeralService.getValorPorNome(Parametrogeral.AGENDAR_SERVICO_POR);
		if(!"ORDEM_SERVICO".equals(paramAgendarServicoPor)){
			filtro.setRequisicao(null);
		}
		request.setAttribute("AGENDAR_SERVICO_POR", paramAgendarServicoPor);
			
		super.listagem(request, filtro);
	}
	
	/**
	 * M�todo que carrega combos para as telas de listagem e entrada
	 * 
	 * @param request
	 * @param agendamentoservico
	 * @author Tom�s Rabelo
	 */
	private void carregaCombos(WebRequestContext request, Agendamentoservico agendamentoservico) {
		request.setAttribute("listaMaterialTipo", materialtipoService.findMaterialTipoPossuiMaterialServico(agendamentoservico != null && agendamentoservico.getMaterial() != null && agendamentoservico.getMaterial().getMaterialtipo() != null ? agendamentoservico.getMaterial().getMaterialtipo() : null));
	}
	
	@Override
	protected void entrada(WebRequestContext request, Agendamentoservico form) throws Exception {
		carregaCombos(request, form);

		if(form != null && form.getCdagendamentoservico() != null){
			String acao = request.getParameter("ACAO");
			if(acao != null && acao.equals("editar") &&form.getAux_agendamentoservico() != null && 
			   !form.getAux_agendamentoservico().getSituacao().equals(SituacaoAgendamentoServico.AGENDADO) && 
			   !form.getAux_agendamentoservico().getSituacao().equals(SituacaoAgendamentoServico.EM_ANDAMENTO))		
				throw new SinedException("N�o � possivel editar um registro com situa��o diferente de 'Agendado' ou 'Em andamento'");
			
			form.setListaagendamentosservicohistorico(agendamentoservicohistoricoService.findHistoricosDoAgendamentoPermissaoUsuario(form, SinedUtil.getUsuarioLogado()));
		} else {
			request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		}
		request.setAttribute("AGENDAR_SERVICO_POR", parametrogeralService.getValorPorNome(Parametrogeral.AGENDAR_SERVICO_POR));
	}
	
	/**
	 * M�todo que prepara para gera��o de hist�ricos para os agendamento de servi�o selecionados
	 * 
	 * @param request
	 * @param agendamentoservicohistorico
	 * @return
	 */
	@Action("prepareLancarHistorico")
	public ModelAndView prepareLancarHistorico(WebRequestContext request, Agendamentoservicohistorico agendamentoservicohistorico){
		String whereIn = request.getParameter("selectedItens");
		String acao = request.getParameter("servicoacao");
		
		if(whereIn == null || whereIn.equals(""))
			throw new SinedException("N�o foi selecionado nenhum item.");
		if(acao == null || acao.equals(""))
			throw new SinedException("Nenhuma a��o foi selecionada.");
		
		agendamentoservicohistorico.setAgendamentoservicoacao(new Agendamentoservicoacao(Integer.valueOf(acao)));
		agendamentoservicohistorico.setWhereIn(whereIn);
		agendamentoservicoService.validaAcoes(agendamentoservicohistorico, request.getBindException());
		
		if(request.getBindException().hasErrors()){
//			request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if("6".equals(acao)){
			request.setAttribute("descricao", "HIST�RICO DE AGENDAMENTO");
		}else if("5".equals(acao)){
			request.setAttribute("descricao", "FINALIZAR AGENDAMENTO");
		}else if("4".equals(acao)){
			request.setAttribute("descricao", "CANCELAR AGENDAMENTO");
		}
		
		request.setAttribute("comboIndex", acao);
		return new ModelAndView("direct:/crud/popup/lancahistoricoagendamentoservico").addObject("agendamentoservicohistorico", agendamentoservicohistorico);
	}

	/**
	 * M�todo que gera hist�ricos para os agendamento de servi�o selecionados
	 * 
	 * @param request
	 * @param agendamentoservicohistorico
	 */
	@Action("lancarHistorico")
	public void lancarHistorico (WebRequestContext request, Agendamentoservicohistorico agendamentoservicohistorico){
		if(agendamentoservicohistorico == null || agendamentoservicohistorico.getWhereIn() == null || agendamentoservicohistorico.getWhereIn().equals(""))
			throw new SinedException("Par�metros inv�lidos!");
		
		agendamentoservicoService.validaAcoes(agendamentoservicohistorico, request.getBindException());
		if(request.getBindException().hasErrors()){
//				request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		if(agendamentoservicohistorico != null && 
				agendamentoservicohistorico.getAgendamentoservicocancela() != null &&
				agendamentoservicohistorico.getAgendamentoservicocancela().getCdagendamentoservicocancela() != null){
			agendamentoservicohistorico.setAgendamentoservicocancela(agendamentoservicocancelaService.load(agendamentoservicohistorico.getAgendamentoservicocancela()));
		}
		
		List<Agendamentoservico> listaAgendamentos = agendamentoservicoService.findByWhereIn(agendamentoservicohistorico.getWhereIn());
		agendamentoservicohistoricoService.saveHistoricos(agendamentoservicohistorico, listaAgendamentos);
		
		//Criando Requisi��o Hist�rico
		if(SinedUtil.isListNotEmpty(listaAgendamentos)){
			Agendamentoservicoacao agendamentoservicoacao = agendamentoservicoacaoService.load(agendamentoservicohistorico.getAgendamentoservicoacao());
			
			String acao ="";
			if(agendamentoservicoacao.equals(Agendamentoservicoacao.REALIZADO))
				acao = "Conclu�da";
			else if(agendamentoservicoacao.equals(Agendamentoservicoacao.CANCELADO))
				acao = "Cancelada";
			else
				acao = "Hist�rico";
			
			for (Agendamentoservico agendamentoservico : listaAgendamentos) {
				Requisicaohistorico requisicaohistorico = new Requisicaohistorico();
				if(agendamentoservico.getRequisicao() != null){
					requisicaohistorico.setRequisicao(agendamentoservico.getRequisicao());
					requisicaohistorico.setObservacao("(Agenda "+ acao + ")" + (agendamentoservicohistorico.getObservacao() != null ? " - " + agendamentoservicohistorico.getObservacao() : ""));
					requisicaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
					requisicaohistorico.setPessoa(SinedUtil.getUsuarioLogado());				
				}
				requisicaohistoricoService.saveOrUpdate(requisicaohistorico);
			}
		}
		request.addMessage("Hist�rico lan�ado com sucesso.");
		SinedUtil.fechaPopUp(request);
	}
	
	@Override
	protected void validateBean(Agendamentoservico bean, BindException errors) {
		if(SinedDateUtils.dateToBeginOfDay(bean.getData()).before(SinedDateUtils.dateToBeginOfDay(new Date(System.currentTimeMillis()))))
			errors.reject("001", "A data informada n�o pode ser igual ou superior a data atual.");
		
		Material material = materialService.carregaMaterial(bean.getMaterial());
		material.setQtdeunidade(material.getQtdeunidade() == null ? 1 : material.getQtdeunidade());
		if(material.getQtdeunidade() != null && material.getQtdeunidade() == agendamentoservicoService.qtdeAgendamentoServicoMaterial(bean).intValue())
			errors.reject("002", "N�o existe disponibilidade neste dia/hor�rio.");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Escalahorario escalahorario = escalahorarioService.load(bean.getEscalahorario());
		if(bean.getCdagendamentoservico() != null){
			List<Agendamentoservico> listaAgendamentos = agendamentoservicoService.verificaAgendamentoClienteHorario(null, bean.getCliente(), escalahorario, bean.getData(), bean);
			if(listaAgendamentos != null && !listaAgendamentos.isEmpty())
				for (Agendamentoservico aux : listaAgendamentos) 
					if(aux != null && aux.getCdagendamentoservico() != null)
						errors.reject("003", "O cliente j� est� alocado na data "+dateFormat.format(aux.getData())+" hor�rio "+aux.getEscalahorario().getDescriptionProperty()+" para colaborador "+aux.getColaborador().getNome()+".");
		}
	}

	/**
	 * M�todo ajax que busca a escala de hor�rio do Colaborador de acordo com o dia
	 * 
	 * @param request
	 * @param agendamentoservico
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView comboBoxEscalaHorario(WebRequestContext request, Agendamentoservico agendamentoservico) {
		List<Escalahorario> lista = agendamentoservicoService.buscarHorariosEntrada(agendamentoservico);
		return new JsonModelAndView().addObject("listaEscalaHorario", lista);
	}

	/*
	 * M�todo ajax que sugere os colaboradores
	 * 
	 * @param request
	 * @param agendamentoservico
	 * @return
	 * @author Tom�s Rabelo
	 *
	public ModelAndView comboBoxColaborador(WebRequestContext request, Agendamentoservico agendamentoservico) {
		String data = agendamentoservico.getData() != null ? new SimpleDateFormat("dd/MM/yyyy").format(agendamentoservico.getData()) : null;
		List<Colaborador> lista = colaboradorService.sugestaoColaboradorMenosAtendimentoPeriodo(agendamentoservico.getMaterial(), data, data, agendamentoservico.getEmpresa());
		return new JsonModelAndView().addObject("listaColaboradores", lista);
	}*/
	
	public ModelAndView ajaxExisteMaterial(WebRequestContext request, Agendamentoservico agendamentoservico){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		Material m = materialService.getUniqueMaterial(agendamentoservico.getMaterial(), agendamentoservico.getMaterial().getMaterialtipo(), agendamentoservico.getEmpresa());
		jsonModelAndView.addObject("existeMaterial",  m != null);
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxExisteColaborador(WebRequestContext request, Agendamentoservico agendamentoservico){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		Colaborador c = colaboradorService.getUniqueColaborador(agendamentoservico.getColaborador(), agendamentoservico.getMaterial(), agendamentoservico.getEmpresa());
		jsonModelAndView.addObject("existeColaborador",  c != null);
		return jsonModelAndView;
	}
	
	/**
	 * M�todo que gera arquivo CSV
	 * 
	 * @param request
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public ModelAndView gerarCsv(WebRequestContext request, AgendamentoservicoFiltro filtro) throws CrudException {
		if (filtro.getCliente()==null && (filtro.getDtde()==null || filtro.getDtate()==null || SinedDateUtils.diferencaDias(filtro.getDtate(), filtro.getDtde())>365)){
			request.addError(agendamentoservicoService.MSG_DATA_GERAR_PDF_CSV);
			return doListagem(request, filtro);
		}
			
		if (agendamentoservicoService.getCountListagem(filtro)>10000){
			request.addError(agendamentoservicoService.MSG_QTDE_GERAR_PDF_CSV);
			return doListagem(request, filtro);
		}
		
		List<Agendamentoservico> lista = agendamentoservicoService.findForCsv(filtro);
		StringBuilder rel = new StringBuilder();
		
		rel.append("\"Categoria\";\"Cliente\";\"Servi�o\";\"Colaborador\";\"Data\";\"Hor�rio\";\"Situa��o\";\n");
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		for (Agendamentoservico agendamentoservico : lista) {
			rel
				.append("\"" + agendamentoservico.getCliente().getCategorias()).append("\";")
				.append("\"" + agendamentoservico.getCliente().getNome()).append("\";")
				.append("\"" + agendamentoservico.getMaterial().getNome()).append("\";")
				.append("\"" + agendamentoservico.getColaborador().getNome()).append("\";")
				.append("\"" + simpleDateFormat.format(agendamentoservico.getData())).append("\";")
				.append("\"" + agendamentoservico.getEscalahorario().getHorario()).append("\";")
				.append("\"" + agendamentoservico.getAux_agendamentoservico().getSituacao().getNome()).append("\";").append("\n");
		}
		
		Resource resource = new Resource("text/csv","agendamentoservico_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, AgendamentoservicoFiltro filtro) throws CrudException {
		if (!filtro.isNotFirstTime()) {
			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			filtro.setColaborador(colaborador);
		}
		return super.doListagem(request, filtro);
	}
	
	/**
	* M�todo que coloca a empresa/cliente/colaborador na sess�o do usu�rio para o automcomplete de requisicao (OS)
	*
	* @param request
	* @param agendamentoservico
	* @since 01/07/2015
	* @author Luiz Fernando
	*/
	public void colocarEmpresaClienteColaboradorSessao(WebRequestContext request, Agendamentoservico agendamentoservico){
		request.getSession().setAttribute("EMPRESA_FILTRO_AGENDASERVICO", agendamentoservico.getEmpresa() != null ? agendamentoservico.getEmpresa().getCdpessoa() : null);
		request.getSession().setAttribute("CLIENTE_FILTRO_AGENDASERVICO", agendamentoservico.getCliente() != null ? agendamentoservico.getCliente().getCdpessoa() : null);
		request.getSession().setAttribute("COLABORADOR_FILTRO_AGENDASERVICO", agendamentoservico.getColaborador() != null ? agendamentoservico.getColaborador().getCdpessoa() : null);
	}
}
