package br.com.linkcom.sined.modulo.servicointerno.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Documentoprocesso;
import br.com.linkcom.sined.geral.bean.Documentoprocessohistorico;
import br.com.linkcom.sined.geral.bean.Documentoprocessosituacao;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.DocumentoprocessoService;
import br.com.linkcom.sined.geral.service.DocumentoprocessohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoprocessosituacaoService;
import br.com.linkcom.sined.geral.service.DocumentoprocessotipoService;
import br.com.linkcom.sined.geral.service.PapelService;
import br.com.linkcom.sined.geral.service.UsuariopapelService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.DocumentoprocessoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/servicointerno/crud/Documentoprocesso", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cddocumentoprocesso", "titulo", "responsavel", "documentoprocessosituacao", "documentoprocessotipo", "dtdocumento", "vencimento"})
public class DocumentoprocessoCrud extends CrudControllerSined<DocumentoprocessoFiltro, Documentoprocesso, Documentoprocesso>{

	private DocumentoprocessosituacaoService documentoprocessoSituacaoService; 
	private DocumentoprocessoService documentoprocessoService;
	private ArquivoService arquivoService;
	private UsuariopapelService usuariopapelService;
	private PapelService papelService;
	private DocumentoprocessotipoService tipoService;
	private DocumentoprocessohistoricoService documentoprocessohistoricoService;
	
	public void setTipoService(DocumentoprocessotipoService tipoService) {
		this.tipoService = tipoService;
	}
	
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}

	public void setUsuariopapelService(UsuariopapelService usuariopapelService) {
		this.usuariopapelService = usuariopapelService;
	}

	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}

	public void setDocumentoprocessoService(
			DocumentoprocessoService documentoprocessoService) {
		this.documentoprocessoService = documentoprocessoService;
	}

	public void setDocumentoprocessoSituacaoService(
			DocumentoprocessosituacaoService documentoprocessoSituacaoService) {
		this.documentoprocessoSituacaoService = documentoprocessoSituacaoService;
	}
	
	public void setDocumentoprocessohistoricoService(
			DocumentoprocessohistoricoService documentoprocessohistoricoService) {
		this.documentoprocessohistoricoService = documentoprocessohistoricoService;
	}

	@Override
	protected void entrada(WebRequestContext request, Documentoprocesso form)throws Exception {
		Usuario user = (Usuario) request.getUser();
		if("editar".equals(request.getParameter("ACAO"))){
			request.setAttribute("editar", true);
		}
		if(!SinedUtil.isUsuarioLogadoAdministrador() && form.getResponsavel() != null && !form.getResponsavel().getCdpessoa().equals(user.getCdpessoa())){
			request.setAttribute("podeExcluir", false);	
			if ("editar".equals(request.getParameter("ACAO"))) {
				throw new SinedException("N�o � poss�vel editar um documento/processo de outro respons�vel.");
			}
		}else{
			request.setAttribute("podeExcluir", true);	
		}		
		request.setAttribute("listatipo", tipoService.findAtivos("documentoprocessotipo.nome"));
		if(form.getCddocumentoprocesso()==null){
			form.setResponsavel(user);
		}
		try{
			Usuario usuario = user;			
			List<Papel> listaPapel = usuariopapelService.carregaPapel(usuario);
			
			if(usuario.getCdpessoa().equals(form.getResponsavel().getCdpessoa()) ||	papelService.isAdministrador(listaPapel)){
				form.setEhEditavel(true);				
			}			
		}catch(Exception e){
			throw new SinedException ("N�o foi poss�vel obter o usu�rio logado.");
		}
				
		super.entrada(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Documentoprocesso bean)
			throws Exception {
		if(bean.getDocumentoprocessosituacao().equals(Documentoprocessosituacao.EM_APROVACAO) && 
				bean.getDtdocumento()!=null){
			bean.setDtdocumento(null);
		}
	if(bean.getCddocumentoprocesso()!=null && ((!bean.getRevisao())  || !request.getLastAction().
			equals("confirmarrevisao")) ){
			Arquivo arquivo = documentoprocessoService.loadArquivo(bean).getArquivo();	
			arquivo = arquivoService.loadWithContents(arquivo);		
			bean.setArquivo(arquivo);
			
	}
	
		super.salvar(request, bean);
	}
	
	@Override
	protected void listagem(WebRequestContext request,DocumentoprocessoFiltro filtro) throws Exception {	
		//Mudando o filtro toda vez que se clica em um bot�o
		 request.getSession().setAttribute("DocumentoprocessoCrudCONTROLLERbr.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.DocumentoprocessoFiltro",
				 				filtro);
		request.setAttribute("listaSituacao",documentoprocessoSituacaoService.findAll());		
		request.setAttribute("listatipo", tipoService.findAll("documentoprocessotipo.nome asc"));
		super.listagem(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,DocumentoprocessoFiltro filtro) throws CrudException {
		if(filtro.getListasituacao()==null){
			List<Documentoprocessosituacao> listaSituacao = new ArrayList<Documentoprocessosituacao>();
			listaSituacao.add(Documentoprocessosituacao.DISPONIVEL);
			listaSituacao.add(Documentoprocessosituacao.ARQUIVO_MORTO);
			listaSituacao.add(Documentoprocessosituacao.EM_APROVACAO);
			filtro.setListasituacao(listaSituacao);
			
		}
		return super.doListagem(request, filtro);
	}
	
	/**
	 * Faz aparecer os campos para inserir um arquivo de revis�o
	 * @param request
	 * @param form
	 * @return
	 * @throws Exception	 
	 * @see #doEntrada(WebRequestContext, Documentoprocesso)
	 * @see br.com.linkcom.sined.geral.service.DocumentoprocessoService#loadArquivo(Documentoprocesso)
	 * @author C�ntia Nogueira
	 */
	@Action("revisao")
	public ModelAndView revisao(WebRequestContext request,Documentoprocesso form) throws Exception{
		Documentoprocesso documento = documentoprocessoService.loadArquivo(form);
		form.setListahistorico(documento.getListahistorico());
		form.setArquivo(documento.getArquivo());
		form.setResponsavel(documento.getResponsavel());
		form.setRevisao(true);
		return doEntrada(request, form);
	}
	
	/**
	 * Cancela a revis�o fazendo desaparecer os campos 
	 * para dar upload no arquivo de revis�o
	 * @param request
	 * @param form
	 * @return
	 * @throws Exception
	 * @see br.com.linkcom.sined.geral.service.DocumentoprocessoService#loadArquivo(Documentoprocesso)
	 * @see #doEntrada(WebRequestContext, Documentoprocesso)
	 * @author C�ntia Nogueira
	 */
	@Action("cancelarrevisao")
	public ModelAndView cancelarRevisao(WebRequestContext request,Documentoprocesso form) throws Exception{
		Documentoprocesso documento = documentoprocessoService.loadArquivo(form);
		form.setListahistorico(documento.getListahistorico());
		form.setArquivo(documento.getArquivo());
		form.setResponsavel(documento.getResponsavel());
		form.setRevisao(false);
		form.setArquivorevisao(null);
		return doEntrada(request, form);
	}
	
	/**
	 * Cria um campo de hist�rico com o arquivo do documento
	 * e substitui o arquivo do documento pelo novo.
	 * @param request
	 * @param form
	 * @return
	 * @throws Exception
	 * @see br.com.linkcom.sined.geral.service.DocumentoprocessoService#loadArquivo(Documentoprocesso)
	 * @see #doSalvar(WebRequestContext, Documentoprocesso) 
	 * @author C�ntia Nogueira
	 */
	@Action("confirmarrevisao")
	public ModelAndView confimarRevisao(WebRequestContext request,Documentoprocesso form) throws Exception{
		
		Documentoprocesso documento = documentoprocessoService.loadArquivo(form);		
		List<Documentoprocessohistorico> listaHistorico= form.getListahistorico();
		if(listaHistorico==null){			
			listaHistorico = new ArrayList<Documentoprocessohistorico>();			
		}
		if(form.getArquivorevisao()==null || form.getArquivorevisao().getNome()==null || form.getArquivorevisao().getNome().equals("")){
			request.addError("O arquivo para a revis�o n�o pode ser vazio.");
			form.setArquivo(documento.getArquivo());
			form.setListahistorico(documento.getListahistorico());	
			form.setResponsavel(documento.getResponsavel());
			return doEntrada(request, form);
		}
		//Criando hist�rico
		Documentoprocessohistorico historico= new Documentoprocessohistorico();
		Arquivo arquivoAnterior = arquivoService.loadWithContents(documento.getArquivo());				
		Arquivo arquivo = new Arquivo();		
		arquivo.setContent(arquivoAnterior.getContent());
		arquivo.setDtmodificacao(arquivoAnterior.getDtmodificacao());
		arquivo.setNome(arquivoAnterior.getNome());
		arquivo.setTamanho(arquivoAnterior.getTamanho());
		arquivo.setTipoconteudo(arquivoAnterior.getTipoconteudo());
		historico.setArquivo(arquivo);
		historico.setObservacao("Revis�o do documento.");
		historico.setDocumentoprocessosituacao(form.getDocumentoprocessosituacao());
		form.setArquivo(form.getArquivorevisao());
		
		if(form.getDocumentoprocessosituacao().equals(Documentoprocessosituacao.DISPONIVEL)){
			form.setDtdocumento(null);
		}
		listaHistorico.add(historico);
		form.setListahistorico(listaHistorico);		
		form.setResponsavel(documento.getResponsavel());
		form.setRevisao(true);
		return doSalvar(request, form);
	}

	@Override
	protected void excluir(WebRequestContext request, Documentoprocesso bean)throws Exception {
		Usuario user = (Usuario) request.getUser();
		bean= documentoprocessoService.loadForEntrada(bean);
		if ( !SinedUtil.isUsuarioLogadoAdministrador() && 
				bean.getResponsavel() != null && !bean.getResponsavel().getCdpessoa().equals(user.getCdpessoa())) {
			throw new SinedException("N�o � poss�vel excluir um documento/processo de outro respons�vel.");
		}
		super.excluir(request, bean);
	}

	@Override
	protected Documentoprocesso criar(WebRequestContext request,Documentoprocesso form) throws Exception {
		form.setDocumentoprocessosituacao(Documentoprocessosituacao.DISPONIVEL);
		return super.criar(request, form);
	}
	
	@Action("abrirRenovarDocumentoprocesso")
	public ModelAndView abrirRenovarDocumentoprocesso(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(!documentoprocessoService.isDtVencimentoAtrasada(whereIn)){
			request.addError("Somente Documentos/processos cuja data de vencimento � inferior � atual podem ser renovados.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		Documentoprocesso documentoprocesso = new Documentoprocesso();
		documentoprocesso.setListaDocumentoprocesso(documentoprocessoService.loadForRenovar(whereIn));
		
		request.setAttribute("tamanhoLista", documentoprocesso.getListaDocumentoprocesso().size());
		request.setAttribute("acaoSalvar", "salvarDocumentoprocessoRenovado");
		return new ModelAndView("direct:/crud/popup/renovarDocumentoprocesso", "documentoprocesso", documentoprocesso);
	}
	
	@Action("salvarDocumentoprocessoRenovado")
	public void salvarDocumentoprocessoRenovado(WebRequestContext request, Documentoprocesso documentoprocesso){
		if(documentoprocesso == null || documentoprocesso.getListaDocumentoprocesso() == null || documentoprocesso.getListaDocumentoprocesso().isEmpty()){
			SinedUtil.fechaPopUp(request);
			throw new SinedException("Par�metro inv�lido!");
		}
		
		documentoprocessoService.saveDocumentoprocessoRenovado(documentoprocesso.getListaDocumentoprocesso());
		
		request.addMessage("Registros renovados com sucesso!");
		SinedUtil.fechaPopUp(request);
	}
	
	@Action("excluirArquivosHistorico")
	public ModelAndView excluirArquivosHistorico(WebRequestContext request, Documentoprocesso form) throws Exception{
		
		if(request.getParameter("cddocumentoprocesso")!=null)
			form.setCddocumentoprocesso(Integer.parseInt(request.getParameter("cddocumentoprocesso")));
		else if(request.getParameter("selectedItens")!=null)
			form.setCddocumentoprocesso(Integer.parseInt(request.getParameter("selectedItens")));			
		else{
			request.addError("Nenhum documeto/processo foi selecionado.");
			return null;
		}
		
		Documentoprocesso documento = documentoprocessoService.loadArquivo(form);			
		
		if(documento!=null && documento.getListahistorico()!=null && !documento.getListahistorico().isEmpty()){			
			if(documentoprocessohistoricoService.getMessenger(documento.getListahistorico()))
				request.addMessage("Registros excluidos com sucesso!");
			else
				request.addError("N�o existem registros de hist�rico com anexos.");			
			form.setListahistorico(documentoprocessohistoricoService.removeHistoricoArquivo(documento.getListahistorico()));
		}else{
			form.setListahistorico(documento.getListahistorico());
			request.addError("N�o existem registros de hist�rico com anexos.");
		}
		
		form.setArquivo(documento.getArquivo());
		form.setResponsavel(documento.getResponsavel());
		
		if(request.getParameter("cddocumentoprocesso")!=null)
			return continueOnAction("consultar", form);
		else if(request.getParameter("selectedItens")!=null)
			return doListagem(request, new DocumentoprocessoFiltro());
		else{
			request.addError("Nenhum documeto/processo foi selecionado.");
			return null;
		}
	}	
}