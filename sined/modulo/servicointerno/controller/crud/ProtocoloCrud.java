package br.com.linkcom.sined.modulo.servicointerno.controller.crud;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Protocolo;
import br.com.linkcom.sined.geral.bean.Protocoloprovidencia;
import br.com.linkcom.sined.geral.bean.Providencia;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.DocumentoprocessoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProtocoloService;
import br.com.linkcom.sined.geral.service.ProvidenciaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.ProtocoloFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/servicointerno/crud/Protocolo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdprotocolo", "assunto", "remetente", "dtenvio", "destinatario", "dtrecebimento"})
public class ProtocoloCrud extends CrudControllerSined<ProtocoloFiltro, Protocolo, Protocolo>{
	
	private ProvidenciaService providenciaService;
	private ProtocoloService protocoloService;
	private UsuarioService usuarioService;
	private DocumentoprocessoService documentoprocessoService;
	private EmpresaService empresaService;
	private EnvioemailService envioemailService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setDocumentoprocessoService(
			DocumentoprocessoService documentoprocessoService) {
		this.documentoprocessoService = documentoprocessoService;
	}	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setProvidenciaService(ProvidenciaService providenciaService) {
		this.providenciaService = providenciaService;
	}
	public void setProtocoloService(ProtocoloService protocoloService) {
		this.protocoloService = protocoloService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}

	@Override
	protected void entrada(WebRequestContext request, Protocolo form) throws Exception {
		if("editar".equals(request.getParameter("ACAO")))
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
		
		if(form.getCdprotocolo() == null){
			form.setRemetente((Usuario) request.getUser());
			request.setAttribute("listaProvidencia",providenciaService.findAll());	
		}
		if(form.getCdprotocolo()!=null && form.getListaprovidencia()!=null){
			 List<Providencia> lista = new ArrayList<Providencia>();
			 for(Protocoloprovidencia protProvidencia: form.getListaprovidencia()){
				 lista.add(protProvidencia.getProvidencia());
			 }
			 form.setListaProvidenciaTransient(lista);
			 request.setAttribute("listaProvidencia",lista);
		}
		
		try{
			Usuario usuario= (Usuario) request.getUser();
			
			request.setAttribute("listaDocumento", documentoprocessoService.findByResponsavel(usuario));
			
			if((form.getCdprotocolo()!=null) && (form.getDestinatario()!=null)){
				if((form.getDtrecebimento()==null)
						&&(form.getDestinatario().getCdpessoa().equals(usuario.getCdpessoa()))){
					protocoloService.recebeProtocolo(form);
					form.setDtrecebimento(new Timestamp(System.currentTimeMillis()));
					enviaEmailConfirmacaoLeitura(request, form);
				}		
			}
		}catch (Exception e) {
			request.addError("Usu�rio logado inv�lido.");
		}
		
		super.entrada(request, form);
	}
	
	/**
	 * Envia email de confirma��o de leitura
	 * @param request
	 * @param bean
	 * @author C�ntia Nogueira
	 */
	protected void enviaEmailConfirmacaoLeitura(WebRequestContext request, Protocolo bean){
		
		Empresa empresa = empresaService.loadPrincipal();
		
		String assunto = bean.getAssunto();
		String corpo= "<html><body>";
		corpo +="Caro(a) " + bean.getRemetente().getNome() +",<br><br>";
		corpo += "Foi recebida a confirma��o de leitura do protocolo de n�";
		corpo += " <a href= " +SinedUtil.getUrlWithContext()+
		  "/servicointerno/crud/Protocolo?ACAO=consultar&cdprotocolo="
		         +bean.getCdprotocolo() +">"+ bean.getCdprotocolo() + "</a>.<br><br>";
		corpo +="<b>Este e-mail foi gerado automaticamente pelo SINED, favor n�o responder.</b>"; 	
		
		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		try {
				email.setFrom(empresa.getEmail());
				email.setSubject(assunto);
				email.setTo(bean.getRemetente().getEmail());
				email.addHtmlText(corpo);
				
				email.sendMessage();
			
		} catch (Exception e) {
			e.printStackTrace();
			//request.addError("N�o foi poss�vel enviar o email para " + usuario.getEmail());
		}

		
	}
	 
	@Override
	protected void validateBean(Protocolo bean, BindException errors) {
		if(bean.getListadocumento()==null || bean.getListadocumento().isEmpty()){
			errors.reject("001", "O protocolo deve ter pelo menos um documento.");
		}
		super.validateBean(bean, errors);
	}
	@Override
	protected void salvar(WebRequestContext request, Protocolo bean)throws Exception {
		List<Protocoloprovidencia> listaProtProvidencia = new ArrayList<Protocoloprovidencia>();
		
			if(bean.getCdprotocolo()==null){
				bean.getRemetente().setEmail(usuarioService.loadWithEmail(bean.getRemetente()).getEmail());
				bean.getDestinatario().setEmail(usuarioService.loadWithEmail(bean.getDestinatario()).getEmail());
				if(bean.getListaProvidenciaTransient()!=null){
				
				for(Providencia providencia: bean.getListaProvidenciaTransient()){
					Protocoloprovidencia protProvidencia = new Protocoloprovidencia();
					protProvidencia.setProvidencia(providencia);
					listaProtProvidencia.add(protProvidencia);
				}
				bean.setListaprovidencia(listaProtProvidencia);
			}else{
				bean.setListaprovidencia(null);
			}
		}
		bean.setDtenvio(new Timestamp(System.currentTimeMillis()));
		super.salvar(request, bean);
	   enviaEmail(request, bean);
	}
	
	@Override
	protected void listagem(WebRequestContext request, ProtocoloFiltro filtro)throws Exception {
		request.setAttribute("listaProvidencia",providenciaService.findAll("providencia.nome asc"));
		super.listagem(request, filtro);
	}

	/**
	 * A��o executada quando se clica em enviar/reenviar
	 * @param request
	 * @param form
	 * @return
	 * @throws Exception
	 * @author C�ntia Nogueira
	 * @see br.com.linkcom.sined.geral.service.ProtocoloService#loadForEntrada(Protocolo)
	 */
	@Action("enviarEmail")	
	public ModelAndView enviar(WebRequestContext request, Protocolo form) throws Exception{
		String id = request.getParameter("id");
		Protocolo protocolo= new Protocolo();
		 try{
			 protocolo.setCdprotocolo(Integer.valueOf(id));
			 protocolo = protocoloService.loadForEntrada(protocolo);
		 }catch (Exception e) {
			 protocolo = null;
		 }
			
		if(protocolo==null){
			request.addError("Protocolo com n�mero inv�lido");
			return doListagem(request, new ProtocoloFiltro());
		}else{
			return doSalvar(request, protocolo);
		}
	}
	
	/**
	 * Envia os emails para os destinat�rios
	 * @param request
	 * @param bean
	 * @author C�ntia Nogueira
	 * @see #montaCorpoEmail(WebRequestContext, Protocolo)
	 */
	protected void enviaEmail(WebRequestContext request, Protocolo bean){
		String corpo= montaCorpoEmail(request, bean);
		String assunto = bean.getAssunto();
		Empresa empresa = empresaService.loadPrincipal();
	
			Usuario usuario =bean.getDestinatario();
			if(usuario!=null){
				if(usuario.getEmail()==null ||usuario.getEmail().equals("")){
					request.addError("O usu�rio "+ usuario.getNome() + " n�o tem e_mail cadastrado.");
				}else{

					EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
					
					Envioemail envioemail = envioemailService.registrarEnvio(empresa.getEmail(), assunto, corpo, 
							Envioemailtipo.ENVIO_PROTOCOLO, usuario, usuario.getEmail(), usuario.getNome(), email);
					try {
							email.setFrom(empresa.getEmail());
							email.setSubject(assunto);
							email.setTo(usuario.getEmail());
							email.addHtmlText(corpo + EmailUtil.getHtmlConfirmacaoEmail(envioemail, usuario.getEmail()));
							
							email.sendMessage();
						
					} catch (Exception e) {
						e.printStackTrace();
						//request.addError("N�o foi poss�vel enviar o email para " + usuario.getEmail());
					}
				}
					
		}
			request.addMessage("O e-mail foi enviado com sucesso.", MessageType.INFO);	
	}
	
	/**
	 * Monta a mensagem do email
	 * @param request
	 * @param bean
	 * @return
	 * @author C�ntia Nogueira
	 */
	private String montaCorpoEmail(WebRequestContext request, Protocolo bean){
		String corpo= "";
		try{
			corpo+= "<html><body>";
			
			
			corpo +="Caro(a) " + bean.getDestinatario().getNome() +",<br><br>";
			
			corpo +="H� uma nova remessa de documentos destinada a voc�. Favor, acesse o protocolo de n�";
			corpo += " <a href= " +SinedUtil.getUrlWithContext()+
			  "/servicointerno/crud/Protocolo?ACAO=consultar&cdprotocolo="
			         +bean.getCdprotocolo() +">"+ bean.getCdprotocolo() + "</a>.<br><br>";
			corpo +="<b>Este e-mail foi gerado automaticamente pelo SINED, favor n�o responder.</b>"; 		
			corpo += "</body></html>";
		}catch(Exception e){
			throw new SinedException("N�o foi poss�vel criar a mensagem do e-mail.");
		}
		return corpo;
	}
	
	@Override
	protected void excluir(WebRequestContext request, Protocolo bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	/**
	 * Carrega Lista de destinat�rios
	 * 
	 * @param request
	 * @param usuario
	 * @author Tom�s Rabelo
	 */
	public void carregaEmailRequisitado(WebRequestContext request, Usuario usuario){
		usuario = usuarioService.loadWithEmail(usuario);
		request.getServletResponse().setContentType("text/html");
		String email = "var email = ";
		email += usuario.getEmail() == null ? "'';" : "'"+usuario.getEmail()+"';";
		View.getCurrent().println(email);
	}
	
}
