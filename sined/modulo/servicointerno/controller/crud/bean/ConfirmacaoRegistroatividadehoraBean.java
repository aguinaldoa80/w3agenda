package br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Registroatividadehora;
import br.com.linkcom.sined.geral.bean.Requisicao;

public class ConfirmacaoRegistroatividadehoraBean {
	
	private Requisicao requisicao;
	private Colaborador colaborador;
	private Registroatividadehora registroatividadehora;
	private Boolean concluir;
	
	private List<ConfirmacaoRegistroatividadehoraItemBean> listaItens = new ListSet<ConfirmacaoRegistroatividadehoraItemBean>(ConfirmacaoRegistroatividadehoraItemBean.class);

	public Requisicao getRequisicao() {
		return requisicao;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Registroatividadehora getRegistroatividadehora() {
		return registroatividadehora;
	}
	public Boolean getConcluir() {
		return concluir;
	}
	public List<ConfirmacaoRegistroatividadehoraItemBean> getListaItens() {
		return listaItens;
	}
	
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setRegistroatividadehora(Registroatividadehora registroatividadehora) {
		this.registroatividadehora = registroatividadehora;
	}
	public void setConcluir(Boolean concluir) {
		this.concluir = concluir;
	}
	public void setListaItens(List<ConfirmacaoRegistroatividadehoraItemBean> listaItens) {
		this.listaItens = listaItens;
	}
}
