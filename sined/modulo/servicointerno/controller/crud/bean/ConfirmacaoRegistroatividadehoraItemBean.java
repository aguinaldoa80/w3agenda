package br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Atividadetipo;

public class ConfirmacaoRegistroatividadehoraItemBean {

	private Date data;
	private Hora inicio;
	private Hora fim;
	private String descricao;
	private Atividadetipo atividadetipo;
	
	private Integer cdapontamento;
	private Integer cdapontamentohoras;
	
	@Required
	@DisplayName("In�cio")
	public Hora getInicio() {
		return inicio;
	}
	
	@Required
	@DisplayName("Fim")
	public Hora getFim() {
		return fim;
	}
	
	@Required
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	@Required
	@DisplayName("Tipo de atividade")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	
	@Required
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	public void setInicio(Hora inicio) {
		this.inicio = inicio;
	}
	public void setFim(Hora fim) {
		this.fim = fim;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}

	public Integer getCdapontamento() {
		return cdapontamento;
	}
	public Integer getCdapontamentohoras() {
		return cdapontamentohoras;
	}

	public void setCdapontamento(Integer cdapontamento) {
		this.cdapontamento = cdapontamento;
	}
	public void setCdapontamentohoras(Integer cdapontamentohoras) {
		this.cdapontamentohoras = cdapontamentohoras;
	}
}
