package br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Etapa;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;

public class ResumoCicloBean {
	
	private Integer cdautorizacaotrabalho;
	private Etapa etapa;
	private Colaborador responsavel;
	private Requisicaoestado situacao;
	private Integer peso;
	private Double estimativa;
	private Date dtprevisao;
	
	public Integer getCdautorizacaotrabalho() {
		return cdautorizacaotrabalho;
	}
	public Etapa getEtapa() {
		return etapa;
	}
	@DisplayName("Respons�vel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	@DisplayName("Situa��o")
	public Requisicaoestado getSituacao() {
		return situacao;
	}
	public Integer getPeso() {
		return peso;
	}
	public Double getEstimativa() {
		return estimativa;
	}
	@DisplayName("Previs�o")
	public Date getDtprevisao() {
		return dtprevisao;
	}
	public void setPeso(Integer peso) {
		this.peso = peso;
	}
	public void setEstimativa(Double estimativa) {
		this.estimativa = estimativa;
	}
	public void setDtprevisao(Date dtprevisao) {
		this.dtprevisao = dtprevisao;
	}
	public void setCdautorizacaotrabalho(Integer cdautorizacaotrabalho) {
		this.cdautorizacaotrabalho = cdautorizacaotrabalho;
	}
	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setSituacao(Requisicaoestado situacao) {
		this.situacao = situacao;
	}

}
