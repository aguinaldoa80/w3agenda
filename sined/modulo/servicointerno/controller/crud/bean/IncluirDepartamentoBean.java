package br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean;

public class IncluirDepartamentoBean {
	
	private String colaborador;
	private String email;
	
	public String getColaborador() {
		return colaborador;
	}
	public String getEmail() {
		return email;
	}
	public void setColaborador(String colaborador) {
		this.colaborador = colaborador;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
