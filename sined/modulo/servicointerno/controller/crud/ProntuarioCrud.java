package br.com.linkcom.sined.modulo.servicointerno.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.BeanDescriptor;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Prontuario;
import br.com.linkcom.sined.geral.bean.Prontuariohistorico;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ProntuariohistoricoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.ProntuarioFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/servicointerno/crud/Prontuario", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"terapeuta", "paciente", "dtinicio", "dtfim"})
public class ProntuarioCrud extends CrudControllerSined<ProntuarioFiltro, Prontuario, Prontuario>{
	
	private ProntuariohistoricoService prontuariohistoricoService;
	private ColaboradorService colaboradorService;
	
	public void setProntuariohistoricoService(ProntuariohistoricoService prontuariohistoricoService) {this.prontuariohistoricoService = prontuariohistoricoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	
	@Override
	protected void entrada(WebRequestContext request, Prontuario form) throws Exception {
		if (form.getCdprontuario()==null){
			form.setTerapeuta(colaboradorService.load(new Colaborador(SinedUtil.getUsuarioLogado().getCdpessoa()),"colaborador.cdpessoa, colaborador.nome"));
			if (form.getDtinicio()==null){
				form.setDtinicio(SinedDateUtils.currentDate());
			}
			if (form.getHrinicio()==null){
				form.setHrinicio(new Hora(System.currentTimeMillis()));
			}
		}else {
			String acao = request.getParameter("ACAO");
			if (!"true".equals(request.getParameter("salvou")) && !"true".equals(request.getParameter("senhaOk")) && (acao.equals(CONSULTAR) || acao.equals(EDITAR))){
				request.setAttribute("ACAO_CONSULTAR_EDITAR", acao);
			}
			
			if (form.getListaProntuariohistorico()!=null){
				for (Prontuariohistorico prontuariohistorico: form.getListaProntuariohistorico()){
					if (prontuariohistorico.getObservacao()!=null){
						prontuariohistorico.setObservacao(prontuariohistorico.getObservacao().replace("\n", "<br/>"));
					}
				}
			}
		}
		
		super.entrada(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Prontuario bean) throws Exception {
		super.salvar(request, bean);
		prontuariohistoricoService.saveProntuariohistorico(bean);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Prontuario bean) {
		BeanDescriptor<Prontuario> beanDescriptor = Neo.getApplicationContext().getBeanDescriptor(bean);
		String idProperty = beanDescriptor.getIdPropertyName();
		Object id = beanDescriptor.getId();
		String requestQuery = request.getRequestQuery();
		String query = "?" + ACTION_PARAMETER + "=consultar&" + idProperty + "=" + id + "&salvou=true";
		
		return new ModelAndView("redirect:" + requestQuery + query);
	}
	
	public ModelAndView popupVerificaSenha(WebRequestContext request, Prontuario bean) throws Exception {
		return new ModelAndView("direct:crud/popup/verificaSenhaProntuario")
					.addObject("ACAO_CONSULTAR_EDITAR", request.getParameter("ACAO_CONSULTAR_EDITAR"))
					.addObject("cdprontuario", request.getParameter("cdprontuario"));
	}
	
	public ModelAndView ajaxVerificaSenha(WebRequestContext request, Prontuario bean) throws Exception {
		String hash = Util.crypto.makeHashMd5(bean.getSenha());
		String senha = SinedUtil.getUsuarioLogado().getSenha();
		return new JsonModelAndView().addObject("ok", hash.equals(senha));
	}

	public ModelAndView popupConsultaHistorico(WebRequestContext request, Prontuario bean) throws Exception {
		Prontuariohistorico prontuariohistorico = prontuariohistoricoService.load(new Prontuariohistorico(Integer.valueOf(request.getParameter("cdprontuariohistorico"))));
		return new ModelAndView("direct:crud/popup/consultaHistoricoProntuario").addObject("prontuariohistorico", prontuariohistorico);
	}
}