package br.com.linkcom.sined.modulo.servicointerno.controller.crud;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetipoitem;
import br.com.linkcom.sined.geral.bean.Atividadetipomodelo;
import br.com.linkcom.sined.geral.bean.Autorizacaotrabalho;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteBancoDados;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratohistorico;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Registroatividadehora;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.bean.Requisicaoprioridade;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefacolaborador;
import br.com.linkcom.sined.geral.bean.Tarefarequisicao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.service.ApontamentoService;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.AtividadetipoitemService;
import br.com.linkcom.sined.geral.service.AtividadetipomodeloService;
import br.com.linkcom.sined.geral.service.AutorizacaotrabalhoService;
import br.com.linkcom.sined.geral.service.ClienteBancoDadosService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.ContratohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialrequisicaoService;
import br.com.linkcom.sined.geral.service.MeiocontatoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendahistoricoService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RegistroatividadehoraService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.RequisicaoestadoService;
import br.com.linkcom.sined.geral.service.RequisicaohistoricoService;
import br.com.linkcom.sined.geral.service.RequisicaoprioridadeService;
import br.com.linkcom.sined.geral.service.TarefaService;
import br.com.linkcom.sined.geral.service.TarefarequisicaoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendahistoricoService;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean.ConfirmacaoRegistroatividadehoraBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean.ConfirmacaoRegistroatividadehoraItemBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.bean.ResumoCicloBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.RequisicaoFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EscolherModeloOSBean;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.ValidadorRequisicao;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/servicointerno/crud/Requisicao", authorizationModule=CrudAuthorizationModule.class)
public class RequisicaoCrud extends CrudControllerSined<RequisicaoFiltro, Requisicao, Requisicao> {
	
	private RequisicaoService requisicaoService;
	private ColaboradorService colaboradorService;
	private MaterialService materialService;
	private AtividadetipoitemService atividadetipoitemService;
	private ParametrogeralService parametrogeralService;
	private EnderecoService enderecoService;
	private ContratoService contratoService;
	private ContatoService contatoService;
	private TarefarequisicaoService tarefarequisicaoService;
	private ProjetoService projetoService;
	private RequisicaohistoricoService requisicaohistoricoService;
	private AutorizacaotrabalhoService autorizacaotrabalhoService;
	private VendaService vendaService;
	private ClienteService clienteService;
	private ApontamentoService apontamentoService;
	private AtividadetipoService atividadetipoService;
	private LocalarmazenagemService localarmazenagemService;
	private EmpresaService empresaService;	
	private VendahistoricoService vendahistoricoService;
	private UnidademedidaService unidademedidaService;
	private DocumentoService documentoService;
	private AtividadetipomodeloService atividadetipomodeloService;
	private ContareceberService contareceberService;
	private NotaDocumentoService notaDocumentoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private PedidovendaService pedidovendaService;
	private UsuarioService usuarioService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private ClienteBancoDadosService clienteBancoDadosService;
	private TarefaService tarefaService;
	private MeiocontatoService meiocontatoService;
	private MaterialrequisicaoService materialrequisicaoService;
	private RegistroatividadehoraService registroatividadehoraService;
	private PedidovendatipoService pedidovendatipoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private ContratohistoricoService contratohistoricoService;
	private InventarioService inventarioService;
	
	public void setMaterialrequisicaoService(
			MaterialrequisicaoService materialrequisicaoService) {
		this.materialrequisicaoService = materialrequisicaoService;
	}
	public void setClienteBancoDadosService(ClienteBancoDadosService clienteBancoDadosService) {
		this.clienteBancoDadosService = clienteBancoDadosService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setRequisicaohistoricoService(
			RequisicaohistoricoService requisicaohistoricoService) {
		this.requisicaohistoricoService = requisicaohistoricoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setRequisicaoService(RequisicaoService requisicaoService) {
		this.requisicaoService = requisicaoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setAtividadetipoitemService(AtividadetipoitemService atividadetipoitemService) {
		this.atividadetipoitemService = atividadetipoitemService;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	
	public void setTarefarequisicaoService(TarefarequisicaoService tarefarequisicaoService) {
		this.tarefarequisicaoService = tarefarequisicaoService;
	}	
	
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	public void setAutorizacaotrabalhoService(
			AutorizacaotrabalhoService autorizacaotrabalhoService) {
		this.autorizacaotrabalhoService = autorizacaotrabalhoService;
	}
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	public void setApontamentoService(ApontamentoService apontamentoService) {
		this.apontamentoService = apontamentoService;
	}
		
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {
		this.vendahistoricoService = vendahistoricoService;
	}
	
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setAtividadetipomodeloService(AtividadetipomodeloService atividadetipomodeloService) {
		this.atividadetipomodeloService = atividadetipomodeloService;
	}
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}
	public void setNotaDocumentoService(
			NotaDocumentoService notaDocumentoService) {
		this.notaDocumentoService = notaDocumentoService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setMeiocontatoService(MeiocontatoService meiocontatoService) {
		this.meiocontatoService = meiocontatoService;
	}
	public void setRegistroatividadehoraService(RegistroatividadehoraService registroatividadehoraService) {
		this.registroatividadehoraService = registroatividadehoraService;
	}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {
		this.pedidovendatipoService = pedidovendatipoService;
	}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setContratohistoricoService(
			ContratohistoricoService contratohistoricoService) {
		this.contratohistoricoService = contratohistoricoService;
	}
	
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@Override
	public boolean isSalvarFiltro() {
		return true;
	}
	
	/**
	 * Action feita para a atualiza��o de horas gerais das requisi��es
	 *
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/01/2014
	 */
	public ModelAndView atualizaHorasRequisicao(WebRequestContext request){
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			throw new SinedException("A��o n�o permitida");
		}
		
		List<Requisicao> listaAllFromAtualizacao = requisicaoService.findAll();
		for (Requisicao requisicao : listaAllFromAtualizacao) {
			requisicao.setListaApontamento(apontamentoService.getApontamentosByRequisicao(requisicao));
			for(Apontamento ap : requisicao.getListaApontamento()){
				if (ap.getListaApontamentoHoras()!=null && !ap.getListaApontamentoHoras().isEmpty()){
					ap.setCdapontamentohoras(ap.getListaApontamentoHoras().get(0).getCdapontamentohoras());
					ap.setHrinicioApontamento(ap.getListaApontamentoHoras().get(0).getHrinicio());
					ap.setHrfimApontamento(ap.getListaApontamentoHoras().get(0).getHrfim());
				}
			}
			
			apontamentoService.getTotalHorasForRequisicao(requisicao);
			requisicaoService.updateQtde(requisicao);
		}
		
		return sendRedirectToAction("listagem");
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Requisicao form) throws CrudException {
		String acao = request.getParameter("ACAO");
		Requisicao requisicaoFound = requisicaoService.carregaRequisicao(form);
		
		if (acao != null && "editar".equals(acao) && requisicaoFound.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CONCLUIDA) && 
				parametrogeralService.getBoolean("OS_CONCLUIDA_NAO_PERMITE_ALTERACAO")) {
			request.addError("Ordem de Servi�o com situa��o 'Conclu�da' n�o poder� ser alterada.");
			SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
			return null;
		}
		
		return super.doEditar(request, form);
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Requisicao form) throws CrudException {
		String cdrequisicao = request.getParameter("cdrequisicao");
		String stCopiar = request.getParameter("copiar");
		if (StringUtils.isNotEmpty(cdrequisicao) && StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
			form.setCdrequisicao(Integer.parseInt(cdrequisicao));
			form = requisicaoService.loadForEntrada(form);
			form.setRequisicaorelacionada(new Requisicao(form.getCdrequisicao()));
			form.setCdrequisicao(null);
			form.setDtconclusao(null);
			requisicaoService.limparReferenciasForCopiar(form);
		}
		return super.doEntrada(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, RequisicaoFiltro filtro)throws Exception {
		List<Requisicaoestado> listaRequisicaoEstado = RequisicaoestadoService.getInstance().findForCombo();
		request.setAttribute("listaRequisicaoEstado", listaRequisicaoEstado);
		List<Requisicaoprioridade> listaRequisicaoprioridade = RequisicaoprioridadeService.getInstance().findForCombo();
		request.setAttribute("atividadeTipo", atividadetipoService.findForServicos());
		request.setAttribute("listaRequisicaoprioridade", listaRequisicaoprioridade);
		request.setAttribute("hoje", System.currentTimeMillis());
		request.setAttribute("listaMeioContato", meiocontatoService.findAtivos());
		if(filtro.getContrato() != null && filtro.getContrato().getCdcontrato() != null)
			request.setAttribute("contratofiltro", "br.com.linkcom.sined.geral.bean.Contrato[cdcontrato=" +filtro.getContrato().getCdcontrato() + "]");		
		if(filtro.getTipo() != null && filtro.getTipo().getCdatividadetipo() != null){
			request.setAttribute("listaATI", atividadetipoitemService.findByTipo(filtro.getTipo()));
		}
		
		String param = parametrogeralService.getValorPorNome(Parametrogeral.W3MPSBR_INTEGRACAO);
		request.setAttribute("isW3mpsbr", param != null && param.toUpperCase().equals("TRUE"));
		request.setAttribute("CSV_OS_FATURAMENTO", parametrogeralService.getBoolean("CSV_OS_FATURAMENTO"));
		
	}
	
	/**
	 * M�todo respons�vel por selecionar como default o mesmo cliente e mesmo contrato selecionados
	 * em Painel de intera��o.
	 * @param request
	 * @param form
	 * @return
	 * @throws CrudException
	 */
	public ModelAndView requisicaoPainelInteracao(WebRequestContext request, Requisicao form) throws CrudException{
		
		if(form.getCliente() != null && form.getCliente().getCdpessoa() != null){
			form.setCliente(ClienteService.getInstance().load(form.getCliente()));
		}
		
		form.setColaboradorresponsavel(SinedUtil.getUsuarioComoColaborador());
		Colaborador colaborador = new Colaborador();
		Usuario usuario = new Usuario();
		usuario = SinedUtil.getUsuarioLogado();
		colaborador = colaboradorService.carregaColaboradorresponsavel(usuario.getCdpessoa());
		
		form.setColaboradorresponsavel(colaborador);

		return getCriarModelAndView(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Requisicao form) throws Exception {
		if(form.getListaMateriaisrequisicao() != null){
			Collections.sort(form.getListaMateriaisrequisicao(), new Comparator<Materialrequisicao>(){
				public int compare(Materialrequisicao o1, Materialrequisicao o2) {
					int compareWithAutorizacao = materialrequisicaoService.compareWithAutorizacao(o1, o2);
					if(compareWithAutorizacao == 0){
						int compareWithItem = materialrequisicaoService.compareWithItem(o1, o2);
						if(compareWithItem == 0){
							return materialrequisicaoService.compareWithNomeMaterial(o1, o2);
						} else return compareWithItem;
					} else return compareWithAutorizacao;
				}
			});
		}
		
		Boolean isOsfaturadabycontrato = false;
		List<Projeto> listaProjeto = new ArrayList<Projeto>();
		if (form.getCdrequisicao()==null) {
			form.setRequisicaoestado(new Requisicaoestado(Requisicaoestado.EMESPERA));
			form.setRequisicaoprioridade(new Requisicaoprioridade(Requisicaoprioridade.MEDIA));
			
			//String paramTarefa = request.getParameter("cdtarefa");
			//Integer cdtarefa = paramTarefa != null && !"".equals(paramTarefa) && !"<null>".equals(paramTarefa) ? Integer.parseInt(paramTarefa) : null;
			String paramTarefa = request.getParameter("cdstarefa");
			String cdstarefa = paramTarefa != null && !"".equals(paramTarefa) && !"<null>".equals(paramTarefa) ? paramTarefa : null;
			
			Meiocontato meiocontato = meiocontatoService.findMeioContatoPadrao();
			if(meiocontato != null){
				form.setMeiocontato(meiocontato);
			}
			
			if(cdstarefa != null){
				//form.setCdtarefa(cdtarefa);
				form.setCdstarefa(cdstarefa);
				List<Integer> listaCdtarefa = SinedUtil.getListaByIds(cdstarefa);
				for (Integer cdtarefa: listaCdtarefa){
					List<Projeto> listaProjetoAux = projetoService.findForGerarOrdemServico(cdtarefa.toString());
					for (Projeto projeto1: listaProjetoAux){
						boolean add = true;
						for (Projeto projeto2: listaProjeto){
							if (projeto1.getCdprojeto().equals(projeto2.getCdprojeto())){
								add = false;
								break;
							}
						}
						if (add){
							listaProjeto.add(projeto1);
						}
					}
				}
				//Se selecionar apenas uma tarefa e tiver projeto relacionado ent�o selecionar o projeto e o cliente
				//Se selecionar mais de uma tarefa e tiver apenas um projeto relacionado em comum nestas tarefas tamb�m selecionar o projeto e o cliente
				if(listaCdtarefa.size()==1 && !listaProjeto.isEmpty() || listaCdtarefa.size()>1 && listaProjeto.size()==1){
					form.setCliente(listaProjeto.get(0).getCliente());
					form.setProjeto(listaProjeto.get(0));
				}
//				listaProjeto = projetoService.findForGerarOrdemServico(cdtarefa.toString());
//				if(listaProjeto != null && listaProjeto.size() > 0){
//					form.setCliente(listaProjeto.get(0).getCliente());
//					form.setProjeto(listaProjeto.get(0));
//				}
				List<Tarefa> listaTarefa = tarefaService.findForGerarOrdemServico(cdstarefa);
				if (SinedUtil.isListNotEmpty(listaTarefa)) {
					StringBuilder descricao = new StringBuilder();
					for (Tarefa tarefa : listaTarefa) {
						descricao.append("Tarefa\n");
						descricao.append(tarefa.getDescricao()).append("\n");
						descricao.append("Respons�veis\n");
						if (SinedUtil.isListNotEmpty(tarefa.getListaTarefacolaborador())) {
							for (Tarefacolaborador tarefacolaborador : tarefa.getListaTarefacolaborador()) {
								if (tarefacolaborador.getColaborador() != null && StringUtils.isNotBlank(tarefacolaborador.getColaborador().getNome())) {
									descricao.append(tarefacolaborador.getColaborador().getNome()).append("\n");
								}
							}
						}
						descricao.append("\n");
					}
					if (descricao.length() > 0) {
						form.setDescricao(descricao.toString());
					}
				}
			}
			
			if (request.getParameter("cdcontratotrans") != null){
				Integer cdcontratotrans = Integer.parseInt(request.getParameter("cdcontratotrans"));
				Contrato contrato = new Contrato(cdcontratotrans);
				requisicaoService.preencheRequisicaoByContrato(form, contrato);
				form.setContrato(contrato);
				form.setCdcontratotrans(cdcontratotrans);
			}
			
			if (request.getParameter("cdvenda") != null){
				Venda venda = vendaService.findVendaByCdVenda(Integer.parseInt(request.getParameter("cdvenda")));
				
				form.setCliente(venda.getCliente());
				
				List<Materialrequisicao> listaMaterial = new ArrayList<Materialrequisicao>();
				Materialrequisicao rm;
				Money valorDesconto;
				Money valorDescontoItem;
				Double multiplicador;
				for (Vendamaterial vm : venda.getListavendamaterial()) {
					rm = new Materialrequisicao();
					rm.setQuantidade(vm.getQuantidade());
					rm.setValorunitario(vm.getPreco());
					rm.setObservacao(vm.getObservacao());
					if(vm.getUnidademedida() != null && vm.getMaterial() != null && vm.getMaterial().getUnidademedida() != null &&
							!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
						Double qtdeconvertida = unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia());
						if(qtdeconvertida != null && qtdeconvertida > 0)
							rm.setQuantidade(qtdeconvertida);
						if(vm.getPreco() != null){
							rm.setValorunitario(vm.getFatorconversaoQtdereferencia() / vm.getPreco());
						}
					}
					
					multiplicador = vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d; 
					valorDescontoItem = vm.getDesconto() != null ? vm.getDesconto() :  new Money();
					valorDesconto = notafiscalprodutoService.getValorDescontoVenda(venda.getListavendamaterial(), vm, venda.getDesconto(), venda.getValorusadovalecompra(), true);
					valorDescontoItem = valorDescontoItem.divide(new Money(vm.getQuantidade()*multiplicador));
					valorDesconto = valorDesconto.add(valorDescontoItem);
					
					if(vm.getQuantidade() != null && vm.getFatorconversaoQtdereferencia() > 0d){
						valorDesconto = valorDesconto.multiply(new Money(vm.getFatorconversaoQtdereferencia()));
					}
					if(valorDesconto != null && valorDesconto.getValue().doubleValue() > 0 && vm.getPreco() != null){
						rm.setValorunitario(rm.getValorunitario() - valorDesconto.getValue().doubleValue());
					}
					
					rm.setMaterial(vm.getMaterial());
					if(rm.getQuantidade() != null && rm.getValorunitario() != null){
						rm.setValortotal(rm.getQuantidade() * rm.getValorunitario());
					}
					listaMaterial.add(rm);
				}
				
				form.setVenda(venda);
				form.setEmpresa(venda.getEmpresa());
				form.setProjeto(venda.getProjeto());
				form.setListaMateriaisrequisicao(listaMaterial);
				form.setColaboradorresponsavel(venda.getColaborador());
			}
			
			if (request.getParameter("cdpedidovenda") != null){
				Pedidovenda pedidovenda = pedidovendaService.findPedidovendaByCdpedidovenda(Integer.parseInt(request.getParameter("cdpedidovenda")));
				
				form.setCliente(pedidovenda.getCliente());
				
				List<Materialrequisicao> listaMaterial = new ArrayList<Materialrequisicao>();
				Materialrequisicao rm;
				Money valorDesconto;
				Money valorDescontoItem;
				Double multiplicador;
				for (Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()) {
					rm = new Materialrequisicao();
					rm.setQuantidade(pvm.getQuantidade());
					rm.setValorunitario(pvm.getPreco());
					rm.setObservacao(pvm.getObservacao());
					if(pvm.getUnidademedida() != null && pvm.getMaterial() != null && pvm.getMaterial().getUnidademedida() != null &&
							!pvm.getUnidademedida().equals(pvm.getMaterial().getUnidademedida())){
						Double qtdeconvertida = unidademedidaService.converteQtdeUnidademedida(pvm.getMaterial().getUnidademedida(), pvm.getQuantidade(), pvm.getUnidademedida(), pvm.getMaterial(), pvm.getFatorconversao(), pvm.getQtdereferencia());
						if(qtdeconvertida != null && qtdeconvertida > 0)
							rm.setQuantidade(qtdeconvertida);
						if(pvm.getPreco() != null){
							rm.setValorunitario(pvm.getFatorconversaoQtdereferencia() / pvm.getPreco());
						}
					}
					
					multiplicador = pvm.getMultiplicador() != null && pvm.getMultiplicador() > 0 ? pvm.getMultiplicador() : 1d; 
					valorDescontoItem = pvm.getDesconto() != null ? pvm.getDesconto() :  new Money();
					valorDesconto = notafiscalprodutoService.getValorDescontoVenda(pedidovenda.getListaPedidovendamaterial(), pvm, pedidovenda.getDesconto(), pedidovenda.getValorusadovalecompra(), true);
					valorDescontoItem = valorDescontoItem.divide(new Money(pvm.getQuantidade()*multiplicador));
					valorDesconto = valorDesconto.add(valorDescontoItem);
					
					if(pvm.getQuantidade() != null && pvm.getFatorconversaoQtdereferencia() > 0d){
						valorDesconto = valorDesconto.multiply(new Money(pvm.getFatorconversaoQtdereferencia()));
					}
					if(valorDesconto != null && valorDesconto.getValue().doubleValue() > 0 && pvm.getPreco() != null){
						rm.setValorunitario(rm.getValorunitario() - valorDesconto.getValue().doubleValue());
					}
					
					rm.setMaterial(pvm.getMaterial());
					if(rm.getQuantidade() != null && rm.getValorunitario() != null){
						rm.setValortotal(rm.getQuantidade() * rm.getValorunitario());
					}
					listaMaterial.add(rm);
				}
				
				form.setPedidovenda(pedidovenda);
				form.setEmpresa(pedidovenda.getEmpresa());
				form.setProjeto(pedidovenda.getProjeto());
				form.setListaMateriaisrequisicao(listaMaterial);
				form.setColaboradorresponsavel(pedidovenda.getColaborador());
			}

			Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
			if(colaborador != null && form.getColaboradorresponsavel()==null)
				form.setColaboradorresponsavel(colaborador);
			
			form.setAtividadetipo(atividadetipoService.findAtividadePadrao());
			
			//request.setAttribute("podeEditarAtividade", (colaborador != null));
		}else {
			isOsfaturadabycontrato = requisicaoService.isFaturadaByContrato(form);
			if(EDITAR.equals(request.getParameter("ACAO")) && (form.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.VISTO) || isOsfaturadabycontrato)){
				throw new SinedException("Requisi��o n�o pode ser alterada.");
			}
		
			String param = parametrogeralService.getValorPorNome(Parametrogeral.W3MPSBR_INTEGRACAO);
			Boolean isW3mpsbr = param != null && param.toUpperCase().equals("TRUE");
			request.setAttribute("isW3mpsbr", isW3mpsbr);
			if (isW3mpsbr){
				List<Autorizacaotrabalho> listaAutorizacaotrabalho = autorizacaotrabalhoService.findByRequisicao(form);
				ResumoCicloBean resumo;
				List<ResumoCicloBean> listaResumoCiclo = new ListSet<ResumoCicloBean>(ResumoCicloBean.class);
				
				for (Autorizacaotrabalho at : listaAutorizacaotrabalho) {
					resumo = new ResumoCicloBean();
					
					resumo.setCdautorizacaotrabalho(at.getCdautorizacaotrabalho());
					resumo.setEtapa(at.getEtapa());
					resumo.setResponsavel(at.getColaboradorresponsavel());
					resumo.setSituacao(at.getAutorizacaoestado());
					resumo.setPeso(at.getPeso());
					resumo.setEstimativa(at.getEstimativa());
					resumo.setDtprevisao(at.getDtprevisao());
					
					listaResumoCiclo.add(resumo);
				}
				if (listaResumoCiclo.size() > 0)
					request.setAttribute("listaResumoCiclo", listaResumoCiclo);
			}
			
			form.setListaApontamento(apontamentoService.getApontamentosByRequisicao(form));
			for(Apontamento ap : form.getListaApontamento()){
				if (ap.getListaApontamentoHoras()!=null && !ap.getListaApontamentoHoras().isEmpty()){
					ap.setCdapontamentohoras(ap.getListaApontamentoHoras().get(0).getCdapontamentohoras());
					ap.setHrinicioApontamento(ap.getListaApontamentoHoras().get(0).getHrinicio());
					ap.setHrfimApontamento(ap.getListaApontamentoHoras().get(0).getHrfim());
				}
			}
			
			if(form.getNotaFiscalServico() != null){
				List<NotaDocumento> listaNotaDocumento = notaDocumentoService.findByNotasForValidacao(form.getNotaFiscalServico().getCdNota().toString());
				for(NotaDocumento notaDocumento : listaNotaDocumento){
					Documento documento = contareceberService.loadWithoutWhereEmpresa(notaDocumento.getDocumento());
					Date hoje = SinedDateUtils.currentDate();
					boolean vencida = SinedDateUtils.beforeIgnoreHour(documento.getDtvencimento(), hoje);
					vencida = vencida && !Documentoacao.CANCELADA.equals(documento.getDocumentoacao()) && !Documentoacao.BAIXADA.equals(documento.getDocumentoacao()) && !Documentoacao.NEGOCIADA.equals(documento.getDocumentoacao());
					if (vencida){
						form.setPendenciafinanceira(vencida);
						request.addMessage("Cliente com pend�ncia financeira h� "+SinedDateUtils.calculaDiferencaDias(documento.getDtvencimento(), hoje)+" dias.", MessageType.ERROR);
						break;
					}
				}
			}
			request.setAttribute("login", request.getUser().getLogin());
			request.setAttribute("senha", request.getUser().getPassword());
		}
		
		boolean isAdmin = Boolean.TRUE.equals(SinedUtil.isUsuarioLogadoAdministrador());
		boolean isColaborador = SinedUtil.getUsuarioComoColaborador()!=null;
		request.setAttribute("podeEditarAtividade", isColaborador);
		request.setAttribute("podeVisualizarAtividade", isColaborador || isAdmin);
		request.setAttribute("existeMovEstoqueOSConcluida", form.getCdrequisicao() != null && form.getRequisicaoestado() != null &&
				Requisicaoestado.CONCLUIDA.equals(form.getRequisicaoestado().getCdrequisicaoestado()) && 
				movimentacaoestoqueService.existeMovimentacaoestoqueByRequisicao(form));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		request.setAttribute("dataAtual", sdf.format(SinedDateUtils.currentDate()));
		sdf = new SimpleDateFormat("HH:mm");
		request.setAttribute("horaAtual", sdf.format(SinedDateUtils.currentDate()));
		request.setAttribute("usuarioColaborador", View.convertToJson(SinedUtil.getUsuarioComoColaborador()).toString());
		if(listaProjeto == null || listaProjeto.isEmpty()){
			listaProjeto = projetoService.findByContrato(form.getContrato());
		}
		request.setAttribute("listaProjeto", listaProjeto);
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("listaAtividadetipo", atividadetipoService.findForServicos());
		request.setAttribute("OS_FATURADA_BYCONTRATO", isOsfaturadabycontrato);
		
		List<Colaborador> listaColaboradorConsulta = new ArrayList<Colaborador>();
		if(CONSULTAR.equals(request.getParameter("ACAO")) && form.getColaboradorresponsavel() != null && 
				form.getColaboradorresponsavel().getCdpessoa() != null){
			listaColaboradorConsulta.add(colaboradorService.load(form.getColaboradorresponsavel(), "colaborador.cdpessoa, colaborador.nome"));
		}
		request.setAttribute("listaColaboradorConsulta", listaColaboradorConsulta);
		request.setAttribute("listaMeioContato", meiocontatoService.findAtivos());
		
		Colaborador usuarioColaborador = SinedUtil.getUsuarioComoColaborador();
		request.setAttribute("isUsuarioColaborador", usuarioColaborador != null);
		if(form.getCdrequisicao() != null && usuarioColaborador != null){
			Registroatividadehora registroatividadehora = registroatividadehoraService.findByUsuarioRequisicao(usuarioColaborador, form); 
			request.setAttribute("registroatividadehora", registroatividadehora);
		}
		
		if (form.getListaRequisicaohistorico()!=null){
			for (Requisicaohistorico requisicaohistorico: form.getListaRequisicaohistorico()){
				if (requisicaohistorico.getObservacao()!=null){
					requisicaohistorico.setObservacao(requisicaohistorico.getObservacao().replaceAll("\n", "\\<br\\>"));
				}
			}
		}
		
		request.setAttribute("isRequisicaoConcluida", form.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CONCLUIDA));
		request.setAttribute("osConcluidaNaoPermiteAlteracao", parametrogeralService.getBoolean("OS_CONCLUIDA_NAO_PERMITE_ALTERACAO"));
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,RequisicaoFiltro filtro) throws CrudException {
		int gerarCodigo = ValidadorRequisicao.gerarCodigo(request.getUser().getLogin(), request.getUser().getPassword());
		request.setAttribute("cid", Integer.toString(gerarCodigo));
		request.setAttribute("login", request.getUser().getLogin());
		request.setAttribute("senha", request.getUser().getPassword());
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request,	Requisicao bean) {		
//		if(bean.getCdtarefa() != null){
		if(bean.getCdstarefa()!=null && !bean.getCdstarefa().trim().equals("")){
			request.clearMessages();
			request.addMessage("Ordem de Servi�o " + bean.getCdrequisicao() + " gerada com sucesso");
			String href = request.getServletRequest().getContextPath() + "/projeto/crud/Tarefa?ACAO=atualizarSituacaoAposGerarRequisicao&cdstarefa=" + bean.getCdstarefa();
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = '"+href+"';window.close();</script>");
			return null;
		}
		
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Requisicao bean)throws Exception {
		try{
			if(bean.getListaApontamento() != null && bean.getListaApontamento().isEmpty()){
				for (Apontamento apontamento : bean.getListaApontamento()) {
					if(apontamento.getHrfimApontamento().before(apontamento.getHrinicioApontamento())){
						throw new SinedException("A hora t�rmino n�o pode ser menor que a hora inicial.");
					}
				}
			}
			
			boolean paramCliente = parametrogeralService.getValorPorNome("NotificacaoClienteRequisicao").toUpperCase().equals("TRUE");
			if (bean.getAtividadetipo() != null){
				paramCliente = (paramCliente && !atividadetipoService.load(bean.getAtividadetipo()).getNaoenviaremailcliente());
			}
			boolean concluida = false;
			boolean criada = true;
			boolean cancelada = false;
			
			Boolean alterouEstadoOuResponsavel = true;
			Boolean existeObservacao = StringUtils.isNotBlank(bean.getObservacao());
			Boolean colaboradordiferente = false;
			Requisicao requisicaoAntiga = null; 
			if (bean.getCdrequisicao() != null) {
				requisicaoAntiga = requisicaoService.carregaRequisicao(bean);
				if(requisicaoAntiga != null && requisicaoAntiga.getColaboradorresponsavel() != null && 
						bean.getColaboradorresponsavel() != null && 
						!bean.getColaboradorresponsavel().equals(requisicaoAntiga.getColaboradorresponsavel())){
					colaboradordiferente = true;
				}
			}
			
			if(bean.getCdrequisicao() != null){
				Requisicao requisicao = requisicaoService.carregaDadosRequisicaoForEmail(bean);
				if(requisicao != null && ( (requisicao.getRequisicaoestado() != null && bean.getRequisicaoestado() != null &&
						requisicao.getRequisicaoestado().equals(bean.getRequisicaoestado())) && !colaboradordiferente)){
					alterouEstadoOuResponsavel = false;
				}
			}
			
			if (bean.getCdrequisicao() != null) {
				criada = false;
				
				if (!requisicaoAntiga.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CANCELADA) && bean.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CANCELADA) &&
						!requisicaoAntiga.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.EMESPERA) &&
						!requisicaoAntiga.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.AGUARDANDO_RESPOSTA)) {
					throw new SinedException("Ordem de Servi�o n�o pode ser cancelada. Para ser cancelada a Ordem de Servi�o tem que estar na situa��o 'EM ESPERA' ou 'AGUARDANDO RESPOSTA'.");
				}
				
				if (bean.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CONCLUIDA) 
						&& !requisicaoAntiga.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CONCLUIDA)) {
					concluida = true;
					bean.setDtconclusao(new Date(System.currentTimeMillis()));
				}else if(!bean.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CONCLUIDA)){
					bean.setDtconclusao(null);
				}			
				if (bean.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CANCELADA)){
					cancelada = true;
				}
			}else if(bean.getRequisicaoestado() != null && bean.getRequisicaoestado().getCdrequisicaoestado() != null &&
					bean.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CONCLUIDA)){
				concluida = true;
				bean.setDtconclusao(new Date(System.currentTimeMillis()));
			}
			
			//Integer cdtarefa = bean.getCdtarefa();	
			String cdstarefa = bean.getCdstarefa();	
			String observacao = bean.getObservacao();
			Arquivo arquivo = bean.getArquivohistorico();
			Integer cdvenda = null;
			Integer cdpedidovenda = null;
			if (bean.getVenda() != null){
				cdvenda = bean.getVenda().getCdvenda();
			}
			if (bean.getPedidovenda() != null){
				cdpedidovenda = bean.getPedidovenda().getCdpedidovenda();
			}
			Integer cdcontrato = bean.getCdcontratotrans();
			
			apontamentoService.getTotalHorasForRequisicao(bean);
			
			setProximonumeroordemservico(bean);
			
			super.salvar(request, bean);
			apontamentoService.saveApontamentoFromRequisicao(bean);
			try {
				if(alterouEstadoOuResponsavel || existeObservacao){
					if(paramCliente){
						paramCliente = alterouEstadoOuResponsavel || (existeObservacao && !Boolean.TRUE.equals(bean.getObservacaoInterna()));
					}
					requisicaoService.enviaEmailRequisicao(request, bean, concluida, criada, cancelada, paramCliente, observacao, bean.getObservacaoInterna());
				}
			} catch (SinedException e) {
//				request.addMessage("N�o foi poss�vel enviar o email para o respons�vel. " + e.getMessage(), MessageType.WARN);
				request.addMessage(e.getMessage(), MessageType.ERROR);
			} catch (Exception e) {
//				request.addMessage("N�o foi poss�vel enviar o email para o respons�vel. " + e.getMessage(), MessageType.WARN);
				request.addMessage("N�o foi poss�vel enviar o e-mail.", MessageType.ERROR);
				e.printStackTrace();
			}		
		
			
			if(arquivo != null && (observacao == null || observacao.equals(""))){
				observacao = "Arquivo em anexo";
			}
			
			if(criada && cdcontrato != null){
				observacao = "Origem: Contrato <a href=\"javascript:visualizarContrato("+ cdcontrato+");\">"+cdcontrato +"</a>.";
				Contratohistorico contratohistorico = new Contratohistorico(new Contrato(cdcontrato),
																			new Timestamp(System.currentTimeMillis()),
																			null,
																			SinedUtil.getUsuarioLogado());

				contratohistorico.setObservacao("Gerada a Ordem de Servi�o <a href=\"javascript:visualizaOS("+bean.getCdrequisicao()+");\">"+bean.getCdrequisicao() +"</a>.");
				contratohistorico.setAcao(Contratoacao.GERAR_OS);
				contratohistoricoService.saveOrUpdate(contratohistorico);
			}
			
			
			if(cdvenda != null && criada){
				observacao = "Origem: Venda <a href=\"javascript:visualizaVenda("+ bean.getVenda().getCdvenda()+");\">"+bean.getVenda().getCdvenda() +"</a>.";
				Vendahistorico vendahistorico = new Vendahistorico();
				vendahistorico.setVenda(bean.getVenda());
				vendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				vendahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				vendahistorico.setObservacao("Ordem de Servi�o: <a href=\"javascript:visualizaOS("+bean.getCdrequisicao()+");\">"+bean.getCdrequisicao() +"</a>.");
				vendahistorico.setAcao("Gera��o da Ordem de Servi�o " + bean.getCdrequisicao());
				vendahistoricoService.saveOrUpdate(vendahistorico);
			}
			if(cdpedidovenda != null && criada){
				observacao = "Origem: Pedido de Venda <a href=\"javascript:visualizaPedidoVenda("+ cdpedidovenda+");\">"+ cdpedidovenda +"</a>.";
				Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
				pedidovendahistorico.setPedidovenda(bean.getPedidovenda());
				pedidovendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				pedidovendahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				pedidovendahistorico.setObservacao("Ordem de Servi�o: <a href=\"javascript:visualizaOS("+bean.getCdrequisicao()+");\">"+bean.getCdrequisicao() +"</a>.");
				pedidovendahistorico.setAcao("Gera��o da Ordem de Servi�o " + bean.getCdrequisicao());
				pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
			}
			
			if(criada) {
				observacao = "(Criado) " + (observacao != null ? observacao : "");
			} else if(concluida){
				observacao = "(Conclu�do) " + (observacao != null ? observacao : "");
			} else {
				observacao = "(Alterada) " + (observacao != null ? observacao : "");
			}  
			requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(arquivo, observacao, bean));
			
			if(cdstarefa!=null && !cdstarefa.trim().equals("")){
				List<Integer> listaCdtarefa = SinedUtil.getListaByIds(cdstarefa);
				for (Integer cdtarefa: listaCdtarefa){
					Tarefarequisicao tarefarequisicao = new Tarefarequisicao();
					tarefarequisicao.setTarefa(new Tarefa(cdtarefa));
					tarefarequisicao.setRequisicao(bean);
					tarefarequisicaoService.saveOrUpdate(tarefarequisicao);
				}
			}
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "fk_requisicaonota_2")) {
				throw new SinedException("Existe(m) item(ns) da Ordem de Servi�o que n�o pode(m) ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
			} else throw e;
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Requisicao bean) throws Exception {
		if(request.getParameter("itenstodelete") != null){
			if (validaExcluirEmMassa(request, bean)) {
				super.excluir(request, bean);
			}else
				throw new SinedException("Requisi��o n�o pode ser exclu�da.");
		}else{ 
			List<Requisicao> listaRequisicao = requisicaoService.carregaRequisicao(bean.getCdrequisicao().toString());
			if (!listaRequisicao.get(0).getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.EMESPERA)) {
				throw new SinedException("Requisi��o n�o pode ser exclu�da.");
			}else{
				super.excluir(request, bean);
			}
		}	
	}

	
	public Boolean validaExcluirEmMassa(WebRequestContext request,Requisicao bean) {
		String itens = request.getParameter("itenstodelete");
		List<Requisicao> listaRequisicao = requisicaoService.carregaRequisicao(itens);
		for (Requisicao requisicao : listaRequisicao) {			
			if (!requisicao.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.EMESPERA)) {
				return false;
			}else{
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected ListagemResult<Requisicao> getLista(WebRequestContext request, RequisicaoFiltro filtro) {
		ListagemResult<Requisicao> listagemResult = super.getLista(request, filtro);
		List<Requisicao> list = listagemResult.list();
		
		String param = parametrogeralService.getValorPorNome(Parametrogeral.W3MPSBR_INTEGRACAO);
		if(param != null && param.toUpperCase().equals("TRUE")){
			String whereIn = CollectionsUtil.listAndConcatenate(list, "cdrequisicao", ",");
			
			if(whereIn != null && !whereIn.equals("")){
				Map<Integer, String> map = requisicaoService.buscaSituacoesAutorizacoes(whereIn);
				
				if(map != null){
					for (Requisicao requisicao : list) {
						if(map.containsKey(requisicao.getCdrequisicao())){
							requisicao.setSituacao(map.get(requisicao.getCdrequisicao()));
						}
					}
				}
			}
		}
		
		for (Requisicao requisicao : list) {
			if(requisicao.getCliente() != null && requisicao.getCliente().getCdpessoa() != null){
				Date dtvencimento = documentoService.getPrimeiroVencimentoDocumentoAtrasado(requisicao.getCliente());	
				requisicao.setPendenciafinanceira(dtvencimento != null);
			}
		}
		
		if(filtro.getExibirTotais() != null && filtro.getExibirTotais()){
			Double horasTrabalhadas = requisicaoService.calculaTotalHorasTrabalhadas(filtro); 
			if(horasTrabalhadas == null || horasTrabalhadas == 0.00){
				filtro.setTotalHorasTrabalhadas("00:00");
			} else {
				filtro.setTotalHorasTrabalhadas(SinedDateUtils.doubleToFormatoHora(horasTrabalhadas));
			}
			filtro.setTotal(requisicaoService.calculaTotal(filtro));
		}
		
		return listagemResult;
	}
	
	
	@OnErrors(LISTAGEM)
	@Action("atualizaRequisicao")
	public void atualizaRequisicao(WebRequestContext request) throws IOException{
		if (request.getParameter("cdrequisicao")!=null) {
			requisicaoService.updateEstado(new Requisicaoestado(Requisicaoestado.EMANDAMENTO), request.getParameter("cdrequisicao"));
//			Requisicao requisicao = new Requisicao(Integer.parseInt(request.getParameter("cdrequisicao")));
		}
//		System.out.println(request.getAttribute("cdrequisicao"));
	}
	
	@Action("gerar")
	public ModelAndView gerar(WebRequestContext request, RequisicaoFiltro filtro){
		
		List<Requisicao> listaRequisicao = requisicaoService.carregaDadosRequisicaoForCSV(filtro);
		Integer total = 0;
		
		//VARI�VEL PARA CONTROLE DO C�DIGO DA REQUISI��O (A CONSULTA PODER� RETORNAR MAIS DE UMA LINHA PARA A MESMA REQUISICAO MAS COM MATERIALREQUISICAO DIFERENTE )
		Integer cdrequisicaoatual = -1;
		
		
		// Cabe�alho
		StringBuffer rel = new StringBuffer();
		rel.append("\"C�DIGO\";");
		rel.append("\"CLIENTE\";");
		rel.append("\"CONTRATO\";");
		rel.append("\"PROJETO\";");
		rel.append("\"DATA DA REQUISI��O\";");
		rel.append("\"DESCRICAO\";");
		rel.append("\"PRIORIDADE\";");
		rel.append("\"SITUA��O\";");		
		rel.append("\"HORAS TRABALHADAS\";");		
		rel.append("\"RESPONS�VEL\";");		
		rel.append("\"AUTORIZA��O\";");		
		rel.append("\"TIPO DE ORDEM DE SERVI�O\";");		
		rel.append("\"ORDEM DE SERVI�O\";");		
		rel.append("\n");
		

		for (Requisicao requisicao : listaRequisicao) {
						
			if(!cdrequisicaoatual.equals(requisicao.getCdrequisicao())){
				cdrequisicaoatual = requisicao.getCdrequisicao();
				total ++;
				
				// Bloco de institui��o autoridade
				rel.append("\"" + (requisicao.getCdrequisicao() != null ? requisicao.getCdrequisicao()+"\";" : "\";"));
				rel.append("\"" + (requisicao.getCliente()!=null && requisicao.getCliente().getCdpessoa()!=null?requisicao.getCliente().getNome()+"\";":"\";"));
				rel.append("\"" + (requisicao.getContrato()!=null?requisicao.getContrato().getDescricao()+"\";":"\";"));
				rel.append("\"" + (requisicao.getProjeto()!=null && requisicao.getProjeto().getNome()!=null?requisicao.getProjeto().getNome()+"\";":"\";"));
				rel.append("\"" + (requisicao.getDtrequisicao() != null ? requisicao.getDtrequisicao()+"\";": "\";"));
				rel.append("\"" + (requisicao.getDescricao() != null ? requisicao.getDescricao().replaceAll("\r\n", "").replaceAll(";", "/,")+"\";": "\";"));
				rel.append("\"" + (requisicao.getRequisicaoprioridade() != null ? requisicao.getRequisicaoprioridade().getDescricao()+"\";": "\";"));
				rel.append("\"" + (requisicao.getRequisicaoestado() != null ? requisicao.getRequisicaoestado().getDescricao()+"\";": "\";"));
	/**/		rel.append("\"" + (requisicao.getHorasTrabalhadas() != null ? requisicao.getHorasTrabalhadas()+"\";": "\";"));
				rel.append("\"" + (requisicao.getColaboradorresponsavel() != null && requisicao.getColaboradorresponsavel().getCdpessoa() != null? requisicao.getColaboradorresponsavel().getNome()+"\";": "\";"));
				rel.append("\"" + (requisicao.getSituacao() != null ? requisicao.getSituacao()+"\";": "\";"));
				rel.append("\"" + (requisicao.getAtividadetipo() != null ? requisicao.getAtividadetipo().getNome()+"\";": "\";"));
				rel.append("\"" + (requisicao.getOrdemservico() != null ? requisicao.getOrdemservico()+"\";": "\";"));
				
				if(requisicao.getMaterialrequisicaoCdrequisicao() != null && !requisicao.getMaterialrequisicaoCdrequisicao().equals(0)){				
					rel.append("\n").append(";");
					rel.append("\"MATERIAL/SERVI�O\";");
					rel.append("\"DATA DISPONIBILIDADE\";");
					rel.append("\"DATA RETIRADA\";");
					rel.append("\"QTDE\";");
					rel.append("\"VALOR TOTAL\";");					
					
					rel.append("\n").append(";");					
					rel.append("\"" + (requisicao.getMaterialNome() != null ? requisicao.getMaterialNome() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoDtdisponibilizacao() != null ? requisicao.getMaterialrequisicaoDtdisponibilizacao() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoDtretirada() != null ? requisicao.getMaterialrequisicaoDtretirada() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoQuantidade() != null ? requisicao.getMaterialrequisicaoQuantidade() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoValortotal() != null ? requisicao.getMaterialrequisicaoValortotal() : ""));			
					rel.append("\";");
				}
				
				rel.append("\n");
			}else{
				if(requisicao.getMaterialrequisicaoCdrequisicao() != null ){				
					
					rel.append(";");					
					rel.append("\"" + (requisicao.getMaterialNome() != null ? requisicao.getMaterialNome() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoDtdisponibilizacao() != null ? requisicao.getMaterialrequisicaoDtdisponibilizacao() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoDtretirada() != null ? requisicao.getMaterialrequisicaoDtretirada() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoQuantidade() != null ? requisicao.getMaterialrequisicaoQuantidade() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoValortotal() != null ? requisicao.getMaterialrequisicaoValortotal() : ""));
					rel.append("\";");
				}		

				rel.append("\n");
			}
		}
		rel.append("\n\"TOTAL DE REGISTROS = " + total + "\";");
		
		Resource resource = new Resource("text/csv","requisicao_" + SinedUtil.datePatternForReport() + ".csv",rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	@Action("gerar")
	public ModelAndView gerarCSVFaturamento(WebRequestContext request, RequisicaoFiltro filtro){
		
		List<Requisicao> listaRequisicao = requisicaoService.carregaDadosRequisicaoForCSV(filtro);
		Integer total = 0;
		
		//VARI�VEL PARA CONTROLE DO C�DIGO DA REQUISI��O (A CONSULTA PODER� RETORNAR MAIS DE UMA LINHA PARA A MESMA REQUISICAO MAS COM MATERIALREQUISICAO DIFERENTE )
		Integer cdrequisicaoatual = -1;
		
		
		// Cabe�alho
		StringBuffer rel = new StringBuffer();
		rel.append("\"N�MERO\";");
		rel.append("\"OS RELACIONADA\";");
		rel.append("\"CLIENTE\";");
		rel.append("\"ENDERE�O\";");
		rel.append("\"MUNIC�PIO\";");
		rel.append("\"DATA DE REQUISI��O\";");
		rel.append("\"TIPO OS\";");
		rel.append("\"DESCRI��O\";");		
		rel.append("\"MATERIAL\";");		
		rel.append("\"QTDE\";");		
		rel.append("\"VALOR UNIT�RIO\";");		
		rel.append("\"VALOR TOTAL\";");		
		rel.append("\"OBS\";");
		rel.append("\"INTERA��ES\";");	
		rel.append("\n");
		

		for (Requisicao requisicao : listaRequisicao) {
						
			if(!cdrequisicaoatual.equals(requisicao.getCdrequisicao())){
				cdrequisicaoatual = requisicao.getCdrequisicao();
				total ++;
				
				// Bloco de institui��o autoridade
				rel.append("\"" + (requisicao.getCdrequisicao() != null ? requisicao.getCdrequisicao()+"\";" : "\";"));
				rel.append("\"" + (requisicao.getRequisicaorelacionada() != null ? requisicao.getRequisicaorelacionada().getCdrequisicao()+"\";" : "\";"));
				rel.append("\"" + (requisicao.getCliente()!=null && requisicao.getCliente().getCdpessoa()!=null?requisicao.getCliente().getNome()+"\";":"\";"));
				rel.append("\"" + (requisicao.getEndereco()!=null?requisicao.getEndereco().getLogradouroCompletoComBairroSemMunicipio()+"\";":"\";"));
				rel.append("\"" + (requisicao.getEndereco()!=null?requisicao.getEndereco().getMunicipioUf()+"\";":"\";"));
				rel.append("\"" + (requisicao.getDtrequisicao() != null ? requisicao.getDtrequisicao()+"\";": "\";"));
				rel.append("\"" + (requisicao.getAtividadetipo() != null ? requisicao.getAtividadetipo().getNome()+"\";": "\";"));
				rel.append("\"" + (requisicao.getDescricao() != null ? requisicao.getDescricao().replaceAll("\r\n", "").replaceAll(";", "/,")+"\";": "\";"));
				rel.append(";;;;;");
				//rel.append("\"" + (requisicao.getMaterialrequisicaoQtdeIteracao() != null ? requisicao.getMaterialrequisicaoQtdeIteracao()+"\";": "\";"));
				
				if(requisicao.getMaterialrequisicaoCdrequisicao() != null && !requisicao.getMaterialrequisicaoCdrequisicao().equals(0)){				
					rel.append("\n").append(";;;;;;;;");					
					rel.append("\"" + (requisicao.getMaterialNome() != null ? requisicao.getMaterialNome() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoQuantidade() != null ? requisicao.getMaterialrequisicaoQuantidade() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoValortotal() != null ? new Money(requisicao.getMaterialrequisicaoValortotal().doubleValue() / (requisicao.getMaterialrequisicaoQuantidade() != null && requisicao.getMaterialrequisicaoQuantidade() > 0 ? requisicao.getMaterialrequisicaoQuantidade() : 1d)) : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoValortotal() != null ? requisicao.getMaterialrequisicaoValortotal() : ""));			
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoObs() != null ? requisicao.getMaterialrequisicaoObs() : ""));			
					rel.append("\";");
				}
				
				rel.append("\n");
			}else{
				if(requisicao.getMaterialrequisicaoCdrequisicao() != null ){				
					rel.append(";;;;;;;;");					
					rel.append("\"" + (requisicao.getMaterialNome() != null ? requisicao.getMaterialNome() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoQuantidade() != null ? requisicao.getMaterialrequisicaoQuantidade() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoValortotal() != null ? new Money(requisicao.getMaterialrequisicaoValortotal().doubleValue() / (requisicao.getMaterialrequisicaoQuantidade() != null && requisicao.getMaterialrequisicaoQuantidade() > 0 ? requisicao.getMaterialrequisicaoQuantidade() : 1d)) : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoValortotal() != null ? requisicao.getMaterialrequisicaoValortotal() : ""));
					rel.append("\";");
					rel.append("\"" + (requisicao.getMaterialrequisicaoObs() != null ? requisicao.getMaterialrequisicaoObs() : ""));			
					rel.append("\";");
				}		

				rel.append("\n");
			}
		}
		rel.append("\n\"TOTAL DE REGISTROS = " + total + "\";");
		
		Resource resource = new Resource("text/csv","requisicao_" + SinedUtil.datePatternForReport() + ".csv",rel.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
//	/**
//	 * M�todo que fatura uma requisi��o
//	 * 
//	 * @param request
//	 * @param requisicao
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	public ModelAndView doFaturarRequisicao(WebRequestContext request, Requisicao requisicao){
//		String whereIn = request.getParameter("selectedItens");
//		List<Requisicao> listaRequisicoes = requisicaoService.carregaRequisicao(whereIn);
//		
//		if(listaRequisicoes != null && !listaRequisicoes.isEmpty())
//			requisicaoService.validaRequisicoesParaFaturar(listaRequisicoes, request.getBindException());
//		else
//			request.getBindException().reject("001", "Nenhuma requisi��o foi selecionada.");
//			
//		if(request.getBindException().hasErrors()){
//			request.addError(request.getBindException().getMessage().toString().substring(request.getBindException().getMessage().toString().lastIndexOf("[")+1, request.getBindException().getMessage().toString().length()-1));
//			if(request.getParameter("entrada") != null && request.getParameter("entrada").toUpperCase().equals("TRUE"))
//				return new ModelAndView("redirect:/servicointerno/crud/Requisicao?ACAO=consultar&cdrequisicao="+whereIn);
//			else
//				return continueOnAction("listagem", (ServidorFiltro)request.getSession().getAttribute(this.getClass().getSimpleName()+"CONTROLLER"+requisicao.getClass().getName()));
//		}
//		
//		NotaFiscalServico nota = requisicaoService.criaNotas(listaRequisicoes);
//		nota.setWhereIn(whereIn);
//		request.getSession().setAttribute("notaFiscalServico", nota);
//		return new ModelAndView("forward:/faturamento/crud/NotaFiscalServico", "nota", nota);
//	}
	
	public void ajaxOnChangeMaterial(WebRequestContext request, Material material){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		material = materialService.loadForNFProduto(material);
		
		view.println("var unidademedida = '" + (material.getUnidademedida() != null && material.getUnidademedida().getSimbolo() != null ? material.getUnidademedida().getSimbolo() : "") + "';");
		view.println("var patrimonio = " + (material.getPatrimonio() != null && material.getPatrimonio() ? "true" : "false") + ";");
		view.println("var valorvenda = '" + (material.getValorvenda() != null ? material.getValorvenda() : "") + "';");
	}
	
	/**
	 * 
	 * @param request
	 * @param bean
	 * @return
	 */
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, Requisicao bean){
		if(bean.getAtividadetipo() == null || bean.getAtividadetipo().getCdatividadetipo() == null){
			throw new SinedException("O tipo de Atividade n�o pode se nulo.");
		}
		
		List<Atividadetipoitem> lista = atividadetipoitemService.findByTipo(bean.getAtividadetipo());
		return new JsonModelAndView()
					.addObject("size", lista.size())
					.addObject("lista", lista);	
	}
	
	/**
	 * M�todo ajax para buscar os enderecos do cliente
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaEnderecoByCliente(WebRequestContext request){
		
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		String listaendereco = null;
		String cdcliente = request.getParameter("cdcliente");
		StringBuilder s = new StringBuilder();
		
		if(cdcliente != null && !cdcliente.equals("")){			
			List<Endereco> listaEndereco = enderecoService.findAtivosByCliente(new Cliente(Integer.parseInt(cdcliente)));
			if(listaEndereco != null && !listaEndereco.isEmpty()){
				listaendereco = SinedUtil.convertToJavaScript(listaEndereco, "listaendereco", null);							
			}
		}
		
		if(listaendereco != null)
			s.append(listaendereco);
		else
			s.append("var listaendereco = '';");
		
		s.append(listaendereco);	
		view.println(s.toString());
		
	}
	
	/**
	 * M�todo ajax para buscar os projetos
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaProjetoByContrato(WebRequestContext request){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		String listaprojeto = null;
		String cdcontrato = request.getParameter("cdcontrato");
		String cdtarefa = request.getParameter("cdtarefa");
		StringBuilder s = new StringBuilder();
		Contrato contrato = null;
		List<Projeto> listaProjeto = null;
		if(cdtarefa != null && !"".equals(cdtarefa) && !"<null>".equals(cdtarefa)){
			listaProjeto = projetoService.findForGerarOrdemServico(cdtarefa.toString());
		}else if(cdcontrato != null && !cdcontrato.equals("")){		
			contrato = new Contrato(new Contrato(Integer.parseInt(cdcontrato)));
		}
		
		if(listaProjeto == null || listaProjeto.isEmpty()){
			listaProjeto = projetoService.findByContrato(contrato);
		}
		
		if(listaProjeto != null && !listaProjeto.isEmpty()){
			listaprojeto = SinedUtil.convertToJavaScript(listaProjeto, "listaprojeto", null);							
		}
		
		if(listaprojeto != null)
			s.append(listaprojeto);
		else
			s.append("var listaprojeto = '';");
		
		s.append(listaprojeto);	
		view.println(s.toString());
		
	}
	
	/**
	 * M�todo ajax para buscas os contratos por clientes que tenham apontamentos em aberto
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaContratoByClienteAbertosApontamento(WebRequestContext request){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		String listacontrato = null;
		String cdcliente = request.getParameter("cdcliente");
		String cdempresa = request.getParameter("cdempresa");
		StringBuilder s = new StringBuilder();
		
		Empresa empresa = null;
		if(cdempresa != null && !"".equals(cdempresa)) empresa = new Empresa(Integer.parseInt(cdempresa));
		
		if(cdcliente != null && !cdcliente.equals("")){			
			List<Contrato> listaContrato = contratoService.findByClienteEmpresaAbertosApontamento(new Cliente(Integer.parseInt(cdcliente)), empresa);
			if(listaContrato != null && !listaContrato.isEmpty()){
				listacontrato = SinedUtil.convertToJavaScript(listaContrato, "listacontrato", null);
								
			}
		}
		
		if(listacontrato != null)
			s.append(listacontrato);
		else
			s.append("var listacontrato = '';");
		
		view.println(s.toString());
	}
	
	/**
	 * M�todo ajaz para buscar os contatos do cliente
	 *
	 * @param request
	 * @author Luiz Fernando
	 */
	public void ajaxBuscaContatoByCliente(WebRequestContext request){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		String listacontato = null;
		String cdcliente = request.getParameter("cdcliente");
		StringBuilder s = new StringBuilder();
		
		String contatoresponsavelos = null;
		if(cdcliente != null && !cdcliente.equals("")){			
			List<Contato> listaContato = contatoService.getListContatoByCliente(new Cliente(Integer.parseInt(cdcliente)), Boolean.TRUE);
			if(listaContato != null && !listaContato.isEmpty()){
				listacontato = SinedUtil.convertToJavaScript(listaContato, "listacontato", null);
				
				for(Contato contato : listaContato){
					if(contato.getResponsavelos() != null && contato.getResponsavelos()){
						contatoresponsavelos = "br.com.linkcom.sined.geral.bean.Contato[cdpessoa="+contato.getCdpessoa()+"]";
					}
				}
			}
		}
		if(listacontato != null){
			s.append(listacontato);
		}else {
			s.append("var listacontato = '';");
		}
		
		if(contatoresponsavelos != null){
			s.append(" var contatoresponsavelos = '"+ contatoresponsavelos +"';");
		}else {
			s.append(" var contatoresponsavelos = '';");
		}
		
		view.println(s.toString());
	}
	
	/**
	 * M�todo que abre o popup para a escolha do novo respons�vel
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirAlterarResponsavel(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhuma ordem de servi�o selecionada.");
			SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
			return null;
		}
		
		String[] requisicoes = StringUtils.split(whereIn, ",");
		
		if (requisicoes != null && requisicoes.length > 0) {
			Requisicao requisicaoFound = null;
			
			for (String requisicao : requisicoes) {
				requisicaoFound = requisicaoService.carregaRequisicao(new Requisicao(Integer.parseInt(requisicao)));
				if (parametrogeralService.getBoolean("OS_CONCLUIDA_NAO_PERMITE_ALTERACAO") && 
						Requisicaoestado.CONCLUIDA.equals(requisicaoFound.getRequisicaoestado().getCdrequisicaoestado())) {
					request.addError("Ordem de Servi�o com situa��o 'Conclu�da' n�o poder� ser alterada.");
					SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
					return null;
				}
			}
		} else {
			request.addError("Nenhuma ordem de servi�o selecionada.");
			SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
			return null;
		}
		
		Requisicao requisicao =  new Requisicao();
		
		request.setAttribute("titulo", "Alterar respon�svel");
		request.setAttribute("acaoSalvar", "alterarResponsavel");
		request.setAttribute("selectedItens", whereIn);
		
		return new ModelAndView("direct:/crud/popup/abrirAlterarResponsavel", "bean", requisicao);
	}
	
	/**
	 * M�todo que faz a altera��o do respons�vel em massa
	 *
	 * @param request
	 * @param requisicao
	 * @return
	 * @author Luiz Fernando
	 */
	public void alterarResponsavel(WebRequestContext request, Requisicao requisicao){
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhuma ordem de servi�o selecionada.");
			SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");			
		}else if(requisicao.getColaboradorresponsavel() == null){
			request.addError("Nenhuma respons�vel selecionado.");
			SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
		}else{		
			String[] cds = whereIn.split(",");
			for(String cdrequisicao : cds){
				requisicaoService.updateResponsavel(requisicao.getColaboradorresponsavel(), Integer.parseInt(cdrequisicao));
			}
			
			request.addMessage("Respons�vel alterado com sucesso.");
			SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
		}
	}
	
	/**
	 * Conclui as requisi��es selecionadas na tela.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaoService#enviaEmailRequisicao(Requisicao requisicao, Boolean concluida)
	 * @see br.com.linkcom.sined.geral.service.RequisicaoService#updateEstado(Requisicaoestado requisicaoestado, String cd)
	 * 
	 * @param request
	 * @return
	 * @since 12/01/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView concluir(WebRequestContext request, Requisicao requisicao){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Boolean registrarSaida = Boolean.valueOf(request.getParameter("registrarSaida") != null ? request.getParameter("registrarSaida") : "false");
		Localarmazenagem localarmazenagem = requisicao.getLocalarmazenagem();
		Date dtconclusao = requisicao.getDtconclusao() != null ? requisicao.getDtconclusao() : null;
		boolean gerarExpedicao = false;
		List<Requisicao> listaRequisicaoSaida = null;
		if(registrarSaida){
			listaRequisicaoSaida = requisicaoService.findForRegistrarsaida(whereIn);
			for(Requisicao req : listaRequisicaoSaida){
				if(pedidovendatipoService.gerarExpedicaoVenda(req.getVenda() != null ? req.getVenda().getPedidovendatipo() : null, false)){
					gerarExpedicao = true;
					break;
				}
			}
			if(gerarExpedicao){
				if(!localarmazenagemService.getPermitirestoquenegativo(localarmazenagem)){
					List<String> listaErro = new ArrayList<String>();
					for(Requisicao reqAux : listaRequisicaoSaida){
						if(reqAux.getListaMateriaisrequisicao() != null && !reqAux.getListaMateriaisrequisicao().isEmpty()){
							for(Materialrequisicao item : reqAux.getListaMateriaisrequisicao()){
								Material material = materialService.getMaterialmestreGradeOrItemGrade(item.getMaterial());
								if(material != null && (material.getServico() == null || !material.getServico())){
									Double qtdeEntrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(material, localarmazenagem, null, null);
									Double qtdeSaida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(material, localarmazenagem, null, null);
									Double quantidade = item.getQuantidade();
									
									if(qtdeEntrada == null) qtdeEntrada = 0d;
									if(qtdeSaida == null) qtdeSaida = 0d;
									if(quantidade == null) quantidade = 0d;
									
									if((qtdeEntrada - qtdeSaida) < quantidade){
										listaErro.add("Material " + material.getNome() + " n�o dispon�vel no local indicado.");
									}
								}
							}
						}
					}
					
					if(listaErro.size() > 0){
						for (String string : listaErro) {
							request.addError(string);
						}
						SinedUtil.fechaPopUp(request);
						return null;
					}
				}
			}
		}
		
		List<Requisicao> lista = requisicaoService.carregaRequisicao(whereIn);
		
		for (Requisicao bean : lista) {
			if (registrarSaida && inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), bean.getEmpresa(), localarmazenagem, bean.getProjeto())) {
				request.addMessage("N�o � poss�vel realizar a baixa,  pois o registro est� vinculado � um registro de invent�rio. Para baixar, � necess�rio cancelar o invent�rio.", MessageType.ERROR);
				SinedUtil.fechaPopUp(request);
				return null;
			}
		}
		
		for (Requisicao bean : lista) {
			boolean paramCliente = parametrogeralService.getValorPorNome("NotificacaoClienteRequisicao").toUpperCase().equals("TRUE") &&
			                       (bean.getAtividadetipo() != null ? !bean.getAtividadetipo().getNaoenviaremailcliente() : true);
			try {
				requisicaoService.enviaEmailRequisicao(request, bean, true, false, false, paramCliente, null, bean.getObservacaoInterna());
			} catch (SinedException e) {
//				request.addMessage("N�o foi poss�vel enviar o email para o respons�vel. " + e.getMessage(), MessageType.WARN);
				request.addMessage(e.getMessage(), MessageType.ERROR);
			} catch (Exception e) {
//				request.addMessage("N�o foi poss�vel enviar o email para o respons�vel. " + e.getMessage(), MessageType.WARN);
				request.addMessage("N�o foi poss�vel enviar o e-mail.", MessageType.ERROR);
				e.printStackTrace();
			}	
			requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, "(Conclu�do)", bean));
			requisicaoService.updateEstado(new Requisicaoestado(Requisicaoestado.CONCLUIDA), bean.getCdrequisicao() + "", dtconclusao);
		}
		
		request.addMessage("Ordem(ns) de servi�o conclu�da(s) com sucesso.");
		if(registrarSaida){
			if(gerarExpedicao){
				requisicaoService.gerarExpedicaoAposConclusao(request, whereIn, localarmazenagem, listaRequisicaoSaida);
			} else {
				if(localarmazenagem != null){
					requisicaoService.registrarSaidaAposConclusao(whereIn, localarmazenagem, listaRequisicaoSaida);
				}
				SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
			}
			return null;
		}
			
		SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
		return null;		
	}
	
	/**
	 * M�todo que abre a popup para escolha do local de armazenagem
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopupRegistrarsaida(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Requisicao requisicao = new Requisicao();
		request.setAttribute("selectedItens", whereIn);
		return new ModelAndView("direct:/crud/popup/abrirPopupRegistrarsaida", "bean", requisicao);
	}
	
	public ModelAndView abrirPopupConcluirOS(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		Requisicao requisicao = new Requisicao();
		requisicao.setDtconclusao(new Date(System.currentTimeMillis()));
		request.setAttribute("selectedItens", whereIn);
		return new ModelAndView("direct:/crud/popup/popupConcluirOS", "bean", requisicao);
	}
	
	/**
	 * M�todo ajax que verifica se existe materiais nas ordens de servi�os
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView ajaxExistMaterialrequisicao(WebRequestContext request){
		Boolean existsmaterial = false;
		String whereIn = SinedUtil.getItensSelecionados(request);
		boolean somenteServico = false;
		
		if(whereIn != null && !"".equals(whereIn)){
			existsmaterial = requisicaoService.existsMaterial(whereIn);
			
			if(existsmaterial){
				List<Requisicao> lista = requisicaoService.findForRegistrarsaida(whereIn);
				if(lista == null || lista.isEmpty()){
					existsmaterial = false;			
				}else {
					int sizeListaMateriaisrequisicao = 0;
					int contSomenteServico = 0;	
					
					for (Requisicao requisicao: lista){
						if (requisicao.getListaMateriaisrequisicao()!=null){
							for (Materialrequisicao materialrequisicao: requisicao.getListaMateriaisrequisicao()){
								if (materialrequisicao.getMaterial()!=null && Boolean.TRUE.equals(materialrequisicao.getMaterial().getServico())){
									contSomenteServico++;
								}
							}
							sizeListaMateriaisrequisicao += requisicao.getListaMateriaisrequisicao().size();
						}
					}
					
					somenteServico = sizeListaMateriaisrequisicao==contSomenteServico;
				}
			}
		}
		return new JsonModelAndView().addObject("existsmaterial", existsmaterial).addObject("somenteServico", somenteServico);
	}
	
	/**
	 * M�todo ajax para carregar os contratos
	 * 
	 * @param request
	 * @return
	 * @since 02/02/2012
	 * @author Luiz Silva
	 */
	public void ajaxPreencheComboContratoByCliente(WebRequestContext request){
		List<Contrato> listaContrato = new ArrayList<Contrato>();
		
		String cdcliente = request.getParameter("cdcliente");
		if(cdcliente != null && !"".equals(cdcliente)){
			listaContrato = contratoService.findBycliente(new Cliente(Integer.parseInt(cdcliente)));
		}		
		
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaContrato, "listaContrato", ""));
	}
	
	/**
	 * Action que faz a busca de infos do cliente via ajax.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ClienteService#loadForRequisicaoInfo(Cliente cliente)
	 *
	 * @param request
	 * @param requisicao
	 * @return
	 * @since 22/06/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxInfoCliente(WebRequestContext request, Requisicao requisicao){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(requisicao.getCliente() != null){
			Cliente cliente = clienteService.loadForRequisicaoInfo(requisicao.getCliente());
			
			String telefones = cliente.getTelefonesHtml();
			String categorias = cliente.getCategorias();
			String segmentos = cliente.getSegmentos();
			String email = cliente.getEmail();
			
			jsonModelAndView.addObject("telefones", telefones);
			jsonModelAndView.addObject("categorias", categorias);
			jsonModelAndView.addObject("segmentos", segmentos);
			jsonModelAndView.addObject("email", email);
		}
		return jsonModelAndView;
	}
	
	/**
	 * M�todo que p�e o cliente, selecionado na entrada da OS, na sess�o para futura pesquisa de OSRelacioanada.
	 * @param request
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public ModelAndView ajaxPutClienteSessao(WebRequestContext request){
		String msgError = "<null>";
		String cdcliente = request.getParameter("cdcliente");
		if(cdcliente != null && !cdcliente.equals(""))
			request.getSession().setAttribute("cdclienteRequisicao", cdcliente);
		else {
			msgError = "Cliente n�o definido";
			request.getSession().removeAttribute("cdclienteRequisicao");
		}
			
		return new JsonModelAndView().addObject("msgError", msgError);
	}
	
	/**
	 * M�todo que busca uma lista de atividadetipoitem e a renderiza em formato html para ser inserido na p�gina.
	 * @param request
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public ModelAndView ajaxLoadListaAtividadetipoitem(WebRequestContext request){
		String msgError = "<null>";
		String cdatividadetipo = request.getParameter("cdatividadetipo");
		JsonModelAndView json = new JsonModelAndView();
		
		if(cdatividadetipo != null && !cdatividadetipo.equals("")){
			Atividadetipo tipo = new Atividadetipo();
			tipo.setCdatividadetipo(Integer.valueOf(cdatividadetipo));
			List<Atividadetipoitem> lista = atividadetipoitemService.findByTipo(tipo);
		
			StringBuilder html = new StringBuilder();
			if(lista != null && !lista.isEmpty()){
				html.append("<option value='<null>'></option>");
				for(Atividadetipoitem ati : lista){
					html.append("<option value='br.com.linkcom.sined.geral.bean.Atividadetipoitem[cdatividadetipoitem=")
						.append(ati.getCdatividadetipoitem().toString())
						.append("]'>")
						.append(ati.getNome())
						.append("</option>");
				}
			}
			else{
				msgError = "N�o existem dados para este tipo.";
				return json.addObject("msgError", msgError);
			}
			
			return json.addObject("html", html.toString()).addObject("msgError", msgError);
		}
		else {
			msgError = "Tipo n�o definido";
			return json.addObject("msgError", msgError);
		}
	}
	
	/**
	 * M�todo que fatura requisi��es;
	 * 
	 * 
	 * @param request
	 * @param requisicao
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView faturar(WebRequestContext request, Requisicao requisicao){
		String faturarMedicao = request.getParameter("faturarMedicao");
		boolean medicao = faturarMedicao != null && faturarMedicao.trim().toLowerCase().equals("true");
		
		String whereIn = request.getParameter("selectedItens");
		String msgError = "";
		
		List<Requisicao> listaRequisicoes = requisicaoService.getListaRequisicoesFaturarPorAutorizacao(requisicaoService.carregaRequisicaoForFaturar(whereIn), requisicao.getListaRequisicoesFaturarPorAutorizacao(), medicao);
		if(listaRequisicoes != null && !listaRequisicoes.isEmpty()){
			msgError += requisicaoService.validaRequisicoesParaFaturarAutorizacao(listaRequisicoes);
		}else {
			msgError += "Nenhuma requisi��o foi selecionada.";
		}
			
		if(!msgError.equals("")){
			request.addError(msgError);
			if(request.getParameter("entrada") != null && request.getParameter("entrada").toUpperCase().equals("TRUE"))
				SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao?ACAO=consultar&cdrequisicao="+whereIn);
			else
				SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
			return null;
		}
		
		List<NotaFiscalServico> listaNotas = requisicaoService.criaNotasByAutorizacao(listaRequisicoes, medicao);
		if(listaNotas == null || listaNotas.isEmpty()){
			request.addError("N�o existem registros para a autoriza��o fornecida.");
			SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao?ACAO=listagem");
			return null;
		}
		requisicaoService.saveFaturarByAutorizacao(whereIn, listaNotas, listaRequisicoes, medicao);

		request.addMessage("Registros faturados com sucesso!");
		SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao?ACAO=listagem");
		return null;
	}

	/**
	 * M�todo que abre popup de faturamento
	 *
	 * @param request
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView doFaturar(WebRequestContext request){
		String faturarMedicao = request.getParameter("faturarMedicao");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhuma ordem de servi�o selecionada.");
			SinedUtil.fechaPopUp(request);
			return new ModelAndView("/servicointerno/crud/Requisicao");
		}
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("faturarMedicao", faturarMedicao);
		
		boolean medicao = faturarMedicao != null && faturarMedicao.trim().toLowerCase().equals("true");
		
		List<Requisicao> listaRequisicao = requisicaoService.carregaRequisicaoForFaturarAutorizacao(whereIn, medicao);
		for (Requisicao requisicao : listaRequisicao) {
			List<Materialrequisicao> listaMateriaisrequisicao = requisicao.getListaMateriaisrequisicao();
			if(listaMateriaisrequisicao != null && listaMateriaisrequisicao.size() > 0){
				for (Materialrequisicao materialrequisicao : listaMateriaisrequisicao) {
					double valorfaturado = 0;
					if(materialrequisicao.getPercentualfaturado() != null){
						valorfaturado = materialrequisicao.getValortotal() * (materialrequisicao.getPercentualfaturado()/100d);
					}
					materialrequisicao.setValorfaturado(valorfaturado);
				}
			}
			
			if(medicao){
				Collections.sort(listaMateriaisrequisicao, new Comparator<Materialrequisicao>(){
					public int compare(Materialrequisicao o1, Materialrequisicao o2) {
						return materialrequisicaoService.compareWithItem(o1, o2);
					}
				});
			} else {
				Collections.sort(listaMateriaisrequisicao, new Comparator<Materialrequisicao>(){
					public int compare(Materialrequisicao o1, Materialrequisicao o2) {
						return materialrequisicaoService.compareWithAutorizacao(o1, o2);
					}
				});
			}
		}
		Requisicao requisicao = new Requisicao();
		requisicao.setListaRequisicoesFaturarPorAutorizacao(listaRequisicao);
		
		String erro = null;
		if(requisicao.getListaRequisicoesFaturarPorAutorizacao() == null || 
				requisicao.getListaRequisicoesFaturarPorAutorizacao().isEmpty()){
			erro = "N�o existe itens para faturar.";
		}else {
			for(Requisicao r : requisicao.getListaRequisicoesFaturarPorAutorizacao()){
				if(r.getRequisicaoestado() == null || 
						r.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.FATURADO) || 
						r.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CANCELADA)){
					erro = "V�lido somente para requisi��es com situa��o diferente de \"Cancelada\" e \"Faturada\".";
				}else if(r.getListaMateriaisrequisicao() == null || r.getListaMateriaisrequisicao().isEmpty()){
					erro = "V�lido somente para requisi��es com Materiais e Servi�os.";
				}
				if(StringUtils.isNotEmpty(erro))
					break;
			}
		}
		
		if (erro==null){
			for (Requisicao r: requisicao.getListaRequisicoesFaturarPorAutorizacao()){
				for (Materialrequisicao materialrequisicao: r.getListaMateriaisrequisicao()){
					if (materialrequisicao.getAutorizacaoFaturamento()==null || materialrequisicao.getAutorizacaoFaturamento().trim().equals("")){
						erro = "Ordem de servi�o possui material(is) sem autoriza��o para faturamento preenchido.";
						break;
					}
				}
			}
		}
		
		if(StringUtils.isNotEmpty(erro)){
			request.addError(erro);
			if(request.getParameter("entrada") != null && request.getParameter("entrada").toUpperCase().equals("TRUE"))
				SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao?ACAO=consultar&cdrequisicao="+whereIn);
			else
				SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
			return null;
		}
		return new ModelAndView("direct:/crud/popup/faturar").addObject("bean", requisicao);
	}
	
	/**
	 * M�todo que abre popup de faturamento de Produtos/Servicos em notas fiscais
	 *
	 * @param request
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView doFaturarNotaFiscal(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		String tipoFaturamento = request.getParameter("tipoFaturamento");
		String error = null;
		
		if(whereIn == null || "".equals(whereIn)){
			error = "Nenhuma ordem de servi�o selecionada.";
		}else if(tipoFaturamento == null || tipoFaturamento.trim().isEmpty()){
			error = "N�o foi poss�vel identificar o tipo de faturamento.";
		} 
		if(error == null){
			error = requisicaoService.validaRequisicaoForFaturarNotaFiscal(whereIn, tipoFaturamento);
		}
		if(error != null){
			request.addError(error);
			SinedUtil.fechaPopUp(request);
			return null;
		}
		request.setAttribute("whereInRequisicao", whereIn);
		return new ModelAndView("direct:/crud/popup/faturarNotaFiscal")
					.addObject("tipoFaturamento", tipoFaturamento)
					.addObject("fromOrdemServico", Boolean.TRUE)
					.addObject("tipoFaturamentoDescricao", "Servico".equals(tipoFaturamento) ? "Servi�o" : tipoFaturamento );
	}
	
	/**
	 * 
	 * @param request
	 * @param requisicao
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView ajaxVerificaPendenciaFinanceira(WebRequestContext request, Requisicao requisicao){
		boolean existePendenciaFinaneceira = false;
		Integer diasAtraso = 0;
		
		if (requisicao.getCliente()!=null && requisicao.getCliente().getCdpessoa()!=null){
			Date dtvencimento = documentoService.getPrimeiroVencimentoDocumentoAtrasado(requisicao.getCliente());	
			if (dtvencimento != null){
				existePendenciaFinaneceira = true;
				diasAtraso = SinedDateUtils.diferencaDias(SinedDateUtils.currentDateToBeginOfDay(), dtvencimento);
			}
		}
		
		return new JsonModelAndView().addObject("existePendenciaFinaneceira", existePendenciaFinaneceira)
									 .addObject("diasAtraso", diasAtraso);
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView verificarModelosOS(WebRequestContext request) throws Exception {
		
		String selectedItens = SinedUtil.getItensSelecionados(request);
		Set<Integer> listaCdreporttemplate = new HashSet<Integer>();
		
		if (selectedItens!=null && !selectedItens.trim().equals("")){
			List<Atividadetipomodelo> listaModelos = atividadetipomodeloService.findForEmitirOS(selectedItens);
			for (Atividadetipomodelo modelo: listaModelos){
				listaCdreporttemplate.add(modelo.getReportTemplate().getCdreporttemplate());
			}
		}		
		
		return new JsonModelAndView().addObject("listaCdreporttemplate", listaCdreporttemplate);
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView escolherModeloOS(WebRequestContext request, RequisicaoFiltro filtro){
		
		String selectedItens = SinedUtil.getItensSelecionados(request);
		
		if (selectedItens==null || selectedItens.trim().equals("")){
			return null;
		}
		
		List<Atividadetipomodelo> listaModelo = atividadetipomodeloService.findForEmitirOS(selectedItens);
		Map<Atividadetipo, Set<ReportTemplateBean>> mapa = new HashMap<Atividadetipo, Set<ReportTemplateBean>>();
		
		for (Atividadetipomodelo modelo: listaModelo){
			Set<ReportTemplateBean> listaAux = mapa.get(modelo.getAtividadetipo());
			if (listaAux==null){
				listaAux = new HashSet<ReportTemplateBean>();
			}
			listaAux.add(modelo.getReportTemplate());
			mapa.put(modelo.getAtividadetipo(), listaAux);
		}
		
		List<EscolherModeloOSBean> listaEscolherModeloOSBean = new ArrayList<EscolherModeloOSBean>();
		
		for (Atividadetipo atividadetipo: mapa.keySet()){
			listaEscolherModeloOSBean.add(new EscolherModeloOSBean(atividadetipo));
		}
		
		filtro.setListaEscolherModeloOSBean(listaEscolherModeloOSBean);
		
		request.setAttribute("titulo", "Escolher Modelo");
		request.setAttribute("selectedItens", selectedItens);
		request.setAttribute("mapa", mapa);
		
		return new ModelAndView("direct:/crud/popup/escolherModeloOS").addObject("filtro", filtro);
	}
	
	/**
	 * 
	 * @param bean
	 * @author Thiago Clemente
	 * 
	 */
	private void setProximonumeroordemservico(Requisicao bean){
		if (bean.getCdrequisicao()==null && bean.getEmpresa()!=null && (bean.getOrdemservico()==null || bean.getOrdemservico().trim().equals(""))){
			Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.proximonumeroordemservico");
			Integer proximonumeroordemservico = empresa.getProximonumeroordemservico();
			if (proximonumeroordemservico!=null){
				bean.setOrdemservico(proximonumeroordemservico.toString());
				proximonumeroordemservico++;
				empresaService.updateProximonumeroordemservico(empresa, proximonumeroordemservico);
			}
		}
	}
	
	public ModelAndView ajaxPrazoConclusao (WebRequestContext request, Requisicao bean) {
		Integer prazoconclusao = atividadetipoService.findPrazoConclusao(bean.getAtividadetipo()).getPrazoconclusao();
		String previsao = "<null>";
		Calendar c = new GregorianCalendar();
		if (prazoconclusao != null) {
			c.setTime(SinedDateUtils.currentDate());
			c.add(Calendar.DAY_OF_MONTH, prazoconclusao);
			previsao = new SimpleDateFormat("dd/MM/yyyy").format(c.getTime());
		}
		return new JsonModelAndView().addObject("previsao", previsao);
	}
	
	public ModelAndView habilitaDesabilitaLink(WebRequestContext webRequestContext){
		String cdcontrato = webRequestContext.getParameter("contrato");
		String url ="";
		boolean redirecionaUrl = false;
		Usuario usuario = usuarioService.carregarUsuarioByLogin("admin");
		if(!cdcontrato.equals("<null>")){
			String [] string = cdcontrato.split("cdcontrato=");
			Contrato contrato = new Contrato();
			contrato.setCdcontrato(Integer.parseInt(string[1].replace("]", "")));
			url = contratoService.carregaUrlSistema(contrato);
			String parametroW3mpsbr = parametrogeralService.buscaValorPorNome(Parametrogeral.W3MPSBR_INTEGRACAO);
			if (parametroW3mpsbr != null && parametroW3mpsbr.toUpperCase().equals("TRUE") && url != null && !url.equals("")){
				redirecionaUrl = true;
			}
		}
		return new JsonModelAndView().addObject("url",url)
			.addObject("urlW3erp", url != null && url.contains("w3erp"))
			.addObject("redirecionaUrl", redirecionaUrl)
			.addObject("usuario", usuario.getLogin())
			.addObject("senha", usuario.getSenha());
	}
	
	public ModelAndView buscaVersaoSistema(WebRequestContext webRequestContext, Requisicao requisicao){
		String versaoAtual = "";
		if(requisicao.getCliente() != null && requisicao.getCliente().getCdpessoa() != null && requisicao.getContrato() != null && requisicao.getContrato().getCdcontrato() != null){		
			ClienteBancoDados clienteBancoDados = clienteBancoDadosService.findForVersaoAtual(requisicao.getCliente().getCdpessoa(), requisicao.getContrato().getCdcontrato()); 
			versaoAtual = clienteBancoDados != null && clienteBancoDados.getVersaoatual()!= null && StringUtils.isNotBlank(clienteBancoDados.getVersaoatual().toString()) ? "Vers�o atual: " + clienteBancoDados.getVersaoatual().toString() : "";
		}
		return new JsonModelAndView().addObject("versaoAtual",versaoAtual);
	}
	
	public ModelAndView registrarAcessoBaseCliente(WebRequestContext context, Requisicao requisicao){
		
		Requisicaohistorico requisicaohistorico = new Requisicaohistorico();
		requisicaohistorico.setRequisicao(requisicao);
		requisicaohistorico.setObservacao("Acesso � base do cliente para suporte.");
		requisicaohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
		requisicaohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		requisicaohistoricoService.saveOrUpdate(requisicaohistorico);
		
		return new JsonModelAndView().addObject("sucesso", true);
	}
	
	/**
	 * M�todo que realiza a a��o enviar email
	 * @param request
	 * @throws Exception
	 */
	public void enviarEmail(WebRequestContext request) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request).toString();
		List<String> listaErro = new ArrayList<String>();
		int contEmalSucesso = 0;
		if (whereIn != null && !"".equals(whereIn)) {
			List<Requisicao> listaRequisicao = new ArrayList<Requisicao>();
			listaRequisicao = requisicaoService.findForEnviarEmail(whereIn);

			if(listaRequisicao != null && !listaRequisicao.isEmpty()){
				StringBuilder htmltext = new StringBuilder();
				String emailCliente = "";
				Empresa empresa = empresaService.loadPrincipal();
				String ultimaObservacao = "";
				for (Requisicao requisicao : listaRequisicao) {
					if(requisicao.getCliente() != null && requisicao.getCliente().getEmail() != null){
						if(requisicao.getListaRequisicaohistorico() != null && requisicao.getListaRequisicaohistorico().size() > 0){
							ultimaObservacao = requisicao.getListaRequisicaohistorico().get(0).getObservacao();
						}
						emailCliente = requisicao.getCliente().getEmail();
						htmltext = new StringBuilder("Altera��o na Ordem de Servi�o: <P>");
						htmltext
						.append("OS : " +requisicao.getCdrequisicao()).append(requisicao.getDescricao() != null ? " - " + requisicao.getDescricao() : "")
						.append("<BR>")
						.append("Data de Previs�o " + (requisicao.getDtprevisao() != null ? requisicao.getDtprevisao() : " - "))
						.append("<BR>")
						.append("Observa��o : " + ultimaObservacao)
						.append("<BR><BR>")
						.append(""+ (requisicao.getAtividadetipo() != null && requisicao.getAtividadetipo().getMensagememail() != null ? requisicao.getAtividadetipo().getMensagememail() : ""));
						
						EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
						try {
							email
							.setFrom(empresa.getEmail())
							.setSubject("W3erp - Altera��o na OS")
							.setTo(emailCliente)
							.addHtmlText(htmltext.toString())
							.sendMessage();	
							contEmalSucesso++;
						} catch (Exception e) {
							listaErro.add("N�o foi possivel enviar o email para o cliente "+ requisicao.getCliente().getNome() + ".");
						}
					}						
				}
				if(listaErro.size() > 0){
					for(String msg : listaErro){
						request.addError(msg);					
					}
					SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
				}else if (contEmalSucesso>0){
					request.addMessage("E-mail enviado.");				
					SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
				}else {
					request.addMessage("Nenhum e-mail enviado.");				
					SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");					
				}
			}
		}else{
			SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
		}
	}
	
	/**
	* M�todo que inicia uma atividade
	*
	* @param request
	* @param requisicao
	* @since 21/10/2015
	* @author Luiz Fernando
	*/
	public void startStopAtividade(WebRequestContext request, Requisicao requisicao){
		try {
			Registroatividadehora registroatividadehora = registroatividadehoraService.findByUsuario(SinedUtil.getUsuarioComoColaborador());					
			
			if(registroatividadehora != null){
				throw new SinedException("N�o � poss�vel ter duas atividades simult�neas em andamento. J� existe atividade aberta para a Ordem de Servi�o " + registroatividadehora.getRequisicao().getCdrequisicao() + ".");
			} 
			registroatividadehora = new Registroatividadehora();
			
			registroatividadehora.setRequisicao(requisicao);
			registroatividadehora.setColaborador(SinedUtil.getUsuarioComoColaborador());
			registroatividadehora.setInicio(new Timestamp(System.currentTimeMillis()));
			registroatividadehoraService.saveOrUpdate(registroatividadehora);
				
			View.getCurrent().println("var sucesso = true;");
		} catch (Exception e) {
			View.getCurrent().println("var sucesso = false;");
			View.getCurrent().println("var msg = '" + e.getMessage() + "';");
			e.printStackTrace();
		}
	}
	
	/**
	* M�todo que abre uma popup para registrar a atividade
	*
	* @param request
	* @param requisicao
	* @return
	* @since 21/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView openConfirmacaoRegistroatividadehora(WebRequestContext request, Requisicao requisicao){
		Registroatividadehora registroatividadehora = registroatividadehoraService.findByUsuario(SinedUtil.getUsuarioComoColaborador());	
		ConfirmacaoRegistroatividadehoraBean bean = new ConfirmacaoRegistroatividadehoraBean();
		Atividadetipo atividadetipo = requisicao.getAtividadetipo();
		if(registroatividadehora != null){
			bean.setListaItens(registroatividadehoraService.makeListaRegistroatividadehora(atividadetipo, registroatividadehora.getInicio()));
			
			bean.setRequisicao(requisicao);
			bean.setRegistroatividadehora(registroatividadehora);
			bean.setColaborador(SinedUtil.getUsuarioComoColaborador());
			
			requisicao = requisicaoService.load(requisicao);
			request.setAttribute("cdrequisicao", requisicao.getCdrequisicao()); 
			request.setAttribute("descricao", requisicao.getDescricao()); 
		}
		
		request.setAttribute("atividadetipoRequisicao", atividadetipo != null ? "br.com.linkcom.sined.geral.bean.Atividadetipo[cdatividadetipo="+atividadetipo.getCdatividadetipo()+"]" : "");
		request.setAttribute("dataAtual", new SimpleDateFormat("dd/MM/yyyy").format(new Date(System.currentTimeMillis())));
		return new ModelAndView("direct:/crud/popup/confirmacaoRegistroatividadehoraPopup", "bean", bean);
	}
	
	/**
	* M�todo que salva e retorna os apontamentos
	*
	* @param request
	* @param bean
	* @return
	* @since 21/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView salvarRegistroatividadehora(WebRequestContext request, ConfirmacaoRegistroatividadehoraBean bean){
		List<ConfirmacaoRegistroatividadehoraItemBean> listaItens = bean.getListaItens();
		Requisicao requisicao = requisicaoService.loadForEntrada(bean.getRequisicao());
		
		Apontamento apontamento;
		ApontamentoHoras apontamentohoras;
		for (ConfirmacaoRegistroatividadehoraItemBean b : listaItens) {
			
			apontamento = new Apontamento();
			apontamento.setDescricao(b.getDescricao());
			apontamento.setAtividadetipo(b.getAtividadetipo());
			apontamento.setDtapontamento(b.getData());
			apontamento.setColaborador(bean.getColaborador());
			apontamento.setApontamentoTipo(new ApontamentoTipo(1));
			apontamento.setRequisicao(requisicao);
			
			List<ApontamentoHoras> lista = new ArrayList<ApontamentoHoras>();
			apontamentohoras = new ApontamentoHoras();
			apontamentohoras.setHrinicio(b.getInicio());
			apontamentohoras.setHrfim(b.getFim());
			lista.add(apontamentohoras);
			apontamento.setListaApontamentoHoras(lista);
			
			double d = (apontamento.getListaApontamentoHoras().get(0).getHrfim().getTime() - apontamento.getListaApontamentoHoras().get(0).getHrinicio().getTime())/1000.0/60.0/60.0;
			d = Math.round(d*10000.0)/10000.0;
			apontamento.setQtdehoras(d);
			
			apontamentoService.saveOrUpdate(apontamento);
			b.setCdapontamento(apontamento.getCdapontamento());
			b.setCdapontamentohoras(apontamentohoras.getCdapontamentohoras());
		}
		
		if (bean.getRegistroatividadehora() != null) {
			registroatividadehoraService.delete(bean.getRegistroatividadehora());
		}
		
		return new JsonModelAndView().addObject("bean", bean);
	}
	
	public ModelAndView abrirPopUpInformacaoClienteOrdemServico(WebRequestContext request, Cliente cliente) {
		return requisicaoService.abrirPopUpInformacaoClienteOrdemServico(request);		
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteOrdemServico(WebRequestContext request){
		return requisicaoService.ajaxBuscaInformacaoClienteOrdemServico(request);
	} 
}