package br.com.linkcom.sined.modulo.servicointerno.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.UsuarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/servicointerno/crud/Pessoa", authorizationModule=CrudAuthorizationModule.class)
public class PessoaSelectCrud extends CrudControllerSined<UsuarioFiltro, Pessoa, Pessoa>{
	
	@Override
	protected void entrada(WebRequestContext request, Pessoa form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Pessoa bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Pessoa bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
}
