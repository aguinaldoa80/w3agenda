package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Escalahorario;

public class Agendamentoservicoapoio {

	public static final int QTDE_DIAS_TABELA = 7;
	
	protected Escalahorario escalahorario;
	protected List<Agendamentoservicoitemapoio> listaagendamentoservicoitemapoio = new ListSet<Agendamentoservicoitemapoio>(Agendamentoservicoitemapoio.class);
	
	protected String descriptionHour;
	
	public Agendamentoservicoapoio(){
	}

	public Agendamentoservicoapoio( Escalahorario escalahorario){
		this.escalahorario = escalahorario;
	}
	
	public Escalahorario getEscalahorario() {
		return escalahorario;
	}
	public List<Agendamentoservicoitemapoio> getListaagendamentoservicoitemapoio() {
		return listaagendamentoservicoitemapoio;
	}
	public String getDescriptionHour() {
		return escalahorario.getHorainicio() + " - " + escalahorario.getHorafim();
	}

	public void setEscalahorario(Escalahorario escalahorario) {
		this.escalahorario = escalahorario;
	}
	public void setListaagendamentoservicoitemapoio(
			List<Agendamentoservicoitemapoio> listaagendamentoservicoitemapoio) {
		this.listaagendamentoservicoitemapoio = listaagendamentoservicoitemapoio;
	}
	public void setDescriptionHour(String descriptionHour) {
		this.descriptionHour = descriptionHour;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Agendamentoservicoapoio)
			return ((Agendamentoservicoapoio)obj).getEscalahorario().equals(this.getEscalahorario());
		return super.equals(obj);
	}
	
}
