package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProntuarioFiltro extends FiltroListagemSined {
	
	private Colaborador terapeuta;
	private Cliente paciente;
	private Date dtinicio;
	private Date dtfim;
	
	@DisplayName("Terapeuta")
	public Colaborador getTerapeuta() {
		return terapeuta;
	}
	@DisplayName("Paciente")
	public Cliente getPaciente() {
		return paciente;
	}
	@DisplayName("Data (in�cio)")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data (fim)")
	public Date getDtfim() {
		return dtfim;
	}
	
	public void setTerapeuta(Colaborador terapeuta) {
		this.terapeuta = terapeuta;
	}
	public void setPaciente(Cliente paciente) {
		this.paciente = paciente;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}	
}