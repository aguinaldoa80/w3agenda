package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetipoitem;
import br.com.linkcom.sined.geral.bean.Classificacao;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Requisicaoprioridade;
import br.com.linkcom.sined.geral.bean.Tipoiteracao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.TipoPesquisa;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EscolherModeloOSBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RequisicaoFiltro extends FiltroListagemSined {

	protected String nomeCliente;
	protected TipoPesquisa tipoPesquisa = TipoPesquisa.NOME_COMPLETO;
	protected String ordemservico;
	protected Projeto projeto;
	protected Cliente cliente;
	protected Contrato contrato;
	protected Date dtinicio;
	protected Date dtfim;
	protected Date dtinicioprevisao;
	protected Date dtfimprevisao;
	protected Requisicaoprioridade requisicaoprioridade;
	protected Empresa empresa;
	protected Colaborador colaboradorresponsavel;
	protected List<Requisicaoestado> listasituacoes;
	protected String mostrar;
	protected String materialservico;
	protected String contato;
	protected Timestamp dtiniciodisponibilizacao;
	protected Timestamp dtfimdisponibilizacao;
	protected Timestamp dtinicioretirada;
	protected Timestamp dtfimretirada;
	protected List<Requisicaoprioridade> listaRequisicaoprioridade;
	protected Boolean mostrarAutorizacao;
	protected String descricao;
	protected Atividadetipo tipo;
	protected String observacao;
	protected Atividadetipoitem atividadetipoitem;
	protected String valor;
	protected Date dtinicioconclusao;
	protected Date dtfimconclusao;
	protected List<EscolherModeloOSBean> listaEscolherModeloOSBean;
	protected Money total;
	protected Usuario usuariohistorico;
	protected Tipoiteracao tipoiteracao;
	protected Meiocontato meiocontato;
	protected String totalHorasTrabalhadas;
	protected Boolean exibirTotais = false;
	protected Tipopessoa tipoPessoa;
	protected Classificacao classificacao;

	protected Cpf cpf;
	protected Cnpj cnpj;
	
	public RequisicaoFiltro(){
		this.colaboradorresponsavel = SinedUtil.getUsuarioComoColaborador();
		
		if(this.getListasituacoes() == null){
			this.setListasituacoes(new ArrayList<Requisicaoestado>());
			this.getListasituacoes().add(new Requisicaoestado(Requisicaoestado.EMESPERA));
			this.getListasituacoes().add(new Requisicaoestado(Requisicaoestado.EMANDAMENTO));
			this.getListasituacoes().add(new Requisicaoestado(Requisicaoestado.EMTESTE));
			this.getListasituacoes().add(new Requisicaoestado(Requisicaoestado.AGUARDANDO_RESPOSTA));
		}
	}
	
	public String getNomeCliente() {
		return nomeCliente;
	}
	@DisplayName("Tipo de Pesquisa")
	public TipoPesquisa getTipoPesquisa() {
		return tipoPesquisa;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	public Contrato getContrato() {
		return contrato;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Date getDtinicioprevisao() {
		return dtinicioprevisao;
	}
	public Date getDtfimprevisao() {
		return dtfimprevisao;
	}
	public List<EscolherModeloOSBean> getListaEscolherModeloOSBean() {
		return listaEscolherModeloOSBean;
	}
	@DisplayName("Prioridade")
	public Requisicaoprioridade getRequisicaoprioridade() {
		return requisicaoprioridade;
	}
	@DisplayName("Respons�vel")
	public Colaborador getColaboradorresponsavel() {
		return colaboradorresponsavel;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Ordem de Servi�o")
	public String getOrdemservico() {
		return ordemservico;
	}
	
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	
	public void setTipoPesquisa(TipoPesquisa tipoPesquisa) {
		this.tipoPesquisa = tipoPesquisa;
	}
	public void setOrdemservico(String ordemservico) {
		this.ordemservico = ordemservico;
	}
	@MaxLength(100)
	@DisplayName("Materiais e Servi�os")
	public String getMaterialservico() {
		return materialservico;
	}
	@MaxLength(100)
	public String getContato() {
		return contato;
	}
	
	@DisplayName("Mostrar")
	public Boolean getMostrarAutorizacao() {
		return mostrarAutorizacao;
	}
	
	@DisplayName("Valor Total")
	public Money getTotal() {
		return total;
	}
	
	@DisplayName("Requisitante")
	public Usuario getUsuariohistorico() {
		return usuariohistorico;
	}

	public void setUsuariohistorico(Usuario usuariohistorico) {
		this.usuariohistorico = usuariohistorico;
	}

	public void setTotal(Money total) {
		this.total = total;
	}

	public void setMostrarAutorizacao(Boolean mostrarAutorizacao) {
		this.mostrarAutorizacao = mostrarAutorizacao;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}
	public void setRequisicaoprioridade(Requisicaoprioridade requisicaoprioridade) {
		this.requisicaoprioridade = requisicaoprioridade;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setDtinicioprevisao(Date dtinicioprevisao) {
		this.dtinicioprevisao = dtinicioprevisao;
	}
	public void setDtfimprevisao(Date dtfimprevisao) {
		this.dtfimprevisao = dtfimprevisao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setColaboradorresponsavel(Colaborador colaboradorresponsavel) {
		this.colaboradorresponsavel = colaboradorresponsavel;
	}
	@DisplayName("Situa��o")
	public List<Requisicaoestado> getListasituacoes() {
		return listasituacoes;
	}
	public void setListasituacoes(List<Requisicaoestado> listasituacoes) {
		this.listasituacoes = listasituacoes;
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	@DisplayName("Mostrar")
	public String getMostrar() {
		return mostrar;
	}
	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}
	public void setMaterialservico(String materialservico) {
		this.materialservico = materialservico;
	}

	public Timestamp getDtiniciodisponibilizacao() {
		return dtiniciodisponibilizacao;
	}

	public Timestamp getDtfimdisponibilizacao() {
		return dtfimdisponibilizacao;
	}

	public Timestamp getDtinicioretirada() {
		return dtinicioretirada;
	}

	public Timestamp getDtfimretirada() {
		return dtfimretirada;
	}
	
	@DisplayName("Horas Trabalhadas")
	public String getTotalHorasTrabalhadas() {
		return totalHorasTrabalhadas;
	}
	
	@DisplayName("Exibir Totais")
	public Boolean getExibirTotais() {
		return exibirTotais;
	}

	public void setDtiniciodisponibilizacao(Timestamp dtiniciodisponibilizacao) {
		this.dtiniciodisponibilizacao = dtiniciodisponibilizacao;
	}

	public void setDtfimdisponibilizacao(Timestamp dtfimdisponibilizacao) {
		this.dtfimdisponibilizacao = dtfimdisponibilizacao;
	}

	public void setDtinicioretirada(Timestamp dtinicioretirada) {
		this.dtinicioretirada = dtinicioretirada;
	}

	public void setDtfimretirada(Timestamp dtfimretirada) {
		this.dtfimretirada = dtfimretirada;
	}
	@DisplayName("Prioridade")
	public List<Requisicaoprioridade> getListaRequisicaoprioridade() {
		return listaRequisicaoprioridade;
	}

	public void setListaRequisicaoprioridade(
			List<Requisicaoprioridade> listaRequisicaoprioridade) {
		this.listaRequisicaoprioridade = listaRequisicaoprioridade;
	}

	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Atividadetipo getTipo() {
		return tipo;
	}

	public void setTipo(Atividadetipo tipo) {
		this.tipo = tipo;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@DisplayName("Campo Adicional")
	public Atividadetipoitem getAtividadetipoitem() {
		return atividadetipoitem;
	}

	public void setAtividadetipoitem(Atividadetipoitem atividadetipoitem) {
		this.atividadetipoitem = atividadetipoitem;
	}

	@DisplayName("Conte�do")
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Date getDtinicioconclusao() {
		return dtinicioconclusao;
	}
	public Date getDtfimconclusao() {
		return dtfimconclusao;
	}

	public void setDtinicioconclusao(Date dtinicioconclusao) {
		this.dtinicioconclusao = dtinicioconclusao;
	}
	public void setDtfimconclusao(Date dtfimconclusao) {
		this.dtfimconclusao = dtfimconclusao;
	}
	public void setListaEscolherModeloOSBean(List<EscolherModeloOSBean> listaEscolherModeloOSBean) {
		this.listaEscolherModeloOSBean = listaEscolherModeloOSBean;
	}
	
	@DisplayName("Tipo de itera��o")
	public Tipoiteracao getTipoiteracao() {
		return tipoiteracao;
	}
	public void setTipoiteracao(Tipoiteracao tipoiteracao) {
		this.tipoiteracao = tipoiteracao;
	}
	
	public void setTotalHorasTrabalhadas(String totalHorasTrabalhadas) {
		this.totalHorasTrabalhadas = totalHorasTrabalhadas;
	}
	
	public void setExibirTotais(Boolean exibirTotais) {
		this.exibirTotais = exibirTotais;
	}
	
	@DisplayName("Meio de contato")
	public Meiocontato getMeiocontato() {
		return meiocontato;
	}
	public void setMeiocontato(Meiocontato meiocontato) {
		this.meiocontato = meiocontato;
	}
	
	@DisplayName("Tipo de Pessoa")
	public Tipopessoa getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(Tipopessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public Classificacao getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}
}