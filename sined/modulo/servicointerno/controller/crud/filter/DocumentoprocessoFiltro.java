package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Documentoprocessosituacao;
import br.com.linkcom.sined.geral.bean.Documentoprocessotipo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;


public class DocumentoprocessoFiltro extends FiltroListagemSined{
	
	protected Integer numDocumento;
	protected List<Documentoprocessosituacao> listasituacao = getListaSituacaoDefault();
	protected Documentoprocessotipo tipo;
	protected Usuario responsavel;
	protected String titulo;
	protected String aprovadopor;
	protected Date inicio;
	protected Date fim;
	protected Date inicioVencimento;
	protected Date fimVencimento;
	protected Projeto projeto;
	protected Material material;
	
	
	@DisplayName("N�mero do documento")
	@MaxLength(10)
	public Integer getNumDocumento() {
		return numDocumento;
	}
	
	@DisplayName("Situa��o")
	public List<Documentoprocessosituacao> getListasituacao() {
		return listasituacao;
	}
	
	public Documentoprocessotipo getTipo() {
		return tipo;
	}
	
	@DisplayName("Respons�vel")
	public Usuario getResponsavel() {
		return responsavel;
	}
	
	@MaxLength(100)
	@DisplayName("T�tulo")
	public String getTitulo() {
		return titulo;
	}
	@MaxLength(100)
	@DisplayName("Aprovado por")
	public String getAprovadopor() {
		return aprovadopor;
	}
	
	@DisplayName("De")
	public Date getInicio() {
		return inicio;
	}
	
	@DisplayName("At�")
	public Date getFim() {
		return fim;
	}
	public void setNumDocumento(Integer numDocumento) {
		this.numDocumento = numDocumento;
	}
	public void setListasituacao(List<Documentoprocessosituacao> listaSituacao) {
		this.listasituacao = listaSituacao;
	}
	public void setTipo(Documentoprocessotipo tipo) {
		this.tipo = tipo;
	}
	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setAprovadopor(String aprovadopor) {
		this.aprovadopor = aprovadopor;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public void setFim(Date fim) {
		this.fim = fim;
	}
	
	@DisplayName("De")
	public Date getInicioVencimento() {
		return inicioVencimento;
	}
	public void setInicioVencimento(Date inicioVencimento) {
		this.inicioVencimento = inicioVencimento;
	}
	
	@DisplayName("At�")
	public Date getFimVencimento() {
		return fimVencimento;
	}
	public void setFimVencimento(Date fimVencimento) {
		this.fimVencimento = fimVencimento;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	private List<Documentoprocessosituacao> getListaSituacaoDefault(){
		ArrayList<Documentoprocessosituacao> lista =
			new ArrayList<Documentoprocessosituacao>();
		lista.add(Documentoprocessosituacao.DISPONIVEL);
		lista.add(Documentoprocessosituacao.EM_APROVACAO);
		return lista;
	}

	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
}
