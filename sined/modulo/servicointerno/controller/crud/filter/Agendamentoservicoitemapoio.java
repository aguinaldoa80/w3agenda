package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.util.SinedDateUtils;

public class Agendamentoservicoitemapoio {

	protected Date data;
	protected String dataAux;
	protected List<Agendamentoservico> listaAgendamentos;
	
	protected Boolean isFeriado = false;
	protected Boolean isDomingo = false;
	protected Boolean isHorarioDisponivel = true;
	protected String motivo;
	
	protected Escalahorario escalahorarioAux;

	public Agendamentoservicoitemapoio(){
	}

	public Agendamentoservicoitemapoio(Date data){
		this.data = data;
	}

	public Agendamentoservicoitemapoio(Date data, List<Agendamentoservico> agendamentos, Boolean isDomingo, Boolean isHorarioDisponivel, Escalahorario escalahorarioAux, Boolean isFeriado, String motivo){
		this.data = data;
		this.dataAux = new SimpleDateFormat("dd/MM/yy").format(data)+" - "+SinedDateUtils.stringDayOfWeek(data);
		this.listaAgendamentos = agendamentos;
		this.isDomingo = isDomingo;
		this.isHorarioDisponivel = isHorarioDisponivel;
		this.escalahorarioAux = escalahorarioAux;
		this.isFeriado = isFeriado;
		this.motivo = motivo;
	}

	public Date getData() {
		return data;
	}
	public List<Agendamentoservico> getListaAgendamentos() {
		return listaAgendamentos;
	}
	public String getDataAux() {
		return dataAux;
	}
	public Boolean getIsFeriado() {
		return isFeriado;
	}
	public Boolean getIsDomingo() {
		return isDomingo;
	}
	public Boolean getIsHorarioDisponivel() {
		return isHorarioDisponivel;
	}
	public Escalahorario getEscalahorarioAux() {
		return escalahorarioAux;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public void setEscalahorarioAux(Escalahorario escalahorarioAux) {
		this.escalahorarioAux = escalahorarioAux;
	}
	public void setIsHorarioDisponivel(Boolean isHorarioDisponivel) {
		this.isHorarioDisponivel = isHorarioDisponivel;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setListaAgendamentos(List<Agendamentoservico> listaAgendamentos) {
		this.listaAgendamentos = listaAgendamentos;
	}
	public void setDataAux(String dataAux) {
		this.dataAux = dataAux;
	}
	public void setIsDomingo(Boolean isDomingo) {
		this.isDomingo = isDomingo;
	}
	public void setIsFeriado(Boolean isFeriado) {
		this.isFeriado = isFeriado;
	}
	
}
