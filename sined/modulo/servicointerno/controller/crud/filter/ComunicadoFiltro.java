package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ComunicadoFiltro extends FiltroListagemSined{
	
	protected Integer numero;
	protected Date inicio;
	protected Date fim;
	protected String assunto;
	protected Colaborador remetente;
	protected Colaborador destinatario;
	
	
	@MaxLength(9)
	@DisplayName("Comunicado")
	public Integer getNumero() {
		return numero;
	}
	
	public Date getInicio() {
		return inicio;
	}
	
	public Date getFim() {
		return fim;
	}
	
	@MaxLength(100)
	public String getAssunto() {
		return assunto;
	}
	
	public Colaborador getRemetente() {
		return remetente;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	
	public void setFim(Date fim) {
		this.fim = fim;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	
	public void setRemetente(Colaborador remetente) {
		this.remetente = remetente;
	}

	@DisplayName("Destinatário")
	public Colaborador getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(Colaborador destinatario) {
		this.destinatario = destinatario;
	}

}
