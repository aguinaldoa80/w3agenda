package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Ausenciamotivo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.util.Log;


public class RegistrarausenciaFiltro implements Log{

	protected String motivo;
	protected Escalahorario horario;
	protected Date date;
	protected Agendamentoservico agendamentoServicoBean;
	protected List<Escalahorario> lista = new ListSet<Escalahorario>(Escalahorario.class);
	protected List<Agendamentoservicoitemapoio> listaAgendamentoServicoItemApoio;
	protected Colaborador colaborador;
	protected Ausenciamotivo ausenciamotivo;
	
	public Agendamentoservico getAgendamentoServicoBean() {
		return agendamentoServicoBean;
	}	
	public Date getDate() {
		return date;
	}
	public Escalahorario getHorario() {
		return horario;
	}
	public String getMotivo() {
		return motivo;
	}
	public List<Escalahorario> getLista(){
		return lista;
	}
	public List<Agendamentoservicoitemapoio> getListaAgendamentoServicoItemApoio(){
		return listaAgendamentoServicoItemApoio;
	}
	public Colaborador getColaborador(){
		return colaborador;
	}
	public Ausenciamotivo getAusenciamotivo(){
		return ausenciamotivo;
	}
	
	public void setAusenciamotivo(Ausenciamotivo ausenciamotivo){
		this.ausenciamotivo = ausenciamotivo;
	}
	public void setColaborador(Colaborador colaborador){
		this.colaborador = colaborador;
	}
	public void setListaAgendamentoServicoItemApoio(List<Agendamentoservicoitemapoio> listaAgendamentoServicoItemApoio) {
		this.listaAgendamentoServicoItemApoio = listaAgendamentoServicoItemApoio;
	}
	public void setLista(List<Escalahorario> lista){
		this.lista = lista;
	}
	public void setAgendamentoServicoBean(Agendamentoservico agendamentoServicoBean) {
		this.agendamentoServicoBean = agendamentoServicoBean;
	}
	public void setMotivo(String Motivo) {
		this.motivo = Motivo;
	}
	public void setHorario(Escalahorario horario) {
		this.horario = horario;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Integer getCdusuarioaltera() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Timestamp getDtaltera() {
		// TODO Auto-generated method stub
		return null;
	}
}
