package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.SolicitacaoServicoPessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicoprioridade;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicosituacao;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class SolicitacaoservicoFiltro extends FiltroListagemSined {
	
	private Integer cdsolicitacaoservico;
	private Timestamp dtenviode;
	private Timestamp dtenvioate;
	private Solicitacaoservicotipo solicitacaoservicotipo;
	private Usuario requisitante;
	private Colaborador requisitado;
	private Projeto projeto;
	private Solicitacaoservicoprioridade solicitacaoservicoprioridade;
	private Timestamp dtiniciode;
	private Timestamp dtinicioate;
	private Timestamp dtfimde;
	private Timestamp dtfimate;
	private SolicitacaoServicoPessoa solicitacaoServicoPessoa;
	private Cliente cliente;
	private Fornecedor fornecedor;
	private String outros;
	
	private List<Solicitacaoservicosituacao> listaSituacao;
	
	public SolicitacaoservicoFiltro() {
		requisitado = SinedUtil.getUsuarioComoColaborador();
		
		listaSituacao = new ArrayList<Solicitacaoservicosituacao>();
		listaSituacao.add(Solicitacaoservicosituacao.PENDENTE);
		listaSituacao.add(Solicitacaoservicosituacao.EM_ANDAMENTO);
//		listaSituacao.add(Solicitacaoservicosituacao.CONCLUIDA);
//		listaSituacao.add(Solicitacaoservicosituacao.CANCELADA);
	}
	
	@DisplayName("Solicitação")
	@MaxLength(9)
	public Integer getCdsolicitacaoservico() {
		return cdsolicitacaoservico;
	}
	public Timestamp getDtenviode() {
		return dtenviode;
	}
	public Timestamp getDtenvioate() {
		return dtenvioate;
	}
	@DisplayName("Tipo")
	public Solicitacaoservicotipo getSolicitacaoservicotipo() {
		return solicitacaoservicotipo;
	}
	public Usuario getRequisitante() {
		return requisitante;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Prioridade")
	public Solicitacaoservicoprioridade getSolicitacaoservicoprioridade() {
		return solicitacaoservicoprioridade;
	}
	public Timestamp getDtiniciode() {
		return dtiniciode;
	}
	public Timestamp getDtinicioate() {
		return dtinicioate;
	}
	public Timestamp getDtfimde() {
		return dtfimde;
	}
	public Timestamp getDtfimate() {
		return dtfimate;
	}
	@DisplayName("Situações")
	public List<Solicitacaoservicosituacao> getListaSituacao() {
		return listaSituacao;
	}
	public Colaborador getRequisitado() {
		return requisitado;
	}
	public void setRequisitado(Colaborador requisitado) {
		this.requisitado = requisitado;
	}
	public void setListaSituacao(List<Solicitacaoservicosituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	public void setDtiniciode(Timestamp dtiniciode) {
		this.dtiniciode = dtiniciode;
	}
	public void setDtinicioate(Timestamp dtinicioate) {
		this.dtinicioate = dtinicioate;
	}
	public void setDtfimde(Timestamp dtfimde) {
		this.dtfimde = dtfimde;
	}
	public void setDtfimate(Timestamp dtfimate) {
		this.dtfimate = dtfimate;
	}
	public void setCdsolicitacaoservico(Integer cdsolicitacaoservico) {
		this.cdsolicitacaoservico = cdsolicitacaoservico;
	}
	public void setDtenviode(Timestamp dtenviode) {
		this.dtenviode = dtenviode;
	}
	public void setDtenvioate(Timestamp dtenvioate) {
		this.dtenvioate = dtenvioate;
	}
	public void setSolicitacaoservicotipo(
			Solicitacaoservicotipo solicitacaoservicotipo) {
		this.solicitacaoservicotipo = solicitacaoservicotipo;
	}
	public void setRequisitante(Usuario requisitante) {
		this.requisitante = requisitante;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setSolicitacaoservicoprioridade(
			Solicitacaoservicoprioridade solicitacaoservicoprioridade) {
		this.solicitacaoservicoprioridade = solicitacaoservicoprioridade;
	}
	
	public SolicitacaoServicoPessoa getSolicitacaoServicoPessoa() {
		return solicitacaoServicoPessoa;
	}
	public void setSolicitacaoServicoPessoa(
			SolicitacaoServicoPessoa solicitacaoServicoPessoa) {
		this.solicitacaoServicoPessoa = solicitacaoServicoPessoa;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getOutros() {
		return outros;
	}

	public void setOutros(String outros) {
		this.outros = outros;
	}
}
