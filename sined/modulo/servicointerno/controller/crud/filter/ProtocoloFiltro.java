package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Providencia;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ProtocoloFiltro extends FiltroListagemSined{
	
	protected Integer numProtocolo;
	protected String assunto;
	protected Usuario remetente;
	protected Date  inicio;
	protected Date fim;
	protected Usuario destinatario;
	protected Providencia providencia;
	protected String titulo;
	
	@MaxLength(100)
	@DisplayName("T�tulo")
	public String getTitulo() {
		return titulo;
	}


	@DisplayName("N�mero do protocolo")
	public Integer getNumProtocolo() {
		return numProtocolo;
	}
	
	@MaxLength(100)
	public String getAssunto() {
		return assunto;
	}
	
	public Usuario getRemetente() {
		return remetente;
	}
		
	public Date getInicio() {
		return inicio;
	}
	
	public Date getFim() {
		return fim;
	}
	
	@DisplayName("Destinat�rio")
	public Usuario getDestinatario() {
		return destinatario;
	}
	
	@DisplayName("Provid�ncia")
	public Providencia getProvidencia() {
		return providencia;
	}
	public void setNumProtocolo(Integer numProtocolo) {
		this.numProtocolo = numProtocolo;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setRemetente(Usuario remetente) {
		this.remetente = remetente;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public void setDestinatario(Usuario destinatario) {
		this.destinatario = destinatario;
	}
	public void setProvidencia(Providencia prodencia) {
		this.providencia = prodencia;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}
