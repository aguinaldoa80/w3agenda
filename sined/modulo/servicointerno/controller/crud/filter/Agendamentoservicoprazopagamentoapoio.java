package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Prazopagamento;

public class Agendamentoservicoprazopagamentoapoio {

	private Cliente cliente;
	private Prazopagamento prazopagamento;
	private Money valortotal;
	private Integer qtdeselecionado;
	private List<Agendamentoservicoprazopagamentoitemapoio> listaparcelas = new ListSet<Agendamentoservicoprazopagamentoitemapoio>(Agendamentoservicoprazopagamentoitemapoio.class);
	
	public Cliente getCliente() {
		return cliente;
	}
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	public Money getValortotal() {
		return valortotal;
	}
	public Integer getQtdeselecionado() {
		return qtdeselecionado;
	}
	public void setQtdeselecionado(Integer qtdeselecionado) {
		this.qtdeselecionado = qtdeselecionado;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public List<Agendamentoservicoprazopagamentoitemapoio> getListaparcelas() {
		return listaparcelas;
	}
	public void setListaparcelas(
			List<Agendamentoservicoprazopagamentoitemapoio> listaparcelas) {
		this.listaparcelas = listaparcelas;
	}
	
}
