package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;
import java.sql.Timestamp;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.Log;



public class AgendamentoservicoapoioFiltro implements Log{

	protected String dataReferencia;
	protected String dataReferenciaAte;
	protected Date dateFrom;
	protected Date dateTo;
	protected Empresa empresa;
	protected Colaborador colaborador;
	protected Material material;
	
	@Required
	public String getDataReferencia() {
		return dataReferencia;
	}
	
	@Required
	public String getDataReferenciaAte() {
		return dataReferenciaAte;
	}
	
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	public Material getMaterial() {
		return material;
	}
	
	@Required
	public Date getDateFrom() {
		return dateFrom;
	}
	
	@Required
	public Date getDateTo() {
		return dateTo;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setDataReferenciaAte(String dataReferenciaAte) {
		this.dataReferenciaAte = dataReferenciaAte;
	}
	
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	
	public Integer getCdusuarioaltera() {
		return null;
	}
	
	public Timestamp getDtaltera() {
		return null;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		
	}
	
}
