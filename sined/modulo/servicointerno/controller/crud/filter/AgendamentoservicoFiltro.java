package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Agendamentoservicocancela;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamentoServico;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AgendamentoservicoFiltro extends FiltroListagemSined{
	
	protected Empresa empresa;
	protected Cliente cliente;
	protected Requisicao requisicao;
	protected Materialtipo materialtipo;
	protected Material material;
	protected Colaborador colaborador;
	protected Date dtde;
	protected Date dtate;
	protected List<SituacaoAgendamentoServico> listaSituacoes;
	protected Categoria categoria;
	protected Agendamentoservicocancela agendamentoservicocancela;

	protected String tipovisaoreport;
	
	public static final String CLIENTE = "Cliente";
	public static final String COLABORADOR = "Colaborador (Simples)";
	public static final String COLABORADOR_COMPLETO = "Colaborador (Completo)";
	
	public AgendamentoservicoFiltro(){
		this.listaSituacoes = new ArrayList<SituacaoAgendamentoServico>();
		listaSituacoes.add(SituacaoAgendamentoServico.AGENDADO);
		listaSituacoes.add(SituacaoAgendamentoServico.ATENCAO);
//		listaSituacoes.add(SituacaoAgendamentoServico.CANCELADO);
		listaSituacoes.add(SituacaoAgendamentoServico.REALIZADO);
		listaSituacoes.add(SituacaoAgendamentoServico.EM_ANDAMENTO);
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Ordem de Servi�o")
	public Requisicao getRequisicao() {
		return requisicao;
	}
	@DisplayName("Tipo de servi�o")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@DisplayName("Servi�o")
	public Material getMaterial() {
		return material;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Date de")
	public Date getDtde() {
		return dtde;
	}
	@DisplayName("Data at�")
	public Date getDtate() {
		return dtate;
	}
	@DisplayName("Situa��es")
	public List<SituacaoAgendamentoServico> getListaSituacoes() {
		return listaSituacoes;
	}
	@DisplayName("Tipo vis�o relat�rio")
	public String getTipovisaoreport() {
		return tipovisaoreport;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	@DisplayName("Motivo do cancelamento")
	public Agendamentoservicocancela getAgendamentoservicocancela() {
		return agendamentoservicocancela;
	}
	public void setAgendamentoservicocancela(
			Agendamentoservicocancela agendamentoservicocancela) {
		this.agendamentoservicocancela = agendamentoservicocancela;
	}
	public void setTipovisaoreport(String tipovisaoreport) {
		this.tipovisaoreport = tipovisaoreport;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtde(Date dtde) {
		this.dtde = dtde;
	}
	public void setDtate(Date dtate) {
		this.dtate = dtate;
	}
	public void setListaSituacoes(List<SituacaoAgendamentoServico> listaSituacoes) {
		this.listaSituacoes = listaSituacoes;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
}
