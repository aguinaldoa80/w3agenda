package br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Documentotipo;

public class Agendamentoservicoprazopagamentoitemapoio {

	private Documentotipo documentotipo;
	private String banco;
	private String conta;
	private String agencia;
	private String cheque;
	private Double valor;
	private Date dtvencimento;
	
	public Agendamentoservicoprazopagamentoitemapoio(){
	}

	public Agendamentoservicoprazopagamentoitemapoio(Double valor, Date dtvencimento){
		this.valor = valor;
		this.dtvencimento = dtvencimento;
	}

	public Agendamentoservicoprazopagamentoitemapoio(Double valor, Date dtvencimento, Documentotipo documentotipo){
		this.valor = valor;
		this.dtvencimento = dtvencimento;
		this.documentotipo = documentotipo;
	}
	
	public String getBanco() {
		return banco;
	}
	public String getConta() {
		return conta;
	}
	public String getAgencia() {
		return agencia;
	}
	public String getCheque() {
		return cheque;
	}
	public Double getValor() {
		return valor;
	}
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public void setCheque(String cheque) {
		this.cheque = cheque;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	
}
