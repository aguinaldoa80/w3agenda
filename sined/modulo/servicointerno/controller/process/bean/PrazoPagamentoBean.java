package br.com.linkcom.sined.modulo.servicointerno.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Prazopagamento;

public class PrazoPagamentoBean {

	private Prazopagamento prazopagamentoAux; 
	private Double valorTotalAux;
	
	public Prazopagamento getPrazopagamentoAux() {
		return prazopagamentoAux;
	}
	
	public void setPrazopagamentoAux(Prazopagamento prazopagamentoAux) {
		this.prazopagamentoAux = prazopagamentoAux;
	}
	
	public Double getValorTotalAux() {
		return valorTotalAux;
	}
	
	public void setValorTotalAux(Double valorTotalAux) {
		this.valorTotalAux = valorTotalAux;
	}
	
	
}
