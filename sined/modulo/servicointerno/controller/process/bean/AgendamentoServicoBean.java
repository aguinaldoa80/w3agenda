package br.com.linkcom.sined.modulo.servicointerno.controller.process.bean;

import java.util.List;

import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoapoioFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoitemapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoprazopagamentoapoio;

public class AgendamentoServicoBean {
	
	private AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro;
	private List<Agendamentoservicoprazopagamentoapoio> listaAgendamentoservicoprazopagamentoapoio;
	private List<Agendamentoservicoitemapoio> listaagendamentoservicoitemapoio;
	
	public List<Agendamentoservicoitemapoio> getListaagendamentoservicoitemapoio() {
		return listaagendamentoservicoitemapoio;
	}

	public void setListaagendamentoservicoitemapoio(
			List<Agendamentoservicoitemapoio> listaagendamentoservicoitemapoio) {
		this.listaagendamentoservicoitemapoio = listaagendamentoservicoitemapoio;
	}

	public AgendamentoservicoapoioFiltro getAgendamentoservicoapoioFiltro() {
		return agendamentoservicoapoioFiltro;
	}
	
	public void setAgendamentoservicoapoioFiltro(
			AgendamentoservicoapoioFiltro agendamentoservicoapoioFiltro) {
		this.agendamentoservicoapoioFiltro = agendamentoservicoapoioFiltro;
	}
	
	public List<Agendamentoservicoprazopagamentoapoio> getListaAgendamentoservicoprazopagamentoapoio() {
		return listaAgendamentoservicoprazopagamentoapoio;
	}
	
	public void setListaAgendamentoservicoprazopagamentoapoio(
			List<Agendamentoservicoprazopagamentoapoio> listaAgendamentoservicoprazopagamentoapoio) {
		this.listaAgendamentoservicoprazopagamentoapoio = listaAgendamentoservicoprazopagamentoapoio;
	}

	
}
