package br.com.linkcom.sined.modulo.servicointerno.controller.process.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoitemapoio;

public class AgendamentoClienteHorarioBean {
	
	private Cliente cliente;
	private List<Agendamentoservicoitemapoio> listaAgendamentoServicoItemApoio;
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public List<Agendamentoservicoitemapoio> getListaAgendamentoServicoItemApoio() {
		return listaAgendamentoServicoItemApoio;
	}
	
	public void setListaAgendamentoServicoItemApoio(
			List<Agendamentoservicoitemapoio> listaAgendamentoServicoItemApoio) {
		this.listaAgendamentoServicoItemApoio = listaAgendamentoServicoItemApoio;
	}
	
	
}
