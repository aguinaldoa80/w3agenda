package br.com.linkcom.sined.modulo.servicointerno.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalacolaborador;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.service.AgendamentoservicoService;
import br.com.linkcom.sined.geral.service.AusenciacolaboradorService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EscalaService;
import br.com.linkcom.sined.geral.service.EscalacolaboradorService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.AgendamentoservicoapoioFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoitemapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.Agendamentoservicoprazopagamentoitemapoio;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.RegistrarausenciaFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.process.bean.AgendamentoClienteHorarioBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.process.bean.AgendamentoServicoBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.process.bean.PrazoPagamentoBean;

@Controller(
		path="/servicointerno/process/AgendaServico",
		authorizationModule=ProcessAuthorizationModule.class
)
public class AgendaServicoProcess extends MultiActionController {
	
	private AgendamentoservicoService agendamentoservicoService;
	private EscalaService escalaService;
	private EscalacolaboradorService escalacolaboradorService;
	private AusenciacolaboradorService ausenciacolaboradorService;
	private EmpresaService empresaService;
	private ClienteService clienteService;
	private MaterialService materialService;
	private DocumentotipoService documentotipoService;
	private PrazopagamentoService prazopagamentoService;
	
	public void setAgendamentoservicoService(
			AgendamentoservicoService agendamentoservicoService) {
		this.agendamentoservicoService = agendamentoservicoService;
	}

	public void setEscalaService(EscalaService escalaservice){
		this.escalaService = escalaservice;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setAusenciacolaboradorService(AusenciacolaboradorService ausenciacolaboradorService){
		this.ausenciacolaboradorService = ausenciacolaboradorService;
	}
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	public void setEscalacolaboradorService(EscalacolaboradorService escalacolaboradorService){
		this.escalacolaboradorService = escalacolaboradorService;
	}
	
	public void setMaterialServicee(MaterialService materialService) {
		this.materialService = materialService;
	}	
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}

	public void setDocumentotipoService(DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}

	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}

	@DefaultAction
	public ModelAndView index(WebRequestContext request, AgendamentoservicoapoioFiltro filtro) {
		request.setAttribute("TEMPLATE_beanNameUncaptalized",  Util.strings.uncaptalize(AgendamentoservicoapoioFiltro.class.getSimpleName()));
		request.setAttribute("TEMPLATE_beanName", Util.strings.uncaptalize(AgendamentoservicoapoioFiltro.class.getSimpleName()));
		request.setAttribute("TEMPLATE_enviar", "salvar");
		request.setAttribute("TEMPLATE_voltar", "listagem");
		request.setAttribute("TEMPLATE_beanDisplayName", Neo.getApplicationContext().getBeanDescriptor(null, AgendamentoservicoapoioFiltro.class).getDisplayName());
		request.setAttribute("TEMPLATE_beanClass", AgendamentoservicoapoioFiltro.class);
		request.setAttribute(Util.strings.uncaptalize(AgendamentoservicoapoioFiltro.class.getSimpleName()), new AgendamentoservicoapoioFiltro());
		Calendar calendar = Calendar.getInstance();
		filtro.setDateFrom(new Date(calendar.getTimeInMillis()));	
		calendar.add(Calendar.WEEK_OF_MONTH, 2);
		filtro.setDateTo(new Date(calendar.getTimeInMillis()));
		return new ModelAndView("process/agendaAtendimento", "filtro", filtro);
	}
	
	public ModelAndView abrirPopUpPagamento(WebRequestContext request){
		return new ModelAndView("direct:/process/popup/pagamento");
	}
	
	public ModelAndView abrirPopUpCliente(WebRequestContext request, ClienteFiltro filtro){
		return new ModelAndView("direct:/process/popup/cliente", "filtro", filtro);
	}
 
	public ModelAndView findCliente(WebRequestContext request, ClienteFiltro filtro){
		List<Cliente> lista = clienteService.findForListagemFlex(filtro);
		return new JsonModelAndView().addObject("clienteList", lista);
	}
 
	public ModelAndView validaAgendamento(WebRequestContext request, AgendamentoClienteHorarioBean clienteHorario){
		JsonModelAndView view = new JsonModelAndView();
		// Verfica se cliente tem agendamento no mesmo hor�rio em outro servi�o ou colaborador
		List<String> agendamentoHoario = agendamentoservicoService.verificaAgendamentoClienteHorarioAlocado(clienteHorario.getCliente(), clienteHorario.getListaAgendamentoServicoItemApoio());
		if(agendamentoHoario.isEmpty()){
			// Verifica se exsiste contas a receber;
			List<Cliente> lista =  clienteService.findClientesContaReceberAtrasadaOuRestricoesEmAberto(clienteHorario.getCliente());
			if(lista.size() > 0){
				view.addObject("success", false);
				view.addObject("pendencias", lista);				 
			} else{
				view.addObject("success", true);
			}
		} else {
			view.addObject("agendamentoHoario", agendamentoHoario);
			view.addObject("success", false);
		}		 
		return view;
	}
	 
	public ModelAndView salvarAgendamento(WebRequestContext request, AgendamentoServicoBean agendamentoServico){
		JsonModelAndView view = new JsonModelAndView();
		
		List<Agendamentoservico> listaAgendamentoServico = new ArrayList<Agendamentoservico>();
		for(Agendamentoservicoitemapoio asia: agendamentoServico.getListaagendamentoservicoitemapoio()){
			for(Agendamentoservico as: asia.getListaAgendamentos()){
				if(as.getCliente() != null && as.getCliente().getCdpessoa() != null){
					as.setCdagendamentoservico(null);
					as.setEmpresa(agendamentoServico.getAgendamentoservicoapoioFiltro().getEmpresa());
					as.setMaterial(agendamentoServico.getAgendamentoservicoapoioFiltro().getMaterial());
					as.setColaborador(agendamentoServico.getAgendamentoservicoapoioFiltro().getColaborador());
					as.setData(asia.getData());					
					listaAgendamentoServico.add(as);
				}								
			}							
		}
		
		 try {
			 agendamentoservicoService.prepareToSaveAgendaNovosServicos(listaAgendamentoServico, agendamentoServico.getAgendamentoservicoapoioFiltro(), 
					 agendamentoServico.getListaAgendamentoservicoprazopagamentoapoio());
			 view.addObject("agendaDataGrid", agendamentoservicoService.buildScheduleService(agendamentoServico.getAgendamentoservicoapoioFiltro()));
			 view.addObject("success", true);
		} catch (Exception e) {
			view.addObject("success", false);
			view.addObject("message", e);
		}
		return view;
	 }
	
	public ModelAndView listar(WebRequestContext request, AgendamentoservicoapoioFiltro filtro){
		List<Agendamentoservicoapoio> lista = agendamentoservicoService.buildScheduleService(filtro); 
		return new JsonModelAndView().addObject("agendaDataGrid", lista);
	}
	
	public ModelAndView getMaterial(WebRequestContext request, AgendamentoservicoapoioFiltro filtro){
		return new JsonModelAndView().addObject("material", materialService.findByCdmaterial(filtro.getMaterial().getCdmaterial()));
	}
	
	public ModelAndView getPrazoPagamento(WebRequestContext request, AgendamentoservicoapoioFiltro filtro){
		return new JsonModelAndView().addObject("prazopagamento", prazopagamentoService.findAllForFlex());
	}
	
	public ModelAndView getFormaPagamento(WebRequestContext request){
		return new JsonModelAndView().addObject("formaPagamento", documentotipoService.findAllForFlex());
	}

	public ModelAndView montaPrazoPagamento(WebRequestContext request, PrazoPagamentoBean prazoPagamento){
		List<Agendamentoservicoprazopagamentoitemapoio> listPrazopagamento = prazopagamentoService.montaPrazoPagamentoFlex(prazoPagamento.getPrazopagamentoAux(), prazoPagamento.getValorTotalAux());
		for(Agendamentoservicoprazopagamentoitemapoio prazopagamento: listPrazopagamento){
			prazopagamento.getDocumentotipo().setListaClienteDocumentoTipo(null);
		}
		return new JsonModelAndView().addObject("parcelas", listPrazopagamento);
	}
	

	public ModelAndView abrirPopupRemarcar(WebRequestContext request,Agendamentoservico agendamentoservicoBean){
		return new ModelAndView("direct:crud/popup/popUpAgendaAtendimentoRemarcacao", "bean", agendamentoservicoBean);
	}
		
	
	public ModelAndView comboBoxEscalaHorario(WebRequestContext request, Agendamentoservico agendamentoservico) {
		List<Escalahorario> lista = agendamentoservicoService.buscarHorariosEntrada(agendamentoservico);
		return new JsonModelAndView().addObject("listaEscalaHorario", lista);
	}
	
	public void salvarRemarcacao(WebRequestContext request, Agendamentoservico agendamentoservicoBean){
		agendamentoservicoBean.setIsHorarioDisponivel(true);		
		agendamentoservicoService.remarcarClienteFlex(agendamentoservicoBean, agendamentoservicoBean.getDataAux(), agendamentoservicoBean.getEscalahorario());
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>" +
				"parent.startScheduleBuild();" +
				"parent.$.akModalRemove(true);" +
				"</script>");
	}

	//DESMARCACAO	
	public ModelAndView selecionarClienteDesmarcar(WebRequestContext request, Agendamentoservico agendamentoservicoBean){
		return new ModelAndView("direct:process/popup/desmarcarCliente", "bean", agendamentoservicoBean);
	}
	
	public ModelAndView abrirPopupDesmarcar(WebRequestContext request,Agendamentoservico agendamentoservicoBean){
		return new ModelAndView("direct:crud/popup/popUpAgendaAtendimentoDesmarcacao", "bean", agendamentoservicoBean);
	}
	
	public void salvarDesmarcacao(WebRequestContext request, Agendamentoservico bean){
		agendamentoservicoService.desalocarClienteFlex(bean, bean.getMotivo());
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>" +
				"parent.startScheduleBuild();" +
				"parent.$.akModalRemove(true);" +
				"</script>");
	}

	//NOVO HORARIO
	public ModelAndView abrirPopupNovoHorario(WebRequestContext request, Escalacolaborador bean){
		return new ModelAndView("direct:crud/popup/popUpAgendaAtendimentoNovoHorario", "bean", bean);
	}
	
	//M�todo ajax que busca a escala de hor�rio do Colaborador de acordo com o dia
	public ModelAndView gerarHorarios(WebRequestContext request,Escala bean) {
		List<Escalahorario> lista = escalaService.prepareGerarEscalaFlex(bean.getHoraInicio().toString(), bean.getHoraFim().toString(), bean.getIntervalo());
		return new JsonModelAndView().addObject("listaEscalaHorario", lista);
	}
	
	public void salvarNovoHorario(WebRequestContext request, Escalacolaborador bean){	
		if(bean.getEscala().getListaescalahorario().size() > 0){
			for (int i = 0; i < bean.getEscala().getListaescalahorario().size(); i++){
				bean.getEscala().getListaescalahorario().get(i).setHorafimstring(bean.getEscala().getListaescalahorario().get(i).getHorafim().toString());
				bean.getEscala().getListaescalahorario().get(i).setHorainiciostring(bean.getEscala().getListaescalahorario().get(i).getHorainicio().toString());
			}	
			escalacolaboradorService.salvaNovaEscalahorarioColaboradorFlex(bean);
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>" +
					"parent.startScheduleBuild();" +
					"parent.$.akModalRemove(true);" +
					"</script>");
			
		}
	}
	
	//REGISTRAR AUSENCIA	
	public ModelAndView abrirPopupRegistrarAusencia(WebRequestContext request, RegistrarausenciaFiltro filter){
		return new ModelAndView("direct:crud/popup/popUpAgendaAtendimentoRegistrarAusencia", "bean", filter);
	}
	
	public void salvarAusencia(WebRequestContext request, RegistrarausenciaFiltro filter){		
			ausenciacolaboradorService.registrarAusenciaFlex(filter.getColaborador(), filter.getListaAgendamentoServicoItemApoio(), filter.getMotivo(), filter.getAusenciamotivo());
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>" +
					"parent.startScheduleBuild();" +
					"parent.$.akModalRemove(true);" +
					"</script>");
		}
}