package br.com.linkcom.sined.modulo.servicointerno.controller.process;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoprioridade;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/servicointerno/process/Requisicaopainel", authorizationModule=CrudAuthorizationModule.class)
public class RequisicaopainelProcess extends MultiActionController{
	
	private RequisicaoService requisicaoService;

	public void setRequisicaoService(RequisicaoService requisicaoService) {
		this.requisicaoService = requisicaoService;
	}	
		
	/**
	 * M�todo que carrega as informa��es/estrutura do painel da ordem de servi�o
	 * 
	 * @see br.com.linkcom.sined.modulo.servicointerno.controller.process.RequisicaopainelProcess#painelOrdemservico(Integer prioridade)
	 *
	 * @param request
	 * @param contacrmcontato
	 * @author Luiz Fernando
	 */
	public void carregaRequisicaopainel(WebRequestContext request, Contacrmcontato contacrmcontato){
		
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		String prioridadestr = request.getParameter("prioridade");
		Integer prioridade = null;		
		if(prioridadestr != null && !"".equals(prioridadestr)){
			prioridade = Integer.parseInt(prioridadestr);
		}
		
		String requisicaopainel = this.painelOrdemservico(prioridade);
		
		String comboRequisicaoprioridade = null;
		if(prioridadestr == null || "".equals(prioridadestr)){
			List<Requisicaoprioridade> lista = new ArrayList<Requisicaoprioridade>();
			lista.add(new Requisicaoprioridade(Requisicaoprioridade.SEM_PRIORIDADE, "Sem prioridade"));
			lista.add(new Requisicaoprioridade(Requisicaoprioridade.BAIXA, "Baixa"));
			lista.add(new Requisicaoprioridade(Requisicaoprioridade.MEDIA, "M�dia"));
			lista.add(new Requisicaoprioridade(Requisicaoprioridade.ALTA, "Alta"));
			lista.add(new Requisicaoprioridade(Requisicaoprioridade.URGENTE, "Urgente"));
			
			comboRequisicaoprioridade = SinedUtil.convertToJavaScript(lista, "comboRequisicaoprioridade", "");
			
		}
		
		view.println("var conteudo = '" + (requisicaopainel != null ? requisicaopainel : "") + "';");
		
		if(comboRequisicaoprioridade != null && !"".equals(comboRequisicaoprioridade))
			view.println(comboRequisicaoprioridade);
		else
			view.println("var comboRequisicaoprioridade = '<null>'");
	}
	
	/**
	 * M�todo para montar a tabela do painel da ordem de servi�o
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaoService#findForPainel(Integer prioridade)
	 *
	 * @param prioridade
	 * @return
	 * @author Luiz Fernando
	 */
	private String painelOrdemservico(Integer prioridade){
		StringBuilder html = new StringBuilder("<tr class=\"dataGridHeader\"><td>OS</td><td>Prioridade</td><td>Descri��o</td><td></td></tr>");
		List<Requisicao> lista = requisicaoService.findForPainel(prioridade);		
		if(lista != null && !lista.isEmpty()){	
			int i = 0;
			for(Requisicao requisicao : lista){
				
				if(i % 2 == 0){
					html.append("<tr class=\"dataGridBody1\">");
				} else {
					html.append("<tr class=\"dataGridBody2\">");	
				}
				i++;
				
				html
					.append("<td class=\"\" align=\"\" style=\"text-align: right;\">")
					.append(requisicao.getCdrequisicao())
					.append("</td>");
				html
					.append("<td class=\"\" align=\"\" style=\"text-align: right;\">")
					.append(requisicao.getRequisicaoprioridade() != null ? requisicao.getRequisicaoprioridade().getDescricao() : "")
					.append("</td>");
				
				String descricao = requisicao.getDescricao() != null && requisicao.getDescricao().length() > 50 ? requisicao.getDescricao().substring(0,50) + "..." : requisicao.getDescricao();
				html
					.append("<td class=\"\" align=\"\" style=\"text-align: left;\">")
					.append(descricao.replaceAll("\r?\n", "").replaceAll("'", ""))
					.append("</td>");
				
				html
					.append("<td style=\"width: 1%; white-space: nowrap; padding-right: 3px;\">")
					.append("<a class=\"activation\" title=\"\" href=\"/w3erp/servicointerno/crud/Requisicao?ACAO=consultar&cdrequisicao=" + requisicao.getCdrequisicao() + "\">")
					.append("<img border=\"0\" src=\"/w3erp/imagens/consultar_icon.gif\"></a>")
					.append("</td>");					
					
				html.append("</tr>");
			}
		}else{
			html.append("<tr class=\"dataGridBody1\">");
			html
				.append("<td class=\"\" align=\"\" style=\"text-align: right;\" colspan=\"4\">")
				.append("N�o existem registros!")
				.append("</td>");			
			html.append("</tr>");
		}
		
		return html.toString();
	}
}
