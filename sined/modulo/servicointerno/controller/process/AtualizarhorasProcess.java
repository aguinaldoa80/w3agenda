package br.com.linkcom.sined.modulo.servicointerno.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.service.AutorizacaotrabalhoService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.util.AlterarEstadoException;
import br.com.linkcom.sined.util.SinedException;


@Controller(path="/servicointerno/process/Atualizarhoras", authorizationModule=ProcessAuthorizationModule.class)
public class AtualizarhorasProcess extends MultiActionController{
	
	protected AutorizacaotrabalhoService autorizacaotrabalhoService;
	protected RequisicaoService requisicaoService;
	
	public void setAutorizacaotrabalhoService(
			AutorizacaotrabalhoService autorizacaotrabalhoService) {
		this.autorizacaotrabalhoService = autorizacaotrabalhoService;
	}
	public void setRequisicaoService(RequisicaoService requisicaoService) {
		this.requisicaoService = requisicaoService;
	}
	
	/**
	 * M�todo respons�vel por atualizar horas de uma ou mais requisi��es.
	 * 
	 * @author Taidson
	 * @since 24/05/2010
	 */
	public ModelAndView atualizarHoras(WebRequestContext request){
		
		String itens[] = request.getParameter("selectedItens").split(",");
		
		for (String item : itens) {
			Integer cdrequisicao = Integer.valueOf(item);
			Double qtdeHoras;
			
			Requisicao requisicao = new Requisicao(cdrequisicao);
			
			qtdeHoras = autorizacaotrabalhoService.calculaHoras(requisicao);
			
			if(qtdeHoras == null) qtdeHoras = 0.0;
			
			try {
				requisicaoService.atualizaHoras(requisicao, qtdeHoras);
			} catch (AlterarEstadoException e) {
				throw new SinedException(e.getMessage());
			}
		}
		request.addMessage("Horas atualizadas com sucesso.");
		return new ModelAndView ("redirect:/servicointerno/crud/Requisicao?ACAO=listagem");
	}
}
