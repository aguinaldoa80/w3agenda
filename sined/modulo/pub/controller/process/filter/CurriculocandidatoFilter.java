package br.com.linkcom.sined.modulo.pub.controller.process.filter;

import java.sql.Timestamp;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Telefone;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Candidatosituacao;
import br.com.linkcom.sined.geral.bean.Estadocivil;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CurriculocandidatoFilter extends FiltroListagemSined{
	
	
	private Cpf cpf;
	private Cpf verificaCpf;
	private String senha;
	private String verificaSenha;
	private String nome;
	private Timestamp dtnascimento;
	private Sexo sexo;
	private String email;
	private Telefone telefone;
	private Telefone celular;
	private Estadocivil estadocivil;
	private Candidatosituacao candidatosituacao;
	
	private Timestamp dtatual;
	
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private Uf uf;
	private Municipio municipio;
	private Cep cep;
	private String caixapostal;
	
	private Grauinstrucao grauinstrucao;
	private String curso;
	private String instituicao;
	
	private String formacaoprofissional;
	
	
	
	  //*************//
	 //**Get & Set**//
	//*************//
	
	@DisplayName("Cpf")
	public Cpf getCpf() {
		return cpf;
	}
	@DisplayName("Cpf")
	public Cpf getVerificaCpf() {
		return verificaCpf;
	}
	@DisplayName("Senha")
	@MaxLength(6)
	public String getSenha() {
		return senha;
	}
	@MaxLength(6)
	@DisplayName("Senha")
	public String getVerificaSenha() {
		return verificaSenha;
	}
	@DisplayName("Nome")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@DisplayName("Data Nascimento")
	public Timestamp getDtnascimento() {
		return dtnascimento;
	}
	@DisplayName("Sexo")
	public Sexo getSexo() {
		return sexo;
	}
	@DisplayName("E-mail")
	@MaxLength(50)
	public String getEmail() {
		return email;
	}
	@DisplayName("Telefone")
	@MaxLength(10)
	public Telefone getTelefone() {
		return telefone;
	}
	@DisplayName("Celular")
	@MaxLength(10)
	public Telefone getCelular() {
		return celular;
	}
	@DisplayName("Estado Civil")
	public Estadocivil getEstadocivil() {
		return estadocivil;
	}
	@DisplayName("Situa��o")
	public Candidatosituacao getCandidatosituacao() {
		return candidatosituacao;
	}
	@DisplayName("Data Atual")
	public Timestamp getDtatual() {
		return dtatual;
	}
	@DisplayName("Logradouro")
	@MaxLength(100)
	public String getLogradouro() {
		return logradouro;
	}
	@DisplayName("N�mero")
	@MaxLength(5)
	public String getNumero() {
		return numero;
	}
	@DisplayName("Complemento")
	@MaxLength(50)
	public String getComplemento() {
		return complemento;
	}
	@DisplayName("Bairro")
	@MaxLength(50)
	public String getBairro() {
		return bairro;
	}
	@DisplayName("Uf")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("Cep")
	@MaxLength(8)
	public Cep getCep() {
		return cep;
	}
	@DisplayName("Caixa Postal")
	@MaxLength(50)
	public String getCaixapostal() {
		return caixapostal;
	}
	@DisplayName("Escolaridade")
	public Grauinstrucao getGrauinstrucao() {
		return grauinstrucao;
	}
	@DisplayName("Curso")
	@MaxLength(100)
	public String getCurso() {
		return curso;
	}
	@DisplayName("Institui��o")
	@MaxLength(100)
	public String getInstituicao() {
		return instituicao;
	}
	@DisplayName("Forma��o Profissional")
	public String getFormacaoprofissional() {
		return formacaoprofissional;
	}
	

	
	
	
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setVerificaCpf(Cpf verificaCpf) {
		this.verificaCpf = verificaCpf;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void setVerificaSenha(String verificaSenha) {
		this.verificaSenha = verificaSenha;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDtnascimento(Timestamp dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	public void setCelular(Telefone celular) {
		this.celular = celular;
	}
	public void setEstadocivil(Estadocivil estadocivil) {
		this.estadocivil = estadocivil;
	}
	public void setCandidatosituacao(Candidatosituacao candidatosituacao) {
		this.candidatosituacao = candidatosituacao;
	}
	public void setDtatual(Timestamp dtatual) {
		this.dtatual = dtatual;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public void setCaixapostal(String caixapostal) {
		this.caixapostal = caixapostal;
	}
	public void setGrauinstrucao(Grauinstrucao grauinstrucao) {
		this.grauinstrucao = grauinstrucao;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}
	public void setFormacaoprofissional(String formacaoprofissional) {
		this.formacaoprofissional = formacaoprofissional;
	}

}
