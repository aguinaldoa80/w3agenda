package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.BufferedOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.EnvioemailitemService;

@Controller(path="/pub/ConfirmacaoEmail")
public class ConfirmacaoEmailController extends MultiActionController {
	
	private EnvioemailitemService envioemailitemService;
	
	public void setEnvioemailitemService(EnvioemailitemService envioemailitemService) {
		this.envioemailitemService = envioemailitemService;
	}

	@DefaultAction
	public void index(WebRequestContext request) {
		//N�o faz nada se chamar esta url sem parametros...		
	}
	
	/**
	 * 
	 * @param request
	 * @throws IOException
	 * @author Thiago Clemente
	 * 
	 */
	public void confirmacao(WebRequestContext request) throws IOException {
		
		try {
			Integer cdenvioemail = Integer.valueOf(request.getParameter("cdenvioemail"));
			String email = request.getParameter("email").toUpperCase().trim();
			envioemailitemService.confirmacaoEmail(cdenvioemail, email);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		imgResponse(request.getServletResponse());	
	}
	
	/**
	 * 
	 * @param response
	 * @throws IOException
	 * @author Thiago Clemente
	 * 
	 */
	private void imgResponse(HttpServletResponse response) throws IOException {
		byte[] imagem = {
							(byte) 0x47, (byte) 0x49, (byte) 0x46, (byte) 0x38,
							(byte) 0x39, (byte) 0x61, (byte) 0x01, (byte) 0x00,
							(byte) 0x01, (byte) 0x00, (byte) 0x80, (byte) 0xFF,
							(byte) 0x00, (byte) 0xC0, (byte) 0xC0, (byte) 0xC0,
							(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x21,
							(byte) 0xF9, (byte) 0x04, (byte) 0x01, (byte) 0x00,
							(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x2C,
							(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
							(byte) 0x01, (byte) 0x00, (byte) 0x01, (byte) 0x00,
							(byte) 0x40, (byte) 0x02, (byte) 0x02, (byte) 0x44,
							(byte) 0x01, (byte) 0x00, (byte) 0x3B
						};

		response.setContentType("image/gif");
		response.setContentLength(imagem.length);
		
		BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream());
		output.write(imagem);
		output.flush();
		output.close();
	}
}