package br.com.linkcom.sined.modulo.pub.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.OtrPneuTipo;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.geral.bean.Pneuqualificacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pneureforma;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.OtrPneuTipoService;
import br.com.linkcom.sined.geral.service.PneuSegmentoService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.PneumarcaService;
import br.com.linkcom.sined.geral.service.PneumedidaService;
import br.com.linkcom.sined.geral.service.PneumodeloService;
import br.com.linkcom.sined.geral.service.PneuqualificacaoService;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericPneuSearchBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericSearchBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PneuSegmentoWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PneuWSBean;
import br.com.linkcom.sined.util.EnumUtils;
import br.com.linkcom.sined.util.ObjectUtils;

@Bean
@Controller(path="/pub/process/IntegracaoWebServicePneu")
public class IntegracaoPneuWebService extends MultiActionController{

	public ModelAndView ajaxInformacoesPneu(WebRequestContext request, GenericPneuSearchBean bean){
		Pneu pneu = PneuService.getInstance().informacoesPneu(request, bean.getPneu());
		return new JsonModelAndView()
					.addObject("pneu", new PneuWSBean(pneu));
	}
	
	public ModelAndView ajaxVerificaMaterialsimilarBanda(WebRequestContext request, GenericPneuSearchBean bean){
		return PneuService.getInstance().ajaxVerificaMaterialsimilarBanda(bean.getMaterialBanda());
	}
	
	public ModelAndView ajaxDadosMaterialbanda(WebRequestContext request, GenericPneuSearchBean bean){
		bean.setPneu(new Pneu());
		bean.getPneu().setMaterialbanda(bean.getMaterialBanda());
		return PneuService.getInstance().ajaxDadosMaterialbanda(bean.getPneu());
	}
	
	public ModelAndView findPneuModeloAutoComplete(WebRequestContext request, GenericPneuSearchBean bean){
		List<Pneumodelo> lista = PneumodeloService.getInstance().findAutocomplete(bean.getValue().toString(), bean.getPneuMarca(), bean.getPneuSegmento());
		return new JsonModelAndView().addObject("listaPneuModelo", ObjectUtils.translateEntityListToGenericBeanList(lista));
	}
	
	public ModelAndView findPneuTipoAutoComplete(WebRequestContext request, GenericPneuSearchBean bean){
		List<OtrPneuTipo> lista = OtrPneuTipoService.getInstance().findAutocomplete(bean.getValue().toString());
		return new JsonModelAndView().addObject("listaPneuTipo", ObjectUtils.translateEntityListToGenericBeanList(lista));
	}
	
	public ModelAndView findPneuMarcaAutoComplete(WebRequestContext request, GenericPneuSearchBean bean){
		List<Pneumarca> lista = PneumarcaService.getInstance().findAutocomplete(bean.getValue().toString());
		return new JsonModelAndView().addObject("listaPneuMarca", ObjectUtils.translateEntityListToGenericBeanList(lista));
	}
	
	public ModelAndView findPneuQualificacaoAutoComplete(WebRequestContext request, GenericPneuSearchBean bean){
		List<Pneuqualificacao> lista = PneuqualificacaoService.getInstance().findAutocomplete(bean.getValue().toString());
		return new JsonModelAndView().addObject("listaPneuQualificacao", ObjectUtils.translateEntityListToGenericBeanList(lista));
	}
	
	public ModelAndView findPneuMedidaAutoComplete(WebRequestContext request, GenericPneuSearchBean bean){
		List<Pneumedida> lista = PneumedidaService.getInstance().findAutocomplete(bean.getValue().toString(), bean.getPneuSegmento());
		return new JsonModelAndView().addObject("listaPneuMedida", ObjectUtils.translateEntityListToGenericBeanList(lista));
	}
	
	public ModelAndView findPneuSegmentoAutoComplete(WebRequestContext request, GenericPneuSearchBean bean){
		List<PneuSegmento> listaSegmento = PneuSegmentoService.getInstance().findAutocomplete(bean.getValue().toString());
		List<PneuSegmentoWSBean> lista = new ArrayList<PneuSegmentoWSBean>();
		for(PneuSegmento pneuSegmento: listaSegmento){
			lista.add(new PneuSegmentoWSBean(pneuSegmento));
		}
		return new JsonModelAndView().addObject("listaPneuSegmento", lista);
	}
	
	public ModelAndView findMaterialBandaAutoComplete(WebRequestContext request, GenericPneuSearchBean bean){
		List<Material> lista = MaterialService.getInstance().findAutocompleteBanda(bean.getValue().toString());
		return new JsonModelAndView().addObject("listaMaterialBanda", ObjectUtils.translateEntityListToGenericBeanList(lista));
	}
	
	public ModelAndView findPneuReforma(WebRequestContext request, GenericPneuSearchBean bean){
		return new JsonModelAndView().addObject("listaPneuReforma", EnumUtils.translateEnumToBean(Pneureforma.class));
	}
	
	public ModelAndView loadForPopupPneu(WebRequestContext request){
		 return new JsonModelAndView()
		 				.addObject("listaPneuReforma", EnumUtils.translateEnumToBean(Pneureforma.class))
		 				.addObject("listaPneuQualificacao", ObjectUtils.translateEntityListToGenericBeanList(PneuqualificacaoService.getInstance().findAtivos()));
	}
	
	public ModelAndView savePneu(WebRequestContext request, GenericPneuSearchBean bean){
		Pneu pneu = bean.getPneu();
		if(!Util.objects.isPersistent(pneu)){
			pneu = new Pneu();
		}
		pneu.setMaterialbanda(bean.getMaterialBanda());
		pneu.setPneumarca(bean.getPneuMarca());
		pneu.setPneumodelo(bean.getPneuModelo());
		pneu.setPneuqualificacao(bean.getPneuQualificacao());
		pneu.setProfundidadesulco(bean.getProfundidadeSulco());
		pneu.setDot(bean.getDot());
		pneu.setSerie(bean.getSerie());
		pneu.setNumeroreforma(bean.getNumeroReforma());
		pneu.setMaterialbanda(bean.getMaterialBanda());
		pneu.setPneumedida(bean.getPneuMedida());
		pneu.setOtrPneuTipo(bean.getPneuTipo());
		pneu.setLonas(bean.getLonas());
		Boolean success = false;
		try {
			PneuService.getInstance().saveOrUpdate(pneu);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new JsonModelAndView().addObject("success", success)
									.addObject("id", Util.strings.toStringIdStyled(pneu));
	}
	
	public ModelAndView findAutoCompletePneu(WebRequestContext request, GenericSearchBean bean){
		List<Pneu> listaPneu = PneuService.getInstance().findAutocompletePneu(bean.getValue());
		List<PneuWSBean> listaBean = new ArrayList<PneuWSBean>();
		for(Pneu pneu: listaPneu){
			listaBean.add(new PneuWSBean(pneu));
		}
		return new JsonModelAndView()
					.addObject("listaPneu", listaBean);
	}
	
	public ModelAndView loadPneuByCodigo(WebRequestContext request, GenericSearchBean bean){
		if(StringUtils.isBlank(bean.getValue())){
			new JsonModelAndView().addObject("pneu", null);
		}
		Pneu pneu = null;
		try {
			pneu = new Pneu(Integer.parseInt(bean.getValue()));
		} catch (Exception e) {
			new JsonModelAndView().addObject("pneu", null);
		}
		GenericPneuSearchBean beanAux = new GenericPneuSearchBean();
		beanAux.setPneu(pneu);
		return this.ajaxInformacoesPneu(request, beanAux);
	}
	
	public ModelAndView findForPesquisaPneu(WebRequestContext request, GenericPneuSearchBean bean){
		return PneuService.getInstance().ajaxFindForPesquisaPneu(bean);
	}
	
}
