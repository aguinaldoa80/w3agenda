package br.com.linkcom.sined.modulo.pub.controller.process;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.jdom.Element;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.BaixarEstoqueBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.CancelarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConsultarClientesBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConsultarEstoqueBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConsultarProdutosBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConsultarResponsabilidadesBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConsultarSituacaoNotaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConsultarVendedoresBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.EstornarProducaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.EstornarProducaoItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.FaturarColetaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.FaturarColetaItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ManterPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ManterPedidovendaItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ManterPedidovendaParcelaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ClienteWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ColaboradorWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ConsultarEstoqueWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ConsultarSituacaoNotaWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.DocumentotipoWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.GenericRespostaWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.PrazopagamentoWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ProdutoWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ServicoWSBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;

@Bean
@Controller(path="/pub/process/IntegracaoSOAP")
public class IntegracaoSOAPProcess extends MultiActionController {
	
	public static final String PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE = "TOKEN_VALIDACAO_WEBSERVICE";
	
	private ParametrogeralService parametrogeralService;
	private ClienteService clienteService;
	private DocumentoService documentoService;
	private ColaboradorService colaboradorService;
	private PrazopagamentoService prazopagamentoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private PedidovendaService pedidovendaService;
	private EmpresaService empresaService;
	private DocumentotipoService documentotipoService;
	private VendaService vendaService;
	private MaterialService materialService;
	private NotaService notaService;
	private LocalarmazenagemService localarmazenagemService;
	
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setDocumentotipoService(
			DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setPrazopagamentoService(
			PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	
	public void consultarClientes(WebRequestContext request, ConsultarClientesBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Timestamp dataReferencia = bean.getDataReferencia();
			if(dataReferencia == null){
				throw new SinedException("Data de refer�ncia � obrigat�ria.");
			}
			
			List<ClienteWSBean> lista = clienteService.findForWSSOAP(dataReferencia);
			for (ClienteWSBean clienteWSBean : lista) {
				respostaWSBean.addElement(clienteWSBean.transformToElement());
			}
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarResponsabilidades(WebRequestContext request, ConsultarResponsabilidadesBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Integer codigo_cliente = bean.getCodigo();
			if(codigo_cliente == null){
				throw new SinedException("C�digo � obrigat�rio.");
			}
			
			String cnpj_empresa_string = bean.getEmpresa();
			if(cnpj_empresa_string == null || cnpj_empresa_string.trim().equals("")){
				throw new SinedException("Empresa � obrigat�ria.");
			}
			
			Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
			Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
			if(empresa == null){
				throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
			}
			
			Cliente cliente = clienteService.load(new Cliente(codigo_cliente), "cliente.cdpessoa, cliente.nome");
			if(cliente == null){
				throw new SinedException("Cliente n�o encontrado com o c�digo '" + codigo_cliente + "'.");
			}
			
			List<Documento> listaContaReceber = documentoService.findForWSSOAP(empresa, cliente);
			Double vencido = 0.0;
			Double avencer = 0.0;
			Date current_date = SinedDateUtils.currentDate();
			for (Documento documento : listaContaReceber) {
				if(documento.getAux_documento() != null && documento.getAux_documento().getValoratual() != null && documento.getDtvencimento() != null){
					if(SinedDateUtils.beforeOrEqualIgnoreHour(current_date, documento.getDtvencimento())){
						avencer += documento.getAux_documento().getValoratual().getValue().doubleValue();
					} else {
						vencido += documento.getAux_documento().getValoratual().getValue().doubleValue();
					}
				}
			}
			
			Element vencidoElt = new Element("vencido");
			vencidoElt.setText(vencido.toString());
			
			Element avencerElt = new Element("avencer");
			avencerElt.setText(avencer.toString());
			
			Element responsabilidadeElt = new Element("responsabilidade");
			responsabilidadeElt.addContent(vencidoElt);
			responsabilidadeElt.addContent(avencerElt);
			
			respostaWSBean.addElement(responsabilidadeElt);
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarVendedores(WebRequestContext request, ConsultarVendedoresBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Timestamp dataReferencia = bean.getDataReferencia();
			if(dataReferencia == null){
				throw new SinedException("Data de refer�ncia � obrigat�ria.");
			}
			
			List<ColaboradorWSBean> lista = colaboradorService.findForWSSOAP(dataReferencia, bean.getCargo());
			for (ColaboradorWSBean colaboradorWSBean : lista) {
				respostaWSBean.addElement(colaboradorWSBean.transformToElement());
			}
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarPrazospagamento(WebRequestContext request, GenericEnvioInformacaoBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			List<PrazopagamentoWSBean> lista = prazopagamentoService.findForWSSOAP();
			for (PrazopagamentoWSBean prazopagamentoWSBean : lista) {
				respostaWSBean.addElement(prazopagamentoWSBean.transformToElement());
			}
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarFormaspagamento(WebRequestContext request, GenericEnvioInformacaoBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}

			List<DocumentotipoWSBean> lista = documentotipoService.findForWSSOAP();
			for (DocumentotipoWSBean documentotipoWSBean : lista) {
				respostaWSBean.addElement(documentotipoWSBean.transformToElement());
			}
						
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarEstoque(WebRequestContext request, ConsultarEstoqueBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String cnpj_empresa_string = bean.getEmpresa();
			if(cnpj_empresa_string == null || cnpj_empresa_string.trim().equals("")){
				throw new SinedException("Empresa � obrigat�ria.");
			}
			
			Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
			Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
			if(empresa == null){
				throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
			}
			
			Localarmazenagem localarmazenagem = null;
			if(bean.getLocalarmazenagem() != null){
				localarmazenagem = localarmazenagemService.load(new Localarmazenagem(bean.getLocalarmazenagem()), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo");
				if(localarmazenagem == null){
					throw new SinedException("Local n�o encontrada com o c�digo '" + bean.getLocalarmazenagem() + "'.");
				}
			}
			
			if(localarmazenagem == null){
				Integer cdlocalarmazenagem = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_LOCALARMAZENAGEM);
				if(cdlocalarmazenagem != null){
					localarmazenagem = localarmazenagemService.load(new Localarmazenagem(cdlocalarmazenagem), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo");
				}
			}
			
			List<ConsultarEstoqueWSBean> listaEstoque = movimentacaoestoqueService.findForWSSOAP(empresa, localarmazenagem, bean.getProdutos());
			for (ConsultarEstoqueWSBean consultarEstoqueWSBean : listaEstoque) {
				respostaWSBean.addElement(consultarEstoqueWSBean.transformToElement());
			}
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void baixarEstoque(WebRequestContext request, BaixarEstoqueBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Date data = bean.getData();
			if(data == null){
				throw new RuntimeException("Data � obrigat�ria.");
			}
			
			String empresa = bean.getEmpresa();
			if(empresa == null || empresa.trim().equals("")){
				throw new RuntimeException("Empresa � obrigat�ria.");
			}
			
			Integer codigo = bean.getCodigo();
			if(codigo == null){
				throw new RuntimeException("C�digo � obrigat�rio.");
			}
			
			String unidadeMedida = bean.getUnidadeMedida();
			if(unidadeMedida == null || unidadeMedida.trim().equals("")){
				throw new RuntimeException("Unidade de Medida � obrigat�ria.");
			}
			
			Double quantidade = bean.getQuantidade();
			if(quantidade == null){
				throw new RuntimeException("Quantidade � obrigat�ria.");
			}
			if(quantidade <= 0){
				throw new RuntimeException("Quantidade tem que ser maior que zero.");
			}
			
			Localarmazenagem localarmazenagem = null;
			if(bean.getLocalarmazenagem() != null){
				localarmazenagem = localarmazenagemService.load(new Localarmazenagem(bean.getLocalarmazenagem()), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo");
				if(localarmazenagem == null){
					throw new RuntimeException("Local n�o encontrada com o c�digo '" + bean.getLocalarmazenagem() + "'.");
				}
			}
			
			boolean sucesso = movimentacaoestoqueService.baixarEstoqueWSSOAP(bean);
			
			Element resultado = new Element("resultado");
			resultado.setText(sucesso + "");
			respostaWSBean.addElement(resultado);
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void manterPedidovenda(WebRequestContext request, ManterPedidovendaBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String empresa = bean.getEmpresa();
			if(empresa == null || empresa.trim().equals("")){
				throw new SinedException("Empresa � obrigat�ria.");
			}
			
			String identificador = bean.getIdentificador();
			if(identificador == null || identificador.trim().equals("")){
				throw new SinedException("Identificador � obrigat�ria.");
			}
			
			Integer cliente = bean.getCliente();
			if(cliente == null){
				throw new SinedException("Cliente � obrigat�rio.");
			}
			
			Integer vendedor = bean.getVendedor();
			if(vendedor == null){
				throw new SinedException("Vendedor � obrigat�rio.");
			}
			
			Date dataPedido = bean.getDataPedido();
			if(dataPedido == null){
				throw new SinedException("Data do Pedido � obrigat�rio.");
			}
			
			List<ManterPedidovendaItemBean> item = bean.getItem();
			if(item == null || item.size() == 0){
				throw new SinedException("� obrigat�rio enviar pelo menos 1 item.");
			}
			for (ManterPedidovendaItemBean itemBean : item) {
				if(itemBean.getCodigo() == null){
					throw new SinedException("C�digo do Item � obrigat�rio.");
				}
				if(itemBean.getIdentificador() == null){
					throw new SinedException("Identificador do Item � obrigat�rio.");
				}
				if(itemBean.getQuantidade() == null){
					throw new SinedException("Quantidade do Item � obrigat�rio.");
				}
				if(itemBean.getValor() == null){
					throw new SinedException("Valor do Item � obrigat�rio.");
				}
			}
			
			List<ManterPedidovendaParcelaBean> parcela = bean.getParcela();
			if(parcela == null || parcela.size() == 0){
				throw new SinedException("� obrigat�rio enviar pelo menos 1 parcela.");
			}
			for (ManterPedidovendaParcelaBean parcelaBean : parcela) {
				if(parcelaBean.getValor() == null){
					throw new SinedException("Valor da Parcela � obrigat�rio.");
				}
				if(parcelaBean.getVencimento() == null){
					throw new SinedException("Vencimento da Parcela � obrigat�rio.");
				}
				if(parcelaBean.getTipo() == null){
					throw new SinedException("Tipo da Parcela � obrigat�rio.");
				}
			}
			
			pedidovendaService.manterPedidovendaWSSOAP(bean);
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void faturarColeta(WebRequestContext request, FaturarColetaBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String empresa = bean.getEmpresa();
			if(empresa == null || empresa.trim().equals("")){
				throw new SinedException("Empresa � obrigat�ria.");
			}
			
			List<FaturarColetaItemBean> item = bean.getItem();
			if(item == null || item.size() == 0){
				throw new SinedException("� obrigat�rio enviar pelo menos 1 item.");
			}
			for (FaturarColetaItemBean itemBean : item) {
				if(itemBean.getIdentificador() == null){
					throw new SinedException("Identificador do Item � obrigat�rio.");
				}
			}
			
			pedidovendaService.faturarColetaWSSOAP(bean);
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void estornarProducao(WebRequestContext request, EstornarProducaoBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}

			String empresa = bean.getEmpresa();
			if(empresa == null || empresa.trim().equals("")){
				throw new RuntimeException("Empresa � obrigat�ria.");
			}
			
			String identificador = bean.getIdentificador();
			if(identificador == null || identificador.trim().equals("")){
				throw new RuntimeException("Identificador � obrigat�rio.");
			}
			
			List<EstornarProducaoItemBean> item = bean.getItem();
			if(item == null || item.size() == 0){
				throw new RuntimeException("� obrigat�rio enviar pelo menos 1 item.");
			}
			
			vendaService.cancelarVendaWSSOAP(bean);
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarSituacaoNota(WebRequestContext request, ConsultarSituacaoNotaBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String empresa = bean.getEmpresa();
			if(empresa == null || empresa.trim().equals("")){
				throw new RuntimeException("Empresa � obrigat�ria.");
			}
			
			Timestamp dataReferencia = bean.getDataReferencia();
			if(dataReferencia == null){
				throw new SinedException("Data de refer�ncia � obrigat�ria.");
			}
			
			List<ConsultarSituacaoNotaWSBean> lista = notaService.consultarSituacaoNotaWSSOAP(bean);
			for (ConsultarSituacaoNotaWSBean consultarSituacaoNotaWSBean : lista) {
				respostaWSBean.addElement(consultarSituacaoNotaWSBean.transformToElement());
			}
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarSituacaoPedido(WebRequestContext request, ConfirmarPedidovendaBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String empresa = bean.getEmpresa();
			if(empresa == null || empresa.trim().equals("")){
				throw new RuntimeException("Empresa � obrigat�ria.");
			}
			
			String identificador = bean.getIdentificador();
			if(identificador == null || identificador.trim().equals("")){
				throw new RuntimeException("Identificador � obrigat�rio.");
			}
			
			Integer[] situacoes = pedidovendaService.consultarSituacaoPedidoWSSOAP(bean);
			
			Element situacaoPedidoElt = new Element("situacaoPedido");
			situacaoPedidoElt.setText(situacoes[0] + "");
			respostaWSBean.addElement(situacaoPedidoElt);
			
			if(situacoes.length > 1){
				Element situacaoVendaElt = new Element("situacaoVenda");
				situacaoVendaElt.setText(situacoes[1] + "");
				respostaWSBean.addElement(situacaoVendaElt);
			}
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void confirmarPedidovenda(WebRequestContext request, ConfirmarPedidovendaBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String empresa = bean.getEmpresa();
			if(empresa == null || empresa.trim().equals("")){
				throw new RuntimeException("Empresa � obrigat�ria.");
			}
			
			String identificador = bean.getIdentificador();
			if(identificador == null || identificador.trim().equals("")){
				throw new RuntimeException("Identificador � obrigat�rio.");
			}
			
			List<ConfirmarPedidovendaItemBean> item = bean.getItem();
			if(item == null || item.size() == 0){
				throw new RuntimeException("� obrigat�rio enviar pelo menos 1 item.");
			}

			boolean sucesso = pedidovendaService.confirmarPedidovendaWSSOAP(bean);
			
			Element resultado = new Element("resultado");
			resultado.setText(sucesso + "");
			respostaWSBean.addElement(resultado);
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void cancelarPedidovenda(WebRequestContext request, CancelarPedidovendaBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}

			String empresa = bean.getEmpresa();
			if(empresa == null || empresa.trim().equals("")){
				throw new RuntimeException("Empresa � obrigat�ria.");
			}
			
			String identificador = bean.getIdentificador();
			if(identificador == null || identificador.trim().equals("")){
				throw new RuntimeException("Identificador � obrigat�rio.");
			}
			
			pedidovendaService.cancelarPedidovendaWSSOAP(bean);
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarServicos(WebRequestContext request, GenericEnvioInformacaoBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}

			List<ServicoWSBean> lista = materialService.findServicoForWSSOAP();
			for (ServicoWSBean servicoWSBean : lista) {
				respostaWSBean.addElement(servicoWSBean.transformToElement());
			}
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	public void consultarProdutos(WebRequestContext request, ConsultarProdutosBean bean){
		GenericRespostaWSBean respostaWSBean = new GenericRespostaWSBean();
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			List<ProdutoWSBean> lista = materialService.findProdutoForWSSOAP(bean.getDataReferencia());
			for (ProdutoWSBean produtoWSBean : lista) {
				respostaWSBean.addElement(produtoWSBean.transformToElement());
			}
			
			View.getCurrent().println(respostaWSBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaWSBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaWSBean, e);
		}
	}
	
	private void retornaMsgErro(GenericRespostaWSBean respostaInformacaoBean, Exception e) {
		e.printStackTrace();
		View.getCurrent().println(respostaInformacaoBean.responseErro(e));
	}

	private boolean validaTokenComunicacao(GenericEnvioInformacaoBean bean){
		String tokenValidacao = parametrogeralService.getValorPorNome(PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
		String hash = bean.getHash();
		return hash != null && tokenValidacao != null && hash.equals(tokenValidacao);
	}
	
}
