package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.CacheusuarioofflineService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.OfflineUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
/**
 * Controller respons�vel por gerar o arquivo offline.manifest
 * @author igorsilveriocosta
 *
 */
@Controller(path={"/pub/Offline.manifest"})
public class OfflineManifest extends MultiActionController {
	private ParametrogeralService parametrogeralService;
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	private static final String QUEBRA_LINHA = "\n";
	
		@DefaultAction
	public void index(WebRequestContext request) throws IOException {
////		InputStream is = NeoWeb.getRequestContext().getWebApplicationContext().getServletContext().getResourceAsStream("/WEB-INF/classes/version.properties");
//		Properties properties = new Properties();		 
//		properties.load(is);
//		String versao = properties.getProperty("offlineversion");
//		
//		
		HttpServletResponse response = request.getServletResponse();
		//definindo os dados dos cabe�alhos HTTP
		response.setContentType("text/cache-manifest");
		response.setHeader("CacheControl", "no-cache, must-revalidate");
		response.setDateHeader("Expires:", new Date().getTime());
		
		//verificando se houve alguma atualiza�ao nas entidades cacheada;
//		Long ultimaAtualizacaoEntidadeOffline = SinedUtil.getUltimaAtualizacaoEntidadeOffline();
		Long verificaCacheEntidadeOffline = parametrogeralService.getLong(Parametrogeral.OFFLINE_DT_MODIFICACAO_ENTIDADES);
		
		/**
		 * Verificando se os arquivos foram atualizados
		 */
		String ultimoCacheArquivoOffline = parametrogeralService.getValorPorNome(Parametrogeral.OFFLINE_DT_MODIFICACAO_ARQUIVOS);
		String ultimoCacheEntidadeOffline = SinedDateUtils.currentDate().toString();

		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioLogado!=null && usuarioLogado.getCdpessoa()!=null ){
			Timestamp timestampUsuario = CacheusuarioofflineService.getInstance().getDtalteraByUsuario(usuarioLogado.getCdpessoa());
			if(timestampUsuario != null)
				ultimoCacheEntidadeOffline = String.valueOf(timestampUsuario.getTime());
		}else{
			return ; //IMPORTANTE: Caso o usu�rio n�o esteja logado, dever� retornar a requisicao com erro para n�o atualizar o cache
		}
		//conte�do do arquivo
		StringBuilder sb = new StringBuilder();
		sb
		.append("CACHE MANIFEST").append(QUEBRA_LINHA)
		.append("CACHE:").append(QUEBRA_LINHA)
		.append("#v " + ultimoCacheArquivoOffline + " | " + ultimoCacheEntidadeOffline).append(QUEBRA_LINHA);
		//cache geral do w3
		//css
		if(ultimoCacheArquivoOffline!=null && !verificaCacheEntidadeOffline.equals(1L)){//adicionado para que quem tenha feito downlod do manifest por engano, n�o fac�a cache de nenhum arquivo
			sb.append("../css/app.css").append(QUEBRA_LINHA)
			.append("../css/theme.css").append(QUEBRA_LINHA)
			.append("../css/default.css").append(QUEBRA_LINHA)
			.append("../css/touchui.css").append(QUEBRA_LINHA)
			.append("../css/mobile/ipad.css").append(QUEBRA_LINHA)
			//js
			.append("../js/sined.js").append(QUEBRA_LINHA)
			.append("../resource/js/ajax.js").append(QUEBRA_LINHA)
			.append("../resource/js/input.js").append(QUEBRA_LINHA)
			.append("../resource/js/validate.js").append(QUEBRA_LINHA)
			.append("../resource/js/util.js").append(QUEBRA_LINHA)
			.append("../resource/js/dynatable.js").append(QUEBRA_LINHA)
			.append("../js/jquery.maskedinput-1.1.1.js").append(QUEBRA_LINHA)
			.append("../js/stringutils.js").append(QUEBRA_LINHA)
			
			//imagens
			.append("../imagens/icone/btnNovo.gif").append(QUEBRA_LINHA)
			.append("../imagens/icone/btnConciliar.png").append(QUEBRA_LINHA)
			.append("../imagens/consultar_icon.gif").append(QUEBRA_LINHA)
			.append("../imagens/down_arrow.png").append(QUEBRA_LINHA)
			.append("../imagens/sys/bg_headerbar.gif").append(QUEBRA_LINHA)
			.append("../imagens/alertgood_icon.gif").append(QUEBRA_LINHA)
			//autocomplete
			.append("../resource/js/autocomplete/jquery.autocomplete.js").append(QUEBRA_LINHA)
			.append("../resource/js/autocomplete/neo.autocomplete.js").append(QUEBRA_LINHA)
			.append("../resource/js/autocomplete/jquery.autocomplete.css").append(QUEBRA_LINHA)
			.append("../resource/js/autocomplete/jquery.js").append(QUEBRA_LINHA)
			.append("../resource/js/autocomplete/autocomplete_exclamacao.png").append(QUEBRA_LINHA)
			.append("../resource/js/autocomplete/autocomplete_excluir.png").append(QUEBRA_LINHA)
			.append("../resource/js/autocomplete/autocomplete_selecionado.png").append(QUEBRA_LINHA)
			.append("../resource/js/autocomplete/thickbox.css").append(QUEBRA_LINHA)
			.append("../resource/js/autocomplete/thickbox-compressed.js").append(QUEBRA_LINHA)
			//tela de pedidovenda
			.append("../imagens/sys/w3erp.png").append(QUEBRA_LINHA)
			.append("../imagens/icone/btnBaixar.gif").append(QUEBRA_LINHA)
			.append("../imagens/excluir_detalhe.gif").append(QUEBRA_LINHA)
			.append("../imagens/icone/btnCancelar.png").append(QUEBRA_LINHA)
			.append("../imagens/icone/btnVoltar.gif").append(QUEBRA_LINHA)
			.append("../js/offline/offline.js").append(QUEBRA_LINHA)
			.append("../js/offline/jquery.md5.js").append(QUEBRA_LINHA)
			.append("../js/offline/store+json2.min.js").append(QUEBRA_LINHA)
			.append("../js/offline/touchui.js").append(QUEBRA_LINHA)
			.append("../pub/Login").append(QUEBRA_LINHA)
			.append("../offline/crud/Pedidovenda?ACAO=criar").append(QUEBRA_LINHA)
			.append("../offline/crud/Pedidovenda?ACAO=listagem").append(QUEBRA_LINHA)
			.append("../offline/crud/Pedidovenda?ACAO=popupGridMaterial&popup=1").append(QUEBRA_LINHA)
			.append("../offline/crud/Cliente?ACAO=criar").append(QUEBRA_LINHA)
			//font
			.append("../font/APEX_NEW_BOOK.otf").append(QUEBRA_LINHA)
			.append("../font/APEX_NEW_HEAVY.otf").append(QUEBRA_LINHA)
			.append("../font/APEX_NEW_LIGHT.otf").append(QUEBRA_LINHA)
			.append("../font/APEX_NEW_MEDIUM.otf").append(QUEBRA_LINHA)
			.append("../font/APEX_NEW_THIN.otf").append(QUEBRA_LINHA)
			
	//		.append("../adm/index").append(QUEBRA_LINHA) 
			.append(OfflineUtil.getOfflineFilesString()).append(QUEBRA_LINHA);
			
	//		if(request.getRequestQuery().contains("Logged"))
	//			sb.append(getEntitiesJSONFiles());
		}
		sb
		.append("NETWORK:").append(QUEBRA_LINHA)
		.append("*");

		
		try {
			response.getWriter().print(sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}