package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import br.com.linkcom.neo.authorization.AuthorizationDAO;
import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresaproprietario;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutocorrecao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.UsuarioDAO;
import br.com.linkcom.sined.geral.service.ArquivoMdfeService;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ArquivonfService;
import br.com.linkcom.sined.geral.service.ArquivonfnotaService;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.InutilizacaonfeService;
import br.com.linkcom.sined.geral.service.LoteConsultaDfeHistoricoService;
import br.com.linkcom.sined.geral.service.LoteConsultaDfeService;
import br.com.linkcom.sined.geral.service.ManifestoDfeEventoService;
import br.com.linkcom.sined.geral.service.ManifestoDfeHistoricoService;
import br.com.linkcom.sined.geral.service.ManifestoDfeService;
import br.com.linkcom.sined.geral.service.MdfeService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutocorrecaoService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeEvento;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.TipoEventoNfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.ManifestoDfeSituacaoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultaDadosBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EnviarArquivoMdfeBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EnviarArquivonfBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EnviarArquivonfnotaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EnviarCartacorrecaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EnviarInutilizacaonfeBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.LoginBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.MarcarFlagBean;
import br.com.linkcom.sined.util.CriptografiaEmissorUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;


@Bean
@Controller(path="/pub/process/EmissorNotaFiscalEletronica")
public class EmissorNotaFiscalEletronicaProcess extends MultiActionController {
	
	public static final String VERSAO_ATUAL = "11.8";

	private EmpresaService empresaService;
	private ConfiguracaonfeService configuracaonfeService;
	private ArquivonfService arquivonfService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private ArquivonfnotaService arquivonfnotaService;
	private InutilizacaonfeService inutilizacaonfeService;
	private NotafiscalprodutocorrecaoService notafiscalprodutocorrecaoService;
	private AvisoService avisoService;
	private MotivoavisoService motivoavisoService;
	private MdfeService mdfeService;
	private ArquivoMdfeService arquivoMdfeService;
	private LoteConsultaDfeService loteConsultaDfeService;
	private ManifestoDfeEventoService manifestoDfeEventoService;
	private ManifestoDfeHistoricoService manifestoDfeHistoricoService;
	private LoteConsultaDfeHistoricoService loteConsultaDfeHistoricoService;
	private ManifestoDfeService manifestoDfeService;
	
	public void setManifestoDfeEventoService(ManifestoDfeEventoService manifestoDfeEventoService) {
		this.manifestoDfeEventoService = manifestoDfeEventoService;
	}

	public void setNotafiscalprodutocorrecaoService(NotafiscalprodutocorrecaoService notafiscalprodutocorrecaoService) {
		this.notafiscalprodutocorrecaoService = notafiscalprodutocorrecaoService;
	}
	
	public void setInutilizacaonfeService(InutilizacaonfeService inutilizacaonfeService) {
		this.inutilizacaonfeService = inutilizacaonfeService;
	}
	
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	public void setArquivonfService(ArquivonfService arquivonfService) {
		this.arquivonfService = arquivonfService;
	}
	
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	
	public void setArquivoMdfeService(ArquivoMdfeService arquivoMdfeService) {
		this.arquivoMdfeService = arquivoMdfeService;
	}
	
	public void setLoteConsultaDfeService(LoteConsultaDfeService loteConsultaDfeService) {
		this.loteConsultaDfeService = loteConsultaDfeService;
	}
	
	public void setManifestoDfeHistoricoService(ManifestoDfeHistoricoService manifestoDfeHistoricoService) {
		this.manifestoDfeHistoricoService = manifestoDfeHistoricoService;
	}
	
	public MdfeService getMdfeService() {
		return mdfeService;
	}
	
	public void setLoteConsultaDfeHistoricoService(LoteConsultaDfeHistoricoService loteConsultaDfeHistoricoService) {
		this.loteConsultaDfeHistoricoService = loteConsultaDfeHistoricoService;
	}
	
	public void setManifestoDfeService(ManifestoDfeService manifestoDfeService) {
		this.manifestoDfeService = manifestoDfeService;
	}
	
	/**
	 * Realiza o login do emissor de nota fiscal
	 *
	 * @param request
	 * @param bean
	 * @since 14/08/2012
	 * @author Rodrigo Freitas
	 */
	public void login(WebRequestContext request, LoginBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("UTF-8");
		
		try{
			if(bean.getLogin() == null || bean.getLogin().equals("") || 
					bean.getSenha() == null || bean.getSenha().equals("") || 
					!CriptografiaEmissorUtil.verificaCriptografia(bean.getLogin(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(!SinedUtil.isAmbienteDesenvolvimento()){
				AuthorizationDAO authorizationDAO = (AuthorizationDAO) Neo.getApplicationContext().getBean("authorizationDAO");
				User user = authorizationDAO.findUserByLogin(bean.getLogin());
				
				if (user != null) {
					boolean passwordMatch = false;
					StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
					
					if(user.getPassword().matches("[a-zA-Z0-9\\+/]{64}")) {
						// Jasypt Strong Encryption
						if (passwordEncryptor.checkPassword(bean.getSenha(), user.getPassword())) {
							passwordMatch = true;
						}
					}
					else if(user.getPassword().matches("[0-9a-f]{32}")) {
						// MD5 simples
						if (Util.crypto.makeHashMd5(bean.getSenha()).equals(user.getPassword())) {
							passwordMatch = true;
						}
					}
					else {
						if (user.getPassword().equals(bean.getSenha())) {
							passwordMatch = true;
						}
					}
					
					if(!passwordMatch){
						throw new SinedException("Login/senha inv�lidos.");
					}
					
					if (user instanceof Usuario){				
						Usuario usuario = UsuarioDAO.getInstance().load((Usuario)user);
						if (usuario.getBloqueado().equals(true)) {					
							throw new SinedException("Usu�rio bloqueado.");					
						}			
					} else throw new SinedException("Login/senha inv�lidos.");
				} else throw new SinedException("Login/senha inv�lidos.");
			}

			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * A��o que realiza a busca pelas as empresas cadastradas.
	 *
	 * @param request
	 * @param bean
	 * @since 07/08/2012
	 * @author Rodrigo Freitas
	 */
	public void consultaEmpresa(WebRequestContext request, GenericEnvioInformacaoBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("consultaEmpresa", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element empresaElement, cdempresaElement, nomeElement, cnpjElement, cpfElement;
			List<Empresa> listaEmpresa = empresaService.findAtivosSemFiltroProprietarioRural();
			for (Empresa empresa : listaEmpresa) {
				empresaElement = new Element("Empresa");
				
				cdempresaElement = new Element("Cdempresa");
				nomeElement = new Element("Nome");
				cnpjElement = new Element("Cnpj");
				cpfElement = new Element("Cpf");

				cdempresaElement.setText(empresa.getCdpessoa() + "");
				String nome = empresa.getNomefantasia();
				if(nome == null || nome.trim().equals("")){
					nome = empresa.getNome();
				}
				nomeElement.setText(Util.strings.tiraAcento(nome));
				cnpjElement.setText(empresa.getCnpj() != null ? empresa.getCnpj().toString() : null);
				
				String cpf = "";
				
				if (empresa.getPropriedadeRural() != null && empresa.getPropriedadeRural() && empresa.getListaEmpresaproprietario() != null && empresa.getListaEmpresaproprietario().size() > 0) {
					if (empresa.getListaEmpresaproprietario().size() == 1 && empresa.getListaEmpresaproprietario().get(0) != null && empresa.getListaEmpresaproprietario().get(0).getColaborador() != null) {
						cpf = empresa.getListaEmpresaproprietario().get(0).getColaborador().getCpf() != null ? empresa.getListaEmpresaproprietario().get(0).getColaborador().getCpf().toString() : null;
					} else {
						for (Empresaproprietario e : empresa.getListaEmpresaproprietario()) {
							if (e.getPrincipal() != null && e.getPrincipal() && e.getColaborador() != null) {
								cpf = e.getColaborador().getCpf() != null ? e.getColaborador().getCpf().toString() : null;
								break;
							}
						}
					}
				} else {
					cpf = empresa.getCpf() != null ? empresa.getCpf().toString() : null;
				}
				
				cpfElement.setText(StringUtils.isNotEmpty(cpf) ? cpf : null);
				
				empresaElement.addContent(cdempresaElement);
				empresaElement.addContent(nomeElement);
				empresaElement.addContent(cnpjElement);
				empresaElement.addContent(cpfElement);
				
				resposta.addContent(empresaElement);
			}
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * A��o que realiza a busca pelas configura��es de NF-e cadastradas.
	 *
	 * @param request
	 * @param bean
	 * @since 07/08/2012
	 * @author Rodrigo Freitas
	 */
	public void consultaConfiguracaonfe(WebRequestContext request, GenericEnvioInformacaoBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("consultaConfiguracaonfe", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			List<Configuracaonfe> listaConfiguracaonfe = configuracaonfeService.findAllForWebservice();
			
			for (Configuracaonfe configuracaonfe : listaConfiguracaonfe) {
				Element configuracaonfeElement = new Element("Configuracaonfe");
				
				Element cdconfiguracaonfeElement = new Element("Cdconfiguracaonfe");
				Element descricaoElement = new Element("Descricao");
				Element tipoconfiguracaonfeElement = new Element("Tipoconfiguracaonfe");
				Element prefixowebserviceElement = new Element("Prefixowebservice");
				Element inscricaomunicipalElement = new Element("Inscricaomunicipal");
				Element nfev4Element = new Element("Nfev4");
				Element tokenElement = new Element("Token");
				Element loginparacatuElement = new Element("Loginparacatu");
				Element senhaparacatuElement = new Element("Senhaparacatu");
				Element cdempresaElement = new Element("Cdempresa");
				
				cdconfiguracaonfeElement.setText(configuracaonfe.getCdconfiguracaonfe() + "");
				descricaoElement.setText(Util.strings.tiraAcento(configuracaonfe.getDescricao()));
				tipoconfiguracaonfeElement.setText(configuracaonfe.getTipoconfiguracaonfe().ordinal() + "");
				prefixowebserviceElement.setText(configuracaonfe.getPrefixowebservice() != null ? configuracaonfe.getPrefixowebservice().ordinal() + "" : "");
				inscricaomunicipalElement.setText(configuracaonfe.getInscricaomunicipal() != null ? configuracaonfe.getInscricaomunicipal() : "");
				nfev4Element.setText("true");
				tokenElement.setText(configuracaonfe.getToken() != null ? configuracaonfe.getToken() : "");
				loginparacatuElement.setText(configuracaonfe.getLoginparacatu() != null ? configuracaonfe.getLoginparacatu() : "");
				senhaparacatuElement.setText(configuracaonfe.getSenhaparacatu() != null ? configuracaonfe.getSenhaparacatu() : "");
				cdempresaElement.setText(Util.objects.isPersistent(configuracaonfe.getEmpresa()) ? configuracaonfe.getEmpresa().getCdpessoa().toString() : "");
				
				configuracaonfeElement.addContent(cdconfiguracaonfeElement);
				configuracaonfeElement.addContent(descricaoElement);
				configuracaonfeElement.addContent(tipoconfiguracaonfeElement);
				configuracaonfeElement.addContent(prefixowebserviceElement);
				configuracaonfeElement.addContent(inscricaomunicipalElement);
				configuracaonfeElement.addContent(nfev4Element);
				configuracaonfeElement.addContent(tokenElement);
				configuracaonfeElement.addContent(loginparacatuElement);
				configuracaonfeElement.addContent(senhaparacatuElement);
				configuracaonfeElement.addContent(cdempresaElement);
				
				if(configuracaonfe.getUf() != null){
					Element codigoufElement = new Element("Codigouf");
					codigoufElement.setText(configuracaonfe.getUf().getCdibge() + "");
					configuracaonfeElement.addContent(codigoufElement);
				}
				
				if(configuracaonfe.getEmpresa() != null && 
						configuracaonfe.getEmpresa().getCnpj() != null && 
						configuracaonfe.getSenha() != null && 
						!configuracaonfe.getSenha().trim().equals("")){
					Element identificacaoPrestadorElement = new Element("IdentificacaoPrestador");
					identificacaoPrestadorElement.setText(configuracaonfe.getEmpresa().getCnpj().getValue());
					configuracaonfeElement.addContent(identificacaoPrestadorElement);
					
					Element senhaPrestadorElement = new Element("SenhaPrestador");
					senhaPrestadorElement.setText(configuracaonfe.getSenha());
					configuracaonfeElement.addContent(senhaPrestadorElement);
				}
				
				resposta.addContent(configuracaonfeElement);
			}
			
			String respostaXML = SinedUtil.getRespostaXML(resposta);
			View.getCurrent().println(respostaXML);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	/**
	 * A��o que marca a flag "emitindo" do Arquivonf.
	 *
	 * @param request
	 * @param bean
	 * @since 07/08/2012
	 * @author Rodrigo Freitas
	 */
	public void marcarFlagArquivonf(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivonf", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivonfService.marcarArquivonf(bean.getWhereInArquivonf(), "emitindo");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagArquivonfConsulta(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivonfConsulta", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivonfService.marcarArquivonf(bean.getWhereInArquivonf(), "consultandolote");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagArquivoMdfe(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivoMdfe", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivoMdfeService.marcarArquivoMdfe(bean.getWhereInArquivoMdfe(), "emitindo");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagArquivoMdfeConsulta(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivoMdfeConsulta", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivoMdfeService.marcarArquivoMdfe(bean.getWhereInArquivoMdfeConsulta(), "consultando");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagArquivoMdfeEncerramento(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivoMdfeEncerramento", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivoMdfeService.marcarArquivoMdfe(bean.getWhereInArquivoMdfeEncerramento(), "encerrando");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagArquivoMdfeCancelamento(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivoMdfeCancelamento", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivoMdfeService.marcarArquivoMdfe(bean.getWhereInArquivoMdfeCancelamento(), "cancelando");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagArquivoMdfeInclusaoCondutor(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivoMdfeInclusaoCondutor", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivoMdfeService.marcarArquivoMdfe(bean.getWhereInArquivoMdfeInclusaoCondutor(), "incluindoCondutor");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * A��o para a aplica��o Desktop de NF-e enviar o arquivo assinado para salvar.
	 * 
	 * @param request
	 * @param bean
	 * @since 10/08/2012
	 * @author Rodrigo Freitas
	 */
	public void enviarArquivonfAssinado(WebRequestContext request, EnviarArquivonfBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivonf() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivonf().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivonf arquivonf = new Arquivonf();
			arquivonf.setCdarquivonf(bean.getCdarquivonf());
			arquivonf = arquivonfService.loadForEntradaWriter(arquivonf);
			
			byte[] content = bean.getArquivo().getContent();
			if(arquivonf.getConfiguracaonfe() != null && 
					arquivonf.getConfiguracaonfe().getTipoconfiguracaonfe() != null &&
					arquivonf.getConfiguracaonfe().getTipoconfiguracaonfe().equals(Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR)){
				Element rootElement = arquivonfService.adicionarQrCode(arquivonf, content);
				String xmlAssinado = new XMLOutputter().outputString(rootElement);
				content = xmlAssinado.getBytes();
			}
			Arquivo arquivo = new Arquivo(content, "nfassinada_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			arquivonf.setArquivoxmlassinado(arquivo);
			
			arquivoDAO.saveFile(arquivonf, "arquivoxmlassinado");
			arquivoService.saveOrUpdate(arquivo);
			arquivonfService.updateArquivoxmlassinado(arquivonf);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoxmlassinadoElement = new Element("Arquivoxmlassinado");
			arquivoxmlassinadoElement.setText(arquivo.getCdarquivo() + "");
			resposta.addContent(arquivoxmlassinadoElement);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarArquivoMdfeAssinado(WebRequestContext request, EnviarArquivoMdfeBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivomdfe() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivomdfe().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfassinada_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			ArquivoMdfe arquivoMdfe = new ArquivoMdfe();
			arquivoMdfe.setCdArquivoMdfe(bean.getCdarquivomdfe());
			arquivoMdfe.setArquivoxmlassinado(arquivo);
			
			arquivoDAO.saveFile(arquivoMdfe, "arquivoxmlassinado");
			arquivoService.saveOrUpdate(arquivo);
			arquivoMdfeService.updateArquivoxmlassinado(arquivoMdfe);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoxmlassinadoElement = new Element("Arquivoxmlassinado");
			arquivoxmlassinadoElement.setText(arquivo.getCdarquivo() + "");
			resposta.addContent(arquivoxmlassinadoElement);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * A��o para a aplica��o Desktop de NF-e enviar o arquivo de retorno do envio para salvar.
	 * Est� a��o retorna tamb�m os arquivos de consulta do Arquivonf.
	 * 
	 *
	 * @param request
	 * @param bean
	 * @since 13/08/2012
	 * @author Rodrigo Freitas
	 */
	public void enviarArquivonfRetornoEnvio(WebRequestContext request, EnviarArquivonfBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivonf() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivonf().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfretornoenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			Arquivonf arquivonf = new Arquivonf();
			arquivonf.setCdarquivonf(bean.getCdarquivonf());
			arquivonf.setArquivoretornoenvio(arquivo);
			
			arquivoDAO.saveFile(arquivonf, "arquivoretornoenvio");
			arquivoService.saveOrUpdate(arquivo);
			arquivonfService.updateArquivoretornoenvio(arquivonf);
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivonf(arquivonf);
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			if(prefixowebservice.equals(Prefixowebservice.BHISS_HOM) || prefixowebservice.equals(Prefixowebservice.BHISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RECIFEISS_PROD) ||  
					prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOLOURENCODASERRAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOLOURENCODASERRAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CAMBUI_HOM) || prefixowebservice.equals(Prefixowebservice.CAMBUI_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JOAO_MONLEVADE_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.JOAO_MONLEVADE_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ARARASISS_HOM) || prefixowebservice.equals(Prefixowebservice.ARARASISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOAODABOAVISTAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOAODABOAVISTAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BALNEARIOCAMBORIUISS_HOM) || prefixowebservice.equals(Prefixowebservice.BALNEARIOCAMBORIUISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MARIANAISS_HOM) || prefixowebservice.equals(Prefixowebservice.MARIANAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BARBACENA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.BARBACENA_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CURITIBAISS_HOM) || prefixowebservice.equals(Prefixowebservice.CURITIBAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.PORTOALEGRE_HOM) || prefixowebservice.equals(Prefixowebservice.PORTOALEGRE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_HOM) || prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.IPATINGA_HOM) || prefixowebservice.equals(Prefixowebservice.IPATINGA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CABEDELOISS_HOM) || prefixowebservice.equals(Prefixowebservice.CABEDELOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_HOM) || prefixowebservice.equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITUMBIARA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.ITUMBIARA_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BETIMISS_HOM) || prefixowebservice.equals(Prefixowebservice.BETIMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MONLEVADEISS_HOM) || prefixowebservice.equals(Prefixowebservice.MONLEVADEISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BARBACENAISS_HOM) || prefixowebservice.equals(Prefixowebservice.BARBACENAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ARACAJUISS_HOM) || prefixowebservice.equals(Prefixowebservice.ARACAJUISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITUMBIARA_HOM) || prefixowebservice.equals(Prefixowebservice.ITUMBIARA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SIMOESFILHOISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SIMOESFILHOISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITAUNAISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITAUNAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FORMIGAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FORMIGAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.UBERABA_HOM) || prefixowebservice.equals(Prefixowebservice.UBERABA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SALVADORISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALVADORISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIODEJANEIROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIODEJANEIROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MACEIOISS_HOM) || prefixowebservice.equals(Prefixowebservice.MACEIOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.DIADEMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.DIADEMAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MINEIROSISS_HOM) || prefixowebservice.equals(Prefixowebservice.MINEIROSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_HOM) || prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.REGISTROISS_HOM) || prefixowebservice.equals(Prefixowebservice.REGISTROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTAREMISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTAREMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FRANCAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FRANCAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FORTALEZA_HOM) || prefixowebservice.equals(Prefixowebservice.FORTALEZA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.COTIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.COTIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_HOM) || prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_HOM) || prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ERECHIM_HOM) || prefixowebservice.equals(Prefixowebservice.ERECHIM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.DIVINOPOLISISS_HOM) || prefixowebservice.equals(Prefixowebservice.DIVINOPOLISISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.OUROPRETO_HOM) || prefixowebservice.equals(Prefixowebservice.OUROPRETO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CAMACARI_HOM) || prefixowebservice.equals(Prefixowebservice.CAMACARI_PROD) ||
					prefixowebservice.equals(Prefixowebservice.AVARE_HOM) || prefixowebservice.equals(Prefixowebservice.AVARE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CONGONHAS_HOM) || prefixowebservice.equals(Prefixowebservice.CONGONHAS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITAJAI_PROD) || prefixowebservice.equals(Prefixowebservice.ITAJAI_HOM) ||
					prefixowebservice.equals(Prefixowebservice.IVOTI_PROD) || prefixowebservice.equals(Prefixowebservice.IVOTI_HOM) || 
					prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MANAUS_HOM) || prefixowebservice.equals(Prefixowebservice.MANAUS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JUAZEIRO_HOM) || prefixowebservice.equals(Prefixowebservice.JUAZEIRO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CHAPECO_HOM) || prefixowebservice.equals(Prefixowebservice.CHAPECO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BOA_VISTA_HOM) || prefixowebservice.equals(Prefixowebservice.BOA_VISTA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.GOIANIA_HOM) || prefixowebservice.equals(Prefixowebservice.GOIANIA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JOAO_PESSOA_HOM) || prefixowebservice.equals(Prefixowebservice.JOAO_PESSOA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_HOM) || prefixowebservice.equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.PETROLINA_HOM) || prefixowebservice.equals(Prefixowebservice.PETROLINA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPA_NOVO_PROD)) {
				arquivonfService.processaRetornoEnvioBhiss(arquivonf, bean.getArquivo().getContent(), configuracaonfe);
            } else if (prefixowebservice.equals(Prefixowebservice.CASTELO_HOM) || prefixowebservice.equals(Prefixowebservice.CASTELO_PROD)) {
                arquivonfService.processaRetornoEnvioEel2(arquivonf, bean.getArquivo().getContent(), configuracaonfe);
            } else if(prefixowebservice.equals(Prefixowebservice.SEFAZBA_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZBA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZMG_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZMG_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZAM_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZAM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZRS_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZRS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SVAN_HOM) || prefixowebservice.equals(Prefixowebservice.SVAN_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SVRS_HOM) || prefixowebservice.equals(Prefixowebservice.SVRS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZGO_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZGO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZPR_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZPR_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZPE_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZPE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZSP_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZSP_PROD)){
				arquivonfService.processaRetornoEnvioSefaz(arquivonf, bean.getArquivo().getContent(), configuracaonfe);
			} else if(prefixowebservice.equals(Prefixowebservice.SPISS_HOM) || prefixowebservice.equals(Prefixowebservice.SPISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_HOM) || prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_PROD)){
				arquivonfService.processaRetornoEnvioSpiss(arquivonf, bean.getArquivo().getContent());
			} else if(prefixowebservice.equals(Prefixowebservice.SALTO_HOM) || prefixowebservice.equals(Prefixowebservice.SALTO_PROD)){
				arquivonfService.processaRetornoEnvioObaratec(arquivonf, bean.getArquivo().getContent());
			} else if(prefixowebservice.equals(Prefixowebservice.SERRAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SERRAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_HOM) || prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JANDIRA_HOM) || prefixowebservice.equals(Prefixowebservice.JANDIRA_PROD)){
				arquivonfService.processaRetornoEnvioSmarapd(arquivonf, bean.getArquivo().getContent(), configuracaonfe);
			} else if(prefixowebservice.equals(Prefixowebservice.CAMPINASISS_HOM) || prefixowebservice.equals(Prefixowebservice.CAMPINASISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SOROCABAISS_PROD) || prefixowebservice.equals(Prefixowebservice.SOROCABAISS_HOM)){
				arquivonfService.processaRetornoEnvioCampinasiss(arquivonf, bean.getArquivo().getContent(), configuracaonfe);
			} else if(prefixowebservice.equals(Prefixowebservice.CURVELO_HOM) || prefixowebservice.equals(Prefixowebservice.CURVELO_PROD)){
				arquivonfService.processaRetornoEnvioMemory(arquivonf, bean.getArquivo().getContent(), configuracaonfe);
			} else if (prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_PROD)) {
				arquivonfService.processaRetornoEnvioAssessorPublico(arquivonf, bean.getArquivo().getContent(), configuracaonfe);
			} else if (prefixowebservice.equals(Prefixowebservice.LIMEIRA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.LIMEIRA_NOVO_PROD)) {
				arquivonfService.processaRetornoEnvioIiBrasil(arquivonf, bean.getArquivo().getContent(), configuracaonfe);
			}
			
			List<String> listaUrlImpressao = new ArrayList<String>();
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			
			for (Arquivonfnota arquivonfnota : listan) {
				if(arquivonfnota != null &&
						arquivonfnota.getNota() != null &&
						arquivonfnota.getNota().getCdNota() != null &&
						arquivonfnota.getNota().getNotaStatus() != null &&
						arquivonfnota.getNota().getNotaStatus().getCdNotaStatus() != null){
					Integer cdnota = arquivonfnota.getNota().getCdNota();
					
					String hash = Util.crypto.makeHashMd5(cdnota.toString());
					String hash2 = Util.crypto.makeHashMd5(hash);
					String hash3 = Util.crypto.makeHashMd5(hash2);
					
					if(arquivonfnota.getNota().getNotaStatus().equals(NotaStatus.NFE_EMITIDA) || arquivonfnota.getNota().getNotaStatus().equals(NotaStatus.NFE_LIQUIDADA)){
						listaUrlImpressao.add(SinedUtil.getUrlWithContext() + "/pub/relatorio/DanfePub?ACAO=gerar&cdnota=" + cdnota + "&hash=" + hash3);
					} else if(arquivonfnota.getNota().getNotaStatus().equals(NotaStatus.NFSE_EMITIDA) || arquivonfnota.getNota().getNotaStatus().equals(NotaStatus.NFSE_LIQUIDADA)){
						listaUrlImpressao.add(SinedUtil.getUrlWithContext() + "/pub/relatorio/NotaFiscalEletronicaPub?ACAO=gerar&cdnota=" + cdnota + "&hash=" + hash3);
					}
				}
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			if(listaUrlImpressao != null && listaUrlImpressao.size() > 0){
				Element impressaoElt = new Element("Impressao");
				
				for (String url : listaUrlImpressao) {
					Element urlElt = new Element("URL");
					urlElt.setText(url);
					impressaoElt.addContent(urlElt);
				}
				
				resposta.addContent(impressaoElt);
			}
			
			arquivonf = arquivonfService.loadForEntradaWriter(arquivonf);
			
			if(arquivonf.getArquivoxmlconsultasituacao() != null){
				Element arquivoxmlconsultasituacao = new Element("Arquivoxmlconsultasituacao");
				arquivoxmlconsultasituacao.setText(arquivonf.getArquivoxmlconsultasituacao().getCdarquivo() + "");			
				resposta.addContent(arquivoxmlconsultasituacao);
			}
			
			if(arquivonf.getArquivoxmlconsultalote() != null){
				Element arquivoxmlconsultalote = new Element("Arquivoxmlconsultalote");
				arquivoxmlconsultalote.setText(arquivonf.getArquivoxmlconsultalote().getCdarquivo() + "");			
				resposta.addContent(arquivoxmlconsultalote);
			}
			
			if(arquivonf.getArquivoretornoenvio() != null){
				Element arquivoretornoenvio = new Element("Arquivoretornoenvio");
				arquivoretornoenvio.setText(arquivonf.getArquivoretornoenvio().getCdarquivo() + "");			
				resposta.addContent(arquivoretornoenvio);
			}
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarArquivoMdfeRetornoEnvio(WebRequestContext request, EnviarArquivoMdfeBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivomdfe() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivomdfe().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfretornoenvio_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			ArquivoMdfe arquivoMdfe = new ArquivoMdfe();
			arquivoMdfe.setCdArquivoMdfe(bean.getCdarquivomdfe());
			arquivoMdfe.setArquivoretornoenvio(arquivo);
			
			arquivoDAO.saveFile(arquivoMdfe, "arquivoretornoenvio");
			arquivoService.saveOrUpdate(arquivo);
			arquivoMdfeService.updateArquivoretornoenvio(arquivoMdfe);
			
			arquivoMdfe = arquivoMdfeService.loadForEntrada(arquivoMdfe);
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivoMdfe(arquivoMdfe);
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			if (Prefixowebservice.SEFAZMDFE_HOM.equals(prefixowebservice) || Prefixowebservice.SEFAZMDFE_PROD.equals(prefixowebservice)) {
				arquivoMdfeService.processaRetorno(arquivoMdfe, bean.getArquivo().getContent(), configuracaonfe);
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			if(arquivoMdfe.getArquivoxmlconsultasituacao() != null){
				Element arquivoxmlconsultasituacao = new Element("Arquivoxmlconsultasituacao");
				arquivoxmlconsultasituacao.setText(arquivoMdfe.getArquivoxmlconsultasituacao().getCdarquivo() + "");			
				resposta.addContent(arquivoxmlconsultasituacao);
			}
			
			if(arquivoMdfe.getArquivoxmlconsultalote() != null){
				Element arquivoxmlconsultalote = new Element("Arquivoxmlconsultalote");
				arquivoxmlconsultalote.setText(arquivoMdfe.getArquivoxmlconsultalote().getCdarquivo() + "");			
				resposta.addContent(arquivoxmlconsultalote);
			}
			
			if(arquivoMdfe.getArquivoretornoenvio() != null){
				Element arquivoretornoenvio = new Element("Arquivoretornoenvio");
				arquivoretornoenvio.setText(arquivoMdfe.getArquivoretornoenvio().getCdarquivo() + "");			
				resposta.addContent(arquivoretornoenvio);
			}
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
			
			try {
				SinedUtil.enviarEmailErro("Erro emissor w3erp", "Erro ao executar enviarArquivoMdfeRetornoEnvio", e);
			} catch (Exception e2) {
				e.printStackTrace();
			}
		}
	}

	
	/**
	 * A��o para enviar o retorno da consulta de situa��o do lote.
	 *
	 * @param request
	 * @param bean
	 * @since 14/08/2012
	 * @author Rodrigo Freitas
	 */
	public void enviarArquivonfRetornoSituacao(WebRequestContext request, EnviarArquivonfBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivonf() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivonf().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getXml() == null){
				throw new SinedException("XML n�o enviado.");
			}
			
			boolean processado = true;
			if(bean.getXml().trim().equals("")){
				processado = false;
			} else {
				try{
					Arquivo arquivo = new Arquivo(bean.getXml().getBytes(), "nfretornoconsultasituacao_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
					
					Arquivonf arquivonf = new Arquivonf();
					arquivonf.setCdarquivonf(bean.getCdarquivonf());
					arquivonf.setArquivoretornoconsultasituacao(arquivo);
					
					try {
						arquivoDAO.saveFile(arquivonf, "arquivoretornoconsultasituacao");
					} catch (Exception e) {
//						System.out.println("## ERRO NA CONSULTA DE SITUACAO ## ID: " + bean.getCdarquivonf() + " URL: " + SinedUtil.getUrlWithoutContext());
						throw e;
					}
					arquivoService.saveOrUpdate(arquivo);
					arquivonfService.updateArquivoretornoconsultasituacao(arquivonf);
					
					Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivonf(arquivonf);
					Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
					
					if(prefixowebservice.equals(Prefixowebservice.BHISS_HOM) || prefixowebservice.equals(Prefixowebservice.BHISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.RECIFEISS_PROD) || 
							prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_NOVO_PROD) ||
							prefixowebservice.equals(Prefixowebservice.MARIANAISS_HOM) || prefixowebservice.equals(Prefixowebservice.MARIANAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.BARBACENA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.BARBACENA_NOVO_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.CURITIBAISS_HOM) || prefixowebservice.equals(Prefixowebservice.CURITIBAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.CABEDELOISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.PORTOALEGRE_HOM) || prefixowebservice.equals(Prefixowebservice.PORTOALEGRE_PROD) ||
							prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_HOM) || prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_PROD) ||
							prefixowebservice.equals(Prefixowebservice.IPATINGA_HOM) || prefixowebservice.equals(Prefixowebservice.IPATINGA_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_PROD) ||
							prefixowebservice.equals(Prefixowebservice.CURVELO_HOM) || prefixowebservice.equals(Prefixowebservice.CURVELO_PROD) ||
							prefixowebservice.equals(Prefixowebservice.BETIMISS_HOM) || prefixowebservice.equals(Prefixowebservice.BETIMISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.MONLEVADEISS_HOM) || prefixowebservice.equals(Prefixowebservice.MONLEVADEISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.BARBACENAISS_HOM) || prefixowebservice.equals(Prefixowebservice.BARBACENAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.ITAUNAISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITAUNAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.FORMIGAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FORMIGAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.UBERABA_HOM) || prefixowebservice.equals(Prefixowebservice.UBERABA_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SALVADORISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALVADORISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.RIODEJANEIROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIODEJANEIROISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.MINEIROSISS_HOM) || prefixowebservice.equals(Prefixowebservice.MINEIROSISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_HOM) || prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.REGISTROISS_HOM) || prefixowebservice.equals(Prefixowebservice.REGISTROISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SANTAREMISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTAREMISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.FRANCAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FRANCAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.FORTALEZA_HOM) || prefixowebservice.equals(Prefixowebservice.FORTALEZA_PROD) ||
							prefixowebservice.equals(Prefixowebservice.MACEIOISS_HOM) || prefixowebservice.equals(Prefixowebservice.MACEIOISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.DIADEMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.DIADEMAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.COTIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.COTIAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SAOLOURENCODASERRAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOLOURENCODASERRAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.CAMBUI_HOM) || prefixowebservice.equals(Prefixowebservice.CAMBUI_PROD) ||
							prefixowebservice.equals(Prefixowebservice.JOAO_MONLEVADE_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.JOAO_MONLEVADE_NOVO_PROD) ||
							prefixowebservice.equals(Prefixowebservice.ARARASISS_HOM) || prefixowebservice.equals(Prefixowebservice.ARARASISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.SAOJOAODABOAVISTAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOAODABOAVISTAISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.BALNEARIOCAMBORIUISS_HOM) || prefixowebservice.equals(Prefixowebservice.BALNEARIOCAMBORIUISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_HOM) || prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_HOM) || prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.CONGONHAS_HOM) || prefixowebservice.equals(Prefixowebservice.CONGONHAS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.ITAJAI_HOM) || prefixowebservice.equals(Prefixowebservice.ITAJAI_PROD) ||
							prefixowebservice.equals(Prefixowebservice.JUAZEIRO_HOM) || prefixowebservice.equals(Prefixowebservice.JUAZEIRO_PROD) ||
							prefixowebservice.equals(Prefixowebservice.MANAUS_HOM) || prefixowebservice.equals(Prefixowebservice.MANAUS_PROD)) {
						processado = arquivonfService.processaRetornoConsultaSituacaoBhiss(arquivonf, bean.getXml(), prefixowebservice);
					} else {
						throw new SinedException("A��o n�o suportada.");
					}
				} catch (Exception e) {
					processado = false;
					e.printStackTrace();
				}
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element processadoElt = new Element("Processado");
			processadoElt.setText(processado + "");
			resposta.addContent(processadoElt);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * A��o para retornar o arquivo de consulta do lote.
	 *
	 * @param request
	 * @param bean
	 * @since 14/08/2012
	 * @author Rodrigo Freitas
	 */
	public void enviarArquivonfRetornoConsulta(WebRequestContext request, EnviarArquivonfBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivonf() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivonf().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getXml() == null){
				throw new SinedException("XML n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getXml().getBytes(), "nfretornoconsulta_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			Arquivonf arquivonf = new Arquivonf();
			arquivonf.setCdarquivonf(bean.getCdarquivonf());
			arquivonf.setArquivoretornoconsulta(arquivo);
			
			arquivoDAO.saveFile(arquivonf, "arquivoretornoconsulta");
			arquivoService.saveOrUpdate(arquivo);
			arquivonfService.updateArquivoretornoconsulta(arquivonf);
			
			boolean processado = true;
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivonf(arquivonf);
			if(configuracaonfe == null){
				Element resposta = new Element("Resposta");
				
				Element status = new Element("Status");
				status.setText("1");			
				resposta.addContent(status);
				
				Element processadoElt = new Element("Processado");
				processadoElt.setText(processado + "");
				resposta.addContent(processadoElt);
				
				View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
			}
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			
			if(prefixowebservice.equals(Prefixowebservice.BHISS_HOM) || prefixowebservice.equals(Prefixowebservice.BHISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RECIFEISS_PROD) ||  
					prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOLOURENCODASERRAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOLOURENCODASERRAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CAMBUI_HOM) || prefixowebservice.equals(Prefixowebservice.CAMBUI_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JOAO_MONLEVADE_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.JOAO_MONLEVADE_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ARARASISS_HOM) || prefixowebservice.equals(Prefixowebservice.ARARASISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOAODABOAVISTAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOAODABOAVISTAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BALNEARIOCAMBORIUISS_HOM) || prefixowebservice.equals(Prefixowebservice.BALNEARIOCAMBORIUISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MARIANAISS_HOM) || prefixowebservice.equals(Prefixowebservice.MARIANAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BARBACENA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.BARBACENA_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CURITIBAISS_HOM) || prefixowebservice.equals(Prefixowebservice.CURITIBAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.PORTOALEGRE_HOM) || prefixowebservice.equals(Prefixowebservice.PORTOALEGRE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_HOM) || prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.IPATINGA_HOM) || prefixowebservice.equals(Prefixowebservice.IPATINGA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CABEDELOISS_HOM) || prefixowebservice.equals(Prefixowebservice.CABEDELOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_HOM) || prefixowebservice.equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITUMBIARA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.ITUMBIARA_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BETIMISS_HOM) || prefixowebservice.equals(Prefixowebservice.BETIMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MONLEVADEISS_HOM) || prefixowebservice.equals(Prefixowebservice.MONLEVADEISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITAUNAISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITAUNAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BARBACENAISS_HOM) || prefixowebservice.equals(Prefixowebservice.BARBACENAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FORMIGAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FORMIGAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.UBERABA_HOM) || prefixowebservice.equals(Prefixowebservice.UBERABA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SALVADORISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALVADORISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIODEJANEIROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIODEJANEIROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MACEIOISS_HOM) || prefixowebservice.equals(Prefixowebservice.MACEIOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.DIADEMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.DIADEMAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MINEIROSISS_HOM) || prefixowebservice.equals(Prefixowebservice.MINEIROSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_HOM) || prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.REGISTROISS_HOM) || prefixowebservice.equals(Prefixowebservice.REGISTROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTAREMISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTAREMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FRANCAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FRANCAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FORTALEZA_HOM) || prefixowebservice.equals(Prefixowebservice.FORTALEZA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.COTIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.COTIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_HOM) || prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_HOM) || prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CONGONHAS_HOM) || prefixowebservice.equals(Prefixowebservice.CONGONHAS_PROD) || 
					prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPA_NOVO_PROD) || 
					prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MANAUS_HOM) || prefixowebservice.equals(Prefixowebservice.MANAUS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JUAZEIRO_HOM) || prefixowebservice.equals(Prefixowebservice.JUAZEIRO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BOA_VISTA_HOM) || prefixowebservice.equals(Prefixowebservice.BOA_VISTA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.GOIANIA_HOM) || prefixowebservice.equals(Prefixowebservice.GOIANIA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITAJAI_PROD) || prefixowebservice.equals(Prefixowebservice.ITAJAI_HOM) || 
					prefixowebservice.equals(Prefixowebservice.IVOTI_HOM) || prefixowebservice.equals(Prefixowebservice.IVOTI_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_HOM) || prefixowebservice.equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.PETROLINA_HOM) || prefixowebservice.equals(Prefixowebservice.PETROLINA_PROD) || 
					prefixowebservice.equals(Prefixowebservice.ARACAJUISS_HOM) || prefixowebservice.equals(Prefixowebservice.ARACAJUISS_PROD)|| 
					prefixowebservice.equals(Prefixowebservice.ITUMBIARA_HOM) || prefixowebservice.equals(Prefixowebservice.ITUMBIARA_PROD)) {
				processado = arquivonfService.processaRetornoConsultaBhiss(arquivonf, bean.getXml(), prefixowebservice);
            } else if (prefixowebservice.equals(Prefixowebservice.CASTELO_HOM) || prefixowebservice.equals(Prefixowebservice.CASTELO_PROD)) {
                processado = arquivonfService.processaRetornoConsultaEel2(arquivonf, bean.getXml(), prefixowebservice);
            } else if(prefixowebservice.equals(Prefixowebservice.CURVELO_HOM) || prefixowebservice.equals(Prefixowebservice.CURVELO_PROD)){
				processado = arquivonfService.processaRetornoConsultaMemory(arquivonf, bean.getXml(), prefixowebservice);
			} else if(prefixowebservice.equals(Prefixowebservice.SERRAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SERRAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_HOM) || prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JANDIRA_HOM) || prefixowebservice.equals(Prefixowebservice.JANDIRA_PROD)){
				processado = arquivonfService.processaRetornoConsultaSmarapd(arquivonf, bean.getXml(), prefixowebservice);
			} else if(prefixowebservice.equals(Prefixowebservice.CAMPINASISS_PROD) || prefixowebservice.equals(Prefixowebservice.CAMPINASISS_HOM) ||
						prefixowebservice.equals(Prefixowebservice.SOROCABAISS_PROD) || prefixowebservice.equals(Prefixowebservice.SOROCABAISS_HOM)){
				processado = arquivonfService.processaRetornoConsultaCampinas(arquivonf, bean.getXml(), prefixowebservice);
			} else if(prefixowebservice.equals(Prefixowebservice.SEFAZBA_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZBA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZMG_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZMG_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZAM_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZAM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZRS_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZRS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SVAN_HOM) || prefixowebservice.equals(Prefixowebservice.SVAN_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SVRS_HOM) || prefixowebservice.equals(Prefixowebservice.SVRS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZGO_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZGO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZPR_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZPR_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZPE_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZPE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZSP_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZSP_PROD)){
				processado = arquivonfService.processaRetornoConsultaSefaz(arquivonf, bean.getXml(), prefixowebservice);
			} else if (prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_PROD)) {
				processado = arquivonfService.processaRetornoConsultaAssessorPublico(arquivonf, bean.getXml(), prefixowebservice);
			}
			
			List<String> listaUrlImpressao = new ArrayList<String>();
			List<Arquivonfnota> listan = arquivonfnotaService.findByArquivonf(arquivonf);
			
			Motivoaviso motivoAvisoErro = null;
			Motivoaviso motivoAvisoNaoEnviado = null;
			
			if (listan != null && listan.size() > 0) {
				motivoAvisoErro = motivoavisoService.findByMotivo(MotivoavisoEnum.LOTE_NF_PROCESSADO_ERRO);
				motivoAvisoNaoEnviado = motivoavisoService.findByMotivo(MotivoavisoEnum.LOTE_NF_NAO_ENVIADO);
			}
			
			arquivonfService.marcarArquivonf(arquivonf.getCdarquivonf(), "consultandolote");
			
			for (Arquivonfnota arquivonfnota : listan) {
				if (arquivonfnota.getCdarquivonfnota() != null) {
					arquivonfnotaService.marcarArquivonfnota(arquivonfnota.getCdarquivonfnota().toString(), "consultandonota");
				}
				
				if(arquivonfnota != null &&
						arquivonfnota.getNota() != null &&
						arquivonfnota.getNota().getCdNota() != null &&
						arquivonfnota.getNota().getNotaStatus() != null &&
						arquivonfnota.getNota().getNotaStatus().getCdNotaStatus() != null){
					Integer cdnota = arquivonfnota.getNota().getCdNota();
					
					String hash = Util.crypto.makeHashMd5(cdnota.toString());
					String hash2 = Util.crypto.makeHashMd5(hash);
					String hash3 = Util.crypto.makeHashMd5(hash2);
					
					if(arquivonfnota.getNota().getNotaStatus().equals(NotaStatus.NFE_EMITIDA) || arquivonfnota.getNota().getNotaStatus().equals(NotaStatus.NFE_LIQUIDADA)){
						listaUrlImpressao.add(SinedUtil.getUrlWithContext() + "/pub/relatorio/DanfePub?ACAO=gerar&cdnota=" + cdnota + "&hash=" + hash3);
					} else if(arquivonfnota.getNota().getNotaStatus().equals(NotaStatus.NFSE_EMITIDA) || arquivonfnota.getNota().getNotaStatus().equals(NotaStatus.NFSE_LIQUIDADA)){
						listaUrlImpressao.add(SinedUtil.getUrlWithContext() + "/pub/relatorio/NotaFiscalEletronicaPub?ACAO=gerar&cdnota=" + cdnota + "&hash=" + hash3);
					}
					
					Aviso aviso = null;
					
					
					try {
						if (arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_ERRO)) {
							aviso = new Aviso("Lote de nota fiscal processado com erro", "N�mero da nota fiscal: " + 
									(arquivonfnota.getNota() != null && arquivonfnota.getNota().getNumero() != null ? arquivonfnota.getNota().getNumero() : "Sem n�mero"), 
									motivoAvisoErro.getTipoaviso(), motivoAvisoErro.getPapel(), motivoAvisoErro.getAvisoorigem(), 
									arquivonfnota.getArquivonf().getCdarquivonf(), empresaService.loadPrincipal(), motivoAvisoErro);
							avisoService.salvarAvisos(aviso, Boolean.TRUE);
						} else if (arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.NAO_ENVIADO)) {
							aviso = new Aviso("Lote de nota fiscal n�o enviado", "N�mero da nota fiscal: " + 
									(arquivonfnota.getNota() != null && arquivonfnota.getNota().getNumero() != null ? arquivonfnota.getNota().getNumero() : "Sem n�mero"), 
									motivoAvisoNaoEnviado.getTipoaviso(), motivoAvisoNaoEnviado.getPapel(), motivoAvisoNaoEnviado.getAvisoorigem(), 
									arquivonfnota.getArquivonf().getCdarquivonf(), empresaService.loadPrincipal(), motivoAvisoNaoEnviado);
							avisoService.salvarAvisos(aviso, Boolean.TRUE);
						}
					} catch (Exception e) {e.printStackTrace();}
				}
			}
			
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element processadoElt = new Element("Processado");
			processadoElt.setText(processado + "");
			resposta.addContent(processadoElt);
			
			if(listaUrlImpressao != null && listaUrlImpressao.size() > 0){
				Element impressaoElt = new Element("Impressao");
				
				for (String url : listaUrlImpressao) {
					Element urlElt = new Element("URL");
					urlElt.setText(url);
					impressaoElt.addContent(urlElt);
				}
				
				resposta.addContent(impressaoElt);
			}
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarArquivoMdfeRetornoConsulta(WebRequestContext request, EnviarArquivoMdfeBean bean) {
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivomdfe() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivomdfe().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getXml() == null){
				throw new SinedException("XML n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getXml().getBytes(), "nfretornoconsulta_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			ArquivoMdfe arquivoMdfe = new ArquivoMdfe();
			arquivoMdfe.setCdArquivoMdfe(bean.getCdarquivomdfe());
			arquivoMdfe.setArquivoretornoconsulta(arquivo);
			
			arquivoDAO.saveFile(arquivoMdfe, "arquivoretornoconsulta");
			arquivoService.saveOrUpdate(arquivo);
			arquivoMdfeService.updateArquivoretornoconsulta(arquivoMdfe);
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivoMdfe(arquivoMdfe);
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			boolean processado = arquivoMdfeService.processaRetornoConsulta(arquivoMdfe, bean.getXml(), prefixowebservice);			
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element processadoElt = new Element("Processado");
			processadoElt.setText(processado + "");
			resposta.addContent(processadoElt);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagArquivonfnota(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivonfnota", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivonfnotaService.marcarArquivonfnota(bean.getWhereInArquivonf(), "cancelando");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagArquivonfnotaConsulta(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagArquivonfnotaConsulta", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			arquivonfnotaService.marcarArquivonfnota(bean.getWhereInArquivonfnota(), "consultandonota");
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagInutilizacaonfe(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagInutilizacaonfe", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			inutilizacaonfeService.marcarInutilizacao(bean.getWhereInInutilizacaonfe());
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagCartacorrecao(WebRequestContext request, MarcarFlagBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagCartacorrecao", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			notafiscalprodutocorrecaoService.marcarFlag(bean.getWhereInCartacorrecao());
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarArquivonfnotaRetornoEncerramentoMdfePorEvento(WebRequestContext request, EnviarArquivoMdfeBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivomdfe() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivomdfe().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfretornoencerramento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			ArquivoMdfe arquivoMdfe = new ArquivoMdfe();
			arquivoMdfe.setCdArquivoMdfe(bean.getCdarquivomdfe());
			arquivoMdfe = arquivoMdfeService.load(arquivoMdfe, "arquivoMdfe.cdArquivoMdfe, arquivoMdfe.mdfe");
			arquivoMdfe.setArquivoxmlretornoencerramento(arquivo);
			
			arquivoDAO.saveFile(arquivoMdfe, "arquivoxmlretornoencerramento");
			arquivoService.saveOrUpdate(arquivo);
			arquivoMdfeService.updateArquivoxmlRetornoEncerramento(arquivoMdfe);
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivoMdfe(arquivoMdfe);
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			arquivoMdfeService.processaRetornoEncerramento(arquivoMdfe, bean.getArquivo().getContent(), prefixowebservice);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoretornocancelamento = new Element("Arquivoretornoencerramento");
			arquivoretornocancelamento.setText(arquivo.getCdarquivo() + "");			
			resposta.addContent(arquivoretornocancelamento);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarArquivonfnotaRetornoCancelamentoMdfePorEvento(WebRequestContext request, EnviarArquivoMdfeBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivomdfe() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivomdfe().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfretornocancelamento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			ArquivoMdfe arquivoMdfe = new ArquivoMdfe();
			arquivoMdfe.setCdArquivoMdfe(bean.getCdarquivomdfe());
			arquivoMdfe = arquivoMdfeService.load(arquivoMdfe, "arquivoMdfe.cdArquivoMdfe, arquivoMdfe.mdfe");
			arquivoMdfe.setArquivoXmlRetornoCancelamento(arquivo);
			
			arquivoDAO.saveFile(arquivoMdfe, "arquivoXmlRetornoCancelamento");
			arquivoService.saveOrUpdate(arquivo);
			arquivoMdfeService.updateArquivoxmlRetornoCancelamento(arquivoMdfe);
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivoMdfe(arquivoMdfe);
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			arquivoMdfeService.processaRetornoCancelamento(arquivoMdfe, bean.getArquivo().getContent(), prefixowebservice);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoretornocancelamento = new Element("Arquivoretornocancelamento");
			arquivoretornocancelamento.setText(arquivo.getCdarquivo() + "");			
			resposta.addContent(arquivoretornocancelamento);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarArquivonfnotaRetornoInclusaoCondutorMdfePorEvento(WebRequestContext request, EnviarArquivoMdfeBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivomdfe() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivomdfe().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfretornoinclusaocondutor_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			ArquivoMdfe arquivoMdfe = new ArquivoMdfe();
			arquivoMdfe.setCdArquivoMdfe(bean.getCdarquivomdfe());
			arquivoMdfe = arquivoMdfeService.load(arquivoMdfe, "arquivoMdfe.cdArquivoMdfe, arquivoMdfe.mdfe");
			arquivoMdfe.setArquivoXmlRetornoInclusaoCondutor(arquivo);
			
			arquivoDAO.saveFile(arquivoMdfe, "arquivoXmlRetornoInclusaoCondutor");
			arquivoService.saveOrUpdate(arquivo);
			arquivoMdfeService.updateArquivoxmlRetornoInclusaoCondutor(arquivoMdfe);
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivoMdfe(arquivoMdfe);
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			arquivoMdfeService.processaRetornoInclusaoCondutor(arquivoMdfe, bean.getArquivo().getContent(), prefixowebservice);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoretornoinclusaocondutor = new Element("Arquivoretornoinclusaocondutor");
			arquivoretornoinclusaocondutor.setText(arquivo.getCdarquivo() + "");			
			resposta.addContent(arquivoretornoinclusaocondutor);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void enviarArquivonfnotaRetornoConsulta(WebRequestContext request, EnviarArquivonfnotaBean bean) {
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try {
			if(bean.getCdarquivonfnota() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivonfnota().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfretornoconsulta_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			Arquivonfnota arquivonfnota = new Arquivonfnota();
			arquivonfnota.setCdarquivonfnota(bean.getCdarquivonfnota());
			arquivonfnota.setArquivoxmlretornoconsulta(arquivo);
			
			arquivoDAO.saveFile(arquivonfnota, "arquivoxmlretornoconsulta");
			arquivoService.saveOrUpdate(arquivo);
			arquivonfnotaService.updateArquivoxmlretornoconsulta(arquivonfnota);
			
			
			Arquivonfnota arquivonfnotaFound = arquivonfnotaService.loadForConsulta(arquivonfnota);
			Arquivonf arquivonf = arquivonfnotaFound.getArquivonf();
			Configuracaonfe configuracaonfeFound = arquivonf != null ? arquivonf.getConfiguracaonfe() : null;
			
			if(configuracaonfeFound != null && 
					configuracaonfeFound.getTipoconfiguracaonfe() != null && 
					configuracaonfeFound.getTipoconfiguracaonfe().equals(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO)){
				String xml = new String(bean.getArquivo().getContent());
				Prefixowebservice prefixowebservice = configuracaonfeFound.getPrefixowebservice();
				if(prefixowebservice.equals(Prefixowebservice.BHISS_HOM) ||
						prefixowebservice.equals(Prefixowebservice.BHISS_PROD)){
					arquivonfService.processaRetornoConsultaBhiss(arquivonf, xml, prefixowebservice);
				} else if(prefixowebservice.equals(Prefixowebservice.CASTELO_HOM) ||
						prefixowebservice.equals(Prefixowebservice.CASTELO_PROD)){
					arquivonfService.processaRetornoConsultaEel2(arquivonf, xml, prefixowebservice);
				}
			} else {
				Element rootElement = SinedUtil.getRootElementXML(bean.getArquivo().getContent());
				List<Element> protNFeListElement = SinedUtil.getListChildElement("protNFe", rootElement.getContent());
				
				if (protNFeListElement != null && protNFeListElement.size() > 0) {
					arquivonfService.processaRetornoConsultaSefazLoteSucesso(protNFeListElement, arquivonf);
				}
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoxmlretornoconsultaElement = new Element("Arquivoxmlretornoconsulta");
			arquivoxmlretornoconsultaElement.setText(arquivo.getCdarquivo() + "");			
			resposta.addContent(arquivoxmlretornoconsultaElement);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarArquivonfnotaRetornoCancelamento(WebRequestContext request, EnviarArquivonfnotaBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivonfnota() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivonfnota().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfretornocancelamento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			Arquivonfnota arquivonfnota = new Arquivonfnota();
			arquivonfnota.setCdarquivonfnota(bean.getCdarquivonfnota());
			arquivonfnota.setArquivoretornocancelamento(arquivo);
			
			arquivoDAO.saveFile(arquivonfnota, "arquivoretornocancelamento");
			arquivoService.saveOrUpdate(arquivo);
			arquivonfnotaService.updateArquivoretornocancelamento(arquivonfnota);
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivonfnota(arquivonfnota);
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			if(prefixowebservice.equals(Prefixowebservice.BHISS_HOM) || prefixowebservice.equals(Prefixowebservice.BHISS_PROD)||
					prefixowebservice.equals(Prefixowebservice.RECIFEISS_PROD) || 
					prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.MOGIMIRIMISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOLOURENCODASERRAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOLOURENCODASERRAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CAMBUI_HOM) || prefixowebservice.equals(Prefixowebservice.CAMBUI_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JOAO_MONLEVADE_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.JOAO_MONLEVADE_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ARARASISS_HOM) || prefixowebservice.equals(Prefixowebservice.ARARASISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOAODABOAVISTAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOAODABOAVISTAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BALNEARIOCAMBORIUISS_HOM) || prefixowebservice.equals(Prefixowebservice.BALNEARIOCAMBORIUISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MARIANAISS_HOM) || prefixowebservice.equals(Prefixowebservice.MARIANAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BARBACENA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.BARBACENA_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOAQUIMBICAS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CURITIBAISS_HOM) || prefixowebservice.equals(Prefixowebservice.CURITIBAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.PORTOALEGRE_HOM) || prefixowebservice.equals(Prefixowebservice.PORTOALEGRE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_HOM) || prefixowebservice.equals(Prefixowebservice.JUIZDEFORAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SETELAGOAS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.IPATINGA_HOM) || prefixowebservice.equals(Prefixowebservice.IPATINGA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CABEDELOISS_HOM) || prefixowebservice.equals(Prefixowebservice.CABEDELOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITABIRITOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.NOVALIMAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_HOM) || prefixowebservice.equals(Prefixowebservice.SAO_MIGUEL_DO_ARAGUAIA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITUMBIARA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.ITUMBIARA_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SANTALUZIAISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CURVELO_HOM) || prefixowebservice.equals(Prefixowebservice.CURVELO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BETIMISS_HOM) || prefixowebservice.equals(Prefixowebservice.BETIMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MONLEVADEISS_HOM) || prefixowebservice.equals(Prefixowebservice.MONLEVADEISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITAUNAISS_HOM) || prefixowebservice.equals(Prefixowebservice.ITAUNAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BARBACENAISS_HOM) || prefixowebservice.equals(Prefixowebservice.BARBACENAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ARACAJUISS_HOM) || prefixowebservice.equals(Prefixowebservice.ARACAJUISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JOAO_PESSOA_HOM) || prefixowebservice.equals(Prefixowebservice.JOAO_PESSOA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITUMBIARA_HOM) || prefixowebservice.equals(Prefixowebservice.ITUMBIARA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SIMOESFILHOISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SIMOESFILHOISS_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FORMIGAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FORMIGAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.UBERABA_HOM) || prefixowebservice.equals(Prefixowebservice.UBERABA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SALVADORISS_HOM) || prefixowebservice.equals(Prefixowebservice.SALVADORISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIODEJANEIROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIODEJANEIROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOBERNARDODOCAMPOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.APARECIDAGOIANIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSERIOPRETOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MACEIOISS_HOM) || prefixowebservice.equals(Prefixowebservice.MACEIOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.DIADEMAISS_HOM) || prefixowebservice.equals(Prefixowebservice.DIADEMAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDOSCAMPOSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MINEIROSISS_HOM) || prefixowebservice.equals(Prefixowebservice.MINEIROSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_HOM) || prefixowebservice.equals(Prefixowebservice.JABOTICABALISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.REGISTROISS_HOM) || prefixowebservice.equals(Prefixowebservice.REGISTROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIOCLAROISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_HOM) || prefixowebservice.equals(Prefixowebservice.GUARULHOSISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SANTAREMISS_HOM) || prefixowebservice.equals(Prefixowebservice.SANTAREMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FRANCAISS_HOM) || prefixowebservice.equals(Prefixowebservice.FRANCAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.FORTALEZA_HOM) || prefixowebservice.equals(Prefixowebservice.FORTALEZA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.COTIAISS_HOM) || prefixowebservice.equals(Prefixowebservice.COTIAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_HOM) || prefixowebservice.equals(Prefixowebservice.RIBEIRAOPRETOISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_HOM) || prefixowebservice.equals(Prefixowebservice.EUNAPOLISISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_HOM) || prefixowebservice.equals(Prefixowebservice.CONTAGEMISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ERECHIM_HOM) || prefixowebservice.equals(Prefixowebservice.ERECHIM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.DIVINOPOLISISS_HOM) || prefixowebservice.equals(Prefixowebservice.DIVINOPOLISISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.OUROPRETO_HOM) || prefixowebservice.equals(Prefixowebservice.OUROPRETO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPAISS_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPAISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.AVARE_HOM) || prefixowebservice.equals(Prefixowebservice.AVARE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CAMACARI_HOM) || prefixowebservice.equals(Prefixowebservice.CAMACARI_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CONGONHAS_HOM) || prefixowebservice.equals(Prefixowebservice.CONGONHAS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.ITAJAI_PROD) || prefixowebservice.equals(Prefixowebservice.ITAJAI_HOM) ||
					prefixowebservice.equals(Prefixowebservice.IVOTI_PROD) || prefixowebservice.equals(Prefixowebservice.IVOTI_HOM) ||
					prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.AVARE_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.SAOJOSEDALAPA_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.JUAZEIRO_HOM) || prefixowebservice.equals(Prefixowebservice.JUAZEIRO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CHAPECO_HOM) || prefixowebservice.equals(Prefixowebservice.CHAPECO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BOA_VISTA_HOM) || prefixowebservice.equals(Prefixowebservice.BOA_VISTA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.LIMEIRA_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.LIMEIRA_NOVO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_HOM) || prefixowebservice.equals(Prefixowebservice.CACHOEIRO_DO_ITAPEMIRIM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.PETROLINA_HOM) || prefixowebservice.equals(Prefixowebservice.PETROLINA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.MANAUS_HOM) || prefixowebservice.equals(Prefixowebservice.MANAUS_PROD)) {
				arquivonfService.processaRetornoCancelamentoBhiss(arquivonfnota, bean.getArquivo().getContent(), prefixowebservice);
            } else if (prefixowebservice.equals(Prefixowebservice.CASTELO_HOM) || prefixowebservice.equals(Prefixowebservice.CASTELO_PROD)) {
                arquivonfService.processaRetornoCancelamentoEel2(arquivonfnota, bean.getArquivo().getContent(), prefixowebservice);
            } else if(prefixowebservice.equals(Prefixowebservice.SEFAZBA_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZBA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZMG_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZMG_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZAM_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZAM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZRS_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZRS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZGO_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZGO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SVAN_HOM) || prefixowebservice.equals(Prefixowebservice.SVAN_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SVRS_HOM) || prefixowebservice.equals(Prefixowebservice.SVRS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZPR_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZPR_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZPE_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZPE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZSP_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZSP_PROD)){
				arquivonfService.processaRetornoCancelamentoSefaz(arquivonfnota, bean.getArquivo().getContent(), prefixowebservice);
			} else if(prefixowebservice.equals(Prefixowebservice.SPISS_HOM) || prefixowebservice.equals(Prefixowebservice.SPISS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_HOM) || prefixowebservice.equals(Prefixowebservice.BLUMENAUISS_PROD)){
				arquivonfService.processaRetornoCancelamentoSp(arquivonfnota, bean.getArquivo().getContent(), prefixowebservice);
			} else if(prefixowebservice.equals(Prefixowebservice.SALTO_HOM) || prefixowebservice.equals(Prefixowebservice.SALTO_PROD)){
				arquivonfService.processaRetornoCancelamentoObaratec(arquivonfnota, bean.getArquivo().getContent(), prefixowebservice);
			} else if(prefixowebservice.equals(Prefixowebservice.SERRAISS_HOM) || 
							prefixowebservice.equals(Prefixowebservice.SERRAISS_PROD) || 
							prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_HOM) || 
							prefixowebservice.equals(Prefixowebservice.MOGIDASCRUZESISS_PROD) ||
							prefixowebservice.equals(Prefixowebservice.JANDIRA_HOM) ||
							prefixowebservice.equals(Prefixowebservice.JANDIRA_PROD)){
				arquivonfService.processaRetornoCancelamentoSmarpd(arquivonfnota, bean.getArquivo().getContent(), prefixowebservice);
			} else if (prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_HOM) || prefixowebservice.equals(Prefixowebservice.VOTORANTIMISS_NOVO_PROD)) {
				arquivonfService.processaRetornoCancelamentoAssessorPublico(arquivonfnota, bean.getArquivo().getContent(), prefixowebservice);
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoretornocancelamento = new Element("Arquivoretornocancelamento");
			arquivoretornocancelamento.setText(arquivo.getCdarquivo() + "");			
			resposta.addContent(arquivoretornocancelamento);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarArquivonfnotaRetornoCancelamentoPorEvento(WebRequestContext request, EnviarArquivonfnotaBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdarquivonfnota() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdarquivonfnota().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "nfretornocancelamento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			Arquivonfnota arquivonfnota = new Arquivonfnota();
			arquivonfnota.setCdarquivonfnota(bean.getCdarquivonfnota());
			arquivonfnota.setArquivoretornocancelamento(arquivo);
			
			arquivoDAO.saveFile(arquivonfnota, "arquivoretornocancelamento");
			arquivoService.saveOrUpdate(arquivo);
			arquivonfnotaService.updateArquivoretornocancelamento(arquivonfnota);
			
			Configuracaonfe configuracaonfe = configuracaonfeService.findByArquivonfnota(arquivonfnota);
			Prefixowebservice prefixowebservice = configuracaonfe.getPrefixowebservice();
			
			if(prefixowebservice.equals(Prefixowebservice.SEFAZBA_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZBA_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZRS_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZRS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZAM_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZAM_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZMG_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZMG_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZGO_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZGO_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SVAN_HOM) || prefixowebservice.equals(Prefixowebservice.SVAN_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SVRS_HOM) || prefixowebservice.equals(Prefixowebservice.SVRS_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZPE_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZPE_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZPR_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZPR_PROD) ||
					prefixowebservice.equals(Prefixowebservice.SEFAZSP_HOM) || prefixowebservice.equals(Prefixowebservice.SEFAZSP_PROD)){
				arquivonfService.processaRetornoCancelamentoPorEventoSefaz(arquivonfnota, bean.getArquivo().getContent(), prefixowebservice);
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoretornocancelamento = new Element("Arquivoretornocancelamento");
			arquivoretornocancelamento.setText(arquivo.getCdarquivo() + "");			
			resposta.addContent(arquivoretornocancelamento);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarInutilizacaonfeRetorno(WebRequestContext request, EnviarInutilizacaonfeBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdinutilizacaonfe() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdinutilizacaonfe().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "inutretorno_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			Inutilizacaonfe inutilizacaonfe = new Inutilizacaonfe();
			inutilizacaonfe.setCdinutilizacaonfe(bean.getCdinutilizacaonfe());
			inutilizacaonfe.setArquivoxmlretorno(arquivo);
			
			arquivoDAO.saveFile(inutilizacaonfe, "arquivoxmlretorno");
			arquivoService.saveOrUpdate(arquivo);
			inutilizacaonfeService.updateArquivoxmlretorno(inutilizacaonfe);
			
			inutilizacaonfeService.processaRetornoInutilizacaoSefaz(inutilizacaonfe, bean.getArquivo().getContent());
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element arquivoretornocancelamento = new Element("Arquivoxmlretorno");
			arquivoretornocancelamento.setText(arquivo.getCdarquivo() + "");			
			resposta.addContent(arquivoretornocancelamento);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarCartacorrecaoRetorno(WebRequestContext request, EnviarCartacorrecaoBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdnotafiscalprodutocorrecao() == null || !CriptografiaEmissorUtil.verificaCriptografia(bean.getCdnotafiscalprodutocorrecao().toString(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Arquivo n�o enviado.");
			}
			
			Arquivo arquivo = new Arquivo(bean.getArquivo().getContent(), "cartacorrecaoretorno_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			Notafiscalprodutocorrecao notafiscalprodutocorrecao = new Notafiscalprodutocorrecao();
			notafiscalprodutocorrecao.setCdnotafiscalprodutocorrecao(bean.getCdnotafiscalprodutocorrecao());
			notafiscalprodutocorrecao.setArquivoretorno(arquivo);
			
			arquivoDAO.saveFile(notafiscalprodutocorrecao, "arquivoretorno");
			arquivoService.saveOrUpdate(arquivo);
			notafiscalprodutocorrecaoService.updateArquivoretorno(notafiscalprodutocorrecao);
			
			notafiscalprodutocorrecaoService.processaRetornoCartacorrecaoSefaz(notafiscalprodutocorrecao, bean.getArquivo().getContent());
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarCert(WebRequestContext request, EnviarArquivonfBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("enviarCert", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Log do emissor n�o enviado.");
			}
			
			String dir = arquivoDAO.getCaminhoCertEmissor();
			
			java.io.File file = new java.io.File(dir + SinedUtil.getUrlWithoutContext() + "_" + bean.getArquivo().getNome() + "_" + bean.getXml());
			if(!file.exists()){
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			OutputStream out = null;
			try {
				out = new FileOutputStream(file);
				out.write(bean.getArquivo().getContent());
				out.flush();
			}
			finally {
				try {
					out.close();
				} catch (Exception e) {}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void enviarLog(WebRequestContext request, EnviarArquivonfBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("enviarLog", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getArquivo() == null){
				throw new SinedException("Log do emissor n�o enviado.");
			}
			
			String dir = arquivoDAO.getCaminhoLogEmissor();
			
			java.io.File file = new java.io.File(dir + bean.getArquivo().getNome());
			if(!file.exists()){
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			OutputStream out = null;
			try {
				out = new FileOutputStream(file);
				out.write(bean.getArquivo().getContent());
				out.flush();
			}
			finally {
				try {
					out.close();
				} catch (Exception e) {}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * A��o que busca todas as informa��es para o emissor.
	 *
	 * @param request
	 * @param bean
	 * @since 12/09/2012
	 * @author Rodrigo Freitas
	 */
	public void consultaDados(WebRequestContext request, ConsultaDadosBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("consultaDados", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Empresa empresa = new Empresa(bean.getCdempresa());
			
			List<Arquivonf> listaArquivonf = arquivonfService.findArquivonfNovosForEmissao(empresa);
			List<Arquivonf> listaArquivonfConsulta = arquivonfService.findArquivonfNovosForConsulta(empresa);
			List<Arquivonfnota> listaArquivonfnotaConsulta = arquivonfnotaService.findArquivonfnotaConsulta(empresa);
			List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findNovosCancelamentos(empresa);
			List<Inutilizacaonfe> listaInutilizacaonfe = inutilizacaonfeService.findInutilizacoesNovas(empresa);
			List<Notafiscalprodutocorrecao> listaCorrecao = notafiscalprodutocorrecaoService.findNovasCorrecoes(empresa);
			List<Arquivonfnota> listaArquivonfnotaPorEvento = arquivonfnotaService.findNovosCancelamentosPorEvento(empresa);
			List<ArquivoMdfe> listaArquivoMdfe = arquivoMdfeService.findArquivoMdfeNovosForEmissao(empresa);
			List<ArquivoMdfe> listaArquivoMdfeConsulta = arquivoMdfeService.findArquivoMdfeNovosForConsulta(empresa);
			List<ArquivoMdfe> listaArquivoEncerramentoMdfe = arquivoMdfeService.findNovosEncerramentos(empresa);
			List<ArquivoMdfe> listaArquivoCancelamentoMdfe = arquivoMdfeService.findNovosCancelamentos(empresa);
			List<ArquivoMdfe> listaArquivoInclusaoCondutorMdfe = arquivoMdfeService.findNovosInclusaoCondutor(empresa);
			List<LoteConsultaDfe> listaLoteConsultaDfe = loteConsultaDfeService.procurarLoteConsultaDfeParaConsulta(empresa);
			List<ManifestoDfeEvento> listaManifestoDfeEvento = manifestoDfeEventoService.procurarManifestoDfeEventoParaConsulta(empresa);
			
			for (Arquivonf arquivonf : listaArquivonf) {
				Element arquivonfElement = new Element("Arquivonf");
				
				Element cdarquivonfElement = new Element("Cdarquivonf");
				Element cdarquivoxmlElement = new Element("Cdarquivoxml");
				Element cdconfiguracaonfeElement = new Element("Cdconfiguracaonfe");

				cdarquivonfElement.setText(arquivonf.getCdarquivonf() + "");
				cdarquivoxmlElement.setText(arquivonf.getArquivoxml().getCdarquivo() + "");
				cdconfiguracaonfeElement.setText(arquivonf.getConfiguracaonfe().getCdconfiguracaonfe() + "");
				
				arquivonfElement.addContent(cdarquivonfElement);
				arquivonfElement.addContent(cdarquivoxmlElement);
				arquivonfElement.addContent(cdconfiguracaonfeElement);

				if(arquivonf.getTipocontigencia() != null){
					Element tipocontigenciaElement = new Element("Tipocontigencia");
					tipocontigenciaElement.setText(arquivonf.getTipocontigencia().name());
					arquivonfElement.addContent(tipocontigenciaElement);
				}
				
				resposta.addContent(arquivonfElement);
			}
			
			for (Arquivonf arquivonf : listaArquivonfConsulta) {
				Element arquivonfElement = new Element("ArquivonfConsulta");
				
				Element cdArquivonfElement = new Element("Cdarquivonf");
				Element cdArquivoXmlConsultaLoteElement = new Element("Cdarquivoxmlconsultalote");
				Element cdConfiguracaoNfeElement = new Element("Cdconfiguracaonfe");
				
				cdArquivonfElement.setText(arquivonf.getCdarquivonf() + "");
				cdArquivoXmlConsultaLoteElement.setText(arquivonf.getArquivoxmlconsultalote().getCdarquivo() + "");
				cdConfiguracaoNfeElement.setText(arquivonf.getConfiguracaonfe().getCdconfiguracaonfe() + "");
				
				arquivonfElement.addContent(cdArquivonfElement);
				arquivonfElement.addContent(cdArquivoXmlConsultaLoteElement);
				arquivonfElement.addContent(cdConfiguracaoNfeElement);
				
				resposta.addContent(arquivonfElement);
			}
			
			for (Inutilizacaonfe inutilizacaonfe : listaInutilizacaonfe) {
				Element inutilizacaonfeElement = new Element("Inutilizacaonfe");
				
				Element cdinutilizacaonfeElement = new Element("Cdinutilizacaonfe");
				Element arquivoxmlenvioElement = new Element("Cdarquivoxmlenvio");
				Element configuracaonfeElement = new Element("Cdconfiguracaonfe");
				
				cdinutilizacaonfeElement.setText(inutilizacaonfe.getCdinutilizacaonfe() + "");
				arquivoxmlenvioElement.setText(inutilizacaonfe.getArquivoxmlenvio().getCdarquivo() + "");
				configuracaonfeElement.setText(inutilizacaonfe.getConfiguracaonfe().getCdconfiguracaonfe() + "");
				
				inutilizacaonfeElement.addContent(cdinutilizacaonfeElement);
				inutilizacaonfeElement.addContent(arquivoxmlenvioElement);
				inutilizacaonfeElement.addContent(configuracaonfeElement);
				
				resposta.addContent(inutilizacaonfeElement);
			}
			
			for (Arquivonfnota arquivonfnota : listaArquivonfnotaConsulta) {
				Element arquivonfnotaConsultaElement = new Element("ArquivonfnotaConsulta");
				
				Element cdarquivonfnotaElement = new Element("Cdarquivonfnota");
				Element arquivoxmlconsultaElement = new Element("Cdarquivoxmlconsulta");
				Element configuracaonfeElement = new Element("Cdconfiguracaonfe");
				Element cdarquivonfElement = new Element("Cdarquivonf");
				
				cdarquivonfnotaElement.setText(arquivonfnota.getCdarquivonfnota() + "");
				configuracaonfeElement.setText(arquivonfnota.getArquivonf().getConfiguracaonfe().getCdconfiguracaonfe() + "");
				arquivoxmlconsultaElement.setText(arquivonfnota.getArquivoxmlconsulta().getCdarquivo() + "");
				cdarquivonfElement.setText(arquivonfnota.getArquivonf().getCdarquivonf() + "");
				
				if(arquivonfnota.getArquivonf().getTipocontigencia() != null){
					Element tipocontigenciaElement = new Element("Tipocontigencia");
					tipocontigenciaElement.setText(arquivonfnota.getArquivonf().getTipocontigencia().name());
					arquivonfnotaConsultaElement.addContent(tipocontigenciaElement);
				}
				
				arquivonfnotaConsultaElement.addContent(cdarquivonfnotaElement);
				arquivonfnotaConsultaElement.addContent(arquivoxmlconsultaElement);
				arquivonfnotaConsultaElement.addContent(configuracaonfeElement);
				arquivonfnotaConsultaElement.addContent(cdarquivonfElement);
				
				resposta.addContent(arquivonfnotaConsultaElement);
			}
			
			for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
				Element arquivonfnotaElement = new Element("Arquivonfnota");
				
				Element cdarquivonfnotaElement = new Element("Cdarquivonfnota");
				Element arquivoxmlcancelamentoElement = new Element("Cdarquivoxmlcancelamento");
				Element configuracaonfeElement = new Element("Cdconfiguracaonfe");
				Element cdarquivonfElement = new Element("Cdarquivonf");
				
				cdarquivonfnotaElement.setText(arquivonfnota.getCdarquivonfnota() + "");
				configuracaonfeElement.setText(arquivonfnota.getArquivonf().getConfiguracaonfe().getCdconfiguracaonfe() + "");
				arquivoxmlcancelamentoElement.setText(arquivonfnota.getArquivoxmlcancelamento().getCdarquivo() + "");
				cdarquivonfElement.setText(arquivonfnota.getArquivonf().getCdarquivonf() + "");
				
				arquivonfnotaElement.addContent(cdarquivonfnotaElement);
				arquivonfnotaElement.addContent(arquivoxmlcancelamentoElement);
				arquivonfnotaElement.addContent(configuracaonfeElement);
				arquivonfnotaElement.addContent(cdarquivonfElement);
				
				resposta.addContent(arquivonfnotaElement);
			}
			
			for (Arquivonfnota arquivonfnota : listaArquivonfnotaPorEvento) {
				Element arquivonfnotaElement = new Element("Arquivonfnotanovo");
				
				Element cdarquivonfnotaElement = new Element("Cdarquivonfnota");
				Element arquivoxmlcancelamentoElement = new Element("Cdarquivoxmlcancelamento");
				Element configuracaonfeElement = new Element("Cdconfiguracaonfe");
				Element cdarquivonfElement = new Element("Cdarquivonf");
				
				cdarquivonfnotaElement.setText(arquivonfnota.getCdarquivonfnota() + "");
				configuracaonfeElement.setText(arquivonfnota.getArquivonf().getConfiguracaonfe().getCdconfiguracaonfe() + "");
				arquivoxmlcancelamentoElement.setText(arquivonfnota.getArquivoxmlcancelamento().getCdarquivo() + "");
				cdarquivonfElement.setText(arquivonfnota.getArquivonf().getCdarquivonf() + "");
				
				arquivonfnotaElement.addContent(cdarquivonfnotaElement);
				arquivonfnotaElement.addContent(arquivoxmlcancelamentoElement);
				arquivonfnotaElement.addContent(configuracaonfeElement);
				arquivonfnotaElement.addContent(cdarquivonfElement);
				
				if(arquivonfnota.getTipocontigenciacancelamento() != null){
					Element tipocontigenciaElement = new Element("Tipocontigencia");
					tipocontigenciaElement.setText(arquivonfnota.getTipocontigenciacancelamento().name());
					arquivonfnotaElement.addContent(tipocontigenciaElement);
				}
				
				resposta.addContent(arquivonfnotaElement);
			}
			
			for (Notafiscalprodutocorrecao nfcorrecao : listaCorrecao) {
				Element cartacorrecaoElement = new Element("Cartacorrecao");
				
				Element cdnotafiscalprodutocorrecaoElement = new Element("Cdnotafiscalprodutocorrecao");
				Element cdarquivoenvioElement = new Element("Cdarquivoenvio");
				Element cdconfiguracaonfeElement = new Element("Cdconfiguracaonfe");
				
				Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProduto(nfcorrecao.getNotafiscalproduto());
				
				cdnotafiscalprodutocorrecaoElement.setText(nfcorrecao.getCdnotafiscalprodutocorrecao() + "");
				cdarquivoenvioElement.setText(nfcorrecao.getArquivoenvio().getCdarquivo() + "");
				cdconfiguracaonfeElement.setText(arquivonfnota.getArquivonf().getConfiguracaonfe().getCdconfiguracaonfe() + "");
				
				cartacorrecaoElement.addContent(cdnotafiscalprodutocorrecaoElement);
				cartacorrecaoElement.addContent(cdarquivoenvioElement);
				cartacorrecaoElement.addContent(cdconfiguracaonfeElement);
				
				resposta.addContent(cartacorrecaoElement);
			}
			
			for (ArquivoMdfe arquivoMdfe : listaArquivoMdfe) {
				Element arquivoMdfeElement = new Element("Arquivomdfe");
				
				Element cdArquivoMdfeElement = new Element("Cdarquivomdfe");
				Element cdArquivoXmlElement = new Element("Cdarquivoxml");
				Element cdConfiguracaoNfeElement = new Element("Cdconfiguracaonfe");
				
				cdArquivoMdfeElement.setText(arquivoMdfe.getCdArquivoMdfe() + "");
				cdArquivoXmlElement.setText(arquivoMdfe.getArquivoxml().getCdarquivo() + "");
				cdConfiguracaoNfeElement.setText(arquivoMdfe.getConfiguracaonfe().getCdconfiguracaonfe() + "");
				
				arquivoMdfeElement.addContent(cdArquivoMdfeElement);
				arquivoMdfeElement.addContent(cdArquivoXmlElement);
				arquivoMdfeElement.addContent(cdConfiguracaoNfeElement);
				
				resposta.addContent(arquivoMdfeElement);
			}
			
			for (ArquivoMdfe arquivoMdfe : listaArquivoMdfeConsulta) {
				Element arquivoMdfeElement = new Element("ArquivoMdfeConsulta");
				
				Element cdArquivoMdfeElement = new Element("Cdarquivomdfe");
				Element cdArquivoXmlConsultaLoteElement = new Element("Cdarquivoxmlconsultalote");
				Element cdConfiguracaoNfeElement = new Element("Cdconfiguracaonfe");
				
				cdArquivoMdfeElement.setText(arquivoMdfe.getCdArquivoMdfe() + "");
				cdArquivoXmlConsultaLoteElement.setText(arquivoMdfe.getArquivoxmlconsultalote().getCdarquivo() + "");
				cdConfiguracaoNfeElement.setText(arquivoMdfe.getConfiguracaonfe().getCdconfiguracaonfe() + "");
				
				arquivoMdfeElement.addContent(cdArquivoMdfeElement);
				arquivoMdfeElement.addContent(cdArquivoXmlConsultaLoteElement);
				arquivoMdfeElement.addContent(cdConfiguracaoNfeElement);
				
				resposta.addContent(arquivoMdfeElement);
			}
			
			for (ArquivoMdfe arquivoMdfe : listaArquivoEncerramentoMdfe) {
				Element arquivoMdfeElement = new Element("ArquivoEncerramentoMdfe");
				
				Element cdArquivoMdfe = new Element("Cdarquivomdfe");
				Element arquivoxmlEncerramentoElement = new Element("Cdarquivoxmlencerramento");
				Element configuracaonfeElement = new Element("Cdconfiguracaonfe");
				
				cdArquivoMdfe.setText(arquivoMdfe.getCdArquivoMdfe() + "");
				configuracaonfeElement.setText(arquivoMdfe.getConfiguracaonfe().getCdconfiguracaonfe() + "");
				arquivoxmlEncerramentoElement.setText(arquivoMdfe.getArquivoxmlencerramento().getCdarquivo() + "");
				
				arquivoMdfeElement.addContent(cdArquivoMdfe);
				arquivoMdfeElement.addContent(arquivoxmlEncerramentoElement);
				arquivoMdfeElement.addContent(configuracaonfeElement);
				
				resposta.addContent(arquivoMdfeElement);
			}
			
			for (ArquivoMdfe arquivoMdfe : listaArquivoCancelamentoMdfe) {
				Element arquivoMdfeElement = new Element("ArquivoCancelamentoMdfe");
				
				Element cdArquivoMdfe = new Element("Cdarquivomdfe");
				Element arquivoxmlCancelamentoElement = new Element("Cdarquivoxmlcancelamento");
				Element configuracaonfeElement = new Element("Cdconfiguracaonfe");
				
				cdArquivoMdfe.setText(arquivoMdfe.getCdArquivoMdfe() + "");
				configuracaonfeElement.setText(arquivoMdfe.getConfiguracaonfe().getCdconfiguracaonfe() + "");
				arquivoxmlCancelamentoElement.setText(arquivoMdfe.getArquivoXmlCancelamento().getCdarquivo() + "");
				
				arquivoMdfeElement.addContent(cdArquivoMdfe);
				arquivoMdfeElement.addContent(arquivoxmlCancelamentoElement);
				arquivoMdfeElement.addContent(configuracaonfeElement);
				
				resposta.addContent(arquivoMdfeElement);
			}
			
			for (ArquivoMdfe arquivoMdfe : listaArquivoInclusaoCondutorMdfe) {
				Element arquivoMdfeElement = new Element("ArquivoInclusaoCondutorMdfe");
				
				Element cdArquivoMdfe = new Element("Cdarquivomdfe");
				Element arquivoxmlInclusaoCondutorElement = new Element("Cdarquivoxmlinclusaocondutor");
				Element configuracaonfeElement = new Element("Cdconfiguracaonfe");
				
				cdArquivoMdfe.setText(arquivoMdfe.getCdArquivoMdfe() + "");
				configuracaonfeElement.setText(arquivoMdfe.getConfiguracaonfe().getCdconfiguracaonfe() + "");
				arquivoxmlInclusaoCondutorElement.setText(arquivoMdfe.getArquivoXmlInclusaoCondutor().getCdarquivo() + "");
				
				arquivoMdfeElement.addContent(cdArquivoMdfe);
				arquivoMdfeElement.addContent(arquivoxmlInclusaoCondutorElement);
				arquivoMdfeElement.addContent(configuracaonfeElement);
				
				resposta.addContent(arquivoMdfeElement);
			}
			
			for (LoteConsultaDfe loteConsultaDfe : listaLoteConsultaDfe) {
				if(loteConsultaDfe.getArquivoXml() != null && loteConsultaDfe.getConfiguracaoNfe() != null){
	                Element loteConsultaDfeElement = new Element("LoteConsultaDfe");
	                
	                Element cdLoteConsultaDfeElement = new Element("cdLoteConsultaDfe");
	                Element cdArquivoXmlElement = new Element("cdArquivoXml");
	                Element cdConfiguracaoNfeElement = new Element("cdConfiguracaoNfe");
	
	                cdLoteConsultaDfeElement.setText(loteConsultaDfe.getCdLoteConsultaDfe() + "");
	                cdArquivoXmlElement.setText(loteConsultaDfe.getArquivoXml().getCdarquivo() + "");
	                cdConfiguracaoNfeElement.setText(loteConsultaDfe.getConfiguracaoNfe().getCdconfiguracaonfe() + "");
	                
	                loteConsultaDfeElement.addContent(cdLoteConsultaDfeElement);
	                loteConsultaDfeElement.addContent(cdArquivoXmlElement);
	                loteConsultaDfeElement.addContent(cdConfiguracaoNfeElement);
	                
	                resposta.addContent(loteConsultaDfeElement);
				}
            }
			
			for (ManifestoDfeEvento manifestoDfeEvento : listaManifestoDfeEvento) {
                Element manifestoDfeEventoElement = new Element("ManifestoDfeEvento");
                
                Element cdManifestoDfeEventoElement = new Element("cdManifestoDfeEvento");
                Element cdArquivoXmlElement = new Element("cdArquivoXml");
                Element cdConfiguracaoNfeElement = new Element("cdConfiguracaoNfe");

                cdManifestoDfeEventoElement.setText(manifestoDfeEvento.getCdManifestoDfeEvento() + "");
                cdArquivoXmlElement.setText(manifestoDfeEvento.getArquivoXml().getCdarquivo() + "");
                cdConfiguracaoNfeElement.setText(manifestoDfeEvento.getConfiguracaoNfe().getCdconfiguracaonfe() + "");
                
                manifestoDfeEventoElement.addContent(cdManifestoDfeEventoElement);
                manifestoDfeEventoElement.addContent(cdArquivoXmlElement);
                manifestoDfeEventoElement.addContent(cdConfiguracaoNfeElement);
                
                resposta.addContent(manifestoDfeEventoElement);
            }
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagLoteConsultaDfe(WebRequestContext request, MarcarFlagBean bean) {
 		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if (!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagLoteConsultaDfe", bean.getHash())) {
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			loteConsultaDfeService.marcarLoteConsultaDfe(bean.getWhereInLoteConsultaDfe(), "enviado");
			
			String[] ids = bean.getWhereInLoteConsultaDfe().split(",");
			
			for (String id : ids) {
				LoteConsultaDfe loteConsultaDfe = loteConsultaDfeService.procurarPorId(Integer.parseInt(id));
				
				LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
				Usuario usuario = SinedUtil.getUsuarioLogado();
				loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				loteConsultaDfeHistorico.setLoteConsultaDfe(loteConsultaDfe);
				loteConsultaDfeHistorico.setObservacao("Distribui��o de DF-e enviada.");
				loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
				
				loteConsultaDfeHistoricoService.saveOrUpdate(loteConsultaDfeHistorico);
			}
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarLoteConsultaDfe(WebRequestContext request, LoteConsultaDfe loteConsultaDfeEmissor) {
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try {
			if (loteConsultaDfeEmissor.getHash() == null || !CriptografiaEmissorUtil.verificaCriptografia("enviarLoteConsultaDfe", loteConsultaDfeEmissor.getHash())) {
				throw new SinedException("Erro de autentica��o.");
			}
			
			if (loteConsultaDfeEmissor.getCdLoteConsultaDfe() == null) {
				throw new SinedException("ID n�o enviado.");
			}
			
			if (loteConsultaDfeEmissor.getArquivoRetornoXml() == null) {
				throw new SinedException("Arquivo de retorno de consulta n�o enviado.");
			}
			
			Arquivo arquivoRetornoXml = new Arquivo(loteConsultaDfeEmissor.getArquivoRetornoXml().getContent(), "retornoLoteConsultaDfe_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			loteConsultaDfeService.processaRetornoLoteConsultaDfe(arquivoRetornoXml, loteConsultaDfeEmissor);
			
			LoteConsultaDfe loteConsultaDfe = loteConsultaDfeService.procurarPorId(loteConsultaDfeEmissor.getCdLoteConsultaDfe());
			
			LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
			Usuario usuario = SinedUtil.getUsuarioLogado();
			loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
			loteConsultaDfeHistorico.setLoteConsultaDfe(loteConsultaDfe);
			loteConsultaDfeHistorico.setObservacao("Distribui��o de DF-e processada.");
			loteConsultaDfeHistorico.setSituacaoEnum(loteConsultaDfe.getSituacaoEnum());
			
			loteConsultaDfeHistoricoService.saveOrUpdate(loteConsultaDfeHistorico);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			resposta.addContent(status);
			
			Element cdArquivoRetornoXmlElement = new Element("cdArquivoRetornoXml");
			cdArquivoRetornoXmlElement.setText(arquivoRetornoXml.getCdarquivo() + "");
			resposta.addContent(cdArquivoRetornoXmlElement);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void enviarManifestoDfeEvento(WebRequestContext request, ManifestoDfeEvento manifestoDfeEventoEmissor) {
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try {
			if (manifestoDfeEventoEmissor.getHash() == null || !CriptografiaEmissorUtil.verificaCriptografia("enviarManifestoDfeEvento", manifestoDfeEventoEmissor.getHash())) {
				throw new SinedException("Erro de autentica��o.");
			}
			
			if (manifestoDfeEventoEmissor.getCdManifestoDfeEvento() == null) {
				throw new SinedException("ID n�o enviado.");
			}
			
			if (manifestoDfeEventoEmissor.getArquivoRetornoXml() == null) {
				throw new SinedException("Arquivo de retorno de consulta n�o enviado.");
			}
			
			Arquivo arquivoRetornoXml = new Arquivo(manifestoDfeEventoEmissor.getArquivoRetornoXml().getContent(), "retornoManifestoDfeEvento_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			
			manifestoDfeEventoService.processaRetornoManifestoDfeEvento(arquivoRetornoXml, manifestoDfeEventoEmissor);
			ManifestoDfeEvento manifestoDfeEvento = manifestoDfeEventoService.procurarPorId(manifestoDfeEventoEmissor.getCdManifestoDfeEvento());
			
			if (manifestoDfeEvento != null) {
				ManifestoDfeHistorico manifestoDfeHistorico = new ManifestoDfeHistorico();
				Usuario usuario = SinedUtil.getUsuarioLogado();
	            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
	            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				manifestoDfeHistorico.setManifestoDfe(manifestoDfeEvento.getManifestoDfe());
				manifestoDfeHistorico.setSituacaoEnum(manifestoDfeEvento.getSituacaoEnum());
				manifestoDfeHistorico.setTipoEventoNfe(manifestoDfeEvento.getTpEvento());
				manifestoDfeHistorico.setObservacao("Manifesto DF-e processado.");
				
				manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistorico);
			}
			
			if (SituacaoEmissorEnum.PROCESSADO_SUCESSO.equals(manifestoDfeEvento.getSituacaoEnum())) {
				ManifestoDfe manifestoDfe = manifestoDfeService.loadForEntrada(manifestoDfeEvento.getManifestoDfe());
				
				if (TipoEventoNfe.CIENCIA_OPERACAO.equals(manifestoDfeEvento.getTpEvento())) {
					manifestoDfe.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.CIENCIA);
					
					if(manifestoDfe.getArquivoNfeCompletoXml() == null && manifestoDfeEvento.getManifestoDfe() != null
							&& manifestoDfeEvento.getManifestoDfe().getLoteConsultaDfeItem() != null 
							&& manifestoDfeEvento.getManifestoDfe().getLoteConsultaDfeItem().getLoteConsultaDfe() != null
							&& manifestoDfeEvento.getManifestoDfe().getLoteConsultaDfeItem().getLoteConsultaDfe().getEmpresa() != null
							&& manifestoDfeEvento.getManifestoDfe().getLoteConsultaDfeItem().getLoteConsultaDfe().getConfiguracaoNfe() != null){
						try {
							manifestoDfeService.criarLoteConsultaDfe(manifestoDfeEvento.getManifestoDfe().getLoteConsultaDfeItem().getLoteConsultaDfe().getEmpresa(), 
																	 manifestoDfeEvento.getManifestoDfe().getLoteConsultaDfeItem().getLoteConsultaDfe().getConfiguracaoNfe(), 
																	 manifestoDfe.getCdManifestoDfe().toString(),
																	 true);
						} catch (Exception e) {
							request.addMessage("Erro ao criar arquivo(s) de lote de consulta de DF-e automaticamente. " + e.getMessage());
						}
					}
				} else if (TipoEventoNfe.CONFIRMACAO_OPERACAO.equals(manifestoDfeEvento.getTpEvento())) {
					manifestoDfe.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.CONFIRMACAO);
				} else if (TipoEventoNfe.DESCONHECIMENTO_OPERACAO.equals(manifestoDfeEvento.getTpEvento())) {
					manifestoDfe.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.DESCONHECIMENTO);
				} else if (TipoEventoNfe.OPERACAO_NAO_REALIZADA.equals(manifestoDfeEvento.getTpEvento())) {
					manifestoDfe.setManifestoDfeSituacaoEnum(ManifestoDfeSituacaoEnum.NAO_REALIZADA);
				}
				
				manifestoDfeService.atualizarManifestoDfeSituacaoEnum(manifestoDfe);
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			resposta.addContent(status);
			
			Element cdArquivoRetornoXmlElement = new Element("cdArquivoRetornoXml");
			cdArquivoRetornoXmlElement.setText(arquivoRetornoXml.getCdarquivo() + "");
			resposta.addContent(cdArquivoRetornoXmlElement);
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void marcarFlagManifestoDfeEvento(WebRequestContext request, MarcarFlagBean bean) {
 		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if (!CriptografiaEmissorUtil.verificaCriptografia("marcarFlagManifestoDfeEvento", bean.getHash())) {
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			manifestoDfeEventoService.marcarManifestoDfeEventoService(bean.getWhereInManifestoDfeEvento(), "enviado");
			
			List<ManifestoDfeEvento> listaManifestoDfeEvento = manifestoDfeEventoService.procurarPorIds(bean.getWhereInManifestoDfeEvento());
			
			for (ManifestoDfeEvento m : listaManifestoDfeEvento) {
				ManifestoDfeHistorico manifestoDfeHistorico = new ManifestoDfeHistorico();
				Usuario usuario = SinedUtil.getUsuarioLogado();
	            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
	            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
				manifestoDfeHistorico.setManifestoDfe(m.getManifestoDfe());
				manifestoDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
				manifestoDfeHistorico.setObservacao("Manifestos DF-e enviado para a sefaz.");
				
				manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistorico);
			}
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
}
