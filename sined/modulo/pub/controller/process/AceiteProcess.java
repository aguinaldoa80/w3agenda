package br.com.linkcom.sined.modulo.pub.controller.process;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Documentodigital;
import br.com.linkcom.sined.geral.bean.Documentodigitaldestinatario;
import br.com.linkcom.sined.geral.bean.Documentodigitalmodelo;
import br.com.linkcom.sined.geral.bean.Documentodigitalusuario;
import br.com.linkcom.sined.geral.service.DocumentodigitalService;
import br.com.linkcom.sined.geral.service.DocumentodigitaldestinatarioService;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/pub/process/Aceite")
public class AceiteProcess extends MultiActionController {
	
	private DocumentodigitaldestinatarioService documentodigitaldestinatarioService;
	private DocumentodigitalService documentodigitalService;
	
	public void setDocumentodigitalService(
			DocumentodigitalService documentodigitalService) {
		this.documentodigitalService = documentodigitalService;
	}
	public void setDocumentodigitaldestinatarioService(
			DocumentodigitaldestinatarioService documentodigitaldestinatarioService) {
		this.documentodigitaldestinatarioService = documentodigitaldestinatarioService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, Documentodigitaldestinatario documentodigitaldestinatario) throws Exception {
		if(StringUtils.isBlank(documentodigitaldestinatario.getToken())){
			throw new SinedException("Token n�o informado.");
		}
		
		documentodigitaldestinatario = documentodigitaldestinatarioService.loadByToken(documentodigitaldestinatario.getToken());
		if(documentodigitaldestinatario == null){
			throw new SinedException("Documento n�o encontrado.");
		}
		
		Documentodigital documentodigital = documentodigitaldestinatario.getDocumentodigital();
		Documentodigitalmodelo documentodigitalmodelo = documentodigital.getDocumentodigitalmodelo();
		Arquivo arquivo = documentodigital.getArquivo();
		String contentType =  arquivo.getContenttype().split("/")[1];
		
		StringBuilder url = new StringBuilder()
			.append(SinedUtil.getUrlWithContext())
			.append("/ACEITE/")
			.append(documentodigitaldestinatario.getToken())
			.append(".")
			.append(contentType);
		
		DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());
		request.setAttribute("texto", documentodigitalmodelo != null && documentodigitalmodelo.getTextotelaaceite() != null ? documentodigitalmodelo.getTextotelaaceite().replaceAll("\r?\n", "<BR>") : "");
		request.setAttribute("hash", documentodigital.getHash());
		request.setAttribute("dtaceite", documentodigitaldestinatario.getDtaceite());
		request.setAttribute("arquivo", arquivo);
		request.setAttribute("fileURL", url.toString());
		request.setAttribute("token", documentodigitaldestinatario.getToken());
		
		return new ModelAndView("direct:process/aceite");
	}
	
	public ModelAndView aceiteDados(WebRequestContext request, Documentodigitaldestinatario documentodigitaldestinatario){
		if(StringUtils.isBlank(documentodigitaldestinatario.getToken())){
			throw new SinedException("Token n�o informado.");
		}
		
		documentodigitaldestinatario = documentodigitaldestinatarioService.loadByToken(documentodigitaldestinatario.getToken());
		if(documentodigitaldestinatario == null){
			throw new SinedException("Documento n�o encontrado.");
		}
		
		return new ModelAndView("direct:process/aceiteDados", "documentodigitaldestinatario", documentodigitaldestinatario);
	}
	
	public ModelAndView aceite(WebRequestContext request, Documentodigitaldestinatario documentodigitaldestinatario){
		if(StringUtils.isBlank(documentodigitaldestinatario.getToken())){
			throw new SinedException("Token n�o informado.");
		}
		
		Documentodigitalusuario documentodigitalusuario = documentodigitaldestinatario.getDocumentodigitalusuario();
		if(documentodigitalusuario == null || 
				StringUtils.isBlank(documentodigitalusuario.getNome()) ||
				documentodigitalusuario.getCpf() == null ||
				documentodigitalusuario.getDtnascimento() == null ||
				StringUtils.isBlank(documentodigitalusuario.getEmail())){
			throw new SinedException("Dados de usu�rio incompletos.");
		}
		
		Documentodigitaldestinatario documentodigitaldestinatario_aux = documentodigitaldestinatarioService.loadByToken(documentodigitaldestinatario.getToken());
		if(documentodigitaldestinatario_aux == null){
			throw new SinedException("Documento n�o encontrado.");
		}
		
		Documentodigital documentodigital = documentodigitaldestinatario_aux.getDocumentodigital();
		documentodigitaldestinatario.setCddocumentodigitaldestinatario(documentodigitaldestinatario_aux.getCddocumentodigitaldestinatario());
		
		documentodigitaldestinatarioService.doAceite(documentodigitaldestinatario);
		documentodigitalService.updateSituacaoAceiteByDestinatarios(documentodigital);
		
		try {
			documentodigitalService.enviarEmailObservadores(documentodigital, documentodigitaldestinatario);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ModelAndView("direct:process/aceiteFinalizacao");
	}
	
}
