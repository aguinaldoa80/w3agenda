package br.com.linkcom.sined.modulo.pub.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.UsuarioService;

@Controller(path = "/pub/Enviasenha")
public class EnviaSenha extends MultiActionController {
	
	private UsuarioService usuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		return new ModelAndView("enviasenha"); 
	}
	
	/**
	 * M�todo utilizado para validar o email informado pelo usu�rio.
	 * 
	 * @see br.com.linkcom.sined.geral.service.UsuarioService#carregarUsuarioByEmail(String)
	 * @see #enviaEmailNovaSenha(WebRequestContext, Usuario)
	 * @param request
	 * @return ModelAndView
	 * @author Jo�o Paulo Zica
	 * @author Flavio Tavares
	 */
	public ModelAndView valida(WebRequestContext request) {
		String campo = request.getParameter("loginemail");
		String errormessage = "";
		if(campo == null || "".equals(campo)) {
			errormessage = "Usu�rio n�o cadastrado.";
		}
		else {
			Usuario usuario = usuarioService.carregarUsuarioByEmail(campo);
			if(usuario == null) {
				usuario = usuarioService.carregarUsuarioByLogin(campo);
			}if(usuario == null) {
				errormessage = "Usu�rio n�o cadastrado.";
				request.setAttribute("errormessage", errormessage);
			} else if (usuario.getBloqueado()) {
				errormessage = "Este usu�rio est� bloqueado, favor consultar o administrador do sistema.";
				request.setAttribute("errormessage", errormessage);
			} else {
				enviaEmailNovaSenha(request,usuario);
			}
		}
		return index(request);
	}
	
	/**
	 * M�todo para gerar nova senha para o usu�rio e enviar por email.
	 * 
	 * Gera uma nova senha e envia para o usu�rio. Caso n�o haja nenhum problema no envio do email, 
	 * a nova senha � criptografada e salva no banco.
	 *
	 * @see br.com.linkcom.sined.geral.service.UsuarioService#enviaEmailRecuperarSenha(Usuario, String)
	 * @see br.com.linkcom.sined.geral.service.UsuarioService#alterarSenhaUsuario(Usuario)
	 * @param request
	 * @param usuario
	 * @author Fl�vio Tavares
	 */
	private void enviaEmailNovaSenha(WebRequestContext request, Usuario usuario){
		
		try {
			usuarioService.enviaEmailRecuperarSenha(usuario);
			request.setAttribute("message", "Senha enviada para o email "+usuario.getEmail()+".");
			usuarioService.alterarSenhaUsuario(usuario,false);
		} catch (Exception e) {
			request.setAttribute("errormessage", "Senha n�o enviada!");
			e.printStackTrace();
		}
	}
}
