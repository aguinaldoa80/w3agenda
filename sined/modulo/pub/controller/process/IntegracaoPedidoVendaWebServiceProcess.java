package br.com.linkcom.sined.modulo.pub.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.auxiliar.PedidoVendaParametersBean;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.ProducaoagendasituacaoEnum;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.IndicecorrecaoService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialFaixaMarkupService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialsimilarService;
import br.com.linkcom.sined.geral.service.MaterialunidademedidaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.ResponsavelFreteService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FornecedoragenciaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericSearchBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaParametroBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaWSBean;
import br.com.linkcom.sined.util.EnumUtils;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.suprimento.MaterialCategoriaTreeFiltro;

@Bean
@Controller(path="/pub/process/IntegracaoWebServicePedidoVenda")
public class IntegracaoPedidoVendaWebServiceProcess extends IntegracaoVendaWebServiceAbstractProcess{
	
	public ModelAndView findAutocompleteClienteForEntrada(WebRequestContext request, GenericSearchBean bean) {
		String paramSearch = bean.getValue();
		String paramEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaCliente", ObjectUtils.translateEntityListToGenericBeanList(ClienteService.getInstance().findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuarioWSVenda(paramSearch, paramEmpresa)));
	}
	
	public ModelAndView findAutocompleteClienteForListagem(WebRequestContext request, GenericSearchBean bean) {
		String paramSearch = bean.getValue();
		String paramEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaCliente", ObjectUtils.translateEntityListToGenericBeanList(ClienteService.getInstance().findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuarioWSVenda(paramSearch, paramEmpresa)));
	}
	
	public ModelAndView findListasByCliente(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaEndereco", ObjectUtils.translateEntityListToGenericBeanList(EnderecoService.getInstance().findForVenda(bean.getCliente())))
					.addObject("listaContato", ContatoService.getInstance().findForWSVenda(bean.getCliente()))
					.addObject("listaDocumentoTipo", DocumentotipoService.getInstance().findDocumentotipoUsuarioForVenda(bean.getCliente(), bean.getWhereIn()));
	}
	
	public ModelAndView findListasByEmpresa(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaFornecedorTransportadora", FornecedorService.getInstance().findForWSVenda(bean.getEmpresa()))
					.addObject("listaContaBoleto", ContaService.getInstance().findContasAtivasComPermissaoWSVenda(bean.getEmpresa()));
	}
	
	public ModelAndView findLocalarmazenagemAtivoEmpresaAutocomplete(WebRequestContext request, GenericSearchBean bean){
		String cdEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaLocalArmazenagem",  ObjectUtils.translateEntityListToGenericBeanList(LocalarmazenagemService.getInstance().findLocalarmazenagemAtivoEmpresaAutocompleteWSVenda(bean.getValue(), cdEmpresa)));
	}
	
	public ModelAndView findMaterialAutocompleteForEntrada(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterial", MaterialService.getInstance().findMaterialAutocompleteVendaForPesquisaWSVenda(bean.getValue(), bean.getEmpresa()));
	}
	
	public ModelAndView findMaterialAutocompleteForListagem(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterial", MaterialService.getInstance().findMaterialAutocompleteWSVenda(bean.getValue(), bean.getEmpresa()));
	}
	
	public ModelAndView findDadosByMaterial(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaUnidadeMedida", MaterialunidademedidaService.getInstance().findByMaterialForWSVenda(bean.getMaterial()))
				.addObject("listaMaterialFaixaMarkup", MaterialFaixaMarkupService.getInstance().findFaixaMarkupWSVenda(bean.getMaterial(), bean.getUnidadeMedida(), bean.getEmpresa()));
	}
	
	public ModelAndView findListasPagamento(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaIndiceCorrecao", IndicecorrecaoService.getInstance().findForWSVenda());
	}
	
	//LISTAGEM
	public ModelAndView findMaterialGrupo(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterialGrupo", ObjectUtils.translateEntityListToGenericBeanList(MaterialgrupoService.getInstance().findAll("materialgrupo.nome")));
	}
	
	public ModelAndView findForCombosListagem(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterialGrupo", MaterialgrupoService.getInstance().findAllForComboWS())
				.addObject("listaProjetos", ObjectUtils.translateEntityListToGenericBeanList(ProjetoService.getInstance().findForCombo()))
				.addObject("listaClienteCategoria", CategoriaService.getInstance().findAllForComboWS())
				.addObject("listaRegiao", RegiaoService.getInstance().findAllForComboWS())
				.addObject("listaEmpresa", EmpresaService.getInstance().findAllForComboWS())
				.addObject("listaEmpresa", PrazopagamentoService.getInstance().findAllForComboWS())
				.addObject("listaPedidoVendaTipo", PedidovendatipoService.getInstance().findAllForComboWS())
				.addObject("listaDocumentoTipo", ObjectUtils.translateEntityListToGenericBeanList(DocumentotipoService.getInstance().getListaDocumentoTipoUsuarioForVenda(null)))
				.addObject("listaClienteCategoria", ObjectUtils.translateEntityListToGenericBeanList(CategoriaService.getInstance().findByTipo(true, false)));
	}
	
	/*public ModelAndView findAutocompleteUsuario(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaUsuario", ObjectUtils.translateEntityListToGenericBeanList(UsuarioService.getInstance().findForAutocomplete(bean.getValue(), "usuario.nome", "usuario.nome", true)))
					
					
					
					.addObject("listaMaterial", MaterialService.getInstance().findAllForComboWS());
	}*/
	
	public ModelAndView findAutocompleteUsuario(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaUsuario", ObjectUtils.translateEntityListToGenericBeanList(UsuarioService.getInstance().findForAutocomplete(bean.getValue(), "usuario.nome", "usuario.nome", true)))
					/*.addObject("listaDocumentoTipo", DocumentotipoService.getInstance().findDocumentotipoUsuarioForWSVenda(bean.getCliente(), null))*/;
	}
	
	public ModelAndView findAutocompleteMaterialCategoria(WebRequestContext request, GenericSearchBean bean) {
		MaterialCategoriaTreeFiltro filtro = new MaterialCategoriaTreeFiltro();
		filtro.setQ(bean.getValue());
		filtro.setAtivo(true);
		List<Materialcategoria> listaMaterialcategoria = MaterialcategoriaService.getInstance().findAutocompleteTreeView(filtro);
		return new JsonModelAndView()
					.addObject("listaMaterialCategoria", ObjectUtils.translateEntityListToGenericBeanList(listaMaterialcategoria));
	}
	
	
	public ModelAndView verificarPedidoVendaTipoAJAX(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().verificarPedidovendatipoAJAXJson(request, bean.getPedidoVendaTipo(), bean.getEmpresa(), bean.getFromPedidoVenda());
	}
	
	public ModelAndView ajaxDocumentoAntecipacao(WebRequestContext request, GenericSearchBean bean){
		List<Documento> listaDocumento = new ArrayList<Documento>();
		Boolean isWms = EmpresaService.getInstance().isIntegracaoWms(EmpresaService.getInstance().loadPrincipal());		
		if (Boolean.TRUE.equals(isWms) || ParametrogeralService.getInstance().getBoolean(Parametrogeral.CONSIDERAR_PEDIDO_ANTECIPACAO))
			listaDocumento = DocumentoService.getInstance().findForAntecipacaoConsideraPedidoVenda(bean.getCliente(), bean.getDocumentoTipo(), null, Documentoclasse.OBJ_RECEBER/* bean.getPedidovenda()*/);
		else
			listaDocumento = DocumentoService.getInstance().findForAntecipacaoConsideraVenda(bean.getCliente(), bean.getDocumentoTipo());
		return new JsonModelAndView()
					.addObject("listaDocumento", ObjectUtils.translateEntityListToGenericBeanList(listaDocumento, "numeroValor"));
	}
	
	public ModelAndView ajaxBuscaSaldoValeCompraByCliente(WebRequestContext request, GenericSearchBean bean){
		return ValecompraService.getInstance().actionSaldovalecompraVendaPedidoOrcamento(bean.getPedidoVenda(), bean.getCliente());
	}
	
	public ModelAndView ajaxCamposAdicionaisPedido(WebRequestContext request, GenericSearchBean bean){
		if(bean.getPedidoVenda() == null){
			bean.setPedidoVenda(new Pedidovenda());
		}
		bean.getPedidoVenda().setPedidovendatipo(bean.getPedidoVendaTipo());
		return PedidovendaService.getInstance().ajaxCamposAdicionais(request, bean.getPedidoVenda());
	}
	
	public ModelAndView exibirConfirmGerarValeCompra(WebRequestContext request, GenericSearchBean bean) {
		return PedidovendaService.getInstance().exibirConfirmGerarValeCompra(request, bean.getPedidoVendaTipo());
	}
	
	public ModelAndView changeFornecedorAgencia(WebRequestContext request, FornecedoragenciaBean fornecedoragenciaBean) {
		return VendaService.getInstance().changeFornecedoragencia(request, fornecedoragenciaBean);
	}
	
	public ModelAndView findForAutocompletePedidovendacomodatoWSVenda(WebRequestContext request, GenericSearchBean bean){
		Integer cdPedidoVenda = bean.getPedidoVenda() != null? bean.getPedidoVenda().getCdpedidovenda(): null;
		List<PedidoVendaWSBean> lista = bean.getValue() != null && StringUtils.isNotBlank(bean.getValue().toString())? PedidovendaService.getInstance().findForAutocompletePedidovendacomodatoWSVenda(bean.getValue().toString(), cdPedidoVenda): new ArrayList<PedidoVendaWSBean>();
		
		return new JsonModelAndView().addObject("listaPedidoVenda", lista);
	}
	
	public ModelAndView ajaxBuscaFaixaMarkup(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("listaFaixaMarkup", MaterialFaixaMarkupService.getInstance().findFaixaMarkupWSVenda(bean.getMaterial(), bean.getUnidadeMedida(), bean.getEmpresa()));
	}
	
	public ModelAndView ajaxHabilitaCamposDeACordoComGrupoMaterial(GenericSearchBean bean){
		return MaterialService.getInstance().ajaxHabilitaCamposDeACordoComGrupoMaterial(bean.getMaterial());
	}
	
	public ModelAndView verificarFornecedorProdutos(WebRequestContext request, GenericSearchBean bean){
		return PedidovendaService.getInstance().verificarFornecedorProdutos(request, bean.getEmpresa(), bean.getWhereIn());
	}
	
	public ModelAndView buscarSugestoesVendaAJAXPedido(WebRequestContext request, GenericSearchBean bean){
		Venda venda = new Venda();
		venda.setCodigo(bean.getWhereIn());
		venda.setEmpresa(venda.getEmpresa());
		venda.setCliente(venda.getCliente());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setPrazopagamento(bean.getPrazoPagamento());
		venda.setMaterialtabelapreco(bean.getMaterialTabelaPreco());
		return PedidovendaService.getInstance().buscarSugestoesVendaAJAX(request, venda, bean.getWhereIn());
	}
	
	public ModelAndView ajaxBuscaInfoPrazoPagamento(WebRequestContext request, GenericSearchBean bean){
		return PedidovendaService.getInstance().ajaxBuscaInfoPrazopagamento(bean.getPrazoPagamento());
	}
	
	public ModelAndView pagamento(WebRequestContext request, GenericSearchBean bean){
		if(bean.getPedidoVenda() == null){
			bean.setPedidoVenda(new Pedidovenda());
		}
		String dtPrazoEntrega = "";
		if(bean.getPedidoVenda().getDtprazoentrega() != null){
			SinedDateUtils.toString(bean.getPedidoVenda().getDtprazoentrega());
		}
		bean.getPedidoVenda().setPrazopagamento(bean.getPrazoPagamento());
		return PedidovendaService.getInstance().pagamento(request, bean.getPedidoVenda(), dtPrazoEntrega);
	}
	
	public ModelAndView findForTrocaMaterialSimilar(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("bean", MaterialsimilarService.getInstance().findForWSVenda(bean.getMaterial()));
	}
	
	public ModelAndView salvar(WebRequestContext request, Pedidovenda pedidoVenda){
		
		BindException errors = new BindException(pedidoVenda, "pedido");
		if(PedidovendaService.getInstance().validate(pedidoVenda, errors)){
			try {
				PedidovendaService.getInstance().salvarCrud(request, pedidoVenda);
				return new JsonModelAndView().addObject("success", true)
						.addObject("id", Util.strings.toStringIdStyled(pedidoVenda));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			String msgErro = "";
			if(SinedUtil.isListNotEmpty(errors.getAllErrors())){
				for(Object obj: errors.getAllErrors()){
					msgErro += ((ObjectError)obj).getDefaultMessage()+"/n";
				}
				return new JsonModelAndView().addObject("success", false)
											.addObject("erro", msgErro);
			}
		}
		return null;
	}
	
	public ModelAndView comprar(WebRequestContext request, Pedidovenda pedidoVenda){
		String msgErro = "";
		Usuario usuario = SinedUtil.getUsuarioLogado();
		usuario.getCdpessoa();
		BindException exc = new BindException(pedidoVenda, "Pedido de venda");
		PedidovendaService.getInstance().validate(pedidoVenda, exc);
		
		if(SinedUtil.isListNotEmpty(exc.getAllErrors())){
			for(Object obj: exc.getAllErrors()){
				msgErro += ((ObjectError)obj).getDefaultMessage()+"/n";
			}
			request.clearMessages();
			return new JsonModelAndView().addObject("success", false)
										.addObject("erro", msgErro);
		}
		if(!PedidovendaService.getInstance().validateComprar(request, pedidoVenda)){
			if(request.getMessages() != null){
				List<String> listaErros = SinedUtil.getMessageError(request.getMessages());
				request.clearMessages();
				return new JsonModelAndView().addObject("success", false)
											.addObject("erro", listaErros);
			}
			request.clearMessages();
			return null;
		}
		
		ModelAndView retorno = PedidovendaService.getInstance().comprar(request, pedidoVenda, pedidoVenda.getParameters());
		if(retorno != null && retorno.getModel() != null && retorno.getModel().containsKey("success")){
			return new JsonModelAndView().addObject("success", true)
					.addObject("id", Util.strings.toStringIdStyled(pedidoVenda));
		}
		return new JsonModelAndView().addObject("success", false);
	}
	
	public ModelAndView loadForEntrada(WebRequestContext request, GenericSearchBean bean){
		ModelAndView retorno = new JsonModelAndView(); 
		if(!Util.objects.isPersistent(bean.getPedidoVenda())){
			bean.setPedidoVenda(new Pedidovenda());
		}
		bean.getPedidoVenda().setFromWebService(true);
		if(bean.getParameters() == null){
			bean.setParameters(new PedidoVendaParametersBean());
		}
		bean.getParameters().setFromWebService(true);
		
		Pedidovenda pedidoVenda = PedidovendaService.getInstance().loadBeanForEntrada(request, bean.getPedidoVenda(), bean.getParameters());
		
		retorno = new JsonModelAndView().addObject("bean", PedidovendaService.getInstance().loadForEntradaWS(pedidoVenda))
										.addObject("listaEmpresa", EmpresaService.getInstance().findForWSVenda())
										.addObject("listaCentroCusto", ObjectUtils.translateEntityListToGenericBeanList(CentrocustoService.getInstance().findAtivos()))
										.addObject("listaProjetos", ObjectUtils.translateEntityListToGenericBeanList(ProjetoService.getInstance().findForCombo()))
										.addObject("listaDocumentoTipo", ObjectUtils.translateEntityListToGenericBeanList(DocumentotipoService.getInstance().findDocumentotipoUsuarioForVenda(bean.getCliente(), bean.getWhereIn())))
										.addObject("listaPrazoPagamento", PrazopagamentoService.getInstance().findByPrazomedioWSVenda(false))
										.addObject("listaPedidoVendaSituacao", EnumUtils.translateEnumToBean(Pedidovendasituacao.class))
										.addObject("listaProducaoAgendaSituacao", EnumUtils.translateEnumToBean(ProducaoagendasituacaoEnum.class))
										.addObject("listaPresencaCompradorNfe", EnumUtils.translateEnumToBean(Presencacompradornfe.class))
										.addObject("listaResponsavelFrete", ResponsavelFreteService.getInstance().findForWSVenda())
										.addObject("listaIndiceCorrecao", IndicecorrecaoService.getInstance().findForWSVenda())
										.addObject("listaContaBoleto", ContaService.getInstance().findContasAtivasComPermissaoWSVenda(pedidoVenda.getEmpresa()))
										.addObject("listaPedidovendatipo", ObjectUtils.translateEntityListToGenericBeanList(PedidovendatipoService.getInstance().findAll()));
		if(Util.objects.isPersistent(pedidoVenda) && Util.objects.isPersistent(pedidoVenda.getCliente())){
			retorno = retorno.addObject("listaEnderecoByCliente", ObjectUtils.translateEntityListToGenericBeanList(EnderecoService.getInstance().findForVenda(pedidoVenda.getCliente())));
		}
		if(request.getAttribute("parametros") != null){
			retorno = retorno.addObject("parametros", (PedidoVendaParametroBean)request.getAttribute("parametros"));
		}
		return retorno;
	}
	
	public ModelAndView validateEditar(WebRequestContext request, GenericSearchBean bean){
		Pedidovenda pedidoVenda = bean.getPedidoVenda();
		List<String> listaErros = null;
		if(Util.objects.isPersistent(pedidoVenda)){
			pedidoVenda.setEmpresa(bean.getEmpresa());
			PedidovendaService.getInstance().validateEditar(request, pedidoVenda, bean.getParameters());
			listaErros = SinedUtil.getMessageError(request.getMessages());
		}
		request.clearMessages();
		return new JsonModelAndView()
					.addObject("erros", listaErros);
	}
	
	public ModelAndView cancelar(WebRequestContext request, GenericSearchBean bean){
		Pedidovenda pedidoVenda = bean.getPedidoVenda();
		if(pedidoVenda == null){
			pedidoVenda = new Pedidovenda();
		}
		pedidoVenda.setIds(bean.getWhereIn());
		ModelAndView retorno = null;
		if(PedidovendaService.getInstance().validateAbrirJustificativaCancelamentoPedidovenda(request, bean.getWhereIn())
				&& PedidovendaService.getInstance().validateCancelamento(request, pedidoVenda)){
			PedidovendaService.getInstance().cancelar(request, pedidoVenda);
			List<String> listaErros = SinedUtil.getMessageError(request.getMessages());
			request.clearMessages();
			retorno = new JsonModelAndView().addObject("errors", listaErros)
											.addObject("success", true);
		}else{
			List<String> listaErros = SinedUtil.getMessageError(request.getMessages());
			request.clearMessages();
			retorno = new JsonModelAndView().addObject("errors", listaErros)
											.addObject("success", false);
		}
		
		return retorno;
	}
	
	public ModelAndView validateCancelar(WebRequestContext request, GenericSearchBean bean){
		Pedidovenda pedidoVenda = bean.getPedidoVenda();
		if(pedidoVenda == null){
			pedidoVenda = new Pedidovenda();
		}
		pedidoVenda.setIds(bean.getWhereIn());
		ModelAndView retorno = null;
		if(PedidovendaService.getInstance().validateAbrirJustificativaCancelamentoPedidovenda(request, bean.getWhereIn())
				&& PedidovendaService.getInstance().validateCancelamento(request, pedidoVenda)){
			retorno = new JsonModelAndView()
							.addObject("podeCancelar", true);
		}else{
			List<String> listaErros = SinedUtil.getMessageError(request.getMessages());
			request.clearMessages();
			retorno = new JsonModelAndView().addObject("errors", listaErros)
											.addObject("podeCancelar", false);
		}
		
		return retorno;
	}
	
	public ModelAndView validateAprovar(WebRequestContext request, GenericSearchBean bean){
		if(!Util.objects.isPersistent(bean.getPedidoVenda())){
			return new JsonModelAndView()
						.addObject("podeAprovar", false)
						.addObject("erro", "Nenhum pedido de venda foi informado.");
		}
		if(!PedidovendaService.getInstance().validateAprovar(request, bean.getPedidoVenda().getCdpedidovenda().toString())){
			List<String> listaErros = SinedUtil.getMessageError(request.getMessages());
			request.clearMessages();
			return new JsonModelAndView()
						.addObject("errors", listaErros)
						.addObject("podeAprovar", false);
		}
		 
		return new JsonModelAndView()
				.addObject("podeAprovar", true);
	}
	
	public ModelAndView ajaxValidaPedidoGarantia(WebRequestContext request, Pedidovenda pedidovenda){
		return PedidovendaService.getInstance().ajaxValidaPedidoGarantia(request, pedidovenda);
	}
	
	public ModelAndView buscarInfMaterialProducaoAJAX(WebRequestContext request, GenericSearchBean bean){
		Pedidovenda venda = bean.getPedidoVenda();
		if(venda == null){
			venda = new Pedidovenda();
		}
		venda.setCliente(bean.getCliente());
		venda.setEmpresa(bean.getEmpresa());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setPrazopagamento(bean.getPrazoPagamento());
		if(Util.objects.isPersistent(bean.getMaterial())){
			venda.setCodigo(bean.getMaterial().getCdmaterial().toString());
		}
		return VendaService.getInstance().buscarInfMaterialProducaoAJAX(request, venda);
	}
	
	public ModelAndView recalcularMaterialProducaoAJAX(WebRequestContext request, GenericSearchBean bean){
		Pedidovenda venda = bean.getPedidoVenda();
		if(venda == null){
			venda = new Pedidovenda();
		}
		venda.setCliente(bean.getCliente());
		venda.setEmpresa(bean.getEmpresa());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setPrazopagamento(bean.getPrazoPagamento());
		if(Util.objects.isPersistent(bean.getMaterial())){
			venda.setCodigo(bean.getMaterial().getCdmaterial().toString());
		}
		return VendaService.getInstance().recalcularMaterialProducaoAJAX(request, venda);
	}
	
	public ModelAndView ajaxVerificarPedidoVendaOrigem(WebRequestContext request, GenericSearchBean bean){
		boolean pedidovendaorigeminvalido = false;
		Pedidovenda pedidovenda = new Pedidovenda();
		pedidovenda.setPedidovendaorigem(bean.getPedidoVenda());
		if(Util.objects.isPersistent(pedidovenda.getPedidovendaorigem()) && !PedidovendaService.getInstance().existeItemComGarantia(pedidovenda.getPedidovendaorigem().getCdpedidovenda())){
			pedidovendaorigeminvalido = true;
		}
		return new JsonModelAndView().addObject("pedidovendaorigeminvalido", pedidovendaorigeminvalido);
	}
	
	public ModelAndView copiar(WebRequestContext request, GenericSearchBean bean){
		if(Util.objects.isPersistent(bean.getPedidoVenda())){
			Pedidovenda pedidoVenda = PedidovendaService.getInstance().criarCopia(bean.getPedidoVenda());
			return new JsonModelAndView()
						.addObject("pedidoVenda", PedidovendaService.getInstance().loadForEntradaWS(pedidoVenda));
		}
		
		return null;
	}
}