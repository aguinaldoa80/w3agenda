package br.com.linkcom.sined.modulo.pub.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivotef;
import br.com.linkcom.sined.geral.service.ArquivotefService;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.IntegracaoTefCallbackBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.thread.IntegracaoTEFCallbackThread;

@Bean
@Controller(path="/pub/process/IntegracaoTEF")
public class IntegracaoTEFPublicoProcess extends MultiActionController {
	
	private ArquivotefService arquivotefService;

	public void setArquivotefService(ArquivotefService arquivotefService) {
		this.arquivotefService = arquivotefService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, IntegracaoTefCallbackBean bean){
		try {
			Integer cdarquivotef = Integer.parseInt(bean.getIntencaoVendaReferencia());
			Arquivotef arquivotef = arquivotefService.load(new Arquivotef(cdarquivotef));
			if(arquivotef == null){
				throw new SinedException("N�o encontrado o pagamento " + bean.getIntencaoVendaReferencia() + " com id " + bean.getIntencaoVendaId());
			}
			
			arquivotefService.createAndSaveHistorico(arquivotef, "Callback recebido", null);
			IntegracaoTEFCallbackThread thread = new IntegracaoTEFCallbackThread(bean, SinedUtil.getSubdominioCliente() + "_w3erp_PostgreSQLDS");
			thread.start();
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonModelAndView().addObject("ok", false);
		}
		return new JsonModelAndView().addObject("ok", true);
	}
	
}
