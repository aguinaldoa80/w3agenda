package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import br.com.linkcom.neo.authorization.AuthorizationDAO;
import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.RetornoWMSEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.dao.UsuarioDAO;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContacrmService;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EntradafiscalService;
import br.com.linkcom.sined.geral.service.EntregadocumentosincronizacaoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialdevolucaoService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.NotaDocumentoService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.BoletoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.AtualizaFaturamentoSLBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CancelamentoFaturamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConfirmarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConfirmarPedidovendaItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultaContasAtrasadasClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultaPagamentoClienteServicoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarContacrmBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarEstoqueBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarPagamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarPagamentoContratoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ExisteClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.FaturarBaixarBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericRespostaInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.InserirCentrocustoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.InserirClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.InserirFaturamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.InserirFaturamentoSLBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.LoginBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PagamentoClienteServicoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.SegundaViaPagamentoBean;
import br.com.linkcom.sined.util.CriptografiaUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;


@Bean
@Controller(path="/pub/process/EnvioInformacao")
public class EnvioInformacaoProcess extends MultiActionController{
	public static final String PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE = "TOKEN_VALIDACAO_WEBSERVICE";

	private ClienteService clienteService;
	private ParametrogeralService parametrogeralService;
	private ContareceberService contareceberService;
	private DocumentoService documentoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotaDocumentoService notaDocumentoService;
	private DocumentohistoricoService documentohistoricoService;
	private ContapagarService contapagarService;
	private EmpresaService empresaService;
	private ContacrmService contacrmService;
	private ContratoService contratoService;
	private CentrocustoService centrocustoService;
	private ContaService contaService;
	private PedidovendaService pedidovendaService;
	private EntradafiscalService entradafiscalService;
	private EntregadocumentosincronizacaoService entregadocumentosincronizacaoService;
	private MaterialdevolucaoService materialdevolucaoService;
	private MaterialService materialService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setContacrmService(ContacrmService contacrmService) {this.contacrmService = contacrmService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setMaterialdevolucaoService(MaterialdevolucaoService materialdevolucaoService) {this.materialdevolucaoService = materialdevolucaoService;}
	public void setEntregadocumentosincronizacaoService(EntregadocumentosincronizacaoService entregadocumentosincronizacaoService) {this.entregadocumentosincronizacaoService = entregadocumentosincronizacaoService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	
	/**
	 * A��o para enviar informa��es de outro sistema para o W3erp.
	 * Esta a��o insere um novo cliente.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void inserirCliente(WebRequestContext request, InserirClienteBean bean) {
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getNome() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getNome().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			
			if(bean.getCnpj() != null && !bean.getCnpj().equals("")){
				clienteService.criaClienteJuridico(bean);
			} else if(bean.getCpf() != null && !bean.getCpf().equals("")){
				clienteService.criaClienteFisica(bean);
			} else {
				throw new SinedException("Preencher o CPF ou CNPJ do cliente.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			
			resposta.addContent(status);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * A��o para enviar informa��es de outro sistema para o W3erp.
	 * Esta a��o insere um novo cliente, retornando um XML com o cdcliente inserido.
	 *
	 * @param request
	 * @param bean
	 * @author Igor Silv�rio Costa
	 */
	public void inserirClienteSL(WebRequestContext request, InserirClienteBean bean) {
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getNome() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getNome().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Integer cdpessoa = null;
			if(bean.getCnpj() != null && !bean.getCnpj().equals("")){
				cdpessoa = clienteService.criaClienteJuridico(bean);
			} else if(bean.getCpf() != null && !bean.getCpf().equals("")){
				cdpessoa = clienteService.criaClienteFisica(bean);
			} else {
				throw new SinedException("Preencher o CPF ou CNPJ do cliente.");
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			resposta.addContent(status);
			

			Element cliente = new Element("Cliente");
			cliente.setText(cdpessoa.toString());
			resposta.addContent(cliente);
			
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	public void cancelarFaturamentoSL(WebRequestContext request, CancelamentoFaturamentoBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCddocumento() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCddocumento().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Documentohistorico documentohistorico = new Documentohistorico();
			documentohistorico.setIds(bean.getCddocumento().toString());
			documentohistorico.setObservacao("Cancelamento via webservice.");
			documentohistorico.setDocumentoacao(Documentoacao.CANCELADA);
			documentohistorico.setFromcontareceber(false);
			
			documentoService.doCancelar(documentohistorico);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			resposta.addContent(status);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * A��o para enviar informa��es de outro sistema para o W3erp.
	 * Esta a��o insere um faturamento.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void inserirFaturamento(WebRequestContext request, InserirFaturamentoBean bean) {
		request.getServletResponse().setContentType("application/xml");
		
//		System.out.println("## FATURAMENTO WEB-SERVICE NORMAL ##");
		
		try{
			Cliente cliente = null;
			if(bean.getCnpjcliente() != null) {
				if(bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCnpjcliente().getBytes(), bean.getHash())){
					throw new SinedException("Erro de autentica��o.");
				}
				
				Cnpj cnpj = new Cnpj(bean.getCnpjcliente());
				cliente = clienteService.findByCnpj(cnpj);
				if(cliente == null){
					throw new SinedException("Cliente n�o encontrado.");
				}
			} else if(bean.getCpfcliente() != null){
				if(bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCpfcliente().getBytes(), bean.getHash())){
					throw new SinedException("Erro de autentica��o.");
				}
				
				Cpf cpf = new Cpf(bean.getCpfcliente());
				cliente = clienteService.findByCpf(cpf);
				if(cliente == null){
					throw new SinedException("Cliente n�o encontrado.");
				}
			} else {
				throw new SinedException("Erro de autentica��o.");
			}
			
			Integer numDiasVencimento = parametrogeralService.getInteger(Parametrogeral.PARAMETRO_DIAS_VENCIMENTO);
			Date dtemissao = new Date(System.currentTimeMillis());
			Date dtvencimento = SinedDateUtils.addDiasData(dtemissao, numDiasVencimento);
			Empresa empresa = empresaService.loadWithEndereco(new Empresa(bean.getCdempresa()));
			
			Documento documento = new Documento();
			documento.setDocumentoacao(Documentoacao.PREVISTA);
			documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
			documento.setDocumentotipo(new Documentotipo(bean.getCddocumentotipo()));
			documento.setDescricao(bean.getDescricao());
			documento.setPessoa(cliente);
			documento.setTipopagamento(Tipopagamento.CLIENTE);
			documento.setDtemissao(dtemissao);
			documento.setDtvencimento(dtvencimento);
			documento.setValor(bean.getValor());
			documento.setEmpresa(empresa);
			
			if (bean.getCdconta() != null){
				Conta conta = contaService.loadConta(new Conta(bean.getCdconta()));
				documento.setConta(conta);
				//Preenche a carteira do documento com a carteira padr�o
				documento.setContacarteira(conta.getContacarteira());
			}
			
			Rateioitem rateioitem = new Rateioitem();
			if(bean.getCdprojeto() != null){
				rateioitem.setProjeto(new Projeto(bean.getCdprojeto()));
			}
			rateioitem.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
			rateioitem.setContagerencial(new Contagerencial(bean.getCdcontagerencial()));
			rateioitem.setPercentual(100d);
			rateioitem.setValor(bean.getValor());
			
			List<Rateioitem> listaItens = new ArrayList<Rateioitem>();
			listaItens.add(rateioitem);
			
			Rateio rateio = new Rateio();
			rateio.setListaRateioitem(listaItens);
			
			documento.setRateio(rateio);
			
			Documentohistorico documentohistorico = new Documentohistorico(documento);
			Set<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
			listaDocumentohistorico.add(documentohistorico);
			
			documento.setListaDocumentohistorico(listaDocumentohistorico);
			
			List<Endereco> listaEnderecos = new ArrayList<Endereco>();
			listaEnderecos = contareceberService.carregaEnderecosAPartirTipoPagamento(documento);
			Endereco endereco = contareceberService.enderecoSugerido(listaEnderecos);
			documento.setEndereco(endereco);
			
			documentoService.saveOrUpdate(documento);
			
			Endereco endEmpresa = empresa.getEndereco();
			
			NotaFiscalServico nota = new NotaFiscalServico();
			nota.setCliente(cliente);
			
//			nota.setCodigocnae(bean.getCodigocnae());
//			nota.setItemlistaservico(bean.getItemlistaservico());
			
			nota.setDtVencimento(dtvencimento);
			nota.setEmpresa(empresa);
			if(endEmpresa != null) nota.setMunicipioissqn(endEmpresa.getMunicipio());
			nota.setEnderecoCliente(endereco);
			
			nota.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);
			nota.setNotaStatus(NotaStatus.EM_ESPERA);
			nota.setNaturezaoperacao(Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
			
			NotaFiscalServicoItem item = new NotaFiscalServicoItem();
			item.setDescricao(bean.getDescricao());
			item.setQtde(1d);
			item.setPrecoUnitario(bean.getValor().getValue().doubleValue());
			
			List<NotaFiscalServicoItem> listaIt = new ArrayList<NotaFiscalServicoItem>();
			listaIt.add(item);
			nota.setListaItens(listaIt);
			
			NotaHistorico notaHistorico = new NotaHistorico(null, nota, NotaStatus.EM_ESPERA, null, null, new Timestamp(System.currentTimeMillis()));
		
			List<NotaHistorico> listaNotaHistorico = new ArrayList<NotaHistorico>();
			listaNotaHistorico.add(notaHistorico);
			nota.setListaNotaHistorico(listaNotaHistorico);
			
			notaFiscalServicoService.saveOrUpdate(nota);
			
			NotaDocumento notaDocumento = new NotaDocumento();
			notaDocumento.setNota(nota);
			notaDocumento.setDocumento(documento);
			notaDocumento.setFromwebservice(true);
			
			notaDocumentoService.saveOrUpdate(notaDocumento);
			
			List<Documento> listaDocumento = documentoService.findForBoleto(documento.getCddocumento().toString());
			Report report;
			for (Documento documentoAux : listaDocumento) {
				if(documentoAux.getConta() == null || documentoAux.getPessoa() == null || documentoAux.getPessoa().getEmail() == null || documentoAux.getPessoa().getEmail().equals("")){
					continue;
				}
				documentoAux.setInformacoescedente(bean.getInformacoescedenteboleto());
				report = new Report("/financeiro/relBoleto");
				report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(documentoAux, false)});
				contareceberService.enviaBoleto(documentoAux, report, bean.getEmailremetenteboleto(), bean.getNomearquivoboleto(), null, null);
				documentoAux.setDocumentoacao(Documentoacao.ENVIO_BOLETO);
				documentohistoricoService.saveOrUpdate(new Documentohistorico(documentoAux));
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			
			Element doc = new Element("Documento");
			doc.setText(documento.getCddocumento().toString());
			
			resposta.addContent(status);
			resposta.addContent(doc);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	
	public void atualizaFaturamentoSL(WebRequestContext request, AtualizaFaturamentoSLBean bean) {
		request.getServletResponse().setContentType("application/xml");
		
		try{
			if(bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCdcliente().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Integer cdcliente = bean.getCdcliente();
			Integer cddocumento = bean.getCddocumento();
			
			documentoService.atualizaFaturamento(cdcliente, cddocumento);
			
			Documento documento = contareceberService.loadForEntradaWithDadosPessoa(new Documento(cddocumento));
			
			Empresa empresa = null;
			if(documento.getEmpresa() != null){
				empresa = empresaService.loadWithEndereco(documento.getEmpresa());
			} else {
				empresa = empresaService.loadPrincipalWithEndereco();
			}
			
			Pessoa cliente = documento.getPessoa();
			Endereco endereco = documento.getEndereco();
			Endereco endEmpresa = empresa.getEndereco();
			
			NotaFiscalServico nota = new NotaFiscalServico();
			nota.setCliente(new Cliente(cliente.getCdpessoa()));
			nota.setDtVencimento(documento.getDtvencimento());
			nota.setEmpresa(empresa);
			if(endEmpresa != null) nota.setMunicipioissqn(endEmpresa.getMunicipio());
			nota.setEnderecoCliente(endereco);
			nota.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);
			nota.setNotaStatus(NotaStatus.LIQUIDADA);
			nota.setNaturezaoperacao(Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
			
			NotaFiscalServicoItem item = new NotaFiscalServicoItem();
			item.setDescricao(documento.getDescricao()); 
			item.setQtde(1d);
			item.setPrecoUnitario(documento.getValor().getValue().doubleValue());
			
			List<NotaFiscalServicoItem> listaIt = new ArrayList<NotaFiscalServicoItem>();
			listaIt.add(item);
			nota.setListaItens(listaIt);
			
			NotaHistorico notaHistorico = new NotaHistorico(
					null, nota, NotaStatus.LIQUIDADA, 
					"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>", 
					null, new Timestamp(System.currentTimeMillis()));
		
			List<NotaHistorico> listaNotaHistorico = new ArrayList<NotaHistorico>();
			listaNotaHistorico.add(notaHistorico);
			nota.setListaNotaHistorico(listaNotaHistorico);
			
			notaFiscalServicoService.saveOrUpdate(nota);
			
			NotaDocumento notaDocumento = new NotaDocumento();
			notaDocumento.setNota(nota);
			notaDocumento.setDocumento(documento);
			notaDocumento.setFromwebservice(true);
			
			notaDocumentoService.saveOrUpdate(notaDocumento);			
			
			Element resposta = new Element("Resposta");
			Element status = new Element("Status");
			status.setText("1");
			resposta.addContent(status);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * A��o para enviar informa��es do Superleiloes para o W3erp.
	 * Esta a��o insere um faturamento.
	 *
	 * @param request
	 * @param bean
	 * @author Igor Silv�rio Costa
	 */
	public void inserirFaturamentoCompletoSL(WebRequestContext request, InserirFaturamentoSLBean bean) {
		request.getServletResponse().setContentType("application/xml");
		
//		System.out.println("## FATURAMENTO WEB-SERVICE ##");
		
		try{
			Cliente cliente = null;
			if(bean.getCnpjcliente() != null) {
				if(bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCnpjcliente().getBytes(), bean.getHash())){
					throw new SinedException("Erro de autentica��o.");
				}
				
				Cnpj cnpj = new Cnpj(bean.getCnpjcliente());
				cliente = clienteService.findByCnpj(cnpj);
				if(cliente == null){
					throw new SinedException("Cliente n�o encontrado.");
				}
			} else if(bean.getCpfcliente() != null){
				if(bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCpfcliente().getBytes(), bean.getHash())){
					throw new SinedException("Erro de autentica��o.");
				}
				
				Cpf cpf = new Cpf(bean.getCpfcliente());
				cliente = clienteService.findByCpf(cpf);
				if(cliente == null){
					throw new SinedException("Cliente n�o encontrado.");
				}
			} else {
				throw new SinedException("Erro de autentica��o.");
			}
			
			Money valorTotal = bean.getValorArremate().add(bean.getValorComissao()).add(bean.getValorDespesa());
			
			Integer numDiasVencimento = parametrogeralService.getInteger(Parametrogeral.PARAMETRO_DIAS_VENCIMENTO);
			Date dtemissao = new Date(System.currentTimeMillis());
			Date dtvencimento = SinedDateUtils.addDiasUteisData(dtemissao, numDiasVencimento);
			
			Empresa empresa = null;
			if(bean.getCdempresa() != null){
				empresa = empresaService.loadWithEndereco(new Empresa(bean.getCdempresa()));
			} else {
				empresa = empresaService.loadPrincipalWithEndereco();
			}
			
			Conta conta = contaService.carregaContaComMensagem(new Conta(bean.getCdconta()), null);
			
			Documento documento = new Documento();
			documento.setDocumentoacao(Documentoacao.DEFINITIVA);
			documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
			documento.setDocumentotipo(new Documentotipo(bean.getCddocumentotipo()));
			documento.setDescricao(bean.getDescricao());
			documento.setPessoa(cliente);
			documento.setTipopagamento(Tipopagamento.CLIENTE);
			documento.setDtemissao(dtemissao);
			documento.setDtvencimento(dtvencimento);
			documento.setValor(valorTotal);
			documento.setEmpresa(empresa);
			documento.setConta(new Conta(bean.getCdconta()));
			documento.setObservacaoHistorico("Conta a receber criada via webservice.");
			documento.setMensagem1(bean.getMensagem1());
			documento.setMensagem2(bean.getMensagem2());
			documento.setMensagem3(bean.getMensagem3());
			documento.setMensagem4(conta.getContacarteira().getMsgboleto1());
			documento.setMensagem5(conta.getContacarteira().getMsgboleto2());
			//Preenche a carteira do documento com a carteira padr�o
			documento.setContacarteira(conta.getContacarteira());
			
			//RATEIO
			
			List<Rateioitem> listaItens = new ArrayList<Rateioitem>();
			
			//Rateioitem do valorArremate
			Rateioitem rateioitemArremate = new Rateioitem();
			rateioitemArremate.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
			rateioitemArremate.setContagerencial(new Contagerencial(bean.getCdcontagerencialArremate()));
			rateioitemArremate.setPercentual(SinedUtil.calculaPorcentagem(bean.getValorArremate(), valorTotal));
			rateioitemArremate.setValor(bean.getValorArremate());
			listaItens.add(rateioitemArremate);
			
			//Rateioitem do valorComissao
			if(bean.getValorComissao() != null && bean.getValorComissao().getValue().doubleValue() > 0){
				Rateioitem rateioitemComissao = new Rateioitem();
				rateioitemComissao.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
				rateioitemComissao.setContagerencial(new Contagerencial(bean.getCdcontagerencialComissao()));
				rateioitemComissao.setPercentual(SinedUtil.calculaPorcentagem(bean.getValorComissao(), valorTotal));
				rateioitemComissao.setValor(bean.getValorComissao());
				listaItens.add(rateioitemComissao);
			}
			
			//Rateioitem do valorDespesa
			if(bean.getValorDespesa() != null && bean.getValorDespesa().getValue().doubleValue() > 0){
				Rateioitem rateioitemTaxa = new Rateioitem();
				rateioitemTaxa.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
				rateioitemTaxa.setContagerencial(new Contagerencial(bean.getCdcontagerencialDespesa()));
				rateioitemTaxa.setPercentual(SinedUtil.calculaPorcentagem(bean.getValorDespesa(), valorTotal));
				rateioitemTaxa.setValor(bean.getValorDespesa());
				listaItens.add(rateioitemTaxa);
			}
			
			Rateio rateio = new Rateio();
			rateio.setListaRateioitem(listaItens);
			documento.setRateio(rateio);
			
			Documentohistorico documentohistorico = new Documentohistorico(documento);
			Set<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
			listaDocumentohistorico.add(documentohistorico);
			
			documento.setListaDocumentohistorico(listaDocumentohistorico);
			
			List<Endereco> listaEnderecos = new ArrayList<Endereco>();
			listaEnderecos = contareceberService.carregaEnderecosAPartirTipoPagamento(documento);
			Endereco endereco = contareceberService.enderecoSugerido(listaEnderecos);
			documento.setEndereco(endereco);
			
			documentoService.saveOrUpdate(documento);
			
			Endereco endEmpresa = empresa.getEndereco();
			
			NotaFiscalServico nota = new NotaFiscalServico();
			nota.setCliente(cliente);
			
//			nota.setCodigocnae(bean.getCodigocnae());
//			nota.setItemlistaservico(bean.getItemlistaservico());
			
			nota.setDtVencimento(dtvencimento);
			nota.setEmpresa(empresa);
			if(endEmpresa != null) nota.setMunicipioissqn(endEmpresa.getMunicipio());
			nota.setEnderecoCliente(endereco);
			
			nota.setNotaTipo(NotaTipo.NOTA_FISCAL_SERVICO);
			nota.setNotaStatus(NotaStatus.LIQUIDADA);
			nota.setNaturezaoperacao(Naturezaoperacao.TRIBUTACAO_NO_MUNICIPIO);
			
			NotaFiscalServicoItem item = new NotaFiscalServicoItem();
			item.setDescricao(bean.getDescricao()); 
			item.setQtde(1d);
			item.setPrecoUnitario(valorTotal.getValue().doubleValue());
			
			List<NotaFiscalServicoItem> listaIt = new ArrayList<NotaFiscalServicoItem>();
			listaIt.add(item);
			nota.setListaItens(listaIt);
			
			NotaHistorico notaHistorico = new NotaHistorico(
					null, nota, NotaStatus.LIQUIDADA, 
					"Gerada a conta a receber <a href=\"javascript:visualizarContaReceber("+documento.getCddocumento()+")\">"+documento.getCddocumento()+"</a>", 
					null, new Timestamp(System.currentTimeMillis()));
		
			List<NotaHistorico> listaNotaHistorico = new ArrayList<NotaHistorico>();
			listaNotaHistorico.add(notaHistorico);
			nota.setListaNotaHistorico(listaNotaHistorico);
			
			notaFiscalServicoService.saveOrUpdate(nota);
			
			NotaDocumento notaDocumento = new NotaDocumento();
			notaDocumento.setNota(nota);
			notaDocumento.setDocumento(documento);
			notaDocumento.setFromwebservice(true);
			
			notaDocumentoService.saveOrUpdate(notaDocumento);
			
//			COMENTADO A PARTE DE ENVIO DE BOLETO
//			
//			List<Documento> listaDocumento = documentoService.findForBoleto(documento.getCddocumento().toString());
//			Report report;
//			for (Documento documentoAux : listaDocumento) {
//				try{
//					if(documentoAux.getConta() == null || documentoAux.getPessoa() == null || documentoAux.getPessoa().getEmail() == null || documentoAux.getPessoa().getEmail().equals("")){
//						continue;
//					}
//					report = new Report("/financeiro/relBoleto");
//					report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(documentoAux)});
//					contareceberService.enviaBoletoCliente(documentoAux, report, bean.getEmailremetenteboleto(), bean.getNomearquivoboleto());
//					documentoAux.setDocumentoacao(Documentoacao.ENVIO_BOLETO);
//					documentohistoricoService.saveOrUpdate(new Documentohistorico(documentoAux));
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			
			Element doc = new Element("Documento");
			doc.setText(documento.getCddocumento().toString());
			
			resposta.addContent(status);
			resposta.addContent(doc);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * A��o para enviar informa��es do Superleiloes para o W3erp.
	 * Esta a��o insere um faturamento.
	 *
	 * @param request
	 * @param bean
	 * @author Igor Silv�rio Costa
	 */
	public void inserirFaturamentoSimplesSL(WebRequestContext request, InserirFaturamentoSLBean bean) {
		request.getServletResponse().setContentType("application/xml");
		
		try{
			if(bean.getPagamentooutros() != null) {
				if(bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getPagamentooutros().getBytes(), bean.getHash())){
					throw new SinedException("Erro de autentica��o.");
				}
			} else {
				throw new SinedException("Erro de autentica��o.");
			}
			
			Money valorTotal = bean.getValorArremate().add(bean.getValorComissao()).add(bean.getValorDespesa());
			
			Integer numDiasVencimento = parametrogeralService.getInteger(Parametrogeral.PARAMETRO_DIAS_VENCIMENTO);
			Date dtemissao = new Date(System.currentTimeMillis());
			Date dtvencimento = SinedDateUtils.addDiasUteisData(dtemissao, numDiasVencimento);
			
			Empresa empresa = null;
			if(bean.getCdempresa() != null){
				empresa = empresaService.loadWithEndereco(new Empresa(bean.getCdempresa()));
			} else {
				empresa = empresaService.loadPrincipalWithEndereco();
			}
			
			Conta conta = contaService.carregaContaComMensagem(new Conta(bean.getCdconta()), null);
			
			Documento documento = new Documento();
			documento.setDocumentoacao(Documentoacao.DEFINITIVA);
			documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
			documento.setDocumentotipo(new Documentotipo(bean.getCddocumentotipo()));
			documento.setDescricao(bean.getDescricao());
			documento.setTipopagamento(Tipopagamento.OUTROS);
			documento.setOutrospagamento(bean.getPagamentooutros());
			documento.setDtemissao(dtemissao);
			documento.setDtvencimento(dtvencimento);
			documento.setValor(valorTotal);
			documento.setEmpresa(empresa);
			documento.setConta(conta);
			documento.setObservacaoHistorico("Conta a receber criada via webservice.");
			documento.setMensagem1(bean.getMensagem1());
			documento.setMensagem2(bean.getMensagem2());
			documento.setMensagem3(bean.getMensagem3());
			documento.setMensagem4(conta.getContacarteira().getMsgboleto1());
			documento.setMensagem5(conta.getContacarteira().getMsgboleto2());
			//Preenche a carteira do documento com a carteira padr�o
			documento.setContacarteira(conta.getContacarteira());
			
			//RATEIO
			
			List<Rateioitem> listaItens = new ArrayList<Rateioitem>();
			
			//Rateioitem do valorArremate
			Rateioitem rateioitemArremate = new Rateioitem();
			rateioitemArremate.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
			rateioitemArremate.setContagerencial(new Contagerencial(bean.getCdcontagerencialArremate()));
			rateioitemArremate.setPercentual(SinedUtil.calculaPorcentagem(bean.getValorArremate(), valorTotal));
			rateioitemArremate.setValor(bean.getValorArremate());
			listaItens.add(rateioitemArremate);
			
			//Rateioitem do valorComissao
			if(bean.getValorComissao() != null && bean.getValorComissao().getValue().doubleValue() > 0){
				Rateioitem rateioitemComissao = new Rateioitem();
				rateioitemComissao.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
				rateioitemComissao.setContagerencial(new Contagerencial(bean.getCdcontagerencialComissao()));
				rateioitemComissao.setPercentual(SinedUtil.calculaPorcentagem(bean.getValorComissao(), valorTotal));
				rateioitemComissao.setValor(bean.getValorComissao());
				listaItens.add(rateioitemComissao);
			}
			
			//Rateioitem do valorDespesa
			if(bean.getValorDespesa() != null && bean.getValorDespesa().getValue().doubleValue() > 0){
				Rateioitem rateioitemTaxa = new Rateioitem();
				rateioitemTaxa.setCentrocusto(new Centrocusto(bean.getCdcentrocusto()));
				rateioitemTaxa.setContagerencial(new Contagerencial(bean.getCdcontagerencialDespesa()));
				rateioitemTaxa.setPercentual(SinedUtil.calculaPorcentagem(bean.getValorDespesa(), valorTotal));
				rateioitemTaxa.setValor(bean.getValorDespesa());
				listaItens.add(rateioitemTaxa);
			}
			
			Rateio rateio = new Rateio();
			rateio.setListaRateioitem(listaItens);
			documento.setRateio(rateio);
			
			Documentohistorico documentohistorico = new Documentohistorico(documento);
			Set<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
			listaDocumentohistorico.add(documentohistorico);
			
			documento.setListaDocumentohistorico(listaDocumentohistorico);
			
			documentoService.saveOrUpdate(documento);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			
			Element doc = new Element("Documento");
			doc.setText(documento.getCddocumento().toString());
			
			resposta.addContent(status);
			resposta.addContent(doc);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	public void consultaPagamento(WebRequestContext request, ConsultarPagamentoBean bean) {
		request.getServletResponse().setContentType("application/xml");
		
		try{
			if(bean.getCddocumento() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCddocumento().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Documento documento = new Documento(bean.getCddocumento());
			documento = documentoService.load(documento, "documento.cddocumento, documento.documentoacao, documento.dtvencimento");	
			
			if(documento != null){
				String statusdocumento = "0";
				if(documento.getDocumentoacao().equals(Documentoacao.BAIXADA))
					statusdocumento = "1";
				else if(documento.getDocumentoacao().equals(Documentoacao.CANCELADA))
					statusdocumento = "2";
				
				Element resposta = new Element("Resposta");
				
				Element status = new Element("Status");
				status.setText("1");
				
				Element doc = new Element("StatusDocumento");
				doc.setText(statusdocumento);
				
				resposta.addContent(status);
				resposta.addContent(doc);
				
				XMLOutputter xout = new XMLOutputter();  
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				xout.output(resposta, byteArrayOutputStream);
				
				View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
			} else {
				View.getCurrent().println(SinedUtil.createXmlErro("Documento n�o encontrado."));
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void consultaPagamentoContrato(WebRequestContext request, ConsultarPagamentoContratoBean bean) {
		request.getServletResponse().setContentType("application/xml");
		
		try{
			if(bean.getCdcontrato() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCdcontrato().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Contrato contrato = new Contrato(bean.getCdcontrato());
			Documento documento = contareceberService.findUltimaReceita(contrato);
			
			if(documento != null){
				String statusdocumento = "0";
				if(documento.getDocumentoacao().equals(Documentoacao.BAIXADA))
					statusdocumento = "1";
				else if(documento.getDocumentoacao().equals(Documentoacao.CANCELADA))
					statusdocumento = "2";
				
				Element resposta = new Element("Resposta");
				
				Element status = new Element("Status");
				status.setText("1");
				
				Element doc = new Element("Documento");
				doc.setText(documento.getCddocumento().toString());
				
				Element stdoc = new Element("StatusDocumento");
				stdoc.setText(statusdocumento);
				
				Element dtemissao = new Element("Dtemissao");
				dtemissao.setText(SinedDateUtils.toString(documento.getDtemissao()));
				
				resposta.addContent(status);
				resposta.addContent(doc);
				resposta.addContent(stdoc);
				resposta.addContent(dtemissao);
				
				XMLOutputter xout = new XMLOutputter();  
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				xout.output(resposta, byteArrayOutputStream);
				
				View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
			} else {
				View.getCurrent().println(SinedUtil.createXmlErro("Documento n�o encontrado."));
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void consultaContacrm(WebRequestContext request, ConsultarContacrmBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getHash() == null || bean.getNome() == null || !CriptografiaUtil.verifySignature(bean.getNome().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			List<Contacrm> listaContacrm = contacrmService.findForWebservice(bean.getNome());
			
			Element resposta = new Element("Resposta");
			
			for (Contacrm crm : listaContacrm) {
				
				Element contacrm = new Element("Contacrm");
				
				Element nome = new Element("Nome");
				nome.setText(crm.getNome());
				contacrm.addContent(nome);
				
				Element tipopessoa = new Element("Tipopessoa");
				tipopessoa.setText(crm.getTipo().getValue().toString());
				contacrm.addContent(tipopessoa);
				
				if(crm.getCpf() != null){
					Element cpf = new Element("Cpf");
					cpf.setText(crm.getCpf().getValue());
					contacrm.addContent(cpf);
				}
				
				if(crm.getCnpj() != null){
					Element cnpj = new Element("Cnpj");
					cnpj.setText(crm.getCnpj().getValue());
					contacrm.addContent(cnpj);
				}
				
				if(crm.getListcontacrmcontato() != null){
					for (Contacrmcontato crmcontato : crm.getListcontacrmcontato()) {
						Element contato = new Element("Contato");
						
						Element nomeContato = new Element("NomeContato");
						nomeContato.setText(crmcontato.getNome());
						contato.addContent(nomeContato);
						
						String logradouroCompleto =  crmcontato.getLogradouro() != null ? crmcontato.getLogradouro() : "";
						logradouroCompleto += crmcontato.getNumero() != null ? ", " + crmcontato.getNumero() : "";
						logradouroCompleto += crmcontato.getComplemento() != null ? ", " + crmcontato.getComplemento() : "";
						logradouroCompleto += crmcontato.getBairro() != null ? ", " + crmcontato.getBairro() : "";
						logradouroCompleto += crmcontato.getMunicipio() != null ? ", " + crmcontato.getMunicipio().getNome() : "";
						logradouroCompleto += crmcontato.getMunicipio() != null && crmcontato.getMunicipio().getUf() != null ? "/ " + crmcontato.getMunicipio().getUf().getSigla() : "";
						
						Element endereco = new Element("Endereco");
						endereco.setText(logradouroCompleto);
						contato.addContent(endereco);
						
						contacrm.addContent(contato);
					}
				}
				
				resposta.addContent(contacrm);
			}
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			
			Format format = Format.getRawFormat();
			format.setEncoding("LATIN1");
			
			xout.setFormat(format);
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
			
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void segundaViaBoleto(WebRequestContext request, SegundaViaPagamentoBean bean) {
		request.getServletResponse().setContentType("application/xml");
		
		try{
			if(bean.getCddocumento() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCddocumento().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Documento documento = new Documento(bean.getCddocumento());
			documento = documentoService.load(documento, "documento.cddocumento, documento.documentoacao, documento.dtvencimento");	
			
			if(documento != null){
//				if(!documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)){
					if(documento.getDocumentoacao().equals(Documentoacao.BAIXADA)){
						throw new SinedException("Boleta j� paga.");
					}
					
					if(!documento.getDocumentoacao().equals(Documentoacao.CANCELADA) && !documento.getDocumentoacao().equals(Documentoacao.PREVISTA) && !documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA)){
						throw new SinedException("Documento manipulado.");
					}
					
					if(documento.getDocumentoacao().equals(Documentoacao.CANCELADA)){
						List<Documento> listaDocumento = documentoService.carregaDocumentos(documento.getCddocumento().toString());
						
						for (Documento d : listaDocumento) {
							Documentohistorico dh = new Documentohistorico();
							dh.setObservacao("Segunda via emitida via webservice.");
							
							dh = documentohistoricoService.gerarHistoricoDocumento(dh, Documentoacao.ESTORNADA, d);
							documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
							
							if(notaDocumentoService.isDocumentoWebservice(documento)){
								notaDocumentoService.estornaNotaDocumentoWebservice(documento);
							}
							
							contapagarService.updateStatusConta(Documentoacao.PREVISTA, documento.getCddocumento().toString());
							documento.setDocumentoacao(Documentoacao.PREVISTA);
						}
					}
					
//					if(documento.getDocumentoacao().equals(Documentoacao.PREVISTA)){
//						List<Documento> listaDocumento = documentoService.carregaDocumentos(documento.getCddocumento().toString());
//
//						for (Documento d : listaDocumento) {
//							Documentohistorico dh = new Documentohistorico();
//							dh.setObservacao("Segunda via emitida via webservice.");
//							
//							dh = documentohistoricoService.gerarHistoricoDocumento(dh, Documentoacao.DEFINITIVA, d);
//							documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
//							
//							contapagarService.updateStatusConta(documento.getCddocumento().toString(), Documentoacao.DEFINITIVA);
//							documento.setDocumentoacao(Documentoacao.DEFINITIVA);
//						}
//					}
					
//				}
				
				
				Integer numDiasVencimento = parametrogeralService.getInteger(Parametrogeral.PARAMETRO_DIAS_VENCIMENTO);
				Date dtvencimento = SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), numDiasVencimento);
				documentoService.atualizaDtvencimento(documento, dtvencimento);
				
				List<Documento> listaDocumento = documentoService.findForBoleto(documento.getCddocumento().toString());
				Report report;
				for (Documento documentoAux : listaDocumento) {
					if(documentoAux.getConta() == null || documentoAux.getPessoa() == null || documentoAux.getPessoa().getEmail() == null || documentoAux.getPessoa().getEmail().equals("")){
						continue;
					}
					
					documentoAux.setInformacoescedente(bean.getInformacoescedenteboleto());
					report = new Report("/financeiro/relBoleto");
					report.setDataSource(new BoletoBean[]{contareceberService.boletoDocumento(documentoAux, false)});
					contareceberService.enviaBoleto(documentoAux, report, bean.getEmailremetenteboleto(), bean.getNomearquivoboleto(), null, null);
					documentoAux.setDocumentoacao(Documentoacao.ENVIO_BOLETO);
					documentohistoricoService.saveOrUpdate(new Documentohistorico(documentoAux));
				}
				
				
				Element resposta = new Element("Resposta");
				
				Element status = new Element("Status");
				status.setText("1");
				resposta.addContent(status);
				
				XMLOutputter xout = new XMLOutputter();  
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				xout.output(resposta, byteArrayOutputStream);
				
				View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
			} else {
				View.getCurrent().println(SinedUtil.createXmlErro("Documento n�o encontrado."));
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void consultaPagamentoClienteServico(WebRequestContext request, ConsultaPagamentoClienteServicoBean bean){
		try{
			if(bean.getCdmaterial() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getCdmaterial().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			if(bean.getDtinicio() == null || bean.getDtfim() == null){
				throw new SinedException("Favor enviar todos os campos necess�rios.");
			}
			
			Cliente cliente = new Cliente(bean.getCdcliente());
			Material material = new Material(bean.getCdmaterial());
			Date dtinicio = bean.getDtinicio();
			Date dtfim = bean.getDtfim();
			
			List<PagamentoClienteServicoBean> listaPagamento = contratoService.buscaListaPagamentoWebservice(cliente, material, dtinicio, dtfim);
			
			Element resposta = new Element("Resposta");
			
			for (PagamentoClienteServicoBean pag : listaPagamento) {
				Element pagamento = new Element("Pagamento");
				
				if(pag.getCdcliente() != null){
					Element idCliente = new Element("IdCliente");
					idCliente.setText(pag.getCdcliente().toString());
					pagamento.addContent(idCliente);
				}
				
				if(pag.getTipo() != null){
					Element tipo = new Element("Tipo");
					tipo.setText(pag.getTipo());
					pagamento.addContent(tipo);
				}
				
				if(pag.getCdfrequencia() != null){
					String str;
					switch (pag.getCdfrequencia()) {
						case 1: str = "UNICA"; break;
						case 2: str = "DIARIA"; break;
						case 3: str = "SEMANAL"; break;
						case 4: str = "QUINZENAL"; break;
						case 5: str = "MENSAL"; break;
						case 6: str = "ANUAL"; break;
						case 7: str = "SEMESTRE"; break;
						case 8: str = "HORA"; break;
						case 9: str = "TRIMESTRAL"; break;
						default: str = "SEM FREQUENCIA"; break;
					}
					
					Element frequencia = new Element("Frequencia");
					frequencia.setText(str);
					pagamento.addContent(frequencia);
				}
				
				if(pag.getCddocumentoacao() != null){
					Element statusConta = new Element("StatusConta");
					if(pag.getCddocumentoacao().equals(Documentoacao.BAIXADA.getCddocumentoacao())){
						statusConta.setText("PAGO");
					} else {
						statusConta.setText("ABERTO");
					}
					pagamento.addContent(statusConta);
				}
				
				if(pag.getQuantidade() != null){
					Element quantidade = new Element("Quantidade");
					quantidade.setText(pag.getQuantidade().toString());
					pagamento.addContent(quantidade);
				}
				
				if(pag.getDtvencimento() != null){
					Element dataVencimento = new Element("DataVencimento");
					dataVencimento.setText(SinedDateUtils.toString(pag.getDtvencimento()));
					pagamento.addContent(dataVencimento);
				}
				
				if(pag.getDtpagamento() != null){
					Element dataPagamento = new Element("DataPagamento");
					dataPagamento.setText(SinedDateUtils.toString(pag.getDtpagamento()));
					pagamento.addContent(dataPagamento);
				}
				
				if(pag.getValormovimentacao() != null){
					Element valor = new Element("Valor");
					valor.setText(pag.getValormovimentacao().toString());
					pagamento.addContent(valor);
				} else if(pag.getValordocumento() != null){
					Element valor = new Element("Valor");
					valor.setText(pag.getValordocumento().toString());
					pagamento.addContent(valor);
				}
				
				if(pag.getCddocumento() != null){
					Element idTransacao = new Element("IdTransacao");
					idTransacao.setText(pag.getCddocumento().toString());
					pagamento.addContent(idTransacao);
				}
				
				resposta.addContent(pagamento);
			}
			
//			<Resposta>
//				<Pagamento>
//					<IdCliente></IdCliente>
//					<Tipo>CONTRATO/VENDA</Tipo>
//					<Frequencia>UNICA/DIARIA/SEMANAL/QUINZENAL/MENSAL/ANUAL/SEMESTRE/HORA/TRIMESTRAL</Frequencia> (Somente Tipo CONTRATO)
//					<StatusConta>ABERTO/PAGO</StatusConta>
//					<Quantidade></Quantidade>
//					<DataVencimento></DataVencimento>
//					<DataPagamento></DataPagamento> (Somente StatusConta PAGO)
//					<Valor></Valor>
//					<IdTransacao></IdTransacao>
//				</Pagamento>
//			</Resposta>
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			
			Format format = Format.getRawFormat();
			format.setEncoding("LATIN1");
			
			xout.setFormat(format);
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void consultaContasAtrasadasCliente(WebRequestContext request, ConsultaContasAtrasadasClienteBean bean){
		try{
			if((bean.getCnpj() == null && bean.getCpf() == null && bean.getCdcliente() == null) || bean.getHash() == null){
				throw new SinedException("Erro de autentica��o.");
			}
			
			// PASSAR O CNPJ TR�S VEZES NO MD5
			
			String chave;
			if(bean.getCnpj() != null && !bean.getCnpj().equals("")) chave = bean.getCnpj(); 
			else if(bean.getCpf() != null && !bean.getCpf().equals("")) chave = bean.getCpf(); 
			else if(bean.getCdcliente() != null) chave = bean.getCdcliente().toString();
			else throw new SinedException("Erro de autentica��o.");
			
			String hash = Util.crypto.makeHashMd5(chave);
			String hash2 = Util.crypto.makeHashMd5(hash);
			String hash3 = Util.crypto.makeHashMd5(hash2);
			
			if(!hash3.equals(bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Cliente cliente = null;
			if(bean.getCnpj() != null && !bean.getCnpj().equals("")) {
				Cnpj cnpj = new Cnpj(bean.getCnpj());
				cliente = clienteService.findByCnpj(cnpj);
			} else if(bean.getCpf() != null && !bean.getCpf().equals("")) {
				Cpf cpf = new Cpf(bean.getCpf());
				cliente = clienteService.findByCpf(cpf);
			} else if(bean.getCdcliente() != null) {
				cliente = clienteService.load(new Cliente(bean.getCdcliente()), "cliente.cdpessoa, cliente.nome");
			}
			
			if(cliente == null){
				throw new SinedException("Cliente n�o encontrado na base de dados.");
			}
			
			List<Documento> lista = documentoService.contasAtrasadasByCliente(cliente);
			
			Element resposta = new Element("Resposta");
			
			for (Documento documento : lista) {
				if(documento == null || 
						documento.getCddocumento() == null || 
						documento.getDtvencimento() == null || 
						documento.getAux_documento() == null || 
						documento.getAux_documento().getValoratual() == null)
					continue;
				
				Element conta = new Element("Conta");
				
				Element idConta = new Element("IdConta");
				idConta.setText(documento.getCddocumento().toString());
				conta.addContent(idConta);
				
				Element dataVencimento = new Element("DataVencimento");
				dataVencimento.setText(SinedDateUtils.toString(documento.getDtvencimento()));
				conta.addContent(dataVencimento);
				
				Element valor = new Element("Valor");
				valor.setText(documento.getAux_documento().getValoratual().toString());
				conta.addContent(valor);
				
				resposta.addContent(conta);
			}
			
//			<Resposta>
//				<Conta>
//					<IdConta></IdConta>
//					<DataVencimento></DataVencimento>
//					<Valor></Valor>
//				</Conta>
//				<Conta>
//					<IdConta></IdConta>
//					<DataVencimento></DataVencimento>
//					<Valor></Valor>
//				</Conta>
//			</Resposta>
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			
			Format format = Format.getRawFormat();
			format.setEncoding("LATIN1");
			
			xout.setFormat(format);
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * A��o para criar um centro de custo.
	 *
	 * @param request
	 * @param bean
	 * @since 01/02/2012
	 * @author Rodrigo Freitas
	 */
	public void inserirCentrocusto(WebRequestContext request, InserirCentrocustoBean bean) {
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getNome() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getNome().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Centrocusto centrocusto = new Centrocusto();
			centrocusto.setNome(bean.getNome());
			centrocusto.setAtivo(Boolean.TRUE);
			centrocustoService.saveOrUpdate(centrocusto);
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			Element centrocustoEle = new Element("Centrocusto");
			centrocustoEle.setText(centrocusto.getCdcentrocusto().toString());
			resposta.addContent(centrocustoEle);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * Action que faz a confirma��o do pedido de venda (Faturamento).
	 * 
	 * @see br.com.linkcom.sined.geral.service.PedidovendaService#findByItens(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.PedidovendaService#confirmarViaWebservice(Pedidovenda pedidovenda, List<ConfirmarPedidovendaItemBean> listaItens)
	 *
	 * @param request
	 * @param bean
	 * @since 16/11/2011
	 * @author Rodrigo Freitas
	 */
	public void confimarPedidovenda(WebRequestContext request, ConfirmarPedidovendaBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getId() == null || bean.getHash() == null || !CriptografiaUtil.verifySignature(bean.getId().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			List<ConfirmarPedidovendaItemBean> listaItens = bean.getListaItens();
			String whereIn = CollectionsUtil.listAndConcatenate(listaItens, "id", ",");
			
			if(whereIn != null && !whereIn.equals("")){
				List<Pedidovenda> listaPedidovenda = pedidovendaService.findByItens(whereIn);
				for (Pedidovenda pedidovenda : listaPedidovenda) {
					Pedidovenda pv = pedidovendaService.loadForConfirmacao(pedidovenda);
					if(pv.getPedidovendatipo() != null && Boolean.TRUE.equals(pv.getPedidovendatipo().getConfirmacaoManualWMS())){
						pv.setIdentificadorcarregamento(bean.getId());
						pedidovendaService.atualizarQuantidadesViaWebservice(pv, listaItens);
					} else{
						pedidovendaService.confirmarViaWebservice(pv, listaItens, bean.getId());
					}
				}
				
				if(bean.getListaPedidovenda() != null){
					pedidovendaService.atualizarObservacaoPedidovenda(bean.getListaPedidovenda());
				}
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	public void login(WebRequestContext request, LoginBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("UTF-8");
		
		try{
			if(bean.getLogin() == null || bean.getLogin().equals("") || 
					bean.getSenha() == null || bean.getSenha().equals("") || 
					!CriptografiaUtil.verifySignature(bean.getLogin().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			
			AuthorizationDAO authorizationDAO = (AuthorizationDAO) Neo.getApplicationContext().getBean("authorizationDAO");
			User user = authorizationDAO.findUserByLogin(bean.getLogin());
		
			if (user != null) {
				boolean passwordMatch = false;
				StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
				
				if(user.getPassword().matches("[a-zA-Z0-9\\+/]{64}")) {
					// Jasypt Strong Encryption
					if (passwordEncryptor.checkPassword(bean.getSenha(), user.getPassword())) {
						passwordMatch = true;
					}
				}
				else if(user.getPassword().matches("[0-9a-f]{32}")) {
					// MD5 simples
					if (Util.crypto.makeHashMd5(bean.getSenha()).equals(user.getPassword())) {
						passwordMatch = true;
					}
				}
				else {
					if (user.getPassword().equals(bean.getSenha())) {
						passwordMatch = true;
					}
				}
				
				if(!passwordMatch){
					throw new SinedException("Login/senha inv�lidos.");
				}
				
				if (user instanceof Usuario){				
					Usuario usuario = UsuarioDAO.getInstance().load((Usuario)user);
					if (usuario.getBloqueado().equals(true)) {					
						throw new SinedException("Usu�rio bloqueado.");					
					}			
				} else throw new SinedException("Login/senha inv�lidos.");
			} else throw new SinedException("Login/senha inv�lidos.");

			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * 
	 * M�todo para receber o retorno do WMS para autorizar a baixa ou entrega. 
	 *
	 *@author Thiago Augusto
	 *@date 01/02/2012
	 * @param request
	 * @param bean
	 */
	public void faturarBaixarEntrega(WebRequestContext request, FaturarBaixarBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			
			if(!CriptografiaUtil.verifySignature(bean.getEntrega().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Entregadocumento entregadocumento = entregadocumentosincronizacaoService.getEntregaOrigemEntregasincronizacao(bean.getEntrega());
//			Entrega entrega = entregadocumento.getEntrega();
			
			switch (bean.getRetorno()) {
			case 4:
				entregadocumento.setRetornowms(RetornoWMSEnum.CONFERIDA_OK);
				break;
			case 5:
				entregadocumento.setRetornowms(RetornoWMSEnum.CONFERIDA_DIVERGENCIA);
				break;
			case 6:
				entregadocumento.setRetornowms(RetornoWMSEnum.CONFERIDA_REJEITADA);
				break;
			case 7:
				entregadocumento.setRetornowms(RetornoWMSEnum.DEVOLVIDA);
				break;
			}
			
			if(entregadocumento != null && entregadocumento.getEntregadocumentosincronizacao() != null && entregadocumento.getEntregadocumentosincronizacao().getDevolucao() != null &&
					entregadocumento.getEntregadocumentosincronizacao().getDevolucao()){
				if(entregadocumento.getRetornowms() != null && RetornoWMSEnum.CONFERIDA_OK.equals(entregadocumento.getRetornowms())){
					materialdevolucaoService.lancarDevolucaoComConfirmacaoWms(bean.getEntrega());
				}				
			}else {
				entradafiscalService.updateRetornoWMS(entregadocumento);
			}
			
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	public void existeCliente(WebRequestContext request, ExisteClienteBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			boolean existe = clienteService.existeCpfCnpjCliente(bean.getCpf_cnpj());
			
			respostaInformacaoBean.addCampo("existe", existe);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
	}
	
	public void criarPedidovenda(WebRequestContext request, CriarPedidovendaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			pedidovendaService.criarPedidovendaVendaWebservice(bean);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
	}
	
	public void criarPedidovendaCliente(WebRequestContext request, CriarPedidovendaClienteBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			pedidovendaService.criarPedidovendaClienteWebservice(bean);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
	}
	
	public void consultaEstoqueMaterial(WebRequestContext request, ConsultarEstoqueBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdusuario() == null || bean.getHash() == null 
					|| !CriptografiaUtil.verifySignature(bean.getCdusuario().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			Element resposta = new Element("Resposta");
			Element status = new Element("Status");
			status.setText("1");
			resposta.addContent(status);
			
			List<Vgerenciarmaterial> listaEstoque = materialService.consultarEstoqueAtualForWms(bean);
			if(listaEstoque == null || listaEstoque.isEmpty()){
				View.getCurrent().println(SinedUtil.createXmlErro("A consulta de estoque no ERP n�o retornou novos resultados."));
			} else{
				Element produtos = new Element("Produtos");
				for (Vgerenciarmaterial item : listaEstoque) {
					
					Element produto = new Element("Produto");
					
					Element id = new Element("Codigoerp");
					id.setText(item.getCdmaterial().toString());
					produto.addContent(id);
					
					Element estoque = new Element("Estoque");
					estoque.setText(item.getQtdedisponivel().toString());
					produto.addContent(estoque);
					
					produtos.addContent(produto);
				}
				resposta.addContent(produtos);
				
				XMLOutputter xout = new XMLOutputter();  
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				
				Format format = Format.getRawFormat();
				format.setEncoding("LATIN1");
				
				xout.setFormat(format);
				xout.output(resposta, byteArrayOutputStream);
				
				View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * M�todo invocado pela rotina do wms de atualiza��o automatica de estoque do erp
	 * 
	 * @param request
	 * @param bean
	 * @author Rafael Salvio
	 */
	public void ajustaEstoqueMaterial(WebRequestContext request, ConsultarEstoqueBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(bean.getCdusuario() == null || bean.getHash() == null 
					|| !CriptografiaUtil.verifySignature(bean.getCdusuario().toString().getBytes(), bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			if(bean.getCnpjDeposito() != null && bean.getWhereInCdmaterial() != null && !bean.getWhereInCdmaterial().trim().isEmpty()){
				movimentacaoestoqueService.ajusteEstoqueFromWms(bean);
				View.getCurrent().println(SinedUtil.createXmlSucesso());
			} else{
				View.getCurrent().println(SinedUtil.createXmlErro("A atualiza��o de estoque no ERP n�o foi executada."));
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * M�todo que faz a valida��o com o token que est� no par�metro.
	 *
	 * @param bean
	 * @return
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 */
	private boolean validaTokenComunicacao(GenericEnvioInformacaoBean bean){
		String tokenValidacao = parametrogeralService.getValorPorNome(PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
		String hash = bean.getHash();
		return hash != null && tokenValidacao != null && hash.equals(tokenValidacao);
	}
	
	private void retornaMsgErro(GenericRespostaInformacaoBean respostaInformacaoBean, Exception e) {
		e.printStackTrace();
		View.getCurrent().println(respostaInformacaoBean.responseErro(e));
	}
}
