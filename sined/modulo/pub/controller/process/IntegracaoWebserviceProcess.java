package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jdom.Element;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Notaerrocorrecao;
import br.com.linkcom.sined.geral.bean.NovidadeVersao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Producaochaofabrica;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.auxiliar.ConfiguracaoEcommerceBeanAux;
import br.com.linkcom.sined.geral.bean.enumeration.ContasFinanceiroSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.TipoCriacaoPedidovenda;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.Ecom_PedidoVendaService;
import br.com.linkcom.sined.geral.service.EmporiumvendaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnvioemailitemhistoricoService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.HistoricoEmailCobrancaService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaerrocorrecaoService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.geral.service.NovidadeVersaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.ProducaochaofabricaService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportadorBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.BaixarPagamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CancelarPagamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CancelarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultaDadosBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultaNovidadeVersaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultaTabelaPreco;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarPagamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarContratoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarFaturaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarFaturaClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarLeadBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarOSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPagamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaRespostaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.DeleteCupomCancelVendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EmailItemWebHook;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.EmailWebHook;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ExisteClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ExistePedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.FinalizarContratoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericRespostaInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GerarFaturamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GerarFaturamentoContratoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.InserirClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ParcelasPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ProcessaCupomVendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaNotaErroCorrecaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VerificaPendenciaFinanceira;
import br.com.linkcom.sined.util.CacheWebserviceUtil;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.CriptografiaEmissorUtil;
import br.com.linkcom.sined.util.IntegracaoEcomUtil;
import br.com.linkcom.sined.util.LocalarmazenagemBeanCacheUtil;
import br.com.linkcom.sined.util.ParametroCacheUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;

import com.ibm.icu.text.SimpleDateFormat;

@Bean
@Controller(path="/pub/process/IntegracaoWebservice")
public class IntegracaoWebserviceProcess extends MultiActionController {

	public static final String PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE = "TOKEN_VALIDACAO_WEBSERVICE";
	
	private ParametrogeralService parametrogeralService;
	private ContratoService contratoService;
	private ClienteService clienteService;
	private DocumentoService documentoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private PedidovendaService pedidovendaService;
	private VendaService vendaService;
	private EmporiumvendaService emporiumvendaService;
	private LeadService leadService;
	private EmpresaService empresaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private ProducaochaofabricaService producaochaofabricaService;
	private NotaerrocorrecaoService notaerrocorrecaoService;
	private NovidadeVersaoService novidadeVersaoService;
	private MaterialService materialService;
	
	public void setNotaerrocorrecaoService(
			NotaerrocorrecaoService notaerrocorrecaoService) {
		this.notaerrocorrecaoService = notaerrocorrecaoService;
	}
	public void setProducaochaofabricaService(
			ProducaochaofabricaService producaochaofabricaService) {
		this.producaochaofabricaService = producaochaofabricaService;
	}
	public void setEmporiumvendaService(
			EmporiumvendaService emporiumvendaService) {
		this.emporiumvendaService = emporiumvendaService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setFechamentofinanceiroService(
			FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	
	public void setNovidadeVersaoService(NovidadeVersaoService novidadeVersaoService) {
		this.novidadeVersaoService = novidadeVersaoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	/**
	 * Integra��o com W3Sendgrid
	 * @author Andrey Leonardo
	 * @param request
	 * @param emailWebHook
	 * @since 24/07/2015
	 */
	public void atualizarStatusEmailWebhook(WebRequestContext request, EmailWebHook emailWebHook){
		String hash1 = Util.crypto.makeHashMd5(emailWebHook.getCdemail().toString());
		String hash2 = Util.crypto.makeHashMd5(hash1);
		if(!Util.crypto.makeHashMd5(hash2).equals(emailWebHook.getHash())){
			throw new SinedException("Erro de autentica��o.");
		}
		
		HistoricoEmailCobrancaService.getInstance().atualizarStatus(emailWebHook);
	}
	
	/**
	 * Action que faz a cria��o de uma conta a pagar
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 28/03/2014
	 */
	public void criarPagamento(WebRequestContext request, CriarPagamentoBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Documento documento = documentoService.criarPagamentoWebservice(bean);
			
			if(documento.getCddocumento() != null)
			respostaInformacaoBean.addCampo("documento", documento.getCddocumento().toString());
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "criarPagamento");
		}
	}
	
	/**
	 * Action que realiza a cria��o do faturamento com nota ou sem nota sem v�nculo com contrato.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 06/12/2012
	 */
	public void criarFatura(WebRequestContext request, CriarFaturaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Documento documento = documentoService.criarFaturaWebservice(bean);
			
			if(documento.getCddocumento() != null)
			respostaInformacaoBean.addCampo("documento", documento.getCddocumento().toString());
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "criarFatura");
		}
	}
	
	
	/**
	 * Action que realiza o faturamento de um contrato passado por par�metro.
	 *
	 * @param request
	 * @param bean
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 */
	public void gerarFaturamentoContrato(WebRequestContext request, GerarFaturamentoContratoBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String whereIn = bean.getCdcontrato() + "";
			
			if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.ATRASADO, SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.EM_ESPERA, SituacaoContrato.CANCELADO)){
				throw new SinedException("Para gerar faturamento, o(s) contrato(s) deve(m) estar com situa��o 'A CONSOLIDAR' ou 'ATEN��O'.");
			}
			
			// Verifica��o de data limite do ultimo fechamento. 
			if(fechamentofinanceiroService.verificaListaFechamentoContrato(whereIn)){
				throw new SinedException("Existem contratos cuja data de pr�ximo vencimento refere-se a um per�odo j� fechado.");
			}
			
			List<Contrato> listaContrato = contratoService.findForCobranca(whereIn);
			if(listaContrato == null || listaContrato.size() == 0)
				throw new SinedException("Nenhum contrato encontrado.");
			
			for (Contrato contrato : listaContrato) {
				contrato.setViaWebService(Boolean.TRUE);
			}
			
			String msg = contratoService.gerarFaturamento(listaContrato, false);
			
			if(msg != null && !msg.equals("")){
				throw new SinedException(msg);
			}
			
			List<String> listaIdsGerados = new ArrayList<String>();
			for (Contrato contrato : listaContrato) {
				if(contrato.getDocumento() != null && contrato.getDocumento().getCddocumento() != null){
					listaIdsGerados.add(contrato.getDocumento().getCddocumento() + "");
				}
			}
			respostaInformacaoBean.addCampo("documentoGerado", CollectionsUtil.concatenate(listaIdsGerados, ","));
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "gerarFaturamentoContrato");
		}
	}
	
	/**
	 * Action que realiza o faturamento de acordo com os dados passados por par�metro.
	 *
	 * @param request
	 * @param bean
	 * @since 11/01/2013
	 * @author Rafael Salvio
	 */
	public void gerarFaturamento(WebRequestContext request, GerarFaturamentoBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Documento documento = contratoService.gerarFaturamentoWebService(bean);

			respostaInformacaoBean.addCampo("documentoGerado", documento.getCddocumento() + "");
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "gerarFaturamento");
		}
	}
	
	/**
	 * M�todo que realiza a cri��o do cliente.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 06/12/2012
	 */
	public void criarCliente(WebRequestContext request, InserirClienteBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Integer cdpessoa = clienteService.criarClienteWebservice(bean);
			
			respostaInformacaoBean.addCampo("cliente", cdpessoa + "");
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "criarCliente");
		}
	}
	
	public void limparCache(WebRequestContext request){
		CacheWebserviceUtil.limparCache();
	}
	
	/**
	 * M�todo que faz a consulta do status do documento.
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 06/12/2012
	 */
	public void consultarPagamento(WebRequestContext request, ConsultarPagamentoBean bean) {
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String statusdocumento = "";
			if(bean.getCddocumento() != null){
				statusdocumento = documentoService.verificaStatusPagamentoWebservice(bean.getCddocumento());
			} else if(bean.getId() != null && !bean.getId().equals("")){
				TipoCriacaoPedidovenda tipoCriacaoPedidovenda = SinedUtil.getTipoCriacaoPedidovenda(bean.getTipo_criacao());
				
				if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.VENDA)){
					statusdocumento = vendaService.verificaStatusPagamentoWebservice(bean.getEmpresa_cnpj(), bean.getId());
				} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA)){
					statusdocumento = pedidovendaService.verificaStatusPagamentoWebservice(bean.getEmpresa_cnpj(), bean.getId());
				} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA_BONIFICACAO)){
					statusdocumento = "1"; // SEMPRE RETORNA 1 POIS N�O EXISTE DOCUMENTO VINCULADO
				}
			} else {
				throw new SinedException("Favor enviar o 'cddocumento' ou o 'id'.");
			}
			
			respostaInformacaoBean.addCampo("statusDocumento", statusdocumento);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "consultarPagamento");
		}
	}
	
	/**
	 * M�todo que faz o cancelamento de um documento
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 06/12/2012
	 */
	public void cancelarPagamento(WebRequestContext request, CancelarPagamentoBean bean) {
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Documento documento = new Documento(bean.getCddocumento());
			documento = documentoService.load(documento, "documento.cddocumento, documento.documentoacao, documento.dtvencimento");	
			
			if(documento == null){
				throw new SinedException("Documento n�o encontrado");
			}
			
			documentoService.validacaoCancelamentoDocumento(request, documento.getCddocumento().toString(), true);
			
			Documentohistorico documentohistorico = new Documentohistorico();
			documentohistorico.setIds(documento.getCddocumento().toString());
			documentohistorico.setObservacao(bean.getMotivo());
			documentohistorico.setDocumentoacao(Documentoacao.CANCELADA);
			documentohistorico.setFromcontareceber(Boolean.TRUE);
			
			documentoService.doCancelar(documentohistorico);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "cancelarPagamento");
		}
	}
	
	/**
	 * M�todo que faz a baixa de um documento
	 *
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 01/02/2013
	 */
	public void baixarPagamento(WebRequestContext request, BaixarPagamentoBean bean) {
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			documentoService.baixarPagamentoWebservice(bean);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "baixarPagamento");
		}
	}
	
	public void criaOSInadimplencia(WebRequestContext request, CriarOSBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			ControleAcessoUtil.util.webServiceCriarRequisicao(bean, respostaInformacaoBean);
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "criarOSInadimplencia");
		}
		
	}
	
	public void verificaNotaErroCorrecao(WebRequestContext request, VerificaNotaErroCorrecaoBean bean){
		
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean();
		
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}	
			Notaerrocorrecao respostaBean = notaerrocorrecaoService.loadNotaErroCorrecao(bean); 
			
			if(respostaBean != null ) {
				if(respostaBean.getCorrecao() != null) {
					respostaInformacaoBean.addCampo("correcao", respostaBean.getCorrecao());
				}
				if(respostaBean.getMensagem() != null) {
					respostaInformacaoBean.addCampo("mensagem", respostaBean.getMensagem());
				}
				if(respostaBean.getUrl() != null) {
					respostaInformacaoBean.addCampo("url", respostaBean.getUrl());
				}	
			}
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
			
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "verificaNotaErroCorrecao");
		}
	}
	
	public void verificaPendenciaFinanceira(WebRequestContext request, VerificaPendenciaFinanceira bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			//RETORNAR OS DADOS DO BANCO E SETAR NO RESPORTAINFORMCAOBEAN(JSON)
			
			ControleAcessoUtil.util.webServicePendenciaFinanceira(bean, respostaInformacaoBean);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "verificaPendenciaFinanceira");
		}
		
	}
	
	public void criarPedidovenda(WebRequestContext request, CriarPedidovendaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			CriarPedidovendaRespostaBean respostaBean = pedidovendaService.criarPedidovendaVendaWebservice(bean);
			
			if(respostaBean.getCdpedidovenda() != null) respostaInformacaoBean.addCampo("cdpedidovenda", respostaBean.getCdpedidovenda());
			if(respostaBean.getCdvenda() != null) respostaInformacaoBean.addCampo("cdvenda", respostaBean.getCdvenda());
			if(respostaBean.getCdvendaorcamento() != null) respostaInformacaoBean.addCampo("cdvendaorcamento", respostaBean.getCdvendaorcamento());
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "criarPedidovenda");
		}
	}
	
	public void cancelarPedidovenda(WebRequestContext request, CancelarPedidovendaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			pedidovendaService.cancelarPedidovendaVendaWebservice(bean);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);

		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "cancelarPedidovenda");
		}
	}
	
	public void criarFaturaCliente(WebRequestContext request, CriarFaturaClienteBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Documento documento = documentoService.criarFaturaClienteWebservice(bean);
			
			if(documento.getCddocumento() != null)
			respostaInformacaoBean.addCampo("documento", documento.getCddocumento().toString());
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "criarPedidovendaCliente");
		}
	}
	
	public void criarPedidovendaCliente(WebRequestContext request, CriarPedidovendaClienteBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			CriarPedidovendaRespostaBean respostaBean = pedidovendaService.criarPedidovendaClienteWebservice(bean);
			
			if(respostaBean.getCdpedidovenda() != null) respostaInformacaoBean.addCampo("cdpedidovenda", respostaBean.getCdpedidovenda());
			if(respostaBean.getCdvenda() != null) respostaInformacaoBean.addCampo("cdvenda", respostaBean.getCdvenda());
			if(respostaBean.getCdvendaorcamento() != null) respostaInformacaoBean.addCampo("cdvendaorcamento", respostaBean.getCdvendaorcamento());
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "criarPedidovendaCliente");
		}
	}
	
	public void existePedidovenda(WebRequestContext request, ExistePedidovendaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			boolean existe = pedidovendaService.existePedidovendaWebservice(bean);
			
			respostaInformacaoBean.addCampo("existe", existe);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "existePedidovenda");
		}
	}
	
	public void existeCliente(WebRequestContext request, ExisteClienteBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			boolean existe = clienteService.existeCpfCnpjCliente(bean.getCpf_cnpj());
			
			respostaInformacaoBean.addCampo("existe", existe);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "existeCliente");
		}
	}
	
	public void consultarParcelasPedidovenda(WebRequestContext request, ExistePedidovendaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			List<ParcelasPedidovendaBean> lista = pedidovendaService.consultarParcelasPedidovendaWebservice(bean);
			if(lista != null){
				respostaInformacaoBean.addCampo("faturas", lista);
			}
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "consultarParcelasPedidovenda");
		}
	}
	
	public void processaCuponsPendentes(WebRequestContext request, ProcessaCupomVendaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			emporiumvendaService.processaVendasPendentes(null, false, bean.getLoja(), null);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEmporiumUtil.util.enviarEmailErro(e, "processaCuponsPendentes");
		}
	}
	
	public void deleteCupomCancelVenda(WebRequestContext request, DeleteCupomCancelVendaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			emporiumvendaService.deleteCupomCancelVenda(bean);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEmporiumUtil.util.enviarEmailErro(e, "deleteCupomCancelVenda");
		}
	}
	
	public void gerarNotaByCupons(WebRequestContext request, ProcessaCupomVendaBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			List<Emporiumvenda> listaEmporiumVenda = EmporiumvendaService.getInstance().findForCriarNotas();
			
			if(SinedUtil.isListNotEmpty(listaEmporiumVenda)){
				NotafiscalprodutoService.getInstance().criasNotasByCupom(listaEmporiumVenda);
			}
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEmporiumUtil.util.enviarEmailErro(e, "gerarNotaByCupons");
		}
	}
	
	public void criarContrato(WebRequestContext request, CriarContratoBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Contrato contrato = contratoService.criarContratoByWebservice(bean);
			if(contrato != null){
				respostaInformacaoBean.addCampo("contrato", contrato.getCdcontrato());
			}
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEmporiumUtil.util.enviarEmailErro(e, "criarContrato");
		}
	}
	
	public void finalizarContrato(WebRequestContext request, FinalizarContratoBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			contratoService.finalizarContratoByWebservice(bean);
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEmporiumUtil.util.enviarEmailErro(e, "finalizarContrato");
		}
	}
	
	public void processarNotaFiscalTDriveWeb(WebRequestContext request) throws IOException {
//		System.out.println("\n\n\nRequisi��o recebida\n\n\n");
		String parametroCNPJ = request.getParameter("cnpj");
		String notas = request.getParameter("notas");
		try{
			Cnpj cnpj = new Cnpj(parametroCNPJ != null ? parametroCNPJ : "");
			
			if(StringUtils.isBlank(cnpj.toString())){
				throw new RuntimeException("CNPJ � obrigat�rio.");
			}
			
			if(!Cnpj.cnpjValido(cnpj.getValue())){
				throw new RuntimeException("CNPJ inv�lido.");
			}

			if(notas == null || notas.equals("")){
				throw new RuntimeException("Arquivo � obrigat�rio.");
			}
			
			Empresa empresa = empresaService.findByCnpj(cnpj);
			if(empresa==null){
				throw new RuntimeException("Empresa n�o encontrada.");
			}

			List<ImportadorBean> msgs = notaFiscalServicoService.processarArquivo(request, notas, empresa);
			retornoSucesso(request, msgs);
		}catch(Exception e){
			e.printStackTrace();
			retornoErro(request, e.getMessage());
		}
	}
	
	private void retornoErro(WebRequestContext request, String erro) throws IOException{
		request.getServletResponse().getWriter().print("MSG_ERRO=" + erro);
	}
	
	private void retornoSucesso(WebRequestContext request, List<ImportadorBean> msgs) throws IOException{
		List<Integer> notasGeradas = new ArrayList<Integer>();
		List<Integer> notasErro = new ArrayList<Integer>();
		if(msgs != null && msgs.size() > 0){
			for (ImportadorBean bean : msgs) {
				if(bean.getErro()){
					notasErro.add(bean.getNumeroNota());
				} else {
					notasGeradas.add(bean.getNumeroNota());
				}
			}
		}
		
		StringBuilder retorno = new StringBuilder("NOTAS_GERADAS=");
		Iterator<Integer> iterator = notasGeradas.iterator();
		while(iterator.hasNext()){
			retorno.append(iterator.next());
			if(iterator.hasNext()) retorno.append(",");
		}
		
		retorno.append("NOTAS_ERRO=");
		iterator = notasErro.iterator();
		while(iterator.hasNext()){
			retorno.append(iterator.next());
			if(iterator.hasNext()) retorno.append(",");
		}
		
		if (request.getParameter("origem") != null && request.getParameter("origem").equals("tdriveweb")) {
			retorno.append("MSG_ERRO=");
			
			Iterator<ImportadorBean> it = msgs.iterator();
			while(it.hasNext()){
				ImportadorBean bean = it.next();
				if(bean.getErro()) {
					retorno.append(bean.getMsgErro() + "/");
				}
			}
		}
		
		request.getServletResponse().getWriter().print(retorno.toString());
	}
	
	/**
	 * M�todo que faz a valida��o com o token que est� no par�metro.
	 *
	 * @param bean
	 * @return
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 */
	private boolean validaTokenComunicacao(GenericEnvioInformacaoBean bean){
		String tokenValidacao = parametrogeralService.getValorPorNome(PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
		String hash = bean.getHash();
		return hash != null && tokenValidacao != null && hash.equals(tokenValidacao);
	}
	
	private void retornaMsgErro(GenericRespostaInformacaoBean respostaInformacaoBean, Exception e) {
		e.printStackTrace();
		View.getCurrent().println(respostaInformacaoBean.responseErro(e));
	}
	
	//TEMP
	public void limpaCacheParametros(WebRequestContext request){
		ParametroCacheUtil.limparCache();
	}
	
	//TEMP
	public void limpaCacheLocalarmazenagemBean(WebRequestContext request){
		LocalarmazenagemBeanCacheUtil.limparCache();
	}
	
	public void criarLead(WebRequestContext request, CriarLeadBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(bean.getFormat());
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Lead lead = leadService.criarLeadWebService(bean);
			respostaInformacaoBean.addCampo("lead", lead.getCdlead());
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
	
	}
	
	/**
	 * Integra��o com W3Sendgrid
	 * @author Mairon Cezar
	 * @param request
	 * @param emailItemWebHook
	 * @since 13/04/2018
	 */
	public void atualizarStatusEmailItemWebhook(WebRequestContext request, EmailItemWebHook emailItemWebHook){
		
		String hash1 = Util.crypto.makeHashMd5(emailItemWebHook.getCdemailitem().toString());
		String hash2 = Util.crypto.makeHashMd5(hash1);
		if(!Util.crypto.makeHashMd5(hash2).equals(emailItemWebHook.getHash())){
			throw new SinedException("Erro de autentica��o.");
		}
		
		EnvioemailitemhistoricoService.getInstance().atualizarStatus(emailItemWebHook);	
	}
	
	public void consultaDadosEtiqueta(WebRequestContext request, ConsultaDadosBean bean){
		request.getServletResponse().setContentType("application/xml");
		request.getServletResponse().setCharacterEncoding("LATIN1");
		
		try{
			if(!CriptografiaEmissorUtil.verificaCriptografia("consultaDadosEtiqueta", bean.getHash())){
				throw new SinedException("Erro de autentica��o.");
			}
			
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");			
			resposta.addContent(status);
			
			List<Producaochaofabrica> listaProducaochaofabrica = producaochaofabricaService.findForEtiquetaAutomatica();
			for (Producaochaofabrica producaochaofabrica : listaProducaochaofabrica) {
				Element etiquetaElement = new Element("Etiqueta");
				
				Element producaoagendaElement = new Element("Producaoagenda");
				producaoagendaElement.setText(producaochaofabrica.getProducaoagendamaterial().getProducaoagenda().getCdproducaoagenda() + "");
				
				Element pneuElement = new Element("Pneu");
				pneuElement.setText(producaochaofabrica.getProducaoagendamaterial().getPneu().getCdpneu() + "");
				
				etiquetaElement.addContent(producaoagendaElement);
				etiquetaElement.addContent(pneuElement);
				resposta.addContent(etiquetaElement);
			}
			
			for (Producaochaofabrica producaochaofabrica : listaProducaochaofabrica) {
				producaochaofabricaService.updateEmitirEtiquetaAutomatico(producaochaofabrica, Boolean.FALSE);
			}
			
			View.getCurrent().println(SinedUtil.getRespostaXML(resposta));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * M�todo que retorna novidades de vers�es em andamento.
	 *
	 * @param bean
	 * @return
	 * @since 06/05/2020
	 * @author Diogo Souza
	 */
	public void retornaNovidadeVersao(WebRequestContext request, ConsultaNovidadeVersaoBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean();
		boolean ultimaVersao = false;
		
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			NovidadeVersao respostaBean = novidadeVersaoService.findByNovidadeVersao(bean);
			
			if(respostaBean == null && !bean.getConsultaMenu()){
				NovidadeVersao aux = novidadeVersaoService.findCdNovidadeVersao(bean);
				bean.setCdnovidadeVersao(aux.getCdnovidadeversao());
				respostaBean = novidadeVersaoService.findUltimaNovidadeVersao(bean);
				ultimaVersao = true;
			}
			
			if(respostaBean != null ) {
				if(respostaBean.getDescricao() != null) {
					respostaInformacaoBean.addCampo("descricao", respostaBean.getDescricao());
				}
				if(respostaBean.getDtfim() != null) {
					String dataFormatada = new SimpleDateFormat("dd/MM/yyyy").format(respostaBean.getDtfim()).toString();
					respostaInformacaoBean.addCampo("dtfim", dataFormatada);
				}
				if(respostaBean.getVersao() != null) {
					respostaInformacaoBean.addCampo("versao", respostaBean.getVersao());
				}
				if(respostaBean.getListaNovidadeVersaoFuncionalidade() != null) {
					respostaInformacaoBean.addCampo("funcionalidades", respostaBean.getListaNovidadeVersaoFuncionalidade());
				}
				if(ultimaVersao){
					respostaInformacaoBean.addCampo("ultimaVersao", true);
				}else {
					respostaInformacaoBean.addCampo("ultimaVersao", false);
				}
			}
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());	
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "retornaNovidadeVersao");
		}
	}
	
	public void retornaVersoesComNovidade(WebRequestContext request, ConsultaNovidadeVersaoBean bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean();
	
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			List<NovidadeVersao> listaResposta = novidadeVersaoService.findVersoesComNovidade(bean); 
			
			if(SinedUtil.isListNotEmpty(listaResposta)) {
				respostaInformacaoBean.addCampo("listaVersoes", listaResposta);
			}
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());	
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
			IntegracaoEcomUtil.util.addErro(e, "retornaNovidadeVersao");
		}
	}

	public void criarPedidovendaWebserviceEcommerce(WebRequestContext request, ConfiguracaoEcommerceBeanAux conf) {
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(conf.getFormat());
		try{
			if(!this.validaTokenComunicacao(conf)){
				throw new SinedException("Erro na autentica��o.");
			}

			List<Ecom_PedidoVenda> lista = Ecom_PedidoVendaService.getInstance().findForCriarPedido();
			for(Ecom_PedidoVenda pedido: lista){
                try {
//                	pedido.setPedidoSituacoes(Ecom_PedidoVendaSituacaoService.getInstance().findByEcomPedidoVenda(pedido));
                    PedidovendaService.getInstance().criarPedidovendaWebserviceEcommerce(pedido, conf);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("PEDIDO "+pedido+" DO ECOMPLETO N�O SINCRONIZADO COM O W3ERP");
                    View.getCurrent().println(respostaInformacaoBean.responseErro(e));
                }
			}
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		
	}
	
/*	public void gerarBaixaAutomaticaPedidovendaWebserviceEcommerce(WebRequestContext request, ConfiguracaoEcommerceBeanAux conf) {
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean(conf.getFormat());
		try{
			if(!this.validaTokenComunicacao(conf)){
				throw new SinedException("Erro na autentica��o.");
			}

			List<Ecom_PedidoVenda> lista = Ecom_PedidoVendaService.getInstance().findForCriarPedido();
			for(Ecom_PedidoVenda pedido: lista){
                try {
//                	pedido.setPedidoSituacoes(Ecom_PedidoVendaSituacaoService.getInstance().findByEcomPedidoVenda(pedido));
                    PedidovendaService.getInstance().criarPedidovendaWebserviceEcommerce(pedido, conf);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("PEDIDO "+pedido+" DO ECOMPLETO N�O SINCRONIZADO COM O W3ERP");
                    View.getCurrent().println(respostaInformacaoBean.responseErro(e));
                }
			}
			
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		} catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		
	}*/
	
	public void retornaConsultaTabelaPrecoForEcommerce(WebRequestContext request, ConsultaTabelaPreco bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean();
	
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			Material material = materialService.load(bean.getMaterial());
			
			pedidovendaService.preecheValorVendaComTabelaPreco(material, bean.getPedidovendatipo(), null, bean.getEmpresa(), bean.getUnidademedida(), bean.getPrazopagamento());
			
			if(material != null) {
				respostaInformacaoBean.addCampo("valorVenda", material.getValorvenda());
			}
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());	
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
	}
	
	public void baixaPedidosPagoEcommerce(WebRequestContext request, ConfiguracaoEcommerceBeanAux bean){
		GenericRespostaInformacaoBean respostaInformacaoBean = new GenericRespostaInformacaoBean();
	
		try{
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			if(SinedUtil.isIntegracaoEcompleto()){
				for(Ecom_PedidoVenda ecom: Ecom_PedidoVendaService.getInstance().findWithContasEmAberto()){
					Pedidovenda pedidoVenda = ecom.getPedidoVenda();
					if(pedidoVenda != null && pedidoVenda.getPedidovendatipo() != null && GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(pedidoVenda.getPedidovendatipo().getGeracaocontareceberEnum())){
						List<Documento> listaDocumento = ContareceberService.getInstance().findSituacaoDocumentoByPedidovenda(pedidoVenda);
						if(SinedUtil.isListNotEmpty(listaDocumento)){
							for(Documento documento : listaDocumento){
								if(Documentoacao.PREVISTA.equals(documento.getDocumentoacao()) || Documentoacao.DEFINITIVA.equals(documento.getDocumentoacao())){
									documento = ContareceberService.getInstance().loadForEntrada(documento);
									if(documento.getRateio() != null){
										documento.setRateio(RateioService.getInstance().loadForEntrada(documento.getRateio()));
									}
									documentoService.baixaAutomaticaByVinculoProvisionado(documento);
								}
							}
						}
					}
				}
			}
			View.getCurrent().println(respostaInformacaoBean.responseSucesso());	
		} catch (SinedException e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
		catch (Exception e) {
			retornaMsgErro(respostaInformacaoBean, e);
		}
	}
}