package br.com.linkcom.sined.modulo.pub.controller.process;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.PedidoVendaParametersBean;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.ProducaoagendasituacaoEnum;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.IndicecorrecaoService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialFaixaMarkupService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialsimilarService;
import br.com.linkcom.sined.geral.service.MaterialunidademedidaService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.ResponsavelFreteService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FornecedoragenciaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericSearchBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaParametroBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VendaWSResponseBean;
import br.com.linkcom.sined.util.EnumUtils;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.suprimento.MaterialCategoriaTreeFiltro;

@Bean
@Controller(path="/pub/process/IntegracaoWebServiceVenda")
public class IntegracaoVendaWebServiceProcess extends IntegracaoVendaWebServiceAbstractProcess{

	public ModelAndView load(WebRequestContext request, GenericSearchBean bean){
		String whereInCddocumentotipo = null;
		return new JsonModelAndView().addObject("pedidoParametro", new PedidoVendaParametroBean())
				.addObject("listaEnderecoByCliente", ObjectUtils.translateEntityListToGenericBeanList(EnderecoService.getInstance().findForVenda(bean.getCliente())))
				.addObject("listaEmpresa", EmpresaService.getInstance().findForWSVenda())
				.addObject("listaCentroCusto", CentrocustoService.getInstance().findAtivos())
				.addObject("listaProjetos", ProjetoService.getInstance().findForCombo())
				.addObject("listaPrazoPagamento", PrazopagamentoService.getInstance().findByPrazomedioWSVenda(false))
				.addObject("listaDocumentoTipo", DocumentotipoService.getInstance().findDocumentotipoUsuarioForVenda(bean.getCliente(), whereInCddocumentotipo))
				.addObject("listaPedidoVendaSituacao", EnumUtils.translateEnumToBean(Pedidovendasituacao.class))
				.addObject("listaProducaoAgendaSituacao", EnumUtils.translateEnumToBean(ProducaoagendasituacaoEnum.class))
				.addObject("listaPresencaCompradorNfe", EnumUtils.translateEnumToBean(Presencacompradornfe.class))
				.addObject("listaContaBoleto", ContaService.getInstance().findContasAtivasComPermissaoWSVenda(bean.getEmpresa()))
				.addObject("listaPedidovendatipo", ObjectUtils.translateEntityListToGenericBeanList(PedidovendatipoService.getInstance().findAll()))
				.addObject("listaResponsavelFrete", ResponsavelFreteService.getInstance().findForWSVenda());
	}
	
	public ModelAndView findAutocompleteClienteForEntrada(WebRequestContext request, GenericSearchBean bean) {
		String paramSearch = bean.getValue();
		String paramEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaCliente", ObjectUtils.translateEntityListToGenericBeanList(ClienteService.getInstance().findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuarioWSVenda(paramSearch, paramEmpresa)));
	}
	
	public ModelAndView findAutocompleteClienteForListagem(WebRequestContext request, GenericSearchBean bean) {
		String paramSearch = bean.getValue();
		String paramEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaCliente", ObjectUtils.translateEntityListToGenericBeanList(ClienteService.getInstance().findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuarioWSVenda(paramSearch, paramEmpresa)));
	}
	
	public ModelAndView findListasByCliente(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaEndereco", ObjectUtils.translateEntityListToGenericBeanList(EnderecoService.getInstance().findForVenda(bean.getCliente())))
					.addObject("listaContato", ContatoService.getInstance().findForWSVenda(bean.getCliente()))
					.addObject("listaDocumentoTipo", DocumentotipoService.getInstance().findDocumentotipoUsuarioForVenda(bean.getCliente(), bean.getWhereIn()));
	}
	
	public ModelAndView findListasByEmpresa(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaFornecedorTransportadora", FornecedorService.getInstance().findForWSVenda(bean.getEmpresa()))
					.addObject("listaContaBoleto", ContaService.getInstance().findContasAtivasComPermissaoWSVenda(bean.getEmpresa()));
	}
	
	public ModelAndView findLocalarmazenagemAtivoEmpresaAutocomplete(WebRequestContext request, GenericSearchBean bean){
		String cdEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaLocalArmazenagem", LocalarmazenagemService.getInstance().findLocalarmazenagemAtivoEmpresaAutocompleteWSVenda(bean.getValue(), cdEmpresa));
	}
	
	public ModelAndView findMaterialAutocompleteForEntrada(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterial", MaterialService.getInstance().findMaterialAutocompleteVendaForPesquisaWSVenda(bean.getValue(), bean.getEmpresa()));
	}
	
	public ModelAndView findMaterialAutocompleteForListagem(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterial", MaterialService.getInstance().findMaterialAutocompleteWSVenda(bean.getValue(), bean.getEmpresa()));
	}
	
	public ModelAndView findDadosByMaterial(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaUnidadeMedida", MaterialunidademedidaService.getInstance().findByMaterialForWSVenda(bean.getMaterial()))
				.addObject("listaMaterialFaixaMarkup", MaterialFaixaMarkupService.getInstance().findFaixaMarkupWSVenda(bean.getMaterial(), bean.getUnidadeMedida(), bean.getEmpresa()));
	}
	
	public ModelAndView findListasPagamento(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaIndiceCorrecao", IndicecorrecaoService.getInstance().findForWSVenda());
	}
	
	//LISTAGEM
	public ModelAndView findMaterialGrupo(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterialGrupo", ObjectUtils.translateEntityListToGenericBeanList(MaterialgrupoService.getInstance().findAll("materialgrupo.nome")));
	}
	
	public ModelAndView loadDadosForListagem(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterialGrupo", MaterialgrupoService.getInstance().findAllForComboWS())
				.addObject("listaProjetos", ObjectUtils.translateEntityListToGenericBeanList(ProjetoService.getInstance().findForCombo()))
				.addObject("listaClienteCategoria", CategoriaService.getInstance().findAllForComboWS())
				.addObject("listaRegiao", RegiaoService.getInstance().findAllForComboWS())
				.addObject("listaEmpresa", EmpresaService.getInstance().findAllForComboWS())
				.addObject("listaEmpresa", PrazopagamentoService.getInstance().findAllForComboWS())
				.addObject("listaPedidoVendaTipo", PedidovendatipoService.getInstance().findAllForComboWS())
				.addObject("listaDocumentoTipo", ObjectUtils.translateEntityListToGenericBeanList(DocumentotipoService.getInstance().getListaDocumentoTipoUsuarioForVenda(null)))
				.addObject("listaClienteCategoria", ObjectUtils.translateEntityListToGenericBeanList(CategoriaService.getInstance().findByTipo(true, false)));
	}
	
	/*public ModelAndView findAutocompleteUsuario(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaUsuario", ObjectUtils.translateEntityListToGenericBeanList(UsuarioService.getInstance().findForAutocomplete(bean.getValue(), "usuario.nome", "usuario.nome", true)))
					
					
					
					.addObject("listaMaterial", MaterialService.getInstance().findAllForComboWS());
	}*/
	
	public ModelAndView findAutocompleteUsuario(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaUsuario", ObjectUtils.translateEntityListToGenericBeanList(UsuarioService.getInstance().findForAutocomplete(bean.getValue(), "usuario.nome", "usuario.nome", true)))
					/*.addObject("listaDocumentoTipo", DocumentotipoService.getInstance().findDocumentotipoUsuarioForWSVenda(bean.getCliente(), null))*/;
	}
	
	public ModelAndView findAutocompleteMaterialCategoria(WebRequestContext request, GenericSearchBean bean) {
		MaterialCategoriaTreeFiltro filtro = new MaterialCategoriaTreeFiltro();
		filtro.setQ(bean.getValue());
		filtro.setAtivo(true);
		List<Materialcategoria> listaMaterialcategoria = MaterialcategoriaService.getInstance().findAutocompleteTreeView(filtro);
		return new JsonModelAndView()
					.addObject("listaMaterialCategoria", ObjectUtils.translateEntityListToGenericBeanList(listaMaterialcategoria));
	}
	
	public ModelAndView ajaxTipoDocumentoBoleto(WebRequestContext request, GenericSearchBean bean){
		if(bean.getDocumentoTipo() != null){
			bean.getDocumentoTipo().setEmpresa(bean.getEmpresa());
		}
		return PedidovendaService.getInstance().ajaxTipoDocumentoBoleto(request, bean.getDocumentoTipo());
	}

	public ModelAndView preencherFornecedor(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("listaFornecedor", ObjectUtils.translateEntityListToGenericBeanList(FornecedorService.getInstance().findForFaturamento(bean.getEmpresa(), bean.getMaterial().getCdmaterial().toString())));
	}
	
	public ModelAndView verificarPedidoVendaTipoAJAX(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().verificarPedidovendatipoAJAXJson(request, bean.getPedidoVendaTipo(), bean.getEmpresa(), bean.getFromPedidoVenda());
	}
	
	public ModelAndView ajaxTaxapedidovendaCliente(WebRequestContext request, GenericSearchBean bean) {
		Money taxapedidovenda = PedidovendaService.getInstance().getTaxapedidovendaCliente(request, bean.getCliente());
		return new JsonModelAndView()
					.addObject("taxaPedidoVenda", taxapedidovenda != null ? SinedUtil.descriptionDecimal(taxapedidovenda.getValue().doubleValue(), true) : "");
	};
	
	public ModelAndView ajaxCalcularValorAproximadoImposto(WebRequestContext request, Pedidovenda venda){
		PedidovendaService.getInstance().calcularValorAproximadoImposto(venda);
		return new JsonModelAndView().addObject("bean", venda);
	}
	
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, GenericSearchBean bean){
		if(bean.getVenda() == null){
			bean.setVenda(new Venda());
		}
		bean.getVenda().setPedidovendatipo(bean.getPedidoVendaTipo());
		return VendaService.getInstance().ajaxCamposAdicionais(request, bean.getVenda());
	}
	
	public ModelAndView exibirConfirmGerarValeCompra(WebRequestContext request, GenericSearchBean bean) {
		return PedidovendaService.getInstance().exibirConfirmGerarValeCompra(request, bean.getPedidoVendaTipo());
	}
	
	public ModelAndView changeFornecedorAgencia(WebRequestContext request, FornecedoragenciaBean fornecedoragenciaBean) {
		return VendaService.getInstance().changeFornecedoragencia(request, fornecedoragenciaBean);
	}
	
	public ModelAndView findForAutocompletePedidovendacomodatoWSVenda(WebRequestContext request, GenericSearchBean bean){
		Integer cdPedidoVenda = bean.getPedidoVenda() != null? bean.getPedidoVenda().getCdpedidovenda(): null;
		List<PedidoVendaWSBean> lista = bean.getValue() != null && StringUtils.isNotBlank(bean.getValue().toString())? PedidovendaService.getInstance().findForAutocompletePedidovendacomodatoWSVenda(bean.getValue().toString(), cdPedidoVenda): new ArrayList<PedidoVendaWSBean>();
		
		return new JsonModelAndView().addObject("listaPedidoVenda", lista);
	}
	
	public ModelAndView buscarSugestoesVendaAJAXPedido(WebRequestContext request, GenericSearchBean bean){
		Venda venda = new Venda();
		venda.setEmpresa(venda.getEmpresa());
		venda.setCliente(venda.getCliente());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setPrazopagamento(bean.getPrazoPagamento());
		venda.setMaterialtabelapreco(bean.getMaterialTabelaPreco());
		return PedidovendaService.getInstance().buscarSugestoesVendaAJAX(request, venda, request.getParameter("codigos"));
	}
	
	public ModelAndView pagamento(WebRequestContext request, GenericSearchBean bean){
		if(bean.getVenda() == null){
			bean.setVenda(new Venda());
		}
		
		try {
			bean.getVenda().setPrazopagamento(bean.getPrazoPagamento());
			if(request.getParameter("qtdeParcelas") != null){
				bean.getVenda().setQtdeParcelas(Integer.parseInt(request.getParameter("qtdeParcelas")));
			}
			if(request.getParameter("valor") != null){
				bean.getVenda().setValor(Double.parseDouble(request.getParameter("valor")));
			}
			if(request.getParameter("dtPedidoVenda") != null){
				bean.getVenda().setDtvenda(SinedDateUtils.stringToDate(request.getParameter("dtVenda")));
			}
			//bean.getPedidoVenda().setDtprazoentrega(SinedDateUtils.stringToDate(request.getParameter("dtPrazoPagamento")));
			if(request.getParameter("pagamentoDataUltimoVencimento") != null){
				bean.getVenda().setPagamentoDataUltimoVencimento(SinedDateUtils.stringToDate(request.getParameter("pagamentoDataUltimoVencimento")));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		bean.getVenda().setPrazopagamento(bean.getPrazoPagamento());
		return VendaService.getInstance().pagamento(request, bean.getVenda(), request.getParameter("dtPrazoEntrega"));
	}
	
	public ModelAndView findForTrocaMaterialSimilar(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("bean", MaterialsimilarService.getInstance().findForWSVenda(bean.getMaterial()));
	}
	
	public ModelAndView salvar(WebRequestContext request, Venda venda){
		try {
			BindException errors = new BindException(venda, "venda");
			VendaService.getInstance().validateBean(venda, errors);
			if(VendaService.getInstance().validateComprar(request, venda)){
				try {
					VendaService.getInstance().salvarCrud(request, venda);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				String msgErro = "";
				if(SinedUtil.isListNotEmpty(errors.getAllErrors())){
					for(Object obj: errors.getAllErrors()){
						msgErro += ((ObjectError)obj).getDefaultMessage()+"/n";
					}
					return new JsonModelAndView().addObject("success", false)
												.addObject("erro", msgErro);
				}
				return  new JsonModelAndView().addObject("success", false)
						.addObject("errors", SinedUtil.getMessageError(request.getMessages()));
			}
		} finally {
			request.clearMessages();
		}
		
		return null;
	}
	
	public ModelAndView comprar(WebRequestContext request, Venda venda){
		String msgErro = "";
		BindException exc = new BindException(venda, "venda");
		
		
		VendaService.getInstance().validateBean(venda, exc);
		if(SinedUtil.isListNotEmpty(exc.getAllErrors())){
			for(Object obj: exc.getAllErrors()){
				msgErro += ((ObjectError)obj).getDefaultMessage()+"/n";
			}
			request.clearMessages();
			return new JsonModelAndView().addObject("success", false)
										.addObject("erro", msgErro);
		}
		if(VendaService.getInstance().validateComprar(request, venda)){
			Pedidovenda pedidovenda = venda.getPedidovenda();
			if(pedidovenda != null){
				pedidovenda = PedidovendaService.getInstance().loadForEntrada(pedidovenda);
			}
			List<Vendamaterial> listaVendamaterialForCriarTabela = VendaService.getInstance().getListavendamaterialForCriarTabela(venda);
			List<String> listaMotivosAguardandoAprovacao = new ArrayList<String>();
			VendaService.getInstance().comprar(request, venda, pedidovenda, listaVendamaterialForCriarTabela, listaMotivosAguardandoAprovacao);
			request.clearMessages();
			return new JsonModelAndView().addObject("success", true)
										.addObject("id", Util.strings.toStringIdStyled(venda));
		}
		return  new JsonModelAndView().addObject("success", false)
									.addObject("errors", SinedUtil.getMessageError(request.getMessages()));
		/*String msgErro = "";
		BindException errors = new BindException(pedidoVenda, "pedido");
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		usuario.getCdpessoa();
		BindException exc = new BindException(pedidoVenda, "Pedido de venda");
		PedidovendaService.getInstance().validate(pedidoVenda, exc);
		
		if(SinedUtil.isListNotEmpty(exc.getAllErrors())){
			for(Object obj: exc.getAllErrors()){
				msgErro += ((ObjectError)obj).getDefaultMessage()+"/n";
			}
			return new JsonModelAndView().addObject("success", false)
										.addObject("erro", msgErro);
		}
		if(!PedidovendaService.getInstance().validateComprar(request, pedidoVenda)){
			
			
			if(request.getMessages() != null){
				for(Message msg: request.getMessages()){
					if(MessageType.ERROR.equals(msg.getType())){
						msgErro += msg.getSource()+"/n";
					}
					
				}
				return new JsonModelAndView().addObject("success", false)
											.addObject("erro", msgErro);
			}
			
			return null;
		}
		
		ModelAndView retorno = PedidovendaService.getInstance().comprar(request, pedidoVenda);
		if(retorno != null && retorno.getModel() != null && retorno.getModel().containsKey("success")){
			return new JsonModelAndView().addObject("success", retorno.getModel().get("success"));
		}
		return new JsonModelAndView().addObject("success", false);*/
	}
	
/*	public ModelAndView loadForEntrada(WebRequestContext request, GenericSearchBean bean){
		PedidoVendaParametersBean parameters = PedidovendaService.getInstance().createParametersFromRequest(request);
		Venda venda = VendaService.getInstance().loadBeanForEntrada(request, bean.getVenda(), parameters);
		return new JsonModelAndView()
					.addObject("bean", VendaService.getInstance().loadForEntradaWS(venda));
	}*/
	
	public JsonModelAndView verificarPedidoVendaTipoBloqueioPrecoAndQuantidade(WebRequestContext request, GenericSearchBean bean) {
		return PedidovendatipoService.getInstance().verificarPedidoVendaTipoBloqueioPreco(request, bean.getPedidoVenda(), bean.getPedidoVendaTipo());
	}
	
	public ModelAndView validateEditar(WebRequestContext request, GenericSearchBean bean){
		Venda venda = bean.getVenda();
		List<String> listaErros = null;
		if(Util.objects.isPersistent(venda)){
			venda.setEmpresa(bean.getEmpresa());
			VendaService.getInstance().validateEditar(request, venda);
			listaErros = SinedUtil.getMessageError(request.getMessages());
		}
		request.clearMessages();
		return new JsonModelAndView()
					.addObject("erros", listaErros);
	}
	
	public ModelAndView cancelar(WebRequestContext request, GenericSearchBean bean){
		Venda venda = bean.getVenda();
		if(venda == null){
			venda = new Venda();
		}
		venda.setIds(bean.getWhereIn());
		venda.setPedidovenda(bean.getPedidoVenda());
		venda.setObservacaohistorico(venda.getObservacaohistorico());
		ModelAndView retorno = null;
		if(VendaService.getInstance().validateCancelamento(request, venda)){
			retorno = VendaService.getInstance().cancelar(request, venda);
		}else{
			List<String> listaErros = SinedUtil.getMessageError(request.getMessages());
			retorno = new JsonModelAndView().addObject("errors", listaErros);
		}
		request.clearMessages();
		return retorno;
	}
	
	public ModelAndView validateCancelar(WebRequestContext request, GenericSearchBean bean){
		Venda venda = bean.getVenda();
		if(venda == null){
			venda = new Venda();
		}
		venda.setIds(bean.getWhereIn());
		venda.setPedidovenda(bean.getPedidoVenda());
		ModelAndView retorno = null;
		if(VendaService.getInstance().validateAbrirJustificativaCancelamentoVenda(request, bean.getWhereIn())
				&& VendaService.getInstance().validateCancelamento(request, venda)){
			retorno = new JsonModelAndView().addObject("podeCancelar", true);
		}else{
			List<String> listaErros = SinedUtil.getMessageError(request.getMessages());
			retorno = new JsonModelAndView().addObject("errors", listaErros)
											.addObject("podeCancelar", false);
		}
		request.clearMessages();
		return retorno;
	}
	
	public ModelAndView validateAprovar(WebRequestContext request, GenericSearchBean bean){
		List<String> listaErros = null;
		if(!Util.objects.isPersistent(bean.getVenda())){
			listaErros = new ArrayList<String>();
			listaErros.add("Nenhuma venda foi informada.");
			return new JsonModelAndView()
						.addObject("podeAprovar", false)
						.addObject("erro", listaErros);
		}
		if(!VendaService.getInstance().validateAprovar(request, bean.getVenda().getCdvenda())){
			listaErros = SinedUtil.getMessageError(request.getMessages());
			request.clearMessages();
			return new JsonModelAndView()
						.addObject("errors", listaErros)
						.addObject("podeAprovar", false);
		}
		request.clearMessages();
		return new JsonModelAndView()
				.addObject("podeAprovar", true);
	}
	
	public ModelAndView buscarInfMaterialProducaoAJAX(WebRequestContext request, GenericSearchBean bean){
		request.clearMessages();
		Venda venda = bean.getVenda();
		if(venda == null){
			venda = new Venda();
		}
		venda.setCliente(bean.getCliente());
		venda.setEmpresa(bean.getEmpresa());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setPrazopagamento(bean.getPrazoPagamento());
		if(Util.objects.isPersistent(bean.getMaterial())){
			venda.setCodigo(bean.getMaterial().getCdmaterial().toString());
		}
		return VendaService.getInstance().buscarInfMaterialProducaoAJAX(request, venda);
	}
	
	public ModelAndView recalcularMaterialProducaoAJAX(WebRequestContext request, GenericSearchBean bean){
		request.clearMessages();
		Venda venda = bean.getVenda();
		if(venda == null){
			venda = new Venda();
		}
		venda.setCliente(bean.getCliente());
		venda.setEmpresa(bean.getEmpresa());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setPrazopagamento(bean.getPrazoPagamento());
		if(Util.objects.isPersistent(bean.getMaterial())){
			venda.setCodigo(bean.getMaterial().getCdmaterial().toString());
		}
		return VendaService.getInstance().recalcularMaterialProducaoAJAX(request, venda);
	}
	
	public ModelAndView confirmarPedidoEmVenda(WebRequestContext request, PedidoVendaParametersBean parameters){
		request.clearMessages();
		if(!PedidovendaService.getInstance().validateConfirmar(request, parameters)){
			List<String> errors = SinedUtil.getMessageError(request.getMessages());
			return new JsonModelAndView()
						.addObject("errors", errors);
		}
		Venda venda = VendaService.getInstance().loadForConfirmarPedido(request, parameters);
		VendaWSResponseBean bean = VendaService.getInstance().loadForEntradaWS(venda);
		return new JsonModelAndView()
					.addObject("venda", bean);
	}
	
	public ModelAndView copiar(WebRequestContext request, GenericSearchBean bean){
		if(Util.objects.isPersistent(bean.getVenda())){
			Venda venda = VendaService.getInstance().criarCopia(bean.getVenda());
			return new JsonModelAndView()
						.addObject("venda", VendaService.getInstance().loadForEntradaWS(venda));
		}
		
		return null;
	}
}