package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.FechamentofinanceiroService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GerarFaturamentoContratoBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/pub/process/IntegracaoIzap")
public class IntegracaoIzapProcess extends MultiActionController {

	public static final String PARAMETRO_TOKEN_VALIDACAO_IZAP = "TOKEN_VALIDACAO_IZAP";
	
	private ParametrogeralService parametrogeralService;
	private ContratoService contratoService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	
	public void setFechamentofinanceiroService(
			FechamentofinanceiroService fechamentofinanceiroService) {
		this.fechamentofinanceiroService = fechamentofinanceiroService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	/**
	 * Action que realiza o faturamento de um contrato passado por par�metro.
	 *
	 * @param request
	 * @param bean
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 */
	public void gerarFaturamentoContrato(WebRequestContext request, GerarFaturamentoContratoBean bean){
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}
			
			String whereIn = bean.getCdcontrato() + "";
			
			if(contratoService.haveContratoSituacao(whereIn, SituacaoContrato.ATRASADO, SituacaoContrato.FINALIZADO, SituacaoContrato.NORMAL, SituacaoContrato.EM_ESPERA, SituacaoContrato.CANCELADO)){
				throw new SinedException("Para gerar faturamento, o(s) contrato(s) deve(m) estar com situa��o 'A CONSOLIDAR' ou 'ATEN��O'.");
			}
			
			// Verifica��o de data limite do ultimo fechamento. 
			if(fechamentofinanceiroService.verificaListaFechamentoContrato(whereIn)){
				throw new SinedException("Existem contratos cuja data de pr�ximo vencimento refere-se a um per�odo j� fechado.");
			}
			
			List<Contrato> listaContrato = contratoService.findForCobranca(whereIn);
			if(listaContrato == null || listaContrato.size() == 0)
				throw new SinedException("Nenhum contrato encontrado.");
			
			for (Contrato contrato : listaContrato) {
				contratoService.selecionarEnderecoFaturamento(contrato);
				contrato.setViaWebService(Boolean.TRUE);
			}
			
			String msg = contratoService.gerarFaturamento(listaContrato, false);
			
			if(msg != null && !msg.equals("")){
				throw new SinedException(msg);
			}
			
			Element resposta = new Element("Resposta");
			
			Element status = new Element("Status");
			status.setText("1");
			resposta.addContent(status);
			
			for (Contrato contrato : listaContrato) {
				if(contrato.getDocumento() != null && contrato.getDocumento().getCddocumento() != null){
					Element documentogerado = new Element("DocumentoGerado");
					documentogerado.setText(contrato.getDocumento().getCddocumento() + "");
					resposta.addContent(documentogerado);
				}
			}
			
			XMLOutputter xout = new XMLOutputter();  
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			xout.output(resposta, byteArrayOutputStream);
			
			View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErroWithStacktrace(e));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * M�todo que faz a valida��o com o token que est� no par�metro.
	 *
	 * @param bean
	 * @return
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 */
	private boolean validaTokenComunicacao(GenericEnvioInformacaoBean bean){
		String tokenValidacao = parametrogeralService.getValorPorNome(PARAMETRO_TOKEN_VALIDACAO_IZAP);
		String hash = bean.getHash();
		return hash != null && tokenValidacao != null && hash.equals(tokenValidacao);
	}
	
}
