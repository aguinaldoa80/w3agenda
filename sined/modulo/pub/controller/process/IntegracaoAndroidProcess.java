package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import br.com.linkcom.neo.authorization.AuthorizationDAO;
import br.com.linkcom.neo.authorization.AuthorizationUtilSined;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.ApplicationContext;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.neo.view.menu.MenuTag;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarPagamentoBean;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/pub/process/IntegracaoAndroid")
public class IntegracaoAndroidProcess extends MultiActionController {
	
	
	/**
	 * M�todo respons�vel por realizar o login do usu�rio no W3erp Android
	 * @param request
	 * @param bean
	 * @since 01/08/2014
	 * @author Rafael Patr�cio da Costa
	 */
	public void doLogin(WebRequestContext request, ConsultarPagamentoBean bean) {
		
		try {
			String erro = "";
			HttpServletResponse response = request.getServletResponse();
			response.setContentType("application/xml");
			
			HttpServletRequest requestHTTP = request.getServletRequest();
			
			String username = request.getParameter("username") != null && !request.getParameter("username").equals("") ? request.getParameter("username") : "0";
			String password = request.getParameter("password") != null && !request.getParameter("password").equals("") ? request.getParameter("password") : "0";
			 
			ApplicationContext applicationContext = Neo.getApplicationContext();
			AuthorizationDAO authorizationDAO = (AuthorizationDAO) applicationContext.getBean("authorizationDAO");
			
			Usuario user = (Usuario) authorizationDAO.findUserByLogin(username);
		
			if (user != null && AuthorizationUtilSined.verifyPassword(user,password)) {
				DefaultWebRequestContext requestContext = (DefaultWebRequestContext) NeoWeb.getRequestContext();
				
				user = (Usuario) AuthorizationUtilSined.updateUserInfo(user, requestHTTP, response);
				if(user == null){
					erro += "Login e/ou senha inv�lidos.";
				}
				requestContext.setUser(user);
				request.getSession().setAttribute(MenuTag.MENU_CACHE_MAP, null);
				
			} else {
				erro += "Login e/ou senha inv�lidos.";
			}
			
			if("".equals(erro)){
				Element resposta = new Element("Resposta");
				Element status = new Element("Status");
				Element id = new Element("Id");
				Element apelido = new Element("Apelido");
				
				status.setText("1");
				id.setText(user.getCdpessoa().toString());
				apelido.setText(user.getApelido());
				
				resposta.addContent(status);
				resposta.addContent(apelido);
				resposta.addContent(id);
			
				XMLOutputter xout = new XMLOutputter();  
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				xout.output(resposta, byteArrayOutputStream);
				
				View.getCurrent().println(new String(byteArrayOutputStream.toByteArray()));
			}else{
				View.getCurrent().println(SinedUtil.createXmlErro(erro));
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				View.getCurrent().println(SinedUtil.createXmlErro(e.getMessage()));
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
	}
}
