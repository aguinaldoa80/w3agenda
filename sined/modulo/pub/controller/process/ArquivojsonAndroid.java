package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.util.AndroidUtil;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path={"/pub/Arquivojsonandroid"})
public class ArquivojsonAndroid extends MultiActionController {
	
	@DefaultAction
	public void index(WebRequestContext request) {
		HttpServletResponse response = request.getServletResponse();
		
		//definindo os dados dos cabeçalhos HTTP
		response.setContentType("application/json; charset=UTF-8");
		
		
		StringBuffer sbf = new StringBuffer();
        try{
	    	String nomeArquivo = AndroidUtil.getDirAndroidJSON(SinedUtil.getStringConexaoBanco()) + System.getProperty("file.separator") + request.getParameter("f");
	    	System.out.println(nomeArquivo);
	        BufferedReader in = new BufferedReader(new FileReader(nomeArquivo));
	        String inputLine;
	        while ( (inputLine = in.readLine()) != null) sbf.append(inputLine);
	        in.close();
        } catch (IOException e) {
			e.printStackTrace();
		}
	        
		try {
			response.getWriter().print(new String(sbf.toString().getBytes(), "iso-8859-1"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}