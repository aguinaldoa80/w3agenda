package br.com.linkcom.sined.modulo.pub.controller.process;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Versao;
import br.com.linkcom.sined.geral.service.VersaoService;


@Bean
@Controller(path="/pub/process/CheckVersion")
public class CheckVersionProcess extends MultiActionController {
	
	private VersaoService versaoService;
	
	public void setVersaoService(VersaoService versaoService) {
		this.versaoService = versaoService;
	}
	
	@DefaultAction
	public void index(WebRequestContext request) {
		Versao versao = versaoService.getVersao();
		View.getCurrent().print(versao.getNumero().intValue() + "");
	}
	
}