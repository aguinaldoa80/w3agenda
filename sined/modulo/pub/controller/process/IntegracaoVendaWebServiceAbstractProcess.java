package br.com.linkcom.sined.modulo.pub.controller.process;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.auxiliar.PedidoVendaParametersBean;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ClientevendedorService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.GarantiareformaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.LoteestoqueService;
import br.com.linkcom.sined.geral.service.MaterialFaixaMarkupService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialsimilarService;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoService;
import br.com.linkcom.sined.geral.service.MaterialunidademedidaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.RegiaoService;
import br.com.linkcom.sined.geral.service.RestricaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.geral.service.VendamaterialService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.FornecedoragenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Impostovenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.UnidademedidaVO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendaPedidoReservado;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericSearchBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.LoteEstoqueWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaWSBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.filter.MaterialHistoricoPrecoFiltro;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.suprimento.MaterialCategoriaTreeFiltro;

public abstract class IntegracaoVendaWebServiceAbstractProcess extends MultiActionController{

	public ModelAndView findAutocompleteColaborador(WebRequestContext request, GenericSearchBean bean) {
		String paramSearch = bean.getValue();
		return new JsonModelAndView()
				.addObject("listaColaborador", ObjectUtils.translateEntityListToGenericBeanList(ColaboradorService.getInstance().findAutocomplete(paramSearch)));
	}
	
	public ModelAndView findAutocompleteClienteForEntrada(WebRequestContext request, GenericSearchBean bean) {
		String paramSearch = bean.getValue();
		String paramEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaCliente", ObjectUtils.translateEntityListToGenericBeanList(ClienteService.getInstance().findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuarioWSVenda(paramSearch, paramEmpresa)));
	}
	
	public ModelAndView findAutocompleteClienteForListagem(WebRequestContext request, GenericSearchBean bean) {
		String paramSearch = bean.getValue();
		String paramEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaCliente", ObjectUtils.translateEntityListToGenericBeanList(ClienteService.getInstance().findClienteAtivosNomeRazaosocialTelefoneEClienteDoUsuarioWSVenda(paramSearch, paramEmpresa)));
	}
	
	public ModelAndView findListasByCliente(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaEndereco", ObjectUtils.translateEntityListToGenericBeanList(EnderecoService.getInstance().findForVenda(bean.getCliente())))
					.addObject("listaContato", ContatoService.getInstance().findForWSVenda(bean.getCliente()))
					.addObject("listaDocumentoTipo", DocumentotipoService.getInstance().findDocumentotipoUsuarioForVenda(bean.getCliente(), bean.getWhereIn()));
	}
	
	public ModelAndView findListasByEmpresa(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaFornecedorTransportadora", FornecedorService.getInstance().findForWSVenda(bean.getEmpresa()))
					.addObject("listaContaBoleto", ContaService.getInstance().findContasAtivasComPermissaoWSVenda(bean.getEmpresa()));
	}
	
	public ModelAndView findLocalarmazenagemAtivoEmpresaAutocomplete(WebRequestContext request, GenericSearchBean bean){
		String cdEmpresa = Util.strings.getIdFromPersistentObject(bean.getEmpresa());
		return new JsonModelAndView()
				.addObject("listaLocalArmazenagem", ObjectUtils.translateEntityListToGenericBeanList(LocalarmazenagemService.getInstance().findLocalarmazenagemAtivoEmpresaAutocompleteWSVenda(bean.getValue(), cdEmpresa)));
	}
	
	public ModelAndView findMaterialAutocompleteForEntrada(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterial", MaterialService.getInstance().findMaterialAutocompleteVendaForPesquisaWSVenda(bean.getValue(), bean.getEmpresa()));
	}
	
	public ModelAndView findMaterialAutocompleteForListagem(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterial", MaterialService.getInstance().findMaterialAutocompleteWSVenda(bean.getValue(), bean.getEmpresa()));
	}
	
	public ModelAndView findDadosByMaterial(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaUnidadeMedida", MaterialunidademedidaService.getInstance().findByMaterialForWSVenda(bean.getMaterial()))
				.addObject("listaMaterialFaixaMarkup", MaterialFaixaMarkupService.getInstance().findFaixaMarkupWSVenda(bean.getMaterial(), bean.getUnidadeMedida(), bean.getEmpresa()));
	}
	
	//LISTAGEM
	public ModelAndView findMaterialGrupo(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterialGrupo", ObjectUtils.translateEntityListToGenericBeanList(MaterialgrupoService.getInstance().findAll("materialgrupo.nome")));
	}
	
	public ModelAndView loadDadosForListagem(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("listaMaterialGrupo", MaterialgrupoService.getInstance().findAllForComboWS())
				.addObject("listaProjetos", ObjectUtils.translateEntityListToGenericBeanList(ProjetoService.getInstance().findForCombo()))
				.addObject("listaClienteCategoria", CategoriaService.getInstance().findAllForComboWS())
				.addObject("listaRegiao", RegiaoService.getInstance().findAllForComboWS())
				.addObject("listaEmpresa", EmpresaService.getInstance().findAllForComboWS())
				.addObject("listaEmpresa", PrazopagamentoService.getInstance().findAllForComboWS())
				.addObject("listaPedidoVendaTipo", PedidovendatipoService.getInstance().findAllForComboWS())
				.addObject("listaDocumentoTipo", ObjectUtils.translateEntityListToGenericBeanList(DocumentotipoService.getInstance().getListaDocumentoTipoUsuarioForVenda(null)))
				.addObject("listaClienteCategoria", ObjectUtils.translateEntityListToGenericBeanList(CategoriaService.getInstance().findByTipo(true, false)));
	}
	
	/*public ModelAndView findAutocompleteUsuario(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaUsuario", ObjectUtils.translateEntityListToGenericBeanList(UsuarioService.getInstance().findForAutocomplete(bean.getValue(), "usuario.nome", "usuario.nome", true)))
					
					
					
					.addObject("listaMaterial", MaterialService.getInstance().findAllForComboWS());
	}*/
	
	public ModelAndView findAutocompleteUsuario(WebRequestContext request, GenericSearchBean bean) {
		return new JsonModelAndView()
					.addObject("listaUsuario", ObjectUtils.translateEntityListToGenericBeanList(UsuarioService.getInstance().findForAutocomplete(bean.getValue(), "usuario.nome", "usuario.nome", true)))
					/*.addObject("listaDocumentoTipo", DocumentotipoService.getInstance().findDocumentotipoUsuarioForWSVenda(bean.getCliente(), null))*/;
	}
	
	public ModelAndView findAutocompleteMaterialCategoria(WebRequestContext request, GenericSearchBean bean) {
		MaterialCategoriaTreeFiltro filtro = new MaterialCategoriaTreeFiltro();
		filtro.setQ(bean.getValue());
		filtro.setAtivo(true);
		List<Materialcategoria> listaMaterialcategoria = MaterialcategoriaService.getInstance().findAutocompleteTreeView(filtro);
		return new JsonModelAndView()
					.addObject("listaMaterialCategoria", ObjectUtils.translateEntityListToGenericBeanList(listaMaterialcategoria));
	}
	
	//IN�CIO DOS AJAXS
	public ModelAndView carregaInfEmpresa(WebRequestContext request, GenericSearchBean bean) {
		Empresa empresa = bean.getEmpresa();
		String prazoEntrega = "";
		if (Util.objects.isPersistent(empresa)) {
			empresa = EmpresaService.getInstance().load(empresa, "empresa.cdpessoa, empresa.exibiripivenda, empresa.proximoidentificadorpedidovenda, empresa.proximoidentificadorvenda, empresa.proximoidentificadorvendaorcamento");
			ModelAndView modelAndView = this.buscaPrazoEntrega(request, bean);
			if(modelAndView != null && modelAndView.getModel() != null && modelAndView.getModel().containsKey("prazoentregaMinimo")){
				prazoEntrega = (String)modelAndView.getModel().get("prazoentregaMinimo");
			}
		}
		
		return new JsonModelAndView().addObject("exibiripivenda", empresa != null && empresa.getExibiripivenda() != null && empresa.getExibiripivenda()).addObject("identificadorAutomaticoPedido", empresa != null && empresa.getProximoidentificadorpedidovenda() != null).addObject("identificadorAutomaticoVenda", empresa != null && empresa.getProximoidentificadorvenda() != null).addObject("identificadorAutomaticoOrcamento", empresa != null && empresa.getProximoidentificadorvendaorcamento() != null)
									.addObject("prazoEntregaMinimo", prazoEntrega);
	}
	
	public ModelAndView ajaxCarregarContaBoleto(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView().addObject("listaContaBoleto", ObjectUtils.translateEntityListToGenericBeanList(ContaService.getInstance().findContasAtivasComPermissao(bean.getEmpresa(), true)));
	}
	
	public ModelAndView ajaxTipoDocumentoBoleto(WebRequestContext request, GenericSearchBean bean){
		if(bean.getDocumentoTipo() != null){
			bean.getDocumentoTipo().setEmpresa(bean.getEmpresa());
		}
		return PedidovendaService.getInstance().ajaxTipoDocumentoBoleto(request, bean.getDocumentoTipo());
	}

	public ModelAndView preencherFornecedor(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("listaFornecedor", ObjectUtils.translateEntityListToGenericBeanList(FornecedorService.getInstance().findForFaturamento(bean.getEmpresa(), bean.getMaterial().getCdmaterial().toString())));
	}
	
	public ModelAndView verificarPedidoVendaTipoAJAX(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().verificarPedidovendatipoAJAXJson(request, bean.getPedidoVendaTipo(), bean.getEmpresa(), bean.getFromPedidoVenda());
	}
	
	public ModelAndView ajaxBuscaLocalArmazenagemUnico(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("listaLocalArmazenagem", ObjectUtils.translateEntityListToGenericBeanList(LocalarmazenagemService.getInstance().loadForVenda(bean.getEmpresa())));
	}
	
	public ModelAndView buscaPrazoEntrega(WebRequestContext request, GenericSearchBean bean){
		Venda venda = new Venda();
		venda.setEmpresa(bean.getEmpresa());
		return VendaService.getInstance().buscaPrazoEntrega(request, venda);
	}
	
	public ModelAndView comboBoxEndereco(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("listaEndereco", ObjectUtils.translateEntityListToGenericBeanList(EnderecoService.getInstance().findForVenda(bean.getCliente())));
	}
	
	public ModelAndView comboBoxContato(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView().addObject("listaContato", ContatoService.getInstance().findForWSVenda(bean.getCliente()));
	}
	
	public ModelAndView verificaRestricao(WebRequestContext request, GenericSearchBean bean){
		Boolean hasRestricao = RestricaoService.getInstance().verificaRestricaoSemDtLiberacao(bean.getCliente());
		return new JsonModelAndView().addObject("hasRestricao", hasRestricao);
	}
	
	public ModelAndView ajaxDocumentoAntecipacao(WebRequestContext request, GenericSearchBean bean){
		List<Documento> listaDocumento = new ArrayList<Documento>();
		Boolean isWms = EmpresaService.getInstance().isIntegracaoWms(EmpresaService.getInstance().loadPrincipal());		
		if ((isWms != null && isWms) || ParametrogeralService.getInstance().getBoolean(Parametrogeral.CONSIDERAR_PEDIDO_ANTECIPACAO))
			listaDocumento = DocumentoService.getInstance().findForAntecipacaoConsideraPedidoVenda(bean.getCliente(), bean.getDocumentoTipo(),(Pedidovenda)null/* bean.getPedidovenda()*/);
		else
			listaDocumento = DocumentoService.getInstance().findForAntecipacaoConsideraVenda(bean.getCliente(), bean.getDocumentoTipo());
		return new JsonModelAndView()
					.addObject("listaDocumento", ObjectUtils.translateEntityListToGenericBeanList(listaDocumento, "numeroValor"));
	}
	
	public ModelAndView ajaxTaxaPedidoVendaCliente(WebRequestContext request, GenericSearchBean bean) {
		Money taxapedidovenda = PedidovendaService.getInstance().getTaxapedidovendaCliente(request, bean.getCliente());
		return new JsonModelAndView()
					.addObject("taxaPedidoVenda", taxapedidovenda != null ? SinedUtil.descriptionDecimal(taxapedidovenda.getValue().doubleValue(), true) : "");
	};
	
	public ModelAndView ajaxValorDebitoContasEmAbertoByCliente(WebRequestContext requesr, GenericSearchBean bean){
		return VendaService.getInstance().ajaxValordebitoContasEmAbertoByClienteWSVenda(bean.getCliente(), bean.getWhereIn());
	}
	
	public ModelAndView ajaxLoadFormaAndPrazoPagamento(WebRequestContext request, GenericSearchBean bean){
		String cdPedidoVenda = null;
		String cdVenda = null;
		if(Util.objects.isPersistent(bean.getPedidoVenda())){
			cdPedidoVenda = bean.getPedidoVenda().getCdpedidovenda().toString();
		}else if(Util.objects.isPersistent(bean.getVenda())){
			cdVenda = bean.getVenda().getCdvenda().toString();
		}
		ModelAndView json = VendaService.getInstance().ajaxLoadFormaAndPrazoPagamento(request, bean.getCliente(), cdPedidoVenda, cdVenda);
		ModelAndView retorno = new JsonModelAndView()
									.addObject("listaPrazoPagamento", ObjectUtils.translateEntityListToGenericBeanList((List)json.getModel().get("listPrazopagamento")))
									.addObject("listaDocumentoTipo", ObjectUtils.translateEntityListToGenericBeanList((List)json.getModel().get("listDocumentotipo")));
		return retorno;
	}
	
	public ModelAndView ajaxValordebitoContasEmAbertoByCliente(WebRequestContext request, Cliente cliente){
		return VendaService.getInstance().ajaxValordebitoContasEmAbertoByCliente(request, cliente);
	}
	
	public ModelAndView ajaxBuscaObservacaoByCliente(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().ajaxBuscaObservaocaoByCliente(request, bean.getCliente());
	}
	
	public ModelAndView ajaxBuscaVendedorPrincipalByCliente(WebRequestContext request, GenericSearchBean bean){
		ModelAndView modelAndView = ClientevendedorService.getInstance().actionVendedorprincipalVendaPedido(bean.getCliente());
		ModelAndView retorno = new JsonModelAndView();
		retorno.addObject("vendedorprincipal", modelAndView.getModel().get("vendedorprincipal"));
		retorno.addObject("isVendedorPrincipal", modelAndView.getModel().get("isVendedorPrincipal"));
		retorno.addObject("colaborador", ObjectUtils.translateEntityToGenericBean(modelAndView.getModel().get("colaborador")));
		retorno.addObject("preencherVendedorPrincipal", modelAndView.getModel().get("preencherVendedorPrincipal"));
		
		return retorno;
	}
	
	public ModelAndView ajaxBuscaSaldoValeCompraByCliente(WebRequestContext request, GenericSearchBean bean){
		return ValecompraService.getInstance().actionSaldovalecompraVendaPedidoOrcamento(bean.getPedidoVenda(), bean.getCliente());
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteVenda(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().ajaxBuscaInformacaoClienteVenda(request, bean.getCliente());
	}
	
	public ModelAndView ajaxExistsGarantiareforma(WebRequestContext request, GenericSearchBean bean){
		return GarantiareformaService.getInstance().ajaxExistsGarantiareforma(request, bean.getCliente(), bean.getPedidoVenda(), bean.getVenda(), bean.getValue());
	}
	
	public ModelAndView buscarSugestaoUltimoValorVenda(WebRequestContext request, GenericSearchBean bean){
		return VendamaterialService.getInstance().buscarSugestaoUltimoValorVendaAJAXJson(bean.getMaterial(), bean.getCliente(), bean.getEmpresa(), bean.getUnidadeMedida());
	}
	
	public ModelAndView ajaxCalcularValorAproximadoImposto(WebRequestContext request, Pedidovenda venda){
		PedidovendaService.getInstance().calcularValorAproximadoImposto(venda);
		return new JsonModelAndView().addObject("bean", venda);
	}
	
	public ModelAndView buscarMaterialAJAX(WebRequestContext request, Venda venda){
		return PedidovendaService.getInstance().buscarMaterialAJAXJson(request, venda, true);
	}
	
	public ModelAndView ajaxCamposAdicionaisPedido(WebRequestContext request, GenericSearchBean bean){
		if(bean.getPedidoVenda() == null){
			bean.setPedidoVenda(new Pedidovenda());
		}
		bean.getPedidoVenda().setPedidovendatipo(bean.getPedidoVendaTipo());
		return PedidovendaService.getInstance().ajaxCamposAdicionais(request, bean.getPedidoVenda());
	}
	
	public ModelAndView preencherObservacaoPadrao(WebRequestContext request, GenericSearchBean bean) {
		return PedidovendaService.getInstance().preencherObservacaoPadrao(request, bean.getPedidoVendaTipo());
	}
	
	public ModelAndView exibirConfirmGerarValeCompra(WebRequestContext request, GenericSearchBean bean) {
		return PedidovendaService.getInstance().exibirConfirmGerarValeCompra(request, bean.getPedidoVendaTipo());
	}
	
	public ModelAndView changeFornecedorAgencia(WebRequestContext request, FornecedoragenciaBean fornecedoragenciaBean) {
		return VendaService.getInstance().changeFornecedoragencia(request, fornecedoragenciaBean);
	}
	
	public ModelAndView findForAutocompletePedidovendacomodatoWSVenda(WebRequestContext request, GenericSearchBean bean){
		Integer cdPedidoVenda = bean.getPedidoVenda() != null? bean.getPedidoVenda().getCdpedidovenda(): null;
		List<PedidoVendaWSBean> lista = bean.getValue() != null && StringUtils.isNotBlank(bean.getValue().toString())? PedidovendaService.getInstance().findForAutocompletePedidovendacomodatoWSVenda(bean.getValue().toString(), cdPedidoVenda): new ArrayList<PedidoVendaWSBean>();
		
		return new JsonModelAndView().addObject("listaPedidoVenda", lista);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ModelAndView getUnidadeMedida(WebRequestContext request, GenericSearchBean bean){
		JsonModelAndView json = (JsonModelAndView) VendaService.getInstance().getUnidademedida(request, bean.getMaterial());
		Map model = json.getModel();
		model.put("unidadeMedidaSelecionada", ObjectUtils.translateEntityToGenericBean(model.get("unidadeMedidaSelecionada")));
		model.put("unidadeMedidaPrioridade", ObjectUtils.translateEntityToGenericBean(model.get("unidadeMedidaPrioridade")));
		
		List<Unidademedida> listaFinal = new ArrayList<Unidademedida>();
		List<UnidademedidaVO> lista = (List) model.get("unidadesMedida");
		if(lista != null && lista.size() > 0){
			for (UnidademedidaVO unidademedidaVO : lista) {
				listaFinal.add(new Unidademedida(unidademedidaVO.getCdunidademedida(), unidademedidaVO.getNome(), null));
			}
		}
		model.put("unidadesMedida", ObjectUtils.translateEntityListToGenericBeanList(listaFinal));
		return json;
	}

	public ModelAndView getLoteEstoque(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().getLoteestoque(request, bean.getMaterial(), bean.getEmpresa(), bean.getUnidadeMedida(), bean.getLocalArmazenagem());
	}
	
	public ModelAndView ajaxBuscaFaixaMarkup(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("listaFaixaMarkup", MaterialFaixaMarkupService.getInstance().findFaixaMarkupWSVenda(bean.getMaterial(), bean.getUnidadeMedida(), bean.getEmpresa()));
	}
	
	public ModelAndView ajaxHabilitaCamposDeACordoComGrupoMaterial(GenericSearchBean bean){
		return MaterialService.getInstance().ajaxHabilitaCamposDeACordoComGrupoMaterial(bean.getMaterial());
	}
	
	public ModelAndView ajaxPercentualComissaoRepresentacao(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().ajaxPercentualComissaoRepresentacao(bean.getEmpresa(), bean.getFornecedor());
	}
	
	public ModelAndView recuperarListaReservados(WebRequestContext request, GenericSearchBean bean) {
		List<VendaPedidoReservado> lista = VendaService.getInstance().recuperarListaReservados(request, Util.objects.getIdIntegerFromPersistentObject(bean.getMaterial()), Util.objects.getIdIntegerFromPersistentObject(bean.getEmpresa()));
		return new JsonModelAndView().addObject("lista", lista);
	}
	
	public ModelAndView buscarPedidoVendaTipoAjax(WebRequestContext request, GenericSearchBean bean) {
		return PedidovendatipoService.getInstance().buscarPedidoVendaTipoAjax(bean.getPedidoVendaTipo());
	}
	
	public ModelAndView verificarFornecedorProdutos(WebRequestContext request, GenericSearchBean bean){
		return PedidovendaService.getInstance().verificarFornecedorProdutos(request, bean.getEmpresa(), bean.getWhereIn());
	}
	
	public ModelAndView buscarSugestoesVendaAJAXPedido(WebRequestContext request, GenericSearchBean bean){
		Venda venda = new Venda();
		venda.setEmpresa(venda.getEmpresa());
		venda.setCliente(venda.getCliente());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setPrazopagamento(bean.getPrazoPagamento());
		venda.setMaterialtabelapreco(bean.getMaterialTabelaPreco());
		return PedidovendaService.getInstance().buscarSugestoesVendaAJAX(request, venda, bean.getWhereIn());
	}
	
	public ModelAndView findQtdeLoteEstoqueForAutocomplete(WebRequestContext request, GenericSearchBean bean){
		List<Loteestoque> listaLoteestoque = LoteestoqueService.getInstance().findQtdeLoteestoqueForAutocomplete(bean.getValue(), true, bean.getMaterial(), bean.getLocalArmazenagem(), bean.getEmpresa(), bean.getUnidadeMedida());
		List<LoteEstoqueWSBean> lista = new ArrayList<LoteEstoqueWSBean>();
		for(Loteestoque le: listaLoteestoque){
			lista.add(new LoteEstoqueWSBean(le));
		}
		return new JsonModelAndView()
				.addObject("listaLoteEstoque", lista);
	}
	
	public ModelAndView getQtdeLoteEstoque(WebRequestContext request, GenericSearchBean bean){
		return PedidovendaService.getInstance().getQtdeLoteestoque(request, bean.getMaterial(), bean.getEmpresa(), bean.getLocalArmazenagem(), bean.getLoteEstoque());
	}
	
	public ModelAndView ajaxTipoDocumentoAntecipacao(WebRequestContext request, GenericSearchBean bean) {
		return VendaService.getInstance().ajaxTipoDocumentoAntecipacao(bean.getDocumentoTipo());
	}
	
	public ModelAndView ajaxBuscaInfoPrazoPagamento(WebRequestContext request, GenericSearchBean bean){
		return PedidovendaService.getInstance().ajaxBuscaInfoPrazopagamento(bean.getPrazoPagamento());
	}
	
	public ModelAndView pagamento(WebRequestContext request, GenericSearchBean bean){
		if(bean.getPedidoVenda() == null){
			bean.setPedidoVenda(new Pedidovenda());
		}
		
		try {
			bean.getPedidoVenda().setPrazopagamento(bean.getPrazoPagamento());
			if(request.getParameter("qtdeParcelas") != null){
				bean.getPedidoVenda().setQtdeParcelas(Integer.parseInt(request.getParameter("qtdeParcelas")));
			}
			if(request.getParameter("valor") != null){
				bean.getPedidoVenda().setValor(Double.parseDouble(request.getParameter("valor")));
			}
			if(request.getParameter("dtPedidoVenda") != null){
				bean.getPedidoVenda().setDtpedidovenda(SinedDateUtils.stringToDate(request.getParameter("dtPedidoVenda")));
			}
			//bean.getPedidoVenda().setDtprazoentrega(SinedDateUtils.stringToDate(request.getParameter("dtPrazoPagamento")));
			if(request.getParameter("pagamentoDataUltimoVencimento") != null){
				bean.getPedidoVenda().setPagamentoDataUltimoVencimento(SinedDateUtils.stringToDate(request.getParameter("pagamentoDataUltimoVencimento")));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		bean.getPedidoVenda().setPrazopagamento(bean.getPrazoPagamento());
		return PedidovendaService.getInstance().pagamento(request, bean.getPedidoVenda(), request.getParameter("dtPrazoEntrega"));
	}
	
	public ModelAndView ajaxValidaLoteEstoqueItensSync(WebRequestContext request, GenericSearchBean bean){
		request.setAttribute("whereIn", bean.getWhereIn());
		request.setAttribute("origemVenda", "true");
		return VendaService.getInstance().ajaxValidaLoteestoqueItensSync(request, bean.getEmpresa(), bean.getLocalArmazenagem());
	}
	
	public ModelAndView ajaxCalcularImposto(WebRequestContext request, Impostovenda impostovenda){	
		return new JsonModelAndView().addObject("bean", VendaService.getInstance().calcularImposto(impostovenda));
	}
	
	public ModelAndView ajaxBuscaMateriaisTabelaprecoByCliente(WebRequestContext request, GenericSearchBean bean){
		return MaterialtabelaprecoService.getInstance().buscaMateriaisTabelaprecoByCliente(request, bean.getCliente(), bean.getEmpresa());
	}
	
	public ModelAndView getFormasPrazosPagamentoClienteJSON(WebRequestContext request, GenericSearchBean bean){
		return ClienteService.getInstance().getFormasPrazosPagamentoClienteAlertaModelAndView(request, bean.getCliente(), bean.getParameters().getAddForma(), bean.getParameters().getAddPrazo());
	}
	
	public ModelAndView abrirPopUpInfoFinanceiraCliente(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().abrirPopUpInfoFinanceiraCliente(request, bean.getCliente() );
	}
	
	public ModelAndView criarBeanClienteInfoFinanceira(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
				.addObject("bean", VendaService.getInstance().criarBeanClienteInfoFinanceira(request, bean.getCliente()));
	}
	
	public ModelAndView findForTrocaMaterialSimilar(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("bean", MaterialsimilarService.getInstance().findForWSVenda(bean.getMaterial()));
	}
	
	public ModelAndView ajaxAtualizarValorVenda(WebRequestContext request, GenericSearchBean bean) {
		PedidoVendaParametersBean parameters = new PedidoVendaParametersBean();
		if(bean.getVenda() != null){
			if(bean.getVenda().getLarguras() != null)
				parameters.setLargura(bean.getVenda().getLarguras().toString());
			if(bean.getVenda().getAlturas() != null)
				parameters.setAltura(bean.getVenda().getAlturas().toString());
			if(bean.getVenda().getComprimentos() != null)
				parameters.setComprimento(bean.getVenda().getComprimentos().toString());
			if(bean.getVenda().getMargemarredondamento() != null)
				parameters.setMargemArredondamento(bean.getVenda().getMargemarredondamento().toString());
			if(bean.getVenda().getValorfrete() != null)
				parameters.setValorFrete(bean.getVenda().getValorfrete().toString());
		}
		return VendaService.getInstance().ajaxAtualizarvalorvenda(request, bean.getMaterial(), bean.getUnidadeMedida(), parameters);
	}
	
	public ModelAndView loadForEntrada(WebRequestContext request, GenericSearchBean bean){
		Venda venda = VendaService.getInstance().loadBeanForEntrada(request, bean.getVenda(), bean.getParameters());
		return new JsonModelAndView()
					.addObject("bean", VendaService.getInstance().loadForEntradaWS(venda));
	}
	
	public ModelAndView buscarMaterialCodigoAJAX(WebRequestContext request, GenericSearchBean bean){
		return VendaService.getInstance().buscarMaterialCodigoAJAXJson(request, bean.getValue());
	}
	
	public ModelAndView getGarantiaReformaClienteBean(WebRequestContext request, GenericSearchBean bean){
		request.setAttribute("whereInGarantiasJaSelecionadas", bean.getWhereIn());
		return new JsonModelAndView()
					.addObject("bean", GarantiareformaService.getInstance().getGarantiareformaClienteBean(request, bean.getCliente(), bean.getPedidoVenda(), bean.getVenda(), bean.getPedidoVendaTipo(), bean.getPrazoPagamento()));
	}
	
	public ModelAndView buscarInfMaterialPromocionalAJAX(WebRequestContext request, GenericSearchBean bean){
		Venda venda = bean.getVenda();
		if(venda == null){
			venda = new Venda();
		}
		venda.setCliente(bean.getCliente());
		venda.setEmpresa(bean.getEmpresa());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setPrazopagamento(bean.getPrazoPagamento());
		venda.setLocalarmazenagem(bean.getLocalArmazenagem());
		venda.setCodigo(bean.getMaterial().getCdmaterial().toString());
		
		return VendaService.getInstance().buscarInfMaterialPromocionalAJAX(request, venda);
	}
	
	public ModelAndView converteUnidadeMedida(WebRequestContext request, GenericSearchBean bean) {
		Material material = bean.getMaterial();
		material.setMaterialtabelapreco(bean.getMaterialTabelaPreco());
		material.setCliente(bean.getCliente());
		material.setEmpresatrans(bean.getEmpresa());
		material.setPedidovendatipo(bean.getPedidoVendaTipo());
		material.setPrazopagamento(bean.getPrazoPagamento());
		material.setProjeto(bean.getProjeto());
		material.setUnidademedida(bean.getUnidadeMedida());
		
		PedidoVendaParametersBean parameters = bean.getParameters();
		
		return VendaService.getInstance().converteUnidademedida(request, material, parameters, bean.getLocalArmazenagem(), bean.getEmpresa());
	}
	
	
	public ModelAndView buscarMaterialIdentificadorTabelaPrecoAJAX(WebRequestContext request, GenericSearchBean bean){
		Venda venda = new Venda();
		venda.setPrazopagamento(bean.getPrazoPagamento());
		venda.setCliente(bean.getCliente());
		venda.setEmpresa(bean.getEmpresa());
		venda.setDtvenda(bean.getData());
		venda.setMaterialtabelapreco(bean.getMaterialTabelaPreco());
		venda.setPedidovendatipo(bean.getPedidoVendaTipo());
		venda.setCodigo(bean.getValue());
		
		Map<String, Object> mapa = VendaService.getInstance().buscarMaterialIdentificadorTabelaprecoMap(request, venda);
		ModelAndView retorno = new JsonModelAndView();
		if(mapa != null){
			retorno.addObject("material", mapa.get("material"));
			retorno.addObject("unidadeMedida", mapa.get("unidadeMedida"));
			retorno.addObject("valorVenda", mapa.get("valorVenda"));
			retorno.addObject("valorVendaMaximo", mapa.get("valorVendaMaximo"));
			retorno.addObject("valorVendaMinimo", mapa.get("valorVendaMinimo"));
		}
		return retorno;
	}
	
	public ModelAndView buscarMaterialTabelaPrecoAJAX(WebRequestContext request, GenericSearchBean bean){
		Venda venda = new Venda();
		venda.setPrazopagamento(bean.getPrazoPagamento());
		venda.setCliente(bean.getCliente());
		venda.setEmpresa(bean.getEmpresa());
		venda.setUnidademedida(bean.getUnidadeMedida());
		venda.setLocalarmazenagem(bean.getLocalArmazenagem());
		if(Util.objects.isPersistent(bean.getMaterial())){
			venda.setCodigo(bean.getMaterial().getCdmaterial().toString());
		}
		
		return VendaService.getInstance().buscarMaterialtabelaprecoAJAX(request, venda);
	}
	
	public ModelAndView ajaxTipoDocumentoBoletoPagamento(WebRequestContext request, GenericSearchBean bean) {
		return VendaService.getInstance().ajaxTipoDocumentoBoletoPagamentoJson(request, bean.getWhereIn(), bean.getEmpresa());
	}
	
	public ModelAndView ajaxVerificaDesconto(WebRequestContext request, GenericSearchBean bean){
		Double percentualDesconto = null;
		if(bean.getPedidoVenda() != null){
			percentualDesconto = bean.getPedidoVenda().getPercentualdesconto();
		}
		ModelAndView modelAndView = VendaService.getInstance().ajaxVerificaDescontoByPrazopagamento(bean.getPrazoPagamento(), percentualDesconto);
		return new JsonModelAndView()
					.addObject("isPercentualDescontoMaiorQuePermitido", modelAndView.getModel().get("retorno"))
					.addObject("haveJuros", modelAndView.getModel().get("haveJuros"));
	}
	
	public ModelAndView ajaxCalcularPesoMaterial(WebRequestContext request, GenericSearchBean bean) {
		return VendaService.getInstance().ajaxCalcularPesoMaterial(request, bean.getMaterial());
	}
	
	public ModelAndView ajaxRecalcularValoresBanda(WebRequestContext request, Venda venda){
		return VendaService.getInstance().ajaxRecalcularValoresBanda(request, venda);
	}
	
	public ModelAndView getSugestaoFormaAndPrazoPagamentoCliente(WebRequestContext request, GenericSearchBean bean) {
		Prazopagamento prazoPagamento = VendaService.getInstance().getSugestaoPrazopagamentoCliente(request, bean.getCliente());
		Documentotipo documentoTipo = VendaService.getInstance().getSugestaoFormaPagamentoCliente(request, bean.getCliente());
		return new JsonModelAndView()
					.addObject("prazoPagamento", ObjectUtils.translateEntityToGenericBean(prazoPagamento))
					.addObject("formaPagamento", ObjectUtils.translateEntityToGenericBean(documentoTipo))
					.addObject("antecipacao", documentoTipo != null && Boolean.TRUE.equals(documentoTipo.getAntecipacao()));
	}
	
	public ModelAndView getListaHistoricoPrecoMaterial(WebRequestContext request, GenericSearchBean bean){
		return new JsonModelAndView()
					.addObject("listaHistoricoPreco", MaterialService.getInstance().getListaHistoricoPrecoMaterial(request, new MaterialHistoricoPrecoFiltro()));
	}
}
