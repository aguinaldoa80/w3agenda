package br.com.linkcom.sined.modulo.pub.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean;
import br.com.linkcom.geradorrelatorio.process.VisualizarRelatorioAbstractProcess;
import br.com.linkcom.geradorrelatorio.process.filtro.VisualizarRelatorioFiltro;
import br.com.linkcom.geradorrelatorio.service.GraficoService;
import br.com.linkcom.geradorrelatorio.service.RelatorioService;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ParametrogeralService;

@Controller(path={"/pub/VisualizarRelatorio"})
public class VisualizarRelatorioPubProcess extends VisualizarRelatorioAbstractProcess<VisualizarRelatorioFiltro, GeradorRelatorioBean, RelatorioService, GraficoService> {

	@Override
	public ModelAndView doGerar(WebRequestContext request, VisualizarRelatorioFiltro filtro) throws Exception {
		String tokenValidacao = ParametrogeralService.getInstance().getValorPorNome(IntegracaoWebserviceProcess.PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
		String hash = request.getParameter("hash");
		if(!hash.equals(tokenValidacao)){
			throw new Exception("Autenticação inválida.");
		}
		
		return super.doGerar(request, filtro);
	}

}
