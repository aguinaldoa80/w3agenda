package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.AuthenticationControlFilter;
import br.com.linkcom.neo.authorization.AuthorizationDAO;
import br.com.linkcom.neo.authorization.AuthorizationUtilSined;
import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.ApplicationContext;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.DefaultWebRequestContext;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.menu.MenuTag;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/pub/ValidateSession")
public class ValidateSession extends MultiActionController {

	@DefaultAction
	public ModelAndView validate(WebRequestContext request){
		return new JsonModelAndView()
		.addObject("valid", NeoWeb.getRequestContext().getUser()!=null)
		.addObject("showAlert", request.getParameter("showAlert") != null ? request.getParameter("showAlert") : "false");
	}
	
	public ModelAndView login(WebRequestContext request){
		request.setAttribute("isValidateSession", true);
		request.setAttribute("urlImagem", SinedUtil.getLogoEmpresaCabecalho(request.getServletRequest()));
		return new ModelAndView("direct:../../../jsp/login");
	}
	
	/**
	 * 
	 * @param request
	 * @see AuthenticationControlFilter#authenticateUser
	 * @return
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public ModelAndView doLogin(WebRequestContext request) throws ServletException, IOException{
		HttpServletResponse response = request.getServletResponse();
		HttpServletRequest requestHTTP = request.getServletRequest();
		String username = request.getParameter("username") != null && !request.getParameter("username").equals("") ? request.getParameter("username") : "0";
		String password = request.getParameter("password") != null && !request.getParameter("password").equals("") ? request.getParameter("password") : "0";
		 
		ApplicationContext applicationContext = Neo.getApplicationContext();
		AuthorizationDAO authorizationDAO = (AuthorizationDAO) applicationContext.getBean("authorizationDAO");
		
		User user = authorizationDAO.findUserByLogin(username);
	
		if (user != null && AuthorizationUtilSined.verifyPassword(user,password)) {
			DefaultWebRequestContext requestContext = (DefaultWebRequestContext) NeoWeb.getRequestContext();
			
			user = AuthorizationUtilSined.updateUserInfo(user, requestHTTP, response);
			if(user == null){
				return dispachError(request, applicationContext);
				
			}
			
			requestContext.setUser(user);
			request.getSession().setAttribute(MenuTag.MENU_CACHE_MAP, null);
//			Object attribute = request.getSession().getAttribute("originator");
//			if(attribute == null)
//				attribute = request.getContextPath()+config.getIndexPage();
//			response.sendRedirect((String)attribute);
		} else {
			return dispachError(request, applicationContext);
		}
		response.setContentType("text/html");
		response.getWriter().print("<html><body><script>parent.$Neo.util.closeModalValidateSession("+new Date().getTime()+");</script></body></html");
		response.getWriter().flush();
		return null;
		
	}
	
	public ModelAndView dispachError(WebRequestContext request, ApplicationContext applicationContext) throws ServletException, IOException {
		request.setAttribute("isValidateSession", true);
		request.setAttribute("login_error", true);
		request.setAttribute("urlImagem", SinedUtil.getLogoEmpresaCabecalho(request.getServletRequest()));
		return new ModelAndView("direct:../../../jsp/login");
		
		//		AuthenticationConfig config = applicationContext.getConfig().getAuthenticationConfig();
//		request.setAttribute("login_error",true);
//		request.getServletRequest().getRequestDispatcher(config.getLoginPage()).forward(request.getServletRequest(), request.getServletResponse());
//		return null;
    }
	
	
}
