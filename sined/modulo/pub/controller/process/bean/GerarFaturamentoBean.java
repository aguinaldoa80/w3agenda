package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

public class GerarFaturamentoBean extends GenericEnvioInformacaoBean {

	private Integer cdcontrato;
	private String documento;
	private String descricao;
	private Date dtvencimento;
	private Double valor;
	private String cpf;
	private Tipoidentificador tipoidentificador;
	private String mensagem1;
	private String mensagem2;
	private String mensagem3;
	private Boolean criarnota = Boolean.TRUE;	
	
	
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	
	public Date getDtvencimento() {
		return dtvencimento;
	}
	
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDocumento() {
		return documento;
	}
	
	public String getMensagem1() {
		return mensagem1;
	}

	public String getMensagem2() {
		return mensagem2;
	}

	public String getMensagem3() {
		return mensagem3;
	}
	
	public Boolean getCriarnota() {
		return criarnota;
	}
	
	public void setCriarnota(Boolean criarnota) {
		this.criarnota = criarnota;
	}

	public void setMensagem1(String mensagem1) {
		this.mensagem1 = mensagem1;
	}

	public void setMensagem2(String mensagem2) {
		this.mensagem2 = mensagem2;
	}

	public void setMensagem3(String mensagem3) {
		this.mensagem3 = mensagem3;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}	
	
	public Tipoidentificador getTipoidentificador() {
		return tipoidentificador;
	}
	
	public void setTipoidentificador(Tipoidentificador tipoidentificador) {
		this.tipoidentificador = tipoidentificador;
	}
	
	public enum Tipoidentificador {
		IDENTIFICADOR, CDCONTRATO;
	}
}
