package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;


public class InserirClienteBean extends GenericEnvioInformacaoBean {
	
	private String nome;
	private String email;
	private Integer cdcategoria;
	private Boolean substituircategoria = Boolean.FALSE;
	
	// FISICA
	private String cpf;
	private String sexo;
	private String rg;
	private Integer cdgrauinstrucao;
	private Date dtnascimento;
	private Integer cdassociacao;
	private Integer cdclienteprofissao;
	private Integer cdestadocivil;
	private Integer cdespecialidade;
	private String registro;
	
	// JURIDICA
	private String razaosocial;
	private String cnpj;
	private String inscricaomunicipal;
	private String inscricaoestadual;
	private String site;
	
	// CONTATO
	private String nomecontato;
	private String telefonecontato;
	private String celularcontato;
	
	// TELEFONES
	private String telefone;
	private String celular;
	private String telefonecomercial;
	
	// ENDERECO
	private String enderecologradouro;
	private String endereconumero;
	private String enderecocomplemento;
	private String enderecobairro;
	private String enderecocep;
	private String enderecomunicipio;
	private String enderecouf;
	private String enderecoreferencia;
	private Integer enderecotipo;
	
	
	/** CAMPOS N�O DISPONIBILIZADOS NO WEBSERVICE **/
	// ATUALIZA��O DO CLIENTE
	private Integer cdcliente;
		
	// USADO NA CRIA��O DO PEDIDO DE VENDA
	private Integer cdendereco;
	
	private boolean fromEcompleto = false;
	private boolean permitirCriarEnderecoSemMunicipio = false;
	
	
	public String getNome() {
		return nome;
	}
	public String getRazaosocial() {
		return razaosocial;
	}
	public String getEmail() {
		return email;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	public String getTelefone() {
		return telefone;
	}
	public String getEnderecologradouro() {
		return enderecologradouro;
	}
	public String getEndereconumero() {
		return endereconumero;
	}
	public String getEnderecocomplemento() {
		return enderecocomplemento;
	}
	public String getEnderecobairro() {
		return enderecobairro;
	}
	public String getEnderecocep() {
		return enderecocep;
	}
	public String getEnderecomunicipio() {
		return enderecomunicipio;
	}
	public String getEnderecouf() {
		return enderecouf;
	}
	public String getEnderecoreferencia() {
		return enderecoreferencia;
	}
	public Integer getEnderecotipo() {
		return enderecotipo;
	}
	public Integer getCdcategoria() {
		return cdcategoria;
	}
	public String getCpf() {
		return cpf;
	}
	public String getSexo() {
		return sexo;
	}
	public String getSite() {
		return site;
	}
	public String getNomecontato() {
		return nomecontato;
	}
	public String getTelefonecontato() {
		return telefonecontato;
	}
	public String getCelularcontato() {
		return celularcontato;
	}
	public String getCelular() {
		return celular;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public Integer getCdendereco() {
		return cdendereco;
	}
	public Boolean getSubstituircategoria() {
		return substituircategoria;
	}
	public String getRg() {
		return rg;
	}
	public Integer getCdgrauinstrucao() {
		return cdgrauinstrucao;
	}
	public Date getDtnascimento() {
		return dtnascimento;
	}
	public Integer getCdassociacao() {
		return cdassociacao;
	}
	public Integer getCdclienteprofissao() {
		return cdclienteprofissao;
	}
	public Integer getCdestadocivil() {
		return cdestadocivil;
	}
	public Integer getCdespecialidade() {
		return cdespecialidade;
	}
	public String getRegistro() {
		return registro;
	}
	public String getTelefonecomercial() {
		return telefonecomercial;
	}
	public void setTelefonecomercial(String telefonecomercial) {
		this.telefonecomercial = telefonecomercial;
	}
	public void setCdclienteprofissao(Integer cdclienteprofissao) {
		this.cdclienteprofissao = cdclienteprofissao;
	}
	public void setCdestadocivil(Integer cdestadocivil) {
		this.cdestadocivil = cdestadocivil;
	}
	public void setCdespecialidade(Integer cdespecialidade) {
		this.cdespecialidade = cdespecialidade;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public void setCdgrauinstrucao(Integer cdgrauinstrucao) {
		this.cdgrauinstrucao = cdgrauinstrucao;
	}
	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	public void setCdassociacao(Integer cdassociacao) {
		this.cdassociacao = cdassociacao;
	}
	public void setSubstituircategoria(Boolean substituircategoria) {
		this.substituircategoria = substituircategoria;
	}
	public void setCdendereco(Integer cdendereco) {
		this.cdendereco = cdendereco;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public void setNomecontato(String nomecontato) {
		this.nomecontato = nomecontato;
	}
	public void setTelefonecontato(String telefonecontato) {
		this.telefonecontato = telefonecontato;
	}
	public void setCelularcontato(String celularcontato) {
		this.celularcontato = celularcontato;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}
	public void setEnderecouf(String enderecouf) {
		this.enderecouf = enderecouf;
	}
	public void setEnderecoreferencia(String enderecoreferencia) {
		this.enderecoreferencia = enderecoreferencia;
	}
	public void setEnderecotipo(Integer enderecotipo) {
		this.enderecotipo = enderecotipo;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setEnderecologradouro(String enderecologradouro) {
		this.enderecologradouro = enderecologradouro;
	}
	public void setEndereconumero(String endereconumero) {
		this.endereconumero = endereconumero;
	}
	public void setEnderecocomplemento(String enderecocomplemento) {
		this.enderecocomplemento = enderecocomplemento;
	}
	public void setEnderecobairro(String enderecobairro) {
		this.enderecobairro = enderecobairro;
	}
	public void setEnderecocep(String enderecocep) {
		this.enderecocep = enderecocep;
	}
	public void setEnderecomunicipio(String enderecomunicipio) {
		this.enderecomunicipio = enderecomunicipio;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public boolean isFromEcompleto() {
		return fromEcompleto;
	}
	public void setFromEcompleto(boolean fromEcompleto) {
		this.fromEcompleto = fromEcompleto;
	}
	public boolean isPermitirCriarEnderecoSemMunicipio() {
		return permitirCriarEnderecoSemMunicipio;
	}
	public void setPermitirCriarEnderecoSemMunicipio(boolean permitirCriarEnderecoSemMunicipio) {
		this.permitirCriarEnderecoSemMunicipio = permitirCriarEnderecoSemMunicipio;
	}
}
