package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;

public class IntegracaoEmporiumBean extends GenericEnvioInformacaoBean {
	
	private Arquivoemporiumtipo tipo;
	private Integer id;
	private Arquivo arquivo;
	
	public Arquivoemporiumtipo getTipo() {
		return tipo;
	}
	public Integer getId() {
		return id;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setTipo(Arquivoemporiumtipo tipo) {
		this.tipo = tipo;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
}
