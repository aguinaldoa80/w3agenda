package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaPagamentoWSBean {

	protected Integer cdpedidovendapagamento;
	protected GenericBean pedidovenda;
	protected GenericBean documento;
	protected GenericBean documentotipo;
	protected Integer banco;
	protected Integer agencia;
	protected Integer conta;
	protected String numero;
	protected Money valororiginal;
	protected Date dataparcela;
	protected Money valorjuros;
	protected GenericBean documentoantecipacao;
	protected String emitente;
	protected String cpfcnpj;
	protected GenericBean cheque;
	
	public Integer getCdpedidovendapagamento() {
		return cdpedidovendapagamento;
	}
	public void setCdpedidovendapagamento(Integer cdpedidovendapagamento) {
		this.cdpedidovendapagamento = cdpedidovendapagamento;
	}
	public GenericBean getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(GenericBean pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public GenericBean getDocumento() {
		return documento;
	}
	public void setDocumento(GenericBean documento) {
		this.documento = documento;
	}
	public GenericBean getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(GenericBean documentotipo) {
		this.documentotipo = documentotipo;
	}
	public Integer getBanco() {
		return banco;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public Integer getAgencia() {
		return agencia;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public Integer getConta() {
		return conta;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Money getValororiginal() {
		return valororiginal;
	}
	public void setValororiginal(Money valororiginal) {
		this.valororiginal = valororiginal;
	}
	public Date getDataparcela() {
		return dataparcela;
	}
	public void setDataparcela(Date dataparcela) {
		this.dataparcela = dataparcela;
	}
	public Money getValorjuros() {
		return valorjuros;
	}
	public void setValorjuros(Money valorjuros) {
		this.valorjuros = valorjuros;
	}
	public GenericBean getDocumentoantecipacao() {
		return documentoantecipacao;
	}
	public void setDocumentoantecipacao(GenericBean documentoantecipacao) {
		this.documentoantecipacao = documentoantecipacao;
	}
	public String getEmitente() {
		return emitente;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	public String getCpfcnpj() {
		return cpfcnpj;
	}
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
	public GenericBean getCheque() {
		return cheque;
	}
	public void setCheque(GenericBean cheque) {
		this.cheque = cheque;
	}
}
