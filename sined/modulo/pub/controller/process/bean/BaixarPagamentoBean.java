package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

public class BaixarPagamentoBean extends GenericEnvioInformacaoBean {
	
	private Integer cddocumento;
	private Integer tipo_criacao;
	private String id;
	private Double valor;
	private Date dtpagamento;
	private Integer cdconta;
	private String empresa_cnpj;
	private Integer cddocumentotipo;
	private Boolean incluir_taxa_venda = Boolean.FALSE;
	private Boolean considerar_valor_atual = Boolean.FALSE;
	
	public Integer getCddocumento() {
		return cddocumento;
	}
	public Date getDtpagamento() {
		return dtpagamento;
	}
	public Double getValor() {
		return valor;
	}
	public Integer getCdconta() {
		return cdconta;
	}
	public Integer getTipo_criacao() {
		return tipo_criacao;
	}
	public String getId() {
		return id;
	}
	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public Boolean getIncluir_taxa_venda() {
		return incluir_taxa_venda;
	}
	public Boolean getConsiderar_valor_atual() {
		return considerar_valor_atual;
	}
	public void setIncluir_taxa_venda(Boolean incluirTaxaVenda) {
		incluir_taxa_venda = incluirTaxaVenda;
	}
	public void setConsiderar_valor_atual(Boolean considerarValorAtual) {
		considerar_valor_atual = considerarValorAtual;
	}
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setEmpresa_cnpj(String empresaCnpj) {
		empresa_cnpj = empresaCnpj;
	}
	public void setTipo_criacao(Integer tipoCriacao) {
		tipo_criacao = tipoCriacao;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
}
