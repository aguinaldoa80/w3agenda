package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.bean.GenericBean;

public class VendaWSResponseBean {

	protected Integer cdvenda;
	protected GenericBean empresa;
	protected GenericBean cliente;
	protected GenericBean contato;
	protected GenericBean clienteindicacao;
	protected Money valorusadovalecompra;
	protected Money valoraproximadoimposto;
	protected GenericBean endereco;
	protected GenericBean colaborador;
	protected Date dtvenda;
	protected Date dtestorno;
	protected String observacao;
	protected String observacaointerna;
	protected TranslateEnumBean presencacompradornfe;
	protected GenericBean vendasituacao;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected GenericBean frete;
	protected GenericBean terceiro;
	protected Money valorfrete;
	protected GenericBean pedidovenda;
	protected String identificadorexterno;
	protected Integer identificadorcarregamento;
	protected Money desconto;
	protected Double percentualdesconto;
	protected Money saldofinal;
	protected GenericBean projeto;
	protected Boolean bonificacao = false;
	protected Boolean comodato = false;
	protected Money taxavenda;
	protected GenericBean localarmazenagem;
	protected GenericBean materialtabelapreco;
	protected TranslateEnumBean expedicaovendasituacao;
	protected GenericBean vendaorcamento;
	protected Boolean origemwebservice;
	protected Money valordescontorepresentacao;
	protected Money valorbrutorepresentacao;
	protected Boolean criacaofinalizada;
	protected Boolean aprovando;
	protected Boolean limitecreditoexcedido;
	
	protected List<VendaMaterialWSBean> listavendamaterial = new ListSet<VendaMaterialWSBean>(VendaMaterialWSBean.class);
	protected List<VendaPagamentoWSBean> listavendapagamento = new ListSet<VendaPagamentoWSBean>(VendaPagamentoWSBean.class);
	protected List<VendaHistoricoWSBean> listavendahistorico = new ListSet<VendaHistoricoWSBean>(VendaHistoricoWSBean.class);
	protected List<VendaMaterialMestreWSBean> listaVendamaterialmestre = new ListSet<VendaMaterialMestreWSBean>(VendaMaterialMestreWSBean.class);
	protected List<VendaValorCampoExtraWSBean> listaVendavalorcampoextra = new ListSet<VendaValorCampoExtraWSBean>(VendaValorCampoExtraWSBean.class);
	protected List<VendaPagamentoRepresentacaoWSBean> listavendapagamentorepresentacao = new ListSet<VendaPagamentoRepresentacaoWSBean>(VendaPagamentoRepresentacaoWSBean.class);
	protected List<ProducaoOrdemItemAdicionalWSBean> listaProducaoordemitemadicional = new ListSet<ProducaoOrdemItemAdicionalWSBean>(ProducaoOrdemItemAdicionalWSBean.class);
	protected List<ProducaoAgendaItemAdicionalWSBean> listaProducaoagendaitemadicional = new ListSet<ProducaoAgendaItemAdicionalWSBean>(ProducaoAgendaItemAdicionalWSBean.class);
	
	protected GenericBean indicecorrecao;
	protected GenericBean contaboleto;
	protected String identificador;
	protected Money taxapedidovenda;
	protected GenericBean fornecedor;
	protected String index;
	protected GenericBean fornecedorAgencia;
	protected Money percentualRepresentacao;
	protected GenericBean centrocusto;
	protected Money valorFreteCIF;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	public Integer getCdvenda() {
		return cdvenda;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public GenericBean getEmpresa() {
		return empresa;
	}
	public void setEmpresa(GenericBean empresa) {
		this.empresa = empresa;
	}
	public GenericBean getCliente() {
		return cliente;
	}
	public void setCliente(GenericBean cliente) {
		this.cliente = cliente;
	}
	public GenericBean getContato() {
		return contato;
	}
	public void setContato(GenericBean contato) {
		this.contato = contato;
	}
	public GenericBean getClienteindicacao() {
		return clienteindicacao;
	}
	public void setClienteindicacao(GenericBean clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}
	public Money getValorusadovalecompra() {
		return valorusadovalecompra;
	}
	public void setValorusadovalecompra(Money valorusadovalecompra) {
		this.valorusadovalecompra = valorusadovalecompra;
	}
	public Money getValoraproximadoimposto() {
		return valoraproximadoimposto;
	}
	public void setValoraproximadoimposto(Money valoraproximadoimposto) {
		this.valoraproximadoimposto = valoraproximadoimposto;
	}
	public GenericBean getEndereco() {
		return endereco;
	}
	public void setEndereco(GenericBean endereco) {
		this.endereco = endereco;
	}
	public GenericBean getColaborador() {
		return colaborador;
	}
	public void setColaborador(GenericBean colaborador) {
		this.colaborador = colaborador;
	}
	public Date getDtvenda() {
		return dtvenda;
	}
	public void setDtvenda(Date dtvenda) {
		this.dtvenda = dtvenda;
	}
	public Date getDtestorno() {
		return dtestorno;
	}
	public void setDtestorno(Date dtestorno) {
		this.dtestorno = dtestorno;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getObservacaointerna() {
		return observacaointerna;
	}
	public void setObservacaointerna(String observacaointerna) {
		this.observacaointerna = observacaointerna;
	}
	public TranslateEnumBean getPresencacompradornfe() {
		return presencacompradornfe;
	}
	public void setPresencacompradornfe(TranslateEnumBean presencacompradornfe) {
		this.presencacompradornfe = presencacompradornfe;
	}
	public GenericBean getVendasituacao() {
		return vendasituacao;
	}
	public void setVendasituacao(GenericBean vendasituacao) {
		this.vendasituacao = vendasituacao;
	}
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public GenericBean getFrete() {
		return frete;
	}
	public void setFrete(GenericBean frete) {
		this.frete = frete;
	}
	public GenericBean getTerceiro() {
		return terceiro;
	}
	public void setTerceiro(GenericBean terceiro) {
		this.terceiro = terceiro;
	}
	public Money getValorfrete() {
		return valorfrete;
	}
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	public GenericBean getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(GenericBean pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public String getIdentificadorexterno() {
		return identificadorexterno;
	}
	public void setIdentificadorexterno(String identificadorexterno) {
		this.identificadorexterno = identificadorexterno;
	}
	public Integer getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}
	public void setIdentificadorcarregamento(Integer identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	public Money getSaldofinal() {
		return saldofinal;
	}
	public void setSaldofinal(Money saldofinal) {
		this.saldofinal = saldofinal;
	}
	public GenericBean getProjeto() {
		return projeto;
	}
	public void setProjeto(GenericBean projeto) {
		this.projeto = projeto;
	}
	public Boolean getBonificacao() {
		return bonificacao;
	}
	public void setBonificacao(Boolean bonificacao) {
		this.bonificacao = bonificacao;
	}
	public Boolean getComodato() {
		return comodato;
	}
	public void setComodato(Boolean comodato) {
		this.comodato = comodato;
	}
	public Money getTaxavenda() {
		return taxavenda;
	}
	public void setTaxavenda(Money taxavenda) {
		this.taxavenda = taxavenda;
	}
	public GenericBean getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(GenericBean localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public GenericBean getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	public void setMaterialtabelapreco(GenericBean materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}
	public TranslateEnumBean getExpedicaovendasituacao() {
		return expedicaovendasituacao;
	}
	public void setExpedicaovendasituacao(TranslateEnumBean expedicaovendasituacao) {
		this.expedicaovendasituacao = expedicaovendasituacao;
	}
	public GenericBean getVendaorcamento() {
		return vendaorcamento;
	}
	public void setVendaorcamento(GenericBean vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}
	public Boolean getOrigemwebservice() {
		return origemwebservice;
	}
	public void setOrigemwebservice(Boolean origemwebservice) {
		this.origemwebservice = origemwebservice;
	}
	public Money getValordescontorepresentacao() {
		return valordescontorepresentacao;
	}
	public void setValordescontorepresentacao(Money valordescontorepresentacao) {
		this.valordescontorepresentacao = valordescontorepresentacao;
	}
	public Money getValorbrutorepresentacao() {
		return valorbrutorepresentacao;
	}
	public void setValorbrutorepresentacao(Money valorbrutorepresentacao) {
		this.valorbrutorepresentacao = valorbrutorepresentacao;
	}
	public Boolean getCriacaofinalizada() {
		return criacaofinalizada;
	}
	public void setCriacaofinalizada(Boolean criacaofinalizada) {
		this.criacaofinalizada = criacaofinalizada;
	}
	public Boolean getAprovando() {
		return aprovando;
	}
	public void setAprovando(Boolean aprovando) {
		this.aprovando = aprovando;
	}
	public Boolean getLimitecreditoexcedido() {
		return limitecreditoexcedido;
	}
	public void setLimitecreditoexcedido(Boolean limitecreditoexcedido) {
		this.limitecreditoexcedido = limitecreditoexcedido;
	}
	public List<VendaMaterialWSBean> getListavendamaterial() {
		return listavendamaterial;
	}
	public void setListavendamaterial(List<VendaMaterialWSBean> listavendamaterial) {
		this.listavendamaterial = listavendamaterial;
	}
	public List<VendaPagamentoWSBean> getListavendapagamento() {
		return listavendapagamento;
	}
	public void setListavendapagamento(
			List<VendaPagamentoWSBean> listavendapagamento) {
		this.listavendapagamento = listavendapagamento;
	}
	public List<VendaHistoricoWSBean> getListavendahistorico() {
		return listavendahistorico;
	}
	public void setListavendahistorico(
			List<VendaHistoricoWSBean> listavendahistorico) {
		this.listavendahistorico = listavendahistorico;
	}
	public List<VendaMaterialMestreWSBean> getListaVendamaterialmestre() {
		return listaVendamaterialmestre;
	}
	public void setListaVendamaterialmestre(
			List<VendaMaterialMestreWSBean> listaVendamaterialmestre) {
		this.listaVendamaterialmestre = listaVendamaterialmestre;
	}
	public List<VendaValorCampoExtraWSBean> getListaVendavalorcampoextra() {
		return listaVendavalorcampoextra;
	}
	public void setListaVendavalorcampoextra(
			List<VendaValorCampoExtraWSBean> listaVendavalorcampoextra) {
		this.listaVendavalorcampoextra = listaVendavalorcampoextra;
	}
	public List<VendaPagamentoRepresentacaoWSBean> getListavendapagamentorepresentacao() {
		return listavendapagamentorepresentacao;
	}
	public void setListavendapagamentorepresentacao(
			List<VendaPagamentoRepresentacaoWSBean> listavendapagamentorepresentacao) {
		this.listavendapagamentorepresentacao = listavendapagamentorepresentacao;
	}
	public List<ProducaoAgendaItemAdicionalWSBean> getListaProducaoagendaitemadicional() {
		return listaProducaoagendaitemadicional;
	}
	public void setListaProducaoagendaitemadicional(
			List<ProducaoAgendaItemAdicionalWSBean> listaProducaoagendaitemadicional) {
		this.listaProducaoagendaitemadicional = listaProducaoagendaitemadicional;
	}
	public List<ProducaoOrdemItemAdicionalWSBean> getListaProducaoordemitemadicional() {
		return listaProducaoordemitemadicional;
	}
	public void setListaProducaoordemitemadicional(
			List<ProducaoOrdemItemAdicionalWSBean> listaProducaoordemitemadicional) {
		this.listaProducaoordemitemadicional = listaProducaoordemitemadicional;
	}
	public GenericBean getIndicecorrecao() {
		return indicecorrecao;
	}
	public void setIndicecorrecao(GenericBean indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}
	public GenericBean getContaboleto() {
		return contaboleto;
	}
	public void setContaboleto(GenericBean contaboleto) {
		this.contaboleto = contaboleto;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public Money getTaxapedidovenda() {
		return taxapedidovenda;
	}
	public void setTaxapedidovenda(Money taxapedidovenda) {
		this.taxapedidovenda = taxapedidovenda;
	}
	public GenericBean getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(GenericBean fornecedor) {
		this.fornecedor = fornecedor;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public GenericBean getFornecedorAgencia() {
		return fornecedorAgencia;
	}
	public void setFornecedorAgencia(GenericBean fornecedorAgencia) {
		this.fornecedorAgencia = fornecedorAgencia;
	}
	public Money getPercentualRepresentacao() {
		return percentualRepresentacao;
	}
	public void setPercentualRepresentacao(Money percentualRepresentacao) {
		this.percentualRepresentacao = percentualRepresentacao;
	}
	public GenericBean getCentrocusto() {
		return centrocusto;
	}
	public void setCentrocusto(GenericBean centrocusto) {
		this.centrocusto = centrocusto;
	}
	public Money getValorFreteCIF() {
		return valorFreteCIF;
	}
	public void setValorFreteCIF(Money valorFreteCIF) {
		this.valorFreteCIF = valorFreteCIF;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
}
