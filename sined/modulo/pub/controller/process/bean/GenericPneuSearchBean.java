package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.OtrPneuTipo;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.geral.bean.Pneuqualificacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pneureforma;
import br.com.linkcom.sined.util.bean.GenericBean;

public class GenericPneuSearchBean extends GenericBean{

	private Pneu pneu;
	private Pneumarca pneuMarca;
	private Pneumodelo pneuModelo;
	private Pneumedida pneuMedida;
	private Pneuqualificacao pneuQualificacao;
	private Pneureforma numeroReforma;
	private PneuSegmento pneuSegmento;
	private Material materialBanda;
	private Double profundidadeSulco;
	private String serie;
	private String dot;
	private Integer lonas;
	private OtrPneuTipo pneuTipo;
	private Integer currentPage;
	
	
	
	public Pneu getPneu() {
		return pneu;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	public Material getMaterialBanda() {
		return materialBanda;
	}
	public void setMaterialBanda(Material materialBanda) {
		this.materialBanda = materialBanda;
	}
	public Pneumarca getPneuMarca() {
		return pneuMarca;
	}
	public void setPneuMarca(Pneumarca pneuMarca) {
		this.pneuMarca = pneuMarca;
	}
	public Pneumodelo getPneuModelo() {
		return pneuModelo;
	}
	public void setPneuModelo(Pneumodelo pneuModelo) {
		this.pneuModelo = pneuModelo;
	}
	public Pneumedida getPneuMedida() {
		return pneuMedida;
	}
	public void setPneuMedida(Pneumedida pneuMedida) {
		this.pneuMedida = pneuMedida;
	}
	public Pneuqualificacao getPneuQualificacao() {
		return pneuQualificacao;
	}
	public void setPneuQualificacao(Pneuqualificacao pneuQualificacao) {
		this.pneuQualificacao = pneuQualificacao;
	}
	public Pneureforma getNumeroReforma() {
		return numeroReforma;
	}
	public void setNumeroReforma(Pneureforma numeroReforma) {
		this.numeroReforma = numeroReforma;
	}
	public PneuSegmento getPneuSegmento() {
		return pneuSegmento;
	}
	public void setPneuSegmento(PneuSegmento pneuSegmento) {
		this.pneuSegmento = pneuSegmento;
	}
	public Double getProfundidadeSulco() {
		return profundidadeSulco;
	}
	public void setProfundidadeSulco(Double profundidadeSulco) {
		this.profundidadeSulco = profundidadeSulco;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getDot() {
		return dot;
	}
	public void setDot(String dot) {
		this.dot = dot;
	}
	public Integer getLonas() {
		return lonas;
	}
	public OtrPneuTipo getPneuTipo() {
		return pneuTipo;
	}
	public void setLonas(Integer lonas) {
		this.lonas = lonas;
	}
	public void setPneuTipo(OtrPneuTipo pneuTipo) {
		this.pneuTipo = pneuTipo;
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
}
