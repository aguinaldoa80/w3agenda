package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class IntegracaoTefCallbackBean {

	private String cpfCnpj;
	private String intencaoVendaId;
	private String intencaoVendaReferencia;
	
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public String getIntencaoVendaId() {
		return intencaoVendaId;
	}
	public String getIntencaoVendaReferencia() {
		return intencaoVendaReferencia;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public void setIntencaoVendaId(String intencaoVendaId) {
		this.intencaoVendaId = intencaoVendaId;
	}
	public void setIntencaoVendaReferencia(String intencaoVendaReferencia) {
		this.intencaoVendaReferencia = intencaoVendaReferencia;
	}
	
}
