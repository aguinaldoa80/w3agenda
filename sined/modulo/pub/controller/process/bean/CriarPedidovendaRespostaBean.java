package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class CriarPedidovendaRespostaBean {
	
	private Integer cdpedidovenda;
	private Integer cdvenda;
	private Integer cdvendaorcamento;
	
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public Integer getCdvenda() {
		return cdvenda;
	}
	public Integer getCdvendaorcamento() {
		return cdvendaorcamento;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setCdvendaorcamento(Integer cdvendaorcamento) {
		this.cdvendaorcamento = cdvendaorcamento;
	}
	
}
