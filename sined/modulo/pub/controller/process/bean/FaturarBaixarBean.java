package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class FaturarBaixarBean extends GenericEnvioInformacaoBean{
	
	private Integer entrega;
	private Integer retorno;
	public Integer getEntrega() {
		return entrega;
	}
	public void setEntrega(Integer entrega) {
		this.entrega = entrega;
	}
	public Integer getRetorno() {
		return retorno;
	}
	public void setRetorno(Integer retorno) {
		this.retorno = retorno;
	}
	
	

}
