package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.util.List;

import br.com.linkcom.sined.geral.bean.auxiliar.TelefoneLeadBean;

public class CriarLeadBean extends GenericEnvioInformacaoBean{


	protected String contato;
	protected Integer cdresponsavel;
	protected Integer cdleadsituacao;
	protected Integer cdleadqualificacao;
	protected String empresa;
	protected String email;
	protected String observacao;
	protected List<TelefoneLeadBean> telefone;
	
	
	public String getContato() {
		return contato;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public Integer getCdresponsavel() {
		return cdresponsavel;
	}
	public void setCdresponsavel(Integer cdresponsavel) {
		this.cdresponsavel = cdresponsavel;
	}
	public Integer getCdleadsituacao() {
		return cdleadsituacao;
	}
	public void setCdleadsituacao(Integer cdleadsituacao) {
		this.cdleadsituacao = cdleadsituacao;
	}
	public Integer getCdleadqualificacao() {
		return cdleadqualificacao;
	}
	public void setCdleadqualificacao(Integer cdleadqualificacao) {
		this.cdleadqualificacao = cdleadqualificacao;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public List<TelefoneLeadBean> getTelefone() {
		return telefone;
	}
	public void setTelefone(List<TelefoneLeadBean> telefone) {
		this.telefone = telefone;
	}
}
