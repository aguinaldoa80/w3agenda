package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class VerificaVersaoBean extends GenericEnvioInformacaoBean {
	
	private Integer versao;
	
	public Integer getVersao() {
		return versao;
	}
	
	public void setVersao(Integer versao) {
		this.versao = versao;
	}

}
