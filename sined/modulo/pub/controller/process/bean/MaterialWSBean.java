package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.bean.GenericBean;

public class MaterialWSBean extends GenericBean{

	private GenericBean materialGrupo;
	private GenericBean materialTipo;
	private String identificacao;
	private String identificacaoOuCdMaterial;

	public MaterialWSBean(){
		
	}

	public MaterialWSBean(Material material){
		this.id = Util.strings.toStringIdStyled(material);
		this.value = material.getNome();
		this.identificacao = material.getIdentificacao();
		this.identificacaoOuCdMaterial = material.getIdentificacao() != null? material.getIdentificacao():
			material.getCdmaterial() != null? material.getCdmaterial().toString(): "";
		this.materialGrupo = ObjectUtils.translateEntityToGenericBean(material.getMaterialgrupo());
		this.materialTipo = ObjectUtils.translateEntityToGenericBean(material.getMaterialtipo());
	}
	
	public GenericBean getMaterialGrupo() {
		return materialGrupo;
	}
	public void setMaterialGrupo(GenericBean materialGrupo) {
		this.materialGrupo = materialGrupo;
	}
	public GenericBean getMaterialTipo() {
		return materialTipo;
	}
	public void setMaterialTipo(GenericBean materialTipo) {
		this.materialTipo = materialTipo;
	}
	public String getIdentificacao() {
		return identificacao;
	}
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	public String getIdentificacaoOuCdMaterial() {
		return identificacaoOuCdMaterial;
	}
	public void setIdentificacaoOuCdMaterial(String identificacaoOuCdMaterial) {
		this.identificacaoOuCdMaterial = identificacaoOuCdMaterial;
	}
}
