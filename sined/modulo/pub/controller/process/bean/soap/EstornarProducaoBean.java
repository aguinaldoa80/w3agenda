package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.util.List;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class EstornarProducaoBean extends GenericEnvioInformacaoBean {
	
	private String empresa;
	private String identificador;
	private List<EstornarProducaoItemBean> item;
	
	public String getIdentificador() {
		return identificador;
	}
	public List<EstornarProducaoItemBean> getItem() {
		return item;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setItem(List<EstornarProducaoItemBean> item) {
		this.item = item;
	}
	
}
