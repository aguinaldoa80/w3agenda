package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class ManterPedidovendaBean extends GenericEnvioInformacaoBean {
	
	private String empresa;
	private Integer localarmazenagem;
	private Integer cliente;
	private Integer vendedor;
	private Date dataPedido;
	private String identificador;
	private String identificadorexterno;
	private Integer tipoPedidoVenda;
	private Boolean emitirNotaColeta;
	private Date prazoEntrega;
	private Double desconto;
	private String observacao;
	private String observacaoInterna;
	private List<ManterPedidovendaItemBean> item;
	private List<ManterPedidovendaParcelaBean> parcela;
	
	public String getEmpresa() {
		return empresa;
	}
	public Integer getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Integer getCliente() {
		return cliente;
	}
	public Integer getVendedor() {
		return vendedor;
	}
	public Date getDataPedido() {
		return dataPedido;
	}
	public String getIdentificador() {
		return identificador;
	}
	public Boolean getEmitirNotaColeta() {
		return emitirNotaColeta;
	}
	public Date getPrazoEntrega() {
		return prazoEntrega;
	}
	public Double getDesconto() {
		return desconto;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getObservacaoInterna() {
		return observacaoInterna;
	}
	public List<ManterPedidovendaItemBean> getItem() {
		return item;
	}
	public List<ManterPedidovendaParcelaBean> getParcela() {
		return parcela;
	}
	public String getIdentificadorexterno() {
		return identificadorexterno;
	}
	public Integer getTipoPedidoVenda() {
		return tipoPedidoVenda;
	}
	public void setTipoPedidoVenda(Integer tipoPedidoVenda) {
		this.tipoPedidoVenda = tipoPedidoVenda;
	}
	public void setIdentificadorexterno(String identificadorexterno) {
		this.identificadorexterno = identificadorexterno;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setLocalarmazenagem(Integer localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}
	public void setVendedor(Integer vendedor) {
		this.vendedor = vendedor;
	}
	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setEmitirNotaColeta(Boolean emitirNotaColeta) {
		this.emitirNotaColeta = emitirNotaColeta;
	}
	public void setPrazoEntrega(Date prazoEntrega) {
		this.prazoEntrega = prazoEntrega;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setObservacaoInterna(String observacaoInterna) {
		this.observacaoInterna = observacaoInterna;
	}
	public void setItem(List<ManterPedidovendaItemBean> item) {
		this.item = item;
	}
	public void setParcela(List<ManterPedidovendaParcelaBean> parcela) {
		this.parcela = parcela;
	}
	
}
