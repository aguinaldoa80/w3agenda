package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.util.List;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class FaturarColetaBean extends GenericEnvioInformacaoBean {

	private String empresa;
	private List<FaturarColetaItemBean> item;
	
	public List<FaturarColetaItemBean> getItem() {
		return item;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setItem(List<FaturarColetaItemBean> item) {
		this.item = item;
	}
	
}
