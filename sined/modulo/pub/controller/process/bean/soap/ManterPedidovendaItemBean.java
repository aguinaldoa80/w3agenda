package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;


public class ManterPedidovendaItemBean {
	
	private Integer identificador;
	private Integer codigo;
	private Integer pneu;
	private Integer marca;
	private Integer modelo;
	private Integer medida;
	private String serie;
	private String dot;
	private Integer banda;
	private Double valor;
	private Double valorcoleta;
	private Double quantidade;
	private Double desconto;
	private String observacao;
	
	public Integer getIdentificador() {
		return identificador;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public Integer getPneu() {
		return pneu;
	}
	public Integer getMarca() {
		return marca;
	}
	public Integer getModelo() {
		return modelo;
	}
	public Integer getMedida() {
		return medida;
	}
	public Integer getBanda() {
		return banda;
	}
	public Double getValor() {
		return valor;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getDesconto() {
		return desconto;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getSerie() {
		return serie;
	}
	public String getDot() {
		return dot;
	}
	public Double getValorcoleta() {
		return valorcoleta;
	}
	public void setValorcoleta(Double valorcoleta) {
		this.valorcoleta = valorcoleta;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public void setDot(String dot) {
		this.dot = dot;
	}
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setPneu(Integer pneu) {
		this.pneu = pneu;
	}
	public void setMarca(Integer marca) {
		this.marca = marca;
	}
	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}
	public void setMedida(Integer medida) {
		this.medida = medida;
	}
	public void setBanda(Integer banda) {
		this.banda = banda;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
}
