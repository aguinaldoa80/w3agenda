package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import org.jdom.Element;

import br.com.linkcom.sined.util.SinedUtil;

public class CorreiosBean {
    private String usuario;
    private String senha;
    private String tipo;
    private String resultado;
    private String lingua;
    private String objetos;
    
    
	public String transformToElementString(){
		Element parent = new Element("objetoCorreios");
		
		String xml = "<usuario>"+this.usuario+"</usuario>"+
				"<senha>"+this.senha+"</senha>"+
				"<tipo>"+this.tipo+"</tipo>"+
				"<resultado>"+this.resultado+"</resultado>"+
				"<lingua>"+this.lingua+"</lingua>";
		for(String str: this.objetos.split(",")){
			xml += "<objetos>"+str+"</objetos>";
		}
		SinedUtil.addContentXML(parent, "usuario", this.usuario);
		SinedUtil.addContentXML(parent, "senha", this.senha);
		SinedUtil.addContentXML(parent, "tipo", this.tipo);
		SinedUtil.addContentXML(parent, "resultado", this.resultado);
		SinedUtil.addContentXML(parent, "lingua", this.lingua);
		SinedUtil.addContentXML(parent, "objetos", this.objetos);
		
		return xml;//parent.getContent().toString();
	}

	public CorreiosBean() {}
	
	public CorreiosBean(String usuario, String senha, String tipo, String resultado, String lingua, String objetos) {
		this.usuario = usuario;
		this.senha = senha;
		this.tipo = tipo;
		this.resultado = resultado;
		this.lingua = lingua;
		this.objetos = objetos;
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getLingua() {
		return lingua;
	}
	public void setLingua(String lingua) {
		this.lingua = lingua;
	}
	public String getObjetos() {
		return objetos;
	}
	public void setObjetos(String objetos) {
		this.objetos = objetos;
	}
}
