package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;


public class FaturarColetaItemBean {
	
	private String identificador;
	
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
}
