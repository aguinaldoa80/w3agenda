package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.sql.Timestamp;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class ConsultarClientesBean extends GenericEnvioInformacaoBean {
	
	private Timestamp dataReferencia;
	
	public Timestamp getDataReferencia() {
		return dataReferencia;
	}
	
	public void setDataReferencia(Timestamp dataReferencia) {
		this.dataReferencia = dataReferencia;
	}
	
}
