package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.util.SinedUtil;

public class ConsultarEstoqueWSBean {

	private Integer codigo;
	private String unidademedida;
	private Double quantidade;
	private Double custo;
	
	public Element transformToElement(){
		Element parent = new Element("estoque");
		
		SinedUtil.addContentXML(parent, "codigo", this.codigo);
		SinedUtil.addContentXML(parent, "unidademedida", this.unidademedida);
		SinedUtil.addContentXML(parent, "quantidade", this.quantidade != null ? this.quantidade.toString() : "0");
		SinedUtil.addContentXML(parent, "custo", this.custo != null ? this.custo.toString() : "0");
		
		return parent;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getUnidademedida() {
		return unidademedida;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public Double getCusto() {
		return custo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setCusto(Double custo) {
		this.custo = custo;
	}

}
