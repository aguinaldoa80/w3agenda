package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.util.SinedUtil;


public class ClienteEnderecoWSBean {

	private String tipo;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cep;
	private String municipio;
	private String uf;
	
	public Element transformToElement() {
		Element parent = new Element("endereco");
		
		SinedUtil.addContentXML(parent, "tipo", this.tipo);
		SinedUtil.addContentXML(parent, "logradouro", this.logradouro);
		SinedUtil.addContentXML(parent, "numero", this.numero);
		SinedUtil.addContentXML(parent, "complemento", this.complemento);
		SinedUtil.addContentXML(parent, "bairro", this.bairro);
		SinedUtil.addContentXML(parent, "cep", this.cep);
		SinedUtil.addContentXML(parent, "municipio", this.municipio);
		SinedUtil.addContentXML(parent, "uf", this.uf);
		
		return parent;
	}
	
	public ClienteEnderecoWSBean() {}
	
	public ClienteEnderecoWSBean(Endereco endereco) {
		this.tipo = endereco.getEnderecotipo() != null ? endereco.getEnderecotipo().getNome() : null;
		this.logradouro = endereco.getLogradouro();
		this.numero = endereco.getNumero();
		this.complemento = endereco.getComplemento();
		this.bairro = endereco.getBairro();
		this.cep = endereco.getCep() != null ? endereco.getCep().getValue() : null;
		this.municipio = endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : null;
		this.uf = endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : null;
	}
	
	public String getTipo() {
		return tipo;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		return cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getUf() {
		return uf;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	
}
