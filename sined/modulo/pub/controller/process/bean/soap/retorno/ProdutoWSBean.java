package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.SinedUtil;

public class ProdutoWSBean {

	private Integer codigo;
	private String nome;
	private String unidadeMedida;
	private String materialTipo;
	private String codigoFabricante;
	
	public Element transformToElement(){
		Element parent = new Element("produto");
		
		SinedUtil.addContentXML(parent, "codigo", this.codigo);
		SinedUtil.addContentXML(parent, "nome", this.nome);
		SinedUtil.addContentXML(parent, "unidadeMedida", this.unidadeMedida);
		SinedUtil.addContentXML(parent, "materialTipo", this.materialTipo);
		SinedUtil.addContentXML(parent, "codigoFabricante", this.codigoFabricante);
		
		return parent;
	}

	public ProdutoWSBean() {}
	
	public ProdutoWSBean(Material material) {
		this.codigo = material.getCdmaterial();
		this.nome = material.getNome();
		this.unidadeMedida = material.getUnidademedida().getSimbolo();
		this.materialTipo = material.getMaterialtipo()!=null ? material.getMaterialtipo().getNome() : null;
		this.codigoFabricante = material.getCodigofabricante()!=null && !material.getCodigofabricante().trim().equals("") ? material.getCodigofabricante() : null;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	public String getNome() {
		return nome;
	}
	public String getUnidadeMedida() {
		return unidadeMedida;
	}
	public String getMaterialTipo() {
		return materialTipo;
	}
	public String getCodigoFabricante() {
		return codigoFabricante;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public void setMaterialTipo(String materialTipo) {
		this.materialTipo = materialTipo;
	}
	public void setCodigoFabricante(String codigoFabricante) {
		this.codigoFabricante = codigoFabricante;
	}	
}