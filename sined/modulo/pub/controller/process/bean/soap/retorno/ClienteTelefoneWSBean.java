package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.util.SinedUtil;


public class ClienteTelefoneWSBean {

	private String tipo;
	private String telefone;
	
	public Element transformToElement() {
		Element parent = new Element("telefone");
		
		SinedUtil.addContentXML(parent, "tipo", this.tipo);
		SinedUtil.addContentXML(parent, "telefone", this.telefone);
		
		return parent;
	}
	
	public ClienteTelefoneWSBean() {}
	
	public ClienteTelefoneWSBean(Telefone telefone) {
		this.tipo = telefone.getTelefonetipo() != null ? telefone.getTelefonetipo().getNome() : null;
		this.telefone = telefone.getTelefone();
	}
	
	public String getTipo() {
		return tipo;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
