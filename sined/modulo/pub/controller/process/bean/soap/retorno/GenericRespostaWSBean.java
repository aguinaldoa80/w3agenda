package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Element;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

public class GenericRespostaWSBean {
	
	private List<Element> lista = new ArrayList<Element>();
	
	public void addElement(Element element){
		lista.add(element);
	}

	public String responseSucesso(){
		NeoWeb.getRequestContext().getServletResponse().setContentType("application/xml");
		return imprimeXMLSucesso();
	}
	
	public String responseErro(Exception exception){
		NeoWeb.getRequestContext().getServletResponse().setContentType("application/xml");
		return imprimeXMLErro(exception);
	}
	
	private String imprimeXMLSucesso() {
		try {
			Element resposta = new Element("resposta");
			
			Element status = new Element("status");
			status.setText("1");
			resposta.addContent(status);
			
			for (Element element : lista) {
				resposta.addContent(element);
			}
		
			return SinedUtil.getRespostaXML(resposta);
		} catch (Exception e) {
			e.printStackTrace();
			return this.imprimeXMLErro(new SinedException("Falha na conversao do BEAN para XML."));
		}
	}
	
	private String imprimeXMLErro(Exception exception) {
		try {
			Element resposta = new Element("resposta");
			
			Element status = new Element("status");
			status.setText("0");
			
			Element erro = new Element("erro");
			erro.setText(Util.strings.tiraAcento(exception.getMessage()));
			
			StringWriter stringWriter = new StringWriter();
			exception.printStackTrace(new PrintWriter(stringWriter));
			
			Element stacktrace = new Element("stacktrace");
			stacktrace.setText(stringWriter.toString());
			
			resposta.addContent(status);
			resposta.addContent(erro);
			resposta.addContent(stacktrace);
			
			return SinedUtil.getRespostaXML(resposta);
		} catch (Exception e) {
			e.printStackTrace();
			return "<resposta><status>0</status><erro>ERRO</erro></resposta>";
		}
	}

}
