package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import java.util.Date;

import org.jdom.Element;

import br.com.linkcom.sined.util.SinedUtil;

public class ConsultarSituacaoNotaWSBean {

	private String numeroNota;
	private String identificador;
	private Integer tipo;
	private Integer tipooperacao;
	private Integer situacaoNota;
	private Date dataEmissao;
	private Date dataCancelamento;
	
	public Element transformToElement(){
		Element parent = new Element("nota");
		
		SinedUtil.addContentXML(parent, "numeroNota", this.numeroNota);
		SinedUtil.addContentXML(parent, "identificador", this.identificador);
		SinedUtil.addContentXML(parent, "tipo", this.tipo);
		SinedUtil.addContentXML(parent, "tipooperacao", this.tipooperacao);
		SinedUtil.addContentXML(parent, "situacaoNota", this.situacaoNota);
		SinedUtil.addContentXML(parent, "dataEmissao", this.dataEmissao);
		SinedUtil.addContentXML(parent, "dataCancelamento", this.dataCancelamento);
		
		return parent;
	}

	public ConsultarSituacaoNotaWSBean() {}
	
	public ConsultarSituacaoNotaWSBean(String numeroNota, String identificador, Integer tipo, Integer tipooperacao,
			Integer situacaoNota, Date dataEmissao, Date dataCancelamento) {
		this.numeroNota = numeroNota;
		this.identificador = identificador;
		this.tipo = tipo;
		this.tipooperacao = tipooperacao;
		this.situacaoNota = situacaoNota;
		this.dataEmissao = dataEmissao;
		this.dataCancelamento = dataCancelamento;
	}

	public String getNumeroNota() {
		return numeroNota;
	}
	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public Integer getSituacaoNota() {
		return situacaoNota;
	}
	public void setSituacaoNota(Integer situacaoNota) {
		this.situacaoNota = situacaoNota;
	}
	public Date getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public Date getDataCancelamento() {
		return dataCancelamento;
	}
	public void setDataCancelamento(Date dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public Integer getTipooperacao() {
		return tipooperacao;
	}
	public void setTipooperacao(Integer tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	
}
