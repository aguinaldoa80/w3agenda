package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.ClienteDocumentoTipo;
import br.com.linkcom.sined.util.SinedUtil;


public class ClienteFormaPagamentoWSBean {

	private Integer codigo;
	
	public Element transformToElement() {
		Element parent = new Element("formapagamento");
		
		SinedUtil.addContentXML(parent, "codigo", this.codigo);
		
		return parent;
	}
	
	public ClienteFormaPagamentoWSBean() {}
	
	public ClienteFormaPagamentoWSBean(ClienteDocumentoTipo clienteDocumentoTipo) {
		this.codigo = clienteDocumentoTipo != null && clienteDocumentoTipo.getDocumentotipo() != null ? clienteDocumentoTipo.getDocumentotipo().getCddocumentotipo() : null;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
}
