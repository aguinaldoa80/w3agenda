package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.util.SinedUtil;

public class DocumentotipoWSBean {

	private Integer codigo;
	private String nome;
	
	public Element transformToElement(){
		Element parent = new Element("formapagamento");
		
		SinedUtil.addContentXML(parent, "codigo", this.codigo);
		SinedUtil.addContentXML(parent, "nome", this.nome);
		
		return parent;
	}

	public DocumentotipoWSBean() {}
	
	public DocumentotipoWSBean(Documentotipo documentotipo) {
		this.codigo = documentotipo.getCddocumentotipo();
		this.nome = documentotipo.getNome();
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
