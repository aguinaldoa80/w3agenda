package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteDocumentoTipo;
import br.com.linkcom.sined.geral.bean.ClientePrazoPagamento;
import br.com.linkcom.sined.geral.bean.Clientevendedor;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.util.SinedUtil;

public class ClienteWSBean {

	private Integer codigo;
	private String cnpj;
	private String cpf;
	private String nome;
	private Integer codigoVendedor;
	private List<ClienteEnderecoWSBean> endereco;
	private List<ClienteTelefoneWSBean> telefone;
	private List<ClientePrazoWSBean> prazo;
	private List<ClienteFormaPagamentoWSBean> formapagamento;
	
	public Element transformToElement(){
		Element parent = new Element("cliente");
		
		SinedUtil.addContentXML(parent, "codigo", this.codigo);
		SinedUtil.addContentXML(parent, "cnpj", this.cnpj);
		SinedUtil.addContentXML(parent, "cpf", this.cpf);
		SinedUtil.addContentXML(parent, "nome", this.nome);
		SinedUtil.addContentXML(parent, "vendedor", this.codigoVendedor);
		
		if(endereco != null && endereco.size() > 0){
			for (ClienteEnderecoWSBean it: endereco) {
				parent.addContent(it.transformToElement());
			}		
		}
		if(telefone != null && telefone.size() > 0){
			for (ClienteTelefoneWSBean it: telefone) {
				parent.addContent(it.transformToElement());
			}		
		}
		if(prazo != null && prazo.size() > 0){
			for (ClientePrazoWSBean it: prazo) {
				parent.addContent(it.transformToElement());
			}		
		}
		if(formapagamento != null && formapagamento.size() > 0){
			for (ClienteFormaPagamentoWSBean it: formapagamento) {
				parent.addContent(it.transformToElement());
			}		
		}
		
		return parent;
	}

	public ClienteWSBean() {}
	
	public ClienteWSBean(Cliente cliente) {
		this.codigo = cliente.getCdpessoa();
		this.cnpj = cliente.getCnpj() != null ? cliente.getCnpj().getValue() : null;
		this.cpf = cliente.getCpf() != null ? cliente.getCpf().getValue() : null;
		this.nome = cliente.getNome();
		
		Set<Endereco> listaEndereco = cliente.getListaEndereco();
		if(listaEndereco != null){
			this.endereco = new ArrayList<ClienteEnderecoWSBean>();
			for (Endereco endereco : listaEndereco) {
				this.endereco.add(new ClienteEnderecoWSBean(endereco));
			}
		}
		
		Set<Telefone> listaTelefone = cliente.getListaTelefone();
		if(listaTelefone != null){
			this.telefone = new ArrayList<ClienteTelefoneWSBean>();
			for (Telefone telefone : listaTelefone) {
				this.telefone.add(new ClienteTelefoneWSBean(telefone));
			}
		}
		
		List<ClienteDocumentoTipo> listDocumentoTipo = cliente.getListDocumentoTipo();
		if(listDocumentoTipo != null){
			this.formapagamento = new ArrayList<ClienteFormaPagamentoWSBean>();
			for (ClienteDocumentoTipo clienteDocumentoTipo : listDocumentoTipo) {
				this.formapagamento.add(new ClienteFormaPagamentoWSBean(clienteDocumentoTipo));
			}
		}
		
		List<ClientePrazoPagamento> listPrazoPagamento = cliente.getListPrazoPagamento();
		if(listPrazoPagamento != null){
			this.prazo = new ArrayList<ClientePrazoWSBean>();
			for (ClientePrazoPagamento clientePrazoPagamento : listPrazoPagamento) {
				this.prazo.add(new ClientePrazoWSBean(clientePrazoPagamento));
			}
		}
		
		if (cliente.getListaClientevendedor()!=null){
			for (Clientevendedor clientevendedor: cliente.getListaClientevendedor()){
				if (Boolean.TRUE.equals(clientevendedor.getPrincipal())){
					if (clientevendedor.getAgencia()!=null){
						codigoVendedor = clientevendedor.getAgencia().getCdpessoa(); 
					}else if (clientevendedor.getColaborador()!=null){
						codigoVendedor = clientevendedor.getColaborador().getCdpessoa(); 
					}
					break;
				}
			}
		}
	}
	
	
	
	public Integer getCodigo() {
		return codigo;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public String getNome() {
		return nome;
	}
	public List<ClienteEnderecoWSBean> getEndereco() {
		return endereco;
	}
	public List<ClienteTelefoneWSBean> getTelefone() {
		return telefone;
	}
	public List<ClientePrazoWSBean> getPrazo() {
		return prazo;
	}
	public List<ClienteFormaPagamentoWSBean> getFormapagamento() {
		return formapagamento;
	}
	public void setPrazo(List<ClientePrazoWSBean> prazo) {
		this.prazo = prazo;
	}
	public void setFormapagamento(
			List<ClienteFormaPagamentoWSBean> formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEndereco(List<ClienteEnderecoWSBean> endereco) {
		this.endereco = endereco;
	}
	public void setTelefone(List<ClienteTelefoneWSBean> telefone) {
		this.telefone = telefone;
	}
	
	
}
