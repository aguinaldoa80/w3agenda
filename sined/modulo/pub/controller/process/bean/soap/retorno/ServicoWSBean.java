package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.util.SinedUtil;

public class ServicoWSBean {

	private Integer codigo;
	private String nome;
	
	public Element transformToElement(){
		Element parent = new Element("servico");
		
		SinedUtil.addContentXML(parent, "codigo", this.codigo);
		SinedUtil.addContentXML(parent, "nome", this.nome);
		
		return parent;
	}

	public ServicoWSBean() {}
	
	public ServicoWSBean(Material material) {
		this.codigo = material.getCdmaterial();
		this.nome = material.getNome();
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
