package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.util.SinedUtil;

public class ColaboradorWSBean {

	private Integer codigo;
	private String cpf;
	private String nome;
	
	public Element transformToElement(){
		Element parent = new Element("colaborador");
		
		SinedUtil.addContentXML(parent, "codigo", this.codigo);
		SinedUtil.addContentXML(parent, "cpf", this.cpf);
		SinedUtil.addContentXML(parent, "nome", this.nome);
		
		return parent;
	}

	public ColaboradorWSBean() {}
	
	public ColaboradorWSBean(Colaborador colaborador) {
		this.codigo = colaborador.getCdpessoa();
		this.cpf = colaborador.getCpf() != null ? colaborador.getCpf().getValue() : null;
		this.nome = colaborador.getNome();
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	public String getCpf() {
		return cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
