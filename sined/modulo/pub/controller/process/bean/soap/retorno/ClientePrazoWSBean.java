package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno;

import org.jdom.Element;

import br.com.linkcom.sined.geral.bean.ClientePrazoPagamento;
import br.com.linkcom.sined.util.SinedUtil;


public class ClientePrazoWSBean {

	private Integer codigo;
	
	public Element transformToElement() {
		Element parent = new Element("prazo");
		
		SinedUtil.addContentXML(parent, "codigo", this.codigo);
		
		return parent;
	}
	
	public ClientePrazoWSBean() {}
	
	public ClientePrazoWSBean(ClientePrazoPagamento clientePrazoPagamento) {
		this.codigo = clientePrazoPagamento != null && clientePrazoPagamento.getPrazopagamento() != null ? clientePrazoPagamento.getPrazopagamento().getCdprazopagamento() : null;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
}
