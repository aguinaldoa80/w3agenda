package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;


public class ConfirmarPedidovendaItemBean {
	
	private Integer identificador;
	private Double quantidade;
	private Boolean recusado;
	
	public Integer getIdentificador() {
		return identificador;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Boolean getRecusado() {
		return recusado;
	}
	public void setRecusado(Boolean recusado) {
		this.recusado = recusado;
	}
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
}
