package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.sql.Timestamp;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class ConsultarVendedoresBean extends GenericEnvioInformacaoBean {
	
	private Timestamp dataReferencia;
	private Integer cargo;
	
	public Timestamp getDataReferencia() {
		return dataReferencia;
	}
	
	public Integer getCargo() {
		return cargo;
	}
	
	public void setCargo(Integer cargo) {
		this.cargo = cargo;
	}
	
	public void setDataReferencia(Timestamp dataReferencia) {
		this.dataReferencia = dataReferencia;
	}
	
}
