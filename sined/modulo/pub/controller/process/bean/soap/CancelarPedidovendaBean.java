package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class CancelarPedidovendaBean extends GenericEnvioInformacaoBean {
	
	private String empresa;
	private String identificador;
	
	public String getIdentificador() {
		return identificador;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
}
