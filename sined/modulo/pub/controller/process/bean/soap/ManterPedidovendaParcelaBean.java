package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.sql.Date;

public class ManterPedidovendaParcelaBean {
	
	private Date vencimento;
	private Double valor;
	private Integer tipo;
	
	public Date getVencimento() {
		return vencimento;
	}
	public Double getValor() {
		return valor;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	
}
