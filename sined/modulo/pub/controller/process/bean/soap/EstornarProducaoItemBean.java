package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;


public class EstornarProducaoItemBean {
	
	private Integer identificador;
	
	public Integer getIdentificador() {
		return identificador;
	}
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	
}
