package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class ConsultarResponsabilidadesBean extends GenericEnvioInformacaoBean {
	
	private String empresa;
	private Integer codigo;
	
	public String getEmpresa() {
		return empresa;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
}
