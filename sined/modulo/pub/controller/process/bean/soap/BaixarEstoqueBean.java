package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.sql.Date;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class BaixarEstoqueBean extends GenericEnvioInformacaoBean {
	
	private Date data;
	private String empresa;
	private Integer localarmazenagem;
	private Integer codigo;
	private String unidadeMedida;
	private Double quantidade;
	private Integer origem;
	
	public Date getData() {
		return data;
	}
	public String getEmpresa() {
		return empresa;
	}
	public Integer getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public String getUnidadeMedida() {
		return unidadeMedida;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Integer getOrigem() {
		return origem;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setLocalarmazenagem(Integer localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setOrigem(Integer origem) {
		this.origem = origem;
	}
	
}
