package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.util.List;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class ConfirmarPedidovendaBean extends GenericEnvioInformacaoBean {
	
	private String empresa;
	private String identificador;
	private Integer tipoPedidoVenda;
	private List<ConfirmarPedidovendaItemBean> item;
	
	public String getIdentificador() {
		return identificador;
	}
	public List<ConfirmarPedidovendaItemBean> getItem() {
		return item;
	}
	public String getEmpresa() {
		return empresa;
	}
	public Integer getTipoPedidoVenda() {
		return tipoPedidoVenda;
	}
	
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setTipoPedidoVenda(Integer tipoPedidoVenda) {
		this.tipoPedidoVenda = tipoPedidoVenda;
	}
	public void setItem(List<ConfirmarPedidovendaItemBean> item) {
		this.item = item;
	}
	
}
