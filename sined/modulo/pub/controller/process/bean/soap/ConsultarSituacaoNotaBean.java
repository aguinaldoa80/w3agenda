package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import java.sql.Timestamp;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class ConsultarSituacaoNotaBean extends GenericEnvioInformacaoBean {
	
	private String empresa;
	private Timestamp dataReferencia;
	
	public Timestamp getDataReferencia() {
		return dataReferencia;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setDataReferencia(Timestamp dataReferencia) {
		this.dataReferencia = dataReferencia;
	}
	
}
