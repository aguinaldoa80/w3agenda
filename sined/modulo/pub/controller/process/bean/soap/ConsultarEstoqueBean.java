package br.com.linkcom.sined.modulo.pub.controller.process.bean.soap;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class ConsultarEstoqueBean extends GenericEnvioInformacaoBean {
	
	private String empresa;
	private Integer localarmazenagem;
	private String produtos;
	
	public String getEmpresa() {
		return empresa;
	}
	public Integer getLocalarmazenagem() {
		return localarmazenagem;
	}
	public String getProdutos() {
		return produtos;
	}
	public void setProdutos(String produtos) {
		this.produtos = produtos;
	}
	public void setLocalarmazenagem(Integer localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
}
