package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaTipoWSBean extends GenericBean{

	private Boolean mostrarIconePopup;
	private Boolean tipoBonificacao;
	private Boolean tipoReserva;
	private String reservarApartir;
	private Boolean tipoComodato;
	private Boolean tipoGarantia;
	private Boolean obrigarTerceiro;
	private Boolean obrigarProjeto;
	private String projetoPrincipal;
	private Boolean obrigarPrazoEntrega;
	private Boolean obrigarEndereco;
	private TranslateEnumBean baixaEstoqueEnum;
	private Boolean obrigarLote;
	private Boolean tipoRepresentacao;
	private Boolean obrigarPresencaCompradorNfe;
	private TranslateEnumBean presencaCompradorNfe;
	private Boolean exibirEscolhaAgenciaVendas;
	private Boolean ipiConta;
	private Boolean considerarValorCusto;
	private Boolean permitirVendaSemEstoque;
	private Boolean bloquearQuantidade;
	
	
	
	public Boolean getMostrarIconePopup() {
		return mostrarIconePopup;
	}
	public void setMostrarIconePopup(Boolean mostrarIconePopup) {
		this.mostrarIconePopup = mostrarIconePopup;
	}
	public Boolean getTipoBonificacao() {
		return tipoBonificacao;
	}
	public void setTipoBonificacao(Boolean tipoBonificacao) {
		this.tipoBonificacao = tipoBonificacao;
	}
	public Boolean getTipoReserva() {
		return tipoReserva;
	}
	public void setTipoReserva(Boolean tipoReserva) {
		this.tipoReserva = tipoReserva;
	}
	public String getReservarApartir() {
		return reservarApartir;
	}
	public void setReservarApartir(String reservarApartir) {
		this.reservarApartir = reservarApartir;
	}
	public Boolean getTipoComodato() {
		return tipoComodato;
	}
	public void setTipoComodato(Boolean tipoComodato) {
		this.tipoComodato = tipoComodato;
	}
	public Boolean getTipoGarantia() {
		return tipoGarantia;
	}
	public void setTipoGarantia(Boolean tipoGarantia) {
		this.tipoGarantia = tipoGarantia;
	}
	public Boolean getObrigarTerceiro() {
		return obrigarTerceiro;
	}
	public void setObrigarTerceiro(Boolean obrigarTerceiro) {
		this.obrigarTerceiro = obrigarTerceiro;
	}
	public Boolean getObrigarProjeto() {
		return obrigarProjeto;
	}
	public void setObrigarProjeto(Boolean obrigarProjeto) {
		this.obrigarProjeto = obrigarProjeto;
	}
	public String getProjetoPrincipal() {
		return projetoPrincipal;
	}
	public void setProjetoPrincipal(String projetoPrincipal) {
		this.projetoPrincipal = projetoPrincipal;
	}
	public Boolean getObrigarPrazoEntrega() {
		return obrigarPrazoEntrega;
	}
	public void setObrigarPrazoEntrega(Boolean obrigarPrazoEntrega) {
		this.obrigarPrazoEntrega = obrigarPrazoEntrega;
	}
	public Boolean getObrigarEndereco() {
		return obrigarEndereco;
	}
	public void setObrigarEndereco(Boolean obrigarEndereco) {
		this.obrigarEndereco = obrigarEndereco;
	}
	public TranslateEnumBean getBaixaEstoqueEnum() {
		return baixaEstoqueEnum;
	}
	public void setBaixaEstoqueEnum(TranslateEnumBean baixaEstoqueEnum) {
		this.baixaEstoqueEnum = baixaEstoqueEnum;
	}
	public Boolean getObrigarLote() {
		return obrigarLote;
	}
	public void setObrigarLote(Boolean obrigarLote) {
		this.obrigarLote = obrigarLote;
	}
	public Boolean getTipoRepresentacao() {
		return tipoRepresentacao;
	}
	public void setTipoRepresentacao(Boolean tipoRepresentacao) {
		this.tipoRepresentacao = tipoRepresentacao;
	}
	public Boolean getObrigarPresencaCompradorNfe() {
		return obrigarPresencaCompradorNfe;
	}
	public void setObrigarPresencaCompradorNfe(Boolean obrigarPresencaCompradorNfe) {
		this.obrigarPresencaCompradorNfe = obrigarPresencaCompradorNfe;
	}
	public TranslateEnumBean getPresencaCompradorNfe() {
		return presencaCompradorNfe;
	}
	public void setPresencaCompradorNfe(TranslateEnumBean presencaCompradorNfe) {
		this.presencaCompradorNfe = presencaCompradorNfe;
	}
	public Boolean getExibirEscolhaAgenciaVendas() {
		return exibirEscolhaAgenciaVendas;
	}
	public void setExibirEscolhaAgenciaVendas(Boolean exibirEscolhaAgenciaVendas) {
		this.exibirEscolhaAgenciaVendas = exibirEscolhaAgenciaVendas;
	}
	public Boolean getIpiConta() {
		return ipiConta;
	}
	public void setIpiConta(Boolean ipiConta) {
		this.ipiConta = ipiConta;
	}
	public Boolean getConsiderarValorCusto() {
		return considerarValorCusto;
	}
	public void setConsiderarValorCusto(Boolean considerarValorCusto) {
		this.considerarValorCusto = considerarValorCusto;
	}
	public Boolean getPermitirVendaSemEstoque() {
		return permitirVendaSemEstoque;
	}
	public void setPermitirVendaSemEstoque(Boolean permitirVendaSemEstoque) {
		this.permitirVendaSemEstoque = permitirVendaSemEstoque;
	}
	public Boolean getBloquearQuantidade() {
		return bloquearQuantidade;
	}
	public void setBloquearQuantidade(Boolean bloquearQuantidade) {
		this.bloquearQuantidade = bloquearQuantidade;
	}
}
