package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaNegociacaoWSBean {

	private Integer cdPedidoVendaNegociacao;
	private GenericBean pedidovenda;
	private GenericBean documento;
	private GenericBean documentoTipo;
	private Integer banco;
	private Integer agencia;
	private Integer conta;
	private String numero;
	private Money valorOriginal;
	private Date dataParcela;
	private Money valorJuros;
	private GenericBean documentoAntecipacao;
	private String emitente;
	private String cpfcnpj;
	private GenericBean cheque;
	
	
	public Integer getCdPedidoVendaNegociacao() {
		return cdPedidoVendaNegociacao;
	}
	public void setCdPedidoVendaNegociacao(Integer cdPedidoVendaNegociacao) {
		this.cdPedidoVendaNegociacao = cdPedidoVendaNegociacao;
	}
	public GenericBean getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(GenericBean pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public GenericBean getDocumento() {
		return documento;
	}
	public void setDocumento(GenericBean documento) {
		this.documento = documento;
	}
	public GenericBean getDocumentoTipo() {
		return documentoTipo;
	}
	public void setDocumentoTipo(GenericBean documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	public Integer getBanco() {
		return banco;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public Integer getAgencia() {
		return agencia;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public Integer getConta() {
		return conta;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Money getValorOriginal() {
		return valorOriginal;
	}
	public void setValorOriginal(Money valorOriginal) {
		this.valorOriginal = valorOriginal;
	}
	public Date getDataParcela() {
		return dataParcela;
	}
	public void setDataParcela(Date dataParcela) {
		this.dataParcela = dataParcela;
	}
	public Money getValorJuros() {
		return valorJuros;
	}
	public void setValorJuros(Money valorJuros) {
		this.valorJuros = valorJuros;
	}
	public GenericBean getDocumentoAntecipacao() {
		return documentoAntecipacao;
	}
	public void setDocumentoAntecipacao(GenericBean documentoAntecipacao) {
		this.documentoAntecipacao = documentoAntecipacao;
	}
	public String getEmitente() {
		return emitente;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	public String getCpfcnpj() {
		return cpfcnpj;
	}
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
	public GenericBean getCheque() {
		return cheque;
	}
	public void setCheque(GenericBean cheque) {
		this.cheque = cheque;
	}
}
