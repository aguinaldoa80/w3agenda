package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;



public class ConsultaPagamentoClienteServicoBean extends GenericEnvioInformacaoBean {
	
	private Integer cdcliente;
	private Integer cdmaterial;
	private Date dtinicio;
	private Date dtfim;
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
}
