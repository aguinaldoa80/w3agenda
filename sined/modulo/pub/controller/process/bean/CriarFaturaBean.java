package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.Money;

public class CriarFaturaBean extends GenericEnvioInformacaoBean {
	
	private Integer cdendereco;
	private Integer cdcliente;
	private Integer cdempresa;
	private Integer cdconta;
	private Integer cddocumentotipo;
	private Integer cdcentrocusto;
	private Integer cdcontagerencial;
	private Integer cdprojeto;
	private String descricao;
	private Money valor;
	private Date dtvencimento;
	private String mensagem1;
	private String mensagem2;
	private String mensagem3;
	private Boolean criarnota = Boolean.TRUE;
	private Integer cdgrupotributacao;
	private Integer situacaonota;
	private List<RateioitemBean> rateioitem;
	
	public Integer getCdcliente() {
		return cdcliente;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public Integer getCdconta() {
		return cdconta;
	}
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	public String getDescricao() {
		return descricao;
	}
	public Money getValor() {
		return valor;
	}
	public String getMensagem1() {
		return mensagem1;
	}
	public String getMensagem2() {
		return mensagem2;
	}
	public String getMensagem3() {
		return mensagem3;
	}
	public Boolean getCriarnota() {
		return criarnota;
	}
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public Integer getCdgrupotributacao() {
		return cdgrupotributacao;
	}
	public Integer getSituacaonota() {
		return situacaonota;
	}
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public List<RateioitemBean> getRateioitem() {
		return rateioitem;
	}
	public Integer getCdendereco() {
		return cdendereco;
	}
	public void setCdendereco(Integer cdendereco) {
		this.cdendereco = cdendereco;
	}
	public void setRateioitem(List<RateioitemBean> rateioitem) {
		this.rateioitem = rateioitem;
	}
	
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setSituacaonota(Integer situacaonota) {
		this.situacaonota = situacaonota;
	}
	public void setCdgrupotributacao(Integer cdgrupotributacao) {
		this.cdgrupotributacao = cdgrupotributacao;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	public void setCriarnota(Boolean criarnota) {
		this.criarnota = criarnota;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setMensagem1(String mensagem1) {
		this.mensagem1 = mensagem1;
	}
	public void setMensagem2(String mensagem2) {
		this.mensagem2 = mensagem2;
	}
	public void setMensagem3(String mensagem3) {
		this.mensagem3 = mensagem3;
	}
	
}
