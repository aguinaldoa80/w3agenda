package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class CriarPedidovendaItemBean {
	
	private String material_identificador;
	private String material_cdmaterial;
	private Double valor_unitario;
	private Double quantidade;
	private Money desconto;
	private Date prazo_entrega;
	private String observacao;
	
	public String getMaterial_identificador() {
		return material_identificador;
	}
	public String getMaterial_cdmaterial() {
		return material_cdmaterial;
	}
	public Double getValor_unitario() {
		return valor_unitario;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Money getDesconto() {
		return desconto;
	}
	public Date getPrazo_entrega() {
		return prazo_entrega;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setMaterial_identificador(String materialIdentificador) {
		material_identificador = materialIdentificador;
	}
	public void setMaterial_cdmaterial(String material_cdmaterial) {
		this.material_cdmaterial = material_cdmaterial;
	}
	public void setValor_unitario(Double valorUnitario) {
		valor_unitario = valorUnitario;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public void setPrazo_entrega(Date prazoEntrega) {
		prazo_entrega = prazoEntrega;
	}
	
}
