package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.types.Money;

public class CriarPagamentoBean extends GenericEnvioInformacaoBean {
	
	private Integer cdcliente;
	private Integer cdfornecedor;
	private Integer cdcolaborador;
	private Integer cdempresa;
	private Integer cddocumentotipo;
	private Integer cdcentrocusto;
	private Integer cdcontagerencial;
	private Integer cdprojeto;
	private String descricao;
	private Money valor;
	
	public Integer getCdcliente() {
		return cdcliente;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	public String getDescricao() {
		return descricao;
	}
	public Money getValor() {
		return valor;
	}
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public Integer getCdfornecedor() {
		return cdfornecedor;
	}
	public Integer getCdcolaborador() {
		return cdcolaborador;
	}
	public void setCdfornecedor(Integer cdfornecedor) {
		this.cdfornecedor = cdfornecedor;
	}
	public void setCdcolaborador(Integer cdcolaborador) {
		this.cdcolaborador = cdcolaborador;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
}
