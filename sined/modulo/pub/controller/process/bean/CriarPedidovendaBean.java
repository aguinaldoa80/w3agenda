package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;

public class CriarPedidovendaBean extends GenericEnvioInformacaoBean {
	
	private Integer tipo_criacao;
	private String empresa_cnpj;
	private String cliente_cpfcnpj;
	private Long colaborador_matricula;
	private String vendedor_cpf;
	
	private Integer frete_tipo;
	private Money frete_valor;
	
	private Date pedidovenda_data;
	private String pedidovenda_identificador;
	private Money pedidovenda_desconto;	
	private String pedidovenda_observacao;
	private String pedidovenda_observacaointerna;

	private Integer cdcontrato;
	private List<CriarPedidovendaItemBean> pedidovenda_item = new ListSet<CriarPedidovendaItemBean>(CriarPedidovendaItemBean.class);
	private List<CriarPedidovendaPagamentoBean> pedidovenda_pagamento = new ListSet<CriarPedidovendaPagamentoBean>(CriarPedidovendaPagamentoBean.class);

	
	/** CAMPOS N�O DISPONIBILIZADOS NO WEBSERVICE **/
	private Integer cliente_cdendereco;

	
	
	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}

	public String getCliente_cpfcnpj() {
		return cliente_cpfcnpj;
	}

	public Integer getFrete_tipo() {
		return frete_tipo;
	}

	public Money getFrete_valor() {
		return frete_valor;
	}

	public Date getPedidovenda_data() {
		return pedidovenda_data;
	}

	public String getPedidovenda_identificador() {
		return pedidovenda_identificador;
	}

	public Money getPedidovenda_desconto() {
		return pedidovenda_desconto;
	}

	public String getPedidovenda_observacao() {
		return pedidovenda_observacao;
	}

	public String getPedidovenda_observacaointerna() {
		return pedidovenda_observacaointerna;
	}


	public List<CriarPedidovendaItemBean> getPedidovenda_item() {
		return pedidovenda_item;
	}
	
	public List<CriarPedidovendaPagamentoBean> getPedidovenda_pagamento() {
		return pedidovenda_pagamento;
	}
	
	public Integer getTipo_criacao() {
		return tipo_criacao;
	}
	
	public Long getColaborador_matricula() {
		return colaborador_matricula;
	}
	
	public String getVendedor_cpf() {
		return vendedor_cpf;
	}
	
	public Integer getCliente_cdendereco() {
		return cliente_cdendereco;
	}
	
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	
	public void setCliente_cdendereco(Integer clienteCdendereco) {
		cliente_cdendereco = clienteCdendereco;
	}
	
	public void setColaborador_matricula(Long colaboradorMatricula) {
		colaborador_matricula = colaboradorMatricula;
	}
	
	public void setVendedor_cpf(String vendedor_cpf) {
		this.vendedor_cpf = vendedor_cpf;
	}
	
	public void setTipo_criacao(Integer tipoCriacao) {
		tipo_criacao = tipoCriacao;
	}
	
	public void setPedidovenda_pagamento(
			List<CriarPedidovendaPagamentoBean> pedidovendaPagamento) {
		pedidovenda_pagamento = pedidovendaPagamento;
	}

	public void setEmpresa_cnpj(String empresaCnpj) {
		empresa_cnpj = empresaCnpj;
	}

	public void setCliente_cpfcnpj(String clienteCpfcnpj) {
		cliente_cpfcnpj = clienteCpfcnpj;
	}

	public void setFrete_tipo(Integer freteTipo) {
		frete_tipo = freteTipo;
	}

	public void setFrete_valor(Money freteValor) {
		frete_valor = freteValor;
	}

	public void setPedidovenda_data(Date pedidovendaData) {
		pedidovenda_data = pedidovendaData;
	}

	public void setPedidovenda_identificador(String pedidovendaIdentificador) {
		pedidovenda_identificador = pedidovendaIdentificador;
	}

	public void setPedidovenda_desconto(Money pedidovendaDesconto) {
		pedidovenda_desconto = pedidovendaDesconto;
	}

	public void setPedidovenda_observacao(String pedidovendaObservacao) {
		pedidovenda_observacao = pedidovendaObservacao;
	}

	public void setPedidovenda_observacaointerna(String pedidovendaObservacaointerna) {
		pedidovenda_observacaointerna = pedidovendaObservacaointerna;
	}

	public void setPedidovenda_item(List<CriarPedidovendaItemBean> pedidovendaItem) {
		pedidovenda_item = pedidovendaItem;
	}

}
