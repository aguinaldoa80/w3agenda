package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class PagamentoClienteServicoBean {
	
	private String tipo;
	private Integer cdcontrato;
	private Integer cdvenda;
	private Integer cdcliente;
	private Integer cdfrequencia;
	private Integer cdmaterial;
	private Integer quantidade;
	private Integer cddocumento;
	private Date dtvencimento;
	private Integer cddocumentoacao;
	private Money valordocumento;
	private Date dtpagamento;
	private Money valormovimentacao;
	
	public PagamentoClienteServicoBean() {
	}
	
	public PagamentoClienteServicoBean(String tipo, Integer cdcontrato,
			Integer cdvenda, Integer cdcliente, Integer cdfrequencia, Integer cdmaterial,
			Integer quantidade, Integer cddocumento, Date dtvencimento,
			Integer cddocumentoacao, Money valordocumento, Date dtpagamento,
			Money valormovimentacao) {
		this.tipo = tipo;
		this.cdcontrato = cdcontrato;
		this.cdvenda = cdvenda;
		this.cdcliente = cdcliente;
		this.cdfrequencia = cdfrequencia ;
		this.cdmaterial = cdmaterial;
		this.quantidade = quantidade;
		this.cddocumento = cddocumento;
		this.dtvencimento = dtvencimento;
		this.cddocumentoacao = cddocumentoacao;
		this.valordocumento = valordocumento;
		this.dtpagamento = dtpagamento;
		this.valormovimentacao = valormovimentacao;
	}
	
	public String getTipo() {
		return tipo;
	}
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	public Integer getCdvenda() {
		return cdvenda;
	}
	public Integer getCdfrequencia() {
		return cdfrequencia;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public Integer getCddocumento() {
		return cddocumento;
	}
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public Integer getCddocumentoacao() {
		return cddocumentoacao;
	}
	public Money getValordocumento() {
		return valordocumento;
	}
	public Date getDtpagamento() {
		return dtpagamento;
	}
	public Money getValormovimentacao() {
		return valormovimentacao;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setCdfrequencia(Integer cdfrequencia) {
		this.cdfrequencia = cdfrequencia;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setCddocumentoacao(Integer cddocumentoacao) {
		this.cddocumentoacao = cddocumentoacao;
	}
	public void setValordocumento(Money valordocumento) {
		this.valordocumento = valordocumento;
	}
	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}
	public void setValormovimentacao(Money valormovimentacao) {
		this.valormovimentacao = valormovimentacao;
	}
	
}
