package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class VerificaPendenciaFinanceira extends GenericEnvioInformacaoBean {
	
	private String cnpj;
	private String cpf;
	
	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}	
}
