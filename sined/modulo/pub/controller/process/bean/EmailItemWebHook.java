package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class EmailItemWebHook {

	private Integer cdemailitem;
	private String event;
	private Long ultimaverificacao;	
	private String response;
	private Integer attempt;
	private String urlclicked;
	private String status;
	private String reason;
	private String type;
	private String ip;
	private String useragent;
	private String hash;
	
	
	public Integer getCdemailitem() {
		return cdemailitem;
	}
	public void setCdemailitem(Integer cdemailitem) {
		this.cdemailitem = cdemailitem;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public Long getUltimaverificacao() {
		return ultimaverificacao;
	}
	public void setUltimaverificacao(Long ultimaverificacao) {
		this.ultimaverificacao = ultimaverificacao;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public Integer getAttempt() {
		return attempt;
	}
	public void setAttempt(Integer attempt) {
		this.attempt = attempt;
	}
	public String getUrlclicked() {
		return urlclicked;
	}
	public void setUrlclicked(String urlclicked) {
		this.urlclicked = urlclicked;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getUseragent() {
		return useragent;
	}
	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
}
