package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;


public class EnviarInutilizacaonfeBean extends GenericEnvioInformacaoBean {
	
	private Integer cdinutilizacaonfe;
	private Arquivo arquivo;
	
	public Integer getCdinutilizacaonfe() {
		return cdinutilizacaonfe;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setCdinutilizacaonfe(Integer cdinutilizacaonfe) {
		this.cdinutilizacaonfe = cdinutilizacaonfe;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
}
