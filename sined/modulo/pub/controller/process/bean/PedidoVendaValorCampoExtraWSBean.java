package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaValorCampoExtraWSBean {

	protected Integer cdpedidovendavalorcampoextra;
	protected GenericBean pedidovenda;
	protected GenericBean campoextrapedidovendatipo;
	protected String valor;
	
	
	public Integer getCdpedidovendavalorcampoextra() {
		return cdpedidovendavalorcampoextra;
	}
	public void setCdpedidovendavalorcampoextra(Integer cdpedidovendavalorcampoextra) {
		this.cdpedidovendavalorcampoextra = cdpedidovendavalorcampoextra;
	}
	public GenericBean getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(GenericBean pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public GenericBean getCampoextrapedidovendatipo() {
		return campoextrapedidovendatipo;
	}
	public void setCampoextrapedidovendatipo(GenericBean campoextrapedidovendatipo) {
		this.campoextrapedidovendatipo = campoextrapedidovendatipo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}
