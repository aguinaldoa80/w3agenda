package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class ExisteClienteBean extends GenericEnvioInformacaoBean {

	private String cpf_cnpj;
	
	public String getCpf_cnpj() {
		return cpf_cnpj;
	}
	
	public void setCpf_cnpj(String cpf_cnpj) {
		this.cpf_cnpj = cpf_cnpj;
	}
	
}
