package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.bean.GenericBean;

public class PneuWSBean extends GenericBean{

	protected Integer codigo;
	protected GenericBean pneuMarca;
	protected GenericBean pneuMedida;
	protected GenericBean pneuModelo;
	protected String serie;
	protected String dot;
	protected String descricao;
	protected GenericBean materialBanda;
	protected GenericBean numeroReforma;
	protected GenericBean pneuQualificacao;
	protected GenericBean otrPneuTipo;
	protected Integer lonas; 
	protected Boolean acompanhaRoda;
	protected Double profundidadeSulco;
	protected PneuSegmentoWSBean pneuSegmento;
	
	public PneuWSBean(Pneu pneu){
		this.id = Util.objects.isPersistent(pneu)? Util.strings.toStringIdStyled(pneu): null;
		this.value = Util.strings.toStringDescription(pneu);
		this.codigo = pneu.getCdpneu();
		this.pneuMarca = ObjectUtils.translateEntityToGenericBean(pneu.getPneumarca());
		this.pneuMedida = ObjectUtils.translateEntityToGenericBean(pneu.getPneumedida());
		this.pneuModelo = ObjectUtils.translateEntityToGenericBean(pneu.getPneumodelo());
		this.pneuQualificacao = ObjectUtils.translateEntityToGenericBean(pneu.getPneuqualificacao());
		this.numeroReforma = ObjectUtils.translateEntityToGenericBean(pneu.getPneuqualificacao());
		this.materialBanda = ObjectUtils.translateEntityToGenericBean(pneu.getMaterialbanda());
		this.otrPneuTipo = ObjectUtils.translateEntityToGenericBean(pneu.getOtrPneuTipo());
		this.lonas = pneu.getLonas();
		this.profundidadeSulco = pneu.getProfundidadesulco();
		this.dot = pneu.getDot();
		this.serie = pneu.getSerie();
//		this.descricao = pneu.getDescricao();
//		this.acompanhaRoda = pneu.getAcompanhaRoda();
		this.pneuSegmento = pneu.getPneuSegmento() != null ? new PneuSegmentoWSBean(pneu.getPneuSegmento()) : null; 
	}
	
	public PneuWSBean(){
		
	}
	
	
	public GenericBean getPneuMarca() {
		return pneuMarca;
	}
	public void setPneuMarca(GenericBean pneuMarca) {
		this.pneuMarca = pneuMarca;
	}
	public GenericBean getPneuMedida() {
		return pneuMedida;
	}
	public void setPneuMedida(GenericBean pneuMedida) {
		this.pneuMedida = pneuMedida;
	}
	public GenericBean getPneuModelo() {
		return pneuModelo;
	}
	public void setPneuModelo(GenericBean pneuModelo) {
		this.pneuModelo = pneuModelo;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getDot() {
		return dot;
	}
	public void setDot(String dot) {
		this.dot = dot;
	}
	public GenericBean getMaterialBanda() {
		return materialBanda;
	}
	public void setMaterialBanda(GenericBean materialBanda) {
		this.materialBanda = materialBanda;
	}
	public GenericBean getNumeroReforma() {
		return numeroReforma;
	}
	public void setNumeroReforma(GenericBean numeroReforma) {
		this.numeroReforma = numeroReforma;
	}
	public GenericBean getPneuQualificacao() {
		return pneuQualificacao;
	}
	public void setPneuQualificacao(GenericBean pneuQualificacao) {
		this.pneuQualificacao = pneuQualificacao;
	}
	public Double getProfundidadeSulco() {
		return profundidadeSulco;
	}
	public void setProfundidadeSulco(Double profundidadeSulco) {
		this.profundidadeSulco = profundidadeSulco;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public PneuSegmentoWSBean getPneuSegmento() {
		return pneuSegmento;
	}
	public void setPneuSegmento(PneuSegmentoWSBean pneuSegmento) {
		this.pneuSegmento = pneuSegmento;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Boolean getAcompanhaRoda() {
		return acompanhaRoda;
	}
	public void setAcompanhaRoda(Boolean acompanhaRoda) {
		this.acompanhaRoda = acompanhaRoda;
	}
	
}
