package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class TranslateEnumBean {

	private String value;
	private int ordinal;
	private String description;
	
	public TranslateEnumBean(){
		
	}
	
	public TranslateEnumBean(String value, int ordinal, String description){
		this.value = value;
		this.ordinal = ordinal;
		this.description = description;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getOrdinal() {
		return ordinal;
	}
	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
