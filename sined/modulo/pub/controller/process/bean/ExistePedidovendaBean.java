package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class ExistePedidovendaBean extends GenericEnvioInformacaoBean {
	
	private Integer tipo_criacao;
	private String empresa_cnpj;
	private String identificador;
	private Boolean verificarCancelado = false; 


	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}

	public String getIdentificador() {
		return identificador;
	}
	
	public Integer getTipo_criacao() {
		return tipo_criacao;
	}
	
	public Boolean getVerificarCancelado() {
		return verificarCancelado;
	}
	
	public void setVerificarCancelado(Boolean verificarCancelado) {
		this.verificarCancelado = verificarCancelado;
	}
	
	public void setTipo_criacao(Integer tipoCriacao) {
		tipo_criacao = tipoCriacao;
	}

	public void setEmpresa_cnpj(String empresaCnpj) {
		empresa_cnpj = empresaCnpj;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

}
