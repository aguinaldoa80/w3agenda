package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class MarcarArquivonfBean extends GenericEnvioInformacaoBean {
	
	private Integer cdarquivonf;
	private String flag;
	
	public Integer getCdarquivonf() {
		return cdarquivonf;
	}
	public String getFlag() {
		return flag;
	}
	public void setCdarquivonf(Integer cdarquivonf) {
		this.cdarquivonf = cdarquivonf;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}
