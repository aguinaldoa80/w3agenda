package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.util.bean.GenericBean;


public class PedidoVendaParametroBean {

	private String mensagemMotivoAguardandoAprovacao;
	private Boolean ignoreHackAndroidDownload;
	private Boolean closeOnSave;
	private Boolean emitirComprovante;
	private Boolean showEditarLink;
	private Boolean showCancelLink;
	private Boolean negociarPendencia;
	private Boolean coletaautomatica;
	private Boolean producaoautomatica;
	private Boolean permissaoProjeto;
	private Boolean exibirValorMinMaxVenda;
	private Boolean parametroRecapagem;
	private String pedidoVendaBuscaIdentificadorTabelaPreco;
	private Boolean confirmarPercentualPedidoVenda;
	private Boolean confirmarPvServicoProduto;
	private Boolean replicarMaterial;
	private String wmsIntegracao;
	private Boolean isMaterialmestregrade;
	private Boolean existGrade;
	private int geracaoContaReceberEnum;
	private Boolean bonificacaoPedidovenda;
	private Boolean reservaPedidovenda;
	private Boolean comodatoPedidovenda;
	private Boolean desfazerReserva;
	private Boolean formaPagamentoAntecipacao;
	private Boolean obrigarLote;
	private Boolean haveVinculoColetaProducao;
	private Boolean existDocumento;
	private Boolean existDocumentoRepresentacao;
	private Boolean vendaSaldoPorMinMax;
	private Boolean vendaSaldoProduto;
	private Boolean vendaSaldoProdutoNegativo;
	private Boolean desconsiderarDescontoSaldoProduto;
	private Boolean bloquearVendaReservada;
	private String menagemVendaSaldoProduto;
	private Boolean bloquearVendaCheque;
	private Boolean clientePossuiChequePendente;
	private Boolean localVendaObrigatorio;
	private String condicaoPagamentoInclusaoProduto;
	private Boolean obrigarTipoPedidoVenda;
	private Boolean alterarQtdeItemProducaoVenda;
	private Boolean definirLoteProduto;
	private Boolean includeAtalho;
	private String uriAtalhoPersonalizado;
	private Boolean venderAbaixoMinimo;
	private Boolean utilizarArredondamentoDesconto;
	private String casasDecimaisArredondamento;
	private Boolean bloqueaEdicaoQtde;
	private Boolean haveTabelaImposto;
	private Boolean exibirIpiNaVenda;
	private Boolean exibirReplicarPneu;
	private Boolean validadeExpiradaLimiteCredito;
	private Boolean mostrarLimiteCredito;
	private Boolean obrigarEnderecoCliente;
	private String qtdeDiaUtilPrazoEntrega;
	private Boolean bloquearLimiteDesconto;
	private Boolean exibirOrdemPedidoVenda;
	private Boolean exibirNomeAlternativoAlturaLargura;
	private Boolean copiarNomeAlternativoParaObsMaterial;
	private Boolean identificadorExterno;
	private Boolean historicoDeCliente;
	private Boolean bloqueioDimensoesVenda;
	private Boolean considerarFreteCif;
	private Boolean pedidoVendaAprovado;
	private Boolean clientePossuiContaAtrasada;
	private Boolean bloquearVendaDevedor;
	private Boolean realizarPedidoVenda;
	private Boolean chamadaImpressaoPedidoVenda;
	private Boolean copiarPedidoVenda;
	private Boolean exibirClassificacaoCliente;
	private Boolean considerarPedidoLimiteCredito;
	private Boolean pedidoVendaMaterial;
	private Boolean pedidoBuscaIdentificadorTabelaPreco;
	private Boolean baixaExpedicao;
	private Boolean permitirAlterarVenda;
	private String utilizacaoValeCompra;
	private Boolean mostrarPopupGrade;
	private GenericBean enderecoEscolhido;
	private Boolean aprovarOrcamento;
	private Boolean realizarVenda;
	private Boolean existeTrocaBanda;
	private Boolean existeTrocaServišo;
	private Boolean existeMovimentacaoEstoque;
	private Boolean gerarNotaAutomatico;
	private Boolean bloqAlteracaoPreco;;
	private Boolean exibirComprimentoOriginal;
	private Boolean showTaxaPedidoVenda;
	private Boolean bloqueioLarguraAlturaVenda;
	private Boolean vendaGerarProducao;
	private String dimensaoVenda1;
	private String dimensaoVenda2;
	private String dimensaoVenda3;
	private Boolean obrigarCamposPneu;

	
	public Boolean getObrigarCamposPneu() {
		return obrigarCamposPneu;
	}
	public void setObrigarCamposPneu(Boolean obrigarCamposPneu) {
		this.obrigarCamposPneu = obrigarCamposPneu;
	}
	public String getMensagemMotivoAguardandoAprovacao() {
		return mensagemMotivoAguardandoAprovacao;
	}
	public void setMensagemMotivoAguardandoAprovacao(
			String mensagemMotivoAguardandoAprovacao) {
		this.mensagemMotivoAguardandoAprovacao = mensagemMotivoAguardandoAprovacao;
	}
	public Boolean getIgnoreHackAndroidDownload() {
		return ignoreHackAndroidDownload;
	}
	public void setIgnoreHackAndroidDownload(Boolean ignoreHackAndroidDownload) {
		this.ignoreHackAndroidDownload = ignoreHackAndroidDownload;
	}
	public Boolean getCloseOnSave() {
		return closeOnSave;
	}
	public void setCloseOnSave(Boolean closeOnSave) {
		this.closeOnSave = closeOnSave;
	}
	public Boolean getEmitirComprovante() {
		return emitirComprovante;
	}
	public void setEmitirComprovante(Boolean emitirComprovante) {
		this.emitirComprovante = emitirComprovante;
	}
	public Boolean getShowEditarLink() {
		return showEditarLink;
	}
	public void setShowEditarLink(Boolean showEditarLink) {
		this.showEditarLink = showEditarLink;
	}
	public Boolean getShowCancelLink() {
		return showCancelLink;
	}
	public void setShowCancelLink(Boolean showCancelLink) {
		this.showCancelLink = showCancelLink;
	}
	public Boolean getNegociarPendencia() {
		return negociarPendencia;
	}
	public void setNegociarPendencia(Boolean negociarPendencia) {
		this.negociarPendencia = negociarPendencia;
	}
	public Boolean getColetaautomatica() {
		return coletaautomatica;
	}
	public void setColetaautomatica(Boolean coletaautomatica) {
		this.coletaautomatica = coletaautomatica;
	}
	public Boolean getProducaoautomatica() {
		return producaoautomatica;
	}
	public void setProducaoautomatica(Boolean producaoautomatica) {
		this.producaoautomatica = producaoautomatica;
	}
	public Boolean getPermissaoProjeto() {
		return permissaoProjeto;
	}
	public void setPermissaoProjeto(Boolean permissaoProjeto) {
		this.permissaoProjeto = permissaoProjeto;
	}
	public Boolean getExibirValorMinMaxVenda() {
		return exibirValorMinMaxVenda;
	}
	public void setExibirValorMinMaxVenda(Boolean exibirValorMinMaxVenda) {
		this.exibirValorMinMaxVenda = exibirValorMinMaxVenda;
	}
	public Boolean getParametroRecapagem() {
		return parametroRecapagem;
	}
	public void setParametroRecapagem(Boolean parametroRecapagem) {
		this.parametroRecapagem = parametroRecapagem;
	}
	public String getPedidoVendaBuscaIdentificadorTabelaPreco() {
		return pedidoVendaBuscaIdentificadorTabelaPreco;
	}
	public void setPedidoVendaBuscaIdentificadorTabelaPreco(
			String pedidoVendaBuscaIdentificadorTabelaPreco) {
		this.pedidoVendaBuscaIdentificadorTabelaPreco = pedidoVendaBuscaIdentificadorTabelaPreco;
	}
	public Boolean getConfirmarPercentualPedidoVenda() {
		return confirmarPercentualPedidoVenda;
	}
	public void setConfirmarPercentualPedidoVenda(
			Boolean confirmarPercentualPedidoVenda) {
		this.confirmarPercentualPedidoVenda = confirmarPercentualPedidoVenda;
	}
	public Boolean getConfirmarPvServicoProduto() {
		return confirmarPvServicoProduto;
	}
	public void setConfirmarPvServicoProduto(Boolean confirmarPvServicoProduto) {
		this.confirmarPvServicoProduto = confirmarPvServicoProduto;
	}
	public Boolean getReplicarMaterial() {
		return replicarMaterial;
	}
	public void setReplicarMaterial(Boolean replicarMaterial) {
		this.replicarMaterial = replicarMaterial;
	}
	public String getWmsIntegracao() {
		return wmsIntegracao;
	}
	public void setWmsIntegracao(String wmsIntegracao) {
		this.wmsIntegracao = wmsIntegracao;
	}
	public Boolean getIsMaterialmestregrade() {
		return isMaterialmestregrade;
	}
	public void setIsMaterialmestregrade(Boolean isMaterialmestregrade) {
		this.isMaterialmestregrade = isMaterialmestregrade;
	}
	public Boolean getExistGrade() {
		return existGrade;
	}
	public void setExistGrade(Boolean existGrade) {
		this.existGrade = existGrade;
	}
	public int getGeracaoContaReceberEnum() {
		return geracaoContaReceberEnum;
	}
	public void setGeracaoContaReceberEnum(int geracaoContaReceberEnum) {
		this.geracaoContaReceberEnum = geracaoContaReceberEnum;
	}
	public Boolean getBonificacaoPedidovenda() {
		return bonificacaoPedidovenda;
	}
	public void setBonificacaoPedidovenda(Boolean bonificacaoPedidovenda) {
		this.bonificacaoPedidovenda = bonificacaoPedidovenda;
	}
	public Boolean getReservaPedidovenda() {
		return reservaPedidovenda;
	}
	public void setReservaPedidovenda(Boolean reservaPedidovenda) {
		this.reservaPedidovenda = reservaPedidovenda;
	}
	public Boolean getComodatoPedidovenda() {
		return comodatoPedidovenda;
	}
	public void setComodatoPedidovenda(Boolean comodatoPedidovenda) {
		this.comodatoPedidovenda = comodatoPedidovenda;
	}
	public Boolean getDesfazerReserva() {
		return desfazerReserva;
	}
	public void setDesfazerReserva(Boolean desfazerReserva) {
		this.desfazerReserva = desfazerReserva;
	}
	public Boolean getFormaPagamentoAntecipacao() {
		return formaPagamentoAntecipacao;
	}
	public void setFormaPagamentoAntecipacao(Boolean formaPagamentoAntecipacao) {
		this.formaPagamentoAntecipacao = formaPagamentoAntecipacao;
	}
	public Boolean getObrigarLote() {
		return obrigarLote;
	}
	public void setObrigarLote(Boolean obrigarLote) {
		this.obrigarLote = obrigarLote;
	}
	public Boolean getHaveVinculoColetaProducao() {
		return haveVinculoColetaProducao;
	}
	public void setHaveVinculoColetaProducao(Boolean haveVinculoColetaProducao) {
		this.haveVinculoColetaProducao = haveVinculoColetaProducao;
	}
	public Boolean getExistDocumento() {
		return existDocumento;
	}
	public void setExistDocumento(Boolean existDocumento) {
		this.existDocumento = existDocumento;
	}
	public Boolean getExistDocumentoRepresentacao() {
		return existDocumentoRepresentacao;
	}
	public void setExistDocumentoRepresentacao(
			Boolean existDocumentoRepresentacao) {
		this.existDocumentoRepresentacao = existDocumentoRepresentacao;
	}
	public Boolean getVendaSaldoPorMinMax() {
		return vendaSaldoPorMinMax;
	}
	public void setVendaSaldoPorMinMax(Boolean vendaSaldoPorMinMax) {
		this.vendaSaldoPorMinMax = vendaSaldoPorMinMax;
	}
	public Boolean getVendaSaldoProduto() {
		return vendaSaldoProduto;
	}
	public void setVendaSaldoProduto(Boolean vendaSaldoProduto) {
		this.vendaSaldoProduto = vendaSaldoProduto;
	}
	public Boolean getVendaSaldoProdutoNegativo() {
		return vendaSaldoProdutoNegativo;
	}
	public void setVendaSaldoProdutoNegativo(Boolean vendaSaldoProdutoNegativo) {
		this.vendaSaldoProdutoNegativo = vendaSaldoProdutoNegativo;
	}
	public Boolean getDesconsiderarDescontoSaldoProduto() {
		return desconsiderarDescontoSaldoProduto;
	}
	public void setDesconsiderarDescontoSaldoProduto(
			Boolean desconsiderarDescontoSaldoProduto) {
		this.desconsiderarDescontoSaldoProduto = desconsiderarDescontoSaldoProduto;
	}
	public Boolean getBloquearVendaReservada() {
		return bloquearVendaReservada;
	}
	public void setBloquearVendaReservada(Boolean bloquearVendaReservada) {
		this.bloquearVendaReservada = bloquearVendaReservada;
	}
	public String getMenagemVendaSaldoProduto() {
		return menagemVendaSaldoProduto;
	}
	public void setMenagemVendaSaldoProduto(String menagemVendaSaldoProduto) {
		this.menagemVendaSaldoProduto = menagemVendaSaldoProduto;
	}
	public Boolean getBloquearVendaCheque() {
		return bloquearVendaCheque;
	}
	public void setBloquearVendaCheque(Boolean bloquearVendaCheque) {
		this.bloquearVendaCheque = bloquearVendaCheque;
	}
	public Boolean getClientePossuiChequePendente() {
		return clientePossuiChequePendente;
	}
	public void setClientePossuiChequePendente(Boolean clientePossuiChequePendente) {
		this.clientePossuiChequePendente = clientePossuiChequePendente;
	}
	public Boolean getLocalVendaObrigatorio() {
		return localVendaObrigatorio;
	}
	public void setLocalVendaObrigatorio(Boolean localVendaObrigatorio) {
		this.localVendaObrigatorio = localVendaObrigatorio;
	}
	public String getCondicaoPagamentoInclusaoProduto() {
		return condicaoPagamentoInclusaoProduto;
	}
	public void setCondicaoPagamentoInclusaoProduto(
			String condicaoPagamentoInclusaoProduto) {
		this.condicaoPagamentoInclusaoProduto = condicaoPagamentoInclusaoProduto;
	}
	public Boolean getObrigarTipoPedidoVenda() {
		return obrigarTipoPedidoVenda;
	}
	public void setObrigarTipoPedidoVenda(Boolean obrigarTipoPedidoVenda) {
		this.obrigarTipoPedidoVenda = obrigarTipoPedidoVenda;
	}
	public Boolean getAlterarQtdeItemProducaoVenda() {
		return alterarQtdeItemProducaoVenda;
	}
	public void setAlterarQtdeItemProducaoVenda(Boolean alterarQtdeItemProducaoVenda) {
		this.alterarQtdeItemProducaoVenda = alterarQtdeItemProducaoVenda;
	}
	public Boolean getDefinirLoteProduto() {
		return definirLoteProduto;
	}
	public void setDefinirLoteProduto(Boolean definirLoteProduto) {
		this.definirLoteProduto = definirLoteProduto;
	}
	public Boolean getIncludeAtalho() {
		return includeAtalho;
	}
	public void setIncludeAtalho(Boolean includeAtalho) {
		this.includeAtalho = includeAtalho;
	}
	public String getUriAtalhoPersonalizado() {
		return uriAtalhoPersonalizado;
	}
	public void setUriAtalhoPersonalizado(String uriAtalhoPersonalizado) {
		this.uriAtalhoPersonalizado = uriAtalhoPersonalizado;
	}
	public Boolean getVenderAbaixoMinimo() {
		return venderAbaixoMinimo;
	}
	public void setVenderAbaixoMinimo(Boolean venderAbaixoMinimo) {
		this.venderAbaixoMinimo = venderAbaixoMinimo;
	}
	public Boolean getUtilizarArredondamentoDesconto() {
		return utilizarArredondamentoDesconto;
	}
	public void setUtilizarArredondamentoDesconto(
			Boolean utilizarArredondamentoDesconto) {
		this.utilizarArredondamentoDesconto = utilizarArredondamentoDesconto;
	}
	public String getCasasDecimaisArredondamento() {
		return casasDecimaisArredondamento;
	}
	public void setCasasDecimaisArredondamento(String casasDecimaisArredondamento) {
		this.casasDecimaisArredondamento = casasDecimaisArredondamento;
	}
	public Boolean getBloqueaEdicaoQtde() {
		return bloqueaEdicaoQtde;
	}
	public void setBloqueaEdicaoQtde(Boolean bloqueaEdicaoQtde) {
		this.bloqueaEdicaoQtde = bloqueaEdicaoQtde;
	}
	public Boolean getHaveTabelaImposto() {
		return haveTabelaImposto;
	}
	public void setHaveTabelaImposto(Boolean haveTabelaImposto) {
		this.haveTabelaImposto = haveTabelaImposto;
	}
	public Boolean getExibirIpiNaVenda() {
		return exibirIpiNaVenda;
	}
	public void setExibirIpiNaVenda(Boolean exibirIpiNaVenda) {
		this.exibirIpiNaVenda = exibirIpiNaVenda;
	}
	public Boolean getExibirReplicarPneu() {
		return exibirReplicarPneu;
	}
	public void setExibirReplicarPneu(Boolean exibirReplicarPneu) {
		this.exibirReplicarPneu = exibirReplicarPneu;
	}
	public Boolean getValidadeExpiradaLimiteCredito() {
		return validadeExpiradaLimiteCredito;
	}
	public void setValidadeExpiradaLimiteCredito(
			Boolean validadeExpiradaLimiteCredito) {
		this.validadeExpiradaLimiteCredito = validadeExpiradaLimiteCredito;
	}
	public Boolean getMostrarLimiteCredito() {
		return mostrarLimiteCredito;
	}
	public void setMostrarLimiteCredito(Boolean mostrarLimiteCredito) {
		this.mostrarLimiteCredito = mostrarLimiteCredito;
	}
	public Boolean getObrigarEnderecoCliente() {
		return obrigarEnderecoCliente;
	}
	public void setObrigarEnderecoCliente(Boolean obrigarEnderecoCliente) {
		this.obrigarEnderecoCliente = obrigarEnderecoCliente;
	}
	public String getQtdeDiaUtilPrazoEntrega() {
		return qtdeDiaUtilPrazoEntrega;
	}
	public void setQtdeDiaUtilPrazoEntrega(String qtdeDiaUtilPrazoEntrega) {
		this.qtdeDiaUtilPrazoEntrega = qtdeDiaUtilPrazoEntrega;
	}
	public Boolean getBloquearLimiteDesconto() {
		return bloquearLimiteDesconto;
	}
	public void setBloquearLimiteDesconto(Boolean bloquearLimiteDesconto) {
		this.bloquearLimiteDesconto = bloquearLimiteDesconto;
	}
	public Boolean getExibirOrdemPedidoVenda() {
		return exibirOrdemPedidoVenda;
	}
	public void setExibirOrdemPedidoVenda(Boolean exibirOrdemPedidoVenda) {
		this.exibirOrdemPedidoVenda = exibirOrdemPedidoVenda;
	}
	public Boolean getExibirNomeAlternativoAlturaLargura() {
		return exibirNomeAlternativoAlturaLargura;
	}
	public void setExibirNomeAlternativoAlturaLargura(
			Boolean exibirNomeAlternativoAlturaLargura) {
		this.exibirNomeAlternativoAlturaLargura = exibirNomeAlternativoAlturaLargura;
	}
	public Boolean getCopiarNomeAlternativoParaObsMaterial() {
		return copiarNomeAlternativoParaObsMaterial;
	}
	public void setCopiarNomeAlternativoParaObsMaterial(
			Boolean copiarNomeAlternativoParaObsMaterial) {
		this.copiarNomeAlternativoParaObsMaterial = copiarNomeAlternativoParaObsMaterial;
	}
	public Boolean getIdentificadorExterno() {
		return identificadorExterno;
	}
	public void setIdentificadorExterno(Boolean identificadorExterno) {
		this.identificadorExterno = identificadorExterno;
	}
	public Boolean getHistoricoDeCliente() {
		return historicoDeCliente;
	}
	public void setHistoricoDeCliente(Boolean historicoDeCliente) {
		this.historicoDeCliente = historicoDeCliente;
	}
	public Boolean getBloqueioDimensoesVenda() {
		return bloqueioDimensoesVenda;
	}
	public void setBloqueioDimensoesVenda(Boolean bloqueioDimensoesVenda) {
		this.bloqueioDimensoesVenda = bloqueioDimensoesVenda;
	}
	public Boolean getConsiderarFreteCif() {
		return considerarFreteCif;
	}
	public void setConsiderarFreteCif(Boolean considerarFreteCif) {
		this.considerarFreteCif = considerarFreteCif;
	}
	public Boolean getPedidoVendaAprovado() {
		return pedidoVendaAprovado;
	}
	public void setPedidoVendaAprovado(Boolean pedidoVendaAprovado) {
		this.pedidoVendaAprovado = pedidoVendaAprovado;
	}
	public Boolean getClientePossuiContaAtrasada() {
		return clientePossuiContaAtrasada;
	}
	public void setClientePossuiContaAtrasada(Boolean clientePossuiContaAtrasada) {
		this.clientePossuiContaAtrasada = clientePossuiContaAtrasada;
	}
	public Boolean getBloquearVendaDevedor() {
		return bloquearVendaDevedor;
	}
	public void setBloquearVendaDevedor(Boolean bloquearVendaDevedor) {
		this.bloquearVendaDevedor = bloquearVendaDevedor;
	}
	public Boolean getRealizarPedidoVenda() {
		return realizarPedidoVenda;
	}
	public void setRealizarPedidoVenda(Boolean realizarPedidoVenda) {
		this.realizarPedidoVenda = realizarPedidoVenda;
	}
	public Boolean getChamadaImpressaoPedidoVenda() {
		return chamadaImpressaoPedidoVenda;
	}
	public void setChamadaImpressaoPedidoVenda(Boolean chamadaImpressaoPedidoVenda) {
		this.chamadaImpressaoPedidoVenda = chamadaImpressaoPedidoVenda;
	}
	public Boolean getCopiarPedidoVenda() {
		return copiarPedidoVenda;
	}
	public void setCopiarPedidoVenda(Boolean copiarPedidoVenda) {
		this.copiarPedidoVenda = copiarPedidoVenda;
	}
	public Boolean getExibirClassificacaoCliente() {
		return exibirClassificacaoCliente;
	}
	public void setExibirClassificacaoCliente(Boolean exibirClassificacaoCliente) {
		this.exibirClassificacaoCliente = exibirClassificacaoCliente;
	}
	public Boolean getConsiderarPedidoLimiteCredito() {
		return considerarPedidoLimiteCredito;
	}
	public void setConsiderarPedidoLimiteCredito(
			Boolean considerarPedidoLimiteCredito) {
		this.considerarPedidoLimiteCredito = considerarPedidoLimiteCredito;
	}
	public Boolean getPedidoVendaMaterial() {
		return pedidoVendaMaterial;
	}
	public void setPedidoVendaMaterial(Boolean pedidoVendaMaterial) {
		this.pedidoVendaMaterial = pedidoVendaMaterial;
	}
	public Boolean getPedidoBuscaIdentificadorTabelaPreco() {
		return pedidoBuscaIdentificadorTabelaPreco;
	}
	public void setPedidoBuscaIdentificadorTabelaPreco(
			Boolean pedidoBuscaIdentificadorTabelaPreco) {
		this.pedidoBuscaIdentificadorTabelaPreco = pedidoBuscaIdentificadorTabelaPreco;
	}
	public Boolean getBaixaExpedicao() {
		return baixaExpedicao;
	}
	public void setBaixaExpedicao(Boolean baixaExpedicao) {
		this.baixaExpedicao = baixaExpedicao;
	}
	public Boolean getPermitirAlterarVenda() {
		return permitirAlterarVenda;
	}
	public void setPermitirAlterarVenda(Boolean permitirAlterarVenda) {
		this.permitirAlterarVenda = permitirAlterarVenda;
	}
	public String getUtilizacaoValeCompra() {
		return utilizacaoValeCompra;
	}
	public void setUtilizacaoValeCompra(String utilizacaoValeCompra) {
		this.utilizacaoValeCompra = utilizacaoValeCompra;
	}
	public Boolean getMostrarPopupGrade() {
		return mostrarPopupGrade;
	}
	public void setMostrarPopupGrade(Boolean mostrarPopupGrade) {
		this.mostrarPopupGrade = mostrarPopupGrade;
	}
	public GenericBean getEnderecoEscolhido() {
		return enderecoEscolhido;
	}
	public void setEnderecoEscolhido(GenericBean enderecoEscolhido) {
		this.enderecoEscolhido = enderecoEscolhido;
	}
	public Boolean getAprovarOrcamento() {
		return aprovarOrcamento;
	}
	public void setAprovarOrcamento(Boolean aprovarOrcamento) {
		this.aprovarOrcamento = aprovarOrcamento;
	}
	public Boolean getRealizarVenda() {
		return realizarVenda;
	}
	public void setRealizarVenda(Boolean realizarVenda) {
		this.realizarVenda = realizarVenda;
	}
	public Boolean getExisteTrocaBanda() {
		return existeTrocaBanda;
	}
	public void setExisteTrocaBanda(Boolean existeTrocaBanda) {
		this.existeTrocaBanda = existeTrocaBanda;
	}
	public Boolean getExisteTrocaServišo() {
		return existeTrocaServišo;
	}
	public void setExisteTrocaServišo(Boolean existeTrocaServišo) {
		this.existeTrocaServišo = existeTrocaServišo;
	}
	public Boolean getExisteMovimentacaoEstoque() {
		return existeMovimentacaoEstoque;
	}
	public void setExisteMovimentacaoEstoque(Boolean existeMovimentacaoEstoque) {
		this.existeMovimentacaoEstoque = existeMovimentacaoEstoque;
	}
	public Boolean getGerarNotaAutomatico() {
		return gerarNotaAutomatico;
	}
	public void setGerarNotaAutomatico(Boolean gerarNotaAutomatico) {
		this.gerarNotaAutomatico = gerarNotaAutomatico;
	}
	public Boolean getBloqAlteracaoPreco() {
		return bloqAlteracaoPreco;
	}
	public void setBloqAlteracaoPreco(Boolean bloqAlteracaoPreco) {
		this.bloqAlteracaoPreco = bloqAlteracaoPreco;
	}
	public Boolean getExibirComprimentoOriginal() {
		return exibirComprimentoOriginal;
	}
	public void setExibirComprimentoOriginal(Boolean exibirComprimentoOriginal) {
		this.exibirComprimentoOriginal = exibirComprimentoOriginal;
	}
	public Boolean getShowTaxaPedidoVenda() {
		return showTaxaPedidoVenda;
	}
	public void setShowTaxaPedidoVenda(Boolean showTaxaPedidoVenda) {
		this.showTaxaPedidoVenda = showTaxaPedidoVenda;
	}
	public Boolean getBloqueioLarguraAlturaVenda() {
		return bloqueioLarguraAlturaVenda;
	}
	public void setBloqueioLarguraAlturaVenda(Boolean bloqueioLarguraAlturaVenda) {
		this.bloqueioLarguraAlturaVenda = bloqueioLarguraAlturaVenda;
	}
	public Boolean getVendaGerarProducao() {
		return vendaGerarProducao;
	}
	public void setVendaGerarProducao(Boolean vendaGerarProducao) {
		this.vendaGerarProducao = vendaGerarProducao;
	}
	public String getDimensaoVenda1() {
		return dimensaoVenda1;
	}
	public void setDimensaoVenda1(String dimensaoVenda1) {
		this.dimensaoVenda1 = dimensaoVenda1;
	}
	public String getDimensaoVenda2() {
		return dimensaoVenda2;
	}
	public void setDimensaoVenda2(String dimensaoVenda2) {
		this.dimensaoVenda2 = dimensaoVenda2;
	}
	public String getDimensaoVenda3() {
		return dimensaoVenda3;
	}
	public void setDimensaoVenda3(String dimensaoVenda3) {
		this.dimensaoVenda3 = dimensaoVenda3;
	}
}
