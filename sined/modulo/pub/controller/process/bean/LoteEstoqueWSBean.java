package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.util.bean.GenericBean;

public class LoteEstoqueWSBean extends GenericBean{

	private Date validade;
	private String numero;
	private Double qtde;
	
	
	public LoteEstoqueWSBean(Loteestoque bean){
		this.setId(Util.strings.toStringIdStyled(bean));
		this.setValue(Util.strings.toStringDescription(bean));
		this.setNumero(bean.getNumero());
		this.setValidade(bean.getValidade());
		this.setQtde(bean.getQtde());
	}
	
	public Date getValidade() {
		return validade;
	}
	public void setValidade(Date validade) {
		this.validade = validade;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Double getQtde() {
		return qtde;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
}
