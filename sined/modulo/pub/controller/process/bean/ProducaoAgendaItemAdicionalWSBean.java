package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.bean.GenericBean;

public class ProducaoAgendaItemAdicionalWSBean {

	protected Integer cdproducaoagendaitemadicional; 
	protected GenericBean producaoagenda;
	protected GenericBean material;
	protected Double quantidade;
	protected GenericBean pneu; 
	
	// TRANSIENTE
	protected Double quantidadeDisponivel;
	protected Double valorvenda;
	protected Money valortotal;
	
	
	
	public Integer getCdproducaoagendaitemadicional() {
		return cdproducaoagendaitemadicional;
	}
	public void setCdproducaoagendaitemadicional(
			Integer cdproducaoagendaitemadicional) {
		this.cdproducaoagendaitemadicional = cdproducaoagendaitemadicional;
	}
	public GenericBean getProducaoagenda() {
		return producaoagenda;
	}
	public void setProducaoagenda(GenericBean producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	public GenericBean getMaterial() {
		return material;
	}
	public void setMaterial(GenericBean material) {
		this.material = material;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public GenericBean getPneu() {
		return pneu;
	}
	public void setPneu(GenericBean pneu) {
		this.pneu = pneu;
	}
	public Double getQuantidadeDisponivel() {
		return quantidadeDisponivel;
	}
	public void setQuantidadeDisponivel(Double quantidadeDisponivel) {
		this.quantidadeDisponivel = quantidadeDisponivel;
	}
	public Double getValorvenda() {
		return valorvenda;
	}
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	public Money getValortotal() {
		return valortotal;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
}
