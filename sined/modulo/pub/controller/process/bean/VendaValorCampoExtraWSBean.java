package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.util.bean.GenericBean;

public class VendaValorCampoExtraWSBean {

	protected Integer cdvendavalorcampoextra;
	protected GenericBean venda;
	protected GenericBean campoextrapedidovendatipo;
	protected String valor;
	
	
	public Integer getCdvendavalorcampoextra() {
		return cdvendavalorcampoextra;
	}
	public void setCdvendavalorcampoextra(Integer cdvendavalorcampoextra) {
		this.cdvendavalorcampoextra = cdvendavalorcampoextra;
	}
	public GenericBean getVenda() {
		return venda;
	}
	public void setVenda(GenericBean venda) {
		this.venda = venda;
	}
	public GenericBean getCampoextrapedidovendatipo() {
		return campoextrapedidovendatipo;
	}
	public void setCampoextrapedidovendatipo(GenericBean campoextrapedidovendatipo) {
		this.campoextrapedidovendatipo = campoextrapedidovendatipo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}
