package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.auxiliar.PedidoVendaParametersBean;

public class GenericSearchBean{

	private String value;
	private Empresa empresa;
	private Usuario usuario;
	private Cliente cliente;
	private Material material;
	private Documentotipo documentoTipo;
	private Pedidovendatipo pedidoVendaTipo;
	private String whereIn;
	private Pedidovenda pedidoVenda;
	private Venda venda;
	private Unidademedida unidadeMedida;
	private Fornecedor fornecedor;
	private Prazopagamento prazoPagamento;
	private Localarmazenagem localArmazenagem;
	private Loteestoque loteEstoque;
	private Pneu pneu;
	private Materialtabelapreco materialTabelaPreco;
	private Projeto projeto;
	private PedidoVendaParametersBean parameters;
	private Boolean fromPedidoVenda;
	private Date data;
	
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Documentotipo getDocumentoTipo() {
		return documentoTipo;
	}
	public void setDocumentoTipo(Documentotipo documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	public Pedidovendatipo getPedidoVendaTipo() {
		return pedidoVendaTipo;
	}
	public void setPedidoVendaTipo(Pedidovendatipo pedidoVendaTipo) {
		this.pedidoVendaTipo = pedidoVendaTipo;
	}
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public Pedidovenda getPedidoVenda() {
		return pedidoVenda;
	}
	public void setPedidoVenda(Pedidovenda pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public Unidademedida getUnidadeMedida() {
		return unidadeMedida;
	}
	public void setUnidadeMedida(Unidademedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Prazopagamento getPrazoPagamento() {
		return prazoPagamento;
	}
	public void setPrazoPagamento(Prazopagamento prazoPagamento) {
		this.prazoPagamento = prazoPagamento;
	}
	public Localarmazenagem getLocalArmazenagem() {
		return localArmazenagem;
	}
	public void setLocalArmazenagem(Localarmazenagem localArmazenagem) {
		this.localArmazenagem = localArmazenagem;
	}
	public Loteestoque getLoteEstoque() {
		return loteEstoque;
	}
	public void setLoteEstoque(Loteestoque loteEstoque) {
		this.loteEstoque = loteEstoque;
	}
	public Pneu getPneu() {
		return pneu;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	public Materialtabelapreco getMaterialTabelaPreco() {
		return materialTabelaPreco;
	}
	public void setMaterialTabelaPreco(Materialtabelapreco materialTabelaPreco) {
		this.materialTabelaPreco = materialTabelaPreco;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public PedidoVendaParametersBean getParameters() {
		return parameters;
	}
	public void setParameters(PedidoVendaParametersBean parameters) {
		this.parameters = parameters;
	}
	public Boolean getFromPedidoVenda() {
		return fromPedidoVenda;
	}
	public void setFromPedidoVenda(Boolean fromPedidoVenda) {
		this.fromPedidoVenda = fromPedidoVenda;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
}