package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;


public class EnviarArquivonfBean extends GenericEnvioInformacaoBean {
	
	private Integer cdarquivonf;
	private Arquivo arquivo;
	private String xml;
	
	public Integer getCdarquivonf() {
		return cdarquivonf;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public String getXml() {
		return xml;
	}
	public void setXml(String xml) {
		this.xml = xml;
	}
	public void setCdarquivonf(Integer cdarquivonf) {
		this.cdarquivonf = cdarquivonf;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
}
