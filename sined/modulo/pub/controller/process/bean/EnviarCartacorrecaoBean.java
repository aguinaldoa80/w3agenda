package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;


public class EnviarCartacorrecaoBean extends GenericEnvioInformacaoBean {
	
	private Integer cdnotafiscalprodutocorrecao;
	private Arquivo arquivo;
	
	public Integer getCdnotafiscalprodutocorrecao() {
		return cdnotafiscalprodutocorrecao;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setCdnotafiscalprodutocorrecao(
			Integer cdnotafiscalprodutocorrecao) {
		this.cdnotafiscalprodutocorrecao = cdnotafiscalprodutocorrecao;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
}
