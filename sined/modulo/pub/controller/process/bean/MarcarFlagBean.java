package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class MarcarFlagBean extends GenericEnvioInformacaoBean {
	
	private String whereInArquivonf;
	private String whereInArquivonfnota;
	private String whereInInutilizacaonfe;
	private String whereInCartacorrecao;
	private String whereInArquivoMdfe;
	private String whereInArquivoMdfeConsulta;
	private String whereInArquivoMdfeEncerramento;
	private String whereInArquivoMdfeCancelamento;
	private String whereInArquivoMdfeInclusaoCondutor;
	private String whereInLoteConsultaDfe;
	private String whereInManifestoDfeEvento;
	
	public String getWhereInArquivonf() {
		return whereInArquivonf;
	}
	
	public String getWhereInArquivonfnota() {
		return whereInArquivonfnota;
	}
	
	public String getWhereInInutilizacaonfe() {
		return whereInInutilizacaonfe;
	}
	
	public String getWhereInCartacorrecao() {
		return whereInCartacorrecao;
	}
	
	public void setWhereInCartacorrecao(String whereInCartacorrecao) {
		this.whereInCartacorrecao = whereInCartacorrecao;
	}
	
	public void setWhereInInutilizacaonfe(String whereInInutilizacaonfe) {
		this.whereInInutilizacaonfe = whereInInutilizacaonfe;
	}
	
	public void setWhereInArquivonfnota(String whereInArquivonfnota) {
		this.whereInArquivonfnota = whereInArquivonfnota;
	}
	
	public void setWhereInArquivonf(String whereInArquivonf) {
		this.whereInArquivonf = whereInArquivonf;
	}

	public String getWhereInArquivoMdfe() {
		return whereInArquivoMdfe;
	}
	
	public String getWhereInLoteConsultaDfe() {
		return whereInLoteConsultaDfe;
	}
	
	public void setWhereInLoteConsultaDfe(String whereInLoteConsultaDfe) {
		this.whereInLoteConsultaDfe = whereInLoteConsultaDfe;
	}
	
	public void setWhereInArquivoMdfe(String whereInArquivoMdfe) {
		this.whereInArquivoMdfe = whereInArquivoMdfe;
	}

	public String getWhereInArquivoMdfeEncerramento() {
		return whereInArquivoMdfeEncerramento;
	}

	public void setWhereInArquivoMdfeEncerramento(String whereInArquivoMdfeEncerramento) {
		this.whereInArquivoMdfeEncerramento = whereInArquivoMdfeEncerramento;
	}
	
	public String getWhereInArquivoMdfeCancelamento() {
		return whereInArquivoMdfeCancelamento;
	}
	
	public void setWhereInArquivoMdfeCancelamento(String whereInArquivoMdfeCancelamento) {
		this.whereInArquivoMdfeCancelamento = whereInArquivoMdfeCancelamento;
	}
	
	public String getWhereInArquivoMdfeInclusaoCondutor() {
		return whereInArquivoMdfeInclusaoCondutor;
	}
	
	public void setWhereInArquivoMdfeInclusaoCondutor(String whereInArquivoMdfeInclusaoCondutor) {
		this.whereInArquivoMdfeInclusaoCondutor = whereInArquivoMdfeInclusaoCondutor;
	}
	
	public String getWhereInArquivoMdfeConsulta() {
		return whereInArquivoMdfeConsulta;
	}
	
	public void setWhereInArquivoMdfeConsulta(String whereInArquivoMdfeConsulta) {
		this.whereInArquivoMdfeConsulta = whereInArquivoMdfeConsulta;
	}
	
	public String getWhereInManifestoDfeEvento() {
		return whereInManifestoDfeEvento;
	}
	
	public void setWhereInManifestoDfeEvento(String whereInManifestoDfeEvento) {
		this.whereInManifestoDfeEvento = whereInManifestoDfeEvento;
	}
}
