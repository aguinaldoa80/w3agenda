package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialseparacao;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaMaterialWSBean {

	protected Integer cdpedidovendamaterial;
	protected Integer ordem;
	protected GenericBean  pedidovenda;
	protected GenericBean  material;
	protected GenericBean  materialmestre;
	protected Double quantidade;
	protected Double preco;
	protected Money desconto;
	protected Double percentualdesconto;
	protected GenericBean  unidademedida;
	protected Date dtprazoentrega;
	protected String observacao;
	protected GenericBean  loteestoque;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected Double valorcustomaterial;
	protected Double valorvendamaterial;
	protected Money saldo;
	protected Double comprimento;
	protected Double comprimentooriginal;
	protected Double largura;
	protected Double altura;
	protected Double multiplicador = 1.0;
	protected Boolean producaosemestoque;
	protected GenericBean  fornecedor;
	protected Money percentualrepresentacao;
	protected GenericBean  materialcoleta;
	protected List<Pedidovendamaterialseparacao> listaPedidovendamaterialseparacao = new ListSet<Pedidovendamaterialseparacao>(Pedidovendamaterialseparacao.class);
	protected List<Entregamaterial> listaEntregamaterial;
	protected Set<Vendamaterial> listaVendamaterial;
	protected String identificadorespecifico;
	protected String identificadorinterno;
	protected PneuWSBean pneu;
	protected Double custooperacional;
	protected Double percentualcomissaoagencia;
	protected Double qtdevolumeswms;
	protected Money valorimposto;
	protected Double ipi;
	protected Money valoripi;
	protected TranslateEnumBean tipocobrancaipi;
	protected TranslateEnumBean tipocalculoipi;
	protected Money aliquotareaisipi;
	protected GenericBean  grupotributacao;
	protected Double peso;
	protected Integer identificadorintegracao;
	protected Double valorcoleta;
	protected Boolean bandaalterada;
	protected Boolean servicoalterado;
	protected Double valorvalecomprapropocional;
	protected Double valordescontosemvalecompra;
	protected Double pesomedio;
	protected GenericBean  comissionamento;
	protected GenericBean  pedidovendamaterialgarantido;
	protected Money descontogarantiareforma;
	protected GenericBean  garantiareformaitem;
	protected Double valorMinimo;
	protected GenericBean  materialFaixaMarkup;
	protected GenericBean  faixaMarkupNome;
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public GenericBean  getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(GenericBean  pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public GenericBean  getMaterial() {
		return material;
	}
	public void setMaterial(GenericBean  material) {
		this.material = material;
	}
	public GenericBean  getMaterialmestre() {
		return materialmestre;
	}
	public void setMaterialmestre(GenericBean  materialmestre) {
		this.materialmestre = materialmestre;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	public GenericBean  getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(GenericBean  unidademedida) {
		this.unidademedida = unidademedida;
	}
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public GenericBean  getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(GenericBean  loteestoque) {
		this.loteestoque = loteestoque;
	}
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	public Money getSaldo() {
		return saldo;
	}
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public Double getComprimentooriginal() {
		return comprimentooriginal;
	}
	public void setComprimentooriginal(Double comprimentooriginal) {
		this.comprimentooriginal = comprimentooriginal;
	}
	public Double getLargura() {
		return largura;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public Double getAltura() {
		return altura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public Double getMultiplicador() {
		return multiplicador;
	}
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}
	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	public GenericBean  getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(GenericBean  fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Money getPercentualrepresentacao() {
		return percentualrepresentacao;
	}
	public void setPercentualrepresentacao(Money percentualrepresentacao) {
		this.percentualrepresentacao = percentualrepresentacao;
	}
	public GenericBean  getMaterialcoleta() {
		return materialcoleta;
	}
	public void setMaterialcoleta(GenericBean  materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	public List<Pedidovendamaterialseparacao> getListaPedidovendamaterialseparacao() {
		return listaPedidovendamaterialseparacao;
	}
	public void setListaPedidovendamaterialseparacao(
			List<Pedidovendamaterialseparacao> listaPedidovendamaterialseparacao) {
		this.listaPedidovendamaterialseparacao = listaPedidovendamaterialseparacao;
	}
	public List<Entregamaterial> getListaEntregamaterial() {
		return listaEntregamaterial;
	}
	public void setListaEntregamaterial(List<Entregamaterial> listaEntregamaterial) {
		this.listaEntregamaterial = listaEntregamaterial;
	}
	public Set<Vendamaterial> getListaVendamaterial() {
		return listaVendamaterial;
	}
	public void setListaVendamaterial(Set<Vendamaterial> listaVendamaterial) {
		this.listaVendamaterial = listaVendamaterial;
	}
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	public PneuWSBean getPneu() {
		return pneu;
	}
	public void setPneu(PneuWSBean pneu) {
		this.pneu = pneu;
	}
	public Double getCustooperacional() {
		return custooperacional;
	}
	public void setCustooperacional(Double custooperacional) {
		this.custooperacional = custooperacional;
	}
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}
	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}
	public Double getQtdevolumeswms() {
		return qtdevolumeswms;
	}
	public void setQtdevolumeswms(Double qtdevolumeswms) {
		this.qtdevolumeswms = qtdevolumeswms;
	}
	public Money getValorimposto() {
		return valorimposto;
	}
	public void setValorimposto(Money valorimposto) {
		this.valorimposto = valorimposto;
	}
	public Double getIpi() {
		return ipi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public Money getValoripi() {
		return valoripi;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public TranslateEnumBean getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	public void setTipocobrancaipi(TranslateEnumBean tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}
	public TranslateEnumBean getTipocalculoipi() {
		return tipocalculoipi;
	}
	public void setTipocalculoipi(TranslateEnumBean tipocalculoipi) {
		this.tipocalculoipi = tipocalculoipi;
	}
	public Money getAliquotareaisipi() {
		return aliquotareaisipi;
	}
	public void setAliquotareaisipi(Money aliquotareaisipi) {
		this.aliquotareaisipi = aliquotareaisipi;
	}
	public GenericBean  getGrupotributacao() {
		return grupotributacao;
	}
	public void setGrupotributacao(GenericBean  grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public Integer getIdentificadorintegracao() {
		return identificadorintegracao;
	}
	public void setIdentificadorintegracao(Integer identificadorintegracao) {
		this.identificadorintegracao = identificadorintegracao;
	}
	public Double getValorcoleta() {
		return valorcoleta;
	}
	public void setValorcoleta(Double valorcoleta) {
		this.valorcoleta = valorcoleta;
	}
	public Boolean getBandaalterada() {
		return bandaalterada;
	}
	public void setBandaalterada(Boolean bandaalterada) {
		this.bandaalterada = bandaalterada;
	}
	public Boolean getServicoalterado() {
		return servicoalterado;
	}
	public void setServicoalterado(Boolean servicoalterado) {
		this.servicoalterado = servicoalterado;
	}
	public Double getValorvalecomprapropocional() {
		return valorvalecomprapropocional;
	}
	public void setValorvalecomprapropocional(Double valorvalecomprapropocional) {
		this.valorvalecomprapropocional = valorvalecomprapropocional;
	}
	public Double getValordescontosemvalecompra() {
		return valordescontosemvalecompra;
	}
	public void setValordescontosemvalecompra(Double valordescontosemvalecompra) {
		this.valordescontosemvalecompra = valordescontosemvalecompra;
	}
	public Double getPesomedio() {
		return pesomedio;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	public GenericBean  getComissionamento() {
		return comissionamento;
	}
	public void setComissionamento(GenericBean  comissionamento) {
		this.comissionamento = comissionamento;
	}
	public GenericBean  getPedidovendamaterialgarantido() {
		return pedidovendamaterialgarantido;
	}
	public void setPedidovendamaterialgarantido(
			GenericBean  pedidovendamaterialgarantido) {
		this.pedidovendamaterialgarantido = pedidovendamaterialgarantido;
	}
	public Money getDescontogarantiareforma() {
		return descontogarantiareforma;
	}
	public void setDescontogarantiareforma(Money descontogarantiareforma) {
		this.descontogarantiareforma = descontogarantiareforma;
	}
	public GenericBean  getGarantiareformaitem() {
		return garantiareformaitem;
	}
	public void setGarantiareformaitem(GenericBean  garantiareformaitem) {
		this.garantiareformaitem = garantiareformaitem;
	}
	public Double getValorMinimo() {
		return valorMinimo;
	}
	public void setValorMinimo(Double valorMinimo) {
		this.valorMinimo = valorMinimo;
	}
	public GenericBean  getMaterialFaixaMarkup() {
		return materialFaixaMarkup;
	}
	public void setMaterialFaixaMarkup(GenericBean  materialFaixaMarkup) {
		this.materialFaixaMarkup = materialFaixaMarkup;
	}
	public GenericBean  getFaixaMarkupNome() {
		return faixaMarkupNome;
	}
	public void setFaixaMarkupNome(GenericBean  faixaMarkupNome) {
		this.faixaMarkupNome = faixaMarkupNome;
	}
}
