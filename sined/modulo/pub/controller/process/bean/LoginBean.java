package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class LoginBean extends GenericEnvioInformacaoBean {
	
	private String login;
	private String senha;
	
	public String getLogin() {
		return login;
	}
	public String getSenha() {
		return senha;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

}
