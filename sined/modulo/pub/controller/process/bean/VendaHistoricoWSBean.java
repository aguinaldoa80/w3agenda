package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Timestamp;

import br.com.linkcom.sined.util.bean.GenericBean;

public class VendaHistoricoWSBean {

	protected Integer cdvendahistorico;
	protected GenericBean venda;
	protected String acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
//	protected Timestamp dtaltera;
	protected GenericBean usuarioaltera;
	protected GenericBean empresa;
	
	
	
	public Integer getCdvendahistorico() {
		return cdvendahistorico;
	}
	public void setCdvendahistorico(Integer cdvendahistorico) {
		this.cdvendahistorico = cdvendahistorico;
	}
	public GenericBean getVenda() {
		return venda;
	}
	public void setVenda(GenericBean venda) {
		this.venda = venda;
	}
	public String getAcao() {
		return acao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
/*	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}*/
	public GenericBean getUsuarioaltera() {
		return usuarioaltera;
	}
	public void setUsuarioaltera(GenericBean usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}
	public GenericBean getEmpresa() {
		return empresa;
	}
	public void setEmpresa(GenericBean empresa) {
		this.empresa = empresa;
	}
}
