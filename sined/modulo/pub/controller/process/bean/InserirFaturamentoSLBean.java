package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.types.Money;


public class InserirFaturamentoSLBean extends GenericEnvioInformacaoBean {
	
	private String cnpjcliente;
	private String cpfcliente;
	private String pagamentooutros;
	
	private Money valorComissao;
	private Money valorDespesa;
	private Money valorArremate;
	private Integer cdcontagerencialComissao;
	private Integer cdcontagerencialDespesa;
	private Integer cdcontagerencialArremate;
	private Integer cdcentrocusto;
	
	private Integer cddocumentotipo;
	private Integer cdempresa;
	private String descricao;
	private Integer cdconta;
	private String codigocnae;
	private String itemlistaservico;
	private String emailremetenteboleto;
	private String nomearquivoboleto;
	
	private String mensagem1;
	private String mensagem2;
	private String mensagem3;
	
	public String getCnpjcliente() {
		return cnpjcliente;
	}
	public String getCpfcliente() {
		return cpfcliente;
	}
	public Money getValorComissao() {
		return valorComissao;
	}
	public Money getValorDespesa() {
		return valorDespesa;
	}
	public Money getValorArremate() {
		return valorArremate;
	}
	public Integer getCdcontagerencialComissao() {
		return cdcontagerencialComissao;
	}
	public Integer getCdcontagerencialDespesa() {
		return cdcontagerencialDespesa;
	}
	public Integer getCdcontagerencialArremate() {
		return cdcontagerencialArremate;
	}
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public String getDescricao() {
		return descricao;
	}
	public Integer getCdconta() {
		return cdconta;
	}
	public String getCodigocnae() {
		return codigocnae;
	}
	public String getItemlistaservico() {
		return itemlistaservico;
	}
	public String getEmailremetenteboleto() {
		return emailremetenteboleto;
	}
	public String getNomearquivoboleto() {
		return nomearquivoboleto;
	}
	public String getPagamentooutros() {
		return pagamentooutros;
	}
	public String getMensagem1() {
		return mensagem1;
	}
	public String getMensagem2() {
		return mensagem2;
	}
	public String getMensagem3() {
		return mensagem3;
	}
	public void setMensagem1(String mensagem1) {
		this.mensagem1 = mensagem1;
	}
	public void setMensagem2(String mensagem2) {
		this.mensagem2 = mensagem2;
	}
	public void setMensagem3(String mensagem3) {
		this.mensagem3 = mensagem3;
	}
	public void setPagamentooutros(String pagamentooutros) {
		this.pagamentooutros = pagamentooutros;
	}
	public void setCnpjcliente(String cnpjcliente) {
		this.cnpjcliente = cnpjcliente;
	}
	public void setCpfcliente(String cpfcliente) {
		this.cpfcliente = cpfcliente;
	}
	public void setValorComissao(Money valorComissao) {
		this.valorComissao = valorComissao;
	}
	public void setValorDespesa(Money valorDespesa) {
		this.valorDespesa = valorDespesa;
	}
	public void setValorArremate(Money valorArremate) {
		this.valorArremate = valorArremate;
	}
	public void setCdcontagerencialComissao(Integer cdcontagerencialComissao) {
		this.cdcontagerencialComissao = cdcontagerencialComissao;
	}
	public void setCdcontagerencialDespesa(Integer cdcontagerencialDespesa) {
		this.cdcontagerencialDespesa = cdcontagerencialDespesa;
	}
	public void setCdcontagerencialArremate(Integer cdcontagerencialArremate) {
		this.cdcontagerencialArremate = cdcontagerencialArremate;
	}
	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setCodigocnae(String codigocnae) {
		this.codigocnae = codigocnae;
	}
	public void setItemlistaservico(String itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}
	public void setEmailremetenteboleto(String emailremetenteboleto) {
		this.emailremetenteboleto = emailremetenteboleto;
	}
	public void setNomearquivoboleto(String nomearquivoboleto) {
		this.nomearquivoboleto = nomearquivoboleto;
	}

}
