package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class AtualizaFaturamentoSLBean extends GenericEnvioInformacaoBean  {

	private Integer cddocumento;
	private Integer cdcliente;
	
	public Integer getCddocumento() {
		return cddocumento;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public Integer getCdcliente() {
		return cdcliente;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	
}
