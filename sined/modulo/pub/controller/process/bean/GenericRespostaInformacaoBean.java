package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import net.sf.json.JSON;

import org.jdom.Element;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.enumeration.WebServiceReturnType;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

public class GenericRespostaInformacaoBean {

	private Map<String, Object> campos = new HashMap<String, Object>();
	private WebServiceReturnType webServiceReturnType = WebServiceReturnType.XML;
	
	public GenericRespostaInformacaoBean() {
	}
	
	public GenericRespostaInformacaoBean(String format) {
		if(format != null && WebServiceReturnType.JSON.equals(format)){
			this.webServiceReturnType = WebServiceReturnType.JSON;
		} else {
			this.webServiceReturnType = WebServiceReturnType.XML;
		}
	}
	
	public void addCampo(String campo, Object valor){
		campos.put(campo, valor);
	}
	
	public String responseSucesso(){
		if(webServiceReturnType.equals(WebServiceReturnType.JSON)){
			NeoWeb.getRequestContext().getServletResponse().setContentType("application/json");
			return imprimeJSONSucesso();
		} else {
			NeoWeb.getRequestContext().getServletResponse().setContentType("application/xml");
			return imprimeXMLSucesso();
		}
	}
	
	public String responseErro(Exception exception){
		if(webServiceReturnType.equals(WebServiceReturnType.JSON)){
			NeoWeb.getRequestContext().getServletResponse().setContentType("application/json");
			return imprimeJSONErro(exception.getMessage());
		} else {
			NeoWeb.getRequestContext().getServletResponse().setContentType("application/xml");
			return imprimeXMLErro(exception);
		}
	}
	
	private String imprimeXMLSucesso() {
		try {
			Element resposta = new Element("resposta");
			
			Element status = new Element("status");
			status.setText("1");
			resposta.addContent(status);
			
			Set<Entry<String, Object>> entrySet = campos.entrySet();
			if(entrySet != null && entrySet.size() > 0){
				for (Entry<String, Object> entry : entrySet) {
					Object value = entry.getValue();
					if(value instanceof List<?>){
						List<?> lista = (List<?>)value;
						for (Object object : lista) {
							Element eltList = new Element(entry.getKey());
							resposta.addContent(eltList);
							
							Class<? extends Object> classe = object.getClass();
							
							for(Method metodo : classe.getDeclaredMethods()){
								if(metodo.getName().startsWith("get") && metodo.getName().length() > 3 ){
									String nome = Util.strings.uncaptalize(metodo.getName().substring(3));
									Object obj = metodo.invoke(object);
									 
									Element elt = new Element(nome);
									elt.setText(obj != null ? obj.toString() : "");
									eltList.addContent(elt);
								}
							}
						}
					} else {
						Element elt = new Element(entry.getKey());
						elt.setText(value != null ? value.toString() : "");
						resposta.addContent(elt);
					}
				}
			}
		
			return SinedUtil.getRespostaXML(resposta);
		} catch (Exception e) {
			e.printStackTrace();
			return this.imprimeXMLErro(new SinedException("Falha na conversao do BEAN para XML."));
		}
	}
	
	private String imprimeXMLErro(Exception exception) {
		try {
			Element resposta = new Element("resposta");
			
			Element status = new Element("status");
			status.setText("0");
			
			Element erro = new Element("erro");
			erro.setText(Util.strings.tiraAcento(exception.getMessage()));
			
			StringWriter stringWriter = new StringWriter();
			exception.printStackTrace(new PrintWriter(stringWriter));
			
			Element stacktrace = new Element("Stacktrace");
			stacktrace.setText(stringWriter.toString());
			
			resposta.addContent(status);
			resposta.addContent(erro);
			resposta.addContent(stacktrace);
			
			return SinedUtil.getRespostaXML(resposta);
		} catch (Exception e) {
			e.printStackTrace();
			return "<resposta><status>0</status><erro>ERRO</erro></resposta>";
		}
	}

	private String imprimeJSONSucesso(){
		try {
			campos.put("status", 1);
			
			return writeJSONResponse(campos);
		} catch (Exception e) {
			e.printStackTrace();
			return this.imprimeJSONErro("Falha na conversao do BEAN para JSON.");
		}
	}
	
	private String imprimeJSONErro(String msgErro){
		Map<String, Object> mapa = new HashMap<String, Object>();
		
		mapa.put("status", 0);
		mapa.put("erro", msgErro);

		return writeJSONResponse(mapa);
	}

	private String writeJSONResponse(Map<String, Object> mapa) {
		JSON jsonObj = View.convertToJson(mapa);
		
		StringWriter stringWriter = new StringWriter();
		jsonObj.write(stringWriter);

		return stringWriter.toString();
	}
	
}
