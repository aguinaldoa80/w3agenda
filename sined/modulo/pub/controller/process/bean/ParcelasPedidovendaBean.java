package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class ParcelasPedidovendaBean {

	private Integer cddocumento;
	private String dtvencimento;
	private String valor;
	
	public Integer getCddocumento() {
		return cddocumento;
	}
	public String getDtvencimento() {
		return dtvencimento;
	}
	public String getValor() {
		return valor;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public void setDtvencimento(String dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
}
