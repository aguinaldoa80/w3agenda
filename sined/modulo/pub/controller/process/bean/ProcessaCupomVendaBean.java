package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class ProcessaCupomVendaBean extends GenericEnvioInformacaoBean {
	
	private String loja;
	
	public String getLoja() {
		return loja;
	}
	public void setLoja(String loja) {
		this.loja = loja;
	}
	
}
