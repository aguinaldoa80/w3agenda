package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialsimilar;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;

public class MaterialSimilarTrocaWSBean{

	private GenericBean material;
	private List<GenericBean> listaMaterialSimilar;
	
	
	public MaterialSimilarTrocaWSBean(){
		
	}
	
	public MaterialSimilarTrocaWSBean(Material material){
		this.material = ObjectUtils.translateEntityToGenericBean(material);
		listaMaterialSimilar = new ArrayList<GenericBean>();
		if(SinedUtil.isListNotEmpty(material.getListaMaterialsimilar())){
			for(Materialsimilar m: material.getListaMaterialsimilar()){
				listaMaterialSimilar.add(ObjectUtils.translateEntityToGenericBean(m.getMaterialsimilaritem()));
			}
		}
	}
	
	public GenericBean getMaterial() {
		return material;
	}
	public void setMaterial(GenericBean material) {
		this.material = material;
	}
	
	public List<GenericBean> getListaMaterialSimilar() {
		return listaMaterialSimilar;
	}
	public void setListaMaterialSimilar(List<GenericBean> listaMaterialSimilar) {
		this.listaMaterialSimilar = listaMaterialSimilar;
	}
}
