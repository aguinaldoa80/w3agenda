package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Timestamp;

import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaHistoricoWSBean {

	protected Integer cdpedidovendahistorico;
	protected GenericBean pedidovenda;
	protected String acao;
	protected String observacao;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	
	public Integer getCdpedidovendahistorico() {
		return cdpedidovendahistorico;
	}
	public void setCdpedidovendahistorico(Integer cdpedidovendahistorico) {
		this.cdpedidovendahistorico = cdpedidovendahistorico;
	}
	public GenericBean getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(GenericBean pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public String getAcao() {
		return acao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
}
