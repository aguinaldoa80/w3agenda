package br.com.linkcom.sined.modulo.pub.controller.process.bean;



public class CancelamentoFaturamentoBean extends GenericEnvioInformacaoBean {
	
	private Integer cddocumento;

	public Integer getCddocumento() {
		return cddocumento;
	}

	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
}
