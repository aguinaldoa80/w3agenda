package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.util.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.bean.GenericBean;

public class VendaPagamentoWSBean {

	protected Integer cdvendapagamento;
	protected GenericBean venda;
	protected GenericBean documento;
	protected GenericBean documentotipo;
	protected Integer banco;
	protected Integer agencia;
	protected Integer conta;
	protected String numero;
	protected Money valororiginal;
	protected Date dataparcela;
	protected Money valorjuros;
	protected Boolean prazomedio;
	protected GenericBean documentoantecipacao;
	protected GenericBean cheque;
	protected String emitente;
	protected String cpfcnpj;
	public Integer getCdvendapagamento() {
		return cdvendapagamento;
	}
	public void setCdvendapagamento(Integer cdvendapagamento) {
		this.cdvendapagamento = cdvendapagamento;
	}
	public GenericBean getVenda() {
		return venda;
	}
	public void setVenda(GenericBean venda) {
		this.venda = venda;
	}
	public GenericBean getDocumento() {
		return documento;
	}
	public void setDocumento(GenericBean documento) {
		this.documento = documento;
	}
	public GenericBean getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(GenericBean documentotipo) {
		this.documentotipo = documentotipo;
	}
	public Integer getBanco() {
		return banco;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public Integer getAgencia() {
		return agencia;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public Integer getConta() {
		return conta;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Money getValororiginal() {
		return valororiginal;
	}
	public void setValororiginal(Money valororiginal) {
		this.valororiginal = valororiginal;
	}
	public Date getDataparcela() {
		return dataparcela;
	}
	public void setDataparcela(Date dataparcela) {
		this.dataparcela = dataparcela;
	}
	public Money getValorjuros() {
		return valorjuros;
	}
	public void setValorjuros(Money valorjuros) {
		this.valorjuros = valorjuros;
	}
	public Boolean getPrazomedio() {
		return prazomedio;
	}
	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}
	public GenericBean getDocumentoantecipacao() {
		return documentoantecipacao;
	}
	public void setDocumentoantecipacao(GenericBean documentoantecipacao) {
		this.documentoantecipacao = documentoantecipacao;
	}
	public GenericBean getCheque() {
		return cheque;
	}
	public void setCheque(GenericBean cheque) {
		this.cheque = cheque;
	}
	public String getEmitente() {
		return emitente;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	public String getCpfcnpj() {
		return cpfcnpj;
	}
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
}
