package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public class ConsultaTabelaPreco extends GenericEnvioInformacaoBean {
	
	private Material material;
	private Pedidovendatipo pedidovendatipo;
	private Empresa empresa;
	private Unidademedida unidademedida;
	private Prazopagamento prazopagamento;
	
	public Material getMaterial() {
		return material;
	}
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
}
