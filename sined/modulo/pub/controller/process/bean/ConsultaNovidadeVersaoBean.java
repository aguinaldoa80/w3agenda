package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class ConsultaNovidadeVersaoBean extends GenericEnvioInformacaoBean {
	
	private String projeto;
	private Boolean consultaMenu;
	private Integer cdnovidadeVersao;
	private Boolean novidadePeriodoAtual;
	
	public Boolean getConsultaMenu() {
		return consultaMenu;
	}
	public void setConsultaMenu(Boolean consultaMenu) {
		this.consultaMenu = consultaMenu;
	}
	public Integer getCdnovidadeVersao() {
		return cdnovidadeVersao;
	}
	public String getProjeto() {
		return projeto;
	}
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	public void setCdnovidadeVersao(Integer cdnovidadeVersao) {
		this.cdnovidadeVersao = cdnovidadeVersao;
	}
	public Boolean getNovidadePeriodoAtual() {
		return novidadePeriodoAtual;
	}
	public void setNovidadePeriodoAtual(Boolean novidadePeriodoAtual) {
		this.novidadePeriodoAtual = novidadePeriodoAtual;
	}
}
