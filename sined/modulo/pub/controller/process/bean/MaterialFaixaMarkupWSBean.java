package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.util.bean.GenericBean;

public class MaterialFaixaMarkupWSBean extends GenericBean{

	private String cor;
	private Double valorVenda;
	
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public Double getValorVenda() {
		return valorVenda;
	}
	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}
}
