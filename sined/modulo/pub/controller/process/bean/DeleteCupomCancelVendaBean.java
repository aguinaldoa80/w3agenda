package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class DeleteCupomCancelVendaBean extends GenericEnvioInformacaoBean {
	
	private String loja;
	private String pdv;
	private String numero_ticket;
	
	public String getLoja() {
		return loja;
	}
	public String getPdv() {
		return pdv;
	}
	public String getNumero_ticket() {
		return numero_ticket;
	}
	public void setLoja(String loja) {
		this.loja = loja;
	}
	public void setPdv(String pdv) {
		this.pdv = pdv;
	}
	public void setNumero_ticket(String numeroTicket) {
		numero_ticket = numeroTicket;
	}
	
}
