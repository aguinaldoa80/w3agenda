package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaPagamentoRepresentacaoWSBean {

	protected Integer cdpedidovendapagamentorepresentacao;
	protected GenericBean pedidovenda;
	protected GenericBean documentotipo;
	protected GenericBean documento;
	protected Date dataparcela;
	protected Money valororiginal;
	protected GenericBean fornecedor;
	
	
	public Integer getCdpedidovendapagamentorepresentacao() {
		return cdpedidovendapagamentorepresentacao;
	}
	public void setCdpedidovendapagamentorepresentacao(
			Integer cdpedidovendapagamentorepresentacao) {
		this.cdpedidovendapagamentorepresentacao = cdpedidovendapagamentorepresentacao;
	}
	public GenericBean getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(GenericBean pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public GenericBean getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(GenericBean documentotipo) {
		this.documentotipo = documentotipo;
	}
	public GenericBean getDocumento() {
		return documento;
	}
	public void setDocumento(GenericBean documento) {
		this.documento = documento;
	}
	public Date getDataparcela() {
		return dataparcela;
	}
	public void setDataparcela(Date dataparcela) {
		this.dataparcela = dataparcela;
	}
	public Money getValororiginal() {
		return valororiginal;
	}
	public void setValororiginal(Money valororiginal) {
		this.valororiginal = valororiginal;
	}
	public GenericBean getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(GenericBean fornecedor) {
		this.fornecedor = fornecedor;
	}
}
