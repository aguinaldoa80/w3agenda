package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaWSBean extends GenericBean{

	private GenericBean cliente;
	
	public GenericBean getCliente() {
		return cliente;
	}
	public void setCliente(GenericBean cliente) {
		this.cliente = cliente;
	}
}
