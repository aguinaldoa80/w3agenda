package br.com.linkcom.sined.modulo.pub.controller.process.bean;



public class ConsultaDadosBean extends GenericEnvioInformacaoBean {
	
	private Integer cdempresa;

	public Integer getCdempresa() {
		return cdempresa;
	}
	
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	
}
