package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class FinalizarContratoBean extends GenericEnvioInformacaoBean {
	
	private Integer contrato;

	public Integer getContrato() {
		return contrato;
	}

	public void setContrato(Integer contrato) {
		this.contrato = contrato;
	}
}
