package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.util.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.bean.GenericBean;

public class VendaPagamentoRepresentacaoWSBean {

	protected Integer cdvendapagamentorepresentacao;
	protected GenericBean venda;
	protected GenericBean documentotipo;
	protected GenericBean documento;
	protected Date dataparcela;
	protected Money valororiginal;
	protected GenericBean fornecedor;
	
	
	public Integer getCdvendapagamentorepresentacao() {
		return cdvendapagamentorepresentacao;
	}
	public void setCdvendapagamentorepresentacao(
			Integer cdvendapagamentorepresentacao) {
		this.cdvendapagamentorepresentacao = cdvendapagamentorepresentacao;
	}
	public GenericBean getVenda() {
		return venda;
	}
	public void setVenda(GenericBean venda) {
		this.venda = venda;
	}
	public GenericBean getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(GenericBean documentotipo) {
		this.documentotipo = documentotipo;
	}
	public GenericBean getDocumento() {
		return documento;
	}
	public void setDocumento(GenericBean documento) {
		this.documento = documento;
	}
	public Date getDataparcela() {
		return dataparcela;
	}
	public void setDataparcela(Date dataparcela) {
		this.dataparcela = dataparcela;
	}
	public Money getValororiginal() {
		return valororiginal;
	}
	public void setValororiginal(Money valororiginal) {
		this.valororiginal = valororiginal;
	}
	public GenericBean getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(GenericBean fornecedor) {
		this.fornecedor = fornecedor;
	}
}