package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class GenericEnvioInformacaoBean {

	private String hash;
	private String format;
	
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
	
}
