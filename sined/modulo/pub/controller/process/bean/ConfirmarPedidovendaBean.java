package br.com.linkcom.sined.modulo.pub.controller.process.bean;


import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Pedidovenda;



public class ConfirmarPedidovendaBean extends GenericEnvioInformacaoBean {
	
	private Integer id;
	private ListSet<ConfirmarPedidovendaItemBean> listaItens = new ListSet<ConfirmarPedidovendaItemBean>(ConfirmarPedidovendaItemBean.class);
	private ListSet<Pedidovenda> listaPedidovenda = new ListSet<Pedidovenda>(Pedidovenda.class);
	
	public ListSet<ConfirmarPedidovendaItemBean> getListaItens() {
		return listaItens;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setListaItens(ListSet<ConfirmarPedidovendaItemBean> listaItens) {
		this.listaItens = listaItens;
	}

	public ListSet<Pedidovenda> getListaPedidovenda() {
		return listaPedidovenda;
	}

	public void setListaPedidovenda(ListSet<Pedidovenda> listaPedidovenda) {
		this.listaPedidovenda = listaPedidovenda;
	}
}
