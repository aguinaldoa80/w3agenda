package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.bean.GenericBean;

public class ProducaoOrdemItemAdicionalWSBean {

	protected Integer cdproducaoordemitemadicional; 
	protected GenericBean producaoordem;
	protected GenericBean material;
	protected GenericBean localarmazenagem;
	protected Double quantidade;
	protected GenericBean pneu; 
	
	// TRANSIENTE
	protected Double quantidadeDisponivel;
	protected Double valorvenda;
	protected Money valortotal;
	
	
	public Integer getCdproducaoordemitemadicional() {
		return cdproducaoordemitemadicional;
	}
	public void setCdproducaoordemitemadicional(Integer cdproducaoordemitemadicional) {
		this.cdproducaoordemitemadicional = cdproducaoordemitemadicional;
	}
	public GenericBean getProducaoordem() {
		return producaoordem;
	}
	public void setProducaoordem(GenericBean producaoordem) {
		this.producaoordem = producaoordem;
	}
	public GenericBean getMaterial() {
		return material;
	}
	public void setMaterial(GenericBean material) {
		this.material = material;
	}
	public GenericBean getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(GenericBean localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public GenericBean getPneu() {
		return pneu;
	}
	public void setPneu(GenericBean pneu) {
		this.pneu = pneu;
	}
	public Double getQuantidadeDisponivel() {
		return quantidadeDisponivel;
	}
	public void setQuantidadeDisponivel(Double quantidadeDisponivel) {
		this.quantidadeDisponivel = quantidadeDisponivel;
	}
	public Double getValorvenda() {
		return valorvenda;
	}
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	public Money getValortotal() {
		return valortotal;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
}
