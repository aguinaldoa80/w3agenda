package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class CriarOSBean extends GenericEnvioInformacaoBean {
	
	private String cnpj;
	private String cpf;
	private Integer cdprojeto;
	private Integer cdatividadetipo;
	private String valor;
	
	
	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public Integer getCdatividadetipo() {
		return cdatividadetipo;
	}
	public String getValor() {
		return valor;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	public void setCdatividadetipo(Integer cdatividadetipo) {
		this.cdatividadetipo = cdatividadetipo;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}
