package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.types.Money;

public class CriarContratoBean extends GenericEnvioInformacaoBean {
	
	private Integer contatipo;
	private Integer conta;
	private Integer contacarteira;
	private Integer documentotipo;
	private Integer contagerencial;
	private Integer periodicidade;
	private Integer cliente;
	private Integer identificador;
	private String descricao;
	private Money valor;
	
	public Integer getCliente() {
		return cliente;
	}
	public Money getValor() {
		return valor;
	}
	public String getDescricao() {
		return descricao;
	}
	public Integer getIdentificador() {
		return identificador;
	}
	public Integer getContatipo() {
		return contatipo;
	}
	public Integer getConta() {
		return conta;
	}
	public Integer getContacarteira() {
		return contacarteira;
	}
	public Integer getDocumentotipo() {
		return documentotipo;
	}
	public Integer getContagerencial() {
		return contagerencial;
	}
	public Integer getPeriodicidade() {
		return periodicidade;
	}
	public void setContatipo(Integer contatipo) {
		this.contatipo = contatipo;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public void setContacarteira(Integer contacarteira) {
		this.contacarteira = contacarteira;
	}
	public void setDocumentotipo(Integer documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setContagerencial(Integer contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setPeriodicidade(Integer periodicidade) {
		this.periodicidade = periodicidade;
	}
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
}
