package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class CriarFaturaClienteBean extends CriarFaturaBean {

	private String cliente_nome;
	private String cliente_email;
	private Integer cliente_cdcategoria;
	private Boolean substituircategoria = Boolean.FALSE;
	
	// FISICA
	private String cliente_sexo;
	private String cliente_cpf;
	
	// JURIDICA
	private String cliente_cnpj;
	private String cliente_razaosocial;
	private String cliente_inscricaomunicipal;
	private String cliente_inscricaoestadual;
	private String cliente_site;
	
	// CONTATO
	private String cliente_nomecontato;
	private String cliente_telefonecontato;
	private String cliente_celularcontato;
	
	// TELEFONES
	private String cliente_telefone;
	private String cliente_celular;
	
	// ENDERECO
	private String cliente_enderecologradouro;
	private String cliente_endereconumero;
	private String cliente_enderecocomplemento;
	private String cliente_enderecobairro;
	private String cliente_enderecocep;
	private String cliente_enderecomunicipio;
	private String cliente_enderecouf;
	
	public String getCliente_nome() {
		return cliente_nome;
	}
	public String getCliente_email() {
		return cliente_email;
	}
	public Integer getCliente_cdcategoria() {
		return cliente_cdcategoria;
	}
	public Boolean getSubstituircategoria() {
		return substituircategoria;
	}
	public void setSubstituircategoria(Boolean substituircategoria) {
		this.substituircategoria = substituircategoria;
	}
	public String getCliente_cpf() {
		return cliente_cpf;
	}
	public String getCliente_sexo() {
		return cliente_sexo;
	}
	public String getCliente_razaosocial() {
		return cliente_razaosocial;
	}
	public String getCliente_cnpj() {
		return cliente_cnpj;
	}
	public String getCliente_inscricaomunicipal() {
		return cliente_inscricaomunicipal;
	}
	public String getCliente_inscricaoestadual() {
		return cliente_inscricaoestadual;
	}
	public String getCliente_site() {
		return cliente_site;
	}
	public String getCliente_nomecontato() {
		return cliente_nomecontato;
	}
	public String getCliente_telefonecontato() {
		return cliente_telefonecontato;
	}
	public String getCliente_celularcontato() {
		return cliente_celularcontato;
	}
	public String getCliente_telefone() {
		return cliente_telefone;
	}
	public String getCliente_celular() {
		return cliente_celular;
	}
	public String getCliente_enderecologradouro() {
		return cliente_enderecologradouro;
	}
	public String getCliente_endereconumero() {
		return cliente_endereconumero;
	}
	public String getCliente_enderecocomplemento() {
		return cliente_enderecocomplemento;
	}
	public String getCliente_enderecobairro() {
		return cliente_enderecobairro;
	}
	public String getCliente_enderecocep() {
		return cliente_enderecocep;
	}
	public String getCliente_enderecomunicipio() {
		return cliente_enderecomunicipio;
	}
	public String getCliente_enderecouf() {
		return cliente_enderecouf;
	}
	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}
	public void setCliente_email(String clienteEmail) {
		cliente_email = clienteEmail;
	}
	public void setCliente_cdcategoria(Integer clienteCdcategoria) {
		cliente_cdcategoria = clienteCdcategoria;
	}
	public void setCliente_cpf(String clienteCpf) {
		cliente_cpf = clienteCpf;
	}
	public void setCliente_sexo(String clienteSexo) {
		cliente_sexo = clienteSexo;
	}
	public void setCliente_razaosocial(String clienteRazaosocial) {
		cliente_razaosocial = clienteRazaosocial;
	}
	public void setCliente_cnpj(String clienteCnpj) {
		cliente_cnpj = clienteCnpj;
	}
	public void setCliente_inscricaomunicipal(String clienteInscricaomunicipal) {
		cliente_inscricaomunicipal = clienteInscricaomunicipal;
	}
	public void setCliente_inscricaoestadual(String clienteInscricaoestadual) {
		cliente_inscricaoestadual = clienteInscricaoestadual;
	}
	public void setCliente_site(String clienteSite) {
		cliente_site = clienteSite;
	}
	public void setCliente_nomecontato(String clienteNomecontato) {
		cliente_nomecontato = clienteNomecontato;
	}
	public void setCliente_telefonecontato(String clienteTelefonecontato) {
		cliente_telefonecontato = clienteTelefonecontato;
	}
	public void setCliente_celularcontato(String clienteCelularcontato) {
		cliente_celularcontato = clienteCelularcontato;
	}
	public void setCliente_telefone(String clienteTelefone) {
		cliente_telefone = clienteTelefone;
	}
	public void setCliente_celular(String clienteCelular) {
		cliente_celular = clienteCelular;
	}
	public void setCliente_enderecologradouro(String clienteEnderecologradouro) {
		cliente_enderecologradouro = clienteEnderecologradouro;
	}
	public void setCliente_endereconumero(String clienteEndereconumero) {
		cliente_endereconumero = clienteEndereconumero;
	}
	public void setCliente_enderecocomplemento(String clienteEnderecocomplemento) {
		cliente_enderecocomplemento = clienteEnderecocomplemento;
	}
	public void setCliente_enderecobairro(String clienteEnderecobairro) {
		cliente_enderecobairro = clienteEnderecobairro;
	}
	public void setCliente_enderecocep(String clienteEnderecocep) {
		cliente_enderecocep = clienteEnderecocep;
	}
	public void setCliente_enderecomunicipio(String clienteEnderecomunicipio) {
		cliente_enderecomunicipio = clienteEnderecomunicipio;
	}
	public void setCliente_enderecouf(String clienteEnderecouf) {
		cliente_enderecouf = clienteEnderecouf;
	}
	
}
