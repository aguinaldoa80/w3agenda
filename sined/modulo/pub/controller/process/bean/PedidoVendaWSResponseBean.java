package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaWSResponseBean {

	protected Integer cdpedidovenda;
	protected String identificador;
	protected String identificacaoExterna;
	protected GenericBean pedidoVendaTipo;
	protected GenericBean empresa;
	protected GenericBean cliente;
	protected GenericBean contato;
	protected Money valorusadovalecompra;
	protected Money valoraproximadoimposto;
	protected GenericBean colaborador;
	protected Date dtpedidovenda = SinedDateUtils.currentDate();
	protected GenericBean endereco;
	protected String observacao;
	protected String observacaointerna;
	protected TranslateEnumBean presencacompradornfe;
	protected TranslateEnumBean pedidovendasituacao;
	protected GenericBean prazopagamento;
	protected GenericBean indicecorrecao;
	protected GenericBean conta;
	protected GenericBean documentotipo;
	protected List<PedidoVendaPagamentoWSBean> listaPedidovendapagamento = new ListSet<PedidoVendaPagamentoWSBean>(PedidoVendaPagamentoWSBean.class);
	protected List<PedidoVendaNegociacaoWSBean> listaPedidoVendaNegociacao = new ListSet<PedidoVendaNegociacaoWSBean>(PedidoVendaNegociacaoWSBean.class);
	protected List<PedidoVendaMaterialWSBean> listaPedidovendamaterial = new ListSet<PedidoVendaMaterialWSBean>(PedidoVendaMaterialWSBean.class);
	protected List<PedidoVendaHistoricoWSBean> listaPedidovendahistorico = new ListSet<PedidoVendaHistoricoWSBean>(PedidoVendaHistoricoWSBean.class);
	//protected Set<Documentoorigem> listaDocumentoorigem = new ListSet<Documentoorigem>(Documentoorigem.class);
	protected List<PedidoVendaValorCampoExtraWSBean> listaPedidovendavalorcampoextra = new ListSet<PedidoVendaValorCampoExtraWSBean>(PedidoVendaValorCampoExtraWSBean.class);
	protected List<PedidoVendaMaterialMestreWSBean> listaPedidovendamaterialmestre = new ListSet<PedidoVendaMaterialMestreWSBean>(PedidoVendaMaterialMestreWSBean.class);
	protected List<GenericBean> listaColeta = new ListSet<GenericBean>(GenericBean.class);
	//protected List<Venda> listaVenda = new ListSet<Venda>(Venda.class);
	protected List<PedidoVendaPagamentoRepresentacaoWSBean> listaPedidovendapagamentorepresentacao = new ListSet<PedidoVendaPagamentoRepresentacaoWSBean>(PedidoVendaPagamentoRepresentacaoWSBean.class);
	;;protected List<GenericBean> listaProducaoagenda = new ListSet<GenericBean>(GenericBean.class);
	protected GenericBean frete;
	protected GenericBean terceiro;
	protected Money valorfrete;
	protected GenericBean projeto;
	protected Boolean ordemcompra;
	protected GenericBean localarmazenagem;
	protected Money desconto;
	protected Double percentualdesconto;
	protected Money valorfinal;
	protected Money valorfinalipi;
	protected Boolean restricaocliente;
	protected Money taxapedidovenda;
	protected GenericBean vendaorcamento;
	protected GenericBean pedidovendaorigem;
	protected GenericBean fornecedor;
	protected Boolean integrar;
	protected Boolean origemwebservice;
	protected GenericBean clienteindicacao;
	protected Money valordescontorepresentacao;
	protected Money valorbrutorepresentacao;
	protected GenericBean prazopagamentorepresentacao;
	protected GenericBean documentotiporepresentacao;
	protected GenericBean contaboletorepresentacao;
	protected GenericBean agencia;
	protected Integer identificadorcarregamento;
	protected String whereInOSV;
	protected Date dtprazoentregamax;
	protected Date prazoentregaMinimo;
	protected Boolean limitecreditoexcedido;
	protected GenericBean centrocusto;
	protected TranslateEnumBean situacaoPendencia;
	protected Money valorFreteCIF;
	protected String observacaoPedidoVendaTipo;
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getIdentificacaoExterna() {
		return identificacaoExterna;
	}
	public void setIdentificacaoExterna(String identificacaoExterna) {
		this.identificacaoExterna = identificacaoExterna;
	}
	public GenericBean getPedidoVendaTipo() {
		return pedidoVendaTipo;
	}
	public void setPedidoVendaTipo(GenericBean pedidoVendaTipo) {
		this.pedidoVendaTipo = pedidoVendaTipo;
	}
	public GenericBean getEmpresa() {
		return empresa;
	}
	public void setEmpresa(GenericBean empresa) {
		this.empresa = empresa;
	}
	public GenericBean getCliente() {
		return cliente;
	}
	public void setCliente(GenericBean cliente) {
		this.cliente = cliente;
	}
	public GenericBean getContato() {
		return contato;
	}
	public void setContato(GenericBean contato) {
		this.contato = contato;
	}
	public Money getValorusadovalecompra() {
		return valorusadovalecompra;
	}
	public void setValorusadovalecompra(Money valorusadovalecompra) {
		this.valorusadovalecompra = valorusadovalecompra;
	}
	public Money getValoraproximadoimposto() {
		return valoraproximadoimposto;
	}
	public void setValoraproximadoimposto(Money valoraproximadoimposto) {
		this.valoraproximadoimposto = valoraproximadoimposto;
	}
	public GenericBean getColaborador() {
		return colaborador;
	}
	public void setColaborador(GenericBean colaborador) {
		this.colaborador = colaborador;
	}
	public Date getDtpedidovenda() {
		return dtpedidovenda;
	}
	public void setDtpedidovenda(Date dtpedidovenda) {
		this.dtpedidovenda = dtpedidovenda;
	}
	public GenericBean getEndereco() {
		return endereco;
	}
	public void setEndereco(GenericBean endereco) {
		this.endereco = endereco;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getObservacaointerna() {
		return observacaointerna;
	}
	public void setObservacaointerna(String observacaointerna) {
		this.observacaointerna = observacaointerna;
	}
	public TranslateEnumBean getPresencacompradornfe() {
		return presencacompradornfe;
	}
	public void setPresencacompradornfe(TranslateEnumBean presencacompradornfe) {
		this.presencacompradornfe = presencacompradornfe;
	}
	public TranslateEnumBean getPedidovendasituacao() {
		return pedidovendasituacao;
	}
	public void setPedidovendasituacao(TranslateEnumBean pedidovendasituacao) {
		this.pedidovendasituacao = pedidovendasituacao;
	}
	public GenericBean getPrazopagamento() {
		return prazopagamento;
	}
	public void setPrazopagamento(GenericBean prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public GenericBean getIndicecorrecao() {
		return indicecorrecao;
	}
	public void setIndicecorrecao(GenericBean indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}
	public GenericBean getConta() {
		return conta;
	}
	public void setConta(GenericBean conta) {
		this.conta = conta;
	}
	public GenericBean getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(GenericBean documentotipo) {
		this.documentotipo = documentotipo;
	}
	public List<PedidoVendaPagamentoWSBean> getListaPedidovendapagamento() {
		return listaPedidovendapagamento;
	}
	public void setListaPedidovendapagamento(
			List<PedidoVendaPagamentoWSBean> listaPedidovendapagamento) {
		this.listaPedidovendapagamento = listaPedidovendapagamento;
	}
	public List<PedidoVendaNegociacaoWSBean> getListaPedidoVendaNegociacao() {
		return listaPedidoVendaNegociacao;
	}
	public void setListaPedidoVendaNegociacao(
			List<PedidoVendaNegociacaoWSBean> listaPedidoVendaNegociacao) {
		this.listaPedidoVendaNegociacao = listaPedidoVendaNegociacao;
	}
	public List<PedidoVendaMaterialWSBean> getListaPedidovendamaterial() {
		return listaPedidovendamaterial;
	}
	public void setListaPedidovendamaterial(
			List<PedidoVendaMaterialWSBean> listaPedidovendamaterial) {
		this.listaPedidovendamaterial = listaPedidovendamaterial;
	}
	public List<PedidoVendaHistoricoWSBean> getListaPedidovendahistorico() {
		return listaPedidovendahistorico;
	}
	public void setListaPedidovendahistorico(
			List<PedidoVendaHistoricoWSBean> listaPedidovendahistorico) {
		this.listaPedidovendahistorico = listaPedidovendahistorico;
	}
	public List<PedidoVendaValorCampoExtraWSBean> getListaPedidovendavalorcampoextra() {
		return listaPedidovendavalorcampoextra;
	}
	public void setListaPedidovendavalorcampoextra(
			List<PedidoVendaValorCampoExtraWSBean> listaPedidovendavalorcampoextra) {
		this.listaPedidovendavalorcampoextra = listaPedidovendavalorcampoextra;
	}
	public List<PedidoVendaMaterialMestreWSBean> getListaPedidovendamaterialmestre() {
		return listaPedidovendamaterialmestre;
	}
	public void setListaPedidovendamaterialmestre(
			List<PedidoVendaMaterialMestreWSBean> listaPedidovendamaterialmestre) {
		this.listaPedidovendamaterialmestre = listaPedidovendamaterialmestre;
	}
	public List<GenericBean> getListaColeta() {
		return listaColeta;
	}
	public void setListaColeta(List<GenericBean> listaColeta) {
		this.listaColeta = listaColeta;
	}
	public List<PedidoVendaPagamentoRepresentacaoWSBean> getListaPedidovendapagamentorepresentacao() {
		return listaPedidovendapagamentorepresentacao;
	}
	public void setListaPedidovendapagamentorepresentacao(
			List<PedidoVendaPagamentoRepresentacaoWSBean> listaPedidovendapagamentorepresentacao) {
		this.listaPedidovendapagamentorepresentacao = listaPedidovendapagamentorepresentacao;
	}
/*	public List<GenericBean> getListaProducaoagenda() {
		return listaProducaoagenda;
	}
	public void setListaProducaoagenda(List<GenericBean> listaProducaoagenda) {
		this.listaProducaoagenda = listaProducaoagenda;
	}*/
	public GenericBean getFrete() {
		return frete;
	}
	public void setFrete(GenericBean frete) {
		this.frete = frete;
	}
	public GenericBean getTerceiro() {
		return terceiro;
	}
	public void setTerceiro(GenericBean terceiro) {
		this.terceiro = terceiro;
	}
	public Money getValorfrete() {
		return valorfrete;
	}
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	public GenericBean getProjeto() {
		return projeto;
	}
	public void setProjeto(GenericBean projeto) {
		this.projeto = projeto;
	}
	public Boolean getOrdemcompra() {
		return ordemcompra;
	}
	public void setOrdemcompra(Boolean ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	public GenericBean getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(GenericBean localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	public Money getValorfinal() {
		return valorfinal;
	}
	public void setValorfinal(Money valorfinal) {
		this.valorfinal = valorfinal;
	}
	public Money getValorfinalipi() {
		return valorfinalipi;
	}
	public void setValorfinalipi(Money valorfinalipi) {
		this.valorfinalipi = valorfinalipi;
	}
	public Boolean getRestricaocliente() {
		return restricaocliente;
	}
	public void setRestricaocliente(Boolean restricaocliente) {
		this.restricaocliente = restricaocliente;
	}
	public Money getTaxapedidovenda() {
		return taxapedidovenda;
	}
	public void setTaxapedidovenda(Money taxapedidovenda) {
		this.taxapedidovenda = taxapedidovenda;
	}
	public GenericBean getVendaorcamento() {
		return vendaorcamento;
	}
	public void setVendaorcamento(GenericBean vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}
	public GenericBean getPedidovendaorigem() {
		return pedidovendaorigem;
	}
	public void setPedidovendaorigem(GenericBean pedidovendaorigem) {
		this.pedidovendaorigem = pedidovendaorigem;
	}
	public GenericBean getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(GenericBean fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Boolean getIntegrar() {
		return integrar;
	}
	public void setIntegrar(Boolean integrar) {
		this.integrar = integrar;
	}
	public Boolean getOrigemwebservice() {
		return origemwebservice;
	}
	public void setOrigemwebservice(Boolean origemwebservice) {
		this.origemwebservice = origemwebservice;
	}
	public GenericBean getClienteindicacao() {
		return clienteindicacao;
	}
	public void setClienteindicacao(GenericBean clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}
	public Money getValordescontorepresentacao() {
		return valordescontorepresentacao;
	}
	public void setValordescontorepresentacao(Money valordescontorepresentacao) {
		this.valordescontorepresentacao = valordescontorepresentacao;
	}
	public Money getValorbrutorepresentacao() {
		return valorbrutorepresentacao;
	}
	public void setValorbrutorepresentacao(Money valorbrutorepresentacao) {
		this.valorbrutorepresentacao = valorbrutorepresentacao;
	}
	public GenericBean getPrazopagamentorepresentacao() {
		return prazopagamentorepresentacao;
	}
	public void setPrazopagamentorepresentacao(
			GenericBean prazopagamentorepresentacao) {
		this.prazopagamentorepresentacao = prazopagamentorepresentacao;
	}
	public GenericBean getDocumentotiporepresentacao() {
		return documentotiporepresentacao;
	}
	public void setDocumentotiporepresentacao(GenericBean documentotiporepresentacao) {
		this.documentotiporepresentacao = documentotiporepresentacao;
	}
	public GenericBean getContaboletorepresentacao() {
		return contaboletorepresentacao;
	}
	public void setContaboletorepresentacao(GenericBean contaboletorepresentacao) {
		this.contaboletorepresentacao = contaboletorepresentacao;
	}
	public GenericBean getAgencia() {
		return agencia;
	}
	public void setAgencia(GenericBean agencia) {
		this.agencia = agencia;
	}
	public Integer getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}
	public void setIdentificadorcarregamento(Integer identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}
	public String getWhereInOSV() {
		return whereInOSV;
	}
	public void setWhereInOSV(String whereInOSV) {
		this.whereInOSV = whereInOSV;
	}
	public Date getDtprazoentregamax() {
		return dtprazoentregamax;
	}
	public void setDtprazoentregamax(Date dtprazoentregamax) {
		this.dtprazoentregamax = dtprazoentregamax;
	}
	public Date getPrazoentregaMinimo() {
		return prazoentregaMinimo;
	}
	public void setPrazoentregaMinimo(Date prazoentregaMinimo) {
		this.prazoentregaMinimo = prazoentregaMinimo;
	}
	public Boolean getLimitecreditoexcedido() {
		return limitecreditoexcedido;
	}
	public void setLimitecreditoexcedido(Boolean limitecreditoexcedido) {
		this.limitecreditoexcedido = limitecreditoexcedido;
	}
	public GenericBean getCentrocusto() {
		return centrocusto;
	}
	public void setCentrocusto(GenericBean centrocusto) {
		this.centrocusto = centrocusto;
	}
	public TranslateEnumBean getSituacaoPendencia() {
		return situacaoPendencia;
	}
	public void setSituacaoPendencia(TranslateEnumBean situacaoPendencia) {
		this.situacaoPendencia = situacaoPendencia;
	}
	public Money getValorFreteCIF() {
		return valorFreteCIF;
	}
	public void setValorFreteCIF(Money valorFreteCIF) {
		this.valorFreteCIF = valorFreteCIF;
	}
	public String getObservacaoPedidoVendaTipo() {
		return observacaoPedidoVendaTipo;
	}
	public void setObservacaoPedidoVendaTipo(String observacaoPedidoVendaTipo) {
		this.observacaoPedidoVendaTipo = observacaoPedidoVendaTipo;
	}
}
