package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.types.Money;


public class InserirFaturamentoBean extends GenericEnvioInformacaoBean {
	
	private String cnpjcliente;
	private String cpfcliente;
	private Money valor;
	private Integer cdprojeto;
	private Integer cdcentrocusto;
	private Integer cdcontagerencial;
	private Integer cddocumentotipo;
	private Integer cdempresa;
	private String descricao;
	private Integer cdconta;
	private String codigocnae;
	private String itemlistaservico;
	private String emailremetenteboleto;
	private String nomearquivoboleto;
	private String informacoescedenteboleto;
	
	public String getCnpjcliente() {
		return cnpjcliente;
	}
	public Money getValor() {
		return valor;
	}
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public Integer getCdempresa() {
		return cdempresa;
	}
	public String getDescricao() {
		return descricao;
	}
	public Integer getCdconta() {
		return cdconta;
	}
	public String getCodigocnae() {
		return codigocnae;
	}
	public String getItemlistaservico() {
		return itemlistaservico;
	}
	public String getCpfcliente() {
		return cpfcliente;
	}
	public String getEmailremetenteboleto() {
		return emailremetenteboleto;
	}
	public String getNomearquivoboleto() {
		return nomearquivoboleto;
	}
	public String getInformacoescedenteboleto() {
		return informacoescedenteboleto;
	}
	public void setInformacoescedenteboleto(String informacoescedenteboleto) {
		this.informacoescedenteboleto = informacoescedenteboleto;
	}
	public void setNomearquivoboleto(String nomearquivoboleto) {
		this.nomearquivoboleto = nomearquivoboleto;
	}
	public void setEmailremetenteboleto(String emailremetenteboleto) {
		this.emailremetenteboleto = emailremetenteboleto;
	}
	public void setCpfcliente(String cpfcliente) {
		this.cpfcliente = cpfcliente;
	}
	public void setCnpjcliente(String cnpjcliente) {
		this.cnpjcliente = cnpjcliente;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
	public void setCodigocnae(String codigocnae) {
		this.codigocnae = codigocnae;
	}
	public void setItemlistaservico(String itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}
	
}
