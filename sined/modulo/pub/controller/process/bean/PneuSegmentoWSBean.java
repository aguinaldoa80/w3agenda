package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.util.bean.GenericBean;

public class PneuSegmentoWSBean extends GenericBean{

	protected boolean tipoPneuOtr;
	protected boolean marca;
	protected boolean descricao;
	protected boolean lonas;
	protected boolean modelo;
	protected boolean medida;
	protected boolean qualificacaoPneu;
	protected boolean serieFogo;
	protected boolean dot;
	protected boolean numeroReformas;
	protected boolean bandaCamelbak;
	protected boolean ativo;
	protected boolean principal;
	protected boolean otr;
	
	public PneuSegmentoWSBean(){
		
	}
	
	public PneuSegmentoWSBean(PneuSegmento pneuSegmento){
		this.id = Util.strings.toStringIdStyled(pneuSegmento);
		this.value = Util.strings.toStringDescription(pneuSegmento);
		this.tipoPneuOtr = pneuSegmento.getTipoPneuOtr() != null && Boolean.TRUE.equals(pneuSegmento.getTipoPneuOtr());
		this.marca = Boolean.TRUE.equals(pneuSegmento.getMarca());
		this.descricao = Boolean.TRUE.equals(pneuSegmento.getDescricao());
		this.lonas = Boolean.TRUE.equals(pneuSegmento.getLonas());
		this.modelo = Boolean.TRUE.equals(pneuSegmento.getModelo());
		this.medida = Boolean.TRUE.equals(pneuSegmento.getMedida());
		this.qualificacaoPneu = Boolean.TRUE.equals(pneuSegmento.getQualificacaoPneu());
		this.serieFogo = Boolean.TRUE.equals(pneuSegmento.getSerieFogo());
		this.dot = Boolean.TRUE.equals(pneuSegmento.getDot());
		this.numeroReformas = Boolean.TRUE.equals(pneuSegmento.getNumeroReformas());
		this.bandaCamelbak = Boolean.TRUE.equals(pneuSegmento.getBandaCamelbak());
		this.ativo = Boolean.TRUE.equals(pneuSegmento.getAtivo());
		this.principal = Boolean.TRUE.equals(pneuSegmento.getPrincipal());
		this.otr = Boolean.TRUE.equals(pneuSegmento.getOtr());
	}
	
	
	public boolean isTipoPneuOtr() {
		return tipoPneuOtr;
	}
	public void setTipoPneuOtr(boolean tipoPneuOtr) {
		this.tipoPneuOtr = tipoPneuOtr;
	}
	public boolean isMarca() {
		return marca;
	}
	public void setMarca(boolean marca) {
		this.marca = marca;
	}
	public boolean isDescricao() {
		return descricao;
	}
	public void setDescricao(boolean descricao) {
		this.descricao = descricao;
	}
	public boolean isLonas() {
		return lonas;
	}
	public void setLonas(boolean lonas) {
		this.lonas = lonas;
	}
	public boolean isModelo() {
		return modelo;
	}
	public void setModelo(boolean modelo) {
		this.modelo = modelo;
	}
	public boolean isMedida() {
		return medida;
	}
	public void setMedida(boolean medida) {
		this.medida = medida;
	}
	public boolean isQualificacaoPneu() {
		return qualificacaoPneu;
	}
	public void setQualificacaoPneu(boolean qualificacaoPneu) {
		this.qualificacaoPneu = qualificacaoPneu;
	}
	public boolean isSerieFogo() {
		return serieFogo;
	}
	public void setSerieFogo(boolean serieFogo) {
		this.serieFogo = serieFogo;
	}
	public boolean isDot() {
		return dot;
	}
	public void setDot(boolean dot) {
		this.dot = dot;
	}
	public boolean isNumeroReformas() {
		return numeroReformas;
	}
	public void setNumeroReformas(boolean numeroReformas) {
		this.numeroReformas = numeroReformas;
	}
	public boolean isBandaCamelbak() {
		return bandaCamelbak;
	}
	public void setBandaCamelbak(boolean bandaCamelbak) {
		this.bandaCamelbak = bandaCamelbak;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public boolean isPrincipal() {
		return principal;
	}
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	public boolean isOtr() {
		return otr;
	}
	public void setOtr(boolean otr) {
		this.otr = otr;
	}
}
