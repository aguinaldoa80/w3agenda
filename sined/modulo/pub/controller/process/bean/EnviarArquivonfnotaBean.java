package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;


public class EnviarArquivonfnotaBean extends GenericEnvioInformacaoBean {
	
	private Integer cdarquivonfnota;
	private Arquivo arquivo;
	
	public Integer getCdarquivonfnota() {
		return cdarquivonfnota;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setCdarquivonfnota(Integer cdarquivonfnota) {
		this.cdarquivonfnota = cdarquivonfnota;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
}
