package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class MarcarInutilizacaonfeBean extends GenericEnvioInformacaoBean {
	
	private Integer cdinutilizacaonfe;
	
	public Integer getCdinutilizacaonfe() {
		return cdinutilizacaonfe;
	}
	public void setCdinutilizacaonfe(Integer cdinutilizacaonfe) {
		this.cdinutilizacaonfe = cdinutilizacaonfe;
	}
	
}
