package br.com.linkcom.sined.modulo.pub.controller.process.bean;



public class ConfirmarPedidovendaItemBean {
	
	private Integer id;
	private Integer id_material;
	private String lote;
	private Double quantidade;
	private Double quantidade_volumes;
	private Double peso_unitario;
	
	public ConfirmarPedidovendaItemBean() {
	}
	
	public ConfirmarPedidovendaItemBean(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
	public Integer getId_material() {
		return id_material;
	}
	public String getLote() {
		return lote;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getQuantidade_volumes() {
		return quantidade_volumes;
	}
	public Double getPeso_unitario() {
		return peso_unitario;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setId_material(Integer idmaterial) {
		id_material = idmaterial;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setQuantidade_volumes(Double quantidadeVolumes) {
		quantidade_volumes = quantidadeVolumes;
	}
	public void setPeso_unitario(Double pesoUnitario) {
		peso_unitario = pesoUnitario;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfirmarPedidovendaItemBean other = (ConfirmarPedidovendaItemBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
