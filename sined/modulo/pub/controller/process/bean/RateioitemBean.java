package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class RateioitemBean {

	private Integer cdprojeto;
	private Integer cdcentrocusto;
	private Integer cdcontagerencial;
	private Double percentual;
	
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	public Double getPercentual() {
		return percentual;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	
}
