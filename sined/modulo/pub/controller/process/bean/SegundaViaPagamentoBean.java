package br.com.linkcom.sined.modulo.pub.controller.process.bean;



public class SegundaViaPagamentoBean extends GenericEnvioInformacaoBean {
	
	private Integer cddocumento;
	private String emailremetenteboleto;
	private String nomearquivoboleto;
	private String informacoescedenteboleto;
	
	public Integer getCddocumento() {
		return cddocumento;
	}
	public String getEmailremetenteboleto() {
		return emailremetenteboleto;
	}
	public String getNomearquivoboleto() {
		return nomearquivoboleto;
	}
	public String getInformacoescedenteboleto() {
		return informacoescedenteboleto;
	}
	public void setInformacoescedenteboleto(String informacoescedenteboleto) {
		this.informacoescedenteboleto = informacoescedenteboleto;
	}
	public void setNomearquivoboleto(String nomearquivoboleto) {
		this.nomearquivoboleto = nomearquivoboleto;
	}
	public void setEmailremetenteboleto(String emailremetenteboleto) {
		this.emailremetenteboleto = emailremetenteboleto;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
}
