package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Vendamaterialseparacao;
import br.com.linkcom.sined.util.bean.GenericBean;

public class VendaMaterialWSBean {

	protected Integer cdvendamaterial;
	protected Integer ordem;
	protected GenericBean venda;
	protected GenericBean material;
	protected GenericBean materialmestre;
	protected Double quantidade;
	protected Double comprimento;
	protected Double comprimentooriginal;
	protected Double largura;
	protected Double altura;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected Double preco;
	protected Money desconto;
	protected GenericBean unidademedida;
	protected GenericBean pedidovendamaterial;
	protected GenericBean vendaorcamentomaterial;
	protected Date prazoentrega;
	protected String observacao;
	protected GenericBean loteestoque;
	protected Double valorcustomaterial;
	protected Double saldo;
	protected Double multiplicador = 1.0;
	protected Boolean producaosemestoque;
	protected GenericBean fornecedor;
	protected Money percentualrepresentacao;
	protected Boolean opcional;
	protected GenericBean patrimonioitem;
	protected GenericBean materialcoleta;
	protected List<Vendamaterialseparacao> listaVendamaterialseparacao = new ListSet<Vendamaterialseparacao>(Vendamaterialseparacao.class);
	protected String identificadorespecifico;
	protected String identificadorinterno;
	protected Double custooperacional;
	protected Double percentualcomissaoagencia;
	protected PneuWSBean pneu;
	protected Double qtdevolumeswms;
	protected Money valorimposto;
	protected Double ipi;
	protected Money valoripi;
	protected TranslateEnumBean tipocobrancaipi;
	protected TranslateEnumBean tipocalculoipi;
	protected Money aliquotareaisipi;
	protected GenericBean grupotributacao;
	protected Integer cdproducaoordemitemadicional;
	protected Integer cdproducaoagendaitemadicional;
	protected Double peso;
	protected Integer identificadorintegracao;
	protected GenericBean comissionamento;
	protected Double pesomedio;
	protected Double percentualdesconto;
	protected Money descontogarantiareforma;
	protected GenericBean garantiareformaitem;
	protected Double valorMinimo;	
	protected GenericBean materialFaixaMarkup;
	protected GenericBean faixaMarkupNome;
	
	
	
	public Integer getCdvendamaterial() {
		return cdvendamaterial;
	}
	public void setCdvendamaterial(Integer cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public GenericBean getVenda() {
		return venda;
	}
	public void setVenda(GenericBean venda) {
		this.venda = venda;
	}
	public GenericBean getMaterial() {
		return material;
	}
	public void setMaterial(GenericBean material) {
		this.material = material;
	}
	public GenericBean getMaterialmestre() {
		return materialmestre;
	}
	public void setMaterialmestre(GenericBean materialmestre) {
		this.materialmestre = materialmestre;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public Double getComprimentooriginal() {
		return comprimentooriginal;
	}
	public void setComprimentooriginal(Double comprimentooriginal) {
		this.comprimentooriginal = comprimentooriginal;
	}
	public Double getLargura() {
		return largura;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public Double getAltura() {
		return altura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public GenericBean getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(GenericBean unidademedida) {
		this.unidademedida = unidademedida;
	}
	public GenericBean getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	public void setPedidovendamaterial(GenericBean pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public GenericBean getVendaorcamentomaterial() {
		return vendaorcamentomaterial;
	}
	public void setVendaorcamentomaterial(GenericBean vendaorcamentomaterial) {
		this.vendaorcamentomaterial = vendaorcamentomaterial;
	}
	public Date getPrazoentrega() {
		return prazoentrega;
	}
	public void setPrazoentrega(Date prazoentrega) {
		this.prazoentrega = prazoentrega;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public GenericBean getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(GenericBean loteestoque) {
		this.loteestoque = loteestoque;
	}
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Double getMultiplicador() {
		return multiplicador;
	}
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}
	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	public GenericBean getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(GenericBean fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Money getPercentualrepresentacao() {
		return percentualrepresentacao;
	}
	public void setPercentualrepresentacao(Money percentualrepresentacao) {
		this.percentualrepresentacao = percentualrepresentacao;
	}
	public Boolean getOpcional() {
		return opcional;
	}
	public void setOpcional(Boolean opcional) {
		this.opcional = opcional;
	}
	public GenericBean getPatrimonioitem() {
		return patrimonioitem;
	}
	public void setPatrimonioitem(GenericBean patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	public GenericBean getMaterialcoleta() {
		return materialcoleta;
	}
	public void setMaterialcoleta(GenericBean materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	public List<Vendamaterialseparacao> getListaVendamaterialseparacao() {
		return listaVendamaterialseparacao;
	}
	public void setListaVendamaterialseparacao(
			List<Vendamaterialseparacao> listaVendamaterialseparacao) {
		this.listaVendamaterialseparacao = listaVendamaterialseparacao;
	}
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	public Double getCustooperacional() {
		return custooperacional;
	}
	public void setCustooperacional(Double custooperacional) {
		this.custooperacional = custooperacional;
	}
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}
	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}
	public PneuWSBean getPneu() {
		return pneu;
	}
	public void setPneu(PneuWSBean pneu) {
		this.pneu = pneu;
	}
	public Double getQtdevolumeswms() {
		return qtdevolumeswms;
	}
	public void setQtdevolumeswms(Double qtdevolumeswms) {
		this.qtdevolumeswms = qtdevolumeswms;
	}
	public Money getValorimposto() {
		return valorimposto;
	}
	public void setValorimposto(Money valorimposto) {
		this.valorimposto = valorimposto;
	}
	public Double getIpi() {
		return ipi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public Money getValoripi() {
		return valoripi;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public TranslateEnumBean getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	public void setTipocobrancaipi(TranslateEnumBean tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}
	public TranslateEnumBean getTipocalculoipi() {
		return tipocalculoipi;
	}
	public void setTipocalculoipi(TranslateEnumBean tipocalculoipi) {
		this.tipocalculoipi = tipocalculoipi;
	}
	public Money getAliquotareaisipi() {
		return aliquotareaisipi;
	}
	public void setAliquotareaisipi(Money aliquotareaisipi) {
		this.aliquotareaisipi = aliquotareaisipi;
	}
	public GenericBean getGrupotributacao() {
		return grupotributacao;
	}
	public void setGrupotributacao(GenericBean grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public Integer getCdproducaoordemitemadicional() {
		return cdproducaoordemitemadicional;
	}
	public void setCdproducaoordemitemadicional(Integer cdproducaoordemitemadicional) {
		this.cdproducaoordemitemadicional = cdproducaoordemitemadicional;
	}
	public Integer getCdproducaoagendaitemadicional() {
		return cdproducaoagendaitemadicional;
	}
	public void setCdproducaoagendaitemadicional(
			Integer cdproducaoagendaitemadicional) {
		this.cdproducaoagendaitemadicional = cdproducaoagendaitemadicional;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public Integer getIdentificadorintegracao() {
		return identificadorintegracao;
	}
	public void setIdentificadorintegracao(Integer identificadorintegracao) {
		this.identificadorintegracao = identificadorintegracao;
	}
	public GenericBean getComissionamento() {
		return comissionamento;
	}
	public void setComissionamento(GenericBean comissionamento) {
		this.comissionamento = comissionamento;
	}
	public Double getPesomedio() {
		return pesomedio;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	public Money getDescontogarantiareforma() {
		return descontogarantiareforma;
	}
	public void setDescontogarantiareforma(Money descontogarantiareforma) {
		this.descontogarantiareforma = descontogarantiareforma;
	}
	public GenericBean getGarantiareformaitem() {
		return garantiareformaitem;
	}
	public void setGarantiareformaitem(GenericBean garantiareformaitem) {
		this.garantiareformaitem = garantiareformaitem;
	}
	public Double getValorMinimo() {
		return valorMinimo;
	}
	public void setValorMinimo(Double valorMinimo) {
		this.valorMinimo = valorMinimo;
	}
	public GenericBean getMaterialFaixaMarkup() {
		return materialFaixaMarkup;
	}
	public void setMaterialFaixaMarkup(GenericBean materialFaixaMarkup) {
		this.materialFaixaMarkup = materialFaixaMarkup;
	}
	public GenericBean getFaixaMarkupNome() {
		return faixaMarkupNome;
	}
	public void setFaixaMarkupNome(GenericBean faixaMarkupNome) {
		this.faixaMarkupNome = faixaMarkupNome;
	}
}
