package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;

public class EnviarArquivoMdfeBean extends GenericEnvioInformacaoBean {

	private Integer cdarquivomdfe;
	private Arquivo arquivo;
	private String xml;
	
	public Integer getCdarquivomdfe() {
		return cdarquivomdfe;
	}
	
	public void setCdarquivomdfe(Integer cdarquivomdfe) {
		this.cdarquivomdfe = cdarquivomdfe;
	}
	
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public String getXml() {
		return xml;
	}
	
	public void setXml(String xml) {
		this.xml = xml;
	}
}
