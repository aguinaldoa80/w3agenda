package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class CancelarPedidovendaBean extends GenericEnvioInformacaoBean {
	
	private Integer tipo_criacao;
	private String id;
	private String empresa_cnpj;
	private Integer cdpedidovenda;
	
	
	public Integer getTipo_criacao() {
		return tipo_criacao;
	}
	public String getId() {
		return id;
	}
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}
	public void setEmpresa_cnpj(String empresaCnpj) {
		empresa_cnpj = empresaCnpj;
	}
	public void setTipo_criacao(Integer tipoCriacao) {
		tipo_criacao = tipoCriacao;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}

}
