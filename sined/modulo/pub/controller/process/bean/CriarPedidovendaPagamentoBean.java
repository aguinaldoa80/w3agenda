package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class CriarPedidovendaPagamentoBean {
	
	private Date datavencimento;
	private Money valor;
	private Integer cddocumento;
	private Integer cddocumentotipo;
	private Double taxa;
	
	public Date getDatavencimento() {
		return datavencimento;
	}
	public Money getValor() {
		return valor;
	}
	public Integer getCddocumento() {
		return cddocumento;
	}
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public Double getTaxa() {
		return taxa;
	}
	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public void setDatavencimento(Date datavencimento) {
		this.datavencimento = datavencimento;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
}
