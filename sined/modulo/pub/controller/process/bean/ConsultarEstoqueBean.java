package br.com.linkcom.sined.modulo.pub.controller.process.bean;




public class ConsultarEstoqueBean extends GenericEnvioInformacaoBean {
	
	private String cnpjDeposito;
	private Long dtReferenciaInMillis;
	private Integer cdusuario;
	private String whereInCdmaterial;
	private String qtdePorCdmaterial;
	private String cpfPorCdmaterial;
	private Boolean saldoForAtualizacaoERP;
	private Boolean ajusteAutomaticoERP;
	
	public Long getDtReferenciaInMillis() {
		return dtReferenciaInMillis;
	}
	public void setDtReferenciaInMillis(Long dtReferenciaInMillis) {
		this.dtReferenciaInMillis = dtReferenciaInMillis;
	}
	
	public String getCnpjDeposito() {
		return cnpjDeposito;
	}
	public void setCnpjDeposito(String cnpjDeposito) {
		this.cnpjDeposito = cnpjDeposito;
	}
	
	public Integer getCdusuario() {
		return cdusuario;
	}
	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}
	
	public String getWhereInCdmaterial() {
		return whereInCdmaterial;
	}
	public void setWhereInCdmaterial(String whereInCdmaterial) {
		this.whereInCdmaterial = whereInCdmaterial;
	}
	
	public String getQtdePorCdmaterial() {
		return qtdePorCdmaterial;
	}
	public void setQtdePorCdmaterial(String qtdePorCdmaterial) {
		this.qtdePorCdmaterial = qtdePorCdmaterial;
	}
	
	public String getCpfPorCdmaterial() {
		if(cpfPorCdmaterial == null){
			cpfPorCdmaterial = "";
		}
		return cpfPorCdmaterial;
	}
	public void setCpfPorCdmaterial(String cpfPorCdmaterial) {
		this.cpfPorCdmaterial = cpfPorCdmaterial;
	}
	
	public Boolean getSaldoForAtualizacaoERP() {
		return saldoForAtualizacaoERP;
	}
	public void setSaldoForAtualizacaoERP(Boolean saldoForAtualizacaoERP) {
		this.saldoForAtualizacaoERP = saldoForAtualizacaoERP;
	}
	
	public Boolean getAjusteAutomaticoERP() {
		return ajusteAutomaticoERP;
	}
	public void setAjusteAutomaticoERP(Boolean ajusteAutomaticoERP) {
		this.ajusteAutomaticoERP = ajusteAutomaticoERP;
	}
}
