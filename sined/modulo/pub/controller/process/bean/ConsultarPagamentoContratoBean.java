package br.com.linkcom.sined.modulo.pub.controller.process.bean;



public class ConsultarPagamentoContratoBean extends GenericEnvioInformacaoBean {
	
	private Integer cdcontrato;
	
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	
}
