package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class ConsultaContasAtrasadasClienteBean extends GenericEnvioInformacaoBean {
	
	private String cnpj;
	private String cpf;
	private Integer cdcliente;
	
	public String getCnpj() {
		return cnpj;
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Integer getCdcliente() {
		return cdcliente;
	}

	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
}
