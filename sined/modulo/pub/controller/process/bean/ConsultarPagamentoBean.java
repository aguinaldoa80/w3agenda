package br.com.linkcom.sined.modulo.pub.controller.process.bean;



public class ConsultarPagamentoBean extends GenericEnvioInformacaoBean {
	
	private Integer cddocumento;
	private Integer tipo_criacao;
	private String id;
	private String empresa_cnpj;
	
	public Integer getCddocumento() {
		return cddocumento;
	}
	public Integer getTipo_criacao() {
		return tipo_criacao;
	}
	public String getId() {
		return id;
	}
	public String getEmpresa_cnpj() {
		return empresa_cnpj;
	}
	public void setEmpresa_cnpj(String empresaCnpj) {
		empresa_cnpj = empresaCnpj;
	}
	public void setTipo_criacao(Integer tipoCriacao) {
		tipo_criacao = tipoCriacao;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
}
