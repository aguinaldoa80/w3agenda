package br.com.linkcom.sined.modulo.pub.controller.process.bean;


public class InserirCentrocustoBean extends GenericEnvioInformacaoBean {
	
	private String nome;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
