package br.com.linkcom.sined.modulo.pub.controller.process.bean;

public class CancelarPagamentoBean extends GenericEnvioInformacaoBean {
	
	private Integer cddocumento;
	private String motivo;
	
	
	public Integer getCddocumento() {
		return cddocumento;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
}
