package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.util.bean.GenericBean;

public class PedidoVendaMaterialMestreWSBean {

	private Integer cdpedidovendamaterialmestre;
    private Double altura;
    private Double largura;
    private Double comprimento;
    private Double peso; 
    private GenericBean pedidovenda;
    private GenericBean material;
    private Double qtde;
    private String identificadorinterno;
    protected Double preco;
	protected Money desconto;
	protected GenericBean unidademedida;
	protected Date dtprazoentrega;
	protected String identificadorespecifico;
	protected String nomealternativo;
	protected Boolean exibiritenskitflexivel;
	private Double saldo;
	private Money valorIpi;
	private Double ipi;
	private TranslateEnumBean tipoCobrancaIpi;
	private TranslateEnumBean tipoCalculoIpi;
	private Money aliquotaReaisIpi;
	private GenericBean grupoTributacao;
	
	
	public Integer getCdpedidovendamaterialmestre() {
		return cdpedidovendamaterialmestre;
	}
	public void setCdpedidovendamaterialmestre(Integer cdpedidovendamaterialmestre) {
		this.cdpedidovendamaterialmestre = cdpedidovendamaterialmestre;
	}
	public Double getAltura() {
		return altura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public Double getLargura() {
		return largura;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	public GenericBean getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(GenericBean pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public GenericBean getMaterial() {
		return material;
	}
	public void setMaterial(GenericBean material) {
		this.material = material;
	}
	public Double getQtde() {
		return qtde;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public GenericBean getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(GenericBean unidademedida) {
		this.unidademedida = unidademedida;
	}
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	public String getNomealternativo() {
		return nomealternativo;
	}
	public void setNomealternativo(String nomealternativo) {
		this.nomealternativo = nomealternativo;
	}
	public Boolean getExibiritenskitflexivel() {
		return exibiritenskitflexivel;
	}
	public void setExibiritenskitflexivel(Boolean exibiritenskitflexivel) {
		this.exibiritenskitflexivel = exibiritenskitflexivel;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Money getValorIpi() {
		return valorIpi;
	}
	public void setValorIpi(Money valorIpi) {
		this.valorIpi = valorIpi;
	}
	public Double getIpi() {
		return ipi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public TranslateEnumBean getTipoCobrancaIpi() {
		return tipoCobrancaIpi;
	}
	public void setTipoCobrancaIpi(TranslateEnumBean tipoCobrancaIpi) {
		this.tipoCobrancaIpi = tipoCobrancaIpi;
	}
	public TranslateEnumBean getTipoCalculoIpi() {
		return tipoCalculoIpi;
	}
	public void setTipoCalculoIpi(TranslateEnumBean tipoCalculoIpi) {
		this.tipoCalculoIpi = tipoCalculoIpi;
	}
	public Money getAliquotaReaisIpi() {
		return aliquotaReaisIpi;
	}
	public void setAliquotaReaisIpi(Money aliquotaReaisIpi) {
		this.aliquotaReaisIpi = aliquotaReaisIpi;
	}
	public GenericBean getGrupoTributacao() {
		return grupoTributacao;
	}
	public void setGrupoTributacao(GenericBean grupoTributacao) {
		this.grupoTributacao = grupoTributacao;
	}
}
