package br.com.linkcom.sined.modulo.pub.controller.process.bean;

import br.com.linkcom.sined.util.bean.GenericBean;

public class UnidadeMedidaWSBean extends GenericBean{

	private String simbolo;
	
	public String getSimbolo() {
		return simbolo;
	}
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
}
