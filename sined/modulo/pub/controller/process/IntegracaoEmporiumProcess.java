package br.com.linkcom.sined.modulo.pub.controller.process;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Emporiumpedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.geral.service.ArquivoemporiumService;
import br.com.linkcom.sined.geral.service.EmporiumpedidovendaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.IntegracaoEmporiumBean;
import br.com.linkcom.sined.util.CacheWebserviceUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;

@Bean
@Controller(path="/pub/process/IntegracaoEmporium")
public class IntegracaoEmporiumProcess extends MultiActionController {

	public static final String PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE = "TOKEN_VALIDACAO_WEBSERVICE";
	public static final String pathExecucaoEmporium = "/var/emporium/pos/RCV/0000/000";
	public static final String pathExportacaoEmporium = "/var/emporium/w3erp/exportacao";
	
	private ParametrogeralService parametrogeralService;
	private ArquivoemporiumService arquivoemporiumService;
	private EmporiumpedidovendaService emporiumpedidovendaService;
	
	public void setEmporiumpedidovendaService(
			EmporiumpedidovendaService emporiumpedidovendaService) {
		this.emporiumpedidovendaService = emporiumpedidovendaService;
	}
	public void setArquivoemporiumService(
			ArquivoemporiumService arquivoemporiumService) {
		this.arquivoemporiumService = arquivoemporiumService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void getActionPedidovenda(WebRequestContext request, GenericEnvioInformacaoBean bean){
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}	
			
			String tokenValidacao = parametrogeralService.getValorPorNome(PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
			
			boolean haveRegistro = false;
			
			StringBuilder sb = new StringBuilder();
			
			haveRegistro = this.getArquivoemporiumCliente(tokenValidacao, haveRegistro, sb);
			haveRegistro = this.getPedidovenda(tokenValidacao, haveRegistro, sb);
			
			if(sb.length() > 0)
				View.getCurrent().print(sb.toString());
			
			request.getServletResponse().setContentType("text/plain");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("ERRO|" + e.getMessage());
		}
	}
	
	public void getActions(WebRequestContext request, GenericEnvioInformacaoBean bean){
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}	
			
			String tokenValidacao = parametrogeralService.getValorPorNome(PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
			
			boolean haveRegistro = false;
			
			StringBuilder sb = new StringBuilder();
			
			haveRegistro = this.getArquivoemporium(tokenValidacao, haveRegistro, sb);
			haveRegistro = this.getPedidovenda(tokenValidacao, haveRegistro, sb);
			haveRegistro = this.sendArquivoemporium(tokenValidacao, haveRegistro, sb);
			
			if(sb.length() > 0)
				View.getCurrent().print(sb.toString());
			
			request.getServletResponse().setContentType("text/plain");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("ERRO|" + e.getMessage());
		}
	}
	
	private boolean sendArquivoemporium(String tokenValidacao, boolean haveRegistro, StringBuilder sb) {
		String pathHistoryEmporium = "/var/emporium/history/" + SinedDateUtils.toString(SinedDateUtils.currentDate(), "yyMMdd") + "/0000/000/RCV";
		
		List<Arquivoemporium> listaArquivoemporiumSEND = arquivoemporiumService.findForWebserviceSEND();
		for (Arquivoemporium arquivoemporium : listaArquivoemporiumSEND) {
			if(arquivoemporium.getArquivoemporiumtipo() != null && 
					arquivoemporium.getArquivo() != null){
				String tipo = arquivoemporium.getArquivoemporiumtipo().name();
				String nome = arquivoemporium.getArquivo().getNome().replaceAll("\\.txt", "");
				String path = pathHistoryEmporium;
				
				if(arquivoemporium.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISGERAIS) ||
						arquivoemporium.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_CLIENTES) ||
						arquivoemporium.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISTRIBUTACAO) ||
						arquivoemporium.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_MOVIMENTODETALHADO)){
					path = pathExportacaoEmporium;
				}
				
				if(haveRegistro){
					sb.append("\n");
				}
				haveRegistro = true;
				
				sb.append("SEND|")
					.append(path)
					.append("|")
					.append(SinedUtil.getUrlWithContext())
					.append("/pub/process/IntegracaoEmporium?ACAO=sendFile&tipo=")
					.append(tipo)
					.append("&hash=")
					.append(tokenValidacao)
					.append("|")
					.append(nome);
			}
		}
		return haveRegistro;
	}
	
	private boolean getArquivoemporiumCliente(String tokenValidacao, boolean haveRegistro, StringBuilder sb) {
		List<Arquivoemporium> listaArquivoemporiumGET = CacheWebserviceUtil.getArquivoemporiumCliente();
		if(listaArquivoemporiumGET == null){
			listaArquivoemporiumGET = arquivoemporiumService.findForWebserviceGET(Arquivoemporiumtipo.IMPORTACAO_CLIENTES);
			CacheWebserviceUtil.putArquivoemporiumCliente(listaArquivoemporiumGET);
		}
		
		return setStringArquivoemporiumGET(tokenValidacao,haveRegistro, sb, listaArquivoemporiumGET);
	}
	
	private boolean getArquivoemporium(String tokenValidacao, boolean haveRegistro, StringBuilder sb) {
		List<Arquivoemporium> listaArquivoemporiumGET = arquivoemporiumService.findForWebserviceGET(null);
		return setStringArquivoemporiumGET(tokenValidacao,haveRegistro, sb, listaArquivoemporiumGET);
	}
	
	private boolean setStringArquivoemporiumGET(String tokenValidacao, boolean haveRegistro, StringBuilder sb, List<Arquivoemporium> listaArquivoemporiumGET) {
		for (Arquivoemporium arquivoemporium : listaArquivoemporiumGET) {
			if(arquivoemporium.getArquivoemporiumtipo() != null && 
					arquivoemporium.getArquivo() != null){
				String tipo = arquivoemporium.getArquivoemporiumtipo().name();
				String nome = arquivoemporium.getArquivo().getNome();
				
				if(haveRegistro){
					sb.append("\n");
				}
				haveRegistro = true;
				
				sb.append("GET|")
					.append(SinedUtil.getUrlWithContext())
					.append("/DOWNLOADFILE/")
					.append(arquivoemporium.getArquivo().getCdarquivo())
					.append("?isNfeDesktop=true&senhaNfeDesktop=linkcom|")
					.append(SinedUtil.getUrlWithContext())
					.append("/pub/process/IntegracaoEmporium?ACAO=confirmation&tipo=")
					.append(tipo)
					.append("&id=")
					.append(arquivoemporium.getCdarquivoemporium())
					.append("&hash=")
					.append(tokenValidacao)
					.append("|")
					.append(pathExecucaoEmporium)
					.append("|")
					.append(nome);
			}
		}
		return haveRegistro;
	}
	
	private boolean getPedidovenda(String tokenValidacao, boolean haveRegistro, StringBuilder sb) {
		List<Emporiumpedidovenda> listaEmporiumpedidovendaGET = CacheWebserviceUtil.getEmporiumpedidovenda();
		if(listaEmporiumpedidovendaGET == null){
			listaEmporiumpedidovendaGET = emporiumpedidovendaService.findForWebserviceGET();
			CacheWebserviceUtil.putEmporiumpedidovenda(listaEmporiumpedidovendaGET);
		}
		
		for (Emporiumpedidovenda emporiumpedidovenda : listaEmporiumpedidovendaGET) {
			if(emporiumpedidovenda.getArquivo() != null){
				String nome = emporiumpedidovenda.getArquivo().getNome();
				
				if(haveRegistro){
					sb.append("\n");
				}
				haveRegistro = true;
				
				sb.append("GET|")
					.append(SinedUtil.getUrlWithContext())
					.append("/DOWNLOADFILE/")
					.append(emporiumpedidovenda.getArquivo().getCdarquivo())
					.append("?isNfeDesktop=true&senhaNfeDesktop=linkcom|")
					.append(SinedUtil.getUrlWithContext())
					.append("/pub/process/IntegracaoEmporium?ACAO=confirmation&tipo=")
					.append(Arquivoemporiumtipo.IMPORTACAO_PEDIDOVENDA.name())
					.append("&id=")
					.append(emporiumpedidovenda.getCdemporiumpedidovenda())
					.append("&hash=")
					.append(tokenValidacao)
					.append("|")
					.append(pathExecucaoEmporium)
					.append("|")
					.append(nome);
			}
		}
		return haveRegistro;
	}
	
	public void confirmation(WebRequestContext request, IntegracaoEmporiumBean bean){
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}	
			
			if(bean.getTipo() != null){
				if(bean.getTipo().equals(Arquivoemporiumtipo.IMPORTACAO_PEDIDOVENDA)){
					emporiumpedidovendaService.updateEnviadoemporium(bean.getId());
				} else {
					arquivoemporiumService.updateEnviadoemporium(bean.getId());
				}
				
				if(bean.getTipo().equals(Arquivoemporiumtipo.IMPORTACAO_PEDIDOVENDA)){
					CacheWebserviceUtil.removeEmporiumpedidovenda();
				} else if(bean.getTipo().equals(Arquivoemporiumtipo.IMPORTACAO_CLIENTES)){
					CacheWebserviceUtil.removeArquivoemporiumCliente();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("ERRO|" + e.getMessage());
		}
	}
	
	public void sendFile(WebRequestContext request, IntegracaoEmporiumBean bean){
		try {
			if(!this.validaTokenComunicacao(bean)){
				throw new SinedException("Erro na autentica��o.");
			}	
			
			if(bean.getTipo() != null){
				String nome_banco = SinedUtil.getStringConexaoBanco();
				String pathDir = IntegracaoEmporiumUtil.util.saveDir(nome_banco, true);
				
				String contentStr = new String(bean.getArquivo().getContent());
				IntegracaoEmporiumUtil.util.stringToFile(pathDir, bean.getArquivo().getNome(), contentStr != null && contentStr.length() > 0 ? contentStr : " ");
				View.getCurrent().print("true");
			} else {
				throw new SinedException("Tipo n�o encontrado");
			}
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().print("false");
		}
	}
	
	/**
	 * M�todo que faz a valida��o com o token que est� no par�metro.
	 *
	 * @param bean
	 * @return
	 * @since 27/09/2012
	 * @author Rodrigo Freitas
	 */
	private boolean validaTokenComunicacao(GenericEnvioInformacaoBean bean){
		String tokenValidacao = parametrogeralService.getValorPorNome(PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
		String hash = bean.getHash();
		return hash != null && tokenValidacao != null && hash.equals(tokenValidacao);
	}
	
}
