package br.com.linkcom.sined.modulo.pub.controller.process;

import nl.captcha.Captcha;
import nl.captcha.servlet.CaptchaServletUtil;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.modulo.pub.controller.report.BoletoPubReport;
import br.com.linkcom.sined.modulo.pub.controller.report.filter.BoletoPubFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/pub/process/BoletoPub")
public class BoletoPubProcess extends MultiActionController {

	private DocumentoService documentoService;
	
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, BoletoPubFiltro filtro) throws Exception {
		request.getSession().setAttribute("isBoletoPub", Boolean.TRUE);
		request.getSession().setAttribute("urlImagem", SinedUtil.getLogoEmpresaCabecalho());
		
		return new ModelAndView("direct:process/boletopub", "filtro", filtro);
	}
	
	public ModelAndView getCaptchaImage(WebRequestContext request, BoletoPubFiltro filtro) {
		Captcha captcha = new Captcha.Builder(120, 40)
			.addText()
			.addBackground()
			.addNoise()
			.build();
		CaptchaServletUtil.writeImage(request.getServletResponse(), captcha.getImage());
		
		request.getSession().setAttribute(Captcha.NAME + filtro.getCddocumento(), captcha);
		
		return null;
	}
	
	public ModelAndView preview(WebRequestContext request, BoletoPubFiltro filtro) throws Exception {
		request.getSession().setAttribute("isBoletoPub", Boolean.TRUE);
		request.getSession().setAttribute("urlImagem", SinedUtil.getLogoEmpresaCabecalho());
		
		Captcha captcha = (Captcha) request.getSession().getAttribute(Captcha.NAME + filtro.getCddocumento());
		request.getSession().removeAttribute("Captcha.NAME" + filtro.getCddocumento());
		
		if (captcha != null && captcha.isCorrect(filtro.getRespostaCaptcha())) {
			Documento documento = documentoService.findEmpresaByDocumento(filtro.getCddocumento());
			String textoBoleto = documento != null && documento.getEmpresa() != null && StringUtils.isNotEmpty(documento.getEmpresa().getTextoautenticidadeboleto()) ? documento.getEmpresa().getTextoautenticidadeboleto() : "";
			
			request.getSession().setAttribute("textoboleto", textoBoleto);
			request.getSession().setAttribute(Parametrogeral.GERAR_RELATORIO_PDF, false);
			
			try {
				return NeoWeb.getObject(BoletoPubReport.class).doGerar(request, filtro);
			} catch (Exception e) {
				request.addError(e.getMessage());
				filtro.setRespostaCaptcha("");
				return new ModelAndView("direct:process/boletopub", "filtro", filtro);
			}
		} else {
			request.addError("O Captcha n�o foi digitado corretamente, tente novamente.");
			filtro.setRespostaCaptcha("");
			return new ModelAndView("direct:process/boletopub", "filtro", filtro);
		}
	}
}
