package br.com.linkcom.sined.modulo.pub.controller.process;

import java.io.IOException;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.VersaoService;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
/**
 * Controller respons�vel por gerar o arquivo offline.manifest
 * @author igorsilveriocosta
 *
 */
@Controller(path={"/pub/Login"})
public class Login extends MultiActionController {
	
	private ParametrogeralService parametroGeralService;
	
	public void setParametroGeralService(ParametrogeralService parametroGeralService) {
		this.parametroGeralService = parametroGeralService;
	}
	
	/**
	 * OBS: Mesmo logado esse met�do tem que redirecionar para a tela de login, 
	 * para fazer o cache da p�gina de login corretamente
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@DefaultAction
	public ModelAndView index(WebRequestContext request) throws IOException {
		try {
			VersaoService.getInstance().doVerificaVersao(request.getServletRequest());
		} catch (SinedException e) {
			request.setAttribute("errorversion", true);
			
			try {
				SinedUtil.enviaEmailAdmin();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException(e.getMessage());
		}
		
		// Pend�ncia Financeira
		ControleAcessoUtil.util.doVerificaPendenciafinanceira(request.getServletRequest());
		
		if(request.getSession().getAttribute("originator")!=null){
			request.setAttribute("originatorRequest", request.getSession().getAttribute("originator"));
			request.getSession().removeAttribute("originator");
		}else{
			request.setAttribute("originatorRequest", "/" + Neo.getApplicationName()+"/adm/index");
		}
		
		if(request.getSession().getAttribute("login_error")!=null){ 
			request.setAttribute("login_error", true);
			request.getSession().removeAttribute("login_error");
		}
    	
		if(request.getSession().getAttribute("login_bloqueado")!=null){
			request.setAttribute("login_bloqueado", true);
			request.getSession().removeAttribute("login_bloqueado");
		}
    	
		if(request.getParameter("errorversion") != null){
			request.setAttribute("errorversion", true);
		}
		
		request.setAttribute("exibirIndicarEmpresa", parametroGeralService.getBoolean("EXIBIR_INDICACAO_EMPRESA"));
		request.setAttribute("urlIndicacaoEmpresa", parametroGeralService.getValorPorNome("URL_INDICACAO_EMPRESA"));
		
		request.setAttribute("ignoreHackAndroidDownload", true);
		request.setAttribute("urlImagem", SinedUtil.getLogoEmpresaCabecalho(request.getServletRequest()));
		return new ModelAndView("direct:../../../jsp/login");
	}
	
	public ModelAndView limparCache(WebRequestContext request){
		ControleAcessoUtil.util.limparCache(request);
		return sendRedirectToAction("index");
	}
	
}