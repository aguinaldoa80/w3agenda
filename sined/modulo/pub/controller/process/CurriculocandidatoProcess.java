package br.com.linkcom.sined.modulo.pub.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Candidato;
import br.com.linkcom.sined.geral.bean.Candidatosituacao;
import br.com.linkcom.sined.geral.bean.Grauinstrucao;
import br.com.linkcom.sined.geral.service.CandidatoService;
import br.com.linkcom.sined.geral.service.GrauinstrucaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.pub.controller.process.filter.CurriculocandidatoFilter;

import com.ibm.icu.text.SimpleDateFormat;


@Bean
@Controller(path="/pub/process/Curriculocandidato")
public class CurriculocandidatoProcess extends MultiActionController{
	
	private CandidatoService candidatoService;
	private GrauinstrucaoService grauinstrucaoService;
	
	public void setCandidatoService(CandidatoService candidatoService) {
		this.candidatoService = candidatoService;
	}
	public void setGrauinstrucaoService( GrauinstrucaoService grauinstrucaoService) {
		this.grauinstrucaoService = grauinstrucaoService;
	}
	
	
	@DefaultAction
	public ModelAndView alterar(WebRequestContext request, CurriculocandidatoFilter filtro) throws Exception {
		return new ModelAndView("process/curriculocandidato", "filtro", filtro);
	}
	
	@Action("getInfoCpf")
	public ModelAndView getInfoCpf(WebRequestContext request, CurriculocandidatoFilter filtro) throws Exception{
		Candidato candidato = candidatoService.carregaCandidatoFromCpf(filtro.getVerificaCpf());
		if(candidato == null){
			return getInfoSenha(request, filtro);
		}else{
			request.setAttribute("status", true);
			return new ModelAndView("process/curriculocandidato", "filtro", filtro);
		}
	}
	
	
	@Action("getInfoSenha")
	public ModelAndView getInfoSenha(WebRequestContext request, CurriculocandidatoFilter filtro) throws Exception{
		
		Candidato candidato = candidatoService.carregaCandidatoFromCpf(filtro.getVerificaCpf());
		
		request.setAttribute("hoje", new SimpleDateFormat("dd/MM/yyyy").format(new java.sql.Date(System.currentTimeMillis())));
		
		if(candidato == null){
			List<Grauinstrucao> listaGrauinstrucao = grauinstrucaoService.findAll();
			request.setAttribute("listaGrauinstrucaoCompleta", listaGrauinstrucao);
			
			Candidato novoCandidato = new Candidato();
			novoCandidato.setCpf(filtro.getVerificaCpf());
			
			candidato = novoCandidato;
		} else if(!filtro.getVerificaSenha().equals(candidato.getSenha())){
			request.setAttribute("senhaNaoConfere", true);
			return new ModelAndView("process/curriculocandidato", "filtro", filtro);
		}
		
		String srtVoltar = ParametrogeralService.getInstance().getValorPorNome("VoltarCurriculo");
		if(srtVoltar  != null && !srtVoltar.equals("")){
			request.setAttribute("srtVoltar", srtVoltar);
		}
		
		return new ModelAndView("process/formulariocurriculo", "candidato", candidato);
		
	}

	@Action("salvar")
	public ModelAndView salvar (WebRequestContext request, Candidato candidato) throws Exception {
		candidato.setCandidatosituacao(Candidatosituacao.EM_ABERTO);
		candidatoService.saveOrUpdate(candidato);
		
		request.addMessage("Curr�culo enviado! Mantenha seus dados atualizados.");
		return new ModelAndView("redirect:/pub/process/Curriculocandidato");
		
	}
	
	
	public ModelAndView enviaEmail (WebRequestContext request, CurriculocandidatoFilter filtro){
		
		Candidato candidato = candidatoService.carregaCandidatoFromCpf(filtro.getVerificaCpf());
		
		String campo = candidato.getEmail();
		
		if(campo == null || "".equals(campo)) {
			request.addError("E-mail cadastrado � inv�lido");
		}
		else {
				enviaEmailNovaSenha(request,candidato);
				request.addMessage("Senha enviada para o email "+candidato.getEmail()+".");
		}
		
		return new ModelAndView("redirect:/pub/process/Curriculocandidato");
	}
	
	private void enviaEmailNovaSenha(WebRequestContext request, Candidato candidato){
		
		try {
			candidatoService.enviaEmailRecuperarSenha(candidato);
			candidatoService.alterarSenhaCandidato(candidato);
		} catch (Exception e) {
			request.setAttribute("errormessage", "Senha n�o enviada!");
			e.printStackTrace();
		}
	}
	
}
