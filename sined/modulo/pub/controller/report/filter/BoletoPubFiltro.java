package br.com.linkcom.sined.modulo.pub.controller.report.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BoletoPubFiltro extends FiltroListagemSined {
	
	private Integer cddocumento;
	private String hash;
	private String respostaCaptcha;
	
	public Integer getCddocumento() {
		return cddocumento;
	}
	
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	@DisplayName("Digite os caracteres acima")
	public String getRespostaCaptcha() {
		return respostaCaptcha;
	}
	
	public void setRespostaCaptcha(String respostaCaptcha) {
		this.respostaCaptcha = respostaCaptcha;
	}
}
