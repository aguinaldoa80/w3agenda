package br.com.linkcom.sined.modulo.pub.controller.report.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class NotaFiscalEletronicaPubFiltro extends FiltroListagemSined {
	
	private Integer cdnota;
	private String hash;
	
	public Integer getCdnota() {
		return cdnota;
	}
	public void setCdnota(Integer cdnota) {
		this.cdnota = cdnota;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}

}
