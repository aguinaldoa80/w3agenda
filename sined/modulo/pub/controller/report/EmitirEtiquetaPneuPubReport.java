package br.com.linkcom.sined.modulo.pub.controller.report;

import java.util.LinkedList;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.service.ProducaoagendamaterialService;
import br.com.linkcom.sined.util.SinedException;

@Controller(path = "/pub/relatorio/EmitirEtiquetaPneuPubReport")
public class EmitirEtiquetaPneuPubReport extends ReportTemplateController<ReportTemplateFiltro>{

	private ProducaoagendamaterialService producaoAgendaMaterialService;
	
	public void setProducaoAgendaMaterialService(ProducaoagendamaterialService producaoAgendaMaterialService) {this.producaoAgendaMaterialService = producaoAgendaMaterialService;}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.ETIQUETA_DO_PNEU;
	}

	@Override
	protected String getFileName() {
		return "etiqueta_pneu";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		return producaoAgendaMaterialService.getTemplateByRaw(request);
	}
	
	@Override
	protected LinkedList<ReportTemplateBean> carregaListaDados(WebRequestContext request, ReportTemplateFiltro filtro)throws Exception {
		return producaoAgendaMaterialService.carregaDadosEtiquetaPneuReport(request);
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {
		String hash = Util.crypto.makeHashMd5(request.getParameter("selectedItens"));
		String hash2 = Util.crypto.makeHashMd5(hash);
		String hash3 = Util.crypto.makeHashMd5(hash2);
		
		if(!hash3.equals(request.getParameter("hash"))){
			throw new SinedException("Erro de autenticação.");
		}
		
		return super.doGerar(request, filtro);
	}

}