package br.com.linkcom.sined.modulo.pub.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.geral.service.NotafiscalprodutoService;
import br.com.linkcom.sined.modulo.pub.controller.report.filter.NotaFiscalEletronicaPubFiltro;
import br.com.linkcom.sined.util.SinedException;

@Controller(path = "/pub/relatorio/DanfePub")
public class DanfePubReport extends ResourceSenderController<NotaFiscalEletronicaPubFiltro>{

	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaService notaService;
	
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, NotaFiscalEletronicaPubFiltro filtro) throws Exception {
		return null;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, NotaFiscalEletronicaPubFiltro filtro) throws Exception {
		if(filtro.getCdnota() == null || filtro.getHash() == null){
			throw new SinedException("Erro de autentica��o.");
		}
		
		// PASSAR O CDDOCUMENTO TR�S VEZES NO MD5
		String hash = Util.crypto.makeHashMd5(filtro.getCdnota().toString());
		String hash2 = Util.crypto.makeHashMd5(hash);
		String hash3 = Util.crypto.makeHashMd5(hash2);
		
		if(!hash3.equals(filtro.getHash())){
			throw new SinedException("Erro de autentica��o.");
		}
		
		if ("true".equalsIgnoreCase(request.getParameter("w3cliente"))){
			if (notaService.haveNFDiferenteStatus(filtro.getCdnota().toString(), NotaStatus.EM_ESPERA, NotaStatus.EMITIDA, NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA, NotaStatus.CANCELADA, NotaStatus.NFE_DENEGADA)){
				request.addError("Para impress�o do danfe as notas tem que estar com a situa��o 'Em Espera', 'Emitida', 'NF-e EMITIDA', 'NF-e LIQUIDADA', 'NF-e DENEGADA' ou 'CANCELADA'.");
				return null;
			}
		}else {
			if(notaService.haveNFDiferenteStatus(filtro.getCdnota().toString(), NotaStatus.NFE_EMITIDA, NotaStatus.NFE_LIQUIDADA, NotaStatus.CANCELADA)){
				request.addError("Para impress�o do danfe as notas tem que estar com a situa��o 'Emitida', 'NF-e EMITIDA', 'NF-e LIQUIDADA' ou 'CANCELADA'.");
				return null;
			}
		}
			
		return notafiscalprodutoService.gerarDanfe(filtro.getCdnota().toString());
	}

	
	
}
