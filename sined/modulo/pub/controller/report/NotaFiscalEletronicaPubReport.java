package br.com.linkcom.sined.modulo.pub.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.NotaService;
import br.com.linkcom.sined.modulo.pub.controller.report.filter.NotaFiscalEletronicaPubFiltro;
import br.com.linkcom.sined.util.SinedException;

@Controller(path="/pub/relatorio/NotaFiscalEletronicaPub")
public class NotaFiscalEletronicaPubReport extends ResourceSenderController<NotaFiscalEletronicaPubFiltro> {

	private NotaService notaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, NotaFiscalEletronicaPubFiltro filtro) throws ResourceGenerationException {
		return null;
	}
	
	@Override
	public Resource generateResource(WebRequestContext request, NotaFiscalEletronicaPubFiltro filtro) throws Exception {
		if(filtro.getCdnota() == null || filtro.getHash() == null){
			throw new SinedException("Erro de autentica��o.");
		}
		
		// PASSAR O CDDOCUMENTO TR�S VEZES NO MD5
		String hash = Util.crypto.makeHashMd5(filtro.getCdnota().toString());
		String hash2 = Util.crypto.makeHashMd5(hash);
		String hash3 = Util.crypto.makeHashMd5(hash2);
		
		if(!hash3.equals(filtro.getHash())){
			throw new SinedException("Erro de autentica��o.");
		}
		
		if(notaService.haveNFProduto(filtro.getCdnota().toString())){
			throw new SinedException("A��o permitida somente para nota fiscal de servi�o.");
		}
		
		return notaFiscalServicoService.gerarRelatorioNfse(filtro.getCdnota().toString(), false);
	}
	
	
	public static void main(String[] args) {
		String hash = Util.crypto.makeHashMd5("661");
		String hash2 = Util.crypto.makeHashMd5(hash);
		String hash3 = Util.crypto.makeHashMd5(hash2);
		System.out.println(hash3);
	}

}
