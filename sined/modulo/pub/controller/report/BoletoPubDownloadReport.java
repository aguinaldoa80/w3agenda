package br.com.linkcom.sined.modulo.pub.controller.report;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.controller.resource.ResourceSenderController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.service.ContareceberService;
import br.com.linkcom.sined.modulo.pub.controller.report.filter.BoletoPubFiltro;
import br.com.linkcom.sined.util.SinedException;

@Controller(path = "/pub/relatorio/BoletoPubDownload")
public class BoletoPubDownloadReport extends ResourceSenderController<BoletoPubFiltro>{

	private ContareceberService contareceberService;
	
	public void setContareceberService(ContareceberService contareceberService) {
		this.contareceberService = contareceberService;
	}

	@Override
	public ModelAndView doFiltro(WebRequestContext request, BoletoPubFiltro filtro) {
		return null;
	}

	@Override
	public Resource generateResource(WebRequestContext request, BoletoPubFiltro filtro) throws Exception {
		
		if(filtro.getCddocumento() == null || filtro.getHash() == null){
			throw new SinedException("Erro de autentica��o.");
		}
		
		// PASSAR O CDDOCUMENTO TR�S VEZES NO MD5
		String hash = Util.crypto.makeHashMd5(filtro.getCddocumento().toString());
		String hash2 = Util.crypto.makeHashMd5(hash);
		String hash3 = Util.crypto.makeHashMd5(hash2);
		
		if(!hash3.equals(filtro.getHash())){
			throw new SinedException("Erro de autentica��o.");
		}
		
		return contareceberService.createBoleto(request, filtro.getCddocumento().toString());
	}
}