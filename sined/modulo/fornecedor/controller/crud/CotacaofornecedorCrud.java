package br.com.linkcom.sined.modulo.fornecedor.controller.crud;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.validator.GenericValidator;

import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.CotacaofornecedorService;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.fornecedor.controller.crud.filter.CotacaofornecedorFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/fornecedor/crud/Cotacaofornecedor")
public class CotacaofornecedorCrud extends CrudControllerSined<CotacaofornecedorFiltro, Cotacaofornecedor, Cotacaofornecedor>{
	
	private ContatoService contatoService;
	private CotacaofornecedorService cotacaofornecedorService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private FornecedorService fornecedorService;
	private UsuarioService usuarioService;
	private EnvioemailService envioemailService;
	
	public void setCotacaofornecedorService(
			CotacaofornecedorService cotacaofornecedorService) {
		this.cotacaofornecedorService = cotacaofornecedorService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}	
	public void setUnidademedidaconversaoService(
			UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	
	@Override
	protected void listagem(WebRequestContext request, CotacaofornecedorFiltro filtro) throws Exception {
		request.setAttribute("logado", (Fornecedor)Neo.getUser());
	}
	
	@Override
	protected void entrada(WebRequestContext request, Cotacaofornecedor form) throws Exception {
		Fornecedor fornecedor = (Fornecedor)Neo.getUser();
		
		request.setAttribute("logado", fornecedor);
		request.setAttribute("listaContatos", contatoService.findByPessoaCombo(fornecedor));
		
		List<Localarmazenagem> listaLocal = new ArrayList<Localarmazenagem>();
		List<Cotacaofornecedoritem> lista = form.getListaCotacaofornecedoritem();
		
		if (lista != null && lista.size() > 0) {
			for (Cotacaofornecedoritem cotacaofornecedoritem : lista) {
				if (cotacaofornecedoritem.getLocalarmazenagem() != null) {
					if (!listaLocal.contains(cotacaofornecedoritem.getLocalarmazenagem())) {
						listaLocal.add(cotacaofornecedoritem.getLocalarmazenagem());
					}
				}
			}
		}
		Collections.sort(listaLocal,new Comparator<Localarmazenagem>(){
			public int compare(Localarmazenagem o1, Localarmazenagem o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		
		request.setAttribute("listaLocal", listaLocal);

		List<Cotacaofornecedoritem> listaCFI = form.getListaCotacaofornecedoritem();
		if( listaCFI != null && !listaCFI.isEmpty()){
			for(Cotacaofornecedoritem cfi : listaCFI){
				cfi.setUnidademedidatrans(cfi.getUnidademedida());
				request.setAttribute("listaUM"+cfi.getMaterial().getCdmaterial(), unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(cfi.getMaterial().getUnidademedida(), cfi.getMaterial()));
			}
		}
	}
	
	@Override
	protected ListagemResult<Cotacaofornecedor> getLista(WebRequestContext request, CotacaofornecedorFiltro filtro) {
		ListagemResult<Cotacaofornecedor> listagemResult = super.getLista(request, filtro);
		List<Cotacaofornecedor> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcotacaofornecedor", ",");
		list.removeAll(list);
		if(!whereIn.equals("")){
			list.addAll(cotacaofornecedorService.loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}

		return listagemResult;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Cotacaofornecedor bean)
			throws Exception {
		try {
			Fornecedor fornecedor = fornecedorService.load(bean.getFornecedor());
			Usuario usuario = new Usuario(fornecedor.getCdusuarioaltera());
			usuario = usuarioService.load(usuario);
			if (GenericValidator.isEmail(fornecedor.getEmail()) && GenericValidator.isEmail(usuario.getEmail())){
			
				String assunto = "Aviso na altera��o de valores de cota��o" ;
				String mensagem = "<b>Aviso do sistema:</b> Os valores da cota��o de Materias/Servi�os j� foram informados pelo Fornecedor";

				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				Envioemail envioemail = envioemailService.registrarEnvio(
								fornecedor.getEmail(),
								assunto,
								mensagem,
								Envioemailtipo.COMUNICADO,
								new Pessoa(fornecedor.getCdpessoa()),
								fornecedor.getEmail(), 
								fornecedor.getNome(),
								email);
				
				email.setFrom(fornecedor.getEmail());
				email.setSubject(assunto);
				email.setTo(usuario.getEmail()); 
				email.addHtmlText(mensagem + EmailUtil.getHtmlConfirmacaoEmail(envioemail, usuario.getEmail()));
		
				email.sendMessage();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.salvar(request, bean);
	}
}
