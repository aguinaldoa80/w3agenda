package br.com.linkcom.sined.modulo.contabil.controller.report.filtro;



import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmitirLivroRazaoFiltro extends FiltroListagemSined{
	
	protected Empresa empresa;
	protected Date dtInicio;
	protected Date dtFim;
	protected Centrocusto centroCusto;
	protected Projeto projeto;
	protected ContaContabil contaContabil;

	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("Data In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	
	@Required
	@DisplayName("Data Fim")
	public Date getDtFim() {
		return dtFim;
	}
	
	@DisplayName("Centro de Custo")
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}

	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Conta Cont�bil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}

}
