package br.com.linkcom.sined.modulo.contabil.controller.report.filtro;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.TipoPeriodoBalancoPatrimonial;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmitirBalancoPatrimonialFiltro extends FiltroListagemSined {
	
	protected Empresa empresa;
	protected Date dtInicio;
	protected Date dtFim;
	protected String mesAno;
	protected Integer ano;
	protected Centrocusto centroCusto;
	protected Projeto projeto;
	protected ContaContabil contaContabil;
	protected TipoPeriodoBalancoPatrimonial tipoPeriodoBalancoPatrimonial;
	protected Boolean isSintetico;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Date getDtInicio() {
		return dtInicio;
	}
	public Date getDtFim() {
		return dtFim;
	}
	public String getMesAno() {
		return mesAno;
	}
	public Integer getAno() {
		return ano;
	}
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	public TipoPeriodoBalancoPatrimonial getTipoPeriodoBalancoPatrimonial() {
		return tipoPeriodoBalancoPatrimonial;
	}
	public Boolean getIsSintetico() {
		return isSintetico;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	public void setTipoPeriodoBalancoPatrimonial(TipoPeriodoBalancoPatrimonial tipoPeriodoBalancoPatrimonial) {
		this.tipoPeriodoBalancoPatrimonial = tipoPeriodoBalancoPatrimonial;
	}
	public void setIsSintetico(Boolean isSintetico) {
		this.isSintetico = isSintetico;
	}
	
}
