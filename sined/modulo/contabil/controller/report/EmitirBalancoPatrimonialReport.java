package br.com.linkcom.sined.modulo.contabil.controller.report;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.TipoPeriodoBalancoPatrimonial;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.modulo.contabil.controller.report.filtro.EmitirBalancoPatrimonialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/contabil/relatorio/EmitirBalancoPatrimonial", authorizationModule=ReportAuthorizationModule.class)
public class EmitirBalancoPatrimonialReport extends SinedReport<EmitirBalancoPatrimonialFiltro>{
	
	MovimentacaocontabilService movimentacaocontabilService;
	
	public void setMovimentacaocontabilService(MovimentacaocontabilService movimentacaocontabilService) {
		this.movimentacaocontabilService = movimentacaocontabilService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, EmitirBalancoPatrimonialFiltro filtro) throws Exception {
		if (filtro.getTipoPeriodoBalancoPatrimonial().equals(TipoPeriodoBalancoPatrimonial.ANUAL)){
			String dataInicioString = "01/01/" + filtro.getAno().toString();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date dataInicio = new Date(sdf.parse(dataInicioString).getTime());
			filtro.setDtInicio(dataInicio);
			
			String dataFimString = "31/12/" + filtro.getAno();
			Date dataFim = new Date(sdf.parse(dataFimString).getTime());
			filtro.setDtFim(dataFim);
		}else if (filtro.getTipoPeriodoBalancoPatrimonial().equals(TipoPeriodoBalancoPatrimonial.MENSAL)){
			String dataInicioString = "01/" + filtro.getMesAno();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date dataInicio = new Date(sdf.parse(dataInicioString).getTime());
			filtro.setDtInicio(dataInicio);
			
			Date dataFim = SinedDateUtils.lastDateOfMonth(dataInicio);
			filtro.setDtFim(dataFim);		
		}

		return movimentacaocontabilService.gerarRelatorioBalancoPatrimonial(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "emitirbalancopatrimonial";
	}

	@Override
	public String getTitulo(EmitirBalancoPatrimonialFiltro filtro) {
		return "BALAN�O PATRIMONIAL";
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request, EmitirBalancoPatrimonialFiltro filtro) throws ResourceGenerationException {
		request.setAttribute("PLANO_CONTAS_CONTABIL_POR_EMPRESA", parametrogeralService.buscaValorPorNome("PLANO_CONTAS_CONTABIL_POR_EMPRESA"));
		return super.doFiltro(request, filtro);
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EmitirBalancoPatrimonialFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		if(filtro.getEmpresa() != null){
			report.addParameter("NOMEEMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		} else {
			report.addParameter("NOMEEMPRESA", "");
		}
		
		if(filtro.getProjeto() != null){
			Projeto projeto = getProjetoService().load(filtro.getProjeto(), "projeto.nome");
			report.addParameter("NOMEPROJETO", projeto.getNome());
		} else {
			report.addParameter("NOMEPROJETO", "");
		}
		
		if(filtro.getCentroCusto() != null){
			Centrocusto centroCusto = getCentrocustoService().load(filtro.getCentroCusto(), "centrocusto.nome");
			report.addParameter("NOMECENTROCUSTO", centroCusto.getNome());
		}else {
			report.addParameter("NOMECENTROCUSTO", "");
		}
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		if(filtro.getDtInicio()!=null){
			String dataInicio = fmt.format(filtro.getDtInicio());
			report.addParameter("DATAINICIO", dataInicio);			
		}
		if(filtro.getDtFim()!=null){
			String dataFim = fmt.format(filtro.getDtFim());
			report.addParameter("DATAFIM", dataFim);			
		}
		
		//report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
		
		if (filtro.getTipoPeriodoBalancoPatrimonial().equals(TipoPeriodoBalancoPatrimonial.ANUAL)){
			report.addParameter("PERIODO", filtro.getAno().toString());
			Integer anoAnterior =  filtro.getAno() - 1;
			String anoAnteriorString = anoAnterior.toString();
			report.addParameter("PERIODOANTERIOR", anoAnteriorString);
		}else if (filtro.getTipoPeriodoBalancoPatrimonial().equals(TipoPeriodoBalancoPatrimonial.MENSAL)){
			report.addParameter("PERIODO", filtro.getMesAno());
			
			Date mesAnteriorDate = SinedDateUtils.addMesData(filtro.getDtInicio(), -1);
			SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
			String mesAnterior = sdf.format(mesAnteriorDate);
			report.addParameter("PERIODOANTERIOR", mesAnterior);
		}
	}
}
