package br.com.linkcom.sined.modulo.contabil.controller.report.bean;

import java.util.Comparator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.ContaContabil;

public class EmitirBalancoPatrimonialBean {
	
	protected ContaContabil contaContabil;
	protected String identificadorNome;
	protected Money saldoAnterior;
	protected Money saldoFinal;
	protected String saldoAnteriorString;
	protected String saldoFinalString;
	protected Boolean temFilho = false;
	protected Boolean isSintetico = false;

	public EmitirBalancoPatrimonialBean(){	
	}
	public EmitirBalancoPatrimonialBean(Integer cdcontacontabil, String nome, Boolean contaretificadora, String identificador) {
		this.contaContabil = new ContaContabil(cdcontacontabil, nome, contaretificadora, identificador);
	}	
	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	public Money getSaldoAnterior() {
		return saldoAnterior;
	}
	public Money getSaldoFinal() {
		return saldoFinal;
	}
	public Boolean getTemFilho() {
		return temFilho;
	}
	public String getSaldoAnteriorString() {
		return saldoAnteriorString;
	}
	public String getSaldoFinalString() {
		return saldoFinalString;
	}
	public Boolean getIsSintetico() {
		return isSintetico;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	public void setSaldoAnterior(Money saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}
	public void setSaldoFinal(Money saldoFinal) {
		this.saldoFinal = saldoFinal;
	}
	public void setIdentificadorNome(String identificadorNome) {
		this.identificadorNome = identificadorNome;
	}
	public void setTemFilho(Boolean temFilho) {
		this.temFilho = temFilho;
	}
	public void setSaldoAnteriorString(String saldoAnteriorString) {
		this.saldoAnteriorString = saldoAnteriorString;
	}
	public void setSaldoFinalString(String saldoFinalString) {
		this.saldoFinalString = saldoFinalString;
	}
	public void setIsSintetico(Boolean isSintetico) {
		this.isSintetico = isSintetico;
	}
	
	public String getIdentificadorNome() {
		this.identificadorNome = getContaContabil().getVcontacontabil().getIdentificador() + "                                             " + getContaContabil().getNome();
		return identificadorNome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((contaContabil == null) ? 0 : contaContabil.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmitirBalancoPatrimonialBean other = (EmitirBalancoPatrimonialBean) obj;
		if (contaContabil == null) {
			if (other.contaContabil != null)
				return false;
		} else if (!contaContabil.equals(other.contaContabil))
			return false;
		return true;
	}
	
	/**
	 * Comparator usado para pesquisa bin�ria na lista de contas gerenciais.
	 * Utiliza como chave o atrinuto 'identificador' dos objetos da lista.
	 * Para encontrar o objeto pai, remove-se do filho os 3 �ltimos caracteres (da direita)
	 * da chave. O que restar do n�mero � o identificador do objeto pai. 
	 */
	public static final Comparator<EmitirBalancoPatrimonialBean> PAI_COMPARATOR = new Comparator<EmitirBalancoPatrimonialBean>(){
		public int compare(EmitirBalancoPatrimonialBean o1, EmitirBalancoPatrimonialBean o2) {
			String identificadorO1 = StringUtils.deleteWhitespace(o1.getContaContabil().getVcontacontabil().getIdentificador());
			String identificadorO2 = StringUtils.deleteWhitespace(o2.getContaContabil().getVcontacontabil().getIdentificador());

			if (identificadorO2.split("\\.").length > 1) {
				identificadorO2 = identificadorO2.substring( 0, (identificadorO2.length() - (identificadorO2.split("\\.")[identificadorO2.split("\\.").length-1].length() + 1)) );
			} else {
				identificadorO2 = "";
			}
			return identificadorO1.compareTo(identificadorO2);
		}
	};
	
	
	
}
