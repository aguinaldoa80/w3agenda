package br.com.linkcom.sined.modulo.contabil.controller.report.bean;

import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.ContaContabil;

public class EmitirLivroRazaoBean {
	
	protected ContaContabil contaContabil;
	protected String identificadorNome;
	protected List<EmitirLivroRazaoItem> listaMovimentacoes;
	protected Money saldoInicial;
	protected Money saldoAtual;
	protected Money totalDebitos;
	protected Money totalCreditos;
	protected String saldoInicialString;
	protected String saldoAtualString;
	protected String totalDebitosString;
	protected String totalCreditosString;
	
	public EmitirLivroRazaoBean(){
		
	}
	
	public EmitirLivroRazaoBean(Integer cdcontagerencial, String nome, String identificador) {
		this.contaContabil = new ContaContabil(cdcontagerencial, nome, identificador);
	}
	
	public String getIdentificadorNome() {
		this.identificadorNome = getContaContabil().getVcontacontabil().getIdentificador() + " - " + getContaContabil().getNome();
		return identificadorNome;
	}
	
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	public void setIdentificadorNome(String identificadorNome) {
		this.identificadorNome = identificadorNome;
	}
	public List<EmitirLivroRazaoItem> getListaMovimentacoes() {
		return listaMovimentacoes;
	}
	public Money getSaldoInicial() {
		return saldoInicial;
	}
	public Money getSaldoAtual() {
		return saldoAtual;
	}
	public Money getTotalDebitos() {
		return totalDebitos;
	}
	public Money getTotalCreditos() {
		return totalCreditos;
	}
	public String getSaldoInicialString() {
		return saldoInicialString;
	}
	public String getSaldoAtualString() {
		return saldoAtualString;
	}
	public String getTotalDebitosString() {
		return totalDebitosString;
	}
	public String getTotalCreditosString() {
		return totalCreditosString;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	public void setListaMovimentacoes(List<EmitirLivroRazaoItem> listaMovimentacoes) {
		this.listaMovimentacoes = listaMovimentacoes;
	}
	public void setSaldoInicial(Money saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public void setSaldoAtual(Money saldoAtual) {
		this.saldoAtual = saldoAtual;
	}
	public void setTotalDebitos(Money totalDebitos) {
		this.totalDebitos = totalDebitos;
	}
	public void setTotalCreditos(Money totalCreditos) {
		this.totalCreditos = totalCreditos;
	}
	public void setSaldoInicialString(String saldoInicialString) {
		this.saldoInicialString = saldoInicialString;
	}
	public void setSaldoAtualString(String saldoAtualString) {
		this.saldoAtualString = saldoAtualString;
	}
	public void setTotalDebitosString(String totalDebitosString) {
		this.totalDebitosString = totalDebitosString;
	}
	public void setTotalCreditosString(String totalCreditosString) {
		this.totalCreditosString = totalCreditosString;
	}

	/**
	 * Comparator usado para pesquisa bin�ria na lista de contas gerenciais.
	 * Utiliza como chave o atrinuto 'identificador' dos objetos da lista.
	 * Para encontrar o objeto pai, remove-se do filho os 3 �ltimos caracteres (da direita)
	 * da chave. O que restar do n�mero � o identificador do objeto pai. 
	 */
	public static final Comparator<EmitirLivroRazaoBean> PAI_COMPARATOR = new Comparator<EmitirLivroRazaoBean>(){
		public int compare(EmitirLivroRazaoBean o1, EmitirLivroRazaoBean o2) {
			String identificadorO1 = StringUtils.deleteWhitespace(o1.getContaContabil().getVcontacontabil().getIdentificador());
			String identificadorO2 = StringUtils.deleteWhitespace(o2.getContaContabil().getVcontacontabil().getIdentificador());

			if (identificadorO2.split("\\.").length > 1) {
				identificadorO2 = identificadorO2.substring( 0, (identificadorO2.length() - (identificadorO2.split("\\.")[identificadorO2.split("\\.").length-1].length() + 1)) );
			} else {
				identificadorO2 = "";
			}
			return identificadorO1.compareTo(identificadorO2);
		}
	};
	
	
}