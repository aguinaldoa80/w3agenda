package br.com.linkcom.sined.modulo.contabil.controller.report.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Projeto;

public class EmitirLivroRazaoItem {
	
	protected Integer cdmovimentacaocontabil;
	protected String contraPartidas;
	protected Date data;
	protected String documento;
	protected String historico;
	protected Money credito;
	protected Money debito;
	protected Money saldo;
	protected Money totalDebitos;
	protected Money totalCreditos;
	protected Money saldoAtual;
	protected String totalDebitosString;
	protected String totalCreditosString;
	protected String saldoAtualString;
	protected Projeto projeto;
	protected Centrocusto centroCusto;
	
	public Integer getCdmovimentacaocontabil() {
		return cdmovimentacaocontabil;
	}
	public String getContraPartidas() {
		return contraPartidas;
	}
	public Date getData() {
		return data;
	}
	public String getDocumento() {
		return documento;
	}
	public String getHistorico() {
		return historico;
	}
	public Money getCredito() {
		return credito;
	}
	public Money getDebito() {
		return debito;
	}
	public Money getSaldo() {
		return saldo;
	}
	public Money getTotalDebitos() {
		return totalDebitos;
	}
	public Money getTotalCreditos() {
		return totalCreditos;
	}
	public Money getSaldoAtual() {
		return saldoAtual;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}
	public String getTotalDebitosString() {
		return totalDebitosString;
	}
	public String getTotalCreditosString() {
		return totalCreditosString;
	}
	public String getSaldoAtualString() {
		return saldoAtualString;
	}
	public void setCdmovimentacaocontabil(Integer cdmovimentacaocontabil) {
		this.cdmovimentacaocontabil = cdmovimentacaocontabil;
	}
	public void setContraPartidas(String contraPartidas) {
		this.contraPartidas = contraPartidas;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setCredito(Money credito) {
		this.credito = credito;
	}
	public void setDebito(Money debito) {
		this.debito = debito;
	}
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
	public void setTotalDebitos(Money totalDebitos) {
		this.totalDebitos = totalDebitos;
	}
	public void setTotalCreditos(Money totalCreditos) {
		this.totalCreditos = totalCreditos;
	}
	public void setSaldoAtual(Money saldoAtual) {
		this.saldoAtual = saldoAtual;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	public void setTotalDebitosString(String totalDebitosString) {
		this.totalDebitosString = totalDebitosString;
	}
	public void setTotalCreditosString(String totalCreditosString) {
		this.totalCreditosString = totalCreditosString;
	}
	public void setSaldoAtualString(String saldoAtualString) {
		this.saldoAtualString = saldoAtualString;
	}
	
}
