package br.com.linkcom.sined.modulo.contabil.controller.report;

import java.text.SimpleDateFormat;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.ResourceGenerationException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.modulo.contabil.controller.report.filtro.EmitirLivroRazaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/contabil/relatorio/EmitirLivroRazao", authorizationModule=ReportAuthorizationModule.class)
public class EmitirLivroRazaoReport extends SinedReport<EmitirLivroRazaoFiltro>{
	
	MovimentacaocontabilService movimentacaocontabilService;
	
	public void setMovimentacaocontabilService(MovimentacaocontabilService movimentacaocontabilService) {
		this.movimentacaocontabilService = movimentacaocontabilService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request, EmitirLivroRazaoFiltro filtro) throws Exception {
		if(filtro.getDtInicio().compareTo(filtro.getDtFim()) > 0){
			throw new SinedException("Data inicial n�o pode ser maior que data final.");
		}
		return movimentacaocontabilService.gerarRelatorioLivroRazao(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "livrorazao";
	}

	@Override
	public String getTitulo(EmitirLivroRazaoFiltro filtro) {
		return "LIVRO RAZ�O";
	}
	
	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			EmitirLivroRazaoFiltro filtro) throws ResourceGenerationException {
		request.setAttribute("PLANO_CONTAS_CONTABIL_POR_EMPRESA", parametrogeralService.buscaValorPorNome("PLANO_CONTAS_CONTABIL_POR_EMPRESA"));
		return super.doFiltro(request, filtro);
	}
	
	@Override
	protected void adicionaParametrosFiltro(Report report, EmitirLivroRazaoFiltro filtro) {
		super.adicionaParametrosFiltro(report, filtro);
		
		if(filtro.getEmpresa() != null){
			report.addParameter("NOMEEMPRESA", getEmpresaService().getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		} else {
			report.addParameter("NOMEEMPRESA", "");
		}
		
		if(filtro.getProjeto() != null){
			Projeto projeto = getProjetoService().load(filtro.getProjeto(), "projeto.nome");
			report.addParameter("NOMEPROJETO", projeto.getNome());
		} else {
			report.addParameter("NOMEPROJETO", "");
		}
		
		if(filtro.getCentroCusto() != null){
			Centrocusto centroCusto = getCentrocustoService().load(filtro.getCentroCusto(), "centrocusto.nome");
			report.addParameter("NOMECENTROCUSTO", centroCusto.getNome());
		}else {
			report.addParameter("NOMECENTROCUSTO", "");
		}
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String dataInicio;
		String dataFim;
		if(filtro.getDtInicio() != null){
			dataInicio = fmt.format(filtro.getDtInicio());
		}else{
			dataInicio = "";
		}
		if(filtro.getDtFim() != null){
			dataFim = fmt.format(filtro.getDtFim());				
		}else{
			dataFim = "";
		}
		
		report.addParameter("DATAINICIO", dataInicio);
		report.addParameter("DATAFIM", dataFim);
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtInicio(), filtro.getDtFim()));
	}
}
