package br.com.linkcom.sined.modulo.contabil.controller.process;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.ContaContabilSaldoInicial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ContaContabilSaldoInicialService;
import br.com.linkcom.sined.geral.service.ContacontabilService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.contabil.controller.process.filter.ContaContabilSaldoInicialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;

@Controller(path="/sistema/process/ContaContabilSaldoInicial", authorizationModule=ProcessAuthorizationModule.class)
public class ContaContabilSaldoInicialProcess extends MultiActionController{
	private EmpresaService empresaService;
	private ContaContabilSaldoInicialService contaContabilSaldoInicialService;
	private ContacontabilService contacontabilService;
	private ParametrogeralService parametrogeralService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setContaContabilSaldoInicialService(
			ContaContabilSaldoInicialService contaContabilSaldoInicialService) {
		this.contaContabilSaldoInicialService = contaContabilSaldoInicialService;
	}
	public void setContacontabilService(
			ContacontabilService contacontabilService) {
		this.contacontabilService = contacontabilService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@DefaultAction
	@Command(session=false)
	public ModelAndView index(WebRequestContext request, ContaContabilSaldoInicialFiltro filtro) {
		filtro.setArquivo(null);
		request.setAttribute("PLANO_CONTAS_CONTABIL_POR_EMPRESA", parametrogeralService.getBoolean(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA));
		return new ModelAndView("process/importacaoContaContabilSaldoInicial", "filtro", filtro);
	}
	
	public ModelAndView carregarArquivo(WebRequestContext request, ContaContabilSaldoInicialFiltro filtro){
		if (filtro.getArquivo() == null || 
				filtro.getArquivo().getContent() == null || 
				filtro.getArquivo().getContent().length == 0)
				throw new SinedException("O arquivo deve ser informado.");		
			
		if(parametrogeralService.getBoolean(Parametrogeral.PLANO_CONTAS_CONTABIL_POR_EMPRESA)
				&& (filtro.getEmpresa() == null || filtro.getEmpresa().getCdpessoa() == null)){
			throw new SinedException("Empresa deve ser informada.");
		}
		String strFile = new String(filtro.getArquivo().getContent());
		String[] linhas = strFile.split("\\r?\\n");
		List<String> linhasPreenchidas = new ArrayList<String>();
		for (int i = 1; i < linhas.length; i++) {
			String linha = linhas[i];
			if(linha != null && !linha.replaceAll(";", "").trim().equals("")){
				linhasPreenchidas.add(linha);
			}
		}
		if(linhasPreenchidas.size() == 0){
			throw new SinedException("Nenhum registro a ser importado.");		
		}
		
		Empresa empresa = null;
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			empresa = empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.nomefantasia");
		}
		
		Integer numeroLinha = 1;
		List<String> listaErro = new ArrayList<String>();
		List<ContaContabilSaldoInicial> lista = new ArrayList<ContaContabilSaldoInicial>();
		List<ContaContabilSaldoInicial> listaBeanErro = new ArrayList<ContaContabilSaldoInicial>();
		
		for (String linha : linhasPreenchidas) {
			numeroLinha++;
			ContaContabilSaldoInicial  bean = new ContaContabilSaldoInicial();
			String erro = "";
			linha += " "; // o espa�o � concatenado para que o �ltimo campo seja pego no split mesmo que esteja vazio
			String[] campo = linha.split(";");
			
			String strCodigoContaContabil = campo[0].trim();
			String strCodigoAlternativoContaContabil = campo[1].trim();
			String strIdentificador = campo[2].trim();
			String strDtSaldoInicial = campo[3];
			String strSaldoInicial = campo[4];
			String descricao = campo[5].equals(" ") ? "" : campo[5].substring(0, campo[5].length() - 1);
			Integer cdContaContabil = null;
			Integer codigoAlternativo = null;
			Date dtSaldoInicial = null;
			Money saldoInicial = new Money();
			ContaContabil contaContabil = null;
			try {
				
				if(StringUtils.isBlank(strCodigoContaContabil) && StringUtils.isBlank(strCodigoAlternativoContaContabil) && StringUtils.isBlank(strIdentificador)){
					erro = msgErroLinha(numeroLinha, "A coluna \"C�digo da conta cont�bil\" ou \"C�digo alternativo da conta cont�bil\" ou \"Identificador\" deve ser preenchida.\n");
					continue;
				}else{
					try {
						saldoInicial = new Money(strSaldoInicial.replace(",", "."));
					} catch (Exception e) {
						erro = "O valor "+strSaldoInicial+" para o saldo n�o � um valor v�lido.\n";
					}
					if(StringUtils.isBlank(strIdentificador)){
						strIdentificador = null;
					}
					if(StringUtils.isNotBlank(strCodigoAlternativoContaContabil)){
						try {
							codigoAlternativo = Integer.parseInt(strCodigoAlternativoContaContabil);
						} catch (Exception e) {
							erro = msgErroLinha(numeroLinha, "O valor "+strCodigoAlternativoContaContabil+" para o C�digo Alternativo da Conta Cont�bil n�o � um valor v�lido.\n");
							continue;
						}
					}
					if(StringUtils.isNotBlank(strCodigoContaContabil)){
						try {
							cdContaContabil = Integer.parseInt(strCodigoContaContabil);
						} catch (Exception e) {
							erro = msgErroLinha(numeroLinha, "O valor "+strCodigoContaContabil+" para o C�digo da Conta Cont�bil n�o � um valor v�lido.\n");
							continue;
						}
					}
					contaContabil = contacontabilService.loadForImportacaoSaldoInicial(codigoAlternativo, cdContaContabil, strIdentificador, empresa);
					if(contaContabil == null){
						erro = msgErroLinha(numeroLinha, "Conta cont�bil n�o encontrada.\n");
						continue;
					}
				}
				
				
				try {
					dtSaldoInicial = SinedDateUtils.stringToDate(strDtSaldoInicial);
				} catch (ParseException e) {
					erro = "O valor "+strDtSaldoInicial+" para a data inicial de saldo n�o � um valor v�lido.\n";
				}
				
				try {
					saldoInicial = new Money(strSaldoInicial.replace(",", "."));
				} catch (Exception e) {
					erro = "O valor "+strSaldoInicial+" para o saldo n�o � um valor v�lido.\n";
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				listaErro.add(e.getMessage());
			} finally{
				bean.setCodigoAlternativoContaContabil(strCodigoAlternativoContaContabil);
				bean.setCodigoContaContabil(strCodigoContaContabil);
				bean.setDescricao(descricao);
				bean.setContaContabil(contaContabil);
				bean.setIdentificador(strIdentificador);
				bean.setSaldo(saldoInicial);
				bean.setDtSaldo(dtSaldoInicial);
				bean.setArquivo(filtro.getArquivo());
				if(StringUtils.isNotBlank(erro)){
					bean.setErro(erro);
					listaBeanErro.add(bean);	
				}else{
					lista.add(bean);
				}
			}
		}
		filtro.setListaBean(lista);
		filtro.setListaBeanComErro(listaBeanErro);
		
		if(listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			return sendRedirectToAction("index");
		}
		String identificadorSessao = SinedDateUtils.currentTimestamp().toString();
		request.getSession().setAttribute("movimentacaocontabil_" + identificadorSessao, filtro);
		request.setAttribute("identificador", "movimentacaocontabil_" + identificadorSessao);
		
		return new ModelAndView("process/importacaoContaContabilSaldoInicialListagem", "filtro", filtro);
	}
	
	private String msgErroLinha(Integer linha, String msg){
		return "Linha "+linha.toString()+". "+msg;
	}
	
	public ModelAndView save(WebRequestContext request, ContaContabilSaldoInicialFiltro filtro){
		String identificador = request.getParameter("identificador");
		
		ContaContabilSaldoInicialFiltro  objFiltro = (ContaContabilSaldoInicialFiltro )request.getSession().getAttribute(identificador);
		
		contaContabilSaldoInicialService.saveOrUpdateList(objFiltro.getListaBean());
		request.addMessage("Importa��o realizada com sucesso!");
		return sendRedirectToAction("index");
	}
}
