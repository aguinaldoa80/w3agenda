package br.com.linkcom.sined.modulo.contabil.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Configuracaodre;
import br.com.linkcom.sined.geral.service.ConfiguracaodreService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREListagemBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.bean.GerarDREListagemLancamentosBean;
import br.com.linkcom.sined.modulo.contabil.controller.process.filter.GerarDREFiltro;

@Controller(path="/contabil/process/GerarDRE", authorizationModule=ProcessAuthorizationModule.class)
public class GerarDREProcess extends MultiActionController{

	private ConfiguracaodreService configuracaodreService;
	private MovimentacaocontabilService movimentacaocontabilService;
	
	public void setMovimentacaocontabilService(
			MovimentacaocontabilService movimentacaocontabilService) {
		this.movimentacaocontabilService = movimentacaocontabilService;
	}
	public void setConfiguracaodreService(
			ConfiguracaodreService configuracaodreService) {
		this.configuracaodreService = configuracaodreService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, GerarDREFiltro filtro){
		request.setAttribute("listaConfiguracaodreAtivos", configuracaodreService.findAtivos());
		return new ModelAndView("process/gerarDRE", "filtro", filtro);
	}
	
	public ModelAndView gerarPDF(WebRequestContext request, GerarDREFiltro filtro){
		try {
			return configuracaodreService.gerarDREPDF(filtro);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return sendRedirectToAction("index");
		}
	}
	
	public ModelAndView gerar(WebRequestContext request, GerarDREFiltro filtro){
		try {
			Configuracaodre configuracaodre = configuracaodreService.loadForDRE(filtro.getConfiguracaodre());
			GerarDREBean bean = configuracaodreService.gerarDRE(filtro, configuracaodre);
			request.setAttribute("filtro", filtro);
			return new ModelAndView("process/gerarDREResultado", "bean", bean);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
			return sendRedirectToAction("index");
		}
	}
	
	public ModelAndView listarLancamentos(WebRequestContext request, GerarDREFiltro filtro){
		List<GerarDREListagemLancamentosBean> lista = movimentacaocontabilService.findForDRE(filtro);
		request.setAttribute("lista", lista);
		return new ModelAndView("direct:process/gerarDREListagemLancamentos", "filtro", filtro);
	}
	
	public ModelAndView listar(WebRequestContext request, GerarDREFiltro filtro){
		List<GerarDREListagemBean> lista = configuracaodreService.gerarDREListagem(filtro);
		for (GerarDREListagemBean bean : lista) {
			if(bean.getHaveFilhos() != null && bean.getHaveFilhos()){
				Money valor = new Money();
				for (GerarDREListagemBean bean2 : lista) {
					if(!bean.getCdcontacontabil().equals(bean2.getCdcontacontabil()) &&
							bean2.getIdentificador().startsWith(bean.getIdentificador() + ".")){
						valor = valor.add(bean2.getValor());
					}
				}
				bean.setValor(valor);
			}
		}
		
		List<GerarDREListagemBean> listaFinal = new ArrayList<GerarDREListagemBean>();
		for (GerarDREListagemBean bean : lista) {
			if(bean.getValor().getValue().doubleValue() > 0 || bean.getValor().getValue().doubleValue() < 0){
				listaFinal.add(bean);
			}
		}
		
		request.setAttribute("lista", listaFinal);
		return new ModelAndView("direct:process/gerarDREListagem", "filtro", filtro);
	}
	
}
