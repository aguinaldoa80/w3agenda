package br.com.linkcom.sined.modulo.contabil.controller.process.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ContaContabilSaldoInicial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContaContabilSaldoInicialFiltro extends FiltroListagemSined{

	private Empresa empresa;
	private Arquivo arquivo;
	private List<ContaContabilSaldoInicial> listaBean;
	private List<ContaContabilSaldoInicial> listaBeanComErro;
	private String identificador;
	private Date dtInicio;
	private Date dtFim;
	
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public List<ContaContabilSaldoInicial> getListaBean() {
		return listaBean;
	}
	public List<ContaContabilSaldoInicial> getListaBeanComErro() {
		return listaBeanComErro;
	}
	public String getIdentificador() {
		return identificador;
	}
	public Date getDtInicio() {
		return dtInicio;
	}
	public Date getDtFim() {
		return dtFim;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setListaBean(List<ContaContabilSaldoInicial> listaBean) {
		this.listaBean = listaBean;
	}
	public void setListaBeanComErro(
			List<ContaContabilSaldoInicial> listaBeanComErro) {
		this.listaBeanComErro = listaBeanComErro;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
}
