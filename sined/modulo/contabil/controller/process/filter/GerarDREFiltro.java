package br.com.linkcom.sined.modulo.contabil.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Configuracaodre;
import br.com.linkcom.sined.geral.bean.Configuracaodretipo;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.SinedDateUtils;

public class GerarDREFiltro {

	private Empresa empresa;
	private Configuracaodre configuracaodre;
	private Date dtinicio = SinedDateUtils.firstDateOfMonth();
	private Date dtfim = SinedDateUtils.lastDateOfMonth();
	private Configuracaodretipo configuracaodretipo;
	private ContaContabil contacontabil;
	private Centrocusto centrocusto;
	private Projeto projeto;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Configura��o de DRE")
	public Configuracaodre getConfiguracaodre() {
		return configuracaodre;
	}
	@Required
	@DisplayName("In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	@DisplayName("Fim")
	public Date getDtfim() {
		return dtfim;
	}
	public Configuracaodretipo getConfiguracaodretipo() {
		return configuracaodretipo;
	}
	public ContaContabil getContacontabil() {
		return contacontabil;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public Projeto getProjeto() {
		return projeto;
	}
	public void setContacontabil(ContaContabil contacontabil) {
		this.contacontabil = contacontabil;
	}
	public void setConfiguracaodretipo(Configuracaodretipo configuracaodretipo) {
		this.configuracaodretipo = configuracaodretipo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setConfiguracaodre(Configuracaodre configuracaodre) {
		this.configuracaodre = configuracaodre;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
}
