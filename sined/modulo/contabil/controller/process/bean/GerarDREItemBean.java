package br.com.linkcom.sined.modulo.contabil.controller.process.bean;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.Configuracaodreoperacao;

public class GerarDREItemBean {
	
	private Integer id;
	private Configuracaodreoperacao operacao;
	private String descricao;
	private Money valor;
	
	public GerarDREItemBean() {
	}
	
	public GerarDREItemBean(Integer id, Configuracaodreoperacao operacao,
			String descricao, Money valor) {
		super();
		this.id = id;
		this.operacao = operacao;
		this.descricao = descricao;
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}
	public Money getValor() {
		return valor;
	}
	public Integer getId() {
		return id;
	}
	public Configuracaodreoperacao getOperacao() {
		return operacao;
	}
	public void setOperacao(Configuracaodreoperacao operacao) {
		this.operacao = operacao;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}

}
