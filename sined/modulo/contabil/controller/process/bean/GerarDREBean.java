package br.com.linkcom.sined.modulo.contabil.controller.process.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;

public class GerarDREBean {

	private Money receitaliquida;
	private Money resultadobruto;
	private Money resultadoliquidoantesimpostos;
	private Money resultadoliquido;
	
	private String receitaliquida_label = "(=) Receita L�quida";
	private String resultadobruto_label = "(=) Lucro Bruto";
	private String resultadoliquidoantesimpostos_label = "(=) Resultado antes Impostos";
	private String resultadoliquido_label = "(=) Resultado L�quido";
	
	private List<GerarDREItemBean> listaOperacionalBruto = new ArrayList<GerarDREItemBean>();
	private List<GerarDREItemBean> listaOperacionalLiquido = new ArrayList<GerarDREItemBean>();
	private List<GerarDREItemBean> listaNaoOperacional = new ArrayList<GerarDREItemBean>();
	
	private List<GerarDREImpostoBean> listaImposto = new ArrayList<GerarDREImpostoBean>();

	public Money getReceitaliquida() {
		return receitaliquida;
	}

	public Money getResultadobruto() {
		return resultadobruto;
	}

	public Money getResultadoliquidoantesimpostos() {
		return resultadoliquidoantesimpostos;
	}

	public Money getResultadoliquido() {
		return resultadoliquido;
	}

	public List<GerarDREItemBean> getListaOperacionalBruto() {
		return listaOperacionalBruto;
	}

	public List<GerarDREItemBean> getListaOperacionalLiquido() {
		return listaOperacionalLiquido;
	}

	public List<GerarDREItemBean> getListaNaoOperacional() {
		return listaNaoOperacional;
	}

	public List<GerarDREImpostoBean> getListaImposto() {
		return listaImposto;
	}

	public String getReceitaliquida_label() {
		return receitaliquida_label;
	}

	public String getResultadobruto_label() {
		return resultadobruto_label;
	}

	public String getResultadoliquidoantesimpostos_label() {
		return resultadoliquidoantesimpostos_label;
	}

	public String getResultadoliquido_label() {
		return resultadoliquido_label;
	}

	public void setReceitaliquida_label(String receitaliquida_label) {
		this.receitaliquida_label = receitaliquida_label;
	}

	public void setResultadobruto_label(String resultadobruto_label) {
		this.resultadobruto_label = resultadobruto_label;
	}

	public void setResultadoliquidoantesimpostos_label(
			String resultadoliquidoantesimpostos_label) {
		this.resultadoliquidoantesimpostos_label = resultadoliquidoantesimpostos_label;
	}

	public void setResultadoliquido_label(String resultadoliquido_label) {
		this.resultadoliquido_label = resultadoliquido_label;
	}

	public void setReceitaliquida(Money receitaliquida) {
		this.receitaliquida = receitaliquida;
	}

	public void setResultadobruto(Money resultadobruto) {
		this.resultadobruto = resultadobruto;
	}

	public void setResultadoliquidoantesimpostos(
			Money resultadoliquidoantesimpostos) {
		this.resultadoliquidoantesimpostos = resultadoliquidoantesimpostos;
	}

	public void setResultadoliquido(Money resultadoliquido) {
		this.resultadoliquido = resultadoliquido;
	}

	public void setListaOperacionalBruto(
			List<GerarDREItemBean> listaOperacionalBruto) {
		this.listaOperacionalBruto = listaOperacionalBruto;
	}

	public void setListaOperacionalLiquido(
			List<GerarDREItemBean> listaOperacionalLiquido) {
		this.listaOperacionalLiquido = listaOperacionalLiquido;
	}

	public void setListaNaoOperacional(List<GerarDREItemBean> listaNaoOperacional) {
		this.listaNaoOperacional = listaNaoOperacional;
	}

	public void setListaImposto(List<GerarDREImpostoBean> listaImposto) {
		this.listaImposto = listaImposto;
	}
	
}
