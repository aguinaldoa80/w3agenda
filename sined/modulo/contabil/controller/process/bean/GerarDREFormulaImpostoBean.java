package br.com.linkcom.sined.modulo.contabil.controller.process.bean;

import java.sql.Date;

public class GerarDREFormulaImpostoBean {
	
	private Double receitaliquida;
	private Double resultadobruto;
	private Double resultadoliquidoantesimpostos;
	private Double resultadoliquido;
	private Integer diasperiodo;
	private Date dtinicio;
	private Date dtfim;
	private String crt;

	public Double getReceitaliquida() {
		return receitaliquida;
	}

	public Double getResultadobruto() {
		return resultadobruto;
	}

	public Double getResultadoliquidoantesimpostos() {
		return resultadoliquidoantesimpostos;
	}

	public Double getResultadoliquido() {
		return resultadoliquido;
	}

	public Date getDtinicio() {
		return dtinicio;
	}

	public Date getDtfim() {
		return dtfim;
	}

	public String getCrt() {
		return crt;
	}
	
	public Integer getDiasperiodo() {
		return diasperiodo;
	}
	
	public void setDiasperiodo(Integer diasperiodo) {
		this.diasperiodo = diasperiodo;
	}

	public void setReceitaliquida(Double receitaliquida) {
		this.receitaliquida = receitaliquida;
	}

	public void setResultadobruto(Double resultadobruto) {
		this.resultadobruto = resultadobruto;
	}

	public void setResultadoliquidoantesimpostos(
			Double resultadoliquidoantesimpostos) {
		this.resultadoliquidoantesimpostos = resultadoliquidoantesimpostos;
	}

	public void setResultadoliquido(Double resultadoliquido) {
		this.resultadoliquido = resultadoliquido;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setCrt(String crt) {
		this.crt = crt;
	}
	
}
