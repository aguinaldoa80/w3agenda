package br.com.linkcom.sined.modulo.contabil.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class GerarDREListagemLancamentosBean {
	
	private Integer cdmovimentacaocontabil;
	private String tipo;
	private Date dtlancamento;
	private String historico;
	private Money valor;
	
	public GerarDREListagemLancamentosBean() {
	}
	
	public GerarDREListagemLancamentosBean(Integer cdmovimentacaocontabil, String tipo,
			Date dtlancamento, String historico, Money valor) {
		super();
		this.cdmovimentacaocontabil = cdmovimentacaocontabil;
		this.tipo = tipo;
		this.dtlancamento = dtlancamento;
		this.historico = historico;
		this.valor = valor;
	}

	public Integer getCdmovimentacaocontabil() {
		return cdmovimentacaocontabil;
	}
	public Date getDtlancamento() {
		return dtlancamento;
	}
	public Money getValor() {
		return valor;
	}
	public String getHistorico() {
		return historico;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setCdmovimentacaocontabil(Integer cdmovimentacaocontabil) {
		this.cdmovimentacaocontabil = cdmovimentacaocontabil;
	}
	public void setDtlancamento(Date dtlancamento) {
		this.dtlancamento = dtlancamento;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}

}
