package br.com.linkcom.sined.modulo.contabil.controller.process.bean;

import br.com.linkcom.neo.types.Money;

public class GerarDREListagemBean {
	
	private Integer cdcontacontabil;
	private String identificador;
	private String nome;
	private Money valor = new Money();
	private Boolean haveFilhos = Boolean.FALSE;
	
	public GerarDREListagemBean() {
	}
	
	public GerarDREListagemBean(Integer cdcontacontabil, String identificador,
			String nome, Money valor, Boolean haveFilhos) {
		super();
		this.cdcontacontabil = cdcontacontabil;
		this.identificador = identificador;
		this.nome = nome;
		this.valor = valor;
		this.haveFilhos = haveFilhos;
	}

	public String getIdentificador() {
		return identificador;
	}
	public String getNome() {
		return nome;
	}
	public Money getValor() {
		return valor;
	}
	public Integer getCdcontacontabil() {
		return cdcontacontabil;
	}
	public Boolean getHaveFilhos() {
		return haveFilhos;
	}
	public void setCdcontacontabil(Integer cdcontacontabil) {
		this.cdcontacontabil = cdcontacontabil;
	}
	public void setHaveFilhos(Boolean haveFilhos) {
		this.haveFilhos = haveFilhos;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}

}
