package br.com.linkcom.sined.modulo.contabil.controller.process.bean;

import br.com.linkcom.neo.types.Money;

public class GerarDREImpostoBean {
	
	private String descricao;
	private Double percentual = 0d;
	private Money valor = new Money();
	
	public String getDescricao() {
		return descricao;
	}
	public Double getPercentual() {
		return percentual;
	}
	public Money getValor() {
		return valor;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}

}
