package br.com.linkcom.sined.modulo.contabil.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.view.Vcontacontabil;
import br.com.linkcom.sined.geral.service.ContacontabilService;
import br.com.linkcom.sined.geral.service.FechamentoContabilService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.contabil.controller.crud.filter.ContacontabilFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Contacontabil", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"vcontacontabil.identificador", "formato", "nome", "ativo", "vcontacontabil.nivel"})
public class ContacontabilCrud extends CrudControllerSined<ContacontabilFiltro, ContaContabil, ContaContabil> {

	private ContacontabilService contacontabilService;
	private ParametrogeralService parametrogeralService;
	private MovimentacaocontabilService movimentacaocontabilService;
	private FechamentoContabilService fechamentoContabilService;
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setContacontabilService(ContacontabilService contacontabilService) {this.contacontabilService = contacontabilService;}
	public void setMovimentacaocontabilService(MovimentacaocontabilService movimentacaocontabilService) {this.movimentacaocontabilService = movimentacaocontabilService;}
	public void setFechamentoContabilService(FechamentoContabilService fechamentoContabilService) {this.fechamentoContabilService = fechamentoContabilService;}
	
	@Override
	protected void salvar(WebRequestContext request, ContaContabil bean) throws Exception {
				
		contacontabilService.preparaBeanParaSalvar(bean);
		
		Integer cdempresa = bean.getEmpresa() != null ? bean.getEmpresa().getCdpessoa() : null;
		
		try {
			bean.setNome(bean.getNome().replaceAll("\"", "''"));
			
			super.salvar(request, bean);
			
			if(Boolean.TRUE.equals(bean.getContaretificadora())){
				bean = contacontabilService.loadWithIdentificador(bean);
				contacontabilService.alteraFilhosRetificadora(bean.getVcontacontabil().getIdentificador(), cdempresa);				
			}			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CONTACONTABIL_NOME")) {
				throw new SinedException("Conta contabil j� cadastrada no sistema.");
			}else {
				throw new SinedException(e.getMessage());
			}
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, ContaContabil form)throws Exception {
		
		String stCopiar = request.getParameter("copiar");
		if (StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")) {
			
			ContaContabil contacontabilpai = new ContaContabil();
			contacontabilpai.setCdcontacontabil(Integer.parseInt(request.getParameter("cdcontacontabil")));
			form.setContaContabilPai(contacontabilpai);
			
			ContaContabil contaContabil = contacontabilService.loadForEntrada(contacontabilpai);
			if(contaContabil != null){
				form.setEmpresa(contaContabil.getEmpresa());
				form.setNatureza(contaContabil.getNatureza());
				form.setNaturezaContaCompensacao(contaContabil.getNaturezaContaCompensacao());
			}
			
			//Atributos a serem modificados.
			form.setCdcontacontabil(null);
			form.setNome(null);
			form.setTipooperacao(Tipooperacao.TIPO_CONTABIL);
			request.setAttribute("listaConta", contacontabilService.findByTipooperacao(form.getTipooperacao()));
			
		} else {
			if(form.getCdcontacontabil() == null){
				form.setTipooperacao(Tipooperacao.TIPO_CONTABIL);
			}
			
			if (form.getPai() != null) {
				form.setContaContabilPai(form.getPai());
			}
						
			form.setTipooperacao2(Tipooperacao.TIPO_CONTABIL);
			request.setAttribute("listaConta", contacontabilService.findByTipooperacao(form.getTipooperacao2()));
			
			if (form.getTipooperacao2() != null) {
				form.setTipooperacao(form.getTipooperacao2());
			}
		}
		
		if (form.getCdcontacontabil() != null) {
			if(form.getVcontacontabil() != null && form.getVcontacontabil().getCdContaContabil() != null && !Hibernate.isInitialized(form.getVcontacontabil())){
				form.setVcontacontabil(new Vcontacontabil());
				form.getVcontacontabil().setCdContaContabil(form.getVcontacontabil().getCdContaContabil());
			}
			request.setAttribute("insert", Boolean.FALSE);
			
			List<ContaContabil> listaFilhos = new ArrayList<ContaContabil>();
			if(form.getVcontacontabil() != null && Hibernate.isInitialized(form.getVcontacontabil()) && form.getVcontacontabil().getIdentificador() != null){
				listaFilhos = contacontabilService.findAllFilhos(form);
			}
			//o metodo findAllFilhos retorna a propria conta inclusive
			if(listaFilhos.size() > 1){
				Money saldo = new Money();
				for (ContaContabil filho : listaFilhos) {
                    if(Boolean.TRUE.equals(filho.getContaretificadora()) && !Boolean.TRUE.equals(form.getContaretificadora())){
						saldo = saldo.subtract(contacontabilService.calculaSaldoAtual(filho, SinedDateUtils.currentDate()));
					}else{
						saldo = saldo.add(contacontabilService.calculaSaldoAtual(filho, SinedDateUtils.currentDate()));						
					}
				}
				form.setSaldoAtual(saldo);
			}else{
				form.setSaldoAtual(contacontabilService.calculaSaldoAtual(form, SinedDateUtils.currentDate()));
			}	
		} else {
			request.setAttribute("insert", Boolean.TRUE);
		}
		
		Boolean haveMovimentacao = false;
		if(EDITAR.equals(request.getParameter(ACTION_PARAMETER)) && form.getCdcontacontabil() != null){
			if(form.getVcontacontabil() != null && Hibernate.isInitialized(form.getVcontacontabil()) && form.getVcontacontabil().getIdentificador() != null){
				haveMovimentacao = movimentacaocontabilService.haveMovimentacaoContabil(form.getVcontacontabil().getIdentificador());
			}
		}
		
		request.setAttribute("haveMovimentacao", haveMovimentacao);
		request.setAttribute("PLANO_CONTAS_CONTABIL_POR_EMPRESA", parametrogeralService.getBoolean("PLANO_CONTAS_CONTABIL_POR_EMPRESA"));
		request.setAttribute("listaNaturezaContagerencial", contacontabilService.getListaNaturezaContabil());
		request.setAttribute("listaNaturezaContaCompensacao", contacontabilService.getListaNaturezaContaCompensacao());
		request.setAttribute("listaTipooperacao", Tipooperacao.getListaTipooperacao(true));
	}

	
	/**
	 * Carregamento do n�vel via ajax para a tela de cadastro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findPais
	 * @see br.com.linkcom.sined.geral.bean.Contagerencial#getNivel
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxNivel(WebRequestContext request, ContaContabil bean) {
		contacontabilService.ajaxNivel(request, bean);
	}

	/**
	 * Carregamento do tipo de opera��o via ajax  para a tela de cadastro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#carregaConta
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxVerificaPaiRetificador(WebRequestContext request, ContaContabil bean) {
		contacontabilService.ajaxVerificaPaiRetificador(request, bean);
	}
	
	/**
	 * Carrega o campo transient boolean para fazer a confirma��o de update no banco.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#isAnalitica
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxAnalitica(WebRequestContext request, ContaContabil bean) {
		contacontabilService.ajaxAnalitica(request, bean);
	}
	
	public void ajaxItem(WebRequestContext request, ContaContabil bean) {
		contacontabilService.ajaxItem(request, bean);
	}
	
	@Override
	protected void listagem(WebRequestContext request, ContacontabilFiltro filtro) throws Exception {
		request.setAttribute("listaNaturezaContagerencial", contacontabilService.getListaNaturezaContabil());
		request.setAttribute("PLANO_CONTAS_CONTABIL_POR_EMPRESA", parametrogeralService.getBoolean("PLANO_CONTAS_CONTABIL_POR_EMPRESA"));
		super.listagem(request, filtro);
	}
	
	@Override
	protected void validateBean(ContaContabil bean, BindException errors) {
		if (bean.getItem()!=null && !contacontabilService.verificarItem(bean)){
			errors.reject("001", "J� possui conta cont�bil com este item cadastrado");
		}
		
		Date dtUltimoFechamentoContabil = fechamentoContabilService.carregarUltimaDataFechamento(bean.getEmpresa());
		if(dtUltimoFechamentoContabil != null 
				&& (bean.getDtSaldoInicial().equals(dtUltimoFechamentoContabil) || bean.getDtSaldoInicial().before(dtUltimoFechamentoContabil))) {
			errors.reject("001", "N�o � possivel criar uma conta cont�bil com data do saldo inicial igual ou anterior ao �ltimo fechamento cont�bil.");
		}
		
		
	}
	
	public void ajaxBuscaNaturezaContaContabil(WebRequestContext request, ContaContabil contacontabil){
		contacontabilService.ajaxBuscaNaturezaContaContabil(request, contacontabil);
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request,ContacontabilFiltro filtro) {
		return new ModelAndView("crud/contacontabilListagem");
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, ContaContabil form) {
		return new ModelAndView("crud/contacontabilEntrada");
	}
}