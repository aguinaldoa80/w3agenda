package br.com.linkcom.sined.modulo.contabil.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ContaContabilSaldoInicial;
import br.com.linkcom.sined.geral.service.ContaContabilSaldoInicialService;
import br.com.linkcom.sined.modulo.contabil.controller.process.filter.ContaContabilSaldoInicialFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/ContaContabilSaldoInicial", authorizationModule=CrudAuthorizationModule.class)
public class ContaContabilSaldoInicialCrud extends CrudControllerSined<ContaContabilSaldoInicialFiltro, ContaContabilSaldoInicial, ContaContabilSaldoInicial>{

	private ContaContabilSaldoInicialService contaContabilSaldoInicialService;
	
	public void setContaContabilSaldoInicialService(
			ContaContabilSaldoInicialService contaContabilSaldoInicialService) {
		this.contaContabilSaldoInicialService = contaContabilSaldoInicialService;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request,
			ContaContabilSaldoInicial form) throws CrudException {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request,
			ContaContabilSaldoInicial form) throws CrudException {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request,
			ContaContabilSaldoInicial form) throws CrudException {
		throw new SinedException("A��o n�o permitida");
	}
}
