package br.com.linkcom.sined.modulo.contabil.controller.crud;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.LoteContabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.service.FechamentoContabilService;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilService;
import br.com.linkcom.sined.modulo.contabil.controller.crud.filter.LoteContabilFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/contabil/crud/LoteContabil", authorizationModule=CrudAuthorizationModule.class)
public class LoteContabilCrud extends CrudControllerSined<LoteContabilFiltro, LoteContabil, LoteContabil> {
	private MovimentacaocontabilService movimentacaocontabilService;
	private FechamentoContabilService fechamentoContabilService;
	
	public void setMovimentacaocontabilService(MovimentacaocontabilService movimentacaocontabilService) {this.movimentacaocontabilService = movimentacaocontabilService;}
	public void setFechamentoContabilService(FechamentoContabilService fechamentoContabilService) {this.fechamentoContabilService = fechamentoContabilService;}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, LoteContabil form) throws CrudException {
		request.addError("A��o n�o permitida");
		return sendRedirectToAction("listagem");
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, LoteContabil form)
			throws CrudException {
		request.addError("A��o n�o permitida");
		return sendRedirectToAction("listagem");
	}
	
	@Override
	public ModelAndView doConsultar(WebRequestContext request, LoteContabil form)
			throws CrudException {
		request.addError("A��o n�o permitida");
		return sendRedirectToAction("listagem");
	}
	
	@Override
	protected void excluir(WebRequestContext request, LoteContabil bean) throws Exception {
		String itens = request.getParameter("itenstodelete");
		
		if(StringUtils.isNotBlank(itens)) {
			List<Movimentacaocontabil> listaMovimentacoes = movimentacaocontabilService.carregarMovimentacoesContabeisPorLote(itens);
			
			if(SinedUtil.isListNotEmpty(listaMovimentacoes)) {
				Map<Integer, Date> mapaFechamentosContabeis = fechamentoContabilService.montarMapaEmpresaDataFechamento(new SinedUtil().getListaEmpresa());
				for(Movimentacaocontabil movimentacao : listaMovimentacoes) {
					Date ultimoFechamentoContabil = mapaFechamentosContabeis.get(movimentacao.getEmpresa().getCdpessoa());
					if(ultimoFechamentoContabil != null
							&&(movimentacao.getDtlancamento().equals(ultimoFechamentoContabil) || movimentacao.getDtlancamento().before(ultimoFechamentoContabil))) {
						throw new SinedException("N�o � possivel excluir um lote de lan�amentos cont�beis com data igual ou anterior ao �ltimo fechamento cont�bil.");
					}
				}
			}
		}
		
		super.excluir(request, bean);
	}
}
