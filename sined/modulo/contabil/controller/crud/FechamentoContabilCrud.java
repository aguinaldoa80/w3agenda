package br.com.linkcom.sined.modulo.contabil.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FechamentoContabil;
import br.com.linkcom.sined.geral.bean.FechamentoContabilHistorico;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.geral.service.FechamentoContabilHistoricoService;
import br.com.linkcom.sined.geral.service.FechamentoContabilService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.contabil.controller.crud.filter.FechamentoContabilFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/contabil/crud/FechamentoContabil", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"empresa", "dtfechamento", "motivo"})
public class FechamentoContabilCrud extends CrudControllerSined<FechamentoContabilFiltro, FechamentoContabil, FechamentoContabil> {
	private UsuarioService usuarioService;
	private FechamentoContabilService fechamentoContabilService;
	private FechamentoContabilHistoricoService fechamentoContabilHistoricoService;
		
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setFechamentoContabilService(FechamentoContabilService fechamentoContabilService) {this.fechamentoContabilService = fechamentoContabilService;}
	public void setFechamentoContabilHistoricoService(FechamentoContabilHistoricoService fechamentoContabilHistoricoService) {this.fechamentoContabilHistoricoService = fechamentoContabilHistoricoService;}
	
	@Override
	protected void entrada(WebRequestContext request, FechamentoContabil form) throws Exception {
		Usuario usuarioLogado = usuarioService.carregaUsuarioWithEmpresa(SinedUtil.getUsuarioLogado());
		if(usuarioLogado != null && SinedUtil.isListNotEmpty(usuarioLogado.getListaUsuarioempresa())) {
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			for(Usuarioempresa usuarioEmpresa : usuarioLogado.getListaUsuarioempresa()) {
				listaEmpresa.add(usuarioEmpresa.getEmpresa());
			}
			
			request.setAttribute("listaEmpresasPermitidasAoUsuario", listaEmpresa);
		}
		
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(FechamentoContabil bean, BindException errors) {
		if(fechamentoContabilService.existeFechamentoCadastradoMesmosDados(bean)) {
			errors.reject("001", "J� existe um fechamento cont�bil com os mesmos dados cadastrados.");
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, FechamentoContabil bean) throws Exception {
		FechamentoContabil anterior = fechamentoContabilService.loadForEntrada(bean);
		FechamentoContabilHistorico historico = fechamentoContabilHistoricoService.preencherHistorico(bean, anterior);
		
		super.salvar(request, bean);
		
		if(historico.getFechamentoContabil() == null) {
			historico.setFechamentoContabil(bean);
		}
		
		fechamentoContabilHistoricoService.saveOrUpdate(historico);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, FechamentoContabilFiltro filtro) throws CrudException {
		Usuario usuarioLogado = usuarioService.carregaUsuarioWithEmpresa(SinedUtil.getUsuarioLogado());
		if(usuarioLogado != null && SinedUtil.isListNotEmpty(usuarioLogado.getListaUsuarioempresa())) {
			List<Empresa> listaEmpresa = new ArrayList<Empresa>();
			for(Usuarioempresa usuarioEmpresa : usuarioLogado.getListaUsuarioempresa()) {
				listaEmpresa.add(usuarioEmpresa.getEmpresa());
			}
			
			request.setAttribute("listaEmpresasPermitidasAoUsuario", listaEmpresa);
		}
		
		return super.doListagem(request, filtro);
	}
}
