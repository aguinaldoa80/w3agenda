package br.com.linkcom.sined.modulo.contabil.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LoteContabilFiltro extends FiltroListagemSined {
	private Integer cdLoteContabil;
	
	@DisplayName("ID do Lote Cont�bil")
	public Integer getCdLoteContabil() {
		return cdLoteContabil;
	}
	
	public void setCdLoteContabil(Integer cdLoteContabil) {
		this.cdLoteContabil = cdLoteContabil;
	}
}
