package br.com.linkcom.sined.modulo.contabil.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FechamentoContabilFiltro extends FiltroListagemSined {
	protected Date dataInicio;
	protected Date dataFim;
	protected String motivo;
	protected Empresa empresa;
	
	public FechamentoContabilFiltro() {}
	
	public Date getDataInicio() {
		return dataInicio;
	}
	
	public Date getDataFim() {
		return dataFim;
	}
	
	@DisplayName("Motivo")
	public String getMotivo() {
		return motivo;
	}
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
