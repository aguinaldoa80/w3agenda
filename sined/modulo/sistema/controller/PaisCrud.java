package br.com.linkcom.sined.modulo.sistema.controller;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Pais;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PaisFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Pais"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdpais", "nome"})
public class PaisCrud extends CrudControllerSined<PaisFiltro, Pais, Pais> {

}
