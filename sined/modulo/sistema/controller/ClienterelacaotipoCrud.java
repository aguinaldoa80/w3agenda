package br.com.linkcom.sined.modulo.sistema.controller;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Clienterelacaotipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ClienterelacaotipoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Clienterelacaotipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class ClienterelacaotipoCrud extends CrudControllerSined<ClienterelacaotipoFiltro, Clienterelacaotipo, Clienterelacaotipo>{

	
}
