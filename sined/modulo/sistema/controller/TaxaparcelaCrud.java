package br.com.linkcom.sined.modulo.sistema.controller;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Taxaparcela;
import br.com.linkcom.sined.geral.service.TaxaparcelaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TaxaparcelaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Taxaparcela"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"numeroparcelas", "taxa"})
public class TaxaparcelaCrud extends CrudControllerSined<TaxaparcelaFiltro, Taxaparcela, Taxaparcela> {
	
	private TaxaparcelaService taxaparcelaService;
	
	public void setTaxaparcelaService(TaxaparcelaService taxaparcelaService) {
		this.taxaparcelaService = taxaparcelaService;
	}
	
	@Override
	protected void validateBean(Taxaparcela bean, BindException errors) {
		if (this.isTaxaParcelaCadastrada(bean)) {
			errors.reject("001", "J� existe taxa cadastrada para este n�mero de parcelas.");
		}
		super.validateBean(bean, errors);
	}
	public boolean isTaxaParcelaCadastrada(Taxaparcela taxaparcela){
		return taxaparcelaService.isTaxaParcelaCadastrada(taxaparcela);
	}
}