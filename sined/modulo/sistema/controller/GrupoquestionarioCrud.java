package br.com.linkcom.sined.modulo.sistema.controller;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Grupoquestionario;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.GrupoquestionarioService;
import br.com.linkcom.sined.geral.service.QuestionarioquestaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GrupoquestionarioFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/sistema/crud/Grupoquestionario", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "ativo"})
public class GrupoquestionarioCrud extends CrudControllerSined<GrupoquestionarioFiltro, Grupoquestionario, Grupoquestionario> {
	
	QuestionarioquestaoService questionarioquestaoService;
	GrupoquestionarioService grupoquestionarioService;
	AtividadetipoService atividadetipoService;
	
	public void setQuestionarioquestaoService(QuestionarioquestaoService questionarioquestaoService) {
		this.questionarioquestaoService = questionarioquestaoService;
	}
	
	public void setGrupoquestionarioService(GrupoquestionarioService grupoquestionarioService) {
		this.grupoquestionarioService = grupoquestionarioService;
	}
	
	public void setAtividadetipoService(
			AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	
	@Override
	protected void validateBean(Grupoquestionario bean, BindException errors) {
		Grupoquestionario grupoquestionario_aux = grupoquestionarioService.findGrupoquestionarioByDescricao(bean);
		if (bean != null && StringUtils.isNotBlank(bean.getDescricao()) && grupoquestionario_aux != null && StringUtils.isNotBlank(grupoquestionario_aux.getDescricao())) {
			if (bean.getDescricao().equalsIgnoreCase(grupoquestionario_aux.getDescricao())) {
				if ((bean.getCdgrupoquestionario() != null && !bean.equals(grupoquestionario_aux)) || bean.getCdgrupoquestionario() == null) {
					errors.reject("1","Grupo de Quest�es j� cadastrado no sistema.");
				}
			}
		}
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Grupoquestionario form)
			throws Exception {
		// TODO Auto-generated method stub
		super.entrada(request, form);
		request.setAttribute("listaAtividadetipo", atividadetipoService.findAtivos());
	}
}
