package br.com.linkcom.sined.modulo.sistema.controller.report;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ChequeFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/sistema/relatorio/Cheque", authorizationModule=ReportAuthorizationModule.class)
public class ChequeReport extends SinedReport<ChequeFiltro> {
	
	private ChequeService chequeService;
	
	public void setChequeService(ChequeService chequeService) {
		this.chequeService = chequeService;
	}

	
	@Override
	public IReport createReportSined(WebRequestContext request,	ChequeFiltro filtro) throws Exception {
		return chequeService.gerarPDF(filtro);
	}
	
	@Override
	public String getNomeArquivo() {
		return "cheques";
	}


	@Override
	public String getTitulo(ChequeFiltro filtro) {
		return "Relat�rio de Cheque";
	}
	
	public ModelAndView gerarExcel(WebRequestContext request,	ChequeFiltro filtro){
		
		List<Cheque> lista = chequeService.findForReport(filtro);
		
		StringBuilder retorno = new StringBuilder();
		retorno.append("Banco;Ag�ncia;Conta;N�mero;Emitente;Bom para;Valor;");
		
		for (Cheque cheque: lista){
			retorno.append("\n");
			retorno.append(cheque.getBanco()!=null ? cheque.getBanco().toString() : "");
			retorno.append(";");
			retorno.append(cheque.getAgencia()!=null ? cheque.getAgencia().toString() : "");
			retorno.append(";");
			retorno.append(cheque.getConta()!=null ? cheque.getConta().toString() : "");
			retorno.append(";");
			retorno.append(cheque.getNumero()!=null ? cheque.getNumero().toString() : "");
			retorno.append(";");
			retorno.append(cheque.getEmitente()!=null ? cheque.getEmitente() : "");
			retorno.append(";");
			retorno.append(cheque.getDtbompara()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(cheque.getDtbompara()) : "");
			retorno.append(";");
			retorno.append(cheque.getValor()!=null ? cheque.getValor().toString() : "");
			retorno.append(";");
		}
		
		Resource resource = new Resource("text/csv", "cheque.csv", retorno.toString().getBytes());
		
		return new ResourceModelAndView(resource);
	}
}
