package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.TelefoneBean;
import br.com.linkcom.sined.util.rest.arvoreacesso.EnderecoBean;

public class EmpresaBean {

	protected String nome;
	protected String nomefantasia;
	protected String razaosocial;
	protected String cnpj;
	protected String inscricaoestadual;
	protected String inscricaomunicipal;
	protected LinkedList<EnderecoBean> endereco;
	protected LinkedList<TelefoneBean> telefone;
	protected String site;
	protected String email;
	protected String informacoesadicionaiscontribuinte;
	
	public EmpresaBean(Empresa empresa){
		this.setEndereco(new LinkedList<EnderecoBean>());
		this.setTelefone(new LinkedList<TelefoneBean>());
		if(empresa != null){
			this.setNome(empresa.getNome());
			this.setNomefantasia(empresa.getNomeFantasiaProprietarioPrincipalOuEmpresa());
			this.setRazaosocial(empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa());
			this.setCnpj(empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa() != null? empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa(): "");
			this.setInscricaoestadual(empresa.getInscricaoestadual());
			this.setInscricaomunicipal(empresa.getInscricaomunicipal());
			this.setSite(empresa.getSite());
			this.setEmail(empresa.getEmail());
			this.setInformacoesadicionaiscontribuinte(empresa.getTextoinfcontribuinte());
			
			if(empresa.getListaEndereco() != null){
				for(Endereco end: empresa.getListaEndereco()){
					this.getEndereco().add(new EnderecoBean(end));
				}				
			}
			
			if(empresa.getListaTelefone() != null){
				for(Telefone tel: empresa.getListaTelefone()){
					this.getTelefone().add(new TelefoneBean(tel));
				}				
			}
		}
	}
	
	public String getNome() {
		return Util.strings.emptyIfNull(nome);
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getNomefantasia() {
		return Util.strings.emptyIfNull(nomefantasia);
	}
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	
	public String getRazaosocial() {
		return Util.strings.emptyIfNull(razaosocial);
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	
	public String getCnpj() {
		return Util.strings.emptyIfNull(cnpj);
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public String getInscricaoestadual() {
		return Util.strings.emptyIfNull(inscricaoestadual);
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	
	public String getInscricaomunicipal() {
		return Util.strings.emptyIfNull(inscricaomunicipal);
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}
	
	public LinkedList<EnderecoBean> getEndereco() {
		return endereco;
	}
	public void setEndereco(LinkedList<EnderecoBean> endereco) {
		this.endereco = endereco;
	}
	
	public LinkedList<TelefoneBean> getTelefone() {
		return telefone;
	}
	public void setTelefone(LinkedList<TelefoneBean> telefone) {
		this.telefone = telefone;
	}
	
	public String getSite() {
		return Util.strings.emptyIfNull(site);
	}
	public void setSite(String site) {
		this.site = site;
	}
	
	public String getEmail() {
		return Util.strings.emptyIfNull(email);
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getInformacoesadicionaiscontribuinte() {
		return Util.strings.emptyIfNull(informacoesadicionaiscontribuinte);
	}
	public void setInformacoesadicionaiscontribuinte(
			String informacoesadicionaiscontribuinte) {
		this.informacoesadicionaiscontribuinte = informacoesadicionaiscontribuinte;
	}
}
