package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import java.sql.Date;
import java.util.LinkedList;

public class GarantiaReformaBean {

	private Integer cdGarantiaReforma;
	private Date dtGarantia;
	private Integer cdPedidoVenda;
	private String idExternoPedidoVenda;
	private String vendedorNome;
	private Integer cdPedidoVendaOrigem;
	private String pedidoExterno;
	private String garantiaSituacao;
	private GarantiaReformaEmpresaBean garantiaReformaEmpresaBean;
	private GarantiaReformaClienteBean garantiaReformaClienteBean;
	private LinkedList<GarantiaReformaPneuBean> listaGarantiaReformaPneuBean;
	private GarantiaReformaResultadoBean garantiaReformaResultadoBean;

	public Integer getCdGarantiaReforma() {
		return cdGarantiaReforma;
	}

	public void setCdGarantiaReforma(Integer cdGarantiaReforma) {
		this.cdGarantiaReforma = cdGarantiaReforma;
	}

	public Date getDtGarantia() {
		return dtGarantia;
	}

	public void setDtGarantia(Date dtGarantia) {
		this.dtGarantia = dtGarantia;
	}

	public Integer getCdPedidoVenda() {
		return cdPedidoVenda;
	}

	public void setCdPedidoVenda(Integer cdPedidoVenda) {
		this.cdPedidoVenda = cdPedidoVenda;
	}

	public String getIdExternoPedidoVenda() {
		return idExternoPedidoVenda;
	}

	public void setIdExternoPedidoVenda(String idExternoPedidoVenda) {
		this.idExternoPedidoVenda = idExternoPedidoVenda;
	}

	public String getVendedorNome() {
		return vendedorNome;
	}

	public void setVendedorNome(String vendedorNome) {
		this.vendedorNome = vendedorNome;
	}

	public Integer getCdPedidoVendaOrigem() {
		return cdPedidoVendaOrigem;
	}

	public void setCdPedidoVendaOrigem(Integer cdPedidoVendaOrigem) {
		this.cdPedidoVendaOrigem = cdPedidoVendaOrigem;
	}

	public String getPedidoExterno() {
		return pedidoExterno;
	}
	
	public void setPedidoExterno(String pedidoExterno) {
		this.pedidoExterno = pedidoExterno;
	}

	public String getGarantiaSituacao() {
		return garantiaSituacao;
	}

	public void setGarantiaSituacao(String garantiaSituacao) {
		this.garantiaSituacao = garantiaSituacao;
	}

	public GarantiaReformaEmpresaBean getGarantiaReformaEmpresaBean() {
		return garantiaReformaEmpresaBean;
	}
	
	public void setGarantiaReformaEmpresaBean(GarantiaReformaEmpresaBean garantiaReformaEmpresaBean) {
		this.garantiaReformaEmpresaBean = garantiaReformaEmpresaBean;
	}
	
	public GarantiaReformaClienteBean getGarantiaReformaClienteBean() {
		return garantiaReformaClienteBean;
	}
	
	public void setGarantiaReformaClienteBean(GarantiaReformaClienteBean garantiaReformaClienteBean) {
		this.garantiaReformaClienteBean = garantiaReformaClienteBean;
	}
	
	public LinkedList<GarantiaReformaPneuBean> getListaGarantiaReformaPneuBean() {
		return listaGarantiaReformaPneuBean;
	}
	
	public void setListaGarantiaReformaPneuBean(LinkedList<GarantiaReformaPneuBean> listaGarantiaReformaPneuBean) {
		this.listaGarantiaReformaPneuBean = listaGarantiaReformaPneuBean;
	}
	
	public GarantiaReformaResultadoBean getGarantiaReformaResultadoBean() {
		return garantiaReformaResultadoBean;
	}
	
	public void setGarantiaReformaResultadoBean(GarantiaReformaResultadoBean garantiaReformaResultadoBean) {
		this.garantiaReformaResultadoBean = garantiaReformaResultadoBean;
	}
}
