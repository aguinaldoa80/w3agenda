package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import java.util.LinkedList;

public class PedidoVendaReportBean {

	private Integer codigo;
	private String responsavel;
	private String idExterno;
	private String campoAdicional;
	private String dataPedido;
	private String logradouroEnderecoCliente;
	private String numeroEnderecoCliente;
	private String bairroEnderecoCliente;
	private String cidadeEnderecoCliente;
	private String ufEnderecoCliente;
	private String cepEnderecoCliente;
	private String complementoEnderecoCliente;
	private String enderecoCliente;
	private LinkedList<CampoAdicionalReportBean> listaCampoAdicional = new LinkedList<CampoAdicionalReportBean>();
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getIdExterno() {
		return idExterno;
	}
	public void setIdExterno(String idExterno) {
		this.idExterno = idExterno;
	}
	public String getCampoAdicional() {
		return campoAdicional;
	}
	public void setCampoAdicional(String campoAdicional) {
		this.campoAdicional = campoAdicional;
	}
	public String getDataPedido() {
		return dataPedido;
	}
	public void setDataPedido(String dataPedido) {
		this.dataPedido = dataPedido;
	}
	public String getLogradouroEnderecoCliente() {
		return logradouroEnderecoCliente;
	}
	public void setLogradouroEnderecoCliente(String logradouroEnderecoCliente) {
		this.logradouroEnderecoCliente = logradouroEnderecoCliente;
	}
	public String getNumeroEnderecoCliente() {
		return numeroEnderecoCliente;
	}
	public void setNumeroEnderecoCliente(String numeroEnderecoCliente) {
		this.numeroEnderecoCliente = numeroEnderecoCliente;
	}
	public String getBairroEnderecoCliente() {
		return bairroEnderecoCliente;
	}
	public void setBairroEnderecoCliente(String bairroEnderecoCliente) {
		this.bairroEnderecoCliente = bairroEnderecoCliente;
	}
	public String getCidadeEnderecoCliente() {
		return cidadeEnderecoCliente;
	}
	public void setCidadeEnderecoCliente(String cidadeEnderecoCliente) {
		this.cidadeEnderecoCliente = cidadeEnderecoCliente;
	}
	public String getUfEnderecoCliente() {
		return ufEnderecoCliente;
	}
	public void setUfEnderecoCliente(String ufEnderecoCliente) {
		this.ufEnderecoCliente = ufEnderecoCliente;
	}
	public String getCepEnderecoCliente() {
		return cepEnderecoCliente;
	}
	public void setCepEnderecoCliente(String cepEnderecoCliente) {
		this.cepEnderecoCliente = cepEnderecoCliente;
	}
	public String getComplementoEnderecoCliente() {
		return complementoEnderecoCliente;
	}
	public void setComplementoEnderecoCliente(String complementoEnderecoCliente) {
		this.complementoEnderecoCliente = complementoEnderecoCliente;
	}
	public LinkedList<CampoAdicionalReportBean> getListaCampoAdicional() {
		return listaCampoAdicional;
	}
	public void setListaCampoAdicional(
			LinkedList<CampoAdicionalReportBean> listaCampoAdicional) {
		this.listaCampoAdicional = listaCampoAdicional;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public String getEnderecoCliente() {
		return enderecoCliente;
	}
	public void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}
	
}
