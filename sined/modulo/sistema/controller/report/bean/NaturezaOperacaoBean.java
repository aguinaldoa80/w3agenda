package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;

public class NaturezaOperacaoBean {

	
	public NaturezaOperacaoBean(Naturezaoperacao naturezaoperacao){
		if(naturezaoperacao != null){
			this.setInformacoesContribuinte(naturezaoperacao.getInfoadicionalcontrib());
		}
	}
	
	private String informacoescontribuinte;
	
	public String getInformacoesContribuinte() {
		return Util.strings.emptyIfNull(informacoescontribuinte);
	}
	
	public void setInformacoesContribuinte(String informacoescontribuinte) {
		this.informacoescontribuinte = informacoescontribuinte;
	}
}
