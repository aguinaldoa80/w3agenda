package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import java.util.LinkedList;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.util.SinedUtil;

public class ColetaInfoContribuinteBean {

	private String observacao;
	private String tipo;
	private String pessoa;
	private String motivodevolucao;
	private String coletor;
	private Integer pedidovenda_id;
	private String notafiscal_id;
	private String entradafiscal_id;
	private LinkedList<ColetaMaterialInfoContribuinteBean> listaColetaMaterial;
	
	
	public ColetaInfoContribuinteBean(Coleta coleta, String notafiscal_id, String entradafiscal_id){
		this.listaColetaMaterial = new LinkedList<ColetaMaterialInfoContribuinteBean>();
		
		if(coleta != null){
			this.setObservacao(coleta.getObservacao());
			this.setTipo(coleta.getTipo()!= null? coleta.getTipo().name(): "");
			this.setColetor(coleta.getColaborador() != null? coleta.getColaborador().getNome(): "");
			this.setMotivodevolucao(coleta.getMotivodevolucaoItens());
			this.setPessoa(coleta.getClienteFornecedorListagem());
			this.setPedidovenda_id(coleta.getPedidovenda() != null? coleta.getPedidovenda().getCdpedidovenda(): 0);
			this.motivodevolucao = getMotivodevolucao() != null ? getMotivodevolucao() : "";
			this.notafiscal_id = StringUtils.isNotBlank(notafiscal_id) ? notafiscal_id : "";
			this.entradafiscal_id = StringUtils.isNotBlank(entradafiscal_id) ? entradafiscal_id : "";
			
			if(SinedUtil.isListNotEmpty(coleta.getListaColetaMaterial())){
				for(ColetaMaterial coletaMaterial : coleta.getListaColetaMaterial()){
					listaColetaMaterial.add(new ColetaMaterialInfoContribuinteBean(coletaMaterial));
				}
			}
		}
	}
	
	
	public String getObservacao() {
		return Util.strings.emptyIfNull(observacao);
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getTipo(){ 
		return Util.strings.emptyIfNull(tipo);
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getPessoa() {
		return Util.strings.emptyIfNull(pessoa);
	}
	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}
	public String getMotivodevolucao() {
		return Util.strings.emptyIfNull(motivodevolucao);
	}
	public void setMotivodevolucao(String motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	public String getColetor() {
		return coletor;
	}
	public void setColetor(String coletor) {
		this.coletor = Util.strings.emptyIfNull(coletor);
	}
	public Integer getPedidovenda_id() {
		return pedidovenda_id;
	}
	public void setPedidovenda_id(Integer pedidovendaId) {
		pedidovenda_id = pedidovendaId;
	}
	public String getNotafiscal_id() {
		return Util.strings.emptyIfNull(notafiscal_id);
	}
	public void setNotafiscal_id(String notafiscalId) {
		notafiscal_id = notafiscalId;
	}
	public String getEntradafiscal_id() {
		return Util.strings.emptyIfNull(entradafiscal_id);
	}
	public void setEntradafiscal_id(String entradafiscalId) {
		entradafiscal_id = entradafiscalId;
	}

	public LinkedList<ColetaMaterialInfoContribuinteBean> getListaColetaMaterial() {
		return listaColetaMaterial;
	}

	public void setListaColetaMaterial(LinkedList<ColetaMaterialInfoContribuinteBean> listaColetaMaterial) {
		this.listaColetaMaterial = listaColetaMaterial;
	}
}
