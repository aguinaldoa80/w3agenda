package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import java.util.Date;

public class PedidoVendaMaterialReportBean {

	private String codigo;
	private String nome;
	private String cicloProducao;
	private Integer item;
	private Integer quantidadeItensPedido;
	private Date prazoEntrega;
	public String getCodigo() {
		return codigo;
	}
	public String getNome() {
		return nome;
	}
	public String getCicloProducao() {
		return cicloProducao;
	}
	public Integer getItem() {
		return item;
	}
	public Integer getQuantidadeItensPedido() {
		return quantidadeItensPedido;
	}
	public Date getPrazoEntrega() {
		return prazoEntrega;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCicloProducao(String cicloProducao) {
		this.cicloProducao = cicloProducao;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	public void setQuantidadeItensPedido(Integer quantidadeItensPedido) {
		this.quantidadeItensPedido = quantidadeItensPedido;
	}
	public void setPrazoEntrega(Date prazoEntrega) {
		this.prazoEntrega = prazoEntrega;
	}
	
	
}
