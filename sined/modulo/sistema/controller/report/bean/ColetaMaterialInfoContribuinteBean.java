package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.PneuReportBean;
import br.com.linkcom.sined.util.SinedUtil;

public class ColetaMaterialInfoContribuinteBean {

	private String observacao;
	private Double quantidade;
	private Double valorunitario;
	private Double quantidadedevolvida;
	private String material_nome;
	private String localentrada_nome;
	private String motivodevolucao_descricao;
	private Integer cdpedidovendamaterial;
	private List<Integer> listaCdpedidovendamaterial;
	private PneuReportBean pneuReportBean;
	
	public ColetaMaterialInfoContribuinteBean(ColetaMaterial coletamaterial){
		if(coletamaterial!= null){
			this.setObservacao(coletamaterial.getObservacao());
			this.setQuantidade(coletamaterial.getQuantidade());
			this.setValorunitario(coletamaterial.getValorunitario());
			this.setQuantidadedevolvida(coletamaterial.getQuantidadedevolvida());
			this.setMaterial_nome(coletamaterial.getMaterial() != null? coletamaterial.getMaterial().getNome(): "");
			this.setLocalentrada_nome(coletamaterial.getLocalarmazenagem() != null? coletamaterial.getLocalarmazenagem().getNome(): "");
//			this.setMotivodevolucao_descricao(coletamaterial.getMotivodevolucao() != null? coletamaterial.getMotivodevolucao().getDescricao(): "");
			this.setMotivodevolucao_descricao(Hibernate.isInitialized(coletamaterial.getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(coletamaterial.getListaColetamaterialmotivodevolucao()) ? CollectionsUtil.listAndConcatenate(coletamaterial.getListaColetamaterialmotivodevolucao(), "motivodevolucao.descricao", ", ") : "");
			
			if(SinedUtil.isListNotEmpty(coletamaterial.getListaColetaMaterialPedidovendamaterial())) {
				this.listaCdpedidovendamaterial = new ArrayList<Integer>();
				if(coletamaterial.getListaColetaMaterialPedidovendamaterial().size() > 1) {
					for(ColetaMaterialPedidovendamaterial itemServico : coletamaterial.getListaColetaMaterialPedidovendamaterial()) {
						this.listaCdpedidovendamaterial.add(itemServico.getPedidovendamaterial().getCdpedidovendamaterial());
					}
				} else {
					this.setCdpedidovendamaterial(coletamaterial.getListaColetaMaterialPedidovendamaterial().get(0)
							.getPedidovendamaterial().getCdpedidovendamaterial());
				}
			} else {
				this.setCdpedidovendamaterial(0);
			}

			this.setPneuReportBean(new PneuReportBean(coletamaterial.getPneu()));
		}
	}
	
	public String getObservacao() {
		return Util.strings.emptyIfNull(observacao);
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public Double getValorunitario() {
		return valorunitario;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public Double getQuantidadedevolvida() {
		return quantidadedevolvida;
	}
	public void setQuantidadedevolvida(Double quantidadedevolvida) {
		this.quantidadedevolvida = quantidadedevolvida;
	}
	public String getMaterial_nome() {
		return Util.strings.emptyIfNull(material_nome);
	}
	public void setMaterial_nome(String materialNome) {
		material_nome = materialNome;
	}
	public String getLocalentrada_nome() {
		return Util.strings.emptyIfNull(localentrada_nome);
	}
	public void setLocalentrada_nome(String localentradaNome) {
		localentrada_nome = localentradaNome;
	}
	public String getMotivodevolucao_descricao() {
		return Util.strings.emptyIfNull(motivodevolucao_descricao);
	}
	public void setMotivodevolucao_descricao(String motivodevolucaoDescricao) {
		motivodevolucao_descricao = motivodevolucaoDescricao;
	}
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public PneuReportBean getPneuReportBean() {
		return pneuReportBean;
	}
	public void setPneuReportBean(PneuReportBean pneuReportBean) {
		this.pneuReportBean = pneuReportBean;
	}
	public List<Integer> getListaCdpedidovendamaterial() {
		return listaCdpedidovendamaterial;
	}
	public void setListaCdpedidovendamaterial(
			List<Integer> listaCdpedidovendamaterial) {
		this.listaCdpedidovendamaterial = listaCdpedidovendamaterial;
	}
}
