package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import java.util.LinkedList;

import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.PneuReportBean;

public class ProducaoAgendaMaterialReportBean {
	
	private PneuReportBean pneuReportBean;
	private LinkedList<ProducaoPneuReportBean> listaProducaoPneuReportBean = new LinkedList<ProducaoPneuReportBean>();
	private EmpresaReportBean empresaReportBean;
	private ClienteReportBean clienteReportBean;
	private PedidoVendaReportBean pedidoVendaReportBean;
	private PedidoVendaMaterialReportBean pedidoVendaMaterialReportBean;
	private Integer situacaoProducaoOrdem;
	
	public PneuReportBean getPneuReportBean() {
		return pneuReportBean;
	}
	public LinkedList<ProducaoPneuReportBean> getListaProducaoPneuReportBean() {
		return listaProducaoPneuReportBean;
	}
	public EmpresaReportBean getEmpresaReportBean() {
		return empresaReportBean;
	}
	public ClienteReportBean getClienteReportBean() {
		return clienteReportBean;
	}
	public PedidoVendaReportBean getPedidoVendaReportBean() {
		return pedidoVendaReportBean;
	}
	public PedidoVendaMaterialReportBean getPedidoVendaMaterialReportBean() {
		return pedidoVendaMaterialReportBean;
	}
	public Integer getSituacaoProducaoOrdem() {
		return situacaoProducaoOrdem;
	}
	public void setPneuReportBean(PneuReportBean pneuReportBean) {
		this.pneuReportBean = pneuReportBean;
	}
	public void setListaProducaoPneuReportBean(LinkedList<ProducaoPneuReportBean> listaProducaoPneuReportBean) {
		this.listaProducaoPneuReportBean = listaProducaoPneuReportBean;
	}
	public void setEmpresaReportBean(EmpresaReportBean empresaReportBean) {
		this.empresaReportBean = empresaReportBean;
	}
	public void setClienteReportBean(ClienteReportBean clienteReportBean) {
		this.clienteReportBean = clienteReportBean;
	}
	public void setPedidoVendaReportBean(PedidoVendaReportBean pedidoVendaReportBean) {
		this.pedidoVendaReportBean = pedidoVendaReportBean;
	}
	public void setPedidoVendaMaterialReportBean(PedidoVendaMaterialReportBean pedidoVendaMaterialReportBean) {
		this.pedidoVendaMaterialReportBean = pedidoVendaMaterialReportBean;
	}
	public void setSituacaoProducaoOrdem(Integer situacaoProducaoOrdem) {
		this.situacaoProducaoOrdem = situacaoProducaoOrdem;
	}
}
