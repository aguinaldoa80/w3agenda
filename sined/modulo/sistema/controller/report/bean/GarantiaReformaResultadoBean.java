package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

public class GarantiaReformaResultadoBean {

	private String nome;
	private String mensagemResultado;
	private String mensagemDestino;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMensagemResultado() {
		return mensagemResultado;
	}

	public void setMensagemResultado(String mensagemResultado) {
		this.mensagemResultado = mensagemResultado;
	}

	public String getMensagemDestino() {
		return mensagemDestino;
	}

	public void setMensagemDestino(String mensagemDestino) {
		this.mensagemDestino = mensagemDestino;
	}
}
