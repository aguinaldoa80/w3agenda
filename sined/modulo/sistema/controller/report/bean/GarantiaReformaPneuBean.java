package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import java.util.LinkedList;

public class GarantiaReformaPneuBean {

	private Integer id;
	private String marca;
	private String modelo;
	private String medida;
	private String qualificacao;
	private String serieFogo;
	private String dot;
	private String numeroreforma;
	private String banda;
	private String profundidadeSulco;
	private String descricaoServicoGarantido;
	private String residuoBanda;
	private LinkedList<String> listaMotivoDevolucao;
	private String garantiaTipo;
	private String garantiaTipoPercentual;
	private String roda;
	private String lonas;
	private String descricao;
	private String otrPneuDesenho;
	private String otrPneuTipoServico;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMedida() {
		return medida;
	}

	public void setMedida(String medida) {
		this.medida = medida;
	}

	public String getQualificacao() {
		return qualificacao;
	}
	
	public void setQualificacao(String qualificacao) {
		this.qualificacao = qualificacao;
	}

	public String getSerieFogo() {
		return serieFogo;
	}

	public void setSerieFogo(String serieFogo) {
		this.serieFogo = serieFogo;
	}

	public String getDot() {
		return dot;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}

	public String getNumeroreforma() {
		return numeroreforma;
	}

	public void setNumeroreforma(String numeroreforma) {
		this.numeroreforma = numeroreforma;
	}

	public String getBanda() {
		return banda;
	}

	public void setBanda(String banda) {
		this.banda = banda;
	}

	public String getProfundidadeSulco() {
		return profundidadeSulco;
	}

	public void setProfundidadeSulco(String profundidadeSulco) {
		this.profundidadeSulco = profundidadeSulco;
	}

	public String getDescricaoServicoGarantido() {
		return descricaoServicoGarantido;
	}

	public void setDescricaoServicoGarantido(String descricaoServicoGarantido) {
		this.descricaoServicoGarantido = descricaoServicoGarantido;
	}

	public String getResiduoBanda() {
		return residuoBanda;
	}

	public void setResiduoBanda(String residuoBanda) {
		this.residuoBanda = residuoBanda;
	}

	public LinkedList<String> getListaMotivoDevolucao() {
		return listaMotivoDevolucao;
	}
	
	public void setListaMotivoDevolucao(LinkedList<String> listaMotivoDevolucao) {
		this.listaMotivoDevolucao = listaMotivoDevolucao;
	}

	public String getGarantiaTipo() {
		return garantiaTipo;
	}

	public void setGarantiaTipo(String garantiaTipo) {
		this.garantiaTipo = garantiaTipo;
	}

	public String getGarantiaTipoPercentual() {
		return garantiaTipoPercentual;
	}

	public void setGarantiaTipoPercentual(String garantiaTipoPercentual) {
		this.garantiaTipoPercentual = garantiaTipoPercentual;
	}

	public String getRoda() {
		return roda;
	}

	public String getLonas() {
		return lonas;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getOtrPneuDesenho() {
		return otrPneuDesenho;
	}

	public String getOtrPneuTipoServico() {
		return otrPneuTipoServico;
	}

	public void setRoda(String roda) {
		this.roda = roda;
	}

	public void setLonas(String lonas) {
		this.lonas = lonas;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setOtrPneuDesenho(String otrPneuDesenho) {
		this.otrPneuDesenho = otrPneuDesenho;
	}

	public void setOtrPneuTipoServico(String otrPneuTipoServico) {
		this.otrPneuTipoServico = otrPneuTipoServico;
	}
	
}
