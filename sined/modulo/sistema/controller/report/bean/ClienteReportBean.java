package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

public class ClienteReportBean {
	private Integer codigoCliente;
	private String identificadorCliente;
	private String nomeCliente;
	private String razaoSocialCliente;
	private String cpfCnpjCliente;
	private String inscricaoEstadualCliente;
	private String ufInscricaoEstadulaCliente;
	private String emailCliente;
	private String telefoneCliente;
	private String logradouroEnderecoCliente;
	private String numeroEnderecoCliente;
	private String bairroEnderecoCliente;
	private String cidadeEnderecoCliente;
	private String ufEnderecoCliente;
	private String cepEnderecoCliente;
	private String complementoEnderecoCliente;
	public Integer getCodigoCliente() {
		return codigoCliente;
	}
	public String getIdentificadorCliente() {
		return identificadorCliente;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public String getRazaoSocialCliente() {
		return razaoSocialCliente;
	}
	public String getCpfCnpjCliente() {
		return cpfCnpjCliente;
	}
	public String getInscricaoEstadualCliente() {
		return inscricaoEstadualCliente;
	}
	public String getUfInscricaoEstadulaCliente() {
		return ufInscricaoEstadulaCliente;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public String getTelefoneCliente() {
		return telefoneCliente;
	}
	public String getLogradouroEnderecoCliente() {
		return logradouroEnderecoCliente;
	}
	public String getNumeroEnderecoCliente() {
		return numeroEnderecoCliente;
	}
	public String getBairroEnderecoCliente() {
		return bairroEnderecoCliente;
	}
	public String getCidadeEnderecoCliente() {
		return cidadeEnderecoCliente;
	}
	public String getUfEnderecoCliente() {
		return ufEnderecoCliente;
	}
	public String getCepEnderecoCliente() {
		return cepEnderecoCliente;
	}
	public String getComplementoEnderecoCliente() {
		return complementoEnderecoCliente;
	}
	public void setCodigoCliente(Integer codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public void setIdentificadorCliente(String identificadorCliente) {
		this.identificadorCliente = identificadorCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public void setRazaoSocialCliente(String razaoSocialCliente) {
		this.razaoSocialCliente = razaoSocialCliente;
	}
	public void setCpfCnpjCliente(String cpfCnpjCliente) {
		this.cpfCnpjCliente = cpfCnpjCliente;
	}
	public void setInscricaoEstadualCliente(String inscricaoEstadualCliente) {
		this.inscricaoEstadualCliente = inscricaoEstadualCliente;
	}
	public void setUfInscricaoEstadulaCliente(String ufInscricaoEstadulaCliente) {
		this.ufInscricaoEstadulaCliente = ufInscricaoEstadulaCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public void setTelefoneCliente(String telefoneCliente) {
		this.telefoneCliente = telefoneCliente;
	}
	public void setLogradouroEnderecoCliente(String logradouroEnderecoCliente) {
		this.logradouroEnderecoCliente = logradouroEnderecoCliente;
	}
	public void setNumeroEnderecoCliente(String numeroEnderecoCliente) {
		this.numeroEnderecoCliente = numeroEnderecoCliente;
	}
	public void setBairroEnderecoCliente(String bairroEnderecoCliente) {
		this.bairroEnderecoCliente = bairroEnderecoCliente;
	}
	public void setCidadeEnderecoCliente(String cidadeEnderecoCliente) {
		this.cidadeEnderecoCliente = cidadeEnderecoCliente;
	}
	public void setUfEnderecoCliente(String ufEnderecoCliente) {
		this.ufEnderecoCliente = ufEnderecoCliente;
	}
	public void setCepEnderecoCliente(String cepEnderecoCliente) {
		this.cepEnderecoCliente = cepEnderecoCliente;
	}
	public void setComplementoEnderecoCliente(String complementoEnderecoCliente) {
		this.complementoEnderecoCliente = complementoEnderecoCliente;
	}

	
	
}
