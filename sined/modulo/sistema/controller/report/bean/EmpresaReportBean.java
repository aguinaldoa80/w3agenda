package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import br.com.linkcom.sined.geral.bean.Arquivo;

public class EmpresaReportBean {
	private String razaosocial;
	private String nomefantasia;
	private String endereco;
	private String telefone;
	private Arquivo logo;
	public String getRazaosocial() {
		return razaosocial;
	}
	public String getNomefantasia() {
		return nomefantasia;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public Arquivo getLogo() {
		return logo;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setLogo(Arquivo logo) {
		this.logo = logo;
	}

	
}
