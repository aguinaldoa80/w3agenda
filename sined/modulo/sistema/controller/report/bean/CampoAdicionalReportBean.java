package br.com.linkcom.sined.modulo.sistema.controller.report.bean;


public class CampoAdicionalReportBean {

	private Integer ordem;
	private String campoAdicional;
	
	public String getCampoAdicional() {
		return campoAdicional;
	}
	
	public Integer getOrdem() {
		return ordem;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	public void setCampoAdicional(String campoAdicional) {
		this.campoAdicional = campoAdicional;
	}
}
