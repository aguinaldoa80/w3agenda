package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

public class ProducaoPneuReportBean {

	private String motivoRecusa;
	private String observacaoRecusa;
	private String constatacao;
	private String observacaoConstatacao;
	public String getMotivoRecusa() {
		return motivoRecusa;
	}
	public String getObservacaoRecusa() {
		return observacaoRecusa;
	}
	public String getConstatacao() {
		return constatacao;
	}
	public String getObservacaoConstatacao() {
		return observacaoConstatacao;
	}
	public void setMotivoRecusa(String motivoRecusa) {
		this.motivoRecusa = motivoRecusa;
	}
	public void setObservacaoRecusa(String observacaoRecusa) {
		this.observacaoRecusa = observacaoRecusa;
	}
	public void setConstatacao(String constatacao) {
		this.constatacao = constatacao;
	}
	public void setObservacaoConstatacao(String observacaoConstatacao) {
		this.observacaoConstatacao = observacaoConstatacao;
	}
	
	
	
}
