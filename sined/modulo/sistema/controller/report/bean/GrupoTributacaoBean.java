package br.com.linkcom.sined.modulo.sistema.controller.report.bean;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Grupotributacao;

public class GrupoTributacaoBean {

	protected String informacoesadicionaiscontribuinte;

	public GrupoTributacaoBean(){
		super();
	}
	
	public GrupoTributacaoBean(Grupotributacao grupoTributacao){
		if(grupoTributacao != null){
			this.setInformacoesadicionaiscontribuinte(grupoTributacao.getInfoadicionalcontrib());
		}
	}

	public String getInformacoesadicionaiscontribuinte() {
		return Util.strings.emptyIfNull(informacoesadicionaiscontribuinte);
	}

	public void setInformacoesadicionaiscontribuinte(
			String informacoesadicionaiscontribuinte) {
		this.informacoesadicionaiscontribuinte = informacoesadicionaiscontribuinte;
	}
}
