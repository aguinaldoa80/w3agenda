package br.com.linkcom.sined.modulo.sistema.controller.report;

import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.modulo.sistema.controller.process.GrupoTributacaoSPEDFiscal;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ConferenciaGrupoTributacaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/sistema/relatorio/ConferencialGrupoTributacao", authorizationModule=ReportAuthorizationModule.class)
public class ConferenciaGrupoTributacaoReport extends SinedReport<ConferenciaGrupoTributacaoFiltro> {
	
	private NaturezaoperacaoService naturezaOperacaoService;
	private CfopService cfopService;
	
	public void setNaturezaOperacaoService(NaturezaoperacaoService naturezaOperacaoService) {
		this.naturezaOperacaoService = naturezaOperacaoService;
	}
	
	public void setCfopService(CfopService cfopService) {
		this.cfopService = cfopService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	ConferenciaGrupoTributacaoFiltro filtro) throws Exception {
		request.setAttribute("titulo", "CONFERÊNCIA DE DADOS - GRUPO DE TRIBUTAÇÃO");
		
		Report report = new Report("/sistema/conferenciaGrupoTributacao");
		List<GrupoTributacaoSPEDFiscal> lista = filtro.getListaGrupoTributacao();
		
		for (GrupoTributacaoSPEDFiscal g : lista) {
			if (g.getNaturezaoperacao() != null) {
				g.setNaturezaoperacao(naturezaOperacaoService.load(g.getNaturezaoperacao(), "naturezaoperacao.nome"));
			}
			
			if (g.getCfop() != null) {
				g.setCfop(cfopService.load(g.getCfop(), "cfop.codigo"));
			}
		}
		
		report.setDataSource(lista);
		
		return report;
	}
	
	@Override
	public String getNomeArquivo() {
		return "conferenciaGrupoTributacao";
	}


	@Override
	public String getTitulo(ConferenciaGrupoTributacaoFiltro filtro) {
		return "Relatório de Conferência de Grupo de Tributação";
	}
}
