package br.com.linkcom.sined.modulo.sistema.controller.report;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.GarantiareformaService;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaBean;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/sistema/crud/GarantiaReformaTemplate", authorizationModule=ReportAuthorizationModule.class)
public class GarantiaReformaTemplateReport extends ReportTemplateController<ReportTemplateFiltro> {

	private GarantiareformaService garantiaReformaService;
	
	public void setGarantiaReformaService(GarantiareformaService garantiaReformaService) {
		this.garantiaReformaService = garantiaReformaService;
	}
	
	@Override
	protected EnumCategoriaReportTemplate setCategoriaReportTemplate() {
		return EnumCategoriaReportTemplate.GARANTIA_DE_REFORMA;
	}

	@Override
	protected String getFileName() {
		return "garantiaReforma";
	}
	
	@Override
	protected ReportTemplateBean setTemplate(WebRequestContext request,	ReportTemplateFiltro filtro) {
		if(filtro != null && filtro.getTemplate() != null && filtro.getTemplate().getCdreporttemplate() != null){
			return getReportTemplateService().load(filtro.getTemplate());
		}
		
		return getReportTemplateService().loadTemplateByCategoria(EnumCategoriaReportTemplate.GARANTIA_DE_REFORMA);
	}
	
	@Override
	protected LinkedHashMap<String, Object> carregaDados(WebRequestContext request, ReportTemplateFiltro filtro) throws Exception {	
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		
		Empresa empresa = null;
		Object empresaFiltro = request.getAttribute("empresaFiltro");
		if(empresaFiltro != null){
			empresa = empresaService.loadComArquivo((Empresa)empresaFiltro);
		} else {
			empresa = empresaService.loadArquivoPrincipal();
		}
		map.put("logo", SinedUtil.getLogoURLForReport(empresa));
		map.put("empresa", empresa);
		
		LinkedList<GarantiaReformaBean> garantiaReformaBeanList = new LinkedList<GarantiaReformaBean>(garantiaReformaService.getGarantiaReformaBeanForTemplate(request.getParameter("selectedItens")));
		
		map.put("lista", garantiaReformaBeanList);
		
		return map;
	}
}
