package br.com.linkcom.sined.modulo.sistema.controller.report;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.sined.geral.service.ContacontabilService;
import br.com.linkcom.sined.modulo.contabil.controller.crud.filter.ContacontabilFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/sistema/relatorio/Contacontabil", 
			authorizationModule=ReportAuthorizationModule.class)
public class ContacontabilReport extends SinedReport<ContacontabilFiltro>{

	protected ContacontabilService contacontabilService;
	
	public void setContacontabilService(ContacontabilService contacontabilService) {
		this.contacontabilService = contacontabilService;
	}

	@Override
	public IReport createReportSined(WebRequestContext request, ContacontabilFiltro filtro) throws Exception {
		return contacontabilService.createRelatorioContaContabil(filtro);
	}

	@Override
	public String getNomeArquivo() {
		return "contacontabil";
	}

	@Override
	public String getTitulo(ContacontabilFiltro filtro) {
		return "RELATÓRIO DE CONTA CONTÁBIL";
	}

}
