package br.com.linkcom.sined.modulo.sistema.controller.report;

import java.util.List;

import br.com.linkcom.neo.authorization.report.ReportAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.modulo.sistema.controller.process.NatOprCfopSPEDFiscal;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ConferenciaNaturezaOperacaoFiltro;
import br.com.linkcom.sined.util.controller.SinedReport;

@Controller(path = "/sistema/relatorio/ConferencialNaturezaOperacao", authorizationModule=ReportAuthorizationModule.class)
public class ConferenciaNaturezaOperacaoReport extends SinedReport<ConferenciaNaturezaOperacaoFiltro> {

	private NaturezaoperacaoService naturezaOperacaoService;
	private CfopService cfopService;
	
	public void setNaturezaOperacaoService(NaturezaoperacaoService naturezaOperacaoService) {
		this.naturezaOperacaoService = naturezaOperacaoService;
	}
	
	public void setCfopService(CfopService cfopService) {
		this.cfopService = cfopService;
	}
	
	@Override
	public IReport createReportSined(WebRequestContext request,	ConferenciaNaturezaOperacaoFiltro filtro) throws Exception {
		request.setAttribute("titulo", "CONFER�NCIA DE DADOS - NATUREZA DE OPERA��O / CFOP");
		
		Report report = new Report("/sistema/conferenciaNaturezaOperacao");
		List<NatOprCfopSPEDFiscal> lista = filtro.getListaNatOprCfopSPEDFiscal();
		
		for (NatOprCfopSPEDFiscal n : lista) {
			if (n.getNaturezaOperacao() != null) {
				n.setNaturezaOperacao(naturezaOperacaoService.load(n.getNaturezaOperacao(), "naturezaoperacao.nome"));
			}
			
			if (n.getCfop() != null) {
				n.setCfop(cfopService.load(n.getCfop(), "cfop.codigo, cfop.descricaoresumida"));
			}
		}
		
		report.setDataSource(lista);
		
		return report;
	}
	
	@Override
	public String getNomeArquivo() {
		return "conferneciaNaturezaOperacao";
	}


	@Override
	public String getTitulo(ConferenciaNaturezaOperacaoFiltro filtro) {
		return "Relat�rio de Confer�ncia de Natureza de Opera��o e CFOP";
	}
}
