package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;

public class NatOprCfopSPEDFiscal {

	private String codigo;
	private String cfopCodigo;
	private String naturezaOperacaoCodigo;
	private String naturezaOperacaoDescricao;
	private int quantidadeRegistros;
	private Naturezaoperacao naturezaOperacao;
	private Cfop cfop;
	private boolean selecionado;
	
	private String cfopLabel;
	private String naturezaOperacaoLabel;

	public NatOprCfopSPEDFiscal() {
	}
	
	public NatOprCfopSPEDFiscal(int quantidadeRegistros) {
		this.quantidadeRegistros = quantidadeRegistros;
	}
	
	public NatOprCfopSPEDFiscal(String cfopCodigo, String naturezaOperacaoCodigo, int quantidadeRegistros) {
		this.quantidadeRegistros = quantidadeRegistros;
		this.cfopCodigo = cfopCodigo;
		this.naturezaOperacaoCodigo = naturezaOperacaoCodigo;
	}
	
	public NatOprCfopSPEDFiscal(String codigo, String cfopCodigo, String naturezaOperacaoCodigo, int quantidadeRegistros, Naturezaoperacao naturezaOperacao, Cfop cfop) {
		this.codigo = codigo;
		this.cfopCodigo = cfopCodigo;
		this.naturezaOperacaoCodigo = naturezaOperacaoCodigo;
		this.quantidadeRegistros = quantidadeRegistros;
		this.naturezaOperacao = naturezaOperacao;
		this.cfop = cfop;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@DisplayName("Qnt. Reg.")
	public int getQuantidadeRegistros() {
		return quantidadeRegistros;
	}

	public void setQuantidadeRegistros(int quantidadeRegistros) {
		this.quantidadeRegistros = quantidadeRegistros;
	}

	public boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}
	
	public String getNaturezaOperacaoCodigo() {
		return naturezaOperacaoCodigo;
	}
	
	public void setNaturezaOperacaoCodigo(String naturezaOperacaoCodigo) {
		this.naturezaOperacaoCodigo = naturezaOperacaoCodigo;
	}
	
	@DisplayName("Natureza de Opera��o")
	public String getNaturezaOperacaoDescricao() {
		return naturezaOperacaoDescricao;
	}
	
	public void setNaturezaOperacaoDescricao(String naturezaOperacaoDescricao) {
		this.naturezaOperacaoDescricao = naturezaOperacaoDescricao;
	}
	
	@DisplayName("Natureza de Opera��o - Associada")
	public Naturezaoperacao getNaturezaOperacao() {
		return naturezaOperacao;
	}

	public void setNaturezaOperacao(Naturezaoperacao naturezaOperacao) {
		this.naturezaOperacao = naturezaOperacao;
	}
	
	@DisplayName("CFOP")
	public String getCfopCodigo() {
		return cfopCodigo;
	}
	
	public void setCfopCodigo(String cfopCodigo) {
		this.cfopCodigo = cfopCodigo;
	}
	
	@DisplayName("CFOP - Associada")
	public Cfop getCfop() {
		return cfop;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public String getCfopLabel() {
		if (cfop != null && StringUtils.isNotEmpty(cfop.getCodigo()) && StringUtils.isNotEmpty(cfop.getDescricaoresumida())) {
			return cfop.getCodigo() + " - " + cfop.getDescricaoresumida();
		} else {
			return "";
		}
	}
	
	public void setCfopLabel(String cfopLabel) {
		this.cfopLabel = cfopLabel;
	}
	
	public String getNaturezaOperacaoLabel() {
		if (naturezaOperacao != null && StringUtils.isNotEmpty(naturezaOperacao.getNome())) {
			return naturezaOperacao.getNome();
		} else {
			return "";
		}
	}
	
	public void setNaturezaOperacaoLabel(String naturezaOperacaoLabel) {
		this.naturezaOperacaoLabel = naturezaOperacaoLabel;
	}
}
