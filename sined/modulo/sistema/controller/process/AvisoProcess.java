package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.service.AvisousuarioService;
import br.com.linkcom.sined.geral.service.SessaoService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/sistema/process/AvisoProcess", authorizationModule=ProcessAuthorizationModule.class)
public class AvisoProcess extends MultiActionController {

	private AvisousuarioService avisoUsuarioService;
	private SessaoService sessaoService;
	
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	
	public void setSessaoService(SessaoService sessaoService) {
		this.sessaoService = sessaoService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request) {
		request.setAttribute("popupAviso", Boolean.TRUE);
		sessaoService.updatePopupAviso();
		return new ModelAndView("direct:/process/popup/popupAviso");
	}
	
	public ModelAndView getAvisosPorMotivoAvisoList(WebRequestContext request){
		if (Boolean.parseBoolean(request.getParameter("popup"))) {
			return new JsonModelAndView().addObject("notificacaoList", avisoUsuarioService.findAllNaoLidasPopup());
		} else {
			return new JsonModelAndView().addObject("notificacaoList", avisoUsuarioService.findAllNaoLidasNotificacao());
		}
	}
	
	public ModelAndView marcarAvisoComoLidoAjax(WebRequestContext request) {
		String selectedItens = request.getParameter("selectedItens");
		List<Motivoaviso> motivoavisoOkList = new ArrayList<Motivoaviso>();
		Motivoaviso motivoaviso = null;
		
		if (selectedItens != null && !selectedItens.equals("")) {
			for (String selectedItem : selectedItens.split(",")) {
				try {
					avisoUsuarioService.atualizarAvisoComoLido(selectedItem, SinedUtil.getUsuarioLogado().getCdpessoa().toString());
					
					motivoaviso = new Motivoaviso();
					motivoaviso.setCdmotivoaviso(Integer.parseInt(selectedItem));
					motivoavisoOkList.add(motivoaviso);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return new JsonModelAndView().addObject("motivoavisoOkList", motivoavisoOkList);
	}
	
	public ModelAndView verificaNecessidadePopupAviso(WebRequestContext request) {
		if (SinedUtil.getUsuarioLogado().getLogin().equals("admin")) {
			return new JsonModelAndView().addObject("admin", Boolean.TRUE);
		}
		
		return new JsonModelAndView().addObject("popup", sessaoService.getUsuarioJaAbriuPopupAviso()).addObject("temAviso", avisoUsuarioService.findAllNaoLidasPopup().size() > 0);
	}
}
