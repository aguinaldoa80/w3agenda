package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.empresaapi.EmpresaAPI;
import br.com.linkcom.empresaapi.bean.EmpresaBean;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;

@Controller(path="/sistema/process/ReceitaFederal", authorizationModule = ProcessAuthorizationModule.class)
public class ReceitaFederalProcess extends MultiActionController {
	
	public static final String CHAVE_EMPRESAAPI = "XN265pnTIA1OPCXcvY0EY1QRGe3m0eLB8ejdES7H";
	
	@DefaultAction
	public ModelAndView index(WebRequestContext context){
		return null;
	}
	
	public ModelAndView consultaCNPJEmpresaAPI(WebRequestContext request, EmpresaBean command){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		String cnpj = command.getCnpj();
		if(StringUtils.isNotBlank(cnpj)){
			EmpresaBean empresaBean = EmpresaAPI.getInstance(CHAVE_EMPRESAAPI).getEmpresaByCNPJ(cnpj);
			if(empresaBean != null){
				jsonModelAndView.addObject("empresa", empresaBean);
			}
		}
		
		return jsonModelAndView;
	}
	
}