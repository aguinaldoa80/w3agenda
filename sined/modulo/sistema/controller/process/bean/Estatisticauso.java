package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

public class Estatisticauso {
	
	// FINANCEIRO
	private Integer agendamento_aconsolidar;
	private Integer agendamento_atrasadas;
	private Integer contapagar_abertas;
	private Integer contapagar_atrasadas;
	private Integer contareceber_abertas;
	private Integer contareceber_atrasadas;
	private Integer movimentacao_naoconciliadas;
	
	// FATURAMENTO
	private Integer contrato_afaturar;
	private Integer contrato_atrasados;
	private Integer venda_pedidospendentes;
	private Integer venda_total;
	
	// SUPRIMENTOS
	private Integer ordemcompra_aguardandoautorizacao;
	private Integer ordemcompra_autorizadas;
	private Integer ordemcompra_aguardandorecebimento;
	
	// CRM
	private Integer oportunidade_retornoatrasado;
	private Integer oportunidade_retornohoje;
	
	// SERVI�OS
	private Integer requisicao_abertas;
	private Integer requisicao_atrasadas;
	
	public Integer getAgendamento_aconsolidar() {
		return agendamento_aconsolidar;
	}
	public Integer getAgendamento_atrasadas() {
		return agendamento_atrasadas;
	}
	public Integer getContapagar_abertas() {
		return contapagar_abertas;
	}
	public Integer getContapagar_atrasadas() {
		return contapagar_atrasadas;
	}
	public Integer getContareceber_abertas() {
		return contareceber_abertas;
	}
	public Integer getContareceber_atrasadas() {
		return contareceber_atrasadas;
	}
	public Integer getMovimentacao_naoconciliadas() {
		return movimentacao_naoconciliadas;
	}
	public Integer getContrato_afaturar() {
		return contrato_afaturar;
	}
	public Integer getContrato_atrasados() {
		return contrato_atrasados;
	}
	public Integer getOrdemcompra_aguardandoautorizacao() {
		return ordemcompra_aguardandoautorizacao;
	}
	public Integer getOrdemcompra_autorizadas() {
		return ordemcompra_autorizadas;
	}
	public Integer getOrdemcompra_aguardandorecebimento() {
		return ordemcompra_aguardandorecebimento;
	}
	public Integer getOportunidade_retornoatrasado() {
		return oportunidade_retornoatrasado;
	}
	public Integer getOportunidade_retornohoje() {
		return oportunidade_retornohoje;
	}
	public Integer getRequisicao_abertas() {
		return requisicao_abertas;
	}
	public Integer getRequisicao_atrasadas() {
		return requisicao_atrasadas;
	}
	public Integer getVenda_pedidospendentes() {
		return venda_pedidospendentes;
	}
	public Integer getVenda_total() {
		return venda_total;
	}
	public void setVenda_pedidospendentes(Integer vendaPedidospendentes) {
		venda_pedidospendentes = vendaPedidospendentes;
	}
	public void setVenda_total(Integer vendaTotal) {
		venda_total = vendaTotal;
	}
	public void setAgendamento_aconsolidar(Integer agendamentoAconsolidar) {
		agendamento_aconsolidar = agendamentoAconsolidar;
	}
	public void setAgendamento_atrasadas(Integer agendamentoAtrasadas) {
		agendamento_atrasadas = agendamentoAtrasadas;
	}
	public void setContapagar_abertas(Integer contapagarAbertas) {
		contapagar_abertas = contapagarAbertas;
	}
	public void setContapagar_atrasadas(Integer contapagarAtrasadas) {
		contapagar_atrasadas = contapagarAtrasadas;
	}
	public void setContareceber_abertas(Integer contareceberAbertas) {
		contareceber_abertas = contareceberAbertas;
	}
	public void setContareceber_atrasadas(Integer contareceberAtrasadas) {
		contareceber_atrasadas = contareceberAtrasadas;
	}
	public void setMovimentacao_naoconciliadas(Integer movimentacaoNaoconciliadas) {
		movimentacao_naoconciliadas = movimentacaoNaoconciliadas;
	}
	public void setContrato_afaturar(Integer contratoAfaturar) {
		contrato_afaturar = contratoAfaturar;
	}
	public void setContrato_atrasados(Integer contratoAtrasados) {
		contrato_atrasados = contratoAtrasados;
	}
	public void setOrdemcompra_aguardandoautorizacao(
			Integer ordemcompraAguardandoautorizacao) {
		ordemcompra_aguardandoautorizacao = ordemcompraAguardandoautorizacao;
	}
	public void setOrdemcompra_autorizadas(Integer ordemcompraAutorizadas) {
		ordemcompra_autorizadas = ordemcompraAutorizadas;
	}
	public void setOrdemcompra_aguardandorecebimento(
			Integer ordemcompraAguardandorecebimento) {
		ordemcompra_aguardandorecebimento = ordemcompraAguardandorecebimento;
	}
	public void setOportunidade_retornoatrasado(Integer oportunidadeRetornoatrasado) {
		oportunidade_retornoatrasado = oportunidadeRetornoatrasado;
	}
	public void setOportunidade_retornohoje(Integer oportunidadeRetornohoje) {
		oportunidade_retornohoje = oportunidadeRetornohoje;
	}
	public void setRequisicao_abertas(Integer requisicaoAbertas) {
		requisicao_abertas = requisicaoAbertas;
	}
	public void setRequisicao_atrasadas(Integer requisicaoAtrasadas) {
		requisicao_atrasadas = requisicaoAtrasadas;
	}
	
}
