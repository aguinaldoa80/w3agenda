package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import br.com.linkcom.sined.util.SinedUtil;

public class AtualizacaoEstoqueTxtColetorBean {

	private String codigobarras;
	private Double quantidade = 0d;
	private String lote;
	private String cnpj;
	
	
	public String getCodigobarras() {
		return codigobarras;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	
	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
		
	public String getLote() {
		return lote;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public String getKey(){
		if(this.lote != null && !this.lote.trim().isEmpty() && this.cnpj != null && !this.cnpj.trim().isEmpty()){
			return this.lote + "|" + this.cnpj;
		}
		return null;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigobarras == null) ? 0 : codigobarras.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtualizacaoEstoqueTxtColetorBean other = (AtualizacaoEstoqueTxtColetorBean) obj;
		if (codigobarras == null) {
			if (other.codigobarras != null)
				return false;
		} else if (!SinedUtil.retiraZeroEsquerda(codigobarras).equals(SinedUtil.retiraZeroEsquerda(other.codigobarras)))
			return false;
		else if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		return true;
	}
	
}
