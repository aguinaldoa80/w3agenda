package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;

public class AtualizacaoEstoqueBean {

	private Empresa empresa;
	private Localarmazenagem localarmazenagem;
	private Date dtestoque;
	private List<AtualizacaoEstoqueMaterialBean> listaMaterial;
	private Boolean gerarSomenteEntrada;
	private Boolean considerarLotes;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Date getDtestoque() {
		return dtestoque;
	}
	public List<AtualizacaoEstoqueMaterialBean> getListaMaterial() {
		return listaMaterial;
	}
	public Boolean getConsiderarLotes() {
		return considerarLotes;
	}
	public void setConsiderarLotes(Boolean considerarLotes) {
		this.considerarLotes = considerarLotes;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setDtestoque(Date dtestoque) {
		this.dtestoque = dtestoque;
	}
	public void setListaMaterial(List<AtualizacaoEstoqueMaterialBean> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	public Boolean getGerarSomenteEntrada() {
		return gerarSomenteEntrada;
	}
	public void setGerarSomenteEntrada(Boolean gerarSomenteEntrada) {
		this.gerarSomenteEntrada = gerarSomenteEntrada;
	}
}
