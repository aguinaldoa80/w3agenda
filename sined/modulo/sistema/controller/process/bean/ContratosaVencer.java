package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Cliente;

public class ContratosaVencer {

	private Integer cdcontrato;
	private String identificador;
	private String descricao;
	private Date datavencimento;
	private Cliente cliente;
	
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	public String getIdentificador() {
		return identificador;
	}
	public Date getDatavencimento() {
		return datavencimento;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDatavencimento(Date datavencimento) {
		this.datavencimento = datavencimento;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
