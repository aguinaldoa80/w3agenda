package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.modulo.sistema.controller.process.bean.enumeration.ReceitaFederalCampoEnum;
import br.com.linkcom.sined.modulo.sistema.controller.util.ReceitaFederalUtil;


public class ReceitaFederalCnpjHtmlBean {
	
	private String html;
	
	public ReceitaFederalCnpjHtmlBean(String html){
		this.html = html.replaceAll("\n","");
	}
	
	public String extrair(ReceitaFederalCampoEnum receitaCampoEnum) {		
		return extrair(receitaCampoEnum.getInicio(), receitaCampoEnum.getFim());
	}

	public String extrairCnaePrincipal() {
		
		String cnaePrincipal = extrair("C�DIGO E DESCRI��O DA ATIVIDADE ECON�MICA PRINCIPAL		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>");
		
		if (cnaePrincipal==null){
			return null;
		}
		
		if (cnaePrincipal.matches("\\d{2}\\.\\d{2}-\\d{1}-\\d{2}.+")){
			String retorno = cnaePrincipal.substring(0, 10).trim();
			if (ReceitaFederalUtil.isCnaePattern(retorno)){
				return retorno;
			}else {
				return null;
			}
		}		
		return null;
	}
	
	public List<String> extrairCnaesSecundarios() {		
		
		List<String> listaCnaeSecundario = new ArrayList<String>();
		String stringCnaesSecundarios = extrair("C�DIGO E DESCRI��O DAS ATIVIDADES ECON�MICAS SECUND�RIAS		</font>		<br>							<font face=\"Arial\" style=\"font-size: 8pt\">			", "</table>");
		
		if (stringCnaesSecundarios==null){
			return listaCnaeSecundario;
		}
		
		int indexBAbre = stringCnaesSecundarios.indexOf("<b>");
		int indexBFecha = stringCnaesSecundarios.indexOf("</b>");
		String cnaeSecundario;
		String retorno;
		
		while (indexBAbre>-1 && indexBFecha>-1){
			cnaeSecundario = stringCnaesSecundarios.substring(indexBAbre+3, indexBFecha).trim();
			retorno = cnaeSecundario.substring(0, 10);
			if (ReceitaFederalUtil.isCnaePattern(retorno)){
				listaCnaeSecundario.add(retorno);
			}
			indexBAbre = stringCnaesSecundarios.indexOf("<b>", indexBFecha);
			indexBFecha = stringCnaesSecundarios.indexOf("</b>", indexBFecha+1);
		}
		
		return listaCnaeSecundario;
	}	
	
	private String extrair(String inicio, String fim) {
		
		int i, f, aux;
		
		String htmlAux = this.html.replaceAll("\t", "");
		inicio = inicio.replaceAll("\t", "");
		fim = fim.replaceAll("\t", "");
		
		i = htmlAux.indexOf(inicio);
		if (i==-1){
			return null;
		}
			
		aux = i + inicio.length(); // expressa a posi��o do primeiro char do string a ser copiado
		f = htmlAux.indexOf(fim, aux);
		
		
		String s = htmlAux.substring(aux, f).trim();
		s = s.replace("&", "%26");
		
		if (s.startsWith("*")){
			return null;
		}
		
		return s;
	}
	
	public boolean isImagemDigitaErrada(){
		boolean imagemDigitaErrada1 = html.contains("<b>Erro na Consulta</b>");
		return imagemDigitaErrada1;
	}
	
	public boolean isCnpjNaoEncontrado(){
		
		boolean cnpjNaoEncontrado1 = html.contains("Não existe no Cadastro de Pessoas Jurídicas o número de CNPJ informado. Verifique se o mesmo foi digitado corretamente.");
		boolean cnpjNaoEncontrado2 = html.contains("N�o existe no Cadastro de Pessoas Jur�dicas o n�mero de CNPJ informado. Verifique se o mesmo foi digitado corretamente.");
		
		return cnpjNaoEncontrado1 || cnpjNaoEncontrado2;		
	}
}