package br.com.linkcom.sined.modulo.sistema.controller.process.bean;


public class AtualizacaoEstoqueCsvPadraoBean {

	private Integer cdmaterial;
	private Double quantidade;
	private String lote;
	private String cnpj;
	
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	public String getLote() {
		return lote;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public String getKey(){
		if(this.lote != null && !this.lote.trim().isEmpty() && this.cnpj != null && !this.cnpj.trim().isEmpty()){
			return this.lote + "|" + this.cnpj;
		}
		return null;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmaterial == null) ? 0 : cdmaterial.hashCode());
		result = prime * result + ((lote == null) ? 0 : lote.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtualizacaoEstoqueCsvPadraoBean other = (AtualizacaoEstoqueCsvPadraoBean) obj;
		if (cdmaterial == null) {
			if (other.cdmaterial != null)
				return false;
		} else if (!cdmaterial.equals(other.cdmaterial))
			return false;
		if (lote == null) {
			if (other.lote != null)
				return false;
		} else if (!lote.equals(other.lote))
			return false;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		return true;
	}
	
}
