package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.Movimentacaoanimalmotivo;

public class RegistroMovimentacaoAnimalMaterialBean {

	protected Empresa empresa;
	protected Localarmazenagem localarmazenagem;
	protected Material material;
	protected Double quantidade;
	protected Double quantidadedisponivel;
	protected Double pesomedio;
	protected Movimentacaoanimalmotivo motivo;
	protected Boolean registrapesomedio;
	protected String localArmazenagemDescricao;
	protected String materialDescricao;
	protected String motivoDescricao;
	protected Centrocusto centrocusto;
	protected Projeto projetoRateio;
	protected Money valor;
	
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Local de armazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@Required
	@DisplayName("Qtde.")
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	@DisplayName("Qtde. dispon�vel")
	public Double getQuantidadedisponivel() {
		return quantidadedisponivel;
	}
	public void setQuantidadedisponivel(Double quantidadedisponivel) {
		this.quantidadedisponivel = quantidadedisponivel;
	}
	
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	
	@Required
	@DisplayName("Motivo")
	public Movimentacaoanimalmotivo getMotivo() {
		return motivo;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	public Projeto getProjetoRateio() {
		return projetoRateio;
	}
	@DisplayName("")
	public Money getValor() {
		return valor;
	}
	
	public void setMotivo(Movimentacaoanimalmotivo motivo) {
		this.motivo = motivo;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjetoRateio(Projeto projetoRateio) {
		this.projetoRateio = projetoRateio;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public Boolean getRegistrapesomedio() {
		return registrapesomedio;
	}
	public void setRegistrapesomedio(Boolean registrapesomedio) {
		this.registrapesomedio = registrapesomedio;
	}
	
	public String getLocalArmazenagemDescricao() {
		return getLocalarmazenagem() != null ? getLocalarmazenagem().getNome() : "";
	}
	public void setLocalArmazenagemDescricao(String localArmazenagemDescricao) {
		this.localArmazenagemDescricao = localArmazenagemDescricao;
	}
	
	public String getMaterialDescricao() {
		return getMaterial() != null ? getMaterial().getNome() : "";
	}
	public void setMaterialDescricao(String materialDescricao) {
		this.materialDescricao = materialDescricao;
	}
	
	@DisplayName("Motivo")
	public String getMotivoDescricao() {
		return motivo.toString();
	}
	public void setMotivoDescricao(String motivoDescricao) {
		this.motivoDescricao = motivoDescricao;
	}
}
