package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.sined.util.SinedDateUtils;

public class EmporiumAcertoPainelBean {

	private Date dtinicio;
	private Date dtfim;
	
	public EmporiumAcertoPainelBean() {
		dtinicio = SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), -1);
		dtfim = SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), -1);
	}
	
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
}
