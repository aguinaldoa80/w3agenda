package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;

public class TransferenciaCategoriaBean {

	protected Empresa empresa;
	protected Localarmazenagem localarmazenagem;
	protected Date dataestoque;
	protected List<TransferenciaCategoriaMaterialBean> listaMateriais;
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	@DisplayName("Data do estoque")
	public Date getDataestoque() {
		return dataestoque;
	}
	public void setDataestoque(Date dataestoque) {
		this.dataestoque = dataestoque;
	}
	
	public List<TransferenciaCategoriaMaterialBean> getListaMateriais() {
		return listaMateriais;
	}
	public void setListaMateriais(
			List<TransferenciaCategoriaMaterialBean> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}
}
