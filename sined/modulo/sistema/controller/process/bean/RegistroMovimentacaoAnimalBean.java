package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.util.List;

public class RegistroMovimentacaoAnimalBean {

	protected List<RegistroMovimentacaoAnimalMaterialBean> listaMateriais;
	
	public List<RegistroMovimentacaoAnimalMaterialBean> getListaMateriais() {
		return listaMateriais;
	}
	public void setListaMateriais(
			List<RegistroMovimentacaoAnimalMaterialBean> listaMateriais) {
		this.listaMateriais = listaMateriais;
	}
}
