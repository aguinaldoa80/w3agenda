package br.com.linkcom.sined.modulo.sistema.controller.process.bean.enumeration;


public enum ReceitaFederalCampoEnum {	
	
	SITUACAO("SITUA��O CADASTRAL		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	DATA_ABERTURA("DATA DE ABERTURA		</font>				<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	RAZAO_SOCIAL("NOME EMPRESARIAL		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	NOME_FANTASIA("T�TULO DO ESTABELECIMENTO (NOME DE FANTASIA)		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	CNAE_PRINCIPAL(),
	CNAES_SECUNDARIOS(),
	LOGRADOURO("LOGRADOURO		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	NUMERO("N�MERO		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	COMPLEMENTO("COMPLEMENTO		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	CEP("CEP		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	BAIRRO("BAIRRO/DISTRITO		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	MUNICIPIO("MUNIC�PIO		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	UF("UF		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>"),
	NATUREZA("C�DIGO E DESCRI��O DA NATUREZA JUR�DICA		</font>		<br>		<font face=\"Arial\" style=\"font-size: 8pt\">		<b>", "</b>");
	
	private String inicio;
	private String fim;
	
	private ReceitaFederalCampoEnum(){				
	}
	
	private ReceitaFederalCampoEnum(String inicio, String fim){
		this.inicio = inicio;
		this.fim = fim;		
	}

	public String getInicio() {
		return inicio;
	}

	public String getFim() {
		return fim;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public void setFim(String fim) {
		this.fim = fim;
	}
}