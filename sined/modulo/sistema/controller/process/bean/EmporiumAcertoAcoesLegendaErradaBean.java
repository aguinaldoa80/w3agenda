package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.SinedDateUtils;

public class EmporiumAcertoAcoesLegendaErradaBean {

	private Empresa empresa;
	private Date data;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmporiumAcertoAcoesLegendaErradaBean other = (EmporiumAcertoAcoesLegendaErradaBean) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!SinedDateUtils.equalsIgnoreHour(data, other.data))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		return true;
	}
	
}
