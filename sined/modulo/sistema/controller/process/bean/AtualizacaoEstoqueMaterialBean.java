package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;

public class AtualizacaoEstoqueMaterialBean implements ValidateLoteestoqueObrigatorioInterface{

	private Material material;
	private Unidademedida unidademedida;
	private Double qtdeSistema;
	private Double qtdeAtual;
	private Boolean atualizado = Boolean.FALSE;
	private String lote;
	private Double pesomedio;
	private Boolean registrarpesomedio;
	private String cnpj;
	private Centrocusto centroCusto;

	private Loteestoque loteestoque;
	
	public AtualizacaoEstoqueMaterialBean(){}
	
	public AtualizacaoEstoqueMaterialBean(AtualizacaoEstoqueMaterialBean item, String lote, String cnpj){
		this.material = item.getMaterial();
		this.unidademedida = item.getUnidademedida();
		this.qtdeSistema = 0d;
		this.qtdeAtual = 0d;
		this.lote = lote;
		this.cnpj = cnpj;
	}
	
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Unidade de medida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	@DisplayName("Qtde. no Sistema")
	public Double getQtdeSistema() {
		return qtdeSistema;
	}
	@DisplayName("Qtde. Atual")
	public Double getQtdeAtual() {
		return qtdeAtual;
	}
	public Boolean getAtualizado() {
		return atualizado;
	}
	public void setAtualizado(Boolean atualizado) {
		this.atualizado = atualizado;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setQtdeSistema(Double qtdeSistema) {
		this.qtdeSistema = qtdeSistema;
	}
	public void setQtdeAtual(Double qtdeAtual) {
		this.qtdeAtual = qtdeAtual;
	}
	public String getLote() {
		return lote;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}

	public Loteestoque getLoteestoque() {
		return loteestoque;
	}

	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	
	public Boolean getRegistrarpesomedio() {
		return registrarpesomedio;
	}
	public void setRegistrarpesomedio(Boolean registrarpesomedio) {
		this.registrarpesomedio = registrarpesomedio;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}
}
