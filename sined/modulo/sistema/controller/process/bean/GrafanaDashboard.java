package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

public class GrafanaDashboard {

	private String uidDashboard;
	private String nomeDashboard;
	
	public String getUidDashboard() {
		return uidDashboard;
	}
	public String getNomeDashboard() {
		return nomeDashboard;
	}
	public void setUidDashboard(String uidDashboard) {
		this.uidDashboard = uidDashboard;
	}
	public void setNomeDashboard(String nomeDashboard) {
		this.nomeDashboard = nomeDashboard;
	}

}
