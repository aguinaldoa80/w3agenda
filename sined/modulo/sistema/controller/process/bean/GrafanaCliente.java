package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.util.List;

public class GrafanaCliente {

	private String nomeBanco;
	private List<GrafanaDashboard> listaDashboard;

	public String getNomeBanco() {
		return nomeBanco;
	}
	
	public List<GrafanaDashboard> getListaDashboard() {
		return listaDashboard;
	}
	
	public void setListaDashboard(List<GrafanaDashboard> listaDashboard) {
		this.listaDashboard = listaDashboard;
	}
	
	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}
	
}
