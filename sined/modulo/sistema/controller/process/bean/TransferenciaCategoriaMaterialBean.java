package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.enumeration.Movimentacaoanimalmotivo;

public class TransferenciaCategoriaMaterialBean {
	
	protected Materialgrupo materialgrupo;
	protected Material materialorigem;
	protected Double quantidadeatual;
	protected Material materialdestino;
	protected Double quantidadetransferir;
	protected Double pesomedio;
	protected Movimentacaoanimalmotivo motivo;
	protected Boolean registrarpesomedio;
	
	@DisplayName("Grupo de material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	public Material getMaterialorigem() {
		return materialorigem;
	}
	public void setMaterialorigem(Material materialorigem) {
		this.materialorigem = materialorigem;
	}
	
	@DisplayName("Qtde. atual")
	public Double getQuantidadeatual() {
		return quantidadeatual;
	}
	public void setQuantidadeatual(Double quantidadeatual) {
		this.quantidadeatual = quantidadeatual;
	}
	
	@DisplayName("Novo material")
	public Material getMaterialdestino() {
		return materialdestino;
	}
	public void setMaterialdestino(Material materialdestino) {
		this.materialdestino = materialdestino;
	}
	
	@DisplayName("Qtde.")
	public Double getQuantidadetransferir() {
		return quantidadetransferir;
	}
	public void setQuantidadetransferir(Double quantidadetransferir) {
		this.quantidadetransferir = quantidadetransferir;
	}
	
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	
	@DisplayName("Motivo")
	public Movimentacaoanimalmotivo getMotivo() {
		return motivo;
	}
	public void setMotivo(Movimentacaoanimalmotivo motivo) {
		this.motivo = motivo;
	}
	
	public Boolean getRegistrarpesomedio() {
		return registrarpesomedio;
	}
	public void setRegistrarpesomedio(Boolean registrarpesomedio) {
		this.registrarpesomedio = registrarpesomedio;
	}
}
