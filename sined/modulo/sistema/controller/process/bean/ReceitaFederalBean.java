package br.com.linkcom.sined.modulo.sistema.controller.process.bean;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.validation.annotation.MaxLength;

public class ReceitaFederalBean {
	
	//Popup
	private String letras;
	private String cnpj;
	private String url;
	private String auxiliar;
	private String url2CaptchaNovaImagem;
	
	//Dados	
	private String status;
	private Date dataAbertura;
	private String razaoSocial;
	private String nomeFantasia;
	private String cnaePrincipal;
	private List<String> listaCnaeSecundario;
	private String logradouro;
	private String numero;
	private String complemento;
	private String cep;
	private String bairro;
	private String municipio;
	private String uf;	
	
	@MaxLength(6)
	public String getLetras() {
		return letras;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getUrl() {
		return url;
	}
	public String getAuxiliar() {
		return auxiliar;
	}
	public String getStatus() {
		return status;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public String getCnaePrincipal() {
		return cnaePrincipal;
	}
	public List<String> getListaCnaeSecundario() {
		return listaCnaeSecundario;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getCep() {
		return cep;
	}
	public String getBairro() {
		return bairro;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getUf() {
		return uf;
	}
	
	public void setLetras(String letras) {
		this.letras = letras;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setCnaePrincipal(String cnaePrincipal) {
		this.cnaePrincipal = cnaePrincipal;
	}
	public void setListaCnaeSecundario(List<String> listaCnaeSecundario) {
		this.listaCnaeSecundario = listaCnaeSecundario;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getUrl2CaptchaNovaImagem() {
		return url2CaptchaNovaImagem;
	}
	public void setUrl2CaptchaNovaImagem(String url2CaptchaNovaImagem) {
		this.url2CaptchaNovaImagem = url2CaptchaNovaImagem;
	}
}