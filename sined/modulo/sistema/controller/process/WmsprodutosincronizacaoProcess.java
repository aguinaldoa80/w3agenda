package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.WmsBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.WmsprodutosincronizacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.wms.WmsWebServiceStub;

@Controller(
		path="/sistema/process/Wmsprodutosincronizacao",
		authorizationModule=ProcessAuthorizationModule.class
)
public class WmsprodutosincronizacaoProcess extends MultiActionController {
	
	private MaterialService materialService;
	private EmpresaService empresaService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, WmsprodutosincronizacaoFiltro filtro){
		request.setAttribute("listaEmpresa", empresaService.findEmpresaIntegracaoWMS());
		return new ModelAndView("process/wmsprodutosincronizacao", "filtro", filtro);
	}
	
	@Action("sincronizarProduto")
	public ModelAndView sincronizarProduto(WebRequestContext request, WmsprodutosincronizacaoFiltro filtro) throws Exception {
		if(filtro.getEmpresa() == null){
			request.addError("O campo Empresa é obrigatório");
		}else {
			WmsWebServiceStub stub = null;
			try {
				stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(filtro.getEmpresa()) + "/webservices/WmsWebService");
			} catch (Exception e){
				request.addError("Erro na url de integração: " + e.getMessage());
			}
			
			if(stub != null){
				List<Material> listaMaterial = materialService.findForSincronizacaoWms(filtro.getMaterial());
				if(SinedUtil.isListNotEmpty(listaMaterial)){
					List<WmsBean> lista = materialService.sincronizarProdutoWMS(filtro.getEmpresa(), listaMaterial);
					if(lista == null || lista.size() < listaMaterial.size()){
						request.addMessage("Sincronização realizada com sucesso.");
					}
					if(SinedUtil.isListNotEmpty(lista)){
						Integer qtdeItens = lista.size();
						request.addError("Erro na sincronização de " + lista.size() + " " + (qtdeItens > 1 ? "itens" : "item"));
						request.setAttribute("listaWmsBean", lista);
					}
				}else {
					request.addError("Nenhum item encontrado.");
				}
			}
		}
		return continueOnAction("carregar", filtro); 
	}
}
