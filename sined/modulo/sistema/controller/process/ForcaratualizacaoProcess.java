package br.com.linkcom.sined.modulo.sistema.controller.process;


import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;

@Bean
@Controller(path="/sistema/process/Forcaratualizacao", authorizationModule = ProcessAuthorizationModule.class)
public class ForcaratualizacaoProcess extends MultiActionController {
	
	@DefaultAction
	public ModelAndView doFilter(WebRequestContext requestContext){
		return new ModelAndView("process/forcarAtualizacao");
	}
	
	
}
