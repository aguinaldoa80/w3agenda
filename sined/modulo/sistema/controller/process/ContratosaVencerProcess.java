package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.ContratosaVencer;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/sistema/process/ContratosaVencer", authorizationModule=ProcessAuthorizationModule.class)
@Bean
public class ContratosaVencerProcess extends MultiActionController {
	
	private ContratoService contratoService;
	public void setContratoService(ContratoService contratoService) { this.contratoService = contratoService; 	}
	
	/**
	 * M�todo respons�vel por retornar os contratos a serem exibidos no dashboard
	 * @param request
	 * @return
	 * @since 27/06/2016
	 * @author C�sar
	 */
	public ModelAndView contratosaVencer(WebRequestContext request){
		Date dataAtual = new Date(System.currentTimeMillis());
		Date dataLimite = SinedDateUtils.addDiasData(dataAtual, 10);//Considerando 10 dias de vencimento 
		List<Contrato> listaCont = contratoService.findForPainelContratos(dataAtual, dataLimite);
		List<ContratosaVencer> listaContaV = new ArrayList<ContratosaVencer>();
		
		if(SinedUtil.isListNotEmpty(listaCont)){
			for (Contrato cont : listaCont) {
				ContratosaVencer contV = new ContratosaVencer();
				contV.setCdcontrato(cont.getCdcontrato());
				contV.setDatavencimento(cont.getDtproximovencimento());
				contV.setIdentificador(cont.getIdentificador());
				contV.setCliente(cont.getCliente());
				contV.setDescricao(cont.getDescricao());
				listaContaV.add(contV);
			}
		}
		return new JsonModelAndView().addObject("contrato", listaContaV);
	}
}
