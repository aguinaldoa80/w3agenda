package br.com.linkcom.sined.modulo.sistema.controller.process;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.AuthorizationModule;
import br.com.linkcom.neo.authorization.AuthorizationProcessFilter;
import br.com.linkcom.neo.authorization.AuthorizationProcessItemFilter;
import br.com.linkcom.neo.authorization.DefaultAuthorizationProcess;
import br.com.linkcom.neo.authorization.HasAccessAuthorizationModule;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.service.TelaService;
import br.com.linkcom.sined.util.LicenseManager;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/sistema/process/Autorizacao", authorizationModule = ProcessAuthorizationModule.class)
public class AuthorizationProcess extends DefaultAuthorizationProcess {

	private TelaService telaService;
	
	public void setTelaService(TelaService telaService) {
		this.telaService = telaService;
	}
	
	@Override
	protected String translatePath(String string) {
		return telaService.getTelaDescriptionByUrl(string) + " - (" + string+")";
	}

	@Override
	@Action("lista")
	public ModelAndView list(WebRequestContext request, AuthorizationProcessFilter authorizationFilter) {
		ModelAndView modelandview = super.list(request, authorizationFilter);
		
		String moduloFilter = request.getParameter("modulo");
		String moduloFilterAjustado = "Recursos Humanos".equalsIgnoreCase(moduloFilter) ? "rh" : moduloFilter;
		String descricao = request.getParameter("descricao");
		List<String> listaModulos = new ArrayList<String>();
		
		Class[] controllerClasses = findControllerClasses(request.getWebApplicationContext());
		for (Class<?> controllerClass : controllerClasses) {
			Controller controller = controllerClass.getAnnotation(Controller.class);
			try {
				AuthorizationModule authorizationModule = controller.authorizationModule().newInstance();
				if(!(authorizationModule instanceof HasAccessAuthorizationModule)){
					String path = controller.path()[0];
					String modulo = path.substring(0, 1).equals("/")? path.split("/")[1]: path.split("/")[0];
					modulo = SinedUtil.getNomeModulo(modulo.toLowerCase());
					if(!listaModulos.contains(modulo)){
						listaModulos.add(modulo);
					}
				}
				
			}catch (Exception e) {
				
			}
		}
				
			
		Map<String, List<AuthorizationProcessItemFilter>> groupAuthorizationMap = authorizationFilter.getGroupAuthorizationMap();
		if (groupAuthorizationMap != null && groupAuthorizationMap.size() > 0){
			Set<String> keySet = groupAuthorizationMap.keySet();
			for (String string : keySet) {
				List<AuthorizationProcessItemFilter> list = groupAuthorizationMap.get(string);
							
				for (Iterator<AuthorizationProcessItemFilter> iterator = list.iterator(); iterator.hasNext();) {
					AuthorizationProcessItemFilter authorizationProcessItemFilter = iterator.next();
					
					String path = authorizationProcessItemFilter.getPath();
					String description = authorizationProcessItemFilter.getDescription();
					String moduloAux = path.split("/")[1].toLowerCase();
					if (!LicenseManager.telaLicenciada(path)){
						iterator.remove();
						continue;
					}
					
					if (moduloFilterAjustado != null && !moduloFilterAjustado.equals("") && !moduloFilterAjustado.equals("<null>") && 
							!Util.strings.tiraAcento(moduloAux.trim()).toUpperCase().equals(Util.strings.tiraAcento(moduloFilterAjustado.trim()).toUpperCase())){
						iterator.remove();
						continue;
					}
					
					if (descricao != null && !descricao.equals("") && 
							Util.strings.tiraAcento(description).toUpperCase().indexOf(Util.strings.tiraAcento(descricao).toUpperCase()) == -1){
						iterator.remove();
						continue;
					}
				}
//				for (int i = 0; i < list.size(); i++) {
//					AuthorizationProcessItemFilter authorizationProcessItemFilter = list.get(i);
//					String path = authorizationProcessItemFilter.getPath();
//					String description = authorizationProcessItemFilter.getDescription();
//					if (!LicenseManager.telaLicenciada(path)){
//						list.remove(authorizationProcessItemFilter);
//						--i;
//					}
//					
//					if (modulo != null && !modulo.equals("") && !modulo.equals("<null>") && 
//							Util.strings.tiraAcento(path).toUpperCase().indexOf(Util.strings.tiraAcento(modulo).toUpperCase()) == -1){
//						list.remove(authorizationProcessItemFilter);
//						--i;
//					}
//					
//					if (descricao != null && !descricao.equals("") && 
//							Util.strings.tiraAcento(description).toUpperCase().indexOf(Util.strings.tiraAcento(descricao).toUpperCase()) == -1){
//						list.remove(authorizationProcessItemFilter);
//						--i;
//					}
//				}
			}
		}
		
		request.setAttribute("descricaoSelecionada", descricao);
		request.setAttribute("moduloSelecionado", moduloFilter);
		request.setAttribute("listaModulosTelaAutorizacao", listaModulos);
		
		return modelandview;
	}	
	
}
