package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.util.List;

import br.com.linkcom.sined.modulo.sistema.controller.process.NatOprCfopSPEDFiscal;

public class ConferenciaNaturezaOperacaoFiltro {
	
	private List<NatOprCfopSPEDFiscal> listaNatOprCfopSPEDFiscal;

	public List<NatOprCfopSPEDFiscal> getListaNatOprCfopSPEDFiscal() {
		return listaNatOprCfopSPEDFiscal;
	}
	
	public void setListaNatOprCfopSPEDFiscal(List<NatOprCfopSPEDFiscal> listaNatOprCfopSPEDFiscal) {
		this.listaNatOprCfopSPEDFiscal = listaNatOprCfopSPEDFiscal;
	}
}
