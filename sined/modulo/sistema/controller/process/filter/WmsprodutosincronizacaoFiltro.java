package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;

public class WmsprodutosincronizacaoFiltro {
	
	protected Empresa empresa;
	protected Material material;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	public Material getMaterial() {
		return material;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
}
