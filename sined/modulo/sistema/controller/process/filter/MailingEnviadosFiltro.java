package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;

public class MailingEnviadosFiltro {

	private Envioemail envioemail;
	private Envioemailtipo envioemailtipo;
	private String nomeDestinatario;
	private String emailDestinatario;
	private String nomeContato;
	private Date dataEnvioDe;
	private Date dataEnvioAte;
	private List<EmailStatusEnum> situacaoemail;
	

	@DisplayName("Tipo de e-mail")
	public Envioemailtipo getEnvioemailtipo() {
		return envioemailtipo;
	}

	public Envioemail getEnvioemail() {
		return envioemail;
	}

	@DisplayName("Nome do Destinatário")
	public String getNomeDestinatario() {
		return nomeDestinatario;
	}

	@DisplayName("E-mail do Destinatário")
	public String getEmailDestinatario() {
		return emailDestinatario;
	}

	@Required
	public Date getDataEnvioDe() {
		return dataEnvioDe;
	}

	@Required
	public Date getDataEnvioAte() {
		return dataEnvioAte;
	}

	public void setNomeDestinatario(String nomeDestinatario) {
		this.nomeDestinatario = nomeDestinatario;
	}

	public void setEmailDestinatario(String emailDestinatario) {
		this.emailDestinatario = emailDestinatario;
	}

	public void setDataEnvioDe(Date dataEnvioDe) {
		this.dataEnvioDe = dataEnvioDe;
	}

	public void setDataEnvioAte(Date dataEnvioAte) {
		this.dataEnvioAte = dataEnvioAte;
	}

	public void setEnvioemailtipo(Envioemailtipo envioemailtipo) {
		this.envioemailtipo = envioemailtipo;
	}

	public void setEnvioemail(Envioemail envioemail) {
		this.envioemail = envioemail;
	}

	public String getNomeContato() {
		return nomeContato;
	}
	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}
	
	@DisplayName("Situação do e-mail")
	public List<EmailStatusEnum> getSituacaoemail() {
		return situacaoemail;
	}
	public void setSituacaoemail(List<EmailStatusEnum> situacaoemail) {
		this.situacaoemail = situacaoemail;
	}
}