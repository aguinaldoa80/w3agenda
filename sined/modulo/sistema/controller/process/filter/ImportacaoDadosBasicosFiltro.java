package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.ImportacaoDadosBasicosTipo;

public class ImportacaoDadosBasicosFiltro {
	
	private Empresa empresa;
	
	private ImportacaoDadosBasicosTipo importacaoDadosBasicosTipo; 
	private Arquivo arquivo;
	
	private String chaveArquivo;
	
	private List<ImportacaoDadosMaterialBean> listaMaterialBean;
	private List<ImportacaoDadosPessoaBean> listaPessoaBean;
	private List<ImportacaoDadosLeadBean> listaLeadBean;
	
	private List<ImportacaoDadosPessoaBean> listaPessoaBeanComErro;
	private List<ImportacaoDadosMaterialBean> listaMateriaisComErro;
	private List<ImportacaoDadosLeadBean> listaLeadComErro;
	
	
	//listas usadas para agrupar os registros por erro, para Pessoa
	private List<ImportacaoDadosPessoaBean> listaErrosNome;
	private List<ImportacaoDadosPessoaBean> listaErrosCpfCnpj;
	private List<ImportacaoDadosPessoaBean> listaErrosClienteFornecedor;
	private List<ImportacaoDadosPessoaBean> listaErrosMunicipio;
	private List<ImportacaoDadosPessoaBean> listaErrosUf;
	private List<ImportacaoDadosPessoaBean> listaErrosCep;
	private List<ImportacaoDadosPessoaBean> listaErrosCategoria;
	private List<ImportacaoDadosPessoaBean> listaErrosDataNascimento;
	private List<ImportacaoDadosPessoaBean> listaErrosEspecialidade;
	private List<ImportacaoDadosPessoaBean> listaErrosRegistro;
	
	//listas usadas para agrupar os registros por erro, para Material
	private List<ImportacaoDadosMaterialBean> listaErrosNomeMaterial;
	private List<ImportacaoDadosMaterialBean> listaErrosGrupoMaterial;
	private List<ImportacaoDadosMaterialBean> listaErrosUnidadeMedida;
	private List<ImportacaoDadosMaterialBean> listaErrosClasse;
	private List<ImportacaoDadosMaterialBean> listaErrosContaGerencial;
	private List<ImportacaoDadosMaterialBean> listaErrosCentroCusto;
	private List<ImportacaoDadosMaterialBean> listaErrosValorCusto;
	private List<ImportacaoDadosMaterialBean> listaErrosValorVenda;
	
	//listas usadas para agrupar os registros por erro, para Lead
	private List<ImportacaoDadosLeadBean> listaLeadErrosContato;
	private List<ImportacaoDadosLeadBean> listaLeadErrosSituacao;
	private List<ImportacaoDadosLeadBean> listaLeadErrosQualificacao;
	private List<ImportacaoDadosLeadBean> listaLeadErrosResponsavel;
	private List<ImportacaoDadosLeadBean> listaLeadErrosMunicipio;
	private List<ImportacaoDadosLeadBean> listaLeadErrosUf;
	private List<ImportacaoDadosLeadBean> listaLeadErrosCep;
	private List<ImportacaoDadosLeadBean> listaLeadErrosSegmento;
	private List<ImportacaoDadosLeadBean> listaLeadErrosSexo;
	private List<ImportacaoDadosLeadBean> listaLeadErrosDtretorno;
	private List<ImportacaoDadosLeadBean> listaLeadErrosCampanha;
	private List<ImportacaoDadosLeadBean> listaLeadErrosProximoPasso;
	
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@Required
	@DisplayName("Destino")
	public ImportacaoDadosBasicosTipo getImportacaoDadosBasicosTipo() {
		return importacaoDadosBasicosTipo;
	}
	
	public List<ImportacaoDadosMaterialBean> getListaMaterialBean() {
		return listaMaterialBean;
	}
	
	public List<ImportacaoDadosPessoaBean> getListaPessoaBean() {
		return listaPessoaBean;
	}
	
	public String getChaveArquivo() {
		return chaveArquivo;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public List<ImportacaoDadosLeadBean> getListaLeadBean() {
		return listaLeadBean;
	}
	
	public void setListaLeadBean(List<ImportacaoDadosLeadBean> listaLeadBean) {
		this.listaLeadBean = listaLeadBean;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setChaveArquivo(String chaveArquivo) {
		this.chaveArquivo = chaveArquivo;
	}
	
	public void setListaPessoaBean(
			List<ImportacaoDadosPessoaBean> listaPessoaBean) {
		this.listaPessoaBean = listaPessoaBean;
	}
	
	public void setListaMaterialBean(
			List<ImportacaoDadosMaterialBean> listaMaterialBean) {
		this.listaMaterialBean = listaMaterialBean;
	}
	
	public void setImportacaoDadosBasicosTipo(
			ImportacaoDadosBasicosTipo importacaoDadosBasicosTipo) {
		this.importacaoDadosBasicosTipo = importacaoDadosBasicosTipo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public List<ImportacaoDadosPessoaBean> getListaPessoaBeanComErro() {
		return listaPessoaBeanComErro;
	}
	public void setListaPessoaBeanComErro(List<ImportacaoDadosPessoaBean> listaPessoaBeanComErro) {
		this.listaPessoaBeanComErro = listaPessoaBeanComErro;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosNome() {
		return listaErrosNome;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosCpfCnpj() {
		return listaErrosCpfCnpj;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosClienteFornecedor() {
		return listaErrosClienteFornecedor;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosMunicipio() {
		return listaErrosMunicipio;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosUf() {
		return listaErrosUf;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosCep() {
		return listaErrosCep;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosCategoria() {
		return listaErrosCategoria;
	}
	
	public List<ImportacaoDadosPessoaBean> getListaErrosDataNascimento() {
		return listaErrosDataNascimento;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosEspecialidade() {
		return listaErrosEspecialidade;
	}

	public List<ImportacaoDadosPessoaBean> getListaErrosRegistro() {
		return listaErrosRegistro;
	}

	public void setListaErrosNome(List<ImportacaoDadosPessoaBean> listaErrosNome) {
		this.listaErrosNome = listaErrosNome;
	}

	public void setListaErrosCpfCnpj(List<ImportacaoDadosPessoaBean> listaErrosCpfCnpj) {
		this.listaErrosCpfCnpj = listaErrosCpfCnpj;
	}

	public void setListaErrosClienteFornecedor(List<ImportacaoDadosPessoaBean> listaErrosClienteFornecedor) {
		this.listaErrosClienteFornecedor = listaErrosClienteFornecedor;
	}

	public void setListaErrosMunicipio(List<ImportacaoDadosPessoaBean> listaErrosMunicipio) {
		this.listaErrosMunicipio = listaErrosMunicipio;
	}

	public void setListaErrosUf(List<ImportacaoDadosPessoaBean> listaErrosUf) {
		this.listaErrosUf = listaErrosUf;
	}

	public void setListaErrosCep(List<ImportacaoDadosPessoaBean> listaErrosCep) {
		this.listaErrosCep = listaErrosCep;
	}

	public void setListaErrosCategoria(List<ImportacaoDadosPessoaBean> listaErrosCategoria) {
		this.listaErrosCategoria = listaErrosCategoria;
	}
	
	public void setListaErrosDataNascimento(List<ImportacaoDadosPessoaBean> listaErrosDataNascimento) {
		this.listaErrosDataNascimento = listaErrosDataNascimento;
	}

	public void setListaErrosEspecialidade(List<ImportacaoDadosPessoaBean> listaErrosEspecialidade) {
		this.listaErrosEspecialidade = listaErrosEspecialidade;
	}
	
	public void setListaErrosRegistro(List<ImportacaoDadosPessoaBean> listaErrosRegistro) {
		this.listaErrosRegistro = listaErrosRegistro;
	}

	public List<ImportacaoDadosMaterialBean> getListaErrosNomeMaterial() {
		return listaErrosNomeMaterial;
	}

	public List<ImportacaoDadosMaterialBean> getListaErrosGrupoMaterial() {
		return listaErrosGrupoMaterial;
	}

	public List<ImportacaoDadosMaterialBean> getListaErrosUnidadeMedida() {
		return listaErrosUnidadeMedida;
	}

	public List<ImportacaoDadosMaterialBean> getListaErrosClasse() {
		return listaErrosClasse;
	}

	public List<ImportacaoDadosMaterialBean> getListaErrosContaGerencial() {
		return listaErrosContaGerencial;
	}

	public List<ImportacaoDadosMaterialBean> getListaErrosCentroCusto() {
		return listaErrosCentroCusto;
	}

	public List<ImportacaoDadosMaterialBean> getListaErrosValorCusto() {
		return listaErrosValorCusto;
	}

	public List<ImportacaoDadosMaterialBean> getListaErrosValorVenda() {
		return listaErrosValorVenda;
	}

	public void setListaErrosNomeMaterial(
			List<ImportacaoDadosMaterialBean> listaErrosNomeMaterial) {
		this.listaErrosNomeMaterial = listaErrosNomeMaterial;
	}

	public void setListaErrosGrupoMaterial(
			List<ImportacaoDadosMaterialBean> listaErrosGrupoMaterial) {
		this.listaErrosGrupoMaterial = listaErrosGrupoMaterial;
	}

	public void setListaErrosUnidadeMedida(
			List<ImportacaoDadosMaterialBean> listaErrosUnidadeMedida) {
		this.listaErrosUnidadeMedida = listaErrosUnidadeMedida;
	}

	public void setListaErrosClasse(
			List<ImportacaoDadosMaterialBean> listaErrosClasse) {
		this.listaErrosClasse = listaErrosClasse;
	}

	public void setListaErrosContaGerencial(
			List<ImportacaoDadosMaterialBean> listaErrosContaGerencial) {
		this.listaErrosContaGerencial = listaErrosContaGerencial;
	}

	public void setListaErrosCentroCusto(
			List<ImportacaoDadosMaterialBean> listaErrosCentroCusto) {
		this.listaErrosCentroCusto = listaErrosCentroCusto;
	}

	public void setListaErrosValorCusto(
			List<ImportacaoDadosMaterialBean> listaErrosValorCusto) {
		this.listaErrosValorCusto = listaErrosValorCusto;
	}

	public void setListaErrosValorVenda(
			List<ImportacaoDadosMaterialBean> listaErrosValorVenda) {
		this.listaErrosValorVenda = listaErrosValorVenda;
	}
	public List<ImportacaoDadosMaterialBean> getListaMateriaisComErro() {
		return listaMateriaisComErro;
	}
	public void setListaMateriaisComErro(
			List<ImportacaoDadosMaterialBean> listaMateriaisComErro) {
		this.listaMateriaisComErro = listaMateriaisComErro;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadComErro() {
		return listaLeadComErro;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadErrosContato() {
		return listaLeadErrosContato;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadErrosSituacao() {
		return listaLeadErrosSituacao;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadErrosQualificacao() {
		return listaLeadErrosQualificacao;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadErrosResponsavel() {
		return listaLeadErrosResponsavel;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadErrosMunicipio() {
		return listaLeadErrosMunicipio;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadErrosUf() {
		return listaLeadErrosUf;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadErrosCep() {
		return listaLeadErrosCep;
	}

	public List<ImportacaoDadosLeadBean> getListaLeadErrosSegmento() {
		return listaLeadErrosSegmento;
	}
	
	public List<ImportacaoDadosLeadBean> getListaLeadErrosCampanha() {
		return listaLeadErrosCampanha;
	}
	
	public void setListaLeadErrosCampanha(
			List<ImportacaoDadosLeadBean> listaLeadErrosCampanha) {
		this.listaLeadErrosCampanha = listaLeadErrosCampanha;
	}

	public void setListaLeadComErro(List<ImportacaoDadosLeadBean> listaLeadComErro) {
		this.listaLeadComErro = listaLeadComErro;
	}

	public void setListaLeadErrosContato(
			List<ImportacaoDadosLeadBean> listaLeadErrosContato) {
		this.listaLeadErrosContato = listaLeadErrosContato;
	}

	public void setListaLeadErrosSituacao(
			List<ImportacaoDadosLeadBean> listaLeadErrosSituacao) {
		this.listaLeadErrosSituacao = listaLeadErrosSituacao;
	}

	public void setListaLeadErrosQualificacao(
			List<ImportacaoDadosLeadBean> listaLeadErrosQualificacao) {
		this.listaLeadErrosQualificacao = listaLeadErrosQualificacao;
	}

	public void setListaLeadErrosResponsavel(
			List<ImportacaoDadosLeadBean> listaLeadErrosResponsavel) {
		this.listaLeadErrosResponsavel = listaLeadErrosResponsavel;
	}

	public void setListaLeadErrosMunicipio(
			List<ImportacaoDadosLeadBean> listaLeadErrosMunicipio) {
		this.listaLeadErrosMunicipio = listaLeadErrosMunicipio;
	}

	public void setListaLeadErrosUf(List<ImportacaoDadosLeadBean> listaLeadErrosUf) {
		this.listaLeadErrosUf = listaLeadErrosUf;
	}

	public void setListaLeadErrosCep(List<ImportacaoDadosLeadBean> listaLeadErrosCep) {
		this.listaLeadErrosCep = listaLeadErrosCep;
	}

	public void setListaLeadErrosSegmento(
			List<ImportacaoDadosLeadBean> listaLeadErrosSegmento) {
		this.listaLeadErrosSegmento = listaLeadErrosSegmento;
	}
	
	public List<ImportacaoDadosLeadBean> getListaLeadErrosSexo() {
		return listaLeadErrosSexo;
	}
	
	public void setListaLeadErrosSexo(
			List<ImportacaoDadosLeadBean> listaLeadErrosSexo) {
		this.listaLeadErrosSexo = listaLeadErrosSexo;
	}
	
	public List<ImportacaoDadosLeadBean> getListaLeadErrosDtretorno() {
		return listaLeadErrosDtretorno;
	}
	
	public void setListaLeadErrosDtretorno(
			List<ImportacaoDadosLeadBean> listaLeadErrosDtretorno) {
		this.listaLeadErrosDtretorno = listaLeadErrosDtretorno;
	}
	
	public List<ImportacaoDadosLeadBean> getListaLeadErrosProximoPasso() {
		return listaLeadErrosProximoPasso;
	}
	
	public void setListaLeadErrosProximoPasso(List<ImportacaoDadosLeadBean> listaLeadErrosProximoPasso) {
		this.listaLeadErrosProximoPasso = listaLeadErrosProximoPasso;
	}
}
