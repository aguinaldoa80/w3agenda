package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;

public class ArquivolicencaFiltro {
	
	protected Arquivo arquivo;
	protected String validade;
	protected String cnpj;
	
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public String getValidade() {
		return validade;
	}
	
	public String getCnpj() {
		return cnpj;
	}
		
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setValidade(String validade) {
		this.validade = validade;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
}
