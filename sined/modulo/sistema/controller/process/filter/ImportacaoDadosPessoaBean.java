package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Especialidade;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Uf;


public class ImportacaoDadosPessoaBean {
	
	// STRINGS
	private String identificador;
	private String nome;
	private String razaosocial;
	private String cpfcnpj;
	private String inscricaoestadual;
	private String inscricaomunicipal;
	private String email;
	private String telefoneprincipal;
	private String telefonecelular;
	private String telefonefax;
	private String contatonome;
	private String contatoemail;
	private String enderecologradouro;
	private String endereconumero;
	private String enderecocomplemento;
	private String enderecobairro;
	private String enderecoufStr;
	private String enderecomunicipioStr;
	private String enderecocepStr;
	private String categoriaStr;
	private String observacao;
	private String dataNascimentoStr;
	private String sexoStr;
	
	private String especialidadeStr1;
	private String registro1;
	private String especialidadeStr2;
	private String registro2;
	private String especialidadeStr3;
	private String registro3;
	private String especialidadeStr4;
	private String registro4;
	private String especialidadeStr5;
	private String registro5;
	
	private String associacao;
	
	// BEANS A SEREM CARREGADOS
	private Cpf cpf;
	private Cnpj cnpj;
	private Uf enderecouf;
	private Municipio enderecomunicipio;
	private Categoria categoria;
	private Cep enderecocep;
	private Date dtnascimento;
	private Sexo sexo;
	private Fornecedor fornecedorassociacao;
	private Especialidade especialidade1;
	private Especialidade especialidade2;
	private Especialidade especialidade3;
	private Especialidade especialidade4;
	private Especialidade especialidade5;
	
	// AUXILIARES
	private Boolean marcado = Boolean.TRUE;
	private Integer linha;
	private String erro;
	private String avisos;
	private Integer cdpessoa;
	
	public Boolean getMarcado() {
		return marcado;
	}
	public Integer getLinha() {
		return linha;
	}
	public String getErro() {
		return erro;
	}
	public String getAvisos() {
		return avisos;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getNome() {
		return nome;
	}
	public String getRazaosocial() {
		return razaosocial;
	}
	public String getCpfcnpj() {
		return cpfcnpj;
	}
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}
	public String getEmail() {
		return email;
	}
	public String getTelefoneprincipal() {
		return telefoneprincipal;
	}
	public String getTelefonecelular() {
		return telefonecelular;
	}
	public String getContatonome() {
		return contatonome;
	}
	public String getContatoemail() {
		return contatoemail;
	}
	public String getEnderecologradouro() {
		return enderecologradouro;
	}
	public String getEndereconumero() {
		return endereconumero;
	}
	public String getEnderecocomplemento() {
		return enderecocomplemento;
	}
	public String getEnderecobairro() {
		return enderecobairro;
	}
	public String getEnderecoufStr() {
		return enderecoufStr;
	}
	public String getEnderecomunicipioStr() {
		return enderecomunicipioStr;
	}
	public String getCategoriaStr() {
		return categoriaStr;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getDataNascimentoStr() {
		return dataNascimentoStr;
	}
	public String getSexoStr() {
		return sexoStr;
	}
	public String getAssociacao() {
		return associacao;
	}
	public Cpf getCpf() {
		return cpf;
	}
	public Cnpj getCnpj() {
		return cnpj;
	}
	public Uf getEnderecouf() {
		return enderecouf;
	}
	public Municipio getEnderecomunicipio() {
		return enderecomunicipio;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public String getTelefonefax() {
		return telefonefax;
	}
	public String getEnderecocepStr() {
		return enderecocepStr;
	}
	public Cep getEnderecocep() {
		return enderecocep;
	}
	public Date getDtnascimento() {
		return dtnascimento;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public Fornecedor getFornecedorassociacao() {
		return fornecedorassociacao;
	}
	public void setEnderecocepStr(String enderecocepStr) {
		this.enderecocepStr = enderecocepStr;
	}
	public void setEnderecocep(Cep enderecocep) {
		this.enderecocep = enderecocep;
	}
	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public void setFornecedorassociacao(Fornecedor fornecedorassociacao) {
		this.fornecedorassociacao = fornecedorassociacao;
	}
	public void setAssociacao(String associacao) {
		this.associacao = associacao;
	}
	public void setTelefonefax(String telefonefax) {
		this.telefonefax = telefonefax;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setTelefoneprincipal(String telefoneprincipal) {
		this.telefoneprincipal = telefoneprincipal;
	}
	public void setTelefonecelular(String telefonecelular) {
		this.telefonecelular = telefonecelular;
	}
	public void setContatonome(String contatonome) {
		this.contatonome = contatonome;
	}
	public void setContatoemail(String contatoemail) {
		this.contatoemail = contatoemail;
	}
	public void setEnderecologradouro(String enderecologradouro) {
		this.enderecologradouro = enderecologradouro;
	}
	public void setEndereconumero(String endereconumero) {
		this.endereconumero = endereconumero;
	}
	public void setEnderecocomplemento(String enderecocomplemento) {
		this.enderecocomplemento = enderecocomplemento;
	}
	public void setEnderecobairro(String enderecobairro) {
		this.enderecobairro = enderecobairro;
	}
	public void setEnderecoufStr(String enderecoufStr) {
		this.enderecoufStr = enderecoufStr;
	}
	public void setEnderecomunicipioStr(String enderecomunicipioStr) {
		this.enderecomunicipioStr = enderecomunicipioStr;
	}
	public void setCategoriaStr(String categoriaStr) {
		this.categoriaStr = categoriaStr;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDataNascimentoStr(String dataNascimentoStr) {
		this.dataNascimentoStr = dataNascimentoStr;
	}
	public void setSexoStr(String sexoStr) {
		this.sexoStr = sexoStr;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setEnderecouf(Uf enderecouf) {
		this.enderecouf = enderecouf;
	}
	public void setEnderecomunicipio(Municipio enderecomunicipio) {
		this.enderecomunicipio = enderecomunicipio;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setLinha(Integer linha) {
		this.linha = linha;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public void setAvisos(String avisos) {
		this.avisos = avisos;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	
	public String getEspecialidadeStr1() {
		return especialidadeStr1;
	}
	public String getRegistro1() {
		return registro1;
	}
	public String getEspecialidadeStr2() {
		return especialidadeStr2;
	}
	public String getRegistro2() {
		return registro2;
	}
	public String getEspecialidadeStr3() {
		return especialidadeStr3;
	}
	public String getRegistro3() {
		return registro3;
	}
	public String getEspecialidadeStr4() {
		return especialidadeStr4;
	}
	public String getRegistro4() {
		return registro4;
	}
	public String getEspecialidadeStr5() {
		return especialidadeStr5;
	}
	public String getRegistro5() {
		return registro5;
	}
	public Especialidade getEspecialidade1() {
		return especialidade1;
	}
	public Especialidade getEspecialidade2() {
		return especialidade2;
	}
	public Especialidade getEspecialidade3() {
		return especialidade3;
	}
	public Especialidade getEspecialidade4() {
		return especialidade4;
	}
	public Especialidade getEspecialidade5() {
		return especialidade5;
	}
	public void setEspecialidadeStr1(String especialidadeStr1) {
		this.especialidadeStr1 = especialidadeStr1;
	}
	public void setRegistro1(String registro1) {
		this.registro1 = registro1;
	}
	public void setEspecialidadeStr2(String especialidadeStr2) {
		this.especialidadeStr2 = especialidadeStr2;
	}
	public void setRegistro2(String registro2) {
		this.registro2 = registro2;
	}
	public void setEspecialidadeStr3(String especialidadeStr3) {
		this.especialidadeStr3 = especialidadeStr3;
	}
	public void setRegistro3(String registro3) {
		this.registro3 = registro3;
	}
	public void setEspecialidadeStr4(String especialidadeStr4) {
		this.especialidadeStr4 = especialidadeStr4;
	}
	public void setRegistro4(String registro4) {
		this.registro4 = registro4;
	}
	public void setEspecialidadeStr5(String especialidadeStr5) {
		this.especialidadeStr5 = especialidadeStr5;
	}
	public void setRegistro5(String registro5) {
		this.registro5 = registro5;
	}
	public void setEspecialidade1(Especialidade especialidade1) {
		this.especialidade1 = especialidade1;
	}
	public void setEspecialidade2(Especialidade especialidade2) {
		this.especialidade2 = especialidade2;
	}
	public void setEspecialidade3(Especialidade especialidade3) {
		this.especialidade3 = especialidade3;
	}
	public void setEspecialidade4(Especialidade especialidade4) {
		this.especialidade4 = especialidade4;
	}
	public void setEspecialidade5(Especialidade especialidade5) {
		this.especialidade5 = especialidade5;
	}
}
