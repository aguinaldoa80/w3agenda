package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.geral.bean.Leadqualificacao;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Uf;


public class ImportacaoDadosLeadBean {

	// STRINGS
	private String contato;
	private String sexoStr;
	private String empresa;
	private String email;
	private String website;
	private String responsavelCpfStr;
	private String situacaoStr;
	private String qualificacaoStr;
	private String telefonePrincipal;
	private String telefoneCelular;
	private String telefoneComercial;
	private String segmentoStr;
	private String enderecologradouro;
	private String endereconumero;
	private String enderecocomplemento;
	private String enderecobairro;
	private String enderecoufStr;
	private String enderecomunicipioStr;
	private String enderecocepStr;
	private String dtretornoStr;
	private String observacao;
	private String concorrenteStr;
	private String campanhaStr;
	private String proximoPasso;
	
	// BEANS A SEREM CARREGADOS
	private Sexo sexo;
	private Colaborador responsavel;
	private Leadsituacao situacao;
	private Leadqualificacao qualificacao;
	private Segmento segmento;
	private Uf enderecouf;
	private Municipio enderecomunicipio;
	private Cep enderecocep;
	private Cpf responsavelCpf;
	private Date dtretorno;
	private Concorrente concorrente;
	private Campanha campanha;
	
	// AUXILIARES
	private Boolean marcado = Boolean.TRUE;
	private Integer linha;
	private String erro;
	private String avisos;
	private Integer cdlead;
	
	public String getContato() {
		return contato;
	}
	public String getSexoStr() {
		return sexoStr;
	}
	public String getEmpresa() {
		return empresa;
	}
	public String getEmail() {
		return email;
	}
	public String getWebsite() {
		return website;
	}
	public String getResponsavelCpfStr() {
		return responsavelCpfStr;
	}
	public String getSituacaoStr() {
		return situacaoStr;
	}
	public String getQualificacaoStr() {
		return qualificacaoStr;
	}
	public String getTelefonePrincipal() {
		return telefonePrincipal;
	}
	public String getTelefoneCelular() {
		return telefoneCelular;
	}
	public String getTelefoneComercial() {
		return telefoneComercial;
	}
	public String getSegmentoStr() {
		return segmentoStr;
	}
	public String getEnderecologradouro() {
		return enderecologradouro;
	}
	public String getEndereconumero() {
		return endereconumero;
	}
	public String getEnderecocomplemento() {
		return enderecocomplemento;
	}
	public String getEnderecobairro() {
		return enderecobairro;
	}
	public String getEnderecoufStr() {
		return enderecoufStr;
	}
	public String getEnderecomunicipioStr() {
		return enderecomunicipioStr;
	}
	public String getEnderecocepStr() {
		return enderecocepStr;
	}
	public String getDtretornoStr() {
		return dtretornoStr;
	}
	public String getObservacao() {
		return observacao;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public Colaborador getResponsavel() {
		return responsavel;
	}
	public Leadsituacao getSituacao() {
		return situacao;
	}
	public Leadqualificacao getQualificacao() {
		return qualificacao;
	}
	public Segmento getSegmento() {
		return segmento;
	}
	public Municipio getEnderecomunicipio() {
		return enderecomunicipio;
	}
	public Uf getEnderecouf() {
		return enderecouf;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public Integer getLinha() {
		return linha;
	}
	public String getErro() {
		return erro;
	}
	public String getAvisos() {
		return avisos;
	}
	public Integer getCdlead() {
		return cdlead;
	}
	public Cpf getResponsavelCpf() {
		return responsavelCpf;
	}
	public Cep getEnderecocep() {
		return enderecocep;
	}
	public String getCampanhaStr() {
		return campanhaStr;
	}
	public Concorrente getConcorrente() {
		return concorrente;
	}
	public String getProximoPasso() {
		return proximoPasso;
	}
	public String getConcorrenteStr() {
		return concorrenteStr;
	}
	public Campanha getCampanha() {
		return campanha;
	}
	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}
	public void setConcorrenteStr(String concorrenteStr) {
		this.concorrenteStr = concorrenteStr;
	}
	public void setCampanhaStr(String campanhaStr) {
		this.campanhaStr = campanhaStr;
	}
	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}
	public void setProximoPasso(String proximoPasso) {
		this.proximoPasso = proximoPasso;
	}
	public void setEnderecocep(Cep enderecocep) {
		this.enderecocep = enderecocep;
	}
	public void setResponsavelCpf(Cpf responsavelCpf) {
		this.responsavelCpf = responsavelCpf;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	public void setSexoStr(String sexoStr) {
		this.sexoStr = sexoStr;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public void setResponsavelCpfStr(String responsavelCpfStr) {
		this.responsavelCpfStr = responsavelCpfStr;
	}
	public void setSituacaoStr(String situacaoStr) {
		this.situacaoStr = situacaoStr;
	}
	public void setQualificacaoStr(String qualificacaoStr) {
		this.qualificacaoStr = qualificacaoStr;
	}
	public void setTelefonePrincipal(String telefonePrincipal) {
		this.telefonePrincipal = telefonePrincipal;
	}
	public void setTelefoneCelular(String telefoneCelular) {
		this.telefoneCelular = telefoneCelular;
	}
	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}
	public void setSegmentoStr(String segmentoStr) {
		this.segmentoStr = segmentoStr;
	}
	public void setEnderecologradouro(String enderecologradouro) {
		this.enderecologradouro = enderecologradouro;
	}
	public void setEndereconumero(String endereconumero) {
		this.endereconumero = endereconumero;
	}
	public void setEnderecocomplemento(String enderecocomplemento) {
		this.enderecocomplemento = enderecocomplemento;
	}
	public void setEnderecobairro(String enderecobairro) {
		this.enderecobairro = enderecobairro;
	}
	public void setEnderecoufStr(String enderecoufStr) {
		this.enderecoufStr = enderecoufStr;
	}
	public void setEnderecomunicipioStr(String enderecomunicipioStr) {
		this.enderecomunicipioStr = enderecomunicipioStr;
	}
	public void setEnderecocepStr(String enderecocepStr) {
		this.enderecocepStr = enderecocepStr;
	}
	public void setDtretornoStr(String dtretornoStr) {
		this.dtretornoStr = dtretornoStr;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setSituacao(Leadsituacao situacao) {
		this.situacao = situacao;
	}
	public void setQualificacao(Leadqualificacao qualificacao) {
		this.qualificacao = qualificacao;
	}
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	public void setEnderecomunicipio(Municipio enderecomunicipio) {
		this.enderecomunicipio = enderecomunicipio;
	}
	public void setEnderecouf(Uf enderecouf) {
		this.enderecouf = enderecouf;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setLinha(Integer linha) {
		this.linha = linha;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public void setAvisos(String avisos) {
		this.avisos = avisos;
	}
	public void setCdlead(Integer cdlead) {
		this.cdlead = cdlead;
	}
	public Date getDtretorno() {
		return dtretorno;
	}
	public void setDtretorno(Date dtretorno) {
		this.dtretorno = dtretorno;
	}
	
}
