package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.util.List;

import br.com.linkcom.sined.modulo.sistema.controller.process.GrupoTributacaoSPEDFiscal;

public class ConferenciaGrupoTributacaoFiltro {

	private List<GrupoTributacaoSPEDFiscal> listaGrupoTributacao;

	public List<GrupoTributacaoSPEDFiscal> getListaGrupoTributacao() {
		return listaGrupoTributacao;
	}
	
	public void setListaGrupoTributacao(List<GrupoTributacaoSPEDFiscal> listaGrupoTributacao) {
		this.listaGrupoTributacao = listaGrupoTributacao;
	}
}
