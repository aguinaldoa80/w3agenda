package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Empresa;

public class EmporiumAcertoExclusaoFiltro {

	private Empresa empresa;
	private Date data1;
	private Date data2;
	private String vendas; 
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Date getData1() {
		return data1;
	}
	public Date getData2() {
		return data2;
	}
	public String getVendas() {
		return vendas;
	}
	public void setVendas(String vendas) {
		this.vendas = vendas;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setData1(Date data1) {
		this.data1 = data1;
	}
	public void setData2(Date data2) {
		this.data2 = data2;
	}
	
}
