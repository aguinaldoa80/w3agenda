package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Unidademedida;


public class ImportacaoDadosMaterialBean {

	// STRINGS
	private String identificador;
	private String referencia;
	private String nome;
	private String nomenf;
	private String materialclasseStr;
	private String unidademedidaStr;
	private String materialgrupoStr;
	private String materialtipoStr;
	private String contagerencialcompraStr;
	private String contagerencialvendaStr;
	private String centrocustovendaStr;
	private String valorcustoStr;
	private String valorvendaStr;
	private String ncm;
	
	// BEANS A SEREM CARREGADOS
	private Materialclasse materialclasse;
	private Unidademedida unidademedida;
	private Materialgrupo materialgrupo;
	private Materialtipo materialtipo;
	private Contagerencial contagerencialcompra;
	private Contagerencial contagerencialvenda;
	private Centrocusto centrocustovenda;
	private Double valorcusto;
	private Double valorvenda;
	
	// AUXILIARES
	private Boolean marcado = Boolean.TRUE;
	private Integer linha;
	private String erro;
	private String avisos;
	private Integer cdmaterial;
	
	public String getIdentificador() {
		return identificador;
	}
	public String getReferencia() {
		return referencia;
	}
	public String getNome() {
		return nome;
	}
	public String getNomenf() {
		return nomenf;
	}
	public String getMaterialclasseStr() {
		return materialclasseStr;
	}
	public String getUnidademedidaStr() {
		return unidademedidaStr;
	}
	public String getMaterialgrupoStr() {
		return materialgrupoStr;
	}
	public String getMaterialtipoStr() {
		return materialtipoStr;
	}
	public String getContagerencialcompraStr() {
		return contagerencialcompraStr;
	}
	public String getContagerencialvendaStr() {
		return contagerencialvendaStr;
	}
	public String getCentrocustovendaStr() {
		return centrocustovendaStr;
	}
	public String getValorcustoStr() {
		return valorcustoStr;
	}
	public String getValorvendaStr() {
		return valorvendaStr;
	}
	public String getNcm() {
		return ncm;
	}
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	public Contagerencial getContagerencialcompra() {
		return contagerencialcompra;
	}
	public Contagerencial getContagerencialvenda() {
		return contagerencialvenda;
	}
	public Centrocusto getCentrocustovenda() {
		return centrocustovenda;
	}
	public Double getValorcusto() {
		return valorcusto;
	}
	public Double getValorvenda() {
		return valorvenda;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public String getErro() {
		return erro;
	}
	public String getAvisos() {
		return avisos;
	}
	public Integer getLinha() {
		return linha;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setLinha(Integer linha) {
		this.linha = linha;
	}
	public void setAvisos(String avisos) {
		this.avisos = avisos;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNomenf(String nomenf) {
		this.nomenf = nomenf;
	}
	public void setMaterialclasseStr(String materialclasseStr) {
		this.materialclasseStr = materialclasseStr;
	}
	public void setUnidademedidaStr(String unidademedidaStr) {
		this.unidademedidaStr = unidademedidaStr;
	}
	public void setMaterialgrupoStr(String materialgrupoStr) {
		this.materialgrupoStr = materialgrupoStr;
	}
	public void setMaterialtipoStr(String materialtipoStr) {
		this.materialtipoStr = materialtipoStr;
	}
	public void setContagerencialcompraStr(String contagerencialcompraStr) {
		this.contagerencialcompraStr = contagerencialcompraStr;
	}
	public void setContagerencialvendaStr(String contagerencialvendaStr) {
		this.contagerencialvendaStr = contagerencialvendaStr;
	}
	public void setCentrocustovendaStr(String centrocustovendaStr) {
		this.centrocustovendaStr = centrocustovendaStr;
	}
	public void setValorcustoStr(String valorcustoStr) {
		this.valorcustoStr = valorcustoStr;
	}
	public void setValorvendaStr(String valorvendaStr) {
		this.valorvendaStr = valorvendaStr;
	}
	public void setNcm(String ncm) {
		this.ncm = ncm;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setContagerencialcompra(Contagerencial contagerencialcompra) {
		this.contagerencialcompra = contagerencialcompra;
	}
	public void setContagerencialvenda(Contagerencial contagerencialvenda) {
		this.contagerencialvenda = contagerencialvenda;
	}
	public void setCentrocustovenda(Centrocusto centrocustovenda) {
		this.centrocustovenda = centrocustovenda;
	}
	public void setValorcusto(Double valorcusto) {
		this.valorcusto = valorcusto;
	}
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	
}
