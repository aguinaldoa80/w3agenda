package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import br.com.linkcom.sined.geral.bean.Arquivo;

public class ImportacaoNfeTDriveFiltro {
	
	private String cnpj;
	private Arquivo arquivo;
	public String getCnpj() {
		return cnpj;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	
}
