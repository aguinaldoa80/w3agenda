package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Password;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AlterasenhaUsuarioFiltro extends FiltroListagemSined {
	protected Usuario usuario;
	protected Boolean admin; 
	protected String oldsenha;
	protected String newsenha;
	protected String confirmasenha;
	
	
	@Required
	@DisplayName("Usu�rio")
	public Usuario getUsuario() {
		return usuario;
	}

	public Boolean getAdmin() {
		return admin;
	}
	@Transient
	@Password
	@Required
	@DisplayName("Confirme a senha")
	@MinLength(4)
	public String getConfirmasenha() {
		return confirmasenha;
	}

	@Transient
	@Password
	@Required
	@DisplayName("Digite a sua senha")
	@MinLength(4)
	public String getOldsenha() {
		return oldsenha;
	}

	@Transient
	@Password
	@Required
	@MinLength(4)
	@DisplayName("Nova senha")
	public String getNewsenha() {
		return newsenha;
	}

	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public void setOldsenha(String oldsenha) {
		this.oldsenha = oldsenha;
	}

	public void setNewsenha(String newsenha) {
		this.newsenha = newsenha;
	}

	public void setConfirmasenha(String confirmasenha) {
		this.confirmasenha = confirmasenha;
	}

		
}
