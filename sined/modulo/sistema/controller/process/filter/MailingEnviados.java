package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.sql.Timestamp;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;

public class MailingEnviados implements Comparable<MailingEnviados> {

	private String nome;
	private String nomecontato;
	private String email;
	private Timestamp dtconfirmacao;
	private Integer cdenvioemailitem;
	private Timestamp ultimaverificacao;
	private EmailStatusEnum situacaoemail;

	public MailingEnviados(Envioemailitem envioemailitem) {
		
		this.nome = envioemailitem.getPessoa() != null ? envioemailitem.getPessoa().getNome() : null;
		this.nomecontato = envioemailitem.getNomecontato();
		this.email = envioemailitem.getEmail();
		this.dtconfirmacao = envioemailitem.getDtconfirmacao();
		this.cdenvioemailitem = envioemailitem.getCdenvioemailitem();
		this.situacaoemail = envioemailitem.getSituacaoemail();
		this.ultimaverificacao = envioemailitem.getUltimaverificacao();
	}

	public String getNome() {
		return nome;
	}

	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}

	@DisplayName("Data de leitura do e-mail")
	public Timestamp getDtconfirmacao() {
		return dtconfirmacao;
	}

	@DisplayName("Contato")
	public String getNomecontato() {
		return nomecontato;
	}

	public int compareTo(MailingEnviados o) {
		try {
			return this.getNome().compareTo(o.getNome());
		} catch (Exception e) {
			return 1;
		}
	}
	
	public Integer getCdenvioemailitem() {
		return cdenvioemailitem;
	}
	
	public void setCdenvioemailitem(Integer cdenvioemailitem) {
		this.cdenvioemailitem = cdenvioemailitem;
	}
	
	@DisplayName("�ltima verifica��o")
	public Timestamp getUltimaverificacao() {
		return ultimaverificacao;
	}
	
	public void setUltimaverificacao(Timestamp ultimaverificacao) {
		this.ultimaverificacao = ultimaverificacao;
	}
	
	@DisplayName("Situa��o do e-mail")
	public EmailStatusEnum getSituacaoemail() {
		return situacaoemail;
	}
	
	public void setSituacaoemail(EmailStatusEnum situacaoemail) {
		this.situacaoemail = situacaoemail;
	}
}