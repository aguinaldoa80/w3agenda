package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ImportacaoTabelaimpostoFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private Arquivo arquivo;

	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
		
}
