package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Projeto;

public class AjaxRateioFiltro {
	
	private Projeto projeto;
	private Centrocusto centrocusto;

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
}
