package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Custooperacionaltipo;

public class CustooperacionalFiltro {
	
	protected Empresa empresa;
	protected Custooperacionaltipo custooperacionaltipo = Custooperacionaltipo.CUSTO_OPERACIONAL;
	protected Date dtinicio;
	protected Date dtfim;
	protected List<Contagerencial> listaContagerencial;
	protected Money custooperacional;
	
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("C�lculo")
	public Custooperacionaltipo getCustooperacionaltipo() {
		return custooperacionaltipo;
	}
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	public List<Contagerencial> getListaContagerencial() {
		return listaContagerencial;
	}
	public Money getCustooperacional() {
		return custooperacional;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCustooperacionaltipo(Custooperacionaltipo custooperacionaltipo) {
		this.custooperacionaltipo = custooperacionaltipo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setListaContagerencial(List<Contagerencial> listaContagerencial) {
		this.listaContagerencial = listaContagerencial;
	}
	public void setCustooperacional(Money custooperacional) {
		this.custooperacional = custooperacional;
	}
}
