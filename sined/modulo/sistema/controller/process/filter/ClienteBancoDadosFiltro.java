package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ClienteBancoDadosFiltro extends FiltroListagemSined{
	
	protected Cliente cliente;
	protected Contrato contrato;
	protected Date dtvalidade;
	
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Contrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@DisplayName("Validade")
	public Date getDtvalidade() {
		return dtvalidade;
	}
	
	public void setDtvalidade(Date dtvalidade) {
		this.dtvalidade = dtvalidade;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
}
