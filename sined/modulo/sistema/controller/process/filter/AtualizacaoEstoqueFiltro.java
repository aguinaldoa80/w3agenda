package br.com.linkcom.sined.modulo.sistema.controller.process.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.enumeration.TipoArquivoAtualizacaoEstoque;
import br.com.linkcom.sined.util.SinedDateUtils;

public class AtualizacaoEstoqueFiltro {
	
	private Empresa empresa;
	private Localarmazenagem localarmazenagem;
	private Date dtestoque = SinedDateUtils.currentDate();
	private Materialgrupo materialgrupo;
	private Material material;
	private TipoArquivoAtualizacaoEstoque tipoArquivoAtualizacaoEstoque;
	private Arquivo arquivo;
	private Boolean mostrarSomenteProdutosEncontrados;
	private Boolean zerarProdutosNaoEncontrados;
	private Boolean gerarSomenteEntrada;
	private Boolean considerarLotes;
	private Centrocusto centroCusto;
	
	@DisplayName("Arquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Data do estoque")
	public Date getDtestoque() {
		return dtestoque;
	}
	
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	@DisplayName("Tipo")
	public TipoArquivoAtualizacaoEstoque getTipoArquivoAtualizacaoEstoque() {
		return tipoArquivoAtualizacaoEstoque;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Zerar Produtos N�o Encontrados")
	public Boolean getZerarProdutosNaoEncontrados() {
		return zerarProdutosNaoEncontrados;
	}
	
	@DisplayName("Mostrar Somente Produtos Encontrados")
	public Boolean getMostrarSomenteProdutosEncontrados() {
		return mostrarSomenteProdutosEncontrados;
	}
	
	@DisplayName("Grupo de Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	
	@DisplayName("Centro de custo")
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}
	
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	public void setMostrarSomenteProdutosEncontrados(
			Boolean mostrarSomenteProdutosEncontrados) {
		this.mostrarSomenteProdutosEncontrados = mostrarSomenteProdutosEncontrados;
	}
	
	public void setZerarProdutosNaoEncontrados(
			Boolean zerarProdutosNaoEncontrados) {
		this.zerarProdutosNaoEncontrados = zerarProdutosNaoEncontrados;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setTipoArquivoAtualizacaoEstoque(
			TipoArquivoAtualizacaoEstoque tipoArquivoAtualizacaoEstoque) {
		this.tipoArquivoAtualizacaoEstoque = tipoArquivoAtualizacaoEstoque;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	public void setDtestoque(Date dtestoque) {
		this.dtestoque = dtestoque;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}

	@DisplayName("Realizar apenas entrada de estoque")
	public Boolean getGerarSomenteEntrada() {
		return gerarSomenteEntrada;
	}
	public void setGerarSomenteEntrada(Boolean gerarSomenteEntrada) {
		this.gerarSomenteEntrada = gerarSomenteEntrada;
	}
	
	@DisplayName("Considerar lote(s)")
	public Boolean getConsiderarLotes() {
		return considerarLotes;
	}
	
	public void setConsiderarLotes(Boolean considerarLotes) {
		this.considerarLotes = considerarLotes;
	}
}


