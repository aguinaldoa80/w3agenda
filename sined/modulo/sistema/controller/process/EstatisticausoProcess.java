package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.OportunidadeService;
import br.com.linkcom.sined.geral.service.OrdemcompraService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.RequisicaoService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.Estatisticauso;

@Controller(path="/sistema/process/Estatisticauso", authorizationModule=ProcessAuthorizationModule.class)
public class EstatisticausoProcess extends MultiActionController {
	
	private OrdemcompraService ordemcompraService;
	private RequisicaoService requisicaoService;
	private OportunidadeService oportunidadeService;
	private DocumentoService documentoService;
	private AgendamentoService agendamentoService;
	private MovimentacaoService movimentacaoService;
	private ContratoService contratoService;
	private VendaService vendaService;
	private PedidovendaService pedidovendaService;
	
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setAgendamentoService(AgendamentoService agendamentoService) {this.agendamentoService = agendamentoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setRequisicaoService(RequisicaoService requisicaoService) {this.requisicaoService = requisicaoService;}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {this.ordemcompraService = ordemcompraService;}
	
	public ModelAndView estatisticaSuprimentos(WebRequestContext request){
		Integer ordemcompra_aguardandoautorizacao = ordemcompraService.getQtdeEmAberto();
		Integer ordemcompra_autorizadas = ordemcompraService.getQtdeAutorizadas();
		Integer ordemcompra_aguardandorecebimento = ordemcompraService.getQtdePedidoEnviado();
		
		Estatisticauso estatisticauso = new Estatisticauso();
		estatisticauso.setOrdemcompra_aguardandoautorizacao(ordemcompra_aguardandoautorizacao);
		estatisticauso.setOrdemcompra_aguardandorecebimento(ordemcompra_aguardandorecebimento);
		estatisticauso.setOrdemcompra_autorizadas(ordemcompra_autorizadas);
		
		return new JsonModelAndView().addObject("estatistica", estatisticauso);
	}
	
	public ModelAndView estatisticaServicos(WebRequestContext request){
		Integer requisicao_abertas = requisicaoService.getQtdeEmEspera();
		Integer requisicao_atrasadas = requisicaoService.getQtdeAtrasadas();
		
		Estatisticauso estatisticauso = new Estatisticauso();
		estatisticauso.setRequisicao_abertas(requisicao_abertas);
		estatisticauso.setRequisicao_atrasadas(requisicao_atrasadas);
		
		return new JsonModelAndView().addObject("estatistica", estatisticauso);
	}
	
	public ModelAndView estatisticaFinanceiro(WebRequestContext request){
		Integer agendamento_aconsolidar = agendamentoService.getQtdeAConsolidar();
		Integer agendamento_atrasadas = agendamentoService.getQtdeAtrasados();
		Integer contapagar_abertas = documentoService.getQtdeAbertas(Documentoclasse.OBJ_PAGAR);
		Integer contapagar_atrasadas = documentoService.getQtdeAtrasadas(Documentoclasse.OBJ_PAGAR);
		Integer contareceber_abertas = documentoService.getQtdeAbertas(Documentoclasse.OBJ_RECEBER);;
		Integer contareceber_atrasadas = documentoService.getQtdeAtrasadas(Documentoclasse.OBJ_RECEBER);;
		Integer movimentacao_naoconciliadas = movimentacaoService.getQtdeTotalNaoConciliados();	
		
		Estatisticauso estatisticauso = new Estatisticauso();
		estatisticauso.setAgendamento_aconsolidar(agendamento_aconsolidar);
		estatisticauso.setAgendamento_atrasadas(agendamento_atrasadas);
		estatisticauso.setContapagar_abertas(contapagar_abertas);
		estatisticauso.setContapagar_atrasadas(contapagar_atrasadas);
		estatisticauso.setContareceber_abertas(contareceber_abertas);
		estatisticauso.setContareceber_atrasadas(contareceber_atrasadas);
		estatisticauso.setMovimentacao_naoconciliadas(movimentacao_naoconciliadas);
		
		return new JsonModelAndView().addObject("estatistica", estatisticauso);
	}
	
	public ModelAndView estatisticaFaturamento(WebRequestContext request){
		Integer contrato_afaturar = contratoService.getQtdeAFaturar();
		Integer contrato_atrasados = contratoService.getQtdeAtrasada();
		Integer venda_pedidospendentes = pedidovendaService.getQtdePrevistos();
		Integer venda_total = vendaService.getQtdeTotalVenda();
		
		Estatisticauso estatisticauso = new Estatisticauso();
		estatisticauso.setContrato_afaturar(contrato_afaturar);
		estatisticauso.setContrato_atrasados(contrato_atrasados);
		estatisticauso.setVenda_pedidospendentes(venda_pedidospendentes);
		estatisticauso.setVenda_total(venda_total);
		
		return new JsonModelAndView().addObject("estatistica", estatisticauso);
	}
	
	public ModelAndView estatisticaCrm(WebRequestContext request){
		Integer oportunidade_retornoatrasado = oportunidadeService.getQtdeRetornoAtrasado();
		Integer oportunidade_retornohoje = oportunidadeService.getQtdeRetornoHoje();
		
		Estatisticauso estatisticauso = new Estatisticauso();
		estatisticauso.setOportunidade_retornoatrasado(oportunidade_retornoatrasado);
		estatisticauso.setOportunidade_retornohoje(oportunidade_retornohoje);
		
		return new JsonModelAndView().addObject("estatistica", estatisticauso);
	}
}
