package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.TransferenciaCategoriaBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.TransferenciaCategoriaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;

@Controller(path="/sistema/process/TransferenciaCategoria", authorizationModule=ProcessAuthorizationModule.class)
public class TransferenciaCategoriaProcess extends MultiActionController{
	
	protected LocalarmazenagemService localarmazenagemService;
	protected MaterialService materialService;
	protected MovimentacaoestoqueService movimentacaoestoqueService;
	protected EmpresaService empresaService;
	
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, TransferenciaCategoriaFiltro filtro){
		filtro.setDataestoque(SinedDateUtils.currentDate());
		return new ModelAndView("process/transferenciaCategoriaMaterial", "filtro", filtro);
	}
	
	@Input("index")
	public ModelAndView buscarEstoque(WebRequestContext request, TransferenciaCategoriaFiltro filtro){
		TransferenciaCategoriaBean bean = new TransferenciaCategoriaBean();
		
		if(filtro.getEmpresa() != null){
			bean.setEmpresa(empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia"));
		}
		if(filtro.getLocalarmazenagem() != null){
			bean.setLocalarmazenagem(localarmazenagemService.load(filtro.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
		}
		bean.setDataestoque(filtro.getDataestoque());
		bean.setListaMateriais(materialService.findForTransferenciaCategoria(filtro, null, null));
		
		return new ModelAndView("process/transferenciaCategoriaMaterialListagem", "bean", bean);
	}
	
	public void saveTransferenciaCategoria(WebRequestContext request, TransferenciaCategoriaBean bean){
		request.clearMessages();
		movimentacaoestoqueService.validaTransferenciaCategoria(request, bean);
		String mensagemErro = "";
		Boolean transferenciaComErro = Boolean.FALSE;
		for(Message msg: request.getMessages()){
			if(msg.getType().equals(MessageType.ERROR)){
				transferenciaComErro = Boolean.TRUE;
				mensagemErro += msg.getSource().toString()+"\\n";
			}
		}
		if(!Boolean.TRUE.equals(transferenciaComErro)){
			movimentacaoestoqueService.transferenciaCategoria(bean);
		}
		View view = View.getCurrent();
		view.println("var ocorreuErro = " + transferenciaComErro + ";");
		view.println("var mensagemErro = '"+mensagemErro+"';");
	}
	
	public ModelAndView finalizacaoTransferenciaCategoria(WebRequestContext request){
		request.addMessage("Atualização realizada com sucesso.");
		return sendRedirectToAction("index");
	}
}
