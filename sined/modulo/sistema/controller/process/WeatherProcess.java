package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.utils.StringUtils;

@Controller(path="/sistema/process/Weather", authorizationModule=ProcessAuthorizationModule.class)
public class WeatherProcess extends MultiActionController{

	public ModelAndView ajaxPesquisaClima(WebRequestContext request){
		String nomeCidade = request.getParameter("nomecidade");
		String estado = request.getParameter("estado");
		nomeCidade = nomeCidade.trim().replace(" ", "%20")+"%20"+StringUtils.limpaNull(estado);
		
		DefaultHttpClient httpCliente = new DefaultHttpClient();
		HttpGet get2 = new HttpGet("http://api.hgbrasil.com/weather/?format=json&city_name="+nomeCidade+"&key=52fb5873");
		StringBuilder responseStrBuilder = new StringBuilder();
		try {
			HttpResponse resposta1 = httpCliente.execute(get2);
			InputStream inputStream1 = resposta1.getEntity().getContent();
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream1, "UTF-8"));
			
		    String inputStr;
		    while ((inputStr = streamReader.readLine()) != null)
		    	responseStrBuilder.append(inputStr);
		} catch (ClientProtocolException e1) {
			// Erro ao tentar conex�o com HgBrasil
			e1.printStackTrace();
		} catch (IOException e1) {
			// Erro ao tentar conex�o com HgBrasil
			e1.printStackTrace();
		}
		
        ModelAndView retorno = new JsonModelAndView()
        					.addObject("jsonTempo", responseStrBuilder.toString());
		return retorno;
	}
	
    public static void getPage(URL url, File file) throws IOException {
        BufferedReader in =
                new BufferedReader(new InputStreamReader(url.openStream()));

        BufferedWriter out = new BufferedWriter(new FileWriter(file));

        String inputLine;

        while ((inputLine = in.readLine()) != null) {
		
            // Imprime p&aacute;gina no console
            System.out.println(inputLine);
			
            // Grava pagina no arquivo
            out.write(inputLine);
            out.newLine();
        }

        in.close();
        out.flush();
        out.close();
    }
}
