package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.text.SimpleDateFormat;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Custooperacionaltipo;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.TabelavalorService;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.CustooperacionalFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/sistema/process/Custooperacional",
		authorizationModule=ProcessAuthorizationModule.class
)
public class CustooperacionalProcess extends MultiActionController {
	
	private ParametrogeralService parametrogeralService;
	private ContagerencialService contagerencialService;
	private MovimentacaoService movimentacaoService;
	private MaterialService materialService;
	private TabelavalorService tabelavalorService;
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setTabelavalorService(TabelavalorService tabelavalorService) {
		this.tabelavalorService = tabelavalorService;
	}
	
	@DefaultAction
	public ModelAndView carregar(WebRequestContext request, CustooperacionalFiltro filtro){
		request.setAttribute("listaContagerencial", contagerencialService.findByTipooperacaoNatureza(Tipooperacao.TIPO_DEBITO, null, NaturezaContagerencial.CONTA_GERENCIAL.getValue().toString(), null));
		request.setAttribute("contagerencialOperacional", CollectionsUtil.listAndConcatenate(contagerencialService.getContagerencialUsadaCustoOperacional(), "cdcontagerencial", ","));
		request.setAttribute("contagerencialComercial", CollectionsUtil.listAndConcatenate(contagerencialService.getContagerencialUsadaCustoComercial(), "cdcontagerencial", ","));
		
		return new ModelAndView("process/custooperacional", "filtro", filtro);
	}
	
	
	public ModelAndView calcularCusto(WebRequestContext request, CustooperacionalFiltro filtro) throws Exception {
		JsonModelAndView json = new JsonModelAndView();
		Money custo = new Money();
		if(Custooperacionaltipo.CUSTO_OPERACIONAL.equals(filtro.getCustooperacionaltipo())){
			custo = movimentacaoService.calcularCustooperacional(filtro);
		}else if(Custooperacionaltipo.CUSTO_COMERCIAL.equals(filtro.getCustooperacionaltipo())){
			custo = movimentacaoService.calcularCustocomercial(filtro);
		}
		json.addObject("custo", custo);
		return json;
	}
	
	public ModelAndView saveCalcularCustooperacional(WebRequestContext request, CustooperacionalFiltro filtro) throws Exception {
		if(filtro.getCustooperacionaltipo() == null){
			request.addError("Tipo de c�lculo inv�lido.");
		}else if(filtro.getDtinicio() == null || filtro.getDtfim() == null){
			request.addError("Per�odo inv�lido.");
		}else if(filtro.getEmpresa() == null){
			request.addError("O campo Empresa � obrigat�rio.");
		}else {
			if(Custooperacionaltipo.CUSTO_OPERACIONAL.equals(filtro.getCustooperacionaltipo())){
				Money custooperacional = movimentacaoService.calcularCustooperacional(filtro);
				if(custooperacional == null){
					request.addError("Custo Operacional n�o pode ser nulo");
				}else  {
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					tabelavalorService.updateValorPorIdentificador(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO, SinedUtil.round(custooperacional.getValue().doubleValue(), 2));
					parametrogeralService.updateValorPorNome(Parametrogeral.DATAINICIO_CUSTO_OPERACIONAL_ADMINISTRATIVO,  format.format(filtro.getDtinicio()));
					parametrogeralService.updateValorPorNome(Parametrogeral.DATAFIM_CUSTO_OPERACIONAL_ADMINISTRATIVO, format.format(filtro.getDtfim()));
					contagerencialService.updateUsadocalculocustooperacional(filtro.getListaContagerencial() != null ?
							SinedUtil.listAndConcatenate(filtro.getListaContagerencial(), "cdcontagerencial", ",") : null);
					
					materialService.atualizarValorcusto(request, Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO);
					
					request.addMessage("Custo operacional salvo com sucesso. R$ " + custooperacional.toString());
				}
			}else if(Custooperacionaltipo.CUSTO_COMERCIAL.equals(filtro.getCustooperacionaltipo())){
				Money custocomercial = movimentacaoService.calcularCustocomercial(filtro);
				if(custocomercial == null){
					request.addError("Custo Comercial n�o pode ser nulo");
				}else  {
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					tabelavalorService.updateValorPorIdentificador(Tabelavalor.CUSTO_COMERCIAL_ADMINISTRATIVO, SinedUtil.round(custocomercial.getValue().doubleValue(), 2));
					parametrogeralService.updateValorPorNome(Parametrogeral.DATAINICIO_CUSTO_COMERCIAL_ADMINISTRATIVO,  format.format(filtro.getDtinicio()));
					parametrogeralService.updateValorPorNome(Parametrogeral.DATAFIM_CUSTO_COMERCIAL_ADMINISTRATIVO, format.format(filtro.getDtfim()));
					contagerencialService.updateUsadocalculocustocomercial(filtro.getListaContagerencial() != null ?
							SinedUtil.listAndConcatenate(filtro.getListaContagerencial(), "cdcontagerencial", ",") : null);
					
					materialService.atualizarValorcusto(request, Tabelavalor.CUSTO_COMERCIAL_ADMINISTRATIVO);
					
					request.addMessage("Custo comercial salvo com sucesso. " + custocomercial.toString() + "%");
				}
			}
		}
		return continueOnAction("carregar", filtro); 
	}
}
