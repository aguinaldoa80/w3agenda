package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailitemhistorico;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.geral.service.EnvioemailService;
import br.com.linkcom.sined.geral.service.EnvioemailitemService;
import br.com.linkcom.sined.geral.service.EnvioemailitemhistoricoService;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.MailingEnviados;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.MailingEnviadosFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/sistema/process/MailingEnviados", authorizationModule=ProcessAuthorizationModule.class)
public class MailingEnviadosProcess extends MultiActionController{
	
	private EnvioemailService envioemailService;
	private EnvioemailitemService envioemailitemService;
	private EnvioemailitemhistoricoService envioemailitemhistoricoService;
	private static List<EmailStatusEnum> listaEmailstatus = Arrays.asList(new EmailStatusEnum[]{EmailStatusEnum.CAIXA_DE_SPAM, EmailStatusEnum.EMAIL_INVALIDO,
																								EmailStatusEnum.ENTREGUE, EmailStatusEnum.ENVIADO,
																								EmailStatusEnum.LIDO}); 
		
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setEnvioemailitemService(EnvioemailitemService envioemailitemService) {
		this.envioemailitemService = envioemailitemService;
	}
	public void setEnvioemailitemhistoricoService(
			EnvioemailitemhistoricoService envioemailitemhistoricoService) {
		this.envioemailitemhistoricoService = envioemailitemhistoricoService;
	}
	
	/**
	 * P�gina default do processo
	 * 
	 * @param request
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, MailingEnviadosFiltro filtro){
		Object _filtro = request.getSession().getAttribute("MailingEnviadosFilter");
		request.setAttribute("listaEmailstatus", listaEmailstatus);
		if(_filtro != null){
			filtro = (MailingEnviadosFiltro) _filtro;
			return filtrar(request, filtro);
		}else{
			return new ModelAndView("process/mailingenviados", "filtro", filtro);
		}
	}	
	
	/**
	 * M�todo que busca os e-mails enviados de acordo com o filtro selecionado
	 * 
	 * @see EnvioemailService#findForMailingEnviados(MailingEnviadosFiltro) 
	 * @param request
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView filtrar(WebRequestContext request, MailingEnviadosFiltro filtro){
		if(request.getParameter("envioemailnota") != null && !"".equals(request.getParameter("envioemailnota")) && 
				request.getParameter("envioemailtiponota") != null && !"".equals(request.getParameter("envioemailtiponota"))){
			filtro.setEnvioemail(new Envioemail(Integer.parseInt(request.getParameter("envioemailnota"))));
			filtro.setEnvioemailtipo(new Envioemailtipo(Integer.parseInt(request.getParameter("envioemailtiponota")),""));
		}
		request.setAttribute("listaEnvioemail", envioemailService.findForMailingEnviados(filtro));
		if(request.getParameter("envioemailnota") != null && !"".equals(request.getParameter("envioemailnota")) && 
				request.getParameter("envioemailtiponota") != null && !"".equals(request.getParameter("envioemailtiponota"))){
			filtro.setEnvioemail(null);
		}
		request.setAttribute("listaEmailstatus", listaEmailstatus);
		request.getSession().setAttribute("MailingEnviadosFilter", filtro);
		return new ModelAndView("process/mailingenviados", "filtro", filtro);
	}	
	
	/**
	 * M�todo que representa o popup dos detalhes do mailing enviado
	 * 
	 * @see EnvioemailService#load(Envioemail)
	 * @see EnvioemailitemService#find(Envioemail)
	 * @see #getListaMailingEnviados(List)
	 * @param request
	 * @author Thiago Clemente
	 * 
	 */
	public ModelAndView popupDetalhes(WebRequestContext request){
		
		Envioemail envioemail = envioemailService.load(new Envioemail(Integer.valueOf(request.getParameter("cdenvioemail"))));
		List<Envioemailitem> listaEnvioemailitem = envioemailitemService.find(envioemail);
		List<MailingEnviados> listaMailingEnviados = getListaMailingEnviados(listaEnvioemailitem);
		
		String mensagem = ajustarMensagemParaExibirImagem(envioemail.getMensagem());
		request.setAttribute("mensagem", mensagem);
		
		return new ModelAndView("process/popup/popupmailingenviados")
					.addObject("mensagem", mensagem)	
					.addObject("envioemail", envioemail)
					.addObject("listaMailingEnviados", listaMailingEnviados);					
	}
	
	/**
	* M�todo para ajustar corpo do email para exibir a imagem
	*
	* @param mensagem
	* @return
	* @since 31/10/2016
	* @author Luiz Fernando
	*/
	private String ajustarMensagemParaExibirImagem(String mensagem) {
		String regexSrcCid = "(src='cid:)";
		try {
			Pattern pattern_src = Pattern.compile(regexSrcCid + "[0-9]*");
			Matcher m = pattern_src.matcher(mensagem);
			int i = 0;
			while(m.find()){
				DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), Long.parseLong(m.group(i).replaceAll(SinedUtil.REGEX_APENAS_NUMEROS_SEFIP, "")));
				i++;
			}
		} catch (Exception e) {}
		return StringUtils.isNotBlank(mensagem) ? mensagem.replaceAll(regexSrcCid, "src='/" +  SinedUtil.getContexto() + DownloadFileServlet.DOWNLOAD_FILE_PATH + "/") : mensagem;
	}
	
	/**
	 * M�todo que retorna uma lista da classe que representa os destinat�rios do mailing enviado  
	 * 
	 * @see MailingEnviados
	 * @param listaEnvioemailitem
	 * @author Thiago Clemente
	 * 
	 */
	private List<MailingEnviados> getListaMailingEnviados(List<Envioemailitem> listaEnvioemailitem){
		
		List<MailingEnviados> listaMailingEnviados = new ArrayList<MailingEnviados>();
		
		for (Envioemailitem envioemailitem: listaEnvioemailitem){
			listaMailingEnviados.add(new MailingEnviados(envioemailitem));
		}
		
		Collections.sort(listaMailingEnviados);
		
		return listaMailingEnviados;
	}

	@Action("removertodos")
	public ModelAndView removertodos(WebRequestContext request, MailingEnviadosFiltro filtro){
		Object _filtro = request.getSession().getAttribute("MailingEnviadosFilter");
		if(_filtro != null){
			filtro = (MailingEnviadosFiltro) _filtro;
		}		
		String email = request.getParameter("idenvioemail");
		List<Integer> listaids = new ArrayList<Integer>();
		String[] ids = email.split(",");
		Integer id = null;
		
		for (int i = 0; i < ids.length; i++) {			
			id = Integer.parseInt(ids[i]);
			listaids.add(id);
		} 
		
		List<Envioemail> listaemail = envioemailService.findForMailingEnviados(filtro);
		List<Envioemail> listaemailnova = new ArrayList<Envioemail>();
		
		for (Envioemail envioemail : listaemail) {
			envioemail.setEnvioemailtipo(filtro.getEnvioemailtipo());
			for (Integer idemail : listaids) {
				if(!envioemail.getCdenvioemail().equals(idemail)){
					listaemailnova.add(envioemail);
				}else{
					for (Envioemailitem envioemailitem : envioemail.getListaEnvioemailitem()) {					
						envioemailitemService.delete(envioemailitem);
					}
					envioemailService.delete(envioemail);
				}
			}
		}
		
        return null;		
	}
	
	public ModelAndView abrePopupMailEvents(WebRequestContext request, Envioemailitem envioemailitem){
		List<Envioemailitemhistorico> listaHistorico = envioemailitemhistoricoService.findByEnvioemailitem(envioemailitem);
		envioemailitem.setListaHistorico(listaHistorico);
		return new ModelAndView("direct:process/popup/popupmailevents", "bean", envioemailitem);
	}
}