package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.util.ArrayList;
import java.util.List;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.AlterasenhaUsuarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(
		path="/sistema/process/alterasenhausuario",
		authorizationModule=ProcessAuthorizationModule.class
)
public class AlteraSenhaUsuarioProcess extends MultiActionController {
	
	private UsuarioService usuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	/**
	 * M�todo para verificar se o usuario logado tem papel de administrador, 
	 * carrega uma listagem usuarios que ser� usada no jsp.
	 * 
	 * @see br.com.linkcom.sined.geral.service.UsuarioService#findAll(usuario.nome)
	 * @param request
	 * @param filtro
	 * @return ModelAndView
	 * @author Ramon Brazil
	 */
	@DefaultAction
	public ModelAndView buscar(WebRequestContext request, AlterasenhaUsuarioFiltro filtro) throws Exception {
		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		if (SinedUtil.isUsuarioLogadoAdministrador()) {
			listaUsuario = usuarioService.findAll("usuario.nome");
		} else {
			listaUsuario.add(SinedUtil.getUsuarioLogado());
		}
		request.setAttribute("listaUsuario", listaUsuario);
		return new ModelAndView("process/alterasenhausuario", "filtro", filtro);
	}

	
		/**
	 * M�todo para verificar se a senha atual � valida, se a nova senha tem 6 posi��es
	 * e se a nova senha e a confirma senha s�o iguais. 
	 *  Se as condi��es forem verdadeiras a senha ser� alterada.  
	 * 
	 * @see br.com.linkcom.sined.geral.service.UsuarioService#carregasenha(usuarioLogado)
	 * @see br.com.linkcom.sined.geral.service.UsuarioService#alterarSenhaUsuario(usuario, true)
	 * @param request
	 * @param filtro
	 * @throws Exception
	 * @return ModelAndView
	 * @author Ramon Brazil
	 */
	@OnErrors("buscar")
	public ModelAndView alterarSenha(WebRequestContext request, AlterasenhaUsuarioFiltro filtro) throws Exception {
		Usuario usuario = filtro.getUsuario();
		String oldsenha = filtro.getOldsenha();
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		usuarioLogado.setSenha(usuarioService.carregarSenha(usuarioLogado));
		
		//Verifica se a senha do usuario logado � valida 
		if (!verifyPassword(usuarioLogado, oldsenha)) {
			request.setAttribute("OK", false);
			throw new SinedException("Sua senha n�o confere. Favor, informe senha correta.");
		}
		
		//Verifica se a nova senha tem 6 posi��es 
		if ((filtro.getNewsenha()).length() < 4) {
			throw new SinedException("A senha deve conter, no m�nimo, 4 caracteres.");
	    }
		
		//Verifica se a nova senha e a confirma senha s�o iguais 
		if (!(filtro.getNewsenha().equals(filtro.getConfirmasenha()))) {
			throw new SinedException("A nova senha e a confirma��o devem ser exatamente iguais.");
		} else {
			usuario.setSenha(filtro.getNewsenha());
			usuarioService.alterarSenhaUsuario(usuario, true);
			request.addMessage("Senha alterada com sucesso.");
			return sendRedirectToAction("buscar").addObject("filtro", filtro);
		}
	}	
	
	private boolean verifyPassword(User user, String password) {
    	boolean passwordMatch = false;
		
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		
		// se o usu�rio existe e a senha foi passada
		if (user != null && user.getPassword() != null) {
			if(user.getPassword().matches("[a-zA-Z0-9\\+/]{64}")) {
				// Jasypt Strong Encryption
				if (passwordEncryptor.checkPassword(password, user.getPassword())) {
					passwordMatch = true;
				}
			}
			else if(user.getPassword().matches("[0-9a-f]{32}")) {
				// MD5 simples
				if (Util.crypto.makeHashMd5(password).equals(user.getPassword())) {
					passwordMatch = true;
				}
			}
			else {
				// Senha pura
				if (user.getPassword().equals(password)) {
					passwordMatch = true;
				}
			}
		}
			
		return passwordMatch;
	}
}
