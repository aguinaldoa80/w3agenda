package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;


@Controller(path="/sistema/process/LocalarmazenagemBean")
public class LocalarmazenagemBeanProcess extends MultiActionController {
	
	private LocalarmazenagemService localarmazenagemService;
	
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}

	public ModelAndView ajaxPermissaoestoquenegativo(WebRequestContext request, Localarmazenagem localarmazenagem){
		return new JsonModelAndView().addObject("permissaoestoquenegativo", localarmazenagemService.getPermitirestoquenegativo(localarmazenagem));
	}
}
