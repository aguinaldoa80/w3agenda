package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumreducaoz;
import br.com.linkcom.sined.geral.bean.Emporiumtotalizadores;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.service.EmporiumpdvService;
import br.com.linkcom.sined.geral.service.EmporiumreducaozService;
import br.com.linkcom.sined.geral.service.EmporiumtotalizadoresService;
import br.com.linkcom.sined.geral.service.EmporiumvendaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.TributacaoecfService;
import br.com.linkcom.sined.geral.service.VendaService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendaSomatorioDiaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.VendaFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.EmporiumAcertoAcoesLegendaErradaBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.EmporiumAcertoPainelBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.EmporiumAcertoExclusaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumExportacaoMovimentosUtil;

import com.ibm.icu.util.Calendar;

@Controller(path="/sistema/process/EmporiumAcerto", authorizationModule=ProcessAuthorizationModule.class)
public class EmporiumAcertoProcess extends MultiActionController {
	
	private EmporiumvendaService emporiumvendaService;
	private EmporiumreducaozService emporiumreducaozService;
	private EmporiumpdvService emporiumpdvService;
	private EmporiumtotalizadoresService emporiumtotalizadoresService;
	private EmpresaService empresaService;
	private VendaService vendaService;
	private TributacaoecfService tributacaoecfService;
	
	public void setTributacaoecfService(
			TributacaoecfService tributacaoecfService) {
		this.tributacaoecfService = tributacaoecfService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setEmporiumtotalizadoresService(
			EmporiumtotalizadoresService emporiumtotalizadoresService) {
		this.emporiumtotalizadoresService = emporiumtotalizadoresService;
	}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {
		this.emporiumpdvService = emporiumpdvService;
	}
	public void setEmporiumreducaozService(
			EmporiumreducaozService emporiumreducaozService) {
		this.emporiumreducaozService = emporiumreducaozService;
	}
	public void setEmporiumvendaService(
			EmporiumvendaService emporiumvendaService) {
		this.emporiumvendaService = emporiumvendaService;
	}
	
	@SuppressWarnings("unchecked")
	@DefaultAction
	public ModelAndView index(WebRequestContext request, EmporiumAcertoPainelBean bean){
		List<Emporiumreducaoz> listaReducaoz = emporiumreducaozService.findForAcerto(bean.getDtinicio(), bean.getDtfim());
		List<Emporiumtotalizadores> listaEmporiumtotalizadores = emporiumtotalizadoresService.findForAcerto(bean.getDtinicio(), bean.getDtfim());
		List<Emporiumvenda> listaEmporiumvenda = emporiumvendaService.findForAcerto(bean.getDtinicio(), bean.getDtfim());
		List<Emporiumvendaitem> listaEmporiumvendaitem = emporiumvendaService.getEmporiumvendaitemForSpedAcerto(listaEmporiumvenda);
		List<Emporiumpdv> listaEmporiumpdv = emporiumpdvService.findForAcerto();
		List<Tributacaoecf> listaTributacaoecf = tributacaoecfService.findAll();
		
		List<Emporiumreducaoz> listaReducaozErrada = new ArrayList<Emporiumreducaoz>();
		List<Emporiumreducaoz> listaReducaozNaoBateVenda = new ArrayList<Emporiumreducaoz>();
		List<Emporiumreducaoz> listaReducaozNaoBateTotalizadores = new ArrayList<Emporiumreducaoz>();
		List<Emporiumtotalizadores> listaEmporiumtotalizadoresCadastradaErrada = new ArrayList<Emporiumtotalizadores>();
		List<Emporiumtotalizadores> listaEmporiumtotalizadoresSomatorioErrado = new ArrayList<Emporiumtotalizadores>();
		List<Emporiumvendaitem> listaEmporiumvendaitemLegendaErrada = new ArrayList<Emporiumvendaitem>();
		
		for (Emporiumpdv emporiumpdv : listaEmporiumpdv) {
			VendaFiltro filtro = new VendaFiltro();
			filtro.setEmporiumpdv(emporiumpdv);
			filtro.setDtinicio(bean.getDtinicio());
			filtro.setDtfim(bean.getDtfim());
			filtro = vendaService.calculaTotaisListagem(filtro, true);
			
			List<Emporiumreducaoz> listaReducaozPDV = emporiumreducaozService.getReducaozByPdv(listaReducaoz, emporiumpdv);
			
			boolean primeiroRegistro = true;
			Integer crzUltimo = null;
			Double gtfinalUltimo = null;
			Date dataUltimo = null;
			
			for (Emporiumreducaoz emporiumreducaoz : listaReducaozPDV) {
				boolean erro = false;
				
				if(primeiroRegistro){
					Emporiumreducaoz emporiumreducaozUltima = emporiumreducaozService.findLastReducaoz(bean.getDtinicio(), emporiumpdv);
					if(emporiumreducaozUltima != null){
						dataUltimo = emporiumreducaozUltima.getData();
						crzUltimo = emporiumreducaozUltima.getCrz();
						gtfinalUltimo = emporiumreducaozUltima.getGtfinal();
					}
				}
				
				if(crzUltimo == null || gtfinalUltimo == null){
					// ERRO N�O ENCONTRADO A ULTIMA REDU��O Z
					erro = true;
				} else {
					Integer crzAtual = emporiumreducaoz.getCrz();
					Double gtfinalAtual = emporiumreducaoz.getGtfinal();
					Double vendabrutaAtual = emporiumreducaoz.getValorvendabruta();
					
					emporiumreducaoz.setCrzUltimo(crzUltimo);
					emporiumreducaoz.setGtfinalUltimo(gtfinalUltimo);
					emporiumreducaoz.setDataUltimo(dataUltimo);
					
					if(!crzAtual.equals(crzUltimo + 1) || // ERRO NO CRZ
							!SinedUtil.round(gtfinalAtual, 2).equals(SinedUtil.round(gtfinalUltimo + vendabrutaAtual, 2))){ // ERRO NO GTFINAL
						erro = true; 
					}
					
					if(erro){
						Date data_aux = new Date(dataUltimo.getTime());
						data_aux = SinedDateUtils.incrementDate(data_aux, 1, Calendar.DAY_OF_MONTH);
						
						StringBuilder sb = new StringBuilder();
						while(!SinedDateUtils.equalsIgnoreHour(data_aux, emporiumreducaoz.getData()) &&
								SinedDateUtils.beforeIgnoreHour(data_aux, emporiumreducaoz.getData())){
							sb.append(SinedDateUtils.toString(data_aux)).append(",");
							data_aux = SinedDateUtils.incrementDate(data_aux, 1, Calendar.DAY_OF_MONTH);
						}
						if(sb.length() > 0){
							emporiumreducaoz.setWhereInDataEntre(sb.substring(0, sb.length() - 1).toString());
						}
					}
				}
				
				if(erro){
					listaReducaozErrada.add(emporiumreducaoz);
				}
				
				crzUltimo = emporiumreducaoz.getCrz();
				gtfinalUltimo = emporiumreducaoz.getGtfinal();
				dataUltimo = emporiumreducaoz.getData();
				primeiroRegistro = false;
			}
			
			
			
			for (Emporiumreducaoz emporiumreducaoz : listaReducaozPDV) {
				List<Emporiumtotalizadores> listaEmporiumtotalizadoresPDVData = emporiumtotalizadoresService.getEmporiumtotalizadoresByDataPDV(listaEmporiumtotalizadores, emporiumpdv, emporiumreducaoz.getData());
				List<Emporiumvendaitem> listaEmporiumvendaitemPDVData = emporiumvendaService.getEmporiumvendaitemByDataPDV(listaEmporiumvendaitem, emporiumpdv, emporiumreducaoz.getData());
				
				Double totalizadores = 0d;
				for (Emporiumtotalizadores emporiumtotalizadores : listaEmporiumtotalizadoresPDVData) {
					if(emporiumtotalizadores.getValorbcicms() != null){
						totalizadores += emporiumtotalizadores.getValorbcicms();
						totalizadores = SinedUtil.round(totalizadores, 2);
						
						Tributacaoecf tributacaoecf = emporiumtotalizadores.getTributacaoecf();
						if(tributacaoecf != null){
							Double somaitem = 0d;
							for (Emporiumvendaitem emporiumvendaitem : listaEmporiumvendaitemPDVData) {
								if(emporiumvendaitem.getLegenda() != null && emporiumvendaitem.getLegenda().equals(tributacaoecf.getCodigoecf())){
									somaitem += emporiumvendaitem.getValorComDesconto();
								}
							}
							
							somaitem = SinedUtil.round(somaitem, 2);
							Double valorbcicms = SinedUtil.round(emporiumtotalizadores.getValorbcicms(), 2);
							if(!somaitem.equals(valorbcicms)){
								emporiumtotalizadores.setSomaitem(somaitem);
								emporiumtotalizadores.setDiferencasomaitem(SinedUtil.round(valorbcicms - somaitem, 2));
								listaEmporiumtotalizadoresSomatorioErrado.add(emporiumtotalizadores);
							}
						}
					}
				}
				
				Double valorreducao = SinedUtil.round(emporiumreducaoz.getValorvendabruta() + 
								emporiumreducaoz.getValoracrescimo() - 
								emporiumreducaoz.getValorcancelados() - 
								emporiumreducaoz.getValordesconto(), 2);
				
				if(!totalizadores.equals(valorreducao)){
					emporiumreducaoz.setTotalizadores(totalizadores);
					emporiumreducaoz.setValorreducao(valorreducao);
					emporiumreducaoz.setDiferencatotalizadores(SinedUtil.round(totalizadores - valorreducao, 2));
					listaReducaozNaoBateTotalizadores.add(emporiumreducaoz);
				}
				
				VendaSomatorioDiaBean somatorioBean = new VendaSomatorioDiaBean(emporiumreducaoz.getData(), emporiumpdv);
				List<VendaSomatorioDiaBean> listaSomatorioDia = filtro.getListaSomatorioDia();
				
				Double valorvendaw3 = 0d;
				if(listaSomatorioDia.contains(somatorioBean)){
					somatorioBean = listaSomatorioDia.get(listaSomatorioDia.indexOf(somatorioBean));
					valorvendaw3 = somatorioBean != null && somatorioBean.getValortotal() != null ? somatorioBean.getValortotal().getValue().doubleValue() : 0d;
				}
				
				valorvendaw3 = SinedUtil.round(valorvendaw3, 2);
				if(!valorvendaw3.equals(valorreducao)){
					emporiumreducaoz.setValorvendaw3(valorvendaw3);
					emporiumreducaoz.setValorreducao(valorreducao);
					emporiumreducaoz.setDiferencavendaw3(SinedUtil.round(valorvendaw3 - valorreducao, 2));
					listaReducaozNaoBateVenda.add(emporiumreducaoz);
				}
			}
			
		}
		
		for (Emporiumtotalizadores emporiumtotalizadores : listaEmporiumtotalizadores) {
			if(emporiumtotalizadores.getValorbcicms() != null && 
					emporiumtotalizadores.getValorbcicms() > 0 &&
					emporiumtotalizadores.getTributacaoecf() == null){
				listaEmporiumtotalizadoresCadastradaErrada.add(emporiumtotalizadores);
			}
		}
		
		List<EmporiumAcertoAcoesLegendaErradaBean> listaAcoesLegendaErrada = new ArrayList<EmporiumAcertoAcoesLegendaErradaBean>();
		List<String> listaLegenda = (List<String>)CollectionsUtil.getListProperty(listaTributacaoecf, "codigoecf");
		for (Emporiumvendaitem emporiumvendaitem : listaEmporiumvendaitem) {
			if(emporiumvendaitem.getLegenda() == null ||
					emporiumvendaitem.getLegenda().equals("") ||
					!listaLegenda.contains(emporiumvendaitem.getLegenda())){
				listaEmporiumvendaitemLegendaErrada.add(emporiumvendaitem);
				
				Empresa empresa = null;
				try{
					Integer pdv = Integer.parseInt(emporiumvendaitem.getEmporiumvenda().getPdv());
					for (Emporiumpdv emporiumpdv : listaEmporiumpdv) {
						Integer codigoemporium = Integer.parseInt(emporiumpdv.getCodigoemporium());
						if(pdv.equals(codigoemporium)){
							empresa = emporiumpdv.getEmpresa();
							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if(empresa != null){
					EmporiumAcertoAcoesLegendaErradaBean emporiumAcertoAcoesLegendaErradaBean = new EmporiumAcertoAcoesLegendaErradaBean();
					emporiumAcertoAcoesLegendaErradaBean.setEmpresa(empresa);
					emporiumAcertoAcoesLegendaErradaBean.setData(new Date(emporiumvendaitem.getEmporiumvenda().getData_hora().getTime()));
					
					if(!listaAcoesLegendaErrada.contains(emporiumAcertoAcoesLegendaErradaBean)){
						listaAcoesLegendaErrada.add(emporiumAcertoAcoesLegendaErradaBean);
					}
				}
			}
		}
		
		request.setAttribute("listaReducaozErrada", listaReducaozErrada);
		request.setAttribute("listaReducaozNaoBateTotalizadores", listaReducaozNaoBateTotalizadores);
		request.setAttribute("listaReducaozNaoBateVenda", listaReducaozNaoBateVenda);
		request.setAttribute("listaEmporiumtotalizadoresCadastradaErrada", listaEmporiumtotalizadoresCadastradaErrada);
		request.setAttribute("listaEmporiumtotalizadoresSomatorioErrado", listaEmporiumtotalizadoresSomatorioErrado);
		request.setAttribute("listaEmporiumvendaitemLegendaErrada", listaEmporiumvendaitemLegendaErrada);
		request.setAttribute("listaAcoesLegendaErrada", listaAcoesLegendaErrada);
		
		return new ModelAndView("process/emporiumAcertoPainel", "bean", bean);
	}
	
	
	public ModelAndView exclusao(WebRequestContext request, EmporiumAcertoExclusaoFiltro filtro){
		return new ModelAndView("process/emporiumAcertoExclusao", "filtro", filtro);
	}
	
	public ModelAndView saveExclusao(WebRequestContext request, EmporiumAcertoExclusaoFiltro filtro){
		try{
			emporiumvendaService.makeExclusao(filtro);
			request.addMessage("Exclus�o realizada com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		return continueOnAction("exclusao", filtro);
	}
	
	public void criarTotalizadoresFaltantes(WebRequestContext request) throws IOException, ParseException {
		String data = request.getParameter("data");
		String cdemporiumpdv = request.getParameter("cdemporiumpdv");
		
		if(data == null || data.trim().equals("")){
			throw new SinedException("Data para arquivos n�o podem ser nula.");
		}
		if(cdemporiumpdv == null || cdemporiumpdv.trim().equals("")){
			throw new SinedException("PDV para arquivos n�o pode ser nulo.");
		}
		
		String stringConexaoBanco = SinedUtil.getStringConexaoBanco();
		Emporiumpdv emporiumpdv = emporiumpdvService.load(new Emporiumpdv(Integer.parseInt(cdemporiumpdv)), "emporiumpdv.cdemporiumpdv, emporiumpdv.empresa");
		
		
		IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacaoFiscaisTributacao(stringConexaoBanco, SinedDateUtils.stringToDate(data), emporiumpdv.getEmpresa());

		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.alert('Arquivo de totalizadores criado com sucesso.');window.close();</script>");
	}
	
	public void criarReducoesZFaltantes(WebRequestContext request) throws ParseException, IOException {
		String datas = request.getParameter("datas");
		String cdemporiumpdv = request.getParameter("cdemporiumpdv");
		
		if(datas == null || datas.trim().equals("")){
			throw new SinedException("Datas para arquivos n�o podem ser nulas.");
		}
		if(cdemporiumpdv == null || cdemporiumpdv.trim().equals("")){
			throw new SinedException("PDV para arquivos n�o pode ser nulo.");
		}
		
		String stringConexaoBanco = SinedUtil.getStringConexaoBanco();
		List<Date> listaData = new ArrayList<Date>();
		Emporiumpdv emporiumpdv = emporiumpdvService.load(new Emporiumpdv(Integer.parseInt(cdemporiumpdv)), "emporiumpdv.cdemporiumpdv, emporiumpdv.empresa");
		
		String[] arrayData = datas.split(",");
		for (String strData : arrayData) {
			listaData.add(SinedDateUtils.stringToDate(strData));
		}
		for (Date data : listaData) {
			IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacaoFiscaisGerais(stringConexaoBanco, data, emporiumpdv.getEmpresa());
		}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.alert('Arquivo(s) de redu��es Z criado(s) com sucesso.');window.close();</script>");
	}
	
	public void criarMovimentosDetalhados(WebRequestContext request, EmporiumAcertoAcoesLegendaErradaBean bean) throws IOException{
		String cdemporiumpdv = request.getParameter("cdemporiumpdv");
		
		Empresa empresa = bean.getEmpresa();
		if(empresa == null && cdemporiumpdv != null && !cdemporiumpdv.trim().equals("")){
			Emporiumpdv emporiumpdv = emporiumpdvService.load(new Emporiumpdv(Integer.parseInt(cdemporiumpdv)), "emporiumpdv.cdemporiumpdv, emporiumpdv.empresa");
			empresa = emporiumpdv.getEmpresa();
		}
		
		empresa = empresaService.load(empresa, "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.integracaopdv");
		String stringConexaoBanco = SinedUtil.getStringConexaoBanco();
		IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacaoMovimentos(stringConexaoBanco, bean.getData(), empresa);
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.alert('Arquivo de movimentos detalhados criado com sucesso.');window.close();</script>");
	}

}
