package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.AjaxRateioFiltro;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/sistema/process/AjaxRateio")
public class AjaxRateioProcess extends MultiActionController {
	
	private ProjetoService projetoService;
	
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	public void carregaCentrocusto(WebRequestContext request, AjaxRateioFiltro filtro){
		SinedUtil.markAsReader();
		Projeto projeto = projetoService.load(filtro.getProjeto(), "projeto.cdprojeto, projeto.centrocusto");
		if(projeto != null && projeto.getCentrocusto() != null && projeto.getCentrocusto().getCdcentrocusto() != null){
			View.getCurrent().println("var centrocusto = 'br.com.linkcom.sined.geral.bean.Centrocusto[cdcentrocusto=" + projeto.getCentrocusto().getCdcentrocusto() + "]';");
		} else {
			View.getCurrent().println("var centrocusto = null;");
		}
	}
	
	public ModelAndView carregaListaProjeto(WebRequestContext request, AjaxRateioFiltro filtro){
		
		List<Projeto> listaProjeto = projetoService.findByCentrocusto(filtro.getCentrocusto());
		
		return new JsonModelAndView().addObject("opcoesProjeto", listaProjeto);
	}
	
}
