package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;

public class NaturezaoperacaoSPEDFiscal {

	private String codigo;
	private String descricao;
	private int quantidadeRegistros;
	private Naturezaoperacao naturezaOperacaoAssociada;
	private boolean selecionado;
	private String descricaoNatOprAssociada;

	public NaturezaoperacaoSPEDFiscal(){}
	
	public NaturezaoperacaoSPEDFiscal(int quantidadeRegistros) {
		this.quantidadeRegistros = quantidadeRegistros;
	}
	
	public NaturezaoperacaoSPEDFiscal(String codigo, String descricao, int quantidadeRegistros, Naturezaoperacao naturezaOperacaoAssociada) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.quantidadeRegistros = quantidadeRegistros;
		this.naturezaOperacaoAssociada = naturezaOperacaoAssociada;
	}

	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@DisplayName("Natureza  de Opera��o")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@DisplayName("Quantidade Registros")
	public int getQuantidadeRegistros() {
		return quantidadeRegistros;
	}

	public void setQuantidadeRegistros(int quantidadeRegistros) {
		this.quantidadeRegistros = quantidadeRegistros;
	}

	@DisplayName("Natureza de Opera��o Associada")
	public Naturezaoperacao getNaturezaOperacaoAssociada() {
		return naturezaOperacaoAssociada;
	}

	public void setNaturezaOperacaoAssociada(Naturezaoperacao naturezaOperacaoAssociada) {
		this.naturezaOperacaoAssociada = naturezaOperacaoAssociada;
	}

	public boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}
	
	public String getDescricaoNatOprAssociada() {
		if (descricaoNatOprAssociada != null) {
			return descricaoNatOprAssociada;
		}
		
		if (naturezaOperacaoAssociada != null && StringUtils.isNotEmpty(naturezaOperacaoAssociada.getNome())) {
			return naturezaOperacaoAssociada.getNome();
		}
		
		return "";
	}
	
	public void setDescricaoNatOprAssociada(String descricaoNatOprAssociada) {
		this.descricaoNatOprAssociada = descricaoNatOprAssociada;
	}
}
