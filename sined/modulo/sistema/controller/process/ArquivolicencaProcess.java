package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Licenca;
import br.com.linkcom.sined.geral.service.LicencaService;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ArquivolicencaFiltro;
import br.com.linkcom.sined.util.LicenseManager;
import br.com.linkcom.sined.util.SinedException;

@Controller(path="/sistema/process/arquivolicenca", authorizationModule=ProcessAuthorizationModule.class)
public class ArquivolicencaProcess extends MultiActionController {
	
	private LicencaService licencaService;
	
	public void setLicencaService(LicencaService licencaService) {
		this.licencaService = licencaService;
	}
	
	@DefaultAction
	public ModelAndView doIndex(WebRequestContext request, ArquivolicencaFiltro filtro) {
		Object[] dadosLicenca = LicenseManager.dadosLicenca();
		if (dadosLicenca != null){
			String empresa = (String)dadosLicenca[0];
			java.util.Date validade = (java.util.Date)dadosLicenca[1];
			filtro.setCnpj(empresa);
			filtro.setValidade(new SimpleDateFormat("dd/MM/yyyy").format(validade));
		}			
		filtro.setArquivo(null);
		return new ModelAndView("process/arquivolicenca", "filtro", filtro);
	}
	
	@Input("doIndex")
	@OnErrors("doIndex")
	public ModelAndView salvar(WebRequestContext request, ArquivolicencaFiltro filtro) {
		if (filtro.getArquivo() == null || 
			filtro.getArquivo().getContent() == null || 
			filtro.getArquivo().getContent().length == 0)
			throw new SinedException("O arquivo deve ser informado.");		
						
		List<String> lista = LicenseManager.dadosArquivoLicenca(filtro.getArquivo());
		if (!LicenseManager.validaLicenca(lista))
			throw new SinedException("O arquivo enviado n�o � v�lido.");
		
		licencaService.deleteAll();
		for (String linha : lista) {
			Licenca licenca = new Licenca();
			licenca.setItem(linha);
			licencaService.saveOrUpdate(licenca);
		}		

		NeoWeb.getRequestContext().getSession().removeAttribute("W3ERP_LICENCAS");
		
		request.addMessage("O arquivo foi enviado com sucesso! Saia e entre novamente no sistema para que as licen�as funcionem corretamente.");
		return doIndex(request, filtro);
	}

}
