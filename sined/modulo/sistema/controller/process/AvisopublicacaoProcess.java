package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.AvisopublicacaoFiltro;

@Controller(path="/sistema/process/Avisopublicacao", authorizationModule=ProcessAuthorizationModule.class)
@Bean
public class AvisopublicacaoProcess extends MultiActionController {

	@DefaultAction
	public ModelAndView doFiltro(WebRequestContext request, AvisopublicacaoFiltro filtro) {
		return new ModelAndView("process/avisopublicacao", "filtro", filtro);
	}
	
	public void emitirAviso(WebRequestContext request, AvisopublicacaoFiltro filtro) {
		View.getCurrent().eval("document.getElementById('avisoRetirar').style.display = 'none';");
		View.getCurrent().eval("document.getElementById('avisoEmitir').style.display = 'block';");
		View.getCurrent().eval("document.getElementById('mensagemEmitir').innerHTML = 'Aviso emitido';");
		request.getApplicationContext().setAttribute("AVISOPUBLICACAO", Boolean.TRUE);
    }		
	
	public void retirarAviso(WebRequestContext request, AvisopublicacaoFiltro filtro) {
		View.getCurrent().eval("document.getElementById('avisoEmitir').style.display = 'none';");
		View.getCurrent().eval("document.getElementById('avisoRetirar').style.display = 'block';");
		View.getCurrent().eval("document.getElementById('mensagemRetirar').innerHTML = 'Aviso retirado';");
		request.getApplicationContext().setAttribute("AVISOPUBLICACAO", Boolean.FALSE);		
    }	
}