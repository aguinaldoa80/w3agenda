package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;

public class GrupoTributacaoSPEDFiscal {

	private boolean selecionado;
	private boolean ncmSelecionado;
	private Empresa empresa;
	private String utilizacao;
	private Cfop cfop;
	private Naturezaoperacao naturezaoperacao;
	private String icms;
	private String aliquotaicms;
	private String ipi;
	private String aliquotaipi;
	private String pis;
	private String aliquotapis;
	private String cofins;
	private String aliquotacofins;
	private int quantidadeRegistros;
	private String ncm;
	private String descricaoNatOprAssociada;
	
	private String cfopLabel;
	private String naturezaOperacaoLabel;
	
	public String key(){
		StringBuilder hash = new StringBuilder();
		hash.append(utilizacao + "-");
		hash.append((cfop != null ? (cfop.getCodigo() + " - " + cfop.getDescricaoresumida()) : ""));
		hash.append((naturezaoperacao != null ? naturezaoperacao.getNome() : null) + "-");
		hash.append(utilizacao + "-");
		hash.append(icms + "-");
		hash.append(aliquotaicms + "-");
		hash.append(ipi + "-");
		hash.append(aliquotaipi + "-");
		hash.append(pis + "-");
		hash.append(aliquotapis + "-");
		hash.append(cofins + "-");
		hash.append(aliquotacofins + "-");
		return hash.toString();
	}
	
	public GrupoTributacaoSPEDFiscal() {
		this.ncm = "";
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@DisplayName("Utiliza��o")
	public String getUtilizacao() {
		return utilizacao;
	}

	public void setUtilizacao(String utilizacao) {
		this.utilizacao = utilizacao;
	}

	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}

	@DisplayName("Natureza de Opera��o")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	public String getIcms() {
		return icms;
	}

	public void setIcms(String icms) {
		this.icms = icms;
	}

	@DisplayName("Al�quota ICMS")
	public String getAliquotaicms() {
		return aliquotaicms;
	}

	public void setAliquotaicms(String aliquotaicms) {
		this.aliquotaicms = aliquotaicms;
	}

	public String getIpi() {
		return ipi;
	}

	public void setIpi(String ipi) {
		this.ipi = ipi;
	}

	public String getPis() {
		return pis;
	}

	public void setPis(String pis) {
		this.pis = pis;
	}

	@DisplayName("Al�quota PIS")
	public String getAliquotapis() {
		return aliquotapis;
	}

	public void setAliquotapis(String aliquotapis) {
		this.aliquotapis = aliquotapis;
	}

	public String getCofins() {
		return cofins;
	}

	public void setCofins(String cofins) {
		this.cofins = cofins;
	}

	@DisplayName("QTDE REGISTROS")
	public int getQuantidadeRegistros() {
		return quantidadeRegistros;
	}

	public void setQuantidadeRegistros(int quantidadeRegistros) {
		this.quantidadeRegistros = quantidadeRegistros;
	}

	public String getNcm() {
		return ncm;
	}

	public void setNcm(String ncm) {
		this.ncm = ncm;
	}

	@DisplayName("Al�quota IPI")
	public String getAliquotaipi() {
		return aliquotaipi;
	}

	public void setAliquotaipi(String aliquotaipi) {
		this.aliquotaipi = aliquotaipi;
	}

	@DisplayName("Al�quota Cofins")
	public String getAliquotacofins() {
		return aliquotacofins;
	}

	public void setAliquotacofins(String aliquotacofins) {
		this.aliquotacofins = aliquotacofins;
	}

	public boolean isNcmSelecionado() {
		return ncmSelecionado;
	}

	public void setNcmSelecionado(boolean ncmSelecionado) {
		this.ncmSelecionado = ncmSelecionado;
	}
	
	public String getDescricaoNatOprAssociada() {
		if (descricaoNatOprAssociada != null) {
			return descricaoNatOprAssociada;
		}
		
		if (naturezaoperacao != null && StringUtils.isNotEmpty(naturezaoperacao.getNome())) {
			return naturezaoperacao.getNome();
		}
		
		return "";
	}
	
	public void setDescricaoNatOprAssociada(String descricaoNatOprAssociada) {
		this.descricaoNatOprAssociada = descricaoNatOprAssociada;
	}
	
	public String getCfopLabel() {
		if (cfop != null && StringUtils.isNotEmpty(cfop.getCodigo())) {
			return cfop.getCodigo();
		} else {
			return "";
		}
	}
	
	public void setCfopLabel(String cfopLabel) {
		this.cfopLabel = cfopLabel;
	}
	
	public String getNaturezaOperacaoLabel() {
		if (naturezaoperacao != null && StringUtils.isNotEmpty(naturezaoperacao.getNome())) {
			return naturezaoperacao.getNome();
		} else {
			return "";
		}
	}
	
	public void setNaturezaOperacaoLabel(String naturezaOperacaoLabel) {
		this.naturezaOperacaoLabel = naturezaOperacaoLabel;
	}
}
