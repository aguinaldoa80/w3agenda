package br.com.linkcom.sined.modulo.sistema.controller.process;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.util.menu.MenuBancoDadosUtil;

@Bean
@Controller(path="/sistema/process/GerenciaMenu")
public class GerenciaMenuProcess extends MultiActionController {
	
	/**
	 * Ajax para carregar o menu.
	 *
	 * @param request
	 * @since 30/07/2012
	 * @author Rodrigo Freitas
	 */
	public void ajaxLoadMenu(WebRequestContext request, Menumodulo menumodulo) {
//		request.getSession().setAttribute("W3ERP_MODULO", menumodulo.getCdmenumodulo());
		StringBuilder sbMenu = new MenuBancoDadosUtil().makeMenu(menumodulo);
		View.getCurrent().println(sbMenu.toString());
	}
}
