package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/sistema/process/ForcarArquivosAndroid", authorizationModule = ProcessAuthorizationModule.class)
public class ForcarArquivosAndroidProcess extends MultiActionController{
	
	private PedidovendaService pedidovendaService;
	
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	
	@DefaultAction
	public ModelAndView doFilter(WebRequestContext requestContext){
		return new ModelAndView("process/forcarArquivosAndroid");
	}
	
	@Action("gerar")
	public synchronized void gerarArquivos(WebRequestContext requestContext){
		try {
			String banco = SinedUtil.getStringConexaoBanco();
			pedidovendaService.gerarArquivosAndroid(banco);
		} catch (Exception e) {
			throw new RuntimeException("Erro ao gerar Arquivos android.");
		}	
	}
		
}
