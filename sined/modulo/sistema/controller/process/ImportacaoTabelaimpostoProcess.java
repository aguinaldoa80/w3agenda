package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tabelaimposto;
import br.com.linkcom.sined.geral.bean.Tabelaimpostoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaimpostoitemtipo;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.TabelaimpostoService;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ImportacaoTabelaimpostoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;
import br.com.linkcom.sined.util.thread.TabelaIBPTThread;

@Controller(path="/sistema/process/ImportacaoTabelaimposto", authorizationModule=ProcessAuthorizationModule.class)
public class ImportacaoTabelaimpostoProcess extends MultiActionController {
	
	private EmpresaService empresaService;
	private TabelaimpostoService tabelaimpostoService;
	
	public void setTabelaimpostoService(
			TabelaimpostoService tabelaimpostoService) {
		this.tabelaimpostoService = tabelaimpostoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ImportacaoTabelaimpostoFiltro filtro) {
		return new ModelAndView("process/importacaoTabelaimposto", "filtro", filtro);
	}
	
	public ModelAndView abrirConfirmacaoDados(WebRequestContext request, ImportacaoTabelaimpostoFiltro filtro) {
		if (filtro.getArquivo() == null || 
			filtro.getArquivo().getContent() == null || 
			filtro.getArquivo().getContent().length == 0)
			throw new SinedException("O arquivo deve ser informado.");		
		
		String strFile = new String(filtro.getArquivo().getContent());
		String[] linhas = strFile.split("\\r?\\n");
		List<String> linhasPreenchidas = new ArrayList<String>();
		for (int i = 1; i < linhas.length; i++) {
			String linha = linhas[i];
			if(linha != null && !linha.replaceAll(";", "").trim().equals("")){
				linhasPreenchidas.add(linha);
			}
		}
		if(linhasPreenchidas.size() == 0){
			throw new SinedException("Nenhum registro a ser importado.");		
		}
		
		Empresa empresa = empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.nomefantasia");
		Date dataAtual = SinedDateUtils.currentDate();
		
		String chave = null;
		String versao = null;
		Date dtvigenciainicio = null;
		Date dtvigenciafim = null;
		
		List<String> listaErro = new ArrayList<String>();
		
		boolean valido = false;
		
		List<Tabelaimpostoitem> listaTabelaimpostoitem = new ArrayList<Tabelaimpostoitem>();
		for (String linha : linhasPreenchidas) {
			try {
				String[] campo = linha.split(";");
				
				String codigo = campo[0];
				String ex = campo[1];
				String tipo = campo[2];
				String descricao = campo[3];
				String nacionalfederal = campo[4];
				String importadosfederal = campo[5];
				String estadual = campo[6];
				String municipal = campo[7];
				String vigenciainicio = campo[8];
				String vigenciafim = campo[9];
				chave = campo[10];
				versao = campo[11];
				// String fonte = campo[12];
				
				try {
					dtvigenciainicio = SinedDateUtils.stringToDate(vigenciainicio);
				} catch (ParseException e) {
					throw new SinedException("Erro na convers�o do in�cio da vig�ncia: " + e.getMessage(), e);
				}
				
				try {
					dtvigenciafim = SinedDateUtils.stringToDate(vigenciafim);
				} catch (ParseException e) {
					throw new SinedException("Erro na convers�o do fim da vig�ncia: " + e.getMessage(), e);
				}
				
				if(!valido){
					if(SinedDateUtils.beforeIgnoreHour(dataAtual, dtvigenciainicio) || SinedDateUtils.afterIgnoreHour(dataAtual, dtvigenciafim)){
						listaErro.add("A tabela usada n�o � uma tabela vigente!");
						break;
					}
					
					valido = true;
				}
				
				Tabelaimpostoitemtipo tipoBean = null;
				try{
					tipoBean = Tabelaimpostoitemtipo.values()[Integer.parseInt(tipo)];
				} catch (Exception e) {
					new SinedException("Erro na convers�o do tipo: " + e.getMessage(), e);
				}
				
				if(codigo != null){
					codigo = br.com.linkcom.neo.util.StringUtils.soNumero(codigo);
					codigo = br.com.linkcom.neo.util.StringUtils.stringCheia(codigo, "0", 9, false);
				}
	
				Tabelaimpostoitem tabelaimpostoitem = new Tabelaimpostoitem();
				tabelaimpostoitem.setCodigo(StringUtils.trimToNull(codigo));
				tabelaimpostoitem.setExtipi(StringUtils.trimToNull(ex));
				tabelaimpostoitem.setTipo(tipoBean);
				tabelaimpostoitem.setDescricao(StringUtils.trimToNull(descricao));
				
				if(!StringUtils.isBlank(importadosfederal)) tabelaimpostoitem.setImportadofederal(Double.parseDouble(importadosfederal));
				if(!StringUtils.isBlank(nacionalfederal)) tabelaimpostoitem.setNacionalfederal(Double.parseDouble(nacionalfederal));
				if(!StringUtils.isBlank(estadual)) tabelaimpostoitem.setEstadual(Double.parseDouble(estadual));
				if(!StringUtils.isBlank(municipal)) tabelaimpostoitem.setMunicipal(Double.parseDouble(municipal));
				
				listaTabelaimpostoitem.add(tabelaimpostoitem);
			} catch (Exception e) {
				e.printStackTrace();
				listaErro.add(e.getMessage());
			}
		}
		
		if(listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			return sendRedirectToAction("index");
		}
		
		
		
		String identificadorSessao = System.currentTimeMillis() + "";
		
		Tabelaimposto tabelaimposto = new Tabelaimposto();
		tabelaimposto.setChave(chave);
		tabelaimposto.setDtimportacao(dataAtual);
		tabelaimposto.setDtvigenciainicio(dtvigenciainicio);
		tabelaimposto.setDtvigenciafim(dtvigenciafim);
		tabelaimposto.setEmpresa(empresa);
		tabelaimposto.setVersao(versao);
		tabelaimposto.setListaTabelaimpostoitem(listaTabelaimpostoitem);
		tabelaimposto.setIdentificadorSessao(identificadorSessao);
		tabelaimposto.setArquivo(filtro.getArquivo());
		
		request.getSession().setAttribute("tabelaimposto_" + identificadorSessao, tabelaimposto);
		request.setAttribute("qtdeListaTabelaimpostoitem", listaTabelaimpostoitem.size());
		
		return new ModelAndView("process/importacaoTabelaimpostoConferencia", "tabelaimposto", tabelaimposto);
	}
	
	public ModelAndView salvaConfirmacaoDados(WebRequestContext request, Tabelaimposto tabelaimposto){
		String sessionKey = "tabelaimposto_" + tabelaimposto.getIdentificadorSessao();
		tabelaimposto = (Tabelaimposto) request.getSession().getAttribute(sessionKey);

		tabelaimpostoService.saveOrUpdate(tabelaimposto);
		
		request.getSession().removeAttribute(sessionKey);
		
		if(IntegracaoEmporiumUtil.util.haveIntegracaoEcf()){
			new TabelaIBPTThread(SinedUtil.getStringConexaoBanco()).start();
		}
		request.addMessage("Tabela importada com sucesso.");
		return sendRedirectToAction("index");
	}
	
}