package br.com.linkcom.sined.modulo.sistema.controller.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;

@Controller(path="/sistema/process/Download", authorizationModule=ProcessAuthorizationModule.class)
public class DownloadProcess extends MultiActionController {

	@DefaultAction
	public ModelAndView index(WebRequestContext request) {
		return new ModelAndView("process/download");
	}
	
}