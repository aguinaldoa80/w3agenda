package br.com.linkcom.sined.modulo.sistema.controller.process;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Campolistagemusuario;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.service.CampolistagemusuarioService;
import br.com.linkcom.sined.geral.service.TelaService;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/sistema/process/GerenciaCampolistagemusuario")
public class GerenciaCampolistagem extends MultiActionController{
		
	private TelaService telaService;
	private CampolistagemusuarioService campolistagemusuarioService;
			
	public void setTelaService(TelaService telaService) {
		this.telaService = telaService;
	}
	public void setCampolistagemusuarioService(
			CampolistagemusuarioService campolistagemusuarioService) {
		this.campolistagemusuarioService = campolistagemusuarioService;
	}


	/**
	* M�todo que salva a ordem das colunas e quais ser�o exibidas (Definido pelo usu�rio)
	* Obs: esse m�todo � chamado toda vez que ele move uma coluna ou clica no checkbox de exibir/ocultar colunas 
	*
	* @param request
	* @param tela
	* @since Oct 25, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxSalvaOrdenacaoCampolistagemUsuario(WebRequestContext request){
		
		String path = request.getParameter("pathTela");
		String camposordem = request.getParameter("ordemUsuario");
		String camposexibicao = request.getParameter("ordemexibicaoUsuario");
		
		if(path != null && !path.equals("")){
			Tela tela = telaService.findTelaByPath(path);
			if(tela != null && tela.getCdtela() != null){
				Campolistagemusuario campolistagemusuario = campolistagemusuarioService.findByTelaUsuario(tela, SinedUtil.getUsuarioLogado());
				
				if(campolistagemusuario != null && campolistagemusuario.getCdcampolistagemusuario() != null){						
					campolistagemusuario.setCamposordem(camposordem);
					campolistagemusuario.setCamposexibicao(camposexibicao);
					campolistagemusuarioService.atualizaCamposOrdemUsuario(campolistagemusuario);
				}else{
					campolistagemusuario = new Campolistagemusuario();
					campolistagemusuario.setCamposordem(camposordem);
					campolistagemusuario.setCamposexibicao(camposexibicao);
					campolistagemusuario.setUsuario(SinedUtil.getUsuarioLogado());
					campolistagemusuario.setTela(tela);
					campolistagemusuarioService.saveOrUpdate(campolistagemusuario);
				}
				
			}
		}
	}
}
