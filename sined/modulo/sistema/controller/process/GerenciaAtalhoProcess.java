package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.util.Iterator;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Pessoaatalho;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.PessoaatalhoService;
import br.com.linkcom.sined.geral.service.TelaService;
import br.com.linkcom.sined.util.SinedUtil;

@Bean
@Controller(path="/sistema/process/GerenciaAtalho")
public class GerenciaAtalhoProcess extends MultiActionController {
	
	private PessoaatalhoService pessoaatalhoService;
	private TelaService telaService;
	
	public void setPessoaatalhoService(PessoaatalhoService pessoaatalhoService) {
		this.pessoaatalhoService = pessoaatalhoService;
	}
	
	public void setTelaService(TelaService telaService) {
		this.telaService = telaService;
	}

	@SuppressWarnings("unchecked")
	public void ajaxAddOrRemoveAtalho(WebRequestContext request) {
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		String action = request.getParameter("action");
		String path = request.getParameter("path");
		Tela tela = null;
		
		List<Tela> listaTela = telaService.findByPath(path);
		if (listaTela != null && !listaTela.isEmpty()) {
			tela = listaTela.get(0);
		}
		
		if (tela != null) {
			if ("add".equals(action)) {
				Pessoaatalho pessoaatalho = new Pessoaatalho(usuarioLogado, tela);
				pessoaatalhoService.saveOrUpdate(pessoaatalho);
			}
			else if ("remove".equals(action)) {
				Pessoaatalho pessoaatalho = pessoaatalhoService.loadByPessoaTela(usuarioLogado, tela);
				pessoaatalhoService.delete(pessoaatalho);
			}
			request.getSession().setAttribute("LISTA_ATALHO", null);
			SinedUtil.carregaAtalhosUsuario(request);
			
			StringBuilder sbMenu = new StringBuilder();
			Object attribute = request.getSession().getAttribute("LISTA_ATALHO");
			if (attribute != null) {
				List<Tela> listaAtalho = (List<Tela>) attribute;
				sbMenu.append(convertToJavaScript(listaAtalho, "listaAtalho", request.getServletRequest().getContextPath()));
			}
			View.getCurrent().println(sbMenu.toString());
		}
	}
	
	private String convertToJavaScript(List<Tela> listaTela, String var, String context) {
		StringBuilder javascript = new StringBuilder();		
		if(Util.strings.isNotEmpty(var)){
			javascript.append("var "+var+" = ");
		}
		javascript.append("[");
		String descricao;
		String path;
		
		for (Iterator<Tela> iter = listaTela.iterator(); iter.hasNext();) {
			Tela tela = iter.next();
			descricao = SinedUtil.escapeSingleQuotes(tela.getDescricao());
			path = SinedUtil.escapeSingleQuotes(context + tela.getPath());
			
			javascript.append("['"+path+"', '"+descricao+"']");
			if(iter.hasNext()){
				javascript.append(",");
			}			
		}
		javascript.append("];");
		return javascript.toString();		

	}	
	
}
